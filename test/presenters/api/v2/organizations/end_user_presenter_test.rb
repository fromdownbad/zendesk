require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Organizations::EndUserPresenter do
  extend Api::Presentation::TestHelper
  fixtures :organizations, :accounts, :users

  before do
    @model     = organizations(:serialization_organization)
    @presenter = Api::V2::Organizations::EndUserPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :shared_tickets, :shared_comments, :url

  should_present_method :id
  should_present_method :name

  should_present_url :api_v2_organization_url

  it "only present id, name and url keys" do
    @presenter.options[:condensed] = true
    assert_same_elements(
      [:id, :name, :url, :shared_tickets, :shared_comments],
      @presenter.model_json(@model).keys
    )
  end
end
