require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Billing::TokenPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  let(:encrypted) do
    {
      payload: 'something-encrypted-by-zbc',
      nonce:   'random-iv-generated-by-encryption'
    }
  end

  before do
    @account   = account
    @model     = encrypted
    @presenter = Api::V2::Billing::TokenPresenter.new(
      @account.owner,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :payload, :nonce

  let(:expected) do
    {
      payload: 'something-encrypted-by-zbc',
      nonce:   'random-iv-generated-by-encryption'
    }
  end

  it 'presents the billing-service connection model' do
    @presenter.model_json(encrypted).must_equal expected
  end
end
