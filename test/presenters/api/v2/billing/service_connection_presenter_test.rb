require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Billing::ServiceConnectionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:service_connection) do
    Billing::ServiceConnection.build(account, user)
  end

  before do
    # Billing::ServiceConnection::V1::JwtToken.
    #   any_instance.stubs(:to_s).returns('token')
    Billing::JWT.stubs(:encrypt).returns('token')

    @account   = account
    @model     = service_connection
    @presenter = Api::V2::Billing::ServiceConnectionPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :url, :token, :payload

  let(:expected) do
    {
      url:     'https://minimum.zd-test.com/billing?issuer=zendesk&token=token',
      token:   'token',
      payload: {
        product_type:      2,
        product_id:        account.id.to_s,
        user_id:           user.id,
        user_name:         'Admin Man',
        user_email:        'minimum_admin@aghassipour.com',
        organization_name: 'minimum'
      }
    }
  end

  it 'presents the billing-service connection model' do
    @presenter.model_json(service_connection).must_equal expected
  end
end
