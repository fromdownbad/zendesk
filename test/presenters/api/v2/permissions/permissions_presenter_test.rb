require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Permissions::PermissionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :users, :accounts

  before do
    @agent = users(:minimum_agent)
    @presenter = Api::V2::Permissions::PermissionsPresenter.new(@agent, url_builder: mock_url_builder)
  end

  it "has external_permissions as the root key" do
    assert_equal :external_permissions, @presenter.model_key
  end

  describe "presented abilities" do
    let(:permissions_agent_client) { stub }
    let(:account) { { id: @agent.account.id, shard_id: 1 } }
    let(:user) { { id: @agent.id, role: @agent.roles, permission_set_id: nil } }
    let(:resource_scopes) { Access::ExternalPermissions::Permissions::ADMIN_PERMISSIONS }
    let(:request) { { input: { account: account, user: user, resource_scopes: resource_scopes } } }
    let(:response) { { "result" => ["organization_fields:manage"] } }

    before(:all) do
      Zendesk::ExternalPermissions::PermissionsAgentClient.stubs(:new).returns(permissions_agent_client)
      permissions_agent_client.stubs(:request).with(account, user, resource_scopes).returns(request)
      permissions_agent_client.stubs(:allow_scopes!).with(request).returns(stub(body: response))
      @agent.account.stubs("has_permissions_organization_fields_manage?").returns(true)
      @output = @presenter.model_json(@agent)
    end

    it "displays :can_manage_organization_fields correctly" do
      assert_equal @presenter.can_manage_organization_fields?(@agent, ["organization_fields:manage"]), @output[:can_manage_organization_fields]
    end

    it 'can_manage_user_fields is false' do
      refute @output[:can_manage_user_fields]
    end
  end
end
