require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Users::AgentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users, :accounts, :organizations, :tickets, :user_settings, :role_settings, :permission_sets, :zopim_subscriptions

  let(:permissions_agent_client) { stub }

  before do
    @model = users(:serialization_user)
    @model.account.stubs(:has_user_and_organization_fields?).returns(false)
    @model.account.stubs(:has_ocp_chat_only_agent_deprecation?).returns(false)
    @model.account.stubs(:has_chat_permission_set?).returns(true)
    @org = OrganizationMembership.create!(account: @model.account, organization_id: organizations(:serialization_organization).id, default: true, user_id: @model.id)
    assert @model.settings.any?
    @url_builder = mock_url_builder
    @url_builder.stubs(:send).
      with(:api_v2_user_url, anything, anything).
      returns("http://fakeurl.com/1.json")
    @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
      url_builder: mock_url_builder,
      user_url_builder: @url_builder)
  end

  describe "#condensed_model_json" do
    it "only present id, name, email and url keys" do
      assert_same_elements(
        [:id, :name, :email, :url],
        @presenter.condensed_model_json(@model).keys
      )
    end
  end

  it "has 'user' as the root key" do
    assert_equal :user, @presenter.model_key
  end

  should_present_keys :id, :external_id, :name, :alias, :created_at, :updated_at,
    :active, :verified, :shared, :locale_id, :time_zone, :iana_time_zone, :last_login_at, :email,
    :phone, :shared_phone_number, :photo, :signature, :details, :notes, :organization_id,
    :role, :role_type, :custom_role_id, :moderator, :ticket_restriction,
    :only_private_comments, :user_fields, :tags, :url,
    :suspended, :restricted_agent, :locale, :shared_agent,
    :chat_only, :two_factor_auth_enabled, :default_group_id, :report_csv

  should_present_method :id
  should_present_method :external_id
  should_present_method :alias
  should_present_method :created_at
  should_present_method :updated_at, value: 10.minutes.from_now
  should_present_method :is_active?, as: :active
  should_present_method :otp_configured?, as: :two_factor_auth_enabled
  should_present_method :is_verified?, as: :verified
  should_present_method :time_zone, value: 'Copenhagen'
  should_present_method :iana_time_zone
  should_present_method :last_login, as: :last_login_at
  should_present_method :email
  should_present_method :phone, value: '+14155558888'
  should_present_method :shared_phone_number, value: false
  should_present_method :photo, with: Api::V2::AttachmentPresenter
  should_present_method :details
  should_present_method :notes
  should_present_method :is_moderator?, as: :moderator
  should_present_method :foreign?, as: :shared
  should_present_method :foreign_agent?, as: :shared_agent
  should_present_method :is_private_comments_only?, as: :only_private_comments
  should_present_method :is_restricted_agent?, as: :restricted_agent
  should_present_method :suspended?, as: :suspended
  should_present_method :is_chat_agent?, as: :chat_only

  it "returns the correct default organization" do
    assert_equal @org.organization_id, @presenter.model_json(@model)[:organization_id]
  end

  it "presents url provided by generator" do
    assert_equal "http://fakeurl.com/#{@model.id}.json", @presenter.model_json(@model)[:url]
  end

  describe "authenticity token" do
    describe "when user is same as model" do
      it "is optionally included" do
        @presenter.options[:url_builder].stubs(:form_authenticity_token).returns(:blah)
        refute @presenter.model_json(@model).include?(:authenticity_token)
        @presenter.options[:include_authenticity_token] = true
        assert @presenter.model_json(@model).include?(:authenticity_token)
      end
    end

    describe "when user is different from the model" do
      before do
        @model = users(:minimum_agent)
      end

      it "is not allowed" do
        @presenter.options[:include_authenticity_token] = true
        refute @presenter.model_json(@model).include?(:authenticity_token)
      end
    end
  end

  describe "with a custom_role_id" do
    describe "user has permission_set" do
      before do
        subject.stubs(custom_role_id: 1)
        @custom_role_id = @presenter.model_json(subject)[:custom_role_id]
      end

      describe "as an agent" do
        subject { users(:multiproduct_custom_agent) }

        it "is 1" do
          assert_equal 1, @custom_role_id
        end
      end
    end

    describe "user does not have permission_set" do
      before do
        @custom_role_id = @presenter.model_json(subject)[:custom_role_id]
      end

      describe "as an agent" do
        subject { users(:minimum_agent) }

        it "is nil" do
          assert_nil @custom_role_id
        end
      end
    end
  end

  describe "with a role_type" do
    describe "account has permission_sets" do
      before do
        subject.account.stubs(has_permission_sets?: true)
      end

      describe "as an agent with a custom role" do
        subject { users(:minimum_agent) }

        before do
          custom_role = PermissionSet.build_default_permission_sets(@presenter.account).first
          subject.permission_set = custom_role
        end

        it "is a custom role type" do
          assert_equal ::PermissionSet::Type::CUSTOM, @presenter.model_json(subject)[:role_type]
        end
      end

      describe "as an agent without a custom role" do
        subject { users(:minimum_agent) }

        it "is nil" do
          assert_nil @presenter.model_json(subject)[:role_type]
        end
      end

      describe "as an admin" do
        subject { users(:minimum_admin) }

        it "is nil" do
          assert_nil @presenter.model_json(subject)[:role_type]
        end
      end
    end

    describe "account does not have permission_sets" do
      before do
        subject.account.stubs(has_permission_sets?: false)
      end

      describe "as an agent" do
        subject { users(:minimum_agent) }

        it "is nil" do
          assert_nil @presenter.model_json(subject)[:role_type]
        end
      end

      describe "account has zopim chat enabled" do
        before do
          subject.stubs(is_chat_agent?: true)
        end

        describe "as a chat agent" do
          subject { users(:with_paid_zopim_subscription_chat_agent) }

          it "is the chat agent role type" do
            assert_equal ::PermissionSet::Type::CHAT_AGENT, @presenter.model_json(subject)[:role_type]
          end
        end
      end

      describe "account has light agents enabled" do
        before do
          subject.stubs(is_light_agent?: true)
          subject.account.stubs(has_light_agents?: true)
        end

        describe "as a light agent" do
          subject { users(:multiproduct_light_agent) }

          it "is the light agent role type" do
            assert_equal ::PermissionSet::Type::LIGHT_AGENT, @presenter.model_json(subject)[:role_type]
          end
        end
      end

      describe "as a Contributor" do
        subject { users(:multiproduct_contributor) }

        it "is the contributor agent role type" do
          assert_equal ::PermissionSet::Type::CONTRIBUTOR, @presenter.model_json(subject)[:role_type]
        end
      end
    end
  end

  it "presents the signature value" do
    @model.stubs(:signature).returns(nil)
    json = @presenter.model_json(@model)
    assert_nil json[:signature]

    @model.stubs(:signature).returns(stub(value: 'signature'))
    json = @presenter.model_json(@model)
    assert_equal 'signature', json[:signature]
  end

  it "presents the user role as a string" do
    @model.stubs(:roles).returns(Role::END_USER.id)
    json = @presenter.model_json(@model)
    assert_equal 'end-user', json[:role]

    @model.stubs(:roles).returns(Role::AGENT.id)
    json = @presenter.model_json(@model)
    assert_equal 'agent', json[:role]

    @model.stubs(:roles).returns(Role::ADMIN.id)
    json = @presenter.model_json(@model)
    assert_equal 'admin', json[:role]
  end

  it "presents the ticket restriction as a string" do
    @model.stubs(:restriction_id).returns(Zendesk::Types::RoleRestrictionType.ORGANIZATION)
    json = @presenter.model_json(@model)
    assert_equal 'organization', json[:ticket_restriction]

    @model.stubs(:restriction_id).returns(Zendesk::Types::RoleRestrictionType.GROUPS)
    json = @presenter.model_json(@model)
    assert_equal 'groups', json[:ticket_restriction]

    @model.stubs(:restriction_id).returns(Zendesk::Types::RoleRestrictionType.ASSIGNED)
    json = @presenter.model_json(@model)
    assert_equal 'assigned', json[:ticket_restriction]

    @model.stubs(:restriction_id).returns(Zendesk::Types::RoleRestrictionType.REQUESTED)
    json = @presenter.model_json(@model)
    assert_equal 'requested', json[:ticket_restriction]

    @model.stubs(:restriction_id).returns(Zendesk::Types::RoleRestrictionType.NONE)
    json = @presenter.model_json(@model)
    assert_nil json[:ticket_restriction]
  end

  describe "restricted_agent" do
    before do
      @restricted_agent = @presenter.model_json(subject)[:restricted_agent]
    end

    describe "as an admin" do
      subject { users(:minimum_admin) }

      it "is false" do
        refute @restricted_agent
      end
    end

    describe "as an unrestricted agent" do
      subject { users(:minimum_agent) }

      it "is false" do
        refute @restricted_agent
      end
    end

    describe "as a restricted agent" do
      subject { users(:with_groups_agent_groups_restricted) }

      it "is true" do
        assert @restricted_agent
      end
    end
  end

  describe "when the account has user_and_organization_fields" do
    before { @model.account.stubs(:has_user_and_organization_fields?).returns(true) }

    it "presents custom user fields" do
      assert_includes @presenter.model_json(@model), :user_fields
    end
  end

  describe "when the account does not have user_and_organization_fields" do
    before { @model.account.stubs(:has_user_and_organization_fields?).returns(false) }

    it "does not present custom user fields" do
      assert_equal({}, @presenter.model_json(@model)[:user_fields])
    end
  end

  describe "when the account has_ocp_chat_only_deprecation? arturo" do
    before { @model.account.stubs(:has_ocp_chat_only_agent_deprecation?).returns(true) }

    it "does not present chat only value for agent" do
      refute @presenter.model_json(@model).include?(:chat_only)
    end
  end

  describe "when the account has user tagging enabled" do
    before { @presenter.account.stubs(:has_user_and_organization_tags?).returns(true) }

    should_present_method :tags

    it "includes taggings in #association_preloads" do
      assert_not_nil @presenter.association_preloads[:taggings]
    end
  end

  describe "when the account has user tagging disabled" do
    before { @presenter.account.settings.stubs(:has_user_and_organization_tags?).returns(false) }

    it "presents an empty array of tags regardless of the tags on the user" do
      @model.stubs(:tags).returns(['tag1', 'tag2'])
      json = @presenter.model_json(@model)
      assert_equal [], json[:tags]
    end

    it "does not include taggings in #association_preloads" do
      assert_nil @presenter.association_preloads[:taggings]
    end
  end

  describe "with stubbed identity presenter" do
    before do
      @model.identities.each do |identity|
        @presenter.send(:identity_presenter).expects(:model_json).with(identity)
      end
    end

    should_side_load :organizations, :roles, :identities, :groups, :highlights, :open_ticket_counts, :open_ticket_count, :related
  end

  describe 'permanently_deleted key' do
    def assert_active_user_nil
      json = @presenter.model_json(@model)
      assert_nil json[:permanently_deleted]
    end

    def assert_soft_deleted_user_nil
      @model.stubs(:deleted?).returns(true)
      @model.stubs(:ultra_deleted?).returns(false)
      json = @presenter.model_json(@model)
      assert_nil json[:permanently_deleted]
    end

    def assert_soft_deleted_user_false
      @model.stubs(:deleted?).returns(true)
      @model.stubs(:ultra_deleted?).returns(false)
      json = @presenter.model_json(@model)
      assert_equal false, json[:permanently_deleted]
    end

    def assert_hard_deleted_user_nil
      @model.stubs(:deleted?).returns(true)
      @model.stubs(:ultra_deleted?).returns(true)
      json = @presenter.model_json(@model)
      assert_nil json[:permanently_deleted]
    end

    def assert_hard_deleted_user_true
      @model.stubs(:deleted?).returns(true)
      @model.stubs(:ultra_deleted?).returns(true)
      json = @presenter.model_json(@model)
      assert json[:permanently_deleted]
    end

    describe 'when include_permanently_deleted is false' do
      before do
        @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
          url_builder: mock_url_builder,
          user_url_builder: @url_builder,
          include_permanently_deleted: false)
      end

      it 'is not presented on active users' do
        assert_active_user_nil
      end

      it 'is not presented on soft deleted users' do
        assert_soft_deleted_user_nil
      end

      it 'is not presented on hard deleted users' do
        assert_hard_deleted_user_nil
      end
    end

    describe 'when include_permanently_deleted is true' do
      before do
        @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
          url_builder: mock_url_builder,
          user_url_builder: @url_builder,
          include_permanently_deleted: true)
      end

      it 'is not presented on active users' do
        assert_active_user_nil
      end

      it 'is presented on soft deleted users' do
        assert_soft_deleted_user_false
      end

      it 'is presented on hard deleted users' do
        assert_hard_deleted_user_true
      end
    end
  end

  describe "side-loading" do
    before do
      @presenter.stubs(:side_load?).returns(false)
    end

    describe "organizations" do
      before do
        @users = [
          User.new(organization: organizations(:minimum_organization1)),
          User.new(organization: organizations(:minimum_organization2)),
          User.new(organization: organizations(:minimum_organization1)),
          User.new(organization: nil)
        ]

        @presenter.account.settings.stubs(:has_user_tags?).returns(false)
        @presenter.expects(:side_load?).with(:organizations).returns(true)
      end

      it "preloads organizations" do
        @users.expects(:pre_load).with(organizations: @presenter.send(:organization_presenter).association_preloads)
        @presenter.side_loads(@users)
      end

      should_do_sql_queries(0) { @presenter.side_loads(@users) }

      it "uses the OrganizationPresenter" do
        @presenter.send(:organization_presenter).expects(:model_json).twice.returns(:foo, :bar)

        assert_equal [:foo, :bar], @presenter.side_loads(@users)[:organizations]
      end

      it "exposes private attributes" do
        @model.roles = Role::AGENT.id
        assert @presenter.present(users(:minimum_end_user))[:organizations][0].key?(:external_id)
      end
    end

    describe 'open_ticket_counts' do
      before do
        @presenter.expects(:side_load?).with(:open_ticket_counts).returns(true)

        @agent = users(:serialization_agent)
        tickets(:serialization_ticket)
      end

      it 'exposes the assigned ticket counts by id' do
        json = @presenter.side_loads([@model, @agent])

        assert_equal({ @model.id => 0, @agent.id => 1 }, json[:open_ticket_counts])
      end
    end

    describe 'open_ticket_count' do
      before do
        @presenter.expects(:side_load?).with(:open_ticket_count).returns(true)

        @agent = users(:serialization_agent)
        tickets(:serialization_ticket)
      end

      it 'exposes the assigned ticket count by id' do
        json = @presenter.side_loads([@model, @agent])

        assert_equal({ @model.id => 0, @agent.id => 1 }, json[:open_ticket_count])
      end
    end

    describe "when blocklist is requested" do
      describe "when presenting multiple users" do
        before do
          @presenter.stubs(side_load?: false)
          @presenter.stubs(:side_load?).with(:blocklist).returns(true)
          @models = [@model, users('minimum_admin'), users('minimum_end_user'), users('minimum_end_user2'), users('minimum_search_user'), users('minimum_author'), users('minimum_agent')]
        end

        should_do_sql_queries(18) { @presenter.present(@models) }
      end

      it "includes whether or not the user is blocklisted" do
        @presenter.expects(:side_load?).with(:blocklist).returns(true).once
        assert @presenter.model_json(@model).key?(:blocklisted)
      end
    end

    describe "abilities" do
      before do
        @presenter.expects(:side_load?).with(:abilities).returns(true)
        @presenter.model_json(@model)
      end

      before_should "use the user abilities presenter" do
        @presenter.user_abilities_presenter.expects(:model_json).with(@model)
      end
    end

    describe "external_permissions" do
      before do
        @presenter.expects(:side_load?).with(:external_permissions).returns(true)
        @presenter.model_json(@model)
      end

      before_should "use the user external_permissions presenter" do
        @presenter.user_permissions_presenter.expects(:model_json).with(@model)
      end
    end

    describe 'related' do
      before do
        @presenter.expects(:side_load?).with(:related).returns(true)
      end

      it 'sideloads user related data when presenting only one user' do
        refute_blank @presenter.side_loads(@model)
        assert_blank @presenter.side_loads([users(:minimum_end_user), users(:minimum_end_user2)])
      end
    end
  end

  describe "nested inclusions" do
    before do
      @presenter.stubs(:side_load?).returns(false)
    end

    describe "settings" do
      before do
        @presenter.expects(:side_load?).with(:settings).returns(true)
      end

      it "is nested under model json for agents" do
        @model.stubs(:is_agent?).returns(true)
        @presenter.send(:settings_presenter).expects(:model_json).with(@model).returns(:foo)
        assert_equal :foo, @presenter.model_json(@model)[:settings]
      end

      it "does not be presented for non-agents" do
        @model.stubs(:is_agent?).returns(false)
        @presenter.send(:settings_presenter).expects(:model_json).never
        assert_nil @presenter.model_json(@model)[:settings]
      end
    end
  end

  describe "user custom fields" do
    before do
      @model.account.stubs(:has_user_and_organization_fields?).returns(true)

      field = @model.account.custom_fields.for_user.build(key: "field_1", title: "Field 1")
      field.type = "Checkbox"
      field.save!

      field = @model.account.custom_fields.for_user.build(key: "field_2", title: "Field 2")
      field.type = "Integer"
      field.save!

      field = @model.account.custom_fields.for_user.build(key: "field_3", title: "Field 3")
      field.type = "Dropdown"
      field.save!

      field = CustomField::Dropdown.find(field.id)
      field.dropdown_choices.build(name: "Foo Name", value: "foo")
      field.dropdown_choices.build(name: "Bar Name", value: "bar")
      field.dropdown_choices.build(name: "Baz Name", value: "baz")
      field.save!

      @json = @presenter.model_json(@model)
    end

    it "is included" do
      assert @json[:user_fields]
    end

    describe "with a custom field or two" do
      before do
        @model.custom_field_values.update_from_hash("field_1" => true, "field_2" => 12345, "field_3" => "baz")
        @model.save!
      end

      it "models the user_fields properly" do
        assert_equal({ "field_1" => true, "field_2" => 12345, "field_3" => "baz"}, @presenter.model_json(@model)[:user_fields])
      end
    end

    describe "exporting users" do
      before do
        @field_1 = @model.account.custom_fields.for_user.first
        @field_2 = @model.account.custom_fields.for_user.last
      end

      it "excludes fields marked as not exportable" do
        @field_1.is_exportable = false
        @field_1.save!
        owner_account = @model.account
        owner_account.stubs(:custom_fields_for_owner).with('User').returns(owner_account.custom_fields.active.for_owner('User').reload)

        presenter = Api::V2::Users::AgentPresenter.new(@model, url_builder: mock_url_builder, exclude_non_exportable_fields: true)

        exported_field_keys = presenter.model_json(@model)[:user_fields].keys
        refute_includes exported_field_keys, @field_1.key
        assert_equal ['field_2', 'field_3'], exported_field_keys
      end

      it "includes fields not specifically excluded from export" do
        presenter = Api::V2::Users::AgentPresenter.new(@model, url_builder: mock_url_builder, exclude_non_exportable_fields: true)

        exported_field_keys = presenter.model_json(@model)[:user_fields].keys
        assert_equal ['field_1', 'field_2', 'field_3'], exported_field_keys
      end

      it "includes fields explicitly marked as allowed for export" do
        @field_1.is_exportable = true
        @field_1.save!
        owner_account = @model.account
        owner_account.stubs(:custom_fields_for_owner).with('User').returns(owner_account.custom_fields.active.for_owner('User').reload)

        presenter = Api::V2::Users::AgentPresenter.new(@model, url_builder: mock_url_builder, exclude_non_exportable_fields: true)

        exported_field_keys = presenter.model_json(@model)[:user_fields].keys
        assert_equal ['field_1', 'field_2', 'field_3'], exported_field_keys
      end
    end
  end

  describe '#present' do
    before do
      field = @model.account.custom_fields.for_user.build(key: "field_1", title: "Field 1")
      field.type = "Checkbox"
      field.save!

      field = @model.account.custom_fields.for_user.build(key: "field_2", title: "Field 2")
      field.type = "Integer"
      field.save!

      @ten_models = [@model, users('minimum_admin'), users('minimum_end_user'), users('minimum_end_user2'), users('minimum_search_user'), users('minimum_author'), users('minimum_agent')]
      @model.account.stubs(:has_user_and_organization_fields?).returns(true)
    end

    describe 'for 1 user' do
      before do
        @model.account.settings.length # preload so it does not count into sql queries, run with test/unit/api/presenters/v2/requests/comment_presenter_test.rb to see it fail
      end

      should_do_sql_queries(12) { @presenter.present(@model) }
    end

    describe 'for many users' do
      should_do_sql_queries(24..26) { @presenter.present(@ten_models) }
    end
  end

  describe "with redacted fields" do
    before do
      @model = users(:serialization_user)
      url_builder = mock_url_builder
      redacted_fields = [:email, :phone]
      @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
        redacted_fields: redacted_fields,
        url_builder: url_builder)
    end

    it 'does not present redacted fields in models' do
      refute @presenter.model_json(@model).key?(:email)
      refute @presenter.model_json(@model).key?(:phone)
    end
  end

  describe "with nil value fields" do
    before do
      @nil_value_fields = [:name]
    end

    describe "when presenting an end user" do
      before do
        @model = users(:minimum_end_user)
        @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
          nil_value_fields: @nil_value_fields,
          url_builder: mock_url_builder)
      end

      it 'preserves the key for nil value fields' do
        assert @presenter.model_json(@model).key?(:name)
      end

      it 'sets the value to nil for nil value fields' do
        assert_nil @presenter.model_json(@model).fetch(:name)
      end
    end

    describe "when presenting an agent" do
      before do
        @model = users(:minimum_agent)
        @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
          nil_value_fields: @nil_value_fields,
          url_builder: mock_url_builder)
      end

      it 'preserves the key for nil value fields' do
        assert @presenter.model_json(@model).key?(:name)
      end

      it 'preserves the value of the field' do
        assert_equal "Agent Minimum", @presenter.model_json(@model).fetch(:name)
      end
    end
  end

  describe 'is_assuming_user_from_monitor flag' do
    it 'is present if true' do
      @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
        url_builder: mock_url_builder,
        shared_session: {
          'auth_assuming_monitor_user_id' => true
        })
      assert @presenter.model_json(@model)[:is_assuming_user_from_monitor]
    end

    it 'is absent if false' do
      @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
        url_builder: mock_url_builder,
        shared_session: {})
      refute @presenter.model_json(@model).key?(:is_assuming_user_from_monitor)
    end

    it 'is absent if shared_session is not provided to the presenter' do
      @presenter = ::Api::V2::Users::AgentPresenter.new(@model,
        url_builder: mock_url_builder)
      refute @presenter.model_json(@model).key?(:is_assuming_user_from_monitor)
    end
  end

  describe "default_group_id" do
    before do
      @account = accounts(:with_groups)
      @model = users(:with_groups_agent1)
    end

    it "does not run N+1 queries" do
      assert_no_n_plus_one { @presenter.model_json(@model) }
    end

    it "returns a value for if there is a default group" do
      assert_equal @account.default_group.id, @presenter.model_json(@model)[:default_group_id]
      assert_equal @model.default_group_id, @presenter.model_json(@model)[:default_group_id]
    end

    it "returns nil if there are no groups" do
      @model.memberships.delete_all

      assert_nil @presenter.model_json(@model)[:default_group_id]
    end
  end

  describe 'external permissions' do
    let(:permissions_agent_client) { stub }
    let(:account) { { id: @model.account.id, shard_id: 1 } }
    let(:user) { { id: @model.id, role: @model.roles, permission_set_id: nil } }
    let(:resource_scopes) { Access::ExternalPermissions::Permissions::ADMIN_PERMISSIONS }
    let(:request) { { input: { account: account, user: user, resource_scopes: resource_scopes } } }
    let(:response) { { "result" => ["organization_fields:manage"] } }

    before do
      Zendesk::ExternalPermissions::PermissionsAgentClient.stubs(:new).returns(permissions_agent_client)
      permissions_agent_client.stubs(:request).with(account, user, resource_scopes).returns(request)
      permissions_agent_client.stubs(:allow_scopes!).with(request).returns(stub(body: response))
      # Stub the rest of the side_load? calls.
      @presenter.stubs(:side_load?)
      @presenter.expects(:side_load?).with(:external_permissions).returns(true)
    end

    describe_with_arturo_disabled :external_permissions_presentation do
      it 'presents empty external permissions for an agent' do
        external_permissions = @presenter.model_json(@model)[:external_permissions]
        expected_permissions = {
          can_manage_ticket_fields: false,
          can_manage_ticket_forms: false,
          can_manage_contextual_workspaces: false,
          can_manage_user_fields: false,
          can_manage_organization_fields: false
        }
        assert_equal expected_permissions, external_permissions
      end
    end

    describe_with_arturo_disabled :external_permissions_presentation do
      let(:admin_user) { users(:minimum_admin) }
      it 'presents external permissions for an admin' do
        external_permissions = @presenter.model_json(admin_user)[:external_permissions]
        expected_permissions = {
          can_manage_ticket_fields: true,
          can_manage_ticket_forms: true,
          can_manage_contextual_workspaces: true,
          can_manage_user_fields: true,
          can_manage_organization_fields: true
        }
        assert_equal expected_permissions, external_permissions
      end
    end

    describe_with_arturo_enabled :external_permissions_presentation do
      it 'presents the correct external permissions for an agent' do
        @model.account.stubs(:has_permissions_organization_fields_manage?).returns(true)
        external_permissions = @presenter.model_json(@model)[:external_permissions]
        expected_permissions = {
          can_manage_ticket_fields: false,
          can_manage_ticket_forms: false,
          can_manage_contextual_workspaces: false,
          can_manage_user_fields: false,
          can_manage_organization_fields: true
        }
        assert_equal expected_permissions, external_permissions
      end
    end

    describe_with_arturo_enabled :external_permissions_presentation do
      let(:admin_user) { users(:minimum_admin) }
      let(:account) { { id: admin_user.account.id, shard_id: 1 } }
      let(:user) { { id: admin_user.id, role: admin_user.roles, permission_set_id: nil } }
      it 'presents external permissions for an admin if OPA response is empty' do
        permissions_agent_client.stubs(:allow_scopes!).with(request).returns(stub(body: { }))
        external_permissions = @presenter.model_json(admin_user)[:external_permissions]
        expected_permissions = {
            can_manage_ticket_fields: true,
            can_manage_ticket_forms: true,
            can_manage_contextual_workspaces: true,
            can_manage_user_fields: true,
            can_manage_organization_fields: true
        }
        assert_equal expected_permissions, external_permissions
      end
    end

    describe_with_arturo_enabled :external_permissions_presentation do
      it 'presents the correct external permissions for an agent if OPA response is empty' do
        @model.account.stubs(:has_permissions_user_fields_manage?).returns(true)
        permissions_agent_client.stubs(:allow_scopes!).with(request).returns(stub(body: { }))
        external_permissions = @presenter.model_json(@model)[:external_permissions]
        expected_permissions = {
            can_manage_ticket_fields: false,
            can_manage_ticket_forms: false,
            can_manage_contextual_workspaces: false,
            can_manage_user_fields: false,
            can_manage_organization_fields: false
        }
        assert_equal expected_permissions, external_permissions
      end
    end
  end
end
