require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Users::ZopimIdentityPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:user) { users(:minimum_agent) }

  before do
    Zopim::Agent.any_instance.stubs(account: accounts(:minimum))

    @model     = user.build_zopim_identity
    @presenter = Api::V2::Users::ZopimIdentityPresenter.new(
      user, url_builder: mock_url_builder
    )
  end

  should_present_keys :zopim_agent_id,
    :email,
    :is_owner,
    :is_enabled,
    :is_administrator,
    :display_name,
    :syncstate,
    :is_linked,
    :is_serviceable,
    :agent_chat_limit

  should_side_load :zopim_subscription, :zopim_integration
end
