require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Users::EntitlementPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:user) { users(:minimum_agent) }

  let(:empty_input) { { 'entitlements' => nil } }
  let(:inactive_response) do
    {
      entitlements: {
        'chat' => { is_active: false },
        'explore' => { is_active: false },
        'talk' => { is_active: false }
      }
    }
  end

  before do
    @model = {
        'explore' => { 'id' => 10001, 'name' => 'viewer', 'is_active' => true },
        'chat' => { 'id' => 10001, 'name' => 'admin', 'is_active' => true },
        'talk' => { 'id' => 10001, 'name' => 'lead', 'is_active' => false }
      }
    @presenter = Api::V2::Users::EntitlementPresenter.new(
      user, url_builder: mock_url_builder
    )
  end

  should_present_keys :explore, :chat, :talk

  it 'should return empty set when presenting nil input' do
    result = @presenter.present(empty_input)

    assert_equal inactive_response, result
  end
end
