require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Users::UserSeatPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:user) { users(:minimum_agent) }

  before do
    @model     = user.user_seats.build(account_id: user.account.id, seat_type: Zendesk::Entitlement::TYPES[:voice])
    @presenter = Api::V2::Users::UserSeatPresenter.new(
      user, url_builder: mock_url_builder
    )
  end

  should_present_keys :user_id, :seat_type, :role
end
