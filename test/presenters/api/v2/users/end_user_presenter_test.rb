require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Users::EndUserPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint
  extend Api::V2::TestHelper
  fixtures :users, :user_settings, :accounts, :organizations, :translation_locales, :organization_memberships, :photos

  before do
    @model = users(:minimum_end_user)
    @model.photo = FactoryBot.create(:photo, filename: "large_1.png")
    @model.save!

    assert @model.settings.any?
    @presenter = Api::V2::Users::EndUserPresenter.new(@model, url_builder: mock_url_builder)
  end

  should_pass_lint_tests

  it "has 'user' as the root key" do
    assert_equal :user, @presenter.model_key
  end

  should_present_keys :id, :name, :created_at, :updated_at, :time_zone, :iana_time_zone, :email,
    :phone, :shared_phone_number, :photo, :url, :locale, :locale_id, :role, :verified,
    :organization_id

  should_present_method :id
  should_present_method :created_at
  should_present_method :updated_at, value: 10.minutes.from_now
  should_present_method :time_zone,  value: 'Copenhagen'
  should_present_method :iana_time_zone
  should_present_method :email
  should_present_method :is_verified?, as: :verified
  should_present_method :role
  should_present_method :phone_number, as: :phone
  should_present_method :photo, with: Api::V2::AttachmentPresenter

  should_present_url :api_v2_end_user_url

  it "returns the correct default organization" do
    assert_equal @model.organization.id, @presenter.model_json(@model)[:organization_id]
  end

  describe "with an anonymous user" do
    before { @model = accounts(:minimum).anonymous_user }

    it "does not present url" do
      assert_nil @presenter.url(@model)
    end
  end

  it "includes photo, identities, and custom field values in #association_preloads" do
    assert_equal({
      photo: { thumbnails: {}},
      identities: {},
      account: {},
      organization_memberships: {},
      organization: {}
    }, @presenter.association_preloads)
  end

  describe "when blocklist is requested" do
    it "includes whether or not the user is blocklisted" do
      @presenter.expects(:side_load?).with(:blocklist).returns(true).once
      assert @presenter.model_json(@model).key?(:blocklisted)
    end
  end

  describe 'for a user that does not specify a locale preference' do
    before { @model.locale_id = nil }

    it 'presents default locale information' do
      json = @presenter.present(@model)[:user]
      assert_not_nil json[:locale]
      assert_not_nil json[:locale_id]
    end
  end

  describe "with account that has voice formatted phone numbers" do
    before do
      @model.account.stubs(has_voice_formatted_phone_numbers?: true)
    end

    it 'presents phone number in formatted manner' do
      @model.phone = "41443643532"
      json = @presenter.present(@model)[:user]
      assert_not_nil json[:formatted_phone]
      assert_equal json[:formatted_phone], '+41 44 364 35 32'
    end

    it 'presents empty string when phone number is empty' do
      @model.phone = ''
      json = @presenter.present(@model)[:user]
      assert_not_nil json[:formatted_phone]
      assert_equal json[:formatted_phone], ''
    end

    it 'presents original string when phone number is invalid' do
      @model.phone = "invalid_phone_number"
      json = @presenter.present(@model)[:user]
      assert_not_nil json[:formatted_phone]
      assert_equal json[:formatted_phone], 'invalid_phone_number'
    end
  end

  describe "with account that has widgets that rely on private attributes" do
    before do
      @model.account.stubs(has_private_attributes_in_end_user_presenter?: true)
    end
    should_present_method :external_id

    describe 'when the account has user and org fields' do
      before do
        @presenter.account.stubs(:has_user_and_organization_tags?).returns(true)
      end

      should_present_method :tags
    end

    describe "with the 'api_tags_disabled_for_end_user_presenter' Arturo flag enabled" do
      before do
        Arturo.enable_feature!(:api_tags_disabled_for_end_user_presenter)
      end

      it 'presents an empty array of tags regardless of the tags on the user' do
        assert_nil @presenter.model_json(@model)[:tags]
      end
    end

    describe "with the 'api_tags_disabled_for_end_user_presenter' Arturo flag disabled" do
      before do
        Arturo.disable_feature!(:api_tags_disabled_for_end_user_presenter)
      end

      it 'presents an empty array of tags regardless of the tags on the user' do
        assert_equal [], @presenter.model_json(@model)[:tags]
      end
    end
  end

  describe "with stubbed identity presenter" do
    before do
      @model.identities.each do |identity|
        @presenter.send(:identity_presenter).expects(:model_json).with(identity)
      end
    end

    should_side_load :organizations, :identities, :highlights
  end

  describe "organizations" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.expects(:side_load?).with(:organizations).returns(true)
    end

    it "does not exposed private attributes" do
      refute @presenter.present(users(:minimum_end_user))[:organizations][0].key?(:external_id)
    end
  end

  describe 'detecting n+1 queries' do
    before do
      @model = users(:serialization_user)
      OrganizationMembership.create!(
        account: @model.account,
        organization_id: organizations(:serialization_organization).id,
        default: true,
        user_id: @model.id
      )
      @one_model = Array(@model)
      User.transaction do
        @ten_models = Array.new(10) do
          m = @model.dup
          m.save!
          User.find(m.id)
        end
      end
      @presenter = ::Api::V2::Users::EndUserPresenter.new(@model,
        redacted_fields: [],
        url_builder: mock_url_builder)
    end

    # 2016.02.29 Note that there is a difference in the queries used for the two following tests (presenting 1x v. 10x).
    # The first one does a query on organization_memberships that the 10x test does not do, and now both are doing
    # queries on accounts so have differing expectations on the number of queries.
    describe "presenting the model once" do
      should_do_sql_queries(5) { @presenter.present(@one_model) }
    end

    # IMPORTANT: Presenting the same model duplicated should not require extra queries, if it does
    # it's almost certainly an N+1
    describe "presenting the model ten times" do
      should_do_sql_queries(5) { @presenter.present(@ten_models) }
    end
  end
end
