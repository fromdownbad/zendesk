require_relative "../../../../support/test_helper"
require_relative '../../../../support/entitlement_test_helper'

SingleCov.covered!

describe Api::V2::Users::DeletedUserPresenter do
  include EntitlementTestHelper

  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint
  extend Api::V2::TestHelper

  fixtures :users, :user_settings, :accounts, :organizations, :translation_locales, :organization_memberships, :photos, :tickets, :events, :role_settings

  let(:default_options) do
    {
      action: ComplianceDeletionStatus::REQUEST_DELETION,
      application: ComplianceDeletionStatus::ALL_APPLICATONS,
      executer: @admin
    }
  end

  before do
    stub_request(:post, "https://sns.us-west-2.amazonaws.com/").to_return(status: 200, body: '', headers: {})

    @admin = users(:minimum_admin)
    @model = users(:minimum_agent)
    @model.organization = organizations(:minimum_organization1)

    @model.photo = FactoryBot.create(:photo, filename: "small.png")
    @model.save!
    @model.current_user = @admin
    @model.delete! # can't ultra_delete! an active user
    @model.ultra_delete!('compliance_deletion_status' => default_options)
    @model.reload

    assert @model.settings.any?
    @presenter = Api::V2::Users::DeletedUserPresenter.new(@model, url_builder: mock_url_builder)
  end

  should_pass_lint_tests

  it "has 'user' as the root key" do
    assert_equal :deleted_user, @presenter.model_key
  end

  should_present_keys :id, :name, :created_at, :updated_at, :time_zone, :email,
    :phone, :shared_phone_number, :photo, :url, :locale, :locale_id, :role,
    :organization_id, :active

  should_present_method :id
  should_present_method :created_at
  should_present_method :updated_at, value: 10.minutes.from_now
  should_present_method :time_zone
  should_present_method :email
  should_present_method :role
  should_present_method :phone_number, as: :phone
  should_present_method :photo, with: Api::V2::AttachmentPresenter

  # should_present_url :api_v2_deleted_user_url

  it "does not present the is_staff attribute" do
    assert_nil @presenter.model_json(@model)[:is_staff]
  end

  it "returns nil for the removed organization" do
    assert_nil @presenter.model_json(@model)[:organization_id]
  end

  describe "with an non deleted user" do
    it "raise an error" do
      assert_raises RuntimeError do
        @presenter.model_json(@admin)
      end
    end
  end

  it "includes photo, identities, and custom field values in #association_preloads" do
    assert_equal({
      photo: { thumbnails: {}},
      identities: {},
      account: {},
      organization_memberships: {},
      organization: {}
    }, @presenter.association_preloads)
  end

  describe 'for a user that does not specify a locale preference' do
    before { @model.locale_id = nil }

    it 'presents default locale information' do
      json = @presenter.present(@model)[:deleted_user]
      assert_not_nil json[:locale]
      assert_not_nil json[:locale_id]
    end
  end

  describe "with account that has widgets that rely on private attributes" do
    before do
      @model.account.stubs(has_private_attributes_in_end_user_presenter?: true)
    end
    should_present_method :external_id

    describe 'when the account has user and org fields' do
      before do
        @presenter.account.stubs(:has_user_and_organization_tags?).returns(true)
      end

      should_present_method :tags
    end
  end

  describe "sideloading latest_tickets" do
    let(:ticket1) { tickets(:minimum_1) }
    let!(:comment1) { ticket1.comments.first }

    before do
      @presenter.stubs(:side_load?).with(:related_ticket_ids).returns(false)
      @presenter.stubs(:side_load?).with(:latest_tickets).returns(true)
      assert comment1.update_column(:author_id, @model.id)
    end

    it "includes latest_tickets where a comment was made" do
      assert @presenter.model_json(@model)[:latest_tickets].map { |t| t[:id] }.include?(ticket1.nice_id)
    end
  end

  describe "sideloading related_ticket_ids" do
    let(:ticket1) { tickets(:minimum_1) }
    let(:ticket2) { tickets(:minimum_2) }
    let!(:comment1) { ticket1.comments.first }

    before do
      @presenter.stubs(:side_load?).with(:related_ticket_ids).returns(true)
      @presenter.stubs(:side_load?).with(:latest_tickets).returns(true)
      ticket2.update_column(:assignee_id, @model.id)
      archive_and_delete ticket2
      assert comment1.update_column(:author_id, @model.id)
    end

    it "includes related_ticket_ids where the user was the author of the event" do
      assert @presenter.model_json(@model)[:related_ticket_ids].include?(ticket1.nice_id)
      assert @presenter.model_json(@model)[:related_ticket_ids].include?(ticket2.nice_id)
    end
  end

  describe "with stubbed identity presenter" do
    before do
      @model.identities.each do |identity|
        @presenter.send(:identity_presenter).expects(:model_json).with(identity)
      end
    end
  end
end
