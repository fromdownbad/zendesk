require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Users::SettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  describe Api::V2::Users::SettingsPresenter do
    before do
      @user      = users(:minimum_agent)
      @model     = @user
      @presenter = Api::V2::Users::SettingsPresenter.new(@user, url_builder: mock_url_builder)
    end

    should_present_keys :lotus

    describe_with_arturo_enabled :user_level_shared_view_ordering do
      should_present_keys :shared_views_order, :lotus

      it "presents the key with the value of the users setting" do
        setting = [@user.shared_views.last.id]
        @user.texts.shared_views_order = [@user.shared_views.last.id]
        @user.texts.save!

        json = @presenter.model_json(@model)
        assert_equal setting, json[:shared_views_order]
      end
    end
  end

  describe Api::V2::Users::SettingsPresenter::LotusPresenter do
    before do
      @user      = users(:minimum_agent)
      @model     = @user
      @settings  = @user.settings
      @presenter = Api::V2::Users::SettingsPresenter::LotusPresenter.new(@user, url_builder: mock_url_builder)
    end

    should_present_keys(
      :show_reporting_video_tutorial,
      :show_onboarding_tooltips,
      :show_welcome_dialog,
      :show_user_assume_tutorial,
      :keyboard_shortcuts_enabled,
      :device_notification,
      :voice_calling_number,
      :show_hc_onboarding_tooltip,
      :show_options_move_tooltip,
      :show_filter_options_tooltip,
      :show_user_nav_tooltip,
      :show_apps_tray,
      :apps_last_collapsed_states,
      :show_feature_notifications,
      :customer_list_onboarding_state,
      :ticket_action_on_save,
      :show_insights_onboarding,
      :show_color_branding_tooltip,
      :two_factor_authentication,
      :show_new_search_tooltip,
      :show_onboarding_modal,
      :show_get_started_animation,
      :show_prediction_satisfaction_dashboard,
      :show_hc_product_tray_tooltip,
      :show_google_apps_onboarding,
      :show_public_comment_warning,
      :show_multiple_ticket_forms_tutorial,
      :show_first_comment_private_tooltip,
      :revere_subscription,
      :show_enable_google_apps_modal,
      :show_zero_state_tour_ticket,
      :quick_assist_first_dismissal,
      :show_help_panel_intro_tooltip,
      :show_new_comment_filter_tooltip,
      :show_answer_bot_dashboard_onboarding,
      :show_answer_bot_trigger_onboarding,
      :show_answer_bot_lotus_onboarding,
      :show_agent_workspace_onboarding,
      :has_seen_channel_switching_onboarding,
      :show_manual_start_translation_tooltip,
      :show_manual_stop_translation_tooltip,
      :show_composer_will_translate_tooltip,
      :show_composer_wont_translate_tooltip
    )

    should_present_method :show_reporting_video_tutorial, from: :settings
    should_present_method :show_onboarding_tooltips,      from: :settings
    should_present_method :show_welcome_dialog,           from: :settings
    should_present_method :show_user_assume_tutorial,     from: :settings
    should_present_method :show_hc_onboarding_tooltip,    from: :settings
    should_present_method :show_options_move_tooltip,     from: :settings
    should_present_method :show_filter_options_tooltip,   from: :settings
    should_present_method :show_user_nav_tooltip,         from: :settings
    should_present_method :show_apps_tray,                from: :settings
    should_present_method :apps_last_collapsed_states,    from: :settings
    should_present_method :show_feature_notifications,    from: :settings
    should_present_method :keyboard_shortcuts_enabled,    from: :settings
    should_present_method :voice_calling_number,          from: :settings
    should_present_method :device_notification?,          as: :device_notification
    should_present_method :eligible_for_2fa?,             as: :two_factor_authentication
    should_present_method :show_new_search_tooltip,       from: :settings
    should_present_method :show_onboarding_modal,         from: :settings
    should_present_method :show_get_started_animation,    from: :settings
    should_present_method :show_hc_product_tray_tooltip,  from: :settings
    should_present_method :show_google_apps_onboarding,   from: :settings
    should_present_method :show_public_comment_warning,   from: :settings
    should_present_method :revere_subscription,           from: :settings
    should_present_method :show_enable_google_apps_modal, from: :settings
    should_present_method :show_zero_state_tour_ticket,   from: :settings
    should_present_method :quick_assist_first_dismissal,  from: :settings
    should_present_method :show_help_panel_intro_tooltip, from: :settings
    should_present_method :show_new_comment_filter_tooltip, from: :settings
    should_present_method :show_multiple_ticket_forms_tutorial, from: :settings
    should_present_method :show_first_comment_private_tooltip,  from: :settings
    should_present_method :show_answer_bot_lotus_onboarding, from: :settings
    should_present_method :show_agent_workspace_onboarding, from: :settings
    should_present_method :has_seen_channel_switching_onboarding, from: :settings
    should_present_method :show_manual_start_translation_tooltip, from: :settings
    should_present_method :show_manual_stop_translation_tooltip, from: :settings
    should_present_method :show_composer_will_translate_tooltip, from: :settings
    should_present_method :show_composer_wont_translate_tooltip, from: :settings

    it "returns false for show_color_branding_tooltip" do
      json = @presenter.model_json(@model)
      assert_equal false, json[:show_color_branding_tooltip]
    end
  end

  describe "As an admin user" do
    before do
      @user      = users(:minimum_admin)
      @model     = @user
      @settings  = @user.settings
      @presenter = Api::V2::Users::SettingsPresenter::LotusPresenter.new(@user, url_builder: mock_url_builder)
    end
    should_present_method :show_color_branding_tooltip?, from: :user, as: :show_color_branding_tooltip
  end
end
