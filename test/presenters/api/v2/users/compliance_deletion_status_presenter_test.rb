require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Api::V2::Users::ComplianceDeletionStatusPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint
  extend Api::V2::TestHelper
  fixtures :users, :accounts

  let(:account)      { accounts(:minimum) }
  let(:deleted_user) { users(:minimum_end_user2) }

  before do
    Zendesk::PushNotifications::Gdpr::GdprSnsPublisher.any_instance.stubs(:publish).returns(true)

    @model = FactoryBot.create(:compliance_deletion_status,
      account: account,
      user: deleted_user)

    @presenter = Api::V2::Users::ComplianceDeletionStatusPresenter.new(@model, url_builder: mock_url_builder)
  end

  it "has 'compliance_deletion_status' as the root key" do
    assert_equal :compliance_deletion_status, @presenter.model_key
  end

  should_present_keys :user_id, :executer_id, :account_subdomain, :action, :application, :created_at
end
