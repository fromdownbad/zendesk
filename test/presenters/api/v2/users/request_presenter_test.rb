require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Users::RequestPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  before do
    @model = users(:minimum_agent)
    @presenter = Api::V2::Users::RequestPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :agent, :photo, :organization_id

  should_present_method :id
  should_present_method :photo, with: Api::V2::AttachmentPresenter
  should_present_method :is_agent?, as: :agent
  should_present_method :organization_id
  should_present_method :safe_name, as: :name
end
