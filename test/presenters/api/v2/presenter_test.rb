require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 27

describe Api::V2::Presenter do
  fixtures :translation_locales

  class FakeBuilder
    def self.api_v2_account_url(_opts)
      "https://foo.com/account.json"
    end

    def self.api_v2_accounts_url(account, _)
      "https://foo.com/accounts/#{account.to_param}"
    end
  end

  extend Api::Presentation::TestHelper

  before do
    @model     = accounts(:minimum)
    @presenter = Api::V2::Presenter.new(@model, url_builder: FakeBuilder)
  end

  it "builds urls via model_key" do
    def @model.to_param
      "12345-fooo"
    end
    @presenter.stubs(model_key: "accounts")
    assert_equal "https://foo.com/accounts/12345-fooo", @presenter.url(@model)
  end

  it "builds urls via model_key with singular resource" do
    @presenter.stubs(singular_resource: true)
    @presenter.stubs(model_key: "account")
    assert_equal "https://foo.com/account.json", @presenter.url(@model)
  end

  describe "#render_dynamic_content" do
    before do
      @user = users(:minimum_agent)
    end

    describe "with DC Context" do
      describe "Rendering" do
        before do
          @presenter = Api::V2::Presenter.new(@user, url_builder: FakeBuilder)
          # This is opaque but it's actually testing that the cache is being requested for the proper locale.
          @cache = stub
          Zendesk::DynamicContent::AccountContent.expects(:cache).with(@user.account, I18n.translation_locale).returns(@cache)
        end

        it "renders" do
          Zendesk::Liquid::DcContext.expects(:render).with("{{dc.text}}", @user.account, @user, 'text/plain', I18n.translation_locale, @cache).returns("text")
          assert_equal "text", @presenter.render_dynamic_content("{{dc.text}}")
        end

        it "returns the original text when the rendered text is empty" do
          Zendesk::Liquid::DcContext.expects(:render).with("{{dc.text}}", @user.account, @user, 'text/plain', I18n.translation_locale, @cache).returns("")
          assert_equal "{{dc.text}}", @presenter.render_dynamic_content("{{dc.text}}")
        end
      end

      describe "with 'locale' option set in the presenter" do
        let(:spanish_locale) { translation_locales(:spanish) }

        before do
          @presenter = Api::V2::Presenter.new(@user, url_builder: FakeBuilder, locale: spanish_locale)
          # This is opaque but it's actually testing that the cache is being requested for the proper locale.
          @spanish_dc_cache = stub
          Zendesk::DynamicContent::AccountContent.expects(:cache).with(@user.account, spanish_locale).returns(@spanish_dc_cache)
        end

        it "uses the locale in the presenter instead of I18n.translation_locale" do
          Zendesk::Liquid::DcContext.expects(:render).with("{{dc.text}}", @user.account, @user, 'text/plain', spanish_locale, @spanish_dc_cache).returns("text")
          assert_not_equal I18n.translation_locale, spanish_locale
          assert_equal "text", @presenter.render_dynamic_content("{{dc.text}}")
        end
      end
    end

    describe "with 'ticket' option" do
      before do
        @presenter = Api::V2::Presenter.new(@user, url_builder: FakeBuilder)
      end

      it "uses the ticket context" do
        expected_options = {
          account: @user.account,
          dc_cache: {},
          custom_fields_cache: nil
        }

        Zendesk::Liquid::TicketContext.
          expects(:render).
          with("{{ticket.id}}", "This should be a ticket", @user, @user.is_agent?, I18n.translation_locale, nil, expected_options).
          returns("Some id")

        assert_equal "Some id", @presenter.render_dynamic_content("{{ticket.id}}", ticket: "This should be a ticket")
      end
    end
  end
end
