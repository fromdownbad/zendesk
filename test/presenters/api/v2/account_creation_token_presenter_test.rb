require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AccountCreationTokenPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:end_user) { users(:minimum_end_user) }
  let(:admin_user) { users(:minimum_admin) }
  let(:oauth_token) { Zendesk::OAuth::Token.create!(account_id: account.id, user_id: end_user.id, client_id: 123) }

  let(:expected_token) do
    {
      oauth_token: {
        id: oauth_token.id,
        user_id: end_user.id,
        token: oauth_token.token(true)
      }
    }
  end

  before do
    @model = oauth_token
    @presenter = Api::V2::AccountCreationTokenPresenter.new(admin_user, url_builder: mock_url_builder)
  end

  should_present_keys :token, :id, :user_id

  it "should present expected result" do
    @presenter.present(oauth_token).must_equal expected_token
  end
end
