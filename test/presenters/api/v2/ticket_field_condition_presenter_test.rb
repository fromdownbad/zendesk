require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TicketFieldConditionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @model     = FactoryBot.create(:ticket_field_condition)
    @user      = @model.account.owner
    @account   = @model.account
    @presenter = Api::V2::TicketFieldConditionPresenter.new(@user, url_builder: mock_url_builder)
  end

  should_present_keys :id, :url, :ticket_form_id, :parent_field_id, :child_field_id, :value, :user_type, :created_at, :updated_at

  should_present_method :id
  should_present_method :value
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_ticket_field_condition_url

  describe "user_type" do
    it "should return a string for the user_type" do
      assert_equal @model.user_type.to_s, @presenter.model_json(@model)[:user_type]
    end
  end
end
