require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AutomaticAnswerPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :ticket_deflections, :tickets, :ticket_deflection_articles

  let(:account) { accounts(:minimum) }

  before do
    @model = ticket_deflections(:minimum_ticket_deflection)

    @presenter = Api::V2::AutomaticAnswerPresenter.new(users(:minimum_admin), url_builder: mock_url_builder)
  end

  describe 'live tickets' do
    before do
      @presenter.send(:preload_associations, @model)
    end

    should_present_keys(
      :id,
      :brand_id,
      :enquiry,
      :answer_bot_channel,
      :solved_article_id,
      :state,
      :ticket_id,
      :user_id,
      :articles,
      :created_at,
      :updated_at
    )

    should_present_method :id
    should_present_method :created_at
    should_present_method :updated_at
  end

  describe 'presenting deflection article information' do
    it 'includes article keys' do
      @model.ticket_deflection_articles << ticket_deflection_articles(:minimum_ticket_deflection_article)
      result = @presenter.present(@model)[:automatic_answer]

      assert_equal %i[
        article_id
        clicked_at
        locale
        user_marked_unhelpful
        agent_marked_unhelpful
        solved_at
      ], result[:articles].first.keys
    end

    describe 'string formatting for answer bot channel' do
      let(:deflection_channel_id) { Zendesk::Types::ViaType.WEB_FORM }
      let(:result) { @presenter.present(@model)[:automatic_answer] }

      before do
        @model.deflection_channel_id = deflection_channel_id
        @model.save
        @model.ticket_deflection_articles << ticket_deflection_articles(:minimum_ticket_deflection_article)
      end

      describe 'when deflection channel id exists' do
        it 'formats the via type as a string' do
          assert_equal 'Web form', result[:answer_bot_channel]
        end
      end

      describe 'when deflection channel id is nil' do
        let(:deflection_channel_id) { nil }

        it 'returns a nil value' do
          assert_nil result[:answer_bot_channel]
        end
      end
    end
  end

  describe 'presenting deleted tickets' do
    let(:ticket) { @model.ticket }

    before do
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.soft_delete!

      assert_equal StatusType.DELETED, ticket.status_id
    end

    it 'includes the ticket information' do
      result = @presenter.present(@model)[:automatic_answer]

      assert_equal ticket.nice_id, result[:ticket_id]
      assert_equal ticket.brand_id, result[:brand_id]
    end
  end

  describe 'presenting archived tickets' do
    let(:ticket) { @model.ticket }

    before do
      ZendeskArchive.delete_after_archive = true
      ticket.archive!
    end

    it 'includes the ticket information' do
      result = @presenter.present(@model)[:automatic_answer]

      assert_equal ticket.nice_id, result[:ticket_id]
      assert_equal ticket.brand_id, result[:brand_id]
    end
  end
end
