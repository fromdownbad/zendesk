require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ForwardingVerificationTokenPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :forwarding_verification_tokens

  before do
    @model     = forwarding_verification_tokens(:gmail_verification_1)
    @presenter = Api::V2::ForwardingVerificationTokenPresenter.new(accounts(:minimum).owner,
      url_builder: mock_url_builder)
  end

  should_present_keys :id, :from_email, :to_email, :email_provider, :value, :created_at, :updated_at
end
