require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::WorkspaceDefinitionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:definitions) do
    Zendesk::Rules::Definitions.new(
      account:   account,
      user:      user,
      rule_type: :contextual_workspaces
    )
  end

  let(:presenter) do
    Api::V2::WorkspaceDefinitionsPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = definitions
    @presenter = presenter
  end

  should_present_keys :conditions_all, :conditions_any

  should_present_collection :conditions_all,
    with: Api::V2::Rules::ConditionDefinitionPresenter

  should_present_collection :conditions_any,
    with: Api::V2::Rules::ConditionDefinitionPresenter
end
