require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TagScorePresenter do
  fixtures :users
  extend Api::Presentation::TestHelper

  before do
    @model     = TagScore.new(tag_name: "hello", score: 80)
    @presenter = Api::V2::TagScorePresenter.new(users(:minimum_admin), url_builder: mock_url_builder)
  end

  should_present_keys :name, :count

  should_present_method :tag_name, as: :name
  should_present_method :score, as: :count
end
