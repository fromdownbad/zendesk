require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::DynamicContent::VariantPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:fbattrs) { {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: 1 } }

  before do
    @text = Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account)
    @model = @text.variants.first
    @presenter = Api::V2::DynamicContent::VariantPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :content, :locale_id, :outdated, :active, :default, :created_at, :updated_at, :url

  should_present_method :id
  should_present_method :value, as: :content
  should_present_method :translation_locale_id, as: :locale_id
  should_present_method :outdated?, as: :outdated
  should_present_method :active
  should_present_method :is_fallback?, as: :default
  should_present_method :created_at
  should_present_method :updated_at
end
