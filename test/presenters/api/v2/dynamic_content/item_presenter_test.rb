require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::DynamicContent::ItemPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:fbattrs) { {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: 1 } }
  let(:variant_presenter) { Api::V2::DynamicContent::VariantPresenter.new(@model.account.owner, url_builder: mock_url_builder) }

  before do
    @model = Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account)
    @presenter = Api::V2::DynamicContent::ItemPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :default_locale_id, :placeholder, :outdated, :created_at, :updated_at, :url, :variants

  should_present_method :id
  should_present_method :name
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :placeholder, as: :placeholder
  should_present_method :default_locale_id

  should_present_url :api_v2_dynamic_content_item_url

  it "uses Api::V2::DynamicContent::VariantPresenter" do
    @presenter.stubs(:variant_presenter).returns(variant_presenter)
    variant_presenter.expects(:present).with(@model.variants).returns(variants: [])
    assert @presenter.model_json(@model)[:variants]
  end

  describe "#outdated" do
    it "presents outdated = 'false' if there are no variants out of date" do
      @model.variants.stubs(outdated: [])
      refute @presenter.model_json(@model)[:outdated]
    end

    it "presents outdated = 'true' if there are variants out of date" do
      @model.variants.stubs(outdated: [fbattrs])
      assert @presenter.model_json(@model)[:outdated]
    end
  end
end
