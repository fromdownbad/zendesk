require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::OrganizationSubscriptionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :forums

  before do
    @model     = forums(:solutions).subscribe(users(:minimum_end_user))
    @presenter = Api::V2::OrganizationSubscriptionPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :user_id, :organization_id, :created_at, :url

  should_present_method :id
  should_present_method :user_id
  should_present_method :source_id, as: :organization_id
  should_present_method :created_at

  describe "side-loading users" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:users).returns(true)
    end

    it "uses the UserPresenter" do
      subscriptions = [stub]
      side_loaded_users = [stub]
      @presenter.expects(:side_load_association).with(subscriptions, :user, @presenter.user_presenter).returns(side_loaded_users)
      assert_equal side_loaded_users, @presenter.side_loads(subscriptions)[:users]
    end
  end

  describe "side-loading organizations" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:organizations).returns(true)
    end

    it "uses the OrganizationPresenter" do
      subscriptions = [stub]
      side_loaded_organizations = [stub]
      @presenter.expects(:side_load_association).with(subscriptions, :source, @presenter.organization_presenter).returns(side_loaded_organizations)
      assert_equal side_loaded_organizations, @presenter.side_loads(subscriptions)[:organizations]
    end
  end
end
