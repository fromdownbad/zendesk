require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Api::V2::SubscriptionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :forums, :entries

  before do
    @model = Watching.create! do |w|
      w.user    = users(:minimum_end_user)
      w.account = accounts(:minimum)
      w.source  = entries(:ponies)
    end

    @presenter = Api::V2::SubscriptionPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :user_id, :source_id, :source_type, :created_at

  should_present_method :id
  should_present_method :user_id
  should_present_method :source_id
  should_present_method :source_type
  should_present_method :created_at

  describe "side-loading forums" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:forums).returns(true)
    end

    it "uses the ForumPresenter" do
      subscriptions = [stub(source_type: 'Forum')]
      side_loaded_forums = [stub]
      @presenter.expects(:side_load_association).with(subscriptions, :source, @presenter.forum_presenter).returns(side_loaded_forums)
      assert_equal side_loaded_forums, @presenter.side_loads(subscriptions)[:forums]
    end
  end

  describe "side-loading topics" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:topics).returns(true)
    end

    it "uses the TopicPresenter" do
      t = stub(source_type: 'Entry')
      subscriptions = [t]
      t.stubs(:source).returns(t)
      side_loaded_topics = [stub]
      side_loaded_forums = [stub]
      @presenter.expects(:side_load_association).with(subscriptions, :source, @presenter.topic_presenter).returns(side_loaded_topics)
      @presenter.expects(:side_load_association).with(subscriptions, :forum, @presenter.forum_presenter).returns(side_loaded_forums)

      json = @presenter.side_loads(subscriptions)
      assert_equal(side_loaded_topics, json[:topics])
      assert_equal(side_loaded_forums, json[:forums])
    end
  end
end
