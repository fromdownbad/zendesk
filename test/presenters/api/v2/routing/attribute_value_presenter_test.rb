require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::AttributeValuePresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:conditions) { nil }

  let(:deco_attribute_value) do
    Zendesk::Deco::AttributeValue.new(
      '1',
      '2',
      'Japanese',
      '2017-08-25T10:43:48Z',
      '2017-08-28T14:51:29Z'
    )
  end

  let(:attribute_value) do
    OpenStruct.new(
      attribute_id: deco_attribute_value.attribute_id,
      id:           deco_attribute_value.id,
      name:         deco_attribute_value.name,
      created_at:   deco_attribute_value.created_at,
      updated_at:   deco_attribute_value.updated_at,
      conditions:   conditions
    )
  end

  let(:presenter) do
    Api::V2::Routing::AttributeValuePresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = attribute_value
    @presenter = presenter
  end

  should_present_keys :id,
    :name,
    :created_at,
    :updated_at,
    :url

  should_present_method :id
  should_present_method :name
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_routing_attribute_value_url, with: :id

  describe '#model_json' do
    let(:json) { presenter.model_json(attribute_value) }

    before do
      @presenter.stubs(:side_load?).returns(false)
    end

    describe 'when not side-loading conditions' do
      describe 'when the attribute value has associated conditions' do
        let(:conditions) { stub('conditions') }

        it 'does not present the `conditions` key' do
          refute json.key?(:conditions)
        end
      end
    end

    describe 'when side-loading conditions' do
      before do
        @presenter.stubs(:side_load?).with(:conditions).returns(true)
      end

      describe 'when the attribute value has associated conditions' do
        let(:conditions) { stub('conditions') }

        should_present_method :conditions,
          with: Api::V2::Routing::ConditionsPresenter
      end

      describe 'when the attribute value does not have associated conditions' do
        it 'does not present the `conditions` key' do
          refute json.key?(:conditions)
        end
      end
    end
  end
end
