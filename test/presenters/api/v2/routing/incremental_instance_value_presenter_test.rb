require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::IncrementalInstanceValuePresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:instance_value) do
    OpenStruct.new(
      id:                 '903eb204-75ad-11e8-9882-b5fd13319a96',
      attribute_value_id: '1e9cbd5c-7326-11e8-b07e-916c0a7eaea9',
      instance_id:        '1',
      time:               '2018-06-22T06:48:14Z',
      type:               'unassociate_ticket',
      type_id:            '10',
      created_at:         '2018-05-22T06:48:14Z',
      deleted_at:         '2018-07-22T06:48:14Z',
      updated_at:         '2018-06-22T06:48:14Z'
    )
  end

  let(:presenter) do
    Api::V2::Routing::IncrementalInstanceValuePresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = instance_value
    @presenter = presenter
  end

  should_present_keys :id,
    :attribute_value_id,
    :instance_id,
    :time,
    :type

  should_present_method :id
  should_present_method :attribute_value_id
  should_present_method :instance_id
  should_present_method :time
  should_present_method :type
end
