require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::AttributePresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:attribute) do
    Zendesk::Deco::Attribute.new(
      '1',
      'Language',
      '2017-08-25T10:43:48Z',
      '2017-08-28T14:51:29Z'
    )
  end

  let(:presenter) do
    Api::V2::Routing::AttributePresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = attribute
    @presenter = presenter
  end

  should_present_keys :id,
    :name,
    :created_at,
    :updated_at,
    :url

  should_present_method :id
  should_present_method :name
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_routing_attribute_url, with: :id
end
