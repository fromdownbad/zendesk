require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::AttributeDefinitionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:definitions) do
    Zendesk::Routing::Attribute::Definitions.new(account, user)
  end

  let(:presenter) do
    Api::V2::Routing::AttributeDefinitionsPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = definitions
    @presenter = presenter
  end

  should_present_keys :conditions_all,
    :conditions_any

  should_present_collection :conditions_all,
    with: Api::V2::Rules::ConditionDefinitionPresenter

  should_present_collection :conditions_any,
    with: Api::V2::Rules::ConditionDefinitionPresenter
end
