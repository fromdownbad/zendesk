require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::AgentPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }
  let(:agent)   { users(:minimum_agent) }

  let(:agents_with_value)   { {} }
  let(:includes_collection) { [] }

  let(:presenter) do
    Api::V2::Routing::AgentPresenter.new(
      user,
      url_builder:       mock_url_builder,
      agents_with_value: agents_with_value,
      includes:          includes_collection
    )
  end

  before do
    @model     = agent
    @presenter = presenter
  end

  describe '#model_json' do
    let(:json) { presenter.model_json(agent) }

    describe 'when the agent is associated with the attribute value' do
      before { agents_with_value[agent.id] = true }

      it 'sets the association status' do
        assert json[:has_attribute_value]
      end
    end

    describe 'when the agent is not associated with the attribute value' do
      before { agents_with_value.delete(agent.id) }

      it 'sets the association status' do
        refute json[:has_attribute_value]
      end
    end

    describe 'when side-loading group_membership' do
      let(:includes_collection) { %i[group_membership] }

      let(:group_ids) { agent.groups.pluck(:id) }

      before_should 'the agent belongs to groups' do
        refute group_ids.empty?
      end

      it "presents the agent's group membership" do
        assert_equal group_ids, json[:group_membership]
      end
    end
  end
end
