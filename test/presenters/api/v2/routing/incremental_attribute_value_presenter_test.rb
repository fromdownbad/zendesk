require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::IncrementalAttributeValuePresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:attribute_value) do
    OpenStruct.new(
      id:           '72e1ce0b-8f95-11e8-b808-8fcae4fccff0',
      attribute_id: '15821cba-7326-11e8-b07e-950ba849aa27',
      name:         'English',
      time:         '2018-07-25T06:01:07Z',
      type:         'create',
      created_at:   '2018-07-25T06:01:07Z',
      deleted_at:   nil,
      updated_at:   '2018-07-25T06:01:07Z'
    )
  end

  let(:presenter) do
    Api::V2::Routing::IncrementalAttributeValuePresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = attribute_value
    @presenter = presenter
  end

  should_present_keys :id,
    :attribute_id,
    :name,
    :time,
    :type

  should_present_method :id
  should_present_method :attribute_id
  should_present_method :name
  should_present_method :time
  should_present_method :type
end
