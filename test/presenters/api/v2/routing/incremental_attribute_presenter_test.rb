require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::IncrementalAttributePresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:attribute) do
    OpenStruct.new(
      id:         '15821cba-7326-11e8-b07e-950ba849aa27',
      name:       'Languagessss',
      time:       '2018-06-27T23:18:22',
      type:       'update',
      created_at: '2018-06-19T01:33:19Z',
      deleted_at:  nil,
      updated_at: '2018-06-27T23:18:22Z'
    )
  end

  let(:presenter) do
    Api::V2::Routing::IncrementalAttributePresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = attribute
    @presenter = presenter
  end

  should_present_keys :id,
    :name,
    :time,
    :type

  should_present_method :id
  should_present_method :name
  should_present_method :time
  should_present_method :type
end
