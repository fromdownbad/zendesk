require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::ConditionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:user) { users(:minimum_agent) }

  let(:presenter) do
    Api::V2::Routing::ConditionsPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = OpenStruct.new(conditions_all: [], conditions_any: [])
    @presenter = presenter
  end

  should_present_keys :all, :any

  should_present_method :conditions_all, as: :all, value: []
  should_present_method :conditions_any, as: :any, value: []

  describe '#definition_json' do
    let(:conditions_all) do
      [DefinitionItem.new('priority_id', 'less_than', [PriorityType.URGENT])]
    end

    let(:conditions_any) do
      [DefinitionItem.new('ticket_is_public', 'is', ['public'])]
    end

    it 'presents the ticket attribute name' do
      assert_equal(
        {
          all: [{subject: :priority, operator: 'less_than', value: 'urgent'}],
          any: [{subject: 'ticket_is_public', operator: 'is', value: 'public'}]
        },
        presenter.present(
          OpenStruct.new(
            conditions_all: conditions_all,
            conditions_any: conditions_any
          )
        )[:conditions]
      )
    end

    describe 'when the definition includes `current_via_id`' do
      let(:definition_item) do
        DefinitionItem.new('current_via_id', 'is', [ViaType.CLOSED_TICKET])
      end

      it 'presents the current via ID condition' do
        assert_equal(
          [{subject: 'current_via_id', operator: 'is', value: 27}],
          presenter.present(
            OpenStruct.new(
              conditions_all: [definition_item],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end

    describe 'when the definition includes `via_id`' do
      let(:definition_item) do
        DefinitionItem.new('via_id', 'is', [ViaType.CLOSED_TICKET])
      end

      it 'presents the via ID condition' do
        assert_equal(
          [{subject: 'via_id', operator: 'is', value: 27}],
          presenter.present(
            OpenStruct.new(
              conditions_all: [definition_item],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end

    describe 'when the definition has a legacy subject' do
      it 'presents the sanitized definition' do
        assert_equal(
          [
            {
              subject:  'requester_id',
              operator: 'value_previous',
              value:    'current_user'
            }
          ],
          presenter.present(
            OpenStruct.new(
              conditions_all: [
                DefinitionItem.new(
                  'requester_id#this_is_very_old',
                  'value_previous',
                  'current_user'
                )
              ],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end
  end
end
