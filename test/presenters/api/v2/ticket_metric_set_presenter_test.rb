require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TicketMetricSetPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @model = ticket_metric_sets(:minimum_set_1)
    @presenter = Api::V2::TicketMetricSetPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :ticket_id, :created_at, :url,
    :updated_at, :group_stations, :assignee_stations,
    :reopens, :replies, :reply_time_in_minutes,
    :first_resolution_time_in_minutes, :full_resolution_time_in_minutes,
    :agent_wait_time_in_minutes, :requester_wait_time_in_minutes, :on_hold_time_in_minutes,
    :assignee_updated_at, :requester_updated_at, :status_updated_at,
    :initially_assigned_at, :assigned_at, :solved_at,
    :latest_comment_added_at

  should_present_method :id
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :group_stations
  should_present_method :assignee_stations
  should_present_method :reopens
  should_present_method :replies

  # FIXME: both values are always nil

  it "presents first reply time" do
    assert_equal_with_nil @model.first_reply_time_in_minutes, @presenter.model_json(@model)[:reply_time_in_minutes][:calendar]
    assert_equal_with_nil @model.first_reply_time_in_minutes_within_business_hours, @presenter.model_json(@model)[:reply_time_in_minutes][:business]
  end

  it "presents first resolution time" do
    assert_equal_with_nil @model.first_resolution_time_in_minutes, @presenter.model_json(@model)[:first_resolution_time_in_minutes][:calendar]
    assert_equal_with_nil @model.first_resolution_time_in_minutes_within_business_hours, @presenter.model_json(@model)[:first_resolution_time_in_minutes][:business]
  end

  it "presents full resolution time" do
    assert_equal_with_nil @model.full_resolution_time_in_minutes, @presenter.model_json(@model)[:full_resolution_time_in_minutes][:calendar]
    assert_equal_with_nil @model.full_resolution_time_in_minutes_within_business_hours, @presenter.model_json(@model)[:full_resolution_time_in_minutes][:business]
  end

  it "presents agent wait time" do
    assert_equal_with_nil @model.agent_wait_time_in_minutes, @presenter.model_json(@model)[:agent_wait_time_in_minutes][:calendar]
    assert_equal_with_nil @model.agent_wait_time_in_minutes_within_business_hours, @presenter.model_json(@model)[:agent_wait_time_in_minutes][:business]
  end

  it "presents requester wait time" do
    assert_equal_with_nil @model.requester_wait_time_in_minutes, @presenter.model_json(@model)[:requester_wait_time_in_minutes][:calendar]
    assert_equal_with_nil @model.requester_wait_time_in_minutes_within_business_hours, @presenter.model_json(@model)[:requester_wait_time_in_minutes][:business]
  end

  it "presents on-hold time" do
    assert_equal_with_nil @model.on_hold_time_in_minutes, @presenter.model_json(@model)[:on_hold_time_in_minutes][:calendar]
    assert_equal_with_nil @model.on_hold_time_in_minutes_within_business_hours, @presenter.model_json(@model)[:on_hold_time_in_minutes][:business]
  end

  it "presents ticket fields" do
    assert_equal @model.ticket.nice_id, @presenter.model_json(@model)[:ticket_id]
    assert_equal @model.ticket.assignee_updated_at, @presenter.model_json(@model)[:assignee_updated_at]
    assert_equal @model.ticket.requester_updated_at, @presenter.model_json(@model)[:requester_updated_at]
    assert_equal @model.ticket.status_updated_at, @presenter.model_json(@model)[:status_updated_at]
    assert_equal @model.ticket.initially_assigned_at, @presenter.model_json(@model)[:initially_assigned_at]
    assert_equal @model.ticket.assigned_at, @presenter.model_json(@model)[:assigned_at]
    assert_equal_with_nil @model.ticket.solved_at, @presenter.model_json(@model)[:solved_at]
    assert_equal_with_nil @model.ticket.resolution_time, @presenter.model_json(@model)[:resolution_time]
    assert_equal_with_nil @model.ticket.latest_comment_added_at, @presenter.model_json(@model)[:latest_comment_added_at]
  end

  describe "scrubbed tickets" do
    before do
      @ticket = @model.ticket
      @ticket.subject = Zendesk::Scrub::SCRUBBED_SUBJECT
      @ticket.status_id = StatusType.DELETED
      @ticket.current_user = @account.owner
      @ticket.will_be_saved_by(@account.owner)
      @ticket.save!
      assert @ticket.scrubbed?
      @model.reload
    end

    it 'should present scrubbed tickets' do
      assert_equal @ticket.nice_id, @presenter.model_json(@model)[:ticket_id]
    end
  end
end
