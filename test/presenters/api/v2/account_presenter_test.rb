require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AccountPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
    Subscription.any_instance.stubs(:voice_account).returns(nil)
    Subscription.any_instance.stubs(:guide_preview).returns(nil)
    Subscription.any_instance.stubs(:suite_allowed?).returns(anything)

    @model = accounts(:minimum)
    @model.remote_authentications.jwt.create!(
      is_active: true,
      remote_login_url: 'https://example.com',
      next_token: Token.generate(48, false)
    )
    manager = Zendesk::Tickets::TicketFieldManager.new(@model)
    manager.build(
      type: 'text',
      title: I18n.t('txt.admin.magento.custom_ticket_field.order_number_field_title', locale: @model.translation_locale)
    ).save!
    @model.api_tokens.create!

    @presenter = Api::V2::AccountPresenter.new(@model.owner, url_builder: mock_url_builder)
  end

  describe "internal" do
    before do
      @presenter = Api::V2::AccountPresenter.new(
        @model.owner,
        internal: true,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :id,
      :name,
      :subdomain,
      :time_format,
      :time_zone,
      :owner_id,
      :url,
      :sandbox,
      :active,
      :multiproduct
  end

  it "has 'account' as the root key" do
    assert_equal :account, @presenter.model_key
  end

  should_present_keys :name,
    :subdomain,
    :time_format,
    :time_zone,
    :owner_id,
    :url,
    :sandbox,
    :multiproduct

  it "presents account url" do
    assert_equal @model.url, @presenter.present(@model)[:account][:url]
  end

  should_side_load :settings,
    :subscription,
    :deployments,
    :sandbox,
    :magento,
    :hc_settings,
    :voice_settings,
    :partner_settings,
    :remote_authentications,
    :zopim_subscription,
    :zopim_integration,
    :voice_subscription,
    :address,
    :users,
    :secondary_subscriptions,
    :account_id

  describe "side-loading settings" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:settings).returns(true)
    end

    it "uses the SettingsPresenter" do
      account = stub
      serialized_settings = stub

      @presenter.settings_presenter.expects(:model_json).with(account).returns(serialized_settings)

      assert_equal serialized_settings, @presenter.side_loads(account)[:settings]
    end
  end

  describe "side-loading subscription" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:subscription).returns(true)
    end

    it "uses the SubscriptionPresenter" do
      subscription = stub
      account = stub(subscription: subscription)
      serialized_sub = stub

      @presenter.subscription_presenter.expects(:model_json).with(subscription).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:subscription]
    end
  end

  describe "side-loading deployments" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:deployments).returns(true)
    end

    it "uses the DeploymentsPresenter" do
      account = stub
      serialized_sub = stub

      @presenter.deployments_presenter.expects(:model_json).with(account).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:deployments]
    end
  end

  describe "side-loading sandbox" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:sandbox).returns(true)
    end

    it "uses the SandboxPresenter" do
      account = stub
      serialized_sub = stub

      @presenter.sandbox_presenter.expects(:model_json).with(account).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:sandbox]
    end
  end

  describe "side-loading magento" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:magento).returns(true)
    end

    it "uses the MagentoAccountPresenter" do
      account = stub
      serialized_sub = stub

      @presenter.magento_presenter.expects(:model_json).with(account).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:magento]
    end
  end

  describe "side-loading partner-settings" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:partner_settings).returns(true)
    end

    it "uses the PartnerSettingsPresenter" do
      account        = stub
      serialized_sub = stub

      @presenter.partner_settings_presenter.expects(:model_json).with(account).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:partner_settings]
    end
  end

  describe "side-loading remote-authentications" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:remote_authentications).returns(true)
    end

    it "uses the RemoteAuthenticationPresenter" do
      collection     = stub
      account        = stub(remote_authentications: collection)
      serialized_sub = stub
      presenter      = stub

      @presenter.remote_authentications_presenter.expects(:collection_presenter).returns(presenter)
      presenter.expects(:model_json).with(collection).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:remote_authentications]
    end
  end

  describe "side-loading zopim-subscription" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:zopim_subscription).returns(true)
    end

    it "uses the ZopimSubscriptionPresenter" do
      account        = stub(zopim_subscription: stub)
      serialized_sub = stub

      @presenter.zopim_subscription_presenter.expects(:model_json).
        with(account.zopim_subscription).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:zopim_subscription]
    end
  end

  describe "side-loading zopim-integration" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:zopim_integration).returns(true)
    end

    it "uses the ZopimIntegrationPresenter" do
      account        = stub(zopim_integration: stub)
      serialized_sub = stub

      @presenter.zopim_integration_presenter.expects(:model_json).
        with(account.zopim_integration).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:zopim_integration]
    end
  end

  describe "side-loading voice-subscription" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:voice_subscription).returns(true)
    end

    it "uses the VoiceSubscriptionPresenter" do
      account        = stub(voice_subscription: stub)
      serialized_sub = stub

      @presenter.voice_subscription_presenter.expects(:model_json).
        with(account.voice_subscription).returns(serialized_sub)

      assert_equal serialized_sub, @presenter.side_loads(account)[:voice_subscription]
    end
  end

  describe "side-loading address" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:address).returns(true)
    end

    it "uses the AddressPresenter" do
      account         = stub(address: stub)
      serialized_stub = stub

      @presenter.address_presenter.expects(:model_json).
        with(account.address).returns(serialized_stub)

      assert_equal serialized_stub, @presenter.side_loads(account)[:address]
    end
  end

  describe "side-loading users" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:users).returns(true)
    end

    it "uses the UserPresenter" do
      account         = stub(owner: stub)
      serialized_stub = stub

      @presenter.user_presenter.expects(:model_json).
        with(account.owner).returns(serialized_stub)

      assert_equal [serialized_stub], @presenter.side_loads(account)[:users]
    end
  end

  describe "side-loading secondary subscriptions" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:secondary_subscriptions).returns(true)
    end

    it "uses the SecondarySubscriptionsPresenter" do
      subscriptions   = stub
      collection      = stub(active: stub(by_name: subscriptions))
      account         = stub(subscription_feature_addons: collection)
      serialized_stub = stub
      presenter       = stub

      @presenter.secondary_subscriptions_presenter.
        expects(:collection_presenter).returns(presenter)

      presenter.expects(:model_json).with(subscriptions).returns(serialized_stub)

      assert_equal serialized_stub, @presenter.side_loads(account)[:secondary_subscriptions]
    end
  end

  describe "including account_id" do
    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:account_id).returns(true)
    end

    it "uses the UserPresenter" do
      assert_equal @model.id, @presenter.side_loads(@model)[:account_id]
    end
  end
end
