require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::CountryPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account   = accounts(:minimum)
    @model     = Country.first
    @presenter = Api::V2::CountryPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :region, :code, :calling_code, :url

  should_present_method :id
  should_present_method :name, value: "Afghanistan"
  should_present_method :region
  should_present_method :code
  should_present_method :calling_code
  should_present_url :api_v2_country_url

  describe "for brazilian portuguese users" do
    before do
      @presenter = Api::V2::CountryPresenter.new(@account.owner, url_builder: mock_url_builder)
      I18n.stubs(:locale).returns(:"pt-BR-x-19")
    end
    should_present_method :name, value: "Afeganistão"
  end
end
