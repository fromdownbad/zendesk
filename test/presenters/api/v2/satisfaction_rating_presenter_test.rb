require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SatisfactionRatingPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :events, :users, :tickets, :groups

  let(:filters) { {} }

  before do
    @account = accounts(:minimum)
    @agent   = users(:minimum_agent)
    @user    = users(:minimum_end_user)
    @group   = groups(:minimum_group)
    @ticket  = tickets(:minimum_2)
    @reason  = FactoryBot.create(:satisfaction_reason, account: @account)

    @model = @account.satisfaction_ratings.create!(
      agent: @agent,
      group: @group,
      ticket: @ticket,
      enduser: @user,
      event: events(:create_comment_for_minimum_ticket_1),
      score: SatisfactionType.BAD,
      reason_code: @reason.reason_code
    )

    @presenter = Api::V2::SatisfactionRatingPresenter.new(@account.owner, url_builder: mock_url_builder, filters: filters)
  end

  should_present_keys :id, :url, :assignee_id, :group_id, :ticket_id, :requester_id, :score, :created_at, :updated_at, :comment

  should_present_method :id
  should_present_method :agent_user_id, as: :assignee_id
  should_present_method :agent_group_id, as: :group_id
  should_present_method :enduser_id, as: :requester_id
  should_present_method :created_at
  should_present_method :updated_at

  should_present "bad", as: :score
  should_present_url :api_v2_satisfaction_rating_url

  it "presents ticket nice_id as ticket_id" do
    assert_equal @ticket.nice_id, @presenter.model_json(@model)[:ticket_id]
  end

  describe "side-loading users" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:users).returns(true)
    end

    it "uses the UserPresenter" do
      users = [stub(id: 1), stub(id: 2), stub(id: 3), stub(id: 4)]
      csats = [
        stub(enduser_id: 1, agent_user_id: nil),
        stub(enduser_id: 1, agent_user_id: 2),
        stub(enduser_id: 1, agent_user_id: 3),
        stub(enduser_id: 1, agent_user_id: 4)
      ]

      relation = stub(to_a: users)
      @presenter.account.all_users.expects(:where).with(id: [1, 2, 3, 4]).returns(relation)

      serialized_users = []

      users.each do |user|
        user_stub = stub
        serialized_users << user_stub
        @presenter.user_presenter.expects(:model_json).with(user).returns(user_stub)
      end

      assert_equal serialized_users, @presenter.side_loads(csats)[:users]
    end
  end

  describe "side-loading statistics" do
    let(:stats_presenter) { mock }

    before do
      @presenter.stubs(:side_load?).returns(false)

      Api::V2::SatisfactionRatingStatisticsPresenter.stubs(:new).returns(stats_presenter)
    end

    describe "when request includes 'statistics'" do
      before do
        @presenter.stubs(:side_load?).with(:statistics).returns(true)
      end

      it "uses the SatisfactionRatingStatisticsPresenter" do
        stats_presenter.expects(:model_json).with(@account)
        @presenter.side_loads([])
      end

      describe "with filters" do
        let(:filters) { { start_time: 2.days.ago, end_time: 1.day.ago } }

        it "pass filter through" do
          Api::V2::SatisfactionRatingStatisticsPresenter.expects(:new).with(filters).returns(stats_presenter)
          stats_presenter.stubs(:model_json)
          @presenter.side_loads([])
        end
      end
    end

    describe "when request does not include 'statistics'" do
      it "uses the SatisfactionRatingStatisticsPresenter" do
        stats_presenter.expects(:model_json).never
        @presenter.side_loads([])
      end
    end
  end

  describe "#present_for_ticket" do
    let(:presenter) do
      Api::V2::SatisfactionRatingPresenter.new(
        @account.owner,
        url_builder: mock_url_builder,
        filters:     filters
      )
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      let(:satisfaction_score) { SatisfactionType.GOOD }

      before do
        @ticket.stubs(satisfaction_score: satisfaction_score)
      end

      it "includes the score" do
        assert_equal 'good', presenter.present_for_ticket(@ticket)[:score]
      end

      it "includes the translated_value for the satisfaction_rating" do
        assert_equal(
          @reason.translated_value,
          presenter.present_for_ticket(@ticket)[:reason]
        )
      end

      it "includes the id for the satisfaction_rating's reason" do
        assert_equal(
          @reason.id,
          presenter.present_for_ticket(@ticket)[:reason_id]
        )
      end

      describe 'and the ticket does not have a satisfaction score' do
        let(:satisfaction_score) { nil }

        it 'does not blow up' do
          assert_nil presenter.present_for_ticket(@ticket)[:id]
        end
      end

      describe "when there is a satisfaction comment" do
        before do
          @ticket.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
          @ticket.satisfaction_comment = "Awesome!"
          @ticket.will_be_saved_by(@agent)
          @ticket.save!

          @model = @ticket.satisfaction_rating
        end

        describe "when comment_event_id is present" do
          before do
            assert_not_nil @model.comment_event_id
          end

          describe_with_and_without_arturo_enabled :satisfaction_rating_disable_comment_fallback do
            it "includes the comment" do
              assert_equal 'Awesome!', presenter.present_for_ticket(@ticket)[:comment]
            end
          end
        end

        describe "when comment_event_id is missing" do
          before do
            @model.update_column(:comment_event_id, nil)
            assert_nil @model.comment_event_id
          end

          describe_with_arturo_disabled :satisfaction_rating_disable_comment_fallback do
            it "includes the comment" do
              assert_equal 'Awesome!', presenter.present_for_ticket(@ticket)[:comment]
            end
          end

          describe_with_arturo_enabled :satisfaction_rating_disable_comment_fallback do
            it "does not include the comment" do
              assert_nil presenter.present_for_ticket(@ticket)[:comment]
            end
          end
        end
      end
    end

    describe_with_arturo_setting_disabled :customer_satisfaction do
      it "returns nil" do
        assert_nil presenter.present_for_ticket(@ticket)
      end
    end
  end

  describe "when csat reason code is enabled" do
    before do
      Account.any_instance.stubs(:csat_reason_code_enabled?).returns(true)
      @presenter = Api::V2::SatisfactionRatingPresenter.new(@account.owner, url_builder: mock_url_builder, filters: filters)
    end

    should_present "Poor performance", as: :reason

    it "should present reason_id value" do
      json = @presenter.model_json(@model)
      assert_equal @reason.id, json[:reason_id]
    end
  end

  describe "when csat reason code is disabled" do
    before do
      Account.any_instance.stubs(:csat_reason_code_enabled?).returns(false)
      @presenter = Api::V2::SatisfactionRatingPresenter.new(@account.owner, url_builder: mock_url_builder, filters: filters)
      @json = @presenter.model_json(@model.reload)
    end

    it "doesn't present reason" do
      refute @json.key?(:reason)
      refute @json.key?(:reason_id)
    end
  end
end
