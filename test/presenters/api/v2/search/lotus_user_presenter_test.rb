require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::LotusUserPresenter do
  fixtures :all
  extend Api::Presentation::TestHelper

  before do
    @model = users(:serialization_user)
    @org = OrganizationMembership.create!(account: @model.account, organization_id: organizations(:serialization_organization).id, default: true, user_id: @model.id)
    @presenter = Api::V2::Search::LotusUserPresenter.new(@model, url_builder: mock_url_builder)
  end

  should_present_keys :created_at, :email, :id, :name, :organization_id, :photo, :role, :updated_at, :url

  it "returns the correct default organization" do
    assert_equal @org.organization_id, @presenter.model_json(@model)[:organization_id]
  end
end
