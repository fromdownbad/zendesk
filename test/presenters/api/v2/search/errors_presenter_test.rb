require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::ErrorsPresenter do
  fixtures :users, :accounts

  describe "#present" do
    before do
      @presenter = Api::V2::Search::ErrorsPresenter.new
      @query     = Zendesk::Search::Query.new('hi')
      error      = Zendesk::Search::Error.new(Timeout::Error.new)
      @query.stubs(:error).returns(error)
    end

    it "provides the error" do
      presentation = @presenter.present(@query)
      expected = {
        error: :unavailable,
        description: "Sorry, we could not complete your search query. Please try again in a moment."
      }

      assert_equal expected, presentation
    end
  end
end
