require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::LotusTicketPresenter do
  fixtures :all
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @model = tickets(:minimum_2)
    @presenter = Api::V2::Search::LotusTicketPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :created_at, :updated_at, :type, :subject, :description, :status, :requester_id, :assignee_id, :organization_id, :group_id, :url
end
