require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::ExportPresenter do
  extend Api::Presentation::TestHelper
  fixtures :tickets

  describe "#item_presenter" do
    before do
      @user = users(:minimum_agent)
      @presenter = Api::V2::Search::ExportPresenter.new(
        @user,
        url_builder: mock_url_builder,
        mixed_collection_includes: {
          ::Ticket => [:users]
        }
      )
    end

    should_return Api::V2::Tickets::MobileTicketPresenter, for: ::Ticket
    should_return Api::V2::Users::AgentPresenter, for: ::User
    should_return Api::V2::Organizations::AgentPresenter, for: ::Organization

    describe "with two ticket records and has_more=true" do
      before do
        @ticket1 = tickets(:minimum_1)
        @ticket2 = tickets(:minimum_2)
        results = [@ticket1, @ticket2]
        results.stubs(facets: nil)
        meta = { has_more: true, after_cursor: "test_cursor" }
        @cbp_results = Zendesk::Search::CBPResults.new(results, meta)
      end

      it "presents the records and CBP information" do
        presentation = @presenter.present(@cbp_results)

        assert_equal 2, presentation[:results].size
        assert_equal @ticket1.subject, presentation[:results].first[:subject]
        assert_equal @ticket2.subject, presentation[:results].second[:subject]

        assert_equal "test_cursor", presentation[:meta][:after_cursor]
        assert presentation[:meta][:has_more]
        assert_nil presentation[:meta][:before_cursor]

        assert_nil presentation[:facets]

        assert_equal 2, presentation[:links].size
        assert_nil presentation[:links][:prev]
        assert_not_nil presentation[:links][:next]
      end
    end
  end
end
