require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::LotusResultsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  describe "#item_presenter" do
    before do
      @presenter = Api::V2::Search::LotusResultsPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
    end

    should_return Api::V2::Search::LotusTicketPresenter, for: ::Ticket
    should_return Api::V2::Search::LotusUserPresenter, for: ::User
    should_return Api::V2::GroupPresenter, for: ::Group
    should_return Api::V2::Organizations::AgentPresenter, for: ::Organization
    should_return Api::V2::TopicPresenter, for: ::Entry
    should_return Api::V2::Search::HashPresenter, for: Hash

    describe "sideloads" do
      before do
        o = organizations(:minimum_organization1)
        o.group = groups(:minimum_group)
        o.save!
      end

      let(:results) do
        results = [tickets(:minimum_1), users(:minimum_end_user), organizations(:minimum_organization1), groups(:minimum_group), entries(:sticky)]
        results.stubs(facets: 'val')
        results
      end

      subject { @presenter.present(results) }

      it 'loads ticket requesters into users' do
        assert_equal 1, subject[:users].size
      end

      it 'loads users organizations' do
        assert_equal 1, subject[:organizations].size
      end

      it 'loads an organizations group' do
        assert_equal 1, subject[:groups].size
      end

      it 'loads an entries forum' do
        assert_equal 1, subject[:forums].size
      end
    end

    describe "sideload group and organization for ticket result" do
      let(:results) do
        results = [tickets(:minimum_2)]
        results.stubs(facets: 'val')
        results
      end

      subject { @presenter.present(results) }

      it 'loads ticket requester' do
        assert_equal 1, subject[:users].size
        assert_equal users(:minimum_end_user).id, subject[:users][0][:id]
      end

      it 'loads ticket organization' do
        assert_equal 1, subject[:organizations].size
        assert_equal organizations(:minimum_organization1).id, subject[:organizations][0][:id]
      end

      it 'loads ticket group' do
        assert_equal 1, subject[:groups].size
        assert_equal groups(:minimum_group).id, subject[:groups][0][:id]
      end
    end

    describe "sideload ticket commenters from highlights" do
      before do
        @highlight_presenter = Api::V2::Search::LotusResultsPresenter.new(
          users(:minimum_agent),
          url_builder: mock_url_builder,
          includes: [:highlights],
          highlights:             { results:               [
            { 'comments' =>
              [
                { 'commenter_id' => users(:minimum_search_user).id },
                { 'commenter_id' => users(:minimum_end_user2).id }
              ]},
            { 'comments' =>
              [
                { 'commenter_id' => users(:minimum_end_user2).id },
                { 'commenter_id' => users(:minimum_agent).id },
              ]}
          ]}
        )
      end

      let(:results) do
        results = [tickets(:minimum_1)]
        results.stubs(facets: 'val')
        results
      end

      subject { @highlight_presenter.present(results) }

      it 'sideloads ticket requester and commenters' do
        assert_equal 4, subject[:users].size
        user_names = subject[:users].map { |user| user[:name] }
        assert((user_names.include? "Author Minimum"), "requester not loaded")
        assert((user_names.include? "minimum_search_user"), "search_user commenter not loaded")
        assert((user_names.include? "minimum_end_user2"), "minimum_end_user2 commenter not loaded")
        assert((user_names.include? "Agent Minimum"), "agent commenter not loaded")
      end
    end

    describe "facets" do
      let(:results) { [].tap { |result| result.stubs(facets: "my value") } }
      subject { @presenter.present(results) }

      it "sides load facets" do
        assert_equal "my value", subject[:facets]
      end
    end
  end
end
