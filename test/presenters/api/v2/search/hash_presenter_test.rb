require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::HashPresenter do
  extend Api::Presentation::TestHelper

  class TestPresenterWithResultType < Api::V2::CollectionPresenter
    include Api::Presentation::MixedCollection
    self.include_result_type = true

    def self.presenters
      { Hash => ::Api::V2::Search::HashPresenter }
    end

    def model_key
      :results
    end
  end

  class TestPresenterWithoutResultType < TestPresenterWithResultType
    self.include_result_type = false
  end

  describe "#model_json" do
    describe "for a presenter with include_result_type set to true" do
      let(:presenter) { TestPresenterWithResultType.new(User.new, url_builder: mock_url_builder) }

      it "presents inside the collection without duplicating result_type" do
        expected = { count: 1, next_page: nil, previous_page: nil, results: [{ "XXX" => 1, :result_type => "x" }] }
        assert_equal(expected, presenter.present([{ "result_type" => "x", "XXX" => 1 }]))
      end
    end

    describe "For a presenter with include_result_type set to false" do
      let(:presenter) { TestPresenterWithoutResultType.new(User.new, url_builder: mock_url_builder) }

      it "presents inside the collection without duplicating result_type" do
        expected = { count: 1, next_page: nil, previous_page: nil, results: [{ "XXX" => 1, :result_type => "x" }] }
        assert_equal(expected, presenter.present([{ "result_type" => "x", "XXX" => 1 }]))
      end

      it "does not add nil result_type" do
        expected = { count: 1, next_page: nil, previous_page: nil, results: [{ "XXX" => 1 }] }
        assert_equal(expected, presenter.present([{ "XXX" => 1 }]))
      end
    end
  end
end
