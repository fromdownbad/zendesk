require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::ResultsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users, :accounts, :tickets, :groups

  describe "#item_presenter" do
    before do
      @user = users(:minimum_agent)
      @presenter = Api::V2::Search::ResultsPresenter.new(
        @user,
        url_builder: mock_url_builder,
        mixed_collection_includes: {
          ::Ticket => [:users],
          ::Group => [:users]
        }
      )
    end

    should_return Api::V2::Tickets::MobileTicketPresenter, for: ::Ticket
    should_return Api::V2::Users::AgentPresenter, for: ::User
    should_return Api::V2::GroupPresenter, for: ::Group
    should_return Api::V2::Organizations::AgentPresenter, for: ::Organization
    should_return Api::V2::TopicPresenter, for: ::Entry
    should_return Api::V2::Search::HashPresenter, for: Hash

    describe "facets" do
      let(:results) { [].tap { |result| result.stubs(facets: "my value") } }
      subject { @presenter.present(results) }

      it "sides load facets" do
        assert_equal "my value", subject[:facets]
      end
    end

    describe "with hash" do
      it "presents result_type" do
        result = [{"result_type" => "xxx", "XXX" => 1}]
        result.stubs(facets: nil)
        expected = {count: 1, facets: nil, next_page: nil, previous_page: nil, results: [{"XXX" => 1, :result_type => "xxx"}]}
        assert_equal expected, @presenter.present(result)
      end
    end

    describe "with regular collection and sideloads" do
      before do
        ticket1 = tickets(:minimum_1)
        ticket2 = tickets(:minimum_2)
        group = groups(:minimum_group)
        @results = [ticket1, ticket2, group]
        @results.stubs(facets: nil)
      end

      it "presents users sideloads" do
        assert @presenter.present(@results).key?(:users)
      end

      it "does not do n + 1 queries" do
        assert_n_plus_ones 9 do
          @presenter.present(@results)
        end
      end

      describe "voice comments on tickets" do
        it "uses GenericCommentPresenter to render comments when passed in options[:comment_presenter]" do
          generic_comment_presenter = Api::V2::Tickets::GenericCommentPresenter.new(@user, url_builder: mock_url_builder)
          presenter = Api::V2::Search::ResultsPresenter.new(
            users(:minimum_agent),
            url_builder: mock_url_builder,
            mixed_collection_includes: {
              ::Ticket => [:users, :first_comment, :last_comment]
            },
            comment_presenter: generic_comment_presenter
          )

          Api::V2::Tickets::GenericCommentPresenter.any_instance.expects(:model_json).with(anything).at_least_once
          presenter.present(@results)
        end
      end
    end
  end
end
