require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Search::CountPresenter do
  fixtures :users, :accounts

  describe "#present" do
    before do
      @presenter = Api::V2::Search::CountPresenter.new
      @result = Zendesk::Search::Results.new(1, 0, 6)
    end

    it "count" do
      presentation = @presenter.present(@result)
      expected = {
          count: 6
      }
      assert_equal expected, presentation
    end
  end
end
