require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ForumPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :forums

  before do
    @model     = forums(:announcements)
    @presenter = Api::V2::ForumPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :description, :category_id, :organization_id, :locale_id, :locked, :position, :forum_type, :access, :created_at, :updated_at, :url, :tags

  should_present_method :id
  should_present_method :name
  should_present_method :description
  should_present_method :category_id
  should_present_method :organization_id
  should_present_method :translation_locale_id, as: :locale_id
  should_present_method :is_locked, as: :locked
  should_present_method :position
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_forum_url, with: :id

  should_present "articles", as: :forum_type
  should_present "everybody", as: :access

  describe "with a questions forum" do
    before do
      @model.display_type_id = ForumDisplayType::QUESTIONS.id
      @model_json = @presenter.model_json(@model)
    end

    it "includes number of unanswered questions" do
      assert_not_nil @model_json[:unanswered_topics]
    end

    it "sets unanswered questions properly" do
      assert_equal @model.unanswered_questions.count(:all), @model_json[:unanswered_topics]
    end
  end
end
