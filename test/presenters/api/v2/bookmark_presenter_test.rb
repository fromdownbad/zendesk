require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::BookmarkPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :tickets

  before do
    @account   = accounts(:minimum)
    @model     = Ticket::Bookmark.create!(user: @account.owner, ticket: @account.tickets.working.last)
    @presenter = Api::V2::BookmarkPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :created_at, :ticket, :url

  should_present_method :id
  should_present_method :created_at
  should_present_method :ticket, with: Api::V2::Tickets::TicketPresenter

  should_present_url :api_v2_bookmark_url

  it "has association_preloads" do
    assert_equal({ticket: @presenter.ticket_presenter.association_preloads}, @presenter.association_preloads)
  end

  describe "with permission to sideload everything" do
    describe_with_arturo_setting_enabled :customer_satisfaction do
      before do
        @expected_side_loads = [
          :users,
          :organizations,
          :groups,
          :last_audits,
          :sharing_agreements,
          :followups,
          :satisfaction_ratings,
          :brands
        ]
      end

      it "side loads all expected types" do
        @presenter.stubs(:includes).returns([])
        non_side_load_keys = @presenter.as_json(@model).keys

        @presenter.stubs(:includes).returns(@expected_side_loads)
        @presenter.instance_variable_set(:@ticket_presenter, nil)
        actual_side_loads = @presenter.as_json(@model).keys - non_side_load_keys

        missing_side_loads = @expected_side_loads - actual_side_loads
        extra_side_loads   = actual_side_loads - @expected_side_loads
        messages = []

        messages << "is missing these side-loads: #{missing_side_loads.join(', ')}" if missing_side_loads.any?
        messages << "has these extra side-loads: #{extra_side_loads.join(', ')}" if extra_side_loads.any?

        flunk("#{@presenter.class.name} " + messages.join(' and ')) if messages.any?
      end
    end
  end

  describe "side-loading users" do
    before do
      @presenter.stubs(:includes).returns([:users])
    end

    it "side loads users referenced by tickets" do
      users   = [stub(id: 1), stub(id: 2), stub(id: 3), stub(id: 4)]
      tickets = [
        stub(id: 1, requester_id: 1, assignee_id: nil, submitter_id: nil, collaborations: [], archived?: false),
        stub(id: 2, requester_id: 1, assignee_id: 2,   submitter_id: nil, collaborations: [], archived?: false),
        stub(id: 3, requester_id: 1, assignee_id: 2,   submitter_id: 3,   collaborations: [], archived?: false),
        stub(id: 4, requester_id: 1, assignee_id: 2,   submitter_id: 3,   collaborations: [stub(user_id: 4)], archived?: false)
      ]
      bookmarks = tickets.map { |ticket| stub(ticket: ticket) }

      relation = stub(to_a: users)
      @presenter.account.all_users.expects(:where).with(id: [1, 2, 3, 4]).returns(relation)

      serialized_users = []

      users.each do |user|
        user_stub = stub
        serialized_users << user_stub
        @presenter.user_presenter.expects(:model_json).with(user).returns(user_stub)
      end

      assert_equal serialized_users, @presenter.side_loads(bookmarks)[:users]
    end
  end
end
