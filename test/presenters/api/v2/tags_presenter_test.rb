require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Api::V2::TagScorePresenter do
  extend Api::Presentation::TestHelper

  fixtures :tickets, :users

  let(:model) { stub(tag_array: %w[hello]) }
  let(:presenter) { Api::V2::TagsPresenter.new(users(:minimum_agent), url_builder: mock_url_builder) }

  subject { presenter.present(model) }

  it "presents tag_array" do
    assert_equal({ tags: %w[hello] }, subject)
  end

  describe 'when presenting tickets' do
    let(:model) { tickets(:minimum_1) }

    it 'uses current_tags' do
      tags = 'hello there friend'

      model.expects(:current_tags).once.returns(tags)
      assert_equal({ tags: tags.split }, subject)
    end
  end
end
