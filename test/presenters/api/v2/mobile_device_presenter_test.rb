require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::MobileDevicePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :mobile_apps

  before do
    @account   = accounts(:minimum)
    @user      = @account.owner
    PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register)
    @model = PushNotifications::FirstPartyDeviceIdentifier.create!(
      user: @user,
      token: "HELLO",
      device_type: "iPhone",
      mobile_app: mobile_apps(:com_zendesk_agent)
    )
    @presenter = Api::V2::MobileDevicePresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :device_type, :token, :created_at, :mobile_app, :updated_at, :url

  should_present_method :id
  should_present_method :device_type
  should_present_method :token
  should_present_method :mobile_app_identifier, as: :mobile_app
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_mobile_device_url
end
