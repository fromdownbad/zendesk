require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AdminBrandPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @model = FactoryBot.create(:brand)
    @presenter = Api::V2::AdminBrandPresenter.new(users(:minimum_admin), url_builder: mock_url_builder)
  end

  should_present_keys(
    :id,
    :name,
    :active,
    :brand_url,
    :has_help_center,
    :help_center_state,
    :default,
    :is_deleted,
    :subdomain,
    :host_mapping,
    :logo,
    :url,
    :ticket_form_ids,
    :signature_template,
    :created_at,
    :updated_at
  )

  should_present_method :id
  should_present_method :name
  should_present_method :url, as: :brand_url
  should_present_method :active
  should_present_method :default?, as: :default
  should_present_method :deleted?, as: :is_deleted
  should_present_method :subdomain
  should_present_method :host_mapping
  should_present_method :logo, with: Api::V2::AttachmentPresenter
  should_present_method :signature_template
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_brand_url

  describe "with includes" do
    before do
      @presenter = Api::V2::AdminBrandPresenter.new(users(:minimum_admin), url_builder: mock_url_builder, includes: [:ticket_forms])
    end

    should_side_load :ticket_forms
  end
end
