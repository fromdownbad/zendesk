require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::GooddataIntegrationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users, :accounts

  before do
    account = accounts(:minimum)
    project = stub(id: 'project_id', dashboards: [])
    GoodData::Project.stubs(:create).returns(project)
    GoodData::Project.stubs(:find).returns(project)
    @model = account.build_gooddata_integration(admin_id: 'bob@foo.com', scheduled_at: '11pm')
    @presenter = Api::V2::GooddataIntegrationPresenter.new(users(:minimum_admin), url_builder: mock_url_builder)
  end

  should_present_keys :id, :project_id, :admin_id, :status, :scheduled_at, :version, :dashboards, :url

  should_present_method :id
  should_present_method :project_id
  should_present_method :admin_id
  should_present_method :status
  should_present_method :scheduled_at
  should_present_method :version
  should_present_method :dashboards

  should_present_url :api_v2_gooddata_integration_url
end
