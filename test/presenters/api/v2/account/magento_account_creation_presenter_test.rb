require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::MagentoAccountCreationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings

  before do
    @account = accounts(:minimum)
    @account.remote_authentications.jwt.create!(
      is_active: true,
      remote_login_url: 'https://example.com',
      next_token: Token.generate(48, false)
    )
    manager = Zendesk::Tickets::TicketFieldManager.new(@account)
    manager.build(
      type: 'text',
      title: I18n.t('txt.admin.magento.custom_ticket_field.order_number_field_title', locale: @account.translation_locale)
    ).save!
    @model = @account
    @presenter = Api::V2::Account::MagentoAccountCreationPresenter.new(@account.owner, url_builder: mock_url_builder)
  end
  should_present_keys :api_token, :remote_authentication_token, :magento_order_number_ticket_field_id
end
