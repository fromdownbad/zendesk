require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::SubscriptionVoicePresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :subscriptions

  before do
    @account = accounts(:minimum)
    @model = subscriptions(:minimum)
    @presenter = Api::V2::Account::SubscriptionVoicePresenter.new(@account.owner, url_builder: mock_url_builder)

    @phone_number1 = FactoryBot.create(:incoming_phone_number, account: @account, number: '+13456789087')
    @phone_number2 = FactoryBot.create(:incoming_phone_number, account: @account, number: '+13456789088')
  end

  should_present_keys :phone_numbers

  should_present_method :phone_numbers, value: ["+13456789087", "+13456789088"]
end
