require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Account::ZopimSubscriptionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:zopim_plan_type) { OpenStruct.new }

  let(:administrator) do
    OpenStruct.new.tap do |m|
      m.user = OpenStruct.new
    end
  end

  let(:model) do
    OpenStruct.new.tap do |m|
      m.created_at      = Date.today
      m.zopim_plan_type = zopim_plan_type
      m.administrators  = [administrator]
    end
  end

  before do
    @model     = model
    @account   = accounts(:minimum)
    @presenter = Api::V2::Account::ZopimSubscriptionPresenter.new(
      @account.owner, url_builder: mock_url_builder
    )
  end

  should_present_keys :zopim_account_id,
    :plan_type,
    :plan_name,
    :max_agents,
    :status,
    :is_serviceable,
    :trial_expires_on,
    :expired_trial,
    :created_at,
    :trial_days,
    :trial_ended_at,
    :trial_extension_eligible,
    :final_trial_expire_date,
    :days_left_in_trial,
    :last_synchronized_at,
    :syncstate,
    :purchased_at,
    :zopim_reseller_id,
    :chat_limits_enabled,
    :account_chat_limit

  should_side_load :zopim_integration,
    :administrators,
    :app_market_integration
end
