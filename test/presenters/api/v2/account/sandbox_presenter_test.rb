require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::SandboxPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings

  before do
    @account = accounts(:minimum)
    @model = @account
    @presenter = Api::V2::Account::SandboxPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  describe 'account with sandbox' do
    before do
      @account.stubs(:sandbox).returns(stub(name: 'Foo Sandbox', subdomain: 'foo1234', url: 'https://foo1234.zendesk.com'))
    end
    should_present_keys :url, :name, :subdomain

    it "should show url of sandbox" do
      assert_equal 'https://foo1234.zendesk.com', @presenter.present(@model)[:sandbox][:url]
    end
  end

  describe 'account without a sandbox' do
    it 'returns nothing' do
      assert_nil @presenter.present(@model)[:sandbox]
    end
  end
end
