require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Account::SecondarySubscriptionsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  let(:temporary_zendesk_agents) do
    {
      zuora_rate_plan_id: '123456',
      name:               'temporary_zendesk_agents',
      quantity:           5,
      starts_at:          2.days.ago,
      expires_at:         1.month.from_now,
      status_id:          SubscriptionFeatureAddon::STATUS[:started]
    }
  end

  before do
    @addon = account.
      subscription_feature_addons.
      create(temporary_zendesk_agents)

    @model = account.
      subscription_feature_addons.
      by_name(:temporary_zendesk_agents).
      first

    @presenter = Api::V2::Account::SecondarySubscriptionsPresenter.new(
      account.owner,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :zuora_rate_plan_id,
    :product,
    :quantity,
    :starts_at,
    :expires_at,
    :status

  should_present_method :zuora_rate_plan_id,  as: :zuora_rate_plan_id
  should_present_method :name,                as: :product
  should_present_method :quantity,            as: :quantity
  should_present_method :starts_at,           as: :starts_at
  should_present_method :expires_at,          as: :expires_at
  should_present_method :status,              as: :status

  describe '#status' do
    it 'should present the status key if present' do
      assert_equal 'started', @presenter.model_json(@model)[:status]
    end

    describe 'set to nil' do
      before do
        @addon.update_column(:status_id, nil)

        @model = account.
          subscription_feature_addons.
          by_name(:temporary_zendesk_agents).
          first
      end

      it 'should present an empty status' do
        assert_nil @presenter.model_json(@model)[:status]
      end
    end
  end
end
