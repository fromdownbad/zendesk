require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Account::SettingsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :account_settings,
    :subscriptions,
    :translation_locales

  describe Api::V2::Account::SettingsPresenter do
    before do
      Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
      Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :active_features,
      :agents,
      :api,
      :apps,
      :billing,
      :branding,
      :brands,
      :cdn,
      :chat,
      :cross_sell,
      :gooddata_advanced_analytics,
      :google_apps,
      :groups,
      :limits,
      :lotus,
      :metrics,
      :onboarding,
      :rule,
      :screencast,
      :statistics,
      :ticket_form,
      :ticket_sharing_partners,
      :tickets,
      :twitter,
      :user,
      :voice,
      :localization

    describe 'when automatic answers is available' do
      before do
        @account.stubs(:has_automatic_answers_enabled?).returns(true)
      end

      it 'should include the automatic_answers settings' do
        assert @presenter.model_json(@account)[:automatic_answers].present?
      end

      it 'should default automatic answers threshold level to balanced' do
        assert_equal(
          "balanced",
          @presenter.model_json(@account)[:automatic_answers][:threshold]
        )
      end
    end

    describe "when `email_simplified_threading_onboarding` is enabled" do
      before { Arturo.enable_feature!(:email_simplified_threading_onboarding) }

      it "presents the `simplified_email_threading` setting" do
        refute @presenter.present(@account).dig(:settings, :email, :simplified_email_threading).nil?
      end
    end

    describe "when `email_simplified_threading_onboarding` is disabled" do
      before { Arturo.disable_feature!(:email_simplified_threading_onboarding) }

      it "does not present the `simplified_email_threading` setting" do
        assert @presenter.present(@account).dig(:settings, :email, :simplified_email_threading).nil?
      end
    end
  end

  describe Api::V2::Account::SettingsPresenter::GroupsPresenter do
    before do
      @account     = accounts(:minimum)
      @model       = @account
      @settings    = @account.settings
      @presenter = Api::V2::Account::SettingsPresenter::GroupsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :check_group_name_uniqueness

    should_present_method :check_group_name_uniqueness, from: :settings
  end

  describe Api::V2::Account::SettingsPresenter::BrandingPresenter do
    before do
      @account     = accounts(:minimum)
      @model       = @account
      @branding    = @model.branding
      @header_logo = @model.header_logo
      @favicon     = @model.favicon

      @presenter = Api::V2::Account::SettingsPresenter::BrandingPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :header_color,
      :page_background_color,
      :tab_background_color,
      :text_color,
      :header_logo_url,
      :favicon_url

    should_present_method :header_color, from: :branding

    should_present_method :page_background_color, from: :branding

    should_present_method :tab_background_color, from: :branding

    should_present_method :text_color, from: :branding
  end

  describe Api::V2::Account::SettingsPresenter::AppsPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::AppsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :use,
      :create_private,
      :create_public

    should_present_method :has_apps_create_public?, as: :create_public
  end

  describe Api::V2::Account::SettingsPresenter::TicketSharingPartnersPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::TicketSharingPartnersPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :support_addresses

    should_present_method :ticket_sharing_partner_support_addresses,
      as: :support_addresses
  end

  describe Api::V2::Account::SettingsPresenter::TicketsPresenter do
    describe 'for enterprise customers' do
      before do
        @account = accounts(:extra_large)
        @account.stubs(:use_status_hold?).returns(true)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
        @model = @account
        @settings = @account.settings

        @presenter = Api::V2::Account::SettingsPresenter::TicketsPresenter.new(
          @account.owner,
          url_builder: mock_url_builder
        )
      end

      describe 'when `skill_based_attribute_ticket_mapping` is enabled' do
        before { Arturo.enable_feature!(:skill_based_attribute_ticket_mapping) }

        describe 'when `skill_based_ticket_routing` is disabled' do
          before { Arturo.disable_feature!(:skill_based_ticket_routing) }

          it 'does not include the `using_skill_based_routing` setting' do
            refute @presenter.present(@account)[:tickets].key?(:using_skill_based_routing)
          end
        end

        describe 'when `skill_based_ticket_routing` is enabled' do
          before do
            Arturo.enable_feature!(:skill_based_ticket_routing)
            @using_skill_based_routing = stub('skill based routing boolean setting')
            @account.settings.stubs(:using_skill_based_routing?).returns(@using_skill_based_routing)
          end

          it 'includes the `using_skill_based_routing` setting' do
            assert_equal @presenter.present(@account)[:tickets][:using_skill_based_routing], @using_skill_based_routing
          end
        end
      end

      describe 'when `skill_based_attribute_ticket_mapping` is disabled' do
        before { Arturo.disable_feature!(:skill_based_attribute_ticket_mapping) }

        describe 'when `skill_based_ticket_routing` is disabled' do
          before { Arturo.disable_feature!(:skill_based_ticket_routing) }

          it 'does not include the `using_skill_based_routing` setting' do
            refute @presenter.present(@account)[:tickets].key?(:using_skill_based_routing)
          end
        end

        describe 'when `skill_based_ticket_routing` is enabled' do
          before { Arturo.enable_feature!(:skill_based_ticket_routing) }

          it 'does not include the `using_skill_based_routing` setting' do
            refute @presenter.present(@account)[:tickets].key?(:using_skill_based_routing)
          end
        end
      end

      describe 'when `skill_based_ticket_routing` is enabled' do
        before { Arturo.enable_feature!(:skill_based_ticket_routing) }

        describe 'when `using_skill_based_routing` is true' do
          before do
            @account.settings.stubs(:using_skill_based_routing?).returns(true)
            @account.settings.stubs(:edit_ticket_skills_permission).returns(0)
          end

          it 'includes the `edit_ticket_skills_permission` setting' do
            assert_equal @presenter.present(@account)[:tickets][:edit_ticket_skills_permission], 0
          end
        end

        describe 'when `using_skill_based_routing` is false' do
          before { @account.settings.stubs(:using_skill_based_routing?).returns(false) }

          it 'does not include the `edit_ticket_skills_permission` setting' do
            refute @presenter.present(@account)[:tickets].key?(:edit_ticket_skills_permission)
          end
        end
      end

      describe 'when `skill_based_ticket_routing` is disabled' do
        before { Arturo.disable_feature!(:skill_based_ticket_routing) }

        describe 'when `using_skill_based_routing` is true' do
          before { @account.settings.stubs(:using_skill_based_routing?).returns(true) }

          it 'includes the `edit_ticket_skills_permission` setting' do
            refute @presenter.present(@account)[:tickets].key?(:edit_ticket_skills_permission)
          end
        end

        describe 'when `using_skill_based_routing` is false' do
          before { @account.settings.stubs(:using_skill_based_routing?).returns(false) }

          it 'does not include the `edit_ticket_skills_permission` setting' do
            refute @presenter.present(@account)[:tickets].key?(:edit_ticket_skills_permission)
          end
        end
      end
    end

    describe 'for non-enterprise customers' do
      before do
        @account = accounts(:minimum)
        @model = @account
        @settings = @account.settings

        @presenter = Api::V2::Account::SettingsPresenter::TicketsPresenter.new(
          @account.owner,
          url_builder: mock_url_builder
        )
      end

      describe 'when `skill_based_attribute_ticket_mapping` is enabled' do
        before { Arturo.enable_feature!(:skill_based_attribute_ticket_mapping) }

        describe 'when `skill_based_ticket_routing` is disabled' do
          before { Arturo.disable_feature!(:skill_based_ticket_routing) }

          it 'does not include the `using_skill_based_routing` setting' do
            refute @presenter.present(@account)[:tickets].key?(:using_skill_based_routing)
          end
        end

        describe 'when `skill_based_ticket_routing` is enabled' do
          before do
            Arturo.enable_feature!(:skill_based_ticket_routing)
            @using_skill_based_routing = stub('skill based routing boolean setting')
            @account.settings.stubs(:using_skill_based_routing?).returns(@using_skill_based_routing)
          end

          it 'does not include the `using_skill_based_routing` setting' do
            refute @presenter.present(@account)[:tickets].key?(:using_skill_based_routing)
          end
        end
      end

      describe 'when `skill_based_attribute_ticket_mapping` is disabled' do
        before { Arturo.disable_feature!(:skill_based_attribute_ticket_mapping) }

        describe 'when `skill_based_ticket_routing` is disabled' do
          before { Arturo.disable_feature!(:skill_based_ticket_routing) }

          it 'does not include the `using_skill_based_routing` setting' do
            refute @presenter.present(@account)[:tickets].key?(:using_skill_based_routing)
          end
        end

        describe 'when `skill_based_ticket_routing` is enabled' do
          before { Arturo.enable_feature!(:skill_based_ticket_routing) }

          it 'does not include the `using_skill_based_routing` setting' do
            refute @presenter.present(@account)[:tickets].key?(:using_skill_based_routing)
          end
        end
      end

      describe 'when `skill_based_ticket_routing` is enabled' do
        before { Arturo.enable_feature!(:skill_based_ticket_routing) }

        describe 'when `using_skill_based_routing` is true' do
          before { @account.settings.stubs(:using_skill_based_routing?).returns(true) }

          it 'does not include the `edit_ticket_skills_permission` setting' do
            refute @presenter.present(@account)[:tickets].key?(:edit_ticket_skills_permission)
          end
        end

        describe 'when `using_skill_based_routing` is false' do
          before { @account.settings.stubs(:using_skill_based_routing?).returns(false) }

          it 'does not include the `edit_ticket_skills_permission` setting' do
            refute @presenter.present(@account)[:tickets].key?(:edit_ticket_skills_permission)
          end
        end
      end

      describe 'when `skill_based_ticket_routing` is disabled' do
        before { Arturo.disable_feature!(:skill_based_ticket_routing) }

        describe 'when `using_skill_based_routing` is true' do
          before { @account.settings.stubs(:using_skill_based_routing?).returns(true) }

          it 'includes the `edit_ticket_skills_permission` setting' do
            refute @presenter.present(@account)[:tickets].key?(:edit_ticket_skills_permission)
          end
        end

        describe 'when `using_skill_based_routing` is false' do
          before { @account.settings.stubs(:using_skill_based_routing?).returns(false) }

          it 'does not include the `edit_ticket_skills_permission` setting' do
            refute @presenter.present(@account)[:tickets].key?(:edit_ticket_skills_permission)
          end
        end
      end

      describe 'when the new collaboration is disabled' do
        before { @account.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }

        should_present_keys :accepted_new_collaboration_tos,
          :agent_collision,
          :agent_ticket_deletion,
          :allow_group_reset,
          :assign_tickets_upon_solve,
          :auto_updated_ccs_followers_rules,
          :collaboration,
          :comments_public_by_default,
          :email_attachments,
          :emoji_autocompletion,
          :follower_and_email_cc_collaborations,
          :is_first_comment_private_enabled,
          :light_agent_email_ccs_allowed,
          :list_empty_views,
          :list_newest_comments_first,
          :markdown_ticket_comments,
          :maximum_personal_views_to_list,
          :private_attachments,
          :rich_text_comments,
          :status_hold,
          :tagging

        describe 'tickets.collaboration' do
          before do
            @presenter = Api::V2::Account::SettingsPresenter::TicketsPresenter.new(
              @account.owner,
              url_builder: mock_url_builder
            )
            @account.stubs(has_follower_and_email_cc_collaborations_enabled?: false)
          end

          describe 'when Legacy CCs are enabled' do
            before { @account.is_collaboration_enabled = true }
            it { assert @presenter.present(@account)[:tickets][:collaboration] }
          end

          describe 'when Legacy CCs are disabled' do
            before { @account.is_collaboration_enabled = false }
            it { refute @presenter.present(@account)[:tickets][:collaboration] }
          end
        end
      end

      describe_with_arturo_disabled :email_ccs_light_agents_v2 do
        it 'does not include the `light_agent_email_ccs_allowed` setting' do
          refute @presenter.present(@account)[:tickets][:light_agent_email_ccs_allowed]
        end
      end

      describe 'when the new collaboration is enabled' do
        before { @account.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }

        should_present_keys :accepted_new_collaboration_tos,
          :agent_can_change_requester,
          :agent_collision,
          :agent_email_ccs_become_followers,
          :agent_ticket_deletion,
          :allow_group_reset,
          :assign_tickets_upon_solve,
          :auto_updated_ccs_followers_rules,
          :collaboration,
          :comment_email_ccs_allowed,
          :comments_public_by_default,
          :email_attachments,
          :emoji_autocompletion,
          :follower_and_email_cc_collaborations,
          :is_first_comment_private_enabled,
          :light_agent_email_ccs_allowed,
          :list_empty_views,
          :list_newest_comments_first,
          :markdown_ticket_comments,
          :maximum_personal_views_to_list,
          :private_attachments,
          :rich_text_comments,
          :status_hold,
          :tagging,
          :ticket_followers_allowed
      end

      describe 'tickets.collaboration' do
        before do
          @presenter = Api::V2::Account::SettingsPresenter::TicketsPresenter.new(
            @account.owner,
            url_builder: mock_url_builder
          )
          @account.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
        end

        describe 'when Legacy CCs are enabled' do
          before { @account.is_collaboration_enabled = true }
          it { refute @presenter.present(@account)[:tickets][:collaboration] }
        end

        describe 'when is_collaboration_enabled is false' do
          before { @account.is_collaboration_enabled = false }
          it { refute @presenter.present(@account)[:tickets][:collaboration] }
        end
      end

      should_present_method :events_reverse_order?,
        as:   :list_newest_comments_first,
        from: :settings

      should_present_method :private_attachments?,
        as:   :private_attachments,
        from: :settings

      should_present_method :ticket_tagging?, as: :tagging, from: :settings

      should_present_method :ticket_show_empty_views?,
        as:   :list_empty_views,
        from: :settings

      should_present_method :assign_tickets_upon_solve?,
        as:   :assign_tickets_upon_solve,
        from: :settings

      should_present_method :ticket_delete_for_agents?,
        as:   :agent_ticket_deletion,
        from: :settings

      should_present_method :change_assignee_to_general_group?,
        as:   :allow_group_reset,
        from: :settings

      should_present_method :has_agent_collision?, as: :agent_collision

      should_present_method :is_collaboration_enabled?, as: :collaboration

      should_present_method :use_status_hold?, as: :status_hold

      should_present_method :is_comment_public_by_default,
        as: :comments_public_by_default

      should_present_method :is_first_comment_private_enabled,
        as: :is_first_comment_private_enabled

      should_present_method :max_private_views_for_display,
        as: :maximum_personal_views_to_list
    end
  end

  describe Api::V2::Account::SettingsPresenter::AgentsPresenter do
    before do
      @account = accounts(:minimum)
      @model = @account
      @settings = @account.settings

      @presenter = Api::V2::Account::SettingsPresenter::AgentsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :agent_workspace, :focus_mode

    should_present_method :polaris, as: :agent_workspace, from: :settings

    it('displays value of polaris setting') do
      refute @account.settings.polaris

      refute @presenter.present(@account)[:agents][:agent_workspace]
    end

    it('displays value of the focus_mode setting') do
      refute @account.settings.focus_mode

      refute @presenter.present(@account)[:agents][:focus_mode]
    end
  end

  describe Api::V2::Account::SettingsPresenter::ChatPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::ChatPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :maximum_request_count,
      :welcome_message,
      :enabled,
      :integrated,
      :available

    should_present_method :maximum_chat_requests, as: :maximum_request_count

    should_present_method :chat_welcome_message, as: :welcome_message

    should_present_method :chat_available?, as: :available
  end

  describe Api::V2::Account::SettingsPresenter::TicketFormPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::TicketFormPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :ticket_forms_instructions,
      :raw_ticket_forms_instructions
  end

  describe Api::V2::Account::SettingsPresenter::BrandsPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::BrandsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :default_brand_id,
      :require_brand_on_new_tickets
  end

  describe Api::V2::Account::SettingsPresenter::UserPresenter do
    before do
      @account  = accounts(:minimum)
      @model    = @account
      @settings = @account.settings

      @presenter = Api::V2::Account::SettingsPresenter::UserPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :tagging,
      :time_zone_selection,
      :language_selection,
      :multiple_organizations,
      :agent_created_welcome_emails,
      :end_user_phone_number_validation,
      :have_gravatars_enabled

    should_present_method :has_user_tags?, as: :tagging, from: :settings

    should_present_method :has_time_zone_selection?, as: :time_zone_selection

    should_present_method :has_individual_language_selection?,
      as: :language_selection

    should_present_method :has_multiple_organizations_enabled?,
      as: :multiple_organizations

    should_present_method :is_welcome_email_when_agent_register_enabled?,
      as: :agent_created_welcome_emails
  end

  describe Api::V2::Account::SettingsPresenter::TwitterPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::TwitterPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :shorten_url

    should_present_method :shortened_url_frequency, as: :shorten_url
  end

  describe Api::V2::Account::SettingsPresenter::GoogleAppsPresenter do
    before do
      @account   = accounts(:minimum)
      @model     = @account
      @settings  = @account.settings

      @presenter = Api::V2::Account::SettingsPresenter::GoogleAppsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :has_google_apps, :has_google_apps_admin

    should_present_method :has_google_apps?, from: @account, as: :has_google_apps
    should_present_method :has_google_apps_admin?, from: :settings, as: :has_google_apps_admin
  end

  describe Api::V2::Account::SettingsPresenter::VoicePresenter do
    before do
      @account  = accounts(:minimum)
      @model    = @account
      @settings = @account.settings

      @presenter = Api::V2::Account::SettingsPresenter::VoicePresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :enabled,
      :logging,
      :outbound_enabled,
      :agent_confirmation_when_forwarding,
      :agent_wrap_up_after_calls,
      :maximum_queue_size,
      :maximum_queue_wait_time,
      :only_during_business_hours,
      :recordings_public,
      :uk_mobile_forwarding

    should_present_method :has_voice_enabled?, as: :enabled

    should_present_method :has_voice_logging?, as: :logging

    should_present_method :voice_outbound_enabled?,
      as:   :outbound_enabled,
      from: :settings

    should_present_method :voice_agent_confirmation_when_forwarding?,
      as:   :agent_confirmation_when_forwarding,
      from: :settings

    should_present_method :voice_agent_wrap_up_after_calls?,
      as:   :agent_wrap_up_after_calls,
      from: :settings

    should_present_method :voice_maximum_queue_size?,
      as:    :maximum_queue_size,
      from:  :settings,
      value: 5

    should_present_method :voice_maximum_queue_wait_time?,
      as:    :maximum_queue_wait_time,
      from:  :settings,
      value: 1

    should_present_method :voice_only_during_business_hours?,
      as:   :only_during_business_hours,
      from: :settings

    should_present_method :voice_recordings_public?,
      as:   :recordings_public,
      from: :settings
  end

  describe Api::V2::Account::SettingsPresenter::ScreencastPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @screenr_integration = @account.screenr_integration

      @presenter = Api::V2::Account::SettingsPresenter::ScreencastPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :enabled_for_tickets, :host, :tickets_recorder_id

    should_present_method :has_screencasts_for_tickets_enabled?,
      as: :enabled_for_tickets

    should_present_method :tickets_recorder_id, from: :screenr_integration

    should_present_method :host, from: :screenr_integration
  end

  describe Api::V2::Account::SettingsPresenter::LotusPresenter do
    before do
      @account  = accounts(:minimum)
      @model    = @account
      @settings = @account.settings

      @presenter = Api::V2::Account::SettingsPresenter::LotusPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :prefer_lotus,
      :reporting,
      :pod_id

    should_present_method :has_lotus_reporting?, as: :reporting

    should_present 1, as: :pod_id
  end

  describe Api::V2::Account::SettingsPresenter::StatisticsPresenter do
    before do
      @account  = accounts(:minimum)
      @model    = @account
      @settings = @account.settings

      @presenter = Api::V2::Account::SettingsPresenter::StatisticsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :forum,
      :search,
      :rule_usage

    should_present_method :has_forum_statistics?, as: :forum

    should_present_method :has_search_statistics?, as: :search

    should_present_method :has_rule_usage_stats?, as: :rule_usage
  end

  describe Api::V2::Account::SettingsPresenter::GooddataAdvancedAnalyticsPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::GooddataAdvancedAnalyticsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :enabled

    should_present_method :has_gooddata_advanced_analytics?, as: :enabled
  end

  describe Api::V2::Account::SettingsPresenter::BillingPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::BillingPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :backend

    should_present_method :backend, value: 'internal'
  end

  describe Api::V2::Account::SettingsPresenter::RulePresenter do
    describe 'for enterprise customers' do
      before do
        @account = accounts(:extra_large)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
        @model = @account
        @settings = @account.settings

        @presenter = Api::V2::Account::SettingsPresenter::RulePresenter.new(
          @account.owner,
          url_builder: mock_url_builder
        )
      end

      describe 'skills based ticket routing settings' do
        describe_with_arturo_enabled :skill_based_view_filters do
          it 'includes the `skill_based_filtered_views` setting' do
            assert_empty @presenter.present(@account)[:rule][:skill_based_filtered_views]
          end
        end

        describe_with_arturo_disabled :skill_based_view_filters do
          it 'does not include the `skill_based_filtered_views` setting' do
            refute @presenter.present(@account)[:rule][:skill_based_filtered_views]
          end
        end
      end
    end

    describe 'for non-enterprise customers' do
      before do
        @account  = accounts(:minimum)
        @model    = @account
        @settings = @account.settings

        @presenter = Api::V2::Account::SettingsPresenter::RulePresenter.new(
          @account.owner,
          url_builder: mock_url_builder
        )
      end

      should_present_keys :macro_most_used,
        :macro_order

      should_present_method :macro_most_used, from: :settings
      should_present_method :macro_order, from: :settings
    end
  end

  describe Api::V2::Account::SettingsPresenter::AutomaticAnswersPresenter do
    before do
      @account  = accounts(:minimum)
      @model    = @account
      @settings = @account.settings

      @presenter = Api::V2::Account::SettingsPresenter::AutomaticAnswersPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :threshold
    should_present "balanced", as: :threshold
  end

  describe Api::V2::Account::SettingsPresenter::ActiveFeaturesPresenter do
    before do
      Arturo.disable_feature!(:polaris)
      Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
      Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
      @account       = accounts(:minimum)
      @model         = @account
      @settings      = @account.settings
      @role_settings = @account.role_settings

      @presenter = Api::V2::Account::SettingsPresenter::ActiveFeaturesPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )

      @account.settings.polaris = false
    end

    should_present_keys :on_hold_status,
      :user_tagging,
      :ticket_tagging,
      :topic_suggestion,
      :voice,
      :business_hours,
      :facebook_login,
      :google_login,
      :twitter_login,
      :forum_analytics,
      :agent_forwarding,
      :chat,
      :chat_about_my_ticket,
      :customer_context_as_default,
      :customer_satisfaction,
      :csat_reason_code,
      :screencasts,
      :markdown,
      :bcc_archiving,
      :allow_ccs,
      :advanced_analytics,
      :sandbox,
      :suspended_ticket_notification,
      :twitter,
      :facebook,
      :dynamic_contents,
      :light_agents,
      :ticket_forms,
      :user_org_fields,
      :insights,
      :satisfaction_prediction,
      :is_abusive,
      :rich_content_in_emails,
      :automatic_answers,
      :explore,
      :explore_on_support_ent_plan,
      :explore_on_support_pro_plan,
      :benchmark_opt_out,
      :good_data_and_explore,
      :fallback_composer

    describe 'when `email_settings_api` is enabled' do
      before { Arturo.enable_feature!(:email_settings_api) }

      it 'does includes the email-specific feature settings' do
        assert @presenter.present(@account)[:active_features].key?(:allow_email_template_customization)
        assert @presenter.present(@account)[:active_features].key?(:custom_dkim_domain)
      end
    end

    describe 'when `email_settings_api` is disabled' do
      before { Arturo.disable_feature!(:email_settings_api) }

      it 'does not include the email-specific feature settings' do
        refute @presenter.present(@account)[:active_features].key?(:allow_email_template_customization)
        refute @presenter.present(@account)[:active_features].key?(:custom_dkim_domain)
      end
    end

    should_present_method :use_status_hold?, as: :on_hold_status

    should_present_method :ticket_forms_is_active?, as: :ticket_forms

    should_present_method :custom_fields_is_active?, as: :user_org_fields

    should_present_method :has_user_tags?, as: :user_tagging, from: :settings

    should_present_method :ticket_tagging?, as: :ticket_tagging, from: :settings

    should_present_method :topic_suggestion_for_ticket_submission?,
      as:   :topic_suggestion,
      from: :settings

    should_present_method :voice?, as: :voice, from: :settings

    should_present_method :business_hours_active?,
      as:   :business_hours,
      from: :account

    should_present_method :is_facebook_login_enabled?,
      as:   :facebook_login,
      from: :settings

    should_present_method :is_google_login_enabled?,
      as:   :google_login,
      from: :settings

    should_present_method :end_user_twitter_login?,
      as:   :twitter_login,
      from: :role_settings

    should_present_method :display_analytics?,
      as:   :forum_analytics,
      from: :settings

    should_present_method :agent_forwardable_emails?,
      as:   :agent_forwarding,
      from: :settings

    should_present_method :chat?, as: :chat, from: :settings

    should_present_method :chat_about_my_ticket?,
      as:   :chat_about_my_ticket,
      from: :settings

    should_present_method :has_customer_satisfaction_enabled?,
      as: :customer_satisfaction

    should_present_method :csat_reason_code_enabled?, as: :csat_reason_code

    should_present_method :screencasts_for_tickets?,
      as:   :screencasts,
      from: :settings

    should_present_method :markdown_ticket_comments?,
      as:   :markdown,
      from: :settings

    should_present_method :bcc_archive_address?,
      as:   :bcc_archiving,
      from: :settings

    should_present_method :collaboration_enabled?,
      as:   :allow_ccs,
      from: :settings

    should_present_presence_of :gooddata_integration, as: :advanced_analytics

    should_present_presence_of :sandbox

    should_present_presence_of :light_agents

    should_present_presence_of :cms_texts, as: :dynamic_contents

    it 'presents presence of Monitored Twitter Handles as twitter' do
      assert_equal(
        MonitoredTwitterHandle.where(account_id: @model.id).any?,
        @presenter.model_json(@model)[:twitter]
      )
    end

    it 'presents presence of active Facebook pages as facebook' do
      assert_equal(
        ::Facebook::Page.where(account_id: @model.id).active.any?,
        @presenter.model_json(@model)[:facebook]
      )
    end

    it 'presents suspended_ticket_notification as presence and frequency of suspended_ticket_notification' do
      valuer = proc do
        @presenter.present(@account)[:active_features][:suspended_ticket_notification]
      end

      assert_nil @account.suspended_ticket_notification
      assert_equal false, valuer.call

      stn = @account.create_suspended_ticket_notification
      @account.reload
      assert_equal false, valuer.call

      stn.update_attributes! frequency: 10, email_list: 'agent@zendesk.com'
      @account.reload
      assert(valuer.call)
    end

    describe 'insights' do
      describe 'insights available' do
        describe 'account has a v2 integration' do
          before do
            @account.stubs(:gooddata_integration).returns(stub)
          end

          it 'presents insights as enabled' do
            assert @presenter.present(@account)[:active_features][:insights]
          end
        end

        describe "account doesn't have an integration" do
          it 'presents insights as disabled' do
            refute @account.gooddata_integration.present?
            refute @presenter.present(@account)[:active_features][:insights]
          end
        end
      end
    end

    describe 'benchmark_opt_out settings' do
      describe_with_arturo_enabled :account_settings_benchmark_opt_out do
        it 'includes the benchmark_opt_out setting' do
          assert @presenter.present(@account)[:active_features].key?(:benchmark_opt_out)
        end
      end

      describe_with_arturo_disabled :account_settings_benchmark_opt_out do
        it 'does not include the benchmark_opt_out setting' do
          refute @presenter.present(@account)[:active_features].key?(:benchmark_opt_out)
        end
      end
    end

    describe 'fallback_composer settings' do
      describe_with_arturo_enabled :fallback_omnicomposer do
        it 'prsents the fallback_composer setting as enabled' do
          assert @presenter.present(@account)[:active_features][:fallback_composer]
        end
      end

      describe_with_arturo_disabled :fallback_omnicomposer do
        it 'presents fallback_composer settings as disabled' do
          refute @presenter.present(@account)[:active_features][:fallback_composer]
        end
      end
    end

    describe_with_arturo_disabled :polaris do
      it 'includes the `customer_context_as_default` setting' do
        @account.settings.polaris = false
        assert_includes @presenter.present(@account)[:active_features].keys, :customer_context_as_default
      end
    end

    describe_with_arturo_enabled :polaris do
      it 'does not include the `customer_context_as_default` setting' do
        @account.settings.polaris = true
        refute @presenter.present(@account)[:active_features][:customer_context_as_default]
      end
    end
  end

  describe Api::V2::Account::SettingsPresenter::LimitsPresenter do
    before do
      @account = accounts(:minimum)
      @model = @account

      @presenter = Api::V2::Account::SettingsPresenter::LimitsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :attachment_size
  end

  describe Api::V2::Account::SettingsPresenter::OnboardingPresenter do
    before do
      @account = accounts(:minimum)
      @model = @account

      @presenter = Api::V2::Account::SettingsPresenter::OnboardingPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :checklist_onboarding_version, :onboarding_segments, :product_sign_up
  end

  describe Api::V2::Account::SettingsPresenter::EmailPresenter do
    before do
      @account = accounts(:minimum)
      @model = @account

      @presenter = Api::V2::Account::SettingsPresenter::EmailPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    describe 'when `email_settings_api` is enabled' do
      before { Arturo.enable_feature!(:email_settings_api) }

      it 'does includes the `email` settings' do
        assert @presenter.present(@account).key?(:email)
      end

      should_present_keys :rich_content_in_emails,
        :email_template_selection,
        :send_gmail_messages_via_gmail,
        :modern_email_template,
        :accept_wildcard_emails,
        :personalized_replies,
        :gmail_actions,
        :email_template_photos,
        :simplified_email_threading,
        :email_sender_authentication,
        :custom_dkim_domain,
        :html_mail_template,
        :text_mail_template,
        :mail_delimiter,
        :no_mail_delimiter
    end

    describe 'when `email_settings_api` is disabled' do
      before { Arturo.disable_feature!(:email_settings_api) }

      it 'does not include the `email` settings' do
        assert_nil @presenter.model_json(@model)[:email]
      end
    end
  end

  describe Api::V2::Account::SettingsPresenter::CrossSellPresenter do
    before do
      @account = accounts(:minimum)
      @model = @account

      @presenter = Api::V2::Account::SettingsPresenter::CrossSellPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :show_chat_tooltip, :xsell_source
  end

  describe Api::V2::Account::SettingsPresenter::CdnPresenter do
    let(:expected) do
      {
       hosts:
         [
           { name: 'cloudfront', url: 'http://cloudfront.example.com' },
           { name: 'default', url: 'http://edgecast.example.com' }
         ].sort_by { |h| h[:name] }
      }
    end

    before do
      @account = accounts(:minimum)
      @model = @account

      @presenter = Api::V2::Account::SettingsPresenter::CdnPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :cdn_provider, :fallback_cdn_provider, :hosts

    it 'renders expected data' do
      json = @presenter.model_json(@model)[:hosts]
      assert_equal expected[:hosts], json.sort_by { |h| h[:name] }
    end

    it 'return empty hosts if CDNs are disabled for account' do
      @account.settings.cdn_provider = 'disabled'
      json = @presenter.model_json(@model)[:hosts]
      assert_empty json
    end

    describe 'using ssl' do
      let(:expected) do
        {
         hosts:
           [
             { name: 'cloudfront', url: 'https://cloudfront.example.com' },
             { name: 'default', url: 'https://edgecast.example.com' }
           ].sort_by { |h| h[:name] }
        }
      end

      before do
        @presenter = Api::V2::Account::SettingsPresenter::CdnPresenter.new(
          @account.owner,
          url_builder: mock_url_builder,
          ssl: true
        )
      end

      it 'renders expected data' do
        json = @presenter.model_json(@model)[:hosts]
        assert_equal expected[:hosts], json.sort_by { |h| h[:name] }
      end
    end
  end

  describe Api::V2::Account::SettingsPresenter::MetricsPresenter do
    before do
      @account = accounts(:minimum)
      @model = @account

      @presenter = Api::V2::Account::SettingsPresenter::MetricsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :account_size
  end

  describe Api::V2::Account::SettingsPresenter::LocalizationPresenter do
    before do
      @account = accounts(:minimum)
      @model   = @account

      @presenter = Api::V2::Account::SettingsPresenter::LocalizationPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    it 'presents allowed_translation_locale_ids as locale_ids' do
      @account.allowed_translation_locale_ids = [translation_locales(:brazilian_portuguese).id]
      assert_equal(
        @account.allowed_translation_locale_ids,
        @presenter.model_json(@model)[:locale_ids]
      )
    end
  end
end
