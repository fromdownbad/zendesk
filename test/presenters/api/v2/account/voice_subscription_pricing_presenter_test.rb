require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::VoiceSubscriptionPricingPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:subscription) { subscriptions(:minimum) }
  let(:voice_subscription_pricing) do
    ZendeskBillingCore::Zuora::VoiceSubscriptionPricing.new(subscription.currency_type)
  end
  let(:presenter) do
    Api::V2::Account::VoiceSubscriptionPricingPresenter.new(
      account.owner,
      url_builder: mock_url_builder
    )
  end

  describe "contents of the :voice_pricing key" do
    let(:voice_pricing) { presenter.model_json(voice_subscription_pricing) }

    before do
      voice_subscription_pricing.expects(:voice_plans_from_zuora)
    end

    it "presents the appropriate keys" do
      expected = ['voice_available_plans'].sort
      actual   = voice_pricing.keys.map(&:to_s).sort
      assert_equal expected, actual
    end
  end
end
