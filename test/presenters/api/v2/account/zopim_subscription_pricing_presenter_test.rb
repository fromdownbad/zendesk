require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::ZopimSubscriptionPricingPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    account = accounts(:minimum)
    @subscription = subscriptions(:minimum)
    @zopim_subscription_pricing = ZBC::Zuora::ZopimSubscriptionPricing.new(@subscription.currency_type)
    @presenter = Api::V2::Account::ZopimSubscriptionPricingPresenter.new(account.owner, url_builder: mock_url_builder)
  end

  describe "contents of the :zopim_pricing key" do
    before do
      @zopim_subscription_pricing.expects(:zopim_plans_from_zuora)
      @zopim_pricing = @presenter.model_json(@zopim_subscription_pricing)
    end

    it "presents the appropriate keys" do
      expected = ["zopim_available_plans"].sort
      actual   = @zopim_pricing.keys.map(&:to_s).sort
      assert_equal expected, actual
    end
  end
end
