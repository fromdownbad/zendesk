require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::TpeSubscriptionPricingPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:described_class)      { Api::V2::Account::TpeSubscriptionPricingPresenter }
  let(:account)              { accounts(:minimum) }
  let(:subscription)         { subscriptions(:minimum) }
  let(:subscription_pricing) { ZBC::Zuora::TpeSubscriptionPricing.new(subscription.currency_type) }
  let(:presenter)            { described_class.new(account.owner, url_builder: mock_url_builder) }

  describe "contents of the :tpe_pricing key" do
    let(:presenter_pricing) { presenter.model_json(subscription_pricing) }

    before { subscription_pricing.expects(:tpe_plans_from_zuora) }

    it "presents the appropriate keys" do
      expected = ['tpe_available_plans'].sort
      actual   = presenter_pricing.keys.map(&:to_s).sort

      assert_equal expected, actual
    end
  end
end
