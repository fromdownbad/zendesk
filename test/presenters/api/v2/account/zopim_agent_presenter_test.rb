require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::ZopimAgentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:model) do
    OpenStruct.new.tap do |m|
      m.user = OpenStruct.new
    end
  end

  before do
    @model     = model
    @account   = accounts(:minimum)
    @presenter = Api::V2::Account::ZopimAgentPresenter.new(
      @account.owner, url_builder: mock_url_builder
    )
  end

  should_present_keys :user,
    :zopim_agent_id,
    :email,
    :is_owner,
    :is_enabled,
    :is_administrator,
    :display_name,
    :syncstate,
    :is_linked,
    :is_serviceable
end
