require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::PartnerSettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Account::PartnerSettingsPresenter.new(
      @account.owner, url_builder: mock_url_builder
    )
  end

  should_present_keys :name, :url
end
