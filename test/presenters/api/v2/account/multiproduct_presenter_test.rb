require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::MultiproductPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :subscriptions

  before do
    @account   = accounts(:minimum)
    @model     = subscriptions(:minimum)
    @presenter = Api::V2::Account::MultiproductPresenter.new(@account.owner, url_builder: mock_url_builder)

    @model.stubs(:account).returns(@account)
    @account.stubs(:billing_multi_product_participant?).returns(false)
  end

  should_present_keys :is_multiproduct_account
end
