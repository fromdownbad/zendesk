require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::ExploreSubscriptionPricingPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:described_class)      { Api::V2::Account::ExploreSubscriptionPricingPresenter }
  let(:account)              { accounts(:minimum) }
  let(:subscription)         { subscriptions(:minimum) }
  let(:subscription_pricing) { ZBC::Zuora::ExploreSubscriptionPricing.new(subscription.currency_type) }
  let(:presenter)            { described_class.new(account.owner, url_builder: mock_url_builder) }

  describe "contents of the :explore_pricing key" do
    let(:presenter_pricing) { presenter.model_json(subscription_pricing) }

    before { subscription_pricing.expects(:explore_plans_from_zuora) }

    it "presents the appropriate base key" do
      expected = ['explore_available_plans']
      actual   = presenter_pricing.keys.map(&:to_s)

      assert_equal expected, actual
    end
  end
end
