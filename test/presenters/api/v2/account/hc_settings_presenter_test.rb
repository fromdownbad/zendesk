require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::HcSettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :role_settings, :brands

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Account::HcSettingsPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :features, :security_policy, :state, :email_setup, :ticket_forms, :rate_limits, :ip_restrictions, :brands, :currency, :default_locale

  it "presents brands with Brand presenter" do
    brand = @account.default_brand
    result = @presenter.model_json(@account)

    brand_presenter = Api::V2::Account::HcSettingsPresenter::BrandPresenter.new(@account.owner, url_builder: mock_url_builder)

    assert_equal brand_presenter.model_json(brand), result[:brands].first
  end

  it "presents currency with currency presenter" do
    result = @presenter.model_json(@account)

    currency_presenter = Api::V2::Account::HcSettingsPresenter::CurrencyPresenter.new(@account, url_builder: mock_url_builder)

    assert_equal currency_presenter.model_json(@account), result[:currency]
  end
end

describe Api::V2::Account::HcSettingsPresenter::FeaturesPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @settings  = @account.settings
    @presenter = Api::V2::Account::HcSettingsPresenter::FeaturesPresenter.new(@account.owner, url_builder: mock_url_builder)
    Guide::MultipleHelpCenters.any_instance.stubs(available_via_support?: true)
  end

  should_present_keys :chat_enabled,
    :chat_about_my_ticket,
    :end_user_password_change_visible,
    :end_user_profile_visible,
    :remote_authentication_enabled,
    :end_user_remote_authentication_enabled,
    :ticket_forms,
    :individual_language_selection,
    :dc_in_templates,
    :permission_sets,
    :has_user_tags,
    :is_open,
    :prefer_lotus,
    :satisfaction_ratings_enabled,
    :csat_reason_code_enabled,
    :end_user_attachments,
    :captcha_required,
    :security_headers,
    :multiple_organizations,
    :is_signup_required,
    :has_agent_aliases,
    :has_multibrand,
    :has_multiple_help_centers,
    :has_ios_mobile_deeplinking,
    :has_android_mobile_deeplinking,
    :has_windows_mobile_deeplinking,
    :has_settable_ccs,
    :has_comment_email_ccs,
    :has_pci_credit_card_custom_field,
    :has_organizations,
    :has_groups,
    :pathfinder_app,
    :has_automatic_answers_enabled,
    :has_native_conditional_fields_enabled,
    :has_gravatars_enabled,
    :has_any_active_ssl_certificates

  should_present_method :chat_about_my_ticket?, from: :settings, as: :chat_about_my_ticket
  should_present_method :is_end_user_password_change_visible?, as: :end_user_password_change_visible
  should_present_method :is_end_user_profile_visible?, as: :end_user_profile_visible
  should_present_method :has_ticket_forms?, as: :ticket_forms
  should_present_method :has_user_and_organization_tags?, as: :has_user_tags
  should_present_method :is_open?, as: :is_open
  should_present_method :has_individual_language_selection?, as: :individual_language_selection
  should_present_method :has_customer_satisfaction_enabled?, as: :satisfaction_ratings_enabled
  should_present_method :csat_reason_code_enabled?, as: :csat_reason_code_enabled
  should_present_method :end_user_attachments?, from: :settings, as: :end_user_attachments
  should_present_method :captcha_required?, from: :settings, as: :captcha_required
  should_present_method :security_headers?, from: :settings, as: :security_headers
  should_present_method :has_multiple_organizations_enabled?, as: :multiple_organizations
  should_present_method :is_signup_required?, as: :is_signup_required
  should_present_method :has_multibrand?, as: :has_multibrand
  should_present_method :has_collaborators_settable_in_help_center?, as: :has_settable_ccs
  should_present_method :has_comment_email_ccs_allowed_enabled?, as: :has_comment_email_ccs
  should_present_method :has_pci_credit_card_custom_field?, as: :has_pci_credit_card_custom_field
  should_present_method :has_organizations?, as: :has_organizations
  should_present_method :has_groups?, as: :has_groups
  should_present_method :has_pathfinder_app?, as: :pathfinder_app
  should_present_method :has_automatic_answers_enabled?, as: :has_automatic_answers_enabled
  should_present_method :has_native_conditional_fields_enabled?, as: :has_native_conditional_fields_enabled
  should_present_method :have_gravatars_enabled?, from: :settings, as: :has_gravatars_enabled

  it "presents has_multiple_help_centers" do
    assert_equal @presenter.model_json(@account)[:has_multiple_help_centers], true
  end

  it "presents has_any_active_ssl_certificates" do
    result = @presenter.model_json(@model)
    assert_equal @model.certificates.active.exists?, result[:has_any_active_ssl_certificates]
  end
end

describe Api::V2::Account::HcSettingsPresenter::SecurityPolicyPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Account::HcSettingsPresenter::SecurityPolicyPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :agent_id, :agent_requirements_in_words,
    :end_user_id, :end_user_requirements_in_words
end

describe Api::V2::Account::HcSettingsPresenter::StatePresenter do
  extend Api::Presentation::TestHelper

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Account::HcSettingsPresenter::StatePresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :help_center, :help_center_onboarding_state

  should_present_method :help_center_state,            as: :help_center
  should_present_method :help_center_onboarding_state, as: :help_center_onboarding_state
end

describe Api::V2::Account::HcSettingsPresenter::EmailSetupPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @model   = @account
    @presenter = Api::V2::Account::HcSettingsPresenter::EmailSetupPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :noreply_address, :from_address, :text_mail_template, :html_mail_template

  should_present_method :text_mail_template
  should_present_method :html_mail_template

  describe "account_address" do
    before do
      @account_address = Zendesk::Mailer::AccountAddress.new(account: @account)
      @result = @presenter.model_json(@model)
    end

    it "presents noreply as noreply_address" do
      assert_equal @account_address.noreply, @result[:noreply_address]
    end

    it "presents from as from_address" do
      assert_equal @account_address.from, @result[:from_address]
    end
  end
end

describe Api::V2::Account::HcSettingsPresenter::RateLimitsPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @model   = @account
    @presenter = Api::V2::Account::HcSettingsPresenter::RateLimitsPresenter.new(@account, url_builder: mock_url_builder)
    @settings  = @account.settings
  end

  should_present_keys :content_rate_limit_per_day, :request_rate_limit_per_hour, :api_rate_limit_per_minute, :api_time_limit_per_minute

  should_present_method :end_user_comment_rate_limit, from: :settings, as: :content_rate_limit_per_day
  should_present_method :end_user_portal_ticket_rate_limit, from: :settings, as: :request_rate_limit_per_hour
end

describe Api::V2::Account::HcSettingsPresenter::IpRestrictionsPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @user = @account.owner
    @model = @account
    @presenter = Api::V2::Account::HcSettingsPresenter::IpRestrictionsPresenter.new(@user, url_builder: mock_url_builder)
    @settings  = @account.settings
    @texts = @account.texts
  end

  should_present_keys :enabled, :enable_agent_ip_restrictions, :allowed_ip_ranges

  should_present_method :ip_restriction_enabled?, from: :settings, as: :enabled
  should_present_method :enable_agent_ip_restrictions?, from: :settings, as: :enable_agent_ip_restrictions
  should_present_method :ip_restriction, from: :texts, as: :allowed_ip_ranges

  describe "allowed_ip_ranges" do
    before do
      @texts.stubs(ip_restriction: "foo-bar")
    end

    it "presents right allowed_ip_ranges if user is admin" do
      @user.stubs(is_admin?: true)
      result = @presenter.model_json(@model)
      assert_equal "foo-bar", result[:allowed_ip_ranges]
    end

    it "does not present right allowed_ip_ranges if user is not admin" do
      @user.stubs(is_admin?: false)
      result = @presenter.model_json(@model)
      assert_equal "", result[:allowed_ip_ranges]
    end
  end
end

describe Api::V2::Account::HcSettingsPresenter::BrandPresenter do
  extend Api::Presentation::TestHelper

  before do
    @brand = accounts(:minimum).brands.first
    @model = @brand
    @presenter = Api::V2::Account::HcSettingsPresenter::BrandPresenter.new(@brand, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :email_setup, :default, :logo

  should_present_method :id
  should_present_method :name
  should_present_method :default?, as: :default
end

describe Api::V2::Account::HcSettingsPresenter::BrandedLogoPresenter do
  fixtures :logos

  extend Api::Presentation::TestHelper

  before do
    @logo  = logos(:minimum_logo)
    @model = @logo
    @presenter = Api::V2::Account::HcSettingsPresenter::BrandedLogoPresenter.new(@logo, url_builder: mock_url_builder)
  end

  should_present_keys :filename, :thumbnails

  should_present_method :filename
end

describe Api::V2::Account::HcSettingsPresenter::BrandedLogoThumbnailPresenter do
  extend Api::Presentation::TestHelper

  before do
    @logo = FactoryBot.create(:brand_logo, filename: "large_1.png")
    @thumbnail = @logo.thumbnails.first
    @model = @thumbnail
    @presenter = Api::V2::Account::HcSettingsPresenter::BrandedLogoThumbnailPresenter.new(@thumbnail, url_builder: mock_url_builder)
  end

  should_present_keys :filename, :thumbnail, :path

  it "presents the filename" do
    assert_match @thumbnail.filename, @presenter.model_json(@thumbnail)[:filename]
  end

  it "presents the thumbnail" do
    assert_match @thumbnail.thumbnail, @presenter.model_json(@thumbnail)[:thumbnail]
  end

  it "presents the path" do
    assert_match @thumbnail.public_filename, @presenter.model_json(@thumbnail)[:path]
  end
end

describe Api::V2::Account::HcSettingsPresenter::CurrencyPresenter do
  extend Api::Presentation::TestHelper

  before do
    @model = accounts(:minimum)
    @presenter = Api::V2::Account::HcSettingsPresenter::CurrencyPresenter.new(@model, url_builder: mock_url_builder)
  end

  should_present_keys :name, :symbol

  it "presents the currency string as name" do
    result = @presenter.model_json(@model)
    assert_equal "USD", result[:name]
  end

  it "presents the currency char as symbol" do
    result = @presenter.model_json(@model)
    assert_equal "$", result[:symbol]
  end

  describe 'missing subscription' do
    before do
      @model.subscription = nil
    end

    it 'returns empty result' do
      assert_equal({ name: nil, symbol: nil }, @presenter.model_json(@model))
    end
  end
end

describe Api::V2::Account::HcSettingsPresenter::DefaultLocalePresenter do
  extend Api::Presentation::TestHelper

  before do
    @model = accounts(:minimum)
    @presenter = Api::V2::Account::HcSettingsPresenter::DefaultLocalePresenter.new(@model, url_builder: mock_url_builder)
  end

  should_present_keys :locale

  it "presents the default locale string" do
    result = @presenter.model_json(@model)
    assert_equal "en-US", result[:locale]
  end
end

describe Api::V2::Account::HcSettingsPresenter::BrandedEmailSetupPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @brand = FactoryBot.create(:brand, account_id: @account.id)
    @model = @brand
    @presenter = Api::V2::Account::HcSettingsPresenter::BrandedEmailSetupPresenter.new(@brand, url_builder: mock_url_builder)
  end

  should_present_keys :noreply_address, :from_address, :text_mail_template, :html_mail_template

  it "presents noreply and from addresses using the brand information" do
    assert_match @brand.route.default_host, @presenter.model_json(@brand)[:noreply_address]
    assert_match @brand.route.default_host, @presenter.model_json(@brand)[:from_address]
    assert_match @brand.name, @presenter.model_json(@brand)[:from_address]
  end

  it "presents noreply and from addresses with the account name if the brand has no default_recipient_address" do
    @brand.stubs(default_recipient_address: nil)
    refute_match @brand.name, @presenter.model_json(@brand)[:from_address]
    assert_match @account.name, @presenter.model_json(@brand)[:from_address]
  end
end
