require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::DeploymentsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings

  def set_arturo_feature(feature, enabled)
    Arturo.send(enabled ? :enable_feature! : :disable_feature!, feature)
  end

  before do
    @account = accounts(:minimum)
    @model = @account
    @presenter = Api::V2::Account::DeploymentsPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  describe "default flags" do
    it "includes only lotus flags" do
      @output = @presenter.model_json(@account)
      Account.capabilities.each do |capability_name, capability|
        if capability.public?
          assert @output.include? capability_name
          assert_equal @output[capability_name], @account.send("has_" + capability_name.to_s + "?")
        else
          refute @output.include?(capability_name)
        end
      end
    end

    it "includes Arturo features with a `lotus_feature_` prefix" do
      @output = @presenter.model_json(@account)
      assert_equal(false, @output.include?("lotus_feature_dummy"))
      Arturo::Feature.create(symbol: :lotus_feature_dummy, deployment_percentage: 100)
      Rails.cache.delete("lotus-features/#{@account.id}")
      @output = @presenter.model_json(@account)
      assert @output.include?("dummy")
    end

    it "ignores deprecated Arturo features with a `lotus_feature_ prefix`" do
      Arturo::Feature.create(symbol: :lotus_feature_dummy1, deployment_percentage: 100)
      Arturo::Feature.create(symbol: :lotus_feature_dummy2, deployment_percentage: 100, deprecated_at: Time.zone.now)
      @output = @presenter.model_json(@account)
      assert_includes @output, "dummy1"
      refute_includes @output, "dummy2"
    end
  end
end
