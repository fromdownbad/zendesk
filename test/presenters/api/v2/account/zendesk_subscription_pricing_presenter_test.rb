require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::ZendeskSubscriptionPricingPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:described_class) { Api::V2::Account::ZendeskSubscriptionPricingPresenter }

  let(:account) { accounts(:minimum) }
  let(:subscription) { subscriptions(:minimum) }
  let!(:guide_subscription) { FactoryBot.create(:guide_subscription, account: account) }
  let(:answer_bot_subscription) { nil }
  let(:outbound_subscription) { nil }
  let(:explore_subscription) { nil }
  let(:subscription_pricing) { ZBC::Zuora::ZendeskSubscriptionPricing.new(subscription) }
  let(:presenter) { described_class.new(account.owner, url_builder: mock_url_builder) }
  let(:pricing) { presenter.model_json(subscription_pricing) }

  before do
    AnswerBot::Subscription.stubs(:find_for_account).returns(answer_bot_subscription)
    Outbound::Subscription.stubs(:find_for_account).returns(outbound_subscription)
    Explore::Subscription.stubs(:find_for_account).returns(explore_subscription)
  end

  describe "contents of the :pricing key" do
    let(:json) { presenter.model_json(subscription_pricing) }
    let(:expected_agent_cost_summary) do
      {
        annual_unit_cost: 996.0,
        agents_per_unit: 1,
        monthly_unit_cost: 100.0,
        monthly_unit_cost_with_annual_billing: 83.0
      }
    end
    let(:expected_pricing_keys) do
      [
        "plan_type",
        "gross",
        "net",
        "discount",
        "agent_cost_summary",
        "pending_changes",
        "days_left_in_billing_cycle",
        "flat_fee_cost_summary"
      ]
    end

    before do
      subscription_pricing.stubs(
        available_plans: [],
        billing_cycle_discount_no_coupon: 0.0,
        billing_cycle_name: 'Monthly',
        charge_discounted_price: 100.0,
        charge_undiscounted_price: 100.0,
        pending_changes: {},
        pricing_model_revision: 5,
        promo_coupon_code: nil,
        promo_coupon_discount: 0.0,
        promo_coupon_expires_on: nil,
        promo_coupon_status: nil,
        voice_active?: false,
        voice_subscription: nil,
        billing_multi_product_participant?: false
      )

      present_subscription_product_calls('zendesk',    subscription_pricing)
      present_subscription_product_calls('zopim',      subscription_pricing)
      present_subscription_product_calls('guide',      subscription_pricing)
      present_subscription_product_calls('tpe',        subscription_pricing)
      present_subscription_product_calls('answer_bot', subscription_pricing)
      present_subscription_product_calls('outbound',   subscription_pricing)
    end

    it "presents the appropriate keys" do
      expected = [
        "agent_cost_summary",
        "available_plans",
        "billing_cycle",
        "charge",
        "pending_changes",
        "plan",
        "products",
        "promo",
        "version",
        "voice",
        "is_multiproduct_account"
      ]
      actual = pricing.keys.map(&:to_s)

      assert_same_elements expected, actual
    end

    describe 'with explore_billing arturo' do
      before do
        Arturo.enable_feature!(:explore_billing)
        present_subscription_product_calls('explore', subscription_pricing)
      end

      it "presents keys for explore product" do
        actual = pricing[:products][:explore].keys.map(&:to_s)
        assert_same_elements expected_pricing_keys, actual
      end
    end

    describe 'without explore_billing arturo' do
      before do
        Arturo.disable_feature!(:explore_billing)
      end

      it 'does not include explore product' do
        refute pricing[:products].key?(:explore)
      end
    end

    describe "#present_subscription_product" do
      it "presents keys for zendesk product" do
        actual = pricing[:products][:zendesk].keys.map(&:to_s)
        assert_same_elements expected_pricing_keys, actual
      end

      it "presents keys for zopim product" do
        actual = pricing[:products][:zopim].keys.map(&:to_s)
        assert_same_elements expected_pricing_keys, actual
      end

      describe "for the guide product" do
        it "presents keys for guide product" do
          actual = pricing[:products][:guide].keys.map(&:to_s)
          assert_same_elements expected_pricing_keys, actual
        end
      end

      describe "for the tpe product" do
        it "presents keys for tpe product" do
          actual = pricing[:products][:tpe].keys.map(&:to_s)
          assert_same_elements expected_pricing_keys, actual
        end
      end

      describe "for the answer_bot product" do
        it "presents keys for answer_bot product" do
          actual = pricing[:products][:answer_bot].keys.map(&:to_s)
          assert_same_elements expected_pricing_keys, actual
        end
      end

      describe "for the outbound product" do
        it "presents keys for outbound product" do
          actual = pricing[:products][:outbound].keys.map(&:to_s)
          assert_same_elements expected_pricing_keys, actual
        end
      end
    end

    describe "#present_voice_product" do
      before do
        subscription_pricing.stubs(
          available_plans: [],
          billing_cycle_discount_no_coupon: 0.0,
          billing_cycle_name: 'Monthly',
          charge_discounted_price: 100.0,
          charge_undiscounted_price: 100.0,
          pricing_model_revision: 5,
          promo_coupon_code: nil,
          promo_coupon_discount: 0.0,
          promo_coupon_expires_on: nil,
          promo_coupon_status: nil,
          voice_active?: false,
          voice_subscription: voice_subscription
        )
      end

      describe "with no voice product" do
        let(:voice_subscription) { nil }
        let(:expected_agent_cost_summary) { {} }

        it "presents an empty hash" do
          assert_empty pricing[:products][:voice].keys
        end
      end

      describe "with an active voice product" do
        let(:voice_subscription) { stub(suspended?: false) }
        let(:expected_agent_cost_summary) do
          {
            annual_unit_cost: 996.0,
            agents_per_unit: 1,
            monthly_unit_cost: 100.0,
            monthly_unit_cost_with_annual_billing: 83.0
          }
        end

        before do
          present_subscription_product_calls('voice', subscription_pricing)
        end

        it "presents the appropriate keys" do
          expected = [
            "active",
            "agent_cost_summary",
            "days_left_in_billing_cycle",
            "discount",
            "gross",
            "net",
            "pending_changes",
            "plan_type",
            "pricing_type",
            "flat_fee_cost_summary"
          ]
          actual = pricing[:products][:voice].keys.map(&:to_s)
          assert_same_elements expected, actual
        end
      end

      describe "with a suspended voice product" do
        let(:voice_subscription) { stub(suspended?: true) }
        let(:expected_agent_cost_summary) do
          {
            annual_unit_cost: 996.0,
            agents_per_unit: 1,
            monthly_unit_cost: 100.0,
            monthly_unit_cost_with_annual_billing: 83.0
          }
        end

        it "presents an empty hash" do
          assert_empty pricing[:products][:voice].keys
        end
      end
    end

    describe "contents of the :plan key" do
      it "presents the appropriate keys" do
        expected = ["id", "name"]
        actual   = pricing[:plan].keys.map(&:to_s)

        assert_same_elements expected, actual
      end

      it "has correct plan id" do
        assert_equal 2, pricing[:plan][:id]
      end

      it "has correct plan name" do
        assert_equal 'Regular', pricing[:plan][:name]
      end
    end

    describe "contents of the :voice key" do
      it "presents the appropriate keys" do
        expected = ["active"]
        actual   = pricing[:voice].keys.map(&:to_s)

        assert_same_elements expected, actual
      end

      describe "when there is no voice sub-account" do
        before { subscription.account.voice_sub_account = nil }

        it "has the :active key value set to false" do
          assert_equal false, pricing[:voice][:active]
        end
      end
    end

    describe "contents of the :billing_cycle key" do
      it "presents the appropriate keys" do
        expected = ["id", "name", "discount"]
        actual   = pricing[:billing_cycle].keys.map(&:to_s)

        assert_same_elements expected, actual
      end

      it "has correct billing cycle id" do
        assert_equal 1, pricing[:billing_cycle][:id]
      end

      it "has correct billing cycle name" do
        assert_equal 'Monthly', pricing[:billing_cycle][:name]
      end

      it "has correct billing cycle discount" do
        assert_equal 0.0, pricing[:billing_cycle][:discount]
      end
    end

    describe "contents of the :charge key" do
      it "presents the appropriate keys" do
        expected = ["currency", "gross", "net"]
        actual   = pricing[:charge].keys.map(&:to_s)

        assert_same_elements expected, actual
      end

      it "has correct currency" do
        assert_equal 'USD', pricing[:charge][:currency]
      end

      it "has correct undiscounted price" do
        assert_equal 100.0, pricing[:charge][:gross]
      end

      it "has correct discounted price" do
        assert_equal 100.0, pricing[:charge][:net]
      end
    end

    describe "contents of the :promo key" do
      it "presents the appropriate keys" do
        expected = ["code", "discount", "ends_at", "status"]
        actual   = pricing[:promo].keys.map(&:to_s)

        assert_same_elements expected, actual
      end

      it "has correct code" do
        assert_nil pricing[:promo][:code]
      end

      it "has correct discount" do
        assert_equal 0.0, pricing[:promo][:discount]
      end

      it "has correct status" do
        assert_nil pricing[:promo][:status]
      end

      it "has correct ends at" do
        assert_nil pricing[:promo][:ends_at]
      end
    end

    describe "contents of the :agent_cost_summary key" do
      it "has correct values" do
        assert_equal expected_agent_cost_summary, pricing[:agent_cost_summary]
      end
    end
  end

  def present_subscription_product_calls(product, pricing)
    pricing.expects(:product_plan_type).with(product)
    pricing.expects(:charge_gross).with(product)
    pricing.expects(:charge_subtotal).with(product)
    pricing.expects(:product_discount).with(product)
    pricing.expects(:multi_product_agent_cost_summary).with(product).
      at_least(1).returns(expected_agent_cost_summary)
    pricing.expects(:flat_fee_cost_summary).with(product)
    pricing.expects(:pending_changes).with(product)
    pricing.expects(:days_left_in_billing_cycle).with(product)
  end
end
