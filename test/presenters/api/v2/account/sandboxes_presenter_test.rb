require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::SandboxesPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings

  before do
    @account = accounts(:minimum)
    @model = @account
    @presenter = Api::V2::Account::SandboxesPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  it "has 'account' as the root key" do
    assert_equal :account, @presenter.model_key
  end

  should_present_keys :name,
    :subdomain,
    :time_format,
    :time_zone,
    :owner_id,
    :url,
    :sandbox,
    :multiproduct,
    :active,
    :created_at,
    :sandbox_type

  it "presents account url" do
    assert_equal @model.url, @presenter.present(@model)[:account][:url]
  end

  it "presents account created_at" do
    assert_equal @model.created_at, @presenter.present(@model)[:account][:created_at]
  end

  describe 'when the sandbox_type is defined' do
    before do
      @model.settings.sandbox_type = 'full'
    end

    it 'presents the sandbox_type' do
      assert_equal @model.settings.sandbox_type, @presenter.present(@model)[:account][:sandbox_type]
    end
  end

  describe "when account is inactive" do
    before do
      @account.stubs(:is_active).returns(false)
    end

    it "presents active=false" do
      refute @presenter.present(@model)[:account][:active]
    end
  end

  describe "when account isn't serviceable" do
    before do
      @account.stubs(:is_serviceable).returns(false)
    end

    it "presents active=false" do
      refute @presenter.present(@model)[:account][:active]
    end
  end
end
