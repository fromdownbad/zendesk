require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Account::AddonsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }

  let(:user) { users(:minimum_agent) }

  let(:presenter) { Api::V2::Account::AddonsPresenter.new(user, url_builder: mock_url_builder) }

  before do
    account.subscription_feature_addons.create!(
      name:               'advanced_security',
      zuora_rate_plan_id: '123456'
    )
    account.subscription_feature_addons.create!(
      name:               'nps',
      boost_expires_at:   2.weeks.from_now
    )
    account.subscription_feature_addons.create!(
      name:               'enterprise_productivity_pack',
      boost_expires_at:   1.week.from_now
    )
  end

  it 'presents addons with metadata' do
    output = presenter.collection_presenter.present(account.subscription_feature_addons.reload)
    assert_kind_of Array, output[:addons]
    assert_equal 3, output[:count]
    assert_equal([:boosted, :name, :purchased, :readable_name, :url], output[:addons].first.keys.sort)
  end

  it 'presents expired addons appropriately' do
    # expire :enterprise_productivity_pack addon
    SubscriptionFeatureAddon.where(account_id: account.id, name: 'enterprise_productivity_pack').first.update_attribute(:boost_expires_at, 1.week.ago)
    output = presenter.collection_presenter.present(account.subscription_feature_addons.reload)
    hv = output[:addons].detect { |a| a[:name] == 'enterprise_productivity_pack' }
    assert_equal false, hv[:boosted]
    assert_equal false, hv[:purchased]
  end
end
