require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::GuideSubscriptionPricingPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:described_class)      { Api::V2::Account::GuideSubscriptionPricingPresenter }
  let(:account)              { accounts(:minimum) }
  let(:subscription)         { subscriptions(:minimum) }
  let(:subscription_pricing) { ZBC::Zuora::GuideSubscriptionPricing.new(subscription.currency_type) }
  let(:presenter)            { described_class.new(account.owner, url_builder: mock_url_builder) }

  describe "contents of the :guide_pricing key" do
    let(:presenter_pricing) { presenter.model_json(subscription_pricing) }

    before { subscription_pricing.expects(:guide_plans_from_zuora) }

    it "presents the appropriate keys" do
      expected = ['guide_available_plans'].sort
      actual   = presenter_pricing.keys.map(&:to_s).sort

      assert_equal expected, actual
    end
  end
end
