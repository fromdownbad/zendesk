require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::VoicePartnerEditionAccountPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :subscriptions

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)

    @account = FactoryBot.create(:account)
    FactoryBot.create(:voice_partner_edition_account, account_id: @account.id)

    @model     = @account.voice_partner_edition_account
    @presenter = Api::V2::Account::VoicePartnerEditionAccountPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :plan_type, :active, :has_addon, :trial, :trial_expires_at
end
