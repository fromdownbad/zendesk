require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::VoiceSubscriptionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :devices

  before do
    ZendeskBillingCore::Zuora::Jobs::AddVoiceJob.stubs(:enqueue)
    @account   = accounts(:minimum)
    @user      = users(:minimum_agent)
    @presenter = Api::V2::Account::VoiceSubscriptionPresenter.new(@user, url_builder: mock_url_builder)
  end

  describe "when a nil record is passed" do
    it "should present empty hash" do
      expected = {}
      assert_equal expected, @presenter.model_json(nil)
    end
  end

  describe "with a voice subscription" do
    let(:voice_subscription) do
      FactoryBot.create(:voice_subscription, account_id: @account.id)
    end

    it "should present" do
      expected = {
        status: "customer",
        max_agents: 5,
        plan_type: 1,
        plan_name: "Basic",
        is_active: true
      }
      assert_equal expected, @presenter.model_json(voice_subscription)
    end
  end
end
