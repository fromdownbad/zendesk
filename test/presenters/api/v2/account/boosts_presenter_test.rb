require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Account::BoostsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @user      = users(:minimum_admin)
    @presenter = Api::V2::Account::BoostsPresenter.new(@user, url_builder: mock_url_builder)
  end

  describe "on an account with both active plan and feature boosts" do
    before do
      @account.create_feature_boost(
        boost_plan: 'ExtraLarge',
        valid_until: 2.weeks.from_now
      )
      @account.subscription_feature_addons.create!(
        name: 'nps',
        boost_expires_at: 2.weeks.from_now
      )
      @result = @presenter.present(@account)
    end

    it 'presents the expected list of two boosts' do
      output = @result[:boosts]
      expected = [
        {type: "addon", name: "nps", days_remaining: 14},
        {type: "plan", name: "Enterprise", days_remaining: 14}
      ]

      assert_equal expected.map { |b| b[:name] }.sort, output.map { |b| b[:name] }.sort
    end
  end

  describe "on an account with active feature boosts" do
    before do
      @account.subscription_feature_addons.create!(
        name: 'advanced_security',
        boost_expires_at: 2.weeks.from_now
      )
      @account.subscription_feature_addons.create!(
        name: 'nps',
        boost_expires_at: 2.weeks.from_now
      )
      @result = @presenter.present(@account)
    end

    it 'presents the expected list of two boosts' do
      output = @result[:boosts]
      expected = [
        {type: "addon", name: "advanced_security", days_remaining: 14},
        {type: "addon", name: "nps", days_remaining: 14}
      ]

      assert_equal expected.map { |b| b[:name] }.sort, output.map { |b| b[:name] }.sort
    end
  end

  describe "on an account with an inactive plan boost" do
    before do
      @account.create_feature_boost(
        boost_plan: 'ExtraLarge',
        valid_until: 2.weeks.ago
      )
      @result = @presenter.present(@account)
    end

    it 'presents the expected list of one boost with a negative days remaining' do
      output = @result[:boosts]
      expected = [{type: "plan", name: "Enterprise", days_remaining: -14}]

      assert_equal expected, output
    end
  end

  describe "on an account with active plan boost" do
    before do
      @account.create_feature_boost(
        boost_plan: 'ExtraLarge',
        valid_until: 2.weeks.from_now
      )
      @result = @presenter.present(@account)
    end

    it 'presents the expected list of one boost' do
      output = @result[:boosts]
      expected = [{type: "plan", name: "Enterprise", days_remaining: 14}]

      assert_equal expected, output
    end
  end

  describe "on an account with neither plan nor feature boosts" do
    before do
      @result = @presenter.present(@account)
    end

    it 'presents a empty list of boosts' do
      output = @result[:boosts]

      assert_empty output
    end
  end
end
