require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::FeaturesPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :devices

  let(:account) { accounts(:minimum) }

  let(:user) { users(:minimum_agent) }

  let(:presenter) do
    Api::V2::Account::FeaturesPresenter.new(user, url_builder: mock_url_builder)
  end

  let(:features) do
    {
      feature1: Account::Capability.new(
        :feature1, public: true, subscription: true
      ),
      feature2: Account::Capability.new(
        :feature2, public: true, subscription: false
      ),
    }
  end

  before do
    @account   = account
    @model     = features
    @presenter = presenter
  end

  should_present_keys :feature1, :feature2

  describe '#present' do
    before do
      Arturo.enable_feature!(:feature1)
      Arturo.disable_feature!(:feature2)
      Subscription.any_instance.stubs(:has_feature1?).returns(true)
    end

    it 'should list feature and availability status for the account' do
      actual   = presenter.present(features)
      expected = {
        features: {
          feature1: { enabled: true },
          feature2: { enabled: false }
        }
      }
      assert_equal expected, actual
    end
  end
end
