require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::KnowledgeSettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @model   = @account
    @presenter = Api::V2::Account::KnowledgeSettingsPresenter.new(
      @account.owner,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :features

  it "presents with Features presenter" do
    result = @presenter.model_json(@account)

    features_presenter = Api::V2::Account::KnowledgeSettingsPresenter::FeaturesPresenter.new(
      @account.owner,
      url_builder: mock_url_builder
    )

    assert_equal features_presenter.model_json(@account), result[:features]
  end
end

describe Api::V2::Account::KnowledgeSettingsPresenter::FeaturesPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @model = @account
    @settings = @account.settings
    @presenter = Api::V2::Account::KnowledgeSettingsPresenter::FeaturesPresenter.new(
      @account.owner,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :can_manage_lists, :has_labels

  should_present_method :has_knowledge_bank_list_management?, as: :can_manage_lists
  should_present_method :has_article_labels?, as: :has_labels
end
