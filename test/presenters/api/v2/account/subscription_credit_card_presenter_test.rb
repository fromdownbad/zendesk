require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::SubscriptionCreditCardPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :subscriptions

  before do
    @account   = accounts(:minimum)
    @model     = subscriptions(:minimum)
    @presenter = Api::V2::Account::SubscriptionCreditCardPresenter.new(@account.owner, url_builder: mock_url_builder)

    credit_card = stub(
      credit_card_mask_number: 'shadowed_number',
      credit_card_expiration_month: 1,
      credit_card_expiration_year: 2000
    )
    @model.stubs(:zuora_subscription).returns(stub(credit_card: credit_card))
  end

  should_present_keys :shadowed_number, :expiry

  should_present_method :shadowed_number, value: 'shadowed_number'
  should_present_method :expiry,          value: {month: 1, year: 2000}
end
