require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all, :translation_locales

  let(:account) do
    account = accounts(:minimum)
    account.create_voice_recharge_settings(enabled: true, minimum_balance: 5, amount: 50)
    account
  end
  let(:recharge_settings) { account.voice_recharge_settings }
  let(:user) { users(:minimum_agent) }

  before do
    @model = recharge_settings
    @presenter = Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter.new(@model, url_builder: mock_url_builder)
  end

  should_present_keys :enabled,
    :minimum_balance,
    :amount,
    :balance,
    :payment_status

  describe "with voice recharge settings" do
    before do
      ZendeskBillingCore::Zuora::VoiceUsage.expects(:monetary_balance).with(account.id).returns(1000.00)
    end

    it "should present" do
      expected = {
        enabled: true,
        minimum_balance: 5.00,
        amount: 50.00,
        balance: 1000.00,
        payment_status: nil
      }
      assert_equal expected, @presenter.model_json(account.voice_recharge_settings)
    end
  end
end
