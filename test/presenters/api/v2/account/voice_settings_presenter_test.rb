require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::VoiceSettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings

  describe Api::V2::Account::VoiceSettingsPresenter do
    before do
      @account   = accounts(:minimum)
      @model     = @account
      @presenter = Api::V2::Account::VoiceSettingsPresenter.new(@account.owner, url_builder: mock_url_builder)
    end

    should_present_keys :features
  end

  describe Api::V2::Account::VoiceSettingsPresenter::FeaturesPresenter do
    before do
      @account   = accounts(:minimum)
      @model     = @account
      @settings  = @account.settings
      @presenter = Api::V2::Account::VoiceSettingsPresenter::FeaturesPresenter.new(@account.owner, url_builder: mock_url_builder)
    end

    should_present_method :has_permission_sets?,    as: :permission_sets
    should_present_method :has_business_hours?,     as: :business_hours
    should_present_method :has_multiple_schedules?, as: :multiple_schedules
    should_present_method :business_hours_active?,  as: :business_hours_active
    should_present_method :has_multibrand?,         as: :multibrand
  end
end
