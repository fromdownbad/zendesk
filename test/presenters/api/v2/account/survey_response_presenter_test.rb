require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::SurveyResponsePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    @account   = accounts(:minimum)
    @model     = @account.create_survey_response
    @presenter = Api::V2::Account::SurveyResponsePresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :industry, :employee_count, :target_audience, :customer_count, :support_structure, :agent_count, :team_count
end
