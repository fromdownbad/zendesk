require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 9

describe Api::V2::Account::SubscriptionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :subscriptions

  let(:zuora_subscription) do
    stub(
      credit_card:                   credit_card,
      assisted?:                     false,
      is_elite?:                     false,
      future_subscription_starts_at: nil,
      payment_method_type:           1
    )
  end

  let (:credit_card) do
    stub(
      credit_card_mask_number:      'shadowed_number',
      credit_card_expiration_month: 1,
      credit_card_expiration_year:  2000
    )
  end

  let(:suite_allowed) { anything }

  before do
    # NOTE: `test_helper.rb` provides many of the stubs assumed in this file.

    Timecop.freeze Date.new(2008, 1, 1)

    pricing_calculations = {
      cycle_discount_no_coupon: 0.0,
      coupon_code:              'ANYCODE',
      cycle_coupon_discount:    0.0,
      coupon_expires_on:        '2012-12-21',
      coupon_status:            :ok,
      cycle_undiscounted_price: 0.0,
      cycle_discounted_price:   0.0
    }
    ZendeskBillingCore::Zuora::Commands::Subscribe::Preview.any_instance.
      stubs(pricing_calculations: pricing_calculations)

    plan = OpenStruct.new(has_base_pricing: false, net_unit_price_per_month: 0.0)
    ZendeskBillingCore::Zuora::Plan.stubs(lookup: plan)

    @account   = accounts(:minimum)
    @model     = subscriptions(:minimum)

    @model.stubs(:account).returns(@account)
    @model.stubs(:zuora_subscription).returns(zuora_subscription)
    @model.stubs(:voice_account).returns(nil)
    @model.stubs(:guide_preview).returns(nil)
    @model.stubs(:term_end_date).returns(nil)
    @model.stubs(:suite_allowed?).returns(suite_allowed)

    @presenter = Api::V2::Account::SubscriptionPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :account_type,
    :active,
    :created_at,
    :trial_expires_on,
    :days_left_in_trial,
    :dunning,
    :collect_vat,
    :help_desk_size,
    :base_agents,
    :max_agents,
    :plan_type,
    :sales_model,
    :plan_name,
    :source,
    :suspended,
    :trial,
    :suite_active,
    :updated_at,
    :voice_customer,
    :billing_backend,
    :file_upload_cap,
    :is_elite,
    :pricing_model_revision,
    :future_subscription_exists,
    :add_agents_available,
    :plans,
    :term_end_date,
    :zuora_payment_method_type,
    :suite_allowed

  should_present_method :is_trial, as: :trial

  describe 'future_subscription_present' do
    describe 'zuora-managed' do
      describe 'without a future subscription' do
        should_present(false, as: :future_subscription_exists)
      end

      describe 'with a future subscription' do
        let(:zuora_subscription) do
          stub(
            credit_card:                   credit_card,
            assisted?:                     false,
            is_elite?:                     false,
            future_subscription_starts_at: 3.months.from_now,
            payment_method_type:           1
          )
        end

        should_present(true, as: :future_subscription_exists)
      end
    end

    describe 'non-zuora-managed' do
      let(:zuora_subscription) { nil }

      should_present(false, as: :future_subscription_exists)
    end
  end

  describe 'add_agents_available' do
    describe 'zuora-managed' do
      describe 'self-service when valid' do
        should_present(true, as: :add_agents_available)
      end

      describe 'assisted sales model with assisted_agent_add_enabled' do
        let(:zuora_subscription) do
          stub(
            credit_card:                   credit_card,
            assisted?:                     true,
            is_elite?:                     false,
            assisted_agent_add_enabled?:   true,
            future_subscription_starts_at: nil,
            payment_method_type:           1
          )
        end

        should_present(true, as: :add_agents_available)
      end

      describe 'assisted sales model without assisted_agent_add_enabled' do
        let(:zuora_subscription) do
          stub(
            credit_card:                   credit_card,
            assisted?:                     true,
            is_elite?:                     false,
            assisted_agent_add_enabled?:   false,
            future_subscription_starts_at: nil,
            payment_method_type:           1
          )
        end

        should_present(false, as: :add_agents_available)
      end

      describe 'customer with addons' do
        let(:zuora_subscription) do
          stub(
            credit_card:                   credit_card,
            assisted?:                     true,
            is_elite?:                     false,
            assisted_agent_add_enabled?:   true,
            future_subscription_starts_at: nil,
            payment_method_type:           1
          )
        end

        before do
          @account.subscription_feature_addons.create!(
            name:               'advanced_security',
            zuora_rate_plan_id: '123456'
          )
        end

        should_present(false, as: :add_agents_available)
      end
    end

    describe 'non-zuora-managed' do
      let(:zuora_subscription) { nil }

      should_present(false, as: :add_agents_available)
    end
  end

  describe 'types of plans' do
    let(:json) { @presenter.present(@model)[@presenter.model_key] }

    describe 'included plans' do
      it 'presents the plan' do
        assert json[:plans].key?(:tpe)
      end
    end

    describe 'zendesk plan' do
      it 'presents the correct keys' do
        expected = [
          :plan_type,
          :plan_name,
          :base_agents,
          :max_agents,
          :is_elite,
          :trial,
          :file_upload_cap,
          :help_desk_size,
          :trial_expires_on,
          :days_left_in_trial
        ]

        assert_equal expected, json[:plans][:zendesk].keys
      end
    end

    describe 'zopim plan' do
      let(:json) { @presenter.present(@model)[@presenter.model_key] }

      describe 'without a zopim subscription' do
        it 'presents nil keys' do
          expected = {
            max_agents:         nil,
            plan_type:          nil,
            trial:              false,
            trial_expires_on:   nil,
            expired_trial:      false,
            days_left_in_trial: nil,
            plan_name:          nil,
            plan_weight:        nil,
            is_legacy_plan:     false
          }

          assert_equal expected, json[:plans][:zopim]
        end
      end

      describe 'with a paid zopim subscription' do
        let(:zopim_subscription) do
          stub(
            max_agents:        1,
            present_plan_type: 'basic',
            trial:             false,
            trial_expires_on:  nil,
            expired_trial?:    false,
            is_active?:        true,
            is_trial?:         false,
            present_plan_name: 'Basic',
            plan_type:         'basic',
            is_legacy_chat?:   true
          )
        end

        before { @model.stubs(:zopim_subscription).returns(zopim_subscription) }

        it 'presents correct keys' do
          expected = {
            max_agents:         1,
            plan_type:          'basic',
            trial:              false,
            trial_expires_on:   nil,
            expired_trial:      false,
            days_left_in_trial: 0,
            plan_name:          'Basic',
            plan_weight:        2,
            is_legacy_plan:     true
          }

          assert_equal expected, json[:plans][:zopim]
        end
      end

      describe 'with a trial zopim subscription' do
        describe 'and smaller number of support agents' do
          let(:zopim_subscription) do
            stub(
              max_agents:        @account.subscription.max_agents + 1,
              present_plan_type: 'premium',
              trial:             true,
              trial_expires_on:  Time.now + 1.day,
              expired_trial?:    false,
              is_active?:        true,
              is_trial?:         true,
              days_left_in_trial: 1,
              present_plan_name: 'Premium',
              plan_type:         'trial',
              is_legacy_chat?:   false
            )
          end

          before { @model.stubs(:zopim_subscription).returns(zopim_subscription) }

          it 'uses the support max agent count' do
            expected = {
              max_agents:         @account.subscription.max_agents,
              plan_type:          'premium',
              trial:              true,
              trial_expires_on:   Time.now + 1.day,
              expired_trial:      false,
              days_left_in_trial: 1,
              plan_name:          'Premium',
              plan_weight:        0,
              is_legacy_plan:     false
            }

            assert_equal expected, json[:plans][:zopim]
          end
        end

        describe 'and larger number of support agents' do
          let(:zopim_subscription) do
            stub(
              max_agents:        @account.subscription.max_agents - 1,
              present_plan_type: 'premium',
              trial:             true,
              trial_expires_on:  Time.now + 1.day,
              expired_trial?:    false,
              is_active?:        true,
              is_trial?:         true,
              days_left_in_trial: 1,
              present_plan_name: 'Premium',
              plan_type:         'trial',
              is_legacy_chat?:   false
            )
          end

          before { @model.stubs(:zopim_subscription).returns(zopim_subscription) }

          it 'uses the zopim trial max agent count' do
            expected = {
              max_agents:         zopim_subscription.max_agents,
              plan_type:          'premium',
              trial:              true,
              trial_expires_on:   Time.now + 1.day,
              expired_trial:      false,
              days_left_in_trial: 1,
              plan_name:          'Premium',
              plan_weight:        0,
              is_legacy_plan:     false
            }

            assert_equal expected, json[:plans][:zopim]
          end
        end
      end
    end

    describe 'voice plan' do
      let(:json) { @presenter.present(@model)[@presenter.model_key] }

      describe 'without a voice subscription' do
        it 'presents the correct keys' do
          expected = [
            :voice_customer
          ]

          assert_equal expected, json[:plans][:voice].keys
        end
      end

      describe 'voice customer' do
        before do
          @model.account.stubs(:current_voice_status).returns('Customer')
        end

        describe 'with an active voice subscription' do
          let(:voice_subscription) do
            stub(
              max_agents:                4,
              plan_type:                 1,
              is_active?:                true,
              trial:                     false,
              legacy:                    false,
              original_voice_plan_type?: false
            )
          end

          before { @model.stubs(:voice_preview).returns(voice_subscription) }

          it 'presents correct keys and values' do
            expected = {
              voice_customer:      true,
              max_agents:          4,
              plan_type:           1,
              trial:               false,
              legacy:              false,
              original_voice_plan: false
            }

            assert_equal expected, json[:plans][:voice]
          end
        end

        describe 'with a suspended voice subscription' do
          let(:voice_subscription) do
            stub(
              max_agents:                4,
              plan_type:                 1,
              is_active?:                false,
              trial:                     false,
              legacy:                    false,
              original_voice_plan_type?: false
            )
          end

          before do
            @model.stubs(:voice_preview).returns(voice_subscription)
          end

          it 'presents correct keys and values' do
            expected = {
              voice_customer: true
            }

            assert_equal expected, json[:plans][:voice]
          end
        end
      end
    end

    describe 'guide plan' do
      let(:guide_plan) { @presenter.guide_plan(guide_subscription) }

      before { @model.stubs(:guide_preview).returns(guide_subscription) }

      describe 'with a guide preview' do
        let(:guide_subscription) do
          FactoryBot.build(:guide_subscription, account: nil)
        end

        before do
          @presenter.stubs(:side_load?).with(:pricing).returns(side_load_pricing)
        end

        describe 'with side load of pricing' do
          let(:side_load_pricing) { true }

          it 'presents the correct attributes' do
            expected = {
              legacy:             guide_subscription.legacy?,
              max_agents:         guide_subscription.max_agents,
              plan_type:          guide_subscription.plan_type,
              trial:              guide_subscription.trial?,
              trial_expires_on:   guide_subscription.trial_expires_at,
              days_left_in_trial: guide_subscription.days_left_in_trial
            }

            assert_equal expected, guide_plan
          end
        end

        describe 'without side load of pricing' do
          let(:side_load_pricing) { false }

          it 'presents an empty hash' do
            expected = {}

            assert_equal expected, guide_plan
          end
        end
      end

      describe 'without a guide preview' do
        let(:guide_subscription) { nil }

        it 'presents an empty hash' do
          expected = {}

          assert_equal expected, guide_plan
        end
      end
    end

    describe 'tpe plan' do
      let(:json) { @presenter.present(@model)[@presenter.model_key] }

      describe 'with an active tpe subscription' do
        let(:tpe_subscription) do
          FactoryBot.build(
            :tpe_subscription,
            active:     true,
            max_agents: 10,
            plan_type:  1,
            account:    nil
          )
        end

        before do
          @model.stubs(:tpe_subscription).returns(tpe_subscription)
        end

        it 'presents the correct attributes' do
          expected = {
            max_agents:         10,
            plan_type:          1,
            trial:              false,
            trial_expires_on:   nil,
            days_left_in_trial: -1
          }

          assert_equal expected, json[:plans][:tpe]
        end
      end

      describe 'with an inactive tpe subscription' do
        let(:tpe_subscription) do
          FactoryBot.build(
            :tpe_subscription,
            active:     false,
            max_agents: 10,
            plan_type:  1,
            account:    nil
          )
        end

        before do
          @model.stubs(:tpe_subscription).returns(tpe_subscription)
        end

        it 'presents an empty hash' do
          expected = {}

          assert_equal expected, json[:plans][:tpe]
        end
      end

      describe 'when tpe is in trial' do
        let(:next_day) { Time.zone.now + 1.day }

        let(:tpe_account) do
          FactoryBot.build(
            :voice_partner_edition_account,
            active:           true,
            trial:            1,
            trial_expires_at: next_day,
            account:          nil
          )
        end

        before do
          @model.account.stubs(:voice_partner_edition_account).returns(tpe_account)
        end

        describe 'with 2 zendesk max agents' do
          before do
            @model.base_agents = 2
          end

          it 'presents tpe trial with 2 agents' do
            expected = {
              max_agents:         2,
              plan_type:          ZBC::Tpe::PlanType::PartnerEdition.plan_type,
              trial:              true,
              trial_expires_on:   next_day,
              days_left_in_trial: 1
            }

            assert_equal expected, json[:plans][:tpe]
          end
        end

        describe 'with 88 zendesk max agents' do
          before do
            @model.base_agents = 88
          end

          it 'presents tpe trial with 88 agents' do
            expected = {
              max_agents:         88,
              plan_type:          ZBC::Tpe::PlanType::PartnerEdition.plan_type,
              trial:              true,
              trial_expires_on:   next_day,
              days_left_in_trial: 1
            }

            assert_equal expected, json[:plans][:tpe]
          end
        end

        describe 'and missing trial_expires_on date' do
          let(:tpe_account) do
            FactoryBot.build(
              :voice_partner_edition_account,
              active:           true,
              trial:            1,
              trial_expires_at: nil,
              account:          nil
            )
          end

          it 'presents 0 as days_left_in_trial value' do
            expected = {
              max_agents:         10,
              plan_type:          ZBC::Tpe::PlanType::PartnerEdition.plan_type,
              trial:              true,
              trial_expires_on:   nil,
              days_left_in_trial: -1
            }

            assert_equal expected, json[:plans][:tpe]
          end
        end
      end
    end

    describe 'answer bot plan' do
      let(:answer_bot_plan) { @presenter.answer_bot_plan(answer_bot_subscription) }

      describe 'no answer bot subscription' do
        let(:answer_bot_subscription) { nil }

        it 'presents a default product to allow purchase of ABot without trial' do
          expected = {
            max_agents:         0,
            plan_type:          1,
            trial:              false,
            trial_expires_on:   nil,
            days_left_in_trial: -1
          }

          assert_equal expected, answer_bot_plan
        end
      end

      describe 'trial or subscribed answer bot product' do
        let(:answer_bot_subscription) do
          stub(
            trial_expires_at: nil,
            max_resolutions:  500,
            plan_type:        1,
            trial?:           false
          )
        end

        it 'presents the answer bot subscription' do
          expected = {
            max_agents:         500,
            plan_type:          1,
            trial:              false,
            trial_expires_on:   nil,
            days_left_in_trial: -1
          }

          assert_equal expected, answer_bot_plan
        end
      end
    end

    describe 'outbound plan' do
      let(:outbound_plan) { @presenter.outbound_plan(outbound_subscription) }

      describe 'no outbound subscription' do
        let(:outbound_subscription) { nil }

        it 'presents a default product to allow purchase of outbound without trial' do
          expected = {}

          assert_equal expected, outbound_plan
        end
      end

      describe 'passive outbound subscription' do
        let(:outbound_subscription) do
          stub(active?: false)
        end

        it 'presents a default product to allow purchase of outbound without trial' do
          expected = {}

          assert_equal expected, outbound_plan
        end
      end

      describe 'trial or subscribed outbound product' do
        let(:outbound_subscription) do
          stub(
            active?:                true,
            trial_expires_at:       nil,
            monthly_messaged_users: 500,
            plan_type:              1,
            trial?:                 false
          )
        end

        it 'presents the outbound subscription' do
          expected = {
            max_agents:         500,
            plan_type:          1,
            plan_name:          'Professional',
            trial:              false,
            trial_expires_on:   nil,
            days_left_in_trial: -1
          }

          assert_equal expected, outbound_plan
        end
      end
    end

    describe 'explore plan' do
      let(:explore_plan) do
        @presenter.explore_plan(explore_subscription, @account.subscription)
      end

      describe 'explore product exists' do
        describe 'and is in trial' do
          let(:trial_ends_at) { 1.month.from_now }

          let(:explore_subscription) do
            stub(
              trial?:           true,
              subscribed?:      false,
              cancelled?:       false,
              expired?:         false,
              trial_expires_at: trial_ends_at,
              max_agents:       nil,
              plan_type:        1
            )
          end

          it 'presents the explore trial with max agents equal to Support' do
            expected = {
              max_agents:         @account.subscription.max_agents,
              plan_type:          1,
              plan_name:          'Professional',
              plan_weight:        1,
              trial:              true,
              trial_expires_on:   trial_ends_at,
              days_left_in_trial: 31,
              purchasable:        true
            }

            assert_equal expected, explore_plan
          end
        end

        describe 'and is subscribed' do
          let(:explore_subscription) do
            stub(
              trial?:           false,
              subscribed?:      true,
              trial_expires_at: nil,
              max_agents:       5,
              plan_type:        1
            )
          end

          it 'presents the explore subscription' do
            expected = {
              max_agents:         5,
              plan_type:          1,
              plan_name:          'Professional',
              plan_weight:        1,
              trial:              false,
              trial_expires_on:   nil,
              days_left_in_trial: -1,
              purchasable:        false
            }

            assert_equal expected, explore_plan
          end
        end

        describe 'but is expired trial' do
          let(:explore_subscription) do
            stub(
              trial?:           false,
              subscribed?:      false,
              expired?:         true,
              cancelled?:       false,
              trial_expires_at: nil,
              max_agents:       nil,
              plan_type:        1
            )
          end

          it 'presents an empty hash' do
            expected = {}

            assert_equal expected, explore_plan
          end

          describe 'and explore_purchasable side-load is included' do
            before do
              @presenter.stubs(:side_load?).
                with(:explore_purchasable).
                returns(true)
            end

            it 'presents purchasable configuration matching Support agents' do
              expected = {
                max_agents:         @account.subscription.max_agents,
                plan_type:          1,
                plan_name:          'Professional',
                plan_weight:        1,
                trial:              false,
                trial_expires_on:   nil,
                days_left_in_trial: -1,
                purchasable:        true
              }

              assert_equal expected, explore_plan
            end
          end
        end

        describe 'but is cancelled' do
          let(:explore_subscription) do
            stub(
              trial?:           false,
              subscribed?:      false,
              expired?:         false,
              cancelled?:       true,
              trial_expires_at: nil,
              max_agents:       5,
              plan_type:        1
            )
          end

          it 'presents an empty hash' do
            expected = {}

            assert_equal expected, explore_plan
          end

          describe 'and explore_purchasable side-load is included' do
            before do
              @presenter.stubs(:side_load?).
                with(:explore_purchasable).
                returns(true)
            end

            it 'presents purchasable configuration matching support agents' do
              expected = {
                max_agents:         @account.subscription.max_agents,
                plan_type:          1,
                plan_name:          'Professional',
                plan_weight:        1,
                trial:              false,
                trial_expires_on:   nil,
                days_left_in_trial: -1,
                purchasable:        true
              }

              assert_equal expected, explore_plan
            end
          end
        end
      end

      describe 'explore product does not exist' do
        let(:explore_subscription) { nil }

        it 'presents an empty hash' do
          expected = {}
          assert_equal expected, explore_plan
        end

        describe 'but explore_purchasable side-load is included' do
          before do
            @presenter.stubs(:side_load?).
              with(:explore_purchasable).
              returns(true)
          end

          # even if side load is included, don't return data if trial was never started
          it 'presents an empty hash' do
            expected = {}
            assert_equal expected, explore_plan
          end
        end
      end
    end
  end

  describe 'with side loads' do
    let(:zopim_subscription) do
      stub(
        max_agents:        1,
        present_plan_type: 'basic',
        trial:             false,
        trial_expires_on:  nil,
        expired_trial?:    false,
        is_active?:        true,
        is_trial?:         false,
        present_plan_name: 'Basic',
        plan_type:         'basic',
        is_legacy_chat?:   true
      )
    end

    let(:answer_bot_subscription) { nil }
    let(:outbound_subscription)   { nil }
    let(:explore_subscription)    { nil }

    def self.should_side_load_all
      should_side_load :pricing,
        :zopim_pricing,
        :voice_pricing,
        :guide_pricing,
        :tpe_pricing,
        :credit_card,
        :voice,
        :partner_settings,
        :remote_authentications,
        :zopim_integration,
        :voice_recharge_settings,
        :multiproduct
    end

    before do
      @plan = stub(name: 'small')
      # TODO: clean this up after 2017 Chat P&P Update goes GA
      # See https://zendesk.atlassian.net/browse/BILL-3716
      @presenter.stubs(:subscription_pricing_presenter).returns(stub(model_json: {}))
      @presenter.expects(:subscription_pricing).with(@model).returns(stub)
      @presenter.expects(:guide_pricing).returns(stub(guide_plans_from_zuora: {}))
      @presenter.expects(:voice_pricing).with(1).returns(stub(voice_plans_from_zuora: {}))
      @presenter.expects(:zopim_pricing).with(1).returns(stub(zopim_plans_from_zuora: {}))
      @presenter.expects(:tpe_pricing).returns(stub(tpe_plans_from_zuora: {}))

      @model.stubs(:zopim_subscription).returns(zopim_subscription)

      AnswerBot::Subscription.
        stubs(:find_for_account).
        returns(answer_bot_subscription)

      Outbound::Subscription.
        stubs(:find_for_account).
        returns(outbound_subscription)
    end

    should_side_load_all

    describe 'with explore_billing arturo' do
      let(:insights_val) { nil }

      before do
        Arturo.enable_feature!(:explore_billing)
        Explore::Subscription.
          expects(:find_for_account).
          returns(explore_subscription)
        @account.stubs(:account_service_client).returns(
          stub(
            products: [
              stub(name: 'support', plan_settings: {'insights' => insights_val})
            ]
          )
        )

        @presenter.expects(:explore_pricing).with(1).returns(stub(explore_plans_from_zuora: {}))
      end

      # should_side_load_all + explore_pricing
      should_side_load :pricing,
        :zopim_pricing,
        :voice_pricing,
        :guide_pricing,
        :explore_pricing,
        :tpe_pricing,
        :credit_card,
        :voice,
        :partner_settings,
        :remote_authentications,
        :zopim_integration,
        :voice_recharge_settings,
        :explore_access,
        :multiproduct

      describe 'explore_access side load' do
        before do
          @presenter.stubs(:side_load?).returns(true)
          @account.stubs(:eligible_for_explore_lite?).returns(true)
        end

        it 'presents explore_lite_available' do
          assert @presenter.present(@model)[:explore_access][:explore_lite_available]
        end
      end
    end

    describe 'without explore_billing arturo' do
      before do
        Arturo.disable_feature!(:explore_billing)
        Explore::Subscription.expects(:find_for_account).never
      end

      should_side_load_all
    end
  end

  describe 'guide_pricing' do
    let(:guide_pricing) do
      [
        {
          plan_type:                             1, # 'lite'
          currency:                              'USD',
          per_agent_cost_with_monthly_billing:   1,
          per_agent_cost_with_quarterly_billing: 4,
          per_agent_cost_with_annual_billing:    12
        },
        {
          plan_type:                             2, # 'professional'
          currency:                              'USD',
          per_agent_cost_with_monthly_billing:   1,
          per_agent_cost_with_quarterly_billing: 4,
          per_agent_cost_with_annual_billing:    12
        },
        {
          plan_type:                             4, # 'enterprise'
          currency:                              'USD',
          per_agent_cost_with_monthly_billing:   1,
          per_agent_cost_with_quarterly_billing: 4,
          per_agent_cost_with_annual_billing:    12
        }
      ]
    end

    let(:guide_subscription) do
      FactoryBot.build(:guide_subscription, account: nil)
    end

    before do
      @presenter.stubs(:side_load?).with(:guide_pricing).returns(true)
      @presenter.expects(:guide_pricing).at_least(1).returns(stub(guide_plans_from_zuora: guide_pricing))

      @presenter.stubs(:side_load?).with(:pricing).returns(false)
      @presenter.stubs(:side_load?).with(:eaa_pricing).returns(false)
      @presenter.stubs(:side_load?).with(:voice_pricing).returns(false)
      @presenter.stubs(:side_load?).with(:voice).returns(false)
      @presenter.stubs(:side_load?).with(:zopim_pricing).returns(false)
      @presenter.stubs(:side_load?).with(:tpe_pricing).returns(false)
      @presenter.stubs(:side_load?).with(:zopim_pricing).returns(false)
      @presenter.stubs(:side_load?).with(:explore_pricing).returns(false)
      @presenter.stubs(:side_load?).with(:credit_card).returns(false)
      @presenter.stubs(:side_load?).with(:partner_settings).returns(false)
      @presenter.stubs(:side_load?).with(:remote_authentications).returns(false)
      @presenter.stubs(:side_load?).with(:zopim_integration).returns(false)
      @presenter.stubs(:side_load?).with(:voice_recharge_settings).returns(false)
      @presenter.stubs(:side_load?).with(:explore_access).returns(false)
      @presenter.stubs(:side_load?).with(:multiproduct).returns(false)

      @model.stubs(:guide_preview).returns(guide_subscription)
    end

    def includes_plan_type?(plan_type)
      @presenter.side_loads(@model)[:guide_pricing][:guide_available_plans].any? do |plan|
        plan[:plan_type] == plan_type.plan_type
      end
    end

    describe 'with guide_enterprise_billing arturo' do
      it 'includes enterprise plan' do
        assert includes_plan_type?(ZBC::Guide::PlanType::Enterprise)
      end

      describe 'and is Guide legacy' do
        let(:guide_subscription) do
          FactoryBot.build(:guide_subscription, account: nil, legacy: true)
        end

        it 'does not include Lite or Professional' do
          refute includes_plan_type?(ZBC::Guide::PlanType::Lite)
          refute includes_plan_type?(ZBC::Guide::PlanType::Professional)
        end
      end

      describe 'and is NOT Guide legacy' do
        let(:guide_subscription) do
          FactoryBot.build(:guide_subscription, account: nil, legacy: false)
        end

        it 'includes Lite and Professional' do
          assert includes_plan_type?(ZBC::Guide::PlanType::Lite)
          assert includes_plan_type?(ZBC::Guide::PlanType::Professional)
        end
      end

      describe 'and does not have Guide product' do
        before do
          @model.stubs(:guide_preview) # guide_preview returns nil
        end

        it 'includes all plans (including Enterprise)' do
          assert includes_plan_type?(ZBC::Guide::PlanType::Lite)
          assert includes_plan_type?(ZBC::Guide::PlanType::Professional)
          assert includes_plan_type?(ZBC::Guide::PlanType::Enterprise)
        end
      end
    end
  end

  describe 'product pricing' do
    describe 'zopim_pricing' do
      it 'uses ZopimSubscriptionPresenter' do
        ZBC::Zuora::ZopimSubscriptionPricing.expects(:new).with(1)
        @presenter.send(:zopim_pricing, 1)
      end
    end

    describe 'explore_pricing' do
      it 'uses ExploreSubscriptionPresenter' do
        ZBC::Zuora::ExploreSubscriptionPricing.expects(:new).with(1)
        @presenter.send(:explore_pricing, 1)
      end
    end
  end

  describe 'suite_allowed' do
    describe 'when suite_allowed is true' do
      let(:suite_allowed) { true }

      should_present(true, as: :suite_allowed)
    end

    describe 'when suite_allowed is false' do
      let(:suite_allowed) { false }

      should_present(false, as: :suite_allowed)
    end
  end
end
