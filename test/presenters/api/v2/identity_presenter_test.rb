require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::IdentityPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users, :accounts, :user_identities

  def self.should_present_url
    it "presents url using api_v2_user_identity_url" do
      expected_url = "stub_api_v2_user_identity_url"
      @presenter.url_builder.expects(:api_v2_user_identity_url).with(@model.user_id, @model, has_entry(format: :json)).returns(expected_url)
      assert_equal expected_url, @presenter.model_json(@model)[:url]
    end
  end

  describe "for an email identity" do
    let(:user_identity) { user_identities(:minimum_end_user) }

    before do
      @model = user_identity
      @presenter = Api::V2::IdentityPresenter.new(@model, url_builder: mock_url_builder)
    end

    should_present_keys :id, :user_id, :type, :value, :verified, :primary, :created_at, :updated_at, :url,
      :undeliverable_count, :deliverable_state

    should_present_method :id
    should_present_method :user_id
    should_present_method :value
    should_present_method :is_verified, as: :verified
    should_present_method :primary?, as: :primary
    should_present_method :identity_type, as: :type
    should_present_method :undeliverable_count
    should_present_method :created_at
    should_present_method :updated_at

    should_present_url

    it "presents anonymous users identity url as nil" do
      @model.id = nil
      @presenter.model_json(@model).fetch(:url).must_be_nil
    end

    describe 'having an identity with an invalid deliverable_state' do
      before { user_identity.stubs(deliverable_state: nil) }

      # [Rollbar-35040] https://rollbar-eu.zendesk.com/Zendesk/Classic/items/35040
      it "doesn't raise an exception" do
        @presenter.model_json(@model).fetch(:deliverable_state)
      end
    end
  end

  describe "for a twitter identity" do
    before do
      @model     = user_identities(:minimum_end_user_twitter)
      @presenter = Api::V2::IdentityPresenter.new(@model, url_builder: mock_url_builder)
    end

    should_present_keys :id, :user_id, :type, :value, :verified, :primary, :created_at, :updated_at, :url

    should_present_method :id
    should_present_method :user_id
    should_present_method :screen_name, as: :value
    should_present_method :is_verified, as: :verified
    should_present_method :primary?, as: :primary
    should_present_method :identity_type, as: :type
    should_present_method :created_at
    should_present_method :updated_at

    should_present_url
  end

  describe "for an AnyChannel identity" do
    before do
      @model     = user_identities(:minimum_end_user_any_channel)
      @presenter = Api::V2::IdentityPresenter.new(@model, url_builder: mock_url_builder)
      ris = stub('RegisteredIntegrationService', name: 'test name')
      ::Channels::AnyChannel::RegisteredIntegrationService.stubs(:find).with('1').returns(ris)
    end

    should_present_keys :id, :user_id, :type, :subtype_name, :value, :verified, :primary, :created_at, :updated_at, :url

    should_present_method :id
    should_present_method :user_id
    should_present_method :screen_name, as: :value
    should_present_method :is_verified, as: :verified
    should_present_method :primary?, as: :primary
    should_present_method :identity_type, as: :type
    should_present_method :created_at
    should_present_method :updated_at

    should_present_url
  end

  describe "for a phone number identity" do
    before do
      @user = users(:minimum_end_user)
      @model = UserPhoneNumberIdentity.new(user: @user, account: @user.account)
      @model.value = '+353891234567'
      @presenter = Api::V2::IdentityPresenter.new(@model, url_builder: mock_url_builder)
    end

    should_present_keys :id, :user_id, :type, :value, :url, :verified, :primary, :created_at, :updated_at

    should_present_method :id
    should_present_method :user_id
    should_present_method :is_verified, as: :verified
    should_present_method :primary?, as: :primary
    should_present_method :identity_type, as: :type
    should_present_method :created_at
    should_present_method :updated_at

    # because should_present_method :value did not work.
    it 'should present value' do
      @model.value = '+353891234567'
      @model.save!
      @presenter.model_json(@model).fetch(:value).must_equal '+353891234567'
    end

    describe 'when account settings is enabled' do
      it "presents phone number with extension" do
        Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
        @model.value = '+353891234567x123'
        @model.save!
        @presenter.model_json(@model).fetch(:value).must_equal '+353891234567x123'
      end
    end

    describe "when voice_formatted_phone_numbers is enabled" do
      before do
        @model.account.stubs(has_voice_formatted_phone_numbers?: true)
      end

      it 'presents :formatted_phone_number' do
        @presenter.model_json(@model).fetch(:formatted_value).must_equal '+353 89 123 4567'
      end
    end
  end
end
