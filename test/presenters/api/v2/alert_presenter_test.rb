require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AlertPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  before do
    @model     = Alert.create!(value: "FOO", link_url: "LINK")
    @presenter = Api::V2::AlertPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder)
  end

  should_present_keys :id, :value, :link_url, :interfaces, :created_at, :updated_at
end
