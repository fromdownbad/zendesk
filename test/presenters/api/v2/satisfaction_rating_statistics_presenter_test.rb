require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SatisfactionRatingStatisticsPresenter do
  fixtures :accounts

  let(:filters) { {} }

  before do
    @account = accounts(:minimum)

    Satisfaction::Rating.stubs(:positive_rated_ticket_count).with(@account, filters).returns(1)
    Satisfaction::Rating.stubs(:negative_rated_ticket_count).with(@account, filters).returns(2)
    Satisfaction::Rating.stubs(:surveys_sent).with(@account, filters).returns(3)
    Satisfaction::Rating.stubs(:response_rate).with(@account, filters).returns(4)
    Satisfaction::Rating.stubs(:current_score).with(@account, filters).returns(5)

    @presenter = Api::V2::SatisfactionRatingStatisticsPresenter.new(filters)
  end

  describe '#model_json' do
    let(:expected) do
      {
        good_ratings: 1,
        bad_ratings: 2,
        surveys_sent: 3,
        response_rate: 4,
        score: 5
      }
    end

    it 'returns expected value' do
      assert_equal expected, @presenter.model_json(@account)
    end

    describe 'with filters' do
      let(:filters) { { start_time: 2.days.ago, end_time: 1.day.ago } }

      it 'returns expected value' do
        assert_equal expected, @presenter.model_json(@account)
      end
    end
  end
end
