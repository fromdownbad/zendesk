require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TicketFieldPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :ticket_fields, :translation_locales, :custom_field_options

  before do
    @model     = ticket_fields(:support_field_subject)
    @user      = @model.account.owner
    @account   = @model.account
    @presenter = Api::V2::TicketFieldPresenter.new(@user, url_builder: mock_url_builder)
  end

  should_present_keys :id, :url, :type, :title, :raw_title, :agent_description, :description, :raw_description, :position, :active, :required, :collapsed_for_agents,
    :regexp_for_validation, :title_in_portal, :raw_title_in_portal, :visible_in_portal, :editable_in_portal,
    :required_in_portal, :tag, :created_at, :updated_at, :removable

  should_present_method :id
  should_present_method :title
  should_present_method :description
  should_present_method :position
  should_present_method :is_active?, as: :active
  should_present_method :is_required?, as: :required
  should_present_method :is_collapsed_for_agents?, as: :collapsed_for_agents
  should_present_method :regexp_for_validation
  should_present_method :title_in_portal
  should_present_method :is_visible_in_portal?, as: :visible_in_portal
  should_present_method :is_editable_in_portal?, as: :editable_in_portal
  should_present_method :is_required_in_portal?, as: :required_in_portal
  should_present_method :tag
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :is_removable?, as: :removable

  should_present "subject", as: :type

  should_present_url :api_v2_ticket_field_url

  describe "Translation" do
    before do
      @spanish = translation_locales(:spanish)
    end

    describe "without the 'locale' option" do
      it "attempts to translate the ticket field into the user's locale" do
        @model.expects(:multilingual_field).
          with(user_locale: @user.translation_locale, account_locale: @account.translation_locale, field_type: :title).
          returns("translated title")

        @model.expects(:multilingual_field).
          with(user_locale: @user.translation_locale, account_locale: @account.translation_locale, field_type: :description).
          returns("translated description")

        @model.expects(:multilingual_field).
          with(user_locale: @user.translation_locale, account_locale: @account.translation_locale, field_type: :title_in_portal).
          returns("translated title in portal")

        output = @presenter.model_json(@model)
        assert_not_equal @user.translation_locale, @spanish
        assert_equal "translated title", output[:title]
        assert_equal "translated description", output[:description]
        assert_equal "translated title in portal", output[:title_in_portal]
      end
    end

    describe "with the 'locale' option" do
      before do
        @presenter = Api::V2::TicketFieldPresenter.new(@user, url_builder: mock_url_builder, locale: @spanish)
      end

      it "attempts to translate the ticket field with the provided locale" do
        @model.expects(:multilingual_field).
          with(user_locale: @spanish, account_locale: @account.translation_locale, field_type: :title).
          returns("spanish translated title")

        @model.expects(:multilingual_field).
          with(user_locale: @spanish, account_locale: @account.translation_locale, field_type: :description).
          returns("spanish translated description")

        @model.expects(:multilingual_field).
          with(user_locale: @spanish, account_locale: @account.translation_locale, field_type: :title_in_portal).
          returns("spanish translated title in portal")

        output = @presenter.model_json(@model)
        assert_not_equal @user.translation_locale, @spanish
        assert_equal "spanish translated title", output[:title]
        assert_equal "spanish translated description", output[:description]
        assert_equal "spanish translated title in portal", output[:title_in_portal]
      end
    end
  end

  describe "when user_locale evaluates to nil" do
    before { User.any_instance.stubs(:translation_locale).returns(nil) }

    it "does not blow up" do
      @presenter.model_json(@model)
    end

    it "yields stored values for title, title_in_portal, and description" do
      output = @presenter.model_json(@model)
      assert_equal ["Subject"], [output[:title], output[:title_in_portal], output[:description]].uniq
    end
  end

  describe "#association_preloads" do
    it "is blank" do
      assert_equal({}, @presenter.association_preloads)
    end
  end

  describe "ticket_field tagger" do
    before do
      @model = ticket_fields(:field_tagger_custom)
      fallback_locale = translation_locales(:english_by_zendesk)
      fbattrs = { is_fallback: true, nested: true, value: "Hellooo dynamic wombat!", translation_locale_id: fallback_locale.id }
      @cms_text = Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: fbattrs, account: @model.account)
      @fallback_variant = @cms_text.fallback
      @presenter = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder)
      @json      = @presenter.model_json(@model)
    end

    it "presents custom field options of tagger" do
      assert_not_nil @json[:custom_field_options]
      assert_equal @model.custom_field_options.size, @json[:custom_field_options].size
    end

    it "presents drop down choice ids" do
      @json[:custom_field_options].zip(@model.custom_field_options) do |json_field_option, model_field_option|
        assert_equal model_field_option.id, json_field_option[:id]
      end
    end

    it 'gets the updated dynamic content of a custom field option is changed' do
      dc_cfo = @json[:custom_field_options].detect { |cfo| cfo[:raw_name] == "{{dc.welcome_wombat}}" }
      assert_equal "Hellooo dynamic wombat!", dc_cfo[:name]

      # Change the translated value of the cms text
      @fallback_variant.update_attributes!(value: "こんにちわ ワムバト！")
      @cms_text.reload
      @model.account.reload # you can't update and render a dynamic content variant in the same request, so simulate a new request with a new account and user object

      assert_equal "こんにちわ ワムバト！", @cms_text.fallback.value

      new_json = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder).model_json(@model)
      new_dc_cfo = new_json[:custom_field_options].detect { |cfo| cfo[:raw_name] == "{{dc.welcome_wombat}}" }
      assert_equal "こんにちわ ワムバト！", new_dc_cfo[:name]
    end

    describe_with_arturo_enabled :special_chars_in_custom_field_options do
      it 'presents the enhanced_value when enabled' do
        # create an option that has a nil value and non-nil enhanced_value
        params = { custom_field_options: [{ name: "Penguin Party", value: 'booring' }] }

        tf_manager = Zendesk::Tickets::TicketFieldManager.new(@model.account)
        tf_manager.update(@model, params)
        @model.save
        # With the arturo enabled, value should be set to nil, and only enhanced value will be set
        # via custom_field_option.rb in value= setter method
        assert_nil @model.custom_field_options.detect { |option| option[:value] == "booring" }
        # Now check the presented value
        new_json = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder).model_json(@model)
        assert_not_nil new_json[:custom_field_options].detect { |option| option[:value] == "booring" }
      end
    end

    describe_with_arturo_enabled :custom_field_options_caching do
      before { @json = @presenter.model_json(@model) }
      it 'stores json for custom field options in the cache' do
        data = Rails.cache.read(@presenter.custom_field_options_cache_key(@model))
        assert data
        assert_equal @json[:custom_field_options], data
      end

      it 'expires cache and updates if the dynamic content of a custom field option is changed' do
        dc_cfo = @json[:custom_field_options].detect { |cfo| cfo[:raw_name] == "{{dc.welcome_wombat}}" }
        assert_equal "Hellooo dynamic wombat!", dc_cfo[:name]

        # Change the translated value of the cms text
        @fallback_variant.update_attributes!(value: "こんにちわ ワムバト！")
        @cms_text.reload
        @model.account.reload # you can't update and render a dynamic content variant in the same request, so simulate a new request with a new account and user object

        # Call again and see if we get the updated data. Note the usage of a new presenter
        # instance here because DynamicContent manager has it's own cache that will memoize the dc values
        new_json = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder).model_json(@model)
        new_dc_cfo = new_json[:custom_field_options].detect { |cfo| cfo[:raw_name] == "{{dc.welcome_wombat}}" }
        assert_equal "こんにちわ ワムバト！", new_dc_cfo[:name]
      end

      it 'expires cache and updates if the content of a custom field option is changed' do
        params = { custom_field_options: [{ value: "booring", name: "Penguin Party" }] }

        # Change the name of one of the custom field options belonging to the ticket field
        tf_manager = Zendesk::Tickets::TicketFieldManager.new(@model.account)
        tf_manager.update(@model, params)
        @model.save
        assert_equal "Penguin Party", @model.custom_field_options.reload.find_by_value("booring").name

        # Call again and see if we get the updated data.
        new_json = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder).model_json(@model)
        updated_cfo = new_json[:custom_field_options].detect { |option| option[:value] == "booring" }
        assert_equal "Penguin Party", updated_cfo[:name]
      end
    end

    describe_with_arturo_disabled :custom_field_options_caching do
      before { @json = @presenter.model_json(@model) }
      it 'does not use the cache for custom field options if arturo is disabled' do
        Rails.expects(:cache).never
        @presenter.model_json(@model)
      end

      it 'gets the updated dynamic content if the content of a variant is changed' do
        dc_cfo = @json[:custom_field_options].detect { |cfo| cfo[:raw_name] == "{{dc.welcome_wombat}}" }
        assert_equal "Hellooo dynamic wombat!", dc_cfo[:name]

        # Change the translated value of the cms text
        @fallback_variant.update_attributes!(value: "こんにちわ ワムバト！")
        @cms_text.reload
        @model.account.reload # you can't update and render a dynamic content variant in the same request, so simulate a new request with a new account and user object

        new_json = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder).model_json(@model)
        new_dc_cfo = new_json[:custom_field_options].detect { |cfo| cfo[:raw_name] == "{{dc.welcome_wombat}}" }
        assert_equal "こんにちわ ワムバト！", new_dc_cfo[:name]
      end
    end
  end

  describe "system ticket_field with options" do
    before do
      @account   = accounts(:minimum)
      @presenter = Api::V2::TicketFieldPresenter.new(@account.owner, account: @account, url_builder: mock_url_builder)
    end

    it "presents system field options of type" do
      model = ticket_fields(:minimum_field_ticket_type)
      json = @presenter.model_json(model)
      ticket_types_for_account = @presenter.get_ticket_types_for(@account)
      assert_not_nil json[:system_field_options]
      assert_equal ticket_types_for_account.collect(&:first), json[:system_field_options].collect { |f| f[:name] }
    end

    it "presents system field options of priority" do
      model = ticket_fields(:minimum_field_priority)
      json = @presenter.model_json(model)
      priorities_for_account = @presenter.get_priorities_for(@account)
      assert_not_nil json[:system_field_options]
      assert_equal priorities_for_account.collect(&:first), json[:system_field_options].collect { |f| f[:name] }
    end

    it "presents system field options of status" do
      model = ticket_fields(:minimum_field_status)
      json = @presenter.model_json(model)
      status_for_account = @presenter.get_status_for(@account)
      assert_not_nil json[:system_field_options]
      assert_equal status_for_account.collect(&:first), json[:system_field_options].collect { |f| f[:name] }
    end
  end

  it "presents the sub type of the priority field" do
    @model.stubs(:type).returns("FieldPriority")
    @model.stubs(:sub_type_id).returns(PrioritySet.BASIC)
    json = @presenter.model_json(@model)
    assert_equal "basic_priority", json[:type]

    @model.stubs(:sub_type_id).returns(PrioritySet.FULL)
    json = @presenter.model_json(@model)
    assert_equal "priority", json[:type]
  end

  describe "dynamic content in ticket fields" do
    before do
      @portuguese     = translation_locales(:brazilian_portuguese)
      @english        = translation_locales(:english_by_zendesk)
      @spanish        = translation_locales(:spanish)

      @model = ticket_fields(:field_with_dc)
      I18n.stubs(:translation_locale).returns(@portuguese)

      # Dynamic Content
      @fbattrs        = { is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: @english.id }
      @cms_params     = { account: @model.account, name: "Welcome Wombat", fallback_attributes: @fbattrs}
      @cms_text       = Cms::Text.create!(@cms_params)

      # Dynamic Content Variants
      @cms_portuguese_variant = @cms_text.variants.create!(active: true, translation_locale_id: @portuguese.id, value: "The portuguese value")
      @cms_spanish_variant = @cms_text.variants.create!(active: true, translation_locale_id: @spanish.id, value: "The spanish value")
    end

    describe "without the 'locale' option" do
      before do
        @presenter = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder)
      end

      it "renders the dynamic content using the locale in I18n.translation_locale" do
        json = @presenter.model_json(@model)
        assert_equal "The portuguese value", json[:title]
        assert_equal "The portuguese value", json[:description]
        assert_equal "The portuguese value", json[:title_in_portal]
      end
    end

    describe "with the 'locale' option" do
      before do
        @presenter = Api::V2::TicketFieldPresenter.new(@model.account.owner, url_builder: mock_url_builder, locale: @spanish)
      end

      it "renders the dynamic content using with the provided locale" do
        json = @presenter.model_json(@model)
        assert_equal "The spanish value", json[:title]
        assert_equal "The spanish value", json[:description]
        assert_equal "The spanish value", json[:title_in_portal]
      end
    end
  end

  describe "#agent_description" do
    before do
      @mmodel = Zendesk::Tickets::TicketFieldManager.new(@account).build(title: "Wombat Ticket Field", agent_description: "")
      @mmodel.save
    end

    it "should present the agent_description key" do
      output = @presenter.model_json(@mmodel)
      assert output.key?(:agent_description)
    end

    it "should present nil if the agent_description is blank" do
      output = @presenter.model_json(@mmodel)
      assert_nil output[:agent_description]
    end
  end

  describe "#creator_user_id" do
    describe "with the creator option passed into the presenter" do
      before { @presenter = Api::V2::TicketFieldPresenter.new(@user, creator: true, url_builder: mock_url_builder) }

      it "presents the creator_user_id and creator_app_id keys if the creator option is passed to the presenter" do
        json = @presenter.model_json(@model)
        assert json.key?(:creator_user_id)
        assert json.key?(:creator_app_name)
      end

      describe "with includes users and creator option was passed to the presenter" do
        before do
          @presenter = Api::V2::TicketFieldPresenter.new(@user, creator: true, url_builder: mock_url_builder, includes: [:users])
        end

        should_side_load :users
      end

      it "returns the id of the zendesk user who created the ticket field" do
        agent = users(:minimum_agent)
        ticket_field = TicketField.new(title: 'Wombat', account: agent.account)
        ticket_field.type = "FieldTagger"
        CIA.audit(actor: agent, effective_actor: agent) do
          ticket_field.save!
        end

        output = @presenter.model_json(ticket_field)
        assert_equal agent.id, output[:creator_user_id]
      end

      it "noel returns the id of the system user if it's a system field" do
        system_field = ticket_fields(:support_field_subject)

        output = @presenter.model_json(system_field)
        assert_equal User.system_user_id, output[:creator_user_id]
      end

      it "returns nil if there is no audit event for the creation of the ticket field" do
        old_field = ticket_fields(:field_decimal_custom)
        output = @presenter.model_json(old_field)
        assert_nil output[:creator_user_id]
      end

      it 'does not perform N+1 requests' do
        agent = users(:minimum_agent)
        ticket_field = TicketField.new(title: 'Wombat', account: agent.account)
        ticket_field.type = "FieldTagger"
        CIA.audit(actor: agent, effective_actor: agent) do
          ticket_field.save!
        end
        assert_no_n_plus_one do
          @presenter.present(ticket_field)
        end
      end
    end

    describe "without the creator option passed to the presenter" do
      it "does not present the creator_user_id and creator_app_id keys" do
        json = @presenter.model_json(@model)
        refute json.key?(:creator_user_id)
        refute json.key?(:creator_app_name)
      end

      describe "with includes users" do
        before do
          @presenter = Api::V2::TicketFieldPresenter.new(@user, url_builder: mock_url_builder, includes: [:users])
        end

        it "does not sideload users" do
          @presenter.expects(:side_load?).with(:users).never
        end
      end
    end
  end
end
