require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Channels::TicketPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :tickets, :users

  before do
    @presenter = Api::V2::Channels::TicketPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
    @model     = tickets(:minimum_1)
  end

  should_present_keys :id, :nice_id, :status, :url

  should_present_method :id
  should_present_method :nice_id

  it 'presents status' do
    assert_equal 'new', @presenter.present(@model)[:ticket][:status]
  end
end
