require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Channels::CommentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :tickets, :events

  before do
    @presenter = Api::V2::Channels::CommentPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
    @model     = events(:create_comment_for_minimum_ticket_1)
  end

  should_present_keys :id, :nice_id, :status, :comment_id, :url

  should_present_method :id, as: :comment_id

  it 'presents ticket data' do
    json = @presenter.present(@model)[:ticket]
    ticket = @model.ticket
    assert_equal ticket.id, json[:id]
    assert_equal ticket.nice_id, json[:nice_id]
    assert_equal 'new', json[:status]
  end
end
