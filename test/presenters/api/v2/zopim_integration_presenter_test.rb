require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ZopimIntegrationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:model) { OpenStruct.new }

  before do
    @model     = model
    @account   = accounts(:minimum)
    @presenter = Api::V2::ZopimIntegrationPresenter.new(
      @account.owner, url_builder: mock_url_builder
    )
  end

  should_present_keys :external_zopim_id,
    :zopim_key,
    :phase,
    :agent_sync,
    :billing,
    :created_at,
    :updated_at

  should_side_load :app_market_integration
end
