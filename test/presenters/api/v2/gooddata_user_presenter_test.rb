require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::GooddataUserPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  let(:last_successful_process) { stub }

  before do
    @account = accounts(:minimum)
    @agent   = users(:minimum_agent)

    @gooddata_integration = @account.create_gooddata_integration(
      project_id: 'test_project_id',
      status: 'complete',
      version: 2
    )

    GooddataIntegration.stubs(:tour_project_sso_url).
      returns('https://tour_project_sso_url.com')

    GooddataIntegration.any_instance.stubs(:sso_dashboards).
      returns('sso_dashboards')

    @model = GooddataUser.create! do |u|
      u.account = @account
      u.user = @agent
      u.gooddata_project_id = 'gooddata_project_id'
      u.gooddata_user_id = 'gooddata_user_id'
    end

    @presenter = Api::V2::GooddataUserPresenter.new(@agent,
      url_builder: mock_url_builder,
      last_successful_process: last_successful_process)
  end

  should_present_keys :id, :account_id, :user_id, :gooddata_user_id,
    :gooddata_project_id, :created_at, :updated_at, :integration, :url

  should_present_method :id
  should_present_method :account_id
  should_present_method :user_id
  should_present_method :gooddata_user_id
  should_present_method :gooddata_project_id
  should_present_method :created_at
  should_present_method :updated_at

  describe "tour url" do
    before do
      Account.any_instance.stubs(:has_gooddata_advanced_analytics?).returns(true)
      @account.gooddata_integration.soft_delete
      @account.reload
      @account.create_gooddata_integration(
        project_id: 'test_project_id',
        status: 'pending',
        version: 2
      )
    end

    it "always be included" do
      assert_equal(
        'https://tour_project_sso_url.com',
        @presenter.model_json(@model)[:integration][:tour_project_sso_url]
      )
    end
  end

  describe ".integration_status_info" do
    before do
      Account.any_instance.stubs(:has_gooddata_advanced_analytics?).returns(false)
      GooddataIntegration.without_arsi.delete_all
      @account.reload
    end

    describe "account is suspended" do
      before do
        @account = @agent.account
        @account.update_attribute(:is_serviceable, false)
        @account.settings.save!
        @presenter = Api::V2::GooddataUserPresenter.new(@agent, url_builder: stub, last_successful_process: last_successful_process)
      end

      it "should return :account_integration_unsupported" do
        assert_equal :account_integration_unsupported, @presenter.integration_status_info
      end
    end

    describe "Account is Plus or Enterprise" do
      before do
        Account.any_instance.stubs(:has_gooddata_advanced_analytics?).returns(true)
      end

      describe "User is Admin" do
        before do
          @user = users(:minimum_admin)
          @presenter = Api::V2::GooddataUserPresenter.new(@user, url_builder: stub, last_successful_process: last_successful_process)
        end

        describe "GoodData integration does not exist" do
          it "returns :admin_account_not_integrated" do
            assert_equal :admin_account_not_integrated, @presenter.integration_status_info
          end
        end

        describe "GoodData integration exists" do
          before do
            @gooddata_integration = @user.account.create_gooddata_integration(
              project_id: 'gooddata_user_test_1',
              status: 'complete',
              version: 2,
              scheduled_at: "1am"
            )
          end

          describe "Integration is V2" do
            before do
              GooddataIntegration.any_instance.stubs(:version).returns(2)
            end

            describe "Integration is complete" do
              describe "the project has been successfully loaded" do
                describe "GoodData user exists" do
                  let(:sso_dashboards) do
                    [
                      {name: 'dashboard1', sso_url: 'https://sso_url1.com'},
                      {name: 'dashboard2', sso_url: 'https://sso_url2.com'},
                    ]
                  end

                  before do
                    GooddataUser.create! do |u|
                      u.account_id = @user.account.id
                      u.user_id = @user.id
                      u.gooddata_project_id = @user.account.gooddata_integration.project_id
                      u.gooddata_user_id = 'gooddata_user_test_user_1'
                    end

                    @gooddata_integration.stubs(:portal_sso_url).returns('https://portal_url.com')
                    @gooddata_integration.stubs(:sso_dashboards).
                      with(GooddataUser.for_user(@user)).returns(sso_dashboards)
                  end

                  it "returns :fully_provisioned" do
                    user_status, details = @presenter.integration_status_info
                    assert_equal :fully_provisioned, user_status
                    assert_equal 'https://portal_url.com', details[:portal_sso_url]
                    assert_equal sso_dashboards, details[:dashboards]
                    assert_equal "1am", details[:scheduled_at]
                  end
                end

                describe "Current user did not create GooddataIntegration" do
                  it "returns :user_not_provisioned" do
                    assert_equal :user_not_provisioned, @presenter.integration_status_info
                  end
                end

                describe "Current user did create GooddataIntegration" do
                  before do
                    @gooddata_integration.admin_id = @user.id
                    @gooddata_integration.save!
                  end

                  it "returns :admin_user_not_provisioned" do
                    assert_equal :admin_user_not_provisioned, @presenter.integration_status_info
                  end
                end
              end

              describe "the project has not yet been loaded" do
                before do
                  @presenter = Api::V2::GooddataUserPresenter.new(@user, url_builder: stub, last_successful_process: nil)
                end

                it 'returns :account_data_load_in_progress' do
                  assert_equal :account_data_load_in_progress, @presenter.integration_status_info
                end
              end
            end

            describe "Integration is not complete" do
              before do
                @gooddata_integration.status = 'invited'
                @gooddata_integration.save!
              end

              it "returns :account_integration_in_progress" do
                user_status, details = @presenter.integration_status_info
                assert_equal :account_integration_in_progress, user_status
                assert_equal 'invited', details[:account_integration_status]
              end
            end
          end
        end
      end

      describe "User is not Admin" do
        before do
          @user = users(:minimum_end_user)
          Account.any_instance.stubs(:has_gooddata_advanced_analytics?).returns(true)
          @presenter = Api::V2::GooddataUserPresenter.new(@user, url_builder: stub, last_successful_process: last_successful_process)
        end

        describe "No GoodData integration" do
          it "returns :account_not_integrated" do
            assert_equal :account_not_integrated, @presenter.integration_status_info
          end
        end

        describe "GoodData integration is disabled" do
          before do
            @gooddata_integration = @user.account.create_gooddata_integration(
              project_id: 'gooddata_user_test_2',
              status: 'disabled',
              version: 2
            )
          end
          it "returns :account_integration_disabled" do
            assert_equal :account_integration_disabled, @presenter.integration_status_info
          end
        end

        describe "When re-enabling Gooddata integration" do
          before do
            @gooddata_integration = @user.account.create_gooddata_integration(
              project_id: 'gooddata_user_test_2',
              status: 're_enabling',
              version: 2
            )
          end
          it "returns :account_integration_in_progress" do
            assert_equal [:account_integration_in_progress,
                          {account_integration_status: "re_enabling"}], @presenter.integration_status_info
          end
        end

        describe "GoodData integration is complete and V2, user not provisioned" do
          before do
            @gooddata_integration = @user.account.create_gooddata_integration(
              project_id: 'gooddata_user_test_2',
              status: 'complete',
              version: 2
            )
          end

          it "returns :user_not_provisioned" do
            assert_equal :user_not_provisioned, @presenter.integration_status_info
          end
        end
      end
    end

    describe "Account is not Plus or Enterprise" do
      before do
        Account.any_instance.stubs(:has_gooddata_advanced_analytics?).returns(false)
      end

      describe "User is Admin" do
        it "returns :admin_account_integration_unsupported" do
          presenter = Api::V2::GooddataUserPresenter.new(users(:minimum_admin), url_builder: stub)
          assert_equal :admin_account_integration_unsupported, presenter.integration_status_info
        end
      end

      describe "User is not Admin" do
        it "returns :account_integration_unsupported" do
          presenter = Api::V2::GooddataUserPresenter.new(users(:minimum_end_user), url_builder: stub)
          assert_equal :account_integration_unsupported, presenter.integration_status_info
        end
      end
    end
  end
end
