require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::OnboardingTasksPresenter do
  fixtures :users

  let(:tasks) do
    [
      {
        "name" => "task_1",
        "conditions" => {"cond1" => true, "cond2" => false},
        "level" => OnboardingTask::LEVEL::USER
      },
      {
        "name" => "task_2",
        "conditions" => {"cond1" => true, "cond2" => false, "cond3" => false},
        "level" => OnboardingTask::LEVEL::ACCOUNT
      },
      {
        "name" => "task_3",
        "conditions" => {"cond1" => true, "cond2" => true},
        "level" => OnboardingTask::LEVEL::ACCOUNT
      }
    ]
  end

  let (:tasks_params_1) do
    {
      "name" => "task_1",
      "status" => OnboardingTask::CompletionStatus::COMPLETED
    }
  end
  let (:tasks_params_2) do
    {
      "name" => "task_2",
      "status" => OnboardingTask::CompletionStatus::COMPLETED
    }
  end

  let (:type_checklist) { 'checklist' }
  let (:type_setup) { 'setup' }

  before do
    @user = users(:minimum_agent)
    @user.stubs(:onboarding).returns(tasks)
    @other_user = users(:minimum_admin)
    @other_user.stubs(:onboarding).returns(tasks)
    @presenter = Api::V2::OnboardingTasksPresenter.new(@user, url_builder: self)
  end

  describe "zero state table is empty" do
    describe "when the type is checklist" do
      before do
        @onboarding_tasks = @presenter.present(@user.onboarding(type_checklist))[:tasks]
      end

      it "return all tasks" do
        assert_equal 3, @onboarding_tasks.length
      end

      it "return the right keys when present is called" do
        expected_keys = [:changed, :completed, :name]
        keys          = @onboarding_tasks[0].keys.map(&:to_s).sort.map(&:to_sym)
        assert_equal expected_keys, keys
      end

      it "one task should have completed:true" do
        completed_tasks = @onboarding_tasks.select { |task| task["completed"] }
        assert_equal 1, completed_tasks.count
      end

      it "task with failed conditions have changed:false" do
        changed_tasks = @onboarding_tasks.select { |task| !task['changed'] }
        assert_equal 2, changed_tasks.count
      end
    end

    describe "when the type is setup" do
      before do
        @onboarding_tasks = @presenter.present(@user.onboarding(type_setup))[:tasks]
      end

      it "return all tasks" do
        assert_equal 3, @onboarding_tasks.length
      end

      it "return the right keys when present is called" do
        expected_keys = [:changed, :completed, :name]
        keys          = @onboarding_tasks[0].keys.map(&:to_s).sort.map(&:to_sym)
        assert_equal expected_keys, keys
      end

      it "one task should have completed:true" do
        completed_tasks = @onboarding_tasks.select { |task| task["completed"] }
        assert_equal 1, completed_tasks.count
      end

      it "task with failed conditions have changed:false" do
        changed_tasks = @onboarding_tasks.select { |task| !task['changed'] }
        assert_equal 2, changed_tasks.count
      end
    end
  end

  describe "zero state table has at least one task" do
    describe 'by current user' do
      before do
        OnboardingTask.from_params(@user, tasks_params_1, type_checklist)
        @onboarding_tasks = @presenter.present(@user.onboarding(type_checklist))[:tasks]
      end

      it "2 tasks should return as completed" do
        completed_tasks = @onboarding_tasks.select { |task| task["completed"] }
        assert_equal 2, completed_tasks.count
      end

      it "tasks_1 should record should override condition and return completed:true" do
        task_1 = @onboarding_tasks.find { |task| task["name"] == 'task_1' }
        assert(task_1['completed'])
      end
    end

    describe 'by another user' do
      before do
        OnboardingTask.from_params(@other_user, tasks_params_1, type_checklist)
        @onboarding_tasks = @presenter.present(@user.onboarding(type_checklist))[:tasks]
      end

      it "1 tasks should return as completed" do
        completed_tasks = @onboarding_tasks.select { |task| task["completed"] }
        assert_equal 1, completed_tasks.count
      end

      it "tasks_1 should be incomplete" do
        task_1 = @onboarding_tasks.find { |task| task["name"] == 'task_1' }
        assert_equal false, task_1['completed']
      end
    end
  end

  describe 'zero state table has at least one completed account level task' do
    describe "by current user" do
      before do
        OnboardingTask.from_params(@user, tasks_params_2, type_checklist)
        @onboarding_tasks = @presenter.present(@user.onboarding(type_checklist))[:tasks]
      end

      it "tasks_1 should be incomplete" do
        task_2 = @onboarding_tasks.find { |task| task["name"] == 'task_2' }
        assert(task_2['completed'])
      end
    end

    describe "by another user" do
      before do
        OnboardingTask.from_params(@other_user, tasks_params_2, type_checklist)
        @onboarding_tasks = @presenter.present(@user.onboarding(type_checklist))[:tasks]
      end

      it "tasks_1 should be incomplete" do
        task_2 = @onboarding_tasks.find { |task| task["name"] == 'task_2' }
        assert(task_2['completed'])
      end
    end
  end
end
