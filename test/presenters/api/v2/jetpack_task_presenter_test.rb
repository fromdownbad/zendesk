require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::JetpackTaskPresenter do
  extend Api::Presentation::TestHelper

  before do
    @model = JetpackTask.new(key: 'foo', section: :admin)
    @presenter = Api::V2::JetpackTaskPresenter.new(::User.new, url_builder: mock_url_builder)
  end

  should_present_keys :section, :status, :enabled, :key
  should_present_methods :section, :status, :enabled, :key
end
