require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::OrganizationMembershipPresenter do
  extend Api::Presentation::TestHelper

  def self.presented_methods
    [
      :id,
      :user_id,
      :organization_id,
      :created_at,
      :updated_at,
      :default
    ]
  end

  def self.presented_keys
    presented_methods + [:url]
  end

  fixtures :organization_memberships, :accounts, :users

  before do
    @model = organization_memberships(:minimum)
    @presenter = Api::V2::OrganizationMembershipPresenter.new(
      @model.account.owner, url_builder: mock_url_builder
    )
  end

  should_present_keys(*presented_keys)
  should_present_methods(*presented_methods)
  should_present_url(:api_v2_organization_membership_url)
  should_side_load(:users, :organizations)
end
