require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::CrmDataPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :external_user_datas

  before do
    @account   = accounts(:minimum)
    @model     = external_user_datas(:external_user_data_1)
    @presenter = Api::V2::CrmDataPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :status, :records, :type, :created_at, :updated_at

  should_present_method :id
  should_present_method :status
  should_present_method :records
  should_present_method :created_at
  should_present_method :updated_at
  should_present "salesforce_data", as: :type
end
