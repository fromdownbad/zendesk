require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Requests::GroupPresenter do
  extend Api::Presentation::TestHelper
  fixtures :groups

  before do
    @model = groups(:minimum_group)
    @presenter = Api::V2::Requests::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name

  should_present_method :id
  should_present_method :name
end
