require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Requests::AttachmentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :attachments

  before do
    @model = attachments(:foreign_file_attachment)
    certificate = @model.account.certificates.first
    certificate.update_attributes(state: 'active', valid_until: Time.zone.now + 1.day)
    @presenter = Api::V2::Requests::AttachmentPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :deleted, :file_name, :content_url, :mapped_content_url, :content_type, :size, :width, :height, :thumbnails, :url, :inline
  should_present_methods :id, :content_type, :size
  should_present_method :display_filename, as: :file_name

  it "presents the account subdomain content_url" do
    json = @presenter.model_json(@model)
    assert_equal @model.account.url(mapped: false, ssl: true) + @model.url(relative: true), json[:content_url]
  end

  it "presents the account hostmapped mapped_content_url" do
    json = @presenter.model_json(@model)
    assert_equal @model.account.url + @model.url(relative: true), json[:mapped_content_url]
  end

  describe "given a brand" do
    before do
      @brand = FactoryBot.create(:brand, account_id: @model.account_id)
      @presenter = Api::V2::Requests::AttachmentPresenter.new(@model.account.owner, url_builder: mock_url_builder, brand: @brand)
    end

    it "presents a non-branded subdomain content_url" do
      json = @presenter.model_json(@model)
      assert_equal @model.account.url(mapped: false, ssl: true) + @model.url(relative: true), json[:content_url]
    end

    it "presents a branded hostmapped mapped_content_url" do
      json = @presenter.model_json(@model)
      assert_equal @brand.url + @model.url(relative: true), json[:mapped_content_url]
    end
  end

  it "presents the file_name escaped" do
    json = @presenter.model_json(@model)
    assert_equal "☃.txt", json[:file_name]
  end

  it "presents the thumbnails using the #thumbnails method" do
    thumbnails = stub(:thumbnails)
    @presenter.expects(:thumbnails).with(@model).returns(thumbnails)

    json = @presenter.model_json(@model)
    assert_equal thumbnails, json[:thumbnails]
  end

  describe "#thumbnails" do
    it "uses #model_json on each thumbnail" do
      @model.thumbnails.each do |thumbnail|
        @presenter.expects(:model_json).with(thumbnail, false)
      end
      @presenter.thumbnails(@model)
    end
  end

  describe "#model_json" do
    it "does not output thumbnails when given false as the second argument" do
      @presenter.expects(:thumbnails).never
      json = @presenter.model_json(@model, false)
      refute json.key?(:thumbnails)
    end
  end
end
