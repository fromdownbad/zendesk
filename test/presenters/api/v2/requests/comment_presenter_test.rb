require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Requests::CommentPresenter do
  extend Api::Presentation::TestHelper

  fixtures :attachments, :events, :users, :tickets, :ticket_fields, :organizations

  before do
    @model = events(:serialization_comment)
    @attachment = attachments(:serialization_comment_attachment)
    @model.attachments << @attachment
    brand = FactoryBot.create(:brand, account_id: @model.account_id)
    @presenter = Api::V2::Requests::CommentPresenter.new(@model.account.owner, url_builder: mock_url_builder, brand: brand)
  end

  should_present_keys :id, :type, :request_id, :body, :html_body, :plain_body, :public, :author_id, :created_at, :attachments, :url

  should_present_method :id
  should_present_method :body
  should_present_method :html_body
  should_present_method :is_public?, as: :public
  should_present_method :author_id
  should_present_method :created_at
  should_present_collection :attachments, with: Api::V2::Requests::AttachmentPresenter

  it "presents the requests's nice_id as request_id" do
    assert_equal @model.ticket.nice_id, @presenter.model_json(@model)[:request_id]
  end

  describe "side-loads" do
    before { @presenter.stubs(:side_load?).returns(false) }
    subject { @presenter.present(@model) }

    describe "with requests" do
      before do
        @presenter.stubs(:side_load?).with(:requests).returns(true)
        subject_field = ticket_fields(:minimum_field_subject)
        Account.any_instance.expects(:field_subject).returns(subject_field)
      end

      subject { @presenter.present(@model) }

      it "presents requests" do
        assert_not_nil subject[:requests].first
      end
    end

    describe "without requests" do
      subject { @presenter.present(@model) }

      it "presents authors" do
        assert_not_nil @model.author_id
        assert_not_nil subject[:users].detect { |user| user[:id] == @model.author_id }
      end

      it "presents author organizations" do
        assert_not_nil @model.author.organization_id
        assert_not_nil subject[:organizations].detect { |org| org[:id] == @model.author.organization_id }
      end

      it "does not present requests" do
        assert_nil subject[:requests]
      end
    end
  end
end
