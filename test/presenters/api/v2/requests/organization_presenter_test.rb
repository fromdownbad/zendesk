require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Requests::OrganizationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :organizations

  before do
    @model = organizations(:minimum_organization1)
    @presenter = Api::V2::Requests::OrganizationPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name

  should_present_method :id
  should_present_method :name
end
