require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Requests::ErrorsPresenter do
  let(:ticket)    { Ticket.new }
  let(:presenter) { Api::V2::Requests::ErrorsPresenter.new }

  describe "#present" do
    it "works as superclass when details not present" do
      ticket.errors.add(:base, "Invalid record")
      json = presenter.present(ticket)
      assert_equal([{ description: "Invalid record" }], json[:details][:base])
    end

    it "adds details to the hash when details are present" do
      message = "Invalid record"
      def message.details
        { foo: "bar" }
      end
      ticket.errors.add(:base, message)
      json = presenter.present(ticket)
      assert_equal([{
        description: "Invalid record",
        foo: "bar"
      }], json[:details][:base])
    end
  end
end
