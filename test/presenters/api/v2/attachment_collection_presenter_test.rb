require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered!

describe Api::V2::AttachmentCollectionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :attachments

  before do
    @user = users(:minimum_agent)
    @item_presenter = Api::V2::AttachmentPresenter.new(@user, url_builder: mock_url_builder)
  end

  it 'should not present inline attachments' do
    presenter = Api::V2::AttachmentCollectionPresenter.new(@user, url_builder: mock_url_builder, item_presenter: @item_presenter)
    inline_attachment = attachments(:inline)
    regular_attachment = attachments(:serialization_comment_attachment)

    collection = [inline_attachment, regular_attachment]
    result = presenter.present(collection)

    assert_equal 1, result[:attachments].length
    refute result[:attachments].first[:inline]
  end

  it 'should include inline attachments if "include_inline_images" is set to true as an option' do
    presenter = Api::V2::AttachmentCollectionPresenter.new(@user, url_builder: mock_url_builder, item_presenter: @item_presenter, include_inline_images: true)
    inline_attachment = attachments(:inline)
    regular_attachment = attachments(:serialization_comment_attachment)

    collection = [inline_attachment, regular_attachment]
    result = presenter.present(collection)

    assert_equal 2, result[:attachments].length
    assert result[:attachments].first[:inline]
  end
end
