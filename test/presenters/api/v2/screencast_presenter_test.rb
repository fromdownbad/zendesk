require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ScreencastPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)

    # Screencast are not active record. They are stored in metadata.
    @model = Screencast.new(
      id: "abcd1234",
      url: "http://foo.bar",
      thumbnail: "http://foo.bar/test.jpg",
      position: "1"
    )

    @presenter = Api::V2::ScreencastPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :content_url, :position, :thumbnail_url
end
