require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::UserRelatedPresenter do
  extend Api::Presentation::TestHelper

  fixtures :users, :accounts, :entries, :posts, :tickets

  let(:user) { users(:minimum_agent) }

  all_keys = [
    :assigned_tickets,
    :ccd_tickets,
    :entry_subscriptions,
    :followed_tickets,
    :forum_subscriptions,
    :organization_subscriptions,
    :requested_tickets,
    :subscriptions,
    :topic_comments,
    :topics,
    :votes
  ]

  before do
    @account   = accounts(:minimum)
    @model     = users(:serialization_user)
    @presenter = Api::V2::UserRelatedPresenter.new(user, url_builder: mock_url_builder)

    Account.any_instance.stubs(
      has_follower_and_email_cc_collaborations_enabled?: false,
      has_ticket_followers_allowed_enabled?: false,
      has_comment_email_ccs_allowed_enabled?: false
    )
  end

  describe 'with follower_and_email_cc_collaborations setting enabled' do
    before { Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }

    describe 'with ticket_followers_allowed enabled and comment_email_ccs_allowed disabled' do
      before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

      should_present_keys *all_keys
    end

    describe 'with comment_email_ccs_allowed setting enabled and ticket_followers_allowed disabled' do
      before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

      should_present_keys *all_keys
    end

    describe 'with ticket_followers_allowed and comment_email_ccs_allowed settings enabled' do
      before do
        Account.any_instance.stubs(
          has_ticket_followers_allowed_enabled?: true,
          has_comment_email_ccs_allowed_enabled?: true
        )
      end

      should_present_keys *all_keys
    end
  end

  should_present_keys *(all_keys - [:followed_tickets])

  should_present_method :ccd_tickets,                value: 0
  should_present_method :topics,                     value: 1
  should_present_method :topic_comments,             value: 1
  should_present_method :vote_count,                 value: 0, as: :votes
  should_present_method :subscriptions,              value: 0
  should_present_method :organization_subscriptions, value: 0
  should_present_method :forum_subscriptions,        value: 0
  should_present_method :entry_subscriptions,        value: 0
  should_present_method :assigned_tickets,           value: 0

  describe "assigned tickets" do
    let(:user) { users(:minimum_agent) }

    describe "caching the count" do
      before do
        Account.any_instance.stubs(has_cache_user_related_counts?: true)
        @model = users(:serialization_user)
      end

      it "caches the count" do
        Ticket.stubs(:count_with_archived).returns(10_001)
        json = @presenter.model_json(user)
        cache_key = @presenter.send(:user_related_count_cache_key, user, :assigned)
        assert_equal 10_001, Rails.cache.read(cache_key), "value was not cached"
        assert_equal 10_001, json[:assigned_tickets], "cached value not returned"
      end
    end
  end

  describe "requested tickets" do
    before do
      @model.tickets.each do |ticket|
        User.any_instance.stubs(:can?).with(:view, ticket).returns(false)
      end
    end

    describe "with api_filter_requested_tickets feature enabled and current_user is not admin" do
      before { Arturo.enable_feature!(:api_filter_requested_tickets) }

      should_present_method :requested_tickets, value: 0

      describe "with archived tickets" do
        before do
          @model.tickets.first.archive!
        end

        it "does not load archived tickets from Riak" do
          ZendeskArchive.router.expects(:get_multi).never
          @presenter.model_json(@model)
        end
      end
    end

    describe "with api_filter_requested_tickets feature enabled and current_user is admin" do
      let(:user) { users(:minimum_admin) }
      before { Arturo.enable_feature!(:api_filter_requested_tickets) }

      should_present_method :requested_tickets, value: 1
    end

    describe "with api_filter_requested_tickets feature disabled" do
      before { Arturo.disable_feature!(:api_filter_requested_tickets) }

      should_present_method :requested_tickets, value: 1
    end

    describe "caching the count" do
      let(:user) { users(:minimum_end_user) }

      before do
        Account.any_instance.stubs(has_cache_user_related_counts?: true)
        Ticket.stubs(:count_with_archived).returns(10_001)
        @model = users(:serialization_user)
      end

      it "caches the count" do
        json = @presenter.model_json(user)
        cache_key = @presenter.send(:user_related_count_cache_key, user, :requested)
        assert_equal 10_001, Rails.cache.read(cache_key), "value was not cached"
        assert_equal 10_001, json[:requested_tickets], "cached value not returned"
      end
    end
  end
end
