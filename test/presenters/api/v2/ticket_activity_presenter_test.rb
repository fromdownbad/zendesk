require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::TicketActivityPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :user_settings, :tickets, :events

  before do
    @account   = accounts(:minimum)
    @event     = tickets(:minimum_1).comments.last
    @model     = TicketActivity.create_activity_for(@account.owner, event: @event, update_type: :update)
    @presenter = Api::V2::TicketActivityPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :url, :title, :verb, :user_id, :actor_id,
    :created_at, :updated_at, :object, :target,
    # deprecated
    :user, :actor

  should_present_method :id
  should_present_method :title
  should_present_method :verb
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :user_id
  should_present_method :actor_id

  should_present_url :api_v2_activity_url, with: :id

  it "presents actor" do
    result = @presenter.present(@model)[:actors]
    assert_equal @model.actor_id, result.first[:id]
  end

  it "presents user" do
    result = @presenter.present(@model)[:users]
    assert_equal @model.user_id, result.first[:id]
  end

  describe "presentation" do
    before { @result = @presenter.present(@model) }

    it "presents object" do
      assert_equal @event.body, @result[:activity][:object][:comment][:value]
    end

    it "presents target" do
      assert_equal @event.ticket.nice_id, @result[:activity][:target][:ticket][:id]
    end

    it "returns nil when the activity_target doesn't exist" do
      @model.activity_target = nil
      assert_nil @presenter.present(@model)[:activity]
    end

    it "returns nil when the activity_object doesn't exist" do
      @model.activity_object = nil
      assert_equal({activity: nil, users: [], actors: []}, @presenter.present(@model))
    end
  end
end
