require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::DevicePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :devices

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    @model   = devices(:minimum)
    @presenter = Api::V2::DevicePresenter.new(@user, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :ip, :user_agent, :location,
    :current, :url, :created_at, :updated_at, :last_active_at, :mobile

  should_present_method :id
  should_present_method :name
  should_present_method :ip
  should_present_method :user_agent
  should_present_method :location
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :last_active_at
  should_present_method :mobile?, as: :mobile

  should_present_url :api_v2_user_devices_url, with: :user_id
end
