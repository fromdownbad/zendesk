require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::DashboardPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  before do
    @model     = users(:minimum_agent)
    @presenter = Api::V2::DashboardPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :open_tickets, :new_and_open_tickets_groups,
    :tickets_solved_this_week, :tickets_rated_good_this_week, :tickets_rated_bad_this_week,
    :sat_score_last_60_days, :account_sat_score_last_60_days

  describe_with_arturo_setting_enabled :customer_satisfaction do
    before do
      @json = @presenter.model_json(@model)
    end

    it "presents satisfaction values" do
      [:tickets_solved_this_week, :tickets_rated_good_this_week, :tickets_rated_bad_this_week].each do |key|
        assert_not_nil @json[key], "#{key} in #{@json.inspect} was nil"
      end
    end
  end

  describe_with_arturo_setting_disabled :customer_satisfaction do
    before do
      @json = @presenter.model_json(@model)
    end

    it "still presents satisfaction keys" do
      [:tickets_solved_this_week, :tickets_rated_good_this_week, :tickets_rated_bad_this_week,
       :sat_score_last_60_days, :account_sat_score_last_60_days].each do |key|
        assert @json.key?(key), "Could not find #{key} in #{@json.inspect}"
      end
    end
  end

  describe "with groups enabled" do
    before do
      @model.account.stubs(:has_groups?).returns(true)
      @json = @presenter.model_json(@model)
    end

    it "presents group tickets" do
      assert_not_nil @json[:new_and_open_tickets_groups]
    end
  end

  describe "with groups disabled" do
    before do
      @model.account.stubs(:has_groups?).returns(false)
      @json = @presenter.model_json(@model)
    end

    it "present groups key with nil value" do
      assert @json.key?(:new_and_open_tickets_groups)
      assert_nil @json[:new_and_open_tickets_groups]
    end
  end
end
