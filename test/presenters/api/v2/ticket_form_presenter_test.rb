require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TicketFormPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :ticket_fields, :brands, :translation_locales

  before do
    Account.any_instance.stubs(:has_multibrand?).returns(true)
    Account.any_instance.stubs(:has_ticket_forms?).returns(true)

    @account = accounts(:minimum)
    @model = TicketForm.new.tap do |t|
      t.name             = "Wombat"
      t.display_name     = "Wombats are everywhere"
      t.position         = 4
      t.active           = true
      t.default          = false
      t.end_user_visible = true
      t.account_id       = @account.id
      t.in_all_brands    = true
    end

    # Adding restricted brands in order to properly test assciation preloads.
    # This will make sure that the association preloading code for 'restricted_brands'
    # is run catching any missuse of preloads in development stage.
    @restricted_brand = brands(:minimum)
    @model.ticket_form_brand_restrictions.build do |tfbr|
      tfbr.account = @account
      tfbr.ticket_form = @model
      tfbr.brand = @restricted_brand
    end

    @model.save!

    @presenter = Api::V2::TicketFormPresenter.new(users(:minimum_admin), url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :raw_name, :display_name, :raw_display_name, :position, :active, :default, :end_user_visible, :url, :ticket_field_ids, :restricted_brand_ids, :in_all_brands, :created_at, :updated_at, :agent_conditions, :end_user_conditions

  should_present_method :id
  should_present_method :name
  should_present_method :position
  should_present_method :active
  should_present_method :default
  should_present_method :end_user_visible
  should_present_method :restricted_brand_ids
  should_present_method :in_all_brands
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_ticket_form_url

  describe "with includes" do
    before do
      @model2 = TicketForm.new.tap do |t|
        t.name             = "Another form"
        t.display_name     = "Another form"
        t.position         = 4
        t.active           = true
        t.default          = false
        t.end_user_visible = true
        t.account_id       = @account.id
        t.in_all_brands    = true
      end

      @model2.save!

      @presenter = Api::V2::TicketFormPresenter.new(users(:minimum_admin), url_builder: mock_url_builder, includes: %i[brands ticket_fields])
    end

    should_side_load :brands, :ticket_fields

    it "does not do n + 1s in brands" do
      assert_sql_queries(1, /SELECT `brands`.* FROM `brands` WHERE `brands`.`deleted_at` IS NULL AND `brands`.`account_id` =/) do
        @presenter.present([@model, @model2])
      end
    end

    describe_with_arturo_disabled :fix_brands_n_plus_one_in_ticket_forms do
      it "generates a n + 1 in brands" do
        assert_sql_queries(2, /SELECT `brands`.* FROM `brands` WHERE `brands`.`deleted_at` IS NULL AND `brands`.`account_id` =/) do
          @presenter.present([@model, @model2])
        end
      end
    end
  end

  describe "Ticket field ids" do
    it "presents ticket_field_ids" do
      ticket_field1 = ticket_fields(:minimum_field_subject)
      ticket_field2 = ticket_fields(:minimum_field_status)
      @model.ticket_form_fields.build(account: @account, ticket_form: @model, ticket_field: ticket_field1, position: 1)
      @model.ticket_form_fields.build(account: @account, ticket_form: @model, ticket_field: ticket_field2, position: 2)
      @model.save!

      assert_equal [ticket_field1.id, ticket_field2.id], @presenter.model_json(@model)[:ticket_field_ids]
    end

    it "preserves the order (by position) of ticket_field_ids" do
      setup_ticket_fields
      assert_equal [@agent_field1.id, @agent_field2.id, @end_user_visible_ticket_field.id], @presenter.model_json(@model)[:ticket_field_ids]
    end
  end

  it "presents restricted_brand_ids" do
    assert_equal [@restricted_brand.id], @presenter.model_json(@model)[:restricted_brand_ids]
  end

  describe "presenting conditionality" do
    describe "when Conditional Ticket Fields is not active" do
      before(:each) { @presenter.user.account.stubs(:has_native_conditional_fields_enabled?).returns(false) }

      describe "#end_user_conditions" do
        it "should not be a key" do
          refute @presenter.model_json(@model).key?(:end_user_conditions)
        end
      end

      describe "#agent_conditions" do
        it "should should not be a key even if logged in as an agent" do
          refute @presenter.model_json(@model).key?(:agent_conditions)
        end
      end
    end

    describe "when Conditional Ticket Fields is active" do
      before(:each) { @presenter.user.account.stubs(:has_native_conditional_fields_enabled?).returns(true) }

      describe "#end_user_conditions" do
        it "should return empty array if there are no conditions" do
          assert_equal [], @presenter.model_json(@model)[:end_user_conditions]
        end

        it "should present End-User conditions when they exist grouped by precondition" do
          account = @presenter.user.account
          @precond1 = FactoryBot.create(:ticket_field_condition, account: account, user_type: :end_user)
          ticket_form = @precond1.ticket_form
          @precond2 = FactoryBot.create(:ticket_field_condition, account: account, ticket_form_id: ticket_form.id, user_type: :end_user)

          # Same precondition as @precond1, different child
          @precond1b = @precond1.dup
          @precond1b.child_field_id = FactoryBot.create(:decimal_ticket_field, account: account).id
          TicketFormField.create(
            ticket_form: @precond1b.ticket_form,
            account: @precond1b.account,
            ticket_field: @precond1b.child_field
          )
          @precond1b.save

          # Same parent as @precond1, different value i.e. different precondition
          @precond3 = FactoryBot.create(
            :ticket_field_condition,
            account: account,
            ticket_form_id: ticket_form.id,
            user_type: :end_user,
            parent_field_id: @precond1.parent_field_id,
            child_field_id: @precond1.child_field_id
          )

          # reload form to see all the new ticket_form_fields
          ticket_form.reload

          grouped_conditions = @presenter.model_json(ticket_form)[:end_user_conditions]

          # 4 rows in the database, 3 preconditions
          assert_equal 3, grouped_conditions.size

          precondition1_group = grouped_conditions.select { |c| c[:parent_field_id] == @precond1.parent_field_id && c[:value] == @precond1.value }.first
          assert_equal precondition1_group[:child_fields], [
            { id: @precond1.child_field_id, is_required: @precond1.is_required },
            { id: @precond1b.child_field_id, is_required: @precond1b.is_required }
          ]

          precondition2_group = grouped_conditions.select { |c| c[:parent_field_id] == @precond2.parent_field_id && c[:value] == @precond2.value }.first
          assert_equal precondition2_group[:child_fields], [{ id: @precond2.child_field_id, is_required: @precond2.is_required }]

          precondition3_group = grouped_conditions.select { |c| c[:parent_field_id] == @precond3.parent_field_id && c[:value] == @precond3.value }.first
          assert_equal precondition3_group[:child_fields], [{ id: @precond3.child_field_id, is_required: @precond3.is_required }]
        end
      end

      describe "#agent_conditions" do
        it "should not surface the agent conditions key if not logged in as an agent" do
          @presenter = Api::V2::TicketFormPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder)

          agent_condition = FactoryBot.create(:ticket_field_condition, account: @presenter.user.account, user_type: :agent)

          refute @presenter.model_json(agent_condition.ticket_form).key?(:agent_conditions)
        end

        it "should not surface the agent conditions key if no relevant data exists" do
          assert_equal [], @presenter.model_json(@model)[:agent_conditions]
        end

        it "should present End-User conditions when they exist grouped by precondition" do
          account = @presenter.user.account
          @precond1 = FactoryBot.create(:ticket_field_condition, account: account, user_type: :agent)
          ticket_form = @precond1.ticket_form
          @precond2 = FactoryBot.create(:ticket_field_condition, account: account, ticket_form_id: ticket_form.id, user_type: :agent)

          # Same precondition as @precond1, different child
          @precond1b = @precond1.dup
          @precond1b.child_field_id = FactoryBot.create(:decimal_ticket_field, account: account).id
          TicketFormField.create(
            ticket_form: @precond1b.ticket_form,
            account: @precond1b.account,
            ticket_field: @precond1b.child_field
          )
          @precond1b.save

          # Same parent as @precond1, different value i.e. different precondition
          @precond3 = FactoryBot.create(
            :ticket_field_condition,
            account: account,
            ticket_form_id: ticket_form.id,
            user_type: :agent,
            parent_field_id: @precond1.parent_field_id
          )

          # reload form to see all the new ticket_form_fields
          ticket_form.reload

          grouped_conditions = @presenter.model_json(ticket_form)[:agent_conditions]

          # 4 rows in the database, 3 preconditions
          assert_equal 3, grouped_conditions.size

          precondition1_group = grouped_conditions.select { |c| c[:parent_field_id] == @precond1.parent_field_id && c[:value] == @precond1.value }.first
          assert_equal precondition1_group[:child_fields], [
            {
              id: @precond1.child_field_id,
              is_required: @precond1.is_required,
              required_on_statuses: {type: "NO_STATUSES"}
            },
            {
                id: @precond1b.child_field_id,
                is_required: @precond1b.is_required,
                required_on_statuses: {type: "NO_STATUSES"}
            }
          ]

          precondition2_group = grouped_conditions.select { |c| c[:parent_field_id] == @precond2.parent_field_id && c[:value] == @precond2.value }.first
          assert_equal precondition2_group[:child_fields], [
            {
              id: @precond2.child_field_id,
              is_required: @precond2.is_required,
              required_on_statuses: {type: "NO_STATUSES"}
            }
          ]

          precondition3_group = grouped_conditions.select { |c| c[:parent_field_id] == @precond3.parent_field_id && c[:value] == @precond3.value }.first
          assert_equal precondition3_group[:child_fields], [
            {
              id: @precond3.child_field_id,
              is_required: @precond3.is_required,
              required_on_statuses: {type: "NO_STATUSES"}
            }
          ]
        end
      end
    end
  end

  describe "Dynamic Content" do
    before do
      @presenter.stubs(:dc_cache).returns({})
      @user = users(:minimum_end_user)
    end

    describe "without the 'locale' option" do
      before do
        # This is opaque but here we make sure the dc_cache is built properly.
        @dc_cache = stub
        Zendesk::DynamicContent::AccountContent.expects(:cache).with(@account, I18n.translation_locale).returns(@dc_cache)
        @presenter = Api::V2::TicketFormPresenter.new(@user, url_builder: mock_url_builder)
      end

      it "presents rendered display names and name that have dc content using I18n.translation_locale" do
        @model.update_attributes(display_name: "{{dc.wombat}}")
        Zendesk::Liquid::DcContext.expects(:render).with("{{dc.wombat}}", @account, @user, "text/plain", I18n.translation_locale, @dc_cache).
          returns("This is the display name")
        Zendesk::Liquid::DcContext.expects(:render).with("Wombat", @account, @user, "text/plain", I18n.translation_locale, @dc_cache).
          returns("This is the name")

        json = @presenter.model_json(@model)
        assert_equal "This is the display name", json[:display_name]
        assert_equal "This is the name", json[:name]
      end

      it "presents raw name if rendered dc content is empty" do
        @model.update_attributes(name: "{{}}")
        json = @presenter.model_json(@model)
        assert_equal "{{}}", json[:name]
      end
    end

    describe "with the 'locale' option" do
      let(:spanish_locale) { translation_locales(:spanish) }

      before do
        # This is opaque but here we make sure the dc_cache is built properly.
        @spanish_dc_cache = stub
        Zendesk::DynamicContent::AccountContent.expects(:cache).with(@account, spanish_locale).returns(@spanish_dc_cache)
        @presenter = Api::V2::TicketFormPresenter.new(@user, url_builder: mock_url_builder, locale: spanish_locale)
      end

      it "presents rendered display names and name that have dc content using the provided locale" do
        @model.update_attributes(display_name: "{{dc.wombat}}")
        Zendesk::Liquid::DcContext.expects(:render).with("{{dc.wombat}}", @account, @user, "text/plain", spanish_locale, @spanish_dc_cache).
          returns("Este es el nombre con CD")
        Zendesk::Liquid::DcContext.expects(:render).with("Wombat", @account, @user, "text/plain", spanish_locale, @spanish_dc_cache).
          returns("Este es el nombre")

        json = @presenter.model_json(@model)
        assert_equal "Este es el nombre con CD", json[:display_name]
        assert_equal "Este es el nombre", json[:name]
      end

      it "presents raw name if rendered dc content is empty" do
        @model.update_attributes(name: "{{}}")
        json = @presenter.model_json(@model)
        assert_equal "{{}}", json[:name]
      end
    end
  end

  it "only present active End-User visible ticket_field_ids for End-Users" do
    @presenter = Api::V2::TicketFormPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder)

    setup_ticket_fields

    assert_equal 3, @model.ticket_form_fields.count(:all)
    assert_equal [@end_user_visible_ticket_field.id], @presenter.model_json(@model)[:ticket_field_ids]
  end

  describe "when sideloading ticket fields" do
    before do
      @presenter = Api::V2::TicketFormPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder)
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:ticket_fields).returns(true)

      setup_ticket_fields

      @ticket_form2 = TicketForm.new.tap do |t|
        t.name             = "Capybara"
        t.display_name     = "Capybaras are everywhere!"
        t.position         = 5
        t.active           = true
        t.default          = false
        t.end_user_visible = true
        t.account_id       = @account.id
        t.in_all_brands    = true
      end

      @ticket_form2.ticket_form_fields.build(account: @account, ticket_form: @ticket_form2, ticket_field: @end_user_visible_ticket_field, position: 3)
      @ticket_form2.ticket_form_fields.build(account: @account, ticket_form: @ticket_form2, ticket_field: @agent_field1, position: 1)
      @ticket_form2.ticket_form_fields.build(account: @account, ticket_form: @ticket_form2, ticket_field: @agent_field2, position: 2)
      @ticket_form2.save!
    end

    let(:model_json) { @presenter.present([@model, @ticket_form2]) }

    it "only present active End-User visible ticket_field_ids for End-Users" do
      assert_equal 3, @model.ticket_form_fields.count(:all)
      assert_equal 1, model_json[:ticket_fields].size
      assert_equal([@end_user_visible_ticket_field.id], model_json[:ticket_fields].map { |field| field[:id] })
    end

    it "does not do n + 1 queries" do
      assert_no_n_plus_one do
        model_json
      end
    end

    it "does not fail when ticket_form_field has a nil ticket_field" do
      TicketFormField.any_instance.stubs(:ticket_field).returns(nil)
      @presenter.present(@model)
    end
  end

  def setup_ticket_fields
    @end_user_visible_ticket_field = ticket_fields(:field_text_custom)

    @agent_field1 = ticket_fields(:minimum_field_subject)
    @agent_field1.update_attributes(is_active: true, is_editable_in_portal: false)

    @agent_field2 = ticket_fields(:minimum_field_status)
    @agent_field2.update_attributes(is_active: true, is_editable_in_portal: false)

    @model.ticket_form_fields.build(account: @account, ticket_form: @model, ticket_field: @end_user_visible_ticket_field, position: 3)
    @model.ticket_form_fields.build(account: @account, ticket_form: @model, ticket_field: @agent_field1, position: 1)
    @model.ticket_form_fields.build(account: @account, ticket_form: @model, ticket_field: @agent_field2, position: 2)
    @model.save!
  end
end
