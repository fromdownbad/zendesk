require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AgentBrandPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @model = FactoryBot.create(:brand)
    @presenter = Api::V2::AgentBrandPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :brand_url, :has_help_center, :help_center_state, :active, :default, :is_deleted, :logo, :url, :ticket_form_ids, :signature_template, :subdomain, :created_at, :updated_at

  should_present_method :id
  should_present_method :name
  should_present_method :url, as: :brand_url
  should_present_method :subdomain
  should_present_method :active
  should_present_method :default?, as: :default
  should_present_method :deleted?, as: :is_deleted
  should_present_method :logo, with: Api::V2::AttachmentPresenter
  should_present_method :signature_template
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_brand_url

  describe "with includes" do
    before do
      @presenter = Api::V2::AgentBrandPresenter.new(users(:minimum_agent), url_builder: mock_url_builder, includes: [:ticket_forms])
    end

    should_side_load :ticket_forms
  end

  it "presents 'disabled' in help_center_state if the brand has no help center" do
    HelpCenter.stubs(:find_by_brand_id).returns(nil)

    assert_equal :disabled, @presenter.present(@model)[:brand][:help_center_state]
  end

  it "presents the state of the brand's help center" do
    HelpCenter.stubs(:find_by_brand_id).with(@model.id).returns(
      HelpCenter.new(id: 42, state: 'enabled')
    )

    assert_equal 'enabled', @presenter.present(@model)[:brand][:help_center_state]
  end

  it "presents ticket_form_ids" do
    assert_equal @account.ticket_forms.map(&:id), @presenter.model_json(@model)[:ticket_form_ids]
  end
end
