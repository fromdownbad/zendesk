require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AuditLogPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @model = CIA::Event.create(
      account: accounts(:minimum),
      actor: users(:minimum_agent),
      source: users(:minimum_agent),
      action: "update"
    )

    @presenter = Api::V2::AuditLogPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder)
  end

  describe 'verify keys' do
    should_present_keys(
      :id,
      :ip_address,
      :url,
      :created_at,
      :actor_id,
      :source_id,
      :source_type,
      :source_label,
      :action,
      :action_label,
      :change_description
    )

    it 'has translated action' do
      assert_equal "update", @presenter.present(@model)[:audit_log][:action]
      assert_equal "Updated", @presenter.present(@model)[:audit_log][:action_label]
    end
  end

  it "does not include html" do
    from = '[["status_id less_than 3","assignee_id is current_user"],[],[]]'
    to   = '[["status_id less_than 3"],["requester_id is 6","assignee_id is current_user"],[]]'
    @model.attribute_changes.build do |c|
      c.attribute_name = "definition"
      c.old_value = from
      c.new_value = to
    end

    assert_equal "Definition: Changed", @presenter.present(@model)[:audit_log][:change_description]
  end

  it "presents dates in changes" do
    @model.attribute_changes.build do |c|
      c.attribute_name = "trial_expires_on"
      c.old_value = nil
      c.new_value = Date.new(2009)
    end

    assert_equal "Trial expires on changed from not set to January 1, 2009", @presenter.present(@model)[:audit_log][:change_description]
  end

  describe "FakeContext" do
    it "responds to random methods" do
      assert_equal 111, Api::V2::AuditLogPresenter::FakeContext.new(xxx: 111).xxx
    end

    it "puts data before helpers" do
      assert_equal "LINK_TO", Api::V2::AuditLogPresenter::FakeContext.new(link_to: "LINK_TO").link_to
    end

    it "supports helpers" do
      assert_equal "<a href=\"yyy\">xxx</a>", Api::V2::AuditLogPresenter::FakeContext.new({}).link_to("xxx", "yyy")
    end

    it "fails on unknown methods" do
      ZendeskExceptions::Logger.expects(:record)
      assert_equal "Error", Api::V2::AuditLogPresenter::FakeContext.new({}).xxx
    end
  end
end
