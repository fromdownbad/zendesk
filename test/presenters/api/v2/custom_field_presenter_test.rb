require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::CustomFieldPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :translation_locales

  before do
    @account = accounts(:minimum)
    @model = @account.custom_fields.build(key: "custom_field_foo", title: "title", owner: "User", description: "desc")
    @model.type = "Text"
    @model.save!
    @presenter = Api::V2::CustomFieldPresenter.new(@model.account.owner, model: "user", url_builder: mock_url_builder)
  end

  should_present_keys :id, :url, :key, :type, :title, :raw_title, :description, :raw_description, :position, :active, :system,
    :regexp_for_validation, :created_at, :updated_at

  should_present_method :id
  should_present_method :key
  should_present_method :title
  should_present_method :description
  should_present_method :position
  should_present_method :is_active?, as: :active
  should_present_method :is_system?, as: :system
  should_present_method :regexp_for_validation
  should_present_method :created_at
  should_present_method :updated_at

  should_present_url :api_v2_user_field_url

  it "presents the type as a downcased string" do
    assert_equal "text", @presenter.model_json(@model)[:type]
  end

  it "presents user models as user_field" do
    assert @presenter.as_json(@model)[:user_field]
  end

  it "presents dropdowns with custom_field_options" do
    @model = CustomField::Dropdown.new(key: "custom_field_dropdown", title: "title", owner: "User", description: "desc")
    @model.account = @account
    @model.dropdown_choices.build(name: "foo", value: "foo")
    @model.dropdown_choices.build(name: "bar", value: "bar")
    @model.dropdown_choices.build(name: "baz", value: "baz")
    @model.save!
    @model.reload
    @model.dropdown_choices.last.soft_delete!

    json = @presenter.as_json(@model)[:user_field]
    assert json[:custom_field_options]
    assert_equal 2, json[:custom_field_options].size
    refute json[:custom_field_options].map { |field| field[:value] }.include?("baz")
    json[:custom_field_options].each do |opt|
      assert opt[:id]
      assert opt[:name]
      assert opt[:value]
    end
  end

  describe "asked to render dynamic content" do
    it "renders DC" do
      rendering_presenter = Api::V2::CustomFieldPresenter.new(@model.account.owner, model: "user", url_builder: mock_url_builder)
      rendering_presenter.expects(:render_dynamic_content).returns('dynamic_attribute')
      assert_equal 'dynamic_attribute', rendering_presenter.render_dc_or_i18n(@model, 'test_attribute')
    end
  end

  describe "#model_json" do
    it "calls render_dc_if_needed for title and description" do
      custom_field = stub(
        id: 1,
        type: 'CustomField::test_type',
        key: 'test_key',
        title: 'test_title',
        description: 'test_description',
        position: 2,
        is_active?: true,
        is_system?: false,
        regexp_for_validation: 'test_regexp',
        created_at: 'test_created_at',
        updated_at: 'test_updated_at'
      )
      @presenter.expects(:render_dc_or_i18n).with(custom_field, 'test_title').returns('rendered_title')
      @presenter.expects(:render_dc_or_i18n).with(custom_field, 'test_description').returns('rendered_description')
      expected = {
        id: 1,
        type: "test_type",
        key: "test_key",
        title: "rendered_title",
        description: "rendered_description",
        position: 2,
        raw_description: "test_description",
        raw_title: "test_title",
        active: true,
        system: false,
        regexp_for_validation: "test_regexp",
        created_at: "test_created_at",
        updated_at: "test_updated_at",
        url: :api_v2_user_field_url
      }
      assert_equal expected, @presenter.model_json(custom_field)
    end
  end

  describe "#dropdown_options" do
    it "calls render_dynamic_content for name" do
      field = stub(dropdown_choices: [stub(active?: true, id: 1, name: 'test_name', value: 'test_value')])
      @presenter.expects(:render_dc_or_i18n).with(field, 'test_name').returns('rendered_name')
      assert_equal [{id: 1, name: "rendered_name", raw_name: "test_name", value: "test_value"}], @presenter.dropdown_options(field)
    end
  end
end
