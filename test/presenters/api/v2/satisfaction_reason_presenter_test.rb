require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SatisfactionReasonPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @model = @account.satisfaction_reasons.create!(value: "Issue took too long")
    @model2 = @account.satisfaction_reasons.create!(value: "Agent had a bad attitude")
    @presenter = Api::V2::SatisfactionReasonPresenter.new(
      @account.owner,
      brand: @account.default_brand,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :id, :url, :reason_code, :value, :raw_value, :active, :created_at, :updated_at, :deleted_at
  should_present_url :api_v2_satisfaction_reason_url

  it 'does not perform n+1 queries' do
    assert_no_n_plus_one do
      @presenter.present([@model, @model2])
    end
  end
end
