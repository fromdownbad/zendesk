require_relative "../../../support/test_helper"
require_relative "../../../support/sharing_test_helper"

SingleCov.covered!

describe Api::V2::SharedTicketPresenter do
  extend Api::Presentation::TestHelper
  include SharingTestHelper
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:agreement) { create_valid_agreement }

  before do
    @model = SharedTicket.new(ticket: ticket, agreement: agreement, uuid: '123ABC', original_id: 123)
    @presenter = Api::V2::SharedTicketPresenter.new(
      ticket.account.owner,
      url_builder: mock_url_builder,
      agreement: agreement
    )
  end

  should_present_keys :remote_ticket_id, :local_ticket_id

  describe "when the agreement is outbound" do
    before do
      refute agreement.in?
      @json = @presenter.model_json(@model)
    end

    it "does not have a remote_ticket_id" do
      assert_nil @json[:remote_ticket_id]
    end
  end

  describe "when the agreement is inbound" do
    before do
      agreement.stubs(in?: true)
      @json = @presenter.model_json(@model)
    end

    it "presents a remote_ticket_id" do
      assert_equal 123, @json[:remote_ticket_id]
    end
  end

  describe "an archived ticket" do
    before do
      @archived_ticket = tickets(:minimum_5)
      archive_and_delete(@archived_ticket)
      @model = SharedTicket.create!(ticket: @archived_ticket, agreement: agreement, uuid: '123ABC', original_id: 123)
      @model.reload
      @json = @presenter.model_json(@model)
    end

    it "has a local_ticket_id" do
      assert_equal @archived_ticket.nice_id, @json[:local_ticket_id]
    end
  end
end
