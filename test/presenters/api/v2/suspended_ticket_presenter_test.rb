require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SuspendedTicketPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @model = suspended_tickets(:minimum_spam)
    @presenter = Api::V2::SuspendedTicketPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :created_at, :message_id,
    :updated_at, :subject, :cause, :cause_id, :content, :recipient,
    :via, :url, :author, :ticket_id, :error_messages, :attachments

  should_present_method :id
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :content
  should_present_method :ticket_id
  should_present_method :recipient
  should_present_method :message_id
  should_present_collection :attachments, with: Api::V2::AttachmentPresenter

  should_present_url :api_v2_suspended_ticket_url, with: :id

  it "presents via" do
    via = mock
    @presenter.expects(:serialized_via_object).with(@model).returns(via)
    assert_equal via, @presenter.model_json(@model)[:via]
  end

  it "presents cause" do
    assert_equal 'Detected as spam', @presenter.model_json(@model)[:cause]
    assert_equal 0, @presenter.model_json(@model)[:cause_id]

    @model.cause = nil
    assert_nil @presenter.model_json(@model)[:cause]
  end

  it "exposes author" do
    assert_not_nil @presenter.model_json(@model)[:author][:id]
    assert_equal @model.author.name, @presenter.model_json(@model)[:author][:name]
    assert_equal @model.author.email, @presenter.model_json(@model)[:author][:email]

    @model.author = nil
    assert_nil @presenter.model_json(@model)[:author][:id]
    assert_equal @model.from_name, @presenter.model_json(@model)[:author][:name]
    assert_equal @model.from_mail, @presenter.model_json(@model)[:author][:email]
  end

  describe "subject" do
    describe "when subject is blank" do
      before do
        @model.subject = nil
      end

      it "presents ticket content as subject" do
        assert_equal @model.content.truncate(50), @presenter.model_json(@model)[:subject].truncate(50)
      end

      describe "and ticket content exceeds Nokogumbo::DEFAULT_MAX_TREE_DEPTH" do
        before do
          @original_model_content = @model.content.clone
          @model.content = nest_html_content(@model.content, Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1)
        end

        it "presents ticket content as subject" do
          assert_equal @original_model_content, @presenter.model_json(@model)[:subject]
        end
      end

      describe "when a SystemStackError exception is raised" do
        before { Nokogiri::HTML5.stubs(:fragment).raises(SystemStackError).twice }

        it "presents an empty string as subject" do
          Rails.logger.expects(:warn).once.with(regexp_matches(/Subject could not be parsed for ticket/))
          Rails.logger.expects(:warn).once.with(regexp_matches(/Content could not be parsed for ticket/))
          assert_equal '', @presenter.model_json(@model)[:subject]
        end
      end
    end
  end

  describe "content" do
    before do
      @original_model_content = @model.content.clone
      @model.content = nest_html_content(@model.content, Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1)
    end

    describe "when ticket content exceeds Nokogumbo::DEFAULT_MAX_TREE_DEPTH" do
      it "presents ticket content as content" do
        assert_equal @original_model_content, @presenter.model_json(@model)[:content]
      end
    end

    describe "when a SystemStackError exception is raised" do
      before { Nokogiri::HTML5.stubs(:fragment).raises(SystemStackError).twice }

      it "presents an empty string as content" do
        Rails.logger.expects(:warn).once.with(regexp_matches(/Subject could not be parsed for ticket/))
        Rails.logger.expects(:warn).once.with(regexp_matches(/Content could not be parsed for ticket/))
        assert_equal '', @presenter.model_json(@model)[:content]
      end
    end
  end

  describe "title and content sanitization" do
    before do
      @model.subject = "SUBJECT<!--><svg onload=\"alert('XSS on '+ document.domain)\">"
      @model.content = "CONTENT<!--><svg onload=\"alert('XSS on '+ document.domain)\">"
    end

    it "sanitizes text" do
      assert_equal "SUBJECT", @presenter.model_json(@model)[:subject]
      assert_equal "CONTENT", @presenter.model_json(@model)[:content]
    end
  end

  describe "brand_id" do
    before { @model.properties = { brand_id: 1 } }

    it "presents brand id when account has multiple active brands" do
      @presenter.account.stubs(:has_multibrand?).returns(true)
      assert_equal 1, @presenter.model_json(@model)[:brand_id]
    end

    it "does not present brand id when account has no multiple active brands" do
      @presenter.account.stubs(:has_multibrand?).returns(false)
      assert_nil @presenter.model_json(@model)[:brand_id]
      assert_equal false, @presenter.model_json(@model).key?('brand_id')
    end
  end

  describe "via_id" do
    describe "WEB_SERVICE" do
      before do
        @model.via_id = ViaType.WEB_SERVICE
      end

      it "does not raise exception" do
        # NOTE: EventViaPresenter#web_source used to call @model#via_reference_id
        # which is not implemented, and raised an exception.
        @presenter.model_json(@model)
      end
    end
  end

  private

  def nest_html_content(html_content, nesting)
    "#{'<span>' * nesting}#{html_content}#{'</span>' * nesting}"
  end
end
