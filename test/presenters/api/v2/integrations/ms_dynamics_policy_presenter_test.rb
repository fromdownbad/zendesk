require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Integrations::MsDynamicsPolicyPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users, :accounts

  describe "#present_xml" do
    before do
      @user = users(:minimum_agent)
      @presenter = Api::V2::Integrations::MsDynamicsPolicyPresenter.new(@user, url_builder: mock_url_builder)
      @integration = MsDynamicsIntegration.new
      @server_address = "http://server.com"
      @additional_domains = ["http://additional1.com", "http://additional2.com"]
      @integration.expects(:server_address_for_policy).returns(@server_address)
      @integration.expects(:additional_domains_for_policy).returns(@additional_domains)
    end

    it "presents the client access policy xml" do
      xml = @presenter.present_xml(@integration)
      doc = Nokogiri::XML(xml)
      server_address_uri = doc.xpath("/access-policy/cross-domain-access/policy/allow-from/domain")[0]['uri']
      first_additional_domain = doc.xpath("/access-policy/cross-domain-access/policy/allow-from/domain")[1]['uri']
      second_additional_domain = doc.xpath("/access-policy/cross-domain-access/policy/allow-from/domain")[2]['uri']
      assert_equal @server_address, server_address_uri
      assert_equal @additional_domains.first, first_additional_domain
      assert_equal @additional_domains.second, second_additional_domain
    end

    describe "with has_dynamics_soapaction?" do
      before do
        Account.any_instance.stubs(:has_dynamics_soapaction?).returns(true)
        @integration.account = @user.account
      end

      it "presents the client access policy with SOAPAction" do
        xml = @presenter.present_xml(@integration)
        doc = Nokogiri::XML(xml)
        assert_equal "SOAPAction", doc.xpath("/access-policy/cross-domain-access/policy/allow-from")[0]['http-request-headers']
      end
    end
  end
end
