require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Integrations::JiraPresenter do
  extend Api::Presentation::TestHelper

  before do
    @presenter = Api::V2::Integrations::JiraPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
  end

  it "returns same JSON passed in" do
    assert_equal({ foo: :bar }, @presenter.model_json(foo: :bar))
  end

  it "returns empty hash for nil" do
    assert_equal({}, @presenter.model_json(nil))
  end
end
