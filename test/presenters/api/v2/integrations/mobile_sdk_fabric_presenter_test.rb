require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Integrations::MobileSdkFabricPresenter do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:mobile_app_sdk_json) do
    {
      'id' => 'id',
      'identifier' => 'identifier',
      'client_identifier' => 'client_identifier'
    }
  end

  it 'presents the fabric integration results' do
    presenter = Api::V2::Integrations::MobileSdkFabricPresenter.new

    result = presenter.present(mobile_app_sdk_json, account)

    assert_equal 'id', result[:id]
    assert_equal account.url(protocol: 'https'), result[:keys][:primary][:url]
    assert_equal 'identifier', result[:keys][:primary][:identifier]
    assert_equal 'client_identifier', result[:keys][:primary][:client_identifier]
  end
end
