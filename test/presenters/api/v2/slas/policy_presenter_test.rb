require_relative '../../../../support/test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered! uncovered: 3

describe Api::V2::Slas::PolicyPresenter do
  include TicketMetricHelper

  extend Api::Presentation::TestHelper

  let(:organization) do
    accounts(:minimum).organizations.first
  end

  let(:conditions_all) do
    [
      {
        source: 'current_tags',
        operator: 'includes',
        value: 'blah'
      },
      {
        source: 'organization_id',
        operator: 'is',
        value: organization.id
      }
    ]
  end

  let(:json) { @presenter.present(@model) }

  before do
    @account = accounts(:minimum)
    @model   = new_sla_policy(account: @account, conditions_all: conditions_all)

    @presenter = Api::V2::Slas::PolicyPresenter.new(
      @account.owner,
      url_builder: mock_url_builder,
      includes: [:organizations]
    )
  end

  should_present_keys :id,
    :description,
    :filter,
    :policy_metrics,
    :position,
    :title,
    :url,
    :created_at,
    :updated_at

  should_present_methods :id,
    :description,
    :position,
    :title,
    :created_at,
    :updated_at

  should_side_load :organizations

  it 'presents filter' do
    expected = {
      all: [
        {
          field: 'current_tags',
          operator: 'includes',
          value: 'blah'
        },
        {
          field: 'organization_id',
          operator: 'is',
          value: organization.id
        }
      ],
      any: []
    }

    assert_equal expected, json[:sla_policy][:filter]
  end

  it 'presents organizations' do
    expected = [
      {
        'id'   => organization.id,
        'name' => organization.name
      }
    ]

    assert_equal expected, json[:organizations]
  end

  should_present_url :api_v2_slas_policy_url
end
