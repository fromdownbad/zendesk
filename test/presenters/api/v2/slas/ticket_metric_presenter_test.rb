require_relative '../../../../support/test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered!

describe Api::V2::Slas::TicketMetricPresenter do
  include TicketMetricHelper

  extend Api::Presentation::TestHelper

  fixtures :accounts, :tickets, :events

  let(:account)       { accounts(:minimum) }
  let(:ticket)        { tickets(:minimum_1) }
  let(:ticket_metric) { Zendesk::Sla::TicketMetric::FirstReplyTime }
  let(:policy_metric) do
    new_sla_policy_metric(
      account: account,
      policy: new_sla_policy(account: account),
      priority_id: ticket.priority_id,
      metric_id: ticket_metric.id,
      target: 10,
      business_hours: false
    )
  end

  before do
    Timecop.freeze

    @ticket_policy = ticket.build_sla_ticket_policy

    @model = @ticket_policy.statuses.build(
      ticket_policy: @ticket_policy,
      policy_metric: policy_metric,
      breach_at: Time.now - 2.minutes,
      fulfilled_at: Time.now - 1.minute,
      paused: false
    )

    @presenter = Api::V2::Slas::TicketMetricPresenter.new(account.owner,
      url_builder: mock_url_builder)
  end

  should_present_keys :breach_at,
    :metric,
    :minutes,
    :stage

  should_present_method :breach_at, value: Time.now
  should_present_method :name, as: :metric
  should_present_method :name, as: :stage, value: :achieved

  describe 'when the breach time is' do
    before { @model.stubs(:breach_at).returns(breach_at) }

    describe 'now' do
      let(:breach_at) { Time.now }

      should_present_method :minutes, value: 0
    end

    describe '29 seconds ago' do
      let(:breach_at) { Time.now - 29.seconds }

      should_present_method :minutes, value: 0
    end

    describe '30 seconds ago' do
      let(:breach_at) { Time.now - 30.seconds }

      should_present_method :minutes, value: -1
    end

    describe 'one minute ago' do
      let(:breach_at) { Time.now - 1.minute }

      should_present_method :minutes, value: -1
    end

    describe '90 minutes ago' do
      let(:breach_at) { Time.now - 90.minutes }

      should_present_method :minutes, value: -90
    end

    describe '91 minutes ago' do
      let(:breach_at) { Time.now - 91.minutes }

      should_present_method :hours, value: -2
    end

    describe '25 hours, 45 minutes ago' do
      let(:breach_at) { Time.now - 25.hours - 45.minutes }

      should_present_method :hours, value: -26
    end

    describe '36 hours, 29 minutes ago' do
      let(:breach_at) { Time.now - 36.hours - 29.minutes }

      should_present_method :hours, value: -36
    end

    describe '36 hours, 30 minutes ago' do
      let(:breach_at) { Time.now - 36.hours - 30.minutes }

      should_present_method :hours, value: -37
    end

    describe '36 hours, 31 minutes ago' do
      let(:breach_at) { Time.now - 36.hours - 31.minutes }

      should_present_method :days, value: -2
    end

    describe '48 hours ago' do
      let(:breach_at) { Time.now - 48.hours }

      should_present_method :days, value: -2
    end

    describe '29 seconds from now' do
      let(:breach_at) { Time.now + 29.seconds }

      should_present_method :minutes, value: 0
    end

    describe '30 seconds from now' do
      let(:breach_at) { Time.now + 30.seconds }

      should_present_method :minutes, value: 1
    end

    describe '90 minutes from now' do
      let(:breach_at) { Time.now + 90.minutes }

      should_present_method :minutes, value: 90
    end

    describe '91 minutes from now' do
      let(:breach_at) { Time.now + 91.minutes }

      should_present_method :hours, value: 2
    end

    describe '25 hours, 45 minutes from now' do
      let(:breach_at) { Time.now + 25.hours + 45.minutes }

      should_present_method :hours, value: 26
    end

    describe '36 hours, 30 minutes from now' do
      let(:breach_at) { Time.now + 36.hours + 30.minutes }

      should_present_method :hours, value: 37
    end

    describe '36 hours, 31 minutes from now' do
      let(:breach_at) { Time.now + 36.hours + 31.minutes }

      should_present_method :days, value: 2
    end

    describe '48 hours from now' do
      let(:breach_at) { Time.now + 48.hours }

      should_present_method :days, value: 2
    end
  end
end
