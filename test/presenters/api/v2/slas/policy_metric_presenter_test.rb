require_relative '../../../../support/test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered!

describe Api::V2::Slas::PolicyMetricPresenter do
  include TicketMetricHelper

  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @model   = Sla::PolicyMetric.new do |pm|
      pm.priority_id    = PriorityType.HIGH
      pm.metric_id      = Zendesk::Sla::TicketMetric::FirstReplyTime.id
      pm.target         = 6
      pm.business_hours = true
    end

    @presenter = Api::V2::Slas::PolicyMetricPresenter.new(@account.owner,
      url_builder: mock_url_builder)
  end

  should_present_keys :priority,
    :metric,
    :target,
    :business_hours

  should_present_method :target, value: 6
  should_present_method :business_hours, value: true
  should_present_method :name, as: :priority, value: 'high'

  should_present_method :name,
    as:    :metric,
    value: Zendesk::Sla::TicketMetric::FirstReplyTime.name
end
