require_relative '../../../../support/test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered!

describe Api::V2::Slas::ErrorsPresenter do
  include TicketMetricHelper
  extend  Api::Presentation::TestHelper

  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @model   = new_sla_policy(account: @account)

    id = Zendesk::Sla::TicketMetric::FirstReplyTime.id

    @model.position = 'foo'
    @model.policy_metrics.build do |pm|
      pm.account        = @account
      pm.business_hours = false
      pm.policy         = @model
      pm.priority_id    = -1
      pm.metric_id      = id
      pm.target         = 30
    end

    refute @model.valid? # Populate model.errors

    @json = Api::V2::Slas::ErrorsPresenter.new.present(@model)
  end

  it 'presents errors' do
    assert_equal 'Position is not a number', @json[:details][:position].first[:description]
  end

  describe 'associations' do
    before do
      @json  = @json[:details][:policy_metrics].first
      @error = @json[:details][:priority].first[:description]
    end

    it 'presents errors' do
      assert_equal 'Priority: is not a recognized value', @error
    end
  end
end
