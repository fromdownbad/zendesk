require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Slas::PolicyFilterPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account   = accounts(:minimum)
    @model     = Definition.build(Hashie::Mash.new(
      conditions_all: [{
        'source'   => 'current_tags',
        'operator' => 'is',
        'value'    => 'list of tags'
      }],
      conditions_any: []
    ))
    @presenter = Api::V2::Slas::PolicyFilterPresenter.new(
      @account.owner,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :all, :any

  should_present_method :all, value: [{
    field: 'current_tags',
    operator: 'is',
    value: 'list of tags'
  }]
  should_present_method :any, value: []
end
