require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Slas::PolicyFilterPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account   = accounts(:minimum)
    @model     = Hashie::Mash.new(
      source: 'ticket_type_id',
      operator: 'is',
      first_value: '2'
    )
    @presenter = Api::V2::Slas::PolicyFilterItemPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :field, :operator, :value

  should_present_method :field,     value: 'ticket_type_id'
  should_present_method :operator,  value: 'is'
  should_present_method :value,     value: '2'
end
