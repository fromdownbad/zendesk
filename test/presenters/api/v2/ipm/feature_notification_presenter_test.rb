require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Ipm::FeatureNotificationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  before do
    @model     = FactoryBot.create(:ipm_feature_notification)
    @presenter = Api::V2::Ipm::FeatureNotificationPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder, internal: internal)
  end

  describe "public" do
    let(:internal) { false }
    should_present_keys(
      :id,
      :title,
      :body,
      :image_name,
      :call_to_action_text,
      :call_to_action_url,
      :learn_more_url,
      :created_at,
      :updated_at
    )
  end

  describe "internal" do
    let(:internal) { true }
    should_present_keys(
      :id,
      :title,
      :body,
      :image_name,
      :call_to_action_text,
      :call_to_action_url,
      :learn_more_url,
      :category_code,
      :created_at,
      :updated_at,
      :state,
      :status,
      :start_date,
      :end_date,
      :test_mode,
      :internal_name,
      :pods,
      :shards,
      :roles,
      :languages,
      :account_types,
      :interfaces,
      :plans,
      :account_ids,
      :exclude_account_ids,
      :subdomains,
      :exclude_subdomains
    )
  end
end
