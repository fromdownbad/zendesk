require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AlertPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  before do
    @model     = FactoryBot.create(:ipm_alert_with_constraints)
    @presenter = Api::V2::Ipm::AlertPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder, internal: internal)
  end

  describe "public" do
    let(:internal) { false }
    should_present_keys(
      :id,
      :text,
      :link_url,
      :link_text,
      :created_at,
      :updated_at
    )
  end

  describe "internal" do
    let(:internal) { true }
    should_present_keys(
      :id,
      :text,
      :link_url,
      :link_text,
      :created_at,
      :updated_at,
      :state,
      :status,
      :start_date,
      :end_date,
      :test_mode,
      :internal_name,
      :pods,
      :shards,
      :roles,
      :languages,
      :account_types,
      :interfaces,
      :plans,
      :account_ids,
      :exclude_account_ids,
      :subdomains,
      :exclude_subdomains,
      :custom_factors
    )
  end
end
