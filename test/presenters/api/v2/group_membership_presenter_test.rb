require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::GroupMembershipPresenter do
  extend Api::Presentation::TestHelper
  fixtures :memberships, :accounts, :users, :groups

  describe "without includes" do
    before do
      @model     = memberships(:minimum)
      @presenter = Api::V2::GroupMembershipPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    end

    should_present_keys :id, :user_id, :group_id, :created_at, :updated_at, :url, :default

    should_present_method :id
    should_present_method :user_id
    should_present_method :group_id
    should_present_method :default
    should_present_method :created_at
    should_present_method :updated_at

    should_present_url :api_v2_group_membership_url
  end

  describe "with includes" do
    before do
      @model     = memberships(:minimum)
      @presenter = Api::V2::GroupMembershipPresenter.new(@model.account.owner, url_builder: mock_url_builder, includes: [:users, :groups])
      @user_presenter = Api::V2::Users::Presenter.new(@model.account.owner, url_builder: mock_url_builder)
      @group_presenter = Api::V2::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    end

    should_present_keys :id, :user_id, :group_id, :created_at, :updated_at, :url, :default

    should_present_method :id
    should_present_method :user_id
    should_present_method :group_id
    should_present_method :default
    should_present_method :created_at
    should_present_method :updated_at

    should_present_url :api_v2_group_membership_url
    should_side_load :users, :groups

    describe_with_arturo_enabled :api_v2_group_membership_full_sideload do
      it "presents all keys from sideloads" do
        assert_equal @user_presenter.model_json(users(:minimum_end_user)).keys, @presenter.as_json(@model)[:users].first.keys
        assert_equal @group_presenter.model_json(groups(:minimum_group)).keys, @presenter.as_json(@model)[:groups].first.keys
      end
    end

    describe_with_arturo_disabled :api_v2_group_membership_full_sideload do
      it "presents consdensed keys from sideloads" do
        assert_equal @user_presenter.condensed_model_json(users(:minimum_end_user)).keys, @presenter.as_json(@model)[:users].first.keys
        assert_equal @group_presenter.condensed_model_json(groups(:minimum_group)).keys, @presenter.as_json(@model)[:groups].first.keys
      end
    end
  end
end
