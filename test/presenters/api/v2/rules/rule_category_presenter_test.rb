require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::RuleCategoryPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  let(:account) { accounts(:minimum) }
  let(:model) { create_category(rule_type: Trigger) }
  let(:includes_collection) { [] }
  let(:presenter) do
    Api::V2::Rules::RuleCategoryPresenter.new(
      users(:minimum_agent),
      url_builder: mock_url_builder,
      includes: includes_collection,
      rule_type: 'Trigger'
    )
  end

  before do
    @model = model
    @presenter = presenter
  end

  should_present_keys :url, :id, :name, :updated_at, :created_at, :position
  should_present_methods :name, :updated_at, :created_at, :position
  should_present_url :api_v2_rule_category_url

  describe '#model_json' do
    it 'should present id as a string' do
      assert_equal model.id.to_s, presenter.model_json(model)[:id]
    end
  end

  it 'can present a collection' do
    result = presenter.present([model])

    assert_equal 1, result[:rule_categories].size
    assert_equal model.id.to_s, result[:rule_categories].first[:id]
  end

  describe 'when sideloading rule counts' do
    let(:includes_collection) { %i[rule_counts] }

    it 'returns rule counts in the response' do
      result = presenter.present([model])
      create_trigger(rules_category_id: model.id, active: true)
      create_trigger(rules_category_id: model.id, active: true)
      create_trigger(rules_category_id: model.id, active: true)

      create_trigger(rules_category_id: model.id, active: false)
      create_trigger(rules_category_id: model.id, active: false)

      assert 3, result[:rule_categories].first[:active_count]
      assert 2, result[:rule_categories].first[:inactive_count]
    end
  end
end
