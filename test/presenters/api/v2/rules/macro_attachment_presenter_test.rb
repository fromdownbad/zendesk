require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::MacroAttachmentPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:presenter) do
    Api::V2::Rules::MacroAttachmentPresenter.new(
      users(:minimum_agent),
      url_builder: mock_url_builder
    )
  end

  describe '#model_json' do
    let(:model) do
      Object.new.tap do |macro_attachment|
        macro_attachment.instance_eval do
          def id
            100
          end

          def filename
            'screenshot.jpg'
          end

          def content_type
            'image/jpeg'
          end

          def size
            4_084
          end

          def created_at
            '2016-08-15T16:04:06Z'
          end
        end
      end
    end

    let(:json) { presenter.model_json(model) }

    let(:content_url) do
      'https://minimum.zendesk-test.com/api/v2/macro/attachments/100/content'
    end

    before do
      presenter.
        url_builder.
        stubs(:content_api_v2_macro_attachment_url).
        with(id: model.id).
        returns(content_url)
    end

    it 'presents `id`' do
      assert_equal 100, json[:id]
    end

    it 'presents `filename`' do
      assert_equal 'screenshot.jpg', json[:filename]
    end

    it 'presents `content_type`' do
      assert_equal 'image/jpeg', json[:content_type]
    end

    it 'presents `content_url`' do
      assert_equal content_url, json[:content_url]
    end

    it 'presents `size`' do
      assert_equal 4_084, json[:size]
    end

    it 'presents `created_at`' do
      assert_equal '2016-08-15T16:04:06Z', json[:created_at]
    end

    it 'presents `url`' do
      assert_equal :api_v2_macro_attachment_url, json[:url]
    end
  end
end
