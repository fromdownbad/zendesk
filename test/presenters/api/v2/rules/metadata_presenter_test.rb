require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::MetadataPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:presenter) do
    Api::V2::Rules::MetadataPresenter.new(user, url_builder: mock_url_builder)
  end

  let(:definition_metadata) do
    stub('definition metadata').tap do |metadata|
      metadata.stubs(:valid_key).with(rule_aspect.subject).returns('Test')
    end
  end

  let(:rule_aspect) do
    stub('rule aspect', subject: 'test', metadata: metadata_keys)
  end

  before do
    Zendesk::Rules::DefinitionMetadata.
      stubs(:new).
      with(account, user).
      returns(definition_metadata)
  end

  describe '#model_json' do
    describe 'when all of the specified metadata are supported' do
      let(:metadata_keys) { %i[valid_key] }

      it 'returns the metadata' do
        assert_equal(
          {metadata: {valid_key: 'Test'}},
          presenter.present(rule_aspect)
        )
      end
    end

    describe 'when not all of the specified metadata are not supported' do
      let(:metadata_keys) { %i[invalid_key valid_key] }

      it 'does not return any metadata' do
        assert_equal({}, presenter.present(rule_aspect))
      end
    end
  end
end
