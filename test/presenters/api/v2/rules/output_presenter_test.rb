require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Rules::OutputPresenter do
  extend Api::Presentation::TestHelper
  fixtures :rules, :accounts, :users, :ticket_fields

  before do
    @view      = rules(:view_assigned_working_tickets)
    @model     = @view.output
    @presenter = Api::V2::Rules::OutputPresenter.new(@view.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :fields, :custom_fields, :group, :sort, :columns,
    :group_by, :group_order, :sort_by, :sort_order

  describe "group" do
    subject { @presenter.present(@model)[:execution][:group] }

    describe "with a group_by" do
      describe "with a rule field" do
        before do
          @model.stubs(group: :description, group_order: "stub_string")
        end

        it "outputs rule id, title, and order" do
          assert_equal({ id: "subject", title: "Subject", order: "stub_string" }, subject)
        end
      end

      describe "with something else?" do
        before do
          @model.stubs(group: :my_weird_field, group_order: "stub_string")
        end

        it "outputs rule id, order" do
          assert_equal({ id: :my_weird_field, order: "stub_string" }, subject)
        end
      end
    end

    describe "without a group_by" do
      before do
        @model.stubs(group: nil)
      end

      it "is nil" do
        assert_nil subject
      end
    end
  end

  describe "sort" do
    subject { @presenter.present(@model)[:execution][:sort] }

    describe "with a sort_by" do
      describe "with a rule field" do
        before do
          @model.stubs(order: :description, sort_order: "stub_string")
        end

        it "outputs rule id, title, and order" do
          assert_equal({ id: "subject", title: "Subject", order: "stub_string" }, subject)
        end
      end

      describe "with something else?" do
        before do
          @model.stubs(order: :my_weird_field, sort_order: "stub_string")
        end

        it "outputs rule id, order" do
          assert_equal({ id: :my_weird_field, order: "stub_string" }, subject)
        end
      end
    end

    describe "without a sort_by" do
      before do
        @model.stubs(order: nil)
      end

      it "is nil" do
        assert_nil subject
      end
    end
  end

  describe 'presenting columns' do
    let(:columns) { @presenter.present(@model)[:execution][:columns] }

    before do
      @custom_field = ticket_fields(:field_integer_custom)

      @model.columns.replace(
        [:score, @custom_field.id, :priority, :updated]
      )
    end

    it 'presents custom and system fields in order' do
      assert_equal(
        [
          {
            id:    'score',
            title: 'Score'
          },
          {
            id:    @custom_field.id,
            title: @custom_field.title,
            type:  'integer',
            url:   :api_v2_ticket_field_url
          },
          {
            id:    'priority',
            title: 'Priority'
          },
          {
            id:    'updated',
            title: 'Updated'
          }
        ],
        columns
      )
    end
  end

  describe 'presenting system fields' do
    let(:fields) { @presenter.present(@model)[:execution][:fields] }

    before { @model.columns.replace(%i[score priority updated]) }

    it 'presents system fields with titles' do
      assert_equal(
        [
          {id: 'score',    title: 'Score'   },
          {id: 'priority', title: 'Priority'},
          {id: 'updated',  title: 'Updated' }
        ],
        fields
      )
    end
  end

  describe 'presenting custom fields' do
    let(:custom_fields) do
      @presenter.present(@model)[:execution][:custom_fields]
    end

    before do
      @custom_field = ticket_fields(:field_integer_custom)

      @model.columns.replace([@custom_field.id])
    end

    it 'presents custom fields with ids' do
      assert_equal(
        [
          {
            id:    @custom_field.id,
            title: @custom_field.title,
            type:  'integer',
            url:   :api_v2_ticket_field_url
          }
        ],
        custom_fields
      )
    end
  end
end
