require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::UserViewPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :rules,
    :users

  let(:user_view) { rules(:minimum_custom_fields_view) }

  let(:account) { user_view.account }
  let(:user)    { account.owner }

  let(:includes_collection) { [] }

  let(:presenter) do
    Api::V2::Rules::UserViewPresenter.new(user,
      url_builder: mock_url_builder,
      user_view:   user_view,
      includes:    includes_collection)
  end

  before do
    @model     = user_view
    @presenter = presenter
  end

  should_present_keys :id,
    :title,
    :raw_title,
    :active,
    :restriction,
    :execution,
    :conditions,
    :updated_at,
    :created_at,
    :url

  should_present_method :id
  should_present_method :title
  should_present_method :is_active?, as: :active
  should_present_method(:conditions,
    value: {
      all: [{
        field:    'custom_fields.date1',
        operator: 'is_not',
        value:    '2012-12-12'
      }],
      any: []
    })
  should_present_method :updated_at
  should_present_method :created_at

  describe '#model_json' do
    let(:json) { presenter.model_json(user_view) }

    describe 'when the owner is an account' do
      before { user_view.stubs(owner: account) }

      it 'presents no restrictions' do
        assert_nil json[:restriction]
      end
    end

    describe 'when the owner is a group' do
      let(:group) { groups(:minimum_group) }

      before { user_view.stubs(owner: group) }

      it 'presents the restriction' do
        assert_equal({type: 'Group', id: group.id}, json[:restriction])
      end
    end

    describe 'when the owner is a user' do
      before { user_view.stubs(owner: user) }

      it 'presents the restriction' do
        assert_equal({type: 'User', id: user.id}, json[:restriction])
      end
    end

    describe 'when the title contains dynamic content' do
      before do
        account.cms_texts.create!(
          account: account,
          name:    'user_view_title',
          fallback_attributes: {
            is_fallback:          'true',
            nested:                true,
            translation_locale_id: 1,
            value:                 'Rendered Title'
          }
        )

        user_view.title = '{{dc.user_view_title}}'
      end

      it 'renders the dynamic content in the title' do
        assert_equal(
          'Rendered Title',
          presenter.model_json(user_view)[:title]
        )
      end
    end

    describe 'when side-loading permissions' do
      let(:includes_collection) { [:permissions] }

      let(:user) { users(:minimum_agent) }

      should_present_keys :id,
        :title,
        :raw_title,
        :active,
        :restriction,
        :execution,
        :conditions,
        :updated_at,
        :created_at,
        :url,
        :permissions

      describe 'when the user cannot edit the user view' do
        before { user.stubs(:can?).with(:edit, user_view).returns(false) }

        it 'sets `can_edit` to false' do
          refute json[:permissions][:can_edit]
        end
      end

      describe 'when the user can edit the user view' do
        before { user.stubs(:can?).with(:edit, user_view).returns(true) }

        it 'sets `can_edit` to true' do
          assert json[:permissions][:can_edit]
        end
      end
    end
  end
end
