require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 0

describe Api::V2::Rules::FilteredViewRowsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :rules, :organizations, :tickets

  let(:view)       { rules(:view_assigned_working_tickets) }
  let(:account)    { accounts(:minimum) }
  let(:user)       { view.account.owner }
  let(:collection) { view.find_tickets(user) }
  let(:side_loads) { [:view] }

  let(:presenter) do
    Api::V2::Rules::FilteredViewRowsPresenter.new(
      user,
      view:        view,
      url_builder: mock_url_builder,
      includes:    side_loads
    )
  end

  before do
    Zendesk::Rules::RuleExecuter.
      any_instance.
      expects(:find_tickets_with_occam_client).
      returns(Array(tickets(:minimum_1)))
  end

  describe 'when presenting a collection' do
    let(:collection_presenter) { stub(:collection_presenter) }

    before do
      Api::V2::Rules::FilteredViewCollectionPresenter.
        stubs(:new).
        returns(collection_presenter)

      presenter.present(collection)
    end

    before_should 'present with `FilteredViewCollectionPresenter`' do
      collection_presenter.expects(:as_json).with(collection)
    end
  end

  describe 'when there are sufficient results' do
    it 'does not append `insufficient_results`' do
      refute presenter.present(collection).key?(:insufficient_results)
    end
  end

  describe 'when results are insufficient' do
    let(:side_loads) { [:view, :insufficient_results] }

    it 'appends `insufficient_results`' do
      assert presenter.present(collection)[:insufficient_results]
    end
  end
end
