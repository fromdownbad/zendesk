require_relative '../../../../support/test_helper'
require_relative '../../../../support/collection_presenter_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::RuleCategoryCollectionPresenter do
  extend Api::Presentation::TestHelper

  include CollectionPresenterHelper
  include TestSupport::Rule::Helper

  fixtures :accounts, :users

  let(:user) { users(:minimum_admin) }
  let(:account) { user.account }

  let(:includes_collection) { [] }

  let(:rule_class) { Trigger }

  let(:item_presenter) do
    Api::V2::Rules::RuleCategoryPresenter.new(
      user,
      model_key: model_key,
      url_builder: mock_url_builder,
      includes: includes_collection,
      rule_type: rule_class.name
    )
  end

  let(:presenter) do
    Api::V2::Rules::RuleCategoryCollectionPresenter.new(
      user,
      url_builder: mock_url_builder,
      item_presenter: item_presenter,
      includes: includes_collection,
      rule_type: item_presenter.rule_type
    )
  end

  let(:model_key) { "#{rule_class.name.downcase}_category" }
  let(:collection_key) { model_key.pluralize.to_sym }

  let(:collection) { RuleCategory.where(account_id: account.id, rule_type: rule_class) }

  let(:json) { presenter.present(collection) }

  before do
    RuleCategory.destroy_all
    rule_class.destroy_all

    create_category(position: 1, rule_type: rule_class.name)
    create_category(position: 2, rule_type: rule_class.name)

    @collection = collection
    @presenter  = presenter
    @model_key  = collection_key
  end

  should_present_collection_keys :id,
    :name,
    :position,
    :updated_at,
    :created_at,
    :url

  describe 'when side loading rule counts' do
    let(:category_1) { collection.first }
    let(:category_2) { collection.second }

    let(:includes_collection) { %i[rule_counts] }

    describe 'and there are both active and inactive rules' do
      before do
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: false)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: false)

        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: false)
      end

      it 'side-loads rule counts' do
        assert_equal 3, json[collection_key].first[:active_count]
        assert_equal 2, json[collection_key].first[:inactive_count]

        assert_equal 4, json[collection_key].second[:active_count]
        assert_equal 1, json[collection_key].second[:inactive_count]
      end

      it 'does not do N + 1s' do
        assert_no_n_plus_one do
          presenter.present(collection)
        end
      end
    end

    describe 'and there are either active or inactive rules' do
      before do
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: true)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_1.id, active: true)

        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: false)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: false)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: false)
        send("create_#{rule_class.name.downcase}", rules_category_id: category_2.id, active: false)
      end

      it 'does not omit counts with 0' do
        assert_equal 3, json[collection_key].first[:active_count]
        assert_equal 0, json[collection_key].first[:inactive_count]

        assert_equal 0, json[collection_key].second[:active_count]
        assert_equal 4, json[collection_key].second[:inactive_count]
      end
    end

    describe 'and there are no rules' do
      it 'displays counts with 0' do
        assert_equal 0, json[collection_key].first[:active_count]
        assert_equal 0, json[collection_key].first[:inactive_count]
      end
    end

    describe 'and rule counts is not included' do
      let(:includes_collection) { [] }

      it 'does not side load rule counts' do
        refute json[collection_key].any? { |rule| rule.key?(:active_count) or rule.key?(:inactive_count) }
      end
    end
  end
end
