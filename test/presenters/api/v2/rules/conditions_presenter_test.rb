require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::ConditionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :rules

  let(:view) { rules(:view_assigned_working_tickets) }
  let(:user) { view.account.owner }

  let(:presenter) do
    Api::V2::Rules::ConditionsPresenter.new(user, url_builder: mock_url_builder)
  end

  before do
    @model     = view.definition
    @presenter = presenter
  end

  should_present_keys :all, :any

  should_present_method :conditions_all, as: :all, value: []
  should_present_method :conditions_any, as: :any, value: []

  describe '#definition_json' do
    let(:definition_item) do
      DefinitionItem.new('status_id', 'less_than', [StatusType.CLOSED])
    end

    it 'presents the ticket attribute name' do
      assert_equal(
        [{field: :status, operator: 'less_than', value: 'closed'}],
        presenter.present(
          OpenStruct.new(
            conditions_all: [definition_item],
            conditions_any: []
          )
        )[:conditions][:all]
      )
    end

    describe 'when the definition includes a recipient mail with capital letters' do
      let(:definition_item) do
        DefinitionItem.new('recipient', 'is', 'fooBar@GMAIL.COM')
      end

      it 'presents the recipient mail downcased' do
        assert_equal(
          [{field: 'recipient', operator: 'is', value: 'foobar@gmail.com'}],
          presenter.present(
            OpenStruct.new(
              conditions_all: [definition_item],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end

    describe 'when the definition includes `current_via_id`' do
      let(:definition_item) do
        DefinitionItem.new('current_via_id', 'is', [ViaType.CLOSED_TICKET])
      end

      it 'presents the current via ID condition' do
        assert_equal(
          [{field: 'current_via_id', operator: 'is', value: 27}],
          presenter.present(
            OpenStruct.new(
              conditions_all: [definition_item],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end

    describe 'when the definition includes `via_id`' do
      let(:definition_item) do
        DefinitionItem.new('via_id', 'is', [ViaType.CLOSED_TICKET])
      end

      it 'presents the via ID condition' do
        assert_equal(
          [{field: 'via_id', operator: 'is', value: 27}],
          presenter.present(
            OpenStruct.new(
              conditions_all: [definition_item],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end

    describe 'when the definition includes `satisfaction_score`' do
      let(:definition_item) do
        DefinitionItem.new(
          'satisfaction_score',
          'is',
          [SatisfactionType.BADWITHCOMMENT]
        )
      end

      it 'presents the satisfaction score condition' do
        assert_equal(
          [
            {
              field:    'satisfaction_score',
              operator: 'is',
              value:    'bad_with_comment'
            }
          ],
          presenter.present(
            OpenStruct.new(
              conditions_all: [definition_item],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end

    describe 'when the definition has a legacy subject' do
      it 'presents the sanitized definition' do
        assert_equal(
          [
            {
              field:    'requester_id',
              operator: 'value_previous',
              value:    'current_user'
            }
          ],
          presenter.present(
            OpenStruct.new(
              conditions_all: [
                DefinitionItem.new(
                  'requester_id#this_is_very_old',
                  'value_previous',
                  'current_user'
                )
              ],
              conditions_any: []
            )
          )[:conditions][:all]
        )
      end
    end
  end
end
