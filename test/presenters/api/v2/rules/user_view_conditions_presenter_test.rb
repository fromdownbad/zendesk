require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Rules::UserViewConditionsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :rules

  before do
    @view = rules(:minimum_custom_fields_view)
    @model = @view.definition
    @presenter = Api::V2::Rules::UserViewConditionsPresenter.new(@view.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :all, :any

  should_present_method :conditions_all, as: :all, value: []
  should_present_method :conditions_any, as: :any, value: []

  it "presents all conditions" do
    subject = @presenter.present(@model)
    assert subject[:conditions]
    assert_equal([{
      field: "custom_fields.date1",
      operator: "is_not",
      value: "2012-12-12"
}], subject[:conditions][:all])
  end

  it "presents organizations with their names" do
    condition = @model.conditions_all.first
    organization = @view.account.organizations.first
    condition.source = "organization_id"
    condition.operator = "is"
    condition.value = organization.id
    subject = @presenter.present(@model)
    assert subject[:conditions]
    assert_equal([{
      field: "organization_id",
      operator: "is",
      value: organization.id,
      label: organization.name
}], subject[:conditions][:all])
  end

  describe "when presenting conditions with presence operators" do
    it "presents conditions without labels" do
      condition = @model.conditions_all.first
      condition.source = "organization_id"
      condition.operator = "is_present"
      condition.value = nil
      subject = @presenter.present(@model)
      assert subject[:conditions]
      assert_equal [{
        field: "organization_id",
        operator: "is_present",
        value: nil
      }], subject[:conditions][:all]
    end
  end
end
