require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::ViewPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :accounts,
    :rules,
    :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:view) { create_view }

  let(:highlights) { nil }

  let(:includes_collection) { [] }

  let(:presenter) do
    Api::V2::Rules::ViewPresenter.new(
      user,
      url_builder: mock_url_builder,
      highlights:  highlights,
      includes:    includes_collection
    )
  end

  before do
    @model     = view
    @presenter = presenter

    View.any_instance.stubs(watchable?: true)
  end

  should_present_keys :id,
    :title,
    :raw_title,
    :active,
    :restriction,
    :position,
    :execution,
    :conditions,
    :updated_at,
    :created_at,
    :url,
    :description,
    :watchable

  should_present_method :id
  should_present_method :title
  should_present_method :is_active?, as: :active
  should_present_method :position
  should_present_method :output,     as: :execution, with: Api::V2::Rules::OutputPresenter
  should_present_method :conditions, with: Api::V2::Rules::ConditionsPresenter
  should_present_method :updated_at
  should_present_method :created_at
  should_present_method :description

  describe '#model_json' do
    let(:json) { presenter.model_json(view) }

    describe 'when highlights are present' do
      let(:highlights) { {view.id => '<em>highlight</em>'} }

      it 'presents the highlights' do
        assert_equal '<em>highlight</em>', json[:highlights]
      end
    end

    describe 'when the owner is an account' do
      before { view.stubs(owner: view.account) }

      it 'presents no restrictions' do
        assert_nil json[:restriction]
      end
    end

    describe 'when the owner is a group' do
      let(:view) { rules(:active_view_for_minimum_group) }

      describe_with_arturo_enabled :multiple_group_views_reads do
        let(:group_1) { groups(:minimum_group) }
        let(:group_2) { groups(:trial_group) }

        before do
          GroupView.destroy_all

          view.owner_type = 'Group'

          [group_1, group_2].each do |group|
            GroupView.create! do |group_view|
              group_view.account_id = account.id
              group_view.group_id   = group.id
              group_view.view_id    = view.id
            end
          end
        end

        it 'presents the restriction' do
          assert_equal(
            {type: 'Group', id: group_1.id, ids: [group_1.id, group_2.id]},
            json[:restriction]
          )
        end
      end

      describe_with_arturo_disabled :multiple_group_views_reads do
        it 'presents the group restriction' do
          assert_equal({type: 'Group', id: view.owner_id}, json[:restriction])
        end
      end
    end

    describe 'when the owner is a user' do
      before { view.owner = user }

      it 'presents the restriction' do
        assert_equal({type: 'User', id: user.id}, json[:restriction])
      end
    end

    describe 'when the title contains dynamic content' do
      let(:view_with_dc) { create_view(title: '{{dc.view_title}}') }

      before do
        account.cms_texts.create!(
          account: account,
          name:    'view_title',
          fallback_attributes: {
            is_fallback:          'true',
            nested:                true,
            translation_locale_id: 1,
            value:                 'Rendered Title'
          }
        )
      end

      it 'renders the dynamic content in the title' do
        assert_equal(
          'Rendered Title',
          presenter.model_json(view_with_dc)[:title]
        )
      end

      describe 'and highlights are present' do
        let(:highlights) { {view_with_dc.id => '<em>dc</em>'} }

        it 'does not present the highlights' do
          assert_nil presenter.model_json(view_with_dc)[:highlights]
        end
      end
    end

    describe 'when side-loading permissions' do
      let(:includes_collection) { [:permissions] }

      let(:user) { users(:minimum_agent) }

      describe 'and the user cannot edit the view' do
        before { user.stubs(:can?).with(:edit, view).returns(false) }

        it 'sets `can_edit` to false' do
          refute json[:permissions][:can_edit]
        end
      end

      describe 'and the user can edit the view' do
        before { user.stubs(:can?).with(:edit, view).returns(true) }

        it 'sets `can_edit` to true' do
          assert json[:permissions][:can_edit]
        end
      end
    end

    describe 'when side-loading the app installation' do
      let(:includes_collection) { %i[app_installation] }

      before do
        view.stubs(:app_installation).returns(
          'id'       => 42,
          'settings' => {'title' => 'Test app'}
        )
      end

      it 'side-loads the app installation' do
        assert_equal 42, presenter.model_json(view)[:app_installation]['id']
      end
    end
  end

  describe 'when presenting a collection' do
    let(:json)       { presenter.present(collection) }
    let(:collection) { View.where(account_id: account.id) }

    before do
      View.destroy_all

      create_view(title: 'Urgent, so urgent')
      create_view(title: 'Assigned to me')
      create_view(title: 'Unassigned')
    end

    it 'presents a collection' do
      assert_equal(
        [
          'Urgent, so urgent',
          'Assigned to me',
          'Unassigned'
        ],
        json[:views].map { |view| view[:title] }
      )
    end

    describe 'and side-loading app installations' do
      let(:includes_collection) { %i[app_installation] }

      before do
        Rule.stubs(:apps_installations).returns(
          View.first.id => {
            'id'       => 42,
            'settings' => {'title' => 'Test app'}
          }
        )
      end

      it 'side-loads the app installations' do
        assert_equal 42, json[:views].first[:app_installation]['id']
      end

      describe 'and it is presented' do
        before { presenter.present(collection) }

        before_should 'make a single request for app installations' do
          Rule.expects(:apps_installations).returns(
            View.first.id => {
              'id'       => 42,
              'settings' => {'title' => 'Test app'}
            }
          ).once
        end
      end
    end
  end
end
