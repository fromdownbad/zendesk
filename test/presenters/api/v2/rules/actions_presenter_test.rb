require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::ActionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :rules

  let(:presenter) do
    Api::V2::Rules::ActionsPresenter.new(rule.account.owner,
      url_builder: mock_url_builder)
  end

  let(:rule) { rules(:macro_incident_escalation) }

  describe '#model_json' do
    let(:json) { presenter.model_json(rule.definition) }

    it 'presents all actions' do
      assert_equal rule.actions.size, json.size
    end

    it 'maps action source to correct field' do
      assert_equal :status, json.first[:field]
    end

    it 'maps action value to correct value' do
      assert_equal 'open', json.first[:value]
    end

    describe 'when mapped action value is nil' do
      before do
        rule.definition.actions.find do |action|
          action.source == 'ticket_type_id'
        end.value = '0'
      end

      it 'shows empty string' do
        assert_equal '', json.find { |action| action[:field] == :type }[:value]
      end
    end

    describe 'when action value represents a boolean' do
      it 'shows the value as string' do
        assert_equal 'false', json.find { |action| action[:field] == 'comment_mode_is_public' }[:value]
      end
    end

    describe 'when the definition has a legacy subject' do
      it 'presents the sanitized definition' do
        assert_equal(
          [{field: 'brand_id', value: '10001'}],
          presenter.present(
            OpenStruct.new(
              conditions_all: [],
              conditions_any: [],
              actions:        [
                DefinitionItem.new('brand_id#this_is_very_old', nil, '10001')
              ]
            )
          )[:actions]
        )
      end
    end
  end
end
