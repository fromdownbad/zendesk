require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::TriggerReplicaPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:model)   { create_trigger }

  let(:presenter) do
    Api::V2::Rules::TriggerReplicaPresenter.new(
      users(:minimum_agent),
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = model
    @presenter = presenter
  end

  describe '#model_json' do
    let(:json) { presenter.model_json(create_trigger) }

    it 'hides extraneous attributes' do
      assert(
        %i[active created_at id updated_at url].none? do |attribute|
          json.key?(attribute)
        end
      )
    end
  end

  describe '#url' do
    it 'returns nil' do
      assert_nil presenter.url
    end
  end

  should_present_keys :title,
    :raw_title,
    :description,
    :conditions,
    :actions,
    :position
end
