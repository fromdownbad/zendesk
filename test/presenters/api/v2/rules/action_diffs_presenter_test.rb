require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::ActionDiffsPresenter do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }

  let(:presenter) do
    Api::V2::Rules::ActionDiffsPresenter.new(
      trigger.account.owner,
      url_builder: mock_url_builder
    )
  end

  let(:trigger) do
    create_trigger(
      definition: build_trigger_definition(
        actions: [
          build_definition_item(field: 'status_id', operator: 'is', value: 1)
        ]
      )
    )
  end

  let(:revision_diff) do
    create_trigger_revision_diff(trigger.revisions.first, trigger.revisions.first)
  end

  describe '#model_json' do
    let(:json) { presenter.model_json(revision_diff) }

    it 'presents the mapped attribute field' do
      assert_equal :status, json.first[:field].first[:content]
    end

    it 'presents the mapped attribute value' do
      assert_equal 'open', json.first[:value].first[:content]
    end

    describe 'when mapping the value of an action returns `nil`' do
      before do
        Api::V2::Tickets::AttributeMappings.
          stubs(:ticket_attribute_value).
          returns(nil)
      end

      it 'maps the value to an empty string' do
        assert_empty json.first[:value].first[:content]
      end
    end
  end
end
