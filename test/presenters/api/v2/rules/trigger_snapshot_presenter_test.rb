require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::TriggerSnapshotPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users

  let(:account) { accounts(:minimum) }

  before do
    @model     = create_trigger.revisions.first.snapshot
    @presenter = Api::V2::Rules::TriggerSnapshotPresenter.new(
      users(:minimum_agent),
      url_builder: mock_url_builder
    )
  end

  should_present_keys :title,
    :active,
    :conditions,
    :actions,
    :description

  should_present_method :actions,    with: Api::V2::Rules::ActionsPresenter
  should_present_method :conditions, with: Api::V2::Rules::ConditionsPresenter
  should_present_method :is_active?, as: :active

  should_present_methods :title,
    :description
end
