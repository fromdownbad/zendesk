require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::ConditionDefinitionPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:metadata_presenter) do
    Api::V2::Rules::MetadataPresenter.new(user, url_builder: mock_url_builder)
  end

  let(:definition_metadata) { stub('definition metadata', valid_key: 'Test') }

  let(:presenter) do
    Api::V2::Rules::ConditionDefinitionPresenter.new(
      user,
      url_builder:        mock_url_builder,
      metadata_presenter: metadata_presenter
    )
  end

  let(:source)        { 'ticket_form_id' }
  let(:list)          { [{value: 1, title: 'One'}, {value: 2, title: 'Two'}] }
  let(:operator_list) { [] }
  let(:type)          { 'list' }

  let(:condition_definition) do
    Zendesk::Rules::Condition.new(
      OpenStruct.new(
        source:        source,
        title:         'title',
        type:          type,
        group:         'ticket',
        list:          list,
        operator_list: operator_list
      ),
      account: account
    )
  end

  before do
    Zendesk::Rules::DefinitionMetadata.
      stubs(:new).
      with(account, user).
      returns(definition_metadata)

    @model     = condition_definition
    @presenter = presenter
  end

  should_present_keys :title,
    :type,
    :subject,
    :group,
    :nullable,
    :repeatable,
    :operators,
    :values

  should_present_method :nullable?,   as: :nullable
  should_present_method :repeatable?, as: :repeatable

  describe 'when the condition contains metadata' do
    before { condition_definition.stubs(metadata: %i[valid_key]) }

    should_present_keys :title,
      :type,
      :subject,
      :group,
      :nullable,
      :repeatable,
      :operators,
      :values,
      :metadata
  end

  describe 'when the condition contains formatted operators' do
    describe 'and it is a `date` condition' do
      let(:type) { 'date' }

      let(:operator_list) do
        [
          {value: 'is_date',    title: 'Date'},
          {value: 'is_integer', title: 'Integer', field_type: 'integer'}
        ]
      end

      should_present_keys :title,
        :type,
        :subject,
        :group,
        :nullable,
        :repeatable,
        :operators,
        :values

      it 'presents the formatted operators' do
        assert_equal(
          [
            {
              value:    'is_date',
              title:    'Date',
              terminal: false,
              format:   'date'
            },
            {
              value:    'is_integer',
              title:    'Integer',
              terminal: false,
              format:   'integer'
            }
          ],
          presenter.model_json(condition_definition)[:operators]
        )
      end
    end
  end

  describe 'when the condition contains non-formatted operators' do
    let(:operator_list) do
      [{value: 'is', title: 'Is'}, {value: 'is_not', title: 'Is not'}]
    end

    should_present_keys :title,
      :type,
      :subject,
      :group,
      :nullable,
      :repeatable,
      :operators,
      :values

    it 'presents the operators' do
      assert_equal(
        [
          {value: 'is',     title: 'Is',     terminal: false},
          {value: 'is_not', title: 'Is not', terminal: false}
        ],
        presenter.model_json(condition_definition)[:operators]
      )
    end
  end

  describe 'when the condition does not contain values' do
    let(:list) { [] }

    should_present_keys :title,
      :type,
      :subject,
      :group,
      :nullable,
      :operators,
      :repeatable
  end
end
