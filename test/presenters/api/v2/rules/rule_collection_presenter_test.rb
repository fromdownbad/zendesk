require_relative '../../../../support/test_helper'
require_relative '../../../../support/collection_presenter_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::RuleCollectionPresenter do
  extend Api::Presentation::TestHelper

  include CollectionPresenterHelper
  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  fixtures :all

  let(:user)    { users(:minimum_admin) }
  let(:account) { user.account }

  let(:includes_collection) { [] }
  let(:allowed_side_loads)  { [] }

  let(:rule_class)           { View }
  let(:item_presenter_class) { Api::V2::Rules::ViewPresenter }

  let(:item_presenter) do
    item_presenter_class.new(
      user,
      url_builder: mock_url_builder,
      includes:    includes_collection
    )
  end

  let(:presenter) do
    Api::V2::Rules::RuleCollectionPresenter.new(
      user,
      url_builder:    mock_url_builder,
      item_presenter: item_presenter,
      includes:       item_presenter.includes,
      side_loads:     allowed_side_loads
    )
  end

  let(:model_key)      { rule_class.name.downcase.to_sym }
  let(:collection_key) { rule_class.name.downcase.pluralize.to_sym }

  let(:collection) { rule_class.where(account_id: account.id) }
  let(:model)      { rule_class.all.first }

  let(:json) { presenter.present(collection) }

  before do
    Timecop.freeze

    Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

    rule_class.destroy_all

    View.any_instance.stubs(watchable?: true)

    send("create_#{rule_class.name.downcase}", title: "#{rule_class.name} 1")
    send("create_#{rule_class.name.downcase}", title: "#{rule_class.name} 2")
    send("create_#{rule_class.name.downcase}", title: "#{rule_class.name} 3")

    @collection = collection
    @presenter  = presenter
    @model_key  = collection_key
  end

  should_present_collection_keys :id,
    :title,
    :raw_title,
    :position,
    :active,
    :conditions,
    :updated_at,
    :created_at,
    :url

  describe 'when side-loading app installations' do
    let(:rule_1) { collection.first }
    let(:rule_2) { collection.second }

    let(:includes_collection) { %i[app_installation] }
    let(:allowed_side_loads)  { %i[app_installation] }

    before do
      Rule.stubs(:apps_installations).returns(
        rule_1.id => {'id' => 42, 'settings' => {'title' => 'AAA'}},
        rule_2.id => {'id' => 46, 'settings' => {'title' => 'BBB'}}
      )
    end

    it 'side-loads app installations' do
      assert_equal(
        [
          {'id' => 42, 'settings' => {'title' => 'AAA'}},
          {'id' => 46, 'settings' => {'title' => 'BBB'}},
          Zendesk::Rules::NullAppInstallation
        ],
        json[collection_key].map { |rule| rule[:app_installation] }
      )
    end

    it 'handles rules without app installations' do
      assert_equal 'null', json[collection_key].last[:app_installation].to_json
    end

    describe 'and it is presented' do
      before { presenter.present(collection) }

      before_should 'make a single request for app installations' do
        Rule.expects(:apps_installations).returns(
          rule_1.id => {'id' => 42, 'settings' => {'title' => 'AAA'}},
          rule_2.id => {'id' => 46, 'settings' => {'title' => 'BBB'}}
        ).once
      end
    end

    describe 'and it is not an allowed side-load' do
      let(:allowed_side_loads) { [] }

      let(:item_presenter_class) do
        Class.new(Api::V2::Rules::RulePresenter) do
          self.model_key = :rule
        end
      end

      it 'does not side-load app installations' do
        refute json[:rules].any? { |rule| rule.key?(:app_installation) }
      end
    end
  end

  describe 'when side-loading usages' do
    let(:includes_collection) { %i[usage_1h usage_24h usage_7d usage_30d] }

    describe 'and it is allowed' do
      let(:rule_class)           { Automation }
      let(:item_presenter_class) { Api::V2::Rules::AutomationPresenter }
      let(:allowed_side_loads)   { includes_collection }

      before do
        [30.minutes.ago, 10.hours.ago, 4.days.ago, 29.days.ago].each do |time|
          Timecop.travel(time) do
            record_usage(type: model_key, id: model.id)
          end
        end
      end

      it 'side-loads hourly usage' do
        assert_equal 1, json[collection_key].first[:usage_1h]
      end

      it 'side-loads daily usage' do
        assert_equal 2, json[collection_key].first[:usage_24h]
      end

      it 'side-loads weekly usage' do
        assert_equal 3, json[collection_key].first[:usage_7d]
      end

      it 'side-loads monthly usage' do
        assert_equal 4, json[collection_key].first[:usage_30d]
      end
    end

    describe 'and it is not allowed' do
      let(:rule_class)           { View }
      let(:item_presenter_class) { Api::V2::Rules::ViewPresenter }
      let(:allowed_side_loads)   { [] }

      it 'does not side-load hourly usage' do
        refute json[collection_key].any? { |rule| rule.key?(:usage_1h) }
      end

      it 'does not side-load daily usage' do
        refute json[collection_key].any? { |rule| rule.key?(:usage_24h) }
      end

      it 'does not side-load weekly usage' do
        refute json[collection_key].any? { |rule| rule.key?(:usage_7d) }
      end

      it 'does not side-load monthly usage' do
        refute json[collection_key].any? { |rule| rule.key?(:usage_30d) }
      end
    end
  end

  describe 'when side-loading categories' do
    let(:includes_collection) { %i[categories] }
    let(:allowed_side_loads)  { %i[categories] }

    let(:rule_class)           { Macro }
    let(:item_presenter_class) { Api::V2::Rules::MacroPresenter }

    before do
      send("create_#{rule_class.name.downcase}", title: "Beta::#{rule_class.name}")
      send("create_#{rule_class.name.downcase}", title: "Eta::#{rule_class.name}")
      send("create_#{rule_class.name.downcase}", title: "Zeta::#{rule_class.name}")
    end

    it 'side-loads categories' do
      assert_equal %w[Beta Eta Zeta], json[:categories]
    end

    describe 'and it is not allowed' do
      let(:allowed_side_loads) { [] }

      let(:rule_class)           { Trigger }
      let(:item_presenter_class) { Api::V2::Rules::TriggerPresenter }

      it 'does not side-load categories' do
        refute json.key?(:categories)
      end
    end
  end

  describe 'when side-loading all possible values' do
    let(:includes_collection) do
      %i[app_installation categories usage_1h usage_24h usage_7d usage_30d]
    end
    let(:allowed_side_loads) { includes_collection }

    let(:rule_class)           { Macro }
    let(:item_presenter_class) { Api::V2::Rules::MacroPresenter }

    before do
      Rule.stubs(:apps_installations).returns(
        model.id => {'id' => 42, 'settings' => {'title' => 'AAA'}}
      ).once
    end

    it 'includes all item side-loads' do
      assert(
        %i[app_installation usage_1h usage_24h usage_7d usage_30d].all? do |key|
          json[collection_key].first.key?(key)
        end
      )
    end

    it 'includes all collection side-loads' do
      assert json.key?(:categories)
    end
  end
end
