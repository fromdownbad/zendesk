require_relative "../../../../support/test_helper"
require_relative "../../../../support/user_views_test_helper"
require_relative "../../../../support/custom_fields_test_helper"

SingleCov.covered!

describe Api::V2::Rules::UserViewRowsPresenter do
  extend Api::Presentation::TestHelper
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  fixtures :accounts, :users, :rules, :cf_fields, :translation_locales

  before do
    @view = rules(:minimum_custom_fields_view)
    @account = @view.account
    @model = @view.account.owner
    @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
  end

  should_present_keys :created_at, "custom_fields.date1", :id, :name, :url, :user

  describe "#association_preloads" do
    before do
      @view.output.columns.pop
      @view.output.sort = nil
      @view.output.group = nil
      @view.save
    end

    it "includes photo, identities" do
      @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
      assert_equal({ photo: { thumbnails: {}}, identities: {}}, @presenter.association_preloads)
    end

    it "includes google profiles if present in columns" do
      @view.output.columns << "google"
      @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
      assert_equal({ photo: { thumbnails: {}}, identities: { google_profile: {}}}, @presenter.association_preloads)
    end

    it "includes custom fields if present in columns" do
      @view.output.columns << "custom_fields.abc"
      @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
      assert_equal({ photo: { thumbnails: {}}, identities: {}, custom_fields: { dropdown_choices: {}}, custom_field_values: {}}, @presenter.association_preloads)
    end

    it "includes custom fields if present in group" do
      @view.output.group = { id: "custom_fields.abc", order: "asc" }
      @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
      assert_equal({ photo: { thumbnails: {}}, identities: {}, custom_fields: { dropdown_choices: {}}, custom_field_values: {}}, @presenter.association_preloads)
    end

    it "includes custom fields if present in sort" do
      @view.output.sort = { id: "custom_fields.abc", order: "asc" }
      @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
      assert_equal({ photo: { thumbnails: {}}, identities: {}, custom_fields: { dropdown_choices: {}}, custom_field_values: {}}, @presenter.association_preloads)
    end

    it "includes organizations if present in columns" do
      @view.output.columns << "organization_id"
      @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
      assert_equal({ photo: { thumbnails: {}}, identities: {}, organizations: {}}, @presenter.association_preloads)
    end

    it "includes taggings if present in columns" do
      @view.output.columns << "current_tags"
      @presenter = Api::V2::Rules::UserViewRowsPresenter.new(@model, url_builder: mock_url_builder, user_view: @view)
      assert_equal({ photo: { thumbnails: {}}, identities: {}, taggings: {}}, @presenter.association_preloads)
    end
  end

  describe "role" do
    before do
      @view.output.columns << :roles
      @view.save
    end

    it "presents role when there is a roles present" do
      @model.roles = Role::END_USER.id
      @model.save
      assert_equal @model.translated_role, @presenter.present(@model)[:row]["roles"]
    end
  end

  describe "locale_id" do
    before do
      @view.output.columns << :locale_id
      @view.save
    end

    it "presents locale when there is a locale_id present" do
      @model.locale_id = ENGLISH_BY_ZENDESK.id
      @model.save

      assert_equal ENGLISH_BY_ZENDESK.localized_name(display_in: ENGLISH_BY_ZENDESK), @presenter.present(@model)[:row]["locale_id"]
    end

    it "presents default locale when no locale_id present" do
      @model.locale_id = nil
      @model.save
      assert_equal ENGLISH_BY_ZENDESK.localized_name(display_in: ENGLISH_BY_ZENDESK), @presenter.present(@model)[:row]["locale_id"]
    end

    it "presents language name" do
      japanese = translation_locales(:japanese)
      I18n.translation_locale = japanese
      @model.locale_id = japanese.id
      @model.save
      assert_equal '日本語', @presenter.present(@model)[:row]["locale_id"]
    end
  end

  describe "organization_id" do
    before do
      Account.any_instance.stubs(:has_multiple_organizations_enabled?).returns(true)
      @view.output.columns.pop
      @view.output.columns << "organization_id"
      @view.save
    end

    subject { @presenter.present(@model) }
    it "presents :columns" do
      expected_columns = [
        {"id" => "id", "title" => "User ID"},
        {"id" => "name", "title" => "Name"},
        {"id" => "created_at", "title" => "Created"},
        {"id" => "organization_id", "title" => "Organization"}
      ]
      actual_columns = JSON.parse(subject[:columns].to_json)
      assert_equal(expected_columns, actual_columns)
    end

    it "presents organization_ids for enterprise" do
      @model.account.subscription.plan_type = SubscriptionPlanType.Large
      @model.account.save
      @model.organizations = @model.account.organizations
      @model.save
      subject = @presenter.present(@model)
      assert_equal @model.account.organizations.map(&:id), subject[:row]["organization_ids"]
    end

    it "does not present organization_ids for non enterprise" do
      Account.any_instance.stubs(:has_multiple_organizations_enabled?).returns(false)
      @model.account.save
      refute subject[:row]["organization_ids"]
    end
  end

  describe "when no custom fields" do
    subject { @presenter.present(@model) }
    before { @view.output.columns.pop } # The last column in rules.yml is a custom user field key

    it "presents :columns" do
      expected_columns = [{"id" => "id", "title" => "User ID"}, {"id" => "name", "title" => "Name"}, {"id" => "created_at", "title" => "Created"}]
      actual_columns = JSON.parse(subject[:columns].to_json)
      assert_equal(expected_columns, actual_columns)
    end
  end

  describe "when has custom fields" do
    subject { @presenter.present(@model) }

    it "presents :columns" do
      expected_columns = [{"id" => "id", "title" => "User ID"}, {"id" => "name", "title" => "Name"}, {"id" => "created_at", "title" => "Created"}, {"id" => "custom_fields.date1", "title" => "Date", "type" => "date", "url" => "api_v2_user_field_url"}]
      actual_columns = JSON.parse(subject[:columns].to_json)
      assert_equal(expected_columns, actual_columns)
    end
  end

  describe "when side-loading user_view" do
    subject { @presenter.present(@model) }
    before { @presenter.stubs(:side_load?).with(:user_view).returns(true) }

    it "presents :user_view" do
      assert subject[:user_view]
      assert_equal @view.id, subject[:user_view][:id]
    end
  end

  describe "has_user_views_sampling_enabled" do
    before do
      @view.account.expects(:has_user_views_sampling_enabled?).returns(true)
      @view.account.settings.user_count = UserView::SAMPLING_THRESHOLD * 10
      @models = [@model]
      @total_entries = 100
      @models.stubs(:total_entries).returns(@total_entries)
    end

    it "presents the sample key" do
      assert @presenter.present(@models)[:sample]
    end

    it "calculates estimated unsampled count" do
      assert_equal @total_entries * 10, @presenter.present(@models)[:estimated_unsampled_count]
    end
  end

  describe "not has_user_views_sampling_enabled" do
    before do
      @view.account.expects(:has_user_views_sampling_enabled?).returns(false)
    end

    it "does not present the sample key" do
      refute @presenter.present(@model).key?(:sample)
    end

    it "does not calculate estimated unsampled count" do
      refute @presenter.present(@model).key?(:estimated_unsampled_count)
    end
  end

  describe "user name, email and photo" do
    subject { @presenter.present(@model) }

    it "presents users" do
      assert_equal @model.name, subject[:row][:user][:name]
      assert_equal @model.email, subject[:row][:user][:email]
      assert_nil subject[:row][:user][:photo]
    end

    describe "with photo" do
      before do
        @model.photo = FactoryBot.create(:photo, filename: "large_1.png")
        @model.save!
      end

      it "presents photo" do
        Api::V2::AttachmentPresenter.any_instance.expects(:model_json).with(@model.photo).returns("stub_response")
        assert_equal "stub_response", subject[:row][:user][:photo]
      end
    end
  end

  describe "with google identity" do
    before do
      @model.identities.last.build_google_profile.save!
      assert @model.identities.last.google?
    end

    subject { @presenter.present(@model) }

    it "uses google identity email as email" do
      assert_equal @model.email, subject[:row][:user][:email]
    end
  end

  describe "with system field" do
    before do
      create_user_custom_field!("system::sf1", "txt.something", "Text", is_system: true)
      @view.output.columns.pop
      @view.output.columns << :"custom_fields.system::sf1"
      @view.save
    end

    subject { @presenter.present(@model) }

    it "presents :columns" do
      expected_columns = [
        {"id" => "id", "title" => "User ID"},
        {"id" => "name", "title" => "Name"},
        {"id" => "created_at", "title" => "Created"},
        {"id" => "custom_fields.system::sf1", "title" => I18n.t('txt.something'), "type" => "text", "url" => "api_v2_user_field_url" }
      ]
      actual_columns = JSON.parse(subject[:columns].to_json)
      assert_equal(expected_columns, actual_columns)
    end
  end

  describe "dynamic content" do
    subject { presenter.present([user]) }
    let(:user) { @model }
    let(:presenter) { Api::V2::Rules::UserViewRowsPresenter.new(user, url_builder: mock_url_builder, user_view: view) }

    describe "with Integer fields" do
      let(:view) do
        create_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "less_than", "value" => "20"}],
                                 "execution" => { "columns" => ["custom_fields.age"] })
      end

      before do
        create_user_custom_field!("age", "age", "Integer")
        user.custom_field_values.update_from_hash("age" => "10")
        user.save!
      end

      it "presents :columns" do
        expected_columns = [{"id" => "custom_fields.age", "title" => "age", "type" => "integer", "url" => "api_v2_user_field_url"}]
        actual_columns = JSON.parse(subject[:columns].to_json)
        assert_equal(expected_columns, actual_columns)
      end

      it "presents :rows" do
        assert_equal 1, subject[:rows].length
        row = JSON.parse(subject[:rows].first.to_json)

        assert_equal user.id, row['id']
        assert_equal 10, row['custom_fields.age']
        assert_equal 'api_v2_user_url', row['url']
        assert_equal({ "name" => "Admin Man", "email" => "minimum_admin@aghassipour.com", "photo" => nil }, row['user'])
      end
    end

    describe "with Decimal fields" do
      let(:view) do
        create_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "less_than", "value" => "20"}],
                                 "execution" => { "columns" => ["custom_fields.age"] })
      end

      before do
        create_user_custom_field!("age", "age", "Decimal")
        user.custom_field_values.update_from_hash("age" => "10.5")
        user.save!
      end

      it "presents :columns" do
        expected_columns = [{"id" => "custom_fields.age", "title" => "age", "type" => "decimal", "url" => "api_v2_user_field_url"}]
        actual_columns = JSON.parse(subject[:columns].to_json)
        assert_equal(expected_columns, actual_columns)
      end

      it "presents :rows" do
        assert_equal 1, subject[:rows].length
        row = JSON.parse(subject[:rows].first.to_json)

        assert_equal user.id, row['id']
        assert_equal 10.5, row['custom_fields.age']
        assert_equal 'api_v2_user_url', row['url']
        assert_equal({ "name" => "Admin Man", "email" => "minimum_admin@aghassipour.com", "photo" => nil }, row['user'])
      end
    end

    describe "with Checkbox fields" do
      let(:view) do
        create_view setup_params("all" => [{"field" => "custom_fields.paid", "operator" => "is", "value" => "true"}],
                                 "execution" => { "columns" => ["custom_fields.paid"] })
      end

      before do
        create_user_custom_field!("paid", "paid", "Checkbox")
        user.custom_field_values.update_from_hash("paid" => "true")
        user.save!
      end

      it "presents :columns" do
        expected_columns = [{"id" => "custom_fields.paid", "title" => "paid", "type" => "checkbox", "url" => "api_v2_user_field_url"}]
        actual_columns = JSON.parse(subject[:columns].to_json)
        assert_equal(expected_columns, actual_columns)
      end

      it "presents :rows" do
        assert_equal 1, subject[:rows].length
        row = JSON.parse(subject[:rows].first.to_json)

        assert_equal user.id, row['id']
        assert(row['custom_fields.paid'])
        assert_equal 'api_v2_user_url', row['url']
        assert_equal({ "name" => "Admin Man", "email" => "minimum_admin@aghassipour.com", "photo" => nil }, row['user'])
      end
    end

    ["Textarea", "Text"].each do |type|
      describe "with #{type} fields" do
        let(:view) do
          create_view setup_params("all" => [{"field" => "custom_fields.url", "operator" => "is", "value" => "http://linkedin.com"}],
                                   "execution" => { "columns" => ["custom_fields.url"] })
        end

        before do
          create_user_custom_field!("url", "url", type)
          user.custom_field_values.update_from_hash("url" => "http://linkedin.com")
          user.save!
        end

        it "presents :columns" do
          expected_columns = [{"id" => "custom_fields.url", "title" => "url", "type" => type.downcase, "url" => "api_v2_user_field_url"}]
          actual_columns = JSON.parse(subject[:columns].to_json)
          assert_equal(expected_columns, actual_columns)
        end

        it "presents :rows" do
          assert_equal 1, subject[:rows].length
          row = JSON.parse(subject[:rows].first.to_json)

          assert_equal user.id, row['id']
          assert_equal 'http://linkedin.com', row['custom_fields.url']
          assert_equal 'api_v2_user_url', row['url']
          assert_equal({ "name" => "Admin Man", "email" => "minimum_admin@aghassipour.com", "photo" => nil }, row['user'])
        end
      end
    end

    describe "with regexp field" do
      let(:view) do
        create_view setup_params("all" => [{"field" => "custom_fields.matcher", "operator" => "is", "value" => "123"}],
                                 "execution" => { "columns" => ["custom_fields.matcher"] })
      end

      before do
        create_user_custom_field!("matcher", "matcher", "Regexp", regexp_for_validation: "[0-9]+")
        user.custom_field_values.update_from_hash("matcher" => "123")
        user.save!
      end

      it "presents :columns" do
        expected_columns = [{"id" => "custom_fields.matcher", "title" => "matcher", "type" => "regexp", "url" => "api_v2_user_field_url"}]
        actual_columns = JSON.parse(subject[:columns].to_json)
        assert_equal(expected_columns, actual_columns)
      end

      it "presents :rows" do
        assert_equal 1, subject[:rows].length
        row = JSON.parse(subject[:rows].first.to_json)

        assert_equal user.id, row['id']
        assert_equal '123', row['custom_fields.matcher']
        assert_equal 'api_v2_user_url', row['url']
        assert_equal({ "name" => "Admin Man", "email" => "minimum_admin@aghassipour.com", "photo" => nil }, row['user'])
      end
    end

    describe "with dropdown fields" do
      let(:view) do
        create_view setup_params("all" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold"}],
                                 "execution" => { "columns" => ["custom_fields.plan_type"] })
      end

      before do
        create_dropdown_field("plan_type", "Plan",
          [{ name: "Gold",   value: "gold" },
           { name: "Silver", value: "silver" },
           { name: "Bronze", value: "bronze" }])
        user.custom_field_values.update_from_hash("plan_type" => "gold")
        user.save!
      end

      it "presents :columns" do
        expected_columns = [{"id" => "custom_fields.plan_type", "title" => "Plan", "type" => "dropdown", "url" => "api_v2_user_field_url"}]
        actual_columns = JSON.parse(subject[:columns].to_json)
        assert_equal(expected_columns, actual_columns)
      end

      it "presents :rows" do
        assert_equal 1, subject[:rows].length
        row = JSON.parse(subject[:rows].first.to_json)

        assert_equal user.id, row['id']
        assert_equal 'Gold', row['custom_fields.plan_type']
        assert_equal 'api_v2_user_url', row['url']
        assert_equal({ "name" => "Admin Man", "email" => "minimum_admin@aghassipour.com", "photo" => nil }, row['user'])
      end
    end

    describe "with date fields" do
      let(:view) do
        create_view setup_params("all" => [{"field" => "custom_fields.date", "operator" => "is", "value" => "1/2/2001"}],
                                 "execution" => { "columns" => ["custom_fields.date"] })
      end

      before do
        create_user_custom_field!("date", "Date", "Date")
        user.custom_field_values.update_from_hash("date" => "1/2/2001")
        user.save!
      end

      it "presents :columns" do
        expected_columns = [{"id" => "custom_fields.date", "title" => "Date", "type" => "date", "url" => "api_v2_user_field_url"}]
        actual_columns = JSON.parse(subject[:columns].to_json)
        assert_equal(expected_columns, actual_columns)
      end

      it "presents :rows" do
        assert_equal 1, subject[:rows].length
        row = JSON.parse(subject[:rows].first.to_json)

        assert_equal user.id, row['id']
        assert_equal '2001/02/01 00:00:00 +0000', row['custom_fields.date']
        assert_equal 'api_v2_user_url', row['url']
        assert_equal({ "name" => "Admin Man", "email" => "minimum_admin@aghassipour.com", "photo" => nil }, row['user'])
      end
    end
  end
end
