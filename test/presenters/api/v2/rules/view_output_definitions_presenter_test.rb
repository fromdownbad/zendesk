require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::ViewOutputDefinitionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users

  let(:account)     { accounts(:minimum) }
  let(:user)        { account.owner }
  let(:view_output) { {title_for_field: 'ID', output_key: 'nice_id'} }

  let(:output) do
    Zendesk::Rules::View::Output.for_definition(
      view_output,
      account: account,
      user:    user
    )
  end

  let(:presenter) do
    Api::V2::Rules::ViewOutputDefinitionsPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  describe '#model_json' do
    it 'returns a formatted hash' do
      assert_equal(
        {title: 'ID', value: 'nice_id'},
        presenter.model_json(output)
      )
    end
  end
end
