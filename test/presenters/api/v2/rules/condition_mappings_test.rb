require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Rules::ConditionMappings do
  fixtures :accounts, :ticket_fields, :custom_field_options

  def self.should_map(from, to = from)
    # tests wrap_value
    to = to.dup.tap { |defn| defn.merge!(value: Array.wrap(to[:value])) }

    describe from.inspect.to_s do
      it "maps to #{to.inspect}" do
        assert_equal to, Api::V2::Rules::ConditionMappings.map_definition_item!(stub, from)
      end
    end
  end

  %w[new oPen PENDING solveD hold].each do |field|
    should_map({ field: field, operator: "greater_than", value: 4.days.ago.as_json },
      field: field.upcase, operator: "less_than", value: 96)

    should_map({ field: field, operator: "less_than", value: 4.days.ago.as_json },
      field: field.upcase, operator: "greater_than", value: 96)

    should_map({ field: field, operator: "less_than", value: "INVALIDVALUE" },
      field: field.upcase, operator: "less_than", value: "INVALIDVALUE")

    should_map({ field: field, operator: "less_than", value: nil },
      field: field.upcase, operator: "less_than", value: nil)

    should_map({ field: field, operator: "less_than", value: "24" },
      field: field.upcase, operator: "less_than", value: "24")
  end

  should_map({ field: "updated_at", operator: "less_than", value: 4.days.ago.as_json },
    field: "updated_at", operator: "greater_than", value: 96)

  should_map({ field: "comment_mode_is_public", value: true },
    field: "comment_mode_is_public", operator: "is", value: "true")

  describe "with a custom field" do
    let(:field) { ticket_fields(:field_checkbox_custom) }
    let(:account) { accounts(:minimum) }

    it "maps field name" do
      assert_equal({ field: "ticket_fields_#{field.id}", operator: "is", value: [true] },
        Api::V2::Rules::ConditionMappings.map_definition_item!(account, field: "custom_fields_#{field.id}", operator: "is", value: true))
    end
  end

  describe "with a custom field tagger" do
    let(:field) { ticket_fields(:field_tagger_custom) }
    let(:option) { custom_field_options(:field_tagger_custom_option_1) }
    let(:account) { accounts(:minimum) }

    describe_with_and_without_arturo_setting_enabled :special_chars_in_custom_field_options do
      it "maps field and values" do
        assert_equal({ field: "ticket_fields_#{field.id}", operator: "is", value: [option.id.to_s] },
          Api::V2::Rules::ConditionMappings.map_definition_item!(account, field: "custom_fields_#{field.id}", operator: "is", value: option.value))
      end

      it "maps values" do
        assert_equal({ field: "ticket_fields_#{field.id}", operator: "is", value: [option.id.to_s] },
          Api::V2::Rules::ConditionMappings.map_definition_item!(account, field: "ticket_fields_#{field.id}", operator: "is", value: option.value))
      end

      it "maps nothing" do
        assert_equal({ field: "ticket_fields_#{field.id}", operator: "is", value: [option.id] },
          Api::V2::Rules::ConditionMappings.map_definition_item!(account, field: "ticket_fields_#{field.id}", operator: "is", value: option.id))
      end

      describe "with an integer in the name" do
        before do
          option.update_attributes!(value: "booring_1")
        end

        it "maps value" do
          assert_equal({ field: "ticket_fields_#{field.id}", operator: "is", value: [option.id.to_s] },
            Api::V2::Rules::ConditionMappings.map_definition_item!(account, field: "ticket_fields_#{field.id}", operator: "is", value: option.value))
        end
      end

      describe "with only integers in the name" do
        before do
          option.update_attributes!(value: "715013")
        end

        it "maps value" do
          assert_equal({ field: "ticket_fields_#{field.id}", operator: "is", value: [option.id.to_s] },
            Api::V2::Rules::ConditionMappings.map_definition_item!(account, field: "ticket_fields_#{field.id}", operator: "is", value: option.value))
        end
      end
    end
  end

  should_map(field: "updated_at", operator: "less_than", value: "24")
  should_map(field: "updated_at", operator: "less_than", value: "INVALID")

  should_map({ field: "id", operator: "less_than", value: "INVALID" },
    field: "nice_id", operator: "less_than", value: "INVALID")

  should_map({ field: "id", value: %w[hello] }, field: "nice_id", operator: "is", value: %w[hello])

  should_map(field: "exact_created_at", operator: "less_than", value: "2015-03-03 00:00")
  should_map(field: "requester.custom_fields.created_at", operator: "less_than_equal", value: "2015-03-03 00:00")

  describe 'with a field of `follower`' do
    let(:account) { accounts(:minimum) }

    describe_with_arturo_enabled :email_ccs do
      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it 'maps `follower` -> `cc`' do
          assert_equal(
            {field: 'cc', operator: 'is', value: ['x']},
            Api::V2::Rules::ConditionMappings.map_definition_item!(
              account,
              field: 'follower', operator: 'is', value: 'x'
            )
          )
        end
      end
    end

    describe_with_arturo_disabled :email_ccs do
      it 'maps `follower` -> `follower`' do
        assert_equal(
          {field: 'follower', operator: 'is', value: ['x']},
          Api::V2::Rules::ConditionMappings.map_definition_item!(
            account,
            field: 'follower', operator: 'is', value: 'x'
          )
        )
      end
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it 'maps `follower` -> `follower`' do
        assert_equal(
          {field: 'follower', operator: 'is', value: ['x']},
          Api::V2::Rules::ConditionMappings.map_definition_item!(
            account,
            field: 'follower', operator: 'is', value: 'x'
          )
        )
      end
    end
  end

  describe 'with tags fields' do
    field_names = ['tags', 'current_tags', 'set_tags', 'remove_tags']

    let(:account) { accounts(:minimum) }
    let(:tags) { 'tag_1, tag*2, tag!@#' }

    describe_with_arturo_enabled :sanitize_rule_tags do
      let(:sanitized_tags) { 'tag_1 tag2 tag' }

      field_names.each do |field_name|
        it "sanitizes values for #{field_name}" do
          assert_equal(
            { field: field_name, operator: 'is', value: [sanitized_tags] },
            Api::V2::Rules::ConditionMappings.map_definition_item!(
              account,
              field: field_name, value: tags
            )
          )
        end
      end
    end

    describe_with_arturo_disabled :sanitize_rule_tags do
      field_names.each do |field_name|
        it "does not sanitize values for #{field_name}" do
          assert_equal(
            { field: field_name, operator: 'is', value: [tags] },
            Api::V2::Rules::ConditionMappings.map_definition_item!(
              account,
              field: field_name, value: tags
            )
          )
        end
      end
    end
  end
end
