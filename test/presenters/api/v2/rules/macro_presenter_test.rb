require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::MacroPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  fixtures :all

  let(:macro) { create_macro }

  let(:account) { accounts(:minimum) }
  let(:user)    { macro.account.owner }

  let(:highlights) { nil }

  let(:includes_collection) { [] }

  let(:presenter) do
    Api::V2::Rules::MacroPresenter.new(
      user,
      url_builder: mock_url_builder,
      highlights:  highlights,
      includes:    includes_collection
    )
  end

  before do
    @model     = macro
    @presenter = presenter
  end

  should_present_keys :id,
    :title,
    :raw_title,
    :active,
    :position,
    :restriction,
    :actions,
    :updated_at,
    :created_at,
    :url,
    :description

  should_present_method :id
  should_present_method :title
  should_present_method :is_active?, as: :active
  should_present_method :position
  should_present_method :actions, with: Api::V2::Rules::ActionsPresenter
  should_present_method :updated_at
  should_present_method :created_at
  should_present_method :description

  describe '#model_json' do
    let(:json) { presenter.model_json(macro) }

    describe 'when the macro has associated attachments' do
      let(:attachments) do
        Array.new(2) do
          RuleAttachment.create! do |attachment|
            attachment.account_id   = account.id
            attachment.user_id      = user.id
            attachment.size         = 1
            attachment.content_type = 'application/unknown'
            attachment.filename     = 'hello_world'
          end
        end
      end

      before { attachments.each { |a| a.associate(macro) } }

      it 'presents the attachment IDs' do
        assert_equal attachments.map(&:id), json[:attachments]
      end
    end

    describe 'when the macro does not have associated attachments' do
      it 'does not present the `attachments` key' do
        refute json.key?(:attachments)
      end
    end

    describe 'when highlights are present' do
      let(:highlights) { {macro.id => '<em>highlight</em>'} }

      it 'presents the highlights' do
        assert_equal '<em>highlight</em>', json[:highlights]
      end
    end

    describe 'when the owner is an account' do
      before { macro.owner = account }

      it 'presents no restrictions' do
        assert_nil json[:restriction]
      end
    end

    describe 'when the owner is a group' do
      let(:group_1) { groups(:minimum_group) }
      let(:group_2) { account.groups.create!(name: '2', is_active: true) }

      before do
        GroupMacro.destroy_all

        macro.owner = group_1

        [group_1, group_2].each do |group|
          GroupMacro.create! do |group_macro|
            group_macro.account_id = account.id
            group_macro.group_id   = group.id
            group_macro.macro_id   = macro.id
          end
        end
      end

      it 'presents the restriction' do
        assert_equal(
          {type: 'Group', id: group_1.id, ids: [group_1.id, group_2.id]},
          json[:restriction]
        )
      end
    end

    describe 'when the owner is a user' do
      before { macro.owner = user }

      it 'presents the restriction' do
        assert_equal({type: 'User', id: user.id}, json[:restriction])
      end
    end

    describe 'when the title contains dynamic content' do
      let(:macro_with_dc) { create_macro(title: '{{dc.macro_title}}') }

      before do
        Arturo.enable_feature!(:dc_in_macro_titles)

        account.cms_texts.create!(
          account: account,
          name:    'macro_title',
          fallback_attributes: {
            is_fallback:          'true',
            nested:                true,
            translation_locale_id: 1,
            value:                 'Rendered Title'
          }
        )
      end

      it 'renders the dynamic content in the title' do
        assert_equal(
          'Rendered Title',
          presenter.model_json(macro_with_dc)[:title]
        )
      end

      describe 'and highlights are present' do
        let(:highlights) { {macro_with_dc.id => '<em>dc</em>'} }

        it 'does not present the highlights' do
          assert_nil presenter.model_json(macro_with_dc)[:highlights]
        end
      end

      describe 'and the `dc_in_macro_titles` feature is not enabled' do
        before { Arturo.disable_feature!(:dc_in_macro_titles) }

        it 'does not render the dynamic content in the title' do
          assert_equal(
            '{{dc.macro_title}}',
            presenter.model_json(macro_with_dc)[:title]
          )
        end

        describe 'and highlights are present' do
          let(:highlights) { {macro_with_dc.id => '<em>dc</em>'} }

          it 'presents the highlights' do
            assert_equal(
              '<em>dc</em>',
              presenter.model_json(macro_with_dc)[:highlights]
            )
          end
        end
      end
    end

    describe 'when a macro has a `cc` action' do
      let(:macro_with_cc_action) do
        cc_action = DefinitionItem.new('cc', 'is', 1)

        create_macro(
          owner:      account,
          definition: build_macro_definition.tap do |definition|
            definition.actions.clear
            definition.actions.push(cc_action)
          end
        )
      end

      let(:presented_field) do
        presenter.model_json(macro_with_cc_action)[:actions].first[:field]
      end

      describe_with_arturo_enabled :email_ccs do
        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          it 'presents the action as `follower`' do
            assert_equal 'follower', presented_field
          end
        end

        describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
          it 'presents the action as `cc`' do
            assert_equal 'cc', presented_field
          end
        end
      end

      describe_with_arturo_disabled :email_ccs do
        it 'presents the action as `cc`' do
          assert_equal 'cc', presented_field
        end
      end
    end

    describe 'when side-loading permissions' do
      let(:includes_collection) { %i[permissions] }

      let(:user) { users(:minimum_agent) }

      describe 'and the user cannot edit the macro' do
        before { user.stubs(:can?).with(:edit, macro).returns(false) }

        it 'sets `can_edit` to false' do
          refute json[:permissions][:can_edit]
        end
      end

      describe 'and the user can edit the macro' do
        before { user.stubs(:can?).with(:edit, macro).returns(true) }

        it 'sets `can_edit` to true' do
          assert json[:permissions][:can_edit]
        end
      end
    end

    describe 'when side-loading usage counts' do
      before do
        Timecop.freeze

        Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

        [30.minutes.ago, 10.hours.ago, 4.days.ago, 29.days.ago].each do |time|
          Timecop.travel(time) do
            record_usage(type: :macro, id: macro.id)
          end
        end
      end

      describe 'and side-loading hourly usage' do
        let(:includes_collection) { %i[usage_1h] }

        describe 'and the hourly usage is stored' do
          before { macro.usage.hourly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_1h]
          end
        end

        describe 'and the hourly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 1, json[:usage_1h]
          end
        end
      end

      describe 'and side-loading daily usage' do
        let(:includes_collection) { %i[usage_24h] }

        describe 'and the daily usage is stored' do
          before { macro.usage.daily = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_24h]
          end
        end

        describe 'and the daily usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 2, json[:usage_24h]
          end
        end
      end

      describe 'and side-loading weekly usage' do
        let(:includes_collection) { %i[usage_7d] }

        describe 'and the weekly usage is stored' do
          before { macro.usage.weekly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_7d]
          end
        end

        describe 'and the weekly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 3, json[:usage_7d]
          end
        end
      end

      describe 'and side-loading monthly usage' do
        let(:includes_collection) { %i[usage_30d] }

        describe 'and the monthly usage is stored' do
          before { macro.usage.monthly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_30d]
          end
        end

        describe 'and the monthly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 4, json[:usage_30d]
          end
        end
      end
    end

    describe 'when side-loading the app installation' do
      let(:includes_collection) { %i[app_installation] }

      before do
        macro.stubs(:app_installation).returns(
          'id'       => 42,
          'settings' => {'title' => 'Test app'}
        )
      end

      it 'sets the app installation' do
        assert_equal 42, presenter.model_json(macro)[:app_installation]['id']
      end
    end
  end

  describe 'when presenting a collection' do
    let(:includes_collection) { %i[categories] }

    let(:json)       { presenter.present(collection) }
    let(:collection) { Macro.where(account_id: account.id) }

    before do
      Macro.destroy_all

      create_macro(title: 'Beta::Epsilon::Three')
      create_macro(title: 'Theta::Iota::Four')
      create_macro(title: 'Zeta::Five')
    end

    it 'does not perform N+1 queries' do
      assert_no_n_plus_one { presenter.present(collection) }
    end

    it 'presents a collection' do
      assert_equal(
        %w[Beta::Epsilon::Three Theta::Iota::Four Zeta::Five],
        json[:macros].map { |macro| macro[:title] }
      )
    end

    describe 'and side-loading categories' do
      let(:includes_collection) { %i[categories] }

      it 'includes the categories for macros in that collection' do
        assert_equal %w[Beta Theta Zeta], json[:categories]
      end
    end
  end
end
