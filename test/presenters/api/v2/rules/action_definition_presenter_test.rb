require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::ActionDefinitionPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:metadata_presenter) do
    Api::V2::Rules::MetadataPresenter.new(user, url_builder: mock_url_builder)
  end

  let(:definition_metadata) { stub('definition metadata', valid_key: 'Test') }

  let(:presenter) do
    Api::V2::Rules::ActionDefinitionPresenter.new(
      user,
      url_builder:        mock_url_builder,
      metadata_presenter: metadata_presenter
    )
  end

  let(:source) { 'ticket_form_id' }
  let(:list)   { [] }
  let(:type)   { 'list' }

  let(:action_definition) do
    Zendesk::Rules::Action.new(
      OpenStruct.new(
        source: source,
        title:  'title',
        type:   type,
        group:  'ticket',
        list:   list
      ),
      account: account
    )
  end

  before do
    Zendesk::Rules::DefinitionMetadata.
      stubs(:new).
      with(account, user).
      returns(definition_metadata)

    @model     = action_definition
    @presenter = presenter
  end

  should_present_keys :title,
    :type,
    :subject,
    :group,
    :nullable,
    :repeatable

  should_present_method :nullable?,   as: :nullable
  should_present_method :repeatable?, as: :repeatable

  describe 'when the action contains metadata' do
    before { action_definition.stubs(metadata: %i[valid_key]) }

    should_present_keys :title,
      :type,
      :subject,
      :group,
      :nullable,
      :repeatable,
      :metadata
  end

  describe 'when the action contains formatted values' do
    describe 'and it is a `target` action' do
      let(:type) { 'target' }

      let(:list) do
        [
          {value: 1, title: 'Plain', v2: false, method: 'get'},
          {value: 2, title: 'Query', v2: true,  method: 'get'}
        ]
      end

      should_present_keys :title,
        :type,
        :subject,
        :group,
        :nullable,
        :repeatable,
        :values

      it 'presents the formatted values' do
        assert_equal(
          [
            {value: '1', title: 'Plain', enabled: true, format: :plain},
            {value: '2', title: 'Query', enabled: true, format: :query}
          ],
          presenter.model_json(action_definition)[:values]
        )
      end
    end

    describe 'and it is a `custom_date` action' do
      let(:type) { 'custom_date' }

      should_present_keys :title,
        :type,
        :subject,
        :group,
        :nullable,
        :repeatable,
        :values

      it 'presents the formatted values' do
        assert_equal(
          [
            {
              value: 'specific_date',
              title: 'Set to a specific date',
              enabled: true,
              format: 'date'
            },
            {
              value: 'days_from_now',
              title: 'Set to a number of days from now',
              enabled: true,
              format: 'text'
            }
          ],
          presenter.model_json(action_definition)[:values]
        )
      end
    end
  end

  describe 'when the action contains non-formatted values' do
    let(:list) { [{value: 1, title: 'Steph'}, {value: 2, title: 'Curry'}] }

    should_present_keys :title,
      :type,
      :subject,
      :group,
      :nullable,
      :repeatable,
      :values

    it 'presents the values' do
      assert_equal(
        [
          {value: '1', title: 'Steph', enabled: true},
          {value: '2', title: 'Curry', enabled: true}
        ],
        presenter.model_json(action_definition)[:values]
      )
    end
  end

  describe 'when the action does not contain values' do
    let(:list) { [] }

    should_present_keys :title,
      :type,
      :subject,
      :group,
      :nullable,
      :repeatable
  end
end
