require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Rules::ViewCountPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :rules, :tickets

  before do
    stub_occam_count(7)
    @view      = rules(:view_assigned_working_tickets)
    @model     = ::CachedRuleTicketCount.lookup(@view, @view.account.owner, refresh: :delayed)
    @presenter = Api::V2::Rules::ViewCountPresenter.new(@view.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :view_id, :value, :pretty, :fresh, :url

  should_present_method :rule_id, as: :view_id
  should_present_method :pretty_print, as: :pretty
  should_present false, as: :fresh

  it 'does not present sha and update_in' do
    json = @presenter.model_json(@model)
    refute json.key?(:sha)
    refute json.key?(:update_in)
  end

  describe 'OccamTicketCount behaves like CachedRuleTicketCount' do
    before do
      @model = Zendesk::Rules::OccamTicketCount.new(
        id: 123, count: 7, fresh: true, updated_at: Time.now.to_s,
        refresh: 'poll', channel: 'abc', poll_wait: 30
      )
    end

    should_present_keys :view_id, :value, :pretty, :fresh, :url, :refresh,
      :channel, :poll_wait

    should_present_method :rule_id, as: :view_id
    should_present_method :pretty_print, as: :pretty
    should_present 123, as: :view_id
    should_present 7, as: :value
    should_present '7', as: :pretty
    should_present true, as: :fresh
    should_present_method :refresh
    should_present_method :channel
    should_present_method :poll_wait
  end

  describe "without a rule_id" do
    let(:preview) do
      view = users(:minimum_agent).views.preview(title: "test", all: [
                                                   { field: "status_id", operator: "is", value: StatusType.OPEN }
                                                 ])

      CachedRulePreviewTicketCount.lookup(view, view.owner, refresh: :delayed)
    end

    subject do
      @presenter.model_json(preview)
    end

    it "does not present a url" do
      assert_nil subject[:url]
    end
  end
end
