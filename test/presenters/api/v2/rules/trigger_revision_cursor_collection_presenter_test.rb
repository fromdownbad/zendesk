require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::TriggerRevisionCursorCollectionPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account)     { accounts(:minimum) }
  let(:user)        { users(:minimum_agent) }
  let(:url_builder) { mock_url_builder }

  let(:presenter) do
    Api::V2::Rules::TriggerRevisionPresenter.new(user, url_builder: url_builder)
  end

  let(:collection_presenter) do
    Api::V2::Rules::TriggerRevisionCursorCollectionPresenter.new(
      user,
      url_builder:    url_builder,
      item_presenter: presenter
    )
  end

  let(:rule_diff_presenter) do
    Api::V2::Rules::RuleDiffPresenter.new(
      user,
      url_builder: url_builder
    )
  end

  describe '#as_json' do
    let(:json) { collection_presenter.as_json(collection) }

    before { url_builder.stubs(:params).returns({}) }

    describe 'pagination cursors' do
      let(:collection) do
        TriggerRevision.where(nice_id: 1).paginate_with_cursor
      end

      before { TriggerRevision.destroy_all }

      describe 'when the collection has the initial revision' do
        before { create_trigger }

        it 'sets the `before_url` attribute to `nil`' do
          assert_nil json[:before_url]
        end

        it 'sets the `before_cursor` attribute to `nil`' do
          assert_nil json[:before_cursor]
        end
      end

      describe 'when the collection does not have the initial revision' do
        it 'sets the `before_url` attribute' do
          assert json[:before_url].present?
        end

        it 'sets the `before_cursor` attribute' do
          assert json[:before_cursor].present?
        end
      end
    end

    describe 'presenting revision diffs' do
      let(:collection) do
        TriggerRevision.
          where(rule_id: trigger.id, account_id: trigger.account_id).
          paginate_with_cursor(id_column: 'nice_id')
      end

      let(:revision_schema) do
        File.read(
          Rails.root.join('lib/zendesk/rules/json_schema/trigger_revisions.json')
        )
      end

      let(:trigger) { create_trigger }

      before do
        create_revision(trigger)
        create_revision(trigger, after: 2.minutes)
      end

      it 'presents the payload in a format that conforms to the schema' do
        assert_empty JSON::Validator.fully_validate(revision_schema, json)
      end

      it 'presents a `diff` for each revision' do
        assert_equal(
          3,
          json[:trigger_revisions].count { |revision| revision.key?(:diff) }
        )
      end

      it 'presents `diff`s between each revision and its ancestor' do
        assert(
          json[:trigger_revisions].all? do |trigger_revision|
            target_id = trigger_revision[:diff][:target_id]
            source_id = trigger_revision[:diff][:source_id] || 0

            target_id - 1 == source_id
          end
        )
      end

      describe 'presenting the initial revision' do
        let(:collection) do
          TriggerRevision.
            where(rule_id: trigger.id, nice_id: 1).
            paginate_with_cursor(id_column: 'nice_id')
        end

        before_should 'compares the revision with a NullRevision' do
          payload = Zendesk::Rules::Trigger::RevisionDiff.new(
            source: NullRevision,
            target: trigger.revisions.first
          )

          Zendesk::Rules::Trigger::RevisionDiff.
            expects(:new).
            with(source: NullRevision, target: collection.first).
            returns(payload)
        end

        before { json }
      end

      describe 'when a revision is missing in the collection' do
        let(:collection) do
          TriggerRevision.
            where(rule_id: trigger.id, nice_id: 2).
            paginate_with_cursor(id_column: 'nice_id')
        end

        let(:expected_payload) do
          rule_diff_presenter.model_json(
            Zendesk::Rules::Trigger::RevisionDiff.new(
              source: trigger.revisions.first,
              target: trigger.revisions.second
            )
          )
        end

        let(:revision_diff) { json[:trigger_revisions].first[:diff] }

        it 'compares the target revision with its ancestor' do
          assert_equal expected_payload, revision_diff
        end
      end
    end
  end
end
