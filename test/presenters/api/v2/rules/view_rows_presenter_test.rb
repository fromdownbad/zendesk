require_relative '../../../../support/test_helper'
require 'zendesk_rules/custom_rule_field'

SingleCov.covered! uncovered: 4

describe Api::V2::Rules::ViewRowsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :rules, :organizations, :tickets, :custom_field_options, :ticket_fields,
    :translation_locales, :events

  before do
    Zendesk::Rules::RuleExecuter.
      any_instance.
      stubs(:find_tickets_with_occam_client).
      returns([tickets(:minimum_1)])

    @view = rules(:view_assigned_working_tickets)
    @user = @view.account.owner
    @collection = @view.find_tickets(@user)
    @model = @collection.first

    # For association checking
    @model.will_be_saved_by(@user)
    @model.update_attribute(:organization, organizations(:minimum_organization1))

    @presenter = Api::V2::Rules::ViewRowsPresenter.new(@user, url_builder: mock_url_builder, view: @view)
  end

  describe "#last_comments" do
    before { @model.stubs(:comments).returns(subject) }

    describe ":agent_as_end_user Arturo is disabled" do
      before do
        Arturo.disable_feature!(:agent_as_end_user)
      end

      describe "> 2 comments" do
        subject { [1, 2, 3, 4] }

        it "returns last two comments" do
          assert_equal [4, 3], @presenter.last_comments(@model)
        end
      end

      describe "2 comments" do
        subject { [1, 2] }

        it "returns reverse" do
          assert_equal [2, 1], @presenter.last_comments(@model)
        end
      end

      describe "< 2 comments" do
        subject { [1] }

        it "returns all" do
          assert_equal [1], @presenter.last_comments(@model)
        end
      end
    end

    describe ":agent_as_end_user Arturo is enabled" do
      before do
        Arturo.enable_feature!(:agent_as_end_user)
      end

      describe "user can view private content" do
        before do
          @user.expects(:can?).with(:view_private_content, @model).returns(true)
        end

        describe "> 2 comments" do
          subject { [1, 2, 3, 4] }

          it "returns last two comments" do
            assert_equal [4, 3], @presenter.last_comments(@model)
          end
        end
      end

      describe "user cannot view private content" do
        before do
          @user.expects(:can?).with(:view_private_content, @model).returns(false)
        end

        describe "> 2 comments" do
          subject { [1, 2, 3, 4] }

          it "returns nocomments" do
            assert_equal [], @presenter.last_comments(@model)
          end
        end
      end
    end
  end

  describe "#comment" do
    describe ":agent_as_end_user Arturo is disabled" do
      before do
        Arturo.disable_feature!(:agent_as_end_user)
      end

      describe "latest comment is mapped" do
        it "returns mapped comment" do
          comment_stub = stub('comment')
          @model.expects(:latest_comment_mapped).returns(comment_stub)
          assert_equal comment_stub, @presenter.comment(@model)
        end
      end

      describe "latest comment is not mapped" do
        it "returns latest comment" do
          comment_stub = stub('comment')
          @model.expects(:latest_comment_mapped).returns(nil)
          @model.expects(:latest_comment).returns(comment_stub)
          assert_equal comment_stub, @presenter.comment(@model)
        end
      end
    end

    describe ":agent_as_end_user Arturo is enabled" do
      before do
        Arturo.enable_feature!(:agent_as_end_user)
      end

      describe "user can view private content" do
        before do
          @user.expects(:can?).with(:view_private_content, @model).returns(true)
        end

        it "returns mapped comment" do
          comment_stub = stub('comment')
          @model.expects(:latest_comment_mapped).returns(comment_stub)
          assert_equal comment_stub, @presenter.comment(@model)
        end
      end

      describe "user cannot view private content" do
        before do
          @user.expects(:can?).with(:view_private_content, @model).returns(false)
        end

        it "returns nil" do
          assert_nil @presenter.comment(@model)
        end
      end
    end
  end

  describe "last_comments side-load" do
    subject { @presenter.model_json(@model)[:ticket] }

    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:last_comments).returns(true)
    end

    it "includes last two comments and all comments count" do
      assert subject[:last_comments].size <= 2,
        "there should never be more than 2 comments inside 'last_comments'"
      assert_equal 3, subject[:comment_count]
    end

    it "does not include last two comments and all comments count unless directed" do
      @presenter.stubs(:side_load?).with(:last_comments).returns(false)
      assert_nil subject[:last_comments]
      assert_nil subject[:comment_count]
    end

    describe "side-loading" do
      subject { @presenter.present(@model) }

      before do
        @presenter.stubs(:side_load?).with(:users).returns(true)
      end

      it "side-loads comment authors" do
        authors = @presenter.last_comments(@model).map(&:author_id)
        assert authors.all? { |author| subject[:users].detect { |user| user[:id] == author } }
      end
    end
  end

  describe "sla_next_breach_at side-load" do
    subject { @presenter.model_json(@model)[:ticket] }

    before do
      @presenter.stubs(side_load?: false)
      @presenter.stubs(:side_load?).with(:sla_next_breach_at).returns(true)
    end

    describe "when the account does not have the SLA feature" do
      before { @presenter.account.stubs(:has_service_level_agreements?).returns(false) }

      it "should not include policy metric info" do
        refute subject.key?(:sla_policy_metric)
      end
    end

    describe "when the account has the SLA feature" do
      before { @presenter.account.stubs(:has_service_level_agreements?).returns(true) }

      it "includes sla_policy_metric info" do
        assert_not_nil subject[:sla_policy_metric]
      end

      it "includes breach_at info" do
        assert subject[:sla_policy_metric].key?(:breach_at)
      end

      it "includes stage info" do
        assert subject[:sla_policy_metric].key?(:stage)
      end

      it "includes metric info" do
        assert subject[:sla_policy_metric].key?(:metric)
      end

      it "does not include sla_policy_metric count unless directed" do
        @presenter.stubs(:side_load?).with(:sla_next_breach_at).returns(false)
        refute subject.key?(:sla_policy_metric)
      end

      describe 'when monitoring performance' do
        let(:span) { stub(:span) }

        before { subject }

        before_should 'trace a ticket sla metric' do
          span.expects(:set_tag).with(
            Datadog::Ext::Analytics::TAG_ENABLED,
            true
          )

          span.expects(:set_tag).with('zendesk.account_id', @model.account_id)
          span.expects(:set_tag).with('zendesk.ticket.id', @model.id)

          span.expects(:set_tag).with(
            'zendesk.account_subdomain',
            @model.account.subdomain
          )

          ZendeskAPM.
            expects(:trace).
            with(
              'view_rows_presenter.ticket.sla.metric',
              service: Zendesk::Sla::APM_SERVICE_NAME
            ).
            yields(span)
        end
      end
    end
  end

  describe "ticket" do
    describe "ticket fields" do
      before do
        @model.stubs(:title).returns("{{ticket.account}}")
        @model.stubs(:description).returns("{{ticket.requester}}")
        @output = @presenter.model_json(@model)
      end

      it "presents ticket id" do
        assert_equal @model.nice_id, @output[:ticket][:id]
      end

      it "presents subject and resolve any placeholders" do
        assert_equal "Minimum account", @output[:ticket][:subject]
      end

      it "stills show subject from title even when subject is null" do
        @model.stubs(:subject).returns(nil)
        assert_not_nil @output[:ticket][:subject]
      end

      it "presents ticket description and resolve any placeholders" do
        assert_equal @model.requester.name, @output[:ticket][:description]
      end
    end

    describe_with_and_without_arturo_enabled :mobile_views_execute_date_sideloads do
      before do
        @presenter.stubs(side_load?: false)
        @presenter.stubs(:side_load?).with(:dates).returns(true)
        @output = @presenter.model_json(@model)
      end

      it "will side-load dates if arturo is enabled and sideload is used" do
        if @arturo_enabled
          assert_equal @model.created_at, @output[:ticket][:created_at]
          assert_equal @model.updated_at, @output[:ticket][:updated_at]
        else
          assert_nil @output[:ticket][:created_at]
          assert_nil @output[:ticket][:updated_at]
        end
      end

      it "will not show dates if not side-loaded, regardless of whether or not arturo is enabled" do
        @presenter.stubs(:side_load?).with(:dates).returns(false)
        output = @presenter.model_json(@model)

        assert_nil output[:ticket][:created_at]
        assert_nil output[:ticket][:updated_at]
      end
    end

    describe "group field" do
      let(:output) { @presenter.present(@model) }

      describe "when the groups feature is enabled" do
        before { Account.any_instance.stubs(:has_groups?).returns(true) }
        it "should show the group_id" do
          assert_includes output[:columns].map { |c| c[:id] }, 'group'
          assert(output[:row].key?("group_id"))
        end
      end

      describe "when the groups feature is disabled" do
        before { Account.any_instance.stubs(:has_groups?).returns(false) }
        it "does NOT include the group field " do
          refute_includes output[:columns].map { |c| c[:id] }, 'group'
          assert_equal false, output[:row].key?("group_id")
        end
      end
    end

    describe 'mapped latest chat message' do
      let(:chat_message) { stub('chat message', body: 'ABC', created_at: 'now', author_id: users(:minimum_agent).id, public?: true) }

      before do
        @model.latest_chat_message_mapped = chat_message
        @presenter = Api::V2::Rules::ViewRowsPresenter.new(@user, url_builder: mock_url_builder, view: @view, includes: [:latest_chat_message])
        @output = @presenter.present(@model)
      end

      it "presents body" do
        assert_equal chat_message.body, @output[:row][:ticket][:last_chat_message][:body]
      end

      it "presents public" do
        assert @output[:row][:ticket][:last_chat_message][:public]
      end

      it "includes user in side loads" do
        assert @output[:users].any? { |user| user[:id] == chat_message.author_id }
      end

      it "presents author_id" do
        assert_equal users(:minimum_agent).id, @output[:row][:ticket][:last_chat_message][:author_id]
      end
    end

    describe "mapped latest comment" do
      subject { stub(id: 10, body: "ABC", plain_body: "ABC", created_at: "now", author_id: users(:minimum_agent).id, is_public?: true, 'url_builder=': nil) }

      before do
        @model.latest_comment_mapped = subject
        @output = @presenter.present(@model)
      end

      it "presents latest comment" do
        assert_equal subject.body, @output[:row][:ticket][:last_comment][:body]
      end

      it "presents latest comment id" do
        assert_equal subject.id, @output[:row][:ticket][:last_comment][:id]
      end

      it "presents public" do
        assert @output[:row][:ticket][:last_comment][:public]
      end

      it "includes user in side loads" do
        assert @output[:users].any? { |user| user[:id] == subject.author_id }
      end

      it "presents author_id" do
        assert_equal users(:minimum_agent).id, @output[:row][:ticket][:last_comment][:author_id]
      end
    end

    describe "with a latest comment " do
      subject { stub(id: 10, body: "ABC", plain_body: "ABC", created_at: "now", author_id: "stub", is_public?: true, 'url_builder=': nil) }

      before do
        @model.expects(:latest_comment).returns(subject)
        @output = @presenter.model_json(@model)
      end

      it "presents latest comment" do
        assert_equal subject.body, @output[:ticket][:last_comment][:body]
      end

      it "presents latest comment id" do
        assert_equal subject.id, @output[:ticket][:last_comment][:id]
      end

      it "presents public" do
        assert @output[:ticket][:last_comment][:public]
      end
    end

    describe "when latest comment has raw markdown" do
      subject { stub(id: 10, body: "**Subdomain**\nbluebells", plain_body: "Subdomain\n\nbluebells", created_at: "now", author_id: "stub", is_public?: true, 'url_builder=': nil) }

      before do
        @model.expects(:latest_comment).returns(subject)
        @output = @presenter.model_json(@model)
      end

      it "presents latest comment without markdown" do
        assert_equal subject.plain_body, @output[:ticket][:last_comment][:body]
      end
    end

    describe "without a latest comment (preview)" do
      before do
        @model.expects(:latest_comment).returns(nil)
        @output = @presenter.model_json(@model)
      end

      it "does not present latest comment" do
        assert_nil @output[:ticket][:comment]
      end
    end
  end

  describe "with stubbed fields" do
    before do
      @view.output.stubs(:columns).returns([*subject])
      @output = @presenter.model_json(@model)
    end

    describe "updated_by_type" do
      subject { :updated_by_type }

      before do
        I18n.with_locale(translation_locales(:brazilian_portuguese)) do
          @output = @presenter.model_json(@model)
        end
      end

      it "is localized" do
        assert_equal "PORTUGUESE AGENT", @output[:updated_by_type]
      end
    end

    describe "nice_id" do
      subject { :nice_id }
      before { @output = @presenter.present(@model) }

      it "does not set nice_id" do
        assert @output[:row].key?(:ticket_id)
        refute @output[:row].key?(:nice_id)
      end

      it "adds to columns" do
        assert_not_nil @output[:columns].detect { |col| col[:id] == "ticket_id" }
        assert_nil @output[:columns].detect { |col| col[:id] == "nice_id" }
      end
    end

    describe "brand_id" do
      subject { :brand_id }

      let(:output) { @presenter.present(@model)[:row][:ticket] }

      describe "on accounts without multibrand" do
        before { Account.any_instance.stubs(:has_multibrand?).returns(false) }

        it "does not set brand_id in the ticket payload" do
          refute output.key?(subject)
        end
      end

      describe "on accounts with multibrand" do
        before { Account.any_instance.stubs(:has_multibrand?).returns(true) }

        it "sets brand_id in the ticket payload" do
          assert_equal @model.brand_id, output[subject]
        end
      end
    end

    describe "with association fields" do
      subject { [:submitter, :assignee, :requester, :group, :organization] }

      it "loads associations" do
        subject.each do |assoc|
          assert_equal @model.send(assoc).id, @output["#{assoc}_id"], "could not find #{assoc} in output"
        end
      end
    end

    describe "locale_id" do
      subject { :locale_id }
      before { @output = @presenter.present(@model) }

      it "loads requester locale" do
        assert_equal @model.requester.translation_locale.name, @output[:row][:locale]
      end

      it "maps locale_id -> locale in columns" do
        assert @output[:columns].detect { |column| column[:id] == "locale" }, "no 'locale' in #{@output[:columns].inspect}"
      end
    end

    describe "subject" do
      subject { :subject }

      before do
        @model.stubs(:subject).returns("{{ticket.requester}}")
        @output = @presenter.present(@model)
      end

      it "resolves the placeholder value" do
        assert_equal @model.requester.name, @output[:row][:subject]
      end
    end
  end

  describe "with given fields" do
    before do
      @custom_fields = [ticket_fields(:field_tagger_custom), ticket_fields(:field_checkbox_custom)]
      @view.output.columns.push(*@custom_fields.map(&:id))

      @fields = ZendeskRules::RuleField.lookup(@view.output.columns)
      @system_fields = @fields.reject { |f| f.is_a?(ZendeskRules::CustomRuleField) }
      @system_fields.map! { |field| field.identifier.to_sym }
    end

    describe "with no field entries" do
      before do
        @output = @presenter.model_json(@model)
      end

      it "does not present any fields" do
        assert_empty @output[:fields], "#{@output[:fields].inspect} should be empty"
      end
    end

    describe "via" do
      before do
        @view.output.stubs(columns: [:nice_id, :via])
      end

      it "presents via object" do
        @presenter.expects(:serialized_via_object).returns(via = stub)
        @output = @presenter.model_json(@model)
        assert_equal via, @output[:via]
      end
    end

    describe "fields" do
      before do
        @model.ticket_field_entries.create!(value: @custom_fields[0].custom_field_options.first.value, ticket_field: @custom_fields[0])
        @model.ticket_field_entries.create!(value: true, ticket_field: @custom_fields[1])
        @output = @presenter.model_json(@model)
      end

      it "presents custom fields" do
        @custom_fields.each do |custom_field|
          assert @output[:fields].any? { |field| field[:id] == custom_field.id },
            "could not find custom field #{custom_field.id} in :fields"
        end
      end

      it "presents custom field under root" do
        @custom_fields.each do |custom_field|
          assert_not_nil @output[custom_field.id],
            "could not find custom field #{custom_field.id} under the root node"
        end
      end

      it "presents custom field names under root" do
        @custom_fields.each do |ticket_field|
          found = @output[ticket_field.id]

          assert_not_nil found, "couldn't find #{ticket_field.inspect} entry in #{@output.inspect}"

          if ticket_field.respond_to?(:custom_field_options)
            assert ticket_field.custom_field_options.any? { |cfo| cfo.name == found }
          end
        end
      end

      it "presents custom field names" do
        @custom_fields.each do |ticket_field|
          found = @output[:fields].find do |field|
            name = if ticket_field.respond_to?(:custom_field_options)
              ticket_field.custom_field_options.any? { |cfo| cfo.name == field[:name] }
            else
              field[:name].nil?
            end

            field[:id] == ticket_field.id && name
          end

          assert found, "couldn't find #{ticket_field.inspect} entry in #{@output[:fields].inspect}"
        end
      end

      describe "dynamic content in custom fields" do
        before do
          @presenter = Api::V2::Rules::ViewRowsPresenter.new(@user, url_builder: mock_url_builder, view: @view)
          @field = @custom_fields.first
          @fbattrs = {is_fallback: true, nested: true, value: "abc", translation_locale_id: 1 }
          @text = Cms::Text.create!(name: "abc", fallback_attributes: @fbattrs, account: accounts(:minimum))
        end

        it "renders dc in columns" do
          @field.title = "{{dc.abc}}"
          @field.save
          assert json = @presenter.present(@model)[:columns].find { |c| c[:id] == @field.id }
          assert_equal json[:title], "abc"
        end

        it "renders dc in custom field values" do
          @field.custom_field_options.first.update_attribute(:name, "{{dc.abc}}")
          assert json = @presenter.present(@model.reload)[:row]
          assert json = json[:custom_fields].find { |cf| cf[:id] == @field.id }
          assert_equal json[:name], "abc"
        end
      end

      describe "output" do
        before do
          [:requester, :assignee, :group, :organization, :submitter].each do |sym|
            next unless @system_fields.include?(sym)
            @system_fields.delete(sym)
            @system_fields.push("#{sym}_id")
          end
        end

        it "presents system fields" do
          (@system_fields + [:ticket]).each do |field|
            assert @output.key?(field), "could not find #{field} in output"
          end
        end
      end
    end

    describe "columns" do
      before do
        @output = @presenter.as_json(@model)
      end

      it "includes columns" do
        @system_fields.each do |field|
          assert @output[:columns].any? { |c| c[:id] == field.to_s }, "could not find #{field} in :columns"
        end
      end
    end

    should_side_load :view, :groups, :organizations

    describe "#side_load?" do
      it "returns true if the field is in the view" do
        assert @presenter.send(:side_load?, :requester)
      end

      it "does not return true if there isn't a field in the view an it isn't explicitly included" do
        refute @presenter.send(:side_load?, :organization)
      end
    end

    describe "users sideloading" do
      before do
        @output = @presenter.present(@model)
        @users = [@model.requester_id, @model.assignee_id].sort
      end

      it "only present basic keys" do
        assert_equal [:id, :name, :url], @output[:users].first.keys.sort_by(&:to_s)
      end

      it "side loads all users" do
        assert_equal @users, @output[:users].map { |user| user[:id] }.sort
      end
    end

    describe "group sideloading" do
      before do
        @output = @presenter.present(@model)
      end

      it "only present basic keys" do
        assert_equal [:id, :name, :url], @output[:groups].first.keys.sort_by(&:to_s)
      end

      it "side loads all groups" do
        assert_equal [@model.group_id], @output[:groups].map { |group| group[:id] }
      end
    end

    describe "conditions sideloading" do
      before do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:conditions).returns(true)
      end

      describe "with view sideloading disabled" do
        before do
          @output = @presenter.as_json(@model)
        end

        it "does not include execution" do
          assert_nil @output[:view]
        end
      end

      describe "with view sideloading enabled" do
        before do
          @presenter.stubs(:side_load?).with(:view).returns(true)
          @output = @presenter.as_json(@model)
        end

        it "includes view" do
          assert_not_nil @output[:view]
        end

        it "includes conditions" do
          assert_not_nil @output[:view][:conditions]
        end
      end
    end

    describe "execution sideloading" do
      before do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:execution).returns(true)
      end

      describe "with view sideloading disabled" do
        before do
          @output = @presenter.as_json(@model)
        end

        it "does not include execution" do
          assert_nil @output[:view]
        end

        describe "with nil body" do
          before do
            @model.comments.any_instance.stubs(:body).returns(nil)
          end
          it 'present tickets' do
            assert @presenter.as_json(@model)
          end
        end
      end

      describe "with view sideloading enabled" do
        before do
          @presenter.stubs(:side_load?).with(:view).returns(true)
          @output = @presenter.as_json(@model)
        end

        it "includes view" do
          assert_not_nil @output[:view]
        end

        it "includes execution" do
          assert_not_nil @output[:view][:execution]
        end
      end
    end

    describe "view side load" do
      before do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:view).returns(true)
        @output = @presenter.as_json(@model)
      end

      it "includes view" do
        assert_equal @view.id, @output[:view][:id]
      end
    end
  end
end
