require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::MacroSearchPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  fixtures :all

  let(:macro) { create_macro }

  let(:account) { accounts(:minimum) }
  let(:user)    { macro.account.owner }

  let(:includes_collection) { [] }

  let(:presenter) do
    Api::V2::Rules::MacroSearchPresenter.new(
      user,
      url_builder: mock_url_builder,
      includes:    includes_collection
    )
  end

  before do
    @model     = macro
    @presenter = presenter
  end

  should_present_keys :id,
    :title,
    :raw_title,
    :active,
    :position,
    :restriction,
    :actions,
    :updated_at,
    :created_at,
    :url,
    :description

  should_present_method :id
  should_present_method :title
  should_present_method :is_active?, as: :active
  should_present_method :position
  should_present_method :actions, with: Api::V2::Rules::ActionsPresenter
  should_present_method :updated_at
  should_present_method :created_at
  should_present_method :description

  describe '#model_json' do
    let(:json) { presenter.model_json(macro) }

    describe 'when side-loading permissions' do
      let(:includes_collection) { %i[permissions] }

      let(:user) { users(:minimum_agent) }

      describe 'and the user cannot edit the macro' do
        before { user.stubs(:can?).with(:edit, macro).returns(false) }

        it 'sets `can_edit` to false' do
          refute json[:permissions][:can_edit]
        end
      end

      describe 'and the user can edit the macro' do
        before { user.stubs(:can?).with(:edit, macro).returns(true) }

        it 'sets `can_edit` to true' do
          assert json[:permissions][:can_edit]
        end
      end
    end

    describe 'when side-loading usage counts' do
      before do
        Timecop.freeze

        Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

        [30.minutes.ago, 10.hours.ago, 4.days.ago, 29.days.ago].each do |time|
          Timecop.travel(time) do
            record_usage(type: :macro, id: macro.id)
          end
        end
      end

      describe 'and side-loading hourly usage' do
        let(:includes_collection) { %i[usage_1h] }

        describe 'and the hourly usage is stored' do
          before { macro.usage.hourly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_1h]
          end
        end

        describe 'and the hourly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 1, json[:usage_1h]
          end
        end
      end

      describe 'and side-loading daily usage' do
        let(:includes_collection) { %i[usage_24h] }

        describe 'and the daily usage is stored' do
          before { macro.usage.daily = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_24h]
          end
        end

        describe 'and the daily usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 2, json[:usage_24h]
          end
        end
      end

      describe 'and side-loading weekly usage' do
        let(:includes_collection) { %i[usage_7d] }

        describe 'and the weekly usage is stored' do
          before { macro.usage.weekly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_7d]
          end
        end

        describe 'and the weekly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 3, json[:usage_7d]
          end
        end
      end

      describe 'and side-loading monthly usage' do
        let(:includes_collection) { %i[usage_30d] }

        describe 'and the monthly usage is stored' do
          before { macro.usage.monthly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_30d]
          end
        end

        describe 'and the monthly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 4, json[:usage_30d]
          end
        end
      end
    end

    describe 'when side-loading the app installation' do
      let(:includes_collection) { %i[app_installation] }

      before do
        macro.stubs(:app_installation).returns(
          'id'       => 42,
          'settings' => {'title' => 'Test app'}
        )
      end

      it 'sets the app installation' do
        assert_equal 42, presenter.model_json(macro)[:app_installation]['id']
      end
    end

    describe 'when presenting a collection' do
      let(:json)       { presenter.present(collection) }
      let(:collection) { Macro.where(account_id: account.id) }

      before do
        Macro.destroy_all

        create_macro(title: 'Beta::Epsilon::Three')
        create_macro(title: 'Theta::Iota::Four')
        create_macro(title: 'Zeta::Five')
      end

      it 'does not perform N+1 queries' do
        assert_no_n_plus_one { presenter.present(collection) }
      end

      it 'presents a collection' do
        assert_equal(
          %w[Beta::Epsilon::Three Theta::Iota::Four Zeta::Five],
          json[:macros].map { |macro| macro[:title] }
        )
      end

      it 'does not include categories' do
        refute json.key?(:categories)
      end
    end
  end
end
