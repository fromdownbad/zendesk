require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::MacroApplicationPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :accounts,
    :rules,
    :taggings,
    :tickets,
    :ticket_fields,
    :users

  let(:account) { accounts(:minimum) }
  let(:user) { account.owner }

  let(:ticket) { nil }

  let(:default_action) do
    DefinitionItem.new('status_id', 'is', StatusType.NEW)
  end

  let(:comment_value_action) do
    DefinitionItem.new(
      'comment_value',
      'is',
      ['Comment. Tags {{ticket.tags}}. Title {{ticket.title}}']
    )
  end

  let(:comment_value_html_action) do
    DefinitionItem.new(
      'comment_value_html',
      nil,
      ['<h1>Rich comment</h1>']
    )
  end

  let(:comment_mode_action) do
    DefinitionItem.new('comment_mode_is_public', nil, [false])
  end

  let(:subject_action) do
    DefinitionItem.new('subject', 'is', ['Hello {{ticket.tags}}'])
  end

  let(:current_tags_action) do
    DefinitionItem.new('current_tags', 'is', ['abc def'])
  end

  let(:set_tags_action) do
    DefinitionItem.new('set_tags', 'is', ['abc def'])
  end

  let(:actions) { [default_action] }

  let(:macro) do
    create_macro(
      definition: Definition.new.tap do |definition|
        actions.each { |action| definition.actions.push(action) }
      end
    )
  end

  let(:model) do
    Zendesk::Rules::Macro::Application.new(
      macro:  macro,
      ticket: ticket,
      user:   user
    )
  end

  let(:presenter) do
    Api::V2::Rules::MacroApplicationPresenter.new(
      user,
      ticket:      ticket,
      url_builder: mock_url_builder
    )
  end

  let(:result) { presenter.present(model)[:result] }

  before do
    @model     = model
    @presenter = presenter
  end

  should_present_keys :ticket

  describe 'when applying a status' do
    let(:macro) { rules(:macro_incident_escalation) }

    it 'includes status' do
      assert_equal 'open', result[:ticket][:status]
    end
  end

  describe 'when applying a priority' do
    let(:macro) { rules(:macro_incident_escalation) }

    it 'includes priority' do
      assert_equal 'normal', result[:ticket][:priority]
    end
  end

  describe 'when applying a type' do
    let(:macro) { rules(:macro_incident_escalation) }

    it 'includes type' do
      assert_equal 'question', result[:ticket][:type]
    end
  end

  describe 'when applying a comment' do
    let(:comment) { result[:ticket][:comment] }

    let(:raw_comment) do
      comment[:scoped_body].find do |channel|
        channel.first == 'channel:raw'
      end
    end

    let(:comment_value) do
      model.result.with_indifferent_access[:comment][:value]
    end

    let(:rendered_comment) { ZendeskText::Markdown.html(comment_value) }

    describe 'and the comment contains liquid' do
      let(:ticket) { tickets(:minimum_1) }
      let(:actions) { [comment_value_action] }

      let(:comment_value_action) do
        DefinitionItem.new(
          'comment_value',
          nil,
          ['[{{ticket.account}}] Requester: {{ticket.requester.name}}']
        )
      end

      it 'presents comment changes' do
        assert_equal comment_value, comment[:body]
      end

      it 'replaces the liquid' do
        assert_no_match %r|{{ticket.*}}|, comment[:body]
      end

      it 'presents the pre-liquid value in the raw channel' do
        Arturo.enable_feature! :macro_application_no_liquid
        assert_match %r|{{ticket.*}}|, raw_comment[1]
      end

      it 'provides the HTML of the rendered comment body' do
        assert_equal rendered_comment, comment[:html_body]
      end

      describe 'and the template contains a link' do
        let(:comment_value_action) do
          DefinitionItem.new('comment_value', nil, ['Link: {{ticket.link}}'])
        end

        let(:link) { ticket.url(for_agent: true) }

        it 'presents the correct `html_body`' do
          assert_equal(
            "<p dir='auto'>Link: <a href=\"#{link}\">#{link}</a></p>",
            comment[:html_body]
          )
        end
      end

      describe 'and the template contains an email address' do
        let(:comment_value_action) do
          DefinitionItem.new(
            'comment_value',
            nil,
            ['Email: {{current_user.email}}']
          )
        end

        let(:email) { user.email }

        it 'presents the correct `html_body`' do
          assert_equal(
            "<p dir='auto'>Email: <a href=\"mailto:#{email}\">#{email}</a></p>",
            comment[:html_body]
          )
        end
      end
    end

    describe 'and the comment does not contain liquid' do
      let(:actions) do
        [
          DefinitionItem.new(
            'comment_value',
            nil,
            [
              "We're currently experiencing unusually high traffic. " \
                "We'll get back to you as soon as possible."
            ]
          )
        ]
      end

      it 'presents the comment changes' do
        assert_equal comment_value, comment[:body]
      end

      it 'does not create a comment with a raw channel' do
        refute raw_comment.present?
      end

      it 'provides the HTML of the rendered comment body' do
        assert_equal rendered_comment, comment[:html_body]
      end
    end

    describe 'and the macro has an HTML comment' do
      let(:actions) { [comment_value_html_action] }

      it 'presents the proper keys' do
        assert_equal %w[body html_body], comment.keys
      end

      it 'presents the correct `body`' do
        assert_equal '<h1>Rich comment</h1>', comment[:body]
      end

      it 'presents the correct `html_body`' do
        assert_equal '<h1>Rich comment</h1>', comment[:html_body]
      end
    end

    describe 'and the macro has a rich and a basic comment' do
      let(:actions) { [comment_value_html_action, comment_value_action] }

      it 'presents the proper keys' do
        assert_equal %w[body html_body], comment.keys
      end

      it 'presents the correct `body`' do
        assert_equal comment_value, comment[:body]
      end

      it 'presents the correct `html_body`' do
        assert_equal '<h1>Rich comment</h1>', comment[:html_body]
      end
    end

    describe 'and the macro also has a comment-mode action' do
      let(:actions) { [comment_value_html_action, comment_mode_action] }

      it 'has the proper keys' do
        assert_equal %w[body html_body public], comment.keys
      end

      it 'has the correct comment mode' do
        assert_equal false, comment['public']
      end
    end

    describe 'and the macro does not have a comment and has a comment mode' do
      let(:actions) { [comment_mode_action] }

      it 'has the proper keys' do
        assert_equal %w[public], comment.keys
      end

      it 'has the correct comment mode' do
        assert_equal false, comment['public']
      end
    end
  end

  describe 'when applying other ticket attributes' do
    let(:ticket_presenter) do
      Api::V2::Tickets::TicketPresenter.new(user, url_builder: mock_url_builder)
    end

    let(:ticket) { tickets(:minimum_1) }
    let(:macro) { rules(:macro_incident_escalation) }

    before do
      ticket.attributes = model.result.with_indifferent_access.except(:comment)
    end

    it 'presents the ticket with changes' do
      assert_equal(
        ticket_presenter.model_json(ticket),
        result[:ticket].except(:comment)
      )
    end

    it 'presents the comment changes' do
      assert_equal(
        model.result.with_indifferent_access[:comment][:value],
        result[:ticket][:comment][:body]
      )
    end
  end

  describe 'when applying tags' do
    let(:actions) { [set_tags_action] }

    it 'includes tags' do
      assert_equal %w[abc def], result[:ticket][:tags]
    end

    describe 'and applying a subject that includes tags' do
      let(:ticket) { tickets(:minimum_1) }

      let(:actions) { [subject_action, set_tags_action] }

      it 'includes the subject with tags' do
        assert_equal 'Hello abc def', result[:ticket][:subject]
      end
    end

    describe 'and applying a comment that includes tags' do
      let(:ticket) { tickets(:minimum_1) }

      let(:actions) { [comment_value_action, subject_action, set_tags_action] }

      it 'includes the comment body with tags' do
        assert_includes(
          result[:ticket][:comment][:body],
          'Comment. Tags abc def. Title Hello abc def'
        )
      end
    end

    describe 'and a ticket is provided' do
      let(:ticket) { tickets(:minimum_1) }
      let(:tag_name) { taggings(:minimum_1).tag_name }

      let(:actions) { [DefinitionItem.new('current_tags', 'is', tags)] }

      describe 'and it does not already have the tags' do
        let(:tags) { %w[hi] }

        it 'includes the new tag' do
          assert_equal [tag_name, 'hi'], result[:ticket][:tags]
        end
      end

      describe 'and it already has the tags' do
        let(:tags) { [tag_name] }

        it 'does not add duplicate tags' do
          assert_equal [tag_name], result[:ticket][:tags]
        end
      end
    end
  end

  describe 'when applying a custom ticket field' do
    let(:custom_ticket_field) { ticket_fields(:field_checkbox_custom) }
    let(:other_custom_ticket_field) { ticket_fields(:field_textarea_custom) }

    let(:custom_ticket_field_action) do
      DefinitionItem.new(
        "ticket_fields_#{custom_ticket_field.id}",
        'is',
        true
      )
    end

    let(:custom_field) do
      result[:ticket][:custom_fields].detect do |field|
        field[:id] == custom_ticket_field.id
      end
    end

    let(:actions) { [custom_ticket_field_action] }

    it 'adds custom fields under the `custom_fields` key' do
      assert custom_field[:value]
    end

    it 'does not add custom ticket fields under the `ticket` key' do
      assert_nil result[:ticket]["ticket_fields_#{custom_ticket_field.id}"]
    end

    describe 'and a ticket is provided' do
      let(:ticket) { tickets(:minimum_1) }

      describe 'and it has an existing entry for that field' do
        before do
          ticket.ticket_field_entries.create(
            ticket_field: custom_ticket_field,
            value:        '1'
          )
        end

        it 'returns the correct custom fields' do
          assert_equal(
            ticket.account.ticket_fields.custom.active.count(:all),
            result[:ticket][:custom_fields].size
          )
        end
      end

      describe 'and it has an existing entry for that field with different value' do
        before do
          ticket.ticket_field_entries.create(
            ticket_field: custom_ticket_field,
            value:        '0'
          )
        end

        it 'returns the correct custom fields with only the new value applied by macro' do
          assert_equal(
            ticket.account.ticket_fields.custom.active.count(:all),
            result[:ticket][:custom_fields].size
          )
          assert result[:ticket][:custom_fields].include?(
            id:    custom_ticket_field.id,
            value: true
          )
          refute result[:ticket][:custom_fields].include?(
            id:    custom_ticket_field.id,
            value: false
          )
        end
      end

      describe 'and it has an existing entry for another field' do
        before do
          ticket.ticket_field_entries.create(
            ticket_field: other_custom_ticket_field,
            value:        'hi'
          )
        end

        it 'returns the existing fields and adds new ones' do
          assert_equal(
            ticket.account.ticket_fields.custom.active.count(:all),
            result[:ticket][:custom_fields].size
          )
        end
      end
    end
  end

  describe 'when applying collaborators' do
    let(:cc_user_action) do
      DefinitionItem.new('cc', 'is', user.id)
    end

    let(:actions) { [cc_user_action] }

    describe_with_arturo_enabled(:email_ccs) do
      describe_with_arturo_setting_enabled(
        :follower_and_email_cc_collaborations
      ) do
        describe_with_arturo_setting_enabled(:ticket_followers_allowed) do
          it 'presents the user under the `follower_ids` key' do
            assert_includes result[:ticket][:follower_ids], user.id
          end

          describe 'and the added user is already a follower' do
            let(:ticket) { tickets(:minimum_1) }

            before do
              ticket.stubs(followers: [user])
            end

            it 'presents a uniq list of users under the `follower_ids` key' do
              assert_equal 1, result[:ticket][:follower_ids].count { |id| id == user.id }
            end

            it 'does not present the `followers_list` key' do
              refute result[:ticket].key?(:followers_list)
            end
          end

          describe 'when there are no collaborators added in the macro' do
            let(:actions) { [default_action] }

            it 'presents an empty value for the `follower_ids` key' do
              assert_equal [], result[:ticket][:follower_ids]
            end
          end
        end
      end
    end

    it 'presents `collaborator_ids`' do
      assert_equal [user.id], result[:ticket][:collaborator_ids]
    end

    describe 'and the added user is already a collaborator' do
      let(:ticket) { tickets(:minimum_1) }

      before { ticket.collaborations.build(user: user) }

      it 'presents a uniq list of users under the `collaborator_ids` key' do
        assert_equal 1, result[:ticket][:collaborator_ids].count { |id| id == user.id }
      end
    end

    it 'does not present the `collaborator_list` key' do
      refute result[:ticket].key?(:collaborator_list)
    end

    describe 'when there are no collaborators added in the macro' do
      let(:actions) { [default_action] }

      it 'presents an empty value for the `collaborator_ids` key' do
        assert_equal [], result[:ticket][:collaborator_ids]
      end
    end
  end

  describe 'when applying attachments' do
    let!(:attachment) do
      RuleAttachment.create! do |ra|
        ra.account_id    = account.id
        ra.user_id       = user.id
        ra.uploaded_data = fixture_file_upload('small.png')
        ra.filename      = 'small.png'
      end
    end

    let(:macro) { create_macro }

    before do
      attachment.associate(macro)
    end

    it 'includes `upload`' do
      assert_equal(
        Api::V2::UploadPresenter.
          new(user, url_builder: mock_url_builder).
          present(model.upload_token)[:upload].with_indifferent_access,
        result[:ticket][:upload]
      )
    end

    describe 'and the macro does not have any attachments' do
      before { RuleAttachmentRule.destroy_all }

      it 'does not include `upload`' do
        assert_nil result[:ticket][:upload]
      end
    end
  end
end
