require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Rules::UserViewExecutionPresenter do
  extend Api::Presentation::TestHelper
  include CustomFieldsTestHelper

  fixtures :rules, :accounts, :users

  before do
    @view      = rules(:minimum_custom_fields_view)
    @account   = @view.account
    @model     = @view.output
    @presenter = Api::V2::Rules::UserViewExecutionPresenter.new(@view.account.owner, url_builder: mock_url_builder, user_view: @view)
  end

  should_present_keys :columns, :group, :sort

  describe "group" do
    subject { @presenter.present(@model)[:execution][:group] }

    describe "with a field" do
      before do
        @model.stubs(group: { id: "id", order: "asc" })
      end

      it "presents rule id, title, and order" do
        assert_equal({ id: "id", title: "Id", order: "asc" }, subject)
      end
    end
    describe "without grouping" do
      before do
        @model.stubs(group: nil)
      end
      it "is nil" do
        assert_nil subject
      end
    end
  end

  describe "sort" do
    subject { @presenter.present(@model)[:execution][:sort] }

    describe "with a field" do
      before do
        @model.stubs(sort: { id: "id", order: "asc" })
      end

      it "presents rule id, title, and order" do
        assert_equal({ id: "id", title: "Id", order: "asc" }, subject)
      end
    end
    describe "without sorting" do
      before do
        @model.stubs(sort: nil)
      end
      it "is nil" do
        assert_nil subject
      end
    end
  end

  describe "columns" do
    subject { @presenter.present(@model)[:execution][:columns] }

    describe "with system field" do
      before do
        create_user_custom_field!("system::sf1", "txt.something", "Text", is_system: true)
        @model.columns = [:id, :name, :created_at, :"custom_fields.system::sf1"]
      end

      it "presents columns" do
        expected = [
          { id: :id, title: "User ID" },
          { id: :name, title: "Name" },
          { id: :created_at, title: "Created" },
          { id: :"custom_fields.system::sf1", title: I18n.t('txt.something'), type: "text", url: :api_v2_user_field_url }
        ]
        assert_equal expected, subject
      end
    end

    describe "with custom field" do
      before do
        @field = create_user_custom_field!("favorites", "Favorites", "Text")
        @model.columns = [:id, :name, :created_at, :"custom_fields.favorites"]
      end

      it "presents columns" do
        expected = [
          { id: :id, title: "User ID" },
          { id: :name, title: "Name" },
          { id: :created_at, title: "Created" },
          { id: :"custom_fields.favorites", title: 'Favorites', type: "text", url: :api_v2_user_field_url }
        ]
        assert_equal expected, subject
      end

      describe 'when the field is deleted' do
        before do
          @field.destroy
        end

        it "presents columns" do
          expected = [
            { id: :id, title: "User ID" },
            { id: :name, title: "Name" },
            { id: :created_at, title: "Created" },
            { id: :"custom_fields.favorites", title: I18n.t("txt.api.v2.user_views.columns.deleted_field") }
          ]
          assert_equal expected, subject
        end
      end
    end
  end
end
