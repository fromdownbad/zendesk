require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::RuleDiffPresenter do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }

  let(:trigger) do
    create_trigger(
      definition: build_trigger_definition(
        conditions_all: conditions,
        conditions_any: conditions
      )
    )
  end

  let(:conditions) do
    [
      build_definition_item(
        field:    'status_id',
        operator: 'less_than',
        value:    1
      )
    ]
  end

  let(:presenter) do
    Api::V2::Rules::RuleDiffPresenter.new(
      trigger.account.owner,
      url_builder: mock_url_builder
    )
  end

  let(:revision_diff) do
    create_trigger_revision_diff(trigger.revisions.first, trigger.revisions.first)
  end

  let(:diff_schema) do
    File.read(
      Rails.root.join('lib/zendesk/rules/json_schema/revision_diff.json')
    )
  end

  before do
    @model     = revision_diff
    @presenter = presenter
  end

  should_present_keys :source_id,
    :target_id,
    :title,
    :description,
    :active,
    :actions,
    :conditions

  describe 'presenting the `conditions` attribute' do
    let(:condition_diffs_presenter_stub) { stub(:condition_diffs_presenter) }

    before_should 'uses the Api::V2::Rules::ConditionDiffsPresenter' do
      Api::V2::Rules::ConditionDiffsPresenter.
        expects(:new).
        returns(condition_diffs_presenter_stub)

      condition_diffs_presenter_stub.expects(:model_json).with(revision_diff)
    end

    before { presenter.model_json(revision_diff) }
  end

  describe 'presenting the `actions` attribute' do
    let(:action_diffs_presenter_stub) { stub(:action_diffs_presenter) }

    before_should 'uses the Api::V2::Rules::ActionDiffsPresenter' do
      Api::V2::Rules::ActionDiffsPresenter.
        expects(:new).
        returns(action_diffs_presenter_stub)

      action_diffs_presenter_stub.expects(:model_json).with(revision_diff)
    end

    before { presenter.model_json(revision_diff) }
  end

  it 'conforms to the revision diff schema' do
    assert_empty(
      JSON::Validator.fully_validate(
        diff_schema,
        presenter.model_json(revision_diff)
      )
    )
  end
end
