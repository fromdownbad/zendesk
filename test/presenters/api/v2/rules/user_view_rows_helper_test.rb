require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 11

describe 'UserViewRowsHelper' do
  fixtures :accounts, :users, :subscriptions

  class TestPresenter
    include Api::V2::Rules::UserViewRowsHelper

    attr_accessor :account

    def initialize(account)
      @account = account
    end
  end

  describe "first" do
    before do
      @account = accounts(:minimum)
      @user = @account.users.first
      @presenter = TestPresenter.new(@account)
    end

    describe "enterprise roles in user views" do
      before do
        Account.any_instance.stubs(:enterprise_roles_in_user_views_enabled?).returns(true)
      end

      describe "has permission sets" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          Arturo.enable_feature!(:dc_in_role_names)
          @permission_set = @account.permission_sets.create!(name: "{{zd.staff_role}}")
          @user.role_or_permission_set = @permission_set
          @user.save!
        end

        it "returns permission set translated name" do
          assert_equal "Staff", @presenter.field_value('roles', @user)
        end

        describe "user is end-user" do
          before do
            @user.roles = Role::END_USER.id
            @user.save!
          end

          it "returns role name" do
            assert_equal @user.translated_role, @presenter.field_value('roles', @user)
          end
        end

        describe "user is admin" do
          before do
            CIA.stubs(:current_actor).returns(@user)
            @user.stubs(:permission_set).returns(nil)
            @user.stubs(:has_permission_set?).returns(false)
            @user.roles = Role::ADMIN.id
            @user.save!
          end

          it "returns role name" do
            assert_equal @user.translated_role, @presenter.field_value('roles', @user)
          end
        end
      end

      describe "doesn't have permission sets" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(false)
        end

        it "returns role name" do
          assert_equal @user.translated_role, @presenter.field_value('roles', @user)
        end
      end
    end

    describe 'account without permission sets' do
      before do
        @account.stubs(:has_permission_sets?).returns(false)
        @chat_permission_set = @account.permission_sets.create!(name: "Chat Test Set")
        @user.role_or_permission_set = @chat_permission_set
        @user.save!
      end

      describe 'with Zopim chat enabled' do
        before { @account.stubs(:has_chat_permission_set?).returns(true) }

        describe 'when the user is a chat-only agent' do
          before { @user.stubs(:is_chat_agent?).returns(true) }

          it 'returns the permission set name' do
            assert_equal @chat_permission_set.name, @presenter.field_value('roles', @user)
          end
        end
      end
    end

    describe "field_value" do
      describe "when passing in a symbol" do
        it "still renders email" do
          assert_equal @user.email, @presenter.field_value(:email, @user)
        end
        it "still renders phone" do
          @user.stubs(:phone_number).returns('+12125550000')
          assert_equal '+12125550000', @presenter.field_value(:phone, @user)
        end
      end
    end
  end
end
