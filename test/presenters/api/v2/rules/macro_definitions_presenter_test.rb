require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::MacroDefinitionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:definitions) do
    Zendesk::Rules::Macro::Definitions.new(account, user)
  end

  let(:presenter) do
    Api::V2::Rules::MacroDefinitionsPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = definitions
    @presenter = presenter
  end

  should_present_keys :actions

  should_present_collection :actions,
    with: Api::V2::Rules::ActionDefinitionPresenter
end
