require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::TriggerRevisionPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account)     { accounts(:minimum) }
  let(:model)       { create_trigger.revisions.first }
  let(:url_builder) { mock_url_builder }

  let(:presenter) do
    Api::V2::Rules::TriggerRevisionPresenter.new(
      users(:minimum_agent),
      url_builder: url_builder
    )
  end

  let(:revision_url_stub) { stub(:revision_url) }

  before do
    @model     = model
    @presenter = presenter
  end

  should_present_keys :id,
    :author_id,
    :created_at,
    :snapshot,
    :url

  should_present_method :nice_id, as: :id

  should_present_method :snapshot,
    with: Api::V2::Rules::TriggerSnapshotPresenter

  should_present_methods :author_id,
    :created_at

  should_side_load :users

  describe 'when presented' do
    before do
      url_builder.
        stubs(:api_v2_trigger_revision_url).
        with(model.rule_id, model.nice_id, format: :json).
        returns(revision_url_stub)
    end

    it 'contains the `url`' do
      assert_equal revision_url_stub, presenter.model_json(model)[:url]
    end
  end

  describe 'when presenting a collection' do
    let(:collection_presenter) { stub(:collection_presenter) }

    before do
      Api::V2::Rules::TriggerRevisionCursorCollectionPresenter.
        stubs(:new).
        returns(collection_presenter)

      presenter.present([model])
    end

    before_should 'present with `TriggerRevisionCursorCollectionPresenter`' do
      collection_presenter.expects(:as_json).with([model])
    end
  end
end
