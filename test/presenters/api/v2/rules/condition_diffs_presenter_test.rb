require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::ConditionsPresenter do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  extend Api::Presentation::TestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }

  let(:conditions) { [build_definition_item] }

  let(:trigger) do
    create_trigger(
      definition: build_trigger_definition(
        conditions_all: conditions,
        conditions_any: conditions
      )
    )
  end

  let(:presenter) do
    Api::V2::Rules::ConditionDiffsPresenter.new(
      trigger.account.owner,
      url_builder: mock_url_builder
    )
  end

  let(:revision_diff) do
    create_trigger_revision_diff(trigger.revisions.first, trigger.revisions.first)
  end

  before do
    @model     = revision_diff
    @presenter = presenter
  end

  should_present_keys :any, :all

  describe '#model_json' do
    let(:json) { presenter.model_json(revision_diff) }

    let(:conditions) do
      [
        build_definition_item(
          field:    'status_id',
          operator: 'less_than',
          value:    1
        )
      ]
    end

    it "maps the `condition_all`s' `field` attributes to the correct values" do
      assert_equal :status, json[:all].first[:field].first[:content]
    end

    it "maps the `condition_any`s' `field` attributes to the correct values" do
      assert_equal :status, json[:all].first[:field].first[:content]
    end

    it "maps the `condition_all`s' `value` attributes to the correct values" do
      assert_equal 'open', json[:all].first[:value].first[:content]
    end

    it "maps the `condition_any`s' `value` attributes to the correct values" do
      assert_equal 'open', json[:all].first[:value].first[:content]
    end

    describe "when the definition's `field` is `current_via_id`" do
      let(:conditions) do
        [
          build_definition_item(
            field:    'current_via_id',
            operator: 'is',
            value:    27
          )
        ]
      end

      it "maps the `condition_all`s' `field` attributes to the correct value" do
        assert_equal 'current_via_id', json[:all].first[:field].first[:content]
      end

      it "maps the `condition_any`s' `field` attributes to the correct value" do
        assert_equal 'current_via_id', json[:all].first[:field].first[:content]
      end

      it "maps the `condition_all`s' `value` attributes to the correct value" do
        assert_equal 27, json[:all].first[:value].first[:content]
      end

      it "maps the `condition_any`s' `value` attributes to the correct value" do
        assert_equal 27, json[:all].first[:value].first[:content]
      end
    end

    describe "when the definition's `field' is `via_id`" do
      let(:conditions) do
        [
          build_definition_item(
            field:    'via_id',
            operator: 'is',
            value:    27
          )
        ]
      end

      it "maps the `condition_all`s' `field` attributes to the correct value" do
        assert_equal 'via_id', json[:all].first[:field].first[:content]
      end

      it "maps the `condition_any`s' `field` attributes to the correct value" do
        assert_equal 'via_id', json[:all].first[:field].first[:content]
      end

      it "maps the `condition_all`s' `value` attributes to the correct value" do
        assert_equal 27, json[:all].first[:value].first[:content]
      end

      it "maps the `condition_any`s' `value` attributes to the correct value" do
        assert_equal 27, json[:all].first[:value].first[:content]
      end
    end
  end
end
