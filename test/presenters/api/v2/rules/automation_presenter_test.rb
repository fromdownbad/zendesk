require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::AutomationPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:automation) { create_automation }

  let(:highlights) { nil }

  let(:includes_collection) { [] }

  let(:presenter) do
    Api::V2::Rules::AutomationPresenter.new(
      user,
      url_builder: mock_url_builder,
      highlights:  highlights,
      includes:    includes_collection
    )
  end

  before do
    @model     = automation
    @presenter = presenter
  end

  should_present_keys :id,
    :title,
    :raw_title,
    :position,
    :active,
    :actions,
    :conditions,
    :updated_at,
    :created_at,
    :url

  should_present_method :id
  should_present_method :title
  should_present_method :position
  should_present_method :is_active?, as:   :active
  should_present_method :actions,    with: Api::V2::Rules::ActionsPresenter
  should_present_method :conditions, with: Api::V2::Rules::ConditionsPresenter
  should_present_method :updated_at
  should_present_method :created_at

  describe '#model_json' do
    let(:json) { presenter.model_json(automation) }

    describe 'when highlights are present' do
      let(:highlights) { {automation.id => '<em>highlight</em>'} }

      it 'presents the highlights' do
        assert_equal '<em>highlight</em>', json[:highlights]
      end
    end

    describe 'when the title contains dynamic content' do
      let(:automation_with_dc) do
        create_automation(title: '{{dc.automation_title}}')
      end

      before do
        account.cms_texts.create!(
          account: account,
          name:    'automation_title',
          fallback_attributes: {
            is_fallback:          'true',
            nested:                true,
            translation_locale_id: 1,
            value:                 'Rendered Title'
          }
        )
      end

      it 'renders the dynamic content in the title' do
        assert_equal(
          'Rendered Title',
          presenter.model_json(automation_with_dc)[:title]
        )
      end

      describe 'and highlights are present' do
        let(:highlights) { {automation_with_dc.id => '<em>dc</em>'} }

        it 'does not present the highlights' do
          assert_nil presenter.model_json(automation_with_dc)[:highlights]
        end
      end
    end

    describe 'when side-loading permissions' do
      let(:includes_collection) { %i[permissions] }

      let(:user) { users(:minimum_agent) }

      describe 'and the user cannot edit the automation' do
        before { user.stubs(:can?).with(:edit, automation).returns(false) }

        it 'sets `can_edit` to false' do
          refute json[:permissions][:can_edit]
        end
      end

      describe 'and the user can edit the automation' do
        before { user.stubs(:can?).with(:edit, automation).returns(true) }

        it 'sets `can_edit` to true' do
          assert json[:permissions][:can_edit]
        end
      end
    end

    describe 'when side-loading usage counts' do
      before do
        Timecop.freeze

        Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

        [30.minutes.ago, 10.hours.ago, 4.days.ago, 29.days.ago].each do |time|
          Timecop.travel(time) do
            record_usage(type: :automation, id: automation.id)
          end
        end
      end

      describe 'and side-loading hourly usage' do
        let(:includes_collection) { %i[usage_1h] }

        describe 'and the hourly usage is stored' do
          before { automation.usage.hourly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_1h]
          end
        end

        describe 'and the hourly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 1, json[:usage_1h]
          end
        end
      end

      describe 'and side-loading daily usage' do
        let(:includes_collection) { %i[usage_24h] }

        describe 'and the daily usage is stored' do
          before { automation.usage.daily = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_24h]
          end
        end

        describe 'and the daily usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 2, json[:usage_24h]
          end
        end
      end

      describe 'and side-loading weekly usage' do
        let(:includes_collection) { %i[usage_7d] }

        describe 'and the weekly usage is stored' do
          before { automation.usage.weekly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_7d]
          end
        end

        describe 'and the weekly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 3, json[:usage_7d]
          end
        end
      end

      describe 'and side-loading monthly usage' do
        let(:includes_collection) { %i[usage_30d] }

        describe 'and the monthly usage is stored' do
          before { automation.usage.monthly = 6 }

          it 'return the stored value' do
            assert_equal 6, json[:usage_30d]
          end
        end

        describe 'and the monthly usage is not stored' do
          it 'returns the rule execution count' do
            assert_equal 4, json[:usage_30d]
          end
        end
      end
    end

    describe 'when side-loading the app installation' do
      let(:includes_collection) { %i[app_installation] }

      before do
        automation.stubs(:app_installation).returns(
          'id'       => 42,
          'settings' => {'title' => 'Test app'}
        )
      end

      it 'side-loads the app installation' do
        assert_equal 42, presenter.model_json(automation)[:app_installation]['id']
      end
    end

    describe 'when presenting a collection' do
      let(:json)       { presenter.present(collection) }
      let(:collection) { Automation.where(account_id: account.id) }

      before do
        Automation.destroy_all

        create_automation(title: 'Notify Requester')
        create_automation(title: 'Notify Assignee')
        create_automation(title: 'Set Priority')
      end

      it 'does not perform N+1 queries' do
        assert_no_n_plus_one { presenter.present(collection) }
      end

      it 'presents a collection' do
        assert_equal(
          [
            'Notify Requester',
            'Notify Assignee',
            'Set Priority'
          ],
          json[:automations].map { |automation| automation[:title] }
        )
      end
    end
  end
end
