require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::ViewDefinitionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:definitions) do
    Zendesk::Rules::Definitions.new(
      account:   account,
      user:      user,
      rule_type: :view
    )
  end

  let(:presenter) do
    Api::V2::Rules::ViewDefinitionsPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = definitions
    @presenter = presenter
  end

  should_present_keys :conditions_all,
    :conditions_any, :output, :groupables, :sortables

  should_present_collection :conditions_all,
    with: Api::V2::Rules::ConditionDefinitionPresenter

  should_present_collection :conditions_any,
    with: Api::V2::Rules::ConditionDefinitionPresenter

  should_present_collection :output,
    with: Api::V2::Rules::ViewOutputDefinitionsPresenter

  should_present_collection :groupables,
    with: Api::V2::Rules::ViewOutputDefinitionsPresenter

  should_present_collection :sortables,
    with: Api::V2::Rules::ViewOutputDefinitionsPresenter
end
