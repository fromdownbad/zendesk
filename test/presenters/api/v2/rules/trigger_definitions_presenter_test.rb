require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::TriggerDefinitionsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:definitions) do
    Zendesk::Rules::Definitions.new(
      account:   account,
      user:      user,
      rule_type: :trigger
    )
  end

  let(:presenter) do
    Api::V2::Rules::TriggerDefinitionsPresenter.new(
      user,
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = definitions
    @presenter = presenter
  end

  should_present_keys :actions,
    :conditions_all,
    :conditions_any

  should_present_collection :actions,
    with: Api::V2::Rules::ActionDefinitionPresenter

  should_present_collection :conditions_all,
    with: Api::V2::Rules::ConditionDefinitionPresenter

  should_present_collection :conditions_any,
    with: Api::V2::Rules::ConditionDefinitionPresenter
end
