require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Rules::FilteredViewCollectionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users

  describe '#total' do
    it 'returns nil' do
      presenter = Api::V2::Rules::FilteredViewCollectionPresenter.new(
        users(:minimum_admin),
        url_builder: mock_url_builder
      )
      refute presenter.total([])
    end
  end
end
