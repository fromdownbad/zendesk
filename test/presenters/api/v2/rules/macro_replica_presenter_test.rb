require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::MacroReplicaPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:model)   { rules(:macro_incident_escalation) }

  let(:presenter) do
    Api::V2::Rules::MacroReplicaPresenter.new(
      users(:minimum_agent),
      url_builder: mock_url_builder
    )
  end

  before do
    @model     = model
    @presenter = presenter
  end

  describe '#model_json' do
    let(:json) { presenter.model_json(model) }

    it 'hides extraneous attributes' do
      assert(
        %i[active created_at id updated_at url].none? do |attribute|
          json.key?(attribute)
        end
      )
    end

    describe 'when there are actions with integer values' do
      before do
        model.actions.find do |action|
          action.source == 'assignee_id'
        end.value = [123]
      end

      it 'converts integer action values to strings' do
        assert_equal(
          '123',
          json[:actions].find do |action|
            action[:field] == 'assignee_id'
          end[:value]
        )
      end
    end
  end

  describe '#url' do
    it 'returns nil' do
      assert_nil presenter.url
    end
  end

  should_present_keys :title,
    :raw_title,
    :description,
    :actions,
    :restriction,
    :position
end
