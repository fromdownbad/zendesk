require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::MobileSdkAppPresenter do
  extend Api::Presentation::TestHelper
  fixtures :mobile_sdk_apps, :mobile_sdk_app_settings, :mobile_sdk_auths, :accounts, :users, :oauth_clients

  before do
    @model = mobile_sdk_apps(:minimum_sdk)
    @settings = @model.settings
    @presenter = Api::V2::MobileSdkAppPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :title, :brand_id, :identifier, :client_identifier, :settings, :mobile_sdk_auth, :url, :signup_source

  should_present_method :title
  should_present_method :identifier
  should_present_method :signup_source

  should_present_url :api_v2_mobile_sdk_app_url, with: :identifier

  describe "#association_preloads" do
    it "is blank" do
      assert_equal({}, @presenter.association_preloads)
    end
  end

  describe_with_and_without_arturo_enabled :mobile_sdk_blip_collection_control do
    it "renders all necessary settings" do
      json = @presenter.model_json(@model)

      assert_settings(@settings, json, mobile_sdk_blip_collection_control: @arturo_enabled)
    end
  end

  describe_with_and_without_arturo_enabled :mobile_sdk_support_never_request_email_setting do
    it "renders all necessary settings" do
      json = @presenter.model_json(@model)

      assert_settings(@settings, json, mobile_sdk_support_never_request_email_setting: @arturo_enabled)
    end
  end

  describe_with_and_without_arturo_enabled :mobile_sdk_help_center_article_voting_enabled do
    it "renders the correct settings" do
      json = @presenter.model_json(@model)

      if @arturo_enabled
        assert_equal @settings.help_center_article_voting_enabled, json[:settings][:help_center_article_voting_enabled]
      else
        refute json[:settings][:help_center_article_voting_enabled], "`help_center_article_voting_enabled` should only be presented when the arturo is enabled"
      end
    end
  end

  describe_with_and_without_arturo_enabled :mobile_sdk_support_show_closed_requests_setting do
    it "renders all necessary settings" do
      json = @presenter.model_json(@model)

      assert_settings(@settings, json, mobile_sdk_support_show_closed_requests_setting: @arturo_enabled)
    end
  end

  describe_with_and_without_arturo_enabled :mobile_sdk_support_show_referrer_logo_setting do
    it "renders all necessary settings" do
      json = @presenter.model_json(@model)

      assert_settings(@settings, json, mobile_sdk_support_show_referrer_logo_setting: @arturo_enabled)
    end
  end

  describe_with_and_without_arturo_enabled :mobile_sdk_answer_bot do
    before do
      stub_request(:get, "https://minimumsdk.zendesk-test.com/api/v2/answer_bot/availability?deflection_channel_id=#{Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK}").
        to_return(status: 200, body: { available: true }.to_json, headers: { 'Content-Type' => 'application/json' })
    end

    it "renders all necessary settings" do
      json = @presenter.model_json(@model)

      assert_settings(@settings, json, mobile_sdk_answer_bot: @arturo_enabled)
    end
  end

  describe_with_and_without_arturo_enabled :mobile_sdk_csat do
    it "renders all necessary settings" do
      json = @presenter.model_json(@model)

      assert_settings(@settings, json, mobile_sdk_csat: @arturo_enabled)
    end
  end

  describe "mobile sdk app" do
    describe "with jwt" do
      before do
        @json = @presenter.model_json(@model)
      end

      it "not nil" do
        assert_not_nil @model.mobile_sdk_auth.endpoint
        assert_equal @model.mobile_sdk_auth.endpoint, @json[:mobile_sdk_auth][:endpoint]
      end
    end

    describe "without jwt" do
      before do
        @model.mobile_sdk_auth.stubs('endpoint').returns('')
        @model.mobile_sdk_auth.stubs('shared_secret').returns('')
        @json = @presenter.model_json(@model)
      end

      it "is blank" do
        assert_equal '', @json[:mobile_sdk_auth][:endpoint]
      end
    end
  end

  def assert_settings(expected, actual, arturo_features = {})
    assert_equal expected.contact_us_tags, actual[:settings][:contact_us_tags]
    assert_equal expected.conversations_enabled, actual[:settings][:conversations_enabled]
    assert_equal expected.rma_enabled, actual[:settings][:rma_enabled]
    assert_equal expected.rma_visits, actual[:settings][:rma_visits]
    assert_equal expected.rma_duration, actual[:settings][:rma_duration]
    assert_equal expected.rma_delay, actual[:settings][:rma_delay]
    assert_equal expected.rma_tags, actual[:settings][:rma_tags]
    assert_equal expected.helpcenter_enabled, actual[:settings][:helpcenter_enabled]
    assert_equal_with_nil expected.rma_android_store_url, actual[:settings][:rma_android_store_url]
    assert_equal_with_nil expected.rma_ios_store_url, actual[:settings][:rma_ios_store_url]
    assert_equal_with_nil expected.authentication, actual[:settings][:authentication]
    assert_equal expected.push_notifications_enabled, actual[:settings][:push_notifications_enabled]
    assert_equal_with_nil expected.push_notifications_callback, actual[:settings][:push_notifications_callback]
    assert_equal_with_nil expected.push_notifications_type, actual[:settings][:push_notifications_type]
    assert_equal_with_nil expected.urban_airship_key, actual[:settings][:urban_airship_key]
    assert_equal_with_nil expected.urban_airship_master_secret, actual[:settings][:urban_airship_master_secret]
    assert_equal expected.connect_enabled, actual[:settings][:connect_enabled]
    assert_equal expected.blips, actual[:settings][:blips]
    assert_equal expected.required_blips, actual[:settings][:required_blips]
    assert_equal expected.behavioural_blips, actual[:settings][:behavioural_blips]
    assert_equal expected.pathfinder_blips, actual[:settings][:pathfinder_blips]

    if arturo_features[:mobile_sdk_blip_collection_control]
      assert_equal expected.collect_only_required_data, actual[:settings][:collect_only_required_data]
    else
      refute actual[:settings].key?(:collect_only_required_data), "the 'collect_only_required_data' setting key was not supposed to be here."
    end

    if arturo_features[:mobile_sdk_support_never_request_email_setting]
      assert_equal expected.support_never_request_email, actual[:settings][:support_never_request_email]
    else
      refute actual[:settings][:support_never_request_email], "the 'support_never_request_email' setting was expected to be false"
    end

    if arturo_features[:mobile_sdk_support_show_closed_requests_setting]
      assert_equal expected.support_show_closed_requests, actual[:settings][:support_show_closed_requests]
    else
      assert actual[:settings][:support_show_closed_requests], "the 'support_show_closed_requests' setting was expected to be true"
    end

    if arturo_features[:mobile_sdk_support_show_referrer_logo_setting]
      assert_equal expected.support_show_closed_requests, actual[:settings][:support_show_referrer_logo]
    else
      assert actual[:settings][:support_show_referrer_logo], "the 'support_show_referrer_logo' setting was expected to be true"
    end

    if arturo_features[:mobile_sdk_answer_bot]
      assert_equal expected.answer_bot, actual[:settings][:answer_bot]
    else
      refute actual[:settings][:answer_bot], "setting 'answer_bot' should be false"
    end

    if arturo_features[:mobile_sdk_csat]
      assert_equal expected.support_show_csat, actual[:settings][:support_show_csat]
    else
      refute actual[:settings][:support_show_csat], "setting 'csat' should be false"
    end
  end
end
