require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::SkipPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :tickets,
    :users

  let(:ticket) { tickets(:minimum_1) }
  let(:agent)  { users(:minimum_agent) }

  let(:json) { @presenter.present(@model) }

  before do
    @account = accounts(:minimum)

    @model = @account.skips.create(
      ticket_id: ticket.id,
      user_id:   agent.id,
      reason:    'Why not?'
    )

    @presenter = Api::V2::SkipPresenter.new(@account.owner,
      url_builder: mock_url_builder)
  end

  should_present_keys :id,
    :ticket_id,
    :user_id,
    :reason,
    :ticket,
    :created_at,
    :updated_at

  should_present_methods :id,
    :user_id,
    :reason,
    :created_at,
    :updated_at

  describe 'when ticket is nil' do
    before { @model.ticket = nil }

    it 'does not have ticket payload' do
      refute json[:skip].key?(:ticket)
    end

    it 'presents `ticket_id` as nil' do
      assert_nil json[:skip][:ticket_id]
    end
  end

  describe 'when ticket is not nil' do
    describe '`ticket` key' do
      let(:presented_value) { stub(:presented_value) }

      before do
        Api::V2::Tickets::TicketPresenter.any_instance.
          stubs(:model_json).
          returns(presented_value)
      end

      it 'presents `ticket` key with `TicketPresenter`' do
        assert_equal presented_value, json[:skip][:ticket]
      end
    end

    it 'presents ticket `nice_id` as `ticket_id`' do
      assert_equal ticket.nice_id, json[:skip][:ticket_id]
    end
  end
end
