require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::CustomStatusPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :custom_statuses

  before do
    @account = accounts(:minimum)
    @model = custom_statuses(:minimum_open)
    @user = users(:minimum_admin)
    @presenter = Api::V2::CustomStatusPresenter.new(
      @user,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :url,
    :id,
    :status_category,
    :agent_label,
    :raw_agent_label,
    :end_user_label,
    :raw_end_user_label,
    :description,
    :raw_description,
    :active,
    :default,
    :created_at,
    :updated_at

  should_present_url :api_v2_custom_status_url

  describe "Dynamic Content" do
    before do
      @dc_cache = {}
      @presenter.stubs(:dc_cache).returns(@dc_cache)
    end

    it "presents translated labels and description" do
      agent_label = "Open"
      raw_agent_label = "{{dc.status_open}}"

      end_user_label = "Open for end-user"
      raw_end_user_label = "{{dc.status_open_end_user}}"

      description = "Description for Open"
      raw_description = "{{dc.status_open_description}}"

      @model.update_attributes(
        agent_label: raw_agent_label,
        end_user_label: raw_end_user_label,
        description: raw_description
      )

      Zendesk::Liquid::DcContext.expects(:render).with(
        raw_agent_label,
        @account,
        @user,
        "text/plain",
        I18n.translation_locale,
        @dc_cache
      ).returns(agent_label)

      Zendesk::Liquid::DcContext.expects(:render).with(
        raw_end_user_label,
        @account,
        @user,
        "text/plain",
        I18n.translation_locale,
        @dc_cache
      ).returns(end_user_label)

      Zendesk::Liquid::DcContext.expects(:render).with(
        raw_description,
        @account,
        @user,
        "text/plain",
        I18n.translation_locale,
        @dc_cache
      ).returns(description)

      json = @presenter.model_json(@model)

      assert_equal agent_label, json[:agent_label]
      assert_equal raw_agent_label, json[:raw_agent_label]

      assert_equal end_user_label, json[:end_user_label]
      assert_equal raw_end_user_label, json[:raw_end_user_label]

      assert_equal description, json[:description]
      assert_equal raw_description, json[:raw_description]
    end
  end
end
