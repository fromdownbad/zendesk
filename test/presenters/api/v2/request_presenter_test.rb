require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::RequestPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @model   = tickets(:minimum_2)
    FieldGroup.any_instance.stubs(is_visible_in_portal?: true)
    @presenter = Api::V2::RequestPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :assignee_id,
    :can_be_solved_by_me,
    :collaborator_ids,
    :created_at,
    :custom_fields,
    :description,
    :due_at,
    :email_cc_ids,
    :fields,
    :followup_source_id,
    :group_id,
    :id,
    :is_public,
    :organization_id,
    :priority,
    :recipient,
    :requester_id,
    :status,
    :subject,
    :type,
    :updated_at,
    :url,
    :via

  should_present_method :nice_id, as: :id
  should_present_method :due_date, as: :due_at
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :organization_id
  should_present_method :requester_id

  should_present_url :api_v2_request_url, with: :nice_id

  it "presents via" do
    via = mock
    @presenter.expects(:serialized_via_object).with(@model).returns(via)
    assert_equal via, @presenter.model_json(@model)[:via]
  end

  should_side_load :users, :organizations, :groups

  describe "with ticket_forms" do
    before do
      Account.any_instance.stubs(:has_ticket_forms?).returns(true)
      setup_custom_fields
      setup_ticket_forms

      @ticket2 = tickets(:minimum_2)
      @ticket2.stubs(:ticket_form).returns(@ticket_form2)
      @ticket2.stubs(:ticket_form_id).returns(@ticket_form2.id)

      @ticket3 = tickets(:minimum_1)
      @ticket3.stubs(:ticket_form).returns(@ticket_form3)
      @ticket3.stubs(:ticket_form_id).returns(@ticket_form3.id)

      @model = @ticket2
    end

    describe "account has ticket forms enabled" do
      it "returns the correct custom fields for the ticket form that is assigned to the ticket being displayed" do
        result = @presenter.present([@ticket2, @ticket3])[:requests]
        assert_equal @ticket2.nice_id, result.first[:id]
        assert_equal @ticket3.nice_id, result.last[:id]
        assert_equal [{id: @ticket_field2.id, value: nil}], result.first[:custom_fields]
        assert_equal [{id: @ticket_field3.id, value: nil}], result.last[:custom_fields]
      end

      it "presents ticket_form_id when the form is end user visible" do
        assert_equal @model.ticket_form_id, @presenter.model_json(@model)[:ticket_form_id]
      end

      it "presents a nil ticket_form_id when the ticket does not have a form" do
        @model.stubs(:ticket_form_id).returns(nil)
        assert_nil @presenter.model_json(@model)[:ticket_form_id]
      end

      it "does not present ticket_form_id when the form is not end user visible" do
        @ticket_form2.stubs(:end_user_visible).returns(false)
        refute @presenter.model_json(@model).key?('ticket_form_id')
      end
    end

    describe "account does not have ticket forms enabled" do
      before do
        Account.any_instance.expects(:has_ticket_forms?).returns(false)

        ticket_fields = @account.ticket_fields.custom.active.visible_in_portal
        @custom_fields = @presenter.ticket_field_entries(@ticket2, ticket_fields)
      end

      it "does not present ticket_form_id" do
        refute @presenter.model_json(@model).key?('ticket_form_id')
      end

      it "returns all end-user visable ticket_fields" do
        result = @presenter.present(@ticket2)[:request]

        assert_equal @ticket_form2.id, @ticket2.ticket_form_id
        assert_equal @custom_fields, result[:custom_fields]
      end
    end
  end

  describe "side-loads" do
    subject { @presenter.present(@model) }
    before do
      @presenter.stubs(:side_load?).returns(true)
    end

    describe "users" do
      let!(:end_user_collaborator) do
        Collaboration.create!(
          user: users(:minimum_end_user2),
          account: @account,
          ticket: @model
        )
      end
      let!(:agent_collaborator) do
        Collaboration.create!(
          user: users(:minimum_agent),
          account: @account,
          ticket: @model
        )
      end
      let(:side_load_user_ids) do
        subject[:users].map { |user| user[:id] }
      end

      it "presents requester" do
        assert_equal subject[:request][:requester_id], @model.requester_id
        assert_includes side_load_user_ids, @model.requester_id
      end

      it "presents collaborators" do
        assert_equal subject[:request][:collaborator_ids].length, 2
        assert_includes side_load_user_ids, end_user_collaborator.user_id
        assert_includes side_load_user_ids, agent_collaborator.user_id
      end

      describe "email ccs" do
        let(:account) { accounts(:minimum) }
        let(:user) { users(:minimum_agent) }

        it "presents collaborators as email_ccs" do
          assert_equal subject[:request][:email_cc_ids].length, 2
          assert_includes side_load_user_ids, end_user_collaborator.user_id
          assert_includes side_load_user_ids, agent_collaborator.user_id
        end

        describe "with follower_and_email_cc_collaborations setting enabled" do
          before do
            Account.any_instance.stubs(:has_follower_and_email_cc_collaborations_enabled?).returns(true)
          end

          describe "when comment_email_ccs_allowed setting is enabled" do
            before do
              Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
            end

            it "presents email_ccs" do
              assert_equal subject[:request][:email_cc_ids].length, 1
              assert_includes side_load_user_ids, end_user_collaborator.user_id
            end
          end

          describe "when comment_email_ccs_allowed setting is disabled" do
            before do
              Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
            end

            it "presents empty email_ccs" do
              assert_empty subject[:request][:email_cc_ids]
            end
          end
        end
      end
    end

    it "presents organizations" do
      assert_not_nil @model.organization_id
      assert_not_nil subject[:organizations].detect { |org| org[:id] == @model.organization_id }
    end

    it "excludes deleted organizations" do
      @model.organization.update_column(:deleted_at, Time.now)
      assert_empty subject[:organizations]
    end

    it "presents group" do
      assert_not_nil @model.group_id
      assert subject[:groups].first[:id] == @model.group_id
    end

    it "excludes deleted groups" do
      @model.group.update_column(:is_active, false)
      assert_empty subject[:groups]
    end

    it "presents requester organization" do
      assert_not_nil @model.requester.organization_id
      assert_not_nil subject[:organizations].detect { |org| org[:id] == @model.requester.organization_id }
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      it "side-loads satisfaction_ratings" do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:satisfaction_ratings).returns(true)

        tickets = [stub]
        side_loaded_ratings = [stub]

        @presenter.expects(:side_load_association).with(tickets, :satisfaction_ratings, @presenter.satisfaction_rating_presenter).returns(side_loaded_ratings)

        assert_equal side_loaded_ratings, @presenter.side_loads(tickets)[:satisfaction_ratings]
      end
    end

    it "side-loads public_updated_at" do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:public_updated_at).returns(true)
      assert_not_nil @presenter.model_json(@model)[:public_updated_at]
    end

    it "side-loads followup sources" do
      @presenter.preload_associations(@model)
      assert @model.followup_source_links.loaded?
      @model.followup_source_links.each do |source_link|
        assert source_link.source.loaded?
        assert source_link.archived_source.loaded?
      end
    end
  end

  it "presents the full subject when the subject field is visible" do
    FieldSubject.any_instance.expects(:is_visible_in_portal?).returns(true)
    @model.stubs(:subject).returns("a lengthy, redundant, lengthy subject!")

    assert_equal 'a lengthy, redundant, lengthy subject!', @presenter.model_json(@model)[:subject]
  end

  it "renders liquid in the subject if the submitter is an agent" do
    FieldSubject.any_instance.expects(:is_visible_in_portal?).returns(true)
    @model.stubs(:subject).returns("Hello {{ticket.requester.name}}!")
    @model.stubs(:submitter).returns(users(:minimum_agent))
    @model.stubs(:requester).returns(users(:minimum_end_user))

    assert_equal 'Hello minimum_end_user!', @presenter.model_json(@model)[:subject]
  end

  it "presents the truncated description as the subject when the subject field is not visible" do
    FieldSubject.any_instance.expects(:is_visible_in_portal?).returns(false)
    @model.stubs(:public_description).returns("a lengthy, redundant, lengthy description!")

    assert_equal 'a lengthy, redundant, lengt...', @presenter.model_json(@model)[:subject]
  end

  it "does not present description when it is not public" do
    @model.comments.update_all(is_public: false)
    assert_equal '', @presenter.model_json(@model)[:description]
  end

  it "presents status" do
    @model.status_id = nil
    assert_nil @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.NEW
    assert_equal 'new', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.OPEN
    assert_equal 'open', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.PENDING
    assert_equal 'pending', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.HOLD
    assert_equal 'hold', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.SOLVED
    assert_equal 'solved', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.CLOSED
    assert_equal 'closed', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.DELETED
    assert_equal 'deleted', @presenter.model_json(@model)[:status]
  end

  it "presents priority" do
    test_cases = {
      nil => nil,
      PriorityType.LOW => 'low',
      PriorityType.NORMAL => 'normal',
      PriorityType.HIGH => 'high',
      PriorityType.URGENT => 'urgent'
    }

    test_cases.each do |actual_value, expected_json|
      @model.priority_id = actual_value
      assert_equal_with_nil expected_json, @presenter.model_json(@model)[:priority]
    end
  end

  it "presents type" do
    test_cases = {
      nil                 => nil,
      TicketType.QUESTION => 'question',
      TicketType.INCIDENT => 'incident',
      TicketType.PROBLEM  => 'problem',
      TicketType.TASK     => 'task',
    }

    test_cases.each do |actual_value, expected_json|
      @model.ticket_type_id = actual_value
      assert_equal_with_nil expected_json, @presenter.model_json(@model)[:type]
    end
  end

  describe "followup_source_id" do
    let(:source) do
      source = Minitest::Mock.new
      source.expect(:try, 42, [:nice_id])
    end

    let(:source_link) do
      source_link = Minitest::Mock.new
      source_link.expect(:try, source, [:source_with_archived])
    end

    it "is nil when ticket is not a followup" do
      @model.stubs(:followup_source_links).returns([])
      assert_nil @presenter.model_json(@model)[:followup_source_id]
    end

    it "is presented when ticket is a followup" do
      @model.stubs(:followup_source_links).returns([source_link])
      assert_equal 42, @presenter.model_json(@model)[:followup_source_id]
    end
  end

  describe "can_be_solved_by_me" do
    before do
      @model.stubs(:assignee_id).returns(123)
      @model.stubs(:working?).returns(true)
      @model.stubs(:ticket_type?).with('problem').returns(false)
      @model.stubs(:requested_by?).with(@account.owner).returns(true)
    end

    it "is true when assignee_id present, ticket is working?, ticket_type not problem, requested by current user" do
      assert @presenter.model_json(@model)[:can_be_solved_by_me]
    end

    it "is false if assignee_id is nil" do
      @model.stubs(:assignee_id).returns(nil)

      refute @presenter.model_json(@model)[:can_be_solved_by_me]
    end

    it "is false if request is not working? state" do
      @model.stubs(:working?).returns(false)

      refute @presenter.model_json(@model)[:can_be_solved_by_me]
    end

    it "is false if request type is problem" do
      @model.stubs(:ticket_type?).with('problem').returns(true)

      refute @presenter.model_json(@model)[:can_be_solved_by_me]
    end

    it "is false if request is not by the current_user" do
      @model.stubs(:requested_by?).with(@account.owner).returns(false)

      refute @presenter.model_json(@model)[:can_be_solved_by_me]
    end
  end

  describe "ticket fields" do
    before do
      @fields = @presenter.model_json(@model)[:custom_fields]
    end

    it "presents all ticket fields" do
      assert_equal @account.ticket_fields.custom.active.visible_in_portal.count(:all), @fields.size
    end

    it "presents field entries" do
      assert @model.ticket_field_entries.any?

      @model.ticket_field_entries.each do |entry|
        assert_includes @fields, id: entry.ticket_field_id, value: entry.value
      end
    end

    describe "with custom fields" do
      describe "with no ticket_forms" do
        before do
          @ticket_field = ticket_fields(:field_tagger_custom)
          @custom_field_option = custom_field_options(:field_tagger_custom_option_1)

          @model.ticket_field_entries.build(ticket_field: @ticket_field, value: @custom_field_option.value)
          @fields = @presenter.model_json(@model)[:custom_fields]
        end

        it "presents option value" do
          assert_equal @custom_field_option.value, @fields.detect { |field| field[:id] == @ticket_field.id }[:value]
        end
      end
    end

    describe "when FieldAssignee is either not active or not visible in portal" do
      before do
        @presenter.instance_variable_set(:@assignee, false)
      end

      should_present_keys :id,
        :status,
        :priority,
        :type,
        :description,
        :created_at,
        :updated_at,
        :group_id,
        :fields,
        :organization_id,
        :via,
        :subject,
        :url,
        :custom_fields,
        :requester_id,
        :collaborator_ids,
        :email_cc_ids,
        :due_at,
        :can_be_solved_by_me,
        :recipient,
        :followup_source_id,
        :is_public
    end

    describe "when FieldAssignee is active and visible in portal" do
      before do
        @presenter.instance_variable_set(:@assignee, true)
      end

      should_present_keys :id,
        :status,
        :priority,
        :type,
        :description,
        :created_at,
        :updated_at,
        :assignee_id,
        :group_id,
        :fields,
        :organization_id,
        :via,
        :subject,
        :url,
        :custom_fields,
        :requester_id,
        :collaborator_ids,
        :email_cc_ids,
        :due_at,
        :can_be_solved_by_me,
        :recipient,
        :followup_source_id,
        :is_public

      describe "side loading" do
        subject { @presenter.present(@model) }
        before { @presenter.stubs(:side_load?).returns(true) }

        before do
          @model.assignee = @agent = users(:minimum_agent)
          @model.will_be_saved_by(@agent)
          @model.save!
        end

        it "sides load assignee" do
          assert_not_nil subject[:users].detect { |user| user[:id] == @model.assignee_id }
        end

        it "sides load requester" do
          assert_not_nil subject[:users].detect { |user| user[:id] == @model.requester_id }
        end

        it "sides load organization" do
          assert_not_nil subject[:organizations].detect { |org| org[:id] == @model.requester.organization_id }
        end
      end
    end

    describe "with boolean fields" do
      before do
        @ticket_field = ticket_fields(:field_checkbox_custom)
        @model.ticket_field_entries.build(ticket_field: @ticket_field, value: "1")
        @fields = @presenter.model_json(@model)[:custom_fields]
      end

      it "presents true/false" do
        assert(@fields.detect { |field| field[:id] == @ticket_field.id }[:value])
      end
    end
  end

  def setup_ticket_forms
    params2 = { ticket_form: { name: "Wombat", display_name: "Wombats are everywhere", position: 2,
                               default: false, active: true, end_user_visible: true, in_all_brands: true, ticket_field_ids: [@ticket_field2.id, @ticket_field_not_visible.id] } }

    @ticket_form2 = Zendesk::TicketForms::Initializer.new(@account, params2).ticket_form
    @ticket_form2.save!

    params3 = { ticket_form: { name: "Giraffes", display_name: "Giraffes are everywhere", position: 3,
                               default: false, active: true, end_user_visible: true, in_all_brands: true, ticket_field_ids: [@ticket_field3.id] } }
    @ticket_form3 = Zendesk::TicketForms::Initializer.new(@account, params3).ticket_form
    @ticket_form3.save!
  end

  def setup_custom_fields
    @ticket_field2 = FieldText.new(title: 'Wombat custom text field', title_in_portal: 'Wombat custom text field', is_visible_in_portal: true)
    @ticket_field2.account = @account
    @ticket_field2.save

    @ticket_field3 = FieldText.new(title: 'Giraffe custom text field', title_in_portal: 'Giraffe custom text field', is_visible_in_portal: true)
    @ticket_field3.account = @account
    @ticket_field3.save

    @ticket_field_not_visible = FieldText.new(title: 'not visible custom text field', is_visible_in_portal: false)
    @ticket_field_not_visible.account = @account
    @ticket_field_not_visible.save
  end
end
