require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Api::V2::FeatureUsageMetricsPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::UsageHelper

  fixtures :all

  def result
    @already_done_once = true
    account.reload if @already_done_once # avoid association caching for rails 3

    @presenter.model_json(account)
  end

  def normalize_time(time)
    # workaround updated_at having non-integer underlying data
    Time.at(time.to_i)
  end

  let(:account) { accounts(:minimum) }

  before do
    Timecop.travel 1.year.from_now # ignore seed data that affects metrics

    @model     = account
    @presenter = Api::V2::FeatureUsageMetricsPresenter.new(
      account.owner,
      url_builder: mock_url_builder
    )
  end

  should_present_keys :automations,
    :macros,
    :triggers,
    :views

  describe '#count' do
    %i[automation trigger view].each do |rule_type|
      rule_type_plural = :"#{rule_type}s"

      describe "when the rule type is #{rule_type}" do
        let(:active_rules) { account.send(rule_type_plural).active.to_a }

        before { active_rules.last.update_attribute(:is_active, false) }

        it "shows active #{rule_type} count" do
          assert_equal result[rule_type_plural][:count], active_rules.size - 1
        end
      end
    end

    describe 'when the rule type is macro' do
      let(:active_rules) { account.shared_macros.active.to_a }

      before { active_rules.last.update_attribute(:is_active, false) }

      it 'shows active shared macros count' do
        assert_equal result[:macros][:count], active_rules.size - 1
      end
    end
  end

  describe '#used_past_day_count' do
    %i[automation macro trigger].each do |rule_type|
      rule_type_plural = :"#{rule_type}s"

      describe "when the rule type is #{rule_type}" do
        let(:ticket) { FactoryBot.create(:ticket, account: account) }

        let(:rule_a) { FactoryBot.create(rule_type, owner: account) }
        let(:rule_b) { FactoryBot.create(rule_type, owner: account) }

        describe 'and none are used in the past day' do
          before do
            Timecop.travel(30.hours.ago) do
              record_usage(type: rule_type, id: rule_a.id)
              record_usage(type: rule_type, id: rule_b.id)
            end
          end

          it 'returns zero' do
            assert_equal 0, result[rule_type_plural][:used_past_day_count]
          end
        end

        describe 'and rules are used in the past day' do
          before do
            Timecop.travel(22.hours.ago) do
              record_usage(type: rule_type, id: rule_a.id, count: 5)
            end
          end

          it 'shows the number used in the past day' do
            assert_equal 1, result[rule_type_plural][:used_past_day_count]
          end
        end
      end
    end

    describe 'when the rule type is view' do
      it 'does not include daily usage' do
        refute result[:views].include?(:used_past_day_count)
      end
    end
  end

  describe '#most_used_past_week' do
    before { Rule.destroy_all }

    %i[automation macro trigger].each do |rule_type|
      rule_type_plural = :"#{rule_type}s"

      describe "when the rule type is #{rule_type}" do
        let(:ticket) { FactoryBot.create(:ticket, account: account) }

        let(:rule_a) { FactoryBot.create(rule_type, owner: account) }
        let(:rule_b) { FactoryBot.create(rule_type, owner: account) }

        describe 'and none have been used in the past week' do
          before do
            Timecop.travel(8.days.ago) do
              record_usage(type: rule_type, id: rule_a.id)
              record_usage(type: rule_type, id: rule_b.id)
            end
          end

          it 'shows only those used in the past week' do
            assert_empty(result[rule_type_plural][:most_used_past_week])
          end
        end

        describe "and #{rule_type_plural} have been used in the past week" do
          before do
            Timecop.travel(4.days.ago) do
              record_usage(type: rule_type, id: rule_a.id)
            end
          end

          it 'shows only those used in the past week' do
            assert_equal(
              [
                {
                  id:                        rule_a.id,
                  title:                     rule_a.title,
                  execution_count_past_week: 1,
                  updated_at:                normalize_time(rule_a.updated_at)
                }
              ],
              result[rule_type_plural][:most_used_past_week]
            )
          end
        end

        describe 'and none have been used in the past week' do
          before do
            Timecop.travel(26.hours.ago) do
              record_usage(type: rule_type, id: rule_a.id)
              record_usage(type: rule_type, id: rule_b.id)
            end
          end
        end
      end
    end

    describe 'when the rule type is view' do
      it 'does not include weekly usage' do
        refute result[:views].include?(:most_used_past_week)
      end
    end
  end

  %i[automation macro trigger view].each do |rule_type|
    rule_type_plural = :"#{rule_type}s"

    describe '#updated_past_day_count' do
      before { Rule.destroy_all }

      describe "when the rule type is #{rule_type}" do
        describe 'and none have been updated in the past day' do
          before do
            Timecop.travel(26.hours.ago) do
              FactoryBot.create(rule_type, owner: account)
            end
          end

          it 'returns zero' do
            assert_equal 0, result[rule_type_plural][:updated_past_day_count]
          end
        end

        describe 'and they have been created in the past day' do
          before do
            Timecop.travel(22.hours.ago) do
              FactoryBot.create(rule_type, owner: account)
            end
          end

          it "counts the #{rule_type_plural} created in the past day" do
            assert_equal 1, result[rule_type_plural][:updated_past_day_count]
          end
        end

        describe 'and they have been updated in the past day' do
          let(:rule) { FactoryBot.create(rule_type, owner: account) }

          before do
            Timecop.travel(26.hours.ago) { rule }

            rule.update_attribute(:title, 'New title')
          end

          it "counts the #{rule_type_plural} updated in the past day" do
            assert_equal 1, result[rule_type_plural][:updated_past_day_count]
          end
        end
      end
    end

    describe '#updated_recently' do
      describe "when the rule type is #{rule_type}" do
        let(:old_rule) { FactoryBot.create(rule_type, owner: account) }

        before do
          6.times do |i|
            Timecop.travel(i.month.ago) do
              FactoryBot.create(
                rule_type,
                owner: account,
                title: "#{i}-month-old"
              )
            end
          end

          Timecop.travel(15.years.ago) { old_rule }

          Timecop.travel(1.day.ago) do
            old_rule.update_attribute(:title, '1-day-old')
          end
        end

        it 'shows only the five most recently updated' do
          assert_equal 5, result[rule_type_plural][:updated_recently].count
        end

        it 'sorts by updated_at' do
          assert_equal(
            %w[
              0-month-old
              1-day-old
              1-month-old
              2-month-old
              3-month-old
            ],
            result[rule_type_plural][:updated_recently].
              map { |rule| rule[:title] }
          )
        end
      end
    end
  end
end
