require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::BrandPresenter do
  it "builds AgentBrandPresenter for agents" do
    presenter = Api::V2::BrandPresenter.new(User.new { |u| u.roles = Role::AGENT.id }, url_builder: "")
    assert_equal Api::V2::AgentBrandPresenter, presenter.class
  end

  it "builds AdminBrandPresenter for admins" do
    presenter = Api::V2::BrandPresenter.new(User.new { |u| u.roles = Role::ADMIN.id }, url_builder: "")
    assert_equal Api::V2::AdminBrandPresenter, presenter.class
  end

  it "builds AdminBrandPresenter for system users" do
    presenter = Api::V2::BrandPresenter.new(User.system, url_builder: "")
    assert_equal Api::V2::AdminBrandPresenter, presenter.class
  end
end
