require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Abilities::OrganizationPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint

  fixtures :users, :accounts, :organizations

  before do
    @model = organizations(:minimum_organization1)
    @actor = users(:minimum_admin)
    @presenter = Api::V2::Abilities::OrganizationPresenter.new(@actor, url_builder: mock_url_builder)
  end

  should_pass_lint_tests

  should_present_keys :organization_id, :url, :can_edit_users, :can_access_tickets

  it "has abilities as the root key" do
    assert_equal :abilities, @presenter.model_key
  end

  it "presents organization url" do
    assert_equal :api_v2_organization_url, @presenter.url(@model)
  end

  describe "presented abilities" do
    before do
      @presenter.expects("can_edit_users?").returns("a-value")
      @presenter.expects("can_access_tickets?").returns("a-value")
      @output = @presenter.model_json(@model)
    end

    it "displays can_edit_users correctly" do
      assert_equal "a-value", @output[:can_edit_users]
    end

    it "displays can_access_tickets correctly" do
      assert_equal "a-value", @output[:can_access_tickets]
    end
  end
end
