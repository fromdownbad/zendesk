require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Abilities::UserPresenter do
  extend Api::Presentation::TestHelper

  presented_abilities = {
    edit: :can_edit,
    edit_password: :can_edit_password,
    manage_identities_of: :can_manage_identities_of,
    manually_verify_identities_of: :can_verify_identities,
    request_password_reset: :can_reset_password,
    change_password: :can_change_password,
    set_password: :can_set_password,
    create_password: :can_create_password,
    set_agent_display_name: :can_set_alias,
    send_verification_email: :can_send_verification_email,
    verify_now: :can_verify_now,
    edit_agent_forwarding: :can_edit_agent_forwarding,
    modify_user_tags: :can_modify_user_tags,
    delete: :can_delete,
    assume: :can_assume,
    manage_people: :can_manage_people
  }

  fixtures :users, :accounts, :organizations, :role_settings

  before do
    @model = users(:minimum_agent)
    @actor = users(:minimum_admin)
    @presenter = Api::V2::Abilities::UserPresenter.new(@actor, url_builder: mock_url_builder)
  end

  should_present_keys(:user_id, :url, *([:can_make_comment_private, :can_view_views, :can_view_reports, :can_export, :can_use_voice_console, :voice_enabled_account, :can_use_voice, :can_view_voice_dashboard] + presented_abilities.values))

  it "has abilities as the root key" do
    assert_equal :abilities, @presenter.model_key
  end

  it "presents user url" do
    assert_equal :api_v2_user_url, @presenter.url(@model)
  end

  describe "presented abilities" do
    before do
      @output = @presenter.model_json(@model)
    end

    it "displays :can_make_private correctly" do
      assert_equal @actor.can?(:make_comment_private, Comment), @output[:can_make_comment_private]
    end

    it "displays :can_view_views correctly" do
      assert_equal @model.can?(:view, View), @output[:can_view_views]
    end

    it "displays :can_view_reports correctly" do
      assert_equal @actor.can?(:view, Report), @output[:can_view_reports]
    end

    describe 'can_export' do
      before do
        @export_configuration = stub
        Zendesk::Export::Configuration.stubs(:new).returns(@export_configuration)
      end

      describe 'when exports are not available' do
        before do
          @export_configuration.expects(:any_accessible?).returns(false)
          @output = @presenter.model_json(@model)
        end

        it 'is false' do
          refute @output[:can_export]
        end
      end

      describe 'when exports are available' do
        before do
          @export_configuration.stubs(:whitelisted?).returns(true)
          @export_configuration.expects(:any_accessible?).returns(true)
          @output = @presenter.model_json(@model)
        end

        it 'is true' do
          assert @output[:can_export]
        end
      end

      describe 'when the user is not on the whitelisted domain' do
        before do
          @export_configuration.stubs(:any_accessible?).returns(true)
          @export_configuration.expects(:whitelisted?).returns(false)
          @output = @presenter.model_json(@model)
        end

        it 'is false' do
          refute @output[:can_export]
        end
      end

      describe 'when the user is on the whitelisetd domain' do
        before do
          @export_configuration.stubs(:any_accessible?).returns(true)
          @export_configuration.expects(:whitelisted?).returns(true)
          @output = @presenter.model_json(@model)
        end

        it 'is true' do
          assert @output[:can_export]
        end
      end
    end

    describe 'can_use_voice_console' do
      describe 'when the account does not have a VoiceAccount' do
        before do
          @model.account.stubs(:voice_enabled?).returns(false)
          @output = @presenter.model_json(@model)
        end

        it 'is false' do
          refute @presenter.model_json(@model)[:can_use_voice_console]
        end
      end

      describe 'when the account has a VoiceAccount' do
        before { @model.account.stubs(:voice_enabled?).returns(true) }

        describe 'when the user does not have a voice subscription' do
          before do
            Voice::FeatureMapper.any_instance.stubs(:feature_enabled?).
              with(:user_seats).returns(false)
            @output = @presenter.model_json(@model)
          end

          it 'displays :can_use_voice_console correctly' do
            assert_equal @model.can?(:accept, Voice::Call), @output[:can_use_voice_console]
          end
        end

        describe 'when the user has a voice subscription' do
          before do
            Voice::FeatureMapper.any_instance.stubs(:feature_enabled?).
              with(:user_seats).returns(true)
          end

          describe 'when the user has a voice seat' do
            before do
              FactoryBot.create(:voice_user_seat, user: @model, account: @model.account)
            end

            it 'is true' do
              assert @presenter.model_json(@model)[:can_use_voice_console]
            end
          end

          describe 'when the user does not have a voice seat' do
            it 'is false' do
              refute @presenter.model_json(@model)[:can_use_voice_console]
            end
          end
        end
      end
    end

    presented_abilities.each do |ability, key|
      before_should "defer :#{ability} to User#ability'" do
        @actor.stubs(:can?)
        @actor.expects(:can?).with(ability, @model).returns('woof')
      end

      it "displays :#{key} correctly" do
        assert_equal @actor.send(:can?, ability, @model), @output[key]
      end
    end
  end
end
