require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SharingAgreementPresenter do
  fixtures :accounts
  extend Api::Presentation::TestHelper

  before do
    @model = FactoryBot.build(:agreement)
    @presenter = Api::V2::SharingAgreementPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :type, :status, :partner_name, :created_at, :remote_subdomain

  should_present_method :id
  should_present_method :name
  should_present_method :created_at

  describe "should present shared_with_id" do
    before do
      @model.stubs(shared_with_id: subject)
      @json = @presenter.model_json(@model)
    end

    describe "== 1" do
      subject { 1 }

      it "presents jira" do
        assert_equal "jira", @json[:partner_name]
      end
    end

    describe "== anything else" do
      subject { "anything else" }

      it "presents nil" do
        assert_nil @json[:partner_name]
      end
    end
  end

  describe 'with remote_accounts set' do
    before do
      @presenter.remote_accounts = { @model.remote_url => subject }
      @json = @presenter.model_json(@model)
    end
  end

  describe "status mapping" do
    Sharing::Agreement::STATUS_MAP.each do |string, id|
      describe "mapping #{id}" do
        before do
          @model.status_id = id
        end

        it "maps to #{string}" do
          assert_equal string.to_s, @presenter.model_json(@model)[:status]
        end
      end
    end
  end

  { true => "inbound", false => "outbound" }.each do |inbound, string|
    describe "mapping inbound => #{inbound}" do
      before do
        @model.stubs(in?: inbound)
      end

      it "presents type as #{string}" do
        assert_equal string, @presenter.model_json(@model)[:type]
      end
    end
  end

  describe "remote account" do
    before do
      @model.stubs(remote_account: subject)
      @json = @presenter.model_json(@model)
    end

    describe "when there is no remote account" do
      subject { nil }

      it "presents nil" do
        assert_nil @json[:remote_subdomain]
      end
    end

    describe "when there is a remote account" do
      subject { accounts(:minimum) }

      it "presents the subdomain" do
        assert_equal subject.subdomain, @json[:remote_subdomain]
      end
    end
  end
end
