require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::RuleCountPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :rules

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Internal::RuleCountPresenter.new(@model.owner, url_builder: mock_url_builder)
  end

  should_present_keys :views, :triggers, :macros, :automations

  it "returns the correct count" do
    result = @presenter.model_json(@account).with_indifferent_access

    assert_equal @account.all_views.count(:all), result["views"]["total"]
    assert_equal @account.all_macros.count(:all), result["macros"]["total"]
  end
end
