require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::RecipientAddressPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :users, :recipient_addresses

  before do
    @model     = recipient_addresses(:default)
    @presenter = Api::V2::Internal::RecipientAddressPresenter.new(users(:systemuser), url_builder: mock_url_builder, includes: [:brands])
  end

  should_present_keys :brand_id,
    :cname_status,
    :created_at,
    :default,
    :domain_verification_code,
    :domain_verification_status,
    :email,
    :external_email_credential,
    :external_email_credential_id,
    :forwarding_status,
    :id,
    :metadata,
    :name,
    :spf_status,
    :updated_at,
    :url
end
