require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::VoiceBurnRatePresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:burn_rate) do
    {
      units: 0,
      created_at: Time.zone.now.to_date
    }
  end

  let(:content) do
    {
      units: burn_rate[:units],
      date: burn_rate[:created_at]
    }
  end

  let(:account) { accounts(:support) }

  let(:presenter) do
    Api::V2::Internal::VoiceBurnRatePresenter.new(account.owner, url_builder: mock_url_builder)
  end

  it 'presents the burn rate' do
    presenter.present(burn_rate)[:burn_rate].must_equal(content)
  end
end
