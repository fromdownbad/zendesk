require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AddonBoostPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:support) }

  let(:presenter) do
    Api::V2::Internal::AddonBoostPresenter.new(account.owner, url_builder: mock_url_builder)
  end

  describe 'presents normal addon' do
    let!(:addon_boost) { FactoryBot.build(:addon_boost) }

    let!(:content) do
      {
        id:               addon_boost.id,
        feature_name:     addon_boost.name,
        readable_name:    'Data center',
        boost_expires_at: addon_boost.boost_expires_at,
        url:              :api_v2_internal_addon_boosts_url
      }
    end

    it 'translates addon name' do
      presenter.present(addon_boost)[:addon_boost].must_equal(content)
    end
  end

  describe "presents api addon" do
    let!(:addon_boost) { FactoryBot.build(:addon_boost_high_volume_api) }

    let!(:content) do
      {
        id:               addon_boost.id,
        feature_name:     addon_boost.name,
        readable_name:    'High volume API',
        boost_expires_at: addon_boost.boost_expires_at,
        url:              :api_v2_internal_addon_boosts_url
      }
    end

    it 'translates api' do
      presenter.present(addon_boost)[:addon_boost].must_equal(content)
    end
  end

  describe 'presents NPS' do
    let!(:addon_boost) { FactoryBot.build(:addon_boost_nps) }

    let!(:content) do
      {
        id:               addon_boost.id,
        feature_name:     addon_boost.name,
        readable_name:    'NPS',
        boost_expires_at: addon_boost.boost_expires_at,
        url:              :api_v2_internal_addon_boosts_url
      }
    end

    it 'translates nps' do
      presenter.present(addon_boost)[:addon_boost].must_equal(content)
    end
  end
end
