require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ComplianceAgreementPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:compliance) { Compliance.create!(name: 'HIPAA') }

  before do
    @model = account.compliance_agreements.create! do |ca|
      ca.compliance = compliance
    end
    @presenter = Api::V2::Internal::ComplianceAgreementPresenter.new(@model, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :url

  should_present_method :id

  should_present "HIPAA", as: :name
end
