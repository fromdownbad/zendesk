require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::AppLinkPresenter do
  extend Api::Presentation::TestHelper
  class AppLinkURLFakeBuilder
    def self.api_v2_internal_app_link_url(param = nil, _)
      ['https://foo.com/api/v2/internal/app_links', param.try(:id)].compact.join('/')
    end
  end

  fixtures :app_links

  before do
    @account   = accounts(:minimum)
    @model     = app_links(:scarlett)
    @presenter = Api::V2::Internal::AppLinkPresenter.new(@account.owner, url_builder: AppLinkURLFakeBuilder)
  end

  should_present_keys :id, :app_id, :package_name, :paths, :fingerprints, :url

  it 'builds urls via model_key' do
    assert_equal "https://foo.com/api/v2/internal/app_links/#{@model.id}", @presenter.url(@model)
  end

  it 'builds urls via model_key with singular resource' do
    @presenter.stubs(singular_resource: true)
    assert_equal 'https://foo.com/api/v2/internal/app_links', @presenter.url(@model)
  end
end
