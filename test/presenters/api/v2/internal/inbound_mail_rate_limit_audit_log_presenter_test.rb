require_relative '../../../../support/test_helper'
require_relative '../../../../support/inbound_mail_rate_limit_test_helper'

SingleCov.covered!

describe Api::V2::Internal::InboundMailRateLimitAuditLogPresenter do
  extend Api::Presentation::TestHelper
  include InboundMailRateLimitTestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:audit_message) { { email: 'akim@zendesk.com', name: 'Amy Kim' }.to_json }

  before do
    rate_limit = create_inbound_mail_rate_limit(is_sender: true, rate_limit: 10)

    @model = CIA.audit(actor: users(:minimum_end_user), ip_address: '168.192.0.1') do
      rate_limit.audit_message = audit_message
      CIA.record(:create, rate_limit)
    end

    @presenter = Api::V2::Internal::InboundMailRateLimitAuditLogPresenter.new(
      users(:minimum_end_user),
      url_builder: mock_url_builder,
      includes: []
    )
  end

  should_present_keys :action,
    :actor_id,
    :action_label,
    :change_description,
    :created_at,
    :id,
    :ip_address,
    :message,
    :source_id,
    :source_label,
    :source_type

  describe 'when event has an invalid message' do
    let(:audit_message) { nil }

    it 'returns an empty string for the message' do
      assert_equal '', @presenter.present(@model)[:audit_log][:message]
    end
  end
end
