require_relative '../../../../support/test_helper'
require_relative '../../../../support/inbound_mail_rate_limit_test_helper'

SingleCov.covered!

describe Api::V2::Internal::InboundMailRateLimitPresenter do
  extend Api::Presentation::TestHelper
  include InboundMailRateLimitTestHelper

  fixtures :accounts
  let(:account) { accounts(:minimum) }

  before do
    @model = create_inbound_mail_rate_limit(is_sender: true, rate_limit: 10)
    @presenter = Api::V2::Internal::InboundMailRateLimitPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder, includes: [])
  end

  should_present_keys :account_id,
    :created_at,
    :deleted_at,
    :description,
    :email,
    :id,
    :is_active,
    :is_sender,
    :rate_limit,
    :ticket_id,
    :updated_at,
    :url
end
