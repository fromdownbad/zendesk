require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AccountSecuritySettingsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users, :remote_authentications, :account_settings

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Internal::AccountSecuritySettingsPresenter.new(@model.owner, url_builder: mock_url_builder)
  end

  should_present_keys :remote_auth,
    :password_reset_override,
    :captcha_required,
    :x_frame_header,
    :security_headers,
    :email_agent_when_sensitive_fields_changed,
    :xss_header,
    :csp_header,
    :content_type_header,
    :ssl_enabled,
    :spam_processor,
    :spam_threshold_multiplier,
    :end_user_comment_rate_limit,
    :spam_prevention

  describe '#spam_processor' do
    describe_with_arturo_enabled(:email_rspamd) do
      it { @presenter.model_json(@model)[:spam_processor].must_equal(Account::SpamSensitivity::RSPAMD) }
    end

    describe_with_arturo_disabled(:email_rspamd) do
      it { @presenter.model_json(@model)[:spam_processor].must_equal(Account::SpamSensitivity::CLOUDMARK) }
    end
  end

  describe "#spam_prevention" do
    [:sf, :cm, :off].each do |type|
      it "has a status for spam filter" do
        @account.expects(spam_prevention: type)
        @presenter.model_json(@model)[:spam_prevention].must_equal(type.to_s)
      end
    end
  end

  it "will return 'true' for x_frame_header if security_headers are enabled" do
    @account.settings.stubs(security_headers?: true)
    @presenter.model_json(@model)[:x_frame_header].must_equal(true)
  end
end
