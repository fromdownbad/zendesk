require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::BoostPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account   = accounts(:minimum)
    @model     = @account.feature_boost || @account.create_feature_boost(boost_level: 4, valid_until: 20.days.from_now)
    @presenter = Api::V2::Internal::BoostPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :active, :expires, :boost_plan, :activated_at, :created_at, :updated_at, :url

  should_present_method :id
  should_present_method :active
  should_present_method :expires
  should_present_method :activated_at
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :boost_plan
  should_present "ExtraLarge", as: :boost_plan
  should_present_url :api_v2_internal_boost_url
end
