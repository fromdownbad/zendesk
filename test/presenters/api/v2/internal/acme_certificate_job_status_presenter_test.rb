require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AcmeCertificateJobStatusPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    @presenter = Api::V2::Internal::AcmeCertificateJobStatusPresenter.new(account.owner, url_builder: mock_url_builder)
    @model = account.acme_certificate_job_statuses.create!
  end

  should_present_keys :account_id, :activate_certificate,
    :completed_at, :created_at, :enqueued_id, :enabling_lets_encrypt,
    :id, :message, :retry_at, :run_at, :run_count, :started_at, :status,
    :status_code, :state, :updated_at
end
