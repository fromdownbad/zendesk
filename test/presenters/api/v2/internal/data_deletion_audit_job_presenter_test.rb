require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::DataDeletionAuditJobPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)

    @account.cancel!
    @account.subscription.credit_card = CreditCard.new
    @account.subscription.canceled_on = 130.days.ago
    @account.subscription.save!
    @account.soft_delete!
    @account.deleted_at = 40.days.ago
    @account.save!

    @audit = @account.data_deletion_audits.first
    @audit.created_at = 40.days.ago
    @audit.save!

    @model = @audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::BaseDataDeleteJob).tap(&:save!)
    @presenter = Api::V2::Internal::DataDeletionAuditJobPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id,
    :shard_id,
    :reason,
    :job,
    :status,
    :created_at,
    :started_at,
    :completed_at,
    :error
end
