require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::SharedSessionRecordPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:multiproduct) }

  before do
    @model = FactoryBot.create(:session_record)
    @presenter = Api::V2::Internal::SharedSessionRecordPresenter.new(account.owner, url_builder: self)
  end

  should_present_keys :account_id,
    :data,
    :expire_at,
    :id,
    :session_id,
    :user_id
end
