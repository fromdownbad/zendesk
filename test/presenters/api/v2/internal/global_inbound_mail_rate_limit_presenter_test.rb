require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::GlobalInboundMailRateLimitPresenter do
  extend Api::Presentation::TestHelper

  before do
    @model = OpenStruct.new(email: 'akim@zendesk.com', rate_limit: 10)
    @presenter = Api::V2::Internal::GlobalInboundMailRateLimitPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder, includes: [])
  end

  should_present_keys :email, :rate_limit
end
