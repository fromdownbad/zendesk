require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 12

describe Api::V2::Internal::UndeletePresenter do
  extend Api::Presentation::TestHelper
  fixtures :forums, :entries, :posts, :tickets

  before do
    @model     = forums(:solutions)
    @presenter = Api::V2::Internal::UndeletePresenter.new(users(:minimum_end_user), url_builder: mock_url_builder)
  end

  should_present_keys :id, :parent_id, :deleted_at, :name

  describe "#deleted_at" do
    let(:ticket) { tickets(:minimum_1) }

    before do
      @deleted_at = Time.now.to_s(:db)
      @updated_at = (Time.now - 1.day).to_s(:db)
    end

    it "returns the updated_at" do
      ticket.stubs(:updated_at).returns(@updated_at)
      @presenter.send(:deleted_at, ticket).must_equal @updated_at
    end

    it "returns deleted_at when the object has that method" do
      ticket.stubs(:deleted_at).returns(@deleted_at)
      @presenter.send(:deleted_at, ticket).must_equal @deleted_at
    end
  end
end
