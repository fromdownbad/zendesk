require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::DataDeletionAuditPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)

    @account.cancel!
    @account.subscription.credit_card = CreditCard.new
    @account.subscription.canceled_on = 130.days.ago
    @account.subscription.save!
    @account.soft_delete!
    @account.deleted_at = 40.days.ago
    @account.save!

    @audit = @account.data_deletion_audits.first
    @audit.created_at = 40.days.ago
    @audit.save!

    @model     = @audit
    @presenter = Api::V2::Internal::DataDeletionAuditPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :url,
    :id,
    :shard_id,
    :account_id,
    :subdomain,
    :reason,
    :status,
    :created_at,
    :started_at,
    :completed_at,
    :jobs,
    :failed_reason
end
