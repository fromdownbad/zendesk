require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ZopimAccountPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users

  before do
    @account = @model = accounts(:minimum)
    @presenter = Api::V2::Internal::ZopimAccountPresenter.new(@account.anonymous_user, url_builder: mock_url_builder)
  end

  it 'presents the account' do
    self.class.should_present @account, as: {account_id: @account.id}
  end
end
