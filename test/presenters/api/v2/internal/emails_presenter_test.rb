require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::EmailsPresenter do
  extend Api::Presentation::TestHelper

  before do
    @account = accounts(:minimum)
    @model = @account.owner.identities.first
    @presenter = Api::V2::Internal::EmailsPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id,
    :user_id,
    :value,
    :verified,
    :type,
    :primary,
    :updated_at,
    :created_at
end
