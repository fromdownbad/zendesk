require_relative "../../../../support/test_helper"
require_relative "../../../../support/ticket_metric_helper"

SingleCov.covered!

describe Api::V2::Internal::AccountStatsPresenter do
  include TicketMetricHelper

  extend Api::Presentation::TestHelper

  fixtures :accounts, :brands, :recipient_addresses,
    :account_settings, :users, :tickets

  let(:account) { accounts(:minimum) }

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Internal::AccountStatsPresenter.new(@model.owner, url_builder: mock_url_builder)

    @light_permission_set = PermissionSet.create_light_agent!(@account)
    @light_agent = users(:minimum_agent)
    @light_agent.update_attribute(:permission_set, @light_permission_set)

    @like_light_permission_set = PermissionSet.new(account: @account, name: "like light agents")
    @like_light_permission_set.permissions(ticket_editing: false,
                                           comment_access: "private",
                                           ticket_access: "within-groups",
                                           assign_tickets_to_any_group: false)
    @like_light_agent = @account.end_users.create!(name: "foo", email: "foo@bar.com")
    @like_light_agent.roles = Role::AGENT.id
    @like_light_agent.update_attribute(:permission_set, @like_light_permission_set)
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
  end

  should_present_keys :active_brand_count, :admin_count, :agent_count,
    :brand_count, :custom_field_options_count, :entry_count, :group_count,
    :inactive_brand_count, :organization_count, :owner, :rule_count,
    :settings, :sla_policies_count, :ticket_count,
    :ticket_field_conditions_count, :ticket_fields_count, :ticket_forms_count, :user_count

  describe "has archived_ticket_count" do
    before do
      t = tickets(:minimum_1)
      t.save
      archive_and_delete t
    end

    it "gives me a count of archived tickets" do
      assert_equal 1, @presenter.model_json(@account)[:ticket_count][:archived]
    end
  end

  describe 'deleted ticket count' do
    before do
      t = tickets(:minimum_1)
      t.save
      t.will_be_saved_by(User.find(-1))
      t.soft_delete
      archive_and_delete t

      t = tickets(:minimum_2)
      t.save
      t.will_be_saved_by(User.find(-1))
      t.soft_delete
    end

    let(:deleted_tickets) do
      account.ticket_archive_stubs.deleted.count + Ticket.with_deleted { account.deleted_tickets.count }
    end

    it 'returns the sum of all deleted tickets + all archived tickets that are deleted' do
      assert_equal deleted_tickets, @presenter.model_json(@account)[:ticket_count][:deleted]
    end
  end

  it "includes the owner" do
    assert_equal @account.owner.id, @presenter.model_json(@account)[:owner][:id]
  end

  describe "has agent_count setting" do
    it "presents normal agent count" do
      assert_equal 4, @presenter.model_json(@account)[:agent_count][:normal]
    end

    it "presents light agent count" do
      assert_equal 1, @presenter.model_json(@account)[:agent_count][:light]
    end

    it "presents chat agent count" do
      assert_not_nil @presenter.model_json(@account)[:agent_count][:chat]
    end

    it "presents inactive brands count" do
      assert_equal 0, @presenter.model_json(@account)[:inactive_brand_count]
    end

    it "presents active brands count" do
      assert_equal 1, @presenter.model_json(@account)[:active_brand_count]
    end
  end

  describe "has user_count setting" do
    before do
      @expected_count = 567
      @account.settings.user_count = @expected_count
    end

    it "presents correct count" do
      assert_equal @expected_count.to_s, @presenter.model_json(@account)[:user_count]
    end
  end

  describe "does not have user_count setting" do
    before do
      @account.settings.user_count = nil
    end

    it "presents correct count" do
      assert_equal @account.users.count(:all).to_s, @presenter.model_json(@account)[:user_count]
    end
  end

  describe 'has sla policies' do
    before do
      new_sla_policy

      @total_slas = @account.sla_policies.active.count(:all)
      @json       = @presenter.model_json(@account)
    end

    it 'presentes sla policies count' do
      assert @total_slas > 0
      assert_equal @total_slas, @json[:sla_policies_count]
    end
  end
end
