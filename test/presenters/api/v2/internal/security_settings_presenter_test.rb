require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::SecuritySettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    @model = account
    @presenter = Api::V2::Internal::SecuritySettingsPresenter.new(@model.owner, url_builder: mock_url_builder)
  end

  should_present_keys :admins_can_set_user_passwords,
    :assumption_expiration,
    :assumption_duration,
    :assumable_account_type,
    :assumable,
    :authentication,
    :custom_session_timeout_is_available,
    :email_agent_when_sensitive_fields_changed,
    :ip,
    :remote_authentications,
    :session_timeout,
    :two_factor_last_update
end
