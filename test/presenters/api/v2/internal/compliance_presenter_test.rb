require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::CompliancePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    @model = Compliance.create!(name: 'HIPAA')
    @presenter = Api::V2::Internal::CompliancePresenter.new(@model, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :url

  should_present_method :id

  should_present "HIPAA", as: :name
end
