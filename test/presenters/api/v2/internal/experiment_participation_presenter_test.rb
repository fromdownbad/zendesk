require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ExperimentParticipationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  let(:account) do
    accounts(:minimum)
  end

  before do
    account.experiments.each(&:delete)
    account.experiments.create(
      [
        {name: 'getting-started', group: 'skilled', version: 4, finished: true},
        {name: 'buy-now', group: 'click', version: 4},
        {name: 'buy-now', group: 'click'},
        {name: 'rate-us', group: 'green', version: 4, finished: true},
        {name: 'red-black', group: 'red', finished: true}
      ]
    )
  end

  before do
    @model              = account.experiments.first.to_data
    @presenter          = Api::V2::Internal::ExperimentParticipationPresenter.new(
      account.owner, url_builder: mock_url_builder
    )
  end

  should_present_keys :name, :group, :version, :finished

  it "shoulds wrap collection with experiment_participations key" do
    assert @presenter.present([@model]).key? :experiment_participations
  end
end
