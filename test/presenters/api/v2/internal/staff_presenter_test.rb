require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::StaffPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }

  before do
    @model = account.owner
    @presenter = Api::V2::Internal::StaffPresenter.new(account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :account_id,
    :created_at,
    :id,
    :is_active,
    :last_login,
    :locale_id,
    :name,
    :roles,
    :time_zone,
    :updated_at,
    :crypted_password,
    :salt

  describe 'when the user locale is nil' do
    before { @model.locale_id.must_be_nil }

    it 'falls back to the account locale' do
      assert_equal account.translation_locale.id, @presenter.model_json(@model).fetch(:locale_id)
    end
  end

  describe 'when the user locale is set' do
    let(:locale_id) { TranslationLocale.where(locale: 'fr').first.id }

    before { @model.update_attribute :locale_id, locale_id }

    it 'uses the user locale' do
      assert_equal locale_id, @presenter.model_json(@model).fetch(:locale_id)
    end
  end
end
