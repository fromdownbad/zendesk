require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::AgentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account   = accounts(:minimum)
    @model     = @account.admins.first
    @presenter = Api::V2::Internal::Monitor::AgentPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :email, :role, :last_login_at, :created_at, :updated_at, :suspended
end
