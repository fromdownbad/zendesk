require_relative '../../../../../support/test_helper'
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"

SingleCov.covered!

describe Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint

  fixtures :mobile_sdk_apps, :accounts, :mobile_sdk_auths, :oauth_clients, :mobile_sdk_app_settings

  let(:expected_json) do
    {
      account: {
        id: @account.id,
        subdomain: @account.subdomain,
        app_count: @account.mobile_sdk_apps.count,
        attachments: {
          enabled: @account.is_attaching_enabled,
          max_attachment_size: @account.max_attachment_size,
        }
      },
      apps: [
        {
          title: @app.title,
          app_id: @app.identifier,
          oauth_client_id: @app.mobile_sdk_auth.client.identifier,
          help_center_article_voting_enabled: @app.settings.help_center_article_voting_enabled,
          support_show_closed_requests: @app.support_show_closed_requests?,
          support_show_referrer_logo: @app.support_show_referrer_logo?,
          support_never_request_email: @app.support_never_request_email?,
          authentication: @app.settings.authentication,
          support_show_csat: @app.settings.support_show_csat?
        }.compact
      ]
    }
  end

  before do
    @account = accounts(:minimum_sdk)
    @app = @account.mobile_sdk_apps.first
    @presenter = Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  it 'presents the correct payload when not using webhook for push notifications and when using anonymous authentication' do
    actual = @presenter.model_json(@account)
    assert_equal expected_json, actual
  end

  describe 'push notifications with webhook' do
    before do
      @app.settings.push_notifications_enabled = true
      @app.settings.push_notifications_type = MobileSdkApp::PUSH_NOTIF_TYPE_WEBHOOK
      @app.settings.push_notifications_callback = 'http://something.com'
      @app.save
    end

    it 'presents the correct payload when push enabled and using webhook' do
      output = @presenter.model_json(@account)[:apps].first

      assert_equal @app.settings.push_notifications_enabled, output[:push_notifications_enabled]
      assert_equal @app.settings.push_notifications_type, output[:push_notifications_type]
      assert_equal @app.settings.push_notifications_callback, output[:push_notifications_callback]
    end
  end

  describe 'push notifications with urban airship' do
    before do
      @app.settings.push_notifications_enabled = true
      @app.settings.push_notifications_type = MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP
      @app.save
    end

    it 'presents the correct payload when push enabled and using urban airship' do
      output = @presenter.model_json(@account)[:apps].first

      assert_equal @app.settings.push_notifications_enabled, output[:push_notifications_enabled]
      assert_equal @app.settings.push_notifications_type, output[:push_notifications_type]
      assert_nil output[:push_notifications_callback]
    end
  end

  describe 'anonymous authentication' do
    before do
      @app.settings.authentication = MobileSdkAuth::TYPE_ANONYMOUS
      @app.save
    end

    it 'presents the correct payload when using anonymous auth' do
      output = @presenter.model_json(@account)[:apps].first

      assert_equal @app.settings.authentication, output[:authentication]
      assert_nil output[:jwt_url]
    end
  end

  describe 'jwt authentication' do
    before do
      @app.settings.authentication = MobileSdkAuth::TYPE_JWT
      @app.save
    end

    it 'presents the correct payload when using jwt auth' do
      output = @presenter.model_json(@account)[:apps].first

      assert_equal @app.settings.authentication, output[:authentication]
      assert_equal @app.mobile_sdk_auth.endpoint, output[:jwt_url]
    end
  end
end
