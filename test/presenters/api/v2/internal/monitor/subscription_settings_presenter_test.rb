require_relative '../../../../../support/test_helper'

SingleCov.covered! uncovered: 4

describe Api::V2::Internal::Monitor::SubscriptionSettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Internal::Monitor::SubscriptionSettingsPresenter.new(@model.owner, url_builder: mock_url_builder)
  end

  should_present_keys :boost,
    :import_mode,
    :has_high_volume_api,
    :can_be_boosted,
    :default_api_rate_limit,
    :default_api_time_rate_limit,
    :api_rate_limit,
    :api_time_rate_limit,
    :default_api_incremental_exports_rate_limit,
    :api_incremental_exports_rate_limit
end
