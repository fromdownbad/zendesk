require_relative '../../../../../support/test_helper'

SingleCov.covered! uncovered: 3

describe Api::V2::Internal::Monitor::FraudPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :tickets

  before do
    @account    = accounts(:minimum)
    @model      = @account
    @presenter  = Api::V2::Internal::Monitor::FraudPresenter.new(@model.owner, url_builder: mock_url_builder)
    @model_json = @presenter.model_json(@model)
  end

  should_present_keys :recent_tickets, :total_tickets, :active_tickets, :total_users, :active_users, :twitter_users,
    :facebook_users, :recent_fraud_scores, :fraud_score_count, :is_whitelisted, :email_integration, :risk_score,
    :templates, :require_sign_in_enabled, :anonymous_ticket_submission_enabled, :ip_address, :premium_support

  describe '#email_integration' do
    before do
      @integration_account = accounts(:multiproduct)
      @integration_account.external_email_credentials.new(
        username: "account@gmail.com",
        current_user: @integration_account.users.first
      ).tap do |external_email_credential|
        external_email_credential.encrypted_value = "unencrypted_token"
        external_email_credential.save!
      end
    end

    it 'should return false by default if integration not set-up' do
      assert_equal false, @model_json[:email_integration]
    end

    it 'should return true if integration is set-up with external email' do
      model      = @integration_account
      presenter  = Api::V2::Internal::Monitor::FraudPresenter.new(model.owner, url_builder: mock_url_builder)
      model_json = presenter.model_json(model)
      assert(model_json[:email_integration])
    end

    it 'should return false if external integration destroyed afterwards' do
      @integration_account.external_email_credentials.destroy_all
      model      = @integration_account
      presenter  = Api::V2::Internal::Monitor::FraudPresenter.new(model.owner, url_builder: mock_url_builder)
      model_json = presenter.model_json(model)
      assert_equal false, model_json[:email_integration]
    end
  end

  describe 'when there is an account with no owner' do
    before do
      Account.any_instance.stubs(:owner).returns(nil)
      presenter = Api::V2::Internal::Monitor::FraudPresenter.new(@account.users.last, url_builder: mock_url_builder)
      @model_json = presenter.model_json(@account)
    end

    it 'returns 0 for twitter and facebook user accounts' do
      assert_equal @model_json[:twitter_users], 0
      assert_equal @model_json[:facebook_users], 0
    end

    it 'returns totals for other counts' do
      assert @model_json[:total_tickets] >= 1
      assert @model_json[:total_users] >= 1
    end
  end

  describe 'when there is an account with client_ip' do
    before do
      @account.trial_extras << TrialExtra.from_hash("client_ip" => "1.1.1.1", "product_trial_selected" => "acme_app")
      @account.save!
      @account.reload
    end

    it 'returns ip_address' do
      presenter = Api::V2::Internal::Monitor::FraudPresenter.new(@account.owner, url_builder: mock_url_builder)
      @model_json = presenter.model_json(@account)
      assert_equal @model_json[:ip_address], '1.1.1.1'
    end
  end

  describe 'when there is an account with no client_ip' do
    before do
      @account.trial_extras << TrialExtra.from_hash("product_trial_selected" => "acme_app")
      @account.save!
      @account.reload
    end

    it 'returns nil ip_address' do
      presenter = Api::V2::Internal::Monitor::FraudPresenter.new(@account.owner, url_builder: mock_url_builder)
      @model_json = presenter.model_json(@account)
      assert_nil @model_json[:ip_address]
    end
  end

  describe 'template presenter data' do
    it 'returns json hash if there is template data' do
      presenter = Api::V2::Internal::Monitor::FraudPresenter.new(@account.owner, url_builder: mock_url_builder)
      @model_json = presenter.model_json(@account)
      assert @model_json[:templates][:text_mail]
    end

    it 'returns an empty hash if there is no template data' do
      Rails.logger.expects(:info).with(regexp_matches(/Could not fetch templates for account/)).once
      Account.any_instance.stubs(:account_property_set).returns(nil)
      presenter = Api::V2::Internal::Monitor::FraudPresenter.new(@account.owner, url_builder: mock_url_builder)
      @model_json = presenter.model_json(@account)
      assert_empty @model_json[:templates]
    end
  end

  describe 'PRESENTER_CAP_AMOUNT will cap total' do
    before do
      Api::V2::Internal::Monitor::FraudPresenter.send(:remove_const, :PRESENTER_CAP_AMOUNT)
      Api::V2::Internal::Monitor::FraudPresenter.const_set(:PRESENTER_CAP_AMOUNT, 1)
    end

    it 'caps total users' do
      users = @presenter.send(:total_users)
      assert_equal users, 1
    end

    it 'caps total tickets' do
      tickets = @presenter.send(:total_tickets)
      assert_equal tickets, 1
    end
  end

  describe '#recent_tickets' do
    before do
      @account.tickets.last.update_column(:via_id, ViaType.TWITTER)
      Api::V2::Internal::Monitor::FraudPresenter.send(:remove_const, :RECENT_TICKET_DATE_THRESHOLD)
      Api::V2::Internal::Monitor::FraudPresenter.const_set(:RECENT_TICKET_DATE_THRESHOLD, 25.days.ago)
      Api::V2::Internal::Monitor::FraudPresenter.send(:remove_const, :RECENT_TICKET_LIMIT)
      Api::V2::Internal::Monitor::FraudPresenter.const_set(:RECENT_TICKET_LIMIT, 3)
      @tickets = @presenter.send(:recent_tickets)
    end

    it 'should return the correct amount of recent tickets' do
      assert_equal @tickets.size, Api::V2::Internal::Monitor::FraudPresenter::RECENT_TICKET_LIMIT
    end

    it 'should return recently created tickets only' do
      assert (@tickets.all? { |t| t.created_at >= Api::V2::Internal::Monitor::FraudPresenter::RECENT_TICKET_DATE_THRESHOLD })
    end

    it 'should return non social media tickets only' do
      assert (@tickets.none? { |t| Api::V2::Internal::Monitor::FraudPresenter::SOCIAL_MEDIA.include?(t.via_id) })
    end
  end
end
