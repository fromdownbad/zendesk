require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::RecentTicketsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :tickets

  before do
    @account   = accounts(:minimum)
    @model     = tickets(:minimum_1)
    @presenter = Api::V2::Internal::Monitor::RecentTicketsPresenter.new(@account.owner, url_builder: mock_url_builder)
    @model_json = @presenter.model_json(@model)
  end

  should_present_keys :collaborator_count, :created_at, :description, :title, :requester, :active, :creation_method, :nice_id

  describe 'creation_method' do
    it 'should return creation name if creation method valid' do
      assert_equal 'Web form', @model_json[:creation_method]
    end

    it 'should return n/a if creation method invalid' do
      @model.via_id = -1
      @model_json = @presenter.model_json(@model)
      assert_equal 'n/a', @model_json[:creation_method]
    end
  end
end
