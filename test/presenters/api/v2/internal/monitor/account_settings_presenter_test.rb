require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::AccountSettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings

  subject { Api::V2::Internal::Monitor::AccountSettingsPresenter.new(account.owner, url_builder: mock_url_builder) }

  let(:account) { accounts(:minimum) }

  describe '#model_json' do
    before { Zendesk::Accounts::Client.any_instance.stubs(abusive: []) }

    describe '#spam_processor' do
      describe_with_arturo_enabled(:email_rspamd) do
        it { subject.model_json(account)[:spam_processor].must_equal(Account::SpamSensitivity::RSPAMD) }
      end

      describe_with_arturo_disabled(:email_rspamd) do
        it { subject.model_json(account)[:spam_processor].must_equal(Account::SpamSensitivity::CLOUDMARK) }
      end
    end

    it 'returns assumable_account_type' do
      subject.model_json(account)[:assumable_account_type].must_equal(false)
    end

    it 'returns assumable' do
      subject.model_json(account)[:assumable].must_equal(false)
    end

    it 'returns always_assumable' do
      subject.model_json(account)[:always_assumable].must_equal(false)
    end

    describe 'spam_prevention' do
      [:sf, :cm, :off].each do |type|
        it 'has a status for spam filter' do
          account.expects(:spam_prevention).returns(type)

          subject.model_json(account)[:spam_prevention].must_equal(type.to_s)
        end
      end
    end

    it 'has remote_auth' do
      remote_auth = subject.model_json(account)[:remote_auth]

      remote_auth[:saml].must_equal(account.remote_authentications.saml.active.exists?)
      remote_auth[:jwt].must_equal(account.remote_authentications.jwt.active.exists?)
    end
  end
end
