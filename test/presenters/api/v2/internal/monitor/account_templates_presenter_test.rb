require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::AccountTemplatesPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Internal::Monitor::AccountTemplatesPresenter.new(@model, url_builder: mock_url_builder)
    @model_json = @presenter.model_json(@model)
  end

  should_present_keys :text_mail, :text_mail_changed, :html_mail, :html_mail_changed, :signup_email_text,
    :signup_email_text_changed, :verify_email_text, :verify_email_text_changed, :template_names

  def reload_presenter
    @model     = @account
    @presenter = Api::V2::Internal::Monitor::AccountTemplatesPresenter.new(@model, url_builder: mock_url_builder)
    @model_json = @presenter.model_json(@model)
  end

  describe 'text_mail' do
    it 'text_mail_changed should detect a change away from default content' do
      @account.stubs(:text_mail_template).returns("test")
      reload_presenter
      assert(@model_json[:text_mail_changed])
    end

    it 'should read text_mail content' do
      @account.stubs(:text_mail_template).returns("test")
      reload_presenter
      assert_equal "test", @model_json[:text_mail]
    end
  end

  describe 'html_mail' do
    it 'html_mail_changed should detect a change away from default content' do
      @account.stubs(:html_mail_template).returns("test")
      reload_presenter
      assert(@model_json[:html_mail_changed])
    end

    it 'should read html_mail content' do
      @account.stubs(:html_mail_template).returns("test")
      reload_presenter
      assert_equal "test", @model_json[:html_mail]
    end
  end

  describe 'signup_email_text' do
    it 'signup_email_text should detect a change away from default content' do
      @account.stubs(:signup_email_text).returns("test")
      reload_presenter
      assert(@model_json[:signup_email_text_changed])
    end

    it 'should read signup_email_text content' do
      @account.stubs(:signup_email_text).returns("test")
      reload_presenter
      assert_equal "test", @model_json[:signup_email_text]
    end
  end

  describe 'verify_email_text' do
    it 'verify_email_text should detect a change away from default content' do
      @account.stubs(:verify_email_text).returns("test")
      reload_presenter
      assert(@model_json[:verify_email_text_changed])
    end

    it 'should read verify_email_text content' do
      @account.stubs(:verify_email_text).returns("test")
      reload_presenter
      assert_equal "test", @model_json[:verify_email_text]
    end
  end
end
