require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::MobileSdkBlipsPresenter do
  extend Api::Presentation::TestHelper
  before do
    @account = accounts(:minimum)
    @app = @account.mobile_sdk_apps.first
    @presenter = Api::V2::Internal::Monitor::MobileSdkBlipsPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  it 'presents mobile sdk blips settings' do
    expected = {
      account: {
        id: @account.id,
        blips: {
          enabled: true,
          name: 'Blips',
          permissions: [
            { name: 'Required Blips', key: 'required_blips', enabled: @account.settings.mobile_sdk_required_blips },
            { name: 'Behavioural Blips', key: 'behavioural_blips', enabled: @account.settings.mobile_sdk_behavioural_blips },
            { name: 'Pathfinder Blips', key: 'pathfinder_blips', enabled: @account.settings.mobile_sdk_pathfinder_blips }
          ]
        }
      },
      apps: [{
        id: @app.id,
        title: @app.title,
        blips: {
          enabled: true,
          name: 'Blips',
          permissions: [
            { name: 'Required Blips', key: 'required_blips', enabled: @app.settings.required_blips },
            { name: 'Behavioural Blips', key: 'behavioural_blips', enabled: @app.settings.behavioural_blips },
            { name: 'Pathfinder Blips', key: 'pathfinder_blips', enabled: @app.settings.pathfinder_blips }
          ]
        }
      }]
    }
    actual = @presenter.present(@account)

    assert_equal expected, actual[:settings]
  end
end
