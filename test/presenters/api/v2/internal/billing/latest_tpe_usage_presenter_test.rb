require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::LatestTpeUsagePresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:trial) }
  let(:owner)   { account.owner }

  let(:presenter) do
    Api::V2::Internal::Billing::LatestTpeUsagePresenter.new(
      owner,
      url_builder: mock_url_builder
    )
  end

  let(:presented_json) { presenter.model_json(usage) }

  before do
    @model     = usage
    @presenter = presenter
  end

  describe 'when passed a Voice::CtiUsage' do
    let!(:usage) do
      Voice::CtiUsage.new.tap do |usage|
        usage.account = account
        usage.user    = owner
        usage.save!(validate: false)
      end
    end

    let(:expected_json) do
      { created_at: usage.created_at }
    end

    should_present_keys :created_at

    it 'presents the usage details' do
      assert_equal expected_json, presented_json
    end
  end

  describe 'when not passed a Voice::CtiUsage' do
    let!(:usage)        { nil }
    let(:expected_json) { { created_at: nil } }

    should_present_keys :created_at

    it 'presents the usage details' do
      assert_equal expected_json, presented_json
    end
  end
end
