require_relative '../../../../../support/test_helper'
require_relative '../../../../../support/suite_test_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::AgentCountPresenter do
  extend Api::Presentation::TestHelper

  include SuiteTestHelper

  fixtures :accounts, :zopim_subscriptions, :permission_sets, :zopim_agents

  before do
    setup_stubs_for_adding_agents(account)

    account.owner.account_id = account.id
    account.owner.save!(validate: false)

    @model     = account
    @presenter = presenter
  end

  let(:owner) { account.owner }

  let(:presenter) do
    Api::V2::Internal::Billing::AgentCountPresenter.new(
      owner,
      url_builder: mock_url_builder
    )
  end

  describe 'when multiproduct account' do
    # :multiproduct account comes with the following agents:
    #  - Multiproduct account owner (Support-only)
    #  - multiproduct_contributor (Chat-only)
    #  - multiproduct_custom_agent (Support + Chat)
    #  - multiproduct_light_agent (not billable)
    #  - multiproduct_support_agent (Support-only)
    #  - multiproduct_billing_admin (Support)
    let(:account)            { accounts(:multiproduct) }
    let(:chat_only_agent)    { users(:multiproduct_contributor) }
    let(:support_chat_agent) { users(:multiproduct_custom_agent) }
    let(:support_only_agent) { users(:multiproduct_support_agent) }

    let(:expected_json) do
      {
        suite_agents: 6 # => owner + support_admin + chat_only_agent + support_chat_agent + support_only_agent + billing_admin
      }
    end

    before do
      chat_only_agent.update_entitlement(product: 'chat', new_entitlement: 'agent')
      support_chat_agent.account = account
      support_chat_agent.update_entitlement(product: 'chat', new_entitlement: 'agent')
      support_chat_agent.save!
    end

    should_present_keys :suite_agents

    it 'presents the agent counts' do
      assert_equal expected_json, presenter.model_json(account)
    end
  end

  describe 'when non-multiproduct account' do
    let(:account) { accounts(:with_trial_zopim_subscription) }

    let(:expected_json) do
      {
        suite_agents: 4 # => owner + all 3 agents
      }
    end

    before do
      FactoryBot.create(:chat_enabled_agent, account: account)
      FactoryBot.create(:chat_only_agent,    account: account)
      FactoryBot.create(:agent,              account: account)
    end

    should_present_keys :suite_agents

    it 'presents the agent counts' do
      assert_equal expected_json, presenter.model_json(account)
    end
  end

  def setup_stubs_for_adding_agents(account)
    PermissionSet.create_chat_agent!(account)

    User.any_instance.stubs(encrypt_password: true)
    User.any_instance.stubs(password_security_policy_enforced: true)

    Zopim::Agent.any_instance.stubs(:link_to_remote_account_record)

    SecurityMailer.stubs(send: true)
  end
end
