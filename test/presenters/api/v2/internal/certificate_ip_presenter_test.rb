require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::CertificateIpPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :certificates, :certificate_ips

  before do
    @account   = accounts(:minimum)
    @model     = certificate_ips(:one)
    @presenter = Api::V2::Internal::CertificateIpPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :alias_record, :certificate_id, :created_at, :id, :ip, :new_ip, :old_ip, :pod_id, :port, :release_at, :sni, :updated_at

  describe "with a SNI certificate" do
    before do
      @model = certificate_ips(:sni)
      @model.ip.must_be_nil
    end

    it "presents the pod A record" do
      @presenter.model_json(@model)['ip'].must_equal("127.0.0.1")
    end

    it "resolves the pod alias_record for an AWS pod" do
      @model.pod_id = 3
      Resolv.expects(:getaddress).with('alias_record').returns('ip address').once
      @presenter.model_json(@model)['ip'].must_equal("ip address")
    end
  end
end
