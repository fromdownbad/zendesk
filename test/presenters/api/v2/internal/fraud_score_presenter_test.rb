require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::FraudScorePresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:user) { account.anonymous_user }
  let(:account) { accounts(:minimum) }
  let(:fraud_score) { FactoryBot.create(:fraud_score, account: account) }

  let(:presenter) do
    Api::V2::Internal::FraudScorePresenter.new(user, url_builder: mock_url_builder)
  end

  before do
    @presenter = presenter
    @model = fraud_score
  end

  should_present_keys :id, :account_id, :verified_fraud, :score, :talk_risky, :support_risky,
    :account_fraud_service_release, :owner_email, :subdomain,
    :created_at, :other_params, :is_whitelisted, :score_params, :source_event
end
