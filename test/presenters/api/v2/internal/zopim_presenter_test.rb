require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ZopimPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account = @model = accounts(:minimum)
    @presenter = Api::V2::Internal::ZopimPresenter.new(@account.anonymous_user, url_builder: mock_url_builder)
  end

  should_present_keys :account_id
end
