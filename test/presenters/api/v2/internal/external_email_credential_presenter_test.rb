require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::ExternalEmailCredentialPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :users, :external_email_credential

  before do
    @model     = external_email_credential(:default)
    @presenter = Api::V2::Internal::ExternalEmailCredentialPresenter.new(users(:systemuser), url_builder: mock_url_builder)
  end

  should_present_keys :id,
    :active,
    :credential_updated_at,
    :error_count,
    :last_error_at,
    :last_error_message,
    :last_fetched_at,
    :need_repair,
    :url
end
