require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ComplianceMovesPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    @account   = accounts(:support)
    @presenter = Api::V2::Internal::ComplianceMovesPresenter.new(@account.owner, url_builder: mock_url_builder)

    ComplianceMove.create!(
      account: @account,
      account_subdomain: @account.subdomain,
      src_shard_id: @account.shard_id,
      src_pod_id: @account.pod_id,
      move_type: 3,
      move_scheduled: false
    )
  end

  describe '#model_json' do
    it 'decorates the compliance move results' do
      results = @presenter.model_json(@account)
      assert_equal 1, results.count
      assert_equal @account.id, results.first['account_id']
      assert_equal 3, results.first['move_type']
      assert_equal @account.shard_id, results.first['src_shard_id']
      assert_equal @account.pod_id, results.first['src_pod_id']
      assert_equal @account.subdomain, results.first['account_subdomain']
      assert_equal false, results.first['move_scheduled']
      assert_equal @account.name, results.first['account_name']
      assert_equal @account.subscription.plan_name, results.first['plan_name']
    end

    it 'does not present accounts where "z3n" is found in any part of the subdomain' do
      @account.subdomain = 'zenz3ntest'
      @account.save!
      results = @presenter.model_json(@account)
      assert_equal 0, results.count
    end

    it 'presents accounts where "z3n" is not found in the subdomain' do
      @account.subdomain = 'zentest'
      @account.save!
      results = @presenter.model_json(@account)
      assert_equal 1, results.count
    end
  end
end
