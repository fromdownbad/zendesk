require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Api::V2::Internal::AccountSettingsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users, :remote_authentications, :account_settings

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @presenter = Api::V2::Internal::AccountSettingsPresenter.new(@model.owner, url_builder: mock_url_builder)
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
  end

  should_present_keys :user_identity_limit_for_trialers, :bulk_user_upload, :allow_keys_locales, :allow_test_locales,
    :default_available_locales, :archive, :archive_after_days, :boost, :cdn_provider, :default_cdn_provider,
    :hc_settings, :partner_settings, :import_mode, :password_reset_override, :prefer_lotus, :remote_auth,
    :security_headers, :x_frame_header, :xss_header, :csp_header, :content_type_header, :is_watchlisted, :support_risky,
    :ssl_enabled, :voice_trial_enabled, :monitored_facebook_page_limit, :spam_processor, :risk_assessment,
    :spam_threshold_multiplier, :spam_prevention, :end_user_portal_ticket_rate_limit, :end_user_entry_rate_limit,
    :end_user_comment_rate_limit, :email_agent_when_sensitive_fields_changed, :active_brand_count,
    :owner, :agent_count, :can_be_boosted, :default_api_rate_limit, :is_abusive, :has_google_apps, :fraudulent,
    :default_api_incremental_exports_rate_limit, :api_incremental_exports_rate_limit,
    :default_api_time_rate_limit, :api_time_rate_limit, :total_in_flight_jobs_limit, :captcha_required,
    :close_after_days, :max_ticket_batch_close_count, :show_csat_on_dashboard, :native_messaging

  describe "fraudulent" do
    describe "when the account is abusive" do
      before { @account.stubs(:abusive?).returns(true) }
      it "returns true" do
        @presenter.model_json(@model)[:fraudulent].must_equal(true)
      end
    end

    describe "when the account is not abusive" do
      before { @account.stubs(:abusive?).returns(false) }

      it "returns false" do
        @presenter.model_json(@model)[:fraudulent].must_equal(false)
      end
    end
  end

  describe '#spam_processor' do
    describe_with_arturo_enabled(:email_rspamd) do
      it { @presenter.model_json(@model)[:spam_processor].must_equal(Account::SpamSensitivity::RSPAMD) }
    end

    describe_with_arturo_disabled(:email_rspamd) do
      it { @presenter.model_json(@model)[:spam_processor].must_equal(Account::SpamSensitivity::CLOUDMARK) }
    end
  end

  describe "#spam_prevention" do
    [:sf, :cm, :off].each do |type|
      it "has a status for spam filter" do
        @account.expects(:spam_prevention).returns(type)
        @presenter.model_json(@model)[:spam_prevention].must_equal(type.to_s)
      end
    end
  end
end
