require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::CommentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @comments = @account.tickets.map(&:comments).flatten
    @presenter_options = {
      ticket_ids: @account.tickets.map(&:nice_id),
      limit: @comments.size / 2,
      url_builder: mock_url_builder
    }
    @presenter = Api::V2::Internal::CommentPresenter.new(@account.owner, @presenter_options)
  end

  it 'displays the ticket_id' do
    refute_nil @presenter.present(@comments.first)[:comment][:ticket_id]
  end

  it 'displays pagination' do
    expects_new_page_url
    presenter_result = @presenter.present(@comments)
    refute_nil presenter_result[:comments]
    asserts_watermark_pagination(presenter_result)
  end

  it 'stills have a next page when the size hits the page limit' do
    expects_new_page_url
    @presenter_options[:limit] = @comments.size
    presenter_result = Api::V2::Internal::CommentPresenter.new(@account.owner, @presenter_options).present(@comments)
    asserts_watermark_pagination(presenter_result)
  end

  it 'does not have more pages when the page size is less than the limit' do
    refutes_new_page
    @presenter_options[:limit] = @comments.size + 1
    presenter_result = Api::V2::Internal::CommentPresenter.new(@account.owner, @presenter_options).present(@comments)
    assert_nil presenter_result[:next_page]
    assert_nil presenter_result[:id_waterline]
    assert_nil presenter_result[:comment_id_waterline]
  end

  describe 'when a comment is missing an event' do
    before do
      @comments.first.stubs(:audit).returns(nil)
      @presenter_options[:url_builder].expects(:show_many_comments_api_v2_internal_tickets_url).once
    end

    it "can't render most of the metadata but via should be ok" do
      rendered_comments = @presenter.present(@comments)
      metadata = rendered_comments[:comments].first[:metadata].except(:decoration)
      assert_equal({}, metadata)
    end
  end

  def asserts_watermark_pagination(presenter_result)
    assert_equal 'test_url', presenter_result[:next_page]
    assert_equal @comments.last.ticket.nice_id, presenter_result[:id_waterline]
    assert_equal @comments.last.id, presenter_result[:comment_id_waterline]
  end

  def expects_new_page_url
    params = {
      ticket_ids: @presenter_options[:ticket_ids],
      format: :json,
      id_waterline: @comments.last.ticket.nice_id,
      comment_id_waterline: @comments.last.id
    }
    url_builder.expects(:show_many_comments_api_v2_internal_tickets_url).with(params).once.returns('test_url')
  end

  def refutes_new_page
    url_builder.expects(:show_many_comments_api_v2_internal_tickets_url).never
  end

  def url_builder
    @presenter_options[:url_builder]
  end
end
