require_relative '../../../../support/test_helper'
require_relative '../../../../support/deflection_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Internal::PredictionSettingsPresenter do
  include DeflectionHelper
  include TestSupport::Rule::Helper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe '#present' do
    let(:presenter) { Api::V2::Internal::PredictionSettingsPresenter.new }
    let(:deflection_triggers) { Trigger.where(title: 'deflection_trigger') }
    let(:potential_deflection_triggers) { Trigger.where(title: 'notification_trigger') }
    let(:presented) { presenter.present(deflection_triggers, potential_deflection_triggers) }
    let(:expected_keys) { [:id, :title, :permalink, :usage_30d].map(&:to_s).sort.map(&:to_sym) }

    before do
      Trigger.destroy_all
      account.stubs(:has_automatic_answers_enabled?).returns(true)
    end

    describe 'JSON keys' do
      before do
        create_trigger(title: 'deflection_trigger', active: true, definition: build_deflection_action)
        create_trigger(title: 'notification_trigger', active: true, definition: build_notification_action_conditions_all)
      end

      it 'contains the `prediction_settings` key' do
        assert presented[:prediction_settings]
      end

      describe 'deflection_triggers' do
        let(:triggers) { presented[:prediction_settings][:deflection_triggers] }
        let(:actual_keys) { triggers.first.keys.map(&:to_s).sort.map(&:to_sym) }

        it 'contains the `deflection_triggers` key' do
          assert triggers
        end

        it 'presents the right keys' do
          assert_equal expected_keys, actual_keys
        end
      end

      describe 'potential_deflection_triggers' do
        let(:triggers) { presented[:prediction_settings][:potential_deflection_triggers] }
        let(:actual_keys) { triggers.first.keys.map(&:to_s).sort.map(&:to_sym) }

        it 'contains the `potential_deflection_triggers` key' do
          assert triggers
        end

        it 'presents the right keys' do
          assert_equal expected_keys, actual_keys
        end
      end
    end

    describe 'result limiting' do
      let(:presented_deflections) { presented[:prediction_settings][:deflection_triggers] }
      let(:presented_potentials) { presented[:prediction_settings][:potential_deflection_triggers] }
      let(:triggers_limit) { Api::V2::Internal::PredictionSettingsPresenter::TRIGGERS_LIMIT }

      before do
        51.times do
          create_trigger(title: 'deflection_trigger', active: true, definition: build_deflection_action)
          create_trigger(title: 'notification_trigger', active: true, definition: build_notification_action_conditions_all)
        end
      end

      it 'limits results to 50' do
        assert_equal triggers_limit, presented_deflections.count
        assert_equal triggers_limit, presented_potentials.count
      end
    end
  end
end
