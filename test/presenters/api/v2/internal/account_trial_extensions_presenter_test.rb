require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AccountTrialExtensionsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account   = accounts(:minimum)
    @model     = @account
    @admin     = users(:minimum_admin)
    @presenter = Api::V2::Internal::AccountTrialExtensionsPresenter.new(@model.owner, url_builder: mock_url_builder)
  end

  should_present_keys :trial_extensions

  describe "with one trial extension" do
    before do
      extension = TrialExtension.new
      extension.author = @admin
      extension.account_id = @account.id
      extension.duration = 70
      extension.save!
    end

    it "will present the trial extension with account id and duration set" do
      @presenter.model_json(@model)[:trial_extensions][0][:account_id].must_equal(@account.id)
      @presenter.model_json(@model)[:trial_extensions][0][:duration].must_equal(70)
    end
  end
end
