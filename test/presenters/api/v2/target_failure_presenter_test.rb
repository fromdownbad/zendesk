require_relative "../../../support/test_helper"
require 'johnny_five'

SingleCov.covered!

describe "target failure presenter" do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :events, :targets

  let(:account) { accounts(:minimum) }
  let(:request) { 'raw_request' }
  let(:response) { 'Not able to process it' }
  let(:presenter) { Api::V2::TargetFailurePresenter.new(account.owner, url_builder: mock_url_builder) }

  describe "display json model with normal target failure" do
    before do
      @target = UrlTargetV2.new(
        title: "HTTP V2 Test Target",
        account: account,
        url: "http://www.example.com/a",
        method: "get"
      )
      @target.save

      @target_failure = TargetFailure.new(
        account: account,
        target: @target,
        event: events(:create_external_for_minimum_ticket_1),
        exception: StandardError.to_s,
        message: 'Generic error is happened',
        status_code: 400,
        consecutive_failure_count: 1,
        raw_http_capture: @target.raw_http_capture,
        response_body: response
      )
    end

    it "should json be built with no option" do
      @target_failure.save

      json = presenter.model_json(@target_failure)

      expect = {
        url: :api_v2_target_failure_url,
        id: @target_failure.id,
        target_name: 'HTTP V2 Test Target',
        status_code: 400,
        message: 'Generic error is happened',
        created_at: @target_failure.created_at,
        consecutive_failure_count: 1
      }

      assert_equal expect, json
    end

    it "should json be built with no option" do
      @target.raw_http_capture.transactions.last.request.stubs(raw: request)

      @target_failure.save

      presenter.options[:action] = 'show'
      json = presenter.model_json(@target_failure)

      expect = {
        url: :api_v2_target_failure_url,
        id: @target_failure.id,
        target_name: 'HTTP V2 Test Target',
        status_code: 400,
        message: 'Generic error is happened',
        created_at: @target_failure.created_at,
        consecutive_failure_count: 1,
        raw_request: request,
        raw_response: "\r\n\r\n#{response}"
      }

      assert_equal expect, json
    end
  end

  describe "display json model with circuit breaker target failure" do
    before do
      @target = UrlTargetV2.new(
        title: "HTTP V2 Test Target",
        account: account,
        url: "http://www.example.com/a",
        method: "get"
      )
      @target.save

      @target_failure = TargetFailure.new(
        account: account,
        target: @target,
        event: events(:create_external_for_minimum_ticket_1),
        exception: ::JohnnyFive::CircuitTrippedError.to_s,
        message: 'Circuit tripped error',
        status_code: 400,
        consecutive_failure_count: 1,
        raw_http_capture: @target.raw_http_capture
      )
    end

    it "should json be built with no option" do
      @target_failure.save

      json = presenter.model_json(@target_failure)

      expect = {
        url: :api_v2_target_failure_url,
        id: @target_failure.id,
        target_name: 'HTTP V2 Test Target',
        message: 'Circuit tripped error',
        created_at: @target_failure.created_at,
        consecutive_failure_count: '-'
      }

      assert_equal expect, json
    end

    it "should json be built with no option" do
      @target_failure.save

      presenter.options[:action] = 'show'
      json = presenter.model_json(@target_failure)

      expect = {
        url: :api_v2_target_failure_url,
        id: @target_failure.id,
        target_name: 'HTTP V2 Test Target',
        message: 'Circuit tripped error',
        created_at: @target_failure.created_at,
        consecutive_failure_count: '-',
        raw_request: 'Circuit tripped error : Request was not issued as this endpoint had more than 100 failures in the last 5 minutes. To protect your endpoint while it is recovering, we will only attempt one request every 5 seconds to this endpoint until that request is successful.',
        raw_response: nil
      }

      assert_equal expect, json
    end
  end

  describe "#association_preloads" do
    it "should preload only target" do
      expect = {
        target: {}
      }
      assert_equal expect, presenter.association_preloads
    end
  end
end
