require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AppMarketIntegrationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:model) { OpenStruct.new }

  before do
    @model     = model
    @account   = accounts(:minimum)
    @presenter = Api::V2::AppMarketIntegrationPresenter.new(
      @account.owner, url_builder: mock_url_builder
    )
  end

  should_present_keys :app_market_id,
    :app_installed,
    :app_name,
    :app_path
end
