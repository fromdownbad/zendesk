require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TargetPresenter do
  fixtures :targets

  extend Api::Presentation::TestHelper

  def self.should_present_target(target_class, methods)
    describe target_class do
      before { prepare_target(target_class) }

      methods = [:id, :created_at, :type, :title, :active, :url] + methods
      should_present_keys *methods

      (methods - [:type, :url, :target_url, :active] - Target::SECRET_SETTINGS).each do |method|
        should_present_method method
      end

      should_present_method :url, as: :target_url if methods.include?(:target_url)
      Target::SECRET_SETTINGS.each do |setting|
        should_present_method setting, value: nil if methods.include?(setting)
      end

      should_present_common_methods(target_class)
    end
  end

  def prepare_target(target_class)
    @account = accounts(:minimum)
    @model = target_class.new { |t| t.id = 12345; t.account = @account }
    @model.instance_variable_set(:@new_record, false)
    @presenter = Api::V2::TargetPresenter.new(@model, url_builder: mock_url_builder)
  end

  def self.should_present_common_methods(target_class)
    should_present_method :active, value: false
    should_present_method :type, value: Zendesk::Targets::Initializer.new(account: Account.new, via: :api).target_types_map.key(target_class)
    should_present_url :api_v2_target_url, with: :id
  end

  should_present_target BasecampTarget, [:target_url, :username, :token, :password, :project_id, :resource, :message_id, :todo_list_id]
  should_present_target CampfireTarget, [:room, :subdomain, :ssl, :preserve_format, :token]
  should_present_target ClickatellTarget, [:username, :password, :api_id, :to, :from, :us_small_business_account, :target_url, :method, :attribute]
  should_present_target EmailTarget, [:email, :subject]
  should_present_target FlowdockTarget, [:api_token]
  should_present_target GetSatisfactionTarget, [:email, :password, :account_name, :target_url]
  should_present_target JiraTarget, [:target_url, :username, :password]
  should_present_target OauthTarget, [:consumer_key, :secret, :callback_token, :site, :oauth_token, :oauth_token_secret]
  should_present_target MsDynamicsTarget, []
  should_present_target PivotalTarget, [:token, :project_id, :owned_by, :requested_by, :story_labels, :story_type, :story_title]
  should_present_target TwitterTarget, [:token, :secret]
  should_present_target UrlTarget, [:target_url, :attribute, :method, :username, :password]
  should_present_target UrlTargetV2, [:target_url, :method, :content_type, :username, :password]
  should_present_target YammerTarget, [:group_id, :consumer_key, :secret, :callback_token, :oauth_token, :oauth_token_secret, :oauth2_token, :site]
end
