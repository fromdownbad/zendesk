require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 2

describe Api::V2::GroupPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint

  fixtures :groups, :accounts, :users

  before do
    @model = groups(:serialization_group)

    @presenter = Api::V2::GroupPresenter.new(
      @model.account.owner,
      url_builder: mock_url_builder
    )
  end

  should_pass_lint_tests

  should_present_keys :id, :name, :description, :created_at, :updated_at, :url, :deleted, :default

  should_present_method :id
  should_present_method :name
  should_present_method :created_at
  should_present_method :updated_at

  should_present false, as: :deleted

  should_present_url :api_v2_group_url

  describe '#model_json with side_load' do
    describe 'for group without users' do
      it "presents user_ids with 'users' includes" do
        presenter = Api::V2::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder, includes: [:users])
        json = presenter.model_json(@model)

        assert_equal([], json[:user_ids])
      end

      it "presents user_ids with 'user_ids' includes" do
        presenter = Api::V2::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder, includes: [:user_ids])
        json = presenter.model_json(@model)

        assert_equal([], json[:user_ids])
      end
    end

    describe 'for group with at least one user' do
      let(:user_id) { mock('user_id') }
      before { @model.stubs(:users).returns([mock('user', id: user_id)]) }

      it "presents user_ids with 'users' includes" do
        presenter = Api::V2::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder, includes: [:users])
        json = presenter.model_json(@model)

        assert_equal([user_id], json[:user_ids])
      end

      it "presents user_ids with 'user_ids' includes" do
        presenter = Api::V2::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder, includes: [:user_ids])
        json = presenter.model_json(@model)

        assert_equal([user_id], json[:user_ids])
      end
    end

    it 'present group_settings with defaults' do
      presenter = Api::V2::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder, includes: [:group_settings])
      json = presenter.model_json(@model)

      defaults = {
        'chat_enabled' => false
      }

      assert_equal(defaults, json[:settings])
    end
  end

  describe '#condensed_model_json' do
    it "only present id, name and url keys" do
      assert_same_elements(
        %i[id name description url],
        @presenter.condensed_model_json(@model).keys
      )
    end
  end

  describe '#side_loads' do
    let(:presented_groups)  { [stub('group')] }
    let(:side_loaded_users) { [stub('side-loaded users')] }

    describe 'when not side-loading' do
      before { @presenter.stubs(:side_load?).returns(false) }

      it 'returns nothing' do
        assert_equal({}, @presenter.side_loads(presented_groups))
      end
    end

    describe 'when side-loading users' do
      before do
        @presenter.stubs(:side_load?).with(:users).returns(true)

        @presenter.stubs(:side_load_association).
          with(presented_groups, :users, @presenter.user_presenter).
          returns(side_loaded_users)
      end

      it 'side-loads users' do
        assert_equal(
          side_loaded_users,
          @presenter.side_loads(presented_groups)[:users]
        )
      end
    end
  end

  describe '#association_preloads' do
    describe 'when not side-loading' do
      before { @presenter.stubs(:side_load?).returns(false) }

      it 'returns nothing' do
        assert_equal({}, @presenter.association_preloads)
      end
    end

    describe 'when side-loading users' do
      before { @presenter.stubs(:side_load?).with(:users).returns(true) }

      it 'returns the `UserPresenter` association preloads' do
        assert_equal(
          @presenter.user_presenter.association_preloads,
          @presenter.association_preloads[:users]
        )
      end
    end
  end
end
