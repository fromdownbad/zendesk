require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Exports::TicketPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @account.stubs(:gooddata_integration).returns(mock(scheduled_at: "15PM"))
    @opts = { timezone: "Europe/Copenhagen", hour_offset: "15" }
    @start_time = Time.now.to_i
    @model      = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(@start_time), 1000, show_metrics: true)
    @presenter  = Api::V2::Exports::TicketPresenter.new(@account.owner, url_builder: mock_url_builder, limit: 1000, poll_interval: 300)
  end

  describe "with no tickets" do
    before do
      @model.stubs(:tickets).returns([])
      @ticket_export = @presenter.present(@model)
    end

    it "presents the field headers and empty results" do
      assert @ticket_export[:field_headers]
      assert_equal [], @ticket_export[:results]
    end
  end

  describe "when there are > 1k tickets in the same window" do
    before do
      @state = states('tries').starts_as('first')
      @tickets = Array.new(1000) { {'generated_timestamp' => @start_time } }
      @model.stubs(:tickets).returns(@tickets).then(@state.is('second'))
      @model.expects(:exact_ts=).with(true).when(@state.is('second'))
      @model.expects(:limit=).with(nil).when(@state.is('second'))
      @ticket_export = @presenter.present(@model)
    end

    it "presents the correct results and metadata" do
      assert @ticket_export[:field_headers]
      assert_equal :api_v2_exports_tickets_url, @ticket_export[:next_page]
      assert_equal @tickets, @ticket_export[:results]
    end

    it "presents the options metadata" do
      assert_not_nil @ticket_export[:options]
      assert_equal @opts, @ticket_export[:options]
    end

    it "re-querys with exact_ts = true, limit = nil and bump end_time" do
      assert_equal @start_time + 1, @ticket_export[:end_time]
    end
  end
end
