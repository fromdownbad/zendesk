require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Exports::IncrementalTicketPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :tickets, :users, :facebook_pages

  before do
    @account = accounts(:minimum)
    @model = tickets(:minimum_2)
  end

  describe '#association_preloads' do
    it 'uses the item presenter preloads' do
      preloads = {
        ticket_metric_set: {},
        users: {}
      }
      item_presenter = stub(association_preloads: preloads)
      url_builder = stub
      @presenter = Api::V2::Exports::IncrementalTicketPresenter.new(
        @account.owner,
        item_presenter: item_presenter,
        url_builder: url_builder
      )

      assert_equal preloads, @presenter.association_preloads
    end
  end

  describe 'scrubbed tickets' do
    let(:ticket) { tickets(:minimum_1) }

    before do
      refute ticket.account.ticket_fields.custom.active.empty?
      @custom_field = ticket.account.ticket_fields.custom.active.detect do |field|
        field.is_a?(FieldTagger)
      end
      ticket.subject = Zendesk::Scrub::SCRUBBED_SUBJECT
      ticket.ticket_field_entries.create!(
        ticket_field: @custom_field,
        account: ticket.account,
        value: 'foo'
      )

      assert_not_nil(@custom_field.value(ticket))
      # Ticket.any_instance.stubs(:scrubbed?).returns(true)
      ticket.stubs(:scrubbed?).returns(true)

      url_builder = stub
      url_builder.stubs(:api_v2_ticket_url).returns('www.google.com')
      @presenter = Api::V2::Exports::IncrementalTicketPresenter.new(ticket.account.owner, url_builder: url_builder)
    end

    it 'removes scrubbed fields with Arturo prevent_deletion_if_churned' do
      Arturo.enable_feature!(:prevent_deletion_if_churned)
      assert_equal [], @presenter.model_json([ticket]).first[:tags]
      assert_equal 'X', @presenter.model_json([ticket]).first[:fields].detect { |h| h[:id] == @custom_field.id }[:value]
      assert_equal 'X', @presenter.model_json([ticket]).first[:custom_fields].detect { |h| h[:id] == @custom_field.id }[:value]
      assert            @presenter.model_json([ticket]).first[:custom_fields].all? { |h| h[:value].nil? || h[:value] == 'X' }
      assert_equal 'X', @presenter.model_json([ticket]).first[:recipient]
    end

    it 'doesnt remove scrubbed fields without prevent_deletion_if_churned' do
      Arturo.disable_feature!(:prevent_deletion_if_churned)
      refute_equal [], @presenter.model_json([ticket]).first[:tags]
      refute_equal 'X', @presenter.model_json([ticket]).first[:fields].detect { |h| h[:id] == @custom_field.id }[:value]
      refute_equal 'X', @presenter.model_json([ticket]).first[:custom_fields].detect { |h| h[:id] == @custom_field.id }[:value]
      refute_equal 'X', @presenter.model_json([ticket]).first[:recipient]
    end
  end

  describe '#redacted_fields' do
    before do
      @models = Array(@model)
      item_presenter = stub(model_json: {name: "derrick", remove: "secret"})
      redacted_fields = [:remove]
      url_builder = stub
      @presenter = Api::V2::Exports::IncrementalTicketPresenter.new(
        @account.owner,
        item_presenter: item_presenter,
        redacted_fields: redacted_fields,
        url_builder: url_builder
      )
    end

    it 'removes redacted fields' do
      refute @presenter.model_json(@models).first.keys.include?(:remove)
    end

    it 'does not remove other fields' do
      assert_equal @presenter.model_json(@models).first[:name], "derrick"
    end
  end

  describe '#model_json' do
    let(:url_builder) { stub('url_builder') }
    let(:item_presenter) { stub('item_presenter', model_json: {}) }
    let(:navigation) { nil }

    let(:presenter) do
      Api::V2::Exports::IncrementalTicketPresenter.new(
        @account.owner,
        url_builder: url_builder,
        item_presenter: item_presenter,
        navigation: navigation
      )
    end

    let(:models) { [@model] }

    it 'includes the ticket generated_timestamp' do
      assert_includes presenter.model_json(models).first, :generated_timestamp
    end

    it 'defers model presentation to the item_presenter' do
      item_presenter.expects(:model_json).with(@model).returns({}).once
      presenter.model_json(models)
    end

    describe 'time bound processing limits' do
      let(:models) { [@model, @model] }

      let(:result) do
        Timecop.freeze do
          presenter.model_json(models)
        end
      end

      describe_with_arturo_enabled :time_bound_incremental_api_processing do
        before do
          @old_limit = Api::V2::Exports::IncrementalTicketPresenter::MAX_PROCESSING_TIME
          Api::V2::Exports::IncrementalTicketPresenter.send(:remove_const, :MAX_PROCESSING_TIME)
          Api::V2::Exports::IncrementalTicketPresenter.const_set(:MAX_PROCESSING_TIME, -1)
        end

        after do
          Api::V2::Exports::IncrementalTicketPresenter.send(:remove_const, :MAX_PROCESSING_TIME)
          Api::V2::Exports::IncrementalTicketPresenter.const_set(:MAX_PROCESSING_TIME, @old_limit)
        end

        describe 'standard pagination' do
          it 'processes all records passed to it' do
            assert_equal models.count, result.count
          end
        end

        describe 'cursor pagination' do
          let(:navigation) { :cursor }

          it 'stops processing records at the time limit' do
            assert_equal 1, result.count
          end
        end
      end

      describe_with_arturo_disabled :time_bound_incremental_api_processing do
        it 'processes all records passed to it' do
          assert_equal models.count, result.count
        end
      end
    end
  end

  describe '#as_json' do
    let(:models) { [@model] }
    let(:url_builder) { stub('url_builder') }
    let(:item_presenter) { stub('item_presenter', model_json: {}, collection_key: :tickets) }
    let(:navigation) { nil }

    let(:presenter_options) do
      {
        url_builder: url_builder,
        navigation: navigation
      }
    end

    let(:presenter) do
      Api::V2::Exports::IncrementalTicketPresenter.new(
        @account.owner,
        presenter_options.merge(item_presenter: item_presenter)
      )
    end

    it 'includes the total number of presented tickets' do
      item_presenter.stubs(side_loads: {})
      assert_equal models.count, presenter.as_json(models)[:count]
    end

    it 'includes side load objects' do
      side_loads = {side_load: { a: :b }}
      item_presenter.expects(:side_loads).with(models).returns(side_loads)

      assert_equal side_loads[:side_load], presenter.as_json(models)[:side_load]
    end

    describe 'time bound processing limits' do
      let(:models) { [@model, @model] }

      let(:result) do
        Timecop.freeze do
          presenter.as_json(models)
        end
      end

      before do
        item_presenter.stubs(side_loads: {})
      end

      describe_with_arturo_enabled :time_bound_incremental_api_processing do
        before do
          @old_limit = Api::V2::Exports::IncrementalTicketPresenter::MAX_PROCESSING_TIME
          Api::V2::Exports::IncrementalTicketPresenter.send(:remove_const, :MAX_PROCESSING_TIME)
          Api::V2::Exports::IncrementalTicketPresenter.const_set(:MAX_PROCESSING_TIME, -1)
        end

        after do
          Api::V2::Exports::IncrementalTicketPresenter.send(:remove_const, :MAX_PROCESSING_TIME)
          Api::V2::Exports::IncrementalTicketPresenter.const_set(:MAX_PROCESSING_TIME, @old_limit)
        end

        describe 'standard pagination' do
          it 'processes all records passed to it' do
            assert_equal models.count, result[:count]
          end
        end

        describe 'cursor pagination' do
          let(:navigation) { :cursor }
          let(:item_presenter) { Api::V2::Tickets::TicketPresenter.new(@account.owner, presenter_options) }

          before do
            url_builder.stubs(params: {}, url_for: '', api_v2_ticket_url: '')
          end

          it 'stops processing records at the time limit' do
            assert_equal 1, result[:tickets].count
          end

          it 'sets the correct cursor for the limited result set' do
            assert_equal(
              Zendesk::CursorPagination::Cursor.new(
                min_timestamp: models.first.generated_timestamp.to_f,
                min_id: models.first.nice_id
              ).base64,
              result[:after_cursor]
            )
          end
        end
      end

      describe_with_arturo_disabled :time_bound_incremental_api_processing do
        it 'processes all records passed to it' do
          assert_equal models.count, result[:count]
        end
      end
    end
  end

  describe '#present' do
    before do
      @account.settings.customer_satisfaction = true
      @account.settings.save!

      # TWITTER-877 Fixing the test for now as the method now check for a main_app call due to external usage from channels gem.
      url_builder = stub_everything('url_builder', main_app: stub_everything)
      includes = [:groups, :metric_sets, :organizations, :sharing_agreements, :ticket_forms, :users]
      @presenter = Api::V2::Exports::IncrementalTicketPresenter.new(
        @account.owner,
        url_builder: url_builder,
        includes: includes
      )
    end

    describe 'tickets from database via Facebook post' do
      before do
        facebook_pages('minimum_facebook_page_1').tap do |page|
          page.account = @account
          page.save!
        end
        @model.via_id = ViaType.FACEBOOK_POST
        @ten_models = Ticket.all.to_a
        @ten_models.each do |model|
          model.via_id = ViaType.FACEBOOK_POST
        end
      end

      describe 'facebook' do
        describe "one ticket" do
          should_do_sql_queries(35..41) { @presenter.present(@model) }
        end

        describe "all tickets" do
          should_do_sql_queries(35..45) { @presenter.present(@ten_models) }
        end
      end
    end

    { 'mail' => ViaType.MAIL, 'web_form' => ViaType.WEB_FORM, 'twitter' => ViaType.TWITTER }.each do |name, id|
      before do
        @model.via_id = id
        @ten_models = Ticket.all.to_a
        @ten_models.each do |model|
          model.via_id = id
        end
      end

      describe "tickets from database via #{name}" do
        # the actual number of queries is not that important, but it shouldn't go up with the number of objects presented
        describe "one ticket" do
          should_do_sql_queries(35..41) { @presenter.present(@model) }
        end
        describe "all tickets" do
          should_do_sql_queries(35..43) { @presenter.present(@ten_models) }
        end

        describe "with satisfaction ratings" do
          before do
            @ten_models.each do |model|
              model.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
            end
          end

          should_do_sql_queries(35..43) { @presenter.present(@ten_models) }
        end
      end

      describe "tickets from archive via #{name}" do
        before do
          @ten_models.each do |model|
            model.reload
            archive_and_delete(model)
          end
        end

        # the actual number of queries is not that important, but it shouldn't go up with the number of objects presented
        describe "one ticket" do
          should_do_sql_queries(35..41) { @presenter.present(@model) }
        end

        describe "all tickets" do
          should_do_sql_queries(35..41) { @presenter.present(@ten_models) }
        end

        describe "with satisfaction ratings" do
          before do
            @ten_models.each do |model|
              model.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
            end
          end

          should_do_sql_queries(35..41) { @presenter.present(@ten_models) }
        end
      end
    end
  end
end
