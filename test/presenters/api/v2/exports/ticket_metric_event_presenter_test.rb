require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Exports::TicketMetricEventPresenter do
  fixtures :tickets,
    :users

  let(:ticket)     { tickets(:minimum_1) }
  let(:event_time) { Time.now }

  let(:gooddata_request?) { false }
  let(:instance_id)       { 1 }
  let(:metric)            { 'reply_time' }

  let(:presenter) do
    Api::V2::Exports::TicketMetricEventPresenter.new(users(:minimum_end_user),
      url_builder:      stub('url_builder'),
      gooddata_request: gooddata_request?)
  end

  describe '#model_json' do
    let(:event) do
      TicketMetric::Activate.create(
        account:     ticket.account,
        ticket:      ticket,
        instance_id: instance_id,
        metric:      metric,
        time:        event_time
      )
    end

    it 'presents the event properly' do
      assert_equal(
        {
          id:          event.id,
          ticket_id:   ticket.nice_id,
          metric:      'reply_time',
          instance_id: instance_id,
          type:        'activate',
          time:        event_time
        },
        presenter.model_json(event)
      )
    end

    describe 'and the request is from GoodData' do
      let(:gooddata_request?) { true }

      describe "and the event has the 'reply_time' metric" do
        let(:metric) { 'reply_time' }

        describe "and the event's 'instance_id' is one" do
          it "presents the metric as 'first_reply_time'" do
            assert_equal(
              'first_reply_time',
              presenter.model_json(event)[:metric]
            )
          end
        end

        describe "and the event's 'instance_id' is not one" do
          let(:instance_id) { 2 }

          it "presents the metric as 'next_reply_time'" do
            assert_equal(
              'next_reply_time',
              presenter.model_json(event)[:metric]
            )
          end
        end
      end

      %w[agent_work_time requester_wait_time].each do |metric_name|
        describe "and the event has the '#{metric_name}' metric" do
          let(:metric) { metric_name }

          it "presents the metric as '#{metric_name}'" do
            assert_equal(
              metric_name,
              presenter.model_json(event)[:metric]
            )
          end
        end
      end
    end

    describe 'and the ticket has been deleted' do
      before { ticket.delete }

      it "presents the 'ticket_id' as nil" do
        assert_nil presenter.model_json(event.reload)[:ticket_id]
      end
    end

    describe 'and the event has metadata' do
      before { event.stubs(:metadata).returns(metadata: true) }

      it 'includes the metadata' do
        assert presenter.model_json(event)[:metadata]
      end
    end
  end
end
