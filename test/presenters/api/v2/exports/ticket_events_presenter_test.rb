require_relative "../../../../support/test_helper"
require_relative "../../../../support/rule"

SingleCov.covered!

describe Api::V2::Exports::TicketEventsPresenter do
  extend Api::Presentation::TestHelper
  include TestSupport::Rule::Helper

  let(:account) { accounts(:minimum) }

  fixtures :accounts, :rules, :users, :events, :tickets

  let(:satisfaction_score_audit) do
    { id: 15,
      author_id: -1,
      created_at: '2011-08-28 04:00:31 UTC',
      via_id: 8,
      value_previous: '{}',
      ticket: { id: 15, nice_id: 15, brand_id: nil },
      events: [
        { id: 15,
          is_public: 0,
          type: 'Change',
          value: '1',
          value_previous: '0',
          value_reference: 'satisfaction_score'}
      ]}
  end

  it 'is able to present a satisfaction score audit without blowing up' do
    presenter = Api::V2::Exports::TicketEventsPresenter.new(
      users(:minimum_end_user),
      url_builder: mock_url_builder
    )
    assert_not_nil presenter.model_json(satisfaction_score_audit)[:child_events].first['satisfaction_score']
  end

  describe 'merged ticket events' do
    let(:presenter) do
      Api::V2::Exports::TicketEventsPresenter.new(
        users(:minimum_end_user),
        url_builder: mock_url_builder
      )
    end

    let(:audits) do
      [
        { id: 15,
          author_id: 1,
          created_at: '2011-08-28 04:00:31 UTC',
          via_id: 1,
          value_previous: '{}',
          type: 'TicketMergeAudit',
          value: "1,9",
          ticket: { id: 15, nice_id: 15, brand_id: nil },
          events: [
            { id: 20,
              is_public: 0,
              author_id: 21,
              type: 'Comment',
              value: 'This is a comment',
              value_previous: nil,
              value_reference: nil}
          ]},
        { id: 16,
          author_id: 1,
          created_at: '2011-08-28 04:00:31 UTC',
          via_id: 1,
          value_previous: '{}',
          type: 'TicketMergeAudit',
          value: nil,
          ticket: { id: 15, nice_id: 15, brand_id: nil },
          events: [
            { id: 20,
              is_public: 0,
              author_id: 21,
              type: 'Comment',
              value: 'This is a comment',
              value_previous: nil,
              value_reference: nil}
          ]}
      ]
    end

    it 'should return the merged ticket ids in the audit' do
      presented = presenter.model_json(audits.find { |audit| audit[:id] == 15 })

      assert_equal [1, 9], presented[:merged_ticket_ids]
    end

    it 'should return empty array in merged ticket ids field if the audit event value is null ' do
      presented = presenter.model_json(audits.find { |audit| audit[:id] == 16 })

      assert_empty presented[:merged_ticket_ids]
    end
  end

  describe 'presenting ticket sharing events' do
    let(:presenter) do
      Api::V2::Exports::TicketEventsPresenter.new(
        users(:minimum_end_user),
        url_builder: mock_url_builder
      )
    end

    let(:audits) do
      [
        { id: 10002,
          ticket: { id: 10091, nice_id: 4, brand_id: nil },
          created_at: "2017-09-11T05:54:01Z",
          author_id: 10001,
          events: [
            { id: 10007,
              type: "TicketSharingEvent",
              value: 10002}
          ]},
        { id: 11136,
          ticket: { id: 10091, nice_id: 4, brand_id: nil },
          created_at: "2017-09-11T08:19:29Z",
          author_id: 10001,
          events: [
            { id: 11141,
              type: "TicketUnshareEvent",
              value: 10002}
          ]}
      ]
    end

    it 'presents share event details' do
      presented_sharing_event = presenter.model_json(audits.find { |audit| audit[:id] == 10002 })
      presented_unshare_event = presenter.model_json(audits.find { |audit| audit[:id] == 11136 })

      assert_equal "shared", presented_sharing_event[:child_events][0][:action]
      assert_equal "unshared", presented_unshare_event[:child_events][0][:action]
      assert_equal 10007, presented_sharing_event[:child_events][0][:id]
      assert_equal 11141, presented_unshare_event[:child_events][0][:id]
    end
  end

  describe 'presenting multiple audits' do
    let(:presenter) do
      Api::V2::Exports::TicketEventsPresenter.new(
        users(:minimum_end_user),
        url_builder: mock_url_builder
      )
    end

    let(:audits) do
      [
        { id: 15,
          author_id: 1,
          created_at: '2011-08-28 04:00:31 UTC',
          via_id: 1,
          value_previous: '{}',
          type: 'Audit',
          ticket: { id: 15, nice_id: 15, brand_id: nil },
          events: [
            { id: 20,
              is_public: 0,
              author_id: 21,
              type: 'Comment',
              value: 'This is a comment',
              value_previous: nil,
              value_reference: nil}
          ]},
        { id: 17,
          author_id: 1,
          created_at: '2011-08-28 04:00:31 UTC',
          via_id: 1,
          value_previous: '{}',
          type: 'Audit',
          ticket: { id: 15, nice_id: 15, brand_id: nil },
          events: [
            { id: 18,
              is_public: 0,
              type: 'Change',
              value: '4',
              value_previous: '1',
              value_reference: 'status_id'}
          ]}
      ]
    end

    it 'presentings one audit should not affect another' do
      presented_with_comment = presenter.model_json(audits.find { |audit| audit[:id] == 15 })
      presented_without_comment = presenter.model_json(audits.find { |audit| audit[:id] == 17 })

      assert_equal 21, presented_with_comment[:updater_id]
      assert_equal 1,  presented_without_comment[:updater_id]
    end

    it 'includes types and previous values when appropriate' do
      presented_audit = presenter.model_json(audits.first)

      assert_equal "Audit", presented_audit[:event_type]
    end
  end

  describe 'presenting child events' do
    let(:user) { users(:minimum_end_user) }

    let(:events) do
      [
        {
          id:              18,
          is_public:       0,
          type:            'Change',
          value:           '4',
          value_previous:  '1',
          value_reference: 'status_id'
        },
        {
          id:              15,
          is_public:       0,
          type:            'Change',
          value:           '1',
          value_previous:  '0',
          value_reference: 'status_id'
        },
        {
          id:              20,
          is_public:       0,
          author_id:       21,
          type:            'Comment',
          value:           'This is a comment',
          value_previous:  nil,
          value_reference: nil
        }
      ]
    end

    let(:child_events) do
      presenter.present(trigger_audit)[:ticket_events][:child_events]
    end

    let(:ticket_hash) { {id: 15, nice_id: 15, brand_id: nil} }

    let(:trigger_audit) do
      {
        id:             15,
        author_id:      1,
        created_at:     '2011-08-28 04:00:31 UTC',
        via_id:         1,
        value_previous: '{}',
        ticket:         ticket_hash,
        ticket_id:      ticket_hash[:id],
        account_id:     2,
        events:         events
      }
    end

    let(:presenter) do
      Api::V2::Exports::TicketEventsPresenter.new(
        user,
        url_builder: mock_url_builder
      )
    end

    it 'orders the child events by ID' do
      assert_equal [15, 18, 20], child_events.map { |event| event[:id] }
    end

    it 'includes the event types' do
      assert_equal(
        %w[Change Change Comment],
        child_events.map { |event| event[:event_type] }
      )
    end

    it 'includes previous values when appropriate' do
      assert_equal(
        ['new', 'open', nil],
        child_events.map { |event| event[:previous_value] }
      )
    end

    describe 'when the events are comments' do
      let(:comment) { Comment.where(account_id: account.id).last }
      let(:ticket)  { comment.ticket }

      let(:event) do
        {
          id:              comment.id,
          is_public:       0,
          type:            'Comment',
          value:           'This is a comment',
          value_previous:  nil,
          value_reference: nil,
          via_id:          ViaType.WEB_FORM
        }
      end

      let(:events) { [event] }

      let(:ticket_hash) do
        {id: ticket.id, nice_id: ticket.nice_id, brand_id: nil}
      end

      let(:presented_event) do
        child_events.find { |child_event| child_event[:id] == event[:id] }
      end

      let(:presenter) do
        Api::V2::Exports::TicketEventsPresenter.new(
          user,
          url_builder:     mock_url_builder,
          include_comment: include_comment
        )
      end

      describe 'and `include_comment` is true' do
        let(:include_comment) { true }

        let(:comment_presenter) do
          Api::V2::Tickets::CommentCollectionPresenter.new(
            user,
            url_builder:            mock_url_builder,
            without_metadata:       true,
            deleted_tickets_lookup: true
          )
        end

        it 'presents the comment' do
          assert_equal(
            comment_presenter.model_json(comment).first.merge(
              event_type:       'Comment',
              via_reference_id: nil
            ),
            presented_event
          )
        end

        describe 'and the ticket is archived' do
          let(:archived_audit) do
            Audit.instantiate_from_archive(trigger_audit.dup.stringify_keys)
          end

          let(:archived_comment) do
            Comment.
              instantiate_from_archive(event.dup.stringify_keys).
              tap do |comment|
                comment.association(:audit).target = archived_audit
              end
          end

          let(:audit_hash_with_voice_comment) do
            author_id = Comment.where(account_id: account.id).last.author_id

            audit_event_hash = {
              id: 11462,
              account_id: 1,
              parent_id: 11457,
              ticket_id: 10082,
              author_id: author_id,
              created_at: Time.parse("2018-08-13 05:15:58 +0000"),
              via_id: 33,
              via_reference_id: nil,
              is_public: 0,
              type: "VoiceComment",
              value: "some value",
              value_previous: "previous value",
              value_reference: nil,
              updated_at: Time.parse("2018/08/13 05:15:58 +0000"),
              notification_sent_at: nil
            }

            audit_ticket_hash = {
              id: 10082,
              nice_id: 50,
              account_id: 1,
              requester_id: author_id,
              assignee_id: author_id,
              organization_id: 10001,
              linked_id: nil,
              external_id: nil,
              group_id: 10001,
              subject: nil,
              generated_timestamp: Time.parse("2018-08-13 05:15:58 UTC"),
              created_at: Time.parse("2018-08-13 05:15:58 UTC"),
              updated_at: Time.parse("2018-08-13 05:15:58 UTC"),
              status_id: 1,
              ticket_type_id: 0,
              description: "Voicemail from: +16617480240",
              brand_id: 10001,
              is_public: 0
            }

            audit_hash = {
              id: 11457,
              account_id: 1,
              parent_id: nil,
              ticket_id: 10082,
              author_id: author_id,
              created_at: Time.parse("2018-08-13 05:15:58 +0000"),
              via_id: 19,
              via_reference_id: nil,
              is_public: 0,
              type: "TicketMergeAudit",
              value: nil,
              value_previous: "{\"system\":{\"ip_address\":\"127.0.0.1\"},\"custom\":{}}",
              value_reference: nil,
              updated_at: Time.parse("2018/08/13 05:15:58 +0000"),
              notification_sent_at: nil,
              events: [audit_event_hash],
              ticket: audit_ticket_hash,
              source: :archive
            }

            audit_hash
          end

          before { archive_and_delete(ticket) }

          it 'presents the archived comment' do
            assert_equal(
              comment_presenter.model_json(archived_comment).first.merge(
                event_type:       'Comment',
                via_reference_id: nil
              ),
              presented_event
            )
          end

          it 'preloads associated ticket \
             when account has_ticket_events_presenter_preload_ticket?' do
            Account.any_instance.stubs(
              has_ticket_events_presenter_preload_ticket?: true
            )
            presenter.preload_associations([audit_hash_with_voice_comment])
            presented = presenter.model_json(audit_hash_with_voice_comment)
            assert_equal(presented.class, Hash)
          end
        end
      end

      describe 'and `include_comment` is false' do
        let(:include_comment) { false }

        it 'presents the comment details' do
          assert_equal(
            {
              id:               event[:id],
              comment_present:  true,
              comment_public:   false,
              event_type:       'Comment',
              via:              'Web form',
              via_reference_id: nil
            },
            presented_event
          )
        end
      end
    end

    describe 'when the events are associated with rules' do
      let(:rule) { rules(:trigger_notify_requester_of_received_request) }

      let(:presented_event) do
        child_events.find { |event| event[:id] == 21 }
      end

      let(:events) do
        [
          {
            id:               21,
            is_public:        0,
            author_id:        21,
            type:             'Change',
            value:            {},
            value_previous:   {},
            value_reference:  'current_tags',
            via_id:           ViaType.RULE,
            via_reference_id: rule.id
          }
        ]
      end

      it 'sets `via` to `Rule`' do
        assert_equal ViaType.to_s(ViaType.RULE), presented_event[:via]
      end

      it 'includes the rule type as the `rel` attribute' do
        assert_equal rule.rule_type, presented_event[:rel]
      end

      describe 'when there are multiple rule-based events' do
        let(:other_rule) { rules(:automation_close_ticket) }

        let!(:events) do
          [
            {
              id:               21,
              is_public:        0,
              author_id:        21,
              type:             'Change',
              value:            {},
              value_previous:   {},
              value_reference:  'current_tags',
              via_id:           ViaType.RULE,
              via_reference_id: rule.id
            },
            {
              id:               22,
              is_public:        0,
              author_id:        21,
              type:             'Change',
              value:            {},
              value_previous:   {},
              value_reference:  'current_tags',
              via_id:           ViaType.RULE,
              via_reference_id: other_rule.id
            }
          ]
        end

        it 'does not perform N+1 requests' do
          assert_no_n_plus_one do
            presenter.present(trigger_audit)
          end
        end
      end
    end

    describe 'when events are associated with rule revisions' do
      let(:revision) { create_revision }

      let(:presented_event) do
        child_events.find { |event| event[:id] == 21 }
      end

      let(:events) do
        [
          {
            id:               21,
            is_public:        0,
            author_id:        21,
            type:             'Change',
            value:            {},
            value_previous:   {},
            value_reference:  'current_tags',
            via_id:           ViaType.RULE_REVISION,
            via_reference_id: revision.id
          }
        ]
      end

      it 'includes the rule type as the `rel` attribute' do
        assert_equal revision.trigger.rule_type, presented_event[:rel]
      end

      it 'includes the revision nice ID as the `revision_id`' do
        assert_equal revision.nice_id, presented_event[:revision_id]
      end

      it 'includes the rule ID as the `via_reference_id`' do
        assert_equal revision.trigger.id, presented_event[:via_reference_id]
      end

      it 'sets `via` to `Rule`' do
        assert_equal(
          ViaType.to_s(ViaType.RULE),
          presented_event[:via]
        )
      end

      describe 'when there are multiple revision-based events' do
        let(:other_revision) { create_revision }

        let!(:events) do
          [
            {
              id:               21,
              is_public:        0,
              author_id:        21,
              type:             'Change',
              value:            {},
              value_previous:   {},
              value_reference:  'current_tags',
              via_id:           ViaType.RULE_REVISION,
              via_reference_id: revision.id
            },
            {
              id:               22,
              is_public:        0,
              author_id:        21,
              type:             'Change',
              value:            {},
              value_previous:   {},
              value_reference:  'priority_id',
              via_id:           ViaType.RULE_REVISION,
              via_reference_id: other_revision.id
            }
          ]
        end

        it 'does not perform N+1 requests' do
          assert_no_n_plus_one do
            presenter.present(trigger_audit)
          end
        end
      end
    end

    describe 'when an event contains a custom ticket field value reference' do
      let(:presented_event) do
        child_events.find { |event| event[:id] == 21 }
      end

      let(:events) do
        [
          {
            id:              21,
            is_public:       0,
            author_id:       21,
            type:            'Change',
            value:           'value',
            value_previous:  {},
            value_reference: '324'
          }
        ]
      end

      it 'returns the custom ticket fields mapped by ID' do
        assert_equal(
          {324 => 'value'},
          presented_event[:custom_ticket_fields]
        )
      end
    end

    describe 'when an event contains a mapped value reference' do
      %w[
        ticket_type_id
        priority_id
        status_id
        satisfaction_score
        subject
      ].each do |value_reference|
        describe "and the value reference is `#{value_reference}`" do
          let(:presented_event) do
            child_events.find { |event| event[:id] == 123 }
          end

          let(:unmapped_value)   { stub('unmapped_value') }
          let(:mapped_value)     { stub('mapped_value') }
          let(:mapped_reference) { stub('mapped_reference') }

          let(:events) do
            [
              {
                id:              123,
                is_public:       0,
                author_id:       21,
                type:            'Create',
                value:           unmapped_value,
                value_previous:  {},
                value_reference: value_reference
              }
            ]
          end

          before do
            presenter.
              stubs(:ticket_attribute_name).
              with(value_reference).
              returns(mapped_reference)

            presenter.
              stubs(:ticket_attribute_value).
              with(value_reference, unmapped_value, account).
              returns(mapped_value)
          end

          it 'presents the ticket attribute mapped values' do
            assert_equal mapped_value, presented_event[mapped_reference]
          end
        end
      end
    end

    describe 'when an event contains a hash value reference after scrubbing' do
      let(:events) do
        [
          {
            id:              21,
            is_public:       0,
            author_id:       21,
            type:            'FacebookComment',
            value:           'value',
            value_previous:  {},
            value_reference: { 'X' => 'X' }
          }
        ]
      end

      it 'does not raise an exception' do
        refute_empty child_events
      end
    end

    describe 'when an event contains an ID-based reference' do
      %w[
        requester_id
        group_id
        assignee_id
      ].each do |value_reference|
        describe "and the value reference is `#{value_reference}`" do
          let(:presented_event) do
            child_events.find { |event| event[:id] == 123 }
          end

          let(:events) do
            [
              {
                id:              123,
                is_public:       0,
                author_id:       21,
                type:            'Create',
                value:           '1776',
                value_previous:  {},
                value_reference: value_reference
              }
            ]
          end

          it 'presents the value as an integer' do
            assert_equal 1776, presented_event[value_reference.to_sym]
          end
        end
      end
    end

    describe 'when an event contains an other value reference' do
      let(:presented_event) do
        child_events.find { |event| event[:id] == 123 }
      end

      let(:value_reference) { 'value_reference' }

      let(:events) do
        [
          {
            id:              123,
            is_public:       0,
            author_id:       21,
            type:            'Create',
            value:           'value',
            value_previous:  {},
            value_reference: value_reference
          }
        ]
      end

      it 'presents the value' do
        assert_equal 'value', presented_event[value_reference.to_sym]
      end
    end

    describe 'when an event contains an ignored value reference' do
      %w[satisfaction_comment current_collaborators].each do |value_reference|
        describe "and the value reference is `#{value_reference}`" do
          let(:events) do
            [
              {
                id:              123,
                is_public:       0,
                author_id:       21,
                type:            'Change',
                value:           'value',
                value_previous:  {},
                value_reference: value_reference
              }
            ]
          end

          it 'is not presented' do
            refute child_events.any? { |event| event[:id] == 123 }
          end
        end
      end
    end

    describe 'where an audit contains a comment' do
      it 'sets the audit updater to the comment author' do
        comment_event = trigger_audit[:events].detect { |e| e[:type] == 'Comment' }
        presented_audit = presenter.model_json(trigger_audit)

        refute_equal presented_audit[:updater_id], trigger_audit[:author_id]
        assert_equal presented_audit[:updater_id], comment_event[:author_id]
      end

      it 'does not include comment by default' do
        comment_event = trigger_audit[:events].detect { |e| e[:type] == 'Comment' }
        child_event = presenter.model_json(trigger_audit)[:child_events].find do |e|
          e[:id] == comment_event[:id]
        end
        assert child_event
        refute child_event.key?(:body)
      end
    end
  end

  describe 'presenting tags' do
    let(:tag_audit) do
      { id: 10,
        created_at: '2011-08-28 04:00:31 UTC',
        via_id: 8,
        value_previous: '{}',
        ticket: { id: 15, nice_id: 15, brand_id: nil },
        ticket_id: 15,
        account_id: 2,
        events: [
          { id: 131,
            account_id: 2,
            parent_id: 10,
            ticket_id: 15,
            author_id: 7,
            created_at: '2011-08-28 04:00:31 UTC',
            via_id: 0,
            via_reference_id: nil,
            is_public: 0,
            type: "Change",
            value: @value,
            value_previous: @value_previous,
            value_reference: "current_tags"},
        ]}
    end

    let(:presenter) do
      Api::V2::Exports::TicketEventsPresenter.new(
        users(:minimum_end_user),
        url_builder: mock_url_builder
      )
    end

    it 'is fine with adding the first tag' do
      @value_previous = nil
      @value = 'new'

      result = presenter.model_json(tag_audit)[:child_events].first
      assert_empty result[:removed_tags]
      assert_equal ['new'], result[:added_tags]
      assert_equal ['new'], result[:tags]
    end

    it 'adds the current tags for the ticket' do
      @value_previous = 'old'
      @value = 'old new awesome'

      result = presenter.model_json(tag_audit)[:child_events].first
      assert_empty result[:removed_tags]
      assert_equal ['old', 'new', 'awesome'], result[:tags]
    end

    it 'adds the tags added to the ticket' do
      @value_previous = 'old'
      @value = 'old new awesome'

      result = presenter.model_json(tag_audit)[:child_events].first
      assert_empty result[:removed_tags]
      assert_equal ['new', 'awesome'], result[:added_tags]
    end

    it 'adds the tags removed from the ticket' do
      @value_previous = 'old'
      @value = 'new awesome'

      result = presenter.model_json(tag_audit)[:child_events].first
      assert_equal ['old'], result[:removed_tags]
      assert_equal ['new', 'awesome'], result[:added_tags]
    end
  end

  describe "presenting metadata" do
    let(:location_audit) do
      { id: 10,
        created_at: '2011-08-28 04:00:31 UTC',
        via_id: 8,
        ticket: { id: 15, nice_id: 15, brand_id: nil },
        ticket_id: 15,
        account_id: 2,
        value_previous: metadata,
        events: []}
    end

    let(:metadata) { JSON.dump(system: @system_metadata) }

    let(:presenter) do
      Api::V2::Exports::TicketEventsPresenter.new(
        users(:minimum_end_user),
        url_builder: mock_url_builder
      )
    end

    it 'is fine if metadata is missing' do
      no_metadata_audit = location_audit.merge(value_previous: nil)
      result = presenter.model_json(no_metadata_audit)

      assert_nil result[:system].fetch(:location)
    end

    it 'does not include ip_address' do
      @system_metadata = { ip_address: '127.0.0.1' }
      result = presenter.model_json(location_audit)

      assert_raises(KeyError) { result[:system].fetch(:ip_address) }
    end

    it 'is fine if location information is missing from metadata' do
      @system_metadata = { client: 'browser info' }
      result = presenter.model_json(location_audit)

      assert_nil result[:system].fetch(:location)
    end

    it 'adds location information to the result' do
      @system_metadata = {
        location: 'San Francisco, CA, United States',
        latitude: 37.77580,
        longitude: -122.4128
        }
      result = presenter.model_json(location_audit)

      assert_equal 'San Francisco, CA, United States', result[:system][:location]
      assert_equal 37.77580, result[:system][:latitude]
      assert_equal(-122.4128, result[:system][:longitude])
    end

    describe 'with badly formed metadata' do
      let(:metadata) { 'CERTAINLY NOT VALID JSON' }
      it 'does not blow up' do
        result = presenter.model_json(location_audit)
        assert_nil result[:system][:location]
      end
    end

    it 'includes a created_at timestamp' do
      assert_equal '2011-08-28 04:00:31 UTC', presenter.model_json(location_audit)[:created_at]
    end
    it 'adds client information' do
      client = 'my awesome browser (10.1)'
      @system_metadata = { client: client }
      result = presenter.model_json(location_audit)

      assert_equal client, result[:system][:client]
    end
  end

  describe 'when url_builder has include_comment option' do
    let(:comment) do
      Comment.where(account_id: account.id).last
    end

    let(:trigger_audit) do
      last_comment_audit = comment.audit
      last_comment_audit.update_column :value_previous, JSON.dump(system: { latitude: 3 }, custom: { foo: 'bar' })
      make_audit_hash_from_audit(last_comment_audit)
    end

    before do
      @presenter = Api::V2::Exports::TicketEventsPresenter.new(
        users(:minimum_end_user),
        url_builder: mock_url_builder,
        include_comment: true
      )
    end

    it 'includes comment' do
      # Need to call #present here
      res = @presenter.present(trigger_audit)

      child_event = res[:ticket_events][:child_events].find do |e|
        e[:id] == comment.id
      end
      assert child_event
      assert child_event.key?(:body)
      assert_equal comment.body, child_event[:body]
    end

    it 'has type in comment' do
      res = @presenter.present(trigger_audit)
      child_event = res[:ticket_events][:child_events].find do |e|
        e[:id] == comment.id
      end
      assert child_event
      assert_equal 'Comment', child_event[:type]
      assert_nil child_event[:previous_value]
    end

    it 'adds metadata' do
      audit_json = @presenter.present(trigger_audit)[:ticket_events]
      assert audit_json.key?(:metadata)

      metadata = { system: { latitude: 3 }, custom: { foo: 'bar' } }.with_indifferent_access
      assert_equal metadata, audit_json[:metadata]
    end

    it 'preserves system metadata' do
      audit_json = @presenter.present(trigger_audit)[:ticket_events]
      assert audit_json.key?(:system)
    end

    describe 'for multiple audits including comments' do
      let(:audits) do
        last_comment_audit = Comment.where(account_id: account.id).last.audit
        first_comment_audit = Comment.where(account_id: account.id).last.audit

        if last_comment_audit.id == first_comment_audit
          raise "we should have 2 different audits for this test"
        end

        [
          make_audit_hash_from_audit(last_comment_audit),
          make_audit_hash_from_audit(first_comment_audit)
        ]
      end

      describe_with_and_without_arturo_enabled :email_ccs_preload_notifications_with_ccs do
        it 'makes only one SQL request when fetching comments' do
          ary = audits
          assert_sql_queries 1, /SELECT `events`.*Comment/ do
            @presenter.present(ary)
          end
        end

        it 'does not do n+1 requests' do
          ary = audits
          assert_no_n_plus_one do
            res = @presenter.present(ary)
            refute res[:ticket_events].empty?
          end
        end
      end
    end

    describe 'for multiple audits including comments from FB' do
      let(:audits) do
        last_comment_audit = Comment.where(account_id: account.id).last.audit
        first_comment_audit = Comment.where(account_id: account.id).last.audit

        if last_comment_audit.id == first_comment_audit
          raise "we should have 2 different audits for this test"
        end

        [
          make_audit_hash_from_audit(last_comment_audit, via_id: ViaType.FACEBOOK_POST),
          make_audit_hash_from_audit(first_comment_audit, via_id: ViaType.FACEBOOK_POST)
        ]
      end

      it 'makes only one more SQL request when fetching comments' do
        ary = audits
        queries = sql_queries do
          @presenter.present(ary)
        end

        assert_equal 1, queries.grep(/SELECT `events`.*Comment/).size, queries.join("\n")
        assert_equal 0, queries.grep(/SELECT `events`.*Event/).size, queries.join("\n")
      end

      it 'does not do n+1 requests' do
        ary = audits
        assert_no_n_plus_one do
          res = @presenter.present(ary)
          refute res[:ticket_events].empty?
        end
      end
    end
  end

  def make_audit_hash_from_audit(audit, via_id: nil)
    h = audit.as_archived.with_indifferent_access
    h[:via_id] = via_id if via_id
    h[:events] = audit.events.map(&:as_archived)
    if via_id
      h[:events].map! { |e| e[:via_id] = via_id; e }
    end

    ticket = audit.ticket
    h[:ticket] = { id: ticket.id, nice_id: ticket.nice_id }

    h
  end
end
