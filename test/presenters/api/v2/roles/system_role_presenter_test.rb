require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Roles::SystemRolePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @presenter = Api::V2::Roles::SystemRolePresenter.new(@account.owner, url_builder: mock_url_builder)
    @model = Role::ADMIN.id
  end

  should_present_keys :id, :name, :description, :created_at, :updated_at, :configuration

  it "contains a well formed configuration" do
    expected = {
      chat_access: true,
      end_user_profile_access: "full",
      end_user_list_access: "full",
      forum_access: "full",
      forum_access_restricted_content: true,
      light_agent: false,
      macro_access: "full",
      manage_business_rules: true,
      manage_dynamic_content: true,
      manage_extensions_and_channels: true,
      manage_facebook: true,
      organization_editing: true,
      organization_notes_editing: true,
      report_access: "full",
      side_conversation_create: true,
      ticket_access: "all",
      ticket_comment_access: "public",
      ticket_deletion: true,
      view_deleted_tickets: true,
      ticket_editing: true,
      ticket_merge: true,
      ticket_tag_editing: true,
      twitter_search_access: true,
      view_access: "full",
      user_view_access: "full",
      voice_access: true,
      moderate_forums: false,
      group_access: true
    }

    assert_equal expected, @presenter.model_json(@model)[:configuration]
  end

  describe_with_arturo_enabled :present_ticket_bulk_edit_permission do
    it "contains a well formed configuration" do
      expected = {
        chat_access: true,
        end_user_profile_access: "full",
        end_user_list_access: "full",
        forum_access: "full",
        forum_access_restricted_content: true,
        light_agent: false,
        macro_access: "full",
        manage_business_rules: true,
        manage_dynamic_content: true,
        manage_extensions_and_channels: true,
        manage_facebook: true,
        organization_editing: true,
        organization_notes_editing: true,
        report_access: "full",
        side_conversation_create: true,
        ticket_access: "all",
        ticket_comment_access: "public",
        ticket_deletion: true,
        view_deleted_tickets: true,
        ticket_editing: true,
        ticket_merge: true,
        ticket_tag_editing: true,
        twitter_search_access: true,
        ticket_bulk_edit: true,
        view_access: "full",
        user_view_access: "full",
        voice_access: true,
        moderate_forums: false,
        group_access: true
      }

      assert_equal expected, @presenter.model_json(@model)[:configuration]
    end
  end

  describe "user_view_access" do
    before do
      @user = users(:minimum_agent)
      @presenter = Api::V2::Roles::SystemRolePresenter.new(@user, url_builder: mock_url_builder)
      @model = @user.roles
    end

    it "presents non restricted agents" do
      assert_equal "manage-personal", @presenter.model_json(@model)[:configuration][:user_view_access]
    end

    it "presents restricted agents" do
      @user.restriction_id = 1
      assert_equal "none", @presenter.model_json(@model)[:configuration][:user_view_access]
    end
  end
end
