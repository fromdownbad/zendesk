require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Roles::CustomRolePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    Timecop.freeze
    @account = accounts(:minimum)
    @presenter = Api::V2::Roles::CustomRolePresenter.new(@account.owner, url_builder: mock_url_builder)
    @model = @account.permission_sets.create!(name: "Wibble", description: "Wobble")
    Account.any_instance.stubs(:has_permission_sets?).returns(true)
    @model.permissions.available_user_lists = "all"
    @model.permissions.save!
  end

  should_present_keys :id, :name, :description, :role_type, :created_at, :updated_at, :configuration

  it "contains a well formed configuration" do
    expected = {
      chat_access: true,
      end_user_profile_access: "full",
      end_user_list_access: "full",
      explore_access: "readonly",
      forum_access: "full",
      forum_access_restricted_content: true,
      light_agent: false,
      macro_access: "full",
      manage_business_rules: true,
      manage_dynamic_content: false,
      manage_extensions_and_channels: false,
      manage_facebook: false,
      organization_editing: true,
      organization_notes_editing: true,
      report_access: "full",
      side_conversation_create: true,
      ticket_access: "all",
      ticket_comment_access: "public",
      ticket_deletion: true,
      view_deleted_tickets: true,
      ticket_editing: true,
      ticket_merge: true,
      ticket_tag_editing: true,
      twitter_search_access: true,
      view_access: "full",
      user_view_access: "readonly",
      voice_access: true,
      moderate_forums: false,
      group_access: true
    }

    assert_equal expected, @presenter.model_json(@model)[:configuration]
  end

  describe_with_arturo_enabled :present_ticket_bulk_edit_permission do
    it "contains a well formed configuration" do
      expected = {
        chat_access: true,
        end_user_profile_access: "full",
        end_user_list_access: "full",
        explore_access: "readonly",
        forum_access: "full",
        forum_access_restricted_content: true,
        light_agent: false,
        macro_access: "full",
        manage_business_rules: true,
        manage_dynamic_content: false,
        manage_extensions_and_channels: false,
        manage_facebook: false,
        organization_editing: true,
        organization_notes_editing: true,
        report_access: "full",
        side_conversation_create: true,
        ticket_access: "all",
        ticket_comment_access: "public",
        ticket_deletion: true,
        view_deleted_tickets: true,
        ticket_editing: true,
        ticket_merge: true,
        ticket_tag_editing: true,
        ticket_bulk_edit: true,
        twitter_search_access: true,
        view_access: "full",
        user_view_access: "readonly",
        voice_access: true,
        moderate_forums: false,
        group_access: true
      }

      assert_equal expected, @presenter.model_json(@model)[:configuration]
    end
  end

  it "returns false for user_view_access if agent cannot view user lists" do
    @model.permissions.available_user_lists = "none"
    @model.permissions.save
    @model.save
    assert_equal "none", @presenter.model_json(@model)[:configuration][:user_view_access]
  end

  describe "presenting updated_at" do
    describe "when a setting has an updated_at more recent than the users's updated_at" do
      before { @model.updated_at = 10.minutes.ago }

      it "presents the setting's updated_at" do
        assert_equal @model.permissions.map(&:updated_at).max, @presenter.present(@model)[:custom_role][:updated_at]
      end
    end

    describe "when the permission_set's updated_at is the most recent" do
      before { @model.updated_at = 10.minutes.from_now }

      it "presents the setting's updated_at" do
        assert_equal @model.updated_at, @presenter.present(@model)[:custom_role][:updated_at]
      end
    end

    describe "when there are no permissions" do
      before { @model.permissions = [] }

      it "presents the permission_set's updated_at" do
        assert_equal @model.updated_at, @presenter.present(@model)[:custom_role][:updated_at]
      end
    end

    describe "when there is a permission with a nil updated_at" do
      before { @model.permissions.first.updated_at = nil }

      it "does not raise an exception" do
        @presenter.present(@model)
      end
    end
  end

  describe 'billing admin' do
    it "contains a well formed configuration" do
      expected = {
        chat_access: true,
        end_user_profile_access: "full",
        end_user_list_access: "full",
        explore_access: "full",
        forum_access: "full",
        forum_access_restricted_content: true,
        light_agent: false,
        macro_access: "full",
        manage_business_rules: true,
        manage_dynamic_content: true,
        manage_extensions_and_channels: true,
        manage_facebook: true,
        organization_editing: true,
        organization_notes_editing: true,
        report_access: "full",
        side_conversation_create: true,
        ticket_access: "all",
        ticket_comment_access: "public",
        ticket_deletion: true,
        view_deleted_tickets: true,
        ticket_editing: true,
        ticket_merge: true,
        ticket_tag_editing: true,
        twitter_search_access: true,
        view_access: "full",
        user_view_access: "full",
        voice_access: true,
        moderate_forums: false,
        group_access: true
      }

      billing_admin = PermissionSet.create_billing_admin!(@account)
      assert_equal expected, @presenter.model_json(billing_admin)[:configuration]
    end
  end
end
