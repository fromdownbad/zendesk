require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SessionRecordPresenter do
  fixtures :accounts, :users
  extend Api::Presentation::TestHelper

  let(:session_record) { FactoryBot.create(:session_record) }
  let(:account) { Account.find(session.account_id) }
  let(:user) { account.users.find(session.user_id) }
  before do
    @presenter = Api::V2::SessionRecordPresenter.new(session_record, url_builder: mock_url_builder)
    @model = session_record
  end

  should_present_keys :id, :url, :user_id, :authenticated_at, :last_seen_at
  should_present_method :id
  should_present_method :user_id

  it "presents url" do
    expected_url = "stub_api_v2_user_session_url"
    @presenter.url_builder.expects(:api_v2_user_session_url).with(@model.user_id, @model, has_entry(format: :json)).returns(expected_url)
    assert_equal expected_url, @presenter.model_json(@model)[:url]
  end

  it "presents authenticated_at" do
    assert_equal Time.at(@model.data['auth_authenticated_at']), @presenter.model_json(@model)[:authenticated_at]
  end

  it "presents last_seen_at" do
    assert_equal Time.at(@model.data['auth_updated_at']), @presenter.model_json(@model)[:last_seen_at]
  end
end
