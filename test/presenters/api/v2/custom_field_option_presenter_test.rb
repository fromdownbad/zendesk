require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::CustomFieldOptionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :cf_fields, :cf_dropdown_choices, :users

  def self.should_present_url
    it "presents url using api_v2_user_field_option_url" do
      expected_url = "stub_api_v2_user_field_option_url"
      @presenter.url_builder.expects(:api_v2_user_field_option_url).with(@model.field, @model, has_entry(format: :json)).returns(expected_url)
      assert_equal expected_url, @presenter.model_json(@model)[:url]
    end
  end

  before do
    @account = accounts(:minimum)
    @model = cf_dropdown_choices(:v100)

    options = {
      url_builder:  mock_url_builder,
      route:        :api_v2_user_field_option_url,
      parent:       'field'
    }

    @presenter = Api::V2::CustomFieldOptionPresenter.new(@account.owner, options)
  end

  should_present_keys :id, :url, :name, :raw_name, :position, :value
  should_present_methods :id, :name, :value, :position

  should_present_url

  describe "#model_json" do
    it "generates user JSON" do
      expected = {
        url:        :api_v2_user_field_option_url,
        id:         6,
        name:       "Something",
        raw_name:   "Something",
        position:   0,
        value:      "100"
      }

      assert_equal expected, @presenter.model_json(@model)
    end
  end
end
