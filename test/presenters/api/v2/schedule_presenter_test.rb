require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::SchedulePresenter do
  extend Api::Presentation::TestHelper

  let(:account) { accounts(:minimum) }

  before do
    @model = account.schedules.create(
      name: 'Test',
      time_zone: 'Pacific Time (US & Canada)'
    )

    @presenter = Api::V2::SchedulePresenter.new(account.owner,
      url_builder: mock_url_builder)
  end

  should_present_methods :id,
    :name,
    :time_zone,
    :created_at,
    :updated_at,
    :deleted_at

  should_present_url :api_v2_business_hours_schedule_url
end
