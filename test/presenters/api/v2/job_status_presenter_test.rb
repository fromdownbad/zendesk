require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::JobStatusPresenter do
  extend Api::Presentation::TestHelper

  class StatusPresenterTestJob < JobWithStatus
    priority :medium
    def work
      total = options[:numbers].count
      at(0, total, 'Begin')

      results = options[:numbers].each_with_index.map do |number, i|
        at(i + 1, total, "Working on number #{number} -- #{i + 1}/#{total}")
        {id: i, value: number, status: 'created'}
      end

      completed(results: results)
    end
  end

  fixtures :accounts, :users

  before do
    @account   = accounts(:minimum)
    @model     = Resque::Plugins::Status::Hash.new("1234uuid")
    @presenter = Api::V2::JobStatusPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :url, :status, :message, :results, :total, :progress

  should_present_method :message
  should_present_method :total
  should_present_method :num, as: :progress
  should_present_method :status
  should_present_method :uuid, as: :id

  should_present_url :api_v2_job_status_url, with: :uuid

  describe 'progress' do
    let(:account) { accounts(:minimum) }
    let(:key) { "Zendesk-JobWithStatusQueue-#{account.id}" }

    before do
      StatusPresenterTestJob.stubs(:perform) # don't want jobs to run right away
    end

    it 'does not have an off-by-one error' do
      uuid = StatusPresenterTestJob.enqueue(account_id: account.id, numbers: (1..5).to_a)
      job = StatusPresenterTestJob.new(uuid, accound_id: account.id, numbers: (1..5).to_a)

      job.work

      status = @presenter.present(Resque::Plugins::Status::Hash.get(uuid))
      job_status = status[:job_status]

      job_status[:total].must_equal 5
      job_status[:progress].must_equal 5
    end
  end
end
