require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::ProductCollectionPresenter do
  extend Api::Presentation::TestHelper

  before do
    lotus_item_mock = mock('lotus')
    lotus_item_mock.stubs(:key).returns('lotus')
    lotus_item_mock.stubs(:label).returns(nil)

    help_center_item_mock = mock('help_center')
    help_center_item_mock.stubs(:key).returns('help_center')
    help_center_item_mock.stubs(:label).returns('beta')

    @account_mock = mock('account')
    @account_mock.stubs(:spp?).returns(false)

    @products = [lotus_item_mock, help_center_item_mock]

    @presenter = Api::V2::ProductCollectionPresenter.new(@account_mock, users(:minimum_agent), url_builder: mock_url_builder)
  end

  it 'generates expected products output' do
    json = @presenter.present(@products)

    assert_equal(
      json[:products],
      [
        { key: 'lotus' },
        { key: 'help_center', label: 'beta' }
      ]
    )
  end

  describe 'when account is not spp' do
    it 'generates expected features output' do
      json = @presenter.present(@products)

      assert_equal(
        json[:features],
        { product_tray_enable_list_view: false }
      )
    end
  end

  describe 'when account is spp' do
    before do
      @account_mock.stubs(:spp?).returns(true)
    end
    it 'enables product_tray_enable_list_view' do
      json = @presenter.present(@products)

      assert_equal(
        json[:features],
        { product_tray_enable_list_view: true }
      )
    end
  end

  describe 'when account is not spp but arturo enabled' do
    before do
      @account_mock.stubs(:spp?).returns(false)
      Arturo.stubs(:feature_enabled_for?).with(:product_tray_enable_list_view, @account_mock).returns(true)
    end
    it 'enables product_tray_enable_list_view' do
      json = @presenter.present(@products)

      assert_equal(
        json[:features],
        { product_tray_enable_list_view: true }
      )
    end
  end
end
