require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::OrganizationRelatedPresenter do
  extend Api::Presentation::TestHelper
  fixtures :organizations

  before do
    @account   = accounts(:minimum)
    @model     = organizations(:serialization_organization)
    @presenter = Api::V2::OrganizationRelatedPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
  end

  should_present_keys :users_count, :tickets_count

  describe_with_arturo_disabled :enable_search_for_org_related do
    describe 'limits' do
      it 'limits count to COUNT_WITH_ARCHIVED_LIMIT' do
        Ticket.stubs(:count_with_archived).returns(150_000)
        result = @presenter.model_json(@model)
        assert_equal 100_000, result[:tickets_count]
      end

      it 'has the appropriate LIMIT sql queries' do
        regex = /LIMIT (\d+)\) subquery_for_count/

        sql = assert_sql_queries(2, regex) do
          @presenter.model_json(@model)
        end

        # based on the count from unarchived table, the limit is adjusted
        # on the archived stubs table
        limits = sql.map { |s| s.match(regex)[1].to_i }
        assert limits.all? { |l| l > 90000 && l <= 100000 }, "limit values: #{limits.inspect} should be close to 100000"
      end
    end
  end

  describe_with_arturo_enabled :enable_search_for_org_related do
    before do
    end

    describe 'limits' do
      it 'calls the search service to fetch counts' do
        ZendeskSearch::Client.any_instance.expects(:search).returns("count" => 0, "results" => [], "facets" => {"type" => {"ticket" => 8, "user" => 1}})

        result = @presenter.model_json(@model)
        assert_equal 1, result[:users_count]
        assert_equal 8, result[:tickets_count]
      end

      it 'falls back to sql if the search service errors out' do
        ZendeskSearch::Client.any_instance.expects(:search).raises(ZendeskSearch::QueryError, "Invalid type:agent")

        regex = /LIMIT (\d+)\) subquery_for_count/

        assert_sql_queries(2, regex) do
          @presenter.model_json(@model)
        end
      end
    end
  end
end
