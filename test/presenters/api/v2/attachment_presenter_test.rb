require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::AttachmentPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    model.stubs(:public_filename).returns("/somewhere/large_1.png")
    # Added to avoid the following error:
    # RuntimeError: You need to set up a @model and @presenter in your test to use the should_present_method
    # /bundle/gems/api_presentation-2.5.6/lib/api/presentation/minitest_helper.rb:27:in `block in should_present_method'
    @model = model
    @presenter = Api::V2::AttachmentPresenter.new(model.account.owner, url_builder: mock_url_builder)
    Account.any_instance.stubs(:is_ssl_enabled?).returns(true)
  end

  [Photo, Attachment, BrandLogo].each do |model_class|
    describe "for a #{model_class.name}" do
      let(:model) { get_model(model_class) }

      should_present_keys :deleted, :id, :file_name, :content_url, :mapped_content_url, :content_type, :size, :width, :height, :thumbnails, :url, :inline

      should_present_method :id
      should_present_method :display_filename, as: :file_name
      should_present_method :content_type
      should_present_method :size
      should_present_method :width
      should_present_method :height

      it "presents the thumbnails using the #thumbnails method" do
        thumbnails = stub(:thumbnails)
        @presenter.expects(:thumbnails).with(model).returns(thumbnails)
        json = @presenter.model_json(model)
        assert_equal thumbnails, json[:thumbnails]
      end

      describe "#thumbnails" do
        it "uses #model_json on each thumbnail" do
          model.thumbnails.each { |thumbnail| @presenter.expects(:model_json).with(thumbnail, false) }
          @presenter.thumbnails(model)
        end
      end

      describe "#model_json" do
        let(:model) { get_model(model_class) }

        it "does not output thumbnails when given false as the second argument" do
          @presenter.expects(:thumbnails).never
          json = @presenter.model_json(model, false)
          refute json.key?(:thumbnails)
        end
      end
    end
  end

  describe "for an Attachment" do
    let(:model) { get_model(Attachment) }

    it "presents a full secure mapped_content_url" do
      model.account.update_attribute(:host_mapping, "mapped.com")
      certificate = model.account.certificates.first
      certificate.update_attribute(:state, "active")
      certificate.update_attribute(:valid_until, Time.zone.now + 1.day)
      json = @presenter.model_json(model)
      assert_equal "https://#{model.account.host_mapping}#{model.url(relative: true)}", json[:mapped_content_url]
    end

    it "presents the file_name escaped" do
      json = @presenter.model_json(model)
      assert_equal "☃.txt", json[:file_name]
    end

    it "presents inline value of an attachment" do
      Attachment.any_instance.expects(:inline).returns(true)
      json = @presenter.model_json(model)
      assert(json[:inline])
    end

    it "presents a full secure content_url on the subdomain" do
      json = @presenter.model_json(model)
      assert_equal "https://#{@presenter.account.subdomain}.zendesk-test.com#{model.url(relative: true)}", json[:content_url]
    end

    describe "#model_json" do
      let(:model) { attachments(:foreign_file_attachment) }

      before { model.remove_from_remote! }

      it 'presents :deleted key' do
        json = @presenter.model_json(model)
        assert json.key?(:deleted)
        assert json[:deleted]
      end

      describe 'and attachment is an external url' do
        before { model.expects(:not_saved_in_stores?).returns(true) }

        it 'presents :deleted key with false' do
          json = @presenter.model_json(model)
          assert json.key?(:deleted)
          refute json[:deleted]
        end
      end
    end
  end

  [BrandLogo, Photo].each do |model_class|
    describe "for a #{model_class.name}" do
      let(:model) { get_model(model_class) }

      it "presents a full secure mapped_content_url" do
        model.account.update_attribute(:host_mapping, "mapped.com")
        certificate = model.account.certificates.first
        certificate.update_attribute(:state, "active")
        certificate.update_attribute(:valid_until, Time.zone.now + 1.day)
        json = @presenter.model_json(model)
        assert_equal "https://#{model.account.host_mapping}#{model.public_filename}", json[:mapped_content_url]
      end

      it "presents the file_name escaped" do
        json = @presenter.model_json(model)
        assert_equal "large_1.png", json[:file_name]
      end

      it "presents inline value of an attachment" do
        json = @presenter.model_json(model)
        assert_equal false, json[:inline]
      end

      it "presents a full secure content_url on the subdomain" do
        json = @presenter.model_json(model)
        assert_equal "https://#{@presenter.account.subdomain}.zendesk-test.com#{model.public_filename}", json[:content_url]
      end

      describe "#model_json" do
        it 'presents :deleted key' do
          json = @presenter.model_json(model)
          assert json.key?(:deleted)
          refute json[:deleted]
        end

        describe 'and attachment is an external url' do
          it 'presents :deleted key with false' do
            json = @presenter.model_json(model)
            assert json.key?(:deleted)
            refute json[:deleted]
          end
        end
      end
    end
  end

  private

  def get_model(model_class)
    if model_class == Photo
      FactoryBot.create(:photo, filename: "large_1.png")
    elsif model_class == BrandLogo
      FactoryBot.create(:brand_logo, filename: "large_1.png")
    else
      attachments(:foreign_file_attachment)
    end
  end
end
