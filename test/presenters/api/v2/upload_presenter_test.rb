require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::UploadPresenter do
  fixtures :accounts, :tokens, :attachments
  extend Api::Presentation::TestHelper

  before do
    @model = tokens(:minimum_upload_token)
    @presenter = Api::V2::UploadPresenter.new(users(:minimum_admin), url_builder: mock_url_builder)
  end

  should_present_keys :attachments, :token, :expires_at

  should_present_method :value, as: :token

  should_present_collection :attachments, with: Api::V2::AttachmentPresenter

  describe 'without an attachment' do
    it 'does not include an attachment key' do
      refute @presenter.model_json(@model).key?(:attachment)
    end
  end

  describe 'with an attachment' do
    before do
      @attachment = @model.attachments.first
      @presenter = Api::V2::UploadPresenter.new(users(:minimum_admin),
        url_builder: mock_url_builder,
        attachment: @attachment)
    end

    it 'includes the attachment' do
      json = @presenter.model_json(@model)
      assert_not_nil json[:attachment]
      assert_equal @attachment.id, json[:attachment][:id]
    end
  end
end
