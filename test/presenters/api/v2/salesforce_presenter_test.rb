require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SalesforcePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :tickets, :users, :groups, :organizations,
    :ticket_fields, :ticket_field_entries, :ticket_metric_sets

  before do
    @account = accounts(:minimum)
    @model = tickets(:minimum_2)
    @model.assignee = users(:minimum_agent)

    # Check if we have a ticket with the necessary data
    assert @model.requester
    assert @model.submitter
    assert @model.assignee
    assert @model.organization
    assert @model.group
    assert @model.ticket_metric_set
    assert @model.ticket_field_entries.present?
    assert @model.comments.present?
    assert @model.brand.present?

    Account.any_instance.stubs(:has_multibrand?).returns(true)

    @presenter = Api::V2::SalesforcePresenter.new(@account.owner, url_builder: mock_url_builder)
    @json = @presenter.present(@model)
  end

  it "presents the expected ticket data" do
    expected = [:id, :url, :type, :subject, :description,
                :priority, :status, :recipient, :group_id,
                :assignee_id, :organization_id, :fields, :brand_id]

    assert Set.new(expected).subset?(Set.new(@json[:ticket].keys))
  end

  it "presents the expected custom fields data" do
    expected = [:id, :title, :value]

    assert Set.new(expected).subset?(Set.new(@json[:ticket][:fields].first.keys))
  end

  it "presents the expected requester data" do
    requester = @json[:users].find { |user| user[:id] == @model.requester_id }

    expected = [:id, :name, :email]

    assert Set.new(expected).subset?(Set.new(requester.keys))
  end

  it "presents the expected submitter data" do
    submitter = @json[:users].find { |user| user[:id] == @model.submitter_id }

    expected = [:id, :name, :email]

    assert Set.new(expected).subset?(Set.new(submitter.keys))
  end

  it "presents the expected assignee data" do
    assignee = @json[:users].find { |user| user[:id] == @model.assignee_id }

    expected = [:id, :name, :email]

    assert Set.new(expected).subset?(Set.new(assignee.keys))
  end

  it "presents the expected group data" do
    expected = [:id, :name]

    assert Set.new(expected).subset?(Set.new(@json[:groups].first.keys))
  end

  it "presents the expected organization data" do
    expected = [:id, :name]

    assert Set.new(expected).subset?(Set.new(@json[:organizations].first.keys))
  end

  it "presents the expected metrics data" do
    expected = [:created_at, :updated_at, :solved_at, :initially_assigned_at]

    expected_with_business = [:reply_time_in_minutes, :full_resolution_time_in_minutes,
                              :agent_wait_time_in_minutes, :requester_wait_time_in_minutes,
                              :on_hold_time_in_minutes]

    assert Set.new(expected + expected_with_business).subset?(Set.new(@json[:metrics].keys))

    expected_with_business.each do |key|
      [:calendar, :business].include?(@json[:metrics][key].keys)
    end
  end

  it "presents the expected brands data" do
    expected = [:name]

    assert Set.new(expected).subset?(Set.new(@json[:brands].first.keys))
  end

  it "presents the expected comments data" do
    expected = [:id, :type, :body, :public, :author_id, :name, :email, :ticket_id, :created_at]

    assert Set.new(expected).subset?(Set.new(@json[:comments].first.keys))
  end
end
