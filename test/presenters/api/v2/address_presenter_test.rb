require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::AddressPresenter do
  extend Api::Presentation::TestHelper
  fixtures :addresses

  before do
    @account           = accounts(:minimum)
    @model             = addresses(:address1)
    @presenter         = Api::V2::AddressPresenter.new(@account.owner, url_builder: mock_url_builder)
    @country_presenter = Api::V2::CountryPresenter.new(@account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :city, :country, :phone, :state, :street, :vat, :zip

  should_present_method :city
  should_present_method :phone
  should_present_method :state
  should_present_method :street
  should_present_method :vat
  should_present_method :zip

  describe "when an address has a country" do
    before do
      @model.stubs(:country_id).returns(1)
      @presenter.stubs(:country_presenter).returns(@country_presenter)
    end

    it "uses the CountryPresenter" do
      @country_presenter.expects(:model_json).with(@model.country)
      @presenter.model_json(@model)
    end
  end
end
