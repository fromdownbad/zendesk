require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Stats::BenchmarkPresenter do
  include Zendesk::Stats::StatRollup::Benchmarking
  extend Api::Presentation::TestHelper
  fixtures :users

  before do
    load_benchmarking_stats
    @model = benchmarking_stats
    @presenter = Api::V2::Stats::BenchmarkPresenter.new(
      users(:minimum_agent),
      url_builder: mock_url_builder,
      category: "industry",
      type: "software"
    )
  end

  should_present_keys :category, :type, :data

  it "has 'benchmark' as the root key" do
    assert_equal :benchmark, @presenter.model_key
  end

  it "includes all the right metrics nested in data" do
    data = @presenter.model_json(@model)[:data]
    ['csr', 'first_response_time', 'created_count'].each do |key|
      assert data.key?(key)
    end
  end

  describe "when the category is 'overall'" do
    before do
      @presenter = Api::V2::Stats::BenchmarkPresenter.new(
        users(:minimum_agent),
        url_builder: mock_url_builder,
        category: "overall"
      )
    end

    it "uses average as the type" do
      assert_equal "average", @presenter.model_json(@model)[:type]
    end
  end
end
