require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TopicPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :attachments, :users, :user_settings, :entries, :forums

  before do
    @model     = entries(:sticky)
    @presenter = Api::V2::TopicPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :title, :body, :topic_type, :submitter_id, :updater_id, :forum_id, :locked,
    :pinned, :highlighted, :position, :tags, :created_at, :updated_at,
    :url, :attachments, :comments_count, :answered, :search_phrases

  should_present_method :id
  should_present_method :title
  should_present_method :body
  should_present_method :forum_type, as: :topic_type
  should_present_method :submitter_id
  should_present_method :updater_id
  should_present_method :forum_id
  should_present_method :locked
  should_present_method :pinned
  should_present_method :highlighted
  should_present_method :position
  should_present_method :attachments, value: []
  should_present_method :posts_count, as: :comments_count
  should_present_method :created_at
  should_present_method :updated_at
  should_present_method :search_phrases

  should_present_url :api_v2_topic_url, with: :id

  should_present ["big"], as: :tags

  should_present_collection :attachments, with: Api::V2::AttachmentPresenter

  should_side_load :users, :forums

  it "does not make too many queries" do
    @models = [entries(:sticky), entries(:ponies)]
    assert_sql_queries 6 do
      @presenter.present(@models)
    end
  end

  describe "with a question" do
    before do
      @model.forum.display_type_id = ForumDisplayType::QUESTIONS.id
      @model_json = @presenter.model_json(@model)
    end

    it "presents answered" do
      assert_not_nil @model_json[:answered]
    end

    it "presents topic answered" do
      assert_equal @model.answered?, @model_json[:answered]
    end
  end

  describe "with a topic on a deleted forum" do
    it "cleans up after itself" do
      @model.forum.update_column(:deleted_at, Time.now)
      assert_nil @presenter.model_json(@model.reload)
      assert @model.deleted?
    end
  end

  describe "side-loading" do
    before do
      @presenter.stubs(:side_load?).returns(false)
      @topics = [stub]
    end

    describe "users" do
      before { @presenter.stubs(:side_load?).with(:users).returns(true) }

      it "uses the UserPresenter" do
        side_loaded_users = [stub]

        @presenter.expects(:side_load_association).with(@topics, :submitter, @presenter.user_presenter).returns(side_loaded_users)

        assert_equal side_loaded_users, @presenter.side_loads(@topics)[:users]
      end
    end

    describe "forums" do
      before { @presenter.stubs(:side_load?).with(:forums).returns(true) }

      it "uses the ForumPresenter" do
        side_loaded_forums = [stub]

        @presenter.expects(:side_load_association).with(@topics, :forum, @presenter.forum_presenter).returns(side_loaded_forums)

        assert_equal side_loaded_forums, @presenter.side_loads(@topics)[:forums]
      end
    end
  end
end
