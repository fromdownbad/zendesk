require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::RecipientAddressPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :users, :recipient_addresses

  before do
    @model     = recipient_addresses(:default)
    @presenter = Api::V2::RecipientAddressPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder, includes: [:brands])
  end

  should_present_keys :brand_id,
    :cname_status,
    :created_at,
    :default,
    :dns_results,
    :domain_verification_code,
    :domain_verification_status,
    :email,
    :forwarding_status,
    :id,
    :name,
    :spf_status,
    :updated_at,
    :url

  describe "when the recipient address has an external_email_credential_id attribute" do
    let(:external_email_credential) do
      ExternalEmailCredential.new(
        account: accounts(:minimum),
        username: "USERNAME@z3nmail.com",
        current_user: users(:minimum_agent)
      ).tap do |external_email_credential|
        external_email_credential.encrypted_value = "unencrypted_token"
        external_email_credential.save!
      end
    end

    let(:recipient_address) do
      external_email_credential.recipient_address
    end

    before do
      @model     = recipient_address
      @presenter = Api::V2::RecipientAddressPresenter.new(users(:minimum_end_user), url_builder: mock_url_builder, includes: [:brands])
    end

    should_present_keys :brand_id,
      :cname_status,
      :created_at,
      :default,
      :dns_results,
      :domain_verification_code,
      :domain_verification_status,
      :email,
      :external_email_credential_id,
      :forwarding_status,
      :id,
      :name,
      :spf_status,
      :updated_at,
      :url
  end

  describe_with_arturo_disabled :email_dns_results do
    should_present_keys :brand_id,
      :cname_status,
      :created_at,
      :default,
      :domain_verification_code,
      :domain_verification_status,
      :email,
      :forwarding_status,
      :id,
      :name,
      :spf_status,
      :updated_at,
      :url
  end

  describe "side-loading brands" do
    should_side_load :brands

    before do
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:brands).returns(true)
    end

    it "uses the BrandPresenter" do
      serialized_brand = stub
      @brand = @model.brand

      @presenter.brand_presenter.expects(:model_json).with(@brand).returns(serialized_brand)

      assert_equal [serialized_brand], @presenter.side_loads(@model)[:brands]
    end
  end
end
