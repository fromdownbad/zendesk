require_relative "../../../support/test_helper"
require_relative '../../../support/rule'

SingleCov.covered!

describe Api::V2::WorkspacePresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper
  fixtures :tickets, :users, :workspaces, :rules, :groups

  let(:options) { {} }
  let(:title) { 'workspace title 1' }
  let(:user) { users(:minimum_agent) }
  let(:definition) { {"all": [{"field": "status", "operator": "is", "value": "3"}], "any": []} }
  let(:ticket) { tickets(:minimum_1) }
  let(:macro) { create_macro }
  let(:account) { ticket.account }
  let(:user)    { macro.account.owner }

  before do
    @model = Workspace.new
    @model.title = title
    @model.definition = definition
    @model.account_id = ticket.account_id
    @model.save_with({ macros: [macro.id]}, user: user)
    @model.reload
    @presenter = Api::V2::WorkspacePresenter.new(user, options.merge(url_builder: mock_url_builder))
  end

  describe "Workspace with default no_macros" do
    should_present_keys :id, :title, :description, :ticket_form_id, :activated, :position, :url, :conditions, :selected_macros, :created_at, :updated_at, :apps, :macro_ids
    should_present_methods :id, :title, :description, :ticket_form_id, :activated, :position, :created_at, :updated_at

    should_present_method :apps, value: []

    should_present_url :api_v2_workspaces_url
    should_present_method :conditions, with: Api::V2::Rules::ConditionsPresenter
  end

  describe "Workspace with no_macros set to true" do
    before do
      @presenter.no_macros = true
    end

    should_present_keys :id, :title, :description, :ticket_form_id, :activated, :position, :url, :conditions, :created_at, :updated_at, :apps, :macro_ids
  end

  describe 'when the owner is an account' do
    before do
      GroupMacro.destroy_all
      macro.owner = account
      @model.save_with({ macros: [macro.id]}, user: user)
    end

    it 'presents no restrictions' do
      json = @presenter.model_json(@model)
      assert_nil json[:selected_macros][0][:restriction]
    end
  end

  describe 'when the owner is a group' do
    let(:macro_1) { rules(:macro_for_minimum_group) }
    let(:group_1) { groups(:minimum_group) }
    let(:group_2) { account.groups.create!(name: '2', is_active: true) }

    before do
      GroupMacro.destroy_all

      [group_1, group_2].each do |group|
        GroupMacro.create! do |group_macro|
          group_macro.account_id = account.id
          group_macro.group_id   = group.id
          group_macro.macro_id   = macro_1.id
        end
      end
    end

    it 'presents the restriction' do
      @model.save_with({ macros: [macro_1.id]}, user: user)
      json = @presenter.model_json(@model)

      assert_equal(
        {type: 'Group', id: group_1.id, ids: [group_1.id, group_2.id]},
        json[:selected_macros][0][:restriction]
      )
    end
  end

  describe 'when the owner is a user' do
    let(:macro_1) { rules(:macro_for_minimum_agent) }

    it 'presents the restriction' do
      @model.save_with({ macros: [macro_1.id]}, user: user)
      json = @presenter.model_json(@model)
      assert_equal({type: 'User', id: macro_1.owner.id}, json[:selected_macros][0][:restriction])
    end
  end
end
