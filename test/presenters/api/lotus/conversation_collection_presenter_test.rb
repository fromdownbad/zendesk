require_relative '../../../support/test_helper'
require_relative '../../../support/collection_presenter_helper'

SingleCov.covered!

describe Api::Lotus::ConversationCollectionPresenter do
  extend Api::Presentation::TestHelper

  include CollectionPresenterHelper

  fixtures :all

  before do
    @model = events(:serialization_comment)
    @collection = [@model]
    @model_key  = :conversations
    @user = users(:minimum_agent)
    @item_presenter = Api::Lotus::ConversationItemPresenter.new(@user, url_builder: mock_url_builder)
    @presenter = Api::Lotus::ConversationCollectionPresenter.new(
      @user,
      url_builder: mock_url_builder,
      item_presenter: @item_presenter
    )
  end

  should_side_load :users
  should_present_collection_keys :metadata, :via, :created_at

  describe "comment decoration data exists" do
    it "includes decoration in metadata" do
      payload = @presenter.present([events(:create_comment_for_minimum_ticket_1)])[:conversations].first
      assert payload[:metadata][:decoration]
    end
  end

  describe "comment decoration data exists but no metadata" do
    before do
      @presenter = Api::Lotus::ConversationCollectionPresenter.new(
        @user,
        url_builder: mock_url_builder,
        item_presenter: @item_presenter,
        without_metadata: true
      )
    end

    it "should not merge on null JSON" do
      payload = @presenter.present([events(:create_comment_for_minimum_ticket_1)])[:conversations].first
      refute payload[:metadata]
    end
  end

  describe "comment decoration data does not exist" do
    it "does not include decoration in metadata" do
      payload = @presenter.present([@model])[:conversations].first
      refute payload[:metadata].key?(:decoration)
    end
  end

  describe "comment is missing an event" do
    before { @model.stubs(audit: nil) }

    it 'presents conversations' do
      assert @presenter.present([@model])[:conversations].present?
    end
  end

  describe "via" do
    describe "a missing ticket for a comment" do
      before do
        @model.update_column :via_id, Zendesk::Types::ViaType.MAIL
        @model.ticket.delete
        @model.reload
      end

      it "does not blow up" do
        assert_not_nil @presenter.model_json([@model])[0][:via]
      end
    end
  end

  describe "side-loading users" do
    describe "email ccs" do
      let(:recipients) { "minimum_end_user@aghassipour.com bar@example.com" }
      let(:account) { accounts(:minimum) }
      let(:ticket) { Ticket.new(account: account, description: "testing recipient sideloads") }

      def save_ticket
        ticket.stubs(:email_cc_addresses).returns(recipients.split(" "))
        ticket.will_be_saved_by(@user, via_id: ViaType.WEB_SERVICE)
        ticket.save!

        @presenter.stubs(:side_load?).with(:users).returns(true)
      end

      before do
        Audit.any_instance.stubs(active_recipients_ids: [users(:minimum_end_user).id])
      end

      describe "when comment_email_ccs_allowed setting is enabled" do
        before do
          Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
        end

        it "side loads recipients" do
          save_ticket
          sideloaded_users = @presenter.present([ticket.comments.first])[:users]
          assert_equal 2, sideloaded_users.size
          assert_includes sideloaded_users, @presenter.user_presenter.model_json(users(:minimum_end_user))
        end
      end

      describe "when comment_email_ccs_allowed setting is disabled" do
        before do
          Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
        end

        it "does not side load recipients" do
          save_ticket
          sideloaded_users = @presenter.present([ticket.comments.first])[:users]
          assert_equal 1, sideloaded_users.size
          refute_includes sideloaded_users, @presenter.user_presenter.model_json(users(:minimum_end_user))
        end
      end
    end

    describe "chat conversation" do
      before do
        @presenter.stubs(:side_load?).with(:users).returns(true)
      end

      it "side loads agents" do
        mock_history = [
          {actor_type: 'agent', actor_id: 1},
          {actor_type: 'agent', actor_id: 1},
          {actor_type: 'agent', actor_id: 2},
          {actor_type: 'visitor', actor_id: 3},
          {actor_type: 'visitor', actor_id: 3},
        ]

        mock_conversation_item = ChatStartedEvent.new(value: {
          'chat_id' => '123',
          'visitor_id' => '456',
          'chat_start_url' => 'https://example.com',
          'history' => mock_history
        })

        agents = [stub(id: 1), stub(id: 2)]
        relation = stub(to_a: agents)
        @presenter.account.all_users.expects(:where).with(id: [1, 2]).returns(relation)

        mock_users_json = [stub, stub]
        @presenter.user_presenter.collection_presenter.expects(:model_json).with(agents).returns(mock_users_json)

        sideloaded_users = @presenter.side_loads([mock_conversation_item])[:users]
        assert_equal mock_users_json, sideloaded_users
      end
    end
  end
end
