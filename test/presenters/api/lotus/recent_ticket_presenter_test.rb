require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::RecentTicketPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users, :tickets

  before do
    @model     = tickets(:minimum_1)
    @presenter = Api::Lotus::RecentTicketPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :title, :status, :requester_name

  should_present_method :nice_id, as: :id
  should_present_method :title

  it "presents status" do
    @model.status_id = nil
    assert_nil @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.NEW
    assert_equal 'new', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.OPEN
    assert_equal 'open', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.PENDING
    assert_equal 'pending', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.HOLD
    assert_equal 'hold', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.SOLVED
    assert_equal 'solved', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.CLOSED
    assert_equal 'closed', @presenter.model_json(@model)[:status]

    @model.status_id = Zendesk::Types::StatusType.DELETED
    assert_equal 'deleted', @presenter.model_json(@model)[:status]
  end

  it "presents requester_name" do
    assert_equal @model.requester.name, @presenter.model_json(@model)[:requester_name]
  end
end
