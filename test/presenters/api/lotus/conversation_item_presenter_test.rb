require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::ConversationItemPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:options) { {} }
  let(:suggested_articles) do
    [
      { id: 1, url: 'http://helpcenter.article/1.json', html_url: 'http://helpcenter.article/1', title: 'article title 1' },
      { id: 2, url: 'http://helpcenter.article/2.json', html_url: 'http://helpcenter.article/2', title: 'article title 2' },
      { id: 3, url: 'http://helpcenter.article/3.json', html_url: 'http://helpcenter.article/3', title: 'article title 3' }
    ]
  end

  describe '#collection_presenter' do
    let(:user) { users(:minimum_admin) }
    let(:options) { { url_builder: mock_url_builder } }

    describe 'without cursor pagination' do
      let(:presenter) do
        Api::Lotus::ConversationItemPresenter.new(user, options)
      end

      it 'returns a `Api::Lotus::ConversationCollectionPresenter`' do
        assert(
          presenter.collection_presenter.is_a?(Api::Lotus::ConversationCollectionPresenter),
          'collection presenter must be an instance of `Api::Lotus::ConversationCollectionPresenter`'
        )
      end
    end

    describe 'with cursor pagination' do
      let(:presenter) do
        Api::Lotus::ConversationItemPresenter.new(user, options.merge(with_cursor_pagination: true))
      end

      it 'returns a `Api::Lotus::ConversationCursorCollectionPresenter`' do
        assert(
          presenter.collection_presenter.is_a?(Api::Lotus::ConversationCursorCollectionPresenter),
          'collection presenter must be an instance of `Api::Lotus::ConversationCollectionPresenter`'
        )
      end
    end
  end

  describe "Comment as conversation item" do
    before do
      @model = events(:serialization_comment)
      @attachment = attachments(:serialization_comment_attachment)
      @model.attachments << @attachment
      @presenter = Api::Lotus::ConversationItemPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :html_body, :plain_body, :public, :attachments, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :html_body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type

    should_present_collection :attachments, with: Api::V2::AttachmentPresenter

    describe "collection presenter" do
      let(:options) { { includes: [:test] } }

      it "has the same side-loads" do
        assert_equal [:test], @presenter.collection_presenter.includes
      end
    end
  end

  describe "AnswerBotNotification as conversation item" do
    let(:ticket) { tickets(:minimum_1) }
    let(:account) { accounts(:minimum) }
    let(:deflection_trigger) do
      deflection_config = { recipient: 'requester_id', subject: 'email subject', body: 'email body' }

      d = Definition.new
      d.actions.push(DefinitionItem.new("deflection", "is", deflection_config))
      d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))

      Trigger.create!(account: account, title: 'deflection trigger', definition: d, owner: account)
    end
    let(:answer_bot_notification) do
      notification = AnswerBotNotification.new(recipients: Array(ticket.requester.id), subject: 'test subject', body: 'test body', audit: ticket.audit)
      notification.via_id = ViaType.RULE
      notification.via_reference_id = deflection_trigger.id
      notification
    end

    before do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)

      @model = answer_bot_notification

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::ConversationItemPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id, :recipients, :via

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :recipients
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end

  describe "AutomaticAnswerSend as conversation item" do
    before do
      ticket = tickets(:minimum_1)
      @model = AutomaticAnswerSend.new
      @model.suggested_articles = suggested_articles

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::ConversationItemPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end

  describe "AutomaticAnswerSolve as conversation item" do
    before do
      ticket = tickets(:minimum_1)
      @model = AutomaticAnswerSolve.new
      @model.solved_article = suggested_articles.first

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::ConversationItemPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end

  describe "AutomaticAnswerReject as conversation item" do
    before do
      ticket = tickets(:minimum_1)
      @model = AutomaticAnswerReject.new
      @model.article = suggested_articles.first
      @model.reviewer_id = ticket.requester_id
      @model.reviewer_name = ticket.requester.name
      @model.irrelevant = true

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::ConversationItemPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end
end
