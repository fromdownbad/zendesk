require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Api::Lotus::MacroApplicationPresenter do
  extend Api::Presentation::TestHelper

  include TestSupport::Rule::Helper

  fixtures :accounts,
    :rules,
    :taggings,
    :tickets,
    :ticket_fields,
    :users

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:ticket)  { nil }

  let(:actions) { [] }

  let(:macro) do
    create_macro(
      definition: Definition.new.tap do |definition|
        actions.each { |action| definition.actions.push(action) }
      end
    )
  end

  let(:model) do
    Zendesk::Rules::Macro::Application.new(
      macro:  macro,
      ticket: ticket,
      user:   user
    )
  end

  let(:presenter) do
    Api::Lotus::MacroApplicationPresenter.new(
      user,
      ticket:      ticket,
      url_builder: mock_url_builder
    )
  end

  let(:result) { presenter.present(model)[:result] }

  before do
    @model     = model
    @presenter = presenter
  end

  describe 'when applying a comment' do
    let(:comment) { result[:comment] }

    let(:raw_comment) do
      comment[:scoped_body].find do |channel|
        channel[0] == 'channel:raw'
      end
    end

    let(:comment_value) do
      model.result.with_indifferent_access[:comment][:value]
    end

    let(:comment_value_action) do
      DefinitionItem.new(
        'comment_value',
        nil,
        ['Basic comment']
      )
    end

    let(:comment_value_html_action) do
      DefinitionItem.new(
        'comment_value_html',
        nil,
        ['<h1>Rich comment</h1>']
      )
    end

    let(:rendered_comment) { ZendeskText::Markdown.html(comment_value) }

    describe 'and the comment contains liquid' do
      let(:ticket)  { tickets(:minimum_1) }
      let(:actions) { [comment_value_action] }

      let(:comment_value_action) do
        DefinitionItem.new(
          'comment_value',
          nil,
          ['[{{ticket.account}}] Requester: {{ticket.requester.name}}']
        )
      end

      it 'presents comment changes' do
        assert_equal comment_value, comment[:body]
      end

      it 'replaces the liquid' do
        assert_no_match /{{ticket\..*}}/, comment[:body]
      end

      it 'presents the pre-liquid value in the raw channel' do
        Arturo.enable_feature! :macro_application_no_liquid
        assert_match /{{ticket\..*}}/, raw_comment[1]
      end

      it 'provides the HTML of the rendered comment body' do
        assert_equal rendered_comment, comment[:html_body]
      end

      describe 'and the template contains a link' do
        let(:comment_value_action) do
          DefinitionItem.new('comment_value', nil, ['Link: {{ticket.link}}'])
        end

        let(:link) { ticket.url(for_agent: true) }

        it 'presents the correct `html_body`' do
          assert_equal(
            "<p dir='auto'>Link: <a href=\"#{link}\">#{link}</a></p>",
            comment[:html_body]
          )
        end
      end

      describe 'and the template contains an email address' do
        let(:comment_value_action) do
          DefinitionItem.new(
            'comment_value',
            nil,
            ['Email: {{current_user.email}}']
          )
        end

        let(:email) { user.email }

        it 'presents the correct `html_body`' do
          assert_equal(
            "<p dir='auto'>Email: <a href=\"mailto:#{email}\">#{email}</a></p>",
            comment[:html_body]
          )
        end
      end
    end

    describe 'and the comment does not contain liquid' do
      let(:actions) { [comment_value_action] }

      it 'presents the comment changes' do
        assert_equal comment_value, comment[:body]
      end

      it 'does not create a comment with a raw channel' do
        assert_nil raw_comment
      end

      it 'provides the HTML of the rendered comment body' do
        assert_equal rendered_comment, comment[:html_body]
      end
    end

    describe 'and the macro has an HTML comment' do
      let(:actions) { [comment_value_html_action] }

      it 'presents the proper keys' do
        assert_equal %w[body html_body], comment.keys
      end

      it 'presents the correct `body`' do
        assert_equal '<h1>Rich comment</h1>', comment[:body]
      end

      it 'presents the correct `html_body`' do
        assert_equal '<h1>Rich comment</h1>', comment[:html_body]
      end
    end

    describe 'and the macro has a rich and a basic comment' do
      let(:actions) { [comment_value_html_action, comment_value_action] }

      it 'presents the proper keys' do
        assert_equal %w[body html_body], comment.keys
      end

      it 'presents the correct `body`' do
        assert_equal 'Basic comment', comment[:body]
      end

      it 'presents the correct `html_body`' do
        assert_equal '<h1>Rich comment</h1>', comment[:html_body]
      end
    end

    describe 'and the macro also has a comment-mode action' do
      let(:actions) do
        [
          DefinitionItem.new('comment_value_html', nil, ['Hi there']),
          DefinitionItem.new('comment_mode_is_public', nil, [true])
        ]
      end

      it 'has the proper keys' do
        assert_equal %w[body html_body public], comment.keys
      end

      it 'has the correct comment mode' do
        assert(comment['public'])
      end
    end

    describe 'and the macro does not have a comment and has a comment mode' do
      let(:actions) do
        [DefinitionItem.new('comment_mode_is_public', nil, [false])]
      end

      it 'has the proper keys' do
        assert_equal %w[public], comment.keys
      end

      it 'has the correct comment mode' do
        assert_equal false, comment['public']
      end
    end
  end

  describe 'when applying other ticket attributes' do
    let(:macro)  { rules(:macro_incident_escalation) }
    let(:ticket) { tickets(:minimum_1) }

    should_present_keys :assignee_id,
      :comment,
      :group_id,
      :priority,
      :status,
      :type

    it 'presents ticket with only changes' do
      assert_equal(
        {
          'assignee_id' => user.id,
          'group_id'    => '2',
          'priority'    => 'normal',
          'status'      => 'open',
          'type'        => 'question'
        },
        result.except(:comment)
      )
    end
  end

  describe 'when applying tags' do
    let(:ticket) { nil }

    let(:actions) do
      [DefinitionItem.new('set_tags', 'is', ['abc def'])]
    end

    it 'includes tags' do
      assert_equal %w[abc def], result[:tags]
    end

    describe 'and a ticket is provided' do
      let(:ticket)   { tickets(:minimum_1) }
      let(:tag_name) { taggings(:minimum_1).tag_name }

      let(:actions) { [DefinitionItem.new('current_tags', 'is', tags)] }

      describe 'and it does not already have the tags' do
        let(:tags) { %w[hi] }

        it 'includes the new tag' do
          assert_equal [tag_name, 'hi'], result[:tags]
        end
      end

      describe 'and it already has the tags' do
        let(:tags) { [tag_name] }

        it 'does not add duplicate tags' do
          assert_equal [tag_name], result[:tags]
        end
      end
    end
  end

  describe 'when applying a custom ticket field' do
    let(:ticket) { nil }

    let(:custom_ticket_field)       { ticket_fields(:field_checkbox_custom) }
    let(:other_custom_ticket_field) { ticket_fields(:field_textarea_custom) }

    let(:custom_field) do
      result[:custom_fields].find do |field|
        field[:id] == custom_ticket_field.id
      end
    end

    let(:actions) do
      [
        DefinitionItem.new(
          "ticket_fields_#{custom_ticket_field.id}",
          'is',
          true
        )
      ]
    end

    it 'adds custom fields under the `custom_fields` key' do
      assert custom_field[:value]
    end

    it 'does not add custom ticket fields under the `ticket` key' do
      assert_nil result["ticket_fields_#{custom_ticket_field.id}"]
    end

    describe 'and a ticket is provided' do
      let(:ticket) { tickets(:minimum_1) }

      describe 'and it has an existing entry for that field' do
        before do
          ticket.ticket_field_entries.create(
            ticket_field: custom_ticket_field,
            value:        '1'
          )
        end

        it 'only presents the custom fields from the macro' do
          assert_equal(
            [custom_ticket_field.id],
            result[:custom_fields].map { |field| field[:id] }
          )
        end
      end

      describe 'and it has an existing entry for another field' do
        before do
          ticket.ticket_field_entries.create(
            ticket_field: other_custom_ticket_field,
            value:        'hi'
          )
        end

        it 'only presents the custom fields from the macro' do
          assert_equal(
            [custom_ticket_field.id],
            result[:custom_fields].map { |field| field[:id] }
          )
        end
      end
    end
  end

  describe 'when applying attachments' do
    let!(:attachment) do
      RuleAttachment.create! do |ra|
        ra.account_id    = account.id
        ra.user_id       = user.id
        ra.uploaded_data = fixture_file_upload('small.png')
        ra.filename      = 'small.png'
      end
    end

    let(:macro) { create_macro }

    before do
      attachment.associate(macro)
    end

    it 'includes `upload`' do
      assert_equal(
        Api::V2::UploadPresenter.
          new(user, url_builder: mock_url_builder).
          present(model.upload_token)[:upload].with_indifferent_access,
        result[:upload]
      )
    end

    describe 'and the macro does not have any attachments' do
      before { RuleAttachmentRule.destroy_all }

      it 'does not include `upload`' do
        assert_nil result[:upload]
      end
    end
  end
end
