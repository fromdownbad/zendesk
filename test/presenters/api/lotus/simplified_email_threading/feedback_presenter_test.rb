require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::SimplifiedEmailThreading::FeedbackPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @presenter = Api::Lotus::SimplifiedEmailThreading::FeedbackPresenter.new(@account.owner, url_builder: mock_url_builder)
    @model = { "id" => "123" }
  end

  should_present_keys :z1_request_url

  it "presents z1_request_url" do
    json = @presenter.model_json(@model)
    assert_equal "https://support.zendesk-test.com/hc/requests/123", json[:z1_request_url]
  end
end
