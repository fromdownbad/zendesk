require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::GroupCollectionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    Account.any_instance.stubs(has_permission_sets?: true)
    Account.any_instance.stubs(has_light_agents?: true)

    @light_agent_role = build_valid_permission_set(accounts(:minimum), 'light')
    @light_agent_role.permissions.disable(:ticket_editing)
    @light_agent_role.save!

    @light_agent = FactoryBot.create(:user,
      account: accounts(:minimum),
      roles: Role::AGENT.id,
      permission_set: @light_agent_role)
    @light_agent.memberships.create!(group: groups(:minimum_group))

    @end_user_who_used_to_be_agent = FactoryBot.create(:user, name: 'end-user', account: accounts(:minimum), roles: Role::AGENT.id)
    @end_user_who_used_to_be_agent.memberships.create!(group: groups(:minimum_group))
    @end_user_who_used_to_be_agent.update_attribute(:roles, Role::END_USER.id)

    @agent_who_was_deleted = FactoryBot.create(:user, name: 'deleted', account: accounts(:minimum), roles: Role::AGENT.id)
    @agent_who_was_deleted.memberships.create!(group: groups(:minimum_group))
    @agent_who_was_deleted.current_user = accounts(:minimum).owner
    @agent_who_was_deleted.delete!

    users(:minimum_admin).update_attribute(:name, 'BBB')
    users(:minimum_agent).update_attribute(:name, 'AAA')

    @ordered_agents = [users(:minimum_agent), users(:minimum_admin)]

    @model = [groups(:minimum_group), groups(:support_group)]

    @owner = accounts(:minimum).owner
    @owner.account # preload account
    @presenter = Api::Lotus::GroupCollectionPresenter.new(@owner, url_builder: mock_url_builder)
  end

  it 'works without any crazy joins' do
    json = nil
    queries = sql_queries do
      json = @presenter.present(@model)
    end

    refute queries.any? { |q| q.include?('JOIN') }

    json.must_equal(
      groups: [
        {
          id: groups(:minimum_group).id,
          name: groups(:minimum_group).name,
          agent_ids: @ordered_agents.map(&:id)
        }, {
          id: groups(:support_group).id,
          name: groups(:support_group).name,
          agent_ids: []
        }
      ],
      agents: [
        { id: @ordered_agents[0].id, name: @ordered_agents[0].name, avatar_url: nil },
        { id: @ordered_agents[1].id, name: @ordered_agents[1].name, avatar_url: nil }
      ],
      next_page: nil,
      previous_page: nil,
      count: 2
    )
  end

  it 'caches the output' do
    json = nil
    assert_sql_queries(7) { json = @presenter.present(@model) }

    assert_sql_queries 0 do
      cached_json = @presenter.present(@model)
      cached_json.must_equal json
    end

    agent = users(:minimum_admin)
    agent.name = "new name should blow cache"
    agent.save!
    Timecop.travel(Time.now + 1.seconds)

    assert_sql_queries 6 do
      cached_json = @presenter.present(@model)
      cached_json.wont_equal json
    end
  end
end
