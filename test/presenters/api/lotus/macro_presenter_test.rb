require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::Lotus::MacroPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts,
    :rules,
    :users

  let(:macro) { rules(:macro_for_minimum_agent) }

  let(:macros) do
    Zendesk::RuleSelection::Scope.viewable(
      Zendesk::RuleSelection::Context::Macro.new(macro.owner)
    )
  end

  let(:presenter) do
    Api::Lotus::MacroPresenter.new(macro.owner, url_builder: mock_url_builder)
  end

  before do
    @model     = macro
    @presenter = presenter
  end

  describe 'when presenting a single macro' do
    it 'does not include most-used macros' do
      refute presenter.present(macro).key?(:most_used)
    end
  end

  describe 'when presenting multiple macros' do
    it 'includes most-used macros' do
      assert presenter.present(macros).key?(:most_used)
    end
  end

  should_present_keys :availability_type,
    :description,
    :id,
    :raw_title,
    :title
end
