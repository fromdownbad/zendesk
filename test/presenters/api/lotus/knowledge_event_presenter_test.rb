require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::KnowledgeEventPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:options) { {} }
  let(:article) { { id: 2, url: 'http://helpcenter.article/1.json', html_url: 'http://helpcenter.article/1', title: 'article title 1', locale: 'en-US' } }
  let(:template) { { id: 2, url: 'http://helpcenter.article/1.json', html_url: 'http://helpcenter.article/1', title: 'article title 1' } }
  let(:user) { users(:minimum_agent) }

  describe "KnowledgeLinked as a knowledge event" do
    before do
      ticket = tickets(:minimum_1)
      @model = KnowledgeLinked.new(article: article)

      ticket.will_be_saved_by(user)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::KnowledgeEventPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end

  describe "KnowledgeCaptured as a knowledge item" do
    before do
      ticket = tickets(:minimum_1)
      @model = KnowledgeCaptured.new(article: article, template: template)

      ticket.will_be_saved_by(user)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::KnowledgeEventPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end

  describe "KnowledgeFlagged as a knowledge item" do
    before do
      ticket = tickets(:minimum_1)
      @model = KnowledgeFlagged.new(article: article)

      ticket.will_be_saved_by(user)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::KnowledgeEventPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end

  describe "KnowledgeLinkAccepted as a knowledge item" do
    before do
      ticket = tickets(:minimum_1)
      @model = KnowledgeLinkAccepted.new(article: article)

      ticket.will_be_saved_by(user)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::KnowledgeEventPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end

  describe "KnowledgeLinkRejected as a knowledge item" do
    before do
      ticket = tickets(:minimum_1)
      @model = KnowledgeLinkRejected.new(article: article)

      ticket.will_be_saved_by(user)
      ticket.audit.events << @model
      ticket.save!

      @presenter = Api::Lotus::KnowledgeEventPresenter.new(@model.account.owner, options.merge(url_builder: mock_url_builder))
    end

    should_present_keys :id, :type, :body, :public, :author_id, :audit_id

    should_present_method :id
    should_present_method :author_id
    should_present_method :body
    should_present_method :is_public?, as: :public
    should_present_method :type, as: :type
  end
end
