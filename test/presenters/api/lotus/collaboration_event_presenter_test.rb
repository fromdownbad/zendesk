require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::CollaborationEventPresenter do
  extend Api::Presentation::TestHelper
  fixtures :tickets, :accounts

  let(:ticket) { tickets(:minimum_1) }
  let(:account) { accounts(:minimum) }

  let(:event) do
    event = CollabThreadCreated.new
    event.subject = "Can someone in IT help us?"
    event.via_id = ViaType.WEB_SERVICE
    event.thread_id = "Thread-1"
    event.recipient_count = 2
    event
  end

  let(:presenter) do
    Api::Lotus::CollaborationEventPresenter.new(account.owner, url_builder: mock_url_builder)
  end

  before do
    ticket.will_be_saved_by(User.system)
    ticket.audit.events << event
    ticket.save!

    @model = event
    @presenter = presenter
  end

  should_present_keys :author_id, :id, :public, :subject, :thread_id, :type, :via, :recipient_count

  should_present "CollabThreadCreated", as: :type
  should_present_method :id
  should_present_method :subject
  should_present_method :thread_id
  should_present_method :recipient_count
end
