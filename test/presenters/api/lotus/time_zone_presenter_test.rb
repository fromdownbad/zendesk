require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::TimeZonePresenter do
  extend Api::Presentation::TestHelper

  let(:time_zone) { ActiveSupport::TimeZone["Amsterdam"] }
  let(:options)   { { url_builder: mock_url_builder } }
  let(:presenter) { Api::Lotus::TimeZonePresenter.new(nil, options) }
  let(:json)      { presenter.model_json(time_zone) }

  it "presents the translated name" do
    expected = "Translated Amsterdam"
    time_zone.expects(:translated_name).returns(expected)
    actual = json[:translated_name]
    assert_equal expected, actual, "Expected #{@presenter.class.name} to present #{expected} as :translated_name"
  end

  it "presents the name" do
    expected = time_zone.name
    actual = json[:name]
    assert_equal expected, actual, "Expected #{@presenter.class.name} to present #{expected} as :name"
  end

  it "presents the full name used in the IANA database" do
    expected = "Europe/Amsterdam"
    actual = json[:iana_name]
    assert_equal expected, actual, "Expected #{@presenter.class.name} to present #{expected} as :name"
  end

  it "presents the *current* UTC offset in a human-friendly string as formatted_offset" do
    expected = "GMT#{time_zone.now.formatted_offset}"
    actual = json[:formatted_offset]
    assert_equal expected, actual, "Expected #{@presenter.class.name} to present #{expected} as :formatted_offset"
  end

  it "presents the *current* UTC offset in minutes as offset" do
    expected = time_zone.now.utc_offset / 60
    actual = json[:offset]
    assert_equal expected, actual, "Expected #{@presenter.class.name} to present #{expected} as :offset"
  end

  it "does not present the url since response will be cached" do
    assert_nil json[:url]
  end

  describe 'moment flag is set to true' do
    let(:options) { { url_builder: mock_url_builder, moment: true } }

    it 'presents time zone data for moment js as :moment_packed' do
      assert_not_nil json[:moment_packed]
    end
  end
end
