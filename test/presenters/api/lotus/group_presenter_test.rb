require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Api::Lotus::GroupPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :groups, :accounts, :users, :memberships

  before do
    @model = groups(:minimum_group)
    @presenter = Api::Lotus::GroupPresenter.new(@model.id, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :description, :deleted, :created_at, :updated_at, :url

  should_present_method :id
  should_present_method :name
  should_present_method :description
end
