require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::CcsAndFollowers::Jobs::UpdateRequesterTargetRulesPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:admin) { account.admins.first }
  let(:url) { 'http://example.com' }
  let(:expected_output) { {result: { url: url } } }

  before do
    @presenter = Api::Lotus::CcsAndFollowers::Jobs::UpdateRequesterTargetRulesPresenter.new(admin, url_builder: mock_url_builder)
  end

  should_present_keys :url

  it 'generates the expected output' do
    expirable_attachment = stub
    expirable_attachment.expects(:url).returns(url)
    json = @presenter.present(expirable_attachment)
    assert_equal expected_output, json
  end

  describe 'when the expirable attachment is nil' do
    let(:url) { '' }
    it 'generates the expected output' do
      json = @presenter.present(nil)
      assert_equal expected_output, json
    end
  end
end
