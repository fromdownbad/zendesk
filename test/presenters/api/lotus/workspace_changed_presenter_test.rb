require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::WorkspaceChangedPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }
  let(:options) { {} }
  let(:ws_add_event) do
    ws_event = WorkspaceChanged.new(account: account)
    ws_event.workspace_attributes = {
      action: :ADD,
      id: 1,
      title: 'workspace 1'
    }
    ws_event
  end

  let(:ws_delete_event) do
    ws_event = WorkspaceChanged.new(account: account)
    ws_event.workspace_attributes = {
      action: :DELETE,
      id: 1,
      title: 'workspace 1'
    }
    ws_event
  end

  let(:ws_change_event) do
    ws_event = WorkspaceChanged.new(account: account)
    ws_event.previous_workspace_attributes = {
      id: 2,
      title: 'workspace 2'
    }

    ws_event.workspace_attributes = {
      action: :CHANGE,
      id: 1,
      title: 'workspace 1'
    }

    ws_event
  end

  describe "WorkspaceChanged event" do
    describe_with_arturo_enabled :updated_workspace_changed_presenter do
      it "presents an add event" do
        @presenter = Api::Lotus::WorkspaceChangedPresenter.new(user, options.merge(url_builder: mock_url_builder))
        response = @presenter.present ws_add_event

        assert_equal ws_add_event.workspace_id, response.dig(:workspace_changed, :value)
        assert_nil response.dig(:workspace_changed, :previous_value)
        assert_equal ws_add_event.action, response.dig(:workspace_changed, :action)
        assert_equal ws_add_event.workspace_title, response.dig(:workspace_changed, :title)
        assert_nil response.dig(:workspace_changed, :previous_title)
      end

      it "presents a delete event" do
        @presenter = Api::Lotus::WorkspaceChangedPresenter.new(user, options.merge(url_builder: mock_url_builder))
        response = @presenter.present ws_delete_event

        assert_equal ws_delete_event.action, response.dig(:workspace_changed, :action)
        assert_nil response.dig(:workspace_changed, :value)
        assert_equal ws_delete_event.workspace_id, response.dig(:workspace_changed, :previous_value)
        assert_nil response.dig(:workspace_changed, :title)
        assert_equal ws_delete_event.workspace_title, response.dig(:workspace_changed, :previous_title)
      end

      it "presents a change event" do
        @presenter = Api::Lotus::WorkspaceChangedPresenter.new(user, options.merge(url_builder: mock_url_builder))
        response = @presenter.present ws_change_event

        assert_equal ws_change_event.workspace_id, response.dig(:workspace_changed, :value)
        assert_equal ws_change_event.previous_workspace_attributes[:id], response.dig(:workspace_changed, :previous_value)
        assert_equal ws_change_event.action, response.dig(:workspace_changed, :action)
        assert_equal ws_change_event.workspace_title, response.dig(:workspace_changed, :title)
        assert_equal ws_change_event.previous_workspace_attributes[:title], response.dig(:workspace_changed, :previous_title)
      end
    end

    describe_with_arturo_disabled :updated_workspace_changed_presenter do
      it "presents an add event" do
        @presenter = Api::Lotus::WorkspaceChangedPresenter.new(user, options.merge(url_builder: mock_url_builder))
        response = @presenter.present ws_add_event

        assert_equal response[:workspace_changed][:id], ws_add_event.workspace_id
        assert_equal response[:workspace_changed][:type], ws_add_event.type
        assert_equal response[:workspace_changed][:value], ws_add_event.workspace_title
        assert_equal response[:workspace_changed][:action], ws_add_event.action
      end

      it "presents a delete event" do
        @presenter = Api::Lotus::WorkspaceChangedPresenter.new(user, options.merge(url_builder: mock_url_builder))
        response = @presenter.present ws_delete_event

        assert_equal response[:workspace_changed][:id], ws_delete_event.workspace_id
        assert_equal response[:workspace_changed][:type], ws_delete_event.type
        assert_equal response[:workspace_changed][:value], ws_delete_event.workspace_title
        assert_equal response[:workspace_changed][:action], ws_delete_event.action
      end

      it "presents a change event" do
        @presenter = Api::Lotus::WorkspaceChangedPresenter.new(user, options.merge(url_builder: mock_url_builder))
        response = @presenter.present ws_change_event

        assert_equal response[:workspace_changed][:id], ws_change_event.workspace_id
        assert_equal response[:workspace_changed][:type], ws_change_event.type
        assert_equal response[:workspace_changed][:value], ws_change_event.workspace_title
        assert_equal response[:workspace_changed][:action], ws_change_event.action
        assert_equal response[:workspace_changed][:value_previous], ws_change_event.previous_workspace_attributes
      end
    end
  end
end
