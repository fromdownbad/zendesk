require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::Lotus::ChatSettingsPresenter do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:chat_product) { nil }

  before do
    @account = accounts(:multiproduct)
    @account.stubs(:account_service_client).
      returns(stub(chat_product: chat_product))
  end

  describe Api::V2::Account::SettingsPresenter do
    before do
      @model = @account

      @presenter = Api::Lotus::ChatSettingsPresenter.new(
        @account.owner,
        url_builder: mock_url_builder
      )
    end

    should_present_keys :url,
      :phase,
      :suite
  end
end
