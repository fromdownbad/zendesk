require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::ConversationCursorCollectionPresenter do
  extend Api::Presentation::TestHelper

  fixtures :users, :events, :tickets

  let(:user) { users(:minimum_admin) }

  let(:presenter) do
    Api::Lotus::ConversationCursorCollectionPresenter.new(
      user,
      url_builder: mock_url_builder,
      item_presenter: Api::Lotus::ConversationItemPresenter.new(
        user,
        url_builder: mock_url_builder
      )
    )
  end

  let(:event) { events(:create_comment_for_minimum_ticket_1) }
  let(:conversation_items) { event.ticket.conversation_items }

  describe 'delgation to the ConversationCollectionPresenter' do
    it 'delegates model_json' do
      Api::Lotus::ConversationCollectionPresenter.any_instance.expects(:model_json)

      presenter.present(conversation_items.paginate_with_cursor)
    end
  end
end
