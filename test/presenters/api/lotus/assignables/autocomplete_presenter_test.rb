require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::Assignables::AutocompletePresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :groups, :role_settings, :photos

  it 'generates expected output' do
    @results = [
      users(:minimum_agent),
      users(:minimum_admin),
      groups(:minimum_group),
    ]

    @presenter = Api::Lotus::Assignables::AutocompletePresenter.new(users(:minimum_agent), url_builder: mock_url_builder)

    json = @presenter.present(@results)

    json.must_equal(
      agents: [
        {
          id: users(:minimum_agent).id,
          name: 'Agent Minimum',
          group_id: groups(:minimum_group).id,
          group: 'minimum_group',
          photo_url: nil
        },
        {
          id: users(:minimum_admin).id,
          name: 'Admin Man',
          group_id: groups(:minimum_group).id,
          group: 'minimum_group',
          photo_url: nil
        },
      ],
      groups: [
        {id: groups(:minimum_group).id, name: 'minimum_group'}
      ],
      count: @results.count
    )
  end

  it 'generates photo_url for agents with photos' do
    @results = [
      users(:photo_agent),
    ]

    @presenter = Api::Lotus::Assignables::AutocompletePresenter.new(users(:with_photo_owner), url_builder: mock_url_builder)

    json = @presenter.present(@results)

    json.must_equal(
      agents: [
        {
          id: users(:photo_agent).id,
          name: 'Photo User',
          group_id: groups(:with_photo_group).id,
          group: 'with_photo_group',
          photo_url: ContentUrlBuilder.new.url_for(users(:photo_agent).photo)
        },
      ],
      groups: [],
      count: @results.count
    )
  end
end
