require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::Assignables::GroupsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :groups, :accounts, :users, :memberships

  before do
    groups(:minimum_group).update_attributes(name: 'XXX', description: 'XXXX')
    groups(:trial_group).update_attributes(name: 'YYY', description: 'YYYY')

    @groups = [groups(:minimum_group), groups(:trial_group)]
    @presenter = Api::Lotus::Assignables::GroupsPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
  end

  it 'generates expected output' do
    json = @presenter.present(@groups)

    json.must_equal(
      groups: [
        { id: @groups[0].id, name: 'XXX', description: 'XXXX' },
        { id: @groups[1].id, name: 'YYY', description: 'YYYY' }
      ],
      next_page: nil,
      previous_page: nil,
      count: 2
    )
  end
end
