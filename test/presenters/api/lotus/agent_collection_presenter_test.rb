require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::AgentCollectionPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :groups, :accounts, :users, :memberships

  let(:group) { groups(:minimum_group) }
  let(:mock_large_photo_url) { '"https://dev.zd-dev.com/system/photos/0001/0021/cat.jpeg"' }

  before do
    users(:minimum_admin).update_attribute(:name, 'BBB')
    users(:minimum_agent).update_attribute(:name, 'AAA')

    agent_with_photo = users(:minimum_agent)
    mock_photo = mock
    agent_with_photo.stubs(:photo).returns(mock_photo)
    agent_with_photo.stubs(:large_photo_url).returns(mock_large_photo_url)
    @agents = [agent_with_photo, users(:minimum_admin)]
    @presenter = Api::Lotus::AgentCollectionPresenter.new(@agents.first, url_builder: mock_url_builder)
  end

  it 'generates expected output' do
    json = @presenter.present(@agents)

    json.must_equal(
      agents: [
        { id: @agents[0].id, name: @agents[0].name, avatar_url: mock_large_photo_url },
        { id: @agents[1].id, name: @agents[1].name, avatar_url: nil }
      ],
      next_page: nil,
      previous_page: nil,
      count: 2
    )
  end

  it 'can present a association' do
    json = @presenter.present(group.users.assignable_agents)
    json[:count].must_equal 2
  end

  describe "#cache_key" do
    it "produces a key" do
      @presenter.cache_key(group).must_include 'lotus_agents_presenter/2'
    end
  end
end
