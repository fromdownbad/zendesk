require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::DeletedTicketPresenter do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    ticket = tickets(:minimum_1)
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.soft_delete!
    ticket.reload
    @model = ticket
    @presenter = Api::Lotus::DeletedTicketPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    @previous_value = Api::V2::Tickets::AttributeMappings::STATUS_MAP[ticket.previous_status.to_s]
    @audit          = ticket.soft_deletion_event
  end

  should_present_keys :id, :subject, :description, :actor, :deleted_at, :previous_state

  it "presents id" do
    assert_equal @model.nice_id, @presenter.model_json(@model)[:id]
  end

  it "presents subject" do
    assert_equal @model.subject, @presenter.model_json(@model)[:subject]
  end

  it "presents description" do
    assert_equal @model.description, @presenter.model_json(@model)[:description]
  end

  it "presents actor" do
    assert_equal @audit.author.id, @presenter.model_json(@model)[:actor][:id]
    assert_equal @audit.author.name, @presenter.model_json(@model)[:actor][:name]
  end

  it "presents previous_state" do
    assert_equal @previous_value, @presenter.model_json(@model)[:previous_state]
  end
end
