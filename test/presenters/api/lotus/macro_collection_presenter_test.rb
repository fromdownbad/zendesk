require_relative '../../../support/test_helper'
require_relative '../../../support/collection_presenter_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Api::Lotus::MacroCollectionPresenter do
  extend Api::Presentation::TestHelper

  include CollectionPresenterHelper
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :rules,
    :users

  let(:account) { accounts(:minimum) }

  let(:user) { users(:minimum_agent) }

  let(:macro_usage) { Zendesk::RuleSelection::MacroUsage.new(user) }

  let!(:macro_1)  { create_macro(title: 'A', owner: user, position: 1) }
  let!(:macro_2)  { create_macro(title: 'B', owner: user, position: 2) }
  let!(:macro_3)  { create_macro(title: 'C', owner: user, position: 3) }
  let!(:macro_4)  { create_macro(title: 'D', owner: user, position: 4) }
  let!(:macro_5)  { create_macro(title: 'E', owner: user, position: 5) }
  let!(:macro_6)  { create_macro(title: 'F', owner: user, position: 6) }
  let!(:macro_7)  { create_macro(title: 'G', owner: user, position: 7) }
  let!(:macro_8)  { create_macro(title: 'H', owner: user, position: 8) }
  let!(:macro_9)  { create_macro(title: 'I', owner: user, position: 9) }
  let!(:macro_10) { create_macro(title: 'J', owner: user, position: 10) }
  let!(:macro_11) { create_macro(title: 'K', owner: user, position: 11) }

  let(:macros) do
    Zendesk::RuleSelection::Scope.viewable(
      Zendesk::RuleSelection::Context::Macro.new(user)
    )
  end

  let(:item_presenter) do
    Api::Lotus::MacroPresenter.new(user,
      includes:    [],
      url_builder: mock_url_builder)
  end

  let(:presenter) do
    Api::Lotus::MacroCollectionPresenter.new(user,
      includes:       item_presenter.includes,
      item_presenter: item_presenter,
      url_builder:    mock_url_builder)
  end

  let(:redis_client) { FakeRedis::Redis.new }

  before do
    @collection = macros
    @model_key  = :macros
    @presenter  = presenter

    Zendesk::RedisStore.redis_client = redis_client

    redis_client.flushall
  end

  after { redis_client.flushall }

  should_present_collection_keys :availability_type,
    :description,
    :id,
    :title

  describe 'when there are most-used macros' do
    before do
      1.times  { macro_usage.record(macro_1.id) }
      3.times  { macro_usage.record(macro_2.id) }
      5.times  { macro_usage.record(macro_3.id) }
      7.times  { macro_usage.record(macro_4.id) }
      9.times  { macro_usage.record(macro_5.id) }
      11.times { macro_usage.record(macro_6.id) }
      10.times { macro_usage.record(macro_7.id) }
      8.times  { macro_usage.record(macro_8.id) }
      6.times  { macro_usage.record(macro_9.id) }
      4.times  { macro_usage.record(macro_10.id) }
      2.times  { macro_usage.record(macro_11.id) }
    end

    describe 'and all are accessible' do
      it 'presents the 10 most-used macros by the user' do
        assert_equal(
          [
            macro_6,
            macro_7,
            macro_5,
            macro_8,
            macro_4,
            macro_9,
            macro_3,
            macro_10,
            macro_2,
            macro_11
          ].map { |macro| item_presenter.model_json(macro) },
          presenter.present(macros)[:most_used]
        )
      end
    end

    describe 'and some are inaccessible' do
      let(:other_user) { users(:minimum_admin) }

      let!(:macro_12) { create_macro(title: 'L', owner: other_user, position: 12) }

      before { 12.times { macro_usage.record(macro_12.id) } }

      it 'excludes inaccessible most-used macros' do
        assert_equal(
          [
            macro_6,
            macro_7,
            macro_5,
            macro_8,
            macro_4,
            macro_9,
            macro_3,
            macro_10,
            macro_2
          ].map { |macro| item_presenter.model_json(macro) },
          presenter.present(macros)[:most_used]
        )
      end
    end
  end

  describe 'when there are no most-used macros' do
    it 'presents no most-used macros' do
      assert_equal [], presenter.present(macros)[:most_used]
    end
  end
end
