require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::AnswerBotNotificationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :all

  let(:ticket) { tickets(:minimum_1) }
  let(:account) { accounts(:minimum) }

  let(:deflection_config) do
    {recipient: 'requester_id', subject: 'email subject', body: 'email body'}
  end
  let(:deflection_trigger) do
    d = Definition.new
    d.actions.push(DefinitionItem.new("deflection", "is", deflection_config))
    d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
    Trigger.create!(account: account, title: 'deflection trigger', definition: d, owner: account)
  end
  let(:rule_id) { deflection_trigger.id }
  let(:notification) do
    notification = AnswerBotNotification.new(recipients: Array(ticket.requester.id), subject: 'test subject', body: 'test body', audit: ticket.audit)
    notification.via_id = ViaType.RULE
    notification.via_reference_id = rule_id
    notification
  end
  let(:presenter) { Api::Lotus::AnswerBotNotificationPresenter.new(account.owner, url_builder: mock_url_builder) }

  before do
    Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
    ticket.will_be_saved_by(User.system)
    ticket.audit.events << notification
    ticket.save!
    @model = notification
    @presenter = presenter
  end

  should_present_keys :id, :type, :subject, :body, :recipients, :via

  should_present_method :id
  should_present_method :subject
  should_present_method :body
  should_present "AnswerBotNotification", as: :type

  it "presents recipients" do
    json = @presenter.model_json(@model)
    assert_equal [ticket.requester.id], json[:recipients]
  end

  it "presents via object" do
    json = @presenter.model_json(@model)
    assert_equal({
      channel: :rule,
      source: {
        from: {
          id: deflection_trigger.id,
          title: deflection_trigger.title,
          deleted: false
        },
        to: {},
        rel: 'trigger'
      }
    }, json[:via])
  end
end
