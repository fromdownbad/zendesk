require_relative "../../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"

SingleCov.covered!

describe Api::Private::MobileSdk::SettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :mobile_sdk_apps, :translation_locales

  let(:account) { accounts(:minimum_sdk) }
  let(:mobile_sdk_app) { mobile_sdk_apps(:minimum_sdk) }
  let(:oauth_client) { FactoryBot.create(:client, account: account) }
  let(:mobile_sdk_auth) { FactoryBot.create(:mobile_sdk_auth, account: account, client: oauth_client) }

  before do
    mobile_sdk_app.account.stubs(:has_connect_sdk?).returns(true)
    @model = mobile_sdk_app
    @presenter = Api::Private::MobileSdk::SettingsPresenter.new(account.owner, url_builder: mock_url_builder)
    @output = @presenter.model_json(mobile_sdk_app)
  end

  describe "root keys" do
    it "has correct keys" do
      assert_equal %i[answer_bot core rma support help_center connect blips].sort, @output.keys.sort
    end

    describe "connect sdk not enabled on account" do
      before do
        mobile_sdk_app.account.stubs(:has_connect_sdk?).returns(false)
        @model = mobile_sdk_app
        @presenter = Api::Private::MobileSdk::SettingsPresenter.new(account.owner, url_builder: mock_url_builder)
        @output = @presenter.model_json(mobile_sdk_app)
      end

      it "has correct keys" do
        assert_equal %i[answer_bot core rma support help_center blips].sort, @output.keys.sort
      end
    end
  end

  describe "core" do
    it "has correct fields" do
      assert_equal %i[identifier authentication brand_id updated_at], @output[:core].keys
    end

    it "displays the correct app" do
      assert_equal mobile_sdk_app.identifier, @output[:core][:identifier]
    end

    describe "updated_at" do
      before do
        mobile_sdk_app.settings.rma_tags = %i[one two]
        mobile_sdk_app.save
        @last_updated_at = mobile_sdk_app.settings.find { |s| s.name = "rma_tags" }.updated_at
        @output = @presenter.model_json(mobile_sdk_app)
      end

      it "displays the correct updated_at" do
        assert_equal @last_updated_at.to_i, @output[:core][:updated_at].to_i
      end
    end
  end

  describe "rma" do
    it "has correct fields" do
      assert_equal %i[enabled visits duration delay tags ios_store_url android_store_url], @output[:rma].keys
    end
  end

  describe "support" do
    it "has correct fields" do
      assert_equal %i[
        conversations
        contact_us
        attachments
        ticket_forms
        never_request_email
        show_closed_requests
        show_referrer_logo
        system_message
        referrer_url
        csat
      ], @output[:support].keys
    end

    it "has correct conversations sub fields" do
      assert_equal @output[:support][:conversations].keys, [:enabled]
    end

    it "has correct contact_us sub fields" do
      assert_equal %i[tags], @output[:support][:contact_us].keys
    end

    it "has correct attachments sub fields" do
      assert_equal %i[enabled max_attachment_size], @output[:support][:attachments].keys
    end

    it "has correct ticket_forms sub fields" do
      assert_equal %i[available], @output[:support][:ticket_forms].keys
    end

    it "has correct value for referrer_url" do
      assert_equal MobileSdkApp::REFERRER_URL, @output[:support][:referrer_url]
    end

    it "has correct value for csat" do
      assert_equal %i[enabled], @output[:support][:csat].keys
    end

    describe_with_arturo_disabled :mobile_sdk_support_never_request_email_setting do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        refute output[:support][:never_request_email], "the key :never_request_email must be false"
      end
    end

    describe_with_arturo_disabled :mobile_sdk_support_show_closed_requests_setting do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert output[:support][:show_closed_requests], "the key :show_closed_requests must be true"
      end
    end

    describe_with_arturo_disabled :mobile_sdk_support_show_referrer_logo_setting do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert output[:support][:show_referrer_logo], "the key :show_referrer_logo must be true"
      end
    end

    describe_with_arturo_disabled :mobile_sdk_csat do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        refute output[:support][:csat][:enabled], "the key :csat must be false"
      end
    end

    describe "system message" do
      let(:presenter_locale) { nil }
      let(:presenter_options) do
        {
          url_builder: mock_url_builder,
          locale: presenter_locale
        }
      end

      let(:presenter) { Api::Private::MobileSdk::SettingsPresenter.new(account.owner, presenter_options) }

      describe_with_arturo_disabled :mobile_sdk_support_system_message do
        let(:output) { presenter.model_json(mobile_sdk_app) }

        describe "default" do
          it "renders en empty string" do
            mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_DEFAULT
            mobile_sdk_app.save!
            assert_equal "", output[:support][:system_message]
          end
        end

        describe "custom" do
          it "renders en empty string" do
            mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_CUSTOM
            mobile_sdk_app.settings.support_system_message = "custom message"
            mobile_sdk_app.save!
            assert_equal "", output[:support][:system_message]
          end
        end
      end

      describe_with_arturo_enabled :mobile_sdk_support_system_message do
        let(:output) { presenter.model_json(mobile_sdk_app) }

        describe "default" do
          before do
            mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_DEFAULT
            mobile_sdk_app.save!
          end

          describe "no locale" do
            it "renders the default message" do
              assert_equal I18n.t('txt.embeddables.support.sdk.default_system_message'), output[:support][:system_message]
            end
          end

          describe "locale provided" do
            it "renders the default message localized" do
              assert_equal I18n.t('txt.embeddables.support.sdk.default_system_message'), output[:support][:system_message]
            end
          end
        end

        describe "custom" do
          before do
            mobile_sdk_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_CUSTOM
            mobile_sdk_app.settings.support_system_message = custom_system_message_string
            mobile_sdk_app.save!
          end

          describe "no dynamic content, no locale" do
            let(:custom_system_message_string) { "custom system message" }

            it "renders the custom message" do
              assert_equal custom_system_message_string, output[:support][:system_message]
            end
          end

          describe "dynamic content, no locale" do
            let(:custom_system_message_string) { "{{dc.sdk_system_message}}" }
            let(:presenter_locale) { nil }

            before do
              Zendesk::Liquid::DcContext.expects(:render).with(
                "{{dc.sdk_system_message}}",
                account,
                account.owner,
                'text/plain',
                account.translation_locale,
                {}
              ).returns("custom system message")
            end

            it "renders the custom message using dc variantions on the account translation locale" do
              assert_equal "custom system message", output[:support][:system_message]
            end
          end

          describe "dynamic content and locale" do
            let(:custom_system_message_string) { "{{dc.sdk_system_message}}" }
            let(:spanish_locale) { translation_locales(:spanish) }
            let(:presenter_locale) { spanish_locale }

            before do
              Zendesk::Liquid::DcContext.expects(:render).with(
                "{{dc.sdk_system_message}}",
                account,
                account.owner,
                'text/plain',
                spanish_locale,
                {}
              ).returns("mensaje de sistema personalizado")
            end

            it "renders the custom message using dc variantions on the provided translation locale" do
              assert_equal "mensaje de sistema personalizado", output[:support][:system_message]
            end
          end
        end
      end
    end
  end

  describe "help_center" do
    describe_with_and_without_arturo_enabled :mobile_sdk_help_center_article_voting_enabled do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert_equal %i[enabled locale help_center_article_voting_enabled], output[:help_center].keys
      end
    end
  end

  describe "connect" do
    it "has correct fields" do
      assert_equal %i[enabled], @output[:connect].keys
    end
  end

  describe "blips" do
    describe_with_arturo_disabled :mobile_sdk_blip_collection_control do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert_equal %i[permissions], output[:blips].keys
      end
    end

    describe_with_arturo_enabled :mobile_sdk_blip_collection_control do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert_equal %i[permissions collect_only_required_data], output[:blips].keys
      end
    end

    it "has correct permissions fields" do
      assert_equal %i[required behavioural pathfinder], @output[:blips][:permissions].keys
    end
  end

  describe "answer_bot" do
    it "has correct fields" do
      assert_equal %i[enabled], @output[:answer_bot].keys
    end

    it "has correct value for the 'enabled' field" do
      assert_equal mobile_sdk_app.answer_bot_enabled?, @output[:answer_bot][:enabled]
    end
  end

  describe "answer bot when availability endpoint return false" do
    before do
      @answer_bot_client = Zendesk::AnswerBotService::InternalApiClient.new(account)
      @answer_bot_client.stubs(:available).with(Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK).returns(false)
    end

    it "has the correct value for the 'enabled' field" do
      refute @output[:answer_bot][:enabled]
    end
  end
end
