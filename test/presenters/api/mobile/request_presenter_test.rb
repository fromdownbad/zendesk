require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::RequestPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint

  fixtures :accounts, :tickets, :ticket_fields, :events, :user_identities

  let(:admin) { users(:minimum_sdk_admin) }
  let(:agent) { users(:minimum_sdk_agent) }
  let(:agent2) { users(:minimum_sdk_agent2) }
  let(:agent3) { users(:minimum_sdk_agent3) }
  let(:end_user) { users(:minimum_sdk_end_user) }
  let(:audit) { events(:create_audit_for_minimum_ticket_1) }

  before do
    @account   = accounts(:minimum_sdk)
    @model     = tickets(:minimum_mobile_sdk)
    @presenter = Api::Mobile::RequestPresenter.new(@account.owner, url_builder: mock_url_builder, csat_enabled: true)
  end

  should_pass_lint_tests

  should_present_keys :assignee_id,
    :can_be_solved_by_me,
    :collaborator_ids,
    :comment_count,
    :created_at,
    :custom_fields,
    :description,
    :due_at,
    :email_cc_ids,
    :fields,
    :followup_source_id,
    :id,
    :is_public,
    :last_commenting_agents_ids,
    :organization_id,
    :priority,
    :recipient,
    :requester_id,
    :status,
    :subject,
    :type,
    :updated_at,
    :url,
    :via,
    :csat

  describe "#id" do
    it "presents the request token if available" do
      @model.request_token = FactoryBot.create(:request_token)
      assert_equal @presenter.model_json(@model)[:id], @model.token
    end

    it "presents the request nice_id when request token is not available" do
      @model.request_token = nil
      assert_equal @presenter.model_json(@model)[:id], @model.nice_id.to_s
    end
  end

  describe "#comment_count" do
    it "presents the correct public comment count" do
      assert_equal @presenter.model_json(@model)[:comment_count], @model.public_comments.size
    end
  end

  describe_with_arturo_disabled :mobile_sdk_support_public_updated_at_using_created_at do
    it 'generates public_updated_at using the updated_at attribute in events (comments)' do
      event = @model.events.first
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:public_updated_at).returns(true)
      assert_equal event.updated_at, @presenter.model_json(@model)[:public_updated_at]
    end
  end

  describe_with_arturo_enabled :mobile_sdk_support_public_updated_at_using_created_at do
    it 'generates public_updated_at using the created_at attribute in events (comments)' do
      event = @model.events.first
      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:public_updated_at).returns(true, true)
      assert_equal event.created_at, @presenter.model_json(@model)[:public_updated_at]
    end
  end

  describe "#last_commenting_agents_ids" do
    before do
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by agent", author: agent, is_public: true, created_at: 3.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by agent2", author: agent2, is_public: true, created_at: 2.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by agent3", author: agent3, is_public: true, created_at: 12.hours.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Private comment 1 by admin", author: admin, is_public: false, created_at: 3.day.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by enduser", author: end_user, is_public: true, created_at: 1.days.ago)
    end

    describe_with_arturo_disabled :mobile_sdk_support_last_commenting_agents do
      it "includes a 'last_commenting_agents_ids' key but it's empty" do
        assert_equal [], @presenter.model_json(@model)[:last_commenting_agents_ids]
      end
    end

    describe_with_arturo_enabled :mobile_sdk_support_last_commenting_agents do
      it "includes the 'last_commenting_agents_ids' key" do
        assert @presenter.model_json(@model)[:last_commenting_agents_ids], "the 'last_commenting_agents_ids' key should be present"
      end

      it "presents only the last 5 *agent* ids that have *publicly* commented on the ticket" do
        output = @presenter.model_json(@model)
        refute output[:last_commenting_agents_ids].include?(admin.id), "the id of admin should not be here as he made a *private* comment"
        refute output[:last_commenting_agents_ids].include?(end_user.id), "end_users should not be here, only agents"
      end

      it "presents the last 5 *agent* ids that have publicly commented on the ticket *in the order* they commented" do
        assert_equal [agent3.id, agent2.id, agent.id], @presenter.model_json(@model)[:last_commenting_agents_ids]
      end
    end
  end

  describe "accepts a single request" do
    before do
      @tickets = [@model, tickets(:minimum_2)]
    end

    it "raises an error if an array is passed" do
      assert_raises(ArgumentError) { @presenter.model_json(@tickets) }
    end

    it "raises the correct error message" do
      exception = assert_raises(ArgumentError) { @presenter.model_json(@tickets) }
      assert_equal "Passing a list to this presenter may cause an N + 1", exception.message
    end
  end

  describe "#model_json without sideloads" do
    subject { @presenter.present(@model)[:request] }

    it "does not present first_comment" do
      assert_nil subject[:first_comment]
    end

    it "does not present last_comment" do
      assert_nil subject[:last_comment]
    end
  end

  describe "'first_comment' and 'last_comment' sideloads" do
    describe "with public comments" do
      subject { @presenter.present(@model)[:request] }

      before do
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:first_comment).returns(true)
        @presenter.stubs(:side_load?).with(:last_comment).returns(true)
      end

      it "presents last_comment" do
        assert_not_nil subject[:last_comment]
        assert_not_nil subject[:last_comment][:url]
      end

      it "presents first_comment" do
        assert_not_nil subject[:first_comment]
        assert_not_nil subject[:first_comment][:url]
      end
    end

    describe "without public comments" do
      subject { @presenter.present(@model)[:request] }

      before do
        @model.comments.update_all(is_public: false)
        @presenter.stubs(:side_load?).returns(false)
        @presenter.stubs(:side_load?).with(:first_comment).returns(true)
        @presenter.stubs(:side_load?).with(:last_comment).returns(true)
      end

      it "presents last_comment with null value" do
        assert subject.key?(:last_comment)
        assert_nil subject[:last_comment]
      end

      it "presents first_comment with null value" do
        assert subject.key?(:first_comment)
        assert_nil subject[:first_comment]
      end
    end
  end

  describe "'last_commenting_agents' sideload" do
    let(:admin) { users(:minimum_sdk_admin) }
    let(:agent) { users(:minimum_sdk_agent) }
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }

    before do
      admin.photo = FactoryBot.create(:photo, filename: "large_1.png")
      admin.save!

      agent.photo = FactoryBot.create(:photo, filename: "large_2.png")
      agent.save!

      @presenter.stubs(:side_load?).returns(false)
      @presenter.stubs(:side_load?).with(:last_commenting_agents).returns(true)

      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by agent", author: agent, is_public: true, created_at: 1.day.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by agent2", author: agent2, is_public: true, created_at: 2.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by agent3", author: agent3, is_public: true, created_at: 3.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Private comment 1 by admin", author: admin, is_public: false, created_at: 1.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model, body: "Public comment 1 by enduser", author: end_user, is_public: true, created_at: 3.days.ago)

      @model2 = tickets(:minimum_mobile_sdk_2)
      Comment.create!(account: @account, audit: audit, ticket: @model2, body: "Public comment 1 on ticket 2 by agent", author: agent, is_public: true, created_at: 1.day.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model2, body: "Public comment 1 on ticket 2 by agent2", author: agent2, is_public: true, created_at: 2.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model2, body: "Public comment 1 on ticket 2 by agent3", author: agent3, is_public: true, created_at: 3.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model2, body: "Private comment 1 on ticket 2 by admin", author: admin, is_public: false, created_at: 2.days.ago)
      Comment.create!(account: @account, audit: audit, ticket: @model2, body: "Public comment 1  on ticket 2 by enduser", author: end_user, is_public: true, created_at: 3.days.ago)
    end

    describe_with_arturo_disabled :mobile_sdk_support_last_commenting_agents do
      before do
        @output = @presenter.side_loads([@model, @model2])
      end

      it "includes a 'last_commenting_agents' key in the root but with an empty array" do
        assert_equal [], @output[:last_commenting_agents]
      end

      it "is a good citized and does not do n + 1s" do
        assert_no_n_plus_one do
          @presenter.side_loads([@model, @model2])
        end
      end
    end

    describe_with_arturo_enabled :mobile_sdk_support_last_commenting_agents do
      before do
        @output = @presenter.side_loads([@model, @model2])
      end

      it "always include a 'last_commenting_agents' key in the root" do
        assert @output[:last_commenting_agents], "the 'last_commenting_agents' key should be present"
      end

      it "presents only the *agents* that have *publicly* commented on the ticket set" do
        refute @output[:last_commenting_agents].include?(admin), "the admin should not be here as she made a *private* comment"
        refute @output[:last_commenting_agents].include?(end_user), "end_users should not be here, only agents"
      end

      it "presents only the last 5 *agents* that have *publicly* comment on the tickets set" do
        expected_commenting_agents_ids = [agent.id, agent2.id, agent3.id].sort
        assert_equal expected_commenting_agents_ids, @output[:last_commenting_agents].map { |a| a[:id] }.sort
      end

      it "is a good citized and does not do n + 1s" do
        assert_no_n_plus_one do
          @presenter.side_loads([@model, @model2])
        end
      end
    end
  end

  describe "satisfaction ratings" do
    let (:good_csat_params) { { score: "good", comment: "Awesome support.", reason_code: 100 } }
    let (:bad_csat_no_comment_params) { { score: "bad" } }

    describe "are present in the request" do
      before do
        Zendesk::Tickets::Initializer.set_rating(@model, good_csat_params)
        @model.will_be_saved_by(end_user, via_id: ViaType.WEB_SERVICE)
        @model.current_user = end_user # Keep ticket validations happy
        @model.save!
      end

      describe "are enabled" do
        it "renders the csat" do
          assert @presenter.model_json(@model)[:csat], "the csat key should be present"
        end

        it "renders the csat correctly" do
          csat = @presenter.model_json(@model)[:csat]
          assert csat[:id], "csat should have an id"
          assert csat[:score], "csat should have a score"
          assert csat[:updated_at], "csat should have a updated_at"
          assert csat[:created_at], "csat should have a created_at"
          assert csat[:comment], "csat should have a comment"
        end

        it "renders the score as a string" do
          csat = @presenter.model_json(@model)[:csat]
          assert_equal "good", csat[:score], "csat score should be mapped to string"
        end

        it "renders only the last csat" do
          Zendesk::Tickets::Initializer.set_rating(@model, bad_csat_no_comment_params)
          @model.will_be_saved_by(end_user, via_id: ViaType.WEB_SERVICE)
          @model.current_user = end_user # Keep ticket validations happy
          @model.save!

          csat = @presenter.model_json(@model)[:csat]
          assert_equal 5, csat.count
          assert_equal "bad", csat[:score]
        end
      end

      describe "are disabled" do
        it "adds an empty csat field" do
          @presenter = Api::Mobile::RequestPresenter.new(@account.owner, url_builder: mock_url_builder, csat_enabled: false)
          assert_nil @presenter.model_json(@model)[:csat], "csats field should not be added when they are disabled"
        end
      end
    end

    describe "are not present in the request" do
      it "adds an empty csat field when they are enabled" do
        assert_nil @presenter.model_json(@model)[:csat], "csats should be empty when there is none in the ticket"
      end

      it "adds an empty csat field when they are disabled" do
        @presenter = Api::Mobile::RequestPresenter.new(@account.owner, url_builder: mock_url_builder, csat_enabled: false)
        assert_nil @presenter.model_json(@model)[:csat], "csat field should not be added when they are disabled"
      end
    end

    describe "with no comment" do
      before do
        Zendesk::Tickets::Initializer.set_rating(@model, bad_csat_no_comment_params)
        @model.will_be_saved_by(end_user, via_id: ViaType.WEB_SERVICE)
        @model.current_user = end_user # Keep ticket validations happy
        @model.save!
      end

      it "are handled correctly" do
        csat = @presenter.model_json(@model)[:csat]
        assert_equal csat[:comment], "", "Nil comments should be handled"
      end
    end
  end
end
