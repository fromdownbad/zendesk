require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::Requests::CommentPresenter do
  extend Api::Presentation::TestHelper

  fixtures :attachments, :events, :users, :tickets, :ticket_fields, :organizations

  before do
    @model = events(:serialization_comment)
    @attachment = attachments(:serialization_comment_attachment)
    @model.attachments << @attachment
    brand = FactoryBot.create(:brand, account_id: @model.account_id)
    @presenter = Api::Mobile::Requests::CommentPresenter.new(@model.account.owner, url_builder: mock_url_builder, brand: brand)
  end

  describe "#requester_id" do
    it "presents the request token if available" do
      ticket = @model.ticket
      ticket.stubs(:token).returns("foo")

      assert_equal ticket.token, @presenter.model_json(@model)[:request_id]
    end
  end
end
