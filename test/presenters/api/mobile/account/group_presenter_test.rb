require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::Mobile::Account::GroupPresenter do
  extend Api::Presentation::TestHelper

  fixtures :groups, :accounts, :users, :role_settings

  before do
    @model     = groups(:with_groups_group1)
    @presenter = Api::Mobile::Account::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder)
  end

  should_present_keys :id, :name, :description, :agents, :deleted

  should_present_method :id
  should_present_method :name
  should_present_method :description

  it "does show the correct amount of users" do
    assert_equal @model.users.active.count(:all), @presenter.model_json(@model)[:agents].count
  end

  it "does show the user role" do
    roles = @model.users.map { |user| user.role.downcase }

    assert_equal roles, @presenter.model_json(@model)[:agents].force.map { |agent| agent[:role] }
  end

  describe "don't show deleted users" do
    before do
      @model = groups(:with_groups_group1)
      removed_user = @model.users.first
      removed_user.current_user = users(:minimum_admin)
      removed_user.delete!
      @model.reload
      @presenter = Api::Mobile::Account::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    end

    it "does not show deleted user" do
      assert_equal @model.users.active.count(:all), @presenter.model_json(@model)[:agents].count
    end
  end

  describe "don't show light agents" do
    before do
      @model = groups(:with_groups_group1)
      # Create light user
      Account.any_instance.stubs(:has_light_agents?).returns(true)
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
      @light_agent = User.create! do |user|
        user.account = @model.account
        user.name = "Prince Light"
        user.email = "prince@example.org"
        user.roles = Role::AGENT.id
      end
      @light_agent.permission_set = PermissionSet.create_light_agent!(@model.account)
      @light_agent.groups = [@model]
      @light_agent.save!
      @model.reload

      @presenter = Api::Mobile::Account::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    end

    it "does not show light agent" do
      non_light_agents = @model.users.agents.active - @model.users.light_agents.active
      assert_equal non_light_agents.count, @presenter.model_json(@model)[:agents].count
    end
  end

  describe "does show all agents" do
    before do
      @model = groups(:with_groups_group1)
      Account.any_instance.stubs(:has_light_agents?).returns(true)
      Account.any_instance.stubs(:has_permission_sets?).returns(true)
      # Admin with light agent permissions in ZD#1798917
      CIA.audit actor: users(:with_groups_admin) do
        @admin = User.create! do |user|
          user.account = @model.account
          user.name = "Prince Admin"
          user.email = "prince@example.org"
          user.roles = Role::ADMIN.id
        end
      end
      @admin.permission_set = PermissionSet.create_light_agent!(@model.account)
      @admin.groups = [@model]
      @admin.save!
      @model.reload

      @presenter = Api::Mobile::Account::GroupPresenter.new(@model.account.owner, url_builder: mock_url_builder)
    end

    it "does show admins that have the light agent permission set" do
      assert_not_nil @presenter.model_json(@model)[:agents].force.find { |v| v[:id] == @admin.id }
    end
  end

  it 'does not do n+1' do
    account = accounts(:minimum)
    Account.any_instance.stubs(:has_light_agents?).returns(true)
    Account.any_instance.stubs(:has_permission_sets?).returns(true)

    models = [
      groups(:minimum_group),
      groups(:support_group),
      groups(:inactive_group),
      account.groups.create(name: 'More groups'),
      account.groups.create(name: 'MOAR groups')
    ]

    user = users(:minimum_search_user)
    models.each { |group| account.memberships.create(group: group, user: user) }

    assert_sql_queries(2) do
      @presenter.present(models)
    end
  end
end
