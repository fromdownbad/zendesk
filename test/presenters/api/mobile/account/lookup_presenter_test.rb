require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Api::Mobile::Account::LookupPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :account_settings, :role_settings, :remote_authentications

  def assert_correct_url(expected, recieved)
    expected_uri = URI(expected)
    received_uri = URI(recieved)
    assert_equal "https", received_uri.scheme
    assert_equal account.default_host, received_uri.host
    assert_equal expected_uri.path, received_uri.path
  end

  let(:account) { accounts(:minimum) }
  let(:remote_ip) { '1.2.3.4' }

  before do
    @model = account
    Rails.application.routes.default_url_options[:host] = account.host_name
    @url_builder = Rails.application.routes.url_helpers
    @agent_logins = Zendesk::Auth::AgentLogins.for(account, remote_ip)
    @presenter = Api::Mobile::Account::LookupPresenter.new(account.owner, url_builder: @url_builder, ip_address: remote_ip, agent_logins: @agent_logins)
    @output = @presenter.model_json(account)
  end

  should_present_keys(
    :subdomain,
    :url,
    :name,
    :agent_logins,
    :active_products
  )

  it "displays the Zendesk login" do
    body = @presenter.present(account)[:lookup]

    assert body[:agent_logins]
    zendesk_login = body[:agent_logins].detect { |al| al[:service] == :zendesk }
    assert zendesk_login
    assert_correct_url @url_builder.access_oauth_mobile_url, zendesk_login[:url]
  end

  it 'presents the agent logins even if no user is given' do
    @presenter = Api::Mobile::Account::LookupPresenter.new(nil, url_builder: @url_builder, ip_address: remote_ip, agent_logins: @agent_logins)

    expected = [{
      service: :zendesk,
      url: 'https://minimum.zendesk-test.com/access/oauth_mobile'
    }]

    result = @presenter.model_json(@model)

    assert_equal expected, result[:agent_logins]
  end

  describe "for accounts with Google sso enabled" do
    before do
      account.role_settings.agent_google_login = true
      account.role_settings.agent_zendesk_login = true
      @agent_logins = Zendesk::Auth::AgentLogins.for(account, remote_ip)
      @presenter = Api::Mobile::Account::LookupPresenter.new(account.owner, url_builder: @url_builder, ip_address: remote_ip, agent_logins: @agent_logins)
      @output = @presenter.model_json(account)
    end

    it "shows the google sso links" do
      body = @presenter.present(account)[:lookup]
      assert body[:agent_logins]
      google_login = body[:agent_logins].detect { |al| al[:service] == :google }
      assert google_login
      assert_correct_url @url_builder.access_google_url(google_domain: account.authentication_domain, profile: 'google'), google_login[:url]
      assert_correct_url @url_builder.access_google_play_url, google_login[:play_url]
    end
  end

  describe "for accounts with saml enabled" do
    before do
      Account.any_instance.stubs(has_saml?: true)
      @auth                  = remote_authentications(:minimum_remote_authentication)
      @auth.fingerprint      = "44:D2:9D:98:49:66:27:30:3A:67:A2:5D:97:62:31:65:57:9F:57:D1"
      @auth.is_active        = true
      @auth.remote_login_url = "https://idp.example.org/saml"
      @auth.auth_mode        = RemoteAuthentication::SAML
      @auth.save!

      account.role_settings.agent_remote_login = true
      account.role_settings.save!

      @agent_logins = Zendesk::Auth::AgentLogins.for(account, remote_ip)
      @presenter = Api::Mobile::Account::LookupPresenter.new(account.owner, url_builder: @url_builder, ip_address: remote_ip, agent_logins: @agent_logins)
      @output = @presenter.model_json(account)
    end

    it "shows the saml link" do
      body = @presenter.present(account)[:lookup]
      body[:agent_logins]
      remote_login = body[:agent_logins].detect { |al| al[:service] == :remote }
      assert remote_login[:url].starts_with?(@auth.remote_login_url)
    end
  end

  describe "for hostmapped accounts" do
    before do
      account.role_settings.agent_google_login = true
      account.role_settings.agent_zendesk_login = true
      account.host_mapping = "wibble.example.com"
      account.save
      @agent_logins = Zendesk::Auth::AgentLogins.for(account, remote_ip)
      @presenter = Api::Mobile::Account::LookupPresenter.new(account.owner, url_builder: @url_builder, ip_address: remote_ip, agent_logins: @agent_logins)

      @output = @presenter.model_json(account)
    end

    it "shows the google sso links" do
      body = @presenter.present(account)[:lookup]

      assert_correct_url body[:url], account.url(mapped: false, protocol: 'https')
      assert body[:agent_logins]

      google_login = body[:agent_logins].detect { |al| al[:service] == :google }

      assert google_login
      assert_correct_url @url_builder.access_google_url(google_domain: account.authentication_domain, profile: 'google'), google_login[:url]
      assert_correct_url @url_builder.access_google_play_url, google_login[:play_url]
    end
  end
end
