require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::Users::EndUserPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint
  extend Api::V2::TestHelper
  include CustomFieldsTestHelper

  fixtures :users, :user_settings, :accounts, :organizations, :translation_locales, :organization_memberships, :photos

  before do
    @model = users(:minimum_sdk_end_user)
    @model.photo = FactoryBot.create(:photo, filename: "large_1.png")
    @model.save!

    @presenter = Api::Mobile::Users::EndUserPresenter.new(@model, url_builder: mock_url_builder)
  end

  should_present_keys :tags, :user_fields
  should_present_method :tags

  it "has 'user' as the root key" do
    assert_equal :user, @presenter.model_key
  end

  describe "Custom Fields" do
    before do
      @account = @model.account
      create_user_custom_field!(:text_cust_field, 'Sample test user field', 'Text')
      create_user_custom_field!(:decimal_cust_field, 'Sample decimal user field', 'Decimal')
      create_user_custom_field!(:integer_cust_field, 'Sample integer user field', 'Integer')

      @model.custom_field_values.update_from_hash(text_cust_field: "sample_text",
                                                  decimal_cust_field: 1.0,
                                                  integer_cust_field: 1)

      @model.save!
    end

    it "should include the 'user_fields' key" do
      assert @presenter.model_json(@model)[:user_fields]
    end

    it "should include the correct values for the subkeys of the 'user_fields' key" do
      assert_equal "sample_text", @presenter.model_json(@model)[:user_fields]["text_cust_field"]
      assert_equal 1.0, @presenter.model_json(@model)[:user_fields]["decimal_cust_field"]
      assert_equal 1, @presenter.model_json(@model)[:user_fields]["integer_cust_field"]
    end
  end

  describe "Tags" do
    before do
      @model.stubs(:tags).returns(["foo", "bar"])
    end

    it "should include the 'tags' key" do
      assert @presenter.model_json(@model)[:tags]
    end

    it "should include the correct values for the subkeys of the 'tags' key" do
      assert_equal ["foo", "bar"], @presenter.model_json(@model)[:tags]
    end
  end
end
