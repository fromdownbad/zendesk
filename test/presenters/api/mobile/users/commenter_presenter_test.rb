require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::Users::EndUserPresenter do
  extend Api::Presentation::TestHelper
  extend Api::Presentation::Lint
  extend Api::V2::TestHelper
  include CustomFieldsTestHelper

  fixtures :users, :user_settings, :accounts, :organizations, :translation_locales, :organization_memberships, :photos

  before do
    @account = accounts(:minimum_sdk)
    @model = users(:minimum_agent)
    @model.photo = FactoryBot.create(:photo, filename: "large_1.png")
    @model.save!

    @presenter = Api::Mobile::Users::CommenterPresenter.new(@model, url_builder: mock_url_builder)
    @presenter.stubs(:user).returns(users(:minimum_end_user))
  end

  should_present_keys :id, :name, :photo

  it "has 'user' as the root key" do
    assert_equal :user, @presenter.model_key
  end

  describe_with_and_without_arturo_enabled :agent_display_names do
    let(:payload) { @presenter.present(@model) }

    it "will not present alias if not set" do
      assert_nil @model.alias
      assert_equal @model.name, payload[:user][:name]
    end

    it "will present alias if set and arturo enabled" do
      @model.update_attribute(:alias, "Agent Alias")

      if @arturo_enabled
        assert_equal @model.alias, payload[:user][:name]
      else
        assert_equal @model.name, payload[:user][:name]
      end
    end
  end
end
