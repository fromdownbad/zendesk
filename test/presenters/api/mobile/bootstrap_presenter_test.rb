require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::Mobile::BootstrapPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
    Subscription.any_instance.stubs(:voice_account).returns(nil)
    Subscription.any_instance.stubs(:guide_preview).returns(nil)
    Subscription.any_instance.stubs(:suite_allowed?).returns(anything)

    Rails.application.routes.default_url_options[:host] = account.host_name
    @presenter = Api::Mobile::BootstrapPresenter.new(user, url_builder: mock_url_builder)
    @output = @presenter.present(user)
  end

  it "presents the correct keys" do
    expected_keys = [
      :account_configuration,
      :account_settings,
      :account_subscription,
      :current_user,
      :current_user_organizations,
      :current_user_identities,
      :current_user_roles,
      :current_user_abilities,
      :brands,
      :macros,
      :ticket_forms,
      :ticket_fields,
      :organizations,
      :assignable_groups
    ]
    assert_equal expected_keys.sort, @output.keys.sort
  end

  it "displays the correct user" do
    assert_equal user.id, @output[:current_user][:id]
  end

  it "displays the correct data" do
    if user.account.has_multibrand?
      assert_equal user.account.brands.count(:all), @output[:brands].count
    else
      assert_nil @output[:brands]
    end

    if user.account.has_ticket_forms?
      assert_equal user.account.ticket_forms.count(:all), @output[:ticket_forms].count
    else
      assert_nil @output[:ticket_forms]
    end

    assert_equal user.shared_and_personal_macros.active.count(:all), @output[:macros].count
    assert_equal user.account.organizations.count(:all), @output[:organizations].count
    assert_equal user.preloaded_assignable_groups.count, @output[:assignable_groups].count
  end
end
