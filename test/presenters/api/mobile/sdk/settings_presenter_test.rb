require_relative "../../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"

SingleCov.covered!

describe Api::Mobile::Sdk::SettingsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :mobile_sdk_apps

  let(:account) { accounts(:minimum) }
  let(:mobile_sdk_app) { mobile_sdk_apps(:minimum_sdk) }
  let(:oauth_client) { FactoryBot.create(:client, account: account) }
  let(:mobile_sdk_auth) { FactoryBot.create(:mobile_sdk_auth, account: account, client: oauth_client) }

  before do
    @model = mobile_sdk_app
    @url_builder = Rails.application.routes.url_helpers
    @presenter = Api::Mobile::Sdk::SettingsPresenter.new(account.owner, url_builder: @url_builder)
    @output = @presenter.model_json(mobile_sdk_app)
  end

  should_present_keys(
    :identifier,
    :authentication,
    :rma,
    :conversations,
    :contact_us,
    :brand_id,
    :help_center,
    :blips,
    :ticket_forms,
    :updated_at
  )

  it "displays the correct app" do
    assert_equal mobile_sdk_app.identifier, @output[:identifier]
  end

  describe "updated_at" do
    before do
      mobile_sdk_app.settings.rma_tags = %i[one two]
      mobile_sdk_app.save
      @last_updated_at = mobile_sdk_app.settings.find { |s| s.name = "rma_tags" }.updated_at
      @output = @presenter.model_json(mobile_sdk_app)
    end

    it "displays the correct updated_at" do
      assert_equal @last_updated_at, @output[:updated_at]
    end
  end

  describe "blips" do
    describe_with_arturo_disabled :mobile_sdk_blip_collection_control do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert_equal %i[permissions], output[:blips].keys
      end
    end

    describe_with_arturo_enabled :mobile_sdk_blip_collection_control do
      it "has correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert_equal %i[permissions collect_only_required_data], output[:blips].keys
      end
    end

    it "has correct permissions fields" do
      assert_equal %i[required behavioural pathfinder], @output[:blips][:permissions].keys
    end
  end

  describe "help_center" do
    describe_with_and_without_arturo_enabled :mobile_sdk_help_center_article_voting_enabled do
      it "has the correct fields" do
        output = @presenter.model_json(mobile_sdk_app)

        assert_equal %i[enabled help_center_article_voting_enabled], output[:help_center].keys
      end
    end
  end
end
