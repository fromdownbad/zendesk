require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Services::Salesforce::ObjectsPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:object) do
    { label: "AccountLabel", name: "AccountName" }.stringify_keys
  end
  let(:object_response) do
    { label: "AccountLabel", api_name: "AccountName" }
  end

  let(:minimum_account) { accounts(:minimum) }

  let(:presenter) { Api::Services::Salesforce::ObjectsPresenter.new(minimum_account, url_builder: mock_url_builder) }

  it "presents object" do
    json = presenter.model_json(object)
    assert_equal object_response, json
  end
end
