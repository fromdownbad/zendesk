require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Services::Salesforce::ConfigurationPresenter do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  let(:configuration) do
    { objects: [] }
  end
  let(:config_settings) do
    { config_settings: { objects: [] } }
  end

  let(:minimum_account) { accounts(:minimum) }

  let(:presenter) { Api::Services::Salesforce::ConfigurationPresenter.new(minimum_account, url_builder: mock_url_builder) }

  it "presents configSettings" do
    json = presenter.present(configuration)
    assert_equal config_settings, json
  end
end
