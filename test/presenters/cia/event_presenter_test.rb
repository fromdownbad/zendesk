require_relative "../../support/test_helper"

SingleCov.covered!

describe CIA::EventPresenter do
  fixtures :accounts, :rules, :users
  fixtures :translation_locales

  def timestamp
    Time.parse("2013-01-08 23:49:00 UTC")
  end

  let(:view_context) { stub(:view_context) }

  let(:cia_event_presenter) do
    CIA::EventPresenter.new(view_context, audit_event)
  end

  let(:actor) { users(:minimum_end_user) }

  let(:audit_event) do
    CIA::Event.new(
      action: "update",
      account: accounts(:minimum),
      actor: actor,
      ip_address: "1.1.1.1",
      created_at: timestamp
    )
  end

  before do
    Timecop.freeze(timestamp)
  end

  describe "#date" do
    it "renders the formatted date" do
      view_context.expects(:format_date).returns("date_string")
      view_context.expects(:format_time).returns("time_string")
      view_context.expects(:link_to).
        with("date_string", filter: {created_at: ['2013-01-08 00:00:00', '2013-01-08 23:59:59']}).
        returns("date_link")

      assert_equal "date_link time_string", cia_event_presenter.date
    end
  end

  describe '#source_label' do
    describe 'from a SubscriptionFeature event' do
      let(:source)       { SubscriptionFeature.new }
      let(:feature_name) { 'my_awesome_feature' }

      before do
        audit_event.stubs(:source_type).returns('SubscriptionFeature')
        audit_event.stubs(:source).returns(source)
        source.stubs(:name).returns(feature_name)
      end

      it 'returns the source name' do
        cia_event_presenter.send(:source_label).must_equal feature_name
      end
    end

    describe "system dynamic content in a view title" do
      let(:account) { accounts(:minimum) }

      before do
        view = account.views.first
        view.update_attribute(:title, '{{zd.your_unsolved_tickets}}')

        audit_event.stubs(:source_type).returns('View')
        audit_event.stubs(:source_display_name).returns(view.title)
        audit_event.stubs(:source).returns(view)
      end

      it "renders the dynamic content" do
        cia_event_presenter.send(:source_label).must_match(/#{I18n.t('txt.default.views.your_unsolved.title')}/)
      end
    end

    describe 'from a Trigger event' do
      let(:source) { Trigger.new }

      before do
        audit_event.stubs(:source_type).returns('Trigger')
        audit_event.stubs(:source).returns(source)
        audit_event.stubs(:source_display_name).returns('Trigger')
      end

      it 'returns the source name' do
        cia_event_presenter.send(:source_label).must_equal 'Trigger: Trigger'
      end
    end

    describe 'from a Role setting' do
      let(:source) { RoleSettings.new }

      before do
        audit_event.stubs(:source_type).returns('RoleSettings')
        audit_event.stubs(:source).returns(source)
      end

      it 'returns the source name' do
        cia_event_presenter.send(:source_label).must_equal 'Login settings'
      end
    end

    describe "missing source reporting" do
      before do
        audit_event.stubs(:source)
        audit_event.stubs(:source_display_name)
        audit_event.stubs(:source_type).returns('SubscriptionFeature')
        audit_event.visible = true
      end

      it "tracks" do
        Zendesk::StatsD::Client.any_instance.stubs(:increment)

        tags = %w[action:update key:txt.admin.views.reports.tabs.audits.model.subscription_feature source_type:SubscriptionFeature]
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('translation_missing', tags: tags).once

        cia_event_presenter.source_label
        cia_event_presenter.send(:translated_source)
      end
    end

    describe "from source table with bad inheritance type value" do
      let(:account) { accounts(:minimum) }
      let(:audit_event) do
        CIA::Event.new(
          account: account,
          actor: users(:minimum_end_user),
          created_at: timestamp,
          source_id: 1,
          source_type: 'Token',
          action: "create"
        )
      end

      before do
        # Have to insert this raw as ActiveRecord will rightly refuse to put this record since this relationship was deprecated.
        legacy_token = "INSERT INTO tokens (id, source_type, source_id, account_id, type, value, token_crc) VALUES (1, 'Account', #{account.id}, #{account.id}, 'LegacyApiToken', 'abc', 123)"
        ActiveRecord::Base.connection.insert(legacy_token)
      end

      it "handles SubclassNotFound activerecord error" do
        cia_event_presenter.send(:source_label).must_equal 'API token'
      end
    end
  end

  describe '#english_source_label' do
    let(:account) { accounts(:minimum) }
    before do
      account.translation_locale = translation_locales(:japanese)
    end

    describe 'from a Trigger event' do
      let(:source) { Trigger.new }

      before do
        audit_event.stubs(:source_type).returns('Trigger')
        audit_event.stubs(:source).returns(source)
        audit_event.stubs(:source_display_name).returns('Trigger')
      end

      it 'returns the source name' do
        cia_event_presenter.send(:english_source_label).must_equal 'Trigger: Trigger'
      end
    end

    describe 'from a User event' do
      let(:source) { User.new }

      before do
        audit_event.stubs(:source_type).returns('User')
        audit_event.stubs(:source).returns(source)
        audit_event.stubs(:source_display_name).returns('User')
      end

      it 'returns the source name' do
        cia_event_presenter.send(:english_source_label).must_equal 'User'
      end
    end

    describe 'from a SubscriptionFeature event' do
      let(:source) { SubscriptionFeature.new }
      let(:feature_name) { 'my_awesome_feature' }

      before do
        audit_event.stubs(:source_type).returns('SubscriptionFeature')
        audit_event.stubs(:source).returns(source)
        source.stubs(:name).returns(feature_name)
      end

      it 'returns the source name' do
        cia_event_presenter.send(:english_source_label).must_equal feature_name
      end
    end

    describe 'from a Macro event' do
      let(:source) { Macro.new }

      before do
        audit_event.stubs(:source_type).returns('Macro')
        audit_event.stubs(:source).returns(source)
      end

      it 'returns the source name' do
        cia_event_presenter.send(:english_source_label).must_equal 'Macro'
      end
    end

    describe "system dynamic content in a view title" do
      before do
        view = account.views.first
        view.update_attribute(:title, '{{zd.your_unsolved_tickets}}')
        audit_event.stubs(:source_type).returns('View')
        audit_event.stubs(:source_display_name).returns(view.title)
        audit_event.stubs(:source).returns(view)
      end

      it "renders the dynamic content" do
        cia_event_presenter.send(:english_source_label).must_equal 'View: Your unsolved tickets'
      end
    end

    describe 'from a Role setting' do
      let(:source) { RoleSettings.new }

      before do
        audit_event.stubs(:source_type).returns('RoleSettings')
        audit_event.stubs(:source).returns(source)
      end

      it 'returns the source name' do
        cia_event_presenter.send(:english_source_label).must_equal 'Login settings'
      end
    end
  end

  describe "#english_only__source" do
    let(:english_source) { cia_event_presenter.send(:english_only_source) }
    let(:setting_name) { 'experiments_allowed' }

    describe "with a source" do
      before do
        audit_event.stubs(:source_type).returns('AccountSetting')
        source = AccountSetting.new
        audit_event.stubs(:source).returns(source)
        source.stubs(:name).returns(setting_name)
      end

      it "falls back to source_type" do
        english_source.must_equal 'Experiments Allowed'
      end
    end
  end

  describe "#translated_source" do
    let(:translated_source) { cia_event_presenter.send(:translated_source) }

    before do
      audit_event.stubs(:source_type).returns("AccountSetting")
    end

    describe "with a missing source" do
      before do
        audit_event.stubs(:source)
      end

      it "falls back to source_type" do
        Rails.logger.expects(:warn).once
        translated_source.must_equal 'Account setting'
      end

      describe "because source is deleted" do
        it "does not record the exception" do
          audit_event.stubs(:action).returns("destroy")
          Rails.logger.expects(:warn).never
          translated_source.must_equal 'Account setting'
        end
      end

      describe "because source is soft_deleted" do
        before do
          audit_event.stubs(:attribute_changes).
            returns(
              [
                CIA::AttributeChange.new(
                  attribute_name: "deleted_at",
                  old_value: nil,
                  new_value: Time.now
                )
              ]
            )
        end

        it "does not record the exception" do
          Rails.logger.expects(:warn).never
          translated_source.must_equal 'Account setting'
        end
      end
    end

    describe "when the source results in a NameError" do
      before do
        audit_event.stubs(:source).raises(NameError.new('uninitialized constant Zuora::Subscription'))
      end

      it "falls back to source_type" do
        Rails.logger.expects(:warn).once
        translated_source.must_equal 'Account setting'
      end
    end

    describe 'badly formatted source type' do
      before do
        audit_event.stubs(:source_type).returns("Staff: John doe")
        audit_event.visible = true
        source = AccountSetting.new
        audit_event.stubs(:source).returns(source)
        source.stubs(:name).returns("foobazzle")
      end

      let(:missing) { 'translation missing: en-US-x-1.txt.admin.views.reports.tabs.audits.model.staff: john doe' }

      it "does not track" do
        Zendesk::StatsD::Client.any_instance.stubs(:increment)

        tags = ['action:update', "key:#{missing}", 'source_type:Staff: John doe']
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('translation_missing', tags: tags).never

        translated_source.must_equal missing
      end
    end

    describe 'Account Settings translations' do
      # Translations show up in Account Audits tab (for customers) and Monitor Account audits.
      #
      # If in doubt just add the translation. All Account Settings are auditable (see AccountSetting auditable_properties
      # call with `audit_all: true` in app/models/account/properties.rb), but only properties that create cia change
      # events will be translated (usually most of them will as we wrap our controller calls in CIA.audit).
      #
      # Exceptions can be allowed here:
      let(:account_settings_translation_allowlist) do
        %w[
          account_wide_api_rate_limit
          anonymous_attachment_limit
          anonymous_request_threshold
          archive
          braintree_migration_group
          collaborators_maximum
          comments_per_ticket_limit
          comments_reverse_order
          conditional_logging_pattern
          csat_hide_description
          csr_org_unit
          csv_export_bom
          csv_export_visible
          default_roles_with_sys_dc
          disabled_cdn
          domain_whitelist_blacklist_maximum_length
          end_user_entry_rate_limit
          export_role_id
          export_ticket_max
          extended_recoverable_ticket_age
          forum_social_sharing_enabled
          forums2
          forwarding_verification_token
          gooddata_dashboard_threads
          grandfathered_web_portal
          hc_stats_start_date
          hide_flag_to_end_user
          import_many_tickets_limit
          import_many_tickets_bytesize_limit
          inbox_addressing_name_template
          incremental_forum_search
          insights_user_name_removal
          job_statuses_limit
          last_login
          link_display_truncation_length
          many_organizations_limit
          many_tickets_limit
          many_users_limit
          max_identities
          max_number_attachments
          max_sdk_identities
          max_target_failures
          mobile_requests_index_cache_control_ttl
          moderation_throttle
          openid_enabled
          organizations_index_cache_control_header_ttl_seconds
          owned_by_zendesk
          parallel_automations_limit
          partner_name
          partner_url
          personal_rules
          prefer_gooddata_v1
          product_sign_up
          push_notification_device_deregistrations_threshold
          sso_bypass
          sso_bypass_can_be_disabled
          suppress_channelback
          ticket_deletions_threshold
          topic_suggestion_for_ticket_submission
          twitter_search_autoconversion
          twitter_ticket_monitoring_limit
          use_salesforce_production
          user_permanent_deletions_threshold
          verification_captcha_enabled
          voice_call_about_my_ticket
          voice_delay_before_pickup
          voice_make_agents_unavailable_after_missed_calls
          voice_sms
          xsell_source
          zuora_managed_billing
        ].freeze
      end

      it "should include translation" do
        properties = accounts(:minimum).settings.keys
        total      = properties - account_settings_translation_allowlist
        assert total.count > 0, 'Should select all account settings possibly included in change event audits'

        to_remove = []

        properties.each do |property|
          source = AccountSetting.new
          audit_event.stubs(:source).returns(source)
          source.stubs(:name).returns(property)
          translated_source = cia_event_presenter.send(:translated_source)

          missing = translated_source.match?(/translation missing/)
          allowed = account_settings_translation_allowlist.include?(property)

          refute missing, "Please add a translation for Account Setting '#{property}'" unless allowed

          to_remove << property if !missing && allowed
        end

        raise "Please remove #{to_remove.inspect} from Account Settings translation allowed list in CIA::EventPresenter tests" unless to_remove.empty?
      end

      describe "with a missing translation" do
        before do
          audit_event.stubs(:source_type).returns("AccountSetting")
          source = AccountSetting.new
          audit_event.stubs(:source).returns(source)
          source.stubs(:name).returns("foobazzle")
        end

        describe 'audit is visible' do
          before { audit_event.visible = true }

          it "tracks" do
            Zendesk::StatsD::Client.any_instance.stubs(:increment)

            tags = %w[action:update key:txt.admin.views.reports.tabs.audits.property.object.foobazzle source_type:AccountSetting]
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('translation_missing', tags: tags)

            translated_source.must_equal "translation missing: en-US-x-1.txt.admin.views.reports.tabs.audits.property.object.foobazzle"
          end
        end

        describe 'audit is not visible' do
          before { audit_event.visible = false }

          it "does not track" do
            Zendesk::StatsD::Client.any_instance.stubs(:increment)

            tags = %w[action:update key:txt.admin.views.reports.tabs.audits.property.object.foobazzle source_type:AccountSetting]
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('translation_missing', tags: tags).never

            translated_source.must_equal "translation missing: en-US-x-1.txt.admin.views.reports.tabs.audits.property.object.foobazzle"
          end
        end
      end
    end
  end

  describe "#action" do
    it "shows always 'update' for properties" do
      audit_event.stubs(:action).returns("create")
      cia_event_presenter.stubs(:property_set_setting?).returns(true)
      assert_equal "update", cia_event_presenter.action
    end

    it "shows always 'destroy' for soft deleted objects" do
      audit_event.stubs(:attribute_changes).
        returns(
          [
            CIA::AttributeChange.new(
              attribute_name: "deleted_at",
              old_value: nil,
              new_value: Time.now
            )
          ]
        )
      assert_equal "destroy", cia_event_presenter.action
    end

    it "shows the original audit_event action for the rest of the objects" do
      audit_event.stubs(:action).returns("create")
      cia_event_presenter.stubs(:property_set_setting?).returns(false)
      assert_equal "create", cia_event_presenter.action
    end
  end

  describe "#action_label" do
    it "renders the translated action label" do
      assert_equal "Updated", cia_event_presenter.action_label
    end

    describe "with a missing translation" do
      before do
        audit_event.visible = true
        cia_event_presenter.stubs(:action).returns("foobazzle")
      end

      it "tracks" do
        Zendesk::StatsD::Client.any_instance.stubs(:increment)

        tags = %w[action:foobazzle key:txt.admin.views.reports.tabs.audits.event.foobazzle source_type:]
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('translation_missing', tags: tags)

        cia_event_presenter.action_label.must_equal "translation missing: en-US-x-1.txt.admin.views.reports.tabs.audits.event.foobazzle"
      end

      it 'uses current-users locale by default' do
        actor.stubs(:locale).returns('fr')
        CIA.audit(actor: actor) do
          cia_event_presenter.action_label.must_equal "translation missing: fr.txt.admin.views.reports.tabs.audits.event.foobazzle"
        end
      end

      it 'uses account locale as fallback' do
        Account.any_instance.stubs(:translation_locale).returns('aus')
        cia_event_presenter.action_label.must_equal "translation missing: aus.txt.admin.views.reports.tabs.audits.event.foobazzle"
      end
    end

    describe "with a login action" do
      before do
        audit_event.visible = true
        audit_event.action = 'login'
      end

      describe_with_arturo_disabled :audit_logs_api_include_login_events do
        it "has a translation missing error for action_label" do
          cia_event_presenter.action_label.must_equal "Sign in"
        end
      end

      describe_with_arturo_enabled :audit_logs_api_include_login_events do
        it "shows action_label as audit" do
          cia_event_presenter.action_label.must_equal "Sign in"
        end
      end
    end
  end

  describe "#ip_address_text" do
    it "shows for regular users" do
      view_context.expects(:link_to).
        with(audit_event.ip_address, filter: {ip_address: audit_event.ip_address}).
        returns("ip_address_link")

      assert_equal "ip_address_link", cia_event_presenter.ip_address_text
    end

    it "does not show for system users" do
      audit_event.actor = User.system
      view_context.expects(:link_to).never

      assert_nil cia_event_presenter.ip_address_text
    end

    it "does not show for unknown ip addresses" do
      audit_event.ip_address = ""
      view_context.expects(:link_to).never

      assert_nil cia_event_presenter.ip_address_text
    end
  end

  describe "#source_link" do
    it "renders User" do
      audit_event.source = User.new do |user|
        user.roles = Role::AGENT.id
        user.name = "James Bond"
      end
      audit_event.source.id = 123

      view_context.
        expects(:link_to).with("James Bond", anything).returns("user_link")

      assert_equal "user_link", cia_event_presenter.source_link
    end

    it "renders Ticket records with the nice id" do
      audit_event.source = Ticket.new do |ticket|
        ticket.status_id = 5
        ticket.nice_id   = 2
        ticket.id        = 123
      end

      view_context.
        expects(:link_to).with("Ticket: #2", anything).returns "ticket_link"

      assert_equal "ticket_link", cia_event_presenter.source_link
    end

    it "renders the correct Rule subclass" do
      audit_event.source = Trigger.new(title: "My cool trigger")
      audit_event.source_type = "Trigger" # this is done in the controller

      view_context.
        expects(:link_to).with("Trigger: My cool trigger", filter: {source_type: 'rule', source_id: nil}).returns "trigger_link"

      assert_equal "trigger_link", cia_event_presenter.source_link
    end

    it "renders the message for Apps auditing" do
      audit_event.source_type = "Zendesk::AppMarket::App"
      audit_event.source_display_name = 'Funky'

      Rails.logger.expects(:warn).once

      view_context.
        expects(:link_to).with("App: Funky", anything).returns "app_link"

      assert_equal "app_link", cia_event_presenter.source_link
    end

    it "renders the message for Apps installation auditing" do
      audit_event.source_display_name = "Funky"
      audit_event.source_type = "Zendesk::AppMarket::Installation"

      Rails.logger.expects(:warn).once
      view_context.
        expects(:link_to).with("App Installation: Funky", anything).returns "installation_link"

      assert_equal "installation_link", cia_event_presenter.source_link
    end

    it "renders other" do
      audit_event.source = Account.new do |account|
        account.id = 123
      end

      view_context.
        expects(:link_to).with("Account", anything).returns "account_link"

      assert_equal "account_link", cia_event_presenter.source_link
    end

    it "renders unknown" do
      audit_event.source = SuspendedTicket.new do |suspended_ticket|
        suspended_ticket.id = 123
      end

      view_context.
        expects(:link_to).with { |text, _options| assert_match(/translation missing/, text); true }.
        returns("unknown_link")

      assert_equal "unknown_link", cia_event_presenter.source_link
    end
  end

  describe "#actor_link" do
    let(:base_filter) do
      {actor_type: "User", actor_id: 1}
    end

    it "renders a link to the actor" do
      filter = {filter: base_filter.merge(actor_id: users(:minimum_end_user).id)}

      view_context.
        expects(:link_to).with("minimum_end_user", filter, anything).returns("actor_link")

      assert_equal "actor_link", cia_event_presenter.actor_link
    end

    it "renders a link to an inactive actor" do
      view_context.expects(:link_to).returns("actor_link")
      audit_event.actor.stubs(:is_active?).returns(false)

      assert_equal "deleted actor_link", cia_event_presenter.actor_link
    end

    it "renders a link to an obfuscated zendesk actor" do
      audit_event.actor  = users(:systemuser)
      audit_event.via_id = Zendesk::Types::ViaType.MONITOR_EVENT

      filter = {filter: base_filter.merge(actor_id: users(:systemuser).id)}
      view_context.
        expects(:link_to).with("Zendesk", filter, anything).returns("actor_link")

      assert_equal "actor_link", cia_event_presenter.actor_link
    end

    describe "with an actor that cannot be found" do
      before do
        audit_event.actor      = nil
        audit_event.actor_type = "User"
        audit_event.actor_id   = 12121212
      end

      it "renders a link" do
        filter = {filter: base_filter.merge(actor_id: 12121212)}
        view_context.
          expects(:link_to).with("User: 12121212", filter, anything).returns("actor_link")

        assert_equal "actor_link", cia_event_presenter.actor_link
      end

      describe "with a missing translation" do
        before do
          audit_event.actor_type.stubs(:underscore).returns("foobazzle")
          audit_event.visible = true
          view_context.expects(:link_to).returns("actor_link")
        end

        it "tracks" do
          Zendesk::StatsD::Client.any_instance.stubs(:increment)

          tags = %w[action:update key:txt.admin.views.reports.tabs.audits.model.foobazzle source_type:]
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('translation_missing', tags: tags)

          assert_equal "actor_link", cia_event_presenter.actor_link
        end
      end
    end

    it "renders 'unknown' for other actors (e.g. monitor users)" do
      audit_event.actor      = nil
      audit_event.actor_id   = 12121212

      assert_equal "Unknown", cia_event_presenter.actor_link
    end

    it "renders 'unknown' when actor is nil" do
      audit_event.actor = nil

      assert_equal "Unknown", cia_event_presenter.actor_link
    end
  end

  describe "#change_descriptions" do
    describe "standard attribute changes" do
      before do
        audit_event.stubs(:attribute_changes).
          returns(
            [
              CIA::AttributeChange.new(
                attribute_name: "roles",
                old_value: "2",
                new_value: "4"
              ),
              CIA::AttributeChange.new(
                attribute_name: "billing_cycle_type",
                old_value: "1",
                new_value: "2"
              )
            ]
          )
      end

      it "renders the event changes" do
        expected_output = ["Role changed from Administrator to Agent",
                           "Billing cycle type changed from Monthly to Quarterly"]

        assert_equal(expected_output, cia_event_presenter.change_descriptions)
      end

      it "returns an empty array if the audit_event action is 'created'" do
        audit_event.action = "create"

        assert_equal([], cia_event_presenter.change_descriptions)
      end

      it "returns an empty array if the audit_event action is 'destroyed'" do
        audit_event.action = "destroy"

        assert_equal([], cia_event_presenter.change_descriptions)
      end
    end

    describe "audit action descriptions" do
      before do
        audit_event.stubs(
          action:  "audit",
          message: "txt.admin.views.reports.tabs.audits.assumption_bypass"
        )
      end

      it "renders the event changes" do
        expected_output = [I18n.t("txt.admin.views.reports.tabs.audits.assumption_bypass")]

        assert_equal(expected_output, cia_event_presenter.change_descriptions)
      end
    end

    describe "multiple level action descriptions" do
      before do
        audit_event.stubs({
          action: "update",
          status_bits: 2,
          message: JSON.dump({
            key: "txt.admin.views.reports.tabs.audits.change",
            attribute: "role",
            old: "txt.admin.helpers.user_helper.administrator_label",
            new: "Staff"
          })
        })
      end

      it "renders the event changes" do
        assert_equal(["role changed from Administrator to Staff"], cia_event_presenter.change_descriptions)
      end
    end

    describe "multiple level action descriptions displays values that appear to be translations keys" do
      before do
        audit_event.stubs({
          action: "update",
          status_bits: 2,
          message: JSON.dump({
            key: "txt.admin.views.reports.tabs.audits.change",
            attribute: "role",
            old: "txt.oops_no_translation",
            new: "Staff"
          })
        })
      end

      it "renders the event changes" do
        assert_equal(["role changed from txt.oops_no_translation to Staff"], cia_event_presenter.change_descriptions)
      end
    end

    describe "login action descriptions" do
      before do
        audit_event.stubs(
          action:  "login",
          message: "agent logged in from somewhere"
        )
      end

      describe 'audit_logs_export_api_enabled arturo disabled' do
        it "renders empty event changes" do
          assert_equal([], cia_event_presenter.change_descriptions)
        end
      end

      describe 'audit_logs_export_api_enabled arturo enabled' do
        before do
          audit_event.account.stubs(has_audit_logs_api_include_login_events?: true)
        end

        it "renders the event changes" do
          assert_equal(["agent logged in from somewhere"], cia_event_presenter.change_descriptions)
        end

        it 'adds login message to any attribute changes' do
          audit_event.stubs(:attribute_changes).
            returns([CIA::AttributeChange.new(attribute_name: "roles", old_value: "2", new_value: "4")])

          expected_output = ["Role changed from Administrator to Agent",
                             "agent logged in from somewhere"]
          assert_equal(expected_output, cia_event_presenter.change_descriptions)
        end

        it 'does not render any non-login messages' do
          audit_event.stubs(
            action:  "foo bar",
            message: "this should not be visible"
          )
          assert_equal([], cia_event_presenter.change_descriptions)
        end
      end
    end

    describe 'message rendering' do
      it 'does not render message text by default' do
        audit_event.stubs(
          message: "this should not be visible"
        )
        assert_equal([], cia_event_presenter.change_descriptions)
      end

      it 'renders the message if the status_bits == 1' do
        audit_event.stubs(
          status_bits: 1,
          message: 'this should be visible'
        )
        assert_equal(['this should be visible'], cia_event_presenter.change_descriptions)
      end

      it 'renders a translated message from JSON' do
        audit_event.stubs(
          status_bits: 2,
          message:
            '{
              "key": "txt.admin.views.people.groups.edit.delete_confirmation_route_to_new",
              "current_group_name": "foo-bar",
              "proposed_group_name": "foo-baz",
              "number": "123"
            }'
        )
        assert_equal(['Replace foo-bar with foo-baz as your default group for 123.'], cia_event_presenter.change_descriptions)
      end

      it 'renders a translated message from JSON with extra parameters' do
        audit_event.stubs(
          status_bits: 2,
          message:
            '{
              "key": "txt.admin.views.people.groups.edit.delete_confirmation_route_to_new",
              "current_group_name": "foo-bar",
              "proposed_group_name": "foo-baz",
              "number": "123",
              "unused": "who cares"
            }'
        )
        assert_equal(['Replace foo-bar with foo-baz as your default group for 123.'], cia_event_presenter.change_descriptions)
      end

      it 'renders an empty message from JSON with too few parameters' do
        audit_event.stubs(
          status_bits: 2,
          message:
            '{
              "key": "txt.admin.views.people.groups.edit.delete_confirmation_route_to_new",
              "current_group_name": "foo-bar"
            }'
        )
        assert_equal([], cia_event_presenter.change_descriptions)
      end

      it 'does not render the message if the action is "create"' do
        audit_event.stubs(
          action: 'create',
          message: 'this should NOT be visible'
        )
        assert_equal([], cia_event_presenter.change_descriptions)
      end

      it 'renders the message if the status_bits == 1 and action is "create"' do
        audit_event.stubs(
          status_bits: 1,
          action: 'create',
          message: 'this should be visible'
        )
        assert_equal(['this should be visible'], cia_event_presenter.change_descriptions)
      end

      it 'renders the message if the status_bits == 1 and action is "destroy"' do
        audit_event.stubs(
          status_bits: 1,
          action: 'destroy',
          message: 'this should be visible'
        )
        assert_equal(['this should be visible'], cia_event_presenter.change_descriptions)
      end
    end
  end
end
