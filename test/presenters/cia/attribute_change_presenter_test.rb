require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe CIA::AttributeChangePresenter do
  fixtures :accounts, :users

  def timestamp
    Time.parse("2013-01-08 23:49:00 UTC")
  end

  def cia_attribute_change(name, old_value, new_value, attributes = {})
    CIA::AttributeChange.new(
      attributes.merge(
        attribute_name: name,
        old_value: old_value,
        new_value: new_value
      )
    )
  end

  let(:view_context) { stub(:view_context) }

  def change_presenter(name, old_value, new_value, attributes = {})
    CIA::AttributeChangePresenter.new(
      view_context,
      cia_attribute_change(name, old_value, new_value, attributes)
    )
  end

  before do
    view_context.stubs(:current_user).returns(users(:minimum_end_user))
    view_context.stubs(:current_account).returns(accounts(:minimum))
  end

  it "displays roles with role-ids changed attributes" do
    assert_equal(
      "Role changed from Administrator to Agent",
      change_presenter("roles", "2", "4").description
    )
  end

  it "displays roles with role strings changed attributes" do
    assert_equal(
      "Role changed from Staff to Admin",
      change_presenter("roles", "Staff", "txt.default.roles.support.admin.name").description
    )
  end

  it "displays agent_security_policy_id" do
    assert_equal(
      "Agent security policy changed from Low to Medium",
      change_presenter("agent_security_policy_id", 100, 200).description
    )
  end

  it "displays end_user_security_policy_id" do
    assert_equal(
      "End-user security policy changed from Low to Medium",
      change_presenter("end_user_security_policy_id", 100, 200).description
    )
  end

  it "displays start_time" do
    assert_equal(
      "Start time changed from Sunday 09:00 to Sunday 10:00",
      change_presenter("start_time", "540", "600").description
    )
  end

  it "displays end_time" do
    assert_equal(
      "End time changed from Sunday 17:00 to Sunday 16:00",
      change_presenter("end_time", "1020", "960").description
    )
  end

  it "displays name" do
    assert_equal(
      "Name changed from Christmas to Company Picnic",
      change_presenter("name", "Christmas", "Company Picnic").description
    )
  end

  it "displays default ticket form and previous" do
    CIA::AttributeChangePresenter.any_instance.stubs(:prev_default_form).returns("Previous Form")
    CIA::AttributeChangePresenter.any_instance.stubs(:event).returns(stub(source_display_name: "Other form"))
    assert_equal(
      "Set as default ticket form, previous: Previous Form",
      change_presenter("default", "0", "1").description
    )
  end

  it "translates old values if the content is an i18n key" do
    assert_equal(
      "Title changed from Notify all agents of received request to asd",
      change_presenter("title", "txt.default.triggers.notify_all_received.title", "asd").description
    )
  end

  it "translates new values if the content is an i18n key" do
    assert_equal(
      "Title changed from asd to Notify all agents of received request",
      change_presenter("title", "asd", "txt.default.triggers.notify_all_received.title").description
    )
  end

  describe "start_date" do
    describe "when the values are integers" do
      it "displays correctly" do
        assert_equal(
          "Start date changed from April 24, 2015 to April 27, 2015",
          change_presenter("start_date", "3400", "3403").description
        )
      end
    end

    describe "when the values are ISO 8601 dates" do
      it "displays correctly" do
        assert_equal(
          "Start date changed from April 24, 2015 to April 27, 2015",
          change_presenter("start_date", "2015-04-24", "2015-04-27").description
        )
      end
    end
  end

  describe "end_date" do
    describe "when the values are integers" do
      it "displays correctly" do
        assert_equal(
          "End date changed from April 24, 2015 to April 27, 2015",
          change_presenter("end_date", "3400", "3403").description
        )
      end
    end

    describe "when the values are ISO 8601 dates" do
      it "displays correctly" do
        assert_equal(
          "End date changed from April 24, 2015 to April 27, 2015",
          change_presenter("end_date", "2015-04-24", "2015-04-27").description
        )
      end
    end
  end

  it "displays time_zone" do
    assert_equal(
      "Time zone changed from Copenhagen to London",
      change_presenter(
        "time_zone",
        "Copenhagen",
        "London"
      ).description
    )
  end

  it "displays workweek" do
    assert_equal(
      CGI.escapeHTML(
        "Workweek changed from {\"mon\":{\"10:30\":\"15:30\"}} to " \
        "{\"mon\":{\"10:30\":\"15:30\"},\"tue\":{\"08:30\":\"13:30\"}}"
      ),
      change_presenter("workweek",
        "{\"mon\":{\"10:30\":\"15:30\"}}",
        "{\"mon\":{\"10:30\":\"15:30\"},\"tue\":{\"08:30\":\"13:30\"}}").description
    )
  end

  it "displays permission_set_id" do
    permission_set = view_context.current_account.permission_sets.create!(name: "Custom Role")

    assert_equal(
      "Role changed from not set to Custom Role",
      change_presenter("permission_set_id", nil, permission_set.id).description
    )
  end

  it "displays billing_cycle_type" do
    assert_equal(
      "Billing cycle type changed from Monthly to Quarterly",
      change_presenter("billing_cycle_type", "1", "2").description
    )
  end

  it "displays max_agents" do
    assert_equal(
      "Max. agents changed from 1 to 2",
      change_presenter("max_agents", "1", "2").description
    )
  end

  it "displays title" do
    assert_equal(
      "Title changed from a to b",
      change_presenter("title", "a", "b").description
    )
  end

  it "displays email" do
    assert_equal(
      "Email changed from a to b",
      change_presenter("email", "a", "b").description
    )
  end

  it "displays is_active" do
    assert_equal(
      "Status changed from active to inactive",
      change_presenter("is_active", "1", "0").description
    )
  end

  describe "state" do
    describe "no source type" do
      it "displays state" do
        assert_equal(
          "State changed from 1 to 0",
          change_presenter("state", 1, 0).description
        )
      end
    end

    describe "source type is Facebook::Page" do
      it "displays state with Facebook strings" do
        assert_equal(
          "State changed from #{Facebook::Page::STATE_TEXT[1]} to #{Facebook::Page::STATE_TEXT[5]}",
          change_presenter("state", "1", "5", source_type: 'Facebook::Page').description
        )
      end
    end

    describe "source type is MonitoredTwitterHandle" do
      it "displays state with MonitoredTwitterHandle strings" do
        assert_equal(
          "State changed from #{MonitoredTwitterHandle::STATE_TEXT[5]} to #{MonitoredTwitterHandle::STATE_TEXT[3]}",
          change_presenter("state", "5", "3", source_type: 'MonitoredTwitterHandle').description
        )
      end
    end
  end

  it "displays position" do
    assert_equal(
      "Position changed from 3 to 1",
      change_presenter("position", "3", "1").description
    )
  end

  it "does not raise an error when definition changes contain items with nil operators" do
    from = %([[],[],["set_tags enduser","ticket_type_id is 1","comment_value "]])
    to   = %([[],[],["set_tags enduser","ticket_type_id is 1","comment_value Incoming Call was received"]])
    expected_output = "<a href=\"#\" rel=\"tooltip\" title=\"From&lt;br/&gt;Actions: Set tags  enduser , Type is Question, and Response/description  (blank)&lt;br/&gt;To&lt;br/&gt;Actions: Set tags  enduser , Type is Question, and Response/description  Incoming Call was received'\">Definition: Changed</a>"

    view_context.
      expects(:link_to_function).
      with('Definition: Changed', nil, rel: 'tooltip', title: 'From<br/>Actions: Set tags  enduser , Type is Question, and Response/description  (blank)<br/>To<br/>Actions: Set tags  enduser , Type is Question, and Response/description  Incoming Call was received').
      returns(expected_output)

    assert_equal(
      expected_output,
      change_presenter("definition", from, to).description
    )
  end

  it "renders the definition using the RuleDictionary" do
    from = '[["status_id less_than 3","assignee_id is current_user"],[],[]]'
    # Added 2 new rules to conditions any:
    to   = '[["status_id less_than 3"],["requester_id is 6","assignee_id is current_user"],[]]'
    expected_output = "<a href=\"#\" rel=\"tooltip\" title=\"From&lt;br/&gt;All: Status less than Solved and Assignee is (current user)&lt;br/&gt;To&lt;br /&gt;All: Status less than Solved Any: Requester is N/A or Assignee is (current user)\">Definition: Changed</a>"

    view_context.
      expects(:link_to_function).
      with('Definition: Changed', nil, rel: 'tooltip', title: 'From<br/>All: Status less than Solved and Assignee is (current user)<br/>To<br/>All: Status less than Solved; Any: Requester is N/A or Assignee is (current user)').
      returns(expected_output)

    assert_equal(
      expected_output,
      change_presenter("definition", from, to).description
    )
  end

  it "renders changes from nil" do
    from = nil
    to   = '[["status_id less_than 3"],[],[]]'
    expected_output = "<a href=\"#\" rel=\"tooltip\" title=\"From&lt;br/&gt;not set&lt;br/&gt;To&lt;br/&gt;All: Status less than Solved\">Definition: Changed</a>"

    view_context.
      expects(:link_to_function).
      with('Definition: Changed', nil, rel: 'tooltip', title: 'From<br/>not set<br/>To<br/>All: Status less than Solved').
      returns(expected_output)

    assert_equal(
      expected_output,
      change_presenter("definition", from, to).description
    )
  end

  it "removes HTML tags in definition item log entries" do
    from = '[["status_id less_than 3","assignee_id is current_user"],[],[]]'
    to   = '[["status_id less_than 3"],[],["assignee_id is foo<h1>jk<script>alert()</script></h1>"]]'
    expected_output = "<a href=\"#\" rel=\"tooltip\" title=\"From&lt;br/&gt;All: Status less than Solved and Assignee is (current user)&lt;br/&gt;To&lt;br/&gt;All: Status less than Solved&lt;br/&gt;Actions: Assignee is foojk  \">Definition: Changed</a>"

    view_context.
      expects(:link_to_function).
      with('Definition: Changed', nil, rel: 'tooltip', title: 'From<br/>All: Status less than Solved and Assignee is (current user)<br/>To<br/>All: Status less than Solved; Actions: Assignee is foojk').
      returns(expected_output)

    assert_equal(
      expected_output,
      change_presenter("definition", from, to).description
    )
  end

  it "displays trial_expires_on when it wasn't previously set" do
    from = nil
    to   = Date.new(timestamp.year)
    view_context.expects(:format_date).with(to).returns("Jan 01")

    assert_equal(
      "Trial expires on changed from not set to Jan 01",
      change_presenter("trial_expires_on", from, to.to_s(:db)).description
    )
  end

  it "displays trial_expires_on when it was previously set" do
    from = Date.new(timestamp.year)
    to   = Date.new(timestamp.year - 1)
    view_context.expects(:format_date).with(from).returns("Jan 01")
    view_context.expects(:format_date).with(to).returns("January 01, 2012")

    assert_equal(
      "Trial expires on changed from Jan 01 to January 01, #{timestamp.year - 1}",
      change_presenter("trial_expires_on", from.to_s(:db), to.to_s(:db)).description
    )
  end

  it "displays is_trial" do
    assert_equal(
      "In trial changed from active to inactive",
      change_presenter("is_trial", "1", "0").description
    )
  end

  it "displays voice_trial" do
    assert_equal(
      "Voice trial changed from active to inactive",
      change_presenter("voice_trial", "1", "0").description
    )
  end

  it "displays deleted for voice_deleted_at column" do
    assert_equal(
      "Deleted",
      change_presenter("voice_deleted_at", "", timestamp).description
    )
  end

  it "displays Error for values that cause errors" do
    assert_equal(
      "Error",
      change_presenter("roles", "1", "116").description
    )
  end

  it "displays crypted password" do
    assert_equal(
      "Password: Changed",
      change_presenter("crypted_password", "asdadsads", "dsafsdfsdsdfddf").description
    )
  end

  it "displays support plan_type" do
    assert_equal(
      "Plan type changed from Starter to Enterprise",
      change_presenter("plan_type", "1", "4", source_type: 'ZendeskBillingCore::Zuora::Subscription').description
    )
  end

  it "displays support plan_type when it is changing from nil" do
    assert_equal(
      "Plan type changed from not set to Enterprise",
      change_presenter("plan_type", nil, "4", source_type: 'ZendeskBillingCore::Zuora::Subscription').description
    )
  end

  it "displays chat plan_type" do
    assert_equal(
      "Plan type changed from Trial to Lite",
      change_presenter("plan_type", 'trial', "lite", source_type: 'ZendeskBillingCore::Zopim::Subscription').description
    )
  end

  it "displays chat plan_type when it is changing from nil" do
    assert_equal(
      "Plan type changed from Not Set to Lite",
      change_presenter("plan_type", nil, "lite", source_type: 'ZendeskBillingCore::Zopim::Subscription').description
    )
  end

  describe 'support patagonia pricing model' do
    before do
      view_context.stubs(:current_account).returns(accounts(:minimum))
      Subscription.any_instance.stubs(:pricing_model_revision).returns(ZBC::Zendesk::PricingModelRevision::PATAGONIA)
    end

    it "displays patagonia plan_type" do
      assert_equal(
        "Plan type changed from Essential to Professional",
        change_presenter("plan_type", "1", "3", source_type: 'ZendeskBillingCore::Zuora::Subscription').description
      )
    end

    it "displays patagonia plan_type when it is changing from nil" do
      assert_equal(
        "Plan type changed from not set to Team",
        change_presenter("plan_type", nil, "2", source_type: 'ZendeskBillingCore::Zuora::Subscription').description
      )
    end
  end

  it "displays voice plan_type" do
    assert_equal(
      "Voice plan type changed from Advanced to LegacyTransfer",
      change_presenter("voice_plan_type", 'advanced', 'legacy_transfer').description
    )
  end

  it "displays nothing for unknown" do
    assert_nil change_presenter("foo_bar", "1", "2").description
  end

  it "displays subdomain" do
    assert_equal(
      "Subdomain changed from foo to bar",
      change_presenter("subdomain", "foo", "bar").description
    )
  end

  it "displays SuspendedTicketNotification frequency" do
    assert_equal(
      "Frequency changed from Every 10 minutes to Never",
      change_presenter("frequency", "10", "0").description
    )
  end

  it "displays SuspendedTicketNotification email_list" do
    assert_equal(
      "Email list changed from foo@bar.com to bar@foo.com",
      change_presenter("email_list", "foo@bar.com", "bar@foo.com").description
    )
  end

  describe "owner_id" do
    it "displays for account" do
      assert_equal(
        "Owner changed from minimum_end_user to Agent Minimum",
        change_presenter("owner_id", users(:minimum_end_user).id, users(:minimum_agent).id, source_type: "Account").description
      )
    end

    it "displays for account and unfound owner" do
      assert_equal(
        "Owner changed from not set to not set",
        change_presenter("owner_id", 123, 234, source_type: "Account").description
      )
    end

    it "displays for rule" do
      assert_equal(
        "Owner changed to User: minimum_end_user",
        change_presenter("owner_id", 123, 234, source: View.new { |v| v.owner = users(:minimum_end_user) }).description
      )
    end

    it "displays for rule with unfound owner" do
      ZendeskExceptions::Logger.expects(:record).never
      assert_equal(
        "Error",
        change_presenter("owner_id", 123, 234, source: View.new).description
      )
    end

    it "displays for unfound rule" do
      assert_equal(
        "not set",
        change_presenter("owner_id", 123, 234, source_type: "Rule").description
      )
    end
  end

  ["description", "rate_limit", "ticket_id"].each do |attribute_name|
    describe "when #{attribute_name} changes" do
      it "returns the correct description text" do
        assert_equal(
          "#{attribute_name} changed from 10 to 20",
          change_presenter(attribute_name, 10, 20).description
        )
      end
    end
  end

  describe "apps_settings" do
    def setting_changes(label, *values)
      Hash[values.each_with_index.map { |value, i| ["#{label}_#{i}", value] }].to_json
    end

    it "displays '(blank') for changes to/from blank values" do
      old = setting_changes('awesomeness', '')
      new = setting_changes('awesomeness', nil)
      assert_equal(
        "awesomeness_0 changed from not set to not set",
        change_presenter("settings", old, new).description
      )
    end

    it "displays simple changes" do
      old = setting_changes('awesomeness', 'eleven')
      new = setting_changes('awesomeness', '11')
      assert_equal(
        "awesomeness_0 changed from eleven to 11",
        change_presenter("settings", old, new).description
      )
    end

    it "does not blow up on non-string values" do
      old = setting_changes('awesomeness', 123)
      new = setting_changes('awesomeness', true)
      assert_equal(
        "awesomeness_0 changed from 123 to true",
        change_presenter("settings", old, new).description
      )
    end

    it "prevents executing code included in changes" do
      old = setting_changes('awesomeness', 'haxlol')
      new = setting_changes('awesomeness', ' ')
      assert_equal(
        "awesomeness_0 changed from haxlol to not set",
        change_presenter("settings", old, new).description
      )
    end

    it "displays multiple settings changes on multiple lines" do
      old = setting_changes('awesomeness', 'eleven', 'Bob')
      new = setting_changes('awesomeness', '11', "Alice")
      description = change_presenter("settings", old, new).description
      assert_equal(
        "awesomeness_0 changed from eleven to 11<br/>awesomeness_1 changed from Bob to Alice",
        description
      )
      assert description.html_safe?
    end

    it "does not display uneven settings changes" do
      old = setting_changes('awesomeness', '11')
      new = setting_changes('awesomeness')
      assert_equal("", change_presenter("settings", old, new).description)
      assert_equal("", change_presenter("settings", new, old).description)
    end
  end

  describe "account properties" do
    it "displays boolean properties" do
      setting = AccountSetting.new(name: "api_token_access")
      CIA::AttributeChange.any_instance.stubs(:source).returns(setting)

      assert_equal(
        "Enabled",
        change_presenter("value", "0", "1").description
      )

      assert_equal(
        "Disabled",
        change_presenter("value", "1", "0").description
      )
    end

    it "displays text properties" do
      setting = AccountSetting.new(name: "cc_subject_template")
      CIA::AttributeChange.any_instance.stubs(:source).returns(setting)

      view_context.
        expects(:link_to_function).
        with('Changed', nil, rel: 'tooltip', title: 'From<br/>foo<br/>To<br/>bar').
        returns("EXPECTED")

      assert_equal(
        "EXPECTED",
        change_presenter("value", "foo", "bar").description
      )
    end

    it "displays text properties" do
      setting = AccountText.new(name: "ip_restriction")
      CIA::AttributeChange.any_instance.stubs(:source).returns(setting)

      expected_output = "<a href=\"#\" rel=\"tooltip\" title=\"From<br/>foo<br/>To<br/>bar\">Range: Changed</a>"

      view_context.
        expects(:link_to_function).
        with('Changed', nil, rel: 'tooltip', title: 'From<br/>foo<br/>To<br/>bar').
        returns(expected_output)

      assert_equal(
        expected_output,
        change_presenter("value", "foo", "bar").description
      )
    end

    describe "session_timeout" do
      before do
        setting = AccountSetting.new(name: "session_timeout")
        CIA::AttributeChange.any_instance.stubs(:source).returns(setting)
        CIA::AttributeChange.any_instance.stubs(:source_type).returns('AccountSetting')
      end

      it "displays session timeout with values in minutes" do
        title = "From<br/>5 minutes<br/>To<br/>30 minutes"
        expected_output = "<a href=\"#\" rel=\"tooltip\" title=\"#{title}\">Range: Changed</a>"

        view_context.
          expects(:link_to_function).
          with('Changed', nil, rel: 'tooltip', title: title).
          returns(expected_output)

        assert_equal(
          expected_output,
          change_presenter("value", "5", "30").description
        )
      end

      it "displays session timeout with values in months and weeks" do
        title = "From<br/>8 hours<br/>To<br/>2 weeks"
        expected_output = "<a href=\"#\" rel=\"tooltip\" title=\"#{title}\">Range: Changed</a>"

        view_context.
          expects(:link_to_function).
          with('Changed', nil, rel: 'tooltip', title: title).
          returns(expected_output)

        assert_equal(
          expected_output,
          change_presenter("value", "480", "20160").description
        )
      end
    end
  end

  describe "user properties" do
    it "displays boolean properties with personalized text" do
      setting = UserSetting.new(name: "suspended")
      CIA::AttributeChange.any_instance.stubs(:source).returns(setting)

      assert_equal(
        "Suspended",
        change_presenter("value", "0", "1").description
      )

      assert_equal(
        "Unsuspended",
        change_presenter("value", "1", "0").description
      )
    end
  end

  describe "#multiline_description" do
    it "removes html" do
      assert_equal "From<br/>This thing<br/>To<br/>That thing", change_presenter("x", "y", "z").send(:multiline_description, "This thing", "<script>That thing</script>")
    end
  end

  describe "#audit_value" do
    it "handles a basic string" do
      assert_equal "asd", change_presenter("", "", "").send(:audit_value, "asd")
    end

    it "strips an html string" do
      assert_equal "asd", change_presenter("", "", "").send(:audit_value, "<tag>asd</tag>")
    end

    it "handles i18n strings" do
      assert_equal "Notify all agents of received request", change_presenter("", "", "").send(:audit_value, "txt.default.triggers.notify_all_received.title")
    end

    it "handles nil" do
      assert_equal "not set", change_presenter("", "", "").send(:audit_value, nil)
    end
  end
end
