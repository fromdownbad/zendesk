require_relative "../support/test_helper"

SingleCov.covered!

describe AppLinkPresenter do
  fixtures :app_links

  let(:apps) { [app_links(:scarlett)] }

  describe '#present_for_apple' do
    it 'builds the json for apple universal links' do
      expected = {
        applinks: {
          apps: [],
          details: apps.map do |app|
            {
              appID: app.app_id,
              paths: app.paths
            }
          end
        }
      }

      assert_equal expected, subject.present_for_apple(apps)
    end
  end

  describe '#present_for_android' do
    it 'builds the json for android app links' do
      expected = apps.map do |app|
        {
          relation: ['delegate_permission/common.handle_all_urls'],
          target: {
            namespace: 'android_app',
            package_name: app.package_name,
            sha256_cert_fingerprints: app.fingerprints
          }
        }
      end

      assert_equal expected, subject.present_for_android(apps)
    end
  end
end
