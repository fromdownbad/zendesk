Rails.cache.clear

Dir[Rails.root + 'db/fixtures/**/*.rb'].sort.each do |seed_file|
  puts "\n== Seed from #{seed_file}"
  seconds = Benchmark.realtime { load seed_file.to_s }
  puts "   #{seconds} seconds"
end
