# Iterate through each admin for the `zendeskaccounts` subdomain
# and create the related OAuth Client.
zendesk_accounts = Account.find_by_subdomain('zendeskaccounts')
ActiveRecord::Base.on_shard(zendesk_accounts.shard_id) do
  ZENDESK_ACCOUNTS_ADMINS.each do |name|
    client = zendesk_accounts.clients.find_by_identifier(name)
    user   = zendesk_accounts.users.find_by_name(name)

    # Ensure that the client hasn't already been created
    # and that the user to associate it with exists.
    next if client.present? || user.nil?

    Zendesk::OAuth::Client.create! do |c|
      c.name         = "Account Creation Client for #{name}"
      c.company      = 'Zendesk'
      c.description  = "This client handles token generation for Account creation for #{name}."
      c.identifier   = name
      c.redirect_uri = 'https://zendeskaccounts.zendesk.com'
      c.account_id   = zendesk_accounts.id
      c.user_id      = user.id

      c.generate_secret
    end
  end
end
