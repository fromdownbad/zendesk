require 'zendesk/i18n/english_by_zendesk'
require 'json'

# Same ids as in production until we clean and make locales unique in production
path_to_locales = File.expand_path('translation_locales.json', File.dirname(__FILE__))
locales_in_json = File.read(path_to_locales)
locales = JSON.parse(locales_in_json)

locales.each do |locale|
  parent_id   = locale.fetch('parent_id', 'NULL')
  official    = locale.fetch('official_translation', false)
  in_glossary = locale.fetch('in_glossary', false)
  rtl         = locale.fetch('right_to_left', false)

  query = <<-SQL
    INSERT INTO translation_locales (id, locale, name, public, localized_agent, flag, parent_id, official_translation, in_glossary, right_to_left, created_at, updated_at)\
    VALUES (#{locale['id']}, '#{locale['locale']}', '#{locale['name']}', 1, 1, '#{locale['flag']}', '#{parent_id}', #{official}, #{in_glossary}, #{rtl}, now(), now())
  SQL

  TranslationLocale.connection.execute(query)
end
Rails.cache.clear

ENGLISH_BY_ZENDESK = Zendesk::I18n::EnglishByZendesk.instantiate
