# Iterate through the configured tokens for `zendeskaccounts` and seed the database.
zendesk_accounts = Account.find_by_subdomain('zendeskaccounts')
ActiveRecord::Base.on_shard(zendesk_accounts.shard_id) do
  Zendesk::Configuration.fetch(:account_creation_tokens).each_with_index do |kv, index|
    name   = kv.first
    value  = kv.last
    client = zendesk_accounts.clients.find_by_identifier(name)
    token  = zendesk_accounts.tokens.find_by_token(value)

    # Ensure that the client exists and that
    # the token hasn't already been created.
    next if client.nil? || token.present?

    query = <<-SQL
      INSERT INTO oauth_tokens (id, account_id, user_id, client_id, token, refresh_token, scopes, created_at, expires_at, global_client_id, last_used_at, monitor_user_id, oauth_grant_id)\
      VALUES (#{index + 1}, #{zendesk_accounts.id}, #{client.user.id}, #{client.id}, '#{value}', NULL, 'accounts:write', now(), NULL, NULL, NULL, NULL, NULL)
    SQL

    Zendesk::OAuth::Token.connection.execute(query)
  end
end
