ActiveRecord::Base.on_all_shards do
  Membership.create!(group_id: -1, user_id: -1)
end

dev_account = Account.find_by_name('DEV')
ActiveRecord::Base.on_shard(dev_account.shard_id) do
  dev_support_group = Group.where(account_id: dev_account.id, name: 'Support').first
  agents = dev_account.agents - [dev_account.owner]

  agents.each do |a|
    Membership.where(group_id: dev_support_group.id, user_id: a[:user_id]).first_or_create
  end
end
