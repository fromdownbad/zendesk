photo_file = StringIOWrapper.new(content_type: 'image/png', filename: 'zd_system_user.png')
photo_file << File.read("#{Rails.root}/public/images/zd_user.png")

ActiveRecord::Base.on_all_shards do
  Photo.create do |p|
    p.account_id = -1
    p.user_id = -1

    p.uploaded_data = photo_file
  end
end
