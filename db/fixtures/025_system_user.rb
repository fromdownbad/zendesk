ActiveRecord::Base.on_all_shards do
  unless User.find_by_id(-1)
    User.connection.insert(
      "INSERT INTO users (id, account_id, is_active, roles, name, time_zone, crypted_password, salt)" \
        " VALUES (-1, -1, 1, #{Role::AGENT.id}, 'Zendesk', 'Copenhagen', 'e6e98e279bf93324deac2aca732de641e77022ca', 'bc2819ee5964217e5e0c0e960cdbfbf2f35b6931')"
    )
    User.connection.execute(
      "INSERT INTO user_identities (id, user_id, account_id, type, value, priority, is_verified)" \
      " VALUES (-1, -1, -1, 'UserEmailIdentity', 'support@system.zendesk.com', 1, 0)"
    )
  end
end
