# This needs cleaning up -- Account/Owner/Address circular dependencies ahoy!

class AccountMaker
  DEFAULT_OWNER_OPTS = {
    name: ENV['AGENT_NAME'] || 'Admin Extraordinaire',
    email: ENV['AGENT_MAIL'] || 'admin@zd-dev.com',
    password: '123456',
    skip_verification: true
  }.freeze

  DEFAULT_SUBSCRIPTION_OPTS = {
    managed_update: true,
    manual_discount: 100,
    manual_discount_expires_on: 10.years.from_now.to_date,
    is_trial: false,
    plan_type: SubscriptionPlanType.ExtraLarge
  }.freeze

  def initialize(subdomain)
    @subdomain = subdomain
  end

  def create!(owner_options: {}, subscription_options: DEFAULT_SUBSCRIPTION_OPTS, account_type: Accounts::Classic)
    owner_options        = DEFAULT_OWNER_OPTS.merge(owner_options)
    subscription_options = DEFAULT_SUBSCRIPTION_OPTS.merge(subscription_options)

    unless Account.find_by_subdomain(@subdomain)
      account = account_type.new do |a|
        a.id = 42 if @subdomain == "mondocam"
        a.name            = @subdomain.capitalize
        a.subdomain       = @subdomain
        a.time_zone       = "Pacific Time (US & Canada)"
        a.help_desk_size  = "Small team"
        a.multiproduct    = false
      end

      ActiveRecord::Base.on_shard(account.shard_id) do
        account.set_owner(owner_options)
        account.set_address(country_code: "US", phone: '123456')
        account.save!

        account.subscription.update_attributes!(subscription_options)

        account.settings.set(
          api_password_access: true,
          api_token_access: true
        )

        account.branding = Branding.create(
          header_color: "C3C3C3",
          sidebox_color: "F6F6F6",
          tab_background_color: "A2A2A2",
          page_background_color: "333333",
          text_color: "FFFFFF",
          tab_hover_color: "C7C7C7"
        ) # "Development Grey"
        account.save!

        # Making every account serviceable by force
        account.update_attribute(:is_serviceable, true)

        # Run the subscription feature service for this newly created account
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!
      end
    end
  end
end

Rails.cache.mute do
  unless Account.find_by_id(-1)
    Account.connection.execute(
      "INSERT INTO accounts (id, name, is_active, subdomain, time_zone, shard_id) VALUES (-1, 'System Account', 1, 'system', 'Copenhagen', 1)"
    )

    ActiveRecord::Base.on_all_shards do
      ActiveRecord::Base.connection.execute("INSERT INTO account_ids (id) VALUES (-1)")
    end
    Rails.cache.clear
    Account.find(-1).routes.create!(subdomain: 'system')
  end

  # Fully Sponsored Enterprise accounts.
  DEV_SUBDOMAINS.each do |subdomain|
    AccountMaker.new(subdomain).create!
  end

  # zendeskaccounts for new account creation.
  zendesk_accounts = Account.find_by_subdomain('zendeskaccounts')
  ActiveRecord::Base.on_shard(zendesk_accounts.shard_id) do
    zendesk_accounts.subscription.base_agents = 50
    zendesk_accounts.subscription.save!
    zendesk_accounts.save!
  end

  # Trial-mode Plus account.
  owner = {
    name: "Imma Trialler",
    email: "agent@zd-dev.com",
    password: 123456,
    skip_verification: true
  }
  subscription = {
    manual_discount: 0,
    trial_expires_on: 30.days.from_now.to_date,
    plan_type: SubscriptionPlanType.Large,
    is_trial: true
  }
  AccountMaker.new("trial").create!(owner_options: owner, subscription_options: subscription)

  mondocam = Account.find_by_subdomain('mondocam')
  ActiveRecord::Base.on_shard(mondocam.shard_id) do
    mondocam.subscription.base_agents = 15
    mondocam.subscription.save!
    mondocam.settings.admins_can_set_user_passwords = 1
    mondocam.save!
  end

  # Canceled account
  reaper = Account.find_by_subdomain('reaper')
  ActiveRecord::Base.on_shard(reaper.shard_id) do
    reaper.subscription.credit_card = CreditCard.new
    reaper.subscription.save!
    reaper.cancel!
    reaper.subscription.canceled_on = 130.days.ago
    reaper.subscription.save!
    reaper.soft_delete!
    reaper.deleted_at = 40.days.ago
    reaper.save!

    audit = reaper.data_deletion_audits.first
    audit.created_at = 40.days.ago
    audit.save!
  end

  support = Account.find_by_subdomain("support")
  TranslationLocale.without_arsi.update_all(account_id: support.id)
  support.allowed_translation_locale_ids = [1, 2, 8, 16, 19, 30, 66, 67, 1176]

  # Automated QA account
  cerberus = Account.find_by_subdomain!('cerberus')
  ActiveRecord::Base.on_shard(cerberus.shard_id) do
    cerberus.source = 'Test Account Creator'
    cerberus.address.phone = '123 456 7890'
    cerberus.settings.set(
      admins_can_set_user_passwords: 1,
      end_user_comment_rate_limit: 5000,
      help_center_state: "enabled"
    )
    cerberus.allowed_translation_locales = TranslationLocale.where(name: ["Deutsch", "English", "Dansk"])
    cerberus.save!
    cerberus.subscription.update_max_agents!(15)
    cerberus.users.each do |user|
      user.settings.set(
        show_feature_notifications: false,
        show_onboarding_tooltips: false,
        show_onboarding_modal: false,
        show_welcome_dialog: false,
        show_get_started_animation: false,
        show_reporting_video_tutorial: false,
        show_zero_state_tour_ticket: false,
        quick_assist_first_dismissal: false,
        show_help_panel_intro_tooltip: false,
        show_agent_workspace_onboarding: false,
        has_seen_channel_switching_onboarding: false,
        show_manual_start_translation_tooltip: false,
        show_manual_stop_translation_tooltip: false,
        show_composer_will_translate_tooltip: false,
        show_composer_wont_translate_tooltip: false
      )
      user.save!
    end
  end
end
