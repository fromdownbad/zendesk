# uses trial account to test support users doing stuff on other accounts
account = Account.find_by_subdomain('support')
account.on_shard do
  # support user as actor (legacy)
  support = Account.find_by_subdomain('support')
  CIA.audit actor: User.system, ip_address: "0.0.0.1", via_reference_id: support.users.first.id, via_id: Zendesk::Types::ViaType.IMPORT do
    account.subscription.attributes = {"max_agents" => 10, "trial_expires_on" => 1.day.from_now, "audit_message" => "Custom message by system"}
    CIA.record(:update, account.subscription)
  end

  # monitor user as actor
  CIA.audit actor: User.system, ip_address: "0.0.0.2", via_reference_id: 123, via_id: Zendesk::Types::ViaType.MONITOR_EVENT do
    account.subscription.attributes = {"is_trial" => true}
    CIA.record(:update, account.subscription)
  end

  # normal user as actor
  user = account.users.first
  CIA.audit actor: user, ip_address: "0.0.0.2" do
    account.subscription.attributes = {"max_agents" => 14, "trial_expires_on" => 2.day.from_now}
    CIA.record(:update, account.subscription)

    user.password = "asdajksdhakjdshajksdhaasdasd"
    user.valid?
    user.roles = Role::END_USER.id
    CIA.record(:update, user)
  end

  # deleted actor to find bugs
  deleted_user = user.dup
  deleted_user.id = 9999
  CIA.audit actor: deleted_user, ip_address: "0.0.0.3" do
    account.subscription.attributes = {"max_agents" => 12, "trial_expires_on" => 3.day.from_now}
    CIA.record(:update, account.subscription)
  end
end
