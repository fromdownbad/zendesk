account = Account.find_by_subdomain("support")
account.on_shard do
  [[FieldInteger, "Integer Field"], [FieldDecimal, "Decimal Field"], [FieldCheckbox, "Checkbox Field"], [FieldDate, "Date Field"]].each do |type, title|
    f = type.new
    f.account = account
    f.title = title
    f.tag = title.downcase.tr(' ', '_')
    f.save!
  end

  f = FieldTagger.new
  f.account = account
  f.title = "Dropdown Field"
  f.tag = "dropdown_field"
  f.custom_field_options.build(name: "Choice 1", value: "choice_1")
  f.custom_field_options.build(name: "Choice 2", value: "choice_2")
  f.custom_field_options.build(name: "Choice 3", value: "choice_3")
  f.save!

  fields =
    [
      [CustomField::Integer,  "User Integer Field"],
      [CustomField::Decimal,  "User Decimal Field"],
      [CustomField::Checkbox, "User Checkbox Field"],
      [CustomField::Date,     "User Date Field"],
      [CustomField::Text,     "User Text Field"],
      [CustomField::Textarea, "User TextArea Field"],
    ]

  # create some user custom fields too whynot.
  fields.each do |type, title|
    f = type.new
    f.account = account
    f.owner = 'User'
    f.title = title
    f.key = title.downcase.tr(' ', '_')
    f.save!
  end

  dropdown = CustomField::Dropdown.new(account: account, owner: 'User', title: 'User Dropdown Field', key: 'user_dropdown_field')
  dropdown.dropdown_choices.build(name: 'choice 1', value: 'choice_1')
  dropdown.dropdown_choices.build(name: 'choice 2', value: 'choice_2')
  dropdown.dropdown_choices.build(name: 'choice 3', value: 'choice_3')
  dropdown.dropdown_choices.build(name: 'choice 3::a', value: 'choice_3_a')
  dropdown.dropdown_choices.build(name: 'choice 3::b', value: 'choice_3_b')
  dropdown.dropdown_choices.build(name: '{{dc.choice_3_c}}', value: 'choice_3_c')
  dropdown.save!
end
