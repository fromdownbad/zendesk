class SupportProducts < BackfillAccountProducts
  include BackfillAccountProductsQuery

  def backfill
    account_query.find_in_batches do |batch|
      batch.each do |account|
        product = account.on_shard do
          Zendesk::Accounts::SupportProductMapper.derive_product(account)
        end

        next if product.nil? || product[:state] == :unknown

        puts "Backfilling support product record for account #{account.idsub} with #{product}"
        product_id = ActiveRecord::Base.connection.insert(insert_product_sql(account_id: account.id, state: product[:state], trial_expires_at: account.subscription.trial_expires_on.try(:to_datetime).try(:iso8601)))
        ActiveRecord::Base.connection.insert(insert_product_settings_sql(account_id: account.id, product_id: product_id, name: 'plan_type', value: product[:plan_settings][:plan_type]))
        ActiveRecord::Base.connection.insert(insert_product_settings_sql(account_id: account.id, product_id: product_id, name: 'max_agents', value: product[:plan_settings][:max_agents]))
        ActiveRecord::Base.connection.insert(insert_product_settings_sql(account_id: account.id, product_id: product_id, name: 'light_agents', value: product[:plan_settings][:light_agents]))
      end
    end
  end

  def insert_product_sql(account_id:, state:, trial_expires_at:)
    <<-SQL
      INSERT IGNORE INTO `account_products`
      SET `account_id` = #{account_id},
        `name` = 'support',
        `state` = '#{state}',
        `state_updated_at` = CURRENT_TIMESTAMP(),
        `trial_expires_at` = '#{trial_expires_at}',
        `updated_at` = CURRENT_TIMESTAMP(),
        `created_at` = CURRENT_TIMESTAMP();
    SQL
  end

  def insert_product_settings_sql(account_id:, product_id:, name:, value:)
    <<-SQL
      INSERT IGNORE INTO `account_product_settings`
      SET `account_id` = #{account_id},
        `product_id` = #{product_id},
        `name` = '#{name}',
        `value` = '#{value}',
        `updated_at` = CURRENT_TIMESTAMP(),
        `created_at` = CURRENT_TIMESTAMP();
    SQL
  end
end

Rails.cache.mute do
  SupportProducts.new.backfill
end
