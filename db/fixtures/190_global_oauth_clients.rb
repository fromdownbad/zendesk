identifier = Zendesk::OAuth::GlobalClient::SYSTEM_USERS_IDENTIFIER
unless Zendesk::OAuth::GlobalClient.exists?(identifier: identifier)
  Zendesk::OAuth::GlobalClient.create! do |client|
    client.name = "Zendesk System Users"
    client.company = "Zendesk"
    client.description = "This client handles token generation for all internal zendesk system users."
    client.identifier = identifier
    client.account_id = 1
    client.user_id = -1
    client.generate_secret
  end
end

unless Zendesk::OAuth::GlobalClient.exists?(identifier: Zendesk::OAuth::GlobalClient::WEB_WIDGET_IDENTIFIER)
  Zendesk::OAuth::GlobalClient.create! do |client|
    client.name = "Zendesk Web Widget"
    client.company = "Zendesk"
    client.description = "This client issues tokens for web widget to access help center content"
    client.identifier = Zendesk::OAuth::GlobalClient::WEB_WIDGET_IDENTIFIER
    client.account_id = 1
    client.user_id = -1
    client.generate_secret
  end
end

unless Zendesk::OAuth::GlobalClient.exists?(identifier: Zendesk::OAuth::GlobalClient::ZOPIM_IDENTIFIER)
  Zendesk::OAuth::GlobalClient.create! do |client|
    client.name = "Zopim by Zendesk"
    client.company = "Zendesk"
    client.description = "This client is used by Zopim to populate tickets."
    client.identifier = Zendesk::OAuth::GlobalClient::ZOPIM_IDENTIFIER
    client.redirect_uri = 'https://www.chat.zd-dev.com/auth/zendesk/token'
    client.account_id = 1
    client.user_id = -1
    client.secret = '243d83cba9273a11c83476e710f07f8f5c3c9b9f1f5608d02decd40c628e60e2'
  end
end

identifier = "zendesk_app_market"
unless Zendesk::OAuth::GlobalClient.exists?(identifier: identifier)
  Zendesk::OAuth::GlobalClient.create! do |client|
    client.name = "Zendesk App Market"
    client.company = "Zendesk"
    client.description = "This client handles authentication from the .com site into the Zendesk App Market"
    client.identifier = identifier
    client.redirect_uri = 'https://appsubmission.zd-dev.com/app_market/auth_callback'
    client.account_id = 1
    client.user_id = -1
    client.generate_secret
  end
end
