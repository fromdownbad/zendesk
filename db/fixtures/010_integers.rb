def insert_integers
  if ActiveRecord::Base.connection.select_value("select count(*) from integers").to_i.zero?
    values = (0..1000).map { |i| "(#{i})" }.join(",")

    ActiveRecord::Base.connection.execute "INSERT INTO integers (i) VALUES #{values}"
  end
end

ActiveRecord::Base.on_shard(nil) { insert_integers }
ActiveRecord::Base.on_all_shards { insert_integers }

Rails.cache.clear
