def create_ticket(account, requester, options = {})
  suspend = options.delete(:suspend)

  ticket = Ticket.new({
    account: account,
    subject: 'First Ticket',
    description: "This ticket was created by running rake db:reset",
    requester: requester,
    group: account.groups.first
  }.merge(options))

  ticket.add_comment(body: ticket.description, is_public: true)

  ticket.will_be_saved_by(requester)

  if suspend
    ticket = ticket.suspend(SuspensionType.SIGNUP_REQUIRED)
  end

  ticket.save!
  ticket
end

def create_tickets(subdomain)
  account = Account.find_by_subdomain(subdomain)
  ActiveRecord::Base.on_shard(account.shard_id) do
    enduser = account.find_user_by_email('enduser@zd-dev.com')
    create_ticket(account, enduser)

    t = create_ticket(
      account,
      enduser,
      subject: "Archived Ticket",
      description: "This is an archived ticket",
      status_id: StatusType.CLOSED,
      created_at: 3.months.ago,
      updated_at: 3.months.ago
    )

    begin
      t.archive!
    rescue StandardError
      puts "Riak is not running... (#{$!})"
    end

    create_ticket(account, enduser, subject: "Suspended Ticket", description: "This ticket got suspended", suspend: true)
  end
end

['dev', 'support', 'mondocam'].each { |s| create_tickets(s) }
