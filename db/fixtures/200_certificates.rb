10.times do |i|
  CertificateIp.create! do |ip|
    ip.ip = "127.0.0.#{1 + i}"
    ip.port = 10_000 + i
    ip.pod_id = 1
  end
end

account = Account.find_by_subdomain('support')
Certificate.create! do |certificate|
  certificate.account        = account
  certificate.state          = 'active'
  certificate.key            = Zendesk::EncryptText.encrypt(Zendesk::Configuration.dig!(:certificates, :ssl_key_password), "#{account.subdomain}_key")
  certificate.crt_chain      = "#{account.subdomain}_crt"
  certificate.certificate_ips << CertificateIp.first
  certificate.valid_until = Date.today + 10.years

  boxen_certificate = '/opt/boxen/repo/modules/zendesk/files/ssl/zd-dev.com.crt'
  if File.exist?(boxen_certificate)
    certificate.crt = File.read(boxen_certificate)
  end
end
