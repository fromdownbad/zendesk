Plan.create! do |p|
  p.has_ssl = 0
  p.id = 1
  p.is_public = 1
  p.max_agents = 1
  p.max_automations = 6
  p.max_end_users = 50
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "Free"
  p.price = 0
end

Plan.create! do |p|
  p.has_ssl = 0
  p.id = 2
  p.is_public = 1
  p.max_agents = 2
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "Duo"
  p.price = 38
end

Plan.create! do |p|
  p.has_ssl = 0
  p.id = 3
  p.is_public = 1
  p.max_agents = 5
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 30
  p.max_triggers = 15
  p.max_views = 100
  p.name = "Small"
  p.price = 95
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 4
  p.is_public = 1
  p.max_agents = 10
  p.max_automations = 10
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "Medium"
  p.price = 190
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 5
  p.is_public = 0
  p.max_agents = 15
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "Large"
  p.price = 285
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 6
  p.is_public = 0
  p.max_agents = 10
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "Large (grandfathered)"
  p.price = 179
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 7
  p.is_public = 1
  p.max_agents = 25
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "X-large"
  p.price = 475
end

Plan.create! do |p|
  p.has_ssl = 0
  p.id = 8
  p.is_public = 1
  p.max_agents = 1
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "Solo"
  p.price = 19
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 9
  p.is_public = 0
  p.max_agents = 25
  p.max_automations = 6
  p.max_end_users = 200
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "EU100A25"
  p.price = 129
end

Plan.create! do |p|
  p.has_ssl = 0
  p.id = 10
  p.is_public = 0
  p.max_agents = 11
  p.max_automations = 6
  p.max_end_users = 100
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "EU100A11"
  p.price = 99
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 11
  p.is_public = 0
  p.max_agents = 5
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "MediumSSL"
  p.price = 129
end

Plan.create! do |p|
  p.has_ssl = 0
  p.id = 12
  p.is_public = 0
  p.max_agents = 11
  p.max_automations = 5000
  p.max_end_users = 100
  p.max_macros = 5000
  p.max_triggers = 5000
  p.max_views = 5000
  p.name = "Wemind"
  p.price = 49
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 13
  p.is_public = 0
  p.max_agents = 5
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "M-SSL-NP"
  p.price = 49
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 14
  p.is_public = 0
  p.max_agents = 25
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "XL-NP"
  p.price = 209
end

Plan.create! do |p|
  p.has_ssl = 1
  p.id = 15
  p.is_public = 0
  p.max_agents = 10
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "L-NP"
  p.price = 107
end

Plan.create! do |p|
  p.has_ssl = 0
  p.id = 16
  p.is_public = 0
  p.max_agents = 5
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "M-NP"
  p.price = 69
end

Plan.create! do |p|
  p.has_ssl = 0
  p.id = 17
  p.is_public = 0
  p.max_agents = 7
  p.max_automations = 6
  p.max_end_users = 5000
  p.max_macros = 100
  p.max_triggers = 15
  p.max_views = 30
  p.name = "Medium Plus"
  p.price = 139
end
