Arsi.disable!

class Seeder
  class << self
    def seed!
      Rails.cache.clear
      flush_redis

      account = Account.find_by_subdomain('mondocam')

      ActiveRecord::Base.on_shard(nil) do
        load_data_files(account, 'db/fixtures/mondocam/main')
      end

      ActiveRecord::Base.on_shard(account.shard_id) do
        load_data_files(account, 'db/fixtures/mondocam/shard')

        account.settings.screencasts_for_tickets = true
        account.settings.help_center_state = "enabled"
        account.settings.web_portal_state = "restricted"
        account.settings.screencasts_for_tickets = true
        account.settings.save!
      end
    end

    private

    def flush_redis
      Redis.current.flushall
    rescue Errno::ECONNREFUSED, Redis::CannotConnectError
    end

    def load_data_files(account, directory)
      Dir["#{directory}/*.csv"].each do |path|
        table = path[/(\w+).csv/, 1]
        CSV.foreach(path, headers: :first_row) do |row|
          values = row.values_at.map do |column|
            case column
            when "NULL" then "NULL"
            when "USER_1" then @agent ||= account.agents.first.id
            when "USER_2" then @user ||= account.end_users.first.id
            else
              column.inspect.gsub("\\n", "\n")
            end
          end
          sql = "INSERT INTO #{table} (#{row.headers.join(", ")}) VALUES(#{values.join(", ")})"
          ActiveRecord::Base.connection.execute(sql)
        end
      end
    end
  end
end

Rails.cache.mute { Seeder.seed! }
