# Add "Light Agent" permission sets so that developers can create users of type "Light Agent" locally.
['support', 'dev'].each do |subdomain|
  account = Account.find_by_subdomain(subdomain)
  account.on_shard do
    PermissionSet.create_light_agent!(account)
  end
end
