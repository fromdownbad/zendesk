ActiveRecord::Base.on_all_shards do
  GROUP_NAME = 'Sallys'.freeze

  User.where('name like "%Sally%"').each do |sally|
    account = sally.account
    group = account.groups.where(name: GROUP_NAME).first_or_create!
    account.memberships.where(group_id: group.id, user_id: sally.id).first_or_create!
  end
end
