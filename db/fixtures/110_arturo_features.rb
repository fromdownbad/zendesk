[
  :account_deletion_kill_switch,
  :android_app_selection,
  :apps_create_public,
  :at_mentions,
  :bime_integration,
  :block_facebook_outgoing_retry,
  :chat_html_notifications,
  :client_side_logging,
  :cms,
  :csat_page_refresh,
  :csat_reason_code,
  :csat_reasson_code_admin,
  :custom_security_policy,
  :custom_widget_jquery,
  :customer_satisfaction,
  :disable_closing_tickets_via_sharing,
  :dropbox_management,
  :emoji_autocompletion,
  :gooddata_advanced_analytics,
  :google_group_alias_x_been_there,
  :help_center_article_labels,
  :help_center_content_mapping_editor,
  :help_center_grandfather_ga,
  :help_center_theme_editor_v2,
  :help_center_theming_app,
  :help_center_use_guide_search,
  :help_center_use_guide_community_search,
  :incident_auto_complete,
  :incremental_forum_search,
  :knowledge_bank,
  :knowledge_bank_editor,
  :knowledge_archiving,
  :lockdown_enabled,
  :lotus_radar_expiry,
  :lotus_reporting,
  :lotus_safe_updates,
  :macro_application_no_liquid,
  :memoizing_matchable_body,
  :new_agent_search_ui,
  :new_automations,
  :nps_csv_export_v2,
  :nps_expand_invite_limit,
  :nps_recipients_editor,
  :nps_survey_name,
  :nps_surveys,
  :nps_web_widget,
  :nps_reporting_d3,
  :office_365_direct_link,
  :paypal,
  :placeholders,
  :restrict_voice_trial,
  :rules_can_reference_macros,
  :salesforce_integration_lookup,
  :satisfaction_dashboard,
  :satisfaction_prediction,
  :es_tags_autocomplete,
  :search_throttling,
  :search_use_ja_extended_analyzer,
  :security_email_notifications_for_account_settings,
  :security_email_notifications_for_password_strategy,
  :ssl_validation_sha1_signature,
  :sso_login,
  :staff_service,
  :status_hold,
  :ticket_metrics_pending_first_reply,
  :urban_airship_response_logging,
  :user_views,
  :voice_trust_on_outbound_calls,
  :chat_product_tray,
  :talk_product_tray,
  :voice_csv_generation,
].each do |feature|
  next if Arturo::Feature.find_feature(feature)
  f = Arturo::Feature.new(symbol: feature)
  f.phase = 'on'
  f.save!
end
