def create_user(account, name, email, role)
  User.create! do |u|
    u.account_id = account.id
    u.is_active = true
    u.roles = role.id
    u.name = name
    u.email = email
    u.is_verified = true
    u.time_zone = "Copenhagen"
    u.password = "123456"
  end
end

account = Account.find_by_subdomain('dev')
3.times do |i|
  ActiveRecord::Base.on_shard(account.shard_id) do
    create_user(account, "Sally Agent #{i}", "sally.agent.#{i}@example.org", Role::AGENT)
  end
end

account = Account.find_by_subdomain('support')
3.times do |i|
  ActiveRecord::Base.on_shard(account.shard_id) do
    create_user(account, "Sally Support Agent #{i}", "sally.support.agent.#{i}@example.org", Role::AGENT)
  end
end

zendesk_accounts = Account.find_by_subdomain("zendeskaccounts")
ZENDESK_ACCOUNTS_ADMINS.each do |admin|
  zendesk_accounts.on_shard do
    create_user(zendesk_accounts, admin, "accountcreation+#{admin}@zd-dev.com", Role::ADMIN)
  end
end

['dev', 'support', 'mondocam'].each do |subdomain|
  account = Account.find_by_subdomain(subdomain)
  ActiveRecord::Base.on_shard(account.shard_id) do
    create_user(account, 'End User', 'enduser@zd-dev.com', Role::END_USER)
  end
end

['mondocam', 'dev', 'support'].each do |subdomain|
  account = Account.find_by_subdomain(subdomain)
  account.on_shard do
    create_user(account, 'Agent Ordinaire', 'agent@zd-dev.com', Role::AGENT)
  end
end

# correct mondocam entries generated from csv with bogus submitter_ids
mondocam = Account.find_by_subdomain("mondocam")
mondocam.on_shard do
  mondocam.entries.where(submitter_id: 62).update_all(submitter_id: mondocam.end_users.first.id)
  mondocam.entries.where(submitter_id: 17).update_all(submitter_id: mondocam.agents.first.id)
end

Rails.cache.clear
