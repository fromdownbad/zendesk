# Contributing

## Requirements

* Request a review from your team, include others if need it (https://github.com/orgs/zendesk/teams).
* If this would be of interest to almost everyone, such as a big, hairy change
(e.g. the Rails 5 upgrade), request a review to `@zendesk/rails-upgrade-team`.
* Add specs, verify all checks are green (Travis, Rubocop, Brakeman)
* Need :+1: before merge.

## Pull Request

### Description

Describe the original problem and the changes made on this PR.

### Steps to reproduce

(If this is a bug) please include steps to reproduce bugs. You can include logs, Arturo flags, screenshots, before/after, or original files too.

### References

Must of all production code must be referenced by a JIRA id. You can add the full URL (https://zendesk.atlassian.net/browse/XXX-123) or just [XXX-123]

### Risks

Define the risk level, choose between **High**, **Medium**, or **Low**. Explain how might failures be experienced? Remember all code changes carry a minimum of risk of **Low** should be a rare exception.

Include how to rollback if there is a problem. Feel free to include notes to Support OPS, TicketDuty, and QA teams here too.

For more information, please read https://zendesk.atlassian.net/wiki/spaces/ENG/pages/92897648.
