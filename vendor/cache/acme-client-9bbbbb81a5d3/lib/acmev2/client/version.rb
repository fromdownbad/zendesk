# frozen_string_literal: true

module AcmeV2
  class Client
    VERSION = '2.0.5'.freeze
  end
end
