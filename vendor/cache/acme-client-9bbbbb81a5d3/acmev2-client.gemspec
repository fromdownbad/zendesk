# -*- encoding: utf-8 -*-
# stub: acmev2-client 2.0.5 ruby lib

Gem::Specification.new do |s|
  s.name = "acmev2-client".freeze
  s.version = "2.0.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Charles Barbier".freeze]
  s.date = "2020-12-18"
  s.email = ["unixcharles@gmail.com".freeze]
  s.files = [".gitignore".freeze, ".rspec".freeze, ".rubocop.yml".freeze, ".travis.yml".freeze, "CHANGELOG.md".freeze, "Gemfile".freeze, "LICENSE.txt".freeze, "README.md".freeze, "Rakefile".freeze, "acmev2-client.gemspec".freeze, "bin/console".freeze, "bin/release".freeze, "bin/setup".freeze, "lib/acmev2-client.rb".freeze, "lib/acmev2/client.rb".freeze, "lib/acmev2/client/certificate_request.rb".freeze, "lib/acmev2/client/certificate_request/ec_key_patch.rb".freeze, "lib/acmev2/client/error.rb".freeze, "lib/acmev2/client/faraday_middleware.rb".freeze, "lib/acmev2/client/jwk.rb".freeze, "lib/acmev2/client/jwk/base.rb".freeze, "lib/acmev2/client/jwk/ecdsa.rb".freeze, "lib/acmev2/client/jwk/rsa.rb".freeze, "lib/acmev2/client/resources.rb".freeze, "lib/acmev2/client/resources/account.rb".freeze, "lib/acmev2/client/resources/authorization.rb".freeze, "lib/acmev2/client/resources/challenges.rb".freeze, "lib/acmev2/client/resources/challenges/base.rb".freeze, "lib/acmev2/client/resources/challenges/dns01.rb".freeze, "lib/acmev2/client/resources/challenges/http01.rb".freeze, "lib/acmev2/client/resources/challenges/unsupported_challenge.rb".freeze, "lib/acmev2/client/resources/directory.rb".freeze, "lib/acmev2/client/resources/order.rb".freeze, "lib/acmev2/client/self_sign_certificate.rb".freeze, "lib/acmev2/client/util.rb".freeze, "lib/acmev2/client/version.rb".freeze]
  s.homepage = "http://github.com/unixcharles/acme-client".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.1.0".freeze)
  s.rubygems_version = "2.7.6.2".freeze
  s.summary = "Client for the ACME protocol.".freeze

  s.installed_by_version = "2.7.6.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, [">= 1.6.9", "~> 1.6"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_development_dependency(%q<rspec>.freeze, [">= 3.3.0", "~> 3.3"])
      s.add_development_dependency(%q<vcr>.freeze, [">= 2.9.3", "~> 2.9"])
      s.add_development_dependency(%q<webmock>.freeze, ["~> 3.3"])
      s.add_runtime_dependency(%q<faraday>.freeze, [">= 0.9.1", "~> 0.9"])
    else
      s.add_dependency(%q<bundler>.freeze, [">= 1.6.9", "~> 1.6"])
      s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_dependency(%q<rspec>.freeze, [">= 3.3.0", "~> 3.3"])
      s.add_dependency(%q<vcr>.freeze, [">= 2.9.3", "~> 2.9"])
      s.add_dependency(%q<webmock>.freeze, ["~> 3.3"])
      s.add_dependency(%q<faraday>.freeze, [">= 0.9.1", "~> 0.9"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, [">= 1.6.9", "~> 1.6"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_dependency(%q<rspec>.freeze, [">= 3.3.0", "~> 3.3"])
    s.add_dependency(%q<vcr>.freeze, [">= 2.9.3", "~> 2.9"])
    s.add_dependency(%q<webmock>.freeze, ["~> 3.3"])
    s.add_dependency(%q<faraday>.freeze, [">= 0.9.1", "~> 0.9"])
  end
end
