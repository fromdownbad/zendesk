# -*- encoding: utf-8 -*-
# stub: koala 3.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "koala".freeze
  s.version = "3.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Alex Koppel".freeze]
  s.date = "2020-11-05"
  s.description = "Koala is a lightweight, flexible Ruby SDK for Facebook.  It allows read/write access to the social graph via the Graph and REST APIs, as well as support for realtime updates and OAuth and Facebook Connect authentication.  Koala is fully tested and supports Net::HTTP and Typhoeus connections out of the box and can accept custom modules for other services.".freeze
  s.email = "alex@alexkoppel.com".freeze
  s.extra_rdoc_files = ["readme.md".freeze, "changelog.md".freeze]
  s.files = [".gitignore".freeze, ".rspec".freeze, ".travis.yml".freeze, ".yardopts".freeze, "Gemfile".freeze, "ISSUE_TEMPLATE".freeze, "LICENSE".freeze, "Manifest".freeze, "PULL_REQUEST_TEMPLATE".freeze, "Rakefile".freeze, "changelog.md".freeze, "code_of_conduct.md".freeze, "koala.gemspec".freeze, "lib/koala.rb".freeze, "lib/koala/api.rb".freeze, "lib/koala/api/batch_operation.rb".freeze, "lib/koala/api/graph_api_methods.rb".freeze, "lib/koala/api/graph_batch_api.rb".freeze, "lib/koala/api/graph_collection.rb".freeze, "lib/koala/api/graph_error_checker.rb".freeze, "lib/koala/configuration.rb".freeze, "lib/koala/errors.rb".freeze, "lib/koala/http_service.rb".freeze, "lib/koala/http_service/multipart_request.rb".freeze, "lib/koala/http_service/request.rb".freeze, "lib/koala/http_service/response.rb".freeze, "lib/koala/http_service/uploadable_io.rb".freeze, "lib/koala/oauth.rb".freeze, "lib/koala/realtime_updates.rb".freeze, "lib/koala/test_users.rb".freeze, "lib/koala/utils.rb".freeze, "lib/koala/version.rb".freeze, "readme.md".freeze, "spec/cases/api_spec.rb".freeze, "spec/cases/configuration_spec.rb".freeze, "spec/cases/error_spec.rb".freeze, "spec/cases/graph_api_batch_spec.rb".freeze, "spec/cases/graph_api_spec.rb".freeze, "spec/cases/graph_collection_spec.rb".freeze, "spec/cases/graph_error_checker_spec.rb".freeze, "spec/cases/http_service/request_spec.rb".freeze, "spec/cases/http_service/response_spec.rb".freeze, "spec/cases/http_service_spec.rb".freeze, "spec/cases/koala_spec.rb".freeze, "spec/cases/koala_test_spec.rb".freeze, "spec/cases/multipart_request_spec.rb".freeze, "spec/cases/oauth_spec.rb".freeze, "spec/cases/realtime_updates_spec.rb".freeze, "spec/cases/test_users_spec.rb".freeze, "spec/cases/uploadable_io_spec.rb".freeze, "spec/cases/utils_spec.rb".freeze, "spec/fixtures/beach.jpg".freeze, "spec/fixtures/cat.m4v".freeze, "spec/fixtures/facebook_data.yml".freeze, "spec/fixtures/mock_facebook_responses.yml".freeze, "spec/fixtures/vcr_cassettes/app_test_accounts.yml".freeze, "spec/fixtures/vcr_cassettes/friend_list_next_page.yml".freeze, "spec/integration/graph_collection_spec.rb".freeze, "spec/spec_helper.rb".freeze, "spec/support/custom_matchers.rb".freeze, "spec/support/graph_api_shared_examples.rb".freeze, "spec/support/koala_test.rb".freeze, "spec/support/mock_http_service.rb".freeze, "spec/support/uploadable_io_shared_examples.rb".freeze]
  s.homepage = "http://github.com/arsduo/koala".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--line-numbers".freeze, "--inline-source".freeze, "--title".freeze, "Koala".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.1".freeze)
  s.rubygems_version = "2.7.6.2".freeze
  s.summary = "A lightweight, flexible library for Facebook with support for the Graph API, the REST API, realtime updates, and OAuth authentication.".freeze
  s.test_files = ["spec/cases/api_spec.rb".freeze, "spec/cases/configuration_spec.rb".freeze, "spec/cases/error_spec.rb".freeze, "spec/cases/graph_api_batch_spec.rb".freeze, "spec/cases/graph_api_spec.rb".freeze, "spec/cases/graph_collection_spec.rb".freeze, "spec/cases/graph_error_checker_spec.rb".freeze, "spec/cases/http_service/request_spec.rb".freeze, "spec/cases/http_service/response_spec.rb".freeze, "spec/cases/http_service_spec.rb".freeze, "spec/cases/koala_spec.rb".freeze, "spec/cases/koala_test_spec.rb".freeze, "spec/cases/multipart_request_spec.rb".freeze, "spec/cases/oauth_spec.rb".freeze, "spec/cases/realtime_updates_spec.rb".freeze, "spec/cases/test_users_spec.rb".freeze, "spec/cases/uploadable_io_spec.rb".freeze, "spec/cases/utils_spec.rb".freeze, "spec/fixtures/beach.jpg".freeze, "spec/fixtures/cat.m4v".freeze, "spec/fixtures/facebook_data.yml".freeze, "spec/fixtures/mock_facebook_responses.yml".freeze, "spec/fixtures/vcr_cassettes/app_test_accounts.yml".freeze, "spec/fixtures/vcr_cassettes/friend_list_next_page.yml".freeze, "spec/integration/graph_collection_spec.rb".freeze, "spec/spec_helper.rb".freeze, "spec/support/custom_matchers.rb".freeze, "spec/support/graph_api_shared_examples.rb".freeze, "spec/support/koala_test.rb".freeze, "spec/support/mock_http_service.rb".freeze, "spec/support/uploadable_io_shared_examples.rb".freeze]

  s.installed_by_version = "2.7.6.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<faraday>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<addressable>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<json>.freeze, [">= 1.8"])
    else
      s.add_dependency(%q<faraday>.freeze, [">= 0"])
      s.add_dependency(%q<addressable>.freeze, [">= 0"])
      s.add_dependency(%q<json>.freeze, [">= 1.8"])
    end
  else
    s.add_dependency(%q<faraday>.freeze, [">= 0"])
    s.add_dependency(%q<addressable>.freeze, [">= 0"])
    s.add_dependency(%q<json>.freeze, [">= 1.8"])
  end
end
