/*globals _, Zendesk, jQuery*/

jQuery(document).ready(function() {
  // Collaboration, Legacy CCs and Followers
  const acceptedNewCollaborationTos =      jQuery('#account_settings_accepted_new_collaboration_tos');
  const agentEmailCcsBecomeFollowers =     jQuery('#account_settings_agent_email_ccs_become_followers');
  const commentEmailCcsAllowed   =         jQuery('#account_settings_comment_email_ccs_allowed');
  const defaultFollowerEmailText =         jQuery('#default-follower-email-text');
  const emailAceEditor =                   jQuery('<pre id="emailAceEditor" class="editor">');
  const emailInputText =                   jQuery('.email_subject_editor');
  const emailResizableContainer =          jQuery('<div class="emailResizableContainer ace_resizable">');
  const emailTextArea =                    jQuery('.email_body_editor');
  const followerAndEmailCcCollaborations = jQuery('#account_settings_follower_and_email_cc_collaborations');
  const launchCcsOnboarding =              jQuery('.ccs-onboarding-modal-activate');
  const lightAgentEmailCcsAllowed =        jQuery('#account_settings_light_agent_email_ccs_allowed');
  const revertFollowerEmailText =          jQuery('#revert-follower-email-text');
  const ticketFollowersAllowed =           jQuery('#account_settings_ticket_followers_allowed');
  const topCollaborationsChecks =          jQuery('#account_settings_ticket_followers_allowed, #account_settings_comment_email_ccs_allowed');
  const ccsRequesterExcludedPublicComments = jQuery('#account_settings_ccs_requester_excluded_public_comments');

  const hasEmailCcsLightAgentsV2 = Zendesk.currentAccount.hasFeature('hasEmailCcsLightAgentsV2')

  emailResizableContainer.append(emailAceEditor);
  emailTextArea.after(emailResizableContainer);
  emailTextArea.hide();

  new InputSuggest(emailInputText);

  jQuery("#activate").click(function() {
    const lotusReact = parent.LotusReact;
    const setSettings = lotusReact.actionsFor('Admin/Collaborators/OnboardingModal').setSettings;
    const settings = {
      agentEmailCcsBecomeFollowers: agentEmailCcsBecomeFollowers.is(':checked'),
      commentEmailCcsAllowed: commentEmailCcsAllowed.is(':checked'),
      followerAndEmailCcCollaborations: followerAndEmailCcCollaborations.val() === 'true',
      ticketFollowersAllowed: ticketFollowersAllowed.is(':checked')
    };

    if (hasEmailCcsLightAgentsV2) {
      settings.lightAgentEmailCcsAllowed = lightAgentEmailCcsAllowed.is(':checked');
    }

    lotusReact.dispatchAction(setSettings(settings));
  });

  const emailEditor = AceSetup(
    emailAceEditor.attr('id'),
    emailResizableContainer,
    emailTextArea,
    'liquid',
    emailResizableContainer.parent().parent().width(),
    'placeholder.'
  );

  revertFollowerEmailText.click(function() {
    emailEditor.getSession().setValue(defaultFollowerEmailText.text().trim());
  });

  // Comments
  var markdownBox   = jQuery('#formatting_options_radio_2'),
      richTextBox   = jQuery('#formatting_options_radio_1'),
      markdownDescription = jQuery("p#markdown_ticket_comments_description"),
      richtextDescription = jQuery('p#rich_text_comments_description');

  var hideAndShowText = function() {
    if (richTextBox.is(':checked')) {
      markdownDescription.addClass('hidden');
      richtextDescription.removeClass('hidden');
    }
    else if (markdownBox.is(':checked')) {
      richtextDescription.addClass('hidden');
      markdownDescription.removeClass('hidden');
    }
  }

  var onRadioChange = function(checkbox, e) {
    hideAndShowText();
    toggleHiddenField(checkbox);
    toggleHiddenField(e.target);
  }

  var toggleHiddenField = function(checkbox) {
    var $currentHiddenField = jQuery(checkbox).prev('input[type="hidden"]');
    var checked = jQuery(checkbox).is(':checked');
    $currentHiddenField.val( (checked ? 1 : 0) );
  }

  markdownBox.change(onRadioChange.bind(this, richTextBox));
  richTextBox.change(onRadioChange.bind(this, markdownBox));

  hideAndShowText();

  // Collaboration ToS
  var colDiv       = jQuery('#collaboration'),
    colChecks      = jQuery('#collaboration input[type="checkbox"]'),
    activateButton = jQuery('#collaboration #activate'),
    disableLink    = jQuery('#collaboration_off');

  // When the checkboxes change
  colChecks.change(function(e) {
    var allChecked = colChecks.not(':checked').length == 0;
    // Set the correct settings values
    if (colDiv.data("mode") == "deactivation"){
      followerAndEmailCcCollaborations.val(!allChecked);
      acceptedNewCollaborationTos.val(!allChecked);
    } else {
      acceptedNewCollaborationTos.val(allChecked);
    }

    // Disable/Enable the submit button
    if (allChecked) {
      activateButton.removeClass('disabled');
    } else {
      activateButton.addClass('disabled');
    }
  });

  // Submit if everything is checked!
  activateButton.click(function(e) {
    if (colChecks.not(':checked').length == 0) {
      jQuery(this).parents('form').submit();
    } else {
      e.preventDefault();
    }
  });

  // "No longer want to be part of the CCs and Followers experience?" link
  disableLink.click(function(e) {
    e.preventDefault();

    colChecks.attr('checked', false);
    colChecks.trigger('change');
  });

  // Admin onboarding v2
  launchCcsOnboarding.click(function() {
    const toggleModal = parent.LotusReact.actionsFor('Admin/Collaborators/OnboardingModal').toggleModal;

    parent.LotusReact.dispatchAction(toggleModal());
  });

  // Automatically make an agent CC a follower
  function verifyCcToFollower() {
    if (topCollaborationsChecks.not(':checked').length > 0) {
      agentEmailCcsBecomeFollowers.attr('checked', false);
      agentEmailCcsBecomeFollowers.attr('disabled', true);
      agentEmailCcsBecomeFollowers.parent().addClass('disabled');
    } else {
      agentEmailCcsBecomeFollowers.removeAttr('disabled');
      agentEmailCcsBecomeFollowers.parent().removeClass('disabled');
    }
  }

  topCollaborationsChecks.change(function(e) { verifyCcToFollower(); });
  agentEmailCcsBecomeFollowers.change(function(e) { verifyCcToFollower(); });
  verifyCcToFollower();

  if (hasEmailCcsLightAgentsV2) {
    const verifyAccountSettingsLightAgentEmailCcsAllowed = function() {
      if (commentEmailCcsAllowed.is(':checked')) {
        lightAgentEmailCcsAllowed.removeAttr('disabled');
        lightAgentEmailCcsAllowed.parent().removeClass('disabled');
      } else {
        lightAgentEmailCcsAllowed.attr('checked', false);
        lightAgentEmailCcsAllowed.attr('disabled', true);
        lightAgentEmailCcsAllowed.parent().addClass('disabled');
      }
    };

    commentEmailCcsAllowed.click(verifyAccountSettingsLightAgentEmailCcsAllowed);

    verifyAccountSettingsLightAgentEmailCcsAllowed();
  }

  function verifyCcsRequesterExcludedPublicCommentsAccountSettings(_event) {
    if (commentEmailCcsAllowed.is(':checked')) {
      if (_event) {
        ccsRequesterExcludedPublicComments.attr('checked', false);
      }
      ccsRequesterExcludedPublicComments.removeAttr('disabled');
    } else {
      ccsRequesterExcludedPublicComments.attr('checked', false);
      ccsRequesterExcludedPublicComments.attr('disabled', true);
    }
  }

  commentEmailCcsAllowed.click(verifyCcsRequesterExcludedPublicCommentsAccountSettings);

  verifyCcsRequesterExcludedPublicCommentsAccountSettings();
});
