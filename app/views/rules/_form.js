/*global Zendesk, jQuery*/

jQuery(document).ready(function() {
  const submitRule = jQuery('.submit-rule');
  const viewForm   = jQuery('#viewform');

  submitRule.click(function() {
    viewForm.submit();

    this.disabled = true;
  });

  /**
   * Begin work-around
   *
   * `text-rendering: auto` is a work around for a
   * `text-rendering: optimizeLegibility` crashing bug in Safari 13.0
   * See Webkit bug with ID: 202055
   * And see Z1 ticket 4898639
   * Follow-up JIRA to remove this code once Safari is updated and can handle
   * the desired `text-rendering: optimizeLegibility` behavior: FANG-156
   *
   * `navigator.userAgent.match(/\sVersion\/13\./)` matches any 13.x version.
   * Tighten this if 13.1 fixes the bug when released.
   */
  if (
    navigator
    && (navigator.vendor || '').match(/Apple/)
    && (navigator.userAgent || '').match(/Version\/13/)
  ) {
    const style = document.createElement("style");
    style.type = "text/css";
    style.innerHTML = "select { text-rendering: auto !important;}";

    document.head.appendChild(style);
  }
  /**
   * End work-around
   */
});
