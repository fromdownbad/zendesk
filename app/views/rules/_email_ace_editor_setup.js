/*globals jQuery, AceSetup, InputSuggest*/

jQuery(".email_editor").each(function() {
  var textArea = jQuery(this),
    subjectInput = textArea.siblings("input.text").first();

  new InputSuggest(subjectInput);
  new InputSuggest(textArea);
});
