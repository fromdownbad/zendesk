xml.instruct!

xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
    xml.title("#{current_user.account.name.capitalize}: #{@page_title}")

    xml.link(url_for(only_path: false))
    # xml.description('This feed lists blog entries')

    @tickets.each do |ticket|
      xml.item do
        desc = ''
        comment = ticket.comments.last
        if comment
          desc << content_tag('p', content_tag('b', "#{comment.author.name}, #{format_absolute_date(comment.created_at)}: "))
          desc << describe(comment)
        end
        ticket_rss_item(xml, ticket, desc, ticket.nice_id)
      end
    end
  end
end
