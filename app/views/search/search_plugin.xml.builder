xml.instruct!
xml.OpenSearchDescription('xmlns' => 'http://a9.com/-/spec/opensearch/1.1/') do
  xml.ShortName "Zendesk search"
  xml.Description 'Searches help desk tickets, users and forum topics'
  xml.Image "http://#{@host}/images/find.png", 'height' => '16', 'width' => '16', 'type' => 'image/png'
  xml.Url 'type' => 'text/html', 'method' => 'GET', 'template' => "http://#{@host}/search?query={searchTerms}"
  xml.InputEncoding 'UTF-8'
  xml.AdultContent 'false'
end
