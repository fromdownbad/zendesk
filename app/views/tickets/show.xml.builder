xml.instruct!
xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
    xml.title("#{current_user.account.name.capitalize}: #{@page_title}")
    xml.link(url_for(only_path: false))
    xml.item do
      desc = ''
      @ticket.audits.each do |audit|
        desc << content_tag('p', content_tag('b', "#{audit.author.name}, #{audit.created_at.to_format(format: :absolute, time_format: current_user.time_format_string)}: "))
        desc << describe(audit)
      end

      xml.title("#{@ticket.ticket_type} ##{@ticket.nice_id}: \"#{@ticket.title(100)}\"")
      xml.link(@ticket.url + "?guid=#{@ticket.nice_id}")
      xml.author(@ticket.requester.name)
      xml.category(@ticket.current_tags)
      xml.pubDate CGI.rfc1123_date(@ticket.updated_at.in_time_zone)
      xml.description(desc)
      xml.guid(@ticket.url)
    end
  end
end
