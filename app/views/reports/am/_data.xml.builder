xml = Builder::XmlMarkup.new
date_range = data[:data].keys.sort

xml.chart do
  xml.series do
    date_range.each do |date|
      xml.value I18n.localize(date, format: I18n.t("date.formats.short")), xid: date
    end
  end

  xml.graphs do
    data[:legends].each do |legend|
      xml.graph gid: 0, line_width: 4, title: escape_javascript(legend_name(legend[:name])), color: legend[:color] do
        date_range.each do |date|
          value = data[:data][date][legend[:name]] || 0
          xml.value value, xid: date
        end
      end
    end
  end
end
