xml = Builder::XmlMarkup.new
xml.settings do
  xml.text_color '#4c4c4c'
  xml.background do
    xml.color '#FFFFFF'
    xml.alpha 100
  end
  xml.plot_area do
    xml.margins do
      xml.left 60
      xml.top 10
      xml.right 60
      xml.bottom 0
    end
  end
  xml.grid do
    xml.x do
      xml.approx_count 11
    end
  end
  xml.axes do
    xml.x do
      xml.alpha 0
    end
    xml.y_left do
      xml.alpha 0
    end
  end
  xml.legend do
    xml.align 'center'
  end
end
