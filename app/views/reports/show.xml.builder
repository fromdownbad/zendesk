xml.instruct! :xml, version: "1.0"

data    = @report.table[:data]
legends = @report.table[:legends]

xml.report do
  xml.title(@report.title)
  xml.sets do
    legends.map { |legend| legend[:name] }.each do |legend|
      xml.set do
        xml.legend(legend)
        xml.values do
          data.keys.sort.each do |key|
            xml.value(date: key.strftime('%Y-%m-%d'), data: (data[key][legend] || 0))
          end
        end
      end
    end
  end
end
