xml.instruct!

xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
    xml.title(I18n.t('txt.views.suspended_tickets.index_xml_builder.suspended_tickets_for_account', name_of_current_account: current_account.name.capitalize))
    xml.link(url_for(only_path: false))

    @suspended_tickets.each do |ticket|
      link = "#{current_account.url}/suspended_tickets/#{ticket.id}"
      xml.item do
        xml.title(h(ticket.title))
        xml.link(link)
        xml.author("#{ticket.from_name} &lt;#{ticket.from_mail}&gt;")
        xml.pubDate CGI.rfc1123_date(ticket.created_at.in_time_zone)
        created_at  = ticket.created_at.to_format(with_time: true, time_format: current_account.time_format_string)
        description = "#{created_at} #{I18n.t('txt.views.suspended_tickets.index_xml_builder.ticket_from', sender_name: h(ticket.from_name))} &lt;#{h(ticket.from_mail)}&gt;<br/>#{I18n.t('txt.controllers.ticket_controllers.cause_of_suspension')}#{SuspensionType[ticket.cause].score}"
        description =  content_tag('p', content_tag('b', description.html_safe))
        description << content_tag('p', h(truncate(ticket.content.sanitize, length: 150, omission: '...')))
        xml.description(description)
        xml.guid(link)
      end
    end
  end
end
