xml.instruct!
xml.macros do
  @macros.each do |_macro|
    xml << {
      only:          %i[id title],
      methods:       %i[availability_type],
      skip_instruct: true
    }
  end
end
