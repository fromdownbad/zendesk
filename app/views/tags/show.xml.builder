xml.instruct!

xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
    xml.title("#{current_user.account.name.capitalize}: #{@page_title}")

    xml.link(url_for(only_path: false))
    # xml.description("This feed lists unassigned ticket")

    @result.each do |item|
      xml.item do
        if item.is_a?(Ticket)
          ticket_rss_item(xml, item, nil)
        else
          entry_rss_item(xml, item)
        end
      end
    end
  end
end
