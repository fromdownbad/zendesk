xml.instruct! :xml, version: "1.0", encoding: "UTF-8"

xml.rss "version" => "2.0", 'xmlns:atom' => "http://www.w3.org/2005/Atom", 'xmlns:dc' => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
    xml.title "Pinned messages | #{current_account.name}"
    xml.link(url_for(only_path: false))
    xml.language "en-us"
    xml.ttl "60"
  end
end
