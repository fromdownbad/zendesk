class DataDeletionAuditJob < ActiveRecord::Base
  class JobCollision < StandardError; end

  # Re-enqueue a replacement job if it has not heartbeat for this duration since
  # being picked up. This indicates the job has crashed or been killed.
  EXECUTION_TIMEOUT = 5.minutes

  # Re-enqueue a replacement job if it has not been picked up by a Resque worker
  # after this duration. This indicates Redis having been flushed, or an error
  # starting jobs. This is higher than EXECUTION_TIMEOUT to allow for worker
  # saturation. If duplicate jobs are enqueued, the earlier instances will
  # error immediately at startup due to the heartbeat_at seed changing.
  QUEUE_TIMEOUT = 30.minutes

  # If a job explicitly failed, backoff for this long before enqueueing it again
  FAILED_JOBS_REENQUEUE_DELAY = 1.hour

  not_sharded
  disable_global_uid

  include DataDeletionLifecycle

  belongs_to :account
  belongs_to :audit, foreign_key: :audit_id, class_name: 'DataDeletionAudit', inherit: :account_id

  delegate :reason, to: :audit

  # Parity with DataDeletionAudit and AccountMove
  alias_attribute :failed_reason, :error
  serialize :error, Zendesk::Coders::JSONWithStringFallback

  attr_accessible :account_id, :shard_id, :job, :heartbeat_at

  def enqueue!(delay = 0)
    # heartbeat_at is overloaded to handle both EXECUTION_TIMEOUT and
    # QUEUE_TIMEOUT. We future-dating the hearbeat_at to account for the queue
    # timeout (a hack), and subtracting the execution timeout to account for
    # the `timed_out?` logic.
    self.heartbeat_at = Time.now + delay + QUEUE_TIMEOUT - EXECUTION_TIMEOUT
    self.status = 'enqueued'
    save!
    job.constantize.enqueue_in(delay, id, "last_heartbeat" => heartbeat_at.to_i)
  end

  # Update the heartbeat_at column using a test-and-set operation.
  # If another process has updated since our last heartbeat, raise `JobCollision`
  def optimistic_heartbeat!(last_heartbeat)
    next_heartbeat = Time.now
    nrows = self.class.where(id: id, heartbeat_at: last_heartbeat).
      update_all(heartbeat_at: next_heartbeat)
    raise JobCollision, "Another process modified heartbeat_at. Expected: #{last_heartbeat.inspect}" unless nrows == 1
    next_heartbeat
  end

  def timed_out?
    heartbeat_at.present? && heartbeat_at < EXECUTION_TIMEOUT.ago
  end

  class << self
    # Used to delete failed and unneccessary data_deletion_audit_job records.
    # There is an option to pass the number of failed jobs to keep.
    # If all failed jobs should be deleted, `jobs_to_keep = 0`
    def delete_failed_audit_jobs(audit_id, job, jobs_to_keep = 1)
      where(audit_id: audit_id).
        where(job: job).
        where(status: 'failed').
        order(created_at: :desc).
        offset(jobs_to_keep).
        destroy_all
    end
  end
end
