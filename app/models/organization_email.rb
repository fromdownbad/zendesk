class OrganizationEmail < ActiveRecord::Base
  include Zendesk::Organization::SharedLogic

  alias_attribute :email, :value
  attr_accessible :email

  def self.valid_values(values)
    values.select { |value| EMAIL_PATTERN.match(value) }
  end

  private

  # Based on Organizaton#validate_domain_names
  def validate_value
    unless EMAIL_PATTERN.match(value) && value == value.downcase
      error = I18n.t('activerecord.errors.models.organization_email.attributes.value.invalid', value: value)
      errors.add(:email, error)
    end
  end
end
