module AnswerBot
  class AuditPresenter
    def initialize(deflection:, requester:)
      @deflection = deflection
      @requester = requester
    end

    def present
      fetched_articles = help_center_client.fetch_articles_for_ids_and_locales(article_ids_and_locales)

      articles = fetched_articles.empty? ? default_articles : fetched_articles
      articles.map { |article| article.slice(:id, :title, :url, :html_url) }
    end

    private

    attr_reader :deflection, :requester

    def ticket_deflection_articles
      @ticket_deflection_articles ||= deflection.ticket_deflection_articles
    end

    def article_ids_and_locales
      ticket_deflection_articles.collect do |article|
        {
          'article_id' => article.article_id,
          'locale' => article.locale
        }.with_indifferent_access
      end
    end

    def default_articles
      ticket_deflection_articles.collect do |article|
        article_url = default_article_url(article)
        {
          id: article.article_id,
          title: article_url,
          url: article_url,
          html_url: article_url
        }
      end
    end

    def default_article_url(article)
      "#{article.brand.url}/hc/#{article.locale}/articles/#{article.article_id}"
    end

    def help_center_client
      Zendesk::HelpCenter::InternalApiClient.new(
        account: deflection.account,
        requester: requester,
        brand: deflection.brand
      )
    end
  end
end
