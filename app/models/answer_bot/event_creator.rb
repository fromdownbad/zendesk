module AnswerBot
  class EventCreator
    class << self
      def send_event(articles)
        return if articles.empty?

        AutomaticAnswerSend.new do |event|
          event.suggested_articles = articles
        end
      end

      def email_notification_event(audit, requester_id, rule_id, subject, body)
        return unless audit && requester_id.present? && rule_id && subject && body

        AnswerBotNotification.new(
          recipients: [requester_id],
          subject: subject,
          body: body,
          audit: audit
        ) do |event|
          event.via_id = ViaType.RULE
          event.via_reference_id = rule_id
        end
      end

      def email_notification_with_ccs_event(audit:, recipient_id:, rule_id:, subject:, body:, recipient_ccs: [])
        return unless audit && recipient_id.present? && rule_id && subject && body

        AnswerBotNotification.new(
          recipients: [recipient_id],
          subject: subject,
          body: body,
          audit: audit,
          ccs: recipient_ccs
        ) do |event|
          event.via_id = ViaType.RULE
          event.via_reference_id = rule_id
        end
      end

      def solve_event(article)
        return unless article

        AutomaticAnswerSolve.new do |event|
          event.solved_article = article
        end
      end

      def reject_event(article:, reviewer_id:, reviewer_name:, irrelevant:, reason:)
        AutomaticAnswerReject.new do |event|
          event.article = article
          event.reviewer_id = reviewer_id
          event.reviewer_name = reviewer_name
          event.irrelevant = irrelevant
          event.reason = reason
        end
      end
    end
  end
end
