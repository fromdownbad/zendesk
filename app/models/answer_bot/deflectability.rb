module AnswerBot
  class Deflectability
    def initialize(account:, ticket:)
      @account = account
      @ticket = ticket
    end

    def deflectable_via_email?
      (
        ticket.present? &&
        not_followup_ticket? &&
        !deflected_by_another_channel?
      )
    end

    def deflected_by_another_channel?
      webform.infer_already_deflected?
    end

    private

    def not_followup_ticket?
      ticket.followup_source.nil?
    end

    attr_reader :account, :ticket

    def webform
      @webform ||= AnswerBot::Webform.new(account: account, ticket: ticket)
    end
  end
end
