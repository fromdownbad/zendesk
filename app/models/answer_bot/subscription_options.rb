module AnswerBot
  # Ensure requirements are meet upon subscribe/amend with AnswerBot
  class SubscriptionOptions
    # Basic error class
    class Error < StandardError; end

    # Thrown when guide dependency is not met
    class InvalidGuidePlan < Error; end

    # Thrown when the resolutions value is invalid
    class InvalidMaxResolutions < Error; end

    VALID_RESOLUTIONS = [
      50,
      100,
      200,
      300,
      400,
      500,
      600,
      700,
      800,
      900,
      1_000,
      1_500,
      2_000,
      2_500,
      3_000,
      3_500,
      4_000,
      5_000,
      5_500,
      6_000,
      6_500,
      7_000,
      7_500,
      8_000,
      8_500,
      10_000,
      20_000,
      30_000,
      50_000
    ].freeze

    private_class_method :new

    def self.build_for(account, subscription_options)
      answer_bot_subscription_options = new(account, subscription_options).build
      answer_bot_subscription_options.send(:subscription_options)
    end

    def build
      return self unless answer_bot_max_resolutions

      if prevent_subscribe?
        remove_answer_bot_from_options
      elsif !zero_resolutions?
        ensure_valid_guide_plan
        ensure_valid_max_resolutions
      end

      self
    end

    private

    attr_reader :account, :subscription_options

    delegate :guide_plan_type,
      :answer_bot_max_resolutions,
      to: :subscription_options

    delegate :subscribed?,
      to: :answer_bot_subscription,
      prefix: :answer_bot,
      allow_nil: true

    delegate :legacy?,
      to: :guide_subscription,
      prefix: :guide,
      allow_nil: true

    def initialize(account, subscription_options)
      @account              = account
      @subscription_options = subscription_options.dup
    end

    def prevent_subscribe?
      zero_resolutions? && !answer_bot_subscribed?
    end

    def remove_answer_bot_from_options
      [:answer_bot_plan_type, :answer_bot_max_resolutions].each do |attribute|
        subscription_options.send(:"#{attribute}=", nil)
      end
    end

    def ensure_valid_guide_plan
      valid = guide_plan_type.present? ? !guide_lite? : guide_legacy?

      raise InvalidGuidePlan unless valid
    end

    def ensure_valid_max_resolutions
      return if VALID_RESOLUTIONS.include?(answer_bot_max_resolutions.to_i)
      raise InvalidMaxResolutions
    end

    def zero_resolutions?
      answer_bot_max_resolutions.to_i.zero?
    end

    def guide_lite?
      guide_plan_type == ZBC::Guide::PlanType::Lite.plan_type
    end

    def answer_bot_subscription
      @answer_bot_subscription ||= account.answer_bot_subscription
    end

    def guide_subscription
      @guide_subscription ||= account.subscription.guide_preview
    end
  end
end
