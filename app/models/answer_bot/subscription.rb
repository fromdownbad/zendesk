module AnswerBot
  class Subscription < AccountServiceSubscription
    PLAN_SETTINGS = %i[
      max_resolutions
      billing_cycle_day
    ].freeze

    define_attr_accessors

    PRODUCT_NAME      = 'answer_bot'.freeze
    DEFAULT_PLAN_TYPE = 1

    alias_method :include_in_easy_agent_add?, :subscribed?

    def max_resolutions
      plan_settings['max_resolutions'] || 0 # default for Zuora preview if trial
    end

    def plan_type
      DEFAULT_PLAN_TYPE
    end

    def inspect
      "#<#{self.class}:0x#{(object_id << 1).to_s(16)} state=#{state} " \
        "max_resolutions=#{max_resolutions} " \
        "billing_cycle_day=#{billing_cycle_day}>"
    end
  end
end
