module AnswerBot
  class AnonymousRequester
    NAME = 'Unknown (Answer Bot)'.freeze

    def self.find_or_create(account:)
      new(account: account).find_or_create_user
    end

    def initialize(account:)
      @account = account
    end

    def find_or_create_user
      existing_user || create_user
    end

    private

    attr_reader :account

    def existing_user
      account.answer_bot_users.first
    end

    def create_user
      account.users.create! do |u|
        u.name = NAME
        u.skip_verification = true
        u.settings.answer_bot = true
      end
    end
  end
end
