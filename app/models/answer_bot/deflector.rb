module AnswerBot
  class Deflector
    def initialize(account)
      @account = account
    end

    def deflect(ticket_id:, rule_id:, deflection_channel_id:)
      ticket = account.tickets.find(ticket_id)
      trigger = AnswerBot::Trigger.config(account, rule_id)

      if should_deflect?(ticket)
        answer_bot_service_client.deflect(
          ticket: ticket,
          deflection_channel_id: deflection_channel_id,
          deflection_channel_reference_id: rule_id,
          label_names: trigger[:labels_for_filter]
        )
      end

      create_email_notification_event(
        ticket,
        rule_id,
        trigger[:subject_template],
        trigger[:body_template]
      )
    end

    private

    attr_reader :account

    def should_deflect?(ticket)
      return false if throttled?

      deflectability = AnswerBot::Deflectability.new(account: account, ticket: ticket)
      deflectability.deflectable_via_email?
    end

    def throttled?
      throttled = Prop.throttle(:ticket_deflection_account, account.id.to_s)

      if throttled
        statsd_client.increment('throttled_prediction', tags: ["throttle_key:ticket_deflection_account"])
      end

      throttled
    end

    def create_email_notification_event(ticket, rule_id, subject, body)
      return unless rule_id && subject && body

      ticket.will_be_saved_by(User.system)
      first_audit = ticket.audits.first

      first_audit.events <<
        AnswerBot::EventCreator.email_notification_with_ccs_event(
          audit: first_audit,
          recipient_id: ticket.requester_id,
          recipient_ccs: deflection_ccs(ticket, rule_id),
          rule_id: rule_id,
          subject: subject,
          body: body
        )

      ticket.save!
    end

    def answer_bot_service_client
      @answer_bot_service_client ||= Zendesk::AnswerBotService::InternalApiClient.new account
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket_deflection'])
    end

    def deflection_ccs(ticket, rule_id)
      return [] unless deflection_sends_to_ccs?(rule_id)

      ticket.email_ccs(filter_for_sending: true)
    end

    def deflection_sends_to_ccs?(rule_id)
      return false unless account.has_comment_email_ccs_allowed_enabled?

      rule_actions = account.rules.find(rule_id).definition.actions

      (rule_actions.find { |action| action.source == 'deflection' }.try(:value) || []).include?('requester_and_ccs')
    end
  end
end
