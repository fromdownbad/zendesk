module AnswerBot
  class RequestSolver
    def initialize(deflection_id:, article_id:, resolution_channel_id:)
      @deflection_id = deflection_id
      @article_id = article_id
      @resolution_channel_id = resolution_channel_id
    end

    def create_and_solve_ticket
      return deflection.ticket if deflection.ticket

      anonymous_requester = AnswerBot::AnonymousRequester.find_or_create(account: account)

      audit_events = [
        answer_bot_send_event(anonymous_requester),
        answer_bot_solve_event(anonymous_requester, article_id)
      ]

      ticket = build_ticket(anonymous_requester)
      ActiveRecord::Base.transaction do
        ticket.will_be_saved_by(anonymous_requester)
        ticket.save!

        associate_deflection_with(anonymous_requester, ticket)

        deflection.solve_with_article(article_id, resolution_channel_id)

        add_answer_bot_audit_events(ticket, audit_events)
        ticket.solve(anonymous_requester)
      end

      record_stats(deflection)
      ticket
    end

    def solve_deflection
      ActiveRecord::Base.transaction do
        deflection.solve_with_article_no_via_id(article_id, resolution_channel_id)
      end
      record_stats(deflection)
    end

    private

    attr_reader :article_id, :resolution_channel_id

    def build_ticket(requester)
      deconstructed_enquiry = deflection.deconstruct_enquiry

      ticket = account.tickets.new

      ticket.set_nice_id
      ticket.subject = deconstructed_enquiry[:subject]
      ticket.description = deconstructed_enquiry[:description]
      ticket.requester = requester
      ticket.submitter = requester
      ticket.via_id = deflection.via_id
      ticket.brand = deflection.brand
      ticket.disable_answer_bot_trigger = true

      ticket
    end

    def associate_deflection_with(requester, ticket)
      deflection.update_attributes!(
        ticket: ticket,
        user: requester
      )
    end

    def add_answer_bot_audit_events(ticket, answer_bot_events)
      ticket.will_be_saved_by(User.system)
      answer_bot_events.each do |event|
        ticket.audit.events << event
      end
      ticket.save!
    end

    def record_stats(deflection)
      tags = {
        language: deflection.detected_locale,
        model_version: deflection.model_version
      }

      ab_statsd_client.solved(tags)
    end

    def answer_bot_send_event(requester)
      article_details = suggested_hc_articles(requester)
      AnswerBot::EventCreator.send_event(article_details)
    end

    def answer_bot_solve_event(requester, article_id)
      article_details = suggested_hc_articles(requester)
      solved_article_detail = article_details.find do |hc_article|
        hc_article[:id] == article_id
      end

      # we assume that only the suggested event will be used to solve
      AnswerBot::EventCreator.solve_event(solved_article_detail) if solved_article_detail
    end

    def suggested_hc_articles(requester)
      @suggested_hc_articles ||= AnswerBot::AuditPresenter.new(deflection: deflection, requester: requester).present
    end

    def ab_statsd_client
      @ab_statsd_client ||= Datadog::AnswerBot.new(
        via_id: deflection.via_id,
        subdomain: deflection.brand.subdomain,
        deflection_channel_id: deflection.deflection_channel_id,
        resolution_channel_id: resolution_channel_id
      )
    end

    def deflection
      @deflection ||= TicketDeflection.find(@deflection_id)
    end

    def account
      @account ||= deflection.account
    end
  end
end
