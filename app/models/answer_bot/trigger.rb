module AnswerBot
  class Trigger
    SUBJECT_POSITION = 1
    BODY_POSITION = 2
    FILTER_LABELS_POSITION = 3

    class << self
      def config(account, rule_id)
        value = definition(account, rule_id).actions.find { |action| action.source == 'deflection' }.try(:value)
        {
          subject_template: value[SUBJECT_POSITION],
          body_template: value[BODY_POSITION],
          labels_for_filter: (value.size == 3) ? nil : value[FILTER_LABELS_POSITION].split(',')
        }
      end

      private

      def definition(account, rule_id)
        account.triggers.find(rule_id).definition
      end
    end
  end
end
