module AnswerBot
  class Webform
    NULL_TICKET_FORM_ID = -1

    def initialize(account:, ticket:)
      @account = account
      @ticket = ticket
    end

    def infer_already_deflected?
      (via_webform? &&
        help_center_setting_enabled? &&
        help_center_form_setting_enabled?)
    end

    private

    attr_reader :account, :ticket

    def via_webform?
      (ticket.via_id == ViaType.WEB_FORM)
    end

    def help_center_setting_enabled?
      help_center_client.answer_bot_setting_enabled?
    end

    def help_center_form_setting_enabled?
      help_center_client.answer_bot_form_setting_enabled?(
        brand_id: ticket.brand_id,
        form_id: form_id_with_null_fallback
      )
    end

    def form_id_with_null_fallback
      # Help Center allows a graceful fallback for account subscriptions that do not have a default ticket form
      ticket.ticket_form_id || NULL_TICKET_FORM_ID
    end

    def help_center_client
      @help_center_client ||= Zendesk::HelpCenter::InternalApiClient.new(account: account)
    end
  end
end
