module Access
  class Denied < StandardError
    attr_reader :message, :action, :subject

    def initialize(message = nil, action = nil, subject = nil)
      @message = message
      @action  = action
      @subject = subject
    end
  end

  module Settings
    class Extensions
    end
    class BusinessRules
    end
  end

  module Chat
    class IncomingChats
    end
  end
end
