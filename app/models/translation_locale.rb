require 'zendesk/models/translation_locale'

class TranslationLocale < ActiveRecord::Base
  prepend Zendesk::LocalePresentation

  has_kasket_on :id, :deleted_at

  def top_level?
    parent == ENGLISH_BY_ZENDESK && !locale.start_with?('en')
  end

  def locale_code
    locale.split('-').first
  end
end
