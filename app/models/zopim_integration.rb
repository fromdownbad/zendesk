class ZopimIntegration < ActiveRecord::Base
  include CIA::Auditable
  include ZBC::Zopim::AppMarketIntegration
  include ChatPhaseFour

  audit_attribute :account_id,
    :external_zopim_id,
    :zopim_key

  belongs_to :account

  after_create do |integration|
    ::EmbeddableChatWidgetEnablerJob.
      enqueue(integration.account.id) unless integration.phase_one?

    if account.settings.polaris?
      Rails.logger.info("Skipping ChatAppInstallationJob for Agent Workspace account, account_id: #{integration.account.id}")
      return
    end

    ::ChatAppInstallationJob.
      enqueue(integration.account.id)
  end

  after_commit :expire_cache_for_lotus
  after_commit :sync_zuora_id, on: :update

  before_destroy do |integration|
    begin
      integration.uninstall_app
    rescue Exception => e # rubocop:disable Lint/RescueException
      Rails.logger.warn("Failed to uninstall zopim app: #{e.message} - #{e.backtrace.join('\n')}")
    end
  end

  attr_accessible :account
  attr_accessible :external_zopim_id, :zopim_key, :phase

  validates_uniqueness_of :account_id, :external_zopim_id
  validates_presence_of :account_id, :external_zopim_id, :zopim_key

  delegate :zopim_subscription, to: :account

  PHASE_I   = 1
  PHASE_II  = 2
  PHASE_III = 3

  def phase
    return PHASE_III if self[:phase] == PHASE_III
    return PHASE_I if zopim_subscription.blank?
    PHASE_II
  end

  def phase_one?
    phase == PHASE_I
  end

  def phase_two?
    phase == PHASE_II
  end

  def phase_three?
    phase == PHASE_III
  end

  # These methods are only used in the presenter for zopim_integration
  alias_method :has_agent_sync?, :phase_two?
  alias_method :has_billing?,    :phase_two?

  def app_enabled?
    app_installed? && app_installation.fetch('enabled')
  end

  def expire_cache_for_lotus
    account.expire_scoped_cache_key(:settings)
  end

  def sync_zuora_id
    return unless phase_three?
    ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.enqueue(account.id, external_zopim_id)
  end
end
