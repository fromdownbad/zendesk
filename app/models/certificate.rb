require 'r509'
require 'aasm'
require 'openssl'
require 'fileutils'
require 'zendesk/models/certificate'

class Certificate < ActiveRecord::Base
  class InvalidKeyError < StandardError; end
  module ErrorCode
    # OK = 0
    NEED_KEY = 1
    NEED_CRT = 11
    BAD_CSR = 2
    BAD_PASSPHRASE = 3
    BAD_KEY = 4
    BAD_CRT = 5
    MISMATCH = 6
    # BAD_HOST = 7 # don't check
    # EXPIRES_SOON = 8 # don't check
    BAD_CHAIN = 9
    NEED_INTERMEDIATE = 10
    SHA1_SIGNATURE = 12
  end

  REPLACE_DAYS        = 3.days
  EXPIRE_WARNING_DAYS = 4.days

  delegate :host_mapping?, to: :account
  has_many :certificate_ips
  belongs_to :ipm_alert, class_name: 'Ipm::Alert'

  attr_accessible :account
  belongs_to :account, inverse_of: :certificates

  scope :for_pod,  ->(pod_id) { where("certificate_ips.pod_id = ?", pod_id).includes(:certificate_ips).references(:certificate_ips).order('certificates.created_at ASC, certificates.id ASC') }
  scope :expiring, ->(expiry) { where("state = 'active' AND valid_until = ?", expiry) }
  scope :revoked, -> { where("state = 'revoked'") }
  scope :updated_since, ->(time) { where("certificates.updated_at > ?", time) }
  scope :last_notified, ->(date) { where("notification_sent_at = ?", date.to_date) }
  scope :customer_provisioned, -> { where(autoprovisioned: false) }

  include CIA::Auditable
  audit_attribute :state, :sni_enabled

  include AASM
  include CertificateObserver

  aasm column: :state do
    state :initial
    # temporary state means that a CSR has been generated, but there is no signed cert
    state :temporary, enter: :generate_temporary_files
    state :pending
    state :active
    state :revoked
    state :cancelled

    event :generate_temporary do
      transitions to: :temporary, from: :initial
    end

    # NOTE: it is assumed that set_uploaded_data() has already verified the uploaded data
    event :verify_certificate do
      transitions to: :pending, from: %i[initial temporary]
    end

    event :cancel_pending do
      transitions to: :cancelled, from: :pending
    end

    event :activate do
      transitions to: :active, from: %i[pending active revoked]
      after do
        update_zendesk_provisioned_ssl_setting!
      end
    end

    event :revoke do
      transitions to: :revoked, from: %i[active revoked]
      after do
        detach_certificate_ips!
      end
    end
  end

  # defaults to 4096, but can set externally (like to 2048)
  attr_writer :key_size

  def self.encrypt_field(value)
    enc = Zendesk::EncryptText.encrypt(Zendesk::Configuration.dig!(:certificates, :ssl_key_password), value) if value
    raise(InvalidKeyError) unless enc
    enc
  end

  def self.decrypt_field(value)
    dec = Zendesk::EncryptText.decrypt(Zendesk::Configuration.dig!(:certificates, :ssl_key_password), value) if value
    raise(InvalidKeyError) unless dec
    dec
  end

  def self.certificate_authorities
    store = Zendesk::Certificate::Store.new
    store.load_from_db
    store
  end

  def certificate_ip!(pod_id:)
    @certificate_ip ||= {}
    (@certificate_ip[pod_id] ||= certificate_ips.find { |ip| ip.pod_id == pod_id }) || raise(ActiveRecord::RecordNotFound)
  end

  def brand_host_mappings
    account.brand_host_mappings.to_a
  end

  def cert_filename_prefix
    account.subdomain
  end

  def subject_domain
    account.host_mapping || account.default_brand.try(:host_mapping) || brand_host_mappings.first
  end

  def subject_org
    account.address ? account.address.name : nil
  end

  def subject_org_unit
    account.settings.csr_org_unit.present? ? account.settings.csr_org_unit : subject_org
  end

  def subject_country
    account.address ? account.address.country_code : nil
  end

  def subject_locality
    account.address ? account.address.city : nil
  end

  def cert_san
    brand_host_mappings - [subject_domain]
  end

  def expired?
    valid_until < DateTime.now
  end

  def subject_state
    %i[state province city].each do |location|
      return account.address.send(location) if account.address.send(location).present?
    end

    "Unknown"
  end

  def upload_certificate
    if sni_enabled?
      activate_sni_certificate
    else
      verify_certificate(:pending)
    end
  end

  def do_cancel_pending
    cancel_pending(:cancelled)
  end

  def set_uploaded_data(crt_data, key_data = nil, passphrase = nil)
    # check for PKCS#12
    if crt_data.nil? && key_data
      begin
        pfx = OpenSSL::PKCS12.new(key_data, passphrase)
      rescue StandardError
        return [ErrorCode::NEED_CRT]
      end

      # unpack
      crt_data = pfx.certificate.to_pem
      key_data = pfx.key.to_pem
      passphrase = nil
    end

    # private key must be in temporary cert or passed in via key_data
    return [ErrorCode::NEED_KEY] unless key || key_data

    # is key properly formatted?
    if key_data

      # -----BEGIN RSA PRIVATE KEY-----\nProc-Type: 4,ENCRYPTED\nDEK-Info: DES-EDE3-CBC,BA844358820D98F4\n
      # NOTE: if key is encrypted a passphrase is required, otherwise RSA.new will hang asking: Enter PEM pass phrase:
      # An API to test if key_data is encrypted could not be located, so doing Regex.
      if /ENCRYPTED/.match?(key_data)
        return [ErrorCode::BAD_PASSPHRASE] if !passphrase || passphrase.length < 4
      end

      @key_object = nil # reset
      self.key = Certificate.encrypt_field(key_data)
      begin
        # calls OpenSSL
        self.key = Certificate.encrypt_field(key_object(passphrase).to_pem) # in case not already PEM
      rescue StandardError
        return [ErrorCode::BAD_KEY] # bad format OR passphrase is wrong
      end
    end

    # is crt properly formatted?
    @crt_object = self.crt_chain = nil # reset
    self.crt = crt_data
    begin
      crt_object # calls OpenSSL
      self.crt = crt_object.to_pem # in case not already PEM
    rescue StandardError
      return [ErrorCode::BAD_CRT]
    end

    # data DB properly decrypted?
    begin
      key_object unless key_data
    rescue StandardError
      return [ErrorCode::BAD_KEY]
    end

    # http://googleonlinesecurity.blogspot.co.uk/2014/09/gradually-sunsetting-sha-1.html
    if account.has_ssl_validation_sha1_signature? && crt_object.signature_algorithm == 'sha1WithRSAEncryption'
      return [ErrorCode::SHA1_SIGNATURE]
    end

    # do key and crt match?
    return [ErrorCode::MISMATCH] unless valid_key?

    # check domain (NOTE: not needed)
    # cn = "/CN=#{account.host_mapping}".downcase
    # subject = crt_object.subject.to_s
    # return [ ErrorCode::BAD_HOST, subject ] unless subject.to_s.downcase.index(cn)

    self.state = "initial" unless state

    # update_attributes_from_crt
    self.subject = crt_object.subject.to_s
    self.valid_until = crt_object.not_after

    # NOTE: ca_chain returns the missing intermediate (string) if unable to complete chain.  In this case, crt_chain is left empty.
    # Monitor will inform ops that a new intermediate needs to be added by checking a pending cert for crt_chain = null, and preventing
    # it from being actived until crt_chain is built.  Ops will use Certificate.find() followed by Certificate.append_intermediate()
    # to add a new intermediate (to ca_intermediates.pem) and build out crt_chain, allow the cert to be activated.  The customer need
    # not be informed of this issue, so don't return an error in this case.

    # check CRT chain
    @ca_chain = nil # reset
    unless ca_chain.is_a?(Array)
      return [ErrorCode::NEED_INTERMEDIATE]
    end

    unless store.verify(ca_chain, self)
      return [ErrorCode::BAD_CHAIN]
    end

    # `crt_chain` what gets written to nginx
    self.crt_chain = generate_crt_chain_to_pem

    # can "save" to DB now
    nil # [ ErrorCode::OK ]
  end

  def csr_object
    @csr_object ||= OpenSSL::X509::Request.new(csr)
  end

  # crt covers only the brand host mapped domains
  # method specific to lets encrypt - no wildcard certificates
  def crt_covers_all_domains?
    brand_host_mappings.sort == all_names_on_crt.sort
  end

  def key_object(passphrase = nil)
    key_data = Certificate.decrypt_field(key)
    # OpenSSL::PKey::RSA.new() hangs if key is encrypted and passphrase is nil
    raise(InvalidKeyError) if (key_data =~ /ENCRYPTED/) && !passphrase
    @key_object ||= OpenSSL::PKey::RSA.new(key_data, passphrase)
  end

  def valid_key?
    crt_object && key_object && crt_object.check_private_key(key_object)
  rescue OpenSSL::X509::CertificateError
    false
  end

  def crt_object
    @crt_object ||= OpenSSL::X509::Certificate.new(crt)
  end

  # list of brand host mappings that are protected by the uploaded certificate
  def crt_object_secured_domains(host_mappings = brand_host_mappings)
    return [] unless crt

    names = []
    wildcards = []

    all_names_on_crt.each do |name|
      if name =~ /^\*/
        wildcards << $'
      else
        names << name.downcase
      end
    end

    host_mappings.find_all do |host_mapping|
      names.include?(host_mapping) ||
        wildcards.find { |wc| host_mapping.end_with?(wc) }
    end
  rescue OpenSSL::X509::CertificateError
    []
  end

  def crt_object_cn
    if crt
      @crt_object_cn ||= R509::Cert.new(cert: crt).subject.CN
    end
  end

  def crt_not_before
    crt_object.not_before if crt
  end

  def crt_not_after
    crt_object.not_after if crt
  end

  def ca_chain
    @ca_chain ||= store.chain(crt_object)
  end

  def store
    @store ||= Certificate.certificate_authorities
  end

  def csr_subject
    subject_data.map { |pairs| "/" + pairs.join("=") }.join
  end

  def csr_san_includes_brands?
    san_names = []
    csr = csr_object.to_text

    brand_host_mappings.each do |host_mapping|
      san_names << host_mapping if csr.include?(host_mapping)
    end

    (brand_host_mappings - san_names).empty?
  end

  def append_intermediate(pem_file_or_string)
    @ca_chain = nil # reset
    msg = if pem_file_or_string.is_a?(String)
      store.append_intermediate_from_pem(pem_file_or_string, crt_object)
    else
      store.append_intermediate(pem_file_or_string, crt_object)
    end

    return msg if msg

    self.crt_chain = generate_crt_chain_to_pem
    # return "Failed to save new crt_chain" unless self.save # don't save or else rename to append_intermediate!
    "OK"
  end

  def to_nginx(pod_id:)
    # :subdomain is only used as a unique filename prefix for the key and cert
    # files to be referenced by nginx, and does not appear in the cert anywhere
    # SEE #generate_nginx_conf
    {
      updated_at:                updated_at,
      subdomain:                 cert_filename_prefix,

      # used by exodus to verify nginx updated after an account move
      host_mappings:             crt_covered_host_mappings, # deprecated
      crt_covered_host_mappings: crt_covered_host_mappings,

      # used by zendesk_certificates to configure nginx to use this certificate for these domains
      all_host_mappings:         all_host_mappings,

      sni:                       sni_enabled?,
      ip:                        sni_enabled? ? nil : certificate_ip!(pod_id: pod_id).ip,
      port:                      sni_enabled? ? nil : certificate_ip!(pod_id: pod_id).port,
      key:                       key,
      crt:                       crt_chain,
      conf:                      generate_nginx_conf(pod_id: pod_id),
      should_be_terminated:      should_be_terminated?
    }
  end

  def notify_customer_of_activation!
    SecurityMailer.deliver_certificate_approval(self)
  end

  def detach_certificate_ips!
    certificate_ips.each(&:detach_from_certificate!)
    certificate_ips.clear
  end

  def update_zendesk_provisioned_ssl_setting!
    if state == "active" && !autoprovisioned? && account.settings.automatic_certificate_provisioning?
      Rails.logger.info "Disabling zendesk provisioned ssl for account #{account_id} since customer provided certificate activated"
      account.settings.automatic_certificate_provisioning = false
      account.settings.save!
    end
  end

  def convert_to_sni!
    transaction do
      detach_certificate_ips!
      self.sni_enabled = true
      build_sni_pod_record
      save!
    end
  end

  def build_sni_pod_record(pod_id = account.pod_id)
    cip = certificate_ips.build
    cip.sni = true
    cip.pod_id = pod_id
    # WHY IS THIS BREAKING THINGS?!?! cip.save
    cip
  end

  def clear_ipm!
    if ipm_alert
      ipm_alert.destroy
      update_attribute(:ipm_alert_id, nil)
    end
  end

  class CertificateAlreadyActive < StandardError; end
  class CertificateIpRequired < StandardError; end
  class CertificateIpInUse < StandardError; end

  def activate_certificate!(certificate_ip:, notify_customer:)
    raise CertificateAlreadyActive if state == 'active'
    raise CertificateIpRequired if !sni_enabled? && !certificate_ip

    transaction do
      certificate_ip.try(:lock!)
      # - replacement certificates should activate their previous IP
      # - new certificates should activate unclaimed IPs
      active_certificates = self.class.where(account_id: account_id).active.to_a

      # a race condition can occur when activate! is called multiple times in quick succession (see ZD#1412854)
      # check if the certificate was activated whilst waiting for the above lock
      raise CertificateAlreadyActive if active_certificates.include?(self)

      if certificate_ip.try(:certificate) &&
        !certificate_ip.alias_record && # pre-created, so always there
        !active_certificates.include?(certificate_ip.certificate)
        raise CertificateIpInUse
      end

      active_certificates.each(&:revoke!)

      if certificate_ip
        certificate_ip.certificate = self
        certificate_ip.save!
      end

      activate!

      notify_customer_of_activation! if notify_customer
    end
  end

  private

  def all_host_mappings
    account.routes.map(&:host_mapping).compact
  end

  def crt_covered_host_mappings
    crt_object_secured_domains(all_host_mappings)
  end

  # Terminate only if account has host mapping for valid cert
  def should_be_terminated?
    crt && nginx_server_names.present? && crt_covered_host_mappings.any?
  end

  def activate_sni_certificate
    verify_certificate(:pending)

    if crt_chain.present?
      activate
      build_sni_pod_record
      Rails.logger.info("SNI certificate auto-activated")
    else
      Rails.logger.info("SNI certificate is missing crt_chain; sending to monitor for approval")
    end
  end

  def subject_data
    [
      ["C",  subject_country],
      ["ST", subject_state],
      ["L",  subject_locality],
      ["O",  subject_org],
      ["OU", subject_org_unit],
      ["CN", subject_domain]
    ].map { |attr, val| [attr, val.to_s] }
  end

  def generate_temporary_files
    set_csr R509::CSR.new(
      subject: subject_data,
      bit_length: @key_size || 4096,
      message_digest: 'sha256',
      san_names: cert_san
    )
  end

  # @param [R509::CSR] csr
  def set_csr(csr) # rubocop:disable Naming/AccessorMethodName
    self.csr = csr.to_pem
    self.key = Certificate.encrypt_field(csr.key.to_pem)
    self.subject = csr.subject.to_s
  end

  # This method used to write the nginx conf file directly to disk, but now
  # simply generates the content for the conf that will be served via API for a
  # separate service (bin/get_certificates in zendesk_certificates project) to
  # fetch and write to disk
  def generate_nginx_conf(pod_id:)
    if sni_enabled?
      generate_sni_nginx_conf(pod_id: pod_id)
    else
      generate_hosted_ssl_nginx_conf(pod_id: pod_id)
    end
  end

  def generate_hosted_ssl_nginx_conf(pod_id:)
    return "" unless crt
    return "" unless cip = certificate_ip!(pod_id: pod_id)
    return "" if cip.alias_record # aws uses dedicated ELBs to terminate SSL certs

    nginx_dir = Zendesk::Configuration.fetch(:certificates)["ssl_nginx_directory"]

    <<~NGINX
      # SSL configuration for #{account.name} on #{cip.ip}
      # Generated on #{Time.now}
      # Server name #{nginx_server_names};

      server {
        listen #{cip.port} ssl default_server;

        ssl_certificate #{nginx_dir}/#{cert_filename_prefix}.crt;
        ssl_certificate_key #{nginx_dir}/#{cert_filename_prefix}.key;

        set $agent_scheme $scheme;

        include /etc/nginx/servers/zendesk_ssl_proxy.incl;
      }
    NGINX
  end

  def generate_sni_nginx_conf(pod_id:)
    return "" unless should_be_terminated?
    nginx_dir = Zendesk::Configuration.fetch(:certificates)["ssl_nginx_directory"]

    # If this account is in an AWS pod, we need to use proxy_protocol so that
    # we correct pass the original IP address of the client.
    if CertificateIp::AWS_POD_IDS.include?(pod_id)
      proxy_protocol = <<~PROXY_PROTO
        listen 8443 ssl proxy_protocol;
        # Includes lists of network addresses to trust the PROXY protocol from
        include /etc/nginx/include/proxy_protocol_trusted_ips.incl;
        # Set $remote_addr to the client ip from the PROXY protocol
        real_ip_header proxy_protocol;
      PROXY_PROTO
    end

    <<~NGINX
      # SNI SSL configuration for #{account.name} (sni is hosted on the pod vip - not a seperate ip address)
      # Generated on #{Time.now}

      server {
        listen 443 ssl;
        #{proxy_protocol}
        # since we are using SNI we need a server_name directive
        server_name #{nginx_server_names};

        ssl_certificate #{nginx_dir}/#{cert_filename_prefix}.crt;
        ssl_certificate_key #{nginx_dir}/#{cert_filename_prefix}.key;

        set $agent_scheme $scheme;

        include /etc/nginx/servers/zendesk_ssl_proxy.incl;
      }
    NGINX
  end

  def nginx_server_names
    account.routes.map(&:host_mapping).reject(&:blank?).join(' ')
  end

  def generate_crt_chain_to_pem
    return nil unless ca_chain.is_a?(Array)
    store.intermediate_chain(ca_chain).map(&:to_pem).join("\n")
  end

  def all_names_on_crt
    cert_r509 = R509::Cert.new(cert: crt)

    begin
      cert_r509.all_names
    rescue R509::R509Error
      Rails.logger.error $!
      Rails.logger.error "cannot parse SANs from certificate #{id} from account #{account_id} - falling back to CN"
      [cert_r509.subject.CN]
    end
  end
end
