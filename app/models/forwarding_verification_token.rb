class ForwardingVerificationToken < ActiveRecord::Base
  GMAIL = 1
  belongs_to :account
  attr_accessible :to_email, :from_email, :email_provider, :value
  validates_presence_of :account_id, :value
end
