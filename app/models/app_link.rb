class AppLink < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible :app_id, :package_name, :paths, :fingerprints
  serialize :paths, Array
  serialize :fingerprints, Array

  validates :app_id, presence: { if: proc { |app| app.package_name.blank? } }
  validates :package_name, presence: { if: proc { |app| app.app_id.blank? } }
end
