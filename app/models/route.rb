require 'zendesk/models/route'

class Route < ActiveRecord::Base
  has_kasket
  has_kasket_on :id
  has_kasket_on :subdomain
  has_kasket_on :host_mapping
  has_kasket_on :gam_domain
  if ENV['KASKET_EXPIRES_IN_ROUTE'].to_i > 0
    kasket_expires_in ENV['KASKET_EXPIRES_IN_ROUTE'].to_i
  end

  ignore_column :ssl_enforced, :stanchion_state, :stanchion_state_unused_old

  def kasket_cacheable?
    super() && account.try(:pod_local?) && !account.is_locked?
  end

  include Zendesk::RoutingValidations
  include CIA::Auditable
  include RouteObserver

  before_validation :fix_host_mapping
  before_destroy :destroy_recipient_addresses
  before_update :update_recipient_address_host, if: :subdomain_changed?
  after_update :touch_brand, if: :subdomain_changed?
  after_update :propagate_subdomain_to_account, if: :subdomain_changed? && :agent_route?
  after_update :propagate_host_mapping_to_account, if: :host_mapping_changed? && :agent_route?
  audit_attribute :subdomain, :host_mapping, if: :auditable?

  attr_accessible :account, :subdomain, :host_mapping

  def self.find_by_fqdn(name)
    if name =~ /([^\.]*?)\.#{Zendesk::Configuration.fetch(:host)}$/
      find_by_subdomain($1)
    else
      find_by_host_mapping(name)
    end
  end

  def active_brand
    brand if brand.try(:active?)
  end

  def agent_route?
    account && account.route == self
  end

  private

  # Audit attributes on accounts without multiple brands will generate
  # 2 events when customer changes subdomain from account settings
  # mentioning a Brand model and those customers are not familiar with that concept
  #
  # Do not audit subdomain rename when we soft delete a brand (brand is nil in that case because of the default scope)
  def auditable?
    brand.present? && account && account.has_multiple_brands?
  end

  def propagate_subdomain_to_account
    if account.subdomain != subdomain
      logger.info "[Route] syncing route.subdomain to account.subdomain (Old: #{account.subdomain} => New: #{subdomain})"
      account.update_attribute(:subdomain, subdomain)
    end
  end

  def propagate_host_mapping_to_account
    if account.host_mapping != host_mapping
      logger.info "[Route] syncing route.host_mapping to account.host_mapping (Old: #{account.host_mapping} => New: #{account.host_mapping})"
      account.update_attribute(:host_mapping, host_mapping)
    end
  end

  def recipient_addresses
    @recipient_addresses ||= account.recipient_addresses.where("email like ?", "%@#{default_host_was}").to_a
  end

  def default_host_was
    host = default_host
    host = host.sub(subdomain, subdomain_was) if subdomain_changed?
    host
  end

  def update_recipient_address_host
    logger.info "[Route] updating #{recipient_addresses.count} recipient addresses like #{default_host_was}"
    recipient_addresses.each do |ra|
      new_email = ra.email.sub(/@.*/, "@#{default_host}")
      logger.info "[Route] updating #{ra.id} email (Old: #{ra.email} => New: #{new_email})"
      ra.update_column(:email, new_email)
    end
  end

  def destroy_recipient_addresses
    recipient_addresses.each(&:destroy)
  end

  def fix_host_mapping
    self.host_mapping = nil if host_mapping.blank?
    self.host_mapping = host_mapping.strip.downcase if host_mapping.present?
  end

  def touch_brand
    brand.touch_without_callbacks if brand
  end
end
