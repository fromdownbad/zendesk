class CouponRedemption < ActiveRecord::Base
  not_sharded
  disable_global_uid

  belongs_to :account
  belongs_to :coupon_application
  belongs_to :payment

  attr_accessible :account, :coupon_application, :discount, :payment

  validates_presence_of :account_id
  validates_presence_of :coupon_application
  validates_presence_of :payment

  delegate :coupon, to: :coupon_application
end
