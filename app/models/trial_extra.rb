class TrialExtra < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible :key, :value

  belongs_to :account

  def self.to_hash
    Hash[*all.flat_map { |te| [te.key, te.value] }]
  end

  def self.from_hash(hash)
    build_hash(hash).map { |h| new(h) }
  end

  def self.build_hash(hash)
    return [] unless hash.present?
    max_db_size = 255

    hash.map do |key, value|
      {
        key: sanitize(key).slice(0, max_db_size),
        value: sanitize(value).slice(0, max_db_size)
      }
    end
  end

  def self.sanitize(string)
    string.to_s.dup.sanitize.strip_tags
  end

  def self.redis_key(account_id)
    "account_#{account_id}_trial_extras"
  end
end
