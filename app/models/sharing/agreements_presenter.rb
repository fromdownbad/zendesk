require 'sharing/agreement_presenter'

module Sharing
  class AgreementsPresenter
    def initialize(agreements)
      @agreements = agreements
    end

    def as_json(_options = {})
      {
        agreements: agreements_as_json
      }
    end

    def agreements_as_json
      @agreements.map do |agreement|
        AgreementPresenter.new(agreement).as_json
      end
    end
  end
end
