require 'zendesk/radar_factory'

module Sharing
  class AgreementJob
    extend ZendeskJob::Resque::BaseJob
    extend Durable
    priority :medium
    self.job_timeout = 10.minutes

    def self.work(account_id, method, agreement_id, _audit = nil)
      account = Account.find(account_id)

      ActiveRecord::Base.on_shard(account.shard_id) do
        agreement = Sharing::Agreement.with_deleted { Sharing::Agreement.find_by_id(agreement_id) }
        method    = method.to_s

        if agreement && valid_method?(method)
          remote_url = agreement.remote_url # during send_to_partner remote_url can change
          logger.info("Ticket Sharing Log: UUID #{agreement.uuid} Remote URL #{remote_url} Status #{agreement.status}")

          begin
            http_response = agreement.send(method)
            if sending_invite?(method)
              response_hash = {code: http_response.status}
              unless [200, 201].include?(http_response.status)
                logger.info("Ticket Sharing Log: Response Code: #{http_response.status} FAILING")
                response_hash[:errors] = JSON.parse(http_response.body) if agreement.with_zendesk_account?
                agreement.update_attributes(status: :failed)
              end

              # update our subscribed radar client with the HTTP response of the POST'd sharing invite
              notification = ::RadarFactory.create_radar_notification(account, "invite/#{radar_key(remote_url)}")
              notification.send('response', response_hash)
            end
          rescue TicketSharing::TooManyRedirects
            logger.info('Ticket Sharing Log: TicketSharing::TooManyRedirects FAILING')
            if sending_invite?(method)
              agreement.update_attributes(status: :failed)
              notification = ::RadarFactory.create_radar_notification(account, "invite/#{radar_key(remote_url)}")
              notification.send('response', code: 500, errors: [["base", I18n.t("txt.error_message.sharing.agreement.radar_response.too_many_redirects").to_s]])
            end
          end
        end
      end
    end

    def self.valid_method?(method)
      ['send_to_partner', 'update_partner', 'auto_decline'].include?(method)
    end

    def self.args_to_log(account_id, method, agreement_id, audit = nil)
      { account_id: account_id, method: method, agreement_id: agreement_id, audit_id: audit_id(audit) }
    end

    def self.sending_invite?(method)
      method == "send_to_partner"
    end

    def self.radar_key(remote_url)
      parsed_url = URI.parse(remote_url)
      radar_key  = parsed_url.host
      # support the use of custom ports since some customers appear to be doing that often
      radar_key += ":#{parsed_url.port}" unless [80, 443].include?(parsed_url.port.to_i)
      radar_key
    end
  end
end
