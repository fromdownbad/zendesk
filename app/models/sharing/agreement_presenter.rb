module Sharing
  class AgreementPresenter
    def initialize(agreement)
      @agreement = agreement
    end

    def as_json
      {
        id: @agreement.id,
        name: @agreement.name,
        inbound: @agreement.in?,
        status: @agreement.status
      }
    end
  end
end
