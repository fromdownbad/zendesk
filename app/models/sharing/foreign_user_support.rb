module Sharing
  module ForeignUserSupport
    private

    def find_or_build_foreign_user(actor)
      # TSTODO what do we do if we didn't get a requester from the api?
      return unless actor

      identity = account.user_identities.foreign.find_by_value(actor.uuid)

      if identity
        user = identity.user
        user.name = actor.name
      else
        user = account.users.build(name: actor.name)

        user.identities << UserForeignIdentity.new(
          user: user,
          value: actor.uuid,
          is_verified: true
        )
        user.foreign = true
      end

      user.foreign_agent = actor.agent?
      user.save!

      user
    end
  end
end
