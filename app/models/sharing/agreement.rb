require 'ticket_sharing/agreement'
require 'sharing/foreign_user_support'

class Sharing::Agreement < ActiveRecord::Base
  include Sharing::ForeignUserSupport
  include Zendesk::ForbiddenAttributesProtection
  include CachingObserver

  self.table_name = 'sharing_agreements'

  has_soft_deletion default_scope: true

  TICKET_SHARING_REQUEST_TIMEOUT = 10
  MAX_INTERNAL_ATTEMPTS = 3

  # Never modify any of these `status => id` relationships.
  # Only add new ones or discontinue use of old ones.
  STATUS_MAP = {
    pending: 1,
    accepted: 2,
    declined: 3,
    inactive: 4,
    failed: 5,
    ssl_error: 6,
    configuration_error: 7
  }.freeze

  STATUS_ID_MAP = STATUS_MAP.invert

  DIRECTION_MAP = {
    in: true,
    out: false
  }.freeze

  SHARED_WITH_MAP = {
    zendesk: 0,
    jira: 1
  }.freeze

  JIRA_URL_SUFFIX = "/plugins/servlet/zendesk/sharing".freeze

  scope :accepted, -> { where(status_id: STATUS_MAP[:accepted]) }
  scope :inbound,  -> { where(direction: DIRECTION_MAP[:in])    }
  scope :outbound, -> { where(direction: DIRECTION_MAP[:out])   }
  scope :inactive, -> { where(status_id: STATUS_MAP[:inactive]) }

  belongs_to :account
  belongs_to :local_admin, class_name: 'User'
  belongs_to :remote_admin, class_name: 'User'
  has_many :shared_tickets, foreign_key: "agreement_id", inverse_of: :agreement, inherit: :account_id

  attr_accessible :accepted, :access_key, :account, :allows_public_comments, :created_type, :deactivated_type, :direction, :local_admin,
    :name, :remote_admin, :remote_subdomain, :remote_url, :shared_with_id, :status, :subdomain, :sync_tags, :sync_custom_fields, :uuid

  attr_accessor :subdomain, :mode

  attr_writer :accepted

  before_validation :set_status, on: :create
  before_validation :set_remote_url_from_subdomain
  before_validation :set_remote_url_protocol
  before_validation :set_name, on: :create
  before_validation :handle_accepted

  before_validation :generate_access_key, on: :create

  validate :subdomain_or_remote_url_exists
  validate :uuid_is_present_on_remote_agreements
  validate :remote_url_is_not_ours
  validate :remote_url_is_not_in_use
  validate :reactivation_rules
  validate :activation_rules
  validate :remote_url_is_formatted_properly, on: :create
  validates_presence_of :account_id
  validates_presence_of :status
  validates_uniqueness_of :uuid, allow_nil: true, scope: :account_id
  validates_format_of :uuid, with: /\A[a-zA-Z0-9]+\z/, if: lambda { |record| record.uuid.present? }

  after_validation :append_jira_url_suffix, on: :create

  before_save :store_deactivated_type

  after_create :generate_uuid
  after_commit :handle_auto_decline, on: :create
  after_commit :notify_owner_of_new_agreement, on: :create
  after_commit :send_agreement_request_to_partner, on: :create

  before_update :check_for_syncable_changes
  before_update :check_for_status_update
  after_commit :refresh_support_addresses, on: :update
  after_commit :enqueue_partner_update, on: :update
  after_update :notify_owner_of_status_change

  def self.find_by_uuid(uuid)
    where(uuid: uuid.to_s).first
  end

  def self.find_by_uuid_and_access_key(uuid, access_key)
    where(uuid: uuid.to_s, access_key: access_key.to_s).first
  end

  def status=(val)
    write_attribute(:status_id, STATUS_MAP[val.to_sym]) unless val.blank?
  end

  def status
    STATUS_ID_MAP[status_id]
  end

  def remote_account
    @remote_account ||= Sharing::Agreement.remote_account_for_url(remote_url)
  end

  def self.remote_account_for_url(remote_url)
    uri = URI.parse(remote_url)
    if uri.host =~ /\A(.+)\.#{Regexp.escape(Zendesk::Configuration.fetch(:host))}\z/
      Account.find_by_subdomain($1)
    else
      Account.find_by_host_mapping(uri.host)
    end
  rescue URI::InvalidURIError
  end

  # This returns a hash
  #  key === all the remote_url's for the agreements needed for the presenter
  #  value is the account that is associated to the remote_url
  #
  # Goal is to stop an N+1 like condition in the SharingAgreements API
  def self.remote_accounts_for(agreements)
    agreements.each_with_object({}) do |agreement, account_hash|
      account_hash[agreement.remote_url] ||= Sharing::Agreement.remote_account_for_url(agreement.remote_url)
    end
  end

  def append_jira_url_suffix
    return unless shared_with_id == SHARED_WITH_MAP[:jira]
    return if remote_url =~ /#{JIRA_URL_SUFFIX}/
    remote_url.strip!
    remote_url.chop! if remote_url.end_with?('/')
    remote_url << JIRA_URL_SUFFIX
  end

  def setup_from_partner(agreement)
    self.status = :pending
    self.created_type = :remote
    self.name = agreement.name
    self.uuid = agreement.uuid
    self.access_key = agreement.access_key
    self.remote_admin = find_or_build_foreign_user(agreement.current_actor)
    self.sync_tags = agreement.sync_tags
    self.sync_custom_fields = agreement.sync_custom_fields
    self.allows_public_comments = agreement.allows_public_comments
    self.remote_url = agreement.sender_url
    self.direction = 'in'
  end

  def update_from_partner(agreement)
    @updated_from_partner = true

    self.name   = agreement.name
    self.status = agreement.status
    self.remote_admin = find_or_build_foreign_user(agreement.current_actor)
    @status_changed_from_partner = status_id_changed?

    if status_id_changed?
      if changes['status_id'] == [STATUS_MAP[:pending], STATUS_MAP[:declined]]
        # agreement is deleted
        @agreement_revoked = true
      elsif changes['status_id'].include?(STATUS_MAP[:declined])
        # invite was declined
        @agreement_terminated = true
      elsif changes['status_id'].include?(STATUS_MAP[:inactive])
        @activation_changed = true
      end
    end

    if status == :declined
      soft_delete_shared_tickets
      self.deleted_at = Time.now
    end
  end

  def remote_admin_name
    remote_admin.try(:name) || I18n.t('txt.admin.models.sharing.agreement.a_remote_administrator')
  end

  def local_admin_name
    local_admin.try(:name) || I18n.t('txt.admin.models.sharing.agreement.a_local_administrator')
  end

  def changed_by
    @status_changed_from_partner ? remote_admin_name : local_admin_name
  end

  def direction=(value)
    write_attribute(:direction, value.to_sym == :in)
  end

  def in?
    direction
  end

  def out?
    !in?
  end

  def created_type=(value)
    write_attribute(:created_type, value.to_sym != :remote)
  end

  def created_locally?
    created_type
  end

  def acceptable?
    !created_locally? && pending?
  end

  def destroy
    original_status = status
    self.status = :declined

    soft_delete_shared_tickets
    soft_delete!

    if original_status == :inactive
      AgreementMailer.deliver_sharing_agreement_deleted(self)
    end
  end

  def failed?
    status == :failed
  end

  def pending?
    status == :pending
  end

  def accepted?
    status == :accepted
  end

  def inactive?
    status == :inactive
  end

  def declined?
    status == :declined
  end

  def error?
    status == :ssl_error || status == :configuration_error
  end

  def cancellable?
    (pending? || error? || declined?) && !acceptable?
  end

  def deactivatable?
    accepted?
  end

  def deactivated_type=(value)
    write_attribute(:deactivated_type, value.to_sym == :local)
  end

  def deactivated_locally?
    deactivated_type
  end

  def reactivatable?
    inactive? && deactivated_locally?
  end

  def deletable?
    inactive? || failed?
  end

  def send_to_partner
    retry_on_internal_error do
      ts_agreement = for_partner
      ts_agreement.send_to(remote_url)
      TicketSharing.handle_response(self, :send_to_partner, ts_agreement.response)
      ts_agreement.response
    end
  rescue SocketError
    if /Temporary failure in name resolution/i.match?($!.message)
      error_response
    else
      Rails.logger.info("Ticket Sharing Log: SocketError: Account #{account_id} Agreement #{id} Method send_to_partner with exception: #{$!} At #{Time.now}")
      record_config_error! unless failed?
      error_response("base", I18n.t("txt.error_message.sharing.agreement.radar_response.socket_error_temp_failure"))
    end
  rescue Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNRESET
    error_response
  rescue OpenSSL::SSL::SSLError, Faraday::SSLError
    if zendesk_http_remote_url?
      self.remote_url = remote_url.sub(/^http:/, "https:")
      if save!
        retry
      end
    end
    record_ssl_error! unless failed?
    error_response
  rescue URI::InvalidURIError
    fail!
    error_response
  rescue Faraday::RestrictIPAddresses::AddressNotAllowed
    record_config_error! unless failed?
    error_response
  rescue TicketSharing::InternalSharingWarning => e
    e
  rescue TicketSharing::InternalSharingError => e
    record_config_error! unless failed?
    e
  end

  def update_partner
    retry_on_internal_error do
      ts_agreement = for_partner
      ts_agreement.update_partner(remote_url)
      TicketSharing.handle_response(self, :update_partner, ts_agreement.response)
      ts_agreement.response
    end
  rescue SocketError
    unless /Temporary failure in name resolution/i.match?($!.message)
      Rails.logger.info("Ticket Sharing Log: SocketError: Account #{account_id} Agreement #{id} Method update_partner with exception: #{$!} At #{Time.now}")
      record_config_error! unless failed?
    end
    error_response
  rescue Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNRESET
    error_response
  rescue OpenSSL::SSL::SSLError, Faraday::SSLError
    # TODO: copy-pasted logic from app/models/shared_ticket.rb
    if zendesk_http_remote_url?
      self.remote_url = remote_url.sub(/^http:/, "https:")
      if save!
        retry
      end
    end
    record_ssl_error! unless failed?
    error_response
  rescue URI::InvalidURIError
    fail!
    error_response
  rescue Faraday::RestrictIPAddresses::AddressNotAllowed
    record_config_error! unless failed?
    error_response
  rescue TicketSharing::InternalSharingWarning => e
    e
  rescue TicketSharing::InternalSharingError => e
    record_config_error! unless failed?
    e
  end

  def retry_on_internal_error(&block)
    attempts = MAX_INTERNAL_ATTEMPTS
    condition = -> (_) do
      Rails.logger.info("Ticket Sharing Log: Recording Internal error: Account #{account_id} Agreement #{id} At #{Time.now}.  Retrying...")
      !failed?
    end
    Zendesk::Retrier.retry_on_error([TicketSharing::InternalSharingFailure], attempts, retry_if: condition, &block)
  end

  def zendesk_http_remote_url?
    with_zendesk_account? && remote_url !~ /^https:\/\//
  end

  def fail!
    Rails.logger.info("Ticket Sharing Log: Failing agreement: Account #{account_id} Agreement #{id} At #{Time.now}")
    self.status = :failed
    save!
  end

  def record_config_error!
    Rails.logger.info("Ticket Sharing Log: Recording configuration error: Account #{account_id} Agreement #{id} At #{Time.now}")
    self.status = :configuration_error
    save!
  end

  def record_ssl_error!
    Rails.logger.info("Ticket Sharing Log: Recording SSL error: Account #{account_id} Agreement #{id} At #{Time.now}")
    # If the SSL error is between another Zendesk, it's our fault and don't change the agreement
    unless with_zendesk_account?
      self.status = :ssl_error
      save!
    end
  end

  def mark_as_deleted
    super
    self.status = :declined
  end

  def auto_decline
    mark_as_deleted
    save!
  end

  def for_partner
    agreement_type = account.try(:has_ticket_sharing_whitelist?) ? TicketSharing::NonVerifyAgreement : TicketSharing::Agreement
    agreement = agreement_type.new(
      'name' => account.sharing_name,
      'uuid' => uuid,
      'access_key' => access_key,
      'status' => status.to_s
    )

    if in?
      agreement.sender_url   = remote_url
      agreement.receiver_url = account.sharing_url
    else
      agreement.sender_url   = account.sharing_url
      agreement.receiver_url = remote_url
    end

    if created_locally? && pending?
      agreement.sync_tags = sync_tags?
      agreement.sync_custom_fields = sync_custom_fields?
      agreement.allows_public_comments = allows_public_comments?
    end

    agreement.current_actor = local_admin.for_partner if local_admin

    agreement
  end

  def authentication_token
    for_partner.authentication_token
  end

  def allows_partner_updates?
    account.is_serviceable? && (accepted? || inactive?)
  end

  def retrieve_jira_projects
    url = remote_url + "/info"
    headers = {"X-Ticket-Sharing-Token" => authentication_token, "accept" => "json"}
    if response = Zendesk::Net::MultiSSL.get_json(url, headers: headers, timeout: 10)
      response["projects"]
    end
  end

  def system_status_change(new_status)
    self.status = new_status
    @system_status_change = true
  end

  def generic_sharing_url(subdomain)
    port = Rails.env.development? && Zendesk::Configuration[:port] ? ":#{Zendesk::Configuration[:port]}" : nil
    "https://#{subdomain}.#{Zendesk::Configuration.fetch(:host)}#{port}/sharing"
  end

  def resend_invite
    now = Time.now
    self.last_sent ||= 10.minute.ago

    # Resend limit 1 request per minute.
    return nil unless (now - self.last_sent) > 60

    self.last_sent = now
    save!
    enqueue_job(:send_to_partner)
  end

  def enqueue_job(method)
    return unless can_communicate_with_partner?
    Sharing::AgreementJob.enqueue(account_id, method, id)
  end

  def can_communicate_with_partner?
    status != :ssl_error
  end

  # TODO: remove the remote_account check after shared_with_id has been backfilled. ZD#1755584
  def with_zendesk_account?
    SHARED_WITH_MAP[:zendesk] == shared_with_id || remote_account
  end

  def support_addresses
    return [] unless remote_account
    response = request_recipient_addresses
    return error_response unless response.try(:status) == 200
    recipient_addresses = response.body.try(:[], "recipient_addresses")
    return error_response if recipient_addresses.nil? || !recipient_addresses.respond_to?(:map)
    recipient_addresses.map { |recipient_address| recipient_address["email"] }
  end

  def remote_zendesk_host
    URI.parse(remote_url).host
  end

  private

  def request_recipient_addresses
    return unless remote_account
    remote_account_subdomain = remote_account.subdomain
    client = Zendesk::InternalApi::Client.new(remote_account_subdomain)
    client.connection.options.timeout = TICKET_SHARING_REQUEST_TIMEOUT
    begin
      client.connection.get("/api/v2/recipient_addresses.json")
    rescue ZendeskAPI::Error::RecordInvalid, ZendeskAPI::Error::NetworkError, ZendeskAPI::Error::RecordNotFound => e
      Rails.logger.error("ERROR requesting recipient addresses")
      Rails.logger.error(e.message)
      Rails.logger.error(e.backtrace)
      nil
    end
  end

  def error_response(attribute = nil, message = nil)
    body = "[[\"#{attribute}\",\"#{message}\"]]"
    TicketSharing::FakeHttpResponse.new(500, body)
  end

  def subdomain_or_remote_url_exists
    return if remote_url.present?

    if subdomain.blank?
      errors.add(I18n.t('activerecord.attributes.sharing.agreement.remote_url'), :blank)
    else
      errors.add(I18n.t('activerecord.attributes.sharing.agreement.subdomain'), I18n.t('txt.errors.sharing.agreement.subdomain_errors.doesnt_exist'))
    end
  end

  def set_remote_url_from_subdomain
    return if remote_url.present?
    return if subdomain.blank?

    # only set the remote_url if it's actually a valid subdomain, otherwise we shortcut past subdomain_or_remote_url_exists
    # we need to silence the account restriction because we may be in the context of another account
    return unless AccountRestrictionObserver.silence { Account.find_by_subdomain(subdomain) }

    self.remote_url = generic_sharing_url(subdomain)
  end

  def set_remote_url_protocol
    return if subdomain.present?
    return if remote_url.present? && remote_url.match(/^https?:\/\//)

    sharing_url     = "http://#{remote_url}"
    sharing_url    += "/sharing" unless SHARED_WITH_MAP[:jira] == shared_with_id
    self.remote_url = sharing_url
  end

  def needs_name?
    name.nil? || (name && name.empty?)
  end

  def set_name
    if subdomain.present?
      self.name = "#{subdomain} @ Zendesk" if needs_name?
    else
      begin
        self.name = URI.parse(remote_url).host if needs_name?
        if name.nil?
          raise URI::InvalidURIError
        end
      rescue URI::InvalidURIError
        errors.add(:base, "#{I18n.t('txt.error_message.sharing.agreement.invalid_uri_error')} #{I18n.t('activerecord.errors.messages.invalid')}")
        false
      end
    end
  end

  def set_status
    self.status_id ||= STATUS_MAP[:pending]
  end

  def send_agreement_request_to_partner
    return unless created_locally?
    enqueue_job(:send_to_partner)
  end

  def generate_access_key
    return unless created_locally?
    self.access_key = Digest::SHA1.hexdigest((Time.now.to_i + rand(10**10)).to_s)
  end

  def generate_uuid
    return if uuid.present?

    key = "#{account.sharing_url}/agreements/#{id}"
    update_column(:uuid, Digest::SHA1.hexdigest(key))
  end

  def uuid_is_present_on_remote_agreements
    return if created_locally?

    if uuid.blank?
      errors.add(:uuid, :blank)
    end
  end

  def remote_url_is_not_ours
    return unless account

    # missing route urls and hostmapping
    own_urls = [
      generic_sharing_url(account.subdomain),
      generic_sharing_url(account.subdomain).sub("https", "http"),
      account.sharing_url
    ]

    if own_urls.include?(remote_url)
      errors.add(I18n.t('activerecord.attributes.sharing.agreement.remote_url'), I18n.t('txt.errors.sharing.agreement.subdomain_errors.cannot_point_to_your_zendesk'))
    end
  end

  def remote_url_is_not_in_use
    return unless account

    siblings = account.sharing_agreements.select do |a|
      a.accepted? &&
        a.remote_url == remote_url &&
        a.direction == direction &&
        a != self
    end

    if siblings.any? && (pending? || accepted?)
      errors.add(:base, I18n.t('txt.error_message.sharing.agreement.you_have_an_active_agreement'))
    end
  end

  def remote_url_is_formatted_properly
    return if remote_url =~ URL_PATTERN
    unless errors.messages.include? I18n.t('activerecord.attributes.sharing.agreement.subdomain').to_sym
      errors.add(I18n.t('activerecord.attributes.sharing.agreement.remote_url'), I18n.t('activerecord.errors.messages.invalid'))
    end
  end

  def handle_accepted
    if @accepted.to_s == 'true'
      self.status = :accepted
    elsif @accepted.to_s == 'false'
      mark_as_deleted
    end
  end

  def check_for_syncable_changes
    syncable_attributes = ['status_id']
    @syncable_changes = (changes.keys & syncable_attributes).any?
    true
  end

  def check_for_status_update
    @status_changed = status_id_changed?
    true
  end

  def enqueue_partner_update
    return if @updated_from_partner
    return if @system_status_change
    return unless @syncable_changes

    enqueue_job(:update_partner)
  end

  def notify_owner_of_new_agreement
    return if created_locally?
    return if account.reject_sharing_invites?

    AgreementMailer.deliver_new_from_partner(self)
  end

  def handle_auto_decline
    enqueue_auto_decline if auto_decline?
  end

  def enqueue_auto_decline
    enqueue_job(:auto_decline)
  end

  def auto_decline?
    account.reject_sharing_invites? && in?
  end

  def notify_owner_of_status_change
    if @agreement_terminated
      AgreementMailer.deliver_sharing_agreement_deleted(self)
    elsif @activation_changed
      AgreementMailer.deliver_activation_change(self)
    elsif @status_changed
      # we don't want to send an email to the person who declined, just the partner
      if @agreement_revoked && declined?
        AgreementMailer.deliver_invite_revoked(self)
      # send email to the partner if status changes, also send email anytime agreement is cancelled, errored, or inactivated
      elsif @status_changed_from_partner || error? || failed? || inactive?
        AgreementMailer.deliver_status_change(self, changed_by)
      end
    end
  end

  def store_deactivated_type
    if changes['status_id'] == [STATUS_MAP[:accepted], STATUS_MAP[:inactive]]
      self.deactivated_type = @updated_from_partner ? :remote : :local
    end

    true
  end

  def activation_rules
    return unless @status_changed_from_partner

    if in? && changes['status_id'] == [STATUS_MAP[:pending], STATUS_MAP[:accepted]]
      errors.add(:base, I18n.t('activerecord.errors.messages.invalid'))
    end
  end

  def reactivation_rules
    return unless attempting_to_reactivate?

    deactivation_location = deactivated_locally? ? :local : :remote
    reactivation_location = @status_changed_from_partner ? :remote : :local

    if deactivation_location != reactivation_location
      errors.add(:base, I18n.t('txt.error_message.sharing.agreement.you_cannot_reactivate_an_agreement'))
    end
  end

  def attempting_to_reactivate?
    changes['status_id'] == [STATUS_MAP[:inactive], STATUS_MAP[:accepted]]
  end

  def soft_delete_shared_tickets
    SharedTicketBulkDeleteJob.enqueue(account.id, id)
  end

  def accepted_status_changed?
    previous_changes['status_id'] && previous_changes['status_id'].include?(STATUS_MAP[:accepted])
  end

  def refresh_support_addresses
    return unless accepted_status_changed?
    TicketSharingSupportAddressesJob.enqueue(account_id)
  end
end
