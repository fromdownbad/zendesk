module Sharing
  class TicketJob
    extend ZendeskJob::Resque::BaseJob
    extend Durable
    priority :medium
    self.job_timeout = 10.minutes

    def self.work(account_id, method, shared_ticket_id, _audit = nil, _unused_env = nil)
      account = Account.find(account_id)
      ActiveRecord::Base.on_shard(account.shard_id) do
        shared_ticket = SharedTicket.with_deleted { SharedTicket.find_by_id(shared_ticket_id) }
        method = method.to_s

        # if you see this it's probably because this job was enqueued by an "after_*" callback and not an "after_commit_on_*" callback
        raise "shared_ticket_id #{shared_ticket_id} not found! (account: #{account.subdomain}, method: #{method})" if shared_ticket.nil?
        raise "invalid method: #{method}" unless valid_method?(method)

        if shared_ticket.deleted_at && method != 'unshare'
          resque_log("Ticket Sharing Log: Aborting ticket update! Ticket already unshared. (shared_ticket_id: #{shared_ticket_id}, account: #{account.subdomain}, method: #{method})")
          return
        end

        if (account.subscription.hub_account || account).has_skip_ticket_sharing_jobs?
          resque_log("Skipping Sharing::TicketJob for #{account.id} due to arturo bit skip_ticket_sharing_job")
          return
        end

        # if the agreement has been deleted skip job and don't raise exception (exceptions ensure Durable persist)
        unless shared_ticket.agreement
          resque_log("Ticket Sharing Log: Sharing ticket job skipped! Agreement deleted or non-existent. (shared_ticket_id: #{shared_ticket_id}, account: #{account.subdomain}, method: #{method})")
          return
        end

        sleep 10 unless Rails.env.test? # this is stupid, but necessary to stagger this job to avoid conflicting with ApplyMacrosJob

        response_code = Zendesk::DB::RaceGuard.cache_locked("post-ticket-event/#{account_id}/#{shared_ticket.ticket_id}", wait_max: 10.seconds) do
          shared_ticket.send(method)
        end
        # 499 => failure to create shared ticket object for partner
        resque_log("Ticket Sharing Log: Response code: #{response_code}")
        if response_code == 404 && shared_ticket.created_at > 2.minutes.ago && ['update_partner', 'unshare'].include?(method)
          # raising error to get this back into the durable queue
          raise "Possible parallel processing problem: shared_ticket_id #{shared_ticket_id} got 404 for #{method} with recently created ticket."
        elsif response_code >= 500
          if remote_account = shared_ticket.agreement.remote_account
            raise "Ticket Sharing network error: shared_ticket_id #{shared_ticket_id} got #{response_code} for #{method} from #{remote_account.id}."
          else
            resque_log("Ignoring #{response_code} response from external account.")
          end
        end
      end
    end

    def self.args_to_log(account_id, method, shared_ticket_id, audit = nil, _unused_env = nil)
      { account_id: account_id, method: method, shared_ticket_id: shared_ticket_id, audit_id: audit_id(audit) }
    end

    def self.valid_method?(method)
      ['send_to_partner', 'update_partner', 'unshare'].include?(method)
    end
  end
end
