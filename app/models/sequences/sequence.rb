class Sequence < ActiveRecord::Base
  class SequenceTransactionError < StandardError; end
  class SequenceLockfreeError < StandardError; end

  APM_SERVICE_NAME = 'classic-sequence'.freeze

  belongs_to :account

  attr_accessible :account, :value

  validates_presence_of :account_id

  def next
    if number_of_open_transactions > 0
      # This is an error- Nice IDs should NOT be generated in a transaction, or
      # the transaction will hold a lock on the sequences table
      s = SequenceTransactionError.new("Sequence#next called within transaction")
      ZendeskExceptions::Logger.record(s, location: self, message: s.message, fingerprint: '76c2e70ecb225c9423657e9a1e3a6fa11564da3d')
    end

    ZendeskAPM.trace(
      'sequence',
      resource: "#{self.class.name}#lockfree_increment",
      service: APM_SERVICE_NAME
    ) do |span|
      span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
      span.set_tag("zendesk.account_id",        account_id)
      span.set_tag("zendesk.account_subdomain", account.subdomain)
      span.set_tag('zendesk.db.shard', account.shard_id)

      if db_cluster = DB_SHARDS_TO_CLUSTERS[account.shard_id]
        span.set_tag('zendesk.db.cluster', db_cluster.to_s)
      end

      lockfree_increment

      span.set_tag("value", value)
    end
    value
  end

  # See: https://dev.mysql.com/doc/refman/5.6/en/innodb-locking-reads.html
  #
  # This pattern is effectively lock-free. Although self-referential updates do
  # appear to open implicit row locks, it exists only for the duration of the
  # single auto-committed query. The duration of the lock is held exclusively
  # in MySQL, and does not wait on the ruby layer to commit.
  def lockfree_increment
    # `insert` returns the LAST_INSERT_ID (even though this is an UPDATE statement)
    new_value = self.class.connection.insert "UPDATE sequences SET value = LAST_INSERT_ID(value + 1) WHERE id = #{id.to_i}"

    # Paranoia around the returned nice_id being missing or stale
    raise SequenceLockfreeError, "Sequence#next decreased. original value: #{value}, new value: #{new_value}" if new_value < value

    self.value = new_value
  end

  def peek
    value + 1
  end

  private

  def number_of_open_transactions
    ActiveRecord::Base.connection.open_transactions
  end
end
