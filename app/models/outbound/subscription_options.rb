module Outbound
  # Ensure requirements are meet upon subscribe/amend with Outbound
  class SubscriptionOptions
    # Basic error class
    class Error < StandardError; end

    # Thrown when the monthly messaged users value is invalid
    class InvalidMonthlyMessagedUsers < Error; end

    private_class_method :new

    def self.build_for(account, subscription_options)
      outbound_subscription_options = new(account, subscription_options).build
      outbound_subscription_options.send(:subscription_options)
    end

    def build
      return self unless outbound_monthly_messaged_users.present?

      if prevent_subscribe?
        remove_outbound_from_options
      elsif !zero_monthly_messaged_users?
        ensure_valid_monthly_messaged_users
      end

      self
    end

    private

    attr_reader :account, :subscription_options

    delegate :outbound_monthly_messaged_users,
      to: :subscription_options

    delegate :subscribed?,
      to: :outbound_subscription,
      prefix: :outbound,
      allow_nil: true

    def initialize(account, subscription_options)
      @account              = account
      @subscription_options = subscription_options.dup
    end

    def prevent_subscribe?
      zero_monthly_messaged_users? && !outbound_subscribed?
    end

    def remove_outbound_from_options
      [:outbound_plan_type, :outbound_monthly_messaged_users].each do |attribute|
        subscription_options.send(:"#{attribute}=", nil)
      end
    end

    def ensure_valid_monthly_messaged_users
      return if outbound_monthly_messaged_users.to_i >= 0
      raise InvalidMonthlyMessagedUsers
    end

    def zero_monthly_messaged_users?
      outbound_monthly_messaged_users.to_i.zero?
    end

    def outbound_subscription
      @outbound_subscription ||= account.outbound_subscription
    end
  end
end
