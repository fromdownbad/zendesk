module Outbound
  class Plan
    attr_reader :account

    delegate :state_updated_at,
      to: :plan

    def initialize(account)
      @account = account
    end

    def active_trial?
      return false unless exists?

      trial? &&
        !trial_expired? &&
        active_plan?
    end

    def active_subscription?
      return false unless exists?
      return false if trial?

      active_plan? && subscribed?
    end

    def billing_cycle
      return nil unless exists?

      start_date, end_date = nil
      now = DateTime.now
      if billing_cycle_day <= now.day
        start_date = billing_date_time(now.month, now.year)
        end_date   = billing_end_date_time(now.month + 1, now.year)
      else
        start_date = billing_date_time(now.month - 1, now.year)
        end_date   = billing_end_date_time(now.month, now.year)
      end

      [start_date, end_date]
    end

    def messaged_users_cap
      return nil unless exists?
      return nil if trial?

      monthly_messaged_users = plan_settings.fetch('monthly_messaged_users', nil)
      boosted_messaged_users = plan_settings.fetch('boosted_messaged_users', 0)
      boost_expires_at       = plan_settings.fetch('boost_expires_at', nil)

      if boost_eligible?(boost_expires_at)
        return monthly_messaged_users.to_i + boosted_messaged_users
      end

      monthly_messaged_users
    end

    def exists?
      plan.present?
    end

    def first_subscribed_period?
      return false unless active_subscription?
      end_of_first_subscribed_period = if billing_cycle_day <= state_updated_at.day
        billing_end_date_time(state_updated_at.month + 1, state_updated_at.year)
      else
        billing_end_date_time(state_updated_at.month, state_updated_at.year)
      end

      state_updated_at <= DateTime.now && DateTime.now <= end_of_first_subscribed_period
    end

    private

    def trial_expired?
      plan.trial_expires_at <= current_date_time_in_account_time_zone
    end

    def trial?
      return false unless exists?

      [:trial].include?(plan.state)
    end

    def subscribed?
      return false unless exists?

      [:subscribed].include?(plan.state)
    end

    def active_plan?
      exists? && plan.active?
    end

    def billing_cycle_day
      return nil unless exists?

      plan_settings.fetch('billing_cycle_day')
    end

    def billing_date_time(month, year)
      month, year = normalize_month_and_year(month, year)

      if Date.valid_date? year, month, billing_cycle_day
        DateTime.new(year, month, billing_cycle_day)
      else
        Date.civil(year, month, -1).to_datetime
      end
    end

    def billing_end_date_time(month, year)
      billing_date_time(month, year).prev_day.end_of_day
    end

    def normalize_month_and_year(month, year)
      if month.zero?
        [12, year - 1]
      elsif month == 13
        [1, year + 1]
      else
        [month, year]
      end
    end

    def boost_eligible?(boost_expires_at)
      return false unless account.has_outbound_boosted_messaged_users?
      return false unless boost_expires_at.present?

      DateTime.now < DateTime.parse(boost_expires_at)
    end

    def client
      @client ||= Zendesk::Accounts::Client.new(account)
    end

    def plan_settings
      plan.plan_settings
    end

    def plan
      Rails.cache.fetch(account.scoped_cache_key(:outbound_plan), expires_in: 1.hour) do
        client.product('outbound')
      end
    end

    def current_date_time_in_account_time_zone
      DateTime.now.in_time_zone(account.time_zone)
    end
  end
end
