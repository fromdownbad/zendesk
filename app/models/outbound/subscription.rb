module Outbound
  class Subscription < AccountServiceSubscription
    PLAN_SETTINGS = %i[
      monthly_messaged_users
      billing_cycle_day
    ].freeze

    define_attr_accessors

    PRODUCT_NAME      = 'outbound'.freeze
    DEFAULT_PLAN_TYPE = 1

    def monthly_messaged_users
      plan_settings['monthly_messaged_users'] || 0 # 0 for trial
    end

    def plan_type
      DEFAULT_PLAN_TYPE
    end

    def active?
      !expired? && (subscribed? || (trial? && !trial_expired?))
    end

    alias_method :include_in_easy_agent_add?, :active?

    def inspect
      "#<#{self.class}:0x#{(object_id << 1).to_s(16)} state=#{state} " \
        "monthly_messaged_users=#{monthly_messaged_users} " \
        "billing_cycle_day=#{billing_cycle_day}>"
    end

    private

    def trial_expired?
      trial_expires_at <= Time.now
    end
  end
end
