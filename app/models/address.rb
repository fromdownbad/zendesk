require 'zendesk/url_normalizer'

module StringArgument
  def country=(string_or_country)
    if string_or_country.is_a?(String)
      string_or_country = Country.find_by_code(string_or_country)
    end

    super string_or_country
  end
end

class Address < ActiveRecord::Base
  extend ActiveHash::Associations::ActiveRecordExtensions
  include PrecreatedAccountConversion
  prepend StringArgument

  not_sharded
  disable_global_uid

  belongs_to :account
  belongs_to_active_hash :country

  attr_accessible :name, :phone, :country, :zip, :province, :city, :country_id, :state, :street, :vat, :website_url

  before_validation     :normalize_website_url
  validates_presence_of :account_id, :name, :phone

  before_save :set_vat
  after_save  :update_account_version

  def normalize_website_url
    self.website_url = UrlNormalizer.normalize(website_url)
  end

  def country_code
    country.try(:code)
  end

  def to_s
    [name, street, zip, city, state, province, country.try(:name) || ""].compact.join(' / ')
  end

  def lines(with_name = true)
    lines = []
    lines << name if with_name
    lines << "VAT #{vat}" unless vat.blank?
    lines << ''

    [:street, :city, :zip, :state, :province].each do |item|
      value = send(item)
      lines << value if value.present? && !value.index('Please add')
    end

    lines << country.name if country.present?
    lines
  end

  private ########################################

  def set_vat
    self.vat = nil if vat.blank? || vat == country_code
  end

  def update_account_version
    if website_url_changed?
      account.smart_touch
    end
  end
end
