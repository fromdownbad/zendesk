module Users::Roles
  extend ActiveSupport::Concern

  MAX_AGENT_EXCEEDED = 'MaxAgentExceeded'.freeze
  included do |base|
    belongs_to :permission_set, inherit: :account_id

    scope :end_users, ->(*_args) { base.where(roles: 0) }
    scope :agents,    ->(*_args) { base.where(roles: [Role::AGENT.id, Role::ADMIN.id]).order("users.name ASC") }
    scope :admins,    ->(*_args) { base.where(roles: Role::ADMIN.id).order("users.name ASC") }
    scope :revere_subscribers, -> (*_args) {
      base.agents.
        joins(:settings).
        where(user_settings: { name: 'revere_subscription', value: '1'})
    }

    scope :light_agents, ->(*_args) {
      base.where(roles: Role::AGENT.id, permission_sets: { role_type: ::PermissionSet::Type::LIGHT_AGENT }).
        joins(:permission_set).
        order("users.name ASC")
    }

    scope :contributors, ->(*) {
      base.where(roles: Role::AGENT.id, permission_sets: { role_type: ::PermissionSet::Type::CONTRIBUTOR }).
        joins(:permission_set).
        order("users.name ASC")
    }

    # Note: CP3 only
    scope :chat_agents, ->(*_args) {
      base.where(roles: Role::AGENT.id, permission_sets: { role_type: ::PermissionSet::Type::CHAT_AGENT }).
        joins(:permission_set).
        order("users.name ASC")
    }

    scope :billable_agents, ->(*_args) {
      base.where("roles = #{Role::ADMIN.id} OR \
        (roles = #{Role::AGENT.id} AND \
          ( \
            users.permission_set_id IS NULL OR \
            (users.permission_set_id IS NOT NULL AND permission_sets.role_type = #{::PermissionSet::Type::CUSTOM}) \
          ) \
        )").
      # For performance, outer join + select instead of include
        joins("LEFT OUTER JOIN permission_sets ON permission_sets.id = users.permission_set_id").
        order("users.name ASC")
    }

    # Note: CP3 only
    scope :chat_enabled_support_agents, ->(*_args) {
      base.agents.
        where("permission_sets.id IS NULL OR permission_sets.role_type != #{::PermissionSet::Type::CHAT_AGENT}").
        joins(:zopim_identity).
        joins('LEFT JOIN permission_sets ON users.permission_set_id = permission_sets.id')
    }

    # Note: CP3 only
    scope :chat_only_agents, ->(*_args) {
      base.agents.where(permission_sets: { role_type: ::PermissionSet::Type::CHAT_AGENT }).
        joins(:permission_set)
    }

    # Note: CP3 only
    scope :non_chat_only_non_light_agents, ->(*_args) {
      base.agents.
        where("permission_sets.id IS NULL OR permission_sets.role_type NOT IN (?)", [::PermissionSet::Type::CHAT_AGENT, ::PermissionSet::Type::LIGHT_AGENT]).
        joins('LEFT JOIN permission_sets ON users.permission_set_id = permission_sets.id')
    }

    # Note: CP3 only
    scope :support_only_agents, ->(*_args) {
      base.agents.
        where('zopim_agents.id IS NULL').
        joins('LEFT JOIN zopim_agents ON users.id = zopim_agents.user_id')
    }

    # NOTE: Overriding the `select` statement which impacts merged scopes
    scope :assignable_agents, ->(*_args) {
      base.where("roles = #{Role::ADMIN.id} OR (roles = #{Role::AGENT.id} AND \
          ( \
            users.permission_set_id IS NULL OR \
            (users.permission_set_id IS NOT NULL AND psp.id IS NULL AND permission_sets.role_type = #{::PermissionSet::Type::CUSTOM}) \
          ) \
        )").
      # For performance, outer join + select instead of include
        joins("LEFT OUTER JOIN permission_sets ON permission_sets.id = users.permission_set_id \
        LEFT OUTER JOIN permission_set_permissions psp ON psp.permission_set_id = permission_sets.id AND psp.name = 'ticket_editing' AND psp.value = '0'").
        select("users.*").
        order("users.name ASC")
    }

    scope :requester_agents, ->(*_args) { base.agents }
    scope :voice_agents, ->(*_args) { base.assignable_agents }

    scope :scrubbed_ticket_anonymous_users, -> (*_args) {
      base.end_users.
        joins(:settings).
        where(user_settings: { name: 'scrubbed_ticket_anonymous_user', value: '1' })
    }

    scope :answer_bot_users, -> (*_args) {
      base.end_users.
        joins(:settings).
        where(user_settings: { name: 'answer_bot', value: '1' })
    }

    scope :with_name_like, -> (name) { base.where('users.name LIKE ?', "#{sanitize_sql_like(name)}%") }

    validate :valid_billable_agent, if: :is_agent?
    validate :valid_group_membership, unless: :is_agent?
    validate :valid_owner_role
    validate :demote_agent_to_light_agent
    validates_inclusion_of :roles, in: [Role::END_USER, Role::AGENT, Role::ADMIN].map(&:id)
    validate               :cannot_remove_last_agent_or_admin, if: :roles_changed?, on: :update

    before_save :coerce_restriction_type_from_permission_set, if: :has_permission_set?
    before_save :set_user_restrictions

    after_update :ensure_current_user_for_non_agents, if: :roles_changed?
    after_update :delete_private_rules,               if: :downgrading_agent_to_end_user?
    after_update :delete_voice_seat,                  if: :downgrading_to_agent_without_voice?
    after_update :delete_voice_forwarding_number,     if: :downgrading_to_agent_without_voice?
    before_update :remove_revere_subscription,        if: :downgrading_agent_to_end_user?
    after_commit :unassign_working_tickets,           if: :downgraded_agent_to_end_user?
  end

  module ClassMethods
    def assignable(account)
      account.assignable_agents.active
    end

    def requesters(account)
      account.requester_agents.active
    end
  end

  def role_can_be_changed?
    !is_account_owner? && !identities.email.empty?
  end

  def role_name
    return I18n.t('txt.users.role_name.owner') if account.owner == self
    return I18n.t('txt.users.role_name.chat_agent') if is_chat_agent?
    return I18n.t('txt.users.role_name.admin') if is_admin?
    if account.subscription.plan_type != SubscriptionPlanType.ExtraLarge
      if is_agent?
        return agent_restriction?(:none) ? I18n.t('txt.users.role_name.agent') : I18n.t('txt.users.role_name.restricted_agent')
      end
    else
      return I18n.t('txt.users.role_name.light_agent') if is_light_agent?
      return I18n.t('txt.users.role_name.agent') if is_agent?
    end
    return I18n.t('txt.users.role_name.end_user') if is_end_user?
    I18n.t('txt.users.role_name.anonymous')
  end

  # Move to Zendesk::Users::Roles
  def role_name_short(care_about_restricted = true)
    return I18n.t('txt.admin.models.user.roles.role_name_short_owner_label') if account.owner_id == id
    return I18n.t('txt.admin.models.user.roles.role_name_short_chat_agent_label') if is_chat_agent?
    return I18n.t('txt.admin.models.user.roles.role_name_short_admin_label') if is_admin?
    return I18n.t('txt.admin.models.user.roles.role_name_short_contributor_label') if is_contributor?
    if account.subscription.plan_type != SubscriptionPlanType.ExtraLarge
      if is_agent?
        return (!care_about_restricted || agent_restriction?(:none)) ? I18n.t('txt.admin.models.user.roles.role_name_short_agent_label') : I18n.t('txt.admin.models.user.roles.role_name_short_restr_agent_label')
      end
    else
      return I18n.t('txt.admin.models.user.roles.role_name_short_light_agent_label') if is_light_agent?
      return I18n.t('txt.admin.models.user.roles.role_name_short_agent_label') if is_agent?
    end
    I18n.t('txt.admin.models.user.roles.role_name_short_user_label')
  end

  def is_zendesk_agent? # rubocop:disable Naming/PredicateName
    is_agent? && account.subdomain == 'support'
  end

  def is_zendesk_admin? # rubocop:disable Naming/PredicateName
    is_admin? && is_zendesk_agent?
  end

  def is_account_owner? # rubocop:disable Naming/PredicateName
    id == account.owner_id
  end

  def is_admin? # rubocop:disable Naming/PredicateName
    roles == Role::ADMIN.id
  end

  def was_admin?
    roles_was == Role::ADMIN.id
  end

  def is_agent? # rubocop:disable Naming/PredicateName
    self.class::AGENT_ROLES.member?(roles)
  end

  def was_agent?
    self.class::AGENT_ROLES.member?(roles_was)
  end

  def is_light_agent? # rubocop:disable Naming/PredicateName
    has_permission_set? && account.has_light_agents? &&
      permission_set.try(:is_light_agent?)
  end

  def is_chat_agent? # rubocop:disable Naming/PredicateName
    has_permission_set? && account.has_chat_permission_set? &&
      permission_set.try(:is_chat_agent?)
  end

  def is_contributor? # rubocop:disable Naming/PredicateName
    permission_set.try(:is_contributor?)
  end

  def is_billing_admin? # rubocop:disable Naming/PredicateName
    permission_set.try(:is_billing_admin?)
  end

  def is_voice_agent? # rubocop:disable Naming/PredicateName
    return false unless is_agent?
    User.voice_agents.find_by_id(id).present?
  end

  def is_non_restricted_agent? # rubocop:disable Naming/PredicateName
    is_admin? || (is_agent? && agent_restriction?(:none))
  end

  def is_restricted_agent? # rubocop:disable Naming/PredicateName
    !is_non_restricted_agent?
  end

  def is_anonymous_user? # rubocop:disable Naming/PredicateName
    !id
  end

  def is_system_user? # rubocop:disable Naming/PredicateName
    id == User.system_user_id
  end

  def can_manage_macros?
    can?(:manage_personal, Macro) || can?(:manage_group, Macro) || can?(:manage_shared, Macro)
  end

  def can_manage_people?
    is_agent? && can?(:edit, Organization) && can?(:edit, Group)
  end

  def global_admin_privileges?
    is_account_owner? || is_admin? || staff_client.admin?(id)
  end

  # In an account which has been upgraded to use permission_sets, legacy agents
  # are agents assigned to a role (permission_set) that was automatically generated for them
  # during the upgrade process (known as legacy roles).
  # After an agent has been reasigned to a user-created role (permission_set)
  # or to admin or end-user, they are no longer a legacy agent.
  def is_legacy_agent? # rubocop:disable Naming/PredicateName
    is_agent? && !is_admin? && account.has_permission_sets? && permission_set.nil?
  end

  def agent_restriction?(value)
    return nil unless is_agent?
    if has_permission_set?
      permission_set.has_ticket_restriction?(value)
    else
      restriction_id == RoleRestrictionType.fuzzy_find(value)
    end
  end

  def ticket_access
    if agent_restriction?(:none)
      "all"
    elsif agent_restriction?(:assigned)
      "assigned-only"
    elsif agent_restriction?(:groups)
      "within-groups"
    elsif agent_restriction?(:organization)
      "within-organization"
    end
  end

  def end_user_restriction?(value)
    return nil unless is_end_user?
    restriction_id == RoleRestrictionType.fuzzy_find(value)
  end

  def has_permission_set? # rubocop:disable Naming/PredicateName
    return false unless is_agent? && (!is_admin? || Arturo.feature_enabled_for?(:permissions_allow_admin_permission_sets, account)) && permission_set.present?
    Zendesk::Accounts::CustomRolesResolver.new(account).enabled_permission_sets?
  end

  def permission_name
    return role_name unless is_agent? && has_permission_set?
    permission_set.name
  end

  def role_or_permission_set
    return 'admin' if is_admin?
    return 'end_user' if is_end_user?
    return permission_set.id unless permission_set.nil?
    'agent'
  end

  def unbillable_agent?
    roles == Role::AGENT.id && permission_set && permission_set.role_type != ::PermissionSet::Type::CUSTOM
  end

  def was_unbillable_agent?
    old_permission_set = permission_set_was
    roles_was == Role::AGENT.id && old_permission_set && old_permission_set.role_type != ::PermissionSet::Type::CUSTOM
  end

  def role_or_permission_set=(value)
    case value.to_s
    when /\d+/             # enterprise only
      ps = account.permission_sets.find_by_id value
      if ps
        self.roles = Role::AGENT.id
        self.permission_set = ps
      end
    when 'admin'           # enterprise & non-enterprise
      self.roles = Role::ADMIN.id
    when 'end_user'        # enterprise & non-enterprise
      self.roles = Role::END_USER.id
    when 'agent'           # non-enterprise only
      self.roles = Role::AGENT.id
    end
  end

  def is_audited_by_cia? # rubocop:disable Naming/PredicateName
    is_active? && (
      is_account_owner? ||
      is_admin? || was_admin? ||
      is_agent? || was_agent? ||
      is_light_agent?
    )
  end

  #############

  def permissions(hash = true)
    p = []

    p << "anon-#{is_anonymous_user? ? 1 : 0}"
    p << "user-#{is_end_user? ? 1 : 0}"
    p << "orgn-#{organization_id.to_i}"
    p << "agnt-#{is_agent? ? 1 : 0}"
    p << "rest-#{restriction_id.to_i}"
    p << "tick-#{ticket_access}"
    p << "admn-#{is_admin? ? 1 : 0}"
    p << "modr-#{can?(:moderate, Entry) ? 1 : 0}"
    p << "tags-#{is_end_user? ? all_tags.join('-') : ''}" # Only end-users are restricted per tags
    p << "role-#{permission_set.to_md5}" if has_permission_set?

    hash ? Digest::MD5.hexdigest(p.join("/")) : p.join("/")
  end

  def set_user_restrictions
    if is_end_user?
      unless [RoleRestrictionType.REQUESTED, RoleRestrictionType.ORGANIZATION].member?(restriction_id)
        self.restriction_id = RoleRestrictionType.REQUESTED
      end
      self.is_moderator = false
    elsif is_agent? && !is_admin?
      unless [RoleRestrictionType.ASSIGNED, RoleRestrictionType.ORGANIZATION, RoleRestrictionType.GROUPS, RoleRestrictionType.NONE].member?(restriction_id)
        self.restriction_id = RoleRestrictionType.NONE
      end
      self.is_moderator = false if is_contributor?
    else
      self.restriction_id = RoleRestrictionType.NONE
      self.is_moderator = true
    end
    true
  end

  def coerce_restriction_type_from_permission_set
    RoleRestrictionType.list.detect do |name, restriction_id|
      if permission_set.has_ticket_restriction?(name)
        self.restriction_id = restriction_id
      end
    end
  end

  protected

  def ensure_current_user_for_non_agents
    if !is_agent? && current_user.nil?
      logger.warn('Missing current_user while changing user role, defaulting to self')
      self.current_user = self
    end
  end

  def delete_private_rules
    # Soft delete private views
    View.soft_delete_all!(views) if views.any?
    # Soft delete private macros
    Macro.soft_delete_all!(macros) if macros.any?
  end

  def delete_voice_seat
    if account.has_voice_delete_number_on_downgrade?
      user_seats.voice.destroy_all
    else
      user_seats.voice.delete_all
    end
  end

  def delete_voice_forwarding_number
    remove_voice_number
  end

  def remove_revere_subscription
    # Burn-Notice. Take away this ex-agent's Revere notification subscription
    if settings.revere_subscription
      settings.revere_subscription = false
      RevereSubscriberUpdateJob.enqueue(account.id, id)
    end
    true
  end

  def downgrading_agent_to_end_user?
    roles_changed? && !is_agent? && is_active?
  end

  def downgraded_agent_to_end_user?
    previous_changes["roles"] && !is_agent? && is_active?
  end

  def downgrading_to_agent_without_voice?
    !can?(:accept, Voice::Call)
  end

  def unassign_working_tickets
    if account.agents.reload.count(:all) == 1
      # Assign released tickets to the only agent in the help desk
      TicketBulkUpdateJob.enqueue(
        account_id: account.id,
        user_id: current_user.id,
        ticket_ids: assigned.not_closed.map(&:nice_id),
        ticket: { assignee_id: account.agents.first.id },
        via_id: ViaType.USER_CHANGE
      )
    else
      # Unassign working tickets assigned to this ex-agent
      UnassignTicketsJob.enqueue(
        account.id,
        nil,
        id,
        current_user.id,
        ViaType.USER_CHANGE
      )
    end
  end

  def cannot_remove_last_agent_or_admin
    if account.admins.size == 1 && account.admins.first == self && !is_admin?
      errors.add(:base, I18n.t('txt.admin.models.user.roles.you_cannot_remove_the_admin_role_from_the_last_Admin'))
      self.roles = Role::ADMIN.id
    end

    if account.agents.size == 1 && account.agents.first == self && is_end_user?
      errors.add(:base, 'You cannot remove the agent role from the last agent')
    end
  end

  def valid_billable_agent
    return true unless is_active?
    unless unbillable_agent?
      if account.subscription.present? && !account.billable_agents.include?(self)
        if account.multiproduct && account.has_active_suite?
          valid_suite_agent
        else
          valid_nonsuite_agent
        end
      end
    end
  end

  def valid_group_membership
    return true unless is_active?
    unless memberships.empty?
      errors.add(:base, 'User cannot be member of a group without being an agent')
    end
  end

  def valid_owner_role
    if id && account.owner_id == id
      # if support is active then they must be an admin
      # or if support is not active they must not be an end user
      if (account.subscription.present? && !is_admin?) || (account.subscription.blank? && is_end_user?)
        Rails.logger.info("Account #{account.idsub} owner #{id} must have an admin role")
        errors.add(:base, I18n.t('txt.admin.models.user.roles.invalid_role_combination_for_account_owner'))
      end
    end
  end

  def admin_center_user_roles_link
    link_url = "/admin/staff/#{id}#product-roles"
    "<a href=\"#{link_url}\" target=\"_blank\">#{::I18n.t('txt.admin.models.user.roles.profile_in_admin_center')}</a>"
  end

  def demote_agent_to_light_agent
    if !new_record? && is_light_agent? && assigned.not_closed.exists?
      errors.add(:base, I18n.t('txt.admin.models.user.roles.reassign_tickets_light_agents_error_message'))
    end
  end

  def agent_becoming_admin?
    roles_change == [Role::AGENT.id, Role::ADMIN.id]
  end

  private

  def valid_suite_agent
    return true if cp4_chat_agent? || reverse_sync
    return true if account.has_ocp_synchronous_entitlements_update? && account.has_central_admin_staff_mgmt_roles_tab?

    seats_remaining = suite_seats_remaining
    if seats_remaining.present?
      add_max_agents_exceeded_error! unless seats_remaining > 0
    else
      add_staff_service_unavailable_error!
    end
  end

  def suite_seats_remaining
    staff_client.seats_remaining_support_or_suite!
  rescue *Zendesk::StaffClient::RETRYABLE_ERRORS
    nil
  end

  def staff_client
    @staff_client ||= Zendesk::StaffClient.new(account)
  end

  def valid_nonsuite_agent
    if account.subscription.max_agents <= account.billable_agents.count
      if account.subscription.is_trial?
        account.subscription.base_agents = account.agents.count(:all) + 1
        Zendesk::SupportAccounts::Product.retrieve(account).save(validate: false)
      else
        add_max_agents_exceeded_error!
      end
    end
  end

  def add_max_agents_exceeded_error!
    errors.add(
      :base,
      Api::Presentation::Error.new(
        'txt.error_message.models.roles.your_account_doesnt_allow_more_than',
        max_agents: account.subscription.max_agents,
        error: MAX_AGENT_EXCEEDED
      )
    )
  end

  def add_staff_service_unavailable_error!
    errors.add(
      :base,
      Api::Presentation::Error.new(
        'txt.error_message.models.roles.staff_service_unavailable',
        error: 'InvalidValue'
      )
    )
  end

  def permission_set_was
    return permission_set unless permission_set_id_changed?
    return unless permission_set_id_was

    PermissionSet.where(id: permission_set_id_was).first
  end
end
