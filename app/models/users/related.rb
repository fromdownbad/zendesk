module Users
  class Related
    ## DEPRECATED: Has been replaced by UserRelatedPresenter.
    ##             Should be removed with migration to V2.

    def initialize(user)
      @user = user
    end

    def as_json
      json = {}

      json[:requested_tickets] = @user.tickets.count_with_archived
      json[:ccd_tickets] = @user.collaborated_tickets.count_with_archived
      json[:topics] = @user.entries.count(:all)
      json[:topic_comments] = @user.posts.count(:all)
      json[:votes] = @user.votes.count(:all)
      json[:subscriptions] = @user.watchings.count(:all)

      json
    end
  end
end
