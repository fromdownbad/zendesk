User.class_eval do
  def can_be_merged_as_loser?
    Users::Merge.valid_loser?(self)
  end

  def can_be_merged_as_winner?
    Users::Merge.valid_winner?(self)
  end

  def can_be_merged_into?(user)
    user.can_be_merged_as_winner? && can_be_merged_as_loser?
  end
end

module Users
  class Merge < ActiveRecord::Base
    self.table_name = 'user_merges'

    DEFAULT_BATCH = 1000

    belongs_to :account
    belongs_to :winner, class_name: "User"
    belongs_to :loser,  class_name: "User"

    validates_presence_of :account_id, :winner, :loser

    before_validation :set_account_from_users

    attr_accessible :winner, :loser
    after_save :flatten_all_previous_merges

    # One of the main entry points for user merge, specifically for the
    # 'invisible' merges that happens when a User is found to own identities
    # that exist on two separate User records, and we want to bring all
    # their data together.
    def self.merge_with_existing_identity(current_user, identity)
      return unless existing_identity = existing_identity(current_user, identity)
      return if user_locked?(current_user) || user_locked?(identity)
      UserMergeJob.enqueue(current_user.account_id, current_user.id, existing_identity.user_id)
      lock_users(current_user, identity)
    end

    def self.existing_identity(current_user, identity)
      identity.class.
        where(account_id: identity.account_id).
        where(value: identity.value).
        where('user_id <> ?', current_user.id).
        first
    end

    # Just the straight up merge, but does it later if it should
    def self.validate_and_merge(winner, loser)
      merge_state = validate_merge(winner, loser)
      return merge_state unless merge_state == :merge_valid
      return false if user_locked?(winner) || user_locked?(loser)
      UserMergeJob.enqueue(winner.account_id, winner.id, loser.id)
      lock_users(winner, loser)
      :merge_complete
    end

    def self.validate_and_merge_with_tickets_now(winner, loser)
      merge_state = validate_merge(winner, loser)
      return merge_state unless merge_state == :merge_valid
      User.connection.transaction do
        new(winner: winner, loser: loser).send(:move_tickets)
      end
      validate_and_merge(winner, loser)
    end

    def self.valid_loser?(user)
      validate_loser(user) == :loser_valid
    end

    def self.validate_loser(user)
      return :loser_invalid if !user ||
                               user.is_anonymous_user? ||
                               !user.is_active? ||
                               user.foreign_agent?
      return :loser_has_sso_external_id unless remote_auth_loser_with_ext_id?(user)
      return :loser_non_enduser unless user.is_end_user?
      return :loser_email_blacklisted if blacklisted_email?(user)
      :loser_valid
    end

    def self.remote_auth_loser_with_ext_id?(user)
      (!user.login_allowed?(:remote) || !user.external_id.present?)
    end

    def self.blacklisted_email?(user)
      user.identities.email.any? do |identity|
        UserEmailIdentity::EMAIL_BLACKLIST.include?(identity.value)
      end
    end

    def self.valid_winner?(user)
      validate_winner(user) == :winner_valid
    end

    def self.validate_winner(user)
      return :winner_invalid if !user ||
                                user.is_anonymous_user? ||
                                !user.is_active?
      return :winner_non_enduser unless user.is_end_user?
      return :winner_email_blacklisted if blacklisted_email?(user)
      :winner_valid
    end

    def self.valid_merge?(winner, loser)
      validate_merge(winner, loser) == :merge_valid
    end

    def self.validate_merge(winner, loser)
      return :winner_is_loser    if winner == loser
      return :account_mismatch   if winner.account_id != loser.account_id

      winner_status = validate_winner(winner)
      return winner_status unless winner_status == :winner_valid

      loser_status = validate_loser(loser)
      return loser_status unless loser_status == :loser_valid

      :merge_valid
    end

    def self.redis_client
      @client ||= Zendesk::RedisStore.client
    end

    def self.user_key(user)
      "user_merge_job_#{user.account_id}/#{user.id}"
    end

    def self.lock_users(*users)
      users.each do |u|
        u = User.find_by_id(u) unless u.is_a?(User)
        next unless u
        redis_client.set(user_key(u), '1', nx: true, ex: 10.minutes)
      end
    end

    def self.unlock_users(*users)
      users.each do |u|
        u = User.find_by_id(u) unless u.is_a?(User)
        next unless u
        redis_client.del(user_key(u))
      end
    end

    def self.user_locked?(user)
      redis_client.exists(user_key(user))
    end

    def merge!
      return false unless self.class.valid_merge?(winner, loser)

      CIA.audit(actor: winner, effective_actor: User.system) do
        User.connection.transaction do
          # TODO: resend verification emails for unverified email identities that were moved
          remove_zopim_agent_records
          move_phone_number
          move_identities
          move_collaborations
          move_tickets
          move_archived_tickets
          move_watchings
          move_entries
          move_votes
          move_posts
          move_attachments
          move_organizations
          move_suspended_tickets
          deactivate_user(loser)

          self.account = winner.account
          add_domain_event
          save!
        end
      end
    end

    private

    def remove_zopim_agent_records
      loser.remove_zopim_agent_record!
      winner.remove_zopim_agent_record!
    end

    def winner_has_phone_identity?
      winner.identities.phone.exists?
    end

    def move_identities
      # Get the highest priority (or 0) of the winner's priorities.
      # We have to make these unique relative to the user_id, so the
      # loser's priorities will all be greater than the winners.
      priority = winner.identities(:reload).maximum(:priority) || 0
      loser.identities(:reload).each do |identity|
        priority += 1
        Rails.logger.warn("Migrating identity #{identity.inspect} to #{winner.id} with priority #{priority}")
        identity.user     = winner
        identity.priority = priority
        identity.save!
      end
      loser.unverified_email_addresses(:reload).each do |identity|
        Rails.logger.warn("Migrating unverified identity #{identity.inspect} to #{winner.id}")
        identity.user = winner
        identity.save!
      end
    end

    def move_phone_number
      # if multiple phone numbers are enabled for this account this method should only
      # move the 'shared' phone number on the user account using the rule
      # 1. winner > loser
      # otherwise
      # set new phone number based on two rules applied in order:
      # 1. identity > phone > nothing
      # 2. winner > loser
      new_phone_number = if winner.has_direct_number?(winner.phone)
        loser.phone
      else
        winner.phone || loser.phone
      end

      winner.update_attribute(:phone, new_phone_number)
    end

    def move_collaborations
      # make the winner collaborate on everything the loser did, except
      #  a) when the ticket has been deleted
      #  b) when the ticket was requested by the winner
      #  c) when the ticket has winner already as collaborator

      loser.collaborations.each do |collaboration|
        next if collaboration.ticket.blank? || collaboration.ticket.deleted?

        if collaboration.ticket.requested_by?(winner) ||
             collaboration.ticket.requested_by?(loser) # technically, this can't happen
          collaboration.delete
        else
          collaboration.user = winner
          collaboration.save!
        end
      end

      winner.collaborations.each do |collaboration|
        next if collaboration.ticket.blank? || collaboration.ticket.deleted?

        if collaboration.ticket.requested_by?(loser)
          collaboration.delete
        end
      end
    end

    def move_tickets
      if winner.account.has_user_merge_job_audit?
        tas_ids = TicketArchiveStub.where(requester_id: loser.id, account_id: loser.account_id).map(&:id)

        Ticket.where(requester_id: loser.id, account_id: loser.account_id).find_each do |ticket|
          next if tas_ids.include? ticket.id

          ticket.will_be_saved_by(User.system, via_id: ViaType.USER_MERGE)
          change = Change.new(
            ticket: ticket,
            account_id: loser.account_id,
            via_id: ViaType.USER_MERGE,
            value: winner.id,
            value_previous: loser.id,
            value_reference: "requester_id",
            is_public: false
          )
          ticket.create_silent_change_event(change)
        end
      end

      if winner.account.has_user_merge_move_tickets_in_batches?
        Ticket.where(requester_id: loser.id, account_id: loser.account_id).find_in_batches(batch_size: DEFAULT_BATCH) do |tickets|
          tickets.each do |ticket|
            ticket.update_column(:requester_id, winner.id)
            ticket.update_column(:submitter_id, winner.id)
          end
          sleep(1)
        end
      else
        Ticket.where(requester_id: loser.id, account_id: loser.account_id).update_all(requester_id: winner.id)
        Ticket.where(submitter_id: loser.id, requester_id: winner.id, account_id: loser.account_id).update_all(submitter_id: winner.id)
      end
    end

    def move_archived_tickets
      ticket_archive_stubs = TicketArchiveStub.where(requester_id: loser.id, account_id: loser.account_id)

      ticket_archive_stubs.each do |ticket_archive_stub|
        Rails.logger.info("User merge: updating old requester: #{loser.id} with new user: #{winner.id} for the archived ticket: #{ticket_archive_stub.id.inspect}")
        ticket = ZendeskArchive.router.get(ticket_archive_stub).data
        ticket["requester_id"] = winner.id
        ZendeskArchive.router.set ticket_archive_stub, ticket
      end

      # Now update the TicketArchiveStub itself
      Rails.logger.info("User merge: updating all enteries of old requester: #{loser.id} with new user: #{winner.id} for TicketArchiveStub")
      TicketArchiveStub.where(requester_id: loser.id, account_id: loser.account_id).update_all(requester_id: winner.id)
    end

    def move_watchings
      winner_sources = winner.watchings.map(&:source)
      loser.watchings.each do |watching|
        if winner_sources.include?(watching.source)
          watching.delete
        else
          watching.user = winner
          watching.save!
        end
      end
    end

    def move_entries
      loser.entries.each do |entry|
        entry.submitter = winner
        entry.save!
      end
    end

    def move_votes
      loser.votes(true).each do |vote|
        if vote.entry.votes.find_by_user_id(winner.id)
          vote.delete
        else
          vote.user = winner
          vote.save!
        end
      end
    end

    def move_posts
      Post.where(user_id: loser.id).update_all(user_id: winner.id)
    end

    def move_attachments
      Attachment.where(author_id: loser.id).update_all(author_id: winner.id)
    end

    def move_organizations
      return if winner.organization && !winner.account.has_multiple_organizations_enabled?
      winner.organizations += loser.organizations
      winner.save!
    end

    def move_suspended_tickets
      SuspendedTicket.where(author_id: loser.id).update_all(author_id: winner.id)
    end

    def deactivate_user(user)
      # deactivating a user deletes things like identities, which we've
      # moved, so want to reload the user, to make sure it only references
      # relations that it sill owns.
      user.reload
      user.current_user = user
      user.delete!
    end

    def set_account_from_users
      self.account ||= winner.try(:account) || loser.try(:account)
    end

    def flatten_all_previous_merges
      Users::Merge.where(winner_id: loser.id, account_id: account.id).each do |merge|
        merge.original_winner_id ||= merge.winner_id
        merge.winner_id = winner.id
        merge.save
      end
    end

    def add_domain_event
      loser.add_domain_event(UserMergedProtobufEncoder.new(winner.id).to_safe_object)
      loser.publish_user_events_to_bus!
    end
  end
end
