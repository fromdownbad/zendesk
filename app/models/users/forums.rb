module Users
  module Forums
    def self.included(base)
      base.class_eval do
        has_many :entries, -> { order(id: :desc) }, foreign_key: :submitter_id, inherit: :account_id # TODO: dependent => :destroy
        has_many :posts, -> { order(id: :desc) }
        has_many :watchings, -> { order(id: :desc) }, dependent: :destroy
        has_many :votes
        has_many :voted_entries, -> { order(id: :desc) }, through: :votes, source: :entry, inherit: :account_id
      end
    end
  end
end
