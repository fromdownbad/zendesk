require 'role'

module Users
  module RoleMapping
    ROLE_MAPPING = {
      1 => Role::END_USER.id,
      2 => Role::AGENT.id,
      3 => Role::ADMIN.id
    }.freeze

    def self.included(base)
      base.validate :validate_custom_role_id, unless: :deleted?
    end

    def validate_custom_role_id
      if permission_set_id.present? && !account.permission_sets.exists?(permission_set_id)
        errors.add(:roles, :invalid)
      end
    end

    def role_id=(role_id)
      self.roles = ROLE_MAPPING[role_id]
    end

    def custom_role_id=(custom_role_id)
      self.permission_set_id = custom_role_id
    end

    def custom_role_id
      permission_set_id
    end

    def role
      _role.name
    end

    def translated_role
      _role.display_name
    end

    def role=(arg)
      if r = Role.find_by_name(arg.to_s.capitalize)
        self.roles = r.id
      end
    end

    private

    def _role
      Role.find_by_id(roles)
    end
  end
end
