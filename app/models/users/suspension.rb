module Users
  module Suspension # mixed into User
    def self.included(base)
      base.property_set(:settings, inherit: :account_id) do
        property :suspended, default: '0'
        property :whitelisted_from_moderation, type: :boolean, default: true
      end

      base.validate :cannot_suspend_owner
    end

    delegate :suspended?, to: :settings

    def suspend!
      self.suspended = true
      save!
    end

    def suspended=(value)
      if suspended? != value
        settings.suspended = value
      end
    end

    def suspend
      self.suspended = true
      save
    end

    def whitelisted_from_moderation=(value)
      self.suspended = false if value
      settings.whitelisted_from_moderation = value
    end

    def whitelisted_from_moderation?
      is_agent? || settings.whitelisted_from_moderation?
    end

    def throttled_posting?
      count = SuspendedEntry.for_account(account).submitted_by(id).count(:all) +
        SuspendedPost.for_account(account).authored_by(id).count(:all)

      count >= account.settings.moderation_throttle
    end

    private

    def cannot_suspend_owner
      errors.add(:base, I18n.t('txt.admin.models.group.validate_on_update')) if is_account_owner? && settings.suspended?
    end
  end
end
