require 'users/password_invalid'
require 'users/invalid_state'

module Users
  module Authentication
    def self.included(base)
      base.class_eval do
        has_many :challenge_tokens, as: :source, dependent: :destroy
        has_many :master_tokens, as: :source, dependent: :destroy
        has_many :verification_tokens, -> { order(id: :desc) }, as: :source, dependent: :destroy
        has_many :password_reset_tokens, -> { order(id: :desc) }, as: :source, dependent: :destroy
        has_many :otp_tokens, as: :source, dependent: :destroy

        delegate :security_policy, :login_allowed?, :only_login_service_allowed?,
          to: :security_settings

        before_validation :encrypt_password
        validate :password_security_policy_enforced, if: :password_required?

        property_set(:settings, inherit: :account_id) do
          property :password_security_policy_id, protected: true
        end

        settings_association = reflect_on_association(:settings)
        settings_association.options[:autosave] = true

        kasket_dirty_methods :dirty_update_last_login
      end
    end

    attr_accessor :password, :skip_password_validation

    def update_last_login
      return if is_system_user? || suspended?
      account.update_last_login
      dirty_update_last_login if (last_login || 1.hour.ago) < 20.minutes.ago
    end

    # This is a separate method which is marked dirty by kasket to ensure kasket integrity
    def dirty_update_last_login
      now = Time.now.utc
      previous_last_login = last_login
      User.where(id: id).update_all(last_login: now, updated_at: now)
      add_domain_event(UserLastLoginChangedProtobufEncoder.new(previous_last_login, now).to_object)
      publish_user_events_to_bus!
    end

    # Allow REST update of challenge token
    def challenge=(value)
      challenge_tokens.create!(value: value)
    end

    def find_or_create_challenge_token(ip_address = nil, shared_session = nil)
      create_challenge_token(ip_address: ip_address, shared_session: shared_session)
    end

    def create_challenge_token(ip_address:, shared_session: nil)
      challenge_tokens.create!(ip_address: ip_address, shared_session_key: shared_session.try(:session_key))
    end

    def set_password(new_password) # rubocop:disable Naming/AccessorMethodName
      if new_password.present? && (current_user.can?(:edit_password, self) || current_user.can?(:set_password, self))
        self.password = new_password
      end
    end

    # Change the password for the user and save the record.
    #
    # new_password - the String password that the user should now use to authenticate.
    #
    # Returns nothing.
    # Raises Users::InvalidState if the user has already been modified and not yet saved.
    # Raises Users::PasswordInvalid if the new password does not meet the security policy requirements.
    def change_password!(new_password)
      raise InvalidState if changed?

      self.password = new_password
      save || raise(PasswordInvalid)

      # this prevents BCrypt from encrypting the password again if we were to
      # save this object again
      self.password = nil
    end

    def authenticated?(password)
      if password.present? && crypted_password == password
        if crypted_password.is_a?(Password::SHA1)
          bcrypted_password = Password::BCrypt.create(password)
          self.class.where(id: id).update_all(crypted_password: bcrypted_password.checksum, salt: bcrypted_password.salt)
          clear_kasket_indices
        end

        true
      else
        false
      end
    end

    def crypted_password
      value = super
      if value
        begin
          Password::BCrypt.new(value, salt: salt.to_s)
        rescue Password::BCrypt::InvalidHash
          Password::SHA1.new(value, salt: salt.to_s)
        end
      end
    end

    def password_expired?
      password_expirable? && security_policy.password_expired?
    end

    def password_expiring?
      password_expirable? && security_policy.password_expiring?
    end

    def password_expires_at
      security_policy.password_expires_at if password_expirable?
    end

    def password_expires_at_in_days
      days = ((password_expires_at - Time.new) / 1.day).round
      days <= 0 ? 0 : days
    end

    def randomize_password
      self.skip_password_validation = true
      self.password = security_policy.random_password
    end

    def password_security_policy_id
      settings.lookup_or_default(:password_security_policy_id).value.to_i
    end

    def password_expirable?
      is_verified? && uses_password_for_authentication?
    end

    def password_security_policy_enforced
      security_policy.enforce(self)
    end

    def encrypt_password
      return if password.nil?

      encrypted = Password::BCrypt.create(password)

      if password.blank?
        self.password         = encrypted
        self.crypted_password = nil
        self.salt             = nil
        return
      end

      self.password          = encrypted
      self.salt              = encrypted.salt
      self.crypted_password  = encrypted.checksum
      self.password_security_policy_id = security_policy.policy_level

      true
    end

    def allowed_to_login_with_password?
      attribute = is_agent? ? :agent_password_allowed : :end_user_password_allowed
      login_allowed?(:zendesk) || account.role_settings.send(attribute)
    end

    protected

    def password_security_policy_id=(new_security_policy_id)
      settings.password_security_policy_id = new_security_policy_id
    end

    def password_required?
      !password.nil? && !@skip_password_validation
    end

    def uses_password_for_authentication?
      (crypted_password.present? && allowed_to_login_with_password?) ||
        !can_authenticate_without_password?
    end

    def can_authenticate_without_password?
      login_allowed?(:remote) ||
        has_twitter_identity? ||
        has_google_identity?  ||
        has_facebook_identity?
    end

    private

    def security_settings
      Zendesk::Users::SecuritySettings.new(self, account)
    end
  end
end
