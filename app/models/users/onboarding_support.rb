require 'erb'
require 'ostruct'
module Users
  module OnboardingSupport
    SEGMENTS = %w[b2b b2c internal_it internal_hr internal_other].freeze

    def self.included(base)
      base.class_eval do
        has_many :user_onboarding_tasks,
          -> { where(level: OnboardingTask::LEVEL::USER) },
          class_name: 'OnboardingTask',
          dependent: :destroy,
          inverse_of: :user
      end
    end

    #
    # Get tasks from configurations for this user
    #
    # The result looks like:
    #
    # [
    #   {
    #     "name"        => "tour_ticket",
    #     "conditions"  => { "has_solved_five_tickets" => true },
    #     "level"       => 1
    #   },
    #
    #   {
    #     "name"        => "solve_a_ticket",
    #     "level"       => 0
    #   }
    # ]
    #

    def onboarding(type)
      onboarding_tasks(type: type)
    rescue
      # silently ignore error.
      []
    end

    def completed_onboarding_task_names
      completed_user_onboarding_task_names = user_onboarding_tasks.where(status: OnboardingTask::CompletionStatus::COMPLETED).pluck(:zero_state)
      (completed_user_onboarding_task_names + completed_account_onboarding_task_names).uniq
    end

    def completed_account_onboarding_task_names
      OnboardingTask.where(
        account_id: account.id,
        level: OnboardingTask::LEVEL::ACCOUNT,
        status: OnboardingTask::CompletionStatus::COMPLETED
      ).pluck(:zero_state)
    end

    def has_setup_email_channel # rubocop:disable Naming/PredicateName
      external_email_credentials_active? ||
        recipient_address_active?
    end

    # zendesk_channels_extraction Eventually we should stop calling twitter/facebook models directly in Classic
    def twitter_handles_active?
      MonitoredTwitterHandle.where(account_id: account.id).any? do |handle|
        handle.active? &&
          (handle.direct_messages_enabled_at || handle.mention_autoconversion_enabled_at || handle.favoriting_enabled_at)
      end
    end

    def facebook_pages_active?
      Facebook::Page.where(account_id: account.id).any? do |page|
        page.active? &&
          (page.include_wall_posts || page.include_messages || page.include_posts_by_page)
      end
    end

    def external_email_credentials_active?
      get_channel_instances(:external_email_credentials).any?(&:active?)
    end

    def recipient_address_active?
      get_channel_instances(:recipient_addresses).any? do |address|
        address.forwarding_verified? &&
        !(address.default_host? && address.email.to_s.split("@").first == 'support')
      end
    end

    def voice_sub_account_active?
      get_channel_instances(:voice_sub_account).any?(&:activated?)
    end

    def invited_team?
      account.agents.count > 1
    end

    private

    def onboarding_tasks(type:, sections: ['default'])
      config = onboarding_config(type: type)
      sections.flat_map { |s| config[s] }
    end

    def onboarding_config(type:)
      config_string = onboarding_config_string(type: type)
      if config_string.present?
        parse_yaml_erb(string: config_string, context: { user: self })
      else
        []
      end
    end

    def onboarding_config_string(type:)
      yml_path = Rails.root.join("config/onboarding/#{type}.yml")
      Rails.cache.fetch("onboarding_checklist_#{type}", expires_in: 1.minute) do
        File.read(yml_path)
      end if File.exist?(yml_path)
    end

    def parse_yaml_erb(string:, context:)
      YAML.load(
        ERB.new(string).
          result(OpenStruct.new(context).instance_eval { binding })
      )
    end

    def get_channel_instances(channel)
      instances = if account.respond_to?(channel)
        account.send(channel)
      elsif account.default_brand.respond_to?(channel)
        account.default_brand.send(channel)
      end
      Array.wrap(instances)
    end
  end
end
