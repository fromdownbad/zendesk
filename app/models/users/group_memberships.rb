module Users
  module GroupMemberships
    def self.included(base)
      base.class_eval do
        scope :in_group, ->(group_or_id) {
          group_or_id = group_or_id.id if group_or_id.is_a?(Group)
          base.includes(:memberships).
            where(memberships: {group_id: group_or_id})
        }

        attr_accessor :group_ids_being_removed

        has_many :memberships, inverse_of: :user, dependent: :destroy, inherit: :account_id
        has_many :groups,
          -> { where('groups.is_active = 1').order(name: :asc) },
          through: :memberships,
          # RAILS5UPGRADE: Remove?
          # Rails 2 and Rails 3 :source option is needed
          # to force joins on includes and eager loading
          # May not be needed on Rails 4
          source: :group,
          inherit: :account_id

        validates_associated :memberships

        before_validation :update_memberships, if: :is_agent?
        before_validation :clear_group_memberships

        after_commit      :enqueue_unassign_ticket_jobs

        def groups=(groups_or_ids)
          @groups_or_ids_being_set = groups_or_ids.is_a?(Array) ? groups_or_ids.reject(&:blank?) : groups_or_ids
        end
      end
    end

    def default_group_id
      memberships.active(account_id).detect(&:default?).try(:group_id)
    end

    def default_group_id=(group_id)
      @default_group_id_being_set = group_id.to_i
    end

    def in_group?(group)
      groups.map(&:id).include?(group.is_a?(Group) ? group.id : group.to_i)
    end

    def add_group_memberships(group_ids)
      return if group_ids.empty?

      account.groups.find(group_ids).each do |group|
        memberships.build(account: account, user: self, group: group)
      end
    end

    def remove_group_memberships(group_ids)
      return if group_ids.empty?
      raise "Missing current_user" if current_user.nil? # required in #enqueue_unassign_ticket_jobs

      memberships.delete(
        memberships.select do |membership|
          group_ids.include?(membership.group_id)
        end
      )
    end

    def set_default_group(group_id) # rubocop:disable Naming/AccessorMethodName
      return unless group_id

      if membership = memberships.find_by_group_id(group_id)
        membership.update_attributes!(default: true)
      elsif membership = memberships.detect { |m| m.group_id == group_id }
        membership.default = true
      else
        group = account.groups.find(group_id)

        memberships.build(
          account: account, user: self, group: group, default: true
        )
      end
    end

    def group_ids
      memberships.map(&:group_id)
    end

    def dispatch_event_at_default_group_update(membership)
      return unless default_membership_updated_or_deleted?(membership)
      current_group, previous_group = membership.destroyed? ? [nil, membership.group] : [membership.group, previous_default_group(membership)]
      add_domain_event(UserDefaultGroupChangedProtobufEncoder.new(self, current: current_group, previous: previous_group).to_object)
      publish_user_events_to_bus!
    end

    private

    def previous_default_group(membership)
      memberships.active(account_id).where.not(id: membership.id).detect(&:default?).try(:group)
    end

    def default_membership_updated_or_deleted?(membership)
      membership.default && (membership.destroyed? || membership.default_changed?)
    end

    def update_memberships
      group_membership_update.update
    end

    def clear_group_memberships
      if downgrading_agent_to_end_user?
        memberships.clear
      end

      true
    end

    def enqueue_unassign_ticket_jobs
      return unless group_ids_being_removed

      group_ids_being_removed.each do |group_id|
        UnassignTicketsJob.enqueue(
          account_id, group_id, id, current_user.id, ViaType.USER_CHANGE
        )
      end
    end

    def group_membership_update
      Zendesk::Users::GroupMembershipUpdate.new(self, @groups_or_ids_being_set, @default_group_id_being_set)
    end
  end
end
