module Users
  module Deletion
    # We need to include the account_id in this query to hit the (`account_id`, `user_id`) index on the
    # `compliance_deletion_statuses` table otherwise we have nested full table scan.
    INACTIVE_BUT_NOT_ULTRA_DELETED_OUTER_JOIN = <<-SQL.freeze
      LEFT OUTER JOIN `compliance_deletion_statuses`
        ON `compliance_deletion_statuses`.`account_id` = `users`.`account_id` AND `compliance_deletion_statuses`.`user_id` = `users`.`id`
    SQL

    def self.included(base)
      base.class_eval do
        scope :active, -> { base.where(is_active: 1) }

        validate :cannot_undelete, on: :update
        validate :cannot_delete_with_unclosed_tickets
        validate :cannot_update_deleted_external_id, on: :update

        before_validation :prepare_user_for_delete, if: :deleting?, on: :update
        after_update :record_destroy_via_cia, if: :being_deleted?
        after_update :remove_collaborations, if: :being_deleted?
        after_update :remove_suspended_tickets, if: :being_deleted?
        after_update :remove_user_seats, if: :being_deleted?
        after_update :delete_personal_macros, if: :being_deleted?
        after_commit :enqueue_unassign_ticket_job, if: :got_deleted?
        after_commit :enqueue_gooddata_destroy_job, if: :got_deleted?
      end
    end

    def delete(options = {})
      raise 'Missing current_user' unless current_user
      self.is_active = false
      prepare_user_for_delete(options)
      yield if block_given?
      save
    end

    def delete!(options = {})
      raise 'Missing current_user' unless current_user
      transaction do
        self.is_active = false
        prepare_user_for_delete(options)
        save!
      end
    end

    def deleted?
      !is_active?
    end

    # Use this method to take most User Personal Identifiable Information out of zendesk.
    #   NOTE: this does not remove a ticket's PII.  That is done on a case by case basis.
    #
    # DO NOT CALL THIS METHOD DIRECTLY!!!
    # DO NOT CALL THIS METHOD DIRECTLY!!!
    # DO NOT CALL THIS METHOD DIRECTLY!!!  Best to call `mark_for_ultra_deletion!` and
    #   let internal gdpr_subscriber services `ultra_delete!` a user.
    #
    # status_arguments requires the following fields:
    # :application => "all", "support", "chat", "talk", "guide"
    # :action      => "request_deletion", "complete", "error"
    # :executer_id => Admin Requesting the user deletion
    #
    # user.ultra_delete!({ application: 'all', action: "request_deletion", executer_id: current_user.id })
    def ultra_delete!(status_arguments = {})
      return false if !account.is_active? && !account.is_serviceable?
      raise ComplianceUserDeletionError, "Can't permanently delete a user that is active" if is_active?
      success = delete(status_arguments) do
        organization_memberships.delete_all
        satisfaction_ratings_received.each(&:scrub_data!)
        satisfaction_ratings_created.each(&:scrub_data!)
        custom_field_values.destroy_all
        self.name = I18n.t("txt.admin.views.settings.people.compliance.user_name")
        photo.destroy if photo
        self.organization_id = nil
      end
      if success
        update_gdpr_status(account, status_arguments, ComplianceDeletionFeedback::COMPLETE)
      else
        # TODO: don't send error message to SNS - it's no longer recorded
        update_gdpr_status(account, status_arguments, 'error')
      end
      success
    rescue
      # TODO: don't send error message to SNS - it's no longer recorded
      update_gdpr_status(account, status_arguments, 'error')
      raise
    end

    def mark_for_ultra_deletion!(executer:)
      raise ComplianceUserDeletionError, "Can't permanently delete a user that is active" if is_active?

      ComplianceDeletionStatus.create_request!(user: self, executer: executer)
    end

    def ultra_deleted?
      deleted? && compliance_deletion_statuses.present?
    end

    protected

    def update_gdpr_status(account, status_arguments, action)
      message = status_arguments.merge(account_id: account.id, account_subdomain: account.subdomain, user_id: id, action: action, application: ComplianceDeletionStatus::CLASSIC, pod: account.pod_id)
      Zendesk::PushNotifications::Gdpr::GdprSnsFeedbackPublisher.new.publish(message: message)
    end

    def record_destroy_via_cia
      old_changes = changed_attributes.dup # do not record changes -> we are trying to fake a delete
      old_active = is_active
      self.is_active = true # enable is_audited_by_cia?

      # @cached_changed_attributes is frozen. This will redefine it rather than update it
      @cached_changed_attributes = {}

      CIA.record(:destroy, self)
    ensure
      self.is_active = old_active
      @cached_changed_attributes = old_changes
    end

    def being_deleted?
      is_active_change == [true, false]
    end

    def got_deleted?
      previous_changes["is_active"] == [true, false]
    end

    def remove_collaborations
      collaborations.find_in_batches(batch_size: 1000) do |ccs|
        Collaboration.delete(ccs.map(&:id))
      end
    end

    def remove_suspended_tickets
      suspended_tickets.clear
    end

    def remove_user_seats
      user_seats.destroy_all
    end

    def enqueue_unassign_ticket_job
      return unless is_agent? || foreign_agent?
      UnassignTicketsJob.enqueue(account_id, nil, id, current_user.id, ViaType.USER_DELETION)
    end

    def enqueue_gooddata_destroy_job
      return unless is_agent?
      Zendesk::Gooddata::UserProvisioning.destroy_for_user(self)
    end

    def cannot_undelete
      if undeleting?
        errors.add(:base, I18n.t('txt.admin.model.user.deletion.cannot_activate_deleted_user'))
      end
    end

    def cannot_update_deleted_external_id
      if !is_active? && external_id
        errors.add(:base, I18n.t('txt.admin.model.user.deletion.cannot_update_deleted_user_external_id'))
      end
    end

    def cannot_delete_with_unclosed_tickets
      return unless deleting?

      not_closed = tickets.not_closed.count(:all)
      if not_closed > 0
        errors.add(:base, I18n.t('txt.admin.model.user.deletion.user_has_open_tickets', count: not_closed))
      end
    end

    def prepare_user_for_delete(options = {})
      randomize_password
      self.external_id = nil

      if options[:remove_content]
        posts.each(&:soft_delete!)
        entries.each(&:soft_delete!)
      end

      shared_sessions.delete_all
      delete_oauth_tokens
      delete_unverified_email_addresses
      delete_watchings
      delete_profiles
      delete_identities
      delete_legacy_phone_number
      delete_verification_tokens
      bookmarks.clear
      delete_zopim_identity
    end

    def delete_oauth_tokens
      tokens.delete_all
    end

    def delete_personal_macros
      return unless account.has_delete_personal_macros_when_delete_agent?
      Macro.soft_delete_all!(macros) if macros.any?
    end

    def delete_watchings
      watchings.each(&:destroy)
    end

    def delete_identities
      notify_channels identities.any_channel
      identities.delete_all
    end

    def delete_legacy_phone_number
      self.phone = nil
    end

    def delete_zopim_identity
      if zopim_identity.present? && !zopim_identity.destroyed?
        unless zopim_identity.destroy
          errors.add(:base, I18n.t('txt.admin.model.user.deletion.zopim_identity_issue'))
        end
      end
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: e.message, fingerprint: 'f10b27d0e35965c116e7a80ecbcfe946591512df')
      raise e
    end

    def delete_unverified_email_addresses
      unverified_email_addresses.delete_all
    end

    def delete_verification_tokens
      verification_tokens.delete_all
    end

    def undeleting?
      is_active_changed? && is_active?
    end

    def deleting?
      is_active_changed? && !is_active?
    end

    def delete_profiles
      Channels::AnyChannelUserProfile.where(user: self).delete_all
    end

    def notify_channels(user_identities)
      return if user_identities.empty?

      Channels::AnyChannel::UserDestroyEventHandler.user_deleted(user_identities)
    end
  end
end
