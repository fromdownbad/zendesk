require 'active_support/concern'

module Users
  module VoiceSupport
    extend ActiveSupport::Concern

    def availability_controls_enabled?
      is_admin? || can?(:view_dashboard, ::Voice::Call)
    end
  end
end
