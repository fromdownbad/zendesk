module Users::ChatAgentSupport
  extend ActiveSupport::Concern

  included do
    before_validation ->(user) { user.permission_set_id = nil },
      if: :was_chat_agent?

    after_save :enable_chat_identity!,
      if: ->(user) { user.is_active? && user.is_chat_agent? }
    after_save :disable_chat_identity!,
      if: ->(user) { user.is_active? && user.is_end_user? && user.zopim_identity.present? }
  end

  def assign_chat_permission_set_id
    make_chat_agent! if is_end_user?
  end

  def make_chat_agent!
    raise ActiveRecord::RecordNotSaved, 'AccountChatAccessNotEnabled' \
      unless account.has_chat_permission_set?
    self.permission_set = account.chat_permission_set
    self.roles          = Role::AGENT.id
    save(validate: false)
  end

  def remove_chat_permission_set_id
    return unless chat_permission_removable?
    self.permission_set = nil
    self.roles          = Role::END_USER.id
    Zendesk::SupportUsers::User.new(self).save!
  end

  def enable_chat_identity!
    if zopim_identity.present?
      return if zopim_identity.is_enabled?
      zopim_identity.enable!
    else
      create_zopim_identity!
    end
  end

  def remove_zopim_agent_record!
    return unless zopim_identity.present?
    zopim_identity.destroy
  end

  private

  def disable_chat_identity!
    zopim_identity.disable!
  end

  def chat_permission_removable?
    is_chat_agent? && account.is_serviceable?
  end

  # Non-enterprise account can still select the custom role "Chat-only Agent"
  # for their users
  def was_chat_agent?
    return false if account.has_permission_sets? || account.chat_permission_set.blank?
    roles_changed? && (permission_set_id_was == account.chat_permission_set.id)
  end
end
