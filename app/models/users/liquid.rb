module Users
  module Liquid
    def to_liquid(format = 'text/plain')
      hash = Zendesk::Liquid::Wrapper.new("name")
      hash.merge!(
        'name'           => safe_name(false),
        'first_name'     => first_name,
        'last_name'      => last_name,
        'email'          => email,
        'phone'          => phone_number,
        'time_zone'      => (id ? time_zone : ''),
        'role'           => role,
        'id'             => id,
        'tags'           => current_tags,
        'external_id'    => external_id,
        'notes'          => notes,
        'details'        => details,
        'signature'      => agent_signature(rich: format == 'text/html'),
        'language'       => lambda { |_x| translation_locale.try(:name) || '' },
        'locale'         => lambda { |_x| translation_locale.try(:locale) || '' },
        'translation_id' => lambda { |_x| translation_locale.try(:id) || '' },
        'organization'   => lambda { |_x| organization },
        'extended_role'  => lambda { |_x| (is_agent? && has_permission_set?) ? permission_set.name : role }
      )

      # merge in custom user fields
      cf_hash = {}
      custom_field_values.to_liquid.each { |k, v| cf_hash[k.to_s] = v } # symbol to string
      hash["custom_fields"] = cf_hash

      hash
    end

    # This should be the source of truth for all dc_cache
    def dc_cache
      @dc_cache ||= Zendesk::DynamicContent::AccountContent.cache(account, translation_locale)
    end
  end
end
