require 'active_support/concern'

module Users::ZopimSupport
  extend ActiveSupport::Concern

  included do
    has_one :zopim_identity,
      dependent:   :destroy,
      foreign_key: :user_id,
      class_name:  '::Zopim::Agent',
      inherit:     :account_id

    before_update :propagate_role_to_zopim,
      if: ->(user) { user.is_active? && user.roles_changed? && user.zopim_agent_id.present? }

    validate :roles, :zopim_owner_requires_admin_role, on: :update

    delegate :zopim_agent_id,
      to: :zopim_identity,
      allow_nil: true
  end

  def zopim_email
    zopim_email_format_for_value(primary_email) if primary_email
  end

  def zopim_email_format_for_value(value)
    suffix = id.to_s
    if Rails.env.development?
      suffix << ('-' + SecureRandom.base64.gsub(/[^A-Za-z]/, '').strip.upcase)
    end

    value.sub(/@/, "+#{suffix}@")
  end

  private

  def zopim_owner_requires_admin_role
    return unless roles_changed?
    return unless zopim_agent_id.present? && zopim_identity.is_owner?
    return if account.zopim_integration.try(:phase_three?)
    return if is_admin?
    errors.add(:roles, I18n.t('txt.admin.models.user.roles.invalid_role_combination_for_zopim_owner'))
    false
  end

  def propagate_role_to_zopim
    zopim_identity.update_attribute(:is_administrator, is_admin?)
  end

  def primary_email
    @primary_email ||= identities.detect { |id| id.is_a?(UserEmailIdentity) && id.primary? }.try(:value)
  end
end
