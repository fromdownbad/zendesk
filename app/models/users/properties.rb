module Users::Properties
  def self.included(base)
    base.class_eval do
      before_create :set_new_settings

      # Generic, non-serializable user settings. Value has a length limit of 255
      property_set(:settings, inherit: :account_id) do
        property :device_notification, type: :boolean, default: false
        property :location,            type: :string
        property :latitude,            type: :float
        property :longitude,           type: :float
        property :via,                 type: :string
        property :machine,             type: :boolean, default: false

        # Lotus settings
        property :show_user_assume_tutorial,              type: :boolean, default: true
        property :show_onboarding_tooltips,               type: :boolean, default: true
        property :show_reporting_video_tutorial,          type: :boolean, default: true
        property :show_welcome_dialog,                    type: :boolean, default: true
        property :show_hc_onboarding_tooltip,             type: :boolean, default: true
        property :show_apps_tray,                         type: :boolean, default: false
        property :apps_last_collapsed_states,             type: :string,  default: '{}'
        property :show_feature_notifications,             type: :boolean, default: true
        property :show_options_move_tooltip,              type: :boolean, default: true
        property :show_filter_options_tooltip,            type: :boolean, default: true
        property :show_user_nav_tooltip,                  type: :boolean, default: true
        property :keyboard_shortcuts_enabled,             type: :boolean, default: true
        property :show_insights_onboarding,               type: :boolean, default: false
        property :customer_list_onboarding_state,         type: :string,  default: UserView::OnboardingStates::INTRO
        property :ticket_action_on_save,                  type: :string
        property :voice_calling_number,                   type: :string
        property :show_color_branding_tooltip,            type: :boolean, default: true
        property :show_new_default_brand_modal,           type: :boolean, default: true
        property :show_new_search_tooltip,                type: :boolean, default: true
        property :show_onboarding_modal,                  type: :boolean, default: true
        property :show_get_started_animation,             type: :boolean, default: true
        property :show_prediction_satisfaction_dashboard, type: :boolean, default: true
        property :show_hc_product_tray_tooltip,           type: :boolean, default: true
        property :show_google_apps_onboarding,            type: :boolean, default: true
        property :show_enable_google_apps_modal,          type: :boolean, default: true
        property :show_public_comment_warning,            type: :boolean, default: true
        property :show_new_comment_filter_tooltip,        type: :boolean, default: true
        property :show_multiple_ticket_forms_tutorial,    type: :boolean, default: true
        property :show_first_comment_private_tooltip,     type: :boolean, default: true
        property :show_answer_bot_dashboard_onboarding,   type: :boolean, default: true
        property :show_answer_bot_trigger_onboarding,     type: :boolean, default: true
        property :show_answer_bot_lotus_onboarding,       type: :boolean, default: true
        property :scrubbed_ticket_anonymous_user,         type: :boolean, default: false

        # For Lotus e-mail verification experiment.
        # String to DateTime conversion - `Time.at(last_lotus_login.to_i / 1000).to_datetime`
        property :last_lotus_login,                       type: :string

        # Revere settings
        property :revere_subscription,                    type: :boolean, default: false

        # Onboarding zero states
        property :show_zero_state_tour_ticket,            type: :boolean, default: true
        property :quick_assist_first_dismissal,           type: :boolean, default: false
        property :show_help_panel_intro_tooltip,          type: :boolean, default: true
        property :show_agent_workspace_onboarding,        type: :boolean, default: true
        property :has_seen_channel_switching_onboarding,  type: :boolean, default: false

        # Auto Translation tooltips
        property :show_manual_start_translation_tooltip,  type: :boolean, default: true
        property :show_manual_stop_translation_tooltip,   type: :boolean, default: true
        property :show_composer_will_translate_tooltip,   type: :boolean, default: true
        property :show_composer_wont_translate_tooltip,   type: :boolean, default: true

        # Product Integration and Migration
        property :migrated_at,                            type: :datetime
        property :migrated_from,                          type: :string
      end

      delegate :via, :via=, :machine?, to: :settings

      # Larger, serializable "settings". Value has a length limit of 2^16 - 1
      property_set(:texts, inherit: :account_id) do
        property :shared_views_order, type: :serialized, default: '[]'
        validates_with SharedViewsOrderValidator, if: ->(s) { s.name.to_sym == :shared_views_order }
      end
    end

    ::UserSetting.class_eval do
      include AuditableProperty
      include PrecreatedAccountConversion
      include CachingObserver

      belongs_to :account
      belongs_to :user, touch: true

      has_kasket_on :user_id

      attr_accessible :name, :value

      auditable_properties :suspended, translations: {
        suspended_enabled:  'txt.admin.views.reports.tabs.audits.property.attribute.suspended',
        suspended_disabled: 'txt.admin.views.reports.tabs.audits.property.attribute.unsuspended'
      }
    end

    ::UserText.class_eval do
      belongs_to :account
      belongs_to :user, touch: true

      has_kasket_on :user_id

      attr_accessible :name, :value
    end
  end

  def set_new_settings
    # If on create the user's account does not currently have FCP then the user will see the tool tip when FCP is on
    settings.show_first_comment_private_tooltip = !account.is_first_comment_private_enabled

    return true if is_end_user?

    settings.show_filter_options_tooltip     = false
    settings.show_options_move_tooltip       = false
    settings.show_user_nav_tooltip           = false
    settings.show_color_branding_tooltip     = false
    settings.show_new_search_tooltip         = false
    settings.show_hc_product_tray_tooltip    = false
    settings.show_new_comment_filter_tooltip = false

    if account.has_polaris?
      # render the correct onboarding experience
      settings.show_agent_workspace_onboarding  = true
      settings.show_zero_state_tour_ticket      = false
    end

    true
  end

  class SharedViewsOrderValidator < ActiveModel::Validator
    def validate(setting)
      desired_views = Array(setting.value) # don't blow up if someone sets this to nil
      max_views     = setting.account.max_views_for_display

      if desired_views.count > max_views
        setting.errors[:base] << I18n.t('txt.user_settings.invalid_shared_views_order.too_many_views', count: max_views)
      end

      allowed_views = setting.user.shared_views(skip_ordering: true).pluck(:id)

      if (desired_views & allowed_views) != desired_views
        setting.errors[:base] << I18n.t('txt.user_settings.invalid_shared_views_order.selected_views')
      end
    end
  end
end
