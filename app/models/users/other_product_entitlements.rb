module Users
  # Cache Metropolis entitlements of other products in Classic
  module OtherProductEntitlements
    PRODUCTS_TO_CACHE = [:chat, :explore, :connect, :voice].freeze

    def self.included(base)
      base.class_eval do
        property_set(:settings, inherit: :account_id) do
          PRODUCTS_TO_CACHE.each do |product|
            property "#{product}_entitlement", protected: true
          end
        end
      end
    end

    def other_products_agent?
      cp4_chat_agent? || connect_agent?
    end

    def cp4_chat_agent?
      !settings.chat_entitlement.blank?
    end

    def connect_agent?
      !settings.connect_entitlement.blank?
    end

    def update_entitlement(product:, new_entitlement:)
      return unless PRODUCTS_TO_CACHE.include?(product.to_sym)

      entitlement = settings.send("#{product}_entitlement")
      if entitlement != new_entitlement
        settings.set("#{product}_entitlement" => new_entitlement)
        if save
          Rails.logger.info("#{product} entitlement changes applied to user #{id}")
        else
          Rails.logger.info("Applying #{product}entitlement changes to user #{id} failed for with errors: #{errors}")
        end
      end
    end
  end
end
