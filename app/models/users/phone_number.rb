module Users
  class PhoneNumber
    def initialize(account, number)
      @account = account
      @number  = number
    end

    def valid?
      if @account.is_end_user_phone_number_validation_enabled?
        strong_validation
      else
        weak_validation
      end
    end

    def unique?
      number = use_normalized_number? ? normalized_number : @number

      @account.user_identities.phone.where(value: number).blank?
    end

    def valid_and_unique?
      valid? && unique?
    end

    private

    def strong_validation
      ::Voice::Core::NumberSupport::E164Number.plausible?(@number.to_s)
    end

    def weak_validation
      if @account.has_apply_prefix_and_normalize_phone_numbers?
        !!(@number =~ ::Voice::Core::NumberSupport::E164Number::E164_REGEX_WITH_EXT)
      else
        true
      end
    end

    def normalized_number
      ::Voice::Core::NumberSupport::E164Number.internal(@number, extension: false)
    end

    def use_normalized_number?
      @account.has_apply_prefix_and_normalize_phone_numbers? || @account.is_end_user_phone_number_validation_enabled?
    end
  end
end
