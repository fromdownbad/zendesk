module Users
  class PhoneNumberValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      return true if record.try(:skip_phone_number_validation)
      if value.blank? || exceptional_number?(value)
        return true
      elsif emergency_number?(value)
        record.errors.add(attribute.to_sym, I18n.t('activerecord.errors.voice.user.phone.no_emergency_numbers', value: value))
      elsif !valid_format?(record, value)
        record.errors.add(attribute.to_sym, Api.error('activerecord.errors.models.user.phone.attributes.value.invalid_e164_format', error: "InvalidFormat", value: value))
      else
        return true
      end
    end

    private

    def valid_format?(record, phone_number)
      if record.account.is_end_user_phone_number_validation_enabled?
        ::Voice::Core::NumberSupport::E164Number.plausible?(phone_number)
      elsif record.class == UserPhoneNumberIdentity
        if record.account.has_apply_prefix_and_normalize_phone_numbers?
          !!(phone_number =~ ::Voice::Core::NumberSupport::E164Number::E164_REGEX)
        else
          true
        end
      else
        true
      end
    end

    def emergency_number?(phone_number)
      UserVoiceForwardingIdentity.new(value: phone_number).emergency_number?
    end

    def exceptional_number?(phone_number)
      phone_number == '+00'
    end
  end
end
