require 'digest/md5'

module Users
  module ChatSupport
    def chat_settings
      { welcome_message: account.chat_welcome_message,
        maximum_requests: account.maximum_chat_requests }
    end
  end
end
