User.class_eval do
  # Skip verify email is exposed via our API. Delegate to send_verify_email instead.
  def skip_verify_email=(skip)
    self.send_verify_email = !skip
  end

  def skip_verify_email
    !send_verify_email
  end
end
