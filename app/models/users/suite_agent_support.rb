module Users::SuiteAgentSupport
  extend ActiveSupport::Concern

  def make_suite_agent!
    return unless legacy_chat_agent?
    disable_chat_only_permission_set_and_remove_role_restriction
    Zendesk::SupportUsers::User.new(self).save!
  end

  private

  # NOTE: Restriction_ids are normally set by the
  #       `#coerce_restriction_type_from_permission_set` before_save callback in
  #       User::Roles, but as this callback is conditional on the presence of a
  #       permission_set, so we need to manually set it to `NONE` here.
  def disable_chat_only_permission_set_and_remove_role_restriction
    self.permission_set = nil
    self.restriction_id = Zendesk::Types::RoleRestrictionType.NONE
  end

  def legacy_chat_agent?
    !!permission_set.try(:is_chat_agent?)
  end
end
