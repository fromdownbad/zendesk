module Users
  # Agent display names: In the Professional and Enterprise versions of Zendesk,
  # agents can create display names that are used in all communications with end-users.
  # When personalized email replies is enabled, the agent's display name overrides their real name.
  module AgentDisplayName
    def self.included(base)
      base.class_eval do
        property_set(:settings, inherit: :account_id) do
          property :agent_display_name, protected: true
        end
      end
    end

    def agent_display_name=(new_agent_display_name)
      old_alias = agent_display_name
      settings.agent_display_name = Users::Naming::Name.new(new_agent_display_name, account).sanitize
      add_domain_event(UserAliasChangedProtobufEncoder.new(self, old_alias, agent_display_name).to_object) unless old_alias == agent_display_name || new_record?
    end

    def agent_display_name
      settings.agent_display_name
    end

    alias :alias= :agent_display_name=
    alias :alias  :agent_display_name

    def safe_name(consumer_is_agent)
      if !prefer_alias_over_name? || consumer_is_agent
        clean_name
      else
        agent_display_name
      end
    end
  end
end
