module Users
  module CRM
    def self.included(base)
      base.class_eval do
        has_one  :salesforce_data
        has_one  :sugar_crm_data
        has_one  :ms_dynamics_data
      end
    end

    def crm_data(create = false)
      account.crm_integration.crm_data(self, create) if account.crm_integration_enabled?
    end

    def needs_crm_sync?
      crm_data.nil? || crm_data.needs_sync?
    end
  end
end
