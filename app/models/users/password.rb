autoload 'BCrypt', 'bcrypt'

module Users
  class Password < String
    attr_accessor :unencrypted, :salt

    def initialize(encrypted, options = {})
      self.salt = options[:salt]
      super(encrypted)
    end

    def self.create(unencrypted, options = {})
      encrypted = encrypt(unencrypted, options)

      password = new(encrypted, options)
      password.unencrypted = unencrypted
      password
    end
  end

  class Password::SHA1 < Password
    def initialize(encrypted, options = {})
      super(encrypted, options)
    end

    def ==(other)
      super || super(self.class.create(other, salt: salt))
    end

    def self.create(unencrypted, options = {})
      options[:salt] ||= generate_salt
      super
    end

    def self.encrypt(unencrypted, options)
      Digest::SHA1.hexdigest("--#{options[:salt]}--#{unencrypted}--")
    end

    def self.generate_salt
      Digest::SHA1.hexdigest(SecureRandom.hex(50))
    end
  end

  class Password::BCrypt < Password
    attr_accessor :checksum

    class InvalidHash < ArgumentError
    end

    def initialize(encrypted, options = {})
      bcrypted = load("#{options[:salt]}#{encrypted}")
      self.checksum = bcrypted.checksum
      options[:salt] ||= bcrypted.salt
      encrypted = bcrypted.to_s
      super
    end

    def ==(other)
      super(::BCrypt::Engine.hash_secret(other, salt))
    end

    def duplicate?(other_encrypted)
      other_hash = load(other_encrypted)
      other_hash == unencrypted
    rescue InvalidHash
      false
    end

    def self.encrypt(unencrypted, options = {})
      options[:cost] ||= cost
      ::BCrypt::Password.create(unencrypted, options).to_s
    end

    def self.cost
      return @cost if @cost

      if Rails.env.test?
        1
      else
        calibrated_cost = ::BCrypt::Engine.calibrate(_milliseconds = 5)
        @cost = [calibrated_cost, 10].max
      end
    end

    protected

    def load(encrypted)
      ::BCrypt::Password.new(encrypted)
    rescue ::BCrypt::Errors::InvalidHash
      raise InvalidHash
    end
  end
end
