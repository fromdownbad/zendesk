module Users
  module Access
    def forums
      @forums ||= account.forums
    end

    # keys are forum ids
    # values are (true/false): if forum tags match user tags
    def forum_id_hash
      @forum_id_hash ||= Hash[forums.collect { |forum| [forum.id, forum.matches_user_tags?(self)] }]
    end

    # this is only taking tags into account, not permissions or anything else like that
    def can_search_entries_in_forum?(forum_id)
      is_agent? || forum_id_hash[forum_id]
    end

    def remove_tag_restricted_forums(forum_ids)
      forum_ids.select { |forum_id| can_search_entries_in_forum?(forum_id) }
    end

    def tag_restricted_forum_ids
      forums.map { |forum| !can_search_entries_in_forum?(forum.id) ? forum.id : nil }.compact
    end

    def can_see_audit_log?
      is_admin? && account.subscription.has_audit_log?
    end

    # Search access control. This version is for the Elasticsearch version of Search.

    def search_access_control_filters(options)
      access_conditions = []
      options[:with] ||= {}
      options[:without] ||= {}

      # This is a web portal search
      if options[:forum_id]
        options[:type] = 'entry'
        # Limit search to the specified forum(s), excluding inaccessible forums.
        # If the user is not allowed to access any of the specified forums, we do nothing here,
        # and let the search proceed against all the forums to which the user has access. For end-users,
        # the options[:without][:forum_id] will get set below in this case.
        allowed_forums = accessible_forums.map(&:id) & Array(options[:forum_id]).map(&:to_i)
        options[:with][:forum_id] = allowed_forums unless allowed_forums.empty?
      end

      # Add a filter to exclude any forums that the user does not have access to (via tag/org restrictions etc.)
      if options[:with][:forum_id].blank?
        inaccessible_forum_ids = (forums - accessible_forums).map(&:id)
        options[:without][:forum_id] = inaccessible_forum_ids unless inaccessible_forum_ids.blank?
      end

      if is_anonymous_user?
        # Anonymous users can only search the forums and help center
        access_conditions << "_type:(entry OR article)"
      elsif is_end_user?
        # End users can search only for entries and articles, as well as requests (i.e. tickets):
        #   - where the user is the requester, or
        #   - were the user is cc'd, or
        #   - where the organization of the ticket is one of the user's organizations.
        org_ids = end_user_viewable_organizations.map(&:id)
        org_condition = org_ids.any? ? " OR organization_id:(#{org_ids.join(' OR ')})" : ""
        access_conditions << "requester_id:#{id} OR cc_id:#{id}#{org_condition} OR _type:(entry OR article)"
      elsif negate_users?(options)
        # Some agents do not have permission to search for users.
        access_conditions << "-_type:user"
      end

      if !is_admin? && is_agent? && !is_user_autocomplete?(options)

        if account.has_agent_as_end_user? && !is_system_user?
          # "Agent as End User" case- do not return tickets for which the user is the submitter
          # and the via_id is HelpCenter. Does not apply to system users
          # Ideally, we'd just skip searching the private comments of those tickets, but
          # that's not supported by Search at this time, so we just won't include those
          # tickets at all.

          access_conditions << "NOT (_type:ticket AND (via_id:#{ViaType.WEB_SERVICE} OR via_id:#{ViaType.WEB_FORM}) AND via_reference_id:#{ViaType.HELPCENTER} AND submitter_id:#{id})"
        end

        if agent_restriction?(:organization)

          # Organization-restricted agent can search for tickets, entries and users
          # in the organizations that the agent belongs to, and tickets they
          # were CC'd on or where they are the requester. Help Center articles can
          # also be searched, but organizations and groups cannot be searched.

          org_ids = organizations.any? ? organizations.map(&:id).join(' OR ') : 0
          access_conditions << "organization_id:(#{org_ids}) OR requester_id:#{id} OR cc_id:#{id} OR _type:article"

        elsif agent_restriction?(:groups)

          # Group-restricted agent can search for tickets in groups that the agent
          # belongs to or tickets they were CC'd on or where they are the requester,
          # and for types other than ticket.

          group_ids = groups.any? ? groups.map(&:id).join(' OR ') : 0
          access_conditions << "group_id:(#{group_ids}) OR requester_id:#{id} OR cc_id:#{id} OR (-_type:ticket)"

        elsif agent_restriction?(:assigned)

          # Assigned-only agent can search for tickets which are assigned to the
          # agent, and for entries, users and articles. They can also search tickets
          # that they were either CC'd on or that they have requested.

          access_conditions << "assignee_id:#{id} OR requester_id:#{id} OR cc_id:#{id} OR _type:(entry OR user OR article)"
        end
      end

      access_conditions
    end

    ##########################
    # Organization access
    ##########################

    def end_user_viewable_organizations
      if can?(:view_all_owned, Organization)
        organizations
      else
        organizations.shared
      end
    end

    ##########################
    # Ticket access
    ##########################

    # TODO
    def limited_access?(ticket)
      can?(:only_create_ticket_comments, ticket)
    end

    # For AR ticket finds
    def ticket_conditions
      return if is_admin?

      if is_agent?
        wrap_in_parentheses(agent_ticket_conditions)
      elsif is_end_user?
        wrap_in_parentheses(end_user_ticket_conditions)
      end
    end

    ##########################
    # User access
    ##########################

    def assignable_agents
      return [] if is_end_user?
      return @assignable_agents unless @assignable_agents.nil?

      group_ids = assignable_groups.map(&:id)
      memberships = Membership.where(account_id: account_id, group_id: group_ids).includes(:user).to_a

      @assignable_agents ||= memberships.map(&:user).uniq.sort_by(&:name)
    end

    def non_light_agent_users(users)
      users.reject do |user|
        user.is_light_agent? ||
          (user.has_permission_set? && !user.permission_set.permissions.ticket_editing?)
      end
    end

    def assignable_groups_and_agents
      assignable_groups.includes(users: { identities: {}, permission_set: :permissions }).map do |group|
        {
          id: group.id,
          name: group.name,
          agents: non_light_agent_users(group.users).map do |user|
            {
              id: user.id,
              name: user.name,
              email: user.email,
              group_id: group.id
            }
          end
        }
      end
    end

    ##########################
    # Group access
    ##########################

    def assignable_groups(_options = {})
      account.groups.assignable(self)
    end

    def preloaded_assignable_groups
      includes = { users: { account: {}, permission_set: :permissions }}
      assignable_groups.includes(includes).to_a
    end

    ##########################
    # Forum access
    ##########################

    def accessible_forums
      Forum.accessible_to(self).order('position, id ASC')
    end

    def accessible_forums_count
      Forum.accessible_to(self).count(:all)
    end

    ##########################
    # Entry access
    ##########################

    def entry_conditions(model = 'entries')
      condition = "#{model}.account_id = #{account_id}"

      unless can?(:access_restricted_content, Forum)
        org_ids = organization_ids

        if org_ids.empty?
          condition += " AND #{model}.organization_id IS NULL"
        else
          orgs       = org_ids.join(', ')
          condition += " AND (#{model}.organization_id IS NULL OR #{model}.organization_id IN (#{orgs}))"
        end
      end

      unless is_agent?
        # FIXME: Ugly, ugly hack because we call entry_conditions("forums") as well.
        condition += if model == 'entries'
          " AND #{model}.is_public = 1"
        else
          forum_visibility_conditions
        end

        if (tag_restricted_forum_ids = forums_barred_due_to_incompatible_tags).any?
          all_forum_ids = account.forums.pluck(:id)
          valid_tag_forum_ids = (all_forum_ids - tag_restricted_forum_ids).join(',')

          if valid_tag_forum_ids.present?
            condition += if model == 'entries'
              " AND entries.forum_id in (#{valid_tag_forum_ids})"
            else
              " AND forums.id in (#{valid_tag_forum_ids})"
            end
          else
            condition = "1=2" # Bail out.
          end
        end
      end
      condition
    end

    def forums_barred_due_to_incompatible_tags
      # TODO: USER_TAGS optimize.
      return [] if is_agent? || !account.settings.has_user_tags?
      if is_anonymous_user? || all_tags.empty?
        conditions = "taggable_type = 'Forum'"
      else
        tag_list = all_tags.collect { |t| "'#{t}'" }.join(',')
        conditions = "taggable_type = 'Forum' and tag_name not in (#{tag_list})"
      end
      account.taggings.where(conditions).group(:taggable_id).pluck(:taggable_id)
    end

    def forum_visibility_conditions
      sql_fragment = " AND forums.deleted_at IS NULL"

      return sql_fragment if is_agent?

      sql_fragment <<
        if is_anonymous_user?
          " AND forums.visibility_restriction_id = #{VisibilityRestriction::EVERYBODY.id}"
        else
          " AND forums.visibility_restriction_id != #{VisibilityRestriction::AGENTS_ONLY.id}"
        end
      sql_fragment << " AND (forums.translation_locale_id = #{translation_locale.id} OR forums.translation_locale_id IS NULL)"

      sql_fragment
    end

    private

    def wrap_in_parentheses(conditions)
      "(#{conditions})" if conditions
    end

    def agent_ticket_conditions
      ensure_collaboration_tickets_access_condition ||
        ensure_requested_tickets_access_condition
    end

    def ensure_collaboration_tickets_access_condition
      return if account.all_collaboration_disabled?

      if conditions = wrap_in_parentheses(ensure_requested_tickets_access_condition)
        "#{conditions} OR tickets.id IN (SELECT ticket_id FROM collaborations WHERE user_id = #{id})"
      end
    end

    def ensure_requested_tickets_access_condition
      if conditions = restricted_agent_conditions
        "#{conditions} OR tickets.requester_id = #{id}"
      end
    end

    def restricted_agent_conditions
      if agent_restriction?(:groups)
        restricted_groups_condition
      elsif agent_restriction?(:organization)
        restricted_organizations_condition
      elsif agent_restriction?(:assigned)
        restricted_assignee_condition
      end
    end

    def restricted_groups_condition
      "tickets.group_id IN (#{allowed_group_ids})"
    end

    def allowed_group_ids
      groups.present? ? group_ids.join(",") : 0
    end

    def restricted_organizations_condition
      "tickets.organization_id IN (#{agent_allowed_organization_ids})"
    end

    def agent_allowed_organization_ids
      organizations.present? ? organization_ids.join(",") : 0
    end

    def restricted_assignee_condition
      "tickets.assignee_id = #{id}"
    end

    def end_user_ticket_conditions
      [end_user_requester_condition].tap do |conditions|
        (conditions << end_user_organizations_condition) if end_user_organizations_condition.present?
        (conditions << end_user_collaboration_condition) if able_to_collaborate?
      end.join(" OR ")
    end

    def able_to_collaborate?
      account.legacy_ccs_or_email_ccs_enabled? && !id.nil?
    end

    def end_user_requester_condition
      "tickets.requester_id = #{id.blank? ? 0 : id}"
    end

    def end_user_organizations_condition
      @end_user_organizations_condition ||= begin
        ids = end_user_viewable_organizations.map(&:id)
        ids.any? ? "tickets.organization_id IN (#{ids.join(',')})" : ""
      end
    end

    def end_user_collaboration_condition
      "tickets.id IN (SELECT ticket_id FROM collaborations WHERE user_id = #{id})"
    end

    def negate_users?(options)
      if is_user_autocomplete?(options)
        !(can?(:search, User) || can?(:lookup, User))
      else
        !can?(:search, User)
      end
    end

    def is_user_autocomplete?(options) # rubocop:disable Naming/PredicateName
      options[:source] == 'autocomplete'
    end
  end
end
