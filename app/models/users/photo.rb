module Users
  module Photo
    VALID_EXTENSIONS = %w[png jpg jpeg gif].freeze
    MAX_URL_SIZE = 255

    def self.included(base)
      base.send(:attr_accessor, :remote_photo_url, :google_profile_image)

      base.after_commit -> {
        enqueue_photo_fetching_job
        enqueue_agent_chat_avatar_synchronization
      }
      base.validate     :remote_photo_url_is_valid
      base.validate     :validate_new_photo
      base.validates_length_of :remote_photo_url, maximum: MAX_URL_SIZE
    end

    def set_photo(data) # rubocop:disable Naming/AccessorMethodName
      return if data.blank? || data[:uploaded_data].blank? || data[:uploaded_data].size.zero?
      keep_photo_data_before_update
      if data[:uploaded_data].class == String
        errors.add(:base, I18n.t('txt.admin.models.users.photo.file_path_does_not_exist'))
        raise ActiveRecord::RecordInvalid, self
      else
        build_photo(account: account, original_url: data[:original_url], uploaded_data: data[:uploaded_data])
      end
    end

    def photo_url(base_url = nil, size = :thumb)
      base_url = account.url(mapped: false) if base_url.blank?
      "#{base_url}#{photo_path(size) || "/images/2016/default-avatar-80.png"}"
    end

    def large_photo_url
      photo_url(nil, nil)
    end

    def enqueue_photo_fetching_job
      if remote_photo_url && valid_url?(remote_photo_url) && remote_photo_url != photo.try(:original_url)
        klass = google_profile_image ? FetchGoogleProfileImageJob : FetchProfileImageJob
        klass.enqueue(account.id, id, remote_photo_url)
      end
    end

    def valid_url?(url)
      url    = URI.encode(url)
      parsed = URI.parse(url) rescue nil
      parsed && parsed.host.present? && parsed.path.present?
    end

    def remote_photo_url_is_valid
      if remote_photo_url.present? && !valid_url?(remote_photo_url)
        errors.add(:base, "Invalid photo URL")
      end
    end

    def validate_new_photo
      if photo && photo.new_record? && !photo.valid?
        # Include acceptable file types in the error message
        if !VALID_EXTENSIONS.include?(photo.extension)
          photo.errors[:extension].each do |extension_error|
            errors.add(:photo, extension_error)
          end
        else
          errors.add(:photo, :invalid)
        end
      end
    end

    def dispatch_event_after_photo_deletion
      keep_photo_data_before_update
      reload

      add_domain_event(UserPhotoChangedProtobufEncoder.new(self).to_object)

      enqueue_agent_chat_avatar_synchronization
    end

    def photo_url_before_update
      @photo_url_before_update ||= photo_url
    end

    def large_photo_url_before_update
      @large_photo_url_before_update ||= large_photo_url
    end

    def enqueue_agent_chat_avatar_synchronization
      return unless is_agent? && settings.chat_entitlement && account&.has_sync_support_agent_avatar_to_chat?

      SyncChatAgentAvatarJob.enqueue(account.id, id)
    end

    private

    def keep_photo_data_before_update
      photo_url_before_update
      large_photo_url_before_update
    end
  end
end
