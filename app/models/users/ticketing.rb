module Users
  module Ticketing
    def self.included(base)
      base.class_eval do
        has_many :tickets,           foreign_key: :requester_id, dependent: :destroy, inherit: :account_id
        has_many :suspended_tickets, foreign_key: :author_id,    dependent: :nullify, inherit: :account_id

        has_many :submitted, class_name: 'Ticket', foreign_key: :submitter_id, inherit: :account_id
        has_many :assigned,  class_name: 'Ticket', foreign_key: :assignee_id, inherit: :account_id

        has_many :rules,  as: :owner, dependent: :destroy, inherit: :account_id
        has_many :macros, as: :owner, inherit: :account_id
        has_many :views,  -> { order('position, title ASC') }, as: :owner, inherit: :account_id

        has_many :collaborations, dependent: :delete_all, inherit: :account_id
        has_many :collaborated_tickets,
          -> { order('tickets.id DESC').distinct },
          source: :ticket,
          through: :collaborations,
          inherit: :account_id
        has_many :followed_tickets,
          -> { where(collaborations: { collaborator_type: [CollaboratorType.FOLLOWER] }).order('tickets.id DESC').distinct },
          source: :ticket,
          through: :collaborations,
          inherit: :account_id
        has_many :ccd_tickets,
          -> { where(collaborations: { collaborator_type: [CollaboratorType.EMAIL_CC, CollaboratorType.LEGACY_CC] }).order('tickets.id DESC').distinct },
          source: :ticket,
          through: :collaborations,
          inherit: :account_id
        has_many :working_collaborated_tickets,
          -> { where(status_id: [StatusType.NEW, StatusType.OPEN, StatusType.PENDING, StatusType.HOLD]).order('tickets.id DESC') },
          source: :ticket,
          through: :collaborations,
          inherit: :account_id

        has_many :unverified_ticket_creations, inherit: :account_id

        before_save :ensure_public_comments_allowed
      end
    end

    def ensure_public_comments_allowed
      self.is_private_comments_only = false if is_end_user? || is_admin?
      true
    end

    def unsolved_ticket_count
      tickets.working.count(:all)
    end

    def tickets_solved_this_week
      tickets_solved_since(start_of_week.utc)
    end

    def tickets_solved_this_week_received_good_rating
      tickets_solved_this_week_received_rating([SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT])
    end

    def tickets_solved_this_week_received_bad_rating
      tickets_solved_this_week_received_rating([SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT])
    end

    def tickets_solved_since(date)
      account.tickets.where('assignee_id = ? AND solved_at > ? AND generated_timestamp > ?', id, date, date).count(:all)
    end

    def aggregate_views(options = {})
      types         = options[:types] || [:personal, :shared]
      only_viewable = options.fetch(:only_viewable, true).present?

      view_types = []
      view_types << shared_views(only_viewable: only_viewable) if types.include?(:shared)
      view_types << personal_views                             if types.include?(:personal)

      scope = account.all_views.and_disjunction_of(*view_types)
      scope = scope.with_ids(options[:only]) if options[:only].present?
      scope.order('IF(rules.owner_type = "User", 1, 0), position, title asc')
    end

    def personal_views
      if account.subscription.has_personal_rules?
        views.where(account_id: account.id).order('position, title asc')
      else
        views.none
      end
    end

    def shared_views(only_viewable: true, skip_ordering: false)
      views = account.all_views

      views = if account.subscription.has_group_rules?
        if !only_viewable && is_admin?
          views.owned_by(Account, Group)
        else
          account_scope = View.account_owned(account_id)

          group_ids = memberships.pluck(:group_id)
          group_scope = View.where(owner_type: 'Group', owner_id: group_ids)

          views.and_disjunction_of(account_scope, group_scope)
        end
      else
        views.account_owned(account_id)
      end

      skip_ordering ? views : order_shared_views(views)
    end

    def shared_and_personal_macros
      all_macros.and_disjunction_of(
        personal_macros,
        shared_macros
      ).order("IF(rules.owner_type = 'User', 1, 0), #{macro_ordering}")
    end

    def personal_macros
      if account.subscription.has_personal_rules?
        macros.where(account_id: account.id).order(macro_ordering)
      else
        macros.none
      end
    end

    def shared_macros
      unless account.subscription.has_group_rules?
        return all_macros.account_owned(account_id).order(macro_ordering)
      end

      group_ids = groups.map(&:id)

      if RAILS4
        group_scope = begin
          Macro.includes(:groups_macros).references(:groups_macros).where(
            "groups_macros.group_id IN (?) AND " \
              "rules.owner_type = 'Group' AND rules.owner_id IN (?)",
            group_ids,
            [*group_ids, Rule.group_owner_id]
          )
        end

        all_macros.and_disjunction_of(
          Macro.account_owned(account_id),
          group_scope
        ).order(macro_ordering)
      else
        group_macros = Macro.where(
          group_id: group_ids,
          owner_type: 'Group',
          owner_id: [*group_ids, Rule.group_owner_id]
        )

        Macro.account_owned(account_id).or(group_macros).order(macro_ordering)
      end
    end

    def all_macros
      return account.all_macros unless account.subscription.has_group_rules?

      account.all_macros.includes(:groups_macros).references(:groups_macros)
    end

    def macro_ordering
      if account.settings.macro_order == 'alphabetical'
        'title asc'
      else
        'position, title asc'
      end
    end

    def shared_views_order
      texts.lookup_or_default(:shared_views_order).value || []
    end

    private

    def tickets_solved_this_week_received_rating(ratings)
      account.tickets.solved.uniq.
        where(satisfaction_score: ratings).
        where(assignee_id: id).
        where('solved_at >= ?', start_of_week).
        count(:all)
    end

    def order_shared_views(views)
      if account.has_user_level_shared_view_ordering? && shared_views_order.any?
        # make sure to use the reader, not user.texts.shared_views_order directly so it handles nil properly
        # Once we upgrade rails there is a `.sanitize_sql_for_order` method that should be used

        # If the id is not in the `FIELD` condition, it will return zero. To account for this, we switch the ordering
        # to descending and reverse the users setting when passing it into SQL. This will ensure the users setting
        # takes precedence over the default.
        order = self.class.send(:sanitize_sql_array, ['FIELD(id, ?) DESC, position ASC, title ASC', shared_views_order.reverse])
        views.order(order)
      else
        views.order('position, title ASC')
      end
    end
  end
end
