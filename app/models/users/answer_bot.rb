module Users
  module AnswerBot
    def self.included(base)
      base.property_set(:settings, inherit: :account_id) do
        property :answer_bot, type: :boolean, default: false
      end
    end

    def answer_bot=(value)
      settings.answer_bot = value
    end

    def answer_bot?
      settings.answer_bot?
    end
  end
end
