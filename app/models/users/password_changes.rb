User.class_eval do
  has_many :password_changes, inherit: :account_id
  after_save :store_password_changes, if: :crypted_password_changed?
  after_save :delete_tokens, if: :crypted_password_changed?

  def will_be_saved_by(user)
    self.current_user = user
  end

  def delete_password_verification_tokens
    verification_tokens.delete_all
    identities.each { |i| i.verification_tokens.delete_all }
  end

  private

  def store_password_changes
    password_changes.create!(
      previous_value: crypted_password_change.first,
      new_value: crypted_password_change.last,
      ip_address: ip_address,
      actor_id: current_user.try(:id)
    )
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Unable to save password change", reraise: !Rails.env.production?, fingerprint: 'b9b2050ddd0812ab0e0a7de9534c1e846a650184')
  end

  def delete_tokens
    verification_tokens.destroy_all
    delete_mobile_devices
    Rails.logger.warn("Password changed for user ##{id}; verification and access tokens deleted")
  end

  def delete_mobile_devices
    device_identifiers.destroy_all
    mobile_devices.destroy_all
  end
end
