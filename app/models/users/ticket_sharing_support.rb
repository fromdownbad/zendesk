require 'ticket_sharing/actor'

module Users
  module TicketSharingSupport
    def self.included(base)
      base.property_set(:settings, inherit: :account_id) do
        property :foreign_agent, default: '0'
        property :foreign, default: '0'
      end
    end

    def foreign_agent=(value)
      settings.foreign_agent = value
    end

    def foreign_agent?
      settings.foreign_agent?
    end

    def foreign=(value)
      settings.foreign = value
    end

    def foreign?
      settings.foreign?
    end

    def for_partner
      TicketSharing::Actor.new(
        'uuid' => generate_uuid,
        'name' => safe_name(false),
        'role' => is_agent? ? 'agent' : 'user'
      )
    end

    def generate_uuid
      key = "#{account.sharing_url}/users/#{id}"
      Digest::SHA1.hexdigest(key)
    end
  end
end
