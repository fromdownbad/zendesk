module Users
  module Naming
    def self.included(base)
      base.class_eval do
        before_validation   :sanitize_name
        validates_length_of :name, minimum: 1
      end
    end

    def to_s
      name
    end

    def first_name
      @first_name ||= full_name.first
    end

    def last_name
      @last_name ||= full_name.last
    end

    def clean_name
      full_name.clean
    end

    def display_name
      prefer_alias_over_name? ? agent_display_name : name
    end

    def prefer_alias_over_name?
      # Aliases are only available to Profesional and Enterprise agents (under agent_display_names feature)
      is_agent? && agent_display_name.present? && account && account.has_agent_display_names?
    end

    def has_last_name? # rubocop:disable Naming/PredicateName
      last_name != name
    end

    protected

    def sanitize_name
      # [ZD#1433689] Fixes directionality
      self.name = full_name.sanitize.fix_directionality
      true
    end

    def full_name
      @full_name = Name.new(name, account)
    end

    class Name
      COMMA = ', '.freeze
      SPACE = ' '.freeze
      PIPE  = '|'.freeze

      attr_accessor :name
      attr_reader :account

      def initialize(name, account = nil)
        @account = account
        @name = name
      end

      def first
        last_name_first? ? parts.last : parts.first
      end

      def last
        last_name_first? ? parts.first : parts.last
      end

      def clean
        return 'Unknown' if name.blank?
        name.gsub(/[\r\n\"]/, '')
      end

      def sanitize
        sanitized = name.to_s

        new_name = sanitized.gsub(URI_IN_NAME_REGEX, '').strip

        if sanitized != new_name
          log_sanitized = name.present? && new_name.blank? ? "Unknown" : new_name
          Rails.logger.info(message: "sanitizing_requester_names, removing URL", account_id: account.id, old: sanitized, sanitized: log_sanitized)

          sanitized = new_name
          if name.present? && sanitized.blank?
            return 'Unknown'
          end
        end
        # Removes email addresses from names and aliases
        sanitized = sanitized.gsub(/\s*<.*?@.*?>\s*/, '') unless sanitized.blank?

        # e.g. if `name` is something like "<someone@email.com>",
        # which would get stripped entirely in the previous sanizatization step
        if name.present? && sanitized.blank? && email_in_name_match = name.match(FIND_EMAIL_PATTERN)
          sanitized = Zendesk::Mail::Address.new(address: email_in_name_match[0]).name
        end

        # Removes `<` chars and trailing spaces
        sanitized.delete("<").strip
      end

      protected

      # Name can be 'John Doe' or 'Doe, John'
      def last_name_first?
        comma?
      end

      def parts
        if name.include?(PIPE)
          clean.split(PIPE)
        else
          clean.split(seperator)
        end
      end

      def seperator
        comma? ? COMMA : SPACE
      end

      def comma?
        name.include?(COMMA)
      end

      def twitter_handle?
        name.starts_with?('@')
      end
    end
  end
end
