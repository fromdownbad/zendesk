module Users
  module OrganizationMemberships
    extend ActiveSupport::Concern

    included do
      belongs_to :organization

      has_many   :organization_memberships,
        autosave: true,
        dependent: :destroy,
        inverse_of: :user,
        inherit: :account_id

      has_many   :organizations,
        -> { order(name: :asc) },
        through: :organization_memberships,
        source: :organization,
        inverse_of: :users do
        def shared
          where("organizations.is_shared = 1")
        end
      end

      before_account_convert :mark_as_during_account_conversion

      validates_associated :organization_memberships_with_active_orgs
      validate             :validate_organization_assignment_size

      before_save  :set_default_organization
      after_commit :assign_unclosed_tickets_to_organization

      def write_attribute_with_log(key, value)
        if key == "organization_id"
          Rails.logger.info("Setting organization_id for user: #{id} to: #{value} was: #{read_attribute(:organization_id)}")
          if account&.has_invalid_organization_logging?
            if @organization_with_fallback_called
              Rails.logger.info "organization_with_fallback was called"
              @organization_with_fallback_called = nil
            elsif value && !organization_memberships.exists?(organization_id: value)
              message = 'User.organization_id was set without updating organization memberships'
              exception = StandardError.new(message)
              exception.set_backtrace(caller)
              ZendeskExceptions::Logger.record(exception, location: self, message: message, fingerprint: '92303b2da273e21e8ed8e5b7a176e5cdb6f3c5c5')
              Rails.logger.info "invalid_organization: #{message}"
              Rails.logger.info "invalid_organization: org_memberships: #{organization_memberships.map(&:organization_id)}, org_id: #{value}"
            end
          end
        end
        write_attribute_without_log(key, value)
      end
      alias_method :write_attribute_without_log, :write_attribute
      alias_method :write_attribute, :write_attribute_with_log

      def organization_with_fallback
        organization_without_fallback || organization_from_default_membership
      end
      alias_method :organization_without_fallback, :organization
      alias_method :organization, :organization_with_fallback

      def organization_with_fallback=(other)
        @organization_with_fallback_called = account&.has_invalid_organization_logging?
        build_or_update_default_organization_membership_for(other)
        self.organization_without_fallback = other || organization_from_default_membership
      end
      alias_method :organization_without_fallback=, :organization=
      alias_method :organization=, :organization_with_fallback=

      def organization_from_default_membership
        default_organization_membership.try(:organization)
      end

      alias_method :default_organization,  :organization
      alias_method :default_organization=, :organization=

      def organizations=(organizations_or_ids)
        synchronize_organization_memberships(
          organization_ids_difference(organizations_or_ids)
        )
        self.organization_without_fallback = organization_from_default_membership
      end
    end

    def set_default_organization
      if organization.nil?
        if default = account.default_organization_for(email_domain)
          self.organization = default.reload
          Rails.logger.info("set_default_organization to #{organization.id} for #{account_id}:#{id}")
        end
      elsif organization_id.nil?
        self.organization_id = organization.id
      end
      organization
    end

    def default_organization_membership
      organization_memberships.detect do |membership|
        membership.default? && !membership.marked_for_destruction?
      end
    end

    def regular_organization_memberships
      organization_memberships.reject do |membership|
        membership.default? || membership.marked_for_destruction?
      end
    end

    def organization_name
      # ZD#449849 -> this is where we return the account name, see other ZD note in view code
      return organization.name if organization_id? && organization
      is_agent? ? account.name : nil
    end

    def move_tickets_to_organization(from, to)
      scope = tickets.not_closed
      scope = scope.where(organization_id: from) unless from == :any
      bulk_update_tickets(scope.pluck(:nice_id), to)
    end

    def has_multiple_organizations? # rubocop:disable Naming/PredicateName
      organization_memberships.size > 1
    end

    def has_organizations?(other) # rubocop:disable Naming/PredicateName
      other = Array(other)
      if persisted? && other.size == 1
        organization_memberships.where(organization_id: other.first).exists?
      else
        ids = organization_ids_with_new
        (ids | other) == ids
      end
    end

    def organization_ids_with_new
      (new_record? ? organization_memberships.map(&:organization_id) : organization_ids)
    end

    def current_organization_ids
      @current_organization_ids ||= organization_memberships.map(&:organization_id)
    end

    private

    attr_accessor :organization_max_exceeded
    alias_method  :organization_max_exceeded?, :organization_max_exceeded

    def build_or_update_default_organization_membership_for(organization)
      if default_organization_membership && organization
        update_default_organization_membership(organization)
      elsif organization && regular_organization_membership_for(organization).present?
        update_regular_organization_membership_to_default_for(organization)
      elsif organization
        build_default_organization_membership(organization)
      elsif default_organization_membership
        reset_or_destroy_default_organization_membership
      end
    end

    def update_default_organization_membership(organization)
      if account.has_multiple_organizations_enabled?
        multiple_organization_membership_default_update(organization)
      else
        default_organization_membership.organization = organization
        @assign_organization_to_tickets = true
      end
    end

    def multiple_organization_membership_default_update(organization)
      # keep default organization correct while we wait for org-membership save callback to unset the previous default
      default_organization_membership.default = false

      if new_default_membership = regular_organization_membership_for(organization)
        new_default_membership.default = true
      else
        build_default_organization_membership(organization)
      end
    end

    def update_regular_organization_membership_to_default_for(organization)
      regular_organization_membership_for(organization).default = true
    end

    def build_default_organization_membership(organization)
      organization_memberships.build do |organization_membership|
        organization_membership.organization = organization
        organization_membership.account_id   = account_id
        organization_membership.default      = true
      end
      @assign_organization_to_tickets = no_existent_organization_memberships?
    end

    def reset_or_destroy_default_organization_membership
      if default = account.default_organization_for(email_domain)
        default_organization_membership.organization = default.reload
      else
        default_organization_membership.mark_for_destruction
      end
    end

    def regular_organization_membership_for(organization)
      regular_organization_memberships.detect do |organization_membership|
        organization_membership.organization_id == organization.id
      end
    end

    def no_existent_organization_memberships?
      organization_memberships.any? && organization_memberships.all?(&:new_record?)
    end

    def organization_ids_difference(organizations_or_ids)
      IdSetsDifference.new(
        organization_memberships.map(&:organization_id), organizations_or_ids
      )
    end

    def synchronize_organization_memberships(difference)
      return if @organization_max_exceeded = organization_max_violated?(difference)

      add_organization_memberships(difference.added)
      remove_organization_memberships(difference.removed)
      ensure_default_organization_membership
      @assign_organization_to_tickets = no_existent_organization_memberships?
    end

    def organization_max_violated?(difference)
      difference.result > organization_max && !difference.added.empty?
    end

    def organization_max
      account.has_multiple_organizations_enabled? ? Float::INFINITY : 1
    end

    def add_organization_memberships(organization_ids)
      account.organizations.find(organization_ids).each do |organization|
        organization_memberships.build do |organization_membership|
          organization_membership.organization = organization
          organization_membership.account      = account
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      raise OrganizationNotFound, e.message.split(' [').first
    end

    class OrganizationNotFound < ActiveRecord::RecordNotFound; end

    def remove_organization_memberships(organization_ids)
      organization_memberships.select do |organization_membership|
        organization_ids.include?(organization_membership.organization_id)
      end.each(&:mark_for_destruction)
    end

    def ensure_default_organization_membership
      unless default_organization_membership
        regular_organization_memberships.first.try(:default=, true)
      end
    end

    def assign_unclosed_tickets_to_organization
      return if @is_during_account_conversion || !assign_organization_to_tickets?
      set_organization_on_unclosed_tickets!
    end

    def assign_organization_to_tickets?
      @assign_organization_to_tickets && tickets.not_closed.any?
    end

    def set_organization_on_unclosed_tickets!
      ids = tickets.not_closed.where('organization_id IS NULL OR organization_id != ?', organization.id).pluck(:nice_id)
      bulk_update_tickets(ids, organization.id)
    end

    def bulk_update_tickets(ticket_ids, new_organization_id)
      return if ticket_ids.empty?
      TicketBulkUpdateJob.enqueue(
        account_id: account_id,
        user_id: CIA.current_actor.try(:id) || id,
        ticket_ids: ticket_ids,
        ticket: { organization_id: new_organization_id },
        via_id: ViaType.USER_CHANGE
      )
    end

    def organization_memberships_with_active_orgs
      organization_memberships.select(&:organization) if organization_memberships
    end

    def validate_organization_assignment_size
      if organization_max_exceeded?
        errors.add(
          :base, I18n.t("txt.error_message.organization_assignment_max_exceeded")
        )
      end
    end

    def mark_as_during_account_conversion
      @is_during_account_conversion = true
    end
  end
end
