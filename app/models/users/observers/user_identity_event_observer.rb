module UserIdentityEventObserver
  extend ActiveSupport::Concern

  included do
    after_create :creation_identity_event
    after_update :changed_identity_event
    after_destroy :destroy_identity_event
  end

  def creation_identity_event
    user.add_domain_event(UserIdentityCreatedProtobufEncoder.new(self).to_object)
    user.publish_user_events_to_bus!
  end

  def changed_identity_event
    return unless changes[:value]

    previous, = changes[:value]
    user.add_domain_event(UserIdentityChangedProtobufEncoder.new(self, previous).to_object)
    user.publish_user_events_to_bus!
  end

  def destroy_identity_event
    user.add_domain_event(UserIdentityRemovedProtobufEncoder.new(self).to_object)
    user.publish_user_events_to_bus!
  end
end
