module OrganizationMembershipObserver
  extend ActiveSupport::Concern

  included do
    after_create :create_membership_event
    after_destroy :destroy_membership_event
  end

  def create_membership_event
    user.add_domain_event(UserOrganizationAddedProtobufEncoder.new(user, organization).to_object)
    user.publish_user_events_to_bus!
  end

  def destroy_membership_event
    user.add_domain_event(UserOrganizationRemovedProtobufEncoder.new(user, organization).to_object)
    user.publish_user_events_to_bus!
  end
end
