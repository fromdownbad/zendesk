module GroupMembershipObserver
  extend ActiveSupport::Concern

  included do
    after_create :create_membership_event
    after_destroy :destroy_membership_event
  end

  def create_membership_event
    user.add_domain_event(UserGroupAddedProtobufEncoder.new(user, group).to_object)
    user.publish_user_events_to_bus!
  end

  def destroy_membership_event
    user.add_domain_event(UserGroupRemovedProtobufEncoder.new(user, group).to_object)
    user.publish_user_events_to_bus!
  end
end
