module UserEntityObserver
  extend ActiveSupport::Concern

  included do
    after_touch :publish_user_entity_snapshot!, if: -> (r) { r.class == User }
    after_save :publish_user_entity_snapshot!, if: -> (r) { r.class == User }
    after_save :publish_user_entity_from_association!, if: -> (r) { r.class.method_defined? :user }
    after_destroy :publish_user_entity_from_association!, if: -> (r) { r.class.method_defined? :user }
  end

  def publish_user_entity_snapshot!
    publish(id: id)
  end

  def publish_user_entity_from_association!
    return if self.class == Photo
    publish(id: user_id)
  end

  private

  def publish(id:)
    user_entity_publisher.publish_for_user(user_id: id)
  end

  def user_entity_publisher
    @user_entity_publisher ||= UserEntityPublisher.new(account_id: account_id)
  end
end
