module Users
  module Identification
    include Voice::Core::Support::Identification

    # NOTE: Tested in zendesk/test/models/user_test.rb
    def self.included(base)
      base.class_eval do
        # NOTE: Duplicate of association defined in zendesk_core, with account ID inheritance
        if ActiveRecord::VERSION::MAJOR >= 4
          has_many :identities, -> { order('priority') }, class_name: 'UserIdentity', dependent: :delete_all, inverse_of: :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
        else
          has_many :identities, class_name: 'UserIdentity', order: 'priority', dependent: :delete_all, inverse_of: :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
        end

        has_many :unverified_email_addresses, dependent: :delete_all

        scope :verified, -> { base.joins(:identities).where(["user_identities.is_verified = ?", true]) }

        validates_presence_of   :email, if: proc { |user| user.is_active && user.is_agent? }
        validates_presence_of   :identities, if: :is_active

        validate :valid_identities, if: :is_active

        after_save :store_new_twitter_identity, if: :new_twitter_identity_present?
        after_save :remove_voice_number, if: :should_remove_voice_number?

        # mocha vs alias on 1.8 removes the alias in teardown <-> can be made alias if test/functional/people/users_controller_test.rb passes
        def is_verified? # rubocop:disable Naming/PredicateName
          is_verified
        end

        def self.find_by_identity(identity)
          find_by_id(identity.user_id) if identity
        end

        # mysql will find records with value 'abc' even when looking for 'abc '
        def self.find_by_external_id(external_id)
          return if external_id.blank?
          where(external_id: external_id.to_s).first
        end
      end
    end

    attr_accessor :skip_verification
    attr_accessor :skip_email_identity_blacklist_validation
    attr_writer :send_verify_email

    def valid_identities
      identities.each do |identity|
        next unless !identity.valid? && identity.errors[:value].present?
        identity.errors[:value].each do |error|
          errors.add(identity.class.identity_type, error)
        end
      end
      errors.delete(:identities)
    end

    # Returns true if the verification skipped or if the user has any (persisted) identity verified.
    def is_verified # rubocop:disable Naming/PredicateName
      skip_verification || identities.any? { |i| !i.new_record? && i.is_verified? }
    end

    def has_verified_email?(email) # rubocop:disable Naming/PredicateName
      if email
        !(identities.select { |i| i.is_a?(UserEmailIdentity) && i.is_verified && i.value == email }).empty?
      else
        false
      end
    end

    def is_verified=(value)
      return if value.nil?
      self.skip_verification ||= value
      if primary_identity = identities.first
        if primary_identity.new_record?
          primary_identity.is_verified = value
        else
          primary_identity.update_attribute(:is_verified, value)
        end
      end
    end

    def main_identity
      return I18n.t('txt.admin.models.user.identification.deleted_label') unless is_active?
      return email if email
      return "@#{twitter_profile.screen_name}" if has_twitter_identity?
      return rendered_phone if direct_number
      I18n.t('txt.admin.models.user.na_label')
    end

    def has_alter_ego? # rubocop:disable Naming/PredicateName
      identities.facebook.first.present? || identities.twitter.first.present?
    end

    # Google

    def has_google_identity? # rubocop:disable Naming/PredicateName
      identities.email.any?(&:google?)
    end

    # Facebook

    def has_facebook_identity? # rubocop:disable Naming/PredicateName
      identities.facebook.any?
    end

    # AnyChannel

    def has_any_channel_identity? # rubocop:disable Naming/PredicateName
      identities.any_channel.any?
    end

    # Email

    def email_address_with_name
      return unless (email_address = email)
      address = email_address
      name = email_name

      # email_name will be "\"\"" if name.blank? so we check for length > 2
      address = "#{name} <#{address}>" if name.length > 2

      parsed_address = Zendesk::Mail::Address.parse(address) rescue nil
      if parsed_address
        address
      else
        Rails.logger.warn("Failed to generate valid email address for delivery (#{address}) - user #{id} - account #{account_id}")
        statsd_client.increment(:invalid_email_address)
        email_address
      end
    end

    def email_name
      ApplicationMailer.encode_address_name(name, account)
    end

    def email_domain
      email.split("@").last if email
    end

    def email_address_with_name_without_quotes
      if email_address = email
        "#{name} <#{email_address}>"
      end
    end

    def send_verify_email
      skip_verification ? false : @send_verify_email
    end

    def should_ask_for_email?
      !is_anonymous_user? && identities.email.empty? && unverified_email_addresses.empty?
    end

    def create_unverified_email_address(email)
      unverified_email_addresses.build(email: email)
    end

    def has_email? # rubocop:disable Naming/PredicateName
      email.present?
    end

    def email=(new_email)
      identity = identities.email.find_by_value(new_email) unless new_record?

      unless identity
        identity = UserEmailIdentity.new(account: account, user: self, value: new_email)
        identities << identity
      end

      identity
    end

    def is_email_google? # rubocop:disable Naming/PredicateName
      email_identity_for_email_accessor.try(:google?)
    end

    def email_is_deliverable?
      email_identity_for_email_accessor.try(:deliverable?)
    end

    # Twitter

    def twitter_profile
      identities.twitter.first.try(:twitter_user_profile)
    end

    def store_new_twitter_identity
      identities << UserTwitterIdentity.new(
        is_verified: true,
        screen_name: @new_twitter_identity, account: account
      )
    end

    def new_twitter_identity_present?
      @new_twitter_identity.present?
    end

    def has_twitter_identity? # rubocop:disable Naming/PredicateName
      if identities.twitter.any?
        return true if twitter_profile.present?
        Rails.logger.info('No twitter profile present for account #{account.id} id #{identities.twitter.first.value}')
      end
      false
    end

    # Phone

    def has_phone_identity? # rubocop:disable Naming/PredicateName
      identities.phone.any?
    end

    def has_only_phone_identity? # rubocop:disable Naming/PredicateName
      (identities.phone.count(:all) == 1) &&
        (identities.where('type != ?', 'UserPhoneNumberIdentity').count(:all) == 0)
    end

    def add_email_address(address)
      identities << UserEmailIdentity.new(account: account, user: self, value: address)
    end

    private

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [:errors])
    end
  end
end
