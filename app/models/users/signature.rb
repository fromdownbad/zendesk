module Users
  module Signature
    def self.included(base)
      base.has_one :signature, dependent: :destroy, autosave: true, class_name: "::Signature"
    end

    def agent_signature(rich:)
      if is_agent? && signature.present?
        rich ? signature.html_value.delete("\n").html_safe : signature.value
      end
    end

    def set_signature(value, rich:)
      return unless is_agent?

      build_signature(account: account, user: self) unless signature.present?

      if rich
        signature.html_value = value
      else
        signature.value = value
      end
    end
  end
end
