module Users
  module Localization
    MAX_UPDATE_ALL_TICKETS = 1_000

    def self.included(base)
      base.extend TimeZoneValidation
      base.class_eval do
        belongs_to :translation_locale, foreign_key: 'locale_id'
        has_many   :translation_locales

        delegate :locale, to: :translation_locale

        validates_presence_of :time_zone
        validates_time_zone   :time_zone
        validate :validate_locale_id, if: :validate_locale?

        before_validation :default_to_account_time_zone_and_clock

        before_save :ensure_proper_locale, if: :roles_changed?
        before_save :clear_locale,         if: :should_clear_locale?
        before_save :clear_time_zone,      if: :cannot_select_time_zone?

        after_commit :update_not_closed_tickets_locale, if: :locale_id_was_changed?
      end
    end

    def locale=(locale)
      @locale_was_changed = true
      self.locale_id = TranslationLocale.find_by_locale(locale)&.id
    end

    def translation_locale
      super ||
        if is_end_user?
          account.translation_locale
        elsif is_agent?
          account.translation_locale.localized_agent ? account.translation_locale : ENGLISH_BY_ZENDESK
        else
          ENGLISH_BY_ZENDESK
        end
    end

    def find_best_locale(available_locales = [], user_locale = translation_locale)
      return user_locale if available_locales.include?(user_locale.locale) || user_locale == ENGLISH_BY_ZENDESK
      find_best_locale(available_locales, user_locale.parent)
    end

    def time_format_string(include_zone = false)
      key = if include_zone
        uses_12_hour_clock? ? "time.supplemental.12hr.tz" : "time.supplemental.24hr.tz"
      else
        uses_12_hour_clock? ? "time.supplemental.12hr" : "time.supplemental.24hr"
      end

      Zendesk::Cldr::DateTime.new(translation_locale).single(key)
    end

    def time_zone
      # Handle this differently - results in a subscription object getting loaded on each request
      # return account.time_zone unless account.subscription && account.subscription.has_individual_time_zone_selection?
      read_attribute(:time_zone) || account.time_zone
    end

    def start_of_week
      Time.now.in_time_zone(time_zone).monday
    end

    def uses_12_hour_clock
      account.uses_12_hour_clock
    end

    def uses_12_hour_clock?
      uses_12_hour_clock
    end

    def available_languages
      return [ENGLISH_BY_ZENDESK] if is_agent?
      available_languages_for_subscription
    end

    def available_languages_for_subscription(localized_for_agents = false)
      alphabetically_sorted do
        if localized_for_agents
          TranslationLocale.available_for_agents(account)
        elsif account.has_individual_language_selection?
          account.available_languages
        else
          [account.translation_locale]
        end
      end
    end

    def alphabetically_sorted
      translations = yield
      translations.sort_by(&:name)
    end

    def available_locales
      alphabetically_sorted do
        translation_locales | TranslationLocale.end_user_available
      end
    end

    protected

    def default_to_account_time_zone_and_clock
      self.time_zone          ||= account.time_zone
      self.uses_12_hour_clock ||= account.uses_12_hour_clock
      true
    end

    def update_not_closed_tickets_locale
      tickets_count = tickets.count(:all)

      # There is a racing condition when creating accounts in AccountsPrecreationJob (PreAccountCreation)
      # and finding the account when performing LocaleBulkUpdateJob
      if tickets_count < MAX_UPDATE_ALL_TICKETS && account && (account.has_skip_locale_bulk_update_job? || account.is_account_precreation?)
        tickets.update_all(locale_id: locale_id)
      elsif tickets_count > 0
        LocaleBulkUpdateJob.enqueue(account_id, id)
      end
    end

    # An agent never has an unavailable locale since we always gets our core translations no matter what
    def should_clear_locale?
      return false if is_agent?
      !account.has_individual_language_selection? || locale_unavailable?
    end

    def ensure_proper_locale
      locale = if is_end_user?
        proper_locale_for_end_user
      else
        proper_locale_for_agent
      end
      self.locale_id = locale
    end

    def proper_locale_for_end_user
      if !account.available_languages.include?(translation_locale)
        nil
      else
        translation_locale.id
      end
    end

    def proper_locale_for_agent
      available_languages_for_agent = available_languages_for_subscription(true)
      if available_languages_for_agent.include?(translation_locale)
        translation_locale.id
      elsif available_languages_for_agent.include?(account.translation_locale)
        account.translation_locale.id
      else
        ENGLISH_BY_ZENDESK.id
      end
    end

    def locale_unavailable?
      translation_locale && !account.available_languages.map(&:id).include?(locale_id) && is_end_user?
    end

    def clear_locale
      self.locale_id = nil
      true
    end

    def cannot_select_time_zone?
      !account.has_time_zone_selection?
    end

    def clear_time_zone
      self.time_zone = nil
      true
    end

    def validate_locale_id
      error_field = locale_was_changed? ? :locale : :locale_id
      errors.add(error_field, :invalid) unless TranslationLocale.exists?(locale_id)
    end

    private

    def validate_locale?
      locale_was_changed? || (locale_id_changed? && !locale_id.nil?)
    end

    def locale_id_was_changed?
      locale_id_changed? || previous_changes.include?('locale_id')
    end

    def locale_was_changed?
      !!@locale_was_changed
    end
  end
end
