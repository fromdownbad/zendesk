module Users; end
module Users::PhoneNumberBehavior
  def self.included(base)
    base.validates :phone, 'users/phone_number': true, if: ->(u) { u.phone_changed? }
  end

  attr_accessor :skip_phone_number_validation

  def direct_number
    phone_identity.try(:value)
  end

  def rendered_phone
    if direct_number.present?
      render_phone_number(direct_number, inline: false)
    else
      phone
    end
  end

  def phone_number
    if shared_phone_number
      phone + extension
    else
      direct_line_number = first_direct_line_number_by_priority
      # handle legacy cases where shared_phone_number contains direct line and extension
      if !phone.blank? && phone.include?(direct_line_number)
        return phone
      else
        return direct_line_number
      end
    end
  end

  def shared_phone_number
    # Can be nil or false
    return @shared_phone_number if defined?(@shared_phone_number)

    # return nil if user has no phone numbers (direct line or shared), otherwise boolean
    @shared_phone_number = if phone.blank?
      first_direct_line_number_by_priority.blank? ? nil : false
    else
      !has_direct_number?
    end
  end

  def remove_phone_number_identity(phone)
    phone_identity(phone).try(:destroy)
    reset_phone_identities_cache
  end

  def add_shared_phone_number_extension(extension)
    # TODO: This seems to be a bug where instances of UserPhoneExtension are created with user_id: nil
    user_phone_extension ||= build_user_phone_extension(account: account)
    user_phone_extension.value = extension.gsub(/\s/, '')
  end

  def remove_shared_phone_number_extension
    user_phone_extension.destroy if user_phone_extension.present?
  end

  def formatted_phone_number
    ::Voice::Core::NumberSupport::E164Number.formatted_number(phone)
  end

  def has_direct_number?(number = nil) # rubocop:disable Naming/PredicateName
    number ||= phone
    return false if number.nil?

    number = clear_phone_number(number)

    phone_identities.detect { |identity| (clear_phone_number(identity.value) == number) }.present?
  end

  def phone_identity(phone = nil)
    phone.nil? ? first_identity_sorted_by_priority : first_identity_by_number(phone)
  end

  def update_phone_and_identity(number, direct_line)
    identity = phone_identity(number)
    if identity
      if direct_line && number.present?
        identity.value = number
        identity.save
      else
        identity.destroy
        reset_phone_identities_cache

        self.phone = number
      end
    elsif direct_line
      add_new_phone_number_identity(number)
    else
      extension = number.to_s.match(/x(\s)*\d+$/).to_s

      if extension.present?
        add_shared_phone_number_extension(extension)

        number[extension] = ''
      end

      self.phone = number
    end
  end

  def with_the_same_phone_number_and_priority_to_identity
    normalized_phone = ::Voice::Core::NumberSupport::E164Number.internal(phone, extension: false)

    identity_with_the_same_phone_number(normalized_phone) ||
      user_with_the_same_phone_number(normalized_phone) ||
      ""
  end

  private

  def clear_phone_number(number)
    number = Voice::Core::NumberSupport::E164Number.internal(number) if account.apply_phone_number_validation?
    number
  end

  def add_new_phone_number_identity(number)
    identity = build_phone_identity(number)
    identities << identity
    reset_phone_identities_cache
    identity.save unless new_record?
  end

  def build_phone_identity(number)
    UserPhoneNumberIdentity.new(user: self, value: number)
  end

  def first_direct_line_number_by_priority
    @first_direct_line_number_by_priority ||= first_identity_value
  end

  def user_with_the_same_phone_number(normalized_phone)
    self.class.
      active.
      where(account_id: account_id).
      where(phone: normalized_phone).
      where('id != ?', id).
      first
  end

  def identity_with_the_same_phone_number(normalized_phone)
    UserPhoneNumberIdentity.
      where(account_id: account_id).
      where(value: normalized_phone).
      where('user_id != ?', id).
      first.
      try(:user)
  end

  def phone_identities
    @identities ||= identities.select { |i| i.instance_of?(UserPhoneNumberIdentity) && !i.destroyed? }
  end

  def first_identity_sorted_by_priority
    @first_identity_sorted_by_priority ||= phone_identities.sort_by(&:priority).first
  end

  def first_identity_value
    phone_number = first_identity_sorted_by_priority
    if phone_number.present?
      return phone_number.value
    end

    nil
  end

  def reset_phone_identities_cache
    @identities = nil
  end

  def first_identity_by_number(phone)
    identities.where(value: phone.to_s.sub(/x(\s)*\d+$/, '')).first
  end

  def extension
    if user_phone_extension.present? && !user_phone_extension.destroyed?
      return user_phone_extension.value
    end

    ''
  end
end
