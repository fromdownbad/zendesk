module Users
  module Tags
    def tags
      tag_array
    end

    def tag_array
      return [] unless account.try(:has_user_and_organization_tags?)

      super
    end

    def all_tags
      t = tag_array
      # This code might be problematic. organization might be nil, while organization_id might have been set.
      # Have not been able to reproduce this in a test environment, but Hoptoad seems to indicate the problem:
      # https://zendesk.hoptoadapp.com/errors/7717717 - based on the old code
      # t += organization.tag_array if organization_id
      t += organization.tag_array if organization
      t.uniq
    end

    def ticket_tags(organization)
      return [] unless account.has_tickets_inherit_requester_tags?

      t  = tag_array
      t += organization.tag_array if organization

      t.uniq
    end
  end
end
