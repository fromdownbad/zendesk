module Users
  module CustomerLists
    def self.included(base)
      base.class_eval do
        before_create     :set_customer_list_onboarding_state
        before_validation :set_sample
      end
    end

    def set_customer_list_onboarding_state
      settings.customer_list_onboarding_state = UserView::OnboardingStates::TOUR
    end

    def set_sample
      return if sample.present?
      self.sample = rand(1..UserView::SAMPLING_RANGE)
      true
    end
  end
end
