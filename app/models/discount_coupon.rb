class DiscountCoupon < Coupon
  validates_presence_of :discount_percentage
  validates_numericality_of :discount_percentage, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  validates_presence_of :minimum_incremental_revenue
  validates_numericality_of :minimum_incremental_revenue, greater_than_or_equal_to: 0

  validates_numericality_of :maximum_monthly_discount, allow_nil: true, greater_than: 0

  # Checks an existing coupon application against a specific subscription instance for validity.
  def is_valid_on?(subscription_instance, coupon_application) # rubocop:disable Naming/PredicateName
    subscription_instance.monthly_undiscounted_price >= coupon_application.initial_full_monthly_price + minimum_incremental_revenue
  end

  private

  def current_redemption_payments
    coupon_redemptions.
      where("payments.period_begin_at < ? AND payments.period_end_at >= ?", Time.now, Time.now).
      joins(:payment).
      each do |r|
        p = r.payment
        yield p, r, (Time.now.at_midnight - p.period_begin_at) / p.duration.to_f
      end
  end
end
