class TicketFormBrandRestriction < ActiveRecord::Base
  include CachingObserver

  attr_accessible

  belongs_to :account
  belongs_to :ticket_form, inherit: :account_id
  belongs_to :brand, inherit: :account_id

  before_validation :set_account

  validates_presence_of :account, :ticket_form, :brand
  validates_uniqueness_of :ticket_form_id, scope: :brand_id
  validate :validate_account_on_brand_and_ticket_form_match, if: :brand_and_ticket_form_present?

  private

  def set_account
    self.account = brand.try(:account)
  end

  def brand_and_ticket_form_present?
    brand.present? && ticket_form.present?
  end

  def validate_account_on_brand_and_ticket_form_match
    if brand.account != ticket_form.account
      errors.add(:ticket_form, "must belong to the same account as the brand")
    end
  end
end
