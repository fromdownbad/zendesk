module DataDeletionLifecycle
  extend ActiveSupport::Concern

  included do
    before_create { self.status ||= 'waiting' }
  end

  def start!
    self.status = 'started'
    self.started_at = Time.now.utc
    save!
  end

  def finish!
    self.status = 'finished'
    self.completed_at = Time.now.utc
    save!
  end

  def fail!(exception)
    self.status = 'failed'
    self.failed_reason = { klass: exception.class.name, message: exception.message, backtrace: exception.backtrace }
    save!
  end

  def cancel!
    if waiting?
      self.status = 'canceled'
      save!
    else
      raise 'You may only `cancel!` a deletion in the "waiting" state'
    end
  end

  # Lifecycle Auxiliary Methods

  def waiting?
    status == 'waiting'
  end

  def enqueued?
    status == 'enqueued'
  end

  def started?
    status == 'started'
  end

  def active?
    enqueued? || started?
  end

  def finished?
    status == 'finished'
  end

  def failed?
    status == 'failed'
  end

  def canceled?
    status == 'canceled'
  end

  # Reason For Deletion

  def for_cancellation?
    reason == 'canceled'
  end

  def for_shard_move?
    reason == 'moved'
  end
end
