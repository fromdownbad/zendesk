require_relative 'trial_limit_observer'

module TrialLimit
  extend ActiveSupport::Concern

  included do
    cattr_accessor :trial_limits
    self.trial_limits = []
  end

  class << self
    def redis_client
      Zendesk::RedisStore.client
    end

    def statsd_client_trial_limit
      Zendesk::StatsD::Client.new(namespace: 'trial_limit')
    end

    def reset_limit_count(account, key)
      key = trial_limit_key(account, key)

      redis_client.del(key).to_i
    end

    def trial_limit_count(account, key)
      key = trial_limit_key(account, key)

      redis_client.get(key).to_i
    end

    def increment_trial_limit_count(account, key, duration)
      key = trial_limit_key(account, key)

      redis_client.multi do
        redis_client.incr(key)
        redis_client.expire(key, duration)
      end
    end

    def reached_trial_limit?(account, key, limit)
      has_trial_limit?(account, key) && trial_limit_count(account, key) >= limit
    end

    def has_trial_limit?(account, key) # rubocop:disable Naming/PredicateName
      key = "#{key}_limit_for_trialers".to_sym

      account.arturo_enabled?(key) && account.is_trial? && account.settings.send(key) &&
        !account.whitelisted? && !account.is_sandbox? && !account.sell_product?
    end

    private

    def trial_limit_key(account, key)
      "account_#{account.id}_trial_limit_#{key}"
    end
  end

  module ClassMethods
    def trial_limit(model, limit: nil, duration: nil)
      model_class = Account.reflect_on_association(model.to_s.pluralize.to_sym).klass

      raise 'Only models with association to account are currently supported' unless model_class.method_defined?(:account)

      trial_limits << model

      define_method("trial_account_#{model}_limit") do
        limit
      end

      model_class.class_eval do
        include TrialLimit::Observer unless self < TrialLimit::Observer

        validate "validate_trial_#{model}_limit".to_sym
        after_commit "update_trial_#{model}_count".to_sym, on: [:create, :update]

        observe_trial_limit model

        define_method("validate_trial_#{model}_limit") do
          if TrialLimit.reached_trial_limit?(account, model, limit)
            errors.add(:value, I18n.t("txt.error_message.models.#{model}_limit_v3"))
            TrialLimit.statsd_client_trial_limit.increment('suspended', tags: ["subdomain:#{account.subdomain}"])
          end
        end

        define_method("update_trial_#{model}_count") do
          return unless increase_model_count_on_commit?
          if TrialLimit.has_trial_limit?(account, model)
            TrialLimit.increment_trial_limit_count(account, model, duration)
          end
        end

        def increase_model_count_on_commit?
          # Originally this arturo was intended to be used solely for the `update` operation.  However, keeping the
          # `create` and `update` operations separate led to a bug where only the `update` callback was used.  We are
          # now repurposing this arturo to control incrementing the model count after commit of either `update` or
          # `create` operations
          account.has_orca_classic_increase_model_count_on_update?
        end
      end
    end
  end
end
