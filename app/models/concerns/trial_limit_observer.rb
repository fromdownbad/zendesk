module TrialLimit
  module Observer
    extend ActiveSupport::Concern

    module ClassMethods
      def observe_trial_limit(model, limit: nil)
        define_method("send_trial_#{model}_limit_notice") do
          send_trial_limit_notification(model, limit)
        end

        define_method("disable_account_for_exceeding_#{model}_limit") do
          disable_account_for_exceeding_trial_limit(model)
        end

        class_eval do
          after_rollback "send_trial_#{model}_limit_notice".to_sym
          after_rollback "disable_account_for_exceeding_#{model}_limit".to_sym
        end
      end
    end

    def send_trial_limit_notification(model, limit)
      return unless errors.full_messages.join.include?(I18n.t("txt.error_message.models.#{model}_limit_v3"))

      key = "#{model}_notice"

      return unless TrialLimit.trial_limit_count(account, key).zero?

      TrialLimit.increment_trial_limit_count(account, key, 1.day.to_i)

      account.admins.each do |recipient|
        TrialLimitMailer.deliver_trial_limit_exceeded_notice(recipient, model, limit)
      end
    end

    def enqueue_perform_fraud_action(account, source_event)
      FraudScoreJob.enqueue_account(account, source_event)
    end

    def disable_account_for_exceeding_trial_limit(model)
      return unless account.is_serviceable
      return unless errors.full_messages.join.include?(I18n.t("txt.error_message.models.#{model}_limit_v3"))

      enqueue_perform_fraud_action(account, Fraud::SourceEvent::USER_IDENTITY_TRIAL_LIMIT_EXCEEDED)

      if model.to_s == 'user_identity'
        CIA::Event.create!(
          account: account,
          actor: CIA.current_actor || User.system,
          source: account,
          action: 'update',
          visible: false,
          message: I18n.t('txt.audit_message.user_identity_limit.message')
        )
      end
    end
  end
end
