# This module is expected to be added to a ticket
module TdeWorkspace
  extend ActiveSupport::Concern

  included do
    has_many :ticket_workspaces, inherit: :account_id
    has_many :workspaces, through: :ticket_workspaces

    after_commit :enqueue_workspace_assignment, if: proc { |ticket| ticket.account.has_contextual_workspaces? && !ticket.account.has_contextual_workspaces_in_js? }
  end

  def workspace
    ActiveRecord::Base.on_slave do
      account.workspaces.joins(:ticket_workspaces).where(ticket_workspaces: { account_id: account_id, ticket_id: id }).last
    end
  end

  def workspace=(new_workspace_or_workspace_id)
    ActiveRecord::Base.transaction do
      workspace_id = if new_workspace_or_workspace_id.is_a?(Workspace)
        new_workspace_or_workspace_id.id
      elsif new_workspace_or_workspace_id.is_a?(Integer)
        new_workspace_or_workspace_id
      end

      account.ticket_workspaces.where(ticket_id: id).delete_all
      return unless workspace_id

      ticket_workspace = account.ticket_workspaces.new
      ticket_workspace.ticket_id = id
      ticket_workspace.workspace_id = workspace_id
      ticket_workspace.save unless new_record?

      statsd_client = Zendesk::StatsD::Client.new(namespace: 'ticket.contextual_workspaces')
      statsd_client.increment("assigned")
    end
  end

  def enqueue_workspace_assignment
    TdeWorkspaces::AssignWorkspaceJob.enqueue(account_id: account.id, ticket_id: id)
  end

  def reassign_workspace
    # Selecting workspaces where definition is not nil. Workspaces created before the definition column was added do not have a definition"
    sorted_workspaces = account.workspaces.active.order(:position).select(&:definition)
    self.workspace = sorted_workspaces.find do |workspace_record|
      begin
        Zendesk::Rules::Match.match?(workspace_record, self)
      rescue StandardError => e
        ZendeskExceptions::Logger.record(e, location: __LINE__, message: "Reassign Workspace failed", fingerprint: 'reassign_workspace_failed', metadata: { account: account.id, ticket: id, workspace: workspace&.id })
      end
    end
  end

  def add_contextual_workspace_audit_event(workspace_attributes = {})
    ws_event = WorkspaceChanged.new
    type = workspace_attributes&.delete(:type)

    workspace_attributes = OpenStruct.new(workspace_attributes)

    begin
      case type
      when 'ADD'
        ws_event.add_new_workspace_audit!(workspace_attributes)
      when 'CHANGE'
        ws_event.add_change_workspace_audit!(workspace_attributes)
      when 'DELETE'
        ws_event.add_delete_workspace_audit!(workspace_attributes)
      else
        return
      end
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: __LINE__, message: "Error while creating WorkspaceChanged Audit Event", fingerprint: "workspace_changed_audit_failed", metadata: { account: account_id, ticket: ticket&.id })
      return
    end

    # audit comes from tickets/initializer
    audit.events << ws_event if audit && ws_event.valid?
  end
end
