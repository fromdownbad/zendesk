require Rails.root.join('lib/zendesk/accounts/client.rb')

module ChatPhaseFour
  extend ActiveSupport::Concern

  included do
    validate :ensure_not_phase_four, on: :create
  end

  private

  def ensure_not_phase_four
    return if phase == 3

    begin
      chat_product = account_client.product!(Zendesk::Accounts::Client::CHAT_PRODUCT)
      if chat_product.present? && chat_product.plan_settings['phase'] == 4
        errors.add(:base, "Cannot create for a phase 4 account.")
      end
    rescue Kragle::ResourceNotFound
      nil
    rescue Kragle::ResponseError, Faraday::Error => e
      errors.add(:base, "Cannot create, unable to fetch chat product record: #{e.message}.")
    end
  end

  def account_client
    @account_client ||= Zendesk::Accounts::Client.new(account)
  end
end
