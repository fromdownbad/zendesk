module PrecreatedAccountConversion
  extend ActiveSupport::Concern

  included do
    unless Accounts::Precreation::CLASSES_TO_BE_UPDATED.include?(name)
      raise "#{name} needs to be added to Accounts::Precreation::CLASSES_TO_BE_UPDATED"
    end

    define_callbacks :account_convert

    after_account_convert :update_timestamps_after_account_conversion
    after_account_convert :publish_entity!
  end

  module ClassMethods
    private

    def before_account_convert(*args, &block)
      set_callback :account_convert, :before, *args, &block
    end

    def after_account_convert(*args, &block)
      set_callback :account_convert, :after, *args, &block
    end
  end

  private

  def update_timestamps_after_account_conversion
    # Don't use update_attribute, b/c this would trigger
    # callbacks on associated objects.

    update_timestamps_for_attributes.map do |k, v|
      next unless respond_to?(k.to_sym)
      update_column(k, v)
    end
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Could not update timestamps after account conversion.", fingerprint: '93a4448eb71df76ba6620b47d9b530968cee5199')
    # Not throwing e to let conversion complete.
  end

  def publish_entity!
    return unless is_a?(Ticket)

    publisher.publish(self)
  end

  # Override this method to add other attributes
  def update_timestamps_for_attributes
    return {} if readonly?
    {
      created_at: account.created_at,
      updated_at: Time.now
    }
  end

  def publisher
    @publisher ||= ViewsObservers::EntityPublisher.new(
      ViewsObservers::TicketObserver::TOPIC,
      'views_entity_stream',
      ViewsEncoders::ViewsTicketProtobufEncoder,
      ViewsObservers::TruthyMatcher
    )
  end
end
