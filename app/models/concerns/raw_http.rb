module RawHttp
  extend ActiveSupport::Concern

  included do
    before_create :set_raw_http_request_and_response
  end

  private

  def set_raw_http_request_and_response
    self.raw_request = sanitize_url_reply(raw_http_request)
    self.raw_response = sanitize_url_reply(raw_http_response)
  end

  def raw_http_response
    "#{raw_http_capture.transactions.last.response.headers.try(:to_utf8!)}\r\n\r\n#{response_body.to_s.to_utf8!}"
  end

  def raw_http_request
    raw_request = raw_http_capture.transactions.last.request.raw
    if account.has_targets_remove_authorization_header?
      raw_request.split("\r\n").reject do |h|
        h.include?("Authorization")
      end.join("\r\n").to_utf8!
    else
      raw_request.to_utf8!
    end
  end

  def sanitize_url_reply(value)
    value.sanitize.presence
  end
end
