require Rails.root.join('lib/zendesk/accounts/client.rb')

module ChatPhaseThree
  extend ActiveSupport::Concern
  attr_accessor :skip_chat_product_sync, :skip_chat_only_permission_set

  included do
    before_update :sync_chat_product_record,
      if: ->(zopim_subscription) { zopim_subscription.phase_three? && !skip_chat_product_sync }

    before_destroy :cancel_chat_product_record,
      if: ->(zopim_subscription) { zopim_subscription.phase_three? && !skip_chat_product_sync }
  end

  def make_phase_three
    account_client.update_or_create_product!('chat', { product: updated_chat_product(3) }, context: "cp3_make_p3")
  end

  private

  def sync_chat_product_record
    account_client.update_or_create_product!('chat', { product: updated_chat_product }, context: "cp3_sync_record")
  rescue Kragle::ClientError, Kragle::ResponseError, Faraday::ClientError => e
    raise_error("Cannot update your chat subscription", e)
  end

  def cancel_chat_product_record
    account_client.update_or_create_product!('chat', { product: { state: Zendesk::Accounts::Product::CANCELLED.to_s } }, context: "cp3_cancel_record")
  rescue Kragle::ClientError, Kragle::ResponseError, Faraday::ClientError => e
    raise_error("Cannot delete your chat subscription:", e)
  end

  def raise_error(message_prefix, error)
    message = "#{message_prefix}: #{error.message}"
    raise ActiveRecord::RecordNotSaved, message
  end

  def updated_chat_product(phase = nil)
    additional_payload = phase ? { phase: phase } : {}

    {
      state: updated_state.to_s,
      plan_settings: updated_chat_plan_settings.merge(additional_payload)
    }
  end

  def updated_chat_plan_settings
    {
      plan_type: look_up_plan_type(plan_type),
      max_agents: max_agents
    }
  end

  # zopim_subscription                chat product record
  #   plan_type (string)   ------->     plan_type (int)
  #   max_agents (int)     ------->     max_agents (int)
  #   status (string)      ------->     active (boolean)
  #      - 'active'        ------->       true
  #      - 'cancelled'     ------->       false

  # chat_product_record.state =
  #   'cancelled'      if status == 'cancelled'
  #   'free'           if status == 'active' && plan_type == 1
  #   'trial'          if status == 'active' && purchased_at.nil?
  #   'subscribed'     if status == 'active' && !purchased_at.nil? && plan_type != 1
  def updated_state
    if status == ZBC::Zopim::SubscriptionStatus::Cancelled
      Zendesk::Accounts::Product::CANCELLED
    elsif purchased_at.present?
      if plan_type == ZBC::Zopim::PlanType::Trial.name
        Rails.logger.warn("Zopim subscription purchased on trial plan. This should not happen.")
      end

      Zendesk::Accounts::Product::SUBSCRIBED
    elsif plan_type == ZBC::Zopim::PlanType::Lite.name
      Zendesk::Accounts::Product::FREE
    else
      if plan_type != ZBC::Zopim::PlanType::Trial.name
        Rails.logger.warn("Zopim subscription not purchased, but on a non-trial/lite plan. This should not happen.")
      end

      Zendesk::Accounts::Product::TRIAL
    end
  end

  def look_up_plan_type(plan_type)
    ZBC::Zopim::PlanType::TYPES.find { |p| p.name == plan_type }.try(:plan_type)
  end

  def account_client
    @account_client ||= Zendesk::Accounts::Client.new(
      account,
      retry_options: {
        max: 3,
        interval: 2,
        exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
        methods: Faraday::Request::Retry::IDEMPOTENT_METHODS + [:patch]
      },
      timeout: 10
    )
  end

  # Overides the parent enable_chat_permission_set in order to disable it in the case of Suite trialers
  def enable_chat_permission_set
    return if skip_chat_only_permission_set
    super
  end
end
