class InboundEmail < ActiveRecord::Base
  prepend Zendesk::InboundMail::Truncation::InboundEmail

  FAILURE_DURATION = 15.minutes

  class_attribute :categories, :category_ids, :category_names, :statuses, :expiration

  self.categories   = SuspensionType.name_ids.merge(ham: 1000).symbolize_keys!
  self.category_ids = Set[*categories.values]
  self.statuses     = Set['rejected']
  self.expiration   = 1.month
  self.category_names = categories.invert

  belongs_to :account
  belongs_to :ticket
  belongs_to :comment

  attr_accessible :from, :message_id, :account, :ticket, :category_id, :comment
  attr_reader :status

  truncate_attributes_to_limit :from, :message_id

  validates_presence_of :account_id
  validates_inclusion_of :category_id,  in: category_ids, allow_blank: true
  validates_inclusion_of :status,       in: statuses,     allow_blank: true

  def self.cleanup
    while ActiveRecord::Base.connection.exec_delete("DELETE FROM inbound_emails WHERE created_at < #{ActiveRecord::Base.connection.quote(expiration.ago.to_s(:db))} LIMIT 1000", "InboundEventCleanup", []) >= 1
      sleep_to_slave_lag
    end
  end

  def self.sleep_to_slave_lag
    @slave_delay ||= ZendeskDatabaseSupport::SlaveDelay.new
    slave_lag = (@slave_delay.max_slave_delay(ActiveRecord::Base.current_shard_id) || 0)**1.8
    sleep slave_lag
  end

  def failed?
    category_id.nil? && created_at < FAILURE_DURATION.ago
  end

  def mark_by_system(category_id)
    self.category_id = category_id
  end

  def mark_by_system!(category_id)
    mark_by_system(category_id)
    save!
  end

  def mark_as_ham!
    update_attributes!(category_id: categories[:ham])
  end

  def mark_as_spam!
    update_attributes!(category_id: categories[:spam])
  end

  def spam?
    category_id == categories[:spam]
  end

  def ham?
    category_id == categories[:ham]
  end

  def reject
    @status = 'rejected'
  end

  def category_name
    (category_names[category_id] || :unknown).to_s
  end

  def references=(refs)
    # pick valid values
    valid = 6..references_limit
    refs = (refs || []).select { |r| valid.include?(r.size) }.uniq.join(" ")

    # we always want the origin
    origin, others = refs.split(" ", 2)

    # add max references from the right (newest)
    if others
      others.prepend " "
      start = [others.size, references_limit - origin.size].min # over length -> nil
      others = others[-start..-1].split(/\s+/, 2)[1]
      origin << " " << others if others
    end

    super(origin)
  end

  def references
    super.to_s.split(" ")
  end

  protected

  def references_limit
    @@references_limit ||= self.class.columns.detect { |c| c.name == "references" }.limit
  end
end
