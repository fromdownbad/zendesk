class JetpackTask < ActiveRecord::Base
  class UnknownStatusError < StandardError
  end

  include CachingObserver

  STATUS = {
    'new'       => 1,
    'started'   => 2,
    'skipped'   => 3,
    'complete'  => 4
  }.freeze

  belongs_to :account

  validates_presence_of :account_id

  attr_accessor :section, :enabled, :order

  attr_accessible :account, :owner, :status, :key, :order, :section, :enabled

  scope :for_owner, ->(owner) { where(owner_type: owner.class.sti_name, owner_id: owner.id) }

  def status=(value)
    if STATUS.key? value
      self.status_id = STATUS.fetch(value.to_s)
    else
      raise(UnknownStatusError, "unknown status: #{value}")
    end
  end

  def status
    STATUS.invert.fetch(status_id)
  end

  def owner=(value)
    write_attribute(:owner_id, value.id)
    write_attribute(:owner_type, value.class.sti_name)
  end

  private

  def status_id
    read_attribute(:status_id) || STATUS['new']
  end
end
