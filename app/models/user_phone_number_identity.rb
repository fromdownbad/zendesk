require 'zendesk/models/user_phone_number_identity'

class UserPhoneNumberIdentity < UserIdentity
  include Zendesk::Serialization::UserPhoneNumberIdentitySerialization
  include Voice::Core::NumberSupport

  has_one :user_phone_extension, dependent: :destroy, inherit: :account_id
  has_one :sms_capability, class_name: 'Voice::UserPhoneNumberSmsCapability', dependent: :destroy, inherit: :account_id

  before_validation :apply_prefix
  before_validation :normalize
  validate :uniqueness_of_value
  validates :value, 'users/phone_number': true, if: ->(i) { i.value_changed? }

  before_save :set_verified
  after_save do
    save_number_with_extension if user_phone_extension.present? && user_phone_extension.value_changed?
  end

  attr_accessor :skip_phone_number_validation

  def self.identity_type
    :phone_number
  end

  def extension
    if user_phone_extension.present? && !user_phone_extension.destroyed?
      return user_phone_extension.value
    end

    ''
  end

  def formatted_phone_number
    ::Voice::Core::NumberSupport::E164Number.formatted_number(value) + extension.to_s
  end

  def sms_capable?
    if sms_capability.try(:available?)
      true
    else
      false
    end
  end

  def update_sms_capability(capable:)
    cap = Voice::UserPhoneNumberSmsCapability.new
    cap.account = account
    cap.user = user
    cap.value = capable

    self.sms_capability = cap
    sms_capability.save!
  end

  private

  def uniqueness_of_value
    return unless value_changed?

    conditions = ["account_id = ? AND value = ? AND type = ?", account_id, value.to_s, self.class.name]
    unless new_record?
      conditions[0] << " AND id <> ?"
      conditions << id
    end

    if existing_identity = self.class.on_slave.where(conditions).first
      if user_id == existing_identity.user_id
        errors.add(:value, I18n.t('activerecord.errors.voice.user.phone.cannot_add_already_existing_direct_line', value: value))
      elsif existing_identity.user.nil?
        Rails.logger.warn("Orphaned Phone Number Identity found for Account: ##{account_id} Id: ##{id}")
        errors.add(:value, I18n.t('activerecord.errors.voice.user.phone.duplicate_direct_line_no_user', value: value))
      else
        errors.add(:value, I18n.t('activerecord.errors.voice.user.phone.duplicate_direct_line', user_id: existing_identity.user.id, user_name: existing_identity.user.name, value: value))
      end
    end
  end

  def set_verified
    self.is_verified = true
  end

  def apply_prefix
    return unless value_changed? && account.has_apply_prefix_and_normalize_phone_numbers?

    add_default_international_prefix
  end

  def normalize
    if value_changed? && !destroyed? && value.present? &&
      (account.is_end_user_phone_number_validation_enabled? || account.has_apply_prefix_and_normalize_phone_numbers?)

      return value if UserVoiceForwardingIdentity.new(value: value).emergency_number?

      normalized_number = Voice::Core::NumberSupport::E164Number.internal(value, extension: true)
      extension = normalized_number.to_s.match(/\S+(x\s*\d+)$/).try(:captures).try(:first)
      if extension.present?
        # TODO: This seems to be a bug where instances of UserPhoneExtension are created with user_id: nil
        user_phone_extension ||= build_user_phone_extension(account: account)
        user_phone_extension.value = extension

        normalized_number[extension] = ''
      end

      self.value = normalized_number
    end
  end

  def add_default_international_prefix
    return if destroyed?

    self.value = Voice::Core::NumberSupport.add_default_international_prefix(value)
  end

  def save_number_with_extension
    user_phone_extension.save
  end
end
