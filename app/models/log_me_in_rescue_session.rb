class LogMeInRescueSession < ActiveRecord::Base
  has_many :linkings, as: :external_link
  serialize :data
  belongs_to :account

  attr_accessible :session_id, :data, :account

  validates_presence_of :session_id, :account_id

  EXTERNALLY_VISIBLE_PARAMS = [:session_id, :data].freeze

  def self.find_from_params(params)
    normalized_params = params.symbolize_keys

    find_by_session_id(normalized_params[:session_id])
  end

  def self.find_by_session_id(session_id)
    where(session_id: session_id.to_s).first
  end

  def ticket_callback(ticket)
    user = User.system
    via_type = ViaType.LOGMEIN_RESCUE

    if data[:comment_hash].present?
      data[:comment_hash][:body] ||= data[:comment_hash].delete(:value)
      ticket.add_comment(data[:comment_hash])
    end

    ticket.will_be_saved_by(user, via_id: via_type)
    ticket.audit.events << ticket.comment
    transcript = data[:logmein_rescue_transcript]
    transcript = LogMeInTranscript.new(transcript)
    ticket.audit.events << transcript
    ticket.additional_tags = data[:tags]
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Error writing LogMeIn Rescue session to ticket", fingerprint: 'e02ebbb8676ca302c2c16c5476acde35309e7839')
  end
end
