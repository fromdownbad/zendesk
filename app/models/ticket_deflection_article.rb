class TicketDeflectionArticle < ActiveRecord::Base
  REASON_UNKNOWN = 0
  REASON_IRRELEVANT = 1
  REASON_RELATED_NOT_ANSWERED = 2

  REASON_STRINGS = {
    REASON_UNKNOWN => 'unknown',
    REASON_IRRELEVANT => 'irrelevant',
    REASON_RELATED_NOT_ANSWERED => 'related_but_not_answered'
  }.freeze

  belongs_to :ticket_deflection, touch: true, inherit: :account_id
  belongs_to :account
  belongs_to :brand, inherit: :account_id

  ignore_column :ticket_id

  validates_numericality_of :score
  validates_presence_of :ticket_deflection, :account, :brand
  validates :article_id, uniqueness: { scope: :ticket_deflection_id, message: 'article_id must be unique per ticket_deflection_id.' }, on: :create

  before_save :add_auto_answer_viewed_event, if: proc { |article| article.changed.include? 'clicked_at' }

  class << self
    def build_from_deflection_and_article(deflection, article)
      new do |deflection_article|
        deflection_article.ticket_deflection = deflection
        deflection_article.brand_id = deflection.brand_id
        deflection_article.account_id = deflection.account_id
        deflection_article.article_id = article.fetch('article_id')
        deflection_article.score = article.fetch('score')
        deflection_article.locale = article.fetch('locale')
        deflection_article.clicked_at = article.fetch('clicked_at', nil)
        deflection_article.irrelevant_by_end_user = false
        deflection_article.irrelevant_reason_by_end_user = REASON_UNKNOWN
        deflection_article.irrelevant_by_agent = false
        deflection_article.irrelevant_reason_by_agent = REASON_UNKNOWN
      end
    end

    def locale(deflection, article_id)
      deflection_articles = deflection.ticket_deflection_articles.where(article_id: article_id)
      article_locale = deflection_articles.first.locale unless deflection_articles.empty?
      article_locale
    end

    def reject_by_end_user(deflection, article_id, reason)
      reject_reason = reason || TicketDeflectionArticle::REASON_UNKNOWN

      exception = ArgumentError.new("Unknown feedback reason: #{reject_reason}")
      raise exception unless REASON_STRINGS.key? reject_reason

      article = deflection.ticket_deflection_articles.where(article_id: article_id).first
      if article
        article.update_attributes(
          irrelevant_by_end_user: true,
          irrelevant_reason_by_end_user: reject_reason,
          irrelevant_by_end_user_updated_at: Time.now
        )
        article.add_auto_answer_reject_event(true, deflection.user_id, deflection.user.try(&:name), reject_reason)
        event_key = "mark_suggested_article_irrelevant"
      else
        event_key = "mark_unsuggested_article_irrelevant"
      end
      statsd_client.increment(event_key, tags: ["subdomain:#{deflection.account.subdomain}", "by:end_user", "reason:#{REASON_STRINGS[reject_reason]}"])
    end

    def reject_by_agent(deflection, article_id, irrelevant, user_id, user_name)
      article = deflection.ticket_deflection_articles.where(article_id: article_id).first
      reason = irrelevant ? REASON_IRRELEVANT : REASON_UNKNOWN
      if article.present?
        article.update_attributes(
          irrelevant_by_agent: irrelevant,
          irrelevant_reason_by_agent: reason,
          irrelevant_by_agent_updated_at: Time.now
        )
        article.add_auto_answer_reject_event(irrelevant, user_id, user_name)
        event_key = "mark_suggested_article_irrelevant"
      else
        event_key = "mark_unsuggested_article_irrelevant"
      end

      if reason == REASON_UNKNOWN
        event_key = event_key.prepend("un")
      end
      statsd_client.increment(event_key, tags: ["subdomain:#{deflection.account.subdomain}", "by:agent", "reason:#{REASON_STRINGS[reason]}"])
    end

    private

    def statsd_client
      Zendesk::StatsD::Client.new(namespace: ['ticket_deflection'])
    end
  end

  def update_clicked_at(clicked_time)
    update_attributes(clicked_at: clicked_time)
  end

  def update_solved_article(solved_time)
    update_attributes(solved_at: solved_time)
  end

  def add_auto_answer_reject_event(irrelevant_value, user_id, user_name, reason = '')
    ticket = ticket_deflection.ticket

    return if ticket.nil? || ticket.closed? || !user_id

    # use the details in the automatic answer send event to load the reject event
    aa_send_event = AutomaticAnswerSend.where(ticket_id: ticket.id).first
    article_detail = aa_send_event.suggested_articles.find { |article| article[:id] == article_id } if aa_send_event

    if article_detail
      ticket.will_be_saved_by(User.system)

      ticket.audit.events << ::AnswerBot::EventCreator.reject_event(
        article: article_detail.slice(:id, :html_url, :url, :title),
        reviewer_id: user_id,
        reviewer_name: user_name,
        irrelevant: irrelevant_value,
        reason: reason
      )

      # https://zendesk.atlassian.net/browse/AI-5584
      # disable triggers is set to true to stop conversation stream from flashing and also to hide an unncessary ticket updated message
      # when an agent marks an answerbot article as off-topic.
      ticket.audit.disable_triggers = true

      ticket.save!
    end
  end

  def add_auto_answer_viewed_event
    ticket = ticket_deflection.ticket

    return if ticket.nil? || ticket.blank?
    return if ticket.closed? || already_viewed_by?(ticket_deflection.user_id)

    # use the details in the automatic answer send event to load the viewed event
    aa_send_event = AutomaticAnswerSend.where(ticket_id: ticket.id).first
    article_detail = aa_send_event.suggested_articles.find { |article| article[:id] == article_id } if aa_send_event

    if article_detail
      aa_view_event = AutomaticAnswerViewed.new
      aa_view_event.article = article_detail.slice(:id, :html_url, :url, :title)
      aa_view_event.viewer_id = ticket_deflection.user_id

      ticket.will_be_saved_by(User.system)
      ticket.audit.events << aa_view_event
      ticket.save!
    end
  end

  def already_viewed_by?(user_id)
    AutomaticAnswerViewed.where(ticket_id: ticket_deflection.ticket_id).all.any? { |event| event.viewer_id == user_id && event.article[:id] == article_id }
  end

  def suppressed?
    return unless clicked_at.present?
    clicked_at < created_at
  end

  def creation_to_clicked_time_delta
    return 0 if clicked_at.nil?

    clicked_at.to_i - ticket_deflection.created_at.to_i
  end

  def ticket
    ticket_deflection.ticket
  end

  def ticket_id
    ticket_deflection.ticket_id
  end
end
