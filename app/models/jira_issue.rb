class JiraIssue < ActiveRecord::Base
  has_many :linkings, as: :external_link
  belongs_to :account

  attr_accessible :account, :issue_id

  validates_presence_of :issue_id, :account_id

  EXTERNALLY_VISIBLE_PARAMS = [:issue_id].freeze

  def self.find_from_params(params)
    normalized_params = params.symbolize_keys

    find_by_issue_id(normalized_params[:issue_id])
  end

  def ticket_callback(ticket)
  end
end
