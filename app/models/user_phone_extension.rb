class UserPhoneExtension < UserPhoneAttribute
  after_initialize :init

  def init
    self.attribute_type ||= UserPhoneAttribute::TYPE[:extension]
  end
end
