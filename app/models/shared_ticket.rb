require 'ticket_sharing/ticket'
require 'sharing/foreign_user_support'

class SharedTicket < ActiveRecord::Base
  include Sharing::ForeignUserSupport
  include ArSkippedCallbackMetrics::Instrument
  extend ZendeskArchive::AssociationLookaside

  belongs_to :account
  belongs_to :agreement, class_name: 'Sharing::Agreement', inverse_of: :shared_tickets, inherit: :account_id
  belongs_to :ticket, inverse_of: :shared_tickets, inherit: :account_id
  association_accesses_archive :ticket

  attr_accessible :account, :agreement, :agreement_id, :custom_fields, :ticket, :uuid, :original_id, :ticket_id

  before_validation :set_account_from_ticket_or_agreement
  before_validation :generate_uuid, on: :create

  validates_presence_of :account_id
  validates_presence_of :agreement_id
  validates_uniqueness_of :ticket_id, scope: [:agreement_id, :deleted_at]
  validates_uniqueness_of :uuid, allow_nil: true, scope: [:agreement_id, :deleted_at]

  after_commit :queue_partner_create, on: :create
  after_commit :enqueue_unshare, if: :deleted?, on: :update

  serialize :custom_fields
  has_soft_deletion default_scope: true

  before_soft_delete :validate_unshare

  attr_reader :error_message

  def self.find_by_uuid(uuid)
    where(uuid: uuid.to_s).first
  end

  DESERIALIZED_STATUS_CODES = {
    'new'     => StatusType.NEW,
    'open'    => StatusType.OPEN,
    'hold'    => StatusType.OPEN,
    'pending' => StatusType.PENDING,
    'solved'  => StatusType.SOLVED,
    'closed'  => StatusType.CLOSED
  }.freeze

  SERIALIZED_STATUS_TYPES = {
    StatusType.NEW     => 'new',
    StatusType.OPEN    => 'open',
    StatusType.PENDING => 'pending',
    StatusType.HOLD    => 'open', # Status hold ticket are reported as open.
    StatusType.SOLVED  => 'solved',
    StatusType.CLOSED  => 'closed'
  }.freeze

  def from_partner(ts_ticket)
    if ticket.nil?
      unless ticket_id.nil?
        @error_message = "Unable to retrieve ticket #{ticket_id}"
        return false
      end

      creating_ticket = true
      set_account_from_ticket_or_agreement
      current_user = find_or_build_foreign_user(ts_ticket.requester)
      self.uuid = ts_ticket.uuid
      self.original_id = ts_ticket.original_id
      self.ticket = account.tickets.build(
        created_at: ts_ticket.requested_at.try(:to_time),
        requester: current_user,
        description: ts_ticket.comments.first.try(:body) || ''
      )
    else
      if ticket.closed?
        @error_message = "Ticket is closed"
        return false
      end
    end

    store_subject(ts_ticket)
    store_tags(ts_ticket)
    store_status(ts_ticket)
    store_custom_fields(ts_ticket)

    reject_public = agreement.out? && !agreement.allows_public_comments?
    existing_comment_uuids = ticket.comments.map { |comment| comment.shared_comment.try(:uuid) }.compact
    ts_ticket.comments.reject! { |comment| (reject_public && comment.public?) || existing_comment_uuids.include?(comment.uuid) }
    # if we get a ticket with no new valid comments load up array with a "blank" comment so we still save ticket data
    ts_ticket.comments << nil if ts_ticket.comments.empty?

    Prop.disabled do
      ts_ticket.comments.each do |ts_comment|
        # Ticket#description= has a side effect of creating a new first comment
        # by setting Ticket#comment= explicitly we have control over the created_at date.
        comment = add_new_comment(ts_comment)
        current_user = comment.try(:author) || find_or_build_foreign_user(ts_ticket.current_actor) || User.system
        unless ticket.will_be_saved_by(current_user, via_id: ViaType.TICKET_SHARING, via_reference_id: agreement.id)
          @error_message = "Unable to create audit for #{uuid}"
          return false
        end

        inject_screencasts_in_metadata_if_needed(ts_comment)
        if creating_ticket
          ticket.audit.ticket_sharing_create = true

          # Insert a ticket sharing object on the last added comment's audit
          if ts_comment == ts_ticket.comments.last
            sharing_event = TicketSharingEvent.new(agreement_id: agreement.id)
            sharing_event.account_id = account&.id
            sharing_event.skip_shared_ticket_create = true
            ticket.audit.events << sharing_event
          end
        end

        unless ticket.save
          @error_message = "Unable to create ticket for #{uuid}, #{ticket.errors.full_messages.join('; ')}"
          return false
        end

        next unless new_record?
        unless save
          @error_message = "Unable to create shared_ticket for ticket #{ticket.id}"
          return false
        end
      end
    end
  end

  def send_to_partner
    handle_network_errors do |ts_ticket|
      response_code = ts_ticket.send_to(agreement.remote_url)
      TicketSharing.handle_response(self, :send_to_partner, ts_ticket.response)
      response_code
    end
  end

  def update_partner
    handle_network_errors do |ts_ticket|
      ts_ticket.current_actor = ticket.audits.latest.author.for_partner

      response_code = ts_ticket.update_partner(agreement.remote_url)
      TicketSharing.handle_response(self, :update_partner, ts_ticket.response)

      if response_code == 410
        agreement.system_status_change(:inactive)
        agreement.save!
      end

      response_code
    end
  end

  def enqueue_unshare
    enqueue_job(:unshare)
  end

  def unshare
    handle_network_errors do |ts_ticket|
      response_code = ts_ticket.unshare(safe_agreement.remote_url)
      TicketSharing.handle_response(self, :unshare, ts_ticket.response)
      if deleted_at?
        Rails.logger.info("#{Time.now} - shared_ticket #{id} has already been deleted #{deleted_at}")
      else
        soft_delete!
      end
      response_code
    end
  end

  def for_partner
    return unless Ticket.with_deleted { ticket }

    setup_shared_comments

    ticket_type = account.try(:has_ticket_sharing_whitelist?) ? TicketSharing::NonVerifyTicket : TicketSharing::Ticket

    ts_ticket = ticket_type.new(
      'uuid' => uuid,
      'subject' => ticket.subject,
      'requested_at' => ticket.created_at.in_time_zone,
      'original_id' => ticket.nice_id,
      'custom_fields' => custom_fields
    )

    ts_ticket.agreement = safe_agreement.for_partner
    ts_ticket.requester = ticket.requester.for_partner
    ts_ticket.comments = comments_for_partner.map do |comment|
      comment.shared_comment.for_partner
    end

    if safe_agreement.sync_tags?
      ts_ticket.tags = ticket.tag_array
    end

    if safe_agreement.allows_public_comments?
      ts_ticket.status = SERIALIZED_STATUS_TYPES[ticket.status_id]
    end

    if safe_agreement.sync_custom_fields?
      ts_ticket.custom_fields ||= {}
      ts_ticket.custom_fields["zd_custom_fields"] = Hash[custom_fields_for_sync]
    end

    ts_ticket
  end

  def custom_fields_for_sync
    account.ticket_fields.active.custom.select { |c| custom_field_eligible_for_sync?(c) }.map { |c| [c.title, c.value(ticket)] }
  end

  def agreement_name
    if a = agreement
      return a.name
    end

    a_id = agreement_id
    a = Sharing::Agreement.with_deleted do
      Sharing::Agreement.find_by_id(a_id)
    end

    [a.try(:name), '(deactivated)'].compact.join(' ')
  end

  def safe_agreement
    agreement ||
      Sharing::Agreement.with_deleted { Sharing::Agreement.find_by_id(agreement_id) }
  end

  def allows_comment_visibility_toggle?
    return unless safe_agreement
    safe_agreement.out? || safe_agreement.allows_public_comments?
  end

  def retrieve_jira_ticket
    url = safe_agreement.remote_url + "/tickets/" + uuid
    headers = { "X-Ticket-Sharing-Token" => safe_agreement.authentication_token, "Accept" => "json" }
    Zendesk::Net::MultiSSL.get_json(url, headers: headers, timeout: 5)
  end

  def enqueue_job(method)
    return unless safe_agreement.accepted?

    Sharing::TicketJob.enqueue(account.id, method, id)
  end

  private

  def custom_field_eligible_for_sync?(custom_field)
    !custom_field.is_a?(FieldPartialCreditCard)
  end

  def validate_unshare
    return false unless deleted_at.nil?
  end

  def handle_network_errors
    return 499 unless ts_ticket = for_partner
    safe_agreement.retry_on_internal_error { yield ts_ticket }
  rescue FaradayMiddleware::RedirectLimitReached
    agreement.record_config_error! unless safe_agreement.failed?
    404
  rescue Faraday::RestrictIPAddresses::AddressNotAllowed => e
    unless safe_agreement.failed?
      # added to mitigate resolution issues with [subdomain].zendesk.com domains. See ZD#1755584
      if agreement.with_zendesk_account? && !zendesk_closed?(e.message)
        add_failure_error(:dns)
      else
        agreement.record_config_error!
      end
    end
    404
  rescue SocketError, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNRESET, Faraday::TimeoutError, Net::OpenTimeout
    408
  rescue OpenSSL::SSL::SSLError, Faraday::SSLError
    if agreement.zendesk_http_remote_url?
      agreement.remote_url = agreement.remote_url.sub(/^http:/, "https:")
      if agreement.save!
        retry
      end
    end
    Rails.logger.info("#{agreement.account.id} #{agreement.account.subdomain} Ticket Sharing SSL error: Agreement #{agreement_id} Ticket #{ticket_id} with exception: #{$!} At #{Time.now}")
    safe_agreement.record_ssl_error! unless safe_agreement.failed?
    add_failure_error(:ssl)
    412
  rescue TicketSharing::InternalSharingWarning => e
    e.response_code
  rescue TicketSharing::InternalSharingError => e
    agreement.record_config_error! unless safe_agreement.failed?
    e.response_code
  end

  def setup_shared_comments
    ticket.comments.each do |comment|
      comment.create_shared_comment unless comment.shared_comment
    end
  end

  def add_new_comment(ts_comment)
    return unless ts_comment
    author = find_or_build_foreign_user(ts_comment.author)

    comment_options = {
      author: author,
      created_at: ts_comment.authored_at.try(:to_time),
      body: ts_comment.body,
      html_body: ts_comment.html_body,
      is_public: ts_comment.public?
    }

    if ts_comment.public?
      comment_options[:channel_back] = channel_back_for(ticket)
    end

    comment = ticket.add_comment(comment_options)

    comment.build_shared_comment(uuid: ts_comment.uuid)
    comment.build_shared_attachments(account, ts_comment)

    comment
  end

  def channel_back_for(ticket)
    if ticket.via_twitter?
      ticket.via?(:twitter_dm) ? "dm" : "true"
    elsif ticket.via_facebook?
      true
    end
  end

  def inject_screencasts_in_metadata_if_needed(ts_comment)
    screencasts_from_ts_comment = Screencast.screencasts_from_ticket_sharing(ts_comment)
    if screencasts_from_ts_comment.any?
      ticket.inject_screencasts_in_metadata(screencasts_from_ts_comment)
    end
  end

  def queue_partner_create
    return unless agreement && agreement.out?
    enqueue_job(:send_to_partner)
  end

  def set_account_from_ticket_or_agreement
    self.account ||= ticket.try(:account) || agreement.try(:account)
  end

  def generate_uuid
    return if uuid.present?
    return if ticket_id.nil?

    previously_shared_ticket = ticket.shared_tickets.detect do |st|
      st != self && st.uuid.present?
    end

    self.uuid =
      if previously_shared_ticket
        previously_shared_ticket.uuid
      else
        Digest::SHA1.hexdigest("#{account.sharing_url}/tickets/#{ticket_id}")
      end
  end

  def store_subject(ts_ticket)
    return unless ts_ticket.subject.present?
    ticket.subject = ts_ticket.subject
  end

  def store_tags(ts_ticket)
    return unless agreement.sync_tags?
    tags = Array(ts_ticket.tags)
    # seems like this would prevent being able to completely clear tags on a ticket
    if tags.any?
      Rails.logger.info("#{account.id} #{account.subdomain} Ticket Sharing setting tags from: #{ticket.current_tags.inspect}, to: #{tags.inspect}")
      ticket.set_tags = tags
    end
  end

  def store_status(ts_ticket)
    ts_ticket_status_id = DESERIALIZED_STATUS_CODES[ts_ticket.status]
    return if !agreement.allows_public_comments? || (ts_ticket_status_id == StatusType.CLOSED && account.has_disable_closing_tickets_via_sharing?)
    ticket.force_status_change = true
    ticket.status_id = ts_ticket_status_id
  end

  def store_custom_fields(ts_ticket)
    return unless agreement.sync_custom_fields?
    return if ts_ticket.custom_fields.try(:[], 'zd_custom_fields').blank?

    ts_ticket.custom_fields['zd_custom_fields'].each do |title, value|
      if ticket_field = account.ticket_fields.active.custom.find_by_title(title)
        store_custom_field(ticket_field, value)
      end
    end
  end

  def store_custom_field(ticket_field, value)
    return unless ticket_field.assignable_value?(value)

    entry = ticket_field_entry(ticket_field)

    old_value = entry.value
    entry.value = value

    if entry.valid?
      ticket.additional_tags = value if ticket_field.is_a?(FieldTagger)
      entry.save(validate: false) unless ticket.new_record?
    else
      entry.value = old_value
      entry.ticket_id = nil if entry.new_record?
    end
  end

  def ticket_field_entry(ticket_field)
    ticket.ticket_field_entries.find_by_ticket_field_id_and_account_id(ticket_field.id, account.id) ||
      ticket.ticket_field_entries.build(ticket_field: ticket_field, account: account)
  end

  def comments_for_partner
    if safe_agreement.in? && !safe_agreement.allows_public_comments?
      ticket.comments.reject(&:is_public?)
    else
      ticket.comments
    end
  end

  def add_failure_error(type)
    statsd_client.increment('failure_error', tags: ["error_type:#{type}"])
    return if ticket.closed?

    ticket.will_be_saved_by(User.system)
    error_key =
      case type
      when :dns
        'ticket_fields.ticket_sharing.dns_failure'
      else
        'ticket_fields.ticket_sharing.ssl_failure'
      end
    ticket.add_translatable_error_event({key: error_key}, false)
    ticket.skip_reshare_ticket_if_necessary = true
    ticket.save!
  end

  def zendesk_closed?(exception_message)
    exception_message.include? "app/help-center-closed/"
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'ticket_sharing')
  end
end
