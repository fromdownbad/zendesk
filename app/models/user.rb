require 'zendesk/models/user'
require 'zendesk/o_auth/models/user'
require 'users/photo'
require 'users/signature'

class User < ActiveRecord::Base
  has_kasket
  has_kasket_on :id, :is_active
  has_kasket_on :id, :is_active, :account_id

  include UserObserver
  include UserEntityObserver
  include UserEventBusObserver
  include UserGuideEntitlementsChangesObserver
  include GooddataSyncObserver
  include Users::Roles
  include Users::Access
  include CIA::Auditable
  include Identifiable
  include ScopedCacheKeys
  include ServiceableAccount
  include Organizations::AccessByName
  include Users::Authentication
  include Users::TicketSharingSupport
  include TagManagement
  include CurrentTagsAccessor
  include Zendesk::Serialization::UserSerialization
  include Users::AgentDisplayName
  include Users::Photo
  include Users::CRM
  include Users::Deletion
  include Users::Forums
  include Users::Identification
  include Users::Liquid
  include Users::Localization
  include Users::Naming
  include Users::GroupMemberships
  include PrecreatedAccountConversion
  include Users::OrganizationMemberships
  include Users::Signature
  include Users::Tags
  include Users::Ticketing
  include Users::Suspension
  include Voice::Core::Support::User
  include Users::PhoneNumberBehavior
  include Voice::Core::NumberSupport
  include Satisfaction::CachedScore
  include CachingObserver
  include Users::RoleMapping
  include Users::SuiteAgentSupport
  include Users::ChatSupport
  include Users::ChatAgentSupport
  include ::Channels::Users::FacebookSupport
  include Users::CustomerLists
  include Users::Properties
  include Users::TwoFactorAuthentication
  include Users::VoiceSupport
  include Users::ZopimSupport
  include Users::OtherProductEntitlements
  include TrialLimit::Observer
  include Users::AnswerBot
  include Zendesk::Users::Identities::UserIdentityLimit
  include ArSkippedCallbackMetrics::Instrument

  extend  Zendesk::CappedCounts
  extend  Zendesk::DB::BulkUidAllocation

  require_dependency "users/deprecation"
  require_dependency "users/merge"
  require_dependency "users/password_changes"
  require_dependency "users/onboarding_support"

  include Users::OnboardingSupport

  cattr_reader :per_page
  @@per_page = API_DEFAULT_PER_PAGE

  UNKNOWN_NAME = "Unknown name".freeze

  attr_accessor :is_bulk_updated, :current_user, :twitter_screen_name, :ip_address,
    :new_twitter_identity, :verify_email_text, :verify_email_subject, :google_address, :facebook_id, :active_brand_id,
    :password_change_email, :reverse_sync

  attr_accessible :account, :agent_display_name, :alias, :details, :groups, :default_group_id, :is_moderator,
    :is_private_comments_only, :private_comments_only, :locale_id, :name, :notes, :organization_id,
    :organization_name, :organization, :phone, :remote_photo_url, :send_verify_email, :skip_verify_email, :tags,
    :current_tags, :set_tags, :remove_tags, :additional_tags, :time_zone, :twitter_screen_name,
    :uses_12_hour_clock, :voice_extension, :voice_number, :permission_set, :shared_phone_number

  # questionable but currently needed
  attr_accessible :facebook_id, :external_id, :email, :challenge, :is_verified, :restriction_id, :suspended,
    :new_twitter_identity, :skip_verification

  delegate :subscription, to: :account

  alias_attribute :private_comments_only, :is_private_comments_only
  has_many :compliance_deletion_statuses, inherit: :account_id
  has_many :satisfaction_ratings_received, class_name: 'Satisfaction::Rating', foreign_key: :agent_user_id, inherit: :account_id
  has_many :satisfaction_ratings_created, class_name: 'Satisfaction::Rating', foreign_key: :enduser_id, inherit: :account_id

  has_many :alert_dismissals, dependent: :destroy
  has_many :ipm_feature_notification_dismissals, dependent: :destroy, class_name: "Ipm::FeatureNotificationDismissal"
  has_many :ipm_alert_dismissals, dependent: :destroy, class_name: "Ipm::AlertDismissal"
  has_many :attachments, foreign_key: :author_id, dependent: :destroy
  has_many :activities, -> { order(created_at: :desc) }, inherit: :account_id
  has_many :shared_sessions, class_name: 'Zendesk::SharedSession::SessionRecord', dependent: :delete_all
  has_many :devices, dependent: :destroy, inherit: :account_id
  has_many :mobile_devices, class_name: 'MobileDevice', dependent: :destroy
  has_many :device_identifiers, class_name: "PushNotifications::DeviceIdentifier", dependent: :destroy
  has_many :bookmarks, class_name: "Ticket::Bookmark", inherit: :account_id, dependent: :destroy

  has_many :ticket_stats,
    -> { where(group_id: 0, organization_id: 0) },
    class_name: 'StatRollup::Ticket',
    foreign_key: :agent_id,
    inherit: :account_id

  has_many :custom_field_values, -> { includes(:field) }, class_name: 'CustomField::Value', as: :owner, autosave: true, inherit: :account_id, extend: Zendesk::CustomField::HasManyExtension
  has_many :custom_fields, through: :custom_field_values, source: :field

  has_many :fraud_reports

  has_many :user_seats

  has_many :skips

  has_one :user_phone_extension, inherit: :account_id

  has_many :agent_downgrade_audit

  scope :limited, -> { limit(30).order('users.name') }

  scope :in_account, ->(account_or_id) { where(account_id: account_or_id) }

  scope :active,   -> { where(is_active: true) }
  scope :inactive, -> { where(is_active: false) }
  scope :inactive_and_not_redacted, -> { inactive.joins(INACTIVE_BUT_NOT_ULTRA_DELETED_OUTER_JOIN).where(compliance_deletion_statuses: { user_id: nil }) }

  validates_uniqueness_of :external_id, case_sensitive: false, scope: [:account_id, :is_active], if: :external_id?
  validate                :account_is_serviceable, if: :account
  validate                :limit_sdk_identities, on: :update
  validate                :limit_identities, on: :update

  validates_lengths_from_database only: [:name, :external_id, :details, :notes]

  before_validation :clear_blank_external_id
  before_validation :switch_kyev_to_kyiv
  before_save       :cleanup_notes_and_details
  before_save       :check_actor_permissions
  after_update      :log_previous_updated_at
  after_save        :save_number_with_extension, if: ->(obj) { obj.user_phone_extension.present? && obj.user_phone_extension.value_changed? }
  before_destroy    :remove_number_extension, if: -> (i) { i.user_phone_extension.present? }

  # new visible attribute -> new translation under txt.admin.views.reports.tabs.audits.attribute.<attribute>
  audit_attribute :roles, :crypted_password, :permission_set_id, if: :is_audited_by_cia?

  def cia_changes
    ca = super.dup
    if account.has_central_admin_staff_mgmt_roles_tab? && account.has_audit_logs_staff_service_entitlements?
      ca.delete_if { |k, _| %w[roles permission_set_id].include?(k) }
    end

    ca
  end

  observe_trial_limit :user_identity

  def domain_events
    @domain_events ||= []
  end

  def add_domain_event(domain_event)
    return if domain_event.nil?
    raise ArgumentError, "Expected UserEvent, received #{domain_event.class}" unless domain_event.is_a?(::Zendesk::Protobuf::Support::Users::V2::UserEvent)
    domain_events << domain_event
  end

  def user_created?
    id_changed?
  end

  def abilities(reload = false)
    if @abilities.blank? || reload
      @abilities = Access::Authorization.new(self)
    end
    @abilities
  end
  delegate :can?, to: :abilities

  def cache_key_with_time
    if new_record?
      "#{cache_key}/#{Time.now.to_f}"
    else
      cache_key
    end
  end

  def cc_blocklisted?
    Zendesk::Auth::ListUtils.address_in_list?(email, account.cc_blacklist)
  end

  def device_notification?
    return false if is_end_user?

    record = settings.device_notification_record
    (record.new_record? && is_admin?) || settings.device_notification
  end

  def show_color_branding_tooltip?
    is_admin? && settings.show_color_branding_tooltip
  end

  # account.users.create_with_email 'foo@bar.com'
  # create_by_email is superceded by AR create_by_email when used on a scope
  def self.create_with_email(email, options = {})
    name = options[:name]
    name = UNKNOWN_NAME if name.blank? || name.strip.size <= 1

    create(name: name, email: email) do |user|
      user.send_verify_email = options[:send_verify_email] if options.key?(:send_verify_email)
    end
  end

  def primary_email_identity
    identities.find_by_type_and_priority('UserEmailIdentity', 1)
  end

  def previous_primary_email
    identities.find_by_type_and_priority('UserEmailIdentity', 2)
  end

  def emails_include?(address)
    # With .compact because email identities should always have emails but you never know
    user_emails = identities.select { |identity| identity.type == "UserEmailIdentity" }.map(&:value).compact
    user_emails.map(&:downcase).include?(address.downcase)
  end

  def dismissed_ipm_feature_notification_ids
    ipm_feature_notification_dismissals.map(&:ipm_feature_notification_id)
  end

  def dismissed_ipm_alert_ids
    ipm_alert_dismissals.map(&:ipm_alert_id)
  end

  def first_valid_token
    tokens = identities.first.verification_tokens
    tokens.detect(&:source) || tokens.create
  end

  def email_verification_path
    "/verification/email/#{first_valid_token.value}"
  end

  def send_password_change_email?
    !(password_change_email.to_s == "false")
  end

  def visible_bookmarks
    bookmarks.select &:visible_to_user?
  end

  def machine?
    !!email_identity_for_email_accessor.try(:machine?)
  end

  def user_handle
    if account.created_at >= DateTime.new(2016, 6, 1)
      "#{account.id}_#{id}"
    else
      "#{account.subdomain}_#{id}"
    end
  end

  def report_csv
    @report_csv ||= ImportExportJobPolicy.new(account, self).accessible_to_user?
  end

  def iana_time_zone
    ActiveSupport::TimeZone[time_zone].tzinfo.name
  end

  protected

  def clear_blank_external_id
    self.external_id = nil if external_id.blank?
  end

  def switch_kyev_to_kyiv
    if time_zone == 'Kyev' && account.has_switch_kyev_to_kyiv?
      self.time_zone = 'Kyiv'
      Rails.logger.info("Updated user time_zone to Kyiv instead of Kyev")
    end
  end

  def cleanup_notes_and_details
    notes.strip! unless notes.nil?
    details.strip! unless details.nil?
    true
  end

  def check_actor_permissions
    # don't enforce this check when outside of a controller.  for now.
    # This is to prevent lots of spurious warnings when creating users for unit tests.
    return unless actor = (CIA.current_transaction || {})[:effective_actor]

    action = if new_record?
      :create
    elsif is_active_was == true && is_active == false
      :delete
    else
      :edit
    end

    unless actor.can?(action, self)
      message = "Failed permission check on #{action}"
      errors.add(:base, message)
      Rails.logger.error(message)
    end
  end

  def log_previous_updated_at
    if changes["updated_at"] && changes["updated_at"][0]
      Rails.logger.info("user #{id} updated_at was #{changes["updated_at"][0].utc.to_s(:db)}")
    elsif changes["updated_at"]
      Rails.logger.info("user #{id} updated_at was nil")
    else
      Rails.logger.info("user #{id} updated_at was unchanged after an update... suspicious!")
    end
  end

  private

  def update_taggings?
    !!account.try(:has_user_and_organization_tags?)
  end

  def save_number_with_extension
    user_phone_extension.save
  end

  def remove_number_extension
    user_phone_extension.destroy
  end

  def limit_sdk_identities
    if identities.sdk.count >= account.settings.max_sdk_identities
      message = "sdk identities limit for user_id: #{id}, account_id: #{account.id} exceeded"
      errors.add(:base, message)
    end
  end

  def limit_identities
    enforce_identity_limit_for_user(self)
  end
end
