class ChatTranscript < ActiveRecord::Base
  store :value, accessors: [:chat_history, :web_paths], coder: JSON

  belongs_to :account
  belongs_to :ticket, touch: true
  belongs_to :chat_started_event, foreign_key: :audit_id

  enum format: %i[json protobuf transient_json]

  validates_presence_of :account, :ticket, :chat_started_event, :chat_id, :visitor_id

  attr_accessible :account, :ticket, :chat_started_event, :format
  attr_reader :initial_value_length

  default_scope { order(:created_at) }

  after_initialize :set_value_defaults, if: :new_record?

  def self.last_message_for_tickets(tickets)
    return {} if tickets.blank?

    tickets.to_a.pre_load(chat_started_events: :chat_transcripts)

    tickets.each_with_object({}) do |ticket, result|
      last_event = ticket.chat_started_events.last
      result[ticket.id] = last_event.chat_transcripts.reverse_each.lazy.map(&:last_message).find(&:present?) if last_event
    end
  end

  def self.map_last_message_to_tickets(tickets)
    messages = last_message_for_tickets(tickets)

    tickets.each do |t|
      t.latest_chat_message_mapped = messages[t.id]
    end
  end

  class ChatMessage
    def initialize(history_item)
      @history_item = history_item
    end

    def author_id
      @history_item['actor_id'].to_i
    end

    def body
      @history_item['message']
    end

    def created_at
      Time.at(0, @history_item['timestamp'], :millisecond)
    end

    def public?
      true
    end
  end

  def last_message
    return unless json?
    chat_history.reverse_each.find { |item| item['type'] == 'ChatMessage' }.try { |item| ChatMessage.new(item) }
  end

  def set_value_defaults
    self.chat_history ||= []
    self.web_paths ||= []
    @initial_value_length = value.to_json.size
    return unless chat_started_event
    self.chat_id = chat_started_event.chat_id
    self.visitor_id = chat_started_event.visitor_id
  end

  def array_item_size(value)
    value.to_json.bytesize + 1 # assume there will be a comma
  end

  class << self
    def value_limit
      @value_limit ||= column_for_attribute(:value).limit
    end
  end
end
