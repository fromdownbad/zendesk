class PostFlagType < ActiveHash::Base
  fields :id, :name

  include ActiveHash::Enum
  enum_accessor :name

  include ActiveHash::Associations
  has_many :posts, foreign_key: :flag_type_id

  self.data = [
    { id: 0, name: "Default" },
    { id: 1, name: "Suspended" }
  ]
end
