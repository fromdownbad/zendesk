module Explore
  class Subscription < AccountServiceSubscription
    PLAN_SETTINGS = %i[
      plan_type
      max_agents
    ].freeze

    define_attr_accessors

    PRODUCT_NAME = 'explore'.freeze

    alias_method :include_in_easy_agent_add?, :subscribed?

    def inspect
      "#<Explore::Subscription:#{object_id} state=#{state} " \
        "plan_type=#{plan_type} " \
        "max_agents=#{max_agents} " \
        "trial_expires_at=#{trial_expires_at}>"
    end

    def plan_name
      ZBC::Explore::PlanType[plan_type].try(:description)
    end
  end
end
