require 'json_with_yaml_fallback_coder'
class FraudScore < ActiveRecord::Base
  extend Zendesk::CappedCounts
  include Fraud::FraudScoreActions

  belongs_to :account

  # Used to store parameters used in fraud score,
  # such as subdomain, owner_email, etc.
  serialize :other_params, Hash

  # Store AFS response hash and access its items using accessors.
  store :service_response, accessors: [:actions, :reasons], coder: JsonWithYamlFallbackCoder.new

  validates :account_id, presence: true
  validates :verified_fraud, inclusion: {in: [true, false]}
  validates :is_whitelisted, inclusion: {in: [true, false]}

  after_commit :notify_voice

  alias_method :current_account, :account

  # Define setters and getters for the standard parameters
  # stored in +other_params+.
  #
  # Example:
  #   fraud_score = FraudScore.new(other_params: {owner_email: 'test@example.com', subdomain: 'sub1'})
  #   fraud_score.subdomain           # => "sub1"
  #   fraud_score.owner_email         # => "test@example.com"
  #   fraud_score.subdomain = 'sub2'
  #
  #   fraud_score = FraudScore.new(owner_email: 'test@example.com', subdomain: 'sub1')
  #   fraud_score.other_params        # => {:owner_email => "test@example.com", :subdomain => "sub1"}
  def self.other_params_accessors_for(param_name)
    define_method param_name do
      other_params[param_name]
    end

    define_method :"#{param_name}=" do |value|
      other_params[param_name] = value
    end
  end

  def source_event
    other_params[:source_event].try(:to_sym)
  end

  def source_event=(value)
    other_params[:source_event] = value
  end

  def free_mail?
    return true if score_params.try(:[], 'free_mail_domain')
    return true if actions.try(:[], :free_mail).nil?
    actions.try(:[], :free_mail) == true
  end

  def talk_risky
    actions.try(:[], :talk_risky)
  end

  def support_risky
    actions.try(:[], :support_risky)
  end

  def risk_score
    score_params.try(:[], 'insights_risk_score')
  end

  def minfraud_id
    score_params.try(:[], 'minfraud_id')
  end

  def auto_suspension?
    Fraud::SourceEvent::SIGNUP_EVENTS.include?(source_event) && verified_fraud
  end

  def manual_suspension?
    source_event == Fraud::SourceEvent::MANUAL_SUSPENSION
  end

  other_params_accessors_for :subdomain
  other_params_accessors_for :owner_email
  other_params_accessors_for :account_name
  other_params_accessors_for :owner_name
  other_params_accessors_for :score_params

  private

  def notify_voice
    return unless account.has_voice_block_risky?
    log_account_creation_delta
    Voice::SendFraudScoreJob.enqueue(account.id, talk_risky)
  end

  def log_account_creation_delta
    statsd_client.histogram('account_creation_delta', Time.now - account.created_at, tags: ["source_event:#{source_event}"])
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['fraud_score_model'])
  end
end
