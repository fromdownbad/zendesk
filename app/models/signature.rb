require 'zendesk_comment_markup'
require 'zendesk_text'

class Signature < ActiveRecord::Base
  belongs_to :user
  belongs_to :account

  attr_accessible :account, :user, :value, :html_value

  validates_presence_of :user, :account_id
  before_validation     :ensure_account

  validates_lengths_from_database only: :value

  def html_value=(new_value)
    self.is_rich = true

    new_value = ZendeskCommentMarkup.filter_input(new_value, from_agent: true).to_s

    write_attribute(:value, new_value)
  end

  def value=(new_value)
    self.is_rich = false
    write_attribute(:value, new_value)
  end

  def value
    v = read_attribute(:value)

    return v if v.blank? || !is_rich?

    ZendeskCommentMarkup::StripperFilter.to_html(v)
  end

  def html_value
    v = read_attribute(:value)

    return v if v.blank?

    if is_rich?
      v
    else
      ZendeskText::Markdown.html(v, ticket_linking: false)
    end
  end

  protected

  def is_rich=(rich)
    write_attribute(:is_rich, rich)
  end

  private

  def ensure_account
    self.account ||= user.account
  end
end
