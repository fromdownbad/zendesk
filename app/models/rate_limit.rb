class RateLimit < ActiveRecord::Base
  belongs_to :account
  belongs_to :rate_limitable, polymorphic: true

  attr_accessible :value

  # Note: This is the percentage value of the account's main rate limit.

  validates :value, inclusion: { in: 0..100, message: 'must be a percentage' }
end
