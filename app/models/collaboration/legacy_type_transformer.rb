class Collaboration
  class LegacyTypeTransformer
    def self.email_ccs(collaborations: [], settings: {}, account_id:, users_preloaded: false)
      new(collaborations: collaborations, settings: settings, account_id: account_id, users_preloaded: users_preloaded).email_ccs
    end

    def self.followers(collaborations: [], settings: {}, account_id:, users_preloaded: false)
      new(collaborations: collaborations, settings: settings, account_id: account_id, users_preloaded: users_preloaded).followers
    end

    def self.filtered_collaborators(collaborations: [], settings: {})
      new(collaborations: collaborations, settings: settings, account_id: nil).filtered_collaborators
    end

    def self.transform(collaborations = [], follower_and_email_cc_collaborations_enabled)
      collaborations.each do |collaboration|
        collaboration.transform(follower_and_email_cc_collaborations_enabled)
      end
    end

    attr_accessor :account_id, :users_preloaded

    def initialize(collaborations:, settings:, account_id:, users_preloaded: false)
      @collaborations = collaborations
      @settings = settings
      @account_id = account_id
      @users_preloaded = users_preloaded
    end

    def email_ccs
      email_cc_users = if follower_and_email_cc_collaborations_enabled?
        @collaborations.map do |collaboration|
          next if filter_collaboration?(collaboration)
          next unless collaboration.email_cc?(verify_user: !do_not_verify_user?)
          collaboration_user(collaboration)
        end.compact.uniq
      else
        []
      end

      email_cc_users.sort_by(&:name)
    end

    def collaboration_user(collaboration)
      return collaboration.user if users_preloaded

      user_id_to_user_map[collaboration.user_id] || collaboration.user
    end

    def user_id_to_user_map
      @user_id_to_user_map ||= User.where(id: @collaborations.map(&:user_id), account_id: @account_id).index_by(&:id)
    end

    def followers
      follower_users = if follower_and_email_cc_collaborations_enabled?
        @collaborations.map do |collaboration|
          next if filter_collaboration?(collaboration)
          next unless collaboration.follower?
          collaboration_user(collaboration)
        end.compact.uniq
      else
        @collaborations.map(&:user)
      end

      follower_users.sort_by(&:name)
    end

    def filtered_collaborators
      collaborators = if follower_and_email_cc_collaborations_enabled?
        @collaborations.map do |collaboration|
          next if filter_collaboration?(collaboration)
          collaboration.user
        end.compact.uniq
      else
        @collaborations.map(&:user)
      end

      collaborators.sort_by(&:name)
    end

    private

    def do_not_verify_user?
      @do_not_verify_user ||= email_ccs_light_agents_v2? && light_agent_email_ccs_allowed_enabled?
    end

    def filter_collaboration?(collaboration)
      (comment_email_ccs_allowed_disabled? && collaboration.email_cc?(verify_user: !do_not_verify_user?)) ||
        (ticket_followers_allowed_disabled? && collaboration.follower?)
    end

    def email_ccs_light_agents_v2?
      @settings[:email_ccs_light_agents_v2]
    end

    def light_agent_email_ccs_allowed_enabled?
      @settings[:light_agent_email_ccs_allowed_enabled]
    end

    def follower_and_email_cc_collaborations_enabled?
      @settings[:follower_and_email_cc_collaborations_enabled]
    end

    def comment_email_ccs_allowed_disabled?
      !@settings[:comment_email_ccs_allowed_enabled]
    end

    def ticket_followers_allowed_disabled?
      !@settings[:ticket_followers_allowed_enabled]
    end
  end
end
