class HelpCenter
  attr_reader :id, :state

  class << self
    # Sets the state of a HelpCenter record to restricted, enabled, or archived.
    #
    # Raises ActiveRecord::RecordNotFound if no record exists with the brand_id.
    # Raises ActiveRecord::RecordInvalid if the state is invalid.
    def change_state(state:, brand_id:)
      record = Record.find_by!(brand_id: brand_id)

      record.help_center_state = state
      record.save!
    end

    def exists_for_brand_id?(brand_id)
      Record.exists?(brand_id: brand_id)
    end

    def find_by_brand_id(brand_id)
      record = Record.find_by(brand_id: brand_id)
      return if record.nil?

      new(id: record.help_center_id, state: record.help_center_state)
    end

    def destroy_by_brand_id(brand_id)
      Record.where(brand_id: brand_id).destroy_all
    end

    def create(id:, state:, account_id:, brand_id:)
      Record.create!(
        help_center_id: id,
        help_center_state: state,
        account_id: account_id,
        brand_id: brand_id
      )
    end
  end

  def initialize(id:, state:)
    @id = id
    @state = state
  end

  def restricted?
    state == "restricted"
  end

  def enabled?
    state == "enabled"
  end

  def archived?
    state == "archived"
  end

  class Record < ActiveRecord::Base
    self.table_name = "help_centers"

    validates :help_center_state, inclusion: {
      in: %w[restricted enabled archived]
    }
  end
end
