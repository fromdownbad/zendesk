class AcmeCertificateJobStatus < ActiveRecord::Base
  belongs_to :account

  scope :uncompleted, -> { where("completed_at is null") }

  def self.enqueue_with_status!(account_id, activate_certificate: true)
    status = AcmeCertificateJobStatus.create!(account_id: account_id)
    AcmeCertificateJob.enqueue(account_id, status.id, activate_certificate)
  end

  def queue_audit
    @queue_audit ||= QueueAudit.where(account_id: account_id, enqueued_id: enqueued_id).first
  end

  def working!
    if started_at.nil?
      update_attributes!(status: 'working', started_at: Time.now)
    end
  end

  def failed!(message = nil)
    update_attributes!(status: 'failed', message: message.to_s, completed_at: Time.now)
  end

  def completed!
    update_attributes(status: 'completed', completed_at: Time.now)
  end

  def failed?
    status == 'failed'
  end

  def working?
    status == 'working'
  end

  def completed?
    status == 'completed'
  end

  def pending_worker?
    status.nil?
  end
end
