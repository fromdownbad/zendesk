module Organizations
  # Name accessor for autocompletion
  module AccessByName
    def self.included(base)
      base.validate :validate_organizations_for_names_exist
    end

    def organization_name
      organization.try(:name)
    end

    def organization_name=(name)
      self.organization = find_organization_for(
        name.try(:strip)
      )
    end

    def organization_names=(names)
      filtered_names = format_and_filter(names)
      organizations  = find_organizations_for(filtered_names)

      if organizations.size == filtered_names.size
        default_organization = account.default_organization_for(email_domain)
        organizations << default_organization if default_organization && account.has_multiple_organizations_enabled?
        self.organizations = organizations
      end
    end

    protected

    attr_writer :missing_organization_names

    def missing_organization_names
      @missing_organization_names ||= []
    end

    def format_and_filter(names)
      names.map(&:strip).reject(&:empty?)
    end

    def find_organizations_for(names)
      names.map { |name| find_organization_for(name) }.compact
    end

    def find_organization_for(name)
      return if name.blank?

      account.organizations.find_by_name(name).tap do |org|
        (missing_organization_names << name) if name.present? && org.nil?
      end
    end

    def validate_organizations_for_names_exist
      return if missing_organization_names.empty?

      errors.add(:base,
        I18n.t(
          "txt.error_message.the_organization_name_of_org_doesnot_exist.#{missing_organization_names_namespace}",
          organization_name: missing_organization_names.join(", ")
        ))
    end

    def missing_organization_names_namespace
      if missing_organization_names.one?
        "one"
      elsif missing_organization_names.many?
        "many"
      end
    end
  end
end
