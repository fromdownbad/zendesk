class FeatureBoost < ActiveRecord::Base
  belongs_to              :account
  before_validation       :set_defaults, on: :create

  attr_accessible :account, :is_active, :active, :activated_at, :created_at, :updated_at, :valid_until, :expires, :boost_plan, :boost_level

  validates_presence_of   :valid_until
  validates_presence_of   :account_id
  validates_uniqueness_of :account_id
  validate                :validate_plan_boost_level
  validate                :validate_boost_expires_in_less_than_thirty_days

  after_commit            :update_subscription_features!

  alias_attribute :active, :is_active
  alias_attribute :expires, :valid_until

  def boost_plan_name
    ZBC::Zendesk::PlanType.plan_name(
      ZBC::Zendesk::PricingModelRevision::PATAGONIA,
      boost_level
    )
  end

  def boost_plan
    SubscriptionPlanType[boost_level].name
  end

  def boost_plan_score
    SubscriptionPlanType[boost_level].score
  end

  def boost_plan=(name)
    self.boost_level = SubscriptionPlanType.find(name)
  end

  def set_defaults
    self.is_active      = true if is_active.nil?
    self.activated_at ||= Time.now
    self.boost_level  ||= SubscriptionPlanType.Large
  end

  private

  def validate_plan_boost_level
    # Don't allow boosting to a lower plan
    if is_active? && account.subscription.plan_type > boost_level
      errors.add(:boost_level, :invalid)
    end
  end

  def validate_boost_expires_in_less_than_thirty_days
    return if account.is_sandbox?

    if valid_until > maximum_boost_length.from_now
      errors.add :valid_until, "can't be more than #{maximum_boost_length.inspect} from now"
    end
  end

  def maximum_boost_length
    eligible_for_remote_support_bundle? ? 6.months : 30.days
  end

  def eligible_for_remote_support_bundle?
    account.has_remote_support_promo_boost_extension_plan? && team_account?
  end

  def team_account?
    current_plan_name == ZBC::Zendesk::PlanType::Team.name.capitalize
  end

  def current_plan_name
    ZBC::Zendesk::PlanType.plan_name(
      account.subscription.pricing_model_revision,
      account.subscription.plan_type
    )
  end

  def update_subscription_features!
    update_or_destroy_boosted_addon

    Zendesk::Features::SubscriptionFeatureService.new(
      account
    ).execute!
  end

  def update_or_destroy_boosted_addon
    if boost_deactivated? || previous_changes.include?(:boost_level)
      account.subscription_feature_addons.boosted.destroy_all
    elsif previous_changes.include?(:valid_until)
      account.subscription_feature_addons.boosted.update_all(boost_expires_at: valid_until)
    end
  end

  def boost_deactivated?
    previous_changes.include?(:is_active) &&
      !is_active &&
      account.subscription.plan_type != boost_level
  end
end
