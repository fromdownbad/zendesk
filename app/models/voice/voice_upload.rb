module Voice
  class VoiceUpload < Voice::Upload
    include Zendesk::ForbiddenAttributesProtection
  end
end
