module Voice
  class PartnerEditionAccount < ActiveRecord::Base
    include CIA::Auditable
    include AccountServiceIntegration

    TRIAL_PERIOD = 30
    PLAN_TYPE    = { legacy: 0, regular: 1 }.freeze

    self.table_name = 'voice_partner_edition_accounts'

    has_soft_deletion default_scope: true

    audit_attribute :plan_type, :active, :trial_expires_at
    attr_accessible :active, :plan_type, :trial_expires_at, :trial

    belongs_to :account

    validates_presence_of :account_id, :plan_type
    validates_inclusion_of :active, in: [true, false]
    validates :plan_type, inclusion: { in: PLAN_TYPE.values }

    before_update :update_active_flag

    def create_and_delete(updated_attributes)
      new_account = build_new_account(updated_attributes)

      if new_account.save
        soft_delete!
        return true
      end

      false
    end

    def subscribe!
      create_and_delete(trial: false, active: true)
    end

    def deactivate!
      create_and_delete(active: false)
    end

    def legacy?
      plan_type == PLAN_TYPE[:legacy]
    end

    def self.find_or_create_by_account(account)
      where(
        account_id: account.id
      ).first_or_create do |partner_account|
        partner_account.plan_type = PLAN_TYPE[:regular]
        partner_account.active = true
        partner_account.trial = true
        partner_account.trial_expires_at = TRIAL_PERIOD.days.from_now
      end
    end

    def updated_subscription!(subscription)
      if subscription.active?
        subscribe!
      else
        deactivate!
      end
    end

    # Used by billing
    def in_trial?
      active? && trial?
    end

    private

    def update_active_flag
      if changes[:trial_expires_at] && trial_expires_at.present?
        self.active = true
      end
    end

    def build_new_account(attributes)
      dup.tap do |new_account|
        new_account.assign_attributes(attributes)
        if new_account.trial && attributes[:trial_expires_at] && attributes[:trial_expires_at].to_date.future?
          new_account.active = true
        end
      end
    end
  end
end
