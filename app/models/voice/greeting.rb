module Voice
  class Greeting < ActiveRecord::Base
    self.table_name = 'voice_greetings'

    belongs_to :account

    def readonly?
      true
    end

    def destroy
      raise ActiveRecord::ReadOnlyRecord
    end

    def delete
      raise ActiveRecord::ReadOnlyRecord
    end
  end
end
