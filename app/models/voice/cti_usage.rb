module Voice
  class CtiUsage < ActiveRecord::Base
    self.table_name = 'voice_cti_usage'

    attr_accessible :user, :display_user

    validates_presence_of :account_id, :user_id

    belongs_to :account
    belongs_to :user
    belongs_to :display_user, class_name: 'User'

    scope :latest, -> { order('created_at DESC').limit(1) }
  end
end
