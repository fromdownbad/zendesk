module Voice
  class Trial
    def self.product_payload
      {
        product: {
          state: Zendesk::Accounts::Product::NOT_STARTED,
          plan_settings: {
            plan_type: ZBC::Voice::PlanType::Professional.plan_type,
            suite: true
          }
        }
      }
    end
  end
end
