module Voice
  module PartnerEditionAccount::AccountServiceIntegration
    extend ActiveSupport::Concern

    included do
      after_create :update_or_create_account_service_product
    end

    private

    ACCOUNT_SERVICE_PRODUCT_NAME = 'talk_partner'.freeze

    # NOTE: The call to Zendesk::Accounts::Client#update_or_create_product! is
    # relying on the default value for the :include_deleted_account param for
    # when it is not supplied.
    def update_or_create_account_service_product
      if account.spp?
        account_service_client.update_or_create_sku!(
          ACCOUNT_SERVICE_PRODUCT_NAME,
          { sku: product_params },
          context: "voice_partner_edition_integration"
        )
      else
        account_service_client.update_or_create_product!(
          ACCOUNT_SERVICE_PRODUCT_NAME,
          { product: product_params },
          context: "voice_partner_edition_integration"
        )
      end
    end

    # Builds the ProductPostRequestBody as described in
    # https://github.com/zendesk/pravda/blob/master/API.md#productpostrequestbody
    #
    # Example:
    #
    # {
    #   product: {
    #     name:             'talk_partner'
    #     state:            :trial
    #     trial_expires_at: '2018-01-10 01:49:35 +0000'
    #     plan_settings: {
    #       plan_type:              1,
    #       has_talk_cti_partner:  false
    #       max_agents: 5
    #    }
    # }
    #
    def product_params
      {
        plan_settings:    account.spp? ? sku_plan_settings : plan_settings,
        state:            product_state,
        trial_expires_at: trial_expires_at.try(:utc).try(:iso8601)
      }.select { |_, value| !value.nil? }
    end

    def product_state
      if trial_expired?
        Zendesk::Accounts::Product::EXPIRED
      elsif in_trial? # ie: active? && trial?
        Zendesk::Accounts::Product::TRIAL
      end
    end

    # NOTE: The method #trial? is slightly different than #in_trial? -- for a
    # talk_partner account to be considered "in trial" state the flag :trial has
    # to be true and the flag :active has to be true -- #trial? simply returns
    # the state of the :trial field/attr and if it's true it does not
    # automatically mean the account is "in trial" (bleh!)
    def plan_settings
      {
        plan_type:            plan_type,
        max_agents:           account.tpe_subscription ? account.tpe_subscription.max_agents : account.subscription.max_agents,
        has_talk_cti_partner: account.has_talk_cti_partner?
      }
    end

    # NOTE: For TPE there is only plan type by default, so we
    # do not need to supply one to it as a parameter
    def sku_plan_settings
      {
        max_agents: account.tpe_subscription ? account.tpe_subscription.max_agents : account.subscription.max_agents
      }
    end

    # NOTE: There is a job that runs every two hours that will check for records
    # where the :trial attribute is true and whose :trial_expires_at attribute
    # value is in the past. Records that match this criteria are soft-deleted
    # and recreated with the :active attribute set to false.
    #
    # Trials may be reactivated after this by setting the :trial_expires_at
    # attribute to a later date. When that happens, the record is soft-deleted
    # and recreated with the new :trial_expires_at attribute value.
    def trial_expired?
      trial? && !active? && deleted_at.nil?
    end

    ACCOUNT_SERVICE_CLIENT_RETRY_OPTIONS = {
      max:        3,
      interval:   2,
      exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
      methods:    %i[post patch]
    }.freeze

    ACCOUNT_SERVICE_CLIENT_TIMEOUT = 10

    def account_service_client
      @account_service_client ||= Zendesk::Accounts::Client.new(
        account,
        retry_options: ACCOUNT_SERVICE_CLIENT_RETRY_OPTIONS,
        timeout: ACCOUNT_SERVICE_CLIENT_TIMEOUT
      )
    end
  end
end
