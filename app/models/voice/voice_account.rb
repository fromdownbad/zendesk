module Voice
  class VoiceAccount < ActiveRecord::Base
    include Zendesk::ForbiddenAttributesProtection

    belongs_to :account

    # It's a hotfix, please remove this code when Voice features are decoupled from Classic
    # Do not use it in Classic, can be deprecetad any moment.
    #
    # begin

    USER_SEATS_PLANS = [
      PLAN_ADVANCED     = 2,
      PLAN_BASIC        = 5,
      PLAN_ENTERPRISE   = 6,
      PLAN_PROFESSIONAL = 7,
      PLAN_TEAM         = 8
    ].freeze

    def user_seats_enabled?
      # calls out to this user_seats_enabled? method and checks for user seats feature
      # can be removed with staff service roll out, once staff service rollout is done
      # we can assume every account is using user_seats or post staff service rollout every account will use talk roles
      # we will clean this up and remove feature checks when we cleanup lotus_feature_voice_staff_service_roles arturo
      if account.has_lotus_feature_voice_staff_service_roles?
        true
      else
        USER_SEATS_PLANS.include?(plan_type)
      end
    end

    # end

    def readonly?
      true
    end

    def self.remote_data_for_account(account)
      return unless account.voice_enabled?

      client   = Zendesk::Voice::InternalApiClient.new(account.subdomain)
      response = client.voice_subscription_details

      return nil unless response && response.respond_to?(:body)
      RemoteDataDelegator.new(response)
    rescue Kragle::ResourceNotFound
      nil
    end

    def destroy
      raise ActiveRecord::ReadOnlyRecord
    end

    def delete
      raise ActiveRecord::ReadOnlyRecord
    end

    class RemoteDataDelegator < SimpleDelegator
      def initialize(response)
        hash = Hashie::Mash.new(response.body['voice_account'])
        hash.created_at = DateTime.parse(hash.created_at) if hash.created_at
        __setobj__(hash)
      end

      def show_trial_or_legacy_voice_subscription?
        is_current_trial = (trial && !trial_expired)
        is_legacy        = billing_plan_type.nil?

        is_current_trial || is_legacy
      end
    end
  end
end
