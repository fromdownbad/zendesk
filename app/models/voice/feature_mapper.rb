module Voice
  class FeatureMapper
    def initialize(account)
      @account = account
    end

    def voice_sub_account
      @voice_sub_account ||= fetch_voice_sub_account
    end

    def feature_enabled?(feature)
      return false unless @account.voice_enabled?
      internal_api_client = Zendesk::Voice::InternalApiClient.new(@account.subdomain)
      body = internal_api_client.get_voice_feature(feature).body
      body['feature'].symbolize_keys![feature]
    rescue ZendeskAPI::Error::RecordNotFound
      return nil
    rescue ZendeskAPI::Error::NetworkError => ex
      ZendeskExceptions::Logger.record(ex, location: self, message: "Can't fetch Voice feature #{feature} for #{@account.id} : #{ex.message}", fingerprint: '3c95dfb566d5a5a8bb5dc95cfdcca0cc79282e9c')
      return nil
    end

    private

    def fetch_voice_sub_account
      return unless @account.voice_enabled?
      internal_api_client = Zendesk::Voice::InternalApiClient.new(@account.subdomain)
      body = internal_api_client.get_sub_account.body
      Hashie::Mash.new(body['sub_account'])
    rescue ZendeskAPI::Error::RecordNotFound
      return nil
    rescue ZendeskAPI::Error::NetworkError => ex
      ZendeskExceptions::Logger.record(ex, location: self, message: "Can't fetch Voice Sub account for #{@account.id} : #{ex.message}", fingerprint: '7c519d52c259b457c3dfdff506c651a95e953205')
      return nil
    end
  end
end
