module Voice
  class UserPhoneNumberSmsCapability < UserPhoneAttribute
    after_initialize :init

    def init
      self.attribute_type ||= UserPhoneAttribute::TYPE[:sms_capability]
    end

    def available?
      value == '1'
    end
  end
end
