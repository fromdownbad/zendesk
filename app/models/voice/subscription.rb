require 'cia'

module Voice
  class Subscription < ActiveRecord::Base
    include ::CIA::Auditable
    include Zendesk::ForbiddenAttributesProtection

    self.table_name = 'voice_subscriptions'

    audit_attribute :account_id,
      :plan_type,
      :max_agents,
      :suspended,
      :is_prepaid,
      callback: :after_commit

    belongs_to :account

    after_commit :update_voice_seats
    after_commit :create_zuora_voice_usage_subscription,
      unless:    :billing_check_voice_usage_subscription_during_sync?,
      on:        :create
    after_commit :notify_voice_of_creation, on: :create
    after_create :allow_voice_trial
    after_commit :notify_voice_of_update, on: :update

    # Send stats about creation and update;
    # this should be removed when has_voice_disable_subscription_notifications is ga'd
    after_commit :log_creation_and_update, on: [:create, :update]

    attr_accessor :trial, :legacy

    def status
      suspended? ? 'suspended' : 'customer'
    end

    def is_active? # rubocop:disable Naming/PredicateName
      !suspended?
    end

    alias_method :include_in_easy_agent_add?, :is_active?

    def plan_name
      ZBC::Voice::PlanType[plan_type].try(:description)
    end

    def suspend!
      update_attributes!(suspended: true)
    end

    def original_voice_plan_type?
      ZBC::Voice::PlanType.original_types.include?(plan_type)
    end

    private

    def create_zuora_voice_usage_subscription
      ZBC::Zuora::Jobs::AddVoiceJob.enqueue(account.id)
    end

    def billing_check_voice_usage_subscription_during_sync?
      Arturo.feature_enabled_for?(:billing_check_voice_usage_subscription_during_sync, account)
    end

    def notify_voice_of_update
      if account.has_voice_disable_subscription_notifications?
        Rails.logger.info "[Voice subscription] Skipping voice notification on subscription update; account id: #{account.id}; plan type: #{plan_type}"
        return
      end
      Rails.logger.info "[Voice subscription] Notifying voice on subscription update; account id: #{account.id}; plan type: #{plan_type}"
      unless previous_changes.blank?
        Voice::UpdateVoiceJob.enqueue(account.id, plan_type: plan_type, suspended: suspended)
      end
    end

    def statsd
      @statsd ||= Zendesk::StatsD::Client.new(namespace: 'voice_subscription_change')
    end

    def log_creation_and_update
      Rails.logger.info '[Voice subscription] Sending stats about subscription changes'
      statsd.increment('voice_subscription_change')
    end

    def allow_voice_trial
      account.settings.voice_trial_enabled = true
      account.settings.save
    end

    def notify_voice_of_creation
      if account.has_voice_disable_subscription_notifications?
        Rails.logger.info "[Voice subscription] Skipping voice notification on subscription creation; account id: #{account.id}; plan type: #{plan_type}"
        return
      end
      Rails.logger.info "[Voice subscription] Notifying voice on subscription creation; account id: #{account.id}; plan type: #{plan_type}"
      Voice::CreateVoiceJob.enqueue(account.id, plan_type)
    end

    def update_voice_seats
      return if account.user_seats.voice.count(:all) <= max_agents

      keep_agent_ids = if account.has_voice_downgrade_agents_support?
        recent_created_agent_ids_with_voice_seat
      else
        # fetch all agents with seats, and put the ones with availability in front
        (recent_active_agent_ids + account.user_seats.voice.pluck(:user_id)).uniq
      end

      # limit the number of agents to keep
      keep_agent_ids = keep_agent_ids[0...max_agents]

      account.user_seats.voice.where('user_id NOT IN (?)', keep_agent_ids).destroy_all
    end

    def recent_active_agent_ids
      api_client.recent_active_agents(limit: max_agents).body['recent_active_agents']
    end

    def recent_created_agent_ids_with_voice_seat
      account.user_seats.voice.joins(:user).order("users.created_at").pluck(:user_id)
    end

    def api_client
      @api_client ||= Zendesk::Voice::InternalApiClient.new(account.subdomain)
    end
  end
end
