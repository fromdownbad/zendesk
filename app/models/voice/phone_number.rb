module Voice
  class PhoneNumber < ActiveRecord::Base
    has_soft_deletion default_scope: true

    attr_accessible

    include Voice::PhoneNumberBehaviour
  end
end
