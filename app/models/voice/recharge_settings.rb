module Voice
  class RechargeSettings < ActiveRecord::Base
    include Zendesk::ForbiddenAttributesProtection

    self.table_name = 'voice_recharge_settings'

    belongs_to :account

    before_validation :ensure_amount, :ensure_minimum_balance

    validates :amount, inclusion: ZendeskBillingCore::Zuora::VoiceRecharge::USD_AMOUNTS

    delegate :subscription, to: :account

    DEFAULT_AMOUNT_BASIC = {
      CurrencyType.USD => 50.00,
      CurrencyType.GBP => 50.00,
      CurrencyType.EUR => 50.00
    }.freeze
    DEFAULT_AMOUNT_ADVANCED = {
      CurrencyType.USD => 200.00,
      CurrencyType.GBP => 200.00,
      CurrencyType.EUR => 200.00
    }.freeze
    DEFAULT_MINIMUM_BALANCE = {
      CurrencyType.USD => 5.00,
      CurrencyType.GBP => 5.00,
      CurrencyType.EUR => 5.00
    }.freeze

    def payment_status
      # ok, recharge_in_progress, failed
    end

    private

    def default_amount
      # Every customer defaults to Basic until we introduce plans
      DEFAULT_AMOUNT_BASIC[currency_type]
    end

    def default_minimum_balance
      [BigDecimal('0.10') * amount.to_f, DEFAULT_MINIMUM_BALANCE[currency_type]].max
    end

    def ensure_amount
      # Default DB amount is set to 0.0. Once we set the default amount to nil,
      # we can move over to `self.amount ||= default_amount`
      self.amount = default_amount if amount == 0.0
    end

    def ensure_minimum_balance
      self.minimum_balance = default_minimum_balance if amount_changed?
    end

    def currency_type
      subscription.currency_type
    end
  end
end
