module Voice
  class InsightsField < ActiveRecord::Base
    self.table_name = 'voice_insights_fields'

    attr_accessible

    def self.filter_fields_for_account(fields, account_id)
      ids = Voice::InsightsField.where(account_id: account_id).pluck(:ticket_field_id)

      if ids.blank?
        fields
      else
        fields.where('ticket_fields.id NOT IN (?)', ids)
      end
    end

    def readonly?
      true
    end

    def destroy
      raise ActiveRecord::ReadOnlyRecord
    end

    def delete
      raise ActiveRecord::ReadOnlyRecord
    end
  end
end
