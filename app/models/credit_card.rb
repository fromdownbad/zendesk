# This model (via Zendesk::Billing::Quickpay::CreditCard) validates card
# information before registering the card with Quickpay. Future payment
# gateways will perform these validations for us. The model will become
# a very simple credit card representation: just masked number, expiry,
# and gateway reference. Any gateway logic should be delegated to
# lib/zendesk/billing/#{gateway}.rb.

class CreditCard < ActiveRecord::Base
  not_sharded
  disable_global_uid

  module PaymentGateway
    QUICKPAY = 'quickpay'.freeze
    BRAINTREE = 'braintree'.freeze
    DEFAULT = BRAINTREE
    ALL = [QUICKPAY, BRAINTREE].freeze
  end

  belongs_to :account
  belongs_to :subscription, inherit: :account_id

  attr_accessor :verification_value
  attr_accessor :number

  attr_accessible :account, :subscription, :expiry, :name, :payment_gateway_reference, :payment_gateway_type,
    :shadowed_number, :card_type, :year, :month, :is_danish_payment, :verification_value, :number

  attr_readonly :payment_gateway_type

  validates_presence_of       :shadowed_number, on: :update
  validates_presence_of       :name, on: :update

  validates_presence_of       :subscription
  validates_presence_of       :payment_gateway_reference, on: :update
  validates_presence_of       :payment_gateway_type
  validates_inclusion_of      :payment_gateway_type, in: CreditCard::PaymentGateway::ALL

  before_validation           :inherit_account_id

  scope :braintree, -> { where(payment_gateway_type: CreditCard::PaymentGateway::BRAINTREE) }
  scope :quickpay, -> { where(payment_gateway_type: CreditCard::PaymentGateway::QUICKPAY) }

  def initialize(*args)
    super(*args)
    self['payment_gateway_type'] ||= CreditCard::PaymentGateway::DEFAULT
  end

  def braintree?
    CreditCard::PaymentGateway::BRAINTREE == payment_gateway_type
  end

  def quickpay?
    CreditCard::PaymentGateway::QUICKPAY == payment_gateway_type
  end

  def gateway
  end

  def registered?
    !payment_gateway_reference.blank?
  end

  def expired?
    Date.today.year > year || (Date.today.year == year && Date.today.month > month)
  end

  def year
    read_attribute(:year).present? ? read_attribute(:year).to_i : Date.today.year + 5
  end

  def month
    read_attribute(:month).present? ? read_attribute(:month).to_i : Date.today.month
  end

  def expiry=(expiry_string)
    self.month  = expiry_string.slice(0, 2)             # FIXME: be prepared for other forms (11/10, 1110, etc)
    self.year   = 2000 + expiry_string.slice(2, 4).to_i # FIXME: y3k bug ;)
  end

  private

  def inherit_account_id
    if subscription && subscription.account
      self.account_id = subscription.account.id
    end
  end
end
