Group.class_eval do
  property_set(:settings, inherit: :account_id) do
    # Chat Settings
    property :chat_enabled, type: :boolean, default: false
  end

  delegate :chat_enabled, to: :settings
end

GroupSetting.class_eval do
  has_kasket_on :group_id
  belongs_to :group, touch: true

  attr_accessible :name, :value
end
