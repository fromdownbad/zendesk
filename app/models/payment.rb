# Only paid accounts have payments. Except accounts that have been downgraded from paid to free,
# these will have the old PAID payments still.
#
# If an account is cancelled, the current PENDING payment is deleted.
#
# Payments are executed via the "Accountant" in lib/, which takes PENDING payments and executes the
# transaction against the payment gateway, sets the payment to PAID, and creates a new PENDING payment
# to be executed in 30 days, unless the payment getting charged is a "one time" payment.
#
# In case the account is a sponsored account, the payment gets sent to SPONSORED and no transaction is
# made towards the payment gateway.
#
# Once an account changes plan or billing cycle, make sure to iterate all PENDING payments and save them
# in order to recalculate the amount

class Payment < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible

  module FeatureType
    STANDARD = 0
    VOICE    = 1
  end

  belongs_to :account
  belongs_to :subscription, inherit: :account_id
  belongs_to :refunded_payment, class_name: "Payment"

  has_one :invoice, dependent: :destroy
  has_one :coupon_redemption, dependent: :destroy
  has_many :dunning_notifications, dependent: :destroy

  validates_presence_of     :account_id
  validates_presence_of     :subscription
  validates_presence_of     :period_begin_at,            if: proc { |p| p.should_validate_presence_of_attribute? }
  validates_presence_of     :period_end_at,              if: proc { |p| p.should_validate_presence_of_attribute? }
  validates_presence_of     :amount,                     if: proc { |p| p.should_validate_presence_of_attribute? }
  validates_presence_of     :net,                        if: proc { |p| p.should_validate_presence_of_attribute? }
  validates_numericality_of :amount, greater_than_or_equal_to: 0, if: proc { |p| p.should_validate_presence_of_attribute? && !p.refunded? }
  validates_numericality_of :net,    greater_than_or_equal_to: 0, if: proc { |p| p.should_validate_presence_of_attribute? && !p.refunded? }
  validates_presence_of     :pricing_model_revision,     if: proc { |p| p.subscription.crediting? }
  validates_presence_of     :payment_method_type,        if: proc { |p| p.paid? }
  validates_presence_of     :charged_at,                 if: proc { |p| p.status == PaymentType.PAID }
  validate                  :validate_begin_at_is_before_end_at
  validate                  :validate_invoicing_discounts
  validate                  :validate_status
  validate                  :validate_refund

  before_validation :inherit_credit_card_details, if: proc { |p| p.status_changed? && p.paid? }
  before_validation :touch_charged_at,            if: proc { |p| p.paid? }

  before_create :generate_token

  after_save        :invoice!, if: proc { |p| p.status_changed? && p.invoiced? && p.standard_payment? }
  after_save        :sync_coupon_redemption

  scope   :pending,   -> { where(status: PaymentType.PENDING) }
  scope   :paid,      -> { where(status: PaymentType.PAID) }
  scope   :postpay,   -> { where(status: PaymentType.POSTPAY) }
  scope   :carryover, -> { where(status: PaymentType.CARRYOVER) }
  scope   :sponsored, -> { where(status: PaymentType.SPONSORED) }
  scope   :invoiced,  -> { where(status: PaymentType.INVOICED) }
  scope   :inactive,  -> { where(status: PaymentType.PENDING, accounts: { is_active: false }).joins(:account) }
  scope   :one_time,  -> { where(is_one_time: true) }
  scope   :recurring, -> { where(is_one_time: false) }
  scope   :charged,   -> { where("charged_at IS NOT NULL") }
  scope   :standard,  -> { where(feature_type: FeatureType::STANDARD) }
  scope   :voice_payments,     -> { where(feature_type: FeatureType::VOICE) }
  scope   :recent_periods,     -> { where('period_begin_at <= ?', Time.now.utc.to_s(:db)).order('period_begin_at desc').limit(12) }
  scope   :failing,            -> (min_failures) { where("failures >= ?", min_failures) }
  scope   :account,            -> (account_id)   { where("account_id = ?", account_id) }
  scope   :status,             -> (status)       { where("status = ?", status) }
  scope   :executable,         -> (period_begin_at = nil) do
    period_begin_at ||= Time.now.end_of_day
    conditions = ["status = ? AND period_begin_at <= ? AND accounts.lock_state = ?", PaymentType.PENDING, period_begin_at, Account::LockState::OPEN]
    where(conditions).includes(:account).references(:account)
  end
  scope   :cycle,              -> (*args) { where("billing_cycle_type IN (?)", args) }
  scope   :payment_method,     -> (payment_method) { where("payment_method_type = ?", payment_method) }
  scope   :voice_payments_for, -> (account_id) { where("account_id = ? AND feature_type = ?", account_id, FeatureType::VOICE) }
  scope   :voice_due,          -> (period_end = nil) { where("status = ?  AND period_end_at <= ? AND feature_type = ?", PaymentType.POSTPAY, (period_end || 1.day.ago.end_of_day.to_s(:db)), Payment::FeatureType::VOICE) }
  scope   :due_in_days,        -> (days) { where("DATE_SUB(DATE(period_begin_at), INTERVAL (?) DAY) = UTC_DATE()", days) }

  delegate :credit_card, to: :subscription
  delegate :is_trial?, to: :subscription

  def self.find_by_token(token)
    where(token: token.to_s).first
  end

  # The following attributes:
  #
  #   period_end_at, period_begin_at, amount, net, pricing_model_revision
  #
  # all have different validation requirements depending on the invoicing status of the subscription.
  #
  #  * All are required for credit card payments, in all payment states.
  #  * None are required for invoicing payments in the pending state.
  #  * All are required for invoicing payments in the invoiced and paid states.
  #
  def should_validate_presence_of_attribute?
    (subscription.invoicing? && !pending?) || subscription.crediting?
  end

  # Is the payment eligible for being charged at next charge job run
  def executable?(beginning = Time.now.end_of_day)
    status == PaymentType.PENDING && period_begin_at < beginning
  end

  def failing?
    pending? && (failures > 0)
  end

  def paid?
    (status == PaymentType.PAID) || sponsored?
  end

  def sponsored?
    status == PaymentType.SPONSORED
  end

  def pending?
    status == PaymentType.PENDING
  end

  def postpay?
    status == PaymentType.POSTPAY
  end

  def carryover?
    status == PaymentType.CARRYOVER
  end

  def invoiced?
    status == PaymentType.INVOICED
  end

  def refunded?
    status == PaymentType.REFUNDED
  end

  def voice_payment?
    feature_type == Payment::FeatureType::VOICE
  end

  def standard_payment?
    feature_type == Payment::FeatureType::STANDARD
  end

  def paid_by_invoice?
    paid? && (payment_method_type == PaymentMethodType.MANUAL)
  end

  def paid_by_credit_card?
    paid? && (payment_method_type == PaymentMethodType.CREDIT_CARD)
  end

  def create_next!
    new_period_begin_at = period_end_at + 1.day
    new_period_end_at   = subscription.invoicing? ? (new_period_begin_at + duration) : nil

    subscription.payments.create!(
      status: PaymentType.PENDING,
      account: account,
      period_begin_at: new_period_begin_at,
      period_end_at: new_period_end_at
    )
  end

  def from
    period_begin_at.strftime('%Y-%m-%d')
  end

  def to
    period_end_at.strftime('%Y-%m-%d')
  end

  def pending_creditcard_payment_for_subscription?
    (subscription.crediting? && pending? && standard_payment?)
  end

  def editable?
    refunded? ||
      errors.present? ||
      invoiced? ||
      (!subscription.crediting? && subscription.invoicing? && (pending? || paid?)) ||
      pending_creditcard_payment_for_subscription?
  end

  def period
    "#{from} - #{to}"
  end

  def days_left
    [0, (period_end_at.to_date - Time.now.utc.to_date).to_i].max
  end

  def invoice_id
    "#{account_id.to_s.rjust(6, '0')}-#{invoice_no.to_i.to_s.rjust(6, '0')}"
  end

  def total(currency = Currency.USD)
    Currency.USD.exchange_to(net.to_f, currency)
  end

  def duration=(duration_in_days)
    self.period_end_at = period_begin_at + duration_in_days.days
  end

  def duration
    self[:period_end_at] - self[:period_begin_at]
  end

  def active_coupon_application(as_of = Time.now)
    subscription.active_coupon_application as_of
  end

  def validate_refund
    if refunded?
      if refunded_payment && refunded_payment.paid_by_credit_card?
        if (- net) > refunded_payment.net
          errors.add(:net, "can not be more than #{refunded_payment.net}")
        end
        if net != amount
          errors.add(:amount, "must be the same as net")
        end
      else
        errors.add(:base, "You can not create a refund without an original paid payment by credit card")
      end
    end
  end

  def is_recurring? # rubocop:disable Naming/PredicateName
    !is_one_time?
  end

  def build_order_line
    raise "Can't build_order_line when invoicing!" if subscription.invoicing? && !order_line.blank?

    self.order_line = "#{period}: #{max_agents} agent#{'s' unless max_agents == 1} on the #{subscription.plan_name.downcase} plan at $#{'%.2f' % amount}"
    order_line << " (one time upgrade fee)" if is_one_time?
    order_line << ". Discount: $#{'%.2f' % discount}. Total: $#{'%.2f' % net}" if discount.to_i > 0

    order_line
  end

  def inherit_credit_card_details
    return self unless status == PaymentType.PAID
    return self if credit_card.blank?

    self.payment_gateway_reference = credit_card.payment_gateway_reference
    self.payment_gateway_type = credit_card.payment_gateway_type
    self
  end

  private

  def generate_token
    self.token = Token.generate
  end

  def validate_invoicing_discounts
    if subscription.invoicing?
      if manual_discount.present? && manual_discount > 0
        errors.add(:manual_discount, "must not be present on invoiced payments")
      end
      if discount.present? && discount > 0
        errors.add(:discount, "must not be present on invoiced payments")
      end
    end
  end

  def validate_status
    if subscription.invoicing?
      errors.add(:status, "is not valid.") unless [PaymentType.SPONSORED, PaymentType.PENDING, PaymentType.INVOICED, PaymentType.PAID, PaymentType.POSTPAY, PaymentType.CARRYOVER].include?(status)
    end

    if subscription.crediting?
      errors.add(:status, "is not valid.") unless [PaymentType.SPONSORED, PaymentType.PENDING, PaymentType.PAID, PaymentType.REFUNDED, PaymentType.POSTPAY, PaymentType.CARRYOVER].include?(status)
    end

    unless voice_payment?
      errors.add(:status, "is not valid.") if [PaymentType.CARRYOVER, PaymentType.POSTPAY].include?(status)
    end
  end

  def validate_begin_at_is_before_end_at
    errors.add(:end_date, "can't be in the past") if period_end_at < period_begin_at
  end

  # Called during validation when an invoicing payment enters the INVOICED state.
  def invoice!
    inherit_subscription_settings
    self.invoice_no = account.invoice_no_sequence.next
    self.status     = PaymentType.INVOICED
    create_next! if is_recurring? && standard_payment?
  end

  def touch_charged_at
    raise unless paid?
    self['charged_at'] ||= DateTime.now.utc
  end

  def sync_coupon_redemption
    if coupon_redemption.present?
      if @should_destroy_coupon_redemption
        self.coupon_redemption = nil
      else
        coupon_redemption.save
      end
    end
  end
end
