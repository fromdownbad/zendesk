require 'objspace'

class Organization < ActiveRecord::Base
  include ScopedCacheKeys
  include CurrentTagsAccessor
  include Subscribable
  include ActionView::Helpers::UrlHelper
  include Zendesk::Serialization::OrganizationSerialization
  include CachingObserver
  include CIA::Auditable
  include PrecreatedAccountConversion
  include OrganizationEntityObserver
  include OrganizationEventBusObserver
  include TagManagement
  include ArSkippedCallbackMetrics::Instrument
  extend  Zendesk::DB::BulkUidAllocation
  extend  Upserting

  has_soft_deletion default_scope: true

  audit_attribute

  attr_accessor :is_bulk_updated

  attr_accessible :account, :details, :external_id, :shared_comments, :is_shared_comments, :name, :notes,
    :shared_tickets, :is_shared, :group, :group_id, :tag_restriction_mode, :additional_tags, :set_tags,
    :tags, :current_tags, :domain_names, :remove_tags

  alias_attribute :shared_tickets, :is_shared
  alias_attribute :shared_comments, :is_shared_comments

  ignore_column :default, :suspended

  belongs_to :account
  belongs_to :group, -> { where(is_active: true) }

  has_many   :users,
    -> { where('users.is_active = 1') },
    source: :user,
    through: :organization_memberships,
    inverse_of: :organizations,
    inherit: :account_id

  has_many   :forums, dependent: :nullify, inherit: :account_id
  has_many   :entries, dependent: :nullify, inherit: :account_id
  has_many   :tickets, inherit: :account_id
  has_many   :custom_field_values,
    -> { where(owner_type: 'Organization') },
    class_name: "CustomField::Value", foreign_key: :owner_id, inherit: :account_id,
    extend: Zendesk::CustomField::HasManyExtension, autosave: true
  has_many   :custom_fields, through: :custom_field_values, source: :field, inherit: :account_id
  has_many   :organization_memberships, inverse_of: :organization, inherit: :account_id, dependent: :destroy # doesn't fire for soft_delete. see after_soft_delete

  has_many   :organization_emails, dependent: :destroy
  has_many   :organization_domains, dependent: :destroy
  # NB: Calling Organization#organization_domains and/or #organization_emails
  # after updating org domain and email associations using Organization#domain_names=
  # and before saving the org could result in returning a list of domains and/or
  # emails with associations that will be deleted after the org is eventually saved.
  # Use Organization#in_flight_organization_domains and
  # Organization#in_flight_organization_emails instead unless you have a very
  # good reason to call the associations directly.
  private :organization_domains, :organization_emails

  before_validation       :massage_organization_data
  before_validation       :clear_blank_external_id
  before_validation       :lock_create_and_update, if: :lock_create_and_update?

  validates_presence_of   :name
  validates_format_of     :name, with: /\A[^|]*\z/
  validate                :validate_unique_name
  validate                :validate_domain_names
  validates_uniqueness_of :external_id, scope: :account_id, if: :external_id?
  validates_lengths_from_database only: [:name, :details, :notes, :external_id]

  after_save              :delete_removed_domains_and_emails

  after_commit            :enqueue_reassignment_job
  after_commit            :enqueue_unset_job, if: :deleted?, on: :update

  after_commit            :unlock_create_and_update, if: :lock_create_and_update?
  after_rollback          :unlock_create_and_update, if: :lock_create_and_update?

  VALID_DOMAIN_SEPARATORS = /[,; ]+/.freeze

  # mysql will find records with value 'abc' even when looking for 'abc '
  def self.find_by_external_id(external_id)
    return nil if external_id.blank?
    where(external_id: external_id.to_s).first.tap do |organization|
      return nil if organization.try(:external_id) != external_id.to_s
    end
  end

  def domain_names
    domain_values = in_flight_organization_domains.map(&:value).sort
    email_values = in_flight_organization_emails.map(&:value).sort
    domain_values + email_values
  end

  def domain_names=(new_domain_names)
    domain_and_email_values = parse_new_domain_names(new_domain_names)

    valid_domain_values = OrganizationDomain.valid_values(domain_and_email_values)
    valid_email_values = OrganizationEmail.valid_values(domain_and_email_values)
    @invalid_values = domain_and_email_values - (valid_domain_values + valid_email_values)

    persisted_organization_domains = organization_domains
    persisted_organization_emails = organization_emails

    persisted_domain_values = persisted_organization_domains.map(&:value)
    new_domain_values_to_add = valid_domain_values - persisted_domain_values
    persisted_domain_values_to_remove = persisted_domain_values - valid_domain_values

    persisted_email_values = persisted_organization_emails.map(&:value)
    new_email_values_to_add = valid_email_values - persisted_email_values
    persisted_email_values_to_remove = persisted_email_values - valid_email_values

    build_new_domains_and_emails(new_domain_values_to_add, new_email_values_to_add)

    @domains_to_remove = persisted_organization_domains.select do |persisted_organization_domain|
      persisted_domain_values_to_remove.include?(persisted_organization_domain.value)
    end

    @emails_to_remove = persisted_organization_emails.select do |persisted_organization_email|
      persisted_email_values_to_remove.include?(persisted_organization_email.value)
    end

    @added_domain_values = new_domain_values_to_add
    @removed_domain_values = persisted_domain_values_to_remove
  end

  def to_liquid
    Zendesk::Liquid::Wrapper.new("name").tap do |wrapper|
      wrapper.merge!(attributes_for_liquid)
      wrapper["custom_fields"] = custom_org_fields_for_liquid
    end
  end

  def to_s
    name
  end

  def tags
    tag_array
  end

  def tag_array
    return [] unless account.try(:has_user_and_organization_tags?)

    super
  end

  def destroy
    self.external_id = "#{id}_deleted_#{external_id}" if external_id
    self.name = "#{id}_deleted_#{name.slice(0, 215)}"
    soft_delete!
  end

  def domain_events
    @domain_events ||= []
  end

  def add_domain_event(domain_event)
    return if domain_event.nil?
    raise ArgumentError, "Expected OrganizationEvent, received #{domain_event.class}" unless domain_event.is_a?(::Zendesk::Protobuf::Support::Users::V2::OrganizationEvent)
    domain_events << domain_event
  end

  class << self
    def listify(string)
      return [] if string.blank?
      normalize_domain_strings(string.split(VALID_DOMAIN_SEPARATORS))
    end

    def normalize_domain_strings(strings)
      strings.
        delete_if(&:blank?).
        map(&:strip).
        map(&:downcase).
        uniq
    end

    def with_name_like(name)
      where("name LIKE ?", "#{sanitize_sql_like(name)}%")
    end

    private

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'organization')
    end
  end

  private

  def massage_organization_data
    name.to_utf8!.strip_all! unless name.nil?
    notes.strip_all! unless notes.nil?
    details.strip_all! if details.respond_to?(:strip!)
    self.is_shared_comments = false unless is_shared?
    true
  end

  def clear_blank_external_id
    self.external_id = nil if external_id.blank?
  end

  def lock_create_and_update?
    # memoize false and nil values as well as truthy ones
    return @lock_create_and_update if defined?(@lock_create_and_update)
    @lock_create_and_update = if account.nil?
      # see comment in OrganizationsController#build_organization
      # for some examples of when account may be nil
      # let's count how often this happens
      # and leave current behavior for now
      statsd_client.increment("organization_no_account_in_scope_for_a_write")
      log_account_nil_with_stack_trace("Organization '#{name}' write attempted")
      false
    else
      name_changed?
    end
  end

  # stronger validation for unique names when multiple POSTs are processed concurrently
  def lock_create_and_update
    @write_lock_token = SecureRandom.uuid
    lock_obtained = redis_client.set(
      single_execution_key(account_id, name),
      @write_lock_token,
      nx: true,
      ex: redis_expiration
    )

    unless lock_obtained
      statsd_client.increment('organization_non_unique_name_write_attempt')
      # TODO: Add I18n
      raise WriteInProgressError, "Failed write for organization '#{name}' due to another write in progress"
    end
  end

  def unlock_create_and_update
    redis_client.watch(single_execution_key(account_id, name)) do
      if @write_lock_token == redis_client.get(single_execution_key(account_id, name))
        redis_client.multi do |multi|
          multi.del(single_execution_key(account_id, name))
        end
      else
        redis_client.unwatch
      end
    end
  rescue Redis::BaseError => e
    statsd_client.increment("organization_unlock_create_and_update_failure")
    Rails.logger.error "#{self.class}: #{e.message}"
  end

  def redis_client
    @redis_client ||= Zendesk::RedisStore.client
  end

  def single_execution_key(account_id, name)
    digest = Digest::SHA1.hexdigest("/entity_type/Organization/account_id/#{account_id}/name/#{name}}")
    @single_execution_key ||= "single_execution_key_#{digest}"
  end

  def redis_expiration
    5
  end

  def enqueue_reassignment_job
    return if @added_domain_values.blank? && @removed_domain_values.blank?

    if account.has_org_reassign_domain_mapping_kill_switch?
      Rails.logger.warn "Account has org_reassign_domain_mapping_kill_switch. Skipping OrganizationReassignJob"
    else
      OrganizationReassignJob.enqueue(account_id, id, @added_domain_values, @removed_domain_values)

      remove_instance_variable(:@added_domain_values)
      remove_instance_variable(:@removed_domain_values)
    end
  end

  def enqueue_unset_job
    OrganizationUnsetJob.enqueue(account_id, id)
  end

  def attributes_for_liquid
    org_tags = if account.has_no_org_tags_for_ticket_placeholder?
      ""
    else
      current_tags
    end

    {
      "details"            => details,
      "external_id"        => external_id,
      "id"                 => id,
      "is_shared"          => is_shared,
      "is_shared_comments" => is_shared_comments,
      "name"               => name,
      "notes"              => notes,
      "tags"               => org_tags
    }
  end

  def custom_org_fields_for_liquid
    {}.tap do |org_fields|
      custom_field_values.to_liquid.each do |key, value|
        org_fields[key.to_s] = value
      end
    end
  end

  # Adds errors for invalid values used to try to build organization_domains
  # and organization_emails associations
  def validate_domain_names
    return unless @invalid_values.present?

    @invalid_values.each do |invalid_value|
      error_message = I18n.t(
        "activerecord.errors.models.organization.attributes.domain_name.invalid",
        value: invalid_value
      )
      errors.add(:domain_name, error_message)
    end
  end

  def validate_unique_name
    return unless name
    errors.add(:name, :taken) if org_exists_with_name?
  end

  def org_exists_with_name?
    orgs = Organization.unscope(where: :deleted_at).where(name: name, account_id: account_id)
    orgs = orgs.where('id != ?', id) if id
    existing_names = orgs.pluck(:name).map(&:downcase)

    # Force Ruby to do the string comparison not MySQL, because the collation
    # for MySQL considers some characters the same but we don't want that.
    # https://support.zendesk.com/agent/tickets/1532703
    existing_names.any? && existing_names.include?(name.downcase)
  end

  def update_taggings?
    !!account.try(:has_user_and_organization_tags?)
  end

  def parse_new_domain_names(new_domain_names)
    if new_domain_names.is_a?(String)
      strings = new_domain_names.split(VALID_DOMAIN_SEPARATORS)
      self.class.normalize_domain_strings(strings)
    elsif new_domain_names.is_a?(Array)
      self.class.normalize_domain_strings(new_domain_names).
        map { |new_domain_name| new_domain_name.split(VALID_DOMAIN_SEPARATORS) }.
        flatten.
        uniq
    elsif new_domain_names.nil?
      []
    else
      raise ArgumentError, "Expected String, Array, or NilClass but received #{new_domain_names.class.name}"
    end
  end

  def build_new_domains_and_emails(new_domain_values_to_add, new_email_values_to_add)
    new_domain_values_to_add.each { |new_domain_value_to_add| organization_domains.build(value: new_domain_value_to_add) }
    new_email_values_to_add.each { |new_email_value_to_add| organization_emails.build(value: new_email_value_to_add) }
  end

  def delete_removed_domains_and_emails
    if @domains_to_remove.present?
      OrganizationDomain.destroy_all(id: @domains_to_remove.map(&:id))
      organization_domains.reload
    end

    if @emails_to_remove.present?
      OrganizationEmail.destroy_all(id: @emails_to_remove.map(&:id))
      organization_emails.reload
    end

    remove_instance_variable(:@domains_to_remove) if defined?(@domains_to_remove)
    remove_instance_variable(:@emails_to_remove) if defined?(@emails_to_remove)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'organization')
  end

  def in_flight_organization_domains
    # Can't use ActiveRecord because we need to include new/unpersisted domains
    # and must hide domains marked for removal.
    organization_domains - (@domains_to_remove || [])
  end

  def in_flight_organization_emails
    # Can't use ActiveRecord because we need to include new/unpersisted emails
    # and must hide emails marked for removal.
    organization_emails - (@emails_to_remove || [])
  end

  def log_account_nil_with_stack_trace(prefix_with_nil_account)
    Rails.logger.info "#{prefix_with_nil_account} with nil account. Stack trace:\n#{caller&.send(:[], 0..25)&.join("\n")}"
  end

  class WriteInProgressError < RuntimeError; end
end
