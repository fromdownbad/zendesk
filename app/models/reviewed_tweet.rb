class ReviewedTweet < ActiveRecord::Base
  belongs_to :account

  attr_accessible :account, :tweet_id

  def self.mark_as_reviewed(account, tweet_ids)
    reviewed_tweets = reviewed(account, tweet_ids)
    tweet_ids = tweet_ids.split(',').map(&:to_i)

    (tweet_ids - reviewed_tweets).each do |tweet_id|
      account.reviewed_tweets.create(tweet_id: tweet_id, account: account)
    end
  end

  def self.reviewed(account, tweet_ids)
    account.
      reviewed_tweets.
      where(tweet_id: tweet_ids.split(',')).
      pluck(:tweet_id)
  end
end
