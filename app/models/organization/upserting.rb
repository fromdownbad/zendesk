class Organization
  module Upserting
    def update_existing_or_initialize_new_organization(current_account, current_user, params)
      if params[:organization][:id]
        begin
          organization = current_account.organizations.find(params[:organization][:id])
        rescue ActiveRecord::RecordNotFound
        end
      end

      if params[:organization][:external_id].present? && organization.nil?
        organization = current_account.organizations.find_by_external_id(params[:organization][:external_id].to_s)
      end

      org_params = params[:organization].except(:organization_fields)

      if organization.nil?
        organization = current_account.organizations.build(org_params)
      else
        if current_user.can?(:edit, Organization)
          organization.attributes = org_params
        else
          organization[:notes] = params[:organization]["notes"]
        end
      end

      if params[:organization][:organization_fields]
        custom_fields = params[:organization].delete(:organization_fields)
        organization.custom_field_values.update_from_hash(custom_fields)
      end

      organization
    end
  end
end
