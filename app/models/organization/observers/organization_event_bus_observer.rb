# Publishes organization domain events to the event bus (Kafka) via "Escape"
module OrganizationEventBusObserver
  extend ActiveSupport::Concern

  EVENT_BUS_TOPIC = 'support.organization_events'.freeze

  included do
    after_save :publish_organization_events_to_bus!
    attr_writer :domain_event_publisher
  end

  # Allow for dependency injection of the event publishing mechanism.
  #
  # This construct exists to allow tests to read the published events, as the
  # BLACKHOLE engine of Escape table can not be easily queried.
  def domain_event_publisher
    @domain_event_publisher || EscKafkaMessage
  end

  def publish_organization_events_to_bus!
    # Return early if the feature is off or we have no events to emit
    return unless organization_events_encoder.any?

    organization_events_encoder.to_a.each do |event|
      domain_event_publisher.create!(
        account_id: account_id,
        topic: EVENT_BUS_TOPIC,
        partition_key: "#{account_id}/#{id}",
        value: event.to_proto
      )
    end

    record_stats
    organization_events_encoder.clear
  rescue StandardError => e
    # For now, we prefer silently dropping domain events over crashing Organization save.
    Rails.logger.error(e)
    event_bus_statsd_client.increment('publish.errors', tags: ["exception:#{e.class}"])
    ZendeskExceptions::Logger.record(e, location: self, message: "OrganizationEventBusObserver: #{e.message}", fingerprint: 'cd964223e40949f8ba46e2fe1c3e20753ed4dbbd')
  end

  private

  def organization_events_encoder
    @organization_events_encoder ||= OrganizationEventsProtobufEncoder.new(self)
  end

  def record_stats
    event_bus_statsd_client.batch do |statsd|
      organization_events_encoder.to_a.each do |event|
        # We may be able to remove this and just use the histogram count, but
        # we suspect histogram counts may not be reliable based on parity checks
        statsd.increment('domain_event', tags: ["event:#{event.event}"])
        statsd.histogram('domain_event.message_size', event.to_proto.size, tags: ["event:#{event.event}"])
      end
    end
  end

  def event_bus_statsd_client
    @event_bus_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['support', 'event_bus', 'organization'], tags: ["protobuf_version:#{Gem.loaded_specs['zendesk_protobuf_clients'].version.version}"])
  end
end
