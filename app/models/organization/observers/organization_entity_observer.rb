module OrganizationEntityObserver
  extend ActiveSupport::Concern

  TOPIC = 'support.organization_entity'.freeze

  included do
    after_save :publish_organization_entity_snapshot!
  end

  def publish_organization_entity_snapshot!
    return unless account.has_publish_organization_entity?
    EscKafkaMessage.create!(
      account_id: account_id,
      topic: TOPIC,
      key: "#{account_id}/#{id}",
      partition_key: "#{account_id}/#{id}",
      value: organization_entity_encoder.to_proto
    )
  end

  private

  def organization_entity_encoder
    @organization_entity ||= OrganizationProtobufEncoder.new(self).to_object
  end
end
