require 'ticket_sharing/attachment'

class Attachment < ActiveRecord::Base
  CONTENT_TYPE_TO_TYPE = {
    # image
    'image/bmp' => { type: 'image', extension: 'bmp' },
    'image/gif' => { type: 'image', extension: 'gif' },
    'image/jpeg' => { type: 'image', extension: 'jpg' },
    'image/jpg' => { type: 'image', extension: 'jpg' },
    'image/png' => { type: 'image', extension: 'png' },
    'image/svg+xml' => { type: 'image', extension: 'svg' },
    'image/webp' => { type: 'image', extension: 'webp' },

    # document
    'application/msword' => { type: 'document', extension: 'doc' },
    'application/x-iwork-pages-sffpages' => { type: 'document', extension: '' },
    'application/vnd.apple.pages' => { type: 'document', extension: '' },
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => { type: 'document', extension: 'docx' },
    'application/vnd.oasis.opendocument.text' => { type: 'document', extension: 'odt' },
    'application/rtf' => { type: 'document', extension: 'rtf' },
    'text/plain' => { type: 'document', extension: 'txt' },
    'text/html' => { type: 'document', extension: 'html' },

    # pdf
    'application/pdf' => { type: 'pdf', extension: 'pdf' },

    # spreadsheet
    'application/x-iwork-numbers-sffnumbers' => { type: 'spreadsheet', extension: '' },
    'application/vnd.apple.numbers' => { type: 'spreadsheet', extension: '' },
    'application/vnd.ms-excel' => { type: 'spreadsheet', extension: 'xls' },
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => { type: 'spreadsheet', extension: 'xlsx' },
    'application/vnd.oasis.opendocument.spreadsheet' => { type: 'spreadsheet', extension: 'ods' },

    # presentation
    'application/x-iwork-keynote-sffkey' => { type: 'presentation', extension: '' },
    'application/vnd.apple.keynote' => { type: 'presentation', extension: '' },
    'application/vnd.ms-powerpoint' => { type: 'presentation', extension: 'ppt' },
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => { type: 'presentation', extension: 'pptx' },
    'application/vnd.oasis.opendocument.presentation' => { type: 'presentation', extension: 'odp' },

    # zip
    'application/zip' => { type: 'zip', extension: 'zip' },
  }.freeze
  REDACTED_FILENAME = "redacted.txt".freeze
  REDACTED_CONTENT = " ".freeze
  # Forbidden == Bidirectional unicode chars:
  FORBIDDEN_FILENAME_CHARS = /[\u061C\u200E\u200F\u202A\u202B\u202C\u202D\u202E]/
  # Replacement for forbidden chars
  UNICODE_REPLACE_CHAR = "\uFFFD".freeze

  # NOTE: The suspended ticket size limit should match the postfix inbound
  # email size limit. We have the limit so we can attach the original EML to
  # suspended tickets with suspension type “Unprocessable”.
  SUSPENDED_TICKET_SIZE_LIMIT = 50 # in megabytes
  SUSPENDED_TICKET_SIZE_LIMIT_V2 = 100 # in megabytes

  include AttachmentMixin
  include Zendesk::Serialization::AttachmentSerialization
  include Zendesk::Attachments::Stores

  # We can't `inherit: :account_id` here because the source could be an account
  belongs_to  :source, polymorphic: true
  belongs_to  :account
  belongs_to  :author, class_name: 'User', foreign_key: :author_id, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  has_one :external_url, class_name: 'AttachmentExternalURL', inherit: :account_id

  attr_accessible :author, :is_public, :source

  has_kasket_on :account_id, :id
  has_kasket_on :account_id, :token

  setup_attachment_fu(
    max_size: 1.gigabyte.to_i,
    path_prefix: 'data/attachments',
    processor: :mini_magick,
    thumbnails: {
      thumb: '80x80>'
    }
  )

  include Zendesk::AttachmentFromCloud

  validates_as_attachment

  scope :originals, -> { where(parent_id: nil) }

  before_create :ensure_token
  before_create :generate_md5
  before_save :make_public_without_source
  before_save :set_ticket_id

  after_save :timestamp_source
  after_commit :enqueue_stores_synchronization_job, if: :safe_to_synchronize_stores?
  before_destroy :timestamp_source

  # attachment_fu adds a lot of before_validation calls.
  # Make sure some of this stuff (like setting stores, filename,
  # inherits, etc) happen before them. Hence prepend all callbacks here.
  # Note that prepend: true, makes it in reverse order.
  # IOW, calls replace then inherits then evaluate
  before_validation :evaluate_processability, prepend: true, if: -> { new_record? || !account.has_process_attachment_on_create_only? }
  before_validation :set_thumbnail_inherits, prepend: true
  before_validation :replace_forbidden_filename_chars, prepend: true

  validates_presence_of :account_id
  validate :plan_size_limit
  validates_lengths_from_database only: [:filename, :display_filename]

  def self.first_by_token_and_filename(token, filename)
    display_filename = CGI.unescape(filename)

    where(token: token).
      where(
        "filename = :filename OR display_filename = :display_filename",
        filename: filename, display_filename: display_filename
      ).
      first
  end

  def self.find_by_token_and_account_id!(token, account_id)
    find_by_account_id_and_token(account_id, token) || raise(ActiveRecord::RecordNotFound)
  end

  def self.find_by_account_id_and_token(account_id, token)
    where(account_id: account_id, token: token.to_s).first
  end

  def thumbnailed?
    !thumbnail_ids.blank?
  end

  def filename=(filename)
    self.display_filename = filename
    super(filename)
  end

  def display_filename
    super || filename
  end

  def file_name_without_extension
    File.basename(display_filename, ".*")
  end

  def file_type
    return if CONTENT_TYPE_TO_TYPE[content_type].nil?

    CONTENT_TYPE_TO_TYPE[content_type][:type]
  end

  def file_extension
    return if CONTENT_TYPE_TO_TYPE[content_type].nil?

    CONTENT_TYPE_TO_TYPE[content_type][:extension]
  end

  def timestamp_source
    if source_type == "Entry" && source.try(:persisted?) && !source.readonly?
      source.touch_without_callbacks
    end
  end

  def escaped_filename
    return CGI.escape(display_filename) if display_filename
    filename
  end

  def redact!
    redacted_attachment = Attachment.new(
      account: account,
      uploaded_data: RestUpload.new(REDACTED_CONTENT, REDACTED_FILENAME, "text/plain")
    )

    if source.try(:add_redacted_attachment!, redacted_attachment)
      if account.has_email_redact_attachments_from_eml_and_json?
        audit = source.audit

        audit.raw_email.redact_attachment_from_json!(filename)
        audit.delete_eml_file!
      end

      destroy && redacted_attachment
    end
  end

  def redacted?
    filename == REDACTED_FILENAME && size == REDACTED_CONTENT.size
  end

  def url(args = {})
    args = args.reverse_merge(token: false, relative: false)
    host = args[:host] || generate_url(args[:brand]) unless args[:relative]
    "#{host}/attachments/token/#{ensure_token}/?name=#{escaped_filename}"
  end

  def generate_url(brand)
    # In the future when changing to account.route.url, we will need to add route to ticket
    # includes to avoid N+1 queries
    brand.try(:url) || account.url
  end

  def self.from_external_url(account, url, filename, content_type = nil, thumbnail = false, display_filename = nil)
    content_type ||= Zendesk::Utils::MimeTypes.try_by_filename(filename)

    attachment = new(account: account)
    attachment.content_type = content_type
    attachment.size = 1
    attachment.filename = filename
    attachment.display_filename = display_filename if display_filename

    # Do not set any data for this attachment, because we don't have any
    attachment.md5 = begin
                       digest = Digest::MD5.new
                       digest.update " "
                       digest.hexdigest
                     end

    attachment.build_external_url(account: account, url: url)
    attachment.thumbnails << from_external_url(account, url, filename, content_type, false, display_filename) if thumbnail
    attachment
  end

  # An external url attachment is not
  # saved in stores
  def not_saved_in_stores?
    super || external_url.present?
  end

  # ExternalUrlAttachment should really be a different class
  def current_data
    if external_url.present?
      return " "
    end
    super
  end

  def self.from_attachment(attachment)
    return unless attachment && attachment.current_data
    # If image attachment does not have a thumbnail, it implies the
    # attachment_fu failed to create one for it. We don't want to crash
    # here again.
    if attachment.image? && attachment.thumbnails.empty?
      Rails.logger.warn("Attachment #{attachment.id} in account #{attachment.account_id} is corrupted!")
      return
    end

    new_attachment = new(account: attachment.account)
    new_attachment.set_temp_data(attachment.current_data)
    new_attachment.size = attachment.size
    new_attachment.content_type = Zendesk::Utils::MimeTypes.mime_type_to_extension(attachment.content_type) ? attachment.content_type : "application/unknown"
    new_attachment.filename = attachment.filename
    new_attachment.display_filename = attachment.display_filename
    new_attachment
  end

  def ensure_md5!
    unless md5.present?
      generate_md5
      save
    end
    md5
  end

  def self.find_by_url(url)
    url = URI.parse(url)
    return unless token = url.path[%r{/(\w+)/?$}, 1]

    account =
      if find_via_routes?
        route = Route.find_by_fqdn(url.host)
        route.try(:account)
      else
        Account.find_by_fqdn(url.host)
      end
    return unless account

    account.on_shard do
      Attachment.find_by_account_id_and_token(account.id, token)
    end
  end

  def self.find_by_cdn_domain_url(url)
    url = URI.parse(url)
    token = CGI.parse(url.query)["token"].first
    return unless token

    token = Zendesk::Attachments::Token.new(token: token)
    account = Account.find_by_id(token.account_id)
    account.on_shard do
      Attachment.where(account_id: token.account_id, id: token.attachment_id).first
    end
  end

  def self.find_via_routes?
    Arturo::Feature.find_feature(:multibrand).try(:phase) == "on" || !Rails.env.production?
  end

  def for_partner
    TicketSharing::Attachment.new(
      'url' => url,
      'filename' => filename,
      'display_filename' => display_filename,
      'content_type' => content_type
    )
  end

  def thumbnailable?
    external_url.blank? && super && valid_dimensions?
  end

  def remove_from_remote!
    return if stores.empty?
    # removes everything from active stores
    destroy_files
    update_column(:stores, '')
  rescue StandardError => e
    type = e.class.name.underscore
    statsd_client = Zendesk::StatsD::Client.new(namespace: %w[attachment remove_from_remote])
    statsd_client.increment('error', tags: ["exception:#{type}"])
  end

  private

  def destroy_thumbnails?
    external_url.blank?
  end

  def set_ticket_id
    if source # might be nil for an archived ticket!  In which case leave in place
      self.ticket_id = if source.is_a?(Comment)
        source.ticket_id
      end
    end
  end

  def make_public_without_source
    self.is_public = true if source.nil?
  end

  def set_thumbnail_inherits
    return unless parent
    self.account_id = parent.account_id
    self.author_id  = parent.author_id
    self.is_public  = parent.is_public
    self.source     = parent.source
  end

  def evaluate_processability
    # hard disable resizing and thumbnail creation
    # if dimensions are not valid
    # XXX This is already handled by attachment_fu, please remove in future.
    @no_thumbnails ||= !valid_dimensions?
    @no_resize = @no_thumbnails
    true
  end

  def ensure_token
    self.token ||= Token.generate
  end

  def plan_size_limit
    return unless stores.present? && size
    if source.is_a?(SuspendedTicket)
      if account.has_email_increase_attachment_size_limit?
        if size > SUSPENDED_TICKET_SIZE_LIMIT_V2.megabytes
          Rails.logger.info("Suspended Ticket Account id: #{account_id} Attachment id: #{id} size: #{size} is greater than #{SUSPENDED_TICKET_SIZE_LIMIT_V2} MB limit - email_increase_attachment_size_limit enabled")
          errors.add(:base, I18n.t('txt.errors.attachments.size_invalid', limit: SUSPENDED_TICKET_SIZE_LIMIT_V2))
        end
      else
        if size > SUSPENDED_TICKET_SIZE_LIMIT.megabytes
          errors.add(:base, I18n.t('txt.errors.attachments.size_invalid', limit: SUSPENDED_TICKET_SIZE_LIMIT))
        end
      end
    elsif !valid_size?
      errors.add(:base, I18n.t('txt.errors.attachments.size_invalid', limit: account.subscription.file_upload_cap))
    end
  end

  def safe_to_synchronize_stores?
    return if external_url.present?
    return unless source_change = previous_changes["source_type"]
    source_change.last != "Token" && thumbnail.nil?
  end

  def replace_forbidden_filename_chars
    # Any non-alphanumeric in filename field are being replaced w/underscores by attachment_fu.sanitize_filename
    display_filename.gsub!(FORBIDDEN_FILENAME_CHARS, UNICODE_REPLACE_CHAR) unless display_filename.nil? || display_filename.frozen?
  end
end
