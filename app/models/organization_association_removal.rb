class OrganizationAssociationRemoval
  MAX_BACKOFF_DURATION = 3.seconds

  def initialize(account_id, organization_id, user_ids)
    self.account_id      = account_id
    self.organization_id = organization_id
    @user_ids            = user_ids
    self.logger          = Rails.logger
  end

  def call
    if @user_ids.empty?
      logger.info("Nothing will be deleted or unset with organization_id: #{organization_id} because users_ids is empty!!!")
      return
    end

    if account.has_multiple_organizations_enabled? || account.has_org_association_always_remove_with_fallback?
      one_or_zero = user_ids_with_one_or_zero_memberships(@user_ids)
      bulk_remove(one_or_zero)
      remove_each_with_backoff(@user_ids - one_or_zero) # need proper fallback
    else
      bulk_remove(@user_ids)
    end
    organization
  end

  private

  attr_accessor :account_id, :organization_id, :logger

  def bulk_remove(user_ids)
    bulk_delete_organization_memberships(user_ids)
    bulk_unset_organization_ids(user_ids)
    bulk_unset_organization_on_open_tickets(user_ids)
  end

  def user_ids_with_one_or_zero_memberships(user_ids)
    account.users.where(id: user_ids).joins(:organization_memberships).having("count(organization_memberships.id) <= 1").pluck(:id)
  end

  def organization
    Organization.find_by_id(organization_id)
  end

  def bulk_delete_organization_memberships(user_ids)
    organization_memberships(user_ids).delete_all
    logger.info(
      "Deleted OrganizationMemberships with organization_id: #{organization_id} for #{user_ids.size} users"
    )
  end

  def bulk_unset_organization_ids(user_ids)
    scope = account.users.where(id: user_ids)
    scope.update_all_with_updated_at(organization_id: nil)
    scope.find_each(&:clear_kasket_indices)
    logger.info("Unset organization_id: #{organization_id} for #{user_ids.size} users")
  end

  def bulk_unset_organization_on_open_tickets(user_ids)
    account.users.where(id: user_ids).find_each do |user|
      user.move_tickets_to_organization(organization_id, nil)
    end

    logger.info(
      "Unset organization_id: #{organization_id} on open tickets for #{user_ids.size} users"
    )
  end

  def account
    @account ||= Account.find(account_id)
  end

  def organization_memberships(user_ids)
    account.organization_memberships.where(user_id: user_ids, organization_id: organization_id)
  end

  def remove_each_with_backoff(user_ids)
    organization_memberships(user_ids).find_each do |membership|
      membership.destroy

      # Deletes are very exensive because they create full table locks in MySQL. This will monitor the
      # database load and lag to ensure that we don't flood deletes and cause cascading performance hits.
      # We set a maximum backoff duration here because this job does need to happen reasonably quickly.
      backoff_duration = [database_backoff.backoff_duration, MAX_BACKOFF_DURATION].min
      Rails.logger.info("[OrganizationAssociationRemoval] backing off deletes for #{backoff_duration} seconds")
      Rails.logger.info("[OrganizationAssociationRemoval] #{database_backoff.to_hash}")
      sleep(backoff_duration)
    end
  end

  def database_backoff
    @database_backoff ||= Zendesk::DatabaseBackoff.new(monitor_maxwell_filters: false)
  end
end
