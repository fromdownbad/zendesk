class MobileSdkSettings < ActiveRecord::Base
  belongs_to :account
  attr_accessible :auth_endpoint, :auth_token, :enabled

  validates_presence_of :account_id, :auth_endpoint, :auth_token
  validates_uniqueness_of :account_id

  def auth_token(force_full = false)
    token = read_attribute(:auth_token)

    if new_record? || force_full
      token
    elsif token
      token[0..9]
    end
  end

  def auth_token=(token)
    if new_record?
      write_attribute(:auth_token, token)
    else
      message = "Not allowed to update auth_token"
      ZendeskExceptions::Logger.record(StandardError.new(message), location: self, message: message, reraise: !Rails.env.production?, fingerprint: 'd16dc3f9c5290d2fef3faba538e1bff56947bba7')
    end
  end
end
