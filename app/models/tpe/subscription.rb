require 'cia'

module Tpe
  class Subscription < ActiveRecord::Base
    include ::CIA::Auditable
    include Zendesk::ForbiddenAttributesProtection

    self.table_name = 'tpe_subscriptions'

    audit_attribute :account_id,
      :active,
      :plan_type,
      :max_agents,
      callback: :after_commit

    belongs_to :account

    DEFAULT_TRIAL_AGENTS = 5

    attr_accessible :plan_type, :max_agents

    alias_attribute :is_active, :active # Note: Added for parity with other products
    alias_attribute :include_in_easy_agent_add, :active

    after_commit :update_partner_edition_account, if: :relevant_attrs_changed?

    def deactivate
      self.active = false
      save!
    end

    # Trials are managed in 'voice_partner_edition_accounts' table
    # This model only manages purchased accounts so always return false here
    # Note: Added for parity with other products
    def is_trial? # rubocop:disable Naming/PredicateName
      false
    end

    def plan_name
      ZBC::Tpe::PlanType[plan_type].try(:description)
    end

    private

    def update_partner_edition_account
      voice_partner_edition_account = Voice::PartnerEditionAccount.
        find_or_create_by_account(account)
      voice_partner_edition_account.updated_subscription!(self)
    end

    def relevant_attrs_changed?
      (previous_changes.keys & %w[max_agents plan_type active]).present?
    end
  end
end
