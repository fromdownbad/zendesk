class ApiActivityClient < ActiveRecord::Base
  attr_accessible
  belongs_to :account
  validates_presence_of :account_id

  API_ACTIVITY_COUNT_KEY_VERSION = 'v5'.freeze

  def update_client(current_hour, last_request_time, hours_to_store)
    count = Rails.cache.read(hour_count_key(current_hour)).to_i
    peak_count = Rails.cache.read(peak_count_hour_key(current_hour)).to_i
    parsed_data = {}.with_indifferent_access
    if count > 0 || peak_count > 0
      begin
        parsed_data = JSON.parse(data).with_indifferent_access || {}.with_indifferent_access
      rescue JSON::ParserError, KeyError, TypeError
        parsed_data = {}.with_indifferent_access
      end

      parsed_data[current_hour.to_s] = {count: count, peak_count: peak_count}
      self.data = parsed_data.to_json
      self.last_request_time = last_request_time
    end
    updated_count = count_last_24_hours(parsed_data, current_hour)
    cleared_old_data = clear_old_data(parsed_data, current_hour, hours_to_store)
    count > 0 || peak_count > 0 || cleared_old_data || updated_count
  end

  # count_data:
  # {"411177"=>{"count"=>18001, "peak_count"=>1300}, "411278"=>{"count"=>9001, "peak_count"=>1200},
  #  "411179"=>{"count"=>18001, "peak_count"=>1300}, "411280"=>{"count"=>9001, "peak_count"=>1200},
  #  "411281"=>{"count"=>18001, "peak_count"=>1300}...
  # keys:
  # ["411187", "411278", "411179", "411279", "411280", ...
  # Time.now => 2016-11-28 10:00:00
  # current_hour = Time.now.to_i/1.hour => 411202
  # first_hour of previous day => 411168 => 2016-11-27 00:00:00
  def count_previous_day(count_data, current_hour)
    keys =  count_data.empty? ? [] : count_data.keys.sort
    current_time = Time.at(current_hour * 1.hour)
    day_to_count = (current_time.to_date - 1.day)
    # get the first hour of the midnight hour of day
    first_hour = day_to_count.to_time.to_i / 1.hour
    last_hour = first_hour + 23
    count = 0
    keys.each do |key|
      next unless key.to_i >= first_hour && key.to_i <= last_hour
      hour_count = count_data[key][:count].to_i
      count += hour_count if hour_count > 0
    end
    count
  end

  def clear_old_data(count_data, current_hour, hours_to_store)
    last_hour_to_delete = current_hour - hours_to_store
    keys = count_data.empty? ? [] : count_data.keys.sort
    num_hours_deleted = 0

    keys.each do |key|
      if key.to_i <= last_hour_to_delete
        count_data.delete(key)
        num_hours_deleted += 1
      end
    end
    self.data = count_data.to_json if num_hours_deleted > 0

    num_hours_deleted > 0
  end

  def count_last_24_hours(count_data, current_hour)
    old_count = total_count_last_24_hours
    self.total_count_last_24_hours = count_previous_day(count_data, current_hour)

    # did we get a new value?
    total_count_last_24_hours != old_count
  end

  def hour_count_key(current_hour_window)
    "api-activity/#{API_ACTIVITY_COUNT_KEY_VERSION}/#{account_id}/#{client_key}/count/#{current_hour_window}"
  end

  def peak_count_hour_key(current_hour_window)
    "api-activity/#{API_ACTIVITY_COUNT_KEY_VERSION}/#{account_id}/peak_count/#{current_hour_window}"
  end
end
