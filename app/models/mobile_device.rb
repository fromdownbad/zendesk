class MobileDevice < Device
  belongs_to :oauth_token, class_name: 'Zendesk::OAuth::Token', dependent: :destroy

  before_destroy :revoke_token

  attr_accessible

  def mobile?
    true
  end

  def create_token_specific_device!(request:, token:, device:)
    new_device = user.mobile_devices.build(
      account: account,
      name: device.fetch('name'),
      token: device.fetch('identifier'),
      user_agent: request.user_agent
    )
    new_device.oauth_token = token.dup.tap { |t| [:token, :expires_at, :last_used_at].each { |f| t[f] = nil } }
    new_device.save!
    new_device
  end

  def register_token!(token)
    if oauth_token_id
      Rails.logger.warn "Deleting oauth token #{oauth_token_id} since we are creating a new token for this device"
      oauth_token.try(:destroy)
    end

    self.oauth_token = token
    save!
  end

  def revoke_token
    if oauth_token
      oauth_token.destroy
    else
      user.tokens.find_by_id(token).try(:destroy)
    end
  end

  protected

  def parse_name
    return unless name.blank?

    self.name = user_agent
  end
end
