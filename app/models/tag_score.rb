# A tag score is also know as a Tag Cloud when generally speaking at Zendesk.
# The TagScore is used by agents to see how many Taggings are associated to non-archived tickets.
#
# SEARCH TERMS: TagCloud, TagScore, tag_score, tag_scores, tagscore. tagcloud, Tagging, tag_name
#
# BACKGROUND JOB
#   The Tag Score is only updated on some requests adding a Tagging.  In order to have the TagScore
#   updated a nightly job (ThesaurusResetJob) is run. (view the schedule in config/resque-schedule.yml)
#
# CREATE NEW TagScores
#   If a Tagging with a new `tag_name` is created a new TagScore will be created with a score of 1.
#
# UPDATING TagScore (outside of Background Jobs)
#   There are places in the app we will manually call TagScore.update.
#
# NOTICE if the background job (ThesaurusResetJob) is changed you should ensure `TagScore.update`
#   is also changed to mimic these changes
#
class TagScore < ActiveRecord::Base
  include Zendesk::Serialization::TagScoreSerialization

  disable_global_uid
  belongs_to :account

  attr_accessible :account, :tag_name, :score

  ignore_column :enhanced_tag_name

  def self.update(account, tag_name)
    # Score is only for taggings within the last 60 days.
    count = Tagging.where(account_id: account.id, tag_name: tag_name).
      where('created_at >= ?', 60.days.ago.beginning_of_day).count(:all)

    # Ensure we're not racing and inserting duplicate entries in TagScores
    # for the same tag
    begin
      current_attempt = 0
      Zendesk::Retrier.retry_on_error([ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound], 2) do
        current_attempt += 1
        ActiveRecord::Base.transaction do
          delete_all(account_id: account.id, tag_name: tag_name)
          if count > 0
            replace_score(account, tag_name, count)
          end
        end
      end
    rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound
      Rails.logger.info("TagScore.update failed after 2 tries on account id #{account.id}, tag_name #{tag_name}")
      statsd_client.increment('update.deadlock')
    end
  end

  def self.replace_score(account, tag_name, score)
    san_sql = Tagging.send(:sanitize_sql, ["(?, ?, ?, ?)", account.id, tag_name, score, Time.now])
    TagScore.connection.execute('REPLACE INTO tag_scores (account_id, tag_name, score, updated_at) VALUES ' + san_sql)
  end

  def self.statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'tagscore')
  end
end
