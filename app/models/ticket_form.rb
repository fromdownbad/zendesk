# TicketForms are customizable forms that accounts setup for creating tickets
#
#  ------------------------              ------------------------             ------------------------
#  |    TicketField       |              |  TicketFieldEntry    |             |       Ticket         |
#  |                      |-------------<|                      |>------------|                      |
#  | type                 |              |                      |             |                      |
#  | title                |              |                      |             |                      |
#  | description          |              ------------------------             ------------------------
#  |  .                   |
#  |  .                   |              ------------------------
#  |  .                   |-------------<|   TicketFormField    |
#  |  .                   |              |  position            |
#  |  .                   |              |                      |
#  ------------------------              ------------------------
#             |                                    \ /
#            / \                                    |
#  ------------------------              ------------------------
#  |  CustomFieldOption   |              |     TicketForm       |
#  |  name                |              |  name                |
#  |  value               |              |  display_name        |
#  |  position            |              |  position            |
#  ------------------------              ------------------------
#
# SEARCH TERMS: ticket_forms ticket_fields ticket_taggers ticket_field_entries ticket_form_fields custom_field_options TicketForm Ticket Form TicketField TicketTagger TicketFieldEntry TicketFormField CustomFieldOption
#
# FUNCTIONALITY
#   Initially a default ticket_form is created on account upgrade to Enterprise plans. Non-enterprise accounts
#   do not have a default ticket form. After which, on Enterprise accounts
#   some users in an account have permissions to add more ticket_forms.
#
#   As the UML shows, a ticket form has many TicketFields.  As with all HTML forms
#   the ticket forms can have dropdown & text & checkboxs.  This is determined by the
#   TicketField "type".
#
#   If for example the TicketField represents a dropdown in the UI the TicketField will
#   have many dropdown options (custom_field_options)
#
#   Finally a ticket is assigned a value through the ticket_field_entries table
#
require 'zendesk/radar_factory'
class TicketForm < ActiveRecord::Base
  include CachingObserver
  include CIA::Auditable

  has_soft_deletion default_scope: true

  # include after soft_deletion
  include TicketForm::TicketFieldConditions

  belongs_to :account
  has_many :ticket_form_fields, -> { order(:position) }, class_name: "TicketFormField", autosave: true, dependent: :destroy, inherit: :account_id
  has_many :ticket_fields, through: :ticket_form_fields, inherit: :account_id
  has_many :ticket_form_brand_restrictions, autosave: true, dependent: :destroy, inherit: :account_id
  has_many :restricted_brands, through: :ticket_form_brand_restrictions, source: :brand, inherit: :account_id

  attr_accessible :account, :name, :display_name, :end_user_visible, :position, :active, :default, :in_all_brands

  scope :deleted,              -> { where("deleted_at is not null") }
  scope :end_user_visible,     -> { where(end_user_visible: true) }
  scope :active,               -> { where(active: true) }
  scope :default,              -> { where(default: true) }
  scope :in_all_brands,        -> { where(in_all_brands: true) }

  before_validation :make_default_visible
  before_validation :ensure_display_name

  after_save     :highlander_rules, if: :made_default?
  after_save     :reorder_all_forms, if: :active_changed?

  after_commit   :notify_radar, on: :update

  before_soft_delete :validate_not_default
  before_soft_delete :reset_position
  before_soft_delete :deactivate

  validate :validate_default_is_active
  validate :validate_one_default_present
  validate :validate_ticket_forms_limits, on: :create

  validates_presence_of   :account_id
  validates_presence_of   :name
  validates_presence_of   :display_name, if: proc { |f| f.end_user_visible }
  validates_presence_of   :position, if: proc { |f| f.deleted_at.nil? }

  audit_attribute :default

  def self.reorder_forms(ids)
    transaction do
      ids.each_with_index do |id, position|
        find(id).update_attribute(:position, position)
      end
    end
  end

  def self.init_default_form(account)
    default_field_ids = account.ticket_fields.active.map(&:id)
    params = { ticket_form: { name: I18n.t('txt.admin.model.ticket_form.default_form_name'), display_name: I18n.t('txt.admin.model.ticket_form.default_form_name'),
                              default: true, end_user_visible: true, position: 1, ticket_field_ids: default_field_ids } }
    Zendesk::TicketForms::Initializer.new(account, params).ticket_form
  end

  def brands
    if in_all_brands?
      account.active_brands
    else
      restricted_brands.active
    end
  end

  def in_all_brands=(val)
    return if val.nil?
    super
  end

  # Get ticket field using objects loaded in presenter to avoid N+1 queries
  def ticket_field(ticket_field_id)
    ticket_form_fields.detect { |tff| tff.ticket_field_id == ticket_field_id }.try(:ticket_field)
  end

  protected

  def validate_ticket_field_limits
    return true unless account.has_ticket_field_limits?
    if ticket_form_fields.size > account.settings.ticket_fields_limit
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.limit_number_of_fields', max_number_of_tags: account.settings.ticket_fields_limit))
    end
  end

  # Limit accounts to have no more than 'account.settings.ticket_forms_limit' ticket forms.
  def validate_ticket_forms_limits
    if account.ticket_forms.count >= account.settings.ticket_forms_limit
      errors.add(:base, I18n.t('txt.admin.model.ticket_form.forms_product_limit', forms_limit: account.settings.ticket_forms_limit))
    end
  end

  def make_default_visible
    if default == true
      self.end_user_visible = true
    end
  end

  def ensure_display_name
    self.display_name ||= name if changes.include?("end_user_visible")
  end

  def validate_default_is_active
    if made_default? && active == false
      errors.add(:base, I18n.t('txt.admin.model.ticket_form.cannot_make_inactive_form_default') + " " + I18n.t('activerecord.errors.messages.invalid'))
    elsif made_inactive? && default == true
      errors.add(:base, I18n.t('txt.admin.model.ticket_form.cannot_make_default_form_inactive') + " " + I18n.t('activerecord.errors.messages.invalid'))
    end
  end

  def deactivate
    update_attribute(:active, false)
  end

  def highlander_rules
    account.ticket_forms.where(default: true).where.not(id: id).update_all_with_updated_at(default: false)
  end

  def validate_not_default
    if default == true
      errors.add(:base, I18n.t('txt.admin.model.ticket_form.cannot_delete_default_form'))
      false
    end
  end

  def reset_position
    self.position = nil
  end

  def made_default?
    default_changed? && default == true
  end

  def made_inactive?
    active_changed? && active == false
  end

  def validate_one_default_present
    if default_changed? && !default && account.ticket_forms.default.first == self
      errors.add(:base, I18n.t('txt.admin.model.ticket_form.cannot_change_default_form') + " " + I18n.t('activerecord.errors.messages.invalid'))
    end
  end

  def reorder_all_forms
    return unless active_changed?
    forms = account.ticket_forms.where.not(id: id).order(:position).all
    active_ids, inactive_ids = forms.partition(&:active?)

    if active?
      active_ids.push(self)
    else
      inactive_ids.unshift(self)
    end
    TicketForm.reorder_forms((active_ids + inactive_ids).map(&:id))
  end

  def notify_radar
    value = { updated_at: updated_at.to_i }

    ::RadarFactory.create_radar_notification(account, "ticket_form/#{id}").send("updated", value.to_json)
  end
end
