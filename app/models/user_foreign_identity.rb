require 'zendesk/models/user_foreign_identity'

class UserForeignIdentity < UserIdentity
  validates_format_of :value, with: /\A[\w\d]+\z/
  before_save :set_verified

  def self.identity_type
    :foreign
  end

  def serialization_options(options = {})
    options
  end

  private

  def set_verified
    self.is_verified = true
  end

  # @overwrite
  def can_be_added_to_foreign_users?
    true
  end
end
