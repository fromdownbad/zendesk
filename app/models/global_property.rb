class GlobalProperty < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible :owner, :name, :value

  validates_presence_of :owner
  validates_uniqueness_of :owner, case_sensitive: false

  validates_presence_of :name

  validates_presence_of :value

  scope :for_owner, ->(owner) { where(owner: owner) }

  def self.lookup(owner, name)
    GlobalProperty.find_by_owner_and_name(owner, name).try(:value)
  end
end
