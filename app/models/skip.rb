class Skip < ActiveRecord::Base
  attr_accessible :ticket_id,
    :user_id,
    :reason

  belongs_to :account
  belongs_to :ticket, inherit: :account_id
  belongs_to :user, inherit: :account_id
end
