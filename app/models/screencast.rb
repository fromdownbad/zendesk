# Collects all Screencast information and business logic
#
# Examples
#
#   screencast = Zendesk::Screencast.new({'position' => 3, 'url' => "http://support1.viewscreencasts.com/embed/123abcd"})
#   screencast.url
#   => "http://support1.viewscreencasts.com/embed/123abcd"
#
class Screencast
  # Public: It generates an Array of Screencasts based on an Audit's Metadata
  #
  # metadata - The Hash containing the metadata used to generate the Array.
  #            it should include the following structure:
  #            { :system => { :screencast => Array } }
  #
  # Returns an Array.
  def self.screencasts_from_metadata(metadata)
    return [] if metadata.blank?
    screencasts = metadata.fetch(:system, {}).fetch(:screencasts, {})
    screencasts.map do |params|
      new(params)
    end
  end

  # Public: generates an array of Hashes each one representing a Screencast
  #         based on a Ticket Sharing Comment received from the
  #         Networked Help Desk API (http://networkedhelpdesk.org/api/)
  #
  # ts_comment - A TicketSharing::Comment
  #              According to convention the Screencasts Array is in:
  #              ts_comment.custom_fields['zendesk']['metadata']['screencasts']
  #
  # Returns an Array ready to be passed to Audit's metadata Screencasts
  # audit.metadata[:custom][:screencasts].
  def self.screencasts_from_ticket_sharing(ts_comment)
    return [] if ts_comment.blank? || ts_comment.custom_fields.blank?
    screencast_data = ts_comment.custom_fields.fetch('zendesk', {}).fetch('metadata', {}).fetch('screencasts', {})

    screencast_data.map do |screencast_json|
      {'position'  => screencast_json['position'],
       'url'       => screencast_json['url'],
       'id'        => screencast_json['id'],
       'thumbnail' => screencast_json['thumbnail']}
    end
  end

  def self.fields(*args)
    @fields = args
    attr_accessor *args
  end

  def self.fields_list
    @fields || []
  end

  fields :id, :position, :url, :thumbnail

  # Create a new Screencast.
  #
  # params - A Hash defining a Screencast (coming from Audit Metadata)
  #
  def initialize(params = {})
    self.class.fields_list.each do |attribute|
      send("#{attribute}=", params[attribute.to_s]) if params.key?(attribute.to_s)
    end
  end

  # Public: Returns a string that defines a Screenr as a word
  #
  # It will contains two informations, its position and the word Screenr I18n.
  #
  # Returns a String
  def display_name
    return if @position.blank?
    # TODO: Consider refactoring I18n away into a presenter.
    "#{I18n.t('txt.models.screencast.screencast_display_name')} #{@position}"
  end

  # Public: the presentation of a screencast to be used in ticket sharing realm.
  #
  # We send to 3rd party only the needed attributes.
  #
  # Returns an Hash.
  def for_partner
    {'id'         => id,
     'position'   => position,
     'url'        => url,
     'thumbnail'  => thumbnail}
  end
end
