class TicketDeflection < ActiveRecord::Base
  ENQUIRY_DELIMITER = 'answerbot_subject_description_flag'.freeze
  STATE_NO_ACTION = 0
  STATE_SOLVED = 1
  STATE_NOT_SOLVED = 2
  STATE_SOLVED_PENDING = 3
  STATE_UNDONE = 4
  EMAIL_RESOLUTION_CHANNEL = 'email'.freeze

  belongs_to :ticket, inverse_of: :ticket_deflection, inherit: :account_id
  belongs_to :user, inherit: :account_id
  belongs_to :account
  belongs_to :brand, inherit: :account_id
  has_many   :ticket_deflection_articles, inherit: :account_id
  validates :brand, presence: true, on: :create
  validates :deflection_channel_id, presence: true, on: :create
  validates :enquiry, presence: true, on: :create

  validates_lengths_from_database only: [:enquiry, :detected_locale, :model_version, :interaction_reference]

  validates_presence_of :account
  validates_uniqueness_of :ticket_id, allow_nil: true, unless: :answer_bot_for_agents?

  def solve_with_article(article_id, resolution_channel_id)
    article = ticket_deflection_articles.where(article_id: article_id).first

    update_attributes(
      solved_article_id: article_id,
      state_updated_at: Time.now,
      state: STATE_SOLVED,
      via_id: ticket.via_id,
      resolution_channel_id: resolution_channel_id
    )

    return unless article

    article.update_solved_article(state_updated_at)
  end

  def solve_with_article_no_via_id(article_id, resolution_channel_id)
    article = ticket_deflection_articles.where(article_id: article_id).first

    update_attributes(
      solved_article_id: article_id,
      state_updated_at: Time.now,
      state: STATE_SOLVED,
      resolution_channel_id: resolution_channel_id
    )

    return unless article

    article.update_solved_article(state_updated_at)
  end

  def solved_pending?
    state == STATE_SOLVED_PENDING
  end

  def solved?
    state == STATE_SOLVED
  end

  def mark_as_not_solved(resolution_channel_id)
    return if solved?

    update_attributes(
      state_updated_at: Time.now,
      state: STATE_NOT_SOLVED,
      resolution_channel_id: resolution_channel_id
    )
  end

  def deconstruct_enquiry
    subject, description = enquiry.split(ENQUIRY_DELIMITER).reject(&:blank?).map(&:strip)

    if subject && description
      {
        subject: subject,
        description: description
      }
    else
      {
        subject: subject,
        description: subject
      }
    end
  end

  # This should be moved out (only called in TicketSolverJob).
  # can change TicketSolverJob to use RequestSolver for populating this audit event
  def add_solved_information_to_ticket(article_id)
    return if ticket.solved?

    ticket.will_be_saved_by(User.system)
    article_info = info_for_article(article_id)
    if article_info
      ticket.audit.events << AnswerBot::EventCreator.solve_event(
        article_info.slice(:id, :title, :url, :html_url)
      )
    end
    ticket.save!
  end

  private

  def info_for_article(article_id)
    article_locale = TicketDeflectionArticle.locale(self, article_id)
    help_center_client.fetch_article(article_id, article_locale)
  end

  def help_center_client
    @help_center_client ||= Zendesk::HelpCenter::InternalApiClient.new(account: account, requester: ticket.requester, brand: brand)
  end

  def creation_to_acceptance_delta(accepted_at:, created_at:)
    accepted_at.to_i - created_at.to_i
  end

  def answer_bot_for_agents?
    deflection_channel_id == ViaType.ANSWER_BOT_FOR_AGENTS
  end
end
