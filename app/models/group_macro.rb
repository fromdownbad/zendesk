class GroupMacro < ActiveRecord::Base
  self.table_name = :groups_macros

  belongs_to :group, inherit: :account_id
  belongs_to :macro, inherit: :account_id

  attr_accessible
end
