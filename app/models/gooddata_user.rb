class GooddataUser < ActiveRecord::Base
  LOGIN = 'zendesk-gooddata+%{user_id}@%{domain}'.freeze

  attr_accessible

  belongs_to :account
  belongs_to :user, inherit: :account_id

  def self.for_user(user)
    where(account_id: user.account_id, user_id: user.id).first
  end

  def self.gooddata_login_for_user(user)
    LOGIN % {
      user_id: user.id,
      domain: Rails.configuration.gooddata_login_domain
    }
  end

  def self.users_for_integration(gooddata_integration)
    where(
      account_id: gooddata_integration.account_id,
      gooddata_project_id: gooddata_integration.project_id
    )
  end

  def self.users_for_account(account)
    where(account_id: account.id)
  end

  delegate :first_name, :last_name, :email, to: :user

  def gooddata_login
    self.class.gooddata_login_for_user(user)
  end

  # Calculate the relevant GoodData role, based on mapping documented in
  # https://zendesk.atlassian.net/wiki/pages/viewpage.action?pageId=62128387
  def gooddata_role
    # Facts in the permission set take precedence over role membership
    if user.has_permission_set?
      case user.permission_set.permissions.report_access
      when 'readonly'
        return :read_only_user
      when 'full'
        return :editor
      else
        return :none
      end
    end

    if user.is_admin? || user.is_agent?
      return :editor
    end

    :none
  end

  def gooddata_role?(role)
    gooddata_role == role.to_sym
  end
end
