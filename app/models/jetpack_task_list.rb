class JetpackTaskList
  def initialize(user)
    @user = user
    @attributes = TaskAttributes.new(@user)
  end

  def all
    tasks = persisted_tasks
    @attributes.all.map do |attrs|
      task = pluck(tasks, attrs[:key])
      if task
        attrs.keys.each { |k| task.send("#{k}=".to_sym, attrs[k]) }
      else
        attrs[:status] ||= 'new'
        task = JetpackTask.new(attrs)
      end
      task
    end
  end

  def update(key, status)
    attrs = @attributes.attributes_for_key(key)
    tasks = JetpackTask.for_owner(attrs[:owner])
    task  = pluck(tasks, key)
    unless task
      task = JetpackTask.new
    end
    attrs[:status] = status
    task.update_attributes(attrs)
    task
  end

  private

  def persisted_tasks
    if @user.is_admin?
      JetpackTask.for_owner(@user) + JetpackTask.for_owner(@user.account)
    else
      JetpackTask.for_owner(@user)
    end
  end

  def pluck(tasks, key)
    tasks.detect { |t| t.key == key }
  end

  class TaskAttributes
    class UnknownTaskTypeError < StandardError
    end

    def initialize(user)
      @user = user
    end

    def attribute_collection
      build_attribute_collection
    end

    # The order in which group gets added first matters for logic in Lotus
    # to determine the next incomplete task.
    def all
      [].tap do |list|
        sections.each do |section|
          list.concat attribute_collection_for_owner(section)
        end
      end
    end

    def sections
      experimental_attributes.keys.delete_if do |k|
        [:configure, :channels].include?(k) && !@user.is_admin?
      end
    end

    def attributes_for_key(key)
      found = nil
      attribute_collection.keys.each do |task_type|
        found = attribute_collection[task_type].detect do |attrs|
          attrs[:key] == key
        end
        break if found
      end
      raise(UnknownTaskTypeError, "unknown task type: #{key}") if found.nil?
      found
    end

    private

    def rename_task(old_name, new_name)
      attribute_collection.each do |key, values|
        attribute_collection[key] = values.map do |hash|
          if hash[:key] == old_name
            hash[:key] = new_name
          end
          hash
        end
      end
    end

    def in_product_discovery_experiment?
      false # NOTE: stubbing for now till deeper cleanup is possible
    end

    def experimental_attributes
      ATTRIBUTE_COLLECTION
    end

    ATTRIBUTE_COLLECTION = {
      configure: [
        { key: 'register',             section: 'admin', status: 'complete', enabled: false },
        { key: 'configure_email_v2',   section: 'admin' },
        { key: 'add_agents',           section: 'admin' },
        { key: 'configure_web_portal', section: 'admin' },
        { key: 'benchmark',            section: 'admin' }
      ],

      user: [
        { key: 'update_profile', section: 'agent' },
        { key: 'test_ticket',    section: 'agent' },
        { key: 'create_view',    section: 'agent' },
        { key: 'create_macro',   section: 'agent' }
      ],

      channels: [
        { key: 'add_feedback_tab', section: 'channels' },
        { key: 'add_chat',         section: 'channels' },
        { key: 'add_facebook',     section: 'channels' },
        { key: 'add_twitter',      section: 'channels' },
        { key: 'add_voice',        section: 'channels' }
      ]
    }.freeze

    def apply_customizations!
      apply_email_customizations

      unless in_product_discovery_experiment?
        apply_web_portal_customization
        apply_feedback_customizations
        apply_marketing_customizations
      end

      # After applying customizations, remove unsupported tasks,
      # and replace chat with zopim if supported
      unless @user.account.subscription.has_chat?
        remove_task('add_chat')
      end

      rename_task("add_chat", "add_zopim_chat_v2")
    end

    def apply_web_portal_customization
      if (@user.account.help_center_onboarding_state == :enabled) ||
        (@user.account.help_center_state != :disabled)
        if (attrs = attributes_for_key('configure_web_portal'))
          attrs[:key] = 'add_help_center'
        end
      end
    end

    def apply_email_customizations
      if @user.account.created_via_google_app_market?
        if (attrs = attributes_for_key('configure_email_v2'))
          attrs[:key] = 'configure_google_email'
        end
      end
    end

    def apply_feedback_customizations
      if (attrs = attributes_for_key('add_feedback_tab'))
        attrs[:key] = 'add_embeddable_widget'
      end
    end

    def apply_marketing_customizations
      feature_string = @user.account.trial_extras.where("trial_extras.key = ? ", 'features').pluck(:value).first
      return if feature_string.blank?

      available_customizations = {
        'embeddables' => 'add_embeddable_widget',
        'chat'        => 'add_chat',
        'voice'       => 'add_voice',
        'help_center' => 'add_help_center'
      }

      # Avoid moving voice task if disabled
      unless @user.account.has_voice_enabled?
        available_customizations.delete('voice')
      end

      features = feature_string.split(',') & available_customizations.keys

      # Promote the location of the passed tasks
      features.reverse_each do |feature|
        task_key = available_customizations[feature]
        remove_task(task_key)

        task = { key: task_key, section: 'admin' }
        task = add_attributes(task, owner: @user.account)
        attribute_collection[:configure].insert(1, task)
      end
    end

    def remove_task(key)
      attribute_collection.each do |_task_type, coll|
        break if coll.reject! { |attrs| attrs[:key] == key }
      end
    end

    def attribute_collection_for_owner(owner_sym)
      attribute_collection[owner_sym]
    end

    def build_attribute_collection
      if @attribute_collection.nil?
        @attribute_collection = {}
        experimental_attributes.keys.each do |task_type|
          @attribute_collection[task_type] = []
          experimental_attributes[task_type].each do |attrs|
            @attribute_collection[task_type] << add_attributes(attrs.dup, owner: task_type)
          end
        end
        apply_customizations!
      end
      @attribute_collection
    end

    def split_testing_starts_at
      if Rails.env.production?
        Time.new(2014, 1, 15, 10, 00, 00, '-08:00')
      else
        Time.new(2013, 12, 1, 10, 00, 00, '-08:00')
      end
    end

    def split_testing_ends_at
      if Rails.env.production?
        Time.new(2014, 3, 1, 10, 00, 00, '-08:00')
      else
        Time.new(2014, 2, 1, 10, 00, 00, '-08:00')
      end
    end

    def within_testing_timeframe?
      @user.account.created_at > split_testing_starts_at && @user.account.created_at < split_testing_ends_at
    end

    # ## What is this for?
    # When we had the getting-started split testing, we created variation A where there are
    # three groups of tasks :user :configure :channels forgetting the fact that each task gets
    # recorded as User or Account level tasks. It was checking if :owner == :account, and since
    # variation A does not have :account tasks, everything gets recorded as User.
    #
    # ## What does this mean?
    # By checking (key == :user), we ensure all tasks get recored corrected *going forward*.
    # For those who participated in the split test, we preserve the old behavior.
    #
    # ## Why do we need this again?
    # Recording the task as 'Account', it shares among all admins in the account. 'User'
    # tasks are not shared, so each user has their own status of the task. And when a task is
    # recorded as 'User', by fixing the condition to recognize those tasks as 'Account', the
    # record will not get updated because the owner_type does not match. So we need to let the
    # broken ones continue to be broken (at least they can update the task), and new ones have
    # the correct behavior going forward.
    #
    # ## Wait, I still don't get it. (Please talk to awen or bgreenbaum)
    def owner_options(key)
      if within_testing_timeframe?
        # This will cause configure_email etc in variation a to get recored as 'User',
        # which is a bug introduced by the split testing, but we want to preserve this behavior
        key == :account ? @user.account : @user
      else
        # This will record configure_email etc in variation to be recored as 'Account',
        # which is the corrected and expected behavior
        key == :user ? @user : @user.account
      end
    end

    def add_attributes(attrs, options = {})
      attrs[:enabled] = attrs.fetch(:enabled, true)
      attrs[:owner] = owner_options(options[:owner])
      attrs[:account] = options.fetch(:account, @user.account)
      attrs
    end
  end
end
