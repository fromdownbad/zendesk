require 'zendesk/models/certificate_ip'

class CertificateIp < ActiveRecord::Base
  AWS_POD_IDS = if Zendesk::Configuration.config_from_env?('zendesk')
    Zendesk::Configuration.fetch(:aws_pod_ids)
  else
    JSON.load ENV.fetch('ZENDESK_AWS_POD_IDS')
  end

  class NoCertificateIpsAvailableInPod < StandardError; end
  attr_accessible

  validates_uniqueness_of :ip, unless: -> { alias_record? || sni? }
  validates_presence_of :ip, unless: -> { alias_record? || sni? }

  validates_each(:ip, :port, :alias_record) do |record, attr, _value|
    record.errors.add(attr, "must be nil with sni") if record.sni? && record[attr]
  end

  validates_presence_of :alias_record, unless: -> { ip? || sni? }
  validates_uniqueness_of :certificate_id, scope: :pod_id, allow_nil: true

  validates_presence_of :port, unless: -> { alias_record? || sni? }
  validates_numericality_of :port, unless: -> { alias_record? || sni? }

  validates_presence_of :pod_id
  validates_numericality_of :pod_id

  scope :ip,         -> { where(sni: false) }
  scope :assigned,   -> { ip.where('certificate_id IS NOT NULL') }
  scope :unassigned, -> { ip.where(certificate_id: nil, release_at: nil) }
  scope :by_pod,     ->(pod_id) { where(pod_id: pod_id) }
  scope :releasable, -> { where('release_at < ?', Time.now) }

  class << self
    def release_scheduled_ips!(pod_id)
      releasable.by_pod(pod_id).find_each(&:detach_from_certificate!)
    end

    def find_unassigned_ip_for_pod(certificate, pod_id)
      if certificate.sni_enabled?
        certificate.build_sni_pod_record(pod_id).tap(&:save!)
      else
        release_scheduled_ips!(pod_id)
        new_cip = unassigned.by_pod(pod_id).first
        unless new_cip
          raise NoCertificateIpsAvailableInPod, "No IPs are available in pod #{pod_id}. " \
            "There may be a temporary high volume of IPs scheduled for release in the future, or Ops may need to purchase more IPs."
        end
        new_cip
      end
    end
  end

  # Schedule a disassociation from certificate_id for re-use by other customers
  def schedule_release!(release_at)
    update_attribute(:release_at, release_at)
  end

  # Cancel a previous `schedule_release!`
  def cancel_release!
    update_attribute(:release_at, nil)
  end

  def detach_from_certificate!
    if sni? || alias_record
      destroy
    else
      self.certificate_id = nil
      self.release_at = nil
      save(validate: false)
    end
  end

  def pod_vip_address
    return nil unless pod_id
    pod = Zendesk::Configuration.dig!(:pods, pod_id)

    if pod['a'].present?
      pod['a']
    elsif pod['alias_record'].present?
      resolve_cname(pod['alias_record'])
    end
  end

  private

  def resolve_cname(cname)
    Resolv.getaddress(cname)
  rescue Resolv::ResolvError
    Rails.logger.warn "unable to resolve #{cname} -> #{$!} - returning nil for cname"
    nil
  end
end
