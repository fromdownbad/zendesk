class UnverifiedTicketCreation < ActiveRecord::Base
  belongs_to :account
  belongs_to :user
  belongs_to :ticket
  belongs_to :user_identity

  attr_accessible :account, :user, :ticket, :user_identity, :from_address

  def identity_currently_verified?
    current_identity.try(:is_verified)
  end

  private

  # the user can delete the identity used to create the email
  # and create another identity later using the same from_address
  # we want to catch that situation too
  def current_identity
    user_identity || account.user_identities.find_by_email(from_address)
  end
end
