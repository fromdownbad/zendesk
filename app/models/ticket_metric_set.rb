class TicketMetricSet < ActiveRecord::Base
  extend ZendeskArchive::AssociationLookaside
  APM_SERVICE_NAME = 'classic-ticket-metric-sets'.freeze

  belongs_to :ticket
  association_accesses_archive :ticket

  belongs_to :account

  validates_presence_of [:account_id, :ticket_id]

  before_validation :set_account_from_ticket

  before_save :set_resolution_time
  before_save :set_assignee_stations
  before_save :set_group_stations
  before_save :set_defaults
  before_save :set_reopens
  before_save :set_replies
  before_save :set_first_reply_time
  before_save :set_wait_time
  before_save :set_on_hold_time

  # Can't use the self.ticket association here
  # - dirty attributes are not preserved in associations
  attr_accessor :dirty_ticket

  attr_accessible :ticket, :dirty_ticket

  def update_metrics(dirty_ticket)
    self.dirty_ticket = dirty_ticket
    save!
  end

  # Fix a bug in prod, which should have a better fix eventually
  def agent_stations
    assignee_stations
  end

  def current_agent_stations
    current_assignee_stations
  end

  private #####################################

  def set_account_from_ticket
    self.account ||= ticket.try(:account)
  end

  def set_resolution_time
    return unless dirty_ticket.working_was? && dirty_ticket.finished?

    minutes = if account.has_resolution_uses_solved_at? && dirty_ticket.solved_at
      minutes_since(dirty_ticket.created_at, dirty_ticket.solved_at)
    else
      minutes_since(dirty_ticket.created_at)
    end

    self.full_resolution_time_in_minutes                       = minutes[0]
    self.full_resolution_time_in_minutes_within_business_hours = minutes[1]

    self.full_resolution_time_in_hours = (minutes[0] / 60.0).round

    self.first_resolution_time_in_minutes ||=
      full_resolution_time_in_minutes
    self.first_resolution_time_in_minutes_within_business_hours ||=
      full_resolution_time_in_minutes_within_business_hours
  end

  def current_assignee_stations
    assignee_stations + (should_increment_assignee_stations? ? 1 : 0)
  end

  def set_assignee_stations
    if should_increment_assignee_stations?
      self.assignee_stations += 1
    end
  end

  def should_increment_assignee_stations?
    dirty_ticket.assignee_id && dirty_ticket.assignee_id_changed?
  end

  def current_group_stations
    group_stations + (should_increment_group_stations? ? 1 : 0)
  end

  def set_group_stations
    if should_increment_group_stations?
      self.group_stations += 1
    end
  end

  def should_increment_group_stations?
    dirty_ticket.group_id && dirty_ticket.group_id_changed?
  end

  def current_reopens
    reopens + (should_increment_reopens? ? 1 : 0)
  end

  def set_reopens
    if should_increment_reopens?
      self.reopens += 1
      self.full_resolution_time_in_minutes                       = nil
      self.full_resolution_time_in_minutes_within_business_hours = nil
      self.full_resolution_time_in_hours                         = nil
    end
  end

  def should_increment_reopens?
    !dirty_ticket.status?(:closed) && dirty_ticket.status_id_changed? && dirty_ticket.status_id_was == StatusType.SOLVED
  end

  def current_replies
    replies + (should_increment_replies? ? 1 : 0)
  end

  def set_replies
    if should_increment_replies?
      self.replies += 1
    end
  end

  def should_increment_replies?
    public_comment_from_agent? && !new_ticket?
  end

  def new_ticket?
    dirty_ticket.id_changed?
  end

  def set_first_reply_time
    return if first_reply_time_in_minutes.present? || new_ticket?

    if public_comment_from_agent? || sharing_event && !require_public_comment?
      customer_first_waiting = (sharing_event || dirty_ticket).created_at

      if customer_first_waiting.present?
        minutes = minutes_since(customer_first_waiting)

        self.first_reply_time_in_minutes                       = minutes[0]
        self.first_reply_time_in_minutes_within_business_hours = minutes[1]
      end
    end
  end

  def sharing_event
    @sharing_event ||= begin
      if dirty_ticket.via_id == ViaType.TICKET_SHARING
        dirty_ticket.events.where(type: 'TicketSharingEvent').first
      end
    end
  end

  def require_public_comment?
    agreement = Sharing::Agreement.where(id: sharing_event.agreement_id).first
    return false if agreement.blank?
    agreement.allows_public_comments
  end

  def set_wait_time
    if dirty_ticket.status_id_changed? && !new_ticket?
      # If the ticket is just moving from a finished state, nobody is waiting
      unless StatusType::FINISHED_LIST.member?(dirty_ticket.status_id_was)

        # updated_at hasn't been set if the ticket has just been created.
        minutes = minutes_since(dirty_ticket.status_updated_at_was)
        if dirty_ticket.status_id_was == StatusType.NEW
          minutes = minutes_since(dirty_ticket.created_at)
        end

        if dirty_ticket.status_id_was == StatusType.PENDING
          set_agent_wait_time(minutes)
        else
          set_requester_wait_time(minutes)
        end
      end
    end
  end

  def set_agent_wait_time(minutes) # rubocop:disable Naming/AccessorMethodName
    self.agent_wait_time_in_minutes                       = minutes[0] + agent_wait_time_in_minutes.to_i
    self.agent_wait_time_in_minutes_within_business_hours = minutes[1] + agent_wait_time_in_minutes_within_business_hours.to_i
  end

  def set_requester_wait_time(minutes) # rubocop:disable Naming/AccessorMethodName
    self.requester_wait_time_in_minutes                       = minutes[0] + requester_wait_time_in_minutes.to_i
    self.requester_wait_time_in_minutes_within_business_hours = minutes[1] + requester_wait_time_in_minutes_within_business_hours.to_i
  end

  def set_on_hold_time
    if dirty_ticket.status_id_changed? && dirty_ticket.status_id_was == StatusType.HOLD
      minutes                                            = minutes_since(dirty_ticket.status_updated_at_was)
      self.on_hold_time_in_minutes                       = minutes[0] + on_hold_time_in_minutes.to_i
      self.on_hold_time_in_minutes_within_business_hours = minutes[1] + on_hold_time_in_minutes_within_business_hours.to_i
    end
  end

  def set_defaults
    if [StatusType.SOLVED, StatusType.CLOSED].member?(dirty_ticket.status_id)
      self.agent_wait_time_in_minutes                           ||= 0
      self.agent_wait_time_in_minutes_within_business_hours     ||= 0
      self.requester_wait_time_in_minutes                       ||= 0
      self.requester_wait_time_in_minutes_within_business_hours ||= 0
    end
  end

  def minutes_since(time1, time2 = dirty_ticket.updated_at)
    calendar_minutes = dirty_ticket.calendar_minutes_diff(time1, time2)

    # TODO: should we only calculate business hour metrics for ticket that were created _after_ created_at on business_hours_definition?
    business_minutes = dirty_ticket.business_minutes_diff(time1, time2)

    [calendar_minutes, business_minutes]
  end

  public_class_method def self.attributes_for_reports
    columns.map(&:name) - ["created_at", "updated_at", "id", "account_id", "ticket_id", "full_resolution_time_in_hours"]
  end

  def public_comment_from_agent?
    return false unless comment = dirty_ticket.comment
    comment.is_public? && (!comment.empty? || comment.attachments.any?) && (comment.author.is_agent? || comment.author.foreign_agent?)
  end
end
