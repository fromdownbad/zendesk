class LogMeInSessionToken < Token
  before_create :set_expiration_time
  before_validation :set_source

  private

  def set_expiration_time
    self.expires_at = 7.days.from_now
  end

  def set_source
    self.source = account
  end
end
