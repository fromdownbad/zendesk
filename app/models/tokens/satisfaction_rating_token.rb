module Tokens
  class SatisfactionRatingToken < ::Token
    validates_presence_of       :ticket
    validate                    :ticket_is_a_ticket
    after_validation :set_expires_at, on: :create

    attr_accessible :source, :source_id, :source_type, :ip_address, :expires_at, :ticket

    def ticket
      source
    end

    def ticket=(t)
      self.source = t
    end

    def self.find_or_create_by_ticket(ticket)
      find_token(ticket) || create_token(ticket)
    end

    def self.find_token(ticket)
      Tokens::SatisfactionRatingToken.where(source_type: "Ticket", source_id: ticket.id).first
    end

    def self.create_token(ticket)
      Tokens::SatisfactionRatingToken.create!(ticket: ticket)
    end

    protected

    def ticket_is_a_ticket
      return if ticket.blank?
      unless ticket.is_a?(Ticket)
        errors.add(:ticket, 'must be a ticket')
      end
    end

    def set_expires_at
      self.expires_at = Time.now + 150.days
    end

    def ensure_value
      self.value ||= Token.generate(25, false)
    end
  end
end
