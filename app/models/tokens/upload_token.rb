# An upload token is generated to upload an attachment before submitting the ticket/comment.
# When the first file gets uploaded, an upload token is generated to attach the uploads to and the
# javascript then sets the token id in the page, such that when the ticket is committed, we check if
# there is a token id, and if so, attach the files for that token to the tickets
class UploadToken < Token
  has_many :attachments,
    -> { order(created_at: :asc, id: :asc) },
    as: :source,
    dependent: :destroy,
    inherit: :account_id

  before_create :initialize_defaults

  attr_accessible :target, :target_id, :target_type

  def add_attachment(attachment, display_filename = nil, inline = false)
    filename = attachment.original_filename

    # Allow nil here, we'll try to divine content_type later
    content_type = attachment.respond_to?(:content_type) ? attachment.content_type : nil

    setup_new_attachment(filename, content_type) do |a|
      a.size              = attachment.size
      a.uploaded_data     = attachment
      a.display_filename  = display_filename if display_filename
      a.inline            = inline
    end
  end

  def add_raw_attachment(attachment, filename)
    content_type = Zendesk::Utils::MimeTypes.determine_mime_type(filename, nil)

    setup_new_attachment(filename, content_type) do |a|
      a.size = attachment.size
      a.set_temp_data(attachment)
    end
  end

  private

  def setup_new_attachment(filename, content_type)
    attachment = attachments.build do |a|
      a.is_public    = false
      a.account      = account
      a.author       = source unless source.is_a?(Account)
      a.filename     = filename
      a.content_type = content_type || "application/unknown"

      yield a
    end

    attachment
  end

  # Zendesk is not an unlimited store for your attachments,
  # Any unlinked token attachments will be cleaned out after they expire.
  def initialize_defaults
    self.expires_at = calculate_expires_at
  end

  # We may need to relax the expiry on a per account basis to allow for easier (Product services) migrations into account.
  # For example, when importing 1 million tickets it is easier to import their 10 million attachments first
  # and then import the tickets and then link them together. You need to be able to expand their expiry
  # as long as the migration requires, and then to be set back to original value
  def calculate_expires_at
    if account && account.settings.upload_token_expiry
      account.settings.upload_token_expiry.seconds.from_now
    else
      3.days.from_now
    end
  end
end
