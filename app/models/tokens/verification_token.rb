# These tokens are issued when a user requests a new welcome email. Clicking on a link with one of these
# tokens verifies the user's email address, and enables them to login to Zendesk.
class VerificationToken < Token
  before_create :set_expiration
  before_create :delete_existing_tokens

  def self.generate
    super(32)
  end

  private

  def set_expiration
    self.expires_at ||= 24.hours.from_now
  end

  def delete_existing_tokens
    if source.is_a?(User)
      source.verification_tokens.destroy_all
      source.identities.each do |identity|
        identity.verification_tokens.destroy_all
      end
    end
  end
end
