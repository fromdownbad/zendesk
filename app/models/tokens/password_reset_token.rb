# These tokens are issue when a user requests a password reset. Clicking on a link with one of these
# tokens will take them to a page to reset the user's password.
class PasswordResetToken < Token
  before_create :set_expiration
  before_create :delete_existing_tokens

  def self.generate
    super(32)
  end

  private

  def set_expiration
    self.expires_at ||= 24.hours.from_now
  end

  def delete_existing_tokens
    if source.is_a?(User)
      source.password_reset_tokens.destroy_all
    end
  end
end
