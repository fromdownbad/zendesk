class RequestToken < ::Token
  validates_presence_of       :ticket
  validate                    :ticket_is_a_ticket
  after_validation :set_expires_at, on: :create

  attr_accessible :source, :source_id, :source_type, :ip_address, :expires_at, :ticket

  alias_attribute :ticket, :source

  def nice_id
    ticket.nice_id
  end

  protected

  def ticket_is_a_ticket
    unless ticket.is_a?(Ticket)
      errors.add(:ticket, 'must be a ticket')
    end
  end

  def set_expires_at
    self.expires_at = Time.now + 90.days
  end
end
