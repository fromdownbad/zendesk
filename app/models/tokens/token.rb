require 'zlib'
require 'securerandom'

class Token < ActiveRecord::Base
  include Zendesk::ScopeExtensions

  belongs_to :account
  belongs_to :source, polymorphic: true
  belongs_to :target, polymorphic: true

  validates_presence_of :source, :account_id, :value, :token_crc

  scope :lookup, ->(value) { where(['token_crc = CRC32(?) AND value = ?', value, value.to_s]) }
  scope :not_expired, -> { where('expires_at > ?', Time.now.utc.to_s(:db)) }

  before_validation :update_account
  before_validation :ensure_value

  validate :uniqueness_of_value

  attr_accessible :source, :source_id, :source_type, :ip_address, :expires_at

  def value=(new_value)
    write_attribute(:value, new_value)
    self.token_crc = Zlib.crc32(new_value)
  end

  def self.find_by_value(value)
    lookup(value).first if value.is_a?(String)
  end

  def self.find_by_value!(value)
    find_by_value(value) || raise(ActiveRecord::RecordNotFound)
  end

  def self.find_all_by_value(values)
    vals = Array(values).map { |val| lookup(val) }

    if RAILS4
      and_disjunction_of(*vals) # class level or is not rails 5 compatible
    else
      vals.reduce { |lhs, rhs| lhs.or(rhs) }
    end
  end

  def self.generate(size = 25, lower = false)
    value = SecureRandom.base64(2 * size).gsub(/[^\w]/, '').slice(0, size)
    value.downcase! if lower
    value
  end

  def self.cleanup
    # BBO, 2011-02-14 -- Disabled until we figure out a proper way of attaching images at the account level
    # UploadToken.destroy_all("expires_at < '#{Time.now.to_s(:db)}'")
    expiry = Time.now.utc

    # UploadToken has a dependency
    UploadToken.includes(:attachments).where(["expires_at < ?", expiry]).find_each(batch_size: 100) do |token|
      next if token.attachments.any?(&:inline)
      next if token.account&.has_skip_deleting_expired_tokens?

      token.destroy
      # this loop is self-throttled by the file removal from cloud storage.
    end

    Token.where(["expires_at < ?", expiry]).select("id, account_id, type, source_type, source_id").find_in_batches(batch_size: 100) do |tokens|
      tokens.reject! do |t|
        (t.account&.has_skip_deleting_expired_tokens?) ||
        t.type == "UploadToken" ||
        (
          t.type == "Tokens::SatisfactionRatingToken" &&
            t.account &&
            t.account.has_csr_token_retention? &&
            t.ticket &&
            Zendesk::Types::StatusType::INOPERABLE_LIST.exclude?(t.ticket.status_id)
        )
      end

      next if tokens.empty?
      Token.delete_all(id: tokens.map(&:id))
      sleep(1) if Rails.env.production?
    end
  end

  def is_expired? # rubocop:disable Naming/PredicateName
    expires_at < Time.now
  end

  def generate
    self.class.generate
  end

  private

  def ensure_value
    self.value = generate if value.blank?
  end

  def update_account
    return unless source
    self.account = source.is_a?(Account) ? source : source.account
  end

  def uniqueness_of_value
    if Token.exists?(["account_id = ? AND token_crc = CRC32(?) AND value = ?", account_id, value, value])
      errors.add(:value, :taken)
      false
    else
      true
    end
  end
end
