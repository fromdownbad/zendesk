class OrganizationAssociationAddition
  def initialize(account_id, organization_id, user_ids)
    self.account_id      = account_id
    self.organization_id = organization_id
    self.user_ids        = user_ids
    self.logger          = Rails.logger
  end

  def call
    set_organization_id_on_users(user_ids_needing_organization_change)

    create_organization_memberships(user_ids_without_organization, default: true)
    if account.has_multiple_organizations_enabled?
      create_organization_memberships(user_ids_associated_with_other_organizations, default: false)
    else
      update_default_organization_memberships(user_ids_associated_with_other_organizations)
    end

    set_organization_on_open_tickets(user_ids_needing_organization_change)

    organization
  end

  private

  attr_accessor :account_id, :organization_id, :user_ids, :logger

  def organization
    Organization.find_by_id(organization_id)
  end

  def set_organization_id_on_users(user_ids) # rubocop:disable Naming/AccessorMethodName
    return if user_ids.empty?
    scope = account.users.where(id: user_ids)
    scope.update_all_with_updated_at(organization_id: organization_id)
    scope.find_each(&:clear_kasket_indices)
    logger.info("Set organization_id to: #{organization_id.inspect} for #{user_ids.count} users")
  end

  def user_ids_needing_organization_change
    user_ids = user_ids_without_organization
    unless account.has_multiple_organizations_enabled?
      user_ids += user_ids_associated_with_other_organizations
    end
    user_ids
  end

  def create_organization_memberships(user_ids, default:)
    return if user_ids.empty?
    db_value = (default ? '1' : 'NULL')
    OrganizationMembership.connection.execute(
      bulk_insert_statement do
        organization_membership_guids_for(user_ids).map do |user_id, guid|
          "(#{guid},#{user_id},#{organization_id},#{account_id},'#{Time.now}','#{Time.now}',#{db_value})"
        end.join(",")
      end
    )

    logger.info(
      "Created OrganizationMemberships for: #{organization_id} for #{user_ids.count} users"
    )
  end

  def update_default_organization_memberships(user_ids)
    return if user_ids.empty?
    conditions = account.has_org_association_update_default_only? ? { user_id: user_ids, default: true } : { user_id: user_ids }
    account.organization_memberships.where(conditions).update_all_with_updated_at(organization_id: organization_id)
    logger.info(
      "Updated OrganizationMemberships to: #{organization_id} for #{user_ids.count} users"
    )
  end

  def set_organization_on_open_tickets(user_ids) # rubocop:disable Naming/AccessorMethodName
    return if user_ids.empty?
    account.users.where(id: user_ids).find_each do |user|
      user.move_tickets_to_organization(:any, organization_id)
    end
    logger.info("Setting organization_id on unclosed tickets for #{user_ids.count} users")
  end

  def user_ids_without_organization
    @user_ids_needing_default_memberships ||= (user_ids - organization_memberships.map(&:user_id)).sort
  end

  def user_ids_associated_with_other_organizations
    @user_ids_associated_with_other_organizations ||= begin
      for_this, for_other = organization_memberships.partition do |organization_membership|
        organization_membership.organization_id == organization_id
      end

      for_other.map(&:user_id).uniq - for_this.map(&:user_id).uniq
    end
  end

  def organization_membership_guids_for(user_ids)
    user_ids.zip(Array(OrganizationMembership.generate_many_uids(user_ids.length)))
  end

  def organization_memberships
    @organization_memberships ||= account.organization_memberships.
      where(user_id: user_ids).to_a
  end

  def account
    @account ||= Account.find(account_id)
  end

  def bulk_insert_statement
    <<-SQL
      INSERT IGNORE
      INTO organization_memberships (id, user_id, organization_id, account_id, created_at, updated_at, `default`)
      VALUES #{yield}
    SQL
  end
end
