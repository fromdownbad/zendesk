class ComplianceAgreement < ActiveRecord::Base
  attr_accessible

  belongs_to :account
  belongs_to :compliance

  validates_presence_of :account_id, :compliance
  validates_uniqueness_of :compliance_id, scope: :account_id
end
