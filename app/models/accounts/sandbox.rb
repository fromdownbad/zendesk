module Accounts
  class Sandbox < Classic
    SANDBOX_COPY_TEXTS = [
      :help_desk_description, :mail_delimiter, :domain_whitelist, :domain_blacklist,
      :cc_blacklist, :forum_title, :salesforce_configuration, :ip_restriction
    ].freeze

    ACTIVE_PRAVDA_PRODUCT_STATES = %i[trial subscribed free].freeze

    # Not synced from master
    # Customer needs to start a new trial due to setup/billing concerns
    DO_NOT_COPY_PRODUCT_LIST = [:answer_bot, :outbound, :talk].freeze

    SKIPPED_USER_IDENTITIES = %w[UserSdkIdentity UserTwitterIdentity].freeze

    SKIPPED_SETTINGS = %w[
      polaris check_group_name_uniqueness social_messaging_agent_workspace social_messaging native_messaging assumption_expiration
    ].freeze

    # These SANDBOX_TYPES represent the enterprise sandbox (standard) and premium sandbox
    # addon plan settings.
    # standard does not involve the sandbox_orchestrator (ie - no config/ticket data)
    # metadata is just config
    # partial = metadata + 10k tickets
    # production = metadata + 100k tickets
    STANDARD = 'standard'.freeze
    METADATA = 'metadata'.freeze
    PARTIAL = 'partial'.freeze
    PRODUCTION = 'production'.freeze
    SANDBOX_TYPES = [STANDARD, METADATA, PARTIAL, PRODUCTION].freeze

    after_create :add_security_policy_from_sandbox_master
    after_create :add_custom_security_policy_from_sandbox_master
    after_create :add_default_form_to_sandbox
    after_create :add_2fa_to_owner

    after_commit :add_admins_from_sandbox_master, on: :create
    after_commit :deactivate_sandbox_siblings, on: :create
    after_commit :copy_pravda_products_from_sandbox_master, on: :create

    # @override
    def reactivate!
      self.is_active = true
      self.is_serviceable = true
      Zendesk::SupportAccounts::Product.retrieve(self).save!

      # feature bits
      set_use_feature_framework_persistence(true)
      Zendesk::Features::SubscriptionFeatureService.new(self).execute!
    end

    def product_addon_sync
      copy_pravda_products_from_sandbox_master
      add_feature_bits
    end

    protected

    # @override
    def generate_owner_welcome_email
    end

    # @override
    def create_guide_plan
    end

    # @override
    def add_properties
      copy_setting_properties_for_sandbox
      copy_text_properties_for_sandbox
    end

    # @override
    def set_settings_defaults
      super

      settings.whitelisted_from_fraud_restrictions = sandbox_master.whitelisted?

      true
    end

    # @override
    def add_owner
      super # this also sets the default organization

      owner.challenge_tokens.lookup_by_ip_address
    end

    # @override
    def add_subscription
      sandbox_plan_type = SubscriptionPlanType.Large

      if sandbox_master.subscription.plan_type == SubscriptionPlanType.ExtraLarge
        sandbox_plan_type = SubscriptionPlanType.ExtraLarge
      end

      attributes_hash = {
        account: self,
        base_agents: [25, sandbox_master.subscription.max_agents].max,
        currency: @currency,
        is_trial: false,
        manual_discount: 100,
        manual_discount_expires_on: 5.years.from_now.to_date,
        plan_type: sandbox_plan_type,
        pricing_model_revision: sandbox_master.subscription.pricing_model_revision
      }

      create_subscription(attributes_hash)
    end

    # @override
    def add_feature_bits
      Zendesk::Features::SubscriptionFeatureService.new(self).
        create_sandbox_features!(sandbox_master.subscription)
    end

    def build_owner(*)
      self.owner = sandbox_master.owner.dup
      sandbox_master.owner.identities.where.not(type: SKIPPED_USER_IDENTITIES).each do |identity|
        owner.identities << identity.class.new(
          user: owner,
          account: self,
          value: identity.value,
          is_verified: identity.is_verified
        )
      end

      owner.skip_verification        = true
      owner.skip_password_validation = true
    end

    def add_admins_from_sandbox_master
      sandbox_master.admins.active.each do |admin|
        next if admin == sandbox_master.owner

        user = admin.dup
        user.skip_verification = true
        user.skip_password_validation = true
        user.account           = self
        user.organization_id   = nil
        user.permission_set_id = nil

        admin.identities.where.not(type: SKIPPED_USER_IDENTITIES).each do |identity|
          user.identities << identity.class.new(
            user: user,
            account: self,
            value: identity.value,
            is_verified: identity.is_verified
          )
        end

        user_otp_setting = UserOtpSetting.where(user_id: admin.id, account_id: admin.account_id).first

        if user_otp_setting
          new_user_otp_setting = user_otp_setting.dup
          new_user_otp_setting.user = user
          new_user_otp_setting.account = self

          new_user_otp_setting.save!
        end

        Zendesk::SupportUsers::User.new(user).save!

        Membership.where(group_id: groups.first.id, user_id: user.id).first_or_create
      end
    end

    def add_security_policy_from_sandbox_master
      role_settings.update_attributes!(
        agent_security_policy_id: sandbox_master.role_settings.agent_security_policy_id,
        end_user_security_policy_id: sandbox_master.role_settings.end_user_security_policy_id
      )
    end

    def add_custom_security_policy_from_sandbox_master
      if sandbox_master.custom_security_policy
        self.custom_security_policy = sandbox_master.custom_security_policy.dup
        custom_security_policy.save!
      end
    end

    def add_default_form_to_sandbox
      return unless subscription.has_ticket_forms?
      TicketForm.init_default_form(self).save!
    end

    def add_2fa_to_owner
      user_otp_setting = UserOtpSetting.where(user_id: sandbox_master.owner.id, account_id: sandbox_master.account.id).first

      if user_otp_setting
        new_user_otp_setting = user_otp_setting.dup
        new_user_otp_setting.user = owner
        new_user_otp_setting.account = self

        new_user_otp_setting.save!
      end
    end

    def deactivate_sandbox_siblings
      return if sandbox_master.multiple_sandboxes?

      return unless is_active?
      Account.where(is_active: true, sandbox_master_id: sandbox_master_id).where.not(id: id).find_each(&:cancel!)
    end

    private

    def copy_text_properties_for_sandbox
      sandbox_master.texts.select { |t| SANDBOX_COPY_TEXTS.include?(t.name.to_sym) }.each do |text|
        if texts.respond_to?(:"#{text.name}=")
          texts.send("#{text.name}=", sandbox_master.texts.send(text.name))
        end
      end
    end

    def copy_setting_properties_for_sandbox
      sandbox_master.settings.all.each do |setting|
        next unless settings.respond_to?(:"#{setting.name}=")
        case setting.name
        when "end_user_phone_number_validation"
          settings.send("#{setting.name}=", false)
        when "screencasts_for_tickets"
          settings.send("#{setting.name}=", false)
        when "help_center_state"
          settings.send("#{setting.name}=", 'disabled')
        when "customer_satisfaction"
          settings.where(name: 'customer_satisfaction').delete_all
        when "automatic_answers"
          settings.send("#{setting.name}=", false)
        when *SKIPPED_SETTINGS
          next
        else
          settings.send("#{setting.name}=", sandbox_master.settings.send(setting.name))
        end
      end
    end

    def copy_pravda_products_from_sandbox_master
      active_products_master.each { |product| copy_pravda_product(product) }
    end

    def active_products_master
      sandbox_master.products.select { |product| ACTIVE_PRAVDA_PRODUCT_STATES.include?(product.state) }
    end

    def copy_pravda_product(product)
      return if DO_NOT_COPY_PRODUCT_LIST.include?(product.name)

      if product.name == :chat
        product.plan_settings["phase"] = 3 if prevent_non_multiproduct_to_be_cp4(product)
      end
      account_service_client.update_or_create_product(
        product.name,
        pravda_product_payload(product),
        context: 'sandbox_creation'
      )
    end

    def prevent_non_multiproduct_to_be_cp4(chat_product)
      chat_product.plan_settings["phase"] == 4 && !multiproduct?
    end

    def pravda_product_payload(product)
      settings = product.plan_settings.reject { |k, v| k == '@class' || v.nil? }

      if product.state == ::Zendesk::Accounts::Product::TRIAL
        {
          product: {
            state: ::Zendesk::Accounts::Product::TRIAL,
            plan_settings: settings,
            trial_expires_at: product.trial_expires_at
          }
        }
      else
        {
          product: {
            state: ::Zendesk::Accounts::Product::FREE,
            plan_settings: settings
          }
        }
      end
    end
  end
end
