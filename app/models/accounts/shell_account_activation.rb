module Accounts
  class ShellAccountActivation < Classic
    def create_billing_admin_policy(permission_set)
      permissions_service_client = Zendesk::ExternalPermissions::PermissionsServiceClient.new(subdomain, nil)
      request_body = { policy: { name: permission_set.name, policy_type: 'billing_admin', subject: { id: permission_set.id.to_s, type: 'custom_role' }, statements: [] } }
      permissions_service_client.create_default_policy!(request_body)
    end

    def user_roles_initialization(activated_by_user)
      ::PermissionSet.enable_contributor!(self)

      if Arturo.feature_enabled_for?(:permissions_billing_admin, self)
        billing_admin = ::PermissionSet.create_billing_admin!(self)
        create_billing_admin_policy(billing_admin)
      end

      Zendesk::SupportUsers::User.new(owner).assign_role(Role::ADMIN.id) unless owner.is_admin?

      downgrade_admins_except_owner

      update_support_role_if_chat_admin(activated_by_user.reload) if activated_by_user.present?

      Omnichannel::AccountUsersEntitlementsSyncJob.enqueue(account_id: id)
    end

    def activate_shard_db_records_in_transaction(activated_by_user)
      ActiveRecord::Base.on_shard(shard_id) do
        # The subsequent transaction depends on account settings to be populated correctly
        # Failure after settings creation sees settings not set correctly on retry
        # Can check if this needs doing by checking the presence of one of the populated settings
        unless settings.where(name: 'ssl_required').exists?
          ActiveRecord::Base.transaction do
            apply_default_account_settings
            add_properties
            store_id_on_shard
            add_shell_settings
            save_settings_and_texts
          end
        end

        # If any of the records in transaction is created, we can assume all records are created
        # So we check groups to make this part idempotent
        return if groups.any?
        ActiveRecord::Base.transaction do
          add_sequences
          add_feature_bits
          create_account_property_set
          add_default_brand
          add_custom_statuses
          add_group
          add_owner
          add_owner_to_group
          add_custom_fields
          add_rules_and_reports
          user_roles_initialization(activated_by_user)
          set_support_suite_trial_refresh_capability
        end
      end
    end

    def activate_accounts_db_records_in_transaction
      # If any of the records in transaction is created, we can assume all records are created
      # So we check subscription to make this method idempotent
      return if subscription.present?
      ActiveRecord::Base.transaction do
        force_downcase_subdomain
        create_role_settings
        add_subscription
        add_address
      end
    end

    private

    def add_shell_settings
      set_created_from_ip_address(trial_extras.to_hash['original_remote_ip'] || "")
      settings.suite_trial = trial_extras.to_hash['suite_trial']
      settings.ssl_required = true
      settings.export_whitelisted_domain = owner&.email&.split("@")&.last
    end

    def downgrade_admins_except_owner
      admins_to_downgrade = admins - [owner]
      admins_to_downgrade.each do |admin|
        Zendesk::Users::AgentDowngrader.perform(agent: admin, products: [:support])
      end
    end

    def update_support_role_if_chat_admin(user)
      return if user.is_anonymous_user? || user.is_system_user? || user.is_admin?
      return unless chat_admin?(user)

      Zendesk::SupportUsers::User.new(user).assign_role(Role::ADMIN.id)
    end

    def chat_admin?(user)
      entitlements = Zendesk::StaffClient.new(self).get_entitlements!(user.id)
      entitlements[:chat] == Zendesk::Entitlement::ROLES[:chat][:admin]
    end
  end
end
