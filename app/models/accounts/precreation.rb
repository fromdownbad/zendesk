module Accounts
  module Precreation
    # This list represents all the models that need to be updated during the fast account conversion process.
    # In addition to being in this Array each model needs to also include the `PrecreatedAccountConversion` module.
    CLASSES_TO_BE_UPDATED = %w[
      Brand
      Route
      UserIdentity
      User
      RemoteAuthentication
      RecipientAddress
      Rule
      Report
      Ticket
      TicketField
      Address
      AccountPropertySet
      AccountSetting
      AccountText
      UserSetting
      Event
      RoleSettings
      Group
      Membership
      OrganizationMembership
      Organization
      Tagging
      TriggerSnapshot
      TriggerRevision
      CustomStatus
    ].freeze

    def self.included(base)
      base.class_eval do
        # these associations are defined in zendesk_core
        [Brand, Route, UserIdentity].each do |assoc|
          assoc.class_eval do
            include PrecreatedAccountConversion
          end
        end

        define_callbacks :convert
        define_callbacks :convert_rollback

        def self.before_convert(*args, &block)
          set_callback :convert, :before, *args, &block
        end

        def self.after_convert(*args, &block)
          set_callback :convert, :after, *args, &block
        end

        def self.after_convert_rollback(*args, &block)
          set_callback :convert_rollback, :after, *args, &block
        end

        before_save    :mark_account_as_serviceable
        after_commit   :finish_precreation_binding,  if: :binding_account?

        after_commit   :product_specific_callbacks,  on: :create, unless: :pre_account_creation
        after_commit   :create_guide_plan,           on: :create, unless: :pre_account_creation
        after_commit   :start_suite_trial,           on: :create, unless: :pre_account_creation
        after_commit   :enqueue_fraud_score_job,     on: :create, unless: :pre_account_creation

        before_convert :pre_created_account_callbacks
        before_convert :product_specific_callbacks

        after_convert  :create_guide_plan
        after_convert  :start_suite_trial
        after_convert  :enqueue_fraud_score_job
        after_convert  :sync_entitlements

        after_rollback -> { run_callbacks :convert_rollback }, if: :binding_account?
      end
    end

    attr_accessor :is_fast_account_creation

    # returns a Hash to represent update status for each table
    # {
    #   brands: true,
    #   routes: true,
    #   user_identities: true
    # }
    def run_after_convertion_callbacks
      Accounts::Precreation::CLASSES_TO_BE_UPDATED.
        map { |class_string| Kernel.const_get(class_string) }.
        each_with_object({}) do |klass, h|
          stats = klass.where(account_id: id).map do |record|
            record.run_callbacks(:account_convert)
          end

          h[klass.table_name] = stats.all?
        end
    end

    def create_guide_plan
      return if settings.suite_trial?
      return if Arturo.feature_enabled_for?(:guide_disable_auto_trial, account)

      Guide::CreateTrialJob.enqueue(id)
    end

    def start_suite_trial
      return unless settings.suite_trial?
      return if spp?

      Rails.logger.info("enqueue_suite_trial_job:#{idsub}")
      SuiteTrialJob.enqueue(id)
    end

    private

    def pre_created_account_callbacks
      # Aggressively fetching subdomain form account master db to clear old cache

      # Change subdomain in account property
      update_property_set

      # Common for pre-created accounts
      precreate_new_account
      update_account_timestamps
      enqueue_timestamps_update_job
      save_trial_extras
    end

    # Specific to our `support` product
    def product_specific_callbacks
      # Reassign owner account object since it is often stale at this point
      owner.account = self
      create_support_product
      serviceable_state_logging(context: 'product_specific_callbacks')
      generate_owner_welcome_email
      generate_sample_ticket_by_locale
      refresh_account_trial_limits
      add_google_domain
      setup_chat_to_support_xsell
      set_support_suite_trial_refresh_capability
    end

    def set_support_suite_trial_refresh_capability
      settings.checklist_onboarding_version = assign_checklist_onboarding_version

      settings.save!
    end

    def assign_checklist_onboarding_version
      return Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH unless Arturo.feature_enabled_for_pod?(:support_suite_trial_refresh_phase2, Zendesk::Configuration.fetch(:pod_id))
      Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH_PHASE2
    end

    def sync_entitlements
      Omnichannel::AccountUsersEntitlementsSyncJob.enqueue(account_id: id)
    end

    def mark_account_as_serviceable
      statsd_client.increment(
        "mark_account_as_serviceable",
        tags: [
          "subdomain:#{subdomain}",
          "account_id:#{id}",
          "binding_account:#{binding_account?}",
          "serviceable:#{is_serviceable}"
        ]
      )
      if binding_account?
        self.is_serviceable = true
      end
    end

    def finish_precreation_binding
      serviceable_state_logging(context: 'pre_created_account_callbacks')
      pre_account_creation.finish_binding
      run_callbacks :convert
    end

    def precreate_new_account
      statsd_client.gauge("account_pool_size", pre_account_creation.pool_size, tags: ["pod_id:#{pre_account_creation.pod_id}", "locale_id:#{pre_account_creation.locale_id}", "region:#{pre_account_creation.region}"])
      pre_account_creation.enqueue_precreate_account_job if pre_account_creation.need_more_accounts?
    end

    def binding_account?
      pre_account_creation && pre_account_creation.is_binding?
    end

    def enqueue_timestamps_update_job
      AccountsAfterConversionJob.enqueue(id)
    end

    def create_support_product
      return if settings.suite_trial?
      Zendesk::TrialActivation.create_support_product(account, retry_max: 0)
      Rails.logger.info("Successfully created support product record for #{idsub} synchronously")
      statsd_client.increment("create_support_product", tags: ["result:success"])
    rescue StandardError => e
      Rails.logger.warn("Failed to create support product record for #{idsub} synchronously, falling back to async job: #{e.class} #{e.message}")
      statsd_client.increment("create_support_product", tags: ["result:fail_to_async"])
      SupportProductCreationJob.enqueue(account_id: id)
    end

    def generate_owner_welcome_email
      erroneous_subdomain = /#{PreAccountCreation::SUBDOMAIN_PREFIX}/i

      tags = [
        "route_subdom:#{route.subdomain}",
        "subdomain:#{subdomain}"
      ]

      is_erroneous = tags.any?(&erroneous_subdomain.method(:match))
      tags << "subdom_err:#{is_erroneous}"

      AccountsMailer.deliver_welcome_account_owner(owner, account_url: url)

      Rails.logger.info("About to generate owner welcome email, checking subdomains: " + tags.join(', '))

      statsd_client.increment(
        "generate_owner_welcome_email",
        tags: tags
      )
    end

    def save_trial_extras
      TrialExtrasJob.enqueue(id)
    end

    def enqueue_fraud_score_job
      if Arturo.feature_enabled_for?(:orca_classic_replace_fraud_report_job_with_fraud_score_job, self)
        FraudScoreJob.account_creation_enqueue(self, Fraud::SourceEvent::TRIAL_SIGNUP)
      end
    end

    # Sample ticket for en locale includes an English video.
    # Video & accompanying text is removed for non-english locales
    def generate_sample_ticket_by_locale
      raise 'Owner must be set' if owner.nil?

      Ticket.generate_sample_ticket_without_notifications(self, owner)
    end

    def update_account_timestamps
      update_column(:created_at, pre_account_creation.bound_at)
      update_column(:updated_at, pre_account_creation.bound_at)
      pre_account_creation.soft_delete!
    end

    def update_property_set
      # on account template conversion, subdomain should be changed. So update the property set here
      account_property_set.reset_signup_email_text if account_property_set
    end

    def refresh_account_trial_limits
      Account.trial_limits.each { |model| TrialLimit.reset_limit_count(self, model) }
    end

    def add_google_domain
      on_shard do
        # Majority of accounts will return here
        return if route.gam_domain.present? || is_sandbox?

        # Accounts created through slow account creation path will still need this logic
        google_apps_domain = settings.google_apps_domain
        unless google_apps_domain.present?
          google_apps_domain = redis_client.get(google_apps_domain_redis_key)
          Rails.logger.warn "Account #{subdomain} does not have settings.google_apps_domain. Using redis value instead" if google_apps_domain.present?
        end
        return unless google_apps_domain.present?
        Rails.logger.info "Route.gam_domain is now to set to #{google_apps_domain} in add_google_domain callback"
        route.update_attribute(:gam_domain, google_apps_domain)
      end
    end

    def setup_chat_to_support_xsell
      if settings.xsell_source == 'zendesk_chat'
        ZendeskAPI::AppInstallation.create!(
          api_client,
          app_id: ZopimIntegration.app_market_id,
          settings: {
            name: ZopimIntegration.app_market_name
          }
        )
      end
    rescue e
      ZendeskExceptions::Logger.record(e, location: self, message: "Could not pre-install Zendesk Chat app", fingerprint: '6483f32ad53fb0ff725b169b1ebbd16e0e2ee9fc')
      statsd_client.increment(
        "chat_app_installation",
        tags: ["account_id:#{id}", "status:failure"]
      )
    end

    def serviceable_state_logging(context: nil)
      Rails.logger.info "Account #{id} serviceable state is: #{is_serviceable}, context: #{context}"
    end

    def redis_client
      Zendesk::RedisStore.client
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['accounts_precreation'])
    end
  end
end
