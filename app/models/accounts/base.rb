module Accounts
  class Base < Account
    DEFAULT_RULES_PATH = Rails.root.join("db/defaults/rules.yml")
    DEFAULT_CCS_FOLLOWERS_RULES_PATH = Rails.root.join("db/defaults/ccs_followers_rules.yml")

    class_attribute :default_enabled_settings, :default_enabled_settings_ccs_followers, :default_disabled_settings

    # IMPORTANT: This affects settings on newly created accounts.
    # Check account's properties for the default values on ALL accounts.
    #
    # Also add new setting names to BOTH default_enabled_settings and
    # default_enabled_settings_ccs_followers until the Arturo controlling them is removed.
    #
    # When removing email_ccs_new_accounts Arturo after it's GA, rename this
    # to default_enabled_settings (and remove the old assignment below)
    self.default_enabled_settings_ccs_followers = [
      :captcha_required,
      :ccs_followers_no_rollback,
      :email_sender_authentication,
      :email_template_photos,
      :follower_and_email_cc_collaborations,
      :modern_email_template,
      :only_extended_metrics,
      :rich_content_in_emails,
      :saml_issuer_account_url,
      :show_introductory_text,
      :ticket_show_empty_views,
      :ticket_tagging,
      :tickets_inherit_requester_tags,
      :twitter_search_stream,
      :use_feature_framework_persistence,
      :no_mail_delimiter
    ]

    # IMPORTANT: See the above ^^^^
    self.default_enabled_settings = [
      :captcha_required,
      :email_sender_authentication,
      :email_template_photos,
      :modern_email_template,
      :only_extended_metrics,
      :rich_content_in_emails,
      :saml_issuer_account_url,
      :show_introductory_text,
      :ticket_show_empty_views,
      :ticket_tagging,
      :tickets_inherit_requester_tags,
      :twitter_search_stream,
      :use_feature_framework_persistence,
      :no_mail_delimiter
    ]

    self.default_disabled_settings = [
      :accept_wildcard_emails,
      :email_template_selection
    ]

    validate :validate_trial_coupon_code

    # Account global default settings
    before_validation :set_default_subdomain, on: :create
    before_validation :apply_default_account_settings, on: :create

    before_create :default_to_12_hour_clock_in_countries_where_it_is_dominant
    before_create :add_properties
    before_create :force_downcase_subdomain

    # Add all the standard information for the account. Order matters.
    after_create :create_role_settings
    after_create :save_settings_and_texts
    after_create :add_sequences
    after_create :add_subscription
    after_create :add_feature_bits
    after_create :create_account_property_set
    after_create :add_default_brand
    after_create :add_custom_statuses
    after_create :add_group
    after_create :add_owner
    after_create :add_owner_to_group
    after_create :add_address
    after_create :add_custom_fields
    after_create :add_rules_and_reports
    after_create :enable_google_login!, if: :created_via_google?
    after_create :enable_office_365_login!, if: :created_via_office_365?

    after_save   :setup_remote_authentications, if: :owner
    after_save   :enable_agent_remote_login_method!, if: :created_with_remote_authentication?

    before_update :update_address

    include Accounts::Precreation

    def apply_default_account_settings
      set_defaults
      set_settings_defaults
    end

    def set_currency(value = "USD") # rubocop:disable Naming/AccessorMethodName
      @currency = value
    end

    def set_remote_authentication_settings(settings) # rubocop:disable Naming/AccessorMethodName
      return if settings.blank?
      @remote_authentication_settings = settings.merge(is_active: true)
    end

    def set_partner_settings(settings) # rubocop:disable Naming/AccessorMethodName
      return if settings.blank?
      self.settings.partner_name = settings[:name]
      self.settings.partner_url  = settings[:url]
    end

    def set_owner(params) # rubocop:disable Naming/AccessorMethodName
      raise 'Owner can only be set this way during initialization' unless owner_id.nil?
      @owner_params = params
    end

    def set_created_from_ip_address(remote_ip) # rubocop:disable Naming/AccessorMethodName
      settings.created_from_ip_address = remote_ip
    end

    def set_creation_channel(creation_channel) # rubocop:disable Naming/AccessorMethodName
      return if creation_channel.blank?
      settings.creation_channel = creation_channel
    end

    def load_rule_categories_from_yaml(path_to_file = 'db/defaults/rule_categories.yml')
      return unless has_trigger_categories_api_enabled?

      create_rule_categories_from_yaml(path_to_file)
    end

    def load_rules_from_yaml(file = DEFAULT_RULES_PATH)
      create_rules_from_yaml(file)
    end

    def load_rule_class_from_yaml(class_name, file = DEFAULT_RULES_PATH)
      create_rules_from_yaml(file, class_name)
    end

    def set_google_apps_creds(google_apps_domain, has_google_apps_admin)
      settings.set(
        google_apps_domain: google_apps_domain,
        has_google_apps_admin: has_google_apps_admin,
        has_google_apps: google_apps_domain.present? && role_settings.try(:agent_google_login)
      )
      route.gam_domain = google_apps_domain if route && google_apps_domain.present?

      # Using Redis to store the domain because frequently,
      # account.settings.google_apps_domain will still be nil by the time the
      # route is updated in the precreation#add_google_domain before_convert/after_commit callback

      redis_client.setex(google_apps_domain_redis_key, 5.minutes.to_i, google_apps_domain)
    end

    def google_apps_domain_redis_key
      "account_#{subdomain}_google_apps_domain"
    end

    protected

    def force_downcase_subdomain
      subdomain.downcase!
    end

    def set_default_subdomain
      self.subdomain ||= ""
    end

    def set_defaults
      self.agents_can_create_users = true
      self.is_active               = true
      self.is_serviceable          = true
      # Keep this value in sync with app/models/account/properties.rb and
      # https://github.com/zendesk/zendesk_mail_parsing_queue/blob/master/lib/account.rb
      self.mail_delimiter          = '{{txt.email.delimiter}}'
      self.is_signup_required      = false
      self.domain_whitelist        = ''
      self.is_open                 = true
      self.is_ssl_enabled          = !Rails.env.test?
      self.loop_threshold          = 20
      self.forum_title                                  = I18n.t("txt.default.forums.title")
      self.is_personalized_reply_enabled                = true
      self.is_welcome_email_when_agent_register_enabled = true
      self.is_collaboration_enabled                     = true
      self.is_collaborators_addable_only_by_agents      = false
      self.is_first_comment_private_enabled             = true
      settings.assign_tickets_upon_solve           = true
      settings.change_assignee_to_general_group    = true
      settings.benchmark_opt_out                   = false

      true
    end

    def set_settings_defaults
      settings.enable_agent_ip_restrictions = true
      settings.enable_ip_mobile_access      = false
      settings.hide_entry_tags_enabled      = true
      settings.web_portal_state             = :disabled
      settings.no_short_url_tweet_append    = has_end_user_ui?
      settings.export_accessible_types      = ImportExportJobPolicy::VIEW_JOBS
      settings.export_whitelisted_domain    = default_export_whitelisted_domain.to_s
      settings.activate_lotus_branding      = true
      settings.api_password_access          = false
      # Avoid race condition when accessing account for first time
      settings.last_login                   = nil

      true
    end

    def default_to_12_hour_clock_in_countries_where_it_is_dominant
      if @addrs_params.present? && AM_PM_COUNTRIES.include?(@addrs_params[:country])
        self.uses_12_hour_clock = true
      end
    end

    def add_properties
      if has_email_ccs_new_accounts?
        default_enabled_settings_ccs_followers.each { |name| settings.enable(name) }
      else
        default_enabled_settings.each { |name| settings.enable(name) }
      end

      if has_trigger_categories_new_accounts?
        settings.enable(:trigger_categories_api)
      end

      default_disabled_settings.each { |name| settings.disable(name) }
    end

    def setup_remote_authentications
      setup_remote_jwt_authentications
      setup_remote_saml_authentications
    end

    def setup_remote_jwt_authentications
      if has_jwt? && !remote_authentications.jwt.exists?
        options = remote_authentication_settings

        if options.delete(:type) == 'jwt'
          api_tokens.create!

          settings.api_token_access = true
          settings.save!

          options[:next_token] = Token.generate(48, false)
        else
          options.clear
        end

        remote_authentications.jwt.create!(options)
      end
    end

    def setup_remote_saml_authentications
      if has_saml? && !remote_authentications.saml.exists?
        options = remote_authentication_settings
        options.clear unless options.delete(:type) == 'saml'

        remote_authentications.saml.create!(options)
      end
    end

    def save_settings_and_texts
      # save settings early so that anyone selecting the account back in the after_save
      # chain gets the settings right
      settings.each(&:save)
      texts.each(&:save)
    end

    def add_sequences
      create_nice_id_sequence(value: 0, account: self)
      create_invoice_no_sequence(value: 0, account: self)
    end

    def add_subscription
      create_subscription(
        trial_coupon_code: trial_coupon_code,
        account: self,
        currency: @currency,
        is_trial: false
      )

      # Below properties are default in DB
      #   :max_agents         => 5
      #   :plan_type          => SubscriptionPlanType.Large
      #   :billing_cycle_type => BillingCycleType.Annually
    end

    def add_feature_bits
      Zendesk::Features::SubscriptionFeatureService.new(
        self
      ).execute!
    end

    def add_group
      groups.create!(name: I18n.t("txt.default.groups.support.name"), default: true)
    end

    # Setting the account owner is a two-step mechanism. First, the owner must be added using the
    # set_owner method before_create. After the account has been created, ie. after_create, the owner
    # object gets linked to the account using this method.
    def add_owner
      if multiproduct? && owner.present?
        owner.account      = self
        owner.organization = account.organizations.first
        Zendesk::SupportUsers::User.new(owner).assign_role(Role::ADMIN.id)
      else
        build_owner

        owner.account           = self
        owner.roles             = Role::ADMIN.id
        owner.account_id        = id
        owner.organization      = organizations.first
        owner.permission_set_id = nil
        Zendesk::SupportUsers::User.new(owner).save!
        becomes(Account).update_column(:owner_id, owner.id)
      end
    end

    def build_owner(*)
      raise 'Owner must be set before account can be created' if @owner_params.nil?

      owner = super(@owner_params)
      owner.password = @owner_params[:password] if @owner_params[:password].present?
    end

    def add_owner_to_group
      Membership.where(group_id: groups.first.id, user_id: owner.id).first_or_create
    end

    def add_address
      return if address # Address have already been set. E.g. a sandbox version.

      raise 'Address must be set before account can be created' if @addrs_params.nil?
      create_address!(@addrs_params)
    end

    def update_address
      return unless @addrs_params && address
      address.update_attributes(@addrs_params)
    end

    def add_default_brand
      return true if default_brand.present? && default_brand.account_id == id # New sandbox has a reference to the master account's brand
      Zendesk::BrandCreator.new(self, route).create!
    end

    def add_custom_statuses
      create_default_custom_statuses if has_create_default_custom_statuses_on_new_accounts?
    end

    def add_custom_fields
      add_field_subject
      add_field_description
      add_field_status
      add_field_ticket_type
      add_field_priority
      add_field_group
      add_field_assignee
    end

    def add_rules_and_reports
      load_rule_categories_from_yaml
      load_rules_from_yaml
      load_reports_from_yaml
    end

    private

    def create_rule_categories_from_yaml(file_path)
      file = File.open(file_path)
      categories = YAML.load(file)

      categories.each do |category|
        attrs = category['attributes'].merge(account: self)
        attrs['name'] = I18n.t(attrs['name'])

        RuleCategory.create!(attrs)
      end
    end

    def create_rules_from_yaml(file_path, class_name = nil)
      require_dependency 'trigger'
      require_dependency 'automation'
      require_dependency 'macro'

      # When removing email_ccs_new_accounts_default once it is GA, we should convert the
      # main DEFAULT_RULES_PATH file to have the contents of the CCs/Followers one and get
      # rid of the code here. We can add a deprecated rules file for the previous content
      # if we want it to stick around for a bit (or forever more likely).
      if has_email_ccs_default_rules_content? &&
          has_follower_and_email_cc_collaborations_enabled? &&
          (file_path == DEFAULT_RULES_PATH)
        file_path = DEFAULT_CCS_FOLLOWERS_RULES_PATH
      end

      file = File.open(file_path)
      rule_definitions = YAML.load(file)

      rule_definitions.each do |rule_definition|
        create_rule(rule_definition) if should_create_rule?(rule_definition, class_name)
      end
    end

    def should_create_rule?(rule_definition, class_name = nil)
      return if class_name && rule_definition['class_name'] != class_name
      definition = rule_definition['attributes']['definition']
      definition_items = definition.conditions_all + definition.conditions_any + definition.actions
      return if !(has_unlimited_triggers? && has_groups?) && definition_items.map(&:source).include?('group_id')
      return if !has_unlimited_triggers? && rule_definition['class_name'] == 'Trigger' && rule_definition['attributes']['is_active'] == '0'
      true
    end

    def create_rule(rule_definition)
      attrs = rule_definition['attributes'].merge(owner: self, owner_id: id, owner_type: "Account")
      assign_rule_category(rule_definition, attrs)
      translate_rule(attrs)
      attrs.except!("type") # Not accessible, type already encoded by class
      rule_definition['class_name'].constantize.create!(attrs)
    end

    def assign_rule_category(rule_definition, attrs)
      if has_trigger_categories_api_enabled? && rule_definition['class_name'] == 'Trigger'
        attrs.merge(rules_category_id: RuleCategory.trigger_categories(self).first.id)
      end
    end

    def remote_authentication_settings
      @remote_authentication_settings.try(:dup) || {}
    end

    def load_reports_from_yaml(path_to_reports = "db/defaults/reports.yml")
      file      = File.open(File.expand_path(path_to_reports, Rails.root))
      item_defs = YAML.load(file)

      item_defs.each do |data|
        pars = data['attributes'].symbolize_keys
        translate_report(pars)
        item = data['class_name'].constantize.new(pars)

        item.account = self
        item.author = owner
        item.save!
      end
    end

    def translate_report(attrs)
      attrs[:title] = I18n.t(attrs[:title])
      attrs[:definition].each do |series|
        series[:legend][:name] = CGI.unescapeHTML(I18n.t(series[:legend][:name]))
      end
    end

    def translate_rule(attrs)
      unless attrs["type"] == 'View'
        attrs["title"] = I18n.t(attrs["title"])
        attrs["description"] = I18n.t(attrs["description"]) if has_orca_classic_descriptions_in_trigger? && attrs["description"].present?
      end
      notification = attrs["definition"].actions.find do |action|
        ["notification_user", "notification_group"].include?(action.source)
      end

      if notification
        notification.value[1] = I18n.t(notification.value[1]).gsub("<br />", "\n")
        notification.value[2] = I18n.t(notification_body_key(notification.value[2])).gsub("<br />", "\n")
      end

      comment = attrs["definition"].actions.find { |action| action.source == "comment_value" }
      comment.value = I18n.t(comment.value) if comment
    end

    def notification_body_key(key)
      key.gsub(/VERSION/, version(key))
    end

    def default_export_whitelisted_domain
      if @owner_params && @owner_params[:email].present?
        @owner_params[:email].split("@").last
      end
    end

    def validate_trial_coupon_code
      unless trial_coupon_code.blank?
        c = Coupon.find_by_coupon_code(trial_coupon_code)
        if c.nil?
          errors.add :trial_coupon_code, "invalid coupon code: #{trial_coupon_code}"
        elsif !c.valid_for_signup?
          errors.add :trial_coupon_code, "coupon code #{trial_coupon_code} is not valid at signup"
        elsif !c.active?
          errors.add :trial_coupon_code, "coupon code #{trial_coupon_code} is not currently available"
        end
      end
    end

    def version(key)
      return "v4" if account.has_email_ccs_default_rules_content?

      # This seems to be done this way to allow for updating the version of the notification body
      # without having to update the stored rule itself. There is a special case here where rather than
      # update all notifications relying on `body_VERSION` being in key, only the "Notify requester of
      # received request" body was updated, leading to thie check here and the modification to use v4
      # instead of v3. The above fix synchronizes all of the strings to be on v4 so that the special
      # case is no longer necessary.
      #
      # In addition, we also default to using template bodies that do not contain placeholders that only
      # work in HC-enabled accounts. RestoreDefaultContentJob contains functionality to support that but
      # this code path does not.
      if key == "txt.default.triggers.notify_requester_received.body_VERSION"
        "v4"
      else
        "v3"
      end
    end

    def add_field_subject
      create_field_subject(
        account: self,
        title: I18n.t("txt.default.fields.subject.title"),
        title_in_portal: I18n.t("txt.default.fields.subject.title"),
        description: '',
        position: 1,
        is_visible_in_portal: true,
        is_editable_in_portal: true,
        is_required_in_portal: true,
        is_active: true
      )
    end

    def add_field_description
      create_field_description(
        account: self,
        title: I18n.t("txt.default.fields.description.title"),
        title_in_portal: I18n.t("txt.default.fields.description.title"),
        description: I18n.t("txt.default.fields.description.description"),
        position: 2,
        is_visible_in_portal: true,
        is_editable_in_portal: true,
        is_required_in_portal: true
      )
    end

    def add_field_status
      create_field_status(
        account: self,
        title: I18n.t("txt.default.fields.status.title"),
        description: I18n.t("txt.default.fields.status.description"),
        position: 3
      )
    end

    def add_field_ticket_type
      create_field_ticket_type(
        account: self,
        title: I18n.t("txt.default.fields.type.title"),
        description: I18n.t("txt.default.fields.type.description"),
        position: 4,
        is_active: true
      )
    end

    def add_field_priority
      create_field_priority(
        account: self,
        title: I18n.t("txt.default.fields.priority.title"),
        title_in_portal: I18n.t("txt.default.fields.priority.title"),
        description: I18n.t("txt.default.fields.priority.description"),
        position: 5,
        is_visible_in_portal: true,
        sub_type_id: PrioritySet.FULL
      )
    end

    def add_field_group
      create_field_group(
        account: self,
        title: I18n.t("txt.default.fields.group.title"),
        description: I18n.t("txt.default.fields.group.description"),
        position: 6
      )
    end

    def add_field_assignee
      create_field_assignee(
        account: self,
        title: I18n.t("txt.default.fields.assignee.title"),
        title_in_portal: I18n.t("txt.default.fields.assignee.title"),
        description: I18n.t("txt.default.fields.assignee.description"),
        position: 7,
        is_visible_in_portal: true,
        is_required: true
      )
    end

    def created_with_remote_authentication?
      remote_authentications.saml.first.try(:is_active?) ||
        remote_authentications.jwt.first.try(:is_active?)
    end

    def enable_agent_remote_login_method!
      role_settings.update_attributes!(
        agent_zendesk_login: false,
        agent_google_login: false,
        agent_twitter_login: false,
        agent_facebook_login: false,
        agent_remote_login: true
      )
    end

    def created_via_google?
      (redis_trial_extras.presence || trial_extras.to_hash)['created_via_google']
    end

    def created_via_office_365?
      (redis_trial_extras.presence || trial_extras.to_hash)['created_via_office_365']
    end
  end
end
