module Accounts
  class Classic < Base
    protected

    # Order dependent
    def add_owner
      add_organization

      super
    end

    def add_organization
      organizations.create!(name: sanitized_name, account: self)
    end

    def sanitized_name
      name.delete("|").squeeze(" ")
    end
  end
end
