module Accounts
  class Magento < Classic
    after_create :enable_api_token_access

    protected

    # @override
    def add_custom_fields
      super

      manager = Zendesk::Tickets::TicketFieldManager.new(self)
      manager.build(
        type: 'text',
        title: I18n.t('txt.admin.magento.custom_ticket_field.order_number_field_title', locale: translation_locale)
      ).save!
    end

    def enable_api_token_access
      api_tokens.create!
      settings.api_token_access = true
    end

    # Override of Base
    def setup_remote_jwt_authentications
      return unless has_jwt? && !remote_authentications.jwt.exists?
      auth_options = if remote_login_url.present?
        {
          is_active: true,
          next_token: Token.generate(48, false),
          remote_login_url: remote_login_url
        }
      else
        { is_active: false }
      end
      remote_authentications.jwt.create!(auth_options)
    end
  end
end
