module Accounts
  class GoogleAppMarket < Classic
    # @override
    def verification_link
      raw_zendesk_url = url(mapped: false)
      "#{raw_zendesk_url}/login?profile=google&return_to=#{CGI.escape("#{raw_zendesk_url}/login")}".html_safe
    end
  end
end
