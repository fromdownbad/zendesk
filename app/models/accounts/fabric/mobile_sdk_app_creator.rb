module Accounts
  module Fabric
    class MobileSdkAppCreator
      APP_SOURCE = 'twitter_fabric'.freeze

      def self.create(name:, account:)
        new(name, account).create
      end

      def initialize(name, account)
        @name = name
        @account = account
      end

      def create
        response = client.post('/api/v2/mobile_sdk_apps', mobile_sdk_app_params)

        response.body['mobile_sdk_app']
      rescue Kragle::ResponseError => e
        Rails.logger.info "[Fabric::MobileSdkAppCreator] Failed to create mobile_sdk_app with for: #{@account} - #{e.message}"
        return
      end

      private

      def client
        @client ||= KragleConnection.build_for_account(@account, 'classic')
      end

      def mobile_sdk_app_params
        {
          mobile_sdk_app: {
            title: "Fabric #{@name}",
            signup_source: APP_SOURCE,
            settings: {
              authentication: MobileSdkAuth::TYPE_ANONYMOUS,
              conversations_enabled: true,
              rma_enabled: true,
              connect_enabled: false,
              helpcenter_enabled: false,
              help_center_article_voting_enabled: true
            }
          }
        }
      end
    end
  end
end
