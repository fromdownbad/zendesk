module Accounts
  module Fabric
    class AccountCreator
      ACCOUNT_SOURCE = 'twitter_fabric'.freeze
      HELPDESK_SIZE = 'Large group'.freeze

      def self.create(params, subdomain:, account_creation_endpoint:, accept_tos_endpoint:, accept_tos: false)
        account_creator = new(
          params,
          subdomain: subdomain,
          account_creation_endpoint: account_creation_endpoint,
          accept_tos_endpoint: accept_tos_endpoint,
          accept_tos: accept_tos
        )
        account_creator.create
        account_creator.accept_tos

        account_creator.account
      end

      attr_reader :account

      def initialize(params, subdomain:, account_creation_endpoint:, accept_tos_endpoint:, accept_tos:)
        @params = params
        @subdomain = subdomain
        @accept_tos = accept_tos
        @account_creation_endpoint = account_creation_endpoint
        @accept_tos_endpoint = accept_tos_endpoint
      end

      def create
        response = client.post(@account_creation_endpoint, account_creation_params)

        @account = Account.find_by_subdomain(response.body['account']['subdomain'])
      rescue Kragle::ResponseError => e
        Rails.logger.info "[Fabric::AccountCreator] Failed to create account with params: #{@params} - #{e.message}"
      end

      def accept_tos
        if @accept_tos && @account
          KragleConnection.
            build_for_account(@account, 'classic').
            post(@accept_tos_endpoint)
        end
      rescue Kragle::ResponseError => e
        Rails.logger.info "[Fabric::AcceptTOS] Failed to accept TOS for: #{@new_account} - #{e.message}"
      end

      private

      def account_creation_params
        {
          owner: {
            name: @params[:name],
            email: @params[:email]
          },
          address: {
            phone: '1111111111'
          },
          account: {
            name: @params[:name],
            subdomain: @subdomain,
            help_desk_size: HELPDESK_SIZE,
            source: ACCOUNT_SOURCE
          }
        }
      end

      def client
        @client ||= begin
          Kragle.new('classic', signing: false) do |connection|
            connection.url_prefix = "https://zendeskaccounts.#{Zendesk::Configuration['host']}"
            connection.authorization :Bearer, Zendesk::Configuration['account_creation_tokens']['fabric']
            connection.options.timeout = 100
          end
        end
      end
    end
  end
end
