module Accounts
  module Fabric
    class SubdomainGenerator
      FORMAT = "fabric-%s".freeze
      NOT_ALLOWED_CHARS = %r{[^0-9a-z\-]}
      TEST_PHASE_PREFIX = 'z3n-'.freeze

      def initialize(email)
        @email = email
      end

      def subdomain
        with_z3n(format(FORMAT, @email)).
          downcase.
          gsub(NOT_ALLOWED_CHARS, ''). # remove invalid subdomain chars
          slice(0, DNS_CHARACTER_LIMIT)
      end

      private

      def with_z3n(subdomain)
        feature = Arturo::Feature.find_feature(:mobile_sdk_fabric_integration_test)

        if feature.try(:phase) == 'on' || feature.try(:deployment_percentage) == 100
          subdomain.prepend(TEST_PHASE_PREFIX)
        else
          subdomain
        end
      end
    end
  end
end
