class Compliance < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible :name, :compliance_agreements

  has_many :compliance_agreements, dependent: :destroy
end
