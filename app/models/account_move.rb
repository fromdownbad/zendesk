require 'zendesk/read_only_model'

class AccountMove < ActiveRecord::Base
  include Zendesk::ReadOnlyModel
  not_sharded
  belongs_to :account

  scope :to_local_pod, -> { where(target_shard_id: Zendesk::Accounts::DataCenter.local_shards) }

  attr_accessible
end
