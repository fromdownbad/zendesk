class SubscriptionFeatureAddon < ActiveRecord::Base
  include CIA::Auditable

  NON_UNIQUE_ADDON_NAMES = [:temporary_zendesk_agents].freeze

  TEMPORARY_ZENDESK_AGENTS_GRACE_PERIOD = 7
  STATUS = { started: 1, expired: 2 }.freeze

  belongs_to :account
  belongs_to :compliance_move

  attr_accessible :account, :account_id, :created_at, :updated_at,
    :boost_expires_at, :name, :zuora_rate_plan_id, :quantity, :expires_at,
    :starts_at, :status_id

  audit_attribute :account_id, :boost_expires_at, :name, :zuora_rate_plan_id,
    :quantity, :expires_at, :starts_at, :status_id, callback: :after_commit

  validates_presence_of :name, :account_id
  validate :require_zuora_id_or_valid_expiry_date!
  validates :name, uniqueness: { scope: :account_id }, if: :require_unique_name?

  # validates_inclusion_of :name, :in => ALL_ADDONS FROM CATALOG

  after_save   :update_co_terminus_expiry_date!, if: :is_boosted?
  after_commit :update_subscription_features!,   if: :is_boosted?

  scope :boosted, -> { where(zuora_rate_plan_id: nil) }
  scope :expired, -> { where('DATE(boost_expires_at) < UTC_DATE()') }

  scope :non_secondary_subscriptions, -> { where('name != ?', :temporary_zendesk_agents) }

  scope :active,  -> { where('DATE(starts_at) <= UTC_DATE() AND DATE(expires_at) > UTC_DATE()') }
  scope :by_name, ->(name) { where(name: name) }
  scope :active_plus_grace_period, ->(days) { where('DATE(starts_at) <= UTC_DATE() AND DATE_ADD(expires_at, INTERVAL ? DAY) > UTC_DATE()', days.to_i) }
  scope :expired_addons, ->(days) { where('DATE_ADD(expires_at, INTERVAL ? DAY) < UTC_DATE()', days.to_i) }
  scope :expires_at_unprocessed, -> { where(status_id: STATUS[:started]) }
  scope :starts_at_unprocessed,  -> { where(status_id: nil) }

  ignore_column :expiry_processed

  def is_boosted? # rubocop:disable Naming/PredicateName
    !!boost_expires_at
  end

  def status
    return if status_id.nil?
    STATUS.key(status_id)
  end

  def active?
    active_boosted? || active_purchased?
  end

  private

  def active_boosted?
    boost_expires_at.to_i > Time.now.to_i
  end

  def active_purchased?
    return false if zuora_rate_plan_id.blank?
    expires_at.blank? || expires_at.to_i > Time.now.to_i
  end

  def update_subscription_features!
    Zendesk::Features::SubscriptionFeatureService.new(
      account
    ).execute!
  end

  def update_co_terminus_expiry_date!
    account.subscription_feature_addons.boosted.update_all(boost_expires_at: boost_expires_at)
    # NOTE: Generally speaking, when saving changes to a FeatureBoost,
    # code should call Zendesk::SupportAccounts::Product#save! or one of the
    # other helper functions.  But in this code, we're probably already in the
    # context of saving a Subscription, and all Subscription saves should happen
    # via Zendesk::SupportAccounts::Product.  If we were to call
    # Zendesk::SupportAccounts::Product#save! here, that could be circular call,
    # which is dangerous. So this call should generally NOT use
    # Zendesk::SupportAccounts::Product#save! (though it should do so if called
    # outside of the scope of a Subscription#save!)
    account.feature_boost.update_attribute(:valid_until, boost_expires_at) if feature_boost_require_update?
  end

  def feature_boost_require_update?
    account.feature_boost.present? && account.feature_boost.is_active
  end

  def require_zuora_id_or_valid_expiry_date!
    if zuora_rate_plan_id.blank? && boost_expires_at.blank?
      errors.add :base, "need to provide either a zuora_rate_plan_id or a boost_expires_at value"
    elsif zuora_rate_plan_id.present? && boost_expires_at.present?
      errors.add :base, "cannot provide value for both zuora_rate_plan_id and boost_expires_at"
    elsif boost_expires_at.present? && boost_expires_at < DateTime.now
      errors.add :boost_expires_at, "can't be in the past"
    elsif boost_expires_at.present? && !account.is_sandbox? && boost_expires_at > maximum_boost_length.from_now
      errors.add :boost_expires_at, "cannot be longer than #{maximum_boost_length.inspect}"
    end
  end

  def maximum_boost_length
    account.has_remote_support_promo_boost_extension_addon? ? 6.months : 30.days
  end

  # Generally, active addon names should be unique. However, there are a few exceptions, defined in #non_unique_addon_names
  def require_unique_name?
    !NON_UNIQUE_ADDON_NAMES.include?(name.to_sym)
  end
end
