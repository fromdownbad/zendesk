class TrialExtension < ActiveRecord::Base
  not_sharded
  disable_global_uid

  TRIAL_EXTENSION_DEFAULT_DURATION = 7

  belongs_to :account, inverse_of: :trial_extensions
  belongs_to :subscription
  has_many :lifecycle_survey_answers, as: :surveyable

  attr_accessible :account, :duration, :author, :lifecycle_survey_answers
  after_initialize :add_default_duration

  before_validation :audit_subsciption

  validates :account, presence: true
  validates :author_account_id, presence: true
  validates :author_user_id, presence: true
  validates :subscription, presence: true
  validates :trial_expiration_was, presence: true

  before_save :extend_subscription

  def author=(author)
    self.author_user_id = author.id
    self.author_account_id = author.account.id
  end

  private

  def add_default_duration
    self.duration ||= TRIAL_EXTENSION_DEFAULT_DURATION
  end

  def audit_subsciption
    return unless account.try(:subscription)
    self.subscription = account.subscription
    self.trial_expiration_was = account.subscription.trial_expires_on
  end

  def extend_subscription
    return unless account.try(:subscription)
    account.subscription.extend_trial!(self.duration)
  end
end
