class SatisfactionRatingIntention < ActiveRecord::Base
  ALLOWED_SCORES = [SatisfactionType.GOOD, SatisfactionType.BAD].freeze
  attr_accessible :score, :user, :ticket

  belongs_to :account
  belongs_to :ticket
  belongs_to :user

  validates_inclusion_of :score, in: ALLOWED_SCORES

  after_create :count_created

  WITH_COMMENT = [SatisfactionType.GOODWITHCOMMENT, SatisfactionType.BADWITHCOMMENT].freeze

  def self.stats
    Zendesk::StatsD::Client.new(namespace: 'satisfaction_rating_intentions')
  end

  def rate!
    return if !ticket || ticket.closed? # deleted or closed
    return if !ticket.account.is_active? || !ticket.account.is_serviceable?

    new_score = if WITH_COMMENT.include?(ticket.satisfaction_score)
      case score
      when SatisfactionType.GOOD then SatisfactionType.GOODWITHCOMMENT
      when SatisfactionType.BAD then SatisfactionType.BADWITHCOMMENT
      else
        raise "Unknown score #{score}"
      end
    else
      score
    end

    ticket.attributes = {satisfaction_score: new_score}
    ticket.will_be_saved_by(user, via_id: ViaType.WEB_FORM)
    ticket.save!
  end

  private

  def count_created
    self.class.stats.increment 'created'
  end
end
