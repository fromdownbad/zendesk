class PermissionSet < ActiveRecord::Base
  belongs_to :account
  has_many :users, -> { where(is_active: true) }, inherit: :account_id

  attr_accessible :account, :name, :description, :role_type

  include PermissionSetObserver
  include CIA::Auditable
  include Zendesk::Serialization::PermissionSetSerialization
  include ::PermissionSet::Validations
  include ::PermissionSet::LightAgent
  include ::PermissionSet::ChatAgent
  include ::PermissionSet::Contributor
  include ::PermissionSet::BillingAdmin
  include CachingObserver
  extend ::PermissionSet::DefaultRoles

  ROLE_NAME_MAX_LENGTH = 255
  ROLE_DESCRIPTION_MAX_LENGTH = 16383

  validates_presence_of   :name
  validates_uniqueness_of :name, scope: :account_id
  validates_presence_of   :account_id
  validates_length_of     :name, maximum: ROLE_NAME_MAX_LENGTH, message: I18n.t('txt.admin.views.people.roles.role_definition.role_name.length_validation_error')
  validates_length_of     :description, maximum: ROLE_DESCRIPTION_MAX_LENGTH, message: I18n.t('txt.admin.views.people.roles.role_definition.description.length_validation_error')

  before_validation :set_default_role_type
  before_validation :disable_ticket_editing_permissions,
    if: proc { |p| !p.permissions.ticket_editing? }
  before_validation :disable_edit_organizations,
    if: proc { |p| %w[readonly edit-within-org].member?(p.permissions.end_user_profile) }
  before_validation :disable_view_deleted_tickets,
    if: proc { |p| !p.permissions.ticket_deletion? }
  before_validation :grant_access_to_restricted_forums
  before_validation :grant_access_to_ticket_editing
  before_validation :set_user_view_access
  before_destroy    :validate_role_has_no_users
  after_update      :set_user_restriction_ids

  scope :custom, -> { where(role_type: ::PermissionSet::Type::CUSTOM) }

  audit_attribute

  def set_user_view_access
    permissions.user_view_access = 'none' unless permissions.available_user_lists == 'all'
  end

  def view_user_profile_lists
    permissions.available_user_lists == 'all'
  end

  def set_default_role_type
    self.role_type ||= ::PermissionSet::Type::CUSTOM
  end

  def self.paginate(current_account, current_page)
    WillPaginate::Collection.create(current_page, ::PermissionSet.per_page) do |pager|
      results = current_account.permission_sets.limit(pager.per_page).offset(pager.offset).to_a

      # Will paginate needs you to specify the total_entries *sometimes*
      # (from the docs...)
      unless pager.total_entries
        pager.total_entries = current_account.permission_sets.count(:all)
      end

      # If we're on the last page of results, add the Admin role.
      if pager.current_page == pager.total_pages
        original_result_length = results.length
        results << Role::LEGACY_AGENT if current_account.agents.where(roles: Role::AGENT.id, permission_set_id: nil).count(:all) > 0
        results << Role::ADMIN

        pager.total_entries = pager.total_entries + (results.length - original_result_length)
      end

      pager.replace(results)
    end
  end

  def disable_ticket_editing_permissions
    permissions.disable(:ticket_deletion)
    permissions.disable(:ticket_merge)
    permissions.disable(:edit_ticket_tags)
    permissions.disable(:view_deleted_tickets)

    true
  end

  def disable_edit_organizations
    permissions.disable(:edit_organizations)
  end

  def has_ticket_restriction?(value) # rubocop:disable Naming/PredicateName
    ticket_access = permissions.ticket_access
    case value.to_s.downcase.to_sym
    when :none
      ticket_access == 'all'
    when :groups
      ticket_access == 'within-groups'
    when :organization
      ticket_access == 'within-organization'
    when :assigned
      ticket_access == 'assigned-only'
    end
  end

  property_set(:permissions, inherit: :account_id) do
    def self.permission(name, type:, default:, valid:)
      property name, type: type, default: default
      validates_inclusion_of :value,
        in: valid,
        if: proc { |permission| permission.name.to_sym == name }
    end

    def self.boolean_permission(name, default:)
      permission name, type: :boolean, default: default, valid: %w[0 1]
    end

    # Tickets
    permission :ticket_access,
      type:    :string,
      default: 'all',
      valid:   %w[all assigned-only within-groups within-organization]
    boolean_permission :ticket_editing,                      default: true
    boolean_permission :ticket_deletion,                     default: true
    boolean_permission :ticket_merge,                        default: true
    boolean_permission :edit_ticket_tags,                    default: true
    boolean_permission :assign_tickets_to_any_group,         default: false
    boolean_permission :view_ticket_satisfaction_prediction, default: true
    boolean_permission :view_deleted_tickets,                default: true
    boolean_permission :ticket_bulk_edit,                    default: true

    # Comments
    permission :comment_access,
      type:    :string,
      default: 'public',
      valid:   %w[none private public]

    # Explore
    permission :explore_access,
      type:    :string,
      default: 'readonly',
      valid:   %w[edit full none readonly]

    # Tools
    permission :report_access,
      type:    :string,
      default: 'full',
      valid:   %w[full none readonly]
    permission :view_access,
      type:    :string,
      default: 'full',
      valid:   %w[full manage-group manage-personal playonly readonly]
    permission :macro_access,
      type:    :string,
      default: 'full',
      valid:   %w[full manage-group manage-personal readonly]
    permission :user_view_access,
      type:    :string,
      default: 'readonly',
      valid:   %w[full manage-group manage-personal none readonly]

    # Users
    permission :available_user_lists,
      type:    :string,
      default: 'all',
      valid:   %w[all none]
    permission :end_user_profile,
      type:    :string,
      default: 'full',
      valid:   %w[edit edit-within-org full readonly]
    boolean_permission :edit_organizations, default: true

    # Forums
    permission :forum_access,
      type:    :string,
      default: 'full',
      valid:   %w[edit-topics full readonly]
    boolean_permission :forum_access_restricted_content, default: false

    # Voice
    boolean_permission :voice_availability_access, default: true
    boolean_permission :voice_dashboard_access, default: false

    # Chat
    boolean_permission :chat_availability_access, default: true

    # System
    boolean_permission :business_rule_management,          default: true
    boolean_permission :extensions_and_channel_management, default: false

    # Twitter
    boolean_permission :view_twitter_searches, default: true

    # Facebook
    boolean_permission :manage_facebook, default: false

    # Dynamic Content
    boolean_permission :manage_dynamic_content, default: false

    # Side Conversations
    boolean_permission :side_conversation_create, default: true
  end

  PermissionSetPermission.class_eval do
    attr_accessible :name, :value

    belongs_to :permission_set, touch: true, inherit: :account_id
    belongs_to :account

    include PermissionGuideEntitlementsChangesObserver
    include PermissionExploreEntitlementsChangesObserver
  end

  def is_system_role? # rubocop:disable Naming/PredicateName
    role_type != ::PermissionSet::Type::CUSTOM
  end

  def to_md5
    Digest::MD5.hexdigest(permissions.map(&:value).join(','))
  end

  # Returns hash of permissions including default values for permissions not defined in this property set
  def configuration
    default_permissions.get(default_permissions.map(&:name)).merge(
      permissions.get(permissions.map(&:name))
    ).symbolize_keys
  end

  def agent_count
    @agent_count ||= users.where(roles: Role::AGENT.id).count(:all)
  end

  # Help Center
  def manage_help_center
    %w[full edit-topics].include?(permissions.forum_access)
  end

  def translated_name
    if is_light_agent?
      I18n.t('txt.default.roles.light_agent.name')
    elsif is_chat_agent?
      I18n.t('txt.default.roles.chat_agent.name')
    elsif is_contributor?
      I18n.t('txt.default.roles.contributor.name')
    elsif is_billing_admin?
      I18n.t('txt.default.roles.billing_admin.name')
    else
      default_or_custom_role_name
    end
  end

  def translated_description
    if is_light_agent?
      I18n.t('txt.default.roles.light_agent.description')
    elsif is_chat_agent?
      I18n.t('txt.default.roles.chat_agent.description')
    elsif is_contributor?
      I18n.t('txt.default.roles.contributor.description')
    elsif is_billing_admin?
      I18n.t('txt.default.roles.billing_admin.description')
    else
      default_or_custom_role_description
    end
  end

  def pravda_role_name
    case role_type
    when Type::LIGHT_AGENT
      Zendesk::SupportAccounts::Role::System::LIGHT_AGENT_ROLE_NAME
    when Type::CUSTOM
      "custom_#{id}"
    end
  end

  def legacy_role_type
    case role_type
    when ::PermissionSet::Type::ADMIN, ::PermissionSet::Type::BILLING_ADMIN
      Role::ADMIN
    else
      Role::AGENT
    end
  end

  private

  def default_permissions
    self.class.build_new_permission_set(account).permissions
  end

  def set_user_restriction_ids
    RoleRestrictionType.list.detect do |name, restriction_id|
      next unless has_ticket_restriction?(name)
      User.where(permission_set_id: id).update_all(restriction_id: restriction_id)
    end
  end

  def grant_access_to_restricted_forums
    if permissions.forum_access == 'full'
      permissions.enable(:forum_access_restricted_content)
    end
  end

  def grant_access_to_ticket_editing
    if permissions.ticket_access == 'assigned-only'
      permissions.enable(:ticket_editing)
    end
  end

  def validate_role_has_no_users
    if agent_count != 0
      errors.add :base, I18n.t('txt.admin.views.people.roles.edit.remove_agent_in_role', count: agent_count)
      return false
    else
      users.where(account_id: account_id).update_all(permission_set_id: nil)
    end

    true
  end

  def disable_view_deleted_tickets
    permissions.disable(:view_deleted_tickets)
    true
  end

  def default_or_custom_role_name
    case name = read_attribute(:name)
    when I18n.t('txt.default.roles.advisor.name', locale: account.translation_locale)
      I18n.t('txt.default.roles.advisor.name')
    when I18n.t('txt.default.roles.staff.name', locale: account.translation_locale)
      I18n.t('txt.default.roles.staff.name')
    when I18n.t('txt.default.roles.team_leader.name', locale: account.translation_locale)
      I18n.t('txt.default.roles.team_leader.name')
    when *Zendesk::DynamicContent::SystemContent::ROLE_PLACEHOLDERS.values
      dc_renderer.render_role_name(name)
    else
      name
    end
  end

  def default_or_custom_role_description
    role_name = read_attribute(:name)

    description_of_custom_role(:advisor, role_name) ||
      description_of_custom_role(:staff, role_name) ||
      description_of_custom_role(:team_leader, role_name) ||
      read_attribute(:description)
  end

  def description_of_custom_role(role, name)
    I18n.t("txt.default.roles.#{role}.description") if [
      Zendesk::DynamicContent::SystemContent::ROLE_PLACEHOLDERS[role],
      I18n.t("txt.default.roles.#{role}.name"),
      I18n.t("txt.default.roles.#{role}.name", locale: account.translation_locale)
    ].include?(name)
  end

  def dc_renderer
    @dc_renderer ||= Zendesk::DynamicContent::Renderer.new(account)
  end
end
