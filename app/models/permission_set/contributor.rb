class PermissionSet < ActiveRecord::Base
  module Contributor
    def self.included(base)
      base.validate(:valid_contributor)
      base.send(:include, InstanceMethods)
      base.extend(ClassMethods)
    end

    def self.configuration
      {
        ticket_access:                     'within-groups',
        ticket_editing:                    false,
        ticket_deletion:                   false,
        view_deleted_tickets:              false,
        ticket_merge:                      false,
        edit_ticket_tags:                  false,
        assign_tickets_to_any_group:       false,
        ticket_bulk_edit:                  false,

        comment_access:                    'private',
        explore_access:                    'none',
        report_access:                     'none',
        view_access:                       'readonly',
        macro_access:                      'readonly',
        user_view_access:                  'none',

        end_user_profile:                  'readonly',
        available_user_lists:              'none',
        edit_organizations:                false,

        forum_access:                      'readonly',
        forum_access_restricted_content:   false,

        voice_availability_access:         false,
        voice_dashboard_access:            false,
        chat_availability_access:          false,

        business_rule_management:          false,
        extensions_and_channel_management: false,

        view_twitter_searches:             false,

        manage_dynamic_content:            false,
        manage_facebook:                   false
      }
    end

    module InstanceMethods
      def is_contributor? # rubocop:disable Naming/PredicateName
        role_type == Type::CONTRIBUTOR
      end

      def valid_contributor
        if new_record? && is_contributor? && account.present? && account.permission_sets.find_by_role_type(Type::CONTRIBUTOR)
          errors.add(:base, I18n.t('txt.default.roles.contributor.error.already_defined'))
        end

        if !new_record? && changed? && is_contributor?
          errors.add(:base, I18n.t('txt.default.roles.contributor.error.read_only'))
        end

        if !new_record? && is_contributor?
          if Contributor.configuration != configuration
            errors.add(:base, I18n.t('txt.default.roles.contributor.error.forbidden_permission'))
          end
        end
      end
    end

    module ClassMethods
      # If not found, will create an associated permission_set with a role_type
      # of `PermissionSet::Type::CONTRIBUTOR` for the account
      #
      # @param [Account] account The account for which the permission_set should
      #   be associated with
      def enable_contributor!(account)
        permission_set = find_contributor(account)
        return permission_set if permission_set

        rename_conflicting_contributor_role(account)

        begin
          PermissionSet.create! do |ps|
            ps.assign_attributes(
              account: account,
              role_type: Type::CONTRIBUTOR,
              name: I18n.t('txt.default.roles.contributor.name'),
              description: I18n.t('txt.default.roles.contributor.description')
            )
            ps.permissions.set(Contributor.configuration)
          end
        rescue ActiveRecord::RecordInvalid => e
          permission_set = find_contributor(account)
          raise e unless permission_set

          permission_set
        end
      end

      def find_contributor(account)
        account.permission_sets.where(role_type: Type::CONTRIBUTOR).first
      end

      def contributor_exists?(account)
        account.permission_sets.where(role_type: Type::CONTRIBUTOR).exists?
      end

      def rename_conflicting_contributor_role(account)
        conflicting_contributor_roles(account).each do |permission_set|
          permission_set.name += " (#{custom_permission_set_suffix})"
          permission_set.save!
        end
      end

      def conflicting_contributor_roles(account)
        cache = Zendesk::DynamicContent::SystemContent.cache
        contributor_role_name = cache['contributor_role']

        account.permission_sets.where(
          name:      contributor_role_name,
          role_type: ::PermissionSet::Type::CUSTOM
        )
      end

      def custom_permission_set_suffix
        I18n.t('txt.admin.helpers.ticket_field_helpers.custom_label')
      end

      # If found, will remove an associated permission_set with a role_type of
      # `PermissionSet::Type::CONTRIBUTOR` from the account
      #
      # @param [Account] account The account for which the permission_set is
      #   associated with
      def destroy_contributor!(account)
        role_type      = Type::CONTRIBUTOR
        permission_set = account.permission_sets.find_by_role_type(role_type)

        permission_set.try(:destroy)
      end
    end
  end
end
