class PermissionSet < ActiveRecord::Base
  module Type
    CUSTOM        = 0
    LIGHT_AGENT   = 1
    CHAT_AGENT    = 2
    CONTRIBUTOR   = 3
    ADMIN         = 4
    BILLING_ADMIN = 5

    def self.list
      [CUSTOM, LIGHT_AGENT, CHAT_AGENT, CONTRIBUTOR, ADMIN, BILLING_ADMIN]
    end
  end
end
