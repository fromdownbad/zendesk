class PermissionSet < ActiveRecord::Base
  module BillingAdmin
    def self.included(base)
      base.validate(:valid_billing_admin)
      base.send(:include, InstanceMethods)
      base.extend(ClassMethods)
    end

    def self.configuration
      {
        ticket_access:                     'all',
        ticket_editing:                    true,
        ticket_deletion:                   true,
        ticket_merge:                      true,
        edit_ticket_tags:                  true,
        assign_tickets_to_any_group:       true,
        view_deleted_tickets:              true,
        ticket_bulk_edit:                  true,

        comment_access:                    'public',
        explore_access:                    'full',
        report_access:                     'full',
        view_access:                       'full',
        macro_access:                      'full',
        user_view_access:                  'full',

        end_user_profile:                  'full',
        available_user_lists:              'all',
        edit_organizations:                true,

        forum_access:                      'full',
        forum_access_restricted_content:   true,

        voice_availability_access:         true,
        voice_dashboard_access:            true,
        chat_availability_access:          true,

        business_rule_management:          true,
        extensions_and_channel_management: true,

        view_twitter_searches:             true,

        manage_dynamic_content:            true,
        manage_facebook:                   true
      }
    end

    module InstanceMethods
      def is_billing_admin? # rubocop:disable Naming/PredicateName
        role_type == Type::BILLING_ADMIN
      end

      def valid_billing_admin
        if new_record? && is_billing_admin? && account.present? && account.permission_sets.find_by_role_type(Type::BILLING_ADMIN)
          errors.add(:base, I18n.t('txt.default.roles.billing_admin.error.already_defined'))
        end

        if !new_record? && changed? && is_billing_admin?
          errors.add(:base, I18n.t('txt.default.roles.billing_admin.error.read_only'))
        end
      end
    end

    module ClassMethods
      def create_billing_admin!(account)
        account.permission_sets.find_by_role_type(Type::BILLING_ADMIN) ||
          PermissionSet.create! do |ps|
            ps.assign_attributes(
              account: account,
              role_type: Type::BILLING_ADMIN,
              name: I18n.t('txt.default.roles.billing_admin.name'),
              description: I18n.t('txt.default.roles.billing_admin.description')
            )
            ps.permissions.set(BillingAdmin.configuration)
          end
      end

      def destroy_billing_admin!(account)
        permission_set = account.permission_sets.find_by_role_type(Type::BILLING_ADMIN)
        permission_set.try(:destroy)
      end
    end
  end
end
