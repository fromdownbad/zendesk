class PermissionSet < ActiveRecord::Base
  module ChatAgent
    def self.included(base)
      base.validate(:valid_chat_agent)
      base.send(:include, InstanceMethods)
      base.extend(ClassMethods)
    end

    def self.configuration
      {
        ticket_access:                     'within-groups',
        ticket_editing:                    false,
        ticket_deletion:                   false,
        view_deleted_tickets:              false,
        ticket_merge:                      false,
        edit_ticket_tags:                  false,
        assign_tickets_to_any_group:       false,
        ticket_bulk_edit:                  false,

        comment_access:                    'private',
        explore_access:                    'none',
        report_access:                     'none',
        view_access:                       'readonly',
        macro_access:                      'readonly',
        user_view_access:                  'none',

        end_user_profile:                  'readonly',
        available_user_lists:              'none',
        edit_organizations:                false,

        forum_access:                      'readonly',
        forum_access_restricted_content:   false,

        voice_availability_access:         false,
        voice_dashboard_access:            false,
        chat_availability_access:          true,

        business_rule_management:          false,
        extensions_and_channel_management: false,

        view_twitter_searches:             false,

        manage_dynamic_content:            false,
        manage_facebook:                   false
      }
    end

    module InstanceMethods
      def is_chat_agent? # rubocop:disable Naming/PredicateName
        role_type == Type::CHAT_AGENT
      end

      def valid_chat_agent
        if new_record? && is_chat_agent? && account.present? && account.permission_sets.find_by_role_type(Type::CHAT_AGENT)
          errors.add(:base, I18n.t('txt.default.roles.chat_agent.error.already_defined'))
        end

        if !new_record? && changed? && is_chat_agent?
          errors.add(:base, I18n.t('txt.default.roles.chat_agent.error.read_only'))
        end

        if !new_record? && is_chat_agent?
          if ChatAgent.configuration != configuration
            errors.add(:base, I18n.t('txt.default.roles.chat_agent.error.forbidden_permission_2'))
          end
        end
      end
    end

    module ClassMethods
      # If not found, will create an associated permission_set with a role_type
      # of `PermissionSet::Type::CHAT_AGENT` for the account
      #
      # @param [Account] account The account for which the permission_set should
      #   be associated with
      def create_chat_agent!(account)
        # In order to deprecate chat only agent, we want to instrument
        # method calls of create_chat_agent!
        Zendesk::MethodCallInstrumentation.new(
          method_name: :create_chat_agent!,
          exec_context: binding,
          source_pattern: 'app\/services\/provisioning\/before_actions'
        ).perform

        # NOTE: Account's with an active Suite subscription are not allowed to
        #       have a CHAT_AGENT permission set.
        return if account.settings.reload.suite_subscription?

        if account.has_ocp_chat_only_agent_deprecation?
          PermissionSet.enable_contributor!(account)
        else
          permission_sets  = account.permission_sets
          chat_permissions = permission_sets.where(role_type: Type::CHAT_AGENT)

          chat_permissions.first_or_create! do |permission_set|
            permission_set.assign_attributes(
              name:        I18n.t('txt.default.roles.chat_agent.name'),
              description: I18n.t('txt.default.roles.chat_agent.description')
            )

            permission_set.permissions.set(ChatAgent.configuration)
          end
        end
      end

      # If found, will remove an associated permission_set with a role_type of
      # `PermissionSet::Type::CHAT_AGENT` from the account
      #
      # @param [Account] account The account for which the permission_set is
      #   associated with
      def destroy_chat_agent!(account)
        role_type      = Type::CHAT_AGENT
        permission_set = account.permission_sets.find_by_role_type(role_type)
        return if permission_set.nil?

        permission_set.try(:destroy)
      end
    end
  end
end
