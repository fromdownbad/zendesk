class PermissionSet < ActiveRecord::Base
  module Validations
    def self.included(base)
      base.send(:validates_inclusion_of, :role_type, in: Type.list)
      base.send(:validate, :max_limit, on: :create)
    end

    # This number is 200 minus number of system roles (agent, admin & light agent)
    MAX_LIMIT = 197

    def max_limit
      if role_type == Type::CUSTOM && account.permission_sets.custom.count > MAX_LIMIT
        errors.add(:base, ::I18n.t('activerecord.errors.models.permission_sets.max_limit_reached'))
      end
    end
  end
end
