class PermissionSet < ActiveRecord::Base
  module LightAgent
    def self.included(base)
      base.validate(:valid_light_agent)
      base.send(:include, InstanceMethods)
      base.extend(ClassMethods)
    end

    def self.configuration
      {
        ticket_access:                     'within-groups',
        ticket_editing:                    false,
        ticket_deletion:                   false,
        ticket_merge:                      false,
        edit_ticket_tags:                  false,
        assign_tickets_to_any_group:       false,
        view_deleted_tickets:              false,
        ticket_bulk_edit:                  true,

        comment_access:                    'private',
        explore_access:                    'readonly',
        report_access:                     'readonly',
        view_access:                       'readonly',
        macro_access:                      'readonly',
        user_view_access:                  'none',

        end_user_profile:                  'readonly',
        available_user_lists:              'all',
        edit_organizations:                false,

        forum_access:                      'readonly',
        forum_access_restricted_content:   false,

        voice_availability_access:         false,
        voice_dashboard_access:            false,
        chat_availability_access:          false,

        business_rule_management:          false,
        extensions_and_channel_management: false,

        view_twitter_searches:             true,

        manage_dynamic_content:            false,
        manage_facebook:                   false
      }
    end

    module InstanceMethods
      def is_light_agent? # rubocop:disable Naming/PredicateName
        role_type == Type::LIGHT_AGENT
      end

      def valid_light_agent
        if new_record? && is_light_agent? && account.present? && account.permission_sets.find_by_role_type(Type::LIGHT_AGENT)
          errors.add(:base, I18n.t('txt.default.roles.light_agent.error.already_defined'))
        end

        if !new_record? && changed? && is_light_agent?
          errors.add(:base, I18n.t('txt.default.roles.light_agent.error.read_only'))
        end

        if !new_record? && is_light_agent?
          editable_permissions = %i[ticket_access assign_tickets_to_any_group report_access view_ticket_satisfaction_prediction]

          if LightAgent.configuration.except(*editable_permissions) != configuration.except(*editable_permissions)
            errors.add(:base, I18n.t('txt.default.roles.light_agent.error.forbidden_permission_2'))
          end

          unless %w[none readonly].include?(configuration[:report_access])
            errors.add(:base, I18n.t('txt.default.roles.light_agent.error.reporting_edit'))
          end
        end
      end
    end

    module ClassMethods
      def create_light_agent!(account)
        renaming_conflicting_light_agent_role(account)

        account.permission_sets.find_by_role_type(Type::LIGHT_AGENT) ||
          initialize_light_agent(account).tap do |permission_set|
            Zendesk::SupportUsers::PermissionSet.new(permission_set).save!(context: 'light_agent_enabled')
          end
      end

      def destroy_light_agent!(account)
        light_agent_role = account.light_agent_permission_set
        return if light_agent_role.nil?

        Zendesk::SupportUsers::PermissionSet.new(light_agent_role).destroy(context: 'light_agent_disabled')
      end

      def renaming_conflicting_light_agent_role(account)
        conflicting_light_agent_roles(account).each do |permission_set|
          permission_set.name += " (#{custom_permission_set_suffix})"
          permission_set.save!
        end
      end

      def conflicting_light_agent_roles(account)
        cache            = Zendesk::DynamicContent::SystemContent.cache
        light_agent_name = cache['light_agent_role']

        account.permission_sets.where(
          name:      light_agent_name,
          role_type: ::PermissionSet::Type::CUSTOM
        )
      end

      def custom_permission_set_suffix
        I18n.t('txt.admin.helpers.ticket_field_helpers.custom_label')
      end

      def initialize_light_agent(account)
        account.permission_sets.new(
          name:        I18n.t('txt.default.roles.light_agent.name'),
          description: I18n.t('txt.default.roles.light_agent.description'),
          role_type:   Type::LIGHT_AGENT
        ).tap do |permission_set|
          permission_set.permissions.set(LightAgent.configuration)
        end
      end
    end
  end
end
