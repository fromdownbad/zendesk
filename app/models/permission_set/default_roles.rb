class PermissionSet < ActiveRecord::Base
  module DefaultRoles
    def build_default_permission_sets(account)
      [
        account.permission_sets.new(
          account:     account,
          name:        I18n.t('txt.default.roles.staff.name'),
          description: I18n.t('txt.default.roles.staff.description'),
          role_type:   ::PermissionSet::Type::CUSTOM
        ).tap do |staff|
          staff.permissions.set(
            ticket_editing:                    true,
            ticket_access:                     'within-groups',
            edit_ticket_tags:                  false,
            ticket_deletion:                   false,
            view_deleted_tickets:              false,
            ticket_merge:                      false,
            ticket_bulk_edit:                  true,

            comment_access:                    'public',

            explore_access:                    'readonly',
            report_access:                     'readonly',
            view_access:                       'manage-personal',
            macro_access:                      'manage-personal',
            user_view_access:                  'manage-personal',

            end_user_profile:                  'readonly',

            forum_access:                      'readonly',
            forum_access_restricted_content:   false,

            business_rule_management:          false,
            extensions_and_channel_management: false,

            view_twitter_searches:             true,
            voice_dashboard_access:            false,
            manage_dynamic_content:            false,
            manage_facebook:                   false
          )
        end,
        account.permission_sets.new(
          account:     account,
          name:        I18n.t('txt.default.roles.team_leader.name'),
          description: I18n.t('txt.default.roles.team_leader.description'),
          role_type:   ::PermissionSet::Type::CUSTOM
        ).tap do |team_leader|
          team_leader.permissions.set(
            ticket_editing:                    true,
            ticket_access:                     'all',
            edit_ticket_tags:                  true,
            ticket_deletion:                   true,
            view_deleted_tickets:              true,
            ticket_merge:                      true,
            ticket_bulk_edit:                  true,

            comment_access:                    'public',

            explore_access:                    'edit',
            report_access:                     'full',
            view_access:                       'full',
            macro_access:                      'full',
            user_view_access:                  'full',

            end_user_profile:                  'full',

            forum_access:                      'full',
            forum_access_restricted_content:   true,

            business_rule_management:          false,
            extensions_and_channel_management: false,

            view_twitter_searches:             true,
            voice_dashboard_access:            true,
            manage_dynamic_content:            false,
            manage_facebook:                   false
          )
        end,
        account.permission_sets.new(
          account:     account,
          name:        I18n.t('txt.default.roles.advisor.name'),
          description: I18n.t('txt.default.roles.advisor.description'),
          role_type:   ::PermissionSet::Type::CUSTOM
        ).tap do |advisor|
          explore_access = if account.has_central_admin_staff_mgmt_roles_tab?
            'readonly'
          else
            'none'
          end

          advisor.permissions.set(
            ticket_editing:                    true,
            ticket_access:                     'within-groups',
            edit_ticket_tags:                  true,
            ticket_deletion:                   false,
            view_deleted_tickets:              false,
            ticket_merge:                      false,
            ticket_bulk_edit:                  true,

            comment_access:                    'private',

            explore_access:                    explore_access,
            report_access:                     'none',
            view_access:                       'full',
            macro_access:                      'full',
            user_view_access:                  'full',

            end_user_profile:                  'readonly',

            forum_access:                      'readonly',
            forum_access_restricted_content:   false,

            business_rule_management:          true,
            extensions_and_channel_management: true,

            view_twitter_searches:             true,
            voice_dashboard_access:            false,
            manage_dynamic_content:            false,
            manage_facebook:                   false
          )
        end
      ]
    end

    def build_new_permission_set(account)
      account.permission_sets.new(
        account:   account,
        role_type: ::PermissionSet::Type::CUSTOM
      ).tap do |role|
        role.permissions.set(
          ticket_editing:                    true,
          ticket_access:                     'all',
          edit_ticket_tags:                  false,
          ticket_deletion:                   false,
          view_deleted_tickets:              false,
          ticket_merge:                      false,
          ticket_bulk_edit:                  true,

          comment_access:                    'public',

          explore_access:                    'readonly',
          report_access:                     'none',
          view_access:                       'readonly',
          macro_access:                      'readonly',
          user_view_access:                  'readonly',

          end_user_profile:                  'readonly',

          edit_organizations:                false,

          forum_access:                      'readonly',
          forum_access_restricted_content:   false,

          business_rule_management:          false,
          extensions_and_channel_management: false,

          view_twitter_searches:             false,
          voice_dashboard_access:            false,
          chat_availability_access:          false,
          manage_dynamic_content:            false,
          manage_facebook:                   false
        )
      end
    end
  end
end
