class ReservedSubdomain < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible

  EXISTENCE_QUERY = '(`pattern` = ? AND `is_regex` = 0) OR (? REGEXP `pattern` AND `is_regex` = 1)'.freeze

  validates_presence_of :pattern
  validates_uniqueness_of :pattern, scope: :is_regex

  class << self
    def reserved?(desired)
      exists?([EXISTENCE_QUERY, desired, desired])
    end
  end
end
