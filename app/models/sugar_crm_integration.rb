require_dependency 'zendesk/crm/integration'

class SugarCrmIntegration < ActiveRecord::Base
  belongs_to :account

  attr_accessible :url, :username, :password, :active, :account

  validates_presence_of :url, :username, :password, :account_id
  validate :validate_url
  after_create  :destroy_other_integrations, :create_target
  after_destroy :destroy_target

  def integration_name
    "SugarCRM"
  end

  def is_sandbox? # rubocop:disable Naming/PredicateName
    false
  end

  def configured?
    [url, username, password].all?
  end

  def enabled?
    configured? && active?
  end

  def fetch_info_for(user)
    return {records: []} unless user.email.present?
    Zendesk::Crm::Integration.parse_user_info_xml(get_user_info(user.email), url)
  end

  def crm_data(user, create)
    if create && user.sugar_crm_data.nil?
      user.create_sugar_crm_data
    else
      user.sugar_crm_data
    end
  end

  def create_case(ticket)
    path = "/custom/zendeskapi/v2/rest.php"
    ticket_xml = Zendesk::Crm::Integration.ticket_xml(ticket)

    xml_data = ticket_xml.gsub(/&/i, '!AMP!')
    rest_data = "{\"session\":#{get_session_id.to_json},\"data\":#{xml_data.to_json}}"
    response = post(path, "method=syncticket&input_type=JSON&rest_data=#{CGI.escape(rest_data)}")

    unless response.status == 200 && response.body.starts_with?("OK")
      raise "Error creating SugarCRM case in integration #{id}: #{response.body}"
    end
    response.body
  end

  private

  def get_session_id # rubocop:disable Naming/AccessorMethodName
    data = JSON.parse(send_login)

    if data['id'].nil?
      raise "SugarCRM Login Failed in integration #{id}: #{data}"
    end

    data['id']
  end

  def get_user_info(email)
    path = "/custom/zendeskapi/v2/rest.php"
    json = <<-JSON
      {
        "session": "#{get_session_id}",
        "email": "#{email}"
      }
    JSON
    data = "method=crmdata&input_type=JSON&rest_data=#{json}"
    response = post(path, data)
    response.body
  end

  def send_login
    path = "/service/v2/rest.php"
    json = <<-JSON
      {
        "user_auth": {
          "user_name": "#{username}",
          "password": "#{OpenSSL::Digest::MD5.new(password)}"
        }
      }
    JSON
    data = "method=login&input_type=JSON&response_type=JSON&rest_data=#{json}"

    # sugar instances sometimes return 500 internal error when requesting a session id
    # so, let's retry a couple of times
    tries = 0
    response = nil
    loop do
      response = post(path, data)
      tries += 1
      break if response.status == 200 || tries == 5
    end

    if response.status != 200
      raise "SugarCRM Connection Problem during login in integration #{id}. Code: #{response.status}, Message: #{response.body}"
    end

    response.body
  end

  def post(path, data)
    uri = URI.parse(url)
    uri.path = File.join(uri.path, path)
    Zendesk::Net::MultiSSL.post(uri.to_s, body: data, ignore_retry_notice: true)
  end

  def validate_url
    errors.add(:url, :invalid) unless url =~ URL_PATTERN
  end

  def destroy_other_integrations
    account.salesforce_production.try(:destroy)
    account.salesforce_sandbox.try(:destroy)
    account.ms_dynamics_integration.try(:destroy)
  end

  def create_target
    unless account.targets.find_by_type("SugarCrmTarget")
      target = SugarCrmTarget.new(account: account, title: "SugarCRM Target", is_active: true)
      target.current_user = account.owner
      target.save
    end
  end

  def destroy_target
    account.targets.where(type: "SugarCrmTarget").map(&:destroy)
  end
end
