class PasswordChange < ActiveRecord::Base
  self.table_name = 'user_audits'
  belongs_to :account
  belongs_to :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  belongs_to :actor, class_name: 'User', inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  attr_accessible :actor_id, :previous_value, :new_value, :ip_address

  scope :previous, ->(limit) { limit(limit).order('created_at desc, id desc') }

  validates_presence_of :user_id, :account_id

  before_validation :inherit_account_from_user, on: :create

  protected

  def inherit_account_from_user
    self.account_id = user.account_id
  end
end
