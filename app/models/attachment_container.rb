class AttachmentContainer < ActiveRecord::Base
  belongs_to :attachment, inherit: :account_id
  belongs_to :account

  attr_accessible :account, :attachment
end
