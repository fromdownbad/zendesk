class CustomFieldOption < ActiveRecord::Base
  include CachingObserver
  extend Zendesk::DB::BulkUidAllocation

  scope :active, -> { where(deleted_at: nil) }

  belongs_to :custom_field, class_name: 'FieldTagger'
  belongs_to :account

  attr_accessible :account, :custom_field, :name, :value, :position, :default

  before_validation :set_account

  validates_presence_of :account_id
  validates_presence_of :value, message: I18n.t('txt.admin.models.ticket_fields.field_tagger.must_have_tag')
  validate :validate_value_is_taggable, unless: :deleted?
  validate :validate_used_in_conditions

  before_validation :normalize_value
  before_save :set_account

  has_soft_deletion default_scope: true

  validates_lengths_from_database only: [:name]

  def validate_used_in_conditions
    return if new_record?
    return unless value_changed? || deleted?
    return unless account.try(:has_native_conditional_fields_enabled?)
    conditions = account.ticket_field_conditions.where(parent_field_id: custom_field_id)
    if conditions.detect { |c| c.value == value_was }
      errors.add(:value, I18n.t('txt.admin.models.ticket_field.ticket_field.option_cannot_be_changed_by_condition', option_name: name))
    end
  end

  # SHARDING: This is only necessary for the transitional period until the account_id has been added to all rows
  alias_method :account_association, :account
  def account
    account_association || set_account
  end

  def self.normalize_value(value, account = nil)
    return unless value.present?

    TagManagement.downcase_tag_string(value.strip, account)
  end

  def value
    if use_enhanced_values?
      # If a field was deleted when this account was added to the special_chars_in_custom_field_options feature
      # and then we try to undelete the field, it will fail because `enhanced_value` is nil. This will fall
      # back to `value` to ensure we can properly undelete the field.
      self[:enhanced_value] || self[:value]
    else
      self[:value]
    end
  end

  def value=(val)
    self[:enhanced_value] = val
    self[:value] = use_enhanced_values? ? nil : val
  end

  def normalize_value
    self.value = self.class.normalize_value(value, account)
  end

  private ##########################################

  def set_account
    self.account = custom_field.account if custom_field.present?
  end

  def use_enhanced_values?
    return false if account.nil?
    account.has_special_chars_in_custom_field_options?
  end

  def validate_value_is_taggable
    invalid_special_characters = (account && account.has_special_chars_in_custom_field_options?) ? false : (value =~ SPECIAL_CHARS)
    if invalid_special_characters || value =~ /[^[:graph:]]/
      errors.add(:value, Api.error('txt.admin.models.ticket_fields.field_tagger.cannot_contain_special_characters', error: 'ValueNotAccepted'))
    elsif value && value.match?(/\|/)
      errors.add(:value, Api.error('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_character', url: I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_documentation'), error: 'ValueNotAccepted'))
    end
  end
end
