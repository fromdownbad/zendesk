require 'zendesk/read_only_model'

class AccountCreationShard < ActiveRecord::Base
  include Zendesk::ReadOnlyModel
  not_sharded

  attr_accessible
end
