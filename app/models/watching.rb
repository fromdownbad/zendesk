class Watching < ActiveRecord::Base
  include ForumObserver
  has_soft_deletion default_scope: true

  @@per_page = 30
  cattr_reader :per_page

  scope :for_account, ->(account_id) {
    account_id = account_id.id if account_id.is_a?(Account)
    where(account_id: account_id)
  }

  scope :for_source, ->(source_type, source_id) {
    where(source_type: source_type, source_id: source_id)
  }

  scope :only_entry, -> { where(source_type: 'Entry') }
  scope :only_forum, -> { where(source_type: 'Forum') }
  scope :only_organization, -> { where(source_type: 'Organization') }

  belongs_to :user
  belongs_to :account
  belongs_to :source, polymorphic: true

  attr_accessible :account, :user, :source

  validates_uniqueness_of :user_id, scope: [:source_id, :source_type]
  validates_presence_of   :source, :user

  before_create :initialize_token

  def self.find_by_account_id_and_token(account_id, token)
    where(account_id: account_id, token: token.to_s).first
  end

  def unsubscribe_posts!
    update_attribute(:include_posts, false)
  end

  def can_view_source?(user)
    source && user.can?(:view, (source.is_a?(Entry) ? source.forum : source))
  end

  class << self
    # Remove watchings based on account_id a token and return the right I18n feedback message
    #
    # (Takes into account if it's a Forum or an Entry and returns proper messages)
    #
    # account_id - the id of the account the token belongs
    # token      - the token associated to the watching
    #
    # Returns the String with an I18n feedback message
    def delete_and_interpolate_feedback_message!(account_id, token)
      watching = Watching.find_by_account_id_and_token(account_id, token)
      # ZD645423 Adding logging to track down issues with unsubscribes
      Rails.logger.warn("Removing #{token} for account #{account_id} with watching object #{watching.inspect}.")

      return I18n.t('txt.watchings.destroy.fail') if watching.nil?
      if delete_all_associated_with(account_id, token)
        return success_removal_feedback_message_for(watching)
      end
      I18n.t('txt.watchings.destroy.fail')
    end

    private

    def delete_all_associated_with(account_id, token)
      return true if Watching.delete_all(account_id: account_id, token: token) > 0
      false
    end

    def success_removal_feedback_message_for(watching)
      return I18n.t('txt.watchings.destroy.message_forum', forum_name: watching.source.name) if watching.source.is_a?(Forum)
      return I18n.t('txt.watchings.destroy.message_topic', topic_name: watching.source.name) if watching.source.is_a?(Entry)
    end
  end

  private

  def initialize_token
    self.token = Token.generate
  end
end
