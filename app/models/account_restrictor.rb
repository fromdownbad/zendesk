# Checks whether records belong to a different account.
#
# This is used by AccountRestrictionObserver to make sure that records aren't
# loaded or saved on the wrong account.
#
# Examples
#
#   AccountRestrictor.deny?(some_record)
#   #=> true
#
class AccountRestrictor
  # These models are allowed to be loaded from more than one account.
  # GlobalClient: Accessible by all accounts, original creating account is kept on record
  SKIPPED_MODELS = Set[TranslationLocale, Zendesk::OAuth::GlobalClient]

  class << self
    # Checks whether a record belongs to a different account.
    #
    # record - The ActiveRecord::Base object that should be checked.
    #
    # Returns true if the record is from a different account, false otherwise.
    def deny?(record)
      return false if Zendesk.current_account.nil?

      account_restricted?(record) && !right_account?(record)
    end

    private

    def account_restricted?(record)
      !SKIPPED_MODELS.include?(record.class)
    end

    def right_account?(record)
      # We need to use read_attribute, as the attribute may not be loaded. For
      # instance, #exists? only loads the id. Calling #account_id on that record
      # would raise ActiveRecord::MissingAttributeError, but #read_attribute
      # just returns nil.
      account_id = record.read_attribute(:account_id)
      return true unless account_id.present?

      account_id == Zendesk.current_account.id ||
        account_id == Account.system_account_id ||
        Zendesk.current_account.is_sandbox_master_of?(account_id)
    end
  end
end
