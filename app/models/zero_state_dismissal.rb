class ZeroStateDismissal < ActiveRecord::Base
  belongs_to :user
  belongs_to :account

  attr_accessible :account, :user, :level, :zero_state, :status

  module DismissalLevel
    ACCOUNT   = 0
    USER      = 1
  end

  module DismissalStatus
    FINISHED  = 0
    DISMISSED = 1
    IGNORED   = 2
  end

  validates :account, :user, :zero_state, :status, presence: true

  class << self
    def from_dismissals(user, dismissals)
      dismissals.map do |dismissal|
        create!(dismissal.merge(account: user.account, user: user)) rescue nil
      end.compact
    end
  end
end
