# Produce to a Kafka topic, through MySQL.
#
# Once written to the database, the message will be extracted and published to
# the specified Kafka topic. This way, you can guarantee that a message
# publication is consistent with a database update by wrapping it in a
# transaction.
#
# This table uses a BLACKHOLE storage engine (the /dev/null of MySQL tables).
# You can INSERT records, but the table will always appear empty.
#
# See https://zendesk.atlassian.net/wiki/spaces/MELINF/pages/570167632/Escape+ESC+Publishing+Kafka+Messages+via+MySQL
#
# ## Example
#
#     EscKafkaMessage.create!(
#       topic: "greetings",
#       key: "hello",
#       value: "world",
#       partition_key: id # Note: using `account_id` can hot-spot consumers
#     )
#
class EscKafkaMessage < ActiveRecord::Base
  def self.primary_key
    nil
  end
  self.primary_key = false
  disable_global_uid

  attr_accessible :account_id, :topic, :partition_key, :key, :value, :metadata
end
