class ResourceCollection < ActiveRecord::Base
  has_soft_deletion default_scope: true

  belongs_to :account

  attr_accessible :account

  has_many :collection_resources,
    class_name: 'ResourceCollectionResource',
    inverse_of: :resource_collection

  scope :for_account, ->(account) { where(account_id: account.id) }
end
