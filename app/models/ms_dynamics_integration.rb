require_dependency 'zendesk/ms_dynamics/integration'
require_dependency 'zendesk/crm/integration'

# user_id is not a user on our system
class MsDynamicsIntegration < ActiveRecord::Base
  belongs_to :account

  attr_accessible :user_id, :password, :account, :web_service_address, :server_address, :discovery_uri,
    :organization_uri, :organization_name, :domain_name, :type_code, :active, :additional_domains

  after_create  :destroy_other_integrations, :create_target
  after_destroy :destroy_target

  validates_presence_of :user_id, :password, :web_service_address, :server_address
  validates_presence_of :discovery_uri, :organization_uri, if: :on_cloud?

  def integration_name
    "Microsoft Dynamics"
  end

  def is_sandbox? # rubocop:disable Naming/PredicateName
    false
  end

  def configured?
    [user_id, password].all?
  end

  def enabled?
    configured? && active?
  end

  def fetch_info_for(user)
    return {records: []} if user.email.blank?
    parse_user_info(get_user_info(user.email))
  end

  def crm_data(user, create)
    if create && user.ms_dynamics_data.nil?
      user.create_ms_dynamics_data
    else
      user.ms_dynamics_data
    end
  end

  def create_case(ticket)
    # Get requester guid
    data = fetch_info_for(ticket.requester)
    guid = data[:records].first[:id] rescue nil

    # If the guid (CrmRecordId) is present the service use that contact to associate the case.
    # If the guid is not present the service will use the given last name (ContactLastName) and email (ContactEmailId) to create a contact and associate the case.
    input = MsDynamics::Integration::EscalateZendeskTicketAsCrmCase.new(ticket_xml(ticket, guid))
    json = driver.escalateZendeskTicketAsCrmCase(input).escalateZendeskTicketAsCrmCaseResult
    parse_ticket_response(json)
  end

  def test_connection
    response = validate_connection

    json = JSON.parse(response.gsub("\n", "\\n"))
    unless json["records"].present?
      message = json["message"] || "No message received"
      raise "Error validating user in integration #{id}: #{message}"
    end
  end

  def additional_domains_for_policy
    additional_domains.present? ? additional_domains.gsub(/\s+/, "").split(",") : []
  end

  def server_address_for_policy
    (on_premise? || ifd?) ? server_uri.host : server_address
  end

  private

  def get_user_info(email)
    input = MsDynamics::Integration::SearchUserProfileByEmailAddress.new(user_info_xml(email))
    driver.searchUserProfileByEmailAddress(input).searchUserProfileByEmailAddressResult
  end

  def validate_connection
    input = MsDynamics::Integration::ValidateUserToGetGUIDandSR.new(validate_connection_xml)
    driver.validateUserToGetGUIDandSR(input).validateUserToGetGUIDandSRResult
  end

  def validate_connection_xml
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.InputXml do
        add_standard_values(xml)
      end
    end

    builder.to_xml(save_with: Nokogiri::XML::Node::SaveOptions::AS_XML | Nokogiri::XML::Node::SaveOptions::NO_DECLARATION).strip
  end

  def user_info_xml(email)
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.InputXml do
        xml.EmailId email
        xml.ZendeskOrganization ""
        add_standard_values(xml)
      end
    end

    builder.to_xml(save_with: Nokogiri::XML::Node::SaveOptions::AS_XML | Nokogiri::XML::Node::SaveOptions::NO_DECLARATION).strip
  end

  def ticket_xml(ticket, guid)
    priority = ticket.priority.gsub("-", "No Value")
    type = TicketType[ticket.ticket_type_id].name.gsub("-", "No Value")

    builder = Nokogiri::XML::Builder.new do |xml|
      xml.InputXml do
        xml.CrmRecordType "C"
        xml.CrmRecordId guid
        xml.ContactLastName ticket.requester.last_name
        xml.ContactEmailId ticket.requester.email
        xml.ZD_CaseTypeCode type
        xml.ZD_Description ticket.description
        xml.ZD_StatusCode ticket.status
        xml.ZD_PriorityCode priority
        xml.ZD_Title ticket.subject
        add_standard_values(xml)
      end
    end

    builder.to_xml(save_with: Nokogiri::XML::Node::SaveOptions::AS_XML | Nokogiri::XML::Node::SaveOptions::NO_DECLARATION).strip
  end

  def add_standard_values(xml)
    xml.CrmInstallTypeCode type_code
    xml.IsCRMsecuredsite secure? ? "1" : "0"
    xml.UserID user_id
    xml.Password password
    xml.DiscoveryUri discovery_uri
    xml.OrganizationUri organization_uri

    xml.ServerAddress ""
    xml.CRMservername server_uri.host
    xml.PortNumber server_uri.port
    xml.Domainname domain_name
    xml.OrganizationName organization_name
  end

  def driver
    @driver ||= MsDynamics::Integration::MSCRMAdapterSoap.new(web_service_uri.to_s)
  end

  def web_service_uri
    uri = URI.parse(web_service_address)
    uri.path = "/MSCRMAdapter.asmx" unless uri.path.include?("MSCRMAdapter.asmx")
    uri
  end

  def server_uri
    URI.parse(server_address)
  end

  def secure?
    server_address.starts_with?("https")
  end

  def on_cloud?
    type_code == 0
  end

  def on_premise?
    type_code == 1
  end

  def ifd?
    type_code == 2
  end

  # delete this after 2012-12-01
  def organization_name
    if self.class.column_names.include?("organization_name")
      read_attribute(:organization_name)
    end
  end

  def parse_user_info(json)
    json = JSON.parse(json)

    {}.tap do |data|
      records = json["records"] || []
      records = [records] unless records.is_a?(Array)

      data[:records] = records.map do |elem|
        fields = elem["fields"] || []
        fields = [fields] unless fields.is_a?(Array)

        fields = fields.map do |field|
          {
            label: field["label"],
            value: field["value"]
          }
        end

        id = elem["id"]
        record_type = elem["record_type"]

        {
          id: id,
          url: user_url(elem["url"]),
          label: elem["label"],
          record_type: record_type,
          fields: fields
        }
      end
    end
  end

  def parse_ticket_response(json)
    json = JSON.parse(json.gsub("\n", "\\n"))
    code = json["code"] || "0"
    message = json["message"] || "No message received"
    if code == "1"
      message
    else
      raise "Error creating MSDynamics case in integration #{id}: #{message}"
    end
  end

  def user_url(received_url)
    url = server_address + received_url
    url.gsub("//main.aspx", "/main.aspx")
  end

  def destroy_other_integrations
    account.salesforce_production.try(:destroy)
    account.salesforce_sandbox.try(:destroy)
    account.sugar_crm_integration.try(:destroy)
  end

  def create_target
    unless account.targets.find_by_type("MsDynamicsTarget")
      target = MsDynamicsTarget.new(account: account, title: "Microsoft Dynamics Target", is_active: true)
      target.current_user = account.owner
      target.save
    end
  end

  def destroy_target
    account.targets.where(type: "MsDynamicsTarget").map(&:destroy)
  end
end
