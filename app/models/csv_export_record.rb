class CsvExportRecord < ActiveRecord::Base
  belongs_to :account
  belongs_to :user

  validates_presence_of :account_id, :user_id

  attr_accessible :user_id, :url

  def self.unexpired
    where("csv_export_records.created_at >= ?", 3.days.ago)
  end

  def self.uploaded
    where("csv_export_records.url IS NOT NULL")
  end

  def self.active
    where("csv_export_records.created_at >= ?", 2.weeks.ago)
  end

  def self.latest
    joins("LEFT JOIN csv_export_records r ON csv_export_records.account_id = r.account_id AND r.created_at > csv_export_records.created_at").
      where('r.id IS NULL')
  end

  def self.available_for?(account)
    account.subscription.has_report_feed?
  end

  def filename
    @filename ||= File.basename(URI.parse(url).path)
  end

  def content_type
    Mime[:zip]
  end
end
