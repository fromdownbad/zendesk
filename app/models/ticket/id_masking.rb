require 'hashids'

class Ticket
  def self.find_by_encoded_id(encoded_id, logger: Rails.logger)
    if decoded_id = IdMasking.unmask(encoded_id, logger: logger)
      find_by_id(decoded_id).tap do |found|
        logger.info("Searched for decoded ticket ID #{decoded_id} but could not find a match") if logger && !found
      end
    end
  end

  def encoded_id
    IdMasking.mask(id)
  end

  def generate_message_id(randomize = true)
    if randomize
      "<#{encoded_id.delete('-')}_#{Mail.random_tag}_sprut@#{Zendesk::Configuration.fetch(:host)}>"
    else
      "<#{encoded_id.delete('-')}@#{Zendesk::Configuration.fetch(:host)}>"
    end
  end

  module IdMasking
    # encoded ID v1
    ENCODING_KEY   = Zendesk::Configuration.fetch(:ticket_encoding_key)
    VALIDATION_KEY = Zendesk::Configuration.fetch(:ticket_validation_key)

    class << self
      def mask(id)
        IdMasking.hashids_encode(id)
      end

      def unmask(encoded_id, logger: Rails.logger)
        decoded_id = hashids_decode(encoded_id)
        logger.info("Failed to decode #{encoded_id} using hashids") if logger && !decoded_id
        decoded_id
      end

      protected

      def hashids_salt
        @hashids_salt ||= Zendesk::Configuration[:email_encoded_id_v2_salt]
      end

      def hashids
        return nil unless hashids_salt
        @hashids ||= Hashids.new(hashids_salt, 10, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
      end

      def hashids_encode(id)
        return nil unless hashids
        hashids.encode(id).insert(6, '-')
      end

      def hashids_decode(encoded_id)
        return nil unless hashids
        id_only = encoded_id.delete("-").upcase
        hashids.decode(id_only).first
      end
    end
  end
end
