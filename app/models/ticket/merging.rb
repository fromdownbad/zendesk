class Ticket < ActiveRecord::Base
  # ZD#176390 To do when merging ticket A into ticket B:
  # 1) If ticket A has an assigned Group/User - we will leave this as is.
  # 2) If not, then we will assign ticket A with the same Group/User as ticket B.
  # 3) If neither A or B has assigned Group/User; use the current behavior and assign current user and the top Group in the list to ticket A.
  def merge_into(target, current_user, comment, is_public)
    self.status_id = StatusType.CLOSED

    if assignee.nil?
      self.assignee = target.assignee || current_user
      if group.nil? || !group.includes_user_with_id?(assignee)
        if target.group && target.group.includes_user_with_id?(assignee)
          self.group = target.group
        else
          # If self.assignee is not in the ticket group, save will raise "assignee is not a member of the group"
          # Reset group and let validation methods infer the right group
          self.group = nil

          # If self.assignee has no groups save will also raise "assignee is not a member of the group"
          # Reset assignee and let validation methods infer the right agent
          if assignee.groups.empty?
            self.assignee = nil
          end
        end
      end
    end

    add_comment(body: comment, is_public: is_public, channel_back: false)
    add_ticket_merge_audit('source', user: current_user, target_id: target.id)
    add_domain_event(TicketMergedProtobufEncoder.new(self, target).to_object)
    self.additional_tags = 'closed_by_merge'
    add_merge_ticket_link(target)
    update_voice_ticket(target)

    valid?
  end

  def mark_as_merged(source_tickets, options = {})
    merge_collaborators(options)
    add_ticket_merge_audit('target', user: options[:user], source_tickets: source_tickets)
  end

  def merge_collaborators(options)
    if !account.has_email_ccs?
      # TODO: This needs to get changed back to options.fetch(:collaborator_ids) once
      # code in extract_collaborator_ids gets changed.
      self.force_collaboration = true
      self.set_collaborators = options.fetch(:follower_ids)
    else
      # Legacy_CCs
      if options[:collaborator_ids].any?
        self.force_collaboration = true
        self.set_collaborators = options.fetch(:collaborator_ids)
      end
      # Followers
      if options[:follower_ids].any?
        self.force_collaboration = true
        self.set_followers = user_ids_to_collaborators_hashes(options.fetch(:follower_ids))
      end
      # Email CCs
      if options[:email_cc_ids].any?
        self.force_collaboration = true
        self.set_email_ccs = user_ids_to_collaborators_hashes(options.fetch(:email_cc_ids))
      end
    end
  end

  def mark_with(source_tickets, target_comment, is_public)
    latest_public_source_comment = source_tickets.first.latest_public_comment
    if latest_public_source_comment.is_a?(VoiceComment)
      new_value_previous = latest_public_source_comment.value_previous.merge(merged: true)
      add_voice_merge_comment(latest_public_source_comment, body: target_comment, is_public: is_public, value_previous: new_value_previous)
    else
      add_comment(body: target_comment, is_public: is_public, channel_back: false)
    end
  end

  def add_ticket_merge_audit(merge_type, options = {})
    @current_user = options[:user]

    @audit = TicketMergeAudit.new(
      author: options[:user],
      via_id: ViaType.MERGE,
      merge_type: merge_type,
      account: account,
      client: client,
      ticket: self,
      disable_triggers: disable_triggers,
      target_id: options[:target_id]
    )

    @audit.value = options[:source_tickets].map(&:nice_id).join(',') if merge_type == 'target'
  end

  def add_merge_ticket_link(target)
    # Create new ticket_link object for this merged ticket.
    # These links will not be used until we are finished rewriting ticket sharing/linking
    target_links.build(source: self, target: target, link_type: TicketLink::MERGE)
  end

  def update_voice_ticket(target)
    if voice_ticket = Voice::Core::Ticket.find(id)
      return if voice_ticket.via_reference_id

      unless voice_ticket.update_attribute(:via_reference_id, target.id)
        Rails.logger.info("Updating ticket_id: #{voice_ticket.id} - could not update via_reference_id")
      end
    else
      Rails.logger.info("Unable to find Voice Ticket for Source: ticket_id: #{id}")
    end
  end
end
