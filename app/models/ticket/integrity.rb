# Ticket attribute integrity and inborn rules handling
# Remember: always check on "attribute_id" instead of "attribute" for associations, e.g. "group_id", as the association might be stale

class Ticket < ActiveRecord::Base
  after_commit :expire_ticket_cache

  TITLE_PLACEHOLDER = "{{ticket.title}}".freeze

  def set_nice_id
    return unless new_record?
    self.nice_id ||= (sanitized_custom_id_override || (account || @current_user.account).nice_id_sequence.next)
  end

  def sanitized_custom_id_override
    return nil unless custom_id_override.present?
    return nil unless @current_user
    a = account || @current_user.account
    return nil unless a
    return nil unless custom_id_override.to_i < a.nice_id_sequence.peek
    return nil if a.tickets.exists?(nice_id: custom_id_override.to_i)
    return nil if custom_id_override.to_i <= 0

    custom_id_override.to_i
  end

  def set_defaults
    @current_user = requester if !@current_user && requester
    return unless @current_user

    self.account         ||= @current_user.account
    self.submitter       ||= @current_user
    self.requester       ||= @current_user
    self.requester_id    ||= requester.id if requester # Due to new requester

    # So triggers will run - status_id etc. has db default of "0"
    status_id_delta_will_change!(from: StatusType.NEW)
    priority_id_delta_will_change!(from: 0)
    ticket_type_id_delta_will_change!(from: 0)

    self.via_id            = @current_via_id || ViaType.WEB_SERVICE
    self.ticket_type_id  ||= account.has_extended_ticket_types? ? 0 : TicketType.INCIDENT
    self.ticket_type_id    = TicketType.INCIDENT if ticket_type_id.to_i == 0 && !account.has_extended_ticket_types?
    self.status_id       ||= StatusType.NEW
    self.priority_id     ||= 0
    self.is_public         = true if is_public.nil?

    true
  end

  def set_checkboxes_from_current_tags
    tags = current_tags.to_s.split(' ')

    checkboxes = account.field_checkboxes.select do |f|
      f.is_active? && f.tag.present?
    end

    checkboxes.each do |checkbox|
      value = tags.member?(checkbox.tag) ? '1' : '0'
      # Normalize values in old/new comparison to avoid unnecessary audits.
      set_ticket_field_entry_value(checkbox, value, normalize_checkbox_value: true)
    end
  end

  # Used by trigger and automations actions
  def enforce_integrity_upon_attribute_change
    ensure_new_assignee_can_be_assigned
    set_assignee_if_account_has_only_one_agent
    set_group_if_account_has_only_one_group
    set_group_if_default_group_present
    set_assignee_upon_solve
    enforce_proper_assignee_group_relationship
    set_assignee_if_group_has_only_one_agent
    group_change_from_auto_assign

    # fix_organization
    fix_tags
    open_new_ticket_upon_assignment if assignee_id_delta_changed?
  end

  ####################################################################
  # Methods called before_save on ticket - i.e. after trigger handling
  ####################################################################

  def ensure_solved_at
    self.solved_at ||= Time.now.utc
  end

  def set_report_attributes
    return if !@audit || @audit.events.blank?

    update_time = Time.now
    self.requester_updated_at = update_time if @current_user.id == requester_id || new_record?
    self.assignee_updated_at = update_time if @current_user.id == assignee_id
    if @current_user.id != User.system_user_id
      self.updated_by_type_id = @current_user.is_agent? ? UpdatedByType.Agent : UpdatedByType.END_USER
    end

    # Status date attributes
    if status_id != status_id_was || new_record?
      self.status_updated_at = update_time

      is_closed_or_solved    = [StatusType.SOLVED, StatusType.CLOSED].include?(status_id)
      was_closed_or_solved   = Zendesk::Types::StatusType::FINISHED_LIST.include?(status_id_was.to_i)

      if is_closed_or_solved && !was_closed_or_solved
        self.resolution_time =
          if created_at && created_at < solved_at
            ((solved_at - created_at) / 1.hour).round
          else
            0
          end
      end

      if [StatusType.NEW, StatusType.OPEN, StatusType.PENDING, StatusType.HOLD].include?(status_id)
        self.solved_at = self.resolution_time = nil
      end
    end

    # Assignee date attributes
    if assignee_id && assignee_id != assignee_id_was
      self.assigned_at = update_time
      self.initially_assigned_at ||= assigned_at
    end

    if requester_id && requester_id != requester_id_was
      self.locale_id = requester.locale_id
    end

    if comment_added?
      update_comments_fields(update_time)
    end

    true
  end

  def update_comments_fields(update_time)
    self.latest_comment_added_at = update_time
    self.latest_public_comment_added_at = update_time if comment.is_public?
    # Any non-end-user role is considered as an admin.
    self.latest_agent_comment_added_at = update_time unless comment.author.is_end_user?
  end

  def cleanup_attributes
    resolve_title_placeholder if subject.present? && subject_changed?

    # Fix stale ticket type properties
    self.ticket_type_id = TicketType.INCIDENT if ticket_type_id_changed? && !account.has_extended_ticket_types?
    self.linked_id = nil if !ticket_type?(:incident) || linked_id == id

    true
  end

  def set_assignee_upon_solve
    return unless account.present? && account.settings.assign_tickets_upon_solve
    will_be_saved_by(@current_user) unless @audit

    if auto_assignable?
      @auto_assigned = true
      return if audit_has_previous_assign_upon_solve_event?('assignee_id')
      self.assignee = @current_user
      if @current_via_id == ViaType.RULE
        audit.events << Change.new(via_id: ViaType.ADMIN_SETTING, via_reference_id: setting_id('assign_tickets_upon_solve'), field_name: "assignee_id", value_previous: nil, value: assignee_id)
        delete_assign_upon_solve_delta_change('assignee_id')
      end
    end
  end

  def set_next_best_assignee(current_user = nil) # rubocop:disable Naming/AccessorMethodName
    current_user ||= @current_user
    # Assign it to the current admin if they are in the ticket's group
    self.assignee = if !(account.settings.assign_tickets_upon_solve && status?(:solved))
      nil
    elsif current_user.groups.include?(group)
      current_user
    # Assign it to the first agent in the group if possible
    elsif (assignee = first_assignable_agent_in_group)
      assignee
    # Otherwise, assign it to the acting admin in their default group
    else
      self.group_id = current_user.default_group_id
      current_user
    end
  end

  # This is gross and wack, but it is temporary code for testing metadata scrubbing in staging ONLY
  # and should never be used in production. It will be removed once testing is completed
  #
  # In dev/staging if the delete_ticket_metadata_pii account setting is enabled AND
  # we have the arturo for testing enabled (test_metadata_scrub) AND
  # the status is changed for the ticket, then we'll wipe the ticket metadata. Because testing...
  # We will not wipe ticket metadata when deleting a ticket in staging; because we can't delete pii for a ticket that doesn't exist (ticket will be nil)
  def metadata_scrub_testing?
    account.has_delete_ticket_metadata_pii_enabled? && account.has_test_metadata_scrub? && previous_changes.include?(:status_id) && status_id != StatusType.DELETED
  end

  private

  # these functions are listed in the order that they are run.
  #
  def set_assignee_if_account_has_only_one_agent
    return if account && assignee_can_be_assigned?

    # The default scope for agents orders the results by name (app/models/users/roles.rb:8).
    # Remove the order since it doesn't matter here and has caused the query planner to use the wrong index.
    if account.present? && account.agents.count(:all) == 1 && account.agents(:reload).unscope(:order).first.memberships.active(account_id).count(:all) > 0
      self.assignee = account.agents.unscope(:order).first
    end
  end

  def set_group_if_account_has_only_one_group
    return if account && group
    if account.present? && account.groups.count(:all) == 1
      self.group = account.groups.first
    end
  end

  def set_group_if_default_group_present
    if !group_id && reload_assignee
      self.group_id = assignee.default_group_id
    end
  end

  # * If requester has changed and current organization is not one of the requester organizations, let's fix and set the organization of the new requester
  # * If requester is a new record, let's fix and set the organization for the requester and the ticket
  # * If the ticket has no organization and the requester has one, let's fix and set the requester's organization
  def fix_organization
    return unless fix_organization_necessary?
    if account.has_invalid_organization_logging?
      Rails.logger.info(
        "invalid_organization: ticket.fix_organizations: "\
        "requester_has_changed?: #{requester_has_changed?}, "\
        "valid_organization?: #{valid_organization?}, "\
        "requester_is_new?: #{requester_is_new?}, "\
        "no_organization_and_requester_has_one?: #{no_organization_and_requester_has_one?}, "\
        "ticket_id: #{id}, "\
        "current_user: #{@current_user&.id}, "\
        "requester_id: #{requester_id}, "\
        "requester.organization_id: #{requester&.organization_id}, "\
        "organization_id: #{organization_id}, "\
        "requester.organization_memberships ids: #{requester&.organization_memberships&.map(&:organization_id)}"
      )
    end

    # Set organization and group, if requester belongs to an organization

    # Requester could be a new record created via set_organization
    if requester.new_record?
      new_organization = requester.set_default_organization

      # normally tags would be added during set_requester_tags, except in this case where the organization had not yet been set
      self.additional_tags = requester.ticket_tags(new_organization)
    else
      requester.reload if requester_id != requester.id
      new_organization = requester.organization
    end

    self.organization = if new_organization.present?
      new_organization
    end

    true
  end

  def set_organization_group
    # Handle the case where an account has only one agent and org has a mapped group. See ZD203299
    if (new_record? && organization_id && organization.group) && ((group_id.nil? && assignee_id.nil?) || (account.present? && account.agents.count(:all) == 1))
      self.group = organization.group
    end
  end

  def fix_organization_necessary?
    (requester_has_changed? && !valid_organization?) || requester_is_new? || no_organization_and_requester_has_one?
  end

  def requester_has_changed?
    requester_id_delta_changed? && requester.present?
  end

  def new_ticket_with_organization?
    new_record? && organization.present?
  end

  def requester_is_new?
    requester_id.blank? && requester.try(:new_record?)
  end

  def no_organization_and_requester_has_one?
    return if account && account.has_unset_ticket_org_when_removing_default_user_org?
    organization.nil? && requester.try(:organization)
  end

  def set_assignee_if_group_has_only_one_agent
    return if @current_user && @current_user.is_light_agent? && (@current_user.id != requester_id)

    if account.present? && !assignee_id && single_agent = single_assignable_agent_in_group
      self.assignee = single_agent
    end
  end

  def enforce_proper_assignee_group_relationship
    if assignee_id.present?
      if !assignee_can_be_assigned? && group_id? && !closed?
        # we've got an invalid agent.
        @allow_assignee_reset_within_group = true
        self.assignee = nil
      elsif group_id.blank? || !reload_group
        infer_group_from_assignee
      elsif !group.includes_user_with_id?(assignee_id)
        # here we've reached a case where the assignee and groups don't match.
        # depending on what the user intended let's either unset the asignee or fix the group
        if group_id_delta_changed?
          self.assignee = nil
        else
          self.group = infer_group_from_assignee
        end
      end
    end
  end

  def group_change_from_auto_assign
    if @auto_assigned && @current_via_id == ViaType.RULE && delta_changes.key?('group_id') && !audit_has_previous_assign_upon_solve_event?('group_id')
      audit.events << Change.new(via_id: ViaType.ADMIN_SETTING, via_reference_id: setting_id('assign_tickets_upon_solve'), transaction: (delta_changes.assoc 'group_id'))
      delete_assign_upon_solve_delta_change('group_id')
    end
  end

  def fix_tags
    return unless current_tags_delta_changed?
    tags = current_tags.to_s.split(' ')

    taggers = if account.has_cache_custom_field_option_values?
      account.field_taggers.active
    else
      account.fetch_active_taggers
    end

    taggers.each do |tagger|
      tags_from_field_tagger = if account.has_cache_custom_field_option_values?
        tags & tagger.custom_field_option_values
      else
        tags & tagger.custom_field_options.map(&:value)
      end

      if tagger.is_a?(FieldMultiselect)
        value = tags_from_field_tagger.sort.join(' ')
      else
        # A ticket can only be tagged with a field tagger tag once. Remove all but the last.
        tags -= tags_from_field_tagger[0..-2] if tags_from_field_tagger.size > 1

        value = tags_from_field_tagger.last.to_s
      end

      Zendesk::DB::RaceGuard.guarded do
        set_ticket_field_entry_value(tagger, value)
      end
    end

    self.set_tags = tags.sort.join(' ')

    set_checkboxes_from_current_tags

    true
  end

  public :fix_tags

  def fix_due_date
    return true unless due_date.present?

    if ticket_type_id != TicketType.TASK
      Rails.logger.warn("zd667786: removing due date #{due_date} for ticket #{id} on #{account.id}")
      self.due_date = nil

      return true
    end

    return true unless due_date_changed?

    if account.has_fix_ticket_due_date_v2?
      # The due date should be the DATE the user indicated, but the TIME should be noon in the
      # timezone of the account.  See HD-919.
      range = Time.use_zone(@current_user.try(:time_zone) || account.time_zone) do
        Range.new(
          due_date.in_time_zone.beginning_of_day,
          due_date.in_time_zone.beginning_of_day.tomorrow
        )
      end
      Time.use_zone(account.time_zone) do
        self.due_date = [due_date.yesterday, due_date, due_date.tomorrow].
          map { |date| Time.zone.local(date.year, date.month, date.mday, 12) }.
          find { |candidate| range.cover?(candidate) }
      end
    else
      self.due_date += 12.hours if due_date.hour.zero?
    end

    true
  end

  def open_new_ticket_upon_assignment
    # return if !assignee_id_delta_changed?
    self.status_id = StatusType.OPEN if assignee_id && status?(:new)
    true
  end

  def open_working_ticket_upon_end_user_update
    return true unless comment && (comment.present? || comment.attachments.any?)

    end_user = @current_user.is_end_user? ||
      (Arturo.feature_enabled_for?(:agent_as_end_user, account) &&
        agent_as_end_user_for?(@current_user))

    # End-user operations always result in an open ticket, if ticket is not new AND user has NOT set ticket to solved himself
    # But do not set to open if the user is a ticket-sharing agent (i.e. foreign_agent)
    self.status_id = StatusType.OPEN if end_user &&
      !@current_user.foreign_agent? &&
      allows_comment_visibility_toggle? &&
      !status?(:new) &&
      !@force_status_change ||= false
    true
  end

  def assignee_can_be_assigned?
    a = if account.has_skip_assignee_reload?
      assignee
    else
      reload_assignee
    end

    a && a.can?(:be_assigned_to, self.class)
  end

  def ensure_new_assignee_can_be_assigned
    if assignee_id && assignee_id_changed? && !assignee_can_be_assigned?
      self.assignee_id = assignee_id_was
    end
  end

  def infer_group_from_assignee
    if reload_organization && organization.group && organization.group.includes_user_with_id?(assignee_id)
      self.group = organization.group # For one agent, multiple groups accounts, make sure that the org group is set, if the agent is member of the org group
    elsif assignee && assignee.groups.present?
      default_group = assignee.groups.find_by_id(assignee.default_group_id)
      self.group = default_group || assignee.groups.first
    end
  end

  def expire_ticket_cache
    # ZD:211015 if we have created/updated a ticket to be a problem or explicitly not to be a problem, then we must bust the cache
    #  so that this ticket can now be included/unincluded in the list of problems for incidents to be linked to.
    if (previous_changes["ticket_type_id"] || []).include?(TicketType.PROBLEM)
      account.expire_scoped_cache_key(:ticket_fields)
    end
    true
  end

  def assignable_agents_in_group(agent_limit)
    return [] unless group(true)

    if account.has_permission_sets?
      group.users.assignable_agents.except(:order).active.limit(agent_limit).to_a
    elsif account.has_chat_permission_set?
      group.users.agents.except(:order).where.not(users: {id: group.users.chat_agents.except(:order).active}).active.to_a
    else
      group.users.agents.except(:order).active.limit(agent_limit).to_a
    end
  end

  def first_assignable_agent_in_group
    ActiveRecord::Base.on_slave do
      assignable_agents_in_group(1).first
    end
  end

  def single_assignable_agent_in_group
    ActiveRecord::Base.on_slave do
      agents = assignable_agents_in_group(2)
      return nil unless agents.size == 1
      agents[0]
    end
  end

  def check_ticket_form_id
    unless account.ticket_forms.find_by_id(ticket_form_id)
      self.ticket_form_id = account.ticket_forms.default.first.try(:id)
    end
  end

  def check_brand_id
    unless account.brands.active.find_by_id(brand_id)
      errors.add(:brand, :invalid)
      false
    end
  end

  def set_default_brand
    self.brand_id = account.try(:default_brand_id)
  end

  def set_ticket_form
    self.ticket_form_id = account.fallback_ticket_form(brand).try(:id) if account
  end

  def resolve_title_placeholder
    return if !subject_was || subject_was.try(:include?, TITLE_PLACEHOLDER) || !subject.include?(TITLE_PLACEHOLDER)
    self.subject = subject.gsub(TITLE_PLACEHOLDER, subject_was)
  end

  def check_safe_update_timestamp
    # CT-3310 Skip safe update check for whatsapp tickets
    # due to the message exchange format from whatsapp the updated_stamp
    # field can be inconsistent with the last sent/received message
    # TODO: This is a temporary fix
    # see: https://zendesk.atlassian.net/browse/CT-3310
    return if skip_whatsapp_ticket_check?

    last_updated = account.tickets.where(id: id).lock(true).pluck(:updated_at).first

    # last_updated can be nil here if we try to update an archived ticket.
    # In this case, we consider that an unsafe update.
    if last_updated.nil? || safe_update_timestamp < last_updated
      Rails.logger.warn("check_safe_update_timestamp failed, #{safe_update_timestamp} != #{last_updated}")
      errors.add :safe_update_timestamp, :invalid
    end
  end

  def setting_id(setting)
    account.settings.where(name: setting).first.id
  end

  def auto_assignable?
    status?(:solved) &&
    (status_id_was != StatusType.CLOSED) &&
    @current_user.can?(:be_assigned_to, self.class) &&
    assignee.nil?
  end

  def group_id_is_valid
    if account.groups.where(id: group_id).blank?
      Rails.logger.warn("validate_group_id_is_valid failed")
      errors.add :group_id, :invalid

      false
    end
  end

  def audit_has_previous_assign_upon_solve_event?(value_reference)
    audit.events.any? { |e| e.type == "Change" && e.via_reference_id == setting_id('assign_tickets_upon_solve') && e.value_reference == value_reference }
  end

  def delete_assign_upon_solve_delta_change(value_reference)
    delta_changed_attributes.delete(value_reference)
  end

  def skip_whatsapp_ticket_check?
    return false unless Arturo.feature_enabled_for?(:skip_safe_update_check, account)

    # check whatsapp ticket tag
    current_tags.to_s.split(' ').include?('whatsapp_support')
  end
end
