class Ticket < ActiveRecord::Base
  before_save :update_score

  def update_score
    self.base_score = calculated_score if changed?
  end

  def calculated_score
    status_score * assignee_score * priority_score * type_score * age_score
  end

  def score
    base_score.to_i
  end

  def assignee_score
    return 1 if assignee_id
    value = (group_id ? 2 : 3)
    value
  end

  def status_score
    return 0 if status_id.nil? || status_id == StatusType.CLOSED
    value = StatusType.score(status_id)
    value
  end

  def priority_score
    return 1 unless priority_id
    value = PriorityType.score(priority_id)
    value
  end

  def type_score
    return 1 unless ticket_type_id
    value = TicketType.score(ticket_type_id)
    value
  end

  def age_score
    return 1 unless created_at
    created_hours_ago = ((Time.now.utc - created_at) / 3600).floor

    value = if created_hours_ago < 2
      1
    elsif created_hours_ago < 25
      2
    elsif created_hours_ago < 121
      3
    else
      5
    end

    value
  end
end
