class Ticket < ActiveRecord::Base
  has_many :satisfaction_rating_comments,
    -> { readonly.where(value_reference: 'satisfaction_comment').order(created_at: :desc, id: :desc) },
    class_name: 'Change'
  has_many :satisfaction_rating_scores,
    -> { readonly.where(value_reference: 'satisfaction_score').order(created_at: :desc, id: :desc) },
    class_name: 'Change'
  has_many :satisfaction_ratings, class_name: "Satisfaction::Rating", dependent: :destroy, inherit: :account_id
  has_one  :satisfaction_rating,
    -> { readonly.order(created_at: :desc, id: :desc) },
    class_name: 'Satisfaction::Rating',
    inherit: :account_id,
    inverse_of: :ticket

  before_save :alter_satisfaction_score_if_comment
  before_save :set_default_reason_code
  after_save :record_rating_statistic
  after_save :consume_csr_token
  after_save :set_satisfaction_rating_status

  validates_inclusion_of :satisfaction_score, in: SatisfactionType.fields

  def can_rate_satisfaction?(user = current_user)
    unless user.nil?
      user.can?(:rate_satisfaction, self)
    end
  end

  def latest_satisfaction_rating(as_viewed_by = current_user)
    {
      :score         => satisfaction_score,
      :comment       => satisfaction_comment,
      'can_modify?'  => can_rate_satisfaction?(as_viewed_by),
      :assignee_id   => assignee_id
    }
  end

  def satisfaction_comment
    @satisfaction_comment ||= satisfaction_comment_event.try(:value)
  end

  def satisfaction_comment_event
    scope = satisfaction_rating_comments
    scope = scope.use_index(:index_events_on_ticket_id_and_type) unless satisfaction_rating_comments.loaded? || archived?
    scope.first
  end

  def satisfaction_comment=(new_comment)
    return if (new_comment.nil? || new_comment.empty?) && satisfaction_comment.nil?
    if new_comment != satisfaction_comment
      satisfaction_comment_delta_will_change!
      @satisfaction_comment_change = true
    end
    @satisfaction_comment = new_comment
  end

  def satisfaction_reason
    Satisfaction::Reason.with_deleted do
      account.satisfaction_reasons.find_by_reason_code(satisfaction_reason_code)
    end
  end

  def apply_satisfaction_reason_code(code)
    self.satisfaction_reason_code = code
  end

  def record_rating_statistic
    if satisfaction_changed?
      @satisfaction_comment = nil
      account.satisfaction_ratings.create!(
        ticket: self, score: satisfaction_score,
        reason_code: satisfaction_reason_code,
        comment: satisfaction_comment_event,
        event: satisfaction_rating_scores.first, agent: assignee,
        enduser: requester, group: group, status_id: status_id
      )
    end
  end

  def consume_csr_token
    return if account.has_csr_token_retention?
    if satisfaction_changed? && [SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT, SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT].include?(satisfaction_score)
      if csr_token = Tokens::SatisfactionRatingToken.find_by_source_type_and_source_id('Ticket', id)
        Rails.logger.info("CSR: Someone is destroying token: #{csr_token.value}")
        csr_token.destroy
      end
    end
  end

  def alter_satisfaction_score_if_comment
    if @satisfaction_comment.present?
      if satisfaction_score == SatisfactionType.GOOD
        self.satisfaction_score = SatisfactionType.GOODWITHCOMMENT
      elsif satisfaction_score == SatisfactionType.BAD
        self.satisfaction_score = SatisfactionType.BADWITHCOMMENT
      end
    end
  end

  def satisfaction_changed?
    @satisfaction_changed ||= (satisfaction_score_changed? || satisfaction_reason_code_changed? || @satisfaction_comment_change)
  end

  def set_satisfaction_rating_status
    return unless status_id_changed?

    satisfaction_ratings.update_all(status_id: status_id)
  end

  private

  def set_default_reason_code
    if [SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT].include?(satisfaction_score)
      self.satisfaction_reason_code = Satisfaction::Reason::NONE
    else
      self.satisfaction_reason_code ||= Satisfaction::Reason::NONE
    end
  end
end
