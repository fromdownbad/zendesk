class Ticket < ActiveRecord::Base
  after_commit :reshare_ticket_if_necessary, on: :update

  protected

  def satisfaction_probability_update?(interesting_changes)
    if interesting_changes.keys == ["updated_at"]
      previous_events = audits.latest.events
      previous_events.count == 1 && previous_events.first.value_reference == 'satisfaction_probability'
    else
      false
    end
  end

  def reshare_ticket_if_necessary
    # Set fields that do not sync over if a ticket is shared
    unshared_fields = ["priority_id", "group_id", "assignee_id", "ticket_type_id", "organization_id"]
    interesting_changes = previous_changes.except(*unshared_fields)

    if shared_tickets.empty?
      return
    elsif interesting_changes.empty? || satisfaction_probability_update?(interesting_changes)
      Rails.logger.warn("Aborting reshare of #{id} due to lack of interesting changes.")
      return
    elsif deleted?
      Rails.logger.warn("Aborting reshare of #{id} due to ticket being deleted.")
      return
    elsif skip_reshare_ticket_if_necessary
      Rails.logger.warn("Aborting reshare of #{id} due to manual override")
      return
    end

    Zendesk::DB::RaceGuard.cache_guarded("ticket-reshare-status/#{account_id}/#{id}/#{events.last.id}") do
      shared_tickets.each do |shared_ticket|
        if shared_ticket.deleted_at.present?
          Rails.logger.warn("Skipping SharedTicket #{shared_ticket.id} due to it having been deleted at #{shared_ticket.deleted_at}.")
          next
        elsif shared_ticket.agreement.blank?
          Rails.logger.warn("Skipping SharedTicket #{shared_ticket.id} due to its agreement being blank.")
          next
        elsif shared_ticket.agreement.status == :declined
          Rails.logger.warn("Skipping SharedTicket #{shared_ticket.id} due to its agreement being declined.")
          next
        elsif shared_ticket.agreement.status == :inactive
          Rails.logger.warn("Skipping SharedTicket #{shared_ticket.id} due to its agreement being inactive.")
          next
        elsif audits.latest.via_id == ViaType.TICKET_SHARING && audits.latest.via_reference_id == shared_ticket.agreement.id
          Rails.logger.warn("Skipping SharedTicket #{shared_ticket.id} due to last audit being via TICKET_SHARING from agreement #{audits.latest.via_reference_id}.")
          next
        end

        shared_ticket.enqueue_job(:update_partner)
      end
    end
  end
end
