class Ticket < ActiveRecord::Base
  has_many :twitter_actions
  has_many :event_decorations

  scope :via_twitter, -> { where(via_id: [ViaType.TWITTER_FAVORITE, ViaType.TWITTER, ViaType.TWITTER_DM]) }

  # trigger twitter conditions start
  def requester_twitter_followers_count
    requester.twitter_profile.try(:followers_count).to_i
  end

  def requester_twitter_statuses_count
    requester.twitter_profile.try(:statuses_count).to_i
  end

  def requester_twitter_verified
    requester.twitter_profile.try(:verified) ? 'true' : nil
  end

  def create_twitter_action(action, comment_id, agent, requester)
    will_be_saved_by(agent)
    return false unless mth = twitter_ticket_source(action.to_s)
    @audit.events << TwitterAction.new(
      ticket: self,
      user_info: {
        monitored_handle_id: mth.id,
        monitored_handle_name: mth.twitter_screen_name,
        requester_handle_name: requester.twitter_profile.screen_name
      },
      comment_id: comment_id,
      action: action.to_s
    )
    save!
    event = twitter_actions.where(value_reference: comment_id, value: action.to_s).last.audit.events.last
    twitter_action_status_message(event)
  end

  def twicket_url
    base_url = (account.has_multiple_active_brands? && brand.present?) ? brand.url : account.url

    "#{base_url}/twickets/#{nice_id}"
  end

  private

  def twitter_update?
    via_twitter? && audit.via_twitter?
  end

  def twitter_action_status_message(event)
    if event.is_a?(::Error)
      { status: :error, message: event.value }
    else
      { status: :ok }
    end
  end
end
