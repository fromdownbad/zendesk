require 'sharing/agreements_presenter'

class Ticket
  class Related
    ## DEPRECATED: This is replaced by Tickets::RelatedPresenter
    ##             Remove after v2 migration is complete.

    def initialize(ticket)
      @ticket = ticket
    end

    def as_json(options = {})
      json = {}

      json[:topic]           = @ticket.entry_id
      json[:jiraIssues]      = @ticket.jira_issues.map(&:issue_id)
      json[:followupSources] = @ticket.followup_sources.all_with_archived.map(&:nice_id)
      json[:followupIds]     = @ticket.followups_with_archived.map(&:nice_id)
      json[:fromArchive]     = @ticket.archived?
      json[:satisfaction]    = { score: @ticket.satisfaction_score, comment: @ticket.satisfaction_comment }

      add_sharing_agreements(json)
      add_twitter_information(json)
      add_facebook_information(json)

      json.as_json(options)
    end

    private

    def add_sharing_agreements(json)
      agreements = @ticket.shared_tickets.map(&:agreement).compact

      json.merge!(shared: Sharing::AgreementsPresenter.new(agreements).agreements_as_json)
    end

    def add_twitter_information(json)
      json[:twitter] = nil

      return unless @ticket.via_twitter?
      return unless @ticket.twitter_ticket_source.present?
      return unless @ticket.twitter_target

      handle  = @ticket.twitter_ticket_source
      profile = handle.twitter_profile

      json[:twitter] = {
        handle: { id: handle.id },
        profile: profile.as_json,
        direct: @ticket.via?(:twitter_dm),
        parent: @ticket.twitter_target.nice_id
      }
    end

    def add_facebook_information(json)
      json[:facebook] = nil

      return unless @ticket.via?(:facebook_post)
      return unless @ticket.facebook_target

      json[:facebook] = {
        parent: @ticket.facebook_target.nice_id
      }
    end
  end
end
