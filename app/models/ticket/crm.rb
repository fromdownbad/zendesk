class Ticket < ActiveRecord::Base
  has_one :salesforce_ticket_data

  def crm_data(create = false)
    account.crm_integration.crm_ticket_data(self, create) if account.crm_integration_enabled?
  end

  def needs_crm_sync?
    crm_data.nil? || crm_data.needs_sync?
  end
end
