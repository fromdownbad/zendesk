class Ticket < ActiveRecord::Base
  def add_flags!(flag_types, flags_options = {})
    add_audit_flags!(flag_types, flags_options)
  end

  def flags_allow_comment?
    new_audit_flags.allowed_to_comment? || comment.try(:current_audit).try(:flags).try(:allowed_to_comment?)
  end

  def trusted?
    # [2015.04.17 rmartz] It's not clear what Ticket#trusted? would even mean but it seems like it should take
    # into account all possible places where flags might live in the Ticket creation process or life cycle.
    return false if audit && !audit.trusted?
    return false if @new_audit_flags && !@new_audit_flags.trusted?
    comments.all?(&:trusted?)
  end

  private

  def new_audit_flags
    @new_audit_flags ||= AuditFlags.new
  end

  def audit_flags
    audit ? audit.flags : new_audit_flags
  end

  def audit_flags_options
    audit ? audit.flags_options : new_audit_flags_options
  end

  def new_audit_flags_options
    new_audit_flags.options
  end

  def add_audit_flags!(flag_types, flags_options = {})
    return if flag_types.all? { |f| audit_flags.include?(f) }

    audit_flags.merge_flags(flag_types, flags_options)
    audit.try(:save_flags!)
  end
end
