class Ticket::Bookmark < ActiveRecord::Base
  include Zendesk::Serialization::BookmarkSerialization
  extend ZendeskArchive::AssociationLookaside

  belongs_to :ticket, inherit: :account_id
  association_accesses_archive :ticket

  belongs_to :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  belongs_to :account

  attr_accessible :user, :ticket

  validates_presence_of   :ticket, :user, :account_id
  validates_uniqueness_of :ticket_id, scope: :user_id
  before_validation       :ensure_account

  def visible_to_user?
    user.can?(:view, ticket)
  end

  private

  def ensure_account
    self.account ||= user.account
  end
end
