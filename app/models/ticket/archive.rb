require 'zendesk_archive'

class Ticket
  include ZendeskArchive::Archivable
  extend  ZendeskArchive::AssociationLookaside

  configure_archive stub_class: TicketArchiveStub, archive_associations: [:events, :ticket_field_entries, :taggings]
  has_one :ticket_archive_stub, foreign_key: :id
  has_one :ticket_archive_disqualification, foreign_key: :id, dependent: :destroy

  association_accesses_archive(:problem)

  event_is_of_type = lambda do |event, type|
    const = event['type'].constantize
    const == type || const < type
  end

  archived_association_scope(:audits,   'events') { |events| events.select { |e| event_is_of_type.call(e, Audit) } }
  archived_association_scope(:comments, 'events') { |events| events.select { |e| event_is_of_type.call(e, Comment) } }
  archived_association_scope(:public_comments, 'events') { |events| events.select { |e| event_is_of_type.call(e, Comment) && e['is_public'] == 1 } }

  archived_association_scope(:conversation_items, 'events') { |events| events.select { |e| AuditEvent::CONVERSATION_EVENTS.include?(e['type']) } }

  archived_association_scope(:satisfaction_rating_comments, 'events') do |events|
    events.select { |e| e['type'] == 'Change' && e['value_reference'] == 'satisfaction_comment' }
  end

  archived_association_scope(:satisfaction_rating_scores, 'events') do |events|
    events.select { |e| e['type'] == 'Change' && e['value_reference'] == 'satisfaction_score' }
  end

  archived_association_scope(:first_comment, 'events') { |events| events.detect { |e| event_is_of_type.call(e, Comment) } }
  archived_association_scope(:latest_comment, 'events') { |events| events.reverse.detect { |e| event_is_of_type.call(e, Comment) } }
end
