# Code related to ticket field entries in the context of a ticket
class Ticket
  module TicketFieldEntries
    def self.included(base)
      base.after_commit :reset_ticket_field_entries_changed
    end

    def set_ticket_field_entry_value(ticket_field, value, options = {})
      field_entry = ticket_field_entries.detect { |tfe| tfe.ticket_field_id == ticket_field.id }

      # Build a new entry only if there's a value to store
      if field_entry.nil? && value.present?
        field_entry = ticket_field_entries.build(account: account, ticket_field: ticket_field)
      end

      # We don't need to do anything if we couldn't find or build an entry
      return if field_entry.nil?

      # Check if the value needs to be updated
      return unless ticket_field_entry_needs_to_be_updated?(field_entry, ticket_field, value, options)

      # Register delta change, add domain event and set/update value
      delta_changed_attributes[ticket_field.id.to_s] = field_entry.value
      @ticket_field_entries_changed = true
      add_domain_event(CustomFieldChangedProtobufEncoder.new(ticket_field, field_entry.value, value).to_safe_object) if account.has_publish_tfe_domain_events?
      field_entry.value = value
      field_entry.redact_credit_card_numbers!
    end

    def ticket_field_entries_changed?
      !!@ticket_field_entries_changed
    end

    private

    # Checkbox:
    #   When we create tickets, we always create an entry with value == '0'.
    #   We only create a Change audit when we send a '0' value for the field. i.e. Lotus.
    #   This is inconsistent behavior and can be revised/changed later after analyzing potential consequences.
    #   Sometimes we need to be more strict in the comparison to avoid undesired audits,
    #   `normalize_checkbox_value` option will normalize values before comparing.
    # Multiselect:
    #   `value` is parsed in FieldMultiselect#normalize_value_and_sync_tags
    #    which is the same code that previously parsed the value that is in the entry
    #    so the order of the option tags will be always the same and we can do a simple comparison here.
    # Dropdown:
    #   `value` is normalized in FieldTagger#normalize_value_and_sync_tags.
    #   We need a simple string comparison here to check old/new option tag.
    # Rest of the fields:
    #   Simple string comparison.
    def ticket_field_entry_needs_to_be_updated?(field_entry, ticket_field, value, options)
      if ticket_field.is_a?(FieldCheckbox) && options[:normalize_checkbox_value]
        ticket_field.normalize_value(value.to_s) != ticket_field.normalize_value(field_entry.value.to_s)
      else
        field_entry.value.to_s != value.to_s
      end
    end

    def reset_ticket_field_entries_changed
      @ticket_field_entries_changed = nil
    end
  end
end
