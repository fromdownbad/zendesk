class Ticket < ActiveRecord::Base
  def delete_attachments
    # Directly using ticket_id on Attachment.where does not have the required index
    comment_ids = comments.pluck(:id)
    attachments = Attachment.where(source_type: "Comment", source_id: comment_ids)
    attachments.each(&:remove_from_remote!)
  end

  def delete_raw_emails
    audits = self.audits.where(via_id: ViaType.MAIL)
    audits.each do |audit|
      raw_email = audit.raw_email

      next unless raw_email

      audit.delete_eml_file unless raw_email.eml_identifier.blank?
      audit.delete_json_files unless raw_email.json_identifier.blank?
      Ticket.with_deleted { audit.save! }
    end
  end
end
