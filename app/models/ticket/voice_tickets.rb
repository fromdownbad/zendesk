class Ticket < ActiveRecord::Base
  has_many :voice_comments, -> { order('events.created_at ASC, events.id ASC') }

  scope :via_voice, -> { where(via_id: [ViaType.VOICEMAIL, ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND]) }

  def via_voice?
    via?(:voicemail) || via?(:phone_call_inbound) || via?(:phone_call_outbound)
  end

  def via_phone?
    via?(:phone_call_inbound) || via?(:phone_call_outbound)
  end

  def linked_call
    return unless id
    call = account.calls.inbound.find_by_ticket_id(id)
    call || account.calls.outbound.find_by_ticket_id(id)
  end

  def via_zendesk_voice?
    via_voice? && linked_call
  end

  def add_voice_merge_comment(old_comment, options)
    options = {
      via_id: old_comment.via_id,
      data: old_comment.data.except(:via_id, :transcription_status),
      is_public: false,
      ticket: self,
      account_id: old_comment.account_id
    }.merge(options)

    build_voice_comment(options)
  end

  def add_voice_comment(call, options)
    options = {
      data: call.voice_comment_data,
      is_public: false,
      ticket: self,
      account_id: call.account_id
    }.merge(options)

    build_voice_comment(options)
  end

  def answering_number
    if via_zendesk_voice?
      linked_call.answering_number
    end
  end

  def calling_number
    if via_zendesk_voice?
      linked_call.calling_number
    end
  end

  protected

  def build_voice_comment(options)
    self.comment = VoiceComment.new(options)
    comment.valid?
    comment
  end
end
