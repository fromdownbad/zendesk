class Ticket < ActiveRecord::Base
  attr_accessor :suspended_ticket
  after_commit :destroy_suspended_ticket, on: :create

  def suspend(cause)
    account.suspended_tickets.new do |t|
      t.subject    = subject
      t.content    = description
      t.cause      = cause
      t.from_name  = requester.name
      t.from_mail  = requester.email
      t.via_id     = via_id
      t.author     = requester unless requester.new_record?
      t.client     = client
      t.uploads    = comment.try(:uploads)
      t.recipient  = recipient
      t.original_recipient_address = original_recipient_address
      t.properties = {}
      t.properties[:tags] = current_tags if current_tags.present?
      t.properties[:due_date] = due_date if due_date.present?
      t.properties[:brand_id] = brand_id if brand_id.present?
      t.properties[:via_reference_id] = via_reference_id if via_reference_id.present?
    end
  end

  protected

  def destroy_suspended_ticket
    suspended_ticket.destroy if suspended_ticket
  end
end
