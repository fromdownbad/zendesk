require 'zendesk_stats'

module TicketStatsObserver
  class << self
    def included(base)
      this = self
      base.after_create { |obj| this.store_create_event(obj) }
      base.after_update { |obj| this.store_update_event(obj) }
    end

    def store_create_event(obj)
      if obj.is_a?(Ticket)
        store_ticket_event('create', obj)

        if obj.audit && obj.audit.id && obj.audit.author.is_agent?
          store_ticket_event('audit', obj)
        end

        # grouponuk and possibly others like to create tickets with status == solved.
        if obj.status_id == StatusType.SOLVED
          store_ticket_event('solve', obj)
        end

      elsif obj.is_a?(Comment)
        store_comment_event(obj)
      end
    end

    def store_update_event(obj)
      if obj.is_a?(Ticket)
        if obj.assignee_id_changed? || obj.group_id_changed?
          store_ticket_event('assign', obj)
        end

        if obj.satisfaction_score_changed? && [SatisfactionType.GOOD, SatisfactionType.BAD, SatisfactionType.GOODWITHCOMMENT, SatisfactionType.BADWITHCOMMENT].include?(obj.satisfaction_score)
          store_ticket_event('csr', obj)
        end

        if obj.audit && obj.audit.id && obj.audit.author.is_agent?
          store_ticket_event('audit', obj)
        end

        if obj.status_id_changed? && obj.status_id == StatusType.SOLVED
          store_ticket_event('solve', obj)
        end
        if obj.status_id_changed? && obj.status_id == StatusType.CLOSED
          store_ticket_event('close', obj)
        end
      end
    end

    private

    def store_ticket_event(type, ticket)
      base = {
        ts: Time.now.to_i, ticket_id: ticket.id, account_id: ticket.account.id, via_id: ticket.via_id,
        account_subdomain: ticket.account.subdomain, type: type, shard_id: ticket.account.shard_id,
        ticket_status: ticket.status, nice_id: ticket.nice_id
      }
      case type
      when 'create'
        base.merge!(user_id: ticket.requester_id, group_id: ticket.group_id, organization_id: ticket.organization_id)
      when 'solve', 'assign', 'close'
        base[:agent_id] = ticket.assignee_id if ticket.assignee_id
        base[:group_id] = ticket.group_id
      when 'csr'
        base[:agent_id] = ticket.assignee_id if ticket.assignee_id
        base[:user_id] = ticket.requester_id
        base[:score] = ticket.satisfaction_score
      when 'audit'
        if comment = ticket.audit.events.detect { |event| event.is_a? Comment }
          if comment.author.is_agent?
            base[:agent_id] = comment.author_id
            base[:audit_id] = ticket.audit.id
          end
        else
          if ticket.audit.author
            base[:agent_id] = ticket.audit.author.id
            base[:audit_id] = ticket.audit.id
          end
        end
        base[:group_id] = ticket.group_id if ticket.group_id
        base[:organization_id] = ticket.organization_id if ticket.organization_id
        base[:satisfaction_probability_changed] = true if ticket.delta_changes.key?("satisfaction_probability")
      end

      Rails.logger.info("store_ticket_event: #{base.inspect}")
      Zendesk::ClassicStats::TicketEvent.store(base)
    end

    def store_comment_event(comment)
      return unless comment.is_public?

      return unless comment.ticket && comment.account && comment.ticket.account # this just guards against some silly tests.

      base = {
        ts: Time.now.to_i, ticket_id: comment.ticket.id, account_id: comment.account.id,
        account_subdomain: comment.ticket.account.subdomain, shard_id: comment.account.shard_id,
        via_id: comment.ticket.via_id, comment_via_id: comment.via_id, is_public: comment.is_public?,
        ticket_status: comment.ticket.status, nice_id: comment.ticket.nice_id
      }
      author = comment.author
      if author.is_agent?
        base[:agent_id] = comment.author.id
        base[:type] = 'agent_comment'
      else
        base[:user_id] = comment.author.id
        base[:type] = 'user_comment'
      end

      Rails.logger.info("store_comment_event: #{base.inspect}")
      Zendesk::ClassicStats::TicketEvent.store(base)
    end
  end
end
