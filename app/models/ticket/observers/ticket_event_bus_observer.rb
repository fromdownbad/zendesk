# Publishes ticket domain events to the event bus (Kafka) via "Escape"
module TicketEventBusObserver
  extend ActiveSupport::Concern

  EVENT_BUS_TOPIC = 'support.ticket_events'.freeze

  included do
    after_save :publish_ticket_events_to_bus!
    attr_writer :domain_event_publisher
  end

  # Allow for dependency injection of the event publishing mechanism.
  #
  # This construct exists to allow tests to read the published events, as the
  # BLACKHOLE engine of Escape table can not be easily queried.
  def domain_event_publisher
    @domain_event_publisher || EscKafkaMessage
  end

  def publish_ticket_events_to_bus!
    # Return early if the feature is off or we have no events to emit
    return unless account&.has_publish_ticket_events_to_bus? && ticket_events_encoder.any?

    event_bus_statsd_client.time('domain_event.publish_time') do
      messages = ticket_events_encoder.to_a.map do |event|
        {
          account_id: account_id,
          topic: EVENT_BUS_TOPIC,
          partition_key: "#{account_id}/#{nice_id}",
          value: event.to_proto
        }
      end
      # max_allowed_packet is 64MB. Events should not be over 1MB
      domain_event_publisher.bulk_import!(messages, batch_size: 64)

      record_stats
    end
  rescue StandardError => e
    # For now, we prefer silently dropping domain events over crashing Ticket save.
    Rails.logger.error(e)
    event_bus_statsd_client.increment('publish.errors', tags: ["exception:#{e.class}"])
    ZendeskExceptions::Logger.record(e, location: self, message: "TicketEventBusObserver: #{e.message}", fingerprint: 'a8da17eab153f898a7b92cfa5b2a1e3933d94043')
  end

  private

  def ticket_events_encoder
    @ticket_events_encoder ||= TicketEventsProtobufEncoder.new(self)
  end

  def record_stats
    event_bus_statsd_client.batch do |statsd|
      statsd.histogram('events_per_save', ticket_events_encoder.to_a.size)
      ticket_events_encoder.to_a.each do |event|
        # We may be able to remove this and just use the histogram count, but
        # we suspect histogram counts may not be reliable based on parity checks
        statsd.increment('domain_event', tags: ["event:#{event.event}"])
        statsd.histogram('domain_event.message_size', event.to_proto.size, tags: ["event:#{event.event}"])
      end
    end
  end

  def event_bus_statsd_client
    @event_bus_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['support', 'event_bus', 'ticket'], tags: ["protobuf_version:#{Gem.loaded_specs['zendesk_protobuf_clients'].version.version}"])
  end
end
