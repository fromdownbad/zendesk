require 'zendesk/sla'

module TicketSlaObserver
  extend ActiveSupport::Concern

  included { before_save :assess_sla }

  private

  def assess_sla
    return unless account.has_service_level_agreements?

    ZendeskAPM.trace(
      'ticket_metric.sla_policy_assignment',
      service: Zendesk::TicketMetric::APM_SERVICE_NAME
    ) do |span|
      span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
      span.set_tag('zendesk.account_id', account.id)
      span.set_tag('zendesk.account_subdomain', account.subdomain)
      span.set_tag('zendesk.ticket.id', id)

      Zendesk::Sla::PolicyAssignment.new(self).assign
    end
  end
end
