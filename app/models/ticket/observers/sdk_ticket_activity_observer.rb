module SdkTicketActivityObserver
  extend ActiveSupport::Concern

  included do
    after_commit on: :update do |ticket|
      send_sdk_push_notifications(ticket)
    end
  end

  def send_sdk_push_notifications(ticket)
    return unless mobile_sdk_update?(ticket)
    return unless ticket.account.has_sdk_manage_requests?
    return unless ticket.requester
    return unless ticket.requester.is_end_user?
    return unless ticket.audit
    return if ticket.deleted?
    mobile_devices = mobile_sdk_get_devices(ticket)
    return if mobile_devices.blank?

    ticket.audit.events(:reload).each do |event|
      if event.author_id != ticket.requester.id && mobile_sdk_event_public_comment?(event)
        mobile_sdk_send_push(ticket, event)
      end
    end
  end

  protected

  def mobile_sdk_update?(ticket)
    ticket.account.has_mobile_sdk_notify_non_sdk_updates? || ticket.via_id == ViaType.MOBILE_SDK
  end

  def mobile_sdk_send_push(ticket, event)
    mobile_sdk_get_devices(ticket).group_by(&:mobile_sdk_app).each do |mobile_sdk_app, devices_for_app|
      I18n.with_locale(ticket.requester.translation_locale) do
        PushNotificationSdkSendJob.enqueue(
          account_id: ticket.account.id,
          device_ids: devices_for_app.collect(&:id),
          mobile_sdk_app_id: mobile_sdk_app.id,
          payload: mobile_sdk_push_payload(event, mobile_sdk_app)
        )
      end
    end
  end

  def mobile_sdk_get_devices(ticket)
    if ticket.sdk_anonymous_device_identifiers.any?
      ticket.sdk_anonymous_device_identifiers
    elsif ticket.requester.device_identifiers.not_first_party.active.any?
      ticket.requester.device_identifiers.not_first_party.active
    end
  end

  def mobile_sdk_push_payload(event, _mobile_app)
    {
      'user_id' => event.ticket.requester.id,
      'msg' => I18n.t("txt.mobile_sdk.push_notifications.comment"),
      'msg_short' => I18n.t("txt.mobile_sdk.push_notifications.comment"),
      'ticket_id' => event.ticket.token || event.ticket.nice_id.to_s
    }
  end

  private

  def mobile_sdk_event_public_comment?(event)
    event.is_a?(Comment) && event.is_public
  end
end
