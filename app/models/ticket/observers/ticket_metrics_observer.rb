require 'zendesk/ticket_metric'

module TicketMetricsObserver
  extend ActiveSupport::Concern

  included do
    after_save :record_metric_events
    after_save :update_ticket_metrics
  end

  private

  def record_metric_events
    return if deleted?

    if account.has_instrument_ticket_save?
      ms = Benchmark.ms { record_metric_events_and_slas }
      rack_request_span&.set_tag('zendesk.ticket.metric_events.time', ms)
    else
      record_metric_events_and_slas
    end
  end

  def update_ticket_metrics
    return if deleted?

    ZendeskAPM.trace(
      'ticket_metric_sets.record_metric_sets',
      service: TicketMetricSet::APM_SERVICE_NAME
    ) do |span|
      span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
      span.set_tag('zendesk.account_id', account_id)
      span.set_tag('zendesk.account_subdomain', account.subdomain)
      span.set_tag('zendesk.ticket.id', id)

      if id_changed?
        TicketMetricSet.create(ticket: self, dirty_ticket: self)
      elsif !creating_shared_ticket?
        ticket_metric_set.present? && ticket_metric_set.update_metrics(self)
      end
    end
  end

  def creating_shared_ticket?
    audit && audit.ticket_sharing_create
  end

  def record_metric_events_and_slas
    if account.has_service_level_agreements?
      ZendeskAPM.trace(
        'ticket_metric.sla_policy_persistence',
        service: Zendesk::TicketMetric::APM_SERVICE_NAME
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.set_tag('zendesk.account_id', account.id)
        span.set_tag('zendesk.account_subdomain', account.subdomain)
        span.set_tag('zendesk.ticket.id', id)
        span.set_tag(
          'zendesk.sla.policy_id',
          sla_ticket_policy.try(:sla_policy_id)
        )

        Zendesk::Sla::PolicyAssignment.persist(self)
      end
    end

    if id_changed? || metric_events.any?
      ZendeskAPM.trace(
        'ticket_metric.evaluate',
        service: Zendesk::TicketMetric::APM_SERVICE_NAME
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.set_tag('zendesk.account_id', account_id)
        span.set_tag('zendesk.account_subdomain', account.subdomain)
        span.set_tag('zendesk.ticket.id', id)
        span.set_tag(
          'zendesk.sla.policy_id',
          sla_ticket_policy.try(:sla_policy_id)
        )

        Zendesk::TicketMetric::StateMachine.all.map do |state_machine|
          state_machine.build(self)
        end.each(&:advance)
      end
    end

    if account.has_service_level_agreements?
      ZendeskAPM.trace(
        'ticket_metric.cache_statuses',
        service: Zendesk::TicketMetric::APM_SERVICE_NAME
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.set_tag('zendesk.account_id', account_id)
        span.set_tag('zendesk.account_subdomain', account.subdomain)
        span.set_tag('zendesk.ticket.id', id)
        span.set_tag(
          'zendesk.sla.policy_id',
          sla_ticket_policy.try(:sla_policy_id)
        )

        sla_ticket_policy.try(:cache_statuses)

        sla.calculate
      end
    end
  end
end
