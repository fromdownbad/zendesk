module TicketActivityObserver
  class << self
    def included(base)
      this = self
      base.after_create { |ticket| this.create_activity(ticket, :create) }
      base.after_update { |ticket| this.update_or_delete_activity(ticket) }
    end

    def update_or_delete_activity(ticket)
      if ticket.deleted?
        delete_activity(ticket)
      else
        create_activity(ticket, :update)
      end
    end

    def create_activity(ticket, update_type)
      return unless ticket.audit

      users = [ticket.assignee]

      users.compact!
      users.uniq!

      return if users.empty?

      ticket.audit.events.reload.each do |event|
        users.each do |user|
          TicketActivity.create_activity_for(user, event: event, update_type: update_type)
        end
      end
    end

    private

    def delete_activity(ticket)
      TicketActivity.
        where(activity_target_type: "Ticket", activity_target_id: ticket.id).
        destroy_all
    end
  end
end
