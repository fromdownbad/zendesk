# Ticket validations
class Ticket < ActiveRecord::Base
  include ActionView::Helpers::TagHelper, TicketsHelper
  include ServiceableAccount

  before_validation :invalid_organization_additional_logging

  validate :validate_requester_is_not_suspended, on: :create
  validate :validate_followup, on: :create
  validate :validate_submitter
  validates_presence_of       :ticket_type_id
  validates_inclusion_of      :ticket_type_id, in: TicketType.fields, message: :invalid
  validate :organization_valid_for_requester, if: :organization_id_changed?
  # do not move up :validate_misc, it needs to run after association validations
  validate :validate_description, :check_end_user_required_fields_are_set_when_creating, on: :create
  validate :validate_ticket_form, if: :ticket_form_id_changed?
  validate :validate_misc_on_update, on: :update
  validate :validate_misc
  validate :check_agent_required_fields, if: :validate_agent_required_fields?

  validate :validate_deletion_rights, if: :deleting?

  # Used for checking valid trigger and automation attribute changes
  def has_invalid_attribute_change? # rubocop:disable Naming/PredicateName
    if requester_invalid?
      [:requester, :invalid]
    elsif group_invalid?
      [:group, :invalid]
    elsif assignee_invalid?
      [:assignee, :invalid]
    elsif assignee_without_permissions?
      [:assignee, :no_permissions]
    elsif priority_invalid?
      [:priority, :invalid]
    elsif priority_reset?
      [:priority, :reset]
    elsif type_reset?
      [:type, :reset]
    elsif status_invalid?
      [:status, :invalid]
    elsif status_reset?
      [:status, :reset]
    elsif status_was_changed_from_closed?
      [:status, :changed_from_closed]
    elsif brand_invalid?
      [:brand, :invalid]
    else
      false
    end
  end

  def attributes_changed?
    changed? || sharing_agreements_changed?
  end

  def sharing_agreements_changed?
    # ZD:295141 before Lotus the mere presence of agreement_id meant that the ticket had been newly shared
    # but Lotus will always send the agreement_id even when it is not changed, so verify that the agreement_id is not
    # present in an associated shared ticket
    @agreement_id.present? && !shared_tickets.any? { |st| @agreement_id.to_s == st.agreement_id.to_s }
  end

  #######################

  private

  def valid_organization?
    if account && account.has_skip_organization_validation?
      Rails.logger.info("Bypassing valid_organization? validation")
      return true
    end
    requester && requester.organization_memberships.map(&:organization_id).include?(organization_id)
  end

  def valid_user_organization?
    requester && requester.organization_memberships.map(&:organization_id).include?(requester.organization_id)
  end

  def email; end # just so that `errors.add :email` does not blow up on rails 3

  def organization_valid_for_requester
    return true unless organization_id

    valid_ticket_organization = valid_organization?

    if account.has_endusers_org_active_fix? && requester
      fix_ticket_organization unless valid_ticket_organization
      fix_user_organization unless valid_user_organization?
    else
      unless valid_ticket_organization
        errors.add(
          :organization_id,
          I18n.t("txt.admin.models.ticket.validation.requester_must_be_in_organization")
        )
        log_invalid_organization(:validation_error_added) if requester
      end
    end
  end

  def invalid_organization_additional_logging
    if account&.has_invalid_organization_logging? && requester
      # user is invalid (requester.organization_id is not in requester organization_memberships)
      log_invalid_organization(:invalid_requester) unless requester.organization_id.in?(requester.organization_memberships.map(&:organization_id))
      # ticket is invalid (ticket.organization_id is not in requester organization_memberships)
      log_invalid_organization(:invalid_ticket) unless organization_id.in?(requester.organization_memberships.map(&:organization_id))
    end
  end

  def log_invalid_organization(type)
    Rails.logger.info(
      "invalid_organization: #{type}: "\
      "organization_id_changed: #{organization_id_changed?}, "\
      "ticket_id: #{id}, "\
      "current_user: #{@current_user&.id}, "\
      "requester_id: #{requester_id}, "\
      "requester.organization_id: #{requester.organization_id}, "\
      "organization_id: #{organization_id}, "\
      "requester.organization_memberships ids: #{requester.organization_memberships.map(&:organization_id)}"
    )
    statsd_client.increment('invalid_organization', tags: ["account_id:#{account_id}", "type:#{type}"])
  end

  # Side effects are bad and this shouldn't be here, But there's been many cases where
  # user's organization memberships have become corrupt. See ZD#2514849 and ZD#2351752 for example
  def fix_user_organization
    incorrect_id = requester.organization_id
    requester.organization_id = requester_default_org
    requester.save!
    Rails.logger.info("Requester #{requester.id} organization fixed from #{incorrect_id} to #{requester.organization_id}")
  end

  def fix_ticket_organization
    incorrect_id = organization_id
    self.organization_id = requester_default_org
    Rails.logger.info("Ticket #{id} organization fixed from #{incorrect_id} to #{organization_id}")
  end

  def requester_default_org
    # NOTE: There isn't a default organization for users creted by the TextAPI
    # (when MTC creates a new user as the requester inside TextApiProcessor)
    @requester_default_org ||= requester.organization_memberships.where(default: true).first.organization_id
  end

  def validate_misc
    return false unless account_is_serviceable

    return errors.add(:base, 'Ticket - can\'t validate, missing current user or account') if @current_user.nil? || account_id.nil?

    # Validate comment - propagate errors up to ticket
    errors.add(:base, comment.errors.to_a.first) if comment && !comment.valid?

    # Validate custom field values - propagate errors up to ticket
    ticket_field_entries.to_a.select(&:changed?).each do |field_entry|
      errors.add(:base, field_entry.errors.to_a.first) unless field_entry.valid?
    end
    errors.delete :ticket_field_entries

    errors.add(:priority_id, :invalid) unless PriorityType[priority_id]

    # Need to always validate new requesters
    errors.add(:requester, requester.errors.to_a.first) if requester && requester.new_record? && !requester.valid?

    return if !delta_changes? || ViaType.kind?(@current_via_id.to_i, :system)

    # Validate requester - propagate errors up to ticket
    errors.add(:requester, :invalid) if requester && requester_invalid?

    # Prevent anonymous users from submitting requests without a valid email
    errors.add(:email, :blank) if requester && requester.is_anonymous_user? && requester.email == 'invalid@example.com'
    propogate_collaboration_errors

    errors.add(:group, :invalid) if group_invalid?
    errors.add(:assignee, :invalid) if assignee_invalid?
    errors.add(:assignee, :no_permissions) if assignee_without_permissions?

    add_to_ticket_field_errors(account.field_priority, :invalid, current_user: @current_user, current_account: account) if priority_invalid?
    add_to_ticket_field_errors(account.field_status, :invalid, current_user: @current_user, current_account: account) if status_invalid?
  end

  def validate_description
    add_to_ticket_field_errors(account.field_description, :blank, current_user: @current_user, current_account: account) if account && description.blank?
  end

  def validate_ticket_form
    errors.add(:ticket_form_id, :invalid) if ticket_form && !ticket_form.active?
  end

  def validate_misc_on_update
    if !@force_reopen && !can_be_updated?
      errors.add(:status, I18n.t('activerecord.errors.messages.closed_prevents'))
    end

    return if ViaType.kind?(@current_via_id, :system)

    errors.add(:status, I18n.t('activerecord.errors.messages.reset_to_new')) if status_reset?

    # Triggers and Automations are pre-empted from the remaning inborn rules.
    return if @current_via_id == ViaType.RULE

    errors.add(:priority, I18n.t('activerecord.errors.messages.reset')) if priority_reset?
    errors.add(:type, I18n.t('activerecord.errors.messages.reset')) if type_reset?
    errors.add(:group, I18n.t('activerecord.errors.messages.reset')) if group_reset?
    if assignee_reset_within_group? && !account.settings.change_assignee_to_general_group?
      errors.add(:base, I18n.t('activerecord.errors.messages.assignee_reset_within_group'))
    end
  end

  def validate_deletion_rights
    errors.add(:base, I18n.t('txt.ticket.errors.cannot_delete_incidents_still_attached')) if ticket_type?(:problem) && incidents.any?
    errors.add(:base, I18n.t('txt.ticket.errors.access_denied_restricted_agent')) if @current_user && @current_user.is_restricted_agent?
  end

  def deleting?
    status_id == StatusType.DELETED && status_id_was != StatusType.DELETED
  end

  def propogate_collaboration_errors
    collaboration_errors = []
    collaborations.each do |collaboration|
      collaboration_errors += collaboration.errors.full_messages unless collaboration.valid?
    end
    if collaboration_errors.present?
      errors.delete(:collaborations)
      errors.add(:base, collaboration_errors.uniq.to_a.join(' '))
    end
  end

  def validate_requester_is_not_suspended
    errors.add(:requester, :suspended) if requester && requester.id && requester.suspended?
  end

  def validate_followup
    return unless account.has_validate_closed_ticket_for_followup_tickets?
    if via?(:closed_ticket) && followup_source.nil?
      errors.add(:base, I18n.t('txt.admin.models.ticket.validation.invalid_followup_source_ticket'))
    end
  end

  def validate_submitter
    if new_record?
      # Handling only on create due to existing missing references and system user
      if submitter && !submitter.is_system_user? && submitter.account_id != account_id
        errors.add(:submitter, :invalid)
      end
    else
      # check built-in changes because submitter_id is not tracked by delta_changes
      if submitter_id_changed?
        error_msg = I18n.t('txt.admin.models.ticket.validation.submitter_id_immutable', old_val: submitter_id_was, new_val: submitter_id)
        errors.add(:submitter, error_msg)
      end
    end
  end

  def requester_invalid?
    requester_id_delta_changed? &&
    (requester.account_id != account_id || !requester.is_active?)
  end

  def group_invalid?
    group_id && group_id_delta_changed? &&
    group(true) &&
    (group.account_id != account_id || !group.is_active?)
  end

  def needs_assignee_check?
    assignee_id && assignee_id_delta_changed? && assignee(true)
  end

  def assignee_invalid?
    needs_assignee_check? &&
    (assignee.groups.count(:all) == 0 ||
     assignee.account_id != account_id ||
     !assignee.is_active? ||
     !assignee.is_agent?)
  end

  def assignee_without_permissions?
    needs_assignee_check? && !assignee.can?(:edit, self)
  end

  def group_reset?
    group_id_delta_changed? && group_id.nil?
  end

  def assignee_reset_within_group?
    !@allow_assignee_reset_within_group && assignee_id_delta_changed? && !group_id_delta_changed? && assignee_id.nil?
  end

  def priority_invalid?
    priority_id_delta_changed? &&
    [PriorityType.LOW, PriorityType.URGENT].member?(priority_id) && !account.has_extended_ticket_priorities?
  end

  def status_invalid?
    StatusType.HOLD == status_id && !account.use_status_hold?
  end

  def priority_reset?
    priority_id_delta_changed? && priority?('-')
  end

  def type_reset?
    ticket_type_id_delta_changed? && ticket_type?('-')
  end

  def status_reset?
    status_id_delta_changed? && status?(:new)
  end

  def status_was_changed_from_closed?
    status_id_delta_changed? && StatusType.order.index(status_id_delta_was) >= StatusType.order.index(StatusType.CLOSED)
  end

  def brand_invalid?
    brand_id_delta_changed? && !brand.try(:active)
  end

  #######################

  # Check for required custom fields upon create
  def check_end_user_required_fields_are_set_when_creating
    if validate_required_fields?
      end_user_required_fields.each do |field|
        add_to_ticket_field_errors(field, :blank, current_user: @current_user, current_account: account) if field.is_blank?(self) || (field.is_a?(FieldCheckbox) && field.value(self) == "0")
      end
    end
  end

  # As long as the fields are coming for an app (web / mobile / ...) we validate all custom fields
  #  Otherwise they don't have the resources to correct errors from places like twitter / facebook / ...
  def validate_required_fields?
    (ViaType.kind?(@current_via_id, :full) || followup_from_full_via?) &&
      (@current_user.is_end_user? || source_rel?("help_center") || current_via_reference_id.to_s == ViaType.HELPCENTER.to_s)
  end

  # return true/false based on if the ticket.via.source.rel value matches the value passed in
  #
  # The format of the rel value is the following:
  #
  # "via": {
  #   "source": {
  #     "from": {},
  #     "to": {},
  #     "rel": 'help_center'
  #   }
  # }
  #
  # Hence with the above data `source_rel?('help_center')` would return true
  #
  # @param [string] or [nil]
  # @return [Boolean]
  def source_rel?(value)
    HashParam.ish?(current_via_reference_id) && current_via_reference_id['rel'] == value
  end

  # Check for required custom fields for agents:
  #   * Globally required fields when solved.
  #   * Conditionally required fields on any status:
  #       (only possible if the current ticket form has agent conditions).
  # When required fields are detected, adds validation errors and rejects ticket save.
  #
  # For code and details: ticket/required_fields.rb
  def check_agent_required_fields
    measure_agent_required_fields { validate_agent_required_fields }
  end

  def check_assignee_belongs_to_group
    if @current_user.nil? || !@current_user.is_system_user?
      if group_id_delta_changed? || assignee_id_delta_changed?
        unless group_id && group(true) && group.includes_user_with_id?(assignee_id)
          errors.add(:assignee, "is not a member of the group")
        end
      end
    end
  end

  def validate_email_only_reply
    if (via?(:twitter) || via?(:facebook)) && comment.present? && comment.is_email_only? && requester.email.nil? && collaborators.empty?
      errors.add :base, I18n.t("txt.admin.models.ticket.validation.email_only_without_email")
    end
  end

  def validate_via_id
    unless ViaType[via_id]
      errors.add :base, I18n.t("txt.admin.models.ticket.validation.invalid_via_id")
    end
  end

  def followup_from_full_via?
    current_via?(:closed_ticket) && ViaType.kind?(@original_via_id, :full)
  end
end
