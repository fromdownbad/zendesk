class Ticket < ActiveRecord::Base
  def validate_agent_required_fields
    agent_required_fields.each do |field|
      add_to_ticket_field_errors(field, agent_required_field_error_message,
        for_agent: @current_user.is_agent?,
        current_user: @current_user,
        current_account: account,
        status: status) if field&.is_blank?(self)
    end
  end

  # We never check agent required fields when:
  #   * Current user is an end-user:
  #       - New request through HC.
  #       - New ticket from Twitter/Facebook.
  #       - New comment in existing ticket from Twitter/Facebook.
  #   * Current user is the system (triggers or automations).
  #   * Solving incidents linked to a solved problem.
  #
  # We check agent required fields when any of these are true:
  #   * Ticket status changed: Lotus, API, Mail API, etc.
  #   * Group changed: Assignee is a required field and we need to check presence.
  #   * Change sent from Lotus: We always execute validations in this case
  #     because any errors can be addressed and fixed through the Web UI.
  #   * There were changes on relevant ticket fields: Lotus, API, Mail API (tagger fields).
  def validate_agent_required_fields?
    @current_user&.is_agent? &&
      !@current_user&.is_system_user? &&
      current_changes_need_agent_fields_validations? &&
      current_via_supported_for_agent_fields_validation? &&
      global_or_conditional_fields_check_necessary?
  end

  def agent_required_fields
    return account.ticket_fields.active.select(&:is_required) unless current_ticket_form

    form_fields = current_ticket_form.ticket_fields.active
    # Global required fields need to be consider only when solving the ticket
    preconditionally_required_fields = status?(:solved) ? form_fields.select(&:is_required) : []
    return preconditionally_required_fields unless account.has_native_conditional_fields_enabled?

    conditionally_required_fields(current_ticket_form.agent_ticket_field_conditions, form_fields, preconditionally_required_fields.map(&:id))
  end

  def end_user_required_fields
    return account.ticket_fields.active.select(&:is_required_in_portal) unless current_ticket_form

    form_fields = current_ticket_form.ticket_fields.active
    preconditionally_required_fields = form_fields.select(&:is_required_in_portal)

    return preconditionally_required_fields unless account.has_native_conditional_fields_enabled?

    conditionally_required_fields(current_ticket_form.end_user_ticket_field_conditions, form_fields, preconditionally_required_fields.map(&:id))
  end

  # Measure only accounts that are part of the Enhanced field requirements project.
  # This code will be removed after we collect the necessary information.
  def measure_agent_required_fields
    ZendeskAPM.trace(
      'validate_agent_required_fields',
      service: TicketField::APM_SERVICE_NAME
    ) do |span|
      span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
      span.set_tag("zendesk.account_id", account.id)
      span.set_tag("zendesk.account_subdomain", account.subdomain)
      span.set_tag("zendesk.ticket.status_id", status_id)

      yield
    end
  end

  private

  def conditionally_required_fields(conditions, form_fields, preconditionally_required_fields_ids)
    child_field_ids = conditions.map(&:child_field_id).uniq
    parent_field_ids = conditions.map(&:parent_field_id).uniq
    # Create hash with the ticket values of the parent fields
    # Ex: { 123: 'option_1', 456: 1 }
    parent_field_values = parent_field_ids.map do |id|
      [id, form_fields.find { |f| f.id == id }&.value(self)]
    end.to_h

    # Field visibility will be filled, on demand, using this block definition:
    # Ex: { 123: true, 456, false }
    field_visibility = Hash.new do |hash, field_id|
      hash[field_id] =
        # Is this a conditional field? If not, it's automatically visible.
        !child_field_ids.include?(field_id) ||
        # Is this field a child a condition that is true for this ticket?
        conditions.any? { |c| condition_satisfied?(c, field_id, parent_field_values, hash) }
    end

    # Get conditionally required field ids
    conditionally_required_field_ids = child_field_ids.find_all do |field_id|
      # Is the field visible?
      field_visibility[field_id] &&

      # Check if there's any satisfied condition marked as required on the current ticket status
      conditions.any? do |c|
        c.required_for?(self) && condition_satisfied?(c, field_id, parent_field_values, field_visibility)
      end
    end

    # Conditional requirement overrides global requirement.
    # Let's remove all conditional fields from the globally required fields,
    # and then add the ones that are visible and marked as required that we calculated here.
    required_field_ids = (preconditionally_required_fields_ids - child_field_ids) + conditionally_required_field_ids

    required_field_ids.map { |id| form_fields.find { |f| f.id == id } }
  end

  def condition_satisfied?(condition, field_id, parent_field_values, field_visibility)
    # Child of condition is current field
    condition.child_field_id == field_id &&
    # Value of condition matches ticket's value
    parent_field_values[condition.parent_field_id].to_s == condition.value &&
    # Parent is also visible
    field_visibility[condition.parent_field_id]
  end

  # When the ticket is solved we need to check global and conditionally required fields.
  # When the ticket is in a different state we need to check only conditionally required fields,
  # which is only possible if the current ticket form has agent conditions.
  def global_or_conditional_fields_check_necessary?
    return true if status?(:solved)
    return false unless account.has_native_conditional_fields_enabled?
    return false unless current_ticket_form&.agent_ticket_field_conditions.present?

    true
  end

  # status_id_delta_changed?:
  #   Always check fields when the ticket status changes.
  #
  # group_id_delta_changed?:
  #   Assignee is a globally required field and we want to be sure
  #   is present when group is updated in a solved ticket.
  #
  # current_via?(:web_form):
  #   If a required field is added to the forms after tickets are solved or the
  #   conditional requirements change, tickets may become invalid.
  #   Adding this ensures that the Web UI will show errors that the user can
  #   address but other posts from sources like Twitter or Facebook that have
  #   no way to edit ticket fields won't have their comments invalidated.
  #
  # relevant_ticket_fields_changed?:
  #   Changes on ticket fields that can be parents in conditions
  #   and that can make a conditionally required field visible.
  def current_changes_need_agent_fields_validations?
    status_id_delta_changed? ||
      group_id_delta_changed? ||
      current_via?(:web_form) ||
      relevant_ticket_fields_changed?
  end

  # Check field changes that can reveal a conditionally required field (Type, Priority or Custom Fields).
  def relevant_ticket_fields_changed?
    # Mail API can set tags that change tagger ticket fields.
    # That can cause undesired mail rejections if the change reveals an always required field.
    return false if current_via?(:mail)

    priority_id_delta_changed? ||
      ticket_type_id_delta_changed? ||
      ticket_field_entries_changed?
  end

  def current_via_supported_for_agent_fields_validation?
    # Mail API can set tags that change tagger ticket fields.
    # That can cause undesired mail rejections if the change reveals an always required field.
    # Do this only for new tickets because we still want to reject status changes
    # through email API when the new status requires certain fields.
    # We also want to still check globally required fields for new Mail tickets marked as solved.
    return false if new_and_non_solved_mail_ticket?
    # Validate fields when we create follow-up tickets.
    return true if current_via?(:closed_ticket)
    # Bypass validations when changes are made by the system
    return false if ViaType.kind?(@current_via_id, :system)
    # Bypass validations when solving incidents linked to a solved problem
    return false if current_via?(:linked_problem)
    # when agent is auto-assigned in a solved ticket because of a mail
    # we reach this point because the group changes
    # but we shouldn't validate required fields in that case
    return false if auto_assigned_through_mail?

    true
  end

  def new_and_non_solved_mail_ticket?
    new_record? && !status?(:solved) && current_via?(:mail)
  end

  def auto_assigned_through_mail?
    current_via?(:mail) &&
      !status_id_delta_changed? && status?(:solved) &&
      assignee_id_delta_changed? && assignee == @current_user
  end

  def agent_required_field_error_message
    if status?(:solved)
      'txt.error_message.models.ticket.Validation.is_required_when_solving_ticket'
    else
      'txt.error_message.models.ticket.validation.field_needed'
    end
  end
end
