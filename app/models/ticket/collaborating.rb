class Ticket < ActiveRecord::Base
  MAX_COLLABORATORS_V2 = 48

  def follower_and_email_cc_settings
    {
      comment_email_ccs_allowed_enabled: account.has_comment_email_ccs_allowed_enabled?,
      email_ccs_light_agents_v2: account.has_email_ccs_light_agents_v2?,
      follower_and_email_cc_collaborations_enabled: account.has_follower_and_email_cc_collaborations_enabled?,
      light_agent_email_ccs_allowed_enabled: account.has_light_agent_email_ccs_allowed_enabled?,
      ticket_followers_allowed_enabled: account.has_ticket_followers_allowed_enabled?
    }
  end

  def add_ccs_changed_domain_events
    follower_ids_added = follower_ids_removed = []

    # Collaborators whose type has changed from legacycc to follower and vice versa. This only happens if the
    # email_ccs_transform_collaborations setting is enabled on the account which changes collaborator_type on existing collaborations
    type_changed = collaborators_type_changed

    # Grab all new collaborations being created, if the user is being created at the same time, we won't have a user_id yet
    # so save that fun for later
    new_collaborations = in_flight_collaborations.select(&:new_record?)

    yield

    # New ccs has both followers and email_cc as types of collaborators.
    # Legacy ccs only has one type of collaborator
    if account.has_follower_and_email_cc_collaborations_enabled?
      email_cc_ids_added, follower_ids_added = [CollaboratorType.EMAIL_CC, CollaboratorType.FOLLOWER].collect { |c_type| collaborator_ids_added(new_collaborations, c_type) }
      email_cc_ids_removed, follower_ids_removed = [CollaboratorType.EMAIL_CC, CollaboratorType.FOLLOWER].collect { |c_type| collaborator_ids_removed(c_type) }
    else
      email_cc_ids_added = new_collaborations.map(&:user_id).uniq
      email_cc_ids_removed = (@collaborations_for_removal || []).map(&:user_id).uniq
    end

    # In the case where the collaboration type changed, then that means it either:
    # 1. was a follower and became an email_cc (remove the id from followers list and add it to emailccs list)
    # 2. was an email_cc and became a follower (remove the id from the emailccs list and add it to followers list)
    [email_cc_ids_added, follower_ids_removed].each { |a| a.concat(type_changed[:changed_to_emailcc]).uniq! }
    [follower_ids_added, email_cc_ids_removed].each { |a| a.concat(type_changed[:changed_to_follower]).uniq! }

    add_domain_event(EmailCcsChangedProtobufEncoder.new(email_cc_ids_added, email_cc_ids_removed).to_safe_object) if (email_cc_ids_added | email_cc_ids_removed).any?
    add_domain_event(FollowersChangedProtobufEncoder.new(follower_ids_added, follower_ids_removed).to_safe_object) if (follower_ids_added | follower_ids_removed).any?
  end

  def persist_collaborators
    delete_removed_collaborators
    save_modified_collaborations

    if account.has_email_ccs_transform_collaborations?
      # `save_modified_collaborations` depends on a `changes` field to be present
      # on certain collaborations. Therefore, the reload should not occur in
      # `delete_removed_collaborators`. It should happen after both methods.
      collaborations.reload
    end
  end

  def collaboration_changes
    [].tap do |collaboration_change_events|
      if account.has_comment_email_ccs_allowed_enabled?
        previous_addresses = previous_email_cc_addresses
        current_addresses = email_cc_addresses

        if CollaborationChange.create?(account, previous_addresses, current_addresses)
          collaboration_change_events << EmailCcChange.new.tap do |change|
            change.current_email_ccs = current_addresses
            change.previous_email_ccs = previous_addresses
          end
        end
      end

      if account.has_ticket_followers_allowed_enabled?
        previous_addresses = previous_follower_addresses
        current_addresses = follower_addresses

        if CollaborationChange.create?(account, previous_addresses, current_addresses)
          collaboration_change_events << FollowerChange.new.tap do |change|
            change.current_followers = current_addresses
            change.previous_followers = previous_addresses
          end
        end
      end
    end
  end

  def collaborator_ids_added(new_collaborations, type)
    new_collaborations.select { |c| (c.collaborator_type == type) }.map(&:user_id).uniq
  end

  def collaborators_type_changed
    # If the account has enabled/disabled the new ccs account setting, then it's possible for a collaborator on an existing
    # collaboration to change from an EMAIL_CC -> FOLLOWER (enabled the feature)
    # and a FOLLOWER -> EMAIL_CC (they disabled the feature)
    in_flight_collaborations.each_with_object(changed_to_emailcc: [], changed_to_follower: []) do |collaboration, user_id_hash|
      next unless collaboration.changes.key?("collaborator_type") && !collaboration.new_record?
      case collaboration.changes["collaborator_type"]
      when [CollaboratorType.FOLLOWER, CollaboratorType.LEGACY_CC]
        user_id_hash[:changed_to_emailcc] << collaboration.user_id
      when [CollaboratorType.LEGACY_CC, CollaboratorType.FOLLOWER]
        user_id_hash[:changed_to_follower] << collaboration.user_id
      end
    end.each_value(&:uniq!)
  end

  def collaborator_ids_removed(type)
    (@collaborations_for_removal || []).select { |c| c.collaborator_type == type }.map(&:user_id).compact.uniq
  end

  def ccs
    # Includes legacy CCs + email CCs + followers.
    in_flight_collaborators(:present?)
  end

  def cc_names
    account.has_follower_and_email_cc_collaborations_enabled? ? display_names(email_ccs) : display_names(ccs)
  end

  def collaborators
    account.has_follower_and_email_cc_collaborations_enabled? ? (followers + email_ccs).uniq : super
  end

  def current_collaborator_ids
    @current_collaborator_ids ||= collaborators.map(&:id)
  end

  def email_ccs(users_preloaded: false, filter_for_sending: false)
    if account.has_follower_and_email_cc_collaborations_enabled?
      recipients = Collaboration::LegacyTypeTransformer.email_ccs(
        collaborations: in_flight_collaborations,
        settings: follower_and_email_cc_settings,
        account_id: account_id,
        users_preloaded: users_preloaded
      )

      filter_for_sending ? non_suppressed_recipients(recipients) : recipients
    else
      []
    end
  end

  def persisted_email_cc_user_ids
    return [] unless account.has_comment_email_ccs_allowed_enabled?

    # Using "collaborations" instead of "in_flight_collaborations" intentionally here in order to get collaborations
    # as they existed when the ticket update commenced. We also remove any newly created/non-persisted ones.
    current_collaborations = collaborations.select(&:persisted?)
    return [] if current_collaborations.empty?

    Collaboration::LegacyTypeTransformer.email_ccs(
      collaborations: current_collaborations,
      settings: follower_and_email_cc_settings,
      account_id: account_id
    ).map(&:id)
  end

  def email_cc_addresses
    return [] unless account.has_comment_email_ccs_allowed_enabled?
    EmailCcChange.current_addresses(account, in_flight_collaborations)
  end

  def email_cc_ids
    email_ccs.map(&:id)
  end

  def email_cc_names
    display_names(email_ccs)
  end

  def followers(include_uncommitted: false, users_preloaded: false)
    if account.has_follower_and_email_cc_collaborations_enabled?
      Collaboration::LegacyTypeTransformer.followers(
        collaborations: in_flight_collaborations,
        settings: follower_and_email_cc_settings,
        account_id: account_id,
        users_preloaded: users_preloaded
      )
    elsif include_uncommitted
      in_flight_collaborators(:present?)
    else
      # Includes (saved): legacy CCs + email CCs + followers.
      collaborators
    end
  end

  def follower_addresses
    return [] unless account.has_ticket_followers_allowed_enabled?
    FollowerChange.current_addresses(account, in_flight_collaborations)
  end

  def follower_ids
    followers.map(&:id)
  end

  def follower_names
    if account.has_follower_and_email_cc_collaborations_enabled?
      display_names(followers)
    else
      display_names([])
    end
  end

  # rubocop:disable Naming/PredicateName
  def is_follower?(user)
    unless account.has_follower_and_email_cc_collaborations_enabled?
      # Avoid loading collaboration.user associations by doing ID check first
      return collaborations.any? do |c|
        c.user_id == user.id || ((c.new_record? || user.new_record?) && c.user == user)
      end
    end

    return false unless account.has_ticket_followers_allowed_enabled?
    return false unless user.is_agent?
    is_collaborator?(user, CollaboratorType.FOLLOWER) || is_collaborator?(user, CollaboratorType.LEGACY_CC)
  end

  def is_email_cc?(user)
    return false unless account.has_comment_email_ccs_allowed_enabled?

    (is_collaborator?(user, CollaboratorType.EMAIL_CC) && user_can_comment_publicly_or_light_agent_ccs_allowed?(user)) ||
    (is_collaborator?(user, CollaboratorType.LEGACY_CC) && !user.is_agent?)
  end

  def is_participant?(user)
    return true if requested_by?(user)
    return true if assignee.present? && user == assignee
    is_collaborator?(user)
  end

  def is_collaborator?(user, collaborator_type = nil)
    unless account.has_follower_and_email_cc_collaborations_enabled?
      # Avoid loading collaboration.user associations by doing ID check first
      return collaborations.any? do |c|
        c.user_id == user.id || ((c.new_record? || user.new_record?) && c.user == user)
      end
    end

    collaborations.any? do |c|
      # Avoid loading collaboration.user associations by doing ID check first
      matches_user = c.user_id == user.id || ((c.new_record? || user.new_record?) && c.user == user)
      next matches_user if !matches_user || !collaborator_type
      c.collaborator_type == collaborator_type
    end
  end
  # rubocop:enable Naming/PredicateName

  def notify_followers
    return unless @audit

    unless @audit.valid?
      statsd_client.increment('notify_followers_with_bad_audit')

      audit_errors = audit.errors.full_messages

      audit_event_errors = (audit.events || []).map do |event|
        next if event.valid?

        event.errors.full_messages.to_sentence
      end.compact

      Rails.logger.warn(
        "Trying to notify followers in ticket save with non-valid audit." \
          "audit: #{audit.inspect}, " \
          "errors: #{audit_errors.inspect}, " \
          "audit.metadata: #{audit.metadata.inspect}, " \
          "audit.events: #{audit.events.inspect}, " \
          "audit.events errors: #{audit_event_errors}, " \
          "account_id: #{account.id}, " \
          "ticket_id: #{id}."
      )

      return
    end

    return if disable_notifications # create events to notify CCs and shared org followers

      # When CCs/Followers is enabled, Ticket#collaborators, Ticket#followers and Ticket#email_ccs are Arrays based on the
      # 'collaborations' association so to reload 'collaborators', 'followers' and 'email_ccs', we need to reload
      # 'collaborations'.
      #
      # Additionally, if CCs/Followers is disabled but email_ccs_transform_collaborations is enabled, we are
      # manipulating the collaborations association more than we would be otherwise prior to this point in the ticket
      # save, and probably need to be reloading it prior to the collaborators association.
    if account.has_follower_and_email_cc_collaborations_enabled? || account.has_email_ccs_transform_collaborations?
      collaborations.reload
    end

      # Reload collaborators if collaborators is still an association/CollectionProxy
    unless account.has_follower_and_email_cc_collaborations_enabled?
      begin
        collaborators.reload
      rescue ArgumentError => e
        Rails.logger.info("Reload of collaborators in notify_followers failed, exception: #{e.inspect}")
      end
    end

    TicketNotifier.notify(self, @audit)
  end

  def only_follower_collaborations_changed?
    return false unless account.has_ticket_followers_allowed_enabled?

    changed_or_new = collaborations.select { |c| c.changed? || c.new_record? }
    return false if changed_or_new.any? { |c| !c.follower? }

    @collaborations_for_removal ||= []
    return false if changed_or_new.empty? && @collaborations_for_removal.empty?

    non_followers_being_removed = !@collaborations_for_removal.empty? && @collaborations_for_removal.any? { |c| !c.follower? }
    return false if non_followers_being_removed

    true
  end

  def user_ids_to_collaborators_hashes(collaborator_ids)
    collaborator_ids.map { |collaborator_id| { user_id: collaborator_id, action: :put } }
  end

  private

  def previous_email_cc_addresses
    EmailCcChange.last_event_addresses(events)
  end

  def previous_follower_addresses
    FollowerChange.last_event_addresses(events)
  end

  def in_flight_collaborations
    # NOTE: Can't use ActiveRecord because we need to include new/unpersisted collaborations.
    collaborations - (@collaborations_for_removal || [])
  end

  def in_flight_collaborators(collaboration_type)
    # NOTE: Macros can render `{{ticket.<xyz>_names}}` placeholders with unsaved users.
    in_flight_collaborations.select(&collaboration_type).map(&:user).uniq
  end

  def cached_collaborators(user_ids)
    return {} if user_ids.empty?

    @cached_collaborators ||= {}

    if account.has_follower_and_email_cc_collaborations_enabled?
      @cached_collaborators.merge!(@email_cc_users_by_id || {})
      @cached_collaborators.merge!(@follower_users_by_id || {})
    end

    user_ids_to_query = user_ids - @cached_collaborators.keys
    if user_ids_to_query.any?
      @cached_collaborators.
        merge!(account.users.where(id: user_ids_to_query).index_by(&:id))
    end

    @cached_collaborators
  end

  # Update Ticket#current_collaborators attribute - note that this cannot
  # be done after validations (such as in a before_save) because currently
  # the light agent checks in ticket_validator.rb depend on current_collaborators
  # being modified a la set_collaborators (and still in set_collaborators_v2).
  # If we eventually clean this up so that the validations look at the collaborations
  # directly, then we could potentially move this into the before_save
  # transform_collaborations as well.
  def update_current_collaborators
    users_by_id = cached_collaborators(in_flight_collaborations.map(&:user_id))

    all_users = in_flight_collaborations.map do |in_flight_collaboration|
      user = users_by_id[in_flight_collaboration.user_id] || in_flight_collaboration.user
      unless user.try(:valid?)
        log_collaboration_with_invalid_user(in_flight_collaboration)
        next nil
      end
      user
    end.compact.uniq

    self.current_collaborators = if current_collaborators.blank? || current_collaborators =~ /<.*@.*>/
      all_users.map { |u| "#{u.name} <#{u.email}>" }
    else
      all_users.map(&:name)
    end.sort.join(", ").presence
  end

  def display_names(users)
    users.map(&:display_name).join(', ')
  end

  def transform_collaborations
    return unless account.has_email_ccs_transform_collaborations?

    followers_and_email_ccs_enabled = account.has_follower_and_email_cc_collaborations_enabled?
    Collaboration::LegacyTypeTransformer.transform(collaborations, followers_and_email_ccs_enabled)

    remove_duplicate_collaborations
    mark_invalid_collaborations
    limit_total_number_of_ccs
  end

  # Persist any new users that are part of email CC collaborations before the
  # triggers run so that they can be included in the NotificationWithCcs recipients
  # list.
  def persist_new_email_cc_users
    return unless account.has_comment_email_ccs_allowed_enabled?
    @collaborations_for_removal ||= []
    in_flight_collaborations.each do |in_flight_collaboration|
      next unless in_flight_collaboration.email_cc? && in_flight_collaboration.user.try(:new_record?)
      begin
        in_flight_collaboration.user.save!
      rescue ActiveRecord::RecordInvalid => invalid
        logger.info(
          "Collaboration user is invalid and won't be persisted: " \
          "errors: #{invalid.record.errors.full_messages}"
        )
        @collaborations_for_removal << in_flight_collaboration unless @collaborations_for_removal.include?(in_flight_collaboration)
      end
    end
  end

  # Removes both persisted and in flight duplicate collaborations by:
  #  - building a map of `user_identity => collaboration_type => collaboration`
  #     - user_identity is the user_id or user.email. We fall back to user.email
  #       when the user is unpersisted - it will not have an ID yet
  #     - the map tracks the collaboration to be saved
  #  - and swapping in better collaborations while deleting the swapped out collaboration
  #     - better collaborations are persisted, and have earlier `created_at` fields
  def remove_duplicate_collaborations
    user_collaboration_map = {}
    in_flight_duplicate_objects = Set.new

    in_flight_collaborations.each do |collaboration|
      user_identity = collaboration.user_id || collaboration.user.email
      next unless user_identity

      collaborator_type = collaboration.collaborator_type

      user_collaboration_map[user_identity] ||= {}

      mapped_collaboration = user_collaboration_map[user_identity][collaborator_type]

      if mapped_collaboration.nil?
        user_collaboration_map[user_identity][collaborator_type] = collaboration

        next
      end

      case [collaboration.persisted?, mapped_collaboration.persisted?]
      when [true, true]
        if mapped_collaboration.created_at < collaboration.created_at
          mark_collaborations_for_deletion([collaboration])
        else
          user_collaboration_map[user_identity][collaborator_type] = collaboration

          mark_collaborations_for_deletion([mapped_collaboration])
        end
      when [true, false]
        user_collaboration_map[user_identity][collaborator_type] = collaboration

        in_flight_duplicate_objects << mapped_collaboration
      else
        in_flight_duplicate_objects << collaboration
      end
    end

    in_flight_duplicate_objects.each do |collaboration|
      logger.info(
        "Removing duplicate non-persisted collaboration:"\
        "ticket_id: #{id}, "\
        "account_id: #{account_id}, "\
        "user_id: #{collaboration.user_id}"
      )
    end
    self.collaborations -= in_flight_duplicate_objects.to_a
  end

  def delete_removed_collaborators
    return if @collaborations_for_removal.blank?

    if account.has_email_ccs_transform_collaborations?
      if @collaborations_for_removal.is_a?(ActiveRecord::Relation)
        @collaborations_for_removal.delete_all
      else
        Collaboration.where(id: @collaborations_for_removal.map(&:id), account_id: account.id).delete_all
      end
    else
      if account.has_follower_and_email_cc_collaborations_enabled? || !@collaborations_for_removal.is_a?(ActiveRecord::Relation)
        Collaboration.where(id: @collaborations_for_removal.map(&:id), account_id: account.id).delete_all
      else
        @collaborations_for_removal.delete_all
      end

      collaborations.reload
    end

    if account.has_email_ccs_clear_collaborations_for_removal?
      remove_instance_variable(:@collaborations_for_removal)
    end
  end

  def save_modified_collaborations
    if account.has_email_ccs_transform_collaborations?
      collaborations.each do |collaboration|
        collaboration.save! if collaboration.changes.key?("collaborator_type")
      end
    end
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Error in save_modified_collaborations", fingerprint: '2dbdf1c5edffa2b93a2a190420bf77335cbfa16f')
  end

  def limit_total_number_of_ccs
    return unless follower_and_email_cc_settings[:comment_email_ccs_allowed_enabled]

    inflight_ccs = email_ccs_collaborations(in_flight_collaborations)

    return unless inflight_ccs.count > max_email_ccs_limit

    new_ccs_to_remove = []

    existing_ccs, new_ccs = inflight_ccs.partition(&:persisted?)
    number_of_new_ccs_allowed = (max_email_ccs_limit - existing_ccs.count)

    if number_of_new_ccs_allowed <= 0
      Rails.logger.info(
        'Existing number of CCs already at the limit, marking all new CCs for removal.'
      )

      new_ccs_to_remove += new_ccs
    else
      Rails.logger.info(
        "Adding all new CCs will exceed the limit. Only " \
        "#{number_of_new_ccs_allowed} new CCs are added"
      )

      new_ccs_to_remove += new_ccs.slice(
        number_of_new_ccs_allowed,
        new_ccs.length
      )
    end

    self.collaborations -= new_ccs_to_remove
  end

  # wrap constant in helper to help test it easily
  def max_email_ccs_limit
    MAX_COLLABORATORS_V2
  end

  def mark_invalid_collaborations
    @collaborations_for_removal ||= []

    invalid_collaborations = collaborations.reject(&:user_valid?)
    persisted_invalid_collaborations, unpersisted_invalid_collaborations = invalid_collaborations.partition(&:persisted?)
    @collaborations_for_removal += persisted_invalid_collaborations
    self.collaborations -= unpersisted_invalid_collaborations

    return unless follower_and_email_cc_settings[:follower_and_email_cc_collaborations_enabled]

    if follower_and_email_cc_settings[:ticket_followers_allowed_enabled]
      @collaborations_for_removal += invalid_followers
    end

    if follower_and_email_cc_settings[:comment_email_ccs_allowed_enabled]
      @collaborations_for_removal += invalid_email_ccs
    end

    @collaborations_for_removal.uniq!
  end

  # collect all the collaborations that have no user or an end-user in it
  def invalid_followers
    collaborations.each_with_object([]) do |collaboration, result|
      if collaboration.follower?(verify_agent: false) &&
        (collaboration.user.nil? || collaboration.user.is_end_user?)
        result << collaboration
      end
    end
  end

  def email_ccs_light_agents_v2?
    @email_ccs_light_agents_v2 ||= account.has_email_ccs_light_agents_v2?
  end

  def light_agent_email_ccs_allowed_enabled?
    @light_agent_email_ccs_allowed_enabled ||= account.has_light_agent_email_ccs_allowed_enabled?
  end

  def user_can_comment_publicly_or_light_agent_ccs_allowed?(user)
    return true if user.can?(:publicly, Comment)

    email_ccs_light_agents_v2? && light_agent_email_ccs_allowed_enabled?
  end

  def invalid_email_ccs
    collaborations.select do |collaboration|
      collaboration.email_cc?(verify_user: false) &&
      (collaboration.user.nil? || !user_can_comment_publicly_or_light_agent_ccs_allowed?(collaboration.user))
    end
  end

  def log_collaboration_with_invalid_user(collaboration)
    Rails.logger.info(
      'Invalid user in update_current_collaborators ' \
      "for account_id: #{account.id} " \
      "in ticket_id: #{id} for collaboration: #{collaboration.id} "\
      "with collaboration_type: #{collaboration.collaborator_type}"
    )
  end

  def non_suppressed_recipients(recipients)
    suppressed_recipients = suppressed_recipients(recipients)

    recipients.reject { |recipient| suppressed_recipients.map(&:id).include? recipient.id }
  end

  def suppressed_recipients(recipients)
    suppressed = []

    suppressed += machine_recipients(recipients)
    suppressed += undeliverable_recipients(recipients)
    suppressed += recipients_with_setting(
      'suspended',
      [true, 'true'],
      recipients
    )

    suppressed.uniq
  end

  def machine_recipients(recipients)
    account.users.where(id: recipients).select(&:machine?)
  end

  def recipients_with_setting(name, value, recipients)
    UserSetting.where(
      account_id: account.id,
      name:       name,
      value:      value,
      user_id:    recipients
    ).map(&:user)
  end

  def undeliverable_recipients(recipients)
    # rubocop:disable Style/InverseMethods
    User.where(id: recipients, account_id: account.id).
      select { |user| !user.email_is_deliverable? }
    # rubocop:enable Style/InverseMethods
  end
end
