require 'zendesk/outgoing_mail/variables'

class AccountPropertySet < ActiveRecord::Base
  include CachingObserver
  include CIA::Auditable
  include PrecreatedAccountConversion

  belongs_to :account
  has_kasket_on :account_id

  attr_accessible :account

  serialize :branding

  before_validation :set_defaults, on: :create
  validates_presence_of :account_id
  validate :validate_html_template, if: :account
  validate :validate_text_template, if: :account

  before_save :tidy_introductory_text, if: :introductory_text_changed?

  after_save :touch_account

  def touch_account
    account.touch_without_callbacks
  end

  # NOTE: new visible attribute -> new translation under txt.admin.views.reports.tabs.audits.attribute.<attribute>
  attr_accessor :current_user # Can be removed when old auditing is removed.

  ignore_column :signature_template

  def text_mail_template
    read_attribute(:text_mail_template).blank? ? Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE : attributes['text_mail_template']
  end

  def validate_html_template
    validate_mail_template(:html_mail_template, ['content'])
  end

  def validate_text_template
    validate_mail_template(:text_mail_template, ['content'])
  end

  def validate_mail_template(name, placeholders)
    template = send(name)
    if template.blank?
      errors.add(:base, I18n.t("txt.admin.controllers.settings.#{name}_blank"))
    else
      placeholders.each do |p|
        errors.add(:base, I18n.t("txt.admin.controllers.settings.#{name}_missing_placeholder", placeholder: p)) unless template.index("{{#{p}}}")
      end
    end

    # Check for incorrect liquid placeholders

    if send("#{name}_changed?".to_sym)
      begin
        Liquid::Template.parse(template)
      rescue StandardError => e
        template_type = name.to_s.split('_')[0]
        i18n_template_type_key = "txt.admin.settings.email.type.#{template_type}"
        translated_template_type = I18n.t(i18n_template_type_key)

        liquid_variable_termination_error = "Liquid syntax error: Variable '{{' was not properly terminated with regexp: /\\}\\}/"

        liquid_error_message = e.message

        error_message =
          case liquid_error_message
          when liquid_variable_termination_error
            I18n.t("txt.admin.settings.email.errors.syntax.variable_termination", template_type: translated_template_type)
          else
            exception_message = "Unknown liquid error found: #{liquid_error_message}"
            ZendeskExceptions::Logger.record(StandardError.new(exception_message), location: self, message: exception_message, reraise: !Rails.env.production?, fingerprint: '24e2614593541d9af6ac3929eda80bf4903e2aea')
            "A #{translated_template_type} email template placeholder is invalid: #{liquid_error_message}"
          end

        errors.add(:base, error_message)
      end

      # Check that updated template does not exceed mysql column size
      column = self.class.columns_hash.fetch(name.to_s)
      column_size_limit = Zendesk::Extensions::TruncateAttributesToLimit.limit(column)
      _prev_value, updated_value = changes[name.to_s]

      if updated_value.length > column_size_limit
        errors.add(:base, I18n.t("txt.admin.controllers.settings.#{name}_too_long"))
        restore_attributes([name])
      end
    end
  end

  def set_defaults
    if !account.is_sandbox?
      self.branding           = Branding.new
      self.signup_page_text   = I18n.t("txt.default.registration_message_v2")
      self.signup_email_text  = I18n.t("txt.default.new_welcome_email_text", account_name: account.name)

      self.text_mail_template = Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE
      self.html_mail_template = Zendesk::OutgoingMail::Variables::ACCESSIBLE_HTML_MAIL_TEMPLATE_V2

      self.cc_email_template = default_cc_email_template
      self.follower_email_template = default_follower_email_template

      self.organization_activity_email_template = default_organization_activity_email_template
      self.verify_email_text = I18n.t("txt.default.verify_email_text")
      self.chat_welcome_message = I18n.t("txt.default.chat_welcome_message")
      self.ticket_forms_instructions = I18n.t("txt.default.ticket_forms_instructions")
    else
      self.branding           = account.sandbox_master.branding
      self.signup_page_text   = account.sandbox_master.signup_page_text
      self.signup_email_text  = account.sandbox_master.signup_email_text

      self.html_mail_template = account.sandbox_master.html_mail_template
      self.text_mail_template = account.sandbox_master.text_mail_template

      self.cc_email_template = account.sandbox_master.cc_email_template
      self.follower_email_template = account.sandbox_master.follower_email_template

      self.organization_activity_email_template = account.sandbox_master.organization_activity_email_template
      self.verify_email_text = account.sandbox_master.verify_email_text
      self.introductory_title = account.sandbox_master.introductory_title
      self.introductory_text  = account.sandbox_master.introductory_text
      self.customer_satisfaction_privacy = account.sandbox_master.customer_satisfaction_privacy
      self.chat_welcome_message = account.sandbox_master.chat_welcome_message
      self.ticket_forms_instructions = account.sandbox_master.ticket_forms_instructions
    end
  end

  def reset_signup_email_text
    update_attribute(:signup_email_text, I18n.t("txt.default.new_welcome_email_text", account_name: account.name))
  end

  def organization_activity_email_template
    read_attribute(:organization_activity_email_template) || default_organization_activity_email_template
  end

  # Legacy-CCs
  def cc_email_template
    super.presence || default_cc_email_template
  end

  def follower_email_template
    super.presence || default_follower_email_template
  end

  protected

  def tidy_introductory_text
    if introductory_text.present?
      self.introductory_text = Nokogiri::HTML::DocumentFragment.parse(introductory_text).to_html
    end
  end

  # Legacy-CCs
  def default_cc_email_template
    I18n.t("txt.default.cc_email_text.paragraph_v2")
  end

  def default_follower_email_template
    I18n.t("txt.default.follower_email_text")
  end

  def default_organization_activity_email_template
    "#{I18n.t("txt.default.organization_activity_email_text")}\n\n{{ticket.comments_formatted}}"
  end
end
