class EntryFlagType < ActiveHash::Base
  fields :id, :name

  include ActiveHash::Enum
  enum_accessor :name

  include ActiveHash::Associations
  has_many :entries, foreign_key: :flag_type_id

  def answered?
    name == "Answered"
  end

  def planned?
    name == "Planned"
  end

  def suspended?
    id == SUSPENDED.id
  end

  def self.lookup_name(id)
    { 1   => "Needs Answer",
      100 => "Answered",
      200 => "Planned",
      201 => "Done",
      300 => "Not Planned",
      400 => "Suspended"}[id]
  end

  self.data = [
    { id: 1,   name: "Unknown" },
    { id: 100, name: "Answered" },
    { id: 200, name: "Planned" },
    { id: 201, name: "Done" },
    { id: 300, name: "Notplanned" },
    { id: 400, name: "Suspended" }
  ]
end
