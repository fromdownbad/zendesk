class CouponApplication < ActiveRecord::Base
  not_sharded
  disable_global_uid

  belongs_to :account
  belongs_to :subscription
  belongs_to :coupon

  has_many :coupon_redemptions
  has_many :payments, through: :coupon_redemptions

  attr_accessible :coupon, :account, :subscription, :exhaustion_date, :initial_full_monthly_price,
    :initial_max_agents, :initial_plan_type, :initial_billing_cycle_type

  validates_presence_of :account_id
  validates_presence_of :subscription
  validates_presence_of :coupon
  validates_presence_of :exhaustion_date

  validates_numericality_of :initial_max_agents, only_integer: true, greater_than_or_equal_to: 0

  validates_presence_of(:initial_plan_type,
    only_integer: true,
    greater_than_or_equal_to: SubscriptionPlanType.Small,
    less_than_or_equal_to: SubscriptionPlanType.ExtraLarge)

  validates_numericality_of(:initial_billing_cycle_type,
    only_integer: true,
    greater_than_or_equal_to: BillingCycleType.Monthly,
    less_than_or_equal_to: BillingCycleType.Annually)

  validates_presence_of :initial_full_monthly_price, greater_than_or_equal_to: 0

  def initialize(*args)
    super(*args)

    # ?!
    unless coupon.nil?
      self.exhaustion_date ||= Time.now + coupon.length_days.days
    end
  end

  def active?
    subscription.active_coupon_application == self
  end

  def exhausted?(as_of = Time.now)
    exhaustion_date < as_of
  end

  def is_valid_on?(subscription_instance) # rubocop:disable Naming/PredicateName
    coupon.is_valid_on?(subscription_instance, self)
  end

  def configure_discount_calculation(calcs, duration_in_days)
    if coupon.type == "DiscountCoupon"
      calcs.coupon_discount_percentage      = coupon.discount_percentage
      calcs.coupon_maximum_monthly_discount = coupon.maximum_monthly_discount
      calcs.coupon_exhaustion_date          = exhaustion_date
      calcs.coupon_excluded_amount          = initial_full_monthly_price * duration_in_days / 30
    end
  end
end
