class OrganizationMembership < ActiveRecord::Base
  include PrecreatedAccountConversion
  include OrganizationMembershipObserver
  extend  Zendesk::CappedCounts

  MAX_PER_USER = 300

  belongs_to :account
  belongs_to :organization, inverse_of: :organization_memberships, inherit: :account_id
  belongs_to :user, inverse_of: :organization_memberships, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  attr_accessible :account, :organization, :organization_id, :user, :user_id, :default

  before_validation :set_account
  before_save       :make_first_default
  before_save       :unset_current_default, if: :made_default?
  after_create      :update_user_on_non_default
  after_update      :move_user_tickets_to_new_organization, if: :organization_id_changed?
  after_update      :remove_previous_watching, if: :organization_id_changed?
  after_destroy     :handle_default_succession,
    :remove_current_watching,
    :move_user_tickets,
    :update_user_on_non_default

  # update_user callbacks after_create and after_destroy can be merged on Rails4
  # as `after_commit :update_user, on: [:create, :destroy]`
  # see: https://github.com/rails/rails/issues/988

  validates_presence_of :account_id, :user, :organization
  validates_uniqueness_of :user_id, scope: :organization_id, message: I18n.t("activerecord.errors.models.organization_membership.unique_user")
  validate :validate_account_on_organization_and_user_match, if: :organization_and_user_present?
  validate :validate_under_limit, on: :create

  scope :regular, -> { where(default: nil) }
  scope :active,  -> { joins(:user).where('users.is_active = ?', true) }

  def default=(value)
    if Zendesk::FALSE_VALUES.include?(value)
      super(nil)
    else
      super
    end
  end

  # For removing the model name in validation messages.
  def self.human_attribute_name(attr, options = {})
    attr.to_s == "user_id" ? "" : super
  end

  def move_user_tickets_from_any
    user.move_tickets_to_organization(:any, organization_id)
  end

  private

  def validate_under_limit
    if user && user.organization_memberships.count(:all) >= MAX_PER_USER
      errors.add(:user, I18n.t("activerecord.errors.models.organization_membership.over_limit", limit: MAX_PER_USER))
    end
  end

  def unset_current_default
    if orgs_to_unset = user.organization_memberships.where(unset_default_conditions).order(:id).pluck(:id).presence
      OrganizationMembership.where(id: orgs_to_unset).update_all(default: nil, updated_at: Time.now)
    end
    if user.organization_id != organization_id
      update_user_organization(organization_id)
    end
  end

  def make_first_default
    if !default? && !user_has_default?
      self.default = true
    end
  end

  def user_has_default?
    user.organization_memberships.exists? ||
      (user.organization_memberships.loaded? && user.organization_memberships.any?(&:default?))
  end

  def unset_default_conditions
    [exclude_this_record, "`default` IS NOT NULL"].compact.join(" AND ")
  end

  def exclude_this_record
    "id != #{id}" if id.present?
  end

  def move_user_tickets
    if account.has_unset_ticket_org_when_removing_default_user_org?
      move_user_tickets_to_nil
    else
      move_user_tickets_to_default
    end
  end

  def move_user_tickets_to_nil
    user.move_tickets_to_organization(organization_id_was, nil)
  end

  def move_user_tickets_to_default
    user.move_tickets_to_organization(organization_id_was, user.organization_id)
  end

  def move_user_tickets_to_new_organization
    user.move_tickets_to_organization(organization_id_was, organization_id)
  end

  def remove_current_watching
    remove_watching_for(organization_id)
  end

  def remove_previous_watching
    remove_watching_for(organization_id_was)
  end

  def remove_watching_for(org_id)
    if watching = user.watchings.for_source("Organization", org_id).first
      watching.destroy
    end
  end

  def made_default?
    default_changed? && default?
  end

  alias_method :user_handled_default_succession?, :marked_for_destruction?

  def handle_default_succession
    return if user_handled_default_succession?

    if no_successor?
      unset_user_organization_id
    elsif default?
      promote_successor_to_default
    end
  end

  def no_successor?
    !user.organization_memberships.
      joins(:organization).
      where('organizations.deleted_at is NULL').
      exists?
  end

  def promote_successor_to_default
    return unless successor
    successor.default = true
    successor.save(validate: false)
    update_user_organization(successor.organization_id)
  end

  def successor
    @successor ||= user.organization_memberships.
      joins(:organization).
      where('organizations.deleted_at is NULL').
      reject(&:default?).
      sort_by(&:created_at).first
  end

  def set_account
    self.account = if user
      user.account
    elsif organization
      organization.account
    end
  end

  def unset_user_organization_id
    # We don't want to trigger callbacks on user that
    # will result the destroyed organization_membership
    # being recreated see Users::Relationships#set_default_organization
    update_user_organization(nil)
  end

  def organization_and_user_present?
    organization.present? && user.present?
  end

  def validate_account_on_organization_and_user_match
    if user.account_id != organization.account_id
      errors.add(:user, "must belong to the same account as the organization")
    end
  end

  def update_user_organization(organization_id)
    User.where(id: user.id).update_all(organization_id: organization_id, updated_at: Time.now)
    user.reload
  end

  def update_user_on_non_default
    user.touch_without_callbacks
  end
end
