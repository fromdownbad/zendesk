class View < Rule
  ignore_column :sla_id

  include Zendesk::Rules::Routing
  include Zendesk::Serialization::ViewSerialization
  include Zendesk::PreventCollectionModificationMixin

  MAX_TABLE_COLUMNS = 12 # emergency fix for ZD:412402

  HARD_LIMIT_FOR_VIEWS = 5500

  # values for preview ids
  INCOMING = 'incoming'.freeze
  MY = 'my'.freeze
  MY_GROUPS = 'my_groups'.freeze
  # values for preview titles
  INCOMING_TITLE = 'Incoming'.freeze
  MY_TITLE = 'My Tickets'.freeze
  MY_GROUPS_TITLE = 'My Groups Tickets'.freeze
  BROKEN_COUNT = -1

  COLUMNS = %i[
    status
    description
    requester
    updated_requester
    group
    assignee
  ].freeze

  has_many :groups_views, class_name: 'GroupView', autosave: true, inherit: :account_id
  has_many :groups, through: :groups_views

  attr_accessor :include_archived_tickets

  attr_accessor :group_owner_ids

  before_validation :strip_invalid_output_columns

  validates_presence_of :owner

  validate :table_does_not_exceed_maximum_columns
  validate :watchability_for_filtered_view, on: :update
  validates_with Zendesk::Rules::Validators::View::DescriptionWordValidator
  validates_with Zendesk::Rules::Validators::View::GroupIdValidator

  before_save :update_associated_groups

  after_save :update_skill_based_filtered_views
  after_save if: :write_group_views? do
    groups.reload if group_owner_ids
    self.group_owner_ids = nil
  end

  # The default dynamic view for 'incoming' tickets for Zendesk Play.
  def self.incoming(current_user)
    view = View.new(title: INCOMING_TITLE)
    view.owner      = current_user
    view.output     = Output.create(
      type:      'table',
      group:     :priority,
      group_asc: false,
      order:     :updated_requester,
      order_asc: true,
      columns:   COLUMNS
    )
    view.definition = Definition.new
    view.definition.conditions_all.push(
      DefinitionItem.new('status_id', 'less_than', StatusType.PENDING)
    )
    view.definition.conditions_any.push(
      DefinitionItem.new('assignee_id', 'is', current_user.id)
    )
    view.definition.conditions_any.push(
      DefinitionItem.new('group_is_set_with_no_assignee', '', '')
    )
    view.definition.conditions_any.push(
      DefinitionItem.new('group_id', 'is', '')
    )
    view.current_user = current_user
    view.include_archived_tickets = false
    view
  end

  def self.my(current_user)
    view = View.new(title: MY_TITLE)
    view.owner      = current_user
    view.output     = Output.create(
      type:      'table',
      group:     :priority,
      group_asc: false,
      order:     :updated_requester,
      order_asc: true,
      columns:   COLUMNS
    )
    view.definition = Definition.new
    view.definition.conditions_all.push(
      DefinitionItem.new('assignee_id', 'is', current_user.id)
    )
    view.current_user = current_user
    view.include_archived_tickets = false
    view
  end

  def self.my_groups(current_user)
    view = View.new(title: MY_GROUPS_TITLE)
    view.owner      = current_user
    view.output     = Output.create(
      type:      'table',
      group:     :priority,
      group_asc: false,
      order:     :updated_requester,
      order_asc: true,
      columns:   COLUMNS
    )
    view.definition = Definition.new
    view.definition.conditions_all.push(
      DefinitionItem.new('group_id', 'is', current_user.group_ids)
    )
    view.current_user = current_user
    view.include_archived_tickets = false
    view
  end

  # Input definition in format:
  # {all: [{ field: 'status', operator: 'is', value: 'OPEN'}], any: [...]}
  def self.preview(conditions)
    preview = begin
      if conditions[:id] == INCOMING
        incoming(conditions[:owner])
      elsif conditions[:id] == MY
        my(conditions[:owner])
      elsif conditions[:id] == MY_GROUPS
        my_groups(conditions[:owner])
      elsif conditions[:id]
        # don't preview inactive views
        active.find(conditions.delete(:id))
      else
        new
      end
    end

    if (conds = conditions.delete(:conditions))
      conditions[:all] ||= conds[:all]
      conditions[:any] ||= conds[:any]
    end

    if conditions[:all].present?
      conditions[:all].each do |condition|
        Api::V2::Rules::ConditionMappings.map_definition_item!(
          preview.account, condition
        )
      end
    end

    if conditions[:any].present?
      conditions[:any].each do |condition|
        Api::V2::Rules::ConditionMappings.map_definition_item!(
          preview.account, condition
        )
      end
    end

    definition = Definition.build(
      conditions_all: conditions.delete(:all),
      conditions_any: conditions.delete(:any)
    )

    if preview.definition.is_a?(Definition)
      definition = preview.definition.merge(definition)
    end

    conditions[:output] ||= {}

    output = begin
      if preview.new_record?
        Output.default(conditions[:output])
      else
        preview.output.merge(conditions[:output])
      end
    end

    scope_attributes = scope_for_create if is_a?(ActiveRecord::Relation)
    conditions.merge!(scope_attributes.except(:id, :type)) if scope_attributes

    conditions[:definition] = definition
    conditions[:output] = output

    preview.tap do |view|
      view.attributes = conditions

      fail ActiveRecord::RecordInvalid, view unless view.valid?
    end
  end

  def output
    super.tap do |out|
      unless account.has_groups?
        out.columns = out.columns.reject { |name| name == :group }
      end

      unless account.has_skill_based_ticket_routing?
        out.columns = out.columns.reject { |name| name == :attributes_match }
      end
    end
  end

  def preview_id
    definition.cache_key
  end

  def ticket_count(user, options = {})
    klass = begin
      if new_record?
        CachedRulePreviewTicketCount
      else
        CachedRuleTicketCount
      end
    end

    klass.lookup(self, user, options)
  end

  def self.find_for_user(current_user, id)
    id == INCOMING ? incoming(current_user) : super
  end

  def preloaded_associations
    output.try(:rule_associations) || []
  end

  def next_ticket(user, options = {})
    Zendesk::Rules::View::ViewQueue.new(self, user, options).pop
  end

  def strip_invalid_output_columns
    output.strip_invalid_output_columns!(account) if output
  end

  def table_does_not_exceed_maximum_columns
    return unless too_many_columns?

    errors.add(
      :base,
      "View table can contain a maximum of #{MAX_TABLE_COLUMNS} columns. " \
        "You selected #{output.columns.size}."
    )
  end

  # TODO: remove silence_warnings call under ruby 1.9, #type is no longer a
  # native method there.
  def too_many_columns?
    output &&
      silence_warnings { output.type } == 'table' &&
      output.columns.size > MAX_TABLE_COLUMNS
  end

  def watchability_for_filtered_view
    return unless watchability_enabled? && skill_based_filtered_view?

    return if watchable?

    errors.add(
      :base,
      I18n.t(
        'txt.admin.models.rules.rule.skill_based_view.unsupported_properties'
      )
    )
  end

  def watchability_enabled?
    account.has_skill_based_view_filters?
  end

  def skill_based_filtered_view?
    account.settings.skill_based_filtered_views.include?(id)
  end

  private

  def update_associated_groups
    return unless write_group_views? && groups_changed?

    groups_views.each(&:mark_for_destruction)

    return unless group_view?

    current_group_owner_ids.each do |group_id|
      groups_views.build do |group_view|
        group_view.account_id = account_id
        group_view.group_id   = group_id
      end
    end
  end

  def groups_changed?
    group_view? &&
      (
        group_owner_ids.present? && group_owner_ids.sort != group_ids.sort ||
          group_owner_ids.blank? && owner_id_changed?
      )
  end

  def current_group_owner_ids
    group_owner_ids.present? ? group_owner_ids : [owner_id]
  end

  def group_view?
    owner_type == 'Group'
  end

  def write_group_views?
    group_view? && account.has_multiple_group_views_writes?
  end

  def hard_limit_for_rule
    HARD_LIMIT_FOR_VIEWS
  end

  def higher_table_limit?
    account.has_higher_views_table_limit?
  end
end
