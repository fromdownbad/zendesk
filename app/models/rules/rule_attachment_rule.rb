class RuleAttachmentRule < ActiveRecord::Base
  self.table_name = :rule_attachment_rules

  belongs_to :rule, touch: true
  belongs_to :rule_attachment

  attr_accessible
end
