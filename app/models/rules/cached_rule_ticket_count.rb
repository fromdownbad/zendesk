require 'occam_client/client'

class CachedRuleTicketCount
  include Comparable

  attr_accessor :user,
    :rule,
    :rule_id,
    :value,
    :updated_at,
    :update_requested_at,
    :execution_time,
    :exactness_key,
    :time_based_rule,
    :request_options

  delegate :<=>, to: :value

  BACKGROUND_COUNT_REQUEST = 'background-count'.freeze

  def initialize(attributes = {})
    self.user                = attributes[:user]
    self.value               = attributes[:value]
    self.updated_at          = attributes[:updated_at]
    self.update_requested_at = attributes[:update_requested_at]
    self.execution_time      = attributes[:execution_time]
    self.exactness_key       = attributes[:exactness_key]
    self.time_based_rule     = attributes[:time_based_rule]

    attributes[:options] ||= {}
    attributes[:options][:caller] ||= caller_option
    self.request_options = attributes[:options]

    @key = attributes[:cache_key]

    if attributes[:rule].is_a?(Rule)
      self.rule    = attributes[:rule]
      self.rule_id = rule.id
    else
      self.rule_id = attributes[:rule_id] || attributes[:rule].to_i
    end
  end

  def rule # rubocop:disable Lint/DuplicateMethods
    @rule ||= user.account.rules.find_by_id(rule_id)
  end

  def zero?
    value.nil? || value.zero?
  end

  def age
    updated_at ? Time.now - updated_at : 24.hours
  end

  def max_age
    return 1.hour if time_based_rule

    adjusted_value = [value || 0, 1].max

    cache_time = begin
      [adjusted_value**1.25, ((execution_time || 0) * 100)**2].max.seconds
    end

    cache_time *= 2

    [cache_time, 1.hour + (30.minutes * rand)].min
  end

  def stale?
    # ZD:268616 the count should not be considered stale just because it is
    # being updated; we want to frequently refresh some counts, but we do not
    # want the ui to reflect that the count is continually "not fresh"
    is_updating? && age > 30.seconds
  end

  def needs_refreshing?
    !exact? && age > max_age
  end

  def fresh?
    !needs_refreshing?
  end

  def exact?
    exactness_key == current_exactness_key
  end

  def current_exactness_key
    user.account.scoped_cache_key(:views)
  end

  def time_based_rule # rubocop:disable Lint/DuplicateMethods
    @time_based_rule ||= rule && rule.is_time_based?
  end

  def is_updating? # rubocop:disable Naming/PredicateName
    update_requested_at.present? && (update_requested_at > 10.minutes.ago)
  end

  def update_delayed
    self.update_requested_at = Time.now
    save

    RuleTicketCountJob.enqueue(user.account_id, rule_id, user.id, false, request_options.merge(caller: BACKGROUND_COUNT_REQUEST))
  end

  def update_now(fallback_after: nil)
    if value && fallback_after && Rails.cache.read("#{key}/timing_out")
      update_delayed

      return value
    end

    # can't fallback to cached value if there's nothing there
    fallback_after = nil unless value

    RuleTicketCountJob.new(rule, user, self, false, request_options).perform(fallback_after)
  rescue Occam::Client::QueryTimeoutError => e
    Rails.cache.write("#{key}/timing_out", true, expires_in: 1.hour)

    unless fallback_after
      raise e unless user.account.has_occam_slow_rule_handler?

      self.value = ::View::BROKEN_COUNT
    end

    update_delayed

    value
  end

  def save
    self.exactness_key = value ? current_exactness_key : nil

    Rails.cache.write(key, to_memcached_hash) unless rule && rule.new_record?
  end

  def key
    @key ||= self.class.key_for(rule, user)
  end

  def to_memcached_hash
    {
      updated_at:          updated_at,
      value:               value,
      update_requested_at: update_requested_at,
      execution_time:      execution_time,
      exactness_key:       exactness_key,
      time_based_rule:     time_based_rule
    }
  end

  def as_json
    {
      rule_id:   rule_id,
      ruleId:    rule_id,
      value:     pretty_print,
      raw_value: value,
      fresh:     !stale?
    }
  end

  def reload
    initialize((Rails.cache.read(key) || {}).merge(user: user, rule: rule))
  end

  def pretty_print
    return '...' if value.nil? || value == ::View::BROKEN_COUNT

    if exact? || age <= 1.minute || (age <= 5.minute && value < 10)
      value.to_s
    elsif age <= 5.minute || value < 10
      "~#{value}"
    elsif age <= 10.minutes || value < 100
      "~#{(value.to_f / 10).round * 10}"
    else
      "~#{(value.to_f / 100).round * 100}"
    end
  end

  def caller_option
    'inband-count'
  end

  class << self
    def create(attributes = {})
      instance = new(attributes)
      instance.save
      instance
    end

    def lookup(rules, user, options = {})
      if rules.is_a?(Array) || rules.is_a?(ActiveRecord::Relation)
        lookup_every(rules, user, options)
      elsif rules.is_a?(Hash)
        lookup_every_by_cache_key(rules, user, options)
      else
        lookup_one(rules, user, options)
      end
    end

    def lookup_every_by_cache_key(rules_to_key_hash, user, options = {})
      key_value_map = Rails.cache.read_multi(*rules_to_key_hash.values)

      rules_to_key_hash.map do |rule_id, cache_key|
        instance = build_or_create(key_value_map[cache_key], rule_id, user, options)

        conditional_refresh_of(instance, options)

        instance
      end
    end

    def lookup_every(rules, user, options = {})
      keys          = rules.map { |rule| key_for(rule, user) }
      key_value_map = Rails.cache.read_multi(*keys)

      [].tap do |instances|
        keys.each_with_index do |key, index|
          instance = build_or_create(key_value_map[key], rules[index], user, options)

          instances << instance

          conditional_refresh_of(instance, options)
        end

        if options[:suspended] == true
          instances << suspended_tickets_rule_count(user)
        end

        if options[:deleted] == true
          instances << deleted_tickets_rule_count(user)
        end
      end
    end

    def lookup_one(rule, user, options = {})
      instance = begin
        build_or_create(Rails.cache.read(key_for(rule, user)), rule, user, options)
      end

      conditional_refresh_of(instance, options)

      instance
    end

    def key_for(rule, user)
      "#{rule.cache_key_for(user)}/count"
    end

    private

    def deleted_tickets_rule_count(user)
      count = Ticket.with_deleted do
        user.account.deleted_tickets.capped_count(5_000)
      end
      custom_rule_count(user, :deleted, count)
    end

    def suspended_tickets_rule_count(user)
      count = user.account.suspended_tickets.capped_count(5_000)
      custom_rule_count(user, :suspended, count)
    end

    def custom_rule_count(user, name, ticket_count)
      create(
        user: user,
        rule_id: name.to_s,
        value: ticket_count,
        cache_key: "views/#{name}-#{Time.now.strftime('%Y%m%d%H%M%S')}/count"
      )
    end

    def conditional_refresh_of(instance, options)
      case options[:refresh]
      when true
        instance.update_now if !instance.exact? || options[:force]
      when :delayed
        instance.update_delayed unless instance.fresh? || instance.is_updating?
      when nil
        instance.update_now unless instance.fresh?
      end
    end

    def build_or_create(hash, rule, user, options = {})
      if hash
        new(hash.merge(user: user, rule: rule, options: options))
      else
        create(user: user, rule: rule, options: options)
      end
    end
  end
end
