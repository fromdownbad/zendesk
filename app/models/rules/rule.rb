class Rule < ActiveRecord::Base
  extend Zendesk::FetchAppsRequirements

  include ScopedCacheKeys
  include Zendesk::Rules::Match
  include Zendesk::Rules::Query
  include Zendesk::Rules::CsvExport
  include Zendesk::Rules::SystemRule
  include Zendesk::Rules::CmsReferences
  include Zendesk::Serialization::RuleSerialization
  include Zendesk::ScopeExtensions
  include CachingObserver
  include PrecreatedAccountConversion

  has_soft_deletion default_scope: true

  include CIA::Auditable

  audit_attribute :title,
    :definition,
    :is_active,
    :owner_id,
    :owner_type

  after_soft_delete :record_deletion_audit

  ignore_column :sla_id

  APM_SERVICE_NAME = 'classic-rules'.freeze

  DEFAULT_POSITION = 9_999

  GROUP_OWNER_ID = 256**8 / 2 - 1

  SUBTYPES = %w[View UserView Trigger Automation Macro MobileView].freeze

  MAX_DEFINITION_SIZE       = 65_535
  MAX_TITLE_LENGTH          = 255
  MAX_DESCRIPTION_LENGTH    = 512
  MAX_EVENT_VALUE_REFERENCE = 8_192

  DEPRECATION_MAP = {
    %w[assignee_id#all_agents not_value assignee_id] =>
      ['assignee_id', 'not_changed', []],
    %w[assignee_id#all_agents value assignee_id] =>
      ['assignee_id', 'changed', []]
  }.freeze

  SOFT_LIMIT_FOR_TRIGGERS    = 3500
  SOFT_LIMIT_FOR_AUTOMATIONS = 1000
  SOFT_LIMIT_FOR_MACROS      = 40_000
  SOFT_LIMIT_FOR_VIEWS       = 5500
  SOFT_LIMIT_FOR_USER_VIEWS  = 15_000

  RULES_TABLE_LIMITS = {
    Automation: SOFT_LIMIT_FOR_AUTOMATIONS,
    Macro:      SOFT_LIMIT_FOR_MACROS,
    Trigger:    SOFT_LIMIT_FOR_TRIGGERS,
    View:       SOFT_LIMIT_FOR_VIEWS,
    UserView:   SOFT_LIMIT_FOR_USER_VIEWS,
    MobileView: SOFT_LIMIT_FOR_VIEWS
  }.with_indifferent_access.freeze

  # Translation keys for each type of rule
  # used in table limit error message in error_message.yml
  TABLE_LIMIT_MESSAGE = {
    Automation: 'automations',
    Macro:      'macros',
    Trigger:    'triggers',
    View:       'views',
    UserView:   'customer_list'
  }.with_indifferent_access.freeze

  CSV_THRESHOLDS = {development: 5, master: 20, staging: 50, other: 500}.freeze

  CSV_TITLES = {
    nice_id:              'id',
    updated_by_type:      'Latest updater type (agent/end-user)',
    type:                 'Ticket type',
    assignee_updated_at:  'Latest update by assignee',
    assignee:             'Assignee',
    created:              'Request date',
    solved:               'Solved date',
    requester_updated_at: 'Latest update by requester',
    updated:              'Latest update',
    assigned:             'Assigned date'
  }.freeze

  belongs_to :owner, polymorphic: true
  belongs_to :account
  belongs_to :group, inherit: :account_id
  belongs_to :user
  belongs_to :rule_category, foreign_key: :rules_category_id

  has_many(
    :rule_ticket_joins,
    -> { group(:ticket_id) },
    class_name: 'TicketJoin'
  ) do
    def tickets(page: 1, per_page: 30, conditions: {})
      proxy_owner = proxy_association.owner

      joins = proxy_owner.rule_ticket_joins.tickets_for(proxy_owner.account_id)

      all_conditions = {
        id: joins.map(&:ticket_id),
        account_id: proxy_owner.account_id
      }.merge(conditions)

      Ticket.where(all_conditions).paginate(
        page: page,
        per_page: per_page
      )
    end
  end

  has_many :rule_references, class_name: 'RuleReference', dependent: :destroy, inherit: :account_id

  has_one :resource_collection_resource, as: :resource, inherit: :account_id

  has_one :simplified_email_rule_bodies, class_name: 'SimplifiedEmailRuleBody', dependent: :destroy, inherit: :account_id

  attr_accessible :account,
    :current_user,
    :definition,
    :description,
    :feature_identifier,
    :is_active,
    :output,
    :owner,
    :owner_id,
    :owner_type,
    :per_page,
    :position,
    :rules_category_id,
    :title

  before_validation :ensure_valid_definition
  before_validation :ensure_valid_title_length
  before_validation :reset_per_page
  before_validation :set_account_user_and_group_from_owner

  validates_presence_of :account_id

  validate :ensure_title_is_present
  validate :ensure_requester_is_not_misused
  validate :run_new_definition_validations
  validate :ensure_status_id_in_range
  validate :other_definition_validations
  validate :invalid_recipient_email
  validate :validate_force_key
  validate :ensure_valid_definition_size
  validate :ensure_valid_description_length

  before_save :set_account_user_and_group_from_owner
  before_save :verify_table_limit

  before_create :set_default_position

  after_save :create_cms_references

  serialize :definition
  serialize :output

  attr_accessor :app_installation,
    :current_user,
    :cached_by_throttled_rule_ticket_finder,
    :cached_by_hanlon,
    :request_origin

  scope :active,   -> { where(is_active: true) }
  scope :inactive, -> { where(is_active: false) }
  scope :ordered,  -> { order('position ASC') }

  scope :with_ids, ->(ids)  { where(id: ids) }
  scope :type,     ->(type) { where(type: type) }

  scope :owned_by, ->(*owner_types) {
    where(owner_type: Array.wrap(owner_types).map(&:to_s))
  }

  scope :account_owned, ->(account_id) {
    where(owner_type: 'Account', owner_id: account_id)
  }

  scope :with_title, ->(query) {
    where('lower(title) LIKE ?', "%#{query.downcase}%")
  }

  scope :in_group, ->(group_ids) {
    where(owner_type: 'Group', owner_id: [*group_ids])
  }

  def self.group_owner_id
    GROUP_OWNER_ID
  end

  def self.csv_threshold
    CSV_THRESHOLDS[Rails.env.to_sym] || CSV_THRESHOLDS[:other]
  end

  def self.find_for_user(user, id, force_all: false)
    user.account.rules.find_by_id(id) ||
      user.rules.find_by_id(id) ||
      find_for_user_by_group(user, id, force_all) ||
      find_for_user_by_assume(user, id)
  end

  def self.with_ordered_ids(ids)
    with_ids(ids).index_by(&:id).slice(*ids).values
  end

  def self.update_position(ids)
    where(id: ids).tap do |rules|
      rules.update_all(
        ['updated_at = ?, position = FIELD(id, ?)', Time.now, ids]
      )

      CachingObserver.expire_fragments_on_save_or_destroy(rules.first)
    end
  end

  def self.apps_installations(account, ids)
    Zendesk::AppMarketClient.new(account.subdomain).installations(ids)
  end

  def self.find_for_user_by_group(user, id, force_all)
    in_group(
      if force_all || user.is_admin?
        user.account.all_group_ids
      else
        user.group_ids
      end
    ).find_by_id(id)
  end

  # If the rule requested is a personal rule, and the requester is someone with
  # permission to assume the owner of the rule, then it's okay to show the rule
  def self.find_for_user_by_assume(user, id)
    rule = Rule.where(
      account_id: user.account_id,
      owner_type: User.name,
      id:         id
    ).first

    rule if rule.present? && user.can?(:assume, rule.owner)
  end

  private_class_method :find_for_user_by_group,
    :find_for_user_by_assume

  def usage
    @usage ||= Zendesk::Rules::UsageCount.new(self)
  end

  def rule_name
    @rule_name ||= self.class.name.downcase
  end

  def rule_type
    @rule_type ||= self.class.name.downcase
  end

  def app_installation # rubocop:disable Lint/DuplicateMethods
    @app_installation ||= begin
      Rule.apps_installations(account, [id]).fetch(id) do
        Zendesk::Rules::NullAppInstallation
      end
    end
  end

  def rendered_title
    @rendered_title ||= begin
      if dc_in_title_enabled?
        Zendesk::Liquid::DcContext.render(title, account, current_user)
      else
        title
      end
    end
  end

  def via_reference
    {via_id: ViaType.RULE, via_reference_id: id}
  end

  def dc_in_title_enabled?
    return false unless account.present?

    is_a?(Macro) ? account.has_dc_in_macro_titles? : true
  end

  def lotus_permalink
    "#{account.url(mapped: false, ssl: true)}#{lotus_relative_link}"
  end

  def lotus_relative_link
    "/agent/admin/#{rule_type.pluralize}/#{id}"
  end

  def account_with_owner_backstop
    if owner.present? && !account_without_owner_backstop.present?
      owner.is_a?(Account) ? owner : owner.account
    else
      account_without_owner_backstop
    end
  end
  alias_method :account_without_owner_backstop, :account
  alias_method :account, :account_with_owner_backstop

  def set_account_user_and_group_from_owner
    return unless owner.present? && owner_id_changed?

    unless account_without_owner_backstop.present?
      self.account = owner.is_a?(Account) ? owner : owner.account
    end

    self.group ||= owner if owner.is_a?(Group)
    self.user  ||= owner if owner.is_a?(User)
  end

  def owner_with_set_account=(new_owner)
    self.owner_without_set_account = new_owner

    set_account_user_and_group_from_owner
  end
  alias_method :owner_without_set_account=, :owner=
  alias_method :owner=, :owner_with_set_account=

  def shared?
    !owner.is_a?(User)
  end

  def execute(ticket, events = nil)
    ticket.reset_delta_changes!

    apply_actions(ticket) if is_a?(Macro) || match?(ticket, events)

    # Convert trigger/macro applied ticket attribute changes to Change events on
    # the audit and return the events
    ticket.delta_changes.to_a.each_with_object([]) do |change, changes|
      Change.new(
        via_id:           ViaType.RULE,
        via_reference_id: id,
        transaction:      change,
        audit:            ticket.audit
      ).tap do |event|
        [changes, ticket.audit.events].each do |event_collection|
          event_collection << event
        end
      end
    end
  end

  def apply_actions(ticket)
    Zendesk::Rules::ActionExecutor.execute(self, ticket, definition.actions)
  end

  def copy
    self.class.new do |copy|
      copy.owner     = owner
      copy.title     = I18n.t(
        'txt.admin.models.rules.rule.copy_of_label',
        rule_title: CGI.escapeHTML(title)
      )
      copy.is_active = is_active
      copy.per_page  = per_page
    end
  end

  def toggle_activation
    Rails.logger.info(
      "Logging trigger state: before: (#{is_active?}) after: (#{!is_active?})"
    )

    is_active? ? deactivate : activate
  end

  def state
    is_active? ? 'activated' : 'deactivated'
  end

  def cia_changes
    changes.dup.tap do |changes|
      # Starting with Rails 4, new records have things like output => [nil, nil]
      # on create
      changes.each do |attribute, values|
        changes.delete(attribute) if values == [nil, nil]
      end

      if changes['definition'].present?
        changes['definition'] = cia_definition_changes
      end
    end
  end

  # deprecate old, default rule conditions
  def prepare_for_editing!
    # an invalid rule can sometimes show up with a string as the definition...
    return unless definition.respond_to?(:conditions_all)

    [definition.conditions_all, definition.conditions_any].each do |cond_array|
      cond_array.each_with_index do |cond, i|
        # these should all be arrays. it makes me mad they're not
        cond.value = [cond.value.to_s] unless cond.value.is_a?(Array)

        if cond.source == 'recipient'
          cond.value[0] = fix_recipient_email(cond.value[0])
        end

        new_definition = DEPRECATION_MAP[
          [cond.source, cond.operator, cond.value.join]
        ]

        if new_definition.present?
          cond_array[i] = DefinitionItem.new(*new_definition)
        end

        if cond.source == 'group_id' && cond.value.join == 'group_id'
          # nonsensical conditions we used to support
          cond_array[i] = nil
        end

        field_id = cond.source.match(/^ticket_fields_(\d+)$/).try(:[], 1)

        # Ignore conditions that refer to non-existing Ticket Fields. See
        # https://support.zendesk.com/agent/tickets/1102253.
        if field_id.present? && !TicketField.exists?(field_id)
          cond_array[i] = nil
        end
      end

      cond_array.compact!
    end
  end

  def is_apps_requirement? # rubocop:disable Naming/PredicateName
    false
  end

  private

  def definition_list(condition_type)
    context = Zendesk::Rules::Context.new(
      account,
      current_user,
      rule_type:      rule_type,
      condition_type: condition_type,
      component_type: :condition
    )

    [context, ZendeskRules::Definitions::Conditions.definition_list(context)]
  end

  def fix_recipient_email(email)
    return email if email.include?('@')

    "#{email}@#{account.host_name(mapped: false)}"
  end

  def cia_definition_changes
    [definition_was, definition].map do |definition|
      definition.presence.try(:serialize_for_cia)
    end
  end

  def log_rule_execution_timing(scope, count, cpu_time, caller, options = {})
    logger.info do
      [
        "#{scope}: #{self.class}",
        "id: #{id}",
        "account_id: #{account_id}",
        "current_user: #{current_user.try(:id).inspect}",
        "count: #{count.inspect}",
        "cpu_time: #{cpu_time}",
        "conditions_all_length: #{definition.conditions_all.length}",
        "conditions_any_length: #{definition.conditions_any.length}",
        "now: #{Time.now.utc.strftime('%T')}",
        "shard_id: #{account.shard_id}",
        "caller: #{caller}",
        "basic_auth: #{options[:basic_auth]}.inspect",
        "cached: #{options[:cached].inspect}",
        "hanlon_cached: #{options[:hanlon_cached].inspect}",
        "page: #{options[:page].inspect}",
        "per_page: #{options[:per_page].inspect}",
        "order: #{options[:order].inspect}",
        "group: #{options[:group].inspect}",
        "db_host: #{options[:db_host].inspect}"
      ].join(', ')
    end
  end

  def set_default_position
    return if position.present?

    self.position = DEFAULT_POSITION

    move_to_last_position
  end

  def move_to_last_position
    max_position = begin
      self.class.
        where('id != ? AND account_id = ?', id, account_id).
        maximum(:position)
    end

    return unless max_position.present?

    self.position = max_position + 1
  end

  def activate
    self.is_active = true

    move_to_last_position
  end

  def deactivate
    self.is_active = false
  end

  def ensure_valid_definition
    return if definition.is_a?(Definition)

    self.definition = nil

    errors.add(
      :base,
      I18n.t(
        'txt.models.rules.rule.invalid_conditions_you_must_select_condition'
      )
    )

    false
  end

  def ensure_valid_title_length
    return unless title.to_s.length > MAX_TITLE_LENGTH

    errors.add(
      :base,
      I18n.t('txt.models.rules.rule.invalid_title_exceeds_character_limit')
    )
  end

  # can be removed once new rule code is 100% on.
  def ensure_status_id_in_range
    conditions = begin
      (definition.conditions_all.to_a + definition.conditions_any.to_a).compact
    end

    if status_less_than_new?(conditions)
      errors.add(
        :base,
        I18n.t('txt.models.rules.rule.status_less_than_new_invalid')
      )
    end

    if status_greater_than_closed?(conditions)
      errors.add(
        :base,
        I18n.t('txt.models.rules.rule.status_greater_than_closed_invalid')
      )
    end

    return unless status_cannot_be_reset?

    errors.add(
      :base,
      I18n.t('txt.admin.models.rules.rule.status_cannot_be_reset')
    )
  end

  def status_less_than_new?(conditions)
    conditions.find do |condition|
      condition.source == 'status_id' &&
        condition.operator == 'less_than' &&
        condition.first_value.to_i == Zendesk::Types::StatusType.NEW
    end
  end

  def status_greater_than_closed?(conditions)
    conditions.find do |condition|
      condition.source == 'status_id' &&
        condition.operator == 'greater_than' &&
        condition.first_value.to_i == Zendesk::Types::StatusType.CLOSED
    end
  end

  def status_cannot_be_reset?
    (is_a?(Trigger) || is_a?(Automation)) &&
      definition.actions.any? do |action|
        action.source == 'status_id' &&
          action.first_value.to_i == Zendesk::Types::StatusType.NEW
      end
  end

  def run_new_definition_validations
    definition.run_new_validations(account, type, self, current_user)
  end

  def other_definition_validations
    ensure_current_user_can_edit

    ensure_query_is_not_too_complex     if is_a?(View) || is_a?(Automation)
    ensure_correct_notification_actions if is_a?(Trigger) || is_a?(Automation)

    ensure_properly_formatted_liquid if definition.actions.any?
  end

  def ensure_current_user_can_edit
    return if current_user_can_edit?

    errors.add(
      :base,
      "#{rule_name.capitalize} cannot be created/updated by you"
    )
  end

  def current_user_can_edit?
    @current_user.nil? || @current_user.can?(:edit, self)
  end

  def ensure_query_is_not_too_complex
    if definition.missing_required_condition?
      if is_a?(Automation)
        errors.add(
          :base,
          I18n.t('txt.admin.models.rules.rule.require_properties_error_automation')
        )
      else
        errors.add(
          :base,
          I18n.t('txt.admin.models.rules.rule.at_least_one_required_error_view')
        )
      end
    end

    return unless too_many_description_word_conditions?

    errors.add(
      :base,
      I18n.t('txt.admin.models.rules.rule.no_more_than_two_description_view')
    )
  end

  def too_many_description_word_conditions?
    (
      definition.conditions_all.to_a + definition.conditions_any.to_a
    ).compact.select do |condition|
      condition.source == 'description_includes_word' &&
        condition.operator.index('includes')
    end.size > 2
  end

  def ensure_correct_notification_actions
    if definition.actions.empty?
      errors.add(
        :base,
        I18n.t("txt.admin.models.rules.rule.at_least_one_action_#{rule_name}")
      )
    elsif notification_email_too_large?
      errors.add(
        :base,
        I18n.t('txt.admin.models.rules.actions.notification_email_size_error')
      )
    end

    definition.actions.each do |action|
      case action.source
      when 'notification_user'
        if [action.value].flatten.first == 'inactive_agent'
          errors.add(
            :base,
            I18n.t('txt.admin.models.rules.rule.inactive_agent')
          )
        end

        ensure_present_notification_actions(action)
      when 'notification_target'
        ensure_valid_url_target_parameters(action)
        ensure_valid_notification_target_length(action)
      when 'notification_group'
        ensure_present_notification_actions(action)
      end
    end
  end

  def notification_email_too_large?
    definition.actions.any? do |action|
      %w[notification_user notification_group deflection].include?(action.source) &&
        action.value[2].to_s.bytes.count > MAX_EVENT_VALUE_REFERENCE
    end
  end

  def ensure_valid_url_target_parameters(action)
    return unless action.value[1].is_a?(Array) &&
                  action.value[1].any? { |key, _| key.blank? }

    errors.add(
      :base,
      I18n.t('txt.error_message.rules.rule.invalid_url_target_parameters')
    )
  end

  def ensure_valid_notification_target_length(action)
    return unless action.value[1].to_s.length > MAX_EVENT_VALUE_REFERENCE

    errors.add(
      :base,
      I18n.t('txt.error_message.rules.rule.external_target_size_error')
    )
  end

  def ensure_present_notification_actions(action)
    return unless [action.value].flatten.first.blank?

    errors.add(
      :base,
      I18n.t("txt.admin.models.rules.rule.#{action.source}_cannot_be_blank")
    )
  end

  # Check for incorrect liquid placeholders
  def ensure_properly_formatted_liquid
    if definition.actions.any?
      definition.actions.each do |action|
        next unless [Array, String].include?(action.value.class)

        Array(action.value).each do |action_value|
          next if action_value.class != String

          Liquid::Template.parse(action_value)
        end
      end
    end
  rescue Liquid::SyntaxError => e
    errors.add(
      :base,
      "A template placeholder is in an invalid format: #{e.message}"
    )
  end

  def invalid_recipient_email
    return true if is_a?(Macro)
    return true unless conditions_detects_invalid_recipient.present?

    errors.add(
      :base,
      I18n.t(
        'txt.error_message.rules.rule.' \
          'invalid_email_address_for_ticket_received_at'
      )
    )
  end

  def conditions_detects_invalid_recipient
    return false unless all_and_any_conditions.present?

    all_and_any_conditions.find do |condition|
      condition.source == 'recipient' && invalid_email(condition.value[0])
    end
  end

  def invalid_email(email)
    email = email.clone

    if !email.include?('@') && support_incomplete_emails?
      email << "@#{account.host_name(mapped: false)}"

      message = "invalid email for recipient in rules #{account_id},#{id}."

      ZendeskExceptions::Logger.record(
        StandardError.new(message),
        location: self.class,
        message: message,
        reraise: !Rails.env.production?,
        fingerprint: '5e36fb1ff4490cd37922391d71224d662221f024'
      )
    end

    !Zendesk::Mail::Address.valid_address?(email)
  end

  def support_incomplete_emails?
    Rails.env.production? &&
      !new_record? &&
      created_when_we_supported_incomplete_emails?
  end

  def created_when_we_supported_incomplete_emails?
    created_at.utc.to_date < Date.new(2013, 4, 18)
  end

  def all_and_any_conditions
    @all_and_any_conditions ||= begin
      definition.conditions_all.to_a + definition.conditions_any.to_a
    end
  end

  def ensure_title_is_present
    unless title.present?
      errors.add(
        :base,
        I18n.t('txt.view.form.error_message.title_needed')
      )
    end
  end

  def ensure_requester_is_not_misused
    conditions = definition.conditions_all.to_a + definition.conditions_any.to_a

    return unless requester_misused?(conditions)

    errors.add(
      :base,
      'The condition <strong>"Requester is not -"</strong> ' \
        'is not valid'.html_safe
    )
  end

  def requester_misused?(conditions)
    conditions.present? &&
      conditions.find do |condition|
        condition.source == 'requester_id' &&
          condition.operator == 'is_not' &&
          condition.value[0].blank?
      end
  end

  def ensure_valid_definition_size
    return unless @attributes['definition'].
      value_for_database.bytesize > MAX_DEFINITION_SIZE

    # this error message is misleading as it's not clear about the
    # serialization overhead, but it's the best we've got
    errors.add(
      :base,
      I18n.t(
        'activerecord.errors.messages.too_long',
        count: MAX_DEFINITION_SIZE
      )
    )
  end

  def ensure_valid_description_length
    return unless description.to_s.length > MAX_DESCRIPTION_LENGTH

    errors.add(
      :base,
      I18n.t(
        'txt.models.rules.rule.invalid_description_exceeds_character_limit'
      )
    )
  end

  def record_deletion_audit
    CIA.record(:destroy, self)
  end

  def verify_table_limit
    return unless table_limits_enabled?

    # Do not run the validation for SystemRules
    return if type.nil?

    return if account.rules.active.where(type: type).count < usage_limit

    # Don't allow activating a rule once the limit has been met
    if persisted?
      return unless is_active_change == [false, true]
    end

    Rails.logger.info 'Rules table limit exceeded: '\
      "account_id: #{account.id}, rule_type: #{type}, "\
      "current limit: #{usage_limit}"

    fail Zendesk::ProductLimit::TableLimitExceededError, I18n.t(
      'txt.admin.models.rules.rule.limit_exceeded',
      limit: usage_limit,
      rule_type: I18n.t(
        "txt.admin.models.rules.rule.rule_type.#{TABLE_LIMIT_MESSAGE[type]}"
      )
    )
  end

  def usage_limit
    higher_table_limit? ? hard_limit_for_rule : soft_limit_for_rule
  end

  def table_limits_enabled?
    account.has_apply_rules_table_limits?
  end

  def soft_limit_for_rule
    account.settings.business_rules_limit[type]
  end

  # Private: Specify hard table limit for particular rule
  # Any inheriting class must override this method
  def hard_limit_for_rule
    fail NotImplementedError
  end

  # Private: Specify if account is eligible for hard table
  # limit for particular rule
  # Any inheriting class must override this method
  def higher_table_limit?
    fail NotImplementedError
  end
end
