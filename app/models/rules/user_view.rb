class UserView < Rule
  ignore_column :sla_id

  belongs_to :account

  validates_presence_of :owner
  validates_presence_of :owner_type

  module OnboardingStates
    OFF   = '0'.freeze
    INTRO = '1'.freeze
    TOUR  = '2'.freeze
    DONE  = '3'.freeze
  end

  before_update :update_position_if_changing_active

  SAMPLING_THRESHOLD = 100_000
  SAMPLING_RANGE     = 10_000
  PRESENCE_OPERATORS = %w[is_present is_not_present].freeze

  HARD_LIMIT_FOR_USER_VIEWS  = 15_000

  USER_ATTRIBUTE_DEFINITIONS = {
    'name' => {
      'operators' => %w[is is_not],
      'type'      => 'text'
    },
    'created_at' => {
      'operators' => %w[is is_not less_than greater_than less_than_or_equal_to
                        greater_than_or_equal_to within_previous_n_days],
      'type'      => 'date'
    },
    'last_login' => {
      'operators' => %w[is is_not is_present is_not_present less_than
                        greater_than less_than_or_equal_to
                        greater_than_or_equal_to within_previous_n_days],
      'type'      => 'date'
    },
    'locale_id' => {
      'operators' => %w[is is_not],
      'type'      => 'dropdown'
    },
    'organization_id' => {
      'operators' => %w[is is_not is_present is_not_present],
      'type'      => 'dropdown'
    },
    'current_tags' => {
      'operators' => %w[includes not_includes],
      'type'      => 'tags'
    },
    'roles' => {
      'operators' => %w[is is_not],
      'type'      => 'dropdown'
    }
  }.freeze

  SPECIAL_CASE_TYPES = {
    'within_previous_n_days' => 'integer',
    'within_next_n_days'     => 'integer'
  }.freeze

  SPECIAL_CASE_TITLES = {
    'date' => {
      'greater_than'             => 'after',
      'less_than'                => 'before',
      'greater_than_or_equal_to' => 'after_or_on',
      'less_than_or_equal_to'    => 'before_or_on'
    },
    'tags' => {
      'includes'     => 'contain_at_least_one_of_the_following',
      'not_includes' => 'contain_none_of_the_following'
    }
  }.freeze

  CUSTOM_FIELD_OPERATORS = {
    'Integer'  => %w[is is_not is_present is_not_present less_than
                     greater_than less_than_or_equal_to
                     greater_than_or_equal_to],
    'Decimal'  => %w[is is_not is_present is_not_present less_than
                     greater_than less_than_or_equal_to
                     greater_than_or_equal_to],
    'Dropdown' => %w[is is_not is_present is_not_present],
    'Date'     => %w[is is_not less_than greater_than less_than_or_equal_to
                     greater_than_or_equal_to within_next_n_days
                     within_previous_n_days],
    'Text'     => %w[is is_not],
    'Textarea' => %w[is is_not],
    'Regexp'   => %w[is is_not],
    'Checkbox' => %w[is is_not]
  }.freeze

  VALID_EXECUTION_ATTRIBUTES = %w[
    name
    id
    email
    twitter
    facebook
    google
    organization_id
    current_tags
    locale_id
    roles
    created_at
    last_login
    external_id
    phone
    time_zone
  ].freeze

  VALID_EXECUTION_ATTRIBUTES_BY_TYPE = {
    'group' => %w[
      created_at
      last_login
      locale_id
      organization_id
      roles
      time_zone
    ],
    'sort' => %w[
      created_at
      external_id
      last_login
      locale_id
      name
      organization_id
      roles
      time_zone
    ]
  }.freeze

  def user_attribute_definitions
    @user_attribute_definitions ||= begin
      self.class.user_attribute_definitions(account)
    end
  end

  def self.user_attribute_definitions(account)
    if account.has_user_and_organization_tags?
      USER_ATTRIBUTE_DEFINITIONS
    else
      USER_ATTRIBUTE_DEFINITIONS.reject { |key, _| key == 'current_tags' }
    end
  end

  def self.valid_execution_attributes(account)
    if account.has_user_and_organization_tags?
      VALID_EXECUTION_ATTRIBUTES
    else
      VALID_EXECUTION_ATTRIBUTES - %w[current_tags]
    end
  end

  def self.valid_execution_attributes_by_type(account)
    VALID_EXECUTION_ATTRIBUTES_BY_TYPE.dup.tap do |attrs|
      attrs['columns'] = valid_execution_attributes(account)
    end
  end

  def self.operator_data(key, type)
    type = SPECIAL_CASE_TYPES[key] || type

    title_key = begin
      (SPECIAL_CASE_TITLES[type] && SPECIAL_CASE_TITLES[type][key]) || key
    end

    title = I18n.t("txt.admin.models.rules.rule_dictionary.#{title_key}_label")

    {title: title, type: type}
  end

  def self.user_attribute_title(attribute)
    I18n.t("txt.api.v2.user_views.columns.#{attribute}")
  end

  def self.definitions(account, user)
    Zendesk::UserViews::Definitions.new(account, user).build
  end

  def self.custom_field?(key)
    key.to_s.starts_with?('custom_fields.')
  end

  def update_position_if_changing_active
    return unless is_active_changed?

    self.position = account.user_views.maximum(:position) + 1
  end

  # When passed the key for a custom field, returns the name of the custom
  # field with the special prefix removed (e.g. 'custom_fields.test' is
  # converted to 'test'.)  If the argument is NOT a custom field, returns false.
  def self.custom_field_key(key)
    string_key = key.to_s

    custom_field?(string_key) && string_key[14, string_key.length]
  end

  def table
    :users
  end

  def entity
    User
  end

  def view_type
    'User'
  end

  def has_grouping? # rubocop:disable Naming/PredicateName
    has_ordering?(:group)
  end

  def has_sorting? # rubocop:disable Naming/PredicateName
    has_ordering?(:sort)
  end

  def has_ordering?(type) # rubocop:disable Naming/PredicateName
    output.present? &&
      output.send(type).present? &&
      output.send(type)[:id].present? &&
      output.send(type)[:order].present?
  end

  def custom_fields_by_key
    @custom_fields_by_key ||= begin
      account.user_custom_fields.each_with_object({}) do |field, accum|
        accum[field.key] = field
      end
    end
  end

  def field_from_source(source)
    account.find_user_custom_field_by_source(source)
  end

  def validator
    @validator ||= Zendesk::UserViews::Validator.new(self)
  end

  def ensure_valid_definition
    # `ensure_valid_definition` is a before_validation callback already (see
    # parent class Rule) adding method here to ensure execution order
    ensure_not_sorted_by_user_id

    validator.validate
  end

  def run_new_definition_validations
    true
  end

  private

  def ensure_not_sorted_by_user_id
    # TODO: remove this check after migration to remove sort by user id from
    # saved views runs in production.
    return unless has_sorting? && output.sort[:id].to_s == 'id'

    output.sort[:id] = 'created_at'
  end

  def hard_limit_for_rule
    HARD_LIMIT_FOR_USER_VIEWS
  end

  def higher_table_limit?
    false
  end
end
