class RuleDictionary
  SOURCES = {
    'current_via_id'                    => 'ticket_update_via_label',
    'via_id'                            => 'ticket_channel_label',
    'status_id'                         => 'status_label',
    'priority_id'                       => 'priority_label',
    'ticket_type_id'                    => 'type_label',
    'group_id'                          => 'group_label',
    'group_id#current_groups'           => 'group_label',
    'assignee_id'                       => 'assignee_label_cap',
    'assignee_id#all_agents'            => 'assignee_label_cap',
    'requester_id'                      => 'requester_label_cap',
    'requester_id#all_agents'           => 'requester_label_cap',
    'set_tags'                          => 'set_tags_label',
    'current_tags'                      => 'add_tags_label',
    'remove_tags'                       => 'remove_tags_label',
    'organization_id'                   => 'organization_label',
    'organization_name'                 => 'organization_label',
    'sla_next_breach_at'                => 'sla_next_breach_at_label',
    'role'                              => 'updater_label',
    'comment_is_public'                 => 'comment_is_label',
    'ticket_is_public'                  => 'ticket_is_public_label',
    'subject_includes_word'             => 'subject_text_label',
    'comment_includes_word'             => 'comment_text_label',
    'description_includes_word'         => 'description_label',
    'macro_id'                          => 'macro_reference_label',
    'macro_id_after'                    => 'macro_reference_label',
    'update_type'                       => 'ticket_is_label',
    'recipient'                         => 'ticket_was_received_at_label',
    'satisfaction_score'                => 'satisfaction_label',
    'resolution_time'                   => 'resolution_time_in_hours_label',
    'NEW'                               => 'hours_since_created_label',
    'OPEN'                              => 'hours_since_opened_label',
    'PENDING'                           => 'hours_since_pending_label',
    'HOLD'                              => 'hours_since_hold_label',
    'SOLVED'                            => 'hours_since_solved_label',
    'CLOSED'                            => 'hours_since_closed_label',
    'assigned_at'                       => 'hours_since_assigned_label',
    'updated_at'                        => 'hours_since_update_label',
    'requester_updated_at'              => 'hours_since_requester_update_label',
    'assignee_updated_at'               => 'hours_since_assignee_update_label',
    'due_date'                          => 'hours_since_due_date_label',
    'notification_user'                 => 'email_user_label',
    'notification_group'                => 'email_group_label',
    'notification_target'               => 'notify_target_label',
    'comment_value'                     => 'response_description_label',
    'comment_mode_is_public'            => 'comment_mode_label',
    'tweet_requester'                   => 'tweet_requester_label',
    'requester_twitter_followers_count' => 'twitter_followers_are_label',
    'requester_twitter_statuses_count'  => 'number_of_tweets_is_label',
    'requester_twitter_verified'        => 'is_verified_by_twitter_label',
    'set_schedule'                      => 'set_schedule_label',
    'collaboration_thread'              => 'collaboration_thread_label'
  }.each_with_object({}) do |(key, label), sources|
    sources.store(key, "txt.admin.models.rules.rule_dictionary.#{label}")
  end.freeze

  OPERATORS = {
    'is'                 => 'is_label',
    'is_not'             => 'is_not_label',
    'less_than'          => 'less_than_label',
    'greater_than'       => 'greater_than_label',
    'changed'            => 'changed_label',
    'value'              => 'changed_to_label',
    'value_previous'     => 'changed_from_label',
    'not_value'          => 'not_changed_to_label',
    'not_changed'        => 'not_changed_label',
    'not_value_previous' => 'not_changed_from_label',
    'includes'           => 'contains_at_leat_one_label',
    'not_includes'       => 'contains_none_label'
  }.each_with_object({}) do |(key, label), operators|
    operators.store(key, "txt.admin.models.rules.rule_dictionary.#{label}")
  end.freeze

  VALUES = {
    'true'                      => 'public_label',
    'false'                     => 'private_label',
    'not_relevant'              => 'present_label',
    'requester_can_see_comment' => 'present_and_requester_can_see_comment_label',
    'Create'                    => 'created_label',
    'Update'                    => 'updated_label',
    'assignee_id'               => 'assignee_label',
    'requester_id'              => 'requester_label',
    'current_user'              => 'current_user_label',
    'current_groups'            => 'current_groups_label',
    'group_id'                  => 'assigned_group_label',
    'all_agents'                => 'all_agents_label'
  }.each_with_object({}) do |(key, label), values|
    values.store(key, "txt.admin.models.rules.rule_dictionary.#{label}")
  end.freeze

  class << self
    def source_translation(attribute)
      lookup(SOURCES, attribute)
    end

    def operator_translation(attribute)
      lookup(OPERATORS, attribute)
    end

    def value_translation(attribute)
      lookup(VALUES, attribute)
    end

    def value_exceptions
      %w[changed not changed]
    end

    private

    def lookup(data, value)
      return unless (key = data[value])

      I18n.t(key)
    end
  end
end
