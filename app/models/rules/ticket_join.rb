class TicketJoin < ActiveRecord::Base
  self.table_name = :rule_ticket_joins

  belongs_to :account
  belongs_to :rule
  belongs_to :ticket

  attr_accessible :account, :rule, :rule_id, :ticket

  validates_presence_of :account_id
  validates_presence_of :rule_id
  validates_presence_of :ticket_id

  before_validation :inherit_account_id

  scope :tickets_for, ->(account_id) {
    select('DISTINCT ticket_id').
      where(account_id: account_id).
      order('updated_at DESC')
  }

  CLEANUP_SLEEP = 0.5
  CLEANUP_LIMIT = 500

  def self.cleanup
    Arsi.disable do
      loop do
        # delete_all doesn't support limit
        num_deleted = connection.delete(
          format(
            "DELETE from %s where created_at < '%s' LIMIT %d",
            table_name,
            8.days.ago.to_s(:db),
            CLEANUP_LIMIT
          )
        )

        break if num_deleted == 0

        sleep CLEANUP_SLEEP
      end
    end
  end

  def inherit_account_id
    self.account_id ||= ticket.account_id
  end
end
