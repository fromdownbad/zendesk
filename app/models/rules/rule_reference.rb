class RuleReference < ActiveRecord::Base
  belongs_to :rule
  belongs_to :item, polymorphic: true
  belongs_to :account

  attr_accessible :account, :rule, :item

  scope :macros,      -> { joins(:rule).where('rules.type = "Macro"') }
  scope :triggers,    -> { joins(:rule).where('rules.type = "Trigger"') }
  scope :automations, -> { joins(:rule).where('rules.type = "Automation"') }

  has_soft_deletion default_scope: true

  validates_presence_of :rule_id
  validates_presence_of :account_id
  validates_presence_of :item_id
end
