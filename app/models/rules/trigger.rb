require 'benchmark'

class Trigger < Rule
  ignore_column :sla_id

  NOTIFICATION_SOURCES = %w[notification_group notification_user].freeze
  REVISION_ATTRIBUTES  = %w[definition description is_active title].freeze

  APM_SERVICE_NAME = 'classic-trigger-execution'.freeze
  HARD_LIMIT_FOR_TRIGGERS = 7000

  has_many :revisions,
    -> { order(:nice_id) },
    foreign_key: :rule_id,
    class_name: 'TriggerRevision',
    dependent: :destroy,
    inherit: :account_id

  delegate :change_conditions?,
    :satisfaction_prediction_conditions?, to: :definition

  validates_presence_of :owner

  validate :ensure_has_conditions
  validate :ensure_valid_sms_notification_definitions
  validate :ensure_valid_rules_category_id, if: -> { categories_enabled? && rules_category_id }
  validate :ensure_valid_category, if: -> { categories_enabled? && category }
  validate :ensure_categories_exist, if: -> { categories_enabled? && !rules_category_id && !category }
  validate :ensure_category_params_exist, on: :update, if: -> { categories_enabled? }

  attr_accessor :current_user, :category
  attr_accessible :category

  include Zendesk::PreventCollectionModificationMixin

  before_save :construct_revision, if: :construct_revision?
  before_save :create_category_and_set_rules_category_id, if: -> { categories_enabled? && category }

  before_create :set_category_from_position, if: -> { categories_enabled? && !account.has_trigger_categories_break_triggers_endpoint? && !rules_category_id }
  before_create :set_default_category_position, if: -> { categories_enabled? && rules_category_id && !position }

  before_update :set_default_category_position, if: -> { categories_enabled? && rules_category_id_changed? && !position_changed? }

  def self.update_position_for_categories(account, values)
    expire_cache_rule = nil

    values.each do |category_id, ids|
      where(account: account, id: ids, rules_category_id: category_id).tap do |rules|
        rules.update_all(
          ['updated_at = ?, position = FIELD(id, ?)', Time.now, ids]
        )

        expire_cache_rule ||= rules.first
      end
    end

    CachingObserver.expire_fragments_on_save_or_destroy(expire_cache_rule)
  end

  # Overrides Rule.set_default_position behavior if categories are enabled
  def set_default_position
    super unless account.has_trigger_categories_api_enabled?
  end

  def create_category_and_set_rules_category_id
    rule_category.save!

    update_category_positions if category.dig(:position)

    self.rules_category_id = rule_category.id
    self.position = 1
  end

  def update_category_positions
    Zendesk::Rules::Categories::PositionUpdate.update_positions([rule_category], self.class.name, account)
  end

  def set_category_from_position
    if position.present?
      return set_to_last_category if position < 1 || position > account.active_triggers.count

      trigger_at_same_position = account.triggers_with_category_position.active.offset(position - 1).first
      if trigger_at_same_position.present?
        self.rules_category_id = trigger_at_same_position.rules_category_id
        self.position = trigger_at_same_position.position
      else
        set_to_last_category
      end
    else
      set_to_last_category
    end
  end

  def set_to_last_category
    self.rules_category_id = trigger_categories.order(position: :desc).first.id

    set_default_category_position
  end

  def set_default_category_position
    max_position = self.class.where(account: account, rules_category_id: rules_category_id).active.maximum(:position) || 0

    self.position = max_position + 1
  end

  def apply_and_collect_changes(ticket, _events)
    changes = []

    time = Benchmark.realtime do
      ticket.reset_delta_changes!

      if ticket_followers_allowed_enabled?
        pre_trigger_follower_addresses = ticket.follower_addresses
      end

      apply_actions(ticket)

      ticket.delta_changes.to_a.each do |change|
        next if should_omit?(change)

        event = change_event(ticket.audit, change)
        changes << event
        ticket.audit.events << event
      end

      if ticket_followers_allowed_enabled?
        current_addresses = ticket.follower_addresses
        should_create_follower_change_event = CollaborationChange.create?(
          account,
          pre_trigger_follower_addresses,
          current_addresses
        )

        if should_create_follower_change_event
          follower_change = FollowerChange.new.tap do |event|
            event.current_followers = current_addresses
            event.previous_followers = pre_trigger_follower_addresses
            event.via_id = via_reference[:via_id]
            event.via_reference_id = via_reference[:via_reference_id]
          end
          changes << follower_change
          ticket.audit.events << follower_change
        end
      end
    end

    time = (time * 1000).to_i

    if time > 200
      Rails.logger.warn(
        "Slow trigger id #{id} on account id #{account_id}: #{time}ms"
      )
    end

    changes
  end

  # Fires this trigger on the given ticket/events. The `fire` method returns a
  # tuple of the resulting status and the resulting change, if any. The possible
  # return values are:
  #
  # [:match, <change set>]     - this trigger matched and the changes are as
  #                              returned
  # [:aborted, nil]            - the trigger matched but changes could not be
  #                              applied
  # [:mismatch, nil]           - this trigger does not match the ticket in its
  #                              current form, but may do that later
  # [:permanent_mismatch, nil] - this trigger can never apply to the given
  #                              ticket
  def fire(ticket, events, run_state)
    status = nil
    changes = nil

    match_time = Benchmark.realtime do
      status = condition_match?(ticket, events, run_state)
    end

    run_state.match_time += match_time if run_state

    if status == :match
      apply_time = Benchmark.realtime do
        changes = apply_and_collect_changes(ticket, events)
      end

      run_state.apply_time += apply_time if run_state

      if changes == false
        status  = :aborted
        changes = nil
      end
    end

    [status, changes]
  end

  def notification_change?
    changes.keys == %w[definition] &&
      !conditions_change? &&
      definition.actions.one? &&
      notification_content_change?(
        *[definition_was, definition].
          map { |definition| definition.actions.first }
      )
  end

  def raw_definition
    account.on_shard do
      ActiveRecord::Base.connection.execute(
        'SELECT definition ' \
          'FROM rules ' \
          "WHERE id = #{id} AND account_id = #{account_id}"
      ).first.first
    end
  end

  def routing_action
    action_sources = definition.actions.map(&:source)
    matching_actions = action_sources & %w[group_id assignee_id]

    return 'both'                 if matching_actions.length > 1
    return matching_actions.first if matching_actions.length == 1

    false
  end

  def via_reference
    return super unless current_revision.present?

    {via_reference_id: current_revision.id, via_id: ViaType.RULE_REVISION}
  end

  private

  def ticket_followers_allowed_enabled?
    @ticket_followers_allowed_enabled ||= account.has_ticket_followers_allowed_enabled?
  end

  def should_omit?(change)
    return false unless account.has_follower_and_email_cc_collaborations_enabled?

    change[0] == 'current_collaborators'
  end

  # `followers_string` takes on one of the following format:
  #
  # 1) "Admin Extraordinaire <admin@zendesk.com>, Agent Ordinaire <agent@zendesk.com>"
  # 2) "admin@zendesk.com, agent@zendesk.com"
  #
  # To standarize the values for the `FollowerChange` audit event, only the
  # email is retained in the audit.

  def format_followers(followers_string)
    return [] if followers_string.nil?

    followers_string.scan(FIND_MULTIPLE_EMAIL_PATTERN).map do |email, _|
      email.downcase
    end.uniq
  end

  def followers_change?(change_type)
    change_type == 'current_collaborators' &&
      account.has_follower_and_email_cc_collaborations_enabled?
  end

  def conditions_change?
    definition_was.conditions_all != definition.conditions_all ||
      definition_was.conditions_any != definition.conditions_any
  end

  def notification_content_change?(original_action, current_action)
    NOTIFICATION_SOURCES.include?(original_action.source) &&
      original_action.source == current_action.source &&
      original_action.value.first == current_action.value.first
  end

  def ensure_has_conditions
    return if definition_conditions_present?

    errors.add(
      :base,
      I18n.t(
        'txt.admin.models.rules.rule.at_least_one_condition_' + rule_name
      )
    )
  end

  def definition_conditions_present?
    definition.conditions_all && definition.conditions_all.any? ||
      definition.conditions_any && definition.conditions_any.any?
  end

  def ensure_valid_sms_notification_definitions
    return unless invalid_sms_notification_definition_items?

    errors.add(
      :base,
      I18n.t('txt.admin.models.rules.rule.invalid_sms_notification')
    )
  end

  def invalid_sms_notification_definition_items?
    items = definition.actions.select do |a|
      %w[notification_sms_user notification_sms_group].include?(a.source)
    end

    items.any? do |item|
      !Zendesk::Rules::SmsNotificationAction.valid_definition_item?(item)
    end
  end

  def construct_revision
    revisions.build do |revision|
      revision.account_id = account.id
      revision.author_id  = current_user.try(:id) || User.system_user_id
      revision.nice_id    = (revisions.maximum(:nice_id) || 0).next
    end
  end

  def construct_revision?
    changes.keys.any? { |key| REVISION_ATTRIBUTES.include?(key) }
  end

  def current_revision
    @current_revision ||= revisions.last
  end

  # `change_diff` has the following format:
  # [
  #   "Admin Extraordinaire <admin@zendesk.com>",
  #   "Admin Extraordinaire <admin@zendesk.com>, Agent Ordinaire <agent@zendesk.com>"
  # ]

  def followers_change_event(change_diff)
    previous_followers, current_followers = change_diff

    formatted_current_followers = format_followers(current_followers)
    formatted_prev_followers = format_followers(previous_followers)

    # if the list of followers did not change, do not create a `FollowerChange`
    # audit event
    return if (formatted_current_followers - formatted_prev_followers).empty?

    FollowerChange.new.tap do |event|
      event.current_followers = formatted_current_followers
      event.previous_followers = formatted_prev_followers
      event.via_id = via_reference[:via_id]
      event.via_reference_id = via_reference[:via_reference_id]
    end
  end

  def change_event(audit, change)
    Change.new(
      transaction: change,
      audit: audit,
      **via_reference
    )
  end

  def hard_limit_for_rule
    HARD_LIMIT_FOR_TRIGGERS
  end

  def higher_table_limit?
    account.has_higher_triggers_table_limit?
  end

  def table_limits_enabled?
    account.has_apply_rules_table_limits_for_triggers?
  end

  def ensure_valid_rules_category_id
    found_rule_category = trigger_categories.find_by_id(rules_category_id)

    errors[:category_id] << I18n.t('txt.admin.models.rules.rule.rule_type.triggers_invalid_category_id') unless found_rule_category
  end

  def ensure_valid_category
    unless rule_category.valid?
      rule_category.errors.each do |_, message|
        errors.add(:category_name, message)
      end
    end
  end

  def ensure_categories_exist
    errors[:category] << I18n.t('txt.admin.models.rules.rule.rule_type.triggers_no_categories_exist') if trigger_categories.count.zero?
  end

  def ensure_category_params_exist
    if !rules_category_id && !category
      Rails.logger.error("No rules_category_id found for entity: #{self}")

      errors[:category] << I18n.t('txt.admin.models.rules.rule.rule_type.triggers_missing_category_params')
    end
  end

  def rule_category
    @rule_category ||= RuleCategory.new(category.merge(rule_type: Trigger.name, account: account))
  end

  def trigger_categories
    @trigger_categories ||= RuleCategory.trigger_categories(account)
  end

  def categories_enabled?
    account.has_trigger_categories_api_enabled?
  end
end
