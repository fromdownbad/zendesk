class MobileView < View
  SATISFACTION = 'satisfaction'.freeze
  GROUPS       = 'groups'.freeze
  UNSOLVED     = 'unsolved'.freeze
  INCOMING     = 'incoming'.freeze

  VIEW_COLUMNS = %i[
    status
    description
    requester
    updated_requester
    group
    assignee
  ].freeze

  before_validation :strip_invalid_output_columns

  validate :table_does_not_exceed_maximum_columns

  def self.satisfaction(current_user)
    MobileView.new(title: 'Satisfaction').tap do |view|
      view.owner = current_user

      view.output = Output.create(
        type:      'table',
        order:     :solved_at,
        order_asc: false,
        group:     nil,
        group_asc: false,
        columns:   VIEW_COLUMNS
      )

      view.definition = Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('assignee_id', 'is', current_user.id)
        )
        definition.conditions_any.push(
          DefinitionItem.new('satisfaction_score', 'is', SatisfactionType.GOOD)
        )
        definition.conditions_any.push(
          DefinitionItem.new('satisfaction_score', 'is', SatisfactionType.BAD)
        )
      end

      view.current_user = current_user
    end
  end

  def self.groups(current_user)
    MobileView.new(title: 'Groups').tap do |view|
      view.owner = current_user

      view.output = Output.create(
        type:      'table',
        order:     :updated_at,
        order_asc: false,
        group:     :status,
        group_asc: true,
        columns:   VIEW_COLUMNS
      )

      view.definition = Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('status_id', 'less_than', StatusType.PENDING)
        )
      end

      view.current_user = current_user
    end
  end

  def self.unsolved(current_user)
    MobileView.new(title: 'Unsolved').tap do |view|
      view.owner = current_user

      view.output = Output.create(
        type:      'table',
        order:     :updated_at,
        order_asc: false,
        group:     :status,
        group_asc: true,
        columns:   VIEW_COLUMNS
      )

      view.definition = Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('status_id', 'is', StatusType.OPEN)
        )
        definition.conditions_all.push(
          DefinitionItem.new('assignee_id', 'is', '')
        )
      end

      view.current_user = current_user
    end
  end

  def self.incoming(current_user)
    MobileView.new(title: 'Incoming').tap do |view|
      view.owner = current_user

      view.output = Output.create(
        type:      'table',
        group:     :priority,
        group_asc: false,
        order:     :updated_requester,
        order_asc: true,
        columns:   VIEW_COLUMNS
      )

      view.definition = Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('status_id', 'less_than', StatusType.PENDING)
        )
        definition.conditions_any.push(
          DefinitionItem.new('assignee_id', 'is', current_user.id)
        )
        definition.conditions_any.push(
          DefinitionItem.new('group_is_set_with_no_assignee', '', '')
        )
        definition.conditions_any.push(
          DefinitionItem.new('group_id', 'is', nil)
        )
      end

      view.current_user = current_user
    end
  end
end
