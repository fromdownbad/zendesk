class RuleCategory < ActiveRecord::Base
  SUPPORTED_RULE_TYPES = [Trigger].freeze

  RULE_CATEGORY_TABLE_LIMITS = {
    Trigger: 500
  }.with_indifferent_access.freeze

  self.table_name = :rules_categories

  attr_accessible :account, :name, :rule_type, :position, :active

  before_validation :set_position_if_missing

  before_save :verify_table_limit

  validates_presence_of :account, :name, :rule_type, :position
  validates_inclusion_of :rule_type, in: SUPPORTED_RULE_TYPES.map(&:name)

  belongs_to :account
  has_many :rules, foreign_key: :rules_category_id

  # Scopes, e.g. #RuleCategory.trigger_categories
  SUPPORTED_RULE_TYPES.each do |t|
    scope "#{t.name.underscore}_categories".to_sym, ->(account) { where(account: account, rule_type: t.name) }
  end

  def self.update_position(account, ids)
    where(account: account, id: ids).tap do |categories|
      categories.update_all(
        ['updated_at = ?, position = FIELD(id, ?)', Time.now, ids]
      )
    end
  end

  private

  def set_position_if_missing
    self.position = (RuleCategory.where(rule_type: rule_type, account: account).maximum(:position) || 0) + 1 if position.nil?
  end

  def usage_limit
    RULE_CATEGORY_TABLE_LIMITS[rule_type]
  end

  def verify_table_limit
    return unless usage_limit && RuleCategory.send("#{rule_type.downcase}_categories", account).count >= usage_limit

    Rails.logger.info 'RuleCategory table limit exceeded: '\
      "account_id: #{account.id} rule_type: #{rule_type} "\
      "current limit: #{usage_limit}"

    fail Zendesk::ProductLimit::TableLimitExceededError, I18n.t(
      "txt.admin.models.rules.rule_categories.#{rule_type.downcase}_category.limit_exceeded",
      limit: usage_limit
    )
  end
end
