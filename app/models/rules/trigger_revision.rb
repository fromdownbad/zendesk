class TriggerRevision < ActiveRecord::Base
  include PrecreatedAccountConversion

  self.table_name = :rule_revisions

  attr_accessible

  validates_uniqueness_of :nice_id, scope: :rule_id

  belongs_to :trigger, foreign_key: :rule_id
  belongs_to :author, class_name: 'User'
  belongs_to :account

  has_one :snapshot,
    foreign_key: :revision_id,
    class_name:  'TriggerSnapshot',
    dependent:   :destroy,
    inherit: :account_id

  before_create :construct_snapshot

  private

  def construct_snapshot
    build_snapshot do |snapshot|
      snapshot.account_id  = account_id
      snapshot.title       = trigger.title
      snapshot.description = trigger.description
      snapshot.definition  = trigger.raw_definition
      snapshot.is_active   = trigger.is_active
    end
  end
end
