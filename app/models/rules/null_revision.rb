class NullRevision
  NULL_SNAPSHOT = TriggerSnapshot.new.tap do |snapshot|
    snapshot.definition = Definition.new
  end
  private_constant :NULL_SNAPSHOT

  def self.nice_id
    nil
  end

  def self.snapshot
    NULL_SNAPSHOT
  end
end
