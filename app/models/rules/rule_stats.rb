# The class holds stats for rules in regards to certain condition or action
# sources Want to know which rules operate on a certain tag?
#   rules_stats.tag['hello'].rules => array of rule id's
#
# Want to know which rules operate on a certain group?
#   rules_stats.group_id[id.to_s].rules => array of rule id's

# Format is rules_stats.item[key]

class RuleStats
  SETS = {
    group_id:        'group_id',
    assignee_id:     'assignee_id',
    organization_id: 'organization_id',
    via_id:          'via_id',
    recipient:       'recipient',
    macro_id:        'macro_id'
  }.freeze

  NOTIFICATION_SETS = {
    notification_user:   'notification_user',
    notification_group:  'notification_group',
    notification_target: 'notification_target'
  }.freeze

  ALL_SET_KEYS = ([:tag] | SETS.keys | NOTIFICATION_SETS.keys).uniq.freeze

  attr_accessor(*ALL_SET_KEYS)

  def initialize(account)
    ALL_SET_KEYS.each do |key|
      instance_variable_set(:"@#{key}", {})
    end

    update_item_callback = update_item

    RuleStatsBuilder.new(account, :shared_views).
      stats.
      each(&update_item_callback)

    RuleStatsBuilder.new(account, :shared_macros).
      stats.
      each(&update_item_callback)

    RuleStatsBuilder.new(account, :triggers).
      stats.
      each(&update_item_callback)

    RuleStatsBuilder.new(account, :automations).
      stats.
      each(&update_item_callback)

    @sizes = {}

    calculate_sizes
  end

  def rules(item, property = :rules)
    send(item).collect { |_key, value| value.send(property) }.flatten.uniq
  end

  def size_of(item)
    @sizes[item.to_s] || 0
  end

  protected

  def update_item
    proc do |(rule_id, item, key, is_condition)|
      send(item)[key.to_s] = RuleStatItem.new if send(item)[key].nil?
      send(item)[key.to_s].rules << rule_id
      send(item)[key.to_s].condition_rules << rule_id if is_condition
      send(item)[key.to_s].action_rules << rule_id unless is_condition
    end
  end

  def calculate_sizes
    property = :rules

    ALL_SET_KEYS.each do |item|
      @sizes[item.to_s] = begin
        send(item).
          collect { |_key, value| value.send(property) }.
          flatten.
          uniq.
          size
      end
    end
  end
end

class RuleStatItem
  attr_accessor :rules,
    :condition_rules,
    :action_rules

  def initialize
    @rules = []
    @condition_rules = []
    @action_rules = []
  end
end
