class TriggerSnapshot < ActiveRecord::Base
  include PrecreatedAccountConversion

  attr_accessible

  belongs_to :revision, class_name: 'TriggerRevision'
  belongs_to :account

  serialize(
    :definition,
    Zendesk::Rules::Trigger::TriggerSnapshotSerialization.new
  )
end
