class CachedRulePreviewTicketCount < CachedRuleTicketCount
  def initialize(attributes = {})
    super

    self.rule    = attributes[:rule]
    self.rule_id = nil
  end

  def update_delayed
    self.update_requested_at = Time.now

    save

    RulePreviewTicketCountJob.enqueue(
      user.account_id,
      serialized_rule,
      user.id,
      rule.include_archived_tickets,
      request_options.merge(caller: BACKGROUND_COUNT_REQUEST)
    )
  end

  # fallback_after is not used but kept to be interchangeable with
  # CachedRuleTicketCount
  # rubocop:disable Lint/UnusedMethodArgument
  def update_now(fallback_after: nil)
    RulePreviewTicketCountJob.new(
      rule.definition,
      user,
      self,
      rule.include_archived_tickets,
      request_options
    ).perform
  end
  # rubocop:enable Lint/UnusedMethodArgument

  def serialized_rule
    YAML.dump(rule.definition)
  end

  def max_age
    return 1.hour if time_based_rule
    return 30.minutes unless execution_time && value

    # (Execution time from seconds to ms) ^ 2
    # 1800 = (1000x)^2; x = ~ 328ms
    adjusted_execution_time = (execution_time * 1000)**2

    # 2 ^ (value / 930)
    # 1800 = 2^(x / 930); x = ~ 10_000 tickets
    adjusted_value = 2**(value / 930)

    cached_time = [adjusted_execution_time, adjusted_value].max

    [cached_time.seconds, 30.minutes].min
  end

  def save
    super

    Rails.cache.write(key, to_memcached_hash)
  end

  def self.key_for(rule, user)
    "#{user.account.cache_key}/views/preview/#{rule.preview_id}/count" \
      "#{'_without_archive' unless rule.include_archived_tickets}"
  end
end
