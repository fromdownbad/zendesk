class RuleStatsBuilder
  attr_reader :name, :stats

  def initialize(account, name)
    @account = account
    @name    = name
    @stats   = []

    build_stats
  end

  def build_stats
    @account.send(name).find_each do |rule|
      conditions = begin
        rule.definition.conditions_all + rule.definition.conditions_any
      end

      actions = rule.definition.actions

      update_stats_for_definitions(rule.id, conditions, true)
      update_stats_for_definitions(rule.id, actions, false)
    end
  end

  def update_stats_for_definitions(rule_id, definitions, is_condition)
    # convert custom field conditions/actions to tag operations
    # Note, this does not work for custom field checkboxes, only dropdowns
    definitions.each do |definition|
      next unless definition.source.index('ticket_fields_')

      value = Array(definition.value).first

      next unless value.to_s.present?

      if (field_option_value = @account.fetch_custom_field_option_value(value))
        definition.source = 'current_tags'
        definition.value = field_option_value
      end
    end

    keys = begin
      definitions.
        select { |i| i.source.index('_tags') }.
        map(&:value).
        join(' ').
        split(/\ |\,/).
        uniq
    end

    keys.each do |key|
      @stats << [rule_id, :tag, key, is_condition]
    end

    definitions.each do |definition|
      RuleStats::SETS.each do |item, filter|
        seen_items = {}

        next unless definition.source.index(filter)

        keys = Array(definition.value).flatten
        keys.each do |key|
          next if seen_items[key]

          @stats << [rule_id, item, key, is_condition]

          seen_items[key] = true
        end
      end

      RuleStats::NOTIFICATION_SETS.each do |item, filter|
        seen_items = {}

        next unless definition.source.index(filter)

        keys = Array(definition.value.first).flatten
        keys.each do |key|
          next if seen_items[key]

          @stats << [rule_id, item, key, is_condition]

          seen_items[key] = true
        end
      end
    end
  end
end
