class RuleAttachment < ActiveRecord::Base
  LIMIT = 5

  private_constant :LIMIT

  include AttachmentMixin
  include Zendesk::Serialization::AttachmentSerialization
  include Zendesk::Attachments::Stores

  has_many :rule_attachment_rules, class_name: 'RuleAttachmentRule'
  has_many :rules, through: :rule_attachment_rules

  attr_accessible

  setup_attachment_fu(
    max_size: 50.megabytes.to_i,
    path_prefix: 'data/rule_attachments',
    processor: :mini_magick,
    thumbnails: {
      thumb: '80x80>'
    }
  )

  validates_as_attachment

  belongs_to :account
  belongs_to :user
  belongs_to :rule

  before_create :ensure_token
  before_create :generate_md5

  before_validation :set_thumbnail_inherits, prepend: true

  validates_presence_of :account_id
  validates_lengths_from_database only: %i[filename display_filename]

  def self.limit
    LIMIT
  end

  def associate(rule)
    rule_attachment_rules.create! do |rule_attachment_rule|
      rule_attachment_rule.account_id = rule.account_id
      rule_attachment_rule.rule_id = rule.id
      rule_attachment_rule.rule_attachment_id = id
    end
  end

  private

  def ensure_token
    self.token ||= Token.generate
  end

  def set_thumbnail_inherits
    return unless parent

    self.account_id = parent.account_id
    self.user_id    = parent.user_id
  end
end
