class Automation < Rule
  ignore_column :sla_id

  attr_accessor :current_user

  include Zendesk::PreventCollectionModificationMixin

  HARD_LIMIT_FOR_AUTOMATIONS = 1500

  # Container for result data for executed automations
  class Result
    attr_accessor :automation,
      :found,
      :changed,
      :select_time,
      :apply_time,
      :queued_audit_ids
    def initialize(
      automation,
      found            = 0,
      changed          = 0,
      select_time      = 0.0,
      apply_time       = 0.0,
      queued_audit_ids = []
    )
      self.automation   = automation
      self.found        = found
      self.changed      = changed
      self.select_time  = select_time
      self.apply_time   = apply_time
      self.queued_audit_ids = queued_audit_ids
    end

    def to_s
      format(
        'Automation id: %d, title: %s changed %d of %d tickets in %.3f ' \
          'seconds. View query: %.3f seconds.',
        automation.id,
        automation.title.inspect,
        changed,
        found,
        total_time,
        select_time
      ).tap do |string|
        if changed > 0
          string << begin
            format(
              ' Average apply time: %.3f seconds/ticket.',
              per_ticket_apply_time
            )
          end
        end
      end
    end

    def to_h
      {
        automation_id:         automation.id,
        automation_title:      automation.title,
        found:                 found,
        changed:               changed,
        total_time:            total_time,
        select_time:           select_time,
        apply_time:            apply_time,
        per_ticket_apply_time: per_ticket_apply_time
      }
    end

    def total_time
      select_time + apply_time
    end

    def per_ticket_apply_time
      apply_time / changed if changed > 0
    end
  end

  PROPERTY_FIELDS = %w[
    status_id
    priority_id
    ticket_type_id
    group_id
    assignee_id
    current_tags
    remove_tags
    set_tags
    satisfaction_score
    locale_id
  ].freeze

  CONDITION_FIELDS = %w[
    NEW
    OPEN
    PENDING
    HOLD
    SOLVED
    CLOSED
    assigned_at
    updated_at
    requester_updated_at
    assignee_updated_at
    due_date
    until_due_date
    sla_next_breach_at
    until_sla_next_breach_at
  ].freeze

  validates_presence_of :owner

  validate :validate_nullifying_actions, if: :account

  # This entry-point is deprecated; use AutomationExecutor directly when calling
  # from code.
  def execute
    Zendesk::Rules::AutomationExecutor.new(automation: self).execute
  end

  def updated_at_conditions
    definition.conditions_all.select do |condition|
      condition.source == 'updated_at' && condition.operator =~ /^is_?/
    end
  end

  def business_hours_conditions
    definition.conditions_all.select do |condition|
      condition.operator == 'is_business_hours'
    end
  end

  def actions_close_ticket?
    definition.actions.any? do |condition|
      condition.source == 'status_id' &&
        condition.value.first == StatusType.CLOSED.to_s
    end
  end

  def skip_validate_nullifying_actions!
    @skip_validate_nullifying_actions = true
  end

  protected

  # Automations MUST alter a condition property they are testing for OR contain
  # an "Hour since" with "is" as operator. Otherwise, the automation is run
  # every hour for the ticket.
  def validate_nullifying_actions
    return if @skip_validate_nullifying_actions
    return if unique_time_based_condition?
    return if condition_nullified_via_action?

    errors.add(:base, I18n.t('txt.admin.models.rules.automation.must_run_once_v2', {
      link: I18n.t('txt.admin.models.rules.automation.must_run_once_learn_more')
    }))
  end

  def condition_nullified_via_action?
    field_keys = [
      account.custom_fields.for_organization.map do |field|
        "organization.custom_fields.#{field.key}"
      end,
      account.custom_fields.for_user.map do |field|
        "requester.custom_fields.#{field.key}"
      end,
      account.ticket_fields.map do |field|
        "ticket_fields_#{field.id}"
      end
    ].flatten!

    property_actions = begin
      definition.actions.select do |condition|
        PROPERTY_FIELDS.member?(condition.source.split('#').first) ||
          field_keys.include?(condition.source)
      end
    end

    property_actions.any? do |action|
      all_conditions.any? { |condition| action.nullifies?(condition) }
    end
  end

  def all_conditions
    @all_conditions ||= definition.conditions_all.to_a
  end

  # a time based condition that is only true once ("24 hours old", not:
  # "created_at < 24 hours")
  def unique_time_based_condition?
    conditions = (all_conditions + definition.conditions_any.to_a).compact.uniq

    conditions.any? do |condition|
      CONDITION_FIELDS.member?(condition.source.split('#').first) &&
        condition.operator.index('is') == 0
    end
  end

  def hard_limit_for_rule
    HARD_LIMIT_FOR_AUTOMATIONS
  end

  def higher_table_limit?
    account.has_higher_automations_table_limit?
  end
end
