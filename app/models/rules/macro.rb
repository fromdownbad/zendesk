class Macro < Rule
  ignore_column :sla_id

  include Zendesk::PreventCollectionModificationMixin

  HARD_LIMIT_FOR_MACROS = 80_000

  has_many :rule_attachments_rules,
    class_name: 'RuleAttachmentRule',
    foreign_key: :rule_id,
    inherit: :account_id

  has_many :attachments,
    through: :rule_attachments_rules,
    source:  :rule_attachment,
    inherit: :account_id

  has_many :groups_macros, class_name: 'GroupMacro', autosave: true, inherit: :account_id

  has_many :groups, through: :groups_macros

  has_many :workspace_elements, as: :element, dependent: :destroy, inherit: :account_id
  has_many :workspaces, through: :workspace_elements

  validates_presence_of :actions
  validates_presence_of :owner_id
  validates_presence_of :owner_type

  validate :ensure_single_rich_comment_action
  validate :ensure_single_basic_comment_action, on: :create
  validate :ensure_limited_basic_comment_actions, on: :update

  before_save :update_associated_groups

  before_validation { title&.squish! }

  before_save if: :multiple_group_macro? do
    self.owner_id = Rule.group_owner_id
  end

  after_create if: :group_macro? do
    groups.reload
  end

  after_save { self.group_owner_ids = nil }

  scope :in_group, ->(group_ids) {
    joins(:groups_macros).where(groups_macros: {group_id: [*group_ids]}).uniq
  }

  def self.categories
    where('title LIKE ?', '%::%').order(:title).map(&:category).uniq
  end

  def self.update_position(ids)
    where(id: ids).tap do |macros|
      macros.update_all(['position = FIELD(id, ?)', ids])

      CachingObserver.expire_fragments_on_save_or_destroy(macros.first)
    end
  end

  attr_accessor :group_owner_ids

  delegate :actions, to: :definition

  def apply(user, ticket = nil)
    Zendesk::Rules::MacroApplication.new(self, user, ticket).run
  end

  def availability_type
    case owner_type
    when 'Account'
      'everyone'
    when 'Group'
      'group'
    when 'User'
      'personal'
    end
  end

  def category
    title.gsub(/::.*/, '')
  end

  def position_after(previous_macro_id)
    macro_ids =
      account.all_macros.active.where('id != ?', id).ordered.pluck(:id)

    return unless macro_ids.include?(previous_macro_id)

    account.all_macros.update_position(
      macro_ids.insert(macro_ids.index(previous_macro_id) + 1, id)
    )
  end

  private

  def ensure_single_rich_comment_action
    return unless definition.rich_comment_actions.count > 1

    errors.add(:base, I18n.t('txt.models.rules.rule.invalid_comment_action'))
  end

  def ensure_single_basic_comment_action
    return unless definition.basic_comment_actions.count > 1

    errors.add(:base, I18n.t('txt.models.rules.rule.invalid_comment_action'))
  end

  def ensure_limited_basic_comment_actions
    return unless excessive_basic_comment_actions?

    errors.add(:base, I18n.t('txt.models.rules.rule.invalid_comment_action'))
  end

  def excessive_basic_comment_actions?
    definition.basic_comment_actions.count >
      [definition_was.basic_comment_actions.count, 1].max
  end

  def update_associated_groups
    return if groups_unchanged?

    groups_macros.each(&:mark_for_destruction)

    return unless group_macro?

    current_group_owner_ids.each do |group_id|
      groups_macros.build do |group_macro|
        group_macro.account_id = account.id
        group_macro.group_id   = group_id
      end
    end
  end

  def groups_unchanged?
    group_macro? &&
      (
        group_owner_ids.present? && group_owner_ids.sort == group_ids.sort ||
          group_owner_ids.blank? && !owner_id_changed?
      )
  end

  def current_group_owner_ids
    group_owner_ids.present? ? group_owner_ids : [owner_id]
  end

  def group_macro?
    owner_type == 'Group'
  end

  def multiple_group_macro?
    group_macro? && current_group_owner_ids.count > 1
  end

  def hard_limit_for_rule
    HARD_LIMIT_FOR_MACROS
  end

  def higher_table_limit?
    account.has_higher_macros_table_limit?
  end
end
