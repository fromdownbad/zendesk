require 'zendesk/models/role'

class Role
  include Zendesk::Serialization::RoleSerialization

  def description
    case id
    when END_USER.id
      I18n.t("txt.admin.helpers.user_helper.end_user_description")
    when ADMIN.id
      I18n.t("txt.admin.helpers.user_helper.admin_description")
    when AGENT.id
      I18n.t("txt.admin.helpers.user_helper.agent_description")
    when LEGACY_AGENT.id
      I18n.t("txt.admin.helpers.user_helper.legacy_agent_description")
    end
  end

  def display_name
    case id
    when END_USER.id
      I18n.t("txt.admin.helpers.user_helper.end_user_name")
    when ADMIN.id
      I18n.t("txt.admin.helpers.user_helper.administrator_label")
    when AGENT.id
      I18n.t("txt.admin.helpers.user_helper.agent_name")
    when LEGACY_AGENT.id
      I18n.t("txt.admin.helpers.user_helper.legacy_agent_name")
    end
  end

  def to_conditions
    case id
    when END_USER.id, ADMIN.id
      { roles: id }
    when AGENT.id
      { roles: [AGENT.id, ADMIN.id] }
    when LEGACY_AGENT.id
      { roles: AGENT.id, permission_set_id: nil }
    end
  end

  def to_query_params
    case id
    when END_USER.id, ADMIN.id
      { role: id }
    when AGENT.id
      { role: [AGENT.id, ADMIN.id] }
    when LEGACY_AGENT.id
      { legacyagents: 1 }
    end
  end
end
