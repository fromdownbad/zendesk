class OutboundEmail < ActiveRecord::Base
  belongs_to :account
  belongs_to :ticket
  belongs_to :notification

  scope :for_ticket, ->(ticket) { where(account_id: ticket.account.id, ticket_id: ticket.id) }
  scope :for_notification, ->(notification) { where(account_id: notification.account.id, notification_id: notification.id) }

  attr_accessible :account, :ticket, :notification
end
