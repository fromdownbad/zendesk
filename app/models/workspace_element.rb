class WorkspaceElement < ActiveRecord::Base
  belongs_to :account
  belongs_to :workspace
  belongs_to :element, polymorphic: true

  inherits_from :workspace, attr: :account_id

  validates :account, :workspace, :element_type, presence: true
  validates :element_id, presence: true

  attr_accessible

  alias_attribute :expand, :expand_by_default

  def app?
    element_type == 'App'
  end

  def macro?
    element_type == 'Macro'
  end

  def app_info_for_admin_page
    { id: element_id, expand: expand_by_default, position: position }
  end

  def build_app_element_with(attributes = {})
    return unless HashParam.ish?(attributes)

    self.workspace_id = attributes[:workspace_id]
    self.element_type = "App"
    self.element_id = attributes[:id]
    self.account_id = attributes[:account_id]
    self.expand = attributes[:expand]
    self.position = attributes[:position]

    self
  end
end
