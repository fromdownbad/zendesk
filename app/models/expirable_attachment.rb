require 'token'

class ExpirableAttachment < ActiveRecord::Base
  include AttachmentMixin
  include Zendesk::Serialization::AttachmentSerialization
  include Zendesk::Attachments::Stores

  DEFAULTS = { ttl: 2.weeks }.freeze

  belongs_to  :account
  belongs_to  :author, class_name: 'User', foreign_key: :author_id, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  attr_accessible :created_via, :author

  scope :latest, ->(created_via) { where(created_via: created_via).order("created_at DESC, id DESC").limit(7) }

  setup_attachment_fu(
    path_prefix: 'data/expirable_attachments',
    size: 0..500.megabytes
  )

  validates_presence_of :account_id, :author_id, :created_via
  validates_as_attachment

  before_create :set_expiry
  before_create :generate_token

  def self.find_by_token_and_account_id!(token, account_id)
    where(token: token.to_s, account_id: account_id).first || raise(ActiveRecord::RecordNotFound)
  end

  def self.find_by_token_or_id_and_account_id!(token_or_id, account_id)
    raise ActiveRecord::RecordNotFound if token_or_id.blank?
    where(account_id: account_id).where('token = ? OR id = ?', token_or_id, token_or_id).first ||
      raise(ActiveRecord::RecordNotFound)
  end

  def self.expirable(args = {})
    settings.merge!(args)
  end

  def self.cleanup
    ExpirableAttachment.where("expire_at < NOW()").each(&:destroy)
  end

  def self.settings
    @settings ||= DEFAULTS.dup
  end

  def url(args = {})
    args   = args.reverse_merge(token: false, relative: false)
    mapped = args.key?(:mapped) ? args[:mapped] : false
    "#{account.url(mapped: mapped) unless args[:relative]}/expirable_attachments/token/#{token}/?name=#{filename}"
  end

  def filename=(filename)
    self.display_filename = filename
    super(filename)
  end

  def display_filename
    super || filename
  end

  private

  def destroy_thumbnails?
    false
  end

  def set_expiry
    self.expire_at = self.class.settings[:ttl].from_now
  end

  def generate_token
    if Arturo.feature_enabled_for?(:voice_csv_generation, account)
      Rails.logger.info("[TOKEN] Generating token for Account ID #{account_id}")
    end
    self.token = Token.generate
  end
end
