class MobileApp < ActiveRecord::Base
  ZD_AGENT_IDENTIFIER = 'com.zendesk.agent'.freeze
  ZD_INBOX_IDENTIFIER = 'com.zendesk.inbox'.freeze
  ZD_AGENT_DEBUG_IDENTIFIER = 'com.zendesk.agent_debug'.freeze

  not_sharded
  disable_global_uid
  attr_accessible
end
