require 'zendesk/models/user_facebook_identity'

class UserFacebookIdentity < UserIdentity
  include Zendesk::Serialization::UserFacebookIdentitySerialization

  def self.identity_type
    :facebook
  end
end
