require 'zendesk/models/user_any_channel_identity'

class UserAnyChannelIdentity < UserIdentity
  include Zendesk::Serialization::UserAnyChannelIdentitySerialization

  def self.identity_type
    :any_channel
  end

  # The name of the AnyChannel subtype, i.e. the name of the RegisteredIntegrationService
  def subtype_name
    # Our 'value' is the ID of the RegisteredIntegrationService and the external ID, separated by a colon
    ::Channels::AnyChannel::RegisteredIntegrationService.where(account_id: account_id, id: value.split(':')[0]).first.try(:name)
  end
end
