require 'zendesk/models/user_identity'

class UserIdentity < ActiveRecord::Base
  MAX_PER_USER = 1000
  include Zendesk::Serialization::UserIdentitySerialization
  include CachingObserver
  include UserIdentityObserver
  include UserIdentityEventObserver
  include Zendesk::Users::Identities::UserIdentityLimit
  include UserEntityObserver

  extend  Zendesk::CappedCounts

  IDENTITY_TYPE_MAP = {
    'email' => 'UserEmailIdentity',
    'facebook' => 'UserFacebookIdentity',
    'phone' => 'UserPhoneNumberIdentity',
    'phone_number' => 'UserPhoneNumberIdentity',
    'sdk' => 'UserSdkIdentity',
    'twitter' => 'UserTwitterIdentity'
  }.freeze

  # NOTE: Duplicate of association defined in zendesk_core, with account ID inheritance
  belongs_to :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  has_kasket_on :user_id
  has_many :verification_tokens, -> { order(created_at: :desc) }, as: :source, dependent: :destroy, inherit: :account_id
  has_one :google_profile, inherit: :account_id
  # TODO: Channels.  TwitterUserProfile is defined in the zendesk_channels repo.
  has_one :twitter_user_profile,
    class_name: '::Channels::TwitterUserProfile',
    foreign_key: :external_id, primary_key: :value, inherit: :account_id

  scope :twitter,      -> { where(type: 'UserTwitterIdentity') }
  scope :foreign,      -> { where(type: 'UserForeignIdentity') }
  scope :phone,        -> { where(type: 'UserPhoneNumberIdentity') }
  scope :facebook,     -> { where(type: 'UserFacebookIdentity') }
  scope :voice_number, -> { where(type: 'UserVoiceForwardingIdentity') }
  scope :sdk,          -> { where(type: 'UserSdkIdentity') }
  scope :any_channel,  -> { where(type: 'UserAnyChannelIdentity') }
  scope :native,       -> { where("type != 'UserForeignIdentity'") }
  scope :unverified_emails, -> do
    email.where("is_verified = false and DATE(created_at) > DATE('2017-09-17')")
  end

  before_validation :update_account

  attr_accessible :value, :is_verified, :account, :user

  # This check is above uniqueness_of_value to prevent triggering a utf8/latin1 collation mix crash
  validate :no_utf8_in_email_addresses

  validates_presence_of :account_id, :user
  validates_length_of :value, maximum: 255, allow_nil: true
  validate :uniqueness_of_value # fast case insensitive
  validates_numericality_of :priority
  validates_uniqueness_of :priority, scope: [:user_id, :type], if: proc { |identity| identity.priority_changed? && identity.user_id.present? }
  validate :user_is_active
  validate :user_accepts_identity
  validate :limit_identities, on: :create

  before_create :auto_verify
  before_destroy :ensure_user_has_at_least_one_identity
  before_validation :set_lowest_priority, on: :create
  before_create :make_primary, if: proc { |identity| identity.is_verified? && @make_primary }
  after_destroy :log_destroyed_identity

  def self.find_by_value(val)
    return nil unless val
    val = val.to_s
    return nil if val.each_byte.detect { |c| c > 127 }
    # seems natural to call "super" or even "method missing" here, but those generate a find_by_value method
    # that replaces ours.
    where(value: val).first
  end

  def self.find_by_email(email, options = {})
    return nil unless Zendesk::Mail::Address.valid_address?(email)

    conditions = options.fetch(:conditions, {})

    identity_conditions = { value: email }
    identity_conditions[:is_verified] = conditions.delete(:is_verified) if conditions.key?(:is_verified)

    self.email.where(identity_conditions).first
  end

  def self.find_by_main_identity(value)
    if EMAIL_PATTERN.match?(value)
      find_by_email(value)
    else
      find_by_twitter_screen_name(value)
    end
  end

  def self.find_by_facebook_id(facebook_id)
    facebook.find_by_value(facebook_id)
  end

  def self.find_by_twitter_screen_name(screen_name)
    screen_name = screen_name.sub("@", "")
    twitter.joins(:twitter_user_profile).where(["channels_user_profiles.name = ?", screen_name]).first
  end

  def self.find_by_sdk_id(guid)
    sdk.find_by_value(guid)
  end

  # Checks if the user-identity for social user already exists
  #
  # identity - a hash containing :type and :value of a social identity
  # eg. for facebook -> { type: 'facebook', value: <facebook_id> }
  #     for twitter  -> { type: 'twitter', value: <screen_name> }
  #
  def self.social_identity_exists?(identity)
    case identity[:type]
    when 'facebook'
      find_by_facebook_id(identity[:value]).present?
    when 'twitter'
      find_by_twitter_screen_name(identity[:value]).present?
    else
      false
    end
  end

  # Sanitize condition to prevent sql injection
  def self.sanitize_sql_for_type_value(type, value)
    UserIdentity.send(:sanitize_sql, ["(type = ? AND value = ?)", type, value])
  end

  # Transfer the identity to `user`.
  #
  # user - the User to which the identity should be transferred.
  #
  # Returns nothing.
  def transfer_to_user(user)
    Rails.logger.warn("Transfering identity #{id} from user #{user_id} to user #{user.id}")
    self.user = user
    set_lowest_priority
    save
  end

  def ensure_user_has_at_least_one_identity
    return true unless user.is_active
    return true if user.account.pre_account_creation
    unless user.identities.count(:all) > 1
      errors.add(:base, I18n.t('txt.user_identity.users_must_have_at_least_one_identity'))
      return false
    end
  end

  def verify
    update_attribute(:is_verified, true) unless is_verified?
  end

  def verify!
    verify
    verification_tokens.clear unless user.crypted_password.blank?
  end

  def unverify
    update_attribute(:is_verified, false)
  end

  def make_primary
    return if primary?

    self.class.where(user_id: user_id || user.id, type: self.class.sti_name).where("priority < ?", priority).update_all("priority = priority + 1")
    user.identities.reload if user.identities.loaded?
    user.verification_tokens.destroy_all

    self.priority = 1
  end

  def make_primary!
    return if primary?

    make_primary
    save!
  end

  def changed_to_primary?
    priority_changed? && primary?
  end

  def primary?
    priority == 1
  end

  def value=(value)
    super(value.try(:to_s).try(:strip))
  end

  def humanize
    self.class.to_s.gsub('User', '').gsub('Identity', '')
  end

  def to_s
    value.to_s
  end

  def identity_type
    self.class.identity_type.to_s
  end

  private

  def update_account
    self.account_id ||= user.account_id
  end

  def auto_verify
    if user.identities == [self] # we only do this when creating the first identity
      self.is_verified ||= user.skip_verification || user.is_verified?
    end
    true
  end

  def value_for_error
    value
  end

  def set_lowest_priority
    self.priority = highest_priority + 1
  end

  def highest_priority
    self.class.where(user_id: (user_id || user.id), type: type).maximum(:priority).to_i
  end

  def user_accepts_identity
    if user && user.foreign? && !can_be_added_to_foreign_users?
      errors.add(:base, 'can not be added to foreign users')
    end
  end

  def limit_identities
    enforce_user_identity_limit(user)
  end

  # To be overridden in sub-classes
  def can_be_added_to_foreign_users?
    false
  end

  def no_utf8_in_email_addresses
    if self.class.name != "UserPhoneNumberIdentity"
      if value && value.each_byte.detect { |byte| byte > 127 }
        errors.add(:value, I18n.t('txt.errors.models.agreement.user_identity.accented_chars_may_not_be_included'))
        Rails.logger.warn("Account: ##{self.account_id} tried to add utf8 email: #{value}")
      end
    end
  end

  def uniqueness_of_value
    return unless value_changed?
    return if self.class.name == "UserPhoneNumberIdentity"
    return if value && value.each_byte.detect { |byte| byte > 127 } # does not validate values with non-ascii characters

    conditions = ["account_id = ? AND value = ? AND type = ?", account_id, value.to_s, self.class.name]
    unless new_record?
      conditions[0] << " AND id <> ?"
      conditions << id
    end

    errors.add(:value, :taken, value: value_for_error) if self.class.on_slave.where(conditions).any?
  end

  def user_is_active
    if user && !user.is_active?
      errors.add(:value, "Cannot add an identity to a deleted user")
    end
  end

  def log_destroyed_identity
    Rails.logger.warn("Destroyed identity #{inspect}")
  end
end
