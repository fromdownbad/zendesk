module Ipm
  class AlertDismissal < ActiveRecord::Base
    self.table_name = 'ipm_alert_dismissals'

    belongs_to :alert, foreign_key: 'ipm_alert_id', class_name: 'Ipm::Alert'
    belongs_to :account
    belongs_to :user

    validates_presence_of :alert, :account_id, :user

    # Deletes alert dismissals for alerts that no longer exist
    def self.cleanup
      alert_ids = ::Ipm::Alert.all.map(&:id)
      without_arsi.delete_all(["ipm_alert_id NOT IN (?)", alert_ids])
    end
  end
end
