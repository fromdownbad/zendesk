module Ipm
  class FeatureNotificationDismissal < ActiveRecord::Base
    self.table_name = 'ipm_feature_notification_dismissals'

    belongs_to :feature_notification, foreign_key: 'ipm_feature_notification_id', class_name: 'Ipm::FeatureNotification'
    belongs_to :account
    belongs_to :user

    attr_accessible :feature_notification, :account, :user

    validates :feature_notification, :account_id, :user, presence: true

    # Deletes feature notification dismissals for feature notifications that no longer exist
    def self.cleanup
      feature_notification_ids = Ipm::FeatureNotification.pluck(:id)
      without_arsi.delete_all(["ipm_feature_notification_id NOT IN (?)", feature_notification_ids])
    end
  end
end
