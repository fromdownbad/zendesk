module Ipm
  class Constraint < ActiveRecord::Base
    not_sharded
    disable_global_uid

    include Zendesk::ForbiddenAttributesProtection

    belongs_to :constrainable, polymorphic: true
    self.table_name = 'ipm_constraints'
  end
end
