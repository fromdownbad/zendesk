module Ipm
  class FeatureNotification < ActiveRecord::Base
    not_sharded
    disable_global_uid

    self.table_name = 'ipm_feature_notifications'

    include Zendesk::Ipm::ActsAsConstrainable
    include Zendesk::Ipm::Category
    include Zendesk::Ipm::CommonConstraints
    include Zendesk::Ipm::ConstraintsMatcher
    include Zendesk::Ipm::Status

    IMAGES = %w[
      zendesk_logo-b876e42defe77cccbb36dbd7005f8339.png
      NPS-f094dcb720a79bbb2022a181483b3d78.png
      help_center-3f6ff17489ad1153c0f488f3cac4e0dc.png
      tshirt_gift-6f07450cb32da4a55cf9670ab709fe98.png
      training_services-3d21d0ae569725bae6cadb4eab82539d.png
      voice_2-432f2fde7ba0fcbe6a821f823f87a2cd.png
      surveys-0516d4ca3cee783b50d83602d010b162.png
      community-cfdce528891154b068aed35f276f8c05.png
      knowledge_base-a31fe50a4a0f1a9f57e904b1467fb1fe.png
      knowledge_base_2-9f20b94061701c5382593b53cb67f341.png
      how_to_webinars-3d82c1d285cc8cb5681232ca04fd98f1.png
      how_to_doc-afc5564d729a93fd4a9c3419f78e72a0.png
      help-2b3c8a55b95846acae6c8b4d9070c716.png
      benefits-b50a867def6a05e1e8579d1392e91943.png
      email-b9c6918da336e2ca78a40f24319bbeb1.png
      how_to_videos-4765ec56f6c0ce7177bd9f0fa7b5a8b7.png
      features-9a30552b6c3a7f54541c95f35b8a3829.png
      chat-da9c85596bcea1ec0f9112ba70bb7069.png
      voice_1-80d64457eff023c5aa3d10069c996df5.png
      apps-4577dc637b0dd02c7eb30b71e79632e1.png
      business_rules-b62ea50451c5389b9d29dc136d603fb6.png
      relevant_content-ca2a00f1ca64bd00b29f8f419a0512b6.png
      tickets-18601eebd6c93e198fe7ddcb206ee83a.png
      follow_us-d4e18ca489e26fa0278295965c0135e7.png
      security-e327e57acd727afba19fc33cc10d8f96.png
      anniversaries-4c8ffcf07a8fe44ff397cd621a2c7e44.png
      renewals-ee4ac48d4e1eee1274e675907cb0a054.png
      reporting_1-183f57dd06cca3386249e7e1e837ce27.png
      reporting_2-1bd3688e547408481aa74b2c98b34739.png
    ].freeze

    attr_accessible :title, :body, :image_name, :test_mode

    validates_length_of :title, maximum: 50, minimum: 1
    validates_length_of :body, maximum: 200, minimum: 1

    validates_length_of :call_to_action_text, maximum: 15

    # The "Call-to-action Text" field is required if "Call-to-action URL" is present and vice versa.
    validates_presence_of :call_to_action_text, if: 'call_to_action_url?'
    validates_presence_of :call_to_action_url, if: 'call_to_action_text?'

    validates_inclusion_of :image_name, in: IMAGES
    validates_presence_of :image_name

    after_initialize :assign_default_image_name, if: 'new_record?'

    def self.images_base_url
      @images_base_url ||= "//assets.zendesk.com/agent/assets/ipm/feature_notifications/"
    end

    def self.for_user(user)
      if user && user.is_agent?
        enabled.select { |fn| fn.show_for_user?(user) }
      else
        []
      end
    end

    def image_url
      "#{FeatureNotification.images_base_url}#{image_name}"
    end

    def show_for_user?(user)
      enabled? &&
        matches_user_and_interface?(user, nil) &&
        !user.dismissed_ipm_feature_notification_ids.include?(id)
    end

    private

    def assign_default_image_name
      self.image_name ||= IMAGES.first
    end
  end
end
