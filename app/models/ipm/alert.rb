module Ipm
  class Alert < ActiveRecord::Base
    not_sharded
    disable_global_uid

    self.table_name = 'ipm_alerts'

    has_one :certificate, foreign_key: :ipm_alert_id

    attr_accessible :text, :link_url, :link_text, :test_mode, :custom_factors, :system, :start_date, :end_date, :state

    validates_length_of :text, maximum: 200, minimum: 1

    include Zendesk::Ipm::ActsAsConstrainable
    include Zendesk::Ipm::CommonConstraints
    include Zendesk::Ipm::ConstraintsMatcher
    include Zendesk::Ipm::Status

    constraint :custom_factors, type: :StrArray

    def self.for_user(user, options = {})
      if user && user.is_agent?
        enabled.includes(:ipm_constraints).select { |fn| fn.show_for_user?(user, options.slice(:interface, :custom)) }
      else
        []
      end
    end

    def show_for_user?(user, options = {})
      enabled? &&
        matches_user_and_interface?(user, options[:interface]) &&
        (options[:ignore_custom] || custom_factors_match?(options[:custom])) &&
        !user.dismissed_ipm_alert_ids.include?(id)
    end

    private

    def custom_factors_match?(custom_factors_found)
      return true if custom_factors.blank?
      (Array(custom_factors_found).map(&:to_s) & custom_factors).any?
    end
  end
end
