class Plan < ActiveRecord::Base
  not_sharded
  disable_global_uid

  UNLIMITED_USERS = 500000

  has_many :payments

  attr_accessible :name, :price, :max_agents, :max_macros, :max_views, :max_triggers,
    :max_automations, :has_ssl, :is_public, :max_end_users

  scope :is_public, -> { where(is_public: true) }
  scope :largest, -> { where(is_public: true).order('price DESC').limit(1) }
  scope :free, -> { where(price: 0) }

  def is_free? # rubocop:disable Naming/PredicateName
    price.zero?
  end
end
