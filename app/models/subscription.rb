require 'zendesk/models/subscription'
require 'zendesk_billing_core'

class Subscription < ActiveRecord::Base
  not_sharded
  disable_global_uid

  include SubscriptionObserver
  include SubscriptionProductStateObserver
  include CIA::Auditable

  # new visible attribute -> new translation under txt.admin.views.reports.tabs.audits.attribute.<attribute>
  audit_attribute :billing_cycle_type, :base_agents, :max_agents, :plan_type, :manual_discount, :manual_discount_expires_on, :payment_method_type, :pricing_model_revision,
    :trial_expires_on, :is_trial, :currency_type, callback: :after_commit

  attr_accessor :audit_message

  attr_protected :account_id

  DEFAULT_AGENTS = 5
  INBOX_AGENT_LIMIT = 250
  BILLING_INPUTS = [:plan_type, :max_agents, :billing_cycle_type, :pricing_model_revision].freeze
  FILE_UPLOAD_CAP = 50 # in megabytes

  belongs_to :account, inverse_of: :subscription
  belongs_to :hub_account, class_name: 'Account', foreign_key: :hub_account_id

  has_one :zopim_subscription,
    dependent:  :destroy,
    class_name: '::ZBC::Zopim::Subscription',
    foreign_key: :subscription_id,
    inherit: :account_id

  has_one    :credit_card, inherit: :account_id, dependent: :destroy
  has_one    :permission_set, inherit: :account_id

  has_many   :dunning_notifications, dependent: :destroy
  has_many   :payments, -> { order(period_begin_at: :asc) }, inherit: :account_id, dependent: :destroy do
    def any_paid?
      any? { |p| p.status == PaymentType.PAID }
    end

    def any_invoiced?
      any? { |p| p.status == PaymentType.INVOICED }
    end

    def any_legacy_sponsored?
      any? { |p| p.billing_cycle_type == BillingCycleType.Biannually && p.status == PaymentType.SPONSORED && p.payment_version_type == 2 }
    end

    def last_pending_failing?
      select { |p| p.status == PaymentType.PENDING }.last.try(:failures).to_i > 0
    end

    def any_sponsored?
      any? { |p| p.status == PaymentType.SPONSORED }
    end
  end
  has_many   :invoices, through: :payments
  has_many   :spoke_accounts, through: :account, source: :spoke_subscriptions
  has_many   :spoke_subscriptions, through: :account, source: :spoke_subscriptions

  validates_numericality_of :plan_type,   only_integer: true
  validates_numericality_of :max_agents,  only_integer: true, greater_than_or_equal_to: 1
  validates_numericality_of :base_agents, only_integer: true, greater_than_or_equal_to: 1

  validates_presence_of     :pricing_model_revision
  validates_presence_of     :payment_method_type
  validates_presence_of     :plan_type

  validates_inclusion_of    :plan_type, in: [SubscriptionPlanType.Inbox, SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge]

  validate                  :validate_copy_invoices_to_emails, on: :update

  # removed touch: true from belongs_to :account, add conditional touch
  after_save                :touch_account, unless: :is_account_precreation?
  after_commit              :async_save_copy_invoice_list, if: :zuora_managed?
  before_save               :generate_max_agents
  before_save               :prepare_changes_for_zuora
  before_update             :update_trial_expiration_date, if: :created_at_changed?
  after_update              :make_account_serviceable, if: :hub_account_id_added?
  after_commit              :update_subscription_features!, on: :update, if: :needs_feature_update?
  after_commit              :instrument_product_sync_validation, on: :update

  attr_accessor :managed_update
  attr_accessor :current_user

  delegate :is_failing?,
    :is_active,
    :is_account_precreation?,
    :guide_subscription,      # Delegating because :guide_subscriptions are sharded, and :accounts is unsharded
    :voice_subscription,      # Delegating because :voice_subscriptions are sharded, and :accounts is unsharded
    :tpe_subscription,        # Delegating because :tpe_subscriptions are sharded, and :accounts is unsharded
    :answer_bot_subscription, # NOTE: this is backed by Accounts Service
    :outbound_subscription,   # NOTE: this is backed by Accounts Service
    :explore_subscription,    # NOTE: this is backed by Accounts Service
    :suite_allowed?,
    to: :account

  delegate :assisted?,
    :term_end_date,
    to: :zuora_subscription,
    allow_nil: true

  scope :dunning, -> { includes(:payments).where("payments.status = ? AND payments.failures > 0", PaymentType.PENDING) }
  scope :invoicing, -> { includes(:account).where(payment_method_type: PaymentMethodType.MANUAL) }

  include ::Subscription::CouponSupport
  include ::Subscription::PlanChangeSupport
  include ::Subscription::SignupSupport
  include ::Subscription::TrialManagement
  include ::Subscription::DelegatedFeatures
  include ::Subscription::PatagoniaShims
  include ::Subscription::VoiceSupport
  include ZBC::Subscription::Support

  def self.expired_before(date = Time.now.utc.to_s(:db))
    where("trial_expires_on <= ? ", date)
  end

  def self.canceled_before(date = Time.now.utc.to_s(:db))
    where("churned_on <= ? ", date)
  end

  def initialize(*args)
    super
    self[:pricing_model_revision] ||= default_pricing_model
    self[:payment_method_type]    ||= PaymentMethodType.CREDIT_CARD
  end

  def default_pricing_model
    ZBC::Zendesk::PricingModelRevision::PATAGONIA
  end

  #  TODO: This pricing_model_revision override can be removed once
  #        `Subscription.where(pricing_model_revision: 5, is_trial: true)` no
  #        longer returns any subscriptions
  def pricing_model_revision
    if is_trial?
      ZBC::Zendesk::PricingModelRevision::PATAGONIA
    else
      self[:pricing_model_revision]
    end
  end

  def pre_patagonia?
    pricing_model_revision < ZBC::Zendesk::PricingModelRevision::PATAGONIA
  end

  def patagonia?
    pricing_model_revision == ZBC::Zendesk::PricingModelRevision::PATAGONIA
  end

  def post_patagonia?
    pricing_model_revision > ZBC::Zendesk::PricingModelRevision::PATAGONIA
  end

  # Attribute alias for churned_on
  #
  def canceled_on
    churned_on
  end

  def canceled_on=(cancel_date)
    self.churned_on = cancel_date
  end

  def with_compulsory_spam_prevention?
    plan_type == SubscriptionPlanType.SMALL
  end

  def extend_trial!(num_of_days)
    start_at = trial_expires_on ? [Date.today, trial_expires_on.to_date].max : Date.today
    update_attribute(:trial_expires_on, start_at + num_of_days.days)

    make_account_serviceable
  end

  def is_expired? # rubocop:disable Naming/PredicateName
    !account.is_serviceable? && past_expiration_date?
  end

  def trial_expired?
    is_expired? && !zuora_managed?
  end

  def past_expiration_date?
    trial_expires_on.present? && trial_expires_on.to_date.past?
  end

  def vat_name
    Country.find_by_id(country_id).try(:vat_name)
  end

  def collect_vat?
    !!vat_name
  end

  def as_json
    {
      is_active: account.is_active,
      is_trial: is_trial,
      is_serviceable: account.is_serviceable?,
      dunning: current_dunning_state,
      collect_vat: collect_vat?,
      plan_type: SubscriptionPlanType[plan_type].to_s,
      max_agents: max_agents,
      source: account.source,
      help_desk_size: account.help_desk_size,
      created_at: created_at,
      updated_at: updated_at,
      trial_expires_at: trial_expires_on,
      is_voice_customer: account.current_voice_status == "Customer",
      canceled_on: canceled_on,
      zuora_payment_method_type: zuora_payment_method_type
    }.with_indifferent_access
  end

  def cancel!
    # unpaid zopim can be on paid or unpaid zendesk account so we must remove first
    zopim_subscription.destroy if zopim_subscription.try(:unpaid?)
    return zuora_subscription.cancel! if zuora_managed?

    self.managed_update = true
    self.manual_discount = 0
    self.is_trial = false
    Zendesk::SupportAccounts::Product.retrieve(account).save(validate: false)

    if credit_card
      !!credit_card.destroy
    else
      true
    end
  end

  def has_forums2? # rubocop:disable Naming/PredicateName
    has_forums2_toggle? ? account.has_forums2? : true
  end

  def plan_name
    ZBC::Zendesk::PlanType.plan_name(
      pricing_model_revision,
      plan_type
    )
  end

  def file_upload_cap
    return FILE_UPLOAD_CAP if account.has_email_increase_attachment_size_limit?

    ZBC::Zendesk::FileUploadCap.for_plan(
      pricing_model_revision,
      plan_type_service.proposed_plan_type
    )
  end

  def max_monitored_twitter_handles
    10
  end

  # Use the feature bits to define the corresponding property sets
  property_set(:features, inherit: :account_id) do
    @@delegated_feature_bits.each do |feature_name|
      property feature_name, default: false, type: :boolean
    end
  end

  SubscriptionFeature.class_eval do
    include AuditableProperty

    attr_accessible :name, :value
    belongs_to :subscription, inherit: :account_id
    belongs_to :account

    auditable_properties audit_all: true
  end

  def active_features
    features.where(value: true)
  end

  def generate_max_agents
    write_attribute(:max_agents, calculate_max_agents)
  end

  def base_agents=(arg)
    write_attribute(:base_agents, arg.to_i)
    generate_max_agents
  end

  def max_agents=(_arg)
    generate_max_agents
  end

  def update_max_agents!(value)
    update_attributes!(base_agents: value)
  end

  def plan_type=(arg)
    write_attribute(:plan_type, arg.to_i)
  end

  def billing_cycle_type=(arg)
    write_attribute(:billing_cycle_type, arg.to_i)
  end

  def crediting?
    payment_method_type == PaymentMethodType.CREDIT_CARD
  end

  def is_churned? # rubocop:disable Naming/PredicateName
    !account.is_active? && zuora_managed?
  end

  def account_type
    return 'trial'     if is_trial?
    return 'spoke'     if is_spoke?
    return 'customer'  if account.is_active? && payments.any_paid?
    return 'customer'  if account.is_active? && is_sponsored? && payment_method_type != PaymentMethodType.CREDIT_CARD
    return 'customer'  if account.is_active? && invoicing? && payments.any_invoiced?
    return 'customer'  if account.is_active? && zuora_managed?
    return 'sandbox'   if account.is_sandbox?
    return 'sponsored' if is_sponsored?
    return 'trial'     if !payments.any_paid? && !account.is_serviceable? && account.is_active? && !payments.any_sponsored?
    return 'churned'   if is_churned?
    'unknown'
  end

  def account_status
    return 'active'    if account.is_active? && zuora_managed?
    return 'canceled'  if !account.is_active? && !payments.any_paid?
    return 'churned'   if !account.is_active? && payments.any_paid?
    # This second churned state is for the accounts that got a 6 month sponsorship when the subscription system
    # got changed. They could have been "paying -> free -> 6 month sponsorship"
    return 'churned'   if payments.any_paid? && payments.any_legacy_sponsored? && !credit_card.present?
    return 'expired'   unless account.is_serviceable?
    'active'
  end

  def payment_status
    return 'void'      if account_type != 'customer'
    return 'failing'   if payments.last_pending_failing?
    return 'paying'    if account.is_active? && payments.any_paid?
    return 'paying'    if account.is_active? && is_sponsored? && payment_method_type != PaymentMethodType.CREDIT_CARD
    return 'pending'   if credit_card.present? && !payments.any_paid?
    'unknown'
  end

  def billing_backend
    zuora_managed? ? 'zuora' : 'internal'
  end

  def payment_method
    return 'void' if account_type != 'customer'
    PaymentMethodType[payment_method_type].to_s
  end

  def dunning?
    dunning_state = ZBC::States::Dunning.new(subscription: self)
    dunning_state.next_state > ZendeskBillingCore::States::Dunning::State::OK
  end

  def current_dunning_state
    if zuora_managed? && zuora_subscription.present?
      ZBC::States::Dunning::STATES[zuora_subscription.dunning_level].name
    else
      ZBC::States::Dunning.new(subscription: self).current_state_name
    end
  end

  def zuora_payment_method_type
    return 'none' unless zuora_subscription.present?
    ZBC::Zuora::PaymentMethod[zuora_subscription.payment_method_type].name
  end

  def sales_model_value
    return ZBC::States::SalesModel::SELF_SERVICE unless zuora_managed? && zuora_subscription.present?
    zuora_subscription.sales_model
  end

  def self_service?
    sales_model_value == ZBC::States::SalesModel::SELF_SERVICE
  end

  # Bang because this saves w/o validations, not because we expect it to raise.
  def add_to_comment_field!(message)
    add_to_comment_field(message)
    save(validate: false)
  end

  def add_to_comment_field(message)
    self.audit_message = [audit_message, message].compact.join(", ")
  end

  def editable_by_account_owner?
    return false if is_sponsored? || invoicing?
    true
  end

  def self.without_payments_after(cutoff = DateTime.now)
    where(
      [
        "subscriptions.payment_method_type = ? "\
        "AND ? > ( "\
        "  SELECT MAX(period_begin_at) "\
        "  FROM  payments "\
        "  WHERE subscription_id = subscriptions.id "\
        "  AND status IN (?) "\
        ")",
        PaymentMethodType.MANUAL, cutoff, [PaymentType.PENDING, PaymentType.INVOICED]
      ]
    ).to_a
  end

  def flatten_payments?
    return false if is_sponsored? # Don't flatten if we've sponsored this account
    return true  if (payments.paid.count(:all) == 0) && payments.exists?(billing_cycle_type: BillingCycleType.Biannually, status: PaymentType.SPONSORED, payment_version_type: 2)
    return true  if !dunning? && (account.is_serviceable == false)
    return false if payments.count(:all) != payments.pending.count(:all) # Don't flatten if there is a sponsored or paid (or whatever) payment on the account.
    return false if payments.exists? "failures > 0" # Don't flatten if we've ever failed to charge this account.
    true
  end

  def copy_invoice_list
    copy_invoices_to.strip.split(/[\s,]+/) unless copy_invoices_to.blank?
  end

  attr_accessor :previewing

  # Apply billing and promo/coupon changes to a (deep) copy of this object and return that copy.
  #
  # NOTE: This method can only fully support Zuora managed subscriptions, especially when applying changes
  # and generating price quotes involving promo/coupon changes.
  #
  # To support for legacy subscriptions involving promo/coupon changes, a call to CouponSupport#apply_coupon_change
  # (or something similar) after the invocation of #apply_billing_changes will be necessary.
  def preview!(subscription_options)
    subscription_dup = dup

    subscription_dup.previewing = true
    subscription_dup.promo_code = subscription_options.coupon_code
    subscription_dup.apply_billing_changes(subscription_options.to_hash)

    raise ActiveRecord::RecordInvalid, subscription_dup unless subscription_dup.valid?
    preview_zopim_subscription(self,      subscription_dup, subscription_options)
    preview_voice_subscription(self,      subscription_dup, subscription_options)
    preview_guide_subscription(self,      subscription_dup, subscription_options)
    preview_tpe_subscription(self,        subscription_dup, subscription_options)
    preview_answer_bot_subscription(self, subscription_dup, subscription_options)
    preview_outbound_subscription(self,   subscription_dup, subscription_options)
    preview_explore_subscription(subscription_dup, subscription_options)

    subscription_dup.freeze
  end

  attr_writer :guide_preview,
    :tpe_preview,
    :voice_preview,
    :answer_bot_preview,
    :outbound_preview,
    :explore_preview,
    :promo_code

  def guide_preview
    # If a guide preview has not been explicitly set, we'll generate one. This
    # is important as much of the pricing data is derived from the preview.

    @guide_preview ||= Guide::Subscription::Previewer.for_account(account)
  end

  def tpe_preview
    @tpe_preview ||= tpe_subscription
  end

  def answer_bot_preview
    @answer_bot_preview ||= answer_bot_subscription
  end

  def outbound_preview
    @outbound_preview ||= outbound_subscription
  end

  def explore_preview
    @explore_preview ||= explore_subscription
  end

  def voice_preview
    return @voice_preview if defined?(@voice_preview)

    @voice_preview = begin
      account.voice_subscription || trial_or_legacy_voice_subscription
    end
  end

  def trial_or_legacy_voice_subscription
    return unless (voice_data = Voice::VoiceAccount.remote_data_for_account(account))
    return unless voice_data.show_trial_or_legacy_voice_subscription?

    account.build_voice_subscription(
      legacy:     voice_data.billing_plan_type.nil?,
      max_agents: max_agents,
      plan_type:  voice_data.billing_plan_type,
      trial:      voice_data.trial
    )
  end

  def preview_voice_subscription(original_subscription, duplicate_subscription, subscription_options)
    voice_options = subscription_options.to_voice_hash
    if voice_options[:max_agents].nil? || voice_options[:plan_type].nil?
      duplicate_subscription.voice_preview = nil
    else
      voice_subscription = original_subscription.voice_subscription || new_voice_subscription(subscription_options)
      [:plan_type, :max_agents].each { |key| voice_subscription[key] = voice_options[key] }
      duplicate_subscription.voice_preview = voice_subscription
    end
  end

  def new_voice_subscription(subscription_options)
    attrs = subscription_options.to_voice_hash.merge(account_id: account.id)
    ::Voice::Subscription.new(attrs)
  end

  def preview_zopim_subscription(original_subscription, duplicate_subscription, subscription_options)
    zopim_subscription = existing_zopim_subscription(original_subscription) ||
      new_zopim_subscription(subscription_options, original_subscription)

    subscription_options.to_zopim_hash.each { |k, v| zopim_subscription[k] = v }

    zopim_subscription.status = ZBC::Zopim::SubscriptionStatus::Active if zopim_subscription.present?

    duplicate_subscription.zopim_subscription = zopim_subscription
  end

  def preview_guide_subscription(original_subscription, duplicate_subscription, subscription_options)
    guide_options = subscription_options.to_guide_hash

    return unless guide_options.slice(:max_agents, :plan_type).values.all?(&:present?)

    guide_subscription = original_subscription.guide_subscription || new_guide_subscription(subscription_options)
    guide_subscription.assign_attributes(guide_options.slice(:max_agents, :plan_type))

    duplicate_subscription.guide_preview = guide_subscription
  end

  def preview_tpe_subscription(original_subscription, duplicate_subscription, subscription_options)
    tpe_options = subscription_options.to_tpe_hash

    return unless tpe_options.slice(:max_agents, :plan_type).values.all?(&:present?)

    tpe_subscription = original_subscription.tpe_subscription || new_tpe_subscription(subscription_options)
    tpe_subscription.assign_attributes(tpe_options.slice(:max_agents, :plan_type))

    duplicate_subscription.tpe_preview = tpe_subscription
  end

  def preview_answer_bot_subscription(original_subscription, duplicate_subscription, subscription_options)
    answer_bot_options = subscription_options.to_answer_bot_hash
    return unless answer_bot_options.slice(:max_resolutions, :plan_type).values.all?(&:present?)

    # NOTE: Since AnswerBot is backed by Accounts Service, and not writable unless making an API
    # call, we return an OpenStruct object here instead
    answer_bot_subscription = original_subscription.answer_bot_subscription

    duplicate_subscription.answer_bot_preview = OpenStruct.new(
      max_resolutions:  answer_bot_options[:max_resolutions].to_i,
      plan_type:        answer_bot_options[:plan_type],
      trial?:           answer_bot_subscription.try(:trial?),
      trial_expires_at: answer_bot_subscription.try(:trial_expires_at)
    )
  end

  def preview_outbound_subscription(original_subscription, duplicate_subscription, subscription_options)
    outbound_options = subscription_options.to_outbound_hash
    return unless outbound_options.slice(:monthly_messaged_users, :plan_type).values.all?(&:present?)

    outbound_subscription = original_subscription.outbound_subscription

    duplicate_subscription.outbound_preview = OpenStruct.new(
      monthly_messaged_users: outbound_options[:monthly_messaged_users].to_i,
      plan_type:              outbound_options[:plan_type],
      trial?:                 outbound_subscription.try(:trial?),
      trial_expires_at:       outbound_subscription.try(:trial_expires_at)
    )
  end

  def preview_explore_subscription(duplicate_subscription, subscription_options)
    explore_options = subscription_options.to_explore_hash
    return unless explore_options.slice(:max_agents, :plan_type).values.all?(&:present?)

    # NOTE: Since Explore is backed by Accounts Service, and not writable unless making an API
    # call, we return an OpenStruct object here instead
    duplicate_subscription.explore_preview = OpenStruct.new(
      max_agents:  explore_options[:max_agents].to_i,
      plan_type:   explore_options[:plan_type],
      # at this point, preview explore sub should never be a trial
      subscribed?: true
    )
  end

  def existing_zopim_subscription(original_subscription)
    if original_subscription.zopim_subscription.try(:is_active?)
      original_subscription.zopim_subscription.dup
    end
  end

  def new_zopim_subscription(subscription_options, _original_subscription)
    options = subscription_options.to_zopim_hash
    unless options.blank?
      ::ZBC::Zopim::Subscription.new(
        account_id:         account_id,
        plan_type:          options[:plan_type],
        max_agents:         options[:max_agents],
        billing_cycle_type: options[:billing_cycle_type]
      )
    end
  end

  def new_guide_subscription(subscription_options)
    return if (options = subscription_options.to_guide_hash).blank?

    ::Guide::Subscription.new(
      account_id: account.id,
      active:     true,
      max_agents: options.fetch(:max_agents),
      plan_type:  options.fetch(:plan_type)
    )
  end

  def new_tpe_subscription(subscription_options)
    return if (options = subscription_options.to_tpe_hash).blank?

    ::Tpe::Subscription.new(
      account_id: account_id,
      active:     true,
      max_agents: options.fetch(:max_agents),
      plan_type:  options.fetch(:plan_type)
    )
  end

  def promo_code
    previewing ? @promo_code : active_promo_code
  end

  def configure_for_ucsf_causeware!
    self.plan_type              = SubscriptionPlanType.Small
    self.billing_cycle_type     = BillingCycleType.Annually
    self.pricing_model_revision = ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE
    self.base_agents            = 3
  end

  def configure_for_ucsf_causeware?
    if pricing_model_revision == ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE
      is_trial?
    else
      plan_type == SubscriptionPlanType.Small
    end
  end

  def ucsf_causeware?
    plan_type == SubscriptionPlanType.Small && ucsf_pricing_model?
  end

  def ucsf_pricing_model?
    pricing_model_revision == ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE
  end

  def legacy_pricing_models
    [
      ZBC::Zendesk::PricingModelRevision::APRIL_2010,
      ZBC::Zendesk::PricingModelRevision::ENTERPRISE,
      ZBC::Zendesk::PricingModelRevision::INITIAL,
      ZBC::Zendesk::PricingModelRevision::NEW_FORUMS
    ]
  end

  def modern_pricing_model?
    !legacy_pricing_models.include?(pricing_model_revision)
  end

  def active_promo_code
    if zuora_managed?
      account.active_zuora_coupon.try(:coupon_code)
    else
      account.active_legacy_coupon.try(:coupon).try(:coupon_code)
    end
  end

  module Multicurrency
    def currency_symbol
      CurrencyType[currency_type].score
    end

    def currency_name
      CurrencyType[currency_type].name
    end

    def currency=(value = "USD")
      self.currency_type = CurrencyType.find(value) || CurrencyType.USD
    end
  end
  include Multicurrency

  def map_role_permissions_to_agents
    account.users.where(roles: Role::AGENT.id).to_a.each do |agent|
      next unless agent.permission_set
      next if agent.unbillable_agent?

      Rails.logger.info("Mapping custom role: #{account_id}/#{account.subdomain} - #{agent.permission_set.id}/#{agent.permission_set.name} into agent #{agent.id}/#{agent.name} permissions")
      ticket_access_mapping = { "all"                 => RoleRestrictionType.NONE,
                                "within-groups"       => RoleRestrictionType.GROUPS,
                                "within-organization" => RoleRestrictionType.ORGANIZATION,
                                "assigned-only"       => RoleRestrictionType.ASSIGNED }

      agent.restriction_id           = ticket_access_mapping[agent.permission_set.permissions.ticket_access]
      agent.is_moderator             = agent.permission_set.permissions.forum_access == "full" ||
                                       agent.permission_set.permissions.forum_access == "edit-topics"
      agent.is_private_comments_only = agent.permission_set.permissions.comment_access != "public"

      agent.permission_set_id = nil

      Zendesk::SupportUsers::User.new(agent).save
    end
  end

  def add_agents_available?
    return false unless valid_agent_add?
    zuora_subscription.assisted? ? zuora_subscription.assisted_agent_add_enabled? : true
  end

  def future_subscription?
    !!zuora_subscription.try(:future_subscription_starts_at)
  end

  def primary_quantity
    base_agents - seasonal_quantity
  end

  def seasonal_quantity
    pravda_client.product('support').plan_settings['seasonal_quantity'] || 0
  end

  def has_groups? # rubocop:disable Naming/PredicateName
    if !Arturo.feature_enabled_for?(:enable_essential_groups, account) && patagonia? && plan_type == SubscriptionPlanType.Small
      return false
    end

    super
  end

  private

  def pravda_client
    @pravda_client ||= Zendesk::Accounts::Client.new(account)
  end

  def valid_agent_add?
    zuora_subscription.present? &&
      !starter_plan? &&
      !future_subscription? &&
      !addons? &&
      !paid_guide_product? &&
      !paid_explore_product? &&
      !suite?
  end

  def paid_guide_product?
    find_pravda_product('guide').try(:subscribed?)
  end

  def paid_explore_product?
    find_pravda_product('explore').try(:subscribed?)
  end

  def find_pravda_product(product_name)
    pravda_client.product(product_name, use_cache: true)
  rescue Kragle::ResourceNotFound, Kragle::ResponseError, Faraday::Error => e
    message = "Failed to GET #{product_name} product in account service: #{e.message}"
    Rails.logger.error(message)
    nil
  end

  def suite?
    account.settings.suite_subscription?
  end

  def addons?
    account.subscription_feature_addons.present?
  end

  def calculate_max_agents
    base_agents + temporary_zendesk_agent_count
  end

  def temporary_zendesk_agent_count
    temporary_agents = account.subscription_feature_addons.by_name(:temporary_zendesk_agents)
    return 0 if temporary_agents.empty?

    active_agents = temporary_agents.active_plus_grace_period(::SubscriptionFeatureAddon::TEMPORARY_ZENDESK_AGENTS_GRACE_PERIOD)
    active_agents.inject(0) { |sum, a| sum + a.quantity.to_i }
  end

  def make_account_serviceable
    account.reload.update_attribute(:is_serviceable, true)
  end

  def validate_copy_invoices_to_emails
    return if copy_invoice_list.blank?

    copy_invoice_list.each do |addr|
      unless Zendesk::Mail::Address.sanitize(addr)
        errors.add(:copy_invoices_to, I18n.t('txt.errors.subscription.copy_invoices_error.address_must_all_be_valid_and_comma_or_space_separated'))
        break
      end
    end

    true
  end

  def prepare_changes_for_zuora
    @zuora_invoice_email_addresses = copy_invoices_to_changed? ? copy_invoices_to : nil
  end

  def async_save_copy_invoice_list
    return if !zuora_subscription.present? || @zuora_invoice_email_addresses.nil?
    zuora_account_id = zuora_subscription.zuora_account_id
    InvoiceEmailUpdateJob.enqueue(zuora_account_id, copy_invoice_list)
  end

  def hub_account_id_added?
    hub_account_id_was.blank? && hub_account_id.present?
  end

  def country_id
    account.try(:address).try(:country_id) || Country.find_by_name("United States").id
  end

  def update_trial_expiration_date
    self.trial_expires_on = (created_at + TRIAL_DURATION).to_date
  end

  def needs_feature_update?
    changed_unpaid_plan_type? || changed_to_payment_method_manual? ||
      changed_sandbox_pricing_model_revision? || changed_sandbox_plan_type? ||
      expired_trial_reactivation? || plan_type_change_with_inbox?
  end

  def changed_unpaid_plan_type?
    previous_changes.include?("plan_type") && (is_trial? || is_payment_method_manual?)
  end

  def changed_to_payment_method_manual?
    previous_changes.include?("payment_method_type") && is_payment_method_manual?
  end

  def changed_sandbox_pricing_model_revision?
    previous_changes.include?("pricing_model_revision") && account.is_sandbox?
  end

  def changed_sandbox_plan_type?
    previous_changes.include?("plan_type") && account.is_sandbox?
  end

  def expired_trial_reactivation?
    previous_changes.include?("is_trial") && is_trial?
  end

  def update_subscription_features!
    skip_feature_triggers = monitor_update? && plan_type_change_with_inbox?
    Zendesk::Features::SubscriptionFeatureService.new(
      account,
      nil,
      nil,
      {},
      skip_feature_triggers
    ).execute!
  end

  def instrument_product_sync_validation
    ProductSyncValidationJob.enqueue_in(ProductSyncValidationJob.job_enqueue_delay, account.id)
  end

  def monitor_update?
    !!update_from_monitor_pod
  end

  def plan_type_change_with_inbox?
    previous_changes.include?("plan_type") &&
      (previous_changes["plan_type"][0] == SubscriptionPlanType.Inbox ||
       previous_changes["plan_type"][1] == SubscriptionPlanType.Inbox)
  end

  def touch_account
    account.touch_without_callbacks
  end

  def is_payment_method_manual? # rubocop:disable Naming/PredicateName
    payment_method_type == PaymentMethodType.MANUAL
  end

  def plan_type_service
    @plan_type_service ||= Zendesk::Features::PlanTypeService.new(account)
  end

  def starter_plan?
    plan_type == SubscriptionPlanType.Small && legacy_pricing_model?
  end

  def legacy_pricing_model?
    pricing_model_revision < ZBC::Zendesk::PricingModelRevision::PATAGONIA
  end
end
