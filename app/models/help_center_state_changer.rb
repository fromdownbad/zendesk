class HelpCenterStateChanger
  def initialize(account:, brand:)
    @account = account
    @brand = brand
  end

  def update(account_help_center_state:, brand_help_center_state:, help_center_id:)
    validate_account_help_center_state(account_help_center_state)
    validate_brand_help_center_state(brand_help_center_state)

    ActiveRecord::Base.transaction do
      update_account_help_center_state(account_help_center_state)

      if HelpCenter.exists_for_brand_id?(brand.id)
        HelpCenter.change_state(
          state: brand_help_center_state,
          brand_id: brand.id
        )
      else
        HelpCenter.create(
          id: help_center_id,
          state: brand_help_center_state,
          account_id: account.id,
          brand_id: brand.id
        )
      end
    end
  end

  def destroy(account_help_center_state:)
    validate_account_help_center_state(account_help_center_state)

    ActiveRecord::Base.transaction do
      update_account_help_center_state(account_help_center_state)
      HelpCenter.destroy_by_brand_id(brand.id)
    end
  end

  private

  attr_reader :account, :brand

  def validate_brand_help_center_state(state)
    unless %w[restricted enabled archived].include?(state)
      raise ArgumentError, "Unknown state '#{state}'"
    end
  end

  def validate_account_help_center_state(state)
    unless %w[enable disable restrict].include?(state)
      raise ArgumentError, "Unknown state '#{state}'"
    end
  end

  def update_account_help_center_state(state)
    preload_associations

    case state
    when 'enable'
      account.enable_help_center
    when 'disable'
      account.disable_help_center
    when 'restrict'
      account.restrict_help_center
    end

    Zendesk::SupportAccounts::Product.retrieve(account).save!
  end

  def preload_associations
    # enable_help_center!, disable_help_center! and restrict_help_center!
    # are calling `Account#save!`
    #
    # Rails starts a transactions when doing `Account#save!` and runs all the
    # validations and call-backs inside that transaction.
    #
    # `active_record_shards` doesn't use slave when in transaction on Account
    # hence we end-up reading from the master main Account DB, with slow calls
    # when running in 2 different geographic regions like pod15 (Australia) and
    # pod5 (US). This scenario, eventually, makes Help Center reach timeouts
    # because this controller doesn't answer within 5 seconds.
    #
    # With the following we pre-call some of the queries that will then be called
    # inside the transaction, by doing that we warm up activerecord cache so that
    # once inside the transaction we will not fire a transatlantic DB connection
    # but instead read from the local cache in memory.
    account.settings.to_a
    account.subscription.payments.to_a
    account.pre_account_creation
  end
end
