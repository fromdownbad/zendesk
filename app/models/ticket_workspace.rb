require 'zendesk/radar_factory'
class TicketWorkspace < ActiveRecord::Base
  belongs_to :account
  belongs_to :workspace
  belongs_to :ticket

  inherits_from :workspace, attr: :account_id

  # Uniqueness constraint is on ticket_id but prepending account_id to utilize index:
  validates :account_id, presence: true, uniqueness: { scope: :ticket_id }

  after_commit :notify_radar, if: :workspace_has_changed?

  attr_accessible

  private

  def workspace_has_changed?
    !persisted? || workspace_reassigned? || workspace_was_updated?
  end

  def workspace_reassigned?
    previous_changes[:workspace_id].present?
  end

  def workspace_was_updated?
    workspace && (previous_changes[:updated_at]&.first || updated_at) < workspace.updated_at
  end

  def notify_radar
    scope = "ticket/#{ticket.nice_id}/contextual_workspaces_reassigned"

    RadarFactory.create_radar_notification(account, scope).send('updated', '')
  end
end
