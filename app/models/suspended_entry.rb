# The scope of this class is to avoid exposing .suspended directly.
# This way valid syntax like
#   Entry.suspended.for_account(account)
# Can be written like
#   SuspendedEntry.suspended.for_account(account)
#
# This avoid terrible and harmless looking statement like
#   @forum.entry.suspended
#
# that will actually result in multi-tenancy violation.
class SuspendedEntry
  class << self
    def submitted_by(ids)
      suspended.where(submitter_id: ids)
    end

    def for_account(account)
      suspended.where(account_id: account.id)
    end

    def for_forum(forum)
      suspended.where(forum_id: forum.id)
    end

    private

    def suspended
      Entry.unscoped.
        where('entries.deleted_at IS NOT NULL').
        where(flag_type_id: EntryFlagType::SUSPENDED.id).
        order('entries.updated_at DESC')
    end
  end
end
