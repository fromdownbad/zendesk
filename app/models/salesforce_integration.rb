require 'zendesk/encryptable'
require_dependency 'zendesk/salesforce'
require_dependency 'zendesk/salesforce/integration'
require_dependency 'zendesk/crm/integration'
require_dependency 'zendesk/salesforce/object_management'
require_dependency 'zendesk/salesforce/configuration'
require_dependency 'zendesk/salesforce/oauth/connection_handler'
require_dependency 'zendesk/salesforce/next_generation/crm_data'

class SalesforceIntegration < ActiveRecord::Base
  include Zendesk::Encryptable
  include Salesforce::ObjectManagement
  include Salesforce::NextGeneration::CrmData
  belongs_to :account

  ENCRYPTION_KEY_NAME = ENV.fetch('ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY_NAME')
  ENCRYPTION_CIPHER_NAME = ENV.fetch('ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_CIPHER_NAME')

  ENCRYPTED_TOKENS_MAP = {
    decrypted_token: :encrypted_token,
    decrypted_secret_value: :encrypted_secret_value,
    decrypted_access_token: :encrypted_access_token,
    decrypted_refresh_token: :encrypted_refresh_token
  }.freeze

  attr_accessible :account,
    :is_sandbox,
    :access_token,
    :refresh_token,
    :instance_url,
    :token,
    :secret,
    :app_latency,
    :show_parent,
    :encrypted_secret_value,
    :encrypted_token,
    :encrypted_refresh_token,
    :encrypted_access_token

  after_destroy :destroy_target

  attr_encrypted :encrypted_secret_value, :encrypted_token, :encrypted_refresh_token, :encrypted_access_token,
    encryption_key_name: ENCRYPTION_KEY_NAME,
    encryption_cipher_name: ENCRYPTION_CIPHER_NAME

  before_save :encrypt!

  def configured?
    configured_with_oauth1? || configured_with_oauth2?
  end

  def enabled?
    configured? && account.settings.salesforce_integration?
  end

  def get_salesforce_session # rubocop:disable Naming/AccessorMethodName
    connection_handler.session_info
  end

  def create_target
    unless account.targets.find_by_type("SalesforceTarget")
      target = SalesforceTarget.new(
        account: account,
        title: "Salesforce Target",
        token: token,
        secret: secret,
        ticket_information_mode: "1",
        is_active: true,
        feature_identifier: 'salesforce_integration'
      )
      target.current_user = account.owner
      target.save
    end
  end

  def configured_with_oauth1?
    token.present? && secret.present?
  end

  def configured_with_oauth2?
    access_token.present? && refresh_token.present? && instance_url.present?
  end

  def fetch_info_for(user)
    begin
      xml = fetch_user_info(user, driver)
    rescue StandardError => e
      if configured_with_oauth2? && e.message.include?('INVALID_SESSION_ID')
        # SOAP API doesn't use the OAuth2 auto-refresh access token
        # Let's manually update token if we receive an invalid session id error
        xml = fetch_user_info(user, driver)
      else
        raise e
      end
    end

    base_url = URI.parse(get_salesforce_session[:server_url]).host
    parse_salesforce_xml(xml, base_url)
  end

  [:decrypted_token, :decrypted_secret_value, :decrypted_access_token, :decrypted_refresh_token].each do |attr|
    define_method(attr) do
      value = decrypted_value_for(ENCRYPTED_TOKENS_MAP[attr]) rescue ''
      self.class.instance_variable_set("@#{attr}", value)
    end
  end

  private

  def destroy_target
    account.targets.where(type: "SalesforceTarget").map(&:destroy)
  end

  def connection_handler
    @salesforce_handler ||= begin
      Salesforce::Oauth::ConnectionHandler.new(
        account: account,
        salesforce_integration: self
      )
    end
  end
end
