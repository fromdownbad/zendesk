require 'zendesk/resource_collection/rule_manager'
require 'zendesk/resource_collection/ticket_field_manager'
require 'zendesk/resource_collection/user_field_manager'
require 'zendesk/resource_collection/organization_field_manager'
require 'zendesk/resource_collection/target_manager'

class ResourceCollectionResource < ActiveRecord::Base
  attr_accessible

  has_soft_deletion default_scope: true

  belongs_to :account
  belongs_to :resource_collection
  belongs_to :resource, polymorphic: true

  TYPE_MAPPING = {
    'Trigger' => 'triggers',
    'View' => 'views',
    'Macro' => 'macros',
    'Automation' => 'automations',
    'Target' => 'targets',
    'TicketField' => 'ticket_fields',
    'User' => 'user_fields',
    'Organization' => 'organization_fields'
  }.freeze

  RESOURCE_MAPPING = {
    'triggers' => ResourceCollection::RuleManager,
    'views' => ResourceCollection::RuleManager,
    'macros' => ResourceCollection::RuleManager,
    'automations' => ResourceCollection::RuleManager,
    'targets' => ResourceCollection::TargetManager,
    'ticket_fields' => ResourceCollection::TicketFieldManager,
    'user_fields' => ResourceCollection::UserFieldManager,
    'organization_fields' => ResourceCollection::OrganizationFieldManager
  }.freeze

  def self.manager_for(account, user, type_name)
    RESOURCE_MAPPING.fetch(type_name).new(account, user, type_name: type_name)
  end

  def manager_for(account, user)
    case resource
    when Target
      ResourceCollection::TargetManager
    when Rule
      ResourceCollection::RuleManager
    when TicketField
      ResourceCollection::TicketFieldManager
    when CustomField::Field
      if resource.owner == 'User'
        ResourceCollection::UserFieldManager
      elsif resource.owner == 'Organization'
        ResourceCollection::OrganizationFieldManager
      end
    end.new(account, user, type_name: resource.type.downcase.pluralize)
  end

  # resource_type is a different class depending on the resource type. This results in
  # having to check in multiple ways to see the actual type.
  # In addition it is possible the resource got deleted.
  def type
    return TYPE_MAPPING[resource_type] if TYPE_MAPPING[resource_type]
    return TYPE_MAPPING[resource.type] || TYPE_MAPPING[resource.owner] if resource
    resource_type
  end
end
