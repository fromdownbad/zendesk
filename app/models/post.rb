class Post < ActiveRecord::Base
  include ServiceableAccount
  include Zendesk::Serialization::PostSerialization
  include Zendesk::ForumSanitizer
  include CachingObserver
  include ForumObserver

  belongs_to :account
  belongs_to :entry, counter_cache: true
  belongs_to :forum, inherit: :account_id
  belongs_to :user
  alias_method :submitter, :user

  has_many :attachments,
    -> { where(parent_id: nil).order(created_at: :asc, id: :desc) },
    as: :source,
    dependent: :destroy,
    inherit: :account_id

  has_soft_deletion default_scope: true

  validates_presence_of :body, :entry_id, :user_id
  validate :account_is_serviceable, if: :account

  before_save :set_account_id_and_forum_id_from_entry
  before_save :htmlify, if: :is_mobile

  before_validation :suspend_moderated_posts, on: :create

  after_commit :subscribe_submitter, on: :create

  # Counter cache gets bumped even though it's deleted, we reset it
  after_create :reset_entry_counter_cache, if: :suspended?

  after_soft_delete :reset_entry_counter_cache
  after_soft_undelete :reset_entry_counter_cache

  after_soft_undelete    :subscribe_submitter
  after_soft_undelete    { |obj| ForumObserver.store_event(obj) }

  after_create           :add_attachments_from_upload_token
  after_save             :sync_data_with_entry
  after_destroy          :sync_data_with_entry

  attr_accessor :uploads, :silence_notification, :is_mobile, :is_last_item

  alias_attribute :informative, :is_informative

  attr_accessible :body, :is_mobile, :uploads, :user_id, :user, :entry_id, :entry, :forum, :informative, :is_informative, :account

  def self.per_page
    100
  end

  def self.authored_by(ids)
    where(user_id: ids)
  end

  def reset_entry_counter_cache
    Post.unscoped.where(deleted_at: nil).scoping do
      Entry.reset_counters(entry_id, :posts)
    end
  end

  def htmlify
    body.gsub!("\r\n", "<br/>")
  end

  def rss_title
    "\"#{entry.title}\" in #{forum.title} - comment added by #{user.name}"
  end

  # Required to 'act like' an 'rss item'
  def creator
    user
  end

  def rss_guid_parts
    entry.rss_guid_parts + [id]
  end

  # Moderation
  def suspended?
    deleted? && flag_type_id == PostFlagType::SUSPENDED.id
  end

  def self.mark_as_soft_deleted_sql
    { deleted_at: Time.now }.merge(mark_as_unsuspended_sql)
  end

  def self.mark_as_unsuspended_sql
    { flag_type_id: PostFlagType::DEFAULT.id, updated_at: Time.now }
  end

  def mark_as_deleted
    super
    self.flag_type_id = PostFlagType::DEFAULT.id
  end

  def suspend!
    return if suspended?
    suspend
    save!
  end

  def unsuspend!
    return unless suspended?
    self.flag_type_id = PostFlagType::DEFAULT.id
    self.silence_notification = false

    soft_undelete!
  end

  def needs_spam_detection?
    entry.account.settings.moderated_posts? && user.is_end_user?
  end

  private

  def suspend
    self.deleted_at = Time.now
    self.flag_type_id = PostFlagType::SUSPENDED.id
    self.silence_notification = true
  end

  def suspend_moderated_posts
    # Will be stopped by later validation, we need an account
    return unless entry

    if needs_spam_detection?
      # Shouldn't be in before_create, otherwise we get ActiveRecord::RecordNotSaved errors
      if user.throttled_posting?
        errors.add(:base, I18n.t("txt.moderation.throttled"))
        return false
      elsif !user.whitelisted_from_moderation?
        suspend
      end
    end
  end

  def subscribe_submitter
    if !entry.forum.subscribed?(user) && !deleted?
      entry.subscribe(user)
    end
  end

  def set_account_id_and_forum_id_from_entry
    self.account_id = entry.account_id
    self.forum_id = entry.forum_id
  end

  def sync_data_with_entry
    entry.updater = user

    if forum.display_type.questions?
      entry.flag = if entry.posts.exists?(is_informative: true)
        EntryFlagType::ANSWERED
      else
        EntryFlagType::UNKNOWN
      end
    end

    entry.save
  end

  def add_attachments_from_upload_token
    if uploads.present?
      upload_tokens = account.upload_tokens.find_all_by_value(uploads)
      upload_tokens.each do |upload_token|
        self.attachments += upload_token.attachments if upload_token.attachments.present?
      end
    end
  end
end
