require 'zendesk/models/user_sdk_identity'

class UserSdkIdentity < UserIdentity
  MAX_PER_USER = 10000

  STATSD_METRIC_IDENTITY_CREATED = "mobile.sdk.identity.created".freeze

  has_one :device_identifier,
    class_name: "PushNotifications::SdkAnonymousDeviceIdentifier",
    foreign_key: "user_identity_id",
    inherit: :account_id

  validates :value, format: { with: /\A[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}\z/i }
  validate  :limit_sdk_identities, on: :create

  after_create :notify_statsd

  def self.identity_type
    :sdk
  end

  private

  def notify_statsd
    statsd_client.increment(STATSD_METRIC_IDENTITY_CREATED, tags: statsd_tags)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [])
  end

  def statsd_tags
    [
      "user_id:#{user.id}",
      "account_id:#{user.account.id}"
    ]
  end

  def limit_sdk_identities
    if user.identities.sdk.count >= account.settings.max_sdk_identities
      message = "sdk identities limit for user_id: #{user.id}, account_id: #{account.id} exceeded"
      errors.add(:base, message)
    end
  end
end
