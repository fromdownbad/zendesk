class DC::Content < ActiveRecord::Base
  self.table_name = 'dc_contents'

  belongs_to :account
  belongs_to :source, polymorphic: true, touch: true
  belongs_to :default_translation, class_name: "DC::Translation"
  has_many :translations, class_name: "DC::Translation", dependent: :destroy, inverse_of: :content

  attr_accessible :account, :default_translation

  def default_value
    default_translation.value
  end

  def set_default_translation!(translation) # rubocop:disable Naming/AccessorMethodName
    update_attribute(:default_translation, translation)
  end

  def translated_to_locale?(locale)
    translations.find_by_locale(simplify(locale)).present?
  end

  def translation_for_locale(locale)
    translations.find_by_locale(simplify(locale))
  end

  def translate!(value, options = {})
    locale = options.fetch(:locale)

    if translation = translation_for_locale(locale)
      translation.update_attributes(options.merge(value: value, locale: simplify(locale)))
      translation
    else
      create_translation!(value, options.merge(locale: simplify(locale)))
    end
  end

  def create_translation!(value, options = {})
    locale = options.fetch(:locale)

    options.update(
      value: value,
      locale: simplify(locale),
      account: account
    )

    translation = translations.create!(options)

    if translations == [translation]
      update_attributes(default_translation: translation)
    end

    translation
  end

  private

  def simplify(locale)
    locale.to_s.split("-x-", 2).first
  end
end
