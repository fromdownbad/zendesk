class DC::Snippet < ActiveRecord::Base
  self.table_name = 'dc_snippets'

  belongs_to :account
  has_one :content, class_name: "DC::Content", as: :source, dependent: :destroy

  scope :for_account, ->(account) { where(account_id: account.id) }

  before_validation :copy_identifier_to_name
  after_initialize :ensure_content_is_present

  validates_presence_of :name, :identifier

  attr_accessible :account, :identifier, :name, :created_at, :updated_at

  content_methods = [
    :default_translation, :set_default_translation!, :default_value,
    :translated_to_locale?, :translations, :create_translation!,
    :translation_for_locale, :update_translation!, :translate!
  ]
  delegate *content_methods, to: :content

  def value_for_locale(locale)
    translation = translation_for_locale(locale)
    translation && translation.body
  end

  def value_for_locale_or_default(locale)
    value_for_locale(locale) || default_value
  end

  def default_value
    default_translation && default_translation.body
  end

  def self.value_for_identifier(identifier, options = {})
    account = options.fetch(:account)
    locale = options.fetch(:locale)

    snippet = for_account(account).find_by_identifier(identifier)
    snippet && snippet.value_for_locale_or_default(locale)
  end

  private

  def ensure_content_is_present
    build_content(account: account) if content.nil?
  end

  def copy_identifier_to_name
    self.name ||= identifier
  end
end
