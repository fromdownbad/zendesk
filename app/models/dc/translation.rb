class DC::Translation < ActiveRecord::Base
  self.table_name = 'dc_translations'

  belongs_to :account
  belongs_to :content, touch: true, inverse_of: :translations

  validates_uniqueness_of :locale, scope: :content_id
  alias_attribute :value, :body

  before_save :downcase_locale

  attr_accessible :account, :value, :locale, :created_at, :updated_at,
    :active, :hidden

  HIDDEN_STATE = 1

  def active?
    !hidden?
  end

  def active=(active)
    self.hidden = !active
  end

  def hidden?
    states_mask & HIDDEN_STATE != 0
  end

  def hidden=(hidden)
    if hidden
      self.states_mask |= HIDDEN_STATE
    else
      self.states_mask &= ~HIDDEN_STATE
    end
  end

  def update_value!(value)
    update_attribute(:value, value)
  end

  private

  def downcase_locale
    self.locale = locale.downcase if locale.present?
  end
end
