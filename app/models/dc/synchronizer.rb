class DC::Synchronizer
  def self.synchronize_snippet(text)
    new(text.account).synchronize(text)
  end

  def self.delete_snippet(text)
    new(text.account).delete(text.identifier)
  end

  attr_reader :account

  def initialize(account)
    @account = account
  end

  def delete(identifier)
    snippet = snippets.find_by_identifier(identifier)
    snippet.destroy if snippet.present?
  end

  def synchronize(text)
    snippet = find_or_create_snippet!(text)

    text.variants.each do |variant|
      value = variant.value
      locale = variant.translation_locale

      translation = snippet.translate!(value,
        locale: locale,
        active: variant.active,
        created_at: variant.created_at,
        updated_at: variant.updated_at)

      snippet.set_default_translation!(translation) if variant.is_fallback?
    end

    translated_locales = text.locales.map { |locale| locale.locale.downcase }

    snippet.translations.each do |translation|
      translation.destroy unless translated_locales.include?(translation.locale)
    end

    snippet
  end

  private

  def find_or_create_snippet!(text)
    find_snippet(text.identifier) || create_snippet!(text)
  end

  def find_snippet(identifier)
    snippets.find_by_identifier(identifier)
  end

  def snippets
    DC::Snippet.for_account(account)
  end

  def create_snippet!(text)
    DC::Snippet.create!(
      account: account,
      identifier: text.identifier,
      name: text.name,
      created_at: text.created_at,
      updated_at: text.updated_at
    )
  end
end
