class EventDecoration < ActiveRecord::Base
  attr_accessible :account, :ticket, :ticket_id, :event, :data

  belongs_to :account
  belongs_to :ticket
  belongs_to :event

  validates_presence_of :account_id, :ticket, :event
  validates_uniqueness_of :event_id

  serialize :data

  def data
    Hashie::Mash.new(super)
  end

  def inactive?
    data.inactive
  end

  def deactivate
    self.data = data.to_h.merge(inactive: true)
    save!
  end
end
