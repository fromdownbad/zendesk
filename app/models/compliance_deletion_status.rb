class ComplianceDeletionError < StandardError; end

class ComplianceUserDeletionError < ComplianceDeletionError; end

class ComplianceDeletionStatus < ActiveRecord::Base
  belongs_to :account
  belongs_to :executer, class_name: "User"
  belongs_to :user

  has_many :compliance_deletion_feedback, inherit: :account_id

  REPUBLISH_WAIT_TIME = 7.days
  REPUBLISH_WAIT_TIME_LOGGING = 14
  # Maximum number of incomplete requests we'll republish in one job (one job per day)
  MAX_REPUBLISH = Integer(ENV.fetch('MAX_GDPR_REPUBLISH_PER_DAY', 150_000))

  # Needed for SNS message attributes
  ALL_APPLICATONS  = 'all'.freeze
  REQUEST_DELETION = 'request_deletion'.freeze

  # APPLICATONS
  AUDIT_LOGS       = 'audit_logs'.freeze
  CDP              = 'cdp'.freeze
  CHAT             = 'chat'.freeze
  CLASSIC          = 'classic'.freeze
  CONNECT          = 'connect'.freeze
  EXPLORE          = 'explore'.freeze
  GUIDE            = 'guide'.freeze
  NPS              = 'nps'.freeze
  PIGEON           = 'pigeon'.freeze
  TALK             = 'talk'.freeze
  TEXT             = 'text'.freeze
  VOYAGER          = 'voyager'.freeze

  APPLICATONS = [
    AUDIT_LOGS,
    CDP,
    CHAT,
    CLASSIC,
    CONNECT,
    EXPLORE,
    GUIDE,
    NPS,
    PIGEON,
    TALK,
    TEXT,
    VOYAGER
  ].freeze

  REQUIRED_APPS_PROD = [
    CDP,
    CHAT,
    CLASSIC,
    CONNECT,
    GUIDE,
    NPS,
    PIGEON,
    TALK,
    TEXT,
    VOYAGER
  ].freeze

  REQUIRED_APPS_STAGING = [
    CDP,
    CLASSIC,
    GUIDE,
    NPS,
    PIGEON,
    TALK,
    TEXT,
    VOYAGER
  ].freeze

  REQUIRED_APPS_DEV = [CLASSIC].freeze

  APPLICATIONS_REQUIRING_FEEDBACK = if Rails.env.staging?
    REQUIRED_APPS_STAGING
  elsif Rails.env.development?
    REQUIRED_APPS_DEV
  else
    REQUIRED_APPS_PROD
  end

  attr_accessible :executer, :user, :account

  validates :account_id,            presence: true
  validates :executer_id,           presence: true
  validates :user_id,               presence: true
  validates :user_id, uniqueness: { scope: :account_id, message: "Only one deletion request needed per user" }

  after_commit :publish_to_world, on: :create

  scope :incomplete, -> { where(complete: false) }
  scope :republishable, -> { incomplete.where("updated_at <= ?", REPUBLISH_WAIT_TIME.ago) }

  class << self
    def create_request!(user:, executer:)
      ActiveRecord::Base.transaction do
        status = create!(
          account: user.account,
          executer: executer,
          user: user
        )

        ComplianceDeletionFeedback.create_feedback!(status: status)
      end
    end

    def republish_incomplete_statuses!(max_republish: MAX_REPUBLISH, shard_id:)
      outstanding_requests = incomplete.where("created_at <= ?", REPUBLISH_WAIT_TIME.ago).count
      statsd_client.distribution('requests_incomplete', outstanding_requests, tags: ["shard_id:#{shard_id}"])

      requests_overdue = republishable.count
      statsd_client.distribution('requests_overdue', requests_overdue, tags: ["shard_id:#{shard_id}"])

      requests_republished = 0
      records_found = true
      # we want to select the requests that have not been published the longest (based on age of updated_at on the initial request),
      # up to the max:
      while records_found && requests_republished < max_republish
        records_found = false
        republishable.order(:updated_at).limit(1000).each do |status|
          Rails.logger.info("Processing GDPR republish for account:#{status.account_id}, user:#{status.user_id}")
          records_found = true

          unless skip_republish?(status, shard_id)
            applications = status.incomplete_applications

            if applications.empty?
              status.check_completeness!
            else
              status.publish_to_incomplete!(applications)
              status.log_app_isnt_complete_for(applications, shard_id)
            end
          end

          status.touch # we published to SNS wait 7 days to republish
          requests_republished += 1
          break if requests_republished >= max_republish
        end
      end
      statsd_client.count('unpublished_requests_overdue', [requests_overdue - requests_republished, 0].max, tags: ["shard_id:#{shard_id}"])
    end

    def sla_tag(seconds)
      if seconds <= 14.days
        :within_expected
      elsif seconds <= 30.days
        :within_sla
      else
        :outside_sla
      end
    end

    private

    def statsd_client
      @statsd_client ||= ::Zendesk::StatsD::Client.new(namespace: ['gdpr', 'compliance_deletion_status'])
    end

    def skip_republish?(status, shard_id)
      if status.account.nil?
        # skip soft_deleted accounts for now
        statsd_client.increment('skip_republish', tags: ["account:#{status.account_id}", "reason:soft_deleted"])
        Rails.logger.info("Skip GDPR republish for account:#{status.account_id}, user:#{status.user_id}. Account soft_deleted")
        true
      elsif status.account.shard_id != shard_id
        statsd_client.increment('skip_republish', tags: ["account:#{status.account_id}", "reason:wrong_shard"])
        Rails.logger.info("Skip GDPR republish for account:#{status.account_id}, user:#{status.user_id}. Wrong shard. shard_id:#{shard_id}")
        true
      elsif !status.account.is_active? || !status.account.is_serviceable?
        # skip cancelled accounts for now
        statsd_client.increment('skip_republish', tags: ["account:#{status.account_id}", "reason:cancelled"])
        Rails.logger.info("Skip GDPR republish for account:#{status.account_id}, user:#{status.user_id}. Account cancelled")
        true
      else
        false
      end
    end
  end

  def log_app_isnt_complete_for(applications, shard_id)
    seconds_since_request = Time.now - created_at
    sla_bucket = ComplianceDeletionStatus.sla_tag(seconds_since_request)

    applications.each do |application|
      tags = ["application:#{application}", "account_id:#{account_id}", "sla_bucket:#{sla_bucket}", "shard_id:#{shard_id}"]
      statsd_client.distribution('time_since_request', seconds_since_request.to_i, tags: tags)
      statsd_client.increment('request_republish', tags: tags)
    end

    elapsed_days = (seconds_since_request / 1.day).to_i
    # Wait until day 14 (2nd republish cycle) to log messages
    return if elapsed_days < REPUBLISH_WAIT_TIME_LOGGING

    outside_sla = elapsed_days > 30
    message = "Failed to execute Deletion for user: #{user_id}, applications: #{applications}, account: #{account.idsub}, days_since_initial_request: #{elapsed_days}"

    statsd_client.event('GDPR deletions has not been performed in the expected timeframe', message, alert_type: 'warning', tags: ["outside_sla:#{outside_sla}"])
    Rails.logger.warn(message)
  end

  def publish_to_incomplete!(applications)
    sns_publisher = Zendesk::PushNotifications::Gdpr::GdprSnsUserDeletionPublisher.new
    Zendesk::PushNotifications::Gdpr::GdprPublisher.new(self, sns_publisher).publish_to_incomplete!(applications)
  end

  def publish_to_world
    sns_publisher = Zendesk::PushNotifications::Gdpr::GdprSnsUserDeletionPublisher.new
    Zendesk::PushNotifications::Gdpr::GdprUserDeletionPublisher.new(self, sns_publisher).publish
    statsd_client.increment('request_publish', tags: ['success:true'])
  rescue StandardError => e
    statsd_client.increment('request_publish', tags: ['success:false', "error_type:#{e.class}"])
    Rails.logger.error("Failed to publish GDPR SNS notification for user:#{user.id}, account:#{account.idsub}, status_id:#{id}, error:#{e.inspect}")

    # no op. The deletion request has been persisted to db, but we encountered an error
    # when trying to publish the deletion request to SNS. A background job will eventually re-publish
    # to SNS.
  end

  def complete_applications
    @complete_applications ||= compliance_deletion_feedback.required.complete.pluck(:application)
  end

  def incomplete_applications
    @incomplete_applications ||= compliance_deletion_feedback.required.incomplete.pluck(:application)
  end

  def received_required_feedback?
    incomplete_applications.empty?
  end

  def check_completeness!
    complete! if received_required_feedback?
  end

  def complete!
    return true if complete? # if true then this is a backfill

    seconds_to_complete = Time.now - created_at
    instrument_completion(seconds_to_complete)
    update_attribute(:complete, true)
  end

  private

  def instrument_completion(seconds_to_complete)
    tags = ["completion_bucket:#{ComplianceDeletionStatus.sla_tag(seconds_to_complete)}"]
    statsd_client.distribution('time_to_complete', seconds_to_complete.to_i, tags: tags)
    statsd_client.increment('request_completed', tags: tags)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['gdpr', 'compliance_deletion_status'])
  end
end
