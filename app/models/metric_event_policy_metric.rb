class MetricEventPolicyMetric < ActiveRecord::Base
  attr_accessible :account, :policy_metric
  belongs_to :account
  belongs_to :metric_event,
    class_name:  'TicketMetric::Event',
    foreign_key: 'metric_event_id',
    inherit: :account_id
  belongs_to :policy_metric, class_name: 'Sla::PolicyMetric', inherit: :account_id
end
