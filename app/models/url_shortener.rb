# This is a convenience model that is backed by an
# AccountPropertySet. It only exposes URL-shortener-related
# attributes and has its own validations.
class UrlShortener < ActiveRecord::Base
  SHORTENERS = ActiveSupport::OrderedHash.new.tap do |h|
    h['Default'] = 'default'
    h['Bit.ly']  = 'bitly'
    h['Is.gd']   = 'isgd'
    h['Ow.ly']   = 'owly'
    h['Custom']  = 'custom'
  end.freeze

  belongs_to :account

  validates_presence_of :name,     if: :url_shortening_on?
  validates_inclusion_of :name,    in: SHORTENERS.values
  validates_presence_of :username, if: :is_bitly?
  validates_presence_of :api_key,  if: :is_bitly_or_owly?
  validates_presence_of :url,      if: :is_custom_shortener?

  attr_accessible :name, :username, :api_key, :url, :add_url_by_default

  def short_url_length
    url_service_length = { isgd: 19, owly: 18 }
    url_service_length.default = 21
    url_shortening_on? ? url_service_length[account.url_shortener.name.to_sym] : 0
  end

  protected

  def url_shortening_on?
    !!account.try(:url_shortening_on?)
  end

  def is_bitly? # rubocop:disable Naming/PredicateName
    url_shortening_on? && name == 'bitly'
  end

  def is_bitly_or_owly? # rubocop:disable Naming/PredicateName
    url_shortening_on? && (name == 'bitly' || name == 'owly')
  end

  def is_custom_shortener? # rubocop:disable Naming/PredicateName
    url_shortening_on? && name == 'custom'
  end
end
