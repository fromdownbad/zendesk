class HelpCenterTicket
  def initialize(ticket)
    @ticket = ticket
  end

  def post_id
    metadata['post_id']
  end

  def post_name
    metadata['post_name']
  end

  def post_url
    "#{brand.url}#{metadata['post_path']}"
  end

  private

  def brand
    @ticket.brand || @ticket.account.default_brand
  end

  def metadata
    @metadata ||= @ticket.initial_audit.metadata.fetch('custom', {})
  end
end
