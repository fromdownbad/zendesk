require 'zendesk/models/user_voice_forwarding_identity'

class UserVoiceForwardingIdentity < UserIdentity
  include Zendesk::Serialization::UserPhoneNumberIdentitySerialization
  include Voice::Core::Support::UserVoiceForwardingIdentity
  include Voice::Core::NumberSupport

  validate :acceptable_number

  after_find :uncast_from_uniq
  before_save :cast_to_uniq
  before_save :set_verified
  after_save :uncast_from_uniq
  after_commit :mark_unavailable_for_phone, on: :destroy

  def self.identity_type
    :agent_forwarding
  end

  def mark_unavailable_for_phone
    api_client.connection.post(
      "/api/v2/channels/voice/internal/availabilities/agent_forwarding_number_removed.json",
      id: user.id
    )
  rescue ZendeskAPI::Error::RecordNotFound => unknown_agent_error
    ZendeskExceptions::Logger.record(unknown_agent_error, location: self, message: "Trying to mark unknown agent via client failed", fingerprint: '39fa9920fa9b47ca7190d6e9b0adea33c0da9943')
  rescue ZendeskAPI::Error::NetworkError => network_error
    ZendeskExceptions::Logger.record(network_error, location: self, message: "Impossible to mark agent via client for Voice due to Voice Error", fingerprint: 'cf1485919724e2fe5c72fb8e571c1438fb36a204')
  end

  def to_s
    has_attribute?(:value) ? sans_uniq : super
  end

  private

  def acceptable_number
    # We can't use value_changed? here because of the 'uncast_from_uniq' and 'case_to_uniq' callbacks.
    # We plan to remove these in a future PR.
    return true if value == @original
    if emergency_number?
      errors.add(:value, I18n.t('activerecord.errors.voice.agent_forwarding.no_emergency_numbers'))
    elsif !valid_format?
      # Phone numbers should remain LTR in RTL languages, and the plus sign should also
      # remain to the left of the number.
      errors.add(:value, I18n.t('activerecord.errors.models.user_identity.attributes.value.invalid', value: "\u202D#{value}\u202C"))
    elsif !zendesk_country
      errors.add(:base, I18n.t('activerecord.errors.voice.agent_forwarding.please_contact_support'))
    end
  end

  def zendesk_country
    extract_country_code = ->(country) { country[:country_code] }
    account.settings.voice_make_calls_outside_zendesk_countries || (callable_countries(account).map(&extract_country_code) & country_from_number(value).map(&:code)).present?
  end

  def valid_format?
    Voice::Core::NumberSupport::E164Number.plausible?(value)
  end

  def set_verified
    self.is_verified = true
  end

  # This is a hack to allow multiple identities with the same value
  # due to the user_identities table having an incorrect index on
  # [account_id, value] in prod instead of [account_id, type, value]
  # https://gist.github.com/52854435f8abe4c3e08a

  def cast_to_uniq
    return unless has_attribute?(:value)
    self.value = "#{value}||UNIQ||"
  end

  def uncast_from_uniq
    return unless has_attribute?(:value)
    self.value = sans_uniq
    @original = value
  end

  def sans_uniq
    value.sub(/\|\|UNIQ\|\|$/, '')
  end

  def uniqueness_of_value
    return if value && value.each_byte.detect { |byte| byte > 127 }

    conditions = ["account_id = ? AND value = ? AND type = ?", account_id, "#{value}||UNIQ||", self.class.name]
    unless new_record?
      conditions[0] << " AND id <> ?"
      conditions << id
    end

    if self.class.exists?(conditions)
      errors.add(:value, :taken, value: value_for_error)
    end
  end

  def api_client
    @api_client ||= Zendesk::InternalApi::Client.new(account.subdomain)
  end
end
