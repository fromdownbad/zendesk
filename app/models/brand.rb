require 'zendesk/models/brand'
require 'zendesk/models/channels_brand'

# Every account has one or more brands.
# By default each account has one brand when they signup for zendesk.
# Until this feature is fully launched, accounts must have "multibrand" feature enabled in Arturo.
#
# REFERENCE INFO:
# - https://zendesk.atlassian.net/wiki/display/PGM/Multibrand+-+Project+Page
#
#          ------------------------------
#          |   SatisfactionReason       |
#          |                            |
#          |                            |
#          |                            |
#          ------------------------------
#                         |
#                        / \
#          --------------------------------------      ------------------------
#          | SatisfactionReasonBrandRestriction |      |  BrandLogo           |
#          |                                    |      |                      |
#          |                                    |      |                      |
#          |                                    |      |                      |
#          --------------------------------------      ------------------------
#                                         \ /             | 1
#                                          |              |
#  ------------------------------     ------------------------       ---------------------------
#  | TicketFormBrandRestriction |     |    Brand             |------<|    RecipientAddress     |
#  |                            |>----|                      |       |  default                |
#  |                            |     | name                 |       ---------------------------
#  |                            |     | route_id             |       ------------------------
#  ------------------------------     | account_id           |       |    Ticket            |
#                \ /                  |                      |------<|  account_id          |
#                 |                   |                      |       |                      |
#  ------------------------------     |                      |       ------------------------
#  |   TicketForm               |     |                      |       --------------------------
#  |                            |     |                      |------<|   ChannelsBrand        |
#  |                            |     |                      |       |                        |
#  |                            |     |                      |       |  account_id            |
#  ------------------------------     |                      |       |  brandable (type & id) |
#                                     |                      |       --------------------------
#  ------------------------------     |                      |                  | 1
#  |   Account                  |----<|                      |                  |
#  |                            |     ------------------------       -------------------------------
#  | route_id                   |                | 1                 |    Brandable                |
#  |                            |                |                   | sample brandable models are |
#  |                            |     ------------------------       |   Facebook::Page            |
#  |                            |----<|     Route            |       |   MonitoredTwitterHandle    |
#  |                            |     |                      |       |   Voice::PhoneNumber        |
#  ------------------------------     |  subdomain           |       |                             |
#                                     |  account_id          |       |                             |
#                                     ------------------------       -------------------------------
#
# SEARCH TERMS: Brands brands ChannelsBrands channels_brands Brandable brandable routes Routes
#
# NOTE: ChannelsBrand model is in the zendesk_core repo.
#      https://github.com/zendesk/zendesk_core/blob/master/lib/zendesk/models/channels_brand.rb
#
# NOTE: The current modeling is not geared toward branding individuals within an account.
#
# FUNCTIONALITY:
#   By default brands do not restrict TicketForms or SatisfactionReasons that can be used for tickets.
#   These are restricted via the TicketFormBrandRestriction & SatisfactionReasonBrandRestriction associations
#
#   Every brand has its own URL via the Route model
#
#   The route_id on the accounts table represents the route that matches the account's main subdomain
#     that agents use to log into their account.
#

class Brand < ActiveRecord::Base
  MAXIMUM_ACTIVE_BRANDS = 5
  DEFAULT_SIGNATURE_TEMPLATE = "{{agent.signature}}".freeze

  has_soft_deletion default_scope: true

  include CIA::Auditable
  include CachingObserver
  include BrandEntityObserver
  include HelpCenterSupport
  include ArSkippedCallbackMetrics::Instrument

  has_kasket
  has_kasket_on :id, :deleted_at
  has_kasket_on :account_id, :deleted_at
  has_kasket_on :route_id, :deleted_at

  # Don't move the following two lines. The first line changes the current reflection
  # (defined in zendesk_core) to autosave, the second line forces the validations to be defined
  # via a new call to add_autosave_association_callbacks
  reflect_on_association(:route).options[:autosave] = true
  belongs_to :route, autosave: true, inherit: :account_id

  has_many :tickets
  has_many :channels_brand
  has_many :recipient_addresses, autosave: true, inherit: :account_id
  has_many :facebook_pages,  class_name: "Facebook::Page",         through: :channels_brand, source: :brandable, source_type: "Facebook::Page"
  has_many :twitter_handles, class_name: "MonitoredTwitterHandle", through: :channels_brand, source: :brandable, source_type: "MonitoredTwitterHandle"
  has_many :phone_numbers,   class_name: "Voice::PhoneNumber",     through: :channels_brand, source: :brandable, source_type: "Voice::PhoneNumber"
  has_many :ticket_form_brand_restrictions, inherit: :account_id
  has_many :restricted_ticket_forms, through: :ticket_form_brand_restrictions, source: :ticket_form, inherit: :account_id
  has_many :unrestricted_ticket_forms, -> { where(in_all_brands: true) }, foreign_key: :account_id, primary_key: :account_id, class_name: 'TicketForm', inherit: :account_id
  has_many :satisfaction_reason_brand_restrictions, class_name: 'Satisfaction::ReasonBrandRestriction', inherit: :account_id
  has_many :satisfaction_reasons, inherit: :account_id, through: :satisfaction_reason_brand_restrictions, class_name: 'Satisfaction::Reason'
  has_one  :default_recipient_address,
    -> { where(default: true) },
    inverse_of: :account,
    class_name: "RecipientAddress",
    inherit: :account_id
  has_one :logo, class_name: "BrandLogo", inverse_of: :brand, dependent: :destroy, inherit: :account_id

  attr_accessible :subdomain, :host_mapping, :signature_template

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }

  before_validation :build_recipient_address, on: :create
  after_validation :delete_orphaned_route, on: :create

  before_soft_delete :validate_can_be_deleted
  before_soft_delete :deactivate_as_part_of_deletion_process
  after_soft_delete :destroy_recipient_addresses
  after_destroy     :destroy_recipient_addresses
  after_soft_delete :update_channel_brands
  after_soft_delete :rename_route_for_deleted_brand
  after_soft_delete :destroy_ticket_form_brand_restrictions
  after_commit      :reassign_brand_on_delete

  validates_presence_of :account_id, :name, :route
  validates_uniqueness_of :name, scope: [:account_id, :deleted_at]
  validates_uniqueness_of :route_id
  validates_associated :logo, if: proc { |brand| brand.logo.present? && brand.logo.new_record? }
  validate :validate_active_state
  validate :under_active_brands_limit
  validate :validate_signature_template
  validate :ensure_under_brand_limit, on: :create

  audit_attribute :active, :deleted_at, :signature_template

  delegate :subdomain=, :host_mapping=, to: :route

  def set_brand_logo(data) # rubocop:disable Naming/AccessorMethodName
    if !data || data[:uploaded_data].blank? || data[:uploaded_data].size.zero?
      logo.destroy if logo
    else
      build_logo(uploaded_data: data[:uploaded_data])
      logo.account_id = account_id
    end
  end

  def default?
    account.default_brand_id == id
  end

  def route_is_agent_route?
    account.route_id == route_id
  end

  def can_be_deactivated?
    !default? && !route_is_agent_route?
  end

  def to_liquid
    Zendesk::Liquid::Wrapper.new("name").merge!("name" => name)
  end

  def reply_address
    default_recipient_address&.email || backup_email_address
  end

  def backup_email_address
    @backup_email_address ||= "support@#{route.default_host}"
  end

  def host_mapping_not_secured_by_cert?
    host_mapping &&
      account.active_cert &&
      !account.active_cert.crt_object_secured_domains.include?(host_mapping)
  end

  def has_email_configured? # rubocop:disable Naming/PredicateName
    recipient_addresses.size > 1
  end

  def has_facebook_configured? # rubocop:disable Naming/PredicateName
    !facebook_pages.active.empty?
  end

  def has_twitter_configured? # rubocop:disable Naming/PredicateName
    !twitter_handles.active.empty?
  end

  def has_phone_configured? # rubocop:disable Naming/PredicateName
    !phone_numbers.empty?
  end

  def invalid_subdomain_change?(current_user)
    if route.subdomain_changed? && route_is_agent_route?
      return !current_user.is_account_owner? || account.is_sandbox?
    end

    false
  end

  def ticket_form_ids
    (unrestricted_ticket_forms + restricted_ticket_forms).map(&:id).uniq
  end

  def ticket_forms
    account.ticket_forms.where(id: ticket_form_ids)
  end

  def inactive_satisfaction_reasons
    account.satisfaction_reasons.selectable - satisfaction_reasons
  end

  private

  def reassign_brand_on_delete
    return unless previous_changes["deleted_at"] && previous_changes["deleted_at"][0].nil?
    TicketBrandReassignJob.enqueue(account_id, id)
  end

  def destroy_recipient_addresses
    recipient_addresses.each(&:force_destroy)
  end

  def destroy_ticket_form_brand_restrictions
    ticket_form_brand_restrictions.destroy_all
  end

  def update_channel_brands
    channels_brand.each do |channels_brand|
      channels_brand.brand_id = account.default_brand_id
      channels_brand.save!
    end
  end

  def delete_orphaned_route
    route.delete if errors.any? && route
  end

  def validate_active_state
    if !new_record? && active_changed? && !active
      if default?
        errors.add(:base, I18n.t('txt.admin.model.brand.cannot_make_default_brand_inactive'))
      elsif route_is_agent_route?
        errors.add(:base, I18n.t('txt.admin.model.brand.cannot_make_agent_brand_inactive'))
      end
    end
  end

  def validate_signature_template
    if signature_template_changed?
      begin
        Liquid::Template.parse(signature_template)
      rescue StandardError => e
        errors.add(:base, I18n.t('txt.admin.model.brand.signature_template_invalid', message: e.message))
      end
    end
  end

  def under_active_brands_limit
    return if account.blank? || account.has_unlimited_multibrand?

    if account.active_brands.count(:all) >= MAXIMUM_ACTIVE_BRANDS
      if new_record? && active?
        errors.add(:base, I18n.t('txt.admin.model.brand.cannot_create_to_exceed_maximum_active_brands'))
      elsif active_was == false && active?
        errors.add(:base, I18n.t('txt.admin.model.brand.cannot_update_to_exceed_maximum_active_brands'))
      end
    end
  end

  def rename_route_for_deleted_brand
    route.host_mapping = nil
    route.subdomain = "#{subdomain}-zdeleted#{Time.now.to_i}"
    route.save(validate: false)
  end

  def validate_can_be_deleted
    if default?
      errors.add(:base, I18n.t('txt.admin.model.brand.cannot_delete_default_brand'))
      false
    elsif route_is_agent_route?
      errors.add(:base, I18n.t('txt.admin.model.brand.cannot_delete_agent_brand'))
      false
    end
  end

  def ensure_under_brand_limit
    if account && !account.under_maximum_brands_limit?
      errors.add(:base, I18n.t('txt.admin.model.brand.cannot_add_more_brands', number_of_brands: account.brands.count))
    end
  end

  def deactivate_as_part_of_deletion_process
    self.active = false

    true
  end

  def build_recipient_address
    return if recipient_addresses.any? || route.blank? # Get called twice in Rails 4.1

    recipient_addresses.build(email: "support@#{route.subdomain && route.default_host}", default: true) do |ra|
      ra.name = name
      ra.account_id = account_id
      ra.creating_for_new_brand = true
      ra.brand = self
    end
  end
end
