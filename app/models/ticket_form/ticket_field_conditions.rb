# Code related to ticket field conditions in the context of a ticket form
class TicketForm
  module TicketFieldConditions
    def self.included(base)
      # Associations
      base.has_many :ticket_field_conditions, dependent: :destroy, inverse_of: :ticket_form, inherit: :account_id

      base.accepts_nested_attributes_for :ticket_field_conditions, allow_destroy: true

      # Callbacks
      base.after_soft_delete :destroy_conditions

      # Validations
      base.validate :limit_conditions_on_form
      base.validate :no_circular_conditions
      base.validate :validate_ticket_fields_in_conditions, on: :update, if: proc { |f|
        f.account.try(:has_native_conditional_fields_enabled?) && f.ticket_field_conditions.any?
      }
      base.validate :validate_ticket_field_limits, on: [:update, :create]
    end

    # Prepares agent conditions for API
    def agent_conditions
      shaped_conditions(:agent)
    end

    # Prepares end-user conditions for API
    def end_user_conditions
      shaped_conditions(:end_user)
    end

    def agent_conditions=(conditions)
      self.ticket_field_conditions_attributes =
        shaped_conditions_to_nested_attributes(
          agent_ticket_field_conditions,
          conditions,
          user_type: :agent
        )
    end

    def end_user_conditions=(conditions)
      self.ticket_field_conditions_attributes =
        shaped_conditions_to_nested_attributes(
          end_user_ticket_field_conditions,
          conditions,
          user_type: :end_user
        )
    end

    def agent_ticket_field_conditions
      @agent_ticket_field_conditions ||= conditions_for_user_type(:agent)
    end

    def end_user_ticket_field_conditions
      @end_user_ticket_field_conditions ||= conditions_for_user_type(:end_user)
    end

    private

    def conditions_for_user_type(user_type)
      ticket_field_conditions.find_all { |condition| condition.user_type.to_sym == user_type }
    end

    # Prepares conditions for API:
    # 1. Agent conditions
    # [
    #   {
    #     "parent_field_id": 102,
    #     "value": "matching_value_1",
    #     "child_fields": [
    #       {
    #         "id": 101,
    #         "is_required": false,
    #         "required_on_statuses": {
    #           "type": "SOME_STATUSES",
    #           "statuses": ["new", "open", "pending", "hold"]
    #         }
    #       },
    #       {
    #         "id": 200,
    #         "is_required": true,
    #         "required_on_statuses": {
    #           "type": "ALL_STATUSES",
    #         }
    #       },
    #       {
    #         "id": 201,
    #         "is_required": false,
    #         "required_on_statuses": {
    #           "type": "NO_STATUSES",
    #         }
    #       }
    #     ]
    #  }
    # ]
    #
    # 2. End-user conditions
    # [
    #   {
    #     "parent_field_id": 100,
    #     "value": "matching_value",
    #     "child_fields": [
    #       { id: 101, is_required: false },
    #       { id: 200, is_required: true }
    #     ]
    #  }
    # ]
    def shaped_conditions(user_type)
      conditions_for_user_type(user_type).group_by(&:parent_field_id).flat_map do |p_id, p_conditions|
        # Skip condition if parent field is missing/deleted
        next unless parent_field = ticket_field(p_id)
        p_conditions.group_by(&:value).map do |value, v_conditions|
          {
            parent_field_id: p_id,
            value: parent_field.try(:present_condition_value, value),
            child_fields: v_conditions.map do |c|
              child_field_attributes = {
                id: c.child_field_id,
                is_required: c.is_required?
              }
              if c.agent?
                child_field_attributes[:required_on_statuses] = c.shaped_required_on_statuses
              end
              child_field_attributes
            end
          }
        end
      end.compact
    end

    # Prepares condition attributes to be saved as part of the ticket form
    def shaped_conditions_to_nested_attributes(existing_conditions, input_conditions, options)
      # Reset cached conditions
      @agent_ticket_field_conditions = nil
      @end_user_ticket_field_conditions = nil

      existing_conditions_map = {}
      existing_conditions.each do |ec|
        key = [ec.parent_field_id, ec.value, ec.child_field_id]
        existing_conditions_map[key] = ec
      end

      flattened_input_conditions = input_conditions.flat_map do |ic|
        parent_field_id = ic[:parent_field_id]
        value = ic[:value]
        parent_field = ticket_field(parent_field_id)
        converted_value = parent_field ? parent_field.condition_value_to_db(value) : value
        ic[:child_fields].map do |cf|
          child_field_id = cf[:id]
          child_field = ticket_field(child_field_id)
          key = [parent_field_id, converted_value, child_field_id]
          existing_condition = existing_conditions_map.delete(key)

          condition_attributes = options.merge(
            id: existing_condition && existing_condition.id,
            account: account,
            parent_field: parent_field,
            value: value,
            child_field: child_field,
            is_required: cf[:is_required]
          )

          if cf[:required_on_statuses].present?
            condition_attributes[:shaped_required_on_statuses] = cf[:required_on_statuses]
          end

          condition_attributes
        end
      end

      # Remaining conditions in map are not in the input_conditions and should be deleted.
      conditions_to_destroy = existing_conditions_map.values.map do |c|
        { id: c.id, _destroy: true }
      end

      flattened_input_conditions + conditions_to_destroy
    end

    # We don't let customers delete/deactivate ticket fields being used in conditions.
    # TicketForm has soft deletion but TicketFieldCondition doesn't have that.
    # If we keep the conditions, the ticket field validations will stop changes on ticket fields
    # being used on old conditions that are part of a soft deleted form.
    def destroy_conditions
      ticket_field_conditions.delete_all
    end

    def limit_conditions_on_form
      max_conditions = account.settings.maximum_user_conditions_per_form
      if agent_ticket_field_conditions.size >= max_conditions || end_user_ticket_field_conditions.size >= max_conditions
        errors.add(:base, I18n.t('txt.admin.model.ticket_form.conditions_product_limit', conditions_limit: max_conditions))
      end
    end

    def no_circular_conditions
      return unless account.has_native_conditional_fields_enabled?
      validate_no_circular_conditions(agent_ticket_field_conditions)
      validate_no_circular_conditions(end_user_ticket_field_conditions)
    end

    # This method detects circular dependencies that will result in an unsubmittable form like:
    # Cond1: F1 => F2; Cond2: F2 => F3; Cond3: F3 => F1
    # In that case, all the fields will be hidden because F1 depends on F3 and F3
    # depends on F2 that at the same time depends on F1. To do that, we collect
    # all the ancestors for each child while checking that the ancestors do not
    # contain the child itself.
    #
    # Important: make sure to ignore conditions marked for destruction
    def validate_no_circular_conditions(conditions)
      valid_conditions = conditions.reject { |c| c.marked_for_destruction? || c.parent_field_id.nil? || c.child_field_id.nil? }
      CircularConditionsDetector.new(self, valid_conditions).execute
    end

    def validate_ticket_fields_in_conditions
      fields_in_use = ticket_field_conditions.pluck(:parent_field_id, :child_field_id).flatten.compact.uniq

      # Make sure all fields used in conditions on this ticket form are still present.
      fields_to_destroy = ticket_form_fields.select(&:marked_for_destruction?).map(&:ticket_field_id)
      if (destroyed_fields_needed = fields_to_destroy & fields_in_use).any?
        field_needed = account.ticket_fields.find_by_id(destroyed_fields_needed.first)
        errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_removed_because_conditions', field_name: field_needed.try(:title)))
      end
      true
    end

    # Topological sort implementation to detect circular dependencies in form conditions.
    #
    # Uses TSort library to get a list of strongly connected components (SCCs).
    # SCCs can be seen as self contained cycles in a graph.
    #   * SCCs of size 1:
    #     - when condition has different parent/child: no cycle.
    #     - when condition has same parent/child: cycle in a single condition (F1 => F1).
    #   * SCCs of size > 1: cycle between 2 or multiple conditions (F1 => F2; F2 => F3; F3 => F1).
    #
    # When a circular condition (cycle) is detected, an error is added to the given ticket form to show
    # the following validation message:
    #
    # Conditional ticket fields cannot have inter-dependencies: F1 and F2 cannot depend on each other.
    #
    # Usage:
    #   CircularConditionsDetector.new(ticket_form, conditions).execute
    #
    class CircularConditionsDetector
      include TSort

      attr_accessor :conditions, :ticket_form

      def initialize(ticket_form, conditions)
        @ticket_form = ticket_form
        @conditions = conditions
      end

      def execute
        strongly_connected_components.each do |scc|
          next if scc.size == 1 && scc.first.parent_field_id != scc.first.child_field_id

          # Cycle detected. Add only one error message to ticket form.
          add_error(scc)
          break
        end
      end

      # Implements TSort#tsort_each_node.
      def tsort_each_node(&block)
        conditions.each(&block)
      end

      # Implements TSort#tsort_each_child.
      def tsort_each_child(node, &block)
        conditions.select do |condition|
          condition.child_field_id == node.parent_field_id
        end.each(&block)
      end

      private

      def add_error(scc)
        # find best combination of fields to show in the error message.
        field_id_1 = scc.last.child_field_id
        field_id_2 = if scc.first.parent_field_id == scc.last.child_field_id
          scc.first.child_field_id
        else
          scc.first.parent_field_id
        end

        ticket_form.errors.add(:base,
          ::I18n.t('txt.admin.model.ticket_form.circular_conditions',
            field_1: field_title(field_id_1),
            field_2: field_title(field_id_2)))
      end

      def field_title(field_id)
        ticket_form.ticket_field(field_id)&.title
      end
    end
  end
end
