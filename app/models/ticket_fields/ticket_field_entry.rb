require 'zendesk/redacted_credit_card_number_validator'
require 'safe_regexp'

class TicketFieldEntry < ActiveRecord::Base
  include ZendeskArchive::Archivable
  include Zendesk::TicketFieldErrors
  include Zendesk::SafeRegexpReporting

  include Zendesk::CreditCardRedaction
  include ViewsObservers::TicketFieldObserver

  credit_card_redaction_attrs :value

  belongs_to        :account
  belongs_to        :ticket, inverse_of: :ticket_field_entries, inherit: :account_id
  belongs_to        :ticket_field, inherit: :account_id
  before_validation :normalize_checkbox_values
  before_validation :set_account
  before_validation :log_missing_ticket_field, if: :persisted?
  before_save       :set_account

  validates_presence_of   :account_id
  validates_presence_of   :ticket_field, if: :new_record?
  validates_uniqueness_of :ticket_field_id, scope: :ticket_id, if: :ticket_exists?, on: :create

  validate :validate_ticket_field_is_not_a_system_field
  validate :validate_value_matches_regexp_for_validation_if_both_are_present
  validate :validate_account_id_matches_ticket_account_id
  validate :validate_partial_credit_card_value

  validates_lengths_from_database only: :value

  has_many :redaction_events, foreign_key: :value, class_name: "TicketFieldRedactEvent", inherit: :account_id

  attr_accessible :value, :account, :ticket_field, :ticket, :ticket_field_id

  def to_xml(options = {})
    return '' if options[:for_end_user] && !ticket_field.is_visible_in_portal?
    options[:only] = [:ticket_field_id, :value]
    super
  end

  def as_json(options = {})
    return '' if options[:for_end_user] && !ticket_field.is_visible_in_portal?
    options[:only] = [:ticket_field_id, :value]
    super(options)
  end

  def redact_credit_card_numbers!
    if [FieldText, FieldTextarea].include?(ticket_field.class) && super
      redaction_events.build(audit: ticket.audit)
      ticket.add_redaction_tag!
    end
  end

  private

  def ticket_exists?
    ticket.present? && !ticket.new_record?
  end

  def set_account
    self.account_id ||= ticket.account_id if ticket_id.present?
  end

  def normalize_checkbox_values
    if ticket_field.is_a?(FieldCheckbox)
      self.value = Zendesk::DB::Util.truthy?(value) ? '1' : '0'
    end
  end

  def validate_ticket_field_is_not_a_system_field
    if TicketField::SYSTEM_FIELDS.include?(ticket_field.class.to_s)
      add_error('txt.admin.models.ticket_field.ticket_field_entry.cannot_be_saved_as_ticket_field')
    end
  end

  def validate_value_matches_regexp_for_validation_if_both_are_present
    if value.present? && ticket_field && ticket_field.regexp_for_validation.present?
      regexp = Regexp.new(ticket_field.regexp_for_validation)
      if account.has_use_safe_regexp?
        begin
          add_error(:invalid) unless SafeRegexp.execute(regexp, :match?, value.to_s)
          safe_regexp_success
        rescue SafeRegexp::RegexpTimeout => e
          safe_regexp_error(e, account_id, ticket_field.id)
          add_error('txt.api.v2.ticket_fields.slow_regex_field_matches')
        rescue Errno::ENOMEM => e
          safe_regexp_error(e, account_id, ticket_field.id)
          add_error(:invalid) unless regexp.match(value.to_s)
        end
      else
        add_error(:invalid) unless regexp.match(value.to_s)
      end
    end
  end

  def validate_account_id_matches_ticket_account_id
    if !account_id.present?
      add_error('txt.admin.models.ticket_field.ticket_field_entry.must_have_account_id')
    elsif ticket.present? && account_id != ticket.account_id
      add_error('txt.admin.models.ticket_field.ticket_field_entry.account_id_must_match')
    end
  end

  def validate_partial_credit_card_value
    return unless ticket_field.is_a?(FieldPartialCreditCard) && value.present?
    if Zendesk::RedactedCreditCardNumberValidator.too_short?(value)
      add_error('txt.admin.models.ticket_field.ticket_field_entry.partial_credit_card_field_is_too_short')
    elsif Zendesk::RedactedCreditCardNumberValidator.too_long?(value)
      add_error('txt.admin.models.ticket_field.ticket_field_entry.partial_credit_card_field_is_too_long')
    elsif !Zendesk::RedactedCreditCardNumberValidator.redacted?(value) && Zendesk::RedactedCreditCardNumberValidator.invalid_format?(value)
      add_error('txt.admin.models.ticket_field.ticket_field_entry.partial_credit_card_field_is_invalid')
    elsif !Zendesk::RedactedCreditCardNumberValidator.redacted?(value)
      add_error('txt.admin.models.ticket_field.ticket_field_entry.partial_credit_card_field_is_unredacted')
    end
  end

  def add_error(error)
    add_to_ticket_field_errors(ticket_field, error, current_user: @current_user, current_account: account)
  end

  def log_missing_ticket_field
    unless ticket_field
      Rails.logger.warn("#{self.account_id}: TicketField #{ticket_field_id} does not exist. TicketFieldEntry ID: #{id},  value: #{value}")
    end
  end
end
