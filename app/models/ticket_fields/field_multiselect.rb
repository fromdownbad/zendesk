class FieldMultiselect < FieldTagger
  def explanation
    I18n.t('txt.admin.views.ticket_field.field_multiselect.explanation')
  end

  def aggregate_and_add_new_value(value, ticket)
    adding_value = account.fetch_custom_field_option_value(value)

    if ticket.present?
      # Actions currently only allows adding a multiselect option.
      # In order to add a tag then we need to send the current value of the field
      # plus the new value being added, otherwise the current values will all be removed
      # in ticket:set_ticket_field_entry
      current_field_value = ticket.ticket_field_entries.detect { |entry| entry.ticket_field_id == id }.try(:value).to_s.split(" ")
      adding_value.nil? ? current_field_value : current_field_value << adding_value
    else
      # Macro application in bulk update (no specific ticket).
      # Send an array with the option value or [] if there's no such an option.
      [adding_value].compact
    end
  end

  def normalize_value_and_sync_tags(ticket, value)
    # all the options that this field allows
    available_options = if account.has_cache_custom_field_option_values?
      custom_field_option_values
    else
      if account.has_special_chars_in_custom_field_options?
        custom_field_options.pluck(:enhanced_value)
      else
        custom_field_options.pluck(:value)
      end
    end
    # the existing selected options that we have for this field and ticket,
    # are stored as a space separated string in DB.
    # Convert to array here to compare with the new options
    existing_selected_options = ticket.ticket_field_entries.detect { |entry| entry.ticket_field_id == id }.try(:value).to_s.split

    # current tags related to the field:
    # * will be ["option1"] if we received "tags":["option1"] in same request
    # * same as existing_selected_options if we received no tags in request,
    # because we keep tags and ticket_field_entry always in sync
    current_tags_array = ticket.current_tags.to_s.split
    existing_related_tags = current_tags_array & available_options

    # normalize new value
    new_value = Array.wrap(value).map { |v| TagManagement.downcase_tag_string(v.to_s.strip, account) }

    # user can add tags with field options, let's see if there are new tags
    new_tags = current_tags_array - ticket.current_tags_was.to_s.split

    # to calculate the new options that we need to set in the field,
    # keep only the new tags that are related and the valid options
    new_options = (new_value + new_tags).uniq.sort & available_options

    # same options we had?, then no need to do anything
    return if new_options == existing_selected_options

    # clear current_tags by removing all things related to this field
    # and add tags for the new options
    ticket.remove_tags = (existing_related_tags + existing_selected_options).uniq
    ticket.additional_tags = new_options

    # return the new value for the ticket_field_entry as a space separated string
    new_options.join(' ')
  end

  def compare(compare_value, operator, cf_option_id)
    case operator
    when 'includes'
      includes_value?(compare_value, cf_option_id)
    when 'not_includes'
      !includes_value?(compare_value, cf_option_id)
    when "present"
      compare_value.present?
    when "not_present"
      compare_value.blank?
    else
      false
    end
  end

  def includes_value?(compare_value, cf_option_id)
    if cf_option_id.blank?
      compare_value.blank?
    elsif compare_value && (custom_field_option_value = account.fetch_custom_field_option_value(cf_option_id))
      curr_value = TagManagement.downcase_tag_string(" #{compare_value} ", account)

      TagManagement.
        downcase_tag_string(custom_field_option_value, account).
        split.
        any? { |word| curr_value.include?(" #{word} ") }
    else
      false
    end
  end
end
