class FieldCheckbox < TicketField
  # TODO: COLUMN_MIGRATION
  alias_attribute :default_value, :tag
  validate :validate_tags

  attr_accessible :default_value

  scope :active, -> { where(is_active: true) }

  include Zendesk::CustomField::Checkbox

  def normalize_value_and_sync_tags(ticket, value)
    # "true" and "false" are saved in the DB as 1 and 0.
    # Without normalizing, "true" != "1" resulting in a delta change
    value = normalize_value(value.to_s.strip)

    if tag.present?
      if Zendesk::DB::Util.truthy?(value)
        ticket.additional_tags = tag
      else
        ticket.remove_tags = tag
      end
    end

    value
  end

  protected

  def validate_tags
    return if tag.blank?
    if tag.index(' ') || tag =~ SPECIAL_CHARS || tag =~ /[^[:graph:]]/
      errors.add(:base, Api.error('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters', error: 'InvalidValue'))
    elsif account&.has_validate_pipes_for_tags? && tag =~ /\|/
      errors.add(:base, Api.error('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_character', error: 'InvalidValue'))
    end

    Zendesk::CustomField::DropdownTagger.validate_unique(errors, self, [tag])
  end
end
