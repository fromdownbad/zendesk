class FieldPriority < TicketField
  I18N_TITLE_KEY = "txt.default.fields.priority.title".freeze
  I18N_DESCRIPTION_KEY = "txt.default.fields.priority.description".freeze

  validate :validate_conditions, on: :update

  def multilingual_field(args = {})
    args[:key] = multilingual_key(args[:field_type])
    super
  end

  def validate_conditions
    return true unless sub_type_id_changed?
    # only changes from FULL to BASIC can affect conditions
    return true unless sub_type_id == PrioritySet.BASIC
    return true unless account.try(:has_native_conditional_fields_enabled?)

    # only conditions with priority as the parent field and with value Low/Urgent can be affected
    any_invalid_conditions = TicketFieldCondition.by_ticket_field(self).any? do |c|
      c.parent_field_id == id &&
      (c.value == PriorityType.LOW.to_s || c.value == PriorityType.URGENT.to_s)
    end

    if any_invalid_conditions
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.priority_type_cannot_be_changed_by_condition'))
    end
  end
end
