class FieldDate < TicketField
  include Zendesk::FieldCompare::Date

  def time_zone
    account.time_zone
  end
end
