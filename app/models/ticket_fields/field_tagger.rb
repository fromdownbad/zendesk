class FieldTagger < TicketField
  include Zendesk::Serialization::FieldTaggerSerialization
  include Zendesk::CustomField::DropdownTagger

  attr_accessor :sort_alphabetically
  has_many :custom_field_options, -> { order('position, id ASC') }, foreign_key: :custom_field_id, dependent: :delete_all, autosave: true, inherit: :account_id, inverse_of: :custom_field

  attr_accessible :custom_field_options

  validate :validate_custom_field_options_are_not_empty
  validate :validate_custom_field_options
  validate :validate_custom_field_options_count
  validate :validate_default_uniqueness

  scope :active, -> { where(is_active: true) }

  before_validation :assign_account_to_custom_field_options
  # NOTE: This callback commits the custom_field_options collection directly to the db.
  # Because of that, any callbacks in CustomFieldOption that happen after around_save,
  # like before_create, after_save or after_commit, will NOT BE EXECUTED.
  around_save :bulk_insert_or_update_custom_field_options
  after_save :remove_deleted

  def validate_custom_field_options_count
    return unless account.has_custom_field_options_limit?
    return if account.created_at < DateTime.parse("2022-6-1")
    if custom_field_options.count > account.settings.custom_field_options_limit
      errors.add(:base, Api.error('txt.admin.models.ticket_fields.field_tagger.must_not_contain_too_many_options', number_of_options: account.settings.custom_field_options_limit))
    end
  end

  def self.taggers_tag_to_name_lookup(account)
    Rails.cache.fetch("#{account.scoped_cache_key(:field_taggers)}-lookup") do
      lookup = {}
      account.fetch_active_taggers.each do |tagger|
        tagger.custom_field_options.each { |o| lookup[o.value] = o.name }
      end
      lookup
    end
  end

  def custom_field_option_values
    @custom_field_option_values ||= begin
      Rails.cache.fetch("#{account.scoped_cache_key([:field_tagger, id])}-values") do
        if account.has_special_chars_in_custom_field_options?
          custom_field_options.map(&:enhanced_value)
        else
          custom_field_options.map(&:value)
        end
      end
    end
  end

  def expire_scoped_caches
    super
    account.expire_scoped_cache_key(:field_taggers)
    account.expire_scoped_cache_key([:field_tagger, id])
  end

  def normalize_value_and_sync_tags(ticket, value)
    # normalize value
    value = TagManagement.downcase_tag_string(value.to_s.strip, account)

    # return if not among available options
    return if value.present? && !option_belongs_to_tagger(value)

    ticket.remove_tags = ticket.ticket_field_entries.detect { |entry| entry.ticket_field_id == id }.try(:value) if value.blank?
    ticket.additional_tags = value

    value
  end

  def option_belongs_to_tagger(value)
    if account.has_cache_custom_field_option_values?
      custom_field_option_values.include?(value)
    else
      if account.has_special_chars_in_custom_field_options?
        custom_field_options.exists?(enhanced_value: value)
      else
        custom_field_options.exists?(value: value)
      end
    end
  end

  def explanation
    I18n.t('txt.admin.views.ticket_field.field_tagger.explanation_text_one')
  end

  protected

  def custom_field_option_class
    CustomFieldOption
  end

  private #############################################

  def validate_default_uniqueness
    if custom_field_options.reject(&:deleted?).select(&:default?).size > 1
      errors.add(:field_options, Api.error('txt.admin.models.ticket_fields.field_tagger.only_one_default', error: 'ValueNotAccepted'))
    end
  end

  def assign_account_to_custom_field_options
    custom_field_options.each do |custom_field_option|
      custom_field_option.account_id = account_id
    end
  end

  def bulk_insert_or_update_custom_field_options
    return yield unless account.has_bulk_insert_or_update_custom_field_options?

    # we will be resetting the `custom_field_options` association (nil out
    # `target`) later, so make sure we hold on to a reference of it
    target = custom_field_options.target
    changed_cfos = target.find_all(&:changed?)

    return yield if changed_cfos.count == 0

    # reset to nil out `target` to avoid ActiveRecord's automatic update of
    # the custom_field_options association
    custom_field_options.reset

    yield

    # Mark off current time to use as updated_at and created_at
    time = Time.now.to_s(:db)

    # make sure new options come after existing ones so that insertion of
    # new values happens after updates of duplicate values
    new_cfos, updated_cfos = changed_cfos.partition(&:new_record?)
    insert_values = (updated_cfos + new_cfos).map do |cfo|
      cfo_id = cfo.id || custom_field_option_class.generate_uid
      # We avoid the getter for 'value' here since it's overridden in a method in the class and may lie to us
      quoted_values = [cfo_id, id, cfo.name, cfo[:value], cfo.position, account_id, time, time, cfo.deleted_at, cfo.default, cfo.enhanced_value].map { |v| custom_field_option_class.connection.quote(v) }
      "(#{quoted_values.join(',')})"
    end

    custom_field_option_class.connection.execute(<<~SQL
      INSERT INTO #{custom_field_option_class.table_name}
        (`id`, `custom_field_id`, `name`, `value`, `position`, `account_id`, `created_at`, `updated_at`, `deleted_at`, `default`, `enhanced_value`)
      VALUES
        #{insert_values.join(", ")}
      ON DUPLICATE KEY UPDATE
        `name`=VALUES(`name`),
        `value`=VALUES(`value`),
        `position`=VALUES(`position`),
        `updated_at`=VALUES(`updated_at`),
        `deleted_at`=VALUES(`deleted_at`),
        `default`=VALUES(`default`),
        `enhanced_value`=VALUES(`enhanced_value`)
    SQL
                                                )
  end
end
