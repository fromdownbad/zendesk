class FieldRegexp < TicketField
  include Zendesk::CustomField::Regexp

  validate :validate_regex
  validates_length_of :regexp_for_validation, maximum: 255
end
