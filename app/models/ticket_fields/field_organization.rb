class FieldOrganization < TicketField
  def self.for_new_request
    field = new(
      title_in_portal: I18n.t("ticket_fields.organization.label"),
      description: I18n.t("ticket_fields.organization.description"),
      is_required_in_portal: true
    )

    # set an arbitrary id just to let ticket forms code handle the field
    field.id = -1

    field
  end

  def self.for_existing_request
    new(title_in_portal: I18n.t("ticket_fields.organization.label"))
  end
end
