# TicketFields are fields shown within a TicketForm
#
#  UML:  See association diagram in app/models/ticket_form.rb
#
class TicketField < ActiveRecord::Base
  include ScopedCacheKeys
  include CIA::Auditable
  include Zendesk::CustomField::Field
  include CachingObserver
  include Zendesk::FieldCompare::Base
  include Zendesk::PreventCollectionModificationMixin
  include PrecreatedAccountConversion
  include Zendesk::TicketFieldConditionConversion
  extend Zendesk::FetchAppsRequirements

  # TicketField collections
  SYSTEM_DESCRIPTORS         = ['FieldSubject', 'FieldDescription'].freeze
  SYSTEM_PROPERTIES          = ['FieldStatus', 'FieldTicketType', 'FieldPriority', 'FieldGroup', 'FieldAssignee'].freeze
  SYSTEM_MINIMAL_PROPERTIES  = ['FieldPriority', 'FieldGroup', 'FieldAssignee'].freeze
  SYSTEM_OPTIONAL            = ['FieldPriority', 'FieldTicketType'].freeze
  SYSTEM_ACCEPTS_SUB_TYPE    = ['FieldPriority', 'FieldStatus'].freeze
  SYSTEM_FIELDS = SYSTEM_PROPERTIES + SYSTEM_DESCRIPTORS
  SYSTEM_REQUIRED = SYSTEM_FIELDS - SYSTEM_OPTIONAL
  APM_SERVICE_NAME = 'ticket_fields-execution'.freeze
  TIME_TRACKING_APP_ID = Rails.env.staging? ? 16 : 35111

  audit_attribute

  attr_accessible :account, :description, :is_active, :is_collapsed_for_agents, :is_editable_in_portal, :is_required,
    :is_required_in_portal, :is_visible_in_portal, :position, :tag, :title, :title_in_portal, :sub_type_id,
    :regexp_for_validation, :sort_alphabetically, :agent_description

  attr_accessor :current_user

  # Only one system field definition per account
  validates_uniqueness_of :type, scope: [:account_id],
                                 if: proc { |model| model.account && model.is_system_field? }

  validates_presence_of :account_id
  validates_presence_of :title
  validates_presence_of :title_in_portal, if: proc { |model| (model.new_record? || model.title_in_portal_changed?) && model.is_visible_in_portal? && model.account.try(:has_title_in_portal_presence_validation?) }

  has_one :resource_collection_resource, as: :resource, inherit: :account_id
  has_one :creation_event, -> { where(visible: '1', source_type: 'TicketField', action: 'create') }, class_name: 'CIA::Event', foreign_key: 'source_id', inherit: :account_id
  has_many       :ticket_field_entries, inherit: :account_id # Dependents are destroyed asynchronously via TicketFieldEntryDeleteJob
  has_many       :ticket_form_fields, class_name: "TicketFormField", dependent: :destroy, inherit: :account_id
  has_many       :ticket_forms, through: :ticket_form_fields, inherit: :account_id
  before_save    :downcase_tag
  before_save    :nullify_sub_type_id
  before_destroy :prevent_undeleteable, unless: :force_destroy?
  before_destroy :validate_not_used
  before_destroy :validate_no_conditions_for_destroy
  after_commit :queue_ticket_field_entry_delete_job, on: :destroy

  validate :validate_system_field_title_unchanged, on: :update
  validate :validate_activation_state, on: :update
  validate :validate_condition_permissions, on: :update
  validate :validate_ticket_field_limits, on: :create
  validates :title, length: { maximum: 255 }
  validate :validate_ticket_field_title
  validates :agent_description, length: { maximum: 512 }

  after_validation :set_field_defaults, :set_ticket_field_defaults
  after_create :update_default_ticket_form, unless: :is_system_field?

  scope :required, -> { where(is_required: true) }
  scope :required_in_portal, -> { where(is_required_in_portal: true) }
  scope :collapsed_for_agents, -> { where(is_collapsed_for_agents: true) }
  scope :not_collapsed_for_agents, -> { where(is_collapsed_for_agents: false) }

  scope :properties,         -> { where(SYSTEM_PROPERTIES.collect { |f| "type = \"#{f}\"" }.join(' OR ')) }
  scope :minimal_properties, -> { where(SYSTEM_MINIMAL_PROPERTIES.collect { |f| "type = \"#{f}\"" }.join(' OR ')) }
  scope :for_visible,        -> { where(SYSTEM_DESCRIPTORS.collect { |f| "type <> \"#{f}\"" }.join(' AND ')) }
  scope :custom,             -> { where(SYSTEM_FIELDS.collect { |f| "type <> \"#{f}\"" }.join(' AND ')) }

  scope :system_fields, -> { where(type: SYSTEM_FIELDS).order(:position) }
  scope :system_required, -> { where(type: SYSTEM_REQUIRED).order(:position) }
  scope :exportable, -> { where(is_exportable: true) }

  scope :with_tag_present,      -> { where('ticket_fields.tag IS NOT NULL AND ticket_fields.tag != ""') }
  scope :checkbox_fields,       -> { where(type: "FieldCheckbox") }
  scope :without_skipped_field, ->(field) { where('ticket_fields.id != ?', field.id) if field.id }

  def self.with_organization(user)
    fields = where(nil)

    if user.has_multiple_organizations?
      # for rails 5 compat... but seems bad? this is literally what
      # rails 4 is doing under the covers via method_missing
      fields = fields.to_a
      fields.unshift(FieldOrganization.for_new_request)
    end

    fields
  end

  def normalize_value_and_sync_tags(_ticket, value)
    # normalize value, no tag synchronization necessary
    value.to_s.strip
  end

  def downcase_tag
    self.tag = tag.try(:downcase)
  end

  def can_be
    to_fom.can_be
  end

  def presentation_type
    to_fom.presentation_type
  end

  def value(ticket)
    to_fom.value(ticket)
  end

  def is_blank?(ticket) # rubocop:disable Naming/PredicateName
    to_fom.is_blank?(ticket)
  end

  def humanize_value(v)
    to_fom.humanize_value(v)
  end

  def to_sym
    self.class.to_s.to_sym
  end

  def to_fom
    @field ||= Zendesk::Fom::Field.for(self)
  end

  def as_json(options = {})
    defaults = {only: api_serializable_attribute_names}
    super defaults.merge(options)
  end

  def to_xml(options = {})
    defaults = {only: api_serializable_attribute_names}
    super defaults.merge(options)
  end

  def api_serializable_attribute_names
    names = attribute_names
    names.delete("deleted_at")
    names
  end

  def is_system_field? # rubocop:disable Naming/PredicateName
    SYSTEM_FIELDS.include?(self.class.to_s)
  end

  def is_removable? # rubocop:disable Naming/PredicateName
    !SYSTEM_REQUIRED.include?(self.class.to_s)
  end

  def accepts_sub_type?
    SYSTEM_ACCEPTS_SUB_TYPE.include?(self.class.to_s)
  end

  def multilingual_field(args = {})
    current_value = send(args[:field_type])

    if args[:key] && args[:user_locale] && current_value == I18n.t((args[:key]).to_s, locale: args[:account_locale])
      I18n.t((args[:key]).to_s, locale: args[:user_locale])
    else
      current_value
    end
  end

  def multilingual_key(field_type)
    case field_type
    when :title, :title_in_portal
      self.class::I18N_TITLE_KEY
    when :description
      self.class::I18N_DESCRIPTION_KEY
    else
      raise "Unsupported field type: #{field_type}"
    end
  end

  def force_destroy!
    @force_destroy = true
    destroy
  end

  # Apps can create ticket fields, however in audit events the system user is used as the creator
  # so we must request the name of the corresponding app via app market
  def creator_app_name
    return unless is_apps_requirement?

    apps_response = Rails.cache.fetch("#{account.scoped_cache_key(:ticket_fields)}/apps_requirements") do
      TicketField.apps_requirements(account)
    end
    apps_response[id]
  end

  # Apps can create ticket fields, however in audit events the system user is used as the creator
  # so we must request the app_id of the corresponding app via app market
  def creator_app_id
    return unless is_apps_requirement?

    apps_response = Rails.cache.fetch("#{account.scoped_cache_key(:ticket_fields)}/apps_id_requirements") do
      TicketField.app_id_requirements(account)
    end

    apps_response[id]
  end

  def created_by_time_tracking_app?
    creator_app_id.to_i == TIME_TRACKING_APP_ID
  end

  def app_field?
    !!creator_app_name
  end

  def expire_scoped_caches
    account.expire_scoped_cache_key(:settings)
    account.expire_scoped_cache_key(:ticket_fields)
    account.expire_scoped_cache_key(:ticket_forms)
  end

  protected

  def force_destroy?
    @force_destroy
  end

  def prevent_undeleteable
    return if can_be[:deleted]
    errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deleted'))
    false
  end

  def queue_ticket_field_entry_delete_job
    TicketFieldEntryDeleteJob.enqueue(account_id, id)
  end

  def set_ticket_field_defaults
    self.is_required ||= false
    self.is_required_in_portal ||= false

    if !is_visible_in_portal?
      self.is_required_in_portal = false
    elsif !is_editable_in_portal?
      self.is_required_in_portal = false
    end
    true
  end

  def validate_activation_state
    return true unless is_active_changed?

    unless can_be[:deactivated]
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deactivated'))
    end

    if account.try(:has_native_conditional_fields_enabled?) && !is_active? && TicketFieldCondition.by_ticket_field(self).any?
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deactivated_by_condition', field_name: title))
    end

    if !can_be[:activated] && is_active?
      if is_a?(FieldPartialCreditCard)
        errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.partial_credit_card_field_cannot_be_activated'))
      else
        errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_activated'))
      end
    end
  end

  def validate_system_field_title_unchanged
    if is_system_field? && title_changed?
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.system_ticket_field.field_title_cannot_be_changed'))
    end
  end

  def validate_condition_permissions
    if account.try(:has_native_conditional_fields_enabled?) && !is_editable_in_portal? && TicketFieldCondition.for_end_users.by_ticket_field(self).any?
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.field_with_end_user_conditions_must_be_end_user_editable', field_name: title))
    end
  end

  public_class_method def self.get_all_tags(skip_field)
    tags_from_other_field_taggers = (skip_field.account.fetch_active_taggers - [skip_field]).flat_map(&:custom_field_options).map(&:value)
    tags_from_checkboxes = skip_field.account.ticket_fields.active.select { |f| f.id != skip_field.try(:id) && f.is_a?(FieldCheckbox) && f.tag.present? }.map(&:tag)
    Set.new(tags_from_other_field_taggers + tags_from_checkboxes)
  end

  def update_default_ticket_form
    return unless account.ticket_forms.active.count(:all) == 1
    default_form = account.ticket_forms.default.first
    field_ids = default_form.ticket_fields.map(&:id) << id
    opts = {id: default_form.id, ticket_form: {ticket_field_ids: field_ids}}
    Zendesk::TicketForms::Initializer.new(account, opts).updated_ticket_form.save
  end

  def update_taggings?
    false
  end

  def nullify_sub_type_id
    self.sub_type_id = nil unless accepts_sub_type?

    true
  end

  # if there is only one ticket form the field is added automatically to the default form.
  # Hence we must ensure we have less than 400 fields
  def validate_ticket_field_limits
    return true unless account.has_ticket_field_limits?
    return true if account.ticket_forms.active.count(:all) > 1
    number_of_options = account.ticket_fields.count + 1
    if number_of_options > account.settings.ticket_fields_limit
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.limit_number_of_fields', max_number_of_tags: account.settings.ticket_fields_limit))
    end
    true
  end

  def validate_not_used
    CustomField::Field.validate_field_not_used(account, "Ticket", self)
  end

  def validate_no_conditions_for_destroy
    if account.try(:has_native_conditional_fields_enabled?) && TicketFieldCondition.by_ticket_field(self).any?
      errors.add(:base, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_deleted_by_condition', field_name: title))
      false # RAILS5UPGRADE: change to `throw(:abort)`
    end
  end

  def validate_ticket_field_title
    # check for ideographic space (unicode 0x3000)
    if title&.match?(/\u3000/)
      errors.add(:base, I18n.t('txt.admin.models.ticket_fields.field_tagger.cannot_contain_special_characters'))
    end
  end
end
