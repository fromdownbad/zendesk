class FieldTicketType < TicketField
  I18N_TITLE_KEY = "txt.default.fields.type.title".freeze
  I18N_DESCRIPTION_KEY = "txt.default.fields.type.description".freeze

  def multilingual_field(args = {})
    args[:key] = multilingual_key(args[:field_type])
    super
  end
end
