class FieldDescription < TicketField
  I18N_TITLE_KEY = "txt.default.fields.description.title".freeze
  I18N_DESCRIPTION_KEY = "txt.default.fields.description.description".freeze

  def multilingual_field(args = {})
    args[:key] = multilingual_key(args[:field_type])
    super
  end
end
