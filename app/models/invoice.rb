class Invoice < ActiveRecord::Base
  not_sharded
  disable_global_uid

  belongs_to :payment

  attr_accessible             :payment
  validates_presence_of       :payment

  delegate                    :net, :discount, :max_agents, :plan_type, :account, :subscription, to: :payment
end
