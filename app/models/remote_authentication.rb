require 'digest/md5'

class RemoteAuthentication < ActiveRecord::Base
  include CIA::Auditable
  include PrecreatedAccountConversion

  MINIMUM_TOKEN_LENGTH = 40
  MAXIMUM_TOKEN_LENGTH = 255
  MAXIMUM_URL_LENGTH = 2048

  NATIVE   = 1
  SAML     = 2
  JWT      = 3

  def self.allowed_scopes
    %w[saml jwt]
  end

  has_kasket_on :account_id
  belongs_to    :account

  attr_accessible :account, :auth_mode, :is_active, :fingerprint, :remote_login_url, :remote_logout_url,
    :update_external_ids, :ip_ranges, :next_token, :priority

  audit_attribute :is_active, :remote_login_url, :remote_logout_url,
    :update_external_ids, :ip_ranges, :priority, :token

  before_validation       :set_token
  validates_presence_of   :account_id
  validates_uniqueness_of :auth_mode, scope: :account_id

  validate                :validate_feature_permissions
  # Move towards validating on every update.
  validate                :validate_token
  validate                :validate_login_url
  validate                :validate_logout_url
  validate                :validate_ip_ranges

  before_save             :ensure_update_external_ids
  after_save              :notify_on_activation

  attr_accessor           :next_token

  default_scope { order("priority ASC") }

  scope :active, -> { where(is_active: true) }
  scope :jwt, -> { where(auth_mode: JWT) }
  scope :saml, -> { where(auth_mode: SAML) }

  def self.key(args, include_tags, version, options = {})
    if version == 2
      # "name|email|external_id|organization|tags|remote_photo_url|token|timestamp"
      parts = []
      parts << args[:name]
      parts << args[:email]
      parts << args[:external_id]
      parts << args[:organization]
      parts << args[:tags]
      parts << args[:remote_photo_url]
      parts << args[:token]
      parts << args[:timestamp]

      if options.fetch(:escaped_delimiters, true)
        parts.map! do |part|
          part.to_s.gsub(/\|/, "%7C")
        end
      end

      input = parts.join("|")
    else
      input = "#{args[:name]}#{args[:email]}#{args[:external_id]}#{args[:organization]}"
      input << (args[:tags]).to_s if include_tags
      input << "#{args[:remote_photo_url]}#{args[:token]}#{args[:timestamp]}"
    end

    Digest::MD5.hexdigest(input)
  end

  def allow_external_id_updates
    update_external_ids
  end

  def handles_ip?(ip_address)
    Zendesk::Net::IPTools.whitelist_handles_ip?(ip_address, ip_ranges)
  end

  def saml_authenticates?(ip_address)
    saml_mode_active? && handles_ip?(ip_address)
  end

  def jwt_authenticates?(ip_address)
    jwt_mode_active? && handles_ip?(ip_address)
  end

  def saml_mode_active?
    is_active? && auth_mode == RemoteAuthentication::SAML
  end

  def jwt_mode_active?
    is_active? && auth_mode == RemoteAuthentication::JWT
  end

  def fingerprint
    auth_mode == RemoteAuthentication::SAML ? token : nil
  end

  attr_writer :fingerprint

  def has_native_token? # rubocop:disable Naming/PredicateName
    token.present? && auth_mode == JWT
  end

  def name
    case auth_mode
    when JWT
      I18n.t('txt.admin.views.settings.security._authentication.jwt_label')
    when SAML
      I18n.t('txt.admin.views.settings.security._authentication.sam_label')
    end
  end

  def cia_changes
    if account&.has_redact_jwt_token_in_audit_log?
      changes = super
      return changes unless changes.key?(:token)

      changes[:token].map!(&method(:redact_token))
      changes
    else
      super
    end
  end

  private

  def ensure_update_external_ids
    if auth_mode == SAML
      self.update_external_ids = false
    end

    true
  end

  def notify_on_activation
    if is_active_changed? && is_active?
      AccountsMailer.deliver_remote_authentication_activated(account, self)
    end
  end

  def set_token
    if auth_mode == SAML
      self.token = @fingerprint if @fingerprint
    elsif auth_mode == JWT
      self.token = next_token if next_token.present?
    end
  end

  def validate_token
    return unless token_changed?

    return if (MINIMUM_TOKEN_LENGTH..MAXIMUM_TOKEN_LENGTH).cover?(token.to_s.size)

    if auth_mode == SAML
      errors.add(:fingerprint, :invalid)
    elsif auth_mode == JWT
      errors.add(:secret, :invalid)
    end
  end

  # needs to be defined or errors.add :secret blows up on rails 3
  def secret
  end

  def validate_ip_ranges
    ip_ranges.to_s.split(' ').each do |range|
      errors.add(:base, I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: range)) unless Zendesk::Net::IPTools.is_valid_ip_range?(range)
    end
  end

  def validate_feature_permissions
    return unless is_active?

    if auth_mode == RemoteAuthentication::SAML && !account.has_saml?
      errors.add(:base, I18n.t("txt.admin.models.remote_authentication.profile_does_not_support_saml"))
    elsif auth_mode == RemoteAuthentication::JWT && !account.has_jwt?
      errors.add(:base, I18n.t("txt.admin.models.remote_authentication.profile_does_not_support_jwt"))
    end
  end

  def validate_login_url
    return unless remote_login_url_changed?
    remote_login_url.strip!

    valid_url?(:remote_login_url)
  end

  def validate_logout_url
    return unless remote_logout_url_changed? && remote_logout_url.present?
    remote_logout_url.strip!

    valid_url?(:remote_logout_url)
  end

  def valid_url?(attribute)
    url = send(attribute)
    uri = URI.parse(url)

    previous_uri = previous_uri_for(attribute)

    errors.add(:base, I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_too_long_v4",
      char_limit: MAXIMUM_URL_LENGTH)) if url.length > MAXIMUM_URL_LENGTH

    return true if valid_local_host?(uri) || grandfathered_http?(uri, previous_uri)

    if http?(uri)
      errors.add(:base, I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_http_v3"))
    elsif !https?(uri) && !http?(uri)
      errors.add(:base, I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_not_url_v3"))
    elsif Zendesk::Net::AddressUtil.zendesk_url?(url)
      errors.add(:base, I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_not_zendesk_v3"))
    end
  rescue URI::InvalidURIError
    errors.add(:base, I18n.t("txt.admin.models.remote_authentication.#{attribute}_is_not_url_v3"))
  end

  def http?(uri)
    uri.try(:scheme) == "http"
  end

  def https?(uri)
    uri.try(:scheme) == "https"
  end

  def grandfathered_http?(uri, previous_uri)
    http?(uri) && http?(previous_uri)
  end

  def valid_local_host?(uri)
    (http?(uri) || https?(uri)) && %w[localhost 127.0.0.1].include?(uri.hostname)
  end

  def previous_uri_for(attribute)
    url = send("#{attribute}_was")
    URI.parse(url) if url
  rescue URI::InvalidURIError
    nil
  end

  # The UI only displays the first 6 characters of the JWT shared secret thus we need to redact the token from the log
  # It's okay for SAML fingerprint to be displayed in full
  # For more context: see https://support.zendesk.com/agent/tickets/5290042
  def redact_token(token)
    return token if token.blank? || auth_mode != JWT

    token[(0..5)] + '*' * (token.size - 6)
  end
end
