require 'zendesk/models/photo'

class Photo < ActiveRecord::Base
  include Zendesk::Serialization::PhotoSerialization
  include AttachmentMixin
  include UserEntityObserver

  belongs_to :user, inverse_of: :photo, touch: true, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  attr_accessible :user, :original_url

  setup_attachment_fu(
    content_type: :image,
    size: 1..20.megabytes,
    processor: :mini_magick,
    resize_to: '80>',
    path_prefix: 'public/system/photos',
    thumbnails: {
      thumb: '32x32'
    }
  )

  validates_as_attachment

  validates_presence_of  :account_id
  validates_inclusion_of :extension, in: Users::Photo::VALID_EXTENSIONS
  validate               :valid_image_file

  attr_reader :url

  before_validation :inherit_parent_attributes

  def extension
    filename.split(".").last.downcase if filename.present?
  end

  def valid_image_file
    return unless account

    unless valid_size?
      errors.add(:base, I18n.t('txt.errors.attachments.size_invalid', limit: account.subscription.file_upload_cap))
      return
    end

    unless valid_dimensions?
      errors.add(:base, I18n.t('txt.errors.attachments.pixel_range_invalid_2', min: min_resize_pixels, max: max_resize_pixels))
      return
    end

    return if temp_path.nil?

    begin
      MiniMagick::Image.open(temp_path)
    rescue MiniMagick::Error, MiniMagick::Invalid => e
      Rails.logger.warn("[SECURITY] Photo deemed invalid by ImageMagick: #{inspect} -- #{e.message}")
      errors.add(:base, "Invalid profile image")
    end
  end

  def self.cleanup
    where(user_id: nil, parent_id: nil).find_each(&:destroy)
  end

  def display_filename
    filename
  end

  private

  def destroy_thumbnails?
    true
  end

  def inherit_parent_attributes
    return unless parent
    self.account_id = parent.account_id
    self.user_id    = parent.user_id
  end
end
