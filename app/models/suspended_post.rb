# The scope of this class is to avoid exposing .suspended directly.
# This way valid syntax like
#   Post.suspended.for_account(account)
# Can be written like
#   SuspendedPost.suspended.for_account(account)
#
# This avoid terrible and harmless looking statement like
#   @entry.post.suspended
#
# that will actually result in multi-tenancy violation.
class SuspendedPost
  class << self
    def authored_by(ids)
      suspended.where(user_id: ids)
    end

    def for_account(account)
      suspended.where(account_id: account.id)
    end

    private

    def suspended
      Post.unscoped.
        where('posts.deleted_at IS NOT NULL').
        where(flag_type_id: PostFlagType::SUSPENDED.id).
        order('posts.updated_at DESC')
    end
  end
end
