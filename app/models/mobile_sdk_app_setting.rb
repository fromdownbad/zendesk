# Without that we get
# NameError: uninitialized constant MobileSdkAppSetting
# in the dev console.
# Also it allows the fixtures to load correctly.
#
# The class is defined in mobile_sdk/app_properties by property_set.
unless defined?(MobileSdkAppSetting)
  MobileSdkApp
end
MobileSdkAppSetting
