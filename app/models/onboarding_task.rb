class OnboardingTask < ActiveRecord::Base
  self.table_name = :zero_state_dismissals
  belongs_to :user
  belongs_to :account

  attr_accessible :account, :user, :level, :zero_state, :status

  module LEVEL
    ACCOUNT   = 0
    USER      = 1
  end

  module CompletionStatus
    COMPLETED = 0
    # not really using this INCOMPLETED flag. We're not going to persist a task into the table and call it incompleted
    # Just here as a placeholder in case we ever need it
    INCOMPLETED = 1
  end

  validates :account, :user, :zero_state, :status, :level, presence: true
  class << self
    def from_params(user, task, type)
      onboarding_tasks_from_config = user.onboarding(type)
      predefined_task = onboarding_tasks_from_config.find do |onboarding_task_from_config|
        onboarding_task_from_config['name'] == task['name']
      end
      if predefined_task.nil?
        return nil
      end
      task["zero_state"] = task.delete "name"
      create!(task.
              slice(*column_names).
              merge(account: user.account, user: user,
                    level: predefined_task['level']))
    end
  end
end
