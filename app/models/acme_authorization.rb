require 'zendesk/acme_job/error'

class AcmeAuthorization < ActiveRecord::Base
  belongs_to :account
  belongs_to :acme_registration

  has_soft_deletion default_scope: true

  before_validation :set_state, on: :create

  # This is allow only one non-deleted identifer per account
  # refactor to once we have rails 4: conditions: -> { where("deleted_at is null") }
  validates_uniqueness_of :identifier, scope: %i[account_id deleted_at], unless: proc { |auth| auth.deleted_at? }

  def authorization_requested?
    challenge_token.present?
  end

  # not needed in v2
  def attempt_to_authorize!(verify_now: true)
    unless authorization_requested?
      request_token!
    end

    request_verification! if verify_now
  end

  def is_authorized_v2? # rubocop:disable Naming/PredicateName
    return false unless authorization_requested?

    retries = 0
    while current_authorization_status == 'pending'
      sleep 2 unless Rails.env.test?
      acme_challenge.reload
      retries += 1
      break if retries == 5
    end

    status = current_authorization_status

    if status == 'valid'
      self.state = 'valid'
      save!
      true
    elsif status == "pending" && authorization_checked?
      request_verification_v2!
      false
    elsif %w[deleted invalid].include?(status)
      false
    elsif !authorization_checked?
      request_verification_v2!
      false
    else
      raise Zendesk::AcmeJob::AuthorizationFailedError, status, "failed to authorize #{identifier} status: #{status}"
    end
  end

  def is_authorized? # rubocop:disable Naming/PredicateName
    return false unless authorization_requested?

    status = current_authorization_status

    if status == 'valid'
      self.state = 'valid'
      save!
      true
    elsif status == "pending" && authorization_checked?
      request_verification!
      false
    elsif %w[deleted invalid].include?(status)
      reset_challenge!
      attempt_to_authorize!
      false
    elsif !authorization_checked?
      request_verification!
      false
    else
      raise Zendesk::AcmeJob::AuthorizationFailedError, status, "failed to authorize #{identifier} status: #{status}"
    end
  end

  def authorization_checked?
    AcmeAuthorization.find(id).accesses > 0
  end

  # not needed in v2
  def request_token!
    auth = client.authorize(domain: identifier)
    http = auth.http01

    self.challenge_type = 'http-01'
    self.challenge_uri = http.uri
    self.challenge_token = http.token
    self.challenge_content = http.file_content
    self.failures = 0
    save!
  rescue
    raise Zendesk::AcmeJob::AuthorizationError, $!
  end

  # not needed in v2
  def reset_challenge!
    update_attributes!(
      challenge_type: nil,
      challenge_uri: nil,
      challenge_token: nil,
      challenge_content: nil,
      accesses: 0,
      failures: 0,
      state: 'pending'
    )
    @acme_challenge = nil
  end

  def request_verification!
    retried = false
    begin
      acme_challenge.request_verification
      self.state = 'verifying'
      save!
    rescue Acme::Client::Error::Malformed => e
      if !retried
        reset_challenge!
        # Once we can schedule a job in the future we should abort this job and
        # schedule another job in a few minutes but for now let's just
        # sleep a few seconds and cross our fingers
        request_token!
        sleep 5 unless Rails.env.test?
        retried = true
        retry
      else
        raise Zendesk::AcmeJob::AuthorizationError, e
      end
    end
  rescue
    raise Zendesk::AcmeJob::AuthorizationError, $!
  end

  def request_verification_v2!
    begin
      acme_challenge.request_validation
      self.state = 'verifying'
      save!
    rescue Acme::Client::Error::Malformed => e
      raise Zendesk::AcmeJob::AuthorizationError, e
    end
  rescue
    raise Zendesk::AcmeJob::AuthorizationError, $!
  end

  def current_authorization_status
    if account.has_lets_encrypt_acme_v2?
      acme_challenge.status
    else
      acme_challenge.verify_status
    end
  rescue Acme::Client::Error::NotFound
    'deleted'
  rescue
    raise Zendesk::AcmeJob::AuthorizationError, $!
  end

  def acme_challenge
    if account.has_lets_encrypt_acme_v2?
      client.challenge(url: challenge_uri)
    else
      @acme_challenge ||= client.challenge_from_hash(
        'token' => challenge_token,
        'uri' => challenge_uri,
        'type' => challenge_type
      )
    end
  end

  def set_state
    self.state = 'pending' if state.nil?
  end

  def client
    if account.has_lets_encrypt_acme_v2? # rubocop:disable Style/ConditionalAssignment
      @client ||= acme_registration.client_acme_v2
    else
      @client ||= acme_registration.client
    end
  end
end
