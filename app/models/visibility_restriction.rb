class VisibilityRestriction < ActiveHash::Base
  fields :id, :label

  include ActiveHash::Enum
  enum_accessor :label

  include ActiveHash::Associations
  has_many :forums, foreign_key: :visibility_restriction_id

  def everybody?
    id == 1
  end

  def logged_in_only?
    id == 2
  end

  def agents_only?
    id == 3
  end
end

VisibilityRestriction.data = [
  {id: 1, label: "Everybody"},
  {id: 2, label: "Logged-in users"},
  {id: 3, label: "Agents only"}
]
