module Redaction
  class RedactionError < StandardError; end

  class CommentRedaction
    REDACTED_INLINE_IMAGE_NODE = Nokogiri::HTML5.fragment("<div class='redacted-image-content'>#{I18n.t('ticket.comment.omniredaction.image_redacted')}</div>").children.first
    ZENDESK_ATTACHMENT_REGEX = /\/attachments\/token\/\S+\/\?name=\S+/i.freeze

    attr_accessor :comment, :user, :ticket, :has_redact, :audit, :external_attachment_urls, :text_redactions

    def initialize
      @text_redactions = []
    end

    def execute!(text)
      doc = Nokogiri::HTML5.fragment(text, Encoding::UTF_8.to_s)

      return { text: text, error: { message: doc.errors.join(',') } } if doc.errors.any?

      replace_inline_images(doc)

      return { text: text, error: { message: 'Comment does not match the redaction text' } } if text && modified_comment?(text)

      elements = doc.search('redact', 'img[redact]', 'a[redact]')

      if elements.count == 0 && external_attachment_urls.empty?
        return { text: text, error: { message: 'No matching text to redact' } }
      end

      self.has_redact = false

      begin
        elements.each do |element|
          transform(element)
          unwrap_redact_tag(element)
        end

        if has_redact
          add_comment_redaction_event
          redact_text_from_email
        end

        external_attachment_urls&.each { |url| redact_attachment(url) }

        { text: doc.to_html, error: nil }
      rescue RedactionError => e
        { text: text, error: { message: e.message } }
      rescue StandardError => e
        { text: text, error: { message: "Unhandled Exception: Redaction failed - #{e.inspect}" } }
      end
    end

    private

    def transform(element)
      if element.text? && !element.ancestors('redact').empty?
        text_redactions.push(element.content)
        element.replace(Zendesk::HtmlRedaction::DEFAULT_REDACTION_CHARACTER * element.content.length)
        self.has_redact = true
      elsif element.element? && element.name == 'img' && element.has_attribute?("redact")
        redact_attachment(element.attribute('src').value)
        element.replace(REDACTED_INLINE_IMAGE_NODE)
      elsif element.name == 'a' && element.has_attribute?("redact")
        element.replace(element.children)
      elsif element.element?
        element.children.each do |ce|
          transform(ce)
        end
      end
    end

    def redact_attachment(url)
      attachment = comment.attachments.find_by_url(url)

      return unless ZENDESK_ATTACHMENT_REGEX.match?(url) # TODO: Add attachment event for images pointing outside of zendesk

      raise RedactionError, "Cannot create redaction event because ticket, user, attachment or audit is not valid" unless [ticket, user, attachment, audit].all?

      redacted_attachment = attachment.redact!
      add_attachment_redaction_event(attachment, redacted_attachment)
    end

    def add_attachment_redaction_event(attachment, redacted_attachment, skip_domain_event = false)
      audit.events << AttachmentRedactionEvent.new(attachment_id: redacted_attachment.id)

      unless skip_domain_event
        ticket.add_domain_event(
          AttachmentRedactedFromCommentProtobufEncoder.new(attachment, comment).to_safe_object
        )
      end
    end

    def add_comment_redaction_event
      audit.events << CommentRedactionEvent.new(comment_id: comment.id)
    end

    def redact_text_from_email
      comment.audit.delete_eml_file!
      text_redactions.each do |text|
        comment.audit.redact_json_files!(text)
      end
    end

    def unwrap_redact_tag(element)
      element.replace(element.children) if element.name == 'redact'
    end

    def modified_comment?(text)
      Nokogiri::HTML5(text).text.strip != Nokogiri::HTML5(comment.value).text.strip
    end

    def replace_inline_images(doc)
      doc.search(".redacted-image-content").map do |element|
        element.replace(REDACTED_INLINE_IMAGE_NODE)
      end
    end
  end
end
