require 'open-uri'

class Currency < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible :code, :rate, :rate_on

  validates :code, uniqueness: true
  validates :rate, presence: true

  EXCHANGE_RATES_URL = 'http://www.nationalbanken.dk/dndk/valuta.nsf/valuta.xml'.freeze

  def self.USD # rubocop:disable Naming/MethodName
    find_by_code('USD')
  end

  def self.DKK # rubocop:disable Naming/MethodName
    find_by_code('DKK')
  end

  # Exchange an amount from one currency to another. Eg. convert $10 to DKK:
  #  u = Currency.find_by_code('USD')
  #  d = Currency.find_by_code('DKK')
  #  u.exchange_to(10.00, d)
  #  "51.43"
  def exchange_to(amount, currency)
    ('%.2f' % (amount * rate / currency.rate)).to_f
  end
end
