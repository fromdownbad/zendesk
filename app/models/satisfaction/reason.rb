class Satisfaction::Reason < ActiveRecord::Base
  self.table_name = :satisfaction_reasons

  attr_accessible :account, :value, :reason_code, :translation_key, :system

  SYSTEM_REASONS = [
    { reason_code: 0, translation_key: 'type.satisfaction_reason_code.none' },
    # Should add the future reasons with values between 5 and 100 to have the PDW report correct stats.
    { reason_code: 5, translation_key: 'type.satisfaction_reason_code.issue_took_too_long' },
    { reason_code: 6, translation_key: 'type.satisfaction_reason_code.issue_not_resolved' },
    { reason_code: 7, translation_key: 'type.satisfaction_reason_code.agent_knowledge_unsatisfactory' },
    { reason_code: 8, translation_key: 'type.satisfaction_reason_code.agent_attitude_unsatisfactory' },
    { reason_code: 100, translation_key: 'type.satisfaction_reason_code.some_other_reason' },
  ].freeze
  DEFAULT_REASONS = [5, 6, 7, 8].freeze
  NONE = 0
  OTHER = 100
  CUSTOM_REASON_CODE_START = 1000

  belongs_to :account
  has_many :brand_restrictions,
    class_name: 'Satisfaction::ReasonBrandRestriction',
    foreign_key: :satisfaction_reason_id,
    dependent: :destroy,
    inherit: :account_id
  has_many :brands, through: :brand_restrictions, inherit: :account_id

  has_soft_deletion default_scope: true

  validates_presence_of :account
  validates_presence_of :value, unless: :system
  validates_uniqueness_of :reason_code, scope: :account_id

  scope :selectable, -> { where("reason_code != #{NONE} AND reason_code != #{OTHER}") }

  before_create :draw_reason_code, unless: :system
  before_soft_delete :validate_not_system

  def self.create_system_reasons_for(account)
    SYSTEM_REASONS.each do |params|
      reason = account.satisfaction_reasons.create(params.merge(system: true))
      brand = account.default_brand

      next if !DEFAULT_REASONS.include?(reason.reason_code) || !brand.present? || brand.account != account
      reason.brand_restrictions.create(account: account, brand: account.default_brand)
    end
  end

  def self.shuffled_options_for(brand, ticket)
    reasons = if brand.account.has_csat_reason_clone?
      brand.satisfaction_reasons.map(&:clone) # RAILS4 switched the roles of dup and clone as they were in RAILS3
    else
      brand.satisfaction_reasons.dup
    end

    ticket_reason = ticket.satisfaction_reason

    reasons.push ticket_reason unless ticket_reason.reason_code == NONE ||
      ticket_reason.reason_code == OTHER ||
      reasons.include?(ticket_reason)

    reasons.shuffle.map { |reason| [reason.translated_value, reason.reason_code] }.
      unshift(['', NONE]).
      push([I18n.t('type.satisfaction_reason_code.some_other_reason'), OTHER])
  end

  def translated_value(locale = nil)
    @translated_value ||= system ? I18n.t(translation_key, locale: locale) : render_dc
  end

  def activate_for!(brand)
    return if brands.include? brand

    restriction = brand_restrictions.new
    restriction.brand_id = brand.id
    restriction.account = account
    restriction.save!
  end

  def deactivate_for!(brand)
    return unless brands.include? brand

    restriction = brand_restrictions.find_by_brand_id(brand.id)
    restriction.destroy
  end

  def deactivated?(brand)
    brands.exclude?(brand)
  end

  protected

  def draw_reason_code
    self.reason_code = next_reason_code
  end

  def next_reason_code
    reason_max = Satisfaction::Reason.with_deleted do
      account.satisfaction_reasons.maximum(:reason_code).try(:next)
    end

    [reason_max, CUSTOM_REASON_CODE_START].compact.max
  end

  def render_dc
    Zendesk::Liquid::DcContext.render(value, account, nil)
  end

  def validate_not_system
    if system
      errors.add(:base, I18n.t('txt.admin.models.reason.cannot_delete_system_reason'))
      return false
    end

    true
  end
end
