class Satisfaction::PredictionSurvey < ActiveRecord::Base
  self.table_name = :satisfaction_prediction_surveys
  attr_accessible :reason, :prediction_value
  belongs_to :ticket
  belongs_to :account
  belongs_to :user, foreign_key: :agent_id

  validates_presence_of :account_id, :agent_id, :ticket_id, :reason, :prediction_value
  validates_length_of :reason, maximum: 2048
end
