class Satisfaction::ReasonBrandRestriction < ActiveRecord::Base
  self.table_name = :satisfaction_reason_brand_restrictions

  belongs_to :account
  belongs_to :brand, inherit: :account_id
  belongs_to :satisfaction_reason, class_name: 'Satisfaction::Reason', inherit: :account_id

  inherits_from :brand, attr: :account_id

  attr_accessible :account, :brand, :satisfaction_reason

  validates :account, :brand, :satisfaction_reason, presence: true
end
