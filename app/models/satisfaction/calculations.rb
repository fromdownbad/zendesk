module Satisfaction::Calculations
  def current_score(context, options = {})
    total_count = total_rated_ticket_count(context, options)
    return unless total_count >= minimum_ratings_for_calculation
    ((positive_rated_ticket_count(context, options).to_f / total_count.to_f).round(2) * 100).to_i # rubocop:disable Style/FloatDivision
  end

  def total_rated_ticket_count(context, options = {})
    options = default_filter_options.merge(options)

    ticket_count(
      context, options,
      [
        SatisfactionType.GOOD,
        SatisfactionType.GOODWITHCOMMENT,
        SatisfactionType.BAD,
        SatisfactionType.BADWITHCOMMENT
      ]
    )
  end

  def positive_rated_ticket_count(context, options = {})
    options = default_filter_options.merge(options)
    ticket_count(
      context, options,
      [
        SatisfactionType.GOOD,
        SatisfactionType.GOODWITHCOMMENT
      ]
    )
  end

  def negative_rated_ticket_count(context, options = {})
    total_rated_ticket_count(context, options) - positive_rated_ticket_count(context, options)
  end

  def surveys_sent(context, options = {})
    options = default_filter_options.merge(options)

    ticket_count(
      context, options,
      [
        SatisfactionType.OFFERED,
        SatisfactionType.BAD,
        SatisfactionType.BADWITHCOMMENT,
        SatisfactionType.GOOD,
        SatisfactionType.GOODWITHCOMMENT,
      ]
    )
  end

  def response_rate(context, options = {})
    return 0 if surveys_sent(context, options) == 0

    (total_rated_ticket_count(context, options).fdiv(surveys_sent(context, options)) * 100).round
  end

  def minimum_ratings_for_calculation
    1
  end

  def default_filter_options
    {
      'start_time' => 60.days.ago,
      'end_time' => Time.now
    }
  end

  private

  def account_for(context)
    if context.is_a?(Account)
      context
    else
      context.account
    end
  end

  def ticket_count(context, options, satisfaction_types)
    return legacy_ticket_count(context, options, satisfaction_types) unless account_for(context).has_csat_metrics_adjustment?

    scope = if context.is_a?(User)
      context.account.satisfaction_ratings.assigned_to(context.id)
    else
      context.satisfaction_ratings
    end

    statsd_client.time('ticket_count_timing') do
      scope.
        non_deleted_with_archived.
        newer_than(options['start_time']).
        older_than(options['end_time']).
        latest.
        where(score: satisfaction_types).
        count(:all)
    end
  end

  def legacy_ticket_count(context, options, satisfaction_types)
    scope = if context.is_a?(User)
      context.account.tickets.where(assignee_id: context.id)
    else
      context.tickets
    end

    # Utilize index on updated_at for performance reasons only
    statsd_client.time('legacy_ticket_count_timing') do
      scope.
        from('`tickets` USE INDEX(`index_tickets_on_account_id_and_status_id_and_updated_at`, `index_tickets_on_account_id_and_satisfaction_score`)').
        where(satisfaction_score: satisfaction_types).
        where('updated_at > ?', options['start_time']).
        where('(solved_at IS NULL AND updated_at BETWEEN ? AND ?) OR solved_at BETWEEN ? AND ?', options['start_time'], options['end_time'], options['start_time'], options['end_time']).
        count(:all)
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[satisfaction_ratings calculations])
  end
end
