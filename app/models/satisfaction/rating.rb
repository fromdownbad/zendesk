class Satisfaction::Rating < ActiveRecord::Base
  self.table_name = 'satisfaction_ratings'
  extend  Satisfaction::Calculations
  include Zendesk::Serialization::SatisfactionRatingSerialization
  extend ZendeskArchive::AssociationLookaside

  SCORE_MAP = {
    "unoffered" => [SatisfactionType.UNOFFERED],
    "offered" => [SatisfactionType.OFFERED],
    "received" => [SatisfactionType.GOOD, SatisfactionType.BAD, SatisfactionType.GOODWITHCOMMENT, SatisfactionType.BADWITHCOMMENT],
    "received_with_comment" => [SatisfactionType.GOODWITHCOMMENT, SatisfactionType.BADWITHCOMMENT],
    "received_without_comment" => [SatisfactionType.GOOD, SatisfactionType.BAD],
    "good" => [SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT],
    "good_with_comment" => [SatisfactionType.GOODWITHCOMMENT],
    "good_without_comment" => [SatisfactionType.GOOD],
    "bad" => [SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT],
    "bad_with_comment" => [SatisfactionType.BADWITHCOMMENT],
    "bad_without_comment" => [SatisfactionType.BAD],
  }.freeze

  ACCOUNT_AND_CREATED_INDEX = 'index_satisfaction_ratings_on_account_id_and_created_at'.freeze

  belongs_to :account
  belongs_to :agent,   foreign_key: :agent_user_id, class_name: 'User', inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  belongs_to :enduser, foreign_key: :enduser_id, class_name: 'User', inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  belongs_to :group,   foreign_key: :agent_group_id, inherit: :account_id
  belongs_to :ticket, inherit: :account_id
  belongs_to :ticket_archive_stub, foreign_key: :ticket_id, inherit: :account_id

  association_accesses_archive :ticket

  belongs_to :event, inherit: :account_id

  belongs_to :comment,
    -> { where(value_reference: 'satisfaction_comment') },
    class_name: 'Change',
    foreign_key: :comment_event_id,
    inherit: :account_id

  attr_accessible :ticket, :score, :comment, :event, :agent, :enduser, :group, :status_id, :reason_code

  scope :good,     -> { where(score: SCORE_MAP["good"]) }
  scope :bad,      -> { where(score: SCORE_MAP["bad"]) }
  scope :received, -> { where(score: SCORE_MAP["received"]) }
  scope :newer_than, ->(datetime) { where(["satisfaction_ratings.created_at > ?", datetime]) }
  scope :older_than, ->(datetime) { where(["satisfaction_ratings.created_at <= ?", datetime]) }

  scope :assigned_to, ->(assignee_id) {
    joins("LEFT JOIN tickets ON tickets.id = satisfaction_ratings.ticket_id").
      where(["tickets.assignee_id = ?", assignee_id])
  }

  scope :non_deleted_with_archived_v1, -> {
    joins("LEFT JOIN ticket_archive_stubs ON ticket_archive_stubs.id = satisfaction_ratings.ticket_id LEFT JOIN tickets ON tickets.id = satisfaction_ratings.ticket_id").
      where(["ticket_archive_stubs.status_id != ? OR tickets.status_id != ?", StatusType.DELETED, StatusType.DELETED])
  }

  scope :non_deleted_with_archived, -> { where(status_id: [StatusType.NEW, StatusType.OPEN, StatusType.PENDING, StatusType.HOLD, StatusType.SOLVED, StatusType.CLOSED, StatusType.ARCHIVED]) }

  scope :latest, -> {
    joins("LEFT JOIN satisfaction_ratings r ON r.ticket_id = satisfaction_ratings.ticket_id AND (r.created_at > satisfaction_ratings.created_at OR (r.created_at = satisfaction_ratings.created_at AND r.id > satisfaction_ratings.id))").
      where('r.id IS NULL')
  }

  validates_presence_of :account_id, :ticket_id, :event_id, :score

  after_create :remove_intentions

  def self.distribution(account)
    account.satisfaction_ratings.
      non_deleted_with_archived.received.latest.
      select("satisfaction_ratings.score").limit(100).
      map { |rating| rating.good? ? 1 : 0 }
  end

  def scrub_data!
    return unless comment_with_fallback
    if !comment_with_fallback.ticket || comment_with_fallback.ticket.archived?
      # use write_attribute in riak store
      comment_with_fallback.send(:write_attribute, :value, Zendesk::Scrub::SCRUB_TEXT)
    else
      comment_with_fallback.update_column(:value, Zendesk::Scrub::SCRUB_TEXT)
    end
  end

  def comment_with_fallback_statsd_client
    @comment_with_fallback_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['satisfaction', 'rating', 'comment_with_fallback'])
  end

  def comment_with_fallback
    comment = comment_without_fallback

    # In the case of old satisfaction_ratings without a reference to the comment_event_id
    # we go back and grab it then set it so that this can code can eventually go away...
    if comment
      comment_with_fallback_statsd_client.increment('comment_event_id')
    else
      if ticket && ticket.archived?
        comment_with_fallback_statsd_client.increment('archived_ticket')

        # blah.  really shoulda archived the satisfaction scores along with the ticket
        if comment_event_id
          comment = ticket.events.find(comment_event_id)
        else
          ev      = ticket.events.detect { |e| e.id == event_id }
          comment = ticket.events.detect do |c|
            c.is_a?(Change) && c.parent_id == ev.parent_id && c.value_reference == 'satisfaction_comment'
          end
        end
      elsif event
        if account.has_satisfaction_rating_disable_comment_fallback?
          comment_with_fallback_statsd_client.increment('event_fallback', tags: ['fallback_disabled:true'])
        else
          parent_id = event.parent_id

          comment = Change.where(
            parent_id: parent_id,
            ticket_id: ticket_id,
            value_reference: 'satisfaction_comment',
            account_id: account_id
          ).first

          if comment
            if readonly?
              comment_with_fallback_statsd_client.increment('event_fallback', tags: ['fallback_disabled:false', 'comment_found:true', 'readonly:true'])
            else
              comment_with_fallback_statsd_client.increment('event_fallback', tags: ['fallback_disabled:false', 'comment_found:true', 'readonly:false'])
              update_attribute(:comment_event_id, comment.id)
            end
          else
            comment_with_fallback_statsd_client.increment('event_fallback', tags: ['fallback_disabled:false', 'comment_found:false'])
          end
        end
      else
        comment_with_fallback_statsd_client.increment('ticket_and_event_not_found')

        # we have a shitty SatisfactionRating with no ticket and no event,
        # so let's just delete it
        if ticket_archive_stub
          update_attribute(:status_id, ticket_archive_stub.status_id)
        else
          Rails.logger.warn("Unable to find a ticket, event, or ticket_archive_stub for Satisfaction::Rating with id: #{id}. Deleting the rating.\n#{inspect}")
          update_attribute(:status_id, StatusType.DELETED)
        end
      end
    end

    comment
  end
  alias_method :comment_without_fallback, :comment
  alias_method :comment, :comment_with_fallback

  def reason
    Satisfaction::Reason.with_deleted do
      account.satisfaction_reasons.find_by_reason_code(reason_code)
    end
  end

  def good?
    [SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT].include?(score)
  end

  private

  def remove_intentions
    account.satisfaction_rating_intentions.find_by_ticket_id(ticket.id).try(:destroy)
  end
end
