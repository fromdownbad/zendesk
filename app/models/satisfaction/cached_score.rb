module Satisfaction::CachedScore
  def satisfaction_score(category = nil)
    method = if category
      "satisfaction_ratings_#{category}"
    else
      "satisfaction_ratings"
    end

    result = Rails.cache.fetch("#{cache_key}/#{method}", expires_in: 2.hours) { send(method).current_score(self) || -1 }
    result == -1 ? nil : result
  end
end
