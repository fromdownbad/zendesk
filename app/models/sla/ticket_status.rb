require 'zendesk/sla'

module Sla
  class TicketStatus < ActiveRecord::Base
    UPSERT_SQL = <<-SQL.freeze
      INSERT INTO `sla_ticket_statuses`
        (
          `id`,
          `account_id`,
          `sla_ticket_policy_id`,
          `sla_policy_metric_id`,
          `paused`,
          `breach_at`,
          `fulfilled_at`,
          `created_at`,
          `updated_at`
        )
        VALUES %{values}
        ON DUPLICATE KEY UPDATE
          `sla_ticket_statuses`.`breach_at`=VALUES(`breach_at`),
          `sla_ticket_statuses`.`fulfilled_at`=VALUES(`fulfilled_at`),
          `sla_ticket_statuses`.`paused`=VALUES(`paused`),
          `sla_ticket_statuses`.`updated_at`=VALUES(`updated_at`)
    SQL

    private_constant :UPSERT_SQL

    self.table_name = :sla_ticket_statuses

    belongs_to :account

    belongs_to :ticket_policy,
      class_name:  'Sla::TicketPolicy',
      foreign_key: 'sla_ticket_policy_id',
      inverse_of:  :statuses

    belongs_to :policy_metric,
      class_name:  'Sla::PolicyMetric',
      foreign_key: 'sla_policy_metric_id',
      inherit: :account_id

    attr_accessible :account,
      :ticket_policy,
      :policy_metric,
      :breach_at,
      :fulfilled_at,
      :paused

    validates_presence_of :account_id,
      :ticket_policy,
      :policy_metric

    validates_uniqueness_of :sla_policy_metric_id, scope: :sla_ticket_policy_id

    def self.upsert_sql
      UPSERT_SQL
    end

    delegate :ticket, to: :ticket_policy

    def policy_metric
      PolicyMetric.with_deleted { super }
    end

    def measured?
      true
    end

    def breached?
      breach_at.present? ? breach_at < Time.now : false
    end

    def fulfilled?
      fulfilled_at.present?
    end

    def stage
      Zendesk::Sla::Stage.for(self)
    end

    def name
      policy_metric.metric.name
    end

    protected

    def <=>(other)
      case more_urgent_metric([self, other])
      when self  then -1
      when other then 1
      end
    end

    private

    def more_urgent_metric(metrics)
      metrics.
        partition { |metric| metric.stage.name == :active }.
        map       do |partition|
          partition.select(&:breach_at).min_by(&:breach_at)
        end.first || metrics.first
    end
  end
end
