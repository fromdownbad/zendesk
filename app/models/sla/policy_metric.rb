require 'zendesk/sla'

module Sla
  class PolicyMetric < ActiveRecord::Base
    VALID_PRIORITIES = [
      PriorityType.LOW,
      PriorityType.NORMAL,
      PriorityType.HIGH,
      PriorityType.URGENT
    ].freeze

    EQUIVALENCY_ATTRIBUTES = %i[
      sla_policy_id
      metric_id
      business_hours
      target
    ].freeze

    self.table_name = :sla_policy_metrics

    has_soft_deletion default_scope: true

    attr_accessible

    scope :with_priority, ->(priority_id) { where(priority_id: priority_id) }

    belongs_to :account
    belongs_to :policy, class_name: 'Sla::Policy', foreign_key: 'sla_policy_id', inherit: :account_id

    validates_presence_of :account_id,
      :policy

    validates_numericality_of :priority_id,
      :metric_id, only_integer: true

    validates_numericality_of :target, only_integer: true, greater_than: 0

    validates_inclusion_of :business_hours, in: [true, false]

    validates_uniqueness_of :metric_id,
      scope: %i[
        account_id
        sla_policy_id
        priority_id
        deleted_at
      ]

    validate :validate_priority,
      :validate_metric

    delegate :core_metric, to: :metric

    def metric
      Zendesk::Sla::TicketMetric.from_id(metric_id)
    end

    def priority
      PriorityType[priority_id]
    end

    def to_audit
      {minutes: target, in_business_hours: business_hours?}
    end

    def equivalent?(policy_metric)
      EQUIVALENCY_ATTRIBUTES.all? do |attribute|
        public_send(attribute) == policy_metric.public_send(attribute)
      end
    end

    private

    def validate_priority
      return if VALID_PRIORITIES.include?(priority_id)

      errors.add(
        :priority_id,
        I18n.t('activerecord.errors.models.policy_metrics.not_recognized')
      )
    end

    def validate_metric
      return if Zendesk::Sla::TicketMetric.from_id(metric_id).present?

      errors.add(
        :metric_id,
        I18n.t('activerecord.errors.models.policy_metrics.not_recognized')
      )
    end
  end
end
