require 'zendesk/sla'

module Sla
  class TicketPolicy < ActiveRecord::Base
    UPSERT_SQL = <<-SQL.
      INSERT INTO `sla_ticket_policies`
        (
          `id`,
          `account_id`,
          `ticket_id`,
          `sla_policy_id`,
          `created_at`,
          `updated_at`
        )
      VALUES
        (
          %{id},
          %{account_id},
          %{ticket_id},
          %{sla_policy_id},
          %{created_at},
          %{updated_at}
        )
      ON DUPLICATE KEY UPDATE
        `sla_ticket_policies`.`sla_policy_id`=VALUES(`sla_policy_id`),
        `sla_ticket_policies`.`updated_at`=VALUES(`updated_at`)
    SQL
      freeze

    private_constant :UPSERT_SQL

    self.table_name = :sla_ticket_policies

    attr_accessible :account,
      :ticket,
      :policy

    belongs_to :account
    belongs_to :ticket, inverse_of: :sla_ticket_policy
    belongs_to :policy, class_name: 'Sla::Policy', foreign_key: 'sla_policy_id', inherit: :account_id

    has_many :statuses,
      class_name:  'Sla::TicketStatus',
      foreign_key: 'sla_ticket_policy_id',
      inverse_of:  :ticket_policy,
      dependent:   :destroy,
      autosave:    true,
      inherit: :account_id

    validates_presence_of :account_id,
      :ticket,
      :policy

    validates_uniqueness_of :ticket_id, scope: :account_id

    def self.upsert_sql
      UPSERT_SQL
    end

    def ticket
      Ticket.with_deleted { super }
    end

    def null_policy?
      sla_policy_id == Zendesk::Sla::NullPolicy.id
    end

    def policy
      null_policy? ? Zendesk::Sla::NullPolicy : super
    end

    def policy_metrics
      policy.policy_metrics.with_priority(ticket.priority_id)
    end

    def current_statuses(include_first_reply_time = false)
      policy_metric_ids = metric_statuses.map { |status| status.policy_metric.id }

      if include_first_reply_time && first_reply_time_policy_metric.present?
        policy_metric_ids << first_reply_time_policy_metric.id
      end

      statuses.where(sla_policy_metric_id: policy_metric_ids)
    end

    def cache_statuses
      if metric_statuses.any?
        ActiveRecord::Base.connection.execute(
          format(TicketStatus.upsert_sql, values: upsert_values)
        )
      end

      statuses(true)
    end

    private

    def metric_statuses
      Zendesk::Sla::MetricStatus.all(ticket).select(&:measured?)
    end

    def first_reply_time_policy_metric
      @first_reply_time_policy_metric ||= Zendesk::Sla::MetricStatus.new(ticket, :reply_time).first_policy_metric
    end

    def upsert_values
      metric_statuses.map do |metric_status|
        [
          TicketStatus.generate_uid,
          account.id,
          id,
          metric_status.policy_metric.id,
          metric_status.paused?,
          datetime_sql(metric_status.breach_at),
          datetime_sql(metric_status.fulfilled_at),
          'NOW()',
          'NOW()'
        ]
      end.map { |values| "(#{values.join(',')})" }.join(',') # rubocop:disable Style/MultilineBlockChain
    end

    def datetime_sql(time)
      time.try { |t| "'#{t.to_s(:db)}'" } || 'NULL'
    end
  end
end
