require 'zendesk/sla'

module Sla
  class Policy < ActiveRecord::Base
    class SlaPolicyLimitExceededError < StandardError
    end

    MAX_FILTER_SIZE = 65_535
    SLA_POLICIES_LIMIT = 2_000

    include CIA::Auditable

    self.table_name = :sla_policies

    attr_accessible :title,
      :description,
      :position,
      :filter

    attr_accessor :current_user

    audit_attribute :title,
      :description,
      :position

    belongs_to :account

    has_many :policy_metrics,
      class_name:  'Sla::PolicyMetric',
      foreign_key: :sla_policy_id,
      inherit: :account_id
    has_many :ticket_policies,
      class_name:  'Sla::TicketPolicy',
      foreign_key: :sla_policy_id,
      inherit: :account_id

    has_soft_deletion

    after_soft_delete :audit_deletion

    validates_presence_of :account,
      :title,
      :filter

    validates :position, presence: true, numericality: {only_integer: true}

    validate :valid_filter
    validate :valid_filter_size
    validate :policy_metric_uniqueness
    validate :within_policy_limit

    serialize :filter

    default_scope { order(:position, :id) }

    scope :active,  -> { where(deleted_at: nil) }
    scope :ordered, -> { order(:position) }

    def self.update_position(ids)
      where(id: ids).tap do |policies|
        policies.update_all(
          ['updated_at = ?, position = FIELD(id, ?)', Time.now, ids]
        )
      end
    end

    alias_attribute :definition, :filter # to support Zendesk::Rules::Match

    def priority_metric_map(priority_id)
      policy_metrics.
        with_priority(priority_id).
        each_with_object({}) do |priority_metric, mapping|
          mapping[priority_metric.metric_id] = priority_metric
        end
    end

    def active?
      deleted_at.blank?
    end

    def match?(ticket)
      Zendesk::Rules::Match.match?(self, ticket)
    end

    def rule_type
      @rule_type ||= self.class.name.underscore
    end

    private

    def audit_deletion
      CIA.record(:destroy, self) if account.has_service_level_agreements?
    end

    def valid_filter
      filter.run_new_validations(account, :sla_policy, self, current_user)
    end

    def valid_filter_size
      return unless filter_size.bytesize > MAX_FILTER_SIZE

      errors.add(
        :filter,
        I18n.t(
          'activerecord.errors.messages.too_long',
          count: MAX_FILTER_SIZE
        )
      )
    end

    def within_policy_limit
      return if account.sla_policies.active.count < SLA_POLICIES_LIMIT

      fail SlaPolicyLimitExceededError, I18n.t(
        'api.errors.slas.limit_exceeded',
        limit: SLA_POLICIES_LIMIT
      )
    end

    def policy_metric_uniqueness
      mapped = policy_metrics.map { |pm| [pm.metric_id, pm.priority_id] }

      return if mapped == mapped.uniq

      errors.add(
        :base,
        I18n.t('activerecord.errors.models.policy.policy_metrics_uniqueness')
      )
    end

    def filter_size
      @attributes['filter'].value_for_database
    end
  end
end
