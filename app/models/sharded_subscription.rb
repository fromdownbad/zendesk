class ShardedSubscription < ActiveRecord::Base
  # ***********************************************
  #
  # This table is updated with billing information on a nightly basis. It is intended to simplify the life of writers of ad-hoc
  # queries, who do not have access to rails infrastructure. It does not represent the current state of the world, which is
  # contained in the account_db in the accounts, payments, subscriptions and credit_cards tables.
  #
  # The intention is for this table to not contain any actual billing data
  # no payments, costs per agent, discounts, coupons, credit_cards, only the
  # consequences of that data (is the account active, what is the plan type, number of max agents.)
  #
  # ***********************************************

  belongs_to :account

  attr_accessible

  validates_presence_of :account_id
  validates_uniqueness_of :account_id

  def sync_from_account_database
    subscription = account.subscription

    self.subdomain = account.subdomain
    self.plan_type = subscription.plan_type
    self.max_agents = subscription.max_agents
    self.account_status = subscription.account_status
    self.account_type = subscription.account_type
    self.is_active = account.is_active?
    self.is_serviceable = account.is_serviceable?
    self.owner_id = account.owner_id
  end
end
