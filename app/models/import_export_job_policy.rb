class ImportExportJobPolicy
  attr_writer :key

  JOB_CLASS_MAPPING = {
    view_csv: "ViewCsvJob",
    xml_export: "XmlExportJob",
    user_xml_export: "UserXmlExportJob",
    report_feed: "CsvExportRecord",
    user_import: "UsersJob",
    organization_import: "OrganizationsJob",
    two_factor_export: "TwoFactorCsvJob"
  }.with_indifferent_access

  IMPORT_JOBS  = %w[user_import organization_import].freeze
  EXPORT_JOBS  = %w[report_feed user_xml_export xml_export].freeze
  VIEW_JOBS    = %w[view_csv].freeze
  ADMIN_JOBS   = %w[two_factor_export].freeze
  DEFAULT_JOBS = EXPORT_JOBS + VIEW_JOBS
  AGENT_JOBS   = IMPORT_JOBS + VIEW_JOBS

  EXPORT_JOB_CLASS_NAMES = JOB_CLASS_MAPPING.values_at(*EXPORT_JOBS)

  def initialize(account, user, param_key = nil)
    self.account = account
    self.user    = user
    self.key     = param_key
  end

  def klass_name
    JOB_CLASS_MAPPING[key]
  end

  def klass
    klass_name.try(:constantize)
  end

  def accessible_to_user?
    user.can?(:manage, Report) && export_configuration.whitelisted?(user)
  end

  def unavailable?
    return false unless klass.respond_to?(:available_for?)
    !klass.available_for?(account)
  end

  def inaccessible?
    return false if AGENT_JOBS.include?(key)
    return !user.is_admin? if ADMIN_JOBS.include?(key)
    !export_configuration.type_accessible?(key) || !accessible_to_user?
  end

  def ticket_count_exceeds_max?
    return false unless klass == XmlExportJob
    export_configuration.maximum_exceeded?(account.tickets.count_with_archived)
  end

  delegate :any_accessible?, to: :export_configuration

  private

  attr_reader :key
  attr_accessor :account, :user

  def export_configuration
    @export_configuration ||= Zendesk::Export::Configuration.new(account)
  end
end
