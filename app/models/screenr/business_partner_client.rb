require 'httparty'

# Talks to Screenr Business Partner API.
#
# It allows:
#   - lookup of tenants
#   - creation of tenants
#
# A Tenant is a registered user at Screenr.
#
# A Business Partner is a company able to create Tenant in the Screenr realm
# using the Business Partner API.
#
# A Business Partner needs to create Tenants in order to
# records Screencasts using Screenr.
#
# A Business Partner might need to lookup a Tenants to check their status
# (e.g. Is the user paying?)
#
# Examples
#   client = Screenr::BusinessPartnerClient.new
#   client.lookup_tenant('minimum')
#   # => {:domain           => "minimum",
#         :domain_url       => "https://minimum.sfssdev.com",
#         :upgraded_account => false,
#         :upgrade_url      =>"https://screenr-upgrade.com/test",
#         :recorders        => {:tickets_recorder_id => "a947c6",
#                               :forums_recorder_id  => "e041c9"}
#        }
#
#   client = Screenr::BusinessPartnerClient.new
#   client.create_tenant(:domain => 'minimum',
#                        :first_name => 'Søren',
#                        :last_name => 'Madsen',
#                        :email => 'sm@test.com')
#   # => {:domain           => "minimum",
#         :domain_url       => "https://minimum.sfssdev.com",
#         :upgraded_account => false,
#         :upgrade_url      => "https://screenr-upgrade.com/test",
#         :recorders        => {:tickets_recorder_id => "a947c6",
#                               :forums_recorder_id  => "e041c9"}
#        }
#
class Screenr::BusinessPartnerClient
  include HTTParty
  base_uri Zendesk::Configuration.dig!(:screenr, :base_uri)
  format :json
  screenr_api_timeout = Zendesk::Configuration.dig!(:screenr, :api_timeout).to_i
  default_timeout (screenr_api_timeout > 0) ? screenr_api_timeout : 10

  # Exception raised when a Tenant is not found after a lookup.
  TenantNotFound = Class.new(StandardError)

  # Exception raised when trying to create a Tenant using a domain already in use.
  # It gets a list of available Screenr domains.
  #
  # Returns an Array of String representing domains that
  # can be used to create an account.
  #
  # Example
  #   client = Screenr::BusinessPartnerClient.new
  #   begin
  #     client.create_tenant(:domain => 'minimum',
  #                          :first_name => 'Søren',
  #                          :last_name => 'Madsen',
  #                          :email => 'sm@test.com')
  #   rescue Screenr::BusinessPartnerClient::DomainAlreadyInUse=>e
  #     e.available_domains
  #     # => ['minimum1', 'minimum2', 'minimum3', 'minimum4', 'minimum5']
  #   end
  #
  class DomainAlreadyInUse < StandardError
    attr_reader :available_domains

    def initialize(available_domains)
      @available_domains = available_domains
    end
  end

  # Exception raised in case of validation failed
  #
  # Implements also the two helper methods first_error_on, first_error_messages
  # in case you want to fix one error at a time in your UI.
  #
  # Example
  #   client = Screenr::BusinessPartnerClient.new
  #   begin
  #     client.create_tenant(:domain => 'minimum',
  #                          :first_name => '',
  #                          :last_name => '',
  #                          :email => 'sm@test.com')
  #   rescue Screenr::BusinessPartnerClient::ValidationFailed=>e
  #     e.errors
  #     # => [{"first_name"  => "The OwnerLastName field is required."},
  #           {"last_name"   => "The OwnerFirstName field is required."}]
  #
  #     e.first_error_on
  #     # => "OwnerLastName"
  #
  #     e.first_error_messages
  #     # => "The OwnerLastName field is required."
  #   end
  #
  class ValidationFailed < StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = remap_to_interface_attributes(errors)
    end

    def first_error_on
      @errors.first.keys[0]
    end

    def first_error_messages
      @errors.first.values[0]
    end

    private
    # Internal: Maps Screenr end points naming convention to our Zendesk one.
    #
    # Returns a mapping Hash.
    def attributes_mapping
      {"Domain"         => 'domain',
       "OwnerFirstName" => 'first_name',
       "OwnerLastName"  => 'last_name',
       "OwnerEmail"     => 'email',
       # Screenr has different attributes for different end points
       # representing the same thing in our domain
       "FirstName"      => 'first_name',
       "LastName"       => 'last_name',
       "Email"          => 'email',
       "Tenant"         => 'domain'}
    end

    def remap_to_interface_attributes(array_of_errors)
      array_of_errors.map do |error|
        HashWithIndifferentAccess.new(attributes_mapping[error['Key']] => error['Value'])
      end
    end
  end

  # Exception raised when downgrade of Tenanthas not been sucessful.
  class DowngradeFailed < StandardError; end

  # Create a new Screenr::BusinessPartnerClient.
  #
  def initialize
    @auth = {:username => Zendesk::Configuration.dig!(:screenr, :username),
             :password => Zendesk::Configuration.dig!(:screenr, :password)}
  end

  # Public: Lookup a tenant.
  #
  # name - A String contaning tenant's name or subdomain.
  #
  # Returns an HashWithIndifferentAccess rappresenting the Tenant associated
  # Raises TenantNotFound if no tenant has been found.
  #
  def lookup_tenant(name)
    response = self.class.get("/tenants/#{name}", :basic_auth => @auth)
    raise TenantNotFound if error_code_from(response) == 404
    tenant_from(response)
  end

  # Create a tenant.
  #
  # Options
  #
  # :domain             - Requested name/sub-domain for tenant.
  # :first_name         - Owner’s first name.
  # :last_name          - Owner’s last name.
  # :email              - Owner’s email address.
  # :force_registration - If true the API will register the first available domain.
  #                       Default to false.
  #
  # Returns an HashWithIndifferentAccess rappresenting the Tenant associated.
  # Raises DomainAlreadyInUse if the domain is in use.
  # Raises ValidationFailed if there is a validation / syntax error.
  #
  def create_tenant(options = {})
    query = {
      "Domain"            => options.fetch(:domain),
      "OwnerFirstName"    => options.fetch(:first_name),
      "OwnerLastName"     => options.fetch(:last_name),
      "OwnerEmail"        => options.fetch(:email),
      "ForceRegistration" => options.fetch(:force_registration, false)
    }
    response = self.class.post("/tenants", :body => query, :basic_auth => @auth)

    if error_code_from(response) == 409
      available_domains = response.parsed_response['AvailableDomains']
      raise DomainAlreadyInUse, available_domains
    end

    if error_code_from(response) == 400
      errors = response.parsed_response['Message']
      raise ValidationFailed, errors
    end

    tenant_from(response)
  end

  # Update a tenant given the associated domain.
  #
  # domain - Tenant's domain.
  #
  # Options
  #
  # :first_name         - Owner’s first name.
  # :last_name          - Owner’s last name.
  # :email              - Owner’s email address.
  #
  # Returns a boolean. True/False of whether operation was successful.
  # Raises ValidationFailed if there is a validation / syntax error.
  #
  def update_tenant(domain, options = {})
    query = {
      "FirstName"  => options.fetch(:first_name),
      "LastName"   => options.fetch(:last_name),
      "Email"      => options.fetch(:email)
    }
    url = "/tenants/#{domain}/users/updateowner"
    response = self.class.post(url, :body => query, :basic_auth => @auth)

    if error_code_from(response) == 400
      errors = response.parsed_response['Message']
      raise ValidationFailed, errors
    end

    response.parsed_response["Success"]
  end

  # Downgrade a tenant given the associated domain.
  #
  # This sends Screenr a request to turn a paying Tenant into a base plan.
  # Tenant with current subscription is cancelled and downgraded to base plan.
  #
  # domain - Tenant's domain.
  #
  # Returns a boolean. True/False of whether operation was successful.
  def downgrade_tenant(domain)
    query = { "Domain" => domain }
    url = "/tenants/downgrade"

    response = self.class.post(url, :body => query, :basic_auth => @auth)

    if error_code_from(response) == 400
      raise DowngradeFailed
    end

    response.parsed_response["Success"]
  end

  private

  def error_code_from(response)
    response.parsed_response['ErrorCode']
  end

  def tenant_from(response)
    decamelize(response.parsed_response)
  end

  def decamelize(hash)
    hash.inject(HashWithIndifferentAccess.new) do |memo, (k, v)|
      memo[k.underscore] = (v.is_a? Hash) ? decamelize(v) : v
      memo
    end
  end

end
