module Screenr
  # Manage Screenr Tenant for an account.
  #
  # A Tenant is created locally to store all the information needed to interact with Screenr.
  #
  # Examples
  #
  #   screenr_integration = Screenr::Integration.new(current_account)
  #   tenant = provide_tenant('minumum')
  #
  #   screenr_integration = Screenr::Integration.new(current_account)
  #   screenr_integration.unlink_tenant!
  #
  class Integration

    # Create a new Screenr::Integration.
    #
    # account - the Account on which to operate
    #
    def initialize(account)
      @account = account
    end

    # Create a new Screenr::Tenant and associate it with the Account.
    #
    # Note that it handles the creation on Screenr side using the
    # Screenr::BusinessPartnerClient API.
    # Screenr will create a Tenant on it's side using fake Zendesk credentials.
    #
    # NOTE: It asks to force registration, meaning that the Screenr's API should
    # register the first available domain name if the requested is not
    # available.
    #
    # domain - a String defining the domain we want to associate to the Tenant.
    # activate_screencasts_for_tickets - a Boolean defining if Screencast for Ticket must be
    #                                    activated after provisioning for the associated Account.
    #
    # Returns the new created Tenant.
    def provide_tenant(domain, activate_screencasts_for_tickets = false)
      client = Screenr::BusinessPartnerClient.new
      result = client.create_tenant({
        :domain             => domain,
        :first_name         => 'Zendesk',
        :last_name          => 'Zendesk',
        :email              => 'noreply@zendesk.com',
        :force_registration => true
      })
      screenr_tenant = @account.build_screenr_tenant(result)
      screenr_tenant.save! # I want an exception if it is not saved

      if (activate_screencasts_for_tickets)
        @account.settings.screencasts_for_tickets = 1
        @account.save!
      end

      return screenr_tenant
    end

    # Send first_name, last_name and email to Screenr for the associated
    # Screenr::Tenant based on a User.
    #
    # user - The User which information will be sent to Screenr
    #
    # Returns an updated version of the associated Tenant.
    def send_personal_data_to_screenr_for(user)
      client = Screenr::BusinessPartnerClient.new
      screenr_tenant = @account.screenr_tenant
      result = client.update_tenant(
        screenr_tenant.domain,
        :first_name => user.first_name,
        :last_name  => user.last_name,
        :email      => user.email
      )

      screenr_tenant.update_attribute(:personal_data_sent_to_screenr, !!result)

      return screenr_tenant.reload
    end

    # Unlink the Tenant associated to the Account.
    #
    # Note that this will also take care of deactivating
    # screencasts_for_tickets in the Account's settings
    #
    #
    # Returns nothing.
    def unlink_tenant!
      @account.screenr_tenant.delete
      @account.settings.screencasts_for_tickets = 0
      @account.save!

    end

    # Downgrade the Tenant associated to the Account.
    #
    # Tenant with current subscription is cancelled and downgraded to base plan.
    #
    # Returns a boolean. True/False of whether operation was successful.
    def downgrade_tenant!
      client = Screenr::BusinessPartnerClient.new
      client.downgrade_tenant(domain)
    end

    # Check if Account has already gone through a successfully
    # provisioning procedure.
    #
    # Returns true if so, false otherwise.
    def has_completed_provisioning?
      screenr_tenant.present?
    end

    # The Screener recorder id that Account will use when
    # recording screencasts associated to Tickets.
    #
    # Returns a String rappresenting the id or nil if Account hasn't completed provisioning.
    def tickets_recorder_id
      screenr_tenant.recorders[:tickets_recorder_id] if has_completed_provisioning?
    end

    # The Screener recorder id that Account will use when
    # recording screencasts associated to Forum Topic.
    #
    # Returns a String rappresenting the id or nil if Account hasn't completed provisioning.
    def forums_recorder_id
      screenr_tenant.recorders[:forums_recorder_id] if has_completed_provisioning?
    end

    # The Screener tenant host name.
    #
    # Example
    #   minimum.viewscreencasts.com
    #
    # Returns a String containing an host name or nil if Account hasn't completed provisioning.
    def host
      screenr_tenant.host if has_completed_provisioning?
    end

    # The domain associated to the Tenant
    #
    # Returns a string or nil if Account hasn't completed provisioning.
    def domain
      screenr_tenant.domain if has_completed_provisioning?
    end

    # The URL where user will be redirected when we need him
    # to upgrade to a paying plan on Screenr.
    #
    # Returns a URL or nil if Account hasn't completed provisioning.
    def upgrade_url
      screenr_tenant.upgrade_url if has_completed_provisioning?
    end

    # The base URI to Screenr Business Partener API.
    # This is for example used by the tinyMCE plugin to communicate
    # with Screenr API to validate status of an account, without using Zendesk
    # as man in the middle.
    #
    # Returns nothing.
    def base_uri
      Zendesk::Configuration.screenr['base_uri']
    end

    private

    def screenr_tenant
      @account.screenr_tenant
    end

  end
end
