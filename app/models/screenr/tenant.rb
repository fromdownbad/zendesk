module Screenr
  class Tenant < ActiveRecord::Base
    self.table_name = 'screenr_tenants'
    belongs_to :account

    attr_accessible :account, :domain, :host, :upgraded_account, :upgrade_url, :recorders

    validates_presence_of :account_id
    validates_presence_of :domain
    validates_presence_of :host

    serialize :recorders, Hash
  end
end
