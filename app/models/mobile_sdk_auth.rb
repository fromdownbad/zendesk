class MobileSdkAuth < ActiveRecord::Base
  include CIA::Auditable

  TYPE_ANONYMOUS = "anonymous".freeze
  TYPE_JWT = "jwt".freeze

  belongs_to :account

  belongs_to :client, class_name: "Zendesk::OAuth::Client", dependent: :destroy

  has_one :mobile_sdk_app, inherit: :account_id

  attr_accessible :endpoint, :id, :name, :shared_secret, :client
  attr_accessor   :next_secret

  before_validation     :set_secret
  after_destroy         :destroy_app
  validates_presence_of :name, :client
  validates_format_of :endpoint, with: /\Ahttps:\/\/[^\s]+\Z/, message: I18n.t("txt.admin.views.mobile_sdk.field.jwt_url.validation_error"),
                                 if: :endpoint_present

  audit_attribute :endpoint, :shared_secret, :deleted_at

  def shared_secret(force_full = false)
    secret = read_attribute(:shared_secret)

    if new_record? || force_full
      secret
    elsif secret
      secret[0..9]
    end
  end

  def shared_secret=(secret)
    if new_record?
      write_attribute(:shared_secret, secret)
    else
      message = "Not allowed to update shared_secret"
      ZendeskExceptions::Logger.record(StandardError.new(message), location: self, message: message, reraise: !Rails.env.production?, fingerprint: '31f3fde99e7b3740ef9d58d1e63518d54da25226')
    end
  end

  def generate_shared_secret
    write_attribute(:shared_secret, Token.generate(48, false))
  end

  def generate_shared_secret!
    generate_shared_secret.tap { save! }
  end

  private

  def set_secret
    write_attribute(:shared_secret, next_secret) if next_secret.present?
  end

  def destroy_app # Avoid an infinite loop with mobile_sdk_app and mobile_sdk_auth destroying each other
    return unless mobile_sdk_app(:reload).present?
    mobile_sdk_app.destroy
  end

  def endpoint_present
    endpoint.present?
  end
end
