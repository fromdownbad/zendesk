class Linking < ActiveRecord::Base
  belongs_to :ticket
  belongs_to :external_link, polymorphic: true
  belongs_to :account

  attr_accessible :external_link, :account

  before_validation     :set_account
  validates_presence_of :account_id

  private

  def set_account
    self.account ||= ticket.account if ticket.present?
  end
end
