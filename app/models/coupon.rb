class Coupon < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible

  has_many :coupon_applications
  has_many :coupon_redemptions, through: :coupon_applications

  before_destroy :validate_editable

  validate :validate_readonly_attributes_after_effective_date, on: :update
  validate :validate_type_not_changed, on: :update

  validates_presence_of :name
  validates_uniqueness_of :name, case_sensitive: false

  validates_presence_of :coupon_code
  validates_uniqueness_of :coupon_code, case_sensitive: false

  validates_presence_of :type

  validates_presence_of :expiry, :effective
  validate :validate_expiry_after_effective

  validates_numericality_of :length_days, only_integer: true, greater_than: 0, allow_nil: true

  def self.find_by_coupon_code(coupon_code)
    where(coupon_code: coupon_code.to_s).first
  end

  def in_effective_window?
    !(before_effective_window? || after_effective_window?)
  end

  def before_effective_window?
    effective > DateTime.now
  end

  def after_effective_window?
    expiry <= DateTime.now
  end

  def active?
    !forced_inactive && in_effective_window?
  end

  def editable?
    return false unless DateTime.now <= (effective_changed? ? effective_was : effective)
    !coupon_applications.exists?
  end

  def valid_for_signup?
    false
  end

  def is_valid_on?(_coupon_application, _subscription_instance) # rubocop:disable Naming/PredicateName
    false
  end

  # HACK: Rails STI's don't include the type field when serializing
  def discount_type
    self.class.name.underscore.to_sym
  end

  def as_json(_options = { })
    super(methods: [:discount_type])
  end

  private

  def validate_editable
    if editable?
      true
    else
      errors.add :base, "Coupons can only be destroyed before their effective date and when there are no applications"
      false
    end
  end

  def validate_type_not_changed
    if !type.nil? && type_changed?
      errors.add :type, "#type cannot be changed once a coupon is saved"
    end
  end

  def validate_expiry_after_effective
    if !expiry.nil? && !effective.nil? && expiry <= effective
      errors.add :base, 'expiry must be after effective'
    end
  end

  MUTABLE_ATTRIBUTES = ["forced_inactive"].freeze

  def validate_readonly_attributes_after_effective_date
    # if effective is being changed consider the **old** effective date, that allows changing effective to the past, but
    # not the present
    return if editable?

    (changed - MUTABLE_ATTRIBUTES).each do |attr|
      errors.add attr, "#{attr} is readonly after the coupon's effective date"
    end
  end
end
