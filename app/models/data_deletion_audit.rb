class DataDeletionAudit < ActiveRecord::Base
  not_sharded
  disable_global_uid

  include DataDeletionLifecycle

  belongs_to  :account
  has_many    :job_audits, foreign_key: :audit_id, class_name: 'DataDeletionAuditJob', inherit: :account_id

  serialize :failed_reason, Zendesk::Coders::JSONWithStringFallback

  attr_accessible :reason

  scope :active,             -> { where(status: :started) }
  scope :enqueued,           -> { where(status: :enqueued) }
  scope :waiting,            -> { where(status: :waiting) }
  scope :for_cancellation,   -> { where(reason: :canceled) }
  # Cancellations infer shard_id from accounts table
  scope :pod_local_canceled, -> { where(reason: :canceled).joins('INNER JOIN accounts ON accounts.id = data_deletion_audits.account_id').where('accounts.shard_id' => pod_local_shard_ids) }
  scope :pod_local_moved,    -> { where(reason: :moved, shard_id: pod_local_shard_ids) }

  def self.build_for_cancellation
    DataDeletionAudit.new(reason: 'canceled')
  end

  def self.pod_local_shard_ids
    ActiveRecord::Base.shard_names.map(&:to_i)
  end

  # Mark deletion as ready to be run at earliest possibility
  def enqueue!
    self.status = 'enqueued'
    save!
  end

  def account
    # Ensure strong consistency:
    # - Slaves could be lagged
    # - Kasket could be stale in this pod, or just weren't purged by Exodus
    Account.with_deleted { Account.on_master.without_kasket { Account.find(account_id) } }
  end

  # `on_master` because `AccountDeletionManager` needs a strongly-consistent
  # view of the world when deciding if a job has already been created, to
  # prevent duplicates.
  #
  # The `job_audits` association should cache after the first call. Since the SQL may
  # go over the WAN, it's significantly faster to filter in ruby instead of
  # making separate calls, and less load to the single master database.
  #
  # @return [Array<DataDeletionAuditJob>]
  def attempts_by_job(job_klass)
    DataDeletionAuditJob.on_master do
      job_audits.select { |job_audit| job_audit.job == job_klass.name }
    end
  end

  def build_job_audit_for_klass(job_klass)
    job_audits.build(account_id: account_id, shard_id: shard_id, job: job_klass.name)
  end
end
