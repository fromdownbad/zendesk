class Experiment < ActiveRecord::Base
  self.table_name = 'experiment_participations'
  attr_accessible :name, :version, :group, :finished

  belongs_to :account

  validates_presence_of :account_id
  validates_presence_of :name
  validates_presence_of :group

  def to_data
    attributes.slice('name', 'group', 'version').
      merge('finished' => finished?).
      symbolize_keys
  end
end
