class TicketPrediction < ActiveRecord::Base
  include CachingObserver
  include ArSkippedCallbackMetrics::Instrument

  attr_accessible :satisfaction_probability

  belongs_to :ticket, inverse_of: :ticket_prediction

  class << self
    def update_satisfaction_probability(ticket, new_probability)
      return if new_probability.blank?
      rounded_probability = new_probability.round(6)

      if rounded_probability != current_satisfaction_probability(ticket)
        ticket.satisfaction_probability_delta_will_change!
        ticket_prediction(ticket).satisfaction_probability = rounded_probability
      end

      rounded_probability
    end

    def current_satisfaction_probability(ticket)
      if ticket.ticket_prediction.present?
        ticket.ticket_prediction.satisfaction_probability
      end
    end

    def satisfaction_prediction_model_available?(account)
      api_client = Zendesk::SatisfactionPredictionApp::InternalApiClient.new(account)
      api_client.fetch_satisfaction_prediction_available
    rescue StandardError => e
      Rails.logger.error("Unable to check the availability of customer satisfaction prediction:- #{e.class}: #{e} -- #{__FILE__}:#{__LINE__}")
      return false
    end

    private

    def ticket_prediction(ticket)
      if ticket.ticket_prediction.present?
        ticket.ticket_prediction
      else
        ticket.build_ticket_prediction
      end
    end
  end
end
