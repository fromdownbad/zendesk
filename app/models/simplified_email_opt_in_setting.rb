class SimplifiedEmailOptInSetting < ActiveRecord::Base
  belongs_to :account

  validates_presence_of :account_id

  attr_accessible :name, :value
end
