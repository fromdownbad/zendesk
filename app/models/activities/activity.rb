class Activity < ActiveRecord::Base
  include CachingObserver
  include Zendesk::Serialization::ActivitySerialization

  after_create PushNotificationObserver

  belongs_to :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  belongs_to :account
  belongs_to :actor, class_name: "User", inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  belongs_to :activity_object, polymorphic: true
  belongs_to :activity_target, polymorphic: true

  attr_accessible :user, :account, :actor, :activity_object, :activity_target, :verb

  validates_presence_of :account_id, :user_id, :actor_id, :activity_object_id, :activity_object_type

  scope :recent, -> { limit(20).order("activities.created_at DESC") }

  scope :since, ->(time) { where(['activities.created_at > ?', time]) }

  def self.create_activity_for(_user, _options)
    raise "Must be implemented in subclass"
  end

  def self.cleanup
    to_delete = []

    all.select('id, created_at').find_each do |activity|
      break if activity.created_at > 1.month.ago
      to_delete << activity.id
    end

    to_delete.in_groups_of(1000) do |id_list|
      delete_all(id: id_list.compact)
    end
  end
end
