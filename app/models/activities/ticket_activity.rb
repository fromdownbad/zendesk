class TicketActivity < Activity
  include Zendesk::Serialization::TicketActivitySerialization

  def self.create_activity_for(user, options)
    event, update_type = { update_type: :update }.merge(options).values_at(:event, :update_type)

    return if user.nil? || !user.is_agent?
    return if event.author_id == user.id || !event.ticket
    return unless kind = activity_type(user, event, update_type)

    return if kind.in?(%w[tickets.assignment tickets.priority_increase]) && event.ticket.solved?

    # don't create priority_increase activities for CCs
    return if kind == "tickets.priority_increase" && user != event.ticket.assignee

    object = (kind == "tickets.assignment" ? event.ticket : event)

    TicketActivity.create!(
      account: event.account,
      user: user,
      actor: event.author,
      verb: kind,
      activity_object: object,
      activity_target: event.ticket
    )
  end

  def title
    I18n.t("txt.activities.#{verb}", title_options)
  end

  def title_options
    { actor_name: actor.name, ticket_nice_id: activity_target.nice_id }.tap do |options|
      if verb == "tickets.comment"
        options[:comment] = activity_object.body
      elsif verb == "tickets.priority_increase"
        options[:new_priority] = PriorityType[activity_object.value.to_i].localized_name
      end
    end
  end

  def detail
    case verb
    when "tickets.assignment"
      activity_target.description
    when "tickets.comment"
      activity_object.body
    when "tickets.priority_increase"
      "%s to %s" % [PriorityType[activity_object.value_previous].to_s, PriorityType[activity_object.value].to_s]
    end
  end

  def serialization_data
    { object: activity_object_data, target: activity_target_data }
  end

  protected

  def activity_object_data
    case verb
    when "tickets.assignment"
      {ticket: {nice_id: activity_object.nice_id, subject: activity_object.title}}
    when "tickets.comment"
      {comment: {value: activity_object.body, is_public: activity_object.is_public}}
    when "tickets.priority_increase"
      {change: {value_previous: activity_object.value_previous, value: activity_object.value}}
    end
  end

  def activity_target_data
    {ticket: {nice_id: activity_target.nice_id, subject: activity_target.title}}
  end

  class << self
    private

    def activity_type(user, evt, update_type)
      if comment?(evt, update_type)
        "tickets.comment"
      elsif assignment?(user, evt)
        "tickets.assignment"
      elsif priority_increased?(evt)
        "tickets.priority_increase"
      end
    end

    def comment?(evt, update_type)
      evt.is_a?(Comment) && (update_type != :create)
    end

    def assignment?(user, evt)
      (evt.value_reference == "assignee_id") && (evt.value.to_i == user.id)
    end

    def priority_increased?(change)
      change.value_reference == "priority_id" && change.value.to_i > change.value_previous.to_i
    end
  end
end
