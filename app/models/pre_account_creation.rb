class PreAccountCreation < ActiveRecord::Base
  not_sharded
  disable_global_uid
  has_soft_deletion default_scope: true

  JOB_DELAY = 10.minutes
  NUM_ACCOUNT_PRECREATE = 50
  OWNER_EMAIL_PLACEHOLDER = 'noreply@zendesk.com'.freeze
  SUBDOMAIN_PREFIX = 'z3nprecreated'.freeze
  DEFAULT_ACCOUNT_CLASS = ::Accounts::Classic.to_s.freeze
  DEFAULT_ACCOUNT_NAME = 'z3n Precreated Account'.freeze

  belongs_to :account
  before_validation :fill_optional_fields

  default_scope { where('pre_account_creations.created_at >= ?', PreAccountCreation.freshness_threshold) }
  scope :valid_accounts, -> { where(status: ProvisionStatus::INIT) }

  validates :account_id,     presence: true
  validates :locale_id,      presence: true
  validates :pod_id,         presence: true
  validates :status,         presence: true
  validates :account_class,  presence: true
  validates :source,         presence: true
  validates :region,         presence: true

  attr_accessible :locale_id, :account_id, :pod_id, :region, :deleted_at
  attr_accessible :status, :account_class, :bound_at, :source

  module ProvisionStatus
    INIT     = 0
    ONGOING  = 1
    FINISHED = 2
  end

  class << self
    def find_precreated_account(conditions = {})
      first_stale_id = find_first_stale_id

      pre_creation = transaction do
        precreated_scope = where(
          default_search_conditions.merge(conditions)
        )
        precreated_scope = precreated_scope.where('id > ?', first_stale_id) if first_stale_id
        precreated_scope.lock(true).first.tap do |precreated_account|
          return unless precreated_account
          precreated_account.start_binding
        end
      end
      pre_creation.mark_latest_stale_record_id(first_stale_id)

      pre_creation.precreate_new_account

      pre_creation
    end

    def find_first_stale_id
      unscoped.where('created_at < ?', freshness_threshold).order('id DESC').limit(1).pluck(:id).first
    end

    def precreation_supported?(account_class)
      supported_account_classes.include?(account_class.to_s)
    end

    def freshness_threshold
      14.days.ago
    end

    private

    def current_pod_id
      Zendesk::Configuration.fetch(:pod_id)
    end

    def supported_account_classes
      [::Accounts::Classic.to_s, ::Accounts::GoogleAppMarket.to_s]
    end

    def default_search_conditions
      {
        locale_id:     ENGLISH_BY_ZENDESK.id,
        account_class: DEFAULT_ACCOUNT_CLASS,
        pod_id:        current_pod_id,
        status:        ProvisionStatus::INIT,
      }
    end
  end

  def account
    Kernel.const_get(account_class).find_by_id(account_id)
  end

  def pool_size
    @pool_size ||= calculate_current_pool_size
  end

  def need_more_accounts?
    pool_size < intended_pool_size
  end

  def intended_pool_size
    ENV.fetch('ZENDESK_PRECREATE_ACCOUNT_POOL_SIZE', NUM_ACCOUNT_PRECREATE).to_i
  end

  def start_binding
    update_attributes!(status: ProvisionStatus::ONGOING)
  end

  def precreate_new_account
    enqueue_precreate_account_job if need_more_accounts?
  end

  def is_binding? # rubocop:disable Naming/PredicateName
    status == ProvisionStatus::ONGOING
  end

  def finish_binding
    update_attributes!(
      status:     ProvisionStatus::FINISHED,
      bound_at:   Time.now
    )
  end

  def bounded?
    status == ProvisionStatus::FINISHED
  end

  def enqueue_precreate_account_job
    AccountsPrecreationJob.enqueue(
      {
        owner: {
          name:           'Precreated account owner',
          email:          OWNER_EMAIL_PLACEHOLDER,
        },

        address: {
          phone:          '123-456-7890'
        },

        account: {
          name:           DEFAULT_ACCOUNT_NAME,
          source:         source,
          language:       TranslationLocale.find(locale_id).locale,
          subdomain:      next_subdomain,
          help_desk_size: '1-9'
        }
      },
      '127.0.0.1',
      region
    )
  end

  def mark_latest_stale_record_id(id)
    @latest_stale_record_id = id
  end

  private

  def calculate_current_pool_size
    query_scope = PreAccountCreation.valid_accounts.where(pod_id: pod_id, locale_id: locale_id, region: region)
    query_scope = query_scope.where('id > ?', latest_stale_record_id) if latest_stale_record_id
    query_scope.count(:all)
  end

  def latest_stale_record_id
    @latest_stale_record_id || self.class.find_first_stale_id
  end

  def fill_optional_fields
    self.status        ||= ProvisionStatus::INIT
    self.account_class ||= DEFAULT_ACCOUNT_CLASS
  end

  def next_subdomain
    "#{SUBDOMAIN_PREFIX}#{rand(10000000000)}"
  end
end
