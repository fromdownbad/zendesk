class AlertDismissal < ActiveRecord::Base
  belongs_to :alert
  belongs_to :account
  belongs_to :user

  attr_accessible :account, :alert, :user

  validates_presence_of :alert, :account_id, :user

  # Deletes alert dismissals for alerts that no longer exist
  def self.cleanup
    alert_ids = Alert.all.map(&:id)
    AlertDismissal.without_arsi.delete_all(["alert_id NOT IN (?)", alert_ids])
  end
end
