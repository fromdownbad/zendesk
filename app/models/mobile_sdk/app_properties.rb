require "property_sets/delegator"

class MobileSdkApp < ActiveRecord::Base
  include PropertySets::Delegator

  REFERRER_URL = "https://www.zendesk.com/embeddables".freeze

  property_set(:settings, inherit: :account_id) do
    property :rma_enabled, type: :boolean, default: true
    property :rma_visits, type: :integer, default: 15
    property :rma_duration, type: :integer, default: 7
    property :rma_delay, type: :integer, default: 3
    property :rma_tags, type: :serialized, default: [].to_json
    property :rma_ios_store_url, type: :string
    property :rma_android_store_url, type: :string

    property :conversations_enabled, type: :boolean, default: false
    property :contact_us_tags, type: :serialized, default: [].to_json

    property :helpcenter_enabled, type: :boolean, default: false

    property :authentication, type: :string

    property :push_notifications_enabled, type: :boolean, default: false
    property :push_notifications_type, type: :string
    property :push_notifications_callback, type: :string
    property :urban_airship_key, type: :string
    property :urban_airship_master_secret, type: :string

    property :connect_enabled, type: :boolean, default: false

    property :help_center_article_voting_enabled, type: :boolean, default: false

    # mobile SDK blips
    property :blips, type: :boolean, default: true
    property :required_blips, type: :boolean, default: true
    property :behavioural_blips, type: :boolean, default: true
    property :pathfinder_blips, type: :boolean, default: true

    property :collect_only_required_data, type: :boolean, default: false
    property :support_never_request_email, type: :boolean, default: false
    property :support_show_closed_requests, type: :boolean, default: true
    property :support_show_referrer_logo, type: :boolean, default: true
    property :support_system_message_type, type: :string, default: MobileSdkApp::SYSTEM_MESSAGE_TYPE_DEFAULT
    property :support_system_message, type: :string

    # Answer bot
    property :answer_bot, type: :boolean, default: true

    # Talk
    property :talk_enabled, type: :boolean, default: false
    property :talk_button_id, type: :string
    property :talk_phone_number_id, type: :integer
    property :talk_group_id, type: :integer
    property :talk_embeddable_id, type: :integer

    # CSAT
    property :support_show_csat, type: :boolean, default: false

    validates_numericality_of :value,
      only_integer: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 99,
      message: I18n.t('txt.admin.views.mobile_sdk.app.edit.rma_visits.validation_error'),
      if: proc { |r| r.name.to_sym == :rma_visits }

    validates_numericality_of :value,
      only_integer: true,
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 99,
      message: I18n.t('txt.admin.views.mobile_sdk.app.edit.rma_duration.validation_error'),
      if: proc { |r| r.name.to_sym == :rma_duration }

    validates_numericality_of :value,
      only_integer: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 3600,
      message: I18n.t('txt.admin.views.mobile_sdk.app.edit.rma_delay.validation_error'),
      if: proc { |r| r.name.to_sym == :rma_delay }

    validates_inclusion_of :value,
      in: [MobileSdkAuth::TYPE_ANONYMOUS, MobileSdkAuth::TYPE_JWT],
      message: I18n.t('txt.admin.views.mobile_sdk.app.edit.authentication.validation_error'),
      if: proc { |r| r.name.to_sym == :authentication }

    validates :value, length: {
        maximum: MobileSdkApp::SYSTEM_MESSAGE_MAX_LENGTH,
        message: I18n.t('txt.admin.views.mobile_sdk.field.system_message_custom.length_error_v2'),
        if: proc { |r| r.name.to_sym == :support_system_message }
      }
  end

  validate :support_system_message_format

  def fetch_settings_for_monitor
    json = settings.each_with_object({}) do |setting, obj|
      next if setting.value.blank?
      value = settings.send(setting.name)

      obj[setting.name.to_sym] =
        case value
        when Array then value.join(', ')
        else value
        end
    end

    # Some of the settings required for Monitor are methods
    # (e.g https://github.com/zendesk/zendesk/blob/master/app/models/mobile_sdk/app_properties.rb#L171-L178)
    # therefore we needed to filter all the methods defined in MobileSdkApp
    # to fetch all the relevant methods and then call them to obtain the results and add it to the payload
    MobileSdkApp.public_instance_methods(false).map(&:to_s).grep(/^(support|help_center).+\?$/).
      each { |method| json[method.tr('?', '').to_sym] = send(method) }

    json
  end

  def support_system_message_format
    Liquid::Template.parse(settings.support_system_message)
  rescue Liquid::SyntaxError
    errors.add(
      :base,
      I18n.t("txt.admin.views.mobile_sdk.field.system_message_custom.format_error")
    )
  end

  def fetch_settings(*properties)
    properties.each_with_object({}) do |property, hash|
      hash[property] = settings.send(property)
    end
  end

  def support_never_request_email?
    account.has_mobile_sdk_support_never_request_email_setting? && settings.support_never_request_email?
  end

  # The actual value of the setting
  def support_show_closed_requests?
    # The default is true for everyone even when the feature is disabled. Pre-v2 apps for example.
    return true unless account.has_mobile_sdk_support_show_closed_requests_setting?
    # If the account does not satisfy the minimal bussiness requirements we show closed requests.
    return true unless account.subscription.plan_type > ZBC::Zendesk::PlanType::Essential.plan_type

    # If the account meets the requirements, then we check what the user actually wants.
    settings.support_show_closed_requests?
  end

  def support_show_referrer_logo?
    # the default is true for everyone even when the feature is disabled.
    return true unless account.has_mobile_sdk_support_show_referrer_logo_setting?
    # only Enterprise plan accounts can disable the referrer logo
    return true unless account.subscription.plan_type == ZBC::Zendesk::PlanType::Enterprise.plan_type

    settings.support_show_referrer_logo?
  end

  def support_system_message(locale, user)
    return "" unless account.has_mobile_sdk_support_system_message?

    case settings.support_system_message_type
    when MobileSdkApp::SYSTEM_MESSAGE_TYPE_DEFAULT
      I18n.with_locale(locale.locale) { I18n.t('txt.embeddables.support.sdk.default_system_message') }
    when MobileSdkApp::SYSTEM_MESSAGE_TYPE_CUSTOM
      @dc_cache ||= Zendesk::DynamicContent::AccountContent.cache(account, locale)
      Zendesk::Liquid::DcContext.render(settings.support_system_message, account, user, 'text/plain', locale, @dc_cache)
    end
  end

  # Blips settings helper methods
  def required_blips_enabled?
    account.has_mobile_sdk_blips? &&                    # global arturo shutdown everything
    account.has_mobile_sdk_required_blips? &&           # global arturo shutdown all required blips
    account.settings.mobile_sdk_blips? &&               # account level shutdown all blips
    account.settings.mobile_sdk_required_blips? &&      # account level shutdown require blips
    settings.blips? &&                                  # app level shutdown all blips
    settings.required_blips?                            # app level shutdown required blips
  end

  def behavioural_blips_enabled?
    account.has_mobile_sdk_blips? &&                    # global arturo shutdown everything
    account.has_mobile_sdk_behavioural_blips? &&        # global arturo shutdown behavioural blips
    account.settings.mobile_sdk_blips? &&               # account level shutdown all blips
    account.settings.mobile_sdk_behavioural_blips? &&   # account level shutdown behavioural blips
    settings.blips? &&                                  # app level shutdown all blips
    settings.behavioural_blips?                         # app level shutdown behavioural blips
  end

  def pathfinder_blips_enabled?
    account.has_mobile_sdk_blips? &&                    # global arturo shutdown everything
    account.has_mobile_sdk_pathfinder_blips? &&         # global arturo shutdown pathfinder blips
    account.settings.mobile_sdk_blips? &&               # account level shutdown all blips
    account.settings.mobile_sdk_pathfinder_blips? &&    # account level shutdown pathfinder blips
    settings.blips? &&                                  # app level shutdown all blips
    settings.pathfinder_blips?                          # app level shutdown pathfinder blips
  end

  def answer_bot_enabled?
    answer_bot_client = Zendesk::AnswerBotService::InternalApiClient.new(account)
    account.has_mobile_sdk_answer_bot? && settings.answer_bot? && answer_bot_client.available(Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK)
  end

  def support_show_csat?
    # The default is false if the account does not have this feature enabled
    return false unless account.has_mobile_sdk_csat?
    # If the account does not have csat enabled return false
    return false unless account.has_customer_satisfaction_enabled?

    settings.support_show_csat?
  end
end

MobileSdkAppSetting.class_eval do
  has_kasket
  has_kasket_on :mobile_sdk_app_id

  belongs_to :account
  belongs_to :mobile_sdk_app

  attr_accessible :name, :value
end
