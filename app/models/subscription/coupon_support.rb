class Subscription
  class InvalidCouponException < StandardError; end

  module CouponSupport
    def self.included(base)
      base.send(:attr_accessor, :trial_coupon_code)
      base.has_many(:coupon_applications, -> { order(exhaustion_date: :desc) }, dependent: :destroy)
      base.before_validation(:save_coupon_changes)
      base.after_create(:apply_trial_coupon)
    end

    def save_coupon_changes
      if @coupon_change.present?
        @coupon_change.commit!
      end
    end

    # batch sets pricing model input fields without saving
    # todo: 1) refactor out the coupon change stuff into Subscription#consider_coupon
    # 2) refactor this whole method into an override of update_attributes(!)?; this requires changing the views to put coupon_code into the same forms as other subscription variables
    def apply_billing_and_coupon_changes(params, new_coupon_code = nil)
      apply_billing_changes(params)
      apply_coupon_change(new_coupon_code)
    end

    def apply_billing_changes(billing)
      (billing || { }).each_pair do |k, v|
        next unless Subscription::BILLING_INPUTS.include?(k.to_sym)
        k = :base_agents if k.to_sym == :max_agents
        send("#{k}=", v)
      end
    end

    def apply_coupon_change(coupon_code = nil)
      coupon_code    = nil if coupon_code.blank?
      @coupon_change = Zendesk::Billing::CouponChange.new(self)
      @coupon_change.consider(coupon_code)
    end

    # TODO: make this three methods --
    # 1. A true "current" coupon application that respects coupon_change
    # 2. A time aware version that looks up any coupon applications active at the given (and required) date
    # 3. An active_coupon_application_was that follows the *_was semantics of Rails
    # OR
    # figure out some way to make the relationship proxy carry the water using helper scoping methods. I think it's possible.
    def active_coupon_application(as_of = nil)
      ca = if (as_of.nil? || as_of >= Time.now) && @coupon_change.present?
        @coupon_change.active_coupon_application
      else
        coupon_applications.first
      end

      ca = nil if ca.present? && ca.exhausted?(as_of.nil? ? Time.now : as_of)
      ca
    end

    # TODO: move this into CouponApplication#initialize(subscription), replace calls to this with Subscription#build_coupon_application, raise if !active_coupon_application.present
    def apply_coupon(coupon)
      if !active_coupon_application.present? && coupon.active?
        initial_subscription_values = if payments.any_paid?
          {
            initial_max_agents: max_agents_was,
            initial_plan_type: plan_type_was,
            initial_billing_cycle_type: billing_cycle_type_was,
            initial_full_monthly_price: pricing_model_was.monthly_undiscounted_price
          }
        else
          {
            initial_max_agents: 0,
            initial_plan_type: SubscriptionPlanType.Small,
            initial_billing_cycle_type: BillingCycleType.Monthly,
            initial_full_monthly_price: 0
          }
        end

        initial_subscription_values[:coupon] = coupon
        initial_subscription_values[:account] = account
        coupon_applications.build initial_subscription_values
      end
    end

    def apply_trial_coupon
      unless @trial_coupon.nil?
        apply_coupon(@trial_coupon).save!
      end
    end
  end
end
