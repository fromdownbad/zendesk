class Subscription
  module TrialManagement
    TRIAL_DURATION = 30.days
    ALTERNATIVE_TRIAL_DURATION = 14.days # 14-day trial test

    def self.included(base)
      base.class_eval do
        before_validation :remove_from_trial
        before_create :configure_trial
        before_update :propagate_trial_extension, if: :trial_expires_on?
      end
    end

    # Subscriptions are considered trial if we're within the trial expiration
    # period and the owner has not yet submitted a credit card.
    def computed_trial_status
      return false if account.try(:is_sandbox?) || registered_with_gateway? || managed_by_external_billing?
      new_record? || in_trial_period?
    end

    def remove_from_trial
      if credit_card.present? || managed_by_external_billing? || is_sponsored? || is_spoke?
        self.is_trial = false
      end
      true
    end

    def in_trial_period?
      if trial_expires_on.nil?
        Rails.logger.warn("Blank trial_expires_on for subscription #{id}: #{inspect}") # Debugging https://zendesk.hoptoadapp.com/errors/1834656/notices/321359575
        false
      else
        Date.today <= trial_expires_on.to_date
      end
    end

    def last_day_of_trial
      return manual_discount_expires_on if is_sponsored?
      return trial_expires_on if is_trial?
    end

    def configure_trial
      set_trial_expiry

      true
    end

    def set_trial_expiry
      set = false

      unless trial_coupon_code.blank?
        @trial_coupon = Coupon.find_by_coupon_code(trial_coupon_code)
        if @trial_coupon.nil?
          raise InvalidCouponException, "coupon #{trial_coupon_code} does not exist"
        else
          raise InvalidCouponException, "coupon #{@trial_coupon.name} may not be applied at signup" unless @trial_coupon.valid_for_signup?

          self.trial_expires_on = @trial_coupon.length_days.days.from_now
          self.is_trial = true
          self.base_agents = @trial_coupon.trial_max_agents
          self.plan_type = @trial_coupon.trial_plan_type
          set = true
        end
      end

      unless set
        if account.created_via_appsumo?
          self.trial_expires_on = 4.months.from_now.to_date
          self.base_agents = 3
          self.plan_type = SubscriptionPlanType.Small
        else
          self.trial_expires_on = default_trial_expiry
        end
        self.is_trial = true
      end
    end

    def default_trial_expiry
      if account.trial_extras_alternative_length?
        ALTERNATIVE_TRIAL_DURATION.from_now.to_date
      else
        TRIAL_DURATION.from_now.to_date
      end
    end

    def expired_trial?
      !account.is_serviceable? && crediting? && credit_card.nil?
    end

    # If the expiry date gets updated to a value in the future, wipe the current notification states
    def propagate_trial_extension
      if trial_expires_on_changed? && trial_expires_on_was && trial_expires_on > trial_expires_on_was && trial_expires_on > Date.today
        account.update_attribute(:is_serviceable, true) unless account.is_serviceable
        self.is_trial = true
        account.set_use_feature_framework_persistence(true)
      end
    end

    def expiration_date
      is_sponsored? ? manual_discount_expires_on : trial_expires_on
    end

    def days_left_in_trial
      seconds_left =  expiration_date.to_time - Time.now.end_of_day
      seconds_left >= 86400 ? (seconds_left / 86400).floor : 0
    end
  end
end
