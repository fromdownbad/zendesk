class Subscription
  module DelegatedFeatures
    # Delegate a bunch of method calls to the pricing model, for convenience.
    @@delegated_feature_bits = (
      Zendesk::Features::Catalogs::LegacyCatalog.features.map(&:name) +
      Zendesk::Features::Catalogs::Catalog.features.map(&:name)
    ).uniq

    @delegated_feature_groups = (
      Zendesk::Features::Catalogs::LegacyCatalog.groups +
      Zendesk::Features::Catalogs::Catalog.groups
    ).uniq

    DEPRECATED_FEATURES = %i[multibrand].freeze

    class << self
      def delegated_feature_bits
        @@delegated_feature_bits
      end

      attr_reader :delegated_feature_groups

      def define_feature_accessor(feature)
        return if DEPRECATED_FEATURES.include? feature

        define_method :"has_#{feature}?" do
          feature_present?(feature)
        end
      end

      def define_group_accessor(group)
        # Have to do the public_instance_methods check because respond_to?,
        # method_defined?, and friends don't work due to being in the eigenclass
        fail "Method '#has_#{group}?' is already defined" if public_instance_methods.include? :"has_#{group}?"
        define_method :"has_#{group}?" do
          group.feature_requirement_satisfied? do |feature|
            feature_present?(feature.name)
          end
        end
      end
    end

    @@delegated_feature_bits.each do |feature|
      define_feature_accessor(feature)
    end

    @delegated_feature_groups.each do |group|
      define_group_accessor(group)
    end

    def feature_value_from_subscription_features_table(feature)
      if account.read_feature_bits_from_db
        account.on_shard { features.send("#{feature}?") }
      else
        cached_db_read(feature)
      end
    end

    def pre_patagonia_pricing_model?
      [
        ZBC::Zendesk::PricingModelRevision::INITIAL,
        ZBC::Zendesk::PricingModelRevision::APRIL_2010,
        ZBC::Zendesk::PricingModelRevision::NEW_FORUMS,
        ZBC::Zendesk::PricingModelRevision::ENTERPRISE,
        ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE,
        ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE,
        ZBC::Zendesk::PricingModelRevision::TEMPORARY_PATAGONIA
      ].include?(subscription.pricing_model_revision)
    end

    def subscription
      account.subscription
    end

    def cached_db_read(feature)
      @db_features ||= Rails.cache.fetch(features_cache_key) do
        account.on_shard { features.to_a }.each_with_object({}) do |f, all|
          all[f.name.to_sym] = (f.value == '1')
        end
      end
      @db_features[feature]
    end

    def invalidate_features_cache
      Rails.cache.delete features_cache_key
      @db_features = nil
    end

    private

    def features_cache_key
      "#{cache_key}/features/all"
    end

    def feature_present?(feature)
      return true if bypass_feature_framework?(feature, account) || system_account?
      !!feature_value_from_subscription_features_table(feature)
    end

    def system_account?
      account.id == Account.system_account_id
    end

    def bypass_feature_framework?(feature, account)
      is_addon?(feature) &&
        account_in_beta?(feature, account) &&
        is_potential_addon_for?(feature, account)
    end

    def is_addon?(feature) # rubocop:disable Naming/PredicateName
      catalog_addons.include? feature
    end

    def catalog_addons
      @catalog_addons ||= (patagonia_catalog_addons + legacy_catalog_addons).uniq
    end

    def patagonia_catalog_addons
      Zendesk::Features::Catalogs::Catalog.addons.map(&:name)
    end

    def legacy_catalog_addons
      Zendesk::Features::Catalogs::LegacyCatalog.addons.map(&:name)
    end

    def is_potential_addon_for?(feature, account) # rubocop:disable Naming/PredicateName
      catalog   = Zendesk::Features::CatalogService.new(account).current_catalog
      plan_type = Zendesk::Features::PlanTypeService.new(account).proposed_plan_type
      catalog.addons_for_plan(plan_type).map(&:name).include?(feature.to_sym)
    end

    def account_in_beta?(feature, account)
      arturo_feature = Arturo::Feature.find_feature(feature.to_sym)
      return false if arturo_feature.nil?
      arturo_feature.external_beta_subdomains.include?(account.subdomain)
    end
  end
end
