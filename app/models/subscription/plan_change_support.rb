class Subscription
  module PlanChangeSupport
    def self.included(base)
      base.send(:attr_accessor, :update_from_monitor_pod)

      base.after_commit :propagate_subscription_settings, unless: :update_from_monitor_pod, on: :update
      base.after_update :deactivate_feature_boost!, if: :plan_type_changed?
    end

    # force_downgrade is true when we split the subscription update between the monitor-pod and the shard-pod
    def propagate_subscription_settings
      (suite? || spp?) ? downgrade_agents_for_suite! : downgrade_agents!
    end

    def deactivate_feature_boost!
      account.on_shard do
        return unless account.feature_boost.present?
        # NOTE: Generally speaking, when saving changes to a FeatureBoost,
        # code should call Zendesk::SupportAccounts::Product#save! or one of the
        # other helper functions.  But in this code, we're already in the context
        # of saving a Subscription, and all Subscription saves should happen
        # via Zendesk::SupportAccounts::Product.  If we were to call
        # Zendesk::SupportAccounts::Product#save! here, that'd be circular call.
        account.feature_boost.update_attribute(:is_active, false)
      end
    end

    def downgrade_agents!
      return if account(true).billable_agents.count <= max_agents

      agents_to_be_downgraded = account.agents_to_be_downgraded(max_agents)
      Rails.logger.info("Agent Downgrade: #{account_id}/#{account.subdomain} #{agents_to_be_downgraded.count} #{max_agents_was} -> #{max_agents}")
      agents_to_be_downgraded.each do |agent|
        downgrade_agent(agent, [:support])
      end
    end

    def downgrade_agents_for_suite!
      if account.has_ocp_unified_suite_downgrade?
        downgrade_agents_for_suite_unified!
      else
        (account.multiproduct? || account.spp?) ? downgrade_agents_for_suite_multiproduct! : downgrade_agents_for_suite_non_multiproduct!
      end
    end

    # This executes downgrade instructions specified in ADR at: https://github.com/zendesk/metropolis/blob/master/doc/architecture/decisions/0003-suite-user-downgrade.md
    def downgrade_agents_for_suite_unified!
      active_chat_staff = Zendesk::StaffClient.new(account).get_staff!(product: Zendesk::Accounts::Client::CHAT_PRODUCT).map { |staff| staff[:id] }
      active_support_staff = account.billable_agents.pluck(:id)
      active_suite_staff = (active_support_staff + active_chat_staff).uniq
      downgrade_count = active_suite_staff.count - max_agents
      return unless downgrade_count > 0
      active_suite_staff.delete(account.owner_id)
      downgrade_list = account.agents.order(created_at: :desc).limit(downgrade_count).find(active_suite_staff)
      downgrade_list.each { |agent| downgrade_agent(agent, [:support, :chat]) }
    end

    def downgrade_agents_for_suite_non_multiproduct!
      account.reload

      agents = OpenStruct.new(
        owners:                  account.users.where('id = ?', account.owner_id),
        chat_only:               account.chat_only_agents.reload.where('users.id != ?', account.owner_id).order('created_at desc'),
        non_chat_only_non_light: account.billable_agents.
                                   reload.
                                   where('users.id != ?', account.owner_id).
                                   order('created_at desc')
      )

      agent_counts = OpenStruct.new(
        owners:                  agents.owners.count,
        chat_only:               agents.chat_only.count,
        non_chat_only_non_light: agents.non_chat_only_non_light.count
      )

      total_agent_count = agent_counts.to_h.values.sum
      downgrade_count   = total_agent_count - max_agents

      return unless downgrade_count > 0

      # Remove as many Chat-only agents as needed/possible first
      chat_only_downgrade_count = [downgrade_count, agent_counts.chat_only].min
      chat_only_downgrades      = agents.chat_only.limit(chat_only_downgrade_count)

      chat_only_downgrades.each do |agent|
        downgrade_agent(agent, [:chat])
      end

      # Remove additional Support agents as necessary
      non_chat_only_non_light_downgrade_count = downgrade_count - chat_only_downgrade_count
      non_chat_only_non_light_downgrades      = agents.non_chat_only_non_light.limit(non_chat_only_non_light_downgrade_count)

      non_chat_only_non_light_downgrades.each { |agent| downgrade_agent(agent, [:support, :chat]) }
    end

    # see: https://github.com/zendesk/metropolis/blob/master/doc/architecture/decisions/0003-suite-user-downgrade.md
    # for more details on multiproduct suite agent downgrade logic/specification
    def downgrade_agents_for_suite_multiproduct!
      billable_chat_staff           = account.agents.select(&:cp4_chat_agent?)
      # track the ids so that we can use #uniq to de-dupe below
      billable_chat_staff_ids       = billable_chat_staff.map(&:id)
      billable_support_staff_ids    = account.billable_agents.map(&:id)
      billable_suite_staff_ids      = (billable_chat_staff_ids + billable_support_staff_ids).uniq
      downgradeable_suite_staff_ids = billable_suite_staff_ids - [account.owner.id] # never downgrade the account owner
      downgradeable_suite_staff     = User.where(id: downgradeable_suite_staff_ids).order(created_at: :desc)

      number_to_downgrade = billable_suite_staff_ids.count - max_agents
      return unless number_to_downgrade > 0

      number_successfully_downgraded = 0
      downgradeable_suite_staff.each do |agent|
        break if number_successfully_downgraded >= number_to_downgrade

        # always downgrade both Chat and Support for Suite agents
        downgrade_agent(agent, [:support, :chat])
        number_successfully_downgraded += 1
      end
    end

    private

    def downgrade_agent(agent, products)
      Zendesk::Users::AgentDowngrader.perform(agent: agent, products: products)
    end

    def suite?
      account.settings.reload.suite_subscription?
    end

    def spp?
      account.spp?
    end
  end
end
