class Subscription
  module SignupSupport
    def signup_as_trial?
      is_trial? || (crediting? && !registered_with_gateway? && !is_sponsored?)
    end
  end
end
