class Subscription
  module PatagoniaShims
    def patagonia_pricing_model?
      account.subscription.pricing_model_revision == ZBC::Zendesk::PricingModelRevision::PATAGONIA
    end
  end
end
