class Subscription
  module VoiceSupport
    def self.included(base)
      base.after_commit :suspend_sub_account, on: :update, if: :should_be_suspended?
      base.after_save :process_payment_method_conversions_for_voice_payments, if: :payment_method_type_changed?
    end

    def process_payment_method_conversions_for_voice_payments
      if account.settings.voice?
        if crediting?
          if pending_voice_payment = payments.pending.voice_payments.last
            Rails.logger.info("Destroying the pending voice payment #{pending_voice_payment} for account #{account.id} due to change to creditcard billing")
            pending_voice_payment.destroy
          end
        elsif invoicing?
          if postpaid_voice_payment = payments.voice_payments.postpay.last
            Rails.logger.info('Shorting the last post paid voice payment to end now due to change to manual billing')
            postpaid_voice_payment.update_attributes!(period_end_at: Time.now)
          end
        end
      end
    end

    private

    def suspend_sub_account
      if account.settings.voice? && should_be_suspended?
        account.suspend_sub_account
      end
    end

    def should_be_suspended?
      expired_trial? || paying_subscription_no_longer_active?
    end

    def paying_subscription_no_longer_active?
      ['customer', 'sponsored'].include?(account_type) && account_status != 'active'
    end
  end
end
