class Membership < ActiveRecord::Base
  include CachingObserver
  include PrecreatedAccountConversion
  include GroupMembershipObserver

  belongs_to :group, inherit: :account_id
  belongs_to :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  belongs_to :account

  attr_accessible :group, :group_id, :user, :user_id, :account, :default

  before_validation :set_account

  validates_presence_of :user, :group, :account_id
  validates_uniqueness_of :user_id, scope: :group_id

  validate :validate_account_on_group_and_user_match, if: proc { |membership| membership.user.present? && membership.group.present? }
  validate :validate_user_is_an_active_agent, if: proc { |membership| membership.user.try(:is_active) }

  after_save :default_group_updated, if: :set_as_default?
  after_destroy :default_group_updated, if: proc { |membership| membership.default }
  after_save :reset_default_groups
  after_save :update_user_cache_key
  after_commit :invalidate_phone_number_routing

  scope :active, ->(account_id) { joins(:group).where(groups: { is_active: true, account_id: account_id }) }

  def self.assignable(user)
    return none if user.is_end_user?

    scope = User.assignable(user.account)

    if user.agent_restriction?(:groups) && !user.can?(:assign_to_any_group, Ticket)
      scope = scope.where(memberships: { group_id: user.groups.pluck(:id) })
    end

    # Removing the `select` clause, which is been manually set
    # in `User::Roles` for `assignable_agents`. This will ensure
    # that we are selecting from `memberships` instead of `users`.
    joins(:user).merge(scope).except(:select)
  end

  private

  def validate_account_on_group_and_user_match
    errors.add(:user, 'must belong to the same account as the group') if user.account_id != group.account_id
    set_account
  end

  def set_account
    if user.present?
      self.account = user.account
    elsif group.present?
      self.account = group.account
    end
  end

  def validate_user_is_an_active_agent
    errors.add(:user, I18n.t('txt.error_message.models.membership.user_must_be_an_agent_to_be_in_a_group')) unless user.is_agent?
  end

  def reset_default_groups
    return unless default? && user.memberships.count > 1
    user.memberships.update_all_with_updated_at("`default` = 0", "id != #{id} AND `default` != 0")
  end

  def update_user_cache_key
    user.touch_without_callbacks
  end

  def invalidate_phone_number_routing
    Voice::Core::GroupRouterCache.invalidate(account) if account
  end

  def set_as_default?
    default_changed? && default
  end

  def default_group_updated
    user.dispatch_event_at_default_group_update(self)
  end
end
