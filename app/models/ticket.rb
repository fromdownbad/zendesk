require 'zendesk/tickets/merger'
require 'delta_changes'
require 'preload'
require 'zendesk/radar_factory'

module HashHandling
  def ticket_field_entries=(new_value)
    return if new_value.blank?

    if new_value.is_a?(Array) && new_value.all? { |el| el.is_a?(Hash) || el.is_a?(ActionController::Parameters) } # RAILS5UPGRADE
      fields_hash = {}
      new_value.each do |attributes|
        attributes = attributes.symbolize_keys
        fields_hash[attributes[:ticket_field_id]] = attributes[:value]
      end

      self.fields = fields_hash
    else # Let Rails handle it
      super new_value
    end
  end
end

class Ticket < ActiveRecord::Base
  include ScopedCacheKeys
  include TagManagement
  include ViaManagement
  include PrecreatedAccountConversion

  include DeltaChanges::Extension
  # Warning: Do not add a new field to delta_changes without careful
  # considerations. An additional field can result in a new row being written to
  # the events table every time a ticket is created or modified.
  delta_changes(
    columns: %w[
      assignee_id
      brand_id
      current_collaborators
      current_tags
      due_date
      external_id
      group_id
      is_public
      linked_id
      locale_id
      organization_id
      original_recipient_address
      priority_id
      requester_id
      satisfaction_comment
      satisfaction_reason_code
      satisfaction_score
      status_id
      subject
      ticket_form_id
      ticket_type_id
    ],
    attributes: %w[
      additional_tags
      satisfaction_comment
      satisfaction_probability
    ]
  )

  include Zendesk::Tickets::Properties
  include Zendesk::TicketFieldErrors
  include Zendesk::Serialization::TicketSerialization
  include Zendesk::Tickets::BusinessHours
  include Zendesk::Tickets::RequesterDataParser
  include Zendesk::Tickets::SetCollaborators
  include Zendesk::Tickets::SetCollaboratorsV2
  include Zendesk::Tickets::CollaborationSupport
  include Zendesk::Tickets::ProblemIncident
  include Zendesk::Tickets::TestTicket
  include Zendesk::Tickets::Facebook
  include Zendesk::Tickets::Channels
  include Zendesk::Tickets::Sms
  include Zendesk::Tickets::SoftDeletion
  extend  Zendesk::Tickets::ArchiveFinders
  extend  Zendesk::CappedCounts
  include CachingObserver
  include Zendesk::CreditCardRedaction
  include TdeWorkspace
  include Ticket::TicketFieldEntries
  include ArSkippedCallbackMetrics::Instrument

  prepend HashHandling

  credit_card_redaction_attrs :subject

  include CIA::Auditable
  audit_attribute :status_id, if: :auditable_ticket_action?
  after_soft_delete :record_deletion_audit
  after_destroy :record_hard_deletion_audit

  SERIALIZATION_FINDER_INCLUDES = {
    ticket_field_entries: {},
    linkings: {},
    comments: :attachments,
    incidents: {},
    account: {}
  }.freeze

  SERIALIZATION_FINDER_INCLUDES_V2 = SERIALIZATION_FINDER_INCLUDES.merge(
    requester: Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES,
    assignee: Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES,
    group: []
  )

  SORTABLE_FIELDS = {
    "created_at"     => "created_at",
    "updated_at"     => "updated_at",
    "status"         => "status_id",
    "id"             => "nice_id",
    "subject"        => "subject",
    "requester"      => "requester_id",
    "requester.name" => { join: "LEFT OUTER JOIN users ON tickets.requester_id = users.id", order: "users.name" },
    "group"          => "group_id",
    "assignee"       => "assignee_id",
    "assignee.name"  => { join: "LEFT OUTER JOIN users ON tickets.assignee_id = users.id", order: "users.name" },
    "locale"         => "locale_id"
  }.freeze

  STATUS_SQL_SORT_BY    = "FIELD(status_id, #{StatusType.order.join(',')})".freeze
  STATUS_SQL_SORT_ORDER = 'ASC'.freeze
  STATUS_SQL_ORDER      = "#{STATUS_SQL_SORT_BY} #{STATUS_SQL_SORT_ORDER}".freeze

  ACCESSIBLE_STATUSES =
    StatusType.fields - Zendesk::Types::StatusType::INACCESSIBLE_LIST

  OLDEST_EVENT_FIRST_ORDER = 'events.created_at ASC, events.id ASC'.freeze
  NEWEST_EVENT_FIRST_ORDER = 'events.created_at DESC, events.id DESC'.freeze
  TICKET_ID_AND_TYPE_INDEX = 'index_events_on_ticket_id_and_type'.freeze
  ACCOUNT_AND_STATUS_AND_UPDATED_INDEX = 'index_tickets_on_account_id_and_status_id_and_updated_at'.freeze
  ACCOUNT_AND_STATUS_AND_CREATED_INDEX = 'index_tickets_on_account_id_and_status_id_and_created_at'.freeze
  ACCOUNT_AND_NICE_ID_INDEX = 'index_tickets_on_account_id_and_nice_id'.freeze
  ACCOUNT_AND_STATUS_AND_LOCALE_INDEX = 'index_tickets_on_account_id_and_status_id_and_locale_id'.freeze

  NON_RICH_VIA_TYPES = [ViaType.TWITTER, ViaType.TWITTER_DM, ViaType.TWITTER_FAVORITE, ViaType.SMS, ViaType.ANY_CHANNEL, ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE].freeze

  belongs_to :account
  belongs_to :brand
  belongs_to :assignee, class_name: 'User', foreign_key: :assignee_id
  belongs_to :entry, inherit: :account_id
  belongs_to :group
  belongs_to :organization
  belongs_to :problem, class_name: 'Ticket', foreign_key: :linked_id
  belongs_to :requester, class_name: 'User', foreign_key: :requester_id
  belongs_to :submitter, class_name: 'User', foreign_key: :submitter_id
  belongs_to :translation_locale, foreign_key: 'locale_id'
  belongs_to :ticket_form, inherit: :account_id
  belongs_to :deleted_ticket_form, -> { where('ticket_forms.deleted_at IS NOT NULL') }, class_name: 'TicketForm', foreign_key: :ticket_form_id, inherit: :account_id

  has_one :latest_comment, -> { order(NEWEST_EVENT_FIRST_ORDER).use_index(:index_events_on_ticket_id_and_type) }, class_name: 'Comment'
  has_one :latest_public_comment, -> { where('events.is_public = 1').order(NEWEST_EVENT_FIRST_ORDER) }, class_name: 'Comment'
  has_one :first_comment, -> { order(OLDEST_EVENT_FIRST_ORDER).use_index(TICKET_ID_AND_TYPE_INDEX) }, class_name: 'Comment'
  has_one :ticket_metric_set, dependent: :destroy
  has_one :sla_ticket_policy, class_name: 'Sla::TicketPolicy', inverse_of: :ticket, inherit: :account_id, autosave: true, dependent: :destroy
  has_one :ticket_prediction, inverse_of: :ticket, inherit: :account_id, autosave: true
  has_one :ticket_deflection, inverse_of: :ticket, inherit: :account_id
  has_many :satisfaction_prediction_surveys, class_name: 'Satisfaction::PredictionSurvey', inherit: :account_id
  has_many :last_few_comments, -> { where('events.is_public = 1').order(NEWEST_EVENT_FIRST_ORDER).use_index(TICKET_ID_AND_TYPE_INDEX).limit(3).offset(1) }, class_name: 'Comment'
  has_many :audits, -> { where('events.parent_id IS NULL').order(OLDEST_EVENT_FIRST_ORDER).force_index(TICKET_ID_AND_TYPE_INDEX) }, inherit: :account_id, dependent: :destroy do
    def latest
      if archived?
        last
      else
        from_string = format("events FORCE INDEX(%<ticket_id_and_type_index>s)", ticket_id_and_type_index: TICKET_ID_AND_TYPE_INDEX)
        from(from_string).last
      end
    end
  end

  has_many :collaborations, inverse_of: :ticket, dependent: :destroy
  has_many :collaborators, -> { where(users: { is_active: true }).order('users.name').distinct }, source: :user, through: :collaborations
  has_many :comments, -> { order(OLDEST_EVENT_FIRST_ORDER) }
  has_many :recent_comments, -> { order(NEWEST_EVENT_FIRST_ORDER).limit(100) }, class_name: 'Comment'
  has_many :public_comments, -> { where(is_public: true).order(OLDEST_EVENT_FIRST_ORDER) }, class_name: 'Comment'
  has_many :conversation_items, -> { where(type: AuditEvent::CONVERSATION_EVENTS).order(OLDEST_EVENT_FIRST_ORDER) }, class_name: 'Event', inherit: :account_id

  has_many :events, -> { order(OLDEST_EVENT_FIRST_ORDER) }, inherit: :account_id
  has_many :redaction_events,
    -> { where(type: ['TicketFieldRedactEvent', 'CommentRedactionEvent', 'AttachmentRedactionEvent']).order(OLDEST_EVENT_FIRST_ORDER) },
    class_name: 'AuditEvent',
    inherit: :account_id
  has_many :incidents, class_name: 'Ticket', foreign_key: :linked_id # Tickets of type Problem can have linked incidents
  has_many :tweets, class_name: "Tweet"
  has_many :ticket_field_entries, dependent: :destroy, inherit: :account_id, inverse_of: :ticket

  has_many :linkings
  has_many :jira_issues, through: :linkings, source: :external_link, source_type: "JiraIssue"
  has_many :get_satisfaction_topics, through: :linkings, source: :external_link, source_type: "GetSatisfactionTopic"
  has_many :log_me_in_rescue_sessions, through: :linkings, source: :external_link, source_type: "LogMeInRescueSession"
  has_one :request_token, -> { where(type: 'RequestToken') }, foreign_key: 'source_id', dependent: :destroy

  has_one :unverified_creation, class_name: 'UnverifiedTicketCreation', inverse_of: :ticket, inherit: :account_id, dependent: :destroy, autosave: true

  # Ticket links
  ticket_link_options  = {class_name: 'TicketLink', dependent: :destroy}
  target_links_options = ticket_link_options.merge(foreign_key: :source_id, inherit: :account_id)
  source_links_options = ticket_link_options.merge(foreign_key: :target_id, inherit: :account_id)

  has_many :target_links, target_links_options
  has_many :source_links, source_links_options
  has_many :followup_target_links, -> { where(ticket_links: { link_type: 'Followup' }) }, target_links_options
  has_many :followup_source_links, -> { where(ticket_links: { link_type: 'Followup' }) }, source_links_options
  has_one :facebook_target_link, -> { where(ticket_links: { link_type: 'Facebook' }) }, target_links_options
  has_one :facebook_target, through: :facebook_target_link, source: :target, validate: false
  has_one :twitter_target_link, -> { where(ticket_links: { link_type: 'Twitter' }) }, target_links_options
  has_one :twitter_target, through: :twitter_target_link, source: :target, validate: false
  has_many :targets, through: :target_links
  has_many :sources, through: :source_links
  has_many :followups, through: :followup_target_links, source: :target, validate: false
  has_many :followup_sources, through: :followup_source_links, source: :source, validate: false
  has_many :shared_tickets, inverse_of: :ticket, inherit: :account_id
  has_many :rule_ticket_joins, class_name: "TicketJoin"
  has_many :rules, through: :rule_ticket_joins
  has_many :triggers, -> { where("rules.type = 'Trigger'") }, through: :rule_ticket_joins, source: :rule
  has_many :automations, -> { where("rules.type = 'Automation'") }, through: :rule_ticket_joins, source: :rule
  has_many :macros, -> { where("rules.type = 'Macro'") }, through: :rule_ticket_joins, source: :rule
  has_many :bookmarks, class_name: "Ticket::Bookmark", inherit: :account_id, dependent: :destroy
  has_many :activities, class_name: "TicketActivity", as: :activity_target, dependent: :destroy, inherit: :account_id
  has_many :inbound_emails, inherit: :account_id
  has_many :sdk_push_notification_subscriptions, class_name: "PushNotifications::SdkTicketSubscription"
  has_many :sdk_anonymous_device_identifiers, through: :sdk_push_notification_subscriptions, source: "device_identifier"
  has_many :metric_events, class_name: 'TicketMetric::Event', inverse_of: :ticket
  has_many :skips, dependent: :destroy
  has_many :chat_started_events, -> { order(:created_at) }, inherit: :account_id
  has_many :chat_transcripts, inherit: :account_id

  require_dependency "ticket/integrity"
  require_dependency "ticket/merging"
  require_dependency "ticket/scoring"
  require_dependency "ticket/validations"
  require_dependency "ticket/required_fields"
  require_dependency "ticket/id_masking"
  require_dependency "ticket/twicket"
  require_dependency "ticket/voice_tickets"
  require_dependency "ticket/ticket_satisfaction"
  require_dependency "ticket/suspension"
  require_dependency "ticket/sharing"
  require_dependency "ticket/crm"
  require_dependency "ticket/archive"
  require_dependency "ticket/flagging"
  require_dependency "ticket/collaborating"
  require_dependency "ticket/remote_files"

  scope :for_user, ->(user) { user ? where(user.ticket_conditions) : nil }
  scope :not_closed, ->(*_args) { where(status_id: (StatusType.fields - Zendesk::Types::StatusType::INOPERABLE_LIST)) }
  scope :working,    ->(*_args) { where(status_id: (StatusType.fields - Zendesk::Types::StatusType::FINISHED_LIST)) }
  scope :solved,     ->(*_args) { where(status_id: [StatusType.SOLVED, StatusType.CLOSED]) }
  scope :solved_since, ->(datetime) { where(['solved_at > ?', datetime]) }
  scope :deleted_and_not_scrubbed_since, ->(datetime) { where(["status_id = ? AND (subject != ? OR subject IS NULL) AND updated_at > ?", StatusType.DELETED, Zendesk::Scrub::SCRUBBED_SUBJECT, datetime]) }
  scope :deleted_and_not_scrubbed_before, ->(datetime) { where(["status_id = ? AND (subject != ? OR subject IS NULL) AND updated_at < ?", StatusType.DELETED, Zendesk::Scrub::SCRUBBED_SUBJECT, datetime]) }

  scope :with_best_index, ->(order) do
    if order.to_s == "created_at" || order.to_s == "status"
      use_index(ACCOUNT_AND_STATUS_AND_CREATED_INDEX)
    elsif order.to_s == "updated_at"
      use_index(ACCOUNT_AND_STATUS_AND_UPDATED_INDEX)
    elsif order.to_s == "id"
      use_index(ACCOUNT_AND_NICE_ID_INDEX)
    elsif order.to_s == "locale"
      use_index(ACCOUNT_AND_STATUS_AND_LOCALE_INDEX)
    else # no index to force
      all
    end
  end

  attr_accessible :account,
    :additional_collaborators,
    :additional_collaborators_with_email_cc_type,
    :additional_collaborators_with_follower_type,
    :additional_tags,
    :agreement_id,
    :assignee,
    :assignee_email,
    :assignee_id,
    :brand,
    :brand_id,
    :client,
    :collaborator_ids,
    :comment,
    :created_at,
    :current_collaborators,
    :custom_id_override,
    :"due_date(1i)",
    :"due_date(2i)",
    :"due_date(3i)",
    :description,
    :due_date,
    :entry_id,
    :external_id,
    :external_links,
    :fields,
    :force_status_change,
    :group,
    :group_id,
    :linked_id,
    :locale_id,
    :metadata,
    :monitored_twitter_handle_id,
    :organization,
    :organization_id,
    :original_recipient_address,
    :priority_id,
    :problem_id,
    :recipient,
    :remove_tags,
    :requester,
    :requester_data,
    :requester_email,
    :requester_id,
    :requester_name,
    :safe_update_timestamp,
    :satisfaction_comment,
    :satisfaction_reason_code,
    :satisfaction_score,
    :screencasts,
    :set_collaborators,
    :set_collaborators_with_email_cc_type,
    :set_collaborators_with_follower_type,
    :set_email_ccs,
    :set_followers,
    :set_tags,
    :sharing_agreement_custom_fields,
    :skip_dm,
    :solved,
    :status_id,
    :subject,
    :submitter,
    :submitter_id,
    :tags,
    :ticket_field_entries,
    :ticket_form_id,
    :ticket_type_id,
    :twitter_status_message_id,
    :unshare_ticket,
    :via_followup_source_id,
    :via_id

  attr_accessor :agent_attributes_match,
    :agreement_id,
    :assignee_email,
    :audit,
    :audit_message,
    :client,
    :comment,
    :current_user,
    :current_via_id,
    :current_via_reference_id,
    :custom_id_override,
    :disable_answer_bot_trigger,
    :disable_notifications,
    :disable_triggers,
    :external_changes,
    :force_collaboration,
    :force_dm,
    :force_status_change,
    :last_executed_rules,
    :latest_comment_mapped,
    :latest_chat_message_mapped,
    :macros_to_be_applied_after_save,
    :monitored_twitter_handle_id,
    :new_sla_policy_id,
    :original_via_id, # will currently only exist at creation time from email or /api/v2/requests
    :override_recipient_address,
    :recipients_for_latest_mail,
    :requester_email,
    :requester_name,
    :screencasts, # Used for suspended_ticket and /requests API
    :sharing_agreement_custom_fields,
    :skip_dm,
    :skip_reshare_ticket_if_necessary,
    :solved,
    :ticket_field_entries_by_ticket_field_id_cache,
    :to_and_ccs_for_latest_mail,
    :twitter_status_message_id,
    :unshare_ticket,
    :via_followup_source_id # Pass nice ID of the closed ticket if you are creating a followup

  attr_writer :attribute_maps,
    :application_logger,
    :macro_ids

  attr_reader :requester_data,
    :safe_update_timestamp

  ignore_column :latest_recipients

  after_initialize { |ticket| ticket.attribute_maps = [] }
  after_initialize { |ticket| ticket.macro_ids = [] }

  # Note - sequence is very important here
  before_validation :redact_credit_card_numbers!
  before_validation :set_defaults, if: :new_record?
  before_validation :auto_tag, if: :new_record?
  before_validation :set_assignee_via_email_address, if: proc { |ticket| ticket.assignee_email.present? }
  before_validation :set_assignee_if_account_has_only_one_agent
  before_validation :group_id_is_valid, if: proc { |ticket| ticket.group_id_changed? && ticket.group_id }
  before_validation :set_group_if_account_has_only_one_group
  before_validation :set_group_if_default_group_present
  before_validation :fix_organization
  before_validation :set_requester_tags
  before_validation :set_organization_group
  before_validation :set_assignee_if_group_has_only_one_agent
  before_validation :set_assignee_upon_solve
  before_validation :enforce_proper_assignee_group_relationship
  before_validation :set_comment
  before_validation :fix_tags
  before_validation :create_ticket_sharing_event
  before_validation :fix_due_date
  before_validation :open_new_ticket_upon_assignment
  before_validation :open_working_ticket_upon_end_user_update, on: :update
  before_validation :ensure_new_assignee_can_be_assigned, on: :update
  before_validation :check_ticket_form_id, if: proc { |ticket| ticket.ticket_form_id_changed? && ticket.ticket_form_id }
  before_validation :check_brand_id, if: proc { |ticket| ticket.brand_id_changed? && ticket.brand_id }
  before_validation :set_default_brand, if: proc { |ticket| ticket.new_record? && ticket.brand_id.nil? }
  before_validation :set_ticket_form, if: proc { |ticket| ticket.account.has_ticket_form_default? && ticket.new_record? && ticket.ticket_form_id.nil? }
  before_validation :check_safe_update_timestamp, if: :safe_update_timestamp

  truncate_attributes_to_limit :subject, :description
  validates_numericality_of :organization_id, allow_nil: true, greater_than: 0
  validates_numericality_of :group_id, allow_nil: true, greater_than: 0
  validates_presence_of :requester, :priority_id, :status_id
  validate :validate_external_id_length
  validate :block_inbound_spam_create, on: :create
  validate :check_assignee_belongs_to_group, if: proc { |ticket| ticket.assignee_id.present? }
  validate :validate_email_only_reply
  validate :validate_via_id
  validate :validate_requester_is_valid

  before_save :assign_attribute_values, if: :map_attributes?
  before_save :save_updated_requester
  before_save :evaluate_privacy
  before_save :ensure_audit_present
  before_save :throttle_updates
  before_save :cleanup_attributes
  before_save :transform_collaborations # must occur before add_ticket_events_to_audit
  before_save :persist_new_email_cc_users # must occur before run_triggers_on_audit
  before_save :add_ticket_events_to_audit
  before_save :run_triggers_on_audit
  before_save :ensure_solved_at, if: :solved?
  before_save :set_report_attributes
  before_save :handle_unshare_request
  before_save :copy_screencasts_into_audit_metadata
  before_save :save_updated_ticket_fields

  before_save :add_audit_as_association
  before_save :fix_overflowing_tags
  before_save :add_queued_errors
  before_save :record_macro_use_by_agent
  before_save :touch_when_manually_set_attribute_values

  around_save :add_ccs_changed_domain_events, if: proc { |ticket| ticket.account.has_publish_cc_events_to_bus? }

  after_save :record_applied_macros
  after_save :save_executed_rule_stats
  after_save :persist_collaborators
  after_save :notify_followers
  after_save :save_manually_set_attribute_values

  after_commit :cleanup_instance_variables
  after_commit :apply_after_save_macros
  after_commit :apply_ticket_deflection
  after_commit :log_ticket_creation, on: :create
  after_commit :notify_radar, on: :update, unless: :skip_notify_radar?
  # A ticket may get touched during the lifetime of a request, despite a failed save operation
  # The presence of previous_changes means that radar has been notified already
  after_touch :notify_radar, if: -> { previous_changes.empty? }
  after_commit :copy_locale_id_from_requester
  after_commit :save_mapped_attribute_values

  after_rollback :send_spam_ticket_to_suspended_queue, on: :create

  # Disable notifications for the pre-created accounts.
  # This will be called only once, and only disabled in memory
  # https://github.com/zendesk/zendesk/blob/v3.49.0/app/models/concerns/precreated_account_conversion.rb#L27
  before_account_convert :disable_audit

  include TicketActivityObserver
  include TicketEventBusObserver
  include TicketMetricsObserver
  include TicketStatsObserver
  include TicketSlaObserver
  include ViewsObservers::TicketObserver
  include SdkTicketActivityObserver
  include Access::Validations::TicketExtensions
  attribute_permission_validator Access::Validations::TicketValidator
  attr_enterprise :set_tags, :additional_tags, :remove_tags, :assignee_id

  def locale_id
    requester && !new_record? ? requester.locale_id : super
  end

  def translation_locale
    requester && !new_record? ? requester.translation_locale : super
  end

  def to_param
    nice_id.to_s
  end

  def problem_id
    problem.try(:nice_id)
  end

  def problem_id=(nice_id)
    self.linked_id = account.tickets.find_by_nice_id(nice_id).try(:id)
  end

  def group_includes?(member)
    group.present? && group.users.include?(member)
  end

  def title(truncate_to = nil, force_description = false)
    value = (force_description || subject.blank?) ? visible_description : subject
    value.to_s.truncate(truncate_to || 30).gsub(/\s/, " ").squish
  end

  def title_for_role(is_agent, truncate_to = nil)
    if is_private? && !is_agent
      Rails.logger.warn("A non-agent can't view a title for a private ticket, account: #{account.subdomain}, ticket #{id}:#{caller.join("\n")}")
      return "Untitled ticket #{nice_id}"
    end

    title(truncate_to, !is_agent && !account.field_subject.is_visible_in_portal?)
  end

  def satisfaction_probability
    @satisfaction_probability ||= TicketPrediction.current_satisfaction_probability(self)
  end

  def satisfaction_probability=(new_probability)
    @satisfaction_probability = TicketPrediction.update_satisfaction_probability(self, new_probability)
  end

  # Whether the ticket was requested by some user.
  #
  # user - the User that could be the requester.
  #
  # Returns true if `user` is the requester, false otherwise.
  def requested_by?(user)
    requester.present? && requester == user
  end

  # Whether the ticket was submitted directly by the requester.
  #
  # If no submitter is set it is assumed that the requester will submit the ticket,
  # as that is the default behavior.
  #
  # Returns true if the ticket's submitter is also its requester or if no submitter
  #   is set, false otherwise.
  def submitted_by_requester?
    submitter.nil? || requester == submitter
  end

  # Check if the submitter is an agent if there is no requester.
  #
  # Part of an effort to address problems with certain endpoints
  #  https://support.zendesk.com/agent/tickets/1131700
  def requester_is_agent?
    (requester || submitter).try(:is_agent?)
  end

  def status
    StatusType.to_s(status_id)
  end

  def status?(value)
    status_id == StatusType.fuzzy_find(value)
  end

  def pending?
    StatusType.PENDING == status_id
  end

  def on_hold?
    StatusType.HOLD == status_id
  end

  def closed?
    StatusType.CLOSED == status_id
  end

  def solved?
    [StatusType.SOLVED, StatusType.CLOSED].include?(status_id)
  end

  def scrubbed?
    subject == Zendesk::Scrub::SCRUBBED_SUBJECT && status_id == StatusType.DELETED
  end

  def solve(user)
    will_be_saved_by(user)
    update_attribute :status_id, StatusType.SOLVED
  end

  def finished?
    StatusType::FINISHED_LIST.include?(status_id)
  end

  def is_private? # rubocop:disable Naming/PredicateName
    !is_public?
  end

  def working?
    !finished?
  end

  def inoperable?
    StatusType::INOPERABLE_LIST.include?(status_id)
  end

  def operable?
    !inoperable?
  end

  def working_was?
    return nil unless status_id_changed?

    !StatusType::FINISHED_LIST.include?(status_id_was)
  end

  def working_changed?
    status_id_changed? && (working? != working_was?)
  end

  def can_be_updated?
    !StatusType::INOPERABLE_LIST.include?(status_id_was)
  end

  def ever_solved?
    solved? || (ticket_metric_set && ticket_metric_set.reopens > 0)
  end

  def deleted?
    status?(:deleted)
  end

  def priority
    PriorityType.to_s(priority_id)
  end

  def priority?(value)
    priority_id == PriorityType.fuzzy_find(value)
  end

  def req_name
    read_attribute(:req_name) || requester.try(:name)
  end

  def ticket_type
    return 'Ticket' if !ticket_type_id || ticket_type_id == 0 || !id # || !account.has_extended_ticket_types?
    TicketType.to_s(ticket_type_id)
  end

  def ticket_type_id=(value)
    super

    self.due_date = nil if value != TicketType.TASK
  end

  def ticket_type?(value)
    ticket_type_id == TicketType.fuzzy_find(value)
  end

  def extended_ticket_types?
    ticket_type_id.present? && account.has_extended_ticket_types?
  end

  def updated_by_type
    updated_by_type_id ? UpdatedByType.to_s(updated_by_type_id) : 'N/A'
  end

  def url_path(options = {})
    return '-' unless id

    "/#{Zendesk::Tickets::UrlBuilder.new(options.merge(account: account)).path_for(self, options)}"
  end

  def url(options = {})
    return '-' unless id

    Zendesk::Tickets::UrlBuilder.new(options.merge(account: account)).url_for(self, options)
  end

  def requester_name_for_display
    return requester.email_address_with_name_without_quotes unless requester.try(:email_address_with_name_without_quotes).nil?
    return requester.name if requester
    requester_name
  end

  # Ticket sharing depends on the behavior of this method and the
  # add_comment method below.  Do not change these, without considering
  # how it effects Ticket sharing.
  def description=(value)
    return description unless new_record?
    return description unless value

    write_attribute('description', value)

    self.comment    ||= add_comment(is_public: true)
    self.comment.body = value
  end

  def public_description(options = {})
    body_method = options[:markdown] ? :body : :html_body
    if archived?
      public_comments.sort_by(&:created_at).first.try(body_method).to_s
    else
      public_comments.use_index('index_events_on_ticket_id_and_type').first.try(body_method).to_s
    end
  end

  def visible_description
    is_private? ? description : public_description(markdown: true)
  end

  private :visible_description

  def description
    if account.has_views_ticket_description_fix? && via?(:closed_ticket) && first_comment.present?
      first_comment.body unless @description_filter
    else
      read_attribute('description') unless @description_filter
    end
  end

  def filter_description
    @description_filter = !first_comment.try(:is_public?)
  end

  def hide_private_comments!
    association(:comments).target = comments.select(&:is_public?)
  end

  def add_comment(options)
    self.comment = Comment.new({ticket: self, account: account}.merge(options))
  end

  def add_voice_api_comment(options)
    self.comment = VoiceApiComment.new({ticket: self, account: account}.merge(data: options.symbolize_keys))
  end

  def agent_comments
    comments.joins(:author).where("`users`.`roles` in (?)", User::AGENT_ROLES)
  end

  def latest_agent_comment
    agent_comments.last
  end

  def add_sharing_event(id, custom_fields)
    unless audit
      raise "You are trying to make changes to the ticket that needs to be recorded in an Audit. You must call #will_be_saved_by beforehand."
    end

    account_agreement_ids = account.sharing_agreements.map(&:id)
    agreement_ids = shared_tickets.map(&:agreement_id)

    id = id.to_i

    return unless account_agreement_ids.include?(id) && !agreement_ids.include?(id)

    new_event = TicketSharingEvent.new(agreement_id: id, custom_fields: custom_fields)
    new_event.account_id = account&.id
    audit.events << new_event
  end

  def create_ticket_sharing_event
    return unless agreement_id.present?
    add_sharing_event(agreement_id, sharing_agreement_custom_fields)
  end

  def handle_unshare_request
    return unless unshare_ticket == '1'

    unshare(shared_tickets)
  end

  def unshare(shared_tickets = [])
    shared_tickets.each do |shared_ticket|
      @audit.events << TicketUnshareEvent.new(agreement_id: shared_ticket.agreement_id)
    end
  end

  def redactions?
    redaction_events.exists?
  end

  # This method is only used in Api::V2::TicketsController#redact
  # This method can string redact or fully redact open, closed and archived tickets
  # Comment value and a matching ticket description will be fully redacted iff text is nil and redact_full_comment is true
  def redact_comment!(user, comment, text, redact_full_comment = false)
    # Return 400 on valid calls made by non-arturo'd accounts
    return false if text.nil? && !account.has_redact_archived_and_closed_comments?

    if text.nil? && !redact_full_comment
      raise ArgumentError, "text cannot be nil while redact_full_comment is false.  You must set redact_full_comment flag to true in order to scrub the full comment."
    end

    if archived? && account.has_redact_archived_and_closed_comments?
      return redact_archived_comment!(comment, text, redact_full_comment)
    end

    success = false
    will_be_saved_by(user)

    # .description= and .description are overwritten
    if redact_full_comment
      # Redact ticket description iff it matches the target comment's value
      if comment.value.eql?(read_attribute(:description))
        redacted_ticket_description = Zendesk::TextRedaction.redact_part_or_all!(read_attribute(:description), text, redact_full_comment)
        write_attribute :description, redacted_ticket_description
      end
    elsif !text.nil? && redacted = Zendesk::TextRedaction.redact(read_attribute(:description), text)
      write_attribute :description, redacted
    end

    # Redact comment value
    success = true if comment.redact!(text, redact_full_comment)

    if closed? && account.has_redact_archived_and_closed_comments?
      # Use update_column to redact a closed ticket without calling save! or creating an event
      update_column(:description, description) if success
    else
      audit.events << CommentRedactionEvent.new(comment_id: comment.id)
      add_domain_event(CommentRedactedProtobufEncoder.new(comment).to_safe_object)
      save! if success
    end
  end

  def omniredact_comment!(user, comment, text, external_attachment_urls = [])
    return false unless account.has_agent_workspace_omni_redaction?

    will_be_saved_by(user)
    if comment.omniredact!(text, user, audit, external_attachment_urls)
      add_domain_event(CommentRedactedProtobufEncoder.new(comment).to_safe_object)
      save!
    end
  end

  def fields=(field_attributes)
    return if field_attributes.blank?

    field_attributes = Array(field_attributes)
    field_ids = field_attributes.map { |attr| attr.first.to_i }

    ticket_fields = account.
      ticket_fields(include_voice_insights: true).
      active.
      where(id: field_ids)

    taggers = ticket_fields.select { |f| f.is_a?(FieldTagger) }
    taggers.pre_load(:custom_field_options) unless account.has_cache_custom_field_option_values?

    field_attributes.each do |id, value|
      if field = ticket_fields.detect { |ticket_field| id.to_i == ticket_field.id }
        set_ticket_field_entry(field, value)
      end
    end
  end

  # Invalidate update_all
  def update_all
    raise "Ticket doesn't support update_all due to the audit system"
  end

  def save(*args)
    # We set the nice_id here as sequence.next locks the sequence table
    # and we do not want that locked for the entire ticket save transaction
    set_nice_id if new_record?

    if account.has_instrument_ticket_save?
      save_output = nil

      ms = Benchmark.ms { save_output = super }
      rack_request_span&.set_tag('zendesk.ticket.save.time', ms)

      save_output
    else
      super
    end
  end

  def save!(*args)
    # We set the nice_id here as sequence.next locks the sequence table
    # and we do not want that locked for the entire ticket save transaction
    set_nice_id if new_record?

    if account.has_instrument_ticket_save?
      save_output = nil

      ms = Benchmark.ms { save_output = super }
      rack_request_span&.set_tag('zendesk.ticket.save_bang.time', ms)

      save_output
    else
      super
    end
  end

  def validate!
    valid? || raise(ActiveRecord::RecordInvalid, self)
  end

  def create_silent_change_event(event)
    raise "use will_be_saved_by" unless audit
    audit.events << event
    audit.disable_triggers = true
    audit.save!

    # Update ticket without running validations and callbacks
    # bump updated-at to ensure ticket actually gets saved -- oftentimes there's no other changes
    self.updated_at = Time.now
    save(validate: false)
  end

  def will_be_saved_by(user, options = {})
    @current_user = user || @current_user || submitter
    return false unless @current_user

    @current_via_id = options[:via_id] || @current_via_id || via_id || ViaType.WEB_SERVICE
    @current_via_reference_id = options[:via_reference_id] || @current_via_reference_id || via_reference_id

    @audit ||= Audit.new(
      author: @current_user,
      via_id: @current_via_id,
      via_reference_id: @current_via_reference_id,
      account: @current_user.is_system_user? ? account : @current_user.account,
      client: client, # client info - e.g. IP address, browser/OS data
      ticket: self,
      disable_triggers: disable_triggers
    )

    @audit.flag!(new_audit_flags, new_audit_flags_options)
    true
  end

  def via_id=(via)
    self.current_via_id = via
    write_attribute(:via_id, via)
  end

  def via_reference_id=(via_reference)
    self.current_via_reference_id = via_reference
    write_attribute(:via_reference_id, via_reference)
  end

  def original_recipient_address=(address)
    write_attribute(:original_recipient_address, Zendesk::Mail::Address.sanitize(address))
  end

  def recipient_address
    override_recipient_address || account.recipient_addresses.by_email(original_recipient_address).first
  end

  # CIA uses the audit_message value to magically record a message on the audit
  def record_subject_change_audit
    self.audit_message = subject_was
    CIA.record(:subject_change, self)
  end

  def record_macro_use_by_agent
    return unless audit.present?
    macro_ids.each do |macro_id|
      if account.has_email_blank_macro_reference? && macro_id.blank?
        logger.info("Not creating an AgentMacroReference AuditEvent because macro_id is blank. account_id: #{account_id}, ticket_id: #{id}")
      else
        audit.events << macro_reference(macro_id)
      end
    end
  end

  def record_applied_macros
    return if account.all_macros.where(id: macro_ids).blank?

    record_rule_usage(macro_ids)

    Zendesk::RuleSelection::MacroUsage.new(current_user).record_many(macro_ids)

    macro_ids.each do |macro_id|
      Zendesk::ClassicStats::RuleUsage.store(
        account_id: account_id,
        shard_id:   account.shard_id,
        ticket_id:  id,
        user_id:    current_user.id,
        rule_id:    macro_id,
        rule_type:  'macro'
      )
    end
  end

  def save_executed_rule_stats
    return unless last_executed_rules.present?

    last_executed_rules.each do |executed_rule|
      Zendesk::ClassicStats::RuleUsage.store(
        account_id: account_id,
        shard_id:   account.shard_id,
        ticket_id:  id,
        user_id:    executed_rule[:user_id],
        rule_id:    executed_rule[:rule_id],
        rule_type:  executed_rule[:rule_type]
      )
    end

    if account.has_rule_usage_stats?
      rule_usage = Zendesk::RuleSelection::RuleUsage.new(account)

      last_executed_rules.
        group_by { |executed_rule| executed_rule[:rule_type].downcase.to_sym }.
        each do |rule_type, executed_rules|
        rule_usage.record_many(
          type:     rule_type,
          rule_ids: executed_rules.map do |executed_rule|
            executed_rule[:rule_id]
          end
        )
      end
    end

    self.last_executed_rules = []
  end

  def external_links=(array_of_links_attributes)
    return unless new_record?

    array_of_links_attributes.each do |link_attributes|
      ExternalLink.build(self, link_attributes)
    end
  end

  def mark_as_unverified(identity)
    build_unverified_creation(
      account: identity.account,
      user: identity.user,
      user_identity: identity,
      from_address: identity.value
    )
  end

  def channel
    if twitter_ticket_source.present?
      { type: "Twitter", monitored_twitter_account: twitter_ticket_source.mention_name }
    end
  end

  def allow_channelback?
    decoration = latest_public_comment.try(:event_decoration)
    decoration_data = if decoration.nil? || decoration.data.nil?
      Hashie::Mash.new
    else
      decoration.data
    end
    interpreter = ::Channels::DecorationDataInterpreter.new(decoration_data)
    interpreter.allow_channelback?
  end

  def followup_source
    @followup_source ||= begin
      t_source = source_links.first
      t_source && t_source.account.tickets.find_by_id(t_source.source_id)
    end
  end

  def set_followup_source(current_user) # rubocop:disable Naming/AccessorMethodName
    return unless via_followup_source_id.present?

    source_ticket = current_user.
      account.
      closed_tickets.
      for_user(current_user).
      find_by_nice_id(via_followup_source_id.to_i)

    if source_ticket
      set_followup_source_ticket(source_ticket)
      return
    end

    self.via_followup_source_id = nil
  end

  def set_followup_source_ticket(source_ticket) # rubocop:disable Naming/AccessorMethodName
    build_followup_from(source_ticket)
    @followup_source = source_ticket
    # followup_sources << source_ticket does not create a valid TicketLink on rails 3.0
    source_links << TicketLink.new(source: source_ticket, target: self, link_type: TicketLink::FOLLOWUP)
  end

  def followups_with_archived
    @followups_with_archived ||= begin
      ticket_ids = followup_target_links.map(&:target_id)
      if ticket_ids.any?
        account.tickets.where(id: ticket_ids).all_with_archived
      else
        []
      end
    end
  end

  def set_comment
    return if !comment || !@current_user || !@audit

    # make sure the comment account is set so attachments can be created
    comment.account = account
    # comment author for followup tickets is not always requester
    comment.author ||= (new_record? && via_id != ViaType.CLOSED_TICKET ? requester : @current_user)
    # do not coerce the comment to private but rather throw an error on save
    # comment.is_public = false if !new_record? && !comment.author.can?(:publicly, Comment)

    unless @audit.events.include?(comment) # Do not add the comment to the audit, if it's already been added
      comment.audit = @audit
      if !comment.empty? || comment.attachments.any?
        @audit.events << comment
        comment.redact_credit_card_numbers!
        if Arturo.feature_enabled_for?(:reply_options, account)
          handle_reply_options
        else
          create_channel_back_event
          create_sms_response if should_create_sms_response?
        end
      elsif comment.is_a?(VoiceApiComment)
        @audit.events << comment
      end
    end
  end

  # only call in case of emergencies.  Like the fire department.
  def force_reopen!(new_status = StatusType.OPEN)
    return unless closed?
    begin
      @force_reopen = true
      self.status_id = new_status
      save!
    ensure
      @force_reopen = nil
    end
  end

  def allows_comment_visibility_toggle?
    shared_tickets.all?(&:allows_comment_visibility_toggle?)
  end

  def mark_as_spam_by!(user)
    return unless user.can?(:edit, requester) || (user.is_agent? && account.has_disable_suspending_email_spam_requesters?)
    return if requester.represents_facebook_page? || user == requester
    will_be_saved_by(user)
    self.class.transaction do
      user.account.inbound_emails.find_by_ticket_id(id).try(:mark_as_spam!)
      if original_raw_email_identifier && !Rails.env.development?
        CloudmarkFeedbackJob.report_spam(user, original_raw_email_identifier)
        RspamdFeedbackJob.report_spam(user: user, identifier: original_raw_email_identifier, recipient: recipient) if account.has_email_rspamd_training_enabled?
      end

      # Ignore cases when Zendesk marks tickets as spam on behalf of customers using the system user
      if account.has_email_record_spam_marking_reply_stats? && current_user != User.system
        record_mark_as_spam_with_reply_comments_metrics
      end

      add_domain_event(TicketMarkedAsSpamProtobufEncoder.new(self).to_safe_object)

      # rubocop:disable Style/IdenticalConditionalBranches
      if account.has_disable_suspending_email_spam_requesters?
        soft_delete!
      else
        soft_delete!
        requester.suspend! unless requester.is_agent?
      end
      # rubocop:enable Style/IdenticalConditionalBranches
    end
  end

  def requester_data=(arg)
    @requester_data = HashParam.ish?(arg) ? arg : parse_requester_data(arg)
    set_requester
    @requester_data
  end

  def has_incidents # rubocop:disable Naming/PredicateName
    problem? && count_incidents > 0
  end

  def count_incidents
    # FIXME
    # count_with_archived doesn't do the
    # right thing upon new ticket records,
    # this is why we return 0.
    return 0 if new_record?

    incidents.count_with_archived
  end

  def shared_with_jira
    shared_tickets.detect { |st| st.safe_agreement.try(:shared_with_id) == Sharing::Agreement::SHARED_WITH_MAP[:jira] }
  end

  def inject_screencasts_in_metadata(screencasts)
    # There must be an audit hence you must call #will_be_saved_by before this method
    audit.metadata[:system].merge!('screencasts' => screencasts) if audit.present?
  end

  def add_translatable_error_event(event_options, enforce_unique = true)
    unless enforce_unique && events.detect { |e| e.is_a?(TranslatableError) && e.value == event_options }
      if audit
        audit.events << TranslatableError.new(ticket: self, value: event_options)
      else
        queued_errors << event_options
      end
    end
  end

  def add_external_change(xc)
    self.external_changes ||= []
    self.external_changes << xc
  end

  def domain_events
    @domain_events ||= []
  end

  # Adds an _explicit_ domain event to be included by the TicketEventsProtobufEncoder
  def add_domain_event(domain_event)
    return if domain_event.nil?
    raise ArgumentError, "Expected TicketEvent, received #{domain_event.class}" unless domain_event.is_a?(::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent)
    domain_events << domain_event
  end

  # Rules still need access to attributes for outputting fields that aren't on the tickets table
  def add_virtual_attribute(attr_name, value)
    # This mimics what ActiveRecord does when using select('xxx.yyy as zzz')
    attributes = @attributes.instance_variable_get(:@attributes)
    attributes[attr_name] = ActiveRecord::Attribute.from_database(attr_name, value, ActiveRecord::Type::Value.new)
  end

  def comment_added?
    comment.present?
  end

  def priority_escalation?
    [PriorityType.HIGH, PriorityType.URGENT].member?(priority_id) && (!priority_id_was || priority_id > priority_id_was)
  end

  def public_updated_at
    events.where(is_public: true).maximum(:updated_at)
  end

  def token
    request_token.try(:value)
  end

  def initial_audit
    audits.first || @audit
  end

  def sla_next_breach_at
    sla.next_breach_at
  end

  def sla
    Zendesk::Sla::TicketStatus.new(self)
  end

  def current_ticket_form
    @current_ticket_form ||= begin
      return nil unless account.has_ticket_forms?

      ticket_form || account.ticket_forms.default.first
    end
  end

  def deleted_ticket_form
    TicketForm.with_deleted { super }
  end

  # Add this because update_many job would pass in a string type timestamp here
  def safe_update_timestamp=(timestamp)
    @safe_update_timestamp = timestamp.is_a?(Time) ? timestamp : timestamp.to_time
  end

  def queued_errors
    @queued_errors ||= []
  end

  def deflect_with_rule(rule_id)
    @deflection_rule_id = rule_id
  end

  def rich?
    NON_RICH_VIA_TYPES.exclude?(via_id)
  end

  def add_knowledge_event(event_type, article_params, user, template_params = nil)
    return unless event_type && article_params && user

    knowledge_event =
      case event_type
      when 'KnowledgeLinked'
        KnowledgeLinked.new(article: article_params)
      when 'KnowledgeCaptured'
        KnowledgeCaptured.new(article: article_params, template: template_params)
      when 'KnowledgeFlagged'
        KnowledgeFlagged.new(article: article_params)
      when 'KnowledgeLinkAccepted'
        KnowledgeLinkAccepted.new(article: article_params)
      when 'KnowledgeLinkRejected'
        KnowledgeLinkRejected.new(article: article_params)
      end

    if knowledge_event
      will_be_saved_by(user)
      audit.disable_triggers = true
      audit.events << knowledge_event
      save!
    end
    knowledge_event
  end

  def set_attribute_values(att_val_ids, audit = nil)
    att_vals = nil
    statsd_client.time('set_attribute_values') do
      bivr = ::Zendesk::Deco::Request::BulkInstanceValues.new(deco)
      att_vals = bivr.post_instance_values(
        Zendesk::Deco::Client::TYPE_ID_TICKET,
        id,
        att_val_ids
      )
    end

    if audit && att_vals
      evt_att_vals = att_vals.map do |att_val|
        { id: att_val.id, name: att_val.name, attribute_id: att_val.attribute_id }
      end
      event = AssociateAttValsEvent.new(attribute_values: evt_att_vals)
      event.audit = audit
      event.save!
    end
  end

  def attribute_value_ids=(new_attribute_value_ids)
    # Rails converts Controller arguments like `[]` to nil, but we want to know
    # that `[]` was supplied, so convert back.  See
    # https://github.com/rails/rails/issues/8832
    @attribute_value_ids = new_attribute_value_ids || []
  end

  def application_logger
    @application_logger || Rails.logger
  end

  # Is this ticket an "agent as end user" ticket for the indicated user?
  # NOTE: Does not check Arturo
  def agent_as_end_user_for?(user)
    submitter_id == user.id &&
    (via_id == ViaType.WEB_SERVICE || via_id == ViaType.WEB_FORM) &&
    via_reference_id == ViaType.HELPCENTER &&
    user.is_agent? &&
    !user.is_admin?
  end

  # Is this ticket an "agent as end user" ticket for the submitting user?
  # NOTE: Does not check Arturo
  def agent_as_end_user?
    (via_id == ViaType.WEB_SERVICE || via_id == ViaType.WEB_FORM) &&
    via_reference_id == ViaType.HELPCENTER &&
    submitter.is_agent? &&
    !submitter.is_admin?
  end

  def record_deletion_audit
    CIA.record(:destroy, self)
  end

  def assign_attribute_values
    if closed?
      errors.add(:status, I18n.t('activerecord.errors.messages.closed_prevents'))
    else
      self.attribute_maps = begin
        account.attribute_ticket_maps.select do |attribute_ticket_map|
          attribute_ticket_map.match?(self)
        end
      end
    end
  end

  def original_raw_email_identifier
    system_metadata = initial_audit.metadata[:system]
    return if system_metadata.blank?

    system_metadata[:raw_email_identifier]
  end

  def tag_array
    if account&.has_build_tag_array_from_current_tags?
      TagManagement.normalize_tags(current_tags)
    else
      super
    end
  end

  ####################################

  protected

  attr_reader :attribute_maps,
    :macro_ids

  ####################################

  private

  ####################################

  def redact_archived_comment!(comment, text, redact_full_comment)
    archived_ticket_json = ZendeskArchive.router.get(ticket_archive_stub).data
    return false unless archived_ticket_json && archived_ticket_json['description']

    targeted_comment = archived_ticket_json['events'].find { |event| event["type"] == "Comment" && event['id'] == comment.id }
    return false unless targeted_comment

    # Redact the ticket description
    if archived_ticket_json['description'].eql?(targeted_comment['value'])
      archived_ticket_json['description'] = Zendesk::TextRedaction.redact_part_or_all!(archived_ticket_json['description'], text, redact_full_comment)
    else
      # String redaction mutates the original string while full redaction does not, so the assignment isn't necessary
      Zendesk::TextRedaction.redact_part_or_all!(archived_ticket_json['description'], text, redact_full_comment)
    end

    # Redact the targeted comment
    redactor = rich? ? Zendesk::HtmlRedaction : Zendesk::TextRedaction
    redacted_comment_value = redactor.redact_part_or_all!(targeted_comment['value'], text, redact_full_comment)
    targeted_comment['value'] = redacted_comment_value

    ZendeskArchive.router.set(ticket_archive_stub, archived_ticket_json)
  end

  # Save the attribute_values that were set by the attribute_ticket_maps via
  # assign_attribute_values
  def save_mapped_attribute_values
    return unless attribute_maps.present?

    unless audit
      raise "You are trying to make changes to the ticket that needs to be recorded in an Audit. You must call #will_be_saved_by beforehand (2)."
    end

    att_val_ids = attribute_maps.map(&:attribute_value_id)

    if account.has_log_attribute_values_assignment?
      Rails.logger.info(
        "Investigate attribute values assignment - att_val_ids: #{att_val_ids}"
      )
    end

    # The actual storage of the info into Deco is done in an asynchronous job
    # to avoid slowing down Ticket save more than necessary.
    unless att_val_ids.empty?
      TicketAttributeValuesSetterJob.enqueue(
        account_id,
        ticket_id: id,
        attribute_value_ids: att_val_ids,
        audit_id: audit.id
      )
    end

    self.attribute_maps = []
  end

  # When we're manually setting Attribute Values, we "touch" the Ticket
  # by resetting the `updated_at` value.  If we do NOT touch the Ticket,
  # then there may be NO changes to the Ticket at all, and an Audit
  # will not get created.  See TR-441.
  def touch_when_manually_set_attribute_values
    # See notes in save_manually_set_attribute_values
    return unless instance_variable_defined?(:@attribute_value_ids)
    return unless account.has_skill_based_ticket_routing?
    # We don't need to "touch" the Ticket if some other code has already changed
    # it.
    self.updated_at = Time.now unless changed?
  end

  # Save the attribute_values that were specified through the API and stored to
  # attribute_value_ids
  def save_manually_set_attribute_values
    # If attribute_value_ids isn't set, the client hasn't specified the
    # attribute values this Ticket should have, so we should leave them
    # unchanged.
    return unless instance_variable_defined?(:@attribute_value_ids)

    # Don't save changes unless feature is available
    return unless account.has_skill_based_ticket_routing?

    set_attribute_values(@attribute_value_ids, audit || audits.latest)
  end

  def deco
    @deco ||= Zendesk::Deco::Client.new(account)
  end

  def map_attributes?
    new_record? && account.has_skill_based_attribute_ticket_mapping?
  end

  # Note: Currently the requester locale is being used during the publication
  # of ticket entity to Kafka. If in future, we decide that locale_id should
  # not be copied from requester locale during ticket save. Please make sure
  # we also update entity publishing locale accordingly
  def copy_locale_id_from_requester
    # Avoid reporting metrics for AR callback skip as entity publication is now
    # getting the locale from requester directly.
    #
    # Read more in lib/zendesk/extensions/ar_skipped_callback_metrics/instrument.rb
    allow_without_entity_publication do
      update_column :locale_id, requester.try(:locale_id) unless destroyed? # don't need or want an audit, but need this to stay in sync
    end
  end

  def set_assignee_via_email_address
    email_assignee = account.find_user_by_email(@assignee_email)
    self.assignee_id = email_assignee.id unless email_assignee.nil?
  end

  def cleanup_instance_variables
    reset_delta_changes!

    self.external_changes  = nil
    self.comment           = nil
    self.new_sla_policy_id = nil

    @audit                    = nil
    @domain_events            = nil
  end

  def add_audit_as_association
    if @audit && @audit.events.any?
      audits << @audit

      self.updated_at = Time.now unless updated_at_changed? # makes sure that updated_at changes even when no other changes are made to the tickets table
    end
    true
  end

  def build_followup_from(source_ticket)
    return unless new_record?

    if taggings.empty? && current_tags.blank?
      self.set_tags = source_ticket.current_tags
    end

    self.brand ||= source_ticket.brand if source_ticket.brand.try(:active?)
    self.ticket_form ||= source_ticket.ticket_form if source_ticket.ticket_form.try(:active?)

    if account.has_followup_only_uses_source_fields?
      source_ticket.ticket_field_entries.each do |entry|
        copy_source_field_to_followup(source_ticket, entry.ticket_field)
      end
    else
      source_ticket.account.ticket_fields.custom.active.each do |field|
        copy_source_field_to_followup(source_ticket, field)
      end
    end
  end

  def copy_source_field_to_followup(source_ticket, field)
    return unless field.is_active && !TicketField::SYSTEM_FIELDS.include?(field.type)
    # Customers expect a follow up ticket's time worked values to start again back at 0.
    return if building_followup_and_created_by_time_tracking?(field)
    ticket_field_entries.build(
      ticket_field: field,
      account: account,
      value: field.value(source_ticket)
    )
    delta_changed_attributes[field.id.to_s] = field.value(self) unless field.value(source_ticket).blank?
  end

  def save_updated_requester
    if requester.changed? && !requester.save
      errors.add(:requester, :invalid)
      false
    end
  end

  # For I18n this does not use the standard rails validation,
  def validate_external_id_length
    if external_id.to_s.size > 255
      errors.add(:base, I18n.t("txt.admin.models.ticket.validation.external_id_length"))
    end
  end

  def validate_requester_is_valid
    errors.add(:requester, @requester_error) if @requester_error
  end

  def block_inbound_spam_create
    return unless run_spam_detection_on_ticket?

    statsd_client.increment('ticket.spam_detector_scan', tags: ["source:#{ViaType[via_id].name}"])

    detector = Fraud::SpamDetector.new(self)
    run_spam_actions_on_ticket(detector)
  end

  def run_spam_actions_on_ticket(detector)
    return unless detector

    # block the ticket
    if detector.reject_ticket?
      statsd_client.increment('ticket.spam_detector_action', tags: ["source:#{ViaType[via_id].name}", "action:reject"])
      Rails.logger.info "ticket_model_rejected_ticket action fired due to indicators:#{detector.spam_indicators} -- account_id:#{account.id}"
      raise Fraud::SpamDetector::SpamTicketFoundException
      # block the ticket and create a suspended ticket
    elsif detector.suspend_ticket?
      statsd_client.increment('ticket.spam_detector_action', tags: ["source:#{ViaType[via_id].name}", "action:suspend"])
      Rails.logger.info "ticket_model_suspended_ticket action fired due to indicators:#{detector.spam_indicators} -- account_id:#{account.id}"
      @suspended_ticket = suspend(SuspensionType.MALICIOUS_PATTERN_DETECTED)
      rack_request_span&.set_tag('zendesk.ticket.spam_detected.subdomain', account.subdomain)
      raise Fraud::SpamDetector::SpamTicketFoundException
    end
  end

  def run_spam_detection_on_ticket?
    # 1. 'spam_detector_inbound_spam' is the main killswitch for SpamDetector
    # 2. When a suspended ticket is recovered and validations are re-run, do not run spam detector, in these cases
    #    the submitter will be an agent instead of an end user creating spam
    # 3. Mail runs SpamDetector separately in the MTC path, do not run it here in the model
    #    - This includes follow-up tickets, which we also want to skip
    return false if from_mail_or_followup?

    result = account.has_spam_detector_inbound_spam? && submitter.try(:is_end_user?)
    unless result
      Rails.logger.info("TICKET - skipping_spam_detection - #{account.subdomain} (#{account.id}). spam_detector_inbound_spam: #{account.has_spam_detector_inbound_spam?}, submitter_is_end_user: #{submitter.try(:is_end_user?)}")
    end

    result
  end

  def from_mail_or_followup?
    return true if via?(:mail)

    result = account.has_orca_classic_closed_ticket_check? && via?(:closed_ticket)
    if result
      application_logger.info("TICKET - from_mail_or_followup - skipping spam detection on ticket - #{self}")
    end

    result
  end

  def send_spam_ticket_to_suspended_queue
    return unless @suspended_ticket
    @suspended_ticket.save!
  end

  def email_registered_with_user?(user, email)
    return true if email.casecmp(user.email.to_s.downcase).zero?
    user.identities.email.each do |identity|
      return true if email.casecmp(identity.value.to_s.downcase).zero?
    end
    false
  end

  def set_requester
    @requester_error = nil
    external_id = requester_data[:external_id] if requester_data.present?
    email = requester_data[:email] if requester_data.present?

    if external_id && requester.try(:external_id) != external_id
      user = account.users.find_by_external_id(external_id.to_s)

      if user && email.present? && !email_registered_with_user?(user, email)
        @requester_error = I18n.t('txt.admin.models.ticket.validation.requester_external_id_and_email_mismatch', external_id: external_id.to_s, email: email)
        # We found the user by external_id, but the emails don't match
        return
      end
    end

    # Tries to find user by existing requester email
    if email.present?
      email = email.to_s.strip # because some people pass in garbage like ['foo']
      if !email.casecmp(requester.try(:email).to_s.downcase).zero?
        user ||= account.find_user_by_email(email)
      else
        user = requester
      end
    end

    return unless requester_data || user

    user ||= build_new_requester.tap do
      # As requester_id will be nil for new requesters
      requester_id_delta_will_change!
    end

    if external_id && !account.has_disable_updating_requester_via_ticket?
      user.attributes = requester_data.slice(:name, :email, :external_id)
    end

    self.requester = user

    set_requester_tags
  end

  def save_needs_audit?
    comment || delta_changes? || self.external_changes
  end

  def ensure_audit_present
    if !@audit && save_needs_audit?
      raise "You have made changes to the ticket that needs to be recorded in an Audit. You must call #will_be_saved_by before save"
    end
  end

  # The following is mainly used for SuspendedTickets with Screencasts.
  # When we convert a SuspendedTicket into a Ticket we "park" an array in
  # Ticket#screencasts attr_accessor.
  # Eventually before_save and after we have created an audit we copy them into
  # audit.metadata[:system]
  # We use the same strategy when receiving screencasts from the /requests API.
  def copy_screencasts_into_audit_metadata
    if screencasts.present?
      inject_screencasts_in_metadata(screencasts)
    end
  end

  def add_ticket_events_to_audit
    @audit.grow if @audit
  end

  def run_triggers_on_audit
    if @audit
      if account.has_pause_trigger_execution?
        error_key = 'txt.error_message.models.ticket.audit_event.paused_execution'
        add_translatable_error_event({ key: error_key }, false)
      else
        if account.has_instrument_ticket_save?
          state = nil
          ms = Benchmark.ms { state = @audit.execute }
          rack_request_span&.set_tag('zendesk.ticket.trigger_execution.time', ms)
        else
          state = @audit.execute
        end
        !state.aborted?
      end
    else
      true
    end
  end

  def notify_radar
    value = { updated_by: current_user&.id, updated_at: updated_at.to_i }

    ::RadarFactory.create_radar_notification(account, "ticket/#{nice_id}").send("updated", value.to_json)
  end

  def skip_notify_radar?
    skip_reason = if !current_user                   then "!current_user"
    elsif audit && audit.disable_radar_notification? then "audit.disable_radar_notification?"
    elsif previous_changes.empty?                    then "previous_changes.empty?"
    elsif deleted?                                   then "deleted?"
    end

    return unless skip_reason
    Rails.logger.info("Skipping #notify_radar because #{skip_reason}")
    true
  end

  def set_ticket_field_entry(ticket_field, value)
    value = ticket_field.normalize_value_and_sync_tags(self, value)
    return if value.nil?

    set_ticket_field_entry_value(ticket_field, value)
  end

  def building_followup_and_created_by_time_tracking?(ticket_field)
    return false unless account.has_skip_time_tracking_fields_on_followups?

    via_followup_source_id.present? && ticket_field.created_by_time_tracking_app?
  end

  def attribute_delta_change(attr)
    if attr.to_i != 0 # a ticket field id ?
      # return ticket_field change
      value = ticket_field_entries.to_a.find { |f| f.ticket_field_id == attr.to_i }.value
      [delta_changed_attributes[attr], value] if attribute_delta_changed?(attr)
    else
      super
    end
  end

  def save_updated_ticket_fields
    # `collection.target` is empty if the association hasn't been loaded yet
    return if ticket_field_entries.target.blank?

    ticket_field_entries.target.all? do |entry|
      if entry.new_record? || !entry.changed? || entry.save
        true
      else
        errors.add(:base, entry.errors.full_messages.join(" "))
        false
      end
    end
  end

  def evaluate_privacy
    if comment.nil? && Zendesk::CommentPublicity.last_public_comment_made_private?(self) # rubocop:disable Style/ConditionalAssignment
      self.is_public = false
    else
      self.is_public = comment.try(:set_privacy!) || public_comments.any?
    end
    true
  end

  def rack_request_span
    grandparent_span = Datadog.tracer.active_span&.parent&.parent
    grandparent_span if grandparent_span&.name == 'rack.request'
  end

  #### Before validation methods

  def build_new_requester
    build_new_user(@requester_data)
  end

  def build_new_user(user_data)
    unless user_data[:locale_id].present?
      user_data[:locale_id] = if via_id == ViaType.WEB_FORM && current_user.present? && current_user.is_anonymous_user?
        I18n.translation_locale.id
      else
        account.translation_locale.id
      end
    end

    if user_data.key?(:email)
      user_data[:identities] = [{type: :email, value: user_data.delete(:email)}]
    elsif user_data.key?(:identity)
      user_data[:identities] = [user_data.delete(:identity)]
    end

    Zendesk::Users::Initializer.new(
      account, current_user, user: user_data
    ).apply(account.users.new)
  end

  def set_requester_tags
    return unless requester_id_changed? || organization_id_changed?

    # reload if requester_id changed, but the requester was already loaded
    new_requester = requester
    new_requester.reload if
    new_requester &&
    !new_requester.new_record? &&
    new_requester.id == requester_id_was

    self.remove_tags = requester_was.try(:ticket_tags, organization_was)
    self.additional_tags = new_requester.try(:ticket_tags, organization)
  end

  def fix_overflowing_tags
    pruned_tags = truncate_tags
    if pruned_tags.present?
      add_translatable_error_event(key: 'ticket_fields.current_tags.too_long_truncated', pruned_tags: pruned_tags.join(' '))
    end
  end

  def add_queued_errors
    queued_errors.each do |error|
      add_translatable_error_event(error)
    end
  end

  def requester_was
    if requester_id_was
      requester.try(:id) == requester_id_was ? requester : User.find_by_id(requester_id_was)
    end
  end

  def organization_was
    if organization_id_was
      organization.try(:id) == organization_id_was ? organization : Organization.where(account_id: account.id, id: organization_id_was).first
    end
  end

  def auto_tag
    return if !comment || !@current_user || !account.settings.ticket_tagging?
    if account.settings.ticket_auto_tagging? && !(@current_user.is_agent? && via?(:web_form)) && current_tags.blank?
      self.set_tags = Thesaurus.autotag(comment.body, account).join(' ')
    end
    true
  end

  def update_taggings?
    !!current_tags_changed?
  end

  #### After save methods

  # Number of updates by a given user are restricted within a time interval. This is to detect and stop loops
  # early on
  def throttle_updates
    return if new_record? || current_user.blank? || facebook_update? || twitter_update?
    if count = Prop.query(:ticket_updates, [id, current_user.id])
      if count > 0 && count % 5 == 0
        Rails.logger.warn("High ticket update ratio #{count} on ticket #{id} by user #{current_user.id} for account #{account_id}/#{account.subdomain} at #{Time.now}")
      end
    end

    throttle_opts = {}
    throttle_opts[:threshold] = account.settings.ticket_update_rate_limit

    Prop.throttle!(:ticket_updates, [id, current_user.id], throttle_opts)
    true
  end

  def apply_ticket_deflection
    return if disable_answer_bot_trigger
    return unless @deflection_rule_id

    TicketDeflectionJob.enqueue(account_id, id, @deflection_rule_id)
  end

  def apply_after_save_macros
    return unless macros_to_be_applied_after_save.present?
    ApplyMacrosJob.enqueue(account_id, id, macros_to_be_applied_after_save)
    self.macros_to_be_applied_after_save = []
  end

  def soft_deleting_or_restoring?
    soft_deleting? || restoring?
  end

  def auditable_ticket_action?
    soft_deleting_or_restoring? || deleting_metadata? || metadata_scrub_testing? || scrubbing_user?
  end

  def soft_deleting?
    (status_id == StatusType.DELETED) && !status_id_was.nil?
  end

  def scrubbing_user?
    account.has_ticket_user_scrubbing_after_close? && closed?
  end

  def restoring?
    status_id_was == StatusType.DELETED
  end

  def deleting_metadata?
    account.has_delete_ticket_metadata_pii_enabled? && closed?
  end

  def record_restore_audit
    CIA.record(:restore, self)
  end

  def record_hard_deletion_audit
    CIA.record(:permanently_destroy, self)
  end

  def log_ticket_creation
    logger.info("Created ticket account: #{account_id} nice_id: #{nice_id} id: #{id}")
  end

  def redact_credit_card_numbers!
    if subject.present? && super
      add_redaction_tag!
    end
  end

  def disable_audit
    audit.disable_triggers = true if audit
  end

  def record_rule_usage(macro_ids)
    return unless account.has_rule_usage_stats?

    Zendesk::RuleSelection::RuleUsage.new(account).
      record_many(type: :macro, rule_ids: macro_ids)

    macro_ids.each do |macro_id|
      rule_ticket_joins.create!(
        account: account,
        ticket:  self,
        rule_id: macro_id
      )
    end
  end

  def macro_reference(macro_id)
    logger.info("Attempting to create a new AgentMacroReference but macro_id is blank. account_id: #{account.id}, ticket_id: #{id}") if account.has_email_log_blank_macro_reference? && macro_id.blank?

    AgentMacroReference.new(
      via_id:           audit.via_id,
      via_reference_id: @current_user.id,
      value:            macro_id,
      audit:            audit
    )
  end

  # Remove if removing functionality behind :email_record_spam_marking_reply_stats
  def record_mark_as_spam_with_reply_comments_metrics
    mark_as_spam_tags = []
    mark_as_spam_tags << 'public_agent_reply'  if public_replies_from_agent?
    mark_as_spam_tags << 'private_agent_reply' if private_replies_from_agent?
    mark_as_spam_tags << 'end_user_reply'      if replies_from_end_user?

    if mark_as_spam_tags.any?
      formatted_mark_as_spam_tags = mark_as_spam_tags.map do |mark_as_spam_tag|
        "reply_type:#{mark_as_spam_tag}"
      end

      Rails.logger.info("Ticket with more than one comment marked as spam: ticket_id: #{id}, spam-marking user_id: #{current_user.id}, reply comment types: [#{mark_as_spam_tags.join(", ")}]")
      statsd_client.increment('mark_as_spam_by', tags: formatted_mark_as_spam_tags)
    end
  end

  # Remove if removing functionality behind :email_record_spam_marking_reply_stats
  def reply_comments
    # This method will only be called in cases where an agent is marking a
    # ticket as spam using the "Mark as spam" feature. These tickets generally
    # only have one comment, and it's extremely unlikely that they'll wind up
    # with more than a few comments. Just to be safe, we only look at the first
    # 25 reply comments here to avoid memory problems.
    @reply_comments ||= comments.limit(26).to_a[1..-1] || []
  end

  # Remove if removing functionality behind :email_record_spam_marking_reply_stats
  def reply_comments_from_agent
    @reply_comments_from_agent ||= reply_comments.select do |reply_comment|
      reply_comment.author.is_agent?
    end
  end

  # Remove if removing functionality behind :email_record_spam_marking_reply_stats
  def public_replies_from_agent?
    is_public? && reply_comments_from_agent.any?(&:is_public?)
  end

  # Remove if removing functionality behind :email_record_spam_marking_reply_stats
  def private_replies_from_agent?
    reply_comments_from_agent.any?(&:is_private?)
  end

  # Remove if removing functionality behind :email_record_spam_marking_reply_stats
  def replies_from_end_user?
    reply_comments.any? do |reply_comment|
      reply_comment.author.is_end_user?
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'ticket')
  end
end
