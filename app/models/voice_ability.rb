require 'access/authorization'

class VoiceAbility < Access::Authorization
  def policy_authorization_models
    {
      Voice::Call           => Access::Policies::VoiceCallPolicy,
      Voice::Core::CallStat => Access::Policies::VoicePolicy,
      Voice::PhoneNumber    => Access::Policies::VoicePolicy,
    }
  end

  def permission_authorization_models
    {
      Voice::Call           => Access::Permissions::VoiceCallPermission,
      Voice::Core::CallStat => Access::Permissions::VoicePermission,
      Voice::PhoneNumber    => Access::Permissions::VoicePermission,
    }
  end
end
