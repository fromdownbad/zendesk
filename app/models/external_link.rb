class ExternalLink
  class Collection < Array
    include Zendesk::Serialization::ExternalLinkCollectionSerialization
  end

  VALID_EXTERNAL_LINK_NAMES = ['JiraIssue', 'GetSatisfactionTopic', 'LogMeInRescueSession'].freeze

  attr_reader :ticket, :params

  def initialize(ticket, params)
    @ticket = ticket
    @params = params
  end

  def self.build(*args)
    new(*args).build
  end

  def self.create!(*args)
    new(*args).create!
  end

  def self.destroy!(*args)
    new(*args).destroy!
  end

  # It is dangerous to use ExternalLink.build and then refer to the :through associations such as
  # ticket.jira_issues, ticket.get_satisfaction_topics etc, as these will not be present until
  # the external link is saved and the ticket is reloaded.
  def build
    external_link = link_class.find_from_params(filtered_params)
    if external_link.present?
      @ticket.linkings.build(external_link: external_link, account: filtered_params[:account])
      external_link.ticket_callback(@ticket)
    else
      external_link = link_class.new(filtered_params)
      @ticket.linkings.build(external_link: external_link, account: filtered_params[:account])
    end
  rescue NameError
    nil
  end

  def create!
    external_link = link_class.find_from_params(filtered_params)
    if external_link.present?
      @ticket.linkings.create!(external_link: external_link, account: filtered_params[:account])
      external_link.ticket_callback(@ticket)
    else
      external_link = link_class.create!(filtered_params)
      @ticket.linkings.create!(external_link: external_link, account: filtered_params[:account])
    end
  end

  def destroy!
    external_link = link_class.find_from_params(@params)
    external_link.linkings.find_by_ticket_id!(@ticket.id).destroy
  end

  private ##########################################

  def filtered_params
    out_params = {}
    link_class::EXTERNALLY_VISIBLE_PARAMS.each do |key|
      out_params[key] = @params[key] if @params.key?(key)
    end
    # we can't always grab the account_id from the ticket as the ticket may only be a stub
    if @ticket.present? && @ticket.account.present?
      out_params[:account] = @ticket.account
    elsif @params.has_key?[:account]
      # log me in rescue sessions may be constructed before their associated tickets.
      out_params[:account] = @params[:account]
    else
      raise NameError, "Account not present"
    end

    out_params
  end

  def link_class
    link_type = @params[:type]
    raise NameError, "External Link type #{link_type} invalid" unless VALID_EXTERNAL_LINK_NAMES.include?(link_type)

    link_type.classify.constantize
  end
end
