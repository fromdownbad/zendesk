module Apps
  class ChatAppInstallation
    # Production: https://<account>.zendesk.com/api/v2/apps/30460.json
    # Staging: https://<account>.zendesk-staging.com/api/v2/apps/36.json
    CHAT_APP_ID = Rails.env.staging? ? 36 : 30460

    def initialize(app_market_client:)
      @app_market_client = app_market_client
    end

    def handle_disable_installation
      disable_installation(installation['id']) if installation_enabled?
    end

    private

    attr_reader :app_market_client

    def installation_enabled?
      installation && installation['enabled']
    end

    def installation
      @installation ||= begin
        installations = app_market_client.fetch_installations
        installations.find { |installation| installation['app_id'] == CHAT_APP_ID }
      rescue ZendeskAPI::Error::RecordNotFound
        false
      end
    end

    def disable_installation(installation_id)
      json = app_market_client.update_installation(installation_id, enabled: false).body
      if json['enabled']
        raise StandardError, "Could not change Chat App Installation state to false"
      end

      true
    end
  end
end
