class GoogleProfile < ActiveRecord::Base
  attr_accessible :account, :user, :user_identity, :value

  validates_presence_of :account_id, :user_id, :user_identity_id

  belongs_to :user
  belongs_to :user_identity, inherit: :account_id

  before_validation :inherit_account_and_user_from_identity, on: :create

  def inherit_account_and_user_from_identity
    self.account_id = user_identity.account_id
    self.user_id    = user_identity.user_id
  end
end
