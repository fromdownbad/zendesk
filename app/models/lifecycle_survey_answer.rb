class LifecycleSurveyAnswer < ActiveRecord::Base
  belongs_to :account
  belongs_to :user
  belongs_to :surveyable, polymorphic: true

  attr_accessible :account, :user, :answer_key, :answer_text, :survey_name

  validates :account,     presence: true
  validates :user,        presence: true
  validates :answer_key,  presence: true
  validates :survey_name, presence: true

  before_save :remove_unrelated_answer_text

  class << self
    #
    # answers: {
    #    survey_name: "trial_extension"
    #    answer_keys: %w[ key1 key2 ]
    #    answer_text: "I am trying to see if it works"
    # }

    def from_answers(account, user, answers)
      return unless answer_keys = answers.with_indifferent_access[:answer_keys]

      common_params = answers.select { |k| k.to_s != 'answer_keys' }
      answer_keys.map do |answer_key|
        new(common_params.merge(
          account: account,
          user: user,
          answer_key: answer_key
        ))
      end
    end
  end

  private

  def remove_unrelated_answer_text
    self.answer_text = nil unless option_is_other?
  end

  def option_is_other?
    answer_key.end_with?('other')
  end
end
