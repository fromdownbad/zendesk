class AttachmentExternalURL < ActiveRecord::Base
  MAX_URL_SIZE = 255

  self.table_name = 'attachment_external_urls'

  belongs_to :account
  belongs_to :attachment, inherit: :account_id

  attr_accessible :account, :attachment, :url

  before_validation :truncate_url

  validates_presence_of :account_id
  validates_presence_of :attachment
  validates_length_of :url, maximum: MAX_URL_SIZE

  private

  def truncate_url
    return unless url.size > MAX_URL_SIZE

    if url =~ /(\.[^\.]{1,16}\z)/
      extension = $1
      url_minus_extension = url[0..-(extension.bytesize + 1)]

      self.url = chop(url, url_minus_extension, "...#{extension}")
    else
      self.url = chop(url, url, '...')
    end
  end

  def chop(entire_url, base_url, ending)
    end_url_index = ((MAX_URL_SIZE - 1) - ending.bytesize)
    chopped = nil
    loop do
      chopped = base_url[0..end_url_index]
      break if URI.decode(entire_url).start_with?(URI.decode(chopped))
      end_url_index -= 1
    end

    "#{chopped}#{ending}"
  end
end
