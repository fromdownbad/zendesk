class Workspace < ActiveRecord::Base
  has_many :ticket_workspaces, dependent: :destroy, inherit: :account_id
  has_many :tickets, through: :ticket_workspaces

  has_many :workspace_elements, autosave: true, dependent: :destroy, inherit: :account_id
  has_many :selected_macros, through: :workspace_elements, source: :element, source_type: "Macro"

  belongs_to :account
  belongs_to :ticket_form, inherit: :account_id

  SUPPORTED_ELEMENT_TYPES = %w[App Macro].freeze
  MAX_ACTIVE_WORKSPACES = 20
  MAX_RULES_IN_WORKSPACE = 10

  attr_accessor :current_user

  serialize :macros, Array
  serialize :apps, Array
  serialize :definition

  scope :active, -> { where(activated: true) }

  before_validation :ensure_valid_definition
  before_validation :convert_zero_to_nil

  validates :title, presence: true
  validates :account_id, presence: true

  validate :ensure_valid_definition_size
  validate :run_new_definition_validations
  validate :check_rules_limit_for_workspace
  validate :check_elements_validity_for_workspace

  before_save :set_defaults
  before_create :set_position_to_last
  before_create :check_product_limit_for_create_workspace
  before_update :check_product_limit_for_update_workspace
  before_update :set_position_to_last, if: proc { |workspace| workspace.activated_changed?(from: false, to: true) }
  after_update :normalize_workspace_positions, if: proc { |workspace| workspace.activated_changed?(from: true, to: false) }
  after_destroy :normalize_workspace_positions

  attr_accessible

  # change to constant variable after Arturo is deprecated
  def max_active_workspaces
    if account.has_lotus_feature_increase_max_contextual_workspaces?
      50
    else
      20
    end
  end

  def macro_ids
    self[:macros].map(&:to_i)
  end

  def app_ids
    self[:apps].map(&:to_i)
  end

  def definition=(params)
    self[:definition] = build_definition(params)
  end

  def apps
    workspace_elements.where(element_type: "App").map { |app| { id: app.element_id, expand: app.expand_by_default, position: app.position } }
  end

  def save_with(params, options)
    self.current_user = options[:user]
    params = ActiveSupport::HashWithIndifferentAccess.new(params)

    ActiveRecord::Base.transaction do
      build_workspace_elements(params[:macros], params[:apps])

      params.keys.each do |key|
        case key
        when "conditions"
          self.definition = params[key]
        else
          self[key] = params[key]
        end
      end

      save
    end
  end

  def self.reorder(ids, account)
    return unless ids.is_a? Array
    workspaces = account.workspaces.where(id: ids)
    ActiveRecord::Base.transaction do
      ids.each_with_index do |id, index|
        workspace = workspaces.find { |w| w.id == id }
        raise StandardError, "Could not find workspace with id \"#{id}\"." unless workspace
        raise StandardError, "Could not update workspace with id #{id}: #{workspace.errors.full_messages.join('.')}" unless workspace.valid?

        workspace.update_column(:position, index + 1)
      end
    end

    # We need to reload in order for the presenter to have the most up to date workspace_elements data
    # https://support.zendesk.com/agent/tickets/4924001
    workspaces.reload
  end

  def rule_type
    @rule_type ||= self.class.name.underscore
  end

  def self.active_workspaces_count(account)
    account.workspaces.active.count
  end

  private

  def set_defaults
    @macros ||= []
    @apps ||= []
  end

  def build_element_for(element_type, element_id)
    return unless SUPPORTED_ELEMENT_TYPES.include? element_type

    workspace_element = workspace_elements.new
    workspace_element.element_id = element_id
    workspace_element.element_type = element_type
    workspace_element.account_id = account_id

    workspace_element
  end

  def build_app_element(attributes = {})
    return unless HashParam.ish?(attributes)

    workspace_element = workspace_elements.new
    workspace_element.build_app_element_with(attributes.merge(account_id: account_id, workspace_id: id))

    workspace_element
  end

  def build_workspace_elements(macros = [], apps = [])
    ActiveRecord::Base.transaction(requires_new: true) do
      workspace_elements.destroy_all
      apps.each { |attributes| build_app_element(attributes) } if apps.is_a? Array
      macros.each { |macro_id| build_element_for("Macro", macro_id) } if macros.is_a? Array
    end
  end

  def build_definition(rule_params = {})
    return unless HashParam.ish?(rule_params)

    if conditions = rule_params.delete(:conditions)
      rule_params[:all] ||= conditions[:all]
      rule_params[:any] ||= conditions[:any]
    end

    if rule_params[:all]
      conditions_all = map_definition_items(rule_params.delete(:all))
    end

    if rule_params[:any]
      conditions_any = map_definition_items(rule_params.delete(:any))
    end

    if conditions_all || conditions_any
      workspace_definition = begin
        Definition.build(
          conditions_all: conditions_all,
          conditions_any: conditions_any
        )
      end
    end

    workspace_definition
  end

  def ensure_valid_definition
    return if definition.is_a?(Definition)

    self.definition = nil

    errors.add(
      :base,
      I18n.t(
        'txt.models.rules.rule.invalid_conditions_you_must_select_condition'
      )
    )

    false
  end

  def convert_zero_to_nil
    # We are sending '0' for the 'No Change' ticket form option when selected
    # and 'Null' for new workspaces. Setting it to 'Null' for consistency
    self.ticket_form_id = nil if ticket_form_id&.zero?
  end

  def ensure_valid_definition_size
    return unless @attributes['definition'].value_for_database.bytesize > Rule::MAX_DEFINITION_SIZE

    # this error message is misleading as it's not clear about the
    # serialization overhead, but it's the best we've got
    errors.add(
      :base,
      I18n.t(
        'activerecord.errors.messages.too_long',
        count: Rule::MAX_DEFINITION_SIZE
      )
    )
  end

  def run_new_definition_validations
    definition.run_new_validations(account, :contextual_workspace, self, current_user) if current_user && account
  end

  def map_definition_items(conditions = [])
    conditions.each do |condition|
      Api::V2::Rules::ConditionMappings.map_definition_item!(
        account,
        condition
      )
    end
  end

  # for creating or activating a workspace
  def set_position_to_last
    self.position = Workspace.active_workspaces_count(account) + 1
  end

  # for deactivating or deleting a workspace
  def normalize_workspace_positions
    ActiveRecord::Base.transaction(requires_new: true) do
      account.workspaces.active.order(:position).each_with_index do |workspace, index|
        workspace.position = index + 1
        workspace.save
      end
    end
  end

  def check_product_limit_for_create_workspace
    if activated && Workspace.active_workspaces_count(account) >= max_active_workspaces
      raise StandardError, I18n.t(
        'activerecord.errors.messages.workspaces.save.product_limit',
        count: max_active_workspaces
      )
    end
  end

  def check_product_limit_for_update_workspace
    if activated_changed?(from: false, to: true) && Workspace.active_workspaces_count(account) >= max_active_workspaces
      raise StandardError, I18n.t(
        'activerecord.errors.messages.workspaces.save.product_limit',
        count: max_active_workspaces
      )
    end
  end

  def number_of_rules
    if definition
      definition.conditions_all.count + definition.conditions_any.count
    else
      0
    end
  end

  def check_rules_limit_for_workspace
    if number_of_rules > MAX_RULES_IN_WORKSPACE
      raise StandardError, I18n.t(
        'activerecord.errors.messages.workspaces.save.rule_limit',
        count: MAX_RULES_IN_WORKSPACE
      )
    end
  end

  def check_elements_validity_for_workspace
    ticket_form_is_valid && macros_are_valid
  end

  def ticket_form_is_valid
    return true if ticket_form_id.nil? # allow nil ticket form, which means don't change the ticket's ticket_form on the UI
    ticket_form = account.ticket_forms.where(id: ticket_form_id).first

    if !ticket_form || !ticket_form.active # if ticket_form_id is passed, makes sure it exists
      errors.add(
        :base,
        I18n.t('activerecord.errors.messages.workspaces.invalid_ticket_form')
      )
    end
  end

  def macros_are_valid
    macro_ids = workspace_elements.map { |element| element.element_type == "Macro" ? element.element_id : nil }.compact
    new_macros_count = Macro.where(id: macro_ids, account_id: account_id).count

    if macro_ids.count != new_macros_count
      errors.add(
        :base,
        I18n.t('activerecord.errors.messages.workspaces.invalid_macros')
      )
    end
  end
end
