class UserContactInformation < ActiveRecord::Base
  not_sharded
  disable_global_uid

  belongs_to :user
  belongs_to :account

  validates_presence_of :user_id, :account_id

  self.table_name = 'user_contact_information'
  attr_accessible

  def self.last_updated_date(account_id)
    date_last_run = select('updated_at').where(account_id: account_id).order('updated_at DESC').limit(1).first
    date_last_run.try(:updated_at)
  end

  def self.get_user_contact_information_id(account_id, user_id)
    user_contact_id = select('id').where(account_id: account_id, user_id: user_id).first
    user_contact_id.try(:id)
  end
end
