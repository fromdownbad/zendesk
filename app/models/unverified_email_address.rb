class UnverifiedEmailAddress < ActiveRecord::Base
  belongs_to :user
  belongs_to :account

  attr_accessible :email, :skip_verification, :user, :account

  validates_presence_of :user, :expires_at
  validates_format_of :email, with: EMAIL_PATTERN
  validates_uniqueness_of :token
  validates_uniqueness_of :email, scope: :user_id

  before_validation :set_account_id
  before_validation :set_expires_at
  before_validation :generate_token

  validate :email_is_not_in_use_by_user
  validate :email_is_not_in_use_by_account
  validate :email_is_unique_when_user_cannot_be_merged

  attr_accessor :skip_verification
  after_commit :send_verification_email, unless: :skip_verification, on: :create
  after_destroy :log_destroyed_address

  def self.find_by_token(token)
    where(token: token.to_s).first
  end

  def will_merge_with_user
    Users::Merge.existing_identity(user, build_identity).try(:user)
  end

  def verify!(merge_confirmation = false)
    return nil unless user.is_active

    new_identity = build_identity
    merge_identity = Users::Merge.existing_identity(user, new_identity)

    # we require confirmation on verifying an address that will be merged
    return false if merge_identity && !(merge_confirmation && Users::Merge.valid_merge?(user, merge_identity.user))

    User.transaction do
      if merge_identity
        loser = merge_identity.user
        merge_identity.transfer_to_user(user)
        new_identity = merge_identity
        UserMergeJob.enqueue(user.account_id, user.id, loser.id)
      else
        user.identities << new_identity
        user.save!
        new_identity.make_primary! unless user.identities.email.first.is_verified?
      end
      destroy
    end
    new_identity
  end

  def send_verification_email
    if Zendesk::UserPortalState.new(account).send_verify_email?(user)
      UsersMailer.deliver_verify_email_address(self, user.active_brand_id)
    end
  end

  def to_s
    email
  end

  def identity_type
    'unverified'
  end

  private

  def build_identity
    UserEmailIdentity.new(user: user, account: user.account,
                          value: email, is_verified: true)
  end

  def set_account_id
    self.account_id = user.try(:account_id)
  end

  def set_expires_at
    unless expires_at && expires_at > Time.now
      self.expires_at = 2.days.from_now
    end
  end

  def generate_token
    self.token ||= Token.generate(20)
  end

  def email_is_not_in_use_by_user
    return if email.blank? || user.nil?
    if user.identities.email.map { |identity| identity.value.downcase }.include?(email.downcase)
      errors.add(:base, I18n.t('txt.error_message.models.unverified_email_address.you_have_already_added_that_email'))
    end
  end

  def email_is_unique_when_user_cannot_be_merged
    return if email.blank? # let the other validations deal with this

    existing_identity = user.account.user_identities.email.find_by_value(email)
    if existing_identity && !existing_identity.user.can_be_merged_into?(user)
      errors.add(:email, :taken)
    end
  end

  def email_is_not_in_use_by_account
    return if email.blank?
    if error = account.email_address_in_use_error(email)
      errors.add(:email, error)
    end
  end

  def log_destroyed_address
    Rails.logger.warn("Destroyed identity #{inspect}")
  end
end
