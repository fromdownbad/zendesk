module ContentMappingSupport
  def self.included(model_class)
    model_class.class_eval do
      has_one :content_mapping,
        ->(model) { where(classic_object_type: "Legacy::#{model.class.sti_name}") },
        foreign_key: :classic_object_id
    end
  end

  def help_center_url
    content_mapping && content_mapping.url
  end
end
