class DunningNotification < ActiveRecord::Base
  not_sharded
  disable_global_uid

  # AllAccountsReport uses subscription_id, dunning_level from DunningNotification for detecting the dunning state
  # of an account.
  validates_presence_of       :subscription, :payment
  validates_numericality_of   :dunning_level, only_integer: true, greater_than_or_equal_to: 0

  validates_uniqueness_of     :dunning_level, scope: [:payment_id]

  belongs_to                  :payment
  belongs_to                  :subscription
  has_one                     :account, through: :subscription

  attr_accessible :subscription, :payment, :dunning_level

  scope :in_level, ->(level) { where(dunning_level: level) }
  scope :unsent, -> { where(delivered_at: nil) }

  after_save :touch_account

  def touch_account
    account.touch_without_callbacks
  end
end
