require 'simple_access/authorizor'
require 'access/permissions/user_permission'
require 'access/permissions/voice_permission'
require 'access/permissions/rule'
require 'access/permissions/settings_permission'
require 'access/policies/rule_policy'
require 'access/policies/voice_policy'

module Access
  # We have two authorization systems:
  # 1. Legacy/Role based. Defined under policy_authorization_models.
  # 2. Enterprise permissions. Defined under permission_authorization_models.
  #
  # When adding new access restrictions, it's likely both models will need to be updated.
  class Authorization < SimpleAccess::Authorizor
    def can?(action, klass_or_instance)
      # Can't allow access to something that doesn't exist
      return false if klass_or_instance.nil?
      # Global: allow system user access
      return true if user.is_system_user? && !(action == :be_assigned_to && klass_or_instance == Ticket)

      # Global: block intra-account access if user is not zendesk manager.
      return false if unauthorized_cross_account_access?(klass_or_instance)

      super
    end

    # api/v1/support
    def to_json(_options = {})
      {
        'ticket' => {
          'tags' => can?(:edit_tags, Ticket) ? 'ReadWrite' : 'ReadOnly'
        }
      }
    end

    protected

    # this is spooky authorization magic that has no org reference, but cannot be removed
    def authorization_model_for(klass)
      super || authorization_models[resolve_event_class(klass)]
    end

    def resolve_event_class(klass)
      Event if klass.ancestors.include?(Event)
    end

    def authorization_models
      return @authorization_models if @authorization_models

      @authorization_models = if user.has_permission_set?
        permission_authorization_models
      else
        policy_authorization_models
      end
    end

    def unauthorized_cross_account_access?(subject)
      subject.respond_to?(:account_id) &&
        subject.account_id.present? &&
        (user.account_id != subject.account_id)
    end

    def policy_authorization_models
      {
        Access::Chat::IncomingChats     => Policies::ChatPolicy,
        Access::Settings::BusinessRules => Policies::RulePolicy,
        Access::Settings::Extensions    => Policies::SettingsPolicy,
        Attachment             => Policies::AttachmentPolicy,
        Automation             => Policies::AutomationPolicy,
        Category               => Policies::CategoryPolicy,
        ::Cms::Text            => Policies::CmsPolicy,
        ::Cms::Variant         => Policies::CmsPolicy,
        Comment                => Policies::CommentPolicy,
        Entry                  => Policies::EntryPolicy,
        ExpirableAttachment    => Policies::ExpirableAttachmentPolicy,
        Event                  => Policies::EventPolicy,
        ::Facebook::Page       => ::Channels::Access::Policies::FacebookPolicy, # TODO: Channels.  FacebookPolicy is defined in the zendesk_channels repo.
        FacebookComment        => Policies::CommentPolicy,
        Forum                  => Policies::ForumPolicy,
        Group                  => Policies::GroupPolicy,
        Membership             => Policies::MembershipPolicy,
        Organization           => Policies::OrganizationPolicy,
        OrganizationMembership => Policies::OrganizationMembershipPolicy,
        Post                   => Policies::PostPolicy,
        Macro                  => Policies::MacroPolicy,
        PermissionSet          => Policies::PermissionSetPolicy,
        Report                 => Policies::ReportPolicy,
        Target                 => Policies::SettingsPolicy,
        Ticket                 => Policies::TicketPolicy,
        TicketField            => ExternalPermissions::TicketFieldExternalPermissions,
        TicketForm             => ExternalPermissions::TicketFormExternalPermissions,
        Trigger                => Policies::TriggerPolicy,
        RuleCategory           => Policies::RuleCategoryPolicy,
        Channels::Twitter::Search => ::Channels::Access::Policies::TwitterPolicy, # TODO: Channels.  TwitterPolicy is defined in the zendesk_channels repo.
        User                   => Policies::UserPolicy,
        UserView               => Policies::UserViewPolicy,
        View                   => Policies::ViewPolicy,
        Voice::Call            => Policies::VoiceCallPolicy,
        Voice::Core::CallStat  => Policies::VoicePolicy,
        Voice::PhoneNumber     => Policies::VoicePolicy,
        VoiceComment           => Policies::CommentPolicy,
        VoiceApiComment        => Policies::CommentPolicy,
        ::Channels::AnyChannel::RegisteredIntegrationService  => ::Channels::Access::Policies::AnyChannelPolicy, # TODO: Channels.  These classes are defined in the zendesk_channels repo.
        ::Channels::AnyChannel::IntegrationServiceInstance    => ::Channels::Access::Policies::AnyChannelPolicy,
        CustomField::Field => ExternalPermissions::CustomFieldsExternalPermissions,
        Workspace => ExternalPermissions::ContextualWorkspacesExternalPermissions
      }
    end

    def permission_authorization_models
      {
        Access::Chat::IncomingChats     => Permissions::ChatPermission,
        Access::Settings::BusinessRules => Permissions::SettingsPermission,
        Access::Settings::Extensions    => Permissions::ExtensionsPermission,
        Attachment             => Policies::AttachmentPolicy,
        Automation             => Permissions::Rule::AutomationPermission,
        Category               => Permissions::CategoryPermission,
        ::Cms::Text            => Permissions::CmsPermission,
        ::Cms::Variant         => Permissions::CmsPermission,
        Comment                => Permissions::CommentPermission,
        Entry                  => Permissions::EntryPermission,
        ExpirableAttachment    => Policies::ExpirableAttachmentPolicy,
        Event                  => Policies::EventPolicy,
        ::Facebook::Page       => ::Channels::Access::Permissions::FacebookPermission, # TODO: Channels.  FacebookPermission is defined in the zendesk_channels repo.
        FacebookComment        => Permissions::CommentPermission,
        Forum                  => Permissions::ForumPermission,
        Group                  => Permissions::GroupPermission,
        Macro                  => Permissions::Rule::MacroPermission,
        Membership             => Permissions::MembershipPermission,
        Organization           => Permissions::OrganizationPermission,
        OrganizationMembership => Permissions::OrganizationMembershipPermission,
        Post                   => Permissions::PostPermission,
        PermissionSet          => Policies::PermissionSetPolicy,
        Report                 => Permissions::ReportPermission,
        Target                 => Permissions::ExtensionsPermission,
        Ticket                 => Permissions::TicketPermission,
        TicketField            => ExternalPermissions::TicketFieldExternalPermissions,
        TicketForm             => ExternalPermissions::TicketFormExternalPermissions,
        Trigger                => Permissions::Rule::TriggerPermission,
        RuleCategory           => Permissions::Rule::RuleCategoryPermission,
        Channels::Twitter::Search => ::Channels::Access::Permissions::TwitterPermission, # TODO: Channels.  TwitterPermission is defined in the zendesk_channels repo.
        User                   => user_permission,
        UserView               => Permissions::Rule::UserViewPermission,
        View                   => Permissions::Rule::ViewPermission,
        Voice::Call            => Permissions::VoiceCallPermission,
        Voice::Core::CallStat  => Permissions::VoicePermission,
        Voice::PhoneNumber     => Permissions::VoicePermission,
        VoiceComment           => Permissions::CommentPermission,
        ::Channels::AnyChannel::RegisteredIntegrationService  => ::Channels::Access::Permissions::AnyChannelPermission, # TODO: Channels.  These classes are defined in the zendesk_channels repo.
        ::Channels::AnyChannel::IntegrationServiceInstance    => ::Channels::Access::Permissions::AnyChannelPermission,
        CustomField::Field => ExternalPermissions::CustomFieldsExternalPermissions,
        Workspace => ExternalPermissions::ContextualWorkspacesExternalPermissions
      }
    end

    def user_permission
      case user.permission_set.permissions.end_user_profile
      when "readonly"        then Permissions::ReadOnlyUserPermission
      when "edit-within-org" then Permissions::EditWithinOrgUserPermission
      when "edit", "full"    then Permissions::FullUserPermission
      end
    end
  end
end
