module Access::ExternalPermissions
  class TicketFormExternalPermissions < Permissions
    def manage?
      allow?([TICKET_FORMS_MANAGE])
    end
  end
end
