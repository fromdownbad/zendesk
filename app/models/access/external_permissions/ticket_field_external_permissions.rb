module Access::ExternalPermissions
  class TicketFieldExternalPermissions < Permissions
    def manage?
      allow?([TICKET_FIELDS_MANAGE])
    end
  end
end
