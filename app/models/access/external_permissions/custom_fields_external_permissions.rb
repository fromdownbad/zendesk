module Access::ExternalPermissions
  class CustomFieldsExternalPermissions < Permissions
    def manage_user_fields?
      allow?([USER_FIELDS_MANAGE])
    end

    def manage_organization_fields?
      allow?([ORGANIZATION_FIELDS_MANAGE])
    end
  end
end
