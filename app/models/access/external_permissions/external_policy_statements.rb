module Access::ExternalPermissions
  class ExternalPolicyStatements
    attr_reader :effect, :resource, :scopes

    def initialize(policy_params)
      @effect = policy_params['effect']
      @resource = policy_params['resource']
      @scopes = policy_params['scopes']
    end
  end
end
