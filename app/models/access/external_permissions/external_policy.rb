module Access::ExternalPermissions
  class ExternalPolicy
    attr_reader :id, :statements

    def initialize(policy_params)
      @id = policy_params['id']
      @statements = policy_params['statements'].map { |s| Access::ExternalPermissions::ExternalPolicyStatements.new(s) }
    end
  end
end
