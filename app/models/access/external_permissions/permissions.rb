module Access::ExternalPermissions
  class Permissions
    attr_reader :user

    TICKET_FIELDS_MANAGE = "ticket_fields:manage".freeze
    TICKET_FORMS_MANAGE = "ticket_forms:manage".freeze
    CONTEXTUAL_WORKSPACES_MANAGE = "contextual_workspaces:manage".freeze
    USER_FIELDS_MANAGE = "user_fields:manage".freeze
    ORGANIZATION_FIELDS_MANAGE = "organization_fields:manage".freeze
    ADMIN_PERMISSIONS = [CONTEXTUAL_WORKSPACES_MANAGE, TICKET_FIELDS_MANAGE, TICKET_FORMS_MANAGE, USER_FIELDS_MANAGE, ORGANIZATION_FIELDS_MANAGE].freeze

    EXTERNAL_PERMISSIONS_MAP = {
      "manage_user_fields" => USER_FIELDS_MANAGE,
      "manage_organization_fields" => ORGANIZATION_FIELDS_MANAGE,
      "manage_ticket_fields" => TICKET_FIELDS_MANAGE,
      "manage_ticket_forms" => TICKET_FORMS_MANAGE,
      "manage_contextual_workspaces" => CONTEXTUAL_WORKSPACES_MANAGE
    }.freeze

    def initialize(user)
      @user = user
    end

    def allow?(request_resource_scopes)
      client.allow!(request(request_resource_scopes)).body["result"]
    rescue StandardError
      false
    end

    def allowed_scopes(request_resource_scopes)
      client.allow_scopes!(request(request_resource_scopes)).body["result"] || []
    rescue StandardError
      []
    end

    private

    def request(request_resource_scopes)
      request_account = { id: user.account.id, shard_id: user.account.shard_id }
      request_user = { id: user.id, role: user.roles, permission_set_id: user.permission_set_id }
      client.request(request_account, request_user, request_resource_scopes)
    end

    def client
      @client ||= Zendesk::ExternalPermissions::PermissionsAgentClient.new
    end
  end
end
