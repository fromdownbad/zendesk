module Access::ExternalPermissions
  class ContextualWorkspacesExternalPermissions < Permissions
    def manage?
      allow?([CONTEXTUAL_WORKSPACES_MANAGE])
    end
  end
end
