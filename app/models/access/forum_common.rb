module Access::ForumCommon
  def edit?
    manage?
  end

  def moderate?
    user.is_admin? || user.is_moderator? || user.is_non_restricted_agent?
  end

  def watch?(forum)
    !user.is_anonymous_user? &&
      user.abilities.defer?(:view, forum) &&
      user.has_email? &&
      forum.account.subdomain != 'twitter'
  end

  private

  def user_belongs_to_forum_organization?(forum)
    !forum.organization_id || user.organization_memberships.exists?(organization_id: forum.organization_id)
  end
end
