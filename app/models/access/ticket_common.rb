module Access
  module TicketCommon
    def apply_common_ticket_permissions(user)
 #     cannot :delete?(ticket)
 #     !user.can?(:delete, Ticket) ||
 #       !user.can?(:edit, ticket)
    end

    def mark_as_spam?(ticket)
      if ticket.requester_id == user.id
        false                       # You can't mark yourself as spam
      elsif ticket.requester_is_agent?
        false                       # You can't mark an agent or admin as spam
      elsif ticket.shared_tickets.any?
        false                       # You can't mark a shared ticket as spam
      else
        # Requester must exist and the current user must be able to edit ticket requester to mark as spam and delete the ticket
        ticket.requester && user.can?(:edit, ticket.requester) && user.can?(:delete, ticket)
      end
    end

    def view_private_content?(ticket)
      return view?(ticket) unless Arturo.feature_enabled_for?(:agent_as_end_user, ticket.account)

      if user.is_admin?
        true
      elsif !user.is_agent?
        false
      elsif ticket.agent_as_end_user_for?(user)
        false
      else
        view?(ticket)
      end
    end

    def view?(ticket)
      if user.is_admin?
        # Admins may view all tickets
        true
      elsif ticket.requester_id == user.id || ticket.requester_id_was == user.id
        # Requesters can view their public tickets.
        ticket.is_public? || user.is_agent?
      elsif user.is_agent? && ticket.new_record?
        true
      elsif user.is_agent? && user.agent_restriction?(:none)
        true
      elsif ticket.is_collaborator?(user)
        # Users may view their public tickets that they are following or already CC'd on.
        ticket.is_public? || user.is_agent?
      elsif user.is_agent? && user.agent_restriction?(:assigned)
        ticket.assignee_id &&
          ((user.id == ticket.assignee_id) || (user.id == ticket.assignee_id_was))
      elsif user.is_agent? && user.agent_restriction?(:groups)
        user.in_group?(ticket.group_id) ||
          # this is to allow group changes for restricted agents that can_assign_to_any_group
          (user.abilities.defer?(:assign_to_any_group, ticket) && user.in_group?(ticket.group_id_was))
      elsif user.is_agent? && user.agent_restriction?(:organization)
        ticket.organization_id && user.has_organizations?(ticket.organization_id)
      else
        ticket.organization ? user.abilities.defer?(:view, ticket.organization) : false
      end
    end

    def only_create_ticket_comments?(ticket)
      # Restricted agents collaborating on the ticket may add/edit comments but not ticket properties depending on their restriction classification.
      if user.is_restricted_agent? && ticket.is_collaborator?(user)
        if user.agent_restriction?(:assigned)
          (user.id != ticket.assignee_id)
        elsif user.agent_restriction?(:organization)
          !user.has_organizations?(ticket.organization_id)
        elsif user.agent_restriction?(:groups)
          !user.in_group?(ticket.group)
        end
      elsif !user.can?(:edit, ticket)
        light_agent_has_restricted_access?(ticket)
      end
    end

    def mark_solved?(ticket)
      ticket.present? &&
        !ticket.solved? &&
        ticket.assignee.present? &&
        ticket.ticket_type_id != 3 &&
        user.account_id == ticket.account_id &&
        (user.is_agent? || user == ticket.requester)
    end

    def keep_solved?(ticket)
      ticket.present? &&
        ticket.solved? &&
        user.account_id == ticket.account_id &&
        (user.is_agent? || user == ticket.requester)
    end

    def rate_satisfaction?(ticket)
      ticket.present? &&
        user.account.present? &&
        (user.is_end_user? || agent_as_end_user_can_rate_satisfaction?(ticket)) &&
        !user.is_anonymous_user? &&
        user.account.has_customer_satisfaction_enabled? &&
        ticket.requested_by?(user) &&
        (ticket.ever_solved? || (ticket.satisfaction_score > SatisfactionType.UNOFFERED)) &&
        !ticket.closed?
    end

    def view_satisfaction?(ticket)
      ticket.present? && (user.is_agent? || ticket.requested_by?(user))
    end

    def add_collaborator?(ticket)
      if user.account.has_follower_and_email_cc_collaborations_enabled?
        add_follower?(ticket) || add_email_cc?(ticket)
      else
        if !user.account.is_collaboration_enabled?
          false
        elsif user.account.is_collaborators_addable_only_by_agents?
          user.is_agent?
        else
          true
        end
      end
    end

    def add_follower?(ticket)
      if user.account.has_follower_and_email_cc_collaborations_enabled?
        user.account.has_ticket_followers_allowed_enabled? && user.is_agent?
      else
        add_collaborator?(ticket)
      end
    end

    def add_email_cc?(ticket)
      if user.account.has_follower_and_email_cc_collaborations_enabled?
        user.account.has_comment_email_ccs_allowed_enabled?
      else
        add_collaborator?(ticket)
      end
    end

    def edit_attribute_values?
      user.is_admin? || agent_can_update_attribute_values?
    end

    protected

    def agent_as_end_user_can_rate_satisfaction?(ticket)
      Arturo.feature_enabled_for?(:agent_as_end_user, ticket.account) &&
        ticket.agent_as_end_user_for?(user)
    end

    def light_agent_has_restricted_access?(ticket)
      if !user.is_light_agent?
        false
      elsif user.permission_set.has_ticket_restriction?(:none)
        true
      elsif user.permission_set.has_ticket_restriction?(:groups) && user.in_group?(ticket.group)
        true
      elsif user.permission_set.has_ticket_restriction?(:organization) && ticket.organization_id && user.has_organizations?(ticket.organization_id)
        true
      end
    end

    def can_merge_ticket?(ticket)
      if !user.abilities.defer?(:view, ticket)
        false
      elsif ticket.new_record?
        false
      elsif ticket.solved?
        false
      elsif ticket.shared_tickets.any?
        false
      elsif user.abilities.defer?(:only_create_ticket_comments, ticket)
        false
      else
        true
      end
    end

    def collaboration_or_email_ccs_enabled?
      user.account.is_collaboration_enabled? || user.account.has_comment_email_ccs_allowed_enabled?
    end

    private

    def agent_can_update_attribute_values?
      user.is_agent? &&
      !user.is_light_agent? &&
      ticket_skills_editable_by_all?
    end

    def ticket_skills_editable_by_all?
      user.account.settings.edit_ticket_skills_permission ==
        Zendesk::
        Accounts::
        SettingsControllerSupport::
        MANAGE_TICKET_SKILLS_PERMISSION[:EDITABLE_BY_ALL]
    end
  end
end
