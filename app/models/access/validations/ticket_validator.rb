module Access::Validations
  class TicketValidator
    RESTRICTED_AGENT_ATTRIBUTE_WHITELIST = %w[latest_recipients current_collaborators requester_id organization_id].freeze
    FLAGGED_ATTRIBUTE_WHITELIST = %w[latest_recipients status_id assignee_id].freeze
    CAN_COMMENT_ATTRIBUTE_WHITELIST = %w[status_id assignee_id].freeze

    def initialize(ticket, actor)
      @ticket = ticket
      @actor = actor
      @logger = ticket.application_logger
    end

    def validate
      check_edit_access
      check_assignee_write_access
      check_tag_access
      check_comment_access
      check_requester_access
    end

    def check_assignee_write_access
      if @ticket.assignee_id_changed? && @ticket.assignee_id.present? && @ticket.assignee.present? && !@ticket.assignee.can?(:edit, @ticket)
        @ticket.errors.add(:base, Api.error('txt.abilities.validations.ticket_validator.sorry_selected_assignee_not_allowed_to_edit', error: 'AccessDenied'))
      end
    end

    def check_tag_access
      if tags_changed_by_user? && !@actor.can?(:edit_tags, @ticket)
        @ticket.errors.add(:base, Api.error('txt.abilities.validations.ticket_validator.sorry_not_allowed_to_add_or_edit_tags', error: 'AccessDenied'))
      end
    end

    private

    # Inspect the tags as originally submitted by the user. The application may have added additional tags, which we should allow.
    def tags_changed_by_user?
      return false unless @ticket.current_tags_changed?
      return false if @ticket.attribute_changes_for_ability_validation.nil? || @ticket.attribute_changes_for_ability_validation.empty?

      set_tags        = @ticket.attribute_changes_for_ability_validation[:set_tags]
      remove_tags     = @ticket.attribute_changes_for_ability_validation[:remove_tags]
      additional_tags = @ticket.attribute_changes_for_ability_validation[:additional_tags]

      unless set_tags.nil?
        return true unless TagManagement.equals?(@ticket.current_tags_was, set_tags)
      end

      !remove_tags.blank? || !additional_tags.blank?
    end

    def check_edit_access
      return unless @actor.present?

      # TODO: remove restricted_agent_edit_access definition when email_restricted_agent_fix is rolled out
      restricted_agent_edit_access = if @ticket.account.has_email_restricted_agent_fix?
        only_allowed_attributes_changed?
      else
        @actor.is_agent? && !@actor.can?(:edit, @ticket) && allowed_attributes_changed?
      end
      allow_flagged_update = @ticket.flags_allow_comment? && (@ticket.changed.all? { |change| FLAGGED_ATTRIBUTE_WHITELIST.include?(change) })
      can_edit_and_publicly_comment_flag = can_edit_and_publicly_comment?

      # TODO: replace restricted_agent_edit_access with only_allowed_attributes_changed? when email_restricted_agent_fix is rolled out
      if @ticket.attributes_changed? && !(can_edit_and_publicly_comment_flag || restricted_agent_edit_access || allow_flagged_update)
        if @ticket.account.has_email_log_ticket_attributes_during_edit_access_check?
          @logger.info(
            "Ticket cannot be updated: #{@ticket.account.id}:#{@ticket.id} - "\
            "List of ticket attributes that have changed: #{@ticket.changes}"
          )

          @logger.info(
            "Ticket cannot be updated: #{@ticket.account.id}:#{@ticket.id} - "\
            "Flags: can_edit_and_publicly_comment: #{can_edit_and_publicly_comment_flag} - "\
            "restricted_agent_edit_access: #{restricted_agent_edit_access} - "\
            "allow_flagged_update: #{allow_flagged_update}"
          )
        end

        @ticket.errors.add(:base,
          Api.error(
            'txt.abilities.validations.ticket_validator.ticket_can_not_be_updated_by',
            user_name: ERB::Util.h(@actor.name),
            locale: @actor.translation_locale,
            error: 'AccessDenied'
          ))
      end
    end

    def can_edit_and_publicly_comment?
      @can_edit_and_publicly_comment ||= if @ticket.account.has_email_restricted_agent_fix?
        @actor.can?(:edit, @ticket) && @actor.can?(:publicly, Comment)
      else
        @actor.can?(:edit, @ticket)
      end
    end

    def check_comment_access
      return if @ticket.new_record?
      return if @ticket.comment.blank? || @ticket.comment.empty?
      return if @ticket.flags_allow_comment?

      unless @actor.can?(:add_comment, @ticket)
        @ticket.errors.add(:base, Api.error('txt.abilities.validations.ticket_validator.not_permission_to_add_comment', error: 'AccessDenied'))
      end

      if @ticket.comment.is_public && !@actor.can?(:publicly, Comment)
        @ticket.errors.add(:base, Api.error('txt.abilities.validations.ticket_validator.not_permission_add_public_comments', error: 'AccessDenied'))
      end
    end

    def check_requester_access
      return if @ticket.requester.blank?
      return unless @ticket.requester.new_record?

      if @actor && !@actor.can?(:create, @ticket.requester)
        @ticket.errors.add(:base, Api.error('txt.abilities.validations.ticket_validator.not_permission_to_create_users', error: 'AccessDenied'))
      end
    end

    # DEPRECATED: use only_allowed_attributes_changed? after email_restricted_agent_fix is rolled out
    def allowed_attributes_changed?
      changes = @ticket.changed - RESTRICTED_AGENT_ATTRIBUTE_WHITELIST
      unless changes.empty?
        @logger.info("#{@ticket.account.id}:#{@ticket.id} Unpermitted light_agent changes to #{changes.inspect}")
        return false
      end

      allowed_collaborator_change? && allowed_requester_change? && !@ticket.sharing_agreements_changed? ? true : false
    end

    def only_allowed_attributes_changed?
      return true if can_edit_and_publicly_comment?
      return true if @ticket.new_record?
      return false unless @actor.is_agent?

      disallowed_changes = @ticket.changed - RESTRICTED_AGENT_ATTRIBUTE_WHITELIST
      disallowed_changes -= CAN_COMMENT_ATTRIBUTE_WHITELIST if restricted_agent_allowed_to_comment?
      unless disallowed_changes.empty? || agent_can_edit?
        @logger.info("#{@ticket.account.id}:#{@ticket.id} Disallowed light_agent changes to #{disallowed_changes.inspect}")
        return false
      end

      allowed_collaborator_change? && allowed_requester_change? && !@ticket.sharing_agreements_changed?
    end

    def restricted_agent_allowed_to_comment?
      !@actor.is_light_agent? && @actor.can?(:add, Comment)
    end

    def agent_can_edit?
      @agent_can_edit ||= @ticket.account.has_email_restricted_agent_fix? && @actor.is_agent? && @actor.can?(:edit, @ticket)
    end

    # TODO: When removing the :email_restricted_agent_fix Arturo, replace the
    # contents of this method with the current contents of the
    # #normalize_collaborators_v2 method below.
    def normalize_collaborators(value)
      value.to_s.scan(/<([^>]*)>/).sort
    end

    # TODO: When removing the :email_restricted_agent_fix Arturo, delete
    # this method.
    def normalize_collaborators_v2(value)
      value.to_s.scan(/<([^>]*)>/).sort.flatten.map(&:downcase)
    end

    def allowed_collaborator_change?
      # ZD#358649 lotus is sorting ccs alphabetically
      return true unless @ticket.changed.include?("current_collaborators")
      # Restricted agents can change followers but not email_ccs
      return true if @ticket.only_follower_collaborations_changed?

      ccs_equal = if @ticket.account.has_email_restricted_agent_fix? && @ticket.account.is_collaboration_enabled?
        # TODO: When removing the :email_restricted_agent_fix Arturo, replace
        # calls to #normalize_collaborators_v2 in the two lines below with calls
        # to #normalize_collaborators:
        ccs_were = normalize_collaborators_v2(@ticket.current_collaborators_was)
        ccs_are  = normalize_collaborators_v2(@ticket.current_collaborators)

        actor_email_addresses = @actor.identities.email.pluck(:value)

        # This ensures that agents who can only comment private, such as
        # light agents, can add themselves as legacy CCs on tickets when the
        # legacy CCs feature is enabled.
        (ccs_were == ccs_are) || (ccs_were == ccs_are - actor_email_addresses)
      else
        ccs_were = normalize_collaborators(@ticket.current_collaborators_was)
        ccs_are  = normalize_collaborators(@ticket.current_collaborators)

        ccs_were == ccs_are
      end

      @logger.info("#{@ticket.account.id}:#{@ticket.id} Disallowed light_agent changes to collaborators from #{ccs_were.inspect} to #{ccs_are.inspect}") unless ccs_equal
      ccs_equal
    end

    def allowed_requester_change?
      # Custom role agents that can edit ticket fields but cannot comment publicly can change the current requester
      return true if agent_can_edit? && !@actor.is_light_agent?
      # light agents can only change the requester if they are the current requester
      return true unless @ticket.changed.include?("requester_id")
      original_requester = @ticket.delta_changes["requester_id"].first
      requesters_equal = original_requester == @actor.id
      @logger.info("#{@ticket.account.id}:#{@ticket.id} Disallowed light_agent changes to #{@ticket.changed.inspect}") unless requesters_equal
      requesters_equal
    end
  end
end
