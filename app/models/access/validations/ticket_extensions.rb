module Access::Validations
  module TicketExtensions
    #
    # This provides an mass assignment-like interface for declaring certain user inputs
    # as requiring access control checks. The was originally implemented for ticket tags,
    # which may or may not be set by the user but always must me allowed to be manipulated
    # by the application code itself.
    #
    def self.included(base)
      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)

      base.validate :validate_actor_attribute_changes
      base.send(:attr_reader, :attribute_changes_for_ability_validation)

      base.prepend InstanceMethods
    end

    module ClassMethods
      #
      # Set the class that will apply permission validations to the model when it is saved.
      #
      #   class Ticket
      #     attribute_permission_validator Abilities::Validations::TicketValidator
      #   end
      #
      # The validator is a class that is defined as following:
      #
      #   class TicketValidator
      #     def initialize(ticket, actor)
      #     end
      #
      #     def validate
      #     end
      #   end
      #
      # The #validate method is run during the object's validation callback. It has context of
      # both the acting user and the object being manipulated itself.
      #
      def attribute_permission_validator(validator_klass = nil)
        (@attribute_permission_validator = validator_klass) if validator_klass
        @attribute_permission_validator
      end

      #
      # Defines attributes to be tracked through the model's lifecycle. This will provide the
      # values (as supplied to #attributes= or #update_attributes!) in the validator for inspection.
      #
      #   class Ticket
      #     attr_enterprise :set_tags, :additional_tags
      #   end
      #
      #   @ticket.attribute_changes_for_ability_validation
      #   => { :set_tags => "abc def" }
      #
      #  This is useful when determining if a value changed at the hands of the user or at the hands of
      #  the application itself.
      #
      def attr_enterprise(*args)
        enterprise_attributes.concat([*args])
      end

      def enterprise_attributes
        @enterprise_attributes ||= []
      end
    end

    module InstanceMethods
      def validate_actor_attribute_changes
        self.class.attribute_permission_validator.new(self, current_user).validate
      end

      def attributes=(attributes, *_args)
        unless attributes.nil?
          @attribute_changes_for_ability_validation = {}
          self.class.enterprise_attributes.each do |attr|
            unless attributes[attr].nil?
              @attribute_changes_for_ability_validation[attr] = attributes[attr]
            end
          end
        end

        super attributes
      end
    end
  end
end
