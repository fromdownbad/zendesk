module Access::Permissions
  module Rule
  end
end

require 'access/permissions/rule/automation_permission'
require 'access/permissions/rule/macro_permission'
require 'access/permissions/rule/role_permission'
require 'access/permissions/rule/trigger_permission'
require 'access/permissions/rule/rule_category_permission'
require 'access/permissions/rule/user_view_permission'
require 'access/permissions/rule/view_permission'
