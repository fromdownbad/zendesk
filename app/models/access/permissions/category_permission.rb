module Access::Permissions
  class CategoryPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.permission_set.permissions.forum_access == 'full' &&
        user.account.subscription.has_categorized_forums?
    end

    def read?
      user.subscription.has_categorized_forums?
    end
  end
end
