module Access::Permissions
  class OrganizationMembershipPermission
    include Access::OrganizationMembershipCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      return false if user.is_light_agent?
      user.is_agent? || user.permission_set.permissions.edit_organizations?
    end
  end
end
