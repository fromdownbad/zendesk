module Access::Permissions
  class CmsPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.permission_set.permissions.manage_dynamic_content?
    end
  end
end
