module Access::Permissions
  class ReportPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      permissions.report_access == 'full'
    end

    def view?
      ['full', 'readonly'].include?(permissions.report_access)
    end

    protected

    def permissions
      user.permission_set.permissions
    end
  end
end
