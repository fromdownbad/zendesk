module Access::Permissions
  class CommentPermission
    delegate :view?, :use_liquid_in?, to: :event_policy

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def add?
      ['public', 'private'].include?(user.permission_set.permissions.comment_access)
    end

    def publicly?
      user.permission_set.permissions.comment_access == 'public'
    end

    # From event ability
    def edit?(event)
      if user.is_admin?
        event.is_public?
      elsif user.is_agent?
        event.is_public? && (event.author_id == user.id)
      end
    end

    def make_comment_private?
      user.is_agent? && !user.is_light_agent?
    end

    protected

    def event_policy
      @event_policy ||= Access::Policies::EventPolicy.new(user)
    end
  end
end
