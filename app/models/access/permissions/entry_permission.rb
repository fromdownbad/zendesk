module Access::Permissions
  class EntryPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage_pinned_order?
      manage?
    end

    def manage?
      user.permission_set.permissions.forum_access == 'full'
    end

    def edit?(entry)
      if entry.forum.nil? || !user.abilities.defer?(:view, entry.forum)
        false
      elsif entry.submitter_id.nil?
        true
      elsif access_to_edit_topics?
        true
      elsif entry.submitter_id != user.id
        false
      elsif entry.forum.is_locked?
        false
      else
        true
      end
    end

    def watch?(entry)
      user.abilities.defer?(:watch, entry.forum)
    end

    def view?(entry)
      user.abilities.defer?(:view, entry.forum)
    end

    def reply_to?(entry)
      (user_can_view_forum?(entry) && !entry.is_locked?) || manage?
    end

    def access_to_edit_topics?
      ['edit-topics', 'full'].include?(user.permission_set.permissions.forum_access)
    end

    alias_method :list_voters?,      :access_to_edit_topics?
    alias_method :list_subscribers?, :access_to_edit_topics?
    alias_method :reorder?,          :access_to_edit_topics?
    alias_method :moderate?,         :access_to_edit_topics?

    def user_can_view_forum?(entry)
      return unless entry.forum
      user.abilities.defer?(:view, entry.forum)
    end
  end
end
