module Access::Permissions
  class GroupPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.permission_set.permissions.edit_organizations?
    end

    def view?
      user.is_agent?
    end
  end
end
