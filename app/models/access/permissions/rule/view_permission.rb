module Access::Permissions
  module Rule
    class ViewPermission < RolePermission
      def view?
        permission != 'playonly'
      end

      private

      def group_edit?(view)
        return false unless view.owner_type == 'Group'

        if view.account.has_multiple_group_views_reads?
          user_groups, non_user_groups = group_ids(view).partition do |group_id|
            user.group_ids.include?(group_id)
          end

          return user_groups.count <= 1 && non_user_groups.none?
        end

        user.group_ids.include?(view.owner_id)
      end

      def permission
        user.permission_set.permissions.view_access
      end

      def group_ids(view)
        return view.group_owner_ids if view.group_owner_ids.present?

        view.group_ids.any? ? view.group_ids : [view.owner_id]
      end
    end
  end
end
