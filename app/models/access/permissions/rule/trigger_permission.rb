module Access::Permissions
  module Rule
    class TriggerPermission
      def initialize(user)
        @user = user
      end

      def edit?
        user.permission_set.permissions.business_rule_management?
      end

      def view?(trigger = nil)
        edit? || trigger.try(:is_active)
      end

      protected

      attr_reader :user
    end
  end
end
