module Access::Permissions
  module Rule
    class MacroPermission < RolePermission
      def view?
        permission != 'readonly'
      end

      private

      def group_edit?(macro)
        return false unless macro.owner_type == 'Group'

        matching_groups, other_groups = group_ids(macro).partition do |group_id|
          user.group_ids.include?(group_id)
        end

        matching_groups.one? && other_groups.none?
      end

      def permission
        user.permission_set.permissions.macro_access
      end

      def group_ids(macro)
        macro.group_owner_ids.present? ? macro.group_owner_ids : macro.group_ids
      end
    end
  end
end
