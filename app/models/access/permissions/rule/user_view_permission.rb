module Access::Permissions
  module Rule
    class UserViewPermission < Rule::RolePermission
      def view?
        user.permission_set.permissions.available_user_lists == 'all'
      end

      private

      def group_edit?(user_view)
        user.group_ids.include?(user_view.owner_id)
      end

      def permission
        user.permission_set.permissions.user_view_access
      end
    end
  end
end
