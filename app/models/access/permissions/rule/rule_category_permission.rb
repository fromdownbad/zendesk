module Access::Permissions
  module Rule
    class RuleCategoryPermission
      def initialize(user)
        @user = user
      end

      def edit?
        user.permission_set.permissions.business_rule_management?
      end

      def view?
        edit?
      end

      protected

      attr_reader :user
    end
  end
end
