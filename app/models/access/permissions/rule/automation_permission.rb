module Access::Permissions
  module Rule
    class AutomationPermission
      def initialize(user)
        @user = user
      end

      def edit?
        user.permission_set.permissions.business_rule_management? &&
          user.account.has_unlimited_automations?
      end

      def view?(automation = nil)
        edit? || automation.try(:is_active)
      end

      protected

      attr_reader :user
    end
  end
end
