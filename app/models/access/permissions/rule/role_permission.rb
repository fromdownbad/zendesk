module Access::Permissions
  module Rule
    class RolePermission
      def initialize(user)
        @user = user
      end

      def edit?(rule)
        case permission
        when 'manage-personal'
          user == rule.owner
        when 'manage-group'
          user == rule.owner || group_edit?(rule)
        when 'full'
          true
        else
          false
        end
      end

      def manage_personal?
        %w[manage-personal manage-group full].include?(permission) &&
          user.account.subscription.has_personal_rules?
      end

      def manage_group?
        %w[manage-group full].include?(permission) &&
          user.account.subscription.has_group_rules?
      end

      def manage_shared?
        permission == 'full'
      end

      protected

      attr_reader :user

      private

      def group_edit?(*)
        fail 'Must define in subclass'
      end

      def permission
        fail 'Must define in subclass'
      end
    end
  end
end
