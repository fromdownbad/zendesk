module Access::Permissions
  class UserPermission
    include Access::UserCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit_password?(_other_user)
      false
    end

    def modify?
      false
    end

    def list?
      permissions.available_user_lists == "all"
    end

    def search?
      permissions.available_user_lists == "all"
    end

    def permissions
      user.permission_set.permissions
    end

    def lookup?
      true
    end

    def view?(_other_user)
      true
    end

    def manually_verify_identities_of?
      true
    end

    def edit_notes?(other_user)
      user.abilities.defer? :edit, other_user
    end

    def create?(_other_user)
      false
    end

    def manage_identities_of?(other_user)
      user.abilities.defer?(:edit, other_user)
    end

    def manage_external_accounts_of?(_other_user)
      false
    end

    def create_new?
      false
    end

    def delete?(_other_user)
      false
    end

    def assume?(_other_user)
      false
    end

    def edit_chat_display_name?(other_user)
      user.is_admin? || (user.id == other_user.id)
    end

    def edit_zopim_identity?
      user.is_admin?
    end
  end

  # "readonly"
  class ReadOnlyUserPermission < UserPermission
    def edit?(other_user)
      if user.id == other_user.id
        can_update?(other_user)
      else
        false
      end
    end
  end

  # "edit-within-org"
  class EditWithinOrgUserPermission < UserPermission
    def edit?(other_user)
      if user.id == other_user.id
        can_update?(other_user)
      else
        # Allow a user to edit anyone in any of their organizations, or if their org is blank, edit any other blank user
        # TL;DR Treat no org like another org
        other_user.is_end_user? && ((user.organizations & other_user.organizations).any? || other_user.organization == user.organization)
      end
    end

    def create?(other_user)
      user.abilities.defer? :edit, other_user
    end

    def create_new?
      true
    end

    def assume?(other_user)
      if user.id == other_user.id
        false
      else
        user.abilities.defer?(:edit, other_user)
      end
    end
  end

  # "edit", "full"
  class FullUserPermission < UserPermission
    def edit?(other_user)
      return false if role_change_denied?(other_user)
      if other_user.foreign?
        false
      elsif other_user.answer_bot?
        false
      elsif user.id == other_user.id
        can_update?(other_user)
      elsif other_user.is_end_user?
        true
      else
        can_edit_non_end_user?(other_user)
      end
    end

    def create?(other_user)
      user.abilities.defer? :edit, other_user
    end

    def create_new?
      true
    end

    def delete?(other_user)
      user.abilities.defer? :edit, other_user
    end

    def assume?(other_user)
      if user.id == other_user.id
        false
      else
        user.abilities.defer?(:edit, other_user)
      end
    end
  end
end
