module Access::Permissions
  class ChatPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def accept?
      user.permission_set.permissions.chat_availability_access? && !user.is_light_agent?
    end
  end
end
