module Access::Permissions
  # BusinessRules, Trigger, Automation
  class SettingsPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.permission_set.permissions.business_rule_management?
    end
  end

  # Extensions
  class ExtensionsPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.permission_set.permissions.extensions_and_channel_management?
    end
  end
end
