module Access::Permissions
  class PostPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.permission_set.permissions.forum_access == 'full'
    end

    def view?(post)
      user.abilities.defer?(:view, post_forum(post))
    end

    def edit?(post)
      if !user.abilities.defer?(:view, post_forum(post))
        false
      elsif can_edit_topics?
        true
      elsif post.entry.is_locked?
        false
      else
        post.user_id.nil? || post.user_id == user.id
      end
    end

    def post_forum(post)
      post.forum ? post.forum : post.entry.forum
    end

    protected

    def can_edit_topics?
      manage? || user.permission_set.permissions.forum_access == 'edit-topics'
    end
  end
end
