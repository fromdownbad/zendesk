module Access::Permissions
  class MembershipPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.is_admin? || user.permission_set.permissions.edit_organizations?
    end

    def view?
      user.is_agent?
    end

    def list?
      user.is_agent?
    end
  end
end
