module Access::Permissions
  class OrganizationPermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.permission_set.permissions.edit_organizations?
    end

    def edit_notes?
      user.permission_set.permissions.edit_organizations?
    end

    def view?(organization)
      not_organization_restricted = !organization_restricted?(user)

      user.is_agent? &&
        (not_organization_restricted || user.has_organizations?(organization.id))
    end

    def list?
      not_organization_restricted = !organization_restricted?(user)

      user.is_agent? && not_organization_restricted
    end

    def watch?(organization)
      user.is_end_user? &&
        user.can?(:view, organization) &&
        organization.is_shared?
    end

    protected

    def readonly_restricted?(user)
      # A readonly restricted agent can see any organization
      # but cannot list organizations
      user.permission_set.permissions.end_user_profile == "readonly"
    end

    def organization_restricted?(user)
      # An organization restricted agent only sees their own organization
      # and cannot list organizations
      user.permission_set.permissions.end_user_profile == "edit-within-org"
    end
  end
end
