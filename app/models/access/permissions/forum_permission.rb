module Access::Permissions
  class ForumPermission
    include Access::ForumCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.permission_set.permissions.forum_access == 'full'
    end

    def view?(forum)
      access_restricted_content? || user_belongs_to_forum_organization?(forum)
    end

    def add_topic?(forum)
      if !user.abilities.defer?(:view, forum)
        false
      elsif can_edit_topics?
        true
      else
        !forum.is_locked?
      end
    end

    def access_restricted_content?
      user.permission_set.permissions.forum_access == 'full' ||
        user.permission_set.permissions.forum_access_restricted_content?
    end

    def list_subscribers?
      ['edit-topics', 'full'].include?(user.permission_set.permissions.forum_access)
    end

    def moderate?
      user.is_admin? || user.is_moderator? || user.is_non_restricted_agent?
    end

    protected

    def can_edit_topics?
      manage? || user.permission_set.permissions.forum_access == 'edit-topics'
    end
  end
end
