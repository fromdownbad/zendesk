module Access::Permissions
  class VoicePermission
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def read?
      user.permission_set.permissions.voice_availability_access? && !user.is_light_agent?
    end
  end

  class VoiceCallPermission < VoicePermission
    def accept?
      can_have_voice?
    end

    private

    def can_have_voice?
      return false if user.is_end_user?

      if user.account.voice_feature_enabled?(:user_seats)
        user_is_assignable?
      else
        return true unless user.permission_set.try(:permissions).present?
        user.permission_set.permissions.voice_availability_access? && user_is_assignable?
      end
    end

    def user_is_assignable?
      user.permission_set.permissions.ticket_editing?
    end

    def view_dashboard?
      dashboard_enabled = user.account.voice_feature_enabled?(:dashboard)

      dashboard_enabled && user.permission_set.permissions.voice_dashboard_access?
    end
  end
end
