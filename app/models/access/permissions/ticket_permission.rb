module Access::Permissions
  class TicketPermission
    include Access::TicketCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    # ?? via comment_exceptions
    # For consistency with the Policy interface
    def add_comment?(_ticket)
      true
    end

    def edit_tags?(ticket = nil)
      user.permission_set.permissions.edit_ticket_tags? || ticket.try(:requester) == user || light_agent_has_tag_access?(ticket) || ticket.try(:new_record?)
    end

    def edit_whitelisted_tags?(ticket = nil)
      edit_tags?(ticket) || tag_changes_whitelisted?(ticket)
    end

    def delete?
      user.permission_set.permissions.ticket_deletion?
    end

    def view_deleted?
      delete? && user.permission_set.permissions.view_deleted_tickets?
    end

    def bulk_merge?
      user.permission_set.permissions.ticket_merge? && collaboration_or_email_ccs_enabled?
    end

    def merge?(ticket)
      can_merge_ticket?(ticket) && user.permission_set.permissions.ticket_merge?
    end

    # Whether the user can be assigned to tickets in general. Does not
    # guarantee the ability to be assigned to a particular ticket.
    # For that, use can?(:edit, ticket)
    def be_assigned_to?
      user.is_active? && user.is_agent? && !user.is_system_user? && user.permission_set.permissions.ticket_editing?
    end

    def assign_to_any_group?
      user.permission_set.permissions.assign_tickets_to_any_group?
    end

    def edit?(ticket)
      if ticket.new_record?
        true
      elsif !user.can?(:view, ticket)
        false                         # Agents may not edit a ticket that they can't see.
      elsif user.permission_set.permissions.ticket_editing?
        true                          # Agents with edit permission may edit all tickets that they can see.
      elsif ticket.requester_id == user.id || ticket.requester_id_was == user.id
        true                          # Agents may edit all tickets that they have requested.
      elsif ticket.is_collaborator?(user)
        # Followers and CCs are cool too.
        true
      end
    end

    def edit_properties?(ticket)
      if ticket.new_record?
        true
      elsif ticket.requester_id == user.id
        true
      elsif user.abilities.defer?(:edit, ticket) && !user.abilities.defer?(:only_create_ticket_comments, ticket)
        user.permission_set.permissions.ticket_editing?
      end
    end

    def view_satisfaction_prediction?
      user.account.has_satisfaction_prediction_enabled? && user.permission_set.permissions.view_ticket_satisfaction_prediction?
    end

    def light_agent_has_tag_access?(ticket)
      ticket.present? && ticket.new_record? && user.is_light_agent?
    end

    def tag_changes_whitelisted?(ticket)
      return true unless ticket.try(:current_tags_delta_changed?)
      # gets the changes, whether adds or removes
      tags_old, tags_new = ticket.delta_changes["current_tags"].map { |tags| tags.to_s.split(" ") }
      symmetric_difference = (tags_old - tags_new) + (tags_new - tags_old)
      whitelisted_tags = user.account.custom_field_options.map(&:value) + user.account.field_checkboxes.map(&:tag)
      # whitelisted tag changes are for tags that have a custom field tag or checkbox entry
      (symmetric_difference - whitelisted_tags).empty?
    end

    def ticket_bulk_edit?
      user.permission_set.permissions.ticket_bulk_edit?
    end

    def create_side_conversation?
      user.permission_set.permissions.side_conversation_create?
    end
  end
end
