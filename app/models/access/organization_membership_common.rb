module Access::OrganizationMembershipCommon
  def view?(organization_membership)
    user.is_agent? || organization_membership.user_id == user.id
  end

  def list?
    user.is_agent?
  end
end
