module Access::Policies
  class EventPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def view?(event)
      event.present? && (user.account_id == event.account_id) && (user.is_agent? || event.is_public?)
    end

    def use_liquid_in?
      user.is_agent?
    end
  end
end
