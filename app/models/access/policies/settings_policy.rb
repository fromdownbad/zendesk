module Access::Policies
  class SettingsPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.is_admin?
    end
  end
end
