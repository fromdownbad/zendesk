module Access::Policies
  class UserPolicy
    include Access::UserCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def modify?
      false
    end

    def create_new?
      user.is_admin? || user.account.agents_can_create_users
    end

    def lookup?
      user.is_agent?
    end

    def search?
      user.is_agent?
    end

    def list?
      user.is_agent? && !user.is_restricted_agent?
    end

    def edit_notes?
      user.is_agent? && !user.is_restricted_agent?
    end

    def edit_chat_display_name?(other_user)
      user.is_admin? || (user.id == other_user.id)
    end

    def edit_zopim_identity?
      user.is_admin?
    end

    def view?(other_user)
      ((user.id == other_user.id) && can_see_own_profile?) || user.is_agent?
    end

    def create?(other_user)
      return false if role_change_denied?(other_user)
      if user.is_end_user?
        # Only allowed as part of a ticket creation, when
        # 1) the ticket and the requester are being created at the same time,
        # and only if ticket.current_user == ticket.requester, OR
        # 2) an anonymous user is creating a ticket and a new user at the same time.
        user.is_anonymous_user? || (user.new_record? && (user == other_user))
      elsif other_user.is_end_user?
        if user.account.has_restrict_user_policy?
          user.is_non_restricted_agent?
        else
          user.is_non_restricted_agent? ||
            !user.agent_restriction?(:organization) ||
            user.has_organizations?(other_user.organization_ids_with_new)
        end
      else
        can_edit_non_end_user?(other_user)
      end
    end

    def edit?(other_user)
      return false if role_change_denied?(other_user)
      if other_user.foreign?
        false
      elsif other_user.answer_bot?
        false
      elsif user.id == other_user.id
        can_update?(other_user)
      elsif other_user.is_end_user?
        user.is_non_restricted_agent?
      else
        can_edit_non_end_user?(other_user)
      end
    end

    def delete?(other_user)
      if other_user.foreign?
        if other_user.is_end_user?
          user.is_non_restricted_agent?
        else
          user.is_admin?
        end
      else
        user.abilities.defer? :edit, other_user
      end
    end

    def assume?(other_user)
      if user.id == other_user.id
        false
      else
        user.abilities.defer?(:edit, other_user)
      end
    end

    def manage_identities_of?(other_user)
      if other_user.login_allowed?(:remote)
        user.is_agent?
      else
        user.abilities.defer?(:edit, other_user)
      end
    end

    def manage_external_accounts_of?(other_user)
      user.abilities.defer?(:edit, other_user)
    end

    def manually_verify_identities_of?(_other_user)
      user.is_agent?
    end

    # Applies only to other users. Everyone can edit their own password.
    def edit_password?(other_user)
      user.id == other_user.id
    end
  end
end
