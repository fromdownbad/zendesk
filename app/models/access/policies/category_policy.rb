module Access::Policies
  class CategoryPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.is_admin? && user.account.subscription.has_categorized_forums?
    end

    def read?
      user.subscription.has_categorized_forums?
    end
  end
end
