module Access::Policies
  class PostPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.is_admin?
    end

    def view?(post)
      return unless post_forum = post_forum(post)
      user.abilities.defer?(:view, post_forum)
    end

    def edit?(post)
      if user.is_anonymous_user?
        false
      elsif !user.abilities.defer?(:view, post)
        false
      elsif user.is_moderator?
        true
      elsif post.entry.is_locked?
        false
      elsif post.user_id.nil?
        true
      else
        post.user_id == user.id
      end
    end

    protected

    def post_forum(post)
      post.try(:forum) ? post.forum : post.try(:entry).try(:forum)
    end
  end
end
