module Access::Policies
  class VoicePolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def read?
      user.is_agent?
    end

    # ? Why does the permission version have a write? defined
    def write?
      user.is_admin?
    end
  end

  class VoiceCallPolicy < VoicePolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def accept?
      user.is_agent?
    end

    def view_dashboard?
      if user.account.has_lotus_feature_voice_staff_service_roles?
        if user.account.has_lotus_feature_voice_agent_view_dashboard?
          user.is_admin? || user.is_agent?
        else
          user.is_admin?
        end
      else
        user.is_admin? || user.is_agent?
      end
    end
  end
end
