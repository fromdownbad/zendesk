module Access::Policies
  class EntryPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def reply_to?(entry)
      manage? || (!user.is_anonymous_user? && !entry.is_locked?)
    end

    def watch?(entry)
      user.abilities.defer?(:watch, entry.forum)
    end

    def manage_pinned_order?
      manage?
    end

    def manage?
      user.is_admin?
    end

    def list_voters?
      user.is_moderator? && user.is_non_restricted_agent?
    end

    def list_subscribers?
      user.is_moderator? && user.is_non_restricted_agent?
    end

    def reorder?
      user.is_moderator?
    end

    def moderate?
      user.is_moderator?
    end

    def view?(entry)
      user.abilities.defer?(:view, entry.forum)
    end

    def edit?(entry)
      if user.is_anonymous_user?
        false
      elsif entry.forum.nil? || !user.abilities.defer?(:view, entry.forum)
        false
      elsif entry.submitter_id.nil?
        true
      elsif user.is_moderator?
        true
      elsif entry.submitter_id != user.id
        false
      elsif entry.forum.is_locked?
        user.is_non_restricted_agent?
      else
        true
      end
    end
  end
end
