module Access::Policies
  class OrganizationMembershipPolicy
    include Access::OrganizationMembershipCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.is_agent?
    end
  end
end
