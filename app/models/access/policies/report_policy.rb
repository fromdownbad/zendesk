module Access::Policies
  class ReportPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.is_admin?
    end

    def view?
      user.is_non_restricted_agent?
    end
  end
end
