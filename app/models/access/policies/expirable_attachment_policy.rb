module Access::Policies
  class ExpirableAttachmentPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def view?(attachment)
      expirable_attachment_permissions(user, attachment)
    end

    protected

    def expirable_attachment_permissions(user, attachment)
      case attachment.created_via
      when CcsAndFollowers::UpdateRequesterTargetRulesJob.to_s
        user.is_admin?
      when *ImportExportJobPolicy::EXPORT_JOB_CLASS_NAMES
        export_configuration = Zendesk::Export::Configuration.new(user.account)
        # ZD:250361 as per Pierre, the correct permission is can?(:manage, Report) for accessing csv report urls
        # this means you need to have full/manage access to even click on the Export tab
        user.abilities.defer?(:manage, Report) && export_configuration.whitelisted?(user)
      else
        user.is_agent?
      end
    end
  end
end
