module Access::Policies
  class ChatPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def accept?
      user.is_agent?
    end
  end
end
