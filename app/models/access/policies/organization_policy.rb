module Access::Policies
  class OrganizationPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.is_admin?
    end

    def edit_notes?
      user.is_non_restricted_agent?
    end

    def view?(organization)
      if user.is_agent?
        if view_all?
          true
        else
          user.has_organizations?(organization.id)
        end
      elsif user.is_end_user?
        if view_all_owned?
          user.has_organizations?(organization.id)
        else
          user.organizations.shared.where(id: organization.id).exists?
        end
      end
    end

    def view_all?
      if user.is_agent?
        !user.agent_restriction?(:organization)
      elsif user.is_end_user?
        false
      end
    end

    def view_all_owned?
      if user.is_agent?
        true
      elsif user.is_end_user?
        user.end_user_restriction?(:organization)
      end
    end

    def list?
      user.is_agent? && !user.agent_restriction?(:organization)
    end

    def watch?(organization)
      user.is_end_user? && user.can?(:view, organization)
    end
  end
end
