module Access::Policies
  class RulePolicy
    def initialize(user)
      @user = user
    end

    def view?
      user.is_agent?
    end

    def edit?
      user.is_admin?
    end

    protected

    attr_reader :user
  end

  class TriggerPolicy < RulePolicy
    def view?(trigger = nil)
      edit? || trigger.try(:is_active)
    end
  end

  class RuleCategoryPolicy < RulePolicy; end

  class AutomationPolicy < RulePolicy
    def edit?
      super && user.account.has_unlimited_automations?
    end

    def view?(automation = nil)
      edit? || automation.try(:is_active)
    end
  end

  # Macros, Views and UserViews
  class RoleEnabledRulePolicy < RulePolicy
    def edit?(rule = nil)
      return false unless user.is_agent?
      user.is_admin? || (rule && user == rule.owner)
    end

    def manage_personal?
      user.is_agent? && user.account.subscription.has_personal_rules?
    end

    def manage_group?
      user.is_admin? && user.account.subscription.has_group_rules?
    end

    def manage_shared?
      user.is_admin?
    end
  end

  class MacroPolicy < RoleEnabledRulePolicy; end

  class ViewPolicy < RoleEnabledRulePolicy
    def edit?(rule = nil)
      super(rule) && user.account.has_unlimited_views?
    end

    def manage_personal?
      super && user.account.has_unlimited_views?
    end

    def manage_group?
      super && user.account.has_unlimited_views?
    end

    def manage_shared?
      super && user.account.has_unlimited_views?
    end
  end

  class UserViewPolicy < RoleEnabledRulePolicy
    def view?
      super && user.is_non_restricted_agent?
    end

    def edit?(rule = nil)
      super(rule) && user.is_non_restricted_agent?
    end

    def manage_personal?
      super && user.is_non_restricted_agent?
    end
  end
end
