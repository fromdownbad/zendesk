module Access::Policies
  class AttachmentPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def view?(attachment)
      case attachment.source
      when Comment, ChatEvent
        comment_permissions(attachment)
      when Post
        user.abilities.defer?(:view, attachment.source)
      when Entry
        user.abilities.defer?(:view, attachment.source.forum)
      when UploadToken
        if attachment.source.target_id && attachment.source.target_type
          # inline image uploads don't get added to comments
          # and remain as UploadToken, so they should respect the
          # account setting - https://github.com/zendesk/zendesk/pull/18370
          if attachment.inline? && !attachment.account.settings.private_attachments?
            true
          # If account has Agent Workspace enabled, and uploaded by the agent
          # the target type will be Ticket to ensure that the attachment is viewable during an ongoing chat
          # Once the chat ends, the permission should follow ChatEvent's since the upload tokens are attached there
          elsif attachment.source.target_type == 'Ticket' && attachment.account.has_polaris?
            true
          # If account has Agent Workspace enabled, and uploaded by the visitor
          # the target type will be User and is a system user to ensure that the attachment is viewable during an ongoing chat
          # Once the chat ends, the permission should follow ChatEvent's since the upload tokens are attached there
          elsif attachment.source.target_type == 'User' && attachment.source.target.is_system_user? && attachment.account.has_polaris?
            true
          else # minor security leak, when an anonymous user's attachment is just uploaded but unattached, it's globally viewable
            # If an account no longer has_polaris, it will still have attachments with target_type 'Ticket' so treat them like User attachments
            !user.is_anonymous_user? && ['User', 'Ticket'].include?(attachment.source.target_type)
          end
        else
          !attachment.account.has_view_attachments_without_target_and_private_attachments_on? ||
          !attachment.account.settings.private_attachments? ||
          !user.is_anonymous_user?
        end
      when nil
        if attachment.source_type == 'Comment'
          # archived ticket likely.
          ticket = attachment.account.tickets.where(id: attachment.ticket_id).first_with_archived(stub_only: true)
          comment_permissions(attachment, ticket)
        else
          # orphaned record, punt and allow
          true
        end
      else
        true
      end
    end

    def create?
      return true if user.is_agent?

      permission_fix_preflight_logging_create_method

      if user.account.has_sse_prevent_anonymous_uploads?
        return false unless user.account.is_attaching_enabled?
        return false if user.is_anonymous_user? && (user.account.settings.private_attachments? || !user.account.is_open?)

        true
      else
        user.account.is_attaching_enabled? && !(user.is_anonymous_user? && user.account.settings.private_attachments?)
      end
    end

    def delete?(attachment)
      case attachment.source
      when Entry       then user.abilities.defer?(:edit, attachment.source)
      when UploadToken then user.is_agent?
      else
        false
      end
    end

    protected

    def comment_permissions(attachment, ticket = nil)
      return false if attachment.source.nil? && ticket.nil?

      ticket ||= attachment.source.ticket
      ticket ||= attachment.account.tickets.where(id: attachment.source.ticket_id).first_with_archived(stub_only: true)

      permission_fix_preflight_logging(attachment, ticket)

      if ticket.nil? then false
      elsif has_permission_fix_and_user_is_agent?(user, attachment) && user.can?(:view, ticket) then true
      elsif has_permission_fix_and_user_is_agent?(user, attachment) && !user.account.settings.private_attachments? then true
      elsif has_permission_fix_and_user_is_agent?(user, attachment) then false
      # When cleaning up the sse_agent_attachment_view_policy Arturo, keep the line above this, and remove the line below
      elsif user.is_agent? then true
      elsif user.id == ticket.requester_id then true
      elsif user.id && ticket.is_collaborator?(user) then true
      elsif ticket.organization.present? && user.abilities.defer?(:view, ticket.organization) then true
      # Attachments bound to messaging channels will remain temporarily public
      # until Product has figured out how to allow end-users to access their attachments privately
      elsif attachment.source.is_a?(ChatEvent) && ticket.via_id != ViaType.CHAT then true
      else
        !user.account.settings.private_attachments?
      end
    end

    # rubocop:disable Naming/PredicateName
    def has_permission_fix_and_user_is_agent?(user, attachment)
      attachment.account.has_sse_agent_attachment_view_policy? && user.is_agent?
    end
    # rubocop:enable Naming/PredicateName

    def permission_fix_preflight_logging_create_method
      return unless user.is_anonymous_user?
      return unless user.account.is_attaching_enabled?
      return unless user.account.has_sse_prevent_anonymous_uploads_log_only?

      tags = if user.account.settings.private_attachments?
        ["outcome:cannot_upload", "reason:private_attachments_enabled"]
      elsif !user.account.is_open?
        ["outcome:cannot_upload", "reason:anonymous_tickets_not_allowed"]
      else
        ["outcome:can_upload", "reason:correct_settings"]
      end

      Rails.logger.info("[Agent attachment policy upload] #{tags.join(', ')}")
      statsd_client.increment('agent_policy_change.upload', tags: tags)
    end

    def permission_fix_preflight_logging(attachment, ticket = nil)
      return if ticket.nil?
      return unless user.is_agent? && user.account.has_sse_agent_attachment_view_policy_log_only?

      tags = if user.can?(:view, ticket)
        ["outcome:can_view", "reason:can_view_ticket"]
      elsif !user.account.settings.private_attachments?
        ["outcome:can_view", "reason:private_attachments_not_enabled"]
      else
        ["outcome:cannot_view", "reason:inadequate_permissions"]
      end

      Rails.logger.info("[Agent attachment policy] #{tags.join(', ')}, attachment_id: #{attachment.id}, ticket_id: #{ticket.id}")
      statsd_client.increment('agent_policy_change', tags: tags)
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'attachments')
    end
  end
end
