module Access::Policies
  class ForumPolicy
    include Access::ForumCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.is_admin?
    end

    def view?(forum)
      if user.is_anonymous_user?
        forum.visibility_restriction.everybody? && forum.current_tags.empty?

      elsif user.is_agent?
        !user.agent_restriction?(:organization) || user_belongs_to_forum_organization?(forum)

      elsif !user.is_agent?
        !forum.visibility_restriction.agents_only? &&
          forum.matches_user_tags?(user) &&
          user_belongs_to_forum_organization?(forum)
      end
    end

    def add_topic?(forum)
      if user.is_non_restricted_agent? || user.is_moderator?
        true
      else
        !forum.is_locked? && user.abilities.defer?(:view, forum)
      end
    end

    def access_restricted_content?
      user.is_agent? && !user.agent_restriction?(:organization)
    end

    def list_subscribers?
      user.is_moderator? && user.is_non_restricted_agent?
    end
  end
end
