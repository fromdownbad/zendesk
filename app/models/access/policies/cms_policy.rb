module Access::Policies
  class CmsPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def manage?
      user.is_admin?
    end
  end
end
