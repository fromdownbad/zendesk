module Access::Policies
  class CommentPolicy
    delegate :view?, :use_liquid_in?, to: :event_policy

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def add?
      true
    end

    def publicly?
      user.is_admin? || user.is_end_user? || !user.is_private_comments_only
    end

    def edit?(event)
      if user.is_admin?
        event.is_public?
      elsif user.is_agent?
        event.is_public? && (event.author_id == user.id)
      end
    end

    def make_comment_private?
      user.is_agent?
    end

    protected

    def event_policy
      @event_policy ||= Access::Policies::EventPolicy.new(user)
    end
  end
end
