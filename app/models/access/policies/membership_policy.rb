module Access::Policies
  class MembershipPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit?
      user.is_admin?
    end

    def view?
      user.is_agent?
    end

    def list?
      user.is_agent?
    end
  end
end
