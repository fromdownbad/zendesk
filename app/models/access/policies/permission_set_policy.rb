module Access::Policies
  class PermissionSetPolicy
    attr_reader :user

    def initialize(user)
      @user = user
    end

    # being used/checked in app/helpers/people/roles_helper.rb by if current_user.can?(:clone, permission_set)
    def clone?(permission_set)
      user.is_admin? && !permission_set.is_system_role?
    end

    def edit?(permission_set)
      user.is_admin? && [::PermissionSet::Type::CUSTOM, ::PermissionSet::Type::LIGHT_AGENT].include?(permission_set.role_type)
    end

    def delete?(permission_set)
      user.is_admin? && !(permission_set.new_record? || permission_set.is_system_role?)
    end
  end
end
