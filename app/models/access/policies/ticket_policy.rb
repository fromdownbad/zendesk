module Access::Policies
  class TicketPolicy
    include Access::TicketCommon

    attr_reader :user

    def initialize(user)
      @user = user
    end

    def edit_tags?
      user.is_agent?
    end

    def edit_whitelisted_tags?
      user.is_agent?
    end

    def add_comment?(ticket)
      user.can?(:edit, ticket)
    end

    def delete?
      user.is_admin? || (user.is_agent? && user.account.settings.ticket_delete_for_agents? && !user.account.has_permission_sets?)
    end

    def view_deleted?
      user.is_admin? || (user.is_non_restricted_agent? && view_deleted_tickets_for_agents_enabled?)
    end

    def bulk_merge?
      user.is_agent? && collaboration_or_email_ccs_enabled?
    end

    def ticket_bulk_edit?
      user.is_agent?
    end

    def create_side_conversation?
      user.is_agent?
    end

    def merge?(ticket)
      user.is_agent? && can_merge_ticket?(ticket)
    end

    # Whether the user can be assigned to tickets in general. Does not
    # guarantee the ability to be assigned to a particular ticket.
    # For that, use can?(:edit, ticket)
    def be_assigned_to?
      user.is_active? && user.is_agent? && !user.is_system_user?
    end

    def assign_to_any_group?
      user.is_non_restricted_agent?
    end

    def edit?(ticket)
      if ticket.new_record?
        true
      elsif user.is_agent?
        user.can?(:view, ticket)       # Agents may edit any ticket that they can see.
      elsif user.foreign?
        true                           # perhaps should become: ticket.shared_tickets.  TSTODO Ticket sharing end-users may edit all tickets
      elsif ticket.requester_id == user.id
        true                           # Non-agents may edit tickets that they have requested.
      elsif ticket.organization &&
        ticket.organization.is_shared_comments? &&
        user.organizations.include?(ticket.organization)
        true                           # May edit if belongs to same org and org has shared_comments enabled?
      elsif ticket.comment && ticket.comment.via_twitter?
        user.has_twitter_identity?     # Twitter replies (without CCs) are allowed to edit the ticket
      elsif ticket.comment && ticket.comment.via_facebook?
        user.has_facebook_identity?    # Facebook replies are allowed to edit the ticket
      elsif ticket.comment && ticket.comment.via_any_channel?
        user.has_any_channel_identity? # AnyChannel replies are allowed to edit the ticket
      elsif ticket.is_collaborator?(user)
        true                           # Followers and CCs are cool too.
      end
    end

    def edit_properties?(ticket)
      (user.abilities.defer?(:edit, ticket) &&
       !user.abilities.defer?(:only_create_ticket_comments, ticket)) ||
       ticket.requested_by?(user)
    end

    def view_satisfaction_prediction?
      user.is_agent? && user.account.has_satisfaction_prediction_enabled?
    end

    protected

    def view_deleted_tickets_for_agents_enabled?
      # Both settings should be enabled
      user.account.settings.ticket_delete_for_agents? &&
        user.account.settings.view_deleted_tickets_for_agents?
    end
  end
end
