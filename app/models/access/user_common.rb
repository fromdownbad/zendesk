module Access
  module UserCommon
    def set_agent_display_name?(other_user) # rubocop:disable Naming/AccessorMethodName
      user.account.has_agent_display_names? && other_user.is_agent? &&
        (user.is_admin? || user.id == other_user.id)
    end

    def request_password_reset?(other_user)
      if !sso_disabled_or_has_password?(other_user) && !other_user.allowed_to_login_with_password?
        false
      elsif user.id != other_user.id && user.is_admin?
        true
      elsif user.is_agent? && other_user.is_end_user? && !other_user.account.admins_can_set_user_passwords && can_modify_enterprise_user?(user)
        true
      elsif user.id == other_user.id && can_change_own_password?
        true
      else
        false
      end
    end

    def set_password?(other_user) # rubocop:disable Naming/AccessorMethodName
      if !user.is_admin? || !other_user.account.admins_can_set_user_passwords
        false
      elsif !other_user.login_allowed?(:remote) || user.is_account_owner? || !other_user.is_admin? || user.id == other_user.id
        true
      else
        false
      end
    end

    def create_password?(other_user)
      if sso_disabled_or_has_password?(other_user)
        false
      elsif user.is_admin? && user.id != other_user.id
        true
      elsif user.is_agent? && other_user.is_end_user? && !other_user.account.admins_can_set_user_passwords && can_modify_enterprise_user?(user)
        true
      else
        false
      end
    end

    def change_password?(other_user)
      if user.id != other_user.id || user.crypted_password.blank?
        false
      elsif user.is_admin? && (!user.login_allowed?(:remote) || user.account.admins_can_set_user_passwords)
        true
      elsif user.is_agent? && sso_disabled_or_has_password?(other_user)
        true
      elsif user.is_end_user? && can_change_own_password? && !user.login_allowed?(:remote)
        true
      end
    end

    def verify_now?(other_user)
      if other_user.is_account_owner?
        false
      else
        user.is_agent?
      end
    end

    def send_verification_email?(_other_user)
      user.is_agent?
    end

    def edit_agent_forwarding?(other_user)
      other_user.can?(:accept, ::Voice::Call)
    end

    # Non restricted Agents can only modify tags of end users
    # Admins can modify tags for anyone but owners and foreign users
    def modify_user_tags?(other_user)
      if user.is_end_user?
        false
      elsif user.account.owner == other_user && user != other_user
        false
      elsif other_user.foreign?
        false
      elsif user.is_admin?
        true
      elsif other_user == user # Trying to modify own tags
        false
      elsif other_user.is_end_user? && user.can?(:edit, other_user)
        true
      else
        false
      end
    end

    protected

    def can_see_own_profile?
      if user.is_end_user?
        user.account.is_end_user_profile_visible?
      else
        true
      end
    end

    def can_edit_non_end_user?(other_user)
      if other_user.is_zendesk_admin?
        user.is_zendesk_admin?
      elsif other_user.foreign?
        false
      else
        user.is_admin? && !other_user.is_account_owner?
      end
    end

    def sso_disabled_or_has_password?(user)
      !user.login_allowed?(:remote) || user.crypted_password.present?
    end

    def can_modify_enterprise_user?(user)
      user.account.subscription.plan_type != SubscriptionPlanType.ExtraLarge
    end

    def can_change_own_password?
      if user.is_agent?
        user.login_allowed?(:remote)
      elsif user.is_end_user?
        user.account.is_end_user_password_change_visible?
      else
        false
      end
    end

    def can_update?(other_user)
      user.is_admin? || (user.permissions == other_user.permissions && can_see_own_profile?)
    end

    def role_change_denied?(other_user)
      role_changes?(other_user) && !can_change_role?
    end

    def can_change_role?
      user.is_admin?
    end

    def role_changes?(other_user)
      other_user.changes.include?('roles')
    end

    def manage_people?(other_user)
      edit?(other_user) || (user.account.has_can_manage_people_permission_check? && user.can_manage_people?)
    end
  end
end
