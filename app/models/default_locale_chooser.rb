class DefaultLocaleChooser
  def self.choose(locale_name)
    return ENGLISH_BY_ZENDESK if locale_name.blank?
    TranslationLocale.end_user_available.find_by_locale(locale_name)
  end
end
