class GetSatisfactionTopic < ActiveRecord::Base
  has_many :linkings, as: :external_link
  belongs_to :account

  validates_presence_of :topic_id, :account_id

  attr_accessible :account, :topic_id

  EXTERNALLY_VISIBLE_PARAMS = [:topic_id].freeze

  def self.find_from_params(params)
    normalized_params = params.symbolize_keys

    find_by_topic_id(normalized_params[:topic_id])
  end

  def ticket_callback(ticket)
  end
end
