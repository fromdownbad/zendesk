class SimplifiedEmailRuleBody < ActiveRecord::Base
  self.table_name = :simplified_email_rule_bodies

  belongs_to :account
  belongs_to :rule, foreign_key: :rule_id

  validates_presence_of :account_id, :rule_id

  attr_accessible

  has_soft_deletion default_scope: true
end
