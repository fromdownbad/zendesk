require 'httparty'

class PushNotifications::WebhookClient
  include HTTParty
  default_timeout 10

  format :json

  def initialize(webhook_url, options = {})
    self.class.base_uri webhook_url
    self.class.headers('Content-Type' => 'application/json')
    @statsd_tags = []
    @statsd_tags += options[:statsd_tags] if options[:statsd_tags].present?
  end

  def push(device_ids, payload)
    device_identifiers = PushNotifications::DeviceIdentifier.find(device_ids)
    skipped = []
    devices = device_identifiers.map do |device|
      if device_type = device_type(device)
        {
          "identifier" => device.token,
          "type" => device_type
        }
      else
        skipped << device
        nil
      end
    end

    @statsd_tags << "device_count:#{devices.compact.count}"
    Rails.logger.info("SDK Devices: #{devices}\nSkipped: #{skipped.map(&:id)}")

    if devices.any?
      request_body = {
        "devices" => devices.compact,
        "notification" => {
          "body" => payload[:msg],
          "title" => payload[:msg_short],
          "ticket_id" => payload[:ticket_id]
        }
      }
      in_silence(request_body) do
        Rails.logger.info("sending SDK webhook notification to URL: #{self.class.base_uri} with body: #{request_body}")
        self.class.post("", body: request_body.to_json)
      end
    end
  end

  private

  def device_type(device)
    if device.ios?
      "ios"
    elsif device.android?
      "android"
    end
  end

  def in_silence(*args, &block)
    response = yield block
    log_stats(["http_status:#{response.code}", "status:success"])
    response
  rescue Timeout::Error, EOFError, SocketError, Errno::ECONNRESET, Errno::EHOSTUNREACH => e
    log_stats(["status:failure"])
    Rails.logger.warn("#{self.class.name} #{e.class} error #{e.message}: #{args.map(&:inspect).join(", ")}")
    raise if e.is_a?(SocketError) && e.to_s !~ /Name or service not known/
    nil
  end

  def log_stats(tags)
    tags += @statsd_tags
    statsd_client = Zendesk::StatsD::Client.new(namespace: ["jobs", "push_notifications"])
    statsd_client.increment('webhook_client', tags: tags)
  rescue
    Rails.logger.error("could not send push_notifications jobs data to statsd")
  end
end
