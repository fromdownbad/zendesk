class PushNotifications::UrbanAirshipPayloadV3 < PushNotifications::UrbanAirshipPayloadBase
  def initialize
    @headers = {'Accept' => 'application/vnd.urbanairship+json; version=3;', 'Content-Type' => 'application/json'}
  end

  def build_ios_push_body(devices, payload, _options = {})
    user_id, msg, badge, ticket_id = {badge: '+1'}.merge(payload).values_at(:user_id, :msg, :badge, :ticket_id)
    alert = truncate_ios_message(user_id, msg, badge, ticket_id)

    {
      audience: build_audience(devices, :ios_channel, :device_token),
      device_types: [:ios],
      notification: {
        ios: {
          badge: badge,
          alert: alert,
          sound: "notification.aif",
          extra: {id: user_id, tid: ticket_id}
}
      }
    }
  end

  def build_android_push_body(devices, payload, options = {})
    alert, tid = payload.values_at(:msg_short, :ticket_id)
    message = gcm_push_payload(alert, tid)

    message[:collapse_key] = Time.now.to_f.to_s if options[:enable_collapse_key]

    {
      audience: build_audience(devices, :android_channel, :apid),
      device_types: [:android],
      notification: {android: truncate_android_payload(message)}
    }
  end

  def build_windows_push_body(devices, payload, _options = {})
    alert, tid = payload.values_at(:msg_short, :ticket_id)
    message = wns_push_payload(alert, tid)

    {
      audience: build_audience(devices, :wns, :wns),
      device_types: [:wns],
      notification: { wns: truncate_windows_payload(message, tid) }
    }
  end

  private

  def build_audience(devices, _channel_key, _non_channel_key)
    audience = devices.group_by { |device| urban_airship_audience_key(device) }
    audience.each do |key, values|
      audience[key] = values.map(&:token)
    end

    if audience.size > 1
      audience = {
        OR: audience.map { |k, v| {k => v} }
      }
    end

    audience
  end

  def urban_airship_audience_key(device)
    is_urban_airship_channel = device.token_type == PushNotifications::DeviceIdentifier::TOKEN_TYPE_UA_CHANNEL

    if device.android?
      is_urban_airship_channel ? :android_channel : :apid
    elsif device.windows?
      :wns
    else
      is_urban_airship_channel ? :ios_channel : :device_token
    end
  end
end
