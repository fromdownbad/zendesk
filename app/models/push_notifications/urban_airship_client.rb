require 'httparty'

class PushNotifications::UrbanAirshipClient
  include HTTParty
  default_timeout 10

  base_uri "https://go.urbanairship.com/api"
  format :json

  MINIMUM_TOKEN_LENGTH = 11

  attr_reader :version

  def initialize(app_key, master_secret, options = {})
    @master_auth = {username: app_key, password: master_secret}

    @version = 3
    @ua_payload = PushNotifications::UrbanAirshipPayloadV3.new
    self.class.headers @ua_payload.headers
    @statsd_tags = []
    @statsd_tags += options[:statsd_tags] if options[:statsd_tags].present?
  end

  def push(device_ids, payload, options = {})
    device_identifiers = PushNotifications::DeviceIdentifier.where(id: device_ids).to_a
    skipped_devices = []
    ios_devices = []
    android_devices = []
    windows_devices = []
    device_identifiers.each do |device|
      if valid_token(device.token)
        if device.ios?
          ios_devices << device
        elsif device.android?
          android_devices << device
        elsif device.windows?
          windows_devices << device
        else
          skipped_devices << device
        end
      else
        # invalid device token
        skipped_devices << device
      end
    end

    @statsd_tags << "device_count:#{device_identifiers.compact.count}"
    Rails.logger.info("Android devices: #{android_devices.map(&:id)}\nIOS Devices: #{ios_devices.map(&:id)} \
                      \nWindows Devices: #{windows_devices.map(&:id)}\nSkipped: #{skipped_devices.map(&:id)}")

    payload = payload.symbolize_keys
    results = []
    results << push_to_device_type(:android, android_devices, payload, options) unless android_devices.empty?
    results << push_to_device_type(:ios, ios_devices, payload, options) unless ios_devices.empty?
    results << push_to_device_type(:windows, windows_devices, payload, options) unless windows_devices.empty?
    results.flatten.compact
  end

  def register(identifier)
    update(identifier.token, identifier.device_type, identifier.registration_payload)
  end

  def unregister(device_token, device_type)
    self.class.delete(token_url_for(device_token, device_type), basic_auth: @master_auth)
  end

  def update(device_token, device_type, attributes)
    in_silence(device_token, device_type, attributes) do
      self.class.put(token_url_for(device_token, device_type), basic_auth: @master_auth, body: attributes.to_json)
    end
  end

  def tokens_deactivated_since(since)
    timestamp = URI.encode(since.iso8601)
    self.class.get("/device_tokens/feedback?since=#{timestamp}", basic_auth: @master_auth)
  end

  private

  def valid_token(token)
    token.length > MINIMUM_TOKEN_LENGTH
  end

  def in_silence(*args, &block)
    response = yield block
    log_stats(["http_status:#{response.code}", "status:success"])
    response
  rescue Timeout::Error, EOFError, SocketError, Errno::ECONNRESET => e
    log_stats(["status:failure"])
    Rails.logger.warn("#{self.class.name} #{e.class} error #{e.message}: #{args.map(&:inspect).join(", ")}")
    raise if e.is_a?(SocketError) && e.to_s !~ /Name or service not known/
    nil
  end

  def token_url_for(token, device_type)
    case device_type.downcase
    when 'android', 'windows'
      "/apids/#{token}"
    when 'ios', 'iphone', 'ipad'
      "/device_tokens/#{token}"
    else
      "/device_tokens/#{token}"
    end
  end

  def push_to_device_type(device_type, devices, payload, options = {})
    body = nil
    case device_type.downcase.to_sym
    when :android
      body = @ua_payload.build_android_push_body(devices, payload, options)
    when :ios
      body = @ua_payload.build_ios_push_body(devices, payload, options)
    when :windows
      body = @ua_payload.build_windows_push_body(devices, payload, options)
    end

    if body
      in_silence(devices, payload, options) do
        self.class.post('/push/', basic_auth: @master_auth, body: body.to_json)
      end
    else
      false
    end
  end

  def log_stats(tags)
    tags += @statsd_tags
    statsd_client = Zendesk::StatsD::Client.new(namespace: ["jobs", "push_notifications"])
    statsd_client.increment("urban_airship_client", tags: tags)
  rescue
    Rails.logger.error("could not send push_notifications jobs data to statsd")
  end
end
