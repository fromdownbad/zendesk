class PushNotifications::UrbanAirshipPayloadBase
  attr_reader :headers

  MAX_PAYLOAD_IOS = 256
  MAX_PAYLOAD_ANDROID = 1024
  MAX_PAYLOAD_WINDOWS = 5000
  ELLIPSIS = "\u2026".freeze

  protected

  ## Payload Creation

  def gcm_push_payload(alert, ticket_id)
    {alert: alert, extra: {tid: ticket_id.to_s}}
  end

  def apn_push_payload(user_id, badge, alert, ticket_id)
    {aps: {badge: badge, alert: alert, sound: "notification.aif"}, id: user_id, tid: ticket_id}
  end

  def wns_push_payload(alert, ticket_id)
    {
      toast: {
          binding: {
            template: "ToastText01",
            text: [alert, "", ticket_id.to_s]
          }
      }
    }
  end

  ## Payload Control

  def truncate_ios_message(user_id, msg_original, badge, ticket_id)
    msg = remove_whitespace(msg_original)
    max_msg_size = calc_max_msg_size_ios(user_id, badge, ticket_id)
    truncate(msg, max_msg_size)
  end

  def truncate_android_payload(message)
    msg = remove_whitespace(message[:alert])
    max_msg_size = calc_max_msg_size_android(message)
    message[:alert] = truncate(msg, max_msg_size)
    message
  end

  def truncate_windows_payload(message, ticket_id)
    msg = remove_whitespace(message[:toast][:binding][:text][0])
    max_msg_size = calc_max_msg_size_windows(ticket_id)
    message[:toast][:binding][:text][0] = truncate(msg, max_msg_size)
    message
  end

  def remove_whitespace(message)
    message.gsub(/(\r\n)+|(\n)+/m, " ")
  end

  def calc_max_msg_size_android(message)
    MAX_PAYLOAD_ANDROID - gcm_push_payload('', message[:extra][:tid]).to_json.bytesize
  end

  def calc_max_msg_size_ios(user_id, badge, ticket_id)
    MAX_PAYLOAD_IOS - apn_push_payload(user_id, badge, "", ticket_id).to_json.bytesize
  end

  def calc_max_msg_size_windows(ticket_id)
    MAX_PAYLOAD_WINDOWS - wns_push_payload("", ticket_id).to_json.bytesize
  end

  def truncate(msg, max_msg_size)
    msg = msg.truncate(max_msg_size)
    available_bytes = max_msg_size - serialization_overhead(msg)

    truncated_message = ''

    msg.each_char do |c|
      if (truncated_message + c).bytesize >= (available_bytes - 2)
        truncated_message = truncated_message.gsub(/\s\w+\s*$/, ELLIPSIS)
        break
      end
      truncated_message.concat(c)
    end
    truncated_message
  end

  def serialization_overhead(truncated)
    truncated.to_json.size - truncated.size - "".to_json.size
  end
end
