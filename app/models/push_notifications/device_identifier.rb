class PushNotifications::DeviceIdentifier < ActiveRecord::Base
  include Zendesk::Serialization::DeviceIdentifierSerialization

  TOKEN_TYPE_UA_CHANNEL      = 'urban_airship_channel_id'.freeze
  TOKEN_TYPE_UA_APID         = 'urban_airship_apid'.freeze
  TOKEN_TYPE_UA_DEVICE_TOKEN = 'urban_airship_device_token'.freeze
  TOKEN_TYPE_GCM             = 'gcm'.freeze
  TOKEN_TYPE_RAW             = 'raw'.freeze # This type represents device identifiers/tokens that are in the native format of the give platform. APNS tokens for Apple and GCM tokens for Android.
  TOKEN_TYPE_UA              = 'urban_airship'.freeze
  WINDOWS                    = 'windows'.freeze
  ANDROID                    = 'android'.freeze
  IPAD                       = 'ipad'.freeze
  IPHONE                     = 'iphone'.freeze

  TOKEN_TYPES = [TOKEN_TYPE_UA_CHANNEL, TOKEN_TYPE_UA_APID, TOKEN_TYPE_UA_DEVICE_TOKEN, TOKEN_TYPE_GCM, TOKEN_TYPE_RAW].freeze

  validates_presence_of :token, :device_type, :account_id
  validates_inclusion_of :token_type, in: TOKEN_TYPES, allow_nil: true
  validates_lengths_from_database only: :token

  belongs_to :account

  attr_accessible :token, :device_type, :account, :token_type

  scope :active, -> { where(active: true) }
  scope :ios, -> { where(device_type: [IPHONE, IPAD]) }
  scope :first_party, -> { where(type: 'PushNotifications::FirstPartyDeviceIdentifier') }
  scope :not_first_party, -> { where.not(type: 'PushNotifications::FirstPartyDeviceIdentifier') }

  before_validation :normalize_device_type

  def self.find_by_token(token)
    where(token: token.to_s).first
  end

  def self.find_by_token_or_id!(token = nil, record_id = nil)
    identifier = token ? find_by_token(token) : find_by_id(record_id)
    identifier || raise(ActiveRecord::RecordNotFound)
  end

  def gateway
    case token_type
    when TOKEN_TYPE_GCM
      TOKEN_TYPE_GCM
    when TOKEN_TYPE_RAW
      TOKEN_TYPE_RAW
    when TOKEN_TYPE_UA_CHANNEL, TOKEN_TYPE_UA_APID, TOKEN_TYPE_UA_DEVICE_TOKEN
      TOKEN_TYPE_UA
    end
  end

  def android?
    normalized_device_type == ANDROID
  end

  def ios?
    [IPHONE, IPAD].include?(normalized_device_type)
  end

  def windows?
    normalized_device_type == WINDOWS
  end

  def token_type
    read_attribute(:token_type) || (android? ? TOKEN_TYPE_UA_APID : TOKEN_TYPE_UA_DEVICE_TOKEN)
  end

  # TODO: Get rid of this entire section when migration of the data in the 'type' column has been
  # performed. This is because when type column of all recordrs gets set to
  # 'PushNotifications::FirstPartyDeviceIdentifier' we will get a proper instance which has the
  # mobile_app belongs_to association.
  def mobile_app
    @mobile_app ||= MobileApp.find(mobile_app_id)
  end

  private

  def normalized_device_type
    device_type.to_s.downcase
  end

  def normalize_device_type
    self.device_type = normalized_device_type if device_type
  end
end
