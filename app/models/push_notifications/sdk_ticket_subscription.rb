class PushNotifications::SdkTicketSubscription < ActiveRecord::Base
  self.table_name = 'mobile_sdk_push_notification_subscriptions'

  attr_accessible

  belongs_to :ticket
  belongs_to :device_identifier, class_name: "PushNotifications::DeviceIdentifier"
end
