require 'httparty'

class PushNotifications::GCMClient
  include HTTParty
  default_timeout 10

  base_uri 'https://gcm-http.googleapis.com/gcm'
  format :json

  def initialize(config)
    @api_key = config.fetch('api_key')
  end

  def push(device_ids, _payload, _options = {})
    devices = PushNotifications::DeviceIdentifier.where(id: device_ids).all

    response = self.class.post(
      '/send',
      headers: {
        'Content-Type' => 'application/json',
        'Authorization' => "key=#{@api_key}"
      },
      body: {
        registration_ids: devices.map(&:token)
      }.to_json
    )

    maintain_devices(devices, response['results'])

    response
  end

  def maintain_devices(devices, results)
    results.each_with_index do |result, index|
      device = devices[index]

      if result['error'] == 'NotRegistered'
        # the device has unregistered.
        device.destroy
      elsif result['registration_id']
        # the device got the message, but has an updated token we should use in the future.
        device.update_attribute(:token, result['registration_id'])
      end
    end
  end
end
