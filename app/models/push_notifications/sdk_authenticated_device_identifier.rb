# Represents push notifications device identifiers related to devices running mobile SDK
# applications authenticated via mobile SDK JWT authentication.

class PushNotifications::SdkAuthenticatedDeviceIdentifier < PushNotifications::DeviceIdentifier
  include PushNotifications::UrbanAirshipSdkRegistration

  validates :user, :mobile_sdk_app, presence: true
  validates :token, uniqueness: { scope: :user_id }

  belongs_to :user
  belongs_to :mobile_sdk_app, inherit: :account_id

  attr_accessible :user, :mobile_sdk_app

  # for compatibility with PushNotifications::UrbaAirshipClient
  def registration_payload
    {}
  end
end
