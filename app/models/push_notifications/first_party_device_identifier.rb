# Represents push notifications device identifiers related to devices running Zendesk first party
# mobile applications (Agent, Inbox).

class PushNotifications::FirstPartyDeviceIdentifier < PushNotifications::DeviceIdentifier
  validates :user, :mobile_app, presence: true
  validates :token, uniqueness: { scope: :user_id }

  belongs_to :user
  belongs_to :mobile_app

  attr_accessible :user, :mobile_app

  before_validation :set_account_from_user
  after_commit :register, on: :create
  after_commit :unregister, on: :destroy

  DEFAULT_MOBILE_APP_IDENTIFIER = 'com.zendesk.agent'.freeze

  def registration_payload
    {alias: "#{user.account.subdomain}:#{user.id}"}
  end

  def register
    return if token_type == TOKEN_TYPE_GCM # Registration happens only on the client side

    PushNotificationRegistrationJob.enqueue(
      account_id: user.account.id,
      operation: :register,
      mobile_app_identifier: mobile_app_identifier,
      identifier_id: id,
      token: token,
      device_type: device_type
    )
  end

  def unregister
    return if token_type == TOKEN_TYPE_GCM # Unregistration happens automatically in PushNotifications::GCMClient

    unless android?
      PushNotificationRegistrationJob.enqueue(
        account_id: account_id,
        operation: :unregister,
        mobile_app_identifier: mobile_app_identifier,
        token: token,
        device_type: device_type,
        sns_target_arn: sns_target_arn
      )
    end
  end

  def record_view!
    if TOKEN_TYPE_GCM == token_type
    elsif TOKEN_TYPE_RAW == token_type || account.has_sns_push_notifications?
      update_attribute(:badge_count, 0)
    else
      PushNotificationUpdateJob.enqueue(
        account_id: account_id,
        token: token,
        device_type: device_type,
        mobile_app_identifier: mobile_app_identifier || DEFAULT_MOBILE_APP_IDENTIFIER,
        badge: 0
      )
    end
  end

  def mobile_app_identifier
    mobile_app.try(:identifier)
  end

  def set_account_from_user
    self.account_id = user.account_id if user_id
  end
end
