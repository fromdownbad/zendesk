# Represents push notifications device identifiers related to devices running mobile SDK
# applications authenticated via mobile SDK anonymous authentication.

class PushNotifications::SdkAnonymousDeviceIdentifier < PushNotifications::DeviceIdentifier
  include PushNotifications::UrbanAirshipSdkRegistration

  validates :user_sdk_identity, :mobile_sdk_app, presence: true
  validates_uniqueness_of :token, scope: :user_identity_id

  belongs_to :user_sdk_identity, foreign_key: "user_identity_id", inherit: :account_id
  belongs_to :mobile_sdk_app, inherit: :account_id
  has_one :sdk_push_notification_subscription, class_name: "PushNotifications::SdkTicketSubscription", foreign_key: "device_identifier_id"
  has_one :ticket, through: :sdk_push_notification_subscription

  attr_accessible :user_sdk_identity, :mobile_sdk_app

  def subscribe_to_ticket(ticket)
    if ticket.requester == user_sdk_identity.user
      PushNotifications::SdkTicketSubscription.create do |sds|
        sds.ticket = ticket
        sds.device_identifier = self
      end
    end
  end

  # for compatibility with PushNotifications::UrbanAirshipClient
  def registration_payload
    {}
  end
end
