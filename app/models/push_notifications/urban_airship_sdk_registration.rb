module PushNotifications::UrbanAirshipSdkRegistration
  extend ActiveSupport::Concern

  included do
    after_commit :register, on: :create, if: :urban_airship_enabled?
    after_commit :unregister, on: :destroy, if: :urban_airship_enabled?
  end

  def register
    PushNotificationSdkRegistrationJob.enqueue(
      'account_id' => account_id,
      'mobile_sdk_app_id' => mobile_sdk_app.id,
      'identifier_id' => id
    )
  end

  def unregister
    PushNotificationSdkDeregistrationJob.enqueue(
      'account_id' => account_id,
      'mobile_sdk_app_id' => mobile_sdk_app.id,
      'identifier_id' => id,
      'device_type' => device_type
    )
  end

  def urban_airship_enabled?
    mobile_sdk_app &&
    mobile_sdk_app.settings.push_notifications_enabled &&
    mobile_sdk_app.settings.push_notifications_type == MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP
  end
end
