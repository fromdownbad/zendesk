class Brand < ActiveRecord::Base
  module HelpCenterSupport
    def self.included(base)
      base.class_eval do
        after_update :restrict_help_center_when_deactivated, if: :active_changed?
        after_soft_delete :delete_help_center
      end
    end

    def has_help_center? # rubocop:disable Naming/PredicateName
      help_center.present?
    end

    def help_center_in_use?
      has_help_center? && !help_center.archived?
    end

    def help_center_enabled?
      has_help_center? && help_center.enabled?
    end

    def help_center_state
      help_center.state if has_help_center?
    end

    def help_center_id
      help_center.id if has_help_center?
    end

    def restrict_help_center_when_deactivated
      if !active && has_help_center?
        help_center_api_client.connection.put(
          "/hc/api/v2/help_centers/#{help_center_id}.json",
          help_center: {state: 'restricted'}
        )
      end
    end

    def delete_help_center
      if has_help_center?
        help_center_api_client.connection.delete(
          "/hc/api/v2/help_centers/#{help_center_id}.json"
        )
      end
    end

    private

    def help_center
      @help_center ||= HelpCenter.find_by_brand_id(id)
    end

    def help_center_api_client
      Zendesk::InternalApi::Client.new(account.subdomain, user: "help_center")
    end
  end
end
