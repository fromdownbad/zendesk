class TicketLink < ActiveRecord::Base
  FOLLOWUP = 'Followup'.freeze
  MERGE = 'Merge'.freeze
  FACEBOOK = 'Facebook'.freeze
  TWITTER = 'Twitter'.freeze

  belongs_to :target, class_name: 'Ticket'
  belongs_to :source, class_name: 'Ticket'
  belongs_to :archived_source, class_name: 'TicketArchiveStub', foreign_key: :source_id
  belongs_to :account

  attr_accessible :target, :source, :link_type

  before_validation      :set_account
  before_validation      :ensure_link_type
  validates_presence_of  :target, :source, :account_id, :link_type

  def source_with_archived
    source || archived_source
  end

  private

  def set_account
    self.account = target.account
  end

  def ensure_link_type
    return if link_type.present?
    self.link_type = FOLLOWUP if target.via?(:closed_ticket)
  end
end
