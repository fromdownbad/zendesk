class ComplianceDeletionFeedback < ActiveRecord::Base
  self.table_name = 'compliance_deletion_feedback'

  belongs_to :account
  belongs_to :compliance_deletion_status
  belongs_to :user

  # valid states
  COMPLETE = 'complete'.freeze
  PENDING  = 'pending'.freeze
  REDO     = 'redo'.freeze

  STATES = [COMPLETE, PENDING, REDO].freeze

  VALID_APPLICATIONS = ComplianceDeletionStatus::APPLICATONS.freeze

  # Hash of valid applications and whether they are required or not
  APPLICATIONS_HASH = {}.tap do |h|
    VALID_APPLICATIONS.each do |app|
      h[app] = ComplianceDeletionStatus::APPLICATIONS_REQUIRING_FEEDBACK.include?(app)
    end
  end

  attr_accessible :account, :application, :compliance_deletion_status, :pod_id, :required, :state, :user

  validates :account_id,                    presence: true
  validates :application,                   presence: true, inclusion: { in: VALID_APPLICATIONS, message: "The application: '%<value>s' is not valid." }
  validates :compliance_deletion_status_id, presence: true
  validates :pod_id,                        presence: true
  validates :state,                         presence: true, inclusion: { in: STATES, message: "The state: '%<value>s' is not valid." }
  validates :user_id,                       presence: true

  validates_inclusion_of :required, in: [true, false]

  scope :complete,   -> { where(state: COMPLETE) }
  scope :incomplete, -> { where.not(state: COMPLETE) }
  scope :required,   -> { where(required: true) }

  class << self
    def create_feedback!(status:, applications: APPLICATIONS_HASH)
      ComplianceDeletionFeedback.connection.execute(batch_insert_query(status: status, applications: applications))
    end

    private

    def batch_insert_query(status:, applications: APPLICATIONS_HASH)
      <<-SQL
        INSERT INTO `#{table_name}`
          (
            `id`,
            `account_id`,
            `compliance_deletion_status_id`,
            `user_id`,
            `application`,
            `state`,
            `pod_id`,
            `required`,
            `created_at`,
            `updated_at`
          )
        VALUES
          #{query_values(status: status, applications: applications)}
      SQL
    end

    def query_values(status:, applications:)
      uids = generate_many_uids(applications.size)

      # rubocop:disable Style/MultilineBlockChain
      applications.map do |app, required|
        [
          uids.shift,
          status.account_id,
          status.id,
          status.user_id,
          "'#{app}'",
          "'#{PENDING}'",
          status.account.pod_id,
          required,
          'NOW()',
          'NOW()'
        ].join(',')
      end.map { |value| "(#{value})" }.join(',')
      # rubocop:enable Style/MultilineBlockChain
    end
  end

  def complete?
    state == COMPLETE
  end

  def complete!
    update_attribute(:state, COMPLETE)
  end

  def redo?
    state == REDO
  end

  def redo!
    update_attributes(pod_id: account.pod_id, state: REDO)
  end
end
