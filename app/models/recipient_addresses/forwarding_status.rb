# tested via test/models/recipient_address_test.rb
module RecipientAddresses
  module ForwardingStatus
    CODE_START = "ZFVTest:".freeze
    WAIT_TIMEOUT = 5.minutes
    TICKET_REFRESH_INTERVAL = 1.hour
    VERIFIED_BY_DEFAULT = Time.at(0)

    def forwarding_status
      if forwarding_verified?
        :verified
      elsif forwarding_sent_at
        if forwarding_sent_at > WAIT_TIMEOUT.ago
          :waiting
        else
          :failed
        end
      else
        :unknown
      end
    end

    def forwarding_verified?
      forwarding_always_verified? || (
        forwarding_verified_at && (
          !forwarding_sent_at || # default verified
          forwarding_verified_at >= forwarding_sent_at # verification emil came back
        )
      )
    end

    def verify_forwarding_status!
      verify_forwarding_status
      save!
    end

    def forwarding_verified!
      self.forwarding_verified_at = Time.now
      save!
    end

    def forwarding_code
      CODE_START + Zendesk::Verifier.generate(
        "recipient_addresses_id" => id,
        "sent_at" => forwarding_sent_at.to_i
      )
    end

    def self.verify_forwarding_code(account, text)
      text = text[/#{CODE_START}([-=\/\w+]+)/, 1]
      data = Zendesk::Verifier.verify(text)
      id = data.fetch("recipient_addresses_id")
      sent_at = data.fetch("sent_at")

      recipient_address = account.recipient_addresses.find(id)

      if recipient_address.forwarding_sent_at.to_i == sent_at
        recipient_address.forwarding_verified!
        "verified"
      else
        "unverified, sent at does not match #{recipient_address.forwarding_sent_at.to_s(:db)} vs #{Time.at(sent_at).to_s(:db)}"
      end
    rescue ActiveSupport::MessageVerifier::InvalidSignature, ActiveRecord::RecordNotFound
      Rails.logger.error("Forwarding status verification failed for account:#{account.id} #{text}")
      false
    end

    def ticket_received!
      return if forwarding_always_verified?

      if forwarding_verified? && forwarding_verified_at + TICKET_REFRESH_INTERVAL < Time.now
        update_column(:forwarding_verified_at, Time.now.utc)
      end
    end

    private

    def forwarding_always_verified?
      default_host? || creating_for_new_brand
    end

    def recently_retried?
      forwarding_sent_at &&
        forwarding_verified_at &&
        forwarding_sent_at > WAIT_TIMEOUT.ago
    end

    def verify_forwarding_status
      return if forwarding_always_verified?
      self.forwarding_sent_at = Time.now
      @send_verification_email = true
    end

    def send_verification_email
      if @send_verification_email
        @send_verification_email = false
        AccountsMailer.deliver_forwarding_status_verification(account, self)
      end
    end

    def validate_no_default_on_unverified
      if default? && !forwarding_verified?
        errors.add(
          :email, I18n.t("activerecord.errors.models.recipient_address.attributes.email.cannot_be_default_without_being_verified")
        )
      end
    end
  end
end
