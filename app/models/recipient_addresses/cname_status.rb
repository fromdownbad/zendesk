require 'resolv'

# tested via test/models/recipient_address_test.rb
module RecipientAddresses::CnameStatus
  CNAME_STATUS = {
    unknown: 0,
    failed: 1,
    verified: 2,
  }.freeze
  DNS_TIMEOUT = 3
  VALID_CNAME_STATUSES = [:verified].freeze
  VALID_HOSTS = ['zendesk1', 'zendesk2', 'zendesk3', 'zendesk4'].freeze

  def cname_status
    if trusted_domain?
      :verified
    else
      CNAME_STATUS.key(cname_status_id)
    end
  end

  def verify_cname_status!
    return if account.has_email_deprecate_cname_checks?

    verify_cname_status
    save!
  end

  def all_cnames_valid?
    return true if trusted_domain?

    if using_cname_verification?
      return false if cname_metadata.blank?

      all_records_verified?(:cname)
    else
      return false if mx_metadata.blank? || txt_metadata.blank?

      all_records_verified?(:mx) && all_records_verified?(:txt)
    end
  end

  def using_cname_verification?
    verification_method == "cname"
  end

  private

  def cname_metadata
    metadata&.dig("cname").to_h
  end

  def verify_cname_status
    status = trusted_domain? ? :verified : check_cname_status
    self.cname_status_id = CNAME_STATUS.fetch(status)
  end

  def check_cname_status
    cname_results = {}
    (1..4).each do |n|
      break if stubbed_cname_status?
      subdomain = "zendesk#{n}"
      lookup_domain = "#{subdomain}.#{domain}"

      dns_record = Resolv::DNS.open do |dns|
        dns.timeouts = DNS_TIMEOUT
        dns.getresources(lookup_domain, Resolv::DNS::Resource::IN::CNAME)
      end

      status = if dns_record.empty? || dns_record.first.name.to_s.downcase != "mail#{n}.#{Zendesk::Configuration.fetch(:host)}"
        :failed
      else
        :verified
      end

      cname_results[subdomain] = { "lookup_result": dns_record.present? ? dns_record.first.name.to_s : "", "status": status }
    end

    update_metadata("cname": cname_results)

    all_cnames_valid? ? :verified : :failed
  end

  def stubbed_cname_status?
    Rails.env.test?
  end
end
