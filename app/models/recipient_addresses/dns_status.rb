require 'resolv'

# tested via test/models/recipient_address_test.rb
module RecipientAddresses::DnsStatus
  DNS_STATUS = {
    unknown: 0,
    failed: 1,
    verified: 2,
  }.freeze

  def verify_dns_status!
    unless account.has_email_deprecate_cname_checks?
      verify_cname_status
      unless all_records_verified?(:cname)
        verify_spf_records
        verify_mx_status
      end
    end

    verify_dns_status
    save!
  end

  def domain_verified?
    return true if trusted_domain?

    (domain_metadata&.dig("status")&.to_sym || :failed) == :verified
  end

  def domain_verification_status
    domain_verified? ? :verified : :failed
  end

  private

  def domain_metadata
    metadata&.dig("domain_verification").to_h
  end

  def verify_dns_status
    status = trusted_domain? ? :verified : check_dns_status
    update_domain_verification_status(status)
  end

  def check_dns_status
    self.dns_checked_at = Time.now

    domain_verification_records.each do |record|
      return :verified if record.data == domain_verification_code
    end

    :failed
  end

  def update_domain_verification_status(verification_status)
    update_metadata("domain_verification": { "status" => verification_status })
  end

  def domain_verification_records
    if stubbed_txt_status?
      # avoid dns lookups in test, they can be super slow ... pretend everything is fine
      [OpenStruct.new(data: domain_verification_code)]
    else
      lookup_domain = domain.ends_with?('.') ? domain : "#{domain}."
      dns_records = Resolv::DNS.open do |dns|
        [dns.getresources("zendesk_verification.#{lookup_domain}", Resolv::DNS::Resource::IN::TXT),
         dns.getresources("zendeskverification.#{lookup_domain}", Resolv::DNS::Resource::IN::TXT)]
      end

      dns_records.flatten
    end
  rescue Resolv::ResolvTimeout => error
    Rails.logger.error("DNS verification timeout #{error}")
    []
  end

  def stubbed_txt_status?
    Rails.env.test?
  end
end
