require 'resolv'

# tested via test/models/recipient_address_test.rb
module RecipientAddresses::MxStatus
  DNS_TIMEOUT = 3
  VALID_MX_RECORDS = {
    "zendesk1" => "feedback-smtp.us-west-2.amazonses.com",
    "zendesk2" => "feedback-smtp.us-east-1.amazonses.com",
    "zendesk3" => "feedback-smtp.eu-west-1.amazonses.com",
    "zendesk4" => "feedback-smtp.eu-central-1.amazonses.com"
  }.freeze

  def mx_status
    if trusted_domain?
      :verified
    else
      all_mx_records_valid? ? :verified : :failed
    end
  end

  def verify_mx_status!
    verify_mx_status
    save!
  end

  def all_mx_records_valid?
    return true if trusted_domain?

    return false unless VALID_MX_RECORDS.all? { |host, _| mx_metadata.key?(host) }

    mx_metadata.all? { |_, value| value["status"] == "verified" }
  end

  private

  def mx_metadata
    metadata&.dig("mx").to_h
  end

  def verify_mx_status
    return if trusted_domain?

    mx_results = {}

    (1..4).each do |n|
      break if stubbed_mx_status?

      subdomain = "zendesk#{n}"
      lookup_domain = "#{subdomain}.#{domain}"
      dns_records = mx_records(lookup_domain)

      status = dns_records.each do |dns|
        break :verified if dns.exchange.to_s == VALID_MX_RECORDS[subdomain]
      end

      status = :failed unless status == :verified

      mx_results[subdomain] = { "status": status }
    end

    update_metadata("mx": mx_results)
  end

  def mx_records(lookup_domain)
    Resolv::DNS.open do |dns|
      dns.timeouts = DNS_TIMEOUT
      dns.getresources(lookup_domain, Resolv::DNS::Resource::IN::MX)
    end
  end

  def stubbed_mx_status?
    Rails.env.test?
  end
end
