require 'resolv'

# tested via test/models/recipient_address_test.rb
module RecipientAddresses::SpfStatus
  SPF_HOPS = 10 # per RFC
  SPF_STATUS = {
    unknown: 0,
    failed: 1,
    verified: 2,
    deprecated: 3,
  }.freeze
  SPF_DNS_TIMEOUT = 3
  VALID_SPF_STATUSES = [:verified, :deprecated].freeze
  VALID_HOSTS = ['zendesk1', 'zendesk2', 'zendesk3', 'zendesk4'].freeze

  def spf_status
    if trusted_domain?
      :verified
    else
      SPF_STATUS.key(spf_status_id)
    end
  end

  def verify_spf_status!
    verify_spf_status
    verify_spf_records
    save!
  end

  def spf_valid?
    VALID_SPF_STATUSES.include?(spf_status)
  end

  def all_txt_records_valid?
    return true if trusted_domain?

    return false unless VALID_HOSTS.all? { |host| txt_metadata.key?(host) }

    all_records_verified?(:txt)
  end

  private

  def txt_metadata
    metadata&.dig("txt").to_h
  end

  def verify_spf_status
    status = default_host? ? :verified : check_spf_status
    self.spf_status_id = SPF_STATUS.fetch(status)
  end

  def check_spf_status
    valid_hosts = ["mail.#{Zendesk::Configuration.fetch(:host)}"]
    deprecated_hosts = ["smtp.#{Zendesk::Configuration.fetch(:host)}", "support.#{Zendesk::Configuration.fetch(:host)}", "_spf.zdsys.com"]

    spf_records do |spf|
      break :verified if includes_spf?(spf, valid_hosts)
      break :deprecated if includes_spf?(spf, deprecated_hosts)
    end || :failed
  end

  def includes_spf?(spf_entries, domains)
    domains = "(#{domains.map { |h| Regexp.escape(h) }.join("|")})"
    spf_entries =~ /\s\+?include:#{domains}(\s|$)/i
  end

  def spf_records(lookup_domain = nil, &block)
    lookup_domain ||= domain
    hops = Array.new(SPF_HOPS).fill(1)
    resolve_spf_entries(lookup_domain, hops, &block)
    nil
  rescue Resolv::ResolvTimeout => e
    Rails.logger.error("spf verification timeout #{e}")
    nil
  end

  def resolve_spf_entries(domain, hops, &block)
    unless entries = dns_txt_records(domain).map(&:data).detect { |dns| dns.start_with?("v=spf1") }
      Rails.logger.error("dns lookup timed out or no valid spf records found for #{domain}")
      return
    end

    yield entries

    # resolve non-zendesk domains up to hops size
    included_domains = entries.scan(/\s\+?include:([\S]+)\b/).flatten
    while (domain = included_domains.shift) && hops.pop
      resolve_spf_entries(domain, hops, &block)
    end
  end

  def dns_txt_records(domain)
    if stubbed_spf_status?
      # avoid dns lookups in test, they can be super slow ... pretend everything is fine
      [OpenStruct.new(data: "v=spf1 include:mail.#{Zendesk::Configuration.fetch(:host)} ~all")]
    else
      lookup_domain = domain.ends_with?('.') ? domain : "#{domain}."

      Resolv::DNS.open do |dns|
        dns.timeouts = SPF_DNS_TIMEOUT
        dns.getresources(lookup_domain, Resolv::DNS::Resource::IN::TXT)
      end
    end
  end

  def verify_spf_records
    return if zendesk_owned_domain_and_account?

    txt_results = {}

    (1..4).each do |n|
      subdomain = "zendesk#{n}"
      lookup_domain = "#{subdomain}.#{domain}"
      valid_hosts = ["mail#{n}.#{Zendesk::Configuration.fetch(:host)}"]

      status = spf_records(lookup_domain) do |spf|
        break :verified if includes_spf?(spf, valid_hosts)
      end || :failed

      txt_results[subdomain] = { "status": status.to_s }
    end

    update_metadata("txt": txt_results)
  end

  # extra method for easy stubbing
  def stubbed_spf_status?
    Rails.env.test?
  end
end
