class StoresBackfillAudit < ActiveRecord::Base
  attr_accessible

  validates_presence_of :account_id, :model_class, :region, :max_id, :last_id, :total_evaluated, :total_updated
  validates :model_class, uniqueness: { scope: :account_id }
  validates :max_id, numericality: { greater_than_or_equal_to: 0 }
  validates :last_id, numericality: { greater_than_or_equal_to: 0 }
  validate :last_id_sanity

  scope :for_account_id, ->(account_id) { where(account_id: account_id) }
  scope :unfinished, ->(*_args) { where('last_id != 0') }
  scope :done, ->(*_args) { where('last_id = 0') }

  # Factory to set up a new audit
  def self.new_audit(account_id, model_class, region)
    audit = StoresBackfillAudit.new
    audit.account_id = account_id
    audit.model_class = model_class
    audit.region = region
    audit.reset_progress
    audit.save!
    audit
  end

  # Takes an account-id and a map of { model_class => region } and
  # returns an array of audits corresponding to the model_classes for the account
  # (ie: there should be 1 audit per model_class in given map, each with
  # the corresponding region for each model_class)
  #
  # This will:
  # 1) create a new audit for any model_class in given map that does not exist
  #    in db
  # 2) Change the region and reset any audit that has a different region than
  #    the one to which the model_class is mapped in the given
  # 3) Return the existing audit untouched if it exists and has correct region
  #
  # In the case of 1) or 2), the new/adjusted audit will be saved to the db.
  #
  def self.account_audits(account_id, model_classes)
    existing_audits = where(account_id: account_id).to_a.
      map { |audit| [audit.model_class, audit] }.
      to_h

    model_classes.map do |mc, rgn|
      audit = existing_audits[mc]
      if audit.nil?
        audit = new_audit(account_id, mc, rgn)
      elsif !correct_stores?(audit.region, rgn)
        audit.reset_progress(rgn)
        audit.save!
      end

      audit
    end
  end

  def self.correct_stores?(existing, desired)
    stores_diff(existing, desired).empty? && stores_diff(desired, existing).empty?
  end

  def self.stores_diff(one, two)
    scoped_stores = two.map { |store| Technoweenie::AttachmentFu::ScopedStores.linked_store_name(store) }

    one - two - scoped_stores
  end
  private_class_method :stores_diff

  def last_id_sanity
    if last_id > max_id + 1
      errors.add(:last_id, "is out of range")
    end
  end

  # Returns region as an array of symbols (it's persisted as a comma-delim string)
  def region
    @region_list ||= build_region_list
  end

  def region=(region_list)
    @region_list = nil
    if region_list.nil? || region_list.empty?
      write_attribute(:region, nil)
      return
    end
    write_attribute(:region, region_list.map(&:to_s).join(','))
  end

  # Class object associated with this audit's model_class
  def assoc_model_class
    @model_class_obj ||= model_class.constantize
  end

  def model_has_thumbnails?
    assoc_model_class.column_names.include?("parent_id")
  end

  def update_progress(total_eval, total_upd, new_last_id)
    raise ArgumentError, "last_id (#{new_last_id}) must be g.t.e 0" if new_last_id < 0
    raise ArgumentError, "total evaluated (#{total_eval}) should be g.t.e. total updated (#{total_updated})" if total_eval < total_upd

    self.total_evaluated += total_eval
    self.total_updated += total_upd
    self.last_id = new_last_id
  end

  # Resets progress for the audit
  def reset_progress(new_region = nil)
    self.region = new_region if new_region

    scope = assoc_model_class.where(account_id: account_id)

    # If the model supports thumbnails, we want the max non-thumbnail
    if model_has_thumbnails?
      scope = scope.where(parent_id: nil)
    end

    self.max_id = scope.maximum(:id)

    if max_id
      self.last_id = max_id + 1
    else
      self.max_id = 0
      self.last_id = 0
    end

    self.total_evaluated = 0
    self.total_updated = 0
  end

  def done!
    self.last_id = 0
  end

  def done?
    last_id == 0
  end

  private

  # Builds list of symbols from comma-delim region string
  def build_region_list
    return [] unless read_attribute(:region)
    read_attribute(:region).split(',').map(&:to_sym)
  end
end
