class TicketSchedule < ActiveRecord::Base
  belongs_to :account
  belongs_to :schedule, class_name: 'Zendesk::BusinessHours::Schedule', inherit: :account_id
  belongs_to :ticket, inverse_of: :ticket_schedule, inherit: :account_id

  attr_accessible :account, :schedule, :ticket

  validates_uniqueness_of :ticket_id
end
