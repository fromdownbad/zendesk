class AgentDowngradeAudit < ActiveRecord::Base
  belongs_to :account
  belongs_to :user

  ignore_column :view_ids

  scope :after, -> (datetime) { where("created_at > ?", datetime) }

  serialize :group_ids, Array
  serialize :rule_ids, Array

  def self.from_user(user)
    create(
      account_id:        user.account_id,
      user_id:           user.id,
      roles:             user.roles,
      default_group_id:  user.default_group_id,
      permission_set_id: user.permission_set_id,
      group_ids:         user.groups.pluck(:id),
      rule_ids:          user.rules.pluck(:id)
    )
  end

  def remediate_agent
    transaction do
      remediate_agent_roles
      remediate_agent_permission_set_id
      remediate_agent_memberships
      remediate_agent_rules
    end
  end

  # Remediates a user's role back to what it was prior to downgrade.
  def remediate_agent_roles
    user.roles = roles
    Zendesk::SupportUsers::User.new(user).save
  end

  # Remediates a user's permission_set_id back to what it was prior to downgrade.
  def remediate_agent_permission_set_id
    user.permission_set_id = permission_set_id
    Zendesk::SupportUsers::User.new(user).save
  end

  # A membership may still exist on an account level, which would cause it to
  # be reassigned to the user after the remediation. This method skips the already
  # existing memberships that belong to the user by default, and creates
  # a new record for the lost groups (since it's a joint table for users and groups),
  # and since memberships are actually deleted and not soft-deleted.
  def remediate_agent_memberships
    group_ids.each do |group_id|
      default = default_group_id == group_id

      account.memberships.find_or_create_by!(
        user_id:  user_id,
        group_id: group_id,
        default:  default
      )
    end
  end

  # Rules are soft-deleted during an agent downgrade process,
  # and this method's purpose is to undo the soft-deletion
  # by setting deleted_at back to nil.
  def remediate_agent_rules
    Rule.unscoped.where(account_id: account_id, id: rule_ids).
      update_all(deleted_at: nil)
  end
end
