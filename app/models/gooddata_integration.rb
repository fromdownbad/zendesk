require 'set'
require 'aasm'

class GooddataIntegration < ActiveRecord::Base
  include AASM
  include CIA::Auditable

  DEFAULT_SYNC_TIME         = '12am'.freeze
  DEFAULT_VERSION           = 1
  DEFAULT_DASHBOARD_THREADS = 2

  DASHBOARD_URL      = 'https://secure.gooddata.com/dashboard.html' \
    '#project=/gdc/projects/%{project_id}&dashboard=%{dashboard_path}'.freeze
  SSO_PORTAL_PATH    =
    '/#s=/gdc/projects/%{project_id}|projectDashboardPage|'.freeze
  SSO_DASHBOARD_PATH = '/dashboard.html#project=/gdc/projects/%{project_id}&' \
    'dashboard=%{dashboard_path}'.freeze

  SSO_TOKEN_CACHE_EXPIRY = GoodData::SSO::TOKEN_EXPIRY_IN_SECONDS.seconds - 5.minutes

  belongs_to :account

  attr_accessible :project_id,
    :admin_id,
    :status,
    :scheduled_at,
    :version

  audit_attribute *accessible_attributes

  has_soft_deletion default_scope: true

  validates_uniqueness_of :project_id, scope: :deleted_at

  # Because GooddataIntegration is a has_one relationship
  # Rails will set the account_id of the old integration to nil
  # when it is updated (e.g. account.gooddata_integration = new_integration),
  # We don't want the account_id to ever be nil, since we want to
  # be able to roll back a soft delete.
  # If you wish to remove the integration from the account,
  # use gooddata_integration.soft_delete
  # account.reload; account.gooddata_integration = new_integration
  validates :account_id, presence: true

  after_initialize :set_defaults, if: :new_record?

  def self.tour_project_sso_url
    config = Zendesk::GooddataIntegrations::Configuration

    dashboard_path = '/gdc/md/%{project_id}/obj/%{dashboard_id}' % {
      project_id: config.fetch(:tour_project_id),
      dashboard_id: config.fetch(:tour_project_dashboard_id)
    }

    Zendesk::Gooddata::Client.v2.sso.sign_in_url(
      user_email: config.fetch(:tour_project_email),
      target_url: SSO_DASHBOARD_PATH % {
        project_id: config.fetch(:tour_project_id),
        dashboard_path: dashboard_path
      }
    )
  end

  aasm column: :status do
    state :pending, initial: true
    state :created
    state :configured
    state :invited
    state :complete
    state :disabled
    state :re_enabling

    event :integration_created do
      transitions from: :pending, to: :created
    end

    event :integration_configured do
      transitions from: :created, to: :configured
    end

    event :admin_invited do
      transitions(
        from: :configured,
        to: :invited,
        after: ->(admin_id, invitation_id) do
          self.admin_id      = admin_id
          self.invitation_id = invitation_id
        end
      )
    end

    event :invite_receipt_confirmed do
      transitions from: :invited, to: :complete
    end

    event :disabled do
      transitions from: :complete, to: :disabled
    end

    event :re_enable do
      transitions from: :disabled, to: :re_enabling
    end

    event :re_enable_complete do
      transitions from: :re_enabling, to: :complete
    end

    event :finished do
      transitions from: :configured, to: :complete
    end
  end

  def as_json(options = {})
    {gooddata_integration: super(options)}
  end

  def dashboards
    complete? ? gooddata_dashboards : []
  end

  def portal_sso_url(gooddata_user)
    Zendesk::Gooddata::Client.v2.sso.sign_in_url(
      user_email: gooddata_user.gooddata_login,
      target_url: SSO_PORTAL_PATH % {project_id: project_id}
    )
  end

  def sso_dashboards(gooddata_user)
    params = { gooddata_login: gooddata_user.gooddata_login, user_id: gooddata_user.gooddata_user_id }
    dashboard_entries = Zendesk::Gooddata::Client.v2.project(id: project_id).dashboards_for_user(params) do |r|
      r[:query][:entries]
    end

    dashboards = generate_viewable_dashboard_data(dashboard_entries, gooddata_user)
    dashboards.sort_by { |dashboard| dashboard[:name].downcase }
  end

  def role_id_for(role)
    send("#{role}_role_id")
  end

  def scheduled_at
    read_attribute(:scheduled_at).try do |value|
      value.strftime('%l%P').strip
    end || DEFAULT_SYNC_TIME
  end

  def scheduled_at=(sync_time)
    write_attribute(:scheduled_at, Time.parse(sync_time))
  end

  def status
    read_attribute(:status).try(:downcase)
  end

  def version
    self[:version] || 2
  end

  def last_successful_process
    if complete?
      gooddata_client.project(id: project_id).integration(connector: :zendesk4).find do |response|
        response[:integration][:lastSuccessfulProcess]
      end
    end
  end

  def subdomain_matches_project?
    api_domain = project_settings.fetch(:api_domain, "")
    account_url = account.url(mapped: false, ssl: true)

    api_domain == account_url
  end

  private

  def project_settings
    gooddata_client.project(id: project_id).integration(connector: :zendesk4).settings
  end

  def gooddata_dashboards
    gooddata_client.project(id: project_id).dashboards do |response|
      response[:query][:entries].
        select { |dashboard| dashboard[:unlisted] == 0 }.
        map do |dashboard|
        dashboard.select { |key, _| [:title, :link].include?(key) }.tap do |d|
          d.store(
            :link,
            DASHBOARD_URL % {
              project_id: project_id,
              dashboard_path: d.fetch(:link)
            }
          )
        end
      end
    end
  end

  def set_defaults
    self.scheduled_at ||= DEFAULT_SYNC_TIME
    self.version      ||= DEFAULT_VERSION

    aasm.enter_initial_state unless status
  end

  def gooddata_client
    Zendesk::Gooddata::Client.v2
  end

  def generate_viewable_dashboard_data(dashboard_entries, gooddata_user)
    dashboard_entries.
      map { |dashboard| build_viewable_dashboard_data(dashboard, gooddata_user) }
  end

  def build_viewable_dashboard_data(dashboard, gooddata_user)
    cache_key = dashboard_cache_key(
      project_id,
      dashboard[:identifier],
      gooddata_user.gooddata_login
    )

    Rails.cache.fetch(cache_key, expires_in: SSO_TOKEN_CACHE_EXPIRY) do
      {
        name: dashboard.fetch(:title),
        personal: dashboard[:unlisted] == 1,
        identifier: dashboard[:identifier],
        sso_url: gooddata_client.sso.sign_in_url(
          user_email: gooddata_user.gooddata_login,
          target_url: SSO_DASHBOARD_PATH % {
            project_id: project_id,
            dashboard_path: dashboard.fetch(:link)
          }
        )
      }
    end
  end

  def dashboard_cache_key(project_id, dashboard_identifier, gooddata_login)
    [
      'GoodDataIntegration',
      'viewable-dashboard-data',
      project_id,
      dashboard_identifier,
      gooddata_login
    ].join('--')
  end
end
