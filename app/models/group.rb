class Group < ActiveRecord::Base
  include Identifiable
  include ScopedCacheKeys
  include Zendesk::Serialization::GroupSerialization
  include Satisfaction::CachedScore
  include CachingObserver
  include PrecreatedAccountConversion
  include GroupDomainEventObserver
  include GroupEntityObserver
  include ArSkippedCallbackMetrics::Instrument

  has_kasket_on :account_id

  require_dependency 'group/settings'

  belongs_to :account
  has_many :tickets, dependent: :nullify, inherit: :account_id
  has_many :organizations # , :dependent => :nullify - don't do this, organizations read groups that are active
  has_many :memberships, dependent: :delete_all, inherit: :account_id
  has_many :users, -> { where('users.is_active = 1').order(name: :asc) }, through: :memberships, source: :user, inherit: :account_id

  has_many :groups_macros, class_name: 'GroupMacro'
  has_many :groups_views, class_name: 'GroupView'

  has_many :macros, as: :owner, dependent: :nullify
  has_many :views, as: :owner, dependent: :nullify
  has_many :satisfaction_ratings, class_name: 'Satisfaction::Rating', foreign_key: :agent_group_id, inherit: :account_id

  has_many :ticket_stats, -> { where(agent_id: 0, organization_id: 0) }, class_name: 'StatRollup::Ticket', inherit: :account_id

  # an assocation that strips off the standard 'group_id = foo' behavior so that the named scope below us can add group_id = 0, agent_id in()
  has_many :member_ticket_stats, class_name: 'StatRollup::Ticket', inherit: :account_id do
    def construct_sql
      @finder_sql = conditions
    end
  end

  scope :active, -> { where(is_active: true) }
  scope :with_ids, ->(ids) { where(id: ids) }
  scope :with_name_like, -> (name) { where('name LIKE ?', "%#{sanitize_sql_like(name)}%") }

  after_commit         :ensure_default_group
  validates_associated :memberships, if: :is_active?
  validate             :is_valid_length,        on: %i[create update]
  validate             :validate_is_active,     on: :update
  validate             :has_no_default_members, on: :update, if: :deleted?
  validate             :is_default_group?,      on: :update, if: :deleted?
  validates            :name, uniqueness: { scope: [:account_id, :is_active] }, if: :check_unique_name?
  validates_length_of  :description, maximum: 1024, allow_blank: true

  attr_accessible :name, :description, :agents, :default, :settings

  attr_accessor :current_user

  after_save :cleanup_memberships
  after_commit :enqueue_unassign_ticket_jobs

  def self.assignable(user)
    if user.is_end_user?
      none
    elsif !user.account.has_groups?
      where(default: true)
    elsif (user.agent_restriction?(:groups) && !user.can?(:assign_to_any_group, Ticket)) || user.agent_restriction?(:assigned)
      user.groups
    else
      user.account.groups
    end
  end

  def self.with_deleted
    yield
  end

  def self.order_for_search(search)
    order(sanitize_sql_for_conditions(['LOCATE(?, name)', search]))
  end

  def includes_user_with_id?(user_id)
    users.exists?(id: user_id)
  end

  def settings=(value)
    settings.set(value)
  end

  def agents=(new_agent_ids)
    new_agent_ids = Array(new_agent_ids).reject(&:blank?).map(&:to_i)

    old_agent_ids = memberships.empty? ? [] : memberships.collect(&:user_id)
    removed_agent_ids = old_agent_ids - new_agent_ids
    added_agent_ids = new_agent_ids - old_agent_ids

    unless removed_agent_ids.empty?
      raise "Reassignment must be done in scope of a user" if current_user.nil?

      @removed_memberships = memberships.select { |m| removed_agent_ids.member?(m.user_id) }
      # this is non-destrcutive -- will make in-memory state correct but performs no side-effects, like .build
      memberships.to_a.delete_if { |m| @removed_memberships.include?(m) }
    end

    unless added_agent_ids.empty?
      account.users.where(id: added_agent_ids).each do |user|
        memberships.build(user: user, group: self)
      end
    end
  end

  def deactivate!
    remove_from_phone_group_routing!
    deactivate(true)
  end

  def activate
    self.is_active = true
  end
  alias :soft_undelete :activate

  def activate!
    self.is_active = true
    save!
  end
  alias :soft_undelete! :activate!

  def deleted?
    !is_active?
  end

  def deactivate(bang = false)
    raise "Destroy must be done in scope of a user" unless current_user
    self.is_active = false
    if (bang && save!) || save
      GroupDeleteJob.enqueue(account_id, current_user.id, id)
      true
    else
      false
    end
  end

  def to_liquid
    hash = Zendesk::Liquid::Wrapper.new("name")
    hash['id'] = id
    hash['name'] = name
    hash
  end

  def to_s
    name
  end

  def description
    super || ''
  end

  protected

  def validate_is_active
    return if is_active?

    if account.groups.count(:all) == 1
      errors.add(:base, I18n.t('txt.admin.models.group.cannot_delete_last_group'))
    end
  end

  def ensure_default_group
    return unless account
    default_groups = account.groups.active.where(default: true).count(:all)

    if default_groups == 0
      update_default_group_for_account
    elsif default_groups > 1
      account.groups.active.where(default: true).where.not(id: id).update_all(default: false)
    end
  end

  def has_no_default_members # rubocop:disable Naming/PredicateName
    return unless default_user_ids = memberships.where(default: true).pluck(:user_id).presence
    max_users = 3
    # only grabbing the first max_users+1 in case there are thousands
    default_user_names = users.where(id: default_user_ids).order(:name).pluck(:name).first(max_users + 1)
    default_user_names[max_users] = "..." if default_user_names[max_users]
    default_user_names.each do |user_name|
      errors.add(:default_group, user_name)
    end
  end

  def is_default_group? # rubocop:disable Naming/PredicateName
    return unless default

    errors.add(:default, name)
  end

  def is_valid_length # rubocop:disable Naming/PredicateName
    errors.add(:base, I18n.t("txt.admin.models.group.name_invalid")) unless name && name.length.between?(1, 100)
  end

  private

  def check_unique_name?
    return unless account.settings.check_group_name_uniqueness? && is_active?
    new_record? || name_changed? || is_active_changed?
  end

  def update_default_group_for_account
    # Fetch the first non-default group and make it the default group
    new_default_group = account.groups.where(default: false).first
    if new_default_group
      new_default_group.send(:write_attribute, :default, true)
    end
  end

  def cleanup_memberships
    if deleted?
      memberships.delete_all
    else
      Array(@removed_memberships).each(&:destroy)
    end
  end

  def remove_from_phone_group_routing!
    Voice::GroupCleanupJob.enqueue(account_id, id)
  end

  def enqueue_unassign_ticket_jobs
    return unless @removed_memberships
    @removed_memberships.each do |membership|
      UnassignTicketsJob.enqueue(account_id, id, membership.user_id, current_user.id, ViaType.GROUP_CHANGE)
    end
    @removed_memberships = nil
  end
end
