class Tagging < ActiveRecord::Base
  include ZendeskArchive::Archivable # archive v2
  include PrecreatedAccountConversion

  belongs_to :account
  belongs_to :taggable, polymorphic: true

  ignore_column :enhanced_tag_name

  before_validation     :sanitize_tag_name
  before_validation     :copy_account_id
  validates_presence_of :taggable, :account_id
  validate              :validate_tag_name

  attr_accessible :tag_name, :taggable, :account

  after_commit :make_available_for_autocomplete, on: :create

  TAG_NAME_MAX_LENGTH = 255

  def <=>(other)
    tag_name <=> other.tag_name
  end

  def self.get_tag_error(tag_name)
    if tag_name.blank?
      return I18n.t('txt.admin.models.rules.rule.tag_cannot_be_blank')
    elsif tag_name =~ /\s/ || tag_name =~ SPECIAL_CHARS || tag_name =~ /[^[:graph:]]/
      return I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters')
    elsif tag_name.to_s.length > TAG_NAME_MAX_LENGTH
      return I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_value_too_long', length: TAG_NAME_MAX_LENGTH)
    end

    nil # no error
  end

  private

  def sanitize_tag_name
    self.tag_name = tag_name.downcase if tag_name.present?
  end

  def validate_tag_name
    error = self.class.get_tag_error(tag_name)
    if account&.has_validate_pipes_for_tags? && !error && tag_name =~ /\|/
      # get_tag_error is called by users & org tags.  There is a specifc bug in ticket.taggings were a
      # pipe `|` character causes the UI to break.  Hence lets add a custom validation here.
      #
      # Also updated records get a specific error because many of these cases are where the | was not added
      #   but is suddenly causing a validation error.  The more specific error helps the customer realize the issue
      if new_record?
        error = I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_special_characters')
      elsif tag_name_changed?
        error = I18n.t('txt.admin.models.ticket_fields.field_checkbox.tag_cannot_contain_pipe_character')
      end
    end
    errors.add(:base, error) if error
  end

  def copy_account_id
    self.account_id ||= taggable.account_id
  end

  def make_available_for_autocomplete
    # When the first tagging for a tag is created, we need to be sure
    # it is added to TagScore table so it is available to the Thesaurus
    # immediately for autocompletion
    TagScore.where(account_id: account_id).find_or_create_by(tag_name: tag_name) { |ts| ts.score = 1 }
  rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, ActiveRecord::RecordNotUnique => e
    if e.message.include?("Duplicate entry")
      Rails.logger.info("Duplicate tagging attempt #{account.id}, tag_name #{tag_name}")
      statsd_client.increment('add_autocomplete.duplicate')
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'tagging')
  end
end
