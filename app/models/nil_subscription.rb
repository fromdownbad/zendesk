class NilSubscription
  NIL_PLAN_TYPE = Module.new

  private_constant :NIL_PLAN_TYPE

  def self.nil_plan_type
    NIL_PLAN_TYPE
  end

  attr_reader :account

  def initialize(account)
    @account = account
  end

  def is_trial? # rubocop:disable Naming/PredicateName
    raise 'No Subscription found while checking for trial' unless account.multiproduct?
    false
  end

  def present?
    false
  end

  def blank?
    true
  end

  def nil?
    true
  end

  def plan_type
    NIL_PLAN_TYPE
  end

  def pre_patagonia?
    false
  end

  def patagonia?
    false
  end

  def post_patagonia?
    false
  end

  def payments
    Payment.none
  end

  def method_missing(*args)
    method_name = args.first.to_s

    # This is a silly and naive catch-all for many subscription feature checks. If they don't have a
    # support subscription, then just assume they don't have access to the thing we're checking. If
    # you need the opposite state, then define the method explicitly in this class.
    if method_name.start_with?('has_') && method_name.end_with?('?')
      Rails.logger.info("Tried to call `#{method_name}` on a nil subscription, returning false.")
      false
    else
      super
    end
  end
end
