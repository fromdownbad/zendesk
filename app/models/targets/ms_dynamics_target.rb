class MsDynamicsTarget < Target
  after_create  :destroy_other_targets
  after_destroy :destroy_trigger
  validate      :validate_ms_configuration

  def kind
    I18n.t('txt.admin.models.targets.ms_dynamics_target.Microsoft_Dynamics_target')
  end

  def to_s
    "MsDynamicsTarget: (id: #{id})"
  end

  def is_test_supported? # rubocop:disable Naming/PredicateName
    true
  end

  def is_message_supported? # rubocop:disable Naming/PredicateName
    false
  end

  def send_message(_message, external)
    if account.ms_dynamics_integration.try(:configured?)
      account.ms_dynamics_integration.create_case(external.ticket)
    else
      raise I18n.t("txt.admin.views.settings.extensions._targets.dyanics_error_flash")
    end
  end

  def create_integration(current_user)
    return if current_user.account.triggers.where(feature_identifier: 'ms-dynamics').exists?

    definition = Definition.new
    # Ticket updated
    definition.conditions_all.push(DefinitionItem.new('update_type', 'is', ['Change']))
    # Status changed to solved
    definition.conditions_all.push(DefinitionItem.new('status_id', 'value', [StatusType.SOLVED.to_s]))

    # Notify Microsoft Dynamics
    definition.actions.push(DefinitionItem.new('notification_target', 'is', [id.to_s, "{{ticket.comments_formatted}}"]))
    current_user.account.triggers.create(
      title: title_for_trigger,
      definition: definition,
      current_user: current_user,
      position: 9999,
      is_active: false,
      feature_identifier: 'ms-dynamics'
    )
  end

  protected

  def validate_ms_configuration
    unless account.ms_dynamics_integration.try(:configured?)
      errors.add(:base, I18n.t('txt.admin.views.settings.extensions._targets.dynamics_error'))
    end
  end

  private

  def ticket_title(ticket)
    if ticket.subject.blank?
      ticket.description.truncate(50)
    else
      ticket.subject
    end
  end

  def title_for_trigger
    I18n.t("txt.admin.views.settings.extensions._targets.dynamics_title_for_trigger", locale: account.translation_locale)
  end

  def destroy_trigger
    trigger = account.triggers.where(feature_identifier: 'ms-dynamics').first
    trigger.try(:destroy)
  end
end
