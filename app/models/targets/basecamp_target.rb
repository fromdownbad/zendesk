require 'basecamp'

class BasecampTarget < Target
  settings_accessor :url, :username, :project_id, :resource, :message_id,
    :todo_list_id
  encrypted_settings_accessor :token, :password

  validates_presence_of :token, :url, :project_id
  validates_inclusion_of :resource, in: ["todo", "message"]
  validate :validate_url

  def validate_url
    errors.add(:url, :invalid) unless url =~ URL_PATTERN
  end

  def is_for_messages? # rubocop:disable Naming/PredicateName
    resource == 'message'
  end

  def to_s
    "BasecampTarget, url: #{url}, token: #{token}, project: #{project_id}"
  end

  def is_test_supported? # rubocop:disable Naming/PredicateName
    is_for_messages? ? !message_id.include?('{{') : !todo_list_id.include?('{{')
  end

  def test_no_supported_reason
    'Placeholders with specific values needed'
  end

  def send_message(message, external)
    ticket = external.ticket
    user = external.author

    if message.blank?
      raise "Can't send a blank message to #{self}"
    end

    connect

    if settings[:project_name]
      # Backwards compatibility
      update_settings
    end

    if token.blank?
      set_token
    end

    if project_id.blank?
      raise "Can't send a message without a project id #{self}"
    end

    if is_for_messages?
      if message_id.blank?
        create_message(ticket, message)
      else
        create_comment(ticket, user, message)
      end
    else
      if todo_list_id.blank?
        create_todo_list(ticket, message)
      else
        create_todo_item(ticket, user, message)
      end
    end
  end

  def create_message(ticket, message)
    m = Basecamp::Message.new(project_id: project_id)
    m.title = if ticket.subject.blank?
      ticket.description.truncate(50)
    else
      ticket.subject
    end
    m.body = message
    m.save rescue raise I18n.t('txt.errors.targets.basecamp_target.the_project_with_id_doesnt_exists', project_id: project_id)
  end

  def create_comment(ticket, user, message)
    the_id = liquidized(message_id, ticket, user)
    c = Basecamp::Comment.new(post_id: the_id)
    c.body = message
    c.save rescue raise I18n.t('txt.errors.targets.basecamp_target.message_with_the_id', the_id: the_id)
  end

  def create_todo_list(ticket, message)
    l = Basecamp::TodoList.new(project_id: project_id)
    l.name = if ticket.subject.blank?
      ticket.description.truncate(50)
    else
      ticket.subject
    end
    l.description = message
    l.save rescue raise I18n.t('txt.errors.targets.basecamp_target.the_project_with_id', project_id: project_id)
  end

  def create_todo_item(ticket, user, message)
    the_id = liquidized(todo_list_id, ticket, user)
    i = Basecamp::TodoItem.new(todo_list_id: the_id)
    i.content = message
    i.save rescue raise I18n.t('txt.errors.targets.basecamp_target.the_todo_list_with_id_doesnt_exists', the_id: the_id)
  end

  def exception_message(msg)
    "The url or credentials are wrong, the API access is disabled in your Basecamp account or #{msg}. Please read more in the sidebar and in the examples to solve the problem."
  end

  def liquidized(field, ticket, user)
    Zendesk::Liquid::TicketContext.render(field, ticket, user, true, ENGLISH_BY_ZENDESK)
  end

  def connect
    uri = URI.parse(url)
    the_username = encrypted_token.blank? ? username : token
    the_password = encrypted_token.blank? ? password : 'x'
    Basecamp.establish_connection!(uri.host, the_username, the_password, uri.scheme == 'https')
  end

  def set_token
    self.token = Basecamp::Person.me.token
    encrypt!
    save!
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Getting basecamp token for target '#{id}' in account '#{account.subdomain}'", fingerprint: '3d3111be506fcf1fd8c352d19def15cf20b2f56b')
  end

  def update_settings
    name = settings[:project_name]
    projects = Basecamp::Project.find(:all)
    projects.each do |project|
      next unless project.name.casecmp(name.downcase).zero?
      settings.delete(:project_name)
      self.settings = settings.merge(project_id: project.id)
      save!
      reload
      return
    end
    ZendeskExceptions::Logger.record(e, location: self, message: "Executing backwards compatibility for basecamp target defined with project name '#{name}'. Target: #{id}. Account: #{account.subdomain}", fingerprint: 'a761ebc7093d1e8956e8a441474ec6d758b1d5ba')
    raise "Couldn't find project with name '#{name}'. Please update your target with the project id instead of the project name. Error: #{e.message}"
  end

  def kind
    I18n.t('txt.admin.models.targets.basecamp_target.Basecamp_target')
  end
end
