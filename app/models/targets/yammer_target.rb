require 'yammer'

class YammerTarget < OauthTarget
  class YammerTargetError < StandardError
    attr_reader :reason

    def self.generic_response(response)
      "#{I18n.t('txt.admin.controllers.targets_controller.yammer_target_error')}code: #{response.code}, error: #{response.body}"
    end

    def initialize(reason)
      @reason = reason
    end
  end

  undef_method :site
  settings_accessor :oauth2_token, :group_id
  validates_presence_of :oauth2_token, on: :create

  def site
    "https://www.yammer.com"
  end

  def to_s
    "YammerTarget: (id: #{id})"
  end

  def requires_authorization?
    true
  end

  def use_oauth2?
    true
  end

  def authorized?
    oauth2_token.present?
  end

  def is_oauth2? # rubocop:disable Naming/PredicateName
    oauth2_token.present?
  end

  # this will force the edit page to show up
  def needs_authorization_upgrade?
    !new_record? && is_oauth_authorized? && !is_oauth2?
  end

  def kind
    I18n.t('txt.admin.models.targets.yammer_target.Yammer_target')
  end

  def send_message(message, external)
    if is_oauth2?
      return send_message_oauth2(message, external)
    end

    raise YammerTargetError, I18n.t('txt.admin.controllers.targets_controller.yammer_target_error').to_s unless is_oauth_authorized?
    response = access_token.post "/api/v1/messages.json", 'body' => message, 'group_id' => group_id
    unless response.is_a?(Net::HTTPCreated)
      doc = REXML::Document.new(response.body)
      # <hash>
      #   <response>
      #     <code>16</code>
      #     <stat>fail</stat>
      #     <message>Token not found.</message>
      #  </response>
      # </hash>
      error = begin
        error_msg = doc.elements['hash/response/message'].text
        "Yammer failed with response '#{error_msg}'"
      rescue StandardError
        "Unknown Yammer Error. Code: #{response.code}, Error: #{response.body}"
      end
      raise error
    end
  end

  def send_message_oauth2(message, _external)
    raise YammerTargetError, I18n.t('txt.admin.controllers.targets_controller.yammer_target_error') unless authorized?
    group_arg = group_id.blank? ? {} : {group_id: group_id}
    response = client.create_message(message, group_arg)
    if response.is_a?(Yammer::ApiResponse)
      unless response.created?
        raise YammerTargetError, YammerTargetError.generic_response(response)
      end
    else
      error_message = begin
        code = response.code
        JSON.parse(response.body)['base']
      rescue JSON::ParserError
        response.body
      rescue StandardError
        ""
      end
      raise YammerTargetError, "#{I18n.t('txt.admin.controllers.targets_controller.yammer_target_error')}code: #{code}, error: #{error_message}"
    end
    response
  end

  ### Oauth authentication related methods
  def request_token(code)
    response = consumer_oauth2.access_token_from_authorization_code(code, redirect_uri: callback_url(self.class.name.underscore))
    if (400..500).cover? response.code.to_i
      raise YammerTargetError, YammerTargetError.generic_response(response)
    end
    body = JSON.load(response.body)
    self.oauth2_token = body['access_token']['token']
    # we do this here since it can be very confusing for the user if we don't save here and
    # force them to update the target
    save! unless new_record?
    oauth2_token
  # yammer will sometimes raise a runtime error
  rescue RuntimeError => e
    raise YammerTargetError, e.message
  end

  def callback_url(id)
    "#{account_url}/targets/yammer_callback?id=#{id}"
  end

  def authorize_url
    consumer_oauth2.webserver_authorization_url(redirect_uri: callback_url(self.class.name.underscore))
  end

  def reauthorize_url
    consumer_oauth2.webserver_authorization_url(redirect_uri: callback_url(id))
  end

  def authorization_failure_message
    I18n.t("txt.access.login.yammer_error")
  end

  ### end

  private

  def account_url
    # force ssl to true since our client app setting on the yammer specifies https
    # if this is removed and the customer does not have ssl enabled they will get an "invalid redirect_uri" error
    account.url(mapped: false, ssl: true)
  end

  def client
    @client ||= Yammer::Client.new(access_token: oauth2_token)
  end

  ### Oauth authentication related methods
  def consumer_oauth2
    @consumer ||= Yammer::OAuth2Client.new(
      Zendesk::Configuration.dig(:yammer, :client_id),
      Zendesk::Configuration.dig(:yammer, :client_secret),
      site: Zendesk::Configuration.dig(:yammer, :site)
    )
  end
end
