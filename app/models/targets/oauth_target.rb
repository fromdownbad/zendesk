require 'oauth/consumer'

class OauthTarget < Target
  settings_accessor :consumer_key, :secret, :callback_token, :site,
    :oauth_token, :oauth_token_secret

  def is_oauth_authorized? # rubocop:disable Naming/PredicateName
    oauth_token.present? && oauth_token_secret.present? && callback_token.present?
  end

  def consumer
    OAuth::Consumer.new(consumer_key, secret, site: site)
  end

  def access_token
    OAuth::AccessToken.new(consumer, oauth_token, oauth_token_secret)
  end

  def to_s
    "OauthTarget, site: #{site}"
  end

  def kind
    I18n.t('txt.admin.models.targets.oauth_target.Oauth_target')
  end
end
