class TargetFailure < ActiveRecord::Base
  include RawHttp
  truncate_attributes_to_limit :raw_request, :raw_response

  belongs_to :account
  belongs_to :target
  belongs_to :event, foreign_key: :external_id, class_name: 'External'

  validate :ensure_target_still_exists

  attr_accessible :account, :target, :event, :status_code, :exception, :message, :consecutive_failure_count,
    :raw_http_capture, :response_body

  attr_accessor :raw_http_capture, :response_body

  after_create :clean_up_target_failures

  TARGET_FAILURES_LIMIT = 25

  private

  def clean_up_target_failures
    target.target_failures.order('created_at DESC').offset(TARGET_FAILURES_LIMIT).destroy_all
  end

  def ensure_target_still_exists
    raise ActiveRecord::RecordNotFound unless target && target.reload
  rescue ActiveRecord::RecordNotFound
    errors.add(:target, "must exist")
  end
end
