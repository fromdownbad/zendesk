require_dependency 'zendesk/pivotal_tracker'

class PivotalTarget < Target
  STORY_TYPES = ["feature", "chore", "bug", "release"].freeze

  validates_presence_of :token, :project_id, :story_type, :story_title
  validates_inclusion_of :story_type, in: STORY_TYPES + STORY_TYPES.collect(&:titleize)

  settings_accessor :story_type, :project_id, :requested_by, :owned_by
  encrypted_settings_accessor :token

  settings_writer :story_title, :story_labels

  def story_title
    settings[:story_title] ||= I18n.t('txt.admin.models.targets.pivotal_target.from_ticket_title_with_no_params')
  end

  # At one time, this was the :labels setting. Then,
  # in 55ab3903c08f523d4fb3ab8849999562d791de5e, it changed to :story_labels.
  # Thus, we define a reader for story_labels that checks both and a writer
  # just for the new version (:story_labels).
  def story_labels
    settings[:story_labels] || settings[:labels]
  end

  def kind
    I18n.t('txt.admin.models.targets.pivotal_target.Pivotal_target')
  end

  def show_story_url
    settings[:show_story_url]
  end

  def to_s
    "PivotalTarget, token: #{token}, project_id: #{project_id}"
  end

  def send_message(message, external)
    @ticket = external.ticket
    @user   = external.author
    raise_error("Message is blank.") if message.blank?

    title = liquidized_title(@ticket, @user)
    response = http_post_new_story_to_pivotal(title, message, @ticket, @user)
    raise_error(error_message_from_response(response)) if response[:kind] == 'error'

    story_url = response[:url]
    audit = Audit.new(account: @ticket.account, ticket: @ticket, via_id: external.via_id, author: @user)
    save_object = audit

    audit.events << Push.new(via_reference_id: external.via_reference_id, value_reference: "Pivotal Tracker", value: "<li>Story created from this ticket: <a href=\"#{story_url}\" target=_blank\">#{story_url}</a></li>", audit: audit)

    unless save_object.save
      raise_error("Unable to save Pivotal Tracker story creation event: #{audit.errors.full_messages.inspect}")
    end
  end

  private

  def project_members
    return @project_members if @project_members

    response = pivotal_client.project_memberships(project_id)
    raise_error(error_message_from_response(response)) if response.is_a?(Hash) && response.fetch(:kind) == 'error'

    @project_members = response.each_with_object({}) do |membership, result|
      person = membership['person']
      id = person['id']
      name = person['name'].downcase

      result[id] = name
    end
  end

  def member_id_for(potential_member)
    project_members.key(potential_member)
  end

  def project_integrations
    return @project_integrations if @project_integrations

    response = pivotal_client.project_integrations(project_id)
    raise_error(error_message_from_response(response)) if response.is_a?(Hash) && response.fetch(:kind) == 'error'

    @project_integrations = response.each_with_object({}) do |integration, result|
      if integration && integration[:active]
        result[integration[:kind].downcase] = { id: integration[:id] }
      end
    end
  end

  def project_integration?(integration)
    project_integrations.keys.include?(integration.downcase)
  end

  def story_labels_params
    return [] unless story_labels
    return @story_label_params if @story_label_params

    response = pivotal_client.project_labels(project_id)
    raise_error(error_message_from_response(response)) if response.is_a?(Hash) && response.fetch(:kind) == 'error'

    @story_label_params = story_labels.split(",").map do |label|
      label.strip!
      project_label = response.find { |l| l[:name] == label }

      if project_label
        { id: project_label[:id], name: project_label[:name] }
      else
        label
      end
    end
  end

  def liquidized(field, ticket, user)
    Zendesk::Liquid::TicketContext.render(field, ticket, user, true, user.account.translation_locale)
  end

  def liquidized_title(ticket, user)
    title = liquidized(story_title, ticket, user)
    title.present? ? title : I18n.t('txt.admin.models.widgets.widget.untitle_label')
  end

  def request(title, message, ticket, user)
    description = message.truncate(5_000)
    fields = {story_type: story_type.downcase, name: title, description: description, labels: story_labels_params}

    if requested_by
      liquid_requested_by = liquidized(requested_by, ticket, user).downcase
      if member_id = member_id_for(liquid_requested_by)
        fields[:requested_by_id] = member_id
      end
    end

    if owned_by
      liquid_owned_by = liquidized(owned_by, ticket, user).downcase
      if owner_id = member_id_for(liquid_owned_by)
        fields[:owner_ids] = [owner_id]
      end
    end

    if project_integration?(integration_name)
      fields[:integration_id] = project_integrations[integration_name][:id]
      fields[:external_id] = ticket.nice_id.to_s
    end

    fields
  end

  def http_post_new_story_to_pivotal(title, message, ticket, user)
    request_body = request(title, message, ticket, user)
    pivotal_client.create_story(project_id, request_body)
  end

  def pivotal_client
    @pivotal_client ||= Zendesk::PivotalTracker.new(token)
  end

  def integration_name
    "zendesk_integration"
  end

  def raise_error(message)
    raise "#{error_message}: #{message}"
  end

  def error_message_from_response(response)
    "#{response[:code].humanize} - #{response[:error]}"
  end

  def error_message
    "Account #{@user.account_id} pivotal story creation failed for ticket #{@ticket.nice_id}/#{@ticket.id}"
  end
end
