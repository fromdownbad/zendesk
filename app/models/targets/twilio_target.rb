require 'twilio-ruby'

class TwilioTarget < Target
  settings_accessor :sid, :to, :from
  encrypted_settings_accessor :token

  def to_s
    "TwilioTarget From: #{from}, To: #{to}, sid: #{sid}"
  end

  def send_message(message, _external)
    twilio_account = ::Twilio::REST::Client.new(sid, token)
    message_length = 1600

    params = {
      from: from,
      to: to,
      body: message.truncate(message_length)
    }

    begin
      twilio_account.messages.create(params)
    rescue ::Twilio::REST::RequestError => e
      raise TwilioException, e.message
    end
  end

  def kind
    I18n.t('txt.admin.models.targets.twilio_target.Twilio_target')
  end

  class TwilioException < StandardError; end;
end
