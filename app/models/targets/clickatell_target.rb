class ClickatellAPIError < StandardError
end

class ClickatellTarget < UrlTarget
  MAX_PAYLOAD = 160

  undef_method :url=
  undef_method :method=
  undef_method :attribute=
  settings_accessor :username, :api_id, :to, :from
  settings_accessor :us_small_business_account, boolean: true
  encrypted_settings_accessor :password

  validates_presence_of :username, :password, :api_id, :to
  validate :require_from_if_us_small_business_account

  def kind
    I18n.t('txt.admin.models.targets.clickatell_target.Clickatell_target')
  end

  def url
    "http://api.clickatell.com/http/sendmsg"
  end

  def method
    "get"
  end

  def attribute
    "text"
  end

  def send_message(message, external)
    response = super(truncate_message(message), external)
    log_response(response)
    response
  end

  protected

  # @overwrite
  def convert_space_to_plus
  end

  def parameters
    params = {
      "user"     => username,
      "password" => password,
      "api_id"   => api_id,
      "to"       => to
    }

    params["from"] = from if from.present?
    params["mo"]   = "1" if us_small_business_account?

    params
  end

  def require_from_if_us_small_business_account
    if us_small_business_account? && from.blank?
      errors.add(:from, I18n.t('txt.errors.clickatell_target.from_errors'))
    end
  end

  # From Clickatell API docs
  # http://support.clickatell.com/faq.php?mode=view_entry&kbid=15&kbcat=9
  # Some characters take up two spaces:
  #
  # 0x1B0A Form feed (lien break)
  # 0x1B14 Circumflex accent ^
  # 0x1B28 Left curly bracket {
  # 0x1B29 Right curly bracket }
  # 0x1B2F Backslash
  # 0x1B3C Left square bracket [
  # 0x1B3D Tilde ~
  # 0x1B3E Right square bracket ]
  # 0x1B40 Vertical bar |
  # 0x1B65 Euro sign €

  HEAVIES = %W(\n ^ { } \\ [ ~ ] | €).freeze

  def truncate_message(message)
    count = 0
    message.each_char.inject("") do |truncated, ch|
      count += if HEAVIES.include?(ch)
        2
      else
        1
      end
      break truncated if count > MAX_PAYLOAD
      truncated << ch
    end
  end

  def us_small_business_account?
    us_small_business_account && us_small_business_account.to_s != '0'
  end

  def log_response(response)
    new_message_id = response.body.split('ID: ')[1] if response.body.include?("ID: ")

    if new_message_id
      Rails.logger.info("HTTP (ClickatellTarget) response OK: #{response.body}")
      return new_message_id
    end

    stringcode        = response.body.match(/\d\d\d/)
    api_response_code = stringcode[0].to_i unless stringcode.blank?

    case api_response_code
    when 001 then (msg = "Authentication failed. Check your username, password and API ID.")
    when 002 then (msg = "Unknown username or password.")
    when 101 then (msg = "Invalid or missing data.")
    when 105 then (msg = "Invalid destination address, check the phone number.")
    when 106 then (msg = "Invalid Source Address.")
    when 108 then (msg = "Please check your API ID.")
    when 114 then (msg = "Please check your phone number – could not route message.")
    when 301 then (msg = "Insufficient credits.")
    else
      msg = 'Unknown Clickatell error status: %s' % api_response_code
    end

    Rails.logger.error("HTTP (ClickatellTarget) failed sending message: http: #{response.status} ERROR #{response.body}")

    exception_msg = "ERROR -- Could not send Clickatell message. "
    exception_msg << msg if msg

    raise ClickatellAPIError, exception_msg
  end
end
