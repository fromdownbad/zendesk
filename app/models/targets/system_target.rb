module SystemTarget
  def is_system_target? # rubocop:disable Naming/PredicateName
    feature_identifier.present?
  end

  def readonly?
    !new_record? && is_locked? && !marked_for_destruction?
  end

  def kind
    I18n.t('txt.admin.models.targets.system_target.System_target')
  end
end
