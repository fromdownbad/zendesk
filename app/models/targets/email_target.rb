class EmailTarget < Target
  before_validation :clean_up_address
  validates_presence_of :email, :subject
  validate :validate_email

  settings_accessor :email, :subject

  def to_s
    "EmailTarget, email: #{email}"
  end

  def render_message(_)
    # rendering is done in a mail rendering job
  end

  def send_message(message, external)
    TargetsMailer.deliver_target_message(external.ticket.account, email, subject, message, external)
  end

  def kind
    I18n.t('txt.admin.models.targets.email_target.Email_target')
  end

  protected

  def clean_up_address
    self.email = Zendesk::Mail::Address.clean_up_address(email)
  end

  def validate_email
    errors.add(:email, :invalid) unless Zendesk::Mail::Address.valid_address?(email)
  end

  def max_failures_reached?
    failures >= Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT
  end
end
