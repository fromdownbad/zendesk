class GetSatisfactionTarget < Target
  DEFAULT_URL = 'http://api.getsatisfaction.com/'.freeze

  settings_accessor :email, :account_name
  settings_accessor :url, default: DEFAULT_URL
  encrypted_settings_accessor :password
  validates_presence_of :email, :password, :account_name

  STATUS_MAPPING = {
    'New' => 'none',
    'Open' => 'active',
    'Pending' => 'pending',
    'Solved' => 'complete',
    'Closed' => 'complete'
  }.freeze

  def to_s
    "GetSatisfactionTarget, url: #{url}, :account_name: #{account_name}, email: #{email}"
  end

  def kind
    I18n.t('txt.admin.models.targets.get_satisfaction_target.GetSatisfaction_target')
  end

  def is_test_supported? # rubocop:disable Naming/PredicateName
    false
  end

  def test_no_supported_reason
    'Ticket with specific GetSatisfaction topic linking needed'
  end

  def send_message(message, external)
    ticket = external.ticket
    get_satisfaction_topic_id = (ticket.get_satisfaction_topics.first.nil? ? nil : ticket.get_satisfaction_topics.first.topic_id) || ticket.external_id
    raise "Cannot send updates for tickets not created via a GetSatisfaction topic" unless ticket.via_id == ViaType.GET_SATISFACTION
    raise "Missing GetSatisfaction topic or external ID" if get_satisfaction_topic_id.nil?
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.start

    unless message.blank?
      request = Net::HTTP::Post.new("/topics/#{get_satisfaction_topic_id}/replies")
      request.basic_auth email, password
      response = http.request(request, "reply[content]=#{message}")

      unless response.is_a?(Net::HTTPCreated)
        raise "GetSatisfaction comment post failed: #{response.inspect}"
      end
    end

    request = Net::HTTP::Put.new("/topics/#{get_satisfaction_topic_id}/status")
    request.basic_auth email, password
    response = http.request(request, "topic[status]=#{STATUS_MAPPING[ticket.status]}")

    unless response.is_a?(Net::HTTPOK)
      raise "HTTP call failed while updating GetSatisfaction status for ticket '#{ticket.id}' with GetSatisfaction topic ID '#{get_satisfaction_topic_id}': #{response.inspect}"
    end
  end

  def create_integration(current_user)
    create_comment_added_trigger(current_user)
    create_status_changed_trigger(current_user)
    create_view(current_user)
  end

  private

  def create_comment_added_trigger(current_user)
    title = 'Notify GetSatisfaction of comment update'
    return if current_user.account.triggers.find_by_title(title)
    definition = Definition.new
    # Ticket updated
    definition.conditions_all.push(DefinitionItem.new('update_type', 'is', ['Change']))
    # Comment is public
    definition.conditions_all.push(DefinitionItem.new('comment_is_public', 'is', ['true']))
    # Get Satisfaction ticket
    definition.conditions_all.push(DefinitionItem.new('via_id', 'is', ['16']))
    # Comment added action
    definition.actions.push(DefinitionItem.new('notification_target', 'is', [id.to_s, "{{ticket.latest_public_comment_formatted}}"]))
    current_user.account.triggers.create(title: title, definition: definition, current_user: current_user, position: 9999)
  end

  def create_status_changed_trigger(current_user)
    title = 'Notify GetSatisfaction of status change'
    return if current_user.account.triggers.find_by_title(title)
    definition = Definition.new
    # Ticket updated
    definition.conditions_all.push(DefinitionItem.new('update_type', 'is', ['Change']))
    # Status changed
    definition.conditions_all.push(DefinitionItem.new('status_id', 'changed'))
    # Get Satisfaction ticket
    definition.conditions_all.push(DefinitionItem.new('via_id', 'is', ['16']))
    # Status changed action
    definition.actions.push(DefinitionItem.new('notification_target', 'is', [id.to_s, ""]))
    current_user.account.triggers.create(title: title, definition: definition, current_user: current_user, position: 9999)
  end

  def create_view(current_user)
    title = 'GetSatisfaction unsolved tickets'
    return if current_user.account.views.find_by_title(title)
    definition = Definition.new
    # Status less than 'Solved'
    definition.conditions_all.push(DefinitionItem.new('status_id', 'less_than', [StatusType.SOLVED.to_s]))
    # Get Satisfaction ticket
    definition.conditions_all.push(DefinitionItem.new('via_id', 'is', ['16']))
    current_user.account.views.create(title: title, definition: definition, current_user: current_user, position: 9999, output: Output.new)
  end
end
