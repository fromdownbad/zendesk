autoload :Jira4R, 'jira4r'

class JiraTarget < Target
  settings_accessor :url, :username
  encrypted_settings_accessor :password

  validates_presence_of :username, :password, :url

  def to_s
    "JiraTarget, username: #{username}, url: #{url}"
  end

  def kind
    I18n.t('txt.admin.models.targets.jira_target.JIRA_target')
  end

  def is_test_supported? # rubocop:disable Naming/PredicateName
    false
  end

  def test_no_supported_reason
    'Ticket with specific external id needed'
  end

  def send_message(message, external)
    ticket = external.ticket
    raise "Cannot send blank message" if message.blank?
    # commented until we implement a good solution to identify the type of the ticket
    # raise "Cannot send updates for non jira tickets" unless ticket.via_id == ViaType.JIRA_WIDGET
    return if ticket.jira_issues.first.nil? && ticket.external_id.nil?

    jira = Jira4R::JiraTool.new(2, url)
    jira.driver.options["protocol.http.ssl_config.verify_mode"] = nil
    jira.login(username, password)

    comment = Jira4R::V2::RemoteComment.new
    comment.body = message

    jira.addComment((ticket.jira_issues.first.nil? ? nil : ticket.jira_issues.first.issue_id) || ticket.external_id, comment)
  end
end
