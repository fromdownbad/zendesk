class FlowdockTarget < Target
  encrypted_settings_accessor :api_token
  validates_presence_of :api_token

  def kind
    I18n.t('txt.admin.models.targets.flowdock_target.Flowdock_target')
  end

  def method
    'post'
  end

  def link
    settings.fetch(:link, "#{account.url}/tickets/{{ticket.id}}")
  end

  def to_s
    "<Flowdock Target api_token: #{api_token}>"
  end

  def send_message(message, evt)
    FlowdockTarget::Gateway.send(self, message, evt)
  end

  class Gateway
    FLOWDOCK_URL = 'https://api.flowdock.com/v1/zendesk/%s'.freeze

    class << self
      def send(target, message, evt)
        uri = FLOWDOCK_URL % target.api_token
        data = remove_empty_values(form_data(target, message, evt)).to_query
        if response = Zendesk::Net::MultiSSL.post(uri.to_s, body: data, ignore_retry_notice: true, raise_errors: true)
          raise "HTTP client call failed: #{response.body}" if response.status >= 400
        end
      end

      private

      # @return [Hash] a Hash that has blank values removed
      def remove_empty_values(hash)
        Hash[*hash.select { |_k, v| v.present? }.flatten]
      end

      def set_details(result, user, type)
        result["#{type}[email]"]      = user.email
        result["#{type}[name]"]       = user.name
        result["#{type}[first_name]"] = user.first_name
        result["#{type}[last_name]"]  = user.last_name
      end

      def form_data(_target, message, evt)
        ticket = evt.ticket
        t = Zendesk::Liquid::TicketDrop.new(ticket, true, 'text/plain')
        assignee = ticket.assignee
        requester = ticket.assignee
        author = evt.author

        result = {
          message: message,
          title: t[:title],
          description: t[:description],
          latest_comment: t[:latest_comment_formatted],
          in_business_hours: t[:in_business_hours],

          url: ticket.url,
          id: ticket.nice_id,
          external_id: ticket.external_id,
          via: ticket.via,
          status: ticket.status,
          priority: ticket.priority,
          type: ticket.ticket_type,
          due_date: ticket.due_date,
          account: ticket.account.to_s,

          cc_names: ticket.cc_names,
          follower_names: ticket.follower_names,
          email_cc_names: ticket.email_cc_names,

          tags: ticket.current_tags,
          group: ticket.group.try(:name),
          organization: ticket.organization.try(:name)
        }

        set_details(result, assignee, 'assignee') if assignee
        set_details(result, requester, 'requester') if requester
        set_details(result, author, 'user') if author

        result
      end
    end
  end
end
