require 'zendesk/targets/url_target_client'
require 'ipaddress'
require 'raw_net_capture'

# The URL target takes a user provided URL and invokes it. It does not try to encode parameters that are given
# as part of the URL query string by the user. It will encode the message payload. It will resolve and encode liquid placeholders.
# It will move the parameter string to the body for PUT and POST requests, but retain them in the URL for GET.
class UrlTarget < Target
  class ::HTTPResponseStatusError < StandardError; end
  include TargetsMetrics::StatsDClient

  before_validation     :convert_space_to_plus
  before_validation     :downcase_method
  validates_presence_of :attribute, if: proc { |target| target.instance_of?(UrlTarget) }
  validate              :validate_url

  attr_accessor :response_code, :response_body, :http_transactions

  settings_accessor :url, :method, :attribute, :username
  encrypted_settings_accessor :password

  def kind
    I18n.t("txt.admin.models.targets.url_target.URL_target")
  end

  def to_s
    "#{self.class.name}, URL: #{url}, method: #{method}"
  end

  def client(message, options = {})
    options = options.merge(
      url: url,
      target: self,
      method: method,
      username: username,
      password: password,
      params: parameters.merge(attribute => message)
    )

    Zendesk::UrlTargetClient.new(options)
  end

  def send_message(message, external)
    ticket = external.ticket
    user   = external.author

    raise "Cannot send blank message" if message.blank?

    response = client(message, ticket: ticket, user: user, account: account).invoke
    statsd_client.increment("response.status_code", tags: %W[source:#{metrics_name} status_code:#{response.status}])
    unless response.success?
      Rails.logger.error("UrlTarget Failed #{title} Response: #{response.env.to_h.slice(:method, :body, :url, :request_headers, :response_headers, :status)}")

      set_response_values(response)
      raise HTTPResponseStatusError, "HTTP client call failed"
    end

    response
  end

  def message_failed(exception, event)
    build_failure(exception, event) if account.has_target_failure?
    super(exception)
  end

  def raw_http_capture
    @raw_http_capture ||= RawHTTPCapture.new
  end

  def statsd_client
    @statsd_client ||= create_statsd_client
  end

  protected

  # Subclasses can specify extra parameters
  def parameters
    { }
  end

  def convert_space_to_plus
    self.url = url.tr(" ", "+") if url.present?
  end

  def downcase_method
    self.method ||= "get"
    self.method.downcase!
  end

  def validate_url
    if url.blank? || url !~ URL_PATTERN
      Rails.logger.error("UrlTarget Invalid: garbage #{url}")
      errors.add(:url, :invalid)
    else
      if Zendesk::Net::AddressUtil.safe_url?(url) || Rails.env.development?
        begin
          without_liquid = url.gsub(/\{\{.*?\}\}/, "X")
          URI.parse(without_liquid)
        rescue URI::InvalidURIError
          Rails.logger.error("UrlTarget Invalid: URI::InvalidURIError #{url}")
          errors.add(:url, :invalid)
        end
      else
        Rails.logger.error("UrlTarget Invalid: unsafe #{url}")
        errors.add(:url, :invalid)
      end
    end
  rescue Resolv::ResolvTimeout
    Rails.logger.error("UrlTarget Invalid: Resolv::ResolvTimeout #{url}")
    errors.add(:url, :invalid)
  end

  def set_response_values(response) # rubocop:disable Naming/AccessorMethodName
    self.response_code = response.status
    self.response_body = response.body
  end

  private

  def build_failure(exception, event)
    failure = TargetFailure.new(
      account: account,
      target: self,
      event: event,
      status_code: response_code,
      exception: exception.class.to_s,
      message: exception.message,
      consecutive_failure_count: increment_current_failures(exception),
      raw_http_capture: raw_http_capture,
      response_body: response_body
    )
    failure.save
  end
end
