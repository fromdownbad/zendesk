require 'zendesk/encryptable'
require 'johnny_five'

class Target < ActiveRecord::Base
  include SystemTarget
  include Zendesk::Encryptable
  include Zendesk::PreventCollectionModificationMixin
  extend Zendesk::FetchAppsRequirements

  ENCRYPTION_KEY_NAME = ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_KEY')
  ENCRYPTION_CIPHER_NAME = ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_CIPHER_NAME')
  LEGACY_SECRET_SETTINGS = [:password, :secret, :oauth_secret].freeze
  ENCRYPTED_SECRET_SETTINGS = [:encrypted_password, :encrypted_token, :encrypted_secret_value].freeze
  SECRET_SETTINGS = [*LEGACY_SECRET_SETTINGS, *ENCRYPTED_SECRET_SETTINGS].freeze
  TARGETS_TO_UPDATE_KEY = 'targets_to_update_shard'.freeze
  ENCRYPTED_SETTINGS_MAP = {
    token: :encrypted_token,
    api_token: :encrypted_token,
    password: :encrypted_password,
    secret: :encrypted_secret_value
  }.freeze

  belongs_to :account

  has_one :resource_collection_resource, as: :resource, inherit: :account_id

  has_many :target_failures, dependent: :destroy, inherit: :account_id

  serialize :data

  attr_accessible :account, :title, :is_active, :feature_identifier, :is_locked,
    :sent, :failures, :error, :settings, :encrypted_password, :encrypted_token,
    :encrypted_secret_value
  attr_encrypted :encrypted_password, :encrypted_token, :encrypted_secret_value,
    encryption_key_name: ENCRYPTION_KEY_NAME, encryption_cipher_name: ENCRYPTION_CIPHER_NAME

  validates_presence_of :title

  scope :active, -> { where(is_active: true).order('title ASC') }
  scope :inactive, -> { where(is_active: false).order('title ASC') }
  scope :email, -> { where(type: EmailTarget) }

  after_update :clear_settings # will have old values
  before_save :reset_target_and_cache
  after_create :create_integration!, if: :creates_integration?

  before_destroy :mark_for_destruction

  attr_accessor :current_user

  class << self
    def setting_writers
      @setting_writers ||= begin
        superclass == ActiveRecord::Base ? [] : superclass.setting_writers.dup
      end
    end

    def encrypted_setting_writers
      @encrypted_setting_writers ||= begin
        superclass == ActiveRecord::Base ? [] : superclass.encrypted_setting_writers.dup
      end
    end

    def types
      [
        CampfireTarget,
        UrlTarget,
        UrlTargetV2,
        EmailTarget,
        ClickatellTarget,
        TwitterTarget,
        GetSatisfactionTarget,
        YammerTarget,
        BasecampTarget,
        JiraTarget,
        PivotalTarget,
        TwilioTarget,
        SalesforceTarget,
        SugarCrmTarget,
        FlowdockTarget,
        MsDynamicsTarget,
      ]
    end

    def with_email(email)
      where("data LIKE ?", "%email: #{email}%")
    end

    def format(*)
      :plain
    end

    def cached_success_count_key(id)
      "targets/success_counter/v1/#{id}"
    end

    def cached_failure_count_key(id)
      "targets/failure_counter/v1/#{id}"
    end

    def cached_exception_message_key(id)
      "targets/exception_message/v1/#{id}"
    end

    protected

    def settings_reader(*attrs)
      options = attrs.extract_options!
      default = options[:default]
      attrs.each do |attr|
        define_method(attr) do
          if options[:boolean]
            settings[attr] == "1"
          else
            settings[attr] || default
          end
        end
      end
    end

    def encrypted_settings_reader(*attrs)
      attrs.each do |attr|
        define_method(attr) do
          decrypted_value_for(ENCRYPTED_SETTINGS_MAP[attr])
        end
      end
    end

    def settings_writer(*attrs)
      options = attrs.extract_options!
      attrs.each do |attr|
        define_method("#{attr}=") do |value|
          settings[attr] = if options[:boolean]
            ["1", true].include?(value) ? "1" : "0"
          else
            value
          end
        end
      end

      attr_accessible(*attrs)
      setting_writers.concat(attrs).uniq!
    end

    def encrypted_settings_writer(*attrs)
      attrs.each do |attr|
        define_method("#{attr}=") do |value|
          self[ENCRYPTED_SETTINGS_MAP[attr]] = value
        end
      end

      attr_accessible(*attrs)
      encrypted_setting_writers.concat(attrs).uniq!
    end

    def settings_accessor(*attrs)
      settings_reader(*attrs)
      settings_writer(*attrs)
    end

    def encrypted_settings_accessor(*attr)
      encrypted_settings_reader(*attr)
      encrypted_settings_writer(*attr)
    end

    def remove_settings_accessor(*attrs)
      attrs.each do |attr|
        undef_method attr, "#{attr}="

        setting_writers.delete(attr)
      end
    end
  end

  def render_message(external)
    Zendesk::Liquid::TicketContext.render(external.body, external.ticket, external.author, true, external.ticket.account.translation_locale)
  end

  def settings
    self.data = {} if data.nil?
    data
  end

  def settings=(args)
    self.data = args
  end

  def kind
    self.class.name.titleize.capitalize
  end

  def edit_template
    "targets/edit/#{self.class.name.underscore}"
  end

  def is_message_supported? # rubocop:disable Naming/PredicateName
    true
  end

  def is_test_supported? # rubocop:disable Naming/PredicateName
    true
  end

  def uses_parameters?
    false
  end

  def test_no_supported_reason
    ''
  end

  def needs_authorization_upgrade?
    false
  end

  def requires_authorization?
    false
  end

  def use_oauth2?
    false
  end

  def authorized?
    true
  end

  def authorization_template
    "targets/authorize/auth_#{self.class.name.underscore}"
  end

  def authorization_failure_message
    "Authorization failed."
  end

  # External event is necessary if the target needs to replace placeholders in urls
  def send_message(_message, _external)
    raise 'Must be declared by subclass'
  end

  def retriable?
    false
  end

  def send_message_with_retry(_message, _external, _retries = 0)
    raise 'No implementation'
  end

  # Send a test message to the target.
  #
  # Subclasses of Target can override this method to provide setup and teardown used when testing.
  #
  # message  - the String message to send.
  # external - an External event.
  #
  # Returns nothing.
  def send_test_message(message, external)
    send_message(message, external)
  end

  def to_s
    raise 'Must be declared by subclass'
  end

  def activate
    self.is_active = true
    reset_failures_and_error
    save!
  end

  def deactivate(send_notification: false)
    self.is_active = false
    deliver_target_disabled_notification if send_notification
    save!
  end

  def message_failed(exception = nil, _event = nil)
    unless circuit_tripped?(exception)
      if account.has_bulk_update_targets_status?
        increment_cached_failure_count(exception)
      else
        self.failures = failures + 1
        self.error = exception.message if exception
      end
    end
    deactivate_if_over_limit(exception)
    return unless changed?
    save
  end

  def message_sent
    if account.has_bulk_update_targets_status?
      increment_cached_success_count
    else
      self.sent = sent + 1
      reset_failures_and_error
      return unless changed?
      save
    end
  end

  def increment_cached_success_count
    Rails.cache.increment(self.class.cached_success_count_key(id))
    Rails.cache.write(self.class.cached_failure_count_key(id), 0, raw: true)
  end

  def increment_cached_failure_count(exception)
    Rails.cache.increment(self.class.cached_failure_count_key(id))
    Rails.cache.write(self.class.cached_exception_message_key(id), exception.message) if exception
  end

  def reset_target_and_cache
    if new_record? || is_active_changed?
      Rails.cache.delete("#{TARGETS_TO_UPDATE_KEY}#{account.shard_id}")
    end

    if is_active_changed?
      reset_failures_and_error if is_active
      Rails.cache.delete(self.class.cached_failure_count_key(id))
      Rails.cache.delete(self.class.cached_success_count_key(id))
    end

    true
  end

  def authorize(oauth_verifier, token, secret)
    request_token = OAuth::RequestToken.new(consumer, token, secret)
    access_token  = request_token.get_access_token(oauth_verifier: oauth_verifier)

    self.encrypted_token = access_token.token
    self.encrypted_secret_value = access_token.secret
    encrypt!
    save unless new_record?
  end

  def creates_integration?
    respond_to?(:create_integration)
  end

  def max_failures_reached?
    if account.has_bulk_update_targets_status?
      current_failures > account.settings.max_target_failures
    else
      failures > account.settings.max_target_failures
    end
  end

  def encrypt!
    self.credential_updated_at = Time.now
    super
  end

  def metrics_name
    self.class.name.demodulize.underscore.to_sym
  end

  def current_failures
    if account.has_bulk_update_targets_status?
      cached_sent = Rails.cache.read(self.class.cached_success_count_key(id)).to_i
      cached_failure = Rails.cache.read(self.class.cached_failure_count_key(id)).to_i
      cached_sent > 0 ? cached_failure : failures + cached_failure
    else
      failures
    end
  end

  private

  def clear_settings
    @settings = nil
  end

  def create_integration!
    raise "Set the user via target.current_user = user before saving" unless current_user
    begin
      create_integration(current_user)
    rescue StandardError => e
      name = self.class.name.titleize.downcase
      message = I18n.t('txt.admin.controllers.targets_controller.there_was_a_problem_creating_the_triggers', name_of_the_target: name)
      errors.add(:base, message)

      ZendeskExceptions::Logger.record(e, location: self, message: "Creating #{name} integration for account '#{account.subdomain}'", fingerprint: '5d0e79f2d791770cc2ef7e5a564bb7c9caa5fc77')

      raise ActiveRecord::Rollback
    end
  end

  def destroy_other_targets
    targets = ["SalesforceTarget", "SugarCrmTarget", "MsDynamicsTarget"].reject { |target| self.class.name == target }
    targets.each { |target| account.targets.where(type: target).map(&:destroy) }
  end

  def deactivate_if_over_limit(exception)
    if max_failures_reached?
      self.is_active = false

      if account.has_bulk_update_targets_status?
        self.failures = failures + Rails.cache.read(self.class.cached_failure_count_key(id)).to_i
        self.error = exception.message if exception
        self.sent = sent + Rails.cache.read(self.class.cached_success_count_key(id)).to_i
      end

      Rails.logger.info("Deactivating Target: Account #{account.id} executed target id #{id} total sent #{sent} total failures #{failures} at #{Time.now}")
      deliver_target_disabled_notification
    end
  end

  def deliver_target_disabled_notification
    Zendesk::DB::RaceGuard.cache_guarded("target-failure/#{account.id}/#{id}") do
      TargetsMailer.deliver_target_disabled(self)
    end
  end

  def collection_allowed_changes
    ['sent', 'failures', 'error', 'is_active']
  end

  def set_credential_updated_at
    self.credential_updated_at = Time.now
  end

  def increment_current_failures(exception)
    circuit_tripped?(exception) ? current_failures : current_failures + 1
  end

  def reset_failures_and_error
    self.failures = 0
    self.error = ''
  end

  def circuit_tripped?(exception)
    !exception.nil? && exception.is_a?(::JohnnyFive::CircuitTrippedError)
  end
end
