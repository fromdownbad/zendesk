class SugarCrmTarget < Target
  after_create  :destroy_other_targets
  after_destroy :destroy_trigger

  validate :validate_sugar_crm

  def kind
    I18n.t('txt.admin.models.targets.sugar_crm_target.SugarCRM_target')
  end

  def to_s
    "SugarCRMTarget: (id: #{id})"
  end

  def is_test_supported? # rubocop:disable Naming/PredicateName
    true
  end

  def is_message_supported? # rubocop:disable Naming/PredicateName
    false
  end

  def send_message(_message, external)
    if account.sugar_crm_integration.try(:configured?)
      account.sugar_crm_integration.create_case(external.ticket)
    else
      raise "There is no SugarCRM integration configured. You need to configure a SugarCRM integration before creating this target."
    end
  end

  def create_integration(current_user)
    return if current_user.account.triggers.find_by_title(title_for_trigger)

    definition = Definition.new
    # Ticket updated
    definition.conditions_all.push(DefinitionItem.new('update_type', 'is', ['Change']))
    # Status changed to solved
    definition.conditions_all.push(DefinitionItem.new('status_id', 'value', [StatusType.SOLVED.to_s]))

    # Notify SugarCRM
    definition.actions.push(DefinitionItem.new('notification_target', 'is', [id.to_s, "{{ticket.comments_formatted}}"]))
    current_user.account.triggers.create(
      title: title_for_trigger,
      definition: definition,
      current_user: current_user,
      position: 9999,
      is_active: false
    )
  end

  protected

  def validate_sugar_crm
    unless account.sugar_crm_integration.try(:configured?)
      errors.add(:base, I18n.t('txt.admin.models.sugar_crm_target.you_need_to_configure_sugarcrm'))
    end
  end

  private

  def ticket_title(ticket)
    if ticket.subject.blank?
      ticket.description.truncate(50)
    else
      ticket.subject
    end
  end

  def title_for_trigger
    "Create SugarCRM Case when ticket is solved"
  end

  def destroy_trigger
    account.triggers.find_by_title(title_for_trigger).try(:destroy)
  end
end
