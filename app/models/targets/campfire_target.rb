require 'tinder'
class CampfireTarget < Target
  settings_accessor :subdomain, :room
  settings_accessor :ssl, :preserve_format, boolean: true
  encrypted_settings_accessor :token

  validates_presence_of :subdomain, :room, :token

  validate :validate_subdomain

  def to_s
    "CampfireTarget, subdomain: #{subdomain}, room: #{room}, token: #{token}"
  end

  def send_message(message, _external)
    raise "Can't send a blank message to the campfire room #{room} for account #{subdomain}" if message.blank?

    campfire_connection = Tinder::Campfire.new(subdomain, ssl: ssl)
    campfire_connection.login token, 'x'
    # if the token is wrong #find_room_by_name throws an exception
    the_room = campfire_connection.find_room_by_name room rescue nil

    raise I18n.t('txt.errors.targets.campfire_target.could_not_find_room_for_account', room: room, subdomain: subdomain) unless the_room

    if preserve_format
      the_room.paste message
    else
      the_room.speak message
    end
    the_room.leave
  end

  def kind
    I18n.t('txt.admin.models.targets.campfire_target.Campfire_target')
  end

  private

  def validate_subdomain
    if subdomain.present? && subdomain !~ /^[a-z0-9-]+$/i
      errors.add(:subdomain, I18n.t('txt.errors.targets.campfire_target.subdomain_errors.is_invalid_only_type_in_the_subdomain'))
    end
  end
end
