require 'zendesk/encryptable'

class TwitterTarget < Target
  attr_accessor :callback_url
  encrypted_settings_accessor :token, :secret

  validates_presence_of :token, :secret

  def authorized?
    @authorized ||= valid_credentials?
  end

  def kind
    I18n.t('txt.admin.models.targets.twitter_target.Twitter_target')
  end

  def send_message(message, _external)
    if authorized?
      send_message_with_oauth(message)
    end
  end

  def send_test_message(message, external)
    message += " #{Kernel.rand(10_000)}"
    super(message, external)
  end

  def requires_authorization?
    true
  end

  def to_s
    "TwitterTarget: (id: #{id})"
  end

  ### Twitter client related methods

  def send_message_with_oauth(message)
    client.tweet(message.truncate(140))
  end

  def valid_credentials?
    if token.blank? || secret.blank?
      false
    else
      !client.verify_credentials.key?(:api_error)
    end
  end

  ### end

  ### Oauth authentication related methods

  def request_token
    @request_token ||= begin
      raise "Must provide a callback_url" unless callback_url

      consumer.get_request_token(oauth_callback: callback_url)
    end
  end

  def access_token
    @access_token ||= OAuth::AccessToken.new(consumer, token, secret)
  end

  def authorize_url
    request_token.authorize_url
  end

  def authorization_failure_message
    I18n.t("txt.access.login.twitter_error")
  end

  ### end

  private

  ### Twitter client related methods
  def client
    # TODO: Channels.  TwitterApi::Client is defined in the zendesk_channels repo.
    @client ||= ::Channels::TwitterApi::Client.new(access_token: token, secret: secret)
  end

  ### Oauth authentication related methods
  def consumer
    profile = Zendesk::Auth::OAUTH_PROFILES[:twitter]
    @consumer ||= OAuth::Consumer.new(profile[:key], profile[:secret], site: profile[:site])
  end
end
