require_dependency 'zendesk/salesforce'
require_dependency 'zendesk/salesforce/integration'
require_dependency 'zendesk/crm/integration'
require_dependency 'zendesk/salesforce/session'

class SalesforceTarget < Target
  include Salesforce::Session

  PARTIAL_MODE = "0".freeze
  FULL_MODE    = "1".freeze

  attr_accessor :callback_url
  settings_accessor :ticket_information_mode
  encrypted_settings_accessor :token, :secret

  after_create  :destroy_other_targets
  after_destroy :destroy_trigger

  def to_s
    "SalesforceTarget: (id: #{id})"
  end

  def kind
    I18n.t('txt.admin.models.targets.salesforce_target.Salesforce_target')
  end

  def consumer
    @oauth_client ||= OAuth::Consumer.new(
      Zendesk::Configuration.dig!(:salesforce, :consumer_key),
      Zendesk::Configuration.dig!(:salesforce, :consumer_secret),
      Zendesk::Configuration.dig!(:salesforce, :oauth_options).symbolize_keys
    )
  end

  def requires_authorization?
    true
  end

  def authorized?
    use_integration_keys? || [token, secret].all? { |k| !k.blank? }
  end

  def authorize_url
    request_token.authorize_url(oauth_consumer_key: Zendesk::Configuration.dig!(:salesforce, :consumer_key))
  end

  def authorization_failure_message
    "Authorization to Salesforce failed."
  end

  def request_token
    return @request_token if defined?(@request_token)

    raise "Must provide a callback_url" unless callback_url

    @request_token = consumer.get_request_token(oauth_callback: https_callback_url)
  end

  def access_token
    @access_token ||= OAuth::AccessToken.new(consumer, token, secret)
  end

  def is_message_supported? # rubocop:disable Naming/PredicateName
    false
  end

  def use_rest_api?
    ticket_information_mode == FULL_MODE
  end

  def send_message(_message, external)
    if authorized?
      session_info = get_salesforce_session

      if use_rest_api?
        send_json_message(session_info, external.ticket)
      else
        send_xml_message(session_info, external.ticket)
      end
    end
  end

  def send_json_message(session_info, ticket)
    json = ticket_json_for_salesforce(ticket)

    if use_integration_keys?
      # This is for system targets, use the integration to send the post request
      options = {
        headers: { 'Content-Type' => 'application/json' },
        body: json
      }

      account.salesforce_integration.http_post("/services/apexrest/Zendesk/RestApi/", options)
    else
      # This is for old non system targets using their own keys
      uri = URI.parse(session_info[:server_url])

      conn = Faraday.new(url: "#{uri.scheme}://#{uri.host}/services/apexrest/Zendesk/RestApi/")
      conn.post do |req|
        req.headers['Content-Type'] = 'application/json'
        req.headers['Authorization'] = "OAuth #{session_info[:session_id]}"
        req.body = json
      end
    end
  end

  def send_xml_message(session_info, ticket)
    driver = Salesforce::Integration::Driver.get_driver(session_info)

    ticket_xml = Salesforce::Integration::SyncTicketData.new(Zendesk::Crm::Integration.ticket_xml(ticket))

    driver.syncTicketData(ticket_xml)
  end

  def create_integration(current_user)
    return if find_trigger

    definition = Definition.new
    # Ticket updated
    definition.conditions_all.push(DefinitionItem.new('update_type', 'is', ['Change']))
    # Status changed to solved
    definition.conditions_all.push(DefinitionItem.new('status_id', 'value', [StatusType.SOLVED.to_s]))

    # Notify Salesforce
    definition.actions.push(DefinitionItem.new('notification_target', 'is', [id.to_s, ""]))
    current_user.account.triggers.create(title: title_for_trigger, definition: definition, current_user: current_user, position: 9999, is_active: false, feature_identifier: 'salesforce_integration')
  end

  def use_integration_keys?
    is_system_target? && account.salesforce_integration.try(:configured?)
  end

  private

  def https_callback_url
    callback_url.gsub(/^http:/, 'https:')
  end

  def get_salesforce_session # rubocop:disable Naming/AccessorMethodName
    if use_integration_keys?
      account.salesforce_integration.get_salesforce_session
    else
      get_session(account, access_token, false)
    end
  end

  def ticket_xml_for_salesforce(ticket)
    options = {
      include: {
        requester: {
          include: :organization
        },
        ticket_field_entries: {},
        comments: {
          include: Comment.new.send(:serialization_options)[:include],
          only: Comment.new.send(:serialization_options)[:only]
        },
        linkings: {}
      }
    }
    ticket.to_xml(options)
  end

  def ticket_json_for_salesforce(ticket)
    original_as_config = ActiveSupport.use_standard_json_time_format
    ActiveSupport.use_standard_json_time_format = true

    json = Time.use_zone("UTC") { json_presenter.present(ticket).to_json }
    ActiveSupport.use_standard_json_time_format = original_as_config

    json
  end

  def json_presenter
    Api::V2::SalesforcePresenter.new(account.owner, url_builder: UrlBuilder.new(account))
  end

  def title_for_trigger
    "Create a record in Salesforce when ticket is solved"
  end

  def find_trigger
    old_title = "Create Salesforce Case when ticket is solved"
    titles = [title_for_trigger, old_title]

    account.triggers.where(title: titles).first
  end

  def destroy_trigger
    find_trigger.try(:destroy)
  end

  class UrlBuilder
    # Use this module instead after upgrading to rails 3:
    # include Rails.application.routes.url_helpers
    include Rails.application.routes.url_helpers
    def initialize(account)
      default_url_options.merge!(account.url_params)
    end
  end
end
