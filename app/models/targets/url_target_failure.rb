class UrlTargetFailure < ActiveRecord::Base
  include RawHttp
  belongs_to :account
  belongs_to :target, foreign_key: :url_target_v2_id, class_name: 'UrlTargetV2'
  belongs_to :event, foreign_key: :external_id, class_name: 'External'

  scope :latest, -> { order("created_at DESC").limit(10) }
  scope :with_target, -> { includes(:target) }

  validate :ensure_target_still_exists

  attr_accessible

  attr_accessor :raw_http_capture, :response_body

  after_create :clean_up_url_target_failures

  URL_TARGET_FAILURES_LIMIT = 25

  private

  def ensure_target_still_exists
    raise ActiveRecord::RecordNotFound unless target && target.reload
  rescue ActiveRecord::RecordNotFound
    errors.add(:target, "must exist")
  end

  def clean_up_url_target_failures
    target.url_target_failures.order('created_at DESC').offset(URL_TARGET_FAILURES_LIMIT).destroy_all
  end
end
