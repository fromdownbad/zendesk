require 'zendesk/targets/url_target_v2_client'
require 'zendesk/targets/metrics/statsd_client'
require 'raw_net_capture'

class UrlTargetV2 < UrlTarget
  include TargetsMetrics::StatsDClient

  LAUNCH_DATE = Date.new(2014, 12, 3)
  METHODS = ['get', 'put', 'post', 'patch', 'delete'].freeze
  CONTENT_TYPES = {
    "JSON" => 'application/json',
    "XML" => 'application/xml',
    "Form encoded" => 'application/x-www-form-urlencoded'
  }.freeze

  FORMAT_TYPES = {
    CONTENT_TYPES['JSON']         => :json,
    CONTENT_TYPES['XML']          => :xml,
    CONTENT_TYPES['Form encoded'] => :form
  }.freeze

  private_constant :FORMAT_TYPES

  has_many :url_target_failures, dependent: :destroy, inherit: :account_id

  before_validation :clear_content_type, unless: :needs_content_type?
  validates_presence_of :username, if: proc { password.present? }
  validates :method, inclusion: { in: METHODS }
  validates :content_type, inclusion: { in: CONTENT_TYPES.values }, if: :needs_content_type?

  attr_accessor :response_code, :response_body, :http_transactions, :exception_message

  settings_accessor :url, :method, :content_type, :username
  encrypted_settings_accessor :password

  remove_settings_accessor :attribute

  def self.format(content_type, method)
    return :query if method == 'get'

    FORMAT_TYPES.fetch(content_type, :none)
  end

  def kind
    I18n.t('txt.admin.models.targets.url_target.HTTP_target')
  end

  def default_max_retries_threshold
    (ENV['HTTP_TARGETS_MAX_ERR_RETRIES'] || 5).to_i
  end

  def send_message_with_retry(message, event, retries = 0)
    Zendesk::UrlTargetV2Client.new(target: self, message: message, event: event).invoke.tap do |response|
      if event.ticket.account.has_log_targets_v2_requests?
        message = "target: #{id} response - status: #{response.status}, body: #{response.body}"

        unless response.headers.nil?
          message += ", headers: #{response.headers}"
        end

        Rails.logger.info(message)
      end

      statsd_client.increment("response.status_code", tags: %W[source:#{metrics_name} status_code:#{response.status}])
      if !response.success?
        retry_sleep_time = calculate_sleep_time(response, retries)
        if event.ticket.account.has_ticket_target_retry? && retry_sleep_time
          retries += 1
          statsd_client.histogram("retry.backoff_time", retry_sleep_time, tags: %W[source:#{metrics_name}])
          statsd_client.increment("retry.count", tags: %W[source:#{metrics_name}])
          TargetJob.enqueue_in(retry_sleep_time, account_id, event.id, retries)
        else
          set_response_values(response)
          raise HTTPResponseStatusError, I18n.t("txt.errors.targets.url_target_v2.http_call_failed",
            locale: event.ticket.account.translation_locale)
        end
      elsif testing?
        statsd_client.increment("count.test.success", tags: %W[source:#{metrics_name}])
        set_response_values(response)
      end
    end
  end

  def retriable?
    true
  end

  def send_test_message(message, event)
    @testing = true
    send_message_with_retry(message, event)
    self.http_transactions = raw_http_capture.transactions
  rescue StandardError => exception
    self.exception_message = exception.message
    self.http_transactions = raw_http_capture.transactions
  ensure
    @testing = false
  end

  def render_message(external)
    message = external.body

    if uses_parameters? && message.is_a?(Array)
      message.map do |key, value|
        [
          Zendesk::Liquid::TicketContext.render(key, external.ticket, external.author, true, requester_locale(external.ticket)),
          Zendesk::Liquid::TicketContext.render(value, external.ticket, external.author, true, requester_locale(external.ticket))
        ]
      end
    else
      if content_type == CONTENT_TYPES["JSON"]
        message = add_json_filter_to_placeholders(message)
      end

      Zendesk::Liquid::TicketContext.render(message, external.ticket, external.author, true, requester_locale(external.ticket))
    end
  end

  def requester_locale(ticket)
    ticket.requester ? ticket.requester.translation_locale : ticket.account.translation_locale
  end

  def message_failed(exception, event)
    build_url_target_failure(exception, event)
    super(exception, event)
  end

  def uses_parameters?
    method == "get" || content_type == CONTENT_TYPES["Form encoded"]
  end

  def needs_content_type?
    ['delete', 'post', 'put', 'patch'].include?(method)
  end

  def edit_template
    'targets/edit/http_target'
  end

  private

  def random_backoff(retry_number)
    if retry_number < 2
      [1, 2].sample
    else
      rand(2**retry_number - 1)
    end
  end

  def calculate_sleep_time(response, retries)
    return if testing?
    return unless retries < default_max_retries_threshold
    case response.status
    when 409
      random_backoff(retries)
    when 429, 503
      return unless response.headers[:retry_after]
      return unless response.headers[:retry_after].to_i < 60
      response.headers[:retry_after].to_i
    end
  end

  def build_url_target_failure(exception, event)
    url_target_failures.build do |new_fail|
      new_fail.account_id = account_id
      new_fail.event = event
      new_fail.status_code = response_code
      new_fail.exception = exception.class.to_s
      new_fail.message = exception.message
      new_fail.consecutive_failure_count = increment_current_failures(exception)
      new_fail.raw_http_capture = raw_http_capture
      new_fail.response_body = response_body
    end
    save
  end

  def clear_content_type
    self.content_type = ''
  end

  def testing?
    !!@testing
  end

  def add_json_filter_to_placeholders(message)
    message.try(:gsub, /{{(.*?)}}/, '{{\1 | json}}')
  end

  def statsd_client
    @statsd_client ||= create_statsd_client
  end
end
