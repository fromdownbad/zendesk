class OrganizationDomain < ActiveRecord::Base
  include Zendesk::Organization::SharedLogic

  alias_attribute :domain_name, :value
  attr_accessible :domain_name

  def self.valid_values(values)
    values.select { |value| DOMAIN_PATTERN.match(value) }
  end

  private

  # Based on Organizaton#validate_domain_names
  def validate_value
    unless DOMAIN_PATTERN.match(value) && value == value.downcase
      error = I18n.t('activerecord.errors.models.organization_domain.attributes.value.invalid', value: value)
      errors.add(:domain_name, error)
    end
  end
end
