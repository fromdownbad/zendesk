class AccountServiceSubscription
  attr_reader :account

  PRODUCT_ATTRIBUTES = %i[
    state
    trial_expires_at
  ].freeze

  DATETIME_ATTRIBUTES = %i[
    trial_expires_at
  ].freeze

  delegate :trial?,
    :expired?,
    :cancelled?,
    :subscribed?,
    :state,
    :trial_expires_at,
    :id,
    to: :remote,
    allow_nil: true

  private_class_method :new

  class << self
    def find_for_account(account)
      subscription = new(account)
      subscription.remote ? subscription : nil
    end

    def create_for_account(account, attrs = {})
      new(account).tap do |subscription|
        subscription.update_attributes(attrs)
      end
    end

    private

    def define_attr_accessors
      self::PLAN_SETTINGS.each do |attribute|
        define_method attribute do
          plan_settings[attribute.to_s]
        end
      end
    end
  end

  def remote
    @remote ||= begin
      account_service_client.product(self.class::PRODUCT_NAME)
    rescue Kragle::BadGateway
      nil
    end
  end

  def update_attributes(attrs)
    payload = account_service_payload(attrs)
    account_service_client.update_or_create_product!(self.class::PRODUCT_NAME, payload, context: "account_service_subscription")

    reload
    true
  end

  def reload
    @remote = nil
    remote
    self
  end

  def cancel!
    update_attributes(state: Zendesk::Accounts::Product::CANCELLED)
  end

  private

  attr_reader :attributes

  def initialize(account)
    @account = account
  end

  def account_service_client
    @account_service_client ||= Zendesk::Accounts::Client.new(account)
  end

  def account_service_payload(attributes)
    validate_attributes(attributes)
    account_service_hash = { product: { plan_settings: {} } }
    attributes.symbolize_keys.each do |key, value|
      # Account Service requires dates in ISO-8601
      value = value.iso8601 if date_time?(key)

      if self.class::PRODUCT_ATTRIBUTES.include?(key)
        account_service_hash[:product][key] = value
      else
        account_service_hash[:product][:plan_settings][key] = value
      end
    end
    account_service_hash
  end

  def validate_attributes(attributes)
    attributes.symbolize_keys.each do |key, value|
      validate_attribute(key, value)
    end
  end

  def validate_attribute(attr_name, attr_value)
    validate_state(attr_value) if attr_name == :state
    all_attrs = self.class::PLAN_SETTINGS + PRODUCT_ATTRIBUTES
    return if all_attrs.include?(attr_name)

    fail ArgumentError, "Not a valid attribute: #{attr_name}"
  end

  def validate_state(value)
    return if Zendesk::Accounts::Product::STATUSES.include?(value)
    fail ArgumentError, 'Not a valid state'
  end

  def date_time?(key)
    self.class::DATETIME_ATTRIBUTES.include?(key)
  end

  def plan_settings
    remote.try(:plan_settings) || {}
  end
end
