# A suspended ticket is an email update/insert for Zendesk which got rejected. It
# has causes, subject, content, from_name, from_email and related suspended attachments.
# Optionally, it has a ticket_id if it refers an exising ticket (update) and optionally
# it has an author if a such could be determined.
class SuspendedTicket < ActiveRecord::Base
  prepend ::Zendesk::InboundMail::Truncation::SuspendedTicket

  include AttachmentsHelper
  include CachingObserver
  include Zendesk::Tickets::CollaborationSupport
  include Zendesk::CreditCardRedaction
  credit_card_redaction_attrs :content, :subject

  extend Zendesk::CappedCounts

  belongs_to :account
  belongs_to :author, class_name: 'User', inherit: :account_id
  belongs_to :ticket,
    primary_key: :nice_id,
    inherit: :account_id

  has_many   :attachments, -> { where(parent_id: nil) }, dependent: :destroy, as: :source, inherit: :account_id

  attr_accessible :account, :author, :cause, :client, :content, :from_mail,
    :from_name, :properties, :subject, :via_id, :recipient, :ticket

  attr_writer :application_logger

  has_soft_deletion
  default_scope { where(deleted: false) }

  serialize :properties

  truncate_attributes_to_limit :client, :content, :from_name, :from_mail, :message_id, :subject

  validates_presence_of :account_id, :from_name, :from_mail, :cause
  validate :raw_content_presence

  before_create :initialize_defaults
  before_validation :redact_credit_card_numbers!
  after_commit :deliver_verification_when_signup_required, unless: :disable_triggers?, on: :create

  def self.find_by_token(token)
    where(token: token.to_s).first
  end

  attr_accessor :uploads

  def title
    (subject.presence || content).to_s.squish.truncate(200)
  end

  def content(raw: false)
    if !raw && html?
      @plain_content ||= Zendesk::MtcMpqMigration::Content::HTMLPlainText.convert(super())
    else
      super()
    end
  end

  def content=(value)
    @plain_content = nil
    super
  end

  def translation_locale
    if properties && properties[:locale_id].present?
      TranslationLocale.find_by_id(properties[:locale_id])
    else
      account.translation_locale
    end
  end

  def deliver_verification_when_signup_required
    if cause == SuspensionType.SIGNUP_REQUIRED
      user = account.find_user_by_email(from_mail)
      if Zendesk::UserPortalState.new(account).send_suspend_verify_email?(user)
        UsersMailer.deliver_suspend_verify(self)
      end
    end
  end

  def to_ticket(current_user = nil)
    account.tickets.new do |t|
      t.description = full_content

      t.comment.html_body = full_content(raw: true) if html?

      t.via_id      = via_id
      t.subject     = (subject.present? && account.field_subject.is_active? ? subject : '')

      if recoverable?
        t.requester = author || build_author
        if properties.try(:[], :from_zendesk)
          requester_email_identity = find_user_email_identity_matching_from_mail(t.requester)
          requester_email_identity.try(:mark_as_machine)
        end
      end
      t.submitter    = current_user
      t.recipient    = recipient
      t.client       = client
      t.original_recipient_address = original_recipient_address

      if properties.present?
        # must pull out fields and set them after setting properties so
        # set_tags doesn't overwrite them
        fields = properties.delete(:fields) if properties.key?(:fields)
        t.disable_triggers = disable_triggers?
        t.ticket_form_id = properties[:ticket_form_id] if properties[:ticket_form_id]
        t.brand_id = brand_id
        t.due_date = properties[:due_date] if properties[:due_date]
        t.via_reference_id = properties[:via_reference_id] if properties[:via_reference_id]
        t.properties = properties.except(:disable_triggers, :ticket_form_id, :brand_id, :machine, :via_reference_id)
        t.fields = fields

        # TODO: CCs - Eventually this will handle restoring :mail_ccs and :followers, based on an account
        # setting agent_ccs_become_followers <-- proposed name
        if metadata.try(:[], :ccs).try(:any?)
          Zendesk::InboundMail::Ticketing::MailCollaboration.build_all(
            t,
            collaborator_addresses: metadata[:ccs].map { |cc| Zendesk::Mail::Address.new(cc) }
          )
        end
      end

      # Copy screencasts back to ticket.
      # Ticket will eventually copy them in audit.metadata[:system] on a before_save.
      if screencasts.present?
        t.screencasts = screencasts
      end
    end
  end

  def redacted!
    self.properties ||= {}
    properties[:redacted] = true
  end

  def redacted?
    properties.present? && properties[:redacted] == true
  end

  def add_flags(new_flags, new_flags_options)
    self.properties ||= {}
    properties[:flags] = new_flags
    properties[:flags_options] = new_flags_options if new_flags_options.present?
  end

  def flags
    properties[:flags] if properties && properties[:flags].present?
  end

  def flags_options
    (properties && properties[:flags_options]) || {}
  end

  def metadata
    properties[:metadata] if properties.present?
  end

  def metadata=(metadata)
    self.properties ||= {}
    properties[:metadata] = metadata
  end

  def set_error_messages(messages) # rubocop:disable Naming/AccessorMethodName
    self.properties ||= {}
    self.properties[:error_messages] = messages.map(&:to_s) # to_s so that we can dump to YAML without issue
  end

  def get_error_messages # rubocop:disable Naming/AccessorMethodName
    self.properties[:error_messages] if self.properties.present?
  end

  def screencasts=(screencasts)
    self.properties ||= {}
    self.properties[:screencasts] = screencasts
  end

  def screencasts
    properties[:screencasts] if properties.present?
  end

  def brand=(brand)
    self.properties ||= {}
    properties[:brand_id] = brand.id if brand
  end

  def brand_id
    properties[:brand_id] if properties && properties[:brand_id]
  end

  # use this to copy attachments to a new upload token, e.g. when manually recovering a suspended ticket
  def copy_attachments_to_upload_token
    return nil unless attachments.present?
    new_token = account.upload_tokens.create(source: account)
    attachments.each do |attachment|
      new_attachment = Attachment.from_attachment(attachment)
      next if new_attachment.nil?
      new_attachment.source = new_token
      new_attachment.save
    end
    new_token
  end

  # used by maintenance job
  def self.cleanup
    scope = where("suspended_tickets.created_at < ?", 2.weeks.ago)
    account_ids = scope.select('distinct account_id').pluck(:account_id)
    account_ids -= Account.shard_local.shard_locked.pluck(:id)

    with_deleted do
      account_ids.each do |account_id|
        to_delete = scope.where(account_id: account_id).pluck(:id)
        Rails.logger.info("About to delete #{to_delete.size} suspended tickets for #{account_id}")

        to_delete.each_slice(500) do |ids|
          begin
            to_delete = where(id: ids).includes(:attachments)
            with_attachments, without_attachments = to_delete.partition { |st| st.attachments.any? }

            Rails.logger.info("About to delete #{with_attachments.size} suspended tickets with attachments for #{account_id}")
            with_attachments.each do |st|
              Rails.logger.info("Destroying suspended ticket id: #{st.id} account_id: #{account_id}")
              begin
                st.destroy
              rescue StandardError
                puts "Error deleting attachment #{st.id} -- #{$!}"
                st.delete
              end
            end

            ids = without_attachments.map(&:id)
            Rails.logger.info("About to delete #{without_attachments.size} suspended tickets without attachments for #{account_id} (#{ids.join(", ")})")
            where(id: ids).delete_all if ids.any?
          rescue Zendesk::Accounts::Locking::LockedException, StandardError
            Rails.logger.error("error cleaning up suspended tickets #{$!}")
            ZendeskExceptions::Logger.record($!, location: self, message: "error cleaning up suspended tickets", reraise: !Rails.env.production?, fingerprint: 'cf711388031faad9a443679c57a3d00a6125c19a')
          end
        end
      end
    end
  end

  def self.mark_as_soft_deleted_sql
    ["deleted = ?, updated_at = ?", true, Time.now]
  end

  def mark_as_deleted
    self.deleted = true
  end

  def mark_as_undeleted
    self.deleted = false
  end

  def deleted?
    deleted
  end

  def self.undelete(account, since_date)
    stamp = since_date.beginning_of_day.in_time_zone(account.time_zone).to_s(:db)
    account.on_shard do
      connection.update("UPDATE suspended_tickets SET deleted = 0 WHERE account_id = #{account.id} AND deleted = 1 AND updated_at > '#{stamp}'")
    end
  end

  def self.bulk_delete(account, ids)
    account.suspended_tickets.soft_delete_all!(ids)
    account.expire_scoped_cache_key(:views)
  end

  # hackish little way to sidestep the fact that named scopes won't override default scopes
  def self.with_deleted(&block)
    unscoped.scoping(&block)
  end

  def recoverable?
    !account.recipient_email?(from_mail)
  end

  def ticket_requester_suspended?
    return false unless ticket
    ticket.requester && ticket.requester.id && ticket.requester.suspended?
  end

  def recover(current_user, recover_as_this_user = nil, run_spam_detector = false)
    return unless recoverable?
    self.author = recover_as_this_user || build_author
    ticket = self.ticket

    if account.has_email_malware_recovery_metric? && cause == SuspensionType.MALWARE_DETECTED
      recovery_creates_tag = ticket ? 'comment' : 'new_ticket'

      Rails.logger.info "SuspensionType.MALWARE_DETECTED recovery: User ##{current_user.id} attempting to recover SuspendedTicket ##{id} from #{from_mail} (Message-ID: #{message_id}) as a #{ticket ? "comment on ticket ##{ticket.id}" : 'new ticket'}."
      suspended_ticket_statsd_client.increment(:malware_recovery, tags: ["recovery_creates:#{recovery_creates_tag}"])
    end

    if ticket.nil? || ticket.closed?
      ticket = to_ticket(current_user)
      return if run_spam_detector && Fraud::SpamDetector.new(ticket, false).suspend_ticket?
    else
      # only assign the current_user when ticket exists
      #
      # The main reason for setting the ticket.current_user here is because we
      # are using MailCollaboration and it requires us to define the
      # current_user on the ticket.
      #
      # Why set it to author?
      # The MailCollaborationUpdateRule rules should be evaluated WRT the
      # author of the suspended ticket so that the new comment (and adding CC)
      # will be done correctly based on what the author is allowed to do and
      # how they are collaborating on the ticket
      #
      # reset the current_user to previous value to avoid disrupting any
      # unrelated computations that may happen outside this method
      existing_current_user_on_ticket = ticket.current_user

      begin
        ticket.current_user = author

        apply_attributes_to_existing_ticket
      rescue StandardError => error
        Rails.logger.info(
          "Error applying attributes to existing ticket: ticket_id: {#{ticket.id}},\
          suspended_ticket_id: #{id}, error:#{error.message}"
        )
      ensure
        ticket.current_user = existing_current_user_on_ticket
      end
    end

    ticket_changer = if author_can_add_comment?
      author
    else
      Rails.logger.info "SuspendedTicket#recover: Author '#{author.name}' " \
      "(#{author.email}) cannot add comment, so setting ticket_changer " \
      "(audit.author) to be ticket.requester."
      ticket.requester
    end
    comment = ticket.comment
    comment.created_at = created_at
    comment.updated_at = updated_at
    comment.attachments += attachments
    comment.author = author
    if properties && properties[:redacted]
      comment.build_redaction_event
      ticket.add_redaction_tag!
    end

    set_author_locale

    mark_as_ham if message_id
    report_ham(current_user) if mail_spam? && !Rails.env.development?
    ticket.will_be_saved_by(ticket_changer, via_id: via_id)

    ticket.audit.metadata[:system].merge!(metadata.except(:ccs)) if metadata
    ticket.audit.flag!(flags, flags_options) if flags
    ticket.audit.flag!([EventFlagType.OTHER_USER_UPDATE]) if author != ticket_changer

    if ticket.valid? && !ticket_requester_suspended?
      ticket.set_nice_id

      done = transaction do
        raise ActiveRecord::Rollback unless ticket.save

        if agent_comment = properties.try(:[], :agent_comment)
          submitter = account.agents.find(agent_comment.fetch(:submitter_id))
          ticket = Zendesk::InboundMail::Ticketing::MailTicket.create_agent_comment_on_ticket(account, ticket, agent_comment.fetch(:value), submitter, agent_comment.fetch(:is_public))
        end

        rows_deleted = self.class.delete(id)
        if rows_deleted == 0
          ticket.errors.add(:base, I18n.t('txt.admin.models.suspended_ticket.suspended_ticket_already_recovered'))
          raise ActiveRecord::Rollback
        end

        true
      end

      if done
        Rails.logger.info("Recovered SuspendedTicket #{id} (cause #{SuspensionType[cause].try(:score)}) recovered as comment #{comment.id} in ticket #{ticket.id} with author #{author.try(:id)} #{author.try(:email)} for account #{account.subdomain} by #{current_user.id} #{current_user.email}")
        return ticket
      end
    end

    ticket.errors.add(:requester, :suspended) if ticket_requester_suspended?
    set_error_messages(ticket.errors.full_messages)
    save!
    Rails.logger.error("Failed to recover SuspendedTicket #{id} (cause #{SuspensionType[cause].try(:score)}) for account #{account.subdomain} by #{current_user.id} #{current_user.email} with errors: #{ticket.errors.full_messages}")
    nil
  end

  def update_with_unknown_author?
    ticket.present? && author.blank?
  end

  def potential_recover_as_users
    users = []

    if ticket
      users << ticket.requester if ticket.requester
      users += ticket.collaborators if ticket.collaborators.any?
    end
    users.reject!(&:is_agent?)
    users
  end

  protected

  def full_content(**options)
    (account.field_subject.is_active? || subject.blank?) ? content(options) : "#{subject}: #{content(options)}"
  end

  def disable_triggers?
    !!properties.try(:[], :disable_triggers)
  end

  def initialize_defaults
    self.token = Token.generate if token.blank?
    self.via_id ||= ViaType.MAIL

    if uploads.present?
      account.upload_tokens.find_all_by_value(uploads).each do |upload_token|
        if upload_token.attachments.present?
          self.attachments += upload_token.attachments
        end
      end
    end
  end

  def apply_attributes_to_existing_ticket
    is_public = (author.is_agent? ? account.is_email_comment_public_by_default : true)
    is_public = false if comment_needs_to_be_private?

    comment_type = (html? ? :html_body : :body)

    ticket.add_comment(is_public: is_public, comment_type => full_content(raw: true))

    if properties.present?
      ticket.fields         = properties[:fields]         if properties[:fields].present?
      ticket.priority_id    = properties[:priority_id]    if properties[:priority_id].present?
      ticket.ticket_type_id = properties[:ticket_type_id] if properties[:ticket_type_id].present?
      ticket.via_reference_id = properties[:via_reference_id] if properties[:via_reference_id].present?
    end

    if account.has_comment_email_ccs_allowed_enabled?
      build_mail_collaborators
    elsif account.is_collaboration_enabled? && ticket.requester != author && !ticket.is_follower?(author)
      author.save if author.new_record?

      # Add the author as collaborator
      ticket.set_collaborators = ticket.follower_ids + [author.id.to_s]
    end

    ticket.client = client if client
  end

  def mark_as_ham
    if email = account.inbound_emails.where(message_id: message_id).first
      email.mark_as_ham!
    else
      Rails.logger.error("Could not find associated email: #{message_id}")
    end
  end

  def mail_spam?
    raw_email_identifier.present? && (cause == SuspensionType.SPAM || cause == SuspensionType.MALICIOUS_PATTERN_DETECTED)
  end

  def html?
    properties && properties[:html]
  end

  def report_ham(current_user)
    CloudmarkFeedbackJob.report_ham(current_user, raw_email_identifier)
    RspamdFeedbackJob.report_ham(user: current_user, identifier: raw_email_identifier, recipient: recipient) if account.has_email_rspamd_training_enabled?
  end

  def raw_email_identifier
    return if metadata.blank?
    @raw_email_identifier ||= metadata[:raw_email_identifier]
  end

  def set_author_locale
    if properties.present? && properties[:locale_id].present? && author.present? && author.locale_id.nil?
      author.locale_id = properties[:locale_id]
      author.save!
    end
  end

  def comment_needs_to_be_private?
    reasons_to_be_private = []
    reasons_to_be_private << :author_is_light_agent          if author.is_light_agent?
    reasons_to_be_private << :author_is_third_party          if author_is_third_party?
    reasons_to_be_private << :requester_is_not_ccd           if requester_not_ccd_and_comment_email_ccs_enabled?
    reasons_to_be_private << :author_cant_add_public_comment if author_cant_add_public_comment?

    Rails.logger.info "[SuspendedTicket] Comment from suspended ticket ##{id} needs to be private because #{reasons_to_be_private.to_sentence}" if reasons_to_be_private.any?
    reasons_to_be_private.any?
  end

  def redact_credit_card_numbers!
    redacted! if super
  end

  def build_author
    @built_author ||=
      account.user_email_identities.find_by_email(from_mail).try(:user) ||
      account.users.new(name: from_name, email: from_mail)
  end

  private

  def author_cant_add_public_comment?
    !author.can?(:publicly, Comment)
  end

  def author_can_add_comment?
    return false unless ticket
    author_is_agent_who_can_add_comment? || (ticket.requester == author) || ticket.is_follower?(author) || ticket.is_email_cc?(author)
  end

  def author_is_agent_who_can_add_comment?
    author.is_agent? && author.can?(:add_comment, ticket)
  end

  def author_is_third_party?
    return false unless account.has_email_make_other_user_comments_private?

    author.is_end_user? && !ticket.is_participant?(author)
  end

  def raw_content_presence
    errors.add(:content, "Raw content must be present") unless content(raw: true).present?
  end

  def build_mail_collaborators
    return unless metadata.try(:[], :ccs).present?

    Zendesk::InboundMail::Ticketing::MailCollaboration.build_all(
      ticket,
      collaborator_addresses: metadata[:ccs].map do |collaboration|
        Zendesk::Mail::Address.new(collaboration)
      end,
      requester_is_on_email: requester_on_suspended_email?,
      recovering_suspended_ticket: true
    )
  end

  def requester_not_ccd_and_comment_email_ccs_enabled?
    account.has_comment_email_ccs_allowed_enabled? && !requester_on_suspended_email?
  end

  # The metadata[:ccs] contains not just `Cc:` but also the `To:` address
  # on the email except the +id address
  def email_addresses_on_suspended_ticket
    return [] unless metadata.try(:[], :ccs).present?

    Rails.logger.info "[SuspendedTicket] Email addresses on suspended ticket ##{id}: #{metadata[:ccs]}"
    metadata[:ccs].map { |c| c[:address] }
  end

  # Check if the existing ticket's requester was present on the email that
  # resulted in suspended ticket
  def requester_on_suspended_email?
    requester = ticket.requester

    user_email_identity = find_user_email_identity_matching_from_mail(requester)
    return true if user_email_identity

    email_addresses_on_suspended_ticket.any? do |address|
      requester.emails_include?(address)
    end
  end

  # Provides a way to route logs to the correct logger instance. E.g. MTC
  def application_logger
    @application_logger || Rails.logger
  end

  def find_user_email_identity_matching_from_mail(user)
    user.identities.find { |i| i.is_a?(UserEmailIdentity) && i.value == from_mail }
  end

  def suspended_ticket_statsd_client
    @suspended_ticket_statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'suspended_ticket')
  end
end
