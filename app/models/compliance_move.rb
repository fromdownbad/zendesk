class ComplianceMove < ActiveRecord::Base
  not_sharded
  disable_global_uid

  belongs_to :account
  has_many :subscription_feature_addons

  attr_accessible :account, :account_subdomain, :src_shard_id, :src_pod_id, :move_type, :move_scheduled

  validates_presence_of :account
  validates_presence_of :move_type

  scope :waiting, -> { where(move_scheduled: false) }

  # [regular][advanced_security][advanced_security] is advanced_security-only
  # [pci][regular][pci] is pci-only
  MOVE_TYPE_IDS = {
    us: 1,
    eu: 2,
    de: 3
  }.freeze

  TAG_NAMES = {
    us: 'US Data Locality',
    eu: 'EU Data Locality',
    de: 'Germany Data Locality'
  }.freeze
end
