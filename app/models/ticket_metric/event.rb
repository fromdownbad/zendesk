require 'zendesk/ticket_metric'

module TicketMetric
  class Event < ActiveRecord::Base
    TRANSITIONS = %i[activate fulfill pause].freeze

    TYPES = [*TRANSITIONS, :apply_sla, :breach, :measure, :update_status].freeze

    NO_INSTANCE = 0

    self.table_name = :ticket_metric_events

    attr_accessible :account,
      :instance_id,
      :metric,
      :ticket,
      :time,
      :type

    belongs_to :account
    belongs_to :ticket, inverse_of: :metric_events, inherit: :account_id

    default_scope { order(:time) }

    has_soft_deletion default_scope: true

    Zendesk::TicketMetric.supported.each do |metric|
      scope metric, -> { where(metric: metric) }
    end

    TYPES.each do |type|
      scope type, -> { where(type: "TicketMetric::#{type.to_s.camelize}") }
    end

    scope(
      :transition,
      -> {
        where(
          type: TRANSITIONS.map { |type| "TicketMetric::#{type.to_s.camelize}" }
        )
      }
    )

    scope :before, ->(time) { where('time < ?', time) }
    scope :after,  ->(time) { where('time >= ?', time) }

    def self.precedence
      0
    end

    def self.latest_instance_id
      pluck(:instance_id).max || NO_INSTANCE
    end

    def self.next_instance_id
      latest_instance_id.succ
    end

    def type
      read_attribute(:type).demodulize.snakecase
    end

    def metric?(metric_name)
      metric.to_s == metric_name.to_s
    end

    def metadata
      {}
    end

    def precedence
      self.class.precedence
    end
  end
end
