module TicketMetric
  class Fulfill < TicketMetric::Transition
    def self.precedence
      Breach.precedence.succ
    end

    def to_state
      :deactivated
    end
  end
end
