module TicketMetric
  class Status < ActiveRecord::Base
    self.table_name = :ticket_metric_statuses

    belongs_to :account
    belongs_to :metric_event,
      class_name:  'TicketMetric::UpdateStatus',
      foreign_key: :ticket_metric_event_id,
      inverse_of:  :statuses

    attr_accessible
  end
end
