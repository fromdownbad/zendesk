module TicketMetric
  class Activate < TicketMetric::Transition
    def self.precedence
      Measure.precedence.succ
    end

    def to_state
      :activated
    end
  end
end
