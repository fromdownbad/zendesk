require 'zendesk/ticket_metric'

module TicketMetric
  class UpdateStatus < TicketMetric::Event
    UNITS = %i[calendar business].freeze

    has_many :statuses,
      class_name:  'TicketMetric::Status',
      foreign_key: :ticket_metric_event_id,
      inverse_of:  :metric_event,
      inherit: :account_id

    before_create :record_statuses

    def self.precedence
      Fulfill.precedence.succ
    end

    def metadata
      super.merge!(status: details)
    end

    def record_statuses
      UNITS.each do |unit|
        statuses << TicketMetric::Status.new do |status|
          status.account = account
          status.unit    = unit
          status.value   = send("#{unit}_periods").calculation.elapsed(to: time)
        end
      end
    end

    private

    def details
      statuses.each_with_object({}) do |status, details|
        details[status.unit.to_sym] = status.value
      end
    end

    def calendar_periods
      Zendesk::TicketMetric::Periods.new(
        ticket:         ticket,
        metric_events:  instance_events,
        business_hours: false
      )
    end

    def business_periods
      Zendesk::TicketMetric::Periods.new(
        ticket:         ticket,
        metric_events:  instance_events,
        business_hours: true
      )
    end

    def instance_events
      @instance_events ||= begin
        ticket.metric_events.where(metric: metric, instance_id: instance_id)
      end
    end
  end
end
