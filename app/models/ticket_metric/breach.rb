module TicketMetric
  class Breach < TicketMetric::Event
    def self.precedence
      ApplySla.precedence.succ
    end

    def time
      deleted? ? updated_at : read_attribute(:time)
    end

    def metadata
      super.merge!(deleted: deleted?)
    end
  end
end
