module TicketMetric
  class Transition < TicketMetric::Event
    def to_state?(state)
      to_state == state
    end
  end
end
