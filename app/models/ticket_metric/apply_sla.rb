module TicketMetric
  class ApplySla < TicketMetric::Event
    has_one :metric_event_policy_metric, foreign_key: :metric_event_id, inherit: :account_id
    has_one :policy_metric, through: :metric_event_policy_metric

    before_create :record_policy_metric

    def self.precedence
      Pause.precedence.succ
    end

    def policy_metric
      Sla::PolicyMetric.with_deleted { super }
    end

    def metadata
      super.merge!(sla: details)
    end

    private

    def details
      return nil unless policy_metric.present?

      {
        target: policy_metric.target,
        business_hours: policy_metric.business_hours,
        policy: {
          id: policy_metric.policy.id,
          title: policy_metric.policy.title,
          description: policy_metric.policy.description
        }
      }
    end

    def record_policy_metric
      return unless context.sla.policy_metric.present?

      build_metric_event_policy_metric(
        account: account,
        policy_metric: context.sla.policy_metric
      )
    end

    def context
      @context ||= Zendesk::TicketMetric::Context.new(ticket, metric)
    end
  end
end
