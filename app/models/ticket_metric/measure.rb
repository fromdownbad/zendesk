module TicketMetric
  class Measure < TicketMetric::Event
    before_create :set_instance_id

    def self.precedence
      Event.precedence.succ
    end

    private

    def set_instance_id
      self.instance_id = NO_INSTANCE
    end
  end
end
