module TicketMetric
  class Pause < TicketMetric::Transition
    def self.precedence
      Activate.precedence.succ
    end

    def to_state
      :paused
    end
  end
end
