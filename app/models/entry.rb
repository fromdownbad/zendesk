require 'friendly_id'
require 'zendesk/search'

class Entry < ActiveRecord::Base
  include ScopedCacheKeys
  include TagManagement
  include ServiceableAccount
  include Subscribable
  include Zendesk::Serialization::EntrySerialization
  include Zendesk::ForumSanitizer
  include SlugIds
  include CachingObserver
  include ContentMappingSupport
  include ForumObserver
  extend ActiveHash::Associations::ActiveRecordExtensions

  PINNED_PER_PAGE = 5

  belongs_to :account
  belongs_to :forum, counter_cache: true, inherit: :account_id
  belongs_to :organization, inherit: :account_id
  belongs_to :updater, class_name: 'User', inherit: :account_id
  belongs_to :submitter, class_name: 'User', foreign_key: :submitter_id, inherit: :account_id
  belongs_to_active_hash :flag, class_name: "EntryFlagType", foreign_key: :flag_type_id

  has_one :category, through: :forum, inherit: :account_id
  has_one :latest_post, -> { order(created_at: :desc, id: :desc) }, class_name: "Post", inherit: :account_id

  has_many :attachments, -> { where(parent_id: nil).order(created_at: :asc, id: :asc) }, as: :source, dependent: :destroy, inherit: :account_id
  has_many :posts, -> { order(created_at: :asc, id: :asc) }, dependent: :delete_all, inherit: :account_id
  has_many :tickets, inherit: :account_id
  has_many :votes, dependent: :delete_all, inherit: :account_id
  has_many :voters, through: :votes, source: :user, inherit: :account_id
  has_many :entry_search_phrases, autosave: true, dependent: :destroy, inherit: :account_id

  scope :for_user, ->(user) { where(user.entry_conditions + user.forum_visibility_conditions).joins(:forum) }
  scope :is_public, -> { where("entries.is_public = 1") }

  scope :pinned, -> { where(is_pinned: true).order('IFNULL(pinned_index, 999999) asc, created_at DESC, id DESC') }
  scope :highlighted, -> { where(is_highlighted: true).order('updated_at DESC') }

  validates_presence_of :account_id, :forum, :submitter, :title
  alias_attribute :text, :body
  validates_presence_of :text, on: :create
  validate :account_is_serviceable, if: :account
  validate :validate_topic_editable_by_user
  validates_lengths_from_database only: [:current_tags]

  before_validation :default_submitter_to_current_user
  before_validation :set_updater_to_submitter, on: :create
  before_validation :suspend_moderated_entries, on: :create
  before_create     :set_defaults
  before_update     :clear_flags_on_forum_change
  before_save       :inherit_parent_forum_attributes_before_save
  before_save       :add_attachments, if: proc { |e| e.uploads.present? }
  before_destroy    :validate_topic_editable_by_user

  after_update      :propagate_changes_after_update
  after_commit      :subscribe_submitter, on: :create

  attr_accessor :is_last_item

  has_soft_deletion default_scope: true

  # Counter cache gets bumped even though it's deleted, we reset it
  after_create :reset_forum_counter_cache, if: :suspended?

  before_soft_delete :unsuspend_posts

  after_soft_delete :reset_forum_counter_cache, unless: :skip_counter_cache?
  after_soft_undelete :reset_forum_counter_cache

  after_soft_undelete :subscribe_submitter
  after_soft_undelete { |obj| ForumObserver.store_event(obj) }

  def reset_forum_counter_cache
    Entry.unscoped.where(deleted_at: nil).scoping do
      Forum.reset_counters(forum_id, :entries)
    end
  end

  attr_accessor  :current_user, :uploads, :silence_notification, :stats_value, :from_ticket
  attr_writer    :skip_counter_cache

  attr_accessible :additional_tags, :is_pinned, :body, :text, :highlighted, :deleted_at, :forum_id,
    :flag_type_id, :title, :created_at, :updated_at, :hits, :posts_count, :is_locked, :submitter_id,
    :is_highlighted, :pinned_index, :current_tags, :tags, :locked, :updater_id, :pinned, :position,
    :uploads, :search_phrases, :set_tags, :submitter, :account, :forum, :remove_tags, :from_ticket

  alias_attribute :locked, :is_locked
  alias_attribute :pinned, :is_pinned
  alias_attribute :highlighted, :is_highlighted

  def self.find_related(account, user, tags)
    return [] if tags.blank?
    tags = tags.map { |t| "tags:#{t}" }.join(" ")

    any_forum = 0
    options = { per_page: 6, type: 'entry', forum_id: any_forum }
    forum_ids = user.accessible_forums.map(&:id)
    forum_ids = -1 if forum_ids.empty?

    cache_key = [
      account.id,
      user.permissions,
      Array(forum_ids).join("/"),
      Digest::MD5.hexdigest(tags)
    ].join("/")

    Rails.cache.fetch(cache_key, expires_in: 1.hour) do
      Zendesk::Search.search(user, tags, options)
    end
  end

  def self.submitted_by(ids)
    where(submitter_id: ids)
  end

  def unpin
    # The @entry may be found by joins to ensure proper access, the entry is read-only for same reason
    update_column(:is_pinned, false)
    # flush index cached action
    account.expire_scoped_cache_key(:forums)
  end

  # For Sphinx search result excerpts
  def excerpt
    return '' if body.nil?
    body.strip_tags.gsub(/\s/, ' ').truncate(200)
  end

  def rss_title
    title
  end

  # Required to 'act like' an 'rss item'
  def creator
    submitter
  end

  def rss_guid_parts
    [forum_id, id]
  end

  # Required to 'act like' an 'rss item'
  def entry
    self
  end

  def to_liquid
    hash = Zendesk::Liquid::Wrapper.new("name")
    hash.merge!('title' => value, 'url' => url, 'attachments' => lambda { |_x| attachments })
    hash
  end

  def url
    "#{account.url}/entries/#{to_param}"
  end

  # To be used by Zenbox (dropbox)
  def relative_path
    "/entries/#{id}"
  end

  def hit!
    self.class.increment_counter(:hits, id)
  end

  def to_param
    slug_id(self)
  end

  def name
    title
  end

  def forum_type
    forum.present? ? forum.display_type.name : nil
  end

  def answered?
    flag_type_id == EntryFlagType::ANSWERED.id
  end

  def search_phrases=(phrases)
    entry_search_phrases.destroy_all && return if phrases.blank?

    self.entry_search_phrases = phrases.map do |phrase|
      EntrySearchPhrase.new(account: account, phrase: phrase, entry: self)
    end
  end

  def search_phrases
    entry_search_phrases.map(&:phrase)
  end

  # Moderation
  def suspended?
    deleted? && flag.suspended?
  end

  def self.mark_as_unsuspended_sql
    { flag_type_id: EntryFlagType::UNKNOWN.id, updated_at: Time.now }
  end

  def self.mark_as_soft_deleted_sql
    ["deleted_at = ?, flag_type_id = ?", Time.now, EntryFlagType::UNKNOWN.id]
  end

  def mark_as_deleted
    super
    self.flag_type_id = EntryFlagType::UNKNOWN.id
  end

  def suspend!
    return if suspended?
    suspend
    save!
  end

  def unsuspend!
    return unless suspended?
    self.flag_type_id = EntryFlagType::UNKNOWN.id
    self.silence_notification = false
    soft_undelete!
  end

  def needs_spam_detection?
    account.settings.moderated_entries? && from_ticket != 'true' && submitter.is_end_user?
  end

  private

  def unsuspend_posts
    SuspendedPost.for_account(account).
      where(entry_id: id).
      update_all(Post.mark_as_unsuspended_sql)
  end

  def add_attachments
    tokens = account.upload_tokens.find_all_by_value(uploads).to_a

    tokens.map!(&:attachments)
    tokens.flatten!

    tokens.each do |attachment|
      attachment.is_public = is_public?
    end

    self.attachments += tokens
  end

  def validate_topic_editable_by_user
    unless @skip_user_permission_check
      if current_user && !current_user.can?(:edit, self)
        errors.add(:base, 'Topics cannot be created/updated by you')
        return false
      end
    end
    true
  end

  def suspend
    self.deleted_at = Time.now
    self.flag_type_id = EntryFlagType::SUSPENDED.id
    self.silence_notification = true
  end

  def suspend_moderated_entries
    # Will be stopped by later validation, we need an account
    return unless account

    if needs_spam_detection?
      if submitter.throttled_posting?
        errors.add(:base, I18n.t("txt.moderation.throttled"))
        return false
      elsif !submitter.whitelisted_from_moderation?
        suspend
      end
    end
  end

  def set_defaults
    self.is_pinned        = false if is_pinned.nil?
    self.is_highlighted   = false if is_highlighted.nil?

    unless current_user.is_agent?
      self.is_pinned      = false
      self.is_highlighted = false
      self.is_locked      = false
      self.is_public      = true
    end
  end

  def inherit_parent_forum_attributes_before_save
    self.is_public       = !forum.visibility_restriction.agents_only?
    self.organization_id = forum.organization_id
    true
  end

  def clear_flags_on_forum_change
    # E.g. topics in Idea forums can't be labelled "Answered"
    if forum_id_changed? && Forum.find(forum_id_was).display_type != Forum.find(forum_id).display_type
      # If the topic was in a question forum, remove "Answer" flags for associated posts (is_informative)
      if Forum.find(forum_id_was).display_type.questions?
        Post.where(entry_id: id).update_all(is_informative: false)
      end
      self.flag_type_id = EntryFlagType::UNKNOWN.id
    end
  end

  def propagate_changes_after_update
    if is_public_changed?
      Attachment.where(source_id: id, source_type: self.class.name).update_all(is_public: is_public)
    end

    if forum_id_changed?
      Post.where(entry_id: id).update_all(forum_id: forum_id)
      Forum.where(id: forum_id_was).update_all_with_updated_at('entries_count = entries_count - 1')
      Forum.where(id: forum_id).update_all_with_updated_at('entries_count = entries_count + 1')
    end
  end

  def subscribe_submitter
    if !forum.subscribed?(submitter) && !deleted?
      subscribe(submitter)
    end
  end

  def forum_id_previously_changed?
    previous_changes['forum_id'].present?
  end

  def set_updater_to_submitter
    self.updater = submitter
    true
  end

  def default_submitter_to_current_user
    self.submitter ||= current_user
  end

  def skip_counter_cache?
    @skip_counter_cache.present?
  end
end
