require 'zendesk/radar_factory'

class UserSeat < ActiveRecord::Base
  include Zendesk::ForbiddenAttributesProtection
  include UserSeatChangesObserver

  belongs_to :account
  belongs_to :user

  validates_inclusion_of :seat_type, in: Zendesk::Entitlement::TYPES.values
  validates_presence_of :role
  validates :user_id, uniqueness: { scope: [:seat_type, :account_id] }
  validate :check_max_voice_seats, on: :create, if: :voice_seat?
  validate :check_user_role, if: :voice_seat?

  scope :voice, -> { where(seat_type: Zendesk::Entitlement::TYPES[:voice]) }
  scope :explore, -> { where(seat_type: Zendesk::Entitlement::TYPES[:explore]) }
  scope :connect, -> { where(seat_type: Zendesk::Entitlement::TYPES[:connect]) }
  scope :for_user, ->(user_or_id) { where(user_id: user_or_id) }

  after_commit :notify_voice, on: :destroy, if: :voice_seat?
  after_commit :delete_voice_number, on: :destroy, if: -> { account.has_voice_delete_number_on_downgrade? && voice_seat? }
  after_commit :notify_lotus_of_voice_seat_creation, on: :create, if: -> { account.has_enable_voice_after_voice_seat_creation? && voice_seat? }

  def self.can_create_new_voice_seats?(account, user_ids)
    # Don't enforce the limit when a voice subscription doesn't exist
    # It means the account is on voice trial
    return true if account.voice_subscription.blank?

    existing_user_ids = account.users.where(id: account.user_seats.voice.pluck(:user_id)).pluck(:id)
    (existing_user_ids + user_ids).uniq.count <= max_agents(account)
  end

  def self.max_agents(account)
    account.voice_subscription.try(:max_agents) || 0
  end

  def voice_seat?
    seat_type == Zendesk::Entitlement::TYPES[:voice]
  end

  def delete_voice_number
    user.remove_voice_number
  end

  private

  def notify_lotus_of_voice_seat_creation
    ::RadarFactory.create_radar_notification(account, "voice_enabled/#{account.owner_id}").send('message', {})
  end

  def notify_voice
    Voice::NotifyVoiceSeatIsDestroyed.enqueue(account_id, user_id)
  end

  def check_user_role
    unless user.is_voice_agent?
      errors.add(:user_id, 'needs to be an assignable agent')
    end
  end

  def check_max_voice_seats
    unless UserSeat.can_create_new_voice_seats?(account, [user_id])
      max_agents = UserSeat.max_agents(account)
      errors.add(:base, I18n.t("txt.job.user_bulk_update_job.talk_seats_limit_reached", count: max_agents))
    end
  end
end
