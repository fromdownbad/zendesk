class ForumDisplayType < ActiveHash::Base
  fields :id, :name

  include ActiveHash::Enum
  enum_accessor :name

  include ActiveHash::Associations
  has_many :forums, foreign_key: :display_type_id

  def articles?
    name == "Articles"
  end

  def ideas?
    name == "Ideas"
  end

  def questions?
    name == "Questions"
  end
end

ForumDisplayType.data = [
  {id: 1, name: "Articles"},
  {id: 2, name: "Ideas"},
  {id: 3, name: "Questions"}
]
