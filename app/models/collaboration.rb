class Collaboration < ActiveRecord::Base
  include Zendesk::Serialization::CollaborationSerialization

  belongs_to :account
  belongs_to :ticket, inverse_of: :collaborations
  belongs_to :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  scope :followers,         -> { where(collaborator_type: CollaboratorType.FOLLOWER) }
  scope :email_ccs,         -> { where(collaborator_type: CollaboratorType.EMAIL_CC) }
  scope :legacy_ccs,        -> { where(collaborator_type: CollaboratorType.LEGACY_CC) }
  scope :with_active_users, -> { joins(:user).where(users: { is_active: true }) }

  attr_accessible :account,
    :ticket,
    :user,
    :user_id,
    :collaborator_type, # Integer
    :collaboration_type # String (deprecated)

  before_validation :set_account_defaults, on: :create
  validates_presence_of :ticket, :user
  validate :validate_associations_match, on: :create

  delegate :is_agent?, to: :user

  # Transformations outlined here: goto.zendesk.com/collaboration_transformation
  def transform(follower_and_email_cc_collaborations_enabled)
    legacy_type = collaborator_type == CollaboratorType.LEGACY_CC

    if follower_and_email_cc_collaborations_enabled && legacy_type
      self.collaborator_type = update_collaboration
    elsif !follower_and_email_cc_collaborations_enabled && !legacy_type
      self.collaborator_type = CollaboratorType.LEGACY_CC
    end

    self
  end

  # Majority of the existing consumers of this method need to verify that the
  # user in collaboration is an agent so the argument is defaulted to true.
  #
  # Consumers such as `invalid_followers` method in collaborating.rb
  # want to skip the agent chck and hence can pass `false` when calling
  def follower?(verify_agent: true)
    return false if collaborator_type == CollaboratorType.EMAIL_CC

    # verify if user is an agent when collaboration is of type LEGACY_CC
    verify_agent = true if collaborator_type == CollaboratorType.LEGACY_CC

    !verify_agent || user.try(:is_agent?)
  end

  # Majority of the existing consumers of this method need to verify that the
  # user in collaboration can publicly comment so the argument is defaulted to
  # true.
  #
  # Consumers such as `invalid_email_ccs` method in collaborating.rb want to
  # skip the agent check and hence can pass `false` when calling
  def email_cc?(verify_user: true)
    return false if collaborator_type == CollaboratorType.FOLLOWER

    (collaborator_type == CollaboratorType.EMAIL_CC &&
      (!verify_user || user_can_comment_publicly_or_light_agent_ccs_allowed?)
    ) ||
    (collaborator_type == CollaboratorType.LEGACY_CC && user.try(:is_end_user?))
  end

  def legacy_cc?
    collaborator_type == CollaboratorType.LEGACY_CC || collaborator_type.nil?
  end

  def user_valid?
    user.try(:valid?)
  end

  protected

  def set_account_defaults
    self.collaboration_type ||= 1
    self.account_id = ticket.account_id
  end

  def validate_associations_match
    if user.account_id != account.id
      errors.add(:base, I18n.t('txt.admin.models.collaboration.cc_invalid_user'))
    end
  end

  private

  def update_collaboration
    if is_agent?
      CollaboratorType.FOLLOWER
    else
      CollaboratorType.EMAIL_CC
    end
  end

  def user_can_comment_publicly_or_light_agent_ccs_allowed?
    return true if user.try(:can?, :publicly, Comment)

    account.has_email_ccs_light_agents_v2? && account.has_light_agent_email_ccs_allowed_enabled?
  end
end
