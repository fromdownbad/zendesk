module Lotus
  class ManifestManager
    attr_reader :account

    TESSA_VERSION_RE = /^v\d+$/

    def initialize(account, develop: false, nocache: false)
      @account = account
      @develop = develop
      @nocache = nocache
    end

    def find(full_ref)
      Lotus::AssetManifest.new(account, manifest_content(full_ref), develop: @develop, nocache: @nocache)
    end

    def version_information(asset_manifest)
      asset_manifest.version_information(current.sha)
    end

    def versions
      tessa_client.fetch_latest_deployed_manifests
    end

    def use_tessa?
      return true if USE_TESSA
      return @using_tessa unless @using_tessa.nil?

      production_or_staging = Rails.env.production? || Rails.env.staging?
      @using_tessa = production_or_staging
    end

    private

    def current
      @current ||= find(:current)
    end

    def get_tessa_request_type(full_ref)
      ref_string = full_ref.to_s

      if ref_string == 'current' || @develop == true
        :current
      elsif TESSA_VERSION_RE.match?(ref_string)
        :version
      else
        :sha
      end
    end

    def get_manifest_content_from_tessa(full_ref)
      request_type = get_tessa_request_type(full_ref)

      if request_type == :sha
        tessa_client.fetch_manifest_by_sha(full_ref)
      elsif request_type == :version
        # At this point we know that the full_ref begins with "v" due to the check in
        # get_tessa_request_type, but Tessa expects an integer value in its request URI.
        ref_without_v_prefix = full_ref[1..-1]
        tessa_client.fetch_manifest_by_version(ref_without_v_prefix)
      else
        tessa_client.fetch_current_manifest
      end
    end

    def manifest_content(full_ref)
      return request_manifest_from_zendesk_console unless use_tessa?

      get_manifest_content_from_tessa(full_ref)
    end

    def filename
      'tessaManifest.json'
    end

    def manifest_url
      "#{account.url(mapped: false)}/agent/#{filename}".tap do |url|
        url.gsub!('https', 'http') if Rails.env.development?
      end
    end

    def request_manifest_from_zendesk_console
      uri = URI.parse(manifest_url)
      path = uri.path
      uri.path = '/'

      connection = Faraday.new(uri.to_s) do |c|
        c.response :raise_error
        c.adapter  Faraday.default_adapter
      end

      begin
        JSON.parse(connection.get(path).body)
      rescue Faraday::Error::ClientError => e
        friendly = "Unable to access the Asset Manifest at #{manifest_url}. Lotus must be running and available in your environment."
        raise "#{friendly}\n  #{e.inspect} #{e.message}"
      end
    end

    def tessa_client
      @tessa_client ||= TessaClient::V1.new(:lotus)
    end
  end
end
