module Lotus
  class AssetManifest
    DEFAULT_ROLLBAR_IDENTIFIER = 'unknown'.freeze

    attr_reader :account, :content, :develop

    # "develop" means the querystring contained ?sha=develop. In this case we are
    # going to serve the assets listed in the `current` manifest but prefix them
    # with https://dev.zd-dev.com and remove the fingerprints.
    def initialize(account, content, develop: false, nocache: false)
      @account = account
      @content = content
      @develop = develop
      @nocache = nocache
    end

    def manifest
      @manifest ||= begin
        json = content || {}

        if @develop
          json['css'].try(:map!, &method(:dev))
          json['js'].try(:map!, &method(:dev))
        end

        if @nocache
          @timestamp = Time.now.to_i.to_s
          json['css'].try(:map!, &method(:force_no_cache))
          json['js'].try(:map!, &method(:force_no_cache))
        end

        json
      end
    end

    def initial_css_assets
      # Webpack does not generate CSS files for its React chunks in development.
      # This is for purposes of hot reloading.
      # So, we remove any React CSS here if we are in development.
      if @develop
        css_to_include.reject { |asset| react_css?(asset['path']) }
      else
        css_to_include
      end
    end

    def initial_js_assets
      # To properly serve lotus_react assets, we need to ensure that the app.{fingerprint}.js is listed last in the array.
      # There will be several entry points, but app.{fingerprint}.js will be the primary entry point.
      # Even though we use the defer attribute on script tags in the HTML, execution order follows the order of definition.
      # The order of assets should be: non-React assets, then the properly ordered React assets.
      partitioned_assets = js_to_include.partition { |asset| react_path?(asset['path']) }
      react_assets, non_react_assets = partitioned_assets
      non_react_assets + ordered_react_scripts(react_assets)
    end

    def sha
      manifest['sha']
    end

    def version
      manifest['version']
    end

    def rollbar_identifier
      version || sha || DEFAULT_ROLLBAR_IDENTIFIER
    end

    def version_information(current_sha)
      {
        'createdAt' => content['createdAt'] || '',
        'sha' => content['sha'] || '',
        'specified' => @develop == true || content['sha'] != current_sha,
        'tag' => content['version'] || ''
      }
    end

    def empty?
      manifest.empty?
    end

    private

    REACT_PATH_RE = %r{/agent/assets/(lr|react/js)/.+(\.\w+)?\.js}
    REACT_CSS_PATH_RE = %r{/agent/assets/(lr|react/js)/.+(\.\w+)?\.css}
    EMBER_PATH_RE = %r{-\w{32}(?=\.[^.]+\Z)}
    REACT_PRIMARY_ENTRY_RE = %r{/agent/assets/(lr|react/js)/app(\.\w+)?\.js}
    REACT_RUNTIME_ENTRY_RE = %r{/agent/assets/(lr|react/js)/runtime(\.\w+)?\.js}
    REACT_FINGERPRINT_RE = %r{\.\w+\.js$}
    REACT_SINGLE_ENTRY_RE = %r{/agent/assets/lr/main\.\w+\.js}
    REACT_SUB_DIRECTORY_RE = %r{/lr/}
    REACT_CSS_CHUNK_RE = %r{.*\.chunk\.css$}

    def react_path?(path)
      path =~ REACT_PATH_RE
    end

    def react_css?(path)
      path =~ REACT_CSS_PATH_RE
    end

    def dev(asset)
      path = if asset['lazy'] == true
        # If the asset is a lazy chunk (like 0.385jd9f8.chunk.js), then we should not alter the path.
        asset['path']
      elsif react_path?(asset['path']) && asset['lazy'] != true
        # /agent/assets/lr/vendor.3jf9e0djf.js becomes
        # /agent/assets/react/js/vendor.js
        without_fingerprint = asset['path'].sub(REACT_FINGERPRINT_RE, '.js')
        without_fingerprint.sub(REACT_SUB_DIRECTORY_RE, '/react/js/')
      else
        # /agent/assets/application-4940680eec80fd1ad3673d9df8700797.js becomes
        # /agent/assets/application.js
        asset['path'].sub(EMBER_PATH_RE, '')
      end

      asset['path'] = path
      asset
    end

    def force_no_cache(asset)
      asset['path'] = asset['path'] + '?' + @timestamp
      asset
    end

    def ordered_react_scripts(assets)
      groups = assets.group_by do |asset|
        path = asset['path']
        if REACT_RUNTIME_ENTRY_RE.match?(path)
          'runtime'
        elsif REACT_PRIMARY_ENTRY_RE.match?(path)
          'primary_entry'
        else
          'unordered'
        end
      end

      # This allows for us to gracefully handle the scenario where a group is empty.
      groups.default = []

      groups['runtime'] + groups['unordered'] + groups['primary_entry']
    end

    def css_to_include
      (manifest['css'] || []).each_with_object([]) do |asset, memo|
        memo.push(asset) if include_css_asset?(asset)
      end
    end

    def js_to_include
      (manifest['js'] || []).each_with_object([]) do |asset, memo|
        memo.push(asset) if include_js_asset?(asset)
      end
    end

    def include_css_asset?(asset)
      asset['path'] !~ REACT_CSS_CHUNK_RE
    end

    def include_js_asset?(asset)
      asset['lazy'] != true
    end

    def account_has_feature?(feature_name)
      ::Arturo.feature_enabled_for?(feature_name.to_sym, account)
    end
  end
end
