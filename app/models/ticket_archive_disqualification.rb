# Tracks whether a ticket is not capable of being archived (e.g. for being too many bytes)
# https://zendesk.atlassian.net/wiki/spaces/INFR/pages/529499450/Archiver+V2+ADR+Do+not+re-attempt+archiving+of+a+ticket+once+it+is+disqualified
class TicketArchiveDisqualification < ActiveRecord::Base
  # `id` will equal tickets.id, so we don't need independent id generation
  disable_global_uid

  belongs_to :account

  attr_accessible

  BYTESIZE    = 1
  EVENT_COUNT = 2

  def self.disqualify_ticket!(ticket, metric:, value:)
    record = new
    record.id = ticket.id
    record.metric = metric
    record.value = value
    record.account_id = ticket.account_id
    record.save!
    record
  end
end
