class CustomSecurityPolicy < ActiveRecord::Base
  include CIA::Auditable
  NO_COMPLEXITY      = 0
  NUMBERS_COMPLEXITY = 1
  SPECIAL_COMPLEXITY = 2

  belongs_to :account

  attr_accessible :account, :password_history_length, :password_length, :password_complexity,
    :password_in_mixed_case, :password_duration, :failed_attempts_allowed, :max_sequence,
    :disallow_local_part_from_email

  audit_attribute :password_history_length, :password_length, :password_complexity,
    :password_in_mixed_case, :password_duration, :failed_attempts_allowed, :max_sequence,
    :disallow_local_part_from_email

  before_update :check_policy_level

  def self.build_from_current_policy(account) # will build from 'low' policy for shell accounts
    agent_security_policy_id = account.role_settings.security_policy_id_for_role(:agent)

    current_security_policy_id = if agent_security_policy_id == Zendesk::SecurityPolicy::Custom.id
      # this is to avoid an endless cycle when for some reason current policy is custom
      # but there was no custom_security_policy for this account causing the call to this method
      Zendesk::SecurityPolicy::High.id
    else
      agent_security_policy_id
    end

    update_or_build_custom_policy_from_non_custom_policy(current_security_policy_id, account)
  end

  def self.update_or_build_custom_policy_from_non_custom_policy(security_policy_id, account, custom_security_policy = CustomSecurityPolicy.new(account: account))
    security_policy = Zendesk::SecurityPolicy.find(security_policy_id, account)

    custom_security_policy.password_length         = security_policy.password_length
    custom_security_policy.failed_attempts_allowed = security_policy.password_attempt_threshold
    custom_security_policy.max_sequence            = security_policy.max_sequence

    case security_policy
    when Zendesk::SecurityPolicy::Low
      custom_security_policy.password_history_length = 0
      custom_security_policy.password_duration       = 0
      custom_security_policy.password_complexity     = NO_COMPLEXITY
      custom_security_policy.password_in_mixed_case  = false
    when Zendesk::SecurityPolicy::Medium
      custom_security_policy.password_history_length = 0
      custom_security_policy.password_duration       = 0
      custom_security_policy.password_complexity     = SPECIAL_COMPLEXITY
      custom_security_policy.password_in_mixed_case  = true
    when Zendesk::SecurityPolicy::High
      custom_security_policy.password_history_length = security_policy.password_history_length
      custom_security_policy.password_duration       = security_policy.password_duration
      custom_security_policy.password_complexity     = SPECIAL_COMPLEXITY
      custom_security_policy.password_in_mixed_case  = true
    end

    custom_security_policy
  end

  def check_policy_level
    self.policy_level ||= Zendesk::SecurityPolicy::Custom.id

    password_required_bigger_now = password_length > password_length_was
    password_required_more_complex_now = password_complexity > password_complexity_was
    password_required_must_include_mixed_case_now = password_in_mixed_case && !password_in_mixed_case_was

    if password_required_bigger_now ||
       password_required_more_complex_now ||
       password_required_must_include_mixed_case_now
      self.policy_level += 1
    end
  end
end
