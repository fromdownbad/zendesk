module Fraud
  class Report < ActiveRecord::Base
    self.table_name = 'fraud_reports'

    after_commit :notify_voice

    include CIA::Auditable

    audit_attribute :account_id,
      :user_id,
      :risk_score,
      :ip_address,
      :source_event,
      :raw_response

    belongs_to :account
    belongs_to :user

    attr_accessible :account,
      :user,
      :risk_score,
      :ip_address,
      :source_event,
      :raw_response

    default_scope { order('created_at ASC') }

    def source_event
      super.to_sym
    end

    private

    def notify_voice
      Voice::SendFraudReportJob.enqueue(account.id, risk_score || 0)
    end
  end
end
