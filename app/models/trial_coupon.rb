class TrialCoupon < Coupon
  attr_accessible :trial_max_agents, :trial_plan_type

  validates_presence_of :trial_plan_type
  validate :validate_trial_plan_type

  validates_presence_of :trial_max_agents
  validates_numericality_of :trial_max_agents, greater_than_or_equal_to: 0

  def valid_for_signup?
    true
  end

  private

  def validate_trial_plan_type
    unless SubscriptionPlanType.fields.include? trial_plan_type
      errors.add :trial_plan_type, "#trial_plan_type must be a valid SubscriptionPlanType"
    end
  end
end
