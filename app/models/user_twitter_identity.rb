require 'zendesk/models/user_twitter_identity'

class UserTwitterIdentity < UserIdentity
  include Zendesk::Serialization::UserTwitterIdentitySerialization

  before_validation :cleanup_error_messages
  validates_format_of :value, with: /\A\d+\z/
  validate :twitter_screen_name_exists

  attr_accessible :screen_name

  def self.identity_type
    :twitter
  end

  def self.find_by_value(value)
    where(value: value.to_s).first
  end

  def to_s
    screen_name
  end

  def screen_name=(new_screen_name)
    # TODO: Channels.  TwitterUserProfile is defined in the zendesk_channels repo.  Convert to API call?
    if (profile = ::Channels::TwitterUserProfile.from_screen_name(account, new_screen_name, user))
      self.value = profile.external_id
    else
      @invalid_screen_name = new_screen_name
    end
  end

  def screen_name
    twitter_user_profile.try(:screen_name)
  end

  private #######################################################################

  def cleanup_error_messages
    if @invalid_screen_name
      self.value = '0' # hide the invalid format error
    end
  end

  def twitter_screen_name_exists
    if @invalid_screen_name
      errors.add(:value, I18n.t('txt.error_messages.user_twitter_identity.identity_doesnt_exists', twitter_name: @invalid_screen_name))
    end
  end

  def value_for_error
    screen_name || value
  end
end
