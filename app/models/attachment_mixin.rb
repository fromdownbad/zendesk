module AttachmentMixin
  include Zendesk::Attachments::FSCompatibility

  def self.included(receiver)
    receiver.extend(ClassMethods)
    receiver.attr_accessible :account, :uploaded_data, :content_type, :filename, :parent_id, :thumbnail, :thumbnail_resize_options
  end

  def url(_args = {})
    raise "Must be implemented in the subclass"
  end

  def to_liquid
    hash = Zendesk::Liquid::Wrapper.new("filename")
    hash['filename'] = filename
    hash['url'] = url
    hash
  end

  def valid_size?
    size && size <= account.max_attachment_size
  end

  # Best move this to a_fu!
  def create_missing_thumbnails!
    new_thumbnails = []

    if create_thumbnails?
      attachment_options[:thumbnails].each do |suffix, size|
        next if thumbnails.any? { |t| t.thumbnail == suffix.to_s }
        # download if necessary
        # Gotcha! this is as if we set new data, since we want the processing
        # to happen
        set_temp_path create_temp_file, is_new: true unless temp_path
        new_thumbnails << create_or_update_thumbnail(suffix, *size)
      end
    end
    # clear stuff here, or another save! will screw us up badly
    cleanup_after_save

    new_thumbnails
  end

  module ClassMethods
    def from_file(file, options = {})
      options[:filename] ||= file.path
      new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(file.path, options))
    end
  end
end
