class MobileSdkApp < ActiveRecord::Base
  PUSH_NOTIF_TYPE_WEBHOOK = "webhook".freeze
  PUSH_NOTIF_TYPE_URBAN_AIRSHIP = "urban_airship".freeze

  SYSTEM_MESSAGE_TYPE_DEFAULT = "default".freeze
  SYSTEM_MESSAGE_TYPE_CUSTOM = "custom".freeze
  SYSTEM_MESSAGE_TYPES = [
    SYSTEM_MESSAGE_TYPE_DEFAULT,
    SYSTEM_MESSAGE_TYPE_CUSTOM,
  ].freeze

  SYSTEM_MESSAGE_MAX_LENGTH = 255

  TALK_CAPABILITY_CLICK_TO_CALL = 3

  include CIA::Auditable

  has_kasket
  has_kasket_on :account_id, :identifier

  require_dependency "mobile_sdk/app_properties"

  belongs_to :account
  belongs_to :brand
  belongs_to :mobile_sdk_auth, dependent: :destroy

  attr_accessible :id, :identifier, :title, :account, :mobile_sdk_auth, :mobile_sdk_auth_id, :brand, :signup_source, :brand_id, :settings

  before_validation :set_default_brand, on: :create, if: proc { |sdk_app| sdk_app.new_record? && sdk_app.brand_id.nil? }

  validates_presence_of :identifier, :account_id, :mobile_sdk_auth
  validates_uniqueness_of :identifier, scope: [:account_id]

  audit_attribute :mobile_sdk_auth, :deleted_at

  def generate_identifier
    identifier_generator.hex(24)
  end

  # Help Center settings are populated by the controller for the presenter
  attr_writer :help_center_settings

  def help_center_settings
    if settings.helpcenter_enabled
      @help_center_settings
    else
      {}
    end
  end

  def jwt_enabled
    settings.authentication == MobileSdkAuth::TYPE_JWT
  end

  def conversations_enabled?
    account.has_sdk_manage_requests? && settings.conversations_enabled
  end

  def help_center_article_voting_enabled?
    account.has_mobile_sdk_help_center_article_voting_enabled? && settings.help_center_article_voting_enabled
  end

  protected

  def set_default_brand
    self.brand_id = account.default_brand.id if account.present?
  end

  def identifier_generator
    defined?(SecureRandom) ? SecureRandom : ActiveSupport::SecureRandom
  end
end
