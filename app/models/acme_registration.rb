require 'openssl'
require 'acme/client'
require 'acmev2/client'
require 'zendesk/acme_job/error'

class AcmeRegistration < ActiveRecord::Base
  belongs_to :account
  has_many :acme_authorizations, dependent: :destroy

  has_soft_deletion default_scope: true

  before_validation :set_state, on: :create

  # This is allow only one non-deleted registration per account
  # refactor to once we have rails 4: conditions: -> { where("deleted_at is null") }
  validates_uniqueness_of :account_id, scope: [:deleted_at], unless: proc { |reg| reg.deleted_at? }

  def register!
    return if registration_valid?
    generate_key
    log "attempting to register"

    response = client_acme_v2.new_account(contact: "mailto:lets_encrypt+#{account.id}@zendesk.com", terms_of_service_agreed: true)
    log "registered"

    self.registration_id = response.kid.match(/\d+$/)[0].to_i
    self.state = 'active'
    save!
  rescue
    log "failed to register"
    raise Zendesk::AcmeJob::RegistrationError, $!
  end

  def registration_valid?
    state == 'active'
  end

  def client_acme_v2
    @client_acme_v2 ||= AcmeV2::Client.new(private_key: private_key, directory: lets_encrypt_endpoint_v2)
  end

  def client
    @client ||= Acme::Client.new(private_key: private_key, endpoint: lets_encrypt_endpoint, connection_options: { request: { open_timeout: 5, timeout: 5 } })
  end

  def authorization_for_domain(domain)
    acme_authorizations.detect { |a| a.identifier == domain } || acme_authorizations.create!(account: account, identifier: domain)
  end

  def fingerprint
    return "no_key" unless private_key
    data_string = OpenSSL::ASN1::Sequence(
      [
        OpenSSL::ASN1::Integer.new(private_key.public_key.n),
        OpenSSL::ASN1::Integer.new(private_key.public_key.e)
      ]
    )
    OpenSSL::Digest::SHA1.hexdigest(data_string.to_der).scan(/../).join(':')
  end

  private

  def log(str)
    Rails.logger.info "[acme] [#{Time.now}] [#{id}] [#{fingerprint}] #{str}"
  end

  def lets_encrypt_endpoint
    # use Zendesk.config.lets_encrypt.fetch('base_uri') once OP-18491 is done
    if Rails.env.development? || Rails.env.test?
      'https://acme-staging.api.letsencrypt.org/'
    else
      'https://acme-v01.api.letsencrypt.org/'
    end
  end

  def lets_encrypt_endpoint_v2
    # use Zendesk.config.lets_encrypt.fetch('base_uri') once OP-18491 is done
    if Rails.env.development? || Rails.env.test?
      'https://acme-staging-v02.api.letsencrypt.org/directory'
    else
      'https://acme-v02.api.letsencrypt.org/directory'
    end
  end

  def set_state
    self.state = 'pending' if state.nil?
  end

  def generate_key
    return unless encrypted_private_key.blank?
    key = OpenSSL::PKey::RSA.new(4096)
    self.encrypted_private_key = Zendesk::EncryptText.encrypt(encryption_key, key.to_s)
    log "generated key"
    save!
  end

  def private_key
    @private_key ||= begin
      generate_key if encrypted_private_key.blank?
      decrypted_private_key = Zendesk::EncryptText.decrypt(encryption_key, encrypted_private_key)
      OpenSSL::PKey::RSA.new(decrypted_private_key)
    end
  end

  def encryption_key
    Zendesk::Configuration.fetch(:certificates)["ssl_key_password"]
  end
end
