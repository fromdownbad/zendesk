class BrandLogo < ActiveRecord::Base
  include AttachmentMixin
  include BrandLogoObserver

  belongs_to :account
  belongs_to :brand, inverse_of: :logo, touch: true, inherit: :account_id

  attr_accessible :brand

  setup_attachment_fu(
    content_type: :image,
    size: 1..20.megabytes,
    processor: :mini_magick,
    resize_to: '80>',
    path_prefix: 'public/system/brands',
    thumbnails: {
      thumb: '32x32',
      small: '24x24'
    }
  )

  validates_as_attachment

  validates_presence_of :account_id, :brand
  validates_inclusion_of :extension, in: %w[png jpg jpeg gif]
  validate :valid_image_file
  before_validation :inherit_parent_attributes
  before_validation :validate_dimensions, prepend: true

  attr_reader :url

  def extension
    filename.split(".").last.downcase if filename.present?
  end

  def display_filename
    filename
  end

  private

  def valid_image_file
    return unless account

    unless valid_size?
      errors.add(:base, I18n.t('txt.errors.attachments.size_invalid', limit: account.subscription.file_upload_cap))
      return
    end

    return if temp_path.nil?

    begin
      MiniMagick::Image.open(temp_path)
    rescue MiniMagick::Error, MiniMagick::Invalid => e
      Rails.logger.warn("[SECURITY] BrandLogo deemed invalid by ImageMagick: #{inspect} -- #{e.message}")
      errors.add(:base, "Invalid brand logo")
    end
  end

  def validate_dimensions
    return if valid_dimensions?
    errors.add(:base, I18n.t('txt.errors.attachments.pixel_range_invalid_2', min: min_resize_pixels, max: max_resize_pixels))
    false
  end

  def destroy_thumbnails?
    true
  end

  def inherit_parent_attributes
    return unless parent
    self.account_id = parent.account_id
    self.brand_id   = parent.brand_id
  end
end
