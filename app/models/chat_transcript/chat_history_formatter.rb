class ChatTranscript::ChatHistoryFormatter
  TYPE_PREDICATES = {
    'ChatJoin' => :visitor?,
    'ChatLeave' => :visitor?,
    'ChatMessage' => :person?,
    'ChatNameChanged' => :visitor?,
    'ChatFileAttachment' => :person?
  }.freeze

  def initialize(chat_history)
    @chat_history = chat_history
  end

  def to_s
    @chat_history.each_with_object([]) do |data, history|
      build_history_line(data) { |line| history << line }
    end.join "\n"
  end

  private

  def visitor?(data)
    data['actor_type'] == 'end-user'
  end

  def person?(data)
    data['actor_type'].in? %w[end-user agent]
  end

  def history_line(data)
    timestamp = Time.at(0, data['timestamp'].to_i, :millisecond).utc.strftime('%I:%M:%S %p')
    chat_message_template = "txt.chat.history_#{data['type'].underscore}"
    message = I18n.t(chat_message_template, data.to_h.symbolize_keys)

    translated_message = data.
      dig('translation', 'msg').
      try { |msg| I18n.t(chat_message_template, data.merge('message' => msg)) }

    [message, translated_message].compact.map { |msg| "(#{timestamp}) #{msg}" }
  end

  def build_history_line(data)
    predicate = TYPE_PREDICATES[data['type']]
    return unless predicate && send(predicate, data)
    history_line(data).each { |line| yield line }
  end
end
