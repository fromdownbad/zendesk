class ChatTranscript::ChatTranscriptsBuilder
  def self.build(options)
    builder = new(options)
    yield builder
    builder.chat_transcripts
  end

  def initialize(options)
    @options = options
    @chat_transcripts = []
  end

  attr_reader :chat_transcripts

  def add_chat_history(values)
    values.each { |value| append(:chat_history, value) }
  end

  def add_web_paths(values)
    values.each { |value| append(:web_paths, value) }
  end

  private

  def add_new_transcript
    @current_transcript = ChatTranscript.new(@options).tap do |transcript|
      @chat_transcripts << transcript
      @current_length = transcript.initial_value_length
    end
  end

  def prepare_current_transcript(value)
    if @current_transcript
      @current_transcript.array_item_size(value).tap do |value_size|
        # ensure that the new value doesn't exceed limitations
        add_new_transcript if @current_length + value_size > ChatTranscript.value_limit
      end
    else
      # the builder always starts with an empty set
      add_new_transcript
      @current_transcript.array_item_size(value)
    end
  end

  def append(resource, value)
    value_size = prepare_current_transcript(value)
    # append and keep track of total size
    @current_transcript.public_send(resource) << value
    @current_length += value_size
  end
end
