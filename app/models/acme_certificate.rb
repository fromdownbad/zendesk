require 'zendesk/acme_job/error'

class AcmeCertificate
  EARLY_RENEWAL_DAYS = (ENV['ACME_CERTIFICATE_EARLY_RENEWAL_DAYS'] || 15).to_i

  attr_reader :account, :client, :issued_certificate
  attr_accessor :certificate, :acme_order

  class NewCert
    extend Forwardable

    attr_reader :x509, :x509_chain, :request, :private_key, :url

    def_delegators :x509, :to_pem, :to_der

    def initialize(certificate, url, chain, request)
      @x509 = certificate
      @url = url
      @x509_chain = chain
      @request = request
    end

    def chain_to_pem
      x509_chain.map(&:to_pem).join
    end

    def x509_fullchain
      [x509, *x509_chain]
    end

    def fullchain_to_pem
      x509_fullchain.map(&:to_pem).join
    end

    def common_name
      x509.subject.to_a.find { |name, _, _| name == 'CN' }[1]
    end
  end

  def initialize(account:, client:, certificate: nil, acme_order: nil)
    @account = account
    @client = client
    @certificate = certificate
    @acme_order = acme_order
  end

  def request_certificate!
    generate_csr! unless certificate
    @issued_certificate = request_certificate_or_raise
    certificate.provisioning_url = issued_certificate.url

    error = certificate.set_uploaded_data(issued_certificate.x509.to_pem)

    if error == [Certificate::ErrorCode::NEED_INTERMEDIATE]
      install_intermediate
      error = certificate.set_uploaded_data(issued_certificate.x509.to_pem)
    end

    certificate.save!
    raise Zendesk::AcmeJob::CertificateUploadFailure, error if error
  end

  def install_certificate!
    # If certificate is already active that means this was a renewal and therefore we are set.
    return if certificate.state == "active"

    # Activate new certificate remove any existing certificate
    certificate.upload_certificate

    if certificate.state == "active"
      Certificate.transaction do
        deactivate_certificate!(account.certificates.active.first)
        certificate.save!
      end
    else
      certificate.save!
      raise Zendesk::AcmeJob::CertificateNotActiveError, "certificate did not activate but is #{certificate.state}"
    end
  end

  def install_intermediate
    if !issued_certificate.x509_chain || !issued_certificate.x509_chain.one?
      raise Zendesk::AcmeJob::InstallIntermediateError, "expected one intermediate got #{issued_certificate.x509_chain.size}"
    end

    certificate.store.append_intermediate_from_pem(issued_certificate.x509_chain.first.to_pem)
  end

  def generate_csr!
    @certificate = account.certificates.new
    @certificate.sni_enabled = true
    @certificate.autoprovisioned = true
    @certificate.generate_temporary
    @certificate
  end

  def needs_renewal?
    self.class.needs_renewal?(certificate)
  end

  def self.needs_renewal?(certificate)
    renewal_cutoff = Time.now + EARLY_RENEWAL_DAYS.days
    certificate.autoprovisioned? && certificate.crt_not_after && certificate.crt_not_after < renewal_cutoff
  end

  private

  def request_certificate_or_raise
    csr = certificate.csr_object

    if account.has_lets_encrypt_acme_v2?
      Rails.logger.info("Requesting certificate for the order #{acme_order.url} for account #{account.id}")
      acme_order.finalize(csr: csr)

      retries = 0
      while acme_order.status == 'processing'
        sleep 2
        acme_order.reload
        retries += 1
        break if retries == 5
      end

      acme_order.reload
      @cert_provisioning_url = acme_order.certificate_url
      acme_cert = acme_order.certificate
      splitted = acme_cert.split("\n\n")
      Rails.logger.warn("Malformed no. of certs issued by Lets Encrypt") if splitted.length != 2

      NewCert.new(OpenSSL::X509::Certificate.new(acme_cert), @cert_provisioning_url, [OpenSSL::X509::Certificate.new(splitted[1])], csr)
    else
      client.new_certificate(csr)
    end
  rescue
    raise Zendesk::AcmeJob::CertificateRequestError, $!
  end

  def deactivate_certificate!(cert)
    return unless cert
    cert.detach_certificate_ips!
    cert.revoke
    cert.save!
  end
end
