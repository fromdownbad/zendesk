require 'json'
class TicketArchiveStub < ActiveRecord::Base
  disable_global_uid

  include CIA::Auditable
  audit_attribute if: :id_was
  audit_attribute :status_id, if: :soft_deletion_changed?

  belongs_to :account
  has_many :shared_tickets, inverse_of: :ticket, inherit: :account_id

  after_save :add_audit_and_archive_ticket, if: :soft_deletion_changed?
  after_save :record_deletion_audit, if: :soft_deleting?

  scope :for_user, ->(user) { user ? where(user.ticket_conditions) : nil }
  scope :deleted, -> { where(status_id: StatusType.DELETED) }

  def remove_from_archive
    ZendeskArchive.router.delete(self)
  end

  def exists_in_archive?
    ZendeskArchive.router.exists_in_any_store?(self)
  end

  def archive_key
    Ticket.archive_key(id: id, account_id: account_id, key_version: key_version)
  end

  def soft_delete(audit, *args)
    # the audit helpfully generated by the Ticket being deleted
    @audit = audit
    self.status_id = StatusType.DELETED
    self.generated_timestamp = Time.now
    save(*args)
  end

  def soft_undelete!(audit)
    @soft_undeletion = true
    @audit = audit
    self.status_id = StatusType.CLOSED
    self.generated_timestamp = Time.now
    save!(validate: false)
  ensure
    @soft_undeletion = false
  end

  private

  def add_audit_and_archive_ticket
    ticket = Ticket.with_deleted { account.tickets.find_by_nice_id(nice_id) }
    change = build_change(@audit)
    ticket.events << [@audit, change]

    ticket.save_to_archive(self)

    change.delete # need to do this manually because save_to_archive makes it readonly
    @audit.destroy
  end

  def build_change(audit)
    tuple = [StatusType.CLOSED, StatusType.DELETED]
    tuple.reverse! if soft_undeleting?
    Change.new do |change|
      change.audit = audit
      change.inherit_defaults_from_audit
      change.transaction = ['status_id', tuple]
    end
  end

  def record_deletion_audit
    CIA.record(:destroy, self)
  end

  def soft_deleting?
    (status_id == StatusType.DELETED) && status_id_was.present?
  end

  def soft_undeleting?
    @soft_undeletion
  end

  def soft_deletion_changed?
    soft_deleting? || soft_undeleting?
  end
end
