# Convenience model for handling uploaded attachments and linking them to an
# appropriate owner. This model is intended to grow to encapsulate different means
# of extracting/initializing attachment data.
class Upload
  class TokenHandler
    include Zendesk::Serialization::UploadSerialization

    def self.build(current_account, current_user, params, file_handle)
      token_string = params[:token]
      display_filename = params.fetch(:filename, nil)
      inline = params.fetch(:inline, false)
      ticket_id = params[:ticket_id]

      token      = resolve_token(current_account, current_user, token_string, ticket_id)
      attachment = token.add_attachment(file_handle, display_filename, inline)

      new(token, attachment)
    end

    def self.resolve_token(current_account, current_user, token_string, ticket_id)
      if token_string.present?
        current_account.upload_tokens.find_by_value!(token_string)
      else
        token_params = { source: current_account }

        if ticket_id.present? && current_account.has_polaris?
          token_params.update(target: current_account.tickets.find_by_nice_id!(ticket_id))
        elsif !current_user.is_anonymous_user?
          token_params.update(target: current_user)
        end

        current_account.upload_tokens.create(token_params)
      end
    end

    attr_accessor :token, :attachment

    def initialize(token, attachment)
      self.token      = token
      self.attachment = attachment
    end

    def save
      attachment.save
    end

    def save!
      attachment.save!
    rescue MiniMagick::Error, MiniMagick::Invalid => e
      message = "Could not process attachment - invalid or corrupted file?"
      ZendeskExceptions::Logger.record(e, location: self, message: message, fingerprint: 'd6f41e36fcef2150d57da3a37e1ecc87d5ae738b')
      Rails.logger.info(message)
      return false
    end

    def errors
      attachment.errors
    end
  end
end
