require_dependency 'zendesk/crm/external_data'

class ExternalTicketData < ActiveRecord::Base
  include Zendesk::Crm::ExternalData
  extend ZendeskArchive::AssociationLookaside

  self.table_name = 'external_ticket_datas'

  serialize :data, Hash
  belongs_to :ticket
  belongs_to :account

  association_accesses_archive :ticket

  attr_accessible :ticket, :sync_status, :data

  before_validation :set_account_id
  validates_presence_of :ticket, :account_id

  private

  def set_account_id
    self.account_id = ticket.account_id if ticket
  end
end
