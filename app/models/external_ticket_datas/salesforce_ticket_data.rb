require_dependency '../../lib/zendesk/salesforce/app_settings'

class SalesforceTicketData < ExternalTicketData
  include Zendesk::Serialization::CrmDataSerialization
  include Salesforce::AppSettings

  def start_sync(ticket)
    self.ticket = ticket
    sync_pending!
    SalesforceTicketSyncJob.enqueue(ticket.account_id, ticket.nice_id)
  end
end
