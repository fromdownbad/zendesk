module Guide
  class Trial
    DEFAULT_TRIAL_DURATION = 30.days
    ALTERNATIVE_TRIAL_DURATION = 14.days # 14-day trial test

    class << self
      def guide_product_payload(account, suite: false)
        {
          product: {
            state: Zendesk::Accounts::Product::TRIAL,
            trial_expires_at: calculate_trial_expires_at(account).iso8601,
            plan_settings: {
              plan_type: ZBC::Guide::PlanType::Professional.plan_type,
              suite: suite
            }
          }
        }
      end

      private

      def calculate_trial_expires_at(account)
        if account.trial_extras_alternative_length?
          DateTime.now.yesterday.end_of_day + ALTERNATIVE_TRIAL_DURATION
        else
          DateTime.now.yesterday.end_of_day + DEFAULT_TRIAL_DURATION
        end
      end
    end
  end
end
