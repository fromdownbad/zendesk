require 'cia'

module Guide
  class Subscription < ActiveRecord::Base
    include ::CIA::Auditable
    include Zendesk::ForbiddenAttributesProtection

    self.table_name = 'guide_subscriptions'

    audit_attribute :account_id,
      :active,
      :plan_type,
      :max_agents,
      callback: :after_commit

    belongs_to :account

    attr_accessible :active,
      :legacy,
      :max_agents,
      :plan_type,
      :trial,
      :trial_expires_at

    attr_accessor :legacy,
      :skip_product_callback,
      :trial,
      :trial_expires_at

    alias_attribute :is_active, :active # Note: Added for parity with other products

    validates_presence_of :account

    def deactivate
      self.active = false
      save!
    end

    def plan_name
      ZBC::Guide::PlanType[plan_type].try(:description)
    end

    def legacy?
      !!legacy
    end

    def trial?
      !!trial
    end

    def days_left_in_trial
      return unless trial_expires_at

      remaining_seconds = trial_expires_at.in_time_zone - Time.zone.now
      remaining_days    = remaining_seconds / 1.day

      remaining_days.ceil
    end
  end
end
