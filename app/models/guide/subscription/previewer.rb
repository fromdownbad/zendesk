module Guide
  class Subscription
    class Previewer
      def self.for_account(account)
        new(account).send(:preview)
      end

      private

      # Legacy plan types are only defined in the account service.
      LEGACY_PLAN_TYPE = 3

      attr_reader :account

      delegate :guide_subscription,
        to:     :account,
        prefix: :existing

      delegate :active?,
        :expired?,
        :plan_settings,
        :trial_expires_at,
        to: :account_service_product

      def initialize(account)
        @account = account
      end

      def preview
        active_guide_subscription || preview_account_service_product
      end

      def active_guide_subscription
        return unless existing_guide_subscription
        return unless existing_guide_subscription.active?

        existing_guide_subscription
      end

      def preview_account_service_product
        return unless account_service_product
        return unless account_service_product.started?

        account.build_guide_subscription(
          active:           active?,
          legacy:           legacy?,
          max_agents:       max_agents,
          plan_type:        plan_type,
          trial:            trial?,
          trial_expires_at: trial_expires_at
        )
      end

      def account_service_client
        Zendesk::Accounts::Client.new(account)
      end

      def account_service_product
        @account_service_product ||= begin
          if account.has_guide_account_service_error?
            account_service_client.product!('guide')
          else
            account_service_client.product('guide', use_cache: true)
          end
        rescue Kragle::ResourceNotFound, Kragle::ResponseError, Faraday::Error => e
          if e.class == Kragle::ResourceNotFound
            # A Kragle::ResourceNotFound error is raised when the account service
            # cannot find a specified product for the account. This may be because
            # the account has not activated the product yet.
            nil
          else
            # All other errors should be logged and propagated
            message = "Failed to GET product in account service: #{e.message}"
            Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
            raise e
          end
        end
      end

      def legacy?
        plan_settings['plan_type'].to_i == LEGACY_PLAN_TYPE
      end

      def max_agents
        account.subscription.max_agents # Pegged to Support's max_agents
      end

      def plan_type
        # Legacy plans get sent to the frontend without a plan_type
        return if legacy?

        plan_settings['plan_type'].to_i
      end

      def trial?
        account_service_product.trial? || account_service_product.expired?
      end
    end
  end
end
