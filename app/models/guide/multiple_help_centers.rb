require 'guide_plans'

class Guide::MultipleHelpCenters
  def initialize(account)
    @account = account
  end

  def available?
    account.has_multibrand? && (multiple_help_centers_from_support? || multiple_help_centers_from_guide?)
  end

  def available_via_support?
    account.has_multibrand? && multiple_help_centers_from_support?
  end

  private

  attr_reader :account

  def multiple_help_centers_from_support?
    account.has_unlimited_multibrand? || account.settings.multibrand_includes_help_centers || (account.spp? && account.has_multibrand?)
  end

  def multiple_help_centers_from_guide?
    return false unless guide_plan && guide_plan.active?

    plan_type = guide_plan.plan_settings['boosted_plan_type'] || guide_plan.plan_settings.fetch('plan_type')
    GuidePlans::Features.features(plan_type: plan_type).include?(:multiple_help_centers)
  end

  def guide_plan
    return @guide_plan if defined?(@guide_plan)

    @guide_plan = Zendesk::Accounts::Client.new(account).product(Zendesk::Accounts::Client::GUIDE_PRODUCT)
  end
end
