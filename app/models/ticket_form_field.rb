class TicketFormField < ActiveRecord::Base
  belongs_to :account
  belongs_to :ticket_form, touch: true, inherit: :account_id
  belongs_to :ticket_field, inherit: :account_id

  attr_accessible :account, :ticket_form, :ticket_field, :position

  validates_presence_of :account_id
  validates_presence_of :ticket_form
  validates_presence_of :ticket_field
end
