class CustomStatus < ActiveRecord::Base
  include PrecreatedAccountConversion
  include Api::V2::Tickets::AttributeMappings

  CATEGORIES = [
    StatusType.NEW,
    StatusType.OPEN,
    StatusType.PENDING,
    StatusType.HOLD,
    StatusType.SOLVED
  ].freeze
  CATEGORY_KEYWORDS = CATEGORIES.map { |status_id| STATUS_MAP[status_id.to_s] }.freeze
  POSITION_RANGES = {
    StatusType.NEW => (0..99),
    StatusType.OPEN => (100..199),
    StatusType.PENDING => (200..299),
    StatusType.HOLD => (300..399),
    StatusType.SOLVED => (400..499)
  }.freeze

  has_soft_deletion default_scope: true

  belongs_to :account

  attr_accessible :status_category,
    :active,
    :agent_label,
    :end_user_label,
    :description

  validates :agent_label,
    presence: true,
    uniqueness: { scope: [:account_id, :deleted_at] },
    length: { maximum: 32 }
  validates :end_user_label, presence: true, length: { maximum: 32 }
  validates :description, length: { maximum: 255 }
  validates :account_id, presence: true
  validates :system_status_id,
    presence: true,
    inclusion: {
      in: CATEGORIES,
      message: "Parent status can be open, pending or solved"
    },
    uniqueness: {
      scope: [:account_id, :position],
      message: "error handling this request"
    },
    unless: proc { |cs| cs.deleted? }

  before_validation :set_position, on: :create, unless: proc { |cs| cs.deleted? || cs.system_status_id.nil? }

  validate :ensure_valid_position, unless: proc { |cs| cs.deleted? || cs.system_status_id.nil? }
  validate :validate_system_status_id_is_immutable, if: :system_status_id_changed?, on: :update
  validate :ensure_not_setting_default_to_false
  validate :ensure_default_is_active

  before_save :ensure_one_default_per_status
  before_soft_delete :reset_position

  scope :for_status, ->(system_status_id) { where(system_status_id: system_status_id) }
  scope :active, -> { where(active: true) }

  def status_category
    STATUS_MAP[system_status_id.to_s]
  end

  def status_category=(category)
    self.system_status_id = StatusType.find(category)
  end

  private

  def set_position
    last_position = account.custom_statuses.for_status(system_status_id).maximum(:position)

    self.position = last_position ? (last_position + 1) : POSITION_RANGES[system_status_id].first
  end

  def ensure_valid_position
    unless POSITION_RANGES[system_status_id].include?(position)
      errors.add(:position, :invalid)
    end
  end

  def reset_position
    self.position = nil
  end

  def validate_system_status_id_is_immutable
    # validator only called if system_status_id_changed?
    errors.add(:system_status_id, 'can not be changed')
  end

  def ensure_one_default_per_status
    if default && default_changed?
      if persisted?
        account.custom_statuses.for_status(system_status_id).where(default: true).where('id != ?', id).update_all(default: false)
      else
        account.custom_statuses.for_status(system_status_id).where(default: true).update_all(default: false)
      end
    end
  end

  def ensure_not_setting_default_to_false
    if persisted? && default_changed? && !default?
      # we should only change the default to false by setting the value of the record you want the default to be true
      errors.add(:default, 'Must set another custom status to be the default')
    end
  end

  def ensure_default_is_active
    if (!active? || deleted_at?) && default?
      # we can't inactivate the default
      errors.add(:default, 'Can not set the default custom status to inactive')
    end
  end
end
