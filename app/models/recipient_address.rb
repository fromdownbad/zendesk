class RecipientAddress < ActiveRecord::Base
  include RecipientAddresses::ForwardingStatus
  include RecipientAddresses::SpfStatus
  include RecipientAddresses::CnameStatus
  include RecipientAddresses::DnsStatus
  include RecipientAddresses::MxStatus
  include PrecreatedAccountConversion

  SUPPORT_PRODUCT = 'support'.freeze
  COLLABORATION_PRODUCT = 'collaboration'.freeze

  belongs_to :account
  belongs_to :brand, touch: true, inherit: :account_id
  belongs_to :external_email_credential, inverse_of: :recipient_address

  UNSAFE_LOCAL_PARTS = %w[administrator hostmaster postmaster webmaster tm].freeze

  before_validation :set_brand_id, on: :create

  before_create :verify_forwarding_status, unless: :forwarding_verified?
  before_create :verify_spf_status
  before_create :verify_cname_status, unless: :deprecate_cname_checks?
  before_create :verify_dns_status
  before_create :verify_mx_status
  before_create :verify_spf_records

  before_save :demote_current_default, if: :is_new_default?
  before_save :update_verification_method

  before_destroy :with_default_halt_destroy, unless: :force_destroy?
  before_destroy :with_backup_halt_destroy, unless: :force_destroy?
  before_destroy :with_external_email_credential_halt_destroy, unless: :force_destroy?
  before_destroy :with_only_collaboration_address_halt_destroy, unless: :force_destroy?

  after_commit :send_verification_email

  include CIA::Auditable
  audit_attribute :email

  validates_presence_of   :account_id, :email
  validates_uniqueness_of :email, scope: :account_id
  validate                :validate_email_properly_formatted
  validate                :validate_email_does_not_belong_to_user,        if: :email_changed?
  validate                :validate_email_on_proper_zendesk_domain,       if: :email_changed?
  validate                :validate_do_not_remove_default,                on: :update
  validate                :validate_email_unchanged, if: :email_changed?, on: :update
  validate                :validate_no_default_on_unverified,             if: :is_new_default?
  validate                :validate_correct_brand,                        if: :brand_id_changed?
  validate                :validate_safe_local_part

  scope :by_email,          -> (email) { where(email: email) }
  scope :not_collaboration, -> { where('product IS NULL OR product != ?', COLLABORATION_PRODUCT) }

  attr_accessible :email, :name, :default, :brand_id, :brand, :product
  attr_accessor :creating_for_new_brand

  serialize :metadata, JSON

  def email=(address)
    address&.strip!
    address&.downcase!
    super(address)
  end

  def default=(value)
    if Zendesk::FALSE_VALUES.include?(value)
      super(nil)
    else
      super
    end
  end

  def backup?
    email == brand.backup_email_address
  end

  def position
    [backup? ? 0 : (default? ? 1 : 2), email] # rubocop:disable Style/NestedTernaryOperator
  end

  # TODO: can be removed once backfill migration has run
  def brand
    super || account.default_brand
  end

  def active_brand
    brand.active? ? brand : account.default_brand
  end

  def send_emails?
    (forwarding_verified? || recently_retried?) && brand.active?
  end

  def default_host?
    account.default_hosts.include? domain
  end

  def force_destroy
    @force_destroy = true
    if external_email_credential
      external_email_credential.destroy
    else
      destroy
    end
  end

  def product
    read_attribute(:product) || SUPPORT_PRODUCT
  end

  def collaboration?
    product == COLLABORATION_PRODUCT
  end

  def only_collaboration_address?
    collaboration? && !account.recipient_addresses.where(product: COLLABORATION_PRODUCT).where('id != ?', id).exists?
  end

  def domain_verification_code
    return if default_host? || email =~ Settings::EmailHelper::EMAIL_PROVIDERS_REGEX
    Digest::MD5.hexdigest("zendesk-verification-code-#{account.id}-#{domain}")[0...16]
  end

  def domain
    email.to_s.split("@").last
  end

  def update_verification_method
    if all_records_verified?(:cname)
      update_metadata("verification_method": "cname")
    elsif all_records_verified?(:mx) && all_records_verified?(:txt)
      update_metadata("verification_method": "mx")
    else
      update_metadata("verification_method": "cname")
    end
  end

  def verification_method
    metadata&.dig("verification_method") || "cname"
  end

  def zendesk_owned_domain_and_account?
    domain == Zendesk::Configuration.fetch(:host) && account.settings.owned_by_zendesk # Zendesk domains are automatically valid for Zendesk-owned accounts
  end

  def trusted_domain?
    default_host? || zendesk_owned_domain_and_account?
  end

  private

  def force_destroy?
    @force_destroy
  end

  def delete
    super
  end

  def with_default_halt_destroy
    if default?
      errors.add(
        :base, I18n.t("activerecord.errors.models.recipient_address.destroy_with_default")
      )
      false
    end
  end

  def with_backup_halt_destroy
    if backup?
      errors.add(
        :base, I18n.t("activerecord.errors.models.recipient_address.destroy_with_backup")
      )
      false
    end
  end

  def with_only_collaboration_address_halt_destroy
    if only_collaboration_address? && account.has_side_conversations_enabled?
      errors.add(
        :base, I18n.t("activerecord.errors.models.recipient_address.destroy_only_collaboration_address")
      )
      false
    end
  end

  def with_external_email_credential_halt_destroy
    if external_email_credential && !external_email_credential.marked_for_destruction?
      errors.add(
        :base, I18n.t("activerecord.errors.models.recipient_address.destroy_with_external_credential")
      )
      false
    end
  end

  def validate_email_properly_formatted
    unless Zendesk::Mail::Address.valid_inbound_address?(email)
      errors.add(
        :email, Api.error("activerecord.errors.models.recipient_address.attributes.email.invalid", error: "InvalidFormat")
      )
    end
  end

  def validate_email_does_not_belong_to_user
    if account.find_user_by_email(email)
      errors.add(
        :email, I18n.t("activerecord.errors.models.recipient_address.attributes.email.is_in_use_by_user")
      )
    end
  end

  def validate_email_unchanged
    errors.add(
      :email, Api.error("activerecord.errors.models.recipient_address.attributes.email.change_not_allowed", error: "ChangeNotAllowed")
    )
  end

  def validate_do_not_remove_default
    # changed the default to false, or change the brand on the default
    if (default_changed? && !default?) || (brand_id_changed? && default?)
      errors.add(:default, I18n.t("activerecord.errors.models.recipient_address.attributes.default_invalid", brand: brand.name))
    end
  end

  def validate_email_on_proper_zendesk_domain
    if invalid_domain?
      errors.add(:email, I18n.t("activerecord.errors.models.recipient_address.attributes.email.incorrect_domain"))
    end
  end

  def validate_correct_brand
    domain_brand_id = account.domain_brand_map[domain]
    if (domain_brand_id && brand_id != domain_brand_id) || (brand.account_id != account_id)
      errors.add(:brand_id, :invalid)
    end
  end

  def validate_safe_local_part
    if zendesk_subdomain? && UNSAFE_LOCAL_PARTS.include?(local_part)
      errors.add(:email, I18n.t("activerecord.errors.models.recipient_address.attributes.email.unsafe_local_part", local_part: local_part))
    end
  end

  def demote_current_default
    brand.recipient_addresses.where(default: true).update_all_with_updated_at(default: nil)
  end

  def is_new_default? # rubocop:disable Naming/PredicateName
    default? && default_changed?
  end

  def invalid_domain?
    return false if creating_for_new_brand
    return false unless domain
    return true if domain == Zendesk::Configuration.fetch(:host) && !account.settings.owned_by_zendesk # Zendesk domains are only valid for Zendesk-owned accounts
    domain.end_with?(".#{Zendesk::Configuration.fetch(:host)}") && !account.default_hosts.include?(domain)
  end

  def zendesk_subdomain?
    return false unless domain
    domain.end_with?(".#{Zendesk::Configuration.fetch(:host)}")
  end

  def local_part
    email.to_s.split("@").first
  end

  def set_brand_id
    return unless account
    return if new_record? && brand && !brand.id
    self.brand_id = brand_id || account.domain_brand_map[domain] || account.default_brand_id
  end

  def update_metadata!(updated_values)
    update_metadata(updated_values)
    save!
  end

  def update_metadata(updated_values)
    self.metadata = metadata.to_h.merge(updated_values)
  end

  def all_records_verified?(type)
    return true if zendesk_owned_domain_and_account?

    records = metadata&.dig(type.to_s).to_h

    return false unless VALID_HOSTS.all? { |host| records.key?(host) }

    records.select { |record, _| VALID_HOSTS.include?(record) }.all? { |_, value| value["status"] == "verified" }
  end

  def deprecate_cname_checks?
    account.has_email_deprecate_cname_checks?
  end
end
