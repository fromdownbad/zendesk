require 'certificate' # or zendesk_core will only load core certificate in dev
require 'zendesk/models/account'
require 'zendesk/models/accounts/cdn'
require 'zendesk/account_cdn'
require 'zendesk/o_auth/models/account'
require 'accounts/api_token_support'
require 'resolv'
require 'set'
require 'zendesk/outgoing_mail/support/account'

class Account < ActiveRecord::Base
  has_kasket
  kasket_dirty_methods :touch
  if ENV['KASKET_EXPIRES_IN_ACCOUNT'].to_i > 0
    kasket_expires_in ENV['KASKET_EXPIRES_IN_ACCOUNT'].to_i
  end

  include Zendesk::Accounts::Cdn
  include Authentication
  include AttachmentSupport
  include BccArchiving
  include BillableAgents
  include MultiproductBillableAgents
  include Capabilities
  include SpamSensitivity
  include MalwareWhitelist
  include ChatSupport
  include LightAgentSupport
  extend  Cleanup
  include CmsSupport
  include Localization
  include Suspension
  include HubAndSpoke
  include Zendesk::OutgoingMail::Support::Account
  include AccountMail
  include Ticketing
  include CustomStatuses
  include TicketSharingSupport
  include Voice::Core::Support::Account
  include Zendesk::Serialization::AccountSerialization
  include Screencasts
  include Channels::Account::TwitterSupport
  include VoiceSupport
  include MobileV2
  include CrmIntegration
  include FeatureFrameworkUsage
  include Zendesk::RoutingValidations
  include CachingObserver
  include Zendesk::Accounts::HelpCenterStatus # Defined in zendesk_core
  include CustomFields
  include ZendeskBillingCore::Account::Support
  include BrandSupport
  include CertificateSupport
  include RouteSupport
  include SoftDelete
  include PartnerSupport
  include AccountNameObserver
  include AccountTimeZoneObserver
  include AccountSubdomainObserver
  include AccountProductStateObserver
  include Accounts::ApiTokenSupport
  include ::Zendesk::Assets::Configuration
  include ZopimSupport
  include Upserting
  include TrialLimit
  include WebWidgetSupport
  include AssumptionSupport
  include TrialExtrasSupport
  include RadarSuspensionSupport
  include OnboardingSupport
  include ExploreSupport
  include FraudSupport
  include SideConversationsSupport
  include SuiteSupport
  include AgentWorkspaceSupport
  include CfaMigration
  include SecurityFeatureSupport
  include MultiproductSupport
  include LedgerRateLimitHelper

  prepend Account::PatagoniaApiRateLimiter

  has_kasket_on :id, :deleted_at
  has_kasket_on :subdomain, :deleted_at
  has_kasket_on :host_mapping, :deleted_at
  ignore_column :host

  def kasket_cacheable?
    super() && !is_locked? && pod_local?
  end

  def clear_kasket_cache!
    delete_keys_from_kasket_cache(kasket_keys)
  end

  def clear_reflections_kasket_cache!
    delete_keys_from_kasket_cache(self.class.reflections.values.map { |r| r.klass.kasket_key_for([[:account_id, id]]) })
  end

  # Reloads account associations individually back into Rails cache
  def reload_associations_into_kasket!
    self.class.reflections.keys.each do |key|
      collection = send(key)
      return unless collection
      collection.reload
    end
  end

  require_dependency "account/creation"
  require_dependency "account/properties"
  require_dependency "account/sandbox"
  require_dependency "account/accessible_attributes"
  require_dependency "account/customer_satisfaction_support"
  require_dependency "account/tracking"
  require_dependency "account/trial_extension_support"
  require_dependency "account/lifecycle_surveys_support"

  include CIA::Auditable
  include ScopedCacheKeys
  include Satisfaction::CachedScore
  include AccountTracking
  include TrialExtensionSupport
  include LifecycleSurveysSupport

  MANY_ORGANIZATIONS_SIZE    = 300
  SUITE_GA_DATETIME_FALLBACK = '2018-05-17 04:00:00 -0700'.freeze
  ORG_ALLOWLIST_DOMAIN_EXCLUSIONS = [
    '0755zb.com',
    '100.com',
    '111.com',
    '123.net',
    '126.com',
    '163.com',
    'aol.com',
    'autorambler.ru',
    'bk.ru',
    'digital.gov.au',
    'fastmail.com',
    'gmail.com',
    'hotmail.com',
    'icloud.com',
    'inbox.ru',
    'lenta.ru',
    'list.ru',
    'lycos.com',
    'mail.com',
    'mail.ru',
    'myrambler.ru',
    'outlook.com',
    'pikabu.ru',
    'protonmail.com',
    'qq.com',
    'rambler.ru',
    'rambler.ua',
    'tutanota.com',
    'yahoo.com',
    'yandex.com',
    'zohomail.co'
  ].freeze

  # new visible attribute -> new translation under txt.admin.views.reports.tabs.audits.attribute.<attribute>
  audit_attribute :multiproduct, :account_group_id, :is_active, :is_serviceable, :time_zone, :subdomain, :locale_id, :owner_id, :deleted_at, :host_mapping, callback: :after_commit
  attr_accessor :audit_message
  attr_reader :read_feature_bits_from_db

  belongs_to :owner, class_name: 'User', foreign_key: :owner_id

  has_one  :address, dependent: :destroy
  has_many :account_property_sets, dependent: :destroy
  has_many :account_settings, dependent: :destroy
  has_many :account_texts, dependent: :destroy
  has_many :permission_set_permissions, dependent: :destroy
  has_many :user_settings, dependent: :destroy, inverse_of: :account
  has_many :user_texts, dependent: :destroy
  has_many :events, dependent: :delete_all
  has_many :simplified_email_rule_bodies, dependent: :destroy

  has_many :compliance_deletion_statuses
  has_many :sequences, dependent: :destroy
  has_one  :pre_account_creation, dependent: :destroy
  has_one  :zuora_subscription, class_name: '::ZendeskBillingCore::Zuora::Subscription'

  # Not destroying dependents in order to retain fraud_score data to retrain the fraud model.
  has_many :fraud_scores, before_add: :validate_fraud_score_limit

  has_one :account_logo, dependent: :destroy, inverse_of: :account
  has_one :header_logo, dependent: :destroy
  has_one :mobile_logo, dependent: :destroy
  has_one :favicon, dependent: :destroy
  has_one :account_property_set, dependent: :destroy
  has_one :survey_response, class_name: 'Account::SurveyResponse'
  has_one :url_shortener, dependent: :destroy
  has_one :sharded_subscription, dependent: :destroy
  has_one :gooddata_integration
  has_one :role_settings, dependent: :destroy
  has_one :mobile_sdk_settings, dependent: :destroy

  has_one  :voice_subscription, dependent: :destroy, class_name: '::Voice::Subscription'
  has_one  :voice_recharge_settings, dependent: :destroy, class_name: '::Voice::RechargeSettings'
  has_one  :voice_partner_edition_account, dependent: :destroy, class_name: '::Voice::PartnerEditionAccount'
  has_one  :guide_subscription, dependent: :destroy, class_name: '::Guide::Subscription'
  has_one  :tpe_subscription, dependent: :destroy, class_name: '::Tpe::Subscription'

  has_one  :compliance_move, dependent: :destroy

  has_many :schedules, dependent: :destroy, class_name: 'Zendesk::BusinessHours::Schedule'
  has_many :compliance_agreements, dependent: :destroy

  has_many :categories, -> { order('position, name') }
  has_many :entries
  has_many :posts
  has_many :forums, -> { order('position ASC, id ASC') }, dependent: :destroy
  has_many :watchings, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :devices, dependent: :destroy
  has_many :mobile_devices, class_name: 'MobileDevice', dependent: :destroy
  has_many :device_identifiers, class_name: "PushNotifications::DeviceIdentifier", dependent: :destroy
  has_many :verification_tokens, -> { order(created_at: :desc, id: :desc) }, dependent: :destroy, inverse_of: :account
  has_many :password_reset_tokens, -> { order(created_at: :desc, id: :desc) }, dependent: :destroy
  has_many :master_tokens, -> { order(created_at: :desc, id: :desc) }, dependent: :destroy
  has_many :challenge_tokens, -> { order(created_at: :desc, id: :desc) }, dependent: :destroy
  has_many :log_me_in_session_tokens, -> { order(created_at: :desc, id: :desc) },  dependent: :destroy
  has_many :upload_tokens, -> { order(created_at: :desc, id: :desc) },  dependent: :destroy, inverse_of: :account
  has_many :satisfaction_rating_tokens, class_name: "Tokens::SatisfactionRatingToken"
  has_many :ticket_forms, -> { order(:position) },  dependent: :destroy, inverse_of: :account
  has_many :forwarding_verification_tokens, -> { order(created_at: :desc) }, dependent: :destroy
  has_many :otp_tokens, -> { order(created_at: :desc, id: :desc) }, dependent: :destroy
  has_many :user_seats
  has_many :skips, dependent: :destroy
  has_many :voice_cti_usages, class_name: 'Voice::CtiUsage', inverse_of: :account
  has_many :simplified_email_opt_in_settings, dependent: :destroy

  # TODO: smartify all users relation lookups...
  has_many :collaborations
  has_many :unverified_email_addresses

  has_many :users, -> { where(is_active: true) }, dependent: :destroy, inverse_of: :account
  has_many :all_users, class_name: 'User', dependent: :destroy

  has_many :experiments

  delegate :admins,
    :agents,
    :answer_bot_users,
    :assignable_agents,
    :chat_agents,
    :chat_enabled_support_agents,
    :chat_only_agents,
    :end_users,
    :light_agents,
    :non_chat_only_non_light_agents,
    :requester_agents,
    :revere_subscribers,
    :support_only_agents,
    :scrubbed_ticket_anonymous_users,
    :contributors,
    to: :users

  delegate :end_users, to: :all_users, prefix: :all

  has_many :groups, -> { where('groups.is_active = 1').order(name: :asc) }
  has_many :all_groups, class_name: "Group", dependent: :destroy
  has_many :memberships, dependent: :destroy
  has_many :organization_memberships, dependent: :destroy
  has_many :organizations, -> { order(name: :asc) }, dependent: :destroy
  has_many :organization_domains, -> { where(deleted_at: nil) }
  has_many :organization_emails, -> { where(deleted_at: nil) }
  has_many :attachments, -> { order(created_at: :asc, id: :asc) }, as: :source
  has_many :expirable_attachments
  has_many :taggings, dependent: :destroy
  has_many :tag_scores, -> { order(score: :desc) }, inverse_of: :account

  has_many :certificates, -> { order(created_at: :asc, id: :asc) }, inverse_of: :account
  has_many :acme_registrations, dependent: :destroy
  has_many :acme_authorizations, dependent: :destroy
  has_many :reviewed_tweets
  has_many :permission_sets, -> { order(name: :asc) }
  has_many :ticket_deflections

  has_many :ticket_stats, class_name: 'StatRollup::Ticket'

  # Uses the index on account_id to speed up the request
  has_many :ticket_audits, -> { where('events.parent_id IS NULL AND events.ticket_id IS NOT NULL').order('events.id DESC') }, class_name: 'Audit', inverse_of: :account

  # HelpCenter
  # the rest are for aggregations -- averages / top-n
  has_many :hc_search_stats, class_name: 'StatRollup::HCSearchStringStat'
  # EO HC

  has_many :user_views, inverse_of: :account

  has_many :csv_export_records, -> { order(created_at: :desc) }
  has_many :satisfaction_rating_intentions
  has_many :data_deletion_audits

  # Mobile SDK
  has_many :mobile_sdk_apps, dependent: :destroy
  has_many :mobile_sdk_auths, dependent: :destroy

  has_many :sla_policies, class_name: 'Sla::Policy'
  has_many :request_tokens
  has_many :shared_sessions, class_name: 'Zendesk::SharedSession::SessionRecord', dependent: :delete_all

  # Fraud reporting
  has_many :fraud_reports, class_name: 'Fraud::Report'

  has_many :subscription_feature_addons, dependent: :destroy
  has_many :api_activity_clients
  has_many :account_moves
  has_many :attribute_ticket_maps
  has_many :inbound_mail_rate_limits
  has_many :workspaces
  has_many :workspace_elements
  has_many :ticket_workspaces
  has_many :ticket_field_conditions
  has_many :agent_downgrade_audits

  delegate :signup_page_text, :signup_page_text=, :signup_email_text,
    :signup_email_text=, :html_mail_template, :html_mail_template=,
    :text_mail_template, :text_mail_template=,
    :branding, :branding=,
    :cc_email_template, :cc_email_template=,
    :follower_email_template, :follower_email_template=,
    :verify_email_text, :verify_email_text=,
    :introductory_text, :introductory_text=,
    :introductory_title, :introductory_title=,
    :url_login, :url_login=,
    :url_apikey, :url_apikey=,
    :organization_activity_email_template, :organization_activity_email_template=,
    :ticket_forms_instructions=,
    to: :account_property_set

  delegate :trial_expired?, :is_trial?, to: :subscription

  before_validation       :switch_kyev_to_kyiv
  validates_acceptance_of :terms_and_conditions
  validates_presence_of   :name
  validates_presence_of   :help_desk_size, on: :create, unless: :is_sandbox?
  validate                :validate_groups, on: :update, unless: :multiproduct?
  validate                :copy_errors_from_property_sets, :valid_owner_id?, on: :update
  validates_associated    :url_shortener, if: :url_shortening_on?
  validate                :validate_canceled_on_deletion
  validate                :validate_route_belongs_to_account, on: :update, if: :route_id_changed?

  before_save             :sanitize_account_name
  before_update           :normalize_settings
  after_update            :save_changes_to_property_set, :clear_caches
  after_save              :reset_url_generator
  after_commit            :instrument_product_sync_validation, on: :update
  after_update            :set_cancellation_date, if: proc { |account| (account.is_active_changed? || account.is_serviceable_changed?) }
  before_soft_delete      :verify_can_delete_for_cancellation
  after_soft_delete       :record_deletion_audit, :revoke_certificates, :delete_route_gam_domain, :deactivate_sharing_agreements!

  scope :exclude_test_expired, -> { where(["subdomain NOT LIKE ? AND subdomain NOT LIKE ?", "%test%", "%expired%"]) }

  # Trial Limits
  trial_limit :user_identity, limit: 1000, duration: 45.days.to_i

  def self.expired_for(days = 0.days)
    # trials that are expired, but not yet canceled
    where(is_active: true, is_serviceable: false).joins(:subscription).merge(Subscription.expired_before(days.ago))
  end

  def self.canceled_for(days = 0.days)
    joins(:subscription).merge(Subscription.canceled_before(days.ago))
  end

  # _MUST_ be called within a `Account.with_deleted { .. }` block.
  def self.deleted_for(days = 0.days)
    where("deleted_at <= ? ", days.ago)
  end

  # Account has been soft deleted with a 'waiting' hard deletion audit (created by after_soft_delete)
  # _MUST_ be called within a `Account.with_deleted { .. }` block.
  def self.waiting_hard_deletion
    where('deleted_at IS NOT NULL').joins(:data_deletion_audits).merge(DataDeletionAudit.waiting)
  end

  def self.lookup(search_string)
    conditions = 'CONCAT(subdomain, name) LIKE ? AND sandbox_master_id is NULL'

    includes(:subscription).where([conditions, "%#{search_string}%"]).order(:subdomain).all
  end

  def self.lookup_by_url(url)
    uri = URI.parse(url)
    matches = uri.host.match(/^(.*)\.#{Zendesk::Configuration['host']}$/)
    if matches
      find_by_subdomain(matches[1])
    else
      find_by_host_mapping(uri.host)
    end
  end

  def initialize(*args)
    super(*args)
    self.shard_id ||= 1
  end

  def schedule
    on_shard { schedules.active.by_time.first }
  end

  def without_feature_bits_caching
    @read_feature_bits_from_db = true
    yield
    @read_feature_bits_from_db = false
  end

  def to_s
    name
  end

  def to_liquid
    wrapper = Zendesk::Liquid::Wrapper.new("name")
    phone_numbers.each { |number| wrapper["incoming_phone_number_#{number.id}"] = number.to_liquid }
    wrapper['name'] = name
    wrapper
  end

  def self.system_account_id
    SYSTEM_ACCOUNT_ID
  end

  def self.root_account_id
    ROOT_ACCOUNT_ID
  end

  # TODO: Replace with 'accepts_nested_attributes_for :address' after moving to Rails 2.3
  def address_attributes=(attributes)
    address.update_attributes(attributes)
  end

  def anonymous_user
    return @anonymous_user if @anonymous_user
    @anonymous_user ||= User.new do |user|
      user.name = I18n.t('txt.users.role_name.anonymous')
      user.roles = Role::END_USER.id
    end
    @anonymous_user.account = self
    @anonymous_user.add_email_address('invalid@example.com')
    @anonymous_user
  end

  def min_autocomplete_chars
    @min_autocomplete_chars ||= Rails.cache.fetch("#{id}/min_autocomplete_chars", expires_in: 1.hour) do
      user_count = users.capped_count(100_000)

      if user_count > 99_000
        4
      elsif user_count > 1000
        3
      else
        2
      end
    end
  end

  def fetch_organizations_for_lists
    organizations.order(:name).limit(1000).pluck(:name, :id)
  end

  def many_organizations?
    organizations.count(:all) >= MANY_ORGANIZATIONS_SIZE
  end

  def has_public_forums? # rubocop:disable Naming/PredicateName
    Rails.cache.fetch("account/public-forums/#{scoped_cache_key(:forums)}") { forums.accessible_to(anonymous_user).exists? }
  end

  def spam_prevention=(status)
    case status
    when :cm, "cm"
      settings.spam_prevention = true
      self.using_wp_spam_filter = false
      self.is_forum_moderation_enabled = true
    when :sf, "sf"
      settings.spam_prevention = true
      self.using_wp_spam_filter = true
      self.is_forum_moderation_enabled = true
    when :off, "off"
      settings.spam_prevention = false
      self.using_wp_spam_filter = false
      self.is_forum_moderation_enabled = false
    end
  end

  def spam_prevention
    return :off unless settings.spam_prevention
    Rails.logger.error("ERROR: Spam Prevention: #{settings.spam_prevention} Content Moderation: false Spam Filter: #{using_wp_spam_filter}")
    :off
  end

  def is_forum_moderation_enabled=(value)
    settings.moderated_posts = value
    settings.moderated_entries = value
  end

  def using_wp_spam_filter
    settings.spam_filter?
  end

  alias_method :using_wp_spam_filter?, :using_wp_spam_filter

  def using_wp_spam_filter=(value)
    settings.spam_filter = value
  end

  def calendar_hours_ago(business_hours)
    if business_hours_active?
      business_hours_calculation.calendar_hours_ago(business_hours)
    else
      business_hours
    end
  end

  def calendar_hours_from_now(business_hours)
    if business_hours_active?
      business_hours_calculation.calendar_hours_from_now(business_hours)
    else
      business_hours
    end
  end

  def time_before_business_hours(time, business_hours)
    if business_hours_active?
      schedule.time(business_hours, :hours).before(time)
    else
      time - business_hours.hours
    end
  end

  def clear_caches
    [is_failing_counter_cache_key, dunning_level_cache_key].each do |key|
      Rails.cache.delete(key)
    end
  end

  def self.ids_to_shard_id_map(account_ids)
    where(id: account_ids).to_a.each_with_object({}) do |account, hash|
      hash[account.id] = account.shard_id
    end
  end

  def unrestricted_agents
    agents.assignable_agents.where('restriction_id = 0')
  end

  def allows?(address)
    if has_domain_blacklist_whitelist_on_slaves?
      ActiveRecord::Base.on_slave do
        Rails.logger.info("Using slave connection for domain whitelist and blacklist")
        whitelists?(address) ? true : !blacklists?(address)
      end
    else
      whitelists?(address) ? true : !blacklists?(address)
    end
  end

  def whitelists?(address)
    return true if in_domain_whitelist?(address)
    return true if organization_whitelists?(address)
    false
  end

  def in_domain_whitelist?(address)
    return false unless EMAIL_PATTERN.match(address)
    Zendesk::Auth::ListUtils.address_in_list?(address, domain_whitelist)
  end

  def organization_whitelists?(address)
    return false unless EMAIL_PATTERN.match(address)

    # NOTE: Organization emails are not affected by the allowlist domain
    # exclusions. The intent is if someone explicity adds an email to the
    # organization then they will want that email to be allowlisted. i.e. If
    # there is an `OrganizationEmail` record with the value "example@gmail.com"
    # then it will be allowlisted even though "gmail.com" is on the
    # `ORG_ALLOWLIST_DOMAIN_EXCLUSIONS`
    return true if organization_emails.exists?(value: address)

    address_domain = address.split("@", 2).last

    if ORG_ALLOWLIST_DOMAIN_EXCLUSIONS.include?(address_domain)
      return false
    end

    possible_allowed_domains = []
    address_domain.count(".").times do
      possible_allowed_domains << address_domain
      address_domain = address_domain[address_domain.index(".") + 1, address_domain.length]
    end

    organization_domains.exists?(value: possible_allowed_domains)
  end

  def blacklists?(address)
    Zendesk::Auth::ListUtils.address_in_list?(address, domain_blacklist)
  end

  def conflicting_domain_list
    blacklisted = domain_blacklist.to_s.split(" ")

    blacklisted_domains = blacklisted.map { |x| x.split("@").last }
    conflicting_domains = organization_domains.where(value: blacklisted_domains).pluck(:value)
    conflicting_emails = organization_emails.where(value: blacklisted).pluck(:value)

    (blacklisted.select { |b| conflicting_domains.include?(b.split("@").last) } + conflicting_emails).uniq.join(" ")
  end

  def default_organization_for(domain_name)
    return if domain_name.blank?

    organizations.
      joins(:organization_domains).
      where(organization_domains: {account: self, value: domain_name}).
      order(name: :ASC).
      limit(1).
      first
  end

  def allows_collaborator?(address)
    address = address.downcase

    recipient_addresses.each do |recipient_address|
      return false if recipient_address.email == address
      return false if recipient_address.default_host? && recipient_address.email == address.sub(/(\+.*)@/, '@')
    end

    !Zendesk::Auth::ListUtils.address_in_list?(address, cc_blacklist)
  end

  def render_forum_title(current_user)
    Zendesk::Liquid::DcContext.render(forum_title, self, current_user, 'text/plain', I18n.translation_locale)
  end

  def find_user_by_email(email, options = {})
    scope = options[:scope] || users
     # RAILS5UPGRADE: options for .find_by_email is deprecated. options[:scope] was the only one found throughout the codebase and is redundant
    scope.find_by_identity user_identities.find_by_email(email.try(:strip))
  end

  def find_user_by_main_identity(value, options = {})
    scope = options[:scope] || users
    scope.find_by_identity user_identities.find_by_main_identity(value)
  end

  def find_user_by_facebook_id(facebook_id, options = {})
    scope = options[:scope] || users
    scope.find_by_identity user_identities.find_by_facebook_id(facebook_id)
  end

  def find_user_by_sdk_id(sdk_id, options = {})
    scope = options[:scope] || users
    scope.find_by_identity user_identities.find_by_sdk_id(sdk_id)
  end

  def find_user_by_twitter_screen_name(screen_name, options = {})
    scope = options[:scope] || users
    scope.find_by_identity user_identities.find_by_twitter_screen_name(screen_name)
  end

  def find_or_create_user_by_email(email, options = {})
    scope = options[:scope] || users
    find_user_by_email(email, options) || scope.create_with_email(email, options)
  end

  def entry_is_top_search_result?(search_string, entry_id)
    @top_entry_hash ||= begin
      res = search_stats.where(origin: 'all', duration: 30.days.to_i).where('ts >= ?', 31.days.ago).to_a
      res.each_with_object({}) do |obj, hash|
        hash[obj.string] = obj.top_entry_id unless obj.top_entry_id.nil?
      end
    end
    @top_entry_hash[search_string] == entry_id
  end

  def default_group
    groups.where(default: true).first
  end

  def chat_welcome_message(show_placeholder = false)
    show_placeholder ? account_property_set.chat_welcome_message : chat_welcome_message_for_user(nil)
  end

  def chat_welcome_message_for_user(user)
    Zendesk::Liquid::DcContext.render(account_property_set.chat_welcome_message, self, user)
  end

  def ticket_forms_instructions
    Zendesk::Liquid::DcContext.render(account_property_set.ticket_forms_instructions, self, nil)
  end

  if defined?(IRB)
    def shard
      Kernel.send(:shard, self)
      self
    end
  end

  def shard_region
    @shard_region ||= Zendesk::Accounts::DataCenter.pod_config_for_shard(self.shard_id)[:region].to_sym
    @shard_region
  end

  # key used to sanity-check arguments during account moves.
  def idsub
    "#{id}:#{subdomain}"
  end

  def fallback_cdn_provider
    return @fallback_cdn_provider if defined?(@fallback_cdn_provider)

    @fallback_cdn_provider = if cdn_provider == 'disabled'
      nil
    else
      fallback_cdn.try(:first)
    end
  end

  def fallback_cdn
    # Select the first CDN provider in the list that isn't the primary one.
    provider_pool = Zendesk::AccountCdn.provider_pool
    primary_provider = provider_pool.account_provider(self)
    provider_pool.providers.detect do |_key, provider|
      provider.host != primary_provider.host && provider.enabled? && provider.cdn
    end
  end

  # This implementation requires the feature to be registered in capabilities.rb
  # A more simple alternative you should use is:
  #     Arturo.feature_enabled_for?(feature_name, account)
  def arturo_enabled?(feature_name)
    feature_method = :"has_#{feature_name}?"
    respond_to?(feature_method) && send(feature_method)
  end

  def expire_trial!
    self.is_serviceable = false unless multiproduct?
    subscription.is_trial = false
    Zendesk::SupportAccounts::Product.retrieve(self).save(validate: false)
    account_service_client_with_patch_retries.update_product('support', { product: { state: 'expired' } }, context: "account_expire_trial")
  end

  def cc_subject_template
    settings.cc_subject_template.presence || I18n.t("txt.default.cc_email_subject_v2")
  end

  def follower_subject_template
    settings.follower_subject_template.presence || I18n.t("txt.default.follower_email_subject")
  end

  def cc_subject_template=(value)
    return if cc_subject_template == value
    settings.cc_subject_template = value
  end

  def follower_subject_template=(value)
    return if follower_subject_template == value
    settings.follower_subject_template = value
  end

  def subdomain=(new_subdomain)
    super

    # reset url_generator before after_save callback
    # to make sure welcome email have the correct value
    reset_url_generator
  end

  # Returns a soft-deleted account back to canceled status
  # This method is getting deprecated, please use
  # Zendesk::SupportAccounts::Product.retrieve(account).restore! instead.
  def restore!
    Zendesk::MethodCallInstrumentation.new(
      method_name: 'Account#restore!',
      exec_context: binding,
      source_pattern: 'app\/models\/account'
    ).perform

    Zendesk::SupportAccounts::Product.retrieve(self).restore!
  end

  def verify_can_delete_for_cancellation
    raise Zendesk::DataDeletion::PermanentError, 'Account deletion prevented by arturo feature: prevent_deletion_if_churned' if has_prevent_deletion_if_churned?
  end

  def is_account_precreation? # rubocop:disable Naming/PredicateName
    PreAccountCreation.where(account_id: id).count(:all) > 0
  end

  def pre_created_account?
    subdomain.starts_with?(PreAccountCreation::SUBDOMAIN_PREFIX) && name == PreAccountCreation::DEFAULT_ACCOUNT_NAME
  end

  def set_conditional_logging(request_method: nil, path_info: nil, http_user_agent: nil, query_string: nil, user_id: nil, status: nil, response: nil)
    patterns = [request_method, path_info, http_user_agent, query_string, user_id, status, response]
    raise ArgumentError, "Only regexp and nil are supported" if patterns.reject { |p| p.nil? || p.is_a?(Regexp) }.any?
    setting = JSON.dump(patterns) if patterns.any?
    raise ArgumentError, "Pattern size is #{setting.length}, but only 254 characters are allowed" if setting && setting.length > 254
    settings.conditional_logging_pattern = setting
  end

  def add_conditional_rate_limit(key:, limit:, interval:, strict_match: false, request_method: nil, endpoint: nil, user_agent: nil, query_string: nil, user_id: nil, enforce_lotus: nil, ip_address: nil, expires_on: nil, owner_email: nil)
    rule = {
      'prop_key'         => key,
      'limit'            => limit,
      'interval_seconds' => interval,
      'expires_on'       => expires_on,
      'owner_email'      => owner_email,
      'strict_match'     => strict_match,
      'properties'       => {
        'request_method' => request_method,
        'endpoint'       => endpoint,
        'user_agent'     => user_agent,
        'query_string'   => query_string,
        'user_id'        => user_id,
        'enforce_lotus'  => enforce_lotus,
        'ip_address'     => ip_address
      }
    }

    current_limits = texts.conditional_rate_limits || []
    current_limits << rule

    texts.set(conditional_rate_limits: current_limits)
    ledger_log_rate_limit_create(endpoint, user_id)
  end

  def add_conditional_rate_limit!(*args)
    add_conditional_rate_limit(*args)
    save!
  end

  def remove_conditional_rate_limit!(key)
    key = key.to_sym
    Prop.configurations.delete(key)
    texts.conditional_rate_limits = texts.conditional_rate_limits.delete_if { |r| r['prop_key'].to_sym == key }
    ledger_log_rate_limit_destroy(key)
    save!
  end

  def clear_conditional_rate_limits!
    Array(texts.conditional_rate_limits).each do |rule|
      Prop.configurations.delete(rule['prop_key'].to_sym)
      ledger_log_rate_limit_destroy(rule['prop_key'])
    end
    texts.conditional_rate_limits = nil
    save!
  end

  def disable_triggers(title_keys)
    titles = title_keys.map { |key| I18n.t(key, locale: translation_locale) }

    triggers.where(title: titles, is_active: true).each do |trigger|
      trigger.update_attribute(:is_active, false)
    end
  end

  def has_google_apps? # rubocop:disable Naming/PredicateName
    route && route.gam_domain.present? && role_settings && role_settings.agent_google_login
  end

  # This will be invoked when role_settings is updated
  def update_has_google_apps
    return unless route.present? && role_settings.present?
    has_google_apps = route.gam_domain.present? && role_settings.agent_google_login
    if has_google_apps != settings.has_google_apps
      CIA.audit(actor: CIA.current_actor || User.system) do
        settings.has_google_apps = has_google_apps
        settings.save!
      end
    end
  end

  def enable_google_login!
    role_settings.update_attributes!(
      agent_zendesk_login: false,
      agent_google_login: true,
      end_user_google_login: true
    )
  end

  def enable_office_365_login!
    role_settings.update_attributes!(
      agent_zendesk_login: false,
      agent_office_365_login: true,
      end_user_office_365_login: true
    )
  end

  def enable_google_apps!(google_domain)
    CIA.audit(actor: CIA.current_actor || User.system) do
      route.update_attribute(:gam_domain, google_domain)
      enable_google_login!
      settings.set(
        google_apps_domain: google_domain,
        switched_to_google_apps: true
      )
      settings.save!
    end
  end

  def disable_google_apps!
    CIA.audit(actor: CIA.current_actor || User.system) do
      route.update_attribute(:gam_domain, nil)
      role_settings.update_attributes!(
        agent_zendesk_login: true,
        agent_google_login: false,
        end_user_google_login: false
      )
      settings.set(
        google_apps_domain: nil,
        has_google_apps_admin: false,
        switched_to_google_apps: false
      )
      settings.save!
    end
  end

  def help_center_onboarding_state
    (help_center_state == :disabled) ? :enabled : :disabled
  end

  def email_settings_path
    "/agent/admin/email"
  end

  def apply_phone_number_validation?
    is_end_user_phone_number_validation_enabled? || has_apply_prefix_and_normalize_phone_numbers?
  end

  def answer_bot_subscription
    AnswerBot::Subscription.find_for_account(self)
  end

  def create_answer_bot_subscription
    AnswerBot::Subscription.create_for_account(self)
  end

  def outbound_subscription
    Outbound::Subscription.find_for_account(self)
  end

  def create_outbound_subscription
    Outbound::Subscription.create_for_account(self)
  end

  def explore_subscription
    Explore::Subscription.find_for_account(self)
  end

  def create_explore_subscription(attributes = nil)
    Explore::Subscription.create_for_account(self, attributes)
  end

  def billing_multi_product_participant?
    Billing::SobaParticipation.eligible?(self)
  end

  def suite_allowed?
    has_active_suite? || suite_allowed_by_restrictions?
  end

  def zuora_account_id
    @zuora_account_id ||= begin
      billing_id || subscription.zuora_subscription.try(:zuora_account_id)
    end
  end

  def products(use_cache: nil)
    cache_option = if use_cache.nil?
      has_ocp_account_products_use_cache?
    else
      use_cache
    end
    @products ||= account_service_client.products(use_cache: cache_option)
  end

  def active_products?
    products.any?(&:active?)
  end

  def subscription
    super || NilSubscription.new(self)
  end

  # An account is canceled or churned when is_active and is_serviceable are false
  # You can re-activate an account by setting is_active or is_serviceable to true
  # Churned On date must be reset on reactivation to avoid deletion of account by maintenance job
  #
  def set_cancellation_date
    if !is_active && !is_serviceable
      if subscription.present? && subscription.churned_on.nil?
        subscription.churned_on = Time.now
        subscription.save!
      end
    elsif is_active || is_serviceable
      if subscription.present? && subscription.churned_on.present?
        subscription.churned_on = nil
        subscription.save!
      end
    end
  end

  def render_custom_uri_hyperlinks
    if has_render_custom_uri_as_links?
      texts.render_custom_uri_hyperlinks.to_s.split(",").map(&:strip)
    else
      []
    end
  end

  def spp?
    has_ocp_spp? && account_service_indicates_spp?
  end

  def spp_suite?
    spp? && !!account_service_client.product('support').zendesk_suite_sku?
  end

  protected

  def account_service_indicates_spp?
    !!account_service_client.account_from_account_service(false)&.fetch('is_spp', false)
  end

  def valid_owner_id?
    if user_from_owner_id.nil? || user_from_owner_id.suspended?
      errors.add(:owner, :invalid)
    end
  end

  def user_from_owner_id
    @user_from_owner_id ||= if Arturo.feature_enabled_for?(:chat_phase_four_staff, self)
      agents.find_by_id(owner_id)
    else
      admins.find_by_id(owner_id)
    end
  end

  def save_changes_to_property_set
    account_property_set.save if account_property_set && account_property_set.changed?
    true
  end

  def validate_groups
    errors.add(:base, 'Account must have at least one group') if groups.empty?
    errors.add(:base, 'Account must have at least one admin') if admins.empty?
  end

  def validate_canceled_on_deletion
    # canceled_on can only be set on inactive and unserviceable accounts
    if deleted_at.present? && !subscription.canceled_on.present?
      errors.add(:deleted_at, 'cannot be set on active or serviceable accounts')
    end
  end

  def validate_route_belongs_to_account
    if route.account != self
      errors.add(:route_id, 'does not belong to account')
    end
  end

  def copy_errors_from_property_sets
    return if !account_property_set || account_property_set.valid?
    account_property_set.errors.each do |attribute, message|
      errors.add(attribute, message)
    end
  end

  def normalize_settings
    self.is_signup_required = false if is_signup_required? && (help_center_disabled? || !is_open?)
    self.host_mapping = host_mapping.presence.try(:downcase)
    true
  end

  def switch_kyev_to_kyiv
    if time_zone == 'Kyev' && has_switch_kyev_to_kyiv?
      self.time_zone = 'Kyiv'
      Rails.logger.info("Updated account time_zone to Kyiv instead of Kyev")
    end
  end

  def sanitize_account_name
    self.name = name.strip_tags
  end

  def record_deletion_audit
    data_deletion_audits << DataDeletionAudit.build_for_cancellation
  end

  def revoke_certificates
    certificates.active.each do |certificate|
      certificate.revoke
      certificate.save!
    end
  end

  def delete_route_gam_domain
    route.update_attribute(:gam_domain, nil) if route
  end

  def business_hours_calculation
    Zendesk::BusinessHoursSupport::Calculation.new(schedule)
  end

  def instrument_product_sync_validation
    ProductSyncValidationJob.enqueue_in(ProductSyncValidationJob.job_enqueue_delay, id)
  end

  private

  def suite_allowed_by_restrictions?
    has_billing_suite? && no_chat_phase_one? && meets_date_requirement?
  end

  def no_chat_phase_one?
    zopim_integration.blank? || !zopim_integration.phase_one?
  end

  def meets_date_requirement?
    has_suite_purchase_override? || billing_allow_suite_by_date?
  end

  def billing_allow_suite_by_date?
    has_restrict_suite? ? false : created_after_suite_ga?
  end

  def created_after_suite_ga?
    created_at > suite_launch_date
  end

  def suite_launch_date
    value = ENV.fetch('BILLING_SUITE_GA_DATETIME', SUITE_GA_DATETIME_FALLBACK)
    Time.parse(value)
  end

  # Clears cache on every memcache server individually, to ensure consistency
  # across every node in the ring. Otherwise, stale values could be returned
  # if the primary node was down during the previous write or the clear.
  def delete_keys_from_kasket_cache(keys)
    memcache_options = Rails.application.config.memcache_options

    servers = if /dalli/i.match?(Kasket.cache.class.name)
      Kasket.cache.stats.keys.map { |addr| ::Dalli::Client.new(addr, memcache_options) }
    else
      options = memcache_options.merge(buffer_requests: true)
      Kasket.cache.addresses.map { |addr| ::Memcached.new(addr, options) }
    end

    Rails.logger.debug("Cache delete_multi: #{keys.inspect}")
    servers.each do |s|
      begin
        keys.each { |k| s.delete(k) }
        s.get(keys)
      rescue Memcached::Error, Dalli::RingError # only fails when server is dead
        Rails.logger.warn("Memcached server error #{$!}")
      end
    end
  end

  def redis_client
    Zendesk::RedisStore.client
  end

  def url_shortening_on?
    !settings.no_short_url_tweet_append? && has_end_user_ui?
  end

  def account_service_client
    @account_service_client ||= Zendesk::Accounts::Client.new(
      self,
      retry_options: {
        max: 3,
        interval: 2,
        exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS
      },
      timeout: 5
    )
  end

  def account_service_client_with_patch_retries
    @account_service_client_with_patch_retries ||= Zendesk::Accounts::Client.new(
      self,
      retry_options: {
        max: 3,
        interval: 2,
        exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
        methods: Faraday::Request::Retry::IDEMPOTENT_METHODS + [:patch]
      },
      timeout: 10
    )
  end
end
