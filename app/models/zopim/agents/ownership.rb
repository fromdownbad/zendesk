module Zopim
  module Agents; end
  module Agents::Ownership
    extend ActiveSupport::Concern

    class AgentsOutOfSyncError < StandardError; end

    module ClassMethods
      def transfer_ownership!(zopim_subscription, user)
        return false unless user.account_id == zopim_subscription.account_id
        return false unless user.is_admin?
        return true if user.zopim_identity.try(:is_owner?)

        fail AgentsOutOfSyncError, 'Zendesk/Zopim Agents or owner are not in ' \
          'sync. Aborting since transfer ownship will fail. Please contact ' \
          'Dev (Billing).' unless agents_in_sync?(zopim_subscription)

        # create zopim identity for user if possible
        if zopim_seats_available?(zopim_subscription)
          enable_chat_identity(user) unless user.zopim_identity.try(:is_enabled)
        end

        if user.zopim_identity.present?
          swap_ownership_with_incumbent(zopim_subscription, user)
        else
          acquire_ownership_from_incumbent(zopim_subscription, user)
        end

        true
      end

      private

      def zopim_seats_available?(zopim_subscription)
        zopim_subscription.zopim_agents.enabled.count < zopim_subscription.max_agents
      end

      def enable_chat_identity(user)
        user.enable_chat_identity!
        user.zopim_identity.reload
      end

      def agents_in_sync?(zopim_subscription)
        zopim_account_id = zopim_subscription.zopim_account_id

        # Verify that email address / zopim agent ID match
        remote_zopim_agents   = Reseller.client.account_agents!(id: zopim_account_id)
        zopim_agents_by_email = zopim_subscription.zopim_agents.index_by(&:email)

        remote_zopim_agents.each do |remote|
          agent = zopim_agents_by_email[remote.email]
          return false if remote.id != agent.try(:zopim_agent_id)
        end

        # Verify that the owner email address matches the remote
        remote_owner = remote_zopim_agents.find(&:owner)
        owner        = zopim_subscription.owner_agent

        owner.email.eql?(remote_owner.email)
      end

      def acquire_ownership_from_incumbent(zopim_subscription, user)
        incumbent = zopim_subscription.owner_agent
        incumbent_remote = incumbent.reseller_data
        payload = {
          first_name: user.first_name,
          last_name: user.last_name,
          display_name: user.name,
          email: user.zopim_email
        }

        configure_remote_agent_record(zopim_subscription, incumbent_remote.id, payload)
        configure_remote_owner_record(zopim_subscription, payload.except(:display_name))

        payload[:user_id] = user.id
        zopim_subscription.zopim_agents.where(id: incumbent.id).
          update_all(payload.except(:first_name, :last_name))
        zopim_subscription.update_column(:owner_email, user.zopim_email)
      end

      def swap_ownership_with_incumbent(zopim_subscription, user)
        incumbent = zopim_subscription.owner_agent
        incumbent_remote = incumbent.reseller_data
        candidate = user.zopim_identity
        candidate_remote = candidate.reseller_data

        configure_remote_agent_record(
          zopim_subscription, candidate_remote.id,
          display_name: "tmp_#{candidate.display_name}",
          email: "tmp_#{candidate.email}"
        )
        configure_remote_agent_record(zopim_subscription, incumbent_remote.id, candidate)
        configure_remote_agent_record(zopim_subscription, candidate_remote.id, incumbent)
        configure_remote_owner_record(zopim_subscription, candidate)

        zopim_subscription.zopim_agents.where(id: incumbent.id).
          update_all(is_owner: false, is_enabled: candidate.is_enabled, zopim_agent_id: candidate_remote.id)
        zopim_subscription.zopim_agents.where(id: candidate.id).
          update_all(is_owner: true, is_enabled: true, zopim_agent_id: incumbent_remote.id)
        zopim_subscription.update_column(:owner_email, candidate.email)
      end

      def configure_remote_agent_record(zopim_subscription, agent_id, identity_or_hash)
        payload = identity_or_hash unless identity_or_hash.instance_of?(Agent)
        payload ||= {
          first_name: identity_or_hash.user.first_name,
          last_name: identity_or_hash.user.last_name,
          display_name: identity_or_hash.display_name,
          email: identity_or_hash.email
        }
        Reseller.client.update_account_agent!(
          id: zopim_subscription.zopim_account_id, agent_id: agent_id, data: payload
        )
      end

      def configure_remote_owner_record(zopim_subscription, identity_or_hash)
        payload = identity_or_hash unless identity_or_hash.instance_of?(Agent)
        payload ||= {
          first_name: identity_or_hash.user.first_name,
          last_name: identity_or_hash.user.last_name,
          email: identity_or_hash.email
        }
        Reseller.client.update_account!(
          id: zopim_subscription.zopim_account_id, data: payload
        )
      end
    end
  end
end
