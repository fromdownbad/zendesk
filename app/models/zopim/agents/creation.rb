module Zopim
  module Agents; end
  module Agents::Creation
    extend ActiveSupport::Concern

    included do
      before_validation :configure,
        on: :create

      validate :account_has_chat_access,
        :chat_account_is_serviceable

      validate :agent_population_limit,
        :trial_population_limit,
        :user_has_chat_privilege,
        :user_has_primary_email,
        on: :create

      before_create :adjust_account_max_agent
      before_create :ensure_user_chat_permission_set, unless: :active_suite_account?
      before_create :link_to_remote_account_record,   unless: :is_owner?
    end

    private

    def configure
      self.account            ||= user.account
      self.zopim_subscription ||= account.zopim_subscription
      self.email              ||= user.zopim_email
      self.display_name       ||= user.name
      self.is_administrator = !!user.is_admin?
      self
    end

    def ensure_user_chat_permission_set
      user.assign_chat_permission_set_id
    end

    def adjust_account_max_agent
      return unless account_max_agent_reached?
      count = active_zopim_agents_count + 1
      zopim_subscription.update_attribute(:max_agents, count)
    end

    def account_max_agent_reached?
      zopim_subscription.try(:is_trial?) &&
        (active_zopim_agents_count + 1) > zopim_subscription.max_agents
    end

    def new_owner_agent?
      new_record? && is_owner?
    end

    def account_has_chat_access
      return if new_owner_agent?
      return if account.has_chat_permission_set_or_active_suite?
      errors.add(:base, 'AccountChatAccessNotEnabled') unless account.has_ocp_chat_only_agent_deprecation?
      return if account.zopim_identity_available?
      errors.add(:base, 'AccountChatAccessNotEnabled')
    end

    def chat_account_is_serviceable
      return if new_owner_agent? && zopim_subscription.try(:is_linked?)
      return if zopim_subscription.try(:is_serviceable?)
      errors.add(:base, 'ServiceableChatAccountRequired')
    end

    def agent_population_limit
      return if zopim_subscription.try(:is_trial?)
      return if active_zopim_agents_count < zopim_subscription.try(:max_agents).to_i
      errors.add(
        :base,
        Api::Presentation::Error.new(
          'TooManyAgents',
          error: Users::Roles::MAX_AGENT_EXCEEDED,
          raw: true
        )
      )
    end

    def trial_population_limit
      return unless zopim_subscription.try(:is_trial?)
      return unless active_zopim_agents_count >= ZendeskBillingCore::Zopim::Subscription::TrialManagement::TRIAL_AGENTS_LIMIT
      errors.add(:base, 'TrialAgentLimitReached')
    end

    def active_zopim_agents_count
      if account.has_ocp_cp3_suite_check_support_for_zopim_agent_creation? && account.has_active_suite?
        (account.zopim_agents.enabled.pluck(:user_id) + account.billable_agents.pluck(:id)).uniq.count
      else
        account.zopim_agents.enabled.count
      end
    end

    def user_has_chat_privilege
      return if user.is_agent? || user.is_chat_agent?
      errors.add(:base, 'UserChatPrivilegeRequired')
    end

    def user_has_primary_email
      return if user.identities.detect { |id| id.is_a?(UserEmailIdentity) && id.primary? }
      errors.add(:base, 'UserPrimaryEmailRequired')
    end

    def link_to_remote_account_record
      self.zopim_agent_id = account.zopim_integration.try(:phase_three?) ? user.id * -1 : remote_account_record.id
    end

    def account_create_payload
      {
        email:         email,
        password:      SecureRandom.hex,
        first_name:    user.first_name,
        last_name:     user.has_last_name? ? user.last_name : '',
        display_name:  display_name,
        administrator: is_administrator?,
        enabled:       true
      }
    end

    def remote_account_record
      @remote_account_record ||= if zopim_agent_id.present?
        Reseller.client.account_agent!(
          id:       zopim_account_id,
          agent_id: zopim_agent_id
        )
      else
        Reseller.client.create_account_agent!(
          id:   zopim_account_id,
          data: account_create_payload
        )
      end
    end

    def active_suite_account?
      account.has_active_suite?
    end
  end
end
