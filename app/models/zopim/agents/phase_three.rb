module Zopim
  module Agents; end
  module Agents::PhaseThree
    extend ActiveSupport::Concern
    include Zendesk::SupportUsers::Internal::AuditHeaders

    ENTITLEMENT_FIELDS = [:is_enabled, :is_administrator].freeze

    included do
      after_commit :update_staff_service, if: :update_staff_service?
    end

    private

    def update_staff_service?
      phase_three? && (new_zopim_identity? || entitlement_changed?)
    end

    def new_zopim_identity?
      previous_changes[:id].present?
    end

    def entitlement_changed?
      ENTITLEMENT_FIELDS.any? { |f| previous_changes[f].present? }
    end

    def update_staff_service
      ChatPhaseThree::EntitlementSyncJob.enqueue(account_id, user_id, audit_headers)
    end

    def chat_entitlement
      return unless is_enabled?

      if is_administrator?
        Zendesk::Entitlement::ROLES[:chat][:admin]
      else
        Zendesk::Entitlement::ROLES[:chat][:agent]
      end
    end
  end
end
