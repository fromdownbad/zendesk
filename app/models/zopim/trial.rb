module Zopim
  class Trial
    DEFAULT_TRIAL_DURATION = 30.days
    ALTERNATIVE_TRIAL_DURATION = 14.days # 14-day trial test
    PHASE = 3
    MULTIPRODUCT_PHASE = 4

    class << self
      def chat_product_payload(account, suite: false, max_agents:)
        product_params = {
          state: Zendesk::Accounts::Product::TRIAL,
          trial_expires_at: calculate_trial_expires_at(account).iso8601,
          plan_settings: {
            plan_type: ZBC::Zopim::PlanType::Trial.plan_type,
            phase: phase(account),
            max_agents: max_agents,
            suite: suite,
            agent_workspace: suite
          }
        }
        { product: product_params }
      end

      private

      def phase(account)
        account.multiproduct? && account.has_ocp_support_shell_account_creation? ? MULTIPRODUCT_PHASE : PHASE
      end

      def calculate_trial_expires_at(account)
        if account.trial_extras_alternative_length?
          DateTime.now + ALTERNATIVE_TRIAL_DURATION
        else
          DateTime.now + DEFAULT_TRIAL_DURATION
        end
      end
    end
  end
end
