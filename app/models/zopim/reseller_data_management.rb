module Zopim
  module ResellerDataManagement
    extend ActiveSupport::Concern

    included do
      extend ClassMethods
      after_save :unset_reseller_data
    end

    private

    def unset_reseller_data
      instance_variables.
        select { |v| v.to_s.end_with?('_reseller_data') }.
        each   { |v| instance_variable_set(v, nil) }
    end

    module ClassMethods
      def reseller_data_reader(endpoint, params:)
        reader = "#{endpoint}_reseller_data".sub(/\!/, '').to_sym
        ref    = :"@__#{reader}"

        define_method reader do
          value = instance_variable_get(ref)
          return value unless value.nil?

          value = Reseller.client.send(endpoint,
            Hash[params.map { |k, v| [k, send(v)] }])
          instance_variable_set(ref, value)
        end
      end
    end
  end
end
