module Zopim
  class Agent < ActiveRecord::Base
    include Zopim::ResellerDataManagement
    include Zopim::Agents::Creation
    include Zopim::Agents::Ownership
    include Zopim::Agents::PhaseThree

    self.table_name = 'zopim_agents'

    has_kasket_on :user_id, :zopim_subscription_id

    include CIA::Auditable
    audit_attribute :zopim_subscription_id,
      :user_id,
      :zopim_agent_id,
      :email,
      :is_owner,
      :is_enabled,
      :is_administrator,
      :display_name

    SYNCHRONIZABLE_ATTRIBUTES = %w[
      email
      is_enabled
      is_administrator
      display_name
    ].freeze

    attr_accessible :email, :display_name, :is_enabled

    belongs_to :zopim_subscription, class_name: '::ZendeskBillingCore::Zopim::Subscription', inherit: :account_id
    belongs_to :account,            class_name: '::Account'
    belongs_to :user,               class_name: '::User', inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

    validate :check_permissions,
      :agent_limit_when_enabling,
      :trial_agent_limit_when_enabling,
      :ensure_not_zopim_owner_when_disabling,
      on: :update

    before_update :update_zopim_subscription_email,
      if: ->(record) { record.email_changed? && record.is_owner? }

    after_update :synchronize!, if: :synchronize?
    after_update :remove_chat_permission_set_id, if: :disabling_agent?

    before_destroy :ensure_not_zopim_owner
    before_destroy :deactivate_zopim_agent_record,
      if: proc { user.is_active? }
    after_destroy  :remove_chat_permission_set_id

    # Why is this also here? When deleting users this is called in a before_validations callback.
    # If user deletion fails/rollback occurs, we don't want to make the request to django.
    # See zendesk/zendesk/pull/27903 for more context
    after_commit :deactivate_zopim_agent_record,
      if: proc { !user.is_active? }

    validates :zopim_subscription_id, presence: true
    validates :account_id,            presence: true
    validates :user_id,               presence: true, uniqueness: true

    validates :zopim_agent_id, presence: true, uniqueness: true, on: :update
    validates :email,          presence: true, uniqueness: true
    validates :display_name,   presence: true

    delegate :last_name, :first_name,
      to: :user

    delegate :zopim_account_id,
      to: :zopim_subscription

    scope :administrators, -> { where(is_administrator: true) }
    scope :enabled, -> { where(is_enabled: true) }

    reseller_data_reader :account_agent!,
      params: {
        id: :zopim_account_id,
        agent_id: :zopim_agent_id
      }
    alias_method :reseller_data, :account_agent_reseller_data

    def self.destroy_agents(zopim_agents)
      zopim_agents.each do |agent|
        if agent.is_owner?
          AgentDeactivator.new(agent).deactivate!
          agent.delete
        else
          agent.destroy
        end
      end
    end

    def self.unserviceable_agent
      new.tap do |instance|
        def instance.is_serviceable? # rubocop:disable Naming/PredicateName
          false
        end
      end
    end

    def is_serviceable? # rubocop:disable Naming/PredicateName
      is_enabled? && is_linked?
    end

    def is_linked? # rubocop:disable Naming/PredicateName
      zopim_agent_id.present?
    end

    def will_be_saved_by(user)
      @current_user = user
    end

    def disable!
      update_attributes(is_enabled: false) unless destroyed?
    end

    def enable!
      update_attributes(is_enabled: true)
    end

    def phase_three?
      account.zopim_integration.try(:phase_three?)
    end

    private

    def update_zopim_subscription_email
      return if zopim_subscription.owner_email == email
      zopim_subscription.update_attribute(:owner_email, email)
    end

    def synchronize!
      Zopim::AgentSyncJob.work(account_id, user_id, payload)
    end

    def synchronize?
      (changes.keys & SYNCHRONIZABLE_ATTRIBUTES).any? && !phase_three?
    end

    def new_owner_agent?
      new_record? && is_owner?
    end

    def ensure_not_zopim_owner
      return if phase_three?
      return unless user.zopim_identity.try(:is_owner?)
      errors.add(:base, 'CannotDisableZopimOwner')
      errors.empty?
    end

    def ensure_not_zopim_owner_when_disabling
      return unless disabling_agent?
      ensure_not_zopim_owner
    end

    def deactivate_zopim_agent_record
      return if errors.any?
      AgentDeactivator.new(self).deactivate!
    end

    def permitted?
      return true if @current_user.nil?
      if changing_only_display_name?
        @current_user.can?(:edit_chat_display_name, user)
      else
        @current_user.can?(:edit_zopim_identity, user)
      end
    end

    def check_permissions
      return if permitted?
      errors.add(:base, "PermissionRequired")
      errors.empty?
    end

    def changing_only_display_name?
      (changes.keys - ["created_at", "updated_at"]) == ["display_name"]
    end

    # Creates a configuration to update an existing Zopim agent record via the
    # reseller API; this is used as the payload when sending an update agent
    # request to the reseller API.
    def reseller_update_payload
      {}.tap do |payload|
        payload[:display_name]  = display_name      if display_name_changed?
        payload[:administrator] = is_administrator? if is_administrator_changed?
        payload[:enabled]       = is_enabled?       if is_enabled_changed?
        payload[:email]         = email             if email_changed?
      end
    end
    alias_method :payload, :reseller_update_payload

    def disabling_agent?
      return false unless changes.key?("is_enabled")
      changes["is_enabled"].first
    end

    def remove_chat_permission_set_id
      user.remove_chat_permission_set_id
    end

    def agent_limit_when_enabling
      return unless changes.key?(:is_enabled)
      return if zopim_subscription.try(:is_trial?)
      return if disabling_agent?
      return if active_zopim_agents_count < zopim_subscription.try(:max_agents).to_i
      errors.add(
        :base,
        Api::Presentation::Error.new(
          'TooManyAgents',
          error: Users::Roles::MAX_AGENT_EXCEEDED,
          raw: true
        )
      )
    end

    def trial_agent_limit_when_enabling
      return unless changes.key?(:is_enabled)
      return unless zopim_subscription.try(:is_trial?)
      return if disabling_agent?
      return unless active_zopim_agents_count >= ZendeskBillingCore::Zopim::Subscription::TrialManagement::TRIAL_AGENTS_LIMIT
      errors.add(:base, 'TrialAgentLimitReached')
    end
  end
end
