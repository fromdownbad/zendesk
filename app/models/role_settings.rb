require 'set'

class RoleSettings < ActiveRecord::Base
  include CIA::Auditable
  include PrecreatedAccountConversion
  include CachingObserver

  ROLES    = Set[:agent, :end_user].freeze
  SERVICES = Set[:zendesk, :google, :office_365, :twitter, :facebook, :remote].freeze

  REMOTE_BYPASS_DISABLED = 0
  REMOTE_BYPASS_ACCOUNT_OWNER = 1
  REMOTE_BYPASS_ADMINS = 2

  attr_accessible :agent_security_policy_id,
    :agent_zendesk_login,
    :agent_password_allowed,
    :agent_google_login,
    :agent_office_365_login,
    :agent_twitter_login,
    :agent_facebook_login,
    :agent_remote_login,
    :agent_remote_bypass,
    :end_user_security_policy_id,
    :end_user_zendesk_login,
    :end_user_google_login,
    :end_user_password_allowed,
    :end_user_office_365_login,
    :end_user_twitter_login,
    :end_user_facebook_login,
    :end_user_remote_login

  audit_attribute *accessible_attributes

  not_sharded
  disable_global_uid

  belongs_to :account

  validates_inclusion_of :agent_security_policy_id,
    in: Zendesk::SecurityPolicy.all.map(&:id)
  validates_inclusion_of :end_user_security_policy_id,
    in: Zendesk::SecurityPolicy.all.map(&:id)
  validates_inclusion_of :agent_remote_bypass,
    in: [REMOTE_BYPASS_DISABLED, REMOTE_BYPASS_ACCOUNT_OWNER, REMOTE_BYPASS_ADMINS]

  before_validation :ensure_passwords_allowed
  after_update -> { account.update_has_google_apps if account }, if: :agent_google_login_changed?

  def disable_agent_social_login!
    update_attributes!(
      agent_twitter_login: false,
      agent_facebook_login: false
    )
  end

  def security_policy_id_for_role(role)
    send("#{role}_security_policy_id")
  end

  def login_allowed_for_role?(service, role)
    send("#{role}_#{service}_login")
  end

  def login_allowed_for_any_role?(service)
    ROLES.any? { |r| login_allowed_for_role?(service, r) }
  end

  def no_login_service_allowed_for_role?(role)
    SERVICES.none? { |s| login_allowed_for_role?(s, role) }
  end

  def login_allowed_only_for_role?(service, role)
    ROLES.one? { |r| login_allowed_for_role?(service, r) } &&
      login_allowed_for_role?(service, role)
  end

  def only_login_service_allowed_for_role?(service, role)
    SERVICES.one? { |s| login_allowed_for_role?(s, role) } &&
      login_allowed_for_role?(service, role)
  end

  def only_login_service_allowed_for_user?(service, user)
    role = user.is_end_user? ? :end_user : :agent
    only_login_service_allowed_for_role?(service, role)
  end

  def role_specific_logins?
    SERVICES.any? { |s| login_allowed_for_role?(s, :agent) != login_allowed_for_role?(s, :end_user) }
  end

  def legacy_login?
    !agent_remote_login &&
      (
        agent_twitter_login ||
        agent_facebook_login
      )
  end

  def agent_remote_bypass_allowed?
    !agent_zendesk_login && agent_remote_bypass != REMOTE_BYPASS_DISABLED
  end

  def ensure_passwords_allowed
    if agent_zendesk_login && !agent_password_allowed
      Rails.logger.info "enabling agent_password_allowed since agent_zendesk_login is: #{agent_zendesk_login}"
      self.agent_password_allowed = true
    end

    if end_user_zendesk_login && !end_user_password_allowed
      Rails.logger.info "enabling end_user_password_allowed since end_user_zendesk_login is: #{end_user_zendesk_login}"
      self.end_user_password_allowed = true
    end
  end

  def sso_bypass_allowed_for_user?(user)
    return false if agent_zendesk_login

    case agent_remote_bypass
    when REMOTE_BYPASS_DISABLED then false
    when REMOTE_BYPASS_ACCOUNT_OWNER then user.is_account_owner?
    when REMOTE_BYPASS_ADMINS then user.global_admin_privileges?
    end
  end
end
