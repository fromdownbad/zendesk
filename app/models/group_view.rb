class GroupView < ActiveRecord::Base
  self.table_name = :groups_views

  belongs_to :group, inherit: :account_id
  belongs_to :view, inherit: :account_id

  attr_accessible
end
