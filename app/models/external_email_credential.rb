require 'zendesk/models/external_email_credential'
require 'zendesk/encryptable'

# is also read/written from zendesk_mail_fetcher project!
class ExternalEmailCredential < ActiveRecord::Base
  include Zendesk::Encryptable

  ENCRYPTION_KEY_NAME = ENV.fetch('EXTERNAL_EMAIL_CREDENTIALS_ENCRYPTION_KEY')

  attr_accessor :current_user, :brand_id

  validates_presence_of :current_user, on: :create
  before_validation :ensure_recipient_address, on: :create
  before_validation :validate_username, on: :update
  before_update :reset_error_count, if: :login_related_changes?
  before_save :set_credential_updated_at, if: :login_related_changes?, on: [:create, :update]
  after_commit :send_test_email, unless: :initial_import?, on: :create

  has_one :recipient_address, autosave: true, dependent: :destroy, inverse_of: :external_email_credential

  attr_accessible :username,
    :account,
    :encrypted_value,
    :last_error_message,
    :last_error_at,
    :error_count,
    :initial_import,
    :current_user

  attr_encrypted :encrypted_value,
    encryption_key_name: ENCRYPTION_KEY_NAME,
    encryption_cipher_name: 'aes-256-gcm'

  before_save :encrypt!

  DEFAULT_INITIAL_IMPORT = true

  def self.redirect_uri
    "https://support.#{Zendesk::Configuration['host']}/ping/redirect_to_account"
  end

  # if we have hosts etc this should show host + username
  def email
    username
  end

  def need_repair?
    error_count > 6
  end

  def destroy
    begin
      RevokeExternalEmailCredentialJob.work(encrypted_value, encryption_key_name, encryption_cipher_name) if encrypted_value
    rescue StandardError => exception
      Rails.logger.error(exception)
      return false
    end

    mark_for_destruction
    super
  end

  def active?
    last_fetched_at && last_fetched_at > 1.day.ago
  end

  def decrypted_refresh_token
    decrypted_value_for(:encrypted_value)
  end

  private

  def send_test_email
    AccountsMailer.deliver_external_email_credential_test(account, current_user, self)
  end

  def ensure_recipient_address
    return unless account
    self.recipient_address = account.recipient_addresses.where(email: email).first_or_initialize do |address|
      address.name = account.name
      address.forwarding_verified_at = RecipientAddresses::ForwardingStatus::VERIFIED_BY_DEFAULT
      address.brand_id = brand_id if brand_id
    end
  end

  def reset_error_count
    self.error_count = 0
  end

  def set_credential_updated_at
    self.credential_updated_at = Time.now
  end

  def login_related_changes?
    (changed & ["encrypted_value"]).any?
  end

  def validate_username
    !username_changed? || username.try(:strip).try(:downcase) == recipient_address.email
  end
end
