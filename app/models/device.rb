require 'zendesk_oauth'
require 'useragent'
class Device < ActiveRecord::Base
  belongs_to :account
  belongs_to :user, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

  attr_accessible :account, :ip, :user_agent, :name, :token

  truncate_attributes_to_limit :user_agent
  validates_presence_of :account_id, :user, :token
  validates_uniqueness_of :token, scope: :user_id

  before_save :parse_name
  before_create :update_activity
  before_validation :generate_token, :geolocate, on: :create
  before_validation :scrub_user_agent

  ACTIVITY_THRESHOLD = 1.hour

  def register_activity!
    unless throttled?
      geolocate
      update_activity!
    end
  end

  def mobile?
    false
  end

  protected

  def scrub_user_agent
    self.user_agent = user_agent.try(:to_utf8)
  end

  def parse_name
    return unless name.blank?

    agent = UserAgent.parse(user_agent)
    self.name = "#{agent.platform}, #{agent.browser}" if name.blank?
  end

  private

  def geolocate
    if force_local_machine?
      self.location = 'Local Machine'
      return
    end

    geo = Zendesk::GeoLocation.locate(ip)
    return unless geo.present?

    self.location = geo.values_at(:city, :country).compact.join(', ')
  end

  def update_activity
    self.last_active_at = Time.now
  end

  def update_activity!
    update_activity
    save
  end

  def generate_token
    self.token ||= Token.generate(32, false)
  end

  def throttled?
    return false if last_active_at.nil?
    last_active_at >= ACTIVITY_THRESHOLD.ago.utc
  end

  def force_local_machine?
    Rails.env.development?
  end
end
