require 'friendly_id'

class Forum < ActiveRecord::Base
  include ScopedCacheKeys
  include Organizations::AccessByName
  include Subscribable
  include Zendesk::Serialization::ForumSerialization
  include TagManagement
  include CurrentTagsAccessor
  include SlugIds
  include CachingObserver
  include ContentMappingSupport
  extend ActiveHash::Associations::ActiveRecordExtensions

  ENTRY_ORDERING = {
    highlight_latest: 'entries.is_highlighted DESC, entries.updated_at DESC',
    highlight_submit_first: 'entries.is_highlighted DESC, entries.created_at DESC, entries.id DESC',
    highlight_submit_last: 'entries.is_highlighted DESC, entries.created_at ASC, entries.id ASC',
    highlight_alphabetical: 'entries.is_highlighted DESC, entries.title ASC',
    latest_activity: 'entries.updated_at DESC',
    submit_first: 'entries.created_at DESC, entries.id DESC',
    submit_last: 'entries.created_at ASC, entries.id ASC',
    alphabetical: 'entries.title ASC',
    most_viewed: 'entries.hits DESC',
    most_comments: 'entries.posts_count DESC',
    manual: 'entries.position, entries.id ASC'
  }.freeze

  DEFAULT_SORT            = ENTRY_ORDERING[:highlight_latest]
  DEFAULT_SORT_FOR_VOTING = 'entries.votes_count DESC, entries.posts_count DESC'.freeze

  SORT_OPTIONS = [
    ['Highlighted first, then latest activity', ENTRY_ORDERING[:highlight_latest]],
    ['Highlighted first, then submit date (latest first)', ENTRY_ORDERING[:highlight_submit_first]],
    ['Highlighted first, then submit date (latest last)', ENTRY_ORDERING[:highlight_submit_last]],
    ['Highlighted first, then alphabetically', ENTRY_ORDERING[:highlight_alphabetical]],
    ['Latest activity', ENTRY_ORDERING[:latest_activity]],
    ['Submit date, latest first', ENTRY_ORDERING[:submit_first]],
    ['Submit date, latest last', ENTRY_ORDERING[:submit_last]],
    ['Alphabetically by title', ENTRY_ORDERING[:alphabetical]],
    ['Most viewed', ENTRY_ORDERING[:most_viewed]],
    ['Most comments', ENTRY_ORDERING[:most_comments]],
    ['Let me set the order manually (order topics via drag-and-drop in the forum)', ENTRY_ORDERING[:manual]]
  ].freeze

  TYPE_OPTIONS = [
    ['Articles - users can vote for the most helpful articles', 1],
    ['Ideas - users vote for the best ideas, and you can mark ideas as planned or done', 2],
    ['Questions - users vote for questions, and you can mark the answer', 3],
  ].freeze

  has_soft_deletion default_scope: true

  before_soft_delete :unsuspend_entries

  before_validation :force_forum_type
  before_create     :set_defaults_before_create
  before_save       :ensure_valid_sorting_before_save
  after_update      :propagate_access_settings_after_update, :reset_flags_on_display_type_change

  belongs_to :account
  belongs_to :category, inherit: :account_id
  belongs_to_active_hash :display_type, class_name: "ForumDisplayType"
  belongs_to :organization, inherit: :account_id
  belongs_to_active_hash :visibility_restriction, class_name: "VisibilityRestriction"
  belongs_to :translation_locale, inherit: :account_id

  attr_accessible :account, :organization, :organization_name, :organization_id,
    :category, :display_type, :visibility_restriction, :translation_locale, :name,
    :description, :is_locked, :locked, :position, :sorting, :use_for_suggestions,
    :display_type_id, :visibility_restriction_id, :translation_locale_id,
    :category_id, :access, :forum_type, :set_tags

  has_many :entries, -> { order(DEFAULT_SORT) }, dependent: :destroy, inherit: :account_id
  has_many :unanswered_questions, -> { where.not(flag_type_id: EntryFlagType::ANSWERED.id) }, class_name: 'Entry', inherit: :account_id
  has_many :posts, inherit: :account_id

  scope :is_public, -> { where(visibility_restriction_id: VisibilityRestriction::EVERYBODY.id) }
  scope :accessible_to, ->(user) { where(user.entry_conditions('forums')) }
  scope :with_entries, -> { where("entries_count > 0") }

  validates_presence_of :name
  validates_exclusion_of :visibility_restriction_id, in: [VisibilityRestriction::AGENTS_ONLY.id],
                                                     if: proc { |record| record.translation_locale.present? }
  def url
    "#{account.url}/forums/#{to_param}"
  end

  def title
    name
  end

  def sorting
    return DEFAULT_SORT_FOR_VOTING if display_type_id != ForumDisplayType::ARTICLES.id
    read_attribute(:sorting)
  end

  def to_param
    slug_id(self)
  end

  def self.sort_options_for(account)
    options = SORT_OPTIONS
    options = [['Most helpful first (as voted by users)', DEFAULT_SORT_FOR_VOTING]] + options if account.subscription.has_forums2?
    options
  end

  def is_public # rubocop:disable Naming/PredicateName
    !visibility_restriction.agents_only?
  end

  # A sorting is legal if it's present in the sort options
  def legal_sorting?(sorting)
    Forum.sort_options_for(account).map { |pair| pair.last.downcase }.member?(sorting.to_s.downcase)
  end

  def articles?
    display_type_id == 1
  end

  def accessible?
    account.subscription.has_forums2? && account.subscription.is_trial? && articles?
  end

  # Below here some attribute mappings introduced by API2

  alias_attribute :locked, :is_locked

  def forum_type
    display_type.name.downcase
  end

  def forum_type=(value)
    if display_type = ForumDisplayType.find_by_name(value.downcase.capitalize)
      self.display_type_id = display_type.id
    end
  end

  def access
    visibility_restriction.label.downcase
  end

  def access=(value)
    if restriction = VisibilityRestriction.find_by_label(value.downcase.capitalize)
      self.visibility_restriction_id = restriction.id
    end
  end

  def tag_array
    return [] unless account.try(:has_user_and_organization_tags?)

    super
  end

  def matches_user_tags?(user)
    (tag_array & user.all_tags).size == tag_array.size
  end

  private #########################################################

  def unsuspend_entries
    SuspendedEntry.for_forum(self).update_all(Entry.mark_as_unsuspended_sql)
  end

  def set_defaults_before_create
    self.position ||= 9999
  end

  def force_forum_type
    self.display_type_id = ForumDisplayType::ARTICLES.id if account && !account.subscription.has_community_forums?
  end

  # Ensures that the forum has a legal sorting
  def ensure_valid_sorting_before_save
    self.sorting = DEFAULT_SORT unless legal_sorting?(read_attribute(:sorting))
    true
  end

  # Update organization and is_public for all entries in forum to reflect those of the parent forum
  def propagate_access_settings_after_update
    if visibility_restriction_id_changed? || organization_id_changed?
      Entry.where(forum_id: id).update_all_with_updated_at(is_public: is_public, organization_id: organization_id)
      Attachment.where(source_type: 'Entry', source_id: entries.map(&:id)).update_all(is_public: is_public)
    end
    true
  end

  def reset_flags_on_display_type_change
    if display_type_id_changed? && display_type_id_was != ForumDisplayType::ARTICLES.id
      Entry.where(forum_id: id).update_all(flag_type_id: EntryFlagType::UNKNOWN.id)
      if display_type_id_was == ForumDisplayType::QUESTIONS.id
        Post.where(forum_id: id).update_all(is_informative: false)
      end
    end
  end
end
