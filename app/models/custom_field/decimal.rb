module CustomField
  class Decimal < Field
    CUSTOM_DECIMAL_REGEXP = /^[-0]?[0-9]{0,#{Zendesk::CustomField::NumberPadding::LEFT_SPAN}}[.,]?[0-9]*$/

    def value_as_json(val)
      is_blank?(val) ? nil : val.to_f
    end

    def cast_value_for_db(val)
      return nil if is_blank?(val)
      val = Float(val) rescue false
      Zendesk::CustomField::NumberPadding.pad(val) if val
    end

    def validate_field_value_object(obj)
      return if obj.nil?

      val = obj.value
      case val
      when ::Float, ::Integer
        nil
      when String
        unless CUSTOM_DECIMAL_REGEXP.match?(val)
          add_value_error(obj)
        end
      else
        add_value_error(obj)
      end
    end

    def cast_for_compare(val)
      return nil if is_blank?(val)
      return val if val.is_a?(::Float) || val.is_a?(::Integer)

      if val.is_a?(String) && (val =~ CUSTOM_DECIMAL_REGEXP)
        val.to_f
      end
    end
  end
end
