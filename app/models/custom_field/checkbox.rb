module CustomField
  class Checkbox < Field
    include Zendesk::CustomField::Checkbox
    def validate_field_value_object(obj)
      return if obj.nil?

      val = obj.value
      case val
      when true, false
        nil
      when String
        unless (TRUTHY_VALUES + FALSY_VALUES).include?(val)
          add_value_error(obj)
        end
      end
    end

    def cast_for_compare(val)
      value_as_json(val)
    end

    def is_blank?(val) # rubocop:disable Naming/PredicateName
      # NOTE: checkbox considers false and "0" to be blank
      val.blank? || FALSY_VALUES.include?(val)
    end

    def value_as_json(val)
      TRUTHY_VALUES.include?(val) ? true : false
    end

    def cast_value_for_db(val)
      TRUTHY_VALUES.include?(val) ? "1" : nil # "0"
    end

    def validate_tag_is_valid
      return unless tag && self.class.validate_tag_is_valid(errors, tag)

      Zendesk::CustomField::DropdownTagger.validate_unique(errors, self, [tag])
    end

    def get_tag(_field_value)
      tag
    end

    def two_state?
      true
    end
  end
end
