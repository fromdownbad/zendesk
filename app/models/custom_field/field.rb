module CustomField
  class Field < ActiveRecord::Base
    self.store_full_sti_class = false
    I18N_TITLE_PATTERN = /\Atxt\..*\Z/
    include Zendesk::CustomField::Field
    include CachingObserver
    include Zendesk::FieldCompare::Base
    include Zendesk::PreventCollectionModificationMixin

    attr_accessible :field, :owner, :account, :key, :description, :title, :title_in_portal, :position,
      :is_active, :is_visible_in_portal, :is_editable_in_portal, :regexp_for_validation, :is_exportable, :tag, :is_system

    scope :for_user, -> { where(owner: "User") }
    scope :for_organization, -> { where(owner: "Organization") }
    scope :for_system, -> { where(is_system: true) }
    scope :for_owner, ->(owner) { where(owner: owner) }
    scope :dropdowns, -> { where(type: "Dropdown") }
    scope :checkboxes, -> { where(type: "Checkbox") }
    scope :api_ordering, -> { order(is_active: :desc, position: :asc) }

    self.table_name = 'cf_fields'

    has_many :dropdown_choices,
      -> { order(:position, :id) },
      class_name: "CustomField::DropdownChoice",
      dependent: :delete_all, autosave: true, inherit: :account_id, foreign_key: :cf_field_id

    alias :custom_field_options :dropdown_choices
    attr_accessor :current_user

    validates_presence_of :account_id, :title, :key
    validates_uniqueness_of :key, scope: [:account_id, :owner]
    validate :validate_owner_is_valid
    validate :validate_key_is_valid, unless: :is_system
    validate :validate_system_field_key, if: :is_system
    validate :validate_system_field_title, if: :is_system
    validate :validate_tag_is_valid
    validate :validate_type
    validate :validate_key_not_changed, on: :update
    validate :validate_type_not_changed, on: :update
    validates_lengths_from_database only: [:title, :description]
    before_destroy :validate_not_used

    has_many :values, class_name: "CustomField::Value", dependent: :delete_all, foreign_key: :cf_field_id, inherit: :account_id

    has_one :resource_collection_resource, as: :resource

    after_validation :set_field_defaults

    def rule_value(val)
      cast_value_for_db(value_as_json(val))
    end

    def value_as_json(val)
      is_blank?(val) ? nil : val
    end

    def cast_value_for_db(val)
      !val.is_a?(String) || is_blank?(val) ? nil : val.to_s
    end

    def validate_field_value_object(val)
    end

    def get_tag(_field_value)
      nil
    end

    def self.validate_tag_is_valid(errors, tag)
      error = tag && Tagging.get_tag_error(tag)
      errors.add(:base, error) if error
      !error
    end

    def liquid_name(field_value)
      field_value.blank? ? value_as_json(nil) : field_value.as_json["value"]
    end

    def liquid_title(field_value)
      field_value.blank? ? value_as_json(nil) : field_value.as_json["value"]
    end

    def to_liquid(field_value)
      liquid_title(field_value)
    end

    MATCHERS = {
      ticket: ['ticket_fields_', ''],
      user: ['requester.custom_fields.'],
      organization: ['organization.custom_fields.', 'requester.organization.custom_fields.']
    }.freeze

    def self.validate_field_not_used(account, type, object)
      type, key =
        case type
        when "Ticket"
          [:ticket, object.id]
        when "User"
          [:user, object.key]
        when "Organization"
          [:organization, object.key]
        else
          raise "Unsupported type #{type}"
        end

      sources = MATCHERS.fetch(type)
      condition = sources.map { |s| "definition like '%source: #{s}#{key}%'" }.join(" OR ")
      return unless rules = account.rules.where(condition).limit(10).presence
      rule_urls = rules.map(&:lotus_permalink).join(", ")
      object.errors.add(:base, I18n.t('txt.admin.models.ticket_field.field_in_use_by_rule', rules: rule_urls))
      false
    end

    def self.get_all_tags(skip_field)
      id_condition = skip_field.id.nil? ? "" : "&& id != #{skip_field.id}"
      fields = skip_field.account.custom_fields.where("owner = '#{skip_field.owner}' #{id_condition}").to_a
      all_tags = Set.new(CustomField::DropdownChoice.where(cf_field_id: fields.map(&:id)).pluck(:value))
      all_tags.merge(fields.map(&:tag).compact)
    end

    private

    def validate_not_used
      CustomField::Field.validate_field_not_used(account, owner, self)
    end

    def validate_owner_is_valid
      unless ['User', 'Organization'].include?(owner)
        errors.add(:owner, :invalid)
      end
    end

    def validate_key_is_valid
      # use Ruby variable's validation Regex
      return unless key.present?
      unless /^[a-zA-Z_0-9]*$/.match?(key)
        errors.add(:key, :invalid)
      end
    end

    def validate_system_field_key
      unless /\Asystem::[a-z_][a-zA-Z_0-9]*\Z/.match?(key)
        errors.add(:key, "System fields must be prefixed with 'system::' and can contain only underscore or alphanumerics")
      end
    end

    def validate_system_field_title
      unless I18N_TITLE_PATTERN.match?(title)
        errors.add(:title, "System fields title must be i18n keys as they will be translated in UI wherever they are shown.")
      end
    end

    # only checkbox fields get tag metadata, so default to invalid
    def validate_tag_is_valid
      errors.add(:tag, :invalid) if tag
    end

    def validate_type
      return errors.add(:type, :invalid) unless type.is_a?(String)
      begin
        CustomField.const_get(type)
      rescue NameError
        return errors.add(:type, :invalid)
      end
    end

    def validate_key_not_changed
      if key_changed?
        errors.add(:key, :may_not_be_updated)
      end
    end

    def validate_type_not_changed
      if type_changed?
        errors.add(:type, :may_not_be_updated)
      end
    end

    def add_value_error(obj)
      obj.errors.add(:base, I18n.t("activerecord.errors.models.custom_field.value.invalid_" + type.downcase, key: title))
    end

    # RAILS5UPGRADE: Remove after upgrading to Rails 5.
    # In Rails 4, determining the type is slow when store_full_sti_class is false.
    # https://github.com/rails/rails/pull/27537
    if RAILS4
      def self.field_types
        @@field_types ||= {
          "Checkbox" => CustomField::Checkbox,
          "Date" => CustomField::Date,
          "Decimal" => CustomField::Decimal,
          "Integer" => CustomField::Integer,
          "Regexp" => CustomField::Regexp,
          "Text" => CustomField::Text,
          "Textarea" => CustomField::Textarea,
          "Dropdown" => CustomField::Dropdown
        }.freeze
      end

      def self.find_sti_class(type_name)
        field_types[type_name] || raise(SubclassNotFound, "Invalid custom field type #{type_name}")
      end
    end
  end
end
