module CustomField
  class Integer < Field
    INTEGER_REGEXP = /^[-0]?\d{1,#{Zendesk::CustomField::NumberPadding::LEFT_SPAN}}$/

    def validate_field_value_object(obj)
      return if obj.nil?

      val = obj.value
      case val
      when ::Integer
        # valid
        nil
      when String
        unless val&.match?(num_regexp)
          add_value_error(obj)
        end
      else
        add_value_error(obj)
      end
    end

    def cast_for_compare(val)
      return nil if is_blank?(val)
      return val if val.is_a?(::Integer)
      return val.to_i if val.is_a?(Float)

      if val.is_a?(String) && (val =~ num_regexp)
        val.to_i
      end
    end

    def value_as_json(val)
      is_blank?(val) ? nil : val.to_i
    end

    def cast_value_for_db(val)
      return nil if is_blank?(val)
      val = if val.is_a?(String)
        Integer(val, 10) rescue false
      else
        Integer(val) rescue false
      end
      Zendesk::CustomField::NumberPadding.pad(val) if val
    end

    private

    def num_regexp
      if regexp_for_validation.present?
        ::Regexp.compile(regexp_for_validation)
      else
        INTEGER_REGEXP
      end
    end
  end
end
