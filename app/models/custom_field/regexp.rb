require 'safe_regexp'

module CustomField
  class Regexp < Field
    include Zendesk::CustomField::Regexp
    include Zendesk::SafeRegexpReporting

    validate :validate_regex

    def validate_field_value_object(obj)
      return if obj.nil?

      val = obj.value
      case val
      when String
        regexp = ::Regexp.new(regexp_for_validation)
        if account.has_use_safe_regexp?
          begin
            add_value_error(obj) unless SafeRegexp.execute(regexp, :match?, val)
            safe_regexp_success
          rescue SafeRegexp::RegexpTimeout => e
            safe_regexp_error(e, account_id, id)
            obj.errors.add(:base, I18n.t('txt.api.v2.custom_fields.slow_regex_field_matches', field_id: id))
          rescue Errno::ENOMEM => e
            safe_regexp_error(e, account_id, id)
            add_value_error(obj) unless val&.match?(regexp)
          end
        else
          unless val&.match?(regexp)
            add_value_error(obj)
          end
        end
      else
        add_value_error(obj)
      end
    end
  end
end
