module CustomField
  class Dropdown < Field
    include Zendesk::CustomField::DropdownTagger

    attr_accessor :sort_alphabetically

    validate :validate_custom_field_options_are_not_empty
    validate :validate_custom_field_options

    after_save :remove_deleted

    def value_is_id_string?(val)
      val.to_i.to_s == val
    end

    def validate_field_value_object(obj)
      return if obj.nil?
      choice = find_choice_by_value(obj.value)
      add_value_error(obj) unless choice
    end

    def cast_for_compare(val)
      return nil if is_blank?(val)
      choice = find_choice_by_value(val)
      choice ? choice.id.to_s : nil
    end

    def value_as_json(val)
      return nil if is_blank?(val)
      get_choice(val).try(:value)
    end

    def cast_value_for_db(val)
      choice = find_choice_by_value(val)
      choice ? choice.id.to_s : nil
    end

    def get_tag(field_value)
      return nil unless field_value
      field = field_value.field
      field.value_as_json(field.cast_value_for_db(field_value.value))
    end

    def to_liquid(field_value)
      Zendesk::Liquid::Wrapper.new("name").tap do |wrapper|
        wrapper["name"]  = liquid_name(field_value).to_s # Needs to be a string, as it's returned by the Wrapper to_s method
        wrapper["title"] = liquid_title(field_value)
      end
    end

    # Check CustomField::Field#to_liquid
    def liquid_title(field_value)
      get_name(field_value.try(:value))
    end

    protected

    def custom_field_option_class
      CustomField::DropdownChoice
    end

    private

    def get_choice(value)
      dropdown_choices.detect { |c| c.id == value.to_i } ||
        dropdown_choices.find_by_id(value.to_i)
    end

    def get_name(value)
      return nil if is_blank?(value)
      get_choice(value).try(:name)
    end

    def find_choice_by_value(val)
      return dropdown_choices.find_by_id(val) if val.is_a?(Integer) # should be string, but just in case
      return nil unless val.is_a?(String)

      if value_is_id_string?(val)
        # string representation of the id? maybe. could still be a numeric key though
        if (choice = dropdown_choices.find_by_id(val.to_i))
          return choice
        elsif (choice = dropdown_choices.find_by_value(val))
          return choice
        end
      elsif (choice = dropdown_choices.find_by_value(val))
        return choice
      end

      nil
    end

    def remove_deleted
      dropdown_choices.target.reject!(&:deleted?)
    end
  end
end
