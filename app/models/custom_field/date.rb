module CustomField
  class Date < Field
    include Zendesk::FieldCompare::Date

    def validate_field_value_object(obj)
      return if obj.nil?

      val = obj.value
      case val
      when Time
        # valid
        nil
      else
        ret = time_parse(val)
        add_value_error(obj) unless ret
      end
    end

    def rule_value(val)
      return val if (Integer(val) rescue false) # This allows integers eg. previous n days
      super
    end

    def value_as_json(val)
      return nil if is_blank?(val)
      Time.parse(val)
    end

    def cast_value_for_db(val)
      return nil if is_blank?(val)
      t = if val.is_a?(Time)
        val
      else
        time_parse(val) rescue false
      end

      t ? t.strftime("%Y-%m-%d") : t
    end
  end
end
