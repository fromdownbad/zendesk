module CustomField
  class Value < ActiveRecord::Base
    include CustomFieldValueObserver

    self.table_name = 'cf_values'
    CUSTOM_FIELD_VALUE_SIZE_LIMIT_IN_BYTES = ENV.fetch("CUSTOM_FIELD_VALUE_SIZE_LIMIT_IN_BYTES", 125000).to_i

    belongs_to :account
    belongs_to :field, class_name: "CustomField::Field", foreign_key: :cf_field_id, inherit: :account_id
    belongs_to :owner, polymorphic: true, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]

    attr_accessible :field, :value, :owner, :owner_type, :owner_id

    validates_presence_of :account_id, :owner, :field
    validate :validate_with_field
    validate :validate_bytesize_below_limit, if: :enforce_bytesize_limit?
    before_save :cast_value_for_db

    inherits_from :owner, attr: :account_id

    # convert field's value string to defined type
    def as_json
      h = super
      h['value'] = field.value_as_json(value)
      h
    end

    # cast input value to be proper type
    def cast_value_for_db
      self.value = field.cast_value_for_db(value)
    end

    def info_for_external_change_event
      { info: { cf_field_id: field.id, owner_id: owner_id } }
    end

    private

    def validate_with_field
      field.validate_field_value_object(self)
    end

    def validate_bytesize_below_limit
      return if value.bytesize < CUSTOM_FIELD_VALUE_SIZE_LIMIT_IN_BYTES

      statsd_client.increment('over_bytesize_limit', tags: ["account_id:#{account_id}", "limit:#{CUSTOM_FIELD_VALUE_SIZE_LIMIT_IN_BYTES / 1000}kb"])
      errors.add(:base, :over_bytesize_limit, key: field.title, limit: CUSTOM_FIELD_VALUE_SIZE_LIMIT_IN_BYTES / 1000)
    end

    def enforce_bytesize_limit?
      account.has_enforce_user_and_org_field_size_limit?
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['custom_field', 'value'])
    end
  end
end
