module CustomField
  class DropdownChoice < ActiveRecord::Base
    extend Zendesk::DB::BulkUidAllocation
    has_soft_deletion default_scope: true

    self.table_name = "cf_dropdown_choices"

    scope :active, -> { where(deleted_at: nil) }

    belongs_to :account

    # WARNING: Because we do not declare :touch here, you must use custom_field_manager
    # to update dropdown choices or the updated_at on the owner field will be stale.
    belongs_to :field, class_name: "CustomField::Dropdown", foreign_key: :cf_field_id, inherit: :account_id

    attr_accessible :account, :field, :position, :name, :value

    validate :validate_system_field_dropdown_choice_name, if: proc { |dc| dc.field && dc.field.is_system? }
    validates_presence_of :value

    def active?
      deleted_at.nil?
    end

    # NOTE: validate here before it gets to Dropdown validation?
    def validate_tag_is_valid
      Field.validate_tag_is_valid(errors, value)
    end

    private

    def validate_system_field_dropdown_choice_name
      unless CustomField::Field::I18N_TITLE_PATTERN.match?(name)
        errors.add(:title, "Names of dropdown choices of system fields must be i18n keys as they will be translated in UI wherever they are shown.")
      end
    end
  end
end
