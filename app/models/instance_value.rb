# InstanceValue readonly model
#
# `instance_values` is a sharded database table deco uses to record
# ticket and agent skills. Improvements to skill based routing in
# Support require that Occam and classic are both able to read from
# (but not write to) this table. Further context is in this classic
# ADR: https://github.com/zendesk/zendesk/pull/41654
class InstanceValue < ActiveRecord::Base
  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :ticket_skills, -> { not_deleted.where(type_id: 10) }
  scope :agent_skills, -> { not_deleted.where(type_id: 20) }
  scope :skills_for, ->(agent) { agent_skills.where(instance_id: agent&.id, account_id: agent&.account_id) }

  def readonly?
    true
  end

  def self.skill_ids_for_agent(agent)
    skills_for(agent).map { |skill| skill.attribute_value_id.unpack1('H*') }
  end
end
