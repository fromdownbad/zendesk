require 'ticket_sharing/comment'

class SharedComment < ActiveRecord::Base
  belongs_to :account
  belongs_to :comment

  attr_accessible :account, :comment, :uuid

  before_validation :set_account_from_comment
  before_validation :generate_uuid, on: :create

  validates_presence_of :account_id, :comment

  def for_partner
    return unless comment
    attachments = comment.attachments.map(&:for_partner)
    attachments = comment.shared_attachments if comment.via?(:facebook_message) && comment.is_a?(FacebookComment) && comment.data[:attachments]

    params = {
      'uuid' => uuid,
      'authored_at' => comment.created_at.in_time_zone,
      'body' => comment.body,
      'html_body' => comment.html_body,
      'public' => comment.is_public?,
      'author' => comment.for_partner,
      'attachments' => attachments
    }

    params.update('custom_fields' => custom_fields) if custom_fields.any?

    TicketSharing::Comment.new(params)
  end

  private

    # TSTODO do we want to include the ticket id in here?
  def generate_uuid
    return unless comment
    return if uuid.present?

    key = "#{account.sharing_url}/comments/#{comment.id}"
    self.uuid = Digest::SHA1.hexdigest(key)
  end

  def set_account_from_comment
    self.account ||= comment.try(:account)
  end

  def custom_fields
    return {} unless comment.screencasts.any?
    {
      'zendesk' => {
        'metadata' => {
          'screencasts' => comment.screencasts.map(&:for_partner),
        }
      }
    }
  end
end
