require 'zendesk/query/fields'

# The report class contains the field necessary to extract data for the given criteria. It
# serializes the user determined data into the definition field. The serialized data is an array of sets,
# each set constituting a series of parameters that result in a line in the chart (or a column in the data
# table for that matter). Thus a sample of serialized data looks like the below, which contains two data sets,
# one displaying all opened urgent tickets, and one displaying all closed urgent tickets:
#
# [
#   { :legend => 'Open tickets', :data => [[ 'status', 'is', 'open' ], [ 'priority' ,'is', 'urgent' ]] },
#   { :legend => 'Closed tickets' :data => [[ 'status', 'is', 'closed' ], [ 'priority' ,'is', 'urgent' ]] }
# ]
#
#
# http://www.xaprb.com/blog/2005/12/07/the-integers-table/
class Report < ActiveRecord::Base
  include Zendesk::Serialization::ReportSerialization
  include PrecreatedAccountConversion

  belongs_to :account
  belongs_to :author, class_name: "User", foreign_key: :author_id

  attr_accessible :title, :author, :relative_interval_in_days, :sets, :account, :is_relative_interval, :definition, :last_run_at, :result,
    :from_date, :"from_date(1i)", :"from_date(2i)", :"from_date(3i)",
    :to_date, :"to_date(1i)", :"to_date(2i)", :"to_date(3i)"

  serialize   :definition
  serialize   :result, Hash

  before_validation     :set_default_state
  validates_presence_of :title, :author
  validate              :validate_report_definition

  attr_accessor :data

  SUMMABLE_FIELDS = ['created_at', 'solved_at'].freeze
  COLOR_CYCLE     = %w[FF0D00 98C332 96DEED E8B200 727070 1F6B95 000000 FD7101].freeze

  def cycle_color
    @color_index ||= -1
    @color_index += 1
    COLOR_CYCLE[@color_index % COLOR_CYCLE.size]
  end

  # Returns a definition if a such exists, otherwise an empty array for use when creating a new report
  def sets
    definition || []
  end

  def sets=(arg)
    self.definition = parse_definition(arg)
    @definition_parse_failure = false
  rescue IllegalMappingException => e
    @definition_parse_failure = true
    Rails.logger.warn("Failed to parse report definition: #{arg.inspect}, #{e.message}, #{e.backtrace.join("\n")}")
  end

  def set_default_state
    sets.each do |set|
      set[:state] = 'created_at' unless ['created_at', 'solved_at', 'working', 'any_closed', 'any', 'net_change'].member?(set[:state])
    end
  end

  def validate_report_definition
    if @definition_parse_failed
      errors.add(:definition, :invalid)
      return
    end

    errors.add(:definition, I18n.t('txt.errors.report.definition_errors.must_contain_at_least_one_data_series')) if sets.empty?
    errors.add(:definition, I18n.t('txt.errors.report.definition_errors.must_have_a_legend_for_each_data_series')) unless sets.select { |r| r[:legend][:name].blank? }.empty?
    errors.add(:definition, I18n.t('txt.errors.report.definition_errors.must_have_a_maximum_of_8_data_series')) if sets.size > 8

    if is_relative_interval
      errors.add(:interval, I18n.t('txt.errors.report.interval_errors.can_span_a_maximum_of_3_months')) if relative_interval_in_days > 93.days
    else
      errors.add(:to_date, I18n.t('txt.errors.report.definition_errors.must_be_later_than_from_date')) if from_date >= to_date
      errors.add(:interval, I18n.t('txt.errors.report.interval_errors.can_span_a_maximum_of_3_months')) if (to_date - from_date).days > 93.days
    end
  end

  def table
    @data ||= execute
  end

  # Executes the report and returns a result set. Should be cached in the view (for a period of time..). The result
  # is on the format:
  #   { Date1 => { 'Legend 1' => DATA, 'Legend 2' => DATA }, Date2 => { 'Legend 1' => DATA ...}}
  #
  def execute(relaxed = false)
    return @data if @data
    Time.use_zone(account.time_zone) do
      @data = { legends: [], data: {} }
      date_range.each do |date|
        @data[:data][date] ||= {}
      end

      sets.each do |set|
        @data[:legends] << set[:legend].merge(summable: SUMMABLE_FIELDS.member?(set[:state]))

        if [nil, 'created_at', 'solved_at'].member?(set[:state])
          query = event_query(set[:data], set[:state])
        elsif set[:state] == 'working'
          query = working_query(set[:data])
        elsif set[:state] == 'any'
          query = any_query(set[:data])
        elsif set[:state] == 'any_closed'
          query = any_closed_query(set[:data])
        else
          raise "Invalid query state: #{set[:state]}"
        end

        begin
          records = ActiveRecord::Base.with_slave.connection.select_rows(query)
        rescue StandardError => e
          ZendeskExceptions::Logger.record(e, location: self, message: "Failed query: #{query}", fingerprint: 'f6df94b99c92056db65714cac5f825806acb45f6')
          raise e
        end

        records.each do |row|
          date  = row.first
          date  = Date.parse(date) if date.is_a?(String)
          count = row.last.to_i

          @data[:data][date][set[:legend][:name]] = count if @data[:data][date]
        end

        sleep(3) if relaxed
      end

      @data[:legends].sort! { |x, y| x[:name] <=> y[:name] }

      update_attributes!(result: @data, last_run_at: Time.now) unless new_record?
    end

    @data
  end

  def from
    if is_relative_interval?
      (relative_interval_in_days || 29).days.ago.beginning_of_day
    else
      Time.zone.parse(from_date.to_s).beginning_of_day
    end
  end

  def to
    if is_relative_interval?
      Time.now.in_time_zone.end_of_day # -> 23:59:59 to_d
    else
      Time.zone.parse(to_date.to_s).end_of_day
    end
  end

  def date_range
    (from.to_date..to.to_date).to_a # to.to_date inclusive
  end

  # The input submitted via the rule javascript framework is on the format:
  #
  # { sets => {
  #   ID => {
  #     legend => VALUE,
  #     conditions => {
  #       ID => { :source => VALUE, :operator => VALUE, :value => { VALUE_MAP } },
  #      }
  #   }
  # }}
  #
  # The ID designates an id which is used for ordering and such, but has not computational value.
  # There can be multiple ID entries on each level, ie. the sets hash contains one-to-many ID keys
  # each of which contain a conditions hash with one-to-many ID keys pointing to the selection
  # criteria. This method parses such a value into the definition format saved with a report record
  def parse_definition(sets)
    sets ||= {}
    parsed = []

    parsed.tap do
      sets.values.sort_by { |a| a[:legend] }.each do |set|
        data = { legend: { name: set[:legend], color: cycle_color }, data: [], state: set[:state] }

        unless set[:conditions].nil?
          set[:conditions].keys.sort.each do |key|
            source   = set[:conditions][key][:source]
            operator = set[:conditions][key][:operator]
            value    = set[:conditions][key][:value]

            next if source.blank? || operator.blank? || value.blank? || value.size != 1

            if operator.include? "business_hours"
              source = "#{source}_within_business_hours"
            end

            next unless field = Zendesk::Query::Fields.lookup(source, account)
            legal = field.legal_values(account)

            data[:data] << field.parse(operator, value.values.first, legal)
          end
        end

        parsed << data
      end
    end
  end

  def items
    items = []
    sets.each do |set|
      items += set[:data].map { |condition| DefinitionItem.new(*condition) }
    end
    items
  end

  def non_deleted_tickets_condition
    "tickets.status_id IN (#{(StatusType.fields - Zendesk::Types::StatusType::INACCESSIBLE_LIST).join(',')})"
  end

  # New Tickets and Resolved Tickets
  def event_query(conditions, state = "created_at")
    "SELECT #{converted_date(state)} AS stamped, COUNT(*) AS matches
       #{from_statement(conditions)} WHERE tickets.account_id = #{account.id}
         #{add_conditions(conditions)}
         AND tickets.#{state} >= CONVERT_TZ('#{from.to_date.to_s(:db)} 00:00:00', '#{tz_name}', '+00:00')
         AND tickets.#{state} <= CONVERT_TZ('#{to.to_date.to_s(:db)} 23:59:59', '#{tz_name}', '+00:00')
         #{"AND tickets.updated_at > CONVERT_TZ('#{(from - 1.day).to_date.to_s(:db)} 00:00:00', '#{tz_name}', '+00:00')" if state == 'solved_at'}
         AND #{non_deleted_tickets_condition}
       GROUP BY stamped
       ORDER BY stamped ASC"
  end

  # Unresolved Tickets
  def working_query(conditions)
    "SELECT #{converted_date('virtual_date_range.created_at_limit')} AS stamped, COUNT(*) AS matches
      #{from_statement(conditions)},
       ( SELECT
           #{converted_end_of_day_sequence} AS created_at_limit,
           #{converted_end_of_day_sequence} AS solved_at_limit
         FROM integers WHERE i < #{date_range.size}
       ) AS virtual_date_range
      WHERE tickets.account_id = #{account.id}
        AND virtual_date_range.created_at_limit >= tickets.created_at
        AND (tickets.solved_at IS NULL OR virtual_date_range.solved_at_limit <= tickets.solved_at)
        AND #{non_deleted_tickets_condition}
        #{add_conditions(conditions)}
      GROUP BY stamped
      ORDER BY stamped ASC"
  end

  # All Tickets
  def any_query(conditions)
    "SELECT #{converted_date('virtual_date_range.index_date')} AS stamped, COUNT(*) AS matches
       #{from_statement(conditions)},
         ( SELECT #{converted_end_of_day_sequence} AS index_date
             FROM integers WHERE i < #{date_range.size}
         ) AS virtual_date_range
       WHERE tickets.account_id = #{account.id}
         AND tickets.created_at <= virtual_date_range.index_date
         AND #{non_deleted_tickets_condition}
         #{add_conditions(conditions)}
       GROUP BY stamped
       ORDER BY stamped ASC"
  end

  # Old Tickets
  def any_closed_query(conditions)
    "SELECT #{converted_date('virtual_date_range.index_date')} AS stamped, COUNT(*) AS matches
       #{from_statement(conditions)},
         ( SELECT #{converted_end_of_day_sequence} AS index_date
             FROM integers WHERE i < #{date_range.size}
         ) AS virtual_date_range
       WHERE tickets.account_id = #{account.id}
         AND tickets.solved_at IS NOT NULL
         AND tickets.solved_at <= virtual_date_range.index_date
         AND tickets.updated_at > '1970-01-01 00:00:00'
         AND #{non_deleted_tickets_condition}
         AND tickets.created_at > '1970-01-01 00:00:00'
         #{add_conditions(conditions)}
       GROUP BY stamped
       ORDER BY stamped ASC"
  end

  def from_statement(conditions)
    if account.extended_ticket_metrics_visible? && conditions.any? { |c| metrics_condition?(c) }
      "FROM tickets INNER JOIN ticket_metric_sets ON ticket_metric_sets.ticket_id = tickets.id"
    else
      "FROM tickets"
    end
  end

  def get_custom_field_condition_sql(ticket_field_id, operator, value)
    ticket_field = TicketField.active.find(ticket_field_id)
    return nil unless ticket_field

    if ticket_field.is_a?(FieldCheckbox)
      tag = ticket_field.tag

      return nil unless tag.present?

      # cast incoming true/false value to operator
      unless Zendesk::DB::Util.truthy?(Array(value).first.to_s)
        operator = operator.index('is') ? 'not_includes' : 'NOT REGEXP'
      end

      value = tag
    else
      option = ticket_field.custom_field_options.find_by_id(value.strip)
      return nil unless option
      value = option.value
    end

    "AND IFNULL(current_tags, '') #{operator} #{Report.send(:sanitize_sql, ['?', "(^| )#{value}($| )"])} != 0 "
  end

  def add_conditions(conditions)
    result = ''

    conditions.each do |condition|
      column, operator, value = condition

      if column =~ /ticket_fields_(\d+)/
        sql = get_custom_field_condition_sql($1.to_i, operator, value)
        result << sql if sql
      else
        if metrics_condition?(condition)
          next unless account.extended_ticket_metrics_visible?
          column = "ticket_metric_sets.#{column}"
          value = (value.to_f * 60).round.to_s if column.include? "in_minutes"
        else
          column = "tickets.#{column}"
        end

        condition_sql =
          case column
          when 'tickets.current_tags'
            operator.gsub!('LIKE', 'REGEXP')
            if value.blank?
              "AND IFNULL(#{column}, '') #{'!' if operator =~ /NOT REGEXP/}= '' "
            else
              value = value.split(' ').map { |v| Regexp.escape(v) }.join('|') # Hope that MySQL and Ruby regexps are somewhat similar..
              "AND IFNULL(#{column}, '') #{operator} #{Report.send(:sanitize_sql, ['?', "(^| )(#{value})($| )"])} != 0 "
            end
          when 'tickets.locale_id'
            default_condition(column, operator, account.translation_locale.id, value)
          when 'tickets.ticket_form_id'
            default_condition(column, operator, account.ticket_forms.default.first.id, value)
          else
            value = "%#{value}%" if operator =~ /LIKE/
            standard_condition(column, operator, value)
          end

        result << condition_sql
      end
    end

    result
  end

  def standard_condition(column, operator, value)
    column_filter = "#{column} #{operator} #{Report.send(:sanitize_sql, ['?', value])}"
    operator == '!=' ? "AND (#{column_filter} OR #{column} IS NULL) " : "AND #{column_filter} "
  end

  def default_condition(column, operator, default_id, value)
    if default_id == value.to_i
      column_filter = "#{column} #{operator} #{Report.send(:sanitize_sql, ['?', value])}"
      operator == '!=' ? "AND #{column_filter} AND #{column} IS NOT NULL " : "AND (#{column_filter} OR #{column} IS NULL) "
    else
      standard_condition(column, operator, value)
    end
  end

  def metrics_condition?(condition)
    column, = condition
    TicketMetricSet.attributes_for_reports.include?(column)
  end

  protected

  def tz_name
    Time.zone.tzinfo.name
  end

  def converted_date(field)
    # push timezone daylight savings threshold calculations onto mysql and its time_zone tables
    # in case of fire, verify ActiveSupport::TimeZone.zones_map[account.time_zone] maps to a valid TZInfo object
    # with equivalent Time Zone Database (www.iana.org/time-zones) identifier in the mysql.time_zone_name table
    "DATE(CONVERT_TZ(tickets.#{field}, '+00:00', '#{tz_name}'))"
  end

  def converted_end_of_day_sequence
    "CONVERT_TZ(ADDDATE('#{from.to_date.to_s(:db)} 23:59:59', i), '#{tz_name}', '+00:00')"
  end
end
