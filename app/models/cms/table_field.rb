class Cms::TableField
  attr_accessor :name, :key

  def initialize(title, keyname)
    @name = title
    @key = keyname
  end
end
