require 'active_record_inherit_assoc'

class Cms::Text < ActiveRecord::Base
  self.table_name = 'cms_texts'

  extend ZendeskCms::Stats
  include CachingObserver

  MAX_FETCH_DEPTH = 5

  belongs_to :account

  has_one :fallback, -> { where(is_fallback: true) }, class_name: "Cms::Variant", foreign_key: :cms_text_id, inverse_of: :text, inherit: :account_id
  has_many :variants,   class_name: "Cms::Variant", foreign_key: :cms_text_id, dependent: :destroy, inverse_of: :text, inherit: :account_id
  has_many :references, class_name: "RuleReference", as: :item, inherit: :account_id

  attr_accessible :name, :identifier, :account, :fallback_attributes, :variants_attributes
  accepts_nested_attributes_for :fallback, :variants, allow_destroy: true

  alias_attribute :title, :name
  validates_presence_of :title, :account_id, :fallback

  validate do |text|
    text.errors.add(I18n.t('txt.admin.models.cms.text.Title'), I18n.t("txt.admin.models.cms.text.invalid_characters")) if should_validate?(text) && text.name =~ /[^a-zA-Z0-9_:~`!@#$%^&*()+={};'"|<>,.?\s\[\]\/\\-]/
    text.errors.add(I18n.t('txt.admin.models.cms.text.Title'), I18n.t("txt.admin.models.cms.text.ending_hyphen")) if should_validate?(text) && text.name =~ /-$/
    text.errors.add(I18n.t('txt.admin.models.cms.text.Title'), I18n.t('txt.admin.models.cms.text.has_already_been_taken')) if (should_validate?(text) || text.title_changed?) && text.account.cms_texts.where.not(id: text.id).find_by_name(text.name)
    text.errors.add(I18n.t('txt.admin.models.cms.text.Content'), I18n.t('txt.admin.models.cms.text.cant_be_blank')) if text.fallback && text.fallback.value.blank?
    if should_validate?(text) && text.account.cms_texts.where.not(id: text.id).find_by_identifier(text.identifier)
      if /[^a-zA-Z0-9\s_:-]+/.match?(text.name)
        text.errors.add(I18n.t('txt.admin.models.cms.text.Placeholder'), I18n.t('txt.admin.models.cms.text.non_ascii_already_in_use', dc_placeholder: "{{dc.#{text.identifier}}}"))
      else
        text.errors.add(I18n.t('txt.admin.models.cms.text.Placeholder'), I18n.t('txt.admin.models.cms.text.already_in_use', dc_placeholder: "{{dc.#{text.identifier}}}"))
      end
    end
  end
  before_validation :set_identifier
  before_destroy :check_references_and_destroy_fallback, prepend: true

  scope :ordered,     ->(*args) { order(sanitized_ordering_params(*args)) }
  scope :for_account, ->(account_id) { where(account_id: account_id) }
  scope :in_category, ->(text_ids) { where(id: text_ids) }
  scope :inactive,    -> { joins(:variants).where('cms_variants.active = false') }
  scope :active,      -> { joins(:variants).where('cms_variants.active = true') }
  scope :outdated,    -> { joins(:variants, :fallback).where('cms_variants.id != fallbacks_cms_texts.id AND cms_variants.updated_at <= fallbacks_cms_texts.updated_at AND cms_variants.active = true') }

  delegate :available_languages, to: :account

  def check_references_and_destroy_fallback
    if references.empty?
      # Prevent deletion of fallbacks unless we're deleting text
      fallback.mark_for_destruction
      fallback.destroy
    else
      errors.add(:base, I18n.t('txt.admin.models.cms.text.Text_still_has_references'))
      false
    end
  end

  def default_locale_id
    fallback.try :translation_locale_id
  end

  def self.setup(params, account)
    params[:cms_text][:fallback_attributes].merge!(
      is_fallback: true,
      nested: true,
      active: true
    )

    text = account.cms_texts.new(params[:cms_text])
    text.account = account

    text
  end

  def outdated?
    variants.active.outdated.exists?
  end

  def locales
    variants.map(&:translation_locale)
  end

  def remaining_locales
    available_languages - locales
  end

  def self.sanitized_ordering_params(sort, direction)
    sort = %w[name updated_at].include?(sort) ? sort : 'name'
    direction = %w[ASC DESC].include?(direction) ? direction : 'ASC'
    "#{sort} #{direction}"
  end

  def smart_fetch(locale, tries = MAX_FETCH_DEPTH)
    # Precedence order: Users Locale, Account's Locale, locale's parent(fallback locale), Fallback variant
    # user.translation_locale will return accounts locale if users locale is nil
    #
    # Top level locales (e.g. Spanish, French, etc) are ENGLISH_BY_ZENDESK's child
    # we should only smart_fetch for non top level locales
    if variant = find_variant_for_locale(locale)
      variant
    elsif tries <= 0 || locale.nil? || locale.top_level?
      fallback
    else
      smart_fetch(locale.parent, tries - 1)
    end
  end

  def fully_qualified_identifier
    "dc.#{identifier}"
  end

  def placeholder
    "{{#{fully_qualified_identifier}}}"
  end

  private

  def set_identifier
    return if identifier || !name
    self.identifier = build_identifier
  end

  def build_identifier
    name.
      downcase.
      gsub(/^[^a-z0-9_-]+/, '').    # remove leading non-alphanumerics
      gsub(/[^a-z0-9_-]+$/, '').    # remove trailing non-alphanumerics
      gsub('::', '-').              # convert :: to -
      gsub(/[^a-z0-9_-]+/, '_')     # convert runs of non-alphanumerics to _
  end

  def inherit_account_id
    self.account_id = account.id
  end

  def find_variant_for_locale(locale)
    variants.active.find_by_translation_locale_id(locale.id) if locale
  end

  def should_validate?(text)
    text.new_record? || text.account.has_validate_cms_text_update?
  end
end
