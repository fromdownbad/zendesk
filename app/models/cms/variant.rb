class Cms::Variant < ActiveRecord::Base
  self.table_name = 'cms_variants'
  include CachingObserver

  belongs_to :account
  belongs_to :text, class_name: "Cms::Text", foreign_key: :cms_text_id, inherit: :account_id
  belongs_to :translation_locale

  scope :active, -> { where(active: true) }
  scope :outdated, -> {
    joins(text: :fallback).
      where("cms_variants.id != fallbacks_cms_texts.id AND cms_variants.updated_at < fallbacks_cms_texts.updated_at")
  }
  # fallbacks_cms_text is the temporary join table that is maintained for the duration of the query
  validates :translation_locale, presence: { message: "invalid locale" }

  validates_presence_of :account_id

  validate do |variant|
    # variants can be fallbacks, and then language is called default language:
    # txt.admin.models.cms.text.Default_Language as opposed to txt.admin.models.cms.variant.Language
    language_key = variant.is_fallback? ? 'text.Default_Language' : 'variant.Language'
    variant.errors.add(I18n.t("txt.admin.models.cms.#{language_key}"), I18n.t('txt.admin.models.cms.variant.cant_be_blank')) if variant.translation_locale_id.blank?
    variant.errors.add(I18n.t('txt.admin.models.cms.variant.Content'), I18n.t('txt.admin.models.cms.variant.cant_be_blank')) if !variant.nested && variant.value.blank?
  end
  validates_uniqueness_of :translation_locale_id, scope: :cms_text_id
  validates_presence_of :text, unless: :nested

  delegate :name, to: :translation_locale
  delegate :localized_name, to: :translation_locale

  before_validation :copy_account_from_text
  before_save :check_fallback
  after_save :deactivate_old_fallback
  before_destroy :check_if_fallback_and_marked_for_destruction

  attr_accessor :nested

  attr_accessible :value, :is_fallback, :translation_locale_id, :translation_locale, :nested, :active

  def check_fallback
    if is_fallback?
      self.active = true unless active
    end
  end

  def deactivate_old_fallback
    if is_fallback?
      Cms::Variant.where(is_fallback: true, cms_text_id: cms_text_id, account_id: account_id).where.not(id: id).update_all(is_fallback: false)
    end
  end

  def outdated?
    return false if is_fallback? || active == false
    updated_at.to_i < text.fallback.updated_at.to_i if text.fallback
  end

  def status
    "#{active? ? I18n.t('txt.admin.models.cms.variant.Active') : I18n.t('txt.admin.models.cms.variant.Inactive')}#{I18n.t('txt.admin.models.cms.variant.Out_of_date') if outdated?}"
  end

  private

  def check_if_fallback_and_marked_for_destruction
    if is_fallback? && !marked_for_destruction?
      errors.add(:base, I18n.t('txt.admin.models.cms.variant.default_variant_cannot_be_deleted'))
      false
    end
  end

  def copy_account_from_text
    self.account_id ||= text.account_id if text.present?
  end
end
