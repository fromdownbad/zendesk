class AttributeTicketMap < ActiveRecord::Base
  MAX_DEFINITION_SIZE = 65_535

  include CIA::Auditable

  attr_accessible :definition

  attr_accessor :current_user

  audit_attribute

  belongs_to :account

  has_soft_deletion default_scope: true

  after_soft_delete :audit_deletion

  validates_presence_of :account
  validates_presence_of :definition

  validate :valid_definition
  validate :valid_definition_size

  serialize :definition

  def match?(ticket)
    Zendesk::Rules::Match.match?(self, ticket)
  end

  def rule_type
    @rule_type ||= self.class.name.underscore
  end

  private

  def audit_deletion
    CIA.record(:destroy, self)
  end

  def valid_definition
    return if deleted_at
    definition.run_new_validations(
      account,
      :attribute,
      self,
      current_user
    )
  end

  def valid_definition_size
    return unless definition_size.bytesize > MAX_DEFINITION_SIZE

    errors.add(
      :definition,
      I18n.t(
        'activerecord.errors.messages.too_long',
        count: MAX_DEFINITION_SIZE
      )
    )
  end

  def definition_size
    @attributes['definition'].value_for_database
  end
end
