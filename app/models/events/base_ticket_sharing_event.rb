module BaseTicketSharingEvent
  # Aliases for consistency. All events use these attribute names.
  alias_attribute :agreement_id, :value

  def to_s
    recipient_name
  end

  def recipient_name
    if account
      agreement = account.sharing_agreements.find_by_id(agreement_id)
      agreement ||= Sharing::Agreement.with_deleted do
        Sharing::Agreement.find_by_account_id_and_id(account.id, agreement_id)
      end
    end

    agreement ? agreement.name : 'a sharing partner'
  end

  def agreement_id
    if v = read_attribute(:value)
      v.to_i
    end
  end
end
