require 'zendesk_archive'

# The event is a generic super class which expresses changes to a ticket. Each event constitutes
# a change to the ticket of some sort
class Event < ActiveRecord::Base
  class ValueTooLarge < ActiveRecord::ActiveRecordError; end

  include ScopedCacheKeys
  include ViaManagement
  include Zendesk::Serialization::EventSerialization
  include ZendeskArchive::Archivable # archive v2
  include PrecreatedAccountConversion

  MAX_VALUE_BYTESIZE = 64.kilobytes

  belongs_to  :account
  belongs_to  :ticket, inherit: :account_id
  belongs_to  :author, class_name: 'User', foreign_key: :author_id, inherit: :account_id, inherit_allowed_list: [SYSTEM_ACCOUNT_ID]
  has_one     :event_decoration, inherit: :account_id

  attr_accessor :mark_for_scrubbing

  attr_accessible :account, :ticket, :author

  before_validation :set_account

  delegate :add_translatable_error_event, to: :ticket

  # Called for some reason because of alias_method_chain in audit_event (audit_with_archived_parent_ticket)
  def self.is_public(*_args) # rubocop:disable Naming/PredicateName
    where(is_public: true)
  end

  def to_s
    (value || 'Empty event').to_s
  end

  # Convenience method for Create/Change event
  def transaction=(change) # change => { 'title' => ["Title", "New Title"] }
    self.value_reference = change[0]
    self.value_previous  = change[1][0]
    self.value           = change[1][1]
  end

  # This is a convenience method for tests
  def inspect_me
    title = " #{Rule.find(via_reference_id).title}" if !via?(:web_form) && via_reference_id
    name  = author.name if author

    "%s : %s : %s%s by %s" % [self.class,
                              self,
                              ViaType.to_s(via_id).downcase,
                              title,
                              name]
  end

  def after_find_from_archive(ticket)
    association(:ticket).target = ticket
    @archived_parent_ticket = ticket
  end

  # See test/integration/comment_sti_serialization_test.rb before modifying this hack!
  # the attribute accessors are already defined and do not get overwritten -> overwrite by hand
  def self.fixed_serialize(attr_name, class_name = Object)
    serialize(attr_name, class_name)

    if RAILS4
      define_method(attr_name) do
        read_attribute(attr_name)
      end

      define_method("#{attr_name}=") do |new_value|
        write_attribute(attr_name, new_value)
      end
    else
      raise "Remove fixed_serialize in rails 5" if RAILS51
    end
  end

  protected

  def set_account
    self.account_id ||= ticket.try(:account_id)
  end

  # It's use to remove the readonly constraint while scrubbing
  def scrubbing?
    @mark_for_scrubbing
  end
end
