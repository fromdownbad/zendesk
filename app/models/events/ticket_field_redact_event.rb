class TicketFieldRedactEvent < AuditEvent
  belongs_to :ticket_field_entry, foreign_key: :value, class_name: 'TicketFieldEntry', inherit: :account_id

  alias_attribute :ticket_field_entry_id, :value

  before_validation :set_value_reference

  protected

  def set_value_reference
    self.value_reference = "ticket_field_entry_id"
  end
end
