class AutomaticAnswerReject < AuditEvent
  fixed_serialize :value, Hash

  before_create :set_type_and_is_public
  validates :article, presence: true
  validates :reviewer_id, presence: true
  validates :reviewer_name, presence: true, on: :create
  validate  :article_should_have_required_fields

  ARTICLE_REQUIRED_FIELDS = [:title, :url, :id, :html_url].freeze

  def article=(article)
    value[:article] = article
  end

  def irrelevant
    value[:irrelevant]
  end

  def irrelevant=(irrelevant_value)
    value[:irrelevant] = irrelevant_value
  end

  def article
    value[:article] || {}
  end

  def reviewer_id=(reviewer_id)
    value[:reviewer_id] = reviewer_id
  end

  def reviewer_id
    value[:reviewer_id]
  end

  def reviewer_name=(reviewer_name)
    value[:reviewer_name] = reviewer_name
  end

  def reviewer_name
    value[:reviewer_name]
  end

  def reason
    value[:reason] || ''
  end

  def reason=(reason)
    value[:reason] = reason
  end

  def body
    value
  end

  private

  def set_type_and_is_public
    self.type = 'AutomaticAnswerReject'
    self.is_public = false
    value[:irrelevant] = false unless value[:irrelevant]
    true
  end

  def article_should_have_required_fields
    ARTICLE_REQUIRED_FIELDS.each do |field|
      errors.add(:article, " does not have the required one of the required keys {id, url, title, html_url}") unless article.key? field
    end
  end
end
