class KnowledgeLinkRejected < AuditEvent
  fixed_serialize :value, Hash

  before_save :set_type_and_is_public

  validates :article, presence: true
  validate  :article_should_have_required_fields

  alias_attribute :article, :value

  attr_accessible :article

  ARTICLE_REQUIRED_FIELDS = %i[title url id html_url locale].freeze

  def article
    value || {}
  end

  alias_method :body, :article

  private

  def set_type_and_is_public
    self.is_public = false
    self.type = 'KnowledgeLinkRejected'
  end

  def article_should_have_required_fields
    ARTICLE_REQUIRED_FIELDS.each do |field|
      errors.add(:article, " does not have the required key '#{field}'") unless article.key?(field)
    end
  end
end
