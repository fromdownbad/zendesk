# A child of an audit
module AuditEventArchivedParentTicket
  def audit(*args)
    if archived? && @archived_parent_ticket && !@audit_association_loaded_from_archive
      association(:audit).target = @archived_parent_ticket.events.detect { |a| a.id == parent_id }
      @audit_association_loaded_from_archive = true
    end
    super(*args)
  end
end
class AuditEvent < Event
  prepend AuditEventArchivedParentTicket
  self.table_name = 'events'
  self.abstract_class = true

  belongs_to    :audit, foreign_key: :parent_id, inherit: :account_id
  before_create :inherit_defaults_from_audit

  COMMENT_EVENTS      = ['Comment', 'VoiceComment', 'VoiceApiComment', 'FacebookComment'].freeze
  CHAT_EVENTS         = ['ChatStartedEvent', 'ChatEndedEvent'].freeze
  CONVERSATION_EVENTS = COMMENT_EVENTS + CHAT_EVENTS + ['AutomaticAnswerSend', 'AutomaticAnswerSolve', 'CollabThreadCreated', 'KnowledgeLinkAccepted'].freeze

  attr_accessible :audit, :created_at

  def readonly?
    !new_record? && !scrubbing?
  end

  def inherit_defaults_from_audit
    self.ticket_id        ||= audit.ticket_id
    self.author_id        ||= audit.author_id
    self.via_id           ||= audit.via_id
    self.via_reference_id ||= audit.via_reference_id
    self.account_id       ||= audit.account_id
    self.is_public        ||= false
    true
  end

  def client
    audit.client
  end

  def recipient_names(recipients)
    ids   = recipients.is_a?(String) ? YAML.load(recipients) : recipients
    users = User.where(id: ids).order(:name).pluck(:name)

    return CGI.escapeHTML(users.join(', ')) if users.any?

    Rails.logger.warn("No recipients for outbound event #{id}, value: #{recipients.inspect}")
    "No such user: #{recipients}"
  end

  protected

  def set_account
    audit_aux = ticket.nil? && account_id.nil? && parent_id.present? ? Audit.find(parent_id) : audit
    self.account_id ||= (ticket || audit_aux).try(:account_id)
  end
end
