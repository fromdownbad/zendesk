class AnswerBotNotification < AuditEvent
  alias_attribute :recipients, :value
  alias_attribute :subject, :value_previous
  alias_attribute :body, :value_reference

  attr_accessible :value, :recipients, :subject, :body, :value_previous, :value_reference
  attr_reader :ccs

  fixed_serialize :value, Array
  truncate_attributes_to_limit :value_reference # value_reference is 8k, but rule actions can have up to 65k of text
  after_commit :deliver_notifications, on: :create

  def initialize(options = {})
    @ccs = options.delete(:ccs) || []
    @recipients = options[:recipients] || []

    recipient_ids = @recipients + ccs.map(&:id)

    super(options.merge(recipients: recipient_ids.compact.uniq))
  end

  protected

  def deliver_notifications
    unless ticket
      Rails.logger.warn("No ticket found for event #{id}, skipping")
      return
    end

    Rails.logger.info("Delivering notifications for event #{id} at #{Time.now}")

    if Arturo.feature_enabled_for?(:deflection_mailer_header, account)
      DeflectionMailer.deliver_answer_bot_email(ticket, self, via_reference_id, ccs)
    else
      OldDeflectionMailer.deliver_answer_bot_email(ticket, via_reference_id, ccs)
    end
  end
end
