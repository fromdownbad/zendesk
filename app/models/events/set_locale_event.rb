class SetLocaleEvent < AuditEvent
  belongs_to :user, foreign_key: "via_reference_id"
  alias_attribute :locale, :value
  alias_attribute :old_locale, :value_previous

  def to_s
    value
  end
end
