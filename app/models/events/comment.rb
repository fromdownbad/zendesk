require 'uri'
require 'zendesk/liquid/comments/comment_drop'
require 'zendesk_comment_markup'
require 'abbreviato'
require 'sanitize'

class Comment < AuditEvent
  MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth
  SANITIZE_PARSER_OPTIONS = { max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH }.freeze

  include Zendesk::Serialization::CommentSerialization
  include ERB::Util
  include AttachmentsHelper
  include EmojiHelper
  include TicketStatsObserver
  include Zendesk::Liquid::Escaping

  include Zendesk::CreditCardRedaction
  credit_card_redaction_attrs :value

  attr_accessor :add_short_url,
    :channel_back,
    :channel_source_id,
    :original_plain_body,
    :override_pre_styled,
    :reply_option,
    :uploads,
    :url_builder

  attr_reader :raw_html_body

  scope :since, ->(time) { where(['events.created_at >= ?', time]) }
  scope :roles, ->(roles) { joins(:author).where(users: { roles: roles }) }

  has_many :attachments,
    -> { where(parent_id: nil).order('created_at ASC, id ASC') },
    as: :source,
    dependent: :destroy,
    inherit: :account_id

  has_one :shared_comment
  has_one :redaction_event, foreign_key: :value, class_name: "CommentRedactionEvent", inherit: :account_id

  require_dependency "voice_comment"
  require_dependency "facebook_comment"

  attr_accessible :add_short_url, :author_id, :body, :channel_back, :created_at, :is_public, :uploads, :value,
    :value_previous, :value_reference, :via_id, :via_reference_id, :channel_source_id,
    :html_body, :format, :reply_option

  before_validation :truncate_text
  before_validation :prepare_rich_value
  validate      :validate_under_limit, on: :create
  validate      :validate_not_empty, on: :create
  validate      :validate_body, on: :create
  validate      :validate_all_attachment_tokens
  validate      :not_in_twitter_maintenance_mode, on: :create, if: :ticket_via_twitter?
  validate      :validate_public_reply, on: :create
  validate      :validate_author_belongs_to_account
  before_create :liquidize_value
  before_create :add_signature
  before_update :create_privacy_change_event, if: :is_public_changed?
  before_update :truncate
  before_create :check_privacy
  before_create :force_privacy
  before_create :create_attachments
  before_create :strip_invalid_images
  before_create :suppress_unicode_replacement
  before_create :sanitize_body
  # Is important that :truncate_html comes last so that it can act upon the final size of the content being saved to the db
  before_create :truncate_html
  after_create  :create_post_on_corresponding_entry
  after_create :add_attachment_linked_domain_events

  delegate :for_partner, to: :author
  delegate :flagged_reasons, to: :audit

  def format=(new_format)
    new_format = nil unless new_format == "rich"
    self.value_previous = new_format
  end

  def format
    value_previous if value_previous == "rich"
  end

  # Posting a comment back to a channel is only supported for the following scenario:
  #
  # 1. Ticket is created via a channel
  # 2. This comment is not from a channels conversion (it's already in the external channel)
  # 3. This comment is a public reply (from an agent)
  # 4. Ticket is not explicitly blocked from channelback
  def supports_channel_back?
    ticket_via_channel? &&
      !comment_via_channel? &&
      public_reply? &&
      ticket.allow_channelback?
  end

  # Actually post the comment back to the channel if:
  #
  # 1. The client has requested it, and
  # 2. Channeling back is supported
  def should_create_channel_back_event?
    @should_create_channel_back_event ||=
      channel_back_requested? && supports_channel_back?
  end

  def comment_via_channel?
    audit && (via_id == audit.ticket.via_id)
  end

  def ticket_via_twitter?
    ticket_via_tweet? || ticket_via_twitter_dm?
  end

  def ticket_via_tweet?
    audit && audit.ticket.via_tweet?
  end

  def ticket_via_twitter_dm?
    audit && audit.ticket.via_twitter_dm?
  end

  def ticket_via_facebook?
    audit && audit.ticket.via_facebook?
  end

  def ticket_via_any_channel?
    audit && audit.ticket.via_any_channel?
  end

  def ticket_via_channel?
    audit && audit.ticket.via_channel?
  end

  def validate_public_reply
    # If the ticket is based on an external social media entity, and the agent chose to make a
    # public reply, then possible scenarios are:
    #
    # 1) Post the reply back to the channel, or
    # 2) Send an email-only reply.
    #
    # However, if the requester doesn't have any e-mail identities, then an email-only reply is
    # invalid. This ensures that we don't get into that state.
    # There is a bigger conundrum of how we're getting into this state, when the client-side (Lotus)
    # apparently doesn't allow this state:
    # https://github.com/zendesk/zendesk_console/blob/1af8f120ebfcf876b305f7605e52f951e606b2ef/app/assets/javascripts/controllers/ticket_controller/ticket_comment_states_mixin.module.js#L6.
    # That warrants further investigation.
    if supports_channel_back? &&
      !channel_back_requested? &&
      ticket && ticket.requester && ticket.requester.email.nil?

      channel = ticket_via_facebook? ? 'facebook' : 'twitter'
      datadog_tag = audit ? audit.ticket.via.snakecase : 'unknown'

      Rails.logger.warn("Attempt to make a non-channelback non-email public reply for ticket #{ticket.id} in account #{account.id} for channel #{datadog_tag}")

      self.class.statsd_client.increment(:invalid_public_reply, tags: ["channel:#{datadog_tag}"])

      # CT-2123 For now we still display the twitter error for AnyChannel for now.
      # Capture metrics and logs before we decide what to fix.
      errors.add(:base, I18n.t("api.errors.#{channel}.invalid_reply"))
    end
  end

  def first?
    self.class.
      where(account_id: account.id, ticket_id: ticket.id).
      where('created_at < ?', created_at).
      none?
  end

  def readonly?
    super && !change_allowed? && !scrubbing?
  end

  def trusted?
    new_record? ? ticket.audit.trusted? : audit && audit.trusted?
  end
  alias_method :trusted, :trusted?

  def flagged?
    new_record? ? ticket.audit.flagged? : audit.flagged?
  end

  def current_audit
    new_record? ? ticket.audit : audit
  end

  def public_reply?
    is_public? && reply?
  end

  def reply?
    if Arturo.feature_enabled_for?(:agent_as_end_user, account)
      (value.present? || attachments.any?) &&
        ((author.is_agent? && !ticket.try(:agent_as_end_user_for?, author)) || author.foreign_agent?)
    else
      (value.present? || attachments.any?) &&
        (author.is_agent? || author.foreign_agent?)
    end
  end

  def from_end_user?
    return false unless is_public?

    is_from_end_user = phone_call_inbound? || (author.is_end_user? && !author.foreign_agent?)

    return is_from_end_user unless Arturo.feature_enabled_for?(:agent_as_end_user, account)

    is_from_end_user || ticket.agent_as_end_user_for?(author)
  end

  def is_private? # rubocop:disable Naming/PredicateName
    !is_public?
  end

  def phone_call_inbound?
    via_id == ViaType.PHONE_CALL_INBOUND
  end

  def phone_call_outbound?
    via_id == ViaType.PHONE_CALL_OUTBOUND
  end

  def self.map_latest_comment_to_tickets(tickets)
    comments = latest_by_created_at_for_tickets(tickets.map(&:id))

    tickets.each do |t|
      t.latest_comment_mapped = comments[t.id]
    end
  end

  def redact!(text, redact_full_comment = false)
    redactor = rich? ? Zendesk::HtmlRedaction : Zendesk::TextRedaction

    temp_allow_changes do
      if redacted = redactor.redact_part_or_all!(value, text, redact_full_comment)
        self.value = redacted
        audit.delete_eml_file!
        audit.redact_json_files!(text)
        save!
      end
    end
  end

  def omniredact!(text, user, ticket_audit, external_attachment_urls = [])
    temp_allow_changes do
      redacted = Redaction::CommentRedaction.new.tap do |redactor|
        redactor.ticket = ticket
        redactor.user = user
        redactor.comment = self
        redactor.audit = ticket_audit
        redactor.external_attachment_urls = external_attachment_urls
      end.execute!(text)

      unless redacted[:error]
        self.value = redacted[:text]
        save!
      end
    end
  end

  def add_redacted_attachment!(attachment)
    temp_allow_changes do
      attachments << attachment
      save!
    end
  end

  def create_attachments
    return if uploads.blank?
    attachments = tokens.to_a.map(&:attachments)
    attachments.flatten!
    attachments.each do |attachment|
      attachment.is_public = is_public?
    end

    self.attachments = attachments
  end

  def add_attachment_linked_domain_events
    # Note: This prevents unnecessary queries to attachments.
    # If attachments have not been set on create, then there won't be any events to add, and this will be false.
    # If attachments have been set on create, then it will be true, and AR will not attempt to query the database when calling `#attachments`.
    if RAILS4
      return unless association_cache.key?(:attachments)
    else
      return unless association_cached?(:attachments)
    end

    non_thumbnail_attachments = attachments.select { |a| a.parent_id.nil? }
    non_thumbnail_attachments.each do |attachment|
      ticket.add_domain_event(AttachmentLinkedToCommentProtobufEncoder.new(attachment, self).to_safe_object)
    end
  end

  def strip_invalid_images
    return unless rich?
    return unless value

    # remove broken safari images
    # https://dev.ckeditor.com/ticket/13029
    # https://bugs.webkit.org/show_bug.cgi?id=49141
    #
    # remove data and base64 images
    value.gsub!(%r{<img (?:[^>]*?)\bsrc="(?:data:.*|webkit-fake-url://)\S+"(?:[^>]*?)>}i, '')
  end

  def validate_all_attachment_tokens
    return true if uploads.nil?
    if tokens.size != Array(uploads).size
      errors.add(:uploads, :invalid)
      return false
    end
  end

  def to_s
    body || 'Empty comment'
  end

  def process_as_markdown?
    return false unless markdown_via?
    return false unless account.settings.markdown_ticket_comments? || legacy_markdown?

    author.present? && (author.is_agent? || author.foreign_agent?)
  end

  def rich?
    format == "rich"
  end

  def legacy_markdown?
    if Arturo.feature_enabled_for?(:legacy_markdown_sunset, account)
      false
    else
      !rich? && account.settings.rich_text_comments?
    end
  end

  def body=(new_body)
    if value.present? && rich?
      value
    else
      self.value = new_body
    end
  end

  # Used by Mail when a truncated email is "expanded" and full comment output filtering is desired. The temp value cannot be saved.
  def temp_plain_body=(new_body)
    self.value = new_body
  end

  def body
    prepare_rich_value if changed?
    @body ||= begin
      if format == "rich" && value.present?
        text = ZendeskCommentMarkup::StripperFilter.to_html(value)
        text.gsub(/\\[_\*<>]/, '\\*' => '*', '\\_' => '_', '\\<' => '<', '\\>' => '>')
      else
        value
      end
    rescue SystemStackError # Catches issues with StripperFilter
      plain_body
    end
  end

  # To guard against incorrect markdown being rendered as plain text
  # (perhaps as a result of incorrect use of markdown/rich text formatting by the end user)
  # we are finding the plain_body by converting all the <br> tags in the html_body to '\n', we then remove any zero-width joiners and non-breaking space characters left over from emoji's
  # then we sanitize all the remaining html tags and then pass the result to redcarpet
  def plain_body(cached_html_body = nil)
    # cached_html_body is used in CommentPresenter when we first call #html_body
    # That way we avoid calling #html_body twice.
    # One alternative could be to change that at the class level, breaking down
    # #html_body into 2 parts, but that's more confusing when reading the code,
    # and more prone to the introduction or errors.
    html = cached_html_body || html_body
    return '' if html.blank?
    new_body = html.gsub("<br>", "\n").gsub(/<!--.*?-->/m, "")
    sanitized_body = Sanitize.fragment(new_body.to_s, parser_options: SANITIZE_PARSER_OPTIONS)
    sanitized_body.delete("\u200d").tr("\u00a0", ' ').strip
  rescue SystemStackError
    Rails.logger.warn("Plain body could not be parsed for comment with id #{id} for account id #{account.id}")
    ''
  end

  # Returns an HTML representation of the comment body.
  #
  # Options are:
  #
  # :for_agent       - Boolean. Are you showing this to an agent?
  # :for_email       - Boolean. Are you sending this out in an email?
  # :base_url        - String. Make all the URLs in the comment absolute on this
  #                    base URL.
  # :css             - String. When rendering markdown or rich comments, use this stylesheet.
  # :ticket_base_url - String. Override the path to tickets. Default value for
  #                    classic agents is "/tickets"
  def html_body(options = {})
    return '' if value.blank?

    html = if rich?
      strip_tailing_space(value)
    else
      plain_body = body || ''
      plain_body = plain_body.truncate(options[:length]) if options[:length]
      plain_body.strip!

      rtl = options[:rtl] || false

      html = if process_as_markdown?
        ZendeskText::Markdown.html(plain_body, ticket_linking: false, rtl: rtl)
      else
        plain_body = strip_tailing_space(plain_body)
        truncation_length = account.settings.link_display_truncation_length
        h(plain_body).present(formatter: :simple_format, rel: 'nofollow', ticket_linking: false) do |url|
          url.truncate(truncation_length)
        end
      end

      emojify!(html) if account.has_emoji_autocompletion_enabled?

      html
    end

    options[:from_agent]      = author.try(:is_agent?)
    options[:for_agent]       = options[:for_agent] || false
    options[:safe_image_url]  = "#{account.url}/attachments/token"
    options[:access_controlled_media] = via?(:twitter_dm)

    options[:agent_base_url] ||= '/users'

    # url_builder may have been defined previously. We do that in CommentCollectionPresenter
    # and Api::V2::Rules::ViewRowsPresenter (for each ticket last comment)
    # to use the same instance for a collection of comments, instead of creating
    # a new instance for every Comment.
    self.url_builder ||= Zendesk::Tickets::UrlBuilder.new(account: account, for_agent: options[:for_agent])
    if ticket_path = url_builder.path_for(nil)
      options[:ticket_base_url] ||= '/' + ticket_path
    end

    doc = ZendeskCommentMarkup.filter_output(html, options).document

    rich_content_in_emails = account.settings.rich_content_in_emails?

    if options[:css] && !rich_content_in_emails
      doc = ZendeskCommentMarkup::CssInliningFilter.to_document(doc, inline_css: options[:css])
    end

    doc.children.first['class'] = "#{doc.children.first['class']} zd-comment-pre-styled" if pre_styled?

    block_elements = %w[p ol ul h1 h2 h3 h4 h5 h6]

    if account && account.has_rtl_lang_html_attr?
      block_elements.each do |element|
        doc.css(element).each { |node| node['dir'] = 'auto' }
      end
    end

    if options[:css] && rich_content_in_emails
      doc = ZendeskCommentMarkup::CssInliningFilter.to_document(doc, inline_css: options[:css])
    end

    if options[:relative_attachment_urls] && rich_content_in_emails && via?(:mail)
      # This behavior is scoped to comments created via HTML email ONLY
      remove_attachment_base_urls(doc)
    end

    # Rebuild inline attachment URLs for accounts with renamed subdomains
    if account.has_inline_images_rebuild_path_v2?
      rebuild_attachment_urls_v2(doc)
    end

    to_html_safe(doc)
  end

  def to_html_safe(doc)
    doc.to_html(indent: 0, encoding: 'UTF-8').html_safe
  rescue SystemStackError
    Rails.logger.warn("Comment with id #{id} for account id #{account.id} could not be parsed")
    ''
  end

  def pre_styled?
    return false unless rich?

    return via?(:mail) unless override_pre_styled

    via?(:mail) || via?(:web_service)
  end

  def html_body=(new_body)
    @raw_html_body = new_body
  end

  # Used by Mail when a truncated email is "expanded" and full comment output filtering is desired. The temp value cannot be saved.
  def temp_html_body=(new_body)
    self.value = nil
    @raw_html_body = new_body
  end

  def channel_back_requested?
    @channel_back.nil? || ["true", true, "1", 1, "dm"].include?(@channel_back)
  end

  def to_liquid
    liquid_hash = Zendesk::Liquid::Wrapper.new("value")

    liquid_hash.merge!(
      'value'                => body,
      'is_public'            => is_public,
      'created_at'           => lambda { |_| created_at.to_format },
      'created_at_with_time' => lambda { |_| created_at.to_format(with_time: true, time_format: account.time_format_string, time_zone: account.time_zone) },
      'author'               => lambda { |_| author },
      'attachments'          => lambda { |_| attachments },
      'id'                   => id
    )

    if Arturo.feature_enabled_for?(:liquid_comment_original_recipients, account) && audit && audit.recipients
      liquid_hash['original_recipients'] = audit.recipients.split(" ")
    end

    liquid_hash['value_rich'] = lambda do |context|
      drop = context['ticket']
      Zendesk::Liquid::Comments::CommentDrop.new(drop.account, self, drop.format, drop.for_agent, drop.current_user, ticket.brand,
        include_tz: drop.include_tz, skip_attachments: drop.skip_attachments, comment_html_only: true).timed_render.html_safe
    end

    liquid_hash
  end

  # Convenience method
  def transaction=(comment) # change => { 'title' => ["Title", "New Title"] }
    self.format    = comment.format
    self.value     = comment.value
    self.is_public = comment.is_public
  end

  def build_shared_attachments(account, ts_comment)
    return unless ts_comment && ts_comment.attachments

    ts_comment.attachments.each do |ts_attachment|
      thumbnail = ts_attachment.content_type =~ /image/
      attachments << Attachment.from_external_url(account,
        ts_attachment.url, ts_attachment.filename, ts_attachment.content_type, thumbnail, ts_attachment.display_filename)
    end
  end

  def intermediate_value
    @intermediate_value || body
  end

  # Set the value of the body, but remember any placeholders.  This is useful when the original body will be needed
  # to be applied later, e.g. when this is a problem ticket and we're creating comments on tickets that are incidents
  # of this ticket
  def set_liquidized_body(new_value) # rubocop:disable Naming/AccessorMethodName
    save_intermediate_value
    self.body = new_value
  end

  def screencasts
    return [] if audit.blank?
    Screencast.screencasts_from_metadata(audit.metadata)
  end

  def is_email_only? # rubocop:disable Naming/PredicateName
    !via_twitter? && !via_facebook? && !channel_back_requested? && is_public?
  end

  def redact_credit_card_numbers!
    if super
      build_redaction_event
      ticket.send(:write_attribute, :description, value) unless ticket.persisted?
      ticket.add_redaction_tag!
    end
  end

  def empty?
    value.blank?
  end

  def reload(options = nil)
    @body = nil
    super
  end

  def self.latest_by_created_at_for_tickets(ticket_ids)
    return [] if ticket_ids.blank?

    comments = Comment.find_by_sql(
      [
        "SELECT e1.* "\
        "FROM events e1, ( "\
        "  SELECT ticket_id, MAX(created_at) AS created_at "\
        "  FROM   events e2 "\
        "  WHERE  ticket_id IN (?) "\
        "  AND    type IN ('Comment', 'VoiceComment', 'VoiceApiComment', 'FacebookComment') "\
        "  GROUP BY ticket_id "\
        ") AS es "\
        "WHERE e1.ticket_id  = es.ticket_id "\
        "AND   e1.type       IN ('Comment', 'VoiceComment', 'VoiceApiComment', 'FacebookComment') "\
        "AND   e1.created_at = es.created_at "\
        "ORDER BY e1.ticket_id DESC, e1.created_at DESC, e1.id DESC", ticket_ids.map(&:to_i)
      ]
    )

    # find_by_sql does not support `includes`, so manually preload author / comments
    # Prevent rendering of multiple comments triggering AR to build the same associations
    # over and over.
    comments.pre_load(:account, :author)

    comments.each_with_object({}) do |record, base|
      base[record.ticket_id] ||= record
    end
  end

  def set_privacy!
    check_privacy
    force_privacy
    is_public?
  end

  def value
    prepare_rich_value
    read_attribute(:value)
  end

  def value=(new_value)
    @body = nil
    write_attribute(:value, new_value)
  end

  def signature_added?
    should_add_signature? && author.signature.present? && !author.signature.value.blank?
  end

  protected

  def prepare_rich_value
    if @raw_html_body.present?
      self.format = "rich"
      self.value = @raw_html_body
      @raw_html_body = nil
    end
  end

  def markdown_via?
    [:web_form, :web_service, :linked_problem, :closed_ticket, :ticket_sharing, :topic, :rule, :merge, :helpcenter, :mobile].detect { |v| via?(v) }
  end

  private

  def validate_author_belongs_to_account
    return if account_id && !account.has_validate_author_belongs_to_account_on_create?
    # If author_id is nil here, it will inherit the author_id of the parent Audit class.
    # See AuditEvent.inherit_defaults_from_audit
    return if author_id.nil?
    return if author.is_system_user?

    if author.account_id != account_id
      errors.add(:author)
    end
  end

  def temp_allow_changes
    old = @temp_allow_changes
    @temp_allow_changes = true
    yield
  ensure
    @temp_allow_changes = old
  end

  # Comments that are *only* becoming private may be updated
  def change_allowed?
    @temp_allow_changes || (changes.length == 1 && !is_public? && is_public_changed? && ticket.operable?)
  end

  # Is this even used anymore?
  # Warning: fetches object from remote store which can take 1-5+ seconds per attachment
  def create_post_on_corresponding_entry
    if is_public? && ticket.entry.present?
      post = ticket.entry.posts.create!(user: author, body: body.try(:present))

      attachments.each do |a|
        next if a.parent_id.present?
        Attachment.create!(
          uploaded_data: RestUpload.new(a.current_data, a.filename, a.content_type),
          account: account, author: author, is_public: true, source: post
        )
      end
    end
  end

  def should_add_signature?
    return false if !is_public? || ticket.new_record?
    return false unless via?(:web_form) || via?(:web_service) || via?(:closed_ticket) || via?(:linked_problem) || via?(:mobile)
    return false if (ticket.via_twitter? || ticket.via_facebook?) && should_create_channel_back_event?
    return false unless author.is_agent? && signature_template.present?

    true
  end

  def signature
    @signature ||= begin
      signature = Zendesk::Liquid::SignatureContext.render(signature_template, author, ticket.requester, 'text/plain')
      rich? ? ZendeskText::Markdown.html(signature) : signature
    end
  end

  def add_signature
    return unless should_add_signature?
    return if signature.blank?

    value <<
      if rich?
        "\n\n<div class=\"signature\">#{signature}</div>"
      else
        "\n\n#{signature}"
      end
  end

  def signature_template
    ticket.brand && ticket.brand.signature_template
  end

  def create_privacy_change_event
    ticket.add_domain_event(CommentMadePrivateProtobufEncoder.new(self).to_object)

    change = CommentPrivacyChange.new(
      audit: ticket.audit,
      ticket: ticket,
      event_id: id,
      value: is_public?,
      value_previous: is_public_was
    )
    ticket.create_silent_change_event(change)

    unless is_public?
      ticket.ticket_metric_set.replies -= 1
    end
  end

  def check_privacy
    self.is_public = false unless (ticket && ticket.allows_comment_visibility_toggle?) || author.foreign?
    true
  end

  def force_privacy
    if author_permission_restricted? || private_ticket_restricted? || private_merge_ticket?
      self.is_public = false
    end
    true
  end

  def author_permission_restricted?
    return false unless author
    author.is_light_agent? || !author.can?(:publicly, Comment)
  end

  def private_ticket_restricted?
    return false unless is_public.nil?
    !ticket.is_public && [ViaType.WEB_SERVICE, ViaType.MAIL].include?(via_id)
  end

  def private_merge_ticket?
    !ticket.is_public && audit.try(:via_id) == ViaType.MERGE
  end

  def sanitize_body
    value.strip! unless value.nil?

    # If a customer shoves a gigantic HTML blob to us via the API, we will try to process it before
    # we decide to truncate it. We could potentially process megabytes of HTML just to truncate it
    # down to 64kb. As a quick escape hatch, we truncate it to 2x our limit, then process it as
    # comment input, then the `before_create` hook will truncate the processed result down to the
    # correct size.

    if Arturo.feature_enabled_for?(:truncate_comment_html_body_before_sanitize, account)
      truncate_html(limit: max_bytesize * 2)
    end

    if value.present? && rich?
      begin
        self.value = ZendeskCommentMarkup.filter_input(
          value,
          ticket_base_url: '/tickets',
          agent_base_url:  '/users',
          from_agent:      ticket.current_user.is_agent?,
          base_url:        account.url,
          admin_allowed_protocols: ticket.account.render_custom_uri_hyperlinks
        ).to_s
      rescue StandardError => e
        raise e unless original_plain_body
        ZendeskExceptions::Logger.record(e, location: self, message: "Error sanitizing HTML comment value", fingerprint: '85a28e10a279db6ad04d00c5d5665442d3aa3bb5')
        self.value = original_plain_body
        self.format = nil
      end
    end
  end

  def save_intermediate_value
    @intermediate_value = value.to_s.dup unless @intermediate_value
  end

  def liquidize_value
    return if @liquidized
    save_intermediate_value

    ticket = self.ticket || audit.try(:ticket)
    author = self.author

    if ticket && ticket.requester && author && author.can?(:use_liquid_in, Event)
      options = {
        raw_template: @intermediate_value,
        rich_comment: account.has_simple_format_dc_in_rich_comments? && rich?
      }

      self.value = Zendesk::Liquid::TicketContext.render(value, ticket, author, !is_public?, ticket.requester.translation_locale, Mime[:text], options)
    end

    @liquidized = true
  end

  def validate_under_limit
    if ticket && ticket.comments.count >= account.settings.comments_per_ticket_limit
      errors.add(:base, I18n.t('txt.error.models.comment.ticket_at_number_of_comments_limit'))
    end
  end

  def validate_not_empty
    if value.blank? && (uploads.present? || screencasts.any?)
      errors.add(:base, I18n.t('txt.error.models.comment.please_provide_a_comment_with_your_attachment_or_screencast'))
    end
  end

  def validate_body
    # body can be nil, but only a String otherwise
    unless body.nil? || body.is_a?(String)
      errors.add(:base, I18n.t('txt.error.models.comment.comment_body_must_be_a_string'))
      false
    end
  end

  def tokens
    @tokens ||= account.upload_tokens.find_all_by_value(uploads)
  end

  def not_in_twitter_maintenance_mode
    if channel_back_requested? && account.has_twitter_maintenance?
      errors.add(:base, ::I18n.t('txt.admin.models.twitter.maintenance.tweetback'))
    end
  end

  def strip_tailing_space(value)
    value.reverse.gsub(/\A[[:space:]]+/, '').reverse!
  end

  def max_bytesize
    @max_bytesize ||= Zendesk::MtcMpqMigration::Content.max_content_size
  end

  # Truncates value html to column limit using Abbreviato to preserve valid
  # HTML structure. Adds a hidden flag indicating truncation took place.
  def truncate_html(limit: nil)
    return if value.blank?
    return unless rich?

    limit ||= max_bytesize

    if value.bytesize > limit
      Rails.logger.info "Shortening Comment html value - #{value.bytesize} bytes is too big (#{limit} max)"
      self.value, truncated_due_to_bytesize = Abbreviato.truncate(value, max_length: limit)
      ticket.add_flags!([EventFlagType.EXCEEDED_64K_SIZE]) if truncated_due_to_bytesize
      Rails.logger.info "Shortened Comment html value down to #{value.bytesize} bytes"
      if value.bytesize > limit
        Rails.logger.info "Byte size is still greater than #{limit}: #{value}"
      end
    end
  end

  def truncate
    rich? ? truncate_html : truncate_text
  end

  def max_text_bytesize
    return max_bytesize unless should_add_signature? && signature
    (max_bytesize - signature.size - 100) # adjust by 100 characters to account for 2 newlines added before signature content and extra padding
  end

  # Truncates value text to column limit and creates a hidden flag indicating
  # truncation has taken place.
  def truncate_text
    return if value.blank? || rich?

    if value.bytesize > max_text_bytesize
      Rails.logger.info "Shortening Comment plaintext value to #{max_text_bytesize} bytes"

      # Same logic as Zendesk::Extensions::TruncateAttributesToLimit
      self.value = value.mb_chars.limit(max_text_bytesize).to_s
      ticket.add_flags!([EventFlagType.EXCEEDED_64K_SIZE])
    end
  end

  def rebuild_attachment_urls_v2(doc)
    host_name = Zendesk::Routes::UrlGenerator.new(account.route || account.routes.first, account).try(:host_name_without_port)
    return unless host_name

    selector_pattern = "img[src*=\"/attachments\"]"

    doc.css(selector_pattern).each do |att_img|
      Rails.logger.info("Transforming #{att_img['src']} since the domain has changed")
      # since we can not identify the previous host mapping domain
      # we need to rebuild the path with the new host
      uri = URI(att_img['src'])
      token = uri.path.split("/").last

      # Let's skip if the attachement Url is fine
      next if account.host_names.include?(uri.host)
      # There's a chance that the img element does not belong to a zendesk attachemnt
      # let's skip unless the attachment belongs to this comment
      next unless attachment_tokens_map.include?(token)
      att_img['src'] = uri.tap { |u| u.host = host_name }.to_s
    end
  end

  def remove_attachment_base_urls(doc)
    host_name = Zendesk::Routes::UrlGenerator.new(account.route || account.routes.first, account).try(:host_name_without_port)
    return unless host_name

    base_url_pattern = /^https?:\/\/#{Regexp.escape(host_name)}/i
    doc.css("img[src*=\"://#{host_name}/attachments\"]").each do |attachment_img|
      attachment_img['src'] = attachment_img['src'].sub(base_url_pattern, '')
    end
  end

  def suppress_unicode_replacement
    return unless account.has_suppress_unicode_replacement?
    value.tr!("\b\x1C\x1D\x1E\x1F\x0F", '')
  end

  def attachment_tokens_map
    @attachment_tokens_map ||= (
      attachments |
      Attachment.joins("INNER JOIN tokens ON attachments.source_id = tokens.id AND attachments.account_id = tokens.account_id").
        where("tokens.target_id = ?", author.id)
    ).map(&:token)
  end

  class << self
    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[classic comment])
    end
  end
end
