class LogMeInTranscript < AuditEvent
  include Zendesk::Serialization::LogMeInTranscriptSerialization

  CHAT_TRANSCRIPT_REGEX = /Chat transcript begins:\n(?<chat_log>(.|\n)*)\nChat transcript ends/i.freeze
  ELLIPSIS = '...'.freeze

  before_create :truncate_body

  # The events table has a few fields that are overloaded as needed in
  # subclasses. Here we're using those fields for our own attributes.

  # Aliases for consistency. All events use these attribute names.
  alias_attribute :body, :value

  attr_accessible :body, :via_id, :via_reference_id, :value, :value_previous, :value_reference, :is_public

  TIME_TRACKING_KEYS = [["WaitingTime", "Wait time (seconds)"], ["WorkTime", "Work time (seconds)"]].freeze
  TIMESTAMP_KEYS = [["PickupTime", "Pickup time (UTC)"], ["ClosingTime", "Closing time (UTC)"], ["LastActionTime", "Last action time (UTC)"]].freeze
  CUSTOM_FIELD_KEYS = (0..7).map { |i| ["CField#{i}", "CField#{i}"] }
  SURVEY_KEYS = (0..9).map { |i| ["TSurvey#{i}", "TSurvey#{i}"] }
  OTHER_KEYS = [["SessionID", "Session ID"], ["TechName", "Technician name"], ["TechEmail", "Technician email"], ["Note", "Technician notes"], ["Platform", "Platform"]].freeze

  # NOTE: The reason for `value_reference` as a fallback is older `LogMeInTranscript` data used
  # `value_reference` to store it's data. #36543 introduced a new change where the data is stored
  # at `value` column instead.
  def chat_transcript
    value || value_reference
  end

  def self.build_comment_from_params(params)
    result = [
      "A LogMeIn Rescue session has completed.",
      collect_other_fields(params),
      collect_time_tracking_keys(params),
      collect_timestamps(params),
      wrap_chat_log(params["ChatLog"])
    ]

    result.join("\n\n")
  end

  def self.build_from_params(params)
    new(body: build_body_from_params(params))
  end

  def self.build_body_from_params(params)
    result = [
      collect_other_fields(params),
      collect_time_tracking_keys(params),
      collect_timestamps(params),
      wrap_chat_log(params["ChatLog"]),
      collect_custom_fields(params),
      collect_survey_fields(params)
    ]

    result.join("\n\n")
  end

  def self.collect_time_tracking_keys(params)
    collect_params(params, TIME_TRACKING_KEYS)
  end

  def self.collect_timestamps(params)
    collect_params(params, TIMESTAMP_KEYS) do |timestamp|
      begin
        timestamp.blank? ? "" : DateTime.parse(timestamp).strftime("%A, %B %d, %Y at %I:%M %p")
      rescue ArgumentError => e
        error_message = "Invalid LogMeIn Rescue timestamp: #{timestamp}"
        ZendeskExceptions::Logger.record(e, location: self, message: error_message, fingerprint: '6a2884a31304b7595ee82b7d44384056fdece45a')
        timestamp
      end
    end
  end

  def self.collect_custom_fields(params)
    collect_present_params(params, CUSTOM_FIELD_KEYS)
  end

  def self.collect_survey_fields(params)
    collect_present_params(params, SURVEY_KEYS)
  end

  def self.collect_other_fields(params)
    collect_params(params, OTHER_KEYS)
  end

  def self.collect_params(params, keys)
    result = keys.map do |key|
      value = params[key.first]
      value = yield value if block_given?
      sprintf("%-25s %s", "#{key.last}:", value)
    end

    result.compact.join("\n")
  end

  def self.collect_present_params(params, keys)
    result = keys.map do |key|
      [key.last, params[key.first]].join(": ") if params[key.first].present?
    end

    result.compact.join("\n")
  end

  def self.wrap_chat_log(chatlog)
    return unless chatlog.present?
    "Chat transcript begins:\n#{chatlog.strip}\nChat transcript ends"
  end

  private

  def max_content_size
    @max_content_size ||= Zendesk::Extensions::TruncateAttributesToLimit.limit(LogMeInTranscript.columns_hash.fetch('value'))
  end

  def truncate_body
    return if body.blank? || body.bytesize <= max_content_size

    result = body.split("\n\n")

    result = result.map do |part|
      part, truncated = truncate_chat_log(part, body.bytesize - max_content_size)
      part = LogMeInTranscript.wrap_chat_log(part) if truncated
      part
    end

    self.body = result.join("\n\n")
  end

  def truncate_chat_log(str, size)
    return [str, false] unless match = str.match(CHAT_TRANSCRIPT_REGEX)
    return [str, false] if str.bytesize < size

    chat_log = match.named_captures["chat_log"]
    size = chat_log.bytesize - size - ELLIPSIS.bytesize
    str = str.mb_chars.compose.to_s
    new_str = str.byteslice(0, size)

    # After doing the byteslice, there's no guarantee that end of the string is a valid utf-8 character. There's
    # a possiblity that the slice happened in the middle of a character. The following is needed to make sure
    # that the string is valid before saving it.
    new_str.chop! until new_str[-1].force_encoding('utf-8').valid_encoding?

    [new_str + ELLIPSIS, true]
  end
end
