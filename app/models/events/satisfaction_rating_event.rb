class SatisfactionRatingEvent < AuditEvent
  alias_attribute :comment,     :value
  alias_attribute :score,       :value_reference
  alias_attribute :assignee_id, :value_previous

  attr_accessible :comment, :score, :assignee_id, :value, :value_previous, :value_reference
end
