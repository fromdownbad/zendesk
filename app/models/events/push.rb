# These are added when the proxy pushes something to an external service
# Events pushed from external sources via the proxy controller
class Push < AuditEvent
  include Zendesk::Serialization::PushSerialization

  attr_accessor :resource_attributes

  validates_presence_of :value_reference
  alias_attribute       :resource, :value

  attr_accessible :value_reference, :resource_attributes, :via_reference_id, :value, :via_id, :value_previous

  before_validation :cache_rendered_resource, if: :resource_attributes?, on: :create
  before_save :remove_javascript_from_value_and_value_reference

  def value_reference
    (read_attribute(:value_reference) || '').html_safe
  end

  def to_s
    (value || 'Empty event').html_safe
  end

  protected

  def resource_attributes?
    resource_attributes.present?
  end

  def cache_rendered_resource
    self.value = render_resource
  end

  def render_resource
    result = ''.html_safe

    resource_attributes.each do |key, value|
      if key.to_s.casecmp('location').zero?
        if value.present?
          result << "<li><strong>Location</strong>: <a href='#{ERB::Util.h(value)}' target='_blank'>#{ERB::Util.h(value)}</a></li>".html_safe
        end
      else
        result << "<li><strong>#{ERB::Util.h(key)}</strong>: #{ERB::Util.h(value)}</li>".html_safe
      end
    end

    result
  end

  def remove_javascript_from_value_and_value_reference
    return if frozen?
    [:value, :value_reference].each do |field|
      value = send(field)
      send("#{field}=", value.sanitize) if value.is_a?(String)
    end
  end
end
