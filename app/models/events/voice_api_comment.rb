class VoiceApiComment < VoiceComment
  include CachingObserver

  validate :presence_of_data,
    :length_of_recording_url,
    :format_of_started_at,
    :format_of_answered_by_id,
    :format_of_call_duration,
    :length_of_location

  before_save :assign_value

  def assign_value
    if author = account.users.find_by_id(data[:author_id])
      self.author_id = author.id
    end

    self.value = body
    self.is_public = data[:public] if data.with_indifferent_access.key?(:public)
    self.via_id = data[:via_id]

    data[:transcription_status] = "completed"
  end

  def recording_url
    data[:recording_url]
  end

  def player_recording_urls
    if /\.mp3|\.wav$/.match?(recording_url)
      [recording_url]
    else
      super
    end
  end

  def transcription
    if render_transcription?
      data[:transcription_text]
    else
      ::I18n.t("event.voice.transcription.not_available")
    end
  end

  def call_duration
    data[:call_duration].to_i
  end

  def answered_by_id
    data[:answered_by_id].to_i
  end

  def recording_duration
    call_duration
  end

  private

  def started_at
    return nil if data.blank? || data[:started_at].blank?
    format_of_started_at
  end

  def length_of_recording_url
    unless data && data[:recording_url].to_s.length <= 2000
      errors.add(:recording_url, "is too long")
    end
  end

  def format_of_answered_by_id
    if data[:answered_by_id].present? && !valid_integer?(data[:answered_by_id])
      errors.add(:answered_by_id, "is not an integer")
    end
  end

  def format_of_call_duration
    unless data && valid_integer?(data[:call_duration])
      errors.add(:call_duration, "is not an integer")
    end
  end

  def format_of_started_at
    if data
      Time.parse(data[:started_at]) rescue errors.add(:started_at, "is an invalid timestamp")
    end
  end

  def length_of_location
    if data && data[:location].to_s.length > 255
      errors.add(:location, "is too long")
    end
  end

  def presence_of_data
    if data.blank?
      errors.add(:data, "can't be blank")
    else
      errors.add(:from, :blank) if data[:from].blank?
      errors.add(:to, :blank) if data[:to].blank?
      errors.add(:call_duration, :blank) if data[:call_duration].blank?
      errors.add(:started_at, :blank) if data[:started_at].blank?
    end
  end

  def valid_phone_number?(number)
    number && number =~ Voice::Core::NumberSupport::E164Number::E164_REGEX_WITH_EXT
  end

  def valid_integer?(value)
    value && value.to_s =~ /\A\d+\Z/
  end
end
