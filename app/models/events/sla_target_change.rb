class SlaTargetChange < AuditEvent
  fixed_serialize :value
  fixed_serialize :value_previous

  attr_accessible :target_info

  alias_attribute :initial_target, :value_previous
  alias_attribute :metric_id, :value_reference

  class << self
    def audit_changes(ticket, initial_metrics, new_metrics, initial_policy, new_policy)
      (initial_metrics.keys + new_metrics.keys).uniq.each do |metric_id|
        audit_change(
          ticket,
          initial_metrics[metric_id],
          new_metrics[metric_id],
          metric_id,
          initial_policy,
          new_policy
        )
      end
    end

    private

    def audit_change(ticket, initial_metric, new_metric, metric_id, initial_policy, new_policy)
      # If policy has changed, audit change.
      # If policy hasn't changed but targets changed, audit change.
      # If policy and targets haven't changed, but business hours flag doesn't match, audit change.
      # Otherwise, don't audit change.
      if initial_policy.try(:id) != new_policy.try(:id) || initial_metric.try(:target) != new_metric.try(:target) || initial_metric.try(:business_hours?) != new_metric.try(:business_hours?)
        if initial_metric.present?
          initial_target = {minutes: initial_metric.target, in_business_hours: initial_metric.business_hours?}
        end
        if new_metric.present?
          final_target = {minutes: new_metric.target, in_business_hours: new_metric.business_hours?}
        end

        ticket.events << new(
          ticket:      ticket,
          audit:       ticket.audit,
          target_info: {
            initial_target:     initial_target,
            final_target:       final_target,
            metric_id:          metric_id,
            current_sla_policy: new_policy
          }
        )
      end
    end
  end

  def target_info=(new_info) # { :initial_target => integer, :final_target => integer, :metric_id => integer, :current_sla_policy => Sla::Policy}
    self.value = {
      final_target:       new_info[:final_target],
      current_sla_policy: new_info[:current_sla_policy].try(:title)
    }

    self.initial_target = new_info[:initial_target]
    self.metric_id      = new_info[:metric_id]
  end

  def final_target
    value[:final_target]
  end

  def current_sla_policy
    value[:current_sla_policy]
  end
end
