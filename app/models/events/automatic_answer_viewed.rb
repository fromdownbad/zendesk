class AutomaticAnswerViewed < AuditEvent
  fixed_serialize :value, Hash

  before_create :set_type_and_is_public
  validates :article, presence: true
  validates :viewer_id, presence: true
  validate  :article_should_have_required_fields

  ARTICLE_REQUIRED_FIELDS = [:title, :url, :id, :html_url].freeze

  def article=(article)
    value[:article] = article
  end

  def article
    value[:article] || {}
  end

  def viewer_id=(viewer_id)
    value[:viewer_id] = viewer_id
  end

  def viewer_id
    value[:viewer_id]
  end

  def body
    value
  end

  private

  def set_type_and_is_public
    self.type = 'AutomaticAnswerViewed'
    self.is_public = false
    true
  end

  def article_should_have_required_fields
    ARTICLE_REQUIRED_FIELDS.each do |field|
      errors.add(:article, " does not have the required one of the required keys {id, url, title, html_url}") unless article.key? field
    end
  end
end
