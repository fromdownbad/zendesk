class TicketUnshareEvent < AuditEvent
  include Zendesk::Serialization::TicketSharingEventSerialization
  include BaseTicketSharingEvent

  attr_accessible :agreement_id

  after_create :delete_shared_ticket

  protected

  def delete_shared_ticket
    shared_ticket = ticket.shared_tickets.where(agreement_id: agreement_id).first
    return unless shared_ticket
    shared_ticket.soft_delete!
  end
end
