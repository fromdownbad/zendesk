class ChannelBackFailedEvent < AuditEvent
  attr_accessible :value
  fixed_serialize :value
end
