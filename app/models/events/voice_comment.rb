class VoiceComment < Comment
  class NullAgent
    def clean_name
      ::I18n.t("event.voice.details.unknown_agent")
    end

    def name
      nil
    end
  end

  include Zendesk::Serialization::VoiceCommentSerialization
  include Voice::Core::NumberSupport
  include TimeDateHelper

  fixed_serialize   :value_previous
  alias_attribute   :data, :value_previous

  has_many :attachments,
    -> { where(parent_id: nil).order(id: :asc) },
    as: :source,
    dependent: :destroy,
    inherit: :account_id

  has_one :shared_comment, foreign_key: :comment_id

  attr_accessible :add_short_url, :author_id, :channel_back, :created_at, :is_public, :uploads, :value,
    :value_previous, :value_reference, :via_id, :via_reference_id, :data, :body

  FORMAT            = "mp3".freeze
  SUPPORTED_FORMATS = ["mp3", "wav"].freeze
  VOICE_VIA_TYPES   = [ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND,
                       ViaType.VOICEMAIL, ViaType.API_PHONE_CALL_INBOUND,
                       ViaType.API_PHONE_CALL_OUTBOUND, ViaType.API_VOICEMAIL].freeze
  FIELDS_FOR_REDACTION = [:to, :from, :location, :transcription_text, :answered_by_name].freeze
  PHONE_LINE_TYPE = 'phone'.freeze
  DIGITAL_LINE_TYPE = 'digital'.freeze
  LINE_TYPES = [PHONE_LINE_TYPE, DIGITAL_LINE_TYPE].freeze

  # Require VoiceApiComment, because has_many includes only known/loaded subtypes in a query
  #
  # E.g.
  #   Ticket.last.voice_comment # =>
  #     SELECT `events`.* FROM `events` WHERE `events`.`type` IN ('VoiceComment')
  #
  require_dependency 'events/voice_api_comment'

  def self.player_recording_urls(base_url)
    VoiceComment::SUPPORTED_FORMATS.map { |fmt| "#{base_url}.#{fmt}" }
  end

  %w[from to].each do |key|
    class_eval %(
      def #{key}
        data[:#{key}]
      end
    ), __FILE__, __LINE__
  end

  def empty?
    false
  end

  def body
    # If an account changes their subdomain after this comment was created the recording_url will break
    # this is why we need to rebuild summary_body each time body is called
    if value_previous_hash[:merged]
      [value.blank? ? display_subject : value, summary_body].join
    else
      [display_subject, summary_body].join
    end
  end

  def value_previous_hash
    # In some cases the value is not a Hash, can be a String when the comment
    # is scrubbed or deleted. More details on
    # https://zendesk.atlassian.net/browse/VOICE-5581
    # https://github.com/zendesk/zendesk/pull/24628
    @value_previous_hash ||= value_previous.is_a?(Hash) ? value_previous : {}
  end

  def data
    @data ||= begin
      result = value_previous_hash

      if result[:recording_url]
        result[:recording_url].sub!(%r{^http://api.twilio.com}, 'https://api.twilio.com')
      end

      result[:answered_by_name] = answered_by if answered_blank?(result)

      result
    end
  end

  def redact!(text, _ = false)
    temp_allow_changes do
      unformatted_text = Zendesk::StrippedPhoneNumber.strip(formatted_number: text)
      FIELDS_FOR_REDACTION.each do |field|
        text_for_redaction = field.in?([:to, :from]) ? unformatted_text : text

        if redacted = Zendesk::TextRedaction.redact(value_previous_hash[field], text_for_redaction)
          value_previous_hash[field] = redacted
        end
      end

      save!
    end
  end

  def formatted_from
    @formatted_from ||= render_phone_number(call_from, inline: false)
  end

  def formatted_to
    @formatted_to ||= digital_call? ? call_to : render_phone_number(call_to, inline: false)
  end

  def formatted_started_at
    started_at
  end

  def via_voice?
    VOICE_VIA_TYPES.include?(via_id)
  end

  def via_phone?
    [ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND].include?(via_id)
  end

  def to_s
    body.to_s.lines.first.to_s.strip
  end

  def display_subject
    return ticket_subject unless via_voice?

    translated_subject
  end

  def translated_subject
    ::I18n.t("event.voice.subject.via_id.#{via_id}", from: formatted_from, to: formatted_to)
  end

  def header_label
    ::I18n.t("event.voice.details.header")
  end

  def render_location?
    data[:location].present?
  end

  def location_label
    ::I18n.t("event.voice.details.location")
  end

  def location
    data[:location]
  end

  def render_transcription?
    return false if via_phone?
    !data[:transcription_text].blank?
  end
  alias_method :transcription_visible, :render_transcription?

  def transcription_label
    ::I18n.t("event.voice.transcription.header")
  end

  def transcription
    if transcription_completed?
      data[:transcription_text]
    else
      ::I18n.t("event.voice.transcription.not_available")
    end
  end

  def transcription_completed?
    data[:transcription_status] == "completed"
  end

  def answered_by_label
    ::I18n.t("event.voice.details.answeredBy")
  end

  def called_by_label
    ::I18n.t("event.voice.details.calledBy")
  end

  def answered_or_called_by_label
    [ViaType.PHONE_CALL_OUTBOUND, ViaType.API_PHONE_CALL_OUTBOUND].include?(via_id) ? called_by_label : answered_by_label
  end

  def answered_by_id
    value_previous_hash[:answered_by_id]
  end

  def answered_by
    @answered_by ||= agent.clean_name
  end

  def answered_by_name
    data[:answered_by_name]
  end

  def called_by
    answered_by if phone_call_outbound?
  end

  def call_from_label
    ::I18n.t("event.voice.details.from")
  end

  def call_from
    data[:from]
  end

  def call_to_label
    ::I18n.t("event.voice.details.to")
  end

  def call_to
    data[:to]
  end

  def time_of_call_label
    ::I18n.t("event.voice.details.time")
  end

  def time_of_call
    formatted_started_at
  end

  def call_duration_label
    ::I18n.t("event.voice.details.length")
  end

  def recording_url_label
    ::I18n.t("event.voice.details.listen")
  end

  def recording_url
    if recording.sid.nil?
      recording_url_with_fallback
    else
      secure_recording_url([account.url, "v2", "recordings", data[:call_id], recording.sid].join("/"))
    end
  end

  def recording
    @recording ||= Recording.new(recording_url_with_fallback)
  end

  def internal_recording_url(format = VoiceComment::FORMAT)
    "#{recording_url_with_fallback}.#{format}"
  end

  def player_recording_urls
    VoiceComment.player_recording_urls(recording_url_with_fallback)
  end

  def recording_duration
    duration = data[:recording_duration]
    duration.blank? ? (call && call.recording_duration) : duration
  end

  def call_duration
    data[:call_duration].to_i
  end

  def call
    account.calls.find_by_id(data[:call_id])
  end

  # Build an absolute url from a relative url.
  #
  # Returns an absolute url to a recording.
  def absolute_recording_url
    return "" if recording_url.blank?

    if internal_comment? && !URI(recording_url).absolute?
      "#{account.url(ssl: true, mapped: false)}#{recording_url}"
    else
      recording_url
    end
  end

  # Used in AuditEventCollectionPresenter#preload_voice_comment_data_answered_by_users
  def agent=(v)
    # To avoid other classes to know NullAgent we assign it here when the given value
    # is nil
    @agent = v || NullAgent.new
  end

  def agent
    # @agent may be already set when preloading data. If not set, we set it here.
    return @agent if @agent

    user_id = value_previous_hash[:answered_by_id]
    self.agent = user_id && account.users.find_by_id(user_id)

    # Is user is nul self.agent = nil will return nil, but we want to return
    # the content of @agent (NullAgent in that case)
    @agent
  end

  protected

  def markdown_via?
    super || via_voice?
  end

  private

  # Check if a comment was created by Zendesk.
  #
  # Returns true if a comment was created by Zendesk, false otherwise.
  def internal_comment?
    data[:call_id].present?
  end

  def started_at
    data[:started_at] # Localized and formatted already
  end

  def ticket_subject
    # Using the .ticket association, ticket is returned as nil when a ticket is archived
    account.tickets.find_by_id(ticket_id).try(:subject) || ""
  end

  def summary_body
    summary = "\n#{header_label}:\n\n"
    summary << "#{call_from_label} #{formatted_from}\n"
    summary << "#{call_to_label} #{formatted_to}\n"
    summary << "#{time_of_call_label} #{time_of_call}\n"
    summary << "#{location_label} #{location}\n" if render_location?
    summary << answered_or_called_by if answered_by_name.present?
    summary << "#{call_duration_label} #{format_time_interval(call_duration)}\n" if call_duration
    summary << "#{recording_consent_action_label} #{recording_consent_action}\n" unless recording_consent_action.nil?
    summary << "#{recording_url_label} #{absolute_recording_url}\n" unless absolute_recording_url.blank?
    summary << "\n#{transcription_label}:\n\n#{transcription}\n" if render_transcription?

    summary
  end

  # ZD#241615
  def recording_url_with_fallback
    url = data[:recording_url]
    secure_recording_url(url.blank? ? (call && call.recording_url) : url)
  end

  def secure_recording_url(url)
    if url
      url = url.sub(%r{^http://api.twilio.com}, 'https://api.twilio.com')
    end
    url
  end

  def answered_blank?(result)
    result.include?(:answered_by_id) && result[:answered_by_name].nil?
  end

  def answered_or_called_by
    "#{answered_or_called_by_label} #{answered_by_name}\n"
  end

  def recording_consent_action_label
    ::I18n.t("event.voice.details.recording_consent_action_label")
  end

  def recording_consent_action
    case data[:recording_consent_action]
    when 'caller_opted_out'
      ::I18n.t('event.voice.details.caller_opted_out')
    when 'caller_opted_in'
      ::I18n.t('event.voice.details.caller_opted_in')
    when 'caller_did_not_opt_out'
      ::I18n.t('event.voice.details.caller_did_not_opt_out')
    when 'caller_did_not_opt_in'
      ::I18n.t('event.voice.details.caller_did_not_opt_in')
    end
  end

  def digital_call?
    data[:line_type] == DIGITAL_LINE_TYPE
  end
end
