class ExternalChangeEvent < AuditEvent
  fixed_serialize :value_reference

  attr_accessible :value, :value_previous, :value_reference

  def self.build_from_values(old_value, new_value)
    return nil unless old_value || new_value # no change

    value = (new_value ? new_value.value : nil)
    value_prev = (old_value ? old_value.value : nil)
    # NOTE: For dropdowns value comes in as Integer or String
    return nil if value.to_s == value_prev.to_s # no change

    ref_obj = (old_value || new_value)
    hash = {object_id: ref_obj.id, object_type: ref_obj.class.name}
    hash.merge!(ref_obj.info_for_external_change_event)

    new(value: value, value_previous: value_prev, value_reference: hash)
  end

  def action_type
    return @action_type if @action_type

    @action_type = if value && value_previous
      "Change"
    elsif value ||
           (field.two_state? && value_previous)
      "Set"
    elsif value_previous
      "Clear"
    else
      "Undefined"
    end
  end
end
