# A notification sent to agents following a ticket
class FollowerNotification < Notification
  include Zendesk::Serialization::CcSerialization

  def body
    audit.comment
  end

  protected

  def deliver_notifications
    Rails.logger.info("Delivering notifications for event #{id} at #{Time.now}")

    localize do
      # In the "Agent as End User" feature, an agent that creates a ticket in
      # Help Center is treated as an end user
      agents = if Arturo.feature_enabled_for?(:agent_as_end_user, account)
        valid_to_recipients.find_all do |recipient|
          recipient.is_agent? &&
          !ticket.agent_as_end_user_for?(recipient)
        end
      else
        valid_to_recipients.find_all(&:is_agent?)
      end

      agents.each_slice(LEGACY_DELIVERY_CHUNK_LIMIT) do |recipients|
        deliver(recipients, true)
      end
    end
  end

  def deliver(recipients, for_agent)
    EventsMailer.deliver_follower_notification(
      ticket,
      recipients,
      for_agent,
      self,
      id
    )
  end
end
