class CollabThreadEvent < AuditEvent
  fixed_serialize :value, Hash

  alias_attribute :thread_id, :value_reference

  validates :subject, presence: true
  validates :thread_id, presence: true
  before_create :set_to_private

  def subject=(subject)
    value[:subject] = subject
  end

  def subject
    value[:subject]
  end

  def recipient_count=(recipient_count)
    value[:recipient_count] = recipient_count
  end

  def recipient_count
    value[:recipient_count]
  end

  def sc_type=(sc_type)
    value[:sc_type] = sc_type
  end

  def sc_type
    value[:sc_type]
  end

  def identifier=(identifier)
    value[:identifier] = identifier
  end

  def identifier
    value[:identifier]
  end

  private

  def set_to_private
    self.is_public = false
    true
  end
end
