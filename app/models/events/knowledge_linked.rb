class KnowledgeLinked < AuditEvent
  fixed_serialize :value, Hash

  before_save :set_type_and_is_public

  validates :article, presence: true
  validates :article_id, presence: true
  validate  :article_should_have_required_fields

  alias_attribute :article, :value
  alias_attribute :article_id, :value_previous

  attr_accessible :article

  ARTICLE_REQUIRED_FIELDS = [:title, :url, :id, :html_url, :locale].freeze

  def initialize(options = {})
    super
    self.article = options[:article]
  end

  def article=(article)
    self.value = article
    self.value_previous = article[:id] rescue nil
  end

  alias_method :body, :article

  private

  def set_type_and_is_public
    self.is_public = false
    self.type = 'KnowledgeLinked'
  end

  def article_should_have_required_fields
    ARTICLE_REQUIRED_FIELDS.each do |field|
      errors.add(:article, " does not have the required key '#{field}'") unless article.key?(field)
    end
  end
end
