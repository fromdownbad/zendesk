class CommentRedactionEvent < AuditEvent
  belongs_to :comment, foreign_key: :value, class_name: 'Comment'

  attr_accessible :comment_id
  before_validation :inherit_audit_from_comment
  before_validation :set_value_reference

  alias_attribute :comment_id, :value

  protected

  def set_value_reference
    self.value_reference = "comment_id"
  end

  def inherit_audit_from_comment
    self.audit ||= comment.audit
  end
end
