# DEPRECATED. This remains for legacy Facebook event support. See ChannelBackEvent for current version.

class FacebookEvent < AuditEvent
  alias_attribute :graph_object_id, :value_previous
  delegate :communication, to: :ticket
  delegate :page, to: :communication

  attr_accessible :via_reference_id, :value, :is_public, :via_id, :graph_object_id

  def to_s
    value
  end
end
