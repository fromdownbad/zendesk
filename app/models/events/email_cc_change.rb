class EmailCcChange < CollaborationChange
  alias_attribute :current_email_ccs,  :value
  alias_attribute :previous_email_ccs, :value_previous

  before_create :initialize_value_reference

  fixed_serialize :value, Array
  fixed_serialize :value_previous, Array

  def self.current_addresses(account, collaborations)
    super(account, collaborations, :email_cc?)
  end

  def initialize_value_reference
    self.value_reference = 'current_email_ccs'
    true
  end
end
