class SmsNotification < AuditEvent
  include Zendesk::Serialization::SmsNotificationSerialization

  alias_attribute :recipients, :value
  alias_attribute :phone_number_id, :value_previous
  alias_attribute :body, :value_reference

  attr_accessible :value, :recipients, :body, :value_previous, :value_reference

  fixed_serialize :value, Array
  truncate_attributes_to_limit :value_reference # value_reference is 8k, but rule actions can have up to 65k of text
  validates_presence_of :recipients

  after_commit :enqueue_sms_notification, on: :create

  protected

  def enqueue_sms_notification
    return if body.blank? || !sms_enabled?

    recipients.each do |user_id|
      user = account.users.find(user_id)

      liquidized_body_value = liquidized_body(user)
      next if liquidized_body_value.blank?

      ::Sms::NotificationJob.enqueue(account_id,
        phone_number_id: phone_number_id,
        user_id: user_id,
        body: liquidized_body_value,
        trigger_id: via_reference_id,
        ticket_id: ticket_id,
        event_id: id)
    end
  end

  private

  def sms_enabled?
    @sms_enabled ||= account.voice_feature_enabled?(:sms)
  end

  def liquidized_body(user)
    Zendesk::Liquid::TicketContext.render(body, ticket, author, user.is_agent?, user.translation_locale)
  end
end
