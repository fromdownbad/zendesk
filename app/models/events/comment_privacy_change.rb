class CommentPrivacyChange < AuditEvent
  validates_presence_of :value_reference
  validate :valid_event_reference

  alias_attribute :event_id, :value_reference

  attr_accessible :event_id, :value, :value_previous, :value_reference

  def to_s(_options = {})
    I18n.t('txt.models.events.comment_privacy_change.comment_made_' + changed_to.to_s.chomp, comment: comment.body.truncate(100)).to_s
  end

  def comment
    ticket.comments.find_by_id(event_id)
  end

  def changed_to
    if [false, '0', 'f'].include?(value)
      'private'
    else
      'public'
    end
  end

  private

  def valid_event_reference
    unless ticket.events.exists?(id: event_id)
      errors.add(:event_id, :invalid)
    end
  end
end
