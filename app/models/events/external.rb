# When a target notifies an external service
class External < AuditEvent
  include Zendesk::Serialization::ExternalSerialization

  belongs_to :target, foreign_key: :value, inherit: :account_id

  validates_presence_of :value
  fixed_serialize :value

  alias_attribute :success, :value_previous
  alias_attribute :body, :value_reference
  alias_attribute :resource, :value

  attr_accessible :via_id, :via_reference_id, :resource, :body, :value_reference, :success, :value, :value_previous

  serialize :body

  after_commit :enqueue_target, on: :create

  def to_s
    target ? target.title : "Invalid target: #{value}"
  end

  def body
    if requires_deserialization? && value_reference.present?
      YAML.load(value_reference)
    else
      value_reference
    end
  end

  def requires_deserialization?
    target.is_a?(UrlTargetV2) && target.uses_parameters?
  end

  def enqueue_target
    if target
      TargetJob.enqueue(account_id, id) if target.valid? && target.is_active
    else
      # https://support.zendesk.com/agent/tickets/2054747
      # Run TriggerCleanupJob to remove target id when the target was deleted already.
      TriggerCleanupJob.enqueue(account_id, via_reference_id, value)
    end
  end
end
