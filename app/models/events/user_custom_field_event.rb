class UserCustomFieldEvent < ExternalChangeEvent
  def to_s
    s = ""
    return s unless owner

    owner_type_i18n = I18n.t('txt.header.new_%s' % owner_type.downcase).downcase
    ec_hash = {
      field: field_name,
      owner_type: owner_type_i18n,
      owner: owner_name
    }

    if action_type == "Change"
      I18n.t('txt.lib.zendesk.fom.field.custom_field_value_changed', ec_hash.merge(value_previous: render(value_previous), value: render(value)))
    elsif action_type == "Set"
      I18n.t('txt.lib.zendesk.fom.field.custom_field_value_set_to', ec_hash.merge(value: render(value)))
    elsif action_type == "Clear"
      I18n.t('txt.lib.zendesk.fom.field.custom_field_value_reset', ec_hash.merge(value_previous: render(value_previous)))
    end
  end

  def field_name
    field ? field.title : nil
  end

  def owner_type
    field ? field.owner : nil
  end

  def owner_name
    owner.to_s
  end

  def field
    return @field if @field

    ref = value_reference
    begin
      cfv = ref[:object_type].constantize.where(id: ref[:object_id], account_id: account.id).first
      return @field = cfv.field if cfv
    rescue # ActiveRecord::RecordNotFound
    end

    begin
      @field = CustomField::Field.where(id: ref[:info][:cf_field_id], account_id: account.id).first
    rescue # ActiveRecord::RecordNotFound
    end

    @field
  end

  def owner
    return @owner if @owner || !field

    ref = value_reference
    @owner = field.owner.constantize.find(ref[:info][:owner_id])
  end

  def render(value)
    field ? field.value_as_json(value) : ""
  end
end
