class Error < AuditEvent
  include Zendesk::Serialization::ErrorSerialization

  validates_presence_of :value

  attr_accessible :value, :via_id, :via_reference_id, :value_previous, :value_reference

  def to_s(*_args)
    "Error: #{value.blank? ? 'blank' : value}"
  end
end
