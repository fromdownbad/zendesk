# Like a Change, but without a previous value (only used on ticket creation)
class Create < AuditEvent
  include Zendesk::Serialization::CreateSerialization

  alias_attribute :field_name, :value_reference
  validates_presence_of :value_reference
  truncate_attributes_to_limit :value # audited columns might contain more than it can hold

  attr_accessible :transaction, :value, :value_previous, :value_reference, :field_name, :via_id, :via_reference_id

  def value_previous
    @value_previous = nil
  end

  def to_s(options = {})
    field = value_reference.present? && Zendesk::Fom::Field.for(value_reference.downcase)
    field ? field.event_to_s(self, options).to_s : 'Unrecognized create reference!'
  end
end
