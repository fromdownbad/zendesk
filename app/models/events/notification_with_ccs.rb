class NotificationWithCcs < Notification
  attr_accessor :ccs, :to_recipient_ids

  def initialize(options)
    @to_recipient_ids = options[:to_recipient_ids] || []
    @ccs              = options[:ccs] || []

    options.except!(:to_recipient_ids, :ccs)

    super(recipients: Array(to_recipient_ids + ccs.map(&:id)), **options)

    self.to_recipient_ids = to_recipient_ids
    self.ccs = ccs
  end

  protected

  def deliver_notifications
    Rails.logger.info("Delivering notifications for event #{id} at #{Time.now}")

    localize { deliver(valid_to_recipients) }
  end

  # rubocop:disable Lint/UnusedMethodArgument
  def deliver(recipients, for_agent = false)
    EventsMailer.deliver_rule(self, ticket, recipients, false, ccs)
  end
  # rubocop:enable Lint/UnusedMethodArgument
end
