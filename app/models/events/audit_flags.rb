class AuditFlags < Set
  def options
    @options ||= HashWithIndifferentAccess.new
  end

  def trusted?
    untrusted_flags.none?
  end

  def allowed_to_comment?
    any? { |flag| EventFlagType.allowed_to_comment?(flag) }
  end

  def untrusted_flags
    reject { |flag| EventFlagType.trusted?(flag) }
  end

  def merge_flags(flag_types, flags_options)
    merge(flag_types)

    # need a new hash so we don't modify the flags_options passed in
    new_options = {}

    flag_types.each_with_object(flags_options) do |flag, options|
      new_options[flag] = (options[flag] || {}).merge(trusted: EventFlagType.trusted?(flag))
    end

    options.merge!(new_options)
  end
end
