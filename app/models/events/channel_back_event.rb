class ChannelBackEvent < AuditEvent
  attr_accessible :value, :source_id, :via_id, :append_ticket_url, :new_thread

  fixed_serialize :value

  alias_attribute :source_id, :via_reference_id
  alias_attribute :new_thread, :value_previous
  alias_attribute :append_ticket_url, :value_reference

  delegate :comment, to: :audit

  after_commit :initiate_channels_conversion, on: :create

  belongs_to :audit, foreign_key: :parent_id, inherit: :account_id
  belongs_to :ticket, inherit: :account_id

  def initiate_channels_conversion
    account.on_shard do
      ::Channels::Converter::ReplyInitiator.new(reply_params).reply
    end
  end

  def reply_params
    attachments = comment.try(:attachments) || []
    {
      account_id: account_id,
      comment_id: comment.try(:id),
      ticket_id: ticket_id,
      source_id: source_id,
      text: value[:text],
      ticket_url: ticket_url,
      new_thread: new_thread,
      recipient_ids: recipient_ids,
      attachments: attachments.select { |a| !a.thumbnail? }.map(&:url)
    }
  end

  # TODO: Extract into common place
  def channels_prefix(postfix, *args)
    return send("twitter_" + postfix.to_s, *args)  if ticket.via_twitter?
    return send("facebook_" + postfix.to_s, *args) if ticket.via_facebook?
    return send("any_channel_" + postfix.to_s, *args) if ticket.via_any_channel?

    fail "Non-channels tickets should never reach here. Ticket ID:#{id}, Account ID:#{account_id}"
  end

  def to_s
    channels_prefix(:text)
  end

  def facebook_text
    resource_type = ticket.via?(:facebook_post) ? "post" : "message"

    communication_href = "/channels/resources?external_id=#{value[:external_id]}&resource_type=facebook_#{resource_type}"
    page_link          = "<a href='https://www.facebook.com/#{value[:source][:external_id]}' target='blank'>#{value[:source][:name]}</a>"

    I18n.t(
      "txt.facebook_integration.facebook_event.#{resource_type}",
      communicationHref: communication_href,
      pageLink: page_link
    )
  end

  def twitter_text
    I18n.t(
      "txt.models.events.channel_back_event.twitter_text",
      requester_name: value[:requester][:name],
      requester_handle_link: twitter_link(value[:requester][:screen_name]),
      monitored_handle_name: value[:source][:name],
      monitored_handle_link: twitter_link(value[:source][:screen_name])
    )
  end

  def twitter_link(screen_name)
    "<a href='http://www.twitter.com/#{screen_name}' target='blank'>@#{screen_name}</a>"
  end

  def any_channel_text
    I18n.t("txt.models.events.channel_back_event.any_channel_text")
  end

  def ticket_url
    return nil unless ticket.via_twitter?
    return nil unless add_link?

    ticket.twicket_url
  end

  def add_link?
    if account.has_end_user_ui? && !account.settings.no_short_url_tweet_append?
      if account.help_center_enabled? && !ticket.brand.help_center_enabled?
        return false
      end

      if account.url_shortener
        account.url_shortener.add_url_by_default || Zendesk::TRUE_VALUES.include?(append_ticket_url)
      else
        if Arturo.feature_enabled_for?(:twitter_add_link_default_false, account)
          false
        else
          true
        end
      end
    else
      false
    end
  end

  def recipient_ids
    # This is only used for AnyChannel tickets.
    return [] unless ticket.via_any_channel?

    ticket.requester.identities.find_all { |i| i.is_a? UserAnyChannelIdentity }.map(&:value)
  end
end
