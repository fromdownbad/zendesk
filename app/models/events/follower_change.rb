class FollowerChange < CollaborationChange
  alias_attribute :current_followers,  :value
  alias_attribute :previous_followers, :value_previous

  before_create :initialize_value_reference

  fixed_serialize :value, Array
  fixed_serialize :value_previous, Array

  def self.current_addresses(account, collaborations)
    super(account, collaborations, :follower?)
  end

  def initialize_value_reference
    self.value_reference = 'current_followers'
    true
  end
end
