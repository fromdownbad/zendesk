class VoiceComment
  class Recording
    attr_reader :account_sid, :sid
    def initialize(url)
      if url
        @account_sid, @sid = url.to_s.match(/\/Accounts\/(.*?)\/Recordings\/(.*?)$/).try(:captures) # Recordings are not always in the Twilio format
      end
    end

    def matches_sid?(other_sid)
      @sid == other_sid
    end
  end
end
