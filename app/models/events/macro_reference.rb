# When a macro is applied by a trigger or automation
class MacroReference < AuditEvent
  include Zendesk::Serialization::MacroReferenceSerialization

  alias_attribute :macro_id, :value
  validates_presence_of :value

  attr_accessible :value, :value_previous, :value_reference, :macro_id, :via_id, :via_reference_id

  def macro(referenced_macro = nil)
    @macro ||= begin
      referenced_macro ||= Macro.with_deleted { Macro.find_by_id(value) }

      unless referenced_macro.present?
        return DeletedMacro.new(
          I18n.t('txt.api.v2.rule_source.hard_deleted_reference')
        )
      end

      if referenced_macro.deleted?
        return DeletedMacro.new(
          I18n.t(
            'txt.api.v2.rule_source.soft_deleted_reference',
            title: referenced_macro.title
          )
        )
      end

      referenced_macro
    end
  end

  def to_s(_options = {})
    "Macro \"#{macro.title}\" referenced"
  end

  DeletedMacro = Struct.new(:title) do
    def deleted?
      true
    end
  end

  private_constant :DeletedMacro
end
