class WorkspaceChanged < AuditEvent
  fixed_serialize :value, Hash
  fixed_serialize :value_previous, Hash

  alias_attribute :workspace_attributes, :value
  alias_attribute :previous_workspace_attributes, :value_previous

  validate :workspace_should_have_id_and_title
  validate :action_should_be_valid

  before_save :set_type_and_is_public

  EVENT_REQUIRED_FIELDS = [:id, :title].freeze
  ACTIONS = [:ADD, :CHANGE, :DELETE].freeze

  def workspace_id
    value[:id]
  end

  def workspace_title
    value[:title]
  end

  def action
    value[:action]
  end

  def add_new_workspace_audit!(workspace_attributes)
    if workspace_attributes.workspace
      self.workspace_attributes = workspace_attributes.workspace.merge(action: :ADD)
    end
  end

  def add_change_workspace_audit!(workspace_attributes)
    previous_workspace = workspace_attributes.delete_field :previous_workspace

    if workspace_attributes.workspace
      self.workspace_attributes = workspace_attributes.workspace.merge(action: :CHANGE)
      self.previous_workspace_attributes = {
        id: previous_workspace[:id],
        title: previous_workspace[:title]
      }
    end
  end

  def add_delete_workspace_audit!(workspace_attributes)
    if workspace_attributes.workspace
      self.workspace_attributes = workspace_attributes.workspace.merge(action: :DELETE)
    end
  end

  private

  def set_type_and_is_public
    self.is_public = false
    self.type = 'WorkspaceChanged'
  end

  def workspace_should_have_id_and_title
    EVENT_REQUIRED_FIELDS.each do |field|
      errors.add(:event, " does not have the required key '#{field}'") unless workspace_attributes.key? field
    end

    if action == :CHANGE
      EVENT_REQUIRED_FIELDS.each do |field|
        errors.add(:event, " does not have the required key '#{field}'") unless previous_workspace_attributes.key? field
      end
    end
  end

  def action_should_be_valid
    errors.add(:event, "contains unsupported action #{action}") unless ACTIONS.include? action
  end
end
