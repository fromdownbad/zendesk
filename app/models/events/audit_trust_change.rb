class AuditTrustChange < AuditEvent
  validates_presence_of :value_reference
  validate :valid_event_reference

  alias_attribute :event_id, :value_reference

  attr_accessible :event_id

  def to_s(_options = {})
    I18n.t('txt.models.events.audit_trust_change.audit_trusted', comment: comment.body.truncate(100))
  end

  def comment
    ticket.audits.find_by_id(event_id).comment
  end

  private

  def valid_event_reference
    unless ticket.events.exists?(id: event_id)
      errors.add(:event_id, :invalid)
    end
  end
end
