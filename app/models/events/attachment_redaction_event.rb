class AttachmentRedactionEvent < AuditEvent
  attr_accessible :attachment_id

  def attachment_id=(attachment_id)
    self.value = attachment_id
    self.value_reference = "attachment_id"
  end

  def attachment_id
    value.present? ? value.to_i : nil
  end

  def comment_id
    if attachment_id
      if attachment = Attachment.find_by_account_id_and_id(account_id, attachment_id)
        attachment.source_id if attachment.source_type == "Comment"
      end
    end
  end
end
