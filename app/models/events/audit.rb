# Each time someone touches a ticket, one Audit is created with references to what happened.

require 'benchmark'
require 'set'
require 'zendesk/geo_location'

module AuditArchivedParentTicket
  def events(*args)
    if archived? && @archived_parent_ticket && !@events_association_loaded_from_archive
      association(:events).target = @archived_parent_ticket.events.select { |e| e.parent_id == id }
      @events_association_loaded_from_archive = true
    end
    super(*args)
  end

  def comment(*args)
    if archived? && @archived_parent_ticket && !@comment_association_loaded_from_archive
      association(:comment).target = @archived_parent_ticket.events.detect { |e| e.is_a?(Comment) && e.parent_id == id }
      @comment_association_loaded_from_archive = true
    else
      if !@comment_association_loaded_from_events && association(:events).loaded? && !association(:comment).loaded?
        association(:comment).target = events.find { |e| e.class.name.in?(AuditEvent::COMMENT_EVENTS) }
        @comment_association_loaded_from_events = true
      end
    end
    super(*args)
  end
end

class Audit < Event
  include Zendesk::Serialization::AuditSerialization
  prepend AuditArchivedParentTicket

  METADATA_SIZE_LIMIT = 4_096
  TRUNCATED_RECIPIENTS_INDICATOR = ' ...'.freeze
  DEFAULT_MAX_RECIPIENTS_SIZE = 65535
  NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT = 50
  SCRUBBED_DATA_VALUES = {
    Array: [],
    String: '',
    Symbol: '',
    Integer: 0,
    Float: 0
  }.freeze
  METADATA_KEY_ALLOWLIST = [
    'notifications_suppressed_for',
    'slack_channel_id',
    'device_model',
    'device_os',
    'device_total_memory',
    'device_storage',
    'device_battery',
    'z1_request_url'
  ].freeze

  CHAT_CHANNELS = [
    ViaType.CHAT,
    ViaType.NATIVE_MESSAGING,
    ViaType.LINE,
    ViaType.WECHAT,
    ViaType.WHATSAPP,
    ViaType.MAILGUN,
    ViaType.MESSAGEBIRD_SMS,
    ViaType.SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER,
    ViaType.TELEGRAM,
    ViaType.TWILIO_SMS,
    ViaType.SUNSHINE_CONVERSATIONS_TWITTER_DM,
    ViaType.VIBER,
    ViaType.GOOGLE_RCS,
    ViaType.APPLE_BUSINESS_CHAT,
    ViaType.GOOGLE_BUSINESS_MESSAGES,
    ViaType.KAKAOTALK,
    ViaType.INSTAGRAM_DM,
    ViaType.SUNSHINE_CONVERSATIONS_API
  ].freeze

  belongs_to :ticket

  has_many    :events, dependent: :destroy, foreign_key: :parent_id
  has_many    :notifications, foreign_key: :parent_id
  has_many    :notifications_with_ccs, class_name: 'NotificationWithCcs', foreign_key: :parent_id
  has_one     :comment, foreign_key: :parent_id
  has_one     :cc, foreign_key: :parent_id

  scope :by_automation, ->(automation) { where(via_id: ViaType.RULE, via_reference_id: automation.id) }

  attr_accessible :via_id, :via_reference_id, :client, :disable_triggers, :created_at, :author_id, :is_public, :value_previous, :value, :value_reference

  validates_presence_of :account_id, :ticket, :author, :via_id
  validate      :validate_metadata_size

  attr_accessor :disable_triggers, :ticket_sharing_create
  attr_writer   :delivery_count,
    :notification_with_ccs, # Can remove when email_ccs_preload_notifications_with_ccs Arturo is GA
    :recipients_notification_ccs,
    :recipients_notification_to

  # This value contains ticket's latest recipients if audit was generated via email
  alias_attribute :recipients, :value

  before_validation :locate_client, on: :create
  before_validation :encode_metadata, on: :create
  before_create :initialize_defaults
  before_create :instrument_metadata_size
  after_commit :log_commit

  scope :filtered, -> {
    joins(:ticket).where("tickets.id IS NOT NULL").merge(Ticket.all)
  }

  def disable_triggers?
    @disable_triggers
  end

  def trusted?
    metadata.fetch("trusted", true)
  end

  # [2015.04.21 rmartz] and [2015.11.02 abeckwith] flag! should be used instead of this for future flag types
  def untrusted!
    flag!([EventFlagType.DEFAULT_UNTRUSTED])
  end

  def flagged_reasons
    flag_names = if flags.untrusted_flags.any?
      flags.untrusted_flags.map { |flag| EventFlagType[flag].name }
    else
      [SuspensionType[suspension_type_id].try(:name)]
    end

    flag_names.select! { |name| EventFlagType.translatable_names.include?(name) }
    flag_names.any? ? flag_names : ["default"]
  end

  def suspension_type_id
    metadata['suspension_type_id']
  end

  def suspension_type_id=(suspension_type_id)
    set_metadata('suspension_type_id', suspension_type_id)
  end

  def flagged?(flag_type = nil)
    flag_type ? flags.include?(flag_type) : flags.any?
  end

  def flag!(flag_types, options = {})
    return if flag_types.empty?
    flags.merge_flags(flag_types, options)
    save_flags!
  end

  def unflag!(flag_type)
    return unless flags.delete?(flag_type)
    save_flags!
  end

  def flags
    @flags ||= AuditFlags.new(metadata['flags']).tap do |flags|
      flags.options.merge!(metadata['flags_options'] || {}) if flags
    end
  end

  def flags_options
    flags.options
  end

  def save_flags!
    set_metadata('flags', flags.to_a)
    set_metadata('flags_options', flags_options)
    set_metadata('trusted', flags.trusted?)
    save! unless new_record?
  end

  def metadata
    @metadata ||= AuditMetadata.parse_metadata(value_previous).freeze
  end

  def set_metadata(key, value)
    self.value_previous = metadata.merge(key => value).to_json
    @metadata = nil
  end

  def apply_metadata(kind, data)
    return unless HashParam.ish?(data)

    metadata[kind].merge!(data)
  end

  def notifications_suppressed_for=(invalid_recipients)
    if invalid_recipients.size > NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT
      invalid_recipients = truncate_invalid_recipients(invalid_recipients)
    end
    set_metadata('notifications_suppressed_for', invalid_recipients)
  end

  def scrub_metadata!
    return unless account.has_delete_ticket_metadata_pii_enabled?
    return unless scrubbing?

    if ticket.archived?
      Rails.logger.error "Failed to scrub_metadata! as ticket is archived: account_id: #{account_id}, ticket_id: #{ticket.id}, audit_id: #{id}"
      Rails.application.config.statsd.client.increment('scrub_metadata.failure.archived', tags: ["account_id:#{account_id}", "ticket_id:#{ticket.id}", "audit_id:#{id}"])
      return
    end

    self.value = nil if via_id == ViaType.MAIL
    self.value_previous = scrub_metadata_hash!(metadata).to_json
    @metadata = nil
    save!
    Rails.application.config.statsd.client.increment('scrub_metadata.success', tags: ["account_id:#{account_id}"])
  rescue StandardError => e
    Rails.logger.error "Failed to scrub_metadata! for account #{account_id}: #{e.message}, ticket_id: #{ticket.id}, audit_id: #{id}"
    Rails.application.config.statsd.client.increment('scrub_metadata.failure.unknown', tags: ["account_id:#{account_id}", "ticket_id:#{ticket.id}", "audit_id:#{id}"])
  end

  def client
    AuditMetadata.legacy_client_string(metadata)
  end

  # once we stop setting client we should remove this
  def client=(new_client)
    if parsed_system = AuditMetadata.parse_metadata(new_client)[:system]
      metadata[:system].merge!(parsed_system)
    end
  end

  def to_s
    value || "Audit via #{ViaType.to_s(via_id)}#{(' ' + Rule.find(via_reference_id).title) if via?(:rule) && via_reference_id}"
  end

  def to_liquid
    hash = Zendesk::Liquid::Wrapper.new("to_s")
    hash.merge!(
      'author'     => lambda { |_x| author },
      'created_at' => lambda { |_x| created_at.to_format },
      'timestamp'  => created_at.to_i.to_s,
      'comment'    => lambda { |_x| comment },
      'changes'    => lambda { |_x| events.select { |e| e.is_a?(Change) } },
      'to_s'       => to_s
    )
    hash
  end

  # This is a convenience method for tests
  def inspect_events
    events.collect(&:inspect_me)
  end

  # Convert ticket attribute changes to create/change events on the audit
  def grow
    klass = ticket.new_record? ? Create : Change
    ticket.delta_changes.to_a.each do |change|
      next if should_omit?(change)
      events << klass.new(transaction: change, audit: self, created_at: created_at)
    end

    (ticket.external_changes || []).each { |external_change| events << external_change }

    # NOTE: collaboration_changes includes EmailCcChange & FollowerChange events
    (ticket.collaboration_changes || []).each { |collaboration_change| events << collaboration_change }

    events
  end

  # Execute triggers against the ticket and audit events.
  def execute(current_events = events)
    if disable_triggers? || current_events.blank?
      return Zendesk::Rules::Trigger::RunState.new
    end

    Zendesk::Rules::Trigger::Execution.execute(
      account:        account,
      events:         events,
      execution_type: execution_type,
      ticket:         ticket
    )
  end

  def changes?(options = {})
    Change.changes?(events, options)
  end

  def delivery_count
    @delivery_count ||= 0
  end

  def redactions?
    events.any? { |event| [TicketFieldRedactEvent, CommentRedactionEvent].include?(event.class) }
  end

  def raw_email
    @raw_email ||= Zendesk::Mailer::RawEmailAccess.new(
      metadata[:system][:raw_email_identifier],
      attachments: comment.try(:attachments).presence,
      json_identifier: metadata[:system][:json_email_identifier],
      email_fix_no_delimiter_redaction: account.has_email_fix_no_delimiter_redaction?
    )
  end

  def set_custom_metadata!(custom_metadata) # rubocop:disable Naming/AccessorMethodName
    scrubbed_metadata_hash = scrub_metadata_hash!(custom_metadata)
    set_metadata(:custom, metadata[:custom].merge(scrubbed_metadata_hash))
    save!
  end

  def delete_eml_file
    return if raw_email.eml_identifier.blank?

    set_metadata(:system, metadata[:system].merge(raw_email_identifier: nil))
    raw_email.delete_eml_file!
  end

  def delete_eml_file!
    delete_eml_file
    save!
  end

  def delete_json_files
    return if raw_email.json_identifier.blank?

    set_metadata(:system, metadata[:system].merge(json_email_identifier: nil))
    raw_email.delete_json_files!
  end

  def delete_json_files!
    delete_json_files
    save!
  end

  def redact_json_files!(text)
    raw_email.redact_json_files!(text)
  end

  def disable_radar_notification?
    disable_triggers? && !always_notify_radar?
  end

  #######################################################################
  ### The following methods are used in various presenters of audit data
  #######################################################################

  def recipients_list
    return [] if recipients.blank?
    return [] unless via?(:mail)
    @recipients_list ||= recipients.split(" ")
  end

  # This currently only excludes inactive users when the via type is MAIL. We may want to look into also filtering
  # inactive users for other via types, but that may need to get pulled into Api::V2::Tickets::CcsAndFollowersHelper
  # similarly to recipients_ids_or_identifiers
  def active_recipients_ids
    if via?(:mail)
      return [] unless recipients_list.present?
      all_ids = account_identities.map(&:user_id)

      # reject inactive users
      recipient_ids = account.users.where(id: all_ids, is_active: true).pluck(:id)

      # reject the author's ID since they will show up as the From
      recipient_ids.delete(author_id) if author

      recipient_ids.uniq
    elsif via_api_lotus_or_closed?
      if account.has_email_ccs_preload_notifications_with_ccs?
        recipients_notification_ccs
      else
        recipients_notification_ccs.map(&:id)
      end
    else
      []
    end
  end

  def support_addresses
    @support_addresses ||= account.recipient_addresses.pluck(:email)
  end

  # This method can be removed when the Arturo email_ccs_preload_notifications_with_ccs is GA as it has moved
  # to Api::V2::Tickets::CcsAndFollowersHelper
  def recipients_ids_or_identifiers
    # Use user IDs or email addresses if the comment was generated by mail
    if via?(:mail)
      return [] unless recipients_list.present?

      recipient_ids = active_recipients_ids

      # Append any email addresses that didn't have corresponding active identities and are
      # not support addresses nor the author
      active_addresses = account_identities.select { |identity| recipient_ids.include?(identity.user_id) }.map(&:value)
      author_address = author ? author.identities.email.pluck(:value) : []
      recipient_ids += recipients_list - active_addresses - author_address
      recipient_ids -= recipients_support_address.nil? ? support_addresses : [recipients_support_address]
    elsif via_api_lotus_or_closed?
      # Otherwise, for non-mail comments, use IDs of the users who received the notification
      # or if they have been deleted, their names (which is all that is available after soft deletion)
      recipients_notification_ccs.each_with_object([]) do |user, notification_recipient_ids_or_info|
        # mimic Events view for inactive users in the notifications
        notification_recipient_ids_or_info << (user.is_active ? user.id : user.name)
      end
    else
      []
    end
  end

  def recipients_support_address
    return unless via?(:mail) && recipients_list.present?

    @email_mapping ||= {}

    recipients_list.select do |recipient_address_with_plus_id|
      unless @email_mapping[recipient_address_with_plus_id].present?
        address_without_plus_id = Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(recipient_address_with_plus_id)

        next unless support_addresses.include?(address_without_plus_id)

        @email_mapping[recipient_address_with_plus_id] = address_without_plus_id
      end

      recipient_address_with_plus_id
    end.first
  end

  def recipients_notification_to
    return if via?(:mail)
    return unless notification_with_ccs.try(:recipients).present?

    # With Arturo email_ccs_preload_notifications_with_ccs, the return value is a User ID.
    # Without Arturo email_ccs_preload_notifications_with_ccs, the return value is a User object.
    @recipients_notification_to ||= if account.has_email_ccs_preload_notifications_with_ccs?
      notification_with_ccs.recipients.first
    else
      all_users_with_ids(notification_with_ccs.recipients.first).first
    end
  end

  def recipients_notification_ccs
    return [] if via?(:mail)
    return [] unless notification_with_ccs
    return @recipients_notification_ccs if @recipients_notification_ccs

    notification_recipient_ids = notification_with_ccs.recipients
    notification_recipient_ids.shift

    # With Arturo email_ccs_preload_notifications_with_ccs, the return value is an array of User IDs.
    # Without Arturo email_ccs_preload_notifications_with_ccs, the return value is an array of User objects.
    @recipients_notification_ccs ||= if account.has_email_ccs_preload_notifications_with_ccs?
      notification_recipient_ids
    else
      all_users_with_ids(notification_recipient_ids)
    end
  end

  def account_identities
    @account_identities ||= account.user_email_identities.select("value, user_id").where(value: recipients_list)
  end

  def via_api_lotus_or_closed?
    via?(:web_service) || via?(:web_form) || via?(:closed_ticket)
  end

  protected

  def always_notify_radar?
    return true if CHAT_CHANNELS.include? ticket.via_id
    [CommentPrivacyChange, KnowledgeLinked, KnowledgeCaptured, KnowledgeFlagged].include?(events.first.class)
  end

  def locate_client
    Zendesk::GeoLocation.locate_ticket_audit!(self)
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "GeoLocation error", fingerprint: '70bf346b99b0f8bfe03ad842e941db65ccc65301')
  end

  def encode_metadata
    self.value_previous = metadata.to_json
  end

  def validate_metadata_size
    if value_previous.present? && value_previous.size > METADATA_SIZE_LIMIT
      errors.add(:metadata, :invalid)
    end
  end

  def log_commit
    message = persisted? ? "Committed audit #{id}" : "Audit not persisted!"
    Rails.logger.warn(message)

    if errors.any?
      errors.full_messages.each { |e| Rails.logger.info("[Audit Errors] - #{e}") }
    end
  end

  def initialize_defaults
    self.is_public ||= false

    if via?(:mail)
      # Ex. "from@example.com reply-to@example.com to@example.com cc1@example.com"
      new_recipients = ticket.recipients_for_latest_mail

      Rails.logger.info("Audit.recipients set from inbound mail: #{new_recipients.inspect}")
    end

    if new_recipients.present?
      self.recipients = if account.has_email_ccs?
        truncate_recipients(new_recipients)
      else
        new_recipients
      end
    end

    true
  end

  def should_omit?(change)
    field_changed = change.first

    if field_changed == "original_recipient_address"
      return true unless account.has_original_recipient_address_audit?
      return ticket.new_record?
    end

    (field_changed == "brand_id" && !account.has_multibrand?) ||
      (field_changed == "current_collaborators" && account.has_follower_and_email_cc_collaborations_enabled?)
  end

  def instrument_metadata_size
    metadata_size_statsd_client.gauge('metadata.size', value_previous.size) if value_previous.present?
  end

  def metadata_size_statsd_client
    @metadata_size_statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[suspended_ticket])
  end

  def truncate_recipients(new_recipients)
    return new_recipients if new_recipients.blank?

    # Get size limit for the "value" column, which stores the recipients of the associated comment
    column = Audit.columns_hash.fetch("value")
    limit = Zendesk::Extensions::TruncateAttributesToLimit.limit(column) || DEFAULT_MAX_RECIPIENTS_SIZE

    if new_recipients.bytesize > limit
      # Same logic as Zendesk::Extensions::TruncateAttributesToLimit
      new_value = new_recipients.mb_chars.limit(limit - TRUNCATED_RECIPIENTS_INDICATOR.length).to_s

      # If no space is found, then we are in the first entry and it will be truncated as well
      new_length = new_value.rindex(" ") || 0

      if new_length != new_recipients.length
        Rails.logger.info "Shortening new Audit recipients value to #{new_length} bytes"
        new_recipients = if new_length == 0
          TRUNCATED_RECIPIENTS_INDICATOR.strip
        else
          new_value.mb_chars.limit(new_length).to_s + TRUNCATED_RECIPIENTS_INDICATOR
        end
      end
    end
    new_recipients
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Error in truncate_recipients", fingerprint: '41bc1f29f0beb477f1bd00d7eed8c85e5fd27a51')
    new_recipients
  end

  def truncate_invalid_recipients(invalid_recipients)
    invalid_recipients[0...NOTIFICATIONS_SUPPRESSED_FOR_COUNT_LIMIT]
  end

  private

  def scrub_metadata_hash!(data)
    data.each_with_object({}) do |(key, value), result|
      result[key] = if METADATA_KEY_ALLOWLIST.include?(key)
        value
      elsif value.is_a?(Hash)
        scrub_metadata_hash!(value)
      else
        SCRUBBED_DATA_VALUES[value.class.to_s.to_sym]
      end
    end
  end

  def execution_type
    return :ticket_sharing_create   if ticket_sharing_create
    return :satisfaction_prediction if satisfaction_prediction?

    :default
  end

  def prepare_recipient_list?
    account.has_comment_email_ccs_allowed_enabled? &&
      via_api_lotus_or_closed? &&
      comment.try(:is_public?)
  end

  def satisfaction_prediction?
    account.has_satisfaction_prediction? &&
      via_id == ViaType.SATISFACTION_PREDICTION
  end

  def notification_with_ccs
    if account.has_email_ccs_preload_notifications_with_ccs?
      notifications_with_ccs.first
    else
      @notification_with_ccs ||= events.where(type: "NotificationWithCcs").first
    end
  end

  def all_users_with_ids(ids)
    account.all_users.where(id: ids).to_a
  end
end
