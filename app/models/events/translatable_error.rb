class TranslatableError < AuditEvent
  fixed_serialize :value

  attr_accessible :value

  def to_s(*_args)
    error_field, error_type = value.values_at(:error_field, :error_type)
    if error_field && error_type
      value[:error] = resolve_error(error_field, error_type)
    end

    I18n.t(value[:key], format_translation_options(value))
  end

  private

  def format_translation_options(value)
    value.tap do |h|
      h[:removed_ccs] = h[:removed_ccs].gsub(/[<>]/, '') if h[:removed_ccs].present?
    end
  end

  def resolve_error(field, error_type)
    t = Ticket.new
    t.define_singleton_method(field) {} unless t.respond_to?(field)
    e = ActiveModel::Errors.new(t)
    e.add field, error_type
    e.full_messages.first
  end
end
