class FacebookComment < Comment
  has_many :attachments,
    -> { where(parent_id: nil).order(id: :asc) },
    as: :source,
    dependent: :destroy,
    inherit: :account_id

  alias_attribute :graph_object_id, :value_previous
  fixed_serialize :value_reference

  attr_accessible :add_short_url, :author_id, :body, :channel_back, :created_at, :is_public, :uploads, :value,
    :value_previous, :value_reference, :via_id, :via_reference_id, :notification_sent_at, :parent_id, :updated_at,
    :graph_object_id, :data

  delegate :page, to: :wall_post

  has_one :shared_comment, foreign_key: :comment_id

  before_save :validate_and_correct_value_reference

  MAX_VALUE_REFERENCE = 8192

  def data
    # If the value_reference cannot be interpreted as YAML, return the empty
    # hash.  See https://support.zendesk.com/agent/tickets/899640.
    value_reference.is_a?(String) ? {} : value_reference
  end

  def data=(new_value)
    self.value_reference = new_value
  end

  def external_id
    if outgoing_comment
      outgoing_comment.graph_object_id
    else
      graph_object_id
    end
  end

  def empty?
    false
  end

  def to_s
    if data[:content].nil? || via_zendesk
      body
    else
      result = data[:content]
      unless result.valid_encoding? # repair invalid utf8
        result.to_utf8!
        temp_allow_changes { update_attribute(:data, data) } unless new_record?
      end
      result
    end
  end

  def via_zendesk
    data[:via_zendesk]
  end

  def should_channel_back_to_facebook?
    (author.is_agent? || author.foreign_agent?) && via_zendesk && channel_back_requested? && is_public?
  end

  def add_signature
    super unless should_channel_back_to_facebook?
  end

  def shared_attachments
    data[:attachments].map do |attachment|
      TicketSharing::Attachment.new(
        'filename' => attachment['name'],
        'content_type' => attachment['mime_type']
      )
    end
  end

  def for_partner
    name = data[:requester] ? data[:requester][:name] : author.safe_name(false)
    TicketSharing::Actor.new(
      'uuid' => generate_uuid,
      'name' => name,
      'role' => 'user'
    )
  end

  def generate_uuid
    user_id = data[:requester] ? "facebook/#{data[:requester][:id]}" : author.id
    key = "#{account.sharing_url}/users/#{user_id}"
    Digest::SHA1.hexdigest(key)
  end

  def validate_and_correct_value_reference
    # If the YAML serialization of value_reference is too long, it'll be truncated
    # when stored in the DB, and hence will be garbage.  Check the length, and
    # replace with {} if too long.  See https://support.zendesk.com/agent/tickets/899640.
    self.value_reference = {} if MAX_VALUE_REFERENCE <= value_reference.to_yaml.length
  end
end
