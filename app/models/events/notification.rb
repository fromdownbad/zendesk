# Outgoing emails from a rule action
class Notification < AuditEvent
  include Zendesk::Serialization::NotificationSerialization

  LEGACY_DELIVERY_CHUNK_LIMIT = 25
  COLLABORATORS_NOTIFICATION_LIMIT = 20

  alias_attribute :recipients, :value
  alias_attribute :subject, :value_previous
  alias_attribute :body, :value_reference

  attr_accessible :value, :via_id, :via_reference_id, :recipients, :subject, :body,
    :value_previous, :value_reference

  fixed_serialize :value, Array
  truncate_attributes_to_limit :value_reference # value_reference is 8k, but rule actions can have up to 65k of text
  validates_presence_of :recipients

  after_commit :enqueue_ticket_update, on: :create

  def inherit_defaults_from_audit
    super

    author_id = audit.comment.try(:author_id)
    if author_id && !(recipients.include?(author_id) && account.end_users.exists?(author_id))
      self.author_id = author_id
    end
  end

  def to_s
    recipient_names(recipients).to_s
  end

  def message_id
    outbound_email.try(:message_id)
  end

  def message_id=(mid)
    return nil if new_record?
    return mid if message_id == mid
    unless outbound_email
      @outbound_email = OutboundEmail.new(account: account, ticket: ticket, notification: self)
    end
    @outbound_email.message_id = mid

    if @outbound_email.ticket_id.nil?
      logger.info("Outbound ticket id is nil for account_id #{account.id} message_id #{mid}")
      statsd_client.increment('outbound_email_missing_ticket_id')
    else
      @outbound_email.save
    end

    message_id
  end

  protected

  def enqueue_ticket_update
    audit.delivery_count += 1

    return deliver_notifications if deliver?

    audit.events << TranslatableError.new(
      ticket: audit.ticket,
      value:  {
        key:            'event.delivery_would_exceed_collab_max',
        notification_id: id
      }
    )
  end

  def to_recipient_ids
    recipients
  end

  def valid_to_recipients
    @valid_to_recipients ||= begin
      account.
        users.
        where(id: to_recipient_ids).
        select('id, account_id, name, roles').
        includes(:identities, :settings).
        to_a.tap do |scoped_recipients|
          scoped_recipients.delete_if do |recipient|
            recipient.suspended? || !recipient.has_email?
          end
        end
    end
  end

  def deliver_notifications
    logger.info("Delivering notifications for event #{id} at #{Time.now}")

    localize do
      agents, end_users = valid_to_recipients.partition(&:is_agent?)

      # In the "Agent as End User" feature, an agent that creates a ticket in
      # Help Center is treated as an end user
      if Arturo.feature_enabled_for?(:agent_as_end_user, account)
        # If one of the agents is the submitter, treat them as an end user
        end_user_agents, agents = agents.partition { |agent| ticket.agent_as_end_user_for?(agent) }
        end_users.concat end_user_agents
      end

      end_users.each_slice(LEGACY_DELIVERY_CHUNK_LIMIT) do |recipients|
        deliver(recipients, false)
      end

      agents.each_slice(LEGACY_DELIVERY_CHUNK_LIMIT) do |recipients|
        deliver(recipients, true)
      end
    end
  end

  def deliver(recipients, for_agent)
    EventsMailer.deliver_rule(self, ticket, recipients, for_agent)
  end

  def localize(&block)
    Time.use_zone(account.time_zone) do
      I18n.with_locale(account.translation_locale || ENGLISH_BY_ZENDESK, &block)
    end
  end

  def outbound_email
    # index is on account/notification IDs
    @outbound_email ||= OutboundEmail.for_notification(self).first
  end

  def deliver?
    unless ticket.present?
      logger.warn("No ticket found for event #{id}, skipping")

      return false
    end

    audit.delivery_count <= COLLABORATORS_NOTIFICATION_LIMIT
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[notification])
  end

  def logger
    Rails.logger
  end
end
