class AssociateAttValsEvent < AuditEvent
  alias_attribute :attribute_values, :value
  alias_attribute :previous_attribute_values, :value_previous

  attr_accessible :value, :attribute_values, :previous_attribute_values,
    :value_previous
  fixed_serialize :value, Array
end
