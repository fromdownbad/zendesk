module Tags
  CHAT_TAGS = %w[zopim_chat].freeze
  CHAT_ENDED_TAGS = %w[zopim_chat_ended].freeze
  CHAT_MISSED_TAGS = %w[zopim_chat_ended zopim_chat_missed].freeze
  CHAT_SYSTEM_TAGS = [*CHAT_TAGS, *CHAT_ENDED_TAGS, *CHAT_MISSED_TAGS].freeze

  SCHEMA = {
    type: 'array',
    items: {
      type: 'string'
    }
  }.freeze

  def included(base)
    base.before_validation :sanitize_tags
  end

  def tags
    super || []
  end

  private

  def sanitize_tags
    self.tags -= CHAT_SYSTEM_TAGS
  end
end

class ChatEvent < AuditEvent
  self.abstract_class = true

  # only used for started events, but association preloading happens on this class
  has_many :chat_transcripts, foreign_key: :audit_id, inherit: :account_id, dependent: :destroy
  has_many :attachments,
    -> { where(parent_id: nil).order('created_at ASC, id ASC') },
    inherit: :account_id,
    as: :source,
    dependent: :destroy

  # some chat only properties here
  attr_accessible :value

  validates :ticket, presence: true

  def ticket_subject
    return unless valid?

    visitor_name = ticket.requester.name
    ::I18n.t('txt.chat.new_conversation_title', visitor_name: visitor_name)
  end

  def apply!
    validate!
    ticket.audit.events << self
    apply_ticket_changes
    ticket.save!
  end

  def ticket_body; end

  def add_attachments(attachments)
    attachments.each do |attachment|
      attachment.is_public = is_public?
      self.attachments << attachment
    end
  end

  protected

  def chat_channel?
    valid? && ticket.via_id == ViaType.CHAT
  end

  def disable_triggers?
    false
  end

  def apply_tags
    ticket.additional_tags = Tags::CHAT_TAGS
  end

  def apply_ticket_changes
    ticket.audit.disable_triggers = disable_triggers?
    apply_tags if chat_channel?
  end
end
