class AutomaticAnswerSolve < AuditEvent
  fixed_serialize :value, Hash

  before_create :set_type_and_is_public
  validates :solved_article, presence: true
  validate :solved_article_should_have_an_id_and_url

  def solved_article=(article)
    self.value = { solved_article: article }
  end

  def solved_article
    value[:solved_article] || {}
  end

  alias_method :body, :solved_article

  private

  def set_type_and_is_public
    self.type = 'AutomaticAnswerSolve'
    self.is_public = false
    true
  end

  def solved_article_should_have_an_id_and_url
    unless solved_article[:id] && solved_article[:url]
      errors.add(:solved_article, " does not have the required one of the required keys {id, url}")
    end
  end
end
