class KnowledgeCaptured < AuditEvent
  fixed_serialize :value, Hash
  fixed_serialize :value_previous, Hash

  before_save :set_type_and_is_public

  validates :article, presence: true
  validates :template, presence: true

  validate  :article_and_template_should_have_required_fields

  alias_attribute :article, :value
  alias_attribute :template, :value_previous

  attr_accessible :article, :template

  ARTICLE_REQUIRED_FIELDS = [:title, :url, :id, :html_url, :locale].freeze
  TEMPLATE_REQUIRED_FIELDS = [:title, :url, :id, :html_url].freeze

  def body
    {
      article: article,
      template: template
    }
  end

  def article
    value || {}
  end

  def template
    value_previous || {}
  end

  private

  def set_type_and_is_public
    self.is_public = false
    self.type = 'KnowledgeCaptured'
  end

  def article_and_template_should_have_required_fields
    ARTICLE_REQUIRED_FIELDS.each do |field|
      errors.add(:article, " does not have the required key '#{field}'") unless article.key?(field)
    end

    TEMPLATE_REQUIRED_FIELDS.each do |field|
      errors.add(:template, " does not have the required one of the required keys {id, url, html_url, title}") unless template.key?(field)
    end
  end
end
