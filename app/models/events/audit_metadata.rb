class AuditMetadata
  def self.parse_metadata(metadata)
    metadata = try_decode(metadata) if metadata.is_a?(String)
    metadata ||= HashWithIndifferentAccess.new

    if metadata.is_a?(String)
      parse_client(metadata)
    elsif metadata.is_a?(Hash)
      metadata = metadata.with_indifferent_access
      metadata[:system] = (metadata[:system] || {}).with_indifferent_access
      metadata[:custom] = (metadata[:custom] || {}).with_indifferent_access
      metadata
    end
  end

  def self.try_decode(metadata)
    ActiveSupport::JSON.decode(metadata)
  rescue Yajl::ParseError, MultiJson::LoadError, JSON::ParserError
    metadata
  end

  CLIENT_PARSER = /([^:]+): (.*)/
  def self.parse_client(client)
    system_metadata = HashWithIndifferentAccess.new

    (client || '').split("\n").each do |line|
      line.strip!

      next unless match = CLIENT_PARSER.match(line)
      name = match[1]
      value = match[2]
      name.strip!
      name.downcase!
      value.strip!

      case name
      when 'message id'
        system_metadata[:message_id] = value
      when 'client'
        system_metadata[:client] = value
      when 'ip address'
        system_metadata[:ip_address] = value
      when 'location'
        system_metadata[:location] = value
      end
    end

    hash = HashWithIndifferentAccess.new
    hash[:system] = system_metadata
    hash[:custom] = HashWithIndifferentAccess.new
    hash
  end

  def self.legacy_client_string(metadata)
    return nil if metadata.nil? || metadata[:system].blank?

    system = metadata[:system]

    parts = []

    parts << "#{I18n.t("txt.admin.models.events.audit_metadata.message_id")}: #{system[:message_id]}" if system[:message_id].present?
    parts << "#{I18n.t("txt.admin.models.events.audit_metadata.client")}: #{system[:client]}"         if system[:client].present?
    parts << "#{I18n.t("txt.admin.models.events.audit_metadata.ip_address")}: #{system[:ip_address]}" if system[:ip_address].present?
    parts << "#{I18n.t("txt.admin.models.events.audit_metadata.location")}: #{system[:location]}"     if system[:location].present?

    parts.join("\n")
  end
end
