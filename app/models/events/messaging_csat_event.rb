class MessagingCsatEvent < AuditEvent
  store :value, accessors: [:chat_started_id], coder: JSON
end
