class CollaborationChange < AuditEvent
  alias_attribute :current,  :value
  alias_attribute :previous, :value_previous

  class << self
    # Returns the "current" addresses from the last change event recorded.
    def last_event_addresses(events)
      # This already does an ORDER BY created_at DESC and will also LIMIT 1
      events.where(type: self).last.try(:value) || []
    end

    # Returns the array of addresses by collaboration type
    def current_addresses(account, collaborations, filter_by = :legacy_cc?)
      # NOTE: At this point new users are already persisted in the DB, but
      # in-flight collaborations are still unsaved (and returning `user_id: nil`)
      filtered_collaborations = collaborations.select(&filter_by)

      # Map the entire user object, to force the inverse association search,
      # and get the users (even the ones from in-flight collaborations)
      collaboration_user_ids = filtered_collaborations.map(&:user).map(&:id).uniq

      # Truncated list of primary email addresses
      addresses = primary_email_addresses_from_users(account, collaboration_user_ids)
      truncate(addresses)
    end

    # We should create a change event when users get added/removed as a whole.
    def create?(account, previous_addresses, current_addresses)
      # When at least one of the addresses lists is truncated.
      return true if truncated?(previous_addresses) || truncated?(current_addresses)

      # Compare actual users, not email addresses.
      user_ids_from_addresses(account, previous_addresses) != user_ids_from_addresses(account, current_addresses)
    end

    # Truncates a big array of email addresses
    def truncate(array_of_addresses)
      Zendesk::Events::AuditEvents::ArrayValueTruncation.truncate_value(array_of_addresses, self)
    end

    # Returns true when the list of addresses include '...'
    def truncated?(array_of_addresses)
      Zendesk::Events::AuditEvents::ArrayValueTruncation.ends_with_truncation_indicator?(array_of_addresses)
    end

    # Returns an array of the primary email address of each user provided
    def primary_email_addresses_from_users(account, user_ids)
      account.users.where(id: user_ids).map(&:email).compact&.sort
    end

    # Returns an array of ids for users who have at least one user email identity with addresses appearing in the `addresses` array.
    def user_ids_from_addresses(account, array_of_addresses)
      account.users.joins(:identities).where(user_identities: { account_id: account.id, type: 'UserEmailIdentity', value: array_of_addresses}).pluck(:id).sort.uniq
    end
  end
end
