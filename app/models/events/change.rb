# When a ticket field changes value
class Change < AuditEvent
  include Zendesk::Serialization::ChangeSerialization

  alias_attribute :field_name, :value_reference
  truncate_attributes_to_limit :value, :value_previous # audited columns might contain more than it can hold
  validates_presence_of :value_reference

  attr_accessible :author_id, :is_public, :via_id, :value_reference, :value_previous, :value, :via_reference_id, :transaction, :field_name

  def to_liquid
    hash = Zendesk::Liquid::Wrapper.new("to_s")
    hash.merge!(
      'field_name'     => field_name,
      'value'          => value,
      'value_previous' => value_previous,
      'to_s'           => to_s
    )
    hash
  end

  def to_s(options = {})
    field = value_reference.present? && Zendesk::Fom::Field.for(value_reference.downcase)
    field ? field.event_to_s(self, options).to_s : 'Unrecognized change reference!'
  end

  def changes?(attribute_name, options = {})
    is_match = attribute_name.to_s == value_reference

    if options.key?(:from)
      is_match &&= if options[:from].nil?
        value_previous.nil?
      else
        options[:from].to_s == value_previous.to_s
      end
    end

    if options.key?(:to)
      is_match &&= if options[:to].nil?
        value.nil?
      else
        options[:to].to_s == value.to_s
      end
    end

    is_match
  end

  def self.changes?(events, changes = {})
    changes.all? do |attribute_name, options|
      events.any? do |event|
        event.is_a?(Change) && event.changes?(attribute_name, options || {})
      end
    end
  end
end
