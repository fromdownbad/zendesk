class TicketSharingEvent < AuditEvent
  include Zendesk::Serialization::TicketSharingEventSerialization
  include BaseTicketSharingEvent

  attr_accessor :custom_fields, :skip_shared_ticket_create

  attr_accessible :agreement_id, :custom_fields, :via_id, :via_reference_id, :value, :value_previous, :value_reference

  after_create :create_shared_ticket
  belongs_to :agreement, class_name: 'Sharing::Agreement', foreign_key: :value, inherit: :account_id

  private

  def create_shared_ticket
    return if skip_shared_ticket_create || SharedTicket.find_by_ticket_id_and_agreement_id(ticket.id, agreement_id)
    shared_ticket = SharedTicket.new(
      ticket: ticket,
      agreement_id: agreement_id,
      custom_fields: custom_fields
    )

    shared_ticket.save!
  end
end
