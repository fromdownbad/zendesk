# DEPRECATED. This remains for legacy Twitter event support. See ChannelBackEvent for current version.

class TwitterEvent < AuditEvent
  include Zendesk::Serialization::TwitterEventSerialization

  self.table_name = 'events'
  self.abstract_class = true

  attr_accessor :add_short_url

  fixed_serialize :value, Array

  # The events table has a few fields that are overloaded as needed in
  # subclasses. Here we're using those fields for our own attributes.
  alias_attribute :sender_mth_id,     :value_previous
  alias_attribute :requester_user_id, :value

  # Aliases for consistency. All events use these attribute names.
  alias_attribute :from,       :value_previous
  alias_attribute :recipients, :value
  alias_attribute :body,       :value_reference

  attr_accessible :sender_mth_id, :requester_user_id, :add_short_url, :body, :via_id, :via_reference_id, :from, :recipients, :requester_user_id, :value, :value_previous, :value_reference

  validates_presence_of :sender_mth_id, :requester_user_id

  def to_s
    recipient_names(recipients)
  end
end
