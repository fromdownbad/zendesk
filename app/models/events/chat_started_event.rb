class ChatStartedEvent < ChatEvent
  store :value, accessors: %i[conversation_id chat_id visitor_id chat_start_url ticket_status tags], coder: JSON

  include Tags

  validate :validate_value

  SCHEMA = {
    type: "object",
    properties: {
      conversation_id: { type: 'string' },
      chat_id: { type: 'string' },
      visitor_id: { type: 'string' },
      chat_start_url: { type: 'string' },
      ticket_status: { type: 'string' },
      tags: Tags::SCHEMA
    },
    additionalProperties: false,
    required: %w[chat_id visitor_id],
  }.freeze

  SUPPORTED_TICKET_STATUS_IDS = [
    StatusType.OPEN,
    StatusType.PENDING,
    StatusType.HOLD
  ].freeze

  def validate_value
    JSON::Validator.validate!(SCHEMA, value)
  rescue JSON::Schema::ValidationError, JSON::Schema::SchemaError => err
    errors.add :value, "Invalid Chat Started event: #{err}"
  end

  private

  def initialize_ticket
    ticket.comment = ticket.add_comment(body: ticket_body, is_public: false)
    ticket.subject = ticket_subject
    ticket.description = ticket.comment.body
  end

  def update_ticket_status
    ticket_status_id = StatusType.find(ticket_status)
    # we ignore unsupported ticket statuses rather than throwing an error
    return unless ticket_status_id.in? SUPPORTED_TICKET_STATUS_IDS
    ticket.status_id = ticket_status_id
  end

  protected

  def disable_triggers?
    !account.has_enable_triggers_for_new_chat_ticket?
  end

  def apply_tags
    super
    ticket.additional_tags = tags
    ticket.remove_tags = [*CHAT_ENDED_TAGS, *CHAT_MISSED_TAGS]
  end

  def apply_ticket_changes
    super
    if ticket.new_record?
      initialize_ticket
    else
      update_ticket_status
    end
  end

  def ticket_body
    <<~BODY.chomp
      #{ticket_subject}

      URL: #{chat_start_url || 'None'}
    BODY
  end
end
