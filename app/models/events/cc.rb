# A notification sent to the ticket CCs
class Cc < Notification
  include Zendesk::Serialization::CcSerialization

  def body
    audit.comment
  end

  protected

  def enqueue_ticket_update
    deliver_notifications if deliver?
  end

  def deliver(recipients, for_agent)
    EventsMailer.deliver_follower_notification(
      ticket,
      recipients,
      for_agent,
      self,
      id
    )
  end

  def deliver?
    unless ticket.present?
      Rails.logger.warn("No ticket found for event #{id}, skipping")

      return false
    end

    ticket.account.is_collaboration_enabled? &&
      ticket.current_collaborators.present?
  end
end
