class TicketMergeAudit < Audit
  alias_attribute :merge_type, :value_reference
  alias_attribute :target_id, :via_reference_id

  attr_accessible :merge_type, :target_id

  def source_ids
    value.split(',').map(&:to_i) if merge_type == "target"
  end
end
