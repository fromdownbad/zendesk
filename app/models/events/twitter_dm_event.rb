# DEPRECATED. This remains for legacy Twitter event support. See ChannelBackEvent for current version.

class TwitterDmEvent < TwitterEvent
  self.table_name = 'events'
end
