class ScheduleAssignment < AuditEvent
  alias_attribute :previous_schedule_id, :value_previous
  alias_attribute :new_schedule_id,      :value

  attr_accessible :value_previous,
    :value,
    :via_id,
    :via_reference_id

  def previous_schedule
    Zendesk::BusinessHours::Schedule.find_by_id(previous_schedule_id)
  end

  def new_schedule
    Zendesk::BusinessHours::Schedule.find_by_id(new_schedule_id)
  end
end
