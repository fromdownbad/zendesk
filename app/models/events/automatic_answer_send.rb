class AutomaticAnswerSend < AuditEvent
  fixed_serialize :value, Hash

  before_create :set_type_and_is_public
  validates :suggested_articles, presence: true
  validate :suggested_articles_should_have_required_fields

  ARTICLE_REQUIRED_FIELDS = [:title, :url, :id, :html_url].freeze

  def suggested_articles=(articles)
    self.value = { articles: articles }
  end

  def suggested_articles
    value[:articles] || []
  end

  # Following method 'suggested_articles_with_reviews' method finds the AutomaticAnswerReject events for the deflection
  # articles and uses them to convert the list of suggested articles and AutomaticAnswerReject events
  # to the following format
  # [
  #   {
  #     article: article_1,
  #     reviews: {
  #       enduser: { user_id: 1001, reason: 'irrelevant' },
  #       agent: [{ user_id: 1003, reason: ''}]
  #     }
  #     viewed: false
  #   },
  #   {
  #     article: article_2,
  #     reviews: {
  #       enduser: nil,
  #       agent: [{ user_id: 1003, reason: ''}]
  #     },
  #     viewed: true
  #   },
  #   {
  #     article: article_3,
  #     reviews: {
  #       enduser: nil,
  #       agent: [{ user_id: 1003, reason: ''}]
  #     },
  #     viewed: false
  #   }
  # ]

  def response
    response_body = {}
    response_body = add_articles_to_response(response_body, suggested_articles)

    reject_events, viewed_events = find_reject_and_viewed_events_for_ticket

    response_body = add_reject_events_to_response(response_body, reject_events)
    response_body = add_viewed_events_to_response(response_body, viewed_events)

    response_body.values
  end

  alias_method :body, :response

  private

  def set_type_and_is_public
    self.is_public = true
    self.type = 'AutomaticAnswerSend'
    true
  end

  def suggested_articles_should_have_required_fields
    has_required_structure = suggested_articles.compact.all? { |article| article_should_have_required_fields(article) }

    unless has_required_structure
      errors.add(:suggested_articles, " does not have the required one of the required keys {#{ARTICLE_REQUIRED_FIELDS.join(', ')}}")
    end
  end

  def article_should_have_required_fields(article)
    ARTICLE_REQUIRED_FIELDS.each do |field|
      return unless article.key? field
    end
  end

  def consolidate_agent_rejections(reviews)
    agent_rejections = []
    reviews_grouped_by_agents = reviews.group_by(&:reviewer_id)
    reviews_grouped_by_agents.each do |_reviewer_id, agent_reviews|
      agent_rejections << find_last_reject_review(agent_reviews)
    end

    agent_rejections.compact
  end

  def find_last_reject_review(reviews)
    reviews.sort_by!(&:created_at).last
    final_review = reviews.last if reviews.count > 0 && reviews.last.irrelevant
    if final_review
      { user_id: final_review.reviewer_id, reason: TicketDeflectionArticle::REASON_STRINGS[final_review.reason] || '' }
    end
  end

  def find_reject_and_viewed_events_for_ticket
    reject_and_viewed_events = Event.on_slave.where(
      ticket_id: ticket_id,
      type: ['AutomaticAnswerReject', 'AutomaticAnswerViewed']
    ).all

    reject_and_viewed_events.partition { |event| event.type == 'AutomaticAnswerReject' }
  end

  def add_articles_to_response(response_body, suggested_articles)
    suggested_articles.each do |article|
      response_body[article[:id]] = {
        article: article,
        reviews: {
          enduser: nil,
          agent: []
        },
        viewed: false
      }
    end

    response_body
  end

  def add_reject_events_to_response(response_body, reject_events)
    reject_events_by_article = reject_events.group_by { |event| event.article[:id] }
    reject_events_by_article.each do |article_id, events|
      # Ensure that the article key exists in the response hash.
      next unless response_body[article_id]

      response_body[article_id][:reviews] = reject_info_for_article(events)
    end

    response_body
  end

  def reject_info_for_article(events)
    requester_events, agent_events = events.partition { |event| event.reviewer_id == ticket.requester_id }
    {
      enduser: find_last_reject_review(requester_events),
      agent: consolidate_agent_rejections(agent_events)
    }
  end

  def add_viewed_events_to_response(response_body, viewed_events)
    viewed_events_by_article = viewed_events.group_by { |event| event.article[:id] }
    viewed_events_by_article.each do |article_id, events|
      # Ensure that the article key exists in the response hash.
      next unless response_body[article_id]

      response_body[article_id][:viewed] = events.empty? ? false : true
    end

    response_body
  end
end
