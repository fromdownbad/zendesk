class ChatEndedEvent < ChatEvent
  store :value, accessors: %i[chat_id visitor_id visitor_email is_served rating tags], coder: JSON

  include Tags

  attr_accessible :chat_started_event
  attr_accessor :chat_started_event

  validates :chat_started_event, presence: true, on: :create
  validate :validate_value

  SCHEMA = {
    type: 'object',
    properties: {
      chat_id: { type: 'string' },
      visitor_id: { type: 'string' },
      visitor_email: { type: 'string' },
      is_served: { type: 'bool' },
      rating: {
        type: 'object',
        properties: {
          score: { type: 'string' },
          comment: { type: 'string' }
        },
        required: %w[score],
        additionalProperties: false,
      },
      tags: Tags::SCHEMA
    },
    additionalProperties: false,
    required: %w[chat_id visitor_id]
  }.freeze

  private

  def ticket_subject
    return super if is_served
    return unless valid?

    visitor_name = ticket.requester.name
    ::I18n.t('txt.chat.missed_conversation_title', visitor_name: visitor_name)
  end

  def validate_value
    JSON::Validator.validate!(SCHEMA, value)
  rescue JSON::Schema::ValidationError, JSON::Schema::SchemaError => err
    errors.add :value, "Invalid Chat Ended event: #{err}"
  end

  def apply_system_tags
    ticket.additional_tags = is_served ? CHAT_ENDED_TAGS : CHAT_MISSED_TAGS
  end

  def apply_user_tags
    # remove tags that were present at chat start, but absent at chat end
    ticket.remove_tags = chat_started_event.tags - tags
    # only add tags that were absent at chat start, but present at chat end
    ticket.additional_tags = tags - chat_started_event.tags
  end

  protected

  def apply_tags
    super
    apply_system_tags
    apply_user_tags
  end

  def apply_csat
    return unless rating.present? && account.has_customer_satisfaction_enabled?
    Zendesk::Tickets::Initializer.set_rating(ticket, rating)
  end

  def apply_ticket_changes
    super
    # For ticket via chat, besides missed chats, visitor name may have changed in the meantime
    # For social messaging ticket, subject overriding is not expected
    if chat_channel?
      ticket.subject = ticket_subject
      ticket.requester_data = { email: visitor_email, name: ticket.requester.name } if visitor_email.present?
    end
    apply_csat
  end
end
