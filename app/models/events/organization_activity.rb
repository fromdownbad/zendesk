# a notification sent to organization subscribers
class OrganizationActivity < Notification
  include Zendesk::Serialization::OrganizationActivitySerialization

  attr_accessible :value

  def body
    account.organization_activity_email_template
  end

  protected

  def enqueue_ticket_update
    deliver_notifications if deliver?
  end

  def deliver(recipients, _for_agent)
    EventsMailer.deliver_organization_activity_notification(
      ticket,
      recipients,
      self,
      id
    )
  end

  def deliver?
    unless ticket.present?
      Rails.logger.warn("No ticket found for event #{id}, skipping")

      return false
    end

    true
  end
end
