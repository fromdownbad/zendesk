# DEPRECATED. This remains for legacy Twitter event support. See ChannelBackEvent for current version.

# Outgoing tweet
class Tweet < TwitterEvent
  fixed_serialize :value, Array

  def prepend_handle(screen_name)
    self.body = "@#{screen_name} " + body unless body =~ /(?:\s|^)@#{screen_name}(?:\s|$)/
  end
end
