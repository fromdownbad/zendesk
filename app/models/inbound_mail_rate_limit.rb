class InboundMailRateLimit < ActiveRecord::Base
  has_soft_deletion default_scope: true

  include CIA::Auditable

  audit_attribute :description,
    :email,
    :is_active,
    :is_sender,
    :rate_limit,
    :ticket_id

  belongs_to :account

  attr_accessor :audit_message

  attr_accessible :description,
    :email,
    :is_active,
    :is_sender,
    :rate_limit
end
