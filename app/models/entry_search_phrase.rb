class EntrySearchPhrase < ActiveRecord::Base
  belongs_to :account
  belongs_to :entry, inherit: :account_id

  attr_accessible :account, :entry, :phrase

  validates_presence_of :account_id, :entry, :phrase

  after_save :touch_entry

  def touch_entry
    entry.touch_without_callbacks
  end
end
