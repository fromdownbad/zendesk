require 'zendesk/models/alert'

class Alert < ActiveRecord::Base
  attr_accessible

  # @return [Array] all alerts that should be shown to +user+
  def self.for(user, options = {})
    if user && user.is_agent?
      active.select { |a| a.show_for?(user, options.slice(:interface, :custom)) }
    else
      []
    end
  end

  # assumes user is an agent
  def show_for?(user, options = {})
    (is_active &&
     pod_matches? &&
     shard_matches?(user) &&
     role_matches?(user) &&
     account_type_matches?(user) &&
     plan_matches?(user) &&
     interface_matches?(options) &&
     !user.alert_dismissals.map(&:alert_id).include?(id)
    )
  end

  protected

  def role_matches?(user)
    roles == 'all' ||
      roles.include?(user.role) ||
      roles.include?('Owner') && user.is_account_owner?
  end

  def account_type_matches?(user)
    account = user.account
    account_types == 'all' ||
      account.subscription &&
      account_types.include?(account.subscription.account_type)
  end

  def plan_matches?(user)
    account = user.account
    plans == 'all' ||
      (account &&
       account.subscription &&
       plans.include?(account.subscription.plan_type))
  end

  def shard_matches?(user)
    shards == 'all' ||
      shards.map(&:to_i).include?(user.account.shard_id)
  end

  def pod_matches?
    pods == 'all' ||
      pods.map(&:to_i).include?(Zendesk::Configuration.fetch(:pod_id))
  end

  def interface_matches?(options)
    if custom?
      custom_interface_matches?(options[:custom])
    else
      interface = options[:interface]
      interfaces == 'all' || interface.nil? || interfaces.include?(interface)
    end
  end

  def custom_interface_matches?(custom_interfaces)
    return false if custom_interfaces.blank? # no custom interface requested
    return false if interfaces.blank?        # no interface defined - custom alerts must be explicit

    # this record matches if any of the requested custom interfaces are in the list of interfaces
    (Array(custom_interfaces).map(&:to_s) & Array(interfaces).map(&:to_s)).any?
  end
end
