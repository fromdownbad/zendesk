class AccountLogo < Logo
  setup_attachment_fu(
    content_type: :image,
    path_prefix: 'public/system/logos',
    max_size: 20.megabytes,
    resize_to: '180>',
    thumbnails: {
      thumb: '16x16',
      small: '80>'
    }
  )

  validates_as_attachment

  def self.set_for_account(account, data)
    super(account, account.account_logo, data)
  end

  def self.human_name
    'home logo'
  end
end
