class Logo < ActiveRecord::Base
  include Zendesk::Attachments::FSCompatibility

  belongs_to :account

  attr_accessible :content_type, :filename, :parent_id, :thumbnail, :thumbnail_resize_options, :uploaded_data

  has_kasket_on :account_id, :type

  before_validation :validate_dimensions, prepend: true

  def self.set_for_account(account, last, data)
    return false if data.nil? || data[:uploaded_data].blank?

    last.destroy unless last.nil?

    logo = new
    logo.uploaded_data = data[:uploaded_data]
    logo.account = account
    logo.save!
  end

  def self.inherited(subclass)
    super
    subclass.before_validation :validate_dimensions, prepend: true
    subclass.before_validation :inherit_parent_account_id
    subclass.validates_presence_of :account_id
  end

  def has_parent? # rubocop:disable Naming/PredicateName
    self.class.reflect_on_all_associations.map(&:name).include?(:parent)
  end

  def inherit_parent_account_id
    self.account_id = parent.account_id if has_parent? && parent
  end

  def validate_dimensions
    return if valid_dimensions?
    errors.add(:base, I18n.t('txt.errors.attachments.pixel_range_invalid_2', min: min_resize_pixels, max: max_resize_pixels))
    false
  end
end
