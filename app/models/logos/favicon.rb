class Favicon < Logo
  setup_attachment_fu(
    content_type: [
      :image,
      'image/vnd.microsoft.icon',
      'image/ico',
      'image/icon',
      'text/ico',
      'application/ico',
      'image/x-icon'
    ],
    path_prefix: 'public/system/logos',
    max_size: 20.megabytes
  )

  validates_as_attachment

  def self.set_for_account(account, data)
    super(account, account.favicon, data)
  end

  def self.human_name
    I18n.t('txt.admin.models.logo.favicon.favicon_name')
  end
end
