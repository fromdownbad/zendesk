class TicketFieldCondition < ActiveRecord::Base
  include CachingObserver
  include CIA::Auditable
  include Api::V2::Tickets::AttributeMappings

  belongs_to :account
  belongs_to :ticket_form, inherit: :account_id
  belongs_to :parent_field, class_name: 'TicketField', inherit: :account_id
  belongs_to :child_field, class_name: 'TicketField', inherit: :account_id

  audit_attribute

  attr_accessible :account, :ticket_form, :parent_field, :parent_field_id, :child_field, :child_field_id, :value, :user_type, :is_required, :shaped_required_on_statuses

  # This is to prepare the app to handle required_statuses -> required_on_statuses column name change.
  # The entire app works with the new name.
  # Hopefully we can change the actual column name when we introduce global
  # required on statuses
  # https://zendesk.slack.com/archives/G13GC0X0S/p1581452426176400
  alias_attribute :required_on_statuses, :required_statuses

  before_validation :set_default_is_required_on_create, on: :create, if: :end_user?
  before_validation :keep_conditional_requirement_values_in_sync, if: :agent?

  before_validation :clear_unowned_associations
  validates :account, :value, :user_type, presence: true
  validates :ticket_form, :parent_field, :child_field, presence: { message: I18n.t("activemodel.errors.messages.invalid") }

  validate :value_is_valid_checkbox, if: proc { |condition| condition.parent_field.is_a?(FieldCheckbox) }
  validate :value_is_valid_priority, if: proc { |condition| condition.parent_field.is_a?(FieldPriority) }
  validate :value_is_valid_ticket_type, if: proc { |condition| condition.parent_field.is_a?(FieldTicketType) }

  # We must check if we have a parent_field/child_field explicitly for the following validations because
  # they can be nil if the ticket_field_ids passed in us are not part of the ticket form. See ticket_field method on ticket_form.rb
  validate :parent_field_is_allowed_field, if: proc { |condition| condition.parent_field.present? }
  validate :child_field_is_allowed_field, if: proc { |condition| condition.child_field.present? }
  validate :parent_field_is_active, if: proc { |condition| condition.parent_field.present? }
  validate :child_field_is_active, if: proc { |condition| condition.child_field.present? }
  validate :fields_are_associated_to_ticket_form, if: proc { |condition| condition.parent_field.present? && condition.child_field.present? }
  validate :parent_field_is_not_app_field, if: proc { |condition| condition.parent_field }
  validate :option_belongs_to_tagger, if: proc { |condition| condition.parent_field.is_a?(FieldTagger) }
  validate :parent_child_different, if: proc { |condition| condition.child_field_id_changed? || condition.parent_field_id_changed? }
  validate :parent_field_is_end_user_editable, if: proc { |condition| condition.end_user? && condition.parent_field.present? }
  validate :child_field_is_end_user_editable, if: proc { |condition| condition.end_user? && condition.child_field.present? }
  validate :unique_condition_set
  validate :only_allow_required_on_statuses_for_agents
  validate :required_on_statuses_is_valid

  scope :for_agents, -> { where(user_type: user_types[:agent]) }
  scope :for_end_users, -> { where(user_type: user_types[:end_user]) }
  scope :by_ticket_field, -> (ticket_field) { where(account_id: ticket_field.account_id).where("parent_field_id = ? OR child_field_id = ?", ticket_field.id, ticket_field.id) }

  after_commit :mark_account_as_migrated, on: :create

  enum user_type: [:agent, :end_user]

  ALLOWED_PARENT_FIELD_TYPES = [FieldTagger, FieldPriority, FieldTicketType, FieldCheckbox, FieldTextarea, FieldText].freeze
  ALLOWED_CHILD_FIELD_TYPES = [FieldTagger, FieldPriority, FieldTicketType, FieldCheckbox, FieldTextarea, FieldText, FieldInteger, FieldDecimal, FieldRegexp, FieldPartialCreditCard, FieldDate, FieldMultiselect].freeze
  REQUIRED_ON_ALL_STATUSES = 'ALL_STATUSES'.freeze
  REQUIRED_ON_NO_STATUSES = 'NO_STATUSES'.freeze
  REQUIRED_ON_SOME_STATUSES = 'SOME_STATUSES'.freeze
  REQUIRED_ON_STATUSES_DELIMITER = ' '.freeze
  ALLOWED_REQUIRED_ON_STATUSES_IDS = [StatusType.NEW, StatusType.OPEN, StatusType.PENDING, StatusType.HOLD, StatusType.SOLVED].freeze
  ALLOWED_TICKET_TYPES = (TicketType.fields - [TicketType.-]).map(&:to_s).freeze

  def is_required=(val)
    @is_required_explicitly_set = true
    super val
  end

  def set_migration_in_progress
    @migration_in_progress = true
  end

  def value=(val)
    super(convert_value_for_db(val))
  end

  def required_for?(ticket)
    return is_required? if end_user?

    (required_on_all_statuses? && ticket.operable?) || required_on_status?(ticket.status_id)
  end

  def required_on_status?(status_id)
    required_on_statuses&.include?(status_id.to_s).present?
  end

  # Shapes required_on_statuses for API
  #
  # Required on all statuses:
  #   Value in DB: 'ALL_STATUSES'
  #   Returns: { type: 'ALL_STATUSES' }
  #
  # Required on some statuses:
  #   Value in DB: '0 1'
  #   Returns: { type: 'SOME_STATUSES', statuses: ['new', 'open'] }
  #
  # Not required on any status:
  #   Value in DB: '' or nil
  #   Returns: { type: 'NO_STATUSES' }
  def shaped_required_on_statuses
    status_list = required_on_statuses_list
    if required_on_all_statuses?
      { type: REQUIRED_ON_ALL_STATUSES }
    elsif not_required_on_any_statuses? || status_list.empty?
      { type: REQUIRED_ON_NO_STATUSES }
    else
      {
        type: REQUIRED_ON_SOME_STATUSES,
        statuses: status_list
      }
    end
  end

  # Converts incoming statuses hash to database format and sets the attribute.
  #
  # Required on all statuses:
  #   Incoming: { type: 'ALL_STATUSES' }
  #   Save: "ALL_STATUSES"
  #
  # Required on some statuses:
  #   Incoming: { type: 'SOME_STATUSES', statuses: ['new', 'open', ...] }
  #   Save (space separated list of status ids): "0 1"
  #
  # Not required on any status:
  #   Incoming: { type: 'NO_STATUSES' }
  #   Save: null
  #
  # Lets model validations handle invalid types and statuses.
  def shaped_required_on_statuses=(required_on_statuses)
    self.required_on_statuses =
      case required_on_statuses[:type]
      when REQUIRED_ON_SOME_STATUSES
        statuses = required_on_statuses[:statuses] || []
        statuses.map { |s| StatusType.find(s) || s }.join(REQUIRED_ON_STATUSES_DELIMITER)
      when REQUIRED_ON_NO_STATUSES
        nil
      else
        required_on_statuses[:type]
      end
  end

  def not_required_on_any_statuses?
    required_on_statuses.blank?
  end

  def required_on_all_statuses?
    required_on_statuses == REQUIRED_ON_ALL_STATUSES
  end

  private

  # Select statuses to display to user
  # If account has hold statuses disabled, filters out
  # hold status from status list.
  def required_on_statuses_list
    return [] if not_required_on_any_statuses?
    statuses = required_on_statuses.split(REQUIRED_ON_STATUSES_DELIMITER)
    if required_on_status?(StatusType.HOLD) && !account&.use_status_hold?
      statuses.reject! { |s| s == StatusType.HOLD.to_s }
    end
    statuses.map { |s| STATUS_MAP[s] }
  end

  def set_default_is_required_on_create
    unless @is_required_explicitly_set
      self.is_required = child_field.is_required_in_portal?
    end
    true # don't fail validation
  end

  # Keep is_required and required_on_statuses in sync.
  # After GA we can start deprecating is_required.
  def keep_conditional_requirement_values_in_sync
    self.is_required = required_on_all_statuses? || required_on_status?(StatusType.SOLVED)
    true # don't fail validation
  end

  def mark_account_as_migrated
    return if @migration_in_progress

    unless account.settings.native_conditional_fields_migrated?
      account.update_attributes(settings: { native_conditional_fields_migrated: true })
    end
  end

  def convert_value_for_db(val)
    return val if parent_field.nil?

    parent_field.condition_value_to_db(val)
  end

  # If parent or child fields belong to a different account, clear those values
  def clear_unowned_associations
    return if account_id.blank?

    [:parent_field, :child_field].each do |association|
      if send(association) && send(association).account_id != account_id
        send("#{association}=", nil)
      end
    end
  end

  def option_belongs_to_tagger
    if value.present? && !parent_field.option_belongs_to_tagger(value)
      errors.add(:value, I18n.t("txt.error_message.models.ticket_field_condition.value_inclusion_v2", value: value))
    end
  end

  def parent_field_is_allowed_field
    unless ALLOWED_PARENT_FIELD_TYPES.include?(parent_field.class)
      errors.add(:parent_field, I18n.t("txt.error_message.models.ticket_field_condition.parent_field_is_not_supported_type", field_name: parent_field_name_for_error))
    end
  end

  def child_field_is_allowed_field
    unless ALLOWED_CHILD_FIELD_TYPES.include?(child_field.class)
      errors.add(:child_field, I18n.t("txt.error_message.models.ticket_field_condition.child_field_is_not_supported_type", field_name: child_field_name_for_error))
    end
  end

  def parent_field_is_not_app_field
    if parent_field.app_field?
      errors.add(:parent_field, I18n.t("txt.error_message.models.ticket_field_condition.parent_field_is_not_app_field_v2", field_name: parent_field_name_for_error))
    end
  end

  def parent_child_different
    if parent_field_id == child_field_id
      errors.add(:child_field, I18n.t("txt.error_message.models.ticket_field_condition.parent_child_different_v2", field_name: child_field_name_for_error))
    end
  end

  def parent_field_is_active
    unless parent_field.is_active?
      errors.add(:parent_field, I18n.t("txt.error_message.models.ticket_field_condition.ticket_field_is_not_active", field_name: parent_field_name_for_error))
    end
  end

  def child_field_is_active
    unless child_field.is_active?
      errors.add(:child_field, I18n.t("txt.error_message.models.ticket_field_condition.ticket_field_is_not_active", field_name: child_field_name_for_error))
    end
  end

  def parent_field_is_end_user_editable
    unless parent_field.is_editable_in_portal
      errors.add(:parent_field, I18n.t("txt.error_message.models.ticket_field_condition.ticket_field_is_not_end_user_editable", field_name: parent_field_name_for_error))
    end
  end

  def child_field_is_end_user_editable
    unless child_field.is_editable_in_portal
      errors.add(:child_field, I18n.t("txt.error_message.models.ticket_field_condition.ticket_field_is_not_end_user_editable", field_name: child_field_name_for_error))
    end
  end

  def fields_are_associated_to_ticket_form
    return unless ticket_form.present?
    ticket_field_ids = ticket_form.ticket_form_fields.map(&:ticket_field_id)

    unless ticket_field_ids.include?(parent_field_id)
      errors.add(:parent_field, I18n.t("txt.error_message.models.ticket_field_condition.field_is_not_in_ticket_form_v2", field_name: parent_field_name_for_error))
    end

    unless ticket_field_ids.include?(child_field_id)
      errors.add(:child_field, I18n.t("txt.error_message.models.ticket_field_condition.field_is_not_in_ticket_form_v2", field_name: child_field_name_for_error))
    end
  end

  def only_allow_required_on_statuses_for_agents
    if required_on_statuses.present? && !agent?
      errors.add(:child_field, I18n.t("txt.error_message.models.ticket_field_condition.required_on_statuses_only_for_agent", field_name: child_field_name_for_error))
    end
  end

  def required_on_statuses_is_valid
    return if not_required_on_any_statuses?
    return if required_on_all_statuses?
    return if required_on_statuses_is_valid_status_list?
    errors.add(:child_field, I18n.t("txt.error_message.models.ticket_field_condition.required_on_statuses_only_includes_valid_statuses", field_name: child_field_name_for_error))
  end

  def value_is_valid_checkbox
    unless ["0", "1"].include?(value)
      errors.add(:value, I18n.t("txt.error_message.models.ticket_field_condition.value_inclusion_v2", value: value))
    end
  end

  def value_is_valid_priority
    unless valid_priority_types.include?(value)
      errors.add(:value, I18n.t("txt.error_message.models.ticket_field_condition.value_inclusion_v2", value: value))
    end
  end

  def valid_priority_types
    @valid_priority_types ||= begin
      field_priority = account.field_priority
      if field_priority.sub_type_id == PrioritySet.BASIC
        [PriorityType.NORMAL, PriorityType.HIGH].map(&:to_s)
      else
        (PriorityType.fields - [PriorityType.-]).map(&:to_s)
      end
    end
  end

  def value_is_valid_ticket_type
    unless ALLOWED_TICKET_TYPES.include?(value)
      errors.add(:value, I18n.t("txt.error_message.models.ticket_field_condition.value_inclusion_v2", value: value))
    end
  end

  # Can't use Rails' uniqueness validators because I18n string requires attributes to be passed in which blows
  # everything up
  def unique_condition_set
    return if account_id.blank? || ticket_form_id.blank? || parent_field_id.blank? || child_field_id.blank? || user_type.blank? || value.blank?

    # Validate uniqueness on parent_field-child_field-value within a ticket form & account context
    # NOTE: Do not change query column order here, this is set up to use indexes on account_id and ticket_form_id
    uniqueness_query = self.class.where(
      account_id: account_id,
      ticket_form_id: ticket_form_id,
      parent_field_id: parent_field_id,
      child_field_id: child_field_id,
      user_type: self[:user_type],
      value: value
    )

    unless new_record?
      uniqueness_query = uniqueness_query.where("id != ?", id)
    end

    is_invalid = uniqueness_query.any?
    if is_invalid
      errors.add(
        :value,
        I18n.t(
          "txt.error_message.models.ticket_field_condition.condition_set_already_exists",
          parent_name: parent_field.try(:title),
          child_name: child_field.try(:title),
          user_type: user_type,
          value: value
        )
      )
    end

    !is_invalid
  end

  ## Validation Helpers
  def parent_field_name_for_error
    parent_field.try(:title) || parent_field_id
  end

  def child_field_name_for_error
    child_field.try(:title) || child_field_id
  end

  # Checks all required_on_statuses are valid status ids and there are no duplications
  def required_on_statuses_is_valid_status_list?
    status_ids = required_on_statuses.split(REQUIRED_ON_STATUSES_DELIMITER)
    allowed_status_ids = ALLOWED_REQUIRED_ON_STATUSES_IDS.map(&:to_s)

    (allowed_status_ids & status_ids).size == status_ids.size
  end
end
