require 'active_support/concern'

module Account::ExploreSupport
  extend ActiveSupport::Concern
  include ZBC::KnowledgeCapture::AppMarketIntegration

  included do
    alias_method :knowledge_capture_app_installed?, :app_installed?
  end

  # ZBC::KnowledgeCapture::AppMarketIntegration requires an `account` method
  def account
    self
  end

  def eligible_for_explore_lite?
    return false unless explore_insights?
    qualifying_support_config? || qualifying_talk_config? || qualifying_guide_config?
  end

  def enterprise_explore?
    support_enterprise_plan? && explore_plan_setting?
  end

  def enterprise_good_data_and_explore?
    support_enterprise_plan? && good_data_and_explore_plan_setting?
  end

  def professional_support_plus_explore?
    support_professional_plan? &&
      (explore_plan_setting? || good_data_and_explore_plan_setting?) &&
      active_explore_product?
  end

  def enterprise_support_plus_explore?
    support_enterprise_plan? &&
      (explore_plan_setting? || good_data_and_explore_plan_setting?) &&
      active_explore_product?
  end

  def explore_plan_setting?
    support_product.present? &&
      (support_product.plan_settings['insights'] == 'explore')
  end

  def good_data_and_explore_plan_setting?
    support_product.present? &&
     (support_product.plan_settings['insights'] == 'good_data_and_explore')
  end

  private

  def explore_insights?
    return unless support_product.present?
    Api::V2::Products::Suite::EXPLORE_INSIGHTS_WHITELIST.include?(
      support_product.plan_settings['insights']
    )
  end

  def active_explore_product?
    explore_product.present? && explore_product.active?
  end

  def explore_product
    return @explore_product if defined?(@explore_product)
    @explore_product = products.find { |p| p.name == Zendesk::Accounts::Client::EXPLORE_PRODUCT.to_sym }
  end

  def support_product
    return @support_product if defined?(@support_product)
    @support_product = products.find { |p| p.name == Zendesk::Accounts::Client::SUPPORT_PRODUCT.to_sym }
  end

  def qualifying_support_config?
    [ZBC::Zendesk::PlanType::Professional, ZBC::Zendesk::PlanType::Enterprise].
      map(&:plan_type).include?(subscription.plan_type)
  end

  def qualifying_talk_config?
    [ZBC::Voice::PlanType::Professional, ZBC::Voice::PlanType::Enterprise].
      map(&:plan_type).include?(voice_subscription.try(:plan_type))
  end

  def qualifying_guide_config?
    (ZBC::Guide::PlanType::Professional.plan_type == guide_subscription.try(:plan_type)) &&
      (knowledge_capture_app_installed? || answer_bot_subscription.present?)
  end

  def support_enterprise_plan?
    subscription.plan_type == ZBC::Zendesk::PlanType::Enterprise.plan_type
  end

  def support_professional_plan?
    subscription.plan_type == ZBC::Zendesk::PlanType::Professional.plan_type
  end
end
