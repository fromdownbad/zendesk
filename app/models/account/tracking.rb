require 'time_difference'
require_relative 'tracking/properties'

class Account < ActiveRecord::Base
  module AccountTracking
    include Account::AccountTracking::Properties

    def exclude_account?
      subdomain.downcase =~ /z3n/ || name.downcase =~ /z3n/ || owner.email =~ /zendesk.com$/
    end

    def customer_type
      return 'Unknown' unless help_desk_size
      low, high = help_desk_size.scan(/\d+/).map(&:to_i)
      high ||= low || 0

      if (1..9).cover?(high) || help_desk_size.downcase =~ /small/ then 'VSB'
      elsif (10..250).cover?(high) || help_desk_size.downcase =~ /medium/ then 'SMB'
      elsif high > 251 || help_desk_size.downcase =~ /large/ then 'MM'
      else
        'Unknown'
      end
    end

    private

    def time_to_action(options = {})
      options.reverse_merge!(
        start_time: Time.now,
        based_on: created_at
      )
      TimeDifference.between(*options.values_at(:start_time, :based_on))
    end
  end
end
