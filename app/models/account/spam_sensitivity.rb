# TODO: Rename this module
module Account::SpamSensitivity
  delegate :spam_threshold_multiplier, to: :settings

  RSPAMD = "Rspamd".freeze
  CLOUDMARK = "Cloudmark".freeze
  DEFAULT_SPAM_MULTIPLIER = 1.0

  def spam_protection_tool
    has_email_rspamd? ? RSPAMD : CLOUDMARK
  end
end
