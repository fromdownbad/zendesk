class Account
  attr_accessor :remote_login_url

  module Authentication
    def self.included(base)
      base.class_eval do
        extend ClassMethods
        delegate :is_google_login_enabled, :is_google_login_enabled=, :is_google_login_enabled?,
          :is_facebook_login_enabled, :is_facebook_login_enabled=, :is_facebook_login_enabled?,
          :admins_can_set_user_passwords, :admins_can_set_user_passwords=,
          :email_agent_when_sensitive_fields_changed, :email_agent_when_sensitive_fields_changed=,
          :set_long_hsts_header_on_host_mapping, :set_long_hsts_header_on_host_mapping=,
          :automatic_certificate_provisioning, :automatic_certificate_provisioning=, to: :settings

        delegate :login_allowed_for_role?,
          :no_login_service_allowed_for_role?,
          :login_allowed_for_any_role?,
          :only_login_service_allowed_for_role?,
          :login_allowed_only_for_role?, to: :role_settings

        has_many :remote_authentications, dependent: :destroy

        has_one :custom_security_policy, dependent: :destroy

        kasket_dirty_methods :dirty_update_last_login
      end
    end

    module ClassMethods
      # a simplified version of what goes on in the account_middleware
      # Note: remove this after we switch to using routes instead, since we have the same code in route.rb
      def find_by_fqdn(name)
        if name =~ /([^\.]*?)\.#{Zendesk::Configuration.fetch(:host)}$/
          # regular old subdomain lookup
          find_by_subdomain($1)
        else
          find_by_host_mapping(name)
        end
      end
    end

    def available_security_policies
      policies = Zendesk::SecurityPolicy.all.map do |policy|
        policy.new(self)
      end
      policies.reject { |policy| policy.level?(:custom) && !has_custom_security_policy? }
    end

    def ip_ranges=(value)
      texts.ip_restriction = value
    end

    def ip_ranges
      texts.ip_restriction
    end

    def logout_all_users_and_destroy_tokens!
      TerminateAllSessionsJob.enqueue(id, true)
    end

    def logout_all_users!
      TerminateAllSessionsJob.enqueue(id, false)
    end

    # support these methods for migration, until we remove the column
    def last_login
      on_shard { settings.last_login }
    end

    def last_login=(val)
      settings.last_login = val
    end

    def update_last_login
      if Arturo.feature_enabled_for?(:last_login_readonly_check, self) && settings.last_login_record.readonly?
        Rails.logger.warn("account settings last_login is readonly")
      else
        dirty_update_last_login if last_login.nil? || (last_login < 60.minutes.ago)
      end
    end

    # This is a separate method which is marked dirty by kasket to ensure kasket integrity
    def dirty_update_last_login
      Rails.logger.info("Updating last_login from #{last_login} to #{Time.now.utc}")
      self.last_login = Time.now.utc
      settings.last_login_record.save!
    end

    def source_allows_delayed_verification?
      Zendesk::Accounts::Source.zendesk_app_user_agent?(source)
    end

    def sign_in_requires_verification?
      !(source_allows_delayed_verification? && created_at > 24.hours.ago)
    end

    def agent_security_policy
      Zendesk::SecurityPolicy.find(
        role_settings.agent_security_policy_id,
        self
      )
    end

    def end_user_security_policy
      Zendesk::SecurityPolicy.find(
        role_settings.end_user_security_policy_id,
        self
      )
    end

    def two_factor_last_update
      settings.find_by_name('two_factor_all_agents').try(:updated_at) || created_at
    end
  end
end
