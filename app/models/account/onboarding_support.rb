class Account
  module OnboardingSupport
    # onboarding-capabilities; see: settings.checklist_onboarding_version
    # CHECKLIST_V1 CHECKLIST_V2 and SUITE_V2 are no longer being used, but keep them available for version tracking
    CHECKLIST_V1 = 1
    CHECKLIST_V2 = 2
    SUITE_V2 = 3
    SUPPORT_SUITE_TRIAL_REFRESH = 4
    SUPPORT_SUITE_TRIAL_REFRESH_PHASE2 = 5

    def onboarding_url
      return '/agent/admin/mobile_sdk' if trial_extras_feature?('sdk')
      return '/voice/admin/onboarding' if trial_extras_product_sign_up?('zendesk_talk')
      return '/hc/start' if trial_extras_product_sign_up?('zendesk_guide')
      return '/hc/start?gather=true' if trial_extras_product_sign_up?('zendesk_gather')
      return direct_buy_url('support') if trial_extras_buy_now?('zendesk_support')
      return direct_buy_url('zendesk_suite') if trial_extras_buy_now?('zendesk_suite')
      return '/agent/get-started/sample-ticket' if settings.suite_trial?
    end

    private

    def direct_buy_url(product_name)
      "/billing/entry/purchase/#{product_name}?plan=#{trial_extras_plan}"
    end
  end
end
