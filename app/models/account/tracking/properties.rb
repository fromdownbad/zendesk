class Account < ActiveRecord::Base
  module AccountTracking
    module Properties
      def basic_properties(user_id)
        {
          subdomain: subdomain,
          help_desk_size: help_desk_size,
          customer_type: customer_type,
          account_created: tracking_time_format(created_at),
          account_created_week: tracking_weekly_group_format(created_at),
          account_created_month: tracking_monthly_group_format(created_at),
          locale_id: locale_id,
          language: translation_locale.name,
          account_id: id,
          user_id: user_id,
          signup_source: source,
          is_partner: via_partner?
        }.tap do |basics|
          if user = (users.find(user_id) rescue false)
            basics.merge!(
              identity: user.user_handle,
              user_role: user.role,
              created: tracking_time_format(user.created_at),
              created_week: tracking_weekly_group_format(user.created_at),
              created_month: tracking_monthly_group_format(user.created_at),
              is_owner: owner.id == user_id,
              is_end_user: user.is_end_user?
            )
          end
        end
      end

      def voice_subscription_properties
        voice_data = Voice::VoiceAccount.remote_data_for_account(self)
        date_params = voice_date_params(voice_data.try(:created_at))

        {
          voice_plan_type: voice_data.try(:plan_type),
          voice_plan_name: voice_data.try(:plan_name),
          voice_is_trial:  voice_data.try(:trial),
          voice_max_agents: voice_data.try(:max_voice_agents)
        }.merge(date_params)
      end

      def subscription_properties
        {}.tap do |prop|
          if subscription
            prop.merge!(
              account_type: subscription.account_type,
              plan_type: subscription.plan_type,
              plan_name: subscription.plan_name,
              is_trial: subscription.is_trial,
              max_agents: subscription.max_agents,
              bc_type: subscription.billing_cycle_type
            )
          end
        end
      end

      def split_testing_properties
        {}.tap do |prop|
          Experiment.where(account_id: id).each do |exp|
            version = exp.version.empty? ? '' : "_#{exp.version}"
            prop.merge!(
              "ab_#{exp.name}#{version}": exp.group,
              "ab_#{exp.name}#{version}_finished": exp.finished?
            )
          end
        end
      end

      def time_to_action_properties(user_id = -1)
        tta_account = time_to_action
        tta_account = {
          tta_in_days: tta_account.in_days,
          tta_in_hours: tta_account.in_hours,
          tta_in_minutes: tta_account.in_minutes
        }
        if user_id > 0 && (user = (users.find(user_id) rescue false))
          tta_user = time_to_action(based_on: user.created_at)
          tta_account.merge!(
            tta_user_in_days: tta_user.in_days,
            tta_user_in_hours: tta_user.in_hours,
            tta_user_in_minutes: tta_user.in_minutes
          )
        end
        tta_account
      end

      #
      # This like uses key indexes according to official document
      # http://dev.mysql.com/doc/refman/5.0/en/mysql-indexes.html
      #

      def trial_extra_properties
        prop = trial_extras.where('trial_extras.key like ?', 'mixpanel_%').each_with_object({}) do |extra, hash|
          hash[extra.key[/mixpanel_(.+)/, 1]] = extra.value
        end

        if features = trial_extras.where(key: 'features').first
          features.value.gsub(/\s/, '').split(/,/).each_with_index do |feature, idx|
            prop["feature#{idx}"] = feature
          end
        end

        if convertro_id = trial_extras.where(key: 'Convertro_SID__c').first
          prop["convertroId"] = convertro_id.value
        end

        prop
      end

      #
      # Session_landing__c is the landing url
      # For instance 'http://www.zendesk.com/?utm_source=infusionsoft&amp;utm_medium=appstore&amp;utm_campaign=profile'
      #

      def landing_utm_properties
        trial_extras.where(key: 'Session_Landing__c').each_with_object({}) do |extra, props|
          landing_url = extra.value
          next if landing_url.blank?
          next unless uri = (URI.parse(landing_url) rescue nil)
          if uri.host
            props['landing_site'] = uri.host
            props['landing_path'] = uri.fragment ? "#{uri.path}##{uri.fragment}" : uri.path
          end
          props.merge!(Hash[uri.query.scan(/(utm_\w+)=(\w+)/)]) if uri.query
        end
      end

      def support_user_properties(user_id)
        {
          user_id: user_id,
          support_language: translation_locale.name,
          support_locale_id: locale_id,
          support_locale: translation_locale.locale.downcase
        }.tap do |basics|
          if user = (users.find(user_id) rescue false)
            if user_language = user.translation_locale.try(:name)
              basics[:support_locale] = user.locale.downcase
              basics[:support_locale_id] = user.locale_id
              basics[:support_language] = user_language
            end

            basics.merge!(
              support_is_admin: user.is_admin?,
              support_is_end_user: user.is_end_user?,
              support_is_owner: owner.id == user_id,
              support_user_role: user.role
            )
          end
        end
      end

      def support_group_properties
        {
          account_id: id,
          account_name: name,
          language: translation_locale.name,
          locale_id: locale_id,
          signup_source: source,
          subdomain: subdomain,
          company_size: help_desk_size,
          has_active_suite: has_active_suite?
        }
      end

      def support_subscription_properties
        {}.tap do |prop|
          if subscription
            prop.merge!(
              support_account_created: tracking_time_format(subscription.created_at),
              support_account_created_week: tracking_weekly_group_format(subscription.created_at),
              support_account_created_month: tracking_monthly_group_format(subscription.created_at),
              support_account_type: subscription.account_type,
              support_max_agents: subscription.max_agents,
              support_plan_name: subscription.plan_name,
              support_plan_type: subscription.plan_type
            )
          end
        end
      end

      def guide_subscription_properties
        {}.tap do |prop|
          if guide_subscription
            prop.merge!(
              guide_account_created: tracking_time_format(guide_subscription.created_at),
              guide_account_created_week: tracking_weekly_group_format(guide_subscription.created_at),
              guide_account_created_month: tracking_monthly_group_format(guide_subscription.created_at),
              guide_max_agents: guide_subscription.max_agents,
              guide_plan_name: guide_subscription.plan_name,
              guide_plan_type: guide_subscription.plan_type
            )
          end
        end
      end

      def chat_subscription_properties
        {}.tap do |prop|
          if zopim_subscription
            prop.merge!(
              chat_account_created: tracking_time_format(zopim_subscription.created_at),
              chat_account_created_week: tracking_weekly_group_format(zopim_subscription.created_at),
              chat_account_created_month: tracking_monthly_group_format(zopim_subscription.created_at),
              chat_max_agents: zopim_subscription.max_agents,
              chat_plan_name: zopim_subscription.present_plan_name,
              chat_plan_type: zopim_subscription.present_plan_type
            )
          end
        end
      end

      def talk_subscription_properties
        talk_data = Voice::VoiceAccount.remote_data_for_account(self)
        {}.tap do |prop|
          if talk_data
            prop.merge!(
              talk_max_agents: talk_data.try(:max_voice_agents),
              talk_plan_name: talk_data.try(:plan_name),
              talk_plan_type: talk_data.try(:plan_type)
            )
            if talk_data.created_at
              prop.merge!(
                talk_account_created: tracking_time_format(talk_data.created_at),
                talk_account_created_week: tracking_weekly_group_format(talk_data.created_at),
                talk_account_created_month: tracking_monthly_group_format(talk_data.created_at)
              )
            end
          end
        end
      end

      private

      def voice_date_params(created_at)
        return {} unless created_at

        {
          voice_created_at: tracking_time_format(created_at),
          voice_created_week: tracking_weekly_group_format(created_at),
          voice_created_month: tracking_monthly_group_format(created_at)
        }
      end

      def tracking_time_format(time)
        time.strftime('%Y-%m-%dT%H:%M:%S')
      end

      def tracking_weekly_group_format(time)
        time.strftime('%G-%WW')
      end

      def tracking_monthly_group_format(time)
        time.strftime('%Y%b')
      end
    end
  end
end
