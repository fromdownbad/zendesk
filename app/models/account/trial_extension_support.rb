class Account
  module TrialExtensionSupport
    def self.included(base)
      base.class_eval do
        has_many :trial_extensions, dependent: :destroy, inverse_of: :account
      end
    end
  end
end
