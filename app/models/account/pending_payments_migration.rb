class PendingPaymentsMigration
  def self.create(dry_run = true)
    logger = Logger.new("#{Rails.root}/log/spoiled_accounts.txt")

    spoiled_accounts = []

    Account.includes(:payments).where(is_serviceable: true, sandbox_master_id: nil, is_active: true).where('id > 0') do |acc|
      spoiled_accounts << acc if acc.payments.pending.standard.recurring.count(:all) < 1
    end

    spoiled_accounts.each do |saccount|
      saccount.on_shard do |sacc|
        last_paid = sacc.payments.paid.standard.recurring.order(:period_end_at).last

        if last_paid.nil?
          puts "Account #{sacc.id} doesn't have a last paid payment"
          logger.info("Account #{sacc.id} doesn't have a last paid payment")

        elsif sacc.subscription.invoicing?
          puts "Invoiced account #{sacc.id} found without pending payment"
          logger.info("Invoiced account #{sacc.id}found without pending payment")
        else
          puts "Last paid payment for account #{sacc.id} ended at  #{last_paid.period_end_at}"
          logger.info(" #{dry_run ? "DRYRUN:" : ""} Creating the successive payment for #{last_paid.id} payment with period ending at #{last_paid.period_end_at} for account #{sacc.id}")
          last_paid.create_next! unless dry_run
        end
      end
    end
  end
end
