class Account < ActiveRecord::Base
  module TicketSharingSupport
    NON_SHARING_STATE_TYPES = DeliverableStateType.list([DeliverableStateType.TICKET_SHARING_PARTNER])

    def self.included(base)
      base.class_eval do
        has_many :sharing_agreements, class_name: 'Sharing::Agreement'
        has_many :shared_tickets, class_name: 'SharedTicket'
      end
    end

    def reject_sharing_invites!
      settings.lookup(:reject_sharing_invites).enable
    end

    def reject_sharing_invites?
      settings.reject_sharing_invites?
    end

    def sharing_agreements_with(partner)
      agreements = sharing_agreements.select { |sa| sa.shared_with_id == Sharing::Agreement::SHARED_WITH_MAP[partner] }
      agreements || []
    end

    def sharing_url
      url(ssl: true, mapped: false) + '/sharing'
    end

    def sharing_name
      name + " @ Zendesk"
    end

    def refresh_ticket_sharing_partner_support_addresses
      partner_support_addresses = sharing_agreements.accepted.flat_map(&:support_addresses).compact.uniq
      errors_removed = partner_support_addresses.reject! { |result| result.is_a?(TicketSharing::FakeHttpResponse) }

      mark_identities(partner_support_addresses, !errors_removed)

      Rails.cache.write(support_addresses_cache_key, partner_support_addresses, expires_in: 1.day)
    end

    def ticket_sharing_partner_support_addresses
      return [] if sharing_agreements.empty?
      Rails.cache.fetch(support_addresses_cache_key, expires_in: 1.day) do
        TicketSharingSupportAddressesJob.enqueue(id)
        known_ticket_sharing_partner_identities
      end
    end

    protected

    def support_addresses_cache_key
      "account/#{id}/ticket_sharing_partner_support_addresses"
    end

    def deactivate_sharing_agreements!
      status_ids = Sharing::Agreement::STATUS_MAP.values_at(:pending, :accepted)
      inbound = sharing_agreements.where(status_id: status_ids)
      outbound = Sharing::Agreement.outbound.where(uuid: inbound.map(&:uuid))
      agreement_ids = [inbound.map(&:id), outbound.map(&:id)].flatten

      Sharing::Agreement.where(id: agreement_ids).update_all(
        deactivated_at: Time.now,
        status_id: Sharing::Agreement::STATUS_MAP[:inactive],
        deactivated_type: true # Initiated "locally" by account holder
      )
    end

    def mark_identities(partner_support_addresses, update_deliverable)
      old_partner_support_addresses = known_ticket_sharing_partner_identities
      modified_addresses_and_states = (partner_support_addresses - old_partner_support_addresses).each_with_object({}) do |address, h|
        h[address] = DeliverableStateType.TICKET_SHARING_PARTNER
      end

      if update_deliverable
        (old_partner_support_addresses - partner_support_addresses).each_with_object(modified_addresses_and_states) do |address, h|
          h[address] = DeliverableStateType.DELIVERABLE
        end
      end

      return unless modified_addresses_and_states.any?

      user_email_identities.where(value: modified_addresses_and_states.keys).each do |identity|
        identity.mark_deliverable_state(modified_addresses_and_states[identity.value])
      end
    end

    def known_ticket_sharing_partner_identities
      ActiveRecord::Base.on_slave do
        user_email_identities.ticket_sharing_partners.pluck(:value)
      end
    end
  end
end
