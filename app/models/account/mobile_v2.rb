class Account
  module MobileV2
    def self.included(base)
      base.attr_accessible :mobile_title
    end

    def mobile_title=(var)
      settings.mobile_title = var
    end

    def mobile_title
      settings.mobile_title
    end
  end
end
