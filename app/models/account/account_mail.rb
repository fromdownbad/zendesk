class Account
  module AccountMail
    def self.included(base)
      base.class_eval do
        has_many :inbound_emails
        has_many :user_email_identities, class_name: "UserEmailIdentity"
        has_many :recipient_addresses, dependent: :delete_all

        validates_length_of :mail_delimiter, in: 20..65, if: :mail_delimiter_changed?
        before_validation   :strip_mail_delimiter, on: :update, if: :mail_delimiter_changed?
        after_save          :persist_default_recipient_address_update
        after_commit        :update_recipient_addresses_on_subdomain_change, on: :update

        attr_accessible :recipient_addresses_attributes
        accepts_nested_attributes_for :recipient_addresses,
          allow_destroy: true,
          reject_if: lambda { |attributes| attributes[:value].blank? }
      end
    end

    def email_address_in_use_error(email_address)
      if recipient_emails.include?(email_address)
        I18n.t("activerecord.errors.models.user_identity.attributes.value.in_use_as_a_support_address", value: email_address)
      end
    end

    def recipient_emails
      recipient_addresses.pluck(:email)
    end

    def recipient_email?(email)
      recipient_addresses.where(email: email).exists?
    end

    delegate :backup_email_address, :default_recipient_address, :reply_address, to: :default_brand, allow_nil: true

    def domain_brand_map
      Hash[routes.map { |r| [r.default_host, r.brand.try(:id) || default_brand_id] }]
    end

    def default_hosts
      @default_hosts ||= begin
        branded_routes = brands.map(&:route_id).compact
        routes.select { |r| branded_routes.include?(r.id) }.map(&:default_host)
      end
    end

    protected

    def update_recipient_addresses_on_subdomain_change
      return if is_sandbox?
      return unless change = previous_changes["subdomain"]

      recipient_addresses.each do |r|
        next unless change.first && r.email.end_with?("@#{change.first.downcase}.#{Zendesk::Configuration.fetch(:host)}")
        new_email = r.email.split("@").first << "@" << default_host
        begin
          r.update_column(:email, new_email)
        rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation => e
          ZendeskExceptions::Logger.record(e, location: self, message: "update_recipient_addresses_on_subdomain_change failed to update recipient address due to duplicate: #{r.email} -> #{new_email}", fingerprint: '4d06fdc502f53a81d4e66940ed7a81b1e999b65c')
        end
      end
    end

    def persist_default_recipient_address_update
      return unless @reply_address_was_changed

      if default_recipient_address.marked_for_destruction?
        default_recipient_address.destroy
      else
        default_recipient_address.try(:save)
      end
    end

    def strip_mail_delimiter
      mail_delimiter.strip!
    end
  end
end
