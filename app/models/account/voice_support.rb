class Account < ActiveRecord::Base
  module VoiceSupport
    extend ActiveSupport::Concern

    included do
      after_validation :mark_available_agents_unavailable, if: :update_disables_voice?, on: :update

      after_commit :update_callback_url_if_needed, on: :update, if: :has_voice?
      after_commit :suspend_sub_account,                        unless: :is_serviceable?
      before_save :ensure_active_zuora_talk_usage_subscription,
        on: :update, if: :reactivating_purchased_account_with_talk?
    end

    def mark_available_agents_unavailable
      api_client.connection.post do |req|
        req.url '/api/v2/channels/voice/internal/sub_accounts/disable_voice.json'
        req.options.timeout = 2.seconds
      end
    end

    def update_disables_voice?
      settings.one? do |setting|
        setting.changed? &&
          setting.name == 'voice' &&
          setting.value == '0'
      end
    end

    def suspend_sub_account
      return unless voice_sub_account_active?
      Rails.logger.info("account_id: #{id}. Suspending a voice sub account.")
      api_client.connection.put('/api/v2/channels/voice/internal/sub_accounts/suspend')
    end

    def enable_voice
      settings.voice = true
      settings.save
    end

    def voice_feature_enabled?(feature)
      # We experienced some performance difficulties with checking user_seats by making HTTP requests,
      # it was hitting Memcached intensively. Therefore we decided to check whether the user seat is enabled
      # by reaching out the database directly.
      #
      # https://zendesk.atlassian.net/browse/ICTF-80
      #
      if feature.to_sym == :user_seats && voice_account.present? && has_voice_read_user_seats_from_the_db?
        return voice_account.user_seats_enabled?
      end

      feature_mapper.feature_enabled?(feature)
    end

    def voice_enabled?
      return @voice_enabled if defined?(@voice_enabled)

      @voice_enabled = voice_account.present? && voice_sub_account_active?
    end

    def voice_sub_account_active?
      voice_sub_account.try(:activated?)
    end

    def voice_account
      @voice_account ||= Voice::VoiceAccount.find_by(account_id: id)
    end

    private

    def feature_mapper
      @feature_mapper ||= Voice::FeatureMapper.new(self)
    end

    def update_callback_url_if_needed
      return unless previous_changes.key?('subdomain')
      api_client.connection.put('/api/v2/channels/voice/internal/sub_accounts/update_subdomain')
    rescue => e
      # Since this is called from an after_commit, any exception will be silently swallowed
      # We don't really want this because it would be a big problem, so better report it
      ZendeskExceptions::Logger.record(e, location: self, message: "Impossible to update twilio urls for the subdomain change #{previous_changes['subdomain']}", fingerprint: '6baeacd01dfd3ff61a8aca2a2b6ae8f0bedd8145')
    end

    def has_voice? # rubocop:disable Naming/PredicateName
      settings.voice?
    end

    def reactivating_purchased_account_with_talk?
      is_active_changed? &&
        is_active &&
        zuora_subscription.present? &&
        voice_sub_account.present?
    end

    def ensure_active_zuora_talk_usage_subscription
      ZBC::Zuora::Jobs::AddVoiceJob.work(id, usage_subscription_start_date)
    end

    def usage_subscription_start_date
      return unless subscription.churned_on_was.present?

      churned_date_str = subscription.churned_on_was.iso8601
      ZBC::Zuora::Date::ZUORA_TIME_ZONE.parse(churned_date_str).to_date
    end
  end
end
