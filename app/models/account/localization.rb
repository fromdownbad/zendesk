# Encapsulate all of the language and time zone settings for an account and
# declare the related associations.
#
# SEARCH TERMS: localization time_zone time zone timezone locale translation_locale TimeZone TranslationLocale
#
# FUNCTIONALITY
#   Accounts initially have no schedules and no languages enabled. This provides
#   access to the languages an account can enable, and which languages it does
#   have enabled.
#
#   When an account changes the default langauge, or enabled langauges, all of
#   the associated users need to be revalidated and default content needs to be
#   retranslated.
#
#   Each time `locale_id` (the default language) is changed, the
#   `RestoreDefaultContent` job gets enqueued to retranslate all default content.
#
#   Each time `allowed_translation_locale_ids` changes, the users need to be
#   checked to make sure they aren't using a language that might have been disabled.
#
class Account
  module Localization
    DEFAULT_TIME_ZONE = 'Eastern Time (US & Canada)'.freeze

    def self.included(base)
      base.class_eval do
        extend TimeZoneValidation

        belongs_to              :translation_locale, foreign_key: 'locale_id'
        has_and_belongs_to_many :allowed_translation_locales, join_table: 'accounts_allowed_translation_locales', class_name: 'TranslationLocale'
        has_and_belongs_to_many :allowed_agent_locales, join_table: 'accounts_allowed_agent_locales', class_name: 'TranslationLocale'

        validates_time_zone     :time_zone
        validates_presence_of   :time_zone

        after_commit :clear_locale_of_end_user, if: :locale_attrs_were_changed?
        after_commit :restore_default_content
      end
    end

    # Set the allowed locales to the given ids and also set the `@previous_allowed_translation_locale_ids`
    # variable to the previous value to track changes.
    #
    # @param [Array<Integer>] new_locale_ids the new locale ids
    # @return [Array<Integer>] the locale ids that were set
    def allowed_translation_locale_ids=(new_locale_ids)
      @previous_allowed_translation_locale_ids = allowed_translation_locale_ids
      super
    end

    # Get the accounts translation locale or fall back to `ENGLISH_BY_ZENDESK`
    # if it is not set.
    #
    # @param [none]
    # @return [TranslationLocale] the accounts translation locale
    def translation_locale
      super || ENGLISH_BY_ZENDESK
    end

    # Build a list of `TranslationLocale`s that are available to the account and
    # its end users. This will also include special locales that are given to
    # an account by the I18n team.
    #
    # @param [none]
    # @return [Array<TranslationLocale>] the locales available to the account
    def locales_for_selection
      @locales_for_selection ||= begin
        locales = TranslationLocale.available_for_account(self)
        locales += TranslationLocale.test_locales if settings.allow_test_locales?
        locales += TranslationLocale.keys_locales if settings.allow_keys_locales?
        locales.uniq
      end
    end

    # Build a list of `TranslationLocale`s that the account as enabled sorted by
    # the localized name of the locale.
    #
    # @param [none]
    # @return [Array<TranslationLocale>] the languages the account has enabled
    def available_languages
      languages = [translation_locale]
      # only add extra languages if the subscription permits
      if has_individual_language_selection?
        languages += allowed_translation_locales
      end

      if languages.size > 1
        collator = ICU::Collation::Collator.new(I18n.translation_locale.locale) # sorting by locale
        languages = languages.uniq.sort { |a, b| collator.compare(a.localized_name(display_in: I18n.translation_locale), b.localized_name(display_in: I18n.translation_locale)) }
      end

      languages
    end

    # Set the accounts time zone. If the account has multiple schedules then
    # update the time zone of the default schedule.
    #
    # @param [String] value the new time zone
    # @return [String] the time zone that was set
    def time_zone=(value)
      schedule.try(:update_attributes!, time_zone: value) unless has_multiple_schedules?
      super
    end

    # Get the hour format from the account settings.
    #
    # @param [none]
    # @return [Integer] the hour format, either 12 or 24
    def time_format
      uses_12_hour_clock? ? 12 : 24
    end

    # Get the ruby format string for time based on the accounts settings.
    #
    # @param [TrueClass, FalseClass] include_zone include the time zone formatter
    # @return [String] the ruby format string
    def time_format_string(include_zone = false)
      key  = "time.supplemental.#{time_format}hr"
      key += '.tz' if include_zone

      Zendesk::Cldr::DateTime.new(translation_locale).single(key)
    end

    # Get the ruby format string for weekdays based on the accounts settings.
    #
    # @param [none]
    # @return [String] the ruby format string
    def week_time_format_string
      Zendesk::Cldr::DateTime.new(translation_locale).single("time.supplemental.weekday.#{time_format}hr")
    end

    # Set the accounts timezone by parsing the given timesone or offset. The
    # given value has to be valid for ActiveSupport::TimeZone[]
    #
    # @param[String] utc_off the time zone or offset
    # @return[String] the name of the time zone that was set
    def set_time_zone(utc_off) # rubocop:disable Naming/AccessorMethodName
      self.time_zone = if Arturo.feature_enabled_for_pod?(:ocp_support_shell_account_creation, Zendesk::Configuration.fetch(:pod_id))
        Account::Localization.utc_to_tz(utc_off)
      else
        if utc_off.is_a?(String) && /\A[+-]?\d+\z/.match?(utc_off)
          tz = ActiveSupport::TimeZone[utc_off.to_i]
          tz ? tz.name : DEFAULT_TIME_ZONE
        elsif utc_off.present? && ActiveSupport::TimeZone[utc_off]
          ActiveSupport::TimeZone[utc_off].name
        else
          DEFAULT_TIME_ZONE
        end
      end
    end

    # Get the start of the week in the accounts time zone.
    def start_of_week
      Time.now.in_time_zone(time_zone).monday
    end

    def self.utc_to_tz(utc_off)
      if utc_off.is_a?(String) && /\A[+-]?\d+\z/.match?(utc_off)
        tz = ActiveSupport::TimeZone[utc_off.to_i]
        tz ? tz.name : DEFAULT_TIME_ZONE
      elsif utc_off.present? && ActiveSupport::TimeZone[utc_off]
        ActiveSupport::TimeZone[utc_off].name
      else
        DEFAULT_TIME_ZONE
      end
    end

    private

    def clear_locale_of_end_user
      ResetEndUsersLocaleJob.enqueue(id)
    end

    def restore_default_content
      return if !previous_changes.include?('locale_id') || pre_created_account?

      old_id, _new_id = previous_changes['locale_id']
      RestoreDefaultContentJob.enqueue(id, old_id, translation_locale.id)
    end

    def locale_attrs_were_changed?
      previous_changes.include?('locale_id') ||
        (
          @previous_allowed_translation_locale_ids &&
          (allowed_translation_locale_ids & @previous_allowed_translation_locale_ids).any?
        )
    end
  end
end
