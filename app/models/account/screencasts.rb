module Account::Screencasts
  def self.included(base)
    base.property_set :settings do
      property :screencasts_for_tickets
    end
    base.has_one :screenr_tenant, class_name: 'Screenr::Tenant'
  end

  def screenr_integration
    @screenr_integration ||= Screenr::Integration.new(self)
  end
end
