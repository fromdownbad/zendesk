class Account < ActiveRecord::Base
  module MultiproductBillableAgents
    ## ### MultiproductBillableAgents
    ## This is similar to /apps/models/account/billable_agents.rb
    ##
    ## BillableAgents was created when Zendesk was a Support centric system, but
    ## with core-services and having multiproduct plan type, it's no longer enough
    ## to return seats usage count based only on Support agents.
    ##
    ## Instead of modifying BillableAgents file, which might have unforeseeable impact,
    ## this module is created for a safer transition.
    ##
    def multiproduct_billable_agent_count!
      total_agent_count = count_billable_agents

      report_comparison(total_agent_count)

      total_agent_count
    end

    private

    def staff_service_client
      @staff_service_client ||= Zendesk::StaffClient.new(self)
    end

    def statsd_client
      @statsd_client ||=
        Zendesk::StatsD::Client.new(namespace: 'staff_profile_rollout')
    end

    def count_billable_agents
      if multiproduct || spp? || has_ocp_check_seats_left_in_staff_service?
        staff_service_client.seats_occupied_support_or_suite!
      else
        billable_agent_count
      end
    end

    def report_comparison(total_agent_count)
      if has_ocp_compare_classic_staff_service_agent_count? && total_agent_count.present? && total_agent_count > subscription.max_agents
        delta = total_agent_count - subscription.max_agents
        logger.info("ocp_compare_classic_staff_service_agent_count.mismatch: delta=#{delta} "\
          "subdomain=#{subdomain} max_agents=#{subscription.max_agents} "\
          "total_agents=#{total_agent_count} multiproduct=#{multiproduct} spp=#{spp?}")
        statsd_client.increment('max_agents_exceeded', tags: ["multiproduct:#{multiproduct}", "spp:#{spp?}"])
      end
    end
  end
end
