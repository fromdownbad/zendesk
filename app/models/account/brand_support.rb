class Account < ActiveRecord::Base
  module BrandSupport
    def self.included(base)
      base.class_eval do
        validates_with DefaultBrandValidator, if: proc { |r| r.settings.default_brand_id_record.value_changed? }

        after_update :propagate_name_to_brand, if: :name_changed?

        property_set :settings do
          property :require_brand_on_new_tickets, type: :boolean, default: false
        end

        delegate :signature_template, to: :default_brand
      end
    end

    # sets signature_template at the account level in all existing brands (active or not).
    #
    # Accounts without multibrand use the old form in agent settings to set
    # signature_template for the account. In that case only the template of the
    # default brand will be used (see delegate above) but we want to keep all brands
    # updated and synchronized in case the account is upgraded to a plan with multibrand.
    def signature_template=(template)
      brands.each { |brand| brand.signature_template = template }
    end

    # return true/false based on if an account has used brands
    #
    # returns false if they have ever created another brand (even if it has been deleted)
    # otherwise returns true
    #
    # @param [none]
    # @return [Boolean]
    def new_to_branding?
      return false if Brand.with_deleted { brands.limit(2).count } > 1
      true
    end

    # query for inactive non-deleted brands
    #
    # @param [none]
    # @return [rails4: ActiveRecord::Relation, rails3: Array of inactive non-deleted Brands ]
    def inactive_brands
      brands.where(active: false)
    end

    # query for active brands
    #
    # @param [none]
    # @return [rails4: ActiveRecord::Relation, rails3: Array ] Array of active Brands
    def active_brands
      brands.where(active: true)
    end

    # Array of related brands host_mappings
    #
    # @param [none]
    # @return [Array] Array of related brands host_mappings
    def brand_host_mappings
      brands.map(&:host_mapping).compact
    end

    # Over-rides []= in ActiveRecord
    #   if the key is :default_brand_id then the settings.default_brand_id is updated
    #   otherwise calls super which
    #   Updates the attribute identified by attr_name with the specified value.
    #
    # @note this repo https://github.com/zendesk/property_sets will help you understand how settings work
    #
    # @param [string or symbol] Key
    # @param [Object] Value
    # @return []
    def []=(key, value)
      if key.to_sym == :default_brand_id
        settings.default_brand_id = value
      else
        super
      end
    end

    # returns `true` if there are more than one active non-deleted brands
    #
    # @param [none]
    # @return [Boolean]
    def has_multiple_active_brands? # rubocop:disable Naming/PredicateName
      has_multibrand? && brands.active.size > 1
    end

    # returns `true` if there are more than one non-deleted brands
    #
    # @param [none]
    # @return [Boolean]
    def has_multiple_brands? # rubocop:disable Naming/PredicateName
      has_multibrand? && brands.size > 1
    end

    def can_add_brand?
      under_maximum_brands_limit? && under_active_brands_limit?
    end

    def under_active_brands_limit?
      has_unlimited_multibrand? || brands.active.count(:all) < Brand::MAXIMUM_ACTIVE_BRANDS
    end

    def under_maximum_brands_limit?
      return true unless has_maximum_brands?
      brands.count < settings.maximum_brands
    end

    def propagate_name_to_brand
      brand = brands.active.first
      brand.update_attribute(:name, name) unless brand.nil? || has_multiple_active_brands?
    end

    class DefaultBrandValidator < ActiveModel::Validator
      def validate(account)
        # Remove this conditional after the default brands backfilling process has been completed (MB-69).
        return unless account.has_multibrand?

        # Clear association cache
        default_brand = account.default_brand(true)

        if !default_brand || default_brand.account_id != account.id
          account.errors.add(:base, I18n.t('txt.admin.model.brand.cannot_change_default_brand'))
        elsif !default_brand.active?
          account.errors.add(:base, I18n.t('txt.admin.model.brand.cannot_make_inactive_brand_default'))
        end
      end
    end
  end
end
