class Account
  module RadarSuspensionSupport
    def radar_enabled?
      killed_at_pod_level = Arturo.feature_enabled_for_pod?(:pod_or_cluster_radar_kill_switch, pod_id)
      !killed_at_pod_level && settings.radar_enabled
    end

    def suspend_radar(audit_params)
      on_shard do
        settings.radar_enabled = false
        settings.save!
        create_radar_audit(audit_params)
      end
      ::RadarFactory.create_radar_client(self).status('reconfigure').set('reconfigure', 'kill_switch')
    end

    def unsuspend_radar(audit_params)
      on_shard do
        settings.radar_enabled = true
        settings.save!
        create_radar_audit(audit_params)
      end
    end

    private

    def create_radar_audit(audit_params)
      return unless audit_params.presence
      audit_params.merge!(
        account_id: id,
        source_id: id,
        source_display_name: subdomain,
        action: 'update',
        source_type: 'Account',
        visible: false
      )

      event = CIA::Event.new(audit_params)
      event.save!
    end
  end
end
