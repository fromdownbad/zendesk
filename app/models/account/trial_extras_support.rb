class Account
  module TrialExtrasSupport
    TRIAL_EXTRAS_TTL = 90.days.to_i

    def add_trial_extras(trial_extras_hash = {})
      statsd_client = Zendesk::StatsD::Client.new(namespace: ["trial_extras"])

      if new_record? # only use redis for fast account creation
        trial_extras.build(TrialExtra.build_hash(trial_extras_hash))
        if trial_extras_hash.present? # don't track precreated accounts
          statsd_client.increment('count', tags: ["status:success", "precreated:false", "account_id:#{id}"])
        end
      else
        begin
          merged_trial_extras = redis_trial_extras.merge(trial_extras_hash)
          redis_client.setex(TrialExtra.redis_key(id), TRIAL_EXTRAS_TTL, JSON.generate(merged_trial_extras))
          statsd_client.increment('count', tags: ["status:success", "precreated:true", "account_id:#{id}"])
        rescue StandardError => e
          ZendeskExceptions::Logger.record(e, location: self, message: "Error writing trial extras to Redis", fingerprint: '92d03d2e94db429fc9f37278cd074e0a20765b4e')
          Rails.logger.error("Failed to write trial extras to Redis. Message #{e.message}.\nBacktrace: #{e.backtrace.join("\n")}")
          statsd_client.increment('count', tags: ["status:failure", "precreated:true", "account_id:#{id}"])
          trial_extras.build(TrialExtra.build_hash(trial_extras_hash))
        end
      end
    end

    def zendesk_suite_trial_extras_buy_now?
      has_billing_buy_now_zendesk_suite? && trial_extras_buy_now?('zendesk_suite')
    end

    def trial_extras_buy_now?(product_name)
      trial_extras_product_sign_up?(product_name) &&
      ['1', 'true'].include?(trial_extras_dirty_read['buy_now'])
    end

    def trial_extras_product_sign_up?(product_name)
      trial_extras_dirty_read['product_sign_up'] == product_name
    end

    def trial_extras_plan
      trial_extras_dirty_read['plan']
    end

    # The "alternative_length" trial extra was added as part of the 14-day trial test but has been given an open-ended name
    # in case we need to conduct other trial length experiments afterward. The theory being that it's highly likely we will
    # not conduct simultaneous experiments around trial length so we've kept the implementation as lean and easy to
    # augment as possible
    def trial_extras_alternative_length?
      # when a trial extra is set to with a value of boolean true it's dirty read will be returned as '1'
      has_use_alternative_trial_length? &&
          (Arturo.feature_enabled_for?(:classic_14_day_trial, account) || ['1', 'true'].include?(trial_extras_dirty_read['alternative_length']))
    end

    private

    def redis_trial_extras
      JSON.parse(redis_client.get(TrialExtra.redis_key(id)) || '{}')
    end

    def trial_extras_dirty_read
      redis_trial_extras.presence || trial_extras.to_hash
    end

    def trial_extras_feature?(feature)
      trial_extras_dirty_read['features'].to_s.split(',').include?(feature)
    end
  end
end
