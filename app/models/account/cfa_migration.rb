class Account
  module CfaMigration
    # Production: https://account.zendesk.com/api/v2/apps/19078.json
    # Staging: https://account.zendesk-staging.com/api/v2/apps/17.json
    CFA_APP_ID = Rails.env.staging? ? 17 : 19078

    def cfa_migration_needed?
      !account.settings.native_conditional_fields_migrated? && cfa_installed?
    end

    def cfa_enabled?
      cfa_installation && cfa_installation[:enabled]
    end

    def cfa_installation_id
      cfa_installation && cfa_installation[:id]
    end

    def cfa_installed?
      cfa_installation.present?
    end

    def multiple_enabled_cfa_installations?
      cfa_installations.select { |installation| installation[:enabled] }.size > 1
    end

    # Alias of migrate_cfa_to_ctf(dry_run: true)
    # This is to simplify the syntax and make it less risky when we execute in console.
    def cfa_to_ctf_dry_run
      migrate_cfa_to_ctf(dry_run: true)
    end

    # Execute migration:
    #   account.migrate_cfa_to_ctf
    # Dry-run:
    #   > result = account.migrate_cfa_to_ctf(dry_run: true)
    #   => {:successful=>[], :failed=>[#<TicketFieldCondition id: nil,...]}
    #   # Get an array with error messages of the conditions that fail:
    #   > result[:failed].map { |c| c.errors.full_messages }
    def migrate_cfa_to_ctf(options = {})
      dry_run = options.fetch(:dry_run, false)

      if ticket_field_conditions.any? && !dry_run
        # Delete all previous conditions in the account
        TicketFieldCondition.where(account_id: id).delete_all
        ticket_field_conditions.reload
      end

      agent_rules, end_user_rules = find_cfa_data

      agent_migrations = parse_cfa_into_ctf(:agent, agent_rules, dry_run)
      end_user_migrations = parse_cfa_into_ctf(:end_user, end_user_rules, dry_run)

      {
        successful: [agent_migrations[:successful] + end_user_migrations[:successful]].flatten,
        failed: [agent_migrations[:failed] + end_user_migrations[:failed]].flatten
      }
    end

    # Useful method to let an account participate in the CTF EAP again after they decided to exit.
    # This will permanently delete all built-in conditions.
    # This method can only be executed through console or internal endpoint, do not expose externally.
    def reentry_ctf_eap
      unless settings.native_conditional_fields_abandoned?
        raise StandardError, "The account didn't exit Conditional Ticket Fields Early Access Program"
      end

      Account.transaction do
        # Delete all built-in conditions
        TicketFieldCondition.where(account_id: id).delete_all
        # Update account settings to reentry CTF EAP
        settings = {
          native_conditional_fields_abandoned: false,
          native_conditional_fields_migrated: false
        }
        update_attributes!(settings: settings)
      end
    end

    private

    # Returns preferred CFA installation to use for Conditional Ticket Fields migration.
    # If there are enabled installations, returns the most recently updated one.
    # If there are only disabled installations, returns the most recently updated one.
    # If there are no `non deleted` installations, returns nil.
    #
    # {
    #   id: 123,
    #   enabled: true,
    #   deleted_at: nil,
    #   updated_at: 2018-06-03 18:32:43
    # }
    def cfa_installation
      enabled, disabled = cfa_installations.partition { |i| i[:enabled] }
      enabled.last || disabled.last
    end

    # Get all `non deleted` CFA installations in this account
    # [
    #   {
    #     id: 123,
    #     enabled: true,
    #     deleted_at: nil,
    #     updated_at: 2018-06-03 18:32:43
    #   },
    #   ...
    # ]
    def cfa_installations
      @cfa_installations ||= begin
        # Get all CFA installations in this account
        query = "SELECT i.id, i.enabled, i.deleted_at, i.updated_at FROM apps_installations i WHERE i.app_id = #{CFA_APP_ID.to_i} AND i.account_id = #{id.to_i} ORDER BY updated_at"
        results = ActiveRecord::Base.connection.select_all(query).rows.map do |installation|
          {
            id: installation[0],
            enabled: installation[1] == 1,
            deleted_at: installation[2],
            updated_at: installation[3]
          }
        end
        # Filter the soft deleted ones
        results.select { |installation| installation[:deleted_at].nil? }
      end
    end

    def find_cfa_data
      return ['', ''] unless cfa_enabled?

      cfa_rules_query = "
        SELECT s.key, s.value
        FROM apps_settings s
        WHERE s.installation_id = #{cfa_installation_id}
        AND s.account_id = #{id.to_i}
        AND s.key IN ('rules','rules_1', 'user_rules', 'user_rules_1')"

      # Rules are split into up to 2 rows cut off mid-JSON blob, retrieve and join data
      agent_rules = []
      end_user_rules = []
      ActiveRecord::Base.connection.execute(cfa_rules_query).each do |rule_type, conditions_string|
        next if conditions_string.blank? || conditions_string == "null"
        case rule_type
        when "rules"
          agent_rules[0] = conditions_string
        when "rules_1"
          agent_rules[1] = conditions_string
        when "user_rules"
          end_user_rules[0] = conditions_string
        when "user_rules_1"
          end_user_rules[1] = conditions_string
        end
      end

      [agent_rules.join, end_user_rules.join]
    end

    def parse_cfa_into_ctf(user_type, conditions_string, dry_run)
      successful = []
      failed = []
      json_array = JSON.parse(conditions_string)

      if json_array.present?
        json_array.sort_by { |c| c["creationDate"] || Time.now.to_i }.each do |condition|
          condition["select"].each do |child_field_id|
            tfc = TicketFieldCondition.new

            tfc.account = self
            tfc.user_type = user_type
            tfc.parent_field_id = condition["field"]
            tfc.child_field_id = child_field_id
            tfc.ticket_form_id = condition["formId"]
            tfc.value = condition["value"]
            tfc.is_required = condition["requireds"] ? condition["requireds"].include?(child_field_id) : false
            tfc.required_on_statuses = StatusType.SOLVED.to_s if tfc.is_required? && tfc.agent?
            tfc.set_migration_in_progress

            if dry_run
              if tfc.valid?
                successful << tfc
              else
                failed << tfc
              end
            else
              if tfc.save
                successful << tfc
              else
                failed << tfc
              end
            end
          end
        end
      end

      {
        successful: successful,
        failed: failed
      }
    end
  end
end
