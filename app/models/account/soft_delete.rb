class Account
  module SoftDelete
    def self.included(base)
      base.class_eval do
        has_soft_deletion # default scope is in core

        def soft_delete_dependencies
          []
        end
      end
    end
  end
end
