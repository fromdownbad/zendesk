class Account < ActiveRecord::Base
  has_many :acme_certificate_job_statuses, -> { order(:created_at) }, dependent: :destroy, inverse_of: :account

  module CertificateSupport
    def active_cert
      certificates.active.last
    end

    def pending_cert
      certificates.pending.last
    end

    def find_existing_certificates
      certificates.where(autoprovisioned: true, state: 'revoked').select { |c| !AcmeCertificate.needs_renewal?(c) && c.crt_covers_all_domains? }
    end

    def existing_acme_certificate
      @existing_acme_certificate ||= find_existing_certificates.sort_by(&:crt_not_after).last
    end
  end
end
