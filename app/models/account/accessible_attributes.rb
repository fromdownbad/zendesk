class Account < ActiveRecord::Base
  # Persisted
  attr_accessible :agents_can_create_users, :captcha_required, :cc_blacklist, :domain_blacklist, :domain_whitelist, :forum_title, :help_desk_size,
    :host_mapping, :is_active, :is_attaching_enabled, :is_collaboration_enabled,
    :is_collaborators_addable_only_by_agents, :is_comment_public_by_default, :is_email_comment_public_by_default, :is_first_comment_private_enabled,
    :is_end_user_password_change_visible, :is_end_user_profile_visible, :is_open, :has_public_forums, :is_end_user_phone_number_validation_enabled,
    :is_personalized_reply_enabled, :have_gravatars_enabled,
    :is_serviceable, :is_signup_required, :is_ssl_enabled,
    :is_welcome_email_when_agent_register_enabled, :last_login, :mail_delimiter, :cc_subject_template, :follower_subject_template,
    :multiproduct, :account_group_id, :name, :reply_address, :source, :time_zone, :uses_12_hour_clock, :locale_id,
    :display_pinned_entries, :display_pinned_entry_content, :is_twitter_login_enabled, :is_google_login_enabled,
    :is_facebook_login_enabled, :markdown_ticket_comments, :rich_text_comments, :spam_prevention, :is_forum_moderation_enabled, :using_wp_spam_filter, :has_requested_multibrand,
    :is_side_conversations_email_enabled, :is_side_conversations_slack_enabled, :ticket_email_ccs_suspension_threshold, :render_custom_uri_hyperlinks

  # Virtual
  attr_accessible :terms_and_conditions, :address_attributes, :branding, :cc_email_template, :follower_email_template, :html_mail_template,
    :text_mail_template, :signup_email_text, :signup_page_text, :verify_email_text, :signature_template,
    :introductory_title, :introductory_text, :maximum_chat_requests, :chat_welcome_message, :trial_coupon_code,
    :customer_satisfaction_privacy, :url_shortener, :url_username, :url_password,
    :organization_activity_email_template, :ticket_forms_instructions

  # Associations and Aggregations
  attr_accessible :account_logo, :account_property_set, :address, :admins, :agents, :all_end_users, :all_groups, :all_users,
    :allowed_translation_locales, :allowed_translation_locale_ids, :attachments, :automations, :certificates,
    :challenge_tokens, :end_users, :entries, :favicon, :settings, :field_assignee, :field_description, :field_group,
    :field_priority, :field_status, :field_subject, :field_taggers, :field_ticket_type, :forums,
    :groups, :header_logo, :inactive_users, :inbound_emails, :incidents, :integrations, :invoice_no_sequence, :jobs,
    :macros, :mobile_logo, :nice_id_sequence, :organizations, :owner, :payments, :problems, :questions,
    :remote_authentication, :reports, :rules, :sandbox, :sla_views,
    :slas, :subscription, :suspended_tickets, :tag_scores, :taggings, :tags, :targets,
    :ticket_fields, :tickets, :translation_locale, :triggers, :user_identities, :users, :verification_tokens, :views,
    :watchings, :salesforce_integration_attributes, :light_agent_permission_set_attributes
end
