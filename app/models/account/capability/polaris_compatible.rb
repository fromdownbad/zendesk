module Account::Capability::PolarisCompatible
  CHAT_PHASE_III = 3
  CHAT_PHASE_IV = 4

  SUPPPORTED_PHASES = [CHAT_PHASE_III, CHAT_PHASE_IV].freeze

  def chat_phase_polaris_compatible?
    client = Zendesk::Accounts::Client.new(account)
    return false unless client.account_from_account_service(false)
    chat_product = client.chat_product
    return false unless chat_product

    SUPPPORTED_PHASES.include? chat_product.plan_settings['phase']
  end

  # Allow accounts that have the following criteria to opt-in to Agent Workspace
  # 1. Has chat product
  # 2. Has Chat Phase 3 or 4
  def polaris_chat_compatible_plan?
    subscription.has_chat? && chat_phase_polaris_compatible?
  end

  def support_polaris_compatible?
    return @support_polaris_compatible if defined? @support_polaris_compatible

    @support_polaris_compatible = begin
      products(use_cache: true).
        find { |product| product.name == Zendesk::Accounts::Client::SUPPORT_PRODUCT.to_sym }.
        try { |product| product.active? && product.plan_settings['agent_workspace'] }
    rescue StandardError
      Rails.application.config.statsd.client.increment('suite_support.pravda.failed')
      false
    end
  end

  def polaris_compatible_plan?
    # regardless of anything else, account must have groups support
    unless support_plan_polaris_eligible?
      # TODO: remove when root cause is found
      if subscription.present? && subscription.plan_type > SubscriptionPlanType.Small
        Rails.logger.info "Agent workspace was denied by features for account #{id}"
        # we know that groups is present for team onwards, so invalidate features cache
        subscription.invalidate_features_cache
      end
      return false
    end
    # more plans will be support-only, so check that plan setting first
    return true if account.support_polaris_compatible? || account.has_active_suite?

    if Arturo.feature_enabled_for?(:aw_broad_availability_multiproduct, account)
      return polaris_chat_compatible_plan?
    end

    false
  end
end
