class Account::Capability
  module Accessors
    # The feature macro declares a new capability with accessor methods.
    # By default, this provides the standard accessor that checks Arturo for availability:
    # feature :advanced_security_policy
    # ...
    # account.has_advanced_security_policy?
    # => true
    #
    ## Availabilities
    # In addition to arturo, subscription and custom code blocks can be checked:
    # feature :advanced_security_policy, :subscription => true, :available_if => lambda { |account| account.is_awesome? }
    #
    ## Configurations
    # If a setting is specified, an enabled query method is added.
    # It will return true if the feature is available and enabled:
    #
    # feature :advanced_security_policy, :setting => true
    # ...
    # account.has_advanced_security_policy_enabled?
    # => true
    #
    # Finally, names may be overriden if they differ from the feature:
    # feature :advanced_security_policy, :arturo => :security_policy
    #
    def self.included(base)
      base.extend(ClassMethods)
      base.class_attribute :capabilities, instance_writer: false
      base.capabilities = {}
    end

    module ClassMethods
      def self.extended(base)
        base.send(:include, base.send(:capability_accessors))
      end

      def feature(name, options = {})
        capability = capabilities[name] = Account::Capability.new(name, options)
        define_available_query(name)
        define_enabled_query(name) if capability.configurations.any?
      end

      def production?
        Rails.env.production?
      end

      protected

      def capability_accessors
        @capability_accessors ||= Module.new
      end

      def define_available_query(name)
        capability_accessors.class_eval(<<-RUBY, __FILE__, __LINE__ + 1)
          def has_#{name}?
            available?(:#{name})
          end
        RUBY
      end

      def define_enabled_query(name)
        capability_accessors.class_eval(<<-RUBY, __FILE__, __LINE__ + 1)
          def has_#{name}_enabled?
            available?(:#{name}) && enabled?(:#{name})
          end
        RUBY
      end
    end

    protected

    def enabled?(feature)
      memoize(:enabled?, feature) do
        capability = capabilities[feature]
        capability.enabled?(self)
      end
    end

    def available?(feature)
      memoize(:available?, feature) do
        capability = capabilities[feature]
        capability.available?(self)
      end
    end

    def memoize(name, arg)
      @memoize ||= {}
      namespace = (@memoize[name] ||= {})
      if namespace.key?(arg)
        namespace[arg]
      else
        namespace[arg] = yield
      end
    end
  end
end
