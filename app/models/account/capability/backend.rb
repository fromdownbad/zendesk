module Account::Capability::Backend
  class Subscription
    attr_reader :name

    def initialize(name)
      @name = name
    end

    def available?(account)
      return false unless account.subscription.present?
      account.subscription.send("has_#{@name}?")
    end
  end

  class Custom
    attr_reader :block

    def initialize(block)
      @block = block
    end

    def available?(account)
      @block.call(account)
    end
  end

  class Arturo
    attr_reader :name

    def initialize(name)
      @name = name
    end

    def available?(account)
      ::Arturo.feature_enabled_for?(@name, account)
    end
  end

  class Setting
    attr_reader :name

    def initialize(name)
      @name = name
    end

    def enabled?(account)
      account.settings.send("#{@name}?")
    end
  end
end
