class Account
  module LifecycleSurveysSupport
    def self.included(base)
      base.class_eval do
        has_many :lifecycle_survey_answers, dependent: :destroy, inverse_of: :account
      end
    end
  end
end
