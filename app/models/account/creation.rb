class Account < ActiveRecord::Base
  after_create :store_id_on_shard
  after_rollback :cleanup_id_on_shard, on: :create

  attr_accessor :trial_coupon_code
  has_many :trial_extras

  DEFAULT_REGION          = 'us'.freeze
  DEFAULT_STAGING_PODS    = [998, 999].freeze
  DEFAULT_PRODUCTION_PODS = [13, 14].freeze

  class << self
    # rubocop:disable Lint/UnusedMethodArgument
    def select_pod(region, type: nil, subdomain: nil)
      report_region_to_datadog(region)

      # default the region to us if web app cannot determine customer country
      # based on IP
      region = region.to_s.presence || DEFAULT_REGION

      current_pod = Zendesk::Configuration.fetch(:pod_id)

      return current_pod if select_current_pod?
      aws_pods(region).sample
    end
    # rubocop:enable Lint/UnusedMethodArgument

    private

    def select_current_pod?
      Rails.env.test? || Rails.env.development?
    end

    def aws_pods(region)
      db_pods = AccountCreationShard.where(region: region).pluck(:pod_id).uniq
      return db_pods unless db_pods.empty?

      ZendeskExceptions::Logger.record(
        Exception.new,
        location: self,
        message: 'account creation pods not found, failback to default',
        fingerprint: '07ca5d135c306566bc2aeae51ad8e69c241350d3'
      )

      Rails.env.production? ? DEFAULT_PRODUCTION_PODS : DEFAULT_STAGING_PODS
    end

    def report_region_to_datadog(region)
      status = region.present? ? 'valid' : 'blank'
      statsd_tags = ["region:#{region}"]

      statsd_client.increment(
        "region_#{status}",
        tags: statsd_tags
      )
    end

    def statsd_client
      @statsd_client ||= ::Zendesk::StatsD::Client.new(namespace: %w[account_creation region])
    end
  end

  def choose_shard!(region, shard_id_override = nil)
    report_shard_region_to_datadog(region)

    shards = ActiveRecord::Base.shard_names.map(&:to_i)
    pod_id = Zendesk::Configuration.fetch(:pod_id)
    desired_region = region.to_s.presence || DEFAULT_REGION

    self.shard_id = if shard_id_override && shards.include?(shard_id_override.to_i)
      shard_id_override
    else
      db_shards = AccountCreationShard.where(pod_id: pod_id, region: desired_region).reload.pluck(:shard_id).uniq

      if db_shards.empty?
        ZendeskExceptions::Logger.record(
          Exception.new,
          location: self,
          message: 'account creation shards not found, failback to default',
          fingerprint: '6a9f7f770373537bbf2220d52435e5f005efc2fa'
        )
        shards.sample
      else
        db_shards.sample
      end
    end
  end

  def report_shard_region_to_datadog(region)
    status = region.present? ? 'present' : 'blank'
    statsd_tags = ["region:#{region}"]
    statsd_client.increment(
      "shard_region_#{status}",
      tags: statsd_tags
    )
  end

  def statsd_client
    @statsd_client ||= ::Zendesk::StatsD::Client.new(namespace: %w[account_creation region])
  end

  def set_address(params) # rubocop:disable Naming/AccessorMethodName
    country_code = params.delete(:country_code)

    @addrs_params = params.merge(name: name)
    @addrs_params.merge!(country: country_code) if country_code.present?
  end

  def add_default_permission_sets
    return if permission_sets.custom.any?
    default_permission_sets = ::PermissionSet.build_default_permission_sets(self)

    default_permission_sets.each do |ps|
      Zendesk::SupportUsers::PermissionSet.new(ps).save!(context: 'updated_plan')
    end
  end

  def created_via_appsumo?
    source == Zendesk::Accounts::Source::APPSUMO
  end

  def created_via_google_app_market?
    source == Zendesk::Accounts::Source::GOOGLE_APP_MARKET
  end

  protected

  def store_id_on_shard
    ActiveRecord::Base.on_shard(SHARDED_ENVIRONMENT ? shard_id : nil) do
      if multiproduct?
        # multiproduct accounts are created by Pravda, are already on a shard, and may already be in table
        ActiveRecord::Base.connection.insert("INSERT IGNORE INTO account_ids (id) VALUES (#{id})")
      else
        ActiveRecord::Base.connection.insert("INSERT INTO account_ids (id) VALUES (#{id})")
      end
      @id_added_to_shard = id
    end
  end

  def cleanup_id_on_shard
    if @id_added_to_shard.present?
      Account.delete_bogus_account_ids_entry(shard_id, @id_added_to_shard)
      @id_added_to_shard = nil
    end
  end
end
