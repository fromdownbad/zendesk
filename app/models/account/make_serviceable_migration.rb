class MakeServiceableMigration
  def self.run(dry_run = true)
    logger = Logger.new("#{Rails.root}/log/serviceable_accounts.txt")

    Account.where(is_serviceable: false).where('id > 0').find_each do |acc|
      acc.on_shard do
        if acc.is_serviceable_by_billing_state?
          logger.info("#{dry_run ? "DRYRUN" : ""} Updating the is_serviceable for account #{acc.id}")
          acc.update_attribute(:is_serviceable, true) unless dry_run
        end
      end
    end
  end
end
