class Account
  module SecurityFeatureSupport
    # ip_restrictions available for Enterprise on Current Support Plans
    SUPPORT_IP_RESTRICTIONS_PLAN_TYPES = Set[
      ZBC::Zendesk::PlanType::Enterprise.plan_type
    ].freeze

    # ip_restrictions available for Enterprise on Legacy Support Plans
    LEGACY_SUPPORT_IP_RESTRICTIONS_PLAN_TYPES = Set[
      ZBC::Zendesk::PlanType::Enterprise.plan_type,
      ZBC::Zendesk::PlanType::Plus.plan_type
    ].freeze

    # ip_restrictions available for Enterprise/Trial on Chat Plans
    CHAT_IP_RESTRICTIONS_PLAN_TYPES = Set[
      ZBC::Zopim::PlanType::Trial.plan_type,
      ZBC::Zopim::PlanType::Enterprise.plan_type
    ].freeze

    # this is the set of support specific features that must be deactivated on support trial expiration
    SUPPORT_SPECIFIC_FEATURES = Set[
      :saml, :jwt, :custom_session_timeout, :custom_security_policy
    ].freeze

    def has_security_feature?(feature) # rubocop:disable Naming/PredicateName
      account_has_feature?(feature)
    end

    def downgrade_support_trial_specific_support_features!
      return if account.has_security_settings_for_all_accounts?
      return unless subscription = account.subscription

      SUPPORT_SPECIFIC_FEATURES.each do |feature|
        downgrade_feature!(feature)
        toggle_subscription_feature_bit_to_false!(feature)
      end

      subscription.invalidate_features_cache
    end

    def chat_product
      @chat_product ||= products.find { |product| product.name == :chat }
    end

    def support_expired?
      pravda_support_product.try(:state) == :expired && !pravda_support_product.try(:active?)
    end

    private

    def downgrade_feature!(feature)
      name  = feature.to_s.camelize
      klass = "Zendesk::Features::Change::#{name}Change".safe_constantize

      klass.new(account.subscription).downgrade
    end

    def toggle_subscription_feature_bit_to_false!(feature)
      subscription = account.subscription

      subscription.features.send("#{feature}=", false)
      subscription.features.detect { |f| f.name == feature.to_s }.save!
    end

    def account_has_feature?(feature)
      case feature
      when :ip_restrictions
        account_has_ip_restrictions?
      when :saml
        account_has_saml?
      when :jwt
        account_has_jwt?
      when :custom_security_policy
        account_has_custom_security_policy?
      when :custom_session_timeout
        account_has_custom_session_timeout?
      else
        Rails.logger.info "feature: #{feature} not implemented for account - ID: #{account.id}, SUBDOMAIN: #{account.subdomain}"
        false
      end
    end

    def account_has_custom_session_timeout?
      return true if account.has_security_settings_for_all_accounts?
      return false unless account.subscription.present?
      account.subscription.send("has_custom_session_timeout?")
    end

    def account_has_custom_security_policy?
      return true if account.has_security_settings_for_all_accounts?
      return false unless account.subscription.present?
      account.subscription.send("has_custom_security_policy?")
    end

    def account_has_jwt?
      return true if account.has_security_settings_for_all_accounts?
      return false unless account.subscription.present?
      account.subscription.send("has_jwt?")
    end

    def account_has_saml?
      return true if account.has_security_settings_for_all_accounts?
      return false unless account.subscription.present?
      account.subscription.send("has_saml?")
    end

    # depends on both support and chat
    def account_has_ip_restrictions?
      return true if account.has_security_settings_for_all_accounts?
      support_has_ip_restrictions? || chat_has_ip_restrictions?
    end

    def support_has_ip_restrictions?
      if subscribed_to_legacy_pricing_plan?
        LEGACY_SUPPORT_IP_RESTRICTIONS_PLAN_TYPES.include? effective_support_plan_type
      else
        SUPPORT_IP_RESTRICTIONS_PLAN_TYPES.include? effective_support_plan_type
      end
    end

    def chat_has_ip_restrictions?
      CHAT_IP_RESTRICTIONS_PLAN_TYPES.include? effective_chat_plan_type
    end

    def subscribed_to_legacy_pricing_plan?
      current_pricing_model_revision < ZBC::Zendesk::PricingModelRevision::PATAGONIA
    end

    def current_pricing_model_revision
      # Use latest PMR if subscription isn't created yet or on a Chat-first account.
      return ZBC::Zendesk::PricingModelRevision::PATAGONIA if subscription.blank?

      subscription.pricing_model_revision
    end

    def effective_support_plan_type
      @effective_support_plan_type ||= begin
        if pravda_support_plan_type.present?
          [active_pravda_support_plan_type, active_boosted_support_plan_type].max
        end
      end
    end

    def effective_chat_plan_type
      @effective_chat_plan_type ||= begin
        if pravda_chat_plan_type.present?
          [active_pravda_chat_plan_type, active_boosted_chat_plan_type].max
        end
      end
    end

    def pravda_chat_product
      @pravda_chat_product ||= account.chat_product
    end

    def pravda_support_product
      @pravda_support_product ||= account.products.find { |p| p.name == :support }
    end

    def pravda_chat_plan_type
      @pravda_chat_plan_type ||= pravda_chat_product.try(:plan_settings).try(:fetch, 'plan_type')
    end

    def active_pravda_chat_plan_type
      pravda_chat_product.active? ? pravda_chat_plan_type : -1
    end

    def boosted_chat_plan_type
      @boosted_chat_plan_type ||=
        pravda_chat_product.try(:plan_settings).try(:[], 'boosted_plan_type')
    end

    # we're returning -1 here if no boost exists, to ensure it will never error out in the .max call above
    # this does not currently exist in Pravda, but eventually will.
    def active_boosted_chat_plan_type
      active_chat_boost_in_pravda? ? boosted_chat_plan_type : -1
    end

    def chat_boost_expiry
      @chat_boost_expiry ||= pravda_chat_product.try(:plan_settings).try(:[], 'boost_expires_at')
    end

    def active_chat_boost_in_pravda?
      boosted_chat_plan_type.present? && chat_boost_expiry.present? && chat_boost_expiry > Time.now
    end

    def pravda_support_plan_type
      @pravda_support_plan_type ||= pravda_support_product.try(:plan_settings).try(:fetch, 'plan_type')
    end

    def active_pravda_support_plan_type
      pravda_support_product.active? ? pravda_support_plan_type : -1
    end

    def support_boost
      @support_boost ||= account.feature_boost
    end

    def boosted_support_plan_type
      @boosted_support_plan_type ||= support_boost.boost_level
    end

    def active_support_boost?
      support_boost && support_boost.is_active && support_boost.valid_until > Time.now
    end

    # we're returning -1 here if no boost exists, to ensure it will never error out in the .max call above
    def active_boosted_support_plan_type
      active_support_boost? ? boosted_support_plan_type : -1
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['account', 'security_feature_support'])
    end
  end
end
