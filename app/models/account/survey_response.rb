class Account::SurveyResponse < ActiveRecord::Base
  INDUSTRIES = ActiveSupport::OrderedHash.new.tap do |h|
    h['Education'] = 'education'
    h['Energy'] = 'energy'
    h['Entertainment & Gaming'] = 'entertainment'
    h['Financial & Insurance Services'] = 'financial'
    h['Government & Non-profit'] = 'nonprofit'
    h['Healthcare'] = 'healthcare'
    h['IT Services & Consultancy'] = 'consultancy'
    h['Manufacturing & Computer Hardware'] = 'manufacturing'
    h['Media & Telecommunications'] = 'media'
    h['Marketing & Advertising'] = 'marketing'
    h['Professional & Business Support Services'] = 'support'
    h['Real Estate'] = 'real_estate'
    h['Retail'] = 'retail'
    h['Social Media'] = 'social_media'
    h['Software'] = 'software'
    h['Travel, Hospitality & Tourism'] = 'travel'
    h['Web Applications'] = 'web_apps'
    h['Web Hosting'] = 'hosting'
    h['Other'] = 'other'
  end.freeze

  EMPLOYEE_COUNTS = ActiveSupport::OrderedHash.new.tap do |h|
    h['1-9'] = '1-9'
    h['10-99'] = '10-99'
    h['100-499'] = '100-499'
    h['500-4999'] = '500-4999'
    h['5000+'] = '5000+'
  end.freeze

  TARGET_AUDIENCES = ActiveSupport::OrderedHash.new.tap do |h|
    h['Internal customers'] = 'internal'
    h['External customers - Businesses'] = 'businesses'
    h['External customers - Consumers'] = 'customers'
  end.freeze

  CUSTOMER_COUNTS = ActiveSupport::OrderedHash.new.tap do |h|
    h["Hundreds"] = 'hundreds'
    h["Thousands"] = 'thousands'
    h["Tens of Thousands"] = 'tens_of_thousands'
    h["Hundreds of Thousands"] = 'hundreds_of_thousands'
    h["Millions"] = 'millions'
    h["I'd rather not say"] = 'rather_not_say'
  end.freeze

  SUPPORT_STRUCTURES = ActiveSupport::OrderedHash.new.tap do |h|
    h['Support is a part-time role'] = 'part_time'
    h['One or more employees do support'] = 'employees'
    h['A dedicated support team of agents'] = 'dedicated_team'
    h['A support organization with multiple teams'] = 'support_organization'
  end.freeze

  belongs_to :account

  attr_accessible :account, :industry, :employee_count, :job_role, :target_audience, :full_time_agents,
    :is_primary_support, :created_at, :updated_at, :primary_support, :customer_count, :support_structure,
    :agent_count, :team_count

  validates_presence_of :account_id
end
