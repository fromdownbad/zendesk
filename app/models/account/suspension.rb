require 'zendesk_billing_core/zuora/logging'

class Account
  module Suspension
    def self.included(base)
      base.class_eval do
        has_one :invoice_no_sequence,  dependent: :destroy
        has_one :feature_boost,        dependent: :destroy
        has_one :subscription,         dependent: :destroy, inverse_of: :account, foreign_key: :account_id # account.subscription returns zopim_subscriptin without foreign_key option here in tests for some weird reason

        has_many :payments, through: :subscription
        has_many :coupon_applications, dependent: :destroy

        before_validation :update_account_suspension_state
        before_validation :suspend_if_inactive

        after_save :disable_features!, if: :disable_features?

        scope :active,      -> { base.where(is_active: true) }
        scope :serviceable, -> { base.where(is_serviceable: true) }
        scope :non_paying_accounts, -> {
          base.joins("INNER JOIN subscriptions on subscriptions.account_id = accounts.id", "LEFT JOIN credit_cards on credit_cards.subscription_id = subscriptions.id").
            where("accounts.is_serviceable = true AND accounts.is_active = true AND subscriptions.payment_method_type =  #{PaymentMethodType.CREDIT_CARD} AND
                  credit_cards.id IS NULL AND accounts.sandbox_master_id is NULL and subscriptions.manual_discount < 100 AND subscriptions.is_trial = false")
        }
      end
    end

    def suspend_if_inactive
      self.is_serviceable = false unless is_active
      true
    end

    def update_account_suspension_state
      return true if skip_update_account_suspension_state?

      is_serviceable_by_billing_state?.tap do |recalculated_state|
        Rails.logger.warn("Setting account #{id} is_serviceable to '#{recalculated_state}'")
        self.is_serviceable = recalculated_state
      end

      true
    end

    def is_serviceable_by_billing_state? # rubocop:disable Naming/PredicateName
      return true if is_sandbox?
      return false unless is_active? # Cancelled accounts are never servicable

      calculated_value = zuora_managed? ||                                   # Most accounts are managed by zuora
        subscription.is_trial? ||                                            # Trials are serviceable unless cancelled.
        subscription.is_sponsored? ||                                        # Sponsored accounts are always serviceable.
        subscription.invoicing? ||                                           # Invoicing accounts are always serviceable.
        (subscription.crediting? && subscription.credit_card.present?) ||    # Accounts w/credit cards are always serviceable, as dunning short-circuit
        subscription.is_spoke?                                               # Spoke accounts are serviceable

      Rails.logger.info("CALCULATED_VALUE of is_serviceable_by_billing_state? for account #{id} is false") unless calculated_value
      calculated_value
    end

    include ZendeskBillingCore::Zuora::Logging

    def cancel!
      destroy_sandbox

      transaction do
        self.is_active      = false
        self.is_serviceable = false
        self.host_mapping   = nil
        Zendesk::SupportAccounts::Product.retrieve(self).save!
        # Free the G Suite domain to be available for other accounts
        route.update_attribute(:gam_domain, nil) if route
      end

      begin
        if subscription.present?
          return subscription.cancel!
        elsif multiproduct?
          # if multiproduct account and no subscription exists, we're done
          return true
        end
      rescue StandardError => e
        log("cancel!", "failed to cancel account #{id}", e.message)
        raise
      end
    end

    def reactivation_attributes
      if (subscription.present? && subscription_is_legacy_billing_or_trial?) ||
          (multiproduct? && billing_id.present?)
        { is_active: true, is_serviceable: true }
      else
        { is_active: true, is_serviceable: false }
      end
    end

    def subscription_is_legacy_billing_or_trial?
      subscription.managed_by_external_billing? ||
        subscription.is_trial? ||
        (subscription.crediting? && subscription.credit_card.present?)
    end

    def reactivate!
      self.attributes = reactivation_attributes
      Zendesk::SupportAccounts::Product.retrieve(self).save!

      if subscription.present?
        subscription.managed_update = true
        subscription.payments.pending.all.each(&:destroy)

        # feature bits
        set_use_feature_framework_persistence(true)
        Zendesk::Features::SubscriptionFeatureService.new(self).execute!
      end
    end

    def boosted?
      subscription.plan_type != SubscriptionPlanType.ExtraLarge && feature_boost.present? && feature_boost.is_active?
    end

    def dunning_level_cache_key
      "account/#{id}/dunning_level"
    end

    def is_failing_counter_cache_key # rubocop:disable Naming/PredicateName
      "account/#{id}/is_failing_counter"
    end

    # NB: Cached in context of the Account to circumvent Subscription lookup.
    def is_failing? # rubocop:disable Naming/PredicateName
      if has_zuora_managed_billing_enabled?
        subscription.present? && !!subscription.zuora_subscription.try(:in_dunning?)
      else
        is_failing_counter >= ZendeskBillingCore::States::Dunning::State::WARNING.failure_count
      end
    end

    def has_zero_paid_payments? # rubocop:disable Naming/PredicateName
      standard_paid_payments.count(:all).zero?
    end

    def show_upsell_for_twitter_accounts?
      MonitoredTwitterHandle.where(account_id: id).count(:all) >= subscription.max_monitored_twitter_handles
    end

    def standard_paid_payments
      payments.standard.paid
    end

    private

    def skip_update_account_suspension_state?
      return true if Rails.env.test?           # Don't suspend accounts from specs
      return true if multiproduct?             # Skip multiproduct accounts
      return true unless is_serviceable?       # Only automatically transition from non suspension -> suspension.
      return true unless subscription.present? # Respect tests that don't fully stub Subscription.
      false
    end

    def is_failing_counter # rubocop:disable Naming/PredicateName
      Rails.cache.fetch(is_failing_counter_cache_key, expires_in: 10.minutes) do
        0
      end
    end

    module FeatureSuspension
      def disable_features?
        is_serviceable_changed? && !is_serviceable?
      end

      def disable_features!
        Rails.logger.info(["disable_features", subdomain, id, subscription.try(:id)].join(" "))

        MonitoredTwitterHandle.where(account_id: id).each(&:deactivate!)

        Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(self)

        revoke_certificates!
      end

      private

      def revoke_certificates!
        certificates.active.each do |cert|
          Rails.logger.info "revoking cert #{cert.id} for account #{subdomain} since account is no longer serviceable"
          cert.revoke!
        end
      end
    end

    include FeatureSuspension
  end
end
