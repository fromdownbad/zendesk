class Account
  module AgentWorkspaceSupport
    def support_plan_polaris_eligible?
      # plan must have groups support, which e.g. essential plan doesn't have
      has_groups?
    end

    def activate_agent_workspace_for_trial
      Rails.logger.info('Activating agent workspace')
      settings.polaris = true
      settings.check_group_name_uniqueness = true
      settings.save!
    end

    def deactivate_agent_workspace
      Rails.logger.info('Deactivating agent workspace')
      settings.polaris = false
      settings.save!
    end

    # called when support product downgrades to or upgrades from a plan that lacks groups
    def agent_workspace_available=(available)
      has_chat = products(use_cache: true).any? { |product| product.name == Zendesk::Accounts::Client::CHAT_PRODUCT.to_sym }

      if available
        # account has become eligible
        # - we can't switch on polaris in this case
        # - if chat hasn't been added while account was ineligible, we can switch group uniqueness back on
        account.settings.check_group_name_uniqueness = true unless has_chat
      else
        # account has become ineligible
        # - polaris must be switched off
        # - chat product may be added while ineligible, so group uniqueness must be switched off in that case
        # - groups can't be created in essential plan, so we don't have to worry about switching it back on later
        account.settings.polaris = false
        account.settings.check_group_name_uniqueness = false unless has_chat
      end
      account.settings.save!
    end
  end
end
