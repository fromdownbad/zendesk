class Account
  module Upserting
    SKIP_EXISTING_USER_KEYS = Set[:email, :user_fields]
    SKIP_EXISTING_USER_FIELD_KEYS = Set[:'system::embeddable_last_seen']

    def update_existing_or_initialize_new_user(current_user, params)
      user = find_user_by_params(params) || users.new
      upserting_statsd_client.increment('upserting')

      if skip_updating_user?(user, params)
        upserting_statsd_client.increment('skip_user')
        return nil
      end

      Zendesk::Users::Initializer.new(self, current_user, params).apply(user)

      user
    end

    def find_user_by_params(params)
      if params[:user][:id]
        user = users.find(params[:user][:id])
      end

      if params[:user][:external_id].present? && user.nil?
        user = users.find_by_external_id(params[:user][:external_id].to_s)
      end

      if params[:user][:email] && user.nil?
        user = find_user_by_email(params[:user][:email])
      end

      user
    end

    def skip_updating_user?(user, params)
      if Arturo.feature_enabled_for?(:user_create_or_update_skip_update_last_seen, self)
        # If the user is NOT new, and we are ONLY setting the system::embeddable_last_seen
        # pseudo-custom field, then skip doing anything and return nil.  See
        # https://zendesk.atlassian.net/browse/SC-92.
        if !user.new_record? &&
           params[:user].keys.map(&:to_sym).to_set == SKIP_EXISTING_USER_KEYS &&
           params[:user][:user_fields].keys.map(&:to_sym).to_set == SKIP_EXISTING_USER_FIELD_KEYS
          upserting_statsd_client.increment('skip_user')
          return true
        end
      end

      false
    end

    def upserting_statsd_client
      @upserting_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['account', 'upserting'])
    end
  end
end
