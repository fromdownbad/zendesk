class Account < ActiveRecord::Base
  module ChatSupport
    def  self.included(base)
      base.delegate :maximum_chat_requests=,
        :chat_welcome_message, :chat_welcome_message=,
        to: :account_property_set
    end

    def maximum_chat_requests
      account_property_set.maximum_chat_requests || 1
    end

    def chat_available?
      subscription.has_chat?
    end

    def chat_integrated?
      ZopimIntegration.exists?(account_id: id)
    end

    def chat_about_ticket_enabled?
      chat_available? && settings.chat_about_my_ticket?
    end

    def has_chat_permission_set_or_active_suite? # rubocop:disable Naming/PredicateName
      has_chat_permission_set? || has_active_suite?
    end

    def has_chat_permission_set? # rubocop:disable Naming/PredicateName
      # checking for chat availability is expensive, only do it once per API call
      return @has_chat_permission_set if defined?(@has_chat_permission_set)
      @has_chat_permission_set = permission_sets.exists?(role_type: ::PermissionSet::Type::CHAT_AGENT) &&
      zopim_subscription.present? &&
      zopim_subscription.is_serviceable?
    end

    def chat_permission_set
      chat = permission_sets.where(role_type: ::PermissionSet::Type::CHAT_AGENT).first
      has_chat_permission_set? ? chat : nil
    end

    def zopim_identity_available?
      @zopim_identity_available ||= account.has_ocp_chat_only_agent_deprecation? &&
        chat_integrated? &&
        zopim_subscription.present? &&
        zopim_subscription.is_serviceable?
    end
  end
end
