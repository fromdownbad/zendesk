class Account < ActiveRecord::Base
  module FeatureFrameworkUsage
    def set_use_feature_framework_persistence(use_feature_framework_persistence) # rubocop:disable Naming/AccessorMethodName
      setting = settings.where(name: 'use_feature_framework_persistence').first_or_initialize

      if setting.value != use_feature_framework_persistence
        setting.value = use_feature_framework_persistence
        setting.save!
      end
    end

    def use_feature_framework_persistence?
      return false if id == Account.system_account_id
      on_shard { settings.use_feature_framework_persistence? }
    end
  end
end
