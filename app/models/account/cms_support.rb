class Account
  module CmsSupport
    def self.included(base)
      base.has_many :cms_texts, class_name: "::Cms::Text", dependent: :destroy
      base.has_many :cms_variants, class_name: "::Cms::Variant", dependent: :destroy
    end
  end
end
