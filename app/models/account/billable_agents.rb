class Account < ActiveRecord::Base
  module BillableAgents
    def billable_agent_count
      billable_agents.count
    end

    def billable_agents
      users.billable_agents
    end

    def unbillable_agents
      light_agents + chat_agents
    end

    def agents_to_be_downgraded(new_max_agent_limit)
      candidates = billable_agents.reject(&:is_account_owner?)

      candidates = candidates.sort_by(&:created_at)
      candidates.last(candidates.count - new_max_agent_limit + 1)
    end
  end
end
