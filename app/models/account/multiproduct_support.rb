class Account
  module MultiproductSupport
    def start_support_trial(user = User.system)
      Rails.logger.info "Enqueuing SupportCreationJob for account #{idsub}"
      job_id = SupportCreationJob.enqueue(account_id: id, user_id: user.id)
      save_job_id(job_id)
    end

    def support_creation_cache_key
      @support_creation_cache_key ||= "support-creation-job-id-for-#{id}"
    end

    private

    def save_job_id(job_id)
      Rails.cache.write(support_creation_cache_key, job_id)
      Rails.logger.info("Saved SupportCreationJob job_id '#{job_id}' for account '#{idsub}'")
    end
  end
end
