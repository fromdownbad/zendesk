require 'active_support/concern'

module Account::ZopimSupport
  extend ActiveSupport::Concern

  included do
    has_one :zopim_integration
    has_one :zopim_subscription,
      dependent:   :destroy,
      foreign_key: :account_id,
      class_name:  '::ZendeskBillingCore::Zopim::Subscription',
      inverse_of: :account
    has_many :zopim_agents,
      foreign_key: :account_id,
      class_name:  '::Zopim::Agent',
      inverse_of: :account

    before_update :propagate_subdomain_to_zopim,
      if: -> (acct) { account_changed_and_not_phase_3?(acct) }

    delegate :zopim_account_id,
      to: :zopim_subscription,
      allow_nil: true

    before_soft_delete :destroy_zopim
  end

  private

  def account_changed_and_not_phase_3?(acct)
    acct.subdomain_changed? &&
      acct.zopim_account_id.present? &&
      !zopim_subscription.is_cancelled? &&
      !zopim_subscription.phase_three?
  end

  def destroy_zopim
    zopim_subscription.try(:destroy)
  end

  def propagate_subdomain_to_zopim
    Zopim::Reseller.client.update_account!(
      id:   zopim_account_id,
      data: { zendesk_subdomain: subdomain }
    )
  end
end
