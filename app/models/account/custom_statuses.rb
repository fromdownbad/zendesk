class Account
  module CustomStatuses
    def self.included(base)
      base.class_eval do
        has_many :custom_statuses, -> { order(:position) }, dependent: :destroy
      end
    end

    def create_default_custom_statuses
      CustomStatus::CATEGORIES.each do |status_id|
        next if custom_statuses.any? { |cs| cs.system_status_id == status_id }

        category = StatusType[status_id].name.to_s.downcase
        custom_statuses.new.tap do |cs|
          cs.system_status_id = status_id
          cs.agent_label = "{{zd.status_#{category}}}"
          cs.end_user_label = "{{zd.status_#{category}}}"
          cs.description = "{{zd.status_#{category}_description}}"
          cs.default = true
        end.save!
      end
    end
  end
end
