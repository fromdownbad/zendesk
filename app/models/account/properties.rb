require 'zendesk/export/ticket_export_options'
require 'property_sets/delegator'

Account.class_eval do
  include PropertySets::Delegator
  include Zendesk::OutgoingMail::Support::Account

  # IMPORTANT: Be aware that `default` value here will be applied to all accounts
  # that don't have that setting stored in the database. Check Accounts::Base if
  # you want to modify only default values for newly created accounts.

  # New properties (or settings) must have a translation under
  # `txt.admin.views.reports.tabs.audits.property.object.{{setting.name}}`
  # Look Api::V2::Tickets::EventViaPresenter#setting_name for more references.

  # Translations show up in Account Audits tab (for customers) and/or Monitor Account audits.
  # If audit only shows up in Monitor, please mark the translation as `hidden` (only English string needed).
  # If in doubt just add the translation. All properties are auditable (see AccountSetting auditable_properties call
  # with `audit_all: true` below), but only properties that create CIA change events will be translated (usually most of
  # them will as we wrap our controller calls in CIA.audit). Exceptions can be allowed in EventPresenter tests
  # (test/presenters/cia/event_presenter_test.rb `account_settings_translation_allowlist` var).

  property_set :settings do
    property :multiple_organizations, type: :boolean, default: false
    property :custom_statuses_enabled, type: :boolean, default: false
    property :markdown_ticket_comments, type: :boolean, default: false
    property :rich_text_comments, type: :boolean, default: true
    property :emoji_autocompletion, type: :boolean, default: true
    property :agent_forwardable_emails, default: '0'
    property :customer_context_as_default, default: '0'
    property :archive, default: '0'
    property :archive_after_days, type: :integer, default: 120
    property :close_after_days, type: :integer, default: 28
    property :max_ticket_batch_close_count, type: :integer, default: 1000
    property :extended_recoverable_ticket_age, type: :integer, default: 60
    property :upload_token_expiry, type: :integer, default: 3.days.to_i
    property :write_to_archive_v2_enabled, type: :string, default: 'true'
    property :syncattachments_enabled, type: :string, default: 'true'
    property :comments_reverse_order, default: '0'
    property :customer_satisfaction, default: '0'
    property :captcha_required, default: '0'
    property :public_customer_satisfaction, default: '0'
    property :events_reverse_order, default: '1'
    property :forums2, default: '0'
    property :forums_on_home_page, default: '0'
    property :forum_social_sharing_enabled, default: '0'
    property :forums_can_post_full_html, default: 'nobody'
    property :no_short_url_tweet_append, default: '0'
    property :personal_rules, default: '0'
    property :private_attachments, default: '0'
    property :email_attachments, default: '0'
    property :home_page_search, default: '0'
    property :ticket_auto_tagging, default: '0'
    property :ticket_tags_via_widget, default: '0'
    property :ticket_delete_for_agents, default: '0'
    property :view_deleted_tickets_for_agents, default: '1'
    property :ticket_show_empty_views, default: '0'
    property :ticket_tagging, default: '0'
    property :has_user_tags, default: '1'
    property :tickets_inherit_requester_tags, default: '1'
    property :twitter_search_stream, default: '0'
    property :show_introductory_text, default: '0'
    property :hide_dashboard, default: '0'
    property :show_csat_on_dashboard, default: true, type: :boolean
    property :show_lotus_to_agents, default: '0', type: :boolean
    property :salesforce_integration, default: '0', type: :boolean
    property :use_salesforce_production, default: '1'
    property :use_salesforce_configuration, default: '0'
    property :chat, default: '0'
    property :chat_about_my_ticket, default: '0'
    property :only_extended_metrics, default: '0'
    property :voice, default: '0'
    property :voice_sms, default: '0'
    property :voice_call_about_my_ticket, default: '0'
    property :voice_only_during_business_hours, default: '0'
    property :voice_maximum_queue_size, default: '5'
    property :voice_maximum_queue_wait_time, default: '1'
    property :voice_recordings_public, default: '1'
    property :voice_delay_before_pickup, default: '0'
    property :voice_agent_wrap_up_after_calls, default: '1'
    property :voice_agent_confirmation_when_forwarding, default: '1'
    property :voice_make_agents_unavailable_after_missed_calls, default: '0'
    property :voice_make_calls_outside_zendesk_countries, default: '0', type: :boolean
    property :voice_outbound_enabled, default: '1'
    property :voice_trial_enabled, default: '0'
    property :voice_failover_activated, type: :boolean, default: false
    property :anonymous_request_threshold, type: :integer, default: 5
    property :user_permanent_deletions_threshold, type: :integer, default: 700
    property :ticket_deletions_threshold, type: :integer, default: 400
    property :ticket_index_threshold, type: :integer
    property :ticket_index_deep_threshold, type: :integer
    property :ticket_index_shallow_threshold, type: :integer
    property :ticket_updates_threshold, type: :integer
    property :ticket_fields_limit, type: :integer, default: 400
    property :ticket_forms_limit, type: :integer, default: 300
    property :custom_field_options_limit, type: :integer, default: 1500
    property :tags_index_threshold, type: :integer
    property :tags_index_deep_threshold, type: :integer
    property :tags_index_shallow_threshold, type: :integer
    property :max_tickets_to_scrub_per_hour, type: :integer, default: 100
    property :delete_ticket_metadata_pii, type: :boolean, default: false
    property :benchmark_opt_out, default: false, type: :boolean

    property :workspace_controller_threshold, type: :integer, default: 400
    property :spam_threshold_multiplier, default: ::Account::SpamSensitivity::DEFAULT_SPAM_MULTIPLIER, type: :float
    property :malware_whitelist, default: '', type: :string
    property :prefer_gooddata_v1, default: false, type: :boolean
    property :business_hours, default: false, type: :boolean
    property :satisfaction_prediction, default: false, type: :boolean
    property :csat_hide_description, default: false, type: :boolean
    property :automatic_answers, default: false, type: :boolean
    property :automatic_answers_threshold, default: Zendesk::Types::PredictionThresholdType.Balanced, type: :integer
    property :radar_enabled, default: true, type: :boolean

    property :api_token_access, default: '0'
    property :api_password_access, default: '1'
    property :mobile_app_access, default: true, type: :boolean
    property :is_google_login_enabled, default: '0'
    property :is_facebook_login_enabled, default: '0'
    property :reject_sharing_invites, default: '0'
    property :max_number_attachments, default: '10'
    property :anonymous_attachment_limit, default: '5', type: :integer
    property :last_login, type: :datetime
    property :assumption_expiration, type: :datetime
    property :assumption_duration, default: 'off', type: :string
    property :display_analytics, default: '1'
    property :twitter_ticket_monitoring_limit, default: '100', type: :integer
    property :hide_flag_to_end_user, type: :boolean, default: false
    property :activate_lotus_branding, type: :boolean, default: false
    property :automatic_certificate_provisioning, type: :boolean, default: false
    property :csr_org_unit, type: :string, default: ''
    property :push_notification_device_deregistrations_threshold, type: :integer, default: 100
    property :comments_per_ticket_limit, type: :integer, default: 5000
    property :organizations_index_cache_control_header_ttl_seconds, type: :integer, default: 30

    # ZD:305311 we will set the global limit to 40 and set the lower limit explicitly for accounts that are known
    # to need the lower limit: 'support', 'habbohelpen', 'habbohelpnl', 'habbohelpbr', 'habbohelpes', 'habbohelpit',
    # 'habbohelpfr', 'habbohelpde', 'habbohelpfi', 'habbohelpdk', 'habbohelpse', 'habbohelpno', 'netsparker'
    property :end_user_portal_ticket_rate_limit, default: 40, type: :integer

    property :end_user_entry_rate_limit, default: 10, type: :integer
    property :end_user_comment_rate_limit, default: 50, type: :integer

    # ZD:354945 make the default link truncation link a reasonably high amount
    property :link_display_truncation_length, default: 500, type: :integer

    property :multibrand_includes_help_centers, default: false, type: :boolean

    # properties migrated out of the accounts table
    property :uses_12_hour_clock, default: '0', type: :boolean
    property :agents_can_create_users, default: '1', type: :boolean
    property :signup_required, default: '0', type: :boolean
    property :openid_enabled, default: '0', type: :boolean

    property :open, default: '1', type: :boolean
    property :welcome_email_when_agent_creates_user, default: true, type: :boolean
    property :default_public_comments, default: true, type: :boolean
    property :first_comment_private_enabled, default: false, type: :boolean
    property :ccs_requester_excluded_public_comments, default: false, type: :boolean
    property :third_party_end_user_public_comments, default: false, type: :boolean
    property :have_gravatars_enabled, default: true, type: :boolean
    property :email_comments_public, default: true, type: :boolean
    property :end_user_profile_visible, default: true, type: :boolean
    property :collaboration_enabled, default: false, type: :boolean
    property :collaborators_addable_only_by_agents, default: false, type: :boolean
    property :collaborators_settable_in_help_center, default: false, type: :boolean
    property :collaborators_maximum, default: 20, type: :integer
    property :email_loop_threshold, default: 20, type: :integer
    property :domain_whitelist_blacklist_maximum_length, default: 10000, type: :integer
    property :personalized_replies, type: :boolean, default: true
    property :end_user_attachments, type: :boolean, default: true
    property :verification_captcha_enabled, default: false, type: :boolean
    property :email_sender_authentication, type: :boolean, default: false

    property :assign_tickets_upon_solve, default: false, type: :boolean

    property :display_pinned_entries, type: :boolean, default: true
    property :display_pinned_entry_content, type: :boolean, default: true
    property :twitter_login, type: :boolean, default: true

    property :end_user_password_changes, type: :boolean, default: true
    property :end_user_phone_number_validation, type: :boolean, default: false
    property :topic_suggestion_for_ticket_submission, type: :boolean, default: false
    property :monitored_facebook_page_limit, type: :integer, default: nil
    property :facebook_message_automation_created, type: :boolean, default: false
    property :admins_can_set_user_passwords, default: false, type: :boolean

    property :grandfathered_host_mapping, default: false, type: :boolean
    property :grandfathered_web_portal, default: false, type: :boolean
    property :incremental_forum_search, type: :boolean, default: true
    property :csat_reason_code, type: :boolean, default: false

    property :prefer_lotus, type: :boolean, default: true
    validates_inclusion_of :value,
      in: ['1'],
      if: proc { |p| (p.name.to_sym == :prefer_lotus) && (p.account.has_multiple_brands? && p.changed?) },
      message: 'Cannot change Lotus preference for accounts with multibrand'

    property :disabled_cdn, type: :boolean, default: false
    property :created_from_ip_address, type: :string, default: ''
    property :creation_channel,        type: :string, default: ''

    property :email_template_selection,            type: :boolean, default: true
    property :modern_email_template,               type: :boolean, default: false
    property :email_template_photos,               type: :boolean, default: false
    property :rich_content_in_emails,              type: :boolean, default: false
    property :accept_wildcard_emails,              type: :boolean, default: true
    property :list_help_centers_in_account_emails, type: :boolean, default: true
    property :cc_subject_template,                 type: :string,  default: ''
    property :follower_subject_template,           type: :string,  default: ''
    property :forwarding_verification_token,       type: :string,  default: nil

    property :zuora_managed_billing,            type: :boolean, default: false
    property :suite_subscription,               type: :boolean, default: false
    property :braintree_migration_group,        type: :string,  default: ''
    property :change_assignee_to_general_group, type: :boolean, default: false

    property :saml_name_qualifier, type: :string, default: nil
    property :saml_issuer_account_url, type: :boolean, default: false

    property :use_feature_framework_persistence, type: :boolean, default: false

    # Mobile
    property :mobile_title
    property :mobile_switch_enabled, default: true, type: :boolean

    property :autocomplete_forum_search, type: :boolean, default: true
    property :boost_curated_content, type: :float, default: 0.0
    property :hide_entry_tags_enabled, default: false, type: :boolean

    property :security_headers, default: true, type: :boolean
    property :x_frame_header, default: true, type: :boolean
    property :xss_header, default: true, type: :boolean
    property :csp_header, default: false, type: :boolean
    property :content_type_header, default: false, type: :boolean
    property :password_reset_override, default: false, type: :boolean
    property :email_agent_when_sensitive_fields_changed, default: true, type: :boolean
    property :set_long_hsts_header_on_host_mapping, default: false, type: :boolean
    property :agreed_to_audioeye_tos, default: false, type: :boolean

    property :security_notifications_recipient, type: :string, default: 'admins'
    property :security_notifications_recipient_id, type: :integer

    property :mobile_requests_index_cache_control_ttl, type: :integer, default: 30

    # TagScores Job
    property :tagging_amount_limit, type: :integer, default: 20_000
    property :tagging_created_at_limit, type: :integer, default: 60

    # Export
    property :csv_export_visible, type: :boolean, default: true
    property :csv_export_duration, type: :integer, default: 0
    property :csv_export_bom, type: :boolean, default: false

    property :export_accessible_types,   type: :serialized, default: ImportExportJobPolicy::DEFAULT_JOBS.to_json
    property :export_role_id,            type: :integer,    default: Role::ADMIN.id
    property :export_whitelisted_domain, type: :string,     default: ''
    property :export_ticket_max,         type: :integer,    default: Zendesk::Export::TicketExportOptions::MAXIMUM_COUNT

    # Forum Moderation
    property :moderated_entries, type: :boolean, default: false
    property :moderated_posts, type: :boolean, default: false
    property :moderation_throttle, type: :integer, default: 5

    # Partner/Zendesk-Reseller Settings
    property :partner_name, type: :string
    property :partner_url,  type: :string

    property :user_count, type: :integer

    property :two_factor_enforce, type: :boolean, default: false
    property :sso_bypass, type: :boolean, default: true
    property :sso_bypass_can_be_disabled, type: :boolean, default: false
    property :two_factor_all_agents, type: :boolean, default: nil
    property :credit_card_redaction, type: :boolean, default: false

    # TODO: Spam prevention cloud be removed and turned into a
    # composition of:
    # - moderated_posts
    # - moderated_entries
    # - spam_filter
    property :spam_prevention, type: :boolean, default: false

    # Whether the account is using the Akismet spam filter on not.
    property :spam_filter, type: :boolean, default: false

    property :suppress_channelback, type: :integer, default: 0

    property :twitter_search_autoconversion, type: :boolean, default: false
    # Stores if the Mobile SDK ToS were accepted
    property :allowed_mobile_sdk, type: :boolean, default: false
    property :max_sdk_identities, type: :integer, default: UserSdkIdentity::MAX_PER_USER

    property :notify_agent_ccs, default: true, type: :boolean

    property :gmail_actions, type: :boolean, default: true

    property :has_requested_multibrand, type: :boolean, default: false
    property :maximum_brands, type: :integer, default: 300

    property :native_conditional_fields_migrated, type: :boolean, default: false
    property :native_conditional_fields_migration_failed, type: :boolean, default: false
    property :native_conditional_fields_abandoned, type: :boolean, default: false
    property :maximum_user_conditions_per_form, type: :integer, default: 1500

    property :many_users_limit, type: :integer, default: nil

    property :many_organizations_limit, type: :integer, default: nil

    property :many_tickets_limit, type: :integer, default: nil

    property :import_many_tickets_limit, type: :integer, default: 100
    property :import_many_tickets_bytesize_limit, type: :integer, default: 5.megabytes

    property :job_statuses_limit, type: :integer, default: 100

    property :max_target_failures, type: :integer, default: 20

    property :allow_test_locales, type: :boolean, default: false
    property :allow_keys_locales, type: :boolean, default: false

    # If API ToS were accepted
    property :accepted_api_agreement, type: :boolean, default: false

    property :account_wide_api_rate_limit, type: :integer, default: 100000

    # Applies to regular accounts. See PatagoniaApiRateLimiter for usage with
    # high volume add-on.
    property :api_incremental_exports_rate_limit, type: :integer, default: Prop.configurations.fetch(:incremental_exports).fetch(:threshold)

    property :ticket_update_rate_limit, type: :integer, default: 30

    property :send_gmail_messages_via_gmail, type: :boolean, default: true

    # Remove user names when incremental API requested by GD
    property :insights_user_name_removal, type: :boolean, default: false

    property :conditional_logging_pattern, type: :string, default: nil
    property :default_roles_with_sys_dc, type: :boolean, default: false

    # Trial Spammers
    property :bulk_user_upload, type: :boolean, default: false
    property :user_identity_limit_for_trialers, type: :boolean, default: true
    property :is_abusive, type: :boolean, default: false
    property :is_watchlisted, type: :boolean, default: false
    property :whitelisted_from_fraud_restrictions, type: :boolean, default: false
    property :risk_assessment, type: :serialized, default: Account::FraudSupport::DEFAULT_RISK_ASSESSMENT.to_json

    # Account takeovers
    property :taken_over_email, type: :string, default: nil
    property :prior_owner_email, type: :string, default: nil
    property :prior_owner_password, type: :string, default: nil
    property :prior_owner_verified, type: :boolean, default: false
    property :prior_risk_assessment, type: :serialized, default: Account::FraudSupport::DEFAULT_RISK_ASSESSMENT.to_json

    # Google App Marketplace
    property :google_site_verification_token, type: :string, default: nil
    property :google_apps_domain, type: :string, default: nil
    property :has_google_apps, type: :boolean, default: nil
    property :has_google_apps_admin, type: :boolean, default: false
    property :switched_to_google_apps, type: :boolean, default: false

    property :macro_most_used, type: :boolean, default: true
    property :macro_order,     type: :string,  default: 'alphabetical'
    property :gooddata_dashboard_threads, type: :integer, default: 2

    # onboarding
    property :checklist_onboarding_version, type: :integer, default: Account::OnboardingSupport::SUPPORT_SUITE_TRIAL_REFRESH
    property :onboarding_segments,          type: :string,  default: nil
    property :product_sign_up,              type: :string,  default: nil

    # Cross sell
    property :show_chat_tooltip, type: :boolean, default: true
    property :xsell_source,      type: :string,  default: nil

    property :total_in_flight_jobs_limit, type: :integer, default: 30
    property :account_cache_version, type: :integer, default: 0

    # Mobile SDK blips
    property :mobile_sdk_blips, type: :boolean, default: true
    property :mobile_sdk_required_blips, type: :boolean, default: true
    property :mobile_sdk_behavioural_blips, type: :boolean, default: true
    property :mobile_sdk_pathfinder_blips, type: :boolean, default: true
    property :max_identities, type: :integer, default: UserIdentity::MAX_PER_USER

    # New collaboration mode: followers and CCs
    property :accepted_new_collaboration_tos,       type: :boolean, default: false
    property :follower_and_email_cc_collaborations, type: :boolean, default: false
    property :auto_updated_ccs_followers_rules,     type: :boolean, default: false
    property :ccs_followers_no_rollback,            type: :boolean, default: false

    property :agent_can_change_requester,          type: :boolean, default: true
    property :agent_email_ccs_become_followers,    type: :boolean, default: true
    property :ccs_followers_rules_update_required, type: :boolean, default: true
    property :comment_email_ccs_allowed,           type: :boolean, default: true
    property :light_agent_email_ccs_allowed,       type: :boolean, default: false
    property :ticket_followers_allowed,            type: :boolean, default: true

    # No-Delimiter
    # Private setting. Disabled by default. Available to change in
    # Support > Admin > Channels > Email, under "Mail delimiter" section
    property :no_mail_delimiter,                   type: :boolean, default: false

    # Simplified Email Threading
    property :simplified_email_threading,          type: :boolean, default: false

    # Side Conversations
    property :side_conversations_email,            type: :boolean, default: false
    property :side_conversations_slack,            type: :boolean, default: false
    property :side_conversations_tickets,          type: :boolean, default: false

    # Skill based routing
    property :using_skill_based_routing, type: :boolean, default: false
    property :edit_ticket_skills_permission, type: :integer, default: 0

    property :session_timeout, type: :integer, default: 480
    validates_inclusion_of :value,
      in: %w[5 10 15 20 25 30 480 20160],
      if: proc { |p| (p.name.to_sym == :session_timeout) },
      message: 'Invalid session timeout value'

    # Zendesk Suite trial
    property :suite_trial, type: :boolean, default: false

    # zendesk skill based ticket routing view setting
    # this setting holds an array of views on which routing filter is applied
    property :skill_based_filtered_views, type: :serialized, default: [].to_json

    # Is this account owned by Zendesk? e.g. Used when deciding
    # if the account can add a support address @zendesk.com
    property :owned_by_zendesk, type: :boolean, default: false

    # Account setting that enforces product limit for each business rule
    # triggers, macros, automations, views
    # this setting holds a key:value pair of rule_type and the corresponding limit
    property :business_rules_limit, type: :serialized, default: ::Rule::RULES_TABLE_LIMITS.to_json

    # Account setting that sets a soft limit for the number of tickets that can
    # be processed by an automation per hour.
    property :parallel_automations_limit, type: :integer, default: 50_000

    # Polaris
    property :polaris, type: :boolean, default: false
    property :social_messaging_agent_workspace, type: :boolean, default: false
    property :native_messaging, type: :boolean, default: false
    property :social_messaging, type: :boolean, default: false
    property :fallback_composer, type: :boolean, default: false

    # Whether group names should be unique within an account (backward compatible)
    property :check_group_name_uniqueness, type: :boolean, default: false

    property :ticket_email_ccs_suspension_threshold, type: :integer, default: 48
    validates_inclusion_of :value,
      in: (0..48).to_a.map(&:to_s),
      if: proc { |p| (p.name.to_sym == :ticket_email_ccs_suspension_threshold) },
      message: I18n.t('txt.admin.views.settings.tickets._settings.ccs_suspension_threshold_validation_message')

    # views
    property :views_find_timeout, type: :integer, default: 30

    # deco
    property :deco_client_incremental_timeout, type: :integer, default: 5

    # Sandbox
    property :sandbox_type, type: :string
    property :sandbox_creation_in_progress, type: :boolean

    # ASK focus mode
    property :focus_mode, type: :boolean, default: false

    # Support Search Torch
    property :search_export_api_rate_limit, type: :integer, default: Prop.configurations.fetch(:search_export_api).fetch(:threshold)

    # Trigger Categories
    property :trigger_categories_api, type: :boolean, default: false

    # Re-engagement
    property :reengagement, type: :boolean, default: false
  end

  delegate_to_property_set(:settings,
    ccs_requester_excluded_public_comments:       :ccs_requester_excluded_public_comments,
    third_party_end_user_public_comments:         :third_party_end_user_public_comments,
    uses_12_hour_clock:                           :uses_12_hour_clock,
    agents_can_create_users:                      :agents_can_create_users,
    display_pinned_entries:                       :display_pinned_entries,
    display_pinned_entry_content:                 :display_pinned_entry_content,
    is_open:                                      :open,
    is_signup_required:                           :signup_required,
    is_welcome_email_when_agent_register_enabled: :welcome_email_when_agent_creates_user,
    is_comment_public_by_default:                 :default_public_comments,
    is_email_comment_public_by_default:           :email_comments_public,
    is_end_user_profile_visible:                  :end_user_profile_visible,
    loop_threshold:                               :email_loop_threshold,
    is_personalized_reply_enabled:                :personalized_replies,
    is_attaching_enabled:                         :end_user_attachments,
    is_twitter_login_enabled:                     :twitter_login,
    is_end_user_password_change_visible:          :end_user_password_changes,
    is_end_user_phone_number_validation_enabled:  :end_user_phone_number_validation,
    is_first_comment_private_enabled:             :first_comment_private_enabled,
    ticket_email_ccs_suspension_threshold:        :ticket_email_ccs_suspension_threshold)

  class ConditionalRateLimitValidator < ActiveModel::Validator
    def validate(setting)
      limits = Array(setting.value)
      return unless limits.count > 1

      enforce_unique_prop_keys(setting, limits)
      enforce_at_least_one_pattern(setting, limits)
    end

    private

    def enforce_unique_prop_keys(setting, limits)
      prop_keys      = limits.map { |l| l['prop_key'] }
      uniq_prop_keys = prop_keys.uniq

      if uniq_prop_keys.count < limits.count
        setting.errors[:base] << "prop_keys must be unique: #{prop_keys.inspect}"
      end
    end

    def enforce_at_least_one_pattern(setting, limits)
      limits.each do |limit|
        if limit['properties'].all? { |_k, v| v.nil? }
          setting.errors[:base] << "the #{limit['prop_key']} limit must contain at least one pattern to match"
        end
      end
    end
  end

  property_set :texts do
    property :help_desk_description

    # Keep this value in sync with app/models/accounts/base.rb and
    # https://github.com/zendesk/zendesk_mail_parsing_queue/blob/master/lib/account.rb
    property :mail_delimiter, default: '{{txt.email.delimiter}}', type: :string
    property :domain_whitelist, default: '', type: :string
    property :domain_blacklist, default: '', type: :string
    property :cc_blacklist, default: '', type: :string
    property :forum_title, default: 'Forums', type: :string
    property :signup_source, default: '', type: :string

    property :salesforce_configuration, default: ''

    property :conditional_rate_limits, type: :serialized
    property :render_custom_uri_hyperlinks, default: '', type: :string

    validates_with ConditionalRateLimitValidator, if: ->(s) { s.name.to_sym == :conditional_rate_limits }
  end

  delegate_to_property_set(:texts,
    mail_delimiter:                :mail_delimiter,
    domain_whitelist:              :domain_whitelist,
    domain_blacklist:              :domain_blacklist,
    cc_blacklist:                  :cc_blacklist,
    forum_title:                   :forum_title,
    source:                        :signup_source,
    conditional_rate_limits:       :conditional_rate_limits,
    render_custom_uri_hyperlinks:  :render_custom_uri_hyperlinks)

  def fetch_settings(*properties)
    properties.each_with_object({}) do |property, hash|
      hash[property] = settings.send(property)
    end
  end
end

AccountSetting.class_eval do
  include CachingObserver
  include AuditableProperty
  include AccountSettingObserver
  include PrecreatedAccountConversion

  has_kasket_on :account_id
  attr_accessible :name, :value

  validate :value_length

  auditable_properties :api_token_access, :api_password_access, :assumption_expiration,
    :assumption_duration, :bcc_archive_address, :allowed_mobile_sdk, :prefer_lotus,
    :use_feature_framework_persistence, :two_factor_enforce, :mobile_app_access,
    :automatic_certificate_provisioning, :ip_restriction_enabled, :enable_ip_mobile_access,
    :enable_agent_ip_restrictions, :delete_ticket_metadata_pii, :polaris, :focus_mode, :captcha_required,
    except: [:last_login],
    audit_all: true

  private

  def value_length
    errors.add(name, I18n.t('txt.admin.controllers.settings.account_controller.account_settings_invalid')) if value.is_a?(String) && value.length > 255
  end
end

AccountText.class_eval do
  include CachingObserver
  include AuditableProperty
  include AccountTextObserver
  include PrecreatedAccountConversion

  has_kasket_on :account_id
  attr_accessible :name, :value

  auditable_properties :ip_restriction, :domain_whitelist, :domain_blacklist, audit_all: true
end
