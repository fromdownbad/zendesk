# Feature options and flags
# =========================
#
# arturo:
# -------
# `arturo:` uses the arturo flag as the backend for this feature.
# `arturo: true` will delegate the check to the arturo flag (which is the default behavior for `feature :foo`)
# `arturo: false` will bypass the arturo flag (and use another backend, like subscription)
# `arturo: !Rails.env.test?` setting it to !Rails.env.test we can make sure that the Arturo is turned-on by
# default for tests. I.E., All our specs run with this Arturo enabled (which is the case when we GA it).
# Hence we don't need to explicitly enable it for all specs.
#
# if `arturo: false` is set and no other backend is specified, the feature will be on for everyone.
# `feature :foo, arturo: Rails.env.production?` will check arturo in production but be true everywhere else.
#
# If you define a feature `abc` with `setting:true` and another Arturo in the require block,
# it will return `true` only when both the Arturo and the setting are enabled. E.g. having:
# `feature :abc, setting:true, arturo:false, require: ->(account) { account.has_xyz? }`
# Then `account.has_abc_enabled?` is true if `account.has_abc_enabled?` setting and
# `account.has_xzy?` arturo are both `true`.
#
# :subscription
# -------------
# `:subscription true` will delegate the check to the subscription, false by default.
#
# :public
# -------
# `:public true` means the flag is exposed via API to Lotus, false by default.
# Make sure you verify the flag name in camelCase too! Like `new_mail_encoder` or `newMailEncoder`
#
# :require
# --------
# `:require ->(account) { <code goes here> }` lets you write arbitrary checks using the account.
#
# **NOTE:** Multiple backends may be used: `feature :foo, arturo: true, :subscription true` will AND the backends.
# :setting is not checked in `has_xxx?`` only in `has_xxx_enabled?``
#
class Account
  include Account::Capability::Accessors
  include Account::Capability::PolarisCompatible

  FIRST_COMMENT_PRIVATE_ROLLOUT_DATE = DateTime.new(2017, 4, 8)
  SUSPEND_TICKET_FROM_UNVERIFIED_ACCOUNT_OWNER_ROLLOUT_DATE = DateTime.new(2019, 7, 8)

  ORGANIZATION_AUTOCOMPLETE_LIMIT = 100

  SUPPORTED_TRANSLATION_CDNS = Set.new(%w[disabled default edgecast cloudfront fastly]).freeze

  # rubocop:disable Layout/ExtraSpacing
  feature :play_mode_skill_match_page_increment, arturo: !Rails.env.test?
  feature :pause_automation_execution
  feature :pause_trigger_execution
  feature :android_app_selection
  feature :image_self_branding
  feature :dropbox_management
  feature :explore_access_permission_set, public: true
  feature :explore_temporary_access, public: true
  feature :feedback_tab_persist, public: true
  feature :gooddata_advanced_analytics, subscription: true, arturo: false, public: true
  feature :bime_integration, public: true
  feature :gooddata_hourly_synchronization, subscription: true, arturo: false, public: true
  feature :explore_show_insights_eol_message, public: true
  feature :explore_show_insights_eol_countdown_message, public: true
  feature :explore_embedding_dashboard_in_lotus, public: true
  feature :ticket_metrics_pending_first_reply
  feature :search_sharding
  feature :status_hold, subscription: true, arturo: false, public: true
  feature :disable_external_services
  feature :salesforce_integration_lookup
  feature :use_salesforce_ssl_pod
  feature :configurable_salesforce_app_latency
  feature :central_admin_salesforce_integration
  feature :salesforce_app_v2_enabled
  feature :salesforce_record_creation
  feature :lotus_moment_js, public: true
  feature :client_side_logging, public: true
  feature :js_exception_logging, public: true
  feature :custom_widget_jquery
  feature :format_conditions_for_view_preview, public: true

  feature :incremental_forum_search, setting: true
  feature :tag_phrases_for_forum_entries, subscription: true, arturo: false
  feature :ticket_forms, subscription: true, public: true, arturo: false
  feature :ticket_form_default
  feature :send_ticket_forms_in_reorder_request, arturo: !Rails.env.test?, public: true
  feature :exclude_archived_default_for_user_tickets

  # Custom Statuses
  feature :custom_statuses, public: true
  feature :custom_statuses_enabled,
    public: true,
    arturo: false,
    require: ->(account) do
      account.has_custom_statuses? &&
      account.settings.custom_statuses_enabled?
    end
  # Temp feature to rollout default CS creation on new accounts
  feature :create_default_custom_statuses_on_new_accounts, arturo: !Rails.env.test?
  # Vortex: enable custom statuses in context for view definition
  feature :custom_statuses_in_views, require: ->(account) { account.has_custom_statuses_enabled? }

  feature :ticket_sharing_whitelist
  feature :csr_token_retention
  feature :multiple_organizations,           setting: true, subscription: true, arturo: false, public: true
  feature :customizable_help_center_themes,  subscription: true, arturo: false
  feature :internal_help_center,             subscription: true, arturo: false
  feature :help_center_article_labels,       subscription: true, arturo: false
  feature :help_center_unlimited_categories, subscription: true, arturo: false
  feature :dc_in_templates,                  subscription: true, arturo: false
  feature :help_center_configurable_spam_filter, subscription: true, arturo: false
  feature :help_center_content_moderation,       subscription: true, arturo: false
  feature :help_center_replicated_s3_bucket
  feature :use_guide_search_in_support
  feature :exclude_articles_from_search_results
  feature :classic_reporting,                arturo: false, public: true
  feature :reporting_leaderboard,            subscription: true, arturo: false, public: true
  feature :satisfaction_prediction,          arturo: true, subscription: true, setting: true, public: true
  feature :macro_preview,                    arturo: true, subscription: true, public: true
  feature :automatic_answers,                arturo: false, setting: true, public: true
  feature :answer_bot_deflect_for_web_widget
  feature :outbound_boosted_messaged_users
  feature :radar_sockets
  feature :radar_polling_timeout
  feature :radar_account_id_in_user_data
  feature :radar_organizations_autofetch_enabled, public: true
  feature :publish_brand_entities
  feature :publish_organization_entity
  feature :publish_group_entities
  feature :publish_group_domain_events

  feature :agent_collision,
    subscription: true,
    arturo: false,
    require: ->(account) { account.agents.count(:all) >= 2 }

  feature :lotus_safe_updates,        public: true
  feature :rule_usage_stats,          subscription: true, arturo: false, public: true
  feature :agent_display_names,       subscription: true, arturo: false
  feature :ticket_sharing_triggers,   subscription: true, arturo: false
  feature :rule_analysis,             subscription: true, arturo: false, public: true
  feature :customer_satisfaction,     subscription: true, arturo: false, public: true, setting: true
  feature :business_hours,            subscription: true, arturo: false, public: true
  feature :multiple_schedules,        subscription: true, arturo: false, public: true
  feature :service_level_agreements,  subscription: true, arturo: false, public: true
  feature :personal_rules,            subscription: true, arturo: false, public: true
  feature :group_rules,               subscription: true, arturo: false, public: true
  feature :extended_ticket_metrics,   subscription: true, arturo: false
  feature :voice,                     setting:      true, arturo: false
  feature :permission_sets,           subscription: true, arturo: false, public: true
  feature :cms,                       subscription: true, arturo: false, public: true
  feature :forum_statistics,          subscription: true, arturo: false, setting: :display_analytics
  feature :search_statistics,         subscription: true, arturo: false, setting: :display_analytics
  feature :search_performance_monitoring, arturo: true
  feature :time_based_indexing,       arturo: true
  feature :custom_date_grouping
  feature :ticket_field_created_by
  feature :authorize_macro_apply,     arturo: !Rails.env.test?

  feature :user_views,
    subscription: true,
    arturo: false,
    public: true,
    require: ->(account) { !account.has_user_views_disabled? }

  feature :user_views_negative_operators_enabled
  feature :user_views_disabled
  feature :user_views_query_tester
  feature :user_views_use_default_language
  feature :occam_count_job_timeout
  feature :nps_surveys,             subscription: true, arturo: false, public: true
  feature :nps_recipients_editor
  feature :nps_csv_export_v2,       public: true
  feature :nps_expand_invite_limit, public: true
  feature :nps_web_widget,          public: true
  feature :nps_reporting_d3,        public: true
  feature :csat_reason_code,        public: true, setting: true
  feature :csat_reason_code_admin
  feature :csat_page_refresh
  feature :csat_description_configure_visibility
  feature :csat_reason_clone
  feature :nps_survey_name,         public: true
  feature :maximum_brands
  feature :multibrand,              subscription: true, public: true, arturo: false
  feature :unlimited_multibrand,    subscription: true, public: true, arturo: false
  feature :heap_integration_customer, public: true
  feature :customer_analytics_integration, public: true
  feature :ios_mobile_deeplinking
  feature :android_mobile_deeplinking
  feature :windows_mobile_deeplinking
  feature :hc_manage_requests, subscription: true, arturo: false
  feature :hc_user_profiles, subscription: true, arturo: false
  feature :sdk_manage_requests, subscription: true, arturo: false, public: true

  feature :groups,         subscription: true, public: true, arturo: false
  feature :organizations,  subscription: true, public: true, arturo: false
  feature :sandbox,        subscription: true, public: true, arturo: false
  feature :light_agents,   subscription: true, public: true, arturo: false
  feature :zopim_chat,     subscription: true, public: true, arturo: false
  feature :targets,          subscription: true, public: true, arturo: false
  feature :crm_integrations, subscription: true, public: true, arturo: false

  feature :rules_can_reference_macros
  feature :disable_external_services
  feature :at_mentions, public: true
  feature :mobile_switch

  feature :allow_netsuite_ccs
  feature :trigger_logging, arturo: Rails.env.production?
  feature :skip_assignee_reload, arturo: Rails.env.production?

  feature :lockdown_enabled
  feature :account_restriction_on_read

  feature :apps_create_public
  feature :apps_lotus_faster_rendering, public: true
  feature :debounced_set_and_save_ui, public: true
  feature :apps_status_button_api_fix, public: true
  feature :apps_expand_collapse, public: true
  feature :apps_react_container, public: true
  feature :apps_tracking, public: true
  feature :apps_new_reorder_page, public: true
  feature :apps_lazy_loading, public: true
  feature :apps_new_ajax_request, public: true
  feature :apps_core_apis_instrumentation, public: true
  feature :apps_zaf_loader, public: true
  feature :zaf_edge, public: true

  feature :account_prefer_lotus
  feature :redact_fields_for_incremental_export

  feature :hide_flag_to_end_users
  feature :dynamics_soapaction
  feature :lotus_reporting
  feature :lotus_garden_chrome_330, public: true
  feature :lotus_radar_expiry
  feature :voice_block_risky, arturo: true
  feature :voice_norwegian_numbers, arturo: true
  feature :voice_2017_ga_numbers, arturo: true
  feature :voice_2017_q2_beta_numbers, arturo: true
  feature :voice_2018_q1_ga_numbers, arturo: true
  feature :voice_beta_phone_number_access, arturo: true
  feature :voice_italian_numbers_purchase_on, arturo: true
  feature :voice_nigerian_numbers, arturo: true
  feature :voice_recharge, public: true
  feature :voice_recharge_self_service, public: true
  feature :voice_logging
  feature :voice_transfer, public: true, arturo: true
  feature :voice_via_only_voice_comments
  feature :voice_chrome_bugfix, public: true
  feature :voice_recording_not_available, public: true
  feature :voice_retry_ticket_pop, public: true
  feature :voice_period_15_mins
  feature :voice_insights_v2, public: true
  feature :voice_html5_player_for_greetings, public: true
  feature :restrict_voice_trial
  feature :voice_reject_when_offline, public: true
  feature :voice_classic_twilio_13
  feature :voice_forwarding_number_verification_disabled
  feature :voice_formatted_phone_numbers, public: true
  feature :talk_cti_partner, subscription: true, arturo: false
  feature :non_usd_percent_coupon, public: true
  feature :voice_data_deletion
  feature :guide_account_service_error
  feature :voice_disable_subscription_notifications
  feature :lotus_feature_voice_staff_service_roles
  feature :voice_ask_routing_integration
  feature :lotus_feature_voice_agent_view_dashboard

  feature :voice_dashboard_enabled,
    arturo: false,
    public: true,
    require: ->(account) { account.voice_feature_enabled?(:dashboard) }

  feature :voice_basic_dashboard_enabled,
    arturo: false,
    public: true,
    require: ->(account) { account.voice_feature_enabled?(:basic_dashboard) }

  feature :voice_vip, arturo: true
  feature :voice_toll_free_au_number, arturo: true
  feature :voice_downgrade_agents_support, arturo: true
  feature :voice_delete_number_on_downgrade, arturo: true
  feature :voice_read_user_seats_from_the_db, arturo: true
  feature :cti_integrations, subscription: true, arturo: false
  feature :verification_captcha
  feature :credit_card_sanitization, setting: :credit_card_redaction, subscription: true, arturo: false
  feature :prevent_v2_basic_auth
  feature :security_email_notifications_for_account_settings
  feature :security_email_notifications_for_user_identities
  feature :security_email_notifications_for_password_strategy
  feature :private_attributes_in_end_user_presenter
  feature :help_center_analytics, public: true, subscription: true, arturo: false
  feature :help_center_google_analytics, subscription: true, arturo: false
  feature :help_center_grandfather_ga
  feature :help_center_onboarding_classic
  feature :help_center_guide_plans, public: true
  feature :knowledge_bank, public: true
  feature :knowledge_bank_list_management, subscription: true, arturo: false
  feature :dropbox2_beta_subdomains
  feature :urban_airship_response_logging
  feature :advanced_logging
  feature :ab_product_discovery # exists only for test/controllers/api/v2/experiments_controller_test.rb
  feature :ab_voice_t4st_v2 # exists only for test/models/jobs/zopim/create_chat_trial_job_test.rb
  feature :low_agent_seat_radar_notification, public: true
  feature :ab_voice_post_onboarding_v1, public: true
  feature :ab_voice_ticket_call_menu_experiment_v1, public: true
  feature :sms_extended_notification_timeout
  feature :web_widget_onboarding, public: true
  feature :discovery_channel_text, public: true
  feature :boost_countdown, public: true
  feature :hosted_ip_ssl
  feature :lotus_feature_voice_soft_delete_recordings
  feature :tiff_prevent_inline
  feature :play_audio_inline
  feature :switch_kyev_to_kyiv
  feature :enable_voice_after_voice_seat_creation, arturo: true
  feature :rescrub_archived_records
  feature :resolution_uses_solved_at
  feature :fix_voice_via_override_on_followup
  feature :remote_support_promo_boost_extension_plan
  feature :remote_support_promo_boost_extension_addon
  feature :update_type_validation, arturo: !Rails.env.test?

  feature :help_widget,
    public: true,
    require: ->(account) {
      account.subscription.pre_patagonia?
    }

  feature :plus_help_widget,
    public: true,
    require: ->(account) {
      account.subscription.pre_patagonia?
    }

  feature :patagonia_enterprise_help_widget,
    public: true,
    require: ->(account) {
      account.subscription.patagonia?
    }

  feature :patagonia_plus_help_widget,
    public: true,
    require: ->(account) {
      account.subscription.patagonia?
    }

  feature :plus_help_widget_chat,
    public: true,
    require: ->(account) {
      account.subscription.plan_type == SubscriptionPlanType.Large
    }

  feature :plus_help_widget_question,
    public: true,
    require: ->(account) {
      account.subscription.plan_type == SubscriptionPlanType.Large
    }

  feature :regular_help_widget_question,
    public: true,
    require: ->(account) {
      account.subscription.plan_type == SubscriptionPlanType.Medium
    }

  feature :chat_html_notifications, public: true
  feature :channels_pause_monitor
  feature :delay_channels_accounts_fetch, public: true
  feature :es_tags_autocomplete
  feature :search_use_ja_extended_analyzer
  feature :search_advanced_tags_autocomplete, arturo: true

  feature :search_return_limit_in_search_service
  feature :search_green
  feature :ss_enable_search_service_hub
  feature :search_grant_access_export_api
  feature :help_center_elasticsearch
  feature :new_agent_search_ui, public: true
  feature :proxima_nova
  feature :google_group_alias_x_been_there
  feature :end_user_search
  feature :allow_agents_to_access_targets

  feature :prevent_deletion_if_churned
  feature :ticket_scrubbing
  feature :ticket_scrubbing_low_priority_job
  feature :ticket_metadata_deletion_at_close
  feature :skip_anonymized_ticket_followup
  feature :ticket_user_scrubbing_after_close
  feature :ticket_user_scrubbing_killswitch
  feature :test_metadata_scrub, arturo: true, public: false, require: ->(_account) do
    !Rails.env.production?
  end
  feature :delete_ticket_metadata_pii, setting: true, arturo: false, public: true, require: ->(account) do
    account.has_ticket_metadata_deletion_at_close? || account.has_test_metadata_scrub?
  end

  feature :extend_recoverable_ticket_age
  feature :saml_audience_verify, arturo: Rails.env.production?
  feature :satisfaction_dashboard, public: true, subscription: true, arturo: false
  feature :optimized_deleted_tickets
  feature :throttle_endpoint_limit_ticket_api
  feature :throttle_endpoint_limit_ticket_api_master
  feature :throttle_endpoint_limit_ticket_index_api
  feature :throttle_endpoint_limit_ticket_index_api_master
  feature :origin_limit_ticket_api
  feature :origin_limit_ticket_api_log
  feature :suppress_sending_datadog_threshold_data
  feature :ticket_field_limits
  feature :throttle_endpoint_limit_tag_index_api
  feature :throttle_endpoint_limit_tag_index_api_master
  feature :use_scaling_strategies_for_rate_limiting
  feature :force_ticket_index_index
  feature :ticket_audit_force_index, arturo: !Rails.env.test?
  feature :mobile_google_auth_enabled, arturo: Rails.env.production?
  feature :user_views_tag_queries_without_forcing_index
  feature :use_user_policy_for_password_resets
  feature :api_v1_jsonp
  feature :log_warden_failures, arturo: Rails.env.production?
  feature :filter_mobile_devices
  feature :mobile_views_execute_date_sideloads

  feature :mobile_sdk_old_support, public: true
  feature :mobile_sdk_jwt_http
  feature :mobile_dropbox_api
  feature :mobile_sdk_skip_unverified_emails_check
  feature :mobile_sdk_support_never_request_email_setting
  feature :mobile_sdk_help_center_article_voting_enabled
  feature :mobile_sdk_support_show_closed_requests_setting
  feature :mobile_sdk_support_show_referrer_logo_setting
  feature :mobile_sdk_support_last_commenting_agents
  feature :mobile_sdk_support_system_message
  feature :mobile_sdk_support_v2_snippets
  feature :mobile_sdk_allow_only_end_users
  feature :mobile_sdk_throttle_push_notification_registrations_1h
  feature :mobile_sdk_dont_show_archived_requests
  feature :mobile_present_error_on_no_agent_logins
  feature :mobile_sdk_throttle_push_notification_deregistrations
  feature :mobile_sdk_support_public_updated_at_using_created_at
  feature :mobile_sdk_jwt_fifteen_seconds_timeout
  feature :mobile_sdk_api_redirector
  feature :mobile_sdk_jwt_circuit_breaker
  feature :mobile_sdk_jwt_low_timeout
  feature :mobile_sdk_talk_settings
  feature :mobile_sdk_show_unity_docs
  feature :sdk_auth_synchronous_user_locale

  # Arturo's to turn on account wide rate limiting for SDK endpoints
  # When you create a Arturo for this category please follow the following naming convention:
  # enable_throttle_for_mobile_sdk_#{controller_name}_#{action}
  # where controller_name is: https://apidock.com/rails/ActionController/Base/controller_name/class
  # and action is the controller action you want to apply the rate limit to.
  feature :enable_throttle_for_mobile_sdk_requests_index
  feature :enable_throttle_for_mobile_sdk_requests_create
  feature :enable_throttle_for_mobile_sdk_requests_show_many
  feature :enable_throttle_for_mobile_sdk_requests_update
  feature :enable_throttle_for_mobile_sdk_requests_show
  feature :enable_throttle_for_mobile_sdk_access_sdk_anonymous
  feature :enable_throttle_for_mobile_sdk_access_sdk_jwt
  feature :enable_throttle_for_mobile_sdk_comments_index
  feature :enable_throttle_for_mobile_sdk_ticket_forms_show_many
  feature :enable_throttle_for_mobile_sdk_user_tags_create
  feature :enable_throttle_for_mobile_sdk_user_tags_destroy_many
  feature :enable_throttle_for_mobile_sdk_current_user_show
  feature :enable_throttle_for_mobile_sdk_current_user_update
  feature :enable_throttle_for_mobile_sdk_uploads_create

  feature :enable_throttle_for_mobile_sdk_settings_v2_show

  # SDK Account Wide Rate Emergency Limit Arturo's
  # When you create a Arturo for this category please follow the following naming convention:
  # mobile_sdk_#{controller_name}_#{action}_emergency_limit
  # where controller_name is: https://apidock.com/rails/ActionController/Base/controller_name/class
  # and action is the controller action you want to apply the rate limit to.
  feature :mobile_sdk_requests_index_emergency_limit
  feature :mobile_sdk_requests_create_emergency_limit
  feature :mobile_sdk_requests_show_many_emergency_limit
  feature :mobile_sdk_requests_update_emergency_limit
  feature :mobile_sdk_requests_show_emergency_limit
  feature :mobile_sdk_access_sdk_anonymous_emergency_limit
  feature :mobile_sdk_access_sdk_jwt_emergency_limit
  feature :mobile_sdk_comments_index_emergency_limit
  feature :mobile_sdk_ticket_forms_show_many_emergency_limit
  feature :mobile_sdk_user_tags_create_emergency_limit
  feature :mobile_sdk_user_tags_destroy_many_emergency_limit
  feature :mobile_sdk_current_user_show_emergency_limit
  feature :mobile_sdk_current_user_update_emergency_limit
  feature :mobile_sdk_uploads_create_emergency_limit

  feature :mobile_sdk_settings_v2_show_emergency_limit

  # Answer Bot SDK
  feature :mobile_sdk_answer_bot

  # mobile SDK blips
  feature :mobile_sdk_blips
  feature :mobile_sdk_required_blips
  feature :mobile_sdk_behavioural_blips
  feature :mobile_sdk_pathfinder_blips

  # Mobile CSAT
  feature :mobile_sdk_csat

  # This flag is used on views to remove 'Rate My App' tab with user's previous
  # 'Rate My App' setting. And also it is used to set user's
  # 'Rate My App' setting to false in controller on update and create actions.
  feature :mobile_sdk_rma_tab_removal

  # Toggles the Cache-Control Headers used by the proxy to cache or not the
  # Mobile SDK settings.
  feature :mobile_sdk_settings_cache_control_headers
  # ZD-5019175 Roll out ticket skills cache fix in lotus
  feature :fix_ticket_skills_cache, public: true, arturo: !Rails.env.development?
  # ZD-5019175 Force ticket skills update to wait for next ticket in Play Mode
  feature :ticket_skills_next_ticket_promise, public: true, arturo: !Rails.env.development?

  # ZD-5969077 Seen tickets are removed from shared queue in play mode
  feature :view_queue_prevent_removing_seen_tickets_from_shared_queue

  feature :skill_based_view_filters,
    public: true,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_skill_based_ticket_routing? }

  feature :skill_based_ticket_routing_v2,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_skill_based_ticket_routing? }

  feature :filtered_views_discrepancy,
    public: true,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_skill_based_ticket_routing? }

  feature :log_skill_based_tickets_in_views,
    public: true,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_skill_based_ticket_routing? }

  feature :log_attribute_values_assignment,
    public: true,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_skill_based_ticket_routing? }

  feature :skill_based_ticket_routing, public: true, arturo: !Rails.env.development?, subscription: true
  # We support automated mapping of skills to tickets if the general feature is on
  # (i.e. if skill_based_ticket_routing is set) AND skill_based_attribute_ticket_mapping
  # is set.  This is a safety valve- we can shut off skill_based_attribute_ticket_mapping
  # in production if the automated mapping is overloading the system, without
  # shutting down all skill based routing features.  But use with caution- skill
  # mapping won't happen, and there's no supported way to recover that data.
  feature :skill_based_attribute_ticket_mapping,
    subscription: true,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_skill_based_ticket_routing? }
  feature :incremental_skill_based_routing_api, arturo: !Rails.env.development?
  feature :show_skills_on_agent_page, public: true, arturo: !Rails.env.development?
  feature :show_edit_ticket_skills_setting, public: true, arturo: !Rails.env.development?
  feature :bulk_assign_agents_to_skill,
    public: true,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_skill_based_ticket_routing? }
  feature :sbrv2_instance_value_durable_backfill

  # TR-544 Manually rerunning routing rules on a ticket via API
  # In addition to the automated mapping of skills to tickets,
  # we support manual running of skills based attribute ticket mapping
  # if the general feature (i.e. if skill_based_ticket_routing is set)
  # AND rerun_skill_based_routing_rules is set.
  feature :rerun_skill_based_routing_rules,
    public: true,
    require: ->(account) { account.has_skill_based_ticket_routing? }

  # Process routing attribute value delete as background job
  feature :delayed_routing_attribute_value_deletion, arturo: !Rails.env.test?

  # Remove invalid attribute ticket map associations for a given account
  feature :backfill_account_attribute_ticket_map_cleanup

  feature :fix_group_filter_for_agent_search

  feature :sunset_macro_actions_endpoint

  feature :dc_context_for_macro_application
  feature :macro_render_dc_markdown
  feature :validate_cms_text_update

  feature :no_org_tags_for_ticket_placeholder

  feature :view_edit,       public: true

  feature :ticket_metrics_sets_with_sub_query
  feature :instrument_ticket_save, arturo: !Rails.env.test?

  feature :trigger_revision_history,
    arturo: false,
    subscription: true,
    public: true

  feature :rule_usage_read_from_rules_cluster, arturo: !Rails.env.test?

  feature :multiple_group_views_reads, arturo: true, public: true
  feature :multiple_group_views_writes, arturo: true, public: true

  feature :apply_rules_table_limits, arturo: !Rails.env.test?
  feature :apply_rules_table_limits_for_triggers, arturo: !Rails.env.test?
  feature :higher_automations_table_limit
  feature :higher_triggers_table_limit
  feature :higher_macros_table_limit
  feature :higher_views_table_limit

  feature :rule_admin_rollbar, require: ->(_) { !Rails.env.development? }

  feature :rule_matching_extraction, public: true

  feature :targets_v2_failures, arturo: !Rails.env.development?
  feature :targets_remove_authorization_header
  feature :yammer_oauth2
  feature :hide_deprecated_targets, arturo: !Rails.env.test?

  feature :log_targets_v2_requests, arturo: !Rails.env.test?
  feature :log_targets_v2_test_targets, arturo: !Rails.env.test?
  feature :log_macro_attachments, arturo: !Rails.env.test?

  # See classic with zendesk_channels gem for this group of features
  # All channels features

  # Twitter features
  feature :twitter_saved_search, arturo: false, subscription: true
  feature :twitter_search_auto_conversion
  feature :twitter_add_link_default_false
  feature :twitter_maintenance, public: true
  feature :twitter_backfill_monitored_twitter_handles, arturo: !Rails.env.test?
  feature :twitter_dms_via_twitter_proxy, arturo: !Rails.env.test?
  feature :twitter_favorites_via_twitter_proxy, arturo: !Rails.env.test?
  feature :twitter_use_id_str_for_user_mapper, arturo: !Rails.env.test?
  feature :twitter_ignore_status_tweets_with_mentions, arturo: !Rails.env.test?
  feature :twitter_auto_populate_reply_metadata, arturo: !Rails.env.test?
  feature :twitter_comment_threading, arturo: !Rails.env.test?
  feature :twitter_fetch_missing_mentions, arturo: !Rails.env.test?
  feature :twitter_quote_tweets, arturo: !Rails.env.test?

  # Facebook features
  feature :block_facebook_outgoing_retry # Hack for Facebook returning retry error for non-retryable action
  feature :facebook_api_version_6_0
  feature :facebook_comment_id_check, arturo: !Rails.env.test? # check for comment if inconsistency
  feature :facebook_extend_comment_polling, arturo: !Rails.env.test?
  feature :facebook_extend_query_daterange, arturo: !Rails.env.test?
  feature :facebook_metadata_backfill, arturo: !Rails.env.test?
  feature :facebook_messenger_eu_privacy_patch, arturo: !Rails.env.test?
  feature :channels_query_only_resource_for_metadata, arturo: !Rails.env.test?

  # AnyChannel Feature
  feature :channels_linking_makes_channelback_events
  feature :channels_return_201_when_integration_exists, arturo: !Rails.env.test?
  feature :channels_allow_any_channels_internal_notes, arturo: !Rails.env.test?
  feature :channels_cfc_use_media_url, arturo: !Rails.env.test?
  feature :skip_safe_update_check, arturo: !Rails.env.test?
  # End of zendesk_channels gem related features

  feature :paginate_triggers_view
  feature :comment_on_queries, arturo: Rails.env.production?
  feature :skip_ticket_sharing_jobs

  # Fraud features (restrictions, jobs, rate limits)
  feature :account_fraud_service_enabled
  feature :fraud_score_job_maxmind, arturo: !Rails.env.test?
  feature :fraud_score_job_new_trials
  feature :fraud_score_job_risk_assessment_daily_job
  feature :orca_classic_disable_email_template_risky_account
  feature :check_verified_identity_if_signup_is_required, arturo: !Rails.env.test?
  feature :check_existing_tickets_if_signup_is_required, arturo: !Rails.env.test?
  feature :unverified_ticket_creations
  feature :use_trigger_req_received_body_v4
  feature :orca_fraud_signals_processor_received_header_patterns
  feature :orca_classic_descriptions_in_trigger, arturo: !Rails.env.test?
  feature :orca_classic_pravda_sell_product
  feature :orca_classic_fraud_signals_span, arturo: !Rails.env.test?
  feature :orca_classic_fraud_score_job_update_owner_email
  feature :orca_classic_increase_model_count_on_update
  feature :orca_classic_ip_safelist, arturo: !Rails.env.test?
  feature :orca_classic_nilsimsa_atsd_block_spam_digest_log_only
  feature :orca_classic_nilsimsa_atsd_compare_spam_digests
  feature :orca_classic_closed_ticket_check
  feature :orca_classic_replace_fraud_report_job_with_fraud_score_job, arturo: !Rails.env.test?
  feature :orca_classic_recaptcha_enterprise
  feature :orca_classic_spam_detector_blacklisted_requester_name, arturo: !Rails.env.test?
  feature :orca_classic_req_name_blacklist_take_action, arturo: !Rails.env.test?

  # Request Endpoint Restrictions
  feature :orca_classic_restrict_anon_requests_endpoint, arturo: !Rails.env.test?
  feature :orca_classic_restrict_anon_requests_endpoint_log_only, arturo: !Rails.env.test?
  feature :orca_help_center_skip_header_for_anon_users, arturo: !Rails.env.test?

  # Fraud service actions
  feature :auto_suspend_via_afs
  feature :cancel_via_afs
  feature :enable_captcha_via_afs
  feature :free_mail_via_afs
  feature :manual_suspend_via_afs
  feature :mark_abusive_via_afs
  feature :mark_not_abusive_via_afs
  feature :mark_risk_assessment_as_safe_age_via_afs
  feature :mark_risk_assessment_as_safe_paid_via_afs
  feature :outbound_email_risky_via_afs
  feature :outbound_email_safe_via_afs
  feature :support_risky_via_afs
  feature :takeover_via_afs
  feature :talk_risky_via_afs
  feature :unsuspend_via_afs
  feature :unwhitelist_via_afs
  feature :whitelist_via_afs

  # Ticket spam detector
  feature :spam_detector_comments
  feature :spam_detector_blacklist_pattern_check
  feature :spam_detector_bad_ip
  feature :orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains
  feature :spam_detector_inbound_spam
  feature :orca_classic_spam_detector_blacklisted_requester_email
  feature :orca_classic_spam_detector_simulate_patterns, arturo: !Rails.env.test?
  feature :orca_classic_rate_limit_master
  feature :orca_classic_rate_limit_request_spam_challenge
  feature :orca_classic_rate_limit_tickets_primary
  feature :orca_classic_rate_limit_new_acc_tickets
  feature :orca_classic_rate_limit_trial_acc_tickets
  feature :orca_classic_log_nilsimsa_score, arturo: !Rails.env.test?
  feature :orca_classic_blocklist_all_channel_log_only, arturo: !Rails.env.test?
  feature :orca_classic_blocklist_all_channel, arturo: !Rails.env.test?

  # Fraud Spam Cleanup
  feature :orca_classic_create_spam_cleanup_z1_ticket
  feature :orca_classic_delete_fraud_ticket
  feature :orca_classic_spam_cleanup_job_enable
  feature :orca_classic_tag_fraud_ticket
  feature :orca_classic_spam_cleanup_job_spammy_strings
  feature :orca_classic_email_rspamd_training

  # Catpcha Controls
  feature :orca_classic_skip_end_user_recaptcha

  feature :internal_audit_name_visibility_check

  feature :pci_credit_card_custom_field,
    public: true,
    arturo: false,
    require: ->(account) { account.has_pci? }

  feature :fix_ticket_due_date_v2, public: true
  feature :occam_rule_execution
  feature :occam_stop_fallback_to_classic
  feature :occam_count_many_api_limit
  feature :occam_slow_rule_handler, public: true
  feature :automatic_view_blocking
  feature :views_disable_count_many
  feature Zendesk::Rules::ViewsRateLimiter.handle_for(:count_many)
  feature Zendesk::Rules::ViewsRateLimiter.handle_for(:view_find)
  feature Zendesk::Rules::ViewsRateLimiter.handle_for(:view_count)
  feature Zendesk::Rules::ViewsRateLimiter.handle_for(:preview_find)
  feature Zendesk::Rules::ViewsRateLimiter.handle_for(:preview_count)
  feature Zendesk::Rules::ViewsRateLimiter.handle_for(:view_play)
  feature Zendesk::Rules::ViewsRateLimiter.handle_for(:view_tickets)
  feature :views_enforce_rate_limits
  feature :views_rate_limit_lotus
  feature :views_strict_description_count_validation
  feature :views_any_channel_via_id
  feature :views_cache_429
  feature :views_circuit_breaker
  feature :views_count_many_circuit_breaker
  feature :hanlon_parity_check
  feature :hanlon_cache
  feature :hanlon_cache_info_api
  feature :hanlon_watch_count
  feature :lotus_real_time_counts, public: true
  feature :lotus_real_time_counts_continuously_polling, public: true
  feature :lotus_real_time_counts_pubsub, public: true
  feature :lotus_real_time_counts_ui_updates, public: true
  feature :cached_view_counts_for_pagination
  feature :audit_logs_refactor
  feature :views_disable_all_features

  feature :rule_execution_metrics
  feature :api_activity, public: true
  feature :api_activity_job, public: true
  feature :api_activity_store_seven_days, public: true

  feature :api_allow_post_in_search
  feature :account_deletion_kill_switch
  feature :help_center_powered_by
  feature :ssl_validation_sha1_signature

  feature :reject_bounced_mail_from_amazon
  feature :apps_api_rate_limit, arturo: !Rails.env.development?
  feature :apps_client_rate_limit, public: true
  feature :account_wide_api_rate_limit, arturo: !Rails.env.development?
  feature :default_json_content_type
  feature :legacy_csv_exports

  feature :access_token_sso_bypass
  feature :auth_flow_mismatch_stats

  feature :user_and_organization_fields, public: true, subscription: true, arturo: false
  feature :play_tickets, public: true, subscription: true, arturo: false
  feature :play_tickets_advanced, public: true, subscription: true, arturo: false
  feature :unlimited_automations, public: true, subscription: true, arturo: false
  feature :unlimited_views, public: true, subscription: true, arturo: false
  feature :unlimited_triggers, public: true, subscription: true, arturo: false

  feature :user_and_organization_tags,
    arturo: false,
    require: ->(account) {
      account.has_user_and_organization_fields? &&
        account.settings.has_user_tags?
    }

  feature :tickets_inherit_requester_tags,
    arturo: false,
    require: ->(account) {
      account.has_user_and_organization_tags? &&
        account.settings.tickets_inherit_requester_tags?
    }
  feature :ticket_tags_via_widget
  feature :validate_pipes_for_tags
  feature :ticket_tagging_fix, arturo: !Rails.env.test?

  feature :billing_primary_sync, public: true
  feature :hpm2_classic, public: true
  feature :neuter_cc_errors, public: true
  feature :billing_multiprod_success_page, public: true
  feature :billing_maintenance, public: true
  feature :billing_paypal_support, public: true
  feature :billing_subscription_iframe, public: true
  feature :billing_agent_add_request, public: true
  feature :billing_suite, public: true
  feature :billing_pod_aware_sync
  feature :billing_migration_request_allowed
  feature :billing_agent_downgrade_audit
  feature :billing_pricebook_consolidation, public: true
  feature :billing_ooc_zuora, public: true
  feature :billing_journal_comparator, public: true
  feature :billing_buy_now_zendesk_suite

  feature :bill_suite_upsell, public: true

  # deprecate billing related classic APIs
  feature :deprecate_classic_coupons_controller
  feature :deprecate_coupons_redemptions_controller
  feature :billing_deprecate_classic_pricing, public: true

  feature :parallel_automations
  feature :automations_find_tickets_on_slave
  feature :restrict_suite, public: true
  feature :suite_purchase_override, public: true

  feature :apps_private, subscription: true, arturo: false, public: true
  feature :apps_public, subscription: true, arturo: false, public: true
  feature :conditional_fields_app, subscription: true, arturo: false, public: true

  feature :macro_application_no_liquid, public: true

  feature :dc_in_macro_titles
  feature :dc_in_role_names
  feature :special_chars_in_custom_field_options
  feature :bulk_insert_or_update_custom_field_options
  feature :disable_closing_tickets_via_sharing

  # Re-engagement features
  feature :reengagement_enabled, public: true, arturo: true, require: ->(account) do
    account.has_native_messaging? && account.settings.reengagement
  end

  # Email features
  feature :email_remove_attachments_if_no_public_comment_placeholder, arturo: !Rails.env.test?
  feature :email_ccs_cache_fix, public: true
  feature :email_light_agent_forwarding_first_comment_private, arturo: !Rails.env.test?
  feature :email_suspend_ticket_logs, arturo: !Rails.env.test?
  feature :email_disable_light_agent_forwarding, arturo: !Rails.env.test?
  feature :email_followup_ticket_html_comment_fix, arturo: !Rails.env.test?
  feature :email_encode_address_name_fix, arturo: !Rails.env.test?
  feature :email_exclude_inline_attachments_fix, arturo: !Rails.env.test?
  feature :email_log_inbound_mail_rate_limit, arturo: !Rails.env.test?
  feature :email_notification_body_with_dir_auto_tags, arturo: !Rails.env.test?
  feature :email_account_whitelist_multiplier, arturo: !Rails.env.test?
  feature :email_private_comment_requester_missing_flag, arturo: !Rails.env.test?
  feature :email_check_invalid_group, arturo: !Rails.env.test?
  feature :email_replace_invalid_group, arturo: !Rails.env.test?
  feature :email_outbound_logging, arturo: !Rails.env.test?
  feature :email_debugging_mode, arturo: !Rails.env.test?
  feature :email_enable_initial_import, public: true, arturo: !Rails.env.test?
  feature :email_suspend_mail_from_intercom, arturo: !Rails.env.test?
  feature :email_suspend_mail_from_auto_response, arturo: !Rails.env.test?
  feature :email_create_undeliverable_user_for_open_accounts_only, arturo: !Rails.env.test?
  feature :email_update_all_targets_on_bounce, arturo: !Rails.env.test?
  feature :email_switch_off_redis_deletions, arturo: !Rails.env.test?
  feature :email_switch_bounce_processing_logging_mode
  feature :email_switch_bounce_processing_debugging_mode
  feature :email_hard_reject_text_api_with_no_content, arturo: !Rails.env.test?
  feature :email_log_text_api_with_no_content, arturo: !Rails.env.test?
  feature :email_outbound_blocker, arturo: !Rails.env.test?
  feature :email_outbound_account_circuit_breaker, arturo: true
  feature :email_cleanup_to_headers_from_forward_header, arturo: !Rails.env.test?
  feature :email_suspend_recipient_nice_id_forgery, arturo: !Rails.env.test?
  feature :email_handle_inline_attachments_with_same_filename, arturo: !Rails.env.test?
  feature :email_real_attachments_send_via_gmail, arturo: !Rails.env.test?
  feature :email_logs_for_gmail_sending, arturo: !Rails.env.test?
  feature :email_recipient_address_enforce_sorting, arturo: !Rails.env.test?
  feature :email_threading_investigation, arturo: !Rails.env.test?
  feature :email_application_mailer_refactor, arturo: !Rails.env.test?
  feature :email_notification_profile_admin_added_duplicates_fix
  feature :email_ignore_rate_limits
  feature :email_uses_rinku_autolinking_with_fallback, arturo: !Rails.env.test?
  feature :email_make_other_user_comments_private, arturo: !Rails.env.test?
  feature :email_settings_paginated, arturo: !Rails.env.test?, public: true
  feature :email_log_welcome_email_bounce, arturo: !Rails.env.test?
  feature :email_suppress_email_identity_update_notifications, arturo: !Rails.env.test?
  feature :email_atsd_processor, arturo: !Rails.env.test?
  feature :email_update_cloudmark_obvious_spam_threshold, arturo: !Rails.env.test?
  feature :email_reject_no_from_name, arturo: !Rails.env.test?
  feature :new_password_expired_email
  feature :truncate_incomplete_row
  feature :email_log_blank_macro_reference, arturo: !Rails.env.test?
  feature :email_blank_macro_reference, arturo: !Rails.env.test?
  feature :email_agent_non_hostmapped_urls, arturo: !Rails.env.test?
  feature :email_gdpr_remote_files, arturo: !Rails.env.test?
  feature :email_suspend_ticket_from_unverified_account_owner, arturo: !Rails.env.test?
  feature :email_replace_message_id_null_string, arturo: !Rails.env.test?
  feature :email_track_smtp_server_busy_errors, arturo: !Rails.env.test?
  feature :email_find_last_header_key_after_first, arturo: !Rails.env.test?
  feature :email_disable_duplicate_verification_emails, public: true, arturo: !Rails.env.test?
  feature :email_set_inbound_email_from_address, arturo: !Rails.env.test?
  feature :email_suspend_mail_from_welcome_freedcamp, arturo: !Rails.env.test?
  feature :email_suspend_mail_from_mailer_doodle, arturo: !Rails.env.test?
  feature :email_audit_email_cache_control_header, arturo: !Rails.env.test?
  feature :email_force_utf8_encoding_in_content_text_parser, arturo: !Rails.env.test?
  feature :email_truncate_notifications_suppressed_for_metadata, arturo: !Rails.env.test?
  feature :email_ticket_email_ccs_suspension_threshold, arturo: !Rails.env.test?
  feature :email_ticket_email_ccs_suspension_threshold_logging, arturo: !Rails.env.test?
  feature :email_inbound_mail_rate_limits, arturo: !Rails.env.test?
  feature :email_log_agent_sender_with_nonmatching_from_and_reply_to, arturo: !Rails.env.test?
  feature :email_ccs_update_for_private_comment_fix, arturo: !Rails.env.test?
  feature :email_log_ticket_attributes_during_edit_access_check, arturo: !Rails.env.test?
  feature :email_yahoo_forwarding_validation_processor_new_regex, arturo: !Rails.env.test?
  feature :email_reply_parser_instrumentation_processor, arturo: !Rails.env.test?
  feature :email_inbound_mail_mtc_latency_fix, arturo: !Rails.env.test?
  feature :email_non_hostmapped_recipients_encoded_id_fix, arturo: !Rails.env.test?
  feature :email_fix_current_collaborators_set_collaborators, arturo: !Rails.env.test?
  feature :email_fix_current_collaborators_mail_collaboration, arturo: !Rails.env.test?
  feature :email_train_rspamd_in_mtc, arturo: !Rails.env.test?
  feature :email_suspended_ticket_ignore_nil_recipient, arturo: !Rails.env.test?
  feature :email_pre_styled_rich_api_comments, arturo: !Rails.env.test?
  feature :email_allow_light_agent_to_add_private_comment, arturo: !Rails.env.test?
  feature :email_redact_attachments_from_eml_and_json, arturo: !Rails.env.test?
  feature :email_escape_forward_header_spaces_fix, arturo: !Rails.env.test?
  feature :email_inbound_pipeline_record_event_based_slo, arturo: !Rails.env.test?
  feature :email_mtc_instrument_processing_duration_event_slo, arturo: !Rails.env.test?
  feature :email_mrj_record_event_based_slo, arturo: !Rails.env.test?
  feature :email_rename_ccs_blocklist, arturo: !Rails.env.test?
  feature :email_deprecate_cname_checks, arturo: !Rails.env.test?, public: true
  feature :email_redact_attachments_when_public_comment_placeholders_not_exists, arturo: !Rails.env.test?
  feature :email_deflection_mailer_redact_attachments, arturo: !Rails.env.test?
  feature :email_mtc_memory_auto_exit, arturo: !Rails.env.test?
  feature :email_mtc_memory_gc_logging, arturo: !Rails.env.test?
  feature :email_metrics_on_suspended_tickets_recovery, arturo: !Rails.env.test?
  feature :email_ignore_spam_multiplier, arturo: !Rails.env.test?
  feature :email_ignore_spam_multiplier_logging, arturo: !Rails.env.test?
  feature :email_malware_recovery_metric, arturo: !Rails.env.test?
  feature :email_allow_logged_with_email_id_smtp_delivery_method, arturo: !Rails.env.test?
  feature :email_fallback_to_logged_smtp_delivery_method, arturo: !Rails.env.test?
  feature :email_comment_placeholder_detected_metric, arturo: !Rails.env.test?

  feature :email_end_user_comment_privacy_settings, arturo: !Rails.env.test?
  feature :ccs_requester_excluded_public_comments, setting: true, arturo: false, require: -> (account) do
    account.has_email_end_user_comment_privacy_settings? && account.has_comment_email_ccs_allowed_enabled?
  end

  # Allow both real_attachments and secure_attachments
  feature :email_attachments_real_and_secure, arturo: !Rails.env.test?

  # These capabilities are on-hold/blocked for the time being
  feature :third_party_end_user_public_comments_setting, arturo: !Rails.env.test?
  feature :third_party_end_user_public_comments, setting: true, arturo: false, require: -> (account) do
    account.has_third_party_end_user_public_comments_setting?
  end

  # Support admin Email redesign
  feature :email_new_settings, arturo: !Rails.env.test?, public: true
  feature :email_dns_results, arturo: !Rails.env.test?
  feature :email_settings_api, arturo: !Rails.env.development?

  # Email Takeover Vulnerability Work
  feature :email_takeover_vulnerability_stats, arturo: !Rails.env.test? # Gather logs and stats only

  # ReplyTo Vulnerability Work
  feature :email_mtc_add_from_header_state_variable, arturo: !Rails.env.test?
  feature :email_reply_to_vulnerability_check, arturo: !Rails.env.test?, require: ->(account) do
    account.has_email_mtc_add_from_header_state_variable?
  end
  feature :email_reply_to_vulnerability_forwarding_processor, arturo: !Rails.env.test?, require: ->(account) do
    account.has_email_reply_to_vulnerability_check?
  end
  feature :email_reply_to_vulnerability_collaborator_processor, arturo: !Rails.env.test?, require: ->(account) do
    account.has_email_reply_to_vulnerability_check?
  end
  feature :email_reply_to_vulnerability_text_api_processor, arturo: !Rails.env.test?, require: ->(account) do
    account.has_email_reply_to_vulnerability_check?
  end
  feature :email_reply_to_vulnerability_surface_reply_to_address, arturo: !Rails.env.test?, require: ->(account) do
    account.has_email_reply_to_vulnerability_check?
  end
  feature :email_reply_to_vulnerability_attachment_processor, arturo: !Rails.env.test?, require: ->(account) do
    account.has_email_reply_to_vulnerability_check?
  end
  feature :email_reply_to_vulnerability_user_processor, arturo: !Rails.env.test?, require: ->(account) do
    account.has_email_reply_to_vulnerability_check?
  end
  feature :email_reply_to_vulnerability_end_user_impersonation, arturo: !Rails.env.test?

  # MTC to MPQ migration
  feature :email_nokogiri_parent_fix, arturo: !Rails.env.test?

  # BYOIP
  feature :email_enable_byoip, arturo: !Rails.env.test?
  feature :email_byoip_relay_warmup, arturo: !Rails.env.test?

  # Abuse/Email features
  feature :email_fraud_signals_processor_received_headers, arturo: !Rails.env.test?
  feature :email_fraud_signals_processor_received_headers_log_only, arturo: !Rails.env.test?
  feature :email_fraud_signals_processor_regruhosting_php_mailer, arturo: !Rails.env.test?
  feature :email_fraud_signals_processor_high_rspamd_localhost_unknown, arturo: !Rails.env.test?
  feature :email_fraud_signals_processor_high_rspamd_localhost_unknown_log_only, arturo: !Rails.env.test?
  feature :email_fraud_signals_processor_scan_signup_suspended_emails, arturo: !Rails.env.test?
  feature :email_placeholder_suppress_subject, arturo: !Rails.env.test?
  feature :email_placeholder_suppress_with_liquid, arturo: !Rails.env.test?
  feature :email_placeholder_subject_deflection_suppression, arturo: !Rails.env.test?
  feature :email_placeholder_body_deflection_suppression, arturo: !Rails.env.test?
  feature :email_placeholder_check_ticket_author, arturo: !Rails.env.test?
  feature :email_yahoo_forward_header_parsing, arturo: !Rails.env.test?

  # Email Rspamd features
  feature :email_skip_spam_processor
  feature :email_rspamd
  feature :email_rspamd_training_enabled, arturo: !Rails.env.test?
  feature :disable_suspending_email_spam_requesters, public: true
  feature :email_record_spam_marking_reply_stats, arturo: !Rails.env.test?
  feature :email_new_spam_processors

  # SenderAuth features
  feature :email_sender_authentication_logging
  feature :email_sender_auth_disreputable_senders, arturo: !Rails.env.test?
  feature :email_sender_auth_skip_rspamd_reply, arturo: !Rails.env.test?
  feature :email_sender_auth_no_recipient_fix, arturo: !Rails.env.test?
  feature :email_mtc_sender_auth_dmarc_logging, arturo: !Rails.env.test?
  feature :email_mtc_sender_auth_dmarc, arturo: !Rails.env.test?
  feature :email_mtc_sender_auth_dmarc_v2, arturo: !Rails.env.test?
  feature :email_sender_auth_report_metrics_v2, arturo: !Rails.env.test?

  # Simplified Email Threading
  feature :email_simplified_threading, public: true, require: ->(account) { account.settings.simplified_email_threading? }
  feature :email_simplified_threading_placeholders, arturo: !Rails.env.test?

  # Allows an Admin to enable/disable the feature from Admin Email Settings page.
  feature :email_simplified_threading_onboarding, public: true, require: ->(account) { account.has_email_simplified_threading_placeholders? }

  # Isolated/general features introduced for Simplified Email Threading
  feature :email_fix_gmail_markup, arturo: !Rails.env.test?
  feature :email_invisible_metadata_span_tags
  feature :email_invisible_message_id, arturo: !Rails.env.test?, require: -> (account) { account.has_email_simplified_threading? }

  # No-Delimiter features
  feature :email_no_delimiter, public: true
  feature :no_mail_delimiter, setting: true, arturo: false, require: ->(account) do
    account.has_email_no_delimiter?
  end
  feature :email_instrument_delimiter
  feature :email_remove_no_delimiter_feedback_link, public: true
  feature :email_enable_mtc_cid_resolution, arturo: !Rails.env.test?, require: ->(account) { account.has_no_mail_delimiter_enabled? }
  feature :email_multipart_html_only_content_fix, arturo: !Rails.env.test?
  feature :email_multipart_text_only_content_fix, arturo: !Rails.env.test?
  feature :email_fix_no_delimiter_redaction, arturo: !Rails.env.test?
  feature :email_attachment_only_reply_fix, arturo: !Rails.env.test?

  # Attachment Size Limit Increase features
  # NOTE: This arturo will be shared between MPQ and MTC/Classic
  feature :email_increase_attachment_size_limit, arturo: !Rails.env.test?

  # New CCs & Followers experience (has_email_ccs? is defined as a separate Capability below due to its prior
  # checks of other Arturos which are now removed - its usage will be removed separately)
  feature :email_ccs, arturo: false, public: true, require: ->(account) { account.has_email_ccs? }

  # The following `has_follower_and_email_cc_collaborations_enabled?` checks identify if an account
  # has CCs and Followers V3.

  # Old Collaboration/"CCs" in Ticket settings. Enabled by default.
  feature :collaboration_enabled, setting: true, arturo: false, public: true, require: ->(account) do
    !account.has_follower_and_email_cc_collaborations_enabled?
  end

  # New CCs & Followers experience, accounts that switched to the new mode. Disabled by default.
  feature :follower_and_email_cc_collaborations, setting: true, arturo: false, public: true, require: ->(account) do
    account.has_email_ccs?
  end

  # "Enable CCs" in Ticket settings. Enabled by default.
  feature :comment_email_ccs_allowed, setting: true, arturo: false, public: true, require: ->(account) do
    account.has_follower_and_email_cc_collaborations_enabled?
  end

  # "Allow light agents to be CC'd on a ticket" in Ticket settings. Disabled by default.
  feature :light_agent_email_ccs_allowed, setting: true, arturo: false, public: true, require: ->(account) do
    account.has_comment_email_ccs_allowed_enabled?
  end

  # "Enable followers" in Ticket settings. Enabled by default.
  feature :ticket_followers_allowed, setting: true, arturo: false, public: true, require: ->(account) do
    account.has_follower_and_email_cc_collaborations_enabled?
  end

  # "Agents can change requester" in Ticket settings. Enabled by default.
  feature :agent_can_change_requester, setting: true, arturo: false, require: ->(account) do
    account.has_follower_and_email_cc_collaborations_enabled?
  end

  # "Automatically make an agent CC a follower" in Ticket settings. Enabled by default.
  feature :agent_email_ccs_become_followers, setting: true, arturo: false, require: ->(account) do
    account.has_comment_email_ccs_allowed_enabled? && account.has_ticket_followers_allowed_enabled?
  end

  # Internal only: Indicates whether an account should be offered the rollback option for CCs/Followers.
  # Default value is off but will eventually be enabled for new accounts.
  feature :ccs_followers_no_rollback, setting: true, arturo: false

  # Post-LA CCs & Followers features
  feature :email_ccs_light_agents_v2, arturo: !Rails.env.test?, public: true, require: ->(account) { account.has_email_ccs? }

  feature :email_ccs_onboarding, arturo: !Rails.env.test?, public: true
  feature :email_ccs_transform_collaborations, arturo: !Rails.env.test?
  feature :email_restricted_agent_fix, arturo: !Rails.env.test?
  feature :email_ccs_preload_notifications_with_ccs, arturo: !Rails.env.test?, require: ->(account) do
    account.has_follower_and_email_cc_collaborations_enabled?
  end
  feature :email_ccs_default_rules_content, arturo: !Rails.env.test?
  feature :email_ccs_new_accounts, arturo: !Rails.env.test?
  feature :email_ccs_internal_comment_fix, public: true
  feature :email_ccs_clear_collaborations_for_removal, arturo: !Rails.env.test?
  feature :email_collaboration_by_light_agents_fixed, arturo: !Rails.env.test?

  feature :original_recipient_address_audit, arturo: !Rails.env.test?
  feature :rounded_avatars, public: true
  feature :title_in_portal_presence_validation, arturo: Rails.env.production?
  feature :skip_time_tracking_fields_on_followups
  feature :followup_only_uses_source_fields
  feature :custom_field_options_caching
  feature :suspend_automated_inbound_emails_from_zendesks
  feature :preserve_recovered_ticket_owner
  feature :sms, public: true
  feature :tweet_requester_action
  feature :lotus_agent_availability, public: true
  feature :suppress_unicode_replacement

  feature :sse_prevent_anonymous_uploads, arturo: !Rails.env.test?
  feature :sse_prevent_anonymous_uploads_log_only, arturo: !Rails.env.test?
  feature :sse_deprecate_portal_requests, arturo: !Rails.env.test?
  feature :sse_agent_attachment_view_policy, arturo: !Rails.env.test?
  feature :sse_deprecate_log_controller, arturo: !Rails.env.test?
  feature :sse_agent_attachment_view_policy_log_only
  feature :sse_remove_attachment_show, arturo: !Rails.env.test?

  feature :collaborators_settable_in_help_center,
    public: true,
    arturo: false,
    require: ->(account) { account.has_collaborators_settable_in_help_center? }

  feature :sse_remove_proxy_direct_endpoint, arturo: !Rails.env.test?

  feature :target_failure, arturo: !Rails.env.development?, public: true
  feature :ticket_target_retry, arturo: true
  feature :anonymous_requests_throttled, arturo: Rails.env.production?
  feature :rtl_lang_html_attr, public: true, arturo: Rails.env.production?
  feature :pathfinder_app, subscription: true, arturo: false, public: true
  feature :channels_jobs_run_in_channels_service, arturo: true
  feature :csat_metrics_adjustment
  feature :public_app_market_admin_ui, arturo: true, public: true
  feature :app_market_main_page_link, arturo: true, public: true
  feature :apps_zaf_v1_extension, arturo: true, public: true
  feature :throttle_user_create_or_update
  feature :throttle_user_writes_leaky_bucket
  feature :rate_limit_create_or_update_on_unique_user
  feature :domain_blacklist_whitelist_on_slaves, arturo: true
  feature :user_identity_limit_for_trialers, arturo: Rails.env.production?
  feature :user_identity_limit
  feature :api_filter_requested_tickets, arturo: true
  feature :ticket_join_force_key
  feature :pluck_billable_agents

  feature :limited_registration, arturo: Rails.env.production?
  feature :x_on_behalf_of, arturo: !Rails.env.development?

  feature :easy_agent_add, public: true

  feature :paypal, public: true # PayPal as a payment method (Zopim on Zuora only)

  # See: ZD#1586362
  feature :eu_data_center, subscription: true, public: true, arturo: false
  feature :germany_data_center, subscription: true, public: true, arturo: false
  feature :us_data_center, subscription: true, public: true, arturo: false
  feature :advanced_security, subscription: true, public: true, arturo: false

  # Revere related features.
  feature :revere_subscriptions, public: true

  feature :view_attachments_without_target_and_private_attachments_on

  feature :voltron_chat, public: true
  feature :synchronous_chat_provisioning

  feature :stores_backfill, public: true

  feature :api_tags_disabled_for_end_user_presenter
  feature :dropdown_multiselect, public: true
  feature :lotus_bootstrap_service

  feature :native_conditional_fields_migration_needed, arturo: false, public: true, require: ->(account) do
    account.has_ticket_forms? &&
    account.cfa_migration_needed?
  end

  feature :native_conditional_fields_enabled, arturo: false, public: true, require: ->(account) do
    account.has_ticket_forms? &&
    !account.has_native_conditional_fields_abandoned? &&
    !account.cfa_migration_needed?
  end

  feature :native_conditional_fields_abandoned, arturo: false, public: true, require: ->(account) do
    account.settings.native_conditional_fields_abandoned?
  end

  feature :conditional_fields_app_installed, arturo: false, public: true, require: ->(account) do
    account.has_ticket_forms? &&
    account.cfa_installed?
  end

  feature :conditional_fields_app_enabled, arturo: false, public: true, require: ->(account) do
    account.has_ticket_forms? &&
    account.cfa_enabled?
  end

  # Temp feature to rollout a security improvement in deleted_tickets_controller
  feature :ticket_restore_access_improvement, arturo: !Rails.env.test?

  # Temp feature to collect data about unverified identities creating
  # anonymous requests in accounts that require verification.
  feature :log_unverified_identities_creating_anonymous_requests, arturo: !Rails.env.test?

  # Temp feature to collect data about accounts without ticket_tagging
  # updating tags on tickets
  feature :log_updating_ticket_tags_without_ticket_tagging, arturo: !Rails.env.test?

  # This is used to collect data for a specific account in the maintenance job (ThesarusuResetJob)
  # that updates the tag scores for all accounts.
  feature :measure_tag_scores_job

  # Temp feature to rollout an API change to let customers override comment publicity in Ticket Merge endpoint
  feature :override_comment_publicity_in_ticket_merge_endpoint, arturo: !Rails.env.test?

  # Temp feature to rollout a fix for a N+1 in ticket forms endpoint
  feature :fix_brands_n_plus_one_in_ticket_forms, arturo: !Rails.env.test?

  # Throws validation error when followup tickets have an invalid via_followup_source_id
  feature :validate_closed_ticket_for_followup_tickets, arturo: !Rails.env.test?

  # Temp feature to rollout a perf. improvement related to custom field options during ticket save
  feature :cache_custom_field_option_values, arturo: !Rails.env.test?

  feature :people_alpha, public: true
  feature :chat_product_tray, public: true
  feature :talk_product_tray, public: true
  feature :voice_csv_generation, arturo: true, public: true

  feature :disable_updating_requester_via_ticket, arturo: !Rails.env.development?

  feature :lotus_react, public: true
  feature :lotus_react_table_in_dashboard, public: true
  feature :lotus_react_table_in_filters, public: true
  feature :lotus_react_table_in_user, public: true
  feature :lotus_react_table_in_org, public: true
  feature :lotus_react_table_in_search, public: true
  feature :lotus_react_table_in_incidents, public: true

  feature :sandbox_eap,
    public: true,
    arturo: false,
    require: ->(account) { account.multiple_sandboxes? }
  feature :admin_center_framework_view_sandbox, public: true

  feature :notes_collapse, public: true

  # Controls access to old feedback tab (before Web Widget)
  feature :sunset_feedback_tab

  feature :welcome_account_owner_line3_v3

  feature :skip_locale_bulk_update_job, arturo: !Rails.env.development?
  feature :short_circuit_locale_bulk_update_job

  # Enable fetching translations from a CDN
  feature :translations_from_cdn,
    arturo: !Rails.env.development?,
    public: true,
    require: ->(account) {
      cdn = account.cdn_provider
      SUPPORTED_TRANSLATION_CDNS.include?(cdn) && account.send(:"has_translations_from_#{cdn}_cdn?")
    }

  feature :translations_fallback_rosetta_api,
    arturo: !Rails.env.development?,
    require: ->(account) { account.has_translations_from_cdn? }

  # One feature for each of `SUPPORTED_TRANSLATION_CDNS` to make the definition
  # explicit and easy to find references. If you add or remove anything here,
  # make sure you check the `translations_from_cdn` feature still works.
  #
  # If you remove one of these features, check for references by searching for
  # `translations_from_` because the feature name is dynamically built in places
  feature :translations_from_disabled_cdn,   arturo: !Rails.env.development?, public: true
  feature :translations_from_default_cdn,    arturo: !Rails.env.development?, public: true
  feature :translations_from_edgecast_cdn,   arturo: !Rails.env.development?, public: true
  feature :translations_from_cloudfront_cdn, arturo: !Rails.env.development?, public: true
  feature :translations_from_fastly_cdn,     arturo: !Rails.env.development?, public: true

  feature :mobile_app_links_ui

  feature :product_update_link, public: true
  feature :strip_bad_external_ids_in_sdk_anonymous

  feature :connect_sdk
  feature :plain_body_on_all_comment_types

  feature :api_enable_show_many_deleted_users
  feature :dc_account_content_cache_default

  feature :api_disable_v2_ticket_exports

  feature :on_behalf_of_user_flag, arturo: !Rails.env.development?

  feature :agent_chat_limits, public: true

  feature :pattern_matching_without_spaces_for_thai
  feature :match_partial_words_in_asian
  feature :apply_prefix_and_normalize_phone_numbers
  feature :simple_format_dc_in_rich_comments

  feature :user_level_shared_view_ordering, public: true
  feature :trial_ticket_bulk_import_rate_limit

  feature :api_add_organization_ids_to_users
  feature :skip_organization_validation
  feature :unset_ticket_org_when_removing_default_user_org, public: true

  feature :endusers_org_active_fix
  feature :invalid_organization_logging
  feature :sns_push_notifications

  # Should be removed when API-893 is validated
  # https://zendesk.atlassian.net/browse/API-893
  # Not enabled on production by default
  feature :rate_limit_unauthorized_api_requests, arturo: Rails.env.production?

  feature :api_v2_group_membership_full_sideload

  # API V1 deprecation bits
  feature :deprecate_api_v1_stats

  # old controller deprecation
  feature :deprecate_home_controller
  feature :deprecate_embedded_requests
  feature :deprecate_portal_search_controller

  feature :disabled_search_sideload_for_end_users
  feature :org_reassign_domain_mapping_kill_switch

  feature :parameter_whitelist_report_violations

  feature :prevent_expired_trial_account_access, arturo: !Rails.env.test?

  feature :mobile_sdk_blip_collection_control
  feature :suspended_users_filter

  feature :user_email_identity_backfill_killswitch

  feature :inline_images_rebuild_path_v2

  feature :redirect_from_lotus_according_agent_entitlements

  feature :react_rte, public: true
  feature :org_reassign_synchronize_on_domain
  feature :mobile_requests_ordering_fix

  feature :audit_remove_force_index
  feature :limit_group_count_with_archived
  feature :etag_caching_organizations_index, arturo: !Rails.env.test?
  feature :sell_skip_trial_cancellation

  # Sunshine
  feature :hawaii_account_has_access, public: true
  feature :mauna_kea, public: true
  feature :custom_objects_filtering_enabled, public: true

  feature :ocp_reverse_sync
  feature :ocp_synchronous_entitlements_update
  feature :ocp_talk_account_sync
  feature :ocp_talk_entitlements_backfill
  feature :ocp_explore_entitlements_backfill
  feature :ocp_unified_suite_downgrade
  feature :ocp_guide_account_sync
  feature :ocp_agent_assignable_on_enterprise
  feature :ocp_support_shell_account_creation
  feature :ocp_support_shell_account_creation_phase1
  feature :ocp_support_shell_account_creation_phase2
  feature :ocp_support_shell_account_creation_all
  feature :ocp_limit_owner_name_cjk_characters_on_account_creation
  feature :central_admin_staff_mgmt_roles_tab, public: true
  feature :ocp_chat_only_agent_deprecation
  feature :ocp_support_agent_roles_backfill
  feature :ocp_reverse_sync_non_multiproduct
  feature :ocp_custom_role_explore_permission_backfill
  feature :ocp_log_crl_events_to_ledger
  feature :ocp_support_entitlements_backfill
  feature :ocp_cp3_suite_check_support_for_zopim_agent_creation
  feature :ocp_check_seats_left_in_staff_service
  feature :staff_service_seats_remaining
  feature :ocp_guide_light_agent_role_sync
  feature :ocp_guide_light_agent_role_backfill
  feature :ocp_last_sell_admin_downgrade
  feature :ocp_guide_light_agent_entitlement_backfill
  feature :ocp_account_products_use_cache
  feature :ocp_unified_entitlement_sync
  feature :ocp_spp
  feature :ocp_guide_entitlement_backfill
  feature :ocp_set_import_job_default_locale
  feature :ocp_aw_backfill
  feature :ocp_user_views_new_limit
  feature :ocp_talk_entitlements_for_talk_lite_backfill
  feature :ocp_spp_light_agent_cap_alternative
  feature :ocp_users_autocomplete_query, public: true
  feature :ocp_compare_classic_staff_service_agent_count

  # Backfill existing essential(small) account with groups features
  feature :backfill_essential_groups

  feature :brand_creator_read_from_master

  # Side Conversations
  feature :ticket_threads, arturo: false, setting: true, subscription: true
  feature :ticket_threads_eap, arturo: !Rails.env.development?
  feature :side_conversation_macros, arturo: false, require: ->(account) do
    (account.has_ticket_threads? || account.has_ticket_threads_eap?) && account.settings.ticket_threads
  end

  feature :side_conversation_slack_macros, arturo: false, require: ->(account) do
    account.has_side_conversations? && account.is_side_conversations_slack_enabled
  end

  feature :side_conversation_ticket_macros, arturo: !Rails.env.development?, require: ->(account) do
    account.has_side_conversations_tickets?
  end

  feature :side_conversations_email, public: true, arturo: false, require: ->(account) do
    account.is_side_conversations_email_enabled
  end

  feature :side_conversations_slack_ga
  feature :side_conversations_slack, public: true, arturo: false, require: ->(account) do
    account.is_side_conversations_slack_enabled
  end

  feature :side_conversations_tickets, public: true, arturo: false, require: ->(account) do
    account.has_side_conversations? &&
      account.settings.side_conversations_tickets &&
      ::Arturo.feature_enabled_for?(:lotus_feature_side_conversations_ticket_channel, account)
  end

  feature :side_conversations, public: true, arturo: false, require: ->(account) do
    account.has_ticket_threads? || account.has_ticket_threads_eap?
  end

  feature :include_side_conversations_in_search, public: true
  feature :include_side_conversations_count_in_search, public: true

  feature :custom_resources_account_deletion
  feature :exclude_facebook_messages

  # Used by Lotus to determine if the Zendesk Suite trial onboarding UI is to be shown
  # To be used for testing that onboarding UI
  feature :suite_trial_onboarding, public: true
  feature :support_suite_trial_refresh_phase2, public: true

  feature :suite_trial,
    arturo: false,
    public: true,
    require: ->(account) {
      account.settings.suite_trial?
    }

  # Sustaining
  feature :use_get_for_rules_analysis_show
  feature :include_archived_on_bulk_updates
  feature :org_id_in_crm_zendesk_users, arturo: !Rails.env.test?
  feature :org_association_update_default_only, arturo: !Rails.env.test?
  feature :org_association_always_remove_with_fallback, arturo: !Rails.env.test?
  feature :bulk_job_errors_attribute
  feature :use_safe_regexp, arturo: !Rails.env.test?
  feature :restrict_user_policy
  feature :user_merge_job_audit
  feature :user_merge_move_tickets_in_batches

  # Argonauts
  feature :enforce_user_and_org_field_size_limit

  # Capacity and performance
  feature :views_cache_control_header
  feature :search_cache_control_header
  feature :dynamic_content_cache_control_header
  feature :ticket_forms_cache_control_header
  feature :experiment_properties_cache_control_header
  feature :account_settings_cache_control_header
  feature :user_show_sideloads_cache_control_header
  feature :current_user_show_cache_control_header
  feature :ticket_requested_cache_control_header
  feature :mobile_requests_index_cache_control_header
  feature :groups_controller_cache_control_header
  feature :custom_roles_cache_control_header
  feature :user_fields_cache_control_header
  feature :organization_fields_cache_control_header
  feature :dashboard_agent_stats_caching
  feature :views_compact_cache_control_header
  feature :disable_rollup_by_group
  feature :brands_index_cache_control_header
  feature :groups_assignable_cache_control_header

  # Pagination
  feature :cursor_pagination_default_users
  feature :remove_offset_pagination_users
  feature :cursor_pagination_default_deleted_users
  feature :remove_offset_pagination_deleted_users
  feature :cursor_pagination_product_limit_users
  feature :cursor_pagination_organization_memberships_index
  feature :remove_offset_pagination_organization_memberships_index
  feature :cursor_pagination_groups_index
  feature :remove_offset_pagination_groups_index
  feature :cursor_pagination_groups_assignable
  feature :remove_offset_pagination_groups_assignable
  feature :cursor_pagination_group_memberships_index
  feature :remove_offset_pagination_group_memberships_index
  feature :cursor_pagination_group_memberships_assignable
  feature :remove_offset_pagination_group_memberships_assignable
  feature :cursor_pagination_custom_fields_index
  feature :remove_offset_pagination_custom_fields_index
  feature :cursor_pagination_custom_fields_options_index
  feature :remove_offset_pagination_custom_fields_options_index
  feature :cursor_pagination_organizations_index
  feature :remove_offset_pagination_organizations_index
  feature :cursor_pagination_remove_v1_deleted_users
  feature :cursor_pagination_remove_v1_users
  feature :bolt_throttle_index_with_deep_pagination_users
  feature :cursor_pagination_v2_ticket_audits_endpoint, arturo: !Rails.env.test?
  feature :cursor_pagination_v2_satisfaction_ratings_endpoint, arturo: !Rails.env.test?
  feature :cursor_pagination_v2_ticket_incidents_endpoint, arturo: !Rails.env.test?
  feature :email_cursor_pagination_suspended_tickets_index, arturo: !Rails.env.test?
  feature :email_cursor_pagination_recipient_addresses_index, arturo: !Rails.env.test?
  feature :cursor_pagination_v2_requests_endpoint, arturo: !Rails.env.test?

  # Count endpoints
  feature :count_tickets_endpoint, arturo: !Rails.env.test?
  feature :count_tags_endpoint, arturo: !Rails.env.test?

  feature :classic_short_circuit_limiter
  feature :search_short_circuit_limiter
  feature :reduce_incremental_event_results
  feature :reduce_incremental_ticket_results
  feature :reduce_incremental_ticket_api_payload
  feature :liquid_c
  feature :cache_dynamic_content_account_content
  feature :ticket_metrics_v2_limiter

  feature :views_api_rescue_occam_errors

  feature :attempt_to_find_ticket_by_ticket_id_from_audit
  feature :ticket_events_presenter_preload_ticket

  feature :restrict_unauthenticated_user
  feature :build_tfe_cache_for_raw_exports

  # In Lotus, if an agent has a ticket open in a tab, but not the tab with focus,
  # and the ticket is updated, notify the agent
  feature :notify_of_external_updates, public: true
  feature :time_bound_incremental_api_processing

  # Facebook has recently removed access to user profile pages.
  # This feature will be used to disable the profile Links ZD-3569639
  feature :turn_off_fb_user_profile_deep_link, public: true

  feature :load_limit_explore_exports

  # MSS-305: Enables the push notification to Mobile SDK APPs of non SDK ticket updates
  feature :mobile_sdk_notify_non_sdk_updates

  # Enable trial length experiments (added for 14-day trial)
  feature :use_alternative_trial_length, arturo: !Rails.env.test?

  feature :agent_as_end_user
  feature :agent_as_end_user_allow_anonymous_via
  feature :downcase_tags_as_ascii
  feature :build_tag_array_from_current_tags, arturo: !Rails.env.test?
  feature :cache_user_related_counts
  feature :contextual_workspaces, subscription: true, arturo: false, public: true
  feature :ticket_presenter_no_satisfaction_preloads
  feature :ticket_presenter_no_satisfaction_score_preloads
  feature :assume_csrf_protection, public: true
  feature :present_ticket_bulk_edit_permission, public: true

  # Temp feature to rollout security fix. Checks if user has permission to change status, priority and ticket type.
  feature :system_fields_permission_check, arturo: !Rails.env.test?

  feature :custom_field_options_limit

  # Enables Lotus React version of GDPR (Deleted Users) page
  feature :gdpr_lotus_react, public: true

  feature :use_comment_body_in_xml_export

  feature :remove_soft_deletion_requirement

  feature :publish_ticket_events_to_bus, arturo: !(Rails.env.test? || Rails.env.development?)
  feature :publish_tfe_domain_events, arturo: !(Rails.env.test? || Rails.env.development?)
  feature :publish_cc_events_to_bus,
    arturo: !(Rails.env.test? || Rails.env.development?),
    require: ->(account) { account.has_publish_ticket_events_to_bus? }
  feature :inc_api_enforce_last_minute_limit

  feature :publish_ticket_entities_to_bus
  feature :views_entity_republisher
  feature :views_entity_republisher_rollout
  feature :publish_ticket_on_scrub, arturo: !Rails.env.test?

  feature :guard_archive_account_reads, arturo: !(Rails.env.test? || Rails.env.development?)

  feature :safe_update_bulk_ticket_jobs, arturo: (!Rails.env.test? || Rails.env.development?)

  # rubocop:enable Layout/ExtraSpacing

  # Agent workspace
  feature :polaris, arturo: false, public: true, require: ->(account) {
    account.has_polaris_option? && account.settings.polaris
  }
  feature :fallback_omnicomposer, arturo: true, public: true, require: ->(account) {
    account.has_polaris?
  }
  feature :public_chat_transcripts
  feature :enable_triggers_for_new_chat_ticket
  feature :chat_uploads
  feature :chat_ticket_metrics, arturo: !Rails.env.test?
  feature :sync_support_agent_avatar_to_chat, require: ->(account) {
    account.has_polaris?
  }

  feature :delete_personal_macros_when_delete_agent, public: true, arturo: !Rails.env.test?
  feature :backfill_delete_orphaned_personal_macros, public: true
  feature :agent_workspace_role_restriction, public: true
  feature :preview_with_last_chat_message, arturo: !Rails.env.test?
  feature :preserve_macros_text_editor_spacing, public: true

  # Controls whether to skip Resque job for Contextual Workspaces
  feature :contextual_workspaces_in_js,
    arturo: true,
    public: true,
    require: ->(account) {
      account.has_contextual_workspaces?
    }

  # Enables checking of author_id when creating Comment
  feature :validate_author_belongs_to_account_on_create

  # Added for this ticket https://support.zendesk.com/agent/tickets/4774586
  feature :skip_deleting_expired_tokens

  feature :refactor_cursor_inc_export_query

  feature :set_macro_attachment_content_disposition, arturo: !Rails.env.test?

  feature :batch_process_gdpr_deletion_requests
  feature :ignore_mobile_v2

  # Stripping of session set-cookie headers from responses
  feature :assets_strip_response_cookies_generated_javascript_locale
  feature :assets_strip_response_cookies_generated_style_branding
  feature :assets_strip_response_cookies_photos_and_logos
  feature :assets_strip_response_cookies_voice_uploads
  feature :v2_users_radar_token_strip_session_set_cookie

  feature :write_encrypted_target_credentials, arturo: !Rails.env.test?
  feature :backfill_encrypted_target_credentials_v2
  feature :backfill_encrypted_target_credentials_v3
  feature :backfill_targets_delete_plaintext_credentials
  feature :backfill_rotate_target_encrypted_values, arturo: !Rails.env.test?
  feature :bulk_update_targets_status
  feature :read_encrypted_target_credentials, arturo: !Rails.env.test?
  feature :url_target_v1_circuit_breaker
  feature :url_target_v2_circuit_breaker
  feature :support_webhook_enabled
  feature :trial_account_target_limit

  feature :v2_users_allow_redacted_users_filter, arturo: !(Rails.env.test? || Rails.env.development?)
  feature :render_custom_uri_as_links
  feature :allow_seats_remaining, public: true
  feature :allow_seats_remaining_suite
  feature :redact_archived_and_closed_comments
  feature :lotus_feature_react_ticket, public: true
  feature :lotus_feature_apps_react_ticket_sidebar, public: true

  feature :sla_sideload_first_reply_time, arturo: !Rails.env.test?
  feature :sla_ticket_metrics_reader, arturo: !Rails.env.test?
  feature :sla_side_conversation_reply_time
  feature :accsrv_product_created_notification

  # Benchmark Survey Settings
  feature :account_settings_benchmark_opt_out, public: true, arturo: !Rails.env.test?
  feature :benchmark_opt_out, setting: true, arturo: false, require: -> (account) do
    account.has_account_settings_benchmark_opt_out?
  end

  feature :audit_logs_export_api_enabled
  feature :audit_logs_export_include_name
  feature :audit_logs_api_include_login_events
  feature :audit_logs_staff_service_entitlements

  feature :disable_lotus_expiry, public: true
  feature :block_large_comment_body_on_initialize

  feature :disable_legacy_people_users_id_page
  feature :can_manage_people_permission_check

  # Bolt / Users / Organizations
  feature :enable_search_for_org_related
  feature :disable_user_assume
  feature :active_taggers_skip_cache
  feature :satisfaction_rating_disable_comment_fallback
  feature :backfill_satisfaction_rating_comment_event_ids

  # SQID / SecDev / Authentication
  feature :account_assumption_control, arturo: !Rails.env.test?
  feature :account_assumption_control_for_sandboxes
  feature :attachments_on_separate_domain
  feature :audioeye_in_auth
  feature :central_admin_non_multiproduct, public: true # important: some billing front-end code depends on this arturo
  feature :classic_help_center_layout_jquery_3
  feature :custom_security_policy_backfill
  feature :disallow_verification_tokens_for_password_resets
  feature :downgrade_support_trial_specific_support_features
  feature :generate_password_reset_token_subclass
  feature :grandfathered_hostmapped_jwt
  feature :jwt_redact_backfill
  feature :lets_encrypt_acme_v2
  feature :log_auth_header
  feature :log_csrf_invalid_token
  feature :log_csrf_invalid_token_debug
  feature :mobile_app_identify_by_cookie # used in zendesk_auth
  feature :new_security_settings_tab, public: true, arturo: true # https://zendesk.atlassian.net/browse/SECDEV-1363 must be completed before this can be removed
  feature :passport
  feature :pci, subscription: true, arturo: false
  feature :publish_assumption_event_to_kafka
  feature :redact_jwt_token_in_audit_log
  feature :regenerate_session_id_after_self_password_change
  feature :restrict_csrf_bypass_to_mobile, arturo: Rails.env.production?
  feature :security_settings_for_all_accounts
  feature :update_account_associations_for_security_settings_api

  feature :jwt,
    arturo: false,
    require: ->(account) { account.has_security_feature?(:jwt) }
  feature :saml,
    arturo: false,
    require: ->(account) { account.has_security_feature?(:saml) }
  feature :ip_restriction,
    arturo: false,
    require: ->(account) { account.has_security_feature?(:ip_restrictions) }
  feature :custom_security_policy,
    arturo: false,
    require: ->(account) { account.has_security_feature?(:custom_security_policy) }
  feature :custom_session_timeout,
    arturo: false,
    require: ->(account) { account.has_security_feature?(:custom_session_timeout) }
  # End SQID / SecDev / Authentication

  # Indicates that account has a SunCo app provisioned, and thus has the
  # capability to enable native messaging and social messaging.
  feature :social_messaging_agent_workspace, public: true, arturo: !Rails.env.test?, require: ->(account) do
    account.settings.social_messaging_agent_workspace && account.has_polaris?
  end

  # Indicates availability of Native Messaging for accounts with social_messaging_agent_workspace capability
  feature :native_messaging, public: true, arturo: false, require: ->(account) do
    account.has_social_messaging_agent_workspace? && account.settings.native_messaging
  end

  # Indicates availability of Social Messaging for accounts with social_messaging_agent_workspace capability
  feature :social_messaging, public: true, arturo: false, require: ->(account) do
    account.has_social_messaging_agent_workspace? && account.settings.social_messaging
  end

  # Indicates availability of all additional SunCo channels. It is currently being used behind social_messaging_agent_workspace
  feature :all_sunco_messaging_channels, public: true

  # Indicates availability of Twitter DM and Facebook Messenger channels through SunCo. Currently used behind social_messaging_agent_workspace
  feature :social_messaging_twitter_facebook

  feature :social_messaging_allow_wechat

  feature :native_messaging_csat, require: ->(account) do
    account.has_native_messaging? && account.has_customer_satisfaction? && account.has_customer_satisfaction_enabled?
  end

  feature :sanitize_rule_tags, arturo: !Rails.env.test?

  # Fang: Allow only setting of active custom fields in business rules
  feature :set_active_only_custom_fields, arturo: !Rails.env.test?

  # Libretto / Fang: trigger improvements
  feature :instrument_trigger_matching, arturo: !Rails.env.test?
  feature :skip_triggers_caching

  # Enables response header rate limits for redirects to HC
  feature :rate_limit_hc_redirects
  feature :rate_limit_hc_redirects_challenge

  feature :rule_match_non_ascii_characters, arturo: !Rails.env.test?
  feature :validate_api_ticket_status, arturo: !Rails.env.test?

  feature :updated_workspace_changed_presenter, public: true

  # Libretto: Trigger categories
  feature :trigger_categories, public: true
  feature :trigger_categories_api, setting: true, arturo: false, public: true, require: -> (account) do
    account.has_trigger_categories? && account.settings.trigger_categories_api
  end
  feature :trigger_categories_new_accounts, require: -> (account) do
    account.has_trigger_categories?
  end
  feature :trigger_categories_break_triggers_endpoint

  # Libretto: Ticketing error budget
  feature :preload_audit_events_in_ticket_presenter, arturo: !Rails.env.test?

  # Polo: Enabled auto translation feature in Agent Workspace
  feature :lotus_feature_auto_translation, public: true

  feature :agent_workspace_omni_redaction, public: true

  feature :permissions_ticket_fields_manage
  feature :permissions_ticket_forms_manage
  feature :permissions_contextual_workspaces_manage
  feature :permissions_user_fields_manage
  feature :permissions_organization_fields_manage
  feature :external_permissions_presentation

  feature :no_partitioning_of_id_in_attachment_url

  feature :permissions_custom_role_policy_dual_save

  feature :publish_user_authentication_events
  feature :views_ticket_description_fix, arturo: !Rails.env.test?

  # Orchid: Contextual Workspaces
  feature :lotus_feature_increase_max_contextual_workspaces

  # Ticket Error Budget
  feature :process_attachment_on_create_only

  # rubocop:disable Naming/PredicateName
  module Capabilities
    # Polaris EAP => GA
    def has_polaris_option?
      # will remove :polaris guard after migrating accounts to lotus_feature_polaris_override
      (account.polaris_compatible_plan? && Arturo.feature_enabled_for?(:polaris, account)) || Arturo.feature_enabled_for?(:lotus_feature_polaris_override, account)
    end

    # Only display group and departments migration for chat compatible plans
    def display_group_and_department_migration?
      account.has_polaris_option? && account.polaris_chat_compatible_plan? && !account.settings.check_group_name_uniqueness
    end

    def has_finished_group_and_department_migration?
      return unless account.has_polaris_option?
      return true unless account.polaris_chat_compatible_plan?
      account.settings.check_group_name_uniqueness
    end

    # New Collaboration EAP, accounts part of the private beta
    def part_of_new_collaboration_eap_and_accepted_tos?
      has_email_ccs? && settings.accepted_new_collaboration_tos?
    end

    def accepted_tos_but_new_collaboration_disabled?
      settings.accepted_new_collaboration_tos? && !has_follower_and_email_cc_collaborations_enabled?
    end

    def has_custom_alerts?
      return false unless subscription.plan_type > SubscriptionPlanType.Small
      !Rails.env.production? || ['support'].member?(subdomain)
    end

    def has_user_views_sampling_enabled?
      return false unless count = settings.user_count
      count > UserView::SAMPLING_THRESHOLD
    end

    def has_emoji_autocompletion_enabled?
      settings.emoji_autocompletion?
    end

    def has_extended_ticket_types?
      @extended_ticket_types ||= field_ticket_type.try(:is_active?)
    end

    def has_extended_ticket_priorities?
      @extended_ticket_priorities ||= (field_priority.sub_type_id == PrioritySet.FULL)
    end

    def has_forums2?
      settings.forums2?
    end

    def business_hours_active?
      has_business_hours? && schedules.active.any?
    end

    def extended_ticket_metrics_visible?
      has_extended_ticket_metrics?
    end

    def use_status_hold?
      has_status_hold? && field_status.use_status_hold?
    end

    def has_mobile_switch_enabled?
      # The setting is enabled by default if the feature is not available
      # This is the reverse logic of the feature macro using settings: true
      settings.mobile_switch_enabled?
    end

    def has_screencasts_for_tickets_enabled?
      subscription.has_screencasts_for_tickets? &&
      settings.screencasts_for_tickets?
    end

    def has_community_enabled?
      subscription.has_community?
    end

    def has_time_zone_selection?
      subscription.try(:has_individual_time_zone_selection?)
    end

    def has_individual_language_selection?
      subscription.try(:has_individual_language_selection?)
    end

    def ticket_forms_is_active?
      return @ticket_forms_is_active if defined?(@ticket_forms_is_active)
      @ticket_forms_is_active = has_ticket_forms? && ticket_forms.limit(2).count > 1
    end

    def custom_fields_is_active?
      custom_fields.any?
    end

    def has_verification_captcha_enabled?
      has_verification_captcha? && settings.verification_captcha_enabled?
    end

    def remote_authentication_available?
      has_saml? || has_jwt?
    end

    def has_article_labels?
      ::Arturo.feature_enabled_for?(:help_center_grandfather_labels, self) ||
        has_help_center_article_labels?
    end

    def has_agent_aliases?
      [SubscriptionPlanType.Large, SubscriptionPlanType.Extralarge].include?(subscription.plan_type)
    end

    def enterprise_roles_in_user_views_enabled?
      subscription.plan_type == SubscriptionPlanType.ExtraLarge
    end

    def has_classic_reporting?
      # disable Classic reporting for accounts created after 2015/04/30
      reporting_deprecation_date = DateTime.new(2015, 4, 30)
      reporting_deprecation_date > created_at
    end

    def has_hc_powered_by_setting?
      subscription.plan_type > SubscriptionPlanType.Small
    end

    # Does the account offer a way for end users to see information, or is their Zendesk only for Agents?
    def has_end_user_ui?
      !(web_portal_disabled? && !help_center_enabled?)
    end

    def is_side_conversations_email_enabled=(value)
      account.settings.side_conversations_email = value
      account.settings.ticket_threads = value
    end

    def is_side_conversations_email_enabled
      account.settings.side_conversations_email || account.settings.ticket_threads
    end

    def is_side_conversations_slack_enabled=(value)
      account.settings.side_conversations_slack = value
    end

    def is_side_conversations_slack_enabled
      if Arturo.feature_enabled_for?(:side_conversations_slack_ga, account)
        account.settings.side_conversations_slack
      else
        if account.settings.any? { |s| s.name == 'side_conversations_slack' }
          account.settings.side_conversations_slack
        else
          Arturo.feature_enabled_for?(:collaboration_slack, account)
        end
      end
    end

    # AKA, "Legacy" CCs
    def is_collaboration_enabled=(value)
      account.settings.collaboration_enabled = value
    end

    # False when the CCs/Followers feature has been opted into
    def is_collaboration_enabled?
      account.has_collaboration_enabled_enabled?
    end
    alias_method :is_collaboration_enabled, :is_collaboration_enabled?

    # New CCs & Followers experience main rollout Arturo, defined here for performance reasons
    def has_email_ccs?
      true
    end

    # Accommodate for previous usage of this setting that had helpers from delegate_to_property_set
    def is_collaborators_addable_only_by_agents=(value)
      account.settings.collaborators_addable_only_by_agents = value
    end

    # Collaborations are addable only by Agents when CCs/Followers are disabled
    # And if the restriction has been enabled (in the settings).
    # So, it's always false when the CCs/Followers feature has been opted into.
    def is_collaborators_addable_only_by_agents?
      !account.has_follower_and_email_cc_collaborations_enabled? &&
        account.settings.collaborators_addable_only_by_agents?
    end
    alias_method :is_collaborators_addable_only_by_agents, :is_collaborators_addable_only_by_agents?

    def has_collaborators_settable_in_help_center?
      !is_trial? &&
        !is_collaborators_addable_only_by_agents? &&
        (legacy_ccs_or_email_ccs_enabled? && settings.collaborators_settable_in_help_center?)
    end

    def legacy_ccs_or_email_ccs_enabled?
      account.is_collaboration_enabled? || account.has_comment_email_ccs_allowed_enabled?
    end

    def all_collaboration_disabled?
      !account.is_collaboration_enabled? && !account.has_comment_email_ccs_allowed_enabled? && !account.has_ticket_followers_allowed_enabled?
    end

    def has_email_sender_authentication?
      settings.email_sender_authentication?
    end

    def send_real_attachments?
      if account.has_email_attachments_real_and_secure?
        settings.email_attachments?
      else
        settings.email_attachments? && !settings.private_attachments?
      end
    end
  end

  def self.pci_required_accounts_on_shard
    where(id: TicketField.where("ticket_fields.type = ?", 'FieldPartialCreditCard').uniq.pluck('account_id'))
  end
  # rubocop:enable Naming/PredicateName
end
