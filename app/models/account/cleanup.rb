class Account < ActiveRecord::Base
  module Cleanup
    # AccountPropertySet, Organization, RemoteAuthentication, Report, TicketField, Widget
    # These are the classes of child objects known to exist on bogus accounts, but that do not have foreign keys into account_ids.
    # These are left in the database to minimize risk.

    def delete_bogus_account_ids_entry(shard_id, bogus_account_id)
      if Account.exists?(bogus_account_id)
        raise "#{bogus_account_id} is an account that actually exists"
      end

      ActiveRecord::Base.on_shard(shard_id) do
        # delete dependent child objects
        [Ticket, Tagging, Group, Rule, Entry, User, Forum, QueueAudit, AccountSetting].each do |child_class|
          select_statement = "select id from #{child_class.table_name} where account_id = #{bogus_account_id.to_i}"
          ActiveRecord::Base.connection.select_values(select_statement).each do |child_id|
            child_class.delete child_id
            Rails.logger.info("Deleting child #{child_class.name} for bogus account #{bogus_account_id} on shard #{shard_id} as part of bogus account cleanup")
          end
        end

        # delete account_ids entry
        delete_statement = "delete from account_ids where id = #{bogus_account_id.to_i}"
        deleted_count = ActiveRecord::Base.connection.delete(delete_statement)
        if deleted_count == 1
          Rails.logger.info("Deleting bogus account_ids entry #{bogus_account_id} on shard #{shard_id}")
        else
          if deleted_count > 1
            raise "Expected exactly one row to be affected, but #{deleted_count} were!"
          else
            Rails.logger.warn("Shard #{shard_id} account_ids entry for #{bogus_account_id} didn't exist; can't be cleaned up.")
          end
        end
      end
    end

    def get_bogus_accounts_on_current_shard # rubocop:disable Naming/AccessorMethodName
      account_id_table_entries_current_shard - real_account_ids_current_shard
    end

    def account_id_table_entries_current_shard
      ActiveRecord::Base.connection.select_values('select * from account_ids').map(&:to_i)
    end

    def real_account_ids_current_shard
      Account.shard_unlocked.all.map(&:id)
    end
  end
end
