class Account < ActiveRecord::Base
  module RouteSupport
    def self.included(base)
      base.class_eval do
        validates_uniqueness_of :route_id, on: :update, case_sensitive: false, allow_nil: true, if: :route_id_changed?

        after_update :propagate_subdomain_to_route, if: :subdomain_changed?
        after_update :propagate_host_mapping_to_route, if: :host_mapping_changed?
        after_update :publish_brand

        before_update :sync_subdomain_from_route, if: :route_id_changed?
        before_update :sync_host_mapping_from_route, if: :route_id_changed?
      end
    end

    def propagate_subdomain_to_route
      if route && route.subdomain != subdomain
        logger.info "[Account] syncing account.subdomain to route.subdomain (Old: #{route.subdomain} => New: #{subdomain})"
        route.subdomain = subdomain
        route.save!(validate: false)
      end
    end

    def propagate_host_mapping_to_route
      if route && route.host_mapping != host_mapping
        logger.info "[Account] syncing account.host_mapping to route.host_mapping (Old: #{route.host_mapping} => New: #{host_mapping})"
        route.update_attribute(:host_mapping, host_mapping)
        # Reload new route back into kasket
        Account.on_master do
          route.reload
          brand = brands.find_by_route_id(route.id)
          brand.route.reload
        end
      end
    end

    def sync_subdomain_from_route
      if route.subdomain != subdomain
        logger.info "[Account] syncing route.subdomain to account.subdomain (Old: #{subdomain} => New: #{route.subdomain})"
        self.subdomain = route.subdomain
      end
    end

    def sync_host_mapping_from_route
      if route.host_mapping != host_mapping
        logger.info "[Account] syncing route.host_mapping to account_host_mapping (Old: #{host_mapping} => New: #{route.host_mapping})"
        self.host_mapping = route.host_mapping
      end
    end

    def publish_brand
      return unless has_publish_brand_entities?
      return unless changes.include?(:subdomain) || changes.include?(:host_mapping)

      BrandPublisher.new(account_id: id).publish_for_account
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: __LINE__, message: "Unable to publish Brand for account", fingerprint: 'account_brand_publish_failed', metadata: { account: id })
    end
  end
end
