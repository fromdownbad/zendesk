class Account
  module PatagoniaApiRateLimiter
    DEFAULT_RATE_LIMIT = 10
    INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON = 30

    def default_api_rate_limit
      return DEFAULT_RATE_LIMIT unless subscription.present?

      if subscription.has_high_volume_api?
        2500
      elsif subscription.has_api_limit_700_rpm_legacy?
        700
      elsif subscription.has_api_limit_700_rpm?
        700
      elsif subscription.has_api_limit_400_rpm?
        400
      elsif subscription.has_api_limit_200_rpm?
        200
      else
        # For Patagonia Bronze/Essentials plan, the API rate limit is 10 requests/min
        DEFAULT_RATE_LIMIT
      end
    end

    def can_be_boosted?
      return false unless subscription.present?
      subscription.has_high_volume_api? || subscription.has_api_limit_700_rpm_legacy?
    end

    # Note: used by Monitor from Api::V2::Internal::AccountSettingsPresenter
    def default_api_incremental_exports_rate_limit
      if subscription.has_high_volume_api?
        return INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON
      end

      # Set in app/models/account/properties.rb
      settings.default(:api_incremental_exports_rate_limit)
    end

    # Note: used by Monitor from Api::V2::Internal::AccountSettingsPresenter
    def api_incremental_exports_rate_limit
      if subscription.has_high_volume_api?
        ary = [
          settings.api_incremental_exports_rate_limit,
          INCREMENTAL_EXPORT_REQS_HIGH_VOLUME_ADD_ON
        ]
        return ary.max
      end

      settings.api_incremental_exports_rate_limit
    end

    def search_export_api_rate_limit
      settings.search_export_api_rate_limit
    end
  end
end
