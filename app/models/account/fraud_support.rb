class Account
  module FraudSupport
    DEFAULT_RISK_ASSESSMENT = {level: 'safe', product: 'support', reason: 'default'}.with_indifferent_access.freeze

    SAFE_AGE_RISK_ASSESSMENT = {level: 'safe', product: 'support', reason: 'daily job safe action'}.with_indifferent_access.freeze
    SAFE_PAID_RISK_ASSESSMENT = {level: 'safe', product: 'support', reason: 'daily job paid action'}.with_indifferent_access.freeze

    MONITOR_WHITELIST_RISK_ASSESSMENT = {level: 'whitelisted', product: 'support', reason: 'monitor whitelist action'}.with_indifferent_access.freeze
    MONITOR_TAKEOVER_RISK_ASSESSMENT = {level: 'suspended', product: 'support', reason: 'monitor takeover action'}.with_indifferent_access.freeze
    MONITOR_SUSPEND_RISK_ASSESSMENT = {level: 'suspended', product: 'support', reason: 'monitor suspend action'}.with_indifferent_access.freeze

    EMAIL_UNSAFE_RISK_ASSESSMENT = {level: 'risky', product: 'support', reason: 'outbound risky'}.with_indifferent_access.freeze
    EMAIL_SAFE_RISK_ASSESSMENT = {level: 'safe', product: 'support', reason: 'outbound safe'}.with_indifferent_access.freeze

    SUSPEND_RISK_ASSESSMENT = {level: 'suspended', product: 'support', reason: 'auto-suspend'}.with_indifferent_access.freeze
    SUPPORT_RISKY_RISK_ASSESSMENT = {level: 'risky', product: 'support', reason: 'support risky'}.with_indifferent_access.freeze
    TALK_RISKY_RISK_ASSESSMENT = {level: 'risky', product: 'talk', reason: 'talk risky'}.with_indifferent_access.freeze

    SAFE_RISK_LEVELS = %w[safe whitelisted].freeze
    UNSAFE_RISK_LEVELS = %w[risky].freeze
    UNDELIVERABLE_RISK_LEVELS = %w[suspended].freeze
    SELL_PRODUCT = 'sell'.freeze

    FRAUD_SCORE_LIMIT = 100

    MAXMIND_FRAUD_SCORE_THRESHOLD = 70

    PROHIBITED_VENDOR_ADDON_NAMES = %w[eu_data_center us_data_center].freeze

    PREMIUM_SUPPORT_TIER = %w[premier_support priority_support].freeze

    def abusive?
      return false if whitelisted?
      settings.is_abusive || pravda_client.try(:abusive)
    end

    def allow_email_template_customization?
      return true if whitelisted?
      return false if risky_email_trial_account?
      return true if outside_of_thresholds? && account_paid_invoice?

      false
    end

    def risky_email_trial_account?
      support_risky = support_risky?
      subscription.is_trial? && !(fraud_scores? && !support_risky)
    end

    def auto_suspended?
      abusive? && fraud_scores.any?(&:auto_suspension?)
    end

    def premium_support?
      subscription_feature_addons.any? { |a| PREMIUM_SUPPORT_TIER.include? a.name }
    end

    def sell_product?
      has_orca_classic_pravda_sell_product? && pravda_client.product(SELL_PRODUCT, use_cache: true).try(:free?)
    end

    def support_risky?
      return false if whitelisted?
      fraud_scores.any?(&:support_risky)
    end

    def talk_risky?
      return false if whitelisted?
      fraud_scores.any?(&:talk_risky)
    end

    def whitelisted?
      return @whitelisted if defined? @whitelisted
      @whitelisted = (settings.whitelisted_from_fraud_restrictions || !!subscription.try(:zuora_subscription).try(:assisted?))
    end

    def recently_created_account?
      created_at > 20.days.ago
    end

    def new_shell_account?
      created_at > 1.day.ago && fraud_scores.none? { |fs| fs.source_event.to_sym == Fraud::SourceEvent::SHELL_CREATED }
    end

    def trusted_bulk_uploader?
      settings.bulk_user_upload || outbound_email_safe?
    end

    def fraud_scores?
      !fraud_scores.empty?
    end

    def first_fraud_score
      fraud_scores.order(:created_at).first
    end

    def latest_fraud_score
      fraud_scores.order(:created_at).last
    end

    # Outbound E-mail (Hermes) helper methods
    def outbound_email_safe?
      settings.risk_assessment.with_indifferent_access[:level].in?(SAFE_RISK_LEVELS)
    end

    def outbound_email_risky?
      settings.risk_assessment.with_indifferent_access[:level].in?(UNSAFE_RISK_LEVELS)
    end

    def outbound_email_deliverable?
      !settings.risk_assessment.with_indifferent_access[:level].in?(UNDELIVERABLE_RISK_LEVELS)
    end

    def account_is_trial?
      # shell trial accounts do not have a subscription
      return true unless subscription.present?
      is_trial?
    end

    def free_mail?
      return true unless first_fraud_score
      first_fraud_score.free_mail?
    end

    def vendor_review_prohibited?
      subscription_feature_addons.any? { |addon| PROHIBITED_VENDOR_ADDON_NAMES.include?(addon.name) }
    end

    private

    # to prevent recursive loop with fraud score creations
    def validate_fraud_score_limit(_score)
      if fraud_scores.size >= FRAUD_SCORE_LIMIT
        Rails.logger.info "Account #{subdomain} reached the fraud score limit of #{FRAUD_SCORE_LIMIT}"
        errors.add(:fraud_scores, :invalid)
        raise 'Max number of fraud scores for account'
      end
    end

    def pravda_client
      @pravda_client ||= Zendesk::Accounts::Client.new(
        self,
        retry_options: {
          max: 3,
          interval: 2,
          exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
          methods: Faraday::Request::Retry::IDEMPOTENT_METHODS
        },
        timeout: 10
      )
    end

    def outside_of_thresholds?
      fraud_score = first_fraud_score
      maxmind_score_check(fraud_score)
    end

    def maxmind_score(fraud_score)
      fraud_score.try(:risk_score) || 0
    end

    def maxmind_score_check(fraud_score)
      maxmind_score(fraud_score) < MAXMIND_FRAUD_SCORE_THRESHOLD
    end

    def account_paid_invoice?
      return true unless has_orca_classic_disable_email_template_risky_account?
      outbound_email_safe?
    end
  end
end
