class Account
  module AssumptionSupport
    ALWAYS_ASSUMABLE = '22000101'.to_time

    def assumable?
      return true if assumable_account_type?
      return true unless has_account_assumption_control?
      return false unless settings.assumption_expiration.present?
      settings.assumption_expiration > Time.now
    end

    def paid_product?
      billing_id.present? || paid_sell_product?
    end

    def paid_sell_product?
      return false if sell_product.nil?
      return false unless sell_product.try(:plan_settings)
      sell_product.try(:plan_settings).try(:fetch, 'billing_source') == "migrated_sell_recurly"
    end

    # This makes a call to Pravda/account service
    def sell_product
      @sell_product ||= products.find { |product| product.name == :sell }
    end

    # if this method returns false, CCUA will be available on the account
    # if account is a sandbox, it should not be assumable account type.
    # if there is a billing_id that's means that there is at least one paid
    # product on the account, even if account is in trial for another product
    # sell product do not have billing_id like other products even if paid.
    def assumable_account_type?
      if multiproduct?
        # This is a workaround to figure out trialing accounts in the absence of subscription records.
        # ASSUMPTIONS:
        # 1. When the billing_id is nil the account only has trial products.
        # 2. sandbox accounts have nil billing_ids but they should not be assumable i.e they should have CCUA.
        # 3. When a product is bought in a multiproduct account, the billing id will not be nil.
        # 4. Except when it's a sell account, in which case we check account service plan billing.
        return false if is_sandbox?
        return false if paid_product?
        true
      else
        subscription.is_trial?
      end
    end

    def always_assumable?
      return false unless settings.assumption_expiration.present?
      settings.assumption_expiration == ALWAYS_ASSUMABLE
    end
  end
end
