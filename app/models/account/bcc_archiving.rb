class Account < ActiveRecord::Base
  module BccArchiving
    def self.included(base)
      base.property_set :settings do
        property :bcc_archive_address
      end

      base.validate :valid_bcc_archive_address, if: :bcc_archive_address?
    end

    def has_bcc_archiving_enabled? # rubocop:disable Naming/PredicateName
      !has_lockdown_enabled? && subscription.has_bcc_archiving? && settings.bcc_archive_address.present?
    end

    def valid_bcc_archive_address
      if sanitized = Zendesk::Mail::Address.sanitize(settings.bcc_archive_address)
        settings.bcc_archive_address = sanitized
      else
        errors.add(:bcc_archive_address, :invalid)
      end
    end

    protected

    # errors.add :bcc_archive_address fails without this
    def bcc_archive_address
      settings.bcc_archive_address
    end

    def bcc_archive_address?
      settings.bcc_archive_address?
    end
  end
end
