class Account < ActiveRecord::Base
  module CustomFields
    def self.included(base)
      base.class_eval do
        has_many :custom_fields, class_name: "CustomField::Field"
      end
    end

    # @param key [String] the custom field key to look up. For example, "age", "plan_type"
    # @return [CustomField::Field]
    def find_user_custom_field_by_key(key)
      key && user_custom_fields.detect { |f| f['key'] == key }
    end

    # @param source [String] the custom field key to look up, prefixed by "custom_fields.". For example, "custom_fields.age"
    # @return [CustomField::Field]
    def find_user_custom_field_by_source(source)
      key = UserView.custom_field_key(source)
      find_user_custom_field_by_key(key)
    end

    def user_custom_fields
      custom_fields_for_owner("User")
    end

    def custom_fields_for_owner(owner)
      @_custom_fields_cache ||= {}
      @_custom_fields_cache[owner] ||= custom_fields.active.for_owner(owner)
    end

    def system_user_custom_field_keys
      @system_user_custom_field_keys ||= custom_fields.for_system.for_user.map(&:key)
    end

    def field_taggers_cache
      @field_taggers_cache ||= FieldTagger.taggers_tag_to_name_lookup(self)
    end
  end
end
