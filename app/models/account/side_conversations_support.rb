class Account
  module SideConversationsSupport
    def self.included(base)
      base.class_eval do
        property_set :settings do
          property :ticket_threads, type: :boolean, default: false
        end

        before_save :ensure_side_conversation_recipient_address_exists, if: :ticket_threads_setting_changed_and_enabled?
      end
    end

    def ticket_threads_setting_changed_and_enabled?
      (settings.ticket_threads && settings.ticket_threads_record.value_changed?) ||
        (
          settings.side_conversations_email &&
          settings.side_conversations_email_record.value_changed?
        )
    end

    def ensure_side_conversation_recipient_address_exists
      recipient_addresses.where(product: 'collaboration').first_or_create do |ra|
        ra.email = "collaboration@#{subdomain_url}"
      end
    end

    def has_side_conversations_enabled? # rubocop:disable Naming/PredicateName
      has_side_conversations? &&
        (has_side_conversations_email? || has_side_conversations_slack? || has_side_conversations_tickets?)
    end
  end
end
