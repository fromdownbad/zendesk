require 'zendesk/internal_api/client'

class Account < ActiveRecord::Base
  class SandboxLimitError < StandardError; end
  class SandboxTypeError < StandardError; end
  class OperatingOnSandboxError < StandardError; end

  SANDBOX_LIMIT = 10

  belongs_to :sandbox_master, class_name: 'Account', foreign_key: :sandbox_master_id
  scope :exclude_sandbox, -> { where(sandbox_master_id: nil) }

  has_many :sandboxes, -> { where(is_active: true, is_serviceable: true) }, class_name: 'Account', foreign_key: :sandbox_master_id, dependent: :destroy

  def sandbox_without_nolocking
    if multiple_sandboxes?
      @current_sandbox
    else
      sandboxes.first
    end
  end

  def sandbox_with_nolocking
    Account.without_locking { sandbox_without_nolocking }
  end

  alias_method :sandbox, :sandbox_with_nolocking

  def assign_current_sandbox(subdomain)
    if multiple_sandboxes?
      @current_sandbox = sandboxes.find { |a| a.subdomain == subdomain }
    end
  end

  def is_sandbox_master? # rubocop:disable Naming/PredicateName
    !is_sandbox? && sandboxes.present?
  end

  def is_sandbox_master_of?(account_id) # rubocop:disable Naming/PredicateName
    return false unless id
    Account.find_by_id(account_id)&.sandbox_master_id == id
  end

  def is_sandbox? # rubocop:disable Naming/PredicateName
    !sandbox_master_id.blank?
  end

  def sandboxes_maxed?
    sandboxes.size >= SANDBOX_LIMIT
  end

  def multiple_sandboxes?
    @multiple_sandboxes ||= Arturo.feature_enabled_for?(:multiple_sandboxes, self) || Arturo.feature_enabled_for?(:admin_center_framework_view_sandbox, self)
  end

  def add_sandbox(sandbox_type:)
    raise(OperatingOnSandboxError, 'Cannot create sandbox for sandbox account') if is_sandbox?
    raise(SandboxTypeError, "'#{sandbox_type}' is not a valid type of sandbox") unless ::Accounts::Sandbox::SANDBOX_TYPES.include?(sandbox_type)
    raise(SandboxLimitError, 'Your sandbox limit has been reached') if sandboxes_maxed?
    spawn_sandbox(sandbox_type: sandbox_type)
  end

  def reset_sandbox
    return false if is_sandbox?

    with_sandbox_destroying(destroy_sandbox) do
      return false if is_sandbox? || (is_sandbox_master? && sandbox_not_destroyed?)

      spawn_sandbox
    end
  end

  def destroy_sandbox
    return unless sandbox
    # Use API call back to classic, because sandbox may be straggling on different pod
    client = ::Zendesk::InternalApi::Client.new(sandbox.subdomain)
    client.connection.delete("/api/v2/internal/monitor/account.json")
    true
  rescue Faraday::Error::ClientError, Zendesk::InternalApi::RecordInvalid
    message = "failed to destroy account -- #{inspect}"
    ZendeskExceptions::Logger.record(Exception.new, location: message, message: message, fingerprint: '9b35ca81f37ade54c43043b34fed92187810950e')
    false
  end

  private

  def spawn_sandbox(sandbox_type: ::Accounts::Sandbox::STANDARD)
    sandbox = becomes_new(Accounts::Sandbox)
    sandbox.settings.sandbox_creation_in_progress = true

    sandbox.sandbox_master_id  = id
    sandbox.created_at         = Time.now
    sandbox.subdomain         += Time.now.to_i.to_s
    sandbox.subdomain.delete!("-")
    sandbox.name              += " #{I18n.t("txt.admin.models.account.sandbox_name_suffix")}"
    sandbox.address            = spawn_address
    sandbox.is_ssl_enabled     = is_ssl_enabled?
    sandbox.host_mapping       = nil
    sandbox.is_serviceable     = true
    sandbox.is_active          = true
    sandbox.route = nil
    sandbox.multiproduct = false
    sandbox.billing_id   = nil
    Zendesk::SupportAccounts::Product.retrieve(sandbox).save!
    sandbox
  ensure
    # only execute this block if the sandbox saved
    if sandbox.persisted?
      sandbox.settings.sandbox_creation_in_progress = false
      sandbox.settings.sandbox_type = sandbox_type
      sandbox.settings.screencasts_for_tickets = 0
      sandbox.settings.save!
      sandbox.settings.reload
    end
  end

  def spawn_address
    return address.dup if address

    Address.new(name: 'Sandbox default', phone: '-')
  end

  def with_sandbox_destroying(result)
    @_sandbox_destroyed = result
    yield
  ensure
    @_sandbox_destroyed = nil
  end

  def sandbox_not_destroyed?
    !@_sandbox_destroyed
  end
end
