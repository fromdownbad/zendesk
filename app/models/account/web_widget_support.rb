class Account
  module WebWidgetSupport
    EKR_BASE_URL = ENV.fetch('EKR_BASE_URL', 'https://ekr.zdassets.com').freeze

    def brand_code_snippet(brand, domain)
      <<~HEREDOC
        <!-- Start of #{brand.subdomain} Zendesk Widget script -->
        #{snippet(brand, domain)}
        <!-- End of #{brand.subdomain} Zendesk Widget script -->
      HEREDOC
    end

    def connect_activation_snippet
      <<~HEREDOC
        <script>
          zE(function() {
            zE.identify({
              name: yourCurrentUser.name, // TODO: Replace with logged-in user's name
              email: yourCurrentUser.email // TODO: Replace with logged-in user's email address
            });
            zE.activateIpm();
          });
        </script>
      HEREDOC
    end

    private

    def snippet(brand, domain)
      ekr_snippet(brand, domain)
    end

    def ekr_snippet(brand, domain)
      fetch_snippet("#{EKR_BASE_URL}/snippets/web_widget/#{brand.subdomain}.#{domain}", dynamic_snippet: true)
    end

    def fetch_snippet(snippet_url, params = {})
      url = URI.parse(snippet_url)
      client = Faraday.new(url: "#{url.scheme}://#{url.host}") do |builder|
        builder.use :http_cache, store: Rails.cache, logger: Rails.logger
        builder.adapter Faraday.default_adapter
      end

      client.get(url.path, params).body
    end
  end
end
