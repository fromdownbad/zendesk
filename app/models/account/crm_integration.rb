require_dependency 'zendesk/salesforce/next_generation/integration'

class Account
  module CrmIntegration
    def self.included(base)
      base.has_one :salesforce_production, -> { where(is_sandbox: false) }, class_name: 'SalesforceIntegration', inverse_of: :account
      base.has_one :salesforce_sandbox, -> { where(is_sandbox: true) }, class_name: 'SalesforceIntegration', inverse_of: :account
      base.has_one :sugar_crm_integration
      base.has_one :ms_dynamics_integration
      base.has_many :salesforce_integrations
    end

    def crm_integration
      salesforce_integration || sugar_crm_integration || ms_dynamics_integration
    end

    def crm_integration_configured?
      crm_integration.try(:configured?)
    end

    def crm_integration_enabled?
      if crm_integration.is_a?(Salesforce::NextGeneration::Integration)
        crm_integration.data_cache_enabled?
      else
        crm_integration.try(:enabled?)
      end
    end

    def salesforce_configuration_enabled?
      salesforce_integration.try(:configured?)
    end

    def salesforce_integration
      if has_central_admin_salesforce_integration? && next_generation_salesforce_integration.configured_and_enabled?
        next_generation_salesforce_integration
      else
        classic_salesforce_integration
      end
    end

    def configured_salesforce_production
      salesforce_production.try(:configured?) ? salesforce_production : nil
    end

    def configured_salesforce_sandbox
      salesforce_sandbox.try(:configured?) ? salesforce_sandbox : nil
    end

    def has_multiple_salesforce_connections? # rubocop:disable Naming/PredicateName
      configured_salesforce_production.present? && configured_salesforce_sandbox.present?
    end

    def salesforce_integration_attributes=(attributes)
      salesforce_integration.update_attributes(attributes)
    end

    private

    def classic_salesforce_integration
      if has_multiple_salesforce_connections?
        settings.use_salesforce_production? ? salesforce_production : salesforce_sandbox
      else
        configured_salesforce_production || configured_salesforce_sandbox
      end
    end

    def next_generation_salesforce_integration
      @next_generation_salesforce_integration ||= Salesforce::NextGeneration::Integration.new(self)
    end
  end
end
