class Account < ActiveRecord::Base
  module PartnerSupport
    def via_partner?
      settings.partner_name.present? || settings.partner_url.present?
    end
    alias_method :reseller_account?, :via_partner?
  end
end
