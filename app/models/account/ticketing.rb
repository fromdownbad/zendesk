class Account
  module Ticketing
    # Ticket recovery limited to thirty days
    # https://zendesk.atlassian.net/wiki/display/PGM/Ticket+Trash+Can
    OLDEST_RECOVERABLE_TICKET_AGE = 30.days

    def self.included(base)
      base.class_eval do
        has_one :nice_id_sequence, dependent: :destroy, inverse_of: :account
        has_one :field_subject
        has_one :field_description
        has_one :field_status
        has_one :field_ticket_type
        has_one :field_priority
        has_one :field_group
        has_one :field_assignee

        has_many :rules, dependent: :destroy, as: :owner
        has_many :all_rules, class_name: 'Rule'
        has_many :all_macros, class_name: 'Macro', inverse_of: :account
        has_many :macros,     as: :owner

        has_many :all_automations, class_name: 'Automation', inverse_of: :account
        has_many :automations, -> { order(:position) }, as: :owner

        has_many :all_triggers, class_name: 'Trigger', inverse_of: :account
        has_many :triggers, -> { order(:position) }, as: :owner
        has_many :triggers_with_category_position, -> {
          joins(:rule_category).order('rules_categories.position', :position, :title)
        }, class_name: 'Trigger', as: :owner, inverse_of: :account
        has_many :active_triggers, -> { where(is_active: true).order(:position) }, class_name: 'Trigger', as: :owner
        has_many :active_triggers_with_category_position, -> {
          joins(:rule_category).where(is_active: true).order('rules_categories.position', :position, :title)
        }, class_name: 'Trigger', as: :owner, inverse_of: :account

        has_many :trigger_categories, -> { where(rule_type: 'Trigger') }, class_name: 'RuleCategory', inverse_of: :account

        has_many :all_views, class_name: 'View', inverse_of: :account
        has_many :views, -> { order('position, title ASC') }, as: :owner

        has_many :reports, -> { order(:title) }
        has_many :suspended_tickets, -> { order(created_at: :asc, id: :asc) }, dependent: :destroy, inverse_of: :account

        has_many :tickets, dependent: :destroy
        has_many :questions, -> { where(ticket_type_id: TicketType.QUESTION) }, class_name: 'Ticket'
        has_many :incidents, -> { where(ticket_type_id: TicketType.INCIDENT) }, class_name: 'Ticket'
        has_many :problems,  -> { where(ticket_type_id: TicketType.PROBLEM) }, class_name: 'Ticket'
        has_many :closed_tickets, -> { where(status_id: StatusType.CLOSED) }, class_name: 'Ticket'

        has_many :ticket_metric_sets, dependent: :destroy

        has_many :targets, dependent: :destroy
        has_many :custom_field_options

        has_many :ticket_fields, -> { order(:position) }, inverse_of: :account
        has_many :field_taggers, -> { order(:position) }, dependent: :destroy
        has_many :field_checkboxes, -> { order(:position) }
        has_many :field_dates, -> { order(:position) }

        has_many :active_taggers, -> { includes(:custom_field_options).where(is_active: true) }, class_name: 'FieldTagger'

        has_one :suspended_ticket_notification, dependent: :destroy

        has_many :ticket_archive_stubs, dependent: :destroy

        has_many :target_failures

        before_destroy :destroy_ticket_fields
      end
    end

    def ticket_fields(include_voice_insights: false)
      if include_voice_insights
        @with_voice_ticket_fields ||= super()
      else
        # Call `super()` with no args to prevent an eager load from a truthy reload parameter
        @without_voice_ticket_fields ||= Voice::InsightsField.filter_fields_for_account(super(), id)
      end
    end

    # Skips the trip to the database when the entire TicketField collection has
    # already been loaded in memory.
    def ticket_fields_by_ids(ids)
      return {} if ids.empty?

      # With _all records_ for the account.
      scope = ticket_fields(include_voice_insights: true)

      if scope.loaded?
        @ticket_fields_by_id ||= scope.to_a.map { |tf| [tf.id, tf] }.to_h
        @ticket_fields_by_id.slice(*ids)
      else
        Rails.logger.warn "Called Account#ticket_fields_by_ids without ticket_fields being pre-loaded. This won't be cached."
        # Idea: We could stash _any_ fetched TicketField into memoized Hash for
        # future use, if we get enough consumers of this mechanism.
        scope.where(id: ids).to_a.map { |tf| [tf.id, tf] }.to_h
      end
    end

    def tickets_solved_this_week
      tickets_solved_since(start_of_week.utc)
    end

    def tickets_solved_since(date)
      tickets.where('solved_at > ? AND generated_timestamp > ?', date, date).count(:all)
    end

    # _MUST_ be called within a `Ticket.with_deleted { .. }` block.
    def deleted_tickets
      since = if has_extend_recoverable_ticket_age?
        settings.extended_recoverable_ticket_age.days
      else
        OLDEST_RECOVERABLE_TICKET_AGE
      end
      tickets.deleted_and_not_scrubbed_since(since.ago)
    end

    def max_views_for_display
      12
    end

    def max_private_views_for_display
      8
    end

    def fetch_active_triggers
      cache_version = scoped_cache_key(:triggers)

      # Memoized for Automations and BulkTicket operations. Resets when CachingObserver expires cache.
      if @fetch_active_triggers && @fetch_active_triggers_cache_version == cache_version
        @fetch_active_triggers
      else
        @fetch_active_triggers_cache_version = cache_version
        @fetch_active_triggers = if has_skip_triggers_caching?
          # Don't attempt caching for accounts with trigger data too large to cache
          active_triggers_for_execution
        else
          Rails.cache.fetch("#{cache_version}-active-triggers", compress: true, expires_in: 5.minutes, race_condition_ttl: 20.seconds) do
            active_triggers_for_execution
          end
        end
      end
    end

    def fetch_active_non_update_triggers
      fetch_active_triggers.reject(&:change_conditions?)
    end

    def fetch_active_prediction_triggers
      fetch_active_triggers.select(&:satisfaction_prediction_conditions?)
    end

    def fetch_active_taggers
      skip_cache = has_active_taggers_skip_cache?
      active_taggers_statsd_client.time("fetch_active_taggers", tags: ["skip_cache:#{skip_cache}"]) do
        if skip_cache
          fetch_active_taggers_with_memoization
        else
          fetch_active_taggers_with_cache
        end
      end
    end

    def fetch_active_tagger(id)
      skip_cache = has_active_taggers_skip_cache?
      active_taggers_statsd_client.time("fetch_active_tagger", tags: ["skip_cache:#{skip_cache}"]) do
        if skip_cache
          fetch_active_tagger_with_memoization(id)
        else
          fetch_active_taggers_with_cache.detect { |tagger| tagger.id == id.to_i }
        end
      end
    end

    def fetch_custom_field_option_value(id)
      return unless id.respond_to?(:to_i) && id = id.to_i
      return unless id > 0

      @cached_custom_field_option_values ||=
        begin
          cache_key = "#{scoped_cache_key(:ticket_fields)}-value-lookup"
          Rails.cache.fetch(cache_key, expires_in: 1.hour) do
            # We can't use CustomFieldOption's value column during the
            # transition to enhanced_value, since the value can come from
            # either `value` or `enhanced_value` columns
            # custom_field_options.pluck(:id, :value).to_h
            custom_field_options.each_with_object({}) { |cfo, values| values[cfo.id] = cfo.value }
          end
        end

      @cached_custom_field_option_values[id]
    end

    def shared_views
      if subscription.has_group_rules?
        all_views.owned_by(Account, Group).order('position, title asc')
      else
        views
      end
    end

    def shared_macros
      order_by = if settings.macro_order == 'alphabetical'
        'title asc'
      else
        'position, title asc'
      end

      if subscription.has_group_rules?
        all_macros.owned_by(Account, Group).order(order_by)
      else
        macros.order(order_by)
      end
    end

    def fallback_ticket_form(brand)
      return nil unless default_form = ticket_forms.default.first

      # If the default ticket form is not available to the brand, return the brand's first active ticket form.
      # Otherwise we always want to return the default ticket form
      brand_forms = (brand && brand.ticket_forms.active) || []
      if brand_forms.any? && !brand_forms.include?(default_form)
        brand_forms.first
      else
        default_form
      end
    end

    private

    def active_taggers_statsd_client
      Zendesk::StatsD::Client.new(namespace: ['active_taggers'])
    end

    def fetch_active_taggers_with_cache
      # ensure classes for `Marshal.load` have been autoloaded
      FieldTagger # rubocop:disable Lint/Void
      FieldMultiselect # rubocop:disable Lint/Void
      CustomFieldOption # rubocop:disable Lint/Void

      cache_version = scoped_cache_key(:ticket_fields)

      # Memoized for Automations and BulkTicket operations. Resets when CachingObserver expires cache.
      if @fetch_active_taggers && @fetch_active_taggers_version == cache_version
        @fetch_active_taggers
      else
        @fetch_active_taggers_version = cache_version
        @fetch_active_taggers = Rails.cache.fetch("#{cache_version}-active-taggers") do
          active_taggers.reload.to_a
        end
      end
    end

    def fetch_active_taggers_with_memoization
      @fetch_active_taggers_memoize ||= active_taggers.reload.to_a
    end

    def fetch_active_tagger_with_memoization(id)
      @fetch_active_tagger ||= {}
      @fetch_active_tagger[id] ||= active_taggers.find_by(id: id)
    end

    def destroy_ticket_fields
      ticket_fields.each(&:force_destroy!)
    end

    def active_triggers_for_execution
      ordered_active_triggers = if has_trigger_categories_api_enabled? && trigger_categories.any?
        active_triggers_with_category_position.reload.to_a
      else
        active_triggers.reload.to_a # load the triggers, not just scope
      end

      ordered_active_triggers.each(&:definition) # force YAML deserialization
      ordered_active_triggers
    end
  end
end
