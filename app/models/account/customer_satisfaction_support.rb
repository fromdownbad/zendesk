class Account < ActiveRecord::Base
  module CustomerSatisfactionSupport
    DEFAULT_AUTOMATION_NAME = 'Request customer satisfaction rating (System Automation)'.freeze
    DEFAULT_VIEW_NAME       = 'Rated tickets solved in last 7 days'.freeze

    EMAIL_SUBJECT = 'Request #{{ticket.id}}: How would you rate the support you received?'.freeze

    EMAIL_BODY = <<~EMAIL.freeze
      Hello {{ticket.requester.name}},

      We'd love to hear what you think of our customer service. Please take a moment to answer one simple question by clicking either link below:

      {{satisfaction.rating_section}}

      Here's a reminder of what your ticket was about:

      {{ticket.comments_formatted}}
    EMAIL

    def self.included(base)
      base.class_eval do
        has_many :satisfaction_ratings,
          -> { order(created_at: :desc, id: :desc) },
          class_name: 'Satisfaction::Rating',
          foreign_key: :account_id

        has_many :satisfaction_reasons, class_name: 'Satisfaction::Reason'

        has_many :satisfaction_reason_brand_restrictions, class_name: 'Satisfaction::ReasonBrandRestriction'

        delegate :customer_satisfaction_privacy, :customer_satisfaction_privacy=,
          to: :account_property_set
      end
    end

    def csat_reason_code_enabled?
      has_customer_satisfaction_enabled? && has_csat_reason_code_enabled?
    end

    def customer_satisfaction_on?
      settings && settings.customer_satisfaction?
    end

    protected

    def activate_or_deactivate_satisfaction_automation_and_view
      should_create_automation_and_view = settings.where(name: 'customer_satisfaction').empty?

      yield

      if has_customer_satisfaction_enabled? && customer_satisfaction_on?
        if should_create_automation_and_view
          create_or_activate_customer_satisfaction_view
          create_or_activate_customer_satisfaction_automation
        end
      else
        toggle_rule_is_active default_view, false
        toggle_rule_is_active default_automation, false
      end
    end

    def create_or_activate_customer_satisfaction_automation
      if default_automation
        toggle_rule_is_active default_automation, true
      else
        @default_automation = automations.create!(
          title: I18n.t('txt.customer_satisfaction_rating.automation_v2.title'),
          definition: Definition.new.tap do |defn|
            defn.conditions_all << DefinitionItem.new('status_id', 'less_than', [StatusType.CLOSED])
            defn.conditions_all << DefinitionItem.new('SOLVED', 'is', ['24'])
            defn.conditions_all << DefinitionItem.new('satisfaction_score', 'is', [SatisfactionType.UNOFFERED])
            defn.conditions_all << DefinitionItem.new('ticket_is_public', 'is', ['public'])
            defn.actions << DefinitionItem.new(
              'notification_user', 'is',
              [
                'requester_id',
                # Note: The # sign in the subject string is mispositioned in RTL. Ideally, we would include the # sign
                # as part of the placeholder content, then wrap the whole placeholder in LTR control characters when
                # we call the translate method. E.g.:
                # I18n.t('txt.customer_satisfaction_rating.automation_v2.subject', { id: "\u202D" + "#" + id + "\u202C" })
                # However, I18n.t is not replacing the placeholders. It is being done later, when the automation is called,
                # by Liquid. This should be addressed when the larger issue of # signs in translations is handled/
                # See: https://zendesk.atlassian.net/browse/LOCAL-1889
                I18n.t('txt.customer_satisfaction_rating.automation_v2.subject'),
                I18n.t('txt.customer_satisfaction_rating.automation_v2.body')
              ]
            )
            defn.actions << DefinitionItem.new('satisfaction_score', 'is', [SatisfactionType.OFFERED])
          end
        )
      end
    end

    def create_or_activate_customer_satisfaction_view
      if default_view
        toggle_rule_is_active default_view, true
      else
        position = views.any? ? (views.order('position DESC').first.position + 1) : 1

        @default_view = views.create!(
          title: '{{zd.recently_rated_tickets}}',
          position: position,
          output: Output.create(
            group: :satisfaction_score,
            group_asc: false,
            order: :updated,
            order_asc: true,
            columns: [:status, :description, :requester, :created, :assignee, :satisfaction_score],
            type: "table"
          ),
          definition: Definition.new.tap do |defn|
            defn.conditions_all << DefinitionItem.new('status_id', 'greater_than', [StatusType.NEW])
            defn.conditions_all << DefinitionItem.new('SOLVED', 'less_than', ['169'])

            [SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT, SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT].each do |rating|
              defn.conditions_any << DefinitionItem.new('satisfaction_score', 'is', [rating])
            end
          end
        )
      end
    end

    def toggle_rule_is_active(rule, state)
      return if !rule || rule.is_active? == state
      rule.update_attributes(is_active: state)
    end

    def default_automation
      return @default_automation if defined?(@default_automation)
      @default_automation = automations.find_by_title(I18n.t('txt.customer_satisfaction_rating.automation_v2.title', locale: translation_locale))
    end

    def default_view
      return @default_view if defined?(@default_view)
      @default_view = views.find_by_title('{{zd.recently_rated_tickets}}')
    end
  end

  include CustomerSatisfactionSupport
  around_save :activate_or_deactivate_satisfaction_automation_and_view, if: :changed_customer_satisfaction, unless: :is_locked?
  attr_accessor :changed_customer_satisfaction
end
