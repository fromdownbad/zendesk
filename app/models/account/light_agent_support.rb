class Account < ActiveRecord::Base
  module LightAgentSupport
    def  self.included(base)
      base.has_one :light_agent_permission_set, -> { where(role_type: PermissionSet::Type::LIGHT_AGENT) }, class_name: "PermissionSet"
      base.accepts_nested_attributes_for :light_agent_permission_set, update_only: true
    end
  end
end
