class Account
  module SuiteSupport
    def has_active_suite? # rubocop:disable Naming/PredicateName
      return @has_active_suite if defined?(@has_active_suite)

      @has_active_suite = begin
        plan_settings = support_product.plan_settings.with_indifferent_access
        is_active     = support_product.active?
        is_suite      = plan_settings[:suite]

        is_active && is_suite
      rescue
        Rails.application.config.statsd.client.
          increment('suite_support.pravda.failed')
        false
      end
    end
  end
end
