class Account
  module AttachmentSupport
    def max_attachment_size
      if has_email_increase_attachment_size_limit?
        subscription.present? ? subscription.file_upload_cap.megabytes : Attachment.attachment_options[:max_size]
      else
        size_limits = [Attachment.attachment_options[:max_size]] # this is specified in config/attachments.yml
        size_limits << subscription.file_upload_cap.megabytes if subscription.present?
        size_limits.min
      end
    end

    def max_attachment_megabytes
      (max_attachment_size / 1.megabytes).to_i
    end
  end
end
