class Account
  module HubAndSpoke
    def self.included(base)
      base.class_eval do
        has_many :spoke_subscriptions, class_name: 'Subscription', foreign_key: :hub_account_id, source: 'hub_account'
        has_many :spoke_accounts, class_name: 'Account', through: :spoke_subscriptions, source: 'account'
      end
    end

    def hub?
      spoke_accounts.any?
    end

    def spoke?
      subscription.is_spoke?
    end

    def request_brands(current_user)
      ticket = {
        requester: {
          name: current_user.name,
          email: current_user.email
        },
        subject: "Multibrand migration request for #{name}",
        comment: {
          body: "A new request to learn about migration from hub and spoke to multibrand was made by #{name} by #{current_user.name} for #{subdomain}.zendesk.com"
        },
        tags: ["multibrand_migration_request"]
      }
      internal_api_client = Zendesk::Accounts::InternalApiClient.new("support")
      internal_api_client.create_hub_spoke_ticket(ticket)
      settings.has_requested_multibrand = true
      settings.save
    end
  end
end
