class Account::Capability
  def self.availabilities
    { arturo: Backend::Arturo,
      subscription: Backend::Subscription,
      require: Backend::Custom }
  end

  def self.configurations
    { setting: Backend::Setting }
  end

  attr_reader :name

  def initialize(name, options = {})
    @name    = name
    @options = options

    enable_by_default :arturo
  end

  def arturo
    find_availability(Backend::Arturo)
  end

  def subscription
    find_availability(Backend::Subscription)
  end
  alias_method :subscription?, :subscription

  def custom
    find_availability(Backend::Custom)
  end

  def setting
    find_configuration(Backend::Setting)
  end

  def public?
    @options.include? :public
  end

  def enabled?(account)
    configurations.all? { |configuration| configuration.enabled?(account) }
  end

  def available?(account)
    availabilities.all? { |availability|  availability.available?(account) }
  end

  # Checks if a feature (arturo, subscription, custom code block) is available.
  # If a corresponding setting exists, also checks whether that setting is enabled.
  def available_and_enabled?(account)
    available?(account) && enabled?(account)
  end

  def availabilities
    @availabilities ||= configure(self.class.availabilities)
  end

  def configurations
    @configurations ||= configure(self.class.configurations)
  end

  protected

  def find_availability(klass)
    availabilities.detect { |availability| availability.is_a?(klass) }
  end

  def find_configuration(klass)
    configurations.detect { |configuration| configuration.is_a?(klass) }
  end

  def configure(backends)
    configured = backends.map do |key, backend|
      if value = @options[key]
        name = (value == true) ? @name : value
        backend.new(name)
      end
    end

    configured.compact
  end

  def enable_by_default(key)
    @options[key] = true if @options[key].nil?
  end
end
