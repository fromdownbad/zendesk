require 'resque-retry'

class SetAgentWorkspaceAvailabilityJob
  extend ZendeskJob::Resque::BaseJob
  extend Resque::Plugins::ExponentialBackoff

  priority :high

  @backoff_strategy = [0, 60, 300] # 0, 1, 5 minutes

  def self.work(account_id, available)
    new(account_id: account_id, available: available).work
  end

  def self.args_to_log(account_id, available)
    { account_id: account_id, available: available }
  end

  def initialize(account_id:, available:)
    @account_id = account_id
    @available = available
  end

  def work
    return unless product(Zendesk::Accounts::Client::SUPPORT_PRODUCT.to_sym)
    Rails.logger.info("Updating agent workspace availability to #{available}")
    account.on_shard { account.agent_workspace_available = available }
    update_plan_settings
  end

  private

  attr_reader :account_id, :available

  def update_plan_settings
    Rails.logger.info("Updating plan settings")
    accounts_client.update_product!(Zendesk::Accounts::Client::SUPPORT_PRODUCT, {
      product: {
        plan_settings: {
          agent_workspace: available
        }
      }
    }, context: "SetAgentWorkspaceAvailabilityJob")
  end

  def product(name)
    account.products(use_cache: true).find { |product| product.name == name }
  end

  def accounts_client
    @accounts_client ||= Zendesk::Accounts::Client.new(account)
  end

  def account
    @account ||= Account.find(account_id)
  end
end
