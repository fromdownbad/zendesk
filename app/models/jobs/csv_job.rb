require 'zip/zip'
require 'zip/zipfilesystem'

class CsvJob < ThrottleJob
  attr_reader :account, :requester, :csv_file, :zipfile, :options, :job_name

  def self.work(account_id, requester_id, options = {})
    new(account_id, name, options).work(requester_id)
  end

  def self.identifier(*args)
    ["account_id", args[0], "requester_id", args[1]].join(":")
  end

  def self.latest(account)
    account.expirable_attachments.latest(name).first
  end

  def self.args_to_log(account_id, requester_id, options = {})
    { account_id: account_id, requester_id: requester_id, options: options }
  end

  def self.available_for?(_account)
    true
  end

  def self.enqueued_translated
    I18n.t('txt.admin.models.jobs.csv_job.enqueued')
  end

  def self.throttled_translated
    I18n.t('txt.admin.models.jobs.csv_job.throttled')
  end

  def self.latest_translated
    I18n.t('txt.admin.models.jobs.csv_job.latest')
  end

  def initialize(account_id, name, options)
    @account   = Account.find(account_id)
    @job_name  = name
    @options   = options
  end

  def work(requester_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      @requester = account.users.find(requester_id)

      Time.use_zone(account.time_zone) do
        I18n.with_locale(requester.translation_locale) do
          with_tempfiles do
            build_csv_file
            build_zipfile
            deliver_attachment
          end
        end
      end
    end
  end

  private

  def with_tempfiles
    @csv_file  = Tempfile.new([job_name, '.csv'])
    @zipfile   = Tempfile.new([job_name, '.zip'])

    yield
  ensure
    csv_file.close
    zipfile.close

    csv_file.unlink
    zipfile.unlink
  end

  def build_csv_file
    CSV.open(csv_file.path, 'a', col_sep: ",", force_quotes: true) do |csv|
      csv << csv_header
      csv_data.each { |row| csv << row }
    end
  end

  def build_zipfile
    Zip::OutputStream.open(zipfile.path) do |zos|
      zos.put_next_entry(file_name + '.csv')
      zos << IO.read(csv_file.path)
    end
  end

  def deliver_attachment
    attachment_url = expirable_attachment.url(token: true, mapped: false)

    JobsMailer.deliver_job_complete(
      requester,
      identifier,
      email_subject,
      email_message(attachment_url)
    )
  end

  def expirable_attachment
    attachment_name = "#{file_name}-csv.zip"
    cgi_file = Zendesk::Attachments::CgiFileWrapper.new(zipfile.path, filename: attachment_name)

    ExpirableAttachment.create(
      uploaded_data: cgi_file,
      account: account,
      author: requester,
      created_via: job_name
    )
  end

  def identifier
    "requester_id:#{requester.id}"
  end

  def email_subject
    I18n.t('txt.admin.models.jobs.csv_email_subject')
  end

  def email_message(url)
    content = I18n.t('txt.admin.models.jobs.csv_email_message_1')
    content << "\n\n#{url}\n\n"
    content << I18n.t('txt.admin.models.jobs.csv_email_message_2')
    content
  end

  def file_name
    raise NotImplementedError
  end

  def csv_header
    raise NotImplementedError
  end

  def csv_data
    raise NotImplementedError
  end
end
