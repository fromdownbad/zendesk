# This job is used to create artifical congestion for manually testing
# congestion prevention measures like CrowdControl and worker timeouts.
#
# You can absolutely backlog a queue and cause serious damage if you aren't
# prudent with how many of these jobs you enqueue.
class DangerousCongestionTestJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Zendesk::Jobs::CrowdControl # :low doesn't opt jobs into CrowdControl by default

  priority :low

  MAX_SLEEP_DURATION = 30.seconds

  def self.before_enqueue_warn(*_args)
    unless ENV['I_KNOW_THIS_IS_DANGEROUS'] == 'true'
      warn "Are you sure you've read and understand #{name}? I don't think you did."
      return false
    end
    true
  end

  # log_identifier is just for logging purposes
  def self.work(_account_id, sleep_duration, _log_identifier = nil)
    sleep_duration = MAX_SLEEP_DURATION if sleep_duration > MAX_SLEEP_DURATION

    sleep sleep_duration
  end

  def self.args_to_log(account_id, sleep_duration, log_identifier = nil)
    { account_id: account_id, sleep_duration: sleep_duration, log_identifier: log_identifier }
  end
end
