class AuditLogsExportJob < CsvJob
  include TimeDateHelper

  priority :medium

  throttle disabled: true # Controlled by Props at controller layer instead

  BATCH_SIZE = 1000

  def file_name
    "audit-logs-export-#{Time.now.strftime('%Y-%m-%dT%H:%M:%S')}"
  end

  def csv_header
    headers = []
    headers << I18n.t('txt.admin.views.reports.tabs.audits.column_created_at')
    headers << I18n.t('txt.admin.views.reports.tabs.audits.column_actor_id') if @account.has_audit_logs_export_include_name?
    headers << I18n.t('txt.admin.views.reports.tabs.audits.column_actor')
    headers << I18n.t('txt.admin.views.reports.tabs.audits.column_ip_address')
    headers << I18n.t('txt.admin.views.reports.tabs.audits.column_action')
    headers << I18n.t('txt.admin.views.reports.tabs.audits.column_source')
    headers << I18n.t('txt.admin.views.reports.tabs.audits.column_changes')
  end

  # Override the base class to allow us to read records in batches.
  def build_csv_file
    CSV.open(csv_file.path, 'a', col_sep: ",", force_quotes: true) do |csv|
      csv << csv_header

      cia_scope.find_in_batches(batch_size: BATCH_SIZE) do |batch|
        batch.each { |audit| csv << csv_row(audit) }
      end
    end
  end

  # needed for API presenter
  def api_v2_audit_log_url(*_args)
    nil
  end

  private

  def cia_scope
    CIA::Event.
      for_account(@account).
      where(Zendesk::AuditLogControllerSupport.conditions(@options.with_indifferent_access['filter'])).
      visible.
      includes(:attribute_changes, :actor)
  end

  def csv_row(audit)
    presenter = Api::V2::AuditLogPresenter.new(@requester, url_builder: self).present(audit)[:audit_log]
    row = []
    row << audit.created_at.utc.iso8601
    row << presenter[:actor_id]
    row << Zendesk::CsvScrubber.scrub_output(audit.actor&.name) if @account.has_audit_logs_export_include_name?
    row << Zendesk::CsvScrubber.scrub_output(presenter[:ip_address])
    row << Zendesk::CsvScrubber.scrub_output(presenter[:action])
    row << Zendesk::CsvScrubber.scrub_output(presenter[:source_label])
    row << Zendesk::CsvScrubber.scrub_output(presenter[:change_description].tr("\n", ','))
  end
end
