class GroupDeleteJob
  extend ZendeskJob::Resque::BaseJob

  priority :high

  class << self
    def work(account_id, requester_id, group_id)
      account = Account.find(account_id)
      ActiveRecord::Base.on_shard(account.shard_id) do
        requester = User.find(requester_id)

        remove_group_from_organizations(requester, group_id)
        unassign_not_closed_tickets(requester, group_id)
      end
    end

    def args_to_log(account_id, requester_id, group_id)
      { account_id: account_id, requester_id: requester_id, group_id: group_id }
    end

    private

    def remove_group_from_organizations(requester, group_id)
      requester.account.organizations.where(group_id: group_id).find_each do |organization|
        organization.update_attribute(:group_id, nil)
      end
    end

    def unassign_not_closed_tickets(requester, group_id)
      requester.account.tickets.not_closed.where(group_id: group_id).find_each do |ticket|
        ticket.will_be_saved_by(requester, via_id: ViaType.GROUP_DELETION)
        ticket.update_attributes(assignee_id: nil, group_id: nil)
      end
    end
  end
end
