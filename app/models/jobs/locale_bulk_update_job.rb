class LocaleBulkUpdateJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :immediate

  class << self
    def work(account_id, user_id, _audit = nil)
      unless account = Account.shard_unlocked.active.serviceable.where(id: account_id).first
        Rails.logger.error("Could not find account #{account_id}, skipping.")
        return
      end

      if account.has_short_circuit_locale_bulk_update_job?
        resque_log("Skipping LocaleBulkUpdateJob for account #{account_id} because the short_circuit_locale_bulk_update_job arturo is enabled")
        return
      end

      account.on_shard do
        unless user = account.users.where(id: user_id).first
          Rails.logger.error("Could not find user #{user_id} for account #{account_id}, skipping user.")
          return
        end

        update_ticket_locales(user)
      end
    rescue StandardError => e
      statsd_client.increment('errors', tags: ["exception_class:#{e.class.name.underscore}"])
      ZendeskExceptions::Logger.record(e, location: self, message: name, fingerprint: 'eef38d6c5f69c1ead43e7104298aafc54ea184cd')
    end

    private

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['resque', 'locale_bulk_update_job'])
    end

    def update_ticket_locales(user)
      user.tickets.find_in_batches(batch_size: Users::Localization::MAX_UPDATE_ALL_TICKETS) do |ticket_batch|
        ids = ticket_batch.map(&:id)
        resque_log("Setting locale to #{user.locale_id} for the following tickets #{ids.inspect}")
        user.tickets.where(id: ids).update_all(locale_id: user.locale_id)

        sleep 1 # Dont block the DB for too long, give it some time to do other things
      end
    end

    def args_to_log(account_id, user_id, audit = nil)
      { account_id: account_id, user_id: user_id, audit_id: audit_id(audit) }
    end
  end
end
