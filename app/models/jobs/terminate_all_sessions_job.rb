class TerminateAllSessionsJob
  extend ZendeskJob::Resque::BaseJob

  BATCH_SIZE = 500

  priority :immediate

  def self.work(account_id, delete_tokens)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      account.shared_sessions.delete_all

      if delete_tokens
        # delete password reset tokens
        account.password_reset_tokens.delete_all

        # delete verification tokens
        account.verification_tokens.delete_all

        # delete oauth tokens
        account.tokens.delete_all

        # delete mobile devices
        account.mobile_devices.find_in_batches(batch_size: BATCH_SIZE) { |b| b.each(&:destroy) }

        # delete mobile push notification tokens
        account.device_identifiers.find_in_batches(batch_size: BATCH_SIZE) { |b| b.each(&:destroy) }
      end
    end
  end

  def self.args_to_log(account_id, delete_tokens)
    { account_id: account_id, delete_tokens: delete_tokens }
  end
end
