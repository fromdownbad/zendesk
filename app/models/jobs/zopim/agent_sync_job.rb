module Zopim
  class AgentSyncJob
    extend ZendeskJob::Resque::BaseJob

    priority :high

    def self.work(*args)
      new(*args).work
    end

    def self.args_to_log(account_id, user_id, payload)
      { account_id: account_id, user_id: user_id, payload: payload }
    end

    def initialize(account_id, user_id, payload)
      @account_id = account_id
      @user_id    = user_id
      @payload    = payload
    end

    def work
      account.on_shard do
        begin
          update_agent!
        rescue => error
          log error
          raise Zopim::SynchronizationError, error.message
        end
      end
    end

    private

    def update_agent!
      return unless zopim_agent.present?
      id       = zopim_subscription.zopim_account_id
      agent_id = zopim_agent.zopim_agent_id
      Reseller.client.update_account_agent!(id: id, agent_id: agent_id, data: @payload)
    end

    def log(error)
      payload = String((error.errors.inspect if error.respond_to?(:errors)))
      message = format_message <<-TEXT
        Zopim agent sync job for Zendesk agent #{zendesk_agent.id}
        failed: [#{error.class}] #{error.message} #{payload}
      TEXT
      ZendeskExceptions::Logger.record(Exception.new, location: self, message: message, fingerprint: '0bd93c3b65be6aae4b878d0afcd16be7edc2eb41')
    end

    def account
      @account ||= Account.find(@account_id)
    end

    def subscription
      account.subscription
    end

    def zopim_subscription
      account.zopim_subscription
    end

    def zendesk_agent
      @zendesk_agent ||= User.find(@user_id)
    end

    def zopim_agent
      zendesk_agent.zopim_identity
    end

    def format_message(message)
      message.gsub(/  /, '').tr("\n", ' ')
    end
  end
end
