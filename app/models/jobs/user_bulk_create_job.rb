# requires these options to be passed:
#   account_id
#   user_id
class UserBulkCreateJob < BulkJob
  priority :medium

  def users
    @users ||= options[:users]
  end

  def name
    "UserBulkCreateJob account: #{options[:account_id]}"
  end

  private

  def work
    total = users.size
    job_span = ZendeskAPM.trace(JOB_SPAN_NAME, service: APM_SERVICE_NAME, resource: self.class.name, tags: common_span_tags.merge!('zendesk.total_records' => total))

    User.reserve_global_uids(total)

    Rails.logger.info("Running UserBulkCreateJob, nusers: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Creating User')

    results = iterate("Creating User") do |id, user|
      started_at = current_time
      result = create_user(id, user)
      record_duration_metric((current_time - started_at) * 1000, result)
      result.except(:action)
    end

    completed(results: results)
  ensure
    job_span.finish
  end

  def iterate(say)
    total = users.size
    at(0, total, say + 's')
    users.each_with_index.map do |user, index|
      at(index + 1, total, "#{say} ##{index + 1}")
      yield index, user
    end
  end

  def create_user(id, user)
    record_span = ZendeskAPM.trace(RECORD_SPAN_NAME, service: APM_SERVICE_NAME, tags: common_span_tags)

    if user[:organization_id]
      return job_error(record_span, id, 'OrganizationNotFound') unless user[:organization_id].in?(organization_map)
    end

    new_user = if upserting?
      current_account.update_existing_or_initialize_new_user(current_user, user: user)
    else
      current_account.users.new
    end

    Zendesk::Users::Initializer.new(current_account, current_user, user: user).apply(new_user) unless upserting?
    action = new_user.new_record? ? :create : :edit
    status = action == :create ? "Create" : "Update"

    record_span.resource = "User##{action}"

    new_user.active_brand_id ||= request_brand_id if new_user.new_record?

    if current_user.can?(action, new_user)
      if Zendesk::SupportUsers::User.new(new_user).save
        result = {id: new_user.id, status: status + 'd'}

        result[:email] = new_user.email if new_user.email
        result[:external_id] = new_user.external_id if new_user.external_id

        result[:action] = action
        register_span_tags(record_span, result_span_tags(result))
        result
      else
        job_error(record_span, id, "User#{status}Error", "#{user[:name]}: #{new_user.errors.full_messages.join(',')}", action)
      end
    else
      job_error(record_span, id, "PermissionDenied", "Insufficient permissions to #{action} user #{user[:name]}", action)
    end
  ensure
    record_span.finish
  end

  def job_error(span, id, error, details = nil, action = nil)
    error_result =
      if current_account.has_bulk_job_errors_attribute?
        result = { id: id, errors: error, status: "Failed" }
        result[:details] = details if details
        result
      else
        fail_job(id, error, details).merge(status: "Failed")
      end

    error_result[:action] = action if action
    register_span_tags(span, error_span_tags(error_result))
    error_result
  end

  def upserting?
    @is_upserting ||= options[:create_or_update]
  end

  def request_brand_id
    @request_brand_id ||= options[:request_brand_id]
  end

  def organization_map
    @organization_map ||= resources_map(:organizations, :id, users.map { |u| u[:organization_id] })
  end
end
