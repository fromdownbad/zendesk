class AccountsAfterConversionJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :high

  def self.work(account_id, _audit_id = nil)
    account = Accounts::Base.on_master do
      Accounts::Base.find(account_id)
    end
    target_locale = account.translation_locale.name.downcase

    account.on_shard do
      account.run_after_convertion_callbacks
    end

    statsd_client.increment('count', tags: ['result:success', "language:#{target_locale}"])
  rescue => e
    error_message = "Failed to convert pre-created account with id #{account_id}"
    ZendeskExceptions::Logger.record(e, location: self, message: error_message, fingerprint: 'd2f217938d6ba3dc818b0650cced110db163757b')

    # no locale tags here since account can not be find
    statsd_client.increment('count', tags: ['result:failure'])
    raise e
  end

  def self.args_to_log(account_id, _audit = nil)
    { account_id: account_id }
  end

  def self.statsd_client
    @client ||= Zendesk::StatsD.client(namespace: name.snakecase)
  end
end
