require 'zendesk/crm/zendesk_users'

# requires these options to be passed:
#   account_id
#   user_id

class CrmZendeskUserBulkCreateJob < BulkJob
  priority :medium

  def users
    @users ||= options[:users]
  end

  def name
    "CrmZendeskUserBulkCreateJob account: #{options[:account_id]}"
  end

  private

  def create_or_update_user(user_options, id)
    org_id = user_options[:organization_id]

    # fail early if the job is trying to set an invalid org on a given user
    return fail_job(id, "OrganizationNotFound", "Organization id:#{org_id} not found") if org_id && !organizations_map.include?(org_id)

    user, is_new_user, = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(current_account, current_user, user_options || {})
    action = is_new_user ? :create : :edit

    return fail_job(id, "Insufficient Permissions", "Insufficient permissions to #{action} user #{user_options[:name]}") unless current_user.can?(action, user)

    user.active_brand_id ||= request_brand_id if is_new_user

    if user.save
      result = {id: user.id, status: is_new_user ? "Created" : "Updated"}

      result[:email] = user.email if user.email
      result[:external_id] = user.external_id if user.external_id

      result
    else
      {status: "Failed #{is_new_user ? 'Creating' : 'Updating'} User #{user_options[:name]}", errors: user.errors.full_messages.join(',')}
    end
  end

  def work
    total = users.size
    User.reserve_global_uids(total)

    Rails.logger.info("Running CrmZendeskUserBulkCreateJob, nusers: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Creating/Updating Users')

    results = users.each_with_index.map do |u, idx|
      at(idx + 1, total, "Creating/updating user ##{idx + 1}")
      create_or_update_user(u, idx)
    end

    completed(results: results)
  end

  def organization_ids
    @organization_ids ||= options[:users].map { |u| u[:organization_id] }.uniq.compact
  end

  def organizations_map
    return [] unless organization_ids
    @organizations_map ||= resources_map(:organizations, :id, organization_ids)
  end

  def request_brand_id
    @request_brand_id ||= options[:request_brand_id]
  end
end
