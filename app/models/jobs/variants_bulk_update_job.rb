# requires these options to be passed:
#   account_id
#   user_id
class VariantsBulkUpdateJob < VariantJob
  priority :immediate

  def name
    "VariantsBulkUpdateJob account: #{options[:account_id]}"
  end

  private

  def work
    total = variants.size
    at(0, total, 'Updating Variants')

    results = variants.each_with_index.map do |variant, index|
      at(index + 1, total, 'Updating Variant ##{index + 1}')

      begin
        variant_to_update = initializer.variant(variant[:id])
        if variant_to_update.update_attributes(initializer.normalized_parameters(variant))
          DC::Synchronizer.synchronize_snippet(initializer.item)
          {id: variant_to_update.id, status: "Updated"}
        else
          {status: "Failed updating variant #{variant[:id]}", errors: variant_to_update.errors.full_messages.join(',')}
        end
      rescue ActiveRecord::RecordNotFound
        {status: "Failed updating variant #{variant[:id]}", errors: "Coudn't find variant with #{variant[:id]}"}
      end
    end

    completed(results: results)
  end
end
