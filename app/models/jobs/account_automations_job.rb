class AccountAutomationsJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::WorkerKiller
  extend Durable

  priority :automations

  # The extra 30 minute offset is to reduce concurrency of the same rule between
  # a job being retried, and a new job being kicked off at the top of the hour.
  self.job_timeout = if ENV.key?('AUTOMATIONS_JOB_TIMEOUT_MINUTES')
    Integer(ENV['AUTOMATIONS_JOB_TIMEOUT_MINUTES']).minutes
  else
    2.hours + 30.minutes
  end

  # Post-rollout, this can be directly assigned like job_timeout
  def self.background_heartbeat_interval
    if Arturo.feature_enabled_for_pod?(:automations_background_heartbeat, Zendesk::Configuration.fetch(:pod_id))
      1.minute
    end
  end

  # Maximum amount of time to wait for a AccountAutomationParallelExecutionJob
  # worker to complete. If exceeded, we start processing the next automation
  # while worker either continues or flaps in the background.
  PARALLEL_JOB_TIMEOUT = 30.minutes

  class << self
    def work(account_id, time_for_conditions, dry_run, audit_id)
      # Capture this at startup to almost elminate race when Arturo changes
      background_heartbeat = background_heartbeat_interval.present?

      account = Account.find(account_id)
      account.on_shard do
        Time.use_zone(account.time_zone) do
          # Resque serializes Time instances as Strings
          time_for_conditions = Time.parse(time_for_conditions) if time_for_conditions.is_a?(String)

          clear_cache = false
          executed = []

          account.automations.active.each do |automation|
            report_invalid_automation(automation)

            catch_and_report_errors "Failed to execute automation ##{automation.id}, account #{account.subdomain}" do
              unless dry_run
                automation_result = execute_automation(account, automation, time_for_conditions)
                resque_log([automation_result.to_s, automation_result.to_h])
                clear_cache ||= automation_result.changed > 0
              end
              executed << automation.id
            end

            # Reset `timeout_at` between automation executions.
            # The job may be double enqueued if an individual automation
            # execution takes longer than `job_timeout`.
            heartbeat([audit_id]) unless background_heartbeat
          end

          catch_and_report_errors "Failed to finalize automation for account #{account.subdomain}" do
            resque_log("Automations #{executed.join(', ')} executed for account #{account.id}") if executed.any?
            account.expire_scoped_cache_key(:views) if clear_cache
          end
        end
      end
    end

    def args_to_log(account_id, time_for_conditions, dry_run, audit = nil)
      {
        account_id:          account_id,
        time_for_conditions: time_for_conditions,
        dry_run:             dry_run,
        audit_id:            audit_id(audit)
      }
    end

    private

    def catch_and_report_errors(message)
      yield
    rescue StandardError => e
      message = "#{message}: #{e.message}"
      resque_log(message)
      ZendeskExceptions::Logger.record(e, location: self, message: message, fingerprint: '54c3eb7d265c668f3c8146c24867af1ee15e9061')
    end

    def execute_automation(account, automation, time_for_conditions)
      automation_result = executor(account, automation, time_for_conditions).execute

      if automation_result.queued_audit_ids.any?
        automation_result.apply_time = Benchmark.realtime do
          poll_until_audits_completed(automation_result.queued_audit_ids)
        end
      end

      automation_result
    end

    def executor(account, automation, time_for_conditions)
      if account.has_parallel_automations?
        ticket_finder = Zendesk::Rules::AutomationDehydratedTicketFinder.new(
          automation: automation,
          find_time: time_for_conditions,
          limit: account.settings.parallel_automations_limit
        )
        ticket_processor = Zendesk::Rules::AutomationParallelAsyncTicketProcessor.new(automation: automation)
      elsif account.has_automations_find_tickets_on_slave?
        ticket_finder    = Zendesk::Rules::AutomationDehydratedTicketFinder.new(automation: automation, find_time: time_for_conditions)
        ticket_processor = Zendesk::Rules::AutomationDehydratedTicketProcessor.new(automation: automation)
      else
        ticket_finder    = Zendesk::Rules::AutomationTicketFinder.new(automation: automation, find_time: time_for_conditions)
        ticket_processor = Zendesk::Rules::AutomationTicketProcessor.new(automation: automation)
      end

      Zendesk::Rules::AutomationExecutor.new(automation: automation, ticket_finder: ticket_finder, ticket_processor: ticket_processor)
    end

    # INFR-3240: report how many automations are invalid, so we can check
    # the possibility of not running them anymore.
    def report_invalid_automation(aut)
      resque_log("Invalid automation: #{aut.inspect}") unless aut.valid?
      tags = ["valid:#{aut.valid?}"]
      automation_stasd_client.increment('automations.executed', tags: tags)
    end

    def automation_stasd_client
      @automation_stasd_client ||= Zendesk::StatsD::Client.new(namespace: 'rules')
    end

    # This will consume a worker process until finished, which will not scale.
    # @todo replace with re-enqueue this job, passing in remaining automation_ids and pending audit_id list
    def poll_until_audits_completed(audit_ids, poll_delay: 1.second)
      ActiveRecord::Base.uncached do
        loop_until_done(poll_delay, PARALLEL_JOB_TIMEOUT) do
          audit_ids.reject! { |id| audit([id]).complete? } == []
        end
      end
    end

    def loop_until_done(time_inc, time_max)
      sleepy_time = 0
      while sleepy_time < time_max
        sleepy_time += time_inc
        sleep(time_inc)
        return if yield
      end
      raise Timeout::Error
    end
  end
end
