# requires these options to be passed:
#   account_id
#   organization_id
class OrganizationBulkDeleteJob < BulkDeleteJob
  priority :medium

  def name
    "OrganizationBulkDeleteJob account: #{options[:account_id]}"
  end

  private

  def work
    total = options[:ids].size
    Rails.logger.info("Running OrganizationBulkDeleteJob, norganizations: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Deleting Organization')

    results = options[:ids].each_with_index.flat_map do |organization_id, index|
      at(index + 1, total, "Deleting Organization} ##{index + 1}")

      organization = organizations_map[organization_id]
      delete_resource(organization_id, organization, "Organization")
    end

    completed(results: results)
  end

  def organizations_map
    @organizations_map ||= resources_map(:organizations, options[:key], options[:ids])
  end
end
