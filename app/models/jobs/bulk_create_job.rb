class BulkCreateJob < BulkJob
  def completed(*messages)
    if all_failed?(messages)
      set_status({
        'status' => 'failed',
        'message' => "Failed at #{Time.now}"
      }, *messages)
    else
      super
    end
  end

  def all_failed?(messages)
    messages.first[:results].none? { |result| result[:id] }
  end

  def fail_job(index, error, details = nil)
    result = { index: index, error: error }
    result[:details] = details if details
    result
  end

  def get_ids(attributes, key)
    attributes.map { |attribute| attribute[key] }.uniq
  end
end
