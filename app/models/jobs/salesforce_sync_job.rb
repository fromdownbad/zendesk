class SalesforceSyncJob
  extend ZendeskJob::Resque::BaseJob

  priority :external

  def self.work(account_id, user_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      return unless user = account.users.find_by_id(user_id)
      fetch_info(user)
    end
  end

  def self.fetch_info(user)
    data = user.account.salesforce_integration.fetch_info_for(user)
    user.salesforce_data.sync!(data)
  rescue Salesforce::Integration::LoginFailed => e
    record_error(e, :login_failed, user)
  rescue Salesforce::NextGeneration::RefreshTokenError => e
    record_error(e, :refresh_token_error, user)
  rescue SOAP::FaultError => e
    record_error(e, :soap_fault, user)
  rescue HTTPClient::ConnectTimeoutError => e
    record_error(e, :connect_timeout, user)
  rescue HTTPClient::ReceiveTimeoutError => e
    record_error(e, :receive_timeout, user)
  rescue Resolv::ResolvError => e
    record_error(e, :sf_subdomain_resolv_failed, user)
  rescue SocketError => e
    raise unless e.message.include? "getaddrinfo: Name or service not known"
    record_error(e, :sf_subdomain_resolv_failed, user)
  rescue Exception => e # rubocop:disable Lint/RescueException
    record_error(e, :error, user)
    raise
  end

  def self.record_error(e, type, user)
    user.salesforce_data.sync_errored!(records: [], error: type)
    resque_warn("SALESFORCE INTEGRATION #{type} (account: #{user.account.id}, user: #{user.id}): " + e.message)
  end

  def self.args_to_log(account_id, user_id)
    { account_id: account_id, user_id: user_id }
  end
end
