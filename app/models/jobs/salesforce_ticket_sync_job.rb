class SalesforceTicketSyncJob
  extend ZendeskJob::Resque::BaseJob

  priority :external

  def self.work(account_id, ticket_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      begin
        ticket = account.tickets.find_by_nice_id(ticket_id)

        return if ticket.nil?

        data = account.salesforce_integration.fetch_ticket_info(ticket)
        ticket.salesforce_ticket_data.sync!(data)
      rescue Exception => e # rubocop:disable Lint/RescueException
        ticket.salesforce_ticket_data.sync_errored!(records: [])

        case e
        when Salesforce::Integration::LoginFailed
          log_error("SALESFORCE INTEGRATION LOGIN ERROR", account, ticket, e)
        when Faraday::Error::ConnectionFailed
          log_error("SALESFORCE INTEGRATION CONNECTION ERROR", account, ticket, e)
        when Faraday::Error::MissingDependency
          log_error("SALESFORCE INTEGRATION MISSING DEPENDENCY ERROR", account, ticket, e)
        when Faraday::Error::ParsingError
          log_error("SALESFORCE INTEGRATION PARSING ERROR", account, ticket, e)
        when Faraday::Error::ResourceNotFound
          log_error("SALESFORCE INTEGRATION RESOURCE NOT FOUND ERROR", account, ticket, e)
        when Faraday::Error::TimeoutError, Errno::ETIMEDOUT
          log_error("SALESFORCE INTEGRATION TIMEOUT ERROR", account, ticket, e)
        when Faraday::Error::ClientError
          log_error("SALESFORCE INTEGRATION UNKNOWN CLIENT ERROR", account, ticket, e)
        when Salesforce::Integration::RuntimeError
          log_error("SALESFORCE INTEGRATION RUNTIME ERROR", account, ticket, e)
        when Salesforce::NextGeneration::RefreshTokenError
          log_error("SALESFORCE INTEGRATION REFRESH TOKEN ERROR", account, ticket, e)
        else
          handle_other_errors(account, ticket, e)
        end
      end
    end
  end

  def self.log_error(message, account, ticket, exception)
    details = exception.respond_to?('response') && !exception.response.blank? ? exception.response : exception.message
    resque_warn("#{message} (account: #{account.id}, ticket: #{ticket.id}): #{details}")
  end

  def self.args_to_log(account_id, ticket_id)
    { account_id: account_id, ticket_id: ticket_id }
  end

  def self.handle_other_errors(account, ticket, e)
    case e.message
    when "TotalRequests Limit exceeded"
      log_error("SALESFORCE INTEGRATION TotalRequests Limit exceeded", account, ticket, e)
    when "The REST API is not enabled for this Organization"
      log_error("SALESFORCE INTEGRATION The REST API is not enabled for this Organization", account, ticket, e)
    else
      raise e
    end
  end
end
