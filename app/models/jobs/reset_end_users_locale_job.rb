class ResetEndUsersLocaleJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium
  BATCH_SIZE = 500

  class << self
    # Will set the end users locale to that of the account
    def work(account_id)
      unless account = Account.shard_unlocked.active.serviceable.where(id: account_id).first
        Rails.logger.error("Could not find account #{account_id}, skipping.")
        return
      end

      ActiveRecord::Base.on_shard(account.shard_id) do
        update_end_users_locale_to_match_account(account)
      end
    end

    def args_to_log(account_id)
      { account_id: account_id }
    end

    private

    def update_end_users_locale_to_match_account(account)
      available_locale_ids = account.available_languages.map(&:id)

      users = account.users.end_users.
        where('locale_id IS NOT NULL').
        where('locale_id NOT IN (?)', available_locale_ids)

      resque_log("Clearing locale for #{users.count(:all)} end users in account #{account.subdomain}")

      users.find_each do |user|
        user.locale_id = nil
        user.save
      end
    end
  end
end
