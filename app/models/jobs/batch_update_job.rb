class BatchUpdateJob < BulkJob
  UPDATED = 'Updated'.freeze

  def batch_success(options = {})
    {action: :update, status: UPDATED, success: true}.merge(options)
  end

  def fail_job(update, index, error, details = nil)
    result = { index: index, error: error }
    if update[:id]
      result[:id] = update[:id]
    elsif update[:external_id]
      result[:external_id] = update[:external_id]
    end
    result[:details] = details if details
    result
  end

  def get_ids(resource)
    @ids = []
    @external_ids = []
    @organization_ids = []

    options[resource].each do |update|
      if update[:id]
        @ids << update[:id]
      elsif update[:external_id]
        @external_ids << update[:external_id]
      end

      if update[:organization_id]
        @organization_ids << update[:organization_id]
      end
    end
  end
end
