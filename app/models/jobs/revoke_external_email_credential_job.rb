require 'uri'
require 'zendesk/encryptable'

class RevokeExternalEmailCredentialJob
  extend ZendeskJob::Resque::BaseJob
  RevokeExternalEmailCredentialError = Class.new(StandardError)
  ENDPOINT = 'https://accounts.google.com/o/oauth2/revoke'.freeze

  priority :low

  def self.work(encrypted_credential, encryption_key_name, encryption_cipher_name)
    new(encrypted_credential, encryption_key_name, encryption_cipher_name).work
  end

  def self.args_to_log(encrypted_credential, encryption_key_name, encryption_cipher_name)
    {
      encrypted_credential: encrypted_credential,
      encryption_key_name: encryption_key_name,
      encryption_cipher_name: encryption_cipher_name
    }
  end

  def initialize(encrypted_credential, encryption_key_name, encryption_cipher_name)
    @encrypted_credential = encrypted_credential
    @encryption_key_name = encryption_key_name
    @encryption_cipher_name = encryption_cipher_name
  end

  def work
    uri = URI(ENDPOINT)
    params = { token: crypt.decrypt(@encrypted_credential) }
    uri.query = URI.encode_www_form(params)
    response = Net::HTTP.get_response(uri)
    body = JSON.parse(response.body)

    if response.is_a?(Net::HTTPSuccess)
      Rails.logger.info("[RevokeExternalEmailCredentialJob] Successfully revoked token.")
      return true
    end

    if body['error_description'] == 'Token expired or revoked'
      Rails.logger.info("[RevokeExternalEmailCredentialJob] Token already revoked.")
      return true
    end

    if body['error'] == 'invalid_token'
      Rails.logger.info("[RevokeExternalEmailCredentialJob] Token already invalidated.")
      return true
    end

    raise RevokeExternalEmailCredentialError, "[RevokeExternalEmailCredentialJob] Failed. #{body['error']} - #{body['error_description']}"
  end

  private

  def crypt
    @crypt ||= Zendesk::MessageEncryptor.new(
      Base64.decode64(ENV[@encryption_key_name]),
      cipher: @encryption_cipher_name
    )
  end
end
