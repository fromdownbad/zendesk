# requires these options to be passed:
#   account_id
#   user_id
class VariantJob < JobWithStatus
  private

  def initializer
    @initializer ||= Zendesk::DynamicContent::Initializer.new(current_account, options)
  end

  def variants
    @variants ||= options[:variants]
  end
end
