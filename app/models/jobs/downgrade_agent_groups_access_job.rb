class DowngradeAgentGroupsAccessJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium
  BATCH_SIZE = 500

  def self.work(account_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      downgrade_agent_groups_access(account)
    end
  end

  def self.args_to_log(account_id)
    { account_id: account_id }
  end

  def self.downgrade_agent_groups_access(account)
    resque_log("#{name}: Downgrading groups access on agents for #{account.subdomain}")
    account.
      agents.
      where.not(restriction_id: nil).
      where(restriction_id: RoleRestrictionType.GROUPS).
      select(:id).
      find_in_batches(batch_size: BATCH_SIZE) do |agents|
        account.agents.where(id: agents.map(&:id)).update_all(restriction_id: RoleRestrictionType.ASSIGNED)
      end
    resque_log("#{name}: Done downgrading groups access on agents for #{account.subdomain}")
  end

  private_class_method :downgrade_agent_groups_access
end
