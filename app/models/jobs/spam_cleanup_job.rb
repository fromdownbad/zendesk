class SpamCleanupJob
  extend ZendeskJob::Resque::BaseJob
  include Fraud::FraudTicketHelper
  include Fraud::FraudServiceHelper

  priority :low

  SPAM_CLEANUP = 'spam_cleanup'.freeze

  ACTION_LIMIT = ENV.fetch('SPAM_CLEANUP_ACTION_LIMIT', 50_000).to_i

  CLEANUP_TICKET_INDEX = 'index_tickets_on_account_id_and_status_id_and_created_at'.freeze

  def self.work(account_id, action, tag, days, spammy_strings)
    new(account_id, action, tag, days, spammy_strings).work
  end

  def initialize(account_id, action, tag, days, spammy_strings)
    @current_account = Account.find(account_id)
    @account_id      = account_id
    @action          = action
    @tag             = tag
    @days            = days
    @spammy_strings  = spammy_strings
  end

  def work
    @current_account.on_shard { generate_spam_cleanup }
  rescue StandardError => e
    message = "Failed to run spam_cleanup_job: #{e.message}"
    Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
    raise
  end

  def self.args_to_log(account_id, action, tag, days, spammy_strings)
    {
      account_id: account_id,
      action:     action,
      tag:        tag,
      days:       days,
      spammy_strings: spammy_strings
    }
  end

  private

  def generate_spam_cleanup
    Rails.logger.info("SpamCleanupJob Started: Account: #{@account_id}")
    action_count = 0
    affected_tickets = get_anonymous_tickets_in_range(@days)

    affected_tickets.each do |ticket|
      if action_count >= ACTION_LIMIT
        Rails.logger.info("SpamCleanupJob: reached action limit of #{ACTION_LIMIT} for Account #{@account_id}")
        break
      end

      next if ticket.status?(:deleted)
      next unless Fraud::SpamDetector.new(ticket).suspend_ticket? || ticket_has_spam_cleanup_job_patterns?(ticket, @spammy_strings)

      if actions_enabled?(@action)
        action_count += 1
        report_ticket_to_rspamd(User.system, ticket)
      end

      ticket.will_be_saved_by(User.system)
      add_tag_to_ticket(@tag, ticket) unless ticket.status?(:closed)
      delete_fraud_ticket_data(@tag, ticket) if should_delete_ticket?
    end

    Rails.logger.info("SpamCleanupJob Finished: Account: #{@account_id}, tag: #{@tag}, action: #{@action}, count: #{action_count}, actions_enabled: #{actions_enabled?(@action)}")
    if action_count > 0 && should_create_spam_cleanup_email?
      afs_response = create_z1_ticket_with_fraud_service(z1_ticket_payload(action_count))
      Rails.logger.info("SpamCleanupJob: creating_z1_ticket_response : #{afs_response}")
    end
  end

  def ticket_has_spam_cleanup_job_patterns?(ticket, spam_cleanup_job_patterns)
    return unless spam_cleanup_job_patterns && @current_account.has_orca_classic_spam_cleanup_job_spammy_strings?
    Fraud::SpamDetector::TICKET_TEXT_FIELDS.each do |ticket_field|
      text = send(ticket_field, ticket)
      if sanitize_spam_cleanup_job_patterns(spam_cleanup_job_patterns).detect { |spammy_string| text.include? spammy_string }
        return true
      end
    end
    false
  end

  def sanitize_spam_cleanup_job_patterns(spam_cleanup_job_patterns)
    spam_cleanup_job_patterns.delete(' ').split(",")
  end

  def should_create_spam_cleanup_email?
    @current_account.has_orca_classic_create_spam_cleanup_z1_ticket? && actions_enabled?(@action)
  end

  def z1_ticket_payload(action_count)
    {
      data: {
        account_id: @account_id,
        subdomain: @current_account.subdomain,
        ticket_action: @action,
        tag_name: @tag,
        action_count: action_count,
        z1_ticket_type: SPAM_CLEANUP
    },
      timestamp: Time.now
    }
  end

  def should_delete_ticket?
    @action.casecmp?(DELETE)
  end

  def get_anonymous_tickets_in_range(ticket_time_days)
    Ticket.
      use_index(CLEANUP_TICKET_INDEX).
      where(account_id: @account_id).
      where('created_at >= ?', ticket_time_days.days.ago).
      includes(:submitter)
  end
end
