require 'zip/zip'
require 'zip/zipfilesystem'

class XmlExportJob < ThrottleJob
  priority :medium

  throttle can_run_every: 24.hours

  def self.work(account_id, requester_id)
    work_with_class_and_name(account_id, requester_id, Zendesk::Export::Xml::XmlZipExporter, "XML")
  end

  def self.enqueued_translated
    I18n.t('txt.admin.models.jobs.user_xml_export_job.enqueued_message')
  end

  def self.throttled_translated
    I18n.t('txt.admin.models.jobs.user_xml_export_job.throttled_message')
  end

  def self.latest_translated
    I18n.t('txt.admin.models.jobs.user_xml_export_job.latest_message')
  end

  def self.work_with_class_and_name(account_id, requester_id, klass, name)
    account = Account.find(account_id)
    output  = nil
    stamp   = resque_session

    account.on_shard do
      unless available_for?(account) && accessible_for?(account)
        resque_log("#{self.name}: Aborting #{name} export for #{account.subdomain} [#{stamp}]: functionality not available.")
        return
      end
      user = User.find(requester_id)
      email_locale = user.locale_id.present? ? user.translation_locale : user.account.translation_locale

      Time.zone = account.time_zone
      resque_log("#{self.name}: Performing #{name} export for #{account.subdomain} [#{stamp}]")
      output = klass.build_for_account(account, stamp)
      attachment = create_expirable_attachment(output, account, user, name)
      JobsMailer.deliver_job_complete(user, identifier(account_id, requester_id), subject(email_locale), message(attachment.url(token: true, mapped: false), email_locale))
      resque_log("#{self.name}: Finished #{name} export for #{account.subdomain} [#{stamp}]")
    end
  rescue StandardError => e
    resque_log("#{self.name}: Failed #{name} export for #{account.subdomain} [#{stamp}]: #{e.message} -- #{e.backtrace.join("\n")}")
  ensure
    output.unlink rescue nil
  end

  def self.args_to_log(account_id, requester_id)
    { requester_id: requester_id, account_id: account_id }
  end

  def self.identifier(*args)
    "account_id:#{args[0]}"
  end

  def self.available_for?(account)
    account.subscription.has_xml_export?
  end

  def self.latest(account)
    account.expirable_attachments.latest(name).first
  end

  def self.create_expirable_attachment(output, account, user, name)
    filename   = PermalinkFu.escape("#{name} export #{Time.now.in_time_zone.to_s(:csv)} xml") + '.zip'
    attachment = ExpirableAttachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(output, filename: filename))
    attachment.account = account
    attachment.author = user
    attachment.created_via = self.name
    attachment.save!
    attachment
  end
  private_class_method :create_expirable_attachment

  def self.subject(email_locale)
    I18n.with_locale(email_locale) do
      I18n.t('txt.admin.models.jobs.report_feed_job.subject')
    end
  end
  private_class_method :subject

  def self.message(url, email_locale)
    I18n.with_locale(email_locale) do
      content = "#{I18n.t('txt.email.models.jobs.xml_export_job.your_export_has_been_generated')}\n\n"
      content << "#{url}\n\n"
      content << I18n.t('txt.admin.models.jobs.report_feed_job.message_2')
      content
    end
  end
  private_class_method :message

  def self.failure_message(stamp, email_locale)
    I18n.with_locale(email_locale) do
      I18n.t('txt.email.models.jobs.xml_export_job.generation_failure', identifier: stamp)
    end
  end
  private_class_method :failure_message
end
