class UserXmlExportJob < XmlExportJob
  priority :medium

  throttle can_run_every: 24.hours

  def self.work(account_id, requester_id)
    work_with_class_and_name(account_id, requester_id, Zendesk::Export::Xml::UserXmlZipExporter, "User XML")
  end

  def self.available_for?(account)
    account.subscription.has_user_xml_export?
  end

  def self.enqueued_translated
    I18n.t('txt.admin.models.jobs.user_xml_export_job.enqueued_message')
  end

  def self.throttled_translated
    I18n.t('txt.admin.models.jobs.user_xml_export_job.throttled_message')
  end

  def self.latest_translated
    I18n.t('txt.admin.models.jobs.user_xml_export_job.latest_message')
  end
end
