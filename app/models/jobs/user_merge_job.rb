require 'zendesk/radar_factory'

class UserMergeJob
  extend ZendeskJob::Resque::BaseJob
  # TODO: maybe this should be a higher priority?
  #      should a logged in user have to wait for their tickets from another login?
  priority :medium

  def self.work(account_id, winner_id, loser_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      begin
        merge(winner_id, loser_id, account)
      ensure
        Users::Merge.unlock_users(winner_id, loser_id)
      end
    end
  end

  def self.args_to_log(account_id, winner_id, loser_id)
    { account_id: account_id, winner_id: winner_id, loser_id: loser_id }
  end

  def self.merge(winner_id, loser_id, account)
    winner = User.find_by_id(winner_id)
    loser  = User.find_by_id(loser_id)

    if winner && loser && winner != loser
      resque_log("Merging users: winner:#{winner_id} -- loser:#{loser_id}")
      user_merge = Users::Merge.new(winner: winner, loser: loser)
      if user_merge.merge!
        update_radar_status(true, loser_id, winner_id, account)
      else
        update_radar_status(false, loser_id, winner_id, account)
      end
    else
      update_radar_status(false, loser_id, winner_id, account)
    end
  end

  def self.update_radar_status(value, loser_id, winner_id, account)
    notification = ::RadarFactory.create_radar_notification(account, "user_merge/#{account.id}/#{winner_id}/#{loser_id}")
    notification.send("response", value)
  end
end
