# requires these options to be passed:
#    account_id
#    admin_id
#    dry_run
#    onboarding_completed
require 'zip/zip'
require 'zip/zipfilesystem'

class CcsAndFollowers::UpdateRequesterTargetRulesJob < JobWithStatus
  priority :medium
  enforce_in_flight_limit

  # Amazon SES enforces a maximum number of 50 recipients per message.
  # "A recipient is any "To", "CC", or "BCC" address."
  # https://docs.aws.amazon.com/ses/latest/DeveloperGuide/limits.html
  MAILARCHIVE_BCC_COUNT = 1;
  NOTIFICATION_RECIPIENT_BATCH_SIZE = 50 - MAILARCHIVE_BCC_COUNT
  DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S %z'.freeze

  private

  def work
    Rails.logger.info("Generating CCs and Followers rules compatibility diff for account #{account_id}.")
    account = Account.find(account_id)
    admin = account.admins.find(admin_id)

    rules_compatibility = Zendesk::CcsAndFollowers::RulesCompatibility.new(account.id)
    rules_diff = rules_compatibility.update_requester_target(dry_run: dry_run?)
    affected_rules = build_list(account, rules_diff)
    admin_locale_groups = admins_grouped_by_translation_locale(account)
    attachment_url = if affected_rules.present?
      Time.use_zone(account.time_zone) do
        store_rules_list(affected_rules, admin_locale_groups.keys, "affected_rules_list", account, admin, dry_run?).url(token: true, mapped: false)
      end
    end
    no_rule_changes = affected_rules.blank?

    if onboarding_completed?
      notify_admins(attachment_url, admin, account, no_rule_changes, admin_locale_groups.values)
      set_ccs_followers_rules_update_required(account, rules_update_required?(affected_rules))
    end

    statsd_client.increment(:enqueued, tags: ["dry_run:#{dry_run?}", "onboarding_completed:#{onboarding_completed?}"])

    completed(results: {url: attachment_url})
  rescue ActiveRecord::RecordInvalid, ::Technoweenie::AttachmentFu::NoBackendsAvailableException => e
    error_message = if affected_rules
      "Unable to create attachment. " \
      "account_id: #{account_id}, admin_id: #{admin_id}, dry_run: #{dry_run?}, " \
      "affected_rules: #{affected_rules.squish.truncate(1000)}, error: #{e.class.name}"
    else
      "Failed to update or diff rules. " \
      "account_id: #{account_id}, admin_id: #{admin_id}, dry_run: #{dry_run?}, error: #{e.class.name}"
    end
    self.class.resque_error(error_message)

    raise e
  end

  def rules_update_required?(affected_rules)
    # If `dry_run?` is `false`, this means the admin has opted to have affected rules automatically updated.
    # In this case, the `ccs_followers_rules_update_required` account setting should be set to `false`
    # as all affected rules will have been updated by the Zendesk::CcsAndFollowers::RulesCompatibility job.
    return false unless dry_run?

    affected_rules.present?
  end

  def build_list(account, rules_diff)
    rules_diff.reduce("") do |string, diff_item|
      string << "#{diff_item.title.first.try(:content)}, " \
                "#{rule_url(account.url, diff_item)}" \
                "\n"
    end
  end

  def affected_rules_list_entry(output_stream, zip_entry_options)
    Zip::Entry.new(output_stream, 'affected_rules_list.txt', *zip_entry_options)
  end

  def instructions_entry(output_stream, locale, zip_entry_options)
    Zip::Entry.new(output_stream, "instructions-#{locale}.txt", *zip_entry_options)
  end

  # See https://github.com/rubyzip/rubyzip/blob/0.9.9/lib/zip/zip_entry.rb#L61-L64 for options
  def zip_entry_options(current_time)
    [
      '',                      # comment
      '',                      # extra
      0,                       # compressed_size
      0,                       # crc
      Zip::Entry::DEFLATED,    # compression_method
      0,                       # size
      current_time             # time
    ]
  end

  def store_rules_list(affected_rules, translation_locales, file_name, account, admin, include_instructions)
    zip_file = Tempfile.new([file_name, '.zip'])
    current_time = Zip::DOSTime.parse(Time.current.strftime(DATETIME_FORMAT))
    options = zip_entry_options(current_time)

    Zip::OutputStream.open(zip_file.path) do |output_stream|
      output_stream.put_next_entry(affected_rules_list_entry(output_stream, options))
      output_stream.puts(affected_rules)

      if include_instructions
        translation_locales.each do |translation_locale|
          instructions = I18n.t(
            'txt.admin.models.jobs.update_requester_target_rules_job.email.instructions_v6',
            locale: translation_locale,
            link: I18n.t(
              'txt.admin.models.jobs.update_requester_target_rules_job.email.instructions_link',
              locale: translation_locale
            )
          )
          output_stream.put_next_entry(instructions_entry(output_stream, translation_locale.locale, options))
          output_stream.puts instructions
        end
      end
    end

    create_attachment(account, admin, zip_file.path, "#{file_name}.zip")
  ensure
    zip_file.close
    zip_file.unlink
  end

  def create_attachment(account, admin, file, file_name)
    cgi_file = Zendesk::Attachments::CgiFileWrapper.new(file, filename: file_name)

    ExpirableAttachment.new(uploaded_data: cgi_file).tap do |attachment|
      attachment.account = account
      attachment.author = admin
      attachment.created_via = self.class.name
      attachment.save!
    end
  end

  def notify_admins(attachment_url, admin, account, no_rule_changes, admin_locale_groups)
    admin_locale_groups.each do |admin_locale_group|
      admin_locale_group.each_slice(NOTIFICATION_RECIPIENT_BATCH_SIZE) do |admin_batch|
        translation_locale = admin_batch.first.translation_locale

        JobsMailer.deliver_job_complete_to_multiple_recipients(
          admin_batch,
          "account_id:#{account.id}:admin_id:#{admin_batch.first.id}",
          email_subject(account.name, translation_locale),
          if no_rule_changes
            email_message_no_changes(admin.name, translation_locale)
          else
            email_message_with_instructions(
              admin.name,
              attachment_url,
              translation_locale,
              if dry_run?
                I18n.t(
                  'txt.admin.models.jobs.update_requester_target_rules_job.email.instructions_v6',
                  locale: translation_locale,
                  link: I18n.t(
                    'txt.admin.models.jobs.update_requester_target_rules_job.email.instructions_link',
                    locale: translation_locale
                  )
                )
              end
            )
          end
        )
      end
    end
  end

  def set_ccs_followers_rules_update_required(account, rules_update_required)
    account.settings.ccs_followers_rules_update_required = rules_update_required
    account.settings.save!
    account.settings.reload
    record = account.settings.where(name: :ccs_followers_rules_update_required).first
    record.touch_without_callbacks if record.present?
  end

  def admins_grouped_by_translation_locale(account)
    account.admins.
      includes(:identities).
      select(&:email).
      sort_by(&:translation_locale).
      group_by(&:translation_locale)
  end

  def rule_url(host, diff_item)
    "#{host}/agent/admin/" \
    "#{diff_item.send(:source).type.downcase}s/" \
    "#{diff_item.source_id}" \
  end

  def email_subject(account_name, translation_locale)
    I18n.t('txt.admin.models.jobs.update_requester_target_rules_job.email.subject',
      account_name: account_name,
      locale: translation_locale)
  end

  def email_message_with_instructions(admin_name, url, translation_locale, instructions)
    message = I18n.t('txt.admin.models.jobs.update_requester_target_rules_job.email.body',
      admin_name: admin_name,
      url: url,
      locale: translation_locale)

    instructions ? message + "\n\n" + instructions : message
  end

  def email_message_no_changes(admin_name, translation_locale)
    I18n.t('txt.admin.models.jobs.update_requester_target_rules_job.email.body_no_changes_v2',
      admin_name: admin_name,
      locale: translation_locale)
  end

  def account_id
    options[:account_id]
  end

  def admin_id
    options[:admin_id]
  end

  def dry_run?
    options[:dry_run]
  end

  def onboarding_completed?
    options[:onboarding_completed]
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[ccs_and_followers update_requester_target_rules_job])
  end
end
