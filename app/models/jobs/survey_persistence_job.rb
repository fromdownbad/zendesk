class SurveyPersistenceJob
  extend ZendeskJob::Resque::BaseJob

  priority :low

  ELOQUA_FORM_NAME = 'Product_Survey'.freeze
  ELOQUA_SITE_ID = '2136619493'.freeze
  ELOQUA_FORM_HOSTNAME = "s#{ELOQUA_SITE_ID}.t.eloqua.com".freeze
  ELOQUA_FORM_URI = "https://#{ELOQUA_FORM_HOSTNAME}/e/f2".freeze
  ELOQUA_FORM_CONFIRMATION_URI = "https://#{ELOQUA_FORM_HOSTNAME}/e/DefaultFormSubmitConfirmation.aspx".freeze

  QUESTION_CHAR_LIMIT = 250
  ANSWER_CHAR_LIMIT = 4000

  class << self
    def work(options)
      options = options.with_indifferent_access

      redis_key = options.fetch(:redis_key)

      payload_str = Zendesk::RedisStore.client.get(redis_key)

      begin
        payload = JSON.parse(payload_str).with_indifferent_access
      rescue
        Rails.logger.error("Redis payload #{payload_str} under key #{redis_key} could not be parsed.")
        return # prevent retry
      end

      eloqua_payload = build_eloqua_payload(payload)

      response = Faraday.post(ELOQUA_FORM_URI, eloqua_payload)

      unless eloqua_form_response_success?(response)
        raise "Could not save survey answer to eloqua for redis key #{redis_key}"
      end
    end

    def args_to_log(options)
      options
    end

    private

    def build_eloqua_payload(payload)
      question_names = payload.fetch(:question_names)
      all_answers = payload.fetch(:answer)

      result = question_names.map.with_index(1).inject({}) do |eloqua_payload, (question_name, i)|
        eloqua_payload.merge(
          "Question_#{format('%02d', i)}" => question_name.truncate(QUESTION_CHAR_LIMIT),
          "Answer_#{format('%02d', i)}" => all_answers.values_at(question_name).join(',').truncate(ANSWER_CHAR_LIMIT)
        )
      end

      if payload.key?(:training_offer_save)
        result["Question_09"] = "Training_Offer_Save"
        result["Answer_09"] = payload.fetch(:training_offer_save).to_s
      end

      if payload.key?(:canceled)
        result["Question_10"] = "Canceled"
        result["Answer_10"] = payload.fetch(:canceled).to_s
      end

      result.merge(
        elqformName: ELOQUA_FORM_NAME,
        elqSiteID: ELOQUA_SITE_ID,
        Form_Name: payload.fetch(:type),
        Responder_Email_Address: payload.fetch(:user_email),
        accountID: payload.fetch(:account_id),
        Subdomain: payload.fetch(:subdomain)
      )
    end

    # Eloqua always returns 200. If the Eloqua body is "\r\n" or a link to the confirmation page then the request was
    # successful.
    def eloqua_form_response_success?(response)
      response.status == 200 && (response.body.strip.empty? || response.body.include?(ELOQUA_FORM_CONFIRMATION_URI))
    end
  end
end
