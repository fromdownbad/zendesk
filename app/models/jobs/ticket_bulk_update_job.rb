# requires these options to be passed:
#   account_id
#   user_id
#   via_id
class TicketBulkUpdateJob < JobWithStatus
  include Zendesk::Tickets::CommentUpdate

  LIMIT = 20
  RETRY_ATTEMPTS = 3
  RETRY_EXCEPTIONS = [ZendeskDatabaseSupport::MappedDatabaseExceptions::MappedDatabaseException].freeze

  priority :medium
  enforce_in_flight_limit

  def name
    "TicketBulkUpdateJob account:#{options[:account_id]} user:#{options[:user_id]}"
  end

  private

  def work
    at(0, ticket_ids.size, 'Looking up tickets')
    total = ticket_ids.size
    i = 0
    results = []

    ticket_ids.each_slice(LIMIT) do |ticket_ids|
      tickets = tickets(ticket_ids)
      while ticket = tickets.pop # tickets get huge after save -> clear them from memory
        i += 1
        at(i, total, "Updating ticket ##{ticket.nice_id}")

        status = if options[:submit_type] == 'delete'
          'Deleted'
        elsif options[:mark_as_spam]
          'Spam'
        else
          'Updated'
        end

        results << begin
          current_attempt = 0

          Zendesk::Retrier.retry_on_error(RETRY_EXCEPTIONS, RETRY_ATTEMPTS) do
            current_attempt += 1

            # Refetching the ticket here helps ensure there isn't any left over
            # state on the ticket object from a previous attempt.
            ticket = refetch_ticket(ticket, current_attempt)

            if ticket.is_a?(TicketArchiveStub) && options[:submit_type] != 'delete'
              result = { success: false, errors: I18n.t('activerecord.errors.messages.closed_prevents'), status: 'failed' }
            else
              result = delete_or_update_ticket(ticket)
              if Arturo.feature_enabled_for?(:validate_event_integrity_after_ticket_bulk_update, current_account)
                # We want to count bad "child" events for this ticket.  To do so, we'll find the "parent" IDs of children, then
                # we'll find which of those parents is bad, then we'll find the count of bad child events for
                # those bad parents.
                parent_ids = Event.where(account_id: ticket.account_id, ticket_id: ticket.id).where('parent_id IS NOT NULL').pluck(:parent_id)
                bad_parent_ids = Event.where(id: parent_ids).where(account_id: 0).pluck(:id)
                bad_child_count = Event.where(parent_id: bad_parent_ids).where(account_id: 0).count

                if bad_child_count > 0
                  # NOTE: Some of the bad children might have previous existed- we didn't necessarily make them.
                  error_msg = "z1 3687276 #{bad_child_count} bad event(s) detected while running TicketBulkUpdateJob for ticket #{ticket.id}, try #{current_attempt}"
                  ZendeskExceptions::Logger.record(
                    StandardError.new(error_msg),
                    location: self,
                    message: error_msg,
                    reraise: false,
                    fingerprint: 'c3af9b504cc24a2d97baf8f157f178bb',
                    metadata: {
                      account_id: ticket.account_id,
                      ticket_id: ticket.id
                    }
                  )
                end
              end
            end

            result
          end
        rescue *RETRY_EXCEPTIONS => e
          Rails.logger.info("Ticket bulk update job failed after #{RETRY_ATTEMPTS} tries on account id #{current_account.id}, ticket id #{ticket.id} -- #{e.message}")
          {success: false, errors: I18n.t("txt.error_message.job_status.database_error"), status: status}
        rescue StandardError => e
          Rails.logger.info("Ticket bulk update job failed after #{RETRY_ATTEMPTS} tries on account id #{current_account.id}, ticket id #{ticket.id} -- #{e.message}")
          {success: false, errors: I18n.t("txt.error_message.job_status.unknown_error"), status: status}
        end
      end
    end

    completed(results: results)
  end

  def refetch_ticket(ticket, current_attempt)
    return ticket unless current_attempt > 1

    Ticket.where(id: ticket.id, account_id: ticket.account_id).first!
  end

  def delete_or_update_ticket(ticket)
    ticket.macro_ids = options[:macro_ids].map(&:to_i) if options[:macro_ids].present?

    I18n.with_locale(current_user.translation_locale) do
      if options[:submit_type] == 'delete'
        delete_ticket(ticket)
      elsif options[:mark_as_spam]
        mark_as_spam(ticket)
      else
        update_ticket(ticket)
      end
    end
  end

  def ticket_ids
    (options[:ticket_ids] || []).map(&:to_i).uniq
  end

  def tickets(ids)
    scope = current_account.tickets.for_user(current_user)
    if options[:submit_type] == 'delete'
      scope.where(nice_id: ids).all_with_archived
    else
      scope = scope.includes(self.class.ticket_includes).where(nice_id: ids)
      if current_account.has_include_archived_on_bulk_updates?
        scope.all_with_archived
      else
        scope.to_a
      end
    end
  end

  # make it testable
  public_class_method def self.ticket_includes
    {
      ticket_metric_set: {account: {subscription: {}, feature_boost: {}}},
      ticket_field_entries: {},
      requester: {settings: {}},
      account: {feature_boost: {}, settings: {}, subscription: {}},
      organization: {},
      assignee: {},
      brand: {}
    }
  end

  def update_ticket(ticket)
    params = options.dup
    params[:id] = ticket.nice_id
    params[:ticket_object] = ticket

    if params[:ticket]
      params[:ticket].delete(:subject) if params[:ticket][:subject].blank?
      params[:ticket].reject! { |_key, value| value == '-1' } # Remove no-change properties
      if params[:ticket][:fields]
        params[:ticket][:fields].reject! { |_key, value| value == NO_CHANGE } # Remove no-change custom field
      end

      # ZD289731: set_tags should override existing tags and not add tags
      if params[:macro_ids].present?
        params[:macro_ids].each do |macro_id|
          macro = Macro.find_for_user(current_user, macro_id.to_i)
          macro.actions.each do |action|
            params[:ticket].update(set_tags: "") if action.source == 'set_tags'
          end
        end
      end
    end

    params[:ticket] ||= {}
    params[:ticket][:request] = params.delete(:request)

    if params[:macro_ids].present?
      params[:ticket][:macro_ids] = params[:macro_ids].map(&:to_i)
    end

    # The following addresses Rails not decoding JSON from jQuery correctly,
    # specifically an array becoming a hash with integer keys
    # https://stackoverflow.com/q/6410810/1183568
    followers = params.dig(:ticket, :followers)
    params[:ticket][:followers] = followers.values if followers.respond_to?(:keys)
    initializer = Zendesk::Tickets::Initializer.new(current_account, current_user, params)

    previous_followers = ticket.followers.map(&:id).sort

    ticket = initializer.ticket

    current_followers = ticket.followers.map(&:id).sort

    # This needs to run only for accounts with > 1 group as we are checking if group has changed or not
    if current_account.groups.count(:all) > 1
      # Enforce proper assignee-group relationship when the group is changed and assignee isn't changed
      if options[:ticket][:group_id] != '-1' && options[:ticket][:assignee_id] == '-1'
        ticket.assignee = nil if ticket.assignee && !ticket.assignee.group_ids.include?(options[:ticket][:group_id].to_i)
      end
    end

    result = { id: ticket.nice_id, title: ticket.title, action: :update, status: 'Updated' }

    return result.merge(success: false, errors: I18n.t('txt.error_message.models.jobs.ticket_bulk_update_job.no_changes')) unless ticket.delta_changes? || ticket.comment || (current_followers != previous_followers)
    # tried to change the ticket tags, but not on whitelist
    return result.merge(success: false, errors: I18n.t('txt.error_message.models.jobs.ticket_bulk_update_job.access_denied_edit')) if ticket.current_tags_delta_changed? && !current_user.can?(:edit_whitelisted_tags, ticket)

    ticket.safe_update_timestamp ||= Time.at(options['enqueued_at']) if current_account.has_safe_update_bulk_ticket_jobs? && options['enqueued_at']

    ticket.audit.disable_triggers = true if options[:disable_triggers]

    if current_user.can?(:edit, ticket) || can_bulk_update_comments(ticket)
      success = ticket.save

      result.merge(success: success, errors: ticket.errors.full_messages.join(', '))
    else
      result.merge(success: false, errors: I18n.t('txt.error_message.models.jobs.ticket_bulk_update_job.access_denied_edit'))
    end
  end

  def delete_ticket(ticket)
    result = { id: ticket.nice_id, title: ticket.title, status: 'Deleted', action: :delete }

    if current_user.can?(:delete, ticket)
      ticket.will_be_saved_by(current_user, via_id: options[:via_id])
      success = ticket.soft_delete!
      Rails.logger.warn("#{Time.now}: #{current_account.subdomain}/#{current_account.id} ticket deletion by #{current_user.name} ##{ticket.nice_id}/#{ticket.id} [BULK]") unless options[:quiet]
      result.merge(success: success, errors: ticket.errors.full_messages.join(', '))
    else
      result.merge(success: false, errors: I18n.t('txt.error_message.models.jobs.ticket_bulk_update_job.access_denied_delete'))
    end
  end

  def mark_as_spam(ticket)
    result = { id: ticket.nice_id, title: ticket.title, status: 'Spam', action: :update, success: true }

    if current_user.can?(:mark_as_spam, ticket)
      begin
        ticket.mark_as_spam_by!(current_user)
        Rails.logger.warn("#{Time.now}: #{current_account.subdomain}/#{current_account.id} ticket marked as spam by #{current_user.name} ##{ticket.nice_id}/#{ticket.id} [BULK]") unless options[:quiet]
      rescue StandardError => e
        ZendeskExceptions::Logger.record(e, location: self, message: "Unhandled exception marking ticket as spam: #{e.message}", fingerprint: 'd2dfcd38739219b2fe352594b8f256de85c8f81e')
        result.merge!(success: false, errors: e.message)
      end
    else
      result[:success] = false
      result[:errors] = I18n.t('txt.error_message.models.jobs.ticket_bulk_update_job.shared_user_suspension')
    end

    result
  end
end
