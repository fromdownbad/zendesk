class RestoreDefaultContentJob
  extend ZendeskJob::Resque::BaseJob

  class UndefinedCompareMethodError < StandardError; end

  TRANSLATABLE_CONTENT = %w[
    roles views user_views macros triggers automations reports ticket_fields
    ticket_forms account_defaults groups sample_tickets
  ].freeze

  VIEW_NAMES = %w[
    txt.default.views.your_unsolved.title
    txt.default.views.group_unsolved.title
    txt.default.views.new_in_groups.title
    txt.default.views.recently_solved.title
    txt.default.views.all_unsolved.title
    txt.default.views.unassigned.title
    txt.default.views.recently_updated.title
    txt.default.views.pending.title
    txt.default.views.current_tasks.title
    txt.default.views.overdue_tasks.title
    txt.models.account.customer_satisfaction_support.default_view_name
  ].freeze

  MACROS_NAMES = %w[
    txt.default.macros.close_and_redirect.
    txt.default.macros.downgrade_and_inform.
    txt.default.macros.not_responding.
    txt.default.macros.take_it.
  ].freeze

  TICKET_FIELD_NAMES = %w[
    txt.default.fields.subject.
    txt.default.fields.description.
    txt.default.fields.status.
    txt.default.fields.type.
    txt.default.fields.priority.
    txt.default.fields.group.
    txt.default.fields.assignee.
  ].freeze

  ROLE_NAMES = %w[
    txt.default.roles.staff.
    txt.default.roles.team_leader.
    txt.default.roles.advisor.
    txt.default.roles.light_agent.
  ].freeze

  USER_VIEW_NAMES = %w[
    txt.default.customer_lists.last_30_days_title
  ].freeze

  TICKET_FORM_NAME = %w[
    txt.admin.model.ticket_form.default_form_name
  ].freeze

  GROUPS_NAME = %w[
    txt.default.groups.support.name
  ].freeze

  TRIGGERS_NAMES = {
    'txt.default.triggers.notify_requester_received.'  => %w[body_v3 body_v4 subject_v3 subject_v2],
    'txt.default.triggers.notify_requester_update.'    => %w[body_v2 body_v3],
    'txt.default.triggers.notify_requester_solved.'    => %w[body_v3],
    'txt.default.triggers.notify_assignee_update.'     => %w[body_v2],
    'txt.default.triggers.notify_assignee_assignment.' => %w[body_v2],
    'txt.default.triggers.notify_assignee_reopened.'   => %w[body_v2],
    'txt.default.triggers.notify_group_assignment.'    => %w[body_v2],
    'txt.default.triggers.notify_all_received.'        => %w[body_v2],
    'txt.default.triggers.auto_assign.'                => []
  }.freeze

  COLLABORATION_TRIGGERS_NAMES = {
    'txt.default.triggers.notify_requester_and_ccs_of_received_request.' => %w[body],
    'txt.default.triggers.notify_requester_and_ccs_of_update.'           => %w[body]
  }.freeze

  # So, the "body_v4 body_v3 body_v4" seen here (and subjects too) and in FIXED_TRIGGERS_NAMES below are pretty gross. It's
  # being done that way to work around not one, but two crazy things going on in this code - one is that
  # for notification bodies, we choose the "last" in the array for when Help Center is activated and the
  # "first" when Help Center is not activated. In the "Notify requester of received request" type of
  # notifications, there was a security fix to remove {{ticket.comments_formatted}} from the default
  # notification body because it was being used by spammers to disseminate spam that looked like it was
  # coming from Zendesk. However (and this is the second crazy thing), we also use the strings associated
  # with the listed versioned "body"s to determine whether we found a trigger match whose body should be
  # replaced with the new default. Because of that, we still need to match against the previous version
  # containing {{ticket.comments_formatted}}, which is why "body_v3" is in the middle. The "body_v4" bookends
  # the list so that it is used regardless of whether Help Center is activated or not.
  COLLABORATION_TRIGGERS_NAMES_V2 = {
    'txt.default.triggers.notify_requester_and_ccs_of_received_request.' => %w[
      body_v4
      body_v3
      body_v4
      subject_v2
      subject
      subject_v2
    ],
    'txt.default.triggers.notify_requester_and_ccs_of_update.' => %w[
      body_v4
      body_v2
    ]
  }.freeze

  NOTIFY_REQUESTER_PROACTIVE_TRIGGER_NAME = {
    'txt.default.triggers.notify_requester_proactive_ticket.' => %w[
      body
      subject
      description
    ]
  }.freeze

  # When email_ccs_new_accounts_default is GA and being removed, the entries here can replace
  # those with matching keys in TRIGGERS_NAMES directly, and this hash removed (as well as the merge below)
  FIXED_TRIGGERS_NAMES = {
    'txt.default.triggers.notify_requester_received.' => %w[
      body_v4
      body_v3
      body_v4
      subject_v3
      subject_v2
      subject_v3
    ],
    'txt.default.triggers.notify_requester_update.' => %w[
      body_v4
      body_v2
    ]
  }.freeze

  AUTOMATIONS_NAMES = {
    'txt.default.automations.close.'                                           => '',
    'txt.default.automations.pending_24_hours.'                                => 'body_v2',
    'txt.default.automations.pending_5_days.'                                  => 'body_v2',
    'txt.customer_satisfaction_rating.automation_v2.'                          => 'body',
    'txt.admin.models.account.facebook_support.close_facebook_message_ticket_' => '',
    'txt.admin.models.twitter.close_twitter_ticket_after_status_solved_'       => ''
  }.freeze

  ACCOUNT_DEFAULTS = {
    "default_cc_subject_template"                  => "cc_subject_template",
    "default_cc_email_template"                    => "cc_email_template",
    "default_follower_subject_template"            => "follower_subject_template",
    "default_follower_email_template"              => "follower_email_template",
    "default_organization_activity_email_template" => "organization_activity_email_template",
    "txt.default.registration_message_v2"          => "signup_page_text",
    "txt.default.new_welcome_email_text"           => "signup_email_text",
    "txt.default.verify_email_text"                => "verify_email_text",
    "txt.default.chat_welcome_message"             => "chat_welcome_message",
    "txt.default.ticket_forms_instructions"        => "ticket_forms_instructions"
  }.freeze

  REPORTS_NAMES = {
    'txt.default.reports.backlog.'       => %w[backlog created solved],
    'txt.default.reports.priorities.'    => %w[high_urgent low_normal],
    'txt.default.reports.incidents.'     => %w[new resolved working],
    'txt.default.reports.high_priority.' => %w[backlog created resolved],
    'txt.default.reports.resolution.'    => %w[solved_less_than_2_hours solved_less_than_8_hours solved_less_than_24_hours],
    'txt.default.reports.unsolved.'      => %w[web email chat twitter phone feedback_tab forum facebook]
  }.freeze

  SIGNUP_EMAIL_TEXT = "txt.default.new_welcome_email_text".freeze
  SAMPLE_TICKET_NICE_ID = 1

  COMPARE_FUNCTION_KEYS = %w[
    default_cc_subject_template
    default_cc_email_template
    default_follower_subject_template
    default_follower_email_template
    default_organization_activity_email_template
  ].freeze

  priority :immediate

  attr_reader :account, :all_locales

  # Usage:
  # ------
  # @account = Account.first
  # new_locale_id = TranslationLocale.where(locale:'es').first.id
  # RestoreDefaultContentJob.work(@account.id, @account.translation_locale.id, new_locale_id)
  #
  def self.work(account_id, old_locale_id, new_locale_id)
    unless account = Account.shard_unlocked.active.serviceable.where(id: account_id).first
      Rails.logger.error("Could not find account #{account_id}, skipping.")
      return
    end

    ActiveRecord::Base.on_shard(account.shard_id) do
      restore_content = RestoreDefaultContentJob.new(account, old_locale_id, new_locale_id)
      restore_content.translate_content
    end
  end

  def self.args_to_log(account_id, old_locale_id, new_locale_id)
    { account_id: account_id, old: old_locale_id, new: new_locale_id }
  end

  def initialize(account, old_locale_id, new_locale_id)
    @account         = account
    @new_locale      = TranslationLocale.find_by_id(new_locale_id)
    old_locale       = TranslationLocale.find_by_id(old_locale_id)
    @all_locales     = @account.locales_for_selection.to_a
    # For perfomance. The old locale should be the first in the array
    old_locale_first_in_array(old_locale)
  end

  def translate_content
    TRANSLATABLE_CONTENT.each do |content|
      begin
        send("translate_#{content}")
      rescue StandardError => e
        statsd_client.increment('errors', tags: ["exception_class:#{e.class.name.underscore}", "content_type:#{content}"])
        self.class.resque_log("Failed to translate default content for #{content.humanize}")
        ZendeskExceptions::Logger.record(e, location: self, message: self.class.name, fingerprint: '27732289fc0a569650dda8d6585cc2ca0e9480d8')

        raise if Rails.env.test?
      end
    end
  end

  private

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['resque', 'restore_default_content_job'])
  end

  def old_locale_first_in_array(old_locale)
    position = @all_locales.index { |locale| locale == old_locale }
    prioritize_locale(position)
  end

  def prioritize_locale(current_position)
    return if current_position == 0
    @all_locales.unshift @all_locales.delete_at(current_position)
  end

  def translate_views
    views = @account.views
    return if views.empty?

    VIEW_NAMES.each do |key|
      match_and_translate \
        objects: views,
        main_key: key,
        multi: true
    end
  end

  def translate_user_views
    user_views = @account.user_views
    return if user_views.empty?

    USER_VIEW_NAMES.each do |key|
      match_and_translate \
        objects: user_views,
        main_key: key,
        multi: true
    end
  end

  def translate_macros
    macros = @account.macros
    return if macros.empty?

    MACROS_NAMES.each do |key|
      match_and_translate \
        objects: macros,
        main_key: "#{key}title",
        action_keys: "#{key}value",
        multi: true
    end
  end

  def translate_automations
    automations = @account.automations
    return if automations.empty?

    AUTOMATIONS_NAMES.each do |key, value|
      action_keys = { subject: "#{key}subject" }
      action_keys[:body] = "#{key}#{value}" unless value.empty?

      match_and_translate \
        objects: automations,
        main_key: "#{key}title",
        action_keys: action_keys,
        multi: true
    end
  end

  def translate_triggers
    triggers = @account.triggers
    return if triggers.empty?

    trigger_names = TRIGGERS_NAMES

    if @account.has_email_ccs_default_rules_content?
      self.class.resque_log("Including COLLABORATION_TRIGGERS_NAMES_V2 and FIXED_TRIGGERS_NAMES and NOTIFY_REQUESTER_PROACTIVE_TRIGGER_NAME for translation")
      trigger_names = trigger_names.merge(COLLABORATION_TRIGGERS_NAMES_V2).merge(FIXED_TRIGGERS_NAMES).
        merge(NOTIFY_REQUESTER_PROACTIVE_TRIGGER_NAME)
    elsif @account.has_email_ccs?
      self.class.resque_log("Including COLLABORATION_TRIGGERS_NAMES for translation")
      trigger_names = trigger_names.merge(COLLABORATION_TRIGGERS_NAMES)
    end

    trigger_names.each do |key, value|
      title = "#{key}title"
      action_keys = {}

      unless value.empty?
        action_keys[:body] = key.to_s
        action_keys[:versions] = value.select { |item| item.include? 'body' }
        action_keys[:subject_versions] = value.select { |item| item.include? 'subject' }
        action_keys[:description] = "#{key}description" if value.include?('description')
      end

      action_keys[:subject] = (subject_versions?(action_keys) ? key.to_s : "#{key}subject")

      match_and_translate \
        objects: triggers,
        main_key: title,
        action_keys: action_keys,
        multi: true
    end
  end

  def subject_versions?(action_keys)
    action_keys.key?(:subject_versions) && action_keys[:subject_versions].any?
  end

  # TODO: remove after Classic Reports go EOL
  def translate_reports
    reports = @account.reports
    return if reports.empty?

    REPORTS_NAMES.each do |key, leyend_names|
      match_and_translate \
        objects: reports,
        main_key: "#{key}title",
        action_keys: leyend_names
    end
  end

  def translate_ticket_fields
    ticket_fields = @account.ticket_fields
    return if ticket_fields.empty?

    TICKET_FIELD_NAMES.each do |key|
      match_and_translate \
        objects: ticket_fields,
        main_key: "#{key}title",
        action_keys: "#{key}description"
    end
  end

  def translate_roles
    roles = @account.permission_sets
    return if roles.empty?

    ROLE_NAMES.each do |key|
      match_and_translate \
        objects: roles,
        main_key: "#{key}name",
        action_keys: "#{key}description"
    end
  end

  def translate_ticket_forms
    ticket_forms = @account.ticket_forms
    return if ticket_forms.empty?

    TICKET_FORM_NAME.each do |key|
      match_and_translate \
        objects: ticket_forms,
        main_key: key
    end
  end

  def translate_groups
    groups = @account.groups
    return if groups.empty?

    GROUPS_NAME.each do |key|
      match_and_translate \
        objects: groups,
        main_key: key
    end
  end

  def translate_account_defaults
    # Check Account#verified_owner_email_id?
    # Raises ActiveRecord::RecordInvalid if owner has no verified identities when saving account.
    unless @account.owner.identities.email.verified.exists?
      self.class.resque_log('Skipping translate_account_defaults as account owner has no verified email identities (account can not be saved)')
      return
    end

    ACCOUNT_DEFAULTS.each do |key, property|
      options = {}
      options[:account_name] = @account.name if key == SIGNUP_EMAIL_TEXT
      current_property_value = @account.send(property)

      # Some db records for old accounts have a nil value for property 'ticket_forms_instructions'.
      # Empty strings are acceptable, but nil causes an exception when #match is called.
      # Sometimes other properties are nil as well, so for now lets just skip them.
      next if current_property_value.nil?
      next unless match(current_property_value, key, options)

      new_translation =
        if COMPARE_FUNCTION_KEYS.include?(key)
          send(key, @new_locale)
        elsif key == SIGNUP_EMAIL_TEXT
          translate(key, account_name: @account.name, locale: @new_locale)
        else
          translate(key)
        end
      @account.send("#{property}=", new_translation)
    end
    @account.save!
  end

  def translate_sample_tickets
    # Raises ActiveRecord::RecordInvalid if over limit.
    if TrialLimit.reached_trial_limit?(@account, :user_identity, @account.trial_account_user_identity_limit)
      self.class.resque_log('Skipping translate_sample_tickets as account is over user identities trial limit')
      return
    end

    ticket = @account.tickets.where(via_id: ViaType.SAMPLE_TICKET, nice_id: SAMPLE_TICKET_NICE_ID).first
    return if !ticket || ticket.closed?
    user = @account.owner
    translation_locale = @account.translation_locale
    template_name = translation_locale.english? ? 'meet_the_ticket_en' : 'meet_the_ticket'
    ticket_template = Ticket.ticket_template_with_context(user, template_name)
    return unless ticket_template

    Ticket.with_triggers_disabled(@account, ["txt.default.triggers.notify_requester_received.title"]) do
      ticket.subject = ticket_template['subject']
      # shady hack to edit description. won't update otherwise
      ticket.send(:write_attribute, :description, ticket_template['description'])
      ticket.requester.name = I18n.t('txt.zero_states.sample_ticket.sample_customer')
      ticket.requester.save!
      ticket.will_be_saved_by(user)
      ticket.save!
    end
  end

  def match_and_translate(objects:, main_key:, action_keys: nil, multi: false)
    if multi
      if objects = multi_match(objects, main_key)
        objects.each { |object| update_object_properties(object, main_key, action_keys) }
      end
    else
      if object = match(objects, main_key)
        update_object_properties(object, main_key, action_keys)
      end
    end
  end

  def update_object_properties(object, main_key, action_keys)
    set_title_or_name(object, main_key)
    set_object_content(object, main_key, action_keys)
    update_object(object)
  end

  def set_title_or_name(object, key)
    case object
    when PermissionSet, Forum, Group
      object.name = translate(key)
    when TicketForm
      translated_name = translate(key)
      if object.name == object.display_name
        object.display_name = translated_name
      end
      object.name = translated_name
    else
      object.title = translate(key)
    end
  end

  def set_object_content(object, main_key, action_keys = nil)
    case object
    when Macro
      match_translate_comment(object, action_keys)
    when Trigger, Automation
      set_trigger_description(object, action_keys)
      match_translate_notification(object, action_keys)
    when Report
      match_translate_legends(object, main_key, action_keys)
    when TicketField
      match_translate_properties(object, main_key, action_keys)
    when PermissionSet
      match_translate_description(object, action_keys)
    else
      self.class.resque_log("Cannot set content for #{object}")
      nil # no-op. The translation was already done by #set_title_or_name
    end
  end

  def update_object(object)
    object.save(validate: false)
  end

  def is_read_only?(object) # rubocop:disable Naming/PredicateName
    object.is_a?(PermissionSet) && object.is_light_agent? || object.is_a?(TicketField) && object.is_system_field?
  end

  def match_translate_description(object, description_key)
    if object.description.present? && match(object.description, description_key)
      object.description = translate(description_key)
    end
  end

  def match_translate_comment(macro, action_key)
    comment = macro.definition.actions.find { |action| action.source == "comment_value" }
    return unless comment.try(:value).present?
    comment.value.each_with_index do |v, i|
      comment.value[i] = translate(action_key) if match(v, action_key)
    end
  end

  def set_trigger_description(object, action_keys)
    return unless action_keys.include?(:description)
    object.description = translate(action_keys[:description])
  end

  # Finds and translates the notification text for triggers and automations
  # Trigger's structure:
  # >> trigger.definition.actions
  # >> <DefinitionItem:0x000000102ea3f0 @operator="is", @source="notification_user",
  # @value=["requester_id", "Solicitud recibida: {{ticket.title}}", "Su solicitud (\#{{ticket.id}})..."]>]
  def match_translate_notification(automation_trigger, action_keys)
    notification = automation_trigger.definition.actions.find do |action|
      ["notification_user", "notification_group"].include?(action.source)
    end

    return unless notification

    if subject_versions?(action_keys) && match_trigger_subject_versions?(notification, action_keys) # Trigger Subject
      notification.value[1] = translate(action_keys[:subject] + notification_body(action_keys[:subject_versions]))
    elsif action_keys[:subject] && match(notification.value[1], action_keys[:subject]) # Automation
      notification.value[1] = translate(action_keys[:subject])
    end

    if action_keys[:versions] # Trigger Body
      if match_trigger_versions(notification, action_keys)
        notification.value[2] = translate(action_keys[:body] + notification_body(action_keys[:versions]))
      end
    else # Automation
      if action_keys[:body] && match(notification.value[2], action_keys[:body])
        notification.value[2] = translate(action_keys[:body])
      end
    end
  end

  def match_trigger_subject_versions?(notification, action_keys)
    action_keys[:subject_versions].any? do |version|
      match(notification.value[1], (action_keys[:subject] + version)).present?
    end
  end

  def match_trigger_versions(notification, action_keys)
    matched_object = nil
    action_keys[:versions].each do |version|
      matched_object = version if match(notification.value[2], (action_keys[:body] + version))
    end
    matched_object
  end

  def match_translate_properties(ticket_field, title_key, description_key)
    ticket_field.title_in_portal = translate(title_key)
    if ticket_field.description.present? && match(ticket_field.description, description_key)
      ticket_field.description = translate(description_key)
    end
  end

  def match_translate_legends(report, title_key, action_keys)
    translation_key = /([\w.]+)title$/.match(title_key)[1]
    report[:definition].each do |definition|
      action_keys.each do |action_key|
        legend_key = "#{translation_key}#{action_key}"
        if match(definition[:legend][:name], legend_key)
          definition[:legend][:name] = CGI.unescapeHTML(translate(legend_key))
          break
        end
      end
    end
  end

  def notification_body(value)
    return value unless value.is_a? Array
    if @account.help_center_enabled?
      value.last
    else
      value.first
    end
  end

  def default_cc_subject_template(locale)
    translate("txt.default.cc_email_subject_v2", locale: locale)
  end

  def default_cc_email_template(locale)
    translate("txt.default.cc_email_text.paragraph_v2", locale: locale)
  end

  def default_follower_subject_template(locale)
    translate("txt.default.follower_email_subject", locale: locale)
  end

  def default_follower_email_template(locale)
    translate("txt.default.follower_email_text", locale: locale)
  end

  def default_organization_activity_email_template(locale)
    "#{translate('txt.default.organization_activity_email_text', locale: locale)}\n\n{{ticket.comments_formatted}}"
  end

  def match(object_to_match, key, options = {})
    return if @all_locales.blank?
    method = choose_method(object_to_match, key)

    object_matched = nil

    match_locale_position = @all_locales.index do |possible_locale|
      options[:locale] = possible_locale
      object_matched = compare_according_to_function(object_to_match, key, method, options)
      object_matched.present?
    end

    prioritize_locale(match_locale_position) if match_locale_position
    object_matched
  end

  def multi_match(objects_to_match, key)
    return if @all_locales.blank?
    method = choose_method(objects_to_match, key)

    @all_locales.map do |possible_locale|
      compare_according_to_function(objects_to_match, key, method, locale: possible_locale)
    end.compact
  end

  def choose_method(object, key)
    if COMPARE_FUNCTION_KEYS.include?(key)
      :compare_function
    elsif object.respond_to?(:title) || object_has_title?(object)
      :compare_title
    elsif object.respond_to? :name
      :compare_name
    elsif object.is_a? String
      :compare_string
    else
      raise UndefinedCompareMethodError, "Cannot find method for #{object.class} with key:#{key}"
    end
  end

  def object_has_title?(objects)
    return false if !objects.is_a?(Array) && !objects.is_a?(ActiveRecord::Relation)
    object = objects.first
    object.is_a?(Rule) || object.is_a?(Report) || object.is_a?(TicketField)
  end

  def compare_according_to_function(object_to_match, key, function_type, options)
    case function_type
    when :compare_function
      object_to_match == send(key, options[:locale])
    when :compare_title
      object_to_match.where(title: translate(key, options)).first
    when :compare_name
      object_to_match.where(name: translate(key, options)).first
    when :compare_string
      normalize_newlines(object_to_match) == translate(key, options)
    end
  end

  def normalize_newlines(string)
    string.try(:gsub, /<br(?:\s*\/)?>/, "\n")
  end

  def translate(key, options = {})
    options[:locale] = @new_locale if options[:locale].blank?
    normalize_newlines(I18n.t(key, options))
  end
end
