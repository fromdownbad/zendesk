# TicketField records can have millions of TicketFieldEntry records for large
# accounts. MySQL will lock up an entire cluster if it tries to DELETE all these
# records in a single transaction. This job handles the cleanup after a
# `TicketField#destroy`.
class TicketFieldEntryDeleteJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :low
  self.job_timeout = 3.hours

  BATCH_SIZE = 500
  BATCH_MINIMUM_SLEEP = 1.second
  BATCH_MAXIMUM_SLEEP = 5.minutes

  class << self
    def work(account_id, ticket_field_id, _durable_queue_audit_id = nil)
      account = Account.find(account_id)
      account.on_shard { new(account_id, ticket_field_id).perform }
    end

    private

    def args_to_log(account_id, ticket_field_id, _durable_queue_audit_id = nil)
      { account_id: account_id, ticket_field_id: ticket_field_id }
    end
  end

  def initialize(account_id, ticket_field_id)
    @account_id = account_id
    @ticket_field_id = ticket_field_id
  end

  def perform
    # ActiveRecord does not support `.limit` on `.delete_all`
    sql = "DELETE FROM `ticket_field_entries` WHERE `ticket_field_id` = #{@ticket_field_id.to_i} AND `account_id` = #{@account_id.to_i} LIMIT #{BATCH_SIZE}"
    while TicketFieldEntry.connection.exec_delete(sql, 'DELETE', []) > 0
      # Throttle deletions using Zendesk::DatabaseBackoff#backoff_duration or 5.minutes if it's more than that
      sleep([BATCH_MAXIMUM_SLEEP, database_backoff.backoff_duration].min)
    end
  end

  def database_backoff
    @database_backoff ||= Zendesk::DatabaseBackoff.new(minimum: BATCH_MINIMUM_SLEEP)
  end
end
