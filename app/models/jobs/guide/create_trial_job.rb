module Guide
  class CreateTrialJob
    extend ZendeskJob::Resque::BaseJob
    extend ZendeskJob::Resque::ShardedJob
    extend Resque::Plugins::ExponentialBackoff

    TIMEOUT = 10
    RETRIES = 2

    priority :high

    @max_attempts = 10
    # 0, 1, 2, 5, 10, 30, 60 min
    @backoff_strategy = [0, 60, 120, 300, 600, 1800, 3600]

    # this should get bumped by each backoff
    @expire_retry_key_after = @backoff_strategy.last + 300

    class << self
      def work(account_id)
        account = Account.find_by_id(account_id)
        return unless account

        start_guide_trial(account)

        log_job_complete(account.id)
      end

      def args_to_log(account_id)
        {
          account_id: account_id
        }
      end

      private

      def start_guide_trial(account)
        create_guide_product(account)
      end

      def create_guide_product(account)
        return if guide_product(account)

        account_service_client(account).create_product!(
          Zendesk::Accounts::Client::GUIDE_PRODUCT,
          Guide::Trial.guide_product_payload(account),
          context: "guide_create_trial_job"
        )
      end

      def account_service_client(account)
        Zendesk::Accounts::Client.new(
          account,
          timeout: TIMEOUT,
          retry_options: { max: RETRIES }
        )
      end

      def guide_product(account)
        account_service_client(account).product!(Zendesk::Accounts::Client::GUIDE_PRODUCT)
      rescue Kragle::ResourceNotFound
        nil
      end

      def log_job_complete(account_id)
        Rails.logger.info(
          <<-LOG.squish
            Job completed:
              Account Id: #{account_id}
          LOG
        )
      end
    end
  end
end
