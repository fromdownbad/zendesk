# requires these options to be passed:
#   account_id
#   user_id
class OrganizationBulkUpdateJob < BulkJob
  priority :medium

  def name
    "OrganizationBulkUpdateJob account: #{options[:account_id]}"
  end

  private

  def work
    update_attributes

    total = options[:ids].size
    Rails.logger.info("Running OrganizationBulkUpdateJob, norganizations: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Updating Organization')

    results = options[:ids].each_with_index.flat_map do |organization_id, index|
      at(index + 1, total, "Updating Organization ##{index + 1}")
      organization = organizations_map[organization_id]
      update_organization(organization, organization_id)
    end

    completed(results: results)
  end

  def update_organization(organization, id)
    return fail_job(id, "OrganizationNotFound") unless organization
    organization.attributes = options[:organization]
    organization.custom_field_values.update_from_hash(options[:custom_fields])

    if organization.save
      success
    else
      fail_job(id, "OrganizationUpdateFailed", organization.errors.full_messages.join(' '))
    end
  end

  def organizations_map
    @organizations_map ||= resources_map(:organizations, options[:key], options[:ids])
  end

  def update_attributes
    unless current_user.can?(:edit, Organization)
      options[:organization] = options[:organization].slice(:notes) || {}
    end
  end
end
