class TicketBulkImportJob < BulkCreateJob
  priority :medium

  def name
    "TicketBulkImportJob account: #{options[:account_id]}"
  end

  private

  def import_ticket(ticket, index)
    imported_ticket = importer.import(ticket)
    return { index: index, id: imported_ticket.nice_id, account_id: current_account.id }
  rescue Zendesk::Tickets::Importer::ImportException => error
    return fail_job(index, "TicketImportFailed", error.message)
  end

  def work
    total = options[:tickets].size
    Rails.logger.info("Running TicketBulkImportJob, numbers: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Importing Tickets')

    results = options[:tickets].each_with_index.flat_map do |ticket, index|
      at(index + 1, total, "Creating Tickets ##{index + 1}")
      import_ticket(ticket, index)
    end

    completed(results: results)
  end

  def importer
    @importer ||= Zendesk::Tickets::V2::Importer.new(account: current_account, user: current_user, archive_immediately: options[:archive_immediately])
  end
end
