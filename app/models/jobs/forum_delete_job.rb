class ForumDeleteJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :low

  def self.work(account_id, _requester_id, forum_id)
    forum = Forum.with_deleted { Forum.find_by_id_and_account_id(forum_id, account_id) }
    forum.mark_as_deleted
    forum.save!

    forum.entries.each { |entry| entry.skip_counter_cache = true }
    Forum.with_deleted { forum.soft_delete! }
  end

  def self.args_to_log(account_id, requester_id, forum_id)
    { account_id: account_id, requester_id: requester_id, forum_id: forum_id }
  end
end
