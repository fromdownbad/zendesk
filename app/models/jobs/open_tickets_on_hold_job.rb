class OpenTicketsOnHoldJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium

  class << self
    # Moves tickets from status hold to status open for the given account
    def work(account_id)
      account = Account.find(account_id)
      account.on_shard do
        change_status_hold_to_open(account)
      end
    end

    def args_to_log(account_id)
      { account_id: account_id }
    end

    private

    def change_status_hold_to_open(account)
      resque_log("#{name}: Changing tickets from hold to open for #{account.subdomain}")
      account.tickets.where(status_id: StatusType.HOLD).find_each do |ticket|
        resque_log("#{name}: Changing ticket #{ticket.nice_id}/#{ticket.id} from hold to open for #{account.subdomain}")
        ticket.will_be_saved_by(User.system)
        ticket.audit.disable_triggers = true
        ticket.status_id = StatusType.OPEN
        ticket.save!
      end
      resque_log("#{name}: Done changing tickets from hold to open for #{account.subdomain}")
    end
  end
end
