class OrganizationUnsetJob
  extend ZendeskJob::Resque::BaseJob

  priority :high

  def self.work(account_id, organization_id)
    account = Account.find_by_id(account_id)

    if account
      ActiveRecord::Base.on_shard(account.shard_id) do
        remove_organization_from_users(account, organization_id)
      end
    else
      resque_log("Unable to find account #{account_id}. Assumed deleted")
    end
  end

  def self.args_to_log(account_id, organization_id)
    { account_id: account_id, organization_id: organization_id }
  end

  def self.remove_organization_from_users(account, organization_id)
    account.
      organization_memberships.
      where(organization_id: organization_id).
      select('id, user_id').
      find_in_batches(batch_size: 100) do |batch|
        user_ids = batch.map(&:user_id)
        resque_log("Empty batch! - organization_id: #{organization_id}") if user_ids.empty?
        OrganizationAssociationRemoval.new(
          account.id, organization_id, user_ids
        ).call
      end
  end
  private_class_method :remove_organization_from_users
end
