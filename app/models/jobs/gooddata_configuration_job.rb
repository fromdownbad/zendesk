class GooddataConfigurationJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  priority :low

  self.job_timeout = 5.minutes

  def self.work(account_id, _audit = nil)
    new(
      Account.find(account_id)
    ).work
  end

  def self.args_to_log(account_id, audit = nil)
    {
      account_id: account_id,
      audit_id: audit_id(audit)
    }
  end

  attr_reader :account

  def initialize(account)
    @account = account
  end

  def work
    log_run

    integration = integration_provisioning.gooddata_integration
    return unless integration.present?

    begin
      log_stats(
        Benchmark.realtime do
          integration_provisioning.set_project_api_domains
          integration_provisioning.set_project_time_zone
          integration_provisioning.set_project_title
        end
      )
    rescue
      statsd_client.increment('failed')
      raise
    end
  end

  private

  def integration_provisioning
    @integration_provisioning ||=
      Zendesk::Gooddata::IntegrationProvisioning.new(account)
  end

  def log_run
    log("Updating project configuration for #{account.subdomain} (#{account.id})")
    statsd_client.increment('count')
  rescue
    error('could not log count of GooddataUserCreateJob runs to statsd')
  end

  def log_stats(running_time)
    log("Updating project configuration for #{account.subdomain} (#{account.id})")
    statsd_client.histogram('execution_time', running_time)
  rescue
    error('could not log execution time of GooddataConfigurationJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['jobs', 'gooddata_configuration_job']
    )
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
