class GooddataUserReprovisioningJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  priority :low

  self.job_timeout = 5.minutes

  def self.work(account_id, zendesk_user_id, _audit = nil)
    account = Account.find(account_id)
    zendesk_user = account.users.where(id: zendesk_user_id).first

    new(account, zendesk_user).work
  end

  def self.args_to_log(account_id, zendesk_user_id, audit = nil)
    {
      account_id: account_id,
      zendesk_user_id: zendesk_user_id,
      audit_id: audit_id(audit)
    }
  end

  attr_reader :account, :zendesk_user

  def initialize(account, zendesk_user)
    @account      = account
    @zendesk_user = zendesk_user
  end

  def work
    log_run
    return unless zendesk_user.present?
    return unless account.gooddata_integration.present?

    statsd_client.increment('reprovision_attempts')

    begin
      log_stats(
        Benchmark.realtime do
          Zendesk::Gooddata::UserProvisioning.reprovision_user(zendesk_user)
        end
      )
    rescue
      statsd_client.increment('failed')
      raise
    end
  end

  private

  def log_run
    log("Reprovision Gooddata user for account #{account.id}, zendesk user #{zendesk_user.try(:id)}")
    statsd_client.increment('count')
  rescue
    error('Could not log count of GooddataUserReprovisioningJob runs to statsd')
  end

  def log_stats(running_time)
    log("Successfully reprovisioned GoodData user for account #{account.id}, zendesk user #{zendesk_user.id}")
    statsd_client.histogram('execution_time', running_time)
  rescue
    error('could not log execution time of GooddataUserReprovisioningJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['jobs', 'gooddata_user_reprovisioning_job']
    )
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
