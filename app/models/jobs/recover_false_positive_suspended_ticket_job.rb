##
# Experimental feature note:
# A *highly* experimental feature automates some of the messier work of dividing up shards to share the work evenly
# among instances of this job.  You can use `RecoverFalsePositiveSuspendedTicketJob.auto_run` as a mostly automated
# approach.  You will need to supply any arguments you wish to be run on each instance of the job.  A bare-bones example
# is:
# RecoverFalsePositiveSuspendedTicketJob.auto_run(start_time: Time.new("2019","01","01","00","00","00"), num_workers: 1)
#
# other arguments you can pass (and their defaults) are:
# end_time: nil
# via_id: nil
# skip_spam_check: false
# run_id: nil
# dry_run: false
#
# which perform the same purpose described below.
#
# There are also helper functions used within the `auto_run` method which can be used directly by a user to spot check
# before running.  `suspended_ticket_counts` is described below, and the output from that can be directly passed to
# `divide_work` along with the number of workers to divide the work out to, e.g.
# `divide_work(suspended_ticket_counts(start_time: Time.new("2020", "05", "01", "00", "00", "00")), 5)`
#
# Further information can be found here:
# https://zendesk.atlassian.net/wiki/spaces/ABUSE/pages/1098457343/Bulk+Unsuspending+False+Positives+due+to+ATSD
#
# This job is intended to be run if there is an errant pattern in one of the spammy string arturos which causes a large
# number of false positives (i.e. non-spammy tickets are suspended), creating an unfortunate burden on our customers
#
# Before running the job itself, you will need to determine which shards to run the job on.  You can use the class
# method suspended_ticket_counts to get a mapping from shard_id to SuspendedTicket count where the first argument is the
# earliest date to consider, e.g.
#
# RecoverFalsePositiveSuspendedTicketJob.suspended_ticket_counts(start_time: Time.new('2020', '01', '01', '00', '00', '00'))
#
# in the future this method may support more arguments, but currently only supports the one described above
#
# Running the job itself requires several parameters, detailed below
# Example invocation (in rails console):
# RecoverFalsePositiveSuspendedTicketJob.enqueue(:start_time => Time.new('2020', '05', '15', '00', '00', '00'), :shard_ids => [1], :options => {via_id: 28})
#
# It may be a good idea to store parameters such as :start_time and :options as variables for repeated calls where only
# the :shard_ids argument is different
#
# Required parameters:
# :start_time - The earliest time to check for suspended tickets to unsuspend.
#               This should be determined by abuse analysts as to when the errant
#               pattern was first added
# :shard_ids - Which shards this job should process. Some pre-work can be done to
#             determine how best to split up the shards amongst multiple job instances
# see https://zendesk.atlassian.net/wiki/spaces/ABUSE/pages/900595751/Backfill+Investigation#Shards-Scaled-by-Suspended-Ticket-Count
# for a brief discussion of how to to evenly distribute them among a number of jobs.
#
# Optional parameter
#
# Optional parameters:
# :dry_run - A flag indicating whether to actually perform recovering (dry_run == false), or to just print which tickets the job would
#            *attempt* to be recovered (dry_run == true)
#
# The options below are passed in the `options` hash, e.g. `options[:run_id]`
# :options[:run_id] - An identifier which will be used when tagging datadog
#                    metrics, for simpler grouping/identification
# :options[:skip_spam_check] - A `boolean` flag indicating whether to skip the
#                             spam check before unsuspending.  Setting this to
#                             true will likely result in actual spammy tickets
#                             being unsuspended along with false positives
# :options[:end_time] - A cutoff date for searching for suspended tickets, i.e.
#                       only suspended tickets in the range
#                       start_time..options[:end_time] will be considered
# :options[:via_id] - The via type (e.g. ViaType.MAIL) to limit the search for
#                     suspended tickets to
class RecoverFalsePositiveSuspendedTicketJob < Zendesk::Jobs::Resque
  not_sharded
  params :shard_ids, :start_time, :dry_run, :options
  self.queue = :high
  class RecoverFalsePositiveSuspendedTicketJobException < StandardError; end

  def work
    if start_time.nil?
      raise RecoverFalsePositiveSuspendedTicketJobException, "This job requires the start_time parameter be set"
    end
    unless shard_ids.present?
      raise RecoverFalsePositiveSuspendedTicketJobException, "This job requires the shard_ids parameter be set and non-empty"
    end
    should_run_spam_check = !options.fetch(:skip_spam_check, false)
    run_id = options[:run_id]
    shard_ids.each do |s|
      ActiveRecord::Base.on_shard(s) do
        query = SuspendedTicket.where("created_at >= :begin_time", begin_time: start_time)
        query = query.unscope(where: :created_at).where(created_at: [start_time..options[:end_time]]) if options[:end_time]
        query = query.where(cause: SuspensionType.MALICIOUS_PATTERN_DETECTED)
        query = query.where(via_id: options[:via_id]) if options[:via_id]
        log "#{"(DRY RUN) -- " if dry_run}Processing #{query.count} suspended tickets on shard #{s}"
        query.each do |susp_ticket|
          unsuspend_ticket(susp_ticket, should_run_spam_check, run_id, dry_run)
        end
      end
    end
    log "#{"(DRY RUN) -- " if dry_run}Finished processing shards #{shard_ids}"
  end

  def unsuspend_ticket(suspended_ticket, should_run_spam_check, run_id, dry_run)
    log "#{"(DRY RUN) -- " if dry_run}About to unsuspend Ticket ##{suspended_ticket.id} from '#{suspended_ticket.account.subdomain}' #{suspended_ticket.account.id} account!"
    return if dry_run
    ticket = suspended_ticket.recover(suspended_ticket.account.owner, nil, should_run_spam_check)
    tags = if run_id.nil?
      []
    else
      ["run_id:#{run_id}"]
    end
    statsd_client.increment('suspended_ticket.recovered', tags: tags) unless ticket.nil?
  rescue StandardError => exception
    log "Error: #{exception.class}: #{exception.message} - #{exception.backtrace}"
    statsd_client.increment('suspended_ticket.errored_recovery', tags: tags)
  end

  def log(val)
    Rails.logger.info("recover_false_positive_suspended_ticket: #{val}")
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'recover_false_positive_suspended_ticket_job')
  end

  def self.suspended_ticket_counts(start_time:, end_time: nil, via_id: nil)
    return unless start_time.present?
    shard_to_count = {}
    ActiveRecord::Base.on_all_shards do |shard_id|
      query = SuspendedTicket.where("created_at >= :begin_time", begin_time: start_time)
      query = query.unscope(where: :created_at).where(created_at: [start_time..end_time]) if end_time.present?
      query = query.where(cause: SuspensionType.MALICIOUS_PATTERN_DETECTED)
      query = query.where(via_id: via_id) if via_id.present?

      shard_to_count[shard_id] = query.count
    end
    shard_to_count
  end

  def self.auto_run(start_time:, end_time: nil, via_id: nil, skip_spam_check: false, run_id: nil, num_workers:, dry_run: false)
    distribution = divide_work(suspended_ticket_counts(start_time: start_time, end_time: end_time, via_id: via_id), num_workers)
    options = {}
    options[:end_time] = end_time if end_time.present?
    options[:via_id] = via_id if via_id.present?
    options[:skip_spam_check] = skip_spam_check
    options[:run_id] = run_id if run_id.present?
    distribution.each do |b|
      RecoverFalsePositiveSuspendedTicketJob.enqueue(start_time: start_time, shard_ids: b.shards, dry_run: dry_run, options: options)
    end
  end

  Bucket = Struct.new(:shards, :count) # convenience struct for calculating optimal distribution of work
  # Divides work amongst "workers" (jobs) by total ticket count to be processed by each worker
  def self.divide_work(shards_to_counts, num_workers)
    sorted_by_tickets = shards_to_counts.select { |_, v| v > 0 }.sort_by { |_, v| -v }
    distribution = []
    [sorted_by_tickets.length, num_workers].min.times do
      elem = sorted_by_tickets.shift
      distribution = add_to_bucket_list(distribution, Bucket.new([elem[0]], elem[1]))
    end

    sorted_by_tickets.each do |shard, count|
      bucket = distribution.shift
      bucket.shards << shard
      bucket.count += count
      distribution = add_to_bucket_list(distribution, bucket)
    end

    distribution
  end

  # Ensure the first element in the list is the one with the least tickets
  def self.add_to_bucket_list(distribution, bucket)
    distribution << bucket
    distribution.sort_by(&:count)
  end

  # TODO: this is just a helper to print out how distribution among workers is done
  def self.dist_report(shard_to_counts, workers)
    total = shard_to_counts.values.sum
    result = divide_work(shard_to_counts, workers)
    result.each_with_index do |b, ix|
      puts "Bucket #{ix} has shards #{b.shards} with #{b.count} tickets (#{100 * b.count.to_f / total}%)"
    end
  end
end
