class EmbeddableChatWidgetEnablerJob
  extend ZendeskJob::Resque::BaseJob

  priority :high

  def self.work(*args)
    new(*args).work
  end

  def self.args_to_log(account_id)
    { account_id: account_id }
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def work
    account.on_shard { enable_embeddable_chat_widget }
  end

  private

  def account
    @account ||= Account.find(@account_id)
  end

  def enable_embeddable_chat_widget
    api_client.connection.post('/embeddable/api/internal/config_sets.json',
      config_set: { chat_enabled: true })
  rescue => error
    msg = "Failed to set embeddable chat widget state: #{error.message}"
    ZendeskExceptions::Logger.record(error, location: self, message: msg, fingerprint: '6dee7ae2db85161f156d5129ffb2a523fcf1c05c')
  end

  def api_client
    Zendesk::InternalApi::Client.new(account.subdomain, user: 'zendesk')
  end
end
