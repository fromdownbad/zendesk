# requires these options to be passed:
#   account_id
class GroupMembershipBulkCreateJob < BulkCreateJob
  priority :medium

  def name
    "GroupMembershipBulkCreateJob account: #{options[:account_id]}"
  end

  private

  def create_group_membership(group_membership, index)
    return fail_job(index, "UserNotFound") unless users_map[group_membership[:user_id]]
    return fail_job(index, "GroupNotFound") unless groups_map[group_membership[:group_id]]

    new_group_membership = current_account.memberships.new(group_membership)

    if new_group_membership.save
      return { index: index, id: new_group_membership.id }
    else
      return fail_job(index, "GroupMembershipCreateFailed", new_group_membership.errors.full_messages.join(''))
    end
  end

  def work
    total = options[:group_memberships].size
    Rails.logger.info("Running GroupMembershipBulkCreateJob, ngroup_memberships: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, "Creating Group Membership")

    results = options[:group_memberships].each_with_index.flat_map do |group_membership, index|
      at(index + 1, total, "Creating group memberships ##{index + 1}")
      create_group_membership(group_membership, index)
    end

    completed(results: results)
  end

  def user_ids
    get_ids(options[:group_memberships], :user_id)
  end

  def group_ids
    get_ids(options[:group_memberships], :group_id)
  end

  def users_map
    @users_map ||= resources_map(:users, :id, user_ids)
  end

  def groups_map
    @groups_map ||= resources_map(:groups, :id, group_ids)
  end
end
