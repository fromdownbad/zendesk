# requires these options to be passed:
#   account_id
#   organization_id
class OrganizationMembershipBulkDeleteJob < BulkDeleteJob
  priority :medium

  def name
    "OrganizationMembershipBulkDeleteJob account: #{options[:account_id]}"
  end

  private

  def work
    total = options[:ids].size
    Rails.logger.info("Running OrganizationMembershipBulkDeleteJob, norganization_memberships: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, "Deleting Organization Membership")

    results = options[:ids].each_with_index.flat_map do |id, index|
      at(index + 1, total, "Deleting Organization Membership ##{index + 1}")
      delete_resource(id, organization_membership_map[id], "OrganizationMembership")
    end

    completed(results: results)
  end

  def organization_membership_map
    @organization_membership_map ||= resources_map(:organization_memberships, :id, options[:ids])
  end
end
