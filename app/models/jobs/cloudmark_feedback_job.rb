class CloudmarkFeedbackJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  def self.report_ham(user, identifier)
    enqueue(classification: 'legit', account_id: user.account_id, user_id: user.id, identifier: identifier)
  end

  def self.report_spam(user, identifier)
    enqueue(classification: 'spam', account_id: user.account_id, user_id: user.id, identifier: identifier)
  end

  def self.work(params)
    new(params).work
  end

  def self.args_to_log(args)
    args
  end

  attr_reader :params

  def initialize(params)
    @params = params.with_indifferent_access
  end

  def work
    return if analysis_unavailable
    CloudmarkFeedbackMailer.deliver_header_feedback(delivery_params)
  end

  protected

  def analysis_unavailable
    if raw_email.blank?
      log("missing raw email. skipping.")
      return true
    end

    if analysis_header.blank?
      log("missing analysis header. skipping.")
      return true
    end

    if spam_score.value.blank?
      log("missing spam score. skipping")
      return true
    end

    if user_agrees_with_cloudmark?
      log("user agrees with cloudmark. skipping.")
      return true
    end

    false
  end

  def delivery_params
    params.merge(analysis_header: analysis_header)
  end

  def analysis_header
    return if message.blank?

    message.headers['X-CMAE-Analysis'].try(:first)
  end

  def user
    @user ||= account.users.find(params[:user_id])
  end

  def account
    @account ||= Account.find(params[:account_id])
  end

  # It's possible the score was due to Spam Assassin or whitelisting instead of Cloudmark
  def user_agrees_with_cloudmark?
    case params[:classification]
    when 'spam'  then  spam_score.obviously_spam?
    when 'legit' then !spam_score.obviously_spam?
    end
  end

  def spam_score
    Zendesk::InboundMail::CloudmarkSpamScore.new(message)
  end

  def message
    return if raw_email.blank?
    raw_email.message
  end

  def raw_email
    @raw_email ||= Zendesk::Mailer::RawEmailAccess.new(params[:identifier])
  end

  def log(message)
    self.class.resque_log(message)
  end
end
