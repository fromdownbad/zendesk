# requires these options to be passed:
#   account_id
#   user_id
class UserBatchUpdateJob < BatchUpdateJob
  priority :medium

  def name
    "UserBatchUpdateJob account: #{options[:account_id]}"
  end

  private

  def update_user(update, idx)
    record_span = ZendeskAPM.trace(RECORD_SPAN_NAME, service: APM_SERVICE_NAME, resource: 'User#update', tags: common_span_tags)

    if update[:id]
      user = user_map[update[:id]]
    elsif update[:external_id]
      user = user_external_id_map[update[:external_id].to_s.downcase]
    else
      return job_error(record_span, update, idx, 'id or external_id required')
    end

    if update[:organization_id]
      return job_error(record_span, update, idx, 'OrganizationNotFound') unless update[:organization_id].in?(organization_map)
    end

    return job_error(record_span, update, idx, 'UserNotFound') unless user
    return job_error(record_span, update, idx, 'PermissionDenied') unless current_user.can?(:edit, user)

    user_initializer = Zendesk::Users::Initializer.new(current_account, current_user, user: update)
    user_initializer.apply(user)

    if Zendesk::SupportUsers::User.new(user).save
      result = batch_success(id: user.id)

      register_span_tags(record_span, result_span_tags(result))
      result
    else
      job_error(record_span, update, idx, 'UserUpdateError', user.errors.full_messages)
    end
  ensure
    record_span.finish
  end

  def job_error(span, update, idx, error, details = nil)
    error_result = fail_job(update, idx, error, details)
    register_span_tags(span, error_span_tags(error_result.merge(action: :update, status: 'Failed')))
    error_result
  end

  def work
    updates = (options[:users] || []).slice(0, 100)
    total = updates.size
    job_span = ZendeskAPM.trace(JOB_SPAN_NAME, service: APM_SERVICE_NAME, resource: self.class.name, tags: common_span_tags.merge!('zendesk.total_records' => total))

    Rails.logger.info("Running UserBatchUpdateJob, nusers: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")

    get_ids(:users)
    at(0, total, "Updating users")
    results = updates.each_with_index.flat_map do |update, idx|
      at(idx + 1, total, "Updating user ##{idx}")

      started_at = current_time
      result = update_user(update, idx)
      record_duration_metric((current_time - started_at) * 1000, result[:error] ? result.merge(action: :update, status: 'Failed') : result)
      result
    end

    completed(results: results)
  ensure
    job_span.finish
  end

  def user_map
    @user_map ||= resources_map(:users, :id, @ids)
  end

  def user_external_id_map
    @user_external_id_map ||= resources_map(:users, :external_id, @external_ids).map { |k, v| [k.to_s.downcase, v] }.to_h
  end

  def organization_map
    @organization_map ||= resources_map(:organizations, :id, @organization_ids)
  end
end
