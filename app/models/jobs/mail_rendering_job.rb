require 'action_mailer/enqueable'
require 'action_mailer/logged_smtp_delivery'
require 'jobs/mail_rendering_job/account_detection'
require 'jobs/mail_rendering_job/deferred'

class MailRenderingJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :outbound_mail

  class_attribute :timeout
  self.timeout = Zendesk::Configuration[:outbound_mail][:rendering_job_timeout] || 1.minute

  def self.work(account_id, deferred, options, audit_id = nil, _unused_env = nil)
    account = Account.find(account_id)

    account.on_shard do
      localize(options) do
        job         = new(deferred)
        job.account = account
        job.audit   = QueueAudit.find_by_enqueued_id(audit_id) if audit_id
        job.work
      end
    end
  end

  def self.args_to_log(account_id, deferred, options, audit = nil, _ununsed_env = nil)
    { account_id: account_id, deferred: deferred, options: options, audit_id: audit_id(audit) }
  end

  def self.localize(options)
    Time.use_zone(options['time_zone']) do
      I18n.with_locale(options['locale']) do
        yield
      end
    end
  end

  attr_reader :deferred
  attr_accessor :account, :audit
  delegate :mail, to: :deferred

  def initialize(deferred)
    Sharing::Agreement.with_deleted do
      @deferred = ActionMailer::Enqueable::Deferred.from_hash(deferred)
    end
  end

  def work
    statsd_client.increment(:queued)
    @execution_starts = Process.clock_gettime(Process::CLOCK_MONOTONIC) if account.has_email_mrj_record_event_based_slo?

    begin
      mail.deliver_now
    rescue Net::SMTPServerBusy => e
      if account.has_email_track_smtp_server_busy_errors?
        statsd_client.increment(:smtp_server_busy_errors)
        failure!
        raise Zendesk::OutgoingMail::SMTPServerBusy, e.message, e.backtrace
      else
        failure!
        raise
      end
    rescue Net::SMTPSyntaxError => e
      failure!
      raise if e.message !~ /501/
    rescue Net::OpenTimeout
      statsd_client.increment(:net_opentimeout_errors)
      failure!
      raise
    rescue Net::ReadTimeout
      statsd_client.increment(:net_readtimeout_errors)
      failure!
      raise
    rescue StandardError
      failure!
      raise
    end

    success!
    mail
  end

  def success!
    instrument_completed_metric('success')
    statsd_client.increment(:processed, tags: ['result:success'])
  end

  def failure!
    instrument_completed_metric('failed')
    statsd_client.increment(:processed, tags: ['result:failed'])
  end

  def instrument_completed_metric(result)
    return unless account.has_email_mrj_record_event_based_slo?

    execution_duration = Process.clock_gettime(Process::CLOCK_MONOTONIC) - @execution_starts
    statsd_client.sli_histogram_decremental(:completed, duration: (execution_duration * 1000).round, tags: ["result:#{result}"])
    statsd_client.timing(:completed_duration, (execution_duration * 1000).round, tags: ["result:#{result}"])
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'outbound_mail.mail_rendering_job')
  end
end

module Zendesk::OutgoingMail
  class SMTPServerBusy < Net::SMTPServerBusy
  end
end
