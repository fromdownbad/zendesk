require "uri"

class FetchProfileImageJob
  extend ZendeskJob::Resque::BaseJob

  priority :high

  class RetrievalError < StandardError; end

  class << self
    def work(account_id, user_id, remote_photo_url)
      begin
        URI.parse(remote_photo_url)
      rescue URI::InvalidURIError
        resque_log("invalid URL, aborting #{remote_photo_url}")
        return
      end

      account = Account.find(account_id)
      account.on_shard do
        if user = account.users.find_by_id(user_id)
          resque_log(["fetching", "start", remote_photo_url])
          response = set_remote_photo(user, remote_photo_url)
          resque_log(["fetching", "complete", response + [remote_photo_url]])
        end
      end
    rescue RetrievalError, SocketError, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNRESET, ActiveRecord::RecordInvalid => e
      resque_log(["profile image fetch error", e.message])
    end

    def args_to_log(account_id, user_id, url)
      { account_id: account_id, user_id: user_id, url: url }
    end

    def set_remote_photo(user, remote_photo_url)
      raise(RetrievalError, "connection failure") unless response = Zendesk::Net::MultiSSL.get(remote_photo_url, raise_errors: false)
      raise(RetrievalError, "bad photo") unless valid_photo?(response.status, response.body, response.headers)

      with_tempfile(remote_photo_url, response.body) do |tmpfile|
        content_type = response.headers['Content-Type']

        data = Zendesk::Attachments::CgiFileWrapper.new(
          tmpfile.path,
          content_type: content_type,
          filename: file_name(user, tmpfile.path, content_type)
        )

        p = user.set_photo(uploaded_data: data, content_type: content_type, original_url: remote_photo_url)

        unless user.save
          resque_log("Failed to set profile image on user: #{user.errors.inspect}")
          resque_log("Photo looked like: #{p.inspect}")
        end
      end

      [response.status, response.body.size, response.headers]
    end

    def valid_photo?(status, file_data, _headers)
      status == 200 && !file_data.empty?
    end

    private

    def with_tempfile(file_url, file_data)
      ext = File.extname(file_url)
      tmpfile = Tempfile.new(["fetch-profile-image-job", ext], Dir.tmpdir, encoding: 'ascii-8bit')
      tmpfile.write(file_data)
      tmpfile.rewind
      yield tmpfile
    ensure
      tmpfile && tmpfile.close
      tmpfile && tmpfile.unlink
    end

    def file_name(user, path, content_type)
      mime_type = Zendesk::Utils::MimeTypes.determine_mime_type(path, content_type)
      ext = Zendesk::Utils::MimeTypes.mime_type_to_extension(mime_type) || File.extname(path)[1..-1]

      "profile_image_#{user.id}_#{user.account_id}.#{ext}"
    end
  end
end
