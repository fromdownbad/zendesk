class ApplyMacrosJob
  extend ZendeskJob::Resque::BaseJob

  priority :immediate

  def self.work(account_id, ticket_id, macro_ids)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      new(account, ticket_id, macro_ids).perform
    end
  end

  def self.args_to_log(account_id, ticket_id, macro_ids)
    { account_id: account_id, ticket_id: ticket_id, macro_ids: macro_ids }
  end

  attr_reader :account, :ticket_id, :macro_ids

  def initialize(account, ticket_id, macro_ids)
    @account   = account
    @ticket_id = ticket_id
    @macro_ids = (macro_ids || []).uniq
  end

  def macros
    @macros ||= account.all_macros.active.where(id: macro_ids).to_a
  end

  def perform
    Zendesk::DB::RaceGuard.cache_locked("post-ticket-event/#{account.id}/#{ticket_id}", wait_max: 10.seconds, execute_on_timeout: true) do
      macros.each do |macro|
        ticket = account.tickets.find(ticket_id)
        ticket.will_be_saved_by((ticket.assignee || User.system), via_id: ViaType.MACRO_REFERENCE)

        macro.execute(ticket)
        ticket.reset_delta_changes!

        (ticket.comment || ticket.audit.events.any? || ticket.changed?) && ticket.save
      end
    end
  end
end
