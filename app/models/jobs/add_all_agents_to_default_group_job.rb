class AddAllAgentsToDefaultGroupJob
  extend ZendeskJob::Resque::BaseJob
  priority :high

  def self.work(account_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      add_all_agents_to_default_group(account)
    end
  end

  def self.args_to_log(account_id)
    { account_id: account_id }
  end

  def self.add_all_agents_to_default_group(account)
    # find or create default group for account
    default_group = account.default_group || account.groups.
      where(name: I18n.t("txt.default.groups.support.name")).first_or_create.tap { |group| group.update(default: true) }

    resque_log("#{name}: Changing agent default group to #{default_group.name} for #{account.subdomain}")
    account.agents.each do |agent|
      unless agent.default_group_id == default_group.id
        agent.set_default_group default_group.id
        agent.save!
      end
    end
    resque_log("#{name}: Done updating agents default group for #{account.subdomain}")
  end

  private_class_method :add_all_agents_to_default_group
end
