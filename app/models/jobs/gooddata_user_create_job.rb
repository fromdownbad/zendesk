class GooddataUserCreateJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  priority :medium

  self.job_timeout = 5.minutes

  def self.work(account_id, user_id, _audit = nil)
    new(
      Account.find(account_id),
      User.find(user_id)
    ).work
  end

  def self.args_to_log(account_id, user_id, audit = nil)
    {
      account_id: account_id,
      user_id: user_id,
      audit_id: audit_id(audit)
    }
  end

  attr_reader :account, :user

  def initialize(account, user)
    @account = account
    @user = user
  end

  def work
    log_run

    return if GooddataUser.for_user(user).present?

    begin
      log_stats(
        Benchmark.realtime do
          gooddata_user_provisioning.create_gooddata_user(user)
        end
      )
    rescue
      statsd_client.increment('failed')
      raise
    end
  end

  private

  def gooddata_user_provisioning
    @gooddata_user_provisioning ||=
      Zendesk::Gooddata::UserProvisioning.new(account)
  end

  def log_run
    log("Creating GoodData user for account #{account.id}, User #{user.id}")
    statsd_client.increment('count')
  rescue
    error('could not log count of GooddataUserCreateJob runs to statsd')
  end

  def log_stats(running_time)
    log("Successfully created GoodData user for account #{account.id}, User #{user.id}")
    statsd_client.histogram('execution_time', running_time)
  rescue
    error('could not log execution time of GooddataUserCreateJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['jobs', 'gooddata_user_create_job']
    )
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
