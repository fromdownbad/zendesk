class OrganizationsJob < BaseImportJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  class << self
    private

    attr_reader :source

    def work(account_id, requester_id, update_records, token, source = nil)
      @source = source

      super(account_id, requester_id, update_records, token)
    end

    def fields
      ['name', 'external_id', 'notes', 'details']
    end

    def finders
      ['external_id', 'name']
    end

    def identifier_field
      'name'
    end

    def kind
      Organization
    end

    def subject_error(user)
      email_locale = user.locale_id.present? ? user.translation_locale : user.account.translation_locale
      I18n.with_locale(email_locale) do
        I18n.t("txt.email.organization_import_job.import_error.title") # "#{kind.name} import error"
      end
    end

    def subject_success(user)
      email_locale = user.locale_id.present? ? user.translation_locale : user.account.translation_locale
      I18n.with_locale(email_locale) do
        I18n.t("txt.email.organization_import_job.import_success.title") # "Your #{kind.name.downcase} import is ready"
      end
    end

    def process_row(organization, columns, row, account, custom_keys)
      fields.each do |field|
        if index = columns.index(field)
          organization.send("#{field}=", row[index])
        end
      end

      process_row_shared_settings(row, columns, organization)
      process_row_group(row, columns, organization, account)
      process_row_tags(row, columns, organization)

      process_row_organization_domains_and_emails(row, columns, organization, account)

      process_row_custom_fields(organization, columns, row, account, custom_keys)

      organization
    end

    def process_row_shared_settings(row, columns, organization)
      if index = columns.index('shared')
        organization.is_shared = row[index].to_s.casecmp('true').zero?
        if organization.is_shared?
          if index = columns.index('shared_comments')
            organization.is_shared_comments = row[index].to_s.casecmp('true').zero?
          end
        end
      end
    end

    def process_row_group(row, columns, organization, account)
      if index = columns.index('group')
        organization.group = account.groups.where(name: row[index]).first
      end
    end

    def process_row_tags(row, columns, organization)
      if index = columns.index('tags')
        organization.set_tags = row[index]
      end
    end

    def process_row_organization_domains_and_emails(row, columns, organization, account)
      if domains_index = columns.index('domains')
        data = row[domains_index]

        Rails.logger.info("OrganizationsJob: extracting DOMAINS for '#{account.subdomain}' account: '#{data}'")

        organization.domain_names = data
      elsif default_index = columns.index('default')
        data = row[default_index]

        Rails.logger.info("OrganizationsJob: extracting DEFAULT for '#{account.subdomain}' account: '#{data}'")

        organization.domain_names = data
      end
    end
  end
end
