class UsersJob < BaseImportJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  class << self
    private

    attr_reader :source

    def work(account_id, requester_id, update_records, token, password_email_change_csv_import = true, source = nil, allow_agent_downgrade = false, report_external_id = false)
      @password_email_change_csv_import = password_email_change_csv_import
      @source = source
      @allow_agent_downgrade = allow_agent_downgrade

      @report_external_id = nil
      @report_external_id = report_external_id
      Rails.logger.info("users_job: begin work  account_id: #{account_id}, report_external_id: #{@report_external_id}")
      super(account_id, requester_id, update_records, token)
    end

    def record_keys(record)
      return super(record) unless @report_external_id
      [record.send(identifier_field), record.send(secondary_identifier_field)]
    end

    def fields
      ['name', 'email', 'external_id', 'details', 'notes', 'phone']
    end

    def append_fields
      ['details', 'notes']
    end

    def finders
      ['external_id', 'email']
    end

    def identifier_field
      'email'
    end

    def secondary_identifier_field
      'external_id'
    end

    def kind
      User
    end

    def subject_error(user)
      I18n.t("txt.email.user_import_job.import_error.title", locale: user.translation_locale) # "#{kind.name} import error"
    end

    def subject_success(user)
      I18n.t("txt.email.user_import_job.import_success.title", locale: user.translation_locale) # "Your #{kind.name.downcase} import is ready"
    end

    def process_row(user, columns, row, account, custom_keys)
      process_row_verification(user, account)

      process_row_brand(user, columns, row, account)

      fields.each do |field|
        next unless index = columns.index(field)
        new_value = row[index]
        if append_fields.include?(field)
          existing_value = user.send(field) || ''
          new_value = existing_value + (new_value || '')
        end
        process_email(account, field, new_value)
        process_phone(user, account, field, new_value, columns, row)
      end

      process_password(user, columns, row, account)
      process_row_role(user, columns, row)
      process_row_tags(user, columns, row)
      process_row_restrictions(user, columns, row, account)
      process_row_organization(user, columns, row)
      process_row_custom_fields(user, columns, row, account, custom_keys)

      user.current_user = @requester

      user
    end

    def bad_data?(field, value)
      return true if field == 'name' && value.blank?
      return true if field == 'email' && value.blank?
      false
    end

    def process_password(user, columns, row, account)
      return unless account.admins_can_set_user_passwords && index = columns.index('password')
      if user.new_record? || !row[index].blank?
        user.password = row[index]
        user.password_change_email = @password_email_change_csv_import
      end
    end

    def process_email(account, field, new_value)
      if field == 'email'
        identity = account.user_identities.email.where(value: new_value).first
        if identity && identity.user.deleted?
          Rails.logger.info("UserEmailIdentity found for #{new_value} belongs to user_id: #{identity.user_id} deleted?: #{identity.user.try(:deleted?)} - Destroying it!")
          identity.destroy
        end
      end
    end

    def shared_phone_number?(columns, row)
      shared_phone_number = columns.index('shared_phone_number')
      return false if shared_phone_number.nil?
      row[shared_phone_number] == 'true'
    end

    def process_phone(user, account, field, new_value, columns, row)
      if field == 'phone'
        if new_value.present? # nil value or '' is not a valid phone identity
          if user.identities.phone.where(value: new_value.to_s.sub(/x(\s)*\d+$/, '')).empty?
            is_direct_line = Users::PhoneNumber.new(account, new_value).valid_and_unique? && !shared_phone_number?(columns, row)
            user.update_phone_and_identity(new_value, is_direct_line)
          end
        end
      else
        user.send("#{field}=", new_value) unless bad_data?(field, new_value)
      end
    end

    def process_row_verification(user, account)
      if user.new_record?
        if user.password.blank?
          user.send_verify_email = account.is_welcome_email_when_agent_register_enabled?
        else
          user.skip_verification = true
        end
      end
    end

    def process_row_role(user, columns, row)
      return unless index = columns.index('role')
      if row[index] && role = Role.find_by_name(row[index].capitalize)
        return if !@allow_agent_downgrade && user.is_agent? && role == Role::END_USER

        user.roles = role.id
      else # role can't be found
        # default to end-user for new user, ignore for existing users
        user.roles = Role::END_USER.id if user.new_record?
      end
    end

    def process_row_tags(user, columns, row)
      return unless index = columns.index('tags')
      user.additional_tags = row[index]
    end

    # Overloading "restriction" field. Why?
    # So that Enterprise plans can create users as "light agent", "staff", "team leader", "advisor", "administrator"
    # or any custom roles that have been created in their helpdesk
    def process_row_restrictions(user, columns, row, account)
      return unless index = columns.index("restriction")

      # First look for a permission_set by that name
      if account.has_permission_sets? && user.is_agent?
        permission_set = account.permission_sets.find_by_name(row[index])
        permission_set ||= account.light_agent_permission_set unless user.is_admin?
        user.permission_set_id = permission_set.try(:id)
      elsif account.has_light_agents? && user.is_agent? && (row[index] == 'Light Agent')
        user.permission_set_id = account.light_agent_permission_set.id
      end

      # If permission_set_id was not set, fallback on normal (non-Enterprise) use of "restriction"
      # e.g., "group", "organization", "assigned", "requested"
      unless user.permission_set_id
        user.restriction_id = RoleRestrictionType.find(row[index]) || RoleRestrictionType.NONE
      end
    end

    def process_row_organization(user, columns, row)
      return unless index = columns.index { |c| c =~ /organization/ }

      organization_value = row[index]
      organization_names = Array.wrap(organization_value.try(:split, "|"))

      user.organization_names = organization_names
    end

    def process_row_brand(user, columns, row, account)
      return unless index = columns.index("brand")
      brand_subdomain = row[index]

      # Used primarily to establish a preferred brand to use for
      # the welcome/verification email sent to the user. We
      # restrict this to active brands that have an enabled help center
      # If brand can't be found, the 'brand' method in the UsersMailer will decide what brand to use
      brand = account.brands.active.detect { |b| b.subdomain == brand_subdomain }
      if brand.try(:help_center_enabled?)
        user.active_brand_id = brand.id
      end
    end
  end
end
