class BaseImportJob
  extend ZendeskJob::Resque::BaseJob
  extend Zendesk::ExternalErrorTranslations

  class OneWeekAttachment < ExpirableAttachment
    expirable ttl: 1.week
  end

  class << self
    KNOWN_ERRORS = ['CSV::MalformedCSVError', 'ArgumentError', 'Missing mandatory fields'].freeze

    def work(account_id, requester_id, update_records, token, _unused_env = nil)
      account = Account.find(account_id)
      account.on_shard do
        @requester = User.find(requester_id)
        retval = process(@requester, update_records, token)
        resque_log("import completed with response: #{retval}")
      end
    end

    def args_to_log(account_id, requester_id, dry_run, token, *_unused_args)
      { account_id: account_id, requester_id: requester_id, dry_run: dry_run, token: token }
    end

    private

    def process(user, update_records, token)
      resque_log("process(#{user.id}, #{update_records}, #{token})")
      begin
        if user&.account&.has_ocp_set_import_job_default_locale?
          ::I18n.locale = user.translation_locale
        end
        data = build_data(user, read_token(user, token))
      rescue CSV::MalformedCSVError => e
        translated_error = translate_error(e.message, user.translation_locale)
        link = I18n.t('txt.email.import_job.bad_format_article_link', locale: user.translation_locale)
        deliver_job_complete(user, subject_error(user), I18n.t('txt.email.import_job.bad_format.message_v2', bad_format_article_link: link, error_message: translated_error, locale: user.translation_locale))
        return I18n.t('txt.email.import_job.bad_format.title', error_message: translated_error, locale: user.translation_locale)
      rescue ArgumentError => e
        translated_error = translate_error(e.message, user.translation_locale)
        link = I18n.t('txt.email.import_job.utf_error_article_link', locale: user.translation_locale)
        deliver_job_complete(user, I18n.t('txt.email.import_job.utf_error.title', error_message: translated_error, locale: user.translation_locale), I18n.t('txt.email.import_job.utf_error.message', utf_error_article_link: link, error_message: translated_error, locale: user.translation_locale))
        return I18n.t('txt.email.import_job.utf_error.title', error_message: translated_error, locale: user.translation_locale)
      rescue StandardError => e
        deliver_job_complete(user, subject_error(user), I18n.t('txt.email.import_job.data_not_ready.message', error_message: e.message, locale: user.translation_locale))
        raise e
      end
      process_rows(user, update_records, data)
    end

    def process_rows(user, update_records, data)
      account = user.account
      columns = format_columns(data.shift)
      return I18n.t("txt.email.import_job.missing_fields.title", locale: user.translation_locale) if missing_mandatory_fields(user, columns)
      created, updated, failed, unchanged = process_records(user, account, columns, data, update_records)
      account.expire_scoped_cache_key(:views)
      attachment_url = build_expirable_attachment(user, account, created, updated, failed, unchanged)
      deliver_job_complete(user, subject_success(user), message_success(attachment_url, user))
      I18n.t("txt.email.import_job.all_rows_processed.title", locale: user.translation_locale)
    end

    def record_keys(record)
      [record.send(identifier_field)]
    end

    # Returns an array of the finders to try, e.g. [ 'external_id', 'email', 'name' ]
    # They will be tried in that order.
    def finders
      raise 'Must be implemented by subclass'
    end

    # Returns the name of the column used to identify a record textually in the result,
    # e.g. 'name'
    def identifier_field
      raise 'Must be implemented by subclass'
    end

    def secondary_identifier_field
      raise 'Must be implemented by subclass'
    end

    def process_row(_record, _columns, _row, _account)
      raise 'Must be implemented by subclass'
    end

    # Returns the object class
    def kind
      raise 'Must be implemented by subclass'
    end

    def deliver_job_complete(user, subject, message)
      JobsMailer.deliver_job_complete(user, name, subject, message)
    end

    def build_expirable_attachment(user, account, created, updated, failed, unchanged)
      basename = PermalinkFu.escape("#{kind.name} import #{Time.now.in_time_zone.to_s(:csv)}")
      name = basename + ".csv"
      csvfile = Tempfile.new([basename, ".csv"])

      CSV.open(csvfile.path, 'a', col_sep: ",", force_quotes: true) do |csv|
        add_collection_result(csv, [header_message(created.size, 'created')], created)
        add_collection_result(csv, [header_message(updated.size, 'updated')], updated)
        add_collection_result(csv, [header_message(failed.size, 'failed'), 'Error message'], failed)
        add_collection_result(csv, [header_message(unchanged.size, 'unchanged')], unchanged)
      end

      attachment = OneWeekAttachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(csvfile.path, filename: name))
      attachment.account = account
      attachment.author = user
      attachment.created_via = self.name
      attachment.save!

      csvfile.close
      csvfile.unlink
      attachment.url(token: true)
    end

    def add_collection_result(csv, headers, collection)
      unless collection.empty?
        csv << headers
        collection.each { |element| csv << Array(element) }
        csv << ['']
      end
    end

    def row_descriptor(record, row)
      if !record.send(identifier_field).blank?
        record.send(identifier_field)
      elsif !row[0].blank?
        row[0]
      else
        'Unknown row descriptor'
      end
    end

    def header_message(count, message)
      "#{count} #{(count == 1) ? kind.name.downcase : kind.name.downcase.pluralize} #{message}"
    end

    def message_success(url, user)
      I18n.t("txt.email.import_job.import_success.message", data_url: url, locale: user.translation_locale)
    end

    # Finds the existent instance using the finders or creates a new object
    def ensure_record(account, columns, row)
      scope = account.send(kind.name.downcase.pluralize)

      finders.each do |finder|
        index = columns.index(finder)
        next unless index && value = row[index].presence
        row[index] = value.strip # Remove trailing space from finder

        value = row[index]
        record =
          case finder
          when "external_id"
            scope.find_by_external_id(value)
          when "name"
            scope.find_by_name(value)
          when "email"
            account.find_user_by_email(value, scope: scope)
          else
            resque_log("Unexpected finder #{finder}")
          end

        return record unless record.nil?
      end

      scope.new
    end

    def read_token(user, token)
      token = user.account.upload_tokens.find_by_value(token)
      attachment = token.attachments.first
      attachment.current_data
    end

    def build_data(_user, csv_data)
      # ZD:399905 force UTF-8 and remove BOM
      csv_data.force_encoding(Encoding::UTF_8)
      csv_data.sub!("\xEF\xBB\xBF".force_encoding(Encoding::UTF_8), '') # strip BOM if present
      CSV.parse(csv_data, col_sep: detect_col_sep(csv_data), encoding: Encoding::UTF_8)
    end

    def detect_col_sep(csv_data)
      return ';' if csv_data.index(',').nil?
      return ',' if csv_data.index(';').nil?
      (csv_data.index(';') < csv_data.index(',')) ? ';' : ','
    end

    def format_columns(columns)
      columns.map { |name| name.to_s.downcase.strip }
    end

    def missing_mandatory_fields(user, columns)
      unless columns.include?(identifier_field)
        deliver_job_complete(user, subject_error(user), I18n.t("txt.email.import_job.missing_fields.message", field_name: identifier_field, locale: user.translation_locale))
        return true
      end

      false
    end

    def process_records(user, account, columns, data, update_records)
      created = []
      updated = []
      failed = []
      unchanged = []
      custom_keys = get_custom_keys(columns, account)
      processed = 0
      resque_log("#{processed} of #{data.size} rows successfully processed.")
      report_to_datadog(data.size) if kind == User || kind == Organization

      data.in_groups_of(100, false) do |rows|
        rows.each do |row|
          begin
            record = ensure_record(account, columns, row)
            if record.new_record? || update_records
              record = process_row(record, columns, row, account, custom_keys)
              # Flag the record to avoid the caching_observer calls
              record.is_bulk_updated = true if record.respond_to?(:is_bulk_updated)
              new_record = record.new_record?
              record_persistence_object = kind == User ? Zendesk::SupportUsers::User.new(record) : record
              if record_persistence_object.save
                new_record ? created << record_keys(record) : updated << record_keys(record)
              else
                failed << [row_descriptor(record, row), record.errors.full_messages.join(', ')]
              end
            else
              unchanged << record_keys(record)
            end
          rescue Exception => e # rubocop:disable Lint/RescueException
            row_data = row.join(', ')
            deliver_job_complete(user, subject_error(user), I18n.t("txt.email.import_job.import_error.message", row_data: row_data, error_message: e.message, locale: user.translation_locale))
            raise e
          end
        end
        processed += rows.size
        resque_log("#{processed} of #{data.size} rows successfully processed.")
        sleep(3) if processed < data.size
      end

      [created, updated, failed, unchanged]
    end

    def get_custom_keys(columns, account)
      valid_keys = account.custom_fields.send("for_#{kind.name.downcase}").active.map(&:key)
      columns.select { |k, _v| k =~ /^custom_fields\..*$/ && valid_keys.include?(k.sub("custom_fields.", "")) }
    end

    def process_row_custom_fields(object, columns, row, _account, custom_keys)
      value_hash = Hash[custom_keys.collect { |key| [key.sub("custom_fields.", ""), row[columns.index(key)]] }]
      object.custom_field_values.update_from_hash(value_hash)
    end

    def report_to_datadog(rows_count)
      model_name = kind.to_s.downcase
      tags = ["source:#{source}", "#{model_name}s_count:#{rows_count}"]
      statsd_client.increment("#{model_name}_bulk_import", tags: tags)
    end

    def statsd_client
      Zendesk::StatsD::Client.new(namespace: 'bulk_change_instrumentation')
    end

    def source
      raise 'Must be implemented by subclass'
    end
  end
end
