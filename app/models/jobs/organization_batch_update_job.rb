# requires these options to be passed:
#   account_id
#   user_id
class OrganizationBatchUpdateJob < BatchUpdateJob
  priority :medium

  def name
    "OrganizationBatchUpdateJob account: #{options[:account_id]}"
  end

  private

  def work
    get_ids(:organizations)

    total = options[:organizations].size
    Rails.logger.info("Running OrganizationBatchUpdateJob, norganizations: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Updating Organization')

    results = options[:organizations].each_with_index.flat_map do |organization, index|
      at(index + 1, total, "Updating Organization ##{index + 1}")
      update_organization(organization, index)
    end

    completed(results: results)
  end

  def update_organization(update, index)
    custom_fields = update.delete(:organization_fields) if update[:organization_fields]

    organization = if update[:id]
      organization_map[update[:id]]
    else
      organization_external_id_map[update[:external_id].to_s]
    end
    return fail_job(update, index, "OrganizationNotFound") unless organization

    if edit_permission
      organization.attributes = update
    elsif update["notes"]
      organization[:notes] = update["notes"]
    end

    organization.custom_field_values.update_from_hash(custom_fields)

    if organization.save
      batch_success(id: organization.id)
    else
      fail_job(update, index, "OrganizationUpdateFailed", organization.errors.full_messages.join(' '))
    end
  end

  def organization_map
    @organization_map ||= resources_map(:organizations, :id, @ids)
  end

  def organization_external_id_map
    @organization_external_id_map ||= resources_map(:organizations, :external_id, @external_ids)
  end

  def edit_permission
    @edit_permission ||= current_user.can?(:edit, Organization)
  end
end
