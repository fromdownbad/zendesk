class PushNotificationUpdateJob < PushNotificationBaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend PushNotificationKillSwitch

  priority :medium

  def self.work(options)
    options = options.with_indifferent_access
    token = options.delete(:token)
    device_type = options.delete(:device_type)
    options.delete(:account_id)
    config = urban_airship_config(options[:mobile_app_identifier])
    client = PushNotifications::UrbanAirshipClient.new(config['app_key'], config['master_secret'])
    client.update(token, device_type, options)
  end

  def self.args_to_log(options)
    { options: options }
  end
end
