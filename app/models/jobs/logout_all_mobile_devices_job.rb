class LogoutAllMobileDevicesJob
  extend ZendeskJob::Resque::BaseJob

  BATCH_SIZE = 500

  priority :immediate

  def self.work(account_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      account.mobile_devices.find_in_batches(batch_size: BATCH_SIZE) { |b| b.each(&:revoke_token) }
    end
  end

  def self.args_to_log(account_id)
    { account_id: account_id }
  end
end
