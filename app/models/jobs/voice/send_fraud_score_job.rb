module Voice
  class SendFraudScoreJob < BaseJob
    priority :high

    def self.work(account_id, risky)
      account = Account.find(account_id)

      return unless account.is_active?
      return unless risky

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_fraud_score(account, risky)

      Rails.logger.info("account_id: #{account.id}. Sending fraud_score #{risky} to Voice.")
    end

    def self.args_to_log(account_id, risky)
      { account_id: account_id, risky: risky }
    end
  end
end
