module Voice
  class BaseJob
    extend ZendeskJob::Resque::BaseJob
    extend ZendeskJob::Resque::ShardedJob

    # https://github.com/lantins/resque-retry#exp
    extend Resque::Plugins::ExponentialBackoff

    priority :low
  end
end
