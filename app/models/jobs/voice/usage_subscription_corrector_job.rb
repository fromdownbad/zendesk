module Voice
  class UsageSubscriptionCorrectorJob < BaseJob
    priority :high

    private_class_method :new

    def self.work(account_id)
      new(account_id).work
    end

    def self.args_to_log(account_id)
      { account_id: account_id }
    end

    def work
      return unless can_monitor?

      statsd_client.increment("correct_subscription")

      begin
        if usage_subscription.present?
          update_kind_of_existing_usage_subscription
        else
          add_usage_subscription
        end
      rescue StandardError
        statsd_client.increment("correct_subscription_error")
      end
    end

    private

    attr_reader :account_id

    def initialize(account_id)
      @account_id = account_id
    end

    # Fetch for a voice usage subscription through ZuoraClient
    # (by product rate plan) instead of IronBank (by subscription kind)
    # to identify an active usage subscription missing kind
    def usage_subscription
      @usage_subscription ||= begin
        subscription = fetch_usage_subscription_by_rate_plan
        return nil if subscription.blank?

        decorated_subscription = ZuoraUsageSubscription.new(subscription)
        decorated_subscription.expired? ? nil : decorated_subscription
      end
    end

    def fetch_usage_subscription_by_rate_plan
      result = nil
      time = Benchmark.realtime do
        result = zuora_client.get_arrears_voice_subscription!
      end
      statsd_client.timing("fetch_usage_subscription_by_rate_plan", time * 1000)

      result
    end

    def update_kind_of_existing_usage_subscription
      return unless usage_subscription.update_kind?

      statsd_client.increment("update_kind_to_usage")

      begin
        usage_subscription.update_kind! if can_correct?
      rescue StandardError
        statsd_client.increment("update_kind_to_usage_error")
      end
    end

    def add_usage_subscription
      statsd_client.increment("add_usage_subscription")

      begin
        ZBC::Zuora::Jobs::AddVoiceJob.enqueue(account_id) if can_correct?
      rescue StandardError
        statsd_client.increment("add_usage_subscription_error")
      end
    end

    def account
      @account ||= Account.find(account_id)
    end

    def zuora_client
      @zuora_client ||= ZendeskBillingCore::Zuora::Client.new(billing_id)
    end

    def billing_id
      @billing_id ||= account.zuora_subscription.zuora_account_id
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: %w[voice_usage usage_subscription_corrector_job]
      )
    end

    def can_monitor?
      Arturo.feature_enabled_for?(
        :billing_voice_unuploaded_usage_cleanup_job_monitoring,
        account
      )
    end

    def can_correct?
      Arturo.feature_enabled_for?(
        :billing_voice_unuploaded_usage_cleanup_job,
        account
      )
    end

    class ZuoraUsageSubscription < SimpleDelegator
      delegate :rate_plans, to: :zuora_voice_subscription

      def expired?
        today = ZBC::Zuora::Date.today.iso8601

        zuora_voice_subscription.respond_to?(:term_end_date) && term_end_date < today
      end

      def missing_usage_kind?
        subscription_kind__c != "Usage"
      end

      def only_one_rate_plan?
        rate_plans.count == 1
      end

      # Usage rate plan should always be the only rate plan on its subscription;
      # if more than one rate plan exist, this is considered an invalid config
      # (likely the usage rate plan is on the primary subscription), and thus
      # we should NOT update the "kind" field
      def update_kind?
        only_one_rate_plan? && missing_usage_kind?
      end

      def update_kind!
        IronBank::Update.call(update_params)
      end

      def update_params
        { type: "Subscription", objects: [{ id: id, subscription_kind__c: "Usage" }] }
      end

      def subscription_kind__c
        zuora_voice_subscription.get_custom_field(:subscription_kind__c)
      end
    end
  end
end
