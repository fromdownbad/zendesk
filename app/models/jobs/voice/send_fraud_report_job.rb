module Voice
  class SendFraudReportJob < BaseJob
    priority :high

    def self.work(account_id, fraud_score)
      account = Account.find(account_id)
      return unless account.is_active?

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_fraud_report(account, fraud_score: fraud_score)
      Rails.logger.info("account_id: #{account.id}. Sending fraud_score = #{fraud_score}")
    end

    def self.args_to_log(account_id, fraud_score)
      { account_id: account_id, fraud_score: fraud_score }
    end
  end
end
