module Voice
  class GroupCleanupJob < BaseJob
    priority :high

    def self.work(account_id, group_id)
      account = Account.find(account_id)
      Zendesk::Voice::InternalApiClient.new(account.subdomain).remove_from_routing(group_id)
    end

    def self.args_to_log(account_id, group_id)
      { account_id: account_id, group_id: group_id }
    end
  end
end
