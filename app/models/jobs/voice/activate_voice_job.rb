module Voice
  class ActivateVoiceJob < BaseJob
    priority :low

    def self.work(account_id)
      account = Account.find(account_id)

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_activated
      Rails.logger.info("account_id: #{account.id}. Voice sub account unsuspended / activated.")
    end

    def self.args_to_log(account_id)
      { account_id: account_id }
    end
  end
end
