module Voice
  class UpdateVoiceJob < BaseJob
    priority :low

    def self.work(account_id, data)
      account = Account.find(account_id)

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_updated(data)
      Rails.logger.info("account_id: #{account.id}. Updating voice subscription.")
    end

    def self.args_to_log(account_id, changes)
      { account_id: account_id, changes: changes }
    end
  end
end
