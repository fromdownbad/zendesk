module Voice
  class CreateVoiceJob < BaseJob
    priority :low

    def self.work(account_id, plan_type)
      account = Account.find(account_id)

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_created(plan_type)
      Rails.logger.info("account_id: #{account.id}. Voice sub account created.")
    end

    def self.args_to_log(account_id, plan_type)
      { account_id: account_id, plan_type: plan_type }
    end
  end
end
