module Voice
  class NotifyVoiceSeatIsDestroyed < BaseJob
    priority :high

    def self.work(account_id, user_id)
      account = Account.find(account_id)
      mark_as_unavailable_on_voice(account, user_id)
    end

    def self.args_to_log(account_id, user_id)
      { account_id: account_id, user_id: user_id }
    end

    def self.mark_as_unavailable_on_voice(account, user_id)
      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_seat_lost(user_id)
    end
  end
end
