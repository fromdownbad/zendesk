class GooddataIntegrationsDestroyJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  priority :medium

  self.job_timeout = 5.minutes

  def self.work(account_id, gooddata_integration_id, _audit = nil)
    new(
      Account.find(account_id),
      GooddataIntegration.unscoped.find_by_id(gooddata_integration_id)
    ).work
  end

  def self.args_to_log(account_id, gooddata_integration_id, audit = nil)
    {
      account_id: account_id,
      gooddata_integration_id: gooddata_integration_id,
      audit_id: audit_id(audit)
    }
  end

  attr_reader :account, :gooddata_integration

  def initialize(account, gooddata_integration)
    @account              = account
    @gooddata_integration = gooddata_integration
  end

  def work
    log_run
    return unless gooddata_integration.present?

    begin
      log_stats(
        Benchmark.realtime do
          integration_provisioning.destroy_gooddata_integration
        end
      )
    rescue
      statsd_client.increment('failed')
      raise
    end
  end

  private

  def integration_provisioning
    @integration_provisioning ||=
      Zendesk::Gooddata::IntegrationProvisioning.new(account, gooddata_integration: gooddata_integration)
  end

  def log_run
    log('Destroying GoodData integration for ' \
        "account #{account.id} (project #{gooddata_integration.project_id})")

    statsd_client.increment('count')
  rescue
    error('Could not log count of GooddataIntegrationsDestroyJob runs to statsd')
  end

  def log_stats(running_time)
    log('Successfully destroyed GoodData integration for ' \
        "account #{account.id} (project #{gooddata_integration.project_id})")

    statsd_client.histogram('execution_time', running_time)
  rescue
    error('could not log execution time of GooddataIntegrationDestroyJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['jobs', 'gooddata_integration_destroy_job']
    )
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
