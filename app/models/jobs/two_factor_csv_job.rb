require 'zip/zip'
require 'zip/zipfilesystem'

class TwoFactorCsvJob < CsvJob
  include Zendesk::CsvScrubber

  priority :medium

  throttle can_run_every: 10.minutes
  throttle disabled: true if Rails.env.development?

  private

  def file_name
    "two-factor-#{Time.now.strftime('%Y-%m-%d')}"
  end

  def csv_header
    [
      "Agent name",
      "Agent email",
      "Two factor enabled?"
    ]
  end

  def csv_data
    csv_data = []

    account.agents.find_each do |agent|
      csv_data << [
        agent.name,
        agent.email,
        two_factor_configured?(agent)
      ].map { |v| Zendesk::CsvScrubber.scrub_output(v.to_s) }
    end

    csv_data
  end

  def two_factor_configured?(agent)
    agent.otp_configured? ? 'Yes' : 'No'
  end
end
