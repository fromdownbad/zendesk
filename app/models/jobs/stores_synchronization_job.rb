class StoresSynchronizationJob
  extend ZendeskJob::Resque::BaseJob

  priority :cloud_storage

  def self.work(account_id, attachment_id, _audit = nil)
    account = Account.find(account_id)

    ActiveRecord::Base.on_shard(account.shard_id) do
      attachment = Attachment.on_master.find(attachment_id)

      attachment.synchronize_stores
      attachment.create_missing_thumbnails!
    end
  end

  def self.args_to_log(account_id, attachment_id, *_args)
    { account_id: account_id, attachment_id: attachment_id }
  end
end
