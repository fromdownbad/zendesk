class DowncaseSubdomainsJob
  extend ZendeskJob::Resque::BaseJob

  priority :immediate

  def work(account_id)
    account = Account.find(account_id)
    account.on_shard do
      account.subdomain.downcase!
      account.save! if account.changed?

      account.routes.each do |route|
        route.subdomain.downcase!
        route.save! if route.changed?
      end
    end
  end
end
