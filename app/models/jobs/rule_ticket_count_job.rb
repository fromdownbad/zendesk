class RuleTicketCountJob
  extend ZendeskJob::Resque::BaseJob
  DEFAULT_COUNT_TIMEOUT = 10.minutes
  COUNT_JOB_TIMEOUT = 60.seconds
  TITLE = 'rule-ticket-count-job'.freeze

  priority :rule_count

  def self.work(account_id, rule, user, include_archived_tickets, options = {})
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      new(rule, user, nil, include_archived_tickets, options).perform
    end
  end

  attr_accessor :options

  def initialize(rule, user, cached_count = nil, _include_archived_tickets = false, options = {})
    if rule.is_a?(Rule)
      @rule = rule
      @rule_id = rule.id
    else
      @rule_id = rule
    end

    if user.is_a?(User)
      @user = user
      @user_id = user.id
    else
      @user_id = user
    end

    @cached_count = cached_count
    @new_count_from_occam = nil

    # When a enqueued rescue job triggers all the symbols are converted to string args
    options[:caller] ||= options['caller'] || TITLE

    @options = options
  end

  def user
    @user ||= User.find(@user_id) if @user_id
  end

  def rule
    @rule ||= Rule.find(@rule_id)
  end

  def cached_count
    @cached_count ||= rule.ticket_count(user, refresh: false)
  end

  def perform(timeout = nil)
    query_timeout = timeout || if user.account.has_occam_count_job_timeout?
                                 COUNT_JOB_TIMEOUT
                               else
                                 DEFAULT_COUNT_TIMEOUT
                               end

    options[:timeout] = query_timeout

    ActiveRecord::Base.with_slave do
      cached_count.execution_time = Benchmark.realtime do
        with_occam_error_handling do
          @new_count_from_occam = @rule.count_tickets(user, options)
        end
      end
      return unless @new_count_from_occam
      cached_count.value = @new_count_from_occam
      cached_count.updated_at = Time.now
      cached_count.update_requested_at = nil
      cached_count.save
    end
  end

  def self.args_to_log(account_id, rule_id, user_id, include_archived_tickets, options = {})
    { account_id: account_id, rule_id: rule_id, user_id: user_id, include_archived_tickets: include_archived_tickets }.merge(options)
  end

  private

  def with_occam_error_handling(&_block)
    yield
  rescue Occam::Client::ClientConnectionError => e
    tags = ["error:#{e.class.name.underscore}", 'degraded:true', 'affects:count']

    if e.respond_to?(:wrapped_exception)
      tags << "wrapped_exception:#{e.wrapped_exception.class.name.underscore}"
    end

    Rails.logger.warn("Connection to Occam failed with: " + e.class.name.underscore)
    Rails.application.config.statsd.client.increment('zendesk.views.soft_errors', tags: tags)
  end
end
