class TicketSharingSupportAddressesJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :low

  def self.work(account_id)
    account = Account.find(account_id)
    begin
      account.refresh_ticket_sharing_partner_support_addresses
    rescue StandardError => e
      resque_warn("Account #{account_id}: Error in TicketSharingSupportAddressesJob: #{e.message}")
    end
  end

  def self.args_to_log(account_id)
    {
      account_id: account_id
    }
  end
end
