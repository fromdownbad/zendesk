class RollupBacklogJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  priority :stats_generic

  class << self
    def work(account_id, duration, time_stamp)
      Zendesk::Stats::TicketBacklog.rollup(account_id, duration, time_stamp)
    rescue ActiveRecord::StatementInvalid => e
      raise e unless e.message.include? "Duplicate entry"
    end

    def args_to_log(account_id, duration, timestamp)
      { account_id: account_id, duration: duration, timestamp: timestamp }
    end
  end
end
