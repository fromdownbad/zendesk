# requires these options to be passed:
#   account_id
#   user_id
#   organization_id
class OrganizationMembershipBulkCreateJob < BulkCreateJob
  priority :medium

  def name
    "OrganizationMembershipBulkCreateJob account: #{options[:account_id]}"
  end

  private

  def create_organization_membership(organization_membership, index)
    user = users_map[organization_membership[:user_id]]
    organization = organizations_map[organization_membership[:organization_id]]

    return fail_job(index, "UserNotFound") unless user
    return fail_job(index, "OrganizationNotFound") unless organization

    new_organization_membership = current_account.organization_memberships.new(user: user, organization: organization)

    # If multiple organizations is disabled and user already has an organization membership,
    # tickets will be moved by the OrganizationMembership after_destroy :move_user_tickets callback
    # when the existing organization membership is destroyed.
    should_move_tickets = !new_organization_membership.user.organization_memberships.exists?

    if new_organization_membership.save
      unless current_account.settings.multiple_organizations?
        old_organization_memberships = new_organization_membership.user.organization_memberships.where('id <> ?', new_organization_membership.id)
        Rails.logger.info("Removing user #{user.id} 's surplus organizations: [#{old_organization_memberships.map(&:organization_id).join(', ')}]")
        old_organization_memberships.where('id <> ?', new_organization_membership.id).destroy_all
        user.reload.organization = organization
        user.save!
      end

      if should_move_tickets
        new_organization_membership.move_user_tickets_from_any
      end

      return { index: index, id: new_organization_membership.id }
    else
      return fail_job(index, "OrganizationMembershipCreateFailed", new_organization_membership.errors.full_messages.join(' '))
    end
  end

  def work
    total = organization_memberships.size
    Rails.logger.info("Running OrganizationMembershipBulkCreateJob, numbers: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Creating Organization Memberships')

    results = organization_memberships.each_with_index.flat_map do |organization_membership, index|
      at(index + 1, total, "Creating organization memberships ##{index + 1}")
      create_organization_membership(organization_membership, index)
    end

    completed(results: results)
  end

  def organization_memberships
    @organization_memberships ||= options[:organization_memberships]
  end

  def user_ids
    get_ids(organization_memberships, :user_id)
  end

  def organization_ids
    get_ids(organization_memberships, :organization_id)
  end

  def users_map
    @users_map ||= resources_map(:users, :id, user_ids)
  end

  def organizations_map
    @organizations_map ||= resources_map(:organizations, :id, organization_ids)
  end
end
