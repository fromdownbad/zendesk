# requires these options to be passed:
#   account_id
#   user_id
class TicketBatchUpdateJob < BatchUpdateJob
  include Zendesk::Tickets::CommentUpdate

  priority :medium

  def name
    "TicketBatchUpdateJob account:#{options[:account_id]} user:#{options[:user_id]}"
  end

  private

  def work
    total = options[:tickets].size
    Rails.logger.info("Running TicketBatchUpdateJob, ntickets: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, "Updating Ticket")

    results = updates.each_with_index.flat_map do |update, index|
      at(index + 1, updates.size, "Updating Ticket ##{index + 1}")
      ticket = ticket_map.delete(update[:id])
      update_ticket(index, ticket, update)
    end

    completed(results: results)
  end

  def update_ticket(index, ticket, attributes)
    return fail_job(attributes, index, "TicketNotFound") unless ticket
    return fail_job(attributes, index, "UnprocessableEntity", "Safe Update requires an updated_stamp") if attributes[:safe_update] && !attributes[:updated_stamp]

    update = options.merge(ticket: attributes)
    update[:id] = ticket.nice_id
    update[:ticket_object] = ticket
    update[:ticket][:request] = update.delete(:request)

    initializer = Zendesk::Tickets::V2::Initializer.new(current_account, current_user, update, request_params: request_params)
    ticket = initializer.ticket
    return fail_job(attributes, index, "PermissionDenied") unless current_user.can?(:edit, ticket) || can_bulk_update_comments(ticket)

    # ZD#4249853 automatically safe update batch updates
    ticket.safe_update_timestamp ||= Time.at(options['enqueued_at']) if options['enqueued_at']

    ticket.audit.disable_triggers = true if options[:disable_triggers]

    if ticket.save
      batch_success(id: ticket.nice_id)
    else
      error = ticket.errors.include?(:safe_update_timestamp) ? "UpdateConflict" : "TicketUpdateFailed"
      fail_job(attributes, index, error, ticket.errors.full_messages.join(' '))
    end
  rescue Prop::RateLimited => e
    message = e.message.gsub(/ for key .*/, "")
    fail_job(attributes, index, "RateLimited", message)
  end

  def updates
    @updates ||= options.delete(:tickets)
  end

  def ids
    updates.map { |ticket| ticket[:id] }
  end

  def ticket_map
    @ticket_map ||= resources_map(:tickets, :nice_id, ids)
  end

  def request_params
    @request_params ||= Zendesk::Tickets::HttpRequestParameters.new(options.delete(:request_params))
  end
end
