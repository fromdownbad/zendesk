class GooddataUserSyncJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  priority :medium

  self.job_timeout = 5.minutes

  def self.work(account_id, gooddata_user_id, _audit = nil)
    new(
      Account.find(account_id),
      GooddataUser.find_by_id(gooddata_user_id)
    ).work
  end

  def self.args_to_log(account_id, gooddata_user_id, audit = nil)
    {
      account_id: account_id,
      gooddata_user_id: gooddata_user_id,
      audit_id: audit_id(audit)
    }
  end

  attr_reader :account, :gooddata_user

  def initialize(account, gooddata_user)
    @account       = account
    @gooddata_user = gooddata_user
  end

  def work
    log_run

    return unless gooddata_user.present?
    return unless account.gooddata_integration.present?

    statsd_client.increment('sync_attempts')

    begin
      log_stats(
        Benchmark.realtime do
          gooddata_user_provisioning.sync_gooddata_user(gooddata_user)
        end
      )
    rescue
      statsd_client.increment('failed')
      raise
    end
  end

  private

  def gooddata_user_provisioning
    @gooddata_user_provisioning ||=
      Zendesk::Gooddata::UserProvisioning.new(account)
  end

  def log_run
    log("Syncing GoodData user information for account #{account.id}, GooddataUser #{gooddata_user.try(:id)}")
    statsd_client.increment('count')
  rescue
    error('could not log count of GooddataUserSyncJob runs to statsd')
  end

  def log_stats(running_time)
    log("Successfully synced GoodData user information for account #{account.id}, GooddataUser #{gooddata_user.id}")
    statsd_client.histogram('execution_time', running_time)
  rescue
    error('could not log execution time of GooddataUserSyncJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['jobs', 'gooddata_user_sync_job']
    )
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
