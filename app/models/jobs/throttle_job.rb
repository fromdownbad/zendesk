class ThrottleJob < Resque::ThrottledJob
  extend ZendeskJob::Resque::BaseJob

  def self.latest
    raise "Must be implemented in the class that includes this module."
  end

  def self.available_for?(_account)
    raise "Must be implemented in the class that includes this module."
  end

  def self.accessible_for?(account)
    Zendesk::Export::Configuration.new(account).type_accessible?(self)
  end
end
