class SyncChatAgentAvatarJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :high

  self.job_timeout = 2.minutes

  class << self
    def work(account_id, user_id, _audit = nil)
      account = Account.find(account_id)

      account.on_shard do
        return unless user = account.users.find_by_id(user_id)

        Zopim::InternalApiClient.new(account_id).update_agent_setting_avatar(user_id, user.photo_url)
      end
    end

    def delay(enqueue_count)
      (enqueue_count**2).minutes
    end

    def args_to_log(account_id, user_id, _audit = nil)
      { account_id: account_id, user_id: user_id }
    end
  end
end
