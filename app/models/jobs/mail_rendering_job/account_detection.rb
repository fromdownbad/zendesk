class MailRenderingJob
  module AccountDetection
    class NotFound < ArgumentError
      def initialize(deferred)
        @deferred = deferred
      end

      def message
        "At least one argument must be an account or have an account id: #{@deferred.mailer_name}##{@deferred.method_id}(#{@deferred.arguments.inspect})"
      end
    end

    def self.find_account_id!(deferred)
      find_account_id(deferred) || raise(NotFound, deferred)
    end

    def self.find_account_id(deferred)
      found = nil

      deferred.arguments.flatten.each do |value|
        found = find_account_id_from_record(value)
        break found if found
      end

      found
    end

    def self.find_account_id_from_record(value)
      return (value[:account] || value['account_id'] || value[:account_id]) if value.is_a?(Hash)

      (value.respond_to?(:account_id) && value.account_id) ||
        (value.is_a?(Account) && value.id)                 ||
        (value.is_a?(DunningNotification) && value.subscription.account_id)
    end
  end
end
