# Provides an enqueue interface for Convoy::ActionMailer::Enqueable.
class MailRenderingJob
  module Deferred
    def self.enqueue(deferred)
      MailRenderingJob.enqueue(*build_arguments(deferred))
    end

    # Supports testing mail delivery with Resque disabled.
    module ResqueEnabled
      def enqueue(deferred)
        Resque.with_inline do
          super(deferred)
        end
      end
    end
    if Rails.env.test?
      class << self
        prepend ResqueEnabled
      end
    end

    def self.build_arguments(deferred)
      deferred.validate!
      zone_name = Time.find_zone!(Time.zone.name).name # AB#73770282 Temporary measure to find breakage source
      options = { 'time_zone' => zone_name, 'locale' => I18n.locale }
      account_id = AccountDetection.find_account_id!(deferred)

      [account_id, deferred.encoded, options]
    end
  end
end
