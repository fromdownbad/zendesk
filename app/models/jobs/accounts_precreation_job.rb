require 'resque-retry'

class AccountsPrecreationJob
  extend ZendeskJob::Resque::BaseJob
  extend Resque::Plugins::ExponentialBackoff

  priority :high

  @backoff_strategy = [3600, 14_400, 28_800] # 1, 4, 8 hrs

  def self.work(params, request_ip, region)
    target_locale = DefaultLocaleChooser.choose(params['language'])

    account = Zendesk::Accounts::Initializer.
      new(params.with_indifferent_access, request_ip, region).
      precreate_account(convert: false)
    account.on_shard do
      Zendesk::SupportAccounts::Product.retrieve(account).save!
      statsd_client.increment("count", tags: ["language:#{account.translation_locale.name.downcase}", "retry:#{retry?}", "type:success"])
    end
    account.update_column(:is_serviceable, false)
    account.update_column(:updated_at, Time.now)
  rescue => e
    statsd_client.increment("count", tags: ["language:#{target_locale.name.downcase}", "retry:#{retry?}", "type:failure"])
    error_message = "Failed to pre-create an account with params: #{params}"
    ZendeskExceptions::Logger.record(e, location: self, message: error_message, fingerprint: '865edd3968baec82a02ae81b238dbf5e445600ab')
    raise e
  end

  def self.retry?
    retry_attempt > 0
  end

  def self.args_to_log(params, request_ip, region)
    { params: params, request_ip: request_ip, region: region }
  end

  def self.statsd_client
    @client ||= Zendesk::StatsD.client(namespace: name.snakecase)
  end
end
