class FraudScoreJob
  extend ZendeskJob::Resque::BaseJob
  extend Resque::Plugins::ExponentialBackoff
  include Fraud::FraudScoreActions
  include Fraud::FraudServiceHelper

  priority :immediate

  @backoff_strategy = [600, 1800, 3600, 10_800, 18_000] # 10min, 30min, 1hour, 3hours, 5hours

  @retry_delay_multiplicand_min = 1.0
  @retry_delay_multiplicand_max = 1.1

  @expire_retry_key_after = @backoff_strategy.last

  attr_reader :source_event, :current_account
  DEFAULT_FRAUD_JOB_DELAY = 60.seconds

  def self.enqueue_account(account, source_event, reason = nil, raw_response = nil, params = nil)
    enqueue(account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, source_event, reason, raw_response, params)
  end

  def self.enqueue_account_in(account, source_event, fraud_job_delay = DEFAULT_FRAUD_JOB_DELAY, reason = nil, raw_response = nil, params = nil)
    enqueue_in(fraud_job_delay, account.id, account.name, account.subdomain, account.owner.email, account.owner.name, account.time_zone, source_event, reason, raw_response, params)
  end

  def self.work(account_id, account_name, subdomain, owner_email, owner_name, time_zone, source_event, reason = nil, raw_response = nil, params = nil)
    new(account_id, account_name, subdomain, owner_email, owner_name, time_zone, source_event, reason, raw_response, params).work
  end

  def initialize(account_id, account_name, subdomain, owner_email, owner_name, time_zone, source_event, reason = nil, raw_response = nil, params = nil)
    @current_account = Account.find(account_id)
    @account_id      = account_id
    @account_name    = account_name
    @subdomain       = subdomain
    @owner_email     = owner_email
    @owner_name      = owner_name
    @time_zone       = time_zone
    @raw_response    = raw_response
    @source_event    = source_event
    @reason          = reason
    @params          = params || {}
  end

  def work
    @current_account.on_shard { generate_fraud_score }
  rescue => e
    log_fraud_score_job_error(e, @source_event)
    raise
  end

  def self.args_to_log(account_id, account_name, subdomain, owner_email, owner_name, time_zone, source_event, reason = nil, raw_response = nil, params = nil)
    {
      account_id:   account_id,
      account_name: account_name,
      subdomain:    subdomain,
      owner_email:  owner_email,
      owner_name:   owner_name,
      time_zone:    time_zone,
      source_event: source_event,
      reason:       reason,
      raw_response: raw_response,
      params:       params,
    }
  end

  def self.account_creation_enqueue(account, source_event, fraud_job_delay = nil)
    if Arturo.feature_enabled_for?(:fraud_score_job_maxmind, account)
      case source_event
      when Fraud::SourceEvent::TRIAL_SIGNUP
        enqueue_account(account, source_event, '', @raw_response.to_json, trial_enqueue_timestamp: Time.now.utc)
      when Fraud::SourceEvent::SHELL_CREATED
        enqueue_account_in(account, source_event, fraud_job_delay)
      end
    end
  end

  protected

  def deep_symbolized_hash(body)
    body.deep_symbolize_keys rescue nil
  end

  private

  def generate_fraud_score
    return unless @current_account.has_account_fraud_service_enabled?

    fraud_service_response = send_to_fraud_service
    return if fraud_service_response.empty?

    actions = (fraud_service_response['actions'] || {})

    create_fraud_score(fraud_service_response, actions['auto_suspend'])
    send_create_metrics_to_datadog(fraud_service_response, @source_event)
    perform_fraud_actions(actions, @source_event)
  end

  def create_fraud_score(body, auto_suspend_val)
    @current_account.fraud_scores.create!(
      score:                         body['score'],
      score_params:                  body['score_params'],
      service_response:              deep_symbolized_hash(body),
      account_fraud_service_release: body['account_fraud_service_release'],
      verified_fraud:                auto_suspend_val,
      is_whitelisted:                false,
      subdomain:                     @subdomain,
      owner_email:                   @owner_email,
      source_event:                  @source_event
    )

    log_enqueue_to_fraud_score_delta if @params['trial_enqueue_timestamp']
  end

  def send_create_metrics_to_datadog(body, source_event)
    body ||= {}
    actions = (body['actions'] || {})

    tags = ["afs_source_event:#{source_event}"]
    tags << "afs_version:#{body['account_fraud_service_release']}"

    actions.each do |action_name, value|
      statsd_client.increment("#{action_name}.eligible", tags: tags) if value == true
    end

    statsd_client.increment("success", tags: tags)
  end

  def log_fraud_score_job_error(e, source_event)
    error_str = "FRAUD_SCORE_JOB_EXCEPTION: "
    error_str << "#{e.class.name} "
    error_str << "message: #{e.message} "
    error_str << "for #{inspect}"
    error_str << e.backtrace.join("\n") if e.backtrace.present?
    Rails.logger.error error_str

    statsd_client.increment("failure", tags: ["afs_source_event:#{source_event}"])
  end

  def log_enqueue_to_fraud_score_delta
    statsd_client.histogram(
      'trial_enqueue_delta',
      Time.now.utc - Time.parse(@params['trial_enqueue_timestamp']),
      tags: ["source_event:#{@source_event}"]
    )
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['fraud_score_job'])
  end
end
