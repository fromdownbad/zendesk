class TrialExtrasJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :high

  def self.work(account_id, _audit)
    begin
      trial_extras_hash = JSON.parse(redis_client.get(TrialExtra.redis_key(account_id)))
    rescue JSON::ParserError
      statsd_client.increment('count', tags: ["status:failure", "reason:trial_extras_missing", "account_id:#{account_id}"])
      return
    end

    begin
      acc = Account.find(account_id)
      acc.trial_extras.build(TrialExtra.build_hash(trial_extras_hash))
      acc.save!
      statsd_client.increment('count', tags: ["status:success", "account_id:#{account_id}"])
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Error writing redis trial extras to DB", fingerprint: 'cdf6b1cabfd4f110135bf656c190cfb84cf24d48')
      Rails.logger.error("Failed to write trial extras to database. Message #{e.message}.\nBacktrace: #{e.backtrace.join("\n")}")
      statsd_client.increment('count', tags: ["status:failure", "account_id:#{account_id}"])
    end
  end

  def self.args_to_log(account_id, _audit = nil)
    {
      account_id: account_id
    }
  end

  def self.statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: name.snakecase)
  end

  def self.redis_client
    Zendesk::RedisStore.client
  end
end
