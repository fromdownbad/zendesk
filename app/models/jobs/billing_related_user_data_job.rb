# requires these options to be passed:
#   account_id
class BillingRelatedUserDataJob < JobWithStatus
  priority :immediate

  HEADERS = [:id, :name, :email, :role, :is_light_agent?, :permission_set_id, :last_login, :subdomain, :account_id].freeze

  private

  def work
    total = users.size
    at(0, total, "Starting BillingRelatedUserDataJob for account #{current_account.id} / #{current_account.subdomain}")
    file = Tempfile.new(["data_export_#{current_account.id}", ".csv"])
    CSV.open(file.path, "w") do |csv|
      csv << HEADERS
      users.each_with_index do |user, idx|
        at(idx + 1, total, "Fetching user data for #{user.id}")
        csv << row_data(user)
      end
    end
    attachment = create_expirable_attachment(file)
    completed(results: {
      s3url: attachment.authenticated_s3_url,
      token: attachment.token
    })
  end

  def row_data(user)
    HEADERS.map do |header|
      scope = user
      scope = scope.account if header.to_s == "subdomain"
      scope.send(header)
    end
  end

  def users
    @users ||= current_account.users.where("roles != 0")
  end

  def create_expirable_attachment(file)
    uploaded_data = Zendesk::Attachments::CgiFileWrapper.new(file.path, filename: "#{Time.now.utc.to_i}_#{current_account.subdomain}_user_export.csv")
    attachment = ExpirableAttachment.new(uploaded_data: uploaded_data)
    attachment.account_id = current_account.id
    attachment.author_id = current_account.owner.id
    attachment.created_via = "ZendeskMonitor"
    attachment.content_type = Mime[:csv].to_s
    attachment.save!
    attachment
  ensure
    file.close
    file.unlink
  end
end
