class TicketDeflectionJob
  extend ZendeskJob::Resque::BaseJob

  priority :immediate

  def self.work(account_id, ticket_id, rule_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      new(account, ticket_id, rule_id).perform
    end
  end

  def self.args_to_log(account_id, ticket_id, rule_id)
    { account_id: account_id, ticket_id: ticket_id, rule_id: rule_id }
  end

  attr_reader :account, :ticket_id, :rule_id

  def initialize(account, ticket_id, rule_id)
    @account   = account
    @ticket_id = ticket_id
    @rule_id = rule_id
  end

  def perform
    return unless account.has_automatic_answers_enabled?

    log_stats(
      Benchmark.realtime do
        Zendesk::DB::RaceGuard.cache_locked("post-ticket-event/#{account.id}/#{ticket_id}", wait_max: 10.seconds, execute_on_timeout: true) do
          deflector = AnswerBot::Deflector.new(account)
          deflector.deflect(ticket_id: ticket_id, deflection_channel_id: ViaType.MAIL, rule_id: rule_id)
        end
      end
    )
  rescue StandardError => e
    error("Could not perform TicketDeflectionJob for account #{@account.id} ticket #{@ticket_id}", e)
  end

  private

  def log_stats(running_time)
    log("Successfully performed TicketDeflectionJob for account #{@account.id} ticket #{@ticket_id}")
    statsd_client.histogram('job_execution_time', running_time)
  rescue
    error('Could not log execution time of TicketDeflectionJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket_deflection'])
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
