require 'zendesk/resource_collection/payload_data'
require 'zendesk/resource_collection/collection_create'

class CollectionResourcesCreateJob < JobWithStatus
  priority :medium

  def name
    "CollectionResourcesCreateJob account:#{options[:account_id]} user:#{options[:user_id]}"
  end

  private

  def work
    resources = []

    ResourceCollection.transaction do
      Rails.logger.info("Running CollectionResourcesCreateJob, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")

      count = 0
      total = payload_data.all_resources.size
      at(count, total, 'Creating resources')

      resources = collection_create.create_resources do |resource|
        count += 1
        at(count, total, "Created #{resource.type} #{resource.identifier}")
      end
      resources.map! do |resource|
        {
          resource_id: resource.resource_id,
          type: resource.type,
          identifier: resource.identifier
        }
      end
    end

    completed(
      results: {
        id: collection.id,
        resources: resources
      }
    )
  rescue => e
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace)
    raise e
  end

  def collection_create
    ResourceCollection::CollectionCreate.new(collection,
      payload_data: payload_data,
      account: current_account,
      user: current_user)
  end

  def payload_data
    @payload_data ||= ResourceCollection::PayloadData.new(options[:resources])
  end

  def collection
    @collection ||= ResourceCollection.create!(account: current_account)
  end
end
