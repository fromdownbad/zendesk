class SalesforceErrorCheckingJob
  extend ZendeskJob::Resque::BaseJob

  priority :external

  def self.work(account_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      begin
        account.salesforce_integration.check_for_errors
      rescue Exception => e # rubocop:disable Lint/RescueException
        resque_warn("ERROR DURING SALESFORCE CONFIGURATION CHECK (account: #{account.id}): " + e.message)
        raise
      end
    end
  end

  def self.args_to_log(account_id)
    { account_id: account_id }
  end
end
