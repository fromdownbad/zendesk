# requires these options to be passed:
#   account_id
class GroupMembershipBulkDeleteJob < BulkDeleteJob
  priority :medium

  def name
    "GroupMembershipBulkDeleteJob account: #{options[:account_id]}"
  end

  private

  def work
    total = options[:ids].size
    Rails.logger.info("Running GroupMembershipBulkDeleteJob, ngroup_memberships: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, "Deleting Group Membership")

    results = options[:ids].each_with_index.flat_map do |id, index|
      at(index + 1, total, "Deleting Group Membership ##{index + 1}")
      if group_membership = group_membership_map[id]
        UnassignTicketsJob.enqueue(current_account.id, group_membership.group_id, group_membership.user_id, current_user.id, ViaType.GROUP_CHANGE)
      end
      delete_resource(id, group_membership, "GroupMembership")
    end

    completed(results: results)
  end

  def group_membership_map
    @group_membership_map ||= resources_map(:memberships, :id, options[:ids])
  end
end
