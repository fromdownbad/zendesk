class SupportCreationJob < JobWithStatus
  # This job runs when adding Support to an existing shell account.
  # TODO: It might not work for accounts that already had a Support product
  # prior to trial activation i.e. Support-first shell accounts.
  extend Resque::Plugins::ExponentialBackoff
  give_up_callback :report_giving_up

  priority :immediate

  def self.namespace
    name.demodulize.underscore
  end

  def self.report_giving_up(error, *args)
    Rails.logger.error("Giving up #{namespace} for #{args} due to error #{error.message}")
    statsd_client.increment(:permanent_failure, tags: ["account:#{args[0][:account_id]}"])
  end

  def self.statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: namespace)
  end

  private

  def work
    Rails.logger.info("Adding Support for account '#{current_account.idsub}'")
    current_account.on_shard do
      Zendesk::TrialActivation.activate_support_trial!(current_account, current_user)
    end
  end

  def current_account
    @current_account ||= ::Accounts::ShellAccountActivation.find(options[:account_id])
  end
end
