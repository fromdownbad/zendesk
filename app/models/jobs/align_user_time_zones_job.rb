class AlignUserTimeZonesJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium
  BATCH_SIZE = 500

  # Will set the users time zone to that of the account
  def self.work(account_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      reset_user_time_zones(account) unless account.has_time_zone_selection?
    end
  end

  def self.args_to_log(account_id)
    { account_id: account_id }
  end

  def self.reset_user_time_zones(account)
    resque_log("#{name}: Resetting user time zones for #{account.subdomain}")
    account.users.where(["time_zone IS NOT NULL AND time_zone != ?", account.time_zone]).select(:id).find_in_batches(batch_size: BATCH_SIZE) do |users|
      User.where(id: users.map(&:id)).update_all(time_zone: nil)
    end
    resque_log("#{name}: Done resetting user time zones for #{account.subdomain}")
  end
  private_class_method :reset_user_time_zones
end
