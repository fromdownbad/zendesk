class PushNotificationRegistrationJob < PushNotificationBaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend PushNotificationKillSwitch

  priority :medium

  def self.work(options)
    options = options.with_indifferent_access
    account = Account.find(options[:account_id])
    account.has_sns_push_notifications? ? process_via_amazon_sns(options) : process_via_urban_airship(options)
  end

  def self.process_via_amazon_sns(options)
    client = Zendesk::PushNotifications::Adapters::SNS.new
    operation = options[:operation]
    case operation.to_sym
    when :register
      client.register(
        mobile_app_identifier: options[:mobile_app_identifier],
        device_identifier_id: options[:identifier_id],
        device_type: options[:device_type],
        device_token: options[:token]
      )
    when :unregister
      identifier_id = options[:identifier_id]
      device_identifier = PushNotifications::DeviceIdentifier.find(identifier_id)
      client.unregister(endpoint_arn: device_identifier.sns_target_arn)
    end
  end

  def self.process_via_urban_airship(options)
    mobile_app_identifier = options[:mobile_app_identifier]
    config = urban_airship_config(mobile_app_identifier)
    client = PushNotifications::UrbanAirshipClient.new(config["app_key"], config["master_secret"])

    operation = options[:operation].to_sym
    case operation
    when :register
      identifier_id = options[:identifier_id]
      device_identifier = PushNotifications::DeviceIdentifier.find(identifier_id)
      client.register(device_identifier)
    when :unregister
      token, device_type = options.values_at(:token, :device_type)
      client.unregister(token, device_type)
    end
  end

  def self.args_to_log(options)
    operation = options[:operation]
    id_or_token = options[:identifier_id] || options[:token]
    mobile_app_identifier = options[:mobile_app_identifier]

    { device_id_or_token: id_or_token, operation: operation, mobile_app_identifier: mobile_app_identifier }
  end

  private_class_method :process_via_amazon_sns, :process_via_urban_airship
end
