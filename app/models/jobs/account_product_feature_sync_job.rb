class AccountProductFeatureSyncJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  priority :high

  def initialize(account_id, subscription_feature, plan_setting)
    @account_id = account_id
    @subscription_feature = subscription_feature
    @plan_setting = plan_setting
  end

  def self.work(account_id, subscription_feature, plan_setting, _audit = nil)
    new(account_id, subscription_feature, plan_setting).work
  end

  def self.args_to_log(account_id, subscription_feature, plan_setting, _audit = nil)
    {
      account_id: account_id,
      subscription_feature: subscription_feature,
      plan_setting: plan_setting
    }
  end

  def work
    account_service.update_product!(
      'support',
      {
        product: {
          plan_settings: {
            @plan_setting => subscription_feature_enabled?
          }
        }
      },
      context: 'product_features_change'
    )
  end

  private

  def account
    @account ||= Account.find(@account_id)
  end

  def account_service
    ::Zendesk::Accounts::Client.new(
      account,
      retry_options: {
        max: 3,
        interval: 2,
        exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
        methods: Faraday::Request::Retry::IDEMPOTENT_METHODS + [:patch]
      },
      timeout: 10
    )
  end

  def subscription_feature_enabled?
    account.subscription.send("has_#{@subscription_feature}?".to_sym)
  end
end
