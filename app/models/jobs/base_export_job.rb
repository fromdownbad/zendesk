class BaseExportJob < ThrottleJob
  def self.base_work(account, user, output, subject, filename, system = false)
    attachment = create_expirable_attachment(output, account, user, filename)
    JobsMailer.deliver_job_complete(user, identifier(user.id), subject, message(attachment.url(token: true, mapped: false), user)) unless system
  rescue ActiveRecord::RecordInvalid, ::Technoweenie::AttachmentFu::NoBackendsAvailableException
    size = File.size(output) if output
    resque_error("Unable to create attachment: requester_id: #{user.id}, csv output: #{output}, csv output size: #{size}, system: #{system} #{e.message}")
  end

  def self.args_to_log(account_id, requester_id, *args)
    { account_id: account_id, requester_id: requester_id, arguments: args }
  end

  def self.create_expirable_attachment(output, account, user, filename)
    attachment = ExpirableAttachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(output, filename: filename))
    attachment.account = account
    attachment.author = user
    attachment.created_via = name
    attachment.save!
    attachment
  end
  private_class_method :create_expirable_attachment

  def self.message(url, user)
    content = "#{I18n.t('txt.admin.models.jobs.report_feed_job.message_1', locale: user.translation_locale)}\n\n"
    content << "#{url}\n\n"
    content << I18n.t('txt.admin.models.jobs.report_feed_job.message_2', locale: user.translation_locale)
    content
  end
end
