# requires these options to be passed:
#   account_id
#   user_id
class UserBulkUpdateJobV2 < BulkJob
  priority :medium

  def name
    "UserBulkUpdateJobV2 account: #{options[:account_id]}"
  end

  private

  def delete_user(id, user)
    record_span = ZendeskAPM.trace(RECORD_SPAN_NAME, service: APM_SERVICE_NAME, resource: 'User#delete', tags: common_span_tags)
    action = :delete

    return job_error(record_span, action, id, 'UserNotFound') unless user
    return job_error(record_span, action, id, 'PermissionDenied') unless user != current_user && current_user.can?(:delete, user)
    user.current_user = current_user

    begin
      user.delete!
      span_result = { id: user.id, status: 'Deleted', action: action }
      register_span_tags(record_span, result_span_tags(span_result))
      [success, span_result]
    rescue ActiveRecord::RecordInvalid
      job_error(record_span, action, id, 'UserDeleteError', user.errors.full_messages.join(' '))
    end
  ensure
    record_span.finish
  end

  def update_user(id, user)
    record_span = ZendeskAPM.trace(RECORD_SPAN_NAME, service: APM_SERVICE_NAME, resource: 'User#edit', tags: common_span_tags)
    action = :edit

    return job_error(record_span, action, id, 'UserNotFound') unless user
    return job_error(record_span, action, id, 'PermissionDenied') unless current_user.can?(:edit, user)
    return job_error(record_span, action, id, 'OrganizationNotFound') if invalid_organization?
    user_initializer.apply(user)

    if Zendesk::SupportUsers::User.new(user).save
      span_result = { id: user.id, status: 'Updated', action: action }
      register_span_tags(record_span, result_span_tags(span_result))
      [success, span_result]
    else
      job_error(record_span, action, id, 'UserUpdateError', user.errors.full_messages.join(' '))
    end
  ensure
    record_span.finish
  end

  def job_error(span, action, id, error, details = nil)
    error_result = fail_job(id, error, details)
    span_result = error_result.merge(action: action, status: 'Failed')
    register_span_tags(span, error_span_tags(span_result))
    [error_result, span_result]
  end

  def iterate(say)
    total = options[:user_ids].size
    at(0, total, say + "s")
    options[:user_ids].each_with_index.flat_map do |id, index|
      at(index + 1, total, "#{say} ##{index + 1}")
      if options[:key] == "external_id"
        user = user_map[id.to_s.downcase]
        user.external_id = id if user
      else
        user = user_map[id]
      end
      yield id, user
    end
  end

  def work
    total = options[:user_ids].size
    job_span = ZendeskAPM.trace(JOB_SPAN_NAME, service: APM_SERVICE_NAME, resource: self.class.name, tags: common_span_tags.merge!('zendesk.total_records' => total))

    Rails.logger.info("Running UserBulkUpdateJobV2, nusers: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")

    results =
      if options[:submit_type] == 'delete'
        iterate("Deleting user") { |id, user| process_record { delete_user(id, user) } }
      else
        iterate("Updating user") { |id, user| process_record { update_user(id, user) } }
      end

    completed(results: results)
  ensure
    job_span.finish
  end

  def process_record
    started_at = current_time
    result, span_result = yield
    record_duration_metric((current_time - started_at) * 1000, span_result)
    result
  end

  def invalid_organization?
    options[:user][:organization_id].present? && organization_map.empty?
  end

  def user_map
    @user_map ||= if options[:key] == "external_id"
      resources_map_with_downcased_keys
    else
      resources_map(:users, options[:key], options[:user_ids])
    end
  end

  def user_initializer
    @user_initializer ||= Zendesk::Users::Initializer.new(current_account, current_user, user: options[:user])
  end

  def resources_map_with_downcased_keys
    resources_map(:users, options[:key], options[:user_ids]).map { |k, v| [k.to_s.downcase, v] }.to_h
  end

  def organization_map
    resources_map(:organizations, :id, options[:user][:organization_id])
  end
end
