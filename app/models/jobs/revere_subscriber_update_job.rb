require 'zendesk/revere/has_api_client'

# A user was changes in such a way that we should notify Revere

# requires these options to be passed:
#   account_id
#   user_id
class RevereSubscriberUpdateJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Zendesk::Revere::HasApiClient

  priority :low

  class << self
    def work(account_id, user_id)
      resque_log("Updating Revere subscription for user (account: #{account_id}, user: #{user_id})")
      account = Account.find(account_id)
      account.on_shard do
        user = account.users.find(user_id)
        revere_api_client.update_subscription(user, user.settings.revere_subscription)
      end
    end

    def args_to_log(account_id, user_id)
      { account_id: account_id, user_id: user_id }
    end
  end
end
