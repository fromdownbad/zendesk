class SuspendedTicketsBulkRecoveryJob < JobWithStatus
  priority :medium

  def name
    "SuspendedTicketsBulkRecoveryJob account:#{options[:account_id]}"
  end

  private

  def work
    Rails.logger.info("Running SuspendedTicketsBulkRecoveryJob, Suspended Tickets to recover: #{suspended_tickets.count(:all)}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")

    total = suspended_tickets.size
    at(0, total, 'Recovering Suspended Tickets')

    results = suspended_tickets.each_with_index.map do |st, idx|
      at(idx + 1, total, "Recovering suspended ticket #{idx + 1}")

      recovered_ticket = st.recover(current_user)

      result = if recovered_ticket
        { status: "Recovered", suspended_id: st.id, id: recovered_ticket.nice_id, account_id: current_account.id }
      else
        { status: "Failed recovering suspended ticket id:#{st.id}", errors: st.errors.full_messages.join(',') }
      end

      Rails.logger.info(result.to_s)
      result
    end

    completed(results: results)
  end

  def suspended_tickets
    @suspended_tickets || current_account.suspended_tickets.where(id: ids)
  end

  def ids
    options[:ids].split(',').map(&:to_i)
  end
end
