class GooddataEnableJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  ERROR_MESSAGE =
    'Failed to complete GoodData project re-enabling process -- %s'.freeze

  priority :immediate

  RETRY_INTERVAL = 2.seconds
  PAUSE_INTERVAL = 1.minute

  def self.work(account_id, user_id, _audit = nil)
    new(Account.find(account_id), User.find(user_id)).work
  rescue ActiveRecord::RecordNotFound => error
    record_error_with_message(error,
      "account_id: #{account_id}, user_id: #{user_id} could not be found")
  rescue GoodData::Error => error
    record_error_with_message(error, 'GoodData API error occurred')
  rescue StandardError => error
    record_error_with_message(error, "Unknown error occurred for account_id: #{account_id}, user_id #{user_id}")
  end

  def self.args_to_log(account_id, user_id, *_args)
    {account_id: account_id, user_id: user_id}
  end

  def self.record_error_with_message(error, message)
    ZendeskExceptions::Logger.record(error, location: self, message: ERROR_MESSAGE % message, fingerprint: '23d2b337f7c13415d3d18243bfb457cd6df539cb')
  end

  attr_reader :account, :user

  def initialize(account, user)
    @account = account
    @user    = user
  end

  def work
    log_run

    log_stats(
      Benchmark.realtime do
        integration_provisioning.re_enable
      end
    )
  rescue GoodData::Error::TooManyRequests
    log("Enabling of account #{account.id} in GoodData failed with " \
      'TooManyRequests error, pausing before retrying')

    sleep(PAUSE_INTERVAL) && retry
  rescue GoodData::Error::ServiceUnavailable
    log("Enabling of account #{account.id} in GoodData failed with " \
      'ServiceUnavailable error, retrying')

    sleep(RETRY_INTERVAL) && retry
  rescue
    statsd_client.increment('failed')
    raise
  end

  private

  def log_run
    log("Re-enabling account #{account.id} in GoodData")
    statsd_client.increment('count')
  rescue
    error('could not log count of GooddataEnableJob runs to statsd')
  end

  def log_stats(running_time)
    log("Successfully enabled account #{account.id} in GoodData")
    statsd_client.histogram('execution_time', running_time)
  rescue
    error('could not log execution time of GooddataEnableJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['jobs', 'gooddata_enable_job']
    )
  end

  def integration_provisioning
    @integration_provisioning ||=
      Zendesk::Gooddata::IntegrationProvisioning.new(account, admin: user)
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
