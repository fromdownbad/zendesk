module Sms
  class NotificationJob
    extend ZendeskJob::Resque::BaseJob
    extend ZendeskJob::Resque::ShardedJob

    # https://github.com/lantins/resque-retry#exp
    extend Resque::Plugins::ExponentialBackoff

    # Set TTL for the retry key to the retry delay + 1 hour
    @expire_retry_key_after = 3600

    priority :medium

    def self.work(account_id, params)
      params.symbolize_keys!
      account = Account.find(account_id)
      trigger_id = params[:trigger_id]
      api_client = Zendesk::Sms::InternalApiClient.new(account)
      event = account.events.find(params[:event_id])
      api_client.send_notification(trigger_id, event, params)

      Rails.logger.info("Sms::NotificationJob - account_id: #{account_id}, params: #{params}")
    end

    def self.args_to_log(account_id, params)
      { account_id: account_id, params: params }
    end
  end
end
