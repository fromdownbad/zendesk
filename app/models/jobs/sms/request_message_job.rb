module Sms
  class RequestMessageJob
    extend ZendeskJob::Resque::BaseJob
    extend ZendeskJob::Resque::ShardedJob

    # https://github.com/lantins/resque-retry#exp
    extend Resque::Plugins::ExponentialBackoff

    priority :medium

    def self.work(account_id, ticket_id, agent_id, outgoing_text)
      account = Account.find(account_id)
      api_client = Zendesk::Sms::InternalApiClient.new(account)
      api_client.send_request_message(ticket_id, agent_id, 'sms', outgoing_text)
      Rails.logger.info("Sms::RequestMessageJob - account_id: #{account_id}. SMS request sent for #{ticket_id}")
    end

    def self.args_to_log(account_id, ticket_id, agent_id, outgoing_text)
      { account_id: account_id, ticket_id: ticket_id, agent_id: agent_id, outgoing_text: outgoing_text }
    end
  end
end
