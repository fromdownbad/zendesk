class OrganizationReassignJob
  extend ZendeskJob::Resque::BaseJob
  class KillSwitchFlipped < StandardError; end

  REDIS_EXPIRATION = 10 * 60 # 10 minutes
  CACHE_KEY = 'OrganizationReassignJob'.freeze

  cattr_accessor :batch_read_size, :batch_write_size
  @@batch_read_size  = 10000
  @@batch_write_size = 1000

  priority :high

  class << self
    def work(account_id, organization_id, added_domains, removed_domains)
      statsd_client.increment('count')
      account = Account.find(account_id)
      account.on_shard do
        statsd_client.time('total_time') do
          bulk_update_organization_associations(
            account, organization_id, added_domains, OrganizationAssociationAddition
          )

          bulk_update_organization_associations(
            account, organization_id, removed_domains, OrganizationAssociationRemoval
          )
        end
      end
    end

    def args_to_log(account_id, organization_id, added_domains = [], removed_domains = [])
      {
        account_id: account_id,
        organization_id: organization_id,
        added_domains: added_domains,
        removed_domains: removed_domains
      }
    end

    private

    def redis_client
      @redis_client ||= Zendesk::RedisStore.client
    end

    def lock?(account_id, domain, organization_id, klass)
      @write_lock_token = SecureRandom.uuid

      got_lock = redis_client.set(
        single_execution_key(account_id, domain),
        @write_lock_token,
        nx: true,
        ex: REDIS_EXPIRATION
      )
      if got_lock
        statsd_client.increment("#{klass}.successful_lock_attempts")
      else
        statsd_client.increment("#{klass}.failed_lock_attempts")
        Rails.logger.error(message: "skipping #{klass} for #{domain} #{organization_id} due to domain lock", account_id: account.id, organization_id: organization_id, domain: domain)
      end
      got_lock
    end

    def unlock(account_id, domain)
      return unless @write_lock_token
      redis_client.watch(single_execution_key(account_id, domain)) do
        if redis_client.get(single_execution_key(account_id, domain)) == @write_lock_token
          redis_client.multi do |multi|
            multi.del(single_execution_key(account_id, domain))
          end
        else
          redis_client.unwatch
        end
      end
    rescue Redis::BaseError => e
      statsd_client.increment("unlock_error")
      Rails.logger.info "#{self.class}: #{e.message}"
    end

    def single_execution_key(account_id, domain)
      [
        CACHE_KEY,
        account_id,
        Digest::SHA1.hexdigest(domain.to_s)
      ].join('/')
    end

    def bulk_update_organization_associations(account, organization_id, domains, klass)
      domains.each do |domain|
        locked = false
        begin
          if account.has_org_reassign_synchronize_on_domain?
            next unless locked = lock?(account.id, domain, organization_id, klass)
          end
          statsd_client.time("lock_time") do
            current_batch = user_ids_in_batches_for(account, domain) do |user_ids|
              organization = statsd_client.time(klass.to_s.underscore) do
                klass.new(account.id, organization_id, user_ids).call
              end
              break(user_ids) if organization.nil? # organization was deleted
            end
            if klass == OrganizationAssociationAddition && current_batch.present?
              OrganizationAssociationRemoval.new(account.id, organization_id, current_batch).call
            end
          end
        ensure
          unlock(account.id, domain) if locked
        end
      end
    end

    def user_ids_in_batches_for(account, domain, &block)
      raise KillSwitchFlipped, 'Skipping user organization updates due to org_reassign_domain_mapping_kill_switch' if account.has_org_reassign_domain_mapping_kill_switch?

      domain = Zendesk::DB::Util.quote_for_like_clause(domain)

      user_ids = user_ids_with_domain_column_query(account, domain)
      user_ids.uniq!
      user_ids.in_groups_of(batch_write_size, false, &block)
    end

    def user_ids_with_domain_column_query(account, domain)
      user_ids = []
      ActiveRecord::Base.on_slave do
        user_identities_scope_for(account).
          where(type: "UserEmailIdentity").
          where(domain: domain).
          select(%i[id user_id]).
          find_in_batches { |batch| user_ids.concat(batch.map(&:user_id)) }
      end
      user_ids
    end

    def user_identities_scope_for(account)
      account.user_identities.verified
    end

    def statsd_client
      @statsd ||= Zendesk::StatsD::Client.new(namespace: 'organization_reassign_job')
    end
  end
end
