# requires these options to be passed:
#    account_id
#    admin_id
#    dry_run
#    onboarding_completed
require 'zip/zip'
require 'zip/zipfilesystem'

class SimplifiedEmailThreading::BaseJob < JobWithStatus
  class MissingImplementation < StandardError; end

  # Amazon SES enforces a maximum number of 50 recipients per message.
  # "A recipient is any "To", "CC", or "BCC" address."
  # https://docs.aws.amazon.com/ses/latest/DeveloperGuide/limits.html
  MAILARCHIVE_BCC_COUNT = 1;
  NOTIFICATION_RECIPIENT_BATCH_SIZE = 50 - MAILARCHIVE_BCC_COUNT
  DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S %z'.freeze

  private

  def work(*)
    raise MissingImplementation, 'Must be implemented by subclass'
  end

  def perform_work
    admin = account.admins.find(admin_id)
    email_template_updated, html_template_updated, rules_diff, additional_rules_diff = yield
    affected_rules = build_list(account, rules_diff)
    additional_affected_rules = build_list(account, additional_rules_diff) unless additional_rules_diff.nil?
    admin_locale_groups = admins_grouped_by_translation_locale(account)
    attachment_url = if affected_rules.present?
      args = {
        affected_rules: affected_rules,
        email_template_updated: email_template_updated,
        html_template_updated: html_template_updated,
        translation_locales: admin_locale_groups.keys,
        account: account,
        admin: admin
      }
      args[:additional_affected_rules] = additional_affected_rules unless additional_affected_rules.nil?
      Time.use_zone(account.time_zone) { store_rules_list(args).url(token: true, mapped: false) }
    end
    no_rule_changes = affected_rules.blank?
    notify_admins(attachment_url, admin, account, no_rule_changes, admin_locale_groups.values) if onboarding_completed?
    statsd_client.increment(:enqueued, tags: tags)
    completed(results: {url: attachment_url})
  rescue ActiveRecord::RecordInvalid, ::Technoweenie::AttachmentFu::NoBackendsAvailableException => e
    handle_error(e, affected_rules)
  end

  def tags
    ["dry_run:#{dry_run?}"].tap do |t|
      t << "onboarding_completed:#{onboarding_completed?}" unless onboarding_completed?.nil?
    end
  end

  def handle_error(error, affected_rules)
    error_message = if affected_rules
      "Unable to create attachment. " \
      "account_id: #{account_id}, admin_id: #{admin_id}, dry_run: #{dry_run?}, " \
      "affected_rules: #{affected_rules.squish.truncate(1000)}, error: #{error.class.name}"
    else
      "Failed to update or diff rules. " \
      "account_id: #{account_id}, admin_id: #{admin_id}, dry_run: #{dry_run?}, error: #{error.class.name}"
    end
    self.class.resque_error(error_message)

    raise error
  end

  def build_list(account, rules_diff)
    rules = account.rules.where(id: rules_diff)
    rules.reduce("") do |string, diff_item|
      string << "#{diff_item.title}, " \
                "#{rule_url(account.url, diff_item)}" \
                "\n"
    end
  end

  def instructions_entry(output_stream, locale, zip_entry_options)
    Zip::Entry.new(output_stream, "instructions-#{locale}.txt", *zip_entry_options)
  end

   # See https://github.com/rubyzip/rubyzip/blob/0.9.9/lib/zip/zip_entry.rb#L61-L64 for options
  def zip_entry_options(current_time)
    [
      '',                      # comment
      '',                      # extra
      0,                       # compressed_size
      0,                       # crc
      Zip::Entry::DEFLATED,    # compression_method
      0,                       # size
      current_time             # time
    ]
  end

  def notify_admins(attachment_url, admin, account, no_rule_changes, admin_locale_groups)
    admin_locale_groups.each do |admin_locale_group|
      admin_locale_group.each_slice(NOTIFICATION_RECIPIENT_BATCH_SIZE) do |admin_batch|
        translation_locale = admin_batch.first.translation_locale
        content = if no_rule_changes
          email_message_no_changes(admin.name, translation_locale)
        else
          email_message_with_instructions(
            admin.name,
            attachment_url,
            translation_locale,
            if dry_run?
              translated_instructions(translation_locale)
            end
          )
        end
        JobsMailer.deliver_job_complete_to_multiple_recipients(
          admin_batch,
          "account_id:#{account.id}:admin_id:#{admin_batch.first.id}",
          email_subject(account.name, translation_locale),
          content,
          translation_locale: translation_locale
        )
      end
    end
  end

  def create_attachment(account, admin, file, file_name)
    cgi_file = Zendesk::Attachments::CgiFileWrapper.new(file, filename: file_name)

    ExpirableAttachment.new(uploaded_data: cgi_file).tap do |attachment|
      attachment.account = account
      attachment.author = admin
      attachment.created_via = self.class.name
      attachment.save!
    end
  end

  def admins_grouped_by_translation_locale(account)
    account.admins.
      includes(:identities).
      select(&:email).
      sort_by(&:translation_locale).
      group_by(&:translation_locale)
  end

  def rule_url(host, diff_item)
    "#{host}/agent/admin/" \
    "#{diff_item.type.downcase}s/" \
    "#{diff_item.id}" \
  end

  def translated_email_template_instructions(account, translation_locale)
    if account.has_ticket_followers_allowed_enabled?
      translated_follower_email_template_instructions(translation_locale)
    elsif account.is_collaboration_enabled?
      translated_cc_email_template_instructions(translation_locale)
    end
  end

  def translated_follower_email_template_instructions(translation_locale)
    I18n.t('txt.admin.models.jobs.simplified_email_threading.email.follower_email_template_instructions', locale: translation_locale)
  end

  def translated_cc_email_template_instructions(translation_locale)
    I18n.t('txt.admin.models.jobs.simplified_email_threading.email.cc_email_template_instructions', locale: translation_locale)
  end

  def translated_html_email_template_instructions(translation_locale)
    I18n.t('txt.admin.models.jobs.simplified_email_threading.email.html_email_template_instructions', locale: translation_locale)
  end

  def account_id
    options[:account_id]
  end

  def admin_id
    options[:admin_id]
  end

  def dry_run?
    options[:dry_run]
  end

  def account
    @account ||= Account.find(account_id)
  end
end
