class SimplifiedEmailThreading::OptOutJob < SimplifiedEmailThreading::BaseJob
  priority :medium
  enforce_in_flight_limit

  private

  def work
    perform_work do
      ActiveRecord::Base.transaction(requires_new: true) do
        simplified_email_threading_opt_out = Zendesk::SimplifiedEmailThreading::OptOut.new(account_id)
        email_template_updated = simplified_email_threading_opt_out.update_email_template(dry_run: dry_run?)
        html_template_updated = simplified_email_threading_opt_out.update_html_template(dry_run: dry_run?)
        updated_rule_ids, non_updated_rule_ids = simplified_email_threading_opt_out.update_opt_out_rules(dry_run: dry_run?)

        unless dry_run?
          account.settings.simplified_email_threading = false
          account.settings.save!
        end

        [email_template_updated, html_template_updated, updated_rule_ids, non_updated_rule_ids]
      end
    end
  rescue StandardError => e
    account.settings.simplified_email_threading = true
    account.settings.save!

    raise e
  end

  def translated_primary_instructions(translation_locale)
    I18n.t(
      'txt.admin.models.jobs.simplified_email_threading.opt_out.email.instructions',
      locale: translation_locale,
      link: I18n.t(
        'txt.admin.models.jobs.simplified_email_threading.email.instructions_link',
        locale: translation_locale
      )
    )
  end

  def translated_secondary_instructions(translation_locale)
    I18n.t(
      'txt.admin.models.jobs.simplified_email_threading.opt_out.email.secondary_instructions',
      locale: translation_locale
    )
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[simplified_email_threading opt_out_job])
  end

  def file_name
    "simplified_email_threading_opt_out_affected_rules"
  end

  def store_rules_list(affected_rules:, additional_affected_rules:, email_template_updated:, html_template_updated:, translation_locales:, account:, admin:)
    zip_file = Tempfile.new([file_name, '.zip'])
    current_time = Zip::DOSTime.parse(Time.current.strftime(DATETIME_FORMAT))
    options = zip_entry_options(current_time)

    Zip::OutputStream.open(zip_file.path) do |output_stream|
      translation_locales.each do |translation_locale|
        output_stream.put_next_entry(instructions_entry(output_stream, translation_locale.locale, options))
        output_stream.puts(translated_primary_instructions(translation_locale))
        output_stream.puts(affected_rules)

        if additional_affected_rules.present?
          output_stream.puts(translated_secondary_instructions(translation_locale))
          output_stream.puts(additional_affected_rules)
        end

        if email_template_updated
          email_template_instructions = translated_email_template_instructions(account, translation_locale)
          output_stream.puts(email_template_instructions) if email_template_instructions.present?
        end

        output_stream.puts(translated_html_email_template_instructions(translation_locale)) if html_template_updated
      end
    end

    create_attachment(account, admin, zip_file.path, "#{file_name}.zip")
  ensure
    zip_file.close
    zip_file.unlink
  end

  def onboarding_completed?
    nil
  end
end
