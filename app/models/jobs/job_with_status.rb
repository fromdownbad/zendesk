require 'resque/job_with_status'

class JobWithStatus < Resque::JobWithStatus
  include JobWithStatusTracking

  # The resque-status gem is incompatible with ZendeskJob::BaseJob. Instead of
  # coupling resque-status-specific logic into BaseJob, instead all the common
  # logic has been extracted to modules so they can be used on both sides.
  #
  # Resque's hook system is to blame here. The existance of arguments can not
  # be changed from a hook, which means plugins can't inject custom state into
  # the argument list when enqueueing, and remove it before it hits the job's
  # `perform` method. The result is resque-status, and the ZendeskJob
  # Environment module need to implement their own `enqueue` and `perform`
  # methods, if they don't want to expose the job to their internals. This can
  # be mitigated by passing a Hash as your job argument (since the hooks can
  # mutate a single object), but ZendeskJob::BaseJob does not enforce this
  # requirement.
  #
  # See https://github.com/resque/resque/issues/632
  ZendeskJob::Resque::BaseJob.included_modules.each { |m| extend m }

  # This normally gets lazily included by by `priority :medium`, but we want to
  # overwrite crowd_control_job_is_sharded? immediately.
  extend Zendesk::Jobs::CrowdControl

  class << self
    attr_reader :queue

    def args_to_log(_uuid, options, *_)
      {
        account_id:         options['account_id'],
        user_id:            options['user_id'],
        raise_on_too_many?: options['raise_on_too_many?']
      }
    end

    def enqueue(options)
      enforce_in_flight_job_with_status_limit(options.with_indifferent_access) do
        super(self, options.merge(capture_environment))
      end
    end

    def perform_now(options = {})
      options[:perform_now] = true
      perform(nil, options)
    end

    def options(account, user, options = {})
      hash = {
        account_id: account.id,
        user_id: user.id
      }

      if request = options.delete(:request)
        hash[:request] = {
          user_agent: request.user_agent,
          remote_ip: request.remote_ip
        }
      end

      hash.merge!(options)
    end

    # This is a hard-coded replacement of a method that normally checks for the
    # presence of `Zendesk::Jobs::Sharding`. This is hard-coded instead because
    # is simplier than intertwining all the other `enqueue`/`perform` patches
    # done here and in resque-status.
    def crowd_control_job_is_sharded?
      Arturo.feature_enabled_for_pod?(:resque_crowd_control_job_with_status, Zendesk::Configuration.fetch(:pod_id))
    end
  end

  def initialize(uuid, options = {})
    super(uuid, options.with_indifferent_access)
  end

  def current_account
    @current_account ||= Account.with_deleted { Account.find(options[:account_id]) }
  end

  def current_user
    @current_user ||= (options[:user_id] == -1 ? User.system : current_account.users.find(options[:user_id]))
  end

  def perform
    result = nil
    current_account.on_shard do
      self.class.restore_environment(options) do
        ActiveRecord::Base.delay_touching do
          result = work
          enforce_per_account_job_status_limit
          remove_job_from_in_flight_list
        end
      end
    end
    result
  end

  def on_failure(e)
    remove_job_from_in_flight_list
    if e.is_a?(ZendeskDatabaseSupport::MappedDatabaseExceptions::MappedDatabaseException)
      self.status = [status, {'name' => name}, I18n.t("txt.error_message.job_status.database_error")]
    end
    raise e if e.is_a?(Exception)
  end

  private

  # our code reports the 'starting' state too, which can have total of 0, but if the total is 0 resque-status blows up
  def at(i, total, *args)
    if total.zero?
      super(i, 0.1, *args)
    else
      super
    end
  end
end
