require 'zendesk/resource_collection/collection_delete'

class CollectionResourcesDeleteJob < JobWithStatus
  priority :medium

  def name
    "CollectionResourcesDeleteJob account:#{options[:account_id]} user:#{options[:user_id]}"
  end

  private

  def work
    resources = []

    ResourceCollection.transaction do
      Rails.logger.info("Running CollectionResourcesDeleteJob, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")

      count = 0
      total = collection.collection_resources.count
      at(count, total, 'Deleting resources')

      resources = collection_delete.delete_resources do |resource|
        count += 1
        at(count, total, "Deleted #{resource.type} #{resource.identifier}")
      end
      resources.map! do |resource|
        {
          resource_id: resource.resource_id,
          type: resource.type,
          identifier: resource.identifier
        }
      end

      collection.soft_delete
    end

    completed(
      results: {
        id: collection.id,
        resources: resources
      }
    )
  rescue => e
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace)
    raise e
  end

  def collection_delete
    ResourceCollection::CollectionDelete.new(
      collection,
      payload_data: nil,
      account: current_account,
      user: current_user
    )
  end

  def collection
    @collection ||= ResourceCollection.find(options[:collection_id])
  end
end
