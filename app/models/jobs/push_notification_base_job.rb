class PushNotificationBaseJob
  extend ZendeskJob::Resque::BaseJob

  priority :medium

  def self.urban_airship_config(identifier)
    Zendesk::Configuration.dig!(:urban_airship, identifier)
  end

  def self.google_cloud_messaging_config(identifier)
    Zendesk::Configuration.dig!(:google_cloud_messaging, identifier)
  end
end
