class CsatCsvJob < CsvJob
  include Zendesk::CsvScrubber

  priority :medium

  throttle can_run_every: 10.minutes
  throttle disabled: true if Rails.env.development?

  BATCH_SIZE = 1000

  def file_name
    "satisfaction-ratings-#{Time.now.strftime('%Y-%m-%d')}"
  end

  def csv_header
    [
      "Requester",
      "User Id",
      "Email",
      "Ticket Id",
      "Brand",
      "Group",
      "Assignee",
      "Satisfaction"
    ] +
    (account.csat_reason_code_enabled? ? ["Reason"] : []) +
    [
      "Comment",
      "Survey Date"
    ]
  end

  def csv_data
    csv_data = []
    total = satisfaction_ratings.count(:all)
    (0..total).step(BATCH_SIZE) do |offset|
      satisfaction_ratings.limit(BATCH_SIZE).offset(offset).each do |rating|
        csv_data << csv_row(rating).map { |v| Zendesk::CsvScrubber.scrub_output(v.to_s) }
      end
    end

    csv_data
  end

  def csv_row(rating)
    [
      rating.enduser.try(:name),
      rating.enduser.try(:id),
      rating.enduser.try(:email),
      rating.ticket.try(:nice_id),
      rating.ticket.try(:brand).try(:name),
      rating.group.try(:name),
      rating.agent.try(:name),
      Api::V2::Tickets::AttributeMappings::SATISFACTION_MAP[rating.score.to_s]
    ] +
    (account.csat_reason_code_enabled? ? [reason(rating)] : []) +
    [
      rating.comment.try(:value),
      rating.created_at.iso8601
    ]
  end

  def satisfaction_ratings
    score = options.fetch('score', 'received')
    start_time = options.fetch('start_time', 60.days.ago)
    end_time = options.fetch('end_time', Time.now)

    ratings = account.satisfaction_ratings.non_deleted_with_archived

    if account.csat_reason_code_enabled?
      reason_code = options.fetch('reason_code', nil)
      ratings = ratings
      ratings = ratings.where(reason_code: reason_code) if reason_code
    end

    ratings.
      where(score: Satisfaction::Rating::SCORE_MAP.fetch(score)).
      order(created_at: :desc, id: :desc).
      newer_than(start_time).
      older_than(end_time).
      includes(:ticket, :enduser, :agent, :group, :comment).
      includes(enduser: [:identities]).
      includes(ticket: [:brand])
  end

  def reason(rating)
    rating.reason.try(:translated_value)
  end
end
