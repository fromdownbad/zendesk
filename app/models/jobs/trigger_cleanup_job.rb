class TriggerCleanupJob
  extend ZendeskJob::Resque::BaseJob
  priority :low

  class << self
    def work(account_id, trigger_id, target_id)
      account = Account.find(account_id)
      ActiveRecord::Base.on_shard(account.shard_id) do
        update_triggers(account, trigger_id, target_id)
      end
    end

    def args_to_log(account_id, trigger_id, target_id)
      { account_id: account_id, trigger_id: trigger_id, target_id: target_id }
    end

    private

    def update_triggers(account, trigger_id, target_id)
      trigger = account.triggers.find(trigger_id)

      actions = trigger.definition.actions.reject! do |action|
        action.source == 'notification_target' && action.first_value == target_id.to_s
      end

      if actions
        trigger.save
      end
    rescue ActiveRecord::RecordNotFound => e
      Rails.logger.info "TriggerCleanupJob: #{e.message}"
    end
  end
end
