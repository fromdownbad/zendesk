class UserBulkDeleteJob < JobWithStatus
  enforce_in_flight_limit

  private

  def work
    total = options[:user_ids].size
    Rails.logger.info("Running UserBulkDeleteJob, #users: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}, current_user: #{current_user.id}")
    at(0, total, 'Deleting Users')
    locale = current_user.translation_locale

    unless current_user.is_admin?
      return completed(results: [I18n.t('txt.job.user_bulk_delete_job.problem_encountered', locale: locale)])
    end

    users = current_account.users.where(id: options[:user_ids])
    failed_saves = []

    users.each_with_index do |user, index|
      begin
        at(index + 1, users.size, "Deleting user #{user.id}")
        user.current_user = current_user
        user.delete!(remove_content: !!options[:remove_content])
      rescue ActiveRecord::RecordInvalid => e
        Rails.logger.error("Failed to delete user #{user.id} #{e}")
        failed_saves << user
      rescue Exception => e # rubocop:disable Lint/RescueException
        Rails.logger.error("Failed to complete job due to #{e}")
        raise e
      end
    end

    if failed_saves.empty?
      Rails.logger.info("All users successfully deleted")
      completed(results: [])
    else
      Rails.logger.info("Not all users deleted. Errored users: #{failed_saves.map(&:id)}")
      if options[:user_ids].length == failed_saves.length
        completed(results: [I18n.t('txt.job.user_bulk_delete_job.problem_encountered', locale: locale)])
      else
        completed(results: [I18n.t('txt.job.user_bulk_delete_job.failed_for_users', count: failed_saves.length, locale: locale)])
      end
    end
  end
end
