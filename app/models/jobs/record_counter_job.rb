class RecordCounterJob < Zendesk::Jobs::Resque
  extend Zendesk::Jobs::SingleExecution

  params :record_counter_class, :account_id, :options
  @queue = :high

  def work
    Rails.cache.write(
      record_counter.cache_key,
      record_counter.cache_value,
      expires_in: 24.hours
    )
  end

  def account
    @account ||= Account.find(account_id)
  end

  def record_counter
    @record_counter ||= begin
      record_counter_class.constantize.new(
        account: account,
        options: options
      )
    end
  end

  def self.single_execution_key(*args)
    job = new(args.first.with_indifferent_access)

    ActiveRecord::Base.on_shard(job.account.shard_id) do
      job.record_counter.cache_key
    end
  end

  def self.single_execution_key_expiration
    1.minute
  end
end
