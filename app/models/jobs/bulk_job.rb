class BulkJob < JobWithStatus
  RETRY_COUNT = 3
  RETRY_INTERVAL = 3
  APM_SERVICE_NAME = 'classic'.freeze
  JOB_SPAN_NAME = 'bulk_job'.freeze
  RECORD_SPAN_NAME = 'bulk_job.record'.freeze

  enforce_in_flight_limit

  def with_retry(retry_count, retry_interval, index, instance, failure_description)
    retry_count.times do |_i|
      begin
        return yield
      rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::MappedDatabaseException => e
        Rails.logger.info("Retrying #{self.class.name} exception: #{e.message}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
        sleep(retry_interval)
      end
    end
    fail_job(index, failure_description, instance.errors.full_messages.join(','))
  end

  def fail_job(id, error, details = nil)
    result = { id: id, error: error }
    result[:details] = details if details
    result
  end

  def success
    []
  end

  def resources(association, key, ids)
    current_account.send(association).where(key => ids)
  end

  def resources_map(association, key, ids)
    resources(association, key, ids).each_with_object({}) do |resource, hash|
      hash[resource.send(key)] = resource
    end
  end

  def current_time
    Process.clock_gettime(Process::CLOCK_MONOTONIC)
  end

  def common_span_tags
    {
      Datadog::Ext::Analytics::TAG_ENABLED => true,
      'zendesk.account_id' => current_account.id,
      'zendesk.account_subdomain' => current_account.subdomain,
      'zendesk.class' => self.class.name,
      'zendesk.shard_id' => current_account.shard_id
    }
  end

  def result_span_tags(result)
    span_tags = { 'zendesk.status' => result[:status] }
    span_tags['zendesk.id'] = result[:id] if result[:id]
    span_tags['zendesk.external_id'] = result[:external_id] if result[:external_id]
    span_tags['zendesk.action'] = result[:action] if result[:action]
    span_tags
  end

  def error_span_tags(result)
    span_tags = result_span_tags(result)
    span_tags['zendesk.error'] = result[:errors] || result[:error]
    span_tags['zendesk.details'] = result[:details] if result[:details]
    span_tags
  end

  def record_duration_metric(duration, result)
    tags = result.slice(:action, :error, :status).map { |k, v| "#{k}:#{v}" }
    statsd_client.histogram('record.execution_time', duration, tags: tags)
  end

  # TODO: Replace setting individual set_tag with bulk `set_tags` once we upgrade ddtrace gem.
  def register_span_tags(span, tags)
    tags.each { |k, v| span.set_tag(k, v) }
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['classic', 'resque', 'bulk_job', self.class.name])
  end
end
