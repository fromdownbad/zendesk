# Whenever a plan change happens, update and persist in db the associated
# account's features.
#
class PersistFeatureBitsJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :low

  class << self
    def args_to_log(account_id)
      { account_id: account_id }
    end

    def work(account_id)
      new(account_id).work
    end
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def work
    if current_subscription
      update_subscription_features!
    end
  end

  private

  def account
    @account ||= Account.find(@account_id)
  end

  def current_subscription
    @current_subscription ||= account.subscription
  end

  def update_subscription_features!
    Zendesk::Features::SubscriptionFeatureService.new(
      account
    ).execute!
  end
end
