require 'digest/md5'

class FetchGoogleProfileImageJob < FetchProfileImageJob
  priority :high

  # Based on unsized default image url
  DEFAULT_IMAGE_URL_DIGEST = "f741f6fb93497e342bf3d623673d040b".freeze

  def self.valid_photo?(status, file_data, headers)
    super && Digest::MD5.hexdigest(file_data) != DEFAULT_IMAGE_URL_DIGEST
  end
end
