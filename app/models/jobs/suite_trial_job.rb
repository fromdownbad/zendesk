class SuiteTrialJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :high
  self.job_timeout = 2.minutes

  class << self
    def work(account_id, _audit = nil)
      account = account(account_id)

      initialize_shared_records(account)
      create_talk_trial(account) unless account.has_voice_disable_subscription_notifications?
    end

    def args_to_log(account_id, audit = nil)
      { account_id: account_id, audit_id: audit_id(audit) }
    end

    # 0, 1, 4, 9, 16, 25, 36, 49, etc. minutes
    # called from QueueAudit
    def delay(enqueue_count)
      (enqueue_count**2).minutes
    end

    private

    def account(id)
      # the job might run before the account is replicated to the slave
      Account.on_master { Account.find(id) }
    end

    def initialize_shared_records(account)
      Zendesk::SuiteTrial.new(account).activate
    end

    def create_talk_trial(account)
      Zendesk::Voice::InternalApiClient.new(account.subdomain).create_trial
    end
  end
end
