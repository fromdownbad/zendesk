require 'zendesk/google/gam_client'

class GAMEmailForwardingJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  MAX_RETRIES = 10

  def self.work(account_id, user_id, access_token, domain, username, forward_from_email, forward_to_email)
    client_api_call(access_token, domain, username, forward_from_email, forward_to_email)
    account = Account.find(account_id)
    user = User.find(user_id)

    # Mark the earlier created recipient address as default now that forwarding is complete
    recipient_address = account.recipient_addresses.find_by_email(forward_from_email)
    recipient_address.creating_for_new_brand = true
    recipient_address.default = true
    recipient_address.save!

    # Create a sample ticket by emailing the user's forward_from email
    EmailForwardingMailer.deliver_forwarding_complete_notice(user, forward_from_email)

    # Reply to earlier ticket and solve it to notify user that forwarding is complete
    ticket = account.tickets.where(subject: I18n.t('txt.zero_states.email_forward_pending_ticket.subject'),
                                   via_id: ViaType.SAMPLE_TICKET).first
    if ticket
      ticket.add_reply_with_template(template_name: 'email_forward_complete_comment')
      ticket.solve(ticket.assignee)
    end

    statsd_client.increment('count', tags: ["status:success"])
  rescue => e
    error_message = "Failed to setup email forwarding for GAM account."
    ZendeskExceptions::Logger.record(e, location: self, message: error_message, fingerprint: 'bed1be4348ab8eee59ec75a93ffaf56cf9f4f344')
    raise e
  end

  def self.args_to_log(account_id, user_id, access_token, domain, username, forward_from_email, forward_to_email)
    {
      account_id: account_id,
      user_id: user_id,
      access_token: access_token,
      domain: domain,
      username: username,
      forward_from_email: forward_from_email,
      forward_to_email: forward_to_email
    }
  end

  def self.client_api_call(access_token, domain, username, forward_from_email, forward_to_email, num_retries = 0)
    gam_client ||= Zendesk::Google::GAMClient.new(
      access_token: access_token,
      domain: domain
    )

    response = gam_client.create_email_forwarding_filter(
      email_owner_username: username,
      forward_from_email: forward_from_email,
      forward_to_email: forward_to_email
    )
    # Confirm forward to email was updated
    if response && response[:property]
      response_forward_to = response[:property].select { |item| item[:name] == "forwardTo" }.first[:value]
      raise "Incorrect forward to email in response to creating email forwarding filter" if response_forward_to != forward_to_email
    else
      if num_retries < MAX_RETRIES
        sleep(2.minutes)
        num_retries += 1
        client_api_call(access_token, domain, username, forward_from_email, forward_to_email, num_retries)
      else
        statsd_client.increment('count', tags: ["status:error", "domain:#{domain}", "type:filter_creation"])
        raise "Error received when trying to create email forwarding filter. Response: #{response}"
      end
    end
  end

  def self.statsd_client
    @client ||= Zendesk::StatsD.client(namespace: name.snakecase)
  end
end
