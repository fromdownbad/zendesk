class RspamdFeedbackJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  def self.report_ham(user:, identifier:, recipient:, classifier: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_CLASSIFIER_PERUSER, backend: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_BACKEND)
    enqueue(
      backend:        backend,
      classification: Zendesk::InboundMail::RspamdSpamScore::HAM,
      classifier:     classifier,
      account_id:     user.account_id,
      user_id:        user.id,
      identifier:     identifier,
      recipient:      recipient
    )
  end

  def self.report_spam(user:, identifier:, recipient:, classifier: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_CLASSIFIER_PERUSER, backend: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_BACKEND)
    enqueue(
      backend:        backend,
      classification: Zendesk::InboundMail::RspamdSpamScore::SPAM,
      classifier:     classifier,
      account_id:     user.account_id,
      user_id:        user.id,
      identifier:     identifier,
      recipient:      recipient
    )
  end

  def self.work(params)
    new(params).work
  end

  def self.args_to_log(args)
    args
  end

  attr_reader :params

  def initialize(params)
    @params = params.with_indifferent_access
  end

  def work
    return if analysis_unavailable
    Zendesk::InboundMail::RspamdTrain.new(
      raw_email:      raw_email,
      classification: params[:classification],
      recipient:      params[:recipient],
      classifier:     params[:classifier],
      backend:        params[:backend]
    ).call
  end

  protected

  def analysis_unavailable
    return true if raw_email_blank?
    return true if analysis_header_blank?
    statsd_client.increment("agree.#{params[:classification]}") if user_agrees_with_rspamd?
    log("Training Rspamd BAYES classifier with #{params[:identifier]} as #{params[:classification]}")
    false
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'rspamd.training')
  end

  def raw_email_blank?
    if raw_email.blank?
      log('missing raw email. skipping.')
      true
    end
  end

  def analysis_header_blank?
    if analysis_header.blank?
      log('missing x-rspamd-status header. skipping.')
      true
    end
  end

  def user_agrees_with_rspamd?
    if params[:classification] == classification
      log('User agrees with Rspamd. Train anyway because we train BAYES classifier.')
      true
    end
  end

  def user
    @user ||= account.users.find(params[:user_id])
  end

  def account
    @account ||= Account.find(params[:account_id])
  end

  def classification
    @classification ||= spam_score.classification
  end

  def spam_score
    @spam_score ||= Zendesk::InboundMail::RspamdSpamScore.new(message)
  end

  def analysis_header
    return if message.blank?
    message.headers['X-Rspamd-Status'].try(:first)
  end

  def message
    return if raw_email.blank?
    raw_email.message
  end

  def raw_email
    @raw_email ||= Zendesk::Mailer::RawEmailAccess.new(params[:identifier])
  end

  def log(message)
    self.class.resque_log(message)
  end
end
