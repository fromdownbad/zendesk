class PushNotificationSdkSendJob < PushNotificationBaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  def self.work(options)
    device_ids, payload, mobile_sdk_app_id, account_id = options.with_indifferent_access.values_at(
      'device_ids', 'payload', 'mobile_sdk_app_id', 'account_id'
    )

    account = Account.find(account_id)
    mobile_sdk_app = account.mobile_sdk_apps.find(mobile_sdk_app_id)
    if mobile_sdk_app.settings.push_notifications_enabled
      case mobile_sdk_app.settings.push_notifications_type
      when MobileSdkApp::PUSH_NOTIF_TYPE_WEBHOOK
        push_via_webhook(mobile_sdk_app.settings.push_notifications_callback, device_ids, payload, account)
      when MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP
        push_via_urban_airship(
          mobile_sdk_app.settings.urban_airship_key, mobile_sdk_app.settings.urban_airship_master_secret,
          device_ids, payload, account
        )
      when nil
        if mobile_sdk_app.settings.push_notifications_enabled
          push_via_webhook(mobile_sdk_app.settings.push_notifications_callback, device_ids, payload, account)
        end
      end
    else
      Rails.logger.info "Mobile SDK App ##{mobile_sdk_app.id} has push notifications disabled"
    end
  end

  def self.push_via_webhook(webhook_url, device_ids, payload, _account)
    client = PushNotifications::WebhookClient.new(webhook_url)

    response = client.push(device_ids, payload)
    if response && !response.success?
      Rails.logger.error("SDK callback device push failed: #{response.code} -- #{response.body}")
    end
  end

  def self.push_via_urban_airship(app_key, app_secret, device_ids, payload, _account)
    client = PushNotifications::UrbanAirshipClient.new(
      app_key, app_secret, statsd_tags: ["name:sdk_send"]
    )

    responses = client.push(
      device_ids,
      payload,
      enable_collapse_key: true
    )

    responses.each do |response|
      next if response.success?
      token = SecureRandom.hex.first(16)
      Rails.logger.error("[#{token}] SDK Urban Airship Response: #{response.code} - #{response.body} - #{response.inspect}")
      Rails.logger.error("[#{token}] SDK Urban Airship device push failed: #{device_ids.inspect} -- #{payload.inspect}")
    end
  end

  def self.args_to_log(options)
    options.with_indifferent_access.slice('device_ids', 'payload', 'mobile_sdk_app_id')
  end
end
