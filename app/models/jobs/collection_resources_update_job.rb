require 'zendesk/resource_collection/payload_data'
require 'zendesk/resource_collection/collection_update'
require 'zendesk/resource_collection/collection_delete'

class CollectionResourcesUpdateJob < JobWithStatus
  priority :medium

  def name
    "CollectionResourcesUpdateJob account:#{options[:account_id]} user:#{options[:user_id]}"
  end

  private

  def work
    resources = []

    ResourceCollection.transaction do
      Rails.logger.info("Running CollectionResourcesUpdateJob, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")

      count = 0
      total = payload_data.all_resources.size
      at(count, total, 'Updating resources')

      resources = collection_update.update_resources do |resource|
        count += 1
        at(count, total, "Updated #{resource.type} #{resource.identifier}")
      end
      resources.map! do |resource|
        {
          resource_id: resource.resource_id,
          type: resource.type,
          identifier: resource.identifier
        }
      end

      collection_delete.cleanup_resources
    end

    completed(
      results: {
        id: collection.id,
        resources: resources
      }
    )
  rescue => e
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace)
    raise e
  end

  def payload_data
    @payload_data ||= ResourceCollection::PayloadData.new(options[:resources])
  end

  def collection
    @collection ||= ResourceCollection.find(options[:collection_id])
  end

  def collection_update
    ResourceCollection::CollectionUpdate.new(collection,
      payload_data: payload_data,
      account: current_account,
      user: current_user)
  end

  def collection_delete
    ResourceCollection::CollectionDelete.new(
      collection,
      payload_data: payload_data,
      account: current_account,
      user: current_user
    )
  end
end
