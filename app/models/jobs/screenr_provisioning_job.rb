# Try to provide a Screenr's Tenant for a Zendesk's Account.
# If something goes wrong, it interpreters the exceptions and return errors.
#
# account_id - The Account's id to which a Screenr account should be provisioned to.
# user_id    - The User's id who is provisioning the account.
# domain     - The String subdomain to use for the Screenr account.
# scope      - The scope where we are provisioning from, can be tickets or dropboxes
#
# Note: we are not sending user's information to Screenr's API but
# we log on our side which user has requested the provisioning.
class ScreenrProvisioningJob < JobWithStatus
  include Zendesk::Tickets::ClientInfo
  priority :immediate

  def name
    "ScreenrProvisioningJob account:#{options[:account_id]} " \
                            "user:#{options[:user_id]} " \
                            "domain:#{options[:domain]} " \
                            "scope:#{options[:scope]}"
  end

  private

  def work
    domain = options[:domain]

    # We force activation of Screencasts for Tickets only if the
    # provisioning request is coming from the ticket settings.
    activate_screencasts_for_tickets = options[:scope] == 'tickets'

    begin
      current_account.screenr_integration.provide_tenant(domain, activate_screencasts_for_tickets)
      result = {
        api_status: 'provisioned'
      }
    rescue Screenr::BusinessPartnerClient::DomainAlreadyInUse => e
      result = {
        api_status: 'domain_in_use',
        alternative_domains: e.available_domains
      }
    rescue Screenr::BusinessPartnerClient::ValidationFailed => e
      result = {
        api_status: 'validation_failed',
        message: e.first_error_messages,
        error_on: e.first_error_on
      }
    end
    completed(results: result)
  end
end
