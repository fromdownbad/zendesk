require 'zendesk/google/gam_client'

class GsuiteDeleteDomainAliasJob
  extend ZendeskJob::Resque::BaseJob

  priority :low

  def self.work(access_token, domain, customer_id, domain_alias)
    return if Rails.env.production?
    client_api_call(access_token, domain, customer_id, domain_alias)
  end

  def self.args_to_log(access_token, domain, customer_id, domain_alias)
    {
      access_token: access_token,
      domain: domain,
      customer_id: customer_id,
      domain_alias: domain_alias
    }
  end

  def self.client_api_call(access_token, domain, customer_id, domain_alias)
    gam_client ||= Zendesk::Google::GAMClient.new(
      access_token: access_token,
      domain: domain
    )
    gam_client.delete_domain_alias(customer_id: customer_id, domain_alias: domain_alias)
  end
end
