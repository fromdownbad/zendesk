class DowngradeOrganizationsAccessJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium
  BATCH_SIZE = 500

  def self.work(account_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      downgrade_agent_organizations_access(account)
      downgrade_end_user_organizations_access(account)
    end
  end

  def self.args_to_log(account_id)
    { account_id: account_id }
  end

  # Agents whose access is set to "Tickets in agent's organizations" will change to "Assigned tickets only"
  def self.downgrade_agent_organizations_access(account)
    resque_log("#{name}: Changing organization access on agents for #{account.subdomain}")
    account.
      agents.
      where.not(restriction_id: nil).
      where(restriction_id: RoleRestrictionType.ORGANIZATION).
      select(:id).
      find_in_batches(batch_size: BATCH_SIZE) do |agents|
        account.agents.where(id: agents.map(&:id)).update_all(restriction_id: RoleRestrictionType.ASSIGNED)
      end
    resque_log("#{name}: Done changing organization access on agents for #{account.subdomain}")
  end

  # End-users whose access is set to "Tickets from users's org." will change to "Tickets requested by user"
  def self.downgrade_end_user_organizations_access(account)
    resque_log("#{name}: Changing organization access on end users for #{account.subdomain}")
    account.
      end_users.
      where.not(restriction_id: nil).
      where(restriction_id: RoleRestrictionType.ORGANIZATION).
      select(:id).
      find_in_batches(batch_size: BATCH_SIZE) do |end_users|
        account.end_users.where(id: end_users.map(&:id)).update_all(restriction_id: RoleRestrictionType.REQUESTED)
      end
    resque_log("#{name}: Done changing organization access on end users for #{account.subdomain}")
  end

  private_class_method :downgrade_agent_organizations_access
  private_class_method :downgrade_end_user_organizations_access
end
