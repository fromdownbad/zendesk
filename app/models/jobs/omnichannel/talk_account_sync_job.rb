module Omnichannel
  class TalkAccountSyncJob < BaseJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id

    def work
      account.on_shard do
        talk_product = account.products.find { |p| p.name == Zendesk::Accounts::Client::TALK_PRODUCT.to_sym }
        return unless account.has_ocp_talk_account_sync? && talk_product

        talk_lite_or_legacy = [3, 9].include?(talk_product.plan_settings['plan_type'])
        return if !talk_lite_or_legacy && account.has_central_admin_staff_mgmt_roles_tab?

        account.agents.each do |agent|
          Omnichannel::TalkEntitlementSyncJob.new(account_id: account_id, user_id: agent.id).work
        end
      end
    end
  end
end
