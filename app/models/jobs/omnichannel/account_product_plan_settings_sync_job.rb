module Omnichannel
  class AccountProductPlanSettingsSyncJob < BaseJob
    @fatal_exceptions = [Kragle::BadRequest]
    @queue = :high
    params :account_id

    def work
      product = Zendesk::SupportAccounts::Product.retrieve(account)
      return if product.nil?
      accounts_client.update_product!(
        Zendesk::Accounts::Client::SUPPORT_PRODUCT,
        { product: { plan_settings: product.plan_settings } },
        context: 'account_product_plan_settings_sync_job'
      )
    end

    private

    def accounts_client
      Zendesk::Accounts::Client.new(account)
    end
  end
end
