module Omnichannel
  class BaseEntitlementSyncJob < BaseJob
    def work
      return unless user.is_active?

      account.on_shard do
        if account.products.map(&:name).include?(product_name.to_sym)
          return if entitlement.no_available_role? && existing_entitlement.nil?

          staff_client.update_full_entitlements!(user_id, entitlements_payload, is_end_user: user.is_end_user?)
        else
          Rails.logger.info("Skipping #{product_name} agent entitlements sync for user #{user_id} due to no active #{product_name.capitalize} product")
          self.class.statsd_client.increment(:product_inactive)
        end
      end
    rescue Kragle::Gone
      Rails.logger.info("Skipping #{product_name} agent entitlements sync for deleted user: #{user_id}")
      self.class.statsd_client.increment(:user_deleted)
    end

    def existing_entitlement
      staff_client.get_entitlements!(user.id)[product_name]
    end

    def staff_client
      @staff_client ||= Zendesk::StaffClient.new(account, audit_headers: audit_headers)
    end

    def product_name
      raise 'Undefined Product'
    end

    def entitlement
      @entitlement ||= Zendesk::SupportUsers::Entitlement.for(user, product_name)
    end

    def entitlements_payload
      { product_name => entitlement.payload }
    end

    def user
      @user ||= User.find(user_id)
    end
  end
end
