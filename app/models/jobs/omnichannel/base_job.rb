module Omnichannel
  class BaseJob < Zendesk::Jobs::Resque
    class Zendesk::ProductNotReady < StandardError; end
    extend Resque::Plugins::ExponentialBackoff
    give_up_callback :report_giving_up

    def self.namespace
      name.demodulize.underscore
    end

    def self.report_giving_up(error, *args)
      Rails.logger.error("Giving up #{namespace} for #{args} due to error #{error.message}")
      statsd_client.increment(:permanent_failure)
    end

    def self.statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: namespace)
    end
  end
end
