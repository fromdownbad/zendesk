module Omnichannel
  class GuideAccountSyncJob < BaseJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id

    def work
      account.on_shard do
        return unless account.has_ocp_guide_account_sync? && account.products(use_cache: false).map(&:name).include?(Zendesk::Accounts::Client::GUIDE_PRODUCT.to_sym)
        account.agents.each do |agent|
          Omnichannel::GuideEntitlementSyncJob.new(account_id: account_id, user_id: agent.id).work
        end
      end
    end
  end
end
