module Omnichannel
  class AccountRolesSyncJob < BaseJob
    @queue = :high
    params :account_id

    def work
      return unless Zendesk::SupportAccounts::Account.new(record: account).role_sync_enabled?

      Zendesk::SupportAccounts::Role.for_account(account_id).each do |role|
        role.sync!(context: 'account_roles_sync_job')
      end
    end
  end
end
