module Omnichannel
  class AccountUsersEntitlementsSyncJob < BaseJob
    BATCH_SIZE = 200

    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id

    def work
      return unless Zendesk::SupportAccounts::Account.new(record: account).entitlements_sync_enabled?

      account.on_shard do
        users_scope = account.multiproduct? ? account.users : account.agents
        users_scope.each_slice(BATCH_SIZE) do |users|
          users.each do |u|
            Zendesk::SupportUsers::EntitlementSynchronizer.perform(u)
          end
        end
      end
    rescue Kragle::UnprocessableEntity => e
      if e.message.include? 'Entitlements Product'
        raise Zendesk::ProductNotReady
      else
        raise e
      end
    end
  end
end
