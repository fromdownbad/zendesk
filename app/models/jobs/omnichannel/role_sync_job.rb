module Omnichannel
  class RoleSyncJob < BaseJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id, :permission_set_id, :role_name, :context, :permissions_changed, :enabled

    def work
      return unless Zendesk::SupportAccounts::Account.new(record: account).role_sync_enabled?

      Zendesk::SupportAccounts::RoleFactory.retrieve(
        account: account, permission_set_id: permission_set_id, role_name: role_name
      ).sync!(context: context, permissions_changed: permissions_changed || [], enabled: enabled)
    end
  end
end
