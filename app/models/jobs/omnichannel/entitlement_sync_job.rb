module Omnichannel
  class EntitlementSyncJob < BaseJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id, :user_id, :audit_headers

    def work
      return unless Zendesk::SupportAccounts::Account.new(record: account).entitlements_sync_enabled?

      account.on_shard do
        user = Zendesk::SupportUsers::User.retrieve(user_id)
        Zendesk::SupportUsers::EntitlementSynchronizer.perform(user.user_model)
      end
    rescue Kragle::UnprocessableEntity => e
      if e.message.include? 'Entitlements Product'
        raise Zendesk::ProductNotReady
      else
        raise e
      end
    end
  end
end
