module Omnichannel
  class ExploreAccountSyncJob < BaseJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id

    def work
      account.on_shard do
        return unless account.products.map(&:name).include?(Zendesk::Accounts::Client::EXPLORE_PRODUCT.to_sym)
        account.light_agents.each do |agent|
          Omnichannel::ExploreEntitlementSyncJob.new(account_id: account_id, user_id: agent.id).work
        end
        non_light_agents = account.agents - account.light_agents
        non_light_agents.each do |agent|
          Omnichannel::ExploreEntitlementSyncJob.new(account_id: account_id, user_id: agent.id).work
        end
      end
    end
  end
end
