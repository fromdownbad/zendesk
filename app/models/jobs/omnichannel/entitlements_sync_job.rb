module Omnichannel
  class EntitlementsSyncJob < BaseJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id, :user_id, :products, :audit_headers

    def work
      return if products.empty? && !user.is_active?

      account.on_shard do
        entitlements = {}
        if user.is_end_user?
          existing_entitlements.each_key { |product| entitlements[product] = nil }
        else
          products.each do |product_name|
            if account.products.map(&:name).include?(product_name.to_sym)
              entitlement = Zendesk::SupportUsers::Entitlement.for(user, product_name)
              next if entitlement.no_available_role? && existing_entitlements[product_name].nil?

              entitlements[product_name] = entitlement.payload
            else
              Rails.logger.info("Skipping #{product_name} agent entitlements sync for user #{user_id} due to no active #{product_name.capitalize} product")
              self.class.statsd_client.increment(:product_inactive)
            end
          end

          return if entitlements.keys.empty?
        end
        logger.info("Performing entitlement sync, account_id: #{account.id}, user: #{user_id}, payload: #{entitlements}")
        staff_client.update_full_entitlements!(user_id, entitlements, is_end_user: user.is_end_user?)
      end
    rescue Kragle::Gone
      Rails.logger.info("Skipping agent entitlements sync for deleted user: #{user_id}")
      self.class.statsd_client.increment(:user_deleted)
    end

    private

    def staff_client
      @staff_client ||= Zendesk::StaffClient.new(account, audit_headers: audit_headers)
    end

    def existing_entitlements
      @existing_entitlements ||= staff_client.get_entitlements!(user.id, user.is_end_user?)
    end

    def user
      @user ||= User.find(user_id)
    end
  end
end
