module Omnichannel
  class ExploreEntitlementSyncJob < BaseEntitlementSyncJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id, :user_id, :modify_is_active, :audit_headers

    def product_name
      Zendesk::Accounts::Client::EXPLORE_PRODUCT
    end

    def entitlements_payload
      { product_name => entitlement.payload(modify_is_active != false) }
    end
  end
end
