module Omnichannel
  class AccountProductSyncJob < BaseJob
    @fatal_exceptions = [Kragle::BadRequest]
    @queue = :high
    params :account_id

    def work
      product = Zendesk::SupportAccounts::Product.retrieve(account)
      return if product.nil?
      accounts_client.update_product!(Zendesk::Accounts::Client::SUPPORT_PRODUCT, { product: product.to_h }, context: 'account_product_sync_job', include_deleted_account: true)
    rescue Kragle::ResourceNotFound => e
      if e.message.include? 'Could not find product support for account'
        raise Zendesk::ProductNotReady
      else
        raise e
      end
    end

    private

    def accounts_client
      Zendesk::Accounts::Client.new(account)
    end
  end
end
