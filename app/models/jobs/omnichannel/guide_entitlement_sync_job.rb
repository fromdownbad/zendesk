module Omnichannel
  class GuideEntitlementSyncJob < BaseEntitlementSyncJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id, :user_id, :modify_is_active, :audit_headers

    def product_name
      Zendesk::Accounts::Client::GUIDE_PRODUCT
    end

    def entitlements_payload
      { product_name => entitlement.payload(modify_is_active != false) }
    end
  end
end
