module Omnichannel
  class TalkEntitlementSyncJob < BaseEntitlementSyncJob
    @queue = :high
    @fatal_exceptions = [Kragle::BadRequest]
    params :account_id, :user_id, :audit_headers

    def product_name
      Zendesk::Accounts::Client::TALK_PRODUCT
    end
  end
end
