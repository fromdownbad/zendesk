class TicketAttributeValuesSetterJob
  extend ZendeskJob::Resque::BaseJob

  priority :high

  def self.work(account_id, options)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      new(account, options).save_attribute_values
    end
  end

  def self.args_to_log(account_id, options)
    options.symbolize_keys!

    {
      account_id: account_id,
      ticket_id: options[:ticket_id],
      attribute_value_ids: options[:attribute_value_ids],
      audit_id: options[:audit_id]
    }
  end

  def initialize(account, options)
    options.symbolize_keys!

    @account = account
    @ticket_id = options[:ticket_id]
    @attribute_value_ids = options[:attribute_value_ids]
    @audit_id = options[:audit_id]
  end

  def save_attribute_values
    log("Saving attribute value IDs for ticket #{@ticket_id} on account #{@account.id}")
    ticket = @account.tickets.find(@ticket_id)
    audit = ticket.audits.find(@audit_id)
    ticket.set_attribute_values(@attribute_value_ids, audit)
  rescue StandardError => e
    error("Failed to save attribute value IDs for ticket #{@ticket_id} on account #{@account.id}: #{e}")
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
