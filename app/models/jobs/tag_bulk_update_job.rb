# requires these options to be passed:
#   account_id
#   user_id
#   tag_name
class TagBulkUpdateJob < JobWithStatus
  include Zendesk::Tickets::ClientInfo

  priority :immediate

  def name
    "TagBulkUpdateJob account:#{options[:account_id]} user:#{options[:user_id]}"
  end

  def tag_name
    options[:tag_name]
  end

  def with_tag_conditions(scope, writable: false)
    s = scope.joins(:taggings).where(taggings: { tag_name: tag_name })
    s = s.readonly(false) if writable
    s
  end

  def tickets_chain
    current_account.tickets.not_closed
  end

  def entries_chain
    current_account.entries
  end

  def total_tickets
    with_tag_conditions(tickets_chain).count(:all)
  end

  def total_entries
    with_tag_conditions(entries_chain).count(:all)
  end

  private

  def work
    results = if current_account.taggings.exists?(tag_name: tag_name)
      total = total_tickets + total_entries
      at(0, total, 'Looking up tickets')

      round = 1

      ticket_count = 0
      with_tag_conditions(tickets_chain, writable: true).find_each do |ticket|
        at(round, total, "Updating ticket ##{ticket.nice_id}")

        ticket.remove_tags = tag_name
        ticket.will_be_saved_by(current_user)
        ticket.audit.disable_triggers = true
        ticket.save

        ticket_count += 1
        round += 1
      end

      entry_count = 0
      with_tag_conditions(entries_chain, writable: true).find_each do |entry|
        at(round, total, "Updating entry ##{entry.id}")

        entry.remove_tags = tag_name
        entry.save

        entry_count += 1
        round += 1
      end

      TagScore.update(current_account, tag_name)

      {
        ticket_count: ticket_count,
        entry_count: entry_count,
        total: round - 1
      }
    else
      {
        ticket_count: 0,
        entry_count: 0,
        total: 0,
        message: "No tickets or topics found with tag \"#{tag_name}\""
      }
    end

    results[:tag_name] = tag_name

    completed(results: results)
  end
end
