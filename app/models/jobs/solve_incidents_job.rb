class SolveIncidentsJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium

  def self.work(account_id, user_id, options)
    account = Account.find(account_id)
    options = options.with_indifferent_access

    account.on_shard do
      user    = find_user(account, user_id)
      problem = find_problem(account, options)

      if problem.blank?
        raise "SolveIncidentsJob: Cannot process a non-existent problem: #{options[:problem].inspect}"
      end

      return solve_linked_incidents(account, user, problem, options)
    end
  end

  def self.args_to_log(account_id, user_id, options)
    { account_id: account_id, user_id: user_id, options: options }
  end

  def self.find_problem(account, options)
    if options[:problem].is_a?(Ticket)
      options[:problem]
    else
      account.tickets.find_by_id(options[:problem])
    end
  end

  def self.find_user(account, user_id)
    if user_id == User.system_user_id
      User.system
    else
      account.users.find_by_id(user_id)
    end
  end

  # Solve all linked working incidents for this problem. If an incident is
  # incompletely assigned, it inherits values from the problem.
  def self.solve_linked_incidents(_account, user, problem, options)
    summary = []

    working_problem_incidents = problem.incidents.working
    resque_log("About to work on #{working_problem_incidents.count(:all)} incidents")

    working_problem_incidents.each do |incident|
      if incident.assignee_id.blank?
        incident.group_id    = problem.group_id
        incident.assignee_id = problem.assignee_id
      end

      incident.status_id = StatusType.SOLVED

      if options[:value].present?
        incident.comment = Comment.new(
          ticket: incident, body: options[:value],
          via_id: ViaType.LINKED_PROBLEM, is_public: !!options[:is_public],
          via_reference_id: problem.id,
          format: options[:format] || problem.comments.last.format
        )
      end

      if options[:attachment_ids].present?
        options[:attachment_ids].each do |id|
          attachment = Attachment.from_attachment(Attachment.find(id))
          incident.comment.attachments << attachment if attachment
        end
      end

      incident.will_be_saved_by(user, via_id: ViaType.LINKED_PROBLEM, via_reference_id: problem.id)
      result = incident.save

      if incident.errors.any?
        resque_log("ERROR #{name} Error solving linked incident #{incident.id}: #{incident.errors.full_messages.join(", ")}")
      end

      incident_summary = {
        success: result,
        status: "Solved", id: incident.nice_id, title: incident.title,
        errors: incident.errors.full_messages.join(", ")
      }
      resque_log("Incident summary: #{incident_summary.inspect}")
      summary << incident_summary
    end

    summary
  end
end
