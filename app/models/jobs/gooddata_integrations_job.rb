class GooddataIntegrationsJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  ERROR_MESSAGE =
    'Failed to complete GoodData project provisioning process -- %s'.freeze

  priority :immediate

  RETRY_INTERVAL = 2.seconds
  PAUSE_INTERVAL = 1.minute

  def self.work(account_id, user_id, _audit = nil)
    new(Account.find(account_id), User.find(user_id)).work
  rescue ActiveRecord::RecordNotFound => error
    record_error_with_message(error,
      "account_id: #{account_id}, user_id: #{user_id} could not be found")
  rescue GoodData::Error => error
    record_error_with_message(error, 'GoodData API error occurred')
  rescue StandardError => error
    record_error_with_message(error,
      "Unknown error occurred for account_id: #{account_id}, user_id #{user_id}")
  end

  def self.args_to_log(account_id, user_id, *_args)
    {account_id: account_id, user_id: user_id}
  end

  def self.record_error_with_message(error, message)
    ZendeskExceptions::Logger.record(error, location: self, message: ERROR_MESSAGE % message, fingerprint: 'e5154670e148642808ddbe7bdd1cb6186abbabd7')
  end

  attr_reader :account, :user

  def initialize(account, user)
    @account = account
    @user    = user
  end

  def work
    log_run

    log_stats(
      Benchmark.realtime do
        integration_provisioning.perform
      end
    )
  rescue GoodData::Error::TooManyRequests
    log("Provisioning of account #{account.id} in GoodData failed with " \
      'TooManyRequests error, pausing before retrying')

    sleep(PAUSE_INTERVAL) && retry
  rescue GoodData::Error::ServiceUnavailable
    log("Provisioning of account #{account.id} in GoodData failed with " \
      'ServiceUnavailable error, retrying')

    sleep(RETRY_INTERVAL) && retry
  rescue
    statsd_client.increment('failed')
    raise
  end

  private

  def log_run
    log("Provisioning account #{account.id} in GoodData")
    statsd_client.increment('count')
  rescue
    error('could not log count of GooddataIntegrationsJob runs to statsd')
  end

  def log_stats(running_time)
    log("Successfully provisioned account #{account.id} in GoodData")
    statsd_client.histogram('execution_time', running_time)
  rescue
    error('could not log execution time of GooddataIntegrationsJob to statsd')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['jobs', 'gooddata_integrations_job']
    )
  end

  def integration_provisioning
    @integration_provisioning ||=
      Zendesk::Gooddata::IntegrationProvisioning.new(account, admin: user)
  end

  def log(*args)
    self.class.resque_log(*args)
  end

  def error(*args)
    self.class.resque_error(*args)
  end
end
