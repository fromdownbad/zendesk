class AccountAutomationParallelExecutionJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :medium

  self.job_timeout = if ENV.key?('AUTOMATIONS_JOB_TIMEOUT_MINUTES')
    Integer(ENV['AUTOMATIONS_JOB_TIMEOUT_MINUTES']).minutes
  else
    30.minutes
  end

  # Post-rollout, this can be directly assigned
  def self.background_heartbeat_interval
    if Arturo.feature_enabled_for_pod?(:parallel_automations_background_heartbeat, Zendesk::Configuration.fetch(:pod_id))
      1.minute
    end
  end

  class << self
    def work(account_id, automation_id, dehydrated_tickets, _audit_id)
      account = Account.find(account_id)
      account.on_shard do
        automation = account.automations.find(automation_id)
        Time.use_zone(account.time_zone) do
          catch_and_report_errors "Failed to execute automation #{automation.id}, account #{account.subdomain}, dehydrated_tickets: #{dehydrated_tickets}" do
            dehydrated_tickets.map! { |t| Zendesk::Rules::DehydratedTicket.new(*t) }
            Zendesk::Rules::AutomationDehydratedTicketProcessor.new(automation: automation).process_tickets(dehydrated_tickets)
          end
        end
      end
    end

    def args_to_log(account_id, automation_id, _dehydrated_tickets, audit = nil)
      {
        account_id:          account_id,
        automation_id:       automation_id,
        audit_id:            audit_id(audit)
      }
    end

    private

    def catch_and_report_errors(message)
      yield
    rescue StandardError => e
      message = "#{message}: #{e.message}"
      resque_log(message)
      ZendeskExceptions::Logger.record(e, location: self, message: message, fingerprint: 'ae5236c0485b5ae3e94c4185d5cd3c43df83ee80')
    end
  end
end
