# requires these options to be passed:
#   account_id
#   user_id
class OrganizationBulkCreateJob < JobWithStatus
  priority :medium
  enforce_in_flight_limit

  def organizations
    @organizations ||= options[:organizations]
  end

  def name
    "OrganizationBulkCreateJob account: #{options[:account_id]}"
  end

  private

  def work
    total = organizations.size
    Organization.reserve_global_uids(total)

    Rails.logger.info("Running OrganizationBulkCreateJob, norgs: #{total}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, total, 'Creating Organizations')

    results = organizations.each_with_index.map do |org, idx|
      at(idx + 1, total, "Creating organization ##{idx + 1}")

      custom_fields = org.delete(:organization_fields)
      new_org = current_account.organizations.build(org)
      new_org.custom_field_values.update_from_hash(custom_fields)

      if new_org.save
        {id: new_org.id, status: "Created"}
      else
        {status: "Failed Creating organization #{org[:name]}", errors: new_org.errors.full_messages.join(',')}
      end
    end

    completed(results: results)
  end
end
