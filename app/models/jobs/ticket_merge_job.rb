require 'zendesk/tickets/merger'

# requires these options to be passed:
#   account_id
#   user_id
#   target
#   sources
#   target_comment
#   source_comment
#   target_is_public
#   source_is_public
#   target_href
#   source_href
class TicketMergeJob < JobWithStatus
  include FlashHelper

  priority :immediate

  private

  def work
    ticket_merger.merge(
      target: target,
      sources: sources,
      target_comment: options[:target_comment],
      source_comment: options[:source_comment],
      target_is_public: options[:target_is_public],
      source_is_public: options[:source_is_public]
    )

    result = is_bulk_merge? ? bulk_merge_success_message : merge_success_message
    completed(results: [{ message: result, success: true }])
  rescue Zendesk::Tickets::Merger::SourceInvalid => error
    ticket = error.ticket
    log_error(ticket, error)
    completed(results: [{ message: invalid_source_message(ticket), success: false }])
  rescue Zendesk::Tickets::Merger::TargetInvalid => error
    ticket = error.ticket
    log_error(ticket, error)
    completed(results: [{ message: invalid_target_message(ticket), success: false }])
  rescue Zendesk::Tickets::Merger::MergeFailed => error
    log_error(target, error)
    completed(results: [{ message: unexpected_error_message, success: false }])
  end

  def ticket_merger
    @ticket_merger ||= Zendesk::Tickets::Merger.new(user: current_user)
  end

  def target
    @target ||= current_account.tickets.find_by_nice_id(options[:target])
  end

  def source_ids
    options[:sources]
  end

  def sources
    @sources ||= source_ids.map { |nice_id| current_account.tickets.find_by_nice_id(nice_id) }
  end

  def source
    @source ||= @sources.first
  end

  def is_bulk_merge? # rubocop:disable Naming/PredicateName
    source_ids.size > 1
  end

  def source_href
    options[:source_href]
  end

  def target_href
    options[:target_href]
  end

  def merge_success_message
    I18n.t('txt.controllers.tickets.merge_controller.ticket_has_been_merged_into_ticket',
      id_of_ticket_merged: source.nice_id,
      ticket_title_merged: "<a class='title' href='#{source_href}'>#{source.title}</a>",
      id_main_ticket: target.nice_id,
      main_ticket_title: "<a class='title' href='#{target_href}'>#{target.title}</a>",
      locale: current_user.translation_locale)
  end

  def bulk_merge_success_message
    I18n.t('txt.controllers.tickets.merge_controller.tickets_have_been_merged_into',
      tickets_joined_by_number_symbol: source_ids.join(", #{I18n.t('txt.controllers.tickets.merge_controller.tickets_have_been_merged_into_number_symbol', locale: current_user.translation_locale)}"),
      ticket_id: "#{target.nice_id} <a class='title' href=#{target_href}'>#{target.title}</a>",
      locale: current_user.translation_locale)
  end

  def invalid_source_message(source)
    msg = I18n.t("txt.controllers.tickets.merge_controller.sorry_ticket_could_not_be_merged",
      ticket_id: source.nice_id,
      ticket_title: "<a class='title' href='#{source_href}'>#{source.title}</a>",
      locale: current_user.translation_locale)
    flash_error(msg, source)
  end

  def invalid_target_message(target)
    msg = I18n.t("txt.controllers.tickets.merge_controller.sorry_target_could_not_be_saved",
      ticket_id: target.nice_id,
      ticket_title: "<a class='title' href='#{target_href}'>#{target.title}</a>",
      locale: current_user.translation_locale)
    flash_error(msg, target)
  end

  def unexpected_error_message
    I18n.t('txt.controllers.tickets.merge_controller.problem_while_merging')
  end

  def log_error(ticket, error)
    ZendeskExceptions::Logger.record(error, location: ticket, message: error.message, fingerprint: '735ecc862d71e84386d0b9882e40b02cb76ef9c1')
  end
end
