# Try to update Screenr's Tenant for a Zendesk's Account using
# first_name, last_name, email from the given user
# If something goes wrong, it interprets the exceptions and return errors.
#
# account_id - The Account's id to which a Screenr account should be provisioned to.
# user_id    - The User's id who is provisioning the account.
#
class ScreenrUpdatingJob < JobWithStatus
  include Zendesk::Tickets::ClientInfo
  priority :immediate

  def name
    "ScreenrUpdatingJob account:#{options[:account_id]} " \
                             "user:#{options[:user_id]} "
  end

  private

  def work
    begin
      current_account.screenr_integration.send_personal_data_to_screenr_for(current_user)
      result = {
        api_status: 'updated',
        upgrade_url: current_account.screenr_integration.upgrade_url
      }
    rescue Screenr::BusinessPartnerClient::ValidationFailed => e
      result = {
        api_status: 'validation_failed',
        message: e.first_error_messages,
        error_on: e.first_error_on
      }
    end
    completed(results: result)
  end
end
