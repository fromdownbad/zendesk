class PushNotificationSdkRegistrationJob < PushNotificationBaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  def self.work(options)
    device_identifier = PushNotifications::DeviceIdentifier.find(options['identifier_id'])
    mobile_sdk_app = device_identifier.mobile_sdk_app
    urban_airship_key = mobile_sdk_app.settings.urban_airship_key
    urban_airship_master_secret = mobile_sdk_app.settings.urban_airship_master_secret

    client = PushNotifications::UrbanAirshipClient.new(
      urban_airship_key, urban_airship_master_secret, statsd_tags: ["name:sdk_registration"]
    )

    client.register(device_identifier)
  end

  def self.args_to_log(options)
    id = options['identifier_id']
    mobile_sdk_app_id = options['mobile_sdk_app_id']

    { device_id: id, mobile_sdk_app_id: mobile_sdk_app_id }
  end
end
