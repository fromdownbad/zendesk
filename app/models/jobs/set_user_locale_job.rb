class SetUserLocaleJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  def self.work(options)
    options = options.with_indifferent_access
    account = Account.find(options[:account_id])
    user = account.users.find(options[:user_id])
    http_accept_language = HttpAcceptLanguage::Parser.new(options[:language])

    available_translation_locales = account.available_languages
    settle = Zendesk::I18n::LanguageSettlement.new(http_accept_language, available_translation_locales)
    translation_locale = settle.find_matching_zendesk_locale || account.translation_locale

    if user.translation_locale != translation_locale
      user.update_attribute(:translation_locale, translation_locale)
    end
  end

  def self.args_to_log(options)
    options.slice(:account_id, :user_id, :language)
  end
end
