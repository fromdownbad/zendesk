require 'zendesk_billing_core/zuora/client'

class InvoiceEmailUpdateJob
  extend ZendeskJob::Resque::BaseJob

  priority :low

  def self.work(zuora_account_id, email_addresses)
    zuora_client = ZendeskBillingCore::Zuora::Client.new(zuora_account_id)
    if !email_addresses.blank?
      zuora_client.update_invoice_email_addresses(email_addresses.join(","))
    else
      zuora_client.update_invoice_email_addresses("")
    end
  end

  def self.args_to_log(zuora_account_id, email_addresses)
    { zuora_account_id: zuora_account_id, email_addresses: email_addresses }
  end
end
