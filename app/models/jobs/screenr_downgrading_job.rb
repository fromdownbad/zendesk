class ScreenrDowngradingJob < JobWithStatus
  include Zendesk::Tickets::ClientInfo
  priority :immediate

  def name
    "ScreenrDowngradingJob account:#{options[:account_id]} " \
                          "user:#{options[:user_id]} "       \
                          "domain:#{options[:domain]}"
  end

  private

  def work
    result = begin
      current_account.screenr_integration.downgrade_tenant!
      { api_status: 'downgraded' }
    rescue Screenr::BusinessPartnerClient::DowngradeFailed
      {
        api_status: 'validation_failed',
        message: "Current subscription can't be downgraded.",
        error_on: ''
      }
    end

    completed(results: result)
  end
end
