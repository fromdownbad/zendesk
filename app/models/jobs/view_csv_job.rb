require 'zip/zip'
require 'zip/zipfilesystem'
require 'prop'

class ViewCsvJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium

  LIMIT = 1000 # tickets per page.
  PROP_KEY = :csv_export_throttle

  OCCAM_CALLER_NAME = 'views-csv-export'.freeze

  def self.work(account_id, requester_id, rule_id, options = {})
    account = Account.find(account_id)
    csv_file = Tempfile.new([name, '.csv'])
    options = options.with_indifferent_access

    account.on_shard do
      user, rule = find_and_validate(requester_id, rule_id)

      Time.use_zone(account.time_zone) do
        I18n.with_locale(user.translation_locale) do
          build_csv_file(csv_file, user, rule, options)
          attachment = create_expirable_attachment(csv_file, account, user, rule)
          deliver_attachment(user, rule, attachment)
        end
      end
    end
  rescue SignalException
    account.on_shard do
      user, rule = find_and_validate(requester_id, rule_id)
      deliver_fail_notification(user, rule)
    end
    self.class.resque_warn('Job has been auto killed')
  ensure
    csv_file.close
    csv_file.unlink
  end

  def self.enqueue(account_id, user_id, rule_id, options = {})
    raise Resque::ThrottledError if Prop.throttled?(PROP_KEY, throttle_key(account_id, user_id))
    super
  end

  # This is gonna replace the identifier in ThrottleJob, since its feature is disabled.
  # Also, no longer using rule_id in token. Expecting it to be like "1-10002".
  def self.throttle_key(account_id, user_id)
    account_id.to_s + "-" + user_id.to_s
  end

  def self.deliver_attachment(user, rule, attachment)
    JobsMailer.deliver_job_complete(user, identifier(user.id, rule.id), subject(rule), message(attachment.url(token: true, mapped: false)))
  end

  def self.deliver_fail_notification(user, rule)
    JobsMailer.deliver_job_complete(user, identifier(user.id, rule.id), fail_subject(rule), fail_message)
  end

  def self.enqueued_translated
    I18n.t('txt.admin.models.jobs.view_csv_job.enqueued')
  end

  def self.throttled_translated
    I18n.t('txt.admin.models.jobs.view_csv_job.throttled')
  end

  def self.latest_translated
    I18n.t('txt.admin.models.jobs.view_csv_job.latest')
  end

  def self.args_to_log(account_id, requester_id, rule_id, options = {})
    { account_id: account_id, requester_id: requester_id, rule_id: rule_id, options: options}
  end

  def self.identifier(*args)
    args[0..2].join("-")
  end

  def self.available_for?(account)
    account.subscription.has_view_csv_export?
  end

  def self.latest(account)
    account.expirable_attachments.latest(ViewCsvJob.name).first
  end

  def self.find_and_validate(requester_id, rule_id)
    [
      User.find(requester_id),
      Rule.find(rule_id)
    ]
  end
  private_class_method :find_and_validate

  def self.build_csv_file(csv_file, user, rule, options = {})
    page = 1
    rule.per_page = LIMIT

    find_ticket_options = {
      output_type: 'table',
      paginate:    true,
      caller:      OCCAM_CALLER_NAME
    }

    find_ticket_options[:timeout] = -1

    loop do
      resque_log("#{name}: Building rule using page > #{page}")

      find_ticket_options[:page] = page

      tickets = Ticket.on_slave do
        rule.find_tickets(user, find_ticket_options)
      end

      CSV.open(csv_file.path, 'a', col_sep: ",", force_quotes: true) do |csv|
        csv << rule.csv_titles(options) if page == 1
        rule.build_csv(user, tickets).each do |row|
          csv << row
        end
      end

      Prop.throttle(PROP_KEY, throttle_key(user.account_id, user.id), increment: tickets.size)

      if tickets.size == LIMIT
        resque_log("#{name}: Size and limit both #{LIMIT}")
        page += 1
        sleep(3)
      else
        resque_log("#{name}: Size is #{tickets.size} and limit #{LIMIT}, break")
        break
      end
    end
  end
  private_class_method :build_csv_file

  def self.create_expirable_attachment(csv_file, account, user, rule)
    zipfile   = Tempfile.new([name, '.zip'])
    name      = PermalinkFu.escape("#{rule.title} view #{Time.now.in_time_zone.to_s(:csv)}")

    Zip::OutputStream.open(zipfile.path) do |zos|
      zos.put_next_entry(name + '.csv')
      zos << IO.read(csv_file.path)
    end

    attachment = ExpirableAttachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(zipfile.path, filename: "#{name}-csv.zip"))
    attachment.account = account
    attachment.author = user
    attachment.created_via = ViewCsvJob.name
    attachment.save!
    attachment
  ensure
    zipfile.close
    zipfile.unlink
  end
  private_class_method :create_expirable_attachment

  def self.subject(rule)
    I18n.t('txt.admin.models.jobs.view_csv_job.subject', title: title(rule))
  end
  private_class_method :subject

  def self.title(rule)
    if rule.is_a?(View)
      Zendesk::DynamicContent::Renderer.render(rule.account, I18n.translation_locale, rule.title)
    else
      rule.title
    end
  end
  private_class_method :title

  def self.message(url)
    content = I18n.t('txt.admin.models.jobs.view_csv_job.message_1')
    content << "\n\n#{url}\n\n"
    content << I18n.t('txt.admin.models.jobs.view_csv_job.message_2')
  end
  private_class_method :message

  def self.fail_message
    I18n.t('txt.admin.models.jobs.view_csv_job.fail_message')
  end
  private_class_method :fail_message

  def self.fail_subject(rule)
    I18n.t('txt.admin.models.jobs.view_csv_job.fail_subject', title: title(rule))
  end
  private_class_method :fail_subject
end
