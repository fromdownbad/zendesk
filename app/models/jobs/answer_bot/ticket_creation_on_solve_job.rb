require 'resque-retry'

module AnswerBot
  class TicketCreationOnSolveJob
    extend ZendeskJob::Resque::BaseJob
    extend ZendeskJob::Resque::ShardedJob
    extend Resque::Plugins::ExponentialBackoff

    priority :medium

    @expire_retry_key_after = 3600 # expire key after `retry_delay` plus 1 hour
    @backoff_strategy = [0, 60, 300] # 0, 1, 5 minutes

    class << self
      def args_to_log(account_id, deflection_id, article_id, resolution_channel_id)
        {
          account_id: account_id,
          deflection_id: deflection_id,
          article_id: article_id,
          resolution_channel_id: resolution_channel_id
        }
      end

      def work(account_id, deflection_id, article_id, resolution_channel_id)
        new(
          account_id: account_id,
          deflection_id: deflection_id,
          article_id: article_id,
          resolution_channel_id: resolution_channel_id
        ).perform
      rescue => error
        if retry_limit_reached?
          record_stats(deflection_id, resolution_channel_id)
          log_failed_job(account_id, deflection_id, article_id, error)
        else
          raise error
        end
      end

      def retry_limit_reached?
        retry_attempt >= @backoff_strategy.length
      end

      def log_failed_job(account_id, deflection_id, article_id, error)
        resque_error(
          <<-LOG.squish
            Job failed after #{retry_attempt} retry:
            Account Id: #{account_id},
            Ticket Deflection Id: #{deflection_id},
            Article Id: #{article_id},
            Error: #{error.message}
          LOG
        )
      end

      def record_stats(deflection_id, resolution_channel_id)
        deflection = TicketDeflection.find(deflection_id)

        ::Datadog::AnswerBot.new(
          subdomain: deflection.brand.subdomain,
          via_id: deflection.via_id,
          deflection_channel_id: deflection.deflection_channel_id,
          resolution_channel_id: resolution_channel_id
        ).ticket_creation_failed
      end
    end

    def initialize(account_id:, deflection_id:, article_id:, resolution_channel_id:)
      @account_id = account_id
      @deflection_id = deflection_id
      @article_id = article_id
      @resolution_channel_id = resolution_channel_id
    end

    def perform
      ticket = AnswerBot::RequestSolver.new(
        deflection_id: deflection_id,
        article_id: article_id,
        resolution_channel_id: resolution_channel_id
      ).create_and_solve_ticket

      ticket_id = ticket.id
      requester_id = ticket.requester_id

      log_successful_job(account_id, deflection_id, article_id, requester_id, ticket_id)
    end

    private

    attr_reader :account_id,
      :deflection_id,
      :article_id,
      :resolution_channel_id

    def log_successful_job(account_id, deflection_id, article_id, requester_id, ticket_id)
      self.class.resque_log(
        <<-LOG.squish
          Job completed:
            Account Id: #{account_id},
            Ticket Deflection Id: #{deflection_id},
            Article Id: #{article_id},
            Requester Id: #{requester_id},
            Ticket Id: #{ticket_id}
        LOG
      )
    end
  end
end
