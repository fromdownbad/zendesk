module AnswerBot
  class TicketSolverJob
    extend ZendeskJob::Resque::BaseJob
    extend ZendeskJob::Resque::ShardedJob

    priority :medium

    class << self
      def args_to_log(account_id, user_id, ticket_deflection_id, article_id, resolution_channel_id, stats)
        {
          account_id: account_id,
          user_id: user_id,
          ticket_deflection_id: ticket_deflection_id,
          article_id: article_id,
          resolution_channel_id: resolution_channel_id,
          stats: stats
        }
      end

      def work(account_id, user_id, ticket_deflection_id, article_id, resolution_channel_id, stats)
        new(
          account_id: account_id,
          user_id: user_id,
          ticket_deflection_id: ticket_deflection_id,
          article_id: article_id,
          resolution_channel_id: resolution_channel_id,
          stats: stats
        ).perform
      end
    end

    def initialize(
      account_id:,
      user_id:,
      ticket_deflection_id:,
      article_id:,
      resolution_channel_id:,
      stats:
    )
      @account_id = account_id
      @user_id = user_id
      @ticket_deflection_id = ticket_deflection_id
      @article_id = article_id
      @resolution_channel_id = resolution_channel_id
      @stats = stats
    end

    def perform
      ticket = ticket_deflection.ticket
      return unless ticket.working?

      solve(ticket, ticket_deflection, user_id, article_id, resolution_channel_id)

      record_stats(stats)
      log_successful_job(account_id, ticket_deflection_id, article_id, user_id)
    rescue => error
      log_failed_job(account_id, ticket_deflection_id, article_id, user_id, error)
      false
    end

    private

    attr_reader :account_id,
      :user_id,
      :ticket_deflection_id,
      :article_id,
      :resolution_channel_id,
      :stats

    def ticket_deflection
      @ticket_deflection ||= TicketDeflection.find(ticket_deflection_id)
    end

    def solve(ticket, ticket_deflection, user_id, article_id, resolution_channel_id)
      user = User.find(user_id)

      ticket_deflection.solve_with_article(article_id, resolution_channel_id)
      ticket_deflection.add_solved_information_to_ticket(article_id)

      ticket.solve(user)
    end

    def log_successful_job(_account_id, ticket_deflection_id, article_id, user_id)
      self.class.resque_log(
        <<-LOG.squish
          Job completed:
            Account Id: #{account_id},
            Ticket Deflection Id: #{ticket_deflection_id},
            Article Id: #{article_id},
            User Id: #{user_id}
        LOG
      )
    end

    def log_failed_job(_account_id, ticket_deflection_id, article_id, user_id, error)
      self.class.resque_error(
        <<-LOG.squish
          Job failed:
            Account Id: #{account_id},
            Ticket Deflection Id: #{ticket_deflection_id},
            Article Id: #{article_id},
            User Id: #{user_id},
            Error: #{error.message}
        LOG
      )
    end

    def record_stats(stats)
      tags = stats.slice('channel', 'model_version', 'language', 'mobile')
      ab_statsd_client.solved(tags.symbolize_keys)
    end

    def ab_statsd_client
      @ab_statsd_client ||= Datadog::AnswerBot.new(
        via_id: stats['via_id'],
        subdomain: stats['subdomain'],
        deflection_channel_id: ticket_deflection.deflection_channel_id,
        resolution_channel_id: resolution_channel_id
      )
    end
  end
end
