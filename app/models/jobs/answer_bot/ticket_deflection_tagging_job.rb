module AnswerBot
  class TicketDeflectionTaggingJob
    extend ZendeskJob::Resque::BaseJob

    RETRY_ATTEMPTS = 3
    RETRY_EXCEPTIONS = [
      ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation,
      ActiveRecord::RecordInvalid
    ].freeze

    priority :low

    def self.work(account_id, ticket_id, tag)
      account = Account.find(account_id)

      ActiveRecord::Base.on_shard(account.shard_id) do
        begin
          Zendesk::Retrier.retry_on_error(RETRY_EXCEPTIONS, RETRY_ATTEMPTS) do
            ticket = account.tickets.find(ticket_id)

            return if ticket.status == 'closed'

            ticket.will_be_saved_by(User.system)
            ticket.update_attributes!(additional_tags: tag, safe_update_timestamp: Time.now)
            sleep 1
          end
        rescue *RETRY_EXCEPTIONS, ActiveRecord::RecordInvalid => e
          resque_log("failed adding Answer Bot tags for account #{account_id}, with tag #{tag} raising exception #{e}")
        end
      end
    end

    def self.args_to_log(account_id, ticket_id, tag)
      { account_id: account_id, ticket_id: ticket_id, tag: tag }
    end
  end
end
