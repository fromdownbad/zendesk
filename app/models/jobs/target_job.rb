require 'faraday'
require 'zendesk/targets/metrics/statsd_client'

class TargetJob
  extend ZendeskJob::Resque::BaseJob
  include TargetsMetrics::StatsDClient

  priority :high

  def self.work(account_id, external_id, retries = 0)
    account = Account.find(account_id)
    account.on_shard do
      begin
        external = External.find(external_id)
        job      = new(external, retries)
        job.validate!
        job.append_log_attributes
        Time.use_zone(account.time_zone) { job.send_message }
      rescue StandardError => e
        resque_warn("Account #{account_id}: Error in TargetJob processing external #{external_id}: #{e.message}")
      end
    end
  end

  def self.args_to_log(account_id, external_id, *_)
    { account_id: account_id, external_id: external_id }
  end

  attr_reader :external, :retries

  def initialize(external, retries = 0)
    @external = external
    @retries = retries
  end

  def append_log_attributes
    TargetJob.logger.append_attributes(
      target_type: target.type
    )
  end

  def log_hash
    {
      account_id: external.account_id,
      target_id: target.id,
      target_type: target.type,
      total_sent: target.sent,
      total_failures: target.current_failures,
      ticket_id: external.ticket.try(:id),
      event_id: external.id
    }
  end

  def send_message_to_target
    if target.retriable?
      target.send_message_with_retry(message, external, retries)
    else
      target.send_message(message, external)
    end
  end

  def send_message
    TargetJob.resque_log(["executing target", log_hash])
    send_message_to_target
    target.message_sent
    TargetJob.resque_log(["succeeded", log_hash])
    statsd_client.increment("count.success", tags: %W[source:#{target.metrics_name}])
  rescue ActiveRecord::StatementInvalid => e
    unless /lock wait timeout/i.match?(e.message)
      # we have some tendancy to deadlock around the targets row itself; or at least we have.  if we hit
      # a deadlock, the absolute worst thing we could do would be to try to update the targets row again.
      TargetJob.resque_log(["exception", log_hash.merge(error_class: e.class.name, error_message: e.message)])
      target.message_failed(e, external)
    end
    statsd_client.increment("count.failure", tags: %W[source:#{target.metrics_name} reason:invalid_statement])
    raise e
  rescue Faraday::TimeoutError, Timeout::Error => e
    TargetJob.resque_log(["timeout", log_hash])
    target.message_failed(e, external)
    statsd_client.increment("count.failure", tags: %W[source:#{target.metrics_name} reason:request_timeout])
    raise I18n.t("txt.errors.targets.url_target_v2.timeout", timeout: 10)
  rescue HTTPResponseStatusError => e
    TargetJob.resque_log(["HTTP call failed", log_hash.merge(error_message: e.message)])
    target.message_failed(e, external)
    statsd_client.increment("count.failure", tags: %W[source:#{target.metrics_name} reason:failed_response])
  rescue Exception => e # rubocop:disable Lint/RescueException
    # eventually I'd love to get rid of this catch-all block. let's figure out what else might raise in message_sent
    # and stomp it out.
    TargetJob.resque_log(["exception", log_hash.merge(error_class: e.class.name, error_message: e.message)])
    target.message_failed(e, external)
    statsd_client.increment("count.failure", tags: %W[source:#{target.metrics_name} reason:generic_error])
    raise e
  end

  def message
    Datadog.tracer.trace('target.message.render', {
      service: 'classic-resque',
      resource: 'TargetJob'
    }) do |span|
      span.set_tag('operation_name', 'resque.job')
      span.set_tag('resource_name', 'TargetJob')
      span.set_tag('target_func', 'render_message')
      span.set_tag('target_type', target.type)

      msg = target.render_message(external)

      if msg.instance_of?(String)
        span.set_tag('target_msg_size', msg.bytesize)
      end

      msg
    end
  end

  def target
    external.target
  end

  def validate!
    raise "Invalid external" unless external_valid?
    raise "Invalid target"   unless target_valid?
  end

  def external_valid?
    external && !external.value.blank?
  end

  def target_valid?
    target && target.valid? && target.is_active
  end

  def statsd_client
    @statsd_client ||= create_statsd_client
  end
end
