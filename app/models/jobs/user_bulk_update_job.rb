class UserBulkUpdateJob < JobWithStatus
  enforce_in_flight_limit

  private

  def work
    current_account = Account.find(options[:account_id])

    at(0, options[:user_ids].size, 'finding users')
    users = current_account.users.where(id: options[:user_ids])
    permission_set = options[:permission_set] == 'admin' ? 'admin' : current_account.permission_sets.find_by_id(options[:permission_set])

    can_create_voice_seats = UserSeat.can_create_new_voice_seats?(current_account, users.map(&:id))
    if options[:voice_seat] == 'enable' && !can_create_voice_seats

      return completed(
        results: [
          I18n.t(
            "txt.job.user_bulk_update_job.talk_seats_limit_reached",
            count: UserSeat.max_agents(current_account),
            locale: options[:locale]
          )
        ]
      )
    end

    failed_saves = []

    users.each_with_index do |user, index|
      at(index + 1, users.size, "updating user #{user.id}")
      user.role_or_permission_set = permission_set

      if options[:voice_seat] == 'enable'
        voice_seat = user.user_seats.voice.where(account_id: current_account.id).first
        Zendesk::SupportUsers::Entitlement.for(user, Zendesk::Entitlement::TYPES[:voice]).grant! unless voice_seat.present?
      elsif options[:voice_seat] == 'disable'
        Zendesk::SupportUsers::Entitlement.for(user, Zendesk::Entitlement::TYPES[:voice]).revoke!
      end

      failed_saves << user unless Zendesk::SupportUsers::User.new(user).save
    end

    if failed_saves.empty?
      completed(results: [])
    else
      if options[:user_ids].length == failed_saves.length
        completed(results: [I18n.t('txt.job.user_bulk_update_job.a_problem_was_encountered', locale: options[:locale])])
      else
        completed(results: [I18n.t('txt.job.user_bulk_update_job.update_failed_for_users', count: failed_saves.length, locale: options[:locale])])
      end
    end
  end
end
