class UpdateAppsFeatureJob
  extend ZendeskJob::Resque::BaseJob
  extend Resque::Plugins::ExponentialBackoff

  priority :high
  @backoff_strategy = [60, 120, 600, 3600, 10800, 21600, 86400]
  @expire_retry_key_after = @backoff_strategy.last

  def self.work(account_id, feature, old_value, new_value)
    @account = Account.find(account_id)
    client = Zendesk::AppMarketClient.new(@account.subdomain)
    client.feature_change(feature, old_value, new_value)
  rescue ActiveRecord::RecordNotFound => e
    Rails.logger.error("Failed to update apps feature. Message #{e.message}.\n")
  rescue => error
    log_error(error, args_to_log(account_id, feature, old_value, new_value)) if retry_limit_reached?
    raise
  end

  def self.args_to_log(account_id, feature, old_value, new_value)
    {
      account_id: account_id,
      feature: feature,
      old_value: old_value,
      new_value: new_value,
    }
  end

  def self.log_error(error, params)
    subdomain = @account.present? ? @account.subdomain : 'Account not found'
    message = JSON.dump(
      subdomain: subdomain,
      params: params,
      error_class: error.class.name,
      error_message: error.message
    )

    Rails.logger.error("Failed to update apps feature. Message #{message}.\nBacktrace: #{error.backtrace.join("\n")}")
    ZendeskExceptions::Logger.record(error, location: self.class, message: message, fingerprint: '78dfaefc6f2d04cb431825a386cc13feb4ca9adc')
  end
end
