module ChatPhaseThree
  class EntitlementSyncJob
    extend ZendeskJob::Resque::BaseJob
    extend ZendeskJob::Resque::ShardedJob
    extend Resque::Plugins::ExponentialBackoff

    STATSD_CLIENT = Zendesk::StatsD::Client.new(namespace: "cp3_entitlement_sync_job")

    NON_RETRYABLE_ERRORS = [
      ActiveRecord::RecordNotFound,
      Kragle::TooManyRequests,
      Kragle::BadRequest,
      Kragle::Conflict
    ].freeze

    priority :high
    try_again_callback :report_retry
    give_up_callback :report_giving_up

    @backoff_strategy = [1, 3, 5]

    class << self
      def work(account_id, user_id, audit_headers = {})
        new(account_id, user_id, audit_headers).work
      end

      def args_to_log(account_id, user_id, audit_headers)
        {
          account_id: account_id,
          user_id: user_id,
          audit_headers: audit_headers
        }
      end

      def statsd_client
        STATSD_CLIENT
      end

      def report_giving_up(error, *args)
        Rails.logger.warn("Giving up CP3 entitlements sync for #{args} due to error #{error.message}")
        statsd_client.increment(:failures)
      end

      def report_retry(error, *args)
        Rails.logger.warn("Retrying CP3 entitlements sync for #{args} due to error #{error.message}")
        statsd_client.increment(:retries)
      end

      # Specifying exceptions that will not be retried via the public API of Resque::Plugins::Retry
      def fatal_exceptions
        NON_RETRYABLE_ERRORS
      end
    end

    def initialize(account_id, user_id, audit_headers = {})
      @account_id = account_id
      @user_id = user_id
      @audit_headers = audit_headers
    end

    def work
      Zendesk::StaffClient.new(account, audit_headers: audit_headers).update_entitlements!(@user_id, entitlements, user.is_end_user?)
    end

    private

    attr_reader :audit_headers

    def entitlements
      { chat: chat_role }
    end

    def chat_role
      if user.is_agent? && zopim_identity.try(:is_enabled?)
        zopim_identity.is_administrator? ? Zendesk::Entitlement::ROLES[:chat][:admin] : Zendesk::Entitlement::ROLES[:chat][:agent]
      end
    end

    def zopim_identity
      @zopim_identity ||= user.zopim_identity
    rescue ActiveRecord::RecordNotFound => _
      nil
    end

    def user
      @user ||= account.users.find(@user_id)
    end

    def account
      @account ||= Account.find(@account_id)
    end
  end
end
