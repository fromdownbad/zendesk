class RemoveRegularOrganizationMembershipsJob
  extend ZendeskJob::Resque::BaseJob
  priority :medium

  def self.work(account_id)
    account = Account.find(account_id)
    account.on_shard do
      account.organization_memberships.regular.find_each(&:destroy)
    end
  end

  def self.args_to_log(account_id)
    {account_id: account_id}
  end
end
