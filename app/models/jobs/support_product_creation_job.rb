class SupportProductCreationJob < Zendesk::Jobs::Resque
  extend Resque::Plugins::ExponentialBackoff
  # This job runs when creating a new legacy (non-shell) account.

  @queue = :immediate

  # 0s, 3s, 5s, 30s, 1m, 5m, 10m, 30m, 1hr, 3hr
  @backoff_strategy = [0, 3, 5, 30, 60, 300, 600, 1_800, 3_600, 10_800]

  params :account_id

  def work
    Zendesk::TrialActivation.create_support_product(account)
  end

  private

  def account
    @account ||= Account.on_master { Account.find(account_id) }
  end
end
