class UnassignTicketsJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  self.background_heartbeat_interval = 15.seconds

  priority :medium

  def self.work(account_id, group_id, user_id, requester_id, unassignment_reason, _audit = nil)
    unassigner = new(
      account_id: account_id,
      group_id: group_id,
      user_id: user_id,
      requester_id: requester_id,
      unassignment_reason: unassignment_reason
    )

    unassigner.remove_assignee_from_working_tickets!
  end

  def self.args_to_log(account_id, group_id, user_id, requester_id, unassignment_reason, audit = nil)
    { account_id: account_id, group_id: group_id, user_id: user_id,
      requester_id: requester_id, reason: unassignment_reason, audit_id: audit_id(audit) }
  end

  attr_reader :account_id, :group_id, :user_id, :requester_id, :unassignment_reason

  def initialize(params)
    @account_id          = params.fetch(:account_id)
    @group_id            = params.fetch(:group_id)
    @user_id             = params.fetch(:user_id)
    @requester_id        = params.fetch(:requester_id)
    @unassignment_reason = params.fetch(:unassignment_reason)
  end

  def remove_assignee_from_working_tickets!
    ensure_job_requester_is_not_system_user

    job_requester.account.tickets.not_closed.where(conditions).find_each do |ticket|
      if job_requester.can? :edit, ticket
        ticket.set_next_best_assignee(job_requester)
        ticket.will_be_saved_by(job_requester, via_id: unassignment_reason)
        ticket.save!
      else
        self.class.resque_log "Ticket unassignment not possible in account #{job_requester.account.id}. Current user #{job_requester.email} cannot edit ticket #{ticket.nice_id}"
      end
    end
  end

  protected

  def job_requester
    @job_requester ||= User.find(requester_id)
  end

  def ensure_job_requester_is_not_system_user
    if job_requester.is_system_user?
      raise "UnassignTicketsJob: Cannot process as a system user: #{requester_id}, group_id: #{group_id}, user_id: #{user_id}, unassignment_reason #{unassignment_reason}"
    end
  end

  def conditions
    conditions = { assignee_id: user_id }
    conditions[:group_id] = group_id if group_id.present?
    conditions
  end
end
