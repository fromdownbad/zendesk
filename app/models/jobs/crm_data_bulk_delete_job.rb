# requires these options to be passed:
#   account_id
class CrmDataBulkDeleteJob < BulkDeleteJob
  include Salesforce::AppSettings
  priority :low
  enforce_in_flight_limit

  def name
    "CrmDataBulkDeleteJob account: #{options[:account_id]}"
  end

  def work
    ticket_datas = ExternalTicketData.where(account_id: options[:account_id]).where('updated_at >= ?', delay.ago)
    ticket_datas.destroy_all unless ticket_datas.nil?
    user_datas = ExternalUserData.where(account_id: options[:account_id]).where('updated_at >= ?', delay.ago)
    user_datas.destroy_all unless user_datas.nil?
  end

  def account
    current_account
  end
end
