class ChatAppInstallationJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  priority :high

  def self.work(account_id, _audit = nil)
    new(account_id).work
  end

  def self.args_to_log(account_id, audit = nil)
    {
      account_id: account_id,
      audit_id: audit_id(audit)
    }
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def work
    account.zopim_integration.try(:install_app)
  rescue StandardError => e
    Rails.logger.warn("Failed to install Chat app: #{e.message}")
  end

  private

  def account
    @account ||= Account.find(@account_id)
  end
end
