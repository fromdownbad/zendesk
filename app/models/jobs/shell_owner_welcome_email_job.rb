class ShellOwnerWelcomeEmailJob < Zendesk::Jobs::Resque
  extend Resque::Plugins::ExponentialBackoff

  class AccountNotReady < StandardError; end

  @queue = :high
  # 1s, 3s, 5s, 10s, 30s, 60s, 2m, 5m, 10m, 20m, 30m, 1hr, 3hr
  @backoff_strategy = [1, 3, 5, 10, 30, 60, 120, 300, 600, 1_200, 1_800, 3_600, 10_800]

  params :account_id, :user_id

  def work
    raise AccountNotReady unless readiness_check == Zendesk::AccountReadiness::STATE_COMPLETE

    account.on_shard do
      AccountsMailer.deliver_welcome_account_owner(user,
        account_url: account.url(mapped: false),
        priority_mail: "Shell account owner welcome email")
    end
  end

  private

  def account
    @account ||= Account.on_master { Account.find(account_id) }
  end

  def user
    @user ||= account.users.find(user_id)
  end

  def readiness_check
    Zendesk::AccountReadiness.new(account_id: account_id, subdomain: account.subdomain, source: "shell_owner_welcome_email_job").readiness_check
  end
end
