class RulePreviewTicketCountJob < RuleTicketCountJob
  attr_reader :rule

  priority :rule_count

  TITLE = 'rule-preview-ticket-count-job'.freeze

  def initialize(definition, user, cached_count = nil, include_archived_tickets = false, options = {})
    super(nil, user, cached_count, false, options)

    # Needs to be Definition, otherwise load will fail
    # and produce a Syck::Object in dev
    unless definition.is_a?(Definition)
      definition = YAML.load(definition)
    end

    @rule = View.new do |rule|
      rule.definition = definition
      rule.output = Output.default
      rule.owner = self.user
      rule.include_archived_tickets = include_archived_tickets
    end
  end

  def self.args_to_log(account_id, _rule, user_id, include_archived_tickets, options = {})
    { account_id: account_id, preview: true, user_id: user_id, include_archived_tickets: include_archived_tickets }.merge(options)
  end
end
