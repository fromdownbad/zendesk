class ReEncryptExternalEmailCredentialJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  CURRENT_ENCRYPTION_KEY_NAME = ENV.fetch('EXTERNAL_EMAIL_CREDENTIALS_ENCRYPTION_KEY')

  priority :low

  def self.args_to_log(account_id)
    { account_id: account_id }
  end

  def self.work(account_id)
    new(account_id).work
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def work
    external_email_credentials = account.external_email_credentials.where.not(encryption_key_name: CURRENT_ENCRYPTION_KEY_NAME)

    external_email_credentials.each do |external_email_credential|
      decrypted_value = external_email_credential.decrypted_refresh_token

      external_email_credential.update_columns(
        encrypted_value: crypt.encrypt(decrypted_value),
        encryption_key_name: CURRENT_ENCRYPTION_KEY_NAME,
        encryption_cipher_name: 'aes-256-gcm'
      )

      Rails.logger.info("Re-encrypted external email credential for the recipient: #{external_email_credential.username}, account_id: #{account.id}")
    end
  end

  private

  def account
    @account ||= Account.find(@account_id)
  end

  def crypt
    @crypt ||= Zendesk::MessageEncryptor.new(Base64.decode64(ENV.fetch(CURRENT_ENCRYPTION_KEY_NAME)))
  end
end
