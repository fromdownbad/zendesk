require 'zendesk/encryptable'

class ReEncryptTargetCredentialsJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :low

  CURRENT_ENCRYPTION_KEY_NAME = ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_KEY')
  CURRENT_ENCRYPTION_CIPHER_NAME = ENV.fetch('TARGET_CREDENTIALS_ENCRYPTION_CIPHER_NAME')
  ENCRYPTABLE_ATTRIBUTES = [
    :encrypted_token,
    :encrypted_password,
    :encrypted_secret_value
  ].freeze

  def self.args_to_log(account_id)
    { account_id: account_id }
  end

  def self.work(account_id)
    new(account_id).work
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def work
    targets = account.targets.where.not(encryption_key_name: [nil, CURRENT_ENCRYPTION_KEY_NAME])

    targets.each do |target|
      target = re_encrypt_value(target)

      next unless target.changed?

      target.save!(validate: false)

      Rails.logger.info("Re-encrypted target with id: #{target.id} on account: #{account.id} using #{CURRENT_ENCRYPTION_KEY_NAME} key and #{CURRENT_ENCRYPTION_CIPHER_NAME} cipher")
    end
  end

  def re_encrypt_value(target)
    ENCRYPTABLE_ATTRIBUTES.each do |field|
      next unless target.try(field).present?
      decrypted_value = target.decrypted_value_for(field)
      target.send("#{field}=", crypt.encrypt(decrypted_value))
      target.encryption_key_name = CURRENT_ENCRYPTION_KEY_NAME
      target.encryption_cipher_name = CURRENT_ENCRYPTION_CIPHER_NAME
    end

    target
  end

  private

  def crypt
    @crypt ||= Zendesk::MessageEncryptor.new(Base64.decode64(ENV.fetch(CURRENT_ENCRYPTION_KEY_NAME)))
  end

  def account
    @account ||= Account.find(@account_id)
  end
end
