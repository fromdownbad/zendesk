module TdeWorkspaces
  class AssignWorkspaceJob < Zendesk::Jobs::Resque
    self.queue = :high

    params :account_id, :ticket_id

    def work
      if ticket.present? && account.has_contextual_workspaces? && !account.has_contextual_workspaces_in_js?
        running_time = Benchmark.realtime do
          ticket&.reassign_workspace
        end

        statsd_client.histogram('job_execution_time', running_time)
        Rails.logger.info("Successfully performed AssignWorkspaceJob for account #{@account.id} ticket #{@ticket_id}")
      else
        statsd_client.increment("not_assigned.no_ticket")
      end
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: __LINE__, message: "Workspace AssignWorkspaceJob failed", fingerprint: 'assign_workspace_job_failed', metadata: { account: account_id, ticket: ticket&.id })
      statsd_client.increment("not_assigned.exception")
    end

    def ticket
      account.tickets.find_by_id(params[:ticket_id])
    end

    private

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'ticket.contextual_workspaces')
    end
  end
end
