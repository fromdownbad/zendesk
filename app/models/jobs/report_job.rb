class ReportJob < ThrottleJob
  priority :medium

  throttle can_run_every: 30.minutes

  def self.work(account_id, requester_id, report_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      user = User.find(requester_id)
      report = Report.find(report_id)
      report.execute(true)
      JobsMailer.deliver_job_complete(user, identifier(requester_id, report_id), subject(report.title, user.translation_locale), message(account.url + "/reports/#{report.id}", user.translation_locale))
    end
  end

  def self.args_to_log(account_id, _requester_id, report_id)
    { account_id: account_id, report_id: report_id }
  end

  def self.identifier(*args)
    account_id, _requester_id, report_id = *args
    "account_id:#{account_id}-report_id:#{report_id}"
  end

  def self.available_for?(_account)
    true
  end

  def self.latest(_account)
    nil
  end

  def self.subject(title, email_locale)
    I18n.t('txt.models.jobs.report_job.report_generated_email_subject', title: title, locale: email_locale)
  end
  private_class_method :subject

  def self.message(url, email_locale)
    content = "#{I18n.t('txt.models.jobs.report_job.your_report_has_been_generated', locale: email_locale)}\n\n"
    content << "#{url}\n\n"
    content
  end
  private_class_method :message
end
