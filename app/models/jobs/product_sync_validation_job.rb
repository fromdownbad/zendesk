require 'digest'

class ProductSyncValidationJob
  extend ZendeskJob::Resque::BaseJob

  DEFAULT_JOB_DELAY_SECONDS = 300 # 5 minutes

  priority :low

  class << self
    def args_to_log(account_id)
      { account_id: account_id }
    end

    def work(account_id)
      new(account_id).work
    end

    def job_enqueue_delay
      Integer(ENV.fetch('PRODUCT_SYNC_VALIDATION_JOB_DELAY_SECONDS', DEFAULT_JOB_DELAY_SECONDS)).seconds
    end
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def work
    return unless account.subscription.present? # skip multiproduct accounts without Support product
    account.on_shard do
      Zendesk::Accounts::ProductSyncValidator.validate(account, instrument_results: true) if product_change_expected?
    end
  rescue ActiveRecord::AdapterNotSpecified # account was probably moved recently
  end

  private

  def account
    @account ||= Account.without_locking do
      Account.unscoped.find(@account_id)
    end
  end

  def current_expected_product_attributes
    Zendesk::Accounts::SupportProductMapper.derive_product(account)
  end

  def product_change_expected?
    cache_key = "derived_support_product_md5/#{account.id}"
    previous_expected_product_md5 = Rails.cache.read(cache_key)
    current_expected_product_md5 = Digest::MD5.hexdigest(current_expected_product_attributes.to_s)
    product_change_expected = previous_expected_product_md5 != current_expected_product_md5

    Rails.cache.write(cache_key, current_expected_product_md5) if product_change_expected

    product_change_expected
  end
end
