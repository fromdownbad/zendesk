# requires these options to be passed:
#   account_id
#   user_id
#   request_params
class TicketBulkCreateJob < BulkCreateJob
  priority :medium

  FAILURE_TYPE = 'TicketCreateFailed'.freeze

  def name
    "TicketBulkCreateJob account:#{options[:account_id]} user:#{options[:user_id]}"
  end

  private

  def create_ticket(ticket, index)
    params = options.merge(ticket: ticket)
    initializer = Zendesk::Tickets::V2::Initializer.new(current_account, current_user, params, request_params: request_params)
    new_ticket = initializer.ticket

    begin
      with_retry(RETRY_COUNT, RETRY_INTERVAL, index, new_ticket, FAILURE_TYPE) do
        new_ticket.save!
        at(index + 1, tickets.size, "creating ticket #{index + 1}/#{tickets.size}")
        { index: index, id: new_ticket.nice_id, account_id: current_account.id }
      end
    rescue StandardError => e
      message = "Failed to create ticket #{index + 1}/#{tickets.size}: #{e.message}"
      errors_text = new_ticket.errors.full_messages.join(',') if new_ticket

      internal_error_text = message + '\n ' + e.backtrace.join('\n ') + '\n'
      internal_error_text += 'attributes: ' + new_ticket.try(:attributes).to_s + '\n'
      internal_error_text += 'errors: ' + errors_text
      Rails.logger.error(internal_error_text)

      fail_job(index, FAILURE_TYPE, errors_text)
    end
  end

  def work
    Rails.logger.info("Running TicketBulkCreateJob, ntickets: #{tickets.size}, account_id: #{current_account.id}, shard_id: #{current_account.shard_id}")
    at(0, tickets.size, "Starting ticket creation")

    results = tickets.each_with_index.flat_map do |ticket, index|
      create_ticket(ticket, index)
    end

    completed(results: results)
  end

  def tickets
    @tickets ||= options.delete(:tickets)
  end

  def request_params
    @request_params ||= Zendesk::Tickets::HttpRequestParameters.new(options.delete(:request_params))
  end
end
