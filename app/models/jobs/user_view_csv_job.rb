require 'zip/zip'
require 'zip/zipfilesystem'

class UserViewCsvJob < ThrottleJob
  priority :medium

  PER_PAGE = 1000
  LIMIT = 100_000

  throttle can_run_every: 10.minutes

  class << self
    include Api::V2::Rules::UserViewRowsHelper
    include Api::V2::Rules::UserViewColumnsHelper

    attr_reader :account, :user
    def work(account_id, user_id, user_view_id)
      @account = Account.find(account_id)
      identifier = identifier(account_id, user_id, user_view_id)

      account.on_shard do
        @user = account.users.find(user_id)
        requester = account.users.find(user_id)
        user_view = account.user_views.find(user_view_id)

        I18n.with_locale(requester.translation_locale) do
          csv_file = create_csv_file(user_view, requester)

          attachment = create_expirable_attachment(csv_file, account, requester, user_view)
          attachment_url = attachment.url(token: true, mapped: false)

          subject = I18n.t("txt.admin.models.jobs.user_view_csv_job.subject", title: user_view.title)
          content = I18n.t("txt.admin.models.jobs.user_view_csv_job.message_1")
          content << "\n\n#{attachment_url}\n\n"
          content << I18n.t("txt.admin.models.jobs.user_view_csv_job.message_2")

          JobsMailer.deliver_job_complete(requester, identifier, subject, content)
        end
      end
    end

    def args_to_log(account_id, user_id, user_view_id)
      { account_id: account_id, user_id: user_id, user_view_id: user_view_id }
    end

    def identifier(*args)
      _, user_id, user_view_id = args
      "user_id:#{user_id},user_view_id:#{user_view_id}"
    end

    def create_csv_file(user_view, requester)
      csv_file = Tempfile.new([UserViewCsvJob.name, '.csv'])

      CSV.open(csv_file.path, 'a', col_sep: ',', force_quotes: true) do |csv|
        csv << csv_titles(user_view)

        find_each_user_for_user_view(user_view, requester) do |user|
          csv << csv_row_for_user(user_view, user)
        end
      end

      csv_file
    end

    def csv_titles(user_view)
      user_view.output.columns.map do |column|
        field = user_view.field_from_source(column)
        column_title(column, field)
      end
    end

    def csv_row_for_user(user_view, user)
      custom_field_values = user.custom_field_values.as_json(title: true, account: user_view.account)

      user_view.output.columns.map do |column|
        value = custom_field_values[UserView.custom_field_key(column)]
        value ||= field_value(column, user)
        value = Zendesk::CsvScrubber.scrub_output(value)

        if column.to_s == "organization_id"
          value &&= user_view.account.organizations.find_by_id(value).try(:name)
        elsif column.to_s == "current_tags"
          value = value.join(", ")
        end
        if value.is_a?(Time)
          value.in_time_zone.to_s(:csv)
        else
          value.to_s.to_str.squish.truncate(10000)
        end
      end
    end

    def find_each_user_for_user_view(user_view, requester)
      paginated_users = Zendesk::UserViews::Executer.users_for_view(user_view, requester, page: 1, per_page: per_page, sampling: false)
      total_pages     = paginated_users.total_pages
      count           = 0

      total_pages.times do |i|
        if i > 0
          paginated_users = Zendesk::UserViews::Executer.users_for_view(user_view, requester, page: i + 1, per_page: per_page, sampling: false)
        end
        paginated_users.each do |user|
          yield user
          count += 1
          break if count >= limit
        end
        break if count >= limit
      end
      nil
    end

    def limit
      LIMIT
    end

    def per_page
      PER_PAGE
    end

    def create_expirable_attachment(csv_file, account, requester, user_view)
      zipfile      = Tempfile.new([UserViewCsvJob.name, '.zip'])
      zipfile_name = PermalinkFu.escape("#{user_view.title} view #{Time.now.in_time_zone.to_s(:csv)}")

      Zip::OutputStream.open(zipfile.path) do |io|
        io.put_next_entry(zipfile_name + '.csv')
        io << IO.read(csv_file.path)
      end

      uploaded_data = Zendesk::Attachments::CgiFileWrapper.new(zipfile.path, filename: "#{zipfile_name}-csv.zip")
      attachment = ExpirableAttachment.new(uploaded_data: uploaded_data)
      attachment.account = account
      attachment.author = requester
      attachment.created_via = UserViewCsvJob.name
      attachment.save!
      attachment
    ensure
      zipfile.close
      zipfile.unlink
    end
  end
end
