class SharedTicketBulkDeleteJob
  extend ZendeskJob::Resque::BaseJob

  RETRY_ATTEMPTS = 3
  RETRY_EXCEPTIONS = [ZendeskDatabaseSupport::MappedDatabaseExceptions::MappedDatabaseException].freeze

  priority :low

  class << self
    def work(account_id, agreement_id)
      account = Account.find(account_id)

      ActiveRecord::Base.on_shard(account.shard_id) do
        agreement = find_agreement_with_deleted(agreement_id)

        agreement.shared_tickets.find_in_batches do |shared_tickets|
          while shared_ticket = shared_tickets.pop # tickets get huge after save -> clear them from memory
            begin
              Zendesk::Retrier.retry_on_error(RETRY_EXCEPTIONS, RETRY_ATTEMPTS) do
                shared_ticket.soft_delete!
              end
            rescue *RETRY_EXCEPTIONS, StandardError => e
              resque_log("Shared ticket bulk delete job failed after #{RETRY_ATTEMPTS} tries on account id #{account_id}, shared ticket id #{shared_ticket.id} -- #{e.message}")
            end
          end
        end
      end
    end

    def args_to_log(account_id, agreement_id)
      { account_id: account_id, agreement_id: agreement_id }
    end

    def find_agreement_with_deleted(agreement_id)
      Sharing::Agreement.with_deleted { Sharing::Agreement.find(agreement_id) }
    end
  end
end
