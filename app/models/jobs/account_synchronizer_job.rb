# This job consumes the Provisioning Payload from Billing to provision account,
# support (and other product) subscription records.
#
# Look inside the Provisioning service class for details on which product and
# related records are provisioned.
#
# For details on the content of the payload, see:
# https://github.com/zendesk/billing/wiki/Classic-Provisioning-Payload
#
# This job is expected to execute on the same pod that where the account
# referenced in the payload belongs.
class AccountSynchronizerJob
  extend ZendeskJob::Resque::BaseJob
  extend Resque::Plugins::Retry

  priority :high

  @retry_limit = 3
  @retry_delay = 3600 # retry with hourly interval

  def self.args_to_log(payload)
    { payload: payload }
  end

  def self.work(payload)
    new(payload).work
  end

  def work
    # TODO: We might need to do: JSON.parse(payload) before passing the payload
    # as param to the AccountSynchronizer service.
    AccountSynchronizer.call(payload)
  end

  private

  attr_reader :payload

  def initialize(payload)
    @payload = payload
  end
end
