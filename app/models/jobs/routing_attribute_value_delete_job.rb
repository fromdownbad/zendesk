# requires these options to be passed:
#   account_id
#   user_id
#   attribute_id
#   id
class RoutingAttributeValueDeleteJob < JobWithStatus
  RETRY_ATTEMPTS = 3
  RETRY_EXCEPTIONS = [
    Kragle::ServiceUnavailable,
    Faraday::TimeoutError,
    Faraday::Error::TimeoutError,
    Faraday::ConnectionFailed
  ].freeze
  DECO_TIMEOUT = 60
  FINGERPRINT = 'fc6f9b606eaaf8ef337d70b416542925866ce46c'.freeze

  alias :account :current_account

  priority :medium

  def name
    "RoutingAttributeValueDeleteJob attribute:#{options[:attribute_id]} attribute_value:#{options[:id]}"
  end

  def work
    Rails.logger.info("Deleting attribute value:#{options[:id]} for account ID:#{account.id}")

    Zendesk::Retrier.retry_on_error(RETRY_EXCEPTIONS, RETRY_ATTEMPTS) do
      if attribute_value.delete(DECO_TIMEOUT)
        completed(success_message)
      else
        completed(failure_message)
      end
    end
  rescue *RETRY_EXCEPTIONS => e
    log_exception(e)
    completed(failure_message(RETRY_ATTEMPTS))
  rescue Kragle::ResourceNotFound
    Rails.logger.info(not_found_message)
    completed(not_found_message)
  rescue Kragle::BadRequest => e
    log_exception(e)
    completed(failure_message)
  end

  private

  def deco
    @deco ||= Zendesk::Deco::Client.new(account)
  end

  def attribute_value
    deco.attribute_value(options[:attribute_id], options[:id])
  end

  def success_message
    "Deleted attribute value. Id: #{options[:id]} AttributeId: #{options[:attribute_id]}."
  end

  def failure_message(retries = nil)
    msg = "Failed to delete attribute value. Id: #{options[:id]} AttributeId: #{options[:attribute_id]}."
    msg << " After #{retries} retries." if retries
    msg
  end

  def not_found_message
    "Could not find attribute value. Id: #{options[:id]} AttributeId: #{options[:attribute_id]}."
  end

  def log_exception(exception)
    ZendeskExceptions::Logger.record(
      exception,
      location: self,
      message: "Exception while running #{name} for account #{account.id}: #{exception}",
      fingerprint: FINGERPRINT
    )
  end
end
