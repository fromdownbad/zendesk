class BulkDeleteJob < BulkJob
  def delete_resource(id, resource, resource_name)
    return fail_job(id, resource_name + "NotFound") unless resource

    if resource.destroy
      success
    else
      fail_job(id, resource_name + "DeleteFailed", resource.errors.full_messages.join(' '))
    end
  end
end
