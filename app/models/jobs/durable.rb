require 'resque/durable'
require 'zendesk_jobs'

# Durable jobs are also sharded. The first parameter must be the account's id, or an options hash containing an 'account_id' key.
module Durable
  def self.extended(klass)
    klass.extend Resque::Durable
    klass.extend ZendeskJob::Resque::ShardedJob
    klass.extend Extensions
    klass.singleton_class.prepend Overrides
    klass.auditor = ::QueueAudit
  end

  # Note .extend can not override methods defined in the class itself
  module Overrides
    def background_heartbeat_interval
      # Background heartbeating is done in a thread, which is incompatible with
      # transactional_fixtures in our tests.
      if !Rails.env.test? && Arturo.feature_enabled_for_pod?(:resque_durable_background_heartbeat, Zendesk::Configuration.fetch(:pod_id))
        super
      end
    end
  end

  module Extensions
    AUDIT_RETRY_TIMES = 3 # with a .1 sleep for .3 seconds of retry

    def build_audit(args)
      audit = super
      resque_log("audit.enqueued_id: #{audit.enqueued_id}")
      audit.account_id = args.first
      audit
    end

    def enqueue_and_get_audit(*args)
      args << build_audit(args) unless args.last.is_a?(auditor)
      enqueue(*args)
      args.last
    end

    def enqueue(*args)
      # `log_enqueue` hits zendesk_job environment capture logic, `super` hits Resque::Durable.enqueue
      log_enqueue("now", args) { super }
    end

    def enqueue_in(_delay, *_args)
      raise "resque-durable does not currently provide resque-scheduler compatibility. Implement support upstream in resque-durable gem if you want this feature."
    end

    def enqueue_at(_time, *_args)
      raise "resque-durable does not currently provide resque-scheduler compatibility. Implement support upstream in resque-durable gem if you want this feature."
    end

    def audit_failed(exception, args)
      notify("Durable job: unable to audit job #{name} with args #{args.inspect}", exception)
    end

    def enqueue_failed(exception, args)
      if redis_error?(exception)
        notify("Durable job: unable to enqueue job #{name} with args #{args.inspect}", exception)
      else
        raise exception
      end
    end

    def audit(args)
      # work around some race conditions for durable jobs
      AUDIT_RETRY_TIMES.times do
        audit = auditor.find_by_enqueued_id(args.last)
        return audit if audit

        Rails.logger.info("Can't find audit #{args.last}, retrying", audit: args.last)
        sleep(0.1)
      end

      audit_failed(ArgumentError.new("Could not find audit: #{args.last}"), args)
      nil
    end

    def audit_id(audit_or_id)
      if audit_or_id.respond_to?(:enqueued_id)
        audit_or_id.enqueued_id
      else
        audit_or_id
      end
    end

    def identify(*args)
      Digest::MD5.hexdigest(args[0, -2].to_s)
    end

    def notify(message, exception)
      if Rails.env.test? || Rails.env.development?
        raise exception
      else
        ZendeskExceptions::Logger.record(exception, location: self, message: message, fingerprint: '184c93c305b1795534c16424af3b2bdbcd65214b')
      end
    end

    def redis_error?(exception)
      exception.is_a?(Errno::ECONNREFUSED) && exception.message =~ /Redis/
    end
  end
end
