require 'zendesk/acme_job/error'

class AcmeCertificateJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Durable

  MAX_TRIES = 3
  PROP_KEY = :lets_encrypt_certificates

  attr_reader :account_id, :audit_id, :job_status_id, :activate_certificate

  priority :high

  class << self
    def work(account_id, job_status_id, activate_certificate, audit_id)
      job = new(account_id, job_status_id, activate_certificate, audit_id)
      job.job_status.working!
      job.raise_if_another_instance_running
      job.work
    end

    def args_to_log(account_id, job_status_id, activate_certificate, audit_id = nil)
      {
        account_id: account_id,
        job_status_id: job_status_id,
        activate_certificate: activate_certificate,
        audit_id: audit_id,
      }
    end
  end

  def initialize(account_id, job_status_id, activate_certificate, audit_id)
    @account_id = account_id
    @audit_id = audit_id
    @job_status_id = job_status_id
    @activate_certificate = activate_certificate
    set_audit_id!
  end

  def work
    tries ||= MAX_TRIES
    process_job
  rescue Prop::RateLimited
    Rails.logger.warn "Account #{@account_id} has hit the let's encrypt rate limit. Aborting job"
    statsd_client.increment('count.failed', tags: ['error:rate_limited'])
    # TODO: Enqueue the job to run once the account is no longer rate limited
    fail! $!
  rescue Zendesk::AcmeJob::Error
    tries -= 1
    if tries > 0
      Rails.logger.warn "Caught #{$!} #{tries} remaining; retrying"
      reset
      retry
    else
      fail! $!
      statsd_client.increment('count.failed', tags: ['error:too_many_retries'])
    end
  end

  def raise_if_another_instance_running
    if QueueAudit.where(account_id: account.id, job_klass: 'AcmeCertificateJob', completed_at: nil).where("timeout_at > ?", Time.now).where("enqueued_id != ?", audit.enqueued_id).any?
      Rails.logger.error "[ACME] only one acme certificate job per account is allowed at a time; aborting this job"
      statsd_client.increment('count.failed', tags: ['error:job_already_running'])
      raise Zendesk::AcmeJob::JobAlreadyRunning
    end
  end

  def job_status
    @job_status ||= account.acme_certificate_job_statuses.find(job_status_id)
  end

  def audit
    @audit ||= QueueAudit.where(account_id: account.id).where("enqueued_id = ?", @audit_id).first
  end

  def account
    @account ||= Account.find(account_id)
  end

  def self.rate_limit!(account_id:)
    Prop.throttle!(PROP_KEY, account_id)
  end

  def rate_limit!
    self.class.rate_limit!(account_id: account_id)
  end

  def self.reset_rate_limit!(account_id:)
    Prop.reset(PROP_KEY, account_id)
  end

  private

  def statsd_client
    @client ||= Zendesk::StatsD.client(namespace: 'acme_certificate_job')
  end

  def set_audit_id!
    if job_status.enqueued_id.blank?
      job_status.update_attributes!(enqueued_id: audit_id)
    elsif job_status.enqueued_id != audit_id
      Rails.logger.error "job_status_id is #{job_status.id} but that status has a different audit_id: #{job_status.enqueued_id} then this job #{audit_id} "
      raise "AcmeCertificateJob enqueued with incorrect job_status_id"
    end
  end

  def reset
    @registration = nil
  end

  def process_job
    current_certificate = account.certificates.active.first
    if account.brand_host_mappings.empty?
      Rails.logger.warn("[ACME] certificate job enqueued but account doesn't have any host mappings. Aborting")
      statsd_client.increment('count.failed', tags: ['error:no_host_mappings'])
      job_status.failed!("Account doesn't have any host mappings. Aborting")
    else
      if renew_certificate?(current_certificate)
        renew_certificate_if_needed(current_certificate)
      else
        authorize_and_request_certificate!
      end
      notify_customer_if_needed(current_certificate)
      job_status.completed!
    end
  end

  def renew_certificate?(current_certificate)
    current_certificate && current_certificate.autoprovisioned? &&
      current_certificate.crt_covers_all_domains?
  end

  def existing_acme_certificate
    @existing_acme_certificate ||= find_existing_certificates.sort_by(&:crt_not_after).last
  end

  def find_existing_certificates
    account.certificates.where(autoprovisioned: true, state: 'revoked').select { |c| !AcmeCertificate.needs_renewal?(c) && c.crt_covers_all_domains? }
  end

  def renew_certificate_if_needed(current_certificate)
    acme_certificate.certificate = current_certificate

    if acme_certificate.needs_renewal?
      authorize_and_request_certificate!
      statsd_client.increment('count.success')
    else
      Rails.logger.warn("[ACME] certificate job enqueued but account already has a LE certificate. Aborting")
      statsd_client.increment('count.failed', tags: ['error:no_renewal_needed'])
    end
  end

  def notify_customer_if_needed(current_certificate)
    statsd_tags = { tags: ['success:without_notification'] }

    if activate_certificate && (!current_certificate || !current_certificate.autoprovisioned?)
      notify_customer_of_activation!
      statsd_tags = { tags: ['success:with_notification'] }
    end

    statsd_tags[:tags] << "activated:#{activate_certificate}"
    statsd_client.increment('count.success', statsd_tags)
  end

  def notify_customer_of_activation!
    SecurityMailer.deliver_lets_encrypt_enabled(acme_certificate.certificate)
  end

  def fail!(exception)
    job_status.failed!(exception)
    Rails.logger.error("[ACME] Acme certificate provisioning failed #{exception}")
    Rails.logger.error("[ACME] Acme certificate provisioning failed #{exception.backtrace.join("\n")}")
    ticket = reporter.report_error(account, "Acme certificate provisioning failed", exception: exception)
    account.acme_registrations.first.try { |reg| reg.update_attributes!(ticket_id: ticket.id) }
  end

  def reporter
    @reporter ||= AcmeReporter.new('support', ticket_id: account.acme_registrations.first.try(:ticket_id))
  end

  def acme_certificate
    @acme_certificate ||= AcmeCertificate.new(account: account, client: client)
  end

  def authorize_and_request_certificate!
    rate_limit!

    Rails.logger.info("Requesting certificate covering #{account.brand_host_mappings.join(',')}")

    if account.has_lets_encrypt_acme_v2?
      existing_acme_certificate.present? ? prepare_existing_certificate : request_new_certificate_v2
    else
      existing_acme_certificate.present? ? prepare_existing_certificate : request_new_certificate
    end

    install_acme_certificate
  end

  # Every time a new order is created, let's overwrite and save the new challenges for all existing acme_authorizations
  # and the new ones. And let's request verifications for all the new challenges too, irrespective of whether they were
  # already verified before. This will ensure there is no discrepancy between the acme order urls.

  def request_new_certificate_v2
    register_and_authorize_order!
    check_pending_authorizations!
    request_new_acme_certificate!
  end

  def register_and_authorize_order!
    acme_authorizations = account.brand_host_mappings.map { |d| registration.authorization_for_domain(d) }

    Rails.logger.info("Requesting new acme_order for account- #{account.id}")
    acme_certificate.acme_order = client.new_order(identifiers: account.brand_host_mappings)

    acme_certificate.acme_order.authorizations.each do |authorization|
      acme_auth = acme_authorizations.find { |a| a.identifier == authorization.domain }
      http_challenge = authorization.http

      acme_auth.challenge_type = 'http-01'
      acme_auth.challenge_uri = http_challenge.url
      acme_auth.challenge_token = http_challenge.token
      acme_auth.challenge_content = http_challenge.file_content
      # no need to put failures since default is 0, and you shouldn't reset it here
      acme_auth.save!
      sleep 5 unless Rails.env.test?
      Rails.logger.info("Requesting verification from Lets Encrypt for subdomain #{authorization.domain} on account #{account.id}")
      acme_auth.request_verification_v2!
    end
  # should I rescue all the specific classes here or does it really matter?
  rescue # rubocop:disable Style/RescueStandardError
    raise Zendesk::AcmeJob::AuthorizationError, $!
  end

  def check_pending_authorizations!
    if auths = pending_authorizations_v2.any?
      statsd_client.increment('count.failed', tags: ['error:authorization_pending'])
      raise Zendesk::AcmeJob::AuthorizationPendingError, auths
    end
  end

  def request_new_acme_certificate!
    acme_certificate.request_certificate!
  end

  def request_new_certificate
    if auths = pending_authorizations.any?
      statsd_client.increment('count.failed', tags: ['error:authorization_pending'])
      raise Zendesk::AcmeJob::AuthorizationPendingError, auths
    end

    acme_certificate.request_certificate!
  end

  def install_acme_certificate
    if activate_certificate
      acme_certificate.install_certificate!
    else
      Rails.logger.warn("[ACME] Activation was not requested. Aborting")
      acme_certificate.certificate.state = 'revoked'
      acme_certificate.certificate.save!
    end
  end

  def prepare_existing_certificate
    existing_acme_certificate.state = 'initial'
    acme_certificate.certificate = existing_acme_certificate
  end

  def pending_authorizations
    authorizations = account.brand_host_mappings.map { |d| registration.authorization_for_domain(d) }

    authorizations.each do |auth|
      auth.attempt_to_authorize! unless auth.authorization_requested?
    end

    sleep 10

    authorizations.reject(&:is_authorized?)
  end

  def pending_authorizations_v2
    Rails.logger.info("Checking the current authorization status for the domains #{account.brand_host_mappings} on account #{account.id}")
    authorizations = account.brand_host_mappings.map { |d| registration.authorization_for_domain(d) }
    authorizations.reject(&:is_authorized_v2?)
  end

  def registration
    return @registration if @registration

    @registration = account.acme_registrations.first || account.acme_registrations.create
    @registration.register! unless @registration.registration_valid?
    @registration
  end

  def client
    if account.has_lets_encrypt_acme_v2?
      registration.client_acme_v2
    else
      registration.client
    end
  end
end
