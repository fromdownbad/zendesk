module PushNotificationKillSwitch
  AGENT_APP = 'com.zendesk.agent'.freeze

  # If this function returns false job won't be queued
  def before_enqueue(options, *_args)
    options = options.with_indifferent_access
    mobile_app_identifier = options[:mobile_app_identifier]

    return false if mobile_app_identifier == AGENT_APP # Verifies if the given account does not support the classic mobile app anymore.
    true
  end

  private

  def statsd_client
    @client ||= Zendesk::StatsD::Client.new(namespace: ['jobs', 'push_notifications'])
  end
end
