class CreateFeatureOnLocalPodJob
  extend ZendeskJob::Resque::BaseJob

  priority :medium

  LOG_COUNT = 500

  AVOID_SHARDS = [8999, 9999, 10999, 11999].freeze

  def self.work(pod_id)
    CreateFeatureOnLocalPodJob.new(pod_id).perform
  end

  def self.args_to_log(pod_id)
    { pod_id: pod_id }
  end

  attr_reader :pod_id

  def initialize(pod_id)
    @pod_id = pod_id
  end

  def perform
    conditions   = { is_serviceable: true, is_active: true }
    num_accounts = Account.pod_local.where(conditions).count
    count        = 0

    log("Pod: #{pod_id} / Accounts: #{num_accounts} / Feature backfill"\
      " started")

    Account.pod_local.where(conditions).find_each do |account|
      next if AVOID_SHARDS.include?(account.shard_id)

      account.on_shard do
        Zendesk::Features::SubscriptionFeatureService.new(account,
          fast_execute: true).execute!
      end

      count += 1

      log("Pod: #{pod_id} / Processed #{count} of #{num_accounts} / "\
        "Feature backfill processing") if (count % LOG_COUNT) == 0
    end

    log("Pod: #{pod_id} / Accounts: #{num_accounts} / Feature backfill "\
      "finished")
  end

  def log(msg)
    CreateFeatureOnLocalPodJob.resque_log(msg)
  end
end
