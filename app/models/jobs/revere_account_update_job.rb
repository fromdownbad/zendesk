require 'zendesk/revere/has_api_client'

# An account was changed in such a way that we should notify Revere.

# requires these options to be passed:
#   account_id
class RevereAccountUpdateJob
  extend ZendeskJob::Resque::BaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend Zendesk::Revere::HasApiClient

  priority :low

  class << self
    def work(account_id)
      resque_log("Updating Revere subscriptions for account (#{account_id})")
      account = Account.find(account_id)
      account.on_shard do
        account.revere_subscribers.each do |user|
          # there should be few enough of these that batching or async will never be necessary.
          revere_api_client.update_subscription(user, user.settings.revere_subscription)
        end
      end
    end

    def args_to_log(account_id)
      { account_id: account_id }
    end
  end
end
