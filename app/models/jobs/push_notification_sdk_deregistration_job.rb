class PushNotificationSdkDeregistrationJob < PushNotificationBaseJob
  extend ZendeskJob::Resque::ShardedJob

  priority :medium

  def self.work(options)
    identifier_id = options.with_indifferent_access['identifier_id']
    device_identifier = PushNotifications::DeviceIdentifier.find(identifier_id)
    mobile_sdk_app = device_identifier.mobile_sdk_app
    urban_airship_key = mobile_sdk_app.settings.urban_airship_key
    urban_airship_master_secret = mobile_sdk_app.settings.urban_airship_master_secret

    client = PushNotifications::UrbanAirshipClient.new(
      urban_airship_key, urban_airship_master_secret, statsd_tags: ["name:sdk_deregistration"]
    )

    client.unregister(device_identifier.token, device_identifier.device_type)
    rescue ActiveRecord::RecordNotFound
      account_id = options.with_indifferent_access['account_id']
      Rails.logger.info("Could not find the device id #{identifier_id} for account: #{account_id}")
  end

  def self.args_to_log(options)
    account_id, identifier_id, mobile_sdk_app_id = options.with_indifferent_access.values_at('account_id', 'identifier_id', 'mobile_sdk_app_id')

    { account_id: account_id, device_id: identifier_id, mobile_sdk_app_id: mobile_sdk_app_id}
  end
end
