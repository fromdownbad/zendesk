class TicketBrandReassignJob
  extend ZendeskJob::Resque::BaseJob
  extend Durable

  priority :high

  def self.work(account_id, brand_id, _audit = nil)
    account = Account.find_by_id(account_id)

    if account
      ActiveRecord::Base.on_shard(account.shard_id) do
        assign_default_brand_to_tickets(account, brand_id)
      end
    else
      resque_log("Unable to find account #{account_id}. Assumed deleted")
    end
  end

  def self.args_to_log(account_id, brand_id, audit = nil)
    {
      account_id: account_id,
      brand_id: brand_id,
      audit_id: audit_id(audit)
    }
  end

  def self.assign_default_brand_to_tickets(account, brand_id)
    new_brand_id = account.default_brand_id

    Rails.logger.info("Running TicketBrandReassignJob to assign tickets from brand #{brand_id} to brand #{new_brand_id}, account: #{account.id}, shard_id: #{account.shard_id}")

    updated = 1
    while updated > 0
      updated = account.tickets.not_closed.where(brand_id: brand_id).limit(500).update_all_with_updated_at(brand_id: new_brand_id)
    end
  end
  private_class_method :assign_default_brand_to_tickets
end
