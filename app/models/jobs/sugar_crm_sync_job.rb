class SugarCrmSyncJob
  extend ZendeskJob::Resque::BaseJob

  priority :high

  def self.work(account_id, user_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      begin
        user = User.find(user_id)
        data = account.sugar_crm_integration.fetch_info_for(user)
        user.sugar_crm_data.sync!(data)
      rescue StandardError => e
        user.sugar_crm_data.sync_errored!(records: [])
        resque_warn("SugarCRM Integration error during fetch info for user (account: #{account_id}, user: #{user_id}): " + e.message)
      end
    end
  end

  def self.args_to_log(account_id, user_id)
    { account_id: account_id, user_id: user_id }
  end
end
