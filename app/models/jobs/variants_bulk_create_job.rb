# requires these options to be passed:
#   account_id
#   user_id
class VariantsBulkCreateJob < VariantJob
  priority :immediate

  def name
    "VariantBulkCreateJob account: #{options[:account_id]}"
  end

  private

  def work
    total = variants.size
    at(0, total, 'Creating Variants')

    results = variants.each_with_index.map do |variant, index|
      at(index + 1, total, 'Creating Variant ##{index + 1}')

      new_variant = initializer.new_variant(variant)
      if new_variant.save
        DC::Synchronizer.synchronize_snippet(initializer.item)
        {id: new_variant.id, status: "Created"}
      else
        {status: "Failed Creating variant #{variant[:content]}", errors: new_variant.errors.full_messages.join(',')}
      end
    end

    completed(results: results)
  end
end
