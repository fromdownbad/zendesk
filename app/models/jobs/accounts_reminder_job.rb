class AccountsReminderJob
  extend ZendeskJob::Resque::BaseJob

  priority :low

  def self.work(email, locale, _account_type = nil)
    new(email, locale).send_email
  end

  def self.args_to_log(email, locale, account_type = nil)
    { email: email, locale: locale, account_type: account_type }
  end

  def initialize(email, locale)
    @sanitized_email = Zendesk::Mail::Address.sanitize(email)
    @locale = locale
  end

  def send_email
    delivery_account.on_shard do
      if accounts.empty?
        UsersMailer.deliver_reminder_no_accounts(
          sanitized_email,
          locale,          # Needed because there is no Account to use for locale
          delivery_account # Hack to satisfy deliver method's dependency :(
        )
      else
        UsersMailer.deliver_reminder(sanitized_email, delivery_account, accounts)
      end
    end
  end

  private

  def delivery_account
    @delivery_account ||= Account.pod_local.active.serviceable.shard_unlocked.first
  end

  attr_reader :sanitized_email, :locale

  def accounts
    @accounts ||= contacts.map(&:account).compact.uniq.select(&:is_active?)
  end

  def contacts
    UserContactInformation.where(email: @sanitized_email)
  end
end
