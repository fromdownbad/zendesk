class PushNotificationSendJob < PushNotificationBaseJob
  extend ZendeskJob::Resque::ShardedJob
  extend PushNotificationKillSwitch

  URBAN_AIRSHIP = 'urban_airship'.freeze
  AMAZON_SNS = 'amazon_sns'.freeze
  GCM = 'gcm'.freeze

  priority :medium

  def self.work(options)
    options = options.with_indifferent_access
    account = Account.find(options[:account_id])
    device_ids, payload = options.values_at(:device_ids, :payload)

    gateway = gateway(account, options)
    if gateway == AMAZON_SNS
      process_via_amazon_sns(account, device_ids, payload)
      # Zendesk::PushNotifications::Adapters::SNS handles logging and metric incrementation itself
    else
      adapter = client(gateway, options)
      responses = adapter.push(
        device_ids,
        payload,
        enable_collapse_key: true
      )
      responses.each do |response|
        unless response.success?
          Rails.logger.error("Device push failed -- #{device_ids.inspect} -- #{payload.inspect} -- #{response.inspect}")
          statsd_client.increment('device_push.failed', tags: [gateway])
        end
      end
    end
  end

  def self.process_via_amazon_sns(account, device_ids, payload)
    device_identifiers = PushNotifications::DeviceIdentifier.where(id: device_ids)
    device_identifiers.each do |device_identifier|
      next if device_identifier.read_attribute(:token_type) == PushNotifications::DeviceIdentifier::TOKEN_TYPE_UA_CHANNEL
      client = sns_client
      register_device_to_sns(client, device_identifier)
      push_payload = build_sns_push_payload(account, device_identifier, payload)
      client.push(
        device_identifier_id: device_identifier.id,
        device_type: device_identifier.device_type.downcase,
        endpoint_arn: device_identifier.sns_target_arn,
        payload: push_payload
      )
    end
  end

  def self.client(gateway, options)
    mobile_app_identifier = options[:mobile_app_identifier]

    case gateway
    when URBAN_AIRSHIP
      config = urban_airship_config(mobile_app_identifier)
      PushNotifications::UrbanAirshipClient.new(config['app_key'], config['master_secret'])
    when GCM
      config = google_cloud_messaging_config(mobile_app_identifier)
      PushNotifications::GCMClient.new(config)
    end
  end

  def self.gateway(account, options)
    if account.has_sns_push_notifications?
      AMAZON_SNS
    else
      options.fetch(:gateway, URBAN_AIRSHIP)
    end
  end

  def self.register_device_to_sns(client, device)
    if device.sns_target_arn.blank? && device.ios?
      client.register(
        mobile_app_identifier: device.mobile_app.identifier,
        device_type: device.device_type,
        device_identifier_id: device.id,
        device_token: device.token
      )
      device.reload
    end
  end

  def self.build_sns_push_payload(_account, device_identifier, payload)
    push_payload_params = {
      alert: payload[:msg],
      badge: device_identifier.badge_count + 1,
      extra_payload: {
        ticket_id: payload[:ticket_id],
        user_id: payload[:user_id],
        enable_collapse_key: true
      }
    }
    generate_sns_push_payload(device_identifier, push_payload_params)
  end

  def self.generate_sns_push_payload(device_identifier, payload_params)
    payload_adapter =
      if device_identifier.ios?
        Zendesk::PushNotifications::Payloads::APNS.new(payload_params)
      elsif device_identifier.android?
        Zendesk::PushNotifications::Payloads::GCM.new(payload_params)
      end
    payload_adapter.payload
  end

  def self.sns_client
    Zendesk::PushNotifications::Adapters::SNS.new
  end

  def self.args_to_log(options)
    options.slice(:device_ids, :payload, :mobile_app_identifier)
  end

  private_class_method :sns_client, :register_device_to_sns, :generate_sns_push_payload, :build_sns_push_payload
end
