require 'friendly_id'
require 'zendesk/search'

class Category < ActiveRecord::Base
  include Zendesk::Serialization::CategorySerialization
  include SlugIds
  include CachingObserver
  include ContentMappingSupport

  has_soft_deletion default_scope: true
  # Will stack with soft_deletion's scope
  default_scope { order('position, name') }

  belongs_to :account

  has_many :forums, -> { order(:position) }, dependent: :nullify, inherit: :account_id
  has_many :entries, through: :forums, inherit: :account_id

  attr_accessible :account, :name, :description, :position

  validates_presence_of :account_id, :name
  validates_uniqueness_of :name, scope: [:account_id, :deleted_at], case_sensitive: false

  before_create :set_defaults_before_create

  def url
    "#{account.url}/categories/#{to_param}"
  end

  def to_param
    slug_id(self)
  end

  private

  def set_defaults_before_create
    self.position = 9999
  end
end
