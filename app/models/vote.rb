class Vote < ActiveRecord::Base
  include ForumObserver

  belongs_to :entry, counter_cache: true, inherit: :account_id
  belongs_to :user
  belongs_to :account

  attr_accessible :entry, :user

  before_validation :set_account

  validates :entry_id, :user_id, :account_id, presence: true
  validates :user_id, uniqueness: { scope: :entry_id }

  has_soft_deletion default_scope: true

  # SHARDING: This is only necessary for the transitional period until the account_id has been added to all rows
  alias_method :account_association, :account
  def account
    account_association || set_account
  end

  private

  def set_account
    self.account = user.account if user.present?
  end
end
