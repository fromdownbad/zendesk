# This class is used to Fake-Scrub a ticket in the incremental API.
#   Hence a ticket that should have been scrubbed but wasn't because a subpoena is not exposing data

class FakeScrubbedTicket < Ticket
  def scrubbed?
    true
  end

  def description
    'X'
  end

  def current_tags
    ""
  end

  def collaborators
    []
  end

  def recipient
    'X'
  end
end
