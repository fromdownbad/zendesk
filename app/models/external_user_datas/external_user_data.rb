require_dependency 'zendesk/crm/external_data'

class ExternalUserData < ActiveRecord::Base
  include Zendesk::Crm::ExternalData

  serialize :data, Hash
  belongs_to :user
  belongs_to :account

  self.table_name = 'external_user_datas'

  attr_accessible :user, :sync_status, :data

  before_validation :set_account_id
  validates_presence_of :user, :account_id

  private

  def set_account_id
    self.account_id = user.account_id
  end
end
