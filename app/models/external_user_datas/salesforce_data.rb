require_dependency '../../lib/zendesk/salesforce/app_settings'

class SalesforceData < ExternalUserData
  include Zendesk::Serialization::CrmDataSerialization
  include Salesforce::AppSettings

  def start_sync(user)
    self.user = user
    sync_pending!
    SalesforceSyncJob.enqueue(user.account_id, user.id)
  end
end
