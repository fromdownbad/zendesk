class SugarCrmData < ExternalUserData
  include Zendesk::Serialization::CrmDataSerialization

  TTL = if !Rails.env.production?
    20.seconds
  else
    1.hour
  end

  def stale?
    updated_at < TTL.ago
  end

  def start_sync(user)
    sync_pending!
    SugarCrmSyncJob.enqueue(user.account_id, user.id)
  end

  def records
    data && data[:records]
  end
end
