class UserPhoneAttribute < ActiveRecord::Base
  TYPE = {
    extension: 1,
    sms_capability: 2
  }.freeze

  attr_accessible :account, :value

  belongs_to :account
  belongs_to :user, inherit: :account_id
  belongs_to :user_phone_number_identity, inherit: :account_id
end
