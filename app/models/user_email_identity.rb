require 'zendesk/models/user_email_identity'
require_relative '../../lib/zendesk/inbound_mail'

class UserEmailIdentity < UserIdentity
  include Zendesk::Serialization::UserEmailIdentitySerialization
  include Zendesk::Constants

  EMAIL_BLACKLIST = Set[
    "noreply@zopim.com", # ZD#3678700
  ].freeze

  has_one :google_profile, foreign_key: 'user_identity_id', dependent: :destroy, inherit: :account_id

  scope :delivery_failed, -> { where("undeliverable_count > 0") }
  scope :ticket_sharing_partners, -> { where(deliverable_state: DeliverableStateType.TICKET_SHARING_PARTNER) }

  validate :value_is_an_email_address
  validate :value_is_not_in_use_by_account
  validate :value_is_not_in_email_blacklist,
    if: ->(record) { new_record? || record.value_changed? }
  validate :value_is_not_a_domain_invalid_address,
    if: ->(record) { new_record? || record.value_changed? }

  before_validation :downcase_address
  before_save :refresh_deliverable_state,
    if: ->(record) { new_record? || record.value_changed? }

  before_save :extract_email_domain

  delegate :zopim_identity, to: :user
  before_update :update_zopim_identity_email,
    if: ->(record) { (record.value_changed? && record.primary?) || record.changed_to_primary? }

  after_create :expire_cache
  after_commit :send_initial_verification_email, on: :create, if: :persisted?
  after_commit :enqueue_fraud_score_job, on: :update, if: :enqueue_fraud_score_job?

  after_update :delete_password_verification_tokens,
    if: ->(record) { record.value_changed? }
  after_update :delete_unverified_ticket_creations,
    if: ->(record) { record.is_verified_changed? && record.is_verified? }

  after_destroy :delete_password_verification_tokens

  include CIA::Auditable
  audit_attribute if: :auditable_identity?

  class << self
    def identity_type
      :email
    end

    # Create a new identity or assume an existing one.
    #
    # If the email address passed with `:email` already exists, the UserEmailIdentity
    # associated with it will be transferred to the User specified with User.
    # 2013/10/16 - Unless that email belongs to an agent
    #
    # If it doesn't exist, it will be created.
    #
    # Options
    #
    # :email - the String email address of the UserEmailIdentity.
    # :user  - the User that should assume the found or created UserEmailIdentity.
    #
    # Returns the UserEmailIdentity that was either found or created.
    def create_or_assume_identity(options = {})
      user  = options.fetch(:user)
      email = options.fetch(:email)

      account = user.account

      if identity = find_by_value_and_account_id(email, account.id)
        identity.transfer_to_user(user) unless identity.user.is_agent?
        identity
      else
        create(user: user, value: email, is_verified: true)
      end
    end

    def reset_undeliverable_count!(values)
      identities = delivery_failed.where(value: values)
      identities.each(&:reset_undeliverable_count!)
      identities
    end

    def increment_undeliverable_count!(values)
      where(value: values).each(&:increment_undeliverable_count!)
    end

    def type_condition_sql
      type_condition.to_sql
    end

    def domain_invalid_address?(address)
      !!/@.*\.invalid$/i.match(address)
    end
  end

  def send_verification_email
    return unless user_portal_state.send_identity_verification_email?(self)
    deliver_verification_email
  end

  def increment_undeliverable_count!
    if reached_undeliverable_count_max?
      Rails.logger.info("Not incrementing undeliverable count (already at #{Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT}) for UserEmailIdentity: #{id}, email: #{value}")
      return
    end

    increment!(:undeliverable_count)
    Rails.logger.info("Incremented undeliverable count to #{undeliverable_count} for UserEmailIdentity: #{id}, email: #{value}")
    mark_as_undeliverable if reached_undeliverable_count_max? && deliverable?
  end

  def reset_undeliverable_count!
    Rails.logger.info("Resetting undeliverable count for UserEmailIdentity: #{id}, email: #{value}") if undeliverable_count > 0
    update_attribute(:undeliverable_count, 0)
  end

  # Returns the name for the DeliverableStateType, or 'uknown' for anything else.
  #   - 1: 'deliverable'
  #   - 2: 'ticket_sharing_partner'
  #   - 3: 'mailing_list'
  #   - 4: 'reserved_example'
  #   - 5: 'undeliverable'
  #   - 6: 'machine'
  #   - 7: 'mailer_daemon'
  def deliverable_state_name
    return 'unknown' if !deliverable_state || !deliverable_state.to_i.between?(1, 7)
    DeliverableStateType[deliverable_state].try(:name)
  end

  def mark_deliverable_state(type)
    if new_record?
      Rails.logger.info("Setting UserEmailIdentity##{id} email #{value} to #{DeliverableStateType[type]} without saving")
      self.deliverable_state = type
    else
      Rails.logger.info("Setting UserEmailIdentity##{id} email #{value} to #{DeliverableStateType[type]} by updating the attribute")
      update_attribute(:deliverable_state, type)
    end
  end

  def mark_as_ticket_sharing_partner
    mark_deliverable_state(DeliverableStateType.TICKET_SHARING_PARTNER)
  end

  def mark_as_mailer_daemon
    mark_deliverable_state(DeliverableStateType.MAILER_DAEMON)
  end

  def mark_as_reserved_example
    mark_deliverable_state(DeliverableStateType.RESERVED_EXAMPLE)
  end

  def mark_as_mailing_list
    mark_deliverable_state(DeliverableStateType.MAILING_LIST)
  end

  def mark_as_machine
    mark_deliverable_state(DeliverableStateType.MACHINE)
  end

  def mark_as_undeliverable
    mark_deliverable_state(DeliverableStateType.UNDELIVERABLE)
  end

  def mark_deliverable
    return unless deliverable_state == DeliverableStateType.UNDELIVERABLE

    # Was marked undeliverable due to soft bounces - decrementing will also set to deliverable
    if reached_undeliverable_count_max?
      decrement_undeliverable_count!
      return
    end

    # Was marked undeliverable due to a hard bounce, so mark it deliverable here
    mark_deliverable_state(DeliverableStateType.DELIVERABLE)
  end

  def identity_type
    google? ? 'google' : 'email'
  end

  def verify
    unless is_verified?
      update_attribute(:is_verified, true)
    end
  end

  def set_primary_when_verifying!
    @make_primary = true
  end

  def google?
    google_profile.present?
  end

  def email?
    !google?
  end

  # Remove when email_suppress_invalid_recipients has been disabled.
  def invalid_recipient?
    return false if allow_invalid_addresses?

    # Invalid address OR using RFC reserved domains
    !!(value =~ /(mailer-daemon|@(.*\.)?example\.(com|net|org)$)/i)
  end

  def reserved_example?
    return false if allow_invalid_addresses?
    !!/@(.*\.)?example\.(com|net|org|edu)$/i.match(value)
  end

  def mailer_daemon?
    return false if allow_invalid_addresses?
    !!/mailer-daemon/i.match(value)
  end

  def machine?
    deliverable_state == DeliverableStateType.MACHINE
  end

  def ticket_sharing_partner?
    account.ticket_sharing_partner_support_addresses.include?(value)
  end

  def verification_token
    verification_tokens.create!(source: self) if verification_tokens.empty?
    verification_tokens.first.value
  end

  def send_initial_verification_email
    return unless user_portal_state.send_initial_identity_verification_email?(self)
    deliver_verification_email
  end

  def make_primary_and_destroy_former!
    former_primary_email_identity = user.identities.detect(&:primary?)
    make_primary!
    former_primary_email_identity.try(:destroy)
  end

  private

  def reached_undeliverable_count_max?
    undeliverable_count >= Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT
  end

  def update_zopim_identity_email
    return unless zopim_identity.present?
    email = user.zopim_email_format_for_value(value)
    zopim_identity.update_attribute(:email, email)
  end

  def user_portal_state
    @user_portal_state ||= Zendesk::UserPortalState.new(user.account)
  end

  def deliver_verification_email # the old send_verification_email
    if user.via == ProvisioningChannel::GOOGLE_APP_MARKET
      WelcomeAgentMailer.deliver_google_verify(self)
    elsif account.multiproduct? && !user.is_end_user?
      UsersMailer.deliver_verify_multiproduct_staff(self, @make_primary)
    else
      UsersMailer.deliver_verify(
        self,
        user.verify_email_text,
        user.verify_email_subject,
        nil,
        user.active_brand_id,
        @make_primary
      )
    end
  end

  def value_is_an_email_address
    unless Zendesk::Mail::Address.valid_address?(value)
      errors.add(:value, I18n.t('activerecord.errors.models.user_identity.attributes.value.invalid', value: CGI.escapeHTML(value)))
    end
  end

  def clean_value
    Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(value)
  end

  def value_is_not_in_use_by_account
    return unless value
    if error = account.email_address_in_use_error(clean_value)
      errors.add(:value, Api.error(error, error: "ReservedValue", raw: true))
    elsif account.default_hosts.include?(email_domain)
      errors.add(:value, Api.error("activerecord.errors.models.user_identity.attributes.value.in_use_as_a_support_address", error: "ReservedValue", value: value))
    end
  end

  def value_is_not_in_email_blacklist
    return if user.skip_email_identity_blacklist_validation

    if EMAIL_BLACKLIST.include?(value)
      errors.add(:value, I18n.t('activerecord.errors.models.user_identity.attributes.value.restricted_address', value: value))
    end
  end

  def value_is_not_a_domain_invalid_address
    if value && self.class.domain_invalid_address?(value)
      errors.add(:value, I18n.t('activerecord.errors.models.user_identity.attributes.value.invalid_address', value: value))
    end
  end

  def downcase_address
    self.value = value.downcase if value
  end

  def expire_cache
    account.expire_scoped_cache_key(:views)
  end

  def auditable_identity?
    # Audit only secondary email addresses
    existing_email = user.identities.email.first
    existing_email && existing_email != self
  end

  def delete_password_verification_tokens
    user.delete_password_verification_tokens
  end

  def delete_unverified_ticket_creations
    user.unverified_ticket_creations.where(from_address: value).delete_all
  end

  def refresh_deliverable_state
    Rails.logger.info "Refreshing deliverable state for UserEmailIdentity ##{id}, email: #{value}"

    # Note: check Users::Identification#invalid_recipient? too.
    self.deliverable_state =
      if ticket_sharing_partner?
        DeliverableStateType.TICKET_SHARING_PARTNER
      elsif reserved_example?
        DeliverableStateType.RESERVED_EXAMPLE
      elsif mailer_daemon?
        DeliverableStateType.MAILER_DAEMON
      elsif machine?
        DeliverableStateType.MACHINE
      else
        DeliverableStateType.DELIVERABLE
      end
  end

  def allow_invalid_addresses?
    # Allow reserved example and mailer-daemon addresses for tests only.
    Rails.env.test?
  end

  def decrement_undeliverable_count!
    if undeliverable_count == 0
      Rails.logger.info("Not decrementing undeliverable count (already at 0) for UserEmailIdentity: #{id}, email: #{value}")
      return
    end

    decrement!(:undeliverable_count)
    Rails.logger.info("Decremented undeliverable count to #{undeliverable_count} for UserEmailIdentity ##{id}, email: #{value}")

    if undeliverable_count < Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT && deliverable_state == DeliverableStateType.UNDELIVERABLE
      mark_deliverable_state(DeliverableStateType.DELIVERABLE)
    end
  end

  def email_domain
    value.to_s.split('@').last
  end

  def extract_email_domain
    self.domain = email_domain
  end

  def enqueue_fraud_score_job?
    account.has_orca_classic_fraud_score_job_update_owner_email? && updated_trial_account_owner_email?
  end

  def enqueue_fraud_score_job
    unless account.whitelisted?
      FraudScoreJob.enqueue_account(account, Fraud::SourceEvent::UPDATE_OWNER_EMAIL)
    end
  rescue StandardError => e
    Rails.logger.error("Error occurred attempting to enqueue fraud score job: #{e}")
  end

  def updated_owner_email?
    return false unless user == account.owner && previous_changes[:value]&.size == 2
    _, old_value = previous_changes[:value]
    old_value != PreAccountCreation::OWNER_EMAIL_PLACEHOLDER
  end

  def updated_trial_account_owner_email?
    account.is_trial? && updated_owner_email?
  end
end
