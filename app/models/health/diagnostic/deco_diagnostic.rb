module Health
  module Diagnostic
    class DecoDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        active_account_in_pod ? deco_client.base_url : 'No active account found in pod'
      end

      def check
        return failed_result("Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}") unless active_account_in_pod

        # Can we retrieve the list of attributes?
        deco_client.attributes.index
        passed_result('Successfully retrieved attribute list from Deco')
      rescue StandardError => e
        failed_result("Deco check failed with error #{e.message}")
      end

      private

      def deco_client
        @deco_client ||= Zendesk::Deco::Client.new(active_account_in_pod)
      end

      def active_account_in_pod
        @active_account_in_pod ||= Account.pod_local.active.serviceable.shard_unlocked.first
      end
    end
  end
end
