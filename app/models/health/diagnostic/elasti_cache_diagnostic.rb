module Health
  module Diagnostic
    class ElastiCacheDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        %w[PROP_ELASTICACHE_ENDPOINT KASKET_ELASTICACHE_ENDPOINT DALLI_ELASTICACHE_ENDPOINT].map do |key|
          ENV[key]
        end.compact.uniq.join(', ')
      end

      def check
        return passed_result('ElastiCache not used') unless using_elasticache

        [
          [using_prop_elasticache, 'PROP_ELASTICACHE_ENDPOINT', 'props'],
          [using_kasket_elasticache, 'KASKET_ELASTICACHE_ENDPOINT', 'kasket'],
          [using_dalli_elasticache, 'DALLI_ELASTICACHE_ENDPOINT', 'dalli']
        ].each do |using_elasticache, elasticache_endpoint_env_var, name|
          next unless using_elasticache
          endpoint = ENV.fetch(elasticache_endpoint_env_var)
          elasticache = Dalli::ElastiCache.new(endpoint)
          return failed_result("No ElastiCache servers found for #{name} (#{endpoint})") unless elasticache.servers.count > 0
        end

        passed_result('Successfully validated ElastiCache')
      rescue StandardError => e
        failed_result("ElastiCache check failed with error #{e.message}")
      end

      private

      def using_elasticache
        [using_prop_elasticache, using_kasket_elasticache, using_dalli_elasticache].any?
      end

      def using_prop_elasticache
        return @using_prop_elasticache if defined? @using_prop_elasticache
        @using_prop_elasticache = ActiveRecord::Type::Boolean.new.cast(ENV['PROP_USE_ELASTICACHE'])
      end

      def using_kasket_elasticache
        return @using_kasket_elasticache if defined? @using_kasket_elasticache
        @using_kasket_elasticache = ActiveRecord::Type::Boolean.new.cast(ENV['KASKET_USE_ELASTICACHE'])
      end

      def using_dalli_elasticache
        return @using_dalli_elasticache if defined? @using_dalli_elasticache
        @using_dalli_elasticache = ActiveRecord::Type::Boolean.new.cast(ENV['USE_DALLI_ELASTICACHE'])
      end
    end
  end
end
