module Health
  module Diagnostic
    class AccountFraudServiceDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        "#{Fraud::FraudServiceClient.build_url_prefix}/z/diagnostic"
      end

      def check
        response = Net::HTTP.get_response(URI(resource))

        if response.is_a?(Net::HTTPSuccess)
          passed_result(
            "Account Fraud service is accessible: #{response.body}"
          )
        else
          failed_result(
            "Account Fraud service is inaccessible: #{response.code} -- " \
            "#{response.body}"
          )
        end
      rescue StandardError => error
        failed_result("Account Fraud service is inaccessible: #{error.message}")
      end
    end
  end
end
