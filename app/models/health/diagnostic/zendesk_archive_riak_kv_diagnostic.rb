module Health
  module Diagnostic
    class ZendeskArchiveRiakKvDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        return '' unless ZendeskArchive.riak_kv.try(:riak_client)

        nodes = ZendeskArchive.riak_kv.riak_client.nodes || []
        nodes.any? ? nodes.map { |node| "#{node.host}:#{node.pb_port}" }.join(', ') : ''
      end

      def check
        return failed_result('Unable to access ZendeskArchive.riak_kv') unless ZendeskArchive.riak_kv
        return failed_result('Unable to access ZendeskArchive.riak_kv.riak_client') unless ZendeskArchive.riak_kv.riak_client
        return failed_result('Unable to access ZendeskArchive.riak_kv.bucket') unless ZendeskArchive.riak_kv.bucket.try(:name)
        return failed_result('Unable to access ZendeskArchive.riak_kv.key_version') unless ZendeskArchive.riak_kv.key_version

        # Generate a fake stub key to check for existance: Riak will look for key 'tickets_fakeid'
        fake_diagnostic_stub_record = OpenStruct.new(id: 'fakeid', account_id: 'fake', key_version: 1)
        ZendeskArchive.riak_kv.exists?(fake_diagnostic_stub_record)

        passed_result('Successfully retrieved information from ZendeskArchive Riak KV store')
      rescue StandardError => e
        failed_result("ZendeskArchive Riak Kv check failed with error #{e.message}")
      end
    end
  end
end
