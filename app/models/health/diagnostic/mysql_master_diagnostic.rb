# A diagnostic check to confirm that the MySQL master database is accessible

module Health
  module Diagnostic
    class MysqlMasterDiagnostic < MysqlDiagnostic
      def resource
        resource_name
      rescue
        'unknown'
      end

      def perform_select
        ActiveRecord::Base.on_master do
          execute_test_query
        end
      end
    end
  end
end
