# A diagnostic check to confirm that a MySQL database is accessible

module Health
  module Diagnostic
    class MysqlDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      include AbstractType

      abstract_method :perform_select

      def check
        select_output = perform_select
        if select_output == 1
          passed_result('MySQL connection is working')
        else
          failed_result("Select was expected to return '1' but returned '#{select_output}'")
        end
      rescue => e
        failed_result("Select failed with error #{e.message}")
      end

      private

      def resource_name
        opts = ActiveRecord::Base.connection.raw_connection.query_options
        "mysql://#{opts[:host]}:#{opts[:port]}"
      end

      def execute_test_query
        ActiveRecord::Base.connection.select_value('SELECT 1 AS val')
      end
    end
  end
end
