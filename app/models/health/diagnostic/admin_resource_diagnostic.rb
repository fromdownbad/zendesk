require 'json-schema'

# Base class for diagnostic checks that confirm the registration of
# admin resources (CSS and JS) in Consul.
module Health
  module Diagnostic
    class AdminResourceDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      include AbstractType

      # The name of the resource, e.g. `routing-admin`, which is used in the
      # Consul query.
      abstract_method :resource_name

      # The name of this diagnostic check, for display to the caller
      abstract_method :check_name

      MANIFEST_SCHEMA = {
        'type' => 'object',
        'required' => ['version', 'js', 'css'],
        'properties' => {
          'version' => {
            'type' => 'object',
            'required' => ['sha', 'tag', 'timestamp'],
            'properties' => {
              'sha' => { 'type' => 'string' },
              'tag' => { 'type' => 'string' },
              'timestamp' => { 'type' => 'string' }
            }
          },
          'js' => {'$ref' => '#/definitions/file_info' },
          'css' => { '$ref' => '#/definitions/file_info' }
        },
        'definitions' => {
          'file_info' => {
            'type' => 'object',
            'required' => ['path', 'fingerprint'],
            'properties' => {
              'path' => { 'type' => 'string' },
              'fingerprint' => { 'type' => 'string' }
            }
          }
        }
      }.freeze

      def resource
        "http://#{consul_hostname}/v1/kv/#{resource_name}/pod#{pod_id}/manifest.json?raw"
      end

      def check
        response = Net::HTTP.get_response(URI(resource))

        if response.is_a?(Net::HTTPSuccess)
          manifest = JSON.parse(response.body)
          validation_error = JSON::Validator.fully_validate(MANIFEST_SCHEMA, manifest)
          return failed_result("#{check_name} manifest invalid: #{validation_error}") unless validation_error.empty?

          passed_result("#{check_name} manifest is valid")
        else
          failed_result("#{check_name} manifest is inaccessible: #{response.code} #{response.message}")
        end
      rescue JSON::ParserError
        failed_result("#{check_name} manifest unparseable")
      rescue StandardError => error
        failed_result("#{check_name} manifest is inaccessible: #{error.message}")
      end

      def pod_id
        Zendesk::Configuration.fetch(:pod_id)
      end

      def consul_hostname
        ENV.fetch('CONSUL_HTTP_ADDR', 'localhost:8500')
      end
    end
  end
end
