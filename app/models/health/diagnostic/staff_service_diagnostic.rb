module Health
  module Diagnostic
    class StaffServiceDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        'http://staff-service.service.consul:12080/z/ping'
      end

      def check
        response = Net::HTTP.get_response(URI(resource))

        if response.code == "200"
          passed_result('Staff service is accessible')
        else
          failed_result('Staff service is inaccessible')
        end
      rescue StandardError => error
        failed_result("Staff service is inaccessible: #{error.message}")
      end
    end
  end
end
