require 'json-schema'

module Health
  module Diagnostic
    class SupportRoutingAdminResourceDiagnostic < AdminResourceDiagnostic
      def resource_name
        'routing-admin'
      end

      def check_name
        'Support routing admin'
      end
    end
  end
end
