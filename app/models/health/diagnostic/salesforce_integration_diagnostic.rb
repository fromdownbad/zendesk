module Health
  module Diagnostic
    class SalesforceIntegrationDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        ::Zendesk::Configuration.dig(:salesforce, :oauth_options, 'site')
      end

      def check
        config_options = ::Zendesk::Configuration.dig(:salesforce, :oauth_options)

        oauth_options = {
          site: config_options['site'],
          authorize_url: '/services/oauth2/authorize',
          token_url: '/services/oauth2/token',
          raise_errors: false
        }

        key = ::Zendesk.config.fetch(:salesforce)['consumer_key']
        secret = ::Zendesk.config.fetch(:salesforce)['consumer_secret']
        client = OAuth2::Client.new(key, secret, oauth_options)

        host = "support.#{::Zendesk.config[:host]}"
        port = ::Zendesk.config[:port].present? ? ":#{::Zendesk.config[:port]}" : nil
        static_callback_url = "https://#{host}#{port}/ping/redirect_to_account"

        client.auth_code.get_token('code', redirect_uri: static_callback_url)
        # get_token.params is {"error"=>"invalid_grant", "error_description"=>"invalid authorization code"}
        # but we don't really need to validate that.

        passed_result('Successfully validated Salesforce Integration')
      rescue StandardError => e
        failed_result("Salesforce Integration check failed with error #{e.message}")
      end
    end
  end
end
