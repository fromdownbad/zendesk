module Health
  module Diagnostic
    class AccountServiceDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        active_account_in_pod ? "#{account_client.url_prefix}#{account_client.products_url}" : ''
      end

      def check
        return failed_result("Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}") unless active_account_in_pod

        # Can we ask about the `guide` product?
        account_client.product!('guide')
        passed_result('Successfully retrieved account info from account service')
      rescue StandardError => e
        failed_result("Account service check failed with error #{e.message}")
      end

      private

      def account_client
        account = active_account_in_pod
        @account_client ||= Zendesk::Accounts::Client.new(account)
      end

      def active_account_in_pod
        @active_account_in_pod ||= Account.pod_local.active.serviceable.shard_unlocked.first
      end
    end
  end
end
