module Health
  module Diagnostic
    class VoyagerDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        return Zendesk::VoyagerClientBuilder.location(active_account_in_pod) if active_account_in_pod
        ''
      end

      def check
        return failed_result("Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}") unless active_account_in_pod
        ActiveRecord::Base.on_shard active_account_in_pod.shard_id do
          # List exports and validate that the response is legit (response code 200)
          export_params = {
            page: 0,
            per_page: 1,
            sort_order: 'DESC',
            all: true
          }
          response = voyager_client.list_exports(export_params)

          return failed_result("Error accessing Voyager service: list_exports returned #{response.errors} (#{response.status})") unless response.successful?

          passed_result('Successfully created export in Voyager service')
        end
      rescue StandardError => error
        failed_result("Error accessing Voyager service: #{error.message}")
      end

      def test_user
        @test_user ||= active_account_in_pod.users.admins.active.first
      end

      private

      def voyager_client
        @voyager_client ||= Zendesk::VoyagerClientBuilder.build(active_account_in_pod)
      end

      def active_account_in_pod
        @active_account_in_pod ||= Account.pod_local.active.serviceable.shard_unlocked.where("id != #{Account.system_account_id}").first
      end
    end
  end
end
