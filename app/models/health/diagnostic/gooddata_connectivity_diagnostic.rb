module Health
  module Diagnostic
    class GooddataConnectivityDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        'GooddataConnectivityDiagnostic'
      end

      def check
        gooddata = Zendesk::Gooddata::Client.v2.send(:connection).log_in

        return failed_result('Failed to connect to gooddata') unless gooddata.present?

        if gooddata.status == :logged_in
          passed_result('Successfully connected to Gooddata')
        else
          failed_result('Failed to verify login to gooddata')
        end
      rescue StandardError => error
        failed_result("Gooddata connectivity check failed with error: #{error.message}")
      end
    end
  end
end
