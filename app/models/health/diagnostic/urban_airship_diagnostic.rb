module Health
  module Diagnostic
    class UrbanAirshipDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        PushNotifications::UrbanAirshipClient.default_options[:base_uri]
      end

      def check
        client = PushNotifications::UrbanAirshipClient.new('test_key', 'test_secret', statsd_tags: ["name:sdk_registration"])
        result = client.tokens_deactivated_since(2.days.ago)

        # Response should be unauthorized
        return failed_result("UrbanAirship check failed.  HTTP response is expected to be 401, but is #{result.code}.") unless result.code == 401

        passed_result('Successfully connected to UrbanAirship')
      rescue StandardError => e
        failed_result("UrbanAirship check failed with error #{e.message}")
      end
    end
  end
end
