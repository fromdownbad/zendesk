module Health
  module Diagnostic
    class SamsonSecretsPullerDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        samson_secret_path
      end

      def check
        return failed_result("Samson Secrets Puller folder #{samson_secret_path} not found") unless File.exist?(samson_secret_path)

        SamsonSecretPuller.to_hash

        passed_result('Successfully accessed Samson Secrets Puller')
      rescue StandardError => e
        failed_result("Samson Secrets Puller check failed with error #{e.message}")
      end

      def samson_secret_path
        SamsonSecretPuller::FOLDER
      end
    end
  end
end
