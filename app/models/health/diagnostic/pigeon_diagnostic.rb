module Health
  module Diagnostic
    class PigeonDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        Zendesk::Configuration.dig!(:pigeon, :url)
      end

      def check
        uri = URI("#{resource}/z/ping")
        response = Net::HTTP.get_response(uri)
        return failed_result("Pigeon check failed: #{response.code} -- #{response.body}") unless response.is_a?(Net::HTTPSuccess)

        passed_result('Successfully pinged Pigeon')
      rescue StandardError => e
        failed_result("Pigeon check failed with error #{e.message}")
      end
    end
  end
end
