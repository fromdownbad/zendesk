# A diagnostic check to confirm that Elasticsearch is accessible

module Health
  module Diagnostic
    class SearchDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def check
        endpoints.each do |endpoint|
          begin
            response = Net::HTTP.get_response(URI(endpoint))

            if response.code != "200"
              @resource = endpoint
              return failed_result("Search Service is inaccessible. Response code: #{response.code}")
            end
          rescue => e
            return failed_result("Search Service is inaccessible: #{e.message}")
          end
        end
        passed_result("Search Service is accessible")
      end

      private

      def endpoints
        ["#{ZendeskSearch.search_service_endpoint_blue}/z/ping", "#{ZendeskSearch.search_service_endpoint_green}/z/ping",]
      end
    end
  end
end
