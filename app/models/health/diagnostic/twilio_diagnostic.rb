require 'twilio-ruby'

module Health
  module Diagnostic
    class TwilioDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def check
        ::Twilio::REST::Client.new('bogus_sid', 'bogus_token').accounts.list
        failed_result('Unknown failure checking Twilio- error expected but none returned')
      rescue Twilio::REST::RequestError
        passed_result('Successfully connected to Twilio')
      rescue StandardError => e
        failed_result("Twilio check failed with error #{e.message}")
      end
    end
  end
end
