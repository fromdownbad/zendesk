# A diagnostic check to confirm that I18n content downloader has successfully
# downloaded fresh content.
# The content is used in Zendesk::I18n::Backend::MarshalFile (included with Zendesk::I18n::Backend::Local).
# Here's some info on the process:
# https://zendesk.atlassian.net/wiki/spaces/i18n/pages/264268491/S3+Translations+Downloader+meta+sync+zendesk+i18n+download

module Health
  module Diagnostic
    class I18nDownloaderDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def check
        return failed_result("I18n directory expected at #{download_directory} but not found") unless Dir.exist?(download_directory)
        Dir.chdir(download_directory) do
          newest_file = Dir.glob("*").max_by { |f| File.mtime(f) }
          return failed_result("I18n directory #{download_directory} is empty") unless newest_file
          age_in_seconds = Time.now.utc - File.mtime(newest_file)
          return failed_result("#{newest_file} is the newest file in #{download_directory} and is #{age_in_seconds} seconds old, which is over the maximum allowed value (#{max_age_in_seconds})") if age_in_seconds > max_age_in_seconds
        end
        passed_result("I18n downloads directory (#{download_directory}) contains fresh content")
      rescue StandardError => e
        failed_result("I18n downloader check failed with error #{e.message}")
      end

      def download_directory
        ::Zendesk::I18n::Backend::MarshalFile::STATIC_PATH
      end

      def max_age_in_seconds
        (4 * 60 * 60) # 4 hours
      end
    end
  end
end
