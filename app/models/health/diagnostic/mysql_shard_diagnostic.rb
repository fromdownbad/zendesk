# A diagnostic check to confirm that a MySQL shard database is accessible.  We
# don't check EVERY shard- we just want to know that network setup lets us get to
# a shard.

module Health
  module Diagnostic
    class MysqlShardDiagnostic < MysqlDiagnostic
      def resource
        ActiveRecord::Base.on_shard(shard_id) do
          resource_name
        end
      rescue
        'unknown'
      end

      def perform_select
        ActiveRecord::Base.on_shard(shard_id) do
          execute_test_query
        end
      end

      private

      def shard_id
        # Use the first shard ID for the pod we're on
        @shard_id ||= Zendesk::Configuration::PodConfig.local_shards.first
      end
    end
  end
end
