# A diagnostic check to confirm that the 'Store' Redis instance is accessible

module Health
  module Diagnostic
    class RedisStoreDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        redis_client.redis.client.options[:url]
      rescue => e
        "Unknown: #{e}"
      end

      def check
        set_result = redis_client.set('_diagnostic_check', '1')
        if set_result == 'OK'
          passed_result('Redis Store is working')
        else
          failed_result("Set was expected to return 'OK' but returned '#{set_result}'")
        end
      rescue => e
        failed_result("Access failed with error #{e.message}")
      end

      def redis_client
        Zendesk::RedisStore.client
      end
    end
  end
end
