require 'zendesk/radar_factory'

module Health
  module Diagnostic
    class RadarRedisDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        return '' unless active_account_in_pod
        # NOTE: It's a little dangerous to call a private method in Configuration,
        # but it's better than not reporting useful info.
        ::Zendesk::Radar::Configuration.send(:configuration_for_shard, active_account_in_pod.shard_id).to_json
      end

      def check
        return failed_result("Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}") unless active_account_in_pod

        radar_shard_map.values.uniq.each do |radar_shard|
          shard_acct = Account.active.where(shard_id: radar_shard).first
          next unless shard_acct

          failure_for_acct = check_account(shard_acct)
          return failure_for_acct if failure_for_acct
        end

        passed_result('Successfully accessed Redis for Radar')
      rescue StandardError => e
        failed_result("Radar Redis check failed with error #{e.message}")
      end

      def check_account(account)
        shard_radar_client = ::RadarFactory.create_radar_client(account)
        return failed_result("Failed to get presence for radar shard #{account.shard_id}") unless shard_radar_client.presence('ticket/1').get
      end

      def radar_shard_map
        Zendesk::Radar::Configuration::SHARD_MAP || {}
      end

      private

      def active_account_in_pod
        @active_account_in_pod ||= Account.active.serviceable.shard_unlocked.where('shard_id in (?)', local_shard_ids).first
      end

      def local_shard_ids
        ActiveRecord::Base.shard_names.map(&:to_i)
      end
    end
  end
end
