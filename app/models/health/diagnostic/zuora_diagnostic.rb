module Health
  module Diagnostic
    class ZuoraDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        unless zuora_subscription
          return "(Zuora not in use)"
        end

        "Zuora user: #{ZendeskBillingCore::Zuora::Configuration.username}"
      rescue => e
        "Unknown: #{e.message}"
      end

      def check
        unless zuora_subscription
          # This is a success case- Zuora is not set up for the Z1 account
          Rails.logger.info("zuora not setup for z1")
          return passed_result("Passed - Zuora not in use")
        end

        zuora_account_id = zuora_subscription.zuora_account_id
        if ZendeskBillingCore::Zuora::Client.new(zuora_account_id).zuora_account
          passed_result("Zuora account found")
        else
          failed_result("Unable to retrieve Zuora account for account ID #{zuora_account_id}")
        end
      rescue => e
        failed_result("Access failed with error #{e.message}")
      end

      private

      def zuora_subscription
        @zuora_subscription ||= Account.find(1).subscription.zuora_subscription
      end
    end
  end
end
