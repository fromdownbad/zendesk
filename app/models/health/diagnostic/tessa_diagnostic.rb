module Health
  module Diagnostic
    class TessaDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        "#{TessaClient::V1.tessa_host_url}/z/ping"
      end

      def check
        TessaClient::V1.fetch_health

        passed_result('Successfully connected to Tessa instance.')
      rescue StandardError => e
        failed_result("Tessa check failed with error #{e.message}")
      end
    end
  end
end
