require 'json-schema'

module Health
  module Diagnostic
    class SupportRuleAdminResourceDiagnostic < AdminResourceDiagnostic
      def resource_name
        'rule-admin'
      end

      def check_name
        'Support rule admin'
      end
    end
  end
end
