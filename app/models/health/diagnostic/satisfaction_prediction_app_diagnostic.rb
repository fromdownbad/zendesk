module Health
  module Diagnostic
    class SatisfactionPredictionAppDiagnostic < ZendeskHealthCheck::Diagnostic::Base
      def resource
        active_account_in_pod ? client.url_prefix : ''
      end

      def check
        return failed_result("Unable to find active account in pod #{Zendesk::Configuration[:pod_id]}") unless active_account_in_pod

        client.fetch_satisfaction_prediction_available
        passed_result('Successfully retrieved account info from satisfaction prediction app')
      rescue StandardError => e
        failed_result("Satisfaction prediction app check failed with error #{e.message}")
      end

      private

      def client
        @client ||= ::Zendesk::SatisfactionPredictionApp::InternalApiClient.new(active_account_in_pod)
      end

      def active_account_in_pod
        @active_account_in_pod ||= Account.pod_local.active.serviceable.shard_unlocked.first
      end
    end
  end
end
