class SuspendedTicketNotification < ActiveRecord::Base
  module Frequency
    NEVER     = ['txt.admin.models.suspended_ticket_notification.Never', 0].freeze
    SOONISH   = ['txt.admin.models.suspended_ticket_notification.Every_10_minutes', 10].freeze
    HOURLY    = ['txt.admin.models.suspended_ticket_notification.Hourly_digest', 60].freeze
    DAILY     = ['txt.admin.models.suspended_ticket_notification.Daily_digest', 60 * 24].freeze
    ALL       = [NEVER, SOONISH, HOURLY, DAILY].freeze

    def self.find(v)
      ALL.detect { |f| f.last == v }
    end
  end

  include CIA::Auditable

  belongs_to :account

  audit_attribute :frequency, :email_list, unless: :just_created?
  attr_accessible :frequency, :email_list

  validates_presence_of :account_id
  validates_inclusion_of :frequency, in: Frequency::ALL.map(&:last)

  validate              :validate_email_list
  before_save           :ensure_default_last_sent_at

  private

  def validate_email_list
    if email_list.blank?
      self.email_list = nil
      self.frequency  = Frequency::NEVER.last
    else
      email_list.to_s.tokenize.each do |address|
        unless Zendesk::Mail::Address.sanitize(address)
          errors.add(:email_list, :invalid)
          return
        end
      end
    end
  end

  def ensure_default_last_sent_at
    self.last_sent_at ||= Time.now
  end

  def just_created?
    created_at > 2.seconds.ago
  end
end
