require 'resque/durable'

class QueueAudit < Resque::Durable::QueueAudit
  belongs_to :account
  scope :with_accounts, -> { includes(:account) }

  IGNORED_CLASSES = ["EmbeddingsUpdateJob"].freeze

  def self.failed
    Account.without_locking do
      audits = super.with_accounts
      audits.select { |audit| audit.account.nil? || !audit.account.is_locked? }
    end
  end

  def self.cleanup(expiration_date)
    Account.without_locking do
      audits = older_than(expiration_date).with_accounts
      audits = audits.select { |audit| audit.account.nil? || !audit.account.is_locked? }
      audits.each(&:destroy)

      audits
    end
  end

  def retryable?
    super && account.shard_local?
  end

  def enqueue
    return if IGNORED_CLASSES.include?(attributes["job_klass"])
    super
  rescue Exception => e # rubocop:disable Lint/RescueException
    message = "Durable queue recovery failed! account: #{account_id} enqueued_id: #{enqueued_id}"

    ZendeskExceptions::Logger.record(e, location: self, message: message, fingerprint: 'a1dd01f69295fa574c88fe2d5542cea47ec56e1a')
    raise
  end

  def enqueued!
    super
    if enqueue_count > 1
      statsd_client.increment('reenqueued', tags: ["enqueue_count:#{enqueue_count}", "job:#{job_klass}"])
    end
  end

  # to_json serializes the logging device -> IO error -> test/unit/jobs/durable_test.rb
  def to_json(_options = {})
    attributes.to_json
  end

  def delay
    if job_klass.respond_to?(:delay)
      job_klass.delay(enqueue_count)
    else
      super
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'resque_durable.queue_audit')
  end

  # heartbeat!, but a check-and-set operation used by Resque::Durable::BackgroundHeartbeat.
  # This is normally called in a Thread, but shard-selection is set in
  # Thread.local, so we need to re-establish the shard.
  def optimistic_heartbeat!(_)
    account.on_shard do
      super
    end
  rescue Resque::Durable::QueueAudit::JobCollision
    statsd_client.increment('job_collision', tags: ["job:#{job_klass}"])
    raise
  end
end
