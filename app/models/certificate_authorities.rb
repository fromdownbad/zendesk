require 'openssl'

class CertificateAuthorities < ActiveRecord::Base
  not_sharded
  disable_global_uid

  attr_accessible

  def self.roots
    to_x509(1)
  end

  def self.intermediates
    to_x509(0)
  end

  # @param [OpenSSL::X509::Certificate] ca_crt_object
  def self.append(ca_crt_object, is_root = false)
    CertificateAuthorities.transaction do
      ca_old = find_by_crt_object(ca_crt_object)

      if ca_old
        msg = "Intermediate replaced root for: #{ca_crt_object.subject}"
        puts msg
        Rails.logger { msg }
        ca_old.delete # replace existing ca intermediate
      end

      ca = CertificateAuthorities.new
      ca.is_root      = is_root
      ca.subject_hash = ca_crt_object.subject.hash
      ca.subject      = ca_crt_object.subject.to_s
      ca.issuer_hash  = ca_crt_object.issuer.hash
      ca.issuer       = ca_crt_object.issuer.to_s
      ca.not_before   = ca_crt_object.not_before
      ca.not_after    = ca_crt_object.not_after
      ca.crt          = ca_crt_object.to_pem
      ca.save!
    end
  end

  # Find certificates by both subject_hash and public_key
  # @param [OpenSSL::X509::Certificate] ca_crt_object
  def self.find_by_crt_object(ca_crt_object)
    CertificateAuthorities.
      where(subject_hash: ca_crt_object.subject.hash).
      find { |ca| ca.crt_object.public_key.to_s == ca_crt_object.public_key.to_s }
  end

  # NOTE: should ca_roots.pem be updated via update_trusted_ca_list.rb?

  # one time process
  def self.import(ca_roots, ca_intermediates)
    # let intermediates (updated roots) replaced roots if subject_hash is the same
    ca_roots.each { |ca_root| CertificateAuthorities.append(ca_root, 1) }
    ca_intermediates.each { |ca_inter| CertificateAuthorities.append(ca_inter, 0) }
  end

  def crt_object
    @crt_object ||= OpenSSL::X509::Certificate.new(crt)
  end

  def self.to_x509(is_root)
    CertificateAuthorities.where(is_root: is_root).pluck(:crt).map do |crt|
      OpenSSL::X509::Certificate.new(crt)
    end
  end
end
