class AppLinksController < ApplicationController
  include ::AllowedParameters
  skip_before_action :authenticate_user
  before_action :ensure_unmapped
  APP_ID = 'com.zendesk.agent.scarlett'.freeze
  PACKAGE_NAME = 'com.zendesk.scarlett.zendesk'.freeze
  PATHS = ['tickets/*'].freeze
  FINGERPRINTS = ['2C:E2:3E:60:9E:E7:E9:8D:2E:71:BE:E9:86:B8:B6:11:3F:01:0D:B8:E3:CA:77:FA:7B:0C:F6:56:2D:FC:D3:EB'].freeze

  allow_parameters :apple, {}
  def apple
    if feature_enabled?(:mobile_app_links_ui)
      render json: presenter.present_for_apple(AppLink.all)
    else
      render json: {
        applinks: {
          apps: [],
          details: [
            {
              appID: APP_ID,
              paths: PATHS
            }
          ]
        }
      }
    end
  end

  allow_parameters :android, {}
  def android
    if feature_enabled?(:mobile_app_links_ui)
      render json: presenter.present_for_android(AppLink.all)
    else
      render json: [
        {
          relation: ['delegate_permission/common.handle_all_urls'],
          target: {
            namespace: 'android_app',
            package_name: PACKAGE_NAME,
            sha256_cert_fingerprints: FINGERPRINTS
          }
        }
      ]
    end
  end

  protected

  def presenter
    @presenter ||= AppLinkPresenter.new
  end

  def ensure_unmapped
    unless request.url.start_with?(current_account.authentication_domain)
      render status: :bad_request, text: 'invalid domain / protocol'
    end
  end
end
