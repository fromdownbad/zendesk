class WatchingsController < ApplicationController
  include ::AllowedParameters

  deprecate_actions [:index, :show, :create, :destroy],
    with_feature: :deprecate_unused_actions_in_watchings_controller

  before_action :get_watched, except: [:unsubscribe, :index, :unsubscribe_comments]
  skip_before_action :authenticate_user, only: [:unsubscribe, :unsubscribe_comments]

  WATCHABLE_TYPES = { forum: :forums, entry: :entries, organization: :organizations }.freeze

  layout :layout_for_role

  type_type = Parameters.enum(*WATCHABLE_TYPES.keys.map(&:to_s))

  allow_parameters :index,
    type: type_type

  def index
    watchings = if params[:type] && WATCHABLE_TYPES[params[:type].to_sym].present?
      current_user.watchings.where(source_type: params[:type].capitalize)
    else
      current_user.watchings
    end

    @subscriptions = watchings.map(&:source)
    render xml: @subscriptions
  end

  allow_parameters :create,
    id: Parameters.bigid,
    user_id: Parameters.bigid,
    type: type_type

  def create
    unless request.post?
      return render_failure(status: :bad_request, title: '400 Bad Request', message: 'Only POST requests are allowed')
    end

    if current_user.is_agent? && params[:user_id]
      user = current_account.users.find_by_id(params[:user_id])
      @watching = user.present? ? @watched.subscribe(user) : nil
    else
      @watching = @watched.subscribe(current_user)
    end
    if @watching
      respond_to do |format|
        format.js { flash[:notice] = I18n.t('txt.watchings.create.message') }
        format.xml { head :created }
        format.json { head :created }
      end
    else
      send_error(I18n.t('txt.watchings.error'))
    end
  end

  allow_parameters :destroy,
    id: Parameters.bigid,
    user_id: Parameters.bigid,
    type: type_type

  def destroy
    @watching = if current_user.is_agent? && params[:user_id]
      @watched.watchings.find_by_user_id(params[:user_id])
    else
      @watched.watchings.find_by_user_id(current_user)
    end
    if @watching && @watching.destroy
      respond_to do |format|
        format.html {}
        format.js { flash[:notice] = I18n.t('txt.watchings.destroy.message') }
        format.xml { head :ok }
        format.json { head :ok }
      end
    else
      send_error(I18n.t('txt.watchings.error'))
    end
  end

  allow_parameters :unsubscribe,
    token: Parameters.string

  def unsubscribe
    @feedback_message = Watching.delete_and_interpolate_feedback_message!(current_account.id, params[:token])
  end

  allow_parameters :unsubscribe_comments,
    token: Parameters.string

  def unsubscribe_comments
    watching = Watching.find_by_account_id_and_token(current_account.id, params[:token])
    if watching.nil?
      @feedback_message = I18n.t('txt.watchings.destroy.fail')
    else
      watching.unsubscribe_posts!
      @feedback_message = I18n.t('txt.watchings.destroy.message_comments', forum_name: watching.source.name)
    end
    render :unsubscribe
  end

  allow_parameters :show,
    id: Parameters.bigid,
    type: type_type

  def show
    if @watched && current_user.can?(:watch, @watched)
      render partial: 'subscribe', locals: { watched: @watched }
    else
      head :ok
    end
  end

  private

  def send_error(message)
    respond_to do |format|
      format.html {}
      format.js do
        flash[:error] = message
        render :update, &:reload_flash
      end
      format.xml { render partial: 'shared/message', locals: { message: message }, status: :not_acceptable, layout: false }
    end
  end

  def get_watched # rubocop:disable Naming/AccessorMethodName
    if params[:type].blank? || params[:id].blank? || WATCHABLE_TYPES[params[:type].to_sym].blank?
      return send_error(I18n.t('txt.watchings.invalid_param'))
    end
    @watched = current_account.send(WATCHABLE_TYPES[params[:type].to_sym]).find_by_id(params[:id])
    if @watched.nil? || !@watched.respond_to?(:watchings)
      return send_error(I18n.t('txt.watchings.not_found'))
    end
  end
end
