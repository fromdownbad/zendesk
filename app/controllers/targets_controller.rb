class TargetsController < ApplicationController
  include ::AllowedParameters
  include RulesHelper
  include Zendesk::LotusEmbeddable
  include Zendesk::Targets::Helper

  target_type = Parameters.map

  usual_params = {
    code:              Parameters.string,
    error_description: Parameters.string,
    id:                Parameters.string,
    oauth:             Parameters.string,
    oauth_verifier:    Parameters.string,
    submit_type:       Parameters.string, # From logging, untested
    target:            target_type
    # _: Parameters.integer # From logging, ignored
  }

  deprecate_action :delete_inactive

  require_capability :targets

  allow_parameters :exceed_maximum_trial_targets_limit?, account: Parameters.anything

  before_action :check_permissions
  before_action :setup_oauth, only: [:new, :edit]
  before_action :request_oauth2_token, only: [:new, :edit]
  before_action :prevent_password_overwrite, only: [:create, :update, :test_target]
  before_action :test_target_rate_limit, only: :test_target
  before_action :verify_exceed_trial_limit, only: [:create]

  tab :account

  layout 'agent'

  ssl_allowed :all

  allow_parameters :index, {}

  def index
    redirect_to settings_extensions_path(anchor: "targets")
  end

  allow_parameters :new, **usual_params

  def new
    if target.requires_authorization? && !target.authorized?
      session[:target_action] = 'new' if target.is_a? YammerTarget
      render target.authorization_template
      return
    end

    render action: 'edit'
  end

  allow_parameters :edit, **usual_params

  def edit
    session[:target_action] = 'edit' if target.is_a? YammerTarget
    if target.requires_authorization? && !target.authorized? && !target.needs_authorization_upgrade?
      render target.authorization_template
      return
    end
  end

  allow_parameters :select_target_to_add, {}

  def select_target_to_add
  end

  allow_parameters :create, **usual_params

  def create
    @target = build_target

    # True only for Yammer, Twitter and Salesforce
    if @target.requires_authorization?
      if @target.use_oauth2?
        @target.oauth2_token = session.delete(:yammer_oauth2_token)
      else
        @target.token = session.delete(:access_token)
      end
      @target.secret = session.delete(:access_secret)

      @target.encrypt!
    end

    @target.current_user = current_user
    if @target.save
      notice = I18n.t('txt.admin.controllers.targets_controller.target_created', target_title: "<a class='title' href='#{edit_target_path(@target)}'>#{ERB::Util.h(@target.title)}</a>")
      notice << "<ul><li>".html_safe
      notice <<
        if @target.creates_integration?
          I18n.t('txt.admin.controllers.targets_controller.triggers_and_views_useful_for_the_target', name_of_the_target: @target.class.name.titleize.downcase)
        else
          I18n.t('txt.admin.controllers.targets_controller.now_add_it_as_a_notify_target')
        end
      notice << "</li></ul>".html_safe
      flash[:notice] = notice
      redirect_to targets_path
    else
      flash[:error] = flash_error(::I18n.t('txt.admin.controllers.targets_controller.target_could_not_be_created'), @target)
      render action: 'edit'
    end
  end

  allow_parameters :update, **usual_params

  def update
    target_updated = target_initializer.set(target, params[:target]).save

    if target_updated
      flash[:notice] = I18n.t('txt.admin.controllers.targets_controller.target_updated', name_of_target: "<a class='title' href='#{edit_target_path(target)}'>#{ERB::Util.h(target.title)}</a>")
      redirect_to targets_path
    else
      flash[:error] = flash_error(::I18n.t('txt.admin.controllers.targets_controller.target_could_not_be_updated'), target)
      render action: 'edit'
    end
  end

  allow_parameters :destroy, **usual_params

  def destroy
    return render_failure(
      status: :bad_request,
      title: 'Bad request',
      message: 'Request must be DELETE with proper permissions'
    ) unless request.delete?

    if target.destroy
      flash[:notice] = I18n.t('txt.admin.controllers.targets_controller.target_deleted_notice', target_name: ERB::Util.h(target.title))
      redirect_to targets_path
    else
      flash[:error] = flash_error(::I18n.t('txt.admin.controllers.targets_controller.target_could_not_be_deleted', name_of_target: ERB::Util.h(target.title)))
      render action: 'edit'
    end
  end

  allow_parameters :delete_inactive, {}

  def delete_inactive
    return render_failure(
      status: :bad_request,
      title: 'Bad request',
      message: 'Request must be DELETE with proper permissions'
    ) unless request.delete?

    if current_account.targets.inactive.delete_all
      flash[:notice] = I18n.t('txt.admin.controllers.targets_controller.all_inactive_Targets_deleted')
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.targets_controller.could_not_delete_inactive_targets'))
    end
    redirect_to targets_path
  end

  allow_parameters :target_params,
    target: target_type

  def target_params
    params[:target].tap do |target_param|
      target_param[:oauth2_token] = session[:yammer_oauth2_token] if target_param && session[:yammer_oauth2_token]
      target_param[:token] = session[:access_token] if target_param && session[:access_token]
      target_param[:secret] = session[:access_secret] if target_param && session[:access_secret]
    end
  end

  allow_parameters :test_target,
    **usual_params,
    message: Parameters.map | Parameters.string # From logging, untested

  def test_target
    target.attributes = target_params
    target.encrypt!

    tester.test(target, message: message)

    if target.is_a?(UrlTargetV2)
      if current_account.has_log_targets_v2_test_targets? && target.http_transactions
        message = ""

        target.http_transactions.each_with_index do |transaction, i|
          unless transaction.response.headers.nil?
            next
          end

          if message.empty?
            message += "target | account: #{current_account.id} | target.http_transactions:"
          end

          message += "\n  #{i}) transaction: #{transaction}"
        end

        unless message.empty?
          Rails.logger.info(message)
        end
      end

      render "targets/transaction_colorbox", layout: false
    else
      render_alert_box(::I18n.t('txt.admin.controllers.targets_controller.the_message_was_successfully_sent'))
    end
  rescue Zendesk::Targets::Tester::TargetInvalid => e
    render_alert_box("#{I18n.t('txt.admin.controllers.targets_controller.invalid_target_configuration')}\n#{e.error_messages.join("\n")}")
  rescue Zendesk::Targets::Tester::TestingNotSupported => e
    render_alert_box(I18n.t('txt.admin.controllers.targets_controller.test_is_not_supported_for_this_target_because', errors: e.reason))
  rescue Zendesk::Targets::Tester::NoTicketsInAccount
    render_alert_box(I18n.t('txt.admin.controllers.targets_controller.there_are_no_tickets_in_your_account'))
  rescue Zendesk::Targets::Tester::TestFailed => e
    render_alert_box("#{I18n.t('txt.admin.controllers.targets_controller.error_during_transmission')}#{e.error.message}")
  rescue YammerTarget::YammerTargetError => e
    render_alert_box(e.reason.to_s)
  end

  allow_parameters :activate, **usual_params

  def activate
    if target.valid?
      target.activate
      flash[:notice] = I18n.t('txt.admin.controllers.targets_controller.target_activated')
      redirect_to settings_extensions_path(anchor: "targets")
    else
      flash[:error] = I18n.t('txt.admin.controllers.targets_controller.invalid_target_configuration')
      redirect_to edit_target_path(target)
    end
  end

  allow_parameters :deactivate, **usual_params

  def deactivate
    if target.valid?
      target.deactivate
      flash[:notice] = I18n.t('txt.admin.controllers.targets_controller.target_deactivated')
      redirect_to settings_extensions_path(anchor: "targets")
    else
      flash[:error] = I18n.t('txt.admin.controllers.targets_controller.invalid_target_configuration')
      redirect_to edit_target_path(target)
    end
  end

  allow_parameters :yammer_callback, **usual_params

  def yammer_callback
    target.attributes = target_params
    target_path = if session[:target_action] == 'edit'
      edit_target_path(target) + "?code=#{params[:code]}"
    else
      new_target_path(target) + "/#{params[:id]}?code=#{params[:code]}"
    end
    session.delete(:target_action)

    redirect_to target_path
  rescue Zendesk::Targets::Tester::TargetInvalid => e
    render_alert_box("#{I18n.t('txt.admin.controllers.targets_controller.invalid_target_configuration')}\n#{e.error_messages.join("\n")}")
  rescue YammerTarget::YammerTargetError => e
    render_alert_box(e.reason.to_s)
  end

  private

  def target
    @target ||= if /\A\d+\z/.match?(params[:id].to_s)
      current_account.targets.find(params[:id])
    else
      build_target
    end
  end

  def build_target
    target_initializer.build((target_params || {}).merge(type: params[:id]))
  end

  def target_initializer
    @target_initializer ||= Zendesk::Targets::Initializer.new(account: current_account, via: :portal)
  end

  def request_oauth2_token
    return unless target.use_oauth2?

    if params.key?('code')
      # use unique session id for yammer - had problems with reloading page new target page passing authorization
      # since the build target was using session identifier
      session[:yammer_oauth2_token] = target.request_token(params['code'])
      if !session[:yammer_oauth2_token]
        flash[:error] = flash_error(::I18n.t('txt.admin.controllers.targets_controller.target_could_not_be_created'), @target)
      else
        flash[:notice] = ::I18n.t('txt.admin.controllers.targets_controller.authorization_successful')
      end
    elsif params.key?('error')
      flash[:error] = flash_error(params['error_description'], @target)
      session.delete(:yammer_oauth2_token)
    else
      # delete session token since we only want to use it when we have a code
      # TODO this is ugly and we need to factor out the oauth2 code from the oauth code
      session.delete(:yammer_oauth2_token)
    end
  rescue YammerTarget::YammerTargetError => e
    flash[:error] = e.reason
  end

  def setup_oauth
    return unless target.requires_authorization?
    return if target.use_oauth2?

    # This block only executes for Salesforce and Twitter ATM

    if params[:oauth_verifier]
      begin
        token  = session.delete(:request_token)
        secret = session.delete(:request_secret)
        target.authorize(params[:oauth_verifier], token, secret)

        if target.new_record?
          session[:access_token]  = target.access_token.token
          session[:access_secret] = target.access_token.secret
        end

        target.callback_url = request.url
      rescue Net::HTTPFatalError
        flash[:error] = target.authorization_failure_message
        redirect_to new_target_path(id: target.class.name.underscore)
      rescue OAuth::Unauthorized
        flash[:error] = target.authorization_failure_message
        redirect_to new_target_path(id: target.class.name.underscore)
      end
    else
      target.callback_url = build_target_callback_url(target)
      session[:request_token]  = target.request_token.token
      session[:request_secret] = target.request_token.secret
    end
  end

  def build_target_callback_url(target)
    if target.is_a? TwitterTarget
      Zendesk::Auth::OauthCallbackHelper.new(
        subdomain: current_account.subdomain,
        oauth_callback_path: oauth_callback_path
      ).twitter_oauth_callback_url
    else
      request.url
    end
  end

  def oauth_callback_path
    csrf_token = Zendesk::Auth::Warden::OAuthStrategy.state(session)
    "/agent?redirect=twitter_target_auth&state=#{csrf_token}"
  end

  def render_alert_box(message)
    respond_to do |format|
      format.js do
        render :update do |page|
          page.alert(message)
        end
      end
    end
  end

  def tester
    Zendesk::Targets::Tester.new(account: current_account, user: current_user)
  end

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end

  def prevent_password_overwrite
    params[:target].delete(:password) if remove_password_param?
  end

  def remove_password_param?
    params[:target] && params[:target][:password].blank? && !params[:target].delete(:password_overwrite)
  end

  def message
    if target.uses_parameters?
      # turns {"0"=>{"0"=>"key", "1"=>"value"}} into [["key", "value"]]
      if params[:message]
        params[:message].values.map(&:values)
      else
        []
      end
    else
      params[:message]
    end
  end

  def test_target_rate_limit
    Prop.throttle!(:test_target, current_user.id)
  end

  def verify_exceed_trial_limit
    if exceed_maximum_trial_targets_limit?(current_account)
      link_url = 'txt.admin.controllers.targets_controller.trial_account_limit_warning_link_url'
      link_text = 'txt.admin.controllers.targets_controller.trial_account_limit_warning_link_text'
      warning_content = 'txt.admin.controllers.targets_controller.trial_account_limit_warning'
      flash[:beware] = I18n.t(warning_content, link_content: view_context.link_to(I18n.t(link_text), I18n.t(link_url), target: '_blank'))
      redirect_to targets_path
    end
  end
end
