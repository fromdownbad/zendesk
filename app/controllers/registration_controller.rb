require 'zendesk/enterprise_recaptcha'

class RegistrationController < ApplicationController
  include ::AllowedParameters
  include HelpCenterRedirection
  include CrossDomainXFrameBypass
  include Zendesk::EnterpriseRecaptcha

  layout 'user'

  before_action :render_404_if_no_web_portal_and_no_help_center, only: :index
  before_action :block_signup, unless: :signup_allowed?
  skip_before_action :authenticate_user
  around_action :preserve_mobile_preference, only: :success
  before_action :redirect_to_home_if_signed_in, only: :index

  helper :captcha

  helper_method :current_brand, :current_route

  ssl_required :register, :request_oauth
  ssl_allowed :index, :success

  private def redirect_to_home_if_signed_in
    redirect_to account_home unless current_user && current_user.is_anonymous_user?
  end

  user_parameters = Parameters.map(
    locale_id: Parameters.string,
    time_zone: Parameters.string,
    timezone_name: Parameters.string,
    email: Parameters.string,
    name: Parameters.string,
    twitter_screen_name: Parameters.string,
    password: Parameters.string,
    roles: Parameters.string | Parameters.array(Parameters.string)
  )

  allow_parameters :index,
    user: user_parameters,
    theme: Parameters.string,
    message: Parameters.string,
    flash_digest: Parameters.string
  #  Allows users to sign up for the help desk, if this is allowed on the account
  def index
    @user = current_account.users.build(
      params[:user].try(:slice, :name, :email, :twitter_screen_name) || {}
    )
    respond_to do |format|
      format.html do
        render 'auth/sso_v2_index', locals: {sso_action: 'registration'}, layout: 'help_center'
      end
      format.mobile_v2 do
        render 'auth/sso_v2_index', locals: {sso_action: 'registration'}
      end
    end
  end

  allow_parameters :register,
    account: Parameters.anything,
    auth_origin: Parameters.string,
    brand_id: Parameters.bigid,
    flash_digest: Parameters.string,
    "g-recaptcha-response": Parameters.string,
    send_verify_email: Parameters.string,
    timezone_name: Parameters.string,
    user: user_parameters,
    return_to: Parameters.string,
    return_to_on_failure: Parameters.string,
    recaptcha_challenge_field: Parameters.string,
    recaptcha_response_field: Parameters.string,
    role: Parameters.string, # From logging, untested
    theme: Parameters.string # From logging, untested
  def register
    unless request.post?
      return redirect_fail(nil)
    end

    Rails.logger.info("ORCA_RECAPTCHA_REGISTRATION_CONTROLLER")

    unless current_account.has_orca_classic_skip_end_user_recaptcha?
      unless valid_recaptcha?(request)
        return redirect_fail(flash_error(I18n.t('txt.requests.new.captcha_error_v2')))
      end

      if params[:recaptcha_response_field] == 'manual_challenge'
        logger.info("manual_challenge is a bad recaptcha response")
        return redirect_fail(flash_error(I18n.t('txt.requests.new.captcha_error_v2')))
      end
    end

    if params[:user].blank? || params[:user][:email].blank?
      return redirect_fail(flash_error(I18n.t('txt.registration.error.email_blank')))
    end

    @user = current_account.find_user_by_email(params[:user][:email])

    unless @user.nil?
      if @user.via == 'Web Widget' && unverified_email_identity = @user.identities.email.unverified.first
        unverified_email_identity.send_verification_email
        set_user_name_and_email_in_session(@user)
        redirect_to(registration_success_return_to_url) && return
      end

      message = if current_account.has_multiple_active_brands?
        I18n.t('txt.registration.error.branded_email_already_registered_notice',
          login_link: notice_login_link,
          account_name: current_account.name)
      else
        I18n.t('txt.registration.error.email_already_registered_notice', login_link: notice_login_link)
      end

      return redirect_fail(flash_error(message))
    end

    locale_id = shared_session[:locale_id]

    if locale_id && params[:user][:locale_id].blank? && current_account.has_individual_language_selection?
      params[:user][:locale_id] = locale_id
    end

    if time_zone = ActiveSupport::TimeZone::MAPPING.key(params[:user][:timezone_name])
      params[:user][:time_zone] = time_zone
    end

    @user = User.new(
      params[:user].
        slice(:name, :email, :twitter_screen_name, :locale_id, :time_zone).
        merge(account: current_account, send_verify_email: true)
    )
    @user.password = params[:user][:password]

    @user.active_brand_id = params[:brand_id]

    unless Zendesk::Mail::Address.sanitize(@user.email)
      return redirect_fail(flash_error(I18n.t('txt.registration.error.email_invalid', user_email: sanitize(@user.email))))
    end

    # Do not allow users who are in the blacklist, and not in the whitelist, to sign up
    unless current_account.allows?(@user.email)
      return redirect_fail(flash_error(I18n.t('txt.registration.error.email_blacklisted', user_email: sanitize(@user.email))))
    end

    unless @user.valid? && @user.save
      return redirect_fail(flash_error(I18n.t('txt.registration.error.user_invalid'), @user))
    end

    set_user_name_and_email_in_session(@user)

    if @user.twitter_screen_name.present?
      session[:return_to] = params[:return_to] || registration_success_return_to_url
      # Store the user_id into the session, for consumption in UsersController#create_twitter_profile
      session[:create_twitter_profile_user_id] = @user.id
      session[:oauth_callback] = create_twitter_profile_user_url(@user)

      redirect_to registration_request_oauth_url
    else
      redirect_to_return_to(registration_success_return_to_url, user_params)
    end
  end

  allow_parameters :notice_login_link, {}
  def notice_login_link
    # Once the auth v2 is deployed we can change this URL to be 'auth/v2/sign_in' and the target to '_self'
    login_url = current_account.help_center_enabled? ? '/hc/signin' : '/login'
    view_context.link_to(I18n.t('txt.registration.error.login_link'), login_url, target: '_top')
  end

  allow_parameters :success,
    user: user_parameters
  def success
    clear_session_and_logout

    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: {sso_action: 'successful_registration'}, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'successful_registration' } }
    end
  end

  allow_parameters :request_oauth, {}
  def request_oauth
    callback_url = session.delete(:oauth_callback)

    Rails.logger.info "oauth: built url #{callback_url}"
    authorize_url = nil

    Timeout.timeout(4, Timeout::Error) do
      authorize_url = Zendesk::Auth::OauthDelegator.request_oauth!('twitter_readonly', callback_url, false)
    end

    redirect_to authorize_url
  rescue Net::HTTPFatalError, Timeout::Error, Zendesk::Auth::InvalidProfileException
    redirect_fail(I18n.t('public.controllers.registration_controller.failed_to_authenticate_your_twitter_Account'))
  end

  private

  def registration_success_return_to_url
    current_account.url + registration_success_path
  end

  def set_user_name_and_email_in_session(user) # rubocop:disable Naming/AccessorMethodName
    session[:user_name] = user.name
    session[:user_email] = user.email
  end

  def user_params
    { user: { name: @user.name } }
  end

  def render_404_if_no_web_portal_and_no_help_center
    if !current_account.help_center_enabled? && current_account.web_portal_disabled?
      render_404_page
    end
  end

  def sanitize(text)
    CGI.escapeHTML(text)
  end

  def redirect_fail(message)
    Rails.logger.warn("Registration error: #{message}")
    flash[:error] = message
    user_data     = { user: (params[:user] || {}).delete_if { |k, _v| !['name', 'email'].member?(k) }}.merge(encoded_flash_params(flash))

    if redirect_to_failure_return_to
    else
      next_url = { protocol: 'http', action: 'index', params: user_data }

      begin
        return_to_uri       = URI.parse(params[:return_to])
        next_url[:protocol] = return_to_uri.scheme
        next_url[:host]     = return_to_uri.host
      rescue URI::InvalidURIError
        # This happens when someone passes a junk URL
        # bad URI(is not URI?): https://ntoenterprise3.zendesk.com/registration/success') OR '1'=('0
      end

      next_url.delete(:host) unless current_account.host_names.include?(next_url[:host])

      redirect_to next_url
    end
  end

  def block_signup
    render_failure(message: I18n.t('txt.registration.error.disabled', account_subdomain: current_account.subdomain), status: :unauthorized)
  end

  def signup_allowed?
    current_account.is_open? &&
       (!current_account.has_limited_registration? ||
        current_account.login_allowed_for_role?(:zendesk, :end_user) || current_account.role_settings.end_user_password_allowed?)
  end

  include MobileSessionPreservation
end
