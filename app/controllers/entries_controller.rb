class EntriesController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Stats::ControllerHelper
  include HelpCenterRedirection

  skip_before_action :authenticate_user, only: :show, if: :should_redirect_to_help_center?
  before_action :redirect_to_help_center_entry, only: :show

  allow_anonymous_users if: proc { |c| c.send(:current_account).has_public_forums? }

  skip_before_action :set_x_xss_protection_header, only: :show
  before_action :is_web_portal_disabled?, only: :show

  allow_parameters :show, id: Parameters.string
  def show
    render_404_page
  end

  private

  def redirect_to_help_center_entry
    return unless should_redirect_to_help_center?
    mapping = ContentMapping.where(account_id: current_account.id, classic_object_id: params[:id].to_i).first
    url = mapping.present? ? mapping.url : nil
    perform_help_center_redirection(url)
  end
end
