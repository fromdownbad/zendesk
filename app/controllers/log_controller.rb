class LogController < ApplicationController
  deprecate_controller with_feature: :sse_deprecate_log_controller

  include ::AllowedParameters

  skip_before_action :authenticate_user, :verify_authenticated_session,
    :verify_authenticity_token

  VIA_WEB_ARCHIVE = /web.archive.org/

  allow_parameters :error, :skip
  def error
    head :ok

    return if params[:message].blank?
    return if request.env['HTTP_VIA'] =~ VIA_WEB_ARCHIVE

    error = JavaScriptError.new(params[:message])
    ZendeskExceptions::Logger.record(error, location: self, message: params[:message], fingerprint: '68a8e14668c303f101a1079eed0814e69274049c')
    statsd_client.increment('javascript_error', tags: ["exception_class:#{error.class.name.underscore}"])
  end

  class JavaScriptError < RuntimeError
  end

  private

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[log_controller])
  end
end
