require 'responds_to_parent'
class AttachmentsController < ApplicationController
  include ::AllowedParameters
  allow_zendesk_mobile_app_access
  include AttachmentsControllerMixin
  include ZendeskCrossOrigin::Helper

  around_action :add_credentials, only: :token

  allow_anonymous_users
  before_action :authorize_agent, only: :index

  skip_before_action :move_warden_message_to_flash # prevent remote-auth warnings
  skip_before_action :verify_authenticity_token, only: :create # tiny_mce/plugins/zd_advimage/image.htm does not have a csrf token

  ssl_allowed :all

  # Lists all images for account
  allow_parameters :index, {}
  def index
    # ZD734060: limit all accounts to the latest 500 images
    @images = current_account.attachments.originals.reverse_order.limit(500).includes(:thumbnails).to_a

    respond_to do |format|
      format.html
      format.xml  { render xml: @images }
      format.json { render json: @images }
      format.js   { render action: 'index', formats: [:rjs] }
    end
  end

  # Create image in tinymce image upload
  allow_parameters :create,
    image: Parameters.map(uploaded_data: Parameters.file),
    filename: Parameters.string,
    uploaded_data: Parameters.file,
    # Taken from attachment.rb
    author: Parameters.string,
    is_public: Parameters.string,
    source: Parameters.string,
    # Taken from attachment_mixin.rb
    account: Parameters.string,
    content_type: Parameters.string,
    parent_id: Parameters.string,
    thumbnail: Parameters.string,
    thumbnail_resize_options: Parameters.string
  def create
    @attachment = if params[:image]
      current_account.attachments.build(params[:image])
    else
      if params[:uploaded_data]
        current_account.attachments.build(params)
      else
        current_account.attachments.build(uploaded_data: RestUpload.new(request.raw_post, params[:filename], request.content_type))
      end
    end

    @attachment.account_id = current_account.id
    @attachment.author     = current_user

    respond_to do |format|
      if @attachment.save
        format.html do
          flash[:notice] = 'Image was successfully created.'
          redirect_to attachment_url(@attachment)
        end
        format.xml  { render xml: @attachment }
        format.json { render json: @attachment }
        format.js do
          responds_to_parent do
            render :update do |page|
              page << "ImageDialog.ts_insert_image('#{@attachment.url(relative: true)}', '#{@attachment.filename}');"
            end
          end
        end
      else
        format.html { render plain: "failed to save attachment" }
        format.xml  { render xml: @attachment.errors, status: :not_acceptable }
        format.json { render json: @attachment.errors, status: :not_acceptable }
        format.js do
          responds_to_parent do
            render :update do |page|
              page.alert('Sorry, error uploading image')
            end
          end
        end
      end
    end
  end

  allow_parameters :show,
    id: Parameters.bigid,
    force_reauth: Parameters.boolean
  def show
    return head :gone if current_account.has_sse_remove_attachment_show?

    attachment = Attachment.find_by_id_and_account_id!(params[:id], current_account.id)
    send_attachment_if_authorized(attachment, params[:force_reauth])
  end
  allow_parameters :download, allowed_parameters[:show]
  alias :download :show

  allow_parameters :token,
    id: Parameters.string,
    attachment_id: Parameters.bigid,
    force_reauth: Parameters.boolean,
    name: Parameters.string,
    access_token: Parameters.string
  def token
    throttle_key = [request.remote_ip, current_user.id.to_i, current_account.id]
    Prop.throttle!(:attachment_requests_by_ip, throttle_key) if Prop.throttled?(:attachment_requests_by_ip, throttle_key)
    begin
      attachment = Attachment.find_by_token_and_account_id!(params[:id], current_account.id)
    rescue ActiveRecord::RecordNotFound
      Prop.throttle!(:attachment_requests_by_ip, throttle_key)
      return render_failure(
        status: :not_found,
        title: I18n.t('txt.errors.not_found.title'),
        message: I18n.t('txt.errors.not_found.message')
      )
    end
    send_attachment_if_authorized(attachment, params[:force_reauth])
  end

  allow_parameters :destroy,
    id: Parameters.bigid
  def destroy
    Attachment.find(params[:id]).destroy
    head :ok
  end

  allow_parameters :download_landing,
    id: Parameters.bigid
  def download_landing
    @attachment = Attachment.find_by_token_and_account_id!(params[:id], current_account.id)
  end

  private ###################################################

  def send_attachment_if_authorized(attachment, force_reauth)
    # external url attachments are not stored anywhere, so exempt them.
    raise ActiveRecord::RecordNotFound if attachment.stores.empty? && !attachment.not_saved_in_stores?

    if current_user.can?(:view, attachment)
      instrument_attachment_use
      if current_account.has_attachments_on_separate_domain?
        redirect_to_attachment_domain(attachment)
      else
        send_attachment(attachment, force_reauth)
      end
    else
      permission_denied(attachment)
    end
  end

  def instrument_attachment_use
    role = if current_user.is_anonymous_user?
      "anonymous"
    elsif current_user.is_end_user?
      "end-user"
    else
      "agent"
    end

    statsd_client.increment('attachment.access', tags: ["role:#{role}"])
  end

  def permission_denied(attachment)
    if current_user.is_anonymous_user?
      # for downloadable attachments, better to take them to the ticket page than to
      # download it but dump them back at login
      if determine_disposition(attachment) != "inline"
        return_to = url_for(action: "download_landing", id: params[:id])
      end
      warden.authenticate! return_to: return_to
    else
      render_failure(status: :unauthorized, title: 'Unauthorized', message: 'You are not authorized to see the file. Make sure that you are <a href="/">logged in here</a>.')
    end
  end

  def warden_scope
    if action_name == 'token'
      :all
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'attachments')
  end

  def add_credentials
    yield
  ensure
    response.headers["Access-Control-Allow-Origin"] = '*'
    response.headers["Access-Control-Expose-Headers"] = 'X-Zendesk-API-Warn'
  end
end
