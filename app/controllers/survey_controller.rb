class SurveyController < ApplicationController
  include ::AllowedParameters

  before_action :authorize_owner

  layout false

  allow_parameters :show,
    id: Parameters.string
  def show
    template = SURVEY_QUESTION_NAMES.keys.detect { |k| k == params.require(:id) }
    return head :bad_request unless template
    render template.dup # call .dup to unfreeze the string
  end

  ## ### Saving a new survey result
  ## `POST /survey`
  ##
  ## Submits the survey result to Eloqua.
  ##
  ## #### Allowed For
  ##
  ##  * Owner
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v https://{subdomain}.zendesk.com/survey \
  ## -X POST \
  ## -u {email_address}:{password} \
  ## -H "Content-Type: application/json" \
  ## -d '{ "type": "support_churn",
  ##       "answer": {
  ##         "someQuestion": ["single choice"],
  ##         "someMoreQuestion": ["mult 0", "mult 1"]
  ##       }
  ##    }'
  ## ```
  ##
  ## #### Example Response
  ##
  ## Always returns 201 with an empty body.
  allow_parameters :create,
    type: Parameters.string,
    answer: Parameters.anything,
    cancel: Parameters.boolean,
    redeem_training_offer: Parameters.boolean

  def create
    @survey_type    = params.require(:type)
    @question_names = SURVEY_QUESTION_NAMES[@survey_type]
    return head :bad_request unless @question_names

    persist_survey_answers
    current_account.cancel! if cancel? && churn_survey?

    head :created
  end

  private

  # The ordering of the question names is important
  SURVEY_QUESTION_NAMES = {
    'support_churn' => %w[
      whyCancel
      whyNoNeed
      whyDidntMeetNeeds
      whichFeaturesDidntMeetNeed
      whichSoln
      whyNewProvider
      whyCovidImpacts
    ]
  }.freeze

  REDIS_EXPIRATION = 30.days

  def survey_allowed_parameters
    @question_names.inject({}) do |allowed_params, question_name|
      allowed_params.merge(
        question_name => Parameters.array(Parameters.string)
      )
    end
  end

  def redis_key
    "survey_#{@survey_type}_#{current_user.id}"
  end

  def persist_survey_answers
    return unless answers = params[:answer].presence

    payload = {
      type:           @survey_type,
      question_names: @question_names,
      answer:         answers.permit(survey_allowed_parameters),
      user_email:     current_user.email,
      account_id:     current_account.id,
      subdomain:      current_account.subdomain
    }

    if churn_survey?
      payload[:canceled]            = cancel?
      payload[:training_offer_save] = params.
        fetch(:redeem_training_offer, false)
    end

    begin
      Zendesk::RedisStore.client.set(
        redis_key,
        payload.to_json,
        ex: REDIS_EXPIRATION
      )

      SurveyPersistenceJob.enqueue(redis_key: redis_key)
    rescue Redis::BaseError => e
      Rails.logger.
        error("Could not save survey submission in redis. #{e.message}")
      statsd_client.increment(
        'failure.redis',
        tags: ["subdomain:#{current_account.subdomain}"]
      )
    end
  end

  def cancel?
    params.fetch(:cancel, true)
  end

  def churn_survey?
    @survey_type == 'support_churn'
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['survey'])
  end
end
