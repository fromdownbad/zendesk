class JobsController < ApplicationController
  include Zendesk::ImportExport::JobEnqueue

  include ::AllowedParameters

  before_action :set_job_klass
  before_action :ensure_job_access

  ssl_allowed :all

  layout false

  job_type = Parameters.map(
    rule:                             Parameters.string,
    token:                            Parameters.string,
    allow_agent_downgrade:            Parameters.string, # Type from code
    password_email_change_csv_import: Parameters.boolean,
    update_records:                   Parameters.string  # Type from code
  )

  allow_parameters :create,
    job: job_type,
    type: Parameters.string # TODO: enum(*ImportExportJobPolicy::IMPORT_JOBS)

  def create
    enqueue
    respond(:notice, notice_message)
  rescue Resque::ThrottledError
    respond(:error, flash_error(throttled_error_message))
  end

  private

  def ensure_job_access
    if ImportExportJobPolicy::IMPORT_JOBS.include?(params[:type])
      authorize_admin
    else
      authorize_agent
    end
    return if performed? # auth failed
    return unavailable_error  if job_policy.unavailable?
    return forbidden!         if job_policy.inaccessible?
    log_exceeded_max          if job_policy.ticket_count_exceeds_max?
  end
end
