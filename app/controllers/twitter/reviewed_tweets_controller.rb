class Twitter::ReviewedTweetsController < ApplicationController
  include ::AllowedParameters

  before_action :authorize_agent

  allow_parameters :index,
    tweet_ids: Parameters.string

  def index
    reviewed = ReviewedTweet.reviewed(current_account, params[:tweet_ids])
    render json: reviewed
  end

  allow_parameters :create,
    tweet_ids: Parameters.string

  def create
    reviewed = ReviewedTweet.mark_as_reviewed(current_account, params[:tweet_ids])
    render json: reviewed, status: :created
  end
end
