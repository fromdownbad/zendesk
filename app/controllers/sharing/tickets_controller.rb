require 'ticket_sharing/ticket'

class Sharing::TicketsController < SharingController
  CREATION_LOCKING_TIMEOUT = 10.seconds

  skip_before_action :authenticate_user
  skip_before_action :verify_authenticity_token
  before_action :authorize_sharing_partner!
  before_action :authorize_sharing_agreement!
  around_action :unique_ticket_creation, only: [:create, :update]
  before_action :preserve_idempotency, only: [:create]
  before_action :ensure_not_creating_right_after_delete
  before_action :find_or_build_shared_ticket, only: [:create, :update]
  before_action :prevent_closed_ticket_updates, only: [:create, :update]
  before_action :parse_shared_ticket, only: [:create, :update]
  around_action :setup_auditing

  # These shapes come from the ticket_sharing gem
  actor_params = {
    name: Parameters.string,
    role: Parameters.string,
    uuid: Parameters.string
  }

  ticket_sharing_ticket_params = {
    uuid: Parameters.string,
    subject: Parameters.string,
    requested_at: Parameters.datetime,
    status: Parameters.string,
    requester: actor_params,
    comments: Parameters.array(
      Parameters.map(
        uuid: Parameters.string,
        author: actor_params,
        body: Parameters.string,
        html_body: Parameters.string,
        authored_at: Parameters.datetime,
        public: Parameters.boolean,
        attachments: Parameters.array(
          Parameters.map(
            url: Parameters.string,
            filename: Parameters.string,
            content_type: Parameters.string,
            display_filename: Parameters.string
          )
        ),
        custom_fields: Parameters.map
      )
    ),
    current_actor: actor_params,
    tags: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil,
    original_id: Parameters.bigid,
    custom_fields: Parameters.map
  }

  allow_parameters :create, **ticket_sharing_ticket_params
  def create
    if @shared_ticket.from_partner(@ts_ticket)
      render json: {}, status: :created
    else
      log("Could not create shared ticket, error: #{@shared_ticket.error_message}")
      render json: {}, status: :unprocessable_entity
    end
  end

  allow_parameters :update, **ticket_sharing_ticket_params
  def update
    if @shared_ticket.from_partner(@ts_ticket)
      render json: {}, status: :ok
    else
      log("Could not update shared ticket id: #{@shared_ticket.id}, error: #{@shared_ticket.error_message}")
      render json: { error: @shared_ticket.error_message }, status: :unprocessable_entity
    end
  end

  allow_parameters :destroy, uuid: Parameters.string
  def destroy
    if @shared_ticket = current_agreement.shared_tickets.find_by_uuid(params[:uuid])
      @shared_ticket.soft_delete!
    end

    head :ok
  end

  private

  def preserve_idempotency
    if current_agreement.shared_tickets.exists?(uuid: params[:uuid])
      render plain: ' ', status: :no_content
    end
  end

  def find_or_build_shared_ticket
    @shared_ticket = active_shared_ticket || current_agreement.shared_tickets.build
  end

  def prevent_closed_ticket_updates
    return unless @shared_ticket.ticket_id
    unless @shared_ticket.ticket
      render plain: ' ', status: :not_found
      return
    end

    if @shared_ticket.ticket.closed?
      render json: { error: 'Can not update closed tickets' }, status: :method_not_allowed
    end
  end

  def parse_shared_ticket
    json_request = request.body.read
    json_request.force_encoding Encoding::UTF_8
    json_request.scrub!('')
    @ts_ticket = TicketSharing::Ticket.parse(json_request)
  end

  def log(message)
    Rails.logger.info("#{current_account.id} #{current_account.subdomain} #{message}")
  end

  def unique_ticket_creation(&block)
    key = "ticket_sharing/#{current_account.id}/#{params[:uuid]}"
    Zendesk::DB::RaceGuard.cache_locked(key, wait_max: CREATION_LOCKING_TIMEOUT, &block)
  end

  def setup_auditing
    options = {
      ip_address: (@audit_ip_address || request.remote_ip),
      actor: User.system,
      effective_actor: User.system,
      via_id: ViaType.TICKET_SHARING,
      via_reference_id: current_agreement.id
    }
    CIA.audit(options) { yield }
  end

  # RAILS5UPGRADE: this is an invalid status code in rails 5
  Rack::Utils::HTTP_STATUS_CODES[499] = 'Request has been forbidden'
  def ensure_not_creating_right_after_delete
    if all_shared_tickets.any? { |shared_ticket| !shared_ticket.deleted_at.nil? && shared_ticket.deleted_at > 10.seconds.ago }
      render(
        json: { error: 'Cannot perform this action within 10 seconds of an unshare' },
        status: 499 # using 499 because we do not reraise later on; 404 and 422 do reraise
      )
    end
  end

  def all_shared_tickets
    @all_shared_tickets ||= SharedTicket.with_deleted { current_agreement.shared_tickets.where(uuid: params[:uuid]).to_a }
  end

  def active_shared_ticket
    all_shared_tickets.detect { |shared_ticket| shared_ticket.deleted_at.nil? }
  end
end
