# Please be advised there are multiple "agreements controllers":
#
#   app/controllers/sharing/agreements_controller.rb
#   app/controllers/sharing_agreements_controller.rb
#   app/controllers/api/**/sharing_agreements_controller.rb
#
# the controller in the app/controllers directory (sharing_agreements.controller.rb) is user facing
# the controller in the app/controllers/sharing directory (agreements_controller.rb) is for creating/updating from partner
# the controllers in the app/controllers/api/** directories are for api work

class Sharing::AgreementsController < SharingController
  skip_before_action :authenticate_user
  skip_before_action :verify_authenticity_token
  before_action :authorize_sharing_partner!, except: :create
  before_action :agreement_exists?, only: [:create]

  # Taken from zendesk/ticket_sharing's agreement.rb and actor.rb
  ticket_sharing_agreement_parameters = {
    receiver_url: Parameters.string,
    sender_url: Parameters.string,
    status: Parameters.string,
    uuid: Parameters.string,
    access_key: Parameters.string,
    name: Parameters.string,
    current_actor: Parameters.map(
      uuid: Parameters.string,
      name: Parameters.string,
      role: Parameters.string
    ),
    sync_tags: Parameters.boolean,
    sync_custom_fields: Parameters.boolean,
    allows_public_comments: Parameters.boolean
  }

  allow_parameters :create, **ticket_sharing_agreement_parameters
  def create
    ts_agreement = TicketSharing::Agreement.parse(request.body.read)
    ts_agreement.uuid = params[:uuid]

    ar_agreement = current_account.sharing_agreements.build
    ar_agreement.setup_from_partner(ts_agreement)

    if ar_agreement.save
      render plain: ' ', status: :created
    else
      render json: ar_agreement.errors, status: :unprocessable_entity
    end
  end

  allow_parameters :update, **ticket_sharing_agreement_parameters
  def update
    ts_agreement = TicketSharing::Agreement.parse(request.body.read)
    ar_agreement = current_account.sharing_agreements.find_by_uuid(params[:uuid])
    ar_agreement.update_from_partner(ts_agreement)

    if ar_agreement.save
      render plain: ' ', status: :ok
    else
      render plain: ar_agreement.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def agreement_exists?
    if current_account.sharing_agreements.exists?(uuid: params[:uuid])
      render json: { message: "Agreement #{params[:uuid]} already exists" }, status: :ok
    end
  end
end
