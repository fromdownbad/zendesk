class ImportController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable
  include Zendesk::ImportExport::JobEnqueue

  before_action :authorize_admin
  before_action :find_kind
  before_action :restrict_bulk_import_from_risky_accounts, only: [:create, :index]
  layout 'agent'

  ssl_allowed :all

  SUPPORTED_MODELS = ['user', 'organization'].freeze

  allow_parameters :index, kind: Parameters.string
  def index
    render
  end

  allow_parameters :create,
    report_external_id: Parameters.string, # Type from code
    allow_agent_downgrade: Parameters.string, # Type from code
    commit: Parameters.string, # from code, presence only required
    kind: Parameters.string,
    password_email_change_csv_import: Parameters.boolean,
    raw_data: Parameters.string,
    update_records: Parameters.string, # Type from code
    uploaded_data: Parameters.file
  def create
    return render_failure(
      status: :bad_request,
      title: '400 Bad Request',
      message: 'Only POST requests are allowed'
    ) unless request.post?

    csv_data = (csv_file_upload || form_upload || rest_upload).to_s

    # if the there's not data in csv file then flash error
    if csv_data.blank?
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.import_controller.error_import_document'))
      respond_to do |format|
        format.html { redirect_to action: 'index', kind: @kind }
        format.xml  { render plain: I18n.t('txt.admin.controllers.import_controller.error_import_document'), status: :not_acceptable, layout: false }
      end
    else
      token = current_account.upload_tokens.create(source: current_user)
      attach = token.add_raw_attachment(csv_data, "#{@kind}_import.csv")

      return if file_too_big(attach)

      if attach.save
        enqueue_job(token)
      else
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.import_controller.error_in_import_task'))
        respond_to do |format|
          format.html { redirect_to action: 'index', kind: @kind }
          format.xml  { render plain: I18n.t('txt.admin.controllers.import_controller.error_in_import_task'), status: :not_acceptable, layout: false }
        end
      end
    end
  end

  private

  def find_kind
    @kind = params[:kind] || 'user'
    return render_failure(
      status: :bad_request,
      title: '400 Bad Request',
      message: "Unrecognized kind"
    ) unless SUPPORTED_MODELS.include?(@kind)
  end

  def enqueue_job(token)
    enqueue_job_params(token)
    if no_class_error = set_job_klass
      return no_class_error
    end

    return unavailable_error  if job_policy.unavailable?
    return forbidden!         if job_policy.inaccessible?

    begin
      enqueue
      respond(:notice, notice_message)
    rescue Resque::ThrottledError
      respond(:error, flash_error(throttled_error_message))
    end
  end

  def enqueue_job_params(token)
    job_params = {
      type: "#{@kind}_import",
      return_to: "/people",
      job: {
        token: token.value,
        update_records: params[:update_records]
      }
    }

    if @kind == 'user'
      job_params[:job][:report_external_id] = params[:report_external_id]
      if params['update_records']
        job_params[:job][:password_email_change_csv_import] = params[:password_email_change_csv_import]
        job_params[:job][:allow_agent_downgrade] = params[:allow_agent_downgrade]
      end
    end

    params.merge!(job_params)
  end

  def file_too_big(attach)
    if attach.size > 2.megabytes
      message = I18n.t('txt.admin.controllers.import_controller.file_size_rejection')
      flash[:error] = flash_error(message)
      respond_to do |format|
        format.html { redirect_to action: 'index', kind: @kind }
        format.xml  { render plain: message, status: :not_acceptable, layout: false }
      end
      return true
    end
    false
  end

  def csv_file_upload
    if params[:uploaded_data].blank?
      nil
    else
      uploaded_io = params[:uploaded_data]
      File.new(uploaded_io.path, "rb").read
    end
  end

  def form_upload
    params[:raw_data].blank? ? nil : params[:raw_data]
  end

  def rest_upload
    # Check parameters commit it will has some value when form is submitted
    if params[:commit]
      nil
    else
      request.raw_post
    end
  end

  def restrict_bulk_import_from_risky_accounts
    return if current_account.trusted_bulk_uploader?
    return head(:forbidden) if @kind == SUPPORTED_MODELS.first
  end
end
