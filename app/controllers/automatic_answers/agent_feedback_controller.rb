module AutomaticAnswers
  class AgentFeedbackController < ApplicationController
    include ::AllowedParameters
    before_action :validate_agent
    # NOTE: This can be replaced once we can use required on the parameter types
    before_action :validate_payload

    rescue_from StrongerParameters::InvalidParameter, ActionController::UnpermittedParameters do
      render_failure(
        title: 'Invalid payload',
        message: 'Invalid payload',
        status: :unprocessable_entity
      )
    end

    allow_parameters :agent_feedback,
      article_id: Parameters.bigid,
      irrelevant: Parameters.boolean,
      ticket_id: Parameters.bigid
    def agent_feedback
      ticket = current_account.tickets.find_by_nice_id!(params[:ticket_id])

      return head(:not_found) unless ticket.ticket_deflection

      TicketDeflectionArticle.reject_by_agent(
        ticket.ticket_deflection,
        params[:article_id],
        params[:irrelevant],
        current_user.id,
        current_user.name
      )

      head(:ok)
    end

    private

    def validate_payload
      raise 'Invalid payload' unless [:ticket_id, :article_id, :irrelevant].all? { |s| params.key? s }
    rescue
      head(:unprocessable_entity)
    end

    def validate_agent
      head(:unauthorized) unless current_user.is_agent?
    end
  end
end
