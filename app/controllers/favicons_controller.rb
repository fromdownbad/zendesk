class FaviconsController < AssetsController
  include HelpCenterRedirection

  skip_before_action :normalize_uri

  allow_parameters :show, id: Parameters.string
  def show
    if Zendesk::UserPortalState.new(current_account).show_help_center_favicon?
      redirect_to '/hc/favicon'
    elsif current_account.favicon
      @path = Zendesk::Assets::Path.new(current_account.favicon.path_for_url)
      super
    else
      send_file "#{Rails.public_path}/favicon.ico", type: "image/x-icon"
    end
  end
end
