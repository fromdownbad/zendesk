class SharingController < ApplicationController
  include ::AllowedParameters
  ssl_allowed :all

  rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do
    render plain: "Database collision - please try again", status: :conflict
  end

  private

  attr_reader :current_agreement

  def authorize_sharing_partner!
    token = request.headers['X-Ticket-Sharing-Token'].to_s
    uuid, access_key = token.split(':', 2)

    if uuid && access_key
      @current_agreement = current_account.sharing_agreements.
        find_by_uuid_and_access_key(uuid, access_key)
    end

    unless @current_agreement
      render plain: '', status: :unauthorized
    end
  end

  def authorize_sharing_agreement!
    unless @current_agreement.allows_partner_updates?
      Rails.logger.info("agreement status #{@current_agreement.status}")
      render plain: '', status: :forbidden
    end
  end
end
