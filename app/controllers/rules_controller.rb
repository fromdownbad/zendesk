require 'zendesk/rules/controller_support'
#
# This is an abstract class. It doesn't directly serve requests.
# Requests are routed to subclasses via the RuleRoutingMiddleware based on params[:id], params[:rule][:type] or params[:filter]
#
class RulesController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Rules::ControllerSupport
  include Zendesk::Rules::ControllerSupport::Copying

  ssl_allowed :all

  rescue_from DefinitionItem::InvalidSourceColumn do |_exception|
    render_failure(
      status: :bad_request, title: "Invalid source column",
      message: "The source column provided is not applicable."
    )
  end

  restrict_access

  before_action :check_permissions_for_destroy_inactive,  only: [:destroy_inactive]
  before_action :authorize_manage_access_for_new_rule,    only: [:new]
  before_action :authorize_access_to_copy_existing_rule,  only: [:new], if: :existing_rule?
  before_action :register_device_activity,                only: [:index]

  helper_method :allow_rule_manipulations?
  helper_method :execution_count
  helper_method :include_preview?

  tab :manage, except: [:search, :show]

  layout 'agent'

  def self.inherited(subclass)
    super
    subclass.extend(Zendesk::Rules::ControllerSupport::InheritViews)
  end

  allow_parameters :index,
    filter: Parameters.string,
    sort: Parameters.string,
    select: Parameters.string,
    page: Parameters.integer # untested, used in active/inactive_rules
  def index
    @page_title = rule_type.capitalize
    @apps_requirements_rules = controller_name.classify.constantize.apps_requirements(current_account)

    active_rules
    inactive_rules

    respond_to do |format|
      format.html
      format.mobile_v2
    end
  end

  # Only Views support show
  allow_parameters :show,
    id: Parameters.bigid
  def show
    render plain: I18n.t('txt.admin.controllers.rules.analysis_controller.na_label')
  end

  allow_parameters :edit,
    return_to: Parameters.string,
    id: Parameters.bigid
  def edit
    rule.prepare_for_editing!
    @definition = rule.definition
    @output     = rule.output
    respond_to do |format|
      format.html { render }
      format.mobile_v2 { return render_failure(status: :not_found, title: I18n.t('txt.errors.not_found.label'), message: I18n.t('txt.errors.not_found.message')) }
    end
  end

  allow_parameters :new,
    filter: Parameters.string,
    copy_id: Parameters.integer,
    return_to: Parameters.string,
    user_id: Parameters.bigid
  def new
    if existing_rule
      build_copy_from_existing_rule
    else
      build_from_params
      rule.per_page = API_DEFAULT_PER_PAGE
    end
    @rule.definition = @definition
    @rule.prepare_for_editing! if existing_rule
    @rule.output = @output
    if params[:user_id].blank?
      rule.owner ||= current_account
    else
      rule.owner = current_account.users.find_by_id(params[:user_id])
    end
    render action: 'edit'
  end

  rule_parameter = Parameters.map(
    owner_type: Parameters.enum('Account', 'Group', 'User'),
    title: Parameters.string,
    type: Parameters.string,
    owner_id: Parameters.integer
  )

  output_parameter = Parameters.map(
      columns: Parameters.string,
      group: Parameters.string,
      group_asc: Parameters.boolean,
      order: Parameters.string,
      order_asc: Parameters.boolean,
      type: Parameters.string
    )

  allow_parameters :create,
    output: output_parameter,
    return_to: Parameters.string,
    rule: rule_parameter,
    sets: Parameters.anything,
    type: Parameters.string
  def create
    @rule = owner.send(rule_type).build(params[:rule])
    @rule.definition   = definition if definition
    @rule.output       = output     if output
    @rule.current_user = current_user

    respond_to do |format|
      if @rule.save
        format.html do
          set_admin_cookie('created', @rule)

          smart_redirect
        end
      else
        format.html do
          flash[:error] = flash_error(I18n.t('txt.admin.controllers.rules_controller.rule_created_failure_' + rule_name), @rule)
          @definition   = @rule.definition
          @output       = @rule.output

          render action: 'edit', status: :not_acceptable
        end
      end
    end
  end

  allow_parameters :update,
    id: Parameters.integer,
    output: output_parameter,
    owner_id: Parameters.integer,
    return_to: Parameters.string,
    rule: rule_parameter.dup.merge(is_active: Parameters.boolean),
    sets: Parameters.anything,
    submit_type: Parameters.enum('delete', 'deactivate', 'activate', 'update')
  def update
    submit_type = params[:submit_type]

    case submit_type
    when "delete"
      return destroy
    when "deactivate"
      params[:rule][:is_active] = false
    when "activate"
      params[:rule][:is_active] = true
    end

    rule.definition = definition if definition
    rule.output     = output     if output
    rule.owner      = owner

    update_rule do
      rule.update_attributes(params[:rule])
    end
  end

  allow_parameters :destroy_inactive,
    filter: Parameters.string,
    return_to: Parameters.string,
    select: Parameters.string,
    page: Parameters.integer # untested, used in inactive_rules
  def destroy_inactive
    @page_title = rule_type.capitalize

    inactive_rules.each { |rule| rule.soft_delete(validate: false) }

    respond_to do |format|
      format.html do
        flash[:notice] = I18n.t("txt.admin.controllers.rules_controller.inactive_deleted_success_count_#{rule_type}", count: inactive_rules.size)
        redirect_to_return_to('/')
      end
    end
  end

  # Rules can be activated/de-activated
  allow_parameters :activate,
    id: Parameters.integer
  def activate
    rule.toggle_activation
    success = (rule.is_active? ? rule.save : rule.save(validate: false))

    unless success
      flash[:error] = flash_error(
        I18n.t(
          'txt.admin.controllers.rules_controller.rule_updated_failure_' +
            rule_name
        ),
        rule
      )
    end

    smart_redirect
  end

  allow_parameters :sort,
    'active-rules-sort-list' => Parameters.array(Parameters.integer),
    filter: Parameters.string
  def sort
    current_account.all_rules.update_position(params['active-rules-sort-list'])
    current_account.expire_scoped_cache_key(:macros)
    current_account.expire_scoped_cache_key(:rules)
    head :ok
  end

  allow_parameters :destroy,
    id: Parameters.integer,
    return_to: Parameters.string
  def destroy
    respond_to do |format|
      if rule.soft_delete(validate: false)
        format.html do
          smart_redirect
        end
      else
        format.html do
          flash[:error] = flash_error(I18n.t('txt.admin.controllers.rules_controller.rule_deleted_failure', rule_type: rule_name, rule_name: CGI.escapeHTML(rule.rendered_title)))
          render(action: 'edit', id: rule.id)
        end
      end
    end
  end

  private

  def include_preview?
    return true if @rule.is_a?(Automation)

    @rule.is_a?(View) && current_user.can?(:view, View)
  end

  def return_to_params
    # remove beginning backslash if it exists
    params[:return_to].to_s.gsub(/^\//, "")
  end

  def smart_redirect
    return_to = if return_to_params.present?
      "#{root_url}#{return_to_params}"
    else
      send("admin_#{rule.rule_name.pluralize}_path")
    end

    redirect_to return_to
  end

  def inactive_rules
    return @inactive_rules if @inactive_rules

    find_rules
    @inactive_rules
  end

  def active_rules
    return @active_rules if @active_rules

    find_rules
    @active_rules
  end

  def rule_name
    @rule.rule_name.capitalize # if you remove capitalize it will break translation keys
  end

  def set_admin_cookie(action, rule)
    cookies[:zd_admin_notice] = {
      expires: 1.minute.from_now,
      value:   {
        action: action == 'created' ? 'create' : action,
        id:     rule.id,
        result: 'success',
        type:   rule.rule_type
      }.to_json
    }
  end

  def update_rule
    respond_to do |format|
      if yield
        format.html do
          set_admin_cookie('update', @rule)

          smart_redirect
        end
      else
        format.html do
          flash[:error] = flash_error(I18n.t('txt.admin.controllers.rules_controller.rule_updated_failure_' + rule_name), rule)
          @definition   = rule.definition
          @output       = rule.output

          render action: 'edit'
        end
      end
    end
  end

  def execution_count(rule)
    rule.usage.public_send(timeframe)
  end
end
