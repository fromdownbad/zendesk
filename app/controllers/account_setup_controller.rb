require 'zendesk/pod_relay'

class AccountSetupController < ApplicationController
  include ::AllowedParameters
  include Zendesk::PodRelay

  # The order of this hash is used to determine the order of precedence when multiple products are being set up
  PRODUCT_TYPE_NAMES = {
    chat: 'chat',
    outbound: 'connect'
  }.freeze

  PRODUCT_TYPE_PATHS = {
    chat: '/chat',
    outbound: ''
  }.freeze

  skip_before_action :authenticate_user
  skip_before_action :verify_current_account
  skip_before_action :ensure_support_is_active!
  skip_before_action :set_ip_address_for_current_user
  skip_before_action :verify_authenticated_session
  skip_before_action :log_session_info
  skip_before_action :set_mobile_format
  skip_before_action :prepare_payment_notification

  before_action :validate_jwt
  before_action :validate_subdomain
  before_action :redirect_to_pod

  layout false

  helper_method :jwt
  helper_method :owner_verify_link
  helper_method :readiness_check_url
  helper_method :first_product_path
  helper_method :first_product_name

  allow_parameters :show, jwt: Parameters.string
  def show
    redirect_to owner_verify_link if readiness_check[:state] == Zendesk::AccountReadiness::STATE_COMPLETE
  end

  allow_parameters :state, jwt: Parameters.string
  def state
    respond_to do |format|
      format.json { render json: readiness_check, status: :ok }
    end
  end

  private

  attr_reader :jwt_claims

  def readiness_check
    current_state = Zendesk::AccountReadiness::STATE_PENDING

    # Lag simulation: Optionally add forced wait time into the readiness check for testing purposes
    Rails.logger.info("Lag simulation: Account setup forced wait time is #{forced_wait_seconds} seconds for #{idsub}")
    if forced_wait_seconds && Time.now < Time.at(iat + forced_wait_seconds)
      tags = ['cause:forced_wait']
      Rails.logger.info("Lag simulation: Account setup forced wait time is ongoing for #{idsub}")
      statsd_client.increment("account_setup.state.#{current_state}", tags: tags)
    else
      current_state = Zendesk::AccountReadiness.new(account_id: account_id, subdomain: subdomain, products_activated: products_activated, source: "account_setup_controller").readiness_check
      completion_time = Time.now - Time.at(iat)
      statsd_client.histogram('account_setup.completion_time', completion_time)
    end

    { state: current_state }
  end

  def jwt
    @jwt ||= params[:jwt]
  end

  def account_service_jwt_secret
    ENV['ACCOUNT_SERVICE_JWT_SECRET']
  end

  def validate_subdomain
    if subdomain != request.host.split('.').first
      statsd_client.increment('account_setup.invalid_subdomain')
      respond_with_not_found
    end
  end

  def idsub
    "#{account_id}:#{subdomain}"
  end

  def account_id
    jwt_claims[:account_id]
  end

  def subdomain
    jwt_claims[:subdomain]
  end

  def products_activated
    jwt_claims[:products_activated]
  end

  def owner_verify_link
    jwt_claims[:owner_verify_link]
  end

  def forced_wait_seconds
    jwt_claims[:forced_wait_seconds].to_i
  end

  def iat
    jwt_claims[:iat].to_i
  end

  def validate_jwt
    @jwt_claims = JWT.decode(jwt, account_service_jwt_secret).first.with_indifferent_access
  rescue JWT::VerificationError, JWT::DecodeError, JWT::ExpiredSignature => e
    Rails.logger.info("Invalid JWT - #{e}")
    statsd_client.increment('account_setup.invalid_jwt')
    respond_with_not_found
  end

  def respond_with_not_found
    respond_to do |format|
      format.html do
        render(
          file: "#{Rails.public_path}/404",
          formats: [:html],
          status: :not_found,
          layout: false
        )
      end
      format.all { head :not_found }
    end
  end

  def redirect_to_pod
    target_pod_id = jwt_claims[:pod_id].to_i
    if target_pod_id != Zendesk::Configuration.fetch(:pod_id) && can_relay_request?
      return relay_request_cross_pod(target_pod_id)
    end
  end

  def readiness_check_url
    "#{state_account_setup_url}?jwt=#{params[:jwt]}"
  end

  def statsd_client
    Rails.application.config.statsd.client
  end

  def first_product
    @first_product ||= begin
      product_hash = products_activated.each_with_object({}) { |p, h| h[p.to_sym] = p }
      product_name = (PRODUCT_TYPE_NAMES.keys & product_hash.keys).first
      (product_hash[product_name] || PRODUCT_TYPE_NAMES.keys.first).to_sym
    end
  end

  def first_product_name
    PRODUCT_TYPE_NAMES[first_product]
  end

  def first_product_path
    PRODUCT_TYPE_PATHS[first_product]
  end
end
