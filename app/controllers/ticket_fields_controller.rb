require 'zendesk/tickets/ticket_field_manager'

class TicketFieldsController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable

  ticket_field_type = Parameters.map # TODO: this looks too variable
  id_type           = Parameters.bigid | Parameters.string

  ssl_allowed :all

  before_action :authorize_admin, except: [:index]
  before_action :authorize_agent, only: [:index]

  before_action :sanitize_user_input, only: [:create, :update]
  before_action :find_field, only: [:show, :edit, :update, :destroy, :activate]
  before_action :new_field, only: [:new, :create]
  before_action :register_device_activity, only: [:index]
  before_action :check_tag_uniqueness, only: [:activate], if: :inactive_tagger_field?

  rescue_from Zendesk::CustomField::UnknownCustomFieldTypeError, with: :unknown_custom_field_type_error

  tab :manage

  @@valid_field_models_for_creation = ['field_checkbox', 'field_date', 'field_decimal', 'field_integer', 'field_regexp', 'field_text', 'field_textarea', 'field_tagger', 'field_partial_credit_card']

  layout 'agent'

  # implicit action
  allow_parameters :edit, id: id_type, ticket_field: ticket_field_type

  allow_parameters :index, {}

  def index
    @active_fields = current_account.ticket_fields.active
    @inactive_fields = current_account.ticket_fields.inactive
    @apps_requirements_ticket_fields = TicketField.apps_requirements(current_account)

    respond_to do |format|
      format.html { render }
    end
  end

  allow_parameters :new, id: id_type, ticket_field: ticket_field_type

  def new
    render action: 'edit'
  end

  allow_parameters :create, id: id_type, ticket_field: ticket_field_type

  def create
    if @ticket_field.save
      flash[:notice] = I18n.t('txt.admin.controllers.ticket_fields_controller.field_created', title_ticket_field: "<a class='title' href='#{edit_ticket_field_path(@ticket_field)}'>#{ticket_field_title_with_dc}</a>")
      redirect_to ticket_fields_path
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.ticket_fields_controller.field_could_not_be_created_label'), @ticket_field)
      render action: 'edit'
    end
  end

  allow_parameters :update, id: id_type, ticket_field: ticket_field_type

  def update
    params[:ticket_field][:custom_field_options] ||= [] if @ticket_field.is_a?(FieldTagger)

    if ticket_field_manager.update(@ticket_field, params[:ticket_field]).save
      flash[:notice] = I18n.t('txt.admin.controllers.ticket_fields_controller.field_updated', title_ticket_field: "<a class='title' href='#{edit_ticket_field_path(@ticket_field)}'>#{ticket_field_title_with_dc}</a>")
      redirect_to ticket_fields_path
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.ticket_fields_controller.field_could_not_be_updated_label'), @ticket_field)
      render action: 'edit'
    end
  end

  allow_parameters :destroy, id: id_type

  def destroy
    render(text: "400 Bad request", status: :bad_request) && return unless request.delete?
    if @ticket_field.destroy
      flash[:notice] = I18n.t('txt.admin.controllers.ticket_fields_controller.field_deleted', title_ticket_field: ticket_field_title_with_dc)
      redirect_to ticket_fields_path
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.ticket_fields_controller.field_could_not_be_deleted', title_ticket_field: ticket_field_title_with_dc), @ticket_field)
      render action: 'edit'
    end
  end

  allow_parameters :activate, id: id_type

  # Fields can be activated/de-activated
  def activate
    unless ticket_field_can_be_toggled?
      @ticket_field.toggle!
      state = @ticket_field.is_active? ? I18n.t('txt.admin.controllers.ticket_fields_controller.ticket_field_activated', title_ticket_field: ticket_field_title_with_dc) : I18n.t('txt.admin.controllers.ticket_fields_controller.ticket_field_deactivated', title_ticket_field: ticket_field_title_with_dc)
      flash[:notice] = state
    end

    redirect_to action: 'index'
  end

  allow_parameters :sort, "field-list": Parameters.array(Parameters.string)

  def sort
    params['field-list'].each do |item|
      TicketField.find(item).update_attribute(:position, params['field-list'].index(item))
    end
    head :ok
  end

  private

  def unknown_custom_field_type_error
    render(text: "400 Bad request", status: :bad_request)
  end

  def ticket_field_can_be_toggled?
    if @ticket_field.is_active && !@ticket_field.can_be[:deactivated]
      flash[:error] = I18n.t('txt.admin.controllers.ticket_fields_controller.field_cannot_be_deactivated', title_ticket_field: ticket_field_title_with_dc)
    elsif !@ticket_field.is_active && !@ticket_field.can_be[:activated]
      flash[:error] = if @ticket_field.is_a?(FieldPartialCreditCard)
        I18n.t('txt.admin.models.ticket_field.ticket_field.partial_credit_card_field_cannot_be_activated_with_name', title_ticket_field: ticket_field_title_with_dc, url: I18n.t('txt.admin.models.ticket_field.ticket_field.partial_credit_card_field_learn_more_url'))
      else
        I18n.t('txt.admin.controllers.ticket_fields_controller.field_cannot_be_activated', title_ticket_field: ticket_field_title_with_dc)
      end
    end

    flash[:error].present?
  end

  def check_tag_uniqueness
    tags = @ticket_field.type == 'FieldTagger' ? @ticket_field.custom_field_options.map { |o| o.value.downcase } : [@ticket_field.tag]

    Zendesk::CustomField::DropdownTagger.validate_to_activate(@ticket_field.errors, @ticket_field, tags)
    if @ticket_field.errors.any?
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.ticket_fields_controller.field_could_not_be_updated_label'), @ticket_field)
    end
  end

  def inactive_tagger_field?
    !@ticket_field.is_active && ['FieldTagger', 'FieldCheckbox'].include?(@ticket_field.type)
  end

  def ticket_field_title_with_dc
    @ticket_field_title_with_dc ||= CGI.escapeHTML(Zendesk::Liquid::DcContext.render(@ticket_field.title, current_account, current_user))
  end

  def find_field
    @ticket_field = current_account.ticket_fields.find(params[:id])
  end

  def new_field
    options = params[:ticket_field]

    if options.nil? && params[:id].present?
      options = { type: params[:id].to_s.camelize }
    end

    @ticket_field = ticket_field_manager.build(options)
  end

  def sanitize_user_input
    return unless params[:ticket_field]
    ['title', 'title_in_portal', 'description'].each do |key|
      next unless params[:ticket_field].key? key
      params[:ticket_field][key] = params['ticket_field'][key].to_s.sanitize.strip
    end
  end

  def ticket_field_manager
    Zendesk::Tickets::TicketFieldManager.new(current_account)
  end
end
