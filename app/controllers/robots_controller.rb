class RobotsController < ApplicationController
  include ::AllowedParameters

  allow_anonymous_users

  allow_parameters :index, {}
  def index
    # Let cloudflare cache ALL requests behind "allow_anonymous_users" filter,
    # there is no reason not to do that
    expires_in(1.hour, public: true)

    # Skip generating the session in order make it cacheable on Cloudflare
    env['zendesk.session.persistence'] = false

    @sitemap_uri = sitemap_uri
    @trial = current_account.is_trial?

    render content_type: Mime[:text]
  end

  private

  def sitemap_uri
    URI.join(current_brand.url, "/hc/", "sitemap.xml")
  end
end
