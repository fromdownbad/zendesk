class SatisfactionRatingsController < ApplicationController
  include ::AllowedParameters

  TOKEN_POSSIBLE = [:new, :create_via_token].freeze
  BOT_USER_AGENTS = [
    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)", # ZD#659682 ignoring 1.3% of our legitimate traffic
    "Mozilla/4.0", # ZD#659682 seems to be either yahoo or chinese firewall bot
  ].freeze

  skip_before_action :authenticate_user, only: TOKEN_POSSIBLE + [:latest]
  skip_before_action :verify_authenticity_token, only: :create_via_token

  before_action :account_must_have_satisfaction_ratings_enabled
  before_action :account_must_have_public_satisfaction_ratings_enabled, only: [:latest]

  before_action :require_own_ticket, only: [:create]
  before_action :load_token, only: TOKEN_POSSIBLE
  before_action :require_ticket_via_token_or_user, only: TOKEN_POSSIBLE
  before_action :register_satisfaction_rating_intention, only: TOKEN_POSSIBLE, if: :can_rate_ticket?
  before_action :redirect_to_request_and_consume_token_if_signed_in, only: TOKEN_POSSIBLE
  before_action :cannot_rate_closed_tickets!, except: [:latest, :good_this_week, :bad_this_week]
  before_action :user_must_be_able_to_rate_satisfaction!, except: [:latest, :good_this_week, :bad_this_week]
  before_action :current_user_must_be_agent, only: [:good_this_week, :bad_this_week]

  skip_before_action :set_mobile_format
  ssl_allowed        :latest

  allow_parameters :latest, {}
  def latest
    @ratings = Satisfaction::Rating.distribution(current_account)

    respond_to do |format|
      format.js   { render }
      format.json { render json: @ratings, callback: params[:callback] }
    end
  end

  allow_parameters :new,
    intention: Parameters.string,
    ticket_id: Parameters.string,
    token: Parameters.string
    # _: Parameters.integer From logging, ignored
  def new
    @latest_satisfaction_rating = @ticket.latest_satisfaction_rating(@ticket.requester)

    if current_account.has_csat_page_refresh? && current_account.has_csr_token_retention?
      @latest_satisfaction_rating[:score] = params[:intention] if params[:intention].present?
      render action: 'new_rating', layout: 'csat'
    else
      render action: 'new', layout: 'neutral'
    end
  end

  ticket_parameters = {
    satisfaction_comment: Parameters.string,
    satisfaction_reason_code: Parameters.integer,
    satisfaction_score: Parameters.integer
  }

  allow_parameters :create,
    intention: Parameters.string,
    ticket: ticket_parameters,
    ticket_id: Parameters.bigid,
    token: Parameters.string
  def create
    @ticket.attributes = params[:ticket]
    @ticket.will_be_saved_by(user, via_id: ViaType.WEB_FORM)
    @ticket.save!

    respond_to do |format|
      format.json { json_create_response(@ticket) }
    end
  end

  allow_parameters :create_via_token,
    intention: Parameters.string,
    satisfaction_rating: Parameters.string, # Passed by form, never used
    ticket: ticket_parameters,
    ticket_id: Parameters.bigid,
    token: Parameters.string
  def create_via_token
    create
  end

  allow_parameters :good_this_week, agent_id: Parameters.bigid
  def good_this_week
    create_view_for_satisfaction_rating
    @tickets = solved_tickets(
      [SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT]
    )
    @count = @tickets.count(:all)
  end

  allow_parameters :bad_this_week, agent_id: Parameters.bigid
  def bad_this_week
    create_view_for_satisfaction_rating
    @tickets = solved_tickets(
      [SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT]
    )
    @count = @tickets.count(:all)
  end

  protected

  # Added as an attempt to solve/mitigate performance issues, see.
  # https://zendesk.atlassian.net/browse/API-927
  # The goal is to reduce the number of incoming requests for the index endpoint.
  def max_per_page
    1000
  end

  def solved_tickets(ratings)
    user = current_account.users.find(params[:agent_id])
    current_account.tickets.solved.
      where("satisfaction_ratings.score IN (?) AND assignee_id = ? AND solved_at >= ?", ratings, user.id, current_account.start_of_week).
      joins(:satisfaction_ratings).includes(:satisfaction_ratings)
  end

  def create_view_for_satisfaction_rating
    @rule = View.new
    @output = Output.create(group: :status, group_asc: true, order: :id, order_asc: true, type: 'table',
                                 columns: [:description, :requester, :created, :updated, :group, :assignee])
    @rule.output = @output
  end

  def account_must_have_satisfaction_ratings_enabled
    head(:forbidden) unless current_account.has_customer_satisfaction_enabled?
  end

  def account_must_have_public_satisfaction_ratings_enabled
    head(:forbidden) unless current_account.settings.public_customer_satisfaction?
  end

  def require_own_ticket
    @ticket = current_account.tickets.find_by_nice_id!(params[:ticket_id])
    cannot_rate_ticket! unless @ticket.requested_by?(current_user)
  end

  def require_ticket_via_token_or_user
    if current_user_exists?
      require_own_ticket
    elsif !@ticket
      head :unauthorized
    end
  end

  def load_token
    # If token is wrong N times in the last window, then no more tries.
    if Prop.throttled?(:satisfaction_token_attempts, [current_account.id, request.remote_ip])
      Rails.logger.warn(
        "[SECURITY] Satisfaction token attempt refused for " \
        "#{request.remote_ip} on account #{current_account.id}"
      )

      render(action: :no_such_token, status: :not_found) && return
    end

    @token = current_account.satisfaction_rating_tokens.lookup(params[:token]).first

    if @token.nil?
      if Prop.throttle(:satisfaction_token_attempts, [current_account.id, request.remote_ip])
        Rails.logger.warn(
          "[SECURITY] Satisfaction token attempts rate limited for " \
          "#{request.remote_ip} on account #{current_account.id}"
        )

        render(action: :no_such_token, status: :not_found) && return
      end
    else
      if @token.ticket && @token.ticket.nice_id == params[:ticket_id].to_i
        @ticket = @token.ticket
        @user   = @ticket.requester
      end
    end
  end

  def redirect_to_request_and_consume_token_if_signed_in
    return if current_account.has_csr_token_retention?

    if current_user && !current_user.is_anonymous_user?
      if @token.present?
        Rails.logger.info("User id: #{current_user.id} is destroying token: #{@token.value}")
        @token.destroy
      end
      ticket = current_account.tickets.find_by_nice_id(params[:ticket_id])
      options = { user: current_user }
      options[:params] = { intention: SatisfactionType[@intention.score].name.downcase } if @intention
      redirect_to ticket.url_path(options)
    end
  end

  def user_must_be_able_to_rate_satisfaction!
    # User may be `current_user` or the requester of the ticket if this rating is being
    # created via token auth.
    cannot_rate_ticket! if no_permission_to_rate?
  end

  def cannot_rate_ticket!
    if current_user.is_agent? || is_assuming_user?
      render_failure(
        status: :forbidden,
        title: I18n.t('txt.errors.access_denied.title'),
        message: I18n.t('public.controllers.satisfaction_ratings_controller.agents_cannot_rate_tickets')
      )
    else
      deny_access
    end
  end

  def cannot_rate_closed_tickets!
    if ticket_closed?
      render_failure(status: :gone, title: I18n.t('txt.satisfaction.request_closed'), message: I18n.t('txt.satisfaction.cannot_rate'))
    end
  end

  def can_rate_ticket?
    @ticket.present? && !ticket_closed? && !no_permission_to_rate?
  end

  def no_permission_to_rate?
    is_assuming_user? || !user.can?(:rate_satisfaction, @ticket)
  end

  def ticket_closed?
    @ticket && @ticket.closed?
  end

  def json_create_response(ticket)
    render json: ticket.latest_satisfaction_rating, status: :created
  end

  def current_user_must_be_agent
    unless current_user.is_agent?
      head(:forbidden)
    end
  end

  def user
    @user || current_user
  end

  def register_satisfaction_rating_intention
    return unless score = params[:intention]

    # validate the score to skip hitting the db
    unless SatisfactionRatingIntention::ALLOWED_SCORES.include?(score.to_s.to_i)
      return render json: { error: "invalid intention value" }, status: :unprocessable_entity
    end

    if !user&.is_anonymous_user? && !prefetch_bot? && !request.head?
      scope = current_account.satisfaction_rating_intentions
      Zendesk::DB::RaceGuard.guarded do
        @intention = scope.find_by_ticket_id(@ticket.id) || scope.build(ticket: @ticket, user: user)
        @intention.score = score
        return render json: { error: "unable to save intention" }, status: :unprocessable_entity unless @intention.save
      end

      if current_account.has_csat_page_refresh?
        flash.now[:notice] ||= I18n.t("txt.satisfaction.intention_created_message")
      else
        flash[:notice] ||= I18n.t("txt.satisfaction.intention_created_message")
      end

      # help us track down bots and later ignore them
      Rails.logger.info("Creating intention for #{request.user_agent} - #{request.accept} -- #{request.ip}")
    end
  end

  def prefetch_bot?
    BOT_USER_AGENTS.include?(request.user_agent.to_s)
  end
end
