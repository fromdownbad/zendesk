class AttachmentTokenController < ApplicationController
  include AttachmentsControllerMixin
  include ::AllowedParameters

  allow_zendesk_mobile_app_access
  skip_before_action :authenticate_user, :verify_current_account, :ensure_support_is_active!

  before_action :validate_domain
  before_action :require_account
  before_action :redirect_nonlocal_accounts
  around_action :on_shard
  before_action :load_token
  before_action :apply_ip_restrictions
  before_action :set_access_control_header

  allow_parameters :show,
    token: Parameters.string,
    account_id: Parameters.bigid,
    attachment_token: Parameters.string

  def show
    if token_expired_or_used?
      handle_error("token expired")
    else
      statsd_client.increment("valid")
      send_attachment(attachment)
    end
  end

  allow_parameters :not_found, :skip # TODO: make stricter
  def not_found
    render_failure(
      status: :not_found, title: 'Attachment expired',
      message: "This attachment has expired. Please try accessing again."
    )
  end

  private

  def token_expired_or_used?
    # with public attachments we don't care if the token is expired or used since anybody can view this attachment
    (@token.expired? || @token.used!) && !account.anonymous_user.can?(:view, attachment)
  end

  def attachment
    @attachment ||= Attachment.where(account_id: account.id).find(@token.attachment_id)
  rescue ActiveRecord::RecordNotFound
    statsd_client.increment("invalid")
    raise $!
  end

  def set_access_control_header
    # Needed due to X-Accel-Redirect
    headers["X-Access-Control-Allow-Origin"] = '*'
  end

  def validate_domain
    return if Rails.env.development?
    if request.base_url != attachment_domain_host
      logger.error "blocking attachment download on non-attachment domain"
      render plain: 'Wrong domain', status: :forbidden
    end
  end

  def apply_ip_restrictions
    user = account.users.find(@token.user_id) if @token.user_id.present?
    if Zendesk::Net::IPTools.should_block_user?(account, user, request.env)
      Rails.logger.info("Blocking #{Zendesk::Net::IPTools.trusted_ip(request.env)} as it is not in the ip-whitelist of account #{account.id}")
      render plain: 'Forbidden', status: :forbidden
    end
  end

  def load_token
    @token ||= Zendesk::Attachments::Token.new(token: params[:token].to_s)
  rescue OpenSSL::Cipher::CipherError, JSON::JWT::InvalidFormat, JSON::JWE::DecryptionFailed, Zendesk::Attachments::Token::InvalidJWE
    handle_error("caught #{$!.class}")
  end

  def handle_error(message)
    logger.error message
    if params[:account_id] && params[:attachment_token] && attachment = Attachment.where(account_id: params[:account_id].to_i, token: params[:attachment_token].to_s).first
      logger.error "redirecting to zendesk domain to get a new token"
      statsd_client.increment("autorefresh")
      redirect_to attachment.url
    else
      logger.error "unable to find attachment - rendering error page"
      statsd_client.increment("invalid", tags: [error: $!.class.to_s])
      not_found
    end
  end

  def require_account
    not_found unless account
  end

  def account
    @account ||= if params[:account_id]
      Account.find(params[:account_id]).tap do |account|
        decorate_trace_with_account(account)
      end
    end
  end

  # We aren't covered by ZendeskCoreMiddleware::Account, so redirect the user here
  def redirect_nonlocal_accounts
    pod_for_shard = Zendesk::Accounts::DataCenter.pod_id_for_shard(account.shard_id)
    if pod_for_shard != Zendesk::Configuration['pod_id']
      raise "Could not lookup pod for shard #{account.shard_id}" if pod_for_shard.nil?
      redirect_to "#{attachment_domain_host(pod: pod_for_shard)}#{request.fullpath}", status: :moved_permanently
    end
  end

  # We aren't covered by ZendeskCoreMiddleware::Account, so add the account info to traces here
  def decorate_trace_with_account(account)
    return unless ZendeskAPM.enabled?

    if request_span = DatadogMiddleware.find_request_span(request.env)
      request_span.set_tag('zendesk.account_id', account&.id || DatadogMiddleware::NOT_AVAILABLE)
      request_span.set_tag('zendesk.account_subdomain', account&.subdomain || DatadogMiddleware::NOT_AVAILABLE)
    end
  end

  def on_shard
    ActiveRecord::Base.on_shard(account.shard_id) { yield }
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["attachments", "jwe"])
  end
end
