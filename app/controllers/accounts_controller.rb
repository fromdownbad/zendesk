require 'zendesk/pod_relay'

class AccountsController < ApplicationController
  include ::AllowedParameters
  allow_zendesk_mobile_app_access

  before_action :set_default_locale

  skip_before_action :authenticate_user, :verify_current_account, :verify_authenticated_session, :ensure_proper_protocol
  skip_around_action :restrict_to_current_account
  skip_around_action :audit_action, only: :new
  skip_before_action :ensure_support_is_active!

  include Zendesk::Accounts::LocationControllerSupport
  include Zendesk::Accounts::OfacControllerSupport
  include Zendesk::PodRelay

  allow_parameters :new, {}
  def new
    redirect_to "http://www.zendesk.com/signup" if Rails.env.production?
  end

  allow_parameters :reminder,
    email: Parameters.string,
    type: Parameters.string,
    callback: Parameters.string
    # _: Parameters.integer - From logging, ignored
  def reminder
    AccountsReminderJob.enqueue(params[:email], I18n.locale, params[:type])
    render json: {success: true}, callback: params[:callback]
  end

  allow_parameters :available,
    subdomain: Parameters.string,
    callback: Parameters.string
  def available
    render(
      json: {success: Zendesk::RoutingValidations.subdomain_available?(params[:subdomain])},
      callback: params[:callback]
    )
  end

  private

  def set_default_locale
    I18n.locale = DefaultLocaleChooser.choose(params[:language])
    Rails.logger.append_attributes(default_locale: I18n.locale.inspect)
  end
end
