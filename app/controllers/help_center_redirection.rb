module HelpCenterRedirection
  private

  def perform_help_center_redirection(url)
    if url
      redirect_to url, status: :moved_permanently
    else
      render_404_page
    end
  end

  def help_center_root_path
    '/hc'
  end

  def help_center_requests_path(id = nil)
    if id.nil?
      '/hc/requests'
    else
      "/hc/requests/#{id}"
    end
  end

  def help_center_new_request_path
    "/hc/requests/new"
  end

  def should_redirect_to_help_center?
    !current_account.help_center_disabled? && !current_brand.nil? && current_brand.help_center_in_use?
  end

  def is_web_portal_disabled? # rubocop:disable Naming/PredicateName
    if current_account.web_portal_state == :disabled
      render_404_page
    end
  end
end
