class Rules::AutomationsController < RulesController
  include Zendesk::LotusEmbeddable
  include Zendesk::Rules::ControllerSupport::BusinessRuleAuthorization

  protected

  def build_from_params
    @rule = current_account.automations.build(params[:rule])
  end

  def load_rules
    @rules = Zendesk::RuleSelection::ExecutionCounter.new(
      account: current_account,
      type:    :automation
    ).rules_with_execution_counts(timeframe, current_account.automations)
  end
end
