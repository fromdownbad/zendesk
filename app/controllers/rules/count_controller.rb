class Rules::CountController < ApplicationController
  include ::AllowedParameters
  allow_zendesk_mobile_app_access

  ssl_allowed :index
  before_action :authorize_agent

  skip_around_action :audit_action, :restrict_to_current_account
  skip_before_action(
    :set_ip_address_for_current_user,
    :prepare_payment_notification,
    :perform_language_detection,
    :set_mobile_format,
    :ensure_proper_protocol
  )

  allow_parameters :index,
    rule_ids: Parameters.array(Parameters.integer) | Parameters.map
  def index
    ids = params[:rule_ids]
    if HashParam.ish?(ids)
      ids = ids.keys
    end

    rules = ::Rule.where(id: ids).to_a
    rules.reject! { |rule| rule.account != current_account }

    counts = ::CachedRuleTicketCount.lookup(rules, current_user, refresh: Rails.env.development? ? nil : :delayed)

    render json: counts.map(&:as_json)
  end
end
