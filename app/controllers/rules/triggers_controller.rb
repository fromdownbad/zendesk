class Rules::TriggersController < RulesController
  include Zendesk::LotusEmbeddable
  include Zendesk::Rules::ControllerSupport::BusinessRuleAuthorization

  before_action :check_permissions_for_edit, only: %i[activate destroy update]

  protected

  def build_from_params
    @rule = current_account.triggers.build(params[:rule])
  end

  def find_rules
    super
    per_page      = 450 # the same as the pagination done in macros
    @active_rules = @active_rules.paginate(page: get_page, per_page: per_page) if current_account.has_paginate_triggers_view?
  end

  def load_rules
    @rules = Zendesk::RuleSelection::ExecutionCounter.new(
      account: current_account,
      type:    :trigger
    ).rules_with_execution_counts(timeframe, current_account.triggers)
  end
end
