class Rules::AnalysisController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable

  before_action :authorize_admin
  before_action :authorize_feature

  caches_action :show, cache_path: proc { |controller| "#{controller.send(:current_account).scoped_cache_key(:rule_analysis)}/#{controller.params[:id]}/#{controller.params[:key]}/#{controller.params[:select]}/#{controller.params[:filter]}" }

  tab :manage

  layout 'agent'

  ssl_allowed :all

  allow_parameters :index, {}
  def index
    fetch_rule_stats
    @page_title = I18n.t('txt.admin.controllers.rules.analysis_controller.rule_analysis_title')
    @taggers = get_all_custom_field_options.map(&:value)
    @tag_items_without_conditions = @rule_stats.tag.select { |_k, v| v.condition_rules.any? && v.action_rules.empty? }.to_a
  end

  allow_parameters :show,
    filter: Parameters.string,
    id: Parameters.string,
    key: Parameters.string,
    select: Parameters.string | Parameters.nil_string
  def show
    params[:filter] = /inactive/.match?(params[:filter].to_s) ? "inactive" : "active"

    unless [:tag, :group_id, :assignee_id, :organization_id, :via_id, :macro_id, :notification_user, :notification_group, :notification_target, :recipient].member?(params[:id].to_sym)
      respond_to do |format|
        format.html { render plain: I18n.t('txt.admin.controllers.rules.analysis_controller.na_label') }
      end
      return
    end

    fetch_rule_stats

    @item_title = view_context.item_title(params[:id])

    @key_title = if current_account.has_use_get_for_rules_analysis_show?
      # Only existing keys are safe to show to the user
      if params[:key] && @rule_stats.send(params[:id])[params[:key]]
        view_context.key_title(params[:id], params[:key])
      else
        ""
      end
    else
      view_context.key_title(params[:id], params[:key])
    end

    if params[:id].to_s.casecmp('tag').zero? && params[:key].present?
      if custom_field_option = get_all_custom_field_options.detect { |c| c.value == params[:key].downcase }
        @tagger = custom_field_option.custom_field
      end
    end

    condition_ids, action_ids =
      if params[:key]
        if @key = @rule_stats.send(params[:id])[params[:key]]
          [@key.condition_rules, @key.action_rules]
        else
          [[], []]
        end
      else
        [@rule_stats.rules(params[:id], :condition_rules), @rule_stats.rules(params[:id], :action_rules)]
      end

    @rule_type = ['triggers', 'automations', 'macros', 'views'].member?(params[:select]) ? params[:select] : 'rules'
    rule_model = @rule_type.classify.constantize

    @condition_rules = rule_model.where(id: condition_ids, owner_type: ['Group', 'Account']).to_a
    @condition_rules = @condition_rules.select { |r| r.is_active? == (params[:filter] != 'inactive') }
    @action_rules    = rule_model.where(id: action_ids, owner_type: ['Group', 'Account']).to_a
    @action_rules    = @action_rules.select { |r| r.is_active? == (params[:filter] != 'inactive') }
  end

  private

  def fetch_rule_stats
    @rule_stats = Rails.cache.fetch("rule-stats-#{current_account.scoped_cache_key(:rule_analysis)}") do
      ::RuleStats.new(current_account)
    end
  end

  def get_all_custom_field_options # rubocop:disable Naming/AccessorMethodName
    # TODO: add checkbox fields here as well
    current_account.custom_field_options
  end

  def authorize_feature
    deny_access unless current_account.subscription.has_rule_analysis?
  end
end
