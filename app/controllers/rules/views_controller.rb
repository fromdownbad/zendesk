class Rules::ViewsController < RulesController
  include Zendesk::LotusEmbeddable
  include Zendesk::Rules::ControllerSupport::Personal

  skip_before_action :authorize_access_to_rule, only: [:search, :preview]
  before_action :set_output_type, only: [:edit]

  OCCAM_REQUEST_TIMEOUT = 30.seconds

  allow_parameters :show,
    id: Parameters.integer,
    group_id: Parameters.integer,
    order: Parameters.string,
    desc: Parameters.string,
    output_type: Parameters.string,
    page: Parameters.integer,
    is_public: Parameters.boolean
  def show
    rule
    set_source_page

    self.tab = :views if public?

    respond_to do |format|
      format.html do
        @tickets = rule.find_tickets(
          current_user,
          paginate: true, order: params[:order],
          desc: params[:desc], output_type: params[:output_type],
          page: get_page, caller: 'classic-api-v1',
          timeout: OCCAM_REQUEST_TIMEOUT
        )
        @rule_count = @tickets.total_entries

        if @tickets.out_of_range?
          redirect_to url_for(params.merge(page: @tickets.total_pages))
        end

        store_view_view(account_id: current_account.id, user_id: current_user.id, rule_id: rule.id)
      end

      format.rss do
        @page_title = rule.rendered_title.capitalize
        @tickets    = rule.find_tickets(current_user, updated_at: http_if_modified_since, caller: 'classic-api-v1')

        render action: 'show', formats: [:xml], layout: false
      end

      format.csv do
        output = Rails.cache.fetch("#{current_account.id}/rules/#{@rule.cache_key}/csv/#{get_page}/#{current_user.id}", expires_in: 10.minutes) do
          @tickets = rule.find_tickets(current_user, output_type: 'table', limit: Rule.csv_threshold, caller: 'classic-api-v1')
          @rule.tickets_to_csv(current_user, @tickets)
        end

        send_data(output, type: get_csv_content_type, filename: PermalinkFu.escape("#{@rule.rendered_title} #{Time.now.in_time_zone.to_s(:csv)}") + '.csv')
      end
    end
  end

  allow_parameters :preview,
    desc: Parameters.string,
    id: Parameters.bigid,
    user_widget: Parameters.anything,
    page: Parameters.integer,
    filter: Parameters.string,
    sets: Parameters.anything,
    order: Parameters.string,
    output: Parameters.map(
      group: Parameters.string,
      order_asc: Parameters.boolean,
      order: Parameters.string,
      type: Parameters.string,
      group_asc: Parameters.boolean,
      columns: Parameters.string
    ),
    return_to: Parameters.string,
    submit_type: Parameters.enum('delete', 'deactivate', 'activate', 'update'),
    rule_type: Parameters.string,
    rule: Parameters.map(
      title: Parameters.string,
      is_active: Parameters.boolean,
      owner_type: Parameters.string,
      owner_id: Parameters.string,
      type: Parameters.string,
      per_page: Parameters.integer
    )
  def preview
    @rule       ||= rule_klass.new(output: Output.create(order: :id, type: 'table', columns: [:description, :requester, :created, :status, :group, :assignee]))
    @rule.owner   = current_account
    @end_user_id = params[:user_widget].to_s.to_i if params[:user_widget]

    rule_preview = Zendesk::Rules::Preview.new(current_user, params, rule)

    @output ||= @rule.output

    @tickets = rule_preview.tickets
    @definition = @rule.definition

    if rule_preview.was_missing_required_condition?
      flash[:beware] = I18n.t('txt.admin.controllers.rules.views_controller.beware_status_less_than_solved')
    end

    respond_to do |format|
      format.html
      format.js do
        Comment.map_latest_comment_to_tickets(@tickets)
        render layout: false
      end
    end
  rescue Zendesk::Rules::Preview::ResultsOutOfRange
    redirect_to url_for(params.merge(page: rule_preview.tickets.total_pages))
  end

  protected

  def rule_klass
    params[:rule_type] == 'automation' ? ::Automation : ::View
  end

  def output
    Output.create(params[:output])
  end

  def public?
    params[:is_public] != 'false'
  end

  def build_from_params
    @rule        = current_account.views.build(params[:rule])
    @definition  = definition if conditions?
    @output      = Output.new
    @output.type = 'table'
  end

  def set_output_type
    rule.output.type = 'table'
  end

  def load_rules
    @rules = rule_collection.to_a
  end

  def conditions?
    definition_params[:conditions_all] || definition_params[:conditions_any]
  end

  def safe_request?
    # return_to_parser over escapes + and Base64 includes + so we need to handle this
    request.post? || form_authenticity_token == params[:authenticity_token].to_s.tr(' ', '+')
  end

  def warden_scope
    if action_name == 'show'
      :all
    end
  end
end
