class Rules::TicketsController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable

  before_action :authorize_admin

  allow_parameters :index,
    rule_id: Parameters.integer,
    page: Parameters.integer,
    group_id: Parameters.integer # from logs, untested & undocumented
  def index
    @rule = current_account.all_rules.find(params[:rule_id])
    @tickets = @rule.rule_ticket_joins.tickets(page: get_page)
    respond_to do |format|
      format.html do
        @output = Output.create(columns: [:nice_id, :status, :description, :requester, :group, :updated, :solved])
      end
      format.json { render json: @tickets }
    end
  end
end
