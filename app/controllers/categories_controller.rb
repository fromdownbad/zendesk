class CategoriesController < ApplicationController
  include ::AllowedParameters
  include HelpCenterRedirection

  allow_anonymous_users if: proc { |c| c.send(:current_account).has_public_forums? }

  skip_before_action :authenticate_user, if: :should_redirect_to_help_center?
  skip_before_action :set_x_xss_protection_header
  before_action :is_web_portal_disabled?

  slug_id = Parameters.bigid | Parameters.string

  allow_parameters :show, id: slug_id
  def show
    action = should_redirect_to_help_center?
    action &&= current_account.categories.find(params[:id])
    action &&= action.help_center_url

    perform_help_center_redirection(action)
  end
end
