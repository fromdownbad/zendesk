class LogosController < ApplicationController
  include ::AllowedParameters

  before_action :authorize_admin
  ssl_allowed :destroy

  allow_parameters :destroy,
    id: Parameters.bigid

  def destroy
    logo = Logo.where(id: params[:id], account_id: current_account.id).first
    logo.destroy unless logo.nil?

    head :ok
  end
end
