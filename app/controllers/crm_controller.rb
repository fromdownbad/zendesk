require_dependency 'zendesk/salesforce'
require_dependency 'zendesk/salesforce/integration'

class CrmController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_agent

  allow_parameters :sync_user_info, id: Parameters.bigid
  def sync_user_info
    user = current_account.users.find(params[:id])
    render_crm_data(user.crm_data(true), user)
  end

  # This method takes arbitrary dot-separated names representing nested
  # objects as keys. stronger_parameters doesn't have a way to handle that so
  # it's better to skip the check so no parameters are filtered out or permitted.
  # ticket_id is known and required, but can't be supplied in addition :skip.
  allow_parameters :sync_ticket_info, :skip
  def sync_ticket_info
    if ticket_id = params[:ticket_id]
      ticket = current_account.tickets.find_by_nice_id(ticket_id)
      render_crm_data(ticket.crm_data(true), ticket) unless ticket.nil?
    else
      # This is from the test widget
      begin
        crm_data = current_account.salesforce_integration.fetch_ticket_info(params)

        respond_to do |format|
          format.json do
            render json: { status: :ok, records: crm_data[:records] }
          end
        end
      rescue Salesforce::Integration::LoginFailed
        respond_to do |format|
          format.json { render json: { status: :errored } }
        end
      end
    end
  end

  private

  def render_crm_data(crm_data, object)
    if crm_data.nil?
      respond_to do |format|
        format.json { render json: {status: :errored} }
      end
    elsif crm_data.needs_sync?
      crm_data.start_sync(object)
      status = crm_data.sync_errored? ? :errored : :pending
      respond_to do |format|
        format.json { render json: {status: status} }
      end
    elsif crm_data.sync_pending?
      respond_to do |format|
        format.json { render json: {status: :pending} }
      end
    else
      respond_to do |format|
        format.json do
          render json: { status: :ok, records: crm_data.data[:records] }
        end
      end
    end
  end
end
