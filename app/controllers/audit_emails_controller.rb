class AuditEmailsController < ApplicationController
  include ::AllowedParameters

  before_action :authorize_agent
  before_action :ensure_audit_has_raw_email
  before_action :ensure_access_to_audit
  before_action :ensure_successful_x_header_scrubbing

  helper_method :raw_email
  layout false

  allow_parameters :show,
    id: Parameters.bigid,
    ticket_id: Parameters.bigid
  def show
    expires_in 60.minutes if current_account.has_email_audit_email_cache_control_header?

    if stale?(etag: ['audit_email', audit, request.format], last_modified: audit.created_at)
      respond_to do |format|
        format.json { render json: raw_email.json_without_x_headers }

        format.eml { send_data(raw_email.eml_without_x_headers, type: :eml, filename: raw_email.eml_file_name) }

        format.text { render plain: raw_email.eml_without_x_headers }

        format.html
      end
    end
  end

  private

  def audit
    return @audit if defined?(@audit)
    @audit ||= if params[:ticket_id]
      ticket = current_account.tickets.find_by_nice_id(params[:ticket_id])
      ticket && ticket.audits.find(params[:id].to_i)
    else
      Audit.where(account_id: current_account.id).find(params[:id])
    end
  end

  def ensure_access_to_audit
    unless current_user.can?(:view, audit.ticket)
      render_failure(
        status: :not_found,
        title: I18n.t("txt.errors.access_denied.title"),
        message: I18n.t("txt.errors.access_denied.message")
      )
    end
  end

  def ensure_audit_has_raw_email
    if raw_email.blank? || !raw_email.message || !audit.ticket
      render "not_found", status: :not_found
    end
  end

  def ensure_successful_x_header_scrubbing
    if eml_x_header_scrubbing_error? || json_x_header_scrubbing_error?
      render "not_found", status: :not_found
    end
  end

  def eml_x_header_scrubbing_error?
    raw_email.eml && !raw_email.eml_without_x_headers
  end

  def json_x_header_scrubbing_error?
    raw_email.json && !raw_email.json_without_x_headers
  end

  def raw_email
    audit.raw_email unless audit.nil?
  end
end
