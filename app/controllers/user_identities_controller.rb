class UserIdentitiesController < ApplicationController
  include Zendesk::Users::Identities::ControllerSupport

  IDENTITY_TYPES = ['email', 'facebook', 'google', 'manual_twitter', 'twitter_readonly'].freeze

  def index
    respond_to do |format|
      format.xml  { render xml: identities }
      format.json { render json: identities }
    end
  end

  def new
    state = Zendesk::Auth::Warden::OAuthStrategy.state(session, Zendesk::Configuration.fetch(:google)['key'])
    case params[:type]
    when 'twitter_readonly'
      @callback_url = successful_twitter_auth_user_url(user.id, state: state, anchor: 'identities')
    when 'facebook'
      @callback_url = build_callback_url_with(facebook_callback_path(state))
      params[:challenge] = user.challenge_tokens.lookup_by_ip_address(request.remote_ip).value
    end

    if Zendesk::Auth::OauthDelegator.valid_auth_type?(params[:type])
      @oauth_url = Zendesk::Auth::OauthDelegator.request_oauth!(params[:type], @callback_url)
    end

    respond_to do |format|
      format.html do
        if type = IDENTITY_TYPES.detect { |t| t == params[:type] }
          render action: "add_#{type}_identity", layout: false
        else
          head(:bad_request) unless params[:type] =~ /\A[_a-z\d]+\Z/
        end
      end
    end
  rescue Net::HTTPFatalError => e
    render_failure(status: e.response.code, message: I18n.t("txt.access.login.#{params[:type]}_error"))
  end

  def make_primary
    identity.make_primary!

    respond_to do |format|
      format.html { redirect_to edit_user_path(user) }
      format.xml  { head :ok }
      format.json { render json: {}, status: :ok }
    end
  end

  def create
    if new_identity.present?
      user.identities << new_identity
    else
      return head :not_acceptable
    end

    respond_to do |format|
      if user.save
        format.html do
          flash[:notice] = I18n.t("txt.controllers.user_identities.has_been_added_#{new_identity.identity_type}")
          redirect_to edit_user_path(user, anchor: 'identities')
        end
        format.xml  { head :created, location: user_user_identity_path(user, new_identity) }
        format.json { head :created, location: user_user_identity_path(user, new_identity) }
      else
        flash[:error] = flash_error(I18n.t("txt.controllers.user_identities.failed_to_add_#{new_identity.identity_type}"), user)
        format.html { redirect_to edit_user_path(user, anchor: 'identities') }
        format.xml  { render xml: user.errors, status: :not_acceptable }
        format.json { render json: user.errors, status: :not_acceptable }
      end
    end
  end

  def destroy
    if identity.destroy
      respond_to do |format|
        format.html do
          flash[:notice] = "The #{identity.identity_type.titleize} identitiy has been removed."
          redirect_to edit_user_path(user)
        end
        format.js   { head :ok }
        format.xml  { head :ok }
        format.json { render json: {}.to_json, status: :ok }
      end
    else
      respond_to do |format|
        flash[:error] = flash_error("Could not remove the #{identity.identity_type.titleize} identitiy.", identity)
        format.html do
          redirect_to edit_user_path(user)
        end
        format.js   { head :not_acceptable }
        format.xml  { render xml: user.errors, status: :not_acceptable }
        format.json { render json: user.errors, status: :not_acceptable }
      end
    end
  end

  private

  def build_callback_url_with(path)
    protocol = URI.parse(request.url).scheme + '://'
    "#{protocol}#{current_account.default_host}#{path}"
  end

  def facebook_callback_path(state)
    url_for(
      only_path: true,
      controller: "people/users",
      id: user.id,
      action: :successful_facebook_auth,
      challenge: user.challenge_tokens.lookup_by_ip_address(request.remote_ip).value,
      real_domain: request.url,
      state: state
    )
  end
end
