class ExpirableAttachmentsController < ApplicationController
  include ::AllowedParameters
  include AttachmentsControllerMixin

  before_action :authorize_agent

  allow_parameters :show,
    id: Parameters.string,
    name: Parameters.string
  def show
    token_or_id = params[:id]
    attachment = ExpirableAttachment.find_by_token_or_id_and_account_id!(token_or_id, current_account.id)
    check_access(attachment)
  end
  alias :download :show
  alias :token :show
  allow_parameters :download, allowed_parameters[:show]
  allow_parameters :token, allowed_parameters[:show]

  private

  def check_access(attachment)
    if !is_monitor_report?(attachment) && current_user.can?(:view, attachment)
      send_attachment_content(attachment)
    else
      render_failure(status: :unauthorized, title: I18n.t("txt.errors.access_denied.title"), message: I18n.t("txt.errors.access_denied.message"))
    end
  end

  def is_monitor_report?(attachment) # rubocop:disable Naming/PredicateName
    attachment.created_via == "ZendeskMonitor"
  end
end
