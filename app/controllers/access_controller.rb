require 'oauth/consumer'
require 'oauth2'

class AccessController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Auth::ControllerSupport
  include Zendesk::MobileSdk::ControllerSupport
  include Zendesk::CloudflareRateLimiting

  SSO_SAML     = 'saml'.freeze
  SSO_JWT      = 'jwt'.freeze

  before_action      :allow_only_mobile_sdk_clients_access, only: [:sdk_anonymous, :sdk_jwt]
  before_action      :redirect_logged_in_users, only: [:index, :dispatch_to_home]
  skip_before_action :authenticate_user
  skip_before_action :verify_authenticity_token, unless: :is_login_action?
  around_action      :preserve_mobile_preference, only: :logout
  skip_before_action :set_encoded_flash_error, only: :return_to
  skip_before_action :ensure_proper_protocol, only: [:dispatch_to_home, :return_to, :jwt, :one_time_password, :sso_bypass, :access_token]
  before_action      :authenticate!, only: :index # makes sure that remote auth users get redirected
  before_action      :setup_channels_callback_urls, only: :oauth
  skip_before_action :destroy_challenge_tokens, only: :return_to
  skip_before_action :ensure_support_is_active!
  skip_before_action :redirect_to_lotus, if: -> { multiproduct? || sell_only_user_redirect? }

  before_action      :require_authentication_domain, if: :action_is_valid?
  before_action      :require_authentication_domain_for_jwt, only: [:jwt]
  before_action      :instrument_normal_login, only: [:login, :normal_login], if: :request_from_normal_or_normal_login?
  before_action      :verify_2fa_in_progress, only: [:request_two_factor, :configure_two_factor]
  skip_before_action :set_x_frame_options_header, if: :csp_frame_ancestors_enabled?
  before_action      :set_csp_header_options, if: :csp_frame_ancestors_enabled?

  log_or_throttle_sdk_action_by_account [:sdk_anonymous], :mobile_sdk_limit_medium
  log_or_throttle_sdk_action_by_account [:sdk_jwt], :mobile_sdk_limit_medium

  layout :layout_for_role

  ssl_required :login, :normal_login, :oauth_mobile_login, :office_365, :google, :gam, :sdk_jwt, :google_play, :google_play_jwt, :sdk_anonymous, :token, :request_two_factor, :configure_two_factor, :normal
  ssl_allowed  :challenge, :logout, :unauthenticated, :return_to, :saml, :oauth, :jwt, :master, :sso
  allow_zendesk_mobile_app_access(only: [:oauth_mobile_login, :jwt, :saml, :sso, :sdk_jwt, :office_365, :google, :gam, :google_play, :google_play_jwt, :sdk_anonymous, :logout])

  helper Zendesk::Auth::V2::LoginsHelper

  helper_method :current_route, :current_brand, :current_route

  login_parameters = {
    user: Parameters.map(
      email: Parameters.string,
      password: Parameters.string
    ),
    return_to: Parameters.string,
    force_unaltered_return_to: Parameters.string,
    form_origin: Parameters.string,
    flash_digest: Parameters.string,
    profile: Parameters.string,
    error: Parameters.string,
    error_description: Parameters.string,
    utf8: Parameters.string,
    return_to_on_failure: Parameters.string,
    brand_id: Parameters.string,
    theme: Parameters.string,
    auth_origin: Parameters.string,
    challenge: Parameters.string,
    remember_me: Parameters.anything
  }

  allow_parameters :index,
    user: Parameters.map(
      name: Parameters.string,
      email: Parameters.string
    ),
    profile: Parameters.string,
    return_to: Parameters.string,
    theme: Parameters.string,
    challenge: Parameters.string,
    flash_digest: Parameters.string
  def index
    build_user_from_params

    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: {sso_action: 'signin'}, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'signin'} }
    end
  end

  allow_parameters :dispatch_to_home, :skip

  def dispatch_to_home
     # leverages generic Cloudflare rate limits. See https://github.com/zendesk/techmenu/blob/master/content/adrs/generic-cloudflare-rate-limit.md
    if help_center_home? && current_account.has_rate_limit_hc_redirects?
      rate_limit_header(:mild, simulate: !current_account.has_rate_limit_hc_redirects_challenge?)
    end

    if !multiproduct? && internal_help_center_root?
      response.headers["X-Accel-Redirect"] = "/hc?internal_root=true"
      head :ok
    else
      redirect_to account_home, status: :moved_permanently
    end
  end

  # this action is hit by warden e.g. after authenticate! failed
  allow_parameters :unauthenticated,
    user: Parameters.map(
      name: Parameters.string,
      email: Parameters.string
    ),
    return_to: Parameters.string,
    theme: Parameters.string,
    flash_digest: Parameters.string,
    email: Parameters.string, # From logging, untested
    password: Parameters.string, # From logging, untested
    client_id: Parameters.string # From logging, implicit via lib/zendesk/mobile_sdk/controller_support.rb
  def unauthenticated
    return redirect_to_mapped_host if host_is_incorrect?

    params.delete(:return_to) if params[:return_to] =~ /access\/logout/

    index
  end

  def sso_bypass_enabled?
    (current_account.has_access_token_sso_bypass? && current_account.settings.sso_bypass?) ||
    current_account.role_settings.agent_remote_bypass_allowed?
  end
  private :sso_bypass_enabled?

  def access_normal_disabled?
    if current_account.has_access_token_sso_bypass?
      true
    else
      !current_account.role_settings.agent_password_allowed? && !current_account.role_settings.end_user_password_allowed?
    end
  end
  private :access_normal_disabled?

  # Allow agents to type in: http://domain.zendesk.com/access/normal such that they can recover their
  # settings should they screw up the configuration of remote authentication
  allow_parameters :normal,
    return_to: Parameters.string
  def normal
    @user = current_account.users.new
    if access_normal_disabled?
      if sso_bypass_enabled?
        redirect_to action: :sso_bypass
      else
        render_404_page
      end
    else
      statsd_client.increment('access.normal')

      respond_to do |format|
        format.html { render 'auth/sso_v2_index', locals: {sso_action: 'normal'}, layout: 'help_center' }
        format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'normal'} }
      end
    end
  end

  allow_parameters :sso_bypass, {}
  def sso_bypass
    if sso_bypass_enabled?
      respond_to do |format|
        format.html { render 'auth/sso_v2_index', locals: {sso_action: 'access_token'}, layout: 'help_center' }
        format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'access_token'} }
      end
    else
      render_404_page
    end
  end

  allow_parameters :request_two_factor, {}
  def request_two_factor
    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: {sso_action: 'one_time_password'}, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'one_time_password'} }
    end
  end

  allow_parameters :configure_two_factor, {}
  def configure_two_factor
    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: {sso_action: 'configure_two_factor'}, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'configure_two_factor'} }
    end
  end

  allow_parameters :token,
    auth_origin: Parameters.string,
    brand_id: Parameters.string, # from logging
    email: Parameters.string,
    return_to_on_failure: Parameters.string # from logging
  def token
    if user && sso_bypass_enabled? && current_account.role_settings.sso_bypass_allowed_for_user?(user)
      token = user.otp_tokens.create!.value
      AccessMailer.deliver_token(user, token)
    end

    redirect_to zendesk_auth.success_access_token_v2_login_url(auth_origin: params[:auth_origin])
  end

  allow_parameters :one_time_password,
    theme: Parameters.string,
    return_to: Parameters.string
  def one_time_password
    respond_to do |format|
      format.html { render action: 'one_time_password', layout: 'help_center' }
      format.mobile_v2 { render action: 'one_time_password' }
    end
  end

  allow_parameters :login,
    {
      user: Parameters.map(
        email: Parameters.string,
        password: Parameters.string,
        remember_me: Parameters.boolean # From logging, untested
      )
    }.merge(login_parameters.except(:user))
  def login
    if request.get?
      perform_auth!
    else
      @user = current_account.users.new
      if HashParam.ish?(params[:user])
        @user.attributes = params[:user].slice(:email)
        @user.password   = params[:user][:password]
      end
      perform_auth!(:password)
    end
  end
  allow_parameters :normal_login, **login_parameters
  alias_method :normal_login, :login

  allow_parameters :access_token,
    return_to: Parameters.string,
    form_origin: Parameters.string,
    profile: Parameters.string,
    error: Parameters.string,
    error_description: Parameters.string,
    token: Parameters.string
  def access_token
    return head :ok if request.head?

    perform_auth!(:access_token)
  end

  allow_parameters :saml,
    {
      SAMLResponse: Parameters.string,
      RelayState: Parameters.string
    }.merge(login_parameters)
  def saml
    clear_session_if_authenticated

    if params[:return_to].blank? && params[:RelayState].present?
      warden.params[:return_to] = params[:return_to] = params[:RelayState]
      Rails.logger.info("Using RelayState to return_to #{params[:return_to].inspect}")
    end
    perform_auth!(:saml)
  end

  allow_parameters :jwt, {
      jwt: Parameters.string,
      code: Parameters.string, # From logging, untested
      client: Parameters.string, # From logging, untested
      domain: Parameters.string, # From logging, untested
      ticket: Parameters.string, # From logging, untested
      ticket_form_id: Parameters.bigid, # From logging, untested
      timestamp: Parameters.string, # From logging, untested
      locale_id: Parameters.bigid, # From logging, untested
      state: Parameters.string, # From logging, untested
      store: Parameters.string # From logging, untested
    }.merge(login_parameters)
  def jwt
    clear_session_if_authenticated
    perform_auth!(:jwt)
  end

  allow_parameters :sdk_jwt,
    {
      user: Parameters.map(
        email: Parameters.string,
        password: Parameters.string,
        token: Parameters.string # from logging, untested
      ),
      client_id: Parameters.string # From logging, implicit via lib/zendesk/mobile_sdk/controller_support.rb
    }.merge(login_parameters.except(:user))
  def sdk_jwt
    perform_auth_via_api!(:sdk_jwt)
  end

  allow_parameters :sdk_anonymous,
    {
      user: Parameters.map(
        email: Parameters.string,
        password: Parameters.string,
        sdk_guid: Parameters.string,
        name: Parameters.string,
        external_id: Parameters.string # from logging, untested except in zendesk_auth
      ),
      client_id: Parameters.string # From logging, implicit via lib/zendesk/mobile_sdk/controller_support.rb
    }.merge(login_parameters.except(:user))
  def sdk_anonymous
    perform_auth_via_api!(:sdk_anonymous)
  end

  allow_parameters :sso, **login_parameters
  def sso
    perform_auth!(:saml, :jwt)
  end

  allow_parameters :office_365,
    **login_parameters,
    id_token: Parameters.string, # From logging, untested
    state: Parameters.string # From logging, untested
  def office_365
    perform_auth!(:office)
  end

  allow_parameters :google,
    {
      code: Parameters.string, # From logging, untested
      google_domain: Parameters.string,
      state: Parameters.string # From logging, untested
    }.merge(login_parameters)
  def google
    perform_auth!(:google_o_auth2)

    if current_account.has_google_apps? && warden.winning_strategy
      session['google_token'] = warden.winning_strategy.google_token
    end
  end

  allow_parameters :gam,
    {
      denied: Parameters.boolean,
      token: Parameters.string
    }.merge(login_parameters)
  def gam
    # If user denied permissions, confirm that this user is an admin and invoked GoogleAppMarketController#enable_google_apps
    if params[:denied] && current_user.is_admin? && session['enabled_google_apps']
      current_account.disable_google_apps!
      redirect_to_return_to(params[:return_to])
      return
    end

    perform_auth!(:gam_o_auth2)

    token = if warden.winning_strategy.nil? && params[:token].present?
      Zendesk::Auth::Warden::GoogleAppMarketOAuth2Strategy.create_gam_token(params[:token])
    elsif warden.winning_strategy.present?
      warden.winning_strategy.gam_token
    end

    session['google_token'] = token.claims[:access_token] if token
  end

  allow_parameters :google_play, {
      email: Parameters.string,
      google_auth_token: Parameters.string
    }.merge(login_parameters)
  def google_play
    perform_auth_via_api!(:google_play)
  end

  allow_parameters :google_play_jwt, {
      email: Parameters.string,
      auth_token: Parameters.string,
      client_id: Parameters.string, # From logging, implicit via lib/zendesk/mobile_sdk/controller_support.rb
      device: Parameters.map(
        identifier: Parameters.string,
        name: Parameters.string
      ),
      native_mobile: Parameters.boolean
    }.merge(login_parameters)
  def google_play_jwt
    perform_auth_via_api!(:google_play_jwt)
  end

  allow_parameters :oauth,
    **login_parameters,
    real_domain: Parameters.string,
    callback_url: Parameters.string,
    code: Parameters.string, # From logging, untested
    state: Parameters.string, # From logging, untested
    error_code: Parameters.string, # From logging, untested
    error_reason: Parameters.string, # From logging, untested
    oauth_token: Parameters.string, # From logging, untested
    oauth_verifier: Parameters.string # From logging, untested
  def oauth
    perform_auth!(:twitter, :facebook)
  end

  allow_parameters :master, {
      token: Parameters.string,
      jwt: Parameters.string
    }.merge(login_parameters)
  def master
    perform_auth!(:master_token)
  end

  allow_parameters :return_to,
    **login_parameters,
    channel: Parameters.string # From logging, untested
  def return_to
    strategy = Warden::Strategies[:challenge_token].new(request.env)
    if strategy.valid?
      clear_session_and_logout(mark_unavailable_for_voice: false)
    else
      # the challenge token strategy isn't run when it isn't valid but still need to destroy the tokens
      destroy_challenge_tokens
    end

    perform_auth!(:challenge_token, flash: false)
    clear_session_and_logout unless logged_in?
  end

  allow_parameters :request_oauth,
    {
      iframe: Parameters.string,
      scheme: Parameters.string # From logging, untested
    }.merge(login_parameters)
  def request_oauth
    profile = %w[twitter twitter_readonly facebook].include?(params[:profile]) ? params[:profile] : 'twitter'

    callback_helper = Zendesk::Auth::OauthCallbackHelper.new(
      subdomain: current_account.subdomain,
      oauth_callback_path: oauth_callback_path
    )

    callback_url = Zendesk::Auth::OauthCallbackHelper::OAUTH_CALLBACK_URL

    Rails.logger.info "oauth: built url #{callback_url}"
    use_xauth = (profile == 'twitter_readonly') ? false : true
    authorize_url_options = params[:iframe] == 'true' ? { display: 'popup' } : {}
    state = callback_helper.state
    # Facebook uses Oauth2 and can pass the state with the generated authorize_url
    # Twitter is not using Oauth2 and needs to pass the state when getting the request_token
    if profile == 'facebook'
      state_param = { state: state.to_s }
      authorize_url_options.merge!(state_param)
    else
      callback_url = callback_helper.twitter_oauth_callback_url
    end
    # `request.env` is necessary for fb_graph_api_v9 Arturo lookup
    # remove after Arturo rollout is complete
    redirect_to Zendesk::Auth::OauthDelegator.request_oauth!(profile, callback_url, use_xauth, request.env, authorize_url_options)
  rescue NoMethodError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Diagnose auth error for: #{profile}, #{callback_url}, #{use_xauth}, #{authorize_url_options}", fingerprint: 'a4beaa4791fe3950dae5a20fa7e906e5040811bc')
    flash[:error] = I18n.t("txt.access.login.#{profile}_error")
    redirect_to action: :unauthenticated
  rescue Net::HTTPFatalError, OAuth::Unauthorized, Zendesk::Auth::InvalidProfileException, OAuth2::Error, Faraday::Error::TimeoutError
    flash[:error] = I18n.t("txt.access.login.#{profile}_error")
    redirect_to action: :unauthenticated
  end

  allow_parameters :signup, {}
  def signup
    redirect_to '/registration'
  end

  # Explicit logout. Removes the cookie and resets the session.
  allow_parameters :logout,
    return_to: Parameters.string,
    force_unaltered_return_to: Parameters.boolean,
    flash_digest: Parameters.string,
    brand_id: Parameters.string,
    SAMLRequest: Parameters.string,
    external_id: Parameters.bigid,
    email: Parameters.string
  def logout
    @auth_via_preserved = request.shared_session.dig(:auth_via)
    clear_session_and_logout
    respond_to do |format|
      format.html { redirect_after_logout }
      format.mobile_v2 { redirect_after_logout }
      format.json do
        render json: { success: true }, callback: params[:callback]
      end
      format.xml { head :ok }
    end
  end

  allow_parameters :oauth_mobile_login,
    client_id: Parameters.string, # From logging, implicit via lib/zendesk/mobile_sdk/controller_support.rb
    redirect_uri: Parameters.string,
    user: Parameters.map(
      email: Parameters.string,
      password: Parameters.string
    ),
    native_mobile: Parameters.boolean,
    device: Parameters.map(
      identifier: Parameters.string,
      name: Parameters.string
    )
  def oauth_mobile_login
    perform_auth_via_api!(:native_mobile)
  end

  protected

  def instrument_normal_login
    statsd_client.increment('access.normal_login')
  end

  def request_from_normal_or_normal_login?
    if action_name == 'normal_login'
      true
    else # are we coming from the new auth?
      return false unless request.referer.present?

      request.referer.include?("/auth/v2/login/normal")
    end
  end

  def action_is_valid?
    ['one_time_password', 'access_token'].include?(action_name)
  end

  def oauth_callback_url
    current_account.authentication_domain # construct a secure callback during auth (for Facebook)
  end

  def oauth_callback_path
    url_for(
      only_path: true,
      action: :oauth,
      profile: params[:profile],
      real_domain: params[:return_to], # TODO: see if this can be removed
      return_to: params[:return_to],
      state: Zendesk::Auth::Warden::OAuthStrategy.state(session, Zendesk::Configuration.fetch(:google)['key'])
    )
  end

  def sso_redirect_from_session
    auth_via = @auth_via_preserved
    if auth_via.nil?
      # fetch the first record based on priority
      current_account.remote_authentications.active.first
    else
      if auth_via.casecmp(SSO_JWT) == 0
        auth_via_mode = RemoteAuthentication::JWT
      elsif auth_via.casecmp(SSO_SAML) == 0
        auth_via_mode = RemoteAuthentication::SAML
      end
      current_account.remote_authentications.active.find { |a| a.auth_mode == auth_via_mode }
    end
  end

  def redirect_after_logout
    remote_params = {}
    return_to_parser.user = current_account.anonymous_user
    remote_authentication =
      if Arturo.feature_enabled_for?(:sso_logout_redirect_url_change, current_account)
        sso_redirect_from_session
      else
        # fetch the first record based on priority
        current_account.remote_authentications.active.first
      end
    if should_redirect_to_remote?(remote_authentication)
      remote_params[:external_id] = current_user.external_id || params[:external_id]
      remote_params[:email] =  current_user.email || params[:email]
      remote_params[:brand_id] = current_brand.id if current_brand
      remote_params[:return_to] = params[:return_to]
      remote_params[:kind] = 'info'
      remote_params[:message] = I18n.t('txt.access.logout.message')
      redirect_to Zendesk::RemoteAuthentications.logout_redirect(
        authentication: remote_authentication,
        request: request,
        user: current_user,
        remote_params: remote_params
      )

    elsif !redirect_to_return_to(nil, remote_params)
      regular_logout
    end
  end

  def regular_logout
    flash[:notice] = I18n.t('txt.access.logout.message') unless request.format == :mobile_v2
    dispatch_to_home
  end

  def redirect_logged_in_users
    warden.authenticate

    clear_current_user

    if current_user_exists?
      flash.keep

      redirect_to_return_to(account_home_or_lotus)
    end
  end

  private

  def should_redirect_to_remote?(remote_authentication)
    current_account.remote_authentication_available? &&
      remote_authentication.try(:remote_logout_url).present? &&
      current_user.login_allowed?(:remote)
  end

  def clear_session_if_authenticated
    clear_session_and_logout if current_user.id
  end

  def internal_help_center_root?
    Arturo.feature_enabled_for?(:internal_hc_redirect, current_account) && help_center_home?
  end

  def account_home_or_lotus
    uri = Addressable::URI.parse("/")
    if !multiproduct? && !request.is_mobile? && (lotus_url = Zendesk::LotusRedirector.redirect(current_account, current_user, uri)) && !sell_only_user_redirect?
      lotus_url
    else
      account_home
    end
  end

  def normalized_return_to(*args)
    url = super
    if %r{/agent(/|$)}.match?(url) # lotus cannot show flash and we do not want to see the flash when returning/iframing classic
      flash.delete(:notice)
      url.sub!(/&flash_digest=\w+/, "")
    end
    url
  end

  def restrict_to_change_password
    false
  end

  def authenticate!
    warden.authenticate!(stay_on_brand: true)
  end

  def perform_auth!(*args)
    options = args.extract_options!
    warden.authenticate!(*args)

    clear_current_user

    if warden.winning_strategy.nil?
      set_flash_notice unless options[:flash] == false
      redirect_to_return_to(account_home_or_lotus)
    elsif warden.winning_strategy.mobile_auth? && warden.winning_strategy.redirect_at_end?
      redirect_to warden.winning_strategy.redirect_url_with_access_token
    elsif warden.winning_strategy.redirect_at_end?
      if authentication.via == 'google_o_auth2' &&
        current_account.has_google_apps? && !current_user.is_end_user? &&
        current_user.email.ends_with?(current_account.route.gam_domain)

        # Add extra redirect to GAM universal nav link so we can request the full range of scopes
        # We can't request scopes earlier until we know the user is not an end-user
        # However, allow agents with gmail emails to continue to log in
        gam_params = {
          return_to: params[:return_to],
          login_hint: current_user.email
        }
        redirect_to Zendesk::Auth::Warden::GoogleAppMarketOAuth2Strategy.gam_initiation_url(gam_params)
        return
      end
      Rails.logger.info("[ACCESS LOGIN SUCCESS] #{success_tags(current_user)}")
      statsd_client.increment('access.login.success', tags: success_tags(current_user))

      set_flash_notice unless options[:flash] == false
      redirect_to_return_to(account_home_or_lotus)
    else
      render json: { access_token: warden.winning_strategy.access_token }
    end
  rescue OAuth::Unauthorized, OAuth2::Error, Net::HTTPFatalError, Timeout::Error, EOFError, Faraday::Error::TimeoutError, Faraday::Error::ConnectionFailed, Channels::Errors::RetryError => e
    channels_error(e)
  rescue Prop::RateLimited
    raise
  rescue StandardError => e
    unknown_error(e)
  end

  def success_tags(current_user)
    @success_tags ||= [
      "user_role:#{current_user.is_agent? ? 'agent' : 'end_user'}",
      "form_origin:#{params[:form_origin]}",
      "auth_flow_by_role:#{auth_flow_by_role_tag(current_user)}",
      "return_to_path:#{return_to_path_tag}",
      "controller_action:#{action_name}"
    ]
  end

  def auth_flow_by_role_tag(current_user)
    "#{current_user.is_agent? ? 'agent' : 'enduser'}_#{auth_flow_by_role_origin}"
  end

  def auth_flow_by_role_origin
    return params[:form_origin] if %w[agent hc].include?(params[:form_origin])
    return 'unknown' unless path = return_to_path

    if /^\/hc\//.match?(path)
      (/^\/hc\/start/.match?(path) ? 'agent' : 'hc')
    elsif /^#{Zendesk::Auth::RemoteRedirection::AGENT_LINK}/.match?(path)
      'agent'
    else
      'unknown'
    end
  end

  def return_to_path_tag
    path = return_to_path
    return 'other' unless path

    if /^(#{Zendesk::Auth::RemoteRedirection::AGENT_LINK}|\/hc)/.match?(path)
      path.split('/')[1]
    else
      'other'
    end
  end

  def return_to_path
    @return_to_path ||= begin
      URI.parse(params[:return_to]).path
    rescue URI::InvalidURIError
      nil
    end
  end

  def perform_auth_via_api!(*args)
    cancel_session
    warden.authenticate!(*args)

    respond_to do |format|
      format.json do
        render json: warden.winning_strategy.present
      end
    end
  rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation => e
    Rails.logger.error("KeyConstraintViolation: #{e.message}")
    render plain: "Database collision - please try again", status: :conflict
  rescue ActiveRecord::RecordInvalid => e
    Rails.logger.error("RecordInvalid: #{e.message}")
    render plain: e.message, status: :unprocessable_entity
  end

  def week_left_in_account_trial?
    current_account.subscription.try(:is_trial?) &&
      current_account.owner == current_user &&
      current_account.subscription.last_day_of_trial.to_date < 8.days.from_now.to_date
  end

  def setup_channels_callback_urls
    if ["facebook", "twitter_readonly"].include?(params[:profile])
      Rails.logger.info "logging in with profile: #{params[:profile]}"
      request.params[:return_to] = params[:return_to] = params[:real_domain]
      request.params[:callback_url] = params[:callback_url] = Zendesk::Auth::OauthCallbackHelper::OAUTH_CALLBACK_URL
      Rails.logger.info "access login: callback url set #{params[:callback_url]}"
    end
  end

  def set_trial_expiry_warning
    last_day_of_trial = ActionController::Base.helpers.distance_of_time_in_words(Time.now, current_account.subscription.last_day_of_trial)
    flash[:beware] = I18n.t('txt.admin.controllers.access_controller.zendesk_triak_expires_in', last_day_of_trial: last_day_of_trial) +
      "&nbsp;<a class='title' href='#{subscription_settings_path}'>#{I18n.t('txt.admin.controllers.access_controller.upgrade_your_zendesk_now')}</a>".html_safe
  end

  def set_mobile_notice
    flash[:notice] = if request.is_device?("iphone|iPhone|iPod|ipod")
      I18n.t('txt.access.login.ios_native_app_notification',
        open_link: "<a href='#openIOSApp'>#{I18n.t('txt.access.login.ios_native_app_notification_open_link')}</a>",
        download_link: "<a href='#getIOSApp'>#{I18n.t('txt.access.login.ios_native_app_notification_download_link')}</a>")
    elsif request.is_device?("android|Android")
      I18n.t('txt.access.login.android_native_app_notification',
        download_link: "<a href='#getAndroidApp'>#{I18n.t('txt.access.login.android_native_app_notification_download_link')}</a>")
    end
  end

  def set_flash_notice
    set_trial_expiry_warning if week_left_in_account_trial?
    if request.format == :mobile_v2
      set_mobile_notice if current_user.is_agent?
    end
  end

  def channels_error(e)
    Rails.logger.info "#{params[:profile]} login error: #{e.inspect}"
    error_type = params[:profile] == "facebook" ? params[:profile] : "twitter"
    flash[:error] = I18n.t("txt.access.login.#{error_type}_error_v2")
    redirect_to action: :unauthenticated
  end

  def unknown_error(e)
    ZendeskExceptions::Logger.record(e, location: self, message: "Unknown error during login", fingerprint: 'fb5e9190e1a9f9ae27239a9d3c4d458e489cd904')

    flash[:error] = if params[:error]
      "Authentication Error: #{params[:error]}: #{params[:error_description]}"
    else
      I18n.t("txt.access.login.unknown_error")
    end

    redirect_to action: :unauthenticated
  end

  def is_login_action? # rubocop:disable Naming/PredicateName
    ['login', 'normal_login'].include?(action_name)
  end

  def require_authentication_domain
    unless request.url.start_with?(current_account.authentication_domain)
      redirect_to url_for(params.merge(
        host: current_account.authentication_host,
        protocol: current_account.authentication_scheme
      ))
    end
  end

  def verify_2fa_in_progress
    if session[Zendesk::Auth::Otp::SESSION_USER_KEY].nil? || current_registered_user
      redirect_to account_home
    end
  end

  def require_authentication_domain_for_jwt
    if request.url.start_with?(current_account.authentication_domain)
      statsd_client.increment('jwt.subdomain')
      return
    end

    if current_account.has_grandfathered_hostmapped_jwt?
      statsd_client.increment('jwt.hostmapped.redirected')
      http = request.ssl?
      subdomain = request.host.ends_with?('.zendesk.com')
      Rails.logger.info "Account:#{current_account.id} grandfathered use of unwise JWT (http:#{http} subdomain:#{subdomain}) - redirecting to authentication domain"

      if session[:return_to].present? && params[:return_to].blank?
        hash = params.except(:controller, :action)
        hash[:return_to] = session[:return_to]
        redirect_path = request.path + "?#{hash.to_query}"
      else
        redirect_path = request.fullpath
      end

      redirect_to current_account.url(mapped: false, ssl: true) + redirect_path
    else
      statsd_client.increment('jwt.hostmapped.blocked')
      Rails.logger.info "blocking use of hostmapped jwt - use subdomain instead"
      render_failure(
        status: :forbidden, title: I18n.t('txt.access.login.jwt.invalid'),
        message: I18n.t('txt.access.login.jwt.invalid_domain_explanation', correct_domain: "#{current_account.authentication_domain}/access/jwt")
      )
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["auth", "return_to"])
  end

  def identity
    @identity ||= current_account.user_identities.email.find_by_value(params[:email])
  end

  def user
    @user ||= identity.try(:user)
  end

  def csp_frame_ancestors_enabled?
    Arturo.feature_enabled_for?(:access_controller_csp_frame_ancestors_enabled, current_account)
  end

  def set_csp_header_options
    hostmappings = current_account.brands.map(&:host_mapping).compact
    hostmappings = hostmappings.join(" ")
    headers["Content-Security-Policy"] = "frame-ancestors 'self' #{hostmappings};"
  end

  include MobileSessionPreservation
end
