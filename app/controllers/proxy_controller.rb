require 'faraday/restrict_ip_addresses'

class ProxyController < ApplicationController
  include ::AllowedParameters

  before_action :authorize_agent_or_end_user
  before_action :verify_csrf_token, only: [:direct]

  layout false
  ssl_allowed :direct, :index, :create, :update, :destroy

  allow_parameters :direct,
    url:         Parameters.string,
    contenttype: Parameters.string,
    SOAPAction:  Parameters.string,
    basic_user:  Parameters.string,
    basic_pass:  Parameters.string,
    cache_key:   Parameters.string,
    cache_for:   Parameters.integer,
    method:      Parameters.string,
    timeout:     Parameters.integer,
    body:        Parameters.anything

  # FIXME: this returns 200 on all kinds of error conditions ... should be 500/400
  def direct
    return render_404_page if current_account.has_sse_remove_proxy_direct_endpoint?

    url = URI.escape(params[:url] || "")

    if url.blank?
      render plain: "Missing mandatory URL parameter"
      return
    elsif !Zendesk::Net::AddressUtil.safe_url?(url)
      render plain: "Invalid URL parameter"
      return
    end

    headers = { 'Content-Type' => params[:contenttype] ? params[:contenttype] : 'text/xml' }
    soap_action = params[:SOAPAction] || request.headers['SOAPAction']
    headers['SOAPAction'] = soap_action if soap_action
    headers['User-Agent'] = "Zendesk"

    # pass on any auth headers sent in the original request.
    if request.headers['authorization']
      headers['authorization'] = request.headers['authorization']
    elsif params[:basic_user] && params[:basic_pass]
      headers['authorization'] = "Basic #{Base64.encode64("#{params[:basic_user]}:#{params[:basic_pass]}")}"
    end

    response = if params[:cache_key]
      Rails.cache.fetch("proxy-direct-#{current_account.id}/#{params[:cache_key]}/#{params[:method]}", expires_in: params[:cache_for].to_i.minutes) do
        uncached_http_request(url, headers)
      end
    else
      uncached_http_request(url, headers)
    end

    respond_to do |format|
      format.xml do
        data = if response && response.body.to_s.present?
          response.body.to_s.strip
        else
          { error: "Empty response" }.to_xml(root: "result")
        end

        render xml: data
      end
    end
  rescue URI::InvalidURIError, Net::HTTPBadResponse, Errno::EHOSTUNREACH => e
    Rails.logger.warn("Failed to connect to destination: #{e.message}")
    render plain: e.message
  rescue SocketError, Errno::ECONNREFUSED, Faraday::ConnectionFailed, Faraday::SSLError => e
    Rails.logger.warn("Connectivity error: #{e.message}")
    render plain: e.message
  rescue Faraday::Error::TimeoutError, Timeout::Error => e
    Rails.logger.error("Timeout while contacting URL #{url}")
    render plain: e.message
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Error connecting to #{url}: #{e.message}", fingerprint: '2ac2eb6b76614774ab639d77ec346529a1dc95ef')
    render plain: "Error connecting to #{url}"
  end

  private ##############################

  def verify_csrf_token
    unless request.headers['HTTP_X_CSRF_TOKEN'] && request.headers['HTTP_X_CSRF_TOKEN'] == form_authenticity_token
      Rails.logger.warn(
        "Proxy CSRF verification failed: '#{request.headers['HTTP_X_CSRF_TOKEN']}'=='#{form_authenticity_token}' " \
        "ACC: '#{current_account.subdomain}' REFERER: '#{request.headers['HTTP_REFERER']}'"
      )
      # check arturo feature and XHR header for now since proxy requests from HC not sending classic CSRF token and some apps from Lotus using $.getScript not sending CSRF token
      # once that all fixed, we can fully switch on CSRF token verification
      csrf_verification_enabled = Arturo.feature_enabled_for?(:proxy_v1_csrf_verification, current_account)
      if csrf_verification_enabled || request.headers['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'
        Rails.logger.warn(
          "Proxy Not Allowed: proxy_v1_csrf_verification feature: #{csrf_verification_enabled}, " \
          "HTTP_X_REQUESTED_WITH: '#{request.headers['HTTP_X_REQUESTED_WITH']}' " \
          "ACC: '#{current_account.subdomain}' REFERER: '#{request.headers['HTTP_REFERER']}'"
        )
        render status: :forbidden, text: 'Proxy Not Allowed'
      end
    end
  end

  # TODO: convert to Faraday
  def uncached_http_request(url, headers)
    timeout = (params[:timeout] || 5).to_i
    options = { headers: headers, timeout: timeout }

    if request.get?
      get(url, options)
    else
      post(url, options.merge(body: params[:body]))
    end
  end

  def get(url, opts = {})
    headers = opts.fetch(:headers)
    timeout = opts.fetch(:timeout)

    connection(timeout: timeout, headers: headers).get(url)
  end

  def post(url, opts = {})
    body    = opts.fetch(:body)
    headers = opts.fetch(:headers)
    timeout = opts.fetch(:timeout)

    connection(timeout: timeout, headers: headers).post(url) do |req|
      req.body = body.to_s
    end
  end

  def connection(opts = {})
    timeout = opts.fetch(:timeout, 5)
    headers = opts.fetch(:headers, [])

    @connection ||= Faraday.new(nil) do |builder|
      builder.use FaradayMiddleware::FollowRedirects, limit: 4
      builder.use Faraday::RestrictIPAddresses, allow_url: lambda { |url| Zendesk::Net::AddressUtil.safe_url?(url.to_s) }
      builder.adapter Faraday.default_adapter
      builder.headers                = headers
      builder.options[:timeout]      = timeout
      builder.options[:open_timeout] = timeout
    end
  end
end
