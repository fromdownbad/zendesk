class ZopimChatStartController < ApplicationController
  include ::AllowedParameters
  include ZopimChatRedirectionMixin

  before_action :authorize_agent

  allow_parameters :index, {}
  def index
    redirect_to redirection_url
  end

  private

  def redirection_url
    if current_account.has_voltron_chat?
      NEW_ZOPIM_START_PAGE
    elsif zopim_setup_required?(current_user)
      zopim_setup_url(current_user)
    else
      ZOPIM_STANDALONE_PAGE
    end
  end
end
