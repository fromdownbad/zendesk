require 'zendesk/tickets/merger'

class Tickets::MergeController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_agent
  before_action :find_sources, except: [:result]
  before_action :find_target, except: [:new, :result]
  before_action :validate_sources, except: [:result]
  before_action :validate_target, except: [:new, :result]

  layout false

  helper_method :is_bulk_merge?

  ssl_allowed :all

  allow_parameters :new,
    source_ids: Parameters.string, # Comma-joined list of IDs
    target_id: Parameters.string,
    background: Parameters.string,
    unchecked: Parameters.string # Comma-joined list of IDs
  def new
    @requested = @current_view = @recent = []
    if is_bulk_merge?
      nice_ids      = params[:unchecked].to_s.split(',')
      @current_view = current_account.tickets.for_user(current_user).working.where(nice_id: nice_ids).order("nice_id DESC").to_a if nice_ids.present?
    else
      requester_id = @sources.first.requester_id
      @requested   = current_account.tickets.for_user(current_user).working.where(requester_id: requester_id).order("nice_id DESC").limit(20).to_a
    end

    if current_account.is_collaboration_enabled? || current_account.has_comment_email_ccs_allowed_enabled?
      @recent = get_recent_for_merge
    end

    @sources.each do |source|
      @requested.delete(source)
      @recent.delete(source)
    end

    @requested.reject! { |ticket| ticket.shared_tickets.any? }
    @current_view.reject! { |ticket| ticket.shared_tickets.any? }
    @recent.reject! { |ticket| ticket.shared_tickets.any? }
  end

  allow_parameters :show,
    source_ids: Parameters.string,
    background: Parameters.string,
    target_id: Parameters.string
  def show
    @comment_can_be_public = Zendesk::CommentPublicity.merge_comment_can_be_public?(@target, @sources)
    if @sources.include? @target
      return show_error(::I18n.t("txt.admin.controllers.tickets.merge_controller.merge_conflict_error", ticket_id: @target.nice_id))
    end
    render
  end

  allow_parameters :create,
    background: Parameters.string,
    source_comment: Parameters.string,
    source_ids: Parameters.string,
    source_is_public: Parameters.string,
    target_comment: Parameters.string,
    target_id: Parameters.string,
    target_is_public: Parameters.string
  def create
    job_options = {
      account_id: current_account.id,
      user_id: current_user.id,
      target: @target.nice_id,
      sources: @sources.map(&:nice_id),
      target_comment: params[:target_comment],
      source_comment: params[:source_comment],
      target_is_public: params[:target_is_public] == '1',
      source_is_public: params[:source_is_public] == '1',
      target_href: ticket_path(@target),
      source_href: ticket_path(@sources.first)
    }

    if params[:background] == 'true'
      job_id = TicketMergeJob.enqueue(job_options)
      render json: { id: job_id, status_url: api_v2_job_status_url(job_id, format: "json") }
    else
      job = TicketMergeJob.perform_now(job_options)
      result(job.status, job_options[:target_href])
    end
  end

  allow_parameters :result,
    background: Parameters.string
  def result(status = nil, target_href = nil)
    unless status
      status = Resque::Plugins::Status::Hash.get(params[:background])
      options = status['options'].with_indifferent_access
      target_href = options['target_href']
    end

    results = status['results'].first.with_indifferent_access

    if results['success']
      flash[:notice] = results['message']
      redirect_to target_href
    else
      flash[:error] = results['message']
      redirect_to :back
    end
  end

  private

  def find_sources
    if params[:source_ids].blank?
      return show_error(::I18n.t("txt.error_message.tickets.merge_controller.no_source_provided"))
    end
    @source_ids = params[:source_ids].split(',')
    @sources = current_account.tickets.for_user(current_user).where(nice_id: @source_ids).includes(:requester).to_a
  end

  def find_target
    @target = current_account.tickets.for_user(current_user).not_closed.find_by_nice_id(params[:target_id], include: [:requester])
  end

  def validate_sources
    @sources.each do |ticket|
      unless current_user.can?(:merge, ticket)
        ticket_title = "<a class='title' href='#{ticket_path(ticket)}'>#{ticket.title(95)}</a>"
        return show_error(::I18n.t('txt.controllers.ticket.merge_controller.you_cant_merge_ticket', ticket_id: ticket.nice_id, ticket_title: ticket_title))
      end
    end

    # this covers the case where some of the sources can't be loaded with current_account.tickets.for_user(current_user)
    # because the user has no access to those tickets
    if @sources.size != @source_ids.size
      return show_error(::I18n.t("txt.error_message.tickets.merge_controller.cannot_merge_tickets_that_belong_to_another_user"))
    end
  end

  def validate_target
    if @target.nil? || (@target && @target.shared_tickets && @target.shared_tickets.any?)
      return show_error(::I18n.t('txt.error_message.tickets.merge_controller.you_are_unable_to_merge_into', ticket_id: params[:target_id].gsub(/[^0-9]/i, '')))
    end

    # when ccs are disabled the requester cannot be added as a cc onto the target, so only merges with the same requester are acceptable
    if Zendesk::Tickets::Merger.different_requesters_not_allowed?(current_account, @sources) && @sources.any? { |source| source.requester.id != @target.requester.id }
      error_message = ::I18n.t(
        'txt.error_message.tickets.merge_controller.cannot_merge_tickets_with_different_requesters',
        target_name: @target.requester.name,
        source_name: @sources.first.requester.name
      )

      if current_user.is_admin?
        error_message << "\s"
        error_message << ::I18n.t(
          'txt.error_message.tickets.merge_controller.cannot_merge_tickets_with_different_requesters_admin_addition',
          learn_more_link: ::I18n.t('txt.admin.views.settings.tickets._settings.ccs_and_followers.learn_more_link')
        )
      end

      show_error(error_message)
    end
  end

  def is_bulk_merge? # rubocop:disable Naming/PredicateName
    @sources.size > 1
  end

  def show_error(message)
    render partial: "error", locals: { message: message }
  end

  def get_recent_for_merge # rubocop:disable Naming/AccessorMethodName
    if recent_tickets.any?
      nice_ids = recent_tickets.map { |ticket| ticket[:nice_id] }
      current_account.tickets.for_user(current_user).working.where(nice_id: nice_ids).includes(:requester).to_a
    else
      []
    end
  end
end
