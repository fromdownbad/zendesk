class GettingStartedController < ApplicationController
  include Zendesk::Users::ControllerSupport

  include ::AllowedParameters

  protect_from_forgery only: :invite_agents

  before_action :authorize_admin, only: [:invite_agents]
  before_action :authorize_agent, only: [:test_ticket]
  before_action :check_addresses, only: [:invite_agents]

  ssl_allowed :all

  allow_parameters :invite_agents,
    data: Parameters.map(email: Parameters.string),
    email: Parameters.string,
    text: Parameters.string

  def invite_agents
    User.transaction do
      @addresses.each do |email|
        next if current_account.find_user_by_email(email)
        name = Zendesk::Mail::Address.new(address: email).name
        @user = current_account.users.new
        @user.account              = current_account
        @user.email                = email
        @user.name                 = name
        @user.roles                = Role::AGENT.id
        @user.restriction_id       = RoleRestrictionType.NONE
        @user.groups               = current_account.groups.map(&:id)
        @user.is_moderator         = true
        @user.send_verify_email    = true
        @user.verify_email_text    = params[:text] || agent_invitation_text(name)
        @user.verify_email_subject = agent_invitation_subject
        Zendesk::SupportUsers::User.new(@user).save!
      end
    end

    respond_to do |format|
      format.json { render json: {}, status: :created }
    end
  rescue ActiveRecord::RecordInvalid => e
    logger.warn("Failed to create agent in getting started: #{@user.errors.inspect}")
    respond_to do |format|
      format.json { render json: Api::V2::ErrorsPresenter.new.present(e.record), status: :unprocessable_entity }
    end
  end

  allow_parameters :test_ticket, {}

  def test_ticket
    ticket = Ticket.create_test_ticket!(current_account, current_user)
    respond_to do |format|
      format.json do
        render json: { ticket_url: "/tickets/#{ticket.nice_id}" },
               status: :created
      end
    end
  rescue StandardError => e
    logger.warn("Failed to create test ticket in account #{current_account.id}: #{e.message}")
    error("Failed to create test ticket. Please try again or contact our support team")
  end

  private

  def check_addresses
    email = params[:email] || (params[:data] && params[:data][:email])
    @addresses = email.to_s.strip.split(/[\s,;]+/)
    if @addresses.empty? || @addresses.find { |address| !address.match(EMAIL_PATTERN) }
      respond_to do |format|
        format.json { render json: {}, status: :unprocessable_entity }
      end
    end
  end

  def error(message)
    render plain: message, status: :not_acceptable
  end

  def agent_invitation_text(_agent_name)
    I18n.with_locale(current_user.account.translation_locale) do
      <<-TEXT
      #{I18n.t('txt.email.controllers.getting_started_controllers.agent_invitation_text.has_just_created_an_account_for_you', current_user_name: current_user.name)}

      #{I18n.t('txt.email.controllers.getting_started_controllers.agent_invitation_text.to_choose_a_password_follow_this')}
      TEXT
    end
  end

  def agent_invitation_subject
    I18n.with_locale(current_user.account.translation_locale) do
      I18n.t('txt.email.controllers.getting_started_controllers.agent_invitation_text.subject', current_user_name: current_user.name)
    end
  end
end
