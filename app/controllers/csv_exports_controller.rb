require 'zendesk_voyager_api_client'

class CsvExportsController < ApplicationController
  include ::AllowedParameters
  include ActionView::Helpers::NumberHelper

  before_action :authorize_report_manager
  skip_around_action :localize_user

  ssl_allowed :all

  allow_parameters :search_stats,
    start: Parameters.integer,
    end: Parameters.integer
  def search_stats
    results = stats(query_options.merge!(include: :entry))
    csv = generate_csv(results, ["search string", "# of searches", "average # results", "Clickthrough rate (CTR) %", "tickets created", "Top clicked result"]) do |stat|
      top_entry   = nil
      searches    = number_with_delimiter(stat[:searches])
      avg_results = number_with_precision(stat[:avg_results], precision: 1, delimiter: ",")
      ctr         = stat[:ctr]
      row = [stat[:string], searches, avg_results, ctr, stat[:tickets], top_entry]
      row.map { |value| Zendesk::CsvScrubber.scrub_output(value) }
    end

    send_data(csv, type: get_csv_content_type, filename: "account_search_stats.csv")
  end

  allow_parameters :hc_search_stats,
    brand_id: Parameters.bigid,
    multibrand: Parameters.boolean,
    statistic: Parameters.string,
    start: Parameters.integer,
    end: Parameters.integer,
    interval: Parameters.integer
  def hc_search_stats
    results = { data: filter_and_order_stats(hc_stats(query_options)).all }
    csv = generate_csv(results, ["Search string", "Total searches", "Avg number of results", "Click-through rate", "Tickets created", "Top clicked result"]) do |stat|
      searches    = number_with_delimiter(stat[:searches])
      avg_results = number_with_precision(stat[:avg_results], precision: 1, delimiter: ",")
      ctr         = stat[:ctr]
      endpoint    =
        case stat[:top_child_type]
        when 'Article' then "/articles/"
        when 'Question' then "/communities/public/questions/"
        when 'CommunityPost' then "/communities/public/posts/"
        end
      top_entry = "#{request.protocol}#{current_account.host_name(mapped: false)}/hc#{endpoint}#{stat[:top_child_id]}" unless endpoint.nil?
      row = [stat[:string], searches, avg_results, ctr, stat[:tickets], top_entry]
      row.map { |value| Zendesk::CsvScrubber.scrub_output(value) }
    end

    send_data(csv, type: get_csv_content_type, filename: "search-strings-#{Time.now.strftime("%Y-%m-%d")}.csv")
  end

  protected

  def start_time
    (params[:start] && Time.at(params[:start].to_i)) || 30.days.ago
  end

  def end_time
    (params[:end] && Time.at(params[:end].to_i))
  end

  def filter_and_order_stats(scope)
    case params[:statistic]
    when "no_results" then scope.having("avg_results = 0").order("searches desc")
    when "no_clicks"  then scope.having("ctr = 0").order("searches desc")
    when "tickets"    then scope.having("tickets > 0").order("tickets desc")
    else                   scope.order("searches desc")
    end
  end

  def query_options
    options = {
        limit:      1000,
        start:      start_time,
        conditions: { origin: "all" }
    }
    options[:end] = end_time if end_time
    options
  end

  def authorize_report_manager
    grant_access csv_export_policy.accessible_to_user?
  end

  def csv_export_policy
    ImportExportJobPolicy.new(current_account, current_user)
  end

  def stats(query)
    { data: current_account.search_stats.top_n(query) }
  end

  def hc_stats(query)
    hc_search_stats_relation.top_n(query)
  end

  def generate_csv(results, column_headers)
    CSV.generate(force_quotes: true) do |csv|
      csv << column_headers
      results[:data].each do |stat|
        csv << yield(stat)
      end
    end
  end

  private

  def hc_search_stats_relation
    if multibrand_request? && brand_id.blank?
      current_account.hc_search_stats
    else
      StatRollup::HCSearchStringStat.where(help_center_id: help_center_id)
    end
  end

  def multibrand_request?
    ['1', 't', 'T', 'true', 'TRUE', 'on', 'ON'].include?(params[:multibrand].to_s)
  end

  def help_center_id
    @help_center_id ||= begin
      if multibrand_request? && brand_id.present?
        brand = current_account.brands.find(brand_id)
        brand && brand.help_center_id || raise(NoHelpCenterException)
      else
        current_brand.help_center_id || raise(NoHelpCenterException)
      end
    end
  end

  def brand_id
    params[:brand_id]
  end
end
