class CertificateSigningRequestController < ApplicationController
  include ::AllowedParameters
  ssl_allowed :all
  before_action :authorize_admin

  allow_parameters :generate, {}
  def generate
    no_host_mapping = if current_account.has_multibrand?
      current_account.brand_host_mappings.blank?
    else
      current_account.host_mapping.blank?
    end

    if no_host_mapping
      flash[:error] = I18n.t('txt.admin.controllers.settings.security_controller.you_must_have_host_mapping_defined')
      redirect_to(action: 'show', controller: '/settings/security', anchor: 'ssl') && return
    end

    cert = current_account.certificates.temporary.last

    # if CSR subject has changed, regenerate CSR rather than downloading saved one
    if cert && (cert.csr_object.subject.to_s != cert.csr_subject || !cert.csr_san_includes_brands?)
      cert.delete
      cert = nil
    end

    unless cert
      cert = current_account.certificates.new

      if !cert.generate_temporary || !cert.save
        cert = nil
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.security_controller.there_was_an_error_creating_your_request'))

        redirect_to(action: 'show', controller: '/settings/security', anchor: 'ssl') && return
      end
    end

    # application/x-x509-ca-cert
    send_data(cert.csr, type: "text/plain", filename: "#{current_account.subdomain}.csr")
  end

  allow_parameters :cancel, {}
  def cancel
    cert = current_account.certificates.pending.last
    return unless cert # cancel_pending only enabled if exists pending cert

    cert.do_cancel_pending && cert.save!

    flash[:notice] = I18n.t('txt.admin.controllers.settings.security_controller.pending_certificate_cancelled')

    redirect_to action: 'show', controller: '/settings/security', anchor: 'ssl'
  end
end
