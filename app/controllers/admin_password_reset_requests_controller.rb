class AdminPasswordResetRequestsController < ApplicationController
  before_action :find_user, only: :create
  before_action :ensure_can_reset_password, only: :create
  include ::AllowedParameters

  allow_parameters :create,
    user_id: Parameters.bigid
  def create
    user.current_user = current_user
    user.randomize_password

    if user.save && user.email

      token = if current_account.has_generate_password_reset_token_subclass?
        user.password_reset_tokens.create
      else
        user.verification_tokens.create
      end
      # We need to move the token generation after the password change
      # since we delete all tokens after crypted password changes.

      UsersMailer.deliver_admin_password_reset(user, token.value)
      flash[:notice] = I18n.t('txt.admin.controllers.admin_password_reset_request_controller.password_reset_mail_sent_to_user')
    elsif user.email.blank?
      flash[:error] = I18n.t('txt.admin.controllers.admin_password_reset_request_controller.password_reset_mail_not_sent')
    else
      flash[:error] = I18n.t('txt.admin.controllers.admin_password_error_modifying_password')
    end

    redirect_to user
  end

  private

  def user
    @user ||= current_account.users.find(params[:user_id])
  end
  alias :find_user :user

  def ensure_can_reset_password
    unless current_user.can?(:request_password_reset, user)
      if user
        Rails.logger.info(
          "Preventing password reset email for " \
          "account_id: #{user.account_id}, " \
          "user_id: #{user.id}, " \
          "email: #{user.email.inspect}, " \
          "reason: the current_user (#{current_user.id}) is not capable of " \
          "requesting a password reset for user (#{user.id})."
        )
      end

      deny_access
    end
  end
end
