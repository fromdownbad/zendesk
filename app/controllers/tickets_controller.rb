require 'vpim/icalendar'
require 'date'

class TicketsController < ApplicationController
  include ::AllowedParameters

  include Zendesk::ReturnTo::Helper
  include Zendesk::PrivateCaching
  include Zendesk::MobileDeeplink::ControllerMixin

  allow_deeplink only: [:show], if: :show_deeplinking?, params: :deeplink_params
  skip_before_action :authenticate_user, if: :show_deeplinking?

  before_action :show_landing_page_for_mobile, unless: :show_deeplinking?
  before_action :redirect_end_users_to_requests_controller, only: [:new, :index, :show]
  before_action :authorize_agent

  include Zendesk::Tickets::ControllerSupport
  before_action :set_sharing_agreement_ids, only: [:show]

  rescue_from ActiveRecord::RecordNotFound do
    render_failure(status: :not_found, title: I18n.t('txt.admin.controllers.tickets_controller.ticket_not_found'), message: I18n.t('txt.admin.controllers.tickets_controller.ticket_either_deleted_or_none_existent', ticket_id: params[:id].to_i))
  end

  rescue_from Access::Denied do |e|
    message = I18n.t('txt.admin.controllers.tickets_controller.you_do_not_have_access_to_ticket', id: e.subject.nice_id)
    render_failure(status: :forbidden, title: I18n.t('txt.admin.controllers.tickets_controller.access_denied'), message: message)
  end

  MAX_BULK_RESULTS = 5

  layout 'agent'

  only_use_private_caches :show

  ssl_allowed :print

  ticket_params = {
    account_id: Parameters.bigid,
    additional_tags: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil, # Untested, from logs, staging failure
    collaborators: Parameters.array(Parameters.bigid) | Parameters.nil,
    description: Parameters.string,
    entry_id: Parameters.bigid,
    fields: Parameters.map,
    group_id: Parameters.integer, # Failure in staging passed -1
    priority_id: Parameters.integer, # PriorityType
    requester_id: Parameters.bigid,
    set_collaborators: Parameters.array(Parameters.bigid) | Parameters.nil,
    set_tags: Parameters.string,
    status_id: Parameters.integer, # StatusType
    subject: Parameters.string,
    ticket_field_entries: Parameters.array(
      Parameters.map(
        ticket_field_id: Parameters.bigid,
        value: Parameters.string
      )
    ) | Parameters.nil,
    uploads: Parameters.string,
    via_followup_source_id: Parameters.bigid
    }

  comment_params = {
    author_id: Parameters.bigid,
    is_public: Parameters.boolean,
    value: Parameters.string
  }

  # Untested and inherited
  allow_parameters :edit, id: Parameters.bigid

  allow_parameters :index,
    flash_digest: Parameters.string, # from logs, untested and undocumented
    id: Parameters.bigid
    # the following parameters were found in logs and intentionally ignored
    # -s, destination, FollowSite, SiteName, goto, header, redirect, ReturnUrl, returnUrl, target, url, whs, whscheck

  def index
    view          = current_user.shared_views.select(&:is_active?).first || current_user.personal_views.select(&:is_active?).first
    redirect_path = view.present? ? rule_path(view) : new_rule_path(filter: "view")

    redirect_to redirect_path
  end

  allow_parameters :new,
    id: Parameters.bigid,
    ticket: Parameters.map(ticket_params),
    comment: comment_params
    # __utma, __utmb, __utmc, __utmk, __utmv, __utmx, and __utmz parameters are intentionally ignored
  def new
    redirect_to controller: 'requests/portal', action: params[:action] unless current_user.is_end_user?
  end

  allow_parameters :show, id: Parameters.string # based on logs, this can be larger than the ubigid constraint
  def show
    @ticket = requested_ticket
    @comment = @ticket.comment
    @ticket.current_user = current_user

    respond_to do |format|
      if Arturo.feature_enabled_for?(:deprecate_tickets_controller_show, current_account)
        format.html { head :gone }
        format.mobile_v2 { head :gone }
      else
        format.html { initialize_show }
        format.mobile_v2 { initialize_show }
      end
      format.rss  { render action: 'show', formats: [:xml], layout: false }
      format.ics  { render_calendar_event(@ticket) }
    end
    update_recent_tickets
  end

  alias :edit :show

  allow_parameters :bulk,
    comment: comment_params,
    quiet: Parameters.boolean,
    macro_applied: Parameters.string,
    source_page: Parameters.string,
    submit_type: Parameters.string,
    ticket: Parameters.map(ticket_params),
    tickets_to_bulk_update: Parameters.string
  def bulk
    params[:ticket] = change_no_change_string_to_key(params[:ticket], get_no_change_field_translated) if params[:ticket] && params[:ticket][:fields]
    ticket_ids = (params[:tickets_to_bulk_update] || '').split(',').compact.uniq
    job_options = params.merge(
      ticket_ids: ticket_ids,
      account_id: current_account.id,
      user_id: current_user.id,
      via_id: via_id_from_request,
      request: {user_agent: request.user_agent, remote_ip: request.remote_ip}
    )

    job_options[:macro_ids] ||= params[:macro_applied].present? ? Array(params[:macro_applied]) : []

    job_options[:macro_ids].each do |macro_id|
      store_bulk_macro_usage rule_id: macro_id.to_i, user_id: current_user.id, ticket_ids: ticket_ids.map(&:to_i)
    end

    if params[:background] == 'true'
      job_id = TicketBulkUpdateJob.enqueue(job_options)
      render json: { id: job_id, status_url: api_v2_job_status_url(job_id, format: "json") }
    else
      job = TicketBulkUpdateJob.perform_now(job_options)
      bulk_result(job.status['results'])
    end
  end

  allow_parameters :bulk_result, background: Parameters.string
  def bulk_result(results = nil)
    unless results
      status = Resque::Plugins::Status::Hash.get(params[:background]) || raise(ActiveRecord::RecordNotFound)
      results = status['results']
    end
    notice = I18n.t('txt.public.javascripts.views.rules._bulk_update.tickets_processed', count: results.length)
    notice << render_summary(results.map(&:with_indifferent_access))
    flash[:notice] = notice
    session_redirect
  end

  allow_parameters :print,
    id: Parameters.bigid,
    events: Parameters.anything, # Untested
    magic_readonly_print_key: Parameters.string
  def print
    @ticket = current_account.tickets.find_by_nice_id(params[:id])
    @events = params[:events]
    render partial: "print"
  end

  ############################################################################
  private

  def get_no_change_field_translated # rubocop:disable Naming/AccessorMethodName
    no_change_locale = is_assuming_user? ? I18n.translation_locale : current_user.translation_locale
    no_change = I18n.t(NO_CHANGE, locale: no_change_locale)
    logger.info("no_change using #{no_change_locale} -> '#{no_change}'")
    no_change
  end

  def change_no_change_string_to_key(params_of_ticket, no_change_value)
    params_of_ticket[:fields].each do |key, value|
      if value == no_change_value
        params_of_ticket[:fields][key] = NO_CHANGE.dup
      end
    end
    params_of_ticket
  end

  def set_sharing_agreement_ids
    # For checking if selected sharing agreement is with JIRA
    # to display the proper form elements
    @jira_sharing_agreement_ids = current_account.sharing_agreements_with(:jira).map(&:id)
  end

  def initialize_show
    @comment = Comment.new(
      is_public: comment_is_public_by_default?,
      channel_back: true
    )
  end

  def comment_is_public_by_default?
    return false if @ticket.entry.present?
    return false unless @ticket.allows_comment_visibility_toggle?

    current_account.is_comment_public_by_default?
  end

  def session_redirect
    redirect_to_return_to(session[:source_page] || '/tickets')
  end

  def render_summary(summary)
    t = ''
    limited_summary = summary[0..(MAX_BULK_RESULTS - 1)]

    limited_summary.each do |message|
      t << '<li>'
      t <<
        if !message[:success]
          if message[:status].casecmp("updated").zero?
            I18n.t('txt.controllers.ticket_controllers.tickets_not_updated') # Since we don't know all the possible status, this is the safe way to go
          else
            "<span class='attention loud'>Not</span> #{message[:status].downcase} "
          end
        else
          if message[:status] == "Updated"
            I18n.t('txt.controllers.ticket_controllers.updated_ticket_with_number_and_title', ticket_number: message[:id])
          elsif message[:status] == "Deleted"
            I18n.t('txt.controllers.ticket_controllers.deleted_ticket_with_number_and_title', ticket_number: message[:id])
          else
            "#{message[:status]} ##{message[:id]}"
          end
        end
      if message[:action] != :delete
        t << " <a class='title' href='/tickets/#{message[:id]}'>#{ERB::Util.h(message[:title])}</a>"
      end
      t << " <em>(#{ERB::Util.h(message[:errors])})</em>" unless message[:success]
      t << '</li>'
    end

    if summary.size > MAX_BULK_RESULTS
      t << "<li class=\"more\">#{I18n.t('txt.controllers.ticket_controllers.and_number_more', count: summary.size - MAX_BULK_RESULTS)}</li>"
    end

    "<ul>#{t}</ul>".html_safe
  end

  def render_calendar_event(ticket)
    cal = Vpim::Icalendar.create2

    ticket.due_date = Date.today.advance(days: 7) unless ticket.due_date

    cal.add_event do |e|
      e.dtstart     Time.local(ticket.due_date.year, ticket.due_date.month, ticket.due_date.day, 10, 0).to_date
      e.dtend       Time.local(ticket.due_date.year, ticket.due_date.month, ticket.due_date.day, 11, 0).to_date
      e.summary     "#{current_account.name}: #{I18n.t('txt.controllers.ticket_controllers.task_ticket_due', ticket_id: ticket.nice_id)} - \"#{CGI.escapeHTML(ticket.title(50))}\""
      e.url         "#{current_url_provider.url(mapped: false, protocol: 'http')}/tickets/#{ticket.nice_id}"
      e.description "#{I18n.t('txt.controllers.ticket_controllers.task_due_on_this_date', ticket_id: ticket.nice_id)}\n#{ticket.description.delete "\r"}"
    end

    encoded = cal.encode
    if request.user_agent.to_s.downcase.include? 'windows'
      # Make it compatible with Outlook
      encoded.gsub!('VERSION:2.0', 'VERSION:1.0')
    end

    send_data encoded, filename: "#{ticket.nice_id}.ics"
  end

  def redirect_end_users_to_requests_controller
    if current_user.is_end_user?
      if params[:action] == 'show' && params[:id]
        ticket = current_account.tickets.find_by_nice_id(params[:id]) || (raise ActiveRecord::RecordNotFound)
        redirect_to ticket.url_path(user: current_user)
      else
        redirect_to controller: 'requests/portal', action: params[:action]
      end

      false
    end
  end

  def via_id_from_request
    if via_web_service?        then ViaType.WEB_SERVICE
    elsif via_followup_source? then ViaType.CLOSED_TICKET
    else
      ViaType.WEB_FORM
    end
  end

  def via_web_service?
    request.format == Mime[:xml] || request.format == Mime[:json]
  end

  def via_followup_source?
    params[:ticket].present? && params[:ticket][:via_followup_source_id].present?
  end

  def show_landing_page_for_mobile
    conditions = [
      request.is_mobile?,
      request.format == :mobile_v2,
      ((request.referer || "/") == "/" || request.referer.match(/access\/unauthenticated/) || request.referer.match(/tickets/)),
      current_user.is_agent?
    ]
    if conditions.all?
      redirect_to mobile_landing_page_path(params: { return_to: request.url, ticket_id: params[:id] })
    elsif request.is_mobile? && request.format == :mobile_v2
      redirect_options = {
        controller: 'requests/portal',
        action: params[:action]
      }
      redirect_options[:id] = params[:id] if params[:id]

      redirect_to redirect_options
    end
  end

  def show_deeplinking?
    show_deeplinking_splash? && deeplinking_enabled?
  end

  def deeplinking_enabled?
    (current_account.has_ios_mobile_deeplinking? && is_ios?) ||
      (current_account.has_android_mobile_deeplinking? && is_android?) ||
      (current_account.has_windows_mobile_deeplinking? && is_windows_phone?)
  end

  def deeplink_params
    {ticket_id: params[:id]}
  end
end
