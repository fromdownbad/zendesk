# Controller for managing I/O from embedded JavaScript forms. Speaks only JSON.
class Requests::EmbeddedController < Requests::BaseController
  include Zendesk::Stats::ControllerHelper
  include ZendeskCrossOrigin::Helper
  include Zendesk::Tickets::Anonymous::ControllerSupport

  skip_before_action :authenticate_user
  skip_before_action :verify_authenticity_token

  before_action      :ensure_open_account
  before_action      :validate_input_parameters, only: :create

  ssl_allowed :create
  allow_cors :create

  deprecate_controller with_feature: :deprecate_embedded_requests

  def self.check_spam?
    true
  end

  allow_parameters :index, {}
  def index
  end

  allow_parameters :create,
    email: Parameters.string,
    subject: Parameters.string,
    description: Parameters.string,
    submitted_from: Parameters.string,
    priority_id: Parameters.integer,
    fields: Parameters.map,
    set_tags: Parameters.string | Parameters.array(Parameters.string),
    ticket: {
      email: Parameters.string,
      subject: Parameters.string,
      description: Parameters.string,
      submitted_from: Parameters.string,
      priority_id: Parameters.integer,
      fields: Parameters.map,
      set_tags: Parameters.string,
      locale_id: Parameters.integer, # From logging, untested
      via_id: Parameters.integer, # From logging, untested
      name: Parameters.string # From logging, untested
    },
    locale_id: Parameters.integer,
    recaptcha_response_field: Parameters.string,
    "g-recaptcha-response": Parameters.string,
    name: Parameters.string,
    via_id: Parameters.string | Parameters.integer,
    ticket_type_id: Parameters.integer,
    client: Parameters.string, # From logging, untested
    ticket_from_search: Parameters.string, # From logging, untested
    submit: Parameters.string, # From logging, untested
    page: Parameters.integer # untested, used in Stats::ControllerHelper
  def create
    verification_missing = current_account.is_signup_required? && !@inbound.author.is_verified?
    if verification_missing || !current_account.allows?(@inbound.author.email)
      ticket = @inbound.to_suspended_ticket
      if current_account.suspended_tickets << ticket
        if ticket.cause == SuspensionType.SIGNUP_REQUIRED
          return send_message(message: "Your request has been created. Please note that you must verify your email address. Check your email.", status: :accepted)
        else
          return send_message(message: "Requests from your domain are suspended.", status: :accepted)
        end
      else
        return send_message(error: "Could not save your request: #{ticket.errors.full_messages.join(', ')}", status: :unprocessable_entity)
      end
    else
      ticket = @inbound.to_ticket
      ticket.will_be_saved_by(@inbound.author)

      if ticket.save
        store_search_ticket_stat(ticket_id: ticket.id, origin: 'dropbox')
        return send_message(message: "Request ##{ticket.nice_id} \"#{ERB::Util.h(ticket.title)}\" created", status: :created)
      else
        return send_message(error: "Could not save your request: #{ticket.errors.full_messages.join(', ')}", status: :unprocessable_entity)
      end
    end
  end

  protected

  def send_message(args)
    status = args.delete(:status)
    render json: args, status: status
  end

  def validate_input_parameters
    # prefer parameters in the :ticket namespace with the exception of nested dropdown custom fields
    # non-namespaced parameters are now considered "deprecated"

    if account_is_voxer? && params[:description].blank?
      log_request
      fix_request_params
    end

    if params.key?(:fields) && params.key?(:ticket)
      nested_fields = params[:ticket].delete(:fields) if params.key?(:ticket)
      params[:fields].update(nested_fields || {})
    end

    params.merge!(params.delete(:ticket) || {})

    email = Zendesk::Mail::Address.sanitize(params[:email])

    return send_message(error: 'Invalid email address', status: :unprocessable_entity) if email.nil?
    return send_message(error: 'Missing description', status: :unprocessable_entity) if params[:description].blank?
    params[:subject] = params[:description] if params[:subject].blank? && current_account.field_subject.is_active? && current_account.field_subject.is_required_in_portal?

    params[:description] += sanitized_submitted_from_footer

    @inbound = AnonymousRequest.new(params.merge(
      account: current_account,
      email: email,
      locale_id: locale_id,
      brand_id: current_brand.id
    ))

    add_client_info_from_request(@inbound, request)

    if @inbound.author.new_record? && !@inbound.author.save
      return send_message(error: "Could not create you as a user: #{@inbound.author.errors.full_messages.join(', ')}", status: :unprocessable_entity)
    end

    if params.key?(:recaptcha_response_field)

      Rails.logger.info("ORCA_RECAPTCHA_EMBEDDED_CONTROLLER")

      if !@inbound.author.is_verified? && !verify_recaptcha
        return send_message(error: 'Verification failed. Please try again.', status: :unauthorized)
      end
    else
      Rails.logger.warn("Embedded requests should switch to CAPTCHA")
    end
  end

  def log_request
    voxer_log(raw_request_body.inspect)
  rescue => e
    voxer_log("error attempting to log request body. #{e.inspect}")
  end

  def fix_request_params
    request.env.delete("action_dispatch.request.parameters")
    request.env.delete("action_dispatch.request.request_parameters")
    @_params = nil
    request.env["rack.input"] = StringIO.new(CGI.unescape(raw_request_body))
  end

  def raw_request_body
    raw = request.body.read
    request.body.rewind if request.body.respond_to?(:rewind)

    raw
  end

  def account_is_voxer?
    current_account.subdomain == 'voxer'
  end

  def voxer_log(message)
    Rails.logger.info("voxer_request: #{message}")
  end

  def locale_id
    if params[:locale_id].present?
      params[:locale_id]
    else
      current_account.locale_id
    end
  end

  def sanitized_submitted_from_footer
    result = params[:submitted_from]
    return '' if result.blank?
    [:token, :challenge, :sessionid, :password].each do |param|
      result.gsub!(%r{[?&][^?&=]*#{param}[^?&=]*=[^?&=]+}, '')
    end
    "\n\n------------------\n #{I18n.t('txt.controllers.request.embedded_controller.submitted_from')} #{result}"
  end
end
