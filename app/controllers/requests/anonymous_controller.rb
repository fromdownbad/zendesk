# This controller handles requests from users who have not logged in. It estimates a few properties and
# uses those to determine if this is spam or not.
class Requests::AnonymousController < Requests::BaseController
  include Zendesk::Stats::ControllerHelper
  include Zendesk::Tickets::Anonymous::ControllerSupport
  include HelpCenterRedirection
  include Zendesk::Tickets::ControllerSupport
  include Zendesk::Stats::ControllerHelper

  allow_anonymous_users if: proc { |c| c.send(:current_account).has_public_forums? }

  skip_before_action :authenticate_user, only: [:new, :create]
  before_action      :ensure_open_account
  before_action      :sanitize_email
  before_action      :validate_blacklist, :validate_human_user, :validate_suspended_user, :validate_rate_limit, only: :create
  before_action      :redirect_end_users_to_portal_controller, only: :new
  before_action      :redirect_to_help_center_new_request, only: :new

  layout 'user'

  helper_method :is_anonymous_request?

  # There are a lot of possible ticket parameters needed in lib/zendesk/tickets/initializer.rb
  # The parameters included here are the ones documented in the tests or clearly applicable to an
  # anonymous ticket creation.
  ticket_parameters = {
    account_id: Parameters.bigid,
    assigned_at: Parameters.string,
    assignee_updated_at: Parameters.string,
    base_score: Parameters.string,
    brand_id: Parameters.string,
    current_collaborators: Parameters.string,
    current_tags: Parameters.string,
    description: Parameters.string,
    fields: Parameters.map,
    priority_id: Parameters.integer,
    requester_data: Parameters.map | Parameters.string,
    subject: Parameters.string,
    ticket_form_id: Parameters.bigid,
    uploads: Parameters.string,
    via_followup_source_id: Parameters.string,
  }

  allow_parameters :index, email: Parameters.string
  def index
    redirect_to requests_path
  end

  allow_parameters :new,
    ticket: ticket_parameters,
    email: Parameters.string
  def new
    return render_404_page unless current_account.web_portal_enabled?

    ticket_params = params[:ticket] || {}
    @ticket = current_account.tickets.build
    @ticket.attributes = ticket_params
    @comment = @ticket.comment unless @ticket.comment.nil?
    @comment ||= Comment.new

    if (h = get_last_search_from_session(5.minutes))
      @from_search = h[:query]
    end

    respond_to do |format|
      format.html do
        render 'requests/portal/new'
      end
      format.mobile_v2 do
        render 'requests/portal/new'
      end
    end
  end

  allow_parameters :create,
    ticket_from_search: Parameters.string,
    email: Parameters.string,
    comment: {
      value: Parameters.string,
    },
    ticket: ticket_parameters,
    request: {
      subject: Parameters.string,
      comment: {
        value: Parameters.string
      }
    }
  def create
    if ticket.save
      case ticket
      when Ticket
        flash[:notice] = I18n.t('txt.requests.new.created', request_title: "##{ticket.nice_id} \"#{ERB::Util.h(ticket.title)}\"")

        store_search_ticket_stat(ticket_id: ticket.id, origin: 'portal')
      when SuspendedTicket
        msg = "#{I18n.t('txt.requests.new.almost_done')}. "
        msg << "<span class=\"footer\">#{I18n.t('txt.requests.new.verify_email_sent')} "

        if anonymous_ticket_initializer.can_login_to_verify?
          verify_link = authenticate_ticket_token_verification_path(token: ticket.token)
          msg << I18n.t('txt.requests.new.verify_login', verify_link: verify_link).to_s
        end

        msg << "</span>"

        flash[:notice] = msg
        store_search_ticket_stat(suspended_ticket_id: ticket.id, origin: 'portal')
      end

      redirect_to account_home
    else
      # will only occur when a Ticket is unsaveable
      # Ticket#valid? is called before a SuspendedTicket is constructed
      invalid_ticket!
    end
  rescue Zendesk::Tickets::Anonymous::TicketInvalid => e
    @ticket = e.record
    invalid_ticket!
  end

  protected

  def ticket
    @ticket ||= initialized_anonymous_ticket
  end

  def invalid_ticket!(to_flash = nil)
    @from_search = params[:ticket_from_search]

    unless @ticket.is_a?(Ticket)
      @ticket = anonymous_ticket_initializer.ticket
    end

    @comment = @ticket.comment

    flash[:error] = if to_flash
      flash_error(to_flash)
    else
      flash_error("#{I18n.t('txt.requests.new.error')}:", @ticket)
    end

    redirect_to new_anonymous_request_path(ticket: @ticket.attributes, email: params[:email], custom_fields: params[:ticket][:fields])
  end

  def validate_blacklist
    unless current_account.allows?(params[:email])
      invalid_ticket!(I18n.t('txt.requests.new.restricted_domain'))
    end
  end

  def validate_human_user
    Rails.logger.info("ORCA_RECAPTCHA_ANONYMOUS_CONTROLLER")

    unless !current_account.settings.captcha_required? || verify_recaptcha
      invalid_ticket!(I18n.t('txt.requests.new.captcha_error_v2'))
    end
  end

  def validate_suspended_user
    identity = find_email_identity(params[:email])

    if identity.present? && identity.user.suspended?
      invalid_ticket!(I18n.t('txt.requests.new.suspended_user'))
    end
  end

  def is_anonymous_request? # rubocop:disable Naming/PredicateName
    true
  end

  def sanitize_email
    params[:email] = Zendesk::Mail::Address.sanitize(params[:email]) || ""
  end

  def redirect_end_users_to_portal_controller
    if current_user_exists? && current_user.is_end_user?
      redirect_to new_request_path
      false
    end
  end

  private

  def redirect_to_help_center_new_request
    if should_redirect_to_help_center?
      redirect_to help_center_new_request_path, status: :moved_permanently
    end
  end
end
