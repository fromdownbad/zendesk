class Requests::MobileApiController < Requests::EmbeddedController
  before_action :ensure_mobile_api_agent

  allow_zendesk_mobile_app_access

  def self.check_spam?
    false
  end

  def create
    if current_account.has_mobile_dropbox_api?
      super
    else
      head :not_found
    end
  end

  private

  def ensure_mobile_api_agent
    send_message(error: 'Access denied') unless request.headers['X-Zendesk-Mobile-API']
  end
end
