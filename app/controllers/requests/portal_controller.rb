class Requests::PortalController < Requests::BaseController
  include HelpCenterRedirection

  before_action :redirect_to_help_center

  allow_parameters :new, :skip
  def new
    render_404_page
  end

  allow_parameters :index, :skip
  def index
    render_404_page
  end

  allow_parameters :show, :skip
  def show
    render_404_page
  end

  allow_parameters :update, :skip
  def update
    render_404_page
  end

  protected

  def redirect_to_help_center
    redirect_to "/hc#{request.path}"
  end
end
