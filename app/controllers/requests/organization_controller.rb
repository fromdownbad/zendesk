class Requests::OrganizationController < Requests::BaseController
  include Zendesk::Search::Responder

  before_action :redirect_agents_to_tickets_controller

  skip_around_action :do_not_render_private_comments, only: [:index]

  before_action :check_access
  helper_method :current_organization

  tab :organization

  allow_parameters :index,
    filter: Parameters.string,
    id: Parameters.string,
    organization_id: Parameters.bigid,
    user_id: Parameters.bigid,
    page: Parameters.integer, # untested, used by get_page
    desc: Parameters.integer,
    order: Parameters.string,
    flash_digest: Parameters.string
  def index
    @tickets, @page_title = find_tickets
    @tickets ||= []

    @tickets.each do |ticket|
      ticket.filter_description
      ticket.hide_private_comments!
    end

    respond_to do |format|
      format.html
      format.rss  { render '/requests/shared/index', layout: false, formats: [:xml] }
      format.xml  { render xml: @tickets.to_xml(for_end_user: true, root: 'tickets'), status: :ok }
      format.json { render json: @tickets.to_json(for_end_user: true), callback: params[:callback] }
    end
  end

  allow_parameters :search,
    query: Parameters.string,
    organization_id: Parameters.bigid,
    per_page: Parameters.integer,
    page: Parameters.integer,
    by_updated: Parameters.integer,
    type: Parameters.string,
    include: Parameters.string,
    forum_id: Parameters.string,
    sort: Parameters.string,
    order_by: Parameters.string
  def search
    @page_title = I18n.t('txt.requests.organization.index.viewing', organization: current_organization.name)

    return redirect_to_matched_ticket if ticket_match.found?

    query.execute(current_user)
    respond_with_search_result
  end

  allow_parameters :sidebar_users,
    page: Parameters.integer,
    organization_id: Parameters.bigid,
    filter: Parameters.string,
    user_id: Parameters.bigid
  def sidebar_users
    @user_page = params[:page]
    render partial: "sidebar_users"
  end

  private

  def paginated_users_for_organization
    current_organization.users.order(:name).paginate(per_page: 50, page: @user_page || 1)
  end
  helper_method :paginated_users_for_organization

  def redirect_to_matched_ticket
    redirect_to request_path(ticket_match.nice_id)
  end

  def ticket_match
    @ticket_match = Zendesk::Search::TicketMatch.new.tap do |match|
      match.query      = query
      match.account    = current_account
      match.conditions = "organization_id = #{current_organization.id}"
      match.user       = current_user
    end
  end

  def query
    @query ||= Zendesk::Search::Query.new(params[:query]).tap do |query|
      query.type = 'ticket'
      query.page = get_page
      query.options.merge!(search_params.by_updated_to_options)
    end
  end

  def search_params
    Zendesk::Search::Params.new(params)
  end

  def find_tickets
    assignee_column = current_account.field_assignee.is_visible_in_portal? ? :assignee : nil
    priority_column = current_account.field_priority.is_visible_in_portal? ? :priority : nil

    select = 'tickets.*'
    joins = []

    if assignee_column
      select << ", #{Zendesk::Fom::Field.for(:assignee).join_field_alias}"
      joins << Zendesk::Fom::Field.for(:assignee).join
    end

    case get_filter
    when 'solved'
      select << ", #{Zendesk::Fom::Field.for(:requester).join_field_alias}"
      joins << Zendesk::Fom::Field.for(:requester).join
      conditions = { status_id: [StatusType.SOLVED, StatusType.CLOSED] }
      @output = Output.create(columns: [:nice_id, :description, :requester, :solved, priority_column, assignee_column])
      page_title = I18n.t('txt.requests.index.title_solved')
      include_archived = true
    when 'requester'
      conditions = {requester_id: params[:user_id] }
      @output = Output.create(columns: [:nice_id, :description, :status, priority_column, assignee_column])
      page_title = I18n.t('txt.requests.organization.index.title_by', user: current_organization.users.find(params[:user_id]))
      include_archived = true
    else
      select << ", #{Zendesk::Fom::Field.for(:requester).join_field_alias}"
      joins << Zendesk::Fom::Field.for(:requester).join

      status_ids = [StatusType.NEW, StatusType.OPEN, StatusType.PENDING]
      status_ids <<  StatusType.HOLD if current_account.use_status_hold?
      conditions = { status_id: status_ids }

      @output    = Output.create(columns: [:nice_id, :description, :requester, :status, priority_column, assignee_column])
      page_title = I18n.t('txt.requests.index.title_open')
      include_archived = false
    end

    tickets = if include_archived
      current_organization.tickets.archived_paginate(
        select: select,
        joins: joins.join(' '),
        conditions: conditions,
        order: @output.set_order(params),
        per_page: API_DEFAULT_PER_PAGE,
        page: get_page
      )
    else
      current_organization.tickets.
        select(select).
        where(conditions).
        joins(joins.join(' ')).
        order(@output.set_order(params)).
        paginate(per_page: API_DEFAULT_PER_PAGE, page: get_page)
    end

    [tickets, page_title]
  end

  def current_organization
    @current_organization ||= if params[:organization_id]
      current_user.organizations.find(params[:organization_id])
    else
      # This should be safe to remove once the /organization_requests
      # route is removed from config/routes.rb (line 195 as of 2013-05-08)
      current_user.default_organization
    end
  end

  def get_filter # rubocop:disable Naming/AccessorMethodName
    params[:filter] || params[:id]
  end

  def check_access
    return deny_access if !current_organization || !current_user.can?(:view, current_organization)
  end
end
