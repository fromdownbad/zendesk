class Requests::BaseController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Tickets::ClientInfo

  around_action :do_not_render_private_comments

  layout 'user'

  protected

  def redirect_agents_to_tickets_controller
    return if request.is_mobile? && request.format == :js # accept ajax requests from mobile sites

    if current_user_exists? && current_user.is_agent?
      redirect_to controller: '/tickets', action: params[:action]
      return false
    end
  end

  def do_not_render_private_comments(&block)
    Comment.where(is_public: true).scoping(&block)
  end
end
