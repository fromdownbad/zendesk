class BrandsController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Brands::ControllerSupport

  before_action :authorize_admin
  before_action :require_multibrand_feature!

  ssl_allowed :all
  layout "brands"

  helper_method :brand_state_classes
  helper_method :multiple_help_centers_available?

  allow_parameters :index, {}
  def index
    @active_brands, @inactive_brands = brands.partition(&:active?)

    @landing_page =
      if current_account.hub? || current_account.spoke?
        'hub_and_spoke_landing'
      elsif !current_account.has_multibrand?
        'checklist_landing'
      elsif current_account.new_to_branding?
        'landing'
      end

    new_brand
  end

  brand_parameters = {
    brand: Parameters.map(
      name: Parameters.string,
      subdomain: Parameters.string,
      host_mapping: Parameters.string,
      signature_template: Parameters.string,
      logo: Parameters.map(uploaded_data: Parameters.file | Parameters.nil_string)
    )
  }

  allow_parameters :create, brand_parameters.dup
  def create
    new_brand.attributes = brand_params.except(:logo)
    new_brand.set_brand_logo(brand_params[:logo]) if brand_params.include?(:logo)

    if new_brand.save
      current_account.class.on_master { new_brand.route.reload }
      flash[:notice] = I18n.t('txt.multibrand.brand.create.success')
    else
      flash[:error] = flash_error(I18n.t('txt.multibrand.brand.create.fail'), new_brand)
    end
    redirect_to brands_path
  end

  allow_parameters :update, brand_parameters.dup.merge(id: Parameters.bigid)
  def update
    brand.attributes = brand_params.except(:logo)
    brand.set_brand_logo(brand_params[:logo]) if brand_params.include?(:logo)

    if brand.invalid_subdomain_change?(current_user)
      flash[:error] = flash_error(I18n.t("txt.multibrand.brand.invalid_subdomain_change"), brand)
      redirect_to(brands_path) && return
    end

    if brand.save
      flash[:notice] = I18n.t('txt.multibrand.brand.update.success')
      # Reload new route back into kasket
      current_account.class.on_master { brand.route.reload }
    else
      flash[:error] = flash_error(I18n.t('txt.multibrand.brand.update.fail'), brand)
    end
    redirect_to brands_path
  end

  allow_parameters :make_default,
    id: Parameters.bigid
  def make_default
    if current_account.update_attributes(default_brand: brand)
      flash[:notice] = I18n.t('txt.multibrand.brand.make_default.success', new_brand_name: brand_name)
      mark_modal_as_seen
    else
      flash[:error] = flash_error(I18n.t('txt.multibrand.brand.make_default.fail', new_brand_name: brand_name))
    end
    redirect_to brands_path
  end

  allow_parameters :destroy,
    id: Parameters.bigid
  def destroy
    if brand.soft_delete
      flash[:notice] = I18n.t('txt.multibrand.brand.destroy.success', brand_name: brand_name)
    else
      flash[:error] = flash_error(I18n.t('txt.multibrand.brand.destroy.fail', brand_name: brand_name))
    end

    redirect_to brands_path
  end

  allow_parameters :toggle,
    id: Parameters.bigid
  def toggle
    if brand.active?
      active = false
      success_msg = I18n.t('txt.multibrand.brand.deactivate.success', brand_name: brand_name)
      failure_msg = I18n.t('txt.multibrand.brand.deactivate.fail', brand_name: brand_name)
    else
      active = true
      success_msg = I18n.t('txt.multibrand.brand.activate.success', brand_name: brand_name)
      failure_msg = I18n.t('txt.multibrand.brand.activate.fail', brand_name: brand_name)
    end

    if brand.update_attributes(active: active)
      flash[:notice] = success_msg
    else
      flash[:error] = failure_msg
    end

    redirect_to brands_path
  end

  allow_parameters :request_multibrand, {}
  def request_multibrand
    current_account.request_brands(current_user)
    head :ok
  end

  allow_parameters :switch_require_brand,
    settings: Parameters.map(
      require_brand_on_new_tickets: Parameters.boolean
    )
  def switch_require_brand
    current_account.settings.set(require_brand_params)

    if current_account.save
      head :ok
    else
      head :bad_request
    end
  end

  private

  def brand_name
    ERB::Util.h(brand.name)
  end

  def require_brand_params
    params.require(:settings).permit(
      require_brand_on_new_tickets: Parameters.boolean
    )
  end

  def brand_state_classes(brand)
    classes = []
    classes << 'new'      if brand.new_record?
    classes << 'default'  if brand.default?
    classes << 'inactive' unless brand.active?
    classes << 'agent'    if brand.route_is_agent_route?
    classes.join(' ')
  end

  def mark_modal_as_seen
    return unless current_user.settings.show_new_default_brand_modal

    current_user.settings.show_new_default_brand_modal = false
    current_user.save!
  end

  def multiple_help_centers_available?
    return @multiple_help_centers_available if defined?(@multiple_help_centers_available)
    @multiple_help_centers_available = Guide::MultipleHelpCenters.new(current_account).available?
  end

  def brands
    @brands ||= current_account.brands.sort_by { |brand| brand.name.downcase }
  end

  def brand
    @brand ||= current_account.brands.find(params.require(:id))
  end

  def brand_params
    params.require(:brand).permit(:name, :subdomain, :host_mapping, :signature_template, logo: :uploaded_data)
  end
end
