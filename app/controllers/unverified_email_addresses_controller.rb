class UnverifiedEmailAddressesController < ApplicationController
  include ::AllowedParameters

  layout false

  skip_before_action :authenticate_user, only: [:verify]
  before_action :find_user, except: [:verify, :manual_agent_verify]
  before_action :authorize_editing, except: [:verify, :manual_agent_verify]

  allow_parameters :new,
    user_id: Parameters.bigid

  def new
  end

  allow_parameters :create,
    email: Parameters.string,
    user_id: Parameters.bigid

  def create
    if current_user.is_end_user? && current_account.arturo_enabled?('disable_legacy_people_users_id_page')
      return render_404_page
    end

    skip_verification = current_user.is_agent? && current_user != @user && !current_account.is_welcome_email_when_agent_register_enabled?
    @email_address = @user.unverified_email_addresses.new(email: params[:email], skip_verification: skip_verification)

    respond_to do |format|
      if @email_address.save
        format.html { redirect_to(edit_user_path(@user, anchor: 'identities')) }
        format.js   { redirect_to(edit_user_path(@user)) }
        format.json { head :created }
      else
        flash[:error] = flash_error(I18n.t('txt.error_message.controllers.unverified_email_address_controller.failed_to_add_new_email_address'), @email_address)
        format.html { redirect_to(edit_user_path(@user, anchor: 'identities')) }
        format.js   { render plain: render_to_string(partial: 'shared/flash'), status: :not_acceptable }
        format.json { render json: @user.errors, status: :not_acceptable }
      end
    end
  end

  allow_parameters :verify,
    token: Parameters.string,
    user_id: Parameters.bigid,
    unverified_email_address: Parameters.map(confirmed: Parameters.boolean)

  def verify
    @email_address = current_account.unverified_email_addresses.find_by_token(params[:token])
    unless @email_address.present?
      return render_failure(
        status: :bad_request, title: I18n.t('txt.email.no_unverified.title'),
        message: I18n.t('txt.email.no_unverified.message')
      )
    end

    if @email_address.user.is_verified
      if authenticate_user && current_user != @email_address.user
        return render_failure(
          status: :bad_request, title: I18n.t('txt.email.no_unverified.title'),
          message: I18n.t('txt.email.no_unverified.message')
        )
      elsif !logged_in?
        # authenticate_user calls #access_denied which renders a 401 which
        # warden will handle
        return
      end
    end

    respond_to do |format|
      verified_identity = CIA.audit(actor: @email_address.user, effective_actor: @email_address.user) do
        merge_confirmation = !!params.try(:[], :unverified_email_address).try(:[], :confirmed)
        @email_address.verify!(merge_confirmation)
      end

      if verified_identity
        self.current_user = @email_address.user
        if @email_address.user.crypted_password.blank?
          token = verified_identity.verification_tokens.create
          format.html { redirect_to(email_token_verification_path(token: token.value)) }
          format.mobile_v2 { redirect_to(email_token_verification_path(token: token.value)) }
        else
          flash[:notice] = I18n.t('txt.verification.verified')
          format.html { redirect_to(edit_user_path(@email_address.user)) }
          format.mobile_v2 { redirect_to new_request_path }
        end
      else
        # the only reason we fail to verify is that we didn't confirm,
        # which is required when a merge is going to happen.
        if email = @email_address.user.email
          @confirmation_button = I18n.t('txt.unverified_email.confirmation_button_email', existing_email: email)
          @confirmation_warning = I18n.t('txt.unverified_email.confirmation_warning_email', existing_email: email)
          @confirmation_question = I18n.t('txt.unverified_email.confirmation_question_email', email: @email_address.email, existing_email: email)
        elsif screen_name = @email_address.user.twitter_profile.try(:screen_name)
          @confirmation_button = I18n.t('txt.unverified_email.confirmation_button_twitter', twitter: screen_name)
          @confirmation_warning = I18n.t('txt.unverified_email.confirmation_warning_twitter', twitter: screen_name)
          @confirmation_question = I18n.t('txt.unverified_email.confirmation_question_twitter', email: @email_address.email, twitter: screen_name)
        else
          @confirmation_button = I18n.t('txt.unverified_email.confirmation_button_username', name: @email_address.user.name)
          @confirmation_warning = I18n.t('txt.unverified_email.confirmation_warning_username', name: @email_address.user.name)
          @confirmation_question = I18n.t('txt.unverified_email.confirmation_question_username', email: @email_address.email, name: @email_address.user.name)
        end
        format.html { render layout: 'user' }
        format.mobile_v2 { render layout: 'user' }
      end
    end
  end

  allow_parameters :manual_agent_verify,
    id: Parameters.string,
    user_id: Parameters.bigid

  def manual_agent_verify
    @email_address = current_account.unverified_email_addresses.find(params[:id])
    respond_to do |format|
      format.html do
        if @email_address.verify!(true)
          flash[:notice] = "#{@email_address.email} manually verified for #{CGI.escapeHTML(@email_address.user.name)}."
        else
          flash[:error] = flash_error("Failed to verify", @email_address)
        end

        redirect_to(user_path(@email_address.user))
      end
    end
  rescue ActiveRecord::RecordInvalid => e
    @email_address.errors.add(:base, e.message)
    flash[:error] = flash_error("Failed to verify", @email_address)
    redirect_to(user_path(@email_address.user))
  end

  allow_parameters :resend_email,
    id: Parameters.string,
    user_id: Parameters.bigid

  def resend_email
    @email_address = @user.unverified_email_addresses.find(params[:id])
    @email_address.send_verification_email
    flash[:notice] = I18n.t('txt.access.help.welcome_mail_sent', user_email: @email_address.email)
    respond_to do |format|
      format.html { redirect_to(user_path(@user)) }
    end
  end

  allow_parameters :destroy,
    id: Parameters.string,
    user_id: Parameters.bigid

  def destroy
    @email_address = @user.unverified_email_addresses.find_by_id(params[:id])

    unless @email_address.present?
      head :not_found
      return
    end

    if @email_address.destroy
      respond_to do |format|
        format.html do
          flash[:notice] = "Email address deleted"
          redirect_to edit_user_path(@user)
        end
        format.js { head :ok }
        format.json { render json: {}, status: :ok }
      end
    else
      respond_to do |format|
        flash[:error] = flash_error("Could not delete email address", @email_address)
        format.html do
          redirect_to edit_user_path(@user)
        end
        format.json { render json: {}, status: :not_acceptable }
        format.js { head :not_acceptable }
      end
    end
  end

  private

  def find_user
    @user = current_account.users.find(params[:user_id])
  end

  def authorize_editing
    return deny_access unless current_user.can?(:edit, @user)
  end
end
