require 'digest/md5'
require 'pg_query'
require 'zendesk_stats'
require 'zendesk/tickets/recent_ticket_management'
require 'zendesk/session_management'
require 'zendesk_core_application/secure_headers/controller_extension'

# Filters added to this controller will be run for all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base
  protected

  helper :all
  helper_method :can?, :get_page, :current_account, :use_remote_authentication?,
    :subscription_settings_path, :current_user_javascript_serializer,
    :user_portal_state

  prevent_zendesk_mobile_app_access

  include Zendesk::CnameUtils
  include FlashHelper
  include Zendesk::Ipm::ControllerSupport
  include Zendesk::Auth::AccountHome

  before_action :disable_link_prefetching # Stop Google Web Accelerator
  before_action :set_default_headers
  before_action :verify_current_account

  include ZendeskCoreApplication::SecureHeaders::ControllerExtension

  before_action :set_mobile_format
  before_action :set_ip_address_for_current_user

  include SslRequirement # Must be included after set_account filter
  include Zendesk::Auth::Warden::ControllerMixin
  include Zendesk::Auth::AuthenticatedSessionMixin
  include Zendesk::BlockMonitorChanges
  include Zendesk::OAuth::ControllerMixin
  include Zendesk::Auth::CsrfControl
  protect_from_forgery with: :exception
  include SharedCsrfTokenControllerSupport

  include ApiHelper
  include BrowserPredicatesHelper
  include Zendesk::SessionManagement
  include Zendesk::Tickets::RecentTicketManagement
  include Zendesk::I18n::LanguageDetection
  include Zendesk::CommentOnQueries
  include Zendesk::I18n::LocalizeUser
  include Zendesk::AccountRestriction
  include Zendesk::ReturnTo::Helper
  include Zendesk::ReturnTo::FailureHelper
  include Zendesk::Stats::ControllerMixin
  include CiaControllerSupport
  include Zendesk::Devices::ControllerMixin
  include Zendesk::Errors::ErrorHandling
  include PageNotFoundRenderingSupport
  include Zendesk::Accounts::Multiproduct
  include Zendesk::Deprecation::ControllerSupport
  private :warden_scope, :warden_scope?

  extend Zendesk::RequireCapability

  before_action :prepare_payment_notification, only: [:new, :index, :show]
  before_action :log_session_info
  before_action :destroy_challenge_tokens
  before_action :ensure_support_is_active!

  layout :layout_for_role

  rescue_from Prop::RateLimited do |exception|
    flash.discard
    flash[:error] = flash[:notice] = nil # Check commit comment if you're wondering
    Rails.logger.warn("Throttle triggered: #{exception.handle} for account #{current_account.id}/#{current_account.subdomain} at #{Time.now}; #{exception.message}")

    render_failure(status: :forbidden, title: "Forbidden by rate limit", message: exception.description)
  end

  rescue_from ActionController::RedirectBackError do
    redirect_to account_home
  end

  rescue_from ActiveRecord::RecordNotFound do
    render_404_page
  end

  rescue_from ActiveHash::RecordNotFound do
    render_404_page
  end

  # TODO: remove this and fix all failing controller tests (integration tests should not fail)
  rescue_from ActionController::UnknownFormat do
    head :not_acceptable
  end

  rescue_from ActionView::MissingTemplate do |exception|
    Rails.logger.error "Missing template #{exception.message}"
    render_failure(
      status: :unsupported_media_type, title: I18n.t("txt.error_message.controllers.application_controller.unsupported_media_type_title"),
      message: I18n.t("txt.error_message.controllers.application_controller.unsupported_media_type_message")
    )
  end

  rescue_from ActiveRecord::UnknownAttributeError do |exception|
    render_failure(
      status: :not_acceptable,
      title: "Unknown attribute",
      message: "Unknown attribute: #{exception.message.split(':').last.strip}"
    )
  end

  rescue_from ActiveRecord::StatementInvalid do |e|
    begin
      # Extract the sql statement without comment from the error message
      sql_statement = e.message.match(/(?<=\): |\.: )(?<sql>.+?)(?= \/\*)/)[:sql]
      fingerprint = Digest::MD5.hexdigest(PgQuery.normalize(sql_statement))
      ZendeskExceptions::Logger.record(e, location: self, message: e.message, fingerprint: fingerprint)
    rescue StandardError
      ZendeskExceptions::Logger.record(e, location: self, message: e.message, fingerprint: Digest::MD5.hexdigest(e.message))
    end

    render json: {
      error: "DatabaseError",
      description: "Unknown database error"
    }, status: :service_unavailable
  end

  rescue_from ActiveRecord::AssociationTypeMismatch do |_exception|
    render_failure(
      status: :not_acceptable, title: I18n.t("txt.error_message.controllers.application_controller.association_type_mismatch_title"),
      message: I18n.t("txt.error_message.controllers.application_controller.association_type_mismatch_message")
    )
  end

  def can?(*args)
    current_user.can?(*args)
  end

  def flash
    f = super
    if f.class == Hash
      logger.warn("Overwriting bad flash hash")
      @_flash = FlashHash.new
    else
      f
    end
  end
  public :flash

  # Use the CNAME host as default if set
  def default_url_options(_options = nil)
    if session.present? && session["cname_master"].present?
      { host: session["cname_master"] }
    else
      {} # host => nil would be invalid on rails 3
    end
  end

  # Determine layout template to use
  # Usage in controller:
  #   layout :layout_for_role
  def layout_for_role
    return 'user' if !current_user || !current_user.id
    current_user.is_end_user? ? 'user' : 'agent'
  end

  def authorize_agent_or_end_user
    grant_access(current_user.is_end_user? || current_user.is_agent?)
  end

  def authorize_agent
    grant_access current_user.is_agent?
  end

  def authorize_unrestricted_agent
    grant_access current_user.is_non_restricted_agent?
  end

  def authorize_manage_people_agent
    grant_access current_user.can_manage_people?
  end

  def authorize_admin
    grant_access current_user.is_admin?
  end

  def authorize_owner
    grant_access(current_user.is_account_owner?)
  end

  def authorize_internal_api
    logger.info "authorize_internal_api: #{request.ip} (#{request.remote_ip}) #{internal_request? ? "is" : "is not"} internal"
    grant_access(internal_request?)
  end

  def authorize_integration_network
    logger.info "authorize_integration_network: #{request.ip} (#{request.remote_ip})"
    grant_access(internal_request? || integration_network?)
  end

  def internal_request?
    Zendesk::Net::IPTools.internal_ip_whitelist_bypass?(request.remote_ip)
  end

  def integration_network?
    Zendesk::Net::IPTools.whitelist_handles_ip?(request.remote_ip, Zendesk::Net::Configuration.fetch(:integration_networks))
  end

  # Set source page - redirected to when ticket is updated
  def set_source_page
    if request.env['PATH_INFO'] && !api_request? && !params[:format]
      path = request.fullpath
      session[:source_page] = path if path.size < 1000 # prevent 500 error on too long pages not fitting into CookieStore
    end
  end

  def http_if_modified_since(fallback = 3.days.ago)
    modified_since = request.headers["If-Modified-Since"]

    this_time = Time.parse(modified_since) rescue (return fallback.strftime('%Y-%m-%d %H:%M:%S'))
    this_time = this_time.utc

    this_time.strftime('%Y-%m-%d %H:%M:%S')
  end

  def deny_access
    if current_registered_user
      forbidden!
    else
      head :unauthorized # will be caught by warden -> redirect to /access/unauthenticated
    end
  end

  def use_remote_authentication?
    return false unless current_account

    current_account.remote_authentications.active.any? do |remote_authentication|
      remote_authentication.handles_ip?(request.remote_ip)
    end
  end

  def is_action_cache_safe? # rubocop:disable Naming/PredicateName
    return false if current_user_exists?
    (current_user.nil? || !current_user.is_agent?) &&
      flash.empty? && !@display_payment_notice &&
      params[:show_version].blank?
  end

  def action_cache_key(hash)
    key = params.dup
    key.delete(:action)
    key.delete(:controller)

    key[:account]  = current_account.cache_key
    key[:xhr]      = request.xhr?
    key[:asset_id] = ENV["RAILS_ASSET_ID"]

    if current_user.is_agent?
      key[:rules]        = current_account.scoped_cache_key(:rules)
      key[:current_user] = current_user.cache_key if current_account.subscription.has_personal_rules?
    end

    key.merge!(hash)
    format = (request.format.is_a?(Mime::Type) ? request.format.to_sym.to_s : request.format.to_s)
    key[:request] = { format: format, accept: request.env["HTTP_ACCEPT"] }

    if key.delete(:logging)
      Rails.logger.info("Action caching #{request.url} with key #{key.inspect}")
    end

    { key: Digest::MD5.hexdigest(key.inspect) }
  end

  def mark_as_possibly_in_lotus
    @possibly_in_lotus = true
  end

  def current_user_javascript_serializer
    Zendesk::Users::JavascriptSerializer.new(
      current_user,
      is_assumed_identity: is_assuming_user?,
      ssl_environment: ssl_environment?,
      is_mobile_agent: Schmobile::UserAgents.is_mobile_agent?(request.user_agent)
    )
  end

  private_class_method def self.tab(tab_name, rules = {})
    before_action rules do |controller|
      controller.tab = tab_name
    end
  end

  protected

  attr_accessor :tab

  # Expose the current account for use in eg. cache keys (see
  # search_controller for an example)
  def current_account
    request.env['zendesk.account']
  end

  private

  def user_portal_state(account = nil)
    @user_portal_state ||= Zendesk::UserPortalState.new(account || current_account)
  end

  def prepare_payment_notification
    if current_user && current_user.is_admin? && current_account.subscription.present?
      if Time.now.to_i > session[:show_account_disabled_at].to_i && current_account && !current_account.is_serviceable?
        session[:show_account_disabled_at] = 1.hour.from_now.to_i
        @display_payment_notice = true
      elsif Time.now.to_i > session[:show_payment_notice_at].to_i && current_account.try(:is_failing?)
        session[:show_payment_notice_at] = 1.day.from_now.to_i
        @display_payment_notice = true
      end
    end
  end

  # TODO: refactor: inline it to make the code clearer
  def grant_access(statement)
    forbidden! unless statement
  end
  protected :grant_access

  # you are authenticated but forbidden access
  def forbidden!
    render_failure(
      status: :forbidden,
      title: I18n.t('txt.errors.access_denied.title'),
      message: I18n.t('txt.errors.access_denied.message')
    )
  end

  def disable_link_prefetching
    if request.env["HTTP_X_MOZ"] == "prefetch"
      head :forbidden
      false
    end
  end

  def set_default_headers
    # Support CNAME - http://blog.leetsoft.com/2007/3/26/domain-claims-and-its-woes
    response.headers['P3P'] = %(CP="NOI DSP COR NID ADMa OPTa OUR NOR")
    # respond with Vary: Accept to help with client-side caching problems (see ZD:146013)
    response.headers['Vary'] = "Accept"
  end

  def verify_current_account
    unless current_account && current_account.is_active?
      respond_to do |format|
        format.html do
          redirect_to_marketing_page
        end

        format.all do
          head :not_found
        end
      end
    end
  end

  def set_mobile_format
    return if (current_account && !current_account.has_mobile_switch_enabled?) || params[:format] == 'html'
    if request.is_mobile? && [Mime[:html], Mime::ALL].member?(request.format.to_s)
      request.format = :mobile_v2 unless current_account&.has_ignore_mobile_v2?
      $statsd.increment('mobile_v2', tags: ["controller:#{controller_name}", "action:#{action_name}"]) # rubocop:disable Style/GlobalVars
    end
  end

  # Set the IP address of the request on the current user.
  #
  # This allows querying the IP address later on using `User#ip_address`.
  #
  # Returns nothing.
  def set_ip_address_for_current_user
    current_user.ip_address = request.remote_ip if current_user.present?
  end

  # RAILS5UPGRADE: This might be eliminated if we're sure we have
  # universally consistent stronger_parameter type definitions
  # Ensures only valid page numbers returned for will_paginate pagination
  def get_page # rubocop:disable Naming/AccessorMethodName
    params[:page].to_s.to_i <= 0 ? 1 : params[:page].to_i
  end

  def get_csv_content_type # rubocop:disable Naming/AccessorMethodName
    if /windows/i.match?(request.user_agent)
      'application/vnd.ms-excel; charset=iso-8859-1; header=present'
    else
      'text/csv; charset=iso-8859-1; header=present'
    end
  end

  def find_email_identity(email)
    current_account.user_identities.where(type: UserEmailIdentity.name, value: email).first
  end

  def ensure_open_account
    unless current_account.is_open?
      respond_to do |format|
        format.any(:html, :mobile_v2) { return render_failure(status: :not_found, title: 'Feature unavailable', message: 'This feature is only available for "open" accounts') }
        format.any(:js, :json) { render json: { error: 'This feature is only available for "open" accounts that do not require users to sign up' }, status: :not_found }
      end
    end
  end

  def subscription_settings_path(_options = {})
    "/agent#/admin/subscription"
  end

  def destroy_challenge_tokens
    if params[:challenge].present?
      Rails.logger.info("Destroying challenge token from #{self.class}##{action_name}")
      token = current_account.challenge_tokens.find_by_value(params[:challenge])
      token.try(:destroy)
    end
  end

  def ensure_support_is_active!
    return unless multiproduct?
    return if request.path == subscription_settings_path # prevent billing redirect loop when support product is not active

    if support_product.nil? || support_product.not_started?
      Rails.logger.info "Support product not started for account #{current_account.idsub}, redirecting to trial activation."
      redirect_to('/agent/start')
    elsif !support_product.active?
      Rails.logger.info "Support product not active for account #{current_account.idsub}, redirecting to billing."
      redirect_to('/account/subscription')
    end
  end
end
