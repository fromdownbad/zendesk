require 'bcrypt'

class ExternalEmailCredentialsController < ApplicationController
  include ::AllowedParameters
  before_action :check_permissions
  allow_zendesk_mobile_app_access # Inbox

  ssl_allowed :all

  allow_parameters :oauth_start, :all_parameters
  # ALSO USED DIRECTLY VIA INBOX
  def oauth_start
    hashed_csrf_token = BCrypt::Password.create(shared_session[:csrf_token])
    url = "https://accounts.google.com/o/oauth2/auth"

    scope = "https://www.googleapis.com/auth/gmail.modify https://www.googleapis.com/auth/userinfo.email"

    url_params = {
      state: "#{current_account.subdomain}:#{oauth_callback_external_email_credentials_path(params.except(:controller, :action))}:#{hashed_csrf_token}",
      prompt: 'consent', # approve every time -> we always get a refresh_token (not only on first call)
      scope: scope,
      response_type: "code",
      access_type: "offline",
      redirect_uri: ExternalEmailCredential.redirect_uri,
      client_id: Zendesk::Configuration.dig!(:external_email_credential, :client_id)
    }

    if params[:select_account] == 'true'
      url_params[:prompt] = 'select_account consent' # select account and approve every time
    end

    if params[:id]
      credential = current_account.external_email_credentials.find(params[:id])

      RevokeExternalEmailCredentialJob.work(credential.encrypted_value, credential.encryption_key_name, credential.encryption_cipher_name) if credential
      url_params[:login_hint] = credential&.email
    end

    redirect_to "#{url}?#{url_params.to_query}"
  end

  allow_parameters :oauth_callback,
    brand_id: Parameters.bigid,
    code: Parameters.string,
    csrf_token: Parameters.string,
    error: Parameters.string,
    id: Parameters.bigid,
    initial_import: Parameters.string,
    return_to: Parameters.string,
    return_to_access_denied: Parameters.string,
    authuser: Parameters.integer, # from logs, untested and undocumented
    hd: Parameters.string, # from logs, untested and undocumented
    prompt: Parameters.string, # from logs, untested and undocumented
    scope: Parameters.string # from logs, untested and undocumented
  def oauth_callback
    verify_token

    if params[:code]
      access_token, refresh_token = oauth_token_exchange(params[:code])
      email = fetch_email_via_oauth(access_token)
      user = current_account.find_user_by_email(email)

      if user
        error = {
          key: "txt.admin.views.settings.email._settings.external_email_credentials.error_user_already_exists_v3",
          params: {link: user.name}
        }
      else
        credential = if params[:id]
          current_account.external_email_credentials.find(params[:id])
        else
          current_account.external_email_credentials.where(username: email).first_or_initialize
        end

        existing_username = credential.username
        brand_id = params[:brand_id]
        credential.brand_id = brand_id if brand_id

        credential.assign_attributes(
          initial_import: (params[:initial_import] == "true"),
          encrypted_value: refresh_token,
          username: email,
          current_user: current_user
        )

        if credential.save
          credential_changes = credential.previous_changes.each_with_object({}) do |change, result|
            redacted_attr = %w[encrypted_value encryption_key_name encryption_cipher_name]
            next if redacted_attr.include?(change[0])

            result[change[0]] = change[1]
          end

          Rails.logger.info("External Email Credential saved: #{credential_changes}")
          flash[:notice] = I18n.t(
            "txt.admin.views.settings.email._settings.external_email_credentials.successfully_connected",
            email: "<strong>#{ERB::Util.h(email)}</strong>".html_safe,
            link: self.class.link_to_disconnect_external_email_credential(view_context, credential)
          )
        elsif params[:id]
          error = {
            key: "txt.admin.views.settings.email._settings.external_email_credentials.error_email_doesnt_match_v2",
            params: {new: email, existing: existing_username}
          }
        end
      end

      if params[:return_to]
        # when redirecting, base64 urlsafe coding it to avoid issues when parsing params
        base64_error = Base64.urlsafe_encode64(error.to_json) if error

        params[:return_to] <<
          (params[:return_to].include?("?") ? "&" : "?") <<
          (error ? {error: base64_error}.to_query : {id: credential.id, recipientId: credential.recipient_address.id}.to_query)
        redirect_to_return_to
      else
        flash[:error] = I18n.t(error[:key], error[:params]) if error
        redirect_to current_account.email_settings_path
      end
    else
      if params[:return_to_access_denied]
        uri = URI.parse(params[:return_to_access_denied])

        # Only do this requested redirect if it's a relative path with no host.
        uri.path = sanitize_redirect_uri(uri)

        # Fixes up paths like "//example/"
        new_path = uri.path.squeeze("/")
        uri = URI.parse(new_path) if new_path != uri.path

        path = uri.path
        path << "?#{URI.escape(uri.query)}" if uri.query

        if params[:error] == 'access_denied'
          error = {
            key: 'txt.admin.views.settings.email._settings.external_email_credentials.access_denied'
          }

          base64_error = Base64.urlsafe_encode64(error.to_json)
          path << (path.include?('?') ? '&' : '?')
          path << { error: base64_error }.to_query
        end

        return redirect_to path
      end

      redirect_to_return_to current_account.email_settings_path
    end
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    external_email_credential = current_account.external_email_credentials.find(params[:id])
    success = external_email_credential.destroy
    save_response(success, external_email_credential)
  end

  def self.link_to_disconnect_external_email_credential(context, credential, options = {})
    title = I18n.t('txt.admin.views.settings.email._settings.external_email_credentials.disconnect')
    context.link_to title, context.external_email_credential_path(credential), options.reverse_merge(method: :delete)
  end

  private

  def save_response(success, external_email_credentials)
    respond_to do |format|
      format.html do
        # everything is done via ajax, so this should never be hit
        default_flash(success)
        redirect_to settings_email_path
      end
      format.json do
        if success
          render json: external_email_credentials
        else
          render json: Api::V2::ErrorsPresenter.new.present(external_email_credentials), status: :not_acceptable
        end
      end
    end
  end

  def default_flash(success)
    if success
      flash[:notice] = I18n.t("txt.controllers.flash.notice.saved")
    else
      flash[:error] = I18n.t("txt.controllers.flash.error.saved")
    end
  end

  def oauth_token_exchange(code)
    response = Faraday.post(
      'https://accounts.google.com/o/oauth2/token',
      client_id: Zendesk::Configuration.dig!(:external_email_credential, :client_id),
      client_secret: Zendesk::Configuration.dig!(:external_email_credential, :client_secret),
      code: code,
      grant_type: 'authorization_code',
      redirect_uri: ExternalEmailCredential.redirect_uri
    )
    raise "Google connection error: #{response.inspect}" unless response.status == 200
    JSON.load(response.body).values_at("access_token", "refresh_token")
  end

  def fetch_email_via_oauth(access_token)
    response = Faraday.get("https://www.googleapis.com/oauth2/v1/userinfo?access_token=#{access_token}")
    raise "Google connection error: #{response.inspect}" unless response.status == 200
    JSON.load(response.body).fetch("email")
  end

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end

  def verify_token
    if params[:csrf_token].blank? || BCrypt::Password.new(params[:csrf_token]) != shared_session[:csrf_token]
      raise(ActionController::InvalidAuthenticityToken)
    end
  rescue BCrypt::Errors::InvalidHash
    raise(ActionController::InvalidAuthenticityToken)
  end

  def sanitize_redirect_uri(uri)
    /^(\/agent\/admin|\/agent\/discovery)/i.match?(uri.path) ? uri.path : current_account.email_settings_path
  end
end
