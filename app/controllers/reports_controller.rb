require 'zendesk/gooddata/user_provisioning'

class ReportsController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable
  include Zendesk::Reports::ControllerSupport

  XML_EXPORT_ARCHIVED_TICKET_LIMIT = 200_000

  ssl_allowed :all

  before_action :check_view_access,        only: [:show, :feed, :settings]
  before_action :check_manage_access,      only: [:create, :new, :edit, :update, :destroy, :preview]
  before_action :register_device_activity, only: [:index]
  before_action :initialize_report,        only: [:new, :create, :preview]

  tab :reports

  layout 'agent'

  # A payload looks something like this:
  #
  # {
  #   "utf8": "✓",
  #   "authenticity_token": "SECRET",
  #   "report": {
  #     "title": "aad US CXT",
  #     "is_relative_interval": "true",
  #     "relative_interval_in_days": "7",
  #     "from_date(1i)": "2016",
  #     "from_date(2i)": "3",
  #     "from_date(3i)": "28",
  #     "to_date(1i)": "2016",
  #     "to_date(2i)": "4",
  #     "to_date(3i)": "4"
  #   },
  #   "sets": {
  #     "1": {
  #       "legend": "Chasity",
  #       "state": "solved_at",
  #       "conditions": {
  #         "1": {
  #           "source": "assignee_id",
  #           "operator": "is",
  #           "value": {
  #             "0": "3902961786"
  #           }
  #         }
  #       }
  #     },
  #     "2": {
  #       "legend": "Dana",
  #       "state": "solved_at",
  #       "conditions": {
  #         "1": {
  #           "source": "assignee_id",
  #           "operator": "is",
  #           "value": {
  #             "0": "6911443343"
  #           }
  #         }
  #       }
  #     },
  #   },
  #   "submit_type": "preview_report",
  #   "commit": "Submit",
  #   "_": ""
  # }
  #
  # The details of sets, conditions, and values can't be modeled with
  # strong(er) parameters as the keys are variable (integer strings) and
  # it doesn't work to try to treat them as arrays.

  sets_type = Parameters.map

  # See app/models/report.rb in #execute
  result_type = Parameters.map(
    legends: Parameters.array(Parameters.map),
    data:    Parameters.map
  )

  # mostly derived from app/models/report.rb in attr_accessible and usage there
  report_type = Parameters.map(
    title:                     Parameters.string,
    is_relative_interval:      Parameters.boolean,
    relative_interval_in_days: Parameters.integer,
    definition:                Parameters.string, # unsure. from view. also serialized
    sets:                      sets_type,
    result:                    result_type,       # from model, serialized hash
    account_id:                Parameters.bigid,  # belongs_to
    author_id:                 Parameters.bigid,  # belongs_to
    last_run_at:               Parameters.datetime,
    from_date:                 Parameters.datetime,
    to_date:                   Parameters.datetime,
    # The following are defined in the model and were observed being sent
    # via DataDog logging during a trial period. They are date components
    # but are manipulated like strings.
    "from_date(1i)":           Parameters.string,
    "from_date(2i)":           Parameters.string,
    "from_date(3i)":           Parameters.string,
    "to_date(1i)":             Parameters.string,
    "to_date(2i)":             Parameters.string,
    "to_date(3i)":             Parameters.string
  )

  allow_parameters :index,
    copy_id: Parameters.bigid,
    report: report_type,
    tab: Parameters.string

  def index
    deny_access unless analytics_access?

    if params[:tab] && can_access_report_tab?(params[:tab])
      case params[:tab]
      when "reports"
        @reports = current_account.reports
      when "export"
        @latest_xml_export      = latest_attachment(XmlExportJob.name)
        @latest_user_xml_export = latest_attachment(UserXmlExportJob.name)
        @latest_csv_export      = current_account.csv_export_records.uploaded.unexpired.first
        @whitelisted_domain     = current_account.settings.export_whitelisted_domain
      end

      render partial: "reports/tabs/#{params[:tab]}"
    end
  end

  allow_parameters :new,
    copy_id: Parameters.bigid,
    report: report_type

  def new
    if params[:copy_id]
      source = current_account.reports.find(params[:copy_id])
      @report.title      = I18n.t('txt.admin.controllers.reports_controler.copy_of_source', source_title: source.title)
      @report.definition = source.definition
    end

    render action: 'edit'
  end

  allow_parameters :create,
    report: report_type,
    sets: sets_type,
    submit_type: Parameters.string # From logging, untested

  def create
    @report.author = current_user
    @report.sets   = params[:sets]

    save_and_render_report
  end

  allow_parameters :edit,
    id: Parameters.string

  def edit
    @report = requested_report
  end

  allow_parameters :update,
    id: Parameters.string,
    report: report_type,
    sets: sets_type,
    submit_type: Parameters.string # From logging, untested

  def update
    @report = requested_report
    @report.attributes = params[:report]
    @report.sets       = params[:sets]

    save_and_render_report
  end

  allow_parameters :data,
    id: Parameters.string

  def data
    render plain: Rails.cache.read(data_key) || raise(ActiveRecord::RecordNotFound)
  end

  allow_parameters :data_key,
    id: Parameters.string

  def data_key
    @data_key ||= "#{current_account.id}/report_data_#{params[:id]}"
  end

  allow_parameters :settings, {}

  def settings
    render partial: "reports/am/settings", formats: [:xml]
  end

  allow_parameters :amline, :all_parameters # handles internally

  def amline
    extra_params = params.keys.to_set - %w[controller action].to_set
    if extra_params.empty?
      send_file("#{Rails.root}/config/amline.swf", filename: "amline.swf", type: "application/x-shockwave-flash", disposition: "inline")
    else
      redirect_to action: 'amline'
    end
  end

  allow_parameters :destroy,
    id: Parameters.string

  def destroy
    requested_report.destroy

    flash[:notice] = I18n.t('txt.admin.controllers.reports_controler.report_deleted_notice', report_title: ERB::Util.html_escape(requested_report.title))
    redirect_to_reports
  end

  allow_parameters :show,
    id: Parameters.string
    # _: Parameters.integer # From logging, ignored

  def show
    processor = Zendesk::Reports::Processor.process(current_account, current_user, requested_report)

    if processor.runs_in_background?
      unless processor.has_earlier_data?
        flash[:notice] = processor.message
        return redirect_to action: "index"
      end
    end

    @report = processor.report
    @token = store_data_in_token(@report)

    respond_to do |format|
      format.html { flash[:notice] = processor.message }
      format.xml  { render xml: @report }
      format.csv do
        title   = @report.title.downcase.gsub(/[^a-z0-9]/, '_').squeeze('_')
        data    = @report.table[:data]
        legends = @report.table[:legends]

        csv_string = CSV.generate(col_sep: ',', headers: true) do |csv|
          csv << ['Date'] + legends.map { |legend| legend[:name] }
          data.keys.sort.each do |key|
            csv << [key.strftime('%Y-%m-%d')] + legends.map { |legend| data[key][legend[:name]] || 0 }
          end
        end

        send_data(csv_string, type: get_csv_content_type, filename: "#{title}.csv")
      end
    end
  end

  allow_parameters :preview,
    report: report_type,
    sets: sets_type,
    submit_type: Parameters.string # From logging, untested

  def preview
    @is_preview = true
    @report.author = current_user
    @report.sets   = params[:sets].to_h.deep_symbolize_keys

    if @report.valid?
      @token = store_data_in_token(@report)
      render partial: 'display_report', layout: false
    else
      render partial: 'preview_errors', layout: false
    end
  end

  allow_parameters :insights,
    copy_id: Parameters.bigid,
    report: report_type

  def insights
    unless analytics_access? && gooddata_integration.present?
      deny_access
      return
    end

    unless gooddata_user.present?
      gooddata_user_provisioning.create_gooddata_user(current_user)
    end

    redirect_to gooddata_integration.portal_sso_url(gooddata_user)
  end

  private

  def initialize_report
    @report = current_account.reports.build(params[:report] || {})
  end

  def latest_attachment(report_name)
    current_account.expirable_attachments.latest(report_name).first
  end

  def save_and_render_report
    if @report.save
      flash[:notice] = I18n.t(
        'txt.admin.controllers.reports_controler.report_created_label',
        report_title: "<a class='title' href='/reports/#{@report.id}'>#{ERB::Util.html_escape(@report.title)}</a>"
      )
      redirect_to_reports
    else
      flash[:error] = flash_error(
        I18n.t('txt.admin.controllers.reports_controler.failed_create_report_notice'),
        @report
      )
      render action: "edit"
    end
  end

  def store_data_in_token(report)
    token = SecureRandom.hex(16)
    xml_data = render_to_string(partial: "reports/am/data", formats: [:xml], locals: { data: report.table, legends: report.table[:legends] })
    Rails.cache.write("#{current_account.id}/report_data_#{token}", xml_data)
    Rails.logger.warn("xml_data written to #{current_account.id}/report_data_#{token}")
    token
  end

  def can_access_report_tab?(tab)
    case tab
    when "overview"         then overview_access?
    when "reports"          then report_tab_access?
    when "forum_analytics"  then false
    when "search_analytics" then false
    when "export"           then export_access?
    else
      raise ActiveRecord::RecordNotFound, "Unknown tab or user trying to load non-tab file"
    end
  end
  helper_method :can_access_report_tab?

  def overview_access?
    reports_access? && current_account.has_classic_reporting?
  end

  def report_tab_access?
    current_account.has_classic_reporting? && reports_access?
  end

  def reports_access?
    current_user.can?(:view, Report)
  end

  def export_access?
    export_accessible?(current_account, current_user)
  end

  def export_accessible?(current_account, current_user)
    policy = ImportExportJobPolicy.new(current_account, current_user)
    policy.any_accessible? && policy.accessible_to_user?
  end

  def analytics_access?
    reports_access? || current_user.can?(:moderate, Entry)
  end

  def redirect_to_reports
    redirect_to reports_path(anchor: "reports")
  end

  def gooddata_integration
    current_account.gooddata_integration
  end

  def gooddata_user
    GooddataUser.for_user(current_user)
  end

  def gooddata_user_provisioning
    Zendesk::Gooddata::UserProvisioning.new(current_account)
  end

  def show_xml_export_option?
    @show_xml_export_option ||= total_ticket_count < XML_EXPORT_ARCHIVED_TICKET_LIMIT
  end

  helper_method :show_xml_export_option?

  def total_ticket_count
    current_account.tickets.count_with_archived
  end
end
