class Settings::CustomersController < Settings::BaseController
  include Settings::CustomerHelper
  include Zendesk::SatisfactionReasons::ControllerSupport
  allow_parameters :allow_email_template_customization?, {}

  around_action :disable_account_settings_kasket, only: [:update_settings, :update_satisfaction]

  ssl_allowed :all

  allow_parameters :show, {}
  def show
    render action: "show"
  end

  account_setting_types = {
      account: {
          is_open: Parameters.boolean,
          domain_whitelist: Parameters.string,
          domain_blacklist: Parameters.string,
          signup_email_text: Parameters.string,
          verify_email_text: Parameters.string,
          is_end_user_profile_visible: Parameters.boolean,
          is_end_user_password_change_visible: Parameters.boolean,
          is_end_user_phone_number_validation_enabled: Parameters.boolean,
          is_signup_required: Parameters.boolean,
          is_welcome_email_when_agent_register_enabled: Parameters.boolean,
          signup_page_text: Parameters.string,
          settings: {
              has_user_tags: Parameters.boolean,
              verification_captcha_enabled: Parameters.boolean,
              multiple_organizations: Parameters.boolean,
              captcha_required: Parameters.boolean,
              list_help_centers_in_account_emails: Parameters.boolean
          }
      }
  }

  allow_parameters :update_settings, **account_setting_types
  def update_settings
    unless @account.allow_email_template_customization?
      params[:account].delete(:signup_email_text)
      params[:account].delete(:verify_email_text)
    end

    domain_blacklist = params[:account][:domain_blacklist]
    domain_whitelist = params[:account][:domain_whitelist]
    max_size = @account.settings.domain_whitelist_blacklist_maximum_length

    if domain_blacklist && domain_blacklist.bytesize > max_size || domain_whitelist && domain_whitelist.bytesize > max_size # Do not allow saving domain blacklists/whitelists which exceed the 1mb object size limit of Memcached.
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.customers_controller.failed_to_updated_customer_settings'), @account)
      show
    elsif @account.update_attributes(params[:account])
      flash[:notice] = I18n.t('txt.admin.controllers.settings.customers_controller.Customer_settings_updated')
      redirect_to action: "show", anchor: "settings"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.customers_controller.failed_to_updated_customer_settings'), @account)
      show
    end
  end

  allow_parameters :update_satisfaction, account: {
    settings: {
      customer_satisfaction: Parameters.boolean,
      csat_reason_code: Parameters.boolean,
      public_customer_satisfaction: Parameters.boolean,
      satisfaction_prediction: Parameters.boolean
    },
    satisfaction_reasons: Parameters.string
  }
  def update_satisfaction
    satisfaction_enabled = params[:account][:settings][:customer_satisfaction] == true
    satisfaction_changed = (params[:account][:settings][:customer_satisfaction] != @account.settings.customer_satisfaction)

    update_satisfaction_reasons if @account.has_csat_reason_code_admin?

    @account.changed_customer_satisfaction = satisfaction_changed

    attribs = params[:account].except(:satisfaction_reasons)

    if @account.update_attributes(attribs)
      current_account.expire_scoped_cache_key(:settings)

      notice = I18n.t('txt.admin.controllers.settings.customers_controller.Satisfaction_settings_updated')

      if satisfaction_enabled && satisfaction_changed
        automation_link = "<a href='/agent/admin/automations'>" + I18n.t('txt.admin.controllers.settings.customers_controller.automation_link_in_key_you_can_customize') + "</a>"
        view_link = "<a href='/agent/admin/views'>" + I18n.t('txt.admin.controllers.settings.customers_controller.view_link_in_key_you_can_customize') + "</a>"
        notice += "<ul><li>".html_safe
        notice += I18n.t('txt.admin.controllers.settings.customers_controller.you_can_customize_the_automation_and_view', automation: automation_link, view: view_link)
        notice += "</li></ul>".html_safe
      end

      flash[:notice] = notice
      redirect_to action: "show", anchor: "satisfaction"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.customers_controller.failed_to_update_satisfaction_settings'), @account)
      show
    end
  end
end
