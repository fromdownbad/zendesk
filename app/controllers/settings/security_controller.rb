module Settings
  class SecurityController < BaseController
    include Zendesk::LotusEmbeddable
    include Settings::SecurityHelper
    include ::AllowedParameters

    ssl_required :show, :update, :enqueue_certificate_job, :disable_zendesk_provisioned_ssl, :ssl_provisioning_status, :transition_to_sni

    skip_before_action :set_account_instance
    after_action :disable_agent_social_login_settings, only: [:update]

    around_action :disable_account_settings_kasket, only: [:update]

    helper_method :saml,
      :jwt,
      :policies,
      :role_settings,
      :agent_login_settings,
      :end_user_login_settings,
      :global_auth_settings,
      :active_cert,
      :pending_cert,
      :security_notifications,
      :account_assumption,
      :any_active_remote_authentications?,
      :multiple_active_remote_authentications?,
      :active_remote_authentications,
      :primary_remote_authentication,
      :remote_authentication_available?,
      :login_allowed_for_role?,
      :initial_ssl_form_state

    delegated = [:remote_authentications, :role_settings, :active_cert, :pending_cert]
    delegate *delegated, to: :current_account
    private *delegated

    allow_parameters :show, {}
    def show
    end

    allow_parameters :update,
      account: {
        is_ssl_enabled: Parameters.boolean,
        set_long_hsts_header_on_host_mapping: Parameters.boolean,
        automatic_certificate_provisioning: Parameters.boolean,
        admins_can_set_user_passwords: Parameters.boolean,
        notifications_recipient: Parameters.string,
        email_agent_when_sensitive_fields_changed: Parameters.boolean,
        ip_ranges: Parameters.string,
        primary_sso: Parameters.integer,
        settings: {
          assumption_duration: Parameters.enum('day', 'week', 'month', 'year', 'always', 'off', 'not_selected', 'already_set'),
          ip_restriction_enabled: Parameters.boolean,
          enable_agent_ip_restrictions: Parameters.boolean,
          enable_ip_mobile_access: Parameters.boolean,
          credit_card_redaction: Parameters.boolean,
          two_factor_enforce: Parameters.boolean,
          agreed_to_audioeye_tos: Parameters.boolean,
          mobile_app_access: Parameters.boolean
        },
        role_settings: {
          agent_security_policy_id: Parameters.integer,
          agent_google_login: Parameters.boolean,
          agent_office_365_login: Parameters.boolean,
          agent_remote_login: Parameters.boolean,
          agent_zendesk_login: Parameters.boolean,
          agent_password_allowed: Parameters.boolean,
          agent_remote_bypass: Parameters.nil | Parameters.enum("0", "1", "2"),
          end_user_security_policy_id: Parameters.integer,
          end_user_google_login: Parameters.boolean,
          end_user_office_365_login: Parameters.boolean,
          end_user_twitter_login: Parameters.boolean,
          end_user_facebook_login: Parameters.boolean,
          end_user_zendesk_login: Parameters.boolean,
          end_user_remote_login: Parameters.boolean,
          end_user_password_allowed: Parameters.boolean
        },
        remote_authentications: {
          saml: {
            id: Parameters.bigid | Parameters.nil_string,
            remote_login_url: Parameters.string,
            remote_logout_url: Parameters.string,
            fingerprint: Parameters.string,
            ip_ranges: Parameters.string,
            is_active: Parameters.boolean
          },
          jwt: {
            id: Parameters.bigid | Parameters.nil_string,
            remote_login_url: Parameters.string,
            remote_logout_url: Parameters.string,
            fingerprint: Parameters.string,
            update_external_ids: Parameters.boolean,
            ip_ranges: Parameters.string,
            next_token: Parameters.string,
            is_active: Parameters.boolean
          }
        }
      },
      hosted_ssl: {
        uploaded_certificate_data: Parameters.file,
        uploaded_key_data: Parameters.file,
        key_data_passphrase: Parameters.string,
        ssl_type: Parameters.enum('sni', 'ip_based'),
        form_state: Parameters.enum('have_a_certificate', 'do_not_have_a_certificate', '')
      },
      challenge: Parameters.string,
      return_to: Parameters.string,
      custom_security_policy: {
        password_history_length: Parameters.integer,
        password_length: Parameters.integer,
        password_complexity: Parameters.integer,
        password_in_mixed_case: Parameters.boolean,
        password_duration: Parameters.integer,
        failed_attempts_allowed: Parameters.integer,
        session_timeout: Parameters.integer,
        max_sequence: Parameters.string,
        disallow_local_part_from_email: Parameters.boolean
      },
      tab: Parameters.string

    def update
      security_policy_update.perform

      if security_policy_update.success?
        flash[:notice] = I18n.t('txt.admin.views.settings.security.update.settings_updated')

        redirect_to action: :show, anchor: params[:tab]
      else
        flash[:error] = flash_error(
          I18n.t('txt.admin.views.settings.security.update.settings_failed_to_update'),
          *security_policy_update.errors
        )

        render :show
      end
    end

    allow_parameters :enqueue_certificate_job, {}
    def enqueue_certificate_job
      if current_account.settings.automatic_certificate_provisioning?
        AcmeCertificateJobStatus.enqueue_with_status!(current_account.id)
      else
        Rails.logger.warn "not enqueuing certificate job since account doesn't have automatic certificate provisioning"
      end

      redirect_to settings_security_url(anchor: "ssl")
    end

    allow_parameters :disable_zendesk_provisioned_ssl, {}
    def disable_zendesk_provisioned_ssl
      current_account.settings.automatic_certificate_provisioning = false
      current_account.settings.save!

      redirect_to settings_security_url(anchor: "ssl")
    end

    allow_parameters :ssl_provisioning_status, {}
    def ssl_provisioning_status
      render json:  { provisioning_status: lets_encrypt_status }
    end

    allow_parameters :transition_to_sni, {}
    def transition_to_sni
      active_cert = current_account.certificates.active.first
      if active_cert && !active_cert.sni_enabled
        Rails.logger.info "Converting certificate #{active_cert.id} to SNI"
        active_cert.convert_to_sni!
        flash[:notice] = I18n.t('txt.admin.controllers.settings.security_controller.ssl.switch_sni_success')
      end

      redirect_to settings_security_url(anchor: "ssl")
    end

    private

    # this will remain until these settings have been disabled for all accounts
    def disable_agent_social_login_settings
      if updating_agent_settings? && security_policy_update.success?
        role_settings.disable_agent_social_login!
      end
    end

    def security_policy_update
      @security_policy_update ||=
        Zendesk::Accounts::Security::SettingsUpdate.create(
          current_user, params, [saml, jwt]
        )
    end

    def updating_agent_settings?
      params[:tab] == 'agents'
    end

    def policies
      @policies ||= current_account.available_security_policies
    end

    def agent_login_settings
      role_settings.attributes.with_indifferent_access.slice(
        :agent_zendesk_login,
        :agent_google_login,
        :agent_office_365_login,
        :agent_remote_login
      )
    end

    def end_user_login_settings
      role_settings.attributes.with_indifferent_access.slice(
        :end_user_zendesk_login,
        :end_user_google_login,
        :end_user_office_365_login,
        :end_user_twitter_login,
        :end_user_facebook_login,
        :end_user_remote_login
      )
    end

    def global_auth_settings
      current_account.fetch_settings(:ip_restriction_enabled)
    end

    def saml
      @saml ||=
        remote_authentications.saml.first || remote_authentications.saml.build(remote_auth_mode_params('saml'))
    end

    def jwt
      @jwt ||=
        remote_authentications.jwt.first || remote_authentications.jwt.build(remote_auth_mode_params('jwt'))
    end

    def remote_auth_mode_params(mode)
      if params[:account] && params[:account][:remote_authentications]
        params[:account][:remote_authentications][mode]
      end
    end

    def remote_authentication_available?
      current_account.remote_authentication_available?
    end

    def login_allowed_for_role?(service, role)
      current_account.login_allowed_for_role?(service, role)
    end

    def security_notifications
      @security_notifications ||=
        Zendesk::Accounts::Security::Notifications.new(current_account)
    end

    def account_assumption
      @account_assumption ||=
        Zendesk::Accounts::Security::AccountAssumption.new(current_account)
    end

    def remote_authentications
      @remote_authentications ||= current_account.remote_authentications
    end

    def any_active_remote_authentications?
      active_remote_authentications.count >= 1
    end

    def multiple_active_remote_authentications?
      active_remote_authentications.count > 1
    end

    def active_remote_authentications
      remote_authentications.select(&:is_active)
    end

    def primary_remote_authentication
      remote_authentications.first
    end

    def initial_ssl_form_state
      if params[:hosted_ssl] && ['have_a_certificate', 'do_not_have_a_certificate'].include?(params[:hosted_ssl][:form_state])
        params[:hosted_ssl][:form_state]
      else
        ''
      end
    end
  end
end
