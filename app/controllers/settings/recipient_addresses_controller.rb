class Settings::RecipientAddressesController < Settings::BaseController
  include ::AllowedParameters

  layout :no_layout_for_xhr
  helper_method :is_assuming_user_from_monitor?

  skip_before_action :authorize_admin
  before_action :check_permissions

  ssl_allowed :all

  RECIPIENT_ADDRESS_PARAMETERS = {
    email: Parameters.string,
    name: Parameters.string,
    brand_id: Parameters.bigid,
    default: Parameters.boolean
  }.freeze

  allow_parameters :new, recipient_address: {
    brand_id: Parameters.bigid
  }
  def new
    if brand_id = params[:recipient_address].try(:[], :brand_id)
      brand = current_account.brands.find(brand_id)
    end
    @recipient_address = current_account.recipient_addresses.build(
      name: (brand || current_account).name,
      brand: brand
    )
  end

  allow_parameters :create, recipient_address: RECIPIENT_ADDRESS_PARAMETERS
  def create
    recipient_address = current_account.recipient_addresses.create(params.require(:recipient_address))
    success = !recipient_address.new_record?
    save_response(success, recipient_address)
  end

  allow_parameters :edit, id: Parameters.bigid
  def edit
    recipient_address
  end

  allow_parameters :update, id: Parameters.bigid, recipient_address: RECIPIENT_ADDRESS_PARAMETERS
  def update
    success = recipient_address.update_attributes(params.require(:recipient_address))
    save_response(success, recipient_address)
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    success = recipient_address.destroy
    save_response(success, recipient_address)
  end

  allow_parameters :preview, recipient_address: RECIPIENT_ADDRESS_PARAMETERS
  def preview
    @recipient_address = current_account.recipient_addresses.new(params.require(:recipient_address))
    render partial: "header_preview"
  end

  allow_parameters :table, {}
  def table
    render partial: "/settings/email/recipient_addresses"
  end

  allow_parameters :show, id: Parameters.bigid
  def show
    render partial: "/settings/email/recipient_address", locals: {address: recipient_address}
  end

  private

  def save_response(success, recipient_address)
    respond_to do |format|
      format.html do
        # everything is done via ajax, so this should never be hit
        default_flash(success)
        redirect_to settings_email_path
      end
      format.json do
        if success
          render json: recipient_address
        else
          render json: Api::V2::ErrorsPresenter.new.present(recipient_address), status: :not_acceptable
        end
      end
    end
  end

  def default_flash(success)
    if success
      flash[:notice] = I18n.t("txt.controllers.flash.notice.saved")
    else
      flash[:error] = I18n.t("txt.controllers.flash.error.saved")
    end
  end

  def recipient_address
    @recipient_address ||= current_account.recipient_addresses.find(params.require(:id))
  end

  def no_layout_for_xhr
    request.xhr? ? false : "agent"
  end

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end
end
