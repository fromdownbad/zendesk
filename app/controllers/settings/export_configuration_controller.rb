class Settings::ExportConfigurationController < Settings::BaseController
  include ::AllowedParameters

  ssl_allowed :all
  before_action :authorize_owner

  around_action :disable_account_settings_kasket, only: [:disable, :update_whitelisted_domain]

  allow_parameters :disable, {}
  def disable
    if update_account_setting(:export_accessible_types, ImportExportJobPolicy::VIEW_JOBS)
      flash[:notice] = I18n.t(
        "txt.admin.controllers.settings.export_configuration_controller.disabled",
        subscription: subscription_link
      )
      redirect_to reports_path
    else
      flash[:error] = I18n.t(
        "txt.admin.controllers.settings.export_configuration_controller.disabling_failed"
      )
      redirect_to reports_path(anchor: "export")
    end
  end

  allow_parameters :enable_request, {}
  def enable_request
    if create_enable_exports_request
      flash[:notice] = I18n.t(
        "txt.admin.controllers.settings.export_configuration_controller.enable_request_success"
      )
    else
      flash[:error] = I18n.t(
        "txt.admin.controllers.settings.export_configuration_controller.enable_request_failure"
      )
    end

    redirect_to settings_account_path
  end

  allow_parameters :update_whitelisted_domain, account: {
    export_whitelisted_domain: Parameters.string
  }
  def update_whitelisted_domain
    if invalid_whitelist_domain
      flash[:error] = I18n.t(
        "txt.admin.controllers.settings.export_configuration_controller.invalid_domains"
      )
    elsif update_account_setting(:export_whitelisted_domain, params[:account][:export_whitelisted_domain])
      flash[:notice] = I18n.t(
        "txt.admin.controllers.settings.export_configuration_controller.updated_whitelisted_domain"
      )
    else
      flash[:error] = I18n.t(
        "txt.admin.controllers.settings.export_configuration_controller.failed_updating_whitelisted_domain"
      )
    end

    redirect_to reports_path(anchor: "export")
  end

  protected

  def create_enable_exports_request
    return false if export_configuration.all_accessible?

    export_configuration_notifier.create_enable_exports_request
  end

  def update_account_setting(key, value)
    return false unless value

    current_account.settings.set(key => value)
    current_account.settings.save
  end

  def export_configuration_notifier
    @export_configuration_notifier ||= Zendesk::Export::ConfigurationNotifier.new(current_account)
  end

  def export_configuration
    @export_configuration ||= Zendesk::Export::Configuration.new(current_account)
  end

  def subscription_link
    view_context.link_to(
      I18n.t("txt.admin.controllers.settings.export_configuration_controller.subscription_link"),
      subscription_settings_path
    )
  end

  def invalid_whitelist_domain
    Socket.gethostbyname(params[:account][:export_whitelisted_domain])
    false
  rescue SocketError
    true
  end
end
