class Settings::BaseController < ApplicationController
  include Zendesk::LotusEmbeddable
  include Zendesk::Accounts::SettingsCacheSupport
  include ::AllowedParameters

  helper_method :lotus?

  before_action :authorize_admin
  before_action :redirect_mobile_browsers
  before_action :set_account_instance
  before_action :register_device_activity, only: [:show]

  layout "agent"
  tab :settings

  allow_parameters :show, {}
  before_action :set_lotus_cookie

  def show
    redirect_to controller: "/settings/account", action: "show"
  end

  protected

  def lotus?
    cookies[:in_lotus] || lotus_referrer? || lotus_update?
  end

  private

  def set_lotus_cookie
    if lotus_referrer? && cookies[:in_lotus].blank?
      cookies[:in_lotus] = {
        value: true,
        domain: request.domain
      }
    end
  end

  def lotus_update?
    params[:lotus_update] == true || params[:lotus] == true
  end

  def lotus_referrer?
    request.referrer =~ /\/agent/
  end

  # Such that we can make form_for(:account)
  def set_account_instance
    @account = current_account
  end

  def redirect_mobile_browsers
    if request.format == :mobile_v2
      render_failure(status: :bad_request, title: "No mobile support", message: "Mobile agents are not supported for the settings pages.")
    end
  end
end
