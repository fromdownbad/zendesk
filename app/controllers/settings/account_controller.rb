require "base64"

class Settings::AccountController < Settings::BaseController
  include Zendesk::PrivateCaching
  include Zendesk::AuditLogControllerSupport
  include ::AllowedParameters

  before_action :authorize_owner, only: [
    :update_benchmark,
    :update_subscription,
    :update_subdomain,
    :update_account_owner,
    :invoice
  ]
  before_action :deny_sandbox, only: [:update_subdomain]
  ssl_required :show, :audits
  ssl_allowed :update_branding, :update_localization, :update_address,
    :update_benchmark, :update_subscription, :invoice,
    :update_account_logos, :update_subdomain, :preview_branding

  around_action :disable_account_settings_kasket, only: [:update_branding, :update_localization]

  helper AuditsHelper

  only_use_private_caches :show

  allow_parameters :audits,
    filter: {
      source_id: Parameters.bigid,
      source_type: Parameters.string,
      actor_id: Parameters.bigid | Parameters.enum('-1'),
      actor_type: Parameters.string,
      ip_address: Parameters.string,
      created_at: Parameters.array(Parameters.string)
    },
    page: Parameters.integer,
    sort_order: Parameters.string,
    sort_by: Parameters.nil_string | Parameters.enum('created_at')

  def audits
    return deny_access unless can_access_audits_tab?
    @events = paginated_cia_events
    render layout: false
  end

  allow_parameters :show, {}
  def show
    @branding               ||= current_account.branding
    @address                ||= current_account.address
    @locales                ||= all_enabled_locales_for_selection
    @hide_flag_to_end_user  ||= current_account.settings.hide_flag_to_end_user
    @benchmark              ||= current_account.survey_response || current_account.create_survey_response
    @subscription           ||= current_account.subscription

    @old_voice_optin_status ||= !!current_account.voice_sub_account.try(:opted_in)

    load_zuora_subscription_and_invoices

    render action: "show"
  end

  allow_parameters :invoice, invoice_id: Parameters.string | Parameters.bigid
  def invoice
    invoice = zuora_client.get_invoice!(params[:invoice_id])
    if invoice
      pdf = decode_invoice(invoice)
      send_data(pdf, filename: invoice.invoice_number + ".pdf", type: "application/pdf")
    else
      head :unprocessable_entity
    end
  end

  allow_parameters :update_branding,
    activate_color_branding: Parameters.boolean,
    header_logo: {
      uploaded_data: Parameters.file
    },
    mobile_logo: {
      uploaded_data: Parameters.file
    },
    account: {
      name: Parameters.string,
      host_mapping: Parameters.string,
      mobile_title: Parameters.string,
      route_id: Parameters.bigid,
      address_attributes: {
        website_url: Parameters.string
      }
    },
    favicon: {
      uploaded_data: Parameters.file
    },
    branding: {
      header_color: Parameters.string,
      page_background_color: Parameters.string,
      tab_background_color: Parameters.string,
      tab_hover_color: Parameters.string,
      text_color: Parameters.string,
      sidebox_color: Parameters.string
    }
  def update_branding
    logo_failures       = update_account_logos
    @account.branding   = Branding.create(params[:branding]) if params[:branding].present?

    route_id = (params[:account] || {}).delete(:route_id)
    if route_id && current_user.is_account_owner?
      @account.route_id = route_id
    end

    @account.attributes = (params[:account] || {})
    update_activate_lotus_branding(params[:activate_color_branding]) if params[:activate_color_branding].present?

    if @account.update_attributes(params[:account]) && logo_failures.empty?
      flash[:notice] = I18n.t('txt.admin.controllers.settings.account_controller.account_settings_updated')
      redirect_to action: "show", anchor: "branding"
    else
      flash[:error] = if !logo_failures.empty?
        if logo_failures.values.find { |e| e.is_a? ::Technoweenie::AttachmentFu::NoBackendsAvailableException }
          flash_error(I18n.t('txt.admin.controllers.settings.account_controller.failed_to_set_one_or_more_logos_temporarily'))
        else
          flash_error(I18n.t('txt.admin.controllers.settings.account_controller.failed_to_set_one_or_more_logos') + " #{logo_failures.keys.join(', ')}")
        end
      else
        flash_error(I18n.t('txt.admin.controllers.settings.account_controller.failed_to_update_account'), @account)
      end
      show
    end
  end

  allow_parameters :preview_branding,
    tabBgColor: Parameters.string,
    tabHoverColor: Parameters.string,
    textColor: Parameters.string,
    borderColor: Parameters.string,
    bgColor: Parameters.string,
    bodyClass: Parameters.string
  def preview_branding
    palette = Zendesk::Branding::Palette.new(OpenStruct.new(header_color: params[:bgColor]))
    lotus_styles = render_to_string partial: "lotus_bootstrap/head/branding", locals: {
      secondary_branding_color: palette.secondary_color,
      navbar_bg_color_rgb:      palette.navbar_bg_color_rgb,
      contrast_color:           palette.contrast_color,
      tab_hover_color:          palette.tab_hover_color,
      header_color:             palette.brand_color
    }
    classic_styles = render_to_string partial: "shared/element_branding", locals: {
      secondary_branding_color: palette.secondary_color,
      header_color:             palette.brand_color
    }
    render json: {
      lotus_styles:   lotus_styles,
      classic_styles: classic_styles
    }
  end

  allow_parameters :update_localization, account: {
    allowed_translation_locale_ids: Parameters.array(Parameters.bigid),
    locale_id: Parameters.bigid,
    hide_flag_to_end_user: Parameters.boolean,
    time_zone: Parameters.string,
    uses_12_hour_clock: Parameters.boolean
  }
  def update_localization
    update_account_localization_attributes(params)

    if @account.save
      notice = I18n.t('txt.admin.controllers.settings.account_controller.Localization_settings_updated')
      notice << @notice_for_restore_default_content if @notice_for_restore_default_content
      flash[:notice] = notice
      redirect_to action: "show", anchor: "localization"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.account_controller.failed_to_update_localization_settings'), @account)
      show
    end
  end

  allow_parameters :update_address, address: {
    name: Parameters.string,
    vat: Parameters.string,
    street: Parameters.string,
    zip: Parameters.string,
    city: Parameters.string,
    state: Parameters.string,
    phone: Parameters.string,
    country_id: Parameters.bigid,
    website_url: Parameters.string
  }
  def update_address
    if current_account.address.update_attributes(address_params)
      update_zuora_address_and_vat
      flash[:notice] = I18n.t('txt.admin.controllers.settings.account_controller.address_updated')
      redirect_to action: "show", anchor: "address"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.account_controller.failed_to_update_address'), current_account.address)
      show
    end
  end

  allow_parameters :update_benchmark, account_survey_response: {
    industry: Parameters.string,
    target_audience: Parameters.string,
    employee_count: Parameters.string,
    customer_count: Parameters.string,
    agent_count: Parameters.string,
    team_count: Parameters.string,
    support_structure: Parameters.string
  }
  def update_benchmark
    if @account.survey_response.update_attributes(params[:account_survey_response])
      flash[:notice] = I18n.t('txt.admin.controllers.settings.account_controller.Benchmark_settings_updated')
      redirect_to action: "show", anchor: "benchmark"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.account_controller.failed_to_update_benchmark_settings'), current_account.survey_response)
      show
    end
  end

  allow_parameters :update_account_owner, account: {
    owner_id: Parameters.bigid
  }
  def update_account_owner
    if params[:account].present?
      @account.attributes = params[:account]
      @account.owner_id   = params[:account][:owner_id]
    end

    success = @account.save

    if success
      flash[:notice] = I18n.t('txt.admin.controllers.settings.account_controller.account_owner_updated')
      redirect_to action: "show", anchor: "owner"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.account_controller.failed_to_update_account_owner'), @account)
      show
    end
  end

  allow_parameters :update_subdomain, account: { subdomain: Parameters.string }
  def update_subdomain
    subdomain = params[:account] && params[:account][:subdomain].to_s.downcase

    if subdomain && Zendesk::RoutingValidations.subdomain_available?(subdomain)
      @account.route.subdomain = subdomain
    end

    if @account.route.changes.any? && @account.route.save
      flash[:notice] = I18n.t('txt.admin.controllers.settings.account_controller.subdomain_updated')
    elsif @account.route.subdomain != subdomain
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.account_controller.subdomain_taken'), @account)
    end
    # Reload new route back into kasket
    Account.on_master do
      @account.route.reload
      brand = @account.brands.find_by_route_id(@account.route)
      brand.route.reload
    end

    redirect_to action: "show", anchor: "branding"
  end

  allow_parameters :update_subscription, {}
  def update_subscription
  end

  allow_parameters :deny_sandbox, {}
  def deny_sandbox
    deny_access if current_account.is_sandbox?
  end

  private

  def all_enabled_locales_for_selection
    (current_account.locales_for_selection + current_account.allowed_translation_locales).uniq
  end

  def update_activate_lotus_branding(activate)
    if !@account.settings.activate_lotus_branding && activate == true
      @account.settings.activate_lotus_branding = true
      @account.save!
    end
  end

  def zuora_subscription
    @zuora_subscription ||= current_account.subscription.try(:zuora_subscription)
  end

  def zuora_client
    @zuora_client ||= ZendeskBillingCore::Zuora::Client.new(zuora_subscription.zuora_account_id)
  end

  def decode_invoice(invoice)
    Base64.decode64(invoice.body)
  end

  def load_zuora_subscription_and_invoices
    return if zuora_managed_and_not_via_partner?
    return unless current_account.zuora_managed?
    @zuora_invoices = zuora_client.get_invoices!
  end

  def zuora_managed_and_not_via_partner?
    current_account.zuora_managed? && !current_account.via_partner?
  end

  def can_access_audits_tab?
    current_user.can_see_audit_log?
  end
  helper_method :can_access_audits_tab?

  def update_account_logos
    failed = {}

    [Favicon, HeaderLogo, AccountLogo, MobileLogo].each do |logo|
      begin
        logo.set_for_account(@account, params[logo.name.underscore])
      rescue StandardError => e
        logger.warn("Failed to set #{logo.name.underscore} for account #{@account.id}: #{e.message}\n#{e.backtrace.join("\n")}")
        failed[logo.human_name] = e
      end
    end

    @account.update_attribute(:updated_at, Time.now.utc)
    failed
  end

  def update_account_localization_attributes(params)
    return unless account_attrs = params[:account].presence

    locale_ids = account_attrs.delete(:allowed_translation_locale_ids) do
      [account_attrs[:locale_id]]
    end.compact

    @account.attributes = account_attrs
    @account.allowed_translation_locale_ids = locale_ids if locale_ids.any?
    @account.settings.hide_flag_to_end_user = account_attrs[:hide_flag_to_end_user]

    if account_attrs[:locale_id].present? && account_attrs[:locale_id].to_s != @account.translation_locale.id.to_s
      @notice_for_restore_default_content = "<ul><li>#{I18n.t('txt.admin.controllers.account_controller.localization.default_content_will_be_translated_within_a_few_minutes')}</li></ul>".html_safe
    end
  end

  def update_zuora_address_and_vat
    return unless current_account.zuora_managed?

    address_modifier = ZendeskBillingCore::Zuora::AccountAddressModifier.new(zuora_subscription)
    address_modifier.update(address: current_account.address, vat: current_account.address.vat)
  end

  def address_params
    params.require(:address).permit(:name, :vat, :street, :zip, :city, :state, :phone, :country_id, :website_url).merge(province: nil)
  end
end
