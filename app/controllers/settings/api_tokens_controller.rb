class Settings::ApiTokensController < Settings::BaseController
  include ::AllowedParameters

  skip_before_action :authorize_admin
  before_action :check_permissions

  ssl_allowed :all

  allow_parameters :create, description: Parameters.string
  def create
    json_response = { json: {}, status: :ok }
    if new_token
      # Creating a token implies enabling token access
      unless current_account.settings.api_token_access?
        current_account.settings.api_token_access = true
        current_account.settings.save
      end

      flash[:notice] = I18n.t('txt.admin.controllers.settings.api_controller.api_token_created')
      json_response[:json][:token] = new_token.temp_full_value
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.api_controller.failure_to_create_api_token'), @account)
      json_response[:json] = { text: I18n.t('txt.admin.controllers.settings.api_controller.failure_to_create_api_token') }
      json_response[:status] = :not_acceptable
    end

    render json_response
  end

  allow_parameters :update, id: Parameters.bigid, description: Parameters.string
  def update
    api_token = current_account.api_tokens.find(params[:id])

    if api_token.update_attributes(params)
      flash[:notice] = I18n.t('txt.admin.controllers.settings.api_controller.appi_settings_updated')
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.api_controller.failure_to_update_appi_settings'), @account)
    end

    redirect_to settings_api_url(anchor: "settings")
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    if token.destroy
      flash[:notice] = I18n.t('txt.admin.controllers.settings.api_controller.appi_settings_updated')
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.api_controller.failure_to_update_appi_settings'), @account)
    end

    redirect_to settings_api_url(anchor: "settings")
  end

  private

  def token
    @token ||= current_account.api_tokens.find(params[:id])
  end

  def new_token
    @new_token ||= current_account.api_tokens.create(description: params[:description])
  end

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end
end
