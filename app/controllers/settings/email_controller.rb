class Settings::EmailController < Settings::BaseController
  include ::AllowedParameters
  allow_parameters :allow_email_template_customization?, {}

  skip_before_action :authorize_admin
  before_action :check_permissions

  around_action :disable_account_settings_kasket, only: [:update]

  ssl_allowed :update
  ssl_required :show

  helper_method :is_assuming_user_from_monitor?

  allow_parameters :show,
    brand_id: Parameters.integer,
    brands_page: Parameters.integer,
    address_page: Parameters.integer

  def show
  end

  allow_parameters :update,
    initial_import: Parameters.boolean,
    account: {
      html_mail_template: Parameters.string,
      text_mail_template: Parameters.string,
      mail_delimiter: Parameters.string,
      is_personalized_reply_enabled: Parameters.boolean,
      settings: {
        no_mail_delimiter: Parameters.boolean,
        simplified_email_threading: Parameters.boolean,
        send_gmail_messages_via_gmail: Parameters.boolean,
        modern_email_template: Parameters.boolean,
        rich_content_in_emails: Parameters.boolean,
        accept_wildcard_emails: Parameters.boolean,
        gmail_actions: Parameters.boolean,
        custom_dkim_domain: Parameters.boolean,
        email_sender_authentication: Parameters.boolean,
        email_template_photos: Parameters.boolean
      }
    }

  def update
    unless @account.allow_email_template_customization?
      params[:account].delete(:html_mail_template)
      params[:account].delete(:text_mail_template)
      params[:account].delete(:mail_delimiter)
    end

    if @account.update_attributes(params[:account])
      flash[:notice] = I18n.t('txt.admin.controllers.settings.email_controller.email_settings_updated')
      redirect_to action: "show", anchor: "settings"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.email_controller.failed_to_update_email_settings'), @account)
      render action: "show"
    end
  end

  private

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end
end
