class Settings::TicketsController < Settings::BaseController
  include ::AllowedParameters

  around_action :disable_account_settings_kasket,
    only: [
      :update_settings,
      :update_ticket_sharing
    ]

  ssl_allowed :all

  allow_parameters :show, agreement_id: Parameters.integer
  def show
    reload_account

    @outbound_agreements    = @account.sharing_agreements.outbound
    @inbound_agreements     = @account.sharing_agreements.inbound
    @agreement              = @account.sharing_agreements.build
    @suspended_notification = @account.suspended_ticket_notification || @account.create_suspended_ticket_notification
    render action: 'show'
  end

  allow_parameters :update_settings,
    ticket_id_sequence: Parameters.string,
    suspended_ticket_notification: {
      frequency: Parameters.integer,
      email_list: Parameters.string
    },
    account: {
      is_first_comment_private_enabled: Parameters.boolean,
      is_comment_public_by_default: Parameters.boolean,
      is_email_comment_public_by_default: Parameters.boolean,
      is_attaching_enabled: Parameters.boolean,
      is_collaboration_enabled: Parameters.boolean,
      is_collaborators_addable_only_by_agents: Parameters.boolean,
      cc_blacklist: Parameters.string,
      cc_subject_template: Parameters.string,
      follower_subject_template: Parameters.string,
      cc_email_template: Parameters.string,
      follower_email_template: Parameters.string,
      ticket_id_sequence: Parameters.string,
      is_side_conversations_email_enabled: Parameters.boolean,
      is_side_conversations_slack_enabled: Parameters.boolean,
      ticket_email_ccs_suspension_threshold: Parameters.integer,
      render_custom_uri_hyperlinks: Parameters.string,
      settings: {
        ticket_threads: Parameters.boolean,
        events_reverse_order: Parameters.boolean,
        markdown_ticket_comments: Parameters.boolean,
        rich_text_comments: Parameters.boolean,
        emoji_autocompletion: Parameters.boolean,
        private_attachments: Parameters.boolean,
        email_attachments: Parameters.boolean,
        ticket_tagging: Parameters.boolean,
        ticket_auto_tagging: Parameters.boolean,
        ticket_tags_via_widget: Parameters.boolean,
        assign_tickets_upon_solve: Parameters.boolean,
        change_assignee_to_general_group: Parameters.boolean,
        ticket_show_empty_views: Parameters.boolean,
        agent_forwardable_emails: Parameters.boolean,
        screencasts_for_tickets: Parameters.boolean,
        bcc_archive_address: Parameters.string,
        collaborators_settable_in_help_center: Parameters.boolean,
        accepted_new_collaboration_tos: Parameters.boolean,
        follower_and_email_cc_collaborations: Parameters.boolean,
        comment_email_ccs_allowed: Parameters.boolean,
        light_agent_email_ccs_allowed: Parameters.boolean,
        ticket_followers_allowed: Parameters.boolean,
        agent_email_ccs_become_followers: Parameters.boolean,
        agent_can_change_requester: Parameters.boolean,
        auto_updated_ccs_followers_rules: Parameters.boolean,
        delete_ticket_metadata_pii: Parameters.boolean,
        ccs_requester_excluded_public_comments: Parameters.boolean,
        third_party_end_user_public_comments: Parameters.boolean,
        side_conversations_tickets: Parameters.boolean,
        # Temp access to enable CTS.
        # Remove after moving the switch to a more specific page.
        custom_statuses_enabled: Parameters.boolean,
        reengagement: Parameters.boolean
      }
    }
  def update_settings
    if params[:ticket_id_sequence].to_i > NiceIdSequence::MAX_ALLOWED_SETTING
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.tickets_controller.ticket_id_out_of_range'))
      return show
    end

    if params[:ticket_id_sequence].to_i > current_account.nice_id_sequence.peek.to_i
      sequence_value = params[:ticket_id_sequence].to_i - 1
      @account.nice_id_sequence.update_attribute(:value, sequence_value)
    end

    notification = @account.suspended_ticket_notification

    if notification.present? && !notification.update_attributes(params[:suspended_ticket_notification])
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.tickets_controller.failed_to_update_ticket_settings'), notification)
      return show
    elsif notification.present? && params[:suspended_ticket_notification] && params[:suspended_ticket_notification]["email_list"].blank? && params[:suspended_ticket_notification]["frequency"] != 0
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.tickets_controller.suspended_ticket_notification_requires_emails'), notification)
      return show
    end

    sanitize_collaboration_settings!
    sanitize_ticket_comments_settings!

    if @account.update_attributes(params[:account])
      flash[:notice] = I18n.t('txt.admin.controllers.settings.tickets_controller.ticket_settings_updated')

      respond_to do |format|
        format.html { redirect_to action: "show", anchor: 'settings' }
        format.json { render json: {}, status: :ok }
      end
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.tickets_controller.failed_to_update_ticket_settings'), @account)
      show
    end
  end

  allow_parameters :update_ticket_sharing, account: {
    settings: {
      reject_sharing_invites: Parameters.boolean
    }
  }
  def update_ticket_sharing
    if @account.update_attributes(params[:account])
      flash[:notice] = I18n.t('txt.admin.controllers.settings.tickets_controller.ticket_sharing_settings_updated')
      redirect_to action: "show", anchor: 'ticket_sharing'
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.tickets_controller.failed_to_update_ticket_sharing_settings'), @account)
      show
    end
  end

  private

  def sanitize_ticket_comments_settings!
    return unless params[:account] && params[:account][:settings]

    params[:account][:settings].except!(:third_party_end_user_public_comments) unless @account.has_third_party_end_user_public_comments_setting?
  end

  def sanitize_collaboration_settings!
    return unless params[:account] && params[:account][:settings]

    if @account.has_email_ccs?
      # The Javascript in app/views/settings/tickets/_settings.js implements the
      # equivalent logic below in the UI. This check ensures consistency across
      # the relevant account settings if the UI is bypassed.
      if params[:account][:settings]['comment_email_ccs_allowed'] == false
        params[:account][:settings]['agent_email_ccs_become_followers'] = false
        params[:account][:settings]['light_agent_email_ccs_allowed'] = false
        params[:account][:settings]['ccs_requester_excluded_public_comments'] = false
      end

      # Updates to the `light_agent_email_ccs_allowed` account setting is only allowed if the
      # account has the `email_ccs_light_agents_v2` arturo enabled
      params[:account][:settings].except!(:light_agent_email_ccs_allowed) unless @account.has_email_ccs_light_agents_v2?
      # Updates to the `ccs_requester_excluded_public_comments` account setting is only allowed if the
      # account has the `email_end_user_comment_privacy_settings` arturo enabled
      params[:account][:settings].except!(:ccs_requester_excluded_public_comments) unless @account.has_email_end_user_comment_privacy_settings?
    else
      params[:account][:settings].except!(
        :ccs_requester_excluded_public_comments,
        :accepted_new_collaboration_tos,
        :agent_can_change_requester,
        :agent_email_ccs_become_followers,
        :auto_updated_ccs_followers_rules,
        :comment_email_ccs_allowed,
        :follower_and_email_cc_collaborations,
        :light_agent_email_ccs_allowed,
        :ticket_followers_allowed
      )
    end
  end

  def reload_account
    @account.settings.reload
    @account.reload
  end
end
