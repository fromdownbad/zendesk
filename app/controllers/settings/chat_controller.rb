class Settings::ChatController < Settings::BaseController
  skip_before_action :authorize_admin
  before_action :check_permissions
  before_action :ensure_property_set_exists
  before_action :ensure_chat_available
  before_action :ensure_voltron_chat_integration_present

  around_action :disable_account_settings_kasket, only: [:update]

  ssl_allowed :all
  allow_parameters :show, {}
  def show
    @app_market_id         = ZopimIntegration.app_market_id(current_account)
    @is_owner              = current_user.is_account_owner?
    @is_sales_assisted     = !!current_account.subscription.zuora_subscription.try(:assisted?)
    @zopim_subscription    = current_account.zopim_subscription
    @zopim_integration     = current_account.zopim_integration
  end

  CHAT_PARAMETERS = {
    settings: {
      chat: Parameters.boolean,
      chat_about_my_ticket: Parameters.boolean
    },
    maximum_chat_requests: Parameters.integer,
    chat_welcome_message: Parameters.string
  }.freeze

  allow_parameters :create, account: CHAT_PARAMETERS
  def create
    update
  end

  allow_parameters :update, account: CHAT_PARAMETERS
  def update
    if current_account.update_attributes(account_params)
      flash[:notice] = I18n.t('txt.admin.controllers.account.chat_settings_controller.updated_chat_settings_label')
    else
      flash[:error] = I18n.t('txt.admin.controllers.account.chat_settings_controller.error_updating_chat_settings_label') + current_account.errors.full_messages.to_sentence.to_s
    end

    redirect_to settings_chat_path
  end

  protected

  def ensure_property_set_exists
    current_account.account_property_set ||= AccountPropertySet.new(account: current_account)
  end

  # @return [Hash] params[:account], but with only the fields
  #         that this controller should be able to update.
  def account_params
    account_params = params[:account] || {}
    {
      settings: account_params[:settings],
      maximum_chat_requests: account_params[:maximum_chat_requests],
      chat_welcome_message: account_params[:chat_welcome_message]
    }
  end

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end

  def ensure_chat_available
    unless current_account.chat_available?
      render_failure(status: :forbidden, message: I18n.t('txt.admin.controllers.base_controller.your_plan_doesnot_provide_this_feature'))
    end
  end

  def ensure_voltron_chat_integration_present
    if current_account.has_voltron_chat? && current_account.zopim_integration.nil?
      render_failure(status: :not_found)
    end
  end
end
