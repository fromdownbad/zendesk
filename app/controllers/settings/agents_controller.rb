class Settings::AgentsController < Settings::BaseController
  include ::AllowedParameters
  include People::RolesHelper

  ssl_allowed :all

  around_action :disable_account_settings_kasket, only: [:update_settings]

  after_action :should_disable_ask, only: [:update_settings]

  allow_parameters :show, {}
  def show
    render action: "show"
  end

  allow_parameters :update_settings, account: {
    signature_template: Parameters.string | Parameters.nil,
    light_agent_permission_set_attributes: {
      permissions: {
        ticket_access: Parameters.string,
        assign_tickets_to_any_group: Parameters.boolean,
        report_access: Parameters.string
      }
    },
    settings: {
      hide_dashboard: Parameters.boolean,
      ticket_delete_for_agents: Parameters.boolean,
      view_deleted_tickets_for_agents: Parameters.boolean,
      agent_forwardable_emails: Parameters.boolean,
      customer_context_as_default: Parameters.boolean,
      show_lotus_to_agents: Parameters.boolean,
      polaris: Parameters.boolean,
      fallback_composer: Parameters.boolean,
      focus_mode: Parameters.boolean
    }
  }
  def update_settings
    light_agent_permission_set_attributes = params[:account].delete(:light_agent_permission_set_attributes)
    handle_polaris_onboarding

    if @account.update_attributes(params[:account])
      if @account.light_agent_permission_set
        @account.light_agent_permission_set.update_attributes! light_agent_permission_set_attributes
      end
      @account.brands.each(&:save) if @account.brands.any?(&:changed?)

      flash[:notice] = I18n.t('txt.admin.controllers.settings.agents_controller.agent_settings_updated')
      redirect_to action: "show", anchor: "settings"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.agents_controller.failed_to_update_agent_settings'), @account)
      show
    end
  end

  private

  def handle_polaris_onboarding
    is_enabling_polaris = params[:account][:settings][:polaris]
    return false unless is_enabling_polaris
    bulk_remove_conflict_onboarding!
    bulk_update_polaris_agent_settings! unless recently_reenabled_polaris?
  end

  def bulk_remove_conflict_onboarding!
    conflicting_onboarding_flags = ['show_zero_state_tour_ticket']

    @account.agents.pluck(:id).each_slice(500) do |id_slice|
      UserSetting.where(
        account_id: @account.id,
        name: conflicting_onboarding_flags,
        user_id: id_slice
      ).update_all(value: '0', updated_at: Time.now)
    end
  end

  def recently_reenabled_polaris?
    account_setting = @account.settings.find_by(name: 'polaris')
    return true unless account_setting

    account_setting.updated_at >= 1.day.ago
  end

  def bulk_update_polaris_agent_settings!
    @account.agents.pluck(:id).each_slice(500) do |id_slice|
      UserSetting.where(
        account_id: @account.id,
        name: 'show_agent_workspace_onboarding',
        user_id: id_slice
      ).update_all(value: '1', updated_at: Time.now)
    end
  end

  # ASK focus mode routing should only be enabled
  # when polaris (Agent Workspace) is also enabled
  #
  # Because of that, when polaris setting is changed to false
  # we should also change focus_mode to false
  def should_disable_ask
    account_setting = @account.settings.polaris?

    unless account_setting
      @account.update_attributes!(settings: { focus_mode: false })
    end
  end
end
