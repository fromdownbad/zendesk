require_dependency 'zendesk/salesforce/oauth/connection_handler'
require_dependency 'zendesk/salesforce/controller_support'

class Settings::ExtensionsController < Settings::BaseController
  include ::AllowedParameters

  before_action :check_capabilities, only: :show
  require_capability :crm_integrations, except: :show

  skip_before_action :authorize_admin
  before_action :check_permissions
  skip_before_action :set_x_xss_protection_header, only: :show

  around_action :disable_account_settings_kasket, only: [:update_salesforce]
  before_action :update_sugar_crm_rate_limit, only: [:update_sugar_crm]

  ssl_allowed :show, :edit_salesforce_object, :edit_salesforce_fields, :salesforce_related_fields, :edit_object_filter, :update_salesforce, :update_sugar_crm, :update_ms_dynamics, :remove_crm
  ssl_required :salesforce_oauth_callback

  allow_parameters :show, {}
  def show
    @apps_requirements_targets = Target.apps_requirements(@account)

    respond_to do |format|
      format.html { render action: "show" }
    end
  end

  allow_parameters :update_salesforce,
    account: {
      settings: {
        use_salesforce_configuration: Parameters.enum("0", "1", nil),
        salesforce_integration: Parameters.boolean
      },
      salesforce_integration_attributes: {
        app_latency: Parameters.enum(*CrmHelper::APP_LATENCY_VALUES),
        show_parent: Parameters.string
      }
    }
  def update_salesforce
    current_configuration = @account.settings.use_salesforce_production

    if @account.update_attributes(params[:account]) &&
      @account.salesforce_integration.update_attributes(params[:account][:salesforce_integration_attributes])
      if current_configuration != @account.settings.use_salesforce_production &&
        @account.salesforce_integration.try(:configuration).present?
        # Salesforce connection changed. We need check the current configuration.
        @account.salesforce_integration.check_for_errors rescue nil
      end
      flash[:notice] = I18n.t('txt.admin.controllers.settings.extensions_controller.Salesforce_settings_updated')
      redirect_to action: "show", anchor: "crm"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.extensions_controller.failed_to_updated_Salesforce_settings'), @account)
      show
    end
  end

  allow_parameters :update_sugar_crm, sugar_crm_integration: {
    url: Parameters.string,
    username: Parameters.string,
    password: Parameters.string,
    active: Parameters.boolean
  }
  def update_sugar_crm
    @crm_integration = @account.sugar_crm_integration || @account.build_sugar_crm_integration
    @crm_integration.attributes = params[:sugar_crm_integration]

    # test connection
    if @crm_integration.valid?
      @crm_integration.fetch_info_for(current_user)
    end

    if @crm_integration.save
      flash[:notice] = I18n.t('txt.admin.controllers.settings.extensions_controller.sugarcrm_configuration_completed')
      redirect_to action: "show", anchor: "crm"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.extensions_controller.failed_to_updated_SugarCRM_settings'), @crm_integration)
      show
    end
  rescue StandardError
    @crm_integration.errors.add(:base, I18n.t('txt.admin.controllers.settings.extensions_controller.problem_when_fetching_data_from_crm'))
    flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.extensions_controller.failed_to_updated_SugarCRM_settings'), @crm_integration)
    show unless performed?
  end

  allow_parameters :update_ms_dynamics, ms_dynamics_integration: {
    type_code: Parameters.integer,
    web_service_address: Parameters.string,
    server_address: Parameters.string,
    additional_domains: Parameters.string,
    domain_name: Parameters.string,
    user_id: Parameters.string,
    password: Parameters.string,
    discovery_uri: Parameters.string,
    organization_name: Parameters.string,
    organization_uri: Parameters.string,
    active: Parameters.boolean
  }
  def update_ms_dynamics
    @crm_integration = @account.ms_dynamics_integration || @account.build_ms_dynamics_integration
    @crm_integration.attributes = params[:ms_dynamics_integration]

    # test connection
    if @crm_integration.valid?
      @crm_integration.test_connection
    end

    if @crm_integration.save
      flash[:notice] = I18n.t('txt.admin.controllers.settings.extensions_controller.ms_dynamics_configuration_completed')
      redirect_to action: "show", anchor: "crm"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.extensions_controller.ms_dynamics_configuration_failed'), @crm_integration)
      show
    end
  rescue StandardError
    @crm_integration.errors.add(:base, I18n.t('txt.admin.controllers.settings.extensions_controller.ms_dynamics_configuration_error'))
    flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.extensions_controller.ms_dynamics_configuration_failed'), @crm_integration)
    show unless performed?
  end

  allow_parameters :remove_crm, {}
  def remove_crm
    @account.sugar_crm_integration.try(:destroy)
    @account.salesforce_integration.try(:destroy)
    @account.ms_dynamics_integration.try(:destroy)
    flash[:notice] = I18n.t('txt.admin.controllers.settings.extensions_controller.crm_removed')
    redirect_to action: "show", anchor: "crm"
  end

  allow_parameters :connect_to_salesforce, salesforce_environment: Parameters.string
  def connect_to_salesforce
    redirect_to salesforce_connection.authorize_url
  end

  allow_parameters :salesforce_oauth_callback, code: Parameters.string
  def salesforce_oauth_callback
    salesforce_connection.callback

    flash[:notice] = I18n.t('txt.admin.controllers.settings.extensions_controller.salesforce_configuration_completed')
    redirect_to action: "show", anchor: "crm"
  rescue Salesforce::Integration::LoginFailed
    flash[:error] = I18n.t('txt.admin.controllers.settings.extensions_controller.failed_to_authenticate_with_salesforce')
    redirect_to action: "show", anchor: "crm"
  end

  allow_parameters :edit_salesforce_object, object: Parameters.string
  def edit_salesforce_object
    @objects = salesforce_support.objects

    render layout: false
  end

  allow_parameters :edit_salesforce_fields, object: Parameters.string
  def edit_salesforce_fields
    @selected_fields                                  = salesforce_support.selected_fields
    @mapping_salesforce_field, @mapping_zendesk_field = salesforce_support.selected_mapping
    @fields                                           = salesforce_support.fields

    render layout: false
  end

  allow_parameters :edit_object_filter, object: Parameters.string, relationship_name: Parameters.string
  def edit_object_filter
    @object_name                  = params[:object]
    @relationship_name            = params[:relationship_name]

    @date_fields, @regular_fields = salesforce_support.available_filter_fields
    @date_field, @regular_field   = salesforce_support.selected_filter_fields
    @limit                        = salesforce_support.relationship_limit

    render layout: false
  end

  allow_parameters :salesforce_related_fields, object: Parameters.string, relationship_name: Parameters.string
  def salesforce_related_fields
    render partial: "salesforce_related_fields", locals: { fields: salesforce_support.related_object_fields }
  end

  allow_parameters :save_object_filter,
    object: Parameters.string,
    relationship_name: Parameters.string,
    limit: Parameters.string,
    filters: Parameters.array(Parameters.string)
  def save_object_filter
    if salesforce_support.save_filter
      head :ok
    else
      render plain: I18n.t("txt.admin.views.settings.extensions._salesforce3.errors.saving_problem"), status: :bad_request
    end
  end

  allow_parameters :save_salesforce_object,
    salesforce_object: Parameters.string,
    fields: Parameters.array(Parameters.string),
    labels: Parameters.string,
    relationships: Parameters.string,
    mapping_salesforce_field: Parameters.string,
    mapping_zendesk_field: Parameters.string,
    mapping_salesforce_field_type: Parameters.string,
    object_label: Parameters.string
  def save_salesforce_object
    if salesforce_support.save_object
      render partial: "salesforce_configuration"
    else
      render plain: I18n.t("txt.admin.views.settings.extensions._salesforce3.errors.saving_problem"), status: :bad_request
    end
  end

  allow_parameters :remove_salesforce_object, object: Parameters.string
  def remove_salesforce_object
    result = salesforce_support.remove_object

    flash[:error] = I18n.t("txt.admin.views.settings.extensions._salesforce3.errors.saving_problem") unless result

    redirect_to action: "show", anchor: "crm"
  end

  allow_parameters :sort_salesforce_objects, salesforce_selected_objects_sort_list: Parameters.array(Parameters.string)
  def sort_salesforce_objects
    salesforce_support.sort_objects

    head :ok
  end

  private

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end

  def salesforce_connection
    @salesforce_connection ||= begin
      Salesforce::Oauth::ConnectionHandler.new(
        account: current_account,
        params: params,
        session: session
      )
    end
  end

  def salesforce_support
    @salesforce_support ||= begin
      Zendesk::Salesforce::ControllerSupport.new(account: current_account, params: params)
    end
  end

  def update_sugar_crm_rate_limit
    Prop.throttle!(:update_sugar_crm, current_user.id)
  end

  def check_capabilities
    deny_access unless current_account.has_targets? || current_account.has_crm_integrations?
  end
end
