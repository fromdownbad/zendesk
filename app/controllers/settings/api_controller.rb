class Settings::ApiController < Settings::BaseController
  include ::AllowedParameters

  skip_before_action :authorize_admin
  before_action :check_permissions
  ssl_allowed :all

  around_action :disable_account_settings_kasket, only: [:update_settings]

  allow_parameters :show, {}
  def show
    @current_api_tokens = current_account.api_tokens
    @clients = current_account.clients.all
    @tokens = current_user.tokens.all

    render action: "show"
  end

  allow_parameters :update_settings, account: {
    settings: {
      api_token_access: Parameters.boolean,
      api_password_access: Parameters.boolean
    }
  }
  def update_settings
    if @account.update_attributes(params[:account])
      flash[:notice] = I18n.t('txt.admin.controllers.settings.api_controller.appi_settings_updated')
      redirect_to action: "show", anchor: "settings"
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.settings.api_controller.failure_to_update_appi_settings'), @account)
      show
    end
  end

  private

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end
end
