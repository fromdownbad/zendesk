class Settings::SlasController < Settings::BaseController
  include ::AllowedParameters

  ssl_allowed :all
  layout false

  skip_before_action :authorize_admin

  before_action :check_permissions
  before_action :feature_enabled?
  before_action :register_device_activity, only: [:show]

  allow_parameters :show, {}
  def show
  end

  private

  def feature_enabled?
    head :not_found unless current_account.has_service_level_agreements?
  end

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::BusinessRules)
  end
end
