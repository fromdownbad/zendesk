class Settings::ChannelsController < Settings::BaseController
  include ::AllowedParameters

  skip_before_action :authorize_admin
  before_action :check_permissions
  ssl_allowed :all

  allow_parameters :show, {}
  def show
  end

  private

  def check_permissions
    grant_access current_user.can?(:edit, Access::Settings::Extensions)
  end
end
