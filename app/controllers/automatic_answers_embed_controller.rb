class AutomaticAnswersEmbedController < ApplicationController
  include ::AllowedParameters
  include ZendeskCrossOrigin::Helper
  include AutomaticAnswers::AuthenticationHelper

  skip_before_action :authenticate_user

  before_action { head :forbidden unless valid_auth_token?(params) }
  before_action :validate_rejection_payload, only: [:reject_article]
  before_action :load_ticket

  allow_cors :fetch_ticket,
    :solve_ticket,
    :reject_article

  MAX_TITLE_LENGTH = 150
  VERIFIED_CSRF_POST_ACTIONS = [
    'solve_ticket',
    'reject_article'
  ].freeze

  allow_parameters :fetch_ticket,
    article_id: Parameters.bigid,
    auth_token: Parameters.string,
    mobile: Parameters.boolean
  def fetch_ticket
    record_deflection_article_clicked(params[:article_id])
    record_stats('ticket_fetched', 'embeddable', params[:mobile])

    render json: {
      ticket: {
        nice_id: @ticket.nice_id,
        title: @ticket.title(MAX_TITLE_LENGTH),
        status_id: @ticket.status_id,
        is_solved_pending: deflection.solved_pending?
      }
    }
  end

  allow_parameters :solve_ticket,
    article_id: Parameters.bigid,
    auth_token: Parameters.string,
    mobile: Parameters.boolean
  def solve_ticket
    ticket_deflector.solve(
      article_id: params[:article_id],
      mobile: params[:mobile]
    )

    render json: { message: 'Ticket solved.' }
  end

  allow_parameters :reject_article,
    article_id: Parameters.bigid,
    auth_token: Parameters.string,
    extra: Parameters.integer,
    reason: Parameters.integer
  def reject_article
    ticket_deflector.process_deflection_rejection(
      article_id: irrelevant_payload[:article_id],
      reason: irrelevant_payload[:reason]
    )

    render json: { message: 'Feedback for this article successfully recorded.' }
  rescue ArgumentError => error
    render json: { message: error.message }, status: :unprocessable_entity
  end

  private

  def load_ticket
    @ticket = current_account.tickets.find_by_nice_id!(decoded_params['ticket_id'])
  end

  def deflection
    @deflection ||= begin
      if decoded_params['deflection_id']
        current_account.ticket_deflections.find(decoded_params['deflection_id'])
      else
        @ticket.ticket_deflection
      end
    end
  end

  def decoded_params
    jwt_token_params(params)
  end

  def record_deflection_article_clicked(article_id)
    deflection_article = deflection.ticket_deflection_articles.where(article_id: article_id).first

    return if deflection_article.nil?

    deflection_article.update_clicked_at(Time.now)
  end

  def ticket_deflector
    @ticket_deflector ||= TicketDeflector.new(
      account: current_account,
      deflection: deflection,
      resolution_channel_id: ViaType.WEB_WIDGET
    )
  end

  def irrelevant_payload
    params.permit(
      article_id: Parameters.integer,
      reason: Parameters.integer,
      auth_token: Parameters.string
    )
  end

  def validate_rejection_payload
    payload = irrelevant_payload
    raise 'Not enough parameters' unless payload[:article_id] && payload[:reason]
  rescue
    head(:unprocessable_entity)
  end

  def record_stats(key, channel, mobile)
    tags = ["subdomain:#{current_account.subdomain}", "channel:#{channel}"]
    tags << "mobile:#{mobile}" unless mobile.nil?

    statsd_client.increment(key, tags: tags)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket_deflection'])
  end

  # Allow anonymous requests to bypass the CSRF token check.
  def verified_request?
    action_name.in?(VERIFIED_CSRF_POST_ACTIONS) || super
  end
end
