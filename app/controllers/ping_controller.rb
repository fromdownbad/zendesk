class PingController < ApplicationController
  include ::AllowedParameters
  skip_before_action :authenticate_user
  skip_around_action :restrict_to_current_account
  skip_before_action :verify_current_account
  skip_before_action :verify_authenticated_session
  skip_before_action :ensure_support_is_active!
  allow_zendesk_mobile_app_access only: [:redirect_to_account] # Inbox
  before_action :validate_state, only: :redirect_to_account
  before_action :verify_internal_request, only: [:shards, :version]

  skip_ssl_requirement only: [:shards, :host, :remote_ip]

  ssl_allowed :redirect_to_account

  allow_parameters :shards,
    shard_id: Parameters.string,
    write: Parameters.string
  def shards
    test_account_access

    if shard_id.nil?
      Rails.cache.fetch("ping/shards/check", expires_in: 30.seconds) do
        ActiveRecord::Base.on_all_shards do
          test_shard_access
        end
        true
      end
    else
      ActiveRecord::Base.on_shard(shard_id) do
        test_shard_access
      end
    end

    respond_to do |format|
      format.json { render json: ['OK'], callback: params[:callback] }
      format.any  { render plain: 'OK' }
    end
  end

  allow_parameters :remote_ip, {}
  def remote_ip
    {
      'HTTP_X_FORWARDED_FOR' => request.env['HTTP_X_FORWARDED_FOR'],
      'REMOTE_ADDR'          => request.env['REMOTE_ADDR'],
      remote_ip:                request.remote_ip,
      trusted_proxies:          Rails.application.config.action_dispatch.trusted_proxies
    }.each do |key, value|
      Rails.logger.info "#{key}: #{value}"
    end
    render plain: request.remote_ip
  end

  allow_parameters :host,
    # Integration tests redirect to here on login with the parameter
    # even though it's not required or used in the code
    flash_digest: Parameters.string
  def host
    render plain: 'ok'
  end

  allow_parameters :version, {}
  def version
    render(json: ApplicationHelper.zendesk_version)
  end

  # Note the looks like state is the only param, but it forwards
  # arbitrary other params, as well.
  allow_parameters :redirect_to_account, :all_parameters
  def redirect_to_account
    subdomain, path, csrf_token = params[:state].split(":")
    separator = path.include?("?") ? "&" : "?"
    path.sub!(/^\//, "")
    params[:csrf_token] = csrf_token if csrf_token
    redirect_to("#{account_url(subdomain)}/#{path}#{separator}#{params.except(:controller, :action, :state).to_query}")
  end

  private

  def validate_state
    params.require(:state)
    subdomain, path = params[:state].split(':')

    unless valid_state(subdomain, path)
      client = Zendesk::StatsD::Client.new(namespace: 'ping_redirect')
      client.increment(current_account.subdomain,
        tags: [
          "subdomain:#{subdomain}",
          "path:#{path}"
        ])
      forbidden!
    end
  end

  def valid_state(subdomain, path)
    (current_account.id == Account.root_account_id ||
    subdomain == current_account.subdomain) &&
    path =~ /^\/?(agent|external_email_credentials\/oauth_callback|access\/oauth\?profile=facebook)|access\/oauth\?profile=twitter|admin\/api\/private\/sunshine\/conversations/
  end

  def shard_id
    if /\d+/.match?(params[:shard_id])
      params[:shard_id].to_i
    end
  end

  def account_url(subdomain)
    account = ActiveRecord::Base.on_slave { Account.find_by_subdomain(subdomain) }
    if account.pod_local?
      account.on_shard { account.url(mapped: false) }
    else
      account.url(protocol: "https", mapped: false)
    end
  end

  def test_write?
    params[:write].present? && params[:write] != "false"
  end

  def test_shard_access
    if test_write?
      # UPDATE `groups` SET `updated_at` = '2018-02-21 18:28:43' WHERE `groups`.`id` = -1
      Group.on_slave.first.touch
    else
      # select_db zd_shardXXX_prod
      ActiveRecord::Base.on_slave.connection
    end
  end

  def test_account_access
    account = Account.on_slave.find(1)
    account.touch if test_write?
  end

  def verify_internal_request
    render_404_page unless internal_request?
  end
end
