class AdminController < ApplicationController
  include AdminHelper
  include ::AllowedParameters

  ASSET_HOST         = 'https://admin-assets.zendesk.com'.freeze
  DEFAULT_CONSUL_URL = 'localhost:8500'.freeze

  MANIFEST_PATH =
    '%{consul_url}/v1/kv/%{namespace}/pod%{pod_id}/manifest.json?raw=true'.
      freeze

  before_action :authorize_admin, only: :routing
  before_action :authorize_agent, only: :rule

  ssl_allowed :all
  layout false

  helper_method(
    :javascript_url,
    :javascript_integrity,
    :stylesheet_url,
    :stylesheet_integrity
  )

  allow_parameters :routing, {}
  def routing
  end

  allow_parameters :rule, {}
  def rule
  end

  private

  def asset_host
    return ASSET_HOST unless action_name == 'routing'

    ENV.fetch('ZENDESK_STATIC_ASSETS_DOMAIN', ASSET_HOST)
  end

  def javascript_url
    "#{asset_host}/#{manifest['js']['path']}"
  end

  def javascript_integrity
    "sha384-#{manifest['js']['fingerprint']}"
  end

  def stylesheet_url
    return unless manifest['css']['path']

    "#{asset_host}/#{manifest['css']['path']}"
  end

  def stylesheet_integrity
    "sha384-#{manifest['css']['fingerprint']}"
  end

  def manifest
    @manifest ||= JSON.parse(Faraday.get(manifest_url).body)
  end

  def manifest_url
    format(
      MANIFEST_PATH,
      consul_url: "http://#{ENV.fetch('CONSUL_HTTP_ADDR', DEFAULT_CONSUL_URL)}",
      namespace:  "#{action_name}-admin",
      pod_id:     Zendesk::Configuration.fetch(:pod_id)
    )
  end
end
