require 'benchmark'

class LotusBootstrapController < ApplicationController
  include ::AllowedParameters
  include ZendeskApi::Controller::Pagination
  include Zendesk::PrivateCaching
  include Zendesk::MobileDeeplink::ControllerMixin
  include Zendesk::ReplicaReads
  include ZopimChatRedirectionMixin
  DEV_SHA = 'develop'.freeze

  layout 'lotus_bootstrap'

  CRASH_ON_ERRORS = %w[cdn_failure translations_failure].freeze

  skip_before_action :authenticate_user

  # CRUFT: This should be removed once we have real URLs
  # https://zendesk.atlassian.net/browse/AI-2489
  before_action :authorize_agent_with_return_to, unless: :show_deeplinking?
  before_action :authorize_version_specification
  before_action :collect_cookie_stats
  before_action :ensure_lotus_access
  before_action :validate_request_url
  before_action :handle_cdn_fallback
  before_action :handle_crash
  before_action :handle_incompatible_browser
  before_action :require_manifest
  before_action :set_development
  before_action :find_activities, only: [:index, :dashboard]

  around_action :time_action

  allow_deeplink only: [:ticket], if: :show_deeplinking?, params: :deeplink_params

  dont_use_any_caches :index, :dashboard, :ticket, :show

  treat_as_read_request :index, :dashboard, :ticket

  ssl_required :index, :show, :dashboard, :ticket, :chat, :talk

  require_capability :chat_product_tray, only: [:chat]
  require_capability :talk_product_tray, only: [:talk]
  before_action :verify_zopim_integration, only: :chat

  helper_method :use_real_lotus_urls?, :statsd_client

  allow_parameters :all,
    ticket_id: Parameters.bigid,
    action: Parameters.string,
    sha: Parameters.string,
    fallback_cdn: Parameters.string,
    fallback_asset_name: Parameters.string,
    translations_asset_fallback_to_api: Parameters.boolean | Parameters.nil_string,
    error_identifier: Parameters.string,
    error_asset_name: Parameters.string,
    error_data: Parameters.string,
    nocache: Parameters.string,
    page: Parameters.integer,
    per_page: Parameters.integer,
    sort_order: Parameters.string,
    sort_by: Parameters.string

  # ### Root Lotus route
  # `GET /agent/`
  #
  # The index action currently routes to `dashboard`.
  # It should always redirect to the current root route
  # defined in the Lotus repository.
  #
  # Any non-index routes (/agent/.+) not defined
  # in routes.rb are served from the show action.
  allow_parameters :index,
    challenge: Parameters.string
  def index
    render 'dashboard', formats: ['html']
  end

  allow_parameters :show,
    route: Parameters.string

  allow_parameters :dashboard,
    fallback_cdn: Parameters.string
  def dashboard
  end

  allow_parameters :ticket,
    fallback_cdn: Parameters.string,
    sha: Parameters.string
  def ticket
    @ticket = find_ticket
    @conversations = paginate(@ticket.conversation_items.order('created_at DESC, id DESC'))
  end

  allow_parameters :talk, {}
  def talk
    @favicon_path = '/images/favicon_talk.ico'
    @standalone_product = 'talk'
  end

  allow_parameters :chat, {}
  def chat
    @favicon_path = '/images/favicon_chat.ico'
    @standalone_product = 'chat'
  end

  private

  def verify_zopim_integration
    if current_account.has_voltron_chat?
      redirect_to NEW_ZOPIM_START_PAGE
    elsif zopim_setup_required?(current_user)
      redirect_to '/zendeskchat/start'
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['lotus', 'bootstrap'])
  end

  # CRUFT: This should be removed once we have real URLs
  # https://zendesk.atlassian.net/browse/AI-2489
  def authorize_agent_with_return_to
    if !current_user_exists?
      render template: 'lotus_bootstrap/login', layout: false
    elsif !current_user.is_agent?
      redirect_or_forbidden
    # NOTE: `find_ticket` will render exception info if agent can't view ticket, so we should not
    # redirect or render in that case.  If we do, we get a DoubleRenderError.
    elsif params[:ticket_id] && current_user.can?(:view, find_ticket) && !current_user.can?(:view_private_content, find_ticket)
      if Arturo.feature_enabled_for?(:help_center_agent_requests, current_account)
        redirect_or_forbidden
      else
        # We shouldn't redirect the agent to Help Center, since they can't see tickets
        # there- Help Center will just redirect back to Support.
        forbidden!
      end
    end
  end

  def redirect_or_forbidden
    if params[:action] == "ticket"
      redirect_to request_url(params[:ticket_id])
    else
      forbidden!
    end
  end

  def authorize_version_specification
    if !Rails.env.development? && (params[:action] == 'versions' || params[:sha].present?)
      authorize_integration_network
    end
  end

  def ensure_lotus_access
    if action_name == 'index' && current_account.has_redirect_from_lotus_according_agent_entitlements? && should_redirect_to_chat?
      redirect_to '/chat'
    end
  end

  def should_redirect_to_chat?
    can_use_chat? && !can_use_support?
  end

  def can_use_chat?
    enabled_products.include?(Zendesk::Accounts::Client::CHAT_PRODUCT.to_sym) && user_entitlements['chat']
  end

  def can_use_support?
    enabled_products.include?(Zendesk::Accounts::Client::SUPPORT_PRODUCT.to_sym) && user_entitlements['support']
  end

  def user_entitlements
    @user_entitlements ||= Zendesk::StaffClient.new(current_account).get_entitlements!(current_user.id)
  end

  def enabled_products
    @enabled_products ||= Zendesk::Accounts::Client.new(current_account).products.map(&:name)
  end

  def validate_request_url
    return if request.host_with_port == current_account.host_name(mapped: false)

    subdomain_host = current_account.host_name(mapped: false)
    protocol = "http#{!Rails.env.development? ? 's' : ''}"
    redirect_to "#{protocol}://#{subdomain_host}#{request.fullpath}"
  end

  # Asset fallback process: If any asset fails to be retrieved from the primary CDN, the page is refreshed (1) and all assets
  # try to retrieve from the fallback CDN. If the fallback CDN fails to return an asset then lotus bootstrap fails.
  #
  # Translation asset fallback process: If any assets fails to be retrieved from the primary CDN, the page is refeshed (1) and
  # all assets try to retrieve from the fallback CDN. If the translation asset fails to retrieve from the fallback CDN the page
  # is refreshed again (2) and the translations are requested from the Rosetta API. On the second refresh, all non-translation
  # assets call the fallback CDN. If any asset fails after the second refresh then lotus bootstrap fails.
  def handle_cdn_fallback
    if fallback_cdn = params[:fallback_cdn]
      asset_name = params[:fallback_asset_name]
      fallback_cdn_provider = current_account.fallback_cdn_provider
      cdn_provider = current_account.cdn_provider

      # When translation asset is falling back to the translation API set the fallback CDN as the last CDN tried.
      if translations_asset_falling_back_to_api?
        cdn_provider = current_account.fallback_cdn_provider
        fallback_cdn = 'translation_api'
      end

      # Log CDN fallbacks so that Ops can track their frequency
      Rails.logger.warn "lotus_bootstrap CDN fallback from #{cdn_provider} to #{fallback_cdn} on account #{current_account.subdomain}/#{current_account.id}, user #{current_user.id}, IP #{request.ip} (failed asset: #{asset_name})"
      statsd_client.increment('cdn.fallback', tags: ["cdn:#{fallback_cdn}", "asset_name:#{asset_name}"])
      statsd_client.increment('cdn.fallback_from', tags: ["cdn:#{cdn_provider}", "fallback:#{fallback_cdn}", "asset_name:#{asset_name}"])
      request.env['zendesk.asset_provider'] = fallback_cdn_provider
    end
  end

  def translations_asset_falling_back_to_api?
    params[:fallback_cdn] && params[:fallback_asset_name] == 'translations' &&
    !params[:translations_asset_fallback_to_api].blank? && current_account.has_translations_fallback_rosetta_api?
  end

  def handle_incompatible_browser
    if params[:error_identifier] == 'incompatible_browser'
      render template: 'lotus_bootstrap/incompatible_browser', layout: false, status: :bad_request
    end
  end

  def handle_crash
    if should_crash?
      asset_name = params[:error_asset_name]
      cdn_provider = crash_cdn_provider

      msg = "Fatal lotus error on #{current_account.subdomain}/#{current_account.id}, user #{current_user.id}"
      log_msg = "#{msg}, IP #{request.ip}: #{params[:error_identifier]}, #{params[:error_data]} (cdn provider: #{cdn_provider}, failed asset: #{asset_name})"

      Rails.logger.warn(log_msg)
      statsd_client.increment('lotus.crash.' + params[:error_identifier], tags: ["cdn:#{cdn_provider}", "asset_name:#{asset_name}"])

      flash[:crashed_already] = true
      render template: 'lotus_bootstrap/error', layout: false, status: :service_unavailable
    end
  end

  def crash_cdn_provider
    if params[:error_identifier] == 'translations_failure' && !current_account.has_translations_from_cdn?
      'n/a'
    else
      current_account.fallback_cdn_provider
    end
  end

  def should_crash?
    params[:error_identifier] &&
      CRASH_ON_ERRORS.include?(params[:error_identifier]) &&
      !flash[:crashed_already]
  end

  def require_manifest
    if asset_manifest.empty?
      Rails.logger.warn "No AssetManifest found. Requested Sha: #{params[:sha] || 'none'}"
      render template: 'lotus_bootstrap/error', layout: false, status: :bad_request
    end
  end

  def set_development
    @local_dev_assets = asset_manifest.develop
  end

  def asset_manifest
    @asset_manifest ||= manifest_manager.find(params[:sha] || :current)
  end
  helper_method :asset_manifest

  def manifest_manager
    @manifest_manager ||= Lotus::ManifestManager.new(current_account, develop: params[:sha] == DEV_SHA, nocache: params[:nocache].present?)
  end
  helper_method :manifest_manager

  def find_ticket
    return @ticket if defined?(@ticket)

    @ticket = current_account.tickets.find_by_nice_id!(params[:ticket_id])

    unless current_user.can?(:view, @ticket)
      message = I18n.t('txt.admin.controllers.tickets_controller.you_do_not_have_access_to_ticket', id: @ticket.nice_id)
      render_failure(
        status: :forbidden,
        title: I18n.t('txt.admin.controllers.tickets_controller.access_denied'),
        message: message
      )
    end

    @ticket
  end

  def find_activities
    @activities ||= paginate(current_user.activities) || []
  end

  def collect_cookie_stats
    cookie_header = request.headers['Cookie'] || ''
    cookie_bytes = cookie_header.length

    statsd_client.gauge('cookies.keys', request.cookies.length)
    statsd_client.gauge('cookies.bytes', cookie_bytes)
  end

  def time_action
    ms = Benchmark.ms do
      yield
    end

    statsd_client.timing(['action', params[:action]], ms)
  end

  def show_deeplinking?
    show_deeplinking_splash? && deeplinking_enabled?
  end

  def deeplinking_enabled?
    (current_account.has_ios_mobile_deeplinking? && is_ios?) ||
    (current_account.has_android_mobile_deeplinking? && is_android?) ||
    (current_account.has_windows_mobile_deeplinking? && is_windows_phone?)
  end

  def deeplink_params
    { ticket_id: params[:ticket_id] }
  end
end
