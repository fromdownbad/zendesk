require 'zendesk/search'

class TagsController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable

  ssl_allowed :all

  before_action :authorize_admin, only: [:index]
  before_action :authorize_agent, except: [:index]
  before_action :register_device_activity, only: [:index]

  tab :manage, except: [:autotag, :autocomplete]
  layout :layout_for_role

  # list tags for tickets and entries
  allow_parameters :index,
    for: Parameters.string
  def index
    @model, @filter = model_and_filter
    @tags = current_account.tag_scores.limit(100).to_a

    respond_to do |format|
      format.html
    end
  end

  # List tickets or topics for given tag(s)
  allow_parameters :show,
    id: Parameters.string,
    for: Parameters.string,
    group_id: Parameters.integer, # from logs, untested & undocumented
    page: Parameters.integer # untested, used by get_page
  def show
    @model, @filter = model_and_filter
    @tag = read_tag_from_params
    @tag_error = Tagging.get_tag_error(@tag)

    find_result_and_output

    respond_to do |format|
      format.html
      format.rss { render action: 'show', formats: [:xml], layout: false }
    end
  end

  allow_parameters :bulk,
    id: Parameters.string,
    background: Parameters.string
  def bulk
    if !request.put? || !current_user.is_admin?
      return render_failure(
        status: :bad_request,
        title: 'Bad request',
        message: 'Request must be PUT with proper permissions'
      )
    end

    tag_name = read_tag_from_params

    job_options = params.merge(
      account_id: current_account.id,
      user_id: current_user.id,
      tag_name: tag_name,
      request: {user_agent: request.user_agent, remote_ip: request.remote_ip}
    )

    if params[:background]
      job_id = TagBulkUpdateJob.enqueue(job_options)
      render json: { id: job_id, status_url: api_v2_job_status_url(job_id, format: "json") }
    else
      job = TagBulkUpdateJob.perform_now(job_options)
      bulk_result(job.status['results'].with_indifferent_access)
    end
  end

  allow_parameters :destroy, id: Parameters.string
  def destroy
    if !request.delete? || !current_user.is_admin?
      return render_failure(
        status: :bad_request,
        title: 'Bad request',
        message: 'Request must be DELETE with proper permissions'
      )
    end

    tag_name = read_tag_from_params

    if current_account.taggings.exists?(tag_name: tag_name)
      ticket_count = 0
      current_account.tickets.not_closed.joins(:taggings).where(taggings: {tag_name: tag_name}).find_each do |ticket|
        ticket.remove_tags = tag_name
        ticket.will_be_saved_by(current_user)
        ticket.save
        ticket_count += 1
      end

      entry_count = 0
      current_account.entries.joins(:taggings).where(taggings: {tag_name: tag_name}).find_each do |entry|
        entry.remove_tags = tag_name
        entry.save
        entry_count += 1
      end

      TagScore.update(current_account, tag_name)

      if ticket_count + entry_count > 0
        message = I18n.t('txt.admin.controllers.tag_controller.tag_removed_from_success', tag_name: ERB::Util.h(tag_name)) + "<ul>"
        message << ("<li>" + I18n.t('txt.admin.controllers.tag_controller.open_tickets', open_tickets: ticket_count, count: ticket_count) + "</li>") if ticket_count > 0
        message << ("<li>" + I18n.t('txt.admin.controllers.tag_controller.topics', entry: entry_count, count: entry_count) + "</li></ul>") if entry_count > 0
        message << "</ul>"
      else
        message = I18n.t('txt.admin.controllers.tag_controller.no_tags_removed_message')
      end

      flash[:notice] = message.html_safe
    else
      flash[:notice] = I18n.t('txt.admin.controllers.tag_controller.no_tickets_found_with_tag', tag_name: tag_name)
    end

    redirect_to action: :index
  end

  allow_parameters :autotag,
    text: Parameters.string,
    target: Parameters.string
  def autotag
    respond_to do |format|
      format.js do
        render :update do |page|
          Thesaurus.autotag(params[:text], current_account).each do |tag|
            page.call("#{params[:target][/[a-z\d\.]*/i]}.addEntry", tag)
          end
        end
      end
    end
  end

  allow_parameters :bulk_result, background: Parameters.string
  def bulk_result(results = nil)
    unless results
      status = Resque::Plugins::Status::Hash.get(params[:background])
      results = status['results'].with_indifferent_access
    end

    ticket_count = results['ticket_count']
    entry_count = results['entry_count']
    tag_name = results['tag_name']

    if ticket_count + entry_count > 0
      message = I18n.t('txt.admin.controllers.tag_controller.tag_removed_from_success', tag_name: ERB::Util.h(tag_name)) + "<ul>".html_safe
      message << ("<li>".html_safe + I18n.t('txt.admin.controllers.tag_controller.open_tickets', open_tickets: ticket_count, count: ticket_count) + "</li>".html_safe) if ticket_count > 0
      message << ("<li>".html_safe + I18n.t('txt.admin.controllers.tag_controller.topics', entry: entry_count, count: entry_count) + "</li></ul>".html_safe) if entry_count > 0
      message << "</ul>".html_safe
    else
      message = results['message'] || I18n.t('txt.admin.controllers.tag_controller.no_tags_removed_message')
    end

    flash[:notice] = message.html_safe

    redirect_to action: :index
  end

  private

  def find_result_and_output
    unless @tag_error.nil?
      return @result = []
    end
    options = {
      skip_parse: true,
      with: {type: Zendesk::Search::Elasticsearch::Search.class_to_type[@model.name]},
      sort_mode: :desc,
      order: :updated_at
    }

    user_query = "tags:#{@tag.mb_chars.downcase}"

    if @model == Ticket
      options[:per_page] = 30
      options[:page] = get_page
      @output = Output.create(columns: [:description, :created, :updated, :status, :priority])
    end

    @result = Zendesk::Search.search(current_user, user_query, options)
  end

  def read_tag_from_params
    tag = CGI.unescape(params[:id].split(' ').first)
    tag.gsub('|||', '/')
  end

  def model_and_filter
    if ['entry', 'topic'].member?(params[:for].to_s.downcase)
      [Entry, 'entries']
    else
      [Ticket, 'tickets']
    end
  end
end
