class PhotosController < ApplicationController
  include ::AllowedParameters

  before_action :authenticate_user
  ssl_allowed :destroy

  allow_parameters :create,
    id: Parameters.bigid,
    photo: Parameters.map(
      uploaded_data: Parameters.string | Parameters.file,
      original_url: Parameters.string
    ),
    user_id: Parameters.bigid

  def create
    user = current_account.users.find_by_id(params[:user_id])
    old_photo = user.photo
    user.reload.set_photo(params[:photo]).save!
    old_photo.destroy if old_photo
    render plain: user.reload.photo.to_json
  rescue ActiveRecord::RecordInvalid => e
    render plain: I18n.t('public.controllers.photos_controller.photo_creation_error', error_message: e.message)
    Rails.logger.warn("photo image create error via API: #{e.message}")
  end

  allow_parameters :destroy,
    id: Parameters.bigid

  def destroy
    photo = Photo.find_by(account_id: current_account.id, id: params[:id])

    if photo && photo.user && current_user.can?(:edit, photo.user)
      photo.destroy

      photo.user.dispatch_event_after_photo_deletion
    end

    head :ok
  end
end
