module AttachmentsControllerMixin
  private

  # don't override statsd_client
  def statsd_client_for_mixin
    @statsd_client_for_mixin ||= Zendesk::StatsD::Client.new(namespace: ["attachments", "controller", "mixin"])
  end

  def via_nginx?
    ['master', 'production', 'staging'].include?(Rails.env)
  end

  def set_cloud_headers(attachment, _force_reauth)
    if attachment.has_s3_storage?
      tags = ["store:s3", "thumbnail:#{attachment.thumbnail?}", "model:#{attachment.class.name}"]
      statsd_client_for_mixin.increment("serve_count", tags: tags)
      set_headers_for_s3(attachment)
    end
  end

  def set_headers_for_s3(attachment) # rubocop:disable Naming/AccessorMethodName
    response.headers['X-Accel-Redirect'] = "/s3/" + attachment.full_filename
    response.headers['X-S3-Storage-Url'] = attachment.header_for_s3
  end

  def send_attachment(attachment, force_reauth = false)
    respond_to do |format|
      format.any(:html, :mobile_v2) do
        begin
          send_attachment_content(attachment, force_reauth)
        rescue RuntimeError
          return render_failure status: :not_found
        end
      end
      format.xml do
        # Android's browser sends us text/xml when it encounters URLs of the form
        # /attachments/token/<token>/?name=foo.png
        if user_agent_is_android_browser? && attachment.image?
          send_attachment_content(attachment, force_reauth)
        else
          render xml: attachment.to_xml
        end
      end
    end
  end

  def send_attachment_content(attachment, force_reauth = false)
    expires_in 30.days
    response.headers['Expires'] = (Time.zone.now + 30.days).httpdate
    disposition = determine_disposition(attachment)

    if attachment.respond_to?(:external_url) && attachment.external_url
      redirect_to attachment.external_url.url
      return
    end

    if via_nginx?
      set_x_accel_redirect(attachment, force_reauth, disposition)
      head :ok, content_type: attachment.content_type
    else
      set_content(attachment, disposition)
      send_file attachment.public_filename, disposition: disposition
    end
  end

  def set_x_accel_redirect(attachment, force_reauth, disposition)
    set_content(attachment, disposition)

    if attachment.has_s3_storage?
      set_cloud_headers(attachment, force_reauth)
    elsif attachment.stored_in?(:fs) && attachment.respond_to?(:fs)
      tags = ["store:fs", "thumbnail:#{attachment.thumbnail?}", "model:#{attachment.class.name}"]
      statsd_client_for_mixin.increment("serve_count", tags: tags) # not possible, want to rule this out
      response.headers['X-Accel-Redirect'] = "/fs" + attachment.fs.public_filename.gsub(Rails.root.to_s, '')
    else
      statsd_client_for_mixin.increment("serve_error")
      raise "Unknown store for attachment #{attachment.id}:#{attachment.stores}"
    end

    Rails.logger.debug("X-Accel-Redirect to: #{response.headers['X-Accel-Redirect']}")
  end

  def get_disposition_header(attachment, disposition)
    filename = attachment.respond_to?(:display_filename) ? attachment.display_filename : attachment.filename
    filename.tr!("\t\r\n", "")
    disposition_headers = [disposition, "filename=\"#{filename}\""]
    unless filename.ascii_only?
      # ZD756115 - add support for http://tools.ietf.org/html/rfc2231 for certain combinations of IE+languages
      disposition_headers << "filename*=utf-8''#{CGI.escape(filename).gsub('+', '%20')}"
    end
    disposition_headers.join("; ")
  end

  def set_content(attachment, disposition)
    response.headers['Content-Type']        = attachment.content_type
    response.headers['Content-Disposition'] = get_disposition_header(attachment, disposition)
    response.headers['Content-Length']      = attachment.size.to_s unless Rails.env.development? # serving files with Content-Length results in 502
  end

  def determine_disposition(attachment)
    browser_supports_inline = [Mime[:png], Mime[:gif], Mime[:jpg], Mime[:jpeg]]
    browser_supports_inline += [Mime[:mpeg], Mime[:mp3]] unless user_agent_is_android_browser?
    browser_supports_inline += [Mime[:mp3], Mime[:wav]] if (current_account || attachment.account).has_play_audio_inline? && attachment.respond_to?(:audio?) && attachment.audio?
    browser_supports_inline.map!(&:to_s).uniq!

    return 'attachment' if attachment.content_type == Mime[:html].to_s # ZD741827
    return 'inline' if browser_supports_inline.include?(attachment.content_type)

    'attachment'
  end

  def user_agent_is_android_browser?
    request.user_agent =~ /\(.*?Android.*\)/
  end

  def redirect_to_attachment_domain(attachment)
    token = Zendesk::Attachments::Token.new(account: current_account, user: current_user, attachment: attachment).encrypt
    redirect_to attachment_domain_host + attachment_token_path(account_id: current_account.id, attachment_token: attachment.token, token: token)
  end

  def attachment_domain_host(pod: nil)
    if Rails.env.development?
      current_account.url(mapped: false, ssl: true)
    elsif Rails.env.test? || Rails.env.production?
      pod ||= Zendesk::Configuration['pod_id']
      "https://p#{pod}.zdusercontent.com"
    else
      # should be like https://pXassets.zd-staging.com
      subdomain = Zendesk::Configuration.dig(:asset_providers, :internal, :subdomain)
      domain = Zendesk::Configuration.dig(:asset_providers, :internal, :domain)
      # My sincere apologies for this hack. At least only for staging.
      subdomain = subdomain.gsub(Zendesk::Configuration['pod_id'].to_s, pod.to_s) if pod
      "https://#{subdomain}.#{domain}"
    end
  end
end
