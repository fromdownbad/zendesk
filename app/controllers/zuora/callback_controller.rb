class Zuora::CallbackController < ActionController::Base
  include ::AllowedParameters
  protect_from_forgery with: :exception

  rescue_from ZBC::Zuora::InvalidConfiguration do
    head :not_found
  end

  before_action :update_synchronizer_request_counter, only: :ping
  before_action :require_zuora_account_id,            only: :ping
  before_action :reject_if_production_environment,    only: :inline_sync
  after_action  :update_credit_card_callback_counter, only: :callback

  # NOTE When the standalone_billing_(new|update)_credit_card Arturo bit is
  # turned-on this action will be routed to the Billing app's /billing/callout#create
  # action.
  allow_parameters :ping, zid: Parameters.string
  def ping
    synchronize_account_with_zuora
    respond_to do |format|
      format.json { head(:ok) }
      format.html do
        render inline: 'Job enqueued, please check back in a few minutes.'
      end
    end
  end

  allow_parameters :inline_sync, zid: Parameters.string
  def inline_sync
    head (!!initiate_inline_sync ? :ok : :not_modified)
  end

  # NOTE: Query-string for this action will have the ff key/value pairs:
  #
  # {
  #   id:                "2c92c0f8385d9f350138775869543830",
  #   tenantId:          "XXXXX",
  #   timestamp:         "1342036904059",
  #   token:             "t3nlakkgjiAMxXTyFY3CRG2W6WWbtIR5",
  #   responseSignature: "OTdlNDJlNWM3YThhNjI1NGU5N2I5Y2VkN2YxMmVkOTk=",
  #   success:           "false",
  #   errorCode:         "Invalid_Security",
  #   errorMessage:      "blacklist token already exists: tenant: XXXXX,
  #                        page: 2c92c0f8385d9f350138775869543830,
  #                        timestamp: 1342036903027,
  #                        token: t3nlakkgjiAMxXTyFY3CRG2W6WWbtIR5,
  #                        sig: YjBmZjUwNWYyNjFjY2I0NDQ5NjRhZWYyZTJhN2IwYTE=",
  #   signature:         "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX="
  # }
  allow_parameters :callback,
    id: Parameters.string,
    tenantId: Parameters.string,
    timestamp: Parameters.string,
    token: Parameters.string,
    responseSignature: Parameters.string,
    success: Parameters.boolean,
    refId: Parameters.string,
    field_passthrough1: Parameters.string,
    field_passthrough2: Parameters.string,
    field_passthrough3: Parameters.string,
    field_passthrough4: Parameters.string,
    field_passthrough5: Parameters.string,
    signature: Parameters.string
  def callback
    @credit_card_form_callout_handler = ZBC::Zuora::CreditCardFormCalloutHandler.
      from_query_string(request.query_string, obfuscate_errors?)
    render layout: false
  end

  private

  # NOTE: This will synchronize the account with Zuora either via a Billing's
  # synchronizer (to handle multi-product participant and chat-standalone
  # accounts) or via Classic's own (to handle sales-assisted accounts and
  # accounts that are not yet participating as multi-product)
  def synchronize_account_with_zuora
    if billing_primary_sync?
      ZBC::Billing::Zuora::Ping.request(zuora_account_id, multi_product: true)
    else
      initiate_multi_product_sync if sync_as_multi_product?
      initiate_pod_aware_sync if pod_aware_sync?
    end
  end

  def require_zuora_account_id
    render status: :bad_request,
           json: {
             error: 'MissingZid',
             description: "'zid' is a required field"
    } unless params[:zid].present?
  end

  def pod_aware_sync?
    return if zendesk_account.blank?
    zendesk_account.has_billing_pod_aware_sync? || !sync_as_multi_product?
  end

  def reject_if_production_environment
    return unless Rails.env.production?
    head :forbidden
  end

  # NOTE: Will initiate a sync via Billing's Zuora ping endpoint. The request
  # will take on the form of:
  #
  #   POST /billing/api/zuora/ping?zid=...&multi_product=true
  #
  # Which forces billing to treat the account associated with the zid as a
  # multi-product account.
  def initiate_multi_product_sync
    key    = multi_product? ? :multi_product : :classic
    params = { key => true }
    ZBC::Billing::Zuora::Ping.request(zuora_account_id, params)
  end

  # NOTE: Will initiate a sync by first jumping to the correct pod (based on the
  # account's subdomain) to ensure that sharded subscription tables are
  # accessible when the sync runs.
  def initiate_pod_aware_sync
    remote_synchronizer_client.synchronize!
  end

  def initiate_inline_sync
    ZBC::Zuora::Synchronizer.synchronize!(zuora_account_id)
  end

  def update_credit_card_callback_counter
    return unless zendesk_account
    status = @credit_card_form_callout_handler.valid? ? 'success' : 'failure'
    client = Zendesk::StatsD::Client.new(namespace: %w[credit_card callbacks])
    tags   = %W[sudbomain:#{customer_subdomain}]
    client.increment("callback_#{status}", tags: tags)
  end

  def update_synchronizer_request_counter
    initiator_id = params[:initiator_id] || 'none'
    client       = Zendesk::StatsD::Client.new(namespace: %w[billing])
    tags         = %W[app:classic initiator_id:#{initiator_id}]
    client.increment('sli.zuora_sync_request', tags: tags)
  end

  # For the ping action we expect that the zid param contains the Zuora Account ID of
  # the target account and we use it to fetch the Zuora::Subscription object to extract
  # the Account object which allow us to determine if the account is participating
  # in standalone_billing_(new|update)_credit_card rollout.
  # URL of the target account
  def zuora_account_id
    @zuora_account_id ||= params.require(:zid)
  end

  # For the callback action we expect that the field_passthrough2 param contains the
  # URL of the target account and we extract the subdomain information from it to
  # fetch the Account object which allow us to determine if the account is participating
  # in standalone_billing_(new|update)_credit_card rollout.
  def customer_subdomain
    @customer_subdomain ||= URI.parse(params[:field_passthrough2]).host.
      split('.').first
  rescue => error
    message = <<-TEXT.squish
      Failed to extract customer subdomain with the following
      error: #{error.message}
    TEXT
    Rails.logger.warn(message)
  end

  def zendesk_account
    @zendesk_account ||= begin
      case action_name
      when 'ping'
        zuora_client.zendesk_account
      when 'callback'
        Account.find_by_subdomain(customer_subdomain)
      else
        fail "Unexpected zendesk_account fetch on action: #{action_name}"
      end
    end
  end

  def sync_as_multi_product?
    return false if salesforce_opportunity?

    if sync_all_self_service_via_billing?
      self_service?
    else
      self_service? && multi_product?
    end
  end

  def multi_product?
    zendesk_account.billing_multi_product_participant?
  end

  # NOTE: Accounts originating from SalesForce will will have a Zuora account
  # record but not have a row in the zuora_subscriptions table
  # (ie: ZBC::Zuora::Subscription) until after the first sync run via
  # ZBC::Zuora::Synchronizer
  def salesforce_opportunity?
    zuora_account.present? && zuora_subscription.blank?
  end

  private *delegate(:zuora_account, :get_sales_model, to: :zuora_client)

  def zuora_client
    @zuora_client ||= ZBC::Zuora::Client.new(zuora_account_id)
  end

  def zuora_subscription
    @zuora_subscription ||= ZBC::Zuora::Subscription.find_by_zuora_account_id(
      zuora_account_id
    )
  end

  # NOTE: We check the sales_model value in Zuora instead of the one in that is
  # stored in the zuora_subscriptions table record -- because an account's
  # sales_model setting is updated in Zuora first before we update the records
  # in Classic's database (ie: zuora_subscriptions and subscriptions tables)
  def self_service?
    get_sales_model.to_s.casecmp(ZBC::States::SalesModel::SELF_SERVICE).zero?
  end

  def remote_synchronizer_client
    @remote_synchronizer_client ||= ZBC::Zuora::RemoteSynchronizerClient.new(
      zuora_account_id,
      zendesk_account.subdomain
    )
  end

  def obfuscate_errors?
    Arturo.feature_enabled_for?(:neuter_cc_errors, zendesk_account)
  end

  def sync_all_self_service_via_billing?
    Arturo.feature_enabled_for?(:billing_sync_all_self_service, zendesk_account)
  end

  def billing_primary_sync?
    zendesk_account.has_billing_primary_sync?
  end
end
