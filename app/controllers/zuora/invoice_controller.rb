class Zuora::InvoiceController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_admin
  before_action :load_zuora_subscription

  allow_parameters :invoices, {}
  def invoices
    invoices = @zuora_client.get_invoices!

    respond_to do |format|
      format.json { render json: invoices }
    end
  end

  allow_parameters :invoice_items, invoice_id: Parameters.bigid
  def invoice_items
    items = @zuora_client.get_invoice_items!(params[:invoice_id])

    respond_to do |format|
      format.json { render json: items }
    end
  end

  protected

  def load_zuora_subscription
    @zuora_subscription = current_account.subscription.zuora_subscription
    @zuora_subscription || render_failure(status: :not_found, title: "Not Found", message: "No invoices found.")
    @zuora_client = ZendeskBillingCore::Zuora::Client.new(@zuora_subscription.zuora_account_id)
  end

  def x_accel_redirect_value
    invoice_id   = params.delete(:invoice_id) if params[:action] == 'invoice_items'
    query_string = params.except(:controller, :action, :format).to_param
    '/billing/api/v2/account/zuora/invoices'.tap do |value|
      value << "/#{invoice_id}" if invoice_id.present?
      value << '.json'
      value << "?#{query_string}" unless query_string.blank?
    end
  end
end
