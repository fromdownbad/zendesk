class Zuora::CreditCardController < ApplicationController
  include ::AllowedParameters
  include ZendeskBillingCore::Zuora::Logging

  before_action :instrument_due_to_referrer
  before_action :verify_zendesk_account
  before_action :configure_zuora_hosted_page
  before_action :verify_encrypted_subdomain
  before_action :throttle_iframe_load, only: :zendesk_iframe

  after_action :count_zuora_hosted_page_load_event, only: :zendesk_iframe

  ssl_allowed :all

  skip_before_action :authenticate_user
  skip_around_action :audit_action, :restrict_to_current_account
  skip_before_action :verify_authenticated_session

  layout "zuora_hosted_page"

  rescue_from Prop::RateLimited do |exception|
    flash.discard
    # Cf. https://github.com/zendesk/zendesk/commit/806f277c0c6e9789fc9c4c671a6a9667ad097c8a
    flash[:error] = flash[:notice] = nil # Check commit comment if you're wondering

    Rails.logger.warn("Throttle triggered: #{exception.handle} for account #{customer_subdomain} at #{Time.now}; #{exception.message}")
    render_failure(status: :forbidden, title: 'Uh oh, something went wrong')
  end

  # if has_standalone_billing_(new|update)_credit_card? then route to /billing/zuora/credit_card/zendesk_iframe
  allow_parameters :zendesk_iframe,
    method: Parameters.string,
    subscription: {
      subdomain: Parameters.string,
      account_id: Parameters.bigid,
      account_url: Parameters.string
    },
    credit_card_encrypted_form: {
      payload: Parameters.string,
      nonce: Parameters.string
    }
  def zendesk_iframe
    render :zuora_hosted_page
    override_x_frame_options("DENY")
  end

  private

  # NOTE: this needs to be a shared key between Classic and Billing. The Zuora
  # API security key for the hosted page is a good candidate, but deploying a
  # new key reserved for this usage (encryption) would be a better choice,
  # however, we are pressed by time to use something readily available.
  SHARED_KEY = ZBC::Zuora::Configuration.page['api_security_key'].freeze

  def configure_zuora_hosted_page
    options = params.slice(:subscription, :coupon_code, :reload)
    options[:request_host] = request.host
    @zuora_hosted_page = ZendeskBillingCore::Zuora::Page.new(options)
  end

  def verify_zendesk_account
    if current_account.subdomain != 'support'
      head(:unprocessable_entity)
      exception = RuntimeError.exception("Zendesk 'support' account required for action.")
      log(exception.class.name, exception.message)
    end
  end

  # NOTE: is this still being used?
  def x_accel_redirect_value
    "/billing/zuora/credit_card/zendesk_iframe?#{request.params.except(:controller, :action, :format).to_param}"
  end

  def customer_subdomain
    @customer_subdomain ||= params.require(:subscription).fetch(:subdomain)
  end

  def count_zuora_hosted_page_load_event
    statsd_client.increment('rendered', tags: ["subdomain:#{customer_subdomain}"])
  end

  def instrument_due_to_referrer
    return if valid_referrer?

    statsd_client.increment(
      'rendered_with_subdomain_mismatch',
      tags: [
        "referrer:#{referrer_subdomain}",
        "subdomain:#{customer_subdomain}"
      ]
    )
  end

  def valid_referrer?
    referrer_url_valid? && subdomain_match?
  end

  def referrer_url_valid?
    request.referrer =~ /\A#{URI::DEFAULT_PARSER.make_regexp('https')}\z/
  end

  def subdomain_match?
    referrer_subdomain == customer_subdomain
  end

  def referrer_subdomain
    referrer_url_valid? ? URI(request.referrer).host.split('.').first : ''
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['credit_card', 'iframe']
    )
  end

  def throttle_iframe_load
    Prop.throttle!(
      :zuora_credit_card_iframe_load_by_account,
      customer_subdomain
    )
  rescue Prop::RateLimited
    return unless customer_account.support_risky?

    statsd_client.increment(
      'rate_limited',
      tags: ["subdomain:#{customer_subdomain}"]
    )

    raise
  end

  def customer_account
    @customer_account ||= Account.find_by_subdomain(customer_subdomain)
  end

  def verify_encrypted_subdomain
    return if decrypted_subdomain.valid?

    statsd_client.increment(
      'decrypted_subdomain_invalid',
      tags: ["subdomain:#{customer_subdomain}"]
    )

    render_failure(status: :forbidden, title: 'Uh oh, something went wrong')
  end

  def decrypted_subdomain
    @decrypted_subdomain ||= ZBC::Zendesk::HostedPage::Decrypt.call(
      shared_key:      SHARED_KEY,
      iv:              Base64.urlsafe_decode64(credit_card_encrypted_params[:nonce]),
      encrypted:       Base64.urlsafe_decode64(credit_card_encrypted_params[:payload]),
      given_subdomain: customer_subdomain
    )
  end

  def credit_card_encrypted_params
    params.require(:credit_card_encrypted_form).permit(:payload, :nonce)
  end

  def account_settings
    @account_settings ||= (monitor_client.account_settings || {})
  end

  def monitor_client
    @monitor_client ||= ZBC::Zuora::MonitorClient.new(customer_subdomain)
  end
end
