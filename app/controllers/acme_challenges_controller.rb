class AcmeChallengesController < ApplicationController
  include ::AllowedParameters

  skip_before_action :authenticate_user
  skip_before_action :ensure_proper_protocol
  skip_before_action :ensure_support_is_active!

  allow_parameters :show, token: Parameters.string

  before_action :require_token
  before_action :disable_cookies

  # This is accessed by Let's Encrypt on the HTTP host mapped domain
  # to verify that we control the domain that we are requesting a certificate for.
  def show
    token.increment!(:accesses)
    render plain: token.challenge_content
  end

  private

  def disable_cookies
    env['rack.session.options'][:skip] = true
  end

  def token
    @token ||= current_account.acme_authorizations.where(challenge_token: params[:token]).first
  end

  def require_token
    unless token
      render plain: 'invalid authorization token', status: :not_found
    end
  end
end
