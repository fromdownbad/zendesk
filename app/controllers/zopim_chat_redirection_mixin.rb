module ZopimChatRedirectionMixin
  ZOPIM_PHASE_ONE_APP_NOT_INSTALLED_OR_ENABLED = 'https://dashboard.zopim.com'.freeze
  ZOPIM_PHASE_TWO_APP_NOT_INSTALLED = 'https://www.zendesk.com/apps/zendesk-chat/'.freeze
  ZOPIM_APP_DISABLED_FOR_ADMIN = '/agent/admin/apps/manage'.freeze

  NO_ZOPIM_ADMIN = '/agent/admin/chat'.freeze
  NO_ZOPIM_AGENT = 'https://www.zendesk.com/live-chat-software'.freeze

  ZOPIM_STANDALONE_PAGE = '/agent/chat'.freeze

  NEW_ZOPIM_START_PAGE = '/chat/start'.freeze

  private

  def zopim_integration
    return @zopim_integration if defined?(@zopim_integration)
    @zopim_integration = current_account.zopim_integration
  end

  def zopim_integration?
    zopim_integration.present?
  end

  # Zendesk apps can have three possible install states:
  # * Not installed (therefore not enabled)
  # * Installed, but disabled
  # * Installed and enabled
  def app_enabled_without_integration?
    return false if zopim_integration?

    new_zopim_integration.app_enabled?
  end

  def app_disabled_without_integration?
    return false if zopim_integration?

    new_zopim_integration.app_installed? && !new_zopim_integration.app_enabled?
  end

  def zopim_setup_required?(user)
    if zopim_integration?
      !zopim_integration.app_enabled?
    else
      !(user.is_admin? && app_enabled_without_integration?)
    end
  end

  def zopim_setup_url(user)
    if zopim_integration?
      case zopim_integration.phase
      when ZopimIntegration::PHASE_I
        ZOPIM_PHASE_ONE_APP_NOT_INSTALLED_OR_ENABLED
      when ZopimIntegration::PHASE_II
        if user.is_admin? && zopim_integration.app_installed?
          ZOPIM_APP_DISABLED_FOR_ADMIN
        else
          ZOPIM_PHASE_TWO_APP_NOT_INSTALLED
        end
      end
    else
      if user.is_admin?
        app_disabled_without_integration? ? ZOPIM_APP_DISABLED_FOR_ADMIN : NO_ZOPIM_ADMIN
      else
        NO_ZOPIM_AGENT
      end
    end
  end

  # This is used to check app status when an account does not actually have
  # a Zopim Integration. It owns methods needed above. We are not actually
  # creating a Zopim Integration here.
  def new_zopim_integration
    @new_zopim_integration ||= ZopimIntegration.new(account: current_account)
  end
end
