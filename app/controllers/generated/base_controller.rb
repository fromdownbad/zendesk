class Generated::BaseController < ApplicationController
  include ::AllowedParameters

  before_action :validate_top_directory

  layout false

  private

  def validate_top_directory
    unless generated_data_path_top_directory_range.include?(params[:top_directory].to_i)
      render_failure(
        status: :not_found,
        title: I18n.t("txt.error_message.controllers.javascripts_controller.invalid_top_directory_parameter"),
        message: I18n.t("txt.error_message.controllers.javascripts_controller.invalid_top_directory_parameter")
      )
    end
  end

  def generated_data_path_top_directory_range
    raise 'implement in subclass'
  end
end
