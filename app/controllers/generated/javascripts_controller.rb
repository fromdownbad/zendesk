class Generated::JavascriptsController < Generated::BaseController
  include Zendesk::Auth::ControllerSessionSupport

  ssl_allowed :locale, :user

  skip_before_action :authenticate_user, :verify_authenticated_session
  skip_before_action :verify_current_account, only: [:locale]
  skip_before_action :ensure_support_is_active!

  before_action :validate_top_directory, only: [:locale]
  after_action :strip_response_cookies, only: :locale

  caches_page :locale, if: :can_cache_page?

  allow_parameters :locale,
    cache_key: Parameters.string,
    id: Parameters.bigid,
    top_directory: Parameters.integer
  def locale
    # If etag is equal "304 Not Modified" will be rendered.
    # If Etag is different (which means locale or locale keys have been updated), the file is rendered.
    if stale?(
      etag: [current_locale.id, current_locale.cache_key],
      public: has_newest_locale_data?,
      last_modified: current_locale.updated_at
    )
      render content_type: Mime[:js]
    end
  end

  allow_parameters :user, {}
  def user
    if Rails.env.development? || stale?(etag: cache_key_for_user)
      respond_to do |format|
        format.json do
          render json: current_user_javascript_serializer.as_json
        end
      end
    end
  end

  allow_parameters :mobile_user, id: Parameters.bigid
  def mobile_user
    if stale?(etag: cache_key_for_user)
      render content_type: Mime[:js]
    end
  end

  private

  def current_locale
    @current_locale ||= begin
      locale = TranslationLocale.find(params[:id].to_i)
      I18n.locale = locale
      locale
    end
  end

  def has_newest_locale_data? # rubocop:disable Naming/PredicateName
    current_locale.cache_key == params[:cache_key].to_s
  end

  def can_cache_page?
    has_newest_locale_data?
  rescue ActiveRecord::RecordNotFound # prevent double render
    false
  end

  def cache_key_for_user
    [
      current_account.cache_key,
      GIT_TIMESTAMPS['app/helpers/js_models_helper.rb'].to_i,
      GIT_TIMESTAMPS['app/controllers/generated/javascripts_controller.rb'].to_i,
      ("business" if current_account.business_hours_active?),
      current_user.is_anonymous_user? ? 'anonymous' : current_user.cache_key,
      current_user.organization.try(:cache_key),
      current_user.password_expires_at,
      current_user.accessible_forums_count,
      current_account.scoped_cache_key(:settings),
      current_account.is_trial?,
      I18n.locale,
      Time.now.to_i / 1.hour,
      form_authenticity_token,
      MonitoredTwitterHandle.where(account_id: current_account.id).all.map(&:cache_key).join(',')
    ].compact.join("-")
  end

  def generated_data_path_top_directory_range
    (-1..(TranslationLocale.maximum(:id) / 100))
  end

  def strip_response_cookies
    disable_session_set_cookie if assets_strip_response_cookies_generated_javascript_locale?
  end

  def assets_strip_response_cookies_generated_javascript_locale?
    # Account can be deleted / current_account nil
    if current_account
      current_account.has_assets_strip_response_cookies_generated_javascript_locale?
    else
      Arturo.feature_enabled_for_pod?(:assets_strip_response_cookies_generated_javascript_locale, Zendesk::Configuration.fetch(:pod_id))
    end
  end
end
