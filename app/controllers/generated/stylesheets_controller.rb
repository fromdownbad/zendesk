class Generated::StylesheetsController < Generated::BaseController
  ssl_allowed :branding, :mobile_branding

  skip_before_action :authenticate_user, :verify_authenticated_session
  skip_before_action :ensure_support_is_active!

  before_action :set_branding
  after_action :strip_response_cookies, only: :branding

  caches_page :branding, :mobile_branding

  allow_parameters :branding,
    account_id: Parameters.bigid,
    top_directory: Parameters.string,
    account_updated_at: Parameters.integer
  def branding
  end

  # This is a complete guess as it's untested and undocumented
  allow_parameters :mobile_branding,
    account_id: Parameters.bigid,
    top_directory: Parameters.string,
    account_updated_at: Parameters.integer
  def mobile_branding
  end

  private

  def set_branding
    if params[:account_id].to_s =~ /^\d+$/ && params[:format] == 'css'
      if params[:account_id].to_i == current_account.id
        @branding = current_account.account_property_set.try('branding') || Branding.new
      else
        head :not_found
      end
    else
      head :not_found
    end
  end

  def generated_data_path_top_directory_range
    # `unscoped` removes deleted_at default_scope to prevents full table scan
    Account.on_slave do
      (-1..(Account.unscoped.maximum(:id) / 100))
    end
  end

  # Prevents SharedSession::Middleware::User from setting a session cookie:
  # https://github.com/zendesk/zendesk_shared_session/blob/affbf32cbf298b6862e99147cf5b23b1d6098824/lib/zendesk_shared_session/middleware/user.rb#L57-L60
  # Always cached in the Proxy layer (see `force_cache_control: public`)
  # https://github.com/zendesk/chef_zendesk_nginx/blob/74d192213233f13551e5ceeacc356df5cdcd6268/attributes/proxy_endpoints.rb#L285-L303
  def strip_response_cookies
    if current_account.has_assets_strip_response_cookies_generated_style_branding?
      Rails.logger.warn 'Stripping Set-Cookie response header for /generated/style/branding asset'
      request.env['rack.session.options'][:skip] = true
    end
  end
end
