class Cms::SearchController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable
  include Zendesk::Search::Responder

  before_action :check_permissions
  layout 'agent'
  tab :manage

  allow_parameters :index,
    query: Parameters.string,
    page: Parameters.integer # untested, used by get_page
  def index
    query.execute(current_user)
    respond_with_search_result
  end

  protected

  def query
    @query ||= Zendesk::Search::Query.new(params[:query]).tap do |query|
      query.endpoint = 'cms'
      query.type     = 'cms'
      query.page     = get_page
      query.per_page = 25
      query.order    = [:created_at]
    end
  end

  private

  def check_permissions
    grant_access(
      current_user.is_admin? ||
        current_user.permission_set.present? &&
          current_user.permission_set.permissions.manage_dynamic_content?
    )
  end
end
