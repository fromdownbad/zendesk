class Cms::BaseController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable

  before_action :restrict_account_access
  before_action :restrict_user_access

  def self.variant_parameters
    {
      is_fallback: Parameters.boolean,
      value: Parameters.string,
      translation_locale_id: Parameters.bigid | Parameters.nil_string,
      active: Parameters.anything
    }
  end

  private

  def restrict_account_access
    return render_failure(
      status: :forbidden,
      title: I18n.t('txt.admin.controllers.base_controller.access_denied_label'),
      message: I18n.t('txt.admin.controllers.base_controller.your_plan_doesnot_provide_this_feature')
    ) unless current_account.has_cms?
  end

  # TODO: move to CanCan.
  def restrict_user_access
    return render_failure(
      status: :forbidden,
      title: I18n.t('txt.admin.controllers.base_controller.access_denied_label'),
      message: I18n.t('txt.admin.controllers.base_controller.dont_have_access_to_this_page')
    ) unless current_user.can?(:manage, Cms::Text)
  end
end
