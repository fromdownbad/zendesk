class Cms::TextsController < Cms::BaseController
  before_action :get_filter_options, only: :index
  before_action :category_load, only: :index
  before_action :register_device_activity, only: [:index]

  ssl_allowed :all

  cms_text_parameters = {
    name: Parameters.string,
    fallback_attributes: variant_parameters,
    # The following were taken from the model but not tested
    identifier: Parameters.anything,
    account: Parameters.anything,
    variants_attributes: variant_parameters
  }

  allow_parameters :index,
    category: Parameters.string, # From before_action, untested
    direction: Parameters.string,
    filter: Parameters.string, # From code, untested
    sort: Parameters.string,
    page: Parameters.integer # untested, used by get_page
  def index
    scope = Cms::Text.for_account(current_account.id)
    @tips = []
    scope = scope.in_category(@category_text_ids) if @category_text_ids
    scope = scope.outdated if params[:filter] == "outdated"

    @cms_texts = scope.ordered(params[:sort], params[:direction]).paginate(per_page: 25, page: get_page)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  allow_parameters :show, id: Parameters.bigid
  def show
    @cms_text = Cms::Text.where(id: params[:id], account_id: current_account.id).first
    @tips = []
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  allow_parameters :new, {}
  def new
    @cms_text = Cms::Text.new(account_id: current_account.id)
    @fallback ||= @cms_text.build_fallback
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  allow_parameters :edit, id: Parameters.bigid
  def edit
    @cms_text = Cms::Text.where(id: params[:id], account_id: current_account.id).first
  end

  allow_parameters :create,
    cms_text: cms_text_parameters
  def create
    @cms_text = Cms::Text.setup(params, current_account)
    respond_to do |format|
      if @cms_text.save
        DC::Synchronizer.synchronize_snippet(@cms_text)
        flash[:notice] = I18n.t('txt.admin.controllers.cms.texts_controller.Dynamic_content_item_created')
        format.html { redirect_to(@cms_text) }
      else
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.texts_controller.Failed_to_create_dynamic_content'), @cms_text)
        format.html { render action: "new" }
      end
    end
  end

  allow_parameters :update,
    id: Parameters.bigid,
    cms_text: cms_text_parameters
  def update
    @cms_text = Cms::Text.where(id: params[:id], account_id: current_account.id).first

    respond_to do |format|
      if @cms_text.update_attributes(params[:cms_text])
        DC::Synchronizer.synchronize_snippet(@cms_text)
        flash[:notice] = I18n.t('txt.admin.controllers.cms.texts_controller.Dynamic_content_item_updated')
        format.html { redirect_to(@cms_text) }
      else
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.texts_controller.Failed_to_update_dynamic_content'), @cms_text)
        format.html { render action: "edit" }
      end
    end
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    @cms_text = Cms::Text.where(id: params[:id], account_id: current_account.id).first
    @cms_text.destroy

    respond_to do |format|
      if current_account.cms_texts.find_by_id(@cms_text.id)
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.texts_controller.Failed_to_delete_dynamic_content'), @cms_text)
        format.html { redirect_to(@cms_text) }
      else
        DC::Synchronizer.delete_snippet(@cms_text)
        flash[:notice] = I18n.t('txt.admin.controllers.cms.texts_controller.Dynamic_content_item_was_deleted')
        format.html { redirect_to(cms_texts_url) }
      end
    end
  end

  # By code inspection, untested
  allow_parameters :export, {}
  def export
    respond_to do |format|
      @latest_csv = latest
      format.html
    end
  end

  # By code inspection, untested
  allow_parameters :export_csv,
    raw_data: Parameters.anything,
    uploaded_data: Parameters.anything
  def export_csv
    Zendesk::Cms::ExportJob.enqueue(current_account.id, current_user.id)
    flash[:notice] = Zendesk::Cms::ExportJob.settings[:enqueued]
    redirect_to cms_texts_url
  rescue Resque::ThrottledError
    message = Zendesk::Cms::ExportJob.settings[:throttled] || I18n.t('txt.admin.controllers.cms.texts_controller.download_throttled')
    message += " <a class='title' href='#{latest.url(token: true)}'>#{Zendesk::Cms::ExportJob.settings[:latest]}</a>".html_safe if latest
    flash[:error] = message
    redirect_to cms_texts_url
  end

  # By code inspection, untested
  allow_parameters :import_data,
    raw_data: Parameters.anything,
    uploaded_data: Parameters.anything
  def import_data
    csv_data = (csv_file_upload || form_upload).to_s
    if csv_data.blank?
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.texts_controller.You_must_select_a_csv'))
      redirect_to import_cms_texts_url
      return
    end

    token = current_account.upload_tokens.create(source: current_user)
    attach = token.add_raw_attachment(csv_data, "cms_import.csv")

    if attach.size > 2.megabytes
      message = I18n.t('txt.admin.controllers.cms.texts_controller.Your_import_file_was_rejected')
      flash[:error] = flash_error(message)
      return
    end

    if attach.save
      Zendesk::Cms::ImportJob.enqueue(current_account.id, current_user.id, token.value)
      flash[:notice] = I18n.t('txt.admin.controllers.cms.texts_controller.Your_job_has_been_submitted')
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.texts_controller.There_was_an_error_creating'))
    end

    redirect_to import_cms_texts_url
  end

  private

  def latest
    current_account.expirable_attachments.latest(Zendesk::Cms::ExportJob.name).first
  end

  def csv_file_upload
    if params[:uploaded_data].blank?
      nil
    else
      params[:uploaded_data].rewind
      params[:uploaded_data].read
    end
  end

  def form_upload
    params[:raw_data].blank? ? nil : params[:raw_data]
  end

  def get_filter_options # rubocop:disable Naming/AccessorMethodName
    @select_options = [
      [I18n.t('txt.admin.controllers.cms.texts_controller.All'), ""],
      [I18n.t('txt.admin.controllers.cms.texts_controller.Out_of_Date'), "outdated"]
    ]
  end

  def category_load
    if params[:category].present?
      category_stats = Cms::Text.category_stats(current_account)
      @category_text_ids = category_stats[params[:category]] ? category_stats[params[:category]] : nil
    end
  end
end
