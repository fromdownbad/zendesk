class Cms::VariantsController < Cms::BaseController
  before_action :load_cms_text

  ssl_allowed :all

  allow_parameters :show,
    cms_text_id: Parameters.bigid,
    id: Parameters.bigid
  def show
    redirect_to action: :edit
  end

  allow_parameters :new, cms_text_id: Parameters.bigid
  def new
    @cms_variant = new_variant

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  allow_parameters :edit,
    cms_text_id: Parameters.bigid,
    id: Parameters.bigid
  def edit
    @cms_variant = @cms_text.variants.find(params[:id])
  end

  allow_parameters :create,
    cms_text_id: Parameters.bigid,
    cms_variant: variant_parameters
  def create
    @cms_variant = new_variant params[:cms_variant]
    respond_to do |format|
      if @cms_variant.save && @cms_variant.text.touch_without_callbacks
        DC::Synchronizer.synchronize_snippet(@cms_text)
        flash[:notice] = I18n.t('txt.admin.controllers.cms.variants_controller.The_variant_was_created')
        format.html { redirect_to(cms_text_path(@cms_text)) }
      else
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.variants_controller.Failed_to_save_variant'), @cms_variant)
        format.html { render action: "new" }
      end
    end
  end

  allow_parameters :update,
    cms_text_id: Parameters.bigid,
    id: Parameters.bigid,
    variant: variant_parameters,
    cms_variant: variant_parameters
  def update
    @cms_variant = @cms_text.variants.find(params[:id])
    respond_to do |format|
      if @cms_variant.update_attributes(params[:cms_variant]) && @cms_variant.touch_without_callbacks && @cms_variant.text.touch_without_callbacks
        DC::Synchronizer.synchronize_snippet(@cms_text)
        flash[:notice] = I18n.t('txt.admin.controllers.cms.variants_controller.The_variant_was_updated')
        format.html { redirect_to(cms_text_path(@cms_text)) }
      else
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.variants_controller.Failed_to_save_variant'), @cms_variant)
        format.html { render action: "edit" }
      end
    end
  end

  allow_parameters :destroy,
    cms_text_id: Parameters.bigid,
    id: Parameters.bigid
  def destroy
    @cms_variant = @cms_text.variants.find(params[:id])
    if @cms_variant.destroy
      respond_to do |format|
        format.html { redirect_to(cms_text_path(@cms_text)) }
      end
    else
      respond_to do |format|
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.cms.variants_controller.failed_to_delete_variant'), @cms_variant)
        format.html { redirect_to(cms_text_path(@cms_text)) }
      end
    end
  end

  private

  def new_variant(options = {})
    variant = Cms::Variant.new(options)
    variant.text = @cms_text
    variant
  end

  def load_cms_text
    @cms_text = Cms::Text.where(id: params[:cms_text_id], account_id: current_account.id).first
  end
end
