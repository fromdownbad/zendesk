class HomeController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Home::ControllerSupport
  include Zendesk::SharedControllerSupport
  include HelpCenterRedirection

  deprecate_controller with_feature: :deprecate_home_controller

  allow_anonymous_access_to_public_forums

  # This method is a helper method such that we do not need to lookup entries that are only used inside a cache block
  helper_method :find_pinned_entries

  layout :layout_for_role

  cache_index_action

  allow_parameters :index,
    force_classic_layout: Parameters.string,  # config/initializers/schmobile.rb
    is_mobile:            Parameters.string,  # same
    show_version:         Parameters.boolean, # application_controller.rb
    challenge:            Parameters.string   # test/integration/authentication_test.rb

  # We are on the "home" page for an account, ie. /portal or /entries
  def index
    set_source_page
    self.tab    = :home
    @forums     = current_user.accessible_forums
    @categories = current_account.categories.all

    respond_to do |format|
      format.html
      format.rss { render action: 'index', formats: [:xml], layout: false }
    end
  end

  private

  def authenticate_user
    if should_redirect_to_help_center?
      redirect_to help_center_root_path
    else
      super
    end
  end
end
