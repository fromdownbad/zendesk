class People::OrganizationsController < ApplicationController
  layout 'agent'
  tab :manage

  before_action :authorize_read_write_access, except: [:index, :show, :autocomplete, :jq_autocomplete, :update_notes]
  before_action :authorize_agent, only: [:index, :show, :autocomplete, :jq_autocomplete]
  before_action :authorize_unrestricted_agent, only: [:update_notes]
  require_capability :organizations, only: [:create]

  before_action :find_organization, only: [:show, :edit, :update, :destroy, :update_notes, :toggle_suspend]

  rescue_from Organization::WriteInProgressError do |e|
    render json: { error: "WriteInProgressError", description: e.message }, status: :unprocessable_entity
  end

  include ::AllowedParameters
  include Zendesk::LotusEmbeddable
  include Zendesk::SharedControllerSupport
  include Zendesk::Users::ControllerSupport

  organization_parameters = {
    account: Parameters.anything,
    additional_tags: Parameters.string,
    default: Parameters.string,
    details: Parameters.anything,
    external_id: Parameters.anything,
    shared_comments: Parameters.anything,
    is_shared_comments: Parameters.anything,
    name: Parameters.string,
    notes: Parameters.string,
    shared_tickets: Parameters.anything,
    is_shared: Parameters.anything,
    group: Parameters.anything,
    group_id: Parameters.integer,
    tag_restriction_mode: Parameters.anything,
    set_tags: Parameters.anything,
    tags: Parameters.anything,
    current_tags: Parameters.anything,
    domain_names: Parameters.anything,
    remove_tags: Parameters.anything,
  }

  ssl_allowed :all

  allow_parameters :new, {}
  def new
    @organization = Organization.new
    render action: :edit
  end

  allow_parameters :index,
    page: Parameters.integer
  def index
    return deny_access unless current_user.can?(:list, Organization)
    @result = current_account.organizations.paginate(page: get_page)

    respond_to do |format|
      format.html { render 'people/search/index' }
    end
  end

  allow_parameters :show,
    id: Parameters.bigid,
    group_id: Parameters.integer,
    page: Parameters.integer,
    select: Parameters.string,
    desc: Parameters.integer, # desc, order, sort_by, and sort_order are used in generic_ticket_list
    order: Parameters.string,
    sort_by: Parameters.string,
    sort_order: Parameters.string
  def show
    return deny_access unless current_user.can?(:view, @organization)

    @allow_user_bulk_update = true
    @ticket_count, @user_count = ActiveRecord::Base.on_slave do
      [
        @organization.tickets.where(account_id: current_account.id).for_user(current_user).count_with_archived,
        @organization.users.in_account(current_account.id).count
      ]
    end

    respond_to do |format|
      format.html do
        if params[:select] == 'tickets'
          @tickets, @output = ActiveRecord::Base.on_slave do
            output, select, joins, order = Rule.generic_ticket_list(params)
            tickets = @organization.tickets.where(account_id: current_account.id).for_user(current_user).archived_paginate(page: get_page, select: select, joins: joins, order: order)
            [tickets, output]
          end
        else
          @users = ActiveRecord::Base.on_slave do
            @organization.users.in_account(current_account.id).paginate(page: get_page)
          end
        end
      end
    end
  end

  allow_parameters :edit, id: Parameters.bigid
  def edit
    respond_to { |format| format.html }
  end

  allow_parameters :create,
    organization: organization_parameters
  def create
    @organization = current_account.organizations.new
    @organization.attributes = org_params_with_domain_names

    if @organization.save
      respond_to do |format|
        format.html do
          flash[:notice] = I18n.t('txt.admin.controllers.people.organization.create_success_notice', organization_name: "<a class='title' href='#{organization_path(@organization)}'>#{ERB::Util.h(@organization.name)}</a>")
          redirect_to action: 'index'
        end
      end
    else
      respond_to do |format|
        format.html do
          flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.organization.create_failure_notice'), @organization)
          render action: 'edit'
        end
      end
    end
  end

  allow_parameters :update,
    id: Parameters.bigid,
    organization: organization_parameters
  def update
    @organization.attributes = org_params_with_domain_names

    if @organization.save
      respond_to do |format|
        format.html do
          flash[:notice] = I18n.t('txt.admin.controllers.people.organization.update_success_notice', organization_name: "<a class='title' href='#{organization_path(@organization)}'>#{ERB::Util.h(@organization.name)}</a>")
          redirect_to action: 'index'
        end
      end
      return
    else
      respond_to do |format|
        format.html do
          flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.organization.update_failure_notice'), @organization)
          render action: 'edit'
        end
      end
    end
  end

  allow_parameters :update_notes,
    id: Parameters.bigid,
    field: Parameters.string,
    organization: {
      notes: Parameters.string
    }
  def update_notes
    @organization.update_attribute(:notes, params[:organization][:notes])
    respond_to do |format|
      format.js { render partial: 'shared/notes_updated', locals: { model: @organization, field: params[:field] }, layout: false }
    end
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    render(text: "400 Bad request", status: :bad_request) && return unless request.delete?
    if @organization.destroy
      respond_to do |format|
        format.html do
          flash[:notice] = I18n.t('txt.admin.controllers.people.organization.delete_success_notice', organization_name: ERB::Util.h(@organization.name))
          redirect_to action: 'index'
        end
      end
    else
      respond_to do |format|
        format.html do
          flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.organization.delete_failure_notice'), @organization)
          redirect_to action: 'index'
        end
      end
    end
  end

  allow_parameters :autocomplete, name: Parameters.string
  def autocomplete
    render json: organizations_like(params[:name]).map(&:name)
  end

  allow_parameters :jq_autocomplete,
    id: Parameters.bigid,
    term: Parameters.string
  def jq_autocomplete
    if params[:term]
      render json: organizations_like(params[:term]).map { |o| {label: o.name, value: o.id} }
    elsif params[:id]
      o = current_account.organizations.find(params[:id].to_i)
      render json: {label: o.name, value: o.id}
    end
  end

  allow_parameters :search,
    query: Parameters.string,
    page: Parameters.integer # untested, used by get_page
  def search
    result = current_account.organizations.where("name like ?", "#{params[:query]}%").to_a
    respond_with_sql_search_result(result)
  end

  private

  def organizations_like(name)
    current_account.organizations.select('id, name').where('name LIKE ?', "#{name}%").to_a
  end

  def find_organization
    # TODO: organization restricted agents can only see own organization?
    @organization = current_account.organizations.find(params[:id])
  end

  def authorize_read_write_access
    grant_access(current_user.can?(:edit, Organization))
  end

  # Replace the :default param value for :domain_names now that we are
  # deleting the `organizations.default` column and using Organization#domain_names=
  # to set `organization_domains` and `organization_emails` to set associations
  # instead:
  def org_params_with_domain_names
    return params[:organization] unless params[:organization]&.key?(:default)

    org_params_with_domain_names = params[:organization].except(:default)
    org_params_with_domain_names[:domain_names] = params[:organization][:default]

    org_params_with_domain_names
  end
end
