class People::PermanentlyDeleteUsersController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Users::ControllerSupport
  include Zendesk::LotusEmbeddable

  ssl_allowed :all

  layout 'agent'
  tab :manage

  before_action :require_inactive_user, only: %i[show destroy]
  before_action :require_user_can_delete, only: %i[show destroy]
  before_action :authorize_admin

  allow_parameters :index,
    desc: Parameters.string,
    order: Parameters.string,
    page: Parameters.integer # untested, used by get_page
  def index
    @users = current_account.all_users.inactive_and_not_redacted.order(order).paginate(page: get_page, per_page: 50)
  end

  allow_parameters :show, id: Parameters.bigid
  def show
    return deny_access if user == current_user
    return deny_access if user.is_active?

    @related_tickets = related_tickets
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    return deny_access if user == current_user
    return deny_access if user.is_active?

    user.current_user = current_user
    user.mark_for_ultra_deletion!(executer: current_user)

    flash[:notice] = I18n.t('txt.admin.views.settings.people.compliance.success_message', name: user.name)
    redirect_to action: :index
  end

  protected

  def require_user_can_delete
    return deny_access unless current_user.can?(:delete, user)
  end

  def require_inactive_user
    return deny_access unless user
    return deny_access if user == current_user
    return deny_access if user.is_active?
  end

  def user
    @user ||= current_account.all_users.inactive_and_not_redacted.find_by_id(params[:id])
  end

  def related_tickets
    return @related_tickets if @related_tickets
    ticket_ids = Ticket.where(account_id: current_account.id).where("submitter_id = ? OR requester_id = ? OR assignee_id = ?", user.id, user.id, user.id).limit(6).pluck(:id)
    if ticket_ids.size < 6
      ticket_ids += TicketArchiveStub.where(account_id: current_account.id).where("requester_id = ? OR assignee_id = ?", user.id, user.id).limit(6 - ticket_ids.size).pluck(:id)
    end
    @related_tickets = Ticket.where(id: ticket_ids).all
  end

  def order
    params[:desc] = '1' unless ['', '1'].include?(params[:desc])
    filter_order = 'users.'
    filter_order += if ['id', 'name'].include?(params[:order])
      params[:order]
    else
      'updated_at'
    end
    filter_order + (params[:desc] == '1' ? ' DESC' : '')
  end
end
