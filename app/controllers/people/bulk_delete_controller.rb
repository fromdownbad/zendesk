class People::BulkDeleteController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Users::ControllerSupport
  include Zendesk::LotusEmbeddable

  ssl_allowed :all

  layout 'agent'
  tab :manage

  before_action :authorize_admin

  allow_parameters :index,
    desc: Parameters.string,
    filter: Parameters.string,
    order: Parameters.string,
    page: Parameters.integer
  def index
    options = {
      page: get_page,
      per_page: 50,
      total_entries: total_entries
    }

    # .all is required here to instantiate the results. If we don't do that, the query is executed
    # outside the on_slave block and hits the master
    @users = ActiveRecord::Base.on_slave do
      current_account.end_users.
        select("users.*, IFNULL(us.value, 'false') as suspended").
        joins("LEFT JOIN user_settings us on us.user_id = users.id AND us.name = 'suspended'").
        where(filter_conditions).
        order(order).
        paginate(options).all
    end
  end

  allow_parameters :new,
    user_ids: [Parameters.bigid]
  def new
    render layout: false
  end

  allow_parameters :create,
    user_ids: [Parameters.bigid]
  def create
    job_options = {
      account_id: current_account.id,
      user_id: current_user.id,
      user_ids: params[:user_ids]
    }

    job = UserBulkDeleteJob.perform_now(job_options)

    failures = job.status['results']

    if failures.any?
      flash[:error] = failures.first
    else
      flash[:notice] = I18n.t('txt.admin.views.people.bulk_delete.delete_success_notice')
    end

    redirect_to :back
  end

  protected

  def filter_conditions
    return unless suspended_users_filter?
    # Adding user_settings.account_id condition so we can use index index_user_settings_on_account_id_and_name_and_value
    "us.account_id = #{current_account.id} AND (us.value = '1' OR us.value='true')"
  end

  def total_entries
    return if suspended_users_filter?
    # doing this to prevent pagination from trying to count the entire join (very slow)
    current_account.end_users.count(:all)
  end

  def order
    params[:desc] = '1' unless ['', '1'].include?(params[:desc])
    params[:order] = 'created_at' unless ['id', 'name', 'suspended', 'created_at'].include?(params[:order])
    params[:order] = 'created_at' if current_account.has_suspended_users_filter? && params[:order] == 'suspended'
    params[:order] + (params[:desc] == '1' ? ' DESC' : '')
  end

  def suspended_users_filter?
    current_account.has_suspended_users_filter? && params[:filter] == 'suspended'
  end
end
