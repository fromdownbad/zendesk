class People::TagsController < ApplicationController
  include ::AllowedParameters
  ssl_allowed :all

  layout 'agent'
  tab :manage

  before_action :authorize_admin

  include Zendesk::LotusEmbeddable

  # TODO: USER_TAGS Is it ok with the OR SQL statements here?

  allow_parameters :index,
    page: Parameters.integer # untested, used by get_page
  def index
    conditions = 'taggable_type = "User" OR taggable_type = "Organization"'
    respond_to do |format|
      format.html do
        @result = ActiveRecord::Base.on_slave do
          # .all is required here to instantiate the results. If we don't do that, the query is executed
          # outside the on_slave block and hits the master
          current_account.taggings.select(:tag_name).where(conditions).group(:tag_name).paginate(page: get_page).all
        end
      end
      format.xml  { render xml: current_account.taggings.where(conditions).group(:tag_name).to_a }
      format.json { render json: current_account.taggings.where(conditions).group(:tag_name).to_a, callback: params[:callback] }
    end
  end

  allow_parameters :destroy, id: Parameters.string
  def destroy
    return unless params[:id].present?
    Tagging.delete_all(["account_id = #{current_account.id} AND taggable_type IN ('User','Organization') AND tag_name=?", params[:id]])
    respond_to do |format|
      format.html do
        flash[:notice] = I18n.t('txt.admin.controllers.people.tags.destroy_message_v2')
        redirect_to people_tags_path
      end
      format.xml  { head :ok }
      format.json { head :ok }
    end
  end
end
