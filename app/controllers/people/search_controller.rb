class People::SearchController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable
  include Zendesk::Search::Responder
  include Zendesk::PrivateCaching
  include Zendesk::Search::ControllerSupport

  before_action :authorize_manage_people_agent
  before_action :register_device_activity, only: [:index]

  helper_method :do_not_execute_query?

  layout 'agent'
  tab :manage
  only_use_private_caches :index
  ssl_allowed :index

  allow_parameters :index,
    query: Parameters.string,
    callback: Parameters.string, # untested, used by respond_with_search_result
    page: Parameters.integer # untested, used by get_page
  def index
    @allow_user_bulk_update = true

    if do_not_execute_query?
      query.no_result
    else
      query.type = :people
      query.per_page = 30
      query.incremental = true
      query.execute(current_user)
    end

    respond_with_search_result
  end

  private

  def do_not_execute_query?
    query.string.blank? && current_account.users.capped_count(100_000).capped?
  end
end
