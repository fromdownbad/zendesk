class People::CurrentUserController < ApplicationController
  include ::AllowedParameters
  include Zendesk::Users::ControllerSupport

  ## ### Current User
  ## #### `GET /users/current`, `GET /users/current.:format`
  ##
  ## #### Allowed For
  ##
  ##  * All
  ##
  ## #### Using curl
  ##
  ##    curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/users/current \
  ##      -H "Accept: application/json"
  ##
  ## #### Example Response
  ##
  ##    Status: 200 OK
  ##
  ##    Status: 200 OK
  ##
  ##    {
  ##      "locale_id": null,
  ##      "name": "Raemes Quirk",
  ##      "created_at": "2007/09/12 17:05:32 -0700",
  ##      "openid_url": "",
  ##      "details": "",
  ##      "last_login": "2011/06/29 11:37:47 -0700",
  ##      "notes": "",
  ##      "updated_at": "2010/11/16 12:56:03 -0800",
  ##      "photo_url": "https://support.example.com/photo/raemes.jpg",
  ##      "external_id": null,
  ##      "id": 2,
  ##      "restriction_id": 0,
  ##      "is_verified": true,
  ##      "is_active": true,
  ##      "phone": "",
  ##      "time_zone": "Pacific Time (US & Canada)",
  ##      "organization_id": null,
  ##      "roles": 2,
  ##      "uses_12_hour_clock": false,
  ##      "email": "raemes@example.com",
  ##      "groups": [
  ##        {
  ##          .. group data ..
  ##        },
  ##        {
  ##          .. group data ..
  ##        }
  ##      ]
  ##    }
  ##
  ##
  allow_parameters :show, {}
  def show
    respond_to do |format|
      format.html { redirect_to user_path(current_user) }
    end
  end

  allow_parameters :edit, {}
  def edit
    redirect_to edit_user_path(current_user)
  end
end
