require 'zendesk/users/user_manager'

class People::PasswordController < ApplicationController
  include ::AllowedParameters
  include ZendeskCrossOrigin::Helper
  include Zendesk::Auth::AuthOriginControllerSupport

  allow_cors :validate_password

  layout :layout_for_role

  TOKEN_AUTH = [:reset, :reset_password, :validate_password, :create, :create_password].freeze
  skip_before_action :authenticate_user, only: TOKEN_AUTH
  skip_before_action :verify_authenticity_token, only: TOKEN_AUTH

  # TODO: Find the right actions to skip/allow
  # Previously this used to say:
  # `skip_before_action :ensure_support_is_active!, :create, :reset`
  # See https://github.com/zendesk/zendesk/pull/43259.
  # The right way to say that was:
  # `skip_before_action :ensure_support_is_active!, only => [ :create, :reset ]`
  # since there are no before_action callbacks called 'create' and 'reset', the
  # effect was that the ensure check was skipped for all actions on this
  # controller.  For now, putting it back the way it was. Limiting it to
  # necessary actions can be done later.
  skip_before_action :ensure_support_is_active!

  before_action :ensure_user_present, except: :validate_password

  ssl_required :update, :validate_password
  ssl_allowed  :index, :reset, :reset_password, :create, :create_password

  helper_method :current_brand, :current_route

  # Show the password change form.
  #
  # Usage
  #
  #   GET /password
  #
  allow_parameters :index, {}
  def index
    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: { sso_action: 'password_change' }, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: { sso_action: 'password_change' } }
      format.all do
        render_failure(
          status: :not_found,
          title: "txt.errors.not_found.title",
          message: "txt.errors.not_found.message"
        )
      end
    end
  end

  # Show the password reset form.
  #
  # Usage
  #
  #   GET /password/reset/:token
  #   GET /password/reset
  #
  # Parameters
  #
  # token - the String token used to verify that the user can reset their password.
  #
  allow_parameters :reset,
    token: Parameters.string
  def reset
    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: { sso_action: 'password_set' }, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: { sso_action: 'password_set' } }
    end
  end

  # Show the password create form.
  #
  # Usage
  #   GET /password/create/:token
  #
  # Parameters
  #
  # token - the String token used to verify that the user can create a password.
  #
  allow_parameters :create, token: Parameters.string
  def create
    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: { sso_action: 'password_create' }, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: { sso_action: 'password_create' } }
    end
  end

  allow_parameters :validate_password,
    password: Parameters.string,
    sequence: Parameters.integer,
    token: Parameters.string,
    user_security_policy_id: Parameters.bigid
    # _sm_byp: Parameters.string - From logging, ignored
  def validate_password
    head(:bad_request) && return unless security_policy_found? && user
    policy = Zendesk::SecurityPolicy.find(params[:user_security_policy_id], current_account)
    user.password = Users::Password::BCrypt.create(URI.decode(params[:password]))
    policy.enforce(user)

    render json: {
      errors: user.errors[:password] || [],
      sequence: params[:sequence].to_i # TODO: Remove to_i when removing simulated
    }
  end

  # Change the password for the current user.
  #
  # If :password_expired is set on the session, it will be deleted, as a new password has
  # been set.
  #
  # Usage
  #
  #   PUT /password
  #
  # Parameters
  #
  # current_password - the String current password for the user.
  # new_password     - the String password that should be used henceforth.
  # return_to        - the String path that should be returned to if successful (optional).
  #
  # Redirects to the path given by `return_to` if specified. If not, redirects to the home path.
  allow_parameters :update,
    auth_origin: Parameters.string, # From code, undocumented & untested
    brand_id: Parameters.string, # From code, undocumented & untested
    current_password: Parameters.string,
    new_password: Parameters.string,
    return_to: Parameters.string, # from docs, untested
    return_to_on_failure: Parameters.string, # from test, undocumented
    user_security_policy_id: Parameters.bigid # from logs, untested & undocumented
  def update
    user_manager.change_password_for(user, from: params[:current_password], to: params[:new_password])

    flash[:notice] = I18n.t('txt.users.update_password.changed')
    redirect_to zendesk_auth.signed_in_v2_login_url(brand_id: params[:brand_id], auth_origin: params[:auth_origin]), notice: flash[:notice]
  rescue Zendesk::UserManager::PasswordInvalid
    fail_update I18n.t('txt.users.update_password.failed'), user
  rescue Zendesk::UserManager::PasswordUnchanged
    fail_update I18n.t('txt.users.update_password.unchanged')
  rescue Zendesk::UserManager::PasswordIncorrect
    fail_update I18n.t('txt.users.update_password.incorrect')
  end

  # Create a password for the current user.
  #
  # Allows a user to create a password using a verification token sent to him.
  # If the user is already signed in, he will be redirected to /password.
  #
  # Usage
  #
  #   POST /password/create
  #
  # Parameters
  #
  # token        - the String token used to verify that the user can reset their password.
  # new_password - the String password that is the users new password.
  #
  # Redirects to the path given by `return_to` if specified. If not, redireicts to the home path.
  allow_parameters :create_password,
    auth_origin: Parameters.string, # from code, untested & undocumented
    new_password: Parameters.string,
    return_to: Parameters.string, # from docs, untested
    token: Parameters.string,
    return_to_on_failure: Parameters.string, # from logs, untested & undocumented
    user_security_policy_id: Parameters.bigid # from logs, untested & undocumented
  def create_password
    reset_password_and_consume_token
    user.identities.first.verify!

    flash[:notice] = I18n.t('txt.users.update_password.changed')

    if authenticate_after_password_reset?
      redirect_to zendesk_auth.v2_login_url(auth_origin: params[:auth_origin]), notice: flash[:notice]
    else
      redirect_to_return_to '/access'
    end
  rescue Zendesk::UserManager::PasswordInvalid
    flash[:error] = flash_error(I18n.t('txt.users.update_password.failed'), user)

    redirect_to_failure_return_to
  end

  # Reset the password for the current user.
  #
  # Allows a user to reset their password using a verification token sent to them. A user that is
  # already signed in will be redirected to /password.
  #
  # Usage
  #
  #   POST /password/reset
  #
  # Parameters
  #
  # token        - the String token used to verify that the user can reset their password.
  # new_password - the String password that should be used henceforth.
  #
  # Redirects to the path given by `return_to` if specified. If not, redirects to the home path.
  allow_parameters :reset_password,
    auth_origin: Parameters.string,
    new_password: Parameters.string,
    return_to: Parameters.string, # from docs, untested
    token: Parameters.string
  def reset_password
    reset_password_and_consume_token
    user.identities.first.verify!

    flash[:notice] = I18n.t('txt.users.update_password.changed')

    if authenticate_after_password_reset?
      redirect_to zendesk_auth.v2_login_url(auth_origin: params[:auth_origin]), notice: flash[:notice]
    else
      redirect_to_return_to '/access'
    end
  rescue Zendesk::UserManager::PasswordInvalid
    flash[:error] = flash_error(I18n.t('txt.users.update_password.failed'), user)
    reset
  end

  private

  def reset_password_and_consume_token
    CIA.audit(actor: user, effective_actor: user) do
      user.active_brand_id = auth_origin.try(:brand).try(:id)
      user_manager.reset_password_for(user, to: params[:new_password])
    end

    unless authenticate_after_password_reset?
      self.current_user = user
      current_user.update_last_login
    end

    @token.destroy
  end

  def authenticate_after_password_reset?
    user.otp_configured? || (user.is_agent? && current_account.settings.two_factor_enforce)
  end

  def user
    @user ||= if params[:token].present?
      @token = lookup_token(params[:token])
      if token_source = @token.try(:source)
        if token_source.is_a?(UserIdentity)
          token_source.user
        elsif token_source.is_a?(User)
          token_source
        end
      end
    elsif current_user_exists?
      current_user
    end
  end

  def lookup_token(token_param)
    if current_account.has_generate_password_reset_token_subclass?
      token = current_account.password_reset_tokens.not_expired.lookup(token_param).first

      if !current_account.has_disallow_verification_tokens_for_password_resets? || action_name == 'validate_password'
        token ||= current_account.verification_tokens.not_expired.lookup(token_param).first
      end

      token
    else
      current_account.verification_tokens.not_expired.lookup(token_param).first
    end
  end

  def user_manager
    @user_manager ||= Zendesk::UserManager.new(authentication)
  end

  def fail_update(*args)
    flash[:error] = flash_error(*args)

    redirect_to_failure_return_to
  end

  def ensure_user_present
    if user.nil?
      redirect_to '/hc/signin', flash: { error: I18n.t('txt.admin.controllers.people.password.ensure_user_present') }
    end
  end

  def security_policy_found?
    Zendesk::SecurityPolicy.all.any? do |policy|
      policy.id == params[:user_security_policy_id].to_i
    end
  end

  def restrict_to_change_password
    false
  end
end
