class People::RolesController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_admin
  before_action :authorize_feature

  before_action :load_permission_set, only: [:show, :edit, :update, :destroy, :clone]
  before_action :check_editable_permission_set, only: [:show, :edit, :update]
  before_action :check_deletable_permission_set, only: [:destroy]

  include Zendesk::LotusEmbeddable

  ssl_allowed :all

  allow_parameters :index,
    page: Parameters.integer
  def index
    @result = PermissionSet.paginate(current_account, get_page)

    respond_to do |format|
      format.html { render 'people/search/index' }
    end
  end

  allow_parameters :new, {}
  def new
    @permission_set = PermissionSet.build_new_permission_set(current_account)
    @policy_service_permissions = {}
    render edit_view
  end

  allow_parameters :edit,
    id: Parameters.string
  def edit
    @policy_service_permissions = current_account.has_permissions_custom_role_policy_dual_save? ? policies_helper.policy_service_permissions(@permission_set) : {}
    render edit_view
  rescue Kragle::ResponseError, Kragle::ClientError, Kragle::ServerError, Faraday::ClientError, Faraday::ConnectionFailed, Faraday::TimeoutError
    render_failure(status: :internal_server_error, title: 'Internal Server Error', message: 'Internal Server Error')
  end

  allow_parameters :show, allowed_parameters[:edit]
  alias :show :edit

  allow_parameters :clone,
    id: Parameters.bigid
  def clone
    @source_permission_set = @permission_set
    @cloneable_attributes = @source_permission_set.attributes.with_indifferent_access.except(:id, :name, :account_id)
    @cloneable_permissions = @source_permission_set.permissions.each_with_object({}) do |p, attrs|
      attrs[p.name.to_sym] = p.value
    end
    @permission_set = current_account.permission_sets.new(@cloneable_attributes)
    @permission_set.name = I18n.t('txt.admin.controllers.people.roles.clone_name', role_name: @source_permission_set.name)
    @permission_set.permissions.set(@cloneable_permissions)
    @policy_service_permissions = current_account.has_permissions_custom_role_policy_dual_save? ? policies_helper.policy_service_permissions(@source_permission_set) : {}
    render edit_view
  rescue Kragle::ResponseError, Kragle::ClientError, Kragle::ServerError, Faraday::ClientError, Faraday::ConnectionFailed, Faraday::TimeoutError
    render_failure(status: :internal_server_error, title: 'Internal Server Error', message: 'Internal Server Error')
  end

  allow_parameters :create, permission_set: Parameters.anything
  def create
    @permissions_attributes = params[:permission_set].delete(:permissions) || {}
    ps_permissions = params[:permission_set].delete(:policy_service_permissions) || {}

    # Since there is no manage_help_center property, mapping it to forum_access
    if can_manage_help_center = params[:permission_set].delete(:manage_help_center)
      @permissions_attributes[:forum_access] = map_to_forum_access_value(can_manage_help_center)
    end

    if view_user_profile_lists = params[:permission_set].delete(:view_user_profile_lists)
      @permissions_attributes[:available_user_lists] = map_to_available_user_lists_value(view_user_profile_lists)
    end

    @permission_set = current_account.permission_sets.new(params[:permission_set])
    @permission_set.permissions.set(@permissions_attributes)

    if Zendesk::SupportUsers::PermissionSet.new(@permission_set).save(context: 'lotus_create')
      if current_account.has_permissions_custom_role_policy_dual_save?
        handle_create_policy(@permission_set, ps_permissions)
      else
        flash[:notice] = I18n.t('txt.admin.controllers.people.role.create_success_notice', role_name: "<a class='title' href='#{people_role_path(@permission_set)}'>#{ERB::Util.h(@permission_set.name)}</a>")
        redirect_to action: :index
      end
    else
      flash.now[:error] = flash_error(I18n.t('txt.admin.controllers.people.role.create_failure_notice'), @permission_set)
      @policy_service_permissions = ps_permissions.each_with_object({}) { |(resource_scope, enabled), result| result[resource_scope] = enabled == "1" }
      render edit_view
    end
  end

  allow_parameters :update,
    id: Parameters.bigid,
    permission_set: Parameters.anything
  def update
    permissions_attributes = params[:permission_set].delete(:permissions) || {}
    ps_permissions = params[:permission_set].delete(:policy_service_permissions) || {}

    if can_manage_help_center = params[:permission_set].delete(:manage_help_center)
      permissions_attributes[:forum_access] = map_to_forum_access_value(can_manage_help_center)
    end

    if view_user_profile_lists = params[:permission_set].delete(:view_user_profile_lists)
      permissions_attributes[:available_user_lists] = map_to_available_user_lists_value(view_user_profile_lists)
    end

    params[:permission_set][:permissions] = permissions_attributes

    @permission_set.update_property_set_attributes(params[:permission_set])
    @permission_set.assign_attributes(params[:permission_set])

    permissions_service_failure_handler = lambda do |perm_set|
      flash.now[:error] = flash_error(I18n.t('txt.admin.controllers.people.role.update_failure_notice'), perm_set)
      @policy_service_permissions = ps_permissions.each_with_object({}) { |(resource_scope, enabled), result| result[resource_scope] = enabled == "1" }
      render edit_view
    end

    update_permission_set_handler = lambda { |perm_set| handle_update_permission_set(perm_set, ps_permissions) }

    policies_helper.handle_update!(@permission_set, update_permission_set_handler, permissions_service_failure_handler, ps_permissions)
  end

  allow_parameters :destroy,
    id: Parameters.bigid
  def destroy
    permissions_service_failure_handler = lambda do |perm_set|
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.role.delete_failure_notice'), perm_set)
      redirect_to action: :edit
    end

    delete_permission_set_handler = lambda { |perm_set| handle_delete_permission_set(perm_set) }

    policies_helper.handle_delete!(@permission_set, delete_permission_set_handler, permissions_service_failure_handler)
  end

  private

  def edit_view
    if Arturo.feature_enabled_for?(:permissions_roles_redesign, current_account)
      { action: :new_edit, layout: 'roles' }
    else
      { action: :edit }
    end
  end

  def load_permission_set
    @permission_set = current_account.permission_sets.find_by_id(params[:id])

    if @permission_set.nil?
      render_failure(status: :not_found, message: I18n.t('txt.admin.controllers.people.role.cannot_find_role'), title: "404 Not Found")
      false
    end
  end

  def check_editable_permission_set
    unless current_user.can?(:edit, @permission_set)
      render_failure(status: :forbidden, message: I18n.t('txt.admin.controllers.people.role.cannot_be_edited'), title: "Forbidden")
      false
    end
  end

  def check_deletable_permission_set
    unless current_user.can?(:delete, @permission_set)
      render_failure(status: :forbidden, message: I18n.t('txt.admin.controllers.people.role.cannot_be_deleted'), title: "Forbidden")
      false
    end
  end

  def authorize_feature
    unless current_account.has_permission_sets?
      render_failure(status: :forbidden, message: I18n.t('txt.admin.controllers.people.role.cannot_use_feature'), title: "Forbidden")
    end
  end

  def map_to_forum_access_value(value)
    if Zendesk::DB::Util.truthy?(value)
      "full"
    else
      "readonly"
    end
  end

  def map_to_available_user_lists_value(value)
    if Zendesk::DB::Util.truthy?(value)
      "all"
    else
      "none"
    end
  end

  def handle_update_permission_set(permission_set, ps_permissions)
    if Zendesk::SupportUsers::PermissionSet.new(permission_set).save(context: 'lotus_update')
      flash[:notice] = I18n.t('txt.admin.controllers.people.role.update_success_notice', role_name: "<a class='title' href='#{people_role_path(permission_set)}'>#{ERB::Util.h(permission_set.name)}</a>")
      redirect_to action: :index
    else
      flash.now[:error] = flash_error(I18n.t('txt.admin.controllers.people.role.update_failure_notice'), permission_set)
      @policy_service_permissions = ps_permissions.each_with_object({}) { |(resource_scope, enabled), result| result[resource_scope] = enabled == "1" }
      render edit_view
    end
  end

  def policies_helper
    @permission_set_policies_helper ||= Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(current_account.subdomain, current_user.id)
  end

  def handle_create_policy(permission_set, ps_permissions)
    response = policies_helper.create_policy!(permission_set, ps_permissions)
    if response.success?
      flash[:notice] = I18n.t('txt.admin.controllers.people.role.create_success_notice', role_name: "<a class='title' href='#{people_role_path(permission_set)}'>#{ERB::Util.h(permission_set.name)}</a>")
      redirect_to action: :index
    else
      undo_then_render(permission_set, ps_permissions)
    end
  rescue Kragle::ResponseError, Kragle::ClientError, Kragle::ServerError, Faraday::ClientError, Faraday::ConnectionFailed, Faraday::TimeoutError
    undo_then_render(permission_set, ps_permissions)
  end

  def undo_then_render(permission_set, ps_permissions)
    @policy_service_permissions = ps_permissions.each_with_object({}) { |(resource_scope, enabled), result| result[resource_scope] = enabled == "1" }
    Zendesk::SupportUsers::PermissionSet.new(permission_set).destroy(context: 'lotus_destroy')
    @permission_set = PermissionSet.build_new_permission_set(current_account)
    flash.now[:error] = flash_error(I18n.t('txt.admin.controllers.people.role.create_failure_notice'), permission_set)
    render edit_view
  end

  def handle_delete_permission_set(permission_set)
    if permission_set.present?
      if Zendesk::SupportUsers::PermissionSet.new(permission_set).destroy(context: 'lotus_destroy')
        flash[:notice] = I18n.t('txt.admin.controllers.people.role.delete_success_notice', role_name: ERB::Util.h(permission_set.name))
      else
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.role.delete_failure_notice'), permission_set)
        redirect_to action: :edit
        return
      end
    else
      flash[:error] = I18n.t('txt.admin.controllers.people.role.cannot_find_role_with_id')
    end

    redirect_to action: :index
  end
end
