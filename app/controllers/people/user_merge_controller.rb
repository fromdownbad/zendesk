require 'zendesk/search'

class People::UserMergeController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_agent
  before_action :find_loser
  before_action :find_winner, except: [:new, :autocomplete]

  layout false

  ssl_allowed :all

  ticket_id_parameter_type = Parameters.bigid | Parameters.nil_string

  allow_parameters :new, user_id: Parameters.bigid
  def new
    search(@loser.name, 4, false)
    render
  end

  allow_parameters :show,
    id: Parameters.bigid, # Necessary due to test_support. Not clear for route
    ticket_id: ticket_id_parameter_type,
    user_id: Parameters.bigid,
    winner: Parameters.string
  def show
    @tickets = @loser.tickets.not_closed
    @topics = @loser.entries
    render
  end

  allow_parameters :create,
    ticket_id: ticket_id_parameter_type, # From code, untested
    user_id: Parameters.bigid,
    winner: Parameters.string
  def create
    if ::Users::Merge.validate_and_merge(@winner, @loser) == :merge_complete
      respond_to do |format|
        format.html { head :ok } # we are using Radar to redirect
        format.js   { @ticket = current_account.tickets.find_by_nice_id(params[:ticket_id]) }
      end
    else
      flash[:error] = I18n.t('txt.admin.controllers.people.user_merge.merge_fail')
      respond_to do |format|
        format.html { redirect_to user_path(@loser) }
        format.js   { render }
      end
    end
  end

  allow_parameters :autocomplete,
    name: Parameters.string,
    user_id: Parameters.bigid
  def autocomplete
    search(params[:name], 50)
    results = @users.map do |user|
      if user.is_a?(User)
        "#{user.name} <#{user.main_identity}>"
      else
        user
      end
    end
    render json: results
  end

  private

  def find_loser
    unless @loser = current_account.users.find_by_id(params[:user_id])
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.find_user_fail_with_no_params'))
    end
    unless current_user.can?(:edit, @loser)
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.no_permission_to_edit', user_name: @loser.name))
    end
    unless @loser.is_end_user?
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.can_only_merge_end_user', user_name: @loser.name))
    end
    unless ::Users::Merge.remote_auth_loser_with_ext_id?(@loser)
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.cannot_merge_with_external_id', user_name: @loser.name))
    end
    unless ::Users::Merge.valid_loser?(@loser)
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.not_a_valid_loser', user_name: @loser.name))
    end
  end

  def find_winner
    if params[:winner].blank?
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.find_user_fail_with_no_params'))
    end

    if /^\d+$/.match?(params[:winner])
      @winner = current_account.users.find_by_id(params[:winner])
    elsif main_identity = params[:winner].split(/<(.*)>/)[1]
      @winner = current_account.find_user_by_main_identity(main_identity)
    elsif EMAIL_PATTERN.match?(params[:winner].strip)
      @winner = current_account.find_user_by_main_identity(params[:winner].strip)
    end
    unless @winner
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.find_user_fail_with_no_params'))
    end
    if @loser == @winner
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.cannot_merge_user_to_itself', user_name: @winner.name))
    end
    unless @winner.is_end_user?
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.can_only_merge_end_user', user_name: @winner.name))
    end
    unless current_user.can?(:edit, @winner)
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.no_permission_to_edit', user_name: @winner.name))
    end
    unless ::Users::Merge.valid_winner?(@winner)
      return show_error(I18n.t('txt.admin.controllers.people.user_merge.not_a_valid_winner', user_name: "#{@winner.name} <#{@winner.main_identity}>"))
    end
  end

  def search(name, limit, want_more = true)
    @users = []
    begin
      @users = Zendesk::Search.search(
        current_user,
        "name:\"#{name}\"",
        per_page: limit,
        incremental: true,
        type: :user,
        with: {user_role: 0}, # Only suggest end users
        without: {id: @loser.id} # Exclude the user being merged from the suggestions
      )

      @users << {more: true} if want_more && (@users.total_entries > @users.size)
    rescue Error => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Failed to connect to search backend", fingerprint: 'e00d867d8d575ff9c3bb5f8fbd9d1dfa562f1756')
      "users.account_id = #{current_account.id} AND users.name LIKE #{User.quote_value('%' + name + '%')}"
    end
  end

  def show_error(message)
    render partial: "error", locals: { message: message }
  end
end
