require 'vpim/vcard'
require 'rqrcode'
require 'zendesk/radar_factory'

class People::UsersController < ApplicationController
  allow_zendesk_mobile_app_access only: :multivalue_autocomplete

  include ::AllowedParameters
  include Zendesk::Users::ControllerSupport
  include Zendesk::LotusEmbeddable
  include UserIdentitiesHelper
  include Zendesk::PrivateCaching
  include Devices::UsersHelper
  include Zendesk::Search::ControllerSupport
  include Zendesk::Elasticsearch::ControllerSupport
  include ZendeskApi::Controller::Pagination

  before_action :authorize_admin, only: [:bulk]
  skip_before_action :verify_authenticity_token, only: [:choose_locale]
  skip_before_action :authorize_admin, only: [:choose_locale]
  deprecate_actions [:new, :create, :find_or_create]

  protect_from_forgery except: [:index, :new, :edit, :select_bulk_action, :destroy, :resend_welcome_email, :autocomplete, :multivalue_autocomplete, :verify, :validate_channel_success, :successful_twitter_auth, :successful_facebook_auth, :build_callback_url_with, :facebook_callback_path, :create_twitter_profile, :merge, :merge_with_winner, :choose_locale]
  skip_before_action :verify_authenticity_token, unless: -> { current_account.has_assume_csrf_protection? }

  layout :layout_for_role
  tab    :manage

  before_action :setup_user, only: [
    :show, :edit, :destroy, :verify, :update_notes, :resend_welcome_email, :add_email_address, :merge_complete,
    :add_twitter_handle_manually, :successful_twitter_auth, :update_number, :successful_facebook_auth, :profiles_with_the_same_phone_number
  ]
  before_action :validate_channel_success, only: [:successful_facebook_auth, :successful_twitter_auth]
  before_action :authorize_agent, only: [:new, :update_notes]

  before_action :check_user_edit_permission, only: [:verify, :update_number, :update_notes]
  before_action :check_user_delete_permission, only: [:destroy]
  before_action :check_view_user_list_permission, only: [:autocomplete, :profiles_with_the_same_phone_number]

  before_action :validate_email_address, only: [:find_or_create]
  before_action :find_winner_and_loser_for_merge, only: [:merge_with_email, :merge_with_winner]
  before_action :device_list_only_for_agents, only: [:show]
  before_action :set_flash_on_merge_completion, only: [:merge_complete]

  skip_before_action :authenticate_user, only: [:revert, :create_twitter_profile, :choose_locale]
  ssl_required :create_twitter_profile
  ssl_allowed :index, :update, :assume, :select_bulk_action, :edit, :bulk, :choose_locale

  only_use_private_caches :index

  helper_method :auth_via?

  OFFSET_PAGINATION_MAX_OFFSET = 100000

  rescue_from Access::Denied do
    grant_access(false)
  end

  allow_parameters :index,
    flash_digest: Parameters.string,
    group: Parameters.string,
    group_id: Parameters.string,
    legacyagents: Parameters.integer,
    limit: Parameters.integer,
    order: Parameters.string,
    order_without_created_at: Parameters.boolean,
    organization: Parameters.string,
    organization_id: Parameters.string,
    permission_set: Parameters.string,
    query: Parameters.string,
    role: (Parameters.array(Parameters.string) | Parameters.string),
    seat_type: Parameters.string,
    page: Parameters.integer
  def index
    @query = params[:query].to_s.strip
    page_size = request.format == :html ? API_DEFAULT_PER_PAGE : 100
    offset_pagination_max_page = OFFSET_PAGINATION_MAX_OFFSET / page_size

    if params[:page] && params[:page] > offset_pagination_max_page
      raise ActiveRecord::RecordNotFound, "page out of range"
    end

    @result = users(per_page: page_size, capped_count: OFFSET_PAGINATION_MAX_OFFSET)
    @total_entries = @result.try(:total_entries)
    @total_entries_formatted = if @total_entries && @total_entries >= OFFSET_PAGINATION_MAX_OFFSET
      ::I18n.t('txt.admin.people.membership_lower_bound', lower_bound: OFFSET_PAGINATION_MAX_OFFSET)
    else
      "(#{@total_entries})"
    end
    @total_entries = @total_entries.to_i

    @result.total_entries = OFFSET_PAGINATION_MAX_OFFSET if @total_entries && @total_entries > OFFSET_PAGINATION_MAX_OFFSET

    @max_agents = current_account.subscription.max_agents

    @total_agents = current_account.multiproduct_billable_agent_count! || '-'

    @allow_user_bulk_update = true
    @show_max_page_tooltip = params[:page] == offset_pagination_max_page
    @pagination_options = { inner_window: 0, outer_window: -2 }

    respond_to do |format|
      format.html { render 'people/search/index' }
      format.xml  { render xml: @result.to_xml(user_serialization_options) }
      format.json { render json: @result.to_json(user_serialization_options), callback: params[:callback] }
    end
  end

  allow_parameters :new,
    inline: Parameters.boolean
  def new
    @user = current_account.users.new(restriction_id: RoleRestrictionType.REQUESTED)

    if params[:inline]
      render partial: 'new_requester', layout: false
    else
      respond_to do |format|
        format.html { render action: :new }
        format.mobile_v2 { return render_failure(status: :not_found, title: "Not found") }
      end
    end
  end

  allow_parameters :show,
    desc: Parameters.integer, # desc, order, sort_by, and sort_order are used in generic_ticket_list
    external_ids: Parameters.string,
    flash_digest: Parameters.string,
    group_id: Parameters.integer, # from logs, untested & undocumented
    id: Parameters.string,
    include: Parameters.array(Parameters.string),
    loser_id: Parameters.bigid,
    order: Parameters.string,
    page: Parameters.integer, # untested, used by get_page
    return_to: Parameters.string,
    select: Parameters.string,
    sort_by: Parameters.string,
    sort_order: Parameters.string
  def show
    return deny_access unless current_user.can?(:view, @user)

    if current_user.is_end_user? && current_account.arturo_enabled?('disable_legacy_people_users_id_page')
      if current_account.help_center_enabled?
        return redirect_to "/hc/profiles/#{@user.id}"
      else
        return render_404_page
      end
    end

    if current_user.is_agent?
      redirect_to "/agent/users/#{@user.id}"
    else
      respond_to do |format|
        format.html do
          @tickets_title = I18n.t('txt.requests.organization.index.title_by', user: current_user).truncate(30)

          options = {page: get_page, order: 'created_at DESC, id DESC'}

          case params[:select]
          when 'ccs'
            @ccs = @user.collaborated_tickets.for_user(current_user).archived_paginate(options)
          when 'entries'
            @entries = @user.entries.for_user(current_user).paginate(options)
          when 'posts'
            @posts = @user.posts.paginate(options)
          when 'votes'
            @voted_entries = @user.voted_entries.for_user(current_user).paginate(options)
          when 'watchings'
            @watchings = @user.watchings.paginate(options)
          when 'devices'
            # No action necessary
            nil
          else
            # Tickets
            if current_user.is_agent?
              @output, select, joins, order = Rule.generic_ticket_list(params)
            else
              @output = Output.create(columns: [:nice_id, :description, :created, :updated, :status])
              select  = nil
              joins   = nil
            end
            @output.type = "table"
            @tickets = if @user.is_agent?
              @user.assigned.for_user(current_user).archived_paginate(
                options.merge(select: select, joins: joins, order: order)
              )
            else
              @user.tickets.for_user(current_user).archived_paginate(
                options.merge(select: select, joins: joins, order: order)
              )
            end
          end

          render 'show'
        end
      end
    end
  end
  allow_parameters :merge_complete, allowed_parameters[:show]
  alias_method :merge_complete, :show

  user_allowed_attributes = {
    account: Parameters.string,
    additional_tags: Parameters.string,
    agent_display_name: Parameters.string,
    alias: Parameters.string,
    current_tags: Parameters.string,
    default_group_id: Parameters.string,
    details: Parameters.string,
    direct_line: Parameters.integer,
    groups: Parameters.array(Parameters.bigid),
    is_moderator: Parameters.boolean,
    is_private_comments_only: Parameters.string,
    locale_id: Parameters.string,
    name: Parameters.string,
    notes: Parameters.string,
    organization: Parameters.string,
    organization_id: Parameters.string,
    organization_name: Parameters.string,
    password: Parameters.string,
    permission_set: Parameters.string,
    phone: Parameters.string,
    private_comments_only: Parameters.string,
    remote_photo_url: Parameters.string,
    remove_tags: Parameters.string,
    role_or_permission_set: Parameters.integer,
    roles: (Parameters.array(Parameters.bigid) | Parameters.bigid),
    send_verify_email: Parameters.string,
    set_tags: Parameters.array(Parameters.string) | Parameters.string,
    shared_phone_number: Parameters.string,
    signature: {
      value: Parameters.string
    },
    skip_verify_email: Parameters.string,
    tags: Parameters.string,
    time_zone: Parameters.string,
    twitter_screen_name: Parameters.string,
    uses_12_hour_clock: Parameters.string,
    voice_extension: Parameters.string,
    voice_number: Parameters.string,

    # questionable but currently needed, taken from user.rb in a comment from 2d7de95ff4da
    facebook_id: Parameters.string,
    external_id: Parameters.string,
    email: Parameters.string,
    challenge: Parameters.string,
    is_verified: Parameters.string,
    restriction_id: Parameters.string,
    suspended: Parameters.string,
    new_twitter_identity: Parameters.string,
    skip_verification: Parameters.string
  }.freeze

  allow_parameters :create,
    user: Parameters.map(user_allowed_attributes)
  def create
    @user = new_user
    return deny_access unless current_user.can?(:create, @user)

    respond_to do |format|
      if @user.save
        format.html do
          message = if current_account.is_welcome_email_when_agent_register_enabled?
            I18n.t('txt.users.created.message_success.welcome_email_enable', user_path_and_name: "<a class='title' href='#{user_path(@user)}'>#{CGI.escapeHTML(@user.name)}</a>")
          else
            I18n.t('txt.users.created.message_success.welcome_email_disable', user_path_and_name: "<a class='title' href='#{user_path(@user)}'>#{CGI.escapeHTML(@user.name)}</a>")
          end

          flash[:notice] = message
          redirect_to action: "index"
        end

        format.xml  { head :created, location: user_url(@user, format: :xml) }
        format.json { render json: @user.to_json }
      else
        format.html do
          flash[:error] = flash_error(I18n.t('txt.users.created.message_failure', user_name: CGI.escapeHTML(@user.name)), @user)
          render action: "new"
        end

        format.xml  { render xml: @user.errors, status: :not_acceptable }
        format.json { render json: @user.errors, status: :not_acceptable }
      end
    end
  end

  allow_parameters :edit,
    id: Parameters.bigid,
    return_to: Parameters.string # From logging, untested
  def edit
    return deny_access unless current_user.can?(:edit, @user)

    if current_user.is_end_user? && current_account.arturo_enabled?('disable_legacy_people_users_id_page')
      render_404_page
    end
  end

  allow_parameters :update,
    anchor: Parameters.string,
    direct_line: Parameters.integer, # From logging, untested
    id: Parameters.string,
    # From logging, untested
    # only referenced as hidden field in app/views/people/users/_basic_info.html.erb
    ignoreupload: Parameters.integer,
    user: Parameters.map(user_allowed_attributes),
    password: Parameters.string,
    photo: Parameters.map(
      uploaded_data: Parameters.file
    ),
    return_to: Parameters.string,
    signature: Parameters.map( # From logging, untested
      value: Parameters.string
    )
  def update
    params[:user] = params.fetch(:user, {}).except(:password) unless
      current_account.admins_can_set_user_passwords
    @user = user_initializer.apply(requested_user)
    return deny_access unless current_user.can?(:edit, @user)

    @user.update_phone_and_identity(params[:user][:phone], direct_line?(params[:user][:phone])) if params[:user][:phone].present?

    respond_to do |format|
      if Zendesk::SupportUsers::User.new(@user).save
        format.html do
          user_link = "<a class='title' href='#{user_path(@user)}'>#{CGI.escapeHTML(@user.name)}</a>"
          flash[:notice] = I18n.t("txt.users.update.success", link_to_user: user_link)
          redirect_to action: "edit", return_to: params[:return_to], anchor: params[:anchor]
        end
        format.xml  { head :ok }
        format.json { head :ok }
        format.js   { render js: "User updated" }
      else
        format.html do
          flash[:error] = flash_error(I18n.t("txt.users.update.failed"), @user, @phone_identity)
          render action: "edit"
        end
        format.xml  { render xml: @user.errors, status: :not_acceptable }
        format.json { render json: @user.errors, status: :not_acceptable }
        format.js   { head :not_acceptable }
      end
    end
  end

  allow_parameters :update_notes,
    field: Parameters.string,
    id: Parameters.bigid,
    user: Parameters.map(
      notes: Parameters.string
    )
  def update_notes
    @user.update_attribute(:notes, params[:user][:notes])
    respond_to do |format|
      format.js { render partial: 'shared/notes_updated', locals: { model: @user, field: params[:field] }, layout: false }
    end
  end

  allow_parameters :select_bulk_action,
    bulk_check: Parameters.string, # From logging, untested
                                   # Most likely UI checkbox artifact
    seat_type: Parameters.string,
    users: Parameters.map(
      ids: Parameters.array(Parameters.bigid)
    )
  # Returns colorbox form for completing bulk user update requests
  def select_bulk_action
    @user_ids = params[:users][:ids]
    users = User.find(@user_ids.map(&:to_i))
    @includes_account_owner = users.any?(&:is_account_owner?)
    @can_edit_voice_seat = users.all? { |u| [Role::ADMIN.id, Role::AGENT.id].include? u.roles }
    render layout: false
  end

  allow_parameters :bulk,
    users: Parameters.map(
      ids: Parameters.array(Parameters.bigid),
      permission_set: Parameters.string,
      voice_seat: Parameters.string
    )
  # Bulk update attributes of users
  # Currently, only permission_set is being updated
  def bulk
    job_options = {
      account_id: current_account.id,
      user_ids: params[:users][:ids],
      permission_set: params[:users][:permission_set],
      voice_seat: params[:users][:voice_seat],
      locale: current_user.translation_locale
    }

    job = UserBulkUpdateJob.perform_now(job_options)

    results = job.status['results']

    if results.any?
      flash[:error] = results.first
    else
      flash[:notice] = I18n.t('txt.users.update.bulk.success_message')
    end

    if current_account.has_low_agent_seat_radar_notification?
      agent_count = current_account.multiproduct_billable_agent_count!
      support_seats_remaining = agent_count && (current_account.subscription.max_agents - agent_count)

      # only send notifications if seat count is present && is 0, use less or equal than just in case any negetive seats remaining
      # put support_seats_remaining into payload to avoid any future support seat remaining changes
      if support_seats_remaining && support_seats_remaining <= 0
        ::RadarFactory.create_radar_notification(
          current_account,
          "growl/#{current_user.id}"
        ).send('low_agent_seat', support_seats_remaining)
      end
    end
    redirect_to :back
  end

  allow_parameters :destroy,
    id: Parameters.bigid,
    return_to: Parameters.string
  def destroy
    begin
      @user.delete!

      flash[:notice] = I18n.t('txt.users.destroy.success_message', user_name: CGI.escapeHTML(@user.name))

      respond_to do |format|
        format.html do
          redirect_to_return_to(url_for(action: 'index'))
        end
        format.xml { head :ok }
      end
      return
    rescue ActiveRecord::RecordInvalid
      flash[:error] = flash_error(I18n.t('txt.users.destroy.failure_message'), @user)
    end

    respond_to do |format|
      format.html do
        if @user.foreign? && params[:return_to].present?
          redirect_to_return_to
        else
          redirect_to action: 'edit'
        end
      end
      format.xml { render xml: @user.errors, status: :forbidden }
    end
  end

  allow_parameters :resend_welcome_email,
    id: Parameters.bigid,
    identity_id: Parameters.bigid
  def resend_welcome_email
    identity = @user.identities.find(params[:identity_id])
    identity.send_verification_email

    flash[:notice] = I18n.t('txt.welcome_email_sent.notice', identity: identity)

    redirect_to action: 'show', id: params[:id]
  end

  allow_parameters :assume,
    id: Parameters.string,
    translation_id: Parameters.string
  def assume
    if current_account.has_disable_user_assume?
      return render_failure(status: :forbidden, message: "Forbidden", title: "Forbidden")
    end

    if is_assuming_user?
      flash[:error] = flash_error(I18n.t('txt.user.assuming_user.already_assuming_user_error'))
      index
    elsif params[:id].to_i != current_user.id
      @user = current_account.users.find_by_id(params[:id]) || current_account.anonymous_user
      return deny_access unless current_user.can?(:assume, @user)

      logger.warn("#{Time.now} assume: #{current_account.subdomain} #{current_user.id}/#{current_user.name} assuming user #{@user.id}/#{@user.name}")
      shared_session[:locale_id] = if !params[:translation_id].nil?
        params[:translation_id]
      else
        current_user.locale_id || current_account.locale_id
      end
      flash[:notice] = I18n.t('txt.user.asumming_user.notice', user_name: @user.name) if params[:translation_id]
      self.original_user = current_user
      self.current_user = @user

      authentication.current_user = @user

      # at this point we want the shared session cache in Doorman to be cleared so we emit a Kafka message
      publish_assumption_event_to_kafka!(event_type: "assume", original_user_id: original_user.id, current_user_id: current_user.id)

      return_to_parser.user = @user
      redirect_to_return_to home_path
    end
  end

  allow_parameters :revert, {}
  def revert
    if current_user.is_anonymous_user?
      return head :unauthorized
    elsif !is_assuming_user?
      redirect_to home_path
      return
    end

    logger.warn("#{Time.now} assume: #{current_account.subdomain} #{current_user.id}/#{current_user.name} reverting assume")

    self.current_user  = original_user
    self.original_user = nil

    session.delete(:locale)

    publish_assumption_event_to_kafka!(event_type: "revert", original_user_id: original_user.try(:id), current_user_id: current_user.id)

    redirect_to home_path
  end

  allow_parameters :autocomplete,
    name: Parameters.string
  def autocomplete
    render_autocomplete do |user_ids|
      # take `garbage",'name` and return parsing friendly `"garbage\",'name" <garbage@example.com>`
      query = "SELECT CONCAT('\"', REPLACE(name, '\"', '\\\\\"'), '\" <', user_identities.value, '>')
        FROM users INNER JOIN user_identities ON user_identities.user_id = users.id
        WHERE users.id IN (#{user_ids.join(',')}) AND user_identities.priority = 1 AND #{UserEmailIdentity.type_condition_sql}"
      if current_user.restriction_id == RoleRestrictionType.ORGANIZATION && current_user.organization_id.present?
        query += " AND users.organization_id = #{current_user.organization_id}"
      end
      User.connection.select_values(query)
    end
  end

  allow_parameters :multivalue_autocomplete, {}
  def multivalue_autocomplete
    render_autocomplete do |user_ids|
      query = "SELECT CONCAT('\"', REPLACE(name, '\"', '\\\\\"'), '\" <', user_identities.value, '>'), users.id
        FROM users INNER JOIN `user_identities` ON user_identities.user_id = users.id
        WHERE users.id IN (#{user_ids.join(',')}) AND user_identities.priority = 1 AND #{UserEmailIdentity.type_condition_sql}"
      if current_user.restriction_id == RoleRestrictionType.ORGANIZATION && current_user.organization_id.present?
        query += " AND users.organization_id = #{current_user.organization_id}"
      end
      query += " ORDER BY users.roles DESC"
      User.connection.select_rows(query)
    end
  end

  allow_parameters :verify,
    id: Parameters.bigid,
    identity_id: Parameters.bigid
  def verify
    identity = @user.identities.find(params[:identity_id])
    unless identity.is_verified?
      identity.verify!
      @user.skip_password_validation = true if @user.password.blank?
      @user.save!
    end

    flash[:notice] = I18n.t('txt.user.verify_identity_manually.notice', identity: identity, user_name: CGI.escapeHTML(@user.name))
    redirect_to action: 'show', id: params[:id]
  end

  allow_parameters :update_number,
    id: Parameters.bigid,
    number: Parameters.string,
    direct_line: Parameters.integer
  def update_number
    if direct_line?(params[:number]) && current_user.is_end_user?
      flash[:error] = I18n.t('txt.user.direct_line.update_direct_line.error_notice')
      redirect_to(user_path(@user.id)) && return
    end

    @user.update_phone_and_identity(params[:number], direct_line?(params[:number]))

    respond_to do |format|
      format.html do
        if @user.save
          flash[:notice] = I18n.t('txt.user.direct_line.update_number.success_notice')
        else
          flash[:error] = flash_error(I18n.t('txt.user.direct_line.update_number.error_notice'), @user)
        end
        redirect_to user_path(@user.id)
      end

      format.json do
        if @user.save
          render json: @user.reload.to_json
        else
          render json: @user.errors, status: :not_acceptable
        end
      end
    end
  end

  allow_parameters :validate_channel_success,
    state: Parameters.string
  def validate_channel_success
    return deny_access unless current_user.can?(:edit, @user)
    return deny_access if params[:state] != Zendesk::Auth::Warden::OAuthStrategy.state(session, Zendesk::Configuration.fetch(:google)['key'])
  end

  allow_parameters :successful_twitter_auth,
    id: Parameters.bigid,
    state: Parameters.string,
    user_id: Parameters.bigid
  def successful_twitter_auth
    @twitter = Zendesk::Auth::Twitter.new('twitter_readonly', request.env)
    access_token = @twitter.create_access_token(params)
    user_mapper = channels_user_mapper(current_account, access_token.params[:user_id], "twitter")
    @user = user_mapper.add_identity(@user)

    if @user.valid?
      flash[:notice] = I18n.t('txt.user.twitter_auth.success_notice')
    else
      flash[:error] = flash_error(I18n.t('txt.user.twitter_auth.error_notice'), @user)
    end

    redirect_to action: :edit, anchor: 'identities'
  rescue OAuth::Unauthorized
    flash[:error] = flash_error(I18n.t('txt.user.twitter_auth.error_authorization_denied_notice'), @user)
    redirect_to action: :edit, anchor: 'identities'
  rescue Net::HTTPFatalError
    flash[:error] = flash_error(I18n.t('txt.user.twitter_auth.error_communication_notice'), @user)
    redirect_to action: :edit, anchor: 'identities'
  end

  allow_parameters :successful_facebook_auth,
    id: Parameters.bigid,
    callback_url: Parameters.string,
    real_domain: Parameters.string,
    state: Parameters.string,
    error: Parameters.string,
    error_description: Parameters.string
  def successful_facebook_auth
    params[:callback_url] = build_callback_url_with(facebook_callback_path)

    @facebook = Zendesk::Auth::Facebook.new(request.env)
    access_token = @facebook.create_access_token(params)

    user_mapper = channels_user_mapper(current_account, 'me', "facebook", access_token: access_token)
    @user = user_mapper.add_identity(@user)

    if match = /^https?:\/\/([^:\/]+)/.match(params[:real_domain])
      return_to_host = match[1]
    else
      flash.now[:error] = flash_error(I18n.t('txt.user.facebook_auth.bad_return_url', return_to_host: ERB::Util.html_escape(return_to_host)))
      redirect_to(action: :edit) && return
    end

    @user.save

    if return_to_host == request.host
      redirect_to action: :edit
    else
      # we are on the wrong domain so we switch
      Rails.logger.info "Cross-domain redirect to #{return_to_host} from #{request.host}"
      redirect_to("http://#{return_to_host}/users/#{@user.id}/edit")
    end
  rescue OAuth2::Error
    flash[:error] = if params[:error] == "access_denied"
      flash_error(I18n.t('txt.user.facebook_auth.error_authorization_denied_notice'), @user)
    else
      flash_error(I18n.t('txt.user.facebook_auth.error_notice', error_description: params[:error_description]), @user)
    end
    redirect_to action: :edit, anchor: 'identites'
  rescue Net::HTTPFatalError
    flash[:error] = flash_error(I18n.t('txt.user.facebook_auth.error_communication_notice'), @user)
    redirect_to action: :edit, anchor: 'identities'
  end

  allow_parameters :build_callback_url_with, {}
  def build_callback_url_with(path)
    protocol = URI.parse(request.url).scheme + "://"
    "#{protocol}#{current_account.default_host}#{path}"
  end

  allow_parameters :facebook_callback_path,
    challenge: Parameters.string,
    real_domain: Parameters.string,
    id: Parameters.bigid
  def facebook_callback_path
    url_for(
      only_path: true,
      action: :successful_facebook_auth,
      challenge: params[:challenge],
      real_domain: params[:real_domain],
      state: Zendesk::Auth::Warden::OAuthStrategy.state(session, Zendesk::Configuration.fetch(:google)['key'])
    )
  end

  allow_parameters :create_twitter_profile,
    oauth_verifier: Parameters.string,
    user_id: Parameters.bigid,
    id: Parameters.bigid
  # This action is intended to be used as an oauth callback
  def create_twitter_profile
    if params[:oauth_verifier]

      # Validate that the user ID that was passed in matches a session value.  If they
      # don't match, then the request has been tampered with.  See
      # https://support.zendesk.com/agent/tickets/1660385
      raise OAuth::Unauthorized unless session[:create_twitter_profile_user_id].to_s == params[:id].to_s
      user            = current_account.users.find_by_id(params[:id])
      twitter         = Zendesk::Auth::Twitter.new('twitter_readonly')
      access_token    = twitter.create_access_token(params)
      twitter_user_id = access_token.params[:user_id]

      user.identities << UserTwitterIdentity.new(user: user, value: twitter_user_id, is_verified: true)
      user.save

      self.current_user = user
      return_to_parser.user = user

      # TODO: Channels.  This class is defined in the zendesk_channels repo.  Convert to API call?
      ::Channels::TwitterUserProfile.from_external_id(
        current_account, twitter_user_id, user, access_token: access_token.token, secret: access_token.secret
      )
    end
  rescue OAuth::Unauthorized
    flash[:error] = flash_error(I18n.t('txt.user.twitter_auth.error_authorization_denied_notice'))
  rescue Net::HTTPFatalError
    flash[:error] = flash_error(I18n.t('txt.user.twitter_auth.error_communication_notice'))
  ensure
    redirect_to_return_to home_path
  end

  allow_parameters :find_or_create,
    user: Parameters.map(
      email: Parameters.string,
      name: Parameters.string,
      skip_verify_email: Parameters.string,
      phone: Parameters.string,
      organization_id: Parameters.bigid
    )
  def find_or_create
    if identity = find_email_identity(params[:user][:email])
      @user = identity.user

      respond_to do |format|
        format.html do
          flash[:notice] = I18n.t('txt.user.found_identity', user_path_and_name: "<a class='title' href='#{user_path(@user)}'>#{CGI.escapeHTML(@user.name)}</a>")
          redirect_to action: 'index'
        end
        format.xml  { head :ok, location: user_url(@user, format: :xml) }
        format.json { render json: @user.to_json }
      end
    else
      if params[:user][:skip_verify_email].blank?
        params[:user][:skip_verify_email] = !current_account.is_welcome_email_when_agent_register_enabled?
      end
      if params[:user][:name].blank?
        address = params[:user][:email]
        params[:user][:name] = Zendesk::Mail::Address.new(address: address).name || ''
      end
      create
    end
  end

  allow_parameters :merge,
    return_to: Parameters.string # From logging, untested
  def merge
    render layout: false
  end

  allow_parameters :merge_with_email,
    email: Parameters.string
  def merge_with_email
    if @winner
       # Email exists in another user account (the winner)
      if @loser == @winner || !@winner.can_be_merged_as_winner?
        # Winner is probably an agent - can't merge into
        flash[:error] = I18n.t('txt.merge.cannot_add_with_params', email: ERB::Util.html_escape(params[:email].to_s))
      elsif @winner.is_verified?
        # Prompt for password for the winner account, before doing merge
        respond_to do |format|
          format.js do
            render(:update) { |page| page.call '$z("people/users/merge").switch_to_password_form()' }
            return
          end
          format.mobile_v2 do
            render 'merge_with_email', params: { email: params[:email] }
            return
          end
        end
      else
        clear_session_and_logout
        # Winner is not verified. Need to verify first. At next loser login, loser can try again.

        @winner.identities.first.send_verification_email
        flash[:error] = I18n.t('txt.merge.email_not_verified', email: ERB::Util.html_escape(params[:email].to_s))
      end
    elsif params[:email]
      # Email does not exist in any user account - add email to current_user
      # If the user has another identity(twitter/facebook) this email is a secondary identity
      if current_user.has_alter_ego?
        current_user.create_unverified_email_address(params[:email])
      else
        current_user.send_verify_email = true
        current_user.identities << UserEmailIdentity.new(user: current_user, account: current_account, value: params[:email])
      end

      if current_user.save
        flash[:notice] = I18n.t('txt.merge.email_verification_sent', email: ERB::Util.html_escape(params[:email].to_s))
      else
        flash[:error] = I18n.t('txt.merge.cannot_add_with_params', email: params[:email])
      end
    end
    respond_to do |format|
      format.js do
        render(:update) { |page| page.redirect_to params[:return_to] }
      end

      format.mobile_v2 do
        redirect_to_return_to
      end
    end
  end

  allow_parameters :merge_with_winner,
    password: Parameters.string,
    email: Parameters.string
  def merge_with_winner
    merge_users_with_auth!(@winner, @loser, params[:password])
    self.current_user = @winner
    render_merge_with_winner
  rescue UserAuthenticationError
    respond_to do |format|
      format.js do
        render(:update) { |page| page.call "$z('people/users/merge').password_does_not_match(\"#{I18n.t('txt.merge.wrong_password')}\")" }
      end
      format.mobile_v2 do
        flash[:error] = I18n.t('txt.merge.wrong_password')
        render :merge_with_email
      end
    end
  rescue UserMergeError
    flash[:error] = I18n.t('txt.merge.cannot_add_with_params', email: ERB::Util.html_escape(params[:email].to_s))
    render_merge_with_winner
  end

  allow_parameters :choose_locale,
    id: Parameters.bigid,
    locale: Parameters.integer
  def choose_locale
    shared_session[:locale_id] = params[:id]

    translation_locale = TranslationLocale.find_by_id(shared_session[:locale_id]) || ENGLISH_BY_ZENDESK
    shared_session[:locale_id] = translation_locale.id

    set_users_locale translation_locale

    return_to = request.env["HTTP_REFERER"] || home_path
    return_to.gsub!(/&?locale=(\d+)/, '')
    redirect_to return_to
  end

  allow_parameters :profiles_with_the_same_phone_number,
    id: Parameters.bigid
  def profiles_with_the_same_phone_number
    respond_to do |format|
      format.json { render json: @user.with_the_same_phone_number_and_priority_to_identity }
    end
  end

  private

  def staff_service_client
    Zendesk::StaffClient.new(current_account)
  end

  def publish_assumption_event_to_kafka!(event_type:, original_user_id:, current_user_id:)
    return unless current_account.has_publish_assumption_event_to_kafka?
    # Using escape, hope it doesn't increase latency
    EscKafkaMessage.create!(
      account_id: current_account.id,
      topic: "auth.assumption_events",
      key: shared_session[:id],
      value: encode_protobuf(event_type: event_type, event_time: Time.now, original_user_id: original_user_id, current_user_id: current_user_id, account_id: current_account.id)
    )
  end

  def encode_protobuf(event_type:, event_time:, account_id:, original_user_id:, current_user_id:)
    AssumptionEventsProtobufEncoder.new(event_type: event_type, event_time: event_time, account_id: account_id, original_user_id: original_user_id, current_user_id: current_user_id).full.to_proto
  end

  def set_users_locale(translation_locale) # rubocop:disable Naming/AccessorMethodName
    I18n.locale = translation_locale
    unless current_user.is_anonymous_user?
      current_user.translation_locale = I18n.translation_locale
      current_user.save
      current_user.smart_touch
    end
  end

  def render_merge_with_winner
    respond_to do |format|
      format.js do
        render(:update) { |page| page.redirect_to params[:return_to] }
      end
      format.mobile_v2 do
        redirect_to new_request_path
      end
    end
  end

  def direct_line?(phone)
    if params[:direct_line].present?
      params[:direct_line].to_i == 1
    else
      Users::PhoneNumber.new(current_account, phone).valid_and_unique?
    end
  end

  def render_autocomplete
    name = params[:name]
    users = []

    if name.present?
      user_ids = autocomplete_user_ids(name)
      if user_ids.any?
        users = yield(user_ids)
        users << {more: true} if user_ids.total_entries > user_ids.size
      end
    end

    render json: users
  end

  def validate_email_address
    unless (params[:user][:email] = Zendesk::Mail::Address.sanitize(params[:user][:email]))
      flash[:error] = I18n.t('txt.user.validate_email_address.error_notice')
      respond_to do |format|
        format.html do
          @user = current_account.users.new
          render action: 'new'
        end
        format.xml  { render xml: flash[:error], status: :not_acceptable }
        format.json do
          render json: {
            html: render_to_string(partial: '/shared/flash'),
            text: flash[:error]
          }.to_json, status: :not_acceptable
          flash.discard
        end
      end
    end
  end

  def setup_user
    @user = requested_user
  end

  def autocomplete_user_ids(name)
    options = {
      user_id: current_user.id,
      locale: current_user.locale,
      time_zone: ActiveSupport::TimeZone[current_user.time_zone].tzinfo.identifier,
      group_ids: current_user.group_ids.join(","),
      size: 50
    }

    response = search_client.autocomplete_users(name, options)
    num_found = response['count']
    docs = response['results'].map { |doc| doc['id'] }

    Zendesk::Search::Results.create(1, 50, num_found) do |pager|
      pager.replace(docs)
    end
  end

  def find_winner_and_loser_for_merge
    @loser = current_user
    return unless @loser.can_be_merged_as_loser?
    @winner = current_account.find_user_by_email(params[:email])
  end

  def check_view_user_list_permission
    grant_access current_user.can?(:lookup, User)
  end

  def check_user_edit_permission
    grant_access current_user.can?(:edit, @user)
  end

  def check_user_delete_permission
    grant_access (@user != current_user) && current_user.can?(:delete, @user)
  end

  def device_list_only_for_agents
    if on_devices_tab? && !show_devices_tab?
      raise ActiveRecord::RecordNotFound
    end
  end

  def set_flash_on_merge_completion
    return unless params[:merge_status]

    case params[:merge_status].to_i
    when 1
      flash[:notice] = I18n.t('txt.admin.controllers.people.user_merge.merge_success', user_name_who_losses: CGI.escapeHTML(params[:loser]), user_name_who_wins: CGI.escapeHTML(@user.name))
    when 0
      flash[:error] = I18n.t('txt.admin.controllers.people.user_merge.merge_fail')
    end
  end

  def channels_user_mapper(account, external_id, channel, options = {})
    ::Channels::UserMapper.new(
      account,
      user:         { id: external_id, id_str: external_id },
      type:         channel,
      access_token: options[:access_token],
      should_fetch: true
    )
  end
end
