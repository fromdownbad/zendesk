class People::GroupsController < ApplicationController
  include ::AllowedParameters
  include Zendesk::LotusEmbeddable
  include Zendesk::SharedControllerSupport
  include Zendesk::Users::ControllerSupport

  layout 'agent'
  tab :manage

  ssl_allowed :all

  before_action :authorize_read_write_access, except: [:index, :show]
  before_action :authorize_agent, only: [:index, :show]
  before_action :setup_group, only: [:show, :edit, :update, :destroy, :confirm_delete]
  require_capability :groups, only: [:create]

  COUNT_WITH_ARCHIVED_LIMIT = 100_000

  # A couple of these were pulled from the model attributes and their types
  # identified from the corresponding API controller.
  group_parameters = {
      agents: [Parameters.bigid | Parameters.string], # [""] clears group
      default: Parameters.boolean,
      description: Parameters.string,
      name: Parameters.string,
      settings: {
        chat_enabled: Parameters.boolean
      }
    }

  allow_parameters :new, {}
  def new
    @group  = Group.new
    @agents = current_account.agents
    @user_memberships = {}
    @user_entitlements = entitlements(current_account.agents)

    render action: :edit
  end

  allow_parameters :index,
    page: Parameters.integer # untested, used by get_page
  def index
    respond_to do |format|
      format.html do
        @result = current_account.groups.assignable(current_user).paginate(page: get_page)
        render 'people/search/index'
      end
    end
  end

  allow_parameters :show,
    id: Parameters.bigid,
    select: Parameters.string,
    page: Parameters.integer, # untested, used by get_page
    group_id: Parameters.integer, # from logs, untested & undocumented
    desc: Parameters.integer | Parameters.nil_string, # desc, order, sort_by, and sort_order are used in generic_ticket_list
    order: Parameters.string,
    sort_by: Parameters.string,
    sort_order: Parameters.string
  def show
    @allow_user_bulk_update = true
    @ticket_count, @user_count = ActiveRecord::Base.on_slave do
      ticket_count = if current_account.has_limit_group_count_with_archived?
        cache_key = "cached_count_tickets_by_group_with_archived_#{@group.id}/#{current_account.id}"
        Rails.cache.fetch(cache_key, expires_in: 1.hour) do
          count = @group.tickets.where(account_id: current_account.id).for_user(current_user).count_with_archived(limit: COUNT_WITH_ARCHIVED_LIMIT)
          count > COUNT_WITH_ARCHIVED_LIMIT ? COUNT_WITH_ARCHIVED_LIMIT : count
        end
      else
        @group.tickets.where(account_id: current_account.id).for_user(current_user).count_with_archived
      end
      [
        ticket_count,
        @group.users.in_account(current_account.id).count
      ]
    end

    respond_to do |format|
      format.html do
        if params[:select] == 'tickets'
          @tickets, @output = ActiveRecord::Base.on_slave do
            output, select, joins, order = Rule.generic_ticket_list(params)
            tickets = @group.tickets.for_user(current_user).archived_paginate(page: get_page, select: select, joins: joins, order: order)
            [tickets, output]
          end
        else
          @users = ActiveRecord::Base.on_slave do
            @group.users.in_account(current_account.id).paginate(page: get_page)
          end
        end
      end
    end
  end

  allow_parameters :edit, id: Parameters.bigid
  def edit
    @user_memberships = @group.memberships.each_with_object({}) { |m, h| h[m.user_id] = {default: m.default } }
    @user_entitlements = entitlements(current_account.agents)

    Membership.
      where(user_id: @user_memberships.keys).
      group(:user_id).
      select([:user_id, 'COUNT(*) as group_count']).
      each_with_object(@user_memberships) do |m, hash|
        hash[m.user_id][:count] = m.group_count
      end
  end

  allow_parameters :create,
    group: group_parameters
  def create
    @group = Group.new

    begin
      Group.transaction do
        @group.account    = current_account
        @group.attributes = params[:group]
        @group.save!
      end

      respond_to do |format|
        format.html do
          flash[:notice] = I18n.t('txt.admin.controllers.people.groups.group.create_success_notice', group_name: "<a class='title' href='#{group_path(@group)}'>#{ERB::Util.h(@group.name)}</a>")
          redirect_to action: 'index'
        end
      end

      return
    rescue ActiveRecord::RecordInvalid
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.groups.group.create_failure_notice'), @group)
    end

    @user_memberships = {}
    @user_entitlements = entitlements(current_account.agents)

    respond_to do |format|
      format.html { render action: 'edit' }
    end
  end

  allow_parameters :update,
    id: Parameters.bigid,
    group: group_parameters
  def update
    Group.transaction do
      @group.current_user = current_user
      ids_being_set = params[:group].delete(:agents)
      update_members((ids_being_set || []).reject(&:blank?))
      @group.update_attributes!(params[:group])
    end

    respond_to do |format|
      format.html do
        flash[:notice] = I18n.t('txt.admin.controllers.people.groups.group.update_success_notice', group_name: "<a class='title' href='#{group_path(@group)}'>#{ERB::Util.h(@group.name)}</a>")
        redirect_to action: 'index'
      end
    end
  rescue ActiveRecord::RecordInvalid
    respond_to do |format|
      format.html do
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.groups.group.update_failure_notice'), @group)
        redirect_to action: 'edit', id: @group.id
      end
    end
  end

  allow_parameters :confirm_delete, id: Parameters.bigid
  def confirm_delete
    @voice_usage = verify_group_usage_with_voice
    render layout: false
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    return render_failure(
      status: :bad_request,
      title: '400 Bad Request',
      messaeg: 'Request must be DELETE'
    ) unless request.delete?

    begin
      @group.current_user = current_user
      @group.deactivate!

      respond_to do |format|
        format.html do
          flash[:notice] = I18n.t('txt.admin.controllers.people.groups.group.delete_success_notice', group_name: ERB::Util.h(@group.name))
          redirect_to action: 'index'
        end
      end

      return
    rescue ActiveRecord::RecordInvalid
      if @group.errors.include?(:default_group)
        default_user_names = @group.errors.messages[:default_group]
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.groups.group.group_still_agent_default'), *default_user_names)
      elsif @group.errors.include?(:default)
        default_group_name = @group.errors.messages[:default].first
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.groups.group.group_is_default', default_group_name: default_group_name))
      else
        flash[:error] = flash_error(I18n.t('txt.admin.controllers.people.groups.group.delete_failure_notice'), @group)
      end
    end

    respond_to do |format|
      format.html { redirect_to action: 'index' }
    end
  end

  allow_parameters :search,
    query: Parameters.string,
    page: Parameters.integer # untested, used by get_page
  def search
    result = current_account.groups.where("name like ?", "#{params[:query]}%").to_a
    respond_with_sql_search_result(result)
  end

  private

  def entitlements(agents)
    # need to get a list of all the users here
    agents.each_with_object({}) do |user, final_result|
      final_result[user["id"]] = [].tap do |arr|
        support_role = Zendesk::SupportUsers::Entitlement.for(user, Zendesk::Accounts::Client::SUPPORT_PRODUCT).role
        chat_role = Zendesk::SupportUsers::Entitlement.for(user, Zendesk::Accounts::Client::CHAT_PRODUCT).role
        talk_role = Zendesk::SupportUsers::Entitlement.for(user, Zendesk::Accounts::Client::TALK_PRODUCT).role
        arr << :support if support_role.present?
        arr << :chat if chat_role.present?
        arr << :talk if talk_role.present?
      end
    end
  end

  def verify_group_usage_with_voice
    response = api_client.connection.get("/api/v2/channels/voice/internal/account/groups/#{@group.id}/usage")
    group    = response.body['group']
    Hashie::Mash.new(group)
  rescue ZendeskAPI::Error::RecordNotFound
    return Hashie::Mash.new(phone_numbers: [], ivrs: [])
  rescue ZendeskAPI::Error::NetworkError => ex
    ZendeskExceptions::Logger.record(ex, location: self, message: "Network error when contacting Voice: #{ex.message}", fingerprint: '657328d5b4445fb64393773c89346717c03ce001')
    return Hashie::Mash.new(phone_numbers: [], ivrs: [])
  end

  def api_client
    @api_client ||= Zendesk::InternalApi::Client.new(current_account.subdomain)
  end

  def authorize_read_write_access
    grant_access(current_user.can?(:edit, Group))
  end

  def setup_group
    @group = current_account.groups.find(params[:id])
    return render_failure(
      status: :not_found,
      title: I18n.t('txt.errors.not_found.title'),
      messaeg: I18n.t('txt.errors.not_found.message')
    ) if @group.nil?
  end

  def update_members(ids_being_set)
    ids_being_set = ids_being_set.map(&:to_i)
    current_memberships = @group.memberships.joins(:user).where('users.is_active = 1').pluck(:user_id)
    membership_remove = current_memberships - ids_being_set
    membership_add = ids_being_set - current_memberships

    User.where(id: membership_add).each do |agent|
      agent.current_user = current_user
      Zendesk::Users::GroupMembershipUpdate.new(agent, agent.memberships.pluck(:group_id) + [@group.id], agent.default_group_id).update
      if agent.errors[:default_group_id].present?
        @group.errors.add(:default_group, "#{agent.name} -- #{agent.errors[:default_group_id][0]}")
      else
        agent.save!
      end
    end

    User.where(id: membership_remove).each do |agent|
      agent.group_ids_being_removed =  agent.memberships.pluck(:group_id) - [@group.id]
      agent.current_user = current_user
      Zendesk::Users::GroupMembershipUpdate.new(agent, agent.group_ids_being_removed, agent.default_group_id).update
      if agent.errors[:default_group_id].present?
        @group.errors.add(:default_group, "#{agent.name} -- #{agent.errors[:default_group_id][0]}")
      else
        agent.save!
      end
    end

    raise ActiveRecord::RecordInvalid, @group if @group.errors.any?
  end
end
