# Please be advised there are multiple "agreements controllers":
#
#   app/controllers/sharing/agreements_controller.rb
#   app/controllers/sharing_agreements_controller.rb
#   app/controllers/api/**/sharing_agreements_controller.rb
#
# the controller in the app/controllers directory (sharing_agreements.controller.rb) is user facing
# the controller in the app/controllers/sharing directory (agreements_controller.rb) is for backend work
# the controllers in the app/controllers/api/** directories are for api work

class SharingAgreementsController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_agent
  before_action :authorize_admin, except: [:show, :jira_projects]

  ssl_allowed :all

  UPDATE_NOTICE_MAP = {
    pending: "txt.controllers.sharing_agreements_controller.agreement_successfully_resent",
    accepted: "txt.controllers.sharing_agreements_controller.agreement_successfully_accepted",
    inactive: "txt.controllers.sharing_agreements_controller.agreement_successfully_deactivated",
    failed: "txt.controllers.sharing_agreements_controller.agreement_successfully_cancelled",
    declined: "txt.controllers.sharing_agreements_controller.agreement_successfully_declined",
  }.freeze

  sharing_agreement_parameters = {
      accepted: Parameters.boolean,
      allows_public_comments: Parameters.boolean,
      name: Parameters.string,
      remote_url: Parameters.string,
      shared_with_id: Parameters.bigid | Parameters.nil_string,
      status: Parameters.string,
      subdomain: Parameters.string | Parameters.nil,
      sync_custom_fields: Parameters.anything,
      sync_tags: Parameters.string
    }

  allow_parameters :create,
    sharing_agreement: sharing_agreement_parameters
  def create
    @agreement = current_account.sharing_agreements.new(params[:sharing_agreement])
    @agreement.local_admin = current_user

    respond_to do |format|
      if @agreement.save
        format.html { redirect_to(settings_tickets_path(anchor: 'ticket_sharing')) }
        format.json do
          render(
            json: @agreement, status: :created,
            location: settings_tickets_url(anchor: 'ticket_sharing', r: @agreement.id)
          )
        end
      else
        format.html do
          flash[:error] = flash_error(I18n.t('txt.controllers.sharing_agreements_controller.agreement_was_not_saved_because'), @agreement)
          redirect_to(settings_tickets_url(anchor: 'ticket_sharing'))
        end
        format.json { render json: @agreement.errors, status: :unprocessable_entity }
      end
    end
  end

  allow_parameters :show, id: Parameters.bigid
  def show
    @agreement = current_account.sharing_agreements.find(params[:id])
    render 'show', layout: false if request.xhr?
  end

  allow_parameters :update,
    id: Parameters.bigid,
    sharing_agreement: sharing_agreement_parameters
  def update
    @agreement = current_account.sharing_agreements.find(params[:id])
    @agreement.local_admin = current_user
    respond_to do |format|
      if @agreement.update_attributes(params[:sharing_agreement])
        status = @agreement.status
        # When we update to a pending status we know we want to resend the agreement
        if status == :pending && !@agreement.resend_invite
          # When resending more than 1 time in a minute, let the user know the resend did not work
          flash[:error] = flash_error(I18n.t('txt.controllers.sharing_agreements_controller.failed_to_resend'))
          format.html { redirect_to(settings_tickets_path(anchor: 'ticket_sharing')) }
        elsif status == :failed
          @agreement.destroy
        end
        # Custom notices for each type of status change
        format.html do
          redirect_to(settings_tickets_path(anchor: 'ticket_sharing'),
            notice: I18n.t(UPDATE_NOTICE_MAP[status]))
        end
      else
        flash[:error] = flash_error(I18n.t('txt.controllers.sharing_agreements_controller.the_aggreement_Was_not_updated'), @agreement)
        format.html { redirect_to(settings_tickets_path(anchor: 'ticket_sharing')) }
      end
    end
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    @agreement = current_account.sharing_agreements.find(params[:id])
    @agreement.destroy

    respond_to do |format|
      format.html { redirect_to(settings_tickets_path(anchor: 'ticket_sharing')) }
    end
  end

  allow_parameters :destroy_inactive, {}
  def destroy_inactive
    current_account.sharing_agreements.inactive.outbound.each(&:destroy)

    redirect_to settings_tickets_path(anchor: "ticket_sharing")
  end

  allow_parameters :jira_projects, id: Parameters.bigid
  def jira_projects
    @agreement = current_account.sharing_agreements.find(params[:id])

    if jira_response = @agreement.retrieve_jira_projects
      render json: jira_response
    else
      render json: {}, status: :bad_gateway
    end
  end
end
