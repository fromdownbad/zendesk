class ActivateTrialController < ApplicationController
  include ::AllowedParameters
  include Zendesk::ReturnTo::Helper

  skip_before_action :ensure_support_is_active!

  before_action :set_system_user_actor, if: :internal_request?
  before_action :ensure_multiproduct_account
  before_action :authenticate_user, unless: :internal_request?
  before_action :authorize_owner_or_admin, except: :index, unless: :internal_request?
  before_action :verify_authenticity_token, unless: :internal_request?
  before_action :check_support_status, except: [:state, :wait, :fail]

  layout false

  helper_method :owner_or_chat_admin?

  STATE_COMPLETE = 'complete'.freeze
  STATE_FAILED = 'failed'.freeze
  STATE_PENDING = 'pending'.freeze
  FAILED_JOB_STATUSES = [Resque::Plugins::Status::STATUS_FAILED, Resque::Plugins::Status::STATUS_KILLED].freeze
  TIMEOUT = 5 # minutes

  allow_parameters :index, {}
  def index
    render :index
  end

  allow_parameters :activate, {}
  def activate
    respond_to do |format|
      format.json do
        user = internal_request? ? User.system : current_user
        if current_account.has_accsrv_product_created_notification?
          create_not_started_support_product
        else
          current_account.start_support_trial(user)
        end
        response = { redirect: url_for(action: :wait) }
        render json: response, status: :accepted
      end
    end
  end

  allow_parameters :wait, {}
  def wait
    if current_account.has_accsrv_product_created_notification?
      return redirect_to(url_for(action: :fail)) if support_inactive_for_too_long?

      return redirect_to(url_for(action: :index)) unless support.present?
    else
      # if job has failed, redirect to fail
      return redirect_to(url_for(action: :fail)) if job_failed?
      # if job is not enqueued, redirect to index
      return redirect_to(url_for(action: :index)) if job_status.nil?
    end

    render :wait
  end

  allow_parameters :fail, {}
  def fail
    render :fail
  end

  allow_parameters :state, {}
  def state
    respond_to do |format|
      activate_state =
        if current_account.has_accsrv_product_created_notification?
          if support.try(:active?)
            STATE_COMPLETE
          elsif support_inactive_for_too_long?
            STATE_FAILED
          else
            STATE_PENDING
          end
        else
          if activation_succeeded?
            STATE_COMPLETE
          elsif support_creation_job_enqueued?
            STATE_PENDING
          else
            STATE_FAILED
          end
        end
      format.json { render json: { state: activate_state }, status: :ok }
    end
  end

  private

  def authorize_owner_or_admin
    grant_access(owner_or_chat_admin?)
  end

  def owner_or_chat_admin?
    current_user.is_account_owner? || chat_admin?
  end

  def chat_admin?
    entitlements = Zendesk::StaffClient.new(current_account).get_entitlements!(current_user.id)
    entitlements[:chat] == 'admin'
  end

  def ensure_multiproduct_account
    deny_access unless current_account.multiproduct?
  end

  def support
    @support ||= current_account.
      products(use_cache: current_account.has_accsrv_product_created_notification?).
      find { |product| product.name == :support }
  end

  def check_support_status
    respond_to do |format|
      format.json do
        deny_access unless current_account.is_serviceable
        head :conflict if support.try(:active?)
        head :accepted if support_creation_job_enqueued?
      end

      format.all do
        if support.try(:active?)
          redirect_to('/agent')
        elsif support_inactive?
          redirect_to('/admin/billing/subscription')
        elsif support_creation_job_enqueued?
          redirect_to(url_for(action: :wait))
        end
      end
    end
  end

  def support_inactive?
    !current_account.is_serviceable || support.try(:expired?) || support.try(:inactive?) || support.try(:cancelled?)
  end

  def internal_request?
    @internal_request ||= request.signed_internal_request?
  end

  def set_system_user_actor
    CIA.current_actor = User.system
  end

  def job_status
    @job_status ||= begin
      job_id = Rails.cache.read(current_account.support_creation_cache_key)
      return if job_id.nil?

      Resque::Plugins::Status::Hash.get(job_id).try(:status)
    end
  end

  def support_creation_job_enqueued?
    return false if job_status.nil? || job_failed?

    true
  end

  def job_failed?
    FAILED_JOB_STATUSES.include?(job_status)
  end

  def activation_succeeded?
    job_status == Resque::Plugins::Status::STATUS_COMPLETED && support.try(:active?)
  end

  def support_inactive_for_too_long?
    # if support hasn't transitioned from not_started yet in the last 5 minutes
    support.present? && !support.active? && support.created_at < TIMEOUT.minutes.ago
  end

  def create_not_started_support_product
    if support.nil?
      payload = { product: { state: Zendesk::Accounts::Product::NOT_STARTED } }
      account_client.create_product!('support', payload, context: "trial_activation_manual")
    end
  end

  def account_client
    @account_client ||= Zendesk::Accounts::Client.new(
      current_account,
      retry_options: {
        max: 3,
        interval: 2,
        exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
        methods: [:post]
      },
      timeout: 10
    )
  end
end
