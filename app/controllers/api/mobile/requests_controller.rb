class Api::Mobile::RequestsController < Api::V2::RequestsController
  include Api::Mobile::MobileOnlyAccess

  include Zendesk::MobileSdk::ControllerSupport
  include Zendesk::MobileSdk::RequestsSupport
  include Zendesk::MobileSdk::Monitoring

  before_action :allow_only_mobile_sdk_tokens_access

  log_or_throttle_sdk_action_by_account [:index, :show_many], :mobile_sdk_limit_high
  log_or_throttle_sdk_action_by_account [:create, :update, :show], :mobile_sdk_limit_low

  after_action :link_push_notifications, only: [:create]

  filter_oauth_scopes :sdk, only: %i[show create update show_many]
  filter_oauth_scopes :read, only: %i[index count]

  # A security risk was found where end users with unverified email addresses
  # had access to tickets coming from these unverified email addresses
  # potentially allowing hackers to access other companies via password reset
  # emails.
  #
  # A fix was made https://github.com/zendesk/zendesk/pull/29682 under the
  # 'restrict_end_users_with_unverified_emails' Arturo feature by the secdev
  # team which fixes the vulnerability by preventing the access to the requests
  # endpoint from users with unverified email identities created after 17-09-2017.
  # This same fix caused a side effect of preventing a subset of
  # SDK anonymous users (ones with unverified email user identities) to access
  # the mobile SDK requests endpoint breaking the SDK.
  #
  # This temporarly fix was made skipping the 'ensure_end_user_email_is_verified'
  # and these tests ensure that the SDK continues working on future code changes.
  #
  # We will be skipping the checks only if the
  # 'mobile_sdk_skip_unverified_emails_check' arturo feature is enabled.
  skip_before_action :ensure_end_user_email_is_verified,
    if: -> { current_account.has_mobile_sdk_skip_unverified_emails_check? }

  allow_parameters :index, status: Parameters.string
  def index
    if current_account.has_mobile_requests_index_cache_control_header?
      expires_in current_account.settings.mobile_requests_index_cache_control_ttl
    end

    super
  end

  allow_parameters :show_many, tokens: Parameters.string, status: Parameters.string
  def show_many
    render json: presenter.present(many_tickets(status_params))
  end

  allow_parameters :create, request: { tags: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil }

  allow_parameters :show, id: Parameters.string

  allow_parameters :update, id: Parameters.string

  ## ### Counting Requests
  ## `GET /api/mobile/requests/count.json`
  ## `GET /api/mobile/requests/count.json?status=hold,open`
  ##
  ## #### Allowed For
  ##
  ##  * End Users
  ##
  ## #### Available parameters
  ##
  ## | Name                  | Type                | Required  | Comments
  ## | --------------------- | --------------------| --------- | -------------------
  ## | `status`              | string              | no        | Ticket statuses to count: `new`, `open`, `pending`, `hold`, `solved`, `closed`
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/mobile/requests/count.json" \
  ##   -H "User-Agent: {mobile_sdk_user_agent}" \
  ##   -H "Authorization: Bearer {end_user_access_token}" \
  ##   -H "Content-Type: application/json" \
  ##   -H "Accept: application/json"
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "requests": {
  ##     "count" => 3
  ##   }
  ## }
  ## ```
  allow_parameters :count, status: Parameters.string
  def count
    @tickets_count ||= Rails.cache.fetch(count_cache_key,
      expires_in: 2.minutes, race_condition_ttl: 20.seconds) do
      current_user.tickets.where(ticket_options(*status_params)[:conditions]).count(:all)
    end
    render json: {requests: {count: @tickets_count}}
  end

  protected

  def presenter
    Api::Mobile::RequestPresenter.new(current_user, url_builder: self, includes: includes, csat_enabled: csat_enabled?)
  end

  def many_tickets(_status)
    raise ActionController::ParameterMissing, "tokens" if tokens_params.blank?
    raise ActionController::ParameterMissing, "status" if status_params.blank?

    @tickets ||= begin
      found_tickets = tickets_scope(tokens_params).where(status_id: status_ids)

      # TODO: Somehow this fails because of ticket archiver
      # paginate(tickets_scope(params[:tokens].split(",")), ticket_options(*status))
      if current_account.has_mobile_requests_ordering_fix?
        paginate(found_tickets.order(requested_order))
      else
        found_tickets
      end
    end
  end

  def tickets(*statuses)
    ticket_options = ticket_options(*statuses)

    @tickets ||= if current_account.has_mobile_sdk_dont_show_archived_requests?
      tickets = user.tickets.where.not(
        id: current_account.ticket_archive_stubs.where(requester_id: current_user.id)
      )

      paginate(tickets, ticket_options)
    else
      super
    end
  end

  # Used in `show` (inherited).
  def requested_ticket
    return @requested_ticket unless @requested_ticket.nil?

    ticket = find_ticket
    ticket.current_user = current_user
    @requested_ticket = ensure_access_to_ticket(ticket)
  end

  # Used in `update` and `print` (from `Zendesk::Tickets::ControllerSupport`)
  def ensure_original_ticket
    requested_ticket
  end

  # For anonymous users we need to link the ticket to their MobileSdkIdentity
  # This after filter should not fail the ticket creation, but should report errors
  def link_push_notifications
    if anonymous_authenticated? && header = request.env["HTTP_MOBILE_SDK_IDENTITY"].presence
      if current_sdk_identity = current_user.identities.sdk.where(account_id: current_account.id, value: header).first
        PushNotifications::DeviceIdentifier.
          where(
            account_id: current_account.id,
            user_identity_id: current_sdk_identity.id
          ).first.try(:subscribe_to_ticket, initialized_ticket)
      else
        Rails.logger.error "Could not find the provided MobileSdkIdentity: #{header}"
      end
    end
  rescue => error
    Rails.logger.error "Could not link the MobileSdkIdentity #{header} \
                        to the new ticket #{initialized_ticket.nice_id} \
                        with error #{error.message}"
  end

  def ticket_initializer
    @ticket_initializer ||= Zendesk::MobileSdk::TicketInitializer.new(
      current_account, current_user, params, include: ticket_finder_includes, request_params: request_params
    )
  end

  def count_cache_key
    "#{current_account.id}/#{current_account.route_id}/#{current_user.id}/mobile/requests/count.json?#{status_params.join(',')}"
  end

  # Used to respect the "support_show_closed_requests" features when using anonymous authentication.
  # This is used basically when hitting "show_many" here.
  # We remove the "Closed" status (which is added in the superclass).
  def status_ids
    @status_ids ||= begin
      status_ids = super
      status_ids.delete(StatusType.CLOSED) unless current_mobile_sdk_app.support_show_closed_requests?
      status_ids.delete(StatusType.ARCHIVED) if current_account.has_mobile_sdk_dont_show_archived_requests?
      status_ids
    end
  end

  # Used to respect the "support_show_closed_requests" features when using JWT authentication.
  # We remove the "Closed" status (which is added in the superclass).
  def ticket_options(*statuses)
    @ticket_options ||= begin
      ticket_options = super

      if current_account.has_mobile_sdk_dont_show_archived_requests?
        ticket_options[:conditions][:status_id].delete(StatusType.ARCHIVED)
      end

      unless current_mobile_sdk_app.support_show_closed_requests?
        ticket_options[:conditions][:status_id].delete(StatusType.CLOSED)
      end

      ticket_options
    end
  end

  def csat_enabled?
    current_mobile_sdk_app.support_show_csat?
  end

  def tokens_params
    @tokens_params ||= params[:tokens].try(:split, ",")
  end
end
