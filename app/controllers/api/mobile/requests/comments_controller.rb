class Api::Mobile::Requests::CommentsController < Api::V2::Requests::CommentsController
  include Api::Mobile::MobileOnlyAccess

  include Zendesk::MobileSdk::RequestsSupport
  include Zendesk::MobileSdk::ControllerSupport
  include Zendesk::MobileSdk::Monitoring

  before_action :allow_only_mobile_sdk_tokens_access

  log_or_throttle_sdk_action_by_account [:index], :mobile_sdk_limit_high

  filter_oauth_scopes :sdk, only: [:index]

  before_action :ensure_ticket_exists

  # A security risk was found where end users with unverified email addresses
  # had access to tickets coming from these unverified email addresses
  # potentially allowing hackers to access other companies via password reset
  # emails.
  #
  # A fix was made https://github.com/zendesk/zendesk/pull/29682 under the
  # 'restrict_end_users_with_unverified_emails' Arturo feature by the secdev
  # team which fixes the vulnerability by preventing the access to the requests
  # endpoint from users with unverified email identities created after 17-09-2017.
  # This same fix caused a side effect of preventing a subset of
  # SDK anonymous users (ones with unverified email user identities) to access
  # the mobile SDK requests endpoint breaking the SDK.
  #
  # This temporarly fix was made skipping the 'ensure_end_user_email_is_verified'
  # and these tests ensure that the SDK continues working on future code changes.
  #
  # We will be skipping the checks only if the
  # 'mobile_sdk_skip_unverified_emails_check' arturo feature is enabled.
  skip_before_action :ensure_end_user_email_is_verified,
    if: -> { current_account.has_mobile_sdk_skip_unverified_emails_check? }

  allow_parameters :all, request_id: Parameters.string

  protected

  def presenter
    Api::Mobile::Requests::CommentPresenter.new(current_user, url_builder: self, includes: includes, brand: current_brand)
  end

  def ticket
    @ticket ||= ticket_scope(params[:request_id])
  end

  def ensure_ticket_exists
    raise ActiveRecord::RecordNotFound unless ticket
  end
end
