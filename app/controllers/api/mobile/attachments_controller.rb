class Api::Mobile::AttachmentsController < Api::Mobile::BaseController
  skip_before_action :require_agent!

  ATTACHMENT_PARAMETER = {
    token: Parameters.string,
    filename: Parameters.string
  }.freeze

  ## ### Gets the thumbnail from the given attachment
  ## `GET /api/mobile/attachment/thumbnail`
  ##
  ## Gets the thumbnail from the given attachment. The attachment is find by
  ## its `token` and `filename`. If the thumbnail exists it's redirected to the
  ## thumbnail URL, otherwise, it returns 404.
  ##
  ## #### Allowed For:
  ##
  ##  * Mobile Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl "/api/mobile/attachment/thumbnail" \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json"
  ## ```
  ##
  ## ```
  allow_parameters :show, ATTACHMENT_PARAMETER
  def show
    attachment = Attachment.
      includes(:thumbnails).
      first_by_token_and_filename(params[:token], params[:filename])

    if attachment.present? && attachment.thumbnailed?
      thumbnail = attachment.thumbnails.first
      redirect_to attachment_by_token_path(id: thumbnail.token, name: thumbnail.filename)
    else
      head :not_found
    end
  end
end
