class Api::Mobile::User::AvatarsController < ::Api::Mobile::BaseController
  skip_before_action :require_agent!

  USER_PARAMETER = {
    userId: Parameters.anything,
  }.freeze

  ## ### Gets the user's avatar
  ## `GET /api/mobile/user/avatar`
  ##
  ## Gets the user's avatar. The user is find by its `id`, parameter `user_id`.
  ## If the user is found and it has a photo, it'll redirect to the photo URL,
  ## otherwise, it returns `:not_found` (HTTP 404).
  ##
  ## #### Allowed For:
  ##
  ##  * Mobile Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl "/api/mobile/user/avatar" \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json"
  ## ```

  allow_parameters :show, user_id: Parameters.string
  def show
    user = User.find_by_id(params[:user_id])

    if user.present? && user.photo.present?
      redirect_to url_for(user.photo.path)
    else
      head :not_found
    end
  end
end
