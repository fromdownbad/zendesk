class Api::Mobile::CurrentUserController < Api::Mobile::BaseController
  include Zendesk::MobileSdk::ControllerSupport

  skip_before_action :require_agent!

  before_action :allow_only_mobile_sdk_tokens_access
  filter_oauth_scopes :sdk

  log_or_throttle_sdk_action_by_account [:show], :mobile_sdk_limit_low
  log_or_throttle_sdk_action_by_account [:update], :mobile_sdk_limit_low

  USER_PARAMETER = {
    user_fields: Parameters.anything
  }.freeze

  ## ### Gets the information about the currently authenticated user
  ## `GET /api/mobile/users/me.json`
  ##
  ## Gets the information about the currently authenticated user
  ##
  ## #### Allowed For:
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl /api/mobile/users/me.json" \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 20O OK
  ## {
  ##   {
  ##   "user": {
  ##     "tags": [
  ##       "foo",
  ##       "bar",
  ##     ],
  ##     "user_fields": {
  ##       "foo": "bar"
  ##     }
  ##   }
  ## }
  ## ```

  allow_parameters :show, {}
  def show
    render json: presenter.present(current_user)
  end

  ## ### Updates the currently authenticated user
  ## `PUT /api/mobile/users/me.json`
  ##
  ## Updates the information of the current authenticated user
  ##
  ## #### Allowed For:
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -X "PUT" "PUT /api/mobile/users/me.json" \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ##  -d $'{
  ##    "user_fields": {
  ##      "foo": "bar"
  ##    }
  ##  }'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 20O OK
  ##
  ##  {
  ##   "user": {
  ##     "tags": [
  ##       "foo",
  ##       "bar",
  ##     ],
  ##     "user_fields": {
  ##       "foo": "bar"
  ##     }
  ##   }
  ##  }
  ## ```
  ## #### Request parameters
  ##
  ## The PUT request takes one parameter, a `user` object that sets one
  ## the device attributes.
  ##
  ## | Name        | Type  | Required | Comments
  ## | ----------- | ----- | -------- | ---------------------------
  ## | user_fields | hash  | no       | Custom fields for the user.
  ##

  allow_parameters :update, user: USER_PARAMETER
  def update
    user_initializer.apply(current_user)
    current_user.save!

    render json: presenter.present(current_user)
  end

  protected

  def presenter
    @presenter ||= Api::Mobile::Users::EndUserPresenter.new(current_user, url_builder: self)
  end

  def user_initializer
    Zendesk::MobileSdk::UserInitializer.new(current_account, current_user, params)
  end
end
