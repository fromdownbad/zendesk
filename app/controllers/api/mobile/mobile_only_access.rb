require 'active_support/concern'

module Api::Mobile::MobileOnlyAccess
  # RAILS5UPGRADE: this is an invalid status code in rails 5
  Rack::Utils::HTTP_STATUS_CODES[450] = 'Blocked by Windows Parental Controls (Microsoft)'

  extend ActiveSupport::Concern
  included do
    before_action :allow_only_zendesk_mobile_access
  end

  private

  def allow_only_zendesk_mobile_access
    return true if current_account.has_mobile_sdk_old_support?
    unless Zendesk::Accounts::Source.zendesk_app_user_agent?(request.user_agent)
      Rails.logger.info("Allowing ONLY mobile app request for #{controller_name}:#{action_name} #{request.url} '#{request.user_agent}'")
      head 450
      notify_statsd
    end
  end
end
