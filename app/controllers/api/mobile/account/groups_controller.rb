class Api::Mobile::Account::GroupsController < Api::Mobile::BaseController
  require_that_user_can! :view, Group, only: :assignable

  # ### Show assignable groups
  # `GET /api/mobile/account/groups/assignable.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/mobile/account/groups/assignable.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "count": 1,
  #   "groups": [{
  #     "id": 11,
  #     "name": "Support",
  #     "agents": [
  #       {
  #         "id":       11,
  #         "name":     "Agent Extraordinaire",
  #         "group_id": 11
  #       },
  #       {
  #         "id":       21,
  #         "name":     "Sally Agent 1",
  #         "group_id": 11
  #       }
  #     ]
  #   }],
  #   next_page: null,
  #   previous_page: null
  # }
  # ```
  allow_parameters :assignable, {}
  require_oauth_scopes :assignable, scopes: %i[users:read read]
  def assignable
    render json: presenter.present(groups)
  end

  private

  def groups
    @groups ||= current_user.preloaded_assignable_groups
  end

  def presenter
    @presenter ||= Api::Mobile::Account::GroupPresenter.new(current_user, url_builder: self, includes: includes)
  end
end
