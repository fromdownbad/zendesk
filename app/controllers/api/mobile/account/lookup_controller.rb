class Api::Mobile::Account::LookupController < Api::Mobile::BaseController
  skip_before_action :enforce_ssl!,
    :require_agent!,
    :authenticate_user,
    :ensure_proper_protocol

  before_action :ensure_mobile_app_access_enabled, :enforce_valid_mobile_products!, :ensure_agent_logins_not_empty
  skip_before_action :enforce_support_is_active!

  # The mobile authentication is only available for account that has at least
  # one of the REQUIRED_PRODUCTS
  REQUIRED_PRODUCTS = %i[support chat sell].freeze

  ## ### Show Settings
  ## `GET /api/mobile/account/lookup.json`
  ##
  ## An unauthorized, non-SSL endpoint for detecting the subdomain and fully qualified
  ## domain name of the account.  This is used by our mobile apps to resolve the account domain
  ## when logging in users who enter a non-SSL, host-mapped domain.
  ##
  ## #### Allowed For:
  ##
  ##  * Anyone
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/mobile/account/lookup.json
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "account" : {
  ##     "subdomain": "example",
  ##     "url": "https://example.zendesk.com",
  ##     "name": "Example Ltd.",
  ##     "agent_logins": [
  ##       {
  ##         "service": "zendesk",
  ##         "url": "https://example.zendesk.com/access/oauth_mobile"
  ##       },
  ##       {
  ##         "service": "google",
  ##         "url": "https://example.zendesk.dev/access/google",
  ##         "play_url": "https://example.zendesk.dev/access/google_play"
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(current_account)
  end

  protected

  def presenter
    @presenter ||= Api::Mobile::Account::LookupPresenter.new(
      current_user,
      url_builder: self,
      params: params,
      session: session,
      request: request,
      ip_address: request.remote_ip,
      active_products: active_products_names,
      agent_logins: agent_logins
    )
  end

  def ensure_mobile_app_access_enabled
    unless current_account.settings.mobile_app_access?
      render json: {error: "MobileAccessDisabled", description: "The administrator for this Zendesk has disabled mobile app access."}, status: 403
    end
  end

  def active_products_names
    active_products.map(&:name)
  end

  def agent_logins
    @agent_logins ||= Zendesk::Auth::AgentLogins.for(current_account, request.remote_ip)
  end

  def enforce_valid_mobile_products!
    if multiproduct? && REQUIRED_PRODUCTS.none? { |product| active_products_names.include?(product) }
      Rails.logger.warn "None of the required products are active for: #{current_account.id}. Denying url: #{request.url}."
      render json: {
        error: 'AccountDoesNotHaveAnyRequiredProduct',
        description: "Your account must have at least one of the following products: #{REQUIRED_PRODUCTS.join(', ')}"
      }, status: :forbidden
    end
  end

  def ensure_agent_logins_not_empty
    if current_account.has_mobile_present_error_on_no_agent_logins? && agent_logins.empty?
      render json: {
        error: 'ContactAdministrator',
        description: "Login restricted. Please contact your administrator"
      }, status: :forbidden
    end
  end
end
