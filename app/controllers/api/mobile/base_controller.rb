class Api::Mobile::BaseController < Api::V2::BaseController
  include Api::Mobile::MobileOnlyAccess
  include Zendesk::MobileSdk::ControllerSupport
  include Zendesk::MobileSdk::Monitoring
end
