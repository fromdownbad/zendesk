class Api::Mobile::UserFieldsController < Api::V2::UserFieldsController
  include Api::Mobile::MobileOnlyAccess
  include Zendesk::MobileSdk::ControllerSupport
  include Zendesk::MobileSdk::Monitoring

  skip_before_action :require_agent!

  before_action :allow_only_mobile_sdk_tokens_access

  filter_oauth_scopes :sdk, only: [:index]

  private

  def external_permissions_enabled?
    false
  end
end
