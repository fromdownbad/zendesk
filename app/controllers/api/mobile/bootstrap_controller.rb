class Api::Mobile::BootstrapController < Api::Mobile::BaseController
  # ### Mobile App Bootstrap
  # `GET /api/mobile/bootstrap.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/mobile/bootstrap.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   TODO
  #   ...
  #
  # }
  # ```
  allow_parameters :index, {}
  def index
    render json: instrument_action { presenter.present(current_user) }
  end

  private

  def presenter
    @presenter ||= Api::Mobile::BootstrapPresenter.new(current_user, url_builder: self, includes: includes)
  end
end
