class Api::Mobile::LookupController < ApplicationController
  include ::AllowedParameters

  # Allows localhost, pod-x-int access
  skip_before_action :verify_current_account,
    :authenticate_user,
    :block_zendesk_mobile_access

  LOOKUP_ENDPOINT = '/api/mobile/account/lookup.json'.freeze
  LOOKUP_SERVICE_NAME = 'lookup'.freeze

  # ### Show Settings
  # `GET /api/mobile/lookup.json`
  #
  # An unauthorized, forced SSL endpoint for detecting the subdomain and fully qualified
  # domain name of the account.  This is used by our mobile apps to resolve the account domain
  # when logging in users who enter a SSL, host-mapped domain.
  #
  # #### Allowed For:
  #
  #  * Anyone
  #
  # ### Available paramaters
  #
  #  * query={search term}
  #
  #  The query can be an account's subdomain or a full hostname
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/mobile/lookup.json?query=test
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "lookup" : {
  #     "subdomain": "example",
  #     "url": "https://example.zendesk.com",
  #     "name": "Example Ltd.",
  #     "agent_logins": [
  #       {
  #         "service": "zendesk",
  #         "url": "https://example.zendesk.com/access/oauth_mobile"
  #       },
  #       {
  #         "service": "google",
  #         "url": "https://example.zendesk.dev/access/google",
  #         "play_url": "https://example.zendesk.dev/access/google_play"
  #       }
  #     ]
  #   }
  # }
  # ```
  allow_parameters :index, query: Parameters.string
  def index
    account.present? ? remote_account_lookup : head(:not_found)
  end

  protected

  def account
    @account ||= begin
      if params[:query].present?
        Account.lookup(params[:query]).first.presence ||
          Account.lookup_by_url(ensure_uri(params[:query]))
      end
    end
  end

  private

  def ensure_uri(query)
    query.start_with?('https') ? query : "https://#{query}"
  end

  def api_client(account)
    Kragle.new(LOOKUP_SERVICE_NAME) do |conn|
      conn.url_prefix = "https://#{account.subdomain}.#{Zendesk::Configuration['host']}"
      conn.headers['User-Agent'] = request.user_agent
    end
  end

  def remote_account_lookup
    response = api_client(account).get(LOOKUP_ENDPOINT)
    render json: response.body
  rescue Kragle::ResponseError => e
    Rails.logger.info "[Api::Mobile::LookupController] Failed to lookup account with id #{account.id} and subdomain #{account.subdomain} - #{e.message}"
    head kragle_exception_to_status_code(e)
  end

  def kragle_exception_to_status_code(exception)
    status_code = exception.to_s.split(" ").first
    status_code.blank? ? 500 : status_code.to_i
  end
end
