# Sub-class of the SatisfactionsRatingController. This allows us to expose CSAT to the SDK without affecting other consumers of this endpoint

class Api::Mobile::SatisfactionRatingsController < Api::V2::SatisfactionRatingsController
  include Zendesk::MobileSdk::ControllerSupport
  include Zendesk::MobileSdk::RequestsSupport

  before_action :allow_only_mobile_sdk_tokens_access
  before_action :require_satisfaction_enabled!, only: %i[create]

  filter_oauth_scopes :sdk, only: %i[create]
  allow_parameters :create, ticket_id: Parameters.string, satisfaction_rating: {
    score: SCORE_PARAMS_ENUM,
    comment: Parameters.string | Parameters.nil,
    reason_code: Parameters.bigid
  }

  protected

  # override super class to find by token
  def ticket
    @ticket ||= find_ticket
  end

  def require_satisfaction_enabled!
    unless current_mobile_sdk_app.support_show_csat?
      render_failure(status: :forbidden, title: 'Forbidden', message: 'CSAT is not supported.')
    end
  end
end
