class Api::Mobile::PushNotificationDevicesController < Api::Mobile::BaseController
  skip_before_action :require_agent!

  before_action :allow_only_mobile_sdk_tokens_access, only: [:create, :destroy]
  before_action :set_current_user_locale, only: [:create]

  before_action :validate_required_create_params, only: :create
  # A noisy exception is observed in the creation action when 2 or more requests are fired within
  # a second, causing a race condition. The reason being: the first request will hit the load balancer
  # and would be routed to one of our application hosts, starting the insertion in the DB, followed
  # by the subsequent requests also being properly routed to another application server while the first
  # hasn't still finished/has not been replicated causing the code to mistakenly try to do `new_push_notification_device.save!`
  # again, only to sadly realise that by this instant the DB is synced and there is already a record there
  # triggering a `ActiveRecord::RecordNotUnique` due to index constraints.
  # See an example of the exception here: https://rollbar-us.zendesk.com/Zendesk/Classic/items/106714
  # We try tp solve this by preventing two requests to hit our application with very short time gap: 1 second.
  # AKA we rate limit.
  before_action :throttle_device_registrations, only: [:create]
  before_action :throttle_device_deregistration, only: [:destroy]
  filter_oauth_scopes :sdk, only: [:create, :destroy]

  ## ### Register a Mobile Device Identifier
  ## `POST /api/mobile/push_notification_devices.json`
  ##
  ## Registers the mobile device for receiving push notifications. The request payload varies
  ## depending on the authentication mechanism used in turn. More specifically when authenticated
  ## via Anonymous you must send the SDK GUID via the 'sdk_guid' parameter.
  ##
  ## #### Allowed For:
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -X "POST" "https://company.zendesk.com/api/mobile/push_notification_devices.json" \
  ##  -H "User-Agent: Zendesk SDK for Android" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ##  -d $'{
  ##    "push_notification_device": {
  ##      "identifier": "3EBA9A0D20",
  ##      "device_type": "iphone",
  ##      "token_type": "urban_airship_channel_id",
  ##      "locale": "fr-FR"
  ##    }
  ##  }'
  ## ```
  ##
  ## For Anonymous authentication (note the the 'sdk_guid' parameter)
  ##
  ## ```bash
  ## curl -X "POST" "https://company.zendesk.com/api/mobile/push_notification_devices.json" \
  ##  -H "User-Agent: Zendesk SDK for Android" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ##  -d $'{
  ##    "push_notification_device": {
  ##      "identifier": "3EBA9A0D20",
  ##      "device_type": "iphone",
  ##      "token_type": "urban_airship_channel_id",
  ##      "locale": "fr-FR"
  ##      "sdk_guid": "BDE7F320-A043-4371-BD16-47811E9C4B8F"
  ##    }
  ##  }'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## {
  ##  "push_notification_device": {
  ##    "id": 89,
  ##    "url": "https://company.zendesk.com/api/mobile/push_notification_devices/89.json",
  ##    "identifier": "3EBA9A0D2",
  ##    "device_type": "iphone",
  ##    "token_type": "urban_airship_channel_id",
  ##    "active": true,
  ##    "created_at": "2015/03/05 20:19:45 +1000",
  ##    "updated_at": "2015/03/05 20:19:45 +1000"
  ##   }
  ## }
  ## ```
  ## #### Request parameters
  ##
  ## The POST request takes one parameter, a `push_notification_device` object that sets one
  ## the device attributes.
  ##
  ## | Name            | Type      | Required  | Comments
  ## | --------------- | ----------| --------- | -------------------
  ## | identifier      | string    | yes       | The push notification device token.
  ## | device_type     | string    | yes       | The push notification device type.
  ## | locale          | string    | yes       | The locale desired for push notifications.
  ## | sdk_guid        | string    | no        | SDK GUID to be sent when the authenticated via anonymous.
  ## | token_type      | string    | no        | Type of token one of "urban_airship_channel_id",
  ##                                             "urban_airship_apid" or "urban_airship_device_token"
  ##                                             (only valid for Urban Airship).
  ##
  create_params = {
    identifier: Parameters.string,
    locale: Parameters.string,
    sdk_guid: Parameters.string,
    device_type: Parameters.string,
    token_type: Parameters.enum(*PushNotifications::DeviceIdentifier::TOKEN_TYPES) | Parameters.nil
  }

  allow_parameters :create, push_notification_device: create_params
  def create
    push_notification_device ||= if anonymous_authenticated?
      if device_identifier_param.is_a?(Integer)
        PushNotifications::SdkAnonymousDeviceIdentifier.where(
          user_identity_id: current_sdk_identity, id: device_identifier_param
        ).first
      else
        PushNotifications::SdkAnonymousDeviceIdentifier.where(
          user_identity_id: current_sdk_identity, token: device_identifier_param
        ).first
      end
    else
      current_user.device_identifiers.where(token: device_identifier_param).first
    end

    if push_notification_device
      push_notification_device.update_attribute(:active, true)
      render json: presenter.present(push_notification_device), status: :ok
    else
      if new_push_notification_device && new_push_notification_device.save!
        render json: presenter.present(new_push_notification_device), status: :created
      end
    end
  end

  ## ### Unregister a Mobile Device Identifier
  ## `DELETE https://company.zendesk.com/api/mobile/push_notification_devices/89.json`
  ##
  ## Unregisters the mobile device in order to stop receiving push notifications.
  ##
  ## #### Allowed For:
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ##  curl -X "DELETE" "https://company.zendesk.com/api/mobile/push_notification_devices/6840283.json" \
  ##  -H "User-Agent: Zendesk SDK for Android" \
  ##  -H "Authorization: Bearer 61cd500bab47d616113ab81f7d4973ccdda255cfac232d18826427c0b6b3ecba" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :destroy, id: Parameters.bigid | Parameters.string
  def destroy
    push_notification_devices ||= if anonymous_authenticated?
      anonymous_scope = current_user.identities.sdk.pluck(:id)
      if device_identifier_param.is_a?(Integer)
        PushNotifications::SdkAnonymousDeviceIdentifier.where(
          user_identity_id: anonymous_scope, id: device_identifier_param
        ).all
      else
        PushNotifications::SdkAnonymousDeviceIdentifier.where(
          user_identity_id: anonymous_scope, token: device_identifier_param
        ).all
      end
    else
      [get_jwt_device_identifier]
    end

    if push_notification_devices.any?
      push_notification_devices.each(&:destroy)
      head :ok
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  protected

  def presenter
    @presenter ||= Api::Mobile::PushNotificationDevicePresenter.new(
      current_user,
      url_builder: self,
      params: params,
      session: session,
      request: request
    )
  end

  private

  def new_push_notification_device
    mobile_sdk_auth = current_account.mobile_sdk_auths.find_by_client_id(oauth_access_token.client.try(:id))
    mobile_sdk_app = mobile_sdk_auth.mobile_sdk_app

    @new_push_notification_device ||= if anonymous_authenticated?
      PushNotifications::SdkAnonymousDeviceIdentifier.new(
        account: current_account,
        token: params[:push_notification_device][:identifier],
        device_type: params[:push_notification_device][:device_type],
        token_type: params[:push_notification_device][:token_type],
        user_sdk_identity: current_sdk_identity,
        mobile_sdk_app: mobile_sdk_app
      )
    else
      PushNotifications::SdkAuthenticatedDeviceIdentifier.new(
        account: current_account,
        token: params[:push_notification_device][:identifier],
        device_type: params[:push_notification_device][:device_type],
        token_type: params[:push_notification_device][:token_type],
        user: current_user,
        mobile_sdk_app: mobile_sdk_app
      )
    end
  end

  def get_jwt_device_identifier # rubocop:disable Naming/AccessorMethodName
    current_user.device_identifiers.where(token: device_identifier_param).first ||
    current_user.device_identifiers.where(id: device_identifier_param).first
  end

  def set_current_user_locale
    translation_locale = get_translation_locale
    if translation_locale && translation_locale != current_user.translation_locale
      current_user.update_attribute(:translation_locale, translation_locale)
    end
  end

  def get_translation_locale # rubocop:disable Naming/AccessorMethodName
    available_langs = current_account.available_languages
    lang_parser = HttpAcceptLanguage::Parser.new(params[:locale])
    lang_settler = Zendesk::I18n::LanguageSettlement.new(lang_parser, available_langs)
    lang_settler.find_matching_zendesk_locale
  end

  def current_sdk_identity
    @current_sdk_identity ||= current_user.identities.sdk.where(value: params[:push_notification_device][:sdk_guid]).first
  end

  def device_identifier_param
    params[:id] || params[:push_notification_device][:identifier]
  end

  def validate_required_create_params
    params.require(:push_notification_device)
  end

  def throttle_device_registrations
    if current_account.has_mobile_sdk_throttle_push_notification_registrations_1h?
      # 1 request per hour
      Prop.throttle!(:push_notification_device_registrations_1h, [current_account.id, device_identifier_param])
    else
      # 1 request per second
      Prop.throttle!(:push_notification_device_registrations, [current_account.id, device_identifier_param])
    end
  end

  def throttle_device_deregistration
    # 1 request per 15 minutes
    Prop.throttle!(:push_notification_device_deregistrations, [current_account.id, device_identifier_param]) if current_account.has_mobile_sdk_throttle_push_notification_deregistrations?
    Prop.throttle!(:push_notification_device_deregistrations_per_account,
      current_account.id,
      threshold: current_account.settings.push_notification_device_deregistrations_threshold)
  end
end
