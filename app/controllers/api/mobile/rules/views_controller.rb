class Api::Mobile::Rules::ViewsController < Api::V2::Rules::BaseController
  include Api::Mobile::MobileOnlyAccess

  id_or_custom_parameter = Parameters.bigid | Parameters.enum('satisfaction') | Parameters.enum('groups') | Parameters.enum('unsolved') | Parameters.enum('incoming')

  # ### Executing Views
  # `GET /api/mobile/views/{id}/execute.json`
  # `GET /api/mobile/views/satisfaction/execute.json`
  # `GET /api/mobile/views/tickets_in_group/execute.json`
  # `GET /api/mobile/views/unsolved_tickets/execute.json`
  #
  # You execute a view in order to get the tickets that fulfill the conditions of the view.
  #
  # The view execution system is designed for periodic rather than high-frequency API usage. In particular, views that are called very
  # frequently by an API client (more often than once every 5 minutes on average) may be cached by our software. This means
  # that the API client will still receive a result, but that result may have been computed at any time within the last 10
  # minutes.
  #
  # If you're looking for a method to get the latest changes to your Zendesk, we recommend the [incremental tickets API](ticket_export). It
  # can be called as often as once a minute, and will return all the tickets that changed since the last poll.
  #
  # View output sorting can be controlled by passing the sort_by and sort_order parameters in the
  # format described in the table under [view previewing](#previewing-views).
  #
  # Custom output columns can be controlled by passing the output_columns parameter to override the view output columns.
  #
  # #### Allowed For:
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/mobile/views/{id}/execute.json \
  #   -v -u {email}:{password}
  # ```
  #
  # With sort options:
  #
  #  ```bash
  # curl 'https://{subdomain}.zendesk.com/mobile/views/{id}/execute.json?sort_by=id&sort_order=desc' \
  #   -v -u {email}:{password}
  # ```
  # With custom output columns:
  #
  #  ```bash
  # curl 'https://{subdomain}.zendesk.com/mobile/views/{id}/execute.json?sort_by=id&output_columns=assigned,assignee,due_date,via' \
  #   -v -u {email}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "view": {
  #     "id": 25,
  #     "url": ...
  #   },
  #   "rows": [
  #     {
  #       "ticket": { ... },
  #       "locale": "en-US",
  #       "group": 1,
  #       ...
  #     },
  #     ...
  #   ],
  #   "columns": [
  #     {
  #       "id": "locale",
  #       "title": "Locale"
  #     },
  #     {
  #       "id": 5,
  #       "title": "Account",
  #       "url": ...
  #     },
  #     ...
  #   ],
  #  "groups": [ ... ]
  # }
  # ```
  allow_parameters :execute,
    id: id_or_custom_parameter,
    group_by: Parameters.string,
    group_order: Parameters.enum('ASC', 'asc', 'DESC', 'desc') | Parameters.nil_string,
    output_columns: Parameters.string
  def execute
    includes << :view
    render json: row_presenter.present(view_tickets)
  end

  allow_parameters :update, :skip # TODO: make stricter
  allow_parameters :create, :skip # TODO: make stricter

  protected

  def row_presenter
    @row_presenter ||= Api::V2::Rules::ViewRowsPresenter.new(current_user, url_builder: self, view: view, includes: includes)
  end

  def view
    @view ||=
      case params[:id]
      when MobileView::SATISFACTION
        MobileView.satisfaction(current_user)
      when MobileView::GROUPS
        MobileView.groups(current_user)
      when MobileView::UNSOLVED
        MobileView.unsolved(current_user)
      when MobileView::INCOMING
        MobileView.incoming(current_user)
      else
        scope.find(params[:id])
      end
  end

  def scope
    @scope ||= current_user.aggregate_views(only: many_ids, only_viewable: false)
  end

  def view_tickets
    raise ActiveRecord::RecordNotFound, "inactive view" unless view.is_active

    @view_tickets ||= begin
      # allows passing group_by, sort_by, etc. as params on execute
      view.output = view.output.merge(params)
      if params[:output_columns]
        view.output.columns = params[:output_columns].to_s.downcase.split(",").map(&:to_sym)
        if view.output && view.output.columns.size > View::MAX_TABLE_COLUMNS
          raise ActiveRecord::ActiveRecordError, "too many columns"
        end
        view.strip_invalid_output_columns
      end

      options = pagination.merge(paginate: true,
                                 include: :latest_comment, output_type: :table)
      merge_view_caching_options(options, 'v2') # for logging

      view.find_tickets(current_user, options).tap do |t|
        raise ActiveRecord::RecordNotFound, "page out of range" if t.out_of_range?
      end
    end
  end
end
