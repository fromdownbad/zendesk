class Api::Mobile::DevicesController < Api::Mobile::BaseController
  before_action :require_mobile_device_oauth_token
  before_action :require_legacy_tokens, only: :register
  before_action :block_legacy_tokens, only: :logout
  before_action :require_device, only: :register

  ## ### Mobile App Device Registration
  ## `POST /api/mobile/devices/register.json`
  ## This API endpoint is intented to be used by the mobile apps to transistion the mobile apps
  ## to using a per device mobile oauth token.
  ##
  ## #### Allowed For
  ##
  ##  * All users with a mobile oauth token
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v https://{subdomain}.zendesk.com/api/mobile/devices/register.json \
  ##   -H "Client-Identifier: zendesk_mobile_ios" \
  ##   -H "Authorization: Bearer {oauth_token}" \
  ##   -H "User-Agent: Zendesk for iPhone"
  ##   -H "Content-Type: application/json" -X POST -d '{"device": {"name": "Morten's iPhone", "identifier": "XXX-YYY-ZZZ"}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "token": "ABC123..."
  ## }
  ## ```
  allow_parameters :register, device: {
    identifier: Parameters.string,
    name: Parameters.string
  }
  def register
    new_device = @mobile_device.create_token_specific_device!(request: request, token: oauth_access_token, device: params[:device])
    render json: { token: new_device.oauth_token.token(true) }
  end

  ## ### Mobile App Device Logout
  ## `DELETE /api/mobile/devices/logout.json`
  ## This API endpoint is intented to be used by the mobile apps to revoke their current oauth token
  ## when a user logouts out of the app
  ##
  ## #### Allowed For
  ##
  ##  * All users with a mobile oauth token
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v https://{subdomain}.zendesk.com/api/mobile/devices/logout.json \
  ##   -H "Client-Identifier: zendesk_mobile_ios" \
  ##   -H "Authorization: Bearer {oauth_token}" \
  ##   -H "User-Agent: Zendesk for iPhone"
  ##   -H "Content-Type: application/json" -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## ```
  allow_parameters :logout, {}
  def logout
    oauth_access_token.destroy

    if mobile_device = current_account.mobile_devices.where(oauth_token_id: oauth_access_token.id).first
      mobile_device.oauth_token = nil
      mobile_device.save!
    end

    head :ok
  end

  private

  def require_device
    if params[:device].blank?
      render json: { status: "No device" }, status: :unprocessable_entity
    end
  end

  def require_mobile_device_oauth_token
    unless oauth_access_token
      Rails.logger.error "Blocking request since an oauth token is required"
      head :forbidden
    end
  end

  def require_legacy_tokens
    unless @mobile_device = current_account.mobile_devices.where(token: oauth_access_token.id).first
      Rails.logger.error "Blocking request since this token is not a mobile oauth token scoped per oauth client"
      head :forbidden
    end
  end

  def block_legacy_tokens
    unless current_account.mobile_devices.where(oauth_token_id: oauth_access_token.id).exists?
      Rails.logger.error "Blocking request since this token is a mobile oauth token scoped per oauth client"
      head :forbidden
    end
  end
end
