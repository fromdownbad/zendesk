# Used by Mobile Support SDK 1.X boot process
# Mobile Core SDK uses `Api::Private::MobileSdk::SettingsController`
class Api::Mobile::Sdk::SettingsController < Api::Mobile::BaseController
  HC_API_ERRORS = [
    ZendeskAPI::Error::ClientError,
    ZendeskAPI::Error::RecordInvalid,
    ZendeskAPI::Error::NetworkError,
    ZendeskAPI::Error::RecordNotFound,
    SocketError,
    Timeout::Error,
    Errno::ECONNREFUSED,
    Errno::EHOSTDOWN,
    Errno::EHOSTUNREACH,
    Errno::ETIMEDOUT
  ].freeze
  LOCALES_ENDPOINT = '/hc/api/v2/locales.json'.freeze
  LOCALES_SERVICE_NAME = 'locales'.freeze

  before_action :allow_only_mobile_sdk_clients_access
  before_action :set_cache_control_header
  after_action :disable_session_set_cookie

  skip_before_action :require_agent!
  skip_before_action :authenticate_user
  # skip_before_action :ensure_proper_protocol

  allow_parameters :show, id: Parameters.string
  def show
    if mobile_sdk_app.settings.helpcenter_enabled
      mobile_sdk_app.help_center_settings = help_center_settings
    end

    render json: presenter.present(mobile_sdk_app)
  end

  protected

  def presenter
    @presenter ||= Api::Mobile::Sdk::SettingsPresenter.new(
      current_user, url_builder: self, params: params, session: session, request: request
    )
  end

  private

  def mobile_sdk_app
    @mobile_sdk_app ||= current_account.mobile_sdk_apps.find_by_identifier!(params[:id])
  end

  def help_center_settings
    found_locale = begin
      hc_languages = help_center_available_languages(help_center_locales)
      settle = Zendesk::I18n::LanguageSettlement.new(http_accept_language, hc_languages)
      settle.find_matching_zendesk_locale.try(:locale) || help_center_default_locale
    end
    {
      locale: found_locale.downcase
    }
  end

  def help_center_default_locale
    help_center_locales.first ||
    current_account.translation_locale.try(:locale).try(:downcase) ||
    'en-us'
  end

  def help_center_available_languages(locales)
    current_account.available_languages.select do |l|
      locales.include?(l.locale.downcase)
    end
  end

  def help_center_locales
    hc_locales = Rails.cache.read(cache_key)
    return hc_locales if hc_locales
    hc_locales = fetch_locales
    Rails.cache.write(cache_key, hc_locales, expires_in: 10.minutes) unless hc_locales == []
    hc_locales
  end

  def cache_key
    @cache_key ||= "#{current_account.id}/#{current_brand.subdomain}/#{LOCALES_ENDPOINT}"
  end

  def fetch_locales
    hc_locales = api_client.get(LOCALES_ENDPOINT).body
    hc_locales.values_at('default_locale', 'locales').flatten
  rescue Kragle::ResponseError => e
    Rails.logger.info "[Api::Mobile::Sdk::SettingsController] Failed to fetch locales from Help Center using the subdomain #{current_brand.subdomain} - #{e.message}"
    []
  end

  def api_client
    Kragle.new(LOCALES_SERVICE_NAME) do |conn|
      conn.url_prefix = "https://#{current_brand.subdomain}.#{Zendesk::Configuration['host']}"
      conn.headers['Accept'] = 'application/json'
    end
  end

  # Cache control will be used by Cloudflare to cache the Mobile SDK settings
  # payload.
  def set_cache_control_header
    if current_account.has_mobile_sdk_settings_cache_control_headers?
      expires_in(10.minutes, public: true)
    end
  end

  def prevent_missing_account_access
    unless current_account.is_serviceable?
      expires_in(10.minutes, public: true)
    end
  rescue NoMethodError
    expires_in(10.minutes, public: true)
  ensure
    super
  end
end
