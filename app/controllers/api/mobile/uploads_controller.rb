class Api::Mobile::UploadsController < Api::V2::UploadsController
  include Api::Mobile::MobileOnlyAccess
  include Zendesk::MobileSdk::Monitoring
  include Zendesk::MobileSdk::ControllerSupport

  before_action :allow_only_mobile_sdk_tokens_access

  log_or_throttle_sdk_action_by_account [:create], :mobile_sdk_limit_low

  filter_oauth_scopes :sdk, only: [:create, :destroy]
end
