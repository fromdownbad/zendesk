class Api::Mobile::UserTagsController < Api::Mobile::BaseController
  include Zendesk::MobileSdk::ControllerSupport

  skip_before_action :require_agent!

  before_action :allow_only_mobile_sdk_tokens_access
  before_action :check_user_tags_setting

  log_or_throttle_sdk_action_by_account [:create], :mobile_sdk_limit_high
  log_or_throttle_sdk_action_by_account [:destroy_many], :mobile_sdk_limit_low

  filter_oauth_scopes :sdk

  TAGS_PARAMETER = {
    tags: Parameters.array(Parameters.string) | Parameters.string
  }.freeze
  ## ### Adds given tags to currently authenticated user
  ## `POST /api/mobile/user_tags.json`
  ##
  ## Adds the provided tags to the current authenticated user.
  ##
  ## #### Allowed For:
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -X "POST" "https://company.zendesk.com/api/mobile/user_tags.json" \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ##  -d $'{
  ##    "tags": "foo, bar"
  ##  }'
  ## ```
  ##
  ##  Supports comma separated string literals and also an array of string literals:
  ##
  ## ```bash
  ## curl -X "POST" "https://company.zendesk.com/api/mobile/user_tags.json" \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ##  -d $'{
  ##    "tags": ["foo", "bar"]
  ##  }'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 20O OK
  ##
  ## {
  ##   "user": {
  ##     "tags": ["foo", "bar"],
  ##     "user_fields": {
  ##       "foo": "bar"
  ##     }
  ##   }
  ## }
  ## ```
  ## #### Request parameters
  ##
  ## The POST request takes one parameter, a `tags` object that sets one
  ## the device attributes.
  ##
  ## | Name  | Type                        | Required  | Comments
  ## | ----- | --------------------------- | --------- | -------------------
  ## | tags  | string or array of strings  | yes       | The push tags to be added to the user.
  ##

  allow_parameters :create, TAGS_PARAMETER
  def create
    current_user.update_attributes!(additional_tags: params.require(:tags))
    render json: presenter.present(current_user)
  end

  ## ### Deletes given tags from currently authenticated user
  ## `DELETE /api/mobile/user_tags/destroy_many.json`
  ##
  ## Adds the provided tags to the current authenticated user.
  ##
  ## #### Allowed For:
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -X "DELETE" "https://company.zendesk.com/api/mobile/user_tags/destroy_many.json?tags=foo,bar" \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ## ```
  ##
  ##  Supports comma separated string literals and also an array of string literals:
  ##
  ## ```bash
  ## curl -X "DELETE" "https://company.zendesk.com/api/mobile/user_tags/destroy_many.json?tags[]=foo&tags[]=bar \
  ##  -H "User-Agent: Zendesk SDK for iOS/1.2.0.1" \
  ##  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec" \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 20O OK
  ##
  ## {
  ##   "user": {
  ##     "tags": ["foo", "bar"],
  ##     "user_fields": {
  ##       "foo": "bar"
  ##     }
  ##   }
  ## }
  ## ```
  ## #### Request parameters
  ##
  ## The DELETE request takes one parameter, a `tags` string of tags separated by commas or an array
  ## with the tags.
  ##
  ## | Name  | Type                        | Required  | Comments
  ## | ----- | --------------------------- | --------- | -------------------
  ## | tags  | string or array of strings  | yes       | The push tags to be added to the user.
  ##

  allow_parameters :destroy_many, TAGS_PARAMETER
  def destroy_many
    current_user.update_attributes!(remove_tags: params.require(:tags))
    render json: presenter.present(current_user)
  end

  protected

  def presenter
    @presenter ||= Api::Mobile::Users::EndUserPresenter.new(current_user, url_builder: self)
  end

  private

  def check_user_tags_setting
    unless current_account.settings.has_user_tags?
      render json: {
        error: "UserTagsDisabled",
        description: "User tags are disabled. Enable the feature in order to add/remove tags."
      }, status: :unprocessable_entity
    end
  end
end
