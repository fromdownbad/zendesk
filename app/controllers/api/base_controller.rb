require 'zendesk/system_user/authorizes_subsystem_users'

require 'activerecord/delay_touching' if RAILS4

module Api
  class BaseController < ActionController::Base
    READ_ACTIONS = %i[index show show_many search new].freeze
    WRITE_ACTIONS = %i[create create_many create_or_update_many create_or_update update update_many destroy destroy_many edit].freeze

    before_action :skip_session_persistence
    before_action :enforce_request_format
    before_action :prevent_missing_account_access
    before_action :set_api_version_header
    before_action :set_application_version_header
    before_action :ensure_proper_protocol
    before_action :log_radar_usage
    before_action :revoke_device, if: :invalid_device?

    around_action :delay_touching if RAILS4

    after_action :log_api_usage

    helper_method :current_account

    include Zendesk::ReplicaReads
    include Zendesk::CnameUtils
    include Zendesk::Auth::Warden::ControllerMixin
    self.warden_scope = :api
    private :warden_scope, :warden_scope?

    include Zendesk::Auth::AuthenticatedSessionMixin
    include Zendesk::OAuth::ControllerMixin
    include Zendesk::Auth::CsrfControl
    protect_from_forgery with: :exception
    include SharedCsrfTokenControllerSupport

    before_action :prevent_inactive_account_access
    include Zendesk::SystemUser::AuthorizesSubsystemUsers
    include Zendesk::SystemUserAuthIPValidation
    include Zendesk::CommentOnQueries
    include Zendesk::I18n::LocalizeUser
    include CiaControllerSupport
    include Zendesk::Devices::ControllerMixin
    include Zendesk::SessionManagement::Expiration
    include ZendeskApi::Controller::ZendeskAccess
    include ZendeskApi::Controller::Pagination

    include Zendesk::Elasticsearch::ControllerSupport
    include Zendesk::BlockMonitorChanges
    include Zendesk::Idempotency::ErrorHandling

    filter_oauth_scopes :read, only: READ_ACTIONS
    filter_oauth_scopes :write, only: WRITE_ACTIONS

    # Must happen after authentication
    before_action :throttle_unauthorized_requests

    skip_before_action :redirect_to_lotus

    include Zendesk::Deprecation::ControllerSupport

    rescue_from Zendesk::OAuth::ScopeError do |e|
      render_failure(
        status: :forbidden, title: 'Forbidden',
        message: "You are missing the following required scopes: #{e.missing_scopes.join(",")}"
      )
      Rails.logger.info "You are missing the following required scopes: #{e.missing_scopes.join(",")}"
    end

    rescue_from ActiveRecord::UnknownAttributeError do |exception|
      render_failure(
        title: 'Invalid attribute',
        message: "Invalid attribute: #{exception.message}",
        status: :bad_request
      )
      Rails.logger.info "Bad Request: Invalid attribute: #{exception.message}"
    end

    # TODO: remove this. Production is set to log, and there are a bunch of
    # test relying on :bad_request responses (instead of fixing the issue)
    rescue_from ActionController::UnpermittedParameters do |exception|
      render_failure(
        title: "Unpermitted attributes",
        message: "You are not allowed to pass these attributes: #{exception.params.join(', ')}",
        status: :bad_request
      )
      Rails.logger.info "Bad Request: You are not allowed to pass these attributes: #{exception.params.join(', ')}"
    end

    rescue_from(StrongerParameters::InvalidParameter) do |exception|
      render_failure(
        title: I18n.t("txt.error_message.controllers.stronger_parameters.base_response_title"),
        message: I18n.t('txt.error_message.controllers.stronger_parameters.base_response_text', key: exception.key, message: exception.message),
        status: :bad_request
      )
      Rails.logger.info "Bad Request: InvalidParameter: #{exception.message}"
    end

    rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, with: :handle_key_constraint_violation

    rescue_from ActiveRecord::RecordNotFound do
      render_failure(
        title: I18n.t("txt.errors.not_found.title"),
        message: I18n.t("txt.errors.not_found.message"),
        status: :not_found
      )
    end

    rescue_from Access::Denied do |exception|
      Rails.logger.warn("Access denied: #{exception.message}")
      deny_access
    end

    rescue_from Prop::RateLimited do |e|
      Rails.logger.warn("Returned 503 on rate limited legacy endpoint")
      Rails.logger.warn("API Rate Limited for #{current_account.subdomain}/#{current_account.id}: #{e.message}")
      Rails.application.config.statsd.client.increment('rate_limited', tags: ["handle:#{e.handle}"])
      render plain: e.description, status: :service_unavailable, content_type: Mime[:text].to_s
    end

    protected

    # Note: there was an attempt to do that in zendesk_api_controller but it was
    # not possible without changing the order of the middlewares inside classic,
    # because authentication happens after the rate limiting.
    # So we chose to do it here instead.
    # See https://github.com/zendesk/zendesk_api_controller/compare/v2.6.11...v2.6.12
    def throttle_unauthorized_requests
      has_rate_limit_unauthorized_api_requests = current_account.try(:has_rate_limit_unauthorized_api_requests?)

      client_ip = request.remote_ip
      return if client_ip.blank?

      if current_user_exists?
        # Resets throttling if we do an authorized request after being
        # rate limited
        if Prop.throttled?(:api_unauthorized_requests, client_ip)
          if has_rate_limit_unauthorized_api_requests
            return Prop.reset(:api_unauthorized_requests, client_ip)
          end
        end

        # If we have a user and it's not an anonymous user
        # we won't be rate limiting anything
        return
      end

      # If we are there, current_registered_user is nil, ie. the request
      # is anonymous.
      if has_rate_limit_unauthorized_api_requests
        Prop.throttle!(:api_unauthorized_requests, client_ip)
      else
        if Prop.throttle(:api_unauthorized_requests, client_ip)
          Rails.logger.info("Would have Rate Limited account <#{current_account.subdomain}> id <#{current_account.id}>, current_user id: <#{current_user.try(:id)}>, throttle key: api_unauthorized_requests")
          Rails.application.config.statsd.client.increment('tmp.api.would_have_been_rate_limited_as_unauthorized_req', tags: ["controller:#{controller_path}"])
        end
      end
    end

    def subsystem_request?
      request.env[Zendesk::SystemUser::SignedWardenStrategy::RACK_SYSTEM_API_HEADER] || # We do not check v1 system user authentication, just that the header is sent
        request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN].try(:internal?)       # We do check that the v2 system token is authorized.
    end

    def handle_key_constraint_violation(exception)
      ZendeskExceptions::Logger.record(exception, location: self, message: exception.message, fingerprint: '71652ba75101d71222bba9228e8e4654247712e8')
      render plain: "Database collision - please try again", status: :conflict
    end

    def invalid_device?
      return false if full_session_management?
      device_manager.invalid_device?
    end

    def full_session_management?
      respond_to?(:verify_authenticated_session)
    end

    # RAILS5UPGRADE: Is this still necessary and is xml still the right default?
    # Issue 1:
    # Rails 2: by default responds with with first respond_to block if no format is given
    # Rails 3: 406 if format does not match any respond_to block
    # http://stackoverflow.com/questions/4643738/rails-3-respond-to-default-format
    #
    # Issue 2:
    # cached action without format has the same cache-key for all accept headers
    # cached json -> request xml -> get json
    #
    # -> always set a format, if nothing matches pick xml
    def enforce_request_format
      set_xml_or_json_format
    end

    def set_xml_or_json_format
      requested_format = (params[:format] || request.format)
      params[:format] =
        request.format =
          if requested_format == Mime[:xml]
            "xml"
          elsif requested_format == Mime[:json]
            "json"
          else
            "xml"
          end
    end

    def set_api_version_header
      raise "API version header required in subclasses"
    end

    def set_application_version_header
      response.headers['X-Zendesk-Application-Version'] = GIT_HEAD_TAG
    end

    def ensure_proper_protocol
      if !request.ssl? && current_account && current_account.is_ssl_enabled?
        redirect_to(current_account.url(protocol: 'https') + request.fullpath)
      end
    end

    def deny_access
      if current_registered_user.present?
        render_failure(status: :forbidden, title: 'Forbidden', message: 'You do not have access to this page. Please contact the account owner of this help desk for further help.')
        Rails.logger.info "You do not have access to this page. Please contact the account owner of this help desk for further help."
      else
        # behavior recommended by http://www.ietf.org/rfc/rfc4559.txt
        if !is_browser_request? || Arturo.feature_enabled_for?(:basic_auth_challenge, current_account)
          response.headers['WWW-Authenticate'] ||= 'Basic realm="Web Password"' unless request.xhr?
        else
          response.headers['WWW-Authenticate'] ||= 'Zendesk realm="API"'
        end
        render_failure(status: :unauthorized, title: 'Unauthorized', message: 'You must supply credentials to complete this request')
      end
    end

    def render_failure(options = {})
      set_xml_or_json_format # some controllers disable the before-filter, but we still want to fail nicely
      warden.custom_failure! if options[:status] == :unauthorized
      render partial: 'shared/message', locals: options, status: (options[:status] || 500), layout: false, formats: [(request.format || "html").to_sym]
    end

    # TODO: make this a macro that takes an HTTP method.
    def require_http_delete!
      unless request.delete?
        # TODO: this should be a 405 Method Not Allowed in v2
        render_failure(status: :bad_request, title: '400 Bad Request', message: 'Only DELETE is permitted')
        Rails.logger.info "Only DELETE is permitted"
      end
    end

    def action_cache_key(hash)
      key = params.dup
      key.delete(:action)
      key.delete(:controller)
      key[:account] = current_account.cache_key
      key[:xhr] = request.xhr?
      if current_user.is_agent?
        key[:rules] = current_account.scoped_cache_key(:rules)
        key[:current_user] = current_user.cache_key if current_account.subscription.has_personal_rules?
      end

      {key: Digest::MD5.hexdigest(key.merge(hash).inspect)}
    end

    def is_action_cache_safe? # rubocop:disable Naming/PredicateName
      current_user.nil? || !current_user.is_agent?
    end

    def api_request?
      true
    end

    def log_radar_usage
      radar_socket = request.headers["X-Zendesk-Radar-Socket"] || "no"
      radar_status = request.headers["X-Zendesk-Radar-Status"] || "no"

      Rails.logger.info("[Radar] socket:#{radar_socket}, status:#{radar_status}")
    end

    def log_api_usage
      return if current_account.nil?

      user = if Zendesk::Auth::Warden::BasicStrategy.basic_auth?(request)
        ActionController::HttpAuthentication::Basic.
          user_name_and_password(request).
          first
      else
        current_user&.id
      end

      Rails.logger.info("API request user:#{user}")
    end

    def delay_touching
      # `delay_touching` should probably be removed for rails 5.
      if current_account.present?
        ActiveRecord::Base.delay_touching do
          yield
        end
      else
        yield
      end
    end if RAILS4

    def merge_view_caching_options(options, caller)
      # for logging
      options.merge!(
        basic_auth: Zendesk::Auth::Warden::BasicStrategy.basic_auth?(request),
        caller: caller,
        lotus: request.headers["X-Zendesk-Lotus-Version"]
      )
    end

    # This is overriden by V2::BaseController and ZCA::Api::ControllerExtension
    def skip_session_persistence
      raise 'must be implemented in subclasses'
    end

    # overrides lib/zendesk/auth/csrf_control.rb
    def csrf_check_disabled_for_http_to_https_action?
      false
    end

    def internal_request?
      Zendesk::Net::IPTools.internal_ip_whitelist_bypass?(request.remote_ip)
    end

    def integration_network?
      Zendesk::Net::IPTools.whitelist_handles_ip?(request.remote_ip, Zendesk::Net.config.fetch(:integration_networks))
    end

    # Anything originating from a Lotus browser instance
    def lotus_request?
      request.headers["X-Zendesk-Lotus-Version"].present?
    end

    # remember, app requests are also lotus requests
    def app_request?
      request.headers["X-Zendesk-App-Id"].present?
    end

    # Lotus requests, but not app requests
    def lotus_internal_request?
      lotus_request? && !app_request?
    end

    private

    def is_browser_request? # rubocop:disable Naming/PredicateName
      # The prefix "Mozilla" is sufficient to identify a modern human-operated browser.
      # Reference: http://en.wikipedia.org/wiki/User_agent#Format_for_human-operated_web_browsers
      request.user_agent.to_s.start_with?('Mozilla')
    end
  end
end
