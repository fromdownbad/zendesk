module Api
  module V1
    class BaseController < Api::BaseController
      skip_before_action :skip_session_persistence
      before_action :prevent_auth
      before_action :set_api_deprecation_header

      protected

      def set_api_version_header
        response.headers["X-Zendesk-API-Version"] = "v1"
      end

      def set_api_deprecation_header
        response.headers["X-Zendesk-API-Warn"] = "API v1 is deprecated. Access will be revoked on February 1st, 2013"
      end

      def prevent_auth
        warden.authenticate(:scope => warden_scope) # fill auth_via
        if auth_via?(:basic) && request.user_agent !~ /SalesforceApp|Zendesk for |WordPress/
          render :status => :bad_request, :text => "api v1 basic auth access is disabled"
        end

        if auth_via?(:token)
          render :status => :bad_request, :text => "api v1 OAuth access is disabled"
        end
      end
    end
  end
end
