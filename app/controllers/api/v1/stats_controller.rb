class Api::V1::StatsController < Api::V1::BaseController
  include ::AllowedParameters
  include StatsHelper
  include Zendesk::Stats::StatRollup::Benchmarking

  before_action :check_view_access!, :except => [ :top_n, :median, :latest, :graph_data, :benchmarking ]
  before_action :require_agent!, :only => [ :top_n, :average, :aggregate_average, :median, :aggregate_median, :latest, :summation, :graph_data, :benchmarking ]
  before_action :validate_stats_options
  before_action :find_subject, :except => [ :benchmarking ]  # sets @stats_subject as side-effect
  before_action :set_cache_control
  before_action :check_benchmarking_params, :only => [ :benchmarking ]
  around_action :shift_time_zone

  deprecate_controller with_feature: :deprecate_api_v1_stats

  # api v2 stats still not built -- the status monitor still uses it.
  skip_before_action :prevent_auth

  rescue_from 'Zendesk::Stats::InvalidStatsRequestError' do |exception|
    render :json => {:errors => [exception.message]}, :status => 422
  end

  stats_parameters = {
    assoc_name: Parameters.string,
    desc: Parameters.string,
    duration: Parameters.string,
    end: Parameters.string,
    group: Parameters.string,
    interval: Parameters.string,
    limit: Parameters.string,
    nocache: Parameters.string,
    object_id: Parameters.bigid,
    object_name: Parameters.string,
    object_type: Parameters.string,
    order: Parameters.string,
    origin: Parameters.string,
    page: Parameters.integer,
    per_page: Parameters.integer,
    precise_time: Parameters.string,
    start: Parameters.string,
    stat_name: Parameters.string,
    threshold: Parameters.float
  }

  # /api/v1/stats/graph_data/entry/4/forum_stats_by_entry/entry_view
  # /api/v1/stats/graph_data/account/0/forum_stats_by_account/entry_view
  # /api/v1/stats/graph_data/account/search_account_stats/searches
  require_oauth_scopes :graph_data, scopes: [:read], arturo: true
  allow_parameters :graph_data, stats_parameters
  def graph_data
    setup_scope

    @options[:interval] = 1.day if (@options[:interval] == nil || @options[:interval] == 0)

    results = @stats_scope.graph_data(params[:stat_name], @options.merge(:association => @stats_association))

    if results.any? { |r| r[:num] }
      counts = results.map { |b| b[:num] }
    else
      counts = []
    end

    if @options[:interval] == 1.day
      results = fix_buckets_for_daylight_shift(results, @options[:precise_time])
    else
      results.map! do |bucket|
        bucket_start = bucket[:start]
        if bucket_start
          if @options[:precise_time]
            bucket_start = bucket_start.to_i
          else
            bucket_start = (bucket[:start].to_i + bucket[:start].utc_offset) * 1000
          end
        end

        [
          bucket_start,
          bucket[:value]
        ]
      end
    end

    response.headers['Content-Disposition'] = "attachment; filename=\"#{params[:stat_name]}.json\""
    render :json => {"label" => @stats_subject.name, "object_id" => @stats_subject.id, "data" => results, "counts" => counts}
  end

  require_oauth_scopes :top_n, scopes: [:read], arturo: true
  allow_parameters :top_n, stats_parameters
  def top_n
    setup_scope

    # Do not send in interval even by accident. interval is for graphing alone.
    @options.delete(:interval)

    scope = paginate(@stats_scope.top_n(@options), page: @options[:page], per_page: @options[:per_page])

    render :json => {data: scope.all, total_pages: scope.total_pages, total_entries: scope.total_entries}
  end

  require_oauth_scopes :latest, scopes: [:read], arturo: true
  allow_parameters :latest, stats_parameters
  def latest
    setup_scope
    scope = @stats_scope.latest(@options)
    render json: Api::V1::StatsPresenter.new(@options[:stat_name], scope).to_json
  end

  # transform: /api/v1/stats/sum_top_n/forum/2/forum_stats_by_entry/entry_view
  # to the average views per entry in forum 2 for the given window
  # another example: /api/v1/stats/aggregate_average/account/forum_stats_by_entry/entry_view
  require_oauth_scopes :aggregate_average, scopes: [:read], arturo: true
  allow_parameters :aggregate_average, stats_parameters
  def aggregate_average
    setup_scope

    # Do not send in interval even by accident. interval is for graphing alone.
    @options.delete(:interval)

    render :json => @stats_scope.aggregate_average(@options)
  end

  require_oauth_scopes :average, scopes: [:read], arturo: true
  allow_parameters :average, stats_parameters
  def average
    setup_scope

    # Do not send in interval even by accident. interval is for graphing alone.
    @options.delete(:interval)

    scope = @stats_scope.calculate_average(@options)
    render json: Api::V1::StatsPresenter.new(@options[:stat_name], scope).to_json
  end

  require_oauth_scopes :aggregate_median, scopes: [:read], arturo: true
  allow_parameters :aggregate_median, stats_parameters
  def aggregate_median
    setup_scope

    # Do not send in interval even by accident. interval is for graphing alone.
    @options.delete(:interval)

    render :json => @stats_scope.aggregate_median(@options)
  end

  require_oauth_scopes :median, scopes: [:read], arturo: true
  allow_parameters :median, stats_parameters
  def median
    setup_scope

    # Do not send in interval even by accident. interval is for graphing alone.
    @options.delete(:interval)

    render :json => @stats_scope.calculate_median(@options)
  end

  require_oauth_scopes :summation, scopes: [:read], arturo: true
  allow_parameters :summation, stats_parameters
  def summation
    setup_scope

    # Do not send in interval even by accident. interval is for graphing alone.
    @options.delete(:interval)

    scope =  @stats_scope.summation(@options)
    render json: Api::V1::StatsPresenter.new(@options[:stat_name], scope).to_json
  end

  require_oauth_scopes :lpf_csr, scopes: [:read], arturo: true
  allow_parameters :lpf_csr, stats_parameters
  def lpf_csr
    setup_scope

    # Do not send in interval even by accident. interval is for graphing alone.
    @options.delete(:interval)

    @options.merge!(:threshold => params[:threshold]) if params[:threshold]
    render :json => @stats_scope.lpf_csr(@options)
  end

  # /api/v1/stats/benchmarking/industry/education
  # /api/v1/stats/benchmarking/target_audience/overall
  # Use overall for overall average
  require_oauth_scopes :benchmarking, scopes: [:read], arturo: true
  allow_parameters :benchmarking,
    object_type: Parameters.string,
    object_name: Parameters.string,
    assoc_name: Parameters.string,
    stat_name: Parameters.string
  def benchmarking
    stat = benchmark(params[:object_type], params[:object_name])
    render :json => { :data => stat, :object_type => params[:object_type], :object_name => params[:object_name] }
  end

  private

  def fix_buckets_for_daylight_shift(results, use_precise_time)
    return [] if results.empty?

    results.sort_by { |bucket| bucket[:start] }

    fixed_results = []
    start_sec = results[0][:start]
    start_date = Time.zone.at(start_sec).to_date
    day = 0

    results.each do |bucket|
      date = start_date + day
      if use_precise_time
        timestamp = bucket[:start].to_i
      else
        timestamp = (date.to_time.to_i + date.to_time.utc_offset) * 1000
      end

      fixed_results << [timestamp, bucket[:value]]
      day += 1
    end

    fixed_results
  end

  def setup_scope
    @options = extract_stats_options
    @stats_scope = scoped_stats_association(@stats_subject, params[:assoc_name])
    extract_conditions!(@stats_scope, @options)
  end
end
