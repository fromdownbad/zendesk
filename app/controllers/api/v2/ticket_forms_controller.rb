module Api
  module V2
    class TicketFormsController < Api::V2::BaseController
      include Zendesk::MobileSdk::ControllerSupport

      before_action :require_ticket_forms_feature!, except: [:show, :show_many]
      before_action :ensure_manage_permissions, only: [:create, :update, :destroy, :clone, :reorder],
        if: -> { current_account.has_permissions_ticket_forms_manage? }
      before_action :require_admin!, only: [:create, :update, :destroy, :clone, :reorder],
        unless: -> { current_account.has_permissions_ticket_forms_manage? }
      before_action :set_locale, only: [:show_many]

      log_or_throttle_sdk_action_by_account [:show_many], :mobile_sdk_limit_high

      skip_before_action :require_agent!, only: [:index, :show, :show_many]

      allow_anonymous_users :index, :show_many
      allow_subsystem_user :embeddable, only: [:index]
      allow_subsystem_user :gooddata, only: [:index]
      allow_bime_user only: [:index]
      allow_cors :show_many

      CONDITIONS_PARAMETER = [
        {
          parent_field_id: Parameters.bigid,
          value: Parameters.string | Parameters.boolean,
          child_fields: [
            {
              id: Parameters.bigid,
              is_required: Parameters.boolean,
              required_on_statuses: {
                type: Parameters.string,
                statuses: [Parameters.string]
              }
            }
          ]
        }
      ].freeze

      TICKET_FORM_PARAMETER = {
        name: Parameters.string,
        raw_name: Parameters.string,
        display_name: Parameters.string,
        raw_display_name: Parameters.string,
        position: Parameters.integer,
        default: Parameters.boolean,
        active: Parameters.boolean,
        end_user_visible: Parameters.boolean,
        in_all_brands: Parameters.boolean | Parameters.nil_string,
        restricted_brand_ids: [Parameters.bigid],
        ticket_field_ids: [Parameters.bigid],
        agent_conditions: CONDITIONS_PARAMETER,
        end_user_conditions: CONDITIONS_PARAMETER
      }.freeze

      ## ### List Ticket Forms
      ## `GET /api/v2/ticket_forms.json`
      ##
      ##  Returns a list of all ticket forms for your account if accessed as an admin or agent.
      ##  End users only see ticket forms that have `end_user_visible` set to true.
      ##
      ## #### Allowed For
      ##
      ##  * Anyone
      ##
      ## #### Available Parameters
      ##
      ## You can filter the results with any combination of the following query string parameters.
      ##
      ## | Name                | Type    | Comment                                                                                                                                            |
      ## | ------------        | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
      ## | active              | boolean | true returns active ticket forms; false returns inactive ticket forms. If not present, returns both                                                |
      ## | end_user_visible    | boolean | true returns ticket forms where `end_user_visible`; false returns ticket forms that are not end-user visible. If not present, returns both         |
      ## | fallback_to_default | boolean | true returns the default ticket form when the criteria defined by the parameters results in a set without active and end-user visible ticket forms |
      ## | associated_to_brand | boolean | true returns the ticket forms of the brand specified by the url's subdomain                                                                        |
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms.json?active=true \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_forms": [
      ##     {
      ##       "id":                    47,
      ##       "url":                   "https://company.zendesk.com/api/v2/ticket_forms/47.json",
      ##       "name":                  "Snowboard Problem",
      ##       "raw_name":              "Snowboard Problem",
      ##       "display_name":          "Snowboard Damage",
      ##       "raw_display_name":      "{{dc.my_display_name}}",
      ##       "end_user_visible":      true,
      ##       "position":              9999,
      ##       "active":                true,
      ##       "default":               true,
      ##       "in_all_brands":         false,
      ##       "restricted_brand_ids":  [ 1,4,6,12,34 ],
      ##       "ticket_field_ids":      [ 2,4,5,32,44 ],
      ##       "agent_conditions":      [
      ##                                  {
      ##                                    "parent_field_id": 5,
      ##                                    "value": "matching_value_1",
      ##                                    "child_fields": [
      ##                                      {
      ##                                        "id": 44,
      ##                                        "is_required": false,
      ##                                        "required_on_statuses": {
      ##                                          "type": "SOME_STATUSES",
      ##                                          "statuses": ["new", "open", "pending", "hold"]
      ##                                        }
      ##                                      },
      ##                                      {
      ##                                        "id": 32,
      ##                                        "is_required": true,
      ##                                        "required_on_statuses": {
      ##                                          "type": "SOME_STATUSES",
      ##                                          "statuses": ["solved"]
      ##                                        }
      ##                                      }
      ##                                    ]
      ##                                  },
      ##                                  {
      ##                                    "parent_field_id": 32,
      ##                                    "value": "matching_value_2",
      ##                                    "child_fields": [
      ##                                      {
      ##                                        "id": 44,
      ##                                        "is_required": true,
      ##                                        "required_on_statuses": {
      ##                                          "type": "ALL_STATUSES",
      ##                                        }
      ##                                      },
      ##                                      {
      ##                                        "id": 32,
      ##                                        "is_required": false,
      ##                                        "required_on_statuses": {
      ##                                          "type": "NO_STATUSES",
      ##                                        }
      ##                                      }
      ##                                    ]
      ##                                  }
      ##                                ],
      ##        "end_user_conditions":  [
      ##                                  {
      ##                                    "parent_field_id": 5,
      ##                                    "value": "matching_value_1",
      ##                                    "child_fields": [ { "id": 32, "is_required": true } ]
      ##                                  },
      ##                                  {
      ##                                    "parent_field_id": 32,
      ##                                    "value": "matching_value_2",
      ##                                    "child_fields": [ { "id": 44, "is_required": false } ]
      ##                                  }
      ##                                ],
      ##       "created_at":            "2012-04-02T22:55:29Z",
      ##       "updated_at":            "2012-04-02T22:55:29Z"
      ##     }
      ##   ]
      ## }
      ## ```
      ##
      allow_parameters :index,
        brand_id: Parameters.bigid,
        end_user_visible: Parameters.boolean,
        active: Parameters.boolean,
        associated_to_brand: Parameters.boolean,
        fallback_to_default: Parameters.boolean
      def index
        expires_in(60.seconds) if use_ticket_forms_cache_control_header?

        render json: presenter.present(ticket_forms)
      end

      ## ### Create Ticket Forms
      ## `POST /api/v2/ticket_forms.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms.json \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -d '
      ## {
      ##   "ticket_form": {
      ##     "name": "Snowboard Problem",
      ##     "end_user_visible": true,
      ##     "display_name": "Snowboard Damage",
      ##     "position": 9999,
      ##     "active" : true,
      ##     "in_all_brands": false,
      ##     "restricted_brand_ids": [ 1,4,6,12,34 ],
      ##     "ticket_field_ids": [ 2,4,5,32,44 ],
      ##     "agent_conditions":    [
      ##                              {
      ##                                "parent_field_id": 5,
      ##                                "value": "matching_value_1",
      ##                                "child_fields": [
      ##                                  {
      ##                                    "id": 44,
      ##                                    "is_required": false,
      ##                                    "required_on_statuses": {
      ##                                      "type": "SOME_STATUSES",
      ##                                      "statuses": ["new", "open", "pending", "hold"]
      ##                                    }
      ##                                  },
      ##                                  {
      ##                                    "id": 32,
      ##                                    "is_required": true,
      ##                                    "required_on_statuses": {
      ##                                      "type": "SOME_STATUSES",
      ##                                      "statuses": ["solved"]
      ##                                    }
      ##                                  }
      ##                                ]
      ##                              },
      ##                              {
      ##                                "parent_field_id": 32,
      ##                                "value": "matching_value_2",
      ##                                "child_fields": [
      ##                                  {
      ##                                    "id": 44,
      ##                                    "is_required": true,
      ##                                    "required_on_statuses": {
      ##                                      "type": "ALL_STATUSES",
      ##                                    }
      ##                                  },
      ##                                  {
      ##                                    "id": 32,
      ##                                    "is_required": false,
      ##                                    "required_on_statuses": {
      ##                                      "type": "NO_STATUSES",
      ##                                    }
      ##                                  }
      ##                                ]
      ##                              }
      ##                            ],
      ##     "end_user_conditions": [
      ##                              {
      ##                                "parent_field_id": 5,
      ##                                "value": "matching_value_1",
      ##                                "child_fields": [ { "id": 32, "is_required": true } ]
      ##                              },
      ##                              {
      ##                                "parent_field_id": 32,
      ##                                "value": "matching_value_2",
      ##                                "child_fields": [ { "id": 44, "is_required": false } ]
      ##                              }
      ##                            ],
      ##     "default" : false
      ##   }
      ## }' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/ticket_forms/{id}.json
      ##
      ## {
      ##   "ticket_form": {
      ##     "id":                    47,
      ##     "url":                   "https://company.zendesk.com/api/v2/ticket_forms/47.json",
      ##     "name":                  "Snowboard Problem",
      ##     "raw_name":              "Snowboard Problem",
      ##     "display_name":          "Snowboard Damage",
      ##     "raw_display_name":      "Snowboard Damage",
      ##     "end_user_visible":      true,
      ##     "position":              9999,
      ##     "active":                true,
      ##     "default":               false,
      ##     "in_all_brands":         false,
      ##     "restricted_brand_ids":  [ 1,4,6,12,34 ],
      ##     "ticket_field_ids":      [ 2,4,5,32,44 ],
      ##     "agent_conditions":      [
      ##                                {
      ##                                  "parent_field_id": 5,
      ##                                  "value": "matching_value_1",
      ##                                  "child_fields": [
      ##                                    {
      ##                                      "id": 44,
      ##                                      "is_required": false,
      ##                                      "required_on_statuses": {
      ##                                        "type": "SOME_STATUSES",
      ##                                        "statuses": ["new", "open", "pending", "hold"]
      ##                                      }
      ##                                    },
      ##                                    {
      ##                                      "id": 32,
      ##                                      "is_required": true,
      ##                                      "required_on_statuses": {
      ##                                        "type": "SOME_STATUSES",
      ##                                        "statuses": ["solved"]
      ##                                      }
      ##                                    }
      ##                                  ]
      ##                                },
      ##                                {
      ##                                  "parent_field_id": 32,
      ##                                  "value": "matching_value_2",
      ##                                  "child_fields": [
      ##                                    {
      ##                                      "id": 44,
      ##                                      "is_required": true,
      ##                                      "required_on_statuses": {
      ##                                        "type": "ALL_STATUSES",
      ##                                      }
      ##                                    },
      ##                                    {
      ##                                      "id": 32,
      ##                                      "is_required": false,
      ##                                      "required_on_statuses": {
      ##                                        "type": "NO_STATUSES",
      ##                                      }
      ##                                    }
      ##                                  ]
      ##                                }
      ##                              ],
      ##     "end_user_conditions":   [
      ##                                {
      ##                                  "parent_field_id": 5,
      ##                                  "value": "matching_value_1",
      ##                                  "child_fields": [ { "id": 32, "is_required": true } ]
      ##                                },
      ##                                {
      ##                                  "parent_field_id": 32,
      ##                                  "value": "matching_value_2",
      ##                                  "child_fields": [ { "id": 44, "is_required": false } ]
      ##                                }
      ##                              ],
      ##     "created_at":            "2012-04-02T22:55:29Z",
      ##     "updated_at":            "2012-04-02T22:55:29Z"
      ##   }
      ## }
      ## ```
      allow_parameters :create, ticket_form: TICKET_FORM_PARAMETER
      def create
        new_ticket_form.save!
        render json: presenter.present(new_ticket_form), status: :created, location: presenter.url(new_ticket_form)
      end

      ## ### Show Ticket Form
      ## `GET /api/v2/ticket_forms/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins, Agents, and End Users
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_form": {
      ##     "id":                    47,
      ##     "url":                   "https://company.zendesk.com/api/v2/ticket_forms/47.json",
      ##     "name":                  "Snowboard Problem",
      ##     "raw_name":              "Snowboard Problem",
      ##     "display_name":          "Snowboard Damage",
      ##     "raw_display_name":      "{{dc.my_display_name}}",
      ##     "end_user_visible":      true,
      ##     "position":              9999,
      ##     "active":                true,
      ##     "default":               true,
      ##     "in_all_brands":         false,
      ##     "restricted_brand_ids":  [ 1,4,6,12,34 ],
      ##     "ticket_field_ids":      [ 2,4,5,32,44 ],
      ##     "agent_conditions":      [
      ##                                {
      ##                                  "parent_field_id": 5,
      ##                                  "value": "matching_value_1",
      ##                                  "child_fields": [
      ##                                    {
      ##                                      "id": 44,
      ##                                      "is_required": false,
      ##                                      "required_on_statuses": {
      ##                                        "type": "SOME_STATUSES",
      ##                                        "statuses": ["new", "open", "pending", "hold"]
      ##                                      }
      ##                                    },
      ##                                    {
      ##                                      "id": 32,
      ##                                      "is_required": true,
      ##                                      "required_on_statuses": {
      ##                                        "type": "SOME_STATUSES",
      ##                                        "statuses": ["solved"]
      ##                                      }
      ##                                    }
      ##                                  ]
      ##                                },
      ##                                {
      ##                                  "parent_field_id": 32,
      ##                                  "value": "matching_value_2",
      ##                                  "child_fields": [
      ##                                    {
      ##                                      "id": 44,
      ##                                      "is_required": true,
      ##                                      "required_on_statuses": {
      ##                                        "type": "ALL_STATUSES",
      ##                                      }
      ##                                    },
      ##                                    {
      ##                                      "id": 32,
      ##                                      "is_required": false,
      ##                                      "required_on_statuses": {
      ##                                        "type": "NO_STATUSES",
      ##                                      }
      ##                                    }
      ##                                  ]
      ##                                }
      ##                              ],
      ##     "end_user_conditions":   [
      ##                                {
      ##                                  "parent_field_id": 5,
      ##                                  "value": "matching_value_1",
      ##                                  "child_fields": [ { "id": 32, "is_required": true } ]
      ##                                },
      ##                                {
      ##                                  "parent_field_id": 32,
      ##                                  "value": "matching_value_2",
      ##                                  "child_fields": [ { "id": 44, "is_required": false } ]
      ##                                }
      ##                              ],
      ##     "created_at":            "2012-04-02T22:55:29Z",
      ##     "updated_at":            "2012-04-02T22:55:29Z"
      ##   }
      ## }
      ## ```
      resource_action :show

      ## ### Show Many Ticket Forms
      ## `GET /api/v2/ticket_forms/show_many.json?ids={ids}`
      ##
      ## Accepts a comma separated list of ticket form ids returning up to 100 records.
      ## This endpoint is meant to be used primarily by the mobile SDKs and the Web Widgets.
      ## See [Zendesk Embeddables](https://developer.zendesk.com/embeddables).
      ##
      ## #### Allowed For:
      ##
      ##  * Anyone
      ##
      ## #### Available Parameters
      ##
      ## You can filter the results with any combination of the following query string parameters.
      ##
      ## | Name                | Type    | Comment                                                                                                                                            |
      ## | ------------        | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
      ## | active              | boolean | true returns active ticket forms; false returns inactive ticket forms. If not present, returns both                                                |
      ## | end_user_visible    | boolean | true returns ticket forms where `end_user_visible`; false returns ticket forms that are not end-user visible. If not present, returns both         |
      ## | fallback_to_default | boolean | true returns the default ticket form when the criteria defined by the parameters results in a set without active and end-user visible ticket forms |
      ## | associated_to_brand | boolean | true returns the ticket forms of the brand specified by the url's subdomain                                                                        |
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms/show_many.json?ids=1,2,3 \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_forms": [
      ##     {
      ##       "id":                    47,
      ##       "url":                   "https://company.zendesk.com/api/v2/ticket_forms/47.json",
      ##       "name":                  "Snowboard Problem",
      ##       "raw_name":              "Snowboard Problem",
      ##       "display_name":          "Snowboard Damage",
      ##       "raw_display_name":      "{{dc.my_display_name}}",
      ##       "end_user_visible":      true,
      ##       "position":              9999,
      ##       "active":                true,
      ##       "default":               true,
      ##       "in_all_brands":         false,
      ##       "restricted_brand_ids":  [ 1,4,6,12,34 ],
      ##       "ticket_field_ids":      [ 2,4,5,32,44 ],
      ##       "agent_conditions":      [
      ##                                  {
      ##                                    "parent_field_id": 5,
      ##                                    "value": "matching_value_1",
      ##                                    "child_fields": [
      ##                                      {
      ##                                        "id": 44,
      ##                                        "is_required": false,
      ##                                        "required_on_statuses": {
      ##                                          "type": "SOME_STATUSES",
      ##                                          "statuses": ["new", "open", "pending", "hold"]
      ##                                        }
      ##                                      },
      ##                                      {
      ##                                        "id": 32,
      ##                                        "is_required": true,
      ##                                        "required_on_statuses": {
      ##                                          "type": "SOME_STATUSES",
      ##                                          "statuses": ["solved"]
      ##                                        }
      ##                                      }
      ##                                    ]
      ##                                  },
      ##                                  {
      ##                                    "parent_field_id": 32,
      ##                                    "value": "matching_value_2",
      ##                                    "child_fields": [
      ##                                      {
      ##                                        "id": 44,
      ##                                        "is_required": true,
      ##                                        "required_on_statuses": {
      ##                                          "type": "ALL_STATUSES",
      ##                                        }
      ##                                      },
      ##                                      {
      ##                                        "id": 32,
      ##                                        "is_required": false,
      ##                                        "required_on_statuses": {
      ##                                          "type": "NO_STATUSES",
      ##                                        }
      ##                                      }
      ##                                    ]
      ##                                  }
      ##                                ],
      ##       "end_user_conditions":   [
      ##                                   {
      ##                                     "parent_field_id": 5,
      ##                                     "value": "matching_value_1",
      ##                                     "child_fields": [ { "id": 32, "is_required": true } ]
      ##                                   },
      ##                                   {
      ##                                     "parent_field_id": 32,
      ##                                     "value": "matching_value_2",
      ##                                     "child_fields": [ { "id": 44, "is_required": false } ]
      ##                                   }
      ##                                 ],
      ##       "created_at":            "2012-04-02T22:55:29Z",
      ##       "updated_at":            "2012-04-02T22:55:29Z"
      ##     },
      ##     {
      ##       ...
      ##     }
      ##   ]
      ## }
      ## ```
      ##
      allow_parameters :show_many,
        ids: Parameters.ids,
        active: Parameters.boolean,
        associated_to_brand: Parameters.boolean,
        end_user_visible: Parameters.boolean,
        fallback_to_default: Parameters.boolean
      require_oauth_scopes :show_many, scopes: %i[sdk]
      def show_many
        expires_in(60.seconds) if use_ticket_forms_cache_control_header?

        render json: presenter.present(many_ticket_forms)
      end

      ## ### Update Ticket Forms
      ## `PUT /api/v2/ticket_forms/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms/{id}.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{ "ticket_form": {
      ##           "name": "Snowboard Fixed",
      ##           "display_name": "Snowboard has been fixed",
      ##           "in_all_brands": true,
      ##           "restricted_brand_ids": [],
      ##           "agent_conditions": [],
      ##           "end_user_conditions": []
      ##       }}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## Location: https://{subdomain}.zendesk.com/api/v2/ticket_forms/47.json
      ##
      ## {
      ##   "ticket_form": {
      ##     "id":                    47,
      ##     "url":                   "https://company.zendesk.com/api/v2/ticket_forms/47.json",
      ##     "name":                  "Snowboard Fixed",
      ##     "raw_name":              "Snowboard Fixed",
      ##     "display_name":          "Snowboard has been fixed",
      ##     "raw_display_name":      "Snowboard has been fixed",
      ##     "end_user_visible":      true,
      ##     "position":              9999,
      ##     "active":                true,
      ##     "default":               true,
      ##     "in_all_brands":         true,
      ##     "restricted_brand_ids":  [],
      ##     "ticket_field_ids":      [ 2,4,5,32,44 ],
      ##     "agent_conditions":      [],
      ##     "end_user_conditions":   [],
      ##     "created_at":            "2012-04-02T22:55:29Z",
      ##     "updated_at":            "2012-04-02T22:55:29Z"
      ##   }
      ## }
      ## ```
      ##
      ##
      allow_parameters :update, id: Parameters.bigid, ticket_form: TICKET_FORM_PARAMETER
      def update
        ActiveRecord::Base.delay_touching do
          updated_ticket_form.save!
        end
        # Reload form to send ticket_form_fields in the correct/new order in the response
        render json: presenter.present(updated_ticket_form.reload)
      end

      ## ### Delete Ticket Form
      ## `DELETE /api/v2/ticket_forms/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      ##
      allow_parameters :destroy, id: Parameters.bigid
      def destroy
        if ticket_form.soft_delete
          default_delete_response
        else
          render json: presenter.present_errors(ticket_form), status: :unprocessable_entity
        end
      end

      ## ### Reorder Ticket Forms
      ## `PUT /api/v2/ticket_forms/reorder.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms/reorder.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{"ticket_form_ids": [2, 23, 46, 50]}' \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_forms": [
      ##     {
      ##       "id":                    47,
      ##       "url":                   "https://company.zendesk.com/api/v2/ticket_forms/47.json",
      ##       "name":                  "Snowboard Fixed",
      ##       "raw_name":              "Snowboard Fixed",
      ##       "display_name":          "Snowboard has been fixed",
      ##       "raw_display_name":      "Snowboard has been fixed",
      ##       "end_user_visible":      true,
      ##       "position":              9999,
      ##       "active":                true,
      ##       "default":               true,
      ##       "in_all_brands":         true,
      ##       "restricted_brand_ids":  [],
      ##       "ticket_field_ids":      [ 2,4,5,32,44 ],
      ##       "agent_conditions":      [],
      ##       "end_user_conditions":   [],
      ##       "created_at":            "2012-04-02T22:55:29Z",
      ##       "updated_at":            "2012-04-02T22:55:29Z"
      ##     }
      ##   ]
      ## }
      ## ```
      ##
      allow_parameters :reorder, ticket_form_ids: [Parameters.bigid]
      require_oauth_scopes :reorder, scopes: [:write], arturo: true
      def reorder
        reorder_ticket_forms
        if current_account.has_send_ticket_forms_in_reorder_request?
          # Reload ticket forms to present them using the new order
          render json: presenter.present(current_account.ticket_forms.reload)
        else
          head :ok
        end
      end

      ## ### Clone an already existing ticket form
      ## `POST /api/v2/ticket_forms/{id}/clone.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_forms/{id}/clone.json \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -v -u {email_address}:{password} \
      ##   -d { "prepend_clone_title": true }
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/ticket_forms/55.json
      ##
      ## {
      ##   "ticket_form": {
      ##     "id":                    55,
      ##     "url":                   "https://company.zendesk.com/api/v2/ticket_forms/55.json",
      ##     "name":                  "Snowboard Fixed",
      ##     "raw_name":              "Snowboard Fixed",
      ##     "display_name":          "Snowboard has been fixed",
      ##     "raw_display_name":      "Snowboard has been fixed",
      ##     "end_user_visible":      true,
      ##     "position":              9999,
      ##     "active":                true,
      ##     "default":               true,
      ##     "in_all_brands":         false,
      ##     "restricted_brand_ids":  [ 1,4,6,12,34 ]
      ##     "ticket_field_ids":      [ 2,4,5,32,44 ]
      ##     "agent_conditions":      [],
      ##     "end_user_conditions":   [],
      ##     "created_at":            "2012-04-02T22:55:29Z",
      ##     "updated_at":            "2012-04-02T22:55:29Z"
      ##   }
      ## }
      ## ```
      ##
      ##
      allow_parameters :clone, id: Parameters.bigid, prepend_clone_title: Parameters.boolean
      require_oauth_scopes :clone, scopes: [:write], arturo: true
      def clone
        cloned_ticket_form.save!
        render json: presenter.present(cloned_ticket_form), status: :created, location: presenter.url(new_ticket_form)
      end

      protected

      def ensure_manage_permissions
        deny_access unless current_user.can?(:manage, TicketForm)
      end

      def require_ticket_forms_feature!
        return if current_user&.is_system_user?

        deny_access unless current_account.has_ticket_forms?
      end

      def presenter
        @presenter ||= Api::V2::TicketFormPresenter.new(current_user, url_builder: self, includes: includes, locale: @locale)
      end

      def new_ticket_form
        @new_ticket_form ||= initialized_ticket_form.ticket_form
      end

      def ticket_form
        @ticket_form ||= TicketForm.with_deleted { scope.find(params[:id]) }
      end

      def updated_ticket_form
        @updated_ticket_form ||= initialized_ticket_form.updated_ticket_form
      end

      def cloned_ticket_form
        @cloned_ticket_form ||= begin
          form = initialized_ticket_form.cloned_ticket_form
          if params[:prepend_clone_title]
            form[:name] = I18n.t('txt.admin.model.ticket_form.forms_copy_prefix', form_title: form.name)
          end
          form
        end
      end

      def many_ticket_forms
        many_ids? ? ticket_forms.where(id: many_ids) : ticket_forms
      end

      def ticket_forms
        @ticket_forms ||= scope
      end

      def initialized_ticket_form
        @initialized_ticket_form ||= Zendesk::TicketForms::Initializer.new(current_account, params)
      end

      def scope
        @scope ||= Zendesk::TicketForms::ScopeSelector.new(current_account, current_brand, current_user, params).scope
      end

      def reorder_ticket_forms
        ids = params[:ticket_form_ids].map(&:to_i)
        unless ids.sort == current_account.ticket_forms.map(&:id).sort
          raise Zendesk::UnknownAttributeError, "incomplete ticket forms list"
        end
        TicketForm.reorder_forms(ids)
      end

      def set_locale
        @locale ||= Zendesk::I18n::LanguageSettlement.new(http_accept_language, current_account.available_languages).find_matching_zendesk_locale ||
        I18n.translation_locale
      end

      def use_ticket_forms_cache_control_header?
        return false unless current_account.has_ticket_forms_cache_control_header?

        # Always use cache control except for admins using Lotus because it causes refresh problems
        !(current_user.is_admin? && lotus_internal_request?)
      end
    end
  end
end
