require 'zendesk/tickets/merger'
require 'zendesk_database_support/sensors/aurora_cpu_sensor'

class Api::V2::TicketsController < Api::V2::BaseController
  include Zendesk::Stats::ControllerMixin
  include Zendesk::Tickets::ControllerSupport
  include Zendesk::Tickets::RecentTicketManagement
  include Zendesk::Idempotency
  include Zendesk::CloudflareRateLimiting

  allow_subsystem_user :account_fraud_service, only: [:create]
  allow_subsystem_user :chat, only: [:create]
  allow_subsystem_user :embeddable, only: [:index]
  allow_subsystem_user :symphony, only: [:create, :index, :show, :update, :requested]
  allow_subsystem_user :collaboration, only: [:show, :create]
  allow_subsystem_user :aws_integration, only: [:create]
  allow_gooddata_user only: [:show_many]
  allow_zopim_user only: [:create, :index, :show, :update, :requested]
  allow_cors_from_zendesk_external_domains :index
  allow_cors_from_zendesk_external_domains :create
  allow_cors_from_zendesk_external_domains :update

  require_that_user_can! :view_private_content, :ticket, only: [:show, :related]
  require_that_user_can! :view, :bulk_tickets, only: [:show_many], unless: proc { current_user.is_admin? }

  after_action  :update_recent_tickets, only: [:show, :create]
  before_action :set_locale, only: [:create, :update]
  before_action :register_device_activity, only: [:update]
  before_action :record_lotus_ticket_open, only: [:show]
  before_action :validate_many_tickets_limit, only: [:update_many, :create_many]
  before_action :validate_unique_ids, only: [:update_many]
  before_action :validate_sources, only: [:merge]
  before_action :validate_target, only: [:merge]
  before_action :validate_no_self_merge, only: [:merge]
  before_action :origin_driven_limit, only: [:update]
  before_action only: [:update, :index] do
    throttle_endpoint!(:v2_tickets_controller)
  end
  before_action :ensure_ids_present, only: [:show_many]
  before_action :throttle_destroy, only: [:destroy]
  around_action idempotent_proxy(expiry: 2.hours), only: :create
  before_action :rate_limit_new_accounts, only: [:create]
  before_action :rate_limit_system_users, only: [:update_many]

  require_capability :count_tickets_endpoint, only: :count

  COMMENT_PARAMETER = {
    author_id: Parameters.bigid,
    uploads: Parameters.array(Parameters.string) | Parameters.string,
    value: Parameters.string,
    body: Parameters.string,
    html_body: Parameters.string,
    public: Parameters.anything,
    add_short_url: Parameters.anything,
    channel_back: Parameters.anything,
    reply_option: Parameters.anything,
    channel_source_id: Parameters.string | Parameters.integer,
    screencasts: Parameters.anything # legacy field from deprecated functionality
  }.freeze

  TICKET_PARAMETER = {
    updated_stamp: Parameters.date_time | Parameters.nil_string,
    safe_update: Parameters.boolean | Parameters.nil_string,
    metadata: Parameters.anything,
    external_id: Parameters.string | Parameters.integer,
    type: Parameters.anything,
    subject: Parameters.anything,
    raw_subject: Parameters.anything,
    priority: Parameters.anything,
    status: Parameters.anything,
    requester_id: Parameters.bigid | Parameters.nil_string,
    submitter_id: Parameters.bigid | Parameters.nil_string,
    recipient: Parameters.anything,
    assignee_id: Parameters.bigid | Parameters.nil_string,
    assignee_email: Parameters.string,
    group_id: Parameters.bigid | Parameters.nil_string,
    organization_id: Parameters.bigid | Parameters.nil_string,
    collaborator_ids: Parameters.array(Parameters.bigid) | Parameters.string,
    collaborators: Parameters.anything,
    additional_collaborators: Parameters.anything,
    email_ccs: Parameters.array(Parameters.map),
    followers: Parameters.array(Parameters.map),
    forum_topic_id: Parameters.bigid,
    problem_id: Parameters.anything,
    macro_id: Parameters.bigid,
    macro_ids: Parameters.array(Parameters.bigid),
    due_at: Parameters.date_time | Parameters.nil_string,
    tags: Parameters.anything,
    fields: Parameters.anything,
    custom_fields: Parameters.anything,
    description: Parameters.anything,
    requester: Parameters.anything,
    ticket_form_id: Parameters.bigid | Parameters.nil_string,
    brand_id: Parameters.bigid,
    via_followup_source_id: Parameters.bigid,
    via_id: Parameters.integer, # we should not allow this
    url: Parameters.string, # ZD#315004
    satisfaction_probability: Parameters.float,
    sharing_agreements: [
      {
        id: Parameters.anything,
        custom_fields: Parameters.anything
      }.freeze
    ].freeze,
    sharing_agreement_ids: Parameters.array(Parameters.bigid),
    via: {
      channel: Parameters.anything,
      source: Parameters.anything
    }.freeze,
    system_metadata: {
      client: Parameters.anything,
      ip_address: Parameters.anything
    }.freeze,
    comment: Parameters.map(COMMENT_PARAMETER) | Parameters.string,
    voice_comment: {
      from: Parameters.anything,
      to: Parameters.anything,
      recording_url: Parameters.anything,
      recording_type: Parameters.enum('call', 'voicemail'),
      recorded: Parameters.boolean,
      recording_consent_action: Parameters.enum('caller_opted_out', 'caller_opted_in', 'caller_did_not_opt_out', 'caller_did_not_opt_in') | Parameters.nil_string,
      call_id: Parameters.bigid,
      call_duration: Parameters.anything,
      answered_by_id: Parameters.anything,
      transcription_text: Parameters.anything,
      started_at: Parameters.anything,
      location: Parameters.anything,
      author_id: Parameters.bigid,
      public: Parameters.anything,
      brand_id: Parameters.bigid,
      via_id: Parameters.anything,
      line_type: Parameters.enum(*VoiceComment::LINE_TYPES),
    }.freeze,
    attribute_value_ids: Parameters.array(Parameters.string),
    tde_workspace: {
      workspace: {
        id: Parameters.bigid,
        title: Parameters.string,
      },
      type: Parameters.enum('ADD', 'CHANGE', 'DELETE'),
      previous_workspace: {
        id: Parameters.bigid,
        title: Parameters.string
      }
    }.freeze,
  }.freeze

  BULK_TICKET_PARAMETER = TICKET_PARAMETER.dup.merge!(
    additional_tags: Parameters.anything,
    followers: Parameters.array(Parameters.map) | Parameters.map,
    remove_tags: Parameters.anything
  ).freeze

  TICKET_BATCH_PARAMETER = BULK_TICKET_PARAMETER.merge(id: Parameters.bigid).freeze

  TARGET_DB_CPU = 0.5

  treat_as_read_request :show_many

  # Caused by Ticket#save_updated_ticket_fields
  # since it returns false from a before_save callback.
  # In Rails 3.2, we could raise RecordInvalid from there safely,
  # but in 2.3 Ticket#save would break.
  rescue_from ActiveRecord::RecordNotSaved do
    render json: presenter.present_errors(initialized_ticket), status: :unprocessable_entity
  end

  ## ### List Tickets
  ##
  ## `GET /api/v2/tickets.json`
  ##
  ## To get a list of all tickets in your account, use the
  ## [Incremental Ticket Export endpoint](./incremental_export#incremental-ticket-export).
  ##
  ## For more filter options, use the [Search API](./search).
  ##
  ## You can also sideload related records with the tickets. See [Side-Loading](./side_loading).
  ##
  ## Archived tickets are not included in the response. See
  ## [About archived tickets](https://support.zendesk.com/hc/en-us/articles/203657756) in
  ## the Support Help Center.
  ##
  ## #### Pagination
  ##
  ## * Cursor pagination (recommended)
  ## * Offset pagination
  ##
  ## See [Pagination](./introduction#pagination).
  ##
  ## Returns a maximum of 100 records per page.
  ##
  ## #### Allowed for
  ##
  ##  * Agents
  ##
  ## The following endpoints are also available:
  ##
  ## `GET /api/v2/organizations/{organization_id}/tickets.json`
  ##
  ## `GET /api/v2/users/{user_id}/tickets/requested.json`
  ##
  ## `GET /api/v2/users/{user_id}/tickets/ccd.json`
  ##
  ## `GET /api/v2/users/{user_id}/tickets/assigned.json`
  ##
  ## `GET /api/v2/tickets/recent.json`
  ##
  ## `ccd` lists tickets that the specified user is cc'd on.
  ##
  ## `recent.json` lists tickets that the requesting agent recently viewed
  ## in the agent interface, not recently created or updated tickets (unless by
  ## the agent recently in the agent interface).
  ##
  ## #### Sorting
  ##
  ## By default, tickets are sorted by id from smallest to largest.
  ##
  ## When using cursor pagination, use the following parameter to change the sort order:
  ##
  ## | Name   | Type   | Required | Comments
  ## | ------ | ------ | -------- | --------
  ## | `sort` | string | no       | Possible values are `updated_at`, `id` (ascending order) or `-updated_at`, `-id` (descending order)
  ##
  ## When using offset pagination, use the following parameters to change the sort order:
  ##
  ## | Name         | Type   | Required | Comments
  ## | ------------ | ------ | -------- | --------
  ## | `sort_by`    | string | no       | Possible values are `assignee`, `assignee.name`, `created_at`, `group`, `id`, `locale`, `requester`, `requester.name`, `status`, `subject`, `updated_at`
  ## | `sort_order` | string | no       | One of `asc`, `desc`. Defaults to `asc`
  ##
  ## When sorting by creation date, the first ticket listed may not be the absolute oldest ticket in your account
  ## due to [ticket archiving](https://support.zendesk.com/entries/28452998-Ticket-Archiving).
  ##
  ## The `query` parameter is not supported for this endpoint. Use the
  ## [Search API](./search) to narrow your results with `query`.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":      35436,
  ##       "subject": "Help I need somebody!",
  ##       ...
  ##     },
  ##     {
  ##       "id":      20057623,
  ##       "subject": "Not just anybody!",
  ##       ...
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ##
  ## ### List Tickets by External Id
  ##
  ## `GET /api/v2/tickets.json?external_id={external_id}`
  ##
  ## External ids don't have to be unique for each ticket. As a result,
  ## the request may return multiple tickets with the same external id.
  ##
  ## #### Pagination
  ##
  ## * Cursor pagination (recommended)
  ## * Offset pagination
  ##
  ## See [Pagination](./introduction#pagination).
  ##
  ## Returns a maximum of 100 records per page.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets.json?external_id={external_id} \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":      35436,
  ##       "subject": "Help I need somebody!",
  ##       ...
  ##     },
  ##     {
  ##       "id":      20057623,
  ##       "subject": "Not just anybody!",
  ##       ...
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index, external_id: Parameters.string, organization_id: Parameters.bigid, user_id: Parameters.bigid
  allow_cursor_pagination_v2_parameters :index
  require_oauth_scopes :index, scopes: [:"tickets:read", :read]
  def index
    render json: presenter.present(tickets)
  end

  ## ### Count Tickets
  ##
  ## * `GET /api/v2/tickets/count.json`
  ## * `GET /api/v2/organizations/{organization_id}/tickets/count.json`
  ##
  ## Returns an approximate count of tickets in the account. If the count exceeds 100,000, it is updated every 24 hours.
  ##
  ## The `count[refreshed_at]` property is a timestamp that indicates when the count was last updated.
  ##
  ## **Note**: When the count exceeds 100,000, `count[refreshed_at]` may occasionally be `null`.
  ## This indicates that the count is being updated in the background, and `count[value]` is limited to 100,000 until the update is complete.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/count.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## {
  ##    count: {
  ##      value: 102,
  ##      refreshed_at: "2020-04-06T02:18:17Z"
  ##    },
  ##    links: {
  ##      url: "https://{subdomain}.zendesk.com/api/v2/tickets/count"
  ##    }
  ## }
  ## ```
  allow_parameters :count, external_id: Parameters.string, organization_id: Parameters.bigid
  def count
    render json: { count: record_counter.present, links: { url: request.original_url } }
  end

  allow_parameters :requested, user_id: Parameters.bigid, exclude_archived: Parameters.boolean, exclude_count: Parameters.boolean
  require_oauth_scopes :requested, scopes: [:"tickets:read", :read, :write]
  def requested
    if current_account.has_ticket_requested_cache_control_header?
      expires_in(1.minute)
    end

    render json: presenter.present(requested_tickets)
  end

  allow_parameters :ccd, user_id: Parameters.bigid, exclude_archived: Parameters.boolean, exclude_count: Parameters.boolean
  require_oauth_scopes :ccd, scopes: [:"tickets:read", :read, :write]
  def ccd
    render json: presenter.present(ccd_tickets)
  end

  allow_parameters :followed, user_id: Parameters.bigid, exclude_archived: Parameters.boolean, exclude_count: Parameters.boolean
  require_oauth_scopes :followed, scopes: [:"tickets:read", :read, :write]
  def followed
    render json: presenter.present(followed_tickets)
  end

  allow_parameters :assigned, user_id: Parameters.bigid, exclude_archived: Parameters.boolean, exclude_count: Parameters.boolean
  require_oauth_scopes :assigned, scopes: [:"tickets:read", :read, :write]
  def assigned
    if user.is_end_user? || user.is_light_agent?
      render json: presenter.present([]) # save a lookup
    else
      render json: presenter.present(assigned_tickets)
    end
  end

  allow_parameters :recent, user_id: Parameters.bigid
  require_oauth_scopes :recent, scopes: [:"tickets:read", :read, :write]
  def recent
    render json: presenter.present(recent_tickets)
  end

  ## ### Show Ticket
  ## `GET /api/v2/tickets/{id}.json`
  ##
  ## Returns a number of ticket properties though not the ticket comments.
  ## To get the comments, use [List Comments](./ticket_comments#list-comments).
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket": {
  ##     "id":      35436,
  ##     "subject": "My printer is on fire!",
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.bigid, view_id: Parameters.bigid
  require_oauth_scopes :show, scopes: [:"tickets:read", :read]
  def show
    includes << :deleted_ticket_form if ticket.deleted_ticket_form

    render json: presenter.present(ticket)
  end

  ## ### Show Multiple Tickets
  ## `GET /api/v2/tickets/show_many.json?ids={ids}`
  ##
  ## Accepts a comma separated list of ticket ids to return.
  ##
  ## This endpoint will return up to 100 tickets records.
  ##
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/show_many.json?ids=1,2,3 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## See [Listing Tickets](#example-response)
  allow_parameters :show_many, ids: Parameters.ids
  require_oauth_scopes :show_many, scopes: [:"tickets:read", :read, :write]
  def show_many
    render json: presenter.present(bulk_tickets)
  end

  ## ### Create Ticket
  ## `POST /api/v2/tickets.json`
  ##
  ## Takes a "ticket" object that specifies the ticket properties. The only
  ## required property is "comment". See [Ticket Comments](./ticket_comments).
  ## All writable properties listed in [JSON Format](#json-format) are optional.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets.json \
  ##   -d '{"ticket": {"subject": "My printer is on fire!", "comment": { "body": "The smoke is very colorful." }}}' \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json
  ##
  ## {
  ##   "ticket": {
  ##     "id":      35436,
  ##     "subject": "My printer is on fire!",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "subject":  "My printer is on fire!",
  ##     "comment":  { "body": "The smoke is very colorful." },
  ##     "priority": "urgent"
  ##   }
  ## }
  ## ```
  ##
  ## Note: Submitting a ticket with HTML data will not result in the HTML being stripped out.
  ##
  ## #### Audit Events
  ## An `audit` object is generated and included in the response when you create
  ## a ticket. The `audit` object has an `events` array listing all the updates
  ## made to the new ticket. For more information, see [Ticket Audits](./ticket_audits).
  ## You can also add your own metadata to the `audit` object. See [Setting metadata](#setting-metadata).
  ##
  ## ```javascript
  ## "audit": {
  ##     "events": [
  ##       {
  ##         "id": 206091192907,
  ##         "type": "Create",
  ##         "field_name": "subject",
  ##         "value": "My printer is on fire!"
  ##       },
  ##       {
  ##         "id":206091192547,
  ##         "type": "Comment",
  ##         "body": "The smoke is very colorful.",
  ##         ...
  ##       }
  ##       ...
  ##     ]
  ##   }
  ## }
  ## ```
  ##
  ## #### Creating follow-up tickets
  ##
  ## Once a ticket is closed (as distinct from solved), it can't be reopened. However, you can
  ## create a new ticket that references the closed ticket. To create the follow-up ticket,
  ## include a `via_followup_source_id` parameter that specifies the closed ticket. Example:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets.json \
  ##   -d '{"ticket": {"via_followup_source_id": 103, "comment": {"body": "My printer is still too hot!"}}}' \
  ##   -v -u {email_address}:{password} -X POST -H "Content-Type: application/json"
  ## ```
  ##
  ## The parameter only works with closed tickets. It has no effect with other tickets.
  ##
  ## By default, the first comment author is the authenticated user making the API
  ## request. To set a different author, include an `author_id` property in the
  ## comment object.
  ##
  ## #### Create ticket asynchronously
  ##
  ## Ticket creation can sometimes take a while because of complex business rules.
  ## Asynchronous creation allows you to get the response back quickly and queues a background job to do the actual work.
  ##
  ## To make an asynchronous request, simply add the parameter `async` in your request.
  ##
  ## `POST api/v2/tickets.json?async=true`
  ##
  ## As soon as the request is received, it returns a `202 Accepted`.
  ## The response includes the new ticket `id` with a `job_status` [JSON object](./job_statuses#json-format).
  ## However, you may not be able to fetch the ticket until the job is done.
  ##
  ## **Note**: Some ticket id numbers may get skipped if ticket creation fails.
  ##
  allow_parameters :create, ticket: TICKET_PARAMETER, async: Parameters.boolean, disable_triggers: Parameters.boolean
  require_oauth_scopes :create, scopes: [:"tickets:write", :write]
  def create
    includes << :audit # Side-load the newly created audit in the response

    if params[:async]
      params.require(:ticket).require(:comment)

      json = {
        ticket: {
          id: asynchronous_new_ticket_id
        }
      }
      json.merge!(job_status_presenter.present(asynchronous_ticket_create_job))

      render json: json, status: :accepted
    else
      params.require(:ticket)

      initialized_ticket.save!

      render json: presenter.present(initialized_ticket), status: :created, location: presenter.url(initialized_ticket)
    end
  end

  ## ### Create Many Tickets
  ## `POST /api/v2/tickets/create_many.json`
  ##
  ## Accepts an array of up to 100 ticket objects.
  ## **Note**: Every ticket created with this endpoint may be affected by
  ## your business rules, which can include sending email notifications to your end users.
  ## If you are importing historical tickets or creating more than 1000 tickets,
  ## consider using the [Ticket Bulk Import](https://developer.zendesk.com/rest_api/docs/support/ticket_import#ticket-bulk-import) endpoint.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Example request body
  ##
  ## ```javascript
  ## {
  ##   "tickets": [
  ##     {
  ##       "subject":  "My printer is on fire!",
  ##       "comment":  { "body": "The smoke is very colorful." },
  ##       "priority": "urgent"
  ##     },
  ##     {
  ##       "subject":  "Help",
  ##       "comment":  { "body": "This is a comment" },
  ##       "priority": "normal"
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/create_many.json \
  ##   -d '{"tickets": [{"subject": "My printer is on fire!", "comment": { "body": "The smoke is very colorful." }}, {"subject": "Help!", "comment": { "body": "Help I need somebody." }}]}' \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  allow_parameters :create_many, tickets: [TICKET_PARAMETER]
  require_oauth_scopes :create_many, scopes: [:"tickets:write", :write, :read]
  def create_many
    # RAILS5UPGRADE: Should this just be params.require(:tickets)?
    raise ActionController::ParameterMissing, "Missing tickets parameters" unless params[:tickets]
    new_params = params.merge(from_api: true)
    status = enqueue_job_with_status(TicketBulkCreateJob, new_params)
    render json: job_status_presenter.present(status)
  end

  ## ### Update Ticket
  ## `PUT /api/v2/tickets/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json \
  ##   -H "Content-Type: application/json" \
  ##   -d '{"ticket": {"status": "open", "comment": { "body": "The smoke is very colorful.", "author_id": 494820284 }}}' \
  ##   -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket": {
  ##      "id":      35436,
  ##      "subject": "My printer is on fire!",
  ##      "status":  "open",
  ##      ...
  ##   },
  ##   "audit": {
  ##      "events": [...],
  ##      ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Request Body
  ##
  ## The PUT request takes one parameter, a `ticket` object that lists the values to update. All properties are optional.
  ##
  ## | Name                      | Description                                          |
  ## | ------------------------- | ---------------------------------------------------- |
  ## | subject                   | The subject of the ticket |
  ## | comment                   | An object that adds a comment to the ticket. See [Ticket comments](./ticket_comments). To include an attachment with the comment, see [Attaching files](tickets#attaching-files) |
  ## | requester\_id             | The numeric ID of the user asking for support through the ticket |
  ## | assignee\_id              | The numeric ID of the agent to assign the ticket to |
  ## | assignee\_email           | The email address of the agent to assign the ticket to |
  ## | group\_id                 | The numeric ID of the group to assign the ticket to |
  ## | organization\_id          | The numeric ID of the organization to assign the ticket to. The requester must be an end user and a member of the specified organization  |
  ## | collaborator\_ids         | An array of the numeric IDs of agents or end users to CC. Note that this replaces any existing collaborators. An email notification is sent to them when the ticket is created |
  ## | collaborators             | An array of numeric IDs, emails, or objects containing `name` and `email` properties. See [Setting Collaborators](#setting-collaborators). An email notification is sent to them when the ticket is updated |
  ## | additional\_collaborators | An array of numeric IDs, emails, or objects containing `name` and `email` properties. See [Setting Collaborators](#setting-collaborators). An email notification is sent to them when the ticket is updated |
  ## | followers                 | An array of objects that represent agent followers to add or delete from the ticket. See [Setting followers](#setting-followers) |
  ## | email_ccs                 | An array of objects that represent agent or end users email CCs to add or delete from the ticket. See [Setting email CCs](#setting-email-ccs)  |
  ## | type                      | Allowed values are "problem", "incident", "question", or "task" |
  ## | priority                  | Allowed values are "urgent", "high", "normal", or "low" |
  ## | status                    | Allowed values are "open", "pending", "hold", "solved" or "closed" |
  ## | tags                      | An array of tags to add to the ticket. Note that the tags replace any existing tags. To keep existing tags, see [Updating tag lists](#updating-tag-lists) |
  ## | external\_id              | An ID to link tickets to local records |
  ## | problem\_id               | For tickets of type "incident", the numeric ID of the problem the incident is linked to, if any |
  ## | due\_at                   | For tickets of type "task", the due date of the task. Accepts the ISO 8601 date format (yyyy-mm-dd) |
  ## | custom\_fields            | An array of the custom field objects consisting of ids and values. Any tags defined with the custom field replace existing tags |
  ## | updated\_stamp            | Datetime of last update received from API. See the `safe_update` property
  ## | safe\_update              | Optional boolean. Prevents updates with outdated ticket data (`updated_stamp` property required when true)
  ## | sharing\_agreement\_ids   | An array of the numeric IDs of sharing agreements. Note that this replaces any existing agreements
  ## | macro\_ids                | An array of macro IDs to be recorded in the ticket audit
  ## | attribute\_value\_ids     | An array of the IDs of attribute values to be associated with the ticket
  ##
  ## #### Example Body
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "comment": { "body": "Thanks for choosing Acme Jet Motors.", "public": true },
  ##     "status":  "solved"
  ##   }
  ## }
  ## ```
  ##
  ## #### Audit Events
  ## An `audit` object is generated and included in the response when you update
  ## a ticket. The `audit` object has an `events` array listing all the
  ## updates made to the ticket. If your update does not change the ticket,
  ## an audit won't be created and included in the response. For more
  ## information, see [Ticket Audits](./ticket_audits). You can also add your
  ## own metadata to the `audit` object. See [Setting metadata](#setting-metadata).
  ##
  ## ```javascript
  ## "audit": {
  ##     "events": [
  ##       {
  ##         "id": 2060476287,
  ##         "type": "Change",
  ##         "field_name": "priority",
  ##         "value": "normal",
  ##         "previous_value": "low"
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update, id: Parameters.bigid, ticket: TICKET_PARAMETER, disable_triggers: Parameters.boolean
  require_oauth_scopes :update, scopes: [:"tickets:write", :write]
  def update
    includes << :audit # Side-load the newly created audit in the response

    if initialized_ticket.audit.present? && !current_account.has_enable_triggers_for_new_chat_ticket?
      # TODO: to be removed when we decide to remove `disable_triggers` for ticket updates
      # we acknowledge that the term 'for_new_chat' is misleading in this case
      # if triggers are already disabled, possibly through params, don't override it
      initialized_ticket.audit.disable_triggers ||= active_chats?
    end

    initialized_ticket.save!
    render json: presenter.present(initialized_ticket)
  end

  ## ### Update Many Tickets
  ##
  ## `PUT /api/v2/tickets/update_many.json`
  ##
  ## `PUT /api/v2/tickets/update_many.json?ids={ids}`
  ##
  ## Accepts an array of up to 100 ticket objects, or a comma-separated list
  ## of up to 100 ticket ids.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Bulk updates
  ##
  ## To make the same change to multiple tickets, use the following endpoint
  ## and data format:
  ##
  ## `https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json?ids=1,2,3`
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "status": "solved"
  ##   }
  ## }
  ## ```
  ##
  ## #### Batch updates
  ##
  ## To make different changes to multiple tickets, use the following endpoint
  ## and data format:
  ##
  ## `https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json`
  ##
  ## ```js
  ## {
  ##   "tickets": [
  ##     { "id": 1, "status": "solved" },
  ##     { "id": 2, "status": "pending" }
  ##   ]
  ## }
  ## ```
  ##
  ## Batch updates are automatically safe and fail when there's another update
  ## to the same tickets after the job started. See [Protecting against ticket update collisions](#protecting-against-ticket-update-collisions)
  ##
  ## #### Updating tag lists
  ##
  ## You can use the bulk update format to add or remove tags to the tag
  ## list of each ticket without overwriting the existing tags. To do so,
  ## include the `additional_tags` or `remove_tags` property in the `ticket`
  ## object. Example:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json?ids=1,2,3 \
  ##   -d '{"ticket": {"additional_tags":["a_new_tag"]}}' \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## The `additional_tags` or `remove_tags` properties only work with bulk
  ## updates, not batch updates.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json?ids=1,2,3 \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X PUT \
  ##   -d '{"ticket": {"status": "solved"}}'
  ## ```
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X PUT \
  ##   -d '{"tickets": [{"id": 1, "status": "solved"}, {"id": 2, "status": "pending"}]}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and
  ## queues a background job to do the work. Use the [Show Job Status](./job_statuses#show-job-status)
  ## endpoint to check for the job's completion.
  allow_parameters :update_many, ids: Parameters.ids, ticket: BULK_TICKET_PARAMETER, tickets: [TICKET_BATCH_PARAMETER], disable_triggers: Parameters.boolean
  require_oauth_scopes :update_many, scopes: [:"tickets:write", :write, :read]
  def update_many
    render json: job_status_presenter.present(bulk_update_job)
  end

  ## ### Protecting against ticket update collisions
  ##
  ## To protect against collisions when updating or bulk updating tickets,
  ## set the safe_update property to true and include an updated_stamp
  ## in your request. See [Request body](./tickets#request-body).
  ## The ticket will only be updated if the updated_stamp datetime value you
  ## specified matches or is more recent than the last recorded ticket update
  ## (in other words, matches or is more recent than the ticket's `updated_at` value).
  ##
  ## When updating a single ticket, the response code will indicate if the ticket was successfully
  ## updated.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "subject":"Updated Subject",
  ##     "updated_stamp":"2015-10-14T16:53:51Z",
  ##     "safe_update":"true",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Example safe update error response
  ##
  ## If the `updated_stamp` property does not match or is earlier than
  ## the `updated_at` datetime value for the ticket, the update operation
  ## will fail and an "UpdateConflict" error response will be sent.
  ##
  ## ```http
  ## Status: 409 Conflict
  ## {
  ##    "description": "Safe Update prevented the update due to outdated ticket data.
  ##                    Please fetch the latest ticket data and try again.",
  ##    "error": "UpdateConflict"
  ## }
  ## ```
  ##
  ## When bulk updating tickets, individual ticket objects can also use the
  ## `safe_update` and `updated_stamp` properties.
  ##
  ## If any tickets in the bulk operation fail to update due to the
  ## `updated_stamp` property not matching or being earlier than the
  ## ticket's `updated_at` datetime value, an "UpdateConflict" error
  ## response will be returned in the [Job Status](./job_statuses#show-job-status).
  ##
  ## Batch updates are automatically safe and fail when there's another update
  ## to the same tickets after the job started.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":"9999",
  ##       "subject":"Ticket 9999 Subject Update",
  ##       "updated_stamp":"2015-10-14T16:58:42Z",
  ##       "safe_update":"true"
  ##     },
  ##     {
  ##       "id":"10",
  ##       "subject":"Ticket 10 Subject Update",
  ##       "updated_stamp":"2015-09-24T21:52:46Z",
  ##       "safe_update":"true"
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  ## #### Example [Job Status](./job_statuses#show-job-status) safe update error response
  ##
  ## ```http
  ## {
  ##   "job_status": {
  ##     "results": [
  ##       {
  ##         "index": 0
  ##         "error": "UpdateConflict",
  ##         "id": 9999,
  ##         "details":"Safe Update prevented the update due to outdated ticket data. Please fetch the latest ticket data and try again."
  ##       }
  ##     ],
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## ### Mark Ticket as Spam and Suspend Requester
  ## `PUT /api/v2/tickets/{id}/mark_as_spam.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/mark_as_spam.json \
  ##   -v -u {email_address}:{password} -X PUT -d '{}' -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :mark_as_spam, id: Parameters.bigid
  require_oauth_scopes :mark_as_spam, scopes: [:"tickets:write", :write, :read]
  def mark_as_spam
    if ticket.mark_as_spam_by!(current_user)
      head :ok
    else
      # One of many reasons you wouldn't be able to edit the user
      # is that the user is a foreign user (ie: shared). ZD813844
      render json: {
        error: 'UnprocessableEntity',
        description: I18n.t('txt.ticket.actions.mark_as_spam.error')
      }, status: :unprocessable_entity
    end
  end

  ## ### Bulk Mark Tickets as Spam
  ## `PUT /api/v2/tickets/mark_many_as_spam.json?ids={ids}`
  ##
  ## Accepts a comma-separated list of up to 100 ticket ids.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/mark_many_as_spam.json?ids=1,2,3 \
  ##   -v -u {email_address}:{password} -X PUT -d '{}' -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  allow_parameters :mark_many_as_spam, ids: Parameters.ids
  require_oauth_scopes :mark_many_as_spam, scopes: [:"tickets:write", :write, :read]
  def mark_many_as_spam
    job = enqueue_job_with_status(TicketBulkUpdateJob, params.merge(ticket_ids: many_ids, mark_as_spam: true))
    render json: job_status_presenter.present(job)
  end

  ## ### Merge Tickets into Target Ticket
  ## `POST /api/v2/tickets/{id}/merge.json`
  ##
  ## Merges one or more tickets into the ticket with the specified id.
  ##
  ## See [Merging tickets](https://support.zendesk.com/hc/en-us/articles/203690916)
  ## in the Support Help Center for ticket merging rules.
  ##
  ## Any attachment to the source ticket is copied to the target ticket.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## Agents in the Enterprise account must have merge permissions.
  ## See [Creating custom roles and assigning agents (Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662026)
  ## in the Support Help Center.
  ##
  ## #### Available parameters
  ##
  ## The request takes a data object with the following properties:
  ##
  ## | Name                     | Type    | Required | Comments                                                |
  ## | ------------------------ | ------- | -------- | ------------------------------------------------------- |
  ## | ids                      | array   | yes      | Ids of tickets to merge into the target ticket          |
  ## | target_comment           | string  | no       | Private comment to add to the target ticket             |
  ## | source_comment           | string  | no       | Private comment to add to the source ticket             |
  ## | target_comment_is_public | boolean | no       | Whether comment in target ticket is public or private   |
  ## | source_comment_is_public | boolean | no       | Whether comment in source tickets are public or private |
  ##
  ## The comments are optional but strongly recommended.
  ##
  ## Comments are private and can't be modified in the following cases:
  ##   * Any of the sources or target tickets are private
  ##   * Any of the sources or target tickets were created through Twitter, Facebook or AnyChannel
  ##  In any other case, comments default to private but can be modified with the comment privacy parameters.
  ##
  ## Example
  ##
  ## ```js
  ## {
  ##   "ids": [123, 456, 789]
  ##   "target_comment": "Combining with #123, #456, #789",
  ##   "source_comment": "Closing in favor of #111"
  ## }
  ## ```
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/merge.json \
  ##   -v -u {email_address}:{password} -X POST \
  ##   -d '{"ids": [123, 456, 789], "source_comment": "Closing in favor of #111", "target_comment": "Combining with #123, #456, #789"}' \
  ##   -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  allow_parameters :merge, id: Parameters.bigid, ids: Parameters.array(Parameters.bigid), source_comment: Parameters.string, target_comment: Parameters.string, source_comment_is_public: Parameters.boolean, target_comment_is_public: Parameters.boolean
  require_oauth_scopes :merge, scopes: [:"tickets:write", :write, :read]
  def merge
    options = {
      account_id: current_account.id,
      user_id: current_user.id,
      target: ticket.nice_id,
      sources: params.require(:ids),
      target_comment: params[:target_comment],
      source_comment: params[:source_comment]
    }

    if current_account.has_override_comment_publicity_in_ticket_merge_endpoint?
      options[:target_is_public] = params[:target_comment_is_public]
      options[:source_is_public] = params[:source_comment_is_public]
    end

    job = enqueue_job_with_status(TicketMergeJob, options)
    render json: job_status_presenter.present(job)
  end

  ## ### Ticket Related Information
  ## `GET /api/v2/tickets/{id}/related.json`
  ##
  ## The request returns a data object with the following properties:
  ##
  ## | Name                | Type    | Comment
  ## | ------------------- | ------- | -------
  ## | topic_id            | string  | Related topic in the Web portal (deprecated feature)
  ## | followup_source_ids | array   | Sources to follow up
  ## | jira_issue_ids      | array   | Linked JIRA issues
  ## | from_archive        | boolean | Is true if the current ticket is archived
  ## | incidents           | integer | A count of related incident occurrences
  ## | twitter             | object  | Twitter information associated with the ticket
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/related.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "topic_id": null,
  ##   "followup_source_ids": [],
  ##   "jira_issue_ids": [],
  ##   "from_archive": false,
  ##   "incidents": 7,
  ##   "twitter": {
  ##     "handle_id": 10,
  ##     "profile": {
  ##       "created_at": "2013/01/08 23:24:49 -0800",
  ##       "description": "Zendesk is the leading ...",
  ##       ...
  ##     },
  ##     "direct": false
  ##   }
  ## }
  ## ```
  allow_parameters :related, id: Parameters.bigid
  require_oauth_scopes :related, scopes: [:"tickets:read", :read, :write]
  def related
    render json: ticket_related_presenter.present(ticket)
  end

  ## ### Setting collaborators
  ##
  ## **Note**: If the [CCs and Followers](https://support.zendesk.com/hc/en-us/articles/203690846)
  ## feature is enabled for the account, using email CCs and followers is preferred. See
  ## [Setting email CCs](#setting-email-ccs) and [Setting followers](#setting-followers).
  ## You can still set collaborators with the `collaborators` and
  ## `additional_collaborators` ticket properties described in this section.
  ## They'll automatically make end users CCs and agents followers.
  ##
  ## When creating or updating a ticket, you can set collaborators on a ticket using one of three properties:
  ## `collaborator_ids`, `collaborators` or `additional_collaborators`. The `collaborators` property provides more flexibility, as outlined
  ## below.
  ##
  ## An email notification is sent to the collaborators when the ticket is created or updated.
  ##
  ## Setting `collaborators` or `collaborator_ids` when updating a ticket replaces existing collaborators. Make sure
  ## to include existing collaborators in the update request if you want to retain them on the ticket.
  ## If you only want to add more collaborators, use `additional_collaborators` instead.
  ##
  ## The `collaborator_ids` property is a basic option that takes an array of user ids.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "collaborator_ids": [562624, 624562, 243642]
  ##   }
  ## }
  ## ```
  ##
  ## The `collaborators` property is a more flexible option. It takes an array consisting of a combination of user ids,
  ## email addresses, or objects containing a `name` and `email` property.
  ##
  ## *  562562562
  ## * "someone@example.com"
  ## * `{ "name": "Someone Special", "email": "someone@example.com" }`
  ##
  ## Use an object to create the user on the fly with the appropriate name in Zendesk Support.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "collaborators": [ 562, "someone@example.com", { "name": "Someone Else", "email": "else@example.com" } ]
  ##   }
  ## }
  ## ```
  ##
  ## The `additional_collaborators` property is used to add additional collaborators. It preserves the existing collaborators,
  ## and adds new collaborators to the ticket. It takes an array consisting of a combination of user ids,
  ## email addresses, or objects containing a `name` and `email` property.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "additional_collaborators": [ 562, "someone@example.com", { "name": "Someone Else", "email": "else@example.com" } ]
  ##   }
  ## }
  ## ```
  ##
  ## Even if you use the `collaborators` property, the JSON response will specify the new or updated collaborators
  ## as an array of user ids in the `collaborator_ids` property. The response doesn't have a `collaborators` property.

  ## ### Setting followers
  ##
  ## When creating or updating a ticket, you can set followers on a ticket using the `followers` property.
  ##
  ## An email notification is sent to the followers when the ticket is created or updated.
  ##
  ## The `followers` property takes an array of objects representing agents. Each object must have an identifier, either user ID (`user_id`) or email address (`user_email`). If a
  ## user_email or user_id is given which doesn't exist in the account, that user object is ignored. Additionally, the `action` key can be set to either "put" or "delete" to
  ## indicate whether the user should be added to or removed from the followers list. If the `action` key is not given, the default value is "put".
  ##
  ## #### Example request setting followers
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "followers": [
  ##       { "user_id": "562624", "action": "put" },
  ##       { "user_id": "624562" },
  ##       { "user_id": "243642", "action": "delete" },
  ##       { "user_id": "562", "user_email": "existing-user1@example.com", "action": "delete" },
  ##       { "user_email": "existing-user2@example.com", "action": "put" },
  ##     ]
  ##   }
  ## }
  ## ```

  ## ### Setting email CCs
  ##
  ## When creating or updating a ticket, you can set email CCs on a ticket using the `email_ccs` property.
  ##
  ## A single email notification is sent to the requester and CCs when the ticket is created or updated, depending on trigger settings.
  ##
  ## The `email_ccs` property takes an array of objects representing users. Each object must have an identifier, either user ID (`user_id`) or email address (`user_email`). If an
  ## email address is specified and the user doesn't exist in the account, a new user is created. Additionally, the key `action` can be set to either "put" or "delete" to
  ## indicate whether the user should be added to or removed from the email CCs list. If the `action` key is not given, the default value is "put". Optionally, if a new user is
  ## created, you can specify `user_name` to set the new user's name. If the user is implicitly created and a `user_name` is not given, the user name is derived from the
  ## local part of the email address. If an email address not associated with a user is included (for example, with the intention to implicitly create the user), but the value of the
  ## `action` key is "delete", that email address is not be created or added as an email CC.
  ##
  ## A ticket can only have 48 email CCs. If more than 48 email CCs are included in the request, a "400 Bad Request" will be returned. If existing email CCs and the
  ## additional email CCs added through the request add up to more than 48, the email CCs will be truncated to 48 and no error will be reported.
  ##
  ## Email CCs will not be updated if an internal note is also added to the ticket in the same update.
  ##
  ## #### Example request setting email_ccs
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "email_ccs": [
  ##       { "user_id": "562624", "action": "put" },
  ##       { "user_id": "243642": "action": "delete" },
  ##       { "user_email": "else@example.com", "user_name": "Someone Else", "action": "put"}
  ##     ]
  ##   }
  ## }
  ## ```

  ## ### Setting metadata
  ##
  ## When you create or update a ticket, an [Audit](./ticket_audits) gets generated if the ticket properties have changed.
  ## On each such audit, you can add up to 1 kilobyte of custom metadata. You can use this to build your own integrations or apps.
  ## **Note**: If your update does not change the ticket, this will not create an Audit and will not save your metadata.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "metadata": { "time_spent": "4m12s", "account": "integrations" },
  ##     "comment":  { "body": "Please press play on tape now" },
  ##     "status":   "pending"
  ##   }
  ## }
  ## ```
  ##
  ## Note that metadata can only be set as part of other regular ticket updates as they are associated to a such
  ## rather than just the ticket. Zendesk Support also adds metadata on each ticket update, and the resulting audit JSON
  ## structure looks like this:
  ##
  ## ```js
  ## {
  ##   "audit": {
  ##     "id":         35436,
  ##     "ticket_id":  47,
  ##     "created_at": "2012-04-20T22:55:29Z",
  ##     "author_id":  35436,
  ##     "metadata": {
  ##       "custom": {
  ##         "time_spent": "4m12s",
  ##         "account": "integrations"
  ##       },
  ##       "system": {
  ##         "ip_address": "184.106.40.75",
  ##         "location": "United States",
  ##         "longitude": -97,
  ##         "latitude": 38,
  ##         "client": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3)"
  ##       }
  ##     },
  ##     "via": {
  ##       "channel": "web"
  ##     },
  ##     "events": [
  ##       {
  ##         "id":          1564245,
  ##         "type":        "Comment"
  ##         "body":        "Please press play on tape now",
  ##         "public":      true,
  ##         "attachments": []
  ##       },
  ##       ...
  ##     ]
  ##   }
  ## }
  ## ```

  ## ### Attaching files
  ##
  ## When creating and updating tickets, you may attach files by passing in
  ## an array of the tokens received from uploading the files. For the upload attachment
  ## to succeed when updating a ticket, a comment must be included.
  ##
  ## To get the token of an upload, see the [Attachments](./attachments) section on
  ## uploading files.
  ##
  ## The upload tokens are single use only.  After a token is used to attach a file to a ticket comment,
  ## that token cannot be used to attach the same upload to an additional ticket comment.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "comment":  { "body": "Please press play on tape now", "uploads":  ["vz7ll9ud8oofowy"] }
  ##   }
  ## }
  ## ```

  ## ### Creating a ticket with a new requester
  ##
  ## You can specify a requester when creating a ticket.
  ## A name, email, and locale id can be set for the requester. A name and email are required. For locale ids, see the [Locales](./locales) API.
  ##
  ## #### Example
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "subject":   "Hello",
  ##     "comment":   { "body": "Some question" },
  ##     "requester": { "locale_id": 8, "name": "Pablo", "email": "pablito@example.org" }
  ##   }
  ## }
  ## ```
  ##
  ## If the requester's identity doesn't exist in Zendesk, a new user profile
  ## is created on the user's behalf. The user needs to verify their identity
  ## to view their requests.
  ##
  ## If a user exists in Zendesk with the specified email address, then
  ## Zendesk uses that user and makes no updates to existing users during the
  ## ticket creation process. In this approach, only the email attribute is
  ## required.
  ##
  ## ### Setting custom field values
  ##
  ## To set the value of one or more custom fields in a ticket, specify
  ## an array of objects consisting of `id` and `value` properties. For text or
  ## numeric fields, specify text or numbers for the `value` property:
  ##
  ## ```bash
  ##  "custom_fields": [{"id": 25356371, "value": "I bought it at Buy More."}, ...]
  ## ```
  ##
  ## For date fields, specify only the date (formatted as a [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601)
  ## string) and omit the time:
  ##
  ## ```bash
  ##  "custom_fields": [{"id": 25356634, "value": "2020-02-24"}, ...]
  ## ```
  ##
  ## For fields with values defined by tags, such as drop-down lists, specify the
  ## option's tag name for the `value` property, not the option text displayed in
  ## the list. For example, if the option's display text is "HD 3000 color printer" and
  ## its tag is hd_3000, set the custom field's value as follows:
  ##
  ## ```bash
  ##   "custom_fields": [{"id": 21938362, "value": "hd_3000"}]
  ## ```
  ##
  ## Multi-select field values are also defined by tags. However, the value property
  ## is a comma-separated list.  For example, if the values of the tags are hd_3000 and hd_5555,
  ## the custom field's values is as follows:
  ##
  ## ```bash
  ##   "custom_fields": [{"id": 21938362, "value": ["hd_3000", "hd_5555"]}]
  ## ```
  ##
  ## For more information, see [Ticket Fields](./ticket_fields).
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "subject": "Hello",
  ##     "comment": { "body": "Some question" },
  ##     "custom_fields": [{ "id": 34, "value": "I need help!" }]
  ##   }
  ## }
  ## ```

  ## ### Delete Ticket
  ## `DELETE /api/v2/tickets/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents with permission to delete tickets
  ##
  ## Agent delete permissions are set in Support. See
  ## [Deleting tickets](https://support.zendesk.com/hc/en-us/articles/203690936)
  ## in the Support Help Center.
  ##
  ## #### Ticket deletion rate limit
  ##
  ## You can delete 400 tickets every 1 minute using this endpoint.
  ## The rate limiting mechanism behaves as described in
  ## [Rate limits](./introduction#rate-limits) in the API introduction.
  ## Zendesk recommends that you obey the Retry-After header values.
  ## To delete many tickets, you may use [Bulk Delete Tickets](./tickets#bulk-delete-tickets).
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  require_oauth_scopes :destroy, scopes: [:"tickets:write", :write]
  def destroy
    requested_ticket.will_be_saved_by(current_user,
      via_id: lotus_request? ? ViaType.WEB_FORM : ViaType.WEB_SERVICE)
    requested_ticket.soft_delete!
    default_delete_response
  end

  ## ### Bulk Delete Tickets
  ## `DELETE /api/v2/tickets/destroy_many.json?ids={ids}`
  ##
  ## Accepts a comma-separated list of up to 100 ticket ids.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents with permission to delete tickets
  ##
  ## Agent delete permissions are set in Support. See
  ## [Deleting tickets](https://support.zendesk.com/hc/en-us/articles/203690936)
  ## in the Support Help Center.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/destroy_many.json?ids=1,2,3 \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_status": {
  ##     "id": "d4239ba02d5f0132c0b2600308a8421a",
  ##     "url": "https://example.zendesk.com/api/v2/job_statuses/d4239ba02d5f0132c0b2600308a8421a.json",
  ##     "total": null,
  ##     "progress": null,
  ##     "status": "queued",
  ##     "message": null,
  ##     "results": null
  ##   }
  ## }
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids
  require_oauth_scopes :destroy_many, scopes: [:"tickets:write", :write, :read]
  def destroy_many
    job = enqueue_job_with_status(TicketBulkUpdateJob, ticket_ids: many_ids, submit_type: 'delete')
    render json: job_status_presenter.present(job)
  end

  protected

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["lotus", "api"])
  end

  def presenter
    @presenter ||= begin
      options = {
        url_builder: self,
        includes: includes,
        comment_presenter: comment_presenter,
        with_cursor_pagination: with_cursor_pagination_v2?,
        exclude_legacy_fields: with_cursor_pagination_v2?
      }

      Api::V2::Tickets::TicketPresenter.new(current_user, options)
    end
  end

  def comment_presenter
    @comment_presenter ||= begin
      Api::V2::Tickets::GenericCommentPresenter.new(current_user, url_builder: self)
    end
  end

  def job_status_presenter
    @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def ticket_related_presenter
    @ticket_related_presenter ||= Api::V2::Tickets::RelatedPresenter.new(current_user, url_builder: self)
  end

  def bulk_tickets
    @bulk_tickets ||= current_account.tickets.where(nice_id: many_ids).all_with_archived
  end

  def tickets
    scope = Zendesk::Tickets::Finder.new(
      current_account,
      current_user,
      params
    ).tickets

    paginate_tickets(scope, order: "nice_id ASC")
  end

  def paginate_tickets(scope, offset_options = {})
    if with_cursor_pagination_v2?
      paginate_tickets_with_cursor(scope)
    else
      paginate_tickets_with_offset(scope, offset_options)
    end
  end

  def paginate_tickets_with_cursor(scope)
    paginate_with_cursor(sort_tickets_for_cursor(scope))
  end

  def paginate_tickets_with_offset(scope, offset_options)
    if current_account.has_force_ticket_index_index?
      scope = scope.with_best_index(params[:sort_by])
    end
    paginate(scope, offset_options)
  end

  def external_id
    params[:external_id]
  end

  def ccd_tickets
    @ccd_tickets ||= if current_account.has_follower_and_email_cc_collaborations_enabled?
      if current_account.has_comment_email_ccs_allowed_enabled?
        paginate(user.ccd_tickets.for_user(current_user), order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: paginate_method, total_entries: exclude_count?)
      else
        []
      end
    else
      paginate(user.collaborated_tickets.for_user(current_user), order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: paginate_method, total_entries: exclude_count?)
    end
  end

  def followed_tickets
    @followed_tickets ||= if current_account.has_ticket_followers_allowed_enabled?
      paginate(user.followed_tickets.for_user(current_user), order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: paginate_method, total_entries: exclude_count?)
    else
      []
    end
  end

  def assigned_tickets
    @assigned_tickets ||= paginate(user.assigned, order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: paginate_method, total_entries: exclude_count?)
  end

  def requested_tickets
    @requested_tickets ||= filter_tickets_by_current_user
  end

  def view
    @view ||= current_user.aggregate_views(types: [:shared, :personal]).find(params[:view_id])
  end

  def user
    @user ||= current_account.users.find(params[:user_id])
  end

  def ticket
    @ticket ||= if params[:view_id]
      view.find_ticket(params[:id], current_user) || raise(ActiveRecord::RecordNotFound)
    else
      current_account.tickets.find_by_nice_id!(params[:id]).tap do |ticket|
        ticket.current_user = current_user
      end
    end
  end

  def ticket_initializer
    @ticket_initializer ||= Zendesk::Tickets::V2::Initializer.new(current_account, current_user, params, request_params: request_params, include: ticket_finder_includes)
  end

  def set_locale
    I18n.locale = ENGLISH_BY_ZENDESK if request.headers['X-Force-Exception-Locale']
  end

  def record_lotus_ticket_open
    if request.headers["X-Zendesk-Lotus-Version"].present?
      statsd_client.increment('ticket.open', tags: ["api:v2", "version:#{request.headers["X-Zendesk-Lotus-Version"]}"])
    end
  end

  def sort_map
    return unless (map = super)
    @sort_map ||= map.merge(
      'locale' => "FIELD(locale_id, #{current_account.available_languages.map(&:id).join(',')})",
      'status' => Ticket::STATUS_SQL_SORT_BY
    )
  end

  def origin_driven_limit
    if current_account.has_origin_limit_ticket_api_log?
      rate_limit_header(:mild, simulate: !current_account.has_origin_limit_ticket_api?)
    end
  end

  def validate_many_tickets_limit
    if (params[:tickets] || many_ids).try(:length).to_i > many_tickets_limit
      raise ZendeskApi::Controller::Errors::TooManyValuesError.new("", many_tickets_limit)
    end
  end

  def many_tickets_limit
    current_account.settings.many_tickets_limit || per_page
  end

  def validate_unique_ids
    if (params[:tickets].try(:map) { |t| t["id"] } || many_ids).try(:uniq!)
      render json: {
        error: "DuplicateValue",
        description: "ids are not unique"
      }, status: :bad_request
    end
  end

  def source_ids
    params.require(:ids)
  end

  def find_sources
    if current_account.has_follower_and_email_cc_collaborations_enabled?
      @find_sources ||= find_tickets_with_requester(source_ids)
    else
      find_tickets_with_requester(source_ids)
    end
  end

  def find_target
    @target = current_account.tickets.for_user(current_user).not_closed.find_by_nice_id(params[:id], include: [:requester])
  end

  def validate_sources
    unless find_sources.map { |ticket| current_user.can?(:merge, ticket) }.all?
      return render json: {
        error: "SourceInvalid",
        description: "No source ticket provided."
      }, status: :unprocessable_entity
    end
    # this covers the case where some of the sources can't be loaded with current_account.tickets.for_user(current_user)
    # because the user has no access to those tickets
    if find_sources.size != source_ids.size
      return render json: {
        error: "SourceInvalid",
        description: "You do not have access to one or more of the tickets you selected. They may have been deleted or may belong to another user."
      }, status: :unprocessable_entity
    end

    true
  end

  def validate_no_self_merge
    target = find_target
    if find_sources.any? { |source| source.id == target.id }
      render json: {
        error: "SourceInvalid",
        description: I18n.t("txt.admin.controllers.apiv2.tickets_controller.self_merge_error", nice_id: target.nice_id)
      }, status: :unprocessable_entity
    end
  end

  def validate_target
    target = find_target
    if target.nil? || (target && target.shared_tickets && target.shared_tickets.any?)
      return render json: {
        error: "TargetInvalid",
        description: "You are unable to merge into ticket id:#{target.try(:id)}. Tickets that are Closed, tickets that are shared with other accounts, and tickets you don't have access to cannot be merged into."
      }, status: :unprocessable_entity
    end

    if Zendesk::Tickets::Merger.different_requesters_not_allowed?(current_account, find_sources) && find_sources.any? { |source| source.requester.id != target.requester.id }
      return render json: {
        error: "TargetInvalid",
        description: "You can only merge tickets with the same requester"
      }, status: :unprocessable_entity
    end

    true
  end

  def bulk_update_job
    if params[:tickets]
      enqueue_job_with_status(TicketBatchUpdateJob, params.merge(from_api: true, request_params: request_params))
    elsif params[:ticket] && params[:ids]
      macro_id = params[:ticket][:macro_id].presence
      if macro_id
        store_bulk_macro_usage rule_id: macro_id.to_i, user_id: current_user.id, ticket_ids: many_ids
      end

      enqueue_job_with_status(TicketBulkUpdateJob, params.merge(ticket_ids: many_ids, from_api: true, request_params: request_params))
    else
      raise ActionController::ParameterMissing, "Both ticket ids and arguments must be present"
    end
  end

  def asynchronous_ticket_create_job
    params[:tickets] = [params.delete(:ticket).merge!(custom_id_override: asynchronous_new_ticket_id)]
    enqueue_job_with_status(TicketBulkCreateJob, params.merge(request_params: request_params))
  end

  def asynchronous_new_ticket_id
    @asynchronous_new_ticket_id ||= current_account.nice_id_sequence.next
  end

  SORT_LOOKUP = Ticket::SORTABLE_FIELDS

  private

  def active_chats?
    # Assuming all chat ended must have a corresponding chat started, count the unique number of events for each chat
    initialized_ticket.
      events.
      where(type: %w[ChatStartedEvent ChatEndedEvent]).
      select(:type, :value).
      each_with_object(Hash.new { |hash, key| hash[key] = Set.new }) { |event, counter| counter[event.chat_id] << event.type }.
      any? { |(_, set)| set.length.odd? }
  end

  def idempotency_cache_prefix
    current_account.id
  end

  def exclude_archived?
    if current_account.has_exclude_archived_default_for_user_tickets?
      params.key?(:exclude_archived) ? params[:exclude_archived].present? : true
    else
      params[:exclude_archived].present?
    end
  end

  def paginate_method
    exclude_archived? ? "paginate" : "archived_paginate"
  end

  def count_method
    exclude_archived? ? "count" : "count_with_archived"
  end

  # The will_paginate gem respects all truthy values as an overridden count.
  # nil => will_paginate issues a count query
  # -1  => will_paginate trusts we already have the count which we can then ignore
  def exclude_count?
    params[:exclude_count].present? ? -1 : nil
  end

  def filter_tickets_by_current_user
    total_entries = if params[:exclude_count].present?
      -1
    else
      cached_pagination_count(user.tickets, count_method: count_method, cache_key: "ticket=count_with_archived/user=#{user.id}")
    end

    tickets = paginate(user.tickets, method: paginate_method, total_entries: total_entries)

    tickets = tickets.to_a if tickets.is_a?(ActiveRecord::AssociationRelation)

    if current_account.has_api_filter_requested_tickets?
      tickets.select! { |ticket| current_user.can?(:view, ticket) }
      tickets.total_entries = tickets.count if tickets.previous_page.nil? && tickets.next_page.nil?
    end

    tickets
  end

  def ensure_ids_present
    head :bad_request unless params[:ids].present?
  end

  def rate_limit_new_accounts
    if current_account.has_orca_classic_rate_limit_tickets_primary? && !current_account.whitelisted?
      if current_account.is_trial?
        rate_limit_header(:trial_account_ticket_creation, simulate: !current_account.has_orca_classic_rate_limit_trial_acc_tickets?)
      elsif current_account.recently_created_account?
        rate_limit_header(:new_account_ticket_creation, simulate: !current_account.has_orca_classic_rate_limit_new_acc_tickets?)
      end
    end
  end

  # TODO: put logic back into find_sources once `email_ccs` is GA'd
  def find_tickets_with_requester(source_ids)
    return [] if source_ids.blank?
    current_account.tickets.for_user(current_user).where(nice_id: source_ids).includes(:requester).to_a
  end

  def throttle_destroy
    Prop.throttle!(:ticket_deletions, current_account.id, threshold: current_account.settings.ticket_deletions_threshold)
  end

  def record_counter
    @record_counter ||= begin
      Zendesk::RecordCounter::Tickets.new(
        account: current_account,
        options: params.merge(current_user_id: current_user.id)
      )
    end
  end

  def rate_limit_system_users
    if current_user&.is_system_user?
      ActiveRecord::Base.on_master.without_kasket do
        db_cpu = database_sensor.cpu_utilization.to_f / 100

        # This should result in a linear decrease in probability
        # once above TARGET_DB_CPU
        rate_limit_probability = if db_cpu < TARGET_DB_CPU
          1
        else
          1 - (db_cpu - TARGET_DB_CPU) / (1 - TARGET_DB_CPU)
        end

        if rate_limit_probability < rand
          render status: :too_many_requests,
                 json: {
                   error: 'TooManyRequests',
                   description: "Database load is currently high, please wait"
                 }
        end
      end
    end
  end

  def database_sensor
    @database_sensor ||= ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.new
  end
end
