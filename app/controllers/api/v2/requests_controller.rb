class Api::V2::RequestsController < Api::V2::BaseController
  include Zendesk::Tickets::ControllerSupport
  include Zendesk::Tickets::Anonymous::ControllerSupport
  include Zendesk::Tickets::RecentTicketManagement
  include Zendesk::Tickets::UnverifiedEmailControllerSupport
  include Zendesk::CloudflareRateLimiting

  REQUEST_PARAMETER = {
    updated_stamp: Parameters.date_time | Parameters.nil_string,
    safe_update: Parameters.boolean | Parameters.nil_string,
    solved: Parameters.boolean,
    organization_id: Parameters.bigid,
    comment: {
      body: Parameters.string,
      html_body: Parameters.string,
      value: Parameters.string,
      uploads: Parameters.array(Parameters.string) | Parameters.string,
      screencasts: Parameters.anything # legacy field from deprecated functionality
    },
    system_metadata: Parameters.map(
      client: Parameters.string,
      ip_address: Parameters.string | Parameters.nil
    )
  }.freeze

  CREATE_REQUEST_PARAMETER = REQUEST_PARAMETER.merge(
    type: Parameters.string,
    tags: Parameters.array(Parameters.string) | Parameters.string,
    priority: Parameters.string,
    fields: Parameters.anything,
    custom_fields: Parameters.anything,
    subject: Parameters.string,
    ticket_form_id: Parameters.bigid,
    due_at: Parameters.date_time | Parameters.nil_string,
    via_followup_source_id: Parameters.bigid,
    via_id: Parameters.bigid,
    via: {
      channel: Parameters.anything,
      source: Parameters.anything
    }.freeze,
    metadata: Parameters.anything,
    collaborators: Parameters.anything,
    email_ccs: Parameters.anything,
    recipient: Parameters.string,
    requester: Parameters.map(
      name: Parameters.string,
      email: Parameters.string,
      locale_id: Parameters.integer,
      locale: Parameters.string
    )
  ).freeze

  UPDATE_REQUEST_PARAMETER = REQUEST_PARAMETER.merge(additional_collaborators: Parameters.anything, email_ccs: Parameters.anything).freeze

  require_that_user_can! :view, :organization, if: lambda { |c| c.params[:organization_id] }

  allow_anonymous_users :create, if: proc { |c| c.send(:allow_anonymous_request_creation?) }

  allow_cors :create

  skip_before_action :require_agent!
  before_action :ensure_user_access, only: :index
  before_action :ensure_end_user_email_is_verified, except: :create
  before_action :ensure_active_brand!
  before_action :set_brand, only: :create
  before_action :set_ticket_form, only: :create
  before_action :sanitize_collaborations, only: [:create, :update]

  allow_parameters :all, user_id: Parameters.bigid, organization_id: Parameters.bigid

  # Silently drop errors when blocking tickets for spam
  rescue_from Fraud::SpamDetector::SpamTicketFoundException do
    if current_account.has_orca_classic_rate_limit_master?
      rate_limit_header(:strong, simulate: !current_account.has_orca_classic_rate_limit_request_spam_challenge?)
    end
    render json: { success: true }
  end

  ## ### List Requests
  ## * `GET /api/v2/requests.json`
  ## * `GET /api/v2/requests.json?status=hold,open`
  ## * `GET /api/v2/requests/open.json`
  ## * `GET /api/v2/requests/solved.json`
  ## * `GET /api/v2/requests/ccd.json`
  ## * `GET /api/v2/users/{id}/requests.json`
  ## * `GET /api/v2/organizations/{id}/requests.json`
  ##
  ## #### Allowed For
  ##
  ##  * End Users
  ##
  ## #### Available parameters
  ##
  ## | Name                  | Type                | Required  | Comments
  ## | --------------------- | --------------------| --------- | -------------------
  ## | `sort_by`             | string              | no        | Possible values are `updated_at`, `created_at`
  ## | `sort_order`          | string              | no        | One of `asc`, `desc`. Defaults to `asc`
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests.json \
  ##   -v -u {email_address}/token:{api_token}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "requests": [
  ##     {
  ##       "id": 33,
  ##       "status": "open",
  ##       "description": "My printer is on fire!",
  ##       ...
  ##     }
  ##     {
  ##       "id": 34,
  ##       "status": "closed",
  ##       "description": "I can't find my keys",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :index, status: Parameters.string
  allow_cursor_pagination_v2_parameters :index
  require_oauth_scopes :index, scopes: %i[requests:read read]
  def index
    render json: presenter.present(tickets(*status_params))
  end

  allow_parameters :open, {}
  allow_cursor_pagination_v2_parameters :open
  require_oauth_scopes :open, scopes: %i[requests:read read write]
  def open
    render json: presenter.present(tickets(:new, :pending, :open, :hold))
  end

  allow_parameters :solved, {}
  allow_cursor_pagination_v2_parameters :solved
  require_oauth_scopes :solved, scopes: %i[requests:read read write]
  def solved
    render json: presenter.present(tickets(:solved, :closed))
  end

  allow_parameters :ccd, status: Parameters.string
  require_oauth_scopes :ccd, scopes: %i[requests:read read write]
  def ccd
    render json: presenter.present(ccd_tickets(*status_params))
  end

  ## ### Search requests
  ## * `GET /api/v2/requests/search.json?query={search_string}`
  ##
  ## The syntax and matching logic for `{search_string}` is detailed in the
  ## [Zendesk Support search reference](https://support.zendesk.com/hc/en-us/articles/203663226).
  ## See also [Query basics](https://developer.zendesk.com/rest_api/docs/support/search#query-basics) in the Tickets API doc.
  ##
  ## Examples:
  ##
  ## * `GET /api/v2/requests/search.json?query=printer`
  ## * `GET /api/v2/requests/search.json?query=printer&organization_id=1`
  ## * `GET /api/v2/requests/search.json?query=printer&cc_id=true`
  ## * `GET /api/v2/requests/search.json?query=printer&status=hold,open`
  ##
  ## #### Allowed For
  ##
  ##  * End Users
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "requests": [
  ##     {
  ##       "id": 33,
  ##       "status": "open",
  ##       "description": "My printer is on fire!",
  ##       ...
  ##     }
  ##     {
  ##       "id": 34,
  ##       "status": "closed",
  ##       "description": "I can't change the paper in my printer",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :search, status: Parameters.string, query: Parameters.string, cc_id: Parameters.string
  require_oauth_scopes :search, scopes: %i[requests:read read write]
  def search
    if params[:query].blank?
      params[:cc_id].present? ? ccd : index
    else
      setting_ticket_query

      query.execute(current_user)

      if query.success?
        render json: presenter.present(query.result)
      else
        render json: query.error.to_json, status: :not_acceptable
      end
    end
  end

  ## ### Show Request
  ## `GET /api/v2/requests/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests/{id}.json \
  ##   -v -u {email_address}/token:{api_token}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "request": {
  ##     "id": 33,
  ##     "status": "open",
  ##     "description": "My printer is on fire!",
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.string
  require_oauth_scopes :show, scopes: %i[requests:read read]
  def show
    render json: presenter.present(requested_ticket)
  end

  ## ### List Email CCs for a Request
  ##
  ## `GET /api/v2/requests/{id}`
  ##
  ## You can list email ccs for a request by side-loading users.
  ##
  ## #### Allowed For
  ##
  ##  * End users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests/{id}?include=users \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   request: {
  ##     url: "https://{subdomain}.zendesk.com/api/v2/requests/32.json",
  ##     id: 32,
  ##     status: "open",
  ##     description: "this is a comment",
  ##     via: {
  ##       channel: "email",
  ##       source: {
  ##         from: {
  ##           address: "foo@example.com",
  ##           name: "Foo"
  ##         },
  ##         to: {
  ##           name: "company name",
  ##           address: "support@{subdomain}.zendesk.com"
  ##         },
  ##         rel: null
  ##       }
  ##     },
  ##     ...
  ##     requester_id: 22310942,
  ##     collaborator_ids: [
  ##       22004591
  ##     ],
  ##     email_cc_ids: [
  ##       22004591
  ##     ],
  ##     ...
  ##   },
  ##   users: [
  ##     {
  ##       id: 21664321,
  ##       name: "Sample customer",
  ##       agent: true,
  ##       ...
  ##       },
  ##     {
  ##       id: 22004591,
  ##       name: "A collaborator",
  ##       agent: false,
  ##       ...
  ##     },
  ##     {
  ##       id: 22310942,
  ##       name: "The requester",
  ##       agent: false,
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  ## ### Create Request
  ## `POST /api/v2/requests.json`
  ##
  ## Accepts a `request` object that sets one or more properties.
  ##
  ## #### Additional properties
  ##
  ## In addition to the writable request properties in the [JSON Format table](#json-format)
  ## above, you can set the following properties when creating a request.
  ##
  ## | Name                | Type   | Mandatory | Comment
  ## | ----------------    | -------| --------- | -------
  ## | comment             | object | yes       | Describes the problem, incident, question, or task. See [Request comments](#request-comments)
  ## | collaborators       | array  | no        | Adds collaborators (cc's) to the request. An email notification is sent to them when the ticket is created. See [Setting collaborators](#setting-collaborators)
  ## | requester           | object | yes*      | \*Required for anonymous requests. Specifies the requester of the anonymous request. See [Creating anonymous requests](#creating-anonymous-requests)
  ##
  ## #### Creating anonymous requests
  ##
  ## Anonymous requests must first be enabled in Support. If you're not an
  ## admin, ask an admin to do it for you.
  ##
  ## To enable anonymous requests:
  ##
  ## 1. Sign in to Support and go to the Customers page (**Admin** > **Settings** > **Customers**).
  ## 2. Enable "Anyone can submit tickets".
  ## 3. Disable "Ask users to register".
  ##
  ## See [Managing end-user settings](https://support.zendesk.com/hc/en-us/articles/203663806)
  ## in the Zendesk Support Help Center.
  ##
  ## To create anonymous requests:
  ##
  ## 1. Omit the authorization header.
  ## 2. Include a `requester` object in the `request` object.
  ##
  ## The `requester` object has the following properties. Only `name` is required.
  ##
  ## | Name      | Required | Description                                |
  ## | --------- | -------- | -------------------------------------------|
  ## | name      | yes      | A real or arbitrary name for the requester |
  ## | email     | no       | An email address                           |
  ## | locale    | no       | A BCP-47 compliant tag                     |
  ## | locale_id | no       | See the [Locales](./locales) API           |
  ##
  ## If both `locale` and `locale_id` are included on create, `locale` is used
  ## and `locale_id` is ignored.
  ##
  ## Example:
  ##
  ##  ```javascript
  ##  {
  ##   "request": {
  ##     "requester": {"name": "Anonymous customer"},
  ##     "subject": "Help!",
  ##     "comment": {"body": "My printer is on fire!" }
  ##   }
  ## }
  ## ```
  ##
  ## #### Allowed For
  ##
  ##  * End users
  ##  * Anonymous users
  ##
  ## #### Using curl
  ##
  ## **Authenticated request**
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests.json \
  ##   -d '{"request": {"subject": "Help!", "comment": {"body": "My printer is on fire!", "uploads": [...] }}}' \
  ##   -v -u {email_address}/token:{api_token} -X POST -H "Content-Type: application/json"
  ## ```
  ##
  ## **Anonymous request**
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests.json \
  ##   -d '{"request": {"requester": {"name": "Anonymous customer"}, "subject": "Help!", "comment": {"body": "My printer is on fire!" }}}' \
  ##   -X POST -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/requests/{id}.json
  ##
  ## {
  ##   "request": {
  ##     "id": 33,
  ##     "status": "new",
  ##     "description": "My printer is on fire!",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Creating follow-up requests
  ##
  ## Once a ticket is closed (as distinct from solved), it can't be reopened. However, you can
  ## create a new request that references the closed ticket. To create the follow-up request,
  ## include a `via_followup_source_id` parameter that specifies the closed ticket. Example:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests.json \
  ##   -d '{"request": {"via_followup_source_id": 103, "comment": {"body": "My printer is still too hot!"}}}' \
  ##   -v -u {email_address}:{password} -X POST -H "Content-Type: application/json"
  ## ```
  ##
  ## The parameter only works with closed tickets. It has no effect with other tickets.
  ##
  allow_parameters :create, request: CREATE_REQUEST_PARAMETER
  require_oauth_scopes :create, scopes: %i[requests:write write]
  def create
    unless params[:request] && params[:request][:comment].present?
      return render json: {}, status: :unprocessable_entity
    end

    if anonymous_request?
      unless current_account.is_open?
        return render json: {}, status: :unauthorized
      end

      if params[:request]
        uploads = Array(params[:request][:comment][:uploads])

        if uploads && uploads.count > current_account.settings.anonymous_attachment_limit
          return render json: { error: "Anonymous uploads are limited to #{current_account.settings.anonymous_attachment_limit} files per request" }, status: :unprocessable_entity
        end
      end

      ticket = initialized_anonymous_ticket
    else
      ticket = initialized_ticket
    end

    if current_user.is_system_user?
      log_user_request_status("succeeded")
    end

    ticket.save!

    render json: presenter.present(ticket), status: :created, location: presenter.url(ticket)
  end

  ## ### Update Request
  ## `PUT /api/v2/requests/{id}.json`
  ##
  ## Updates a request with a comment or collaborators (cc's). The end user who created the request can
  ## also use it to mark the request as solved. The endpoint can't be used to update other request attributes.
  ##
  ## #### Writable properties
  ## This endpoint can only update the following properties in the request.
  ##
  ## | Name                     | Type    | Required | Description                                          |
  ## | ------------------------ | ------- | -------- | ---------------------------------------------------- |
  ## | comment                  | object  | no       | Adds a comment to the request. See [Request comments](#request-comments) |
  ## | solved                   | boolean | no       | Marks the request as solved. Example: `{"request": {"solved": "true"}}` |
  ## | additional_collaborators | array   | no       | Adds collaborators to the request. An email notification is sent to them when the ticket is updated. See [Adding collaborators](#adding-collaborators) |
  ##
  ## #### Allowed For
  ##
  ##  * End users
  ##
  ## #### Using curl
  ##
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests/{id}.json \
  ##   -d '{"request": {"comment": {"body": "Thanks!"}}}' \
  ##   -v -u {email_address}/token:{api_token} -X PUT -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "request": {
  ##     "id": 33,
  ##     "status": "new",
  ##     "description": "My printer is on fire!",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update, id: Parameters.bigid, request: UPDATE_REQUEST_PARAMETER
  require_oauth_scopes :update, scopes: %i[requests:write write]
  def update
    initialized_ticket.save!
    render json: presenter.present(initialized_ticket)
  end

  ## ### Setting collaborators
  ##
  ## <span class="alert alert-block alert-warning"><strong>Note</strong>: If the account has the CCs and Followers feature enabled, then collaborators can still be set using <code>collaborators</code> and <code>additional_collaborators</code>, which will automatically make end users CCs and agents followers. The use of email_ccs and followers is preferred for these accounts. See <a href="https://developer.zendesk.com/rest_api/docs/support/tickets#setting-email-ccs">Setting Email CCs</a> and <a href="https://developer.zendesk.com/rest_api/docs/support/tickets#setting-followers">Setting Followers</a> for more details.</span>
  ##
  ## When creating a ticket, you can set collaborators on a ticket using the `collaborators` parameter.
  ##
  ## An email notification is sent to the collaborators when the ticket is created.
  ##
  ## The `collaborators` parameter takes an array consisting of a combination of user ids, email addresses, or objects containing a `name` and `email` property.
  ##
  ## *  562562562
  ## * "someone@example.com"
  ## * `{ "name": "Someone Special", "email": "someone@example.com" }`
  ##
  ## Use an object to create the user on the fly with the appropriate name in your Zendesk Support instance.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "collaborators": [ 562, "someone@example.com", { "name": "Someone Else", "email": "else@example.com" } ]
  ##   }
  ## }
  ## ```
  ##
  ## The new or updated collaborators will be specified in the JSON response by a property named `collaborator_ids`.
  ## The response won't have a `collaborators` property.

  ## ### Adding collaborators
  ##
  ## <span class="alert alert-block alert-warning"><strong>Note</strong>: If the account has the CCs and Followers feature enabled, then collaborators can still be set using <code>collaborators</code> and <code>additional_collaborators</code>, which will automatically make end users CCs and agents followers. The use of email_ccs and followers is preferred for these accounts. See <a href="https://developer.zendesk.com/rest_api/docs/support/tickets#setting-email-ccs">Setting Email CCs</a> and <a href="https://developer.zendesk.com/rest_api/docs/support/tickets#setting-followers">Setting Followers</a> for more details.</span>
  ##
  ## When updating a ticket, you can add new collaborators on a ticket using the `additional_collaborators` parameter.
  ##
  ## An email notification is sent to the collaborators when the ticket is updated.
  ##
  ## The `additional_collaborators` takes an array consisting of a combination of user ids, email addresses, or objects containing a `name` and `email` property.
  ##
  ## *  562562562
  ## * "someone@example.com"
  ## * `{ "name": "Someone Special", "email": "someone@example.com" }`
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "additional_collaborators": [ 562, "someone@example.com", { "name": "Someone Else", "email": "else@example.com" } ]
  ##   }
  ## }
  ## ```
  ##
  ## The new or updated collaborators will be specified in the JSON response by a property named `collaborator_ids`.
  ## The response won't have a `additional_collaborators` property.

  ## ### Setting email CCs
  ##
  ## When creating or updating a request, you can set email CCs on a request using the parameter `email_ccs`.
  ##
  ## An email notification is sent to the CCs when the request is created or updated depending on trigger settings.
  ##
  ## The `email_ccs` parameter takes an array of objects representing users. Each object must have an identifier, either user ID (`user_id`) or email address (`user_email`). If an
  ## email address is specified and the user doesn't exist in the account, a new user will be created. Additionally, the key `action` can be set to either "put" or "delete" - which
  ## indicates whether the user should be added to or removed from the email CCs list. If the `action` key is not given, the default of `put` is used. Optionally, if a new user is
  ## created, `user_name` can be specified to set the new user's name. If the user will be implicitly created and a `user_name` is not given, the user name will be derived from the
  ## local part of the email address. If an email address not associated with a user is included (with eg. the intention to implicitly create the user), but the "action" key is valued
  ## "delete", that email address will not be created and/or added as an email CC.
  ##
  ## Email CCs will not be updated if an internal note is also added to the request in the same update.
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "request": {
  ##     "email_ccs": [
  ##       { "user_id": "562624", "action": "put" },
  ##       { "user_id": "243642", "action": "delete" },
  ##       { "user_email": "else@example.com", "user_name": "Someone Else", "action": "put"}
  ##     ]
  ##   }
  ## }
  ## ```

  protected

  def presenter
    @presenter ||= current_presenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: with_cursor_pagination_v2?)
  end

  def current_presenter
    @current_present ||=
      case @initialized_anonymous_ticket
      when SuspendedTicket
        Api::V2::SuspendedTicketPresenter
      else
        Api::V2::RequestPresenter
      end
  end

  def tickets(*statuses)
    @tickets ||= begin
      org_or_user_tickets = params[:organization_id] ? organization.tickets : user.tickets
      if use_cursor_pagination_v2?
        paginate_with_cursor(
          sort_tickets_for_cursor(scope_tickets_for_cursor(org_or_user_tickets, *statuses))
        )
      else
        paginate(org_or_user_tickets, ticket_options(*statuses))
      end
    end
  end

  def requested_order
    # By default we sort by id
    sorting_attributes = ['tickets.id']

    # We allow to order by the following attributes
    allowed_sorting_attributes = ["updated_at", "created_at"]

    if allowed_sorting_attributes.include? params[:sort_by]
      sorting_attributes.unshift("tickets.#{params[:sort_by]}")
    end

    sort_order = if params[:sort_order].present? && params[:sort_order].casecmp("desc").zero?
      "DESC"
    else
      "ASC"
    end

    sorting_attributes.map { |a| "#{a} #{sort_order}" }.join(", ")
  end

  def ccd_tickets(*statuses)
    @ccd_tickets ||= if current_account.has_follower_and_email_cc_collaborations_enabled?
      current_account.has_comment_email_ccs_allowed_enabled? ? paginate(user.ccd_tickets, ticket_options(*statuses)) : []
    else
      paginate(user.collaborated_tickets, ticket_options(*statuses))
    end
  end

  def organization
    @organization ||= current_account.organizations.find(params[:organization_id])
  end

  def ensure_user_access
    deny_access if current_user != user && !current_user.is_agent?
  end

  def set_brand
    params[:request][:brand_id] = current_brand.try(:id) if HashParam.ish?(params[:request])
  end

  def set_ticket_form
    params[:request][:ticket_form_id] ||= fallback_ticket_form.try(:id) if params.key? :request
  end

  def ensure_access_to_ticket(ticket)
    super

    if current_account.has_multiple_active_brands?
      ticket_brand_id = find_ticket_brand_id(ticket)
      if ticket_brand_id != current_brand.id
        Rails.logger.info "Record not found: current_brand.id #{current_brand.id} does not match the brand.id #{ticket_brand_id} in the ticket #{ticket.nice_id}"
        raise ActiveRecord::RecordNotFound, ticket.nice_id
      end
    end

    ticket
  end

  def use_cursor_pagination_v2?
    # Remove this conditional after we rollout 'cursor_pagination_v2_requests_endpoint'.
    #
    # If we receive a '?page[:size]=2' request before we enable the
    # feature, we want to show the same error message we were showing
    # before adding the new parameter types.
    if !current_account.has_cursor_pagination_v2_requests_endpoint? &&
          HashParam.ish?(params[:page])
      raise StrongerParameters::InvalidParameter.new(
        StrongerParameters::IntegerConstraint.new.value(params[:page]),
        'page'
      )
    end

    with_cursor_pagination_v2? && current_account.has_cursor_pagination_v2_requests_endpoint?
  end

  def user
    @user ||= if params[:user_id]
      current_account.users.find(params[:user_id])
    else
      current_user
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: :requests)
  end

  private

  def allow_anonymous_request_creation?
    return true unless current_account.has_orca_classic_restrict_anon_requests_endpoint? && current_account.is_open?
    log_only_mode = current_account.has_orca_classic_restrict_anon_requests_endpoint_log_only?

    # Guide(Webform requests) uses a zendesk system user
    unless current_user.is_system_user?
      log_user_request_status("denied", log_only_mode)
      unless log_only_mode
        return false
      end
    end

    log_user_request_status("succeeded", log_only_mode)
    true
  end

  def via_id
    params[:request].respond_to?(:dig) && params.dig(:request, :via_id)
  end

  def log_user_request_status(status, log_only_mode = false)
    feature_guide_webform_sets_anonymous_users_as_system_users = current_account.has_orca_help_center_skip_header_for_anon_users?

    Rails.logger.append_attributes(
      anonymous_requests_controller_create: {
        account_id: current_account.id,
        current_user: current_user.id,
        anonymous_request: current_user.is_anonymous_user?,
        system_user: current_user.is_system_user?,
        via_id: via_id,
        account_is_open: current_account.is_open?,
        log_only_mode: log_only_mode,
        feature_guide_webform_sets_anonymous_users_as_system_users: feature_guide_webform_sets_anonymous_users_as_system_users
      }
    )

    Rails.logger.info "Anonymous_request_#{status}"
  end

  def status_params
    valid_status_types(params[:status].to_s.split(','))
  end

  def valid_status_types(candidates)
    candidates.select { |status| StatusType.find(status) }
  end

  def status_ids
    @status_ids ||= params[:status].to_s.split(',').map { |status| StatusType.find(status) }.compact.uniq
  end

  def ticket_options(*statuses)
    conditions = {is_public: true}

    if statuses.any?
      conditions[:status_id] = statuses.map { |status| StatusType.find(status.to_s) }
    end

    if current_account.has_multiple_active_brands?
      conditions[:brand_id] = current_brand.id
    end

    {
      order: requested_order,
      method: "archived_paginate",
      conditions: conditions
    }
  end

  def scope_tickets_for_cursor(scope, *statuses)
    scope.where(ticket_options(*statuses)[:conditions])
  end

  def query
    @query ||= Zendesk::Search::Query.new(params[:query], query_options).tap do |query|
      query.page = get_page
      query_params = Zendesk::Search::Params.new(params)
      query_params.sort_key = :sort_order
      query_params.order_key = :sort_by
      query.options.merge!(query_params.to_options)
      query.options.delete(:forum_id)
    end
  end

  def setting_ticket_query
    query.options[:with] ||= {}
    query.type = "ticket"
    query.options[:endpoint] = "requests"
    query.options[:field_weights] = { nice_id_string: 100, subject: 100, comment: 50 }

    if current_account.has_multiple_active_brands?
      query.options[:with][:brand_id] = current_brand.id
    end

    query.options[:with][:is_public] = true

    if params[:cc_id] || params[:organization_id]
      query.options[:field_weights][:requester_name] = 100
      query.options[:with][:cc_id] = current_user.id if params[:cc_id]
      query.options[:with][:organization_id] = params[:organization_id] if params[:organization_id]
    else
      query.options[:with][:requester_id] = current_user.id
    end

    query.options[:with][:status_id] = status_ids if status_ids.any?
  end

  def fallback_ticket_form
    current_account.fallback_ticket_form(current_brand)
  end

  def find_ticket_brand_id(ticket)
    return ticket.brand_id if ticket.brand_id.present?
    ticket.ticket_archive_stub && ticket.ticket_archive_stub.brand_id
  end

  def query_options
    { request: request, account: current_account }
  end

  def current_user
    if from_web_widget?
      current_account.anonymous_user
    else
      super
    end
  end

  def from_web_widget?
    via_id == ViaType.WEB_WIDGET
  end

  # This is to allow anonymous requests to bypass our CSRF token check for the create method.
  def verified_request?
    (action_name == 'create' && current_user.is_anonymous_user?) || super
  end

  def sanitize_collaborations
    # This method sanitizes collaborations for submitters.
    # A light agent should not be able to change collaborations, email_ccs or followers
    if current_account.has_follower_and_email_cc_collaborations_enabled?
      if current_user.is_agent? && !current_user.can?(:publicly, Comment)
        params[:request] = params[:request].except(
          :collaborators,
          :email_ccs,
          :additional_collaborators
        )
      end
    end
  end
end
