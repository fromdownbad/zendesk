class Api::V2::ReportsController < Api::V2::BaseController
  include Zendesk::Reports::ControllerSupport

  # ### Reports
  # `GET /api/v2/reports/{id}/result.json`
  #
  # #### Allowed For
  #
  #  * Admins
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/reports/{id}/result.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # { report data }
  #
  # Status: 202 Accepted
  # job has old data, returned
  # { old report data }
  #
  # Status: 202 Accepted
  # job started in the background, will be ready soon
  # { "message": "<current status>"}
  # ```
  allow_parameters :result, :skip # TODO: make stricter
  require_oauth_scopes :result, scopes: [:read], arturo: true
  def result
    status, data = report_result
    respond_to do |format|
      format.json { render json: data, status: status }
    end
  end
end
