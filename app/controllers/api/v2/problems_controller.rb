class Api::V2::ProblemsController < Api::V2::BaseController
  ## ### Listing Ticket Problems
  ## `GET /api/v2/problems.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/problems.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## The response is always ordered by updated_at, in descending order
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":          33,
  ##       "subject":     "My printer is on fire",
  ##       "description": "The fire is very colorful.",
  ##       "status":      "open",
  ##       ...
  ##     },
  ##     {
  ##       "id":          34,
  ##       "subject":     "The printer is on fire over here too",
  ##       "description": "The fire is very colorful as well!",
  ##       "status":      "pending",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(problems)
  end

  ## ### Autocomplete Problems
  ## `POST /api/v2/problems/autocomplete.json?text={name}`
  ##
  ## Returns tickets whose type is "problem" and whose subject contains the string specified in the `text` parameter.
  ##
  ## You can specify the `text` parameter in the request body rather than the query string. Example:
  ##
  ## `{"text": "fire"}`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/problems/autocomplete.json?text=fire \
  ##   -X POST -H "Content-Type: application/json" \
  ##   -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":          33,
  ##       "subject":     "My printer is on fire",
  ##       "description": "The flames are very colorful.",
  ##       "type":        "problem",
  ##       ...
  ##     },
  ##     {
  ##       "id":          34,
  ##       "subject":     "The printer is on fire over here too",
  ##       "description": "The flames are very colorful as well!",
  ##       "type":        "problem",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :autocomplete, text: Parameters.string
  require_oauth_scopes :autocomplete, scopes: [:write], arturo: true
  def autocomplete
    render json: presenter.present(autocomplete_problems)
  end

  protected

  def presenter
    @presenter ||= Api::V2::Tickets::TicketPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def problems
    @problems ||= paginate(current_account.problems.working.for_user(current_user).order(updated_at: :desc))
  end

  def autocomplete_problems
    query_field = current_account.field_subject.is_active? ? "subject" : "description"
    conditions  = ["#{query_field} LIKE ?", "%#{params[:text]}%"]

    current_account.problems.working.for_user(current_user).where(conditions).paginate(pagination)
  end
end
