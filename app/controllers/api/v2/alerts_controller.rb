class Api::V2::AlertsController < Api::V2::BaseController
  include Zendesk::Alerts::ControllerSupport

  ## ### Listing Alerts
  ## `GET /api/v2/alerts.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/alerts.json \
  ##   -v -u {email_address}:{token}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "alerts": [
  ##     {
  ##       "id": 2127301143,
  ##       "link_url": "http://alerts.io",
  ##       "value": "Please ignore this alert",
  ##       "interfaces": "all"
  ##       "updated_at": "2011/09/25 22:35:44 -0700",
  ##       "created_at": "2011/09/25 22:35:44 -0700"
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, interface: Parameters.string
  def index
    render json: presenter.present(alerts)
  end

  ## ### Dismissing Alerts
  ## `DELETE /api/v2/alerts/{id}.json
  ##
  ## #### Allowed For
  ##
  ## * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/alerts/{id}.json \
  ##   -X DELETE \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    if alert
      AlertDismissal.create!(alert: alert, account: current_account, user: current_user)
      default_delete_response
    else
      head :not_found
    end
  end

  ## ### Dismissing Multiple Alerts
  ## `DELETE /api/v2/alerts/destroy_many.json?ids={id1},{id2}`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/alerts/destroy_many.json?ids={id1},{id2} \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids
  def destroy_many
    if many_ids?
      alerts = Alert.find(many_ids)
      alerts.each do |alert|
        AlertDismissal.create!(alert: alert, account: current_account, user: current_user) if alert.show_for?(current_user)
      end
    end
    default_delete_response
  end

  private

  def alert
    @alert ||= begin
      alert = Alert.find(params[:id].to_i)
      alert if alert.show_for?(current_user)
    end
  end

  def alerts
    @alerts ||= current_user_alerts(interface: params[:interface])
  end

  def presenter
    @presenter ||= Api::V2::AlertPresenter.new(current_user, url_builder: self)
  end
end
