require 'zendesk/offset_pagination_limiter'

class Api::V2::DeletedUsersController < Api::V2::BaseController
  allow_gooddata_user

  require_that_user_can! :list, User, only: [:index, :count], unless: :allowed_gooddata_request?
  require_that_user_can! :delete, :user, only: [:destroy, :show], unless: :allowed_gooddata_request?

  around_action :handle_rate_limiting_on_deep_offset_pagination, only: [:index]

  include Zendesk::Users::ControllerSupport

  ## ### List Deleted Users
  ##
  ## * `GET /api/v2/deleted_users.json`
  ##
  ## Returns deleted users, including permanently deleted users.
  ##
  ## If the results contains permanently deleted users, the users' properties
  ## that normally contain personal data, such as `email` and `phone`,
  ## are null. The `name` property is "Permanently Deleted User".
  ##
  ## #### Pagination
  ##
  ## * Cursor pagination (recommended)
  ## * Offset pagination
  ##
  ## See [Pagination](./introduction#pagination).
  ##
  ## Returns a maximum of 100 records per page.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_users.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "deleted_users": [
  ##     {
  ##       "id": 223443,
  ##       "name": "Johnny Agent",
  ##       ...
  ##     },
  ##     {
  ##       "id": 8678530,
  ##       "name": "James A. Rosen",
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, include_redacted: (Parameters.nil_string | Parameters.boolean)
  allow_cursor_pagination_parameters :index
  allow_cursor_pagination_v2_parameters :index
  require_oauth_scopes :index, scopes: %i[users:read read]
  def index
    throttle_index_with_deep_offset_pagination
    include_redacted = params_include_redacted?

    unless current_account.has_v2_users_allow_redacted_users_filter?
      include_redacted = true
    end

    presented_users = if current_account.arturo_enabled?('cursor_pagination_default_deleted_users')
      if with_offset_pagination? && !current_account.arturo_enabled?('remove_offset_pagination_deleted_users')
        presenter.present(paginate(users(include_redacted), per_page: per_page))
      else
        index_paginate_with_cursor(include_redacted)
      end
    else
      if with_cursor_pagination? || current_account.arturo_enabled?('remove_offset_pagination_deleted_users')
        index_paginate_with_cursor(include_redacted)
      else
        presenter.present(paginate(users(include_redacted), per_page: per_page))
      end
    end

    render json: presented_users
  end

  ## ### Count Deleted Users
  ##
  ## `GET /api/v2/deleted_users/count.json`
  ##
  ## Returns an approximate count of deleted users, including permanently deleted users. If the count exceeds 100,000, it is updated every 24 hours.
  ##
  ## The `count[refreshed_at]` property is a timestamp that indicates when the count was last updated.
  ##
  ## **Note**: When the count exceeds 100,000, `count[refreshed_at]` may occasionally be `null`.
  ## This indicates that the count is being updated in the background, and `count[value]` is limited to 100,000 until the update is complete.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_users/count.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ##  {
  ##    count: {
  ##      value: 13,
  ##      refreshed_at: "2020-04-06T02:18:17Z"
  ##    },
  ##    links: {
  ##      url: "https://{subdomain}.zendesk.com/api/v2/deleted_users/count"
  ##    }
  ##  }
  ## ```
  allow_parameters :count, include_redacted: Parameters.boolean
  require_oauth_scopes :count, scopes: %i[users:read read]
  def count
    render json: { count: record_counter.present, links: { url: request.original_url } }
  end

  ## ### Show Deleted User
  ##
  ## `GET /api/v2/deleted_users/{id}.json`
  ##
  ## These are users that have been deleted but not permanently yet. See [Permanently Delete User](#permanently-delete-user).
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_users/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "deleted_user": {
  ##     "id":   35436,
  ##     "name": "Johnny Agent",
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: ActionController::Parameters.bigid
  require_oauth_scopes :show, scopes: %i[users:read read]
  def show
    render json: presenter.present(user)
  end

  ## ### Permanently Delete User
  ##
  ## `DELETE /api/v2/deleted_users/{id}.json`
  ##
  ## Before permanently deleting a user, you must delete the user first. See [Delete User](./users#delete-user).
  ##
  ## WARNING: Permanently deleting a user deletes all of their information. This information is not recoverable.
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions on certain actions
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_users/{id}.json \
  ##   -v -u {email_address}:{password} \
  ##   -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "deleted_user": {
  ##     "id":     9873843,
  ##     "active": false,
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Permanent user deletion rate limit
  ##
  ## You can permanently delete 700 users every 10 minutes.
  ## The rate limiting mechanism behaves as described in
  ## [Rates Limits](./introduction#rate-limits) in the API introduction.
  ## Zendesk recommends that you obey the Retry-After header values.
  allow_parameters :destroy, id: Parameters.bigid
  require_oauth_scopes :destroy, scopes: %i[users:write write]
  def destroy
    Prop.throttle!(:user_permanent_deletions, current_account.id, threshold: current_account.settings.user_permanent_deletions_threshold)

    return deny_access if user == current_user
    return deny_access if user.is_active?

    user.mark_for_ultra_deletion!(executer: current_user)
    render json: presenter.present(user)
  end

  private

  def allowed_gooddata_request?
    gooddata_request? && !Rails.env.production?
  end

  def presenter
    @presenter ||= Api::V2::Users::DeletedUserPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def cursor_presenter
    @cursor_presenter ||= Api::V2::Users::DeletedUserPresenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
  end

  def includes
    values = super
    values.delete(:latest_tickets) unless action_name.eql?('show')
    values.delete(:related_ticket_ids) unless action_name.eql?('show')
    values
  end

  def users(include_redacted)
    @users ||= include_redacted ? current_account.all_users.inactive : current_account.all_users.inactive_and_not_redacted
  end

  def user
    @user ||= current_account.all_users.inactive.find(params[:id]).tap do |user|
      user.current_user = current_user
    end
  end

  def include_redacted?
    return true unless current_account.has_v2_users_allow_redacted_users_filter?

    params_include_redacted?
  end

  def params_include_redacted?
    params[:include_redacted].nil? || params[:include_redacted]
  end

  def enforce_default_cursor_pagination_params
    params[:order] = DEFAULT_CURSOR_ORDERING if params[:order].blank?
    params[:limit] = CURSOR_PAGINATION_MAX_SIZE if params[:limit].nil? || params[:limit] > CURSOR_PAGINATION_MAX_SIZE
    params[:order_without_created_at] = true
  end

  def index_paginate_with_cursor(include_redacted)
    if with_cursor_pagination_v2? || current_account.arturo_enabled?('cursor_pagination_remove_v1_deleted_users')
      sorted_records = sorted_scope_for_cursor(users(include_redacted))
      cursor_presenter.present(paginate_with_cursor(sorted_records))
    else
      enforce_default_cursor_pagination_params
      cursor_presenter.present(users(include_redacted).paginate_with_cursor(params))
    end
  end

  def record_counter
    @record_counter ||= begin
      Zendesk::RecordCounter::DeletedUsers.new(
        account: current_account,
        options: {
          include_redacted: include_redacted?
        }
      )
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: "deleted_users")
  end

  def throttle_index_with_deep_offset_pagination
    return nil unless params[:page].is_a?(Integer)

    Zendesk::OffsetPaginationLimiter.new(
      entity: controller_name,
      account_id: current_account.id,
      account_subdomain: current_account.subdomain,
      page: params[:page],
      per_page: per_page,
      log_only: true
    ).throttle_index_with_deep_offset_pagination
  end

  def handle_rate_limiting_on_deep_offset_pagination
    yield
  rescue Prop::RateLimited => e
    if e.handle == :index_deleted_users_with_deep_offset_pagination_ceiling_limit
      Rails.logger.info("[DeletedUsersController] Rate limiting #index by account: #{current_account.subdomain}, page: #{params[:page]}, per_page: #{per_page}, throttle key: index_deleted_users_with_deep_offset_pagination_ceiling_limit")
      statsd_client.increment("api_v2_deleted_users_index_ceiling_rate_limit", tags: ["account_id:#{current_account.id}"])
      response.headers['Retry-After'] = e.retry_after.to_s
      render plain: I18n.t("txt.api.v2.deleted_users.index_with_deep_offset_pagination.rate_limit_by_account") + " per_page: #{params[:per_page]}, page: #{params[:page]}", status: 429, content_type: Mime[:text].to_s
    else
      Rails.logger.warn("Api::V2::DeletedUsersController Rate Limited #{e.message}")
      raise e
    end
  end
end
