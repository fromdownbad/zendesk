require 'zendesk/radar_factory'

module Api::V2::Channels::Voice
  class UsersController < BaseController
    include Zendesk::Voice::ControllerSupport

    ## ### Open a user's profile in an agent's browser
    ## `POST /api/v2/channels/voice/agents/{agent_id}/users/{user_id}/display.json`
    ##
    ## Allows you to instruct an agent's browser to open a user's profile.
    ##
    ## #### Allowed For
    ##
    ##  * Agents
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/channels/voice/agents/{agent_id}/users/{user_id}/display.json \
    ##   -H "Content-Type: application/json" \
    ##   -v -u {email_address}:{password} -X POST
    ## ```
    ## #### Example Response
    ##
    ## When the message has been sucessfully delivered to an agent's browser:
    ##
    ## ```http
    ## Status: 200 OK
    ## ```
    ##
    ## When the `agent_id` or `user_id` is invalid:
    ##
    ## ```http
    ## Status: 404 Not Found
    ## ```
    allow_parameters :display, agent_id: Parameters.bigid, id: Parameters.bigid
    require_oauth_scopes :display, scopes: [:write]
    def display
      user = current_account.users.find(params[:id])
      agent = current_account.agents.find(params[:agent_id])

      capture_cti_usage(action: 'users#display', display_user_id: agent.try(:id))

      raise ActiveRecord::RecordNotFound if agent.is_end_user?

      ::RadarFactory.create_radar_notification(current_account, "voice_integration_users/#{agent.id}").
        send("id", user.id)
      head :ok
    end
  end
end
