require 'zendesk/radar_factory'
module Api::V2::Channels::Voice
  class TicketsController < Api::V2::TicketsController
    include Zendesk::Voice::ControllerSupport

    ## ### Open Ticket in Agent's Browser
    ## `POST /api/v2/channels/voice/agents/{agent_id}/tickets/{ticket_id}/display.json`
    ##
    ## Allows you to instruct an agent's browser to open a ticket.
    ##
    ## #### Allowed For
    ##
    ##  * Agents
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/channels/voice/agents/{agent_id}/tickets/{ticket_id}/display.json \
    ##   -H "Content-Type: application/json" \
    ##   -v -u {email_address}:{password} -X POST
    ## ```
    ##
    ## #### Example Responses
    ##
    ## When the message is successfully delivered to an agent's browser:
    ##
    ## ```http
    ## Status: 200 OK
    ## ```
    ##
    ## When `agent_id` or `ticket_id` is invalid:
    ##
    ## ```http
    ## Status: 404 Not Found
    ## ```
    allow_parameters :display, id: Parameters.bigid, agent_id: Parameters.bigid
    require_oauth_scopes :display, scopes: %i[tickets:write write]
    def display
      ticket = current_account.tickets.find_by_nice_id!(params[:id])
      agent = find_agent(params[:agent_id])

      capture_cti_usage(action: 'tickets#display', display_user_id: agent.try(:id), ticket_id: ticket.id)

      display_to_agent(agent.id, ticket.nice_id)
      head :ok
    end

    ## ### Create Ticket
    ## `POST /api/v2/channels/voice/tickets.json`
    ##
    ## #### Allowed For
    ##
    ##  * Agents
    ##
    ## #### Request parameters
    ##
    ## The POST request takes a mandatory `ticket` object that lists the values to set when the ticket is created.
    ## You may also include an optional `display_to_agent` value such as the ID of the agent that will see the newly created ticket.
    ##
    ## Tickets created using this endpoint must have a `via_id` parameter. See the following
    ## section for possible values.
    ##
    ## #### Zendesk Talk Integration Via IDs
    ##
    ## Tickets created using this endpoint must have one of the following `via_id` parameters:
    ##
    ## | ID       | Description
    ## | ---------| -------------
    ## | 44       | Voicemail
    ## | 45       | Phone call (inbound)
    ## | 46       | Phone call (outbound)
    ##
    ## #### Using curl
    ##
    ## **data.json file**
    ##
    ## ```json
    ## {
    ##   "display_to_agent": 1234,
    ##   "ticket": {
    ##     "via_id": 45,
    ##     "subject": "My printer is on fire!",
    ##     "comment": { "body": "The smoke is very colorful." },
    ##     "priority": "urgent"
    ##   }
    ## }
    ## ```
    ##
    ## **curl request**
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/channels/voice/tickets.json \
    ##   -d @data.json \
    ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X POST
    ## ```
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 201 Created
    ## Location: https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json
    ##
    ## {
    ##   "ticket": {
    ##     {
    ##       "id":      35436,
    ##       "subject": "My printer is on fire!",
    ##       ...
    ##     }
    ##   }
    ## }
    ## ```
    ##
    ## ### Create Voicemail Ticket
    ## `POST /api/v2/channels/voice/tickets.json`
    ##
    ## #### Request parameters
    ##
    ## The POST request takes a mandatory `ticket` object that lists the values to set when the ticket is created.
    ## The ticket must have a `voice_comment` with the following values:
    ##
    ## | Name               | Type                  | Comment
    ## | ------------------ | ----------------------| -------
    ## | from               | string                | Incoming phone number
    ## | to                 | string                | Dialed phone number
    ## | recording_url      | string                | URL of the recording
    ## | started_at         | date                  | [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601) timestamp of the call starting time
    ## | call_duration      | integer               | Duration in seconds of the call
    ## | answered_by_id     | integer               | The agent who answered the call
    ## | transcription_text | string                | Transcription of the call (optional)
    ## | location           | string                | Location of the caller (optional)
    ##
    ## #### Using curl
    ##
    ## **data.json file**
    ##
    ## ```json
    ## {
    ##   "ticket": {
    ##     "via_id": 44,
    ##     "description": "Voicemail from: +16617480240",
    ##     "voice_comment": {
    ##       "from": "+16617480240",
    ##       "to": "+16617480123",
    ##       "recording_url": "http://yourdomain.com/recordings/1.mp3",
    ##       "started_at": "2019-04-16T09:14:57Z",
    ##       "call_duration": 40,
    ##       "answered_by_id": 28,
    ##       "transcription_text": "The transcription of the call",
    ##       "location": "Dublin, Ireland"
    ##     }
    ##   }
    ## }
    ## ```
    ##
    ## **curl request**
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/channels/voice/tickets.json \
    ##   -d @data.json \
    ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X POST
    ## ```
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 201 Created
    ## Location: https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json
    ##
    ## {
    ##   "ticket": {
    ##     {
    ##       "id":      35436,
    ##       "description": "Incoming phone call from: +16617480240",
    ##       ...
    ##     }
    ##   }
    ## }
    ## ```
    allow_parameters :create, display_to_agent: Parameters.bigid, ticket: Parameters.anything
    require_oauth_scopes :create, scopes: %i[tickets:write write]
    def create
      includes << :audit # Side-load the newly created audit in the response

      initialized_ticket.save!

      agent = find_agent(params['display_to_agent']) if params['display_to_agent']
      display_to_agent(agent.id, initialized_ticket.nice_id) if agent

      capture_cti_usage(action: 'tickets#create', display_user_id: agent.try(:id), ticket_id: initialized_ticket.id)

      render json: presenter.present(initialized_ticket), status: :created, location: presenter.url(initialized_ticket)
    end

    private

    def find_agent(user_id)
      current_account.agents.find(user_id)
    end

    def display_to_agent(agent_id, ticket_id)
      ::RadarFactory.create_radar_notification(current_account, "voice_integration_tickets/#{agent_id}").
        send("id", ticket_id)
    end
  end
end
