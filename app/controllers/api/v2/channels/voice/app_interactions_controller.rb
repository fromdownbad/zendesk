require 'zendesk/radar_factory'
module Api::V2::Channels::Voice
  class AppInteractionsController < BaseController
    include Zendesk::Voice::ControllerSupport
    ## ### Record App Interactions
    ## POST /api/v2/channels/voice/app_interactions.json?event={event_string}`
    ##
    ## Allows you record usage by an app that has listened to some event
    ##
    ## **curl request**
    ##
    ## ```bash
    # curl https://{subdomain}.zendesk.com/api/v2/channels/voice/app_interactions.json?event={event_string}
    #   -H "Content-Type: application/json" -v -u {email_address}:{password} -X POST
    ## ```
    ## #### Example Responses
    ##
    ## When the message is successfully delivered to an agent's browser:
    ##
    ## ```http
    ## Status: 204 no content
    ## ```
    ##
    ## When event param is not sent:
    ##
    ## ```http
    ## Status: 400 Bad Request
    ## ```

    allow_parameters :create, id: Parameters.bigid, event: Parameters.string
    require_oauth_scopes :create, scopes: [:write]
    def create
      params.require(:event)
      capture_cti_usage(action: 'app_interactions#create')
      head :no_content
    end
  end
end
