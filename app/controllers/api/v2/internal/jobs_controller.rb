module Api::V2::Internal
  class JobsController < Api::V2::Internal::BaseController
    include Zendesk::ImportExport::JobEnqueue

    # NOTE This controller only implements two_factor_export
    # foundation for all jobs but throttle and validation would need to be added.

    allow_subsystem_user :centraladminapp

    before_action :set_job_klass
    before_action :valid_two_factor_export_job
    before_action :require_requester_id

    layout false

    allow_parameters :create,
      type: Parameters.enum('two_factor_export'),
      requester_id: Parameters.bigid

    def create
      @current_user = requester
      enqueue
      respond(:notice, notice_message)
    rescue Resque::ThrottledError
      respond_when_throttled
    end

    private

    def valid_two_factor_export_job
      render_failure(
        message: 'Internal Job Not Implemented',
        status: :unauthorized
      ) unless params[:type] == 'two_factor_export'
    end

    def require_requester_id
      return if params[:requester_id].present?
      render status: :bad_request,
             json: {
               error: 'MissingRequesterId',
               description: "'requester_id' is a required field"
             }
    end

    def respond_when_throttled
      render status: :too_many_requests,
             json: {
                error: 'TooManyRequests',
                description: "too many requests made to #{params[:type]} job"
             }
    end

    def requester
      @requester ||= current_account.users.find(params[:requester_id])
    end
  end
end
