require 'zendesk/users/user_manager'

module Api
  module V2
    module Internal
      class PasswordController < Api::V2::Internal::BaseController
        include Zendesk::StaffServiceControllerSupport

        rescue_from Zendesk::UserManager::Error do
          I18n.with_locale(@locale_for_exception_rendering) do
            render json: errors_presenter.present(staff), status: :unprocessable_entity
          end
        end

        allow_parameters :reset,
          requester_id: Parameters.bigid,
          staff_id: Parameters.bigid
        def reset
          token = if current_account.has_generate_password_reset_token_subclass?
            staff.password_reset_tokens.create
          else
            staff.verification_tokens.create
          end

          if staff.crypted_password.nil?
            UsersMailer.deliver_multiproduct_admin_password_change(staff, token.value)
          else
            UsersMailer.deliver_multiproduct_password_reset(staff, token.value)
          end

          head :ok
        end

        allow_parameters :create,
          requester_id: Parameters.bigid,
          password: Parameters.string,
          staff_id: Parameters.bigid
        def create
          user_manager.reset_password_for(staff, to: params[:password], verify_identity: true)

          head :ok
        end

        allow_parameters :update,
          requester_id: Parameters.bigid,
          password: Parameters.string,
          previous_password: Parameters.string,
          session_record_id: Parameters.bigid,
          staff_id: Parameters.bigid
        def update
          user_manager.change_password_for(staff,
            from: params[:previous_password],
            to: params[:password],
            shared_session_record_id: params[:session_record_id])

          head :ok
        end

        private

        def errors_presenter
          @errors_presenter ||= Api::V2::ErrorsPresenter.new
        end

        def staff
          @staff ||= current_account.users.find(params[:staff_id])
        end

        def user_manager
          authentication.current_user = requester
          @user_manager ||= Zendesk::UserManager.new(authentication)
        end
      end
    end
  end
end
