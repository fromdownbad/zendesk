module Api
  module V2
    module Internal
      class AuditsController < Api::V2::Internal::BaseController
        allow_parameters :all, ticket_id: Parameters.bigid
        require_that_user_can! :view_private_content, :ticket

        allow_parameters :index, filter_events: Parameters.array(Parameters.string)
        def index
          render json: presenter.present(audits)
        end

        private

        def ticket
          @ticket ||= current_account.tickets.for_user(current_user).find_by_id!(params[:ticket_id])
        end

        def audits
          @audits ||= paginate(ordered_audit_collection)
        end

        def presenter
          @presenter ||= if params[:filter_events]
            Api::V2::Tickets::AuditPresenter.new(current_user, url_builder: self, includes: includes, filter_event_types: params[:filter_events])
          else
            Api::V2::Tickets::AuditPresenter.new(current_user, url_builder: self, includes: includes)
          end
        end

        def ordered_audit_collection
          audits = ticket.audits

          if ticket.archived?
            if params[:sort_order] == "desc"
              audits = audits.to_a.reverse! # proxy needs to be converted to array before reversing
            end
          else
            audits = audits.order(requested_order)
          end

          audits
        end

        def requested_order
          if params[:sort_order] == "desc"
            "events.created_at DESC, events.id DESC"
          else
            "events.created_at ASC, events.id ASC"
          end
        end
      end
    end
  end
end
