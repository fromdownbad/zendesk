module Api
  module V2
    module Internal
      class FieldExportController < Api::V2::Internal::BaseController
        allow_parameters :ticket_fields, :skip
        require_oauth_scopes :ticket_fields, scopes: [:read], arturo: true
        def ticket_fields
          field_types_not_exported = ['FieldDescription']
          fields = current_account.ticket_fields
          exportable_fields = fields.reject { |field| field_types_not_exported.include?(field.type.to_s) }

          json = { 'ticket_fields' => exportable_fields.as_json }
          render json: json
        end

        allow_parameters :custom_fields, :skip
        require_oauth_scopes :custom_fields, scopes: [:read], arturo: true
        def custom_fields
          owner = params[:owner]
          json = { "#{owner.downcase}_fields" => current_account.custom_fields_for_owner(owner) }
          render json: json
        end

        allow_parameters :update_exportable_fields, :skip
        require_oauth_scopes :update_exportable_fields, scopes: [:write], arturo: true
        def update_exportable_fields
          type = params[:type]
          id_value_map = params[:id_value_map]

          fields = if type == 'ticket'
            current_account.ticket_fields.where(id: id_value_map.keys)
          else
            current_account.custom_fields_for_owner(type.capitalize).where(id: id_value_map.keys)
          end

          fields.each do |field|
            field.is_exportable = id_value_map[field.id.to_s] == 'true'
            field.save!
          end

          render json: {}
        end
      end
    end
  end
end
