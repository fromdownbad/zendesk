module Api::V2::Internal
  class StaffEventsController < Api::V2::Internal::BaseController
    allow_subsystem_user :metropolis

    ENTITLEMENT_UPDATED = 'entitlement_updated'.freeze

    ## ### Staff notifications
    ## `POST /api/v2/internal/staff_events`
    ##
    ## Create new change notification for staff user
    ##
    ## #### Allowed for
    ##
    ## * Metropolis system user
    ##
    ## #### Example Request
    ##
    ## {
    ##   "account_id": 1,
    ##   "user_id": 1,
    ##   "notification_type": "entitlement_updated",
    ## }
    allow_parameters :create,
      account_id: Parameters.bigid,
      user_id: Parameters.bigid,
      shard_id: Parameters.bigid, # From logging, untested
      pod_id: Parameters.bigid, # From logging, untested
      subdomain: Parameters.string, # From logging, untested
      updated_at: Parameters.datetime, # From logging, untested
      notification_type: Parameters.string
    def create
      Rails.logger.info("Received staff change notification for user ##{params[:user_id]} with #{params[:notification_type]}")

      sync_with_metropolis if user&.is_agent? && params[:notification_type] == ENTITLEMENT_UPDATED

      head :ok
    end

    protected

    def sync_with_metropolis
      Rails.logger.info("User ##{params[:user_id]}'s entitlements changed.")

      entitlements = Zendesk::StaffClient.new(current_account).get_entitlements!(params[:user_id])
      entitlements.each do |key, value|
        if key == 'support'
          Zendesk::SupportUsers::EntitlementSynchronizer.perform(user, strategy: :pull) if current_account.has_ocp_reverse_sync?
        else
          sync_other_product_entitlements(product: key, entitlement: value)
        end
      end
    end

    def sync_other_product_entitlements(product:, entitlement:)
      user.update_entitlement(product: product, new_entitlement: entitlement)
    end

    def user
      @user ||= current_account.users.find_by_id(params[:user_id])
    end
  end
end
