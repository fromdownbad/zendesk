module Api::V2::Internal
  class AccountSettingsController < Api::V2::Internal::BaseController
    allow_subsystem_user :embeddable, only: [:index, :show_many, :update]
    allow_subsystem_user :passport, only: [:index, :show_many, :update]
    allow_subsystem_user :metropolis, only: [:index, :show_many, :update]
    allow_subsystem_user :zopim, only: [:index, :show_many, :update]
    allow_subsystem_user :sunco_configurator, only: [:index, :show_many, :update]
    allow_subsystem_user :answer_bot_flow_composer, only: [:index, :show_many]

    ## ### Account Settings
    ## `GET /api/v2/internal/account/settings`
    ##
    ## Enabling import mode relaxes the API throttles for the given account for 2 weeks.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ## ```
    allow_parameters :index, {}
    def index
      if stale?(last_modified: current_account.settings.maximum(:updated_at))
        render json: { account_settings: current_account.settings.get }
      end
    end

    ## ### List Account Settings by ID
    ## `GET /api/v2/internal/account/settings/show_many.json?ids=ticket_tagging,polaris`
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ## #### Available Parameters
    ##
    ## | Name       | Type        | Comment
    ## | ---------- | ----------- | -------
    ## | ids        | list of id  | List of account setting names, separated by commas.
    ##
    allow_parameters :show_many, ids: Parameters.string
    def show_many
      return head :bad_request unless many_ids?

      setting_ids = params[:ids].split ','
      # get() would return ALL settings if an empty array is passed
      settings = setting_ids.any? ? current_account.settings.get(setting_ids) : {}

      render json: { account_settings: settings }
    end

    ## ### Account Settings
    ## `PUT /api/v2/internal/account/settings`
    ##
    ## Enabling import mode relaxes the API throttles for the given account for 2 weeks.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ## ```
    allow_parameters :update, account_settings: Parameters.anything
    require_oauth_scopes :settings, scopes: [:write], arturo: true
    def update
      params.fetch(:account_settings, {}).each do |key, value|
        set_account_setting key, value
      end

      current_account.touch_without_callbacks

      head :ok
    rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotSaved => e
      ZendeskExceptions::Logger.record(e, location: self, message: e.message, fingerprint: 'c04a973513e4e9eb7d845bd5d14c932200a1b787')
      render status: :unprocessable_entity, json: { error: e.message }
    end
  end
end
