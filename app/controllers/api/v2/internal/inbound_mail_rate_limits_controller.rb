module Api::V2::Internal
  class InboundMailRateLimitsController < Api::V2::Internal::BaseController
    before_action :validate_account, only: [:index, :show, :audits]
    before_action :validate_email, only: [:create, :update]
    before_action :validate_rate_limit, only: [:create, :update]
    around_action :with_cia_audit, only: [:create, :update, :destroy]

    INBOUND_MAIL_RATE_LIMIT_PARAMS = {
      account_id: Parameters.bigid,
      deleted_at: Parameters.nil_string | Parameters.datetime,
      description: Parameters.string,
      email: Parameters.string,
      is_active: Parameters.boolean,
      is_sender: Parameters.boolean,
      rate_limit: Parameters.integer,
      ticket_id: Parameters.bigid
    }.freeze

    allow_parameters :index,
      account_id: Parameters.bigid
    def index
      inbound_mail_rate_limits = InboundMailRateLimit.with_deleted do
        account.inbound_mail_rate_limits.all
      end

      render json: collection_presenter.present(inbound_mail_rate_limits)
    end

    allow_parameters :show,
      account_id: Parameters.bigid,
      id: Parameters.bigid
    def show
      inbound_mail_rate_limit = InboundMailRateLimit.with_deleted do
        account.inbound_mail_rate_limits.find(params[:id])
      end

      return head(:not_found) unless inbound_mail_rate_limit.present?

      render json: presenter.present(inbound_mail_rate_limit)
    end

    allow_parameters :create,
      account_id: Parameters.bigid,
      inbound_mail_rate_limit: INBOUND_MAIL_RATE_LIMIT_PARAMS,
      user: {
        email: Parameters.string,
        name: Parameters.string
      }
    def create
      return restore if deleted_inbound_mail_rate_limit.present?

      inbound_mail_rate_limit = InboundMailRateLimit.new(
        description: description,
        email: email,
        is_active: true,
        is_sender: inbound_mail_rate_limit_data[:is_sender],
        rate_limit: rate_limit
      ).tap do |new_inbound_mail_rate_limit|
        new_inbound_mail_rate_limit.account_id = account_id
        new_inbound_mail_rate_limit.ticket_id = ticket_id
      end

      inbound_mail_rate_limit.audit_message = cia_audit_user
      inbound_mail_rate_limit.save

      render json: presenter.present(inbound_mail_rate_limit)
    end

    allow_parameters :update,
      account_id: Parameters.bigid,
      id: Parameters.bigid,
      inbound_mail_rate_limit: INBOUND_MAIL_RATE_LIMIT_PARAMS,
      user: {
        email: Parameters.string,
        name: Parameters.string
      }
    def update
      inbound_mail_rate_limit = InboundMailRateLimit.find(params[:id])
      inbound_mail_rate_limit.audit_message = cia_audit_user
      inbound_mail_rate_limit.update(inbound_mail_rate_limit_data)
      CIA.record(:update, inbound_mail_rate_limit)

      render json: collection_presenter.present(inbound_mail_rate_limit)
    end

    allow_parameters :destroy,
      account_id: Parameters.bigid,
      id: Parameters.bigid,
      user: {
        email: Parameters.string,
        name: Parameters.string
      }
    def destroy
      inbound_mail_rate_limit = InboundMailRateLimit.find(params[:id])

      return head(:not_found) unless inbound_mail_rate_limit.present?

      inbound_mail_rate_limit.soft_delete!
      inbound_mail_rate_limit.audit_message = cia_audit_user
      CIA.record(:destroy, inbound_mail_rate_limit)

      render json: collection_presenter.present(inbound_mail_rate_limit)
    end

    allow_parameters :audits,
      account_id: Parameters.bigid,
      id: Parameters.bigid
    def audits
      cia_events = InboundMailRateLimit.with_deleted do
        account.inbound_mail_rate_limits.find(params[:id]).cia_events.order(created_at: :desc)
      end

      render json: audit_log_presenter.present(cia_events)
    end

    private

    def with_cia_audit
      CIA.audit(actor: current_user, ip_address: request.remote_ip) { yield }
    end

    def restore
      deleted_inbound_mail_rate_limit.rate_limit = rate_limit
      deleted_inbound_mail_rate_limit.description = description
      deleted_inbound_mail_rate_limit.deleted_at = nil
      deleted_inbound_mail_rate_limit.is_active = true
      deleted_inbound_mail_rate_limit.audit_message = cia_audit_user
      deleted_inbound_mail_rate_limit.save
      deleted_inbound_mail_rate_limit.audit_message = cia_audit_user
      CIA.record(:restore, deleted_inbound_mail_rate_limit)

      render json: presenter.present(deleted_inbound_mail_rate_limit)
    end

    def validate_account
      head(:not_found) unless account.present?
    end

    def deleted_inbound_mail_rate_limit
      @deleted_inbound_mail_rate_limit ||= InboundMailRateLimit.with_deleted do
        InboundMailRateLimit.where.not(
          deleted_at: nil
        ).find_by(
          account_id: account_id,
          is_sender: inbound_mail_rate_limit_data[:is_sender],
          email: email
        )
      end
    end

    def valid_email?(value)
      Zendesk::Mail::Address.valid_address?(value)
    end

    def valid_integer?(value)
      value && value.to_s =~ /\A\d+\Z/
    end

    def validate_email
      head(:bad_request) unless !email || valid_email?(email)
    end

    def validate_rate_limit
      head(:bad_request) unless !rate_limit || valid_integer?(rate_limit)
    end

    def audit_log_presenter
      @audit_log_presenter ||= Api::V2::Internal::InboundMailRateLimitAuditLogPresenter.new(current_user, url_builder: self)
    end

    def presenter
      @presenter ||= Api::V2::Internal::InboundMailRateLimitPresenter.new(current_user, url_builder: self)
    end

    def collection_presenter
      @collection_presenter ||= begin
        Api::V2::CollectionPresenter.new(
          current_user,
          url_builder: self,
          item_presenter: presenter
        )
      end
    end

    def account
      @account ||= Account.find(params[:account_id])
    end

    def cia_audit_user
      return nil unless params[:user].present?

      {
        name: params[:user][:name],
        email: params[:user][:email]
      }.to_json
    end

    def inbound_mail_rate_limit_data
      params[:inbound_mail_rate_limit] || {}
    end

    def account_id
      inbound_mail_rate_limit_data[:account_id]
    end

    def email
      inbound_mail_rate_limit_data[:email]
    end

    def description
      inbound_mail_rate_limit_data[:description]
    end

    def rate_limit
      inbound_mail_rate_limit_data[:rate_limit]
    end

    def ticket_id
      inbound_mail_rate_limit_data[:ticket_id]
    end
  end
end
