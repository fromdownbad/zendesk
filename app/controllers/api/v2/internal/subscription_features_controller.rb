module Api::V2::Internal
  class SubscriptionFeaturesController < BaseController
    before_action :validate_feature,   only: :create_master_job
    before_action :validate_local_pod, only: :create_pod_job

    ## ### Initialize a backfill job across all accounts for a given feature
    ## `POST /api/v2/internal/subscription_features/master`
    ##
    ## #### Allowed for
    ##
    ## * Subsystem User
    ##
    ## #### Using cURL
    ##
    ## ```bash
    ## curl -X PUT https://{subdomain}.zendesk.com/api/v2/internal/subscription_features/master \
    ## -d '{ "feature_name": "valid_feature" }' \
    ##  -v -u {email_address}:{password}
    ## ```
    ##
    ## ### Example Response
    ##
    ## ```http
    ## 201 created
    ## ```
    ##
    allow_parameters :create_master_job, feature_name: Parameters.string
    def create_master_job
      internal_clients.each do |pod_id, client|
        client.connection.post(
          '/api/v2/internal/subscription_features/local',
          pod_id: pod_id
        )
      end
      head :created
    end

    ## ### Initialize a backfill job for a specific pod
    ## `POST /api/v2/internal/subscription_features/local`
    ##
    ## #### Allowed for
    ##
    ## * Subsystem User
    ##
    ## #### Using cURL
    ##
    ## ```bash
    ## curl -X PUT https://{subdomain}.zendesk.com/api/v2/internal/subscription_features/local \
    ## -d '{ "pod_id": "valid_pod_id" }' \
    ##  -v -u {email_address}:{password}
    ## ```
    ##
    ## #### Example Response
    ##
    ## ```http
    ## 201 created
    ## ```
    ##
    allow_parameters :create_pod_job, pod_id: Parameters.integer
    def create_pod_job
      CreateFeatureOnLocalPodJob.enqueue(params[:pod_id])
      head :created
    end

    private

    def validate_feature
      feature_name = params[:feature_name].try(:to_sym)
      return if feature_name.present? && all_features.include?(feature_name)

      message = "feature (#{feature_name}) is either blank or does not exist " \
                "in any catalog."
      render json: { error: message }, status: :unprocessable_entity
    end

    def validate_local_pod
      pod_id = params[:pod_id]
      return if pod_id.present? && all_pods.include?(pod_id.to_i)

      message = "pod #{params[:pod_id]} does not exist in Zendesk config"
      render json: { error: message }, status: :unprocessable_entity
    end

    def internal_clients
      @internal_clients ||= all_pods.each_with_object({}) do |pod_id, result|
        unless Account.find_by_subdomain("pod#{pod_id}").nil?
          result[pod_id] = Zendesk::InternalApi::Client.new("pod#{pod_id}")
        end
      end
    end

    def all_pods
      Zendesk::Configuration::PodConfig.pod_ids
    end

    def all_features
      @all_features ||= (patagonia_features + legacy_features).uniq
    end

    def patagonia_features
      Zendesk::Features::Catalogs::Catalog.features.map(&:name)
    end

    def legacy_features
      Zendesk::Features::Catalogs::LegacyCatalog.features.map(&:name)
    end
  end
end
