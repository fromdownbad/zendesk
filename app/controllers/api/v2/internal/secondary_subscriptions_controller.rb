module Api::V2::Internal
  class SecondarySubscriptionsController < Api::V2::Internal::BaseController
    ## ### Fetch the account's Secondary Subscription records
    ##
    ## `GET /api/v2/internal/secondary_subscriptions.json`
    ##
    ## This will fetch the Secondary Subscription records of the account, AKA
    ## the different seasonal agent add-ons.
    ##
    ## #### Allowed for
    ##
    ##  * allow_only_subsystem_user :zendesk
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ##   "secondary_subscriptions": [
    ##     {
    ##       "zuora_rate_plan_id": "123456",
    ##       "product": "temporary_zendesk_agents",
    ##       "quantity": 5,
    ##       "starts_at": "2016-03-22 00:00:00Z"
    ##       "expires_at": "2016-03-22 00:00:00Z"
    ##       "status": "started"
    ##     },
    ##     {
    ##       "zuora_rate_plan_id": "234567",
    ##       "product": "temporary_zendesk_agents",
    ##       "quantity": 2,
    ##       "starts_at": "2016-04-15 00:00:00Z"
    ##       "expires_at": "2016-04-15 00:00:00Z"
    ##       "status": "started"
    ##     },
    ##     ...
    ##   ],
    ## ```
    ##
    allow_parameters :index, {}
    def index
      render json: presenter.collection_presenter.present(secondary_subscriptions)
    end

    private

    def presenter
      Api::V2::Account::SecondarySubscriptionsPresenter.new(
        current_user,
        url_builder: self
      )
    end

    def secondary_subscriptions
      current_account.subscription_feature_addons.
        active_plus_grace_period(SubscriptionFeatureAddon::TEMPORARY_ZENDESK_AGENTS_GRACE_PERIOD).
        by_name(:temporary_zendesk_agents)
    end
  end
end
