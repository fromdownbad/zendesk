module Api
  module V2
    module Internal
      class AuditEmailsController < Api::V2::Internal::BaseController
        class DeletedTicketError < StandardError; end

        helper TabsHelper
        helper_method :raw_email

        skip_before_action :enforce_request_format

        before_action :ensure_audit_has_raw_email

        layout false

        rescue_from DeletedTicketError do
          head :gone
        end

        allow_parameters :show,
          account_id: Parameters.bigid,
          id: Parameters.bigid,
          ticket_id: Parameters.bigid
        def show
          if stale?(etag: ['audit_email', audit, request.format], last_modified: audit.created_at)
            respond_to do |format|
              format.json { render json: raw_email.json }

              format.eml { send_data(raw_email.eml, type: :eml, filename: raw_email.eml_file_name) }

              format.text { render plain: raw_email.text }

              format.html { render 'audit_emails/show' }
            end
          end
        end

        private

        def audit
          return @audit if defined?(@audit)

          @audit ||= begin
            ticket = current_account.tickets.find_by_nice_id(ticket_id)

            if ticket.nil?
              deleted_ticket = Ticket.with_deleted { current_account.tickets.find_by_nice_id(ticket_id) }

              return nil unless deleted_ticket.present?

              raise DeletedTicketError, "ticket #{ticket_id} was been deleted on #{deleted_ticket.status_updated_at}"
            end

            ticket.comments.find(params[:id].to_i)&.audit
          end
        end

        def ensure_audit_has_raw_email
          return if raw_email.present? && raw_email.message.present? && audit.ticket

          render 'audit_emails/not_found', status: :not_found
        end

        def ticket_id
          params[:ticket_id]
        end

        def raw_email
          audit&.raw_email
        end
      end
    end
  end
end
