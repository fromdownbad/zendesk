module Api::V2::Internal
  class UserOtpSettingsController < Api::V2::Internal::BaseController
    allow_only_subsystem_users :centraladminapp

    rescue_from ActiveRecord::RecordNotFound do |exception|
      render status: :not_found, json: {
        error: "RecordNotFound",
        description: exception.message
      }
    end

    ## ### Deletes the otp_setting record associated with the user
    ## `DELETE /api/v2/internal/users/:user_id/user_otp_settings.json`
    ##
    ## Endpoint that allows centraladminapp to delete the otp_setting record associated with a user's two factor configuration setup.
    ##
    ## #### Allowed For
    ##
    ##  * 'centraladminapp' Subsystem User
    ##
    ## #### Example Response
    ##
    ##  ```Status: 200 Ok```
    allow_parameters :destroy, user_id: Parameters.bigid
    def destroy
      user_otp_setting.destroy
      head :ok
    end

    ## ### Retrieves the recovery code details for the user
    ## `GET /api/v2/internal/users/:user_id/recovery_code.json`
    ##
    ## Endpoint that allows centraladminapp to get the recovery codes for a user.
    ##
    ## #### Allowed For
    ##
    ##  * 'centraladminapp' Subsystem User
    ##
    ## #### Example Response
    ##
    ##  ```Status: 200 Ok {"recovery_code":"rAnDoMCoDe"}```
    allow_parameters :recovery_code, user_id: Parameters.bigid
    def recovery_code
      return deny_access unless otp_configured?
      statsd_client.increment('get_recovery_code', tags: ["caller:central_admin"])
      render json: { recovery_code: user_otp_setting.backup_codes["codes"].first }
    end

    ## ### Returns if the current user OTP setting is configured or not
    ## `GET /api/v2/internal/users/:user_id/user_otp_settings/configured.json`
    ##
    ## Endpoint that allows centraladminapp to determine if a user's two factor configuration is enabled.
    ##
    ## #### Allowed For
    ##
    ##  * 'centraladminapp' Subsystem User
    ##
    ## #### Example Response
    ##
    ##  ```
    ##   When OTP is configured:
    ##   Status: 200 OK { "otp_configured": true | false }
    ##   When user is not found:
    ##   Status: 404 NOT FOUND
    ##  ```
    allow_parameters :configured, user_id: Parameters.bigid
    def configured
      render json: { otp_configured: otp_configured? }
    end

    private

    def otp_configured?
      !!otp_user.otp_configured?
    end

    def user_otp_setting
      @user_otp_setting ||= UserOtpSetting.find_by_user_id!(otp_user.id)
    end

    def otp_user
      @otp_user ||= current_account.users.find_by_id!(user_id)
    end

    def user_id
      @user_id ||= params[:user_id]
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['user_otp_settings_controller'])
    end
  end
end
