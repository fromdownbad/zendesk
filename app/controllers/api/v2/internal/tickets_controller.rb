module Api
  module V2
    module Internal
      class TicketsController < Api::V2::Internal::BaseController
        before_action :find_user, :find_ticket, only: [:permissions]

        allow_pigeon_user only: %i[permissions]

        ## ### List Tickets
        ## `GET /api/v2/internal/tickets.json`
        ##
        ## This is a special case internal API that allows you to find tickets requested by
        ## a list of requester email addresses.
        ##
        ## The special case supported here, is one required by the Zendesk Monitor, such that we
        ## can see if the agents on a specific account have any recent ticket with us on https://support.zendesk.com
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        ## #### Example Response
        ##
        ## As a regular ticket
        allow_parameters :index, :skip # TODO: make stricter
        def index
          render json: presenter.present(tickets)
        end

        ## ### List Multiple Ticket Comments
        ## `GET /api/v2/internal/tickets/show_many_comments.json?ticket_ids=1,2,3&id_waterline=2&comment_id_waterline=10001`
        ##  or
        ## `GET http://dev.zd-dev.com/api/v2/internal/tickets/show_many_comments.json?ticket_ids[]=1&ticket_ids[]=2&id_waterline=2&comment_id_waterline=10001`
        ##
        ## This is a special case internal API that allows you to find comments for many tickets requested by
        ## a list of several ticket ids.
        ##
        ## The special case supported here, is one required by Zendesk Voyager, such that we can get many comments
        ## for many tickets without having to make 1 call per ticket.
        ## This endpoint is restricted to a maximum of 100 ticket_ids
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        ## #### Example Response
        ##
        ## {
        ##   "comments": [
        ##     {
        ##       "id": 10171,
        ##       "type": "Comment",
        ##       "author_id": 10006,
        ##       "ticket_id": 1,
        ##       "body": "This is the first comment. Feel free to delete this sample ticket.",
        ##       "html_body": "<p>This is the first comment. Feel free to delete this sample ticket.<\/p>",
        ##       "public": true,
        ##       "attachments": [
        ##
        ##       ],
        ##       "via": {
        ##         "channel": "sample_ticket",
        ##         "source": {
        ##           "from": {
        ##
        ##           },
        ##           "to": {
        ##           },
        ##          "rel": null
        ##         }
        ##       },
        ##      "metadata": {
        ##        "system": {
        ##
        ##        },
        ##        "custom": {
        ##
        ##        }
        ##      },
        ##      "created_at": "2015-03-12T11:10:08.000Z"
        ##    },
        ##
        ## ...
        ##
        ##  ],
        ##    "next_page": null,
        ##    "previous_page": null,
        ##    "count": 3
        ## }
        ##
        allow_parameters :show_many_comments, {
          ticket_ids: Parameters.array(Parameters.bigid) | Parameters.ids,
          id_waterline: Parameters.bigid,
          comment_id_waterline: Parameters.bigid | Parameters.nil,
          max_comments: Parameters.integer | Parameters.nil
        }.freeze
        def show_many_comments
          raise ActionController::ParameterMissing, 'Ids must be present' unless ticket_ids?
          raise ZendeskApi::Controller::Errors::TooManyValuesError.new(:ticket_ids, max_ids) if ticket_ids.size > max_ids
          render json: comments_presenter.present(many_comments)
        end

        ## ### Archive Ticket
        ## `PUT /api/v2/internal/tickets/{nice_id}/archive.json`
        ##
        ## This is a special case internal API that allows you to archive tickets
        allow_parameters :archive, {}.freeze
        def archive
          return head :bad_request if Rails.env.production?
          ticket = current_account.tickets.find_by_nice_id!(params[:id])
          ticket.archive!(key_version: Zendesk::Archive::Archiver::KEY_VERSION)
          render json: presenter.present(ticket)
        end

        ## ### Get user permissions for a ticket
        ## `GET /api/v2/internal/tickets/{nice_id}/permissinos.json?user_id={user_id}`
        ##
        ## Internal API endpoint API to return the user permissions for a ticket
        allow_parameters :permissions, {
          id: Parameters.bigid,
          user_id: Parameters.bigid
        }.freeze
        def permissions
          render json: permissions_presenter.present(@ticket)
        end

        private

        def find_user
          @user = current_account.users.find_by_id(params[:user_id])
          render(json: not_found_error("Couldn't find User with id = #{params[:user_id]}"), status: :not_found) if @user.nil?
        end

        def find_ticket
          @ticket = current_account.tickets.find_by_nice_id(params[:id])
          render(json: not_found_error("Couldn't find Ticket with nice_id = #{params[:id]}"), status: :not_found) if @ticket.nil?
        end

        def not_found_error(msg)
          {
            error: 'RecordNotFound',
            description: msg
          }
        end

        def permissions_presenter
          @permissions_presenter ||= begin
            includes << :extended_permissions
            Api::V2::Tickets::TicketPermissionsPresenter.new(@user,
              url_builder: self,
              includes: includes)
          end
        end

        def requester_ids
          @requester_ids ||= current_account.user_identities.email.where(value: params[:emails]).pluck(:user_id).uniq
        end

        def tickets
          @tickets ||= current_account.tickets.where(requester_id: requester_ids).order(nice_id: :desc).paginate(pagination)
        end

        def presenter
          @presenter ||= Api::V2::Tickets::TicketPresenter.new(current_user, url_builder: self, includes: includes)
        end

        def comments_presenter
          @comments_presenter ||= Api::V2::Internal::CommentPresenter.new(current_user, ticket_ids: ticket_ids, limit: max_comments, url_builder: self)
        end

        def many_comments
          Zendesk::Export::CommentsFinder.new(current_account, ticket_ids, id_waterline, comment_id_waterline, max_comments).find_comments
        end

        def ticket_ids?
          params.key?(:ticket_ids) && !ticket_ids.empty?
        end

        def ticket_ids
          @ticket_ids ||= begin
            ticket_ids = params[:ticket_ids]
            ticket_ids = params[:ticket_ids].split(',').map(&:strip).map(&:to_i) if ticket_ids.is_a?(String)
            ticket_ids.uniq.sort
          end
        end

        def id_waterline
          params[:id_waterline] || 0
        end

        def comment_id_waterline
          params[:comment_id_waterline] || 0
        end

        def max_comments
          params[:max_comments] || 50
        end

        def max_ids
          100
        end
      end
    end
  end
end
