module Api
  module V2
    module Internal
      class AuditLogsController < Api::V2::Internal::BaseController
        # ### Create an Audit Log
        # `POST /api/v2/internal/audit_logs.json`
        #
        # This is an internal API that creates an audit log.
        #
        # #### Allowed For
        #
        #  * Subsystem Users
        #
        # #### Example Request
        #
        # ```js
        # {
        #   "event":{
        #     "account_id":2,
        #     "actor_id": 5,
        #     "actor_type": 'User',
        #     "ip_address": '172.58.4.6',
        #     "source_id": 3,
        #     "source_type": 'Voice::VoiceAccount',
        #     "source_display_name": 'Voice::VoiceAccount',
        #     "message": 'customer message',
        #     "visible": true,
        #     "action": 'update',
        #     "changes": {"attribute_name"=>[2, 3], "another_attribute_name"=>[false, true]}
        #    }
        # }
        # ```
        #
        # #### Response
        #
        #    Status: 201 Created
        #
        allow_only_subsystem_users :zendesk, :zopim, :account_service, :metropolis, :billing
        allow_parameters :create, event: Parameters.anything # TODO: make stricter
        def create
          return head :bad_request unless HashParam.ish?(params[:event])

          if current_account.has_audit_logs_refactor?
            CiaEventCreator.create!(params[:event])
          else
            event_params = params[:event].slice(
              :account_id,
              :actor_id,
              :actor_type,
              :ip_address,
              :source_id,
              :source_type,
              :source_display_name,
              :message,
              :visible,
              :action,
              :via_id,
              :via_reference_id
            )

            event = CIA::Event.new(event_params)
            Zendesk::Monitor::AuditLogsHandler.new(event).add_attribute_changes(params[:event])
            event.save!
          end

          head :created
        end
      end
    end
  end
end
