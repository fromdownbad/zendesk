module Api
  module V2
    module Internal
      class FraudScoresController < Api::V2::Internal::BaseController
        include Fraud::FraudScoreActions

        allow_subsystem_user :account_fraud_service
        allowed_fraud_score_parameters = {
          verified_fraud: Parameters.boolean,
          score: Parameters.boolean,
          account_fraud_service_release: Parameters.string,
          owner_email: Parameters.string,
          subdomain: Parameters.string,
          other_params: Parameters.anything,
          is_whitelisted: Parameters.boolean,
          score_params: Parameters.string,
          source_event: Parameters.string,
          account_id: Parameters.bigid,
          actions: Parameters.map(
            auto_suspend: Parameters.boolean,
            enable_captcha: Parameters.boolean
          )
        }

        rescue_from StandardError do |se|
          render json: { message: se.message, backtrace: se.backtrace }, status: :internal_server_error
        end

        ## ### List Accounts
        ## `GET /api/v2/internal/fraud_scores.json`
        ##
        ## Returns a list of Fraud Scores for the account ordered by +id+ in descending order,
        ## and capped at a 1000 rows.
        ##
        ## #### Example Response
        ##
        ## {
        ##   "fraud_scores": [
        ##      {
        ##        "id":527,
        ##        "account_id": 48 ,
        ##        "verified_fraud":false,
        ##        "account_fraud_service_release":"1",
        ##        "other_params": {"owner_email":"indika+dev1@zendesk.com","subdomain":"dev1"},
        ##        ...
        ##      },
        ##     ...
        ##   ]
        ## }
        allow_parameters :index, {}
        def index
          fraud_scores = current_account.fraud_scores.order('created_at DESC').limit(1000)
          render json: presenter.collection_presenter.present(fraud_scores)
        end

        ## ### Show a Fraud Score record
        ## `GET /api/v2/internal/fraud_scores/:id.json`
        ##
        ##  Returns an fraud score for your account.
        ##
        ##
        ## ```bash
        ## curl https://{subdomain}.zendesk.com/api/v2/internal/fraud_scores/{id}.json \
        ##   -v -u {email_address}:{password}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "fraud_score": {
        ##     ...
        ##   }
        ## }
        ## ```
        ##
        resource_action :show

        ## ### Create FraudScore
        ## `POST /api/v2/internal/fraud_scores.json`
        ##
        ## ```bash
        ## curl https://{subdomain}.zendesk.com/api/v2/internal/fraud_scores.json \
        ##   -H "Content-Type: application/json" -X POST \
        ##   -d '{"fraud_score": {"account_id": 1234, "subdomain": "brand1"}}' \
        ##   -v -u {email_address}:{password}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 201 Created
        ## Location: https://{subdomain}.zendesk.com/api/v2/internal/fraud_scores/{id}.json
        ##
        ## {
        ##   "fraud_score": {
        ##     "id":          47,
        ##     "account_id":  "1",
        ##     "email":       "braind1@example.com",
        ##     "subdomain":   "brand1",
        ##     ...
        ##   }
        ## }
        ## ```
        ##
        allow_parameters :create, fraud_score: allowed_fraud_score_parameters
        def create
          if new_fraud_score!
            fraud_actions
            statsd_client.increment('fraud_score_create', tags: ["source:#{params[:fraud_score][:source_event]}"])
            render json: presenter.present(current_account.latest_fraud_score), status: :created
          end
        end

        ## ### Update FraudScore
        ## `PUT /api/v2/internal/fraud_scores/:id.json`
        ##
        ##
        ## ```bash
        ## curl https://{subdomain}.zendesk.com/api/v2/internal/fraud_score/:id.json \
        ##   -H "Content-Type: application/json" -X PUT \
        ##   -d '{"fraud_score": {"account_id": 1234, "subdomain": "brand1"}}' \
        ##   -v -u {email_address}:{password}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 Successful
        ## Location: https://{subdomain}.zendesk.com/api/v2/internal/fraud_score/{id}.json
        ##
        ## {
        ##   "fraud_score": {
        ##     "id":         47,
        ##     "account_id": "https://company.zendesk.com/api/v2/brands/47.json",
        ##     "email":      "Brand 1",
        ##     "subdomain":  "brand1",
        ##     ...
        ##   }
        ## }
        ## ```
        ##
        allow_parameters :update, fraud_score: allowed_fraud_score_parameters, id: Parameters.bigid
        def update
          if fraud_score.update_attributes(params[:fraud_score])
            render json: presenter.present(fraud_score)
          else
            render json: presenter.present_errors(fraud_score), status: :unprocessable_entity
          end
        end

        allow_parameters :vendor_review_prohibited, {}
        def vendor_review_prohibited
          render json: {vendor_review_prohibited: current_account.vendor_review_prohibited?}
        end

        private

        def fraud_actions
          perform_fraud_actions(params[:fraud_score][:actions], params[:fraud_score][:source_event])
        end

        def fraud_score
          @fraud_score ||= current_account.fraud_scores.find(params[:id])
        end

        def new_fraud_score!
          current_account.fraud_scores.create(build_fraud_score)
        end

        def build_fraud_score
          fraud_score_params = params[:fraud_score]
          {
            score:                         fraud_score_params[:score],
            score_params:                  fraud_score_params[:score_params],
            service_response:              deep_symbolized_hash(fraud_score_params),
            account_fraud_service_release: fraud_score_params[:account_fraud_service_release],
            verified_fraud:                fraud_score_params.fetch(:verified_fraud, false),
            is_whitelisted:                fraud_score_params.fetch(:is_whitelisted, false),
            subdomain:                     current_account.subdomain,
            owner_email:                   current_account.owner.email,
            source_event:                  fraud_score_params[:source_event]
          }
        end

        def deep_symbolized_hash(body)
          body.deep_symbolize_keys rescue nil
        end

        def presenter
          @presenter ||= Api::V2::Internal::FraudScorePresenter.new(current_user, url_builder: self)
        end

        def statsd_client
          @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['fraud_score_controller'])
        end
      end
    end
  end
end
