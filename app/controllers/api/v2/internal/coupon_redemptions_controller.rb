module Api::V2::Internal
  class CouponRedemptionsController < Api::V2::Internal::BaseController
    around_action :switch_to_account_shard

    deprecate_action :index, with_feature: :deprecate_coupons_redemptions_controller

    ## ### List coupon redemptions/applications for account
    ##
    ## `GET /api/v2/internal/account/:account_id/coupon_redemptions.json
    ##
    ## Fetches the list of coupon redemptions for the account identified
    ## by :account_id - the list compiles redemptions for classic and
    ## zuora coupons applied to the account.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :index, account_id: Parameters.bigid
    def index
      render json: presenter.present(coupon_redemptions)
    end

    private

    def switch_to_account_shard
      ActiveRecord::Base.on_shard(account.shard_id) do
        yield
      end
    end

    def coupon_redemptions
      if account.zuora_managed?
        account.zuora_coupon_redemptions
      else
        account.subscription.coupon_applications
      end
    end

    def account
      @account ||= Account.find(params[:account_id])
    end

    def presenter
      @presenter ||= Api::V2::Internal::CouponRedemptionPresenter.new(current_user, url_builder: self)
    end
  end
end
