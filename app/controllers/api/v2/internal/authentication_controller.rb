class Api::V2::Internal::AuthenticationController < Api::V2::Internal::BaseController
  allow_only_subsystem_user :classic

  before_action :prevent_production_access

  allow_parameters :update,
    ip_ranges: Parameters.string,
    sso: {
      is_active: Parameters.boolean
    },
    google_login: Parameters.boolean,
    prefer_lotus: Parameters.boolean,
    public_forums: Parameters.boolean,
    help_center_state: Parameters.string,
    web_portal_state: Parameters.string,
    agent: Parameters.boolean

  # Bulk Configuration API
  # Allows setting of the following:
  # * SSO - all modes, all options
  # * Help Center
  # * Lotus Preference
  # * OpenId modes allowed: Google
  # * IP Limitations
  def update
    AccountSetting.transaction do
      update_ip_restrictions
      update_role_settings
      update_account_settings
      update_public_forums
    end

    head :ok
  end

  protected

  def prevent_production_access
    head :bad_request if Rails.env.production?
  end

  def update_ip_restrictions
    if params[:ip_ranges].present?
      current_account.texts.ip_restriction = params[:ip_ranges]
      current_account.texts.save!

      current_account.settings.ip_restriction_enabled = true
    else
      current_account.settings.ip_restriction_enabled = false
    end
  end

  def update_role_settings
    # By default, all combinations are off
    RoleSettings::ROLES.each do |role|
      RoleSettings::SERVICES.each do |service|
        combined = "#{role}_#{service}_login="
        current_account.role_settings.send(combined, false)
      end
    end

    # Google login and SSO don't mix, so set
    # one or the other or zendesk login
    if params[:google_login]
      update_google_login
    elsif (sso = current_account.remote_authentications.jwt.first)
      update_sso(sso)
    else
      current_account.role_settings.agent_zendesk_login = true
      current_account.role_settings.end_user_zendesk_login = true
    end

    current_account.role_settings.save!
  end

  def update_sso(sso)
    sso.update_attributes!(params[:sso])

    if sso.is_active?
      if params[:agent]
        current_account.role_settings.agent_remote_login = true
        current_account.role_settings.end_user_zendesk_login = true
      else
        current_account.role_settings.end_user_remote_login = true
        current_account.role_settings.agent_zendesk_login = true
      end
    end
  end

  def update_google_login
    if params[:agent]
      current_account.role_settings.agent_google_login = true
      current_account.role_settings.end_user_zendesk_login = true
    else
      current_account.role_settings.end_user_google_login = true
      current_account.role_settings.agent_zendesk_login = true
    end
  end

  def update_account_settings
    %w[
      prefer_lotus
      help_center_state
      web_portal_state
    ].each do |key|
      current_account.settings.send("#{key}=", params[key]) if params.key?(key)
    end

    current_account.settings.save!
  end

  def update_public_forums
    # Default to no public forums
    current_account.forums.update_all(visibility_restriction_id: VisibilityRestriction::AGENTS_ONLY.id)

    if params[:public_forums]
      forum = current_account.forums.first
      forum.visibility_restriction_id = VisibilityRestriction::EVERYBODY.id
      forum.save!
    end
  end
end
