module Api
  module V2
    module Internal
      class CertificateMovesController < Api::V2::Internal::BaseController
        skip_before_action :prevent_missing_account_access
        allow_only_subsystem_user :certificates

        # This is internal, let's be transparent with our consumers
        rescue_from StandardError do |exception|
          render status: :internal_server_error, json: {
            error: exception.class.to_s,
            description: exception.message,
            backtrace: exception.backtrace
          }
        end

        # api_controller's handler does not include custom messages
        rescue_from ActiveRecord::RecordNotFound do |exception|
          render status: :not_found, json: {
            error: "RecordNotFound",
            description: exception.message
          }
        end

        rescue_from CertificateIp::NoCertificateIpsAvailableInPod do |exception|
          render status: :service_unavailable, json: {
            error: exception.class.to_s,
            description: exception.message
          }
        end

        ## ### Prepare new pod
        ##
        ## `PUT /api/v2/internal/certificate_moves/:account_id/prepare`
        ##
        ## Allocate an IP for SSL on the new target pod in advance of the
        ## swap operation.
        ##
        ## Returns the newly acquired IP on target pod
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :prepare,
          account_id:      Parameters.integer,
          target_shard_id: Parameters.integer
        require_oauth_scopes :prepare, scopes: [:write], arturo: true
        def prepare
          render json: presenter.present(mover.prepare(pod_id: target_pod_id))
        end

        ## ### Prepare new pod
        ##
        ## `POST /api/v2/internal/certificate_moves/swap`
        ##
        ## Promote IP in target_shard_id to the primary, and schedule the previous
        ## source_shard_id IP address to be automatically released.
        ##
        ## Returns the new primary IP on the target pod
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :swap,
          account_id: Parameters.integer,
          source_shard_id: Parameters.integer,
          target_shard_id: Parameters.integer
        require_oauth_scopes :swap, scopes: [:write], arturo: true
        def swap
          render json: presenter.present(mover.swap(source_pod_id, target_pod_id))
        end

        ## ### Rollback either a `prepare` or `swap` operation
        ##
        ## `POST /api/v2/internal/certificate_moves/rollback`
        ##
        ## Attempt to restore the IP previously used in the source pod. If IP
        ## was already released and re-used, provide with any free
        ## certificate_ip on the source_shard_id.
        ##
        ## Returns the new primary IP on the source pod
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :rollback,
          account_id: Parameters.integer,
          source_shard_id: Parameters.integer,
          target_shard_id: Parameters.integer
        require_oauth_scopes :rollback, scopes: [:write], arturo: true
        def rollback
          render json: presenter.present(mover.rollback(source_pod_id, target_pod_id))
        end

        private

        def mover
          Zendesk::Certificate::Mover.new(certificate)
        end

        def source_pod_id
          Zendesk::Accounts::DataCenter.pod_id_for_shard(params[:source_shard_id])
        end

        def target_pod_id
          Zendesk::Accounts::DataCenter.pod_id_for_shard(params[:target_shard_id])
        end

        def certificate
          @certificate ||= Certificate.on_master.active.find_by_account_id(params[:account_id]) ||
              raise(ActiveRecord::RecordNotFound, "Account '#{params[:account_id]}' does not have any certificates.")
        end

        def presenter
          @presenter ||= Api::V2::Internal::CertificateIpPresenter.new(current_user, url_builder: self)
        end
      end
    end
  end
end
