module Api
  module V2
    module Internal
      class ZendeskProvisionedSslController < Api::V2::Internal::BaseController
        allow_only_subsystem_user :certificates

        # This is internal, let's be transparent with our consumers
        rescue_from StandardError do |exception|
          render status: :internal_server_error, json: {
            error: exception.class.to_s,
            description: exception.message,
            backtrace: exception.backtrace
          }
        end

        # api_controller's handler does not include custom messages
        rescue_from ActiveRecord::RecordNotFound do |exception|
          render status: :not_found, json: {
            error: "RecordNotFound",
            description: exception.message
          }
        end

        ## ### Get recent AcmeCertificateJobStatuses
        ##
        ## `GET /api/v2/internal/zendesk_provisioned_ssl/jobs`
        ##
        ## Returns the last 10 AcmeCertificateJobStatuses for an account
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :jobs, {}
        def jobs
          records = current_account.acme_certificate_job_statuses.order("created_at desc").limit(10)
          render json: job_presenter.present(records)
        end

        ## ### Enqueue an AcmeCertificateJob for an account
        ##
        ## `POST /api/v2/internal/zendesk_provisioned_ssl/enqueue`
        ##
        ## Enqueue an AcmeCertificateJob for an account. This will enable Zendesk Provisioned SSL on an account if it's not currently enabled.
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :enqueue, {}
        def enqueue
          if current_account.acme_certificate_job_statuses.where(completed_at: nil).any?
            render status: :unprocessable_entity, json: { error: 'A job is already enqueued for this account. Please wait until that job completes before enqueuing another job' }
          else
            AcmeCertificateJobStatus.enqueue_with_status!(current_account.id)
            head :ok
          end
        end

        ## ### Reset rate limits for an account
        ##
        ## `DELETE /api/v2/internal/zendesk_provisioned_ssl/rate_limits`
        ##
        ## Reset the Zendesk Provisioned SSL rate limits for an account.
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :rate_limits, {}
        def rate_limits
          AcmeCertificateJob.reset_rate_limit!(account_id: current_account.id)
          head :ok
        end

        ## ### Kill uncompleted jobs
        ##
        ## `DELETE /api/v2/internal/zendesk_provisioned_ssl/jobs`
        ##
        ## This will mark all uncompleted ACME jobs as failed.
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :kill_jobs, {}
        def kill_jobs
          current_account.acme_certificate_job_statuses.uncompleted.find_each do |job|
            job.failed!("Killed")
          end

          head :ok
        end

        ## ### Reset Zendesk Provisioned SSL for an account
        ##
        ## `POST /api/v2/internal/zendesk_provisioned_ssl/reset`
        ##
        ## Resets various state for Zendesk Provisioned SSL on an account.
        ##
        ## #### Allowed For
        ##
        ##  * 'certificates' Subsystem User
        ##
        allow_parameters :reset, {}
        def reset
          AcmeCertificateJob.reset_rate_limit!(account_id: current_account.id)

          unless current_account.certificates.active.first.try(:autoprovisioned?)
            current_account.acme_authorizations.each(&:destroy)
            current_account.acme_registrations.each(&:soft_delete!)
          end

          head :ok
        end

        private

        def job_presenter
          @job_presenter ||= Api::V2::Internal::AcmeCertificateJobStatusPresenter.new(current_user, url_builder: self)
        end
      end
    end
  end
end
