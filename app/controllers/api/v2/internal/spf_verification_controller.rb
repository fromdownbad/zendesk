module Api::V2::Internal
  class SpfVerificationController < Api::V2::Internal::BaseController
    before_action :validate_account
    before_action :validate_email_suffix

    VERIFIED_SPF_STATUS = RecipientAddress::SPF_STATUS.fetch(:verified)

    allow_parameters :index,
      id: Parameters.bigid,
      email_suffix: Parameters.string
    def index
      render json: presenter.present(recipient_addresses)
    end

    allow_parameters :update,
      id: Parameters.bigid,
      email_suffix: Parameters.string
    def update
      unverified_recipient_addresses_ids = unverified_recipient_addresses.map(&:id)
      unverified_recipient_addresses.update_all(spf_status_id: VERIFIED_SPF_STATUS)
      json = presenter.present(recipient_addresses)
      json[:recipient_addresses].each do |recipient_address|
        recipient_address[:changed] = unverified_recipient_addresses_ids.include?(recipient_address[:id])
      end

      render json: json
    end

    private

    def validate_account
      head(:not_found) unless account.present?
    end

    def validate_email_suffix
      head(:bad_request) unless params[:email_suffix].present?
    end

    def presenter
      @presenter ||= Api::V2::RecipientAddressPresenter.new(current_user, url_builder: self, includes: includes)
    end

    def unverified_recipient_addresses
      @unverified_recipient_addresses ||= recipient_addresses.where.not(spf_status_id: VERIFIED_SPF_STATUS)
    end

    def recipient_addresses
      @recipient_addresses ||= account.recipient_addresses.not_collaboration.where("email like ?", "%#{params[:email_suffix]}")
    end

    def account
      @account ||= Account.find(params[:id])
    end
  end
end
