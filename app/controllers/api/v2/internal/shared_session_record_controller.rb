module Api::V2::Internal
  class SharedSessionRecordController < Api::V2::Internal::BaseController
    allow_subsystem_user :passport

    allow_parameters :create,
      user_id: Parameters.bigid,
      auth_via: Parameters.string,
      auth_duration: Parameters.integer,
      invalidate_id: Parameters.boolean,
      remember_me_requested: Parameters.boolean,
      session_id: Parameters.string,
      user_role: Parameters.integer,
      csrf_token: Parameters.string
    def create
      shared_session_record.save!
      render json: presenter.present(shared_session_record), status: :created
    rescue => e # rubocop:disable Style/RescueStandardError
      Rails.logger.error(
        "internal_api_shared_session_record_creations error: #{e.message} - #{e.backtrace}."
      )
      render json: { errors: e.message }, status: :unprocessable_entity
    end

    private

    def shared_session_record
      @shared_session_record ||= Zendesk::SharedSession::SessionRecord.new.tap do |shared_session_record|
        shared_session_record.account_id = current_account.id
        shared_session_record.user_id = params[:user_id]
        shared_session_record.session_id = params[:session_id]
        shared_session_record.data[:account] = current_account.id
        shared_session_record.data[:id] = params[:session_id]
        shared_session_record.data[:'warden.user.default.key'] = params[:user_id]
        shared_session_record.data[:auth_via] = params[:auth_via]
        shared_session_record.data[:auth_duration] = params[:auth_duration].to_i # passport will calculate something like this - https://github.com/zendesk/zendesk/blob/master/lib/zendesk/authenticated_session.rb#L48
        shared_session_record.data[:auth_updated_at] = Time.now.to_i
        shared_session_record.data[:auth_stored] = true # hardcode to true, but investigate if other cases possible
        shared_session_record.data[:auth_authenticated_at] = Time.now.to_i
        shared_session_record.data[:user_role] = params[:user_role]
        shared_session_record.data[:csrf_token] = params[:csrf_token]
        shared_session_record.expire_at = Time.at(shared_session_record.data[:auth_updated_at] + shared_session_record.data[:auth_duration])
      end
    end

    def presenter
      @presenter ||= Api::V2::Internal::SharedSessionRecordPresenter.new(current_user, url_builder: self)
    end
  end
end
