module Api::V2::Internal
  module Billing::Subscription
    class PreviewsController < Api::V2::Internal::BaseController
      allow_subsystem_user :billing

      allow_parameters :show, :skip
      def show
        quote = current_account.subscription.preview!(preview_options)
        render json: presenter.present(quote)
      end

      private

      def preview_options
        options = (params[:subscription] || {}).with_indifferent_access
        ZendeskBillingCore::Zuora::SubscriptionOptions.new(current_account.id, options)
      end

      def presenter
        @presenter ||= Api::V2::Account::SubscriptionPresenter.new(
          current_user, url_builder: self, includes: includes
        )
      end
    end
  end
end
