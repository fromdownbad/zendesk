module Api::V2::Internal
  module Billing::Subscription
    class FeaturesController < Api::V2::Internal::BaseController
      include Api::V2::Internal::SubscriptionFeaturesHelper
      allow_subsystem_user :billing

      before_action :validate!, only: :update

      ## ### Sets the features on an account when zuora synchronizes
      ## `PUT /api/v2/internal/billing/subscription/features.json`
      ##
      ## #### Allowed for
      ##
      ## * Subsystem User
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl -X PUT  https://{subdomain}.zendesk.com/api/v2/internal/billing/subscription/features.json \
      ## -d '{"zuora_data":
      ##       {
      ##         "pricing_model": 5
      ##         "plan_type": 1,
      ##         "addons": {"light_agents" => "rate_plan_id"}
      ##       }
      ##      }' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## ```
      ##
      allow_parameters :update, zuora_data: {
        pricing_model: Parameters.integer,
        plan_type: Parameters.integer,
        addons: Parameters.anything
      }
      def update
        pricing_model, plan_type, addons = *(params[:zuora_data] || {}).values_at(:pricing_model, :plan_type, :addons)
        subscription_feature_service(pricing_model, plan_type, addons).execute!
        head :ok
      end

      private

      def subscription_feature_service(zuora_pricing_model, zuora_plan_type, zuora_addons)
        @subscription_feature_service ||= Zendesk::Features::SubscriptionFeatureService.new(
          current_account, zuora_pricing_model, zuora_plan_type, zuora_addons
        )
      end

      def validate!
        if params.dig(:zuora_data, :addons).present? && !valid_addon_names?
          message = "addon #{params[:addon_name]} is not available for account #{current_account.subdomain}/#{current_account.id}"
          render json: { error: message }, status: :unprocessable_entity
        end
      end

      def valid_addon_names?
        (params.dig(:zuora_data, :addons) || {}).keys.all? do |addon_name|
          addon_name.present? && catalog_addons.any? { |a| a.to_s == addon_name }
        end
      end
    end
  end
end
