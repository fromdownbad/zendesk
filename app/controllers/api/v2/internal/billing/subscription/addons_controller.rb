module Api::V2::Internal
  module Billing::Subscription
    class AddonsController < Api::V2::Internal::BaseController
      include Api::V2::Internal::SubscriptionFeaturesHelper
      allow_subsystem_user :billing

      before_action :validate!, only: :update

      ## ### Sets the addons on an account when zuora synchronizes
      ##
      ## Note: this does not set the feature bits or enable functionality (see ./features_controller.rb)
      ##
      ## `PUT /api/v2/internal/billing/subscription/addons.json`
      ##
      ## #### Allowed for
      ##
      ## * Subsystem User
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl -X PUT  https://{subdomain}.zendesk.com/api/v2/internal/billing/subscription/addons.json \
      ## -d '{"addons": {"light_agents" => "rate_plan_id"}}' \
      ## -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## ```
      ##
      allow_parameters :update, addons: Parameters.anything
      def update
        addon_service.execute!
        head :ok
      end

      private

      def addon_service
        @addon_service ||= Zendesk::Features::AddonService.new(current_account, @addons)
      end

      def validate!
        @addons = params[:addons] || {}
        unless valid_addon_names?
          message = "addon(s) #{@addons.keys} not available for account #{current_account.subdomain}/#{current_account.id}"
          render json: { error: message }, status: :unprocessable_entity
        end
      end

      def valid_addon_names?
        @addons.keys.all? do |addon_name|
          addon_name.present? && catalog_addons.any? { |a| a.to_s == addon_name }
        end
      end
    end
  end
end
