################################################################################
# This API is deprecated, and will be removed in the future
# If you have concerns, please email narwhals@zendesk.com
################################################################################
module Api::V2::Internal
  module Billing
    class AccountsController < Api::V2::Internal::BaseController
      allow_subsystem_user :billing

      allow_parameters :update, account: {
        is_serviceable: Parameters.boolean,
        is_active:      Parameters.boolean,
        subscription:   {
          plan_type:              Parameters.integer,
          pricing_model_revision: Parameters.integer,
          max_agents:             Parameters.integer,
          billing_cycle_type:     Parameters.integer,
          payment_method_type:    Parameters.integer,
          is_trial:               Parameters.boolean,
          audit_message:          Parameters.string
        }
      }
      def update
        current_account.subscription.attributes = params[:account][:subscription]
        current_account.is_serviceable = params[:account][:is_serviceable]
        current_account.is_active = params[:account][:is_active]
        Zendesk::SupportAccounts::Product.retrieve(current_account).save!
        head :ok
      end
    end
  end
end
