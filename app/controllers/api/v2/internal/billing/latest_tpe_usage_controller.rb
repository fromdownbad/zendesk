module Api::V2::Internal
  module Billing
    class LatestTpeUsageController < Api::V2::Internal::BaseController
      allow_subsystem_user :billing

      ## ### Descriptions
      ## Returns details about the latest TPE usage for an account.
      ##
      ## #### Request Format
      ##  * account_id: The id of the account for which to the letest TPE usage (REQUIRED)
      ##
      ## #### Allowed For
      ##  * Billing subsystem user
      ##
      ## #### Example Response
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "usage": {
      ##     "created_at": "Sun, 01 Apr 2018 12:00:00 UTC +00:00"
      ##   }
      ## }
      ## ```
      allow_parameters :show, account_id: Parameters.bigid
      def show
        render json: presenter.present(latest_tpe_usage)
      end

      private

      def account
        @account ||= Account.find(params[:account_id])
      end

      def latest_tpe_usage
        @latest_tpe_usage ||= account.voice_cti_usages.latest.
          select(:created_at).first
      end

      def presenter
        @presenter ||= Api::V2::Internal::Billing::LatestTpeUsagePresenter.new(
          current_user,
          url_builder: self
        )
      end
    end
  end
end
