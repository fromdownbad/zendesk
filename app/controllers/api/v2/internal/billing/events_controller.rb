module Api::V2::Internal
  module Billing
    class EventsController < Api::V2::Internal::BaseController
      allow_subsystem_user :billing

      allow_parameters :create, event: {
        topic:   Parameters.string,
        payload: Parameters.anything
      }
      def create
        enqueue_account_synchronizer
        head :accepted
      end

      private

      def enqueue_account_synchronizer
        AccountSynchronizerJob.enqueue(payload)
      end

      def create_params
        params[:event].deep_symbolize_keys
      end

      def payload
        create_params[:payload]
      end
    end
  end
end
