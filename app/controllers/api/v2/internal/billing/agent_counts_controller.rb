module Api::V2::Internal
  module Billing
    class AgentCountsController < Api::V2::Internal::BaseController
      allow_subsystem_user :billing

      ## ### Descriptions
      ## Returns agent counts for an account.
      ##
      ## #### Request Format
      ##  * account_id: The id of the account for which to show agent counts (REQUIRED)
      ##
      ## #### Allowed For
      ##  * Billing subsystem user
      ##
      ## #### Example Response
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "agent_counts": {
      ##     "suite_agents": 4
      ##   }
      ## }
      ## ```
      allow_parameters :show, account_id: Parameters.bigid
      def show
        render json: presenter.present(account)
      end

      private

      def account
        @account ||= Account.find(params[:account_id])
      end

      def presenter
        @presenter ||= Api::V2::Internal::Billing::AgentCountPresenter.new(
          current_user,
          url_builder: self
        )
      end
    end
  end
end
