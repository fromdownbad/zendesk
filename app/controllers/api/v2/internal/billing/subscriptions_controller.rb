module Api::V2::Internal
  module Billing
    class SubscriptionsController < Api::V2::Internal::BaseController
      allow_subsystem_user :billing

      allow_parameters :show, :skip
      def show
        render json: presenter.present(current_account.subscription)
      end

      allow_parameters :destroy, :skip
      def destroy
        default_delete_response && return if current_account.cancel!

        errors = current_account.errors.map(&:to_s)
        render status: :unprocessable_entity, json: { errors: errors }
      end

      private

      def presenter
        @presenter ||= Api::V2::Account::SubscriptionPresenter.new(
          current_user, url_builder: self, includes: includes
        )
      end
    end
  end
end
