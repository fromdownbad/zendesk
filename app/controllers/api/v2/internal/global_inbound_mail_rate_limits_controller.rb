module Api::V2::Internal
  class GlobalInboundMailRateLimitsController < Api::V2::Internal::BaseController
    allow_parameters :index, {}
    def index
      render json: {
        recipient_exceptions: collection_presenter.present(recipient_exceptions)[presenter_key],
        sender_exceptions: collection_presenter.present(sender_exceptions)[presenter_key]
      }
    end

    private

    def presenter
      @presenter ||= Api::V2::Internal::GlobalInboundMailRateLimitPresenter.new(current_user, url_builder: self)
    end

    def collection_presenter
      @collection_presenter ||= Api::V2::CollectionPresenter.new(current_user, url_builder: self, item_presenter: presenter)
    end

    def presenter_key
      collection_presenter.model_key
    end

    def recipient_exceptions
      @recipient_exceptions ||= rate_limit_exceptions_map(Zendesk::InboundMail::RateLimit::RECIPIENT_EXCEPTIONS)
    end

    def sender_exceptions
      @sender_exceptions ||= rate_limit_exceptions_map(Zendesk::InboundMail::RateLimit::SENDER_EXCEPTIONS)
    end

    def rate_limit_exceptions_map(exceptions_map)
      exceptions_map.map do |email, rate_limit|
        OpenStruct.new(email: email, rate_limit: rate_limit)
      end
    end
  end
end
