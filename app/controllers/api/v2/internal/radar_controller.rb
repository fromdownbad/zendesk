require 'zendesk/radar_factory'

module Api
  module V2
    module Internal
      class RadarController < Api::V2::Internal::BaseController
        # Accept requests to https://pod-X.zendesk.com
        skip_before_action :prevent_missing_account_access, only: :reconfigure

        # api_controller's handler does not include custom messages
        rescue_from(
          ActiveRecord::RecordNotFound,
          Zendesk::Radar::Configuration::InvalidShardError,
          Zendesk::Radar::Configuration::InvalidSubdomainError
        ) do |exception|
          render status: :bad_request, json: {
            error: exception.class.to_s,
            description: exception.message
          }
        end

        ## ### Reconfigure radar
        ##
        ## `POST /api/v2/internal/radar/reconfigure`
        ##
        ## Send a radar:/reconfigure message to the account so that the radar
        ## server instructs clients of the account to disconnect and reconnect,
        ## thereby picking up an independently updated config.
        ##
        allow_parameters :reconfigure,
          shard_id:           Parameters.integer,
          account_subdomain:  Parameters.string
        def reconfigure
          render json: { 'status' => 'ok', 'reconfigure_return' => publish_reconfigure }

        # Exodus will retry with a 503 return value; not with a 500
        rescue Redis::BaseConnectionError => e
          render status: 503, json: { 'class' => e.class, 'message' => e.message }
        end

        allow_parameters :suspend,
          audit: Parameters.anything
        ## ### Suspend radar
        ##
        ## `POST /api/v2/internal/radar/suspend`
        ##
        ## Send a radar:/reconfigure a kill_switch message to the account so that the radar
        ## server instructs clients of the account to disconnect and try to do a delayed reconnect.
        def suspend
          current_account.suspend_radar(audit_params)
          render json: { 'enabled' => false }
        rescue Redis::BaseConnectionError => e
          render status: 503, json: { 'class' => e.class, 'message' => e.message }
        end

        allow_parameters :unsuspend,
          audit: Parameters.anything
        ## ### UnSuspend radar
        ##
        ## `POST /api/v2/internal/radar/unsuspend`
        ##
        ## Sets the radar_enabled setting to true
        def unsuspend
          current_account.unsuspend_radar(audit_params)
          render json: { 'enabled' => true }
        end

        private

        def audit_params
          (params[:audit] || {}).slice(:actor_id, :actor_type, :ip_address,
            :message, :via_id, :via_reference_id)
        end

        def publish_reconfigure
          reconfigure_radar_client.status('reconfigure').set('reconfigure', Time.now.to_i.to_s)
        end

        def reconfigure_radar_client
          subdomain = params[:account_subdomain]
          src_shard_id = params[:shard_id]
          ::Account.where(subdomain: subdomain).first || raise(ActiveRecord::RecordNotFound, "Couldn't find Account with subdomain = #{subdomain}")
          ::RadarFactory.create_exodus_radar_client(subdomain, src_shard_id)
        end
      end
    end
  end
end
