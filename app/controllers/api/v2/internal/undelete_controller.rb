module Api
  module V2
    module Internal
      class UndeleteController < Api::V2::Internal::BaseController
        before_action :ensure_type
        before_action :ensure_deleted_at, only: :index

        allow_parameters :index, :skip
        def index
          models = type.with_deleted do
            conditions = {account_id: current_account.id}.merge(deleted_condition)
            sort_order = params[:sort_order] == "desc" ? "DESC" : "ASC"
            type.paginate(
              per_page: params[:all_tickets] ? 500 : 50, # fetches all for MOST accounts
              page: (params[:page] || 1).to_i
            ).where(conditions).order("updated_at #{sort_order}").to_a
          end

          render json: presenter.present(models)
        end

        allow_parameters :update, :skip
        def update
          with_all_deleted do
            model = begin
              if type == Ticket
                type.where(account_id: current_account.id, nice_id: params[:id]).first
              else
                type.where(account_id: current_account.id, id: params[:id]).first
              end
            end

            model ? model.soft_undelete! : raise(ActiveRecord::RecordNotFound)
          end
          render json: {}, status: :ok
        end

        private

        def ensure_type
          @type =
            case params[:type]
            when "forum" then Forum
            when "entry" then Entry
            when "post" then Post
            when "ticket" then Ticket
            when "organization" then Organization
            when "group" then Group
            else
              render status: :bad_request, text: "unknown type"
            end
        end

        def presenter
          Api::V2::Internal::UndeletePresenter.new(current_user, url_builder: self)
        end

        def ensure_deleted_at
          if params[:deleted_at]
            params[:deleted_at] = params[:deleted_at].first..params[:deleted_at].last
          else
            render status: :bad_request, text: "missing deleted_at"
          end
        end

        def deleted_condition
          if type == Ticket
            {status_id: StatusType.DELETED, updated_at: params[:deleted_at] }
          elsif type == Group
            {is_active: false, updated_at: params[:deleted_at] }
          else
            {deleted_at: params[:deleted_at]}
          end
        end

        def with_all_deleted(&block)
          if [Ticket, Organization, Group].include?(type)
            type.with_deleted(&block)
          else
            Entry.with_deleted { Post.with_deleted { Forum.with_deleted(&block) } }
          end
        end

        attr_reader :type
      end
    end
  end
end
