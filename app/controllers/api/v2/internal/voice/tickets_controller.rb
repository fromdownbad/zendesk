module Api::V2::Internal::Voice
  class TicketsController < Api::V2::Internal::BaseController
    include Zendesk::Tickets::ControllerSupport

    allow_parameters :create, ticket: Parameters.anything
    def create
      notification_trigger_keys = [
        "txt.default.triggers.notify_requester_received.title",
        "txt.default.triggers.notify_all_received.title",
        "txt.default.triggers.notify_assignee_assignment.title",
        "txt.default.triggers.notify_group_assignment.title"
      ]

      ticket = Ticket.with_triggers_disabled(current_account, notification_trigger_keys) do
        initialized_ticket.save!
        initialized_ticket
      end

      render json: presenter.present(ticket), status: :created, location: presenter.url(ticket)
    end

    private

    def presenter
      @presenter ||= begin
        klass = Api::V2::Tickets::TicketPresenter
        klass.new(current_user, url_builder: self, includes: includes, comment_presenter: comment_presenter)
      end
    end

    def comment_presenter
      @comment_presenter ||= begin
        Api::V2::Tickets::GenericCommentPresenter.new(current_user, url_builder: self)
      end
    end
  end
end
