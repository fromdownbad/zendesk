module Api::V2::Internal::Voice
  class SubscriptionsController < Api::V2::Internal::BaseController
    allow_parameters :show, {}
    def show
      # We don't return 404 Not Found when the account doesn't have a subscription,
      # but we still want to cache a response, hence 'cache key stub'
      cache_key = voice_subscription.try(:cache_key) || 'cache key stub'

      if stale?(etag: cache_key)
        render json: subscription_presenter.present(voice_subscription)
      end
    end

    private

    def voice_subscription
      @voice_subscription ||= current_account.voice_subscription
    end

    def subscription_presenter
      @subscription_presenter ||= Api::V2::Account::VoiceSubscriptionPresenter.new(current_user, url_builder: self)
    end
  end
end
