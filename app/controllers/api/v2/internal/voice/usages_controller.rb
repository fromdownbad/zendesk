module Api::V2::Internal::Voice
  class UsagesController < Api::V2::Internal::BaseController
    allow_parameters :create, usage: {
      units: Parameters.integer,
      usage_type: Parameters.string,
      zuora_reference_id: Parameters.string,
      voice_reference_id: Parameters.bigid | Parameters.string # Undocumented, untested
    }

    def create
      if serialize_voice_usage?
        params[:usage][:account_id] = current_account.id
        ZBC::Voice::UnprocessedUsage.create!(params[:usage])
      else
        current_account.zuora_voice_usages.create!(params[:usage])
      end
      render json: { status: "ok" }
    end

    private

    def serialize_voice_usage?
      @serialize_voice_usage ||= Arturo.feature_enabled_for?(:serialize_voice_usage, current_account)
    end
  end
end
