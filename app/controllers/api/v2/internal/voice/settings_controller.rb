module Api
  module V2
    module Internal
      module Voice
        class SettingsController < Api::V2::Internal::BaseController
          include ::Voice::BillingHelper

          before_action :only_allow_voice_settings, only: :cleanup_legacy_settings

          allow_parameters :update, settings: Parameters.map(
            voice:                                    Parameters.boolean,
            voice_outbound_enabled:                   Parameters.boolean,
            voice_maximum_queue_size:                 Parameters.integer & Parameters.gte(0) & Parameters.lte(100),
            voice_maximum_queue_wait_time:            Parameters.integer & Parameters.gte(1) & Parameters.lte(60),
            voice_recordings_public:                  Parameters.boolean,
            voice_only_during_business_hours:         Parameters.boolean,
            voice_agent_wrap_up_after_calls:          Parameters.boolean,
            voice_agent_confirmation_when_forwarding: Parameters.boolean
          )
          def update
            # This interface is extracted from internal to public
            # if voice_settings_audit_display_right_user feature is enabled.
            head(:not_found)
          end

          allow_parameters :cleanup_legacy_settings, settings: Parameters.array(Parameters.string) | Parameters.string
          def cleanup_legacy_settings
            current_account.settings.where(name: params.require(:settings)).delete_all
            head :ok
          end

          allow_parameters :billing_periods, {}
          def billing_periods
            @periods = if current_account.subscription.invoicing? || current_account.zuora_managed?
              periods_with_invoice
            else
              periods_with_payments
            end

            render json: @periods
          end

          private

          def only_allow_voice_settings
            settings = params.require(:settings)
            settings = [settings] if settings.is_a?(String)
            head(:unprocessable_entity) if settings.any? { |s| s !~ /^voice_/ }
          end
        end
      end
    end
  end
end
