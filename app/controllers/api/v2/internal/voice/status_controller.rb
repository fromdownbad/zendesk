module Api::V2::Internal::Voice
  class StatusController < Api::V2::Internal::BaseController
    allow_parameters :update, enabled: Parameters.boolean
    def update
      settings = current_account.settings
      settings.voice = params[:enabled]
      if settings.save
        head :ok
      else
        head :unprocessable_entity
      end
    end
  end
end
