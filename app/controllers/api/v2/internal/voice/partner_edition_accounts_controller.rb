module Api::V2::Internal::Voice
  class PartnerEditionAccountsController < Api::V2::Internal::BaseController
    ## ### Show the account's Talk Partner Edition account record.
    ##
    ## `GET /api/v2/internal/voice/partner_edition_account.json`
    ##
    ## This will show the account's Talk Partner Edition account record.
    ##
    ## #### Allowed for
    ##
    ##  * allow_only_subsystem_user :zendesk
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "voice_partner_edition_account": {
    ##     "plan_type":  1,
    ##     "active":     false,
    ##     "has_addon":  false,
    ##     "trial":      false,
    ##     "trial_expires_at":  "2017-01-03 12:00:00"
    ##   }
    ## }
    ## ```
    allow_parameters :show, {}
    def show
      render json: voice_partner_edition_account_presenter.present(voice_partner_edition_account)
    end

    ## ### Update the account's Talk Partner Edition account record.
    ##
    ## `PUT /api/v2/internal/voice/partner_edition_account.json`
    ##
    ## This will update the account's Talk Partner Edition account record.
    ##
    ## #### Allowed for
    ##
    ##  * allow_only_subsystem_user :zendesk
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "voice_partner_edition_account": {
    ##     "plan_type":  1,
    ##     "active":     false,
    ##     "has_addon":  false,
    ##     "trial":      false,
    ##     "trial_expires_at":  "2017-01-03 12:00:00"
    ##   }
    ## }
    ## ```
    allow_parameters :update, voice_partner_edition_account: {
      plan_type: Parameters.integer,
      active: Parameters.boolean,
      trial_expires_at: Parameters.string
    }
    def update
      if voice_partner_edition_account.create_and_delete(params[:voice_partner_edition_account])
        render json: voice_partner_edition_account_presenter.present(current_account.voice_partner_edition_account)
      else
        render json: voice_partner_edition_account_presenter.present_errors(voice_partner_edition_account), status: :unprocessable_entity
      end
    end

    private

    def voice_partner_edition_account
      @voice_partner_edition_account ||= Voice::PartnerEditionAccount.find_by_account_id!(current_account.id)
    end

    def voice_partner_edition_account_presenter
      @voice_partner_edition_account_presenter ||= Api::V2::Account::VoicePartnerEditionAccountPresenter.new(current_account, url_builder: self)
    end
  end
end
