module Api::V2::Internal::Voice
  class RechargeSettingsController < Api::V2::Internal::BaseController
    include Zendesk::Voice::RechargeSupport
    include ZBC::Voice::RechargeSupport

    allow_subsystem_user :billing

    before_action :require_no_recharge_settings, only: [:create]
    before_action :require_recharge_settings, only: [:update, :recharge]

    # Customer can't create or update recharge settings on trial
    before_action :require_zuora_account

    ## ### Create the account's Voice Recharge Settings record.
    ##
    ## `POST /api/v2/internal/voice/recharge_settings.json`
    ##
    ## This will create the account's voice recharge settings record.
    ##
    ## #### Allowed for
    ##
    ##  * allow_only_subsystem_user :zendesk
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "recharge_settings": {
    ##     "enabled":         true,
    ##     "minimum_balance": 5.0,
    ##     "amount":          50.0,
    ##     "balance":         1000.00,
    ##     "payment_status":  "ok"
    ##   }
    ## }
    ## ```
    allow_parameters :create, recharge_settings: {
      enabled: Parameters.boolean,
      amount: Parameters.float | Parameters.integer
    }
    def create
      update_recharge_settings(create_params)
      setup_voice_recharge_placeholder_subscription
      render json: recharge_settings_presenter.present(recharge_settings), status: :created
    end

    ## ### Fetch the account's Voice Recharge Settings record.
    ##
    ## `GET /api/v2/internal/voice/recharge_settings.json`
    ##
    ## This will fetch the account's voice recharge settings record.
    ##
    ## #### Allowed for
    ##
    ##  * allow_only_subsystem_user :zendesk
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 201 CREATED
    ##
    ## {
    ##   "recharge_settings": {
    ##     "enabled":         true,
    ##     "minimum_balance": 5.0,
    ##     "amount":          50.0,
    ##     "balance":         1000.00,
    ##     "payment_status":  "ok"
    ##   }
    ## }
    ## ```
    allow_parameters :show, :skip
    def show
      render json: recharge_settings_presenter.present(recharge_settings)
    end

    ## ### Update the account's Voice Recharge Settings record.
    ##
    ## `PUT /api/v2/internal/voice/recharge_settings.json`
    ##
    ## This will update the account's voice recharge settings record.
    ##
    ## #### Allowed for
    ##
    ##  * allow_only_subsystem_user :zendesk
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "recharge_settings": {
    ##     "enabled":         true,
    ##     "minimum_balance": 5.0,
    ##     "amount":          50.0,
    ##     "balance":         1000.00,
    ##     "payment_status":  "ok"
    ##   }
    ## }
    ## ```

    allow_parameters :update, recharge_settings: {
      enabled: Parameters.boolean,
      amount: Parameters.float | Parameters.integer
    }
    def update
      if update_recharge_settings(params.require(:recharge_settings))
        render json: recharge_settings_presenter.present(recharge_settings)
      else
        render json: recharge_settings_presenter.present_errors(recharge_settings), status: :unprocessable_entity
      end
    end

    allow_parameters :recharge, :skip
    def recharge
      retry_recharge!
      head :created
    end

    private

    # create a voice recharge placeholder subscription
    def setup_voice_recharge_placeholder_subscription
      ZBC::Zuora::VoiceRecharge.subscribe!(current_account)
    end

    def recharge_settings_presenter
      @recharge_settings_presenter ||= Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter.new(nil, url_builder: self)
    end

    def account # HACK: this is needed to make ZBC::Voice::RechargeSupport work
      current_account
    end
  end
end
