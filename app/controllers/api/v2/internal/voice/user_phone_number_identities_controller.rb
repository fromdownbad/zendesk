module Api::V2::Internal::Voice
  class UserPhoneNumberIdentitiesController < Api::V2::Internal::BaseController
    before_action :ensure_presence_of_sms_capable, except: [:show]

    allow_parameters :update,
      id: Parameters.bigid,
      identity: {
        sms_capable: Parameters.boolean
      }
    def update
      identity = UserPhoneNumberIdentity.where(account_id: current_account.id, id: params[:id]).first
      if identity
        identity.update_sms_capability(capable: sms_capable)
        head :ok
      else
        head :not_found
      end
    end

    allow_parameters :show,
      user_id: Parameters.bigid | Parameters.nil_string,
      number: Parameters.string
    def show
      identity = current_account.user_identities.phone.find_by(user_id: params[:user_id], value: params[:number])

      if identity
        render json: identity_presenter.present(identity)
      else
        head :not_found
      end
    end

    private

    def ensure_presence_of_sms_capable
      head :bad_request unless [true, false].include?(sms_capable)
    end

    def sms_capable
      @sms_capable ||= params[:identity] && params[:identity][:sms_capable]
    end

    def identity_presenter
      @identity_presenter ||= Api::V2::IdentityPresenter.new(current_user, url_builder: self)
    end
  end
end
