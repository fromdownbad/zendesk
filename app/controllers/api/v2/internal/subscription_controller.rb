module Api
  module V2
    module Internal
      class SubscriptionController < Api::V2::Internal::BaseController
        ## ### Sets the canceled_on (churned_on) date on the subscription model for an account
        ## `PATCH /api/v2/internal/accounts/:id/subscription.json`
        ##
        ## #### Allowed for
        ##
        ## * Subsystem User
        ##
        ## #### Using curl
        ##
        ## ```bash
        ## curl -X PATCH  https://{subdomain}.zendesk.com/api/v2/internal/accounts/{id}/subscription.json \
        ## -d '{"subscription": {"canceled_on": "2014-01-01"}}' \
        ##   -v -u {email_address}:{password}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## ```
        ##
        allow_parameters :update, :skip
        def update
          if subscription.update_attributes(subscription_params)
            head :ok
          else
            render json: subscription.errors, status: :unprocessable_entity
          end
        end

        protected

        def subscription
          @subscription ||= ::Account.find(params[:id]).subscription
        end

        def subscription_params
          params.require(:subscription).permit(:canceled_on)
        end
      end
    end
  end
end
