module Api
  module V2
    module Internal
      module Monitor
        class BaseController < Api::V2::Internal::BaseController
          private

          rescue_from ActiveRecord::RecordNotFound do |rnf|
            render json: { message: rnf.message, backtrace: rnf.backtrace }, status: :not_found
          end

          rescue_from StandardError do |se|
            render json: { message: se.message, backtrace: se.backtrace }, status: :internal_server_error
          end

          def last_modified_cache_key(scopes, non_scopes = [])
            [
              scopes.map! { |s| s.maximum(:updated_at) },
              non_scopes
            ].flatten!.compact.max || Time.now
          end

          def fetch_with_cache(key, scopes, non_scopes = [])
            last_modified = last_modified_cache_key(scopes, non_scopes)

            if stale?(last_modified: last_modified)
              payload = Rails.cache.fetch("/api/v2/internal/monitor/#{key}/#{current_account.id}/#{last_modified.iso8601}") do
                yield
              end

              render json: payload
            end
          end
        end
      end
    end
  end
end
