module Api
  module V2
    module Internal
      module Monitor
        class SubscriptionsController < Api::V2::Internal::Monitor::BaseController
          allow_parameters :settings, {}
          def settings
            scopes = [
              current_account.settings,
              Arturo::Feature
            ]

            fetch_with_cache('subscription/settings', scopes, [current_account.subscription.updated_at]) do
              subscription_settings_presenter.present(current_account)
            end
          end

          private

          def subscription_settings_presenter
            @subscription_settings_presenter ||= Api::V2::Internal::Monitor::SubscriptionSettingsPresenter.new(current_user, url_builder: self)
          end
        end
      end
    end
  end
end
