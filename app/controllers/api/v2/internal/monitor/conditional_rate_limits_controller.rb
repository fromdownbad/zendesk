module Api
  module V2
    module Internal
      module Monitor
        class ConditionalRateLimitsController < Api::V2::Internal::Monitor::BaseController
          allow_parameters :index, {}
          def index
            render json: { conditional_rate_limits: current_account.texts.conditional_rate_limits.to_a }
          end

          allow_parameters :create, rate_limit: {
            key: Parameters.string,
            limit: Parameters.integer,
            interval: Parameters.integer,
            request_method: Parameters.string,
            endpoint: Parameters.string,
            user_agent: Parameters.string,
            query_string: Parameters.string,
            user_id: Parameters.integer,
            ip_address: Parameters.string,
            expires_on: Parameters.string,
            owner_email: Parameters.string,
            enforce_lotus: Parameters.boolean
          }
          def create
            current_account.add_conditional_rate_limit!(params[:rate_limit].symbolize_keys)
            head :created
          end

          allow_parameters :destroy, key: Parameters.string
          def destroy
            current_account.remove_conditional_rate_limit!(params[:key])
            head :no_content
          end

          allow_parameters :destroy_all, {}
          def destroy_all
            current_account.clear_conditional_rate_limits!
            head :no_content
          end
        end
      end
    end
  end
end
