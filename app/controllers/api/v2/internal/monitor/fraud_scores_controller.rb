module Api
  module V2
    module Internal
      module Monitor
        class FraudScoresController < Api::V2::Internal::Monitor::BaseController
          allowed_fraud_score_parameters = {
            account_id: Parameters.bigid,
            verified_fraud: Parameters.boolean,
            owner_email: Parameters.string,
            subdomain: Parameters.string,
            is_whitelisted: Parameters.boolean,
            source_event: Parameters.string,
            account_fraud_service_release: Parameters.string
          }

          allow_parameters :create, fraud_score: allowed_fraud_score_parameters
          def create
            fraud_score = new_fraud_score
            if fraud_score.save
              render json: monitor_presenter.present(fraud_score), status: :created
            else
              render json: monitor_presenter.present_errors(fraud_score), status: :unprocessable_entity
            end
          end

          resource_action :show
          def show
            fraud_score = current_account.fraud_scores.order('created_at DESC').first

            return render json: {} if fraud_score.nil?

            render json: presenter.present(fraud_score)
          end

          private

          def new_fraud_score
            current_account.fraud_scores.build(params[:fraud_score].reverse_merge(default_params))
          end

          def default_params
            {
              verified_fraud: false,
              score: false,
              account_fraud_service_release: "1.0.0",
              is_whitelisted: false
            }
          end

          def presenter
            @presenter ||= Api::V2::Internal::FraudScorePresenter.new(current_account.owner, url_builder: self)
          end

          def monitor_presenter
            @monitor_presenter ||= Api::V2::Internal::Monitor::FraudScorePresenter.new(current_account.owner, url_builder: self)
          end
        end
      end
    end
  end
end
