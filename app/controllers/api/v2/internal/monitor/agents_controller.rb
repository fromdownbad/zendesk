module Api
  module V2
    module Internal
      module Monitor
        class AgentsController < Api::V2::Internal::Monitor::BaseController
          ACTION_PARAMETERS = {
            id:          Parameters.string,
            name:        Parameters.string,
            email:       Parameters.string,
            sort_column: Parameters.string,
            sort_order:  Parameters.string,
            per_page:    Parameters.integer,
            page:        Parameters.integer
          }.freeze

          SORTABLE_COLUMNS = %w[id name created_at updated_at last_login_at email].freeze

          BILLABLE_AGENT_CONDITION = "`users`.`permission_set_id` IS NULL OR `permission_sets`.`role_type` = #{::PermissionSet::Type::CUSTOM}".freeze

          DEFAULT_AGENTS_PER_PAGE = 30

          allow_parameters :admins, ACTION_PARAMETERS
          def admins
            admins_scope = current_account.
              admins.
              where('`users`.`id` != ?', current_account.owner_id)

            render_users(admins_scope)
          end

          allow_parameters :agents, ACTION_PARAMETERS
          def agents
            billable_agent_scope = current_account.
              users.
              joins('LEFT OUTER JOIN `permission_sets` ON `permission_sets`.`id` = `users`.`permission_set_id`').
              where(BILLABLE_AGENT_CONDITION).
              where(roles: Role::AGENT.id)

            render_users(billable_agent_scope)
          end

          allow_parameters :light_agents, ACTION_PARAMETERS
          def light_agents
            render_users(current_account.light_agents)
          end

          allow_parameters :chat_agents, ACTION_PARAMETERS
          def chat_agents
            render_users(current_account.chat_agents)
          end

          allow_parameters :contributors, ACTION_PARAMETERS
          def contributors
            render_users(current_account.contributors)
          end

          allow_parameters :associated_accounts, email: Parameters.string
          def associated_accounts
            sanitized_email = Zendesk::Mail::Address.sanitize(params.require(:email))

            contact_information_sql = UserContactInformation.
              joins(:account).
              where(email: sanitized_email).
              where(accounts: { is_active: true }).
              select([:account_id, :role, 'user_contact_information.name']).
              to_sql

            render json: UserContactInformation.connection.select_all(contact_information_sql)
          end

          private

          def render_users(base_scope)
            prepared_scope = prepare_scope(base_scope)

            # TODO: add etag here

            payload = user_presenter.present(
              prepared_scope
            )

            payload[:page]     = prepared_scope.current_page.to_i
            payload[:per_page] = prepared_scope.per_page

            render json: payload
          end

          def prepare_scope(base_scope)
            subquery = base_scope.select('users.id')
            users_in_scope = User.joins("INNER JOIN (#{subquery.to_sql}) AS a ON users.id = a.id")

            prepared_scope = users_in_scope.
              reorder("#{sort_column} #{sort_order}").
              includes(:identities). # Avoid N+1 to display the email
              paginate(
                per_page: (params[:per_page] || DEFAULT_AGENTS_PER_PAGE),
                page: (params[:page] || 1)
              )

            filter_scope(prepared_scope)
          end

          def filter_scope(base_scope)
            prepared_scope = base_scope

            if params[:id].present?
              prepared_scope = prepared_scope.where("`users`.`id` like ?", "%#{params[:id]}%")
            end

            if params[:name].present?
              prepared_scope = prepared_scope.where("`users`.`name` like ?", "%#{params[:name]}%")
            end

            if params[:email].present?
              prepared_scope = prepared_scope.
                joins(:identities).
                where("`user_identities`.`value` like ?", "%#{params[:email]}%")
            end

            if params[:sort_column] == 'email'
              prepared_scope = prepared_scope.joins(:identities)
            end

            prepared_scope
          end

          def sort_column
            @sort_column ||= if SORTABLE_COLUMNS.include?(params[:sort_column])
              if params[:sort_column] == 'email'
                '`user_identities`.`value`'
              else
                "`users`.`#{params[:sort_column]}`"
              end
            else
              '`users`.`name`'
            end
          end

          def sort_order
            @sort_order ||= if params[:sort_order] == 'desc'
              'desc'
            else
              'asc'
            end
          end

          def user_presenter
            @user_presenter ||= Api::V2::Internal::Monitor::AgentPresenter.new(current_user, url_builder: self, includes: includes)
          end
        end
      end
    end
  end
end
