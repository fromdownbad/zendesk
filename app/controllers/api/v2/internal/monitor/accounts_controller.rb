require 'zendesk/monitor/owner_changer'

module Api
  module V2
    module Internal
      module Monitor
        class AccountsController < Api::V2::Internal::Monitor::BaseController
          allow_parameters :settings, {}
          def settings
            scopes = [
              current_account.settings,
              current_account.remote_authentications,
              Arturo::Feature,
            ]

            fetch_with_cache('account/settings', scopes, [current_account.updated_at]) do
              account_settings_presenter.present(current_account)
            end
          end

          allow_parameters :owner, {}
          def owner
            owner = current_account.owner

            fetch_with_cache("account/owner/#{owner.id}", [owner.identities], [owner.updated_at]) do
              owner.attributes.slice('id', 'name', 'last_login').merge!(
                is_verified: owner.is_verified?,
                email: owner.email
              )
            end
          end

          allow_parameters :agent_counts, {}
          def agent_counts
            fetch_with_cache('account/agent_counts', [current_account.agents, current_account.permission_sets]) do
              {
                normal: current_account.multiproduct_billable_agent_count!,
                light:  current_account.light_agents.count(:all),
                chat:   current_account.chat_agents.count(:all),
                # Experimental code to retain old values so we can compare between
                # Classic counts and staff service counts (Team Lyrebird)
                legacy: current_account.billable_agent_count,
              }
            end
          end

          allow_parameters :change_account_owner, new_owner_id: Parameters.bigid, demote_role_id: Parameters.bigid
          def change_account_owner
            change_owner(current_account.users.find(params[:new_owner_id]), params[:demote_role_id])
          end

          allow_parameters :create_new_account_owner, new_owner: {
            name:  Parameters.string,
            email: Parameters.string,
            demote_role_id: Parameters.bigid
          }
          def create_new_account_owner
            api_user = current_account.send(:api_client).connection.post('/api/v2/users.json', user: params[:new_owner]).body
            new_owner = current_account.users.find(api_user['user']['id'])
            change_owner(new_owner, params[:new_owner][:demote_role_id])
          end

          allow_parameters :change_account_country_id, address: {
            country_id:  Parameters.integer,
          }
          def change_account_country_id
            if current_account.address.update_attributes(params[:address])
              head :ok
            else
              render json: { error: 'cannot update country id' }, status: :unprocessable_entity
            end
          end

          private

          def account_settings_presenter
            @account_settings_presenter ||= Api::V2::Internal::Monitor::AccountSettingsPresenter.new(current_user, url_builder: self)
          end

          def change_owner(new_owner, demote_role_id)
            owner_changer = Zendesk::Monitor::OwnerChanger.new(current_account, new_owner, demote_role_id)
            owner_changer.change_owner
            if owner_changer.success
              head :ok
            else
              render json: { message: owner_changer.message }, status: :unprocessable_entity
            end
          end
        end
      end
    end
  end
end
