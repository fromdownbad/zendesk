module Api::V2::Internal::Monitor
  class MobileSdkBlipsController < Api::V2::Internal::Monitor::BaseController
    skip_before_action :check_subsystem_authorization

    MOBILE_SDK_BLIPS_KEYS = [
      Api::V2::Internal::Monitor::MobileSdkBlipsPresenter::BLIPS,
      Api::V2::Internal::Monitor::MobileSdkBlipsPresenter::REQUIRED_BLIPS,
      Api::V2::Internal::Monitor::MobileSdkBlipsPresenter::BEHAVIOURAL_BLIPS,
      Api::V2::Internal::Monitor::MobileSdkBlipsPresenter::PATHFINDER_BLIPS
    ].freeze

    UPDATE_PARAMS = {
      blips: {
        enabled: Parameters.boolean,
        permission: Parameters.map(
          key: Parameters.string,
          enabled: Parameters.boolean
        )
      }
    }.freeze

    allow_parameters :show, {}
    def show
      render json: presenter.present(current_account)
    end

    allow_parameters :update_account, UPDATE_PARAMS
    def update_account
      update_blips_settings(current_account.settings, params[:blips], blips_settings_prefix: 'mobile_sdk_')

      if current_account.save
        head :ok
      else
        head :unprocessable_entity
      end
    end

    allow_parameters :update_app,
      id: Parameters.bigid,
      **UPDATE_PARAMS
    def update_app
      app = current_account.mobile_sdk_apps.find(params[:id])
      update_blips_settings(app.settings, params[:blips])

      if app.save
        head :ok
      else
        head :unprocessable_entity
      end
    end

    protected

    def presenter
      @presenter ||= Api::V2::Internal::Monitor::MobileSdkBlipsPresenter.new(current_user, url_builder: self)
    end

    private

    def update_blips_settings(settings, blips, blips_settings_prefix: '')
      if blips.key?(:enabled)
        settings.set("#{blips_settings_prefix}blips" => enabled?(blips[:enabled]))
      end

      if blips.key?(:permission) && MOBILE_SDK_BLIPS_KEYS.include?(blips[:permission][:key])
        key = "#{blips_settings_prefix}#{blips[:permission][:key]}"
        value = enabled?(blips[:permission][:enabled])

        settings.set(key => value)
      end
    end

    def enabled?(value)
      [true, 'true'].include? value
    end
  end
end
