module Api::V2::Internal::Monitor
  class MobileSdkAppSettingsController < Api::V2::Internal::Monitor::BaseController
    skip_before_action :check_subsystem_authorization

    allow_parameters :show, {}
    def show
      if stale?(etag: etag)
        payload = if current_account.mobile_sdk_apps.any?
          key = "#{current_account.mobile_sdk_apps.count}-#{latest_updated_at_timestamp.iso8601}"
          Rails.cache.fetch("/api/v2/internal/monitor/#{current_account.id}/#{key}") do
            presenter.present(current_account)
          end
        else
          presenter.present(current_account)
        end

        render json: payload
      end
    end

    protected

    def presenter
      @presenter ||= Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.new(current_user, url_builder: self)
    end

    private

    def etag
      [
        current_account.mobile_sdk_apps.count,
        latest_updated_at_timestamp
      ]
    end

    def latest_updated_at_timestamp
      # Get the latest updated at from the app itself, it's settings and it's authentication
      updated_at = current_account.mobile_sdk_apps.maximum(:updated_at)
      settings_updated = MobileSdkAppSetting.where(account_id: current_account, mobile_sdk_app_id: current_account.mobile_sdk_apps).maximum(:updated_at)
      auth_updated = MobileSdkAuth.where(account_id: current_account, client_id: current_account.mobile_sdk_apps).maximum(:updated_at)

      [updated_at, settings_updated, auth_updated].compact.max
    end
  end
end
