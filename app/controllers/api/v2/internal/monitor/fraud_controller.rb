module Api
  module V2
    module Internal
      module Monitor
        class FraudController < Api::V2::Internal::Monitor::BaseController
          DEFAULT_DAYS = 7
          LOW_RANGE_DAYS = 1
          HIGH_RANGE_DAYS = 365

          allow_parameters :show, {}
          def show
            render json: fraud_presenter.present(current_account)
          end

          allow_parameters :fraud_score_job, source_event: Parameters.string
          def fraud_score_job
            source_event = Fraud::SourceEvent[params[:source_event]].name
            enqueue_fraud_score_job(source_event)
            render json: {}
          end

          allow_parameters :watchlist, reason: Parameters.string
          def watchlist
            if current_account.settings.is_watchlisted
              current_account.settings.is_watchlisted = false
            else
              enqueue_fraud_score_job(Fraud::SourceEvent::WATCHLISTED, params[:reason])
              current_account.settings.is_watchlisted = true
            end
            current_account.settings.save!
            render json: {}
          end

          allow_parameters :spam_cleanup_job, ticket_action: Parameters.string, tag: Parameters.string, days: Parameters.string, spammy_strings: Parameters.string
          def spam_cleanup_job
            if current_account.has_orca_classic_spam_cleanup_job_enable?
              days = cleanse_days(params[:days])
              SpamCleanupJob.enqueue(current_account.id, params[:ticket_action], params[:tag], days, params[:spammy_strings])
            end
            render json: {}
          end

          private

          def fraud_presenter
            @fraud_presenter ||= Api::V2::Internal::Monitor::FraudPresenter.new(current_account.owner, url_builder: self)
          end

          def enqueue_fraud_score_job(source_event, reason = nil)
            FraudScoreJob.enqueue_account(current_account, source_event, reason)
          end

          def cleanse_days(days)
            if days && days.to_i.between?(LOW_RANGE_DAYS, HIGH_RANGE_DAYS)
              days.to_i
            else
              Rails.logger.info "Malformed days: #{days} for #{current_account}"
              DEFAULT_DAYS
            end
          end
        end
      end
    end
  end
end
