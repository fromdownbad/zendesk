# frozen_string_literal: true

require 'zendesk_database_support/sensors/aurora_cpu_sensor'

module Api::V2::Internal::EntityLookup
  class ViewsTicketsController < Api::V2::Internal::BaseController
    include Zendesk::CursorPagination::ControllerSupport

    skip_around_action :delay_touching if RAILS4 # This is adding large, empty transactions

    # This is a subdomain-less endpoint. The account id is passed as a param,
    # so skip these checks
    skip_before_action :prevent_missing_account_access
    skip_before_action :prevent_inactive_account_access
    skip_after_action  :ensure_csrf_token_into_shared_session # If an account has no routes this check blows up. It's not needed for intra-service comms

    allow_subsystem_user :entity_republisher, only: [:index]

    ZE = Zendesk::EntityPublication
    MAX_DB_CPU = 50.0

    rescue_from ZE::DatabaseBackoffError do |e|
      Rails.logger.info("[Republisher #{self.class.name}] Database over capacity: #{e.db_cpu_util}% util")
      response.headers['X-DB-CPU'] = e.db_cpu_util
      head :too_many_requests
    end

    rescue_from NotImplementedError do
      head :service_unavailable
    end

    # User messes this up, everything else could be derived directly from search_params
    USER_TICKETS_QUERY = 'account_id = :account_id AND (assignee_id = :user_id OR requester_id = :user_id)'
    GROUP_TICKETS_QUERY = 'account_id = :account_id AND group_id = :group_id'
    BRAND_TICKETS_QUERY = 'account_id = :account_id AND brand_id = :brand_id'
    ORGANIZATION_TICKETS_QUERY = 'account_id = :account_id AND organization_id = :organization_id'

    allow_parameters :index,
      account_id: Parameters.bigid,
      type: Parameters.string,
      search_params: Parameters.map
    allow_cursor_pagination_v2_parameters :index

    def index
      # 404 means we lose messages but can roll the consumers out progressively
      return head :not_found unless current_account.has_views_entity_republisher_rollout?

      # 500 means we don't loose messages but block the consumers
      raise NotImplementedError unless current_account.has_views_entity_republisher?

      current_account.on_shard do
        statsd.time('find_time') do
          tickets =
            case params[:type]
            when 'user_fanout'  then user_tickets
            when 'group_fanout' then group_tickets
            when 'brand_fanout' then brand_tickets
            when 'organization_fanout' then organization_tickets
            when 'backfill' then backfill_tickets
            end

          render json: presenter.present(tickets)
        end
      end
    rescue ActiveRecord::AdapterNotSpecified
      # Account is in a different pod. This shouldn't happen, but if the requester
      # sends the wrong account it's important we return 400, as 500 will block the consumer
      raise ActiveRecord::RecordNotFound
    end

    private

    def presenter
      @presenter ||= Api::V2::Internal::EntityLookup::ViewsTicketsPresenter.new(
        current_user,
        url_builder: self,
        includes: { shard_id: account_shard },
        with_cursor_pagination: true
      )
    end

    def user_tickets
      user = current_account.users.find(search_params[:user_id])
      execute(scope(USER_TICKETS_QUERY, account_id: current_account.id, user_id: user.id))
    end

    def group_tickets
      group = current_account.groups.find(search_params[:group_id])
      execute(scope(GROUP_TICKETS_QUERY, account_id: current_account.id, group_id: group.id))
    end

    def brand_tickets
      brand = current_account.brands.find(search_params[:brand_id])
      execute(scope(BRAND_TICKETS_QUERY, account_id: current_account.id, brand_id: brand.id))
    end

    def organization_tickets
      organization = current_account.organizations.find(search_params[:organization_id])
      execute(scope(ORGANIZATION_TICKETS_QUERY, account_id: current_account.id, organization_id: organization.id))
    end

    def backfill_tickets
      from = search_params[:from]
      to = search_params[:to]
      query = "account_id = #{current_account.id}"
      query += " AND updated_at >= '#{from}'" if from
      query += " AND updated_at <= '#{to}'" if to
      execute(scope(query))
    end

    def execute(scope)
      Ticket.without_kasket do
        db_cpu = database_sensor.cpu_utilization.to_f
        raise ZE::DatabaseBackoffError, db_cpu if db_cpu > MAX_DB_CPU

        scope.paginate_with_cursor(cursor_params)
      end
    end

    def scope(query, **query_params)
      Ticket.unscoped.where(query, query_params).select(:id).order(:id)
    end

    def cursor_params
      {
        page: params[:page],
        cursor_pagination_version: 2,
      }
    end

    def current_account
      @current_account ||= ::Account.find(params[:account_id])
    end

    def account_shard
      current_account.shard_id
    end

    def search_params
      @search_params ||= params[:search_params].symbolize_keys
    end

    def statsd
      @statsd ||= Zendesk::StatsD::Client.new(namespace: ['support', 'views_entity_stream'])
    end

    def database_sensor
      @database_sensor ||= ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.new
    end
  end
end
