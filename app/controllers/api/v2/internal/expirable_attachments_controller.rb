module Api
  module V2
    module Internal
      class ExpirableAttachmentsController < Api::V2::Internal::BaseController
        allow_only_subsystem_user :zendesk

        allow_parameters :create,
          account_id: Parameters.bigid,
          user_id: Parameters.bigid,
          filename: Parameters.string,
          uploaded_data: Parameters.file

        ### ### Create Expirable attachment for voice
        ## `POST /api/v2/internal/expirable_attachments`
        ##
        ## This is and endpoint for voice to create an expirable attachment
        ## for the Export Job
        ##
        ## #### Parameters
        ##
        ## account_id
        ## user_id
        ## filename
        ## uploaded_data
        ##
        ## #### Allowed For
        ##
        ## * Subsystem User: voice
        ##
        ## ##### Example Reponse
        ## ``` http
        ## 201 Created
        ##
        ## {
        ##  "url" : "https://helpdesk.zendesk.com/expirable_attachments/token/Ai78v9ph2CG4WMnGJ7tkiyUmH/?name=report.zip"
        ##  }
        ## ```
        def create
          render json: { url: create_expirable_attachment.url }, status: :created
        end

        private

        def create_expirable_attachment
          attachment = ExpirableAttachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(uploaded_data, filename: filename))
          attachment.account = account
          attachment.author = user
          attachment.created_via = "Voice::Calls::ExportJob"
          if Arturo.feature_enabled_for?(:voice_csv_generation, account)
            attachment.save!
          else
            attachment.save
          end
          attachment
        end

        def account
          @account ||= ::Account.find params[:account_id]
        end

        def user
          @user ||= account.users.find params[:user_id]
        end

        def uploaded_data
          @uploaded_data ||= params[:uploaded_data]
        end

        def filename
          @filename ||= params[:filename]
        end
      end
    end
  end
end
