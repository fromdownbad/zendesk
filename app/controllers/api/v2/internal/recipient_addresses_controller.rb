module Api
  module V2
    module Internal
      class RecipientAddressesController < Api::V2::Internal::BaseController
        allow_zopim_user only: [:index, :show]
        allow_subsystem_user :sell, only: [:index, :show]

        ## ### Show the current account's incoming support addresses
        ## `GET /api/v2/internal/recipient_addresses`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   recipient_addresses: [
        ##     {
        ##       url: "https://rebels.zd-dev.com/api/v2/recipient_addresses/10061.json",
        ##       id: 10061,
        ##       brand_id: 10056,
        ##       default: true,
        ##       name: "Rebel Alliance",
        ##       email: "support@rebels.zd-dev.com",
        ##       forwarding_status: "verified",
        ##       spf_status: "verified",
        ##       cname_status: "verified",
        ##       domain_verification_status: "verified",
        ##       domain_verification_code: null,
        ##       metadata: {
        ##         verification_method: "cname"
        ##       },
        ##       created_at: "2019-05-22T23:32:32Z",
        ##       updated_at: "2019-05-22T23:32:47Z"
        ##     },
        ##     {
        ##       url: "https://rebels.zd-dev.com/api/v2/recipient_addresses/10071.json",
        ##       id: 10071,
        ##       brand_id: 10056,
        ##       default: null,
        ##       name: "Rebel Alliance",
        ##       email: "hello@example.com",
        ##       forwarding_status: "verified",
        ##       spf_status: "failed",
        ##       cname_status: "failed",
        ##       domain_verification_status: "failed",
        ##       domain_verification_code: "deb77495f6bfdfdd",
        ##       metadata: {
        ##         verification_method: "cname",
        ##         cname: {
        ##           zendesk1: {
        ##             lookup_result: "mail1.example.com",
        ##             status: "verified"
        ##           },
        ##           zendesk2: {
        ##             lookup_result: "mail2.example.com",
        ##             status: "verified"
        ##           },
        ##           zendesk3: {
        ##             lookup_result: "example.com",
        ##             status: "failed"
        ##           },
        ##           zendesk4: {
        ##             lookup_result: "example.com",
        ##             status: "failed"
        ##           }
        ##         },
        ##         domain_verification: {
        ##           status: "failed"
        ##         },
        ##     [...]
        ## ```
        ## `forwarding`, `spf`, `cname`, `domain_verification` and `ses` status fields
        ## will all return either "verified" or "failed". Additionally the SPF
        ## status could be "deprecated" if the customer is using older SPF
        ## setups.
        ##
        allow_parameters :index, {}
        allow_cursor_pagination_v2_parameters :index
        def index
          render json: presenter.present(recipient_addresses)
        end

        allow_parameters :show, id: Parameters.integer
        def show
          render json: presenter.present(recipient_address)
        end

        ## ### Verify Support Address Forwarding
        ## `PUT /api/v2/internal/recipient_addresses/{id}/verify.json`
        ##
        ## Sends a test email to the specified support address to verify that
        ## email forwarding for the address works. An external support address won't
        ##  work in Zendesk Support until it's verified.
        ##
        ## **Note**: You don't need to verify Zendesk support addresses.
        ##
        ## The endpoint takes the following body parameter: `{"type": "forwarding"}`.
        ##
        ## Use this endpoint after [adding](#create-support-address) an external support
        ## address to Zendesk Support and setting up forwarding on your email server.
        ## See [Forwarding incoming email to Zendesk Support](https://support.zendesk.com/hc/en-us/articles/203663266).
        ##
        ## The endpoint doesn't return the results of the test. Instead, use the
        ## [Show Support Address](#show-support-address) endpoint to check that the
        ## `forwarding_status` property is "verified".
        ##
        ## Other verification checks can also be performed using this API.
        ## These include SPF checks, DNS checks, and SES verification checks.
        ##
        ## Add the type of check as a URL param.
        ##
        ## `PUT /api/v2/internal/recipient_addresses/{id}/verify.json?type=dns`
        ## `PUT /api/v2/internal/recipient_addresses/{id}/verify.json?type=spf`
        ## `PUT /api/v2/internal/recipient_addresses/{id}/verify.json?type=ses`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## ```
        ##
        allow_parameters :verify,
          id: Parameters.bigid,
          type: Parameters.enumeration("forwarding", "spf", "ses", "dns")
        def verify
          case params[:type]
          when "spf" then recipient_address.verify_spf_status!
          when "dns" then recipient_address.verify_dns_status!
          else recipient_address.verify_forwarding_status!
          end
          render json: {}
        end

        private

        def presenter
          @presenter ||= Api::V2::Internal::RecipientAddressPresenter.new(
            current_user,
            url_builder: self,
            with_cursor_pagination: use_cursor_based_pagination?
          )
        end

        def recipient_address
          @recipient_address ||= current_account.recipient_addresses.not_collaboration.find(params[:id])
        end

        def recipient_addresses
          @recipient_addresses ||= begin
            if current_account.has_email_cursor_pagination_recipient_addresses_index?
              if current_account.has_email_recipient_address_enforce_sorting? && use_cursor_based_pagination?
                scope.not_collaboration.order(:created_at).reorder(id: :asc).paginate_with_cursor(
                  cursor_pagination_version: 2,
                  page: params[:page]
                )
              else
                paginate(scope.not_collaboration)
              end
            else
              if current_account.has_email_recipient_address_enforce_sorting?
                paginate(scope.not_collaboration.order(:created_at))
              else
                paginate(scope.not_collaboration)
              end
            end
          end
        end

        def use_cursor_based_pagination?
          return with_cursor_pagination_v2? if current_account.has_email_cursor_pagination_recipient_addresses_index?
          raise StrongerParameters::InvalidParameter.new(StrongerParameters::IntegerConstraint.new.value(params[:page]), 'page') if HashParam.ish?(params[:page])
        end

        def scope
          @scope ||= current_account.recipient_addresses
        end
      end
    end
  end
end
