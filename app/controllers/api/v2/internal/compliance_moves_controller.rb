module Api
  module V2
    module Internal
      class ComplianceMovesController < Api::V2::Internal::BaseController
        allow_parameters :index, :skip
        def index
          render json: presenter.present(current_account)
        end

        private

        def presenter
          @presenter ||= Api::V2::Internal::ComplianceMovesPresenter.new(current_user, url_builder: self)
        end
      end
    end
  end
end
