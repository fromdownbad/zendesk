module Api
  module V2
    module Internal
      class ImportExportJobPolicyController < Api::V2::Internal::BaseController
        # ### Export job keys by type
        # `GET /api/v2/internal/import_export_job_policy/job_keys_by_type`
        #
        # Returns hash of job key arrays by type from ImportExportJobPolicy
        #
        # #### Allowed For
        #
        #  * Subsystem Users
        #
        # #### Example Response
        # {
        #   "default": [ "report_feed", "user_xml_export", "xml_export", "view_csv" ],
        #   "export": [ "report_feed", "user_xml_export", "xml_export" ],
        #   "view": [ "view_csv" ]
        # }
        #
        allow_parameters :job_keys_by_type, {}
        require_oauth_scopes :job_keys_by_type, scopes: [:read], arturo: true
        def job_keys_by_type
          render json: {
            default: ImportExportJobPolicy::DEFAULT_JOBS,
            export: ImportExportJobPolicy::EXPORT_JOBS,
            view: ImportExportJobPolicy::VIEW_JOBS
          }
        end
      end
    end
  end
end
