# Internal API for Mobile SDK settings inside Zendesk Monitor
module Api::V2::Internal
  class MobileSdkSettingsController < Api::V2::Internal::BaseController
    allow_parameters :show, {}
    def show
      render json: presenter.present(mobile_sdk_settings)
    end

    allow_parameters :update, mobile_sdk_settings: {
      auth_endpoint: Parameters.string,
      enabled: Parameters.boolean
    }
    def update
      mobile_sdk_settings.auth_endpoint = params[:mobile_sdk_settings][:auth_endpoint] if params[:mobile_sdk_settings].key?(:auth_endpoint)
      mobile_sdk_settings.enabled = params[:mobile_sdk_settings][:enabled] if params[:mobile_sdk_settings].key?(:enabled)
      mobile_sdk_settings.save!
      render json: presenter.present(mobile_sdk_settings)
    end

    allow_parameters :create, mobile_sdk_settings: {
      auth_endpoint: Parameters.string,
      auth_token: Parameters.string
    }
    def create
      current_account.create_mobile_sdk_settings!(
        auth_endpoint: params[:mobile_sdk_settings][:auth_endpoint],
        auth_token: params[:mobile_sdk_settings][:auth_token],
        enabled: true
      )
      render json: presenter.present(mobile_sdk_settings), status: :created
    end

    private

    def mobile_sdk_settings
      @mobile_sdk_settings ||= current_account.mobile_sdk_settings || raise(ActiveRecord::RecordNotFound)
    end

    def presenter
      @presenter ||= Api::V2::Account::MobileSdkSettingsPresenter.new(current_user, url_builder: self)
    end
  end
end
