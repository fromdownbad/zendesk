module Api
  module V2
    module Internal
      module Chat
        class TicketsController < Api::V2::Internal::BaseController
          include Zendesk::Idempotency

          allow_zopim_user only: %i[create update chats]

          around_action idempotent_proxy(expiry: 15.minutes), only: :create

          # NOTE: DO NOT REMOVE THIS TRANSACTION!
          #
          # rails 4 has 1 more transaction wrapping everything than
          # rails 5 does. As such, `after_commit` can trigger in a
          # different place than before. In the case of adding a
          # ChatEndedEvent, it is very dependent on Ticket NOT
          # triggering `cleanup_instance_variables` in between
          # `new_event` (below) and `apply!`.

          around_action :add_transaction, only: :update if RAILS5

          def add_transaction
            ActiveRecord::Base.transaction do
              yield
            end
          end
          private :add_transaction

          MESSAGING_CHANNELS = [
            'CHAT',
            'NATIVE_MESSAGING',
            'LINE',
            'WECHAT',
            'WHATSAPP',
            'MAILGUN',
            'MESSAGEBIRD_SMS',
            'SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER',
            'TELEGRAM',
            'TWILIO_SMS',
            'SUNSHINE_CONVERSATIONS_TWITTER_DM',
            'VIBER',
            'GOOGLE_RCS',
            'APPLE_BUSINESS_CHAT',
            'GOOGLE_BUSINESS_MESSAGES',
            'KAKAOTALK',
            'INSTAGRAM_DM',
            'SUNSHINE_CONVERSATIONS_API'
          ].freeze

          ALLOWED_SYSTEM_FIELDS = %w[requester.name requester.email requester.locale group_id priority organization_id assignee_id brand_id tags].freeze

          CHAT_PARAMETER = {
            chat_id: Parameters.string,
            history: Parameters.array(Parameters.map),
            webpath: Parameters.array(Parameters.map)
          }.freeze

          CHAT_STARTED_PARAMETER = {
              visitor: Parameters.map(
                id: Parameters.string,
                name: Parameters.string,
                email: Parameters.string,
                phone: Parameters.string,
                locale: Parameters.string,
                social_identity: Parameters.map(
                  type: Parameters.enum('facebook', 'twitter'),
                  value: Parameters.string
                ),
                verified: Parameters.map(
                  name: Parameters.boolean,
                  email: Parameters.boolean,
                  phone: Parameters.boolean
                ),
                sunco_integration: Parameters.map(
                  conversation_id: Parameters.string,
                  integration_id: Parameters.string,
                  brand_id: Parameters.string
                )
              ),
              start_url: Parameters.string,
              requester_id: Parameters.bigid,
              group_id: Parameters.bigid,
              via: Parameters.map(
                channel: Parameters.enum(*MESSAGING_CHANNELS)
              ),
              tags: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil,
              sunco_meta: Parameters.map,
              **CHAT_PARAMETER
          }.freeze

          CHAT_ENDED_PARAMETER = {
            visitor_id: Parameters.string,
            visitor_email: Parameters.string,
            rating: Parameters.map(
              comment: Parameters.string,
              score: Parameters.string
            ),
            tags: Parameters.array(Parameters.string),
            public: Parameters.boolean,
            is_served: Parameters.boolean,
            **CHAT_PARAMETER
          }.freeze

          # ### Create Ticket with chat_started event
          # `POST /api/v2/internal/chat/tickets.json`
          # This internal API is used to create ChatStartedEvents on a new ticket.
          #
          # #### Allowed For
          #
          # * Subsystem users
          #
          # #### Example Response
          #
          # ```http
          # Status: 200 OK
          #
          # {
          #   "ticket": {
          #     "id": 4321,
          #     ...
          #   }
          # }
          # ```
          allow_parameters :create, chat_started: CHAT_STARTED_PARAMETER
          def create
            ticket = new_ticket
            event = new_chat_started_event(ticket).tap(&:apply!)

            render json: ticket_presenter.present(ticket).merge(chat_event_presenter.present(event)),
                   status: :created
          end

          # ### Update Ticket with chat_started | chat_ended event
          # `PUT /api/v2/internal/chat/tickets/{id}.json`
          # This internal API is used to create ChatStartedEvents | ChatEndedEvents
          #  on an existing ticket.
          #
          # #### Allowed For
          #
          # * Subsystem users
          #
          # #### Example Response
          #
          # ```http
          # Status: 200 OK
          #
          # {
          #   "ticket": {
          #     "id": 4321,
          #     ...
          #   }
          # }
          # ```
          allow_parameters(
            :update,
            id: Parameters.bigid,
            chat_event_id: Parameters.bigid,
            chat_started: CHAT_STARTED_PARAMETER,
            ticket_status: Parameters.string,
            chat_ended: CHAT_ENDED_PARAMETER,
            chat_history_updated: CHAT_PARAMETER
          )
          def update
            ticket = find_existing_ticket
            event = if chat_history_updated?
              find_chat_started_event!(ticket, chat_id: chat_param.require(:chat_id)).tap do |chat_started_event|
                save_chat_transcript(ticket, chat_started_event, transient: true)
              end
            else
              add_event(ticket)
            end

            render json: ticket_presenter.present(ticket).merge(chat_event_presenter.present(event))
          end

          allow_parameters :chats, id: Parameters.bigid
          def chats
            ticket = Ticket.with_deleted { find_existing_ticket }
            fields = %i[audit_id chat_id visitor_id]
            transcripts = ticket.chat_transcripts.group(*fields).select(*fields)

            render json: transcripts.map { |transcript| transcript.slice(*fields) }
          end

          private

          def set_social_identity(requester, visitor)
            social_identity = visitor[:social_identity].presence.try do |identity|
              { type: identity.require(:type), value: identity.require(:value) }
            end
            return unless social_identity && !current_account.user_identities.social_identity_exists?(social_identity)
            requester[:identity] = social_identity
            requester[:is_verified] = true
          end

          def requester_from_visitor
            base = {
              name: visitor_param.require(:name),
              email: visitor_param[:email],
              phone: visitor_param[:phone]
            }
            base[:locale] = visitor_param[:locale] if visitor_param[:locale].present?
            base.tap { |requester| set_social_identity(requester, visitor_param) }.reject { |_, v| v.blank? }
          end

          def update_path(container, path, change)
            *head, tail = path.split('.')
            return unless tail
            head.reduce(container) { |acc, key| acc[key] ||= {} }.deep_merge!(tail => change)
          end

          def normalize_ticket_field_value(id, value)
            ticket_field = current_account.ticket_fields.find { |field| field.id == id }
            case ticket_field
            when FieldTagger
              current_account.fetch_custom_field_option_value(value)
            else
              value
            end
          end

          def new_ticket_params
            ticket_data = { 'group_id' => chat_param[:group_id], 'via' => via, 'fields' => {} }

            if requester_id
              ticket_data['requester_id'] = requester_id
            else
              ticket_data['requester'] = requester_from_visitor
            end

            if Arturo.feature_enabled_for?(:apply_sunco_ticket_data, current_account)
              chat_param.fetch(:sunco_meta, {}).each do |name, value|
                case name
                when /^dataCapture\.ticketField\.(\d+)$/
                  ticket_data['fields'][$1] = normalize_ticket_field_value($1.to_i, value)
                when /^dataCapture\.systemField\.([\w.]+)$/
                  update_path(ticket_data, $1, value) if $1.in? ALLOWED_SYSTEM_FIELDS
                end
              end
            end

            { ticket: ticket_data.with_indifferent_access }
          end

          def requester_id
            @requester_id ||= begin
              id = params.require(:chat_started)[:requester_id]
              if current_account.users.exists?(id: id)
                id
              else
                ::Users::Merge.where(account: current_account).find_by(loser_id: id).try(:winner_id)
              end
            end
          end

          def via
            @via ||= begin
              params[:chat_started].fetch(:via, {}).tap { |via| via[:channel] ||= ViaType.CHAT.to_s }
            end
          end

          def chat_param
            @chat_param ||= params.require(%i[chat_started chat_history_updated chat_ended].find { |key| params.key? key })
          end

          def chat_history_param
            chat_param.fetch(:history, [])
          end

          def visitor_param
            chat_param.require(:visitor)
          end

          def new_ticket
            Zendesk::Tickets::V2::Initializer.new(current_account, current_user, new_ticket_params).ticket
          end

          def find_existing_ticket
            Zendesk::Tickets::Initializer.new(current_account, current_user, params).find_ticket
          end

          def chat_event_presenter
            Api::V2::Tickets::ChatEventPresenter.new(current_user, url_builder: self)
          end

          def ticket_presenter
            Api::V2::Tickets::TicketPresenter.new(current_user, url_builder: self)
          end

          def chat_history_updated?
            params.key? :chat_history_updated
          end

          def chat_ended?
            params.key? :chat_ended
          end

          def new_chat_started_event(ticket)
            chat_started_params = params.require(:chat_started)
            attributes = {
              visitor_id: visitor_param.require(:id),
              conversation_id: visitor_param.dig(:sunco_integration, :conversation_id),
              chat_id: chat_started_params.require(:chat_id),
              chat_start_url: chat_started_params[:start_url],
              ticket_status: params[:ticket_status],
              tags: chat_started_params[:tags]
            }.compact
            ChatStartedEvent.new(ticket: ticket, value: attributes.compact).tap do |event|
              save_chat_transcript(ticket, event, transient: true) if chat_history_param.present?
            end
          end

          def find_chat_started_event!(ticket, chat_id:)
            event_id = params.require(:chat_event_id)
            chat_started_event = ticket.events.find_by(id: event_id, type: ChatStartedEvent)
            return chat_started_event if chat_started_event&.chat_id == chat_id
            raise ActionController::ParameterMissing, "Invalid event id"
          end

          def save_chat_transcript(ticket, event, transient: false)
            chat_transcript_params = {
              chat_started_event: event,
              ticket: ticket,
              account: current_account,
              format: transient ? :transient_json : :json
            }

            records = ChatTranscript::ChatTranscriptsBuilder.build(chat_transcript_params) do |builder|
              builder.add_chat_history chat_history_param
              builder.add_web_paths chat_param.fetch(:webpath, [])
            end
            event.chat_transcripts.replace records
          end

          def create_public_comment?
            chat_param.fetch(:public, false) if current_account.has_public_chat_transcripts?
          end

          def new_chat_ended_event(ticket, chat_started_event:)
            attributes = {
              chat_id: chat_param.require(:chat_id),
              visitor_id: chat_param.require(:visitor_id),
              visitor_email: chat_param[:visitor_email],
              is_served: chat_param.fetch(:is_served, true),
              rating: chat_param[:rating],
              tags: chat_param[:tags],
            }.compact
            ChatEndedEvent.new(ticket: ticket, value: attributes, chat_started_event: chat_started_event)
          end

          def add_public_transcript_comment(ticket, history:, uploads:)
            ticket.audit.via_id = ViaType.CHAT_TRANSCRIPT
            ticket.add_comment(
              author_id: ticket.requester_id,
              is_public: true,
              body: ChatTranscript::ChatHistoryFormatter.new(history),
              uploads: uploads.presence # an empty array would result in an error during validation
            )
          end

          def chat_transcript
            Zendesk::ChatTranscripts::ChatAttachmentsParser.new(chat_history_param)
          end

          def apply_ticket_metrics(ticket)
            return unless current_account.has_chat_ticket_metrics? && ViaType.CHAT != ticket.via_id
            # only record metrics for messaging and when arturo is available
            Zendesk::ChatTranscripts::ChatHistoryTicketEnumerator.
              new(chat_history_param).
              with_first_agent_comment(ticket) do |shadow_ticket|
                ticket.ticket_metric_set.update_metrics(shadow_ticket)
              end
          end

          def new_event(ticket)
            return new_chat_started_event(ticket) unless chat_ended?

            chat_started_event = find_chat_started_event!(ticket, chat_id: chat_param.require(:chat_id))
            save_chat_transcript(ticket, chat_started_event)
            apply_ticket_metrics(ticket)

            if create_public_comment? && ViaType.CHAT == ticket.via_id
              # attachments on public comment
              add_public_transcript_comment(ticket, history: chat_transcript.history, uploads: chat_transcript.upload_tokens)
            elsif current_account.has_chat_uploads?
              # attachments on chat started event
              chat_started_event.add_attachments(chat_transcript.attachments(current_account))
            end

            new_chat_ended_event(ticket, chat_started_event: chat_started_event)
          end

          def add_event(ticket)
            ticket.will_be_saved_by(current_user, via_id: ticket.via_id)
            new_event(ticket).tap(&:apply!)
          rescue ActiveRecord::RecordInvalid
            raise unless ticket.reload.inoperable? && chat_ended?
          end
        end
      end
    end
  end
end
