module Api
  module V2
    module Internal
      class BrandsController < Api::V2::Internal::BaseController
        ## ### Remove a brands's host mapping
        ## `PUT /api/v2/internal/brands/{id}/remove_host_mapping.json`
        ##
        ## Removes the host_mapping from the given brand
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## ```
        allow_parameters :remove_host_mapping, id: Parameters.bigid

        def remove_host_mapping
          brand.host_mapping = nil
          brand.save!
          render json: {}, status: :ok
        end

        protected

        def brand
          @brand ||= Brand.with_deleted { current_account.brands.find(params[:id]) }
        end
      end
    end
  end
end
