module Api
  module V2
    module Internal
      class GooddataIntegrationController < Api::V2::Internal::BaseController
        ## ### Show the current account's GoodData integration
        ## `GET /api/v2/internal/gooddata_integration.json`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "gooddata_integration": {
        ##     "project_id": "ntgy78m0k4bb8qnoc2r1sg1ilu7e7w1g"
        ##   }
        ## }
        ## ```
        allow_parameters :show, :skip
        def show
          render json: integration.as_json(only: [:project_id, :version]).merge(subdomain_match: subdomain_match)
        end

        ## ### Update the current account's GoodData integration
        ## `PUT /api/v2/internal/gooddata_integration.json`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "gooddata_integration": {
        ##     "project_id": "ayrdbghfwqrg978t5tvndum7nl4k9jky"
        ##   }
        ## }
        ## ```
        allow_parameters :update, :skip
        def update
          if different_project?
            current_integration = integration

            Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(current_account)
            new_integration = create_new_integration_from(current_integration, params[:gooddata_integration][:project_id])

            render json: new_integration.as_json
          else
            render json: integration.as_json
          end
        end

        ## ### Sync the account configuration to GoodData
        ## `PUT /api/v2/internal/gooddata_integration/sync_configuration.json`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ## ```http
        ## Status: 200 OK
        ## ```
        allow_parameters :sync_configuration, {}
        def sync_configuration
          if integration.present?
            GooddataConfigurationJob.enqueue(current_account.id)
          end

          head :ok
        end

        ## ### Reprovision GoodData user
        ## If user is deleted in gooddata side, destroy and re create user
        ## else, force a user sync
        ##
        ## `PUT /api/v2/internal/gooddata_integration/reprovision_user.json`
        ##
        ## #### Request Format
        ##  * user_id: id of the agent/admin to reprovision
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ## * Return 404 if user not found or not agent/admin
        ##
        ## ```http
        ## Status: 200 OK
        ## ```
        allow_parameters :reprovision_user, user_id: Parameters.bigid
        def reprovision_user
          if integration.present?
            user_id = params[:user_id]
            zendesk_user = current_account.agents.find(user_id)

            GooddataUserReprovisioningJob.enqueue(current_account.id, zendesk_user.id)
          end

          head :ok
        end

        ## ### Delete the current account's GoodData integration
        ## `DELETE /api/v2/internal/gooddata_integration.json`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "gooddata_integration": {
        ##     "project_id": "ntgy78m0k4bb8qnoc2r1sg1ilu7e7w1g"
        ##   }
        ## }
        ## ```
        allow_parameters :destroy, :skip
        def destroy
          Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(current_account)

          render json: integration.as_json(only: [:project_id, :version])
        end

        private

        def subdomain_match
          integration.subdomain_matches_project?
        rescue # we don't want to hide the project information if a GD API call fails
          false
        end

        def integration
          @integration ||= current_account.gooddata_integration || {}
        end

        def different_project?
          integration.project_id != params[:gooddata_integration][:project_id]
        end

        def create_new_integration_from(old_integration, new_project_id)
          excluded_attributes = %w[updated_at created_at id deleted_at invitation_id project_id]

          new_integration = GooddataIntegration.new project_id: new_project_id
          attributes_to_update = old_integration.attributes.reject { |name, _| excluded_attributes.include?(name) }
          attributes_to_update.each do |name, _|
            new_integration.send("#{name}=", old_integration.send(name))
          end

          integration_provisioning =
            Zendesk::Gooddata::IntegrationProvisioning.new(current_account, gooddata_integration: new_integration)
          integration_provisioning.assign_role_attributes

          new_integration.save!

          new_integration
        end
      end
    end
  end
end
