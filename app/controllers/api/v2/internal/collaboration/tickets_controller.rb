module Api
  module V2
    module Internal
      module Collaboration
        class TicketsController < Api::V2::BaseController
          allow_subsystem_user :collaboration

          ## ### Create Ticket
          ## `POST /api/v2/internal/collaboration/tickets.json`
          ## This internal API is used to create tickets from side conversations.
          ##
          ## #### Allowed For
          ##
          ## * Subsystem users
          ##
          ## #### Example Response
          ##
          ## ```http
          ## Status: 200 OK
          ##
          ## {
          ##   "ticket": {
          ##     "id": 4321
          ##   }
          ## }
          ## ```
          allow_parameters(
            :create,
            ticket: {
              subject: Parameters.string,
              external_id: Parameters.string,
              group_id: Parameters.bigid,
              assignee_id: Parameters.bigid | Parameters.nil_string,
              comment: {
                html_body: Parameters.string,
                attachments: Parameters.array(
                  Parameters.map(
                    url: Parameters.string,
                    filename: Parameters.string,
                    content_type: Parameters.string
                  )
                )
              }
            }
          )
          def create
            initializer = Zendesk::Tickets::Initializer.new(
              current_account,
              current_user,
              create_params,
              request_params: Zendesk::Tickets::HttpRequestParameters.new(request)
            )

            ticket = initializer.ticket

            ticket.via_id = ticket.audit.via_id = ticket.comment.via_id = Zendesk::Types::ViaType.SIDE_CONVERSATION

            add_attachments(params.dig(:ticket, :comment, :attachments), ticket.comment)

            ticket.save!

            render json: { ticket: { id: ticket.nice_id } }, status: :ok
          end

          ## ### Update Ticket
          ## `PUT /api/v2/internal/collaboration/tickets/{id}.json`
          ##
          ## #### Allowed For
          ##
          ## * Subsystem users
          ##
          ## #### Example Response
          ##
          ## ```http
          ## Status: 200 OK
          ##
          ## {
          ##   "audit": {
          ##     "id": 4321
          ##   }
          ## }
          ## ```
          allow_parameters(
            :update,
            id: Parameters.bigid,
            ticket: {
              comment: {
                html_body: Parameters.string,
                attachments: Parameters.array(
                  Parameters.map(
                    url: Parameters.string,
                    filename: Parameters.string,
                    content_type: Parameters.string
                  )
                )
              }
            }
          )
          def update
            ticket = current_account.tickets.find_by_nice_id(params[:id])
            ticket.add_comment(html_body: params.dig(:ticket, :comment, :html_body), is_public: true)
            ticket.will_be_saved_by(current_user, via_id: ViaType.SIDE_CONVERSATION)
            add_attachments(params.dig(:ticket, :comment, :attachments), ticket.comment)
            audit = ticket.audit
            ticket.save!

            render json: { audit: { id: audit.id } }, status: :ok
          end

          protected

          def create_params
            {
              ticket: {
                subject: params.dig(:ticket, :subject),
                external_id: params.dig(:ticket, :external_id),
                group_id: params.dig(:ticket, :group_id),
                assignee_id: params.dig(:ticket, :assignee_id),
                comment: {
                  html_body: params.dig(:ticket, :comment, :html_body),
                  is_public: true
                }
              }
            }
          end

          def add_attachments(attachments, comment)
            attachments&.each do |attachment|
              comment.attachments << Attachment.from_external_url(
                current_account,
                attachment[:url],
                attachment[:filename],
                attachment[:content_type],
                attachment[:content_type]&.starts_with?('image/')
              )
            end
          end
        end
      end
    end
  end
end
