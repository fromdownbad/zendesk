module Api
  module V2
    module Internal
      module Collaboration
        class EventsController < Api::V2::BaseController
          allow_subsystem_user :collaboration

          ## ### Create Collaboration Events on Tickets
          ## `POST /api/v2/internal/collaboration/events.json`
          ## This internal API is used to create audit_events related to events for
          ## the Collaboration feature.
          ##
          ## #### Allowed For
          ##
          ## * Subsystem users
          ##
          ## #### Example Response
          ##
          ## ```http
          ## Status: 200 OK
          ##
          ## {
          ##   "id": 4321
          ## }
          ## ```
          allow_parameters :create, ticket_id: Parameters.bigid, disable_triggers: Parameters.boolean,
                                    event: {
                                      subject: Parameters.string,
                                      type: Parameters.enum('thread_created', 'thread_closed', 'thread_reopened', 'thread_reply'),
                                      thread_id: Parameters.string,
                                      recipient_count: Parameters.integer,
                                      sc_type: Parameters.string,
                                      identifier: Parameters.string
                                    }
          def create
            if ticket.nil?
              render json: { message: 'Ticket does not exist' }, status: :unprocessable_entity
              return
            end

            if Arturo.feature_enabled_for?(:sc_skip_duplicate_events, current_account) && created_event_exists?
              statsd_client.increment('collab_thread_events.create_event_exists')
              render json: { message: 'Ticket CollabThreadCreated event already exists' }, status: :unprocessable_entity
              return
            end

            if reply_event_exists?
              statsd_client.increment('collab_thread_events.reply_event_exists')
              render json: { message: 'Ticket CollabThreadReply event already exists' }, status: :unprocessable_entity
              return
            end

            ticket.will_be_saved_by(current_user, via_id: ViaType.SIDE_CONVERSATION)
            ticket.audit.disable_triggers = params[:disable_triggers] || false
            ticket.audit.events << event
            audit = ticket.audit
            ticket.save!

            render json: { id: audit.id }, status: :ok
          end

          private

          def created_event_exists?
            params[:event][:type] == 'thread_created' && ticket.events.where(type: CollabThreadCreated.to_s).where(value_reference: params[:event][:thread_id]).any?
          end

          def reply_event_exists?
            params[:event][:type] == 'thread_reply' && ticket.events.where(type: CollabThreadReply.to_s).where(value_reference: params[:event][:thread_id]).where("value LIKE ?", "%:identifier: #{params[:event][:identifier]}%").any?
          end

          def event
            event_type = params[:event][:type]
            case event_type
            when 'thread_created'
              event = CollabThreadCreated.new
            when 'thread_closed'
              event = CollabThreadClosed.new
            when 'thread_reopened'
              event = CollabThreadReopened.new
            when 'thread_reply'
              event = CollabThreadReply.new
            end

            event.subject = params[:event][:subject]
            event.thread_id = params[:event][:thread_id]
            event.recipient_count = params[:event][:recipient_count]
            event.sc_type = params[:event][:sc_type]
            event.identifier = params[:event][:identifier] if params[:event][:identifier]

            event
          end

          def ticket
            @ticket ||= current_account.tickets.find_by_nice_id(params[:ticket_id])
          end

          def statsd_client
            Rails.application.config.statsd.client
          end
        end
      end
    end
  end
end
