module Api::V2::Internal
  class CouponsController < Api::V2::Internal::BaseController
    class RecordNotDestroyed < ActiveRecord::RecordNotSaved
      # NADA
    end

    before_action :identify_coupon_type

    deprecate_actions [:index, :show, :edit, :update, :new, :create, :destroy],
      with_feature: :deprecate_classic_coupons_controller

    # Write operations on classic coupons are no longer supported. This filter will reject those action/operations
    # when the type parameter sent with the request is set to "classic"; it will also set the response code to HTTP
    # 405 (Method Not Allowed)
    before_action :reject_action, only: [:new, :create, :edit, :update, :destroy], if: :is_classic_coupon_type?

    rescue_from ActiveRecord::RecordInvalid,  with: :record_not_saved_handler
    rescue_from ActiveRecord::RecordNotSaved, with: :record_not_saved_handler

    ## ### List coupons.
    ##
    ## `GET /api/v2/internal/coupons.json?type=(classic|zuora)`
    ##
    ## Fetches the list of coupons for the :type specified.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :index, :skip
    def index
      render json: presenter.present(coupons)
    end

    ## ### Get coupon detail for read.
    ##
    ## `GET /api/v2/internal/coupons/:id.json?type=(classic|zuora)`
    ##
    ## Fetches detail information for the coupon with matching the :id and :type.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :show, id: Parameters.bigid, type: Parameters.string
    resource_action :show

    ## ### Get coupon detail for read/write.
    ##
    ## `GET /api/v2/internal/coupons/:id/edit.json?type=(classic|zuora)`
    ##
    ## Fetches detail information for the coupon with matching the :id and :type suitable
    ## for editing.
    ##
    ## Will respond with an HTTP status 405 if the coupon referenced is no longer editable,
    ## otherwise 200.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :edit, :skip
    def edit
      if coupon.editable?
        render json: presenter.present(coupon)
      else
        error = presenter.present_errors(coupon)
        error[:resource] = presenter.present(coupon)
        render json: error, status: :method_not_allowed
      end
    end

    ## ### Update coupon.
    ##
    ## `PUT /api/v2/internal/coupons/:id.json`
    ##
    ## Updates the coupon with the content of the submitted form data. Form data *must* include the
    ## the :type parameter (one of: "classic", "zuora") along with the actual coupon data.
    ##
    ## Will respond with an HTTP status 422 if it is unable to save/update the coupon record,
    ## otherwise 200.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :update, :all_parameters
    def update
      coupon.update_attributes!(params[:coupon])
      render json: presenter.present(coupon)
    end

    ## ### New coupon.
    ##
    ## `GET /api/v2/internal/coupons/new.json?type=(classic|zuora)`
    ##
    ## Builds and returns default coupon record values suitable for creating a new
    ## coupon record.
    ##
    ## Will respond with an HTTP status 405 if creating coupons for the specified type is no
    ## longer allowed, otherwise 200.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :new, :all_parameters
    def new
      render json: presenter.present(new_zuora_coupon)
    end

    ## ### Create coupon.
    ##
    ## `POST /api/v2/internal/coupons.json`
    ##
    ## Creates a new coupon record with the content of the submitted form data. Form data *must* include the
    ## the :type parameter (one of: "classic", "zuora") along with the actual coupon data.
    ##
    ## Will respond with an HTTP status 422 if it is unable to save/update the coupon record,
    ## otherwise 200.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :create, :all_parameters
    def create
      coupon = new_zuora_coupon
      coupon.save!
      render json: presenter.present(coupon), status: :created
    end

    ## ### Delete coupon.
    ##
    ## `DELETE /api/v2/internal/coupons/:id.json?type=(classic|zuora)`
    ##
    ## Deletes the coupon referenced by the :id and :type parameters.
    ##
    ## Will respond with an HTTP status 422 if it is unable to delete the coupon record,
    ## otherwise 200.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    ##
    allow_parameters :destroy, :skip
    def destroy
      if coupon.destroy
        default_delete_response
      else
        error = presenter.present_errors(coupon).merge(resource: presenter.present(coupon))
        render json: error, status: :unprocessable_entity
      end
    end

    private

    def is_classic_coupon_type? # rubocop:disable Naming/PredicateName
      @coupon_type == :classic
    end

    def identify_coupon_type
      @coupon_type = params[:type].try(:to_sym)

      unless @coupon_type.present?
        head :bad_request
        return
      end

      unless [:classic, :zuora].include? @coupon_type
        head :bad_request
        return
      end
    end

    def coupons
      @coupons ||= coupon_class.all.to_a
    end

    def coupon
      @coupon ||= coupon_class.find(params[:id])
    end

    def coupon_class
      @coupon_class ||=
        case @coupon_type
        when :classic then Coupon
        when :zuora   then ZendeskBillingCore::Zuora::Coupon
        end
    end

    def new_zuora_coupon
      defaults = {
        start_date: Date.today,
        end_date: 90.days.from_now.to_date,
        discount_type: ZendeskBillingCore::Zuora::Coupon::DISCOUNT_TYPES.first,
        discount_amount: 0.0,
        duration: 1
      }
      ZendeskBillingCore::Zuora::Coupon.new(params[:coupon] || defaults)
    end

    def reject_action
      head :method_not_allowed
    end

    def record_not_saved_handler(exception)
      record = exception.record
      error  = presenter.present_errors(record).merge(resource: presenter.present(record))
      render json: error, status: :unprocessable_entity
    end

    def presenter
      @presenter ||= Api::V2::Internal::CouponPresenter.new(current_user, url_builder: self)
    end
  end
end
