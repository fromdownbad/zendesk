module Api
  module V2
    module Internal
      class PodMovesController < Api::V2::Internal::BaseController
        allow_only_subsystem_user :pod_mover

        before_action :load_accounts, except: [:fetch_feature, :enable_feature, :disable_feature]

        ### ### Fetch Feature
        ## `GET /api/v2/internal/fetch_feature`
        ##
        ## Exodus reqires that some services be disabled for a move (i.e. SyncAttachments, Archiver).
        ## The endpoint gets the status of the feature in the account_settings table.
        ## The api will be polled by Exodus to determine when a feature has been changed.
        ##
        ##  #### Allowed For
        ##  * subsystem user: pod_mover
        ##
        ## #### Example Response
        ##
        ## Status: 200 OK
        ## {
        ##  "feature": "write_to_archive_v2_enabled"
        ##  "feature_status": "pending_disable"
        ## }
        allow_parameters :fetch_feature, feature: Parameters.string, account_id: Parameters.bigid
        def fetch_feature
          if Zendesk::ShardMover::Accounts.feature_capable_of_disablement?(params[:feature])
            account = ::Account.find(params[:account_id].to_i)
            feature_status = Zendesk::ShardMover::Accounts.fetch_feature params[:feature], account
            render json: { feature: params[:feature], feature_status: feature_status }
          else
            head 400
          end
        end

        ### ### Enable Feature
        ## `PUT /api/v2/internal/enable_feature`
        ##
        ## Exodus reqires that some services be disabled for a move (i.e. SyncAttachments, Archiver).
        ## This endpoint sets properties in the account_settings table to re-enable the services.
        ##
        ##  #### Allowed For
        ##  * subsystem user: pod_mover
        ##
        ## #### Example Response
        ##
        ## 200 OK
        allow_parameters :enable_feature, feature: Parameters.string, account_id: Parameters.bigid
        def enable_feature
          if Zendesk::ShardMover::Accounts.feature_capable_of_disablement?(params[:feature])
            account = ::Account.find(params[:account_id].to_i)
            Zendesk::ShardMover::Accounts.enable_feature params[:feature], account
            head :ok
          else
            head 400
          end
        end

        ### ### Disable Feature
        ## `PUT /api/v2/internal/disable_feature`
        ##
        ## Exodus reqires that some services be disabled for a move (i.e. SyncAttachments, Archiver).
        ## The endpoint sets the disable_pending flag to be ack'd by the services and returns the
        ## status of the service.
        ##
        ##  #### Allowed For
        ##  * subsystem user: pod_mover
        ##
        ## #### Example Response
        ##
        ## Status: 200 OK
        ## {
        ##  "feature": "write_to_archive_v2_enabled"
        ##  "feature_status": "pending_disable"
        ## }
        allow_parameters :disable_feature, feature: Parameters.string, account_id: Parameters.bigid
        def disable_feature
          if Zendesk::ShardMover::Accounts.feature_capable_of_disablement?(params[:feature])
            account = ::Account.find(params[:account_id].to_i)
            response = Zendesk::ShardMover::Accounts.disable_feature params[:feature], account
            render json: {feature: params[:feature], feature_status: response}
          else
            head 400
          end
        end

        private

        attr_accessor :accounts

        # TODO: make this a single Account.find
        def load_accounts
          self.accounts = params[:accounts].map do |account|
            ::Account.find(account.to_i)
          end
        end
      end
    end
  end
end
