module Api::V2::Internal
  class HelpCenterStatesController < Api::V2::Internal::BaseController
    ## ### Update Help Center state.
    ##
    ## `PUT /api/v2/internal/help_center_state.json`
    ##
    ## Updates the Account's and Brand's Help Center states according to the submitted form data.
    ## Form data *must* include the :directive parameter (one of: "enable", "disable", "restrict").
    ## Form data *must* include the :help_center_state parameter (one of: "restricted", "enabled", "archived").
    ## Form data *must* include the :help_center_id parameter.
    ##
    ## Will respond with an HTTP status 400 if directive is not allowed,
    ## otherwise 200.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    allow_parameters :update, directive: Parameters.string, help_center_state: Parameters.string, help_center_id: Parameters.integer
    def update
      account_help_center_state = params.fetch(:directive).to_s
      brand_help_center_state = params.fetch(:help_center_state).to_s
      help_center_id = params.fetch(:help_center_id).to_i

      help_center_state_changer = HelpCenterStateChanger.new(
        account: current_account,
        brand: current_brand
      )

      help_center_state_changer.update(
        account_help_center_state: account_help_center_state,
        brand_help_center_state: brand_help_center_state,
        help_center_id: help_center_id
      )
      head :ok
    rescue ArgumentError, KeyError
      head :bad_request
    end

    ## ### Update Help Center state when a Help Center is soft-deleted.
    ##
    ## `DELETE /api/v2/internal/help_center_state.json`
    ##
    ## Updates the Account's Help Center state according to the submitted form data,
    ## and deletes the Brand Help Center representation.
    ## Form data *must* include the :directive parameter (one of: "enable", "disable", "restrict").
    ##
    ## Will respond with an HTTP status 400 if directive is not allowed,
    ## otherwise 200.
    ##
    ## #### Allowed For
    ##
    ##  * Subsystem Users
    ##
    allow_parameters :destroy, directive: Parameters.string
    def destroy
      account_help_center_state = params.fetch(:directive).to_s

      help_center_state_changer = HelpCenterStateChanger.new(
        account: current_account,
        brand: current_brand
      )

      help_center_state_changer.destroy(
        account_help_center_state: account_help_center_state
      )
      head :ok
    rescue ArgumentError, KeyError
      head :bad_request
    end
  end
end
