module Api
  module V2
    module Internal
      class BoostsController < Api::V2::Internal::BaseController
        before_action :ensure_feature_boost, only: :update

        allow_subsystem_user :embeddable, only: [:show]

        ## ### Showing Boost
        ## `GET /api/v2/internal/boost.json`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        ## #### Example Response
        ##
        ## See the boost presenter
        allow_parameters :show, {}
        def show
          render json: presenter.present(feature_boost)
        end

        ## ### Create or Update Boost
        ## `PUT /api/v2/internal/boost.json`
        allow_parameters :update, boost: Parameters.anything
        def update
          feature_boost.update_attributes!(params[:boost])
          render json: presenter.present(feature_boost)
        end

        ## ### Remove Boost
        ## `DELETE /api/v2/internal/boost.json`
        allow_parameters :destroy, {}
        def destroy
          feature_boost.update_attribute(:is_active, false)
          render json: presenter.present(feature_boost)
        end

        private

        def ensure_feature_boost
          @feature_boost ||= (current_account.feature_boost || current_account.build_feature_boost)
        end

        def feature_boost
          @feature_boost ||= current_account.feature_boost || raise(ActiveRecord::RecordNotFound)
        end

        def presenter
          @presenter ||= Api::V2::Internal::BoostPresenter.new(current_account.owner, url_builder: self)
        end
      end
    end
  end
end
