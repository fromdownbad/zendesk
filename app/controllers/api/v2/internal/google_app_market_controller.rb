module Api
  module V2
    module Internal
      # Nomenclature Note: Google App Market has been rebranded as G Suite
      class GoogleAppMarketController < Api::V2::BaseController
        before_action :require_admin!, only: [:email_forwarding_setup, :group_email_forwarding_setup, :qualifies_for_google_apps, :enable_google_apps]
        before_action :capture_required_params, only: [:email_forwarding_setup, :group_email_forwarding_setup, :purge_google_apps_domain_aliases]

        skip_before_action :enforce_ssl!, :require_agent!, :authenticate_user,
          :ensure_proper_protocol, :verify_authenticity_token,
          only: [:google_site_verification, :unbind_google_apps_domain, :purge_google_apps_domain_aliases]

        # Following two actions would prevent Google's GET to /google<token>.html
        skip_before_action :prevent_missing_account_access, :prevent_inactive_account_access, only: [:unbind_google_apps_domain]

        # G Suite accounts are created with a default domain alias
        DEFAULT_DOMAIN_ALIAS = /test-google-a.com/
        QUALIFIES_GAM_EXPIRATION = 1.day.to_i

        allow_parameters :email_forwarding_setup,
          customer_id: Parameters.string,
          domain: Parameters.string,
          owner: Parameters.string,
          email: Parameters.string
        require_oauth_scopes :email_forwarding_setup, scopes: [:write], arturo: true
        def email_forwarding_setup
          ## 4 Google API calls are necessary for setting up email forwarding
          # 1. Add our subdomain as a domain alias of the user's G Suite account
          domain_alias = zendesk_subdomain
          response = gam_client.add_domain_alias(customer_id: params[:customer_id], domain_alias: domain_alias)
          return if render_error(response, ignore_duplicate: true)

          verification_domain = zendesk_subdomain
          # 2. Get site verification token from Google
          response = get_and_write_site_verification_token(domain: verification_domain)
          return if render_error(response)

          # 3. Tell Google to verify our subdomain
          response = verify_site_token(domain: verification_domain)
          return if render_error(response)

          # Create new recipient address
          create_recipient_address

          # 3. Finally, set up email forwarding to the support email
          forward_to_email = "support@#{domain_alias}"
          GAMEmailForwardingJob.enqueue_in(
            5.minutes,
            current_account.id, current_user.id,
            @access_token, @domain, params[:owner], params[:email], forward_to_email
          )

          # Create a ticket to notify user that email forwarding is still being setup
          begin
            Ticket.generate_email_forward_pending_ticket(current_account, current_user)
          rescue => e
            ZendeskExceptions::Logger.record(e, location: self, message: "Could not create sample ticket.", fingerprint: '20b3cfe4f7c0ab0e8632cec7b80b624a88f67e08')
          end

          head 204
        end

        allow_parameters :group_email_forwarding_setup, email: Parameters.string
        require_oauth_scopes :group_email_forwarding_setup, scopes: [:write], arturo: true
        def group_email_forwarding_setup
          forward_to_email = "support@#{zendesk_subdomain}"

          # Add our support address to the list of group members
          response = gam_client.add_group_member(group_email: params[:email], member_email: forward_to_email)
          return if render_error(response, ignore_duplicate: true)

          # Update the group settings, since by default, they can't receive emails sent from the public
          # Note that this is only possible if the following Groups for Business avanced setting
          # is checked: "Group owners can allow incoming email from outside this domain"
          response = gam_client.enable_public_group_emails(group_email: params[:email])
          return if render_error(response, ignore_external: true)

          # Create new recipient address
          create_recipient_address(default: true)

          head 204
        end

        ## Google will access this endpoint for site verification
        allow_parameters :google_site_verification, token: Parameters.string
        require_oauth_scopes :google_site_verification, scopes: [:read], arturo: true
        def google_site_verification
          token_param = params[:token]
          head(400) && return unless token_param =~ /^[a-z0-9]+$/
          head(404) && return unless current_account

          current_account.on_shard do
            token_setting = current_account.settings.google_site_verification_token
            unless token_setting
              Rails.logger.info "google_site_verification: could not find token in account settings"
            end
            token_setting ||= redis_client.get(google_site_verification_token_redis_key(current_account))
            unless token_setting
              Rails.logger.info "google_site_verification: could not find token in redis"
            end
            unless @token == token_setting
              Rails.logger.info "google_site_verification: tokens do not match #{@token} != #{token_setting}"
            end

            @token = "google#{token_param}.html"
            head(400) && return unless @token == token_setting
            render '/verification/google_site_verification', layout: false, content_type: 'text/plain'
          end
        end

        allow_parameters :qualifies_for_google_apps, {}
        require_oauth_scopes :qualifies_for_google_apps, scopes: [:write], arturo: true
        def qualifies_for_google_apps
          key = google_apps_domain_redis_key(current_account)
          cached = redis_client.get(key)
          unless cached.nil?
            return render json: cached.present?
          end

          domain_or_nil = eligible_google_apps_domain
          redis_client.setex(key, QUALIFIES_GAM_EXPIRATION, domain_or_nil)
          render json: domain_or_nil.present?
        end

        allow_parameters :enable_google_apps,
          return_to: Parameters.string
        require_oauth_scopes :enable_google_apps, scopes: [:write], arturo: true
        def enable_google_apps
          if current_account.has_google_apps?
            return render json: { error: 'AlreadyEnabled', description: 'Account already has G Suite integration' }, status: :unprocessable_entity
          end

          google_domain = eligible_google_apps_domain
          key = google_apps_domain_redis_key(current_account)
          redis_client.setex(key, QUALIFIES_GAM_EXPIRATION, google_domain)
          unless google_domain.present?
            return render json: { error: 'DoesNotQualify', description: 'Account does not qualify for G Suite integration' }, status: :unprocessable_entity
          end

          current_account.enable_google_apps!(google_domain)
          subdomain = current_account.subdomain

          return_to = params[:return_to]
          gam_params = {
            subdomain: subdomain,
            source: 'activate-google-apps',
            hd: google_domain
          }
          gam_params[:return_to] = return_to if return_to
          gam_nav_link = Zendesk::Auth::Warden::GoogleAppMarketOAuth2Strategy.gam_initiation_url(gam_params)
          session['enabled_google_apps'] = true
          render json: gam_nav_link.to_json
        end

        # Used by QA to nil route.gam_domain in order to allow new GAM accounts
        # to use the same G Suite domain
        # This endpoint is only valid in non-production environments
        allow_parameters :unbind_google_apps_domain,
          domain: Parameters.string
        require_oauth_scopes :unbind_google_apps_domain, scopes: [:write], arturo: true
        def unbind_google_apps_domain
          head(400) && return if Rails.env.production?

          domain = params[:domain]
          return unless domain.present?
          if Route.exists?(gam_domain: domain)
            route = Route.where(gam_domain: domain).first.reload
            route.update_attribute(:gam_domain, nil)
          end

          head 200
        end

        # Used by QA to delete all domain aliases from the G Suite
        # account since there is a maximum number of domains/domain aliases
        # This endpoint is only valid in non-production environments
        allow_parameters :purge_google_apps_domain_aliases,
          domain: Parameters.string,
          customer_id: Parameters.string
        require_oauth_scopes :purge_google_apps_domain_aliases, scopes: [:write], arturo: true
        def purge_google_apps_domain_aliases
          head(400) && return if Rails.env.production?

          customer_id = params[:customer_id]
          if @access_token.present? && customer_id.present?
            response = gam_client.get_domain_aliases(customer_id: customer_id)
            response[:domainAliases].map do |domain_alias|
              alias_name = domain_alias[:domainAliasName]
              # Skip deletion of the default domain alias
              unless DEFAULT_DOMAIN_ALIAS.match?(alias_name)
                GsuiteDeleteDomainAliasJob.enqueue(@access_token, @domain, customer_id, alias_name)
              end
            end
          else
            Rails.logger.warn "purge_google_apps_domain_aliases: insufficient parameters. access_token: #{@access_token}, customer_id: #{customer_id}"
          end

          head 200
        end

        private

        def capture_required_params
          @access_token = session['google_token']
          @domain = params[:domain]
        end

        def subdomain_label
          @subdomain_label ||= @domain.split('.').first
        end

        def zendesk_subdomain
          "#{current_account.subdomain}.#{Zendesk::Configuration.fetch(:host)}"
        end

        def gam_client
          @gam_client ||= Zendesk::Google::GAMClient.new(
            access_token: @access_token,
            domain: @domain
          )
        end

        def render_error(response, ignore_duplicate: false, ignore_external: false)
          return false unless response[:error]

          if response[:error][:errors]
            error = response[:error][:errors].first

            # Ignore duplicate group members and domain aliases. Don't ignore the error if the
            # duplicate domain belongs to another customer
            if ignore_duplicate && error[:reason] == "duplicate" && error[:message] != "Domain already exists in some other Customer."
              return false
            end

            # Ignore errors caused by "Group owners can allow incoming email from outside this domain" being unchecked
            if ignore_external && error[:reason] == "invalid" && error[:message].include?('Cannot allow external entities to post.')
              return false
            end
          end

          render json: response.to_json, status: response[:error][:code]
          true
        end

        def get_and_write_site_verification_token(domain:)
          domain ||= zendesk_subdomain
          response = gam_client.get_site_verification_token(domain: domain)

          token = response[:token]
          return response unless token

          current_account.settings.google_site_verification_token = token
          current_account.settings.save!
          key = google_site_verification_token_redis_key(current_account)
          redis_client.setex(key, 1.day.to_i, token)
          Rails.logger.info "get_and_write_site_verification_token: saving token #{token} to acct settings & redis for key: #{key}"

          response
        end

        def verify_site_token(domain:)
          domain ||= zendesk_subdomain
          gam_client.verify_site_token(domain: domain)
        end

        def create_recipient_address(default: false)
          address = current_account.recipient_addresses.build(
            email: params[:email],
            default: default,
            name: (current_brand || current_account).name,
            brand: current_brand
          )
          # Prevent verification email from being sent
          address.creating_for_new_brand = true
          begin
            address.save!
          rescue => e
            ZendeskExceptions::Logger.record(e, location: self, message: "Could not create recipient address.", fingerprint: '48a0dbcb8d677957d31097f733ed3e75157073fc')
            statsd_client.increment('count', tags: ["status:error", "method:#{__method__}", "error:#{e.class}"])
          end
        end

        def statsd_client
          @client ||= Zendesk::StatsD.client(namespace: self.class.name.snakecase)
        end

        def redis_client
          Zendesk::RedisStore.client
        end

        def google_site_verification_token_redis_key(account)
          "account_#{account.id}_site_verification_token"
        end

        def google_apps_domain_redis_key(account)
          "account_#{account.id}_qualifies_for_google_apps"
        end

        def eligible_google_apps_domain
          return unless current_account && current_account.subscription &&
            !current_account.has_multiple_brands? &&
            !current_account.hub? && !current_account.spoke?

          # 1. Ensure account is currently using an applicable login method. Ignore remote auth accounts for now
          valid_login_method = current_account.role_settings.agent_zendesk_login || current_account.role_settings.agent_google_login
          unless valid_login_method
            Rails.logger.info 'agent remote login cannot be enabled'
            return
          end

          # 2. Find qualifying emails with G Suite MX records
          google_apps_domains = qualifying_google_apps_domains

          # If owner has zero or multiple G Suite domains, account does not qualify for GAM
          unless google_apps_domains.count == 1
            Rails.logger.info "owner does not have one valid G Suite email, but has #{google_apps_domains.count}"
            return
          end

          # 3. Verify google domain isn't being used with another zendesk instance
          google_domain = google_apps_domains.first

          return if google_domain == 'zendesk.com'

          if Route.exists?(gam_domain: google_domain)
            route = Route.where(gam_domain: google_domain).first.reload
            if route != current_account.route
              Rails.logger.info 'gam_domain already in use'
              return
            end
          end

          # 4. Ensure all admins & agents have at least one email that's G Suite
          agent_subquery = current_account.agents.select('id').reorder('').to_sql
          identities = UserEmailIdentity.select('user_id, value').
            joins("JOIN (#{agent_subquery}) as a ON a.id = user_identities.user_id").reorder('')

          num_agents = identities.map(&:user_id).uniq.length
          num_agents_with_google_domain = identities.select { |i| i.value.split('@').last == google_domain }.
            map(&:user_id).uniq.length

          unless num_agents == num_agents_with_google_domain
            Rails.logger.info 'not all admins and agents are on the same G Suite domain'
            return
          end

          google_domain
        end

        def qualifying_google_apps_domains
          emails = current_account.owner.identities.where(type: UserEmailIdentity.name).pluck(:value).map { |e| e.split('@') }
          # Remove emails with aliases (+ sign) or gmail emails
          emails = emails.reject { |email_split| email_split.first.include?('+') || email_split.last == 'gmail.com' }
          # Check emails for GoogleApps MX records
          emails.map(&:last).select do |domain|
            mx_handlers = Resolv::DNS.open.getresources(domain, Resolv::DNS::Resource::IN::MX)
            mx_handlers.any? do |host|
              case host.exchange.to_s.downcase
              when /google/ then true
              end
            end
          end.compact
        end
      end
    end
  end
end
