module Api
  module V2
    module Internal
      class EmailsController < Api::V2::Internal::BaseController
        include Zendesk::StaffServiceControllerSupport

        email_parameters = Parameters.map(
          type: Parameters.string,
          value: Parameters.string,
          verified: Parameters.boolean # From logging, undocumented, untested
        )

        allow_parameters :create,
          email: email_parameters,
          requester_id: Parameters.bigid,
          staff_id: Parameters.bigid
        def create
          new_email.save!
          render json: presenter.present(new_email), status: :created
        end

        allow_parameters :update,
          id: Parameters.bigid,
          email: email_parameters,
          requester_id: Parameters.bigid,
          staff_id: Parameters.bigid
        def update
          value = params[:email][:value]
          email.update_attributes!(value: value) if value
          email.verify! if params[:email][:verified] && !email.is_verified?

          render json: presenter.present(email)
        end

        allow_parameters :destroy,
          id: Parameters.bigid,
          requester_id: Parameters.bigid,
          staff_id: Parameters.bigid
        def destroy
          if email.destroy
            render json: presenter.present(email)
          else
            render json: presenter.present_errors(email), status: :unprocessable_entity
          end
        end

        allow_parameters :deliver_verify,
          email_id: Parameters.string,
          requester_id: Parameters.bigid,
          staff_id: Parameters.string
        def deliver_verify
          user = current_account.users.find(params[:staff_id])
          email = user.identities.find(params[:email_id])

          UsersMailer.deliver_verify_multiproduct_staff(email)
          head :ok
        end

        private

        def user
          @user ||= current_account.users.find(params[:staff_id])
        end

        def email
          @email ||= user.identities.find(params[:id])
        end

        def new_email
          @new_email ||= initializer.identity
        end

        def initializer
          @initializer ||= Zendesk::Users::Identities::Initializer.new(current_account, requester, user, params[:email])
        end

        def presenter
          @presenter ||= Api::V2::Internal::EmailsPresenter.new(requester, url_builder: self)
        end
      end
    end
  end
end
