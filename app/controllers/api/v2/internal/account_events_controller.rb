module Api::V2::Internal
  class AccountEventsController < Api::V2::Internal::BaseController
    allow_subsystem_user :account_service
    allow_subsystem_user :billing

    ACCOUNT_UPDATED = 'account_updated'.freeze
    PRODUCT_CREATED = 'product_created'.freeze
    PRODUCT_UPDATED = 'product_updated'.freeze
    ROUTE_UPDATED   = 'route_updated'.freeze
    OWNER_CHANGED   = 'owner_changed'.freeze
    ACCOUNT_NAME_CHANGED = 'account_name_changed'.freeze
    ACCOUNT_SUBDOMAIN_CHANGED = 'account_subdomain_changed'.freeze
    ACCOUNT_CANCELLED = 'account_cancelled'.freeze
    ACCOUNT_REACTIVATED = 'account_reactivated'.freeze
    FRAUD_JOB_DELAY = ENV.fetch('FRAUD_JOB_DELAY_SECONDS', 10).seconds

    RETRYABLE_ERRORS = [
      ActiveRecord::ConnectionNotEstablished,
      ActiveRecord::ConnectionTimeoutError,
      Faraday::TimeoutError
    ].freeze

    CHAT_PHASE_III = 3
    CHAT_PHASE_IV = 4

    allow_parameters :create,
      account_id:            Parameters.bigid,
      id:                    Parameters.bigid,
      name:                  Parameters.string,
      new_owner_id:          Parameters.bigid,
      new_account_name:      Parameters.string,
      new_account_subdomain: Parameters.string,
      notification_type:     Parameters.string,
      old_owner_id:          Parameters.bigid,
      old_account_name:      Parameters.string,
      old_account_subdomain: Parameters.string,
      product_name:          Parameters.string,   # from logs, untested & undocumented
      trial_expires_at:      Parameters.datetime, # from logs, untested & undocumented
      destination:           Parameters.map(      # from logs, untested & undocumented
        pod_id:              Parameters.integer,
        region:              Parameters.string,
        shard_id:            Parameters.integer
      ),
      source:                Parameters.map(      # from logs, untested & undocumented
        pod_id:              Parameters.integer,
        region:              Parameters.string,
        shard_id:            Parameters.integer
      )
    def create
      Rails.logger.info("Change notification received: #{params}.")
      case notification_type
      when ACCOUNT_UPDATED
        response = clear_cache_status
        Rails.logger.info("Account with ID #{account_id} updated.")
        enqueue_fraud_score_job if should_enqueue_fraud_score_job?
        return response
      when PRODUCT_CREATED
        Rails.logger.info("Product with ID #{product_id} created.")
        current_account.start_support_trial if support_trial_ready?
        enqueue_guide_account_sync_job if should_enqueue_guide_account_sync_job?
      when PRODUCT_UPDATED
        Rails.logger.info("Product with ID #{product_id} updated.")
        expire_answer_bot_cache if current_account.has_automatic_answers_enabled?
        enqueue_guide_account_sync_job if should_enqueue_guide_account_sync_job?
        enqueue_explore_account_sync_job if should_enqueue_explore_account_sync_job?
        enqueue_talk_account_sync_job if should_enqueue_talk_account_sync_job?
        downgrade_light_agents if light_agent_addon_updated?
        update_spp_account if should_update_spp_account?
        return product_sync_status
      when ROUTE_UPDATED
        Rails.logger.info("Route with ID #{product_id} updated.")
        # Not yet implemented
      when OWNER_CHANGED
        Rails.logger.info("Owner for account with ID #{account_id} has been changed.")
        return owner_changed_status
      when ACCOUNT_NAME_CHANGED
        Rails.logger.info("Name for account with ID #{account_id} has been changed from #{old_account_name.inspect} to #{new_account_name.inspect}.")
        return account_name_changed_status
      when ACCOUNT_SUBDOMAIN_CHANGED
        Rails.logger.info("Subdomain for account with ID #{account_id} has been changed from #{old_account_subdomain.inspect} to #{new_account_subdomain.inspect}.")
        return account_subdomain_changed_status
      when ACCOUNT_CANCELLED
        Rails.logger.info("Account with ID #{account_id} has been cancelled.")
        return account_cancelled_status
      when ACCOUNT_REACTIVATED
        Rails.logger.info("Account with ID #{account_id} has been reactivated.")
        return account_reactivated_status
      end

      head :ok
    end

    private

    def enqueue_explore_account_sync_job
      Omnichannel::ExploreAccountSyncJob.enqueue(account_id: current_account.id)
    end

    def enqueue_guide_account_sync_job
      Omnichannel::GuideAccountSyncJob.enqueue(account_id: current_account.id)
    end

    def should_enqueue_explore_account_sync_job?
      product_name == Zendesk::Accounts::Client::EXPLORE_PRODUCT
    end

    def should_enqueue_guide_account_sync_job?
      product_name == Zendesk::Accounts::Client::GUIDE_PRODUCT
    end

    def enqueue_talk_account_sync_job
      Omnichannel::TalkAccountSyncJob.enqueue(account_id: current_account.id)
    end

    def should_enqueue_talk_account_sync_job?
      product_name == Zendesk::Accounts::Client::TALK_PRODUCT
    end

    def downgrade_light_agents
      Zendesk::Accounts::LightAgentDowngrader.perform(account: current_account)
    end

    def light_agent_addon_updated?
      product_name == Zendesk::Accounts::Client::LIGHT_AGENT_ADDON
    end

    def update_spp_billing_id
      current_account.subscription_feature_addons.each do |addon|
        if current_account.is_trial?
          addon.boost_expires_at = nil
          # ensure a non-nil rate plan ID for trial accounts (NB: there is no uniqeness constraint)
          addon.zuora_rate_plan_id = 1
        else
          addon.zuora_rate_plan_id = account_client.zuora_billing_id unless addon.boost_expires_at
        end
        addon.save!
      end
    end

    def update_spp_features
      Zendesk::Features::SubscriptionFeatureService.new(current_account).execute!
    end

    def update_spp_subscription
      product = account_client.products.detect { |p| p.name == Zendesk::Accounts::Client::SUPPORT_PRODUCT.to_sym }
      return unless product
      current_account.subscription.tap do |s|
        s.trial_expires_on = product.trial_expires_at
        s.plan_type = product.plan_settings["plan_type"]
        s.base_agents = product.plan_settings["max_agents"]
      end.save!
    end

    delegate :active?, to: :support_product, prefix: true, allow_nil: true
    private :support_product_active?

    def support_product
      # if the support product itself was updated, we must not rely on cached values
      return super unless support_updated?
      current_account.products(use_cache: false).find { |product| product.name == Zendesk::Accounts::Client::SUPPORT_PRODUCT.to_sym }
    end

    def update_spp_account
      update_spp_subscription
      update_spp_features
      update_spp_billing_id
    rescue => e
      raise e if RETRYABLE_ERRORS.include?(e.class)
      msg = "Exception while processing support product (SPP) update."
      Rails.logger.error("#{msg}: #{e.message} - #{e.backtrace}")
      Rails.application.config.statsd.client.event(
        "Failed support product (SPP) sync",
        "#{msg} - #{truncate_exception_msg(e)}",
        alert_type: 'error',
        aggregation_key: 'failed_support_product_sync'
      )
    end

    def should_update_spp_account?
      product_name == Zendesk::Accounts::Client::SUPPORT_PRODUCT &&
      current_account.spp?
    end

    def set_owner_as_admin
      owner = current_account.users.find(owner_id)
      Zendesk::SupportUsers::User.new(owner).assign_role(Role::ADMIN.id)
    end

    def notification_type
      @notification_type ||= params[:notification_type]
    end

    def account_id
      @account_id ||= params[:account_id]
    end

    def product_id
      @product_id ||= params[:id]
    end

    def product_name
      @product_name ||= params[:name]
    end

    def old_account_name
      @old_account_name ||= params[:old_account_name]
    end

    def new_account_name
      @new_account_name ||= params[:new_account_name]
    end

    def old_account_subdomain
      @old_account_subdomain ||= params[:old_account_subdomain]
    end

    def new_account_subdomain
      @new_account_subdomain ||= params[:new_account_subdomain]
    end

    def enqueue_fraud_score_job
      Rails.cache.fetch("account_id_for_fraud_score_job:#{current_account.id}", expires_in: 5.minutes, race_condition_ttl: 20.seconds) do
        if current_account.has_orca_classic_replace_fraud_report_job_with_fraud_score_job?
          FraudScoreJob.account_creation_enqueue(current_account, Fraud::SourceEvent::SHELL_CREATED, FRAUD_JOB_DELAY)
        end
        Time.now.utc
      end
    end

    def should_enqueue_fraud_score_job?
      current_account.multiproduct? && current_account.new_shell_account?
    end

    # HACK: allows multiproduct accounts (with Chat) to deactivate ip restrictions if the plan type does not support it.
    # TODO: redo this once the global_feature_framework service is built.
    def deactivate_ip_restrictions!
      Rails.logger.info("Account #{account_id} losing ip restrictions due to plan change")
      current_account.settings.lookup(:ip_restriction_enabled).disable
    end

    def clear_cache_status
      construct_response_status do
        expire_account_client_account_cache
        current_account.clear_kasket_cache!
        current_account.reload
      end
    end

    def product_sync_status
      construct_response_status do
        current_account.start_support_trial if support_trial_ready_to_start?
        sync_chat_product if !current_account.has_synchronous_chat_provisioning? && chat_product_updated? && chat_phase_three?
        expire_answer_bot_cache if answer_bot_updated?
        expire_support_product_cache if support_updated?
        deactivate_ip_restrictions! if should_deactivate_ip_restrictions?
        current_account.downgrade_support_trial_specific_support_features! if should_downgrade_support_trial_specific_support_features?
        current_account.deactivate_agent_workspace if support_updated? && !support_product_active?
      end
    end

    def support_trial_ready_to_start?
      return false if current_account.is_sandbox?
      current_account.has_ocp_support_shell_account_creation? &&
        !current_account.has_accsrv_product_created_notification? &&
        support_updated? &&
        current_account.products&.map(&:name) == [:support] &&
        current_account.products.first.not_started?
    end

    def support_trial_ready?
      # sandboxes already have a support trial
      return false if current_account.is_sandbox?
      current_account.has_ocp_support_shell_account_creation? &&
        current_account.has_accsrv_product_created_notification? &&
        support_updated?
    end

    def owner_changed_status
      construct_response_status do
        set_owner_as_admin
      end
    end

    def account_name_changed_status
      construct_response_status do
        # Nb. Ideally this `AccountNamePropagator` could consolidate all the `Account#name` propagation,
        # but it has remained split in two because:
        # * `address, organizations & recipient_addresses` are changed on a `before_update` callback, whereas
        # * `brand` is changed on an `after_update` callback
        # so to be safe we'll keep them separated and call them both here...
        AccountNamePropagator.call(current_account, name: new_account_name, name_was: old_account_name)
        current_account.propagate_name_to_brand
      end
    end

    def account_subdomain_changed_status
      construct_response_status do
        AccountSubdomainPropagator.call(current_account, subdomain: new_account_subdomain, subdomain_was: old_account_subdomain)
      end
    end

    def account_cancelled_status
      construct_response_status do
        current_account.set_cancellation_date
      end
    end

    def account_reactivated_status
      construct_response_status do
        current_account.set_cancellation_date
      end
    end

    # `current_account.has_ip_restriction?` makes a call to Account Services for multiproduct accounts
    def should_deactivate_ip_restrictions?
      chat_product_updated? && current_account.settings.ip_restriction_enabled? && (chat_phase_four? || chat_phase_three?) && !current_account.has_ip_restriction?
    end

    def should_downgrade_support_trial_specific_support_features?
      support_updated? && current_account.has_downgrade_support_trial_specific_support_features? && support_trial_expired?
    end

    def support_trial_expired?
      current_account.support_expired? && current_account.subscription.past_expiration_date?
    end

    def construct_response_status
      yield
      head :ok
    rescue *RETRYABLE_ERRORS => e
      Rails.logger.error("Encountered a retryable error while processing #{notification_type} notification: #{e.message}.")
      head :internal_server_error
    rescue => e
      Rails.logger.error(
        "Encountered an unexpected error while processing #{notification_type} notification: #{e.message} - #{e.backtrace}."
      )
      Rails.application.config.statsd.client.event(
        "Unexpected change notification error",
        truncate_exception_msg(e),
        alert_type: 'error',
        aggregation_key: 'change_notification_error',
        tags: ['support']
      )
      head :ok
    end

    def chat_phase_three?
      chat_product_phase == CHAT_PHASE_III
    end

    def chat_phase_four?
      chat_product_phase == CHAT_PHASE_IV
    end

    def sync_chat_product
      if chat_product.active?
        ActiveRecord::Base.transaction do
          if no_current_chat_record?
            create_zopim_subscription
          elsif chat_record_present?
            update_zopim_subscription
          else
            msg = error_message_with_zopim_states("Error while processing chat product notification")
            Rails.application.config.statsd.client.event(
              'Failed chat product sync',
              msg,
              alert_type: 'error',
              aggregation_key: 'failed_chat_product_sync'
            )
          end
        end
      else
        current_account.zopim_subscription.try(:destroy)
      end
    rescue => e
      raise e if RETRYABLE_ERRORS.include?(e.class)
      record_chat_info_and_errors(e)
    end

    def no_current_chat_record?
      zopim_integration.nil? && zopim_subscription.nil?
    end

    def chat_record_present?
      zopim_integration.present? && zopim_subscription.present?
    end

    def create_zopim_subscription
      z = current_account.build_zopim_subscription(
        status: ZBC::Zopim::SubscriptionStatus::Active,
        plan_type: chat_plan_type,
        max_agents: chat_max_agents,
        trial_days: chat_trial_days
      )
      z.phase = chat_product_phase
      z.skip_chat_only_permission_set = chat_is_suite_trial?
      z.save!
    end

    def update_zopim_subscription
      if zopim_integration.phase_one? || zopim_integration.phase_two?
        Rails.logger.error("Cannot sync phase 3 with a phase #{zopim_integration.phase} for account ID #{current_account.id}.")
      else
        zopim_subscription.plan_type = chat_plan_type
        zopim_subscription.max_agents = chat_max_agents
        zopim_subscription.trial_days = chat_trial_days
        zopim_subscription.save!
      end
    end

    def chat_plan_type
      look_up_plan_type(chat_product.plan_settings['plan_type'])
    end

    def chat_max_agents
      chat_product.plan_settings['max_agents']
    end

    def chat_trial_days
      trial_expires_at = chat_product.trial_expires_at

      if trial_expires_at.present?
        days = (trial_expires_at - Date.today).to_i
        days > 0 ? days : 0
      else
        ZBC::Zopim::Subscription::TrialManagement::DEFAULT_TRIAL_LENGTH_IN_DAYS
      end
    end

    def chat_is_suite_trial?
      chat_product.trial? && chat_product.plan_settings['suite']
    end

    def record_chat_info_and_errors(exception)
      msg = error_message_with_zopim_states("Exception while processing chat product notification")
      Rails.logger.error("#{msg}: #{exception.message} - #{exception.backtrace}")
      Rails.application.config.statsd.client.event(
        "Failed chat product sync",
        "#{msg} - #{truncate_exception_msg(exception)}",
        alert_type: 'error',
        aggregation_key: 'failed_chat_product_sync'
      )
    end

    def truncate_exception_msg(error)
      "#{error.message} - #{error.backtrace.join(" , ")[0..256]}"
    end

    def error_message_with_zopim_states(additional_message)
      "#{additional_message}. #{zopim_chat_current_states}"
    end

    def zopim_chat_current_states
      {
        account: current_account.inspect,
        zopim_integration: loggable_zopim_integration.inspect,
        zopim_subscription: zopim_subscription.inspect,
        chat_product_record: chat_product.inspect
      }
    end

    def loggable_zopim_integration
      if zopim_integration.nil?
        {}
      else
        {
          created_at: zopim_integration.created_at,
          updated_at: zopim_integration.updated_at,
          external_zopim_id: zopim_integration.external_zopim_id,
          phase: zopim_integration.phase
        }
      end
    end

    def look_up_plan_type(plan_type)
      ZBC::Zopim::PlanType::TYPES.find { |p| p.plan_type == plan_type }.try(:name)
    end

    def zopim_subscription
      @zopim_subscription ||= current_account.zopim_subscription
    end

    def zopim_integration
      @zopim_integration ||= current_account.zopim_integration
    end

    def chat_product_updated?
      product_name == Zendesk::Accounts::Client::CHAT_PRODUCT
    end

    def expire_answer_bot_cache
      Rails.cache.delete(current_account.scoped_cache_key(:answer_bot))
      Rails.cache.delete(current_account.scoped_cache_key(:answer_bot_guide_plan))
    end

    def answer_bot_updated?
      product_name.in?([Zendesk::Accounts::Client::ANSWER_BOT_PRODUCT, Zendesk::Accounts::Client::GUIDE_PRODUCT])
    end

    def chat_product
      current_account.chat_product
    end

    def chat_product_phase
      chat_product.plan_settings['phase']
    end

    def support_updated?
      product_name == Zendesk::Accounts::Client::SUPPORT_PRODUCT
    end

    def expire_support_product_cache
      account_client.expire_product_cache(Zendesk::Accounts::Client::SUPPORT_PRODUCT)
    end

    def expire_account_client_account_cache
      account_client.expire_account_client_account_cache
    end

    def owner_id
      account_client.owner_id(use_cache: false)
    end

    def account_client
      @account_client ||= Zendesk::Accounts::Client.new(
        current_account,
        retry_options: {
          max: 3,
          interval: 2,
          exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS
        },
        timeout: 15
      )
    end
  end
end
