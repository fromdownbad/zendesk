module Api::V2::Internal
  class AddonBoostsController < Api::V2::Internal::BaseController
    before_action :require_future_boost_expires_at!, only: [:create, :update]
    before_action :require_available_feature_name!,  only: :create
    before_action :require_actionable_boost!,        only: [:update, :destroy]

    allow_subsystem_user :embeddable, only: [:index]

    CREATE_PARAMETERS = {
     name: Parameters.string,
     boost_expires_at: Parameters.datetime
    }.freeze

    UPDATE_PARAMETERS = {
      boost_expires_at: Parameters.datetime
    }.freeze

    ## ### Fetch the account's available add-ons for boosting (minus the ones
    ## ### that are already boosted)
    ##
    allow_parameters :index, :skip # TODO: make stricter
    def index
      render json: presenter.collection_presenter.present(available_addons)
    end

    ## ### Create an account's boosted add-on
    ##
    allow_parameters :create, subscription_feature_addon: CREATE_PARAMETERS
    def create
      addon_boost = current_account.subscription_feature_addons.new(params[:subscription_feature_addon])
      Zendesk::SupportAccounts::Product.retrieve(current_account).update_plan_settings!(addon_boost)
      render json: presenter.present(addon_boost), status: :created
    end

    ## ### Remove an account's boosted add-on
    ##
    allow_parameters :destroy, id: Parameters.bigid | Parameters.string
    def destroy
      if SubscriptionFeatureAddon.destroy(params[:id])
        default_delete_response
      else
        raise ActiveRecord::RecordNotFound
      end
    end

    ## ### Update account's boosted add-ons boost_expires_at
    ##
    allow_parameters :update, id: Parameters.bigid | Parameters.string, subscription_feature_addon: UPDATE_PARAMETERS
    def update
      addon.assign_attributes(params[:subscription_feature_addon])
      Zendesk::SupportAccounts::Product.retrieve(current_account).update_plan_settings!(addon)
      render json: presenter.present(addon)
    end

    private

    def catalog
      Zendesk::Features::CatalogService.new(current_account).current_catalog
    end

    def available_addons
      return [] if current_account.subscription.nil?

      @available_addons ||= catalog_boostable_addons.each_with_object([]) do |addon, result|
        result << SubscriptionFeatureAddon.new(name: addon.name.to_s) unless purchased_or_boosted_addons.include?(addon.name.to_s)
      end
    end

    def purchased_or_boosted_addons
      @purchased_or_boosted_addons ||= current_account.
        subscription_feature_addons.
        non_secondary_subscriptions.
        map(&:name).
        to_set
    end

    def catalog_boostable_addons
      catalog.addons_for_plan(plan_type_service.proposed_plan_type).select(&:boostable?)
    end

    def plan_type_service
      @plan_type_service ||= Zendesk::Features::PlanTypeService.new(current_account)
    end

    def presenter
      @presenter ||= Api::V2::Internal::AddonBoostPresenter.new(current_user, url_builder: self)
    end

    def require_future_boost_expires_at!
      return if valid_boost_expires_at?
      render status: :unprocessable_entity, json: { error: 'boost add-on requires a valid (future) expiry date' }
    end

    def valid_boost_expires_at?
      return unless time = params.dig(:subscription_feature_addon, :boost_expires_at).presence
      DateTime.now < time
    end

    def require_available_feature_name!
      return if valid_feature_name?
      render status: :unprocessable_entity, json: { error: 'boost add-on requires an available and boostable feature name' }
    end

    def valid_feature_name?
      return unless name = params[:subscription_feature_addon][:name].presence
      available_addons.map(&:name).include?(name)
    end

    def require_actionable_boost!
      return if valid_boost?
      render status: :unprocessable_entity, json: { error: 'cannot modify purchased add-on' }
    end

    def valid_boost?
      params[:id].present? && addon && addon.is_boosted?
    end

    def addon
      @addon ||= current_account.subscription_feature_addons.find(params[:id])
    end
  end
end
