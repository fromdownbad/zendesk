module Api
  module V2
    module Internal
      class PredictionSettingsController < Api::V2::Internal::BaseController
        ## ### Show the current account's triggers that contains an action with deflection.
        ## `GET /api/v2/internal/prediction_settings/automatic_answers_triggers`
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem users
        ##
        ## #### Example Response
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "prediction_settings": {
        ##     "deflection_triggers": [
        ##       {
        ##         "id": 5555,
        ##         "title": "Another Deflection Trigger",
        ##         "permalink": "https://support.zd-dev.com/admin/settings/triggers/5555",
        ##         "usage_30d": 10
        ##       },
        ##       {
        ##         "id": 123455,
        ##         "title": "A Deflection Trigger",
        ##         "permalink": "https://support.zd-dev.com/admin/settings/triggers/123455",
        ##         "usage_30d": 4
        ##       }
        ##     ],
        ##     "potential_deflection_triggers": [
        ##       {
        ##         "id": 99,
        ##         "title": "Another potential deflection trigger",
        ##         "permalink": "https://support.zd-dev.com/admin/settings/triggers/99",
        ##         "usage_30d": 8
        ##       },
        ##       {
        ##         "id": 666,
        ##         "title": "A potential deflection trigger",
        ##         "permalink": "https://support.zd-dev.com/admin/settings/triggers/666",
        ##         "usage_30d": 0
        ##       }
        ##     ]
        ##   }
        ## }
        ##
        ## ```
        allow_parameters :automatic_answers_triggers, {}
        def automatic_answers_triggers
          render json: presenter.present(deflection_triggers, potential_deflection_triggers)
        end

        private

        def presenter
          @presenter ||= Api::V2::Internal::PredictionSettingsPresenter.new
        end

        def deflection_triggers
          triggers = active_triggers.select(&method(:contains_deflection?))
          triggers_with_usage = triggers_with_monthly_usage(triggers)

          sort_triggers_by_monthly_usage(triggers_with_usage)
        end

        def potential_deflection_triggers
          triggers = active_triggers.select(&method(:potential_for_deflection?))
          triggers_with_usage = triggers_with_monthly_usage(triggers)

          sort_triggers_by_monthly_usage(triggers_with_usage)
        end

        def contains_deflection?(trigger)
          trigger.definition.actions.any? do |action|
            action.source == 'deflection'
          end
        end

        def potential_for_deflection?(trigger)
          contains_notification_user?(trigger) && contains_ticket_is_created?(trigger)
        end

        def contains_notification_user?(trigger)
          trigger.definition.actions.any? do |action|
            action.source == 'notification_user' && (action.value.first == 'requester_id' || action.value.first == 'requester_and_ccs')
          end
        end

        def contains_ticket_is_created?(trigger)
          conditions = trigger.definition.conditions_all + trigger.definition.conditions_any

          conditions.any? do |condition|
            condition.source == 'update_type' && condition.operator == 'is' && condition.value == ['Create']
          end
        end

        def active_triggers
          current_account.triggers.active
        end

        def sort_triggers_by_monthly_usage(triggers)
          triggers.sort { |trigger_a, trigger_b| trigger_b.usage.monthly <=> trigger_a.usage.monthly }
        end

        def triggers_with_monthly_usage(triggers)
          execution_counter.rules_with_execution_counts(:monthly, triggers)
        end

        def execution_counter
          @execution_counter ||= Zendesk::RuleSelection::ExecutionCounter.new(
            account: current_account,
            type: :trigger
          )
        end
      end
    end
  end
end
