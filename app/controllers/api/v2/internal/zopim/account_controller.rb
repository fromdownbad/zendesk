module Api
  module V2
    module Internal
      module Zopim
        class AccountController < Api::V2::BaseController
          require_zopim_user

          # ### Show the current account
          # `GET /api/v2/internal/zopim/account.json`
          #
          # #### Allowed For
          #
          #  * Zopim system bearer user
          #    See lib/zendesk/auth/system_bearer_users.rb
          #
          # #### Example Response
          # ```http
          # Status: 200 OK
          #
          # {
          #   "zopim": {
          #     "id": "123456"
          #   }
          # }
          # ```
          allow_parameters :show, {}
          def show
            render json: presenter.present(current_account)
          end

          protected

          def presenter
            Api::V2::Internal::ZopimPresenter.new(current_user, url_builder: self)
          end
        end
      end
    end
  end
end
