module Api
  module V2
    module Internal
      module Zopim
        class SatisfactionRatingsController < Api::V2::BaseController
          require_zopim_user

          skip_before_action :verify_authenticity_token
          before_action :require_satisfaction_enabled!
          before_action :set_system_user_actor

          # ### Create a Satisfaction Rating
          # `POST /api/v2/internal/zopim/tickets/{ticket_id}/satisfaction_rating.json`
          #
          # This adds the ability for zopim to create a CSAT rating without having the ticket ever be solved
          # Essentially this makes it so Zopim bypasses the `#can_rate_satisfaction?` permissions
          # app/models/access/ticket_common.rb#L81-L90
          #
          # #### Allowed For:
          #
          #  * Zopim system bearer user
          #    See lib/zendesk/auth/system_bearer_users.rb
          #
          # #### Example Response
          #
          # ```http
          # Status: 200 OK
          #
          # {
          #   "satisfaction_rating": {
          #     "id":              35436,
          #     "url":             "https://company.zendesk.com/api/v2/satisfaction_ratings/35436.json",
          #     "assignee_id":     135,
          #     "group_id":        44,
          #     "requester_id":    7881,
          #     "ticket_id":       208,
          #     "score":           "good",
          #     "updated_at":      "2011-07-20T22:55:29Z",
          #     "created_at":      "2011-07-20T22:55:29Z",
          #     "comment":         { ... }
          #   }
          # }
          # ```
          allow_parameters :create, ticket_id: Parameters.bigid, satisfaction_rating: {
            score: Parameters.string,
            comment: Parameters.string | Parameters.nil
          }
          def create
            Zendesk::Tickets::Initializer.set_rating(ticket, params.require(:satisfaction_rating))
            ticket.will_be_saved_by(ticket.requester)
            ticket.save!
            render json: presenter.present(ticket.satisfaction_rating)
          end

          protected

          def ticket
            @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
          end

          def presenter
            @presenter ||= Api::V2::SatisfactionRatingPresenter.new(current_user, url_builder: self, includes: includes)
          end

          def set_system_user_actor
            CIA.current_actor = User.system
          end

          def require_satisfaction_enabled!
            unless current_account.has_customer_satisfaction_enabled?
              render_failure(status: :forbidden, title: 'Cannot create satisfaction ratings, setting is not enabled')
            end
          end
        end
      end
    end
  end
end
