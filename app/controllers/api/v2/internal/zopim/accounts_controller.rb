class Api::V2::Internal::Zopim::AccountsController < Api::V2::Internal::BaseController
  allow_subsystem_user :zopim

  # ### Show the zendesk account ID linked to the given zopim account ID
  # `GET /api/v2/internal/zopim/accounts/{id}.json`
  #
  # #### Allowed For
  #
  #  * Zopim system user
  #
  # #### Example Response
  # ```http
  # Status: 200 OK
  #
  # {
  #   "account_id": 123456
  # }
  # ```
  allow_parameters :show, id: Parameters.integer
  def show
    subscription = ZendeskBillingCore::Zopim::Subscription.find_by_zopim_account_id(params[:id])
    raise ActiveRecord::RecordNotFound if subscription.nil? || subscription.account.nil?
    render json: presenter.present(subscription.account)
  end

  protected

  def presenter
    @presenter ||= Api::V2::Internal::ZopimAccountPresenter.new(current_user, url_builder: self)
  end
end
