module Api
  module V2
    module Internal
      module Zopim
        class ChatLimitSettingsController < Api::V2::BaseController
          require_zopim_user

          # ### Update chat limit settings from Zopim dashboard
          # `PUT /api/v2/internal/zopim/chat_limit_settings.json`
          #
          # #### Allowed For
          #
          #  * Zopim system bearer user
          #    See lib/zendesk/auth/system_bearer_users.rb
          #
          # #### Example Response
          # ```http
          # Status: 200 OK
          #
          # ```
          allow_parameters :update, enabled:       Parameters.boolean,
                                    level:         Parameters.string,
                                    account_limit: Parameters.integer
          def update
            zopim_subscription.chat_limits_enabled = params[:enabled]
            zopim_subscription.chat_limit_level    = params[:level]
            zopim_subscription.account_chat_limit  = params[:account_limit]

            if zopim_subscription.save
              head :ok
            else
              render json: zopim_subscription.errors, status: :unprocessable_entity
            end
          end

          private

          def zopim_subscription
            current_account.zopim_subscription
          end
        end
      end
    end
  end
end
