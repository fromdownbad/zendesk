module Api
  module V2
    module Internal
      module Zopim
        class AgentController < Api::V2::BaseController
          before_action :require_owner_agent_eligible!,    only: :transfer_ownership

          # ### Downgrade the  Zopim Agents of current account's Zopim subscription
          # `put /api/v2/internal/zopim/agent/downgrade.json`
          #
          # #### Allowed For
          #
          #  * Zendesk system user
          #
          # #### Example Response
          ##
          ## ```http
          ## Status: 200 OK
          ## }
          ## ```
          allow_parameters :downgrade, excess_agent_count: Parameters.integer
          def downgrade
            downgradable_agent_identities.each do |identity|
              Zendesk::Users::AgentDowngrader.perform(agent: identity.user, products: [:chat])
            end
            head :ok
          end

          # ### Destroy the Zopim Agent of current account's Zopim subscription
          # `DELETE /api/v2/internal/zopim/agent.json`
          #
          # #### Allowed For
          #
          #  * Zendesk system user
          #
          # #### Example Response
          ##
          ## ```http
          ## Status: 204 No Content
          ## }
          ## ```
          allow_parameters :destroy, {}
          def destroy
            ::Zopim::Agent.destroy_agents(current_zopim_subscription.zopim_agents)
            default_delete_response
          end

          ## ### Transfer the ownership record of the account's Zopim Subscription.
          ##
          ## `POST /api/v2/internal/zopim/agent/transfer_ownership.json`
          ##
          ## This will transfer the ownership of the account's Zopim Subscription
          ## to the Zendesk Agent referenced by the user_id parameter.
          ##
          ## #### Allowed for
          ##
          ##  * allow_only_subsystem_user :zendesk
          ##
          ## #### Example Response
          ##
          ## ```http
          ## Status: 200 OK
          ## }
          ## ```
          allow_parameters :transfer_ownership, user_id: Parameters.bigid
          def transfer_ownership
            ::Zopim::Agent.transfer_ownership!(current_zopim_subscription, zendesk_user)
            head :ok
          rescue ::Zopim::Agents::Ownership::AgentsOutOfSyncError => e
            render status: :unprocessable_entity, json: { domain: 'Zopim', error: e.message }
          end

          protected

          def zendesk_user
            @zendesk_user ||= current_account.agents.find(params[:user_id])
          end

          def require_owner_agent_eligible!
            return if zendesk_user.is_admin? && !zendesk_user.zopim_identity.try(:is_owner?)
            render status: :unprocessable_entity, json: { domain: 'Zopim', error: 'RecipientNotEligible' }
          end

          def current_zopim_subscription
            @current_zopim_subscription ||= current_account.zopim_subscription || raise(ActiveRecord::RecordNotFound)
          end

          def downgradable_agent_identities
            current_zopim_subscription.zopim_agents.order('created_at desc').limit(params[:excess_agent_count])
          end
        end
      end
    end
  end
end
