module Api
  module V2
    module Internal
      class UserReportsController < Api::V2::Internal::BaseController
        allow_parameters :new, :skip
        def new
          render json: job_status_presenter.present(enqueue_job_with_status(BillingRelatedUserDataJob, params))
        end

        private

        def job_status_presenter
          @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
        end
      end
    end
  end
end
