module Api
  module V2
    module Internal
      class ZopimSubscriptionController < Api::V2::Internal::BaseController
        allow_subsystem_user :account_service
        allow_subsystem_user :embeddable, only: [:show]

        before_action :can_destroy_zopim_subscription!,  only: :reset
        before_action :require_broken_integration!,      only: [:restore_integration, :restore_phase_3_integration]
        before_action :require_owner_agent_eligible!,    only: :transfer_ownership

        CHAT_PHASE_III = 3
        CHAT_PHASE_III_DELETED = -1

        VALID_PHASES = [CHAT_PHASE_III, CHAT_PHASE_III_DELETED].freeze

        rescue_from ::ZendeskBillingCore::Zopim::Subscription::TrialManagement::Error do |e|
          render status: :unprocessable_entity, json: { domain: 'Zopim', error: e.to_s.demodulize }
        end

        product_type_params = Parameters.map(
          id: Parameters.bigid,
          account_id: Parameters.bigid,
          name: Parameters.string,
          state: Parameters.string,
          state_updated_at: Parameters.datetime,
          trial_expires_at: Parameters.datetime,
          plan_settings: Parameters.map(
            plan_type: Parameters.integer,
            max_agents: Parameters.integer,
            phase: Parameters.integer,
            suite: Parameters.boolean # From logging, undocumented
            # podless: Parameters.boolean, # From logging, ignored
            # "@class": Parameters.string, # From logging, ignored
            # seasonal_quantity: Parameters.integer, # From logging, ignored
            # agent_workspace: from logging, ignored
          ),
          # product_type: from logging, ignored
          active: Parameters.boolean,
          created_at: Parameters.datetime,
          updated_at: Parameters.datetime
        )

        ## ### Fetch the account's Zopim Subscription record.
        ##
        ## `GET /api/v2/internal/zopim_subscription.json`
        ##
        ## This will fetch the Zopim Subscription record of
        ## the account.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "zopim_subscription": {
        ##     "zopim_account_id":     891865,
        ##     "plan_type":            "trial",
        ##     "max_agents":           8,
        ##     "status":               "active",
        ##     "is_serviceable":       true,
        ##     "expired_trial":        false,
        ##     "last_synchronized_at": "2015-01-12T21:20:40Z",
        ##     "syncstate":            "ready",
        ##     ...
        ##   }
        ## }
        ## ```
        allow_parameters :show, {}
        def show
          render json: presenter.present(current_zopim_subscription)
        end

        ## ### Create a new phase three Zopim Subscription record.
        ##
        ## ```http
        ## POST /api/v2/internal/zopim_subscription/phase_three.json
        ##
        ## {
        ##   "product": {
        ##     "id": 1,
        ##     "account_id": 1,
        ##     "name": "chat"",
        ##     "state": "trial",
        ##     "state_updated_at": "2015-03-11T05:00Z",
        ##     "trial_expires_at: "2015-03-11T05:00Z",
        ##     "plan_settings": {
        ##        "plan_type": 1,
        ##        "max_agents": 100,
        ##        "phase": 3
        ##      },
        ##     "created_at": "2015-03-11T05:00Z",
        ##     "updated_at": "2015-03-11T05:00Z"
        ##   },
        ##   "user_id":  123456
        ## }
        ##
        ## ```
        ##
        ## If `user_id` is specified, a zopim identity will be created for the
        ## user if they are eligible for and do not already have one.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 204 No Content
        ## ```
        allow_parameters :create_phase_three,
          product: product_type_params,
          user_id: Parameters.bigid
        def create_phase_three
          product = Zendesk::Accounts::Product.new(params[:product])
          plan_type = chat_plan_type(product)
          max_agents = chat_max_agents(product)
          product_phase = chat_product_phase(product)
          user_id = params[:user_id]

          return head :bad_request if invalid_phase_three_parameters?(plan_type, max_agents, product_phase)
          return head :bad_request if product_phase == CHAT_PHASE_III_DELETED
          if chat_phase_one?
            Rails.logger.info("Cannot create a CP3 product record for existing Phase 1 customer with account_id: #{product.account_id}")
            return head :unprocessable_entity
          end

          if no_current_chat_record?
            Rails.logger.info("Provisioning new zopim records for chat phase three with the following info { plan_type: #{plan_type}, max_agents: #{max_agents}, chat_trial_days: #{chat_trial_days(product)}, user_id: #{user_id} }")
            provision_chat(product)

            if user_id.present?
              user = current_account.users.find_by_id(user_id)
              if user.present?
                begin
                  user.enable_chat_identity!
                  Rails.logger.info("Enabled zopim identity for user #{user_id}")
                rescue StandardError => e
                  Rails.logger.warn("Failed to enable zopim identity for user #{user_id}: #{e.message}")
                end
              else
                Rails.logger.warn("Failed to enable zopim identity for user #{user_id}: User not found")
              end
            end
          end

          head :no_content
        end

        ## ### Update a phase three Zopim Subscription record.
        ##
        ## ```http
        ## PUT /api/v2/internal/zopim_subscription/phase_three.json
        ##
        ## {
        ##   "product": {
        ##     "id": 1,
        ##     "account_id": 1,
        ##     "name": "chat"",
        ##     "state": "trial",
        ##     "state_updated_at": "2015-03-11T05:00Z",
        ##     "trial_expires_at: "2015-03-11T05:00Z",
        ##     "plan_settings": {
        ##        "plan_type": 1,
        ##        "max_agents": 100,
        ##        "phase": 3
        ##      },
        ##     "created_at": "2015-03-11T05:00Z",
        ##     "updated_at": "2015-03-11T05:00Z"
        ##   }
        ## }
        ##
        ## ```
        ##
        ## This will fetch the Zopim Subscription record of
        ## the account.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 204 No Content
        ## ```
        allow_parameters :update_phase_three,
          product: product_type_params
        def update_phase_three
          product = Zendesk::Accounts::Product.new(params[:product])
          plan_type = chat_plan_type(product)
          max_agents = chat_max_agents(product)
          product_phase = chat_product_phase(product)

          return head :bad_request if invalid_phase_three_parameters?(plan_type, max_agents, product_phase)

          if no_current_chat_record?
            provision_chat(product) unless product.deleted? || product.cancelled?
            head :no_content
          elsif zopim_integration.nil? || zopim_subscription.nil?
            head :unprocessable_entity
          else
            if product.deleted? || product.cancelled?
              zopim_subscription.skip_chat_product_sync = true
              zopim_subscription.destroy
            else

              if product.subscribed? && zopim_subscription.purchased_at.nil?
                zopim_subscription.purchased_at = DateTime.now
              elsif !product.subscribed? && zopim_subscription.purchased_at.present?
                zopim_subscription.purchased_at = nil
              end

              zopim_subscription.plan_type = chat_plan_type(product)
              zopim_subscription.max_agents = chat_max_agents(product)
              zopim_subscription.trial_days = chat_trial_days(product, zopim_subscription.created_at)
              zopim_subscription.skip_chat_product_sync = true
              zopim_subscription.save!
            end
            head :no_content
          end
        end

        ## ### Toggle the Zopim Subscription Status of account.
        ##
        ## `PUT /api/v2/internal/zopim_subscription/toggle_status.json`
        ##
        ## This will toggle the status from 'active' to 'cancelled' and
        ## vice-versa.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## ```http
        ## Status: 200 OK
        ## ```
        allow_parameters :toggle_status, {}
        def toggle_status
          status = current_zopim_subscription.is_active? ? :suspend! : :activate!
          current_zopim_subscription.send status
          head :ok
        end

        ## ### Reset the Zopim Subscription of the account.
        ##
        ## `DELETE /api/v2/internal/zopim_subscription/reset.json`
        ##
        ## This will delete the Zopim subscription and agent records of the
        ## account.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 204 No Content
        ## ```
        allow_parameters :reset, {}
        def reset
          current_zopim_subscription.destroy
          default_delete_response
        end

        ## ### Toggle the Zopim App Installation of the account.
        ##
        ## `PUT /api/v2/internal/zopim_subscription/toggle_app_installation.json`
        ##
        ## This will install or uninstall the Zopim App for the account
        ## depending on its current installation status.
        ##
        ## Note that this action will only successfully complete if the account
        ## is participating as a Zopim Phase II account.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## ```
        allow_parameters :toggle_app_installation, {}
        def toggle_app_installation
          if current_account.settings.polaris?
            Rails.logger.info("Skip Chat App (LM App) install/uninstall since account #{current_account.id} has Agent Workspace enabled")
            return head :not_modified
          end

          action = current_zopim_integration.app_installed? ? :uninstall_app : :install_app
          current_zopim_integration.send action
          head :ok
        end

        ## ### Restore the account's Zopim Integration record.
        ##
        ## `POST /api/v2/internal/zopim_subscription/restore_integration.json`
        ##
        ## This will recreate a missing Zopim Integration record.
        ##
        ## Note that this action will only successfully complete if the account
        ## is participating as a Zopim Phase II account.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## }
        ## ```
        allow_parameters :restore_integration, {}
        def restore_integration
          zopim_account = current_zopim_subscription.reseller_data
          current_account.create_zopim_integration!(
            external_zopim_id: zopim_account.id,
            zopim_key: zopim_account.account_key
          )
          head :ok
        end

        ## ### Restore the account's Zopim Integration record for phase 3 accounts.
        ##
        ## `POST /api/v2/internal/zopim_subscription/restore_phase_3_integration.json`
        ##
        ## This will recreate a missing Zopim Integration record.
        ##
        ## Note that this action will only successfully complete if the account
        ## is participating as a Zopim Phase III account.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## }
        ## ```
        allow_parameters :restore_phase_3_integration, zopim_key: Parameters.string
        def restore_phase_3_integration
          zopim_key = params[:zopim_key]
          return head :bad_request unless zopim_key
          current_account.create_zopim_integration!(
            external_zopim_id: current_account.id * -1,
            zopim_key: zopim_key,
            phase: 3
          )
          head :ok
        end

        ## ### Extend the account's Zopim Subscription trial date.
        ##
        ## `PUT /api/v2/internal/zopim_subscription/extend_trial_to_date.json`
        ##
        ## This will move/extend the account's Zopim Subscription trial expiration
        ## date to the date specified by the expire_date parameter.
        ##
        ## Note the the value of the expire_date parameter will be resolved into a
        ## Date value.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## }
        ## ```
        allow_parameters :extend_trial_to_date, expire_date: Parameters.datetime
        def extend_trial_to_date
          expire_date = params[:expire_date]
          current_zopim_subscription.extend_trial_to_date!(expire_date)
          head :ok
        end

        ## ### Restore the account's Zopim Subscription trial status.
        ##
        ## `PUT /api/v2/internal/zopim_subscription/restore_trial_status.json`
        ##
        ## This will restore the account's Zopim Subscription trial status and
        ## set a new expiration date as specified by the expire_date parameter.
        ##
        ## Note the the value of the expire_date parameter will be resolved into a
        ## Date value.
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## }
        ## ```
        allow_parameters :restore_trial_status, expire_date: Parameters.datetime
        def restore_trial_status
          expire_date = params[:expire_date]
          current_zopim_subscription.restore_trial_status!(expire_date)
          head :ok
        end

        ## ### Sets the phase of Zopim Subscription + Integration
        ##
        ## `PUT /api/v2/internal/zopim_subscription/restore_phase.json `
        ## --data { phase: 2 }
        ##
        ## This will convert the chat integration to either phase 2 or phase 3,
        ## and propagate that change to the chat record stored in the account_service.
        ##
        ## In addition to the phase field, the following fields will be
        ## copied over to the account_service:
        ## * max_agents
        ## * plan_type
        ## * state
        ## * status
        ##
        ## #### Allowed for
        ##
        ##  * allow_only_subsystem_user :zendesk
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## }
        ## ```
        allow_parameters :restore_phase, phase: Parameters.integer
        def restore_phase
          phase = Integer(params[:phase])
          valid_phases = 3

          return head :bad_request unless valid_phases == phase
          current_zopim_integration.phase = phase
          current_zopim_integration.save!

          current_zopim_subscription.make_phase_three

          head :ok
        end

        private

        def preload_associations
          # #provision_chat eventually calls `Account#save!`
          #
          # Rails starts a transactions when doing `Account#save!` and runs all the
          # validations and call-backs inside that transaction.
          #
          # `active_record_shards` doesn't use slave when in transaction on Account
          # hence we end-up reading from the master main Account DB, with slow calls
          # when running in 2 different geographic regions like pod15 (Australia) and
          # pod5 (US). This scenario, eventually, makes Zopim reach timeouts
          # because this controller doesn't answer within 5 seconds.
          #
          # With the following we pre-call some of the queries that will then be called
          # inside the transaction, by doing that we warm up activerecord cache so that
          # once inside the transaction we will not fire a transatlantic DB connection
          # but instead read from the local cache in memory.
          current_account.zopim_subscription
          current_account.subscription
          current_account.pre_account_creation
          current_account.route
        end

        def chat_plan_type(product)
          plan_type = product.plan_settings['plan_type']
          look_up_plan_type(plan_type.nil? ? nil : Integer(plan_type))
        end

        def chat_max_agents(product)
          max_agents = product.plan_settings['max_agents']
          max_agents.nil? ? nil : Integer(max_agents)
        end

        def chat_trial_days(product, trial_created_at = nil)
          starting_from = (trial_created_at || Date.today).to_date
          trial_expires_at = product.trial_expires_at

          if trial_expires_at.present?
            days = (trial_expires_at - starting_from).to_i
            days > 0 ? days : 0
          else
            ZBC::Zopim::Subscription::TrialManagement::DEFAULT_TRIAL_LENGTH_IN_DAYS
          end
        end

        def chat_product_phase(product)
          phase = product.plan_settings['phase']
          phase.nil? ? nil : Integer(phase)
        end

        def chat_is_suite_trial?(product)
          product.trial? && product.plan_settings['suite']
        end

        def look_up_plan_type(plan_type)
          ZBC::Zopim::PlanType::TYPES.find { |p| p.plan_type == plan_type }.try(:name)
        end

        def invalid_phase_three_parameters?(plan_type, max_agents, product_phase)
          plan_type.nil? || max_agents.nil? || product_phase.nil? || VALID_PHASES.exclude?(product_phase)
        end

        def chat_phase_one?
          zopim_integration.try(:phase_one?)
        end

        def no_current_chat_record?
          Rails.logger.info("ZopimIntegration exists: #{!zopim_integration.nil?}")
          Rails.logger.info("ZopimSubscription exists: #{!zopim_subscription.nil?}")
          zopim_integration.nil? && zopim_subscription.nil?
        end

        def zopim_subscription
          @zopim_subscription ||= current_account.zopim_subscription
        end

        def zopim_integration
          @zopim_integration ||= current_account.zopim_integration
        end

        def presenter
          Api::V2::Account::ZopimSubscriptionPresenter.new(
            current_user, url_builder: self, includes: includes
          )
        end

        def current_zopim_subscription
          @current_zopim_subscription ||= current_account.zopim_subscription || raise(ActiveRecord::RecordNotFound)
        end

        def current_zopim_integration
          @current_zopim_integration ||= current_zopim_subscription.zopim_integration
        end

        def zendesk_user
          @zendesk_user ||= current_account.agents.find(params[:user_id])
        end

        def require_owner_agent_eligible!
          return if zendesk_user.is_admin? && !zendesk_user.zopim_identity.try(:is_owner?)
          render status: :unprocessable_entity, json: { domain: 'Zopim', error: 'RecipientNotEligible' }
        end

        def require_broken_integration!
          return unless current_zopim_integration.present?
          render status: :unprocessable_entity, json: { domain: 'Zopim', error: 'IntegrationAlreadyExists' }
        end

        def can_destroy_zopim_subscription!
          return if cancelled_zopim_subscription? || unpurchased_zopim_subscription?
          render status: :unprocessable_entity, json: { domain: 'Zopim', error: 'CannotResetPurchasedZopimSubscription' }
        end

        def cancelled_zopim_subscription?
          current_zopim_subscription.is_cancelled?
        end

        def unpurchased_zopim_subscription?
          current_zopim_subscription.purchased_at.blank?
        end

        def provision_chat(product)
          preload_associations

          z = current_account.build_zopim_subscription(
            status: ZBC::Zopim::SubscriptionStatus::Active,
            plan_type: chat_plan_type(product),
            max_agents: chat_max_agents(product),
            trial_days: chat_trial_days(product)
          )
          z.purchased_at = product.subscribed? ? DateTime.now : nil
          z.phase = CHAT_PHASE_III
          z.skip_chat_product_sync = true
          z.skip_chat_only_permission_set = chat_is_suite_trial?(product)
          z.save!
        rescue StandardError => e
          Rails.logger.error("Failed to provision zopim records for the following reason: #{e.backtrace}")
          raise e
        end
      end
    end
  end
end
