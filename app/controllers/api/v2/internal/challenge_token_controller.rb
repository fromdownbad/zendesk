module Api
  module V2
    module Internal
      class ChallengeTokenController < Api::V2::Internal::BaseController
        ## The logins controller in zendesk_auth is making this API call to create challenge tokens
        ## https://github.com/zendesk/zendesk_auth/blob/master/app/controllers/zendesk/auth/v2/logins_controller.rb
        allow_parameters :create, ip_address: Parameters.string, session_key: Parameters.string, user_id: Parameters.bigid
        def create
          user = current_account.users.find(params[:user_id])
          challenge_token = user.challenge_tokens.create!(ip_address: params[:ip_address], shared_session_key: params[:session_key])

          render json: { challenge: challenge_token.value }
        end
      end
    end
  end
end
