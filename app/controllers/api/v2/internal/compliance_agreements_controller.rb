module Api
  module V2
    module Internal
      class ComplianceAgreementsController < Api::V2::Internal::BaseController
        allow_parameters :index, :skip
        def index
          render json: presenter.present(compliance_agreements)
        end

        allow_parameters :create, compliance_agreement: { compliance_id: Parameters.bigid }
        def create
          new_compliance.save!
          render json: presenter.present(new_compliance), status: :created
        end

        resource_action :destroy

        private

        def compliance_agreements
          @compliance_agreements ||= current_account.compliance_agreements
        end

        def compliance_agreement
          @compliance_agreement ||= compliance_agreements.find(params[:id])
        end

        def new_compliance
          @new_compliance ||= compliance_agreements.build do |ca|
            ca.compliance_id = params[:compliance_agreement][:compliance_id]
          end
        end

        def presenter
          @presenter ||= Api::V2::Internal::ComplianceAgreementPresenter.new(current_user, url_builder: self)
        end
      end
    end
  end
end
