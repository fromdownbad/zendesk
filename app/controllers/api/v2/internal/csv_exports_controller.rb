module Api
  module V2
    module Internal
      class CsvExportsController < Api::V2::Internal::BaseController
        skip_before_action :prevent_missing_account_access,
          :prevent_inactive_account_access, :verify_authenticated_session, only: [:accounts]

        allow_parameters :create, user_id: Parameters.bigid
        def create
          record = current_account.csv_export_records.create!(url: params[:url], user_id: user.id)
          url = csv_export_url(record, current_account.url_params)

          JobsMailer.deliver_job_complete(user, identifier, subject(user), BaseExportJob.message(url, user))

          head :ok
        end

        allow_parameters :accounts, {}
        require_oauth_scopes :accounts, scopes: [:read], arturo: true
        def accounts
          render json: { accounts: csv_accounts }
        end

        private

        def csv_accounts
          accounts = []
          account_scope = ::Account.active.exclude_sandbox.shard_unlocked.serviceable
          initial_exports = {}

          ActiveRecord::Base.on_all_shards do
            csv_export_records = latest_active_csv_export_records

            accounts += account_scope.shard_local.where(id: csv_export_records.map(&:account_id)).to_a

            csv_export_records.each do |record|
              next unless record.url.nil?
              initial_exports[record.account_id] = record.user_id
            end
          end

          accounts.map do |account|
            {
              id: account.id,
              subdomain: account.subdomain,
              initial_exporter: initial_exports[account.id]
            }
          end
        end

        # Finds the latest export record created with a distinct account_id
        def latest_active_csv_export_records
          CsvExportRecord.active.latest.select("csv_export_records.account_id, csv_export_records.user_id, csv_export_records.url")
        end

        def user
          @user ||= current_account.users.find(params[:user_id])
        end

        def subject(user)
          I18n.t('txt.admin.models.jobs.report_feed_job.subject', locale: user.translation_locale)
        end

        def identifier
          "account_id:#{current_account.id};csv_exporter"
        end
      end
    end
  end
end
