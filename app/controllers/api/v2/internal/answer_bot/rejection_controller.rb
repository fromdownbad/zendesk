module Api
  module V2
    module Internal
      module AnswerBot
        class RejectionController < Api::V2::Internal::BaseController
          before_action { head :not_found unless deflection }

          ## ### Reject a suggested article with a reason
          ## This endpoint provide a mechanism to update Ticket Deflection and Ticket Deflection Article
          ## when an article is marked as unhelpful (rejected) by the user.
          ##
          ## `POST /api/v2/internal/answer_bot/rejection`
          ##
          ## #### Request Format
          ##  * deflection_id: id of the deflection record
          ##  * article_id: id of the article that is rejected
          ##  * resolution_channel_id: the Zendesk via type id of the resolution channel
          ##  * reason: one of TicketDeflectionArticle reasons
          ##
          ## #### Allowed For
          ##  * Subsystem users
          ##
          allow_parameters :reject, deflection_id: Parameters.bigid,
                                    article_id: Parameters.bigid,
                                    resolution_channel_id: Parameters.integer,
                                    reason: Parameters.integer
          def reject
            ticket_deflector.process_deflection_rejection(
              article_id: params[:article_id],
              reason: params[:reason]
            )

            head :ok
          end

          private

          def deflection
            @deflection ||= current_account.ticket_deflections.find(params[:deflection_id])
          end

          def ticket_deflector
            @ticket_deflector ||= TicketDeflector.new(
              account: current_account,
              deflection: deflection,
              resolution_channel_id: params[:resolution_channel_id]
            )
          end
        end
      end
    end
  end
end
