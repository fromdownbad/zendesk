module Api
  module V2
    module Internal
      module AnswerBot
        class ViewedController < Api::V2::Internal::BaseController
          before_action { head :not_found unless ticket_deflection }
          before_action { head :bad_request unless params[:article_id] }

          ## ### Resolve deflection
          ##
          ## `POST /api/v2/internal/answer_bot/viewed`
          ##
          ## #### Request Format
          ##  * deflection_id: id of ticket deflection record
          ##  * article_id:    id of the article to record click_at
          ##
          ## #### Allowed For
          ##  * Subsystem users
          ##
          allow_parameters :viewed, deflection_id: Parameters.bigid,
                                    article_id: Parameters.bigid

          def viewed
            record_clicked_at(params[:article_id])

            head :ok
          end

          private

          def record_clicked_at(article_id)
            deflection_article = ticket_deflection.ticket_deflection_articles.where(article_id: article_id).first!

            deflection_article.update_clicked_at(Time.now)
          end

          def ticket_deflection
            current_account.ticket_deflections.find(params[:deflection_id])
          end
        end
      end
    end
  end
end
