module Api
  module V2
    module Internal
      module AnswerBot
        class DeflectionController < Api::V2::Internal::BaseController
          STATE_SOLVED = 'solve'.freeze
          STATE_IRRELEVANT = 'irrelevant'.freeze

          ## ### Create Ticket Delection and Ticket Deflection Articles Entries.
          ## If the ticket already has a ticket_deflection, this endpoint will return the existing TD Record.
          ## `POST /api/v2/internal/answer_bot/deflection/
          ##
          ## This internal API receives ticket_id, model_version and articles to creates the records in the database.
          ## The table is used for tracking deflections which is part of Answer Bot.
          ##
          ## #### Request Format
          ##  * ticket_id: nice_id of the ticket (bigid), optional
          ##  * enquiry: Text representing the question asked
          ##  * model_version: MegaMike model version (int)
          ##  * via_id: the Zendesk via type id of the prediction request
          ##  * deflection_channel_id: the Zendesk via type id of deflection channel
          ##  * deflection_channel_reference_id: reference for sub division of a channel (eg. webform_id OR trigger_id)
          ##  * brand_id: brand_id of the deflection
          ##  * language:  Language as determined by CB for prediction.
          ##  * articles: An array of JSON objects
          ##    * article_id: id of the recommended article
          ##    * brand_id:   id of the brand the article
          ##    * locale:     locale of the article
          ##    * score:      Prediction confidence score.
          ##    * clicked_at: Time when the user viewed the article
          ##    * title:      Title of the article
          ##    * url:        JSON url of the article
          ##    * html_url:   HTML url of the article
          ## #### Allowed For
          ##
          ##  * Subsystem users
          ##
          ## #### Example Response
          ## ```http
          ## Status: 200 OK
          ##
          ## {
          ##   "id":100154,
          ##   "auth_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50X2lkIjo5MDUzOCwidXNlcl9pZCI6MTk2NjIsInRpY2tldF9pZCI6MSwiYXJ0aWNsZXMiOlsyMDM2NjM3MzYsMjAzNjYzNzM3XSwidG9rZW4iOm51bGwsImV4cCI6MTUwMzAzNTg3Nn0.JrPqcOd3JlOYlnQU7PY5GbP6T4W5gY5RBLwzlAqLVSE",
          ##   "ticket_deflection_articles":[
          ##     {
          ##       "id":100181,
          ##       "article_id":203663736,
          ##       "brand_id":10101
          ##     },
          ##     {
          ##       "id":100182,
          ##       "article_id":203663737,
          ##       "brand_id":10101
          ##     }
          ##   ]
          ## }
          ## ```
          allow_parameters :create, ticket_id: Parameters.bigid,
                                    enquiry: Parameters.string,
                                    model_version: Parameters.integer,
                                    via_id: Parameters.integer,
                                    deflection_channel_id: Parameters.integer,
                                    deflection_channel_reference_id: Parameters.bigid,
                                    brand_id: Parameters.integer,
                                    language: Parameters.string,
                                    interaction_reference_type: Parameters.integer,
                                    interaction_reference: Parameters.string,
                                    articles: Parameters.array(Parameters.map(
                                      article_id: Parameters.bigid,
                                      brand_id: Parameters.bigid,
                                      locale: Parameters.string,
                                      score: Parameters.float,
                                      title: Parameters.string,
                                      url: Parameters.string,
                                      html_url: Parameters.string,
                                      clicked_at: Parameters.date_time
                                    ))
          def create
            if ticket_required?
              create_deflection_with_ticket
            else
              create_deflection_without_ticket
            end
          end

          ## ### Update Ticket Deflection and Ticket Deflection Articles
          ## This endpoint provide a mechanism to update Ticket Deflection and Ticket Deflection Article
          ## when ticket is solved on the client side.
          ##
          ## `POST /api/v2/internal/answer_bot/deflection/:id`
          ##
          ## #### Request Format
          ##  * id: id of ticket deflection record
          ##  * ticket_id: nice_id of the ticket (bigid)
          ##  * article_id: id of the article that resolved the request
          ##  * resolution_channel_id: the Zendesk via type id of the resolution channel
          ##
          ## #### Allowed For
          ##  * Subsystem users
          ##
          allow_parameters :update, id: Parameters.bigid,
                                    ticket_id: Parameters.bigid,
                                    article_id: Parameters.bigid,
                                    resolution_channel_id: Parameters.integer,
                                    state: Parameters.string,
                                    reason: Parameters.integer # one of TicketDeflectionArticle reasons
          def update
            return render json: {}, status: :not_found unless ticket && ticket_deflection
            return render json: { error: 'Mismatched Ticket and TicketDeflection.'}, status: :unprocessable_entity unless matching_ticket_and_deflection?

            state = params[:state]
            info = { article_id: params[:article_id] }

            case state
            when STATE_SOLVED
              ticket_deflector.solve(info)
            when STATE_IRRELEVANT
              ticket_deflector.process_deflection_rejection(info.merge(reason: params[:reason]))
            else
              valid_states = [STATE_SOLVED, STATE_IRRELEVANT]
              error_message = "Invalid state. State '#{state}' is not one of #{valid_states}."
              return render json: { error: error_message }, status: :unprocessable_entity
            end

            render json: {}, status: :ok
          end

          private

          def ticket_required?
            params[:ticket_id].present?
          end

          def create_deflection_without_ticket
            deflection = create_ticket_deflection

            auth_token = auth_token(
              account_id: current_account.id,
              requester_id: deflection.ticket.try(:requester_id),
              nice_id: deflection.ticket.try(:nice_id),
              ticket_deflection: deflection
            )

            render json: {
              id: deflection.id,
              auth_token: auth_token,
              ticket_deflection_articles: presentable_deflection_articles(deflection)
            }
          end

          def create_deflection_with_ticket
            unless ticket.present?
              head status: :unprocessable_entity
              return
            end

            deflection = answer_bot_for_agent? ? create_ticket_deflection : find_or_create_ticket_deflection

            auth_token = auth_token(account_id: current_account.id,
                                    requester_id: deflection.ticket.requester_id,
                                    nice_id: deflection.ticket.nice_id,
                                    ticket_deflection: deflection)

            render json: {
              id: deflection.id,
              auth_token: auth_token,
              ticket_deflection_articles:  presentable_deflection_articles(deflection)
            }
          end

          def find_or_create_ticket_deflection
            return ticket.ticket_deflection if ticket.ticket_deflection

            create_ticket_deflection
          end

          def create_ticket_deflection
            attrs = {
              user: ticket.try(:requester),
              ticket: ticket,
              enquiry: params.fetch(:enquiry),
              via_id: params.fetch(:via_id),
              deflection_channel_id: params.fetch(:deflection_channel_id),
              deflection_channel_reference_id: params.fetch(:deflection_channel_reference_id, nil),
              model_version: params.fetch(:model_version),
              brand_id: params.fetch(:brand_id),
              detected_locale: params.fetch(:language, 'en'),
              interaction_reference_type: params.fetch(:interaction_reference_type, nil),
              interaction_reference: params.fetch(:interaction_reference, nil)
            }

            deflection = current_account.ticket_deflections.new(attrs)

            deflection.save!

            create_deflection_articles(deflection)

            deflection
          end

          def create_deflection_articles(deflection)
            articles = params.fetch(:articles)

            if articles.blank? && ticket.present? && !answer_bot_for_agent?
              create_empty_deflection_tag(ticket)
            end

            unless articles.blank?
              articles.map { |article| build_article(deflection, article) }.
                each(&:save!)
              return if ticket.blank? || answer_bot_for_agent?

              create_successful_deflection_tag(ticket)
              create_send_event(articles_for_send_event(articles), ticket)
            end
          end

          def create_send_event(articles, ticket)
            ticket.will_be_saved_by(User.system)
            ticket.audit.events << ::AnswerBot::EventCreator.send_event(articles)

            ticket.save!
          end

          def articles_for_send_event(articles)
            articles.map do |article|
              article.merge(id: article[:article_id])
            end
          end

          def build_article(deflection, article)
            TicketDeflectionArticle.build_from_deflection_and_article(deflection, article)
          end

          def create_empty_deflection_tag(ticket)
            tag = 'ab_suggest_false'.freeze
            add_tag(ticket, tag)
          end

          def create_successful_deflection_tag(ticket)
            tag = 'ab_suggest_true'.freeze
            add_tag(ticket, tag)
          end

          def add_tag(ticket, tag)
            if Arturo.feature_enabled_for?(:answer_bot_tag_automation, current_account)
              ::AnswerBot::TicketDeflectionTaggingJob.enqueue(current_account.id, ticket.id, tag)
            end
          end

          def answer_requested_event(deflection)
            {
              brand_id: deflection.brand_id,
              ticket_id: ticket.try(:id),
              deflection_id: deflection.id,
              channel: Zendesk::Types::ViaType[deflection.via_id].name.downcase
            }
          end

          def article_suggested_event(deflection, article)
            {
              brand_id: article.brand_id,
              ticket_id: deflection.ticket_id,
              deflection_id: deflection.id,
              article_id: article.article_id,
              locale: article.locale,
              channel: Zendesk::Types::ViaType[deflection.via_id].name.downcase
            }
          end

          def presentable_deflection_articles(deflection)
            deflection.ticket_deflection_articles.map do |td|
              { id: td.id, article_id: td.article_id, brand_id: td.brand_id }
            end
          end

          def auth_token(account_id:, requester_id:, nice_id:, ticket_deflection:)
            AutomaticAnswersJwtToken.encrypted_token(account_id, requester_id, nice_id, ticket_deflection.id, ticket_deflection.ticket_deflection_articles.map(&:article_id).map(&:to_i))
          end

          def deflectability
            @deflectability ||= ::AnswerBot::Deflectability.new(
              account: current_account, ticket: ticket
            )
          end

          def ticket
            @ticket ||= current_account.tickets.find_by_nice_id(params[:ticket_id])
          end

          def ticket_deflection
            @ticket_deflection ||= current_account.ticket_deflections.find(params[:id])
          end

          def matching_ticket_and_deflection?
            ticket_deflection.ticket.id == ticket.id
          end

          def ticket_deflector
            @ticket_deflector ||= TicketDeflector.new(
              account: current_account,
              deflection: ticket.ticket_deflection,
              resolution_channel_id: params[:resolution_channel_id]
            )
          end

          def answer_bot_for_agent?
            params.fetch(:deflection_channel_id) == ViaType.ANSWER_BOT_FOR_AGENTS
          end
        end
      end
    end
  end
end
