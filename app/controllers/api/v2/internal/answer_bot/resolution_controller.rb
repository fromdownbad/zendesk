module Api
  module V2
    module Internal
      module AnswerBot
        class ResolutionController < Api::V2::Internal::BaseController
          before_action { head :not_found unless ticket_deflection }

          ## ### Resolve deflection
          ##
          ## `POST /api/v2/internal/answer_bot/resolution`
          ##
          ## #### Request Format
          ##  * deflection_id: id of ticket deflection record
          ##  * article_id:    id of the article that resolved the request
          ##  * resolution_channel_id: the Zendesk via type id of the resolution channel
          ##
          ## #### Allowed For
          ##  * Subsystem users
          ##
          allow_parameters :resolve, deflection_id: Parameters.bigid,
                                     article_id: Parameters.bigid,
                                     resolution_channel_id: Parameters.integer
          def resolve
            if ticket_deflection.ticket
              ticket_deflector.solve(article_id: params[:article_id])
            elsif answer_bot_for_web_widget?
              ticket_creation_solve
            else
              deflection_solve
            end

            head status: :accepted
          end

          private

          def answer_bot_for_web_widget?
            params[:resolution_channel_id] == ViaType.ANSWER_BOT_FOR_WEB_WIDGET
          end

          def deflection_solve
            ::AnswerBot::DeflectionSolveJob.enqueue(
              current_account.id,
              ticket_deflection.id,
              params[:article_id],
              params[:resolution_channel_id]
            )
          end

          def ticket_creation_solve
            ::AnswerBot::TicketCreationOnSolveJob.enqueue(
              current_account.id,
              ticket_deflection.id,
              params[:article_id],
              params[:resolution_channel_id]
            )
          end

          def ticket_deflector
            @ticket_deflector ||= TicketDeflector.new(
              account: current_account,
              deflection: ticket_deflection,
              resolution_channel_id: params[:resolution_channel_id]
            )
          end

          def ticket_deflection
            @ticket_deflection ||= current_account.ticket_deflections.find(params[:deflection_id])
          end
        end
      end
    end
  end
end
