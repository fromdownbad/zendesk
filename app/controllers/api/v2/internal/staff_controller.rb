module Api::V2::Internal
  class StaffController < Api::V2::Internal::BaseController
    include Zendesk::Users::ControllerSupport
    include Zendesk::StaffServiceControllerSupport
    before_action :require_not_owner, only: [:destroy]
    before_action :require_multiproduct_or_contributor_role, only: [:create]
    before_action :custom_created_must_be_in_past, only: [:create]
    skip_before_action :require_requester_id, only: [:owner_welcome]
    skip_before_action :prevent_missing_account_access, only: [:owner_welcome]

    OWNER_WELCOME_CACHE_EXPIRY = 60.minutes

    allow_parameters :create,
      user: {
        email: Parameters.string,
        locale_id: Parameters.bigid,
        name: Parameters.string,
        time_zone: Parameters.string,
        custom_created_at: Parameters.date_time,
        migrated_from: Parameters.string,
        skip_verify_email: Parameters.boolean # From logging, untested
      },
      requester_id: Parameters.bigid
    def create
      contributor_role = PermissionSet.enable_contributor!(current_account)
      params[:user][:custom_role_id] = contributor_role.id
      params[:user][:roles] = ::Role::AGENT.id
      new_staff = end_user_to_promote || current_account.users.new
      user_initializer.apply(new_staff)
      Zendesk::SupportUsers::User.new(new_staff).save!

      render json: presenter.present(new_staff), status: :created
    end

    allow_parameters :update,
      user: {
        locale_id: Parameters.bigid,
        name: Parameters.string,
        time_zone: Parameters.string
      },
      requester_id: Parameters.bigid,
      id: Parameters.bigid # From logging, untested
    def update
      user_initializer.apply(user)
      user.save!

      render json: presenter.present(user.reload)
    end

    allow_parameters :destroy,
      requester_id: Parameters.bigid,
      id: Parameters.bigid # From logging, untested
    def destroy
      user.delete!
      executor = current_account.users.find(params.fetch(:requester_id))

      user.mark_for_ultra_deletion!(executer: executor)
      render json: presenter.present(user)
    end

    ## ### Send a welcome email to the user
    ##
    ## PUT `/api/v2/internal/staff/owner_welcome`
    ##
    ## #### Example Request
    ##
    ## {
    ##   "account": {
    ##     "id": 382723
    ##   },
    ##   "user": {
    ##     "id": 35873443434
    ##   }
    ## }
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ## ```
    allow_parameters :owner_welcome,
      account: {
        id: Parameters.integer,
        shard_id: Parameters.bigid, # From logging, untested
        subdomain: Parameters.string # From logging, untested
      },
      user: {
        id: Parameters.bigid
      }
    def owner_welcome
      account_id = params[:account][:id]
      user_id = params[:user][:id]

      unless Rails.cache.read(owner_welcome_job_cache_key(account_id))
        # Not foolproof but should prevent duplicate jobs most of the time
        ::ShellOwnerWelcomeEmailJob.enqueue(account_id: account_id, user_id: user_id)
        Rails.cache.write(owner_welcome_job_cache_key(account_id), true, expires_in: OWNER_WELCOME_CACHE_EXPIRY)
      end

      begin
        current_account.clear_kasket_cache!
      rescue StandardError => e
        Rails.logger.warn("Failed to clear account cache #{e.message}")
      end

      head :ok
    end

    private

    def owner_welcome_job_cache_key(account_id)
      "owner-welcome-job-queued/#{account_id}"
    end

    def require_not_owner
      return if params[:id].to_i != current_account.owner_id
      render status: :unprocessable_entity,
             json: {
               error: 'CannotDeleteOwner',
               description: 'This staff is an owner and cannot be deleted'
              }
    end

    def require_multiproduct_or_contributor_role
      return if current_account.multiproduct? || current_account_has_contributor_role?
      render status: :unprocessable_entity,
             json: {
               error: 'MultiproductOrContributorRoleMissing',
               description: 'Account must have `multiproduct` set to true or have contributor role enabled'
              }
    end

    def custom_created_must_be_in_past
      return if params[:user][:custom_created_at].blank? || params[:user][:custom_created_at] <= Time.now.utc

      render status: :unprocessable_entity,
             json: {
               error: 'CustomCreatedAtInvalid',
               description: 'custom_created_at must be in the future'
             }
    end

    def current_account_has_contributor_role?
      current_account.permission_sets.where(role_type: PermissionSet::Type::CONTRIBUTOR).any?
    end

    def end_user_to_promote
      user = current_account.find_user_by_email(params[:user][:email])
      user if user&.is_end_user?
    end

    def user
      @user ||= current_account.all_users.find(params[:id]).tap do |u|
        u.current_user = requester
      end
    end

    # Overrides user_initializer in Zendesk::Users::ControllerSupport
    def user_initializer
      Zendesk::Users::Initializer.new(current_account, requester, params, true)
    end

    def presenter
      @presenter ||= Api::V2::Internal::StaffPresenter.new(requester, url_builder: self)
    end
  end
end
