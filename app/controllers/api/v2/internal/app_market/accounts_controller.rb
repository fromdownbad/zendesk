module Api
  module V2
    module Internal
      module AppMarket
        class AccountsController < Api::V2::Internal::BaseController
          ONBOARDING_SEGMENT_MAPPING = {
              "b2b" => "Businesses",
              "b2c" => "Customers",
              "internal_hr" => "Internal - HR",
              "internal_it" => "Internal - IT",
              "internal_other" => "Internal - Other"
            }.freeze

          INDUSTRY_MAPPING = {
            'Other' => [
              'energy', 'entertainment', 'healthcare', 'manufacturing', 'media',
              'marketing', 'real_estate', 'social_media', 'travel', 'other'
            ],
            'Education' => ['education', 'nonprofit'],
            'Prof Services' => ['support', 'financial'],
            'SaaS' => ['consultancy', 'software', 'web_apps', 'hosting'],
            'Retail' => ['retail']
          }.freeze

          allow_parameters :billable_agents, {}
          def billable_agents
            render json: { support: support_users }
          end

          allow_parameters :app_recommendations_info, {}
          def app_recommendations_info
            render json: { app_recommendations_info: {
                agent_count: current_account.agents.count,
                zendesk_plan: zendesk_plan,
                industry: industry,
                target_audience: target_audience,
                region: current_account.address.try(:country).try(:region)
              }}
          end

          private

          def industry
            survey_industry = current_account.survey_response.try(:industry)
            recommendations_industry = INDUSTRY_MAPPING.keys.find do |key|
              INDUSTRY_MAPPING[key].include?(survey_industry)
            end
            recommendations_industry || 'Other'
          end

          def target_audience
            from_onboarding = current_account.settings.onboarding_segments
            ONBOARDING_SEGMENT_MAPPING[from_onboarding] || 'Other'
          end

          def zendesk_plan
            types = ZendeskBillingCore::Zendesk::PlanType::TYPES
            current_account_type = current_account.subscription.plan_type
            types.find { |t| t.plan_type == current_account_type }.finance_name
          end

          def support_users
            if Arturo.feature_enabled_for?(:pluck_billable_agents, current_account)
              billable_agents = current_account.billable_agents.includes(:groups).references(:groups).pluck(
                :id,
                :roles,
                :permission_set_id,
                'groups.id'
              )
              collate_group_ids(billable_agents)
            else
              billable_agents = current_account.billable_agents
              billable_agents.map do |agent|
                {
                  user_id: agent.id,
                  role: agent.roles,
                  custom_role_id: agent.custom_role_id,
                  groups: agent.groups.map(&:id)
                }
              end
            end
          end

          def collate_group_ids(agents)
            collated_agents = {}
            agents.each do |agent|
              agent_id = agent[0]
              # Initialize hash once for each user
              unless collated_agents[agent_id]
                collated_agents[agent_id] = {
                  user_id: agent_id,
                  role: agent[1],
                  custom_role_id: agent[2],
                  groups: []
                }
              end
              collated_agents[agent_id][:groups] << agent[3] # Add each group membership to user hash
            end
            collated_agents.values
          end
        end
      end
    end
  end
end
