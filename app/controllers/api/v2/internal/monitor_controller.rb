require 'zendesk/internal_api/client'

module Api
  module V2
    module Internal
      class MonitorController < Api::V2::Internal::BaseController
        include ::Voice::Core::NumberSupport
        include Fraud::FraudScoreActions

        allow_subsystem_user :account_service
        allow_bime_user only: [:account_stats]

        allow_cors :all

        before_action :set_audit_info
        caches_action :account_stats, expires_in: 6.hours, cache_path: lambda { |c| c.send(:cache_path_for_account_stats) }

        account_parameter = {
          multiproduct: Parameters.anything,
          is_serviceable: Parameters.anything
        }
        roles_parameter = (Parameters.array(Parameters.bigid) | Parameters.nil)

        allow_parameters :token, user_id: Parameters.bigid
        def token
          return reject_action unless request.post?

          user  = current_account.users.find(params[:user_id])
          token = user.master_tokens.create!(expires_at: 10.minutes.from_now, assuming_monitor_user_id: request.headers['X-Zendesk-Via-Reference-ID'])
          render json: {verification_token: token.attributes.slice("expires_at", "id", "value")}, status: :created
        end

        allow_parameters :takeover_account, {}
        def takeover_account
          enqueue_fraud_score_job(Fraud::SourceEvent::TAKEN_OVER_ACCOUNT)
          render json: {}
        end

        allow_parameters :logout_all_users_and_devices, {}
        def logout_all_users_and_devices
          current_account.logout_all_users!

          render json: {}
        end

        allow_parameters :account, account: account_parameter
        def account
          if request.put?
            if params[:account][:is_serviceable] == true
              enqueue_fraud_score_job(Fraud::SourceEvent::UNSUSPENDED)
            elsif params[:account][:is_serviceable] == false
              enqueue_fraud_score_job(Fraud::SourceEvent::MANUAL_SUSPENSION)
            end
            current_account.attributes = params[:account]
            Zendesk::SupportAccounts::Product.retrieve(current_account).save!

            render json: account_presenter.present(current_account)
          elsif request.delete?
            # also used via app/models/account/sandbox.rb
            destroy_account
          else
            reject_action
          end
        end

        allow_parameters :cancel_account,
          human_verdict: Parameters.boolean

        def cancel_account
          current_account.cancel!

          if params[:human_verdict]
            enqueue_fraud_score_job(Fraud::SourceEvent::CANCELLED_ACCOUNT)
          end

          render json: {}
        end

        allow_parameters :reactivate_account, {}
        def reactivate_account
          current_account.reactivate!
          enqueue_fraud_score_job(Fraud::SourceEvent::UNSUSPENDED)
          render json: {}
        end

        allow_parameters :soft_delete_account, {}
        def soft_delete_account
          Zendesk::SupportAccounts::Product.retrieve(current_account).soft_delete!
          render json: {}, status: :ok
        rescue => e
          render json: { error: 'Cannot soft delete', description: e.message }, status: :unprocessable_entity
        end

        allow_parameters :release_account_subdomain, account_id: Parameters.bigid
        def release_account_subdomain
          account = ::Account.with_deleted { ::Account.find(params[:account_id]) }

          Zendesk::Maintenance::Util::SubdomainReleaser.new(account).release_subdomain!

          begin
            account.clear_kasket_cache!
          rescue
          end
          render json: {}, status: :ok
        rescue => e
          render json: { error: 'Cannot release subdomain', description: e.message }, status: :unprocessable_entity
        end

        allow_parameters :compliance_moves, {}
        def compliance_moves
          render json: compliance_moves_presenter.present(current_account)
        end

        allow_parameters :account_stats, {}
        def account_stats
          render json: account_stats_presenter.present(current_account)
        end

        allow_parameters :settings, {}
        def settings
          render json: account_settings_presenter.present(current_account)
        end

        allow_parameters :security_settings, {}
        def security_settings
          render json: account_security_settings_presenter.present(current_account)
        end

        allow_parameters :trial_extensions, {}
        def trial_extensions
          render json: account_trial_extensions_presenter.present(current_account)
        end

        allow_parameters :subscription,
          subscription: Parameters.map(
            voice_sub_account: Parameters.map(
              opted_in: Parameters.string
            ),
            audit_message: Parameters.string,
            payment_method_type: Parameters.integer,
            trial_expires_on: Parameters.string | Parameters.nil,
            plan_type: Parameters.string,
            base_agents: Parameters.integer,
            currency_type: Parameters.integer,
            billing_cycle_type: Parameters.integer,
            hub_account_id: Parameters.bigid | Parameters.nil_string,
            pricing_model_revision: Parameters.integer
          )
        def subscription
          return reject_action unless request.put?

          update_voice_sub_account(params[:subscription].delete(:voice_sub_account))

          current_account.subscription.managed_update = true
          current_account.subscription.update_from_monitor_pod = true
          current_account.subscription.audit_message = params[:subscription].delete(:audit_message)

          subscription_fraud_job
          current_account.subscription.attributes = params[:subscription]
          Zendesk::SupportAccounts::Product.retrieve(current_account).save!

          current_account.subscription.propagate_subscription_settings

          render json: {}
        end

        allow_parameters :experiment_participations, {}
        def experiment_participations
          last_modified = current_account.experiments.maximum(:updated_at) || current_account.updated_at
          if stale?(last_modified: last_modified)
            participations = current_account.experiments.map(&:to_data)

            collection = experiment_participation_presenter.present(participations)
            render json: collection
          end
        end

        allow_parameters :generate_twilio_account, {}
        def generate_twilio_account
          return reject_action unless request.put?
          ZBC::Zuora::Jobs::UpdateVoiceSubAccountJob.enqueue(current_account.id, false, audit_headers)

          head :ok
        end

        allow_parameters :enable_voice_trial, {}
        def enable_voice_trial
          return reject_action unless request.put?
          current_account.settings.voice_trial_enabled = 1
          current_account.settings.save!

          head :ok
        end

        allow_parameters :disable_voice_trial, {}
        def disable_voice_trial
          return reject_action unless request.put?
          current_account.settings.voice_trial_enabled = 0
          current_account.settings.save!

          head :ok
        end

        allow_parameters :enable_voice_failover_activated, {}
        def enable_voice_failover_activated
          return reject_action unless request.put?
          current_account.settings.voice_failover_activated = true
          current_account.settings.save!

          head :ok
        end

        allow_parameters :disable_voice_failover_activated, {}
        def disable_voice_failover_activated
          return reject_action unless request.put?
          current_account.settings.voice_failover_activated = false
          current_account.settings.save!

          head :ok
        end

        allow_parameters :create_voice_usage,
          amount: Parameters.float | Parameters.integer | Parameters.string,
          reference: Parameters.string
        def create_voice_usage
          return reject_action unless request.put?

          currency_amount         = params[:amount].to_s.gsub(/[$,]/, '')
          usage_units_adjustment  = (currency_amount.to_f * ZBC::Voice::RechargeSupport::CREDITS_PER_UNIT)

          description     = params[:reference] || "fixup"
          timestamp       = Time.now.to_i
          unique_voice_id = "#{description}-#{timestamp}"

          attributes = {
            units:              usage_units_adjustment,
            voice_reference_id: unique_voice_id,
            usage_type:         'fixup'
          }

          if Arturo.feature_enabled_for?(:serialize_voice_usage, current_account)
            attributes[:account_id] = current_account.id
            ZBC::Voice::UnprocessedUsage.create!(attributes)
          else
            current_account.zuora_voice_usages.create!(attributes)
          end

          head :ok
        end

        allow_parameters :facebook_pages, {}
        def facebook_pages
          pages = ::Facebook::Page.where(account_id: current_account.id).order(:state).to_a
          render json: facebook_page_presenter.present(pages)
        end

        allow_parameters :twitter_handles, {}
        def twitter_handles
          handles = MonitoredTwitterHandle.where(account_id: current_account.id).order(:created_at).to_a
          render json: twitter_handle_presenter.present(handles)
        end

        allow_parameters :registered_integration_services, {}
        def registered_integration_services
          riss = ::Channels::AnyChannel::RegisteredIntegrationService.where(account_id: current_account.id).order(:created_at).to_a
          render json: registered_integration_service_presenter.present(riss)
        end

        allow_parameters :integration_service_instances, {}
        def integration_service_instances
          isis = ::Channels::AnyChannel::IntegrationServiceInstance.where(account_id: current_account.id).order(:created_at).to_a
          render json: integration_service_instance_presenter.present(isis)
        end

        allow_parameters :undelete_suspended,
          undelete_since: Parameters.datetime
        def undelete_suspended
          count = SuspendedTicket.undelete(current_account, Date.parse(params[:undelete_since]))
          render json: {undeleted: count}
        end

        allow_parameters :audit_events,
          limit: Parameters.bigid,
          page: Parameters.bigid,
          from: Parameters.string,
          to: Parameters.string,
          per_page: Parameters.integer,
          roles: roles_parameter,
          source_type: Parameters.string | Parameters.array(Parameters.string)

        def audit_events
          render json: {audit_events: all_audit_events}
        end

        allow_parameters :reset_email,
          email: Parameters.string
        def reset_email
          if user = current_account.find_user_by_email(params[:email], scope: current_account.all_users)
            if user.is_active?
              render json: {error: "The user with email #{params[:email]} is active and can be edited normally"}, status: :unprocessable_entity
            else
              user.identities.delete_all
              render json: {}
            end
          else
            render json: {error: "No user found by email #{params[:email]}"}, status: :unprocessable_entity
          end
        end

        allow_parameters :mobile_devices,
          user_id: Parameters.bigid
        def mobile_devices
          user_id = params[:user_id].to_i
          if user = current_account.users.find(user_id)
            render json: mobile_device_presenter.present(user.device_identifiers.active)
          else
            render json: {error: "No user found by id #{user_id}"}, status: :unprocessable_entity
          end
        end

        allow_parameters :mobile_device,
          device_id: Parameters.bigid
        def mobile_device
          return reject_action unless request.delete?

          device_id = params[:device_id].to_i
          if device = PushNotifications::FirstPartyDeviceIdentifier.find_by_account_id_and_id(current_account.id, device_id)
            device.destroy
            render json: {}
          else
            render json: {error: "No device found by id #{device_id}"}, status: :unprocessable_entity
          end
        end

        allow_parameters :reset_mobile_device_push_notif_count,
          device_id: Parameters.bigid
        def reset_mobile_device_push_notif_count
          return reject_action unless request.put?

          device_id = params[:device_id].to_i
          if device = PushNotifications::FirstPartyDeviceIdentifier.where(id: device_id, account_id: current_account.id).first
            device.record_view!
            render json: {}
          else
            render json: {error: "No device found by id #{device_id}"}, status: :not_found
          end
        end

        allow_parameters :rename_subdomain,
          subdomain: Parameters.string
        def rename_subdomain
          subdomain = params[:subdomain]
          if Zendesk::RoutingValidations.subdomain_available?(subdomain, internal: true)
            current_account.route.subdomain = subdomain
            current_account.route.save(validate: false)
            # Reload new route back into kasket
            current_account.class.on_master do
              current_account.route.reload
              brand = current_account.brands.find_by_route_id(current_account.route)
              brand.route.reload
            end

            # This endpoint should be called rarely enough, that we wait until we're inside the job before we check
            # if its even necessary to update Revere.
            RevereAccountUpdateJob.enqueue(current_account.id)

            render json: {}
          else
            render json: {error: "subdomain unavailable"}, status: :unprocessable_entity
          end
        end

        allow_parameters :remove_host_mapping, {}
        def remove_host_mapping
          current_account.host_mapping = nil
          if current_account.save
            render json: {}
          else
            render json: {error: "Unable to save"}, status: :unprocessable_entity
          end
        end

        allow_parameters :rename_brand_subdomain, brand_id: Parameters.bigid, subdomain: Parameters.string
        def rename_brand_subdomain
          subdomain = params[:subdomain]
          brand = current_account.brands.find(params[:brand_id])

          if Zendesk::RoutingValidations.subdomain_available?(subdomain, internal: true)
            if brand.update_attributes(subdomain: subdomain)
              render json: {}
            else
              render json: {error: "Unable to save"}, status: :unprocessable_entity
            end
          else
            render json: {error: "Subdomain unavailable"}, status: :unprocessable_entity
          end
        end

        allow_parameters :burn_rates,
          start_date_time: Parameters.string,
          end_date_time: Parameters.string
        def burn_rates
          burn_rates = ZBC::Zuora::VoiceUsage.burn_rates(current_account.id, params[:start_date_time], params[:end_date_time])
          render json: voice_burn_rate_presenter.present(burn_rates)
        end

        allow_parameters :update_zuora_subscription,
          zuora_subscription: Parameters.map(
            zuora_account_id: Parameters.bigid
          ),
          account: account_parameter,
          subscription: Parameters.string,
          zuora_account_id: Parameters.bigid

        def update_zuora_subscription
          return reject_action unless request.put?
          return head(:not_found) if current_account.subscription.zuora_subscription.blank?
          new_zuora_account_id = zuora_subscription_params[:zuora_account_id]
          new_zendesk_id       = get_zendesk_id(new_zuora_account_id)
          zuora_subscription   = current_account.subscription.zuora_subscription
          zendesk_id           = current_account.id

          if zendesk_ids_match?(zendesk_id, new_zendesk_id)
            zuora_subscription.update_attributes!(zuora_subscription_params)
            pravda_client.update_account(billing_id: new_zuora_account_id)
            ZBC::Zuora::Synchronizer.synchronize!(new_zuora_account_id)
            head :ok
          else
            error_message =
              if new_zendesk_id.nil?
                "The zuora account doesn't exist, please check your input"
              else
                "The two zuora accounts don't have the same zendesk id, please check your input"
              end
            render json: { error: error_message }, status: :unprocessable_entity
          end
        end

        allow_parameters :feature_boost,
          feature_boost: Parameters.map(
            valid_until: Parameters.datetime,
            boost_level: Parameters.string,
            is_active: Parameters.boolean
          ),
          zuora_account_id: Parameters.string,
          id: Parameters.bigid
        def feature_boost
          if request.put?
            update_feature_boost
          elsif request.delete?
            destroy_feature_boost
          else
            reject_action
          end
        end

        allow_parameters :zopim_subscription_boost,
          premium_boost_date_expiry: Parameters.datetime
        def zopim_subscription_boost
          if request.put? && params[:premium_boost_date_expiry].present?
            premium_boost_date_expiry = params[:premium_boost_date_expiry].to_date
            if premium_boost_date_expiry >= Date.tomorrow.midnight.to_date
              update_zopim_subscription_boost(premium_boost_date_expiry)
            else
              Rails.logger.error "Premium boost date expiration must be set to midnight the following day, or later"
              reject_params
            end
          elsif request.delete?
            destroy_zopim_subscription_boost
          end
        end

        allow_parameters :destroy_zopim_subscription_boost, {}
        def destroy_zopim_subscription_boost
          # sets premium boost date to midnight of the following day (will not expire immediately)
          update_zopim_subscription_boost(Date.tomorrow.midnight.to_date)
        end

        allow_parameters :update_zopim_subscription_boost, {}
        def update_zopim_subscription_boost(premium_boost_date_expiry)
          zopim_account_id = current_account.zopim_subscription.zopim_account_id
          case current_account.zopim_subscription.plan_type
          when 'premium' # should be filtered by the view
            Rails.logger.error("Zopim account ##{zopim_account_id} is already Premium: boost unavailable")
          when 'trial' # should be filtered by the view
            Rails.logger.error("Zopim account ##{zopim_account_id} is Trial: boost unavailable")
          when 'lite'
            boost_lite_subscription(premium_boost_date_expiry)
          else
            boost_paid_subscription(premium_boost_date_expiry)
          end
          render json: {}, status: :ok
        end

        allow_parameters :boost_lite_subscription, {}
        def boost_lite_subscription(premium_boost_date_expiry)
          ::Zopim::Reseller.client.update_account!(
            id:   current_account.zopim_subscription.zopim_account_id,
            data: { plan: 'trial', trial_end_date: premium_boost_date_expiry.strftime("%Y-%m-%d") }
          )
        end

        # The Zopim Reseller API clients endpoint to update an account accepts
        # an optional parameter to set the its premium boost date expiration of
        #
        # This field is currently undocumented and is called :premium_boost_date
        # --- however when an account is queried for details the field for the
        # :premium_boost_date value is called  :premium_boost_date_expiry
        # (this is also undocumented)
        #
        # Setting the :premium_boost_date is applicable only to purchased
        # accounts that are not on the Premium plan-type. It allows the account
        # to access Premium features up to the the date set.
        #
        # Setting this value on an account with any other plan-type or whose
        # trial status is no longer active is a no-op.
        #
        # SEE: http://reseller-docs.zopim.com/api/accounts#TOC-Update
        # SEE: http://reseller-docs.zopim.com/api/accounts#TOC-Show
        allow_parameters :boost_paid_subscription, {}
        def boost_paid_subscription(premium_boost_date_expiry)
          ::Zopim::Reseller.client.update_account!(
            id:   current_account.zopim_subscription.zopim_account_id,
            data: { premium_boost_date: premium_boost_date_expiry.strftime("%Y-%m-%d") }
          )
        end

        allow_parameters :zuora_approve_subscription,
          zuora_account_id: Parameters.bigid
        def zuora_approve_subscription
          return reject_action unless request.post?

          id = params[:zuora_account_id]
          zs = ZBC::Zuora::Subscription.find_by_zuora_account_id(id)
          head(status: :not_found) && return if zs.nil?

          zs.approval_required = false
          head status: (zs.save ? :ok : :unprocessable_entity)
        end

        allow_parameters :zuora_subscription,
          zuora_account_id: Parameters.bigid
        def zuora_subscription
          return reject_action unless request.post?
          zuora_account_id = params[:zuora_account_id]
          ZBC::Zuora::Jobs::AccountSyncJob.enqueue(zuora_account_id)
          render json: {}
        end

        allow_parameters :temporary_agent_count,
          zuora_account_id: Parameters.bigid
        def temporary_agent_count
          return reject_action unless request.get?
          zuora_account_id = params[:zuora_account_id]
          zuora_client = ZBC::Zuora::Client.new(zuora_account_id)
          total_temp_agents = zuora_client.get_temporary_zendesk_agents_products.inject(0) do |sum, temp_agents_prod|
            temp_agents_hash = temp_agents_prod.addon_rateplan_info['temporary_zendesk_agents']
            temp_count       = (temp_agents_hash && temp_agents_hash[:quantity]) || 0
            sum + temp_count.to_i
          end
          render json: { temporary_agent_count: total_temp_agents }
        end

        allow_parameters :zuora_sync_subscription,
          id: Parameters.bigid
        def zuora_sync_subscription
          ZBC::Zuora::Jobs::AccountSyncJob.enqueue(params[:id])
          render json: {}
        end

        allow_parameters :agents,
          page: Parameters.integer,
          per_page: Parameters.integer,
          roles: roles_parameter

        def agents
          roles = params[:roles] || [2, 4]
          users = current_account.agents.where(roles: roles)

          if params[:page] && params[:per_page]
            users = users.paginate(per_page: params[:per_page], page: params[:page])
          end

          render json: user_presenter.present(users.order("name #{sort_order}").to_a)
        end

        allow_parameters :agent_id_name_hash,
          page: Parameters.integer,
          per_page: Parameters.integer,
          roles: roles_parameter

        def agent_id_name_hash
          render json: agent_id_name_hash_data
        end

        # Send notice to customer that their cert is now active
        allow_parameters :update_dns, certificate_id: Parameters.bigid
        def update_dns
          cert = Certificate.find(params.require(:certificate_id))
          if cert.state == "active"
            cert.notify_customer_of_activation!
          end
          render json: {}
        end

        allow_parameters :reports, {}
        def reports
          report_filenames = [
            Zendesk::Export::BillingReport.filename,
            Zendesk::Export::BenchmarkingReport.filename,
          ]

          report_urls = report_filenames.each_with_object({}) do |filename, urls|
            report_store   = Zendesk::S3ReportingStore.new(filename)
            urls[filename] = report_store.fetch.url
          end

          render json: {reports: report_urls}
        end

        allow_parameters :append_intermediate_certificate,
          cert_id: Parameters.bigid,
          cert: Parameters.string
        def append_intermediate_certificate
          im_file = Tempfile.new('im_cert.crt')
          im_file.write(params.require(:cert))
          im_file.close

          cert = current_account.certificates.find(params.require(:cert_id).to_i)
          cert.append_intermediate(im_file)
          cert.convert_to_sni!

          render json: {}
        ensure
          if im_file
            im_file.close
            im_file.unlink
          end
        end

        allow_parameters :delete_attachment, token: Parameters.string
        def delete_attachment
          if Attachment.where(token: params[:token], account_id: current_account.id).count >= 1 # images have 2 total
            Attachment.where(token: params[:token], account_id: current_account.id).destroy_all
            render json: {}
          else
            render json: {error: "Attachment Not Found"}, status: :not_found
          end
        end

        allow_parameters :send_one_time_login_to_user, user_id: Parameters.bigid
        def send_one_time_login_to_user
          monitor_user_id = request.headers['X-Zendesk-Via-Reference-ID']
          user = current_account.users.find params.require(:user_id)

          if current_account.settings.sso_bypass || current_account.role_settings.agent_remote_bypass_allowed?
            render json: { error: "SSO bypass is enabled for this account" }, status: :unprocessable_entity
          elsif monitor_user_id.nil?
            render json: { error: "Monitor user id required" }, status: :unprocessable_entity
          elsif !user.is_admin?
            render json: { error: "User is not an administrator" }, status: :unprocessable_entity
          else
            token = user.otp_tokens.create!.value
            Rails.logger.error "[security] created sso bypass token for monitor_user_id:#{monitor_user_id} account:#{current_account.id} user:#{user.id} token:#{token[0..5]}"
            AccessMailer.deliver_token(user, token)
            render json: {}
          end
        end

        allow_parameters :reserved_subdomains, {}
        def reserved_subdomains
          subdomains = ReservedSubdomain.all.map do |rs|
            { pattern: rs.pattern, is_regex: rs.is_regex }
          end

          render json: { subdomains: subdomains }
        end

        allow_parameters :add_reserved_subdomain,
          pattern: Parameters.string,
          is_regex: Parameters.boolean
        def add_reserved_subdomain
          pattern = params.require(:pattern)
          is_regex = params.require(:is_regex)

          new_reservation = ReservedSubdomain.new.tap do |rs|
            rs.pattern = pattern
            rs.is_regex = is_regex
          end

          json, status = if new_reservation.save
            json = {
              subdomain: {
                pattern: new_reservation.pattern,
                is_regex: new_reservation.is_regex,
              }
            }

            [json, :created]
          else
            json = { errors: new_reservation.errors.full_messages }

            [json, :bad_request]
          end

          render json: json, status: status
        end

        private

        def enqueue_fraud_score_job(source_event, reason = nil)
          FraudScoreJob.enqueue_account(current_account, source_event, reason)
        end

        def agent_id_name_hash_data
          Hash[current_account.agents.map { |user| [user.id, user.name] }]
        end

        def cache_path_for_account_stats
          cache_key = "api/v2/internal/monitor/account_stats/#{current_account.cache_key}"
          cache_key << "/rate-limit-boost-#{current_account.api_rate_limit_boosted?}"
          cache_key << "/cdn-provider-#{current_account.cdn_provider}"
          cache_key << "/settings-#{Digest::MD5.hexdigest(current_account.settings.map(&:cache_key).join('/'))}"

          if current_account.feature_boost.present?
            cache_key << "/#{current_account.feature_boost.cache_key}"
          end

          cache_key
        end

        def zuora_subscription_params
          params.require(:zuora_subscription).permit(:zuora_account_id)
        end

        def zendesk_ids_match?(zendesk_id, other_zendesk_id)
          return false if zendesk_id.blank? || other_zendesk_id.blank?
          zendesk_id.to_s == other_zendesk_id.to_s
        end

        def get_zendesk_id(zuora_account_id)
          ZBC::Zuora::Client.session.account.
            where(id: zuora_account_id).first.try(:zendesk_account_id__c)
        end

        def update_feature_boost
          boost = (current_account.feature_boost || current_account.build_feature_boost)
          boost.attributes = params[:feature_boost].slice(:valid_until, :boost_level).merge(is_active: true)
          Zendesk::SupportAccounts::Product.retrieve(current_account).update_plan_settings!(boost)
          render json: {}
        end

        def destroy_feature_boost
          boost = current_account.feature_boost
          if boost
            boost.is_active = false
            Zendesk::SupportAccounts::Product.retrieve(current_account).update_plan_settings!(boost)
          end
          render json: {}
        end

        def all_audit_events
          limit = params[:limit] ? params[:limit].to_i : 500

          events = CIA::Event.previous.for_account(current_account).using_visible_index.
            created_between(params[:from], params[:to]).
            for_source_type(params[:source_type]).
            limit(limit)

          filter_and_process(events, limit).map do |event|
            presenter = CIA::EventPresenter.new(view_context, event)
            # show names for legacy audits involving zendesk agents
            agent_name = support_agents[event.via_reference_id.to_s] if event.via_id == ViaType.IMPORT
            # monitor is specifically English only application
            source_label = presenter.english_source_label
            source_label = event.source_display_name if source_label.include?("translation missing: ") # not all models are properly translated
            {
              action: event.action,
              source_type: event.source_type,
              source_id: event.source_id,
              actor_type: event.actor_type,
              actor_id: event.actor_id,
              actor_name: agent_name || event.actor.try(:name), # avoid side-loading
              zendesk_agent: !!agent_name,
              ip_address: event.ip_address,
              created_at: event.created_at,
              attribute_changes: event.attribute_change_hash,
              monitor_user_id: (event.via_reference_id if event.via_id == ViaType.monitor_EVENT),
              source_display_name: source_label,
              message: event.message
            }
          end
        end

        def filter_and_process(events, limit)
          if events.blank? && source_is_rule_subclass?
            # Types Trigger, Macro, Automation, View, are initially only of source_type Rule
            rule_events = CIA::Event.previous.for_account(current_account).using_visible_index.
              created_between(params[:from], params[:to]).
              for_source_type('Rule')
            CIA::Event.assign_exact_source_type(rule_events)
            typed_events = rule_events.find_all { |event| params[:source_type].include?(event.source_type) }
            date_scope_and_paginate(typed_events)
          else
            paginate_args = {
              per_page: 100,
              page: (params[:page] ? params[:page].to_i : nil)
            }
            paginated_events = events.paginate(paginate_args).
              includes([:attribute_changes, :actor]).
              limit(limit)
            CIA::Event.assign_exact_source_type(paginated_events)
            paginated_events
          end
        end

        def source_is_rule_subclass?
          rule_sources = %w[Trigger Macro Automation View]
          if params[:source_type].present?
            param_sources = params[:source_type]
            search_sources = rule_sources & param_sources
            search_sources.present?
          else
            false
          end
        end

        def date_scope_and_paginate(events)
          date_scope(events).paginate(include: [:attribute_changes, :actor], per_page: 100, page: params[:page])
        end

        def date_scope(events)
          return events unless params[:from] && params[:to]
          from = params[:from].to_date.beginning_of_day
          to = params[:to].to_date.end_of_day

          return events.find_all { |event| event.created_at >= from && event.created_at <= to } if events.is_a? Array
          events.where(['created_at >= ? and created_at <= ?', from, to])
        end

        def support_agents
          @support_agents ||= (Rails.env.development? ? agent_id_name_hash_data : self.class.support_agents)
        end

        # we cannot use on_shard if this runs on a pod that has no access to support shard -> use api
        public_class_method def self.support_agents
          Rails.cache.fetch "monitor_support_agent_names_v3", expires_in: 1.day do
            client = Zendesk::InternalApi::Client.new("support")
            client.connection.get("/api/v2/internal/monitor/agent_id_name_hash.json").to_hash[:body]
          end
        end

        def update_voice_sub_account(params)
          return unless params

          optin = ::Voice::Core::OptinSupport.new(
            voice: (current_account.voice_sub_account.try(:opted_in) == true),
            voice_key: :opted_in,
            true: 'true',
            false: 'false'
          )
          optin.update(params)

          if optin.changed?(:voice)
            ZBC::Zuora::Jobs::UpdateVoiceSubAccountJob.enqueue(current_account.id, optin.enabled?(:voice), audit_headers)
          end
        end

        def destroy_account
          if current_account.cancel!
            render json: {}
          else
            render json: { errors: current_account.errors.map(&:to_s) }, status: :unprocessable_entity
          end
        end

        def subscription_fraud_job
          return if current_account.whitelisted?

          if params[:subscription][:trial_expires_on] &&
              current_account.subscription.trial_expires_on &&
              Date.parse(params[:subscription][:trial_expires_on]) > current_account.subscription.trial_expires_on
            enqueue_fraud_score_job(Fraud::SourceEvent::TRIAL_EXTENSION)
          elsif params[:subscription][:plan_type].to_i == SubscriptionPlanType.ExtraLarge
            enqueue_fraud_score_job(Fraud::SourceEvent::ENTERPRISE_SUBSCRIPTION)
          end
        end

        def audit_headers
          audit_headers = {}
          %w[X-Forwarded-For X-Zendesk-Via X-Zendesk-Via-Reference-ID].each do |header_name|
            audit_headers[header_name] = request.headers[header_name]
          end
          audit_headers
        end

        def set_audit_info
          if ip = request.headers["HTTP_X_FORWARD_FOR"]
            CIA.current_transaction[:ip_address] = ip
          end
        end

        def account_stats_presenter
          @account_stats_presenter ||= Api::V2::Internal::AccountStatsPresenter.new(current_user, url_builder: self)
        end

        def account_settings_presenter
          @account_settings_presenter ||= Api::V2::Internal::AccountSettingsPresenter.new(current_user, url_builder: self)
        end

        def compliance_moves_presenter
          @compliance_moves_presenter ||= Api::V2::Internal::ComplianceMovesPresenter.new(current_user, url_builder: self)
        end

        def account_security_settings_presenter
          @account_security_settings_presenter ||= Api::V2::Internal::AccountSecuritySettingsPresenter.new(current_user, url_builder: self)
        end

        def account_trial_extensions_presenter
          @account_trial_extensions_presenter ||= Api::V2::Internal::AccountTrialExtensionsPresenter.new(current_user, url_builder: self)
        end

        def user_presenter
          @user_presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self, includes: includes)
        end

        def facebook_page_presenter
          @facebook_page_presenter ||= ::Channels::Api::V2::Internal::FacebookPagePresenter.new(current_user, url_builder: self)
        end

        def mobile_device_presenter
          @mobile_device_presenter ||= Api::V2::MobileDevicePresenter.new(current_user, url_builder: self)
        end

        def twitter_handle_presenter
          @twitter_handle_presenter ||= ::Channels::Api::V2::Internal::TwitterHandlePresenter.new(
            current_user,
            url_builder: self
          )
        end

        def registered_integration_service_presenter
          @ris_presenter ||= ::Channels::Api::V2::Internal::RegisteredIntegrationServicePresenter.new(
            current_user,
            url_builder: self
          )
        end

        def integration_service_instance_presenter
          @isi_presenter ||= ::Channels::Api::V2::Internal::IntegrationServiceInstancePresenter.new(
            current_user,
            url_builder: self
          )
        end

        def account_presenter
          @account_presenter ||= Api::V2::AccountPresenter.new(current_account.owner, url_builder: self, includes: includes)
        end

        def experiment_participation_presenter
          @experiment_participation_presenter ||= Api::V2::Internal::ExperimentParticipationPresenter.new(current_user, url_builder: self)
        end

        def voice_burn_rate_presenter
          @voice_burn_rate_presenter ||= Api::V2::Internal::VoiceBurnRatePresenter.new(current_user, url_builder: self)
        end

        def reject_action
          Rails.logger.error "Method is not allowed on this action."
          head :method_not_allowed
        end

        def reject_params
          head :unprocessable_entity
        end

        def pravda_client
          @pravda_client ||= Zendesk::Accounts::Client.new(current_account)
        end
      end
    end
  end
end
