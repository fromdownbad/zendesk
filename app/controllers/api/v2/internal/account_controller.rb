module Api
  module V2
    module Internal
      class AccountController < Api::V2::Internal::BaseController
        allow_subsystem_user :outbound, only: [:enable_contributor]
        allow_subsystem_user :zopim, only: [:enable_contributor]
        allow_subsystem_user :sell, only: [:enable_contributor]

        ## ### Account Contributor role
        ## `POST /api/v2/internal/account/enable_contributor`
        ## Enabling Contributor role for the given account
        ##
        ## #### Allowed For
        ##
        ##  * outbound Subsystem Users
        ##  * chat Subsystem Users
        ##  * sell Subsystem Users
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 201 CREATED
        ## ```
        allow_parameters :enable_contributor, {}
        def enable_contributor
          PermissionSet.enable_contributor!(current_account)

          head :created
        end

        ## ### Account Route
        ## `PUT /api/v2/internal/account/route`
        ##
        ## Enables updating the route's gam_domain for linking existing accounts to
        ## their G Suite domain.
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ## ```
        allow_parameters :route,
          domain: Parameters.anything,
          gam_domain: Parameters.string
        require_oauth_scopes :route, scopes: [:write], arturo: true
        def route
          if request.put?
            domain = params[:gam_domain]
            domain = domain.present? ? domain : nil
            current_account.route.gam_domain = domain
            begin
              current_account.route.save!
              head :ok
            rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation
              route = Route.where(gam_domain: domain).first.reload
              result = { error: 'Domain in use', description: "The G Suite domain '#{domain}' is currently taken by the '#{route.account.subdomain}' subdomain"}
              render json: result, status: :conflict
            end
          end
        end
      end
    end
  end
end
