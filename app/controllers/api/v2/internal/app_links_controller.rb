module Api::V2::Internal
  class AppLinksController < Api::V2::Internal::BaseController
    APP_LINK_PARAMETER = {
      id: Parameters.integer,
      app_link: {
        app_id: Parameters.string,
        package_name: Parameters.string,
        paths: Parameters.array(Parameters.string),
        fingerprints: Parameters.array(Parameters.string)
      }
    }.freeze

    allow_parameters :index, {}
    def index
      render json: presenter.present(AppLink.all)
    end

    resource_action :show

    allow_parameters :update, APP_LINK_PARAMETER
    def update
      app_link = AppLink.find(params[:id])

      if app_link.update_attributes(params[:app_link])
        head :ok
      else
        render json: presenter.present_errors(app_link), status: :unprocessable_entity
      end
    end

    allow_parameters :create, APP_LINK_PARAMETER
    def create
      app_link = AppLink.new(params[:app_link])

      if app_link.save
        head :ok
      else
        render json: presenter.present_errors(app_link), status: :unprocessable_entity
      end
    end

    resource_action :destroy

    private

    def presenter
      @presenter ||= Api::V2::Internal::AppLinkPresenter.new(current_user, url_builder: self)
    end

    def app_link
      AppLink.find(params[:id])
    end
  end
end
