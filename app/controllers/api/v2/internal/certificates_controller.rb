module Api
  module V2
    module Internal
      class CertificatesController < Api::V2::Internal::BaseController
        # Accept requests to https://pod-X.zendesk.com
        skip_before_action :prevent_missing_account_access

        # - #show is used by Exodus explicitly during an account lock
        # - #index should always work regardless
        around_action :allow_locked_accounts, only: [:index, :show]

        allow_only_subsystem_user :certificates, only: [:index, :show]
        allow_only_subsystem_user :stanchion, only: [:show]
        allow_only_subsystem_user :zendesk, only: [:destroy]

        allow_parameters :index,           pod_id: Parameters.integer,
                                           updated_since: Parameters.date_time | Parameters.nil_string
        def index
          pod_id = params.require(:pod_id)
          CertificateIp.release_scheduled_ips!(pod_id)
          render json: certificates_as_json(pod_id)
        end

        ## ### Get active SSL certificate for a given account_id
        ## `GET /api/v2/internal/certificates/:account_id.json`
        ##
        ## #### Allowed For
        ##
        ##  * 'certificate' Subsystem User
        ##
        ## #### Example Response
        ##
        ## {
        ##   "certificate": {
        ##     "updated_at": "2014/09/22 16:23:25 +0000",
        ##     "subdomain":  "support",
        ##     "ip":         "1.2.3.4",
        ##     "port":       10000,
        ##     "key":        "(private key encrypted against ssl_key_password)"
        ##     "crt":        "(certificate and CA certs in x509 format)"
        ##     "conf":       "(nginx configuration)"
        ##   }
        ## }
        allow_parameters :show, id: Parameters.integer
        def show
          account = ::Account.find(params[:id])
          certificate = account.certificates.active.first!
          current_pod = Zendesk::Accounts::DataCenter.pod_id_for_shard(account.shard_id)
          render json: { certificate: certificate.to_nginx(pod_id: current_pod) }
        end

        ## ### Revoke the requested certificate. Certificate must be in the active state
        ## `DELETE /api/v2/internal/certificates/:certificate_id.json`
        ##
        ## If the account has Zendesk Provisioned SSL enabled and the certificate is from Let's Encrypt
        ## revoking it will also disable Zendesk Provisioned SSL
        ##
        ## #### Allowed For
        ##
        ##  * 'zendesk' Subsystem User
        ##
        ## #### Example Response
        ##  ```Status: 204 No Content```
        ##
        allow_parameters :destroy, id: Parameters.integer
        def destroy
          certificate = current_account.certificates.active.find params[:id]
          certificate.revoke!

          if certificate.autoprovisioned?
            current_account.settings.automatic_certificate_provisioning = false
            current_account.settings.save!
          end

          default_delete_response
        end

        ## ### Activates the requested certificate. Certificate must be in the pending or revoked state
        ## `PUT /api/v2/internal/certificates/:certificate_id.json`
        ##
        ## Will attempt to activate the certificate with the optionally requested certificate_ip
        ##
        ## #### Allowed For
        ##
        ##  * 'zendesk' Subsystem User
        ##
        ## #### Example Response
        ##  ```Status: 204 No Content```
        ##

        allow_parameters :activate,           id: Parameters.integer,
                                              certificate_ip_id: Parameters.integer
        require_oauth_scopes :active, scopes: [:write], arturo: true
        def activate
          certificate = current_account.certificates.find params[:id]

          ip = params[:certificate_ip_id] ? CertificateIp.find(params[:certificate_ip_id]) : nil
          certificate.activate_certificate!(certificate_ip: ip, notify_customer: true)

          head :ok
        end

        private

        def certificates_as_json(pod_id)
          certificates(pod_id).map { |c| c.to_nginx(pod_id: pod_id) }.to_json
        end

        def certificates(pod_id)
          certificates = Certificate.active.for_pod(pod_id)
          certificates = certificates.updated_since(params[:updated_since]) if params[:updated_since]
          certificates = certificates.to_a
          Rails.logger.info("#{certificates.length} certificates returned")

          valid_certs, invalid_certs = certificates.partition(&:valid_key?)

          invalid_certs.each do |invalid|
            Rails.logger.warn "skipping invalid certificate #{invalid.id} for account #{invalid.account_id} #{invalid.account.subdomain}"
          end

          valid_certs
        end
      end
    end
  end
end
