class Api::V2::Internal::Ipm::BaseController < Api::V2::Internal::BaseController
  ## ### Listing IPM records
  ## `GET /api/v2/internal/ipm/alerts.json`
  ##
  ## #### Allowed For
  ##
  ##  * Subsystem Users
  ##

  # :skip is used for all allow_parameters directives in this controller since param permitting is
  # handled by strong parameter useage in the child controllers
  allow_parameters :index, :skip
  def index
    ipms = ipm_klass.all
    render json: presenter.present(ipms)
  end

  ## ### Create IPM record.
  ##
  ## `POST /api/v2/internal/ipm/(ipm type).json`
  ##
  ## Creates a new record record with the content of the submitted form data.
  ##
  ## Will respond with an HTTP status 422 if it is unable to save the record record,
  ## otherwise 201.
  ##
  ## #### Allowed For
  ##
  ##  * Subsystem Users
  ##
  allow_parameters :create, :skip # specific params are permitted in ipm_params
  def create
    ipm = ipm_klass.create!(ipm_params)
    render json: presenter.present(ipm), status: :created
  end

  ## ### Read a specific record.
  ##
  ## `GET /api/v2/internal/ipm/(ipm type)/:id.json`
  ##
  ## Fetches detail information for the record with matching the :id
  ##
  ## #### Allowed For
  ##
  ##  * Subsystem Users
  ##
  allow_parameters :show, id: Parameters.bigid
  def show
    render json: presenter.present(ipm_record)
  end

  ## ### Update IPM record.
  ##
  ## `PUT /api/v2/internal/ipm/(ipm type)/:id.json`
  ##
  ## Updates the record with the content of the submitted form data.
  ##
  ## Will respond with an HTTP status 422 if it is unable to save/update the coupon record,
  ## otherwise 200.
  ##
  ## #### Allowed For
  ##
  ##  * Subsystem Users
  ##
  allow_parameters :update, :skip # specific params are permitted in ipm_params
  def update
    ipm_record.update_attributes!(ipm_params)
    update_ipm_record_state if state.present?
    render json: presenter.present(ipm_record)
  end

  ## ### Delete IPM record.
  ##
  ## `DELETE /api/v2/internal/ipm/(ipm type)/:id.json`
  ##
  ## Deletes the IPM referenced by the :id and :type parameters.
  ##
  ## Will respond with an HTTP status 200 if it is able to delete the IPM record.
  ##
  ## #### Allowed For
  ##
  ##  * Subsystem Users
  ##
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    if ipm_record.destroy
      default_delete_response
    else
      error = presenter.present_errors(ipm_record).merge(resource: presenter.present(ipm_record))
      render json: error, status: :unprocessable_entity
    end
  end

  protected

  def ipm_base_params
    {
      state: Parameters.integer | Parameters.string,
      start_date: Parameters.string,
      end_date: Parameters.string,
      test_mode: Parameters.boolean,

      # Made explicit from Ipm::Alert::CONSTRAINTS
      internal_name: Parameters.string,
      pods: Parameters.array(Parameters.integer | Parameters.string) | Parameters.string,
      shards: Parameters.array(Parameters.integer) | Parameters.string,
      roles: Parameters.array(Parameters.string),
      languages: Parameters.array(Parameters.integer | Parameters.string),
      account_types: Parameters.array(Parameters.string),
      interfaces: Parameters.array(Parameters.string),
      plans: Parameters.array(Parameters.integer | Parameters.string),
      account_ids: Parameters.array(Parameters.integer | Parameters.string),
      exclude_account_ids: Parameters.array(Parameters.integer | Parameters.string),
      subdomains: Parameters.string,
      exclude_subdomains: Parameters.string,
      custom_factors: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil
    }
  end

  def ipm_record
    @ipm_record ||= ipm_klass.find(params[:id])
  end

  private

  def update_ipm_record_state
    ipm_record.state = state
    ipm_record.save!
  end

  def state
    ipm_params[:state]
  end
end
