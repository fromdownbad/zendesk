class Api::V2::Internal::Ipm::FeatureNotificationsController < Api::V2::Internal::Ipm::BaseController
  private

  def ipm_params
    params.require(:feature_notification).permit({
      title: Parameters.string,
      body: Parameters.string,
      image_name: Parameters.string,
      call_to_action_text: Parameters.string,
      call_to_action_url: Parameters.string,
      learn_more_url: Parameters.string,
      category_code: Parameters.string,
    }.merge(ipm_base_params))
  end

  def ipm_klass
    ::Ipm::FeatureNotification
  end

  def presenter
    @presenter ||= Api::V2::Ipm::FeatureNotificationPresenter.new(current_user, url_builder: self, internal: true)
  end
end
