class Api::V2::Internal::Ipm::AlertsController < Api::V2::Internal::Ipm::BaseController
  private

  def ipm_params
    params.require(:alert).permit({
      text: Parameters.string,
      link_url: Parameters.string,
      state: Parameters.integer | Parameters.string,
      start_date: Parameters.string,
      end_date: Parameters.string,
    }.merge(ipm_base_params))
  end

  def ipm_klass
    ::Ipm::Alert
  end

  def presenter
    @presenter ||= Api::V2::Ipm::AlertPresenter.new(current_user, url_builder: self, internal: true)
  end
end
