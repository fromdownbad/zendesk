module Api
  module V2
    module Internal
      class SandboxesController < Api::V2::Internal::BaseController
        allow_parameters :index, {}

        # sandbox_orchestrator needs to trigger email notification by POSTing to :notification
        skip_before_action :check_sandbox_orchestrator_permissions, only: :notification

        def index
          sandboxes = ::Accounts::Sandbox.where(sandbox_master_id: current_account.id).order(updated_at: :desc).limit(10)
          render json: account_presenter.present(sandboxes)
        end

        allow_parameters :update,
          id: Parameters.bigid
        allow_parameters :reactivate,
          id: Parameters.bigid

        def reactivate
          if !multiple_sandboxes? && current_account.sandbox
            current_account.sandbox.cancel!
          end
          old_sandbox.reactivate!
          render json: {}
        end
        # TODO: change calls to update to use reactivate
        alias_method :update, :reactivate

        allow_parameters :product_addon_sync,
          id: Parameters.bigid

        def product_addon_sync
          sandbox = ::Accounts::Sandbox.find_by_id!(params[:id])
          sandbox.product_addon_sync
          render json: { status: 'Products and addons synchronised' }
        end

        allow_parameters :notification,
          id: Parameters.bigid,
          user_id: Parameters.bigid,
          sandbox_id: Parameters.bigid,
          notification_type: Parameters.string
        def notification
          user = current_account.agents.find_by_id(params[:user_id])
          if user.nil?
            return render json: { error: "User #{params[:user_id]} not found for account #{params[:id]}" }, status: :bad_request
          end

          case params[:notification_type]
          when 'success'
            sandbox = ::Accounts::Sandbox.find_by_id!(params[:sandbox_id])
            SandboxMailer.deliver_success_notification(current_account, user, sandbox.subdomain)
          when 'ready'
            sandbox = ::Accounts::Sandbox.find_by_id!(params[:sandbox_id])
            SandboxMailer.deliver_ready_notification(current_account, user, sandbox.subdomain)
          when 'failure'
            SandboxMailer.deliver_failure_notification(current_account, user)
          else
            return render json: { error: "Unreognized notification_type #{params[:notification_type]}" }, status: :bad_request
          end
          render nothing: true, status: :ok
        end

        private

        def account_presenter
          @account_presenter ||= Api::V2::AccountPresenter.new(@current_account.owner, url_builder: self, internal: true)
        end

        def old_sandbox
          @old_sandbox ||= ::Accounts::Sandbox.find_by_id!(params[:id])
        end

        def multiple_sandboxes?
          Arturo.feature_enabled_for?(:multiple_sandboxes, current_account) || Arturo.feature_enabled_for?(:admin_center_framework_view_sandbox, current_account)
        end
      end
    end
  end
end
