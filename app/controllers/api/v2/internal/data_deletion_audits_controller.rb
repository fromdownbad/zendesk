module Api
  module V2
    module Internal
      class DataDeletionAuditsController < Api::V2::Internal::BaseController
        allow_only_subsystem_user :zendesk

        # Accept requests to https://pod-X.zendesk.com, necessary for enqueuing jobs in specific pods
        skip_before_action :prevent_missing_account_access

        before_action :set_account, only: [:index, :active, :create]

        ## ### Create a deletion audit
        ## `POST /api/v2/internal/data_deletion_audits.json`
        ##
        ## #### Allowed for
        ##
        ## * Subsystem User
        ##
        ## #### Using curl
        ##
        ## ```bash
        ## curl -X POST https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits.json \
        ##   -v -u {email_address}:{password} -d {'reason': 'moved', 'account_id': 123, shard_id: '201', enqueue: true}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "data_deletion_audit": {
        ##     "id": 1
        ##     "shard_id": null
        ##     "reason": "cancellation"
        ##     "status": "enqueued"
        ##     "created_at": 2015-02-18 22:39:54
        ##     "started_at": null
        ##     "completed_at": null
        ##     "url": "https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits/1.json",
        ##     "jobs": [...]
        ##   },
        ## }
        ## ```
        ##

        allow_parameters :create,
          account_id: Parameters.bigid,
          enqueue: Parameters.boolean,
          id: Parameters.bigid,
          reason: Parameters.string,
          shard_id: Parameters.integer

        def create
          audit = new_data_deletion_audit
          if DataDeletionAudit.where(account_id: audit.account_id, shard_id: audit.shard_id, status: ['enqueued', 'started', 'waiting']).count > 0
            # fail
            render json: { success: false, error: "DuplicateShardDeletion", description: "Account already being deleted from given shard." }
          else
            audit.save
            audit.enqueue! if params[:enqueue]
            render json: presenter.present(audit).merge!(success: true)
          end
        end

        ## ### List deletion audits
        ## `GET /api/v2/internal/data_deletion_audits.json`
        ##
        ## #### Allowed for
        ##
        ## * Subsystem User
        ##
        ## #### Using curl
        ##
        ## ```bash
        ## curl 'https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits.json?status=enqueued,started' \
        ##   -v -u {email_address}:{password}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "data_deletion_audits": [
        ##     {
        ##       "id": 1
        ##       "shard_id": null
        ##       "reason": "cancellation"
        ##       "status": "waiting"
        ##       "created_at": 2015-02-18 22:39:54
        ##       "started_at": null
        ##       "completed_at": null
        ##       "url": "https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits/1.json",
        ##       "jobs": [...]
        ##     }, ... ],
        ##  "total_entries": 3
        ## }
        ## ```
        ##
        SORT_LOOKUP = { 'id' => 'id' }.freeze
        allow_parameters :index,
          account_id: Parameters.bigid,
          page: Parameters.integer,
          per_page: Parameters.integer,
          status: Parameters.string,
          sort_by: Parameters.string,
          sort_order: Parameters.string

        def index
          audits = DataDeletionAudit.all

          if params[:account_id].present?
            audits = audits.where(account_id: params[:account_id].to_i)
          end

          if params[:status].present?
            audits = audits.where(status: params[:status].split(','))
          end

          # This table does not use global_uid, so ids are time-ordered
          results = paginate_with_large_count_cache(audits, order: "id DESC")
          render json: presenter.present(results).merge(total_entries: results.total_entries)
        end

        ## ### List active deletion audits for an account
        ## `GET /api/v2/internal/account/:account_id/data_deletion_audits/active.json`
        ##
        ## #### Allowed for
        ##
        ## * Subsystem User
        ##
        ## #### Using curl
        ##
        ## ```bash
        ## curl https://{subdomain}.zendesk.com/api/v2/internal/account/:account_id/data_deletion_audits/active.json \
        ##   -v -u {email_address}:{password}
        ## ```
        ## #### Example Response
        ##
        ## See index action
        allow_parameters :active, :skip
        require_oauth_scopes :active, scopes: [:read], arturo: true
        def active
          render json: presenter.present(account.data_deletion_audits.active)
        end

        ## ### View status of a deletion audit
        ## `GET /api/v2/internal/data_deletion_audits/:id.json`
        ##
        ## #### Allowed for
        ##
        ## * Subsystem User
        ##
        ## #### Using curl
        ##
        ## ```bash
        ## curl https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits/{id}.json \
        ##   -v -u {email_address}:{password}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 200 OK
        ##
        ## {
        ##   "data_deletion_audit": {
        ##     "id": 1
        ##     "shard_id": null
        ##     "reason": "cancellation"
        ##     "status": "waiting"
        ##     "created_at": 2015-02-18 22:39:54
        ##     "started_at": null
        ##     "completed_at": null
        ##     "url": "https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits/1.json",
        ##     "jobs": [...]
        ##   }
        ## }
        ## ```
        ##
        allow_parameters :show, :skip
        def show
          render json: presenter.present(data_deletion_audit)
        end

        ## ### Schedule an account hard deletion
        ## `PATCH /api/v2/internal/data_deletion_audits/:id/enqueue`
        ##
        ## #### Allowed for
        ##
        ## * Subsystem User
        ##
        ## #### Using curl
        ##
        ## ```bash
        ## curl -X PATCH  https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits/{id}/enqueue.json \
        ##   -v -u {email_address}:{password}
        ## ```
        ##
        ## #### Example Response
        ##
        ## ```http
        ## Status: 202 ACCEPTED
        ## Location: https://{subdomain}.zendesk.com/api/v2/internal/data_deletion_audits/{id}.json,
        ## ```
        ##
        allow_parameters :enqueue, :skip
        require_oauth_scopes :enqueue, scopes: [:write], arturo: true
        def enqueue
          if data_deletion_audit && data_deletion_audit.enqueue!
            head :accepted, location: api_v2_internal_data_deletion_audit_url(data_deletion_audit)
          else
            head :internal_server_error
          end
        end

        private

        def set_account
          params[:account_id] ||= params[:id] # routing hack
        end

        def account
          @account ||= ::Account.with_deleted { ::Account.find(params[:account_id]) }
        end

        def data_deletion_audit
          @data_deletion_audit ||= DataDeletionAudit.find_by_id(params[:id])
        end

        def presenter
          @presenter ||= Api::V2::Internal::DataDeletionAuditPresenter.new(nil, url_builder: self)
        end

        def new_data_deletion_audit
          audit = account.data_deletion_audits.new
          audit.reason = move_reason
          audit.shard_id = params[:shard_id].to_i
          audit
        end

        def move_reason
          return 'moved' if params[:reason] == 'moved'
          return 'canceled' if params[:reason] == 'canceled'
          raise 'Invalid or missing reason provided'
        end
      end
    end
  end
end
