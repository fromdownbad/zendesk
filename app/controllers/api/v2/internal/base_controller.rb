module Api
  module V2
    module Internal
      class BaseController < Api::V2::BaseController
        include AllowedParameters
        skip_before_action :verify_authenticity_token
        skip_before_action :enforce_support_is_active!
        allow_only_subsystem_users :zendesk, :monitor

        # This is temporary DB exception, return 503
        rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::TemporaryException do |exception|
          render status: :service_unavailable, json: {
            error: exception.class.to_s,
            description: exception.message,
            backtrace: exception.backtrace
          }
        end

        protected

        def set_api_version_header
          response.headers["X-Zendesk-API-Version"] = "internal"
        end

        def set_account_setting(key, value)
          param = current_account.settings.lookup(key)
          message = "Could not update setting '#{key}' for account ##{current_account.id}"

          if param.value.to_s.eql?(value.to_s)
            raise ActiveRecord::RecordNotSaved, "#{message}: already set to #{value}"
          end

          current_account.settings.send(key.to_s + "=", value)
          param.changed? ? param.save! : (raise ActiveRecord::RecordNotSaved, message)
        end

        def allow_locked_accounts(&block)
          ::Account.without_locking(&block)
        end
      end
    end
  end
end
