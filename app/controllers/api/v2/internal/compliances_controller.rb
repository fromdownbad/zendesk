module Api
  module V2
    module Internal
      class CompliancesController < Api::V2::Internal::BaseController
        allow_parameters :index, :skip
        def index
          render json: presenter.present(compliances)
        end

        private

        def compliances
          @compliances ||= Compliance.all
        end

        def presenter
          @presenter ||= Api::V2::Internal::CompliancePresenter.new(current_user, url_builder: self)
        end
      end
    end
  end
end
