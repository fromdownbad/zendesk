module Api
  module V2
    module Internal
      class SecuritySettingsController < Api::V2::Internal::BaseController
        include Zendesk::Accounts::SettingsCacheSupport
        include Api::V2::Internal::SecuritySettingsErrorsFormatterHelper

        allow_only_subsystem_users :centraladminapp, :zopim, :metropolis
        allow_only_subsystem_user :passport, only: [:show]
        allow_bime_user only: [:show]

        around_action :disable_account_settings_kasket, only: [:update]

        before_action :requester_exists, :require_requester_is_admin_or_owner, only: :update
        before_action :require_requester_is_owner, if: :action_requires_owner?

        allow_parameters :show, {}
        def show
          render json: presenter.present(current_account)
        end

        allow_parameters :update,
          requester_id: Parameters.integer,
          admins_can_set_user_passwords: Parameters.boolean,
          email_agent_when_sensitive_fields_changed: Parameters.boolean,
          assumption_duration: Parameters.enum('day', 'week', 'month', 'year', 'always', 'off', 'not_selected', 'already_set'),
          session_timeout: Parameters.enum('5', '10', '15', '20', '25', '30', '480', '20160'),
          ip: {
            ip_ranges: Parameters.string,
            ip_restriction_enabled: Parameters.boolean,
            enable_agent_ip_restrictions: Parameters.boolean,
          },
          authentication: {
            agent: {
              security_policy_id: Parameters.enum('100', '200', '300', '400'),
              google_login: Parameters.boolean,
              office_365_login: Parameters.boolean,
              zendesk_login: Parameters.boolean,
              remote_login: Parameters.boolean,
              enforce_sso: Parameters.boolean,
              remote_bypass: Parameters.enum('1', '2'),
              password: {
                password_history_length: Parameters.enum('0', '3', '4', '5', '6', '7'),
                password_length: Parameters.enum('5', '6', '7', '8', '9', '10', '11', '12'),
                password_complexity: Parameters.enum('0', '1', '2'),
                password_in_mixed_case: Parameters.boolean,
                failed_attempts_allowed: Parameters.enum('3', '4', '5', '6', '7', '8', '9', '10'),
                max_sequence: Parameters.enum('3', '4', '5'),
                disallow_local_part_from_email: Parameters.boolean,
                password_duration: Parameters.enum('0', '30', '60', '90', '180', '365')
              }
            },
            end_user: {
              security_policy_id: Parameters.enum('100', '200', '300'),
              google_login: Parameters.boolean,
              office_365_login: Parameters.boolean,
              zendesk_login: Parameters.boolean,
              remote_login: Parameters.boolean,
              twitter_login: Parameters.boolean,
              facebook_login: Parameters.boolean,
              enforce_sso: Parameters.boolean
            },
            two_factor_enforce: Parameters.boolean
          },
          remote_authentications: {
            saml: {
              is_active: Parameters.boolean,
              remote_login_url: Parameters.string,
              remote_logout_url: Parameters.string,
              ip_ranges: Parameters.string,
              fingerprint: Parameters.string,
              priority: Parameters.enum('0', '1')
            },
            jwt: {
              is_active: Parameters.boolean,
              remote_login_url: Parameters.string,
              remote_logout_url: Parameters.string,
              ip_ranges: Parameters.string,
              update_external_ids: Parameters.boolean,
              shared_secret: Parameters.string,
              priority: Parameters.enum('0', '1')
            }
          }

        def update
          api_settings_update.perform

          if api_settings_update.errors.any?
            render json: { errors: hashify_errors(api_settings_update.errors) }, status: :unprocessable_entity
          else
            if current_account.has_update_account_associations_for_security_settings_api?
              render json: presenter.present(api_settings_update.account), status: :ok
            else
              render json: presenter.present(ActiveRecord::Base.on_master { current_account.reload }), status: :ok
            end
          end
        end

        private

        def api_settings_update
          @api_settings_update ||= ::Zendesk::Accounts::Security::ApiSettingsUpdate.new(current_user, params)
        end

        def presenter
          @presenter ||= Api::V2::Internal::SecuritySettingsPresenter.new(current_user, url_builder: self)
        end

        def requester
          @requester ||= current_account.users.find_by_id(params[:requester_id])
        end

        def requester_exists
          if requester.nil?
            render_invalid_requester_id('Requester does not exist')
          end
        end

        def require_requester_is_admin_or_owner
          unless requester_is_owner? || requester_is_admin?
            render_invalid_requester_id('Requester does not have entitlements to update security settings')
          end
        end

        def require_requester_is_owner
          unless requester_is_owner?
            render_invalid_requester_id('Requester is not owner, and is trying to change an owner-only setting: admins_can_set_user_passwords')
          end
        end

        def action_requires_owner?
          params.key? :admins_can_set_user_passwords
        end

        def render_invalid_requester_id(description)
          render(
            status: :forbidden,
            json: {
              error: 'InvalidRequesterId',
              description: description
            }
          )
        end

        def requester_is_admin?
          Zendesk::StaffClient.new(current_account).admin?(requester.id)
        end

        def requester_is_owner?
          current_account.owner_id == requester.id
        end
      end
    end
  end
end
