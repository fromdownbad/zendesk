require 'channels/user_mapper'

module Api
  module V2
    module Internal
      class UsersController < Api::V2::Internal::BaseController
        allow_subsystem_user :profile_service, only: [:find_user_identity, :create_or_update]
        allow_subsystem_user :collaboration, only: [:create_or_update]
        allow_subsystem_user :sunshine_conversations, only: [:create_or_update]
        allow_subsystem_user :hawaii, only: [:find_user_identity]

        before_action :ensure_user_is_present, only: [:create_or_update]

        ## ### Social Identity Lookup (find or create with social identity for SSO)
        ## `POST /api/v2/internal/users/find_or_create_with_social_identity.json?`
        ##
        ## This is a special case internal API that allows you to find users by twitter or facebook identity and external_id
        ## if the user does not exist it will create a user and the social identity.
        ##
        ## The special case supported here, is one required for SSO of end user from social networks.
        ## This is used by zendesk_auth and in the future passport.
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        SOCIAL_IDENTITY_PARAMS = {
          external_id:    Parameters.string,
          channel:        Parameters.enum('twitter', 'facebook', 'any_channel'),
          access_token:   Parameters.string,
          allow_creation: Parameters.boolean
        }.freeze

        allow_parameters :find_or_create_user_by_social_identity, social_identity: SOCIAL_IDENTITY_PARAMS
        def find_or_create_user_by_social_identity
          user = channels_user_mapper.find_user || create_with_social_identity

          if user
            render json: presenter.present(user)
          else
            # will occur when !current_account.open? and allow_creation is false
            head :unprocessable_entity
          end
        end

        ## ### User Identity Lookup (find user identity associated with an email)
        ## `GET /api/v2/internal/users/find_user_identity_by_email?email=minimum_end_user%40aghassipour.com`
        ##
        ## This is a special case internal API that allows you to find user identity by email.
        ## if the user identity does not exist it will return a 404 not found.
        ##
        ## The special case supported here, is one required for SSO of users using jwt, saml in warden's SSO strategy.
        ## This is used by zendesk_auth and in the future passport.
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        allow_parameters :find_user_identity_by_email, email: Parameters.string
        def find_user_identity_by_email
          identity = current_account.user_identities.email.find_by_value(params[:email])

          if identity
            render json: identity_presenter.present(identity)
          else
            head :not_found
          end
        end

        ## ### User Identity Lookup
        ## `GET /api/v2/internal/users/find_user_identity?type=phone&value=12345678`
        ##
        ## This is a special case internal API that allows you to find a user identity by type and value.
        ## If a matching user identity does not exist, it will return a 404 not found.
        ##
        ## This endpoint is being specifically built out for the Sunshine Profile Service, as it needs
        ## to look up users by their identities of all types.
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        allow_parameters :find_user_identity, type: Parameters.string, value: Parameters.string
        def find_user_identity
          identity_type = UserIdentity::IDENTITY_TYPE_MAP[params[:type]]
          return head(:bad_request) unless identity_type

          identity = current_account.user_identities.where(type: identity_type).find_by_value(params[:value])

          if identity
            render json: identity_presenter.present(identity)
          else
            head :not_found
          end
        end

        ## ### Create Or Update User
        ## `POST /api/v2/internal/users/create_or_update.json`
        ##
        ## Creates a user if the user does not alerady exist, or updates and existing user
        ## identified by ID, external ID or identities.
        ##
        ## #### Allowed For
        ##  * Subsystem users
        ##
        CREATE_OR_UPDATE_USER_PARAMETER = {
          id: Parameters.bigid,
          name: Parameters.string,
          external_id: Parameters.string | Parameters.integer,
          identities: Parameters.array(Parameters.map(
            type:    Parameters.string,
            value:   Parameters.string
          )),
          skip_verify_email: Parameters.boolean,
          overwrite_existing_name: Parameters.string
        }.freeze

        allow_parameters :create_or_update,
          user: CREATE_OR_UPDATE_USER_PARAMETER,
          requester_id: Parameters.bigid
        def create_or_update
          requester_user = params[:requester_id].present? ? requester : current_user
          initializer = internal_upsert_initializer(requester_user)

          is_new_user = initializer.user.new_record?
          return deny_access unless requester_user.can?(is_new_user ? :create : :edit, initializer.user)

          user = initializer.apply
          user.active_brand_id ||= current_brand.id if is_new_user
          Zendesk::SupportUsers::User.new(user).save! if initializer.requires_save?

          render json: { user: { id: user.id, name: user.name} }, status: is_new_user ? :created : :ok
        rescue Zendesk::Users::UpsertInitializer::UpsertError
          render json: users_error.merge(details: initializer.error), status: :unprocessable_entity
        rescue Zendesk::Users::UpsertInitializer::RequesterIdNotFound
          render json: requester_error, status: :unprocessable_entity
        end

        private

        def create_with_social_identity
          if external_user_params[:allow_creation]
            channels_user_mapper.create_user
          else
            Rails.logger.info "[Internal::UsersController] not creating user because account: #{current_account} does not allow creation"
            nil
          end
        end

        def identity_presenter
          @identity_presenter ||= Api::V2::IdentityPresenter.new(current_user, url_builder: self)
        end

        def presenter
          @presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self)
        end

        def external_user_params
          @external_user_params ||= params.require(:social_identity)
        end

        def channels_user_mapper
          ::Channels::UserMapper.new(
            current_account,
            user:         {
              id: external_user_params[:external_id],
              id_str: external_user_params[:external_id]
            },
            type:         external_user_params[:channel],
            access_token: external_user_params[:access_token],
            should_fetch: true
          )
        end

        def ensure_user_is_present
          unless params[:user].present?
            raise Zendesk::UnknownAttributeError, 'missing user parameter'
          end
        end

        def internal_upsert_initializer(requester_user)
          Zendesk::Users::UpsertInitializer.new(current_account, requester_user, params)
        end

        def requester
          @requester ||= current_account.users.find(params[:requester_id])
        rescue ActiveRecord::RecordNotFound
          raise Zendesk::Users::UpsertInitializer::RequesterIdNotFound
        end

        def users_error
          { error: 'RecordInvalid', description: 'Record validation errors' }
        end

        def requester_error
          { error: 'RequesterIdNotFound', description: 'Requester not found' }
        end
      end
    end
  end
end
