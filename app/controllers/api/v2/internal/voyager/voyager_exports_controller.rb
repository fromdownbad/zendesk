module Api
  module V2
    module Internal
      module Voyager
        class VoyagerExportsController < Api::V2::Internal::BaseController
          allow_parameters :notify, :skip
          def notify
            JobsMailer.deliver_job_complete(user, identifier, subject(user), BaseExportJob.message(params[:url], user))
            head :ok
          end

          private

          def subject(user)
            I18n.t('txt.admin.models.jobs.report_feed_job.subject', locale: user.translation_locale)
          end

          def user
            @user ||= current_account.users.find(params[:user_id])
          end

          def identifier
            "account_id:#{current_account.id};voyager_exporter"
          end
        end
      end
    end
  end
end
