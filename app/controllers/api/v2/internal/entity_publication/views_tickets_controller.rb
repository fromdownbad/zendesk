# frozen_string_literal: true

require 'zendesk_database_support/sensors/aurora_cpu_sensor'

module Api::V2::Internal::EntityPublication
  class ViewsTicketsController < Api::V2::Internal::BaseController
    skip_around_action :delay_touching if RAILS4 # This is adding large, empty transactions
    # This is a subdomain-less endpoint. The account id is passed as a param,
    # so skip these checks
    skip_before_action :prevent_missing_account_access
    skip_before_action :prevent_inactive_account_access
    skip_after_action  :ensure_csrf_token_into_shared_session # If an account has no routes this check blows up. It's not needed for intra-service comms

    allow_subsystem_user :entity_republisher, only: [:index, :update]
    before_action :check_batch_size!

    ZE = Zendesk::EntityPublication
    MAX_DB_CPU = 50.0

    rescue_from ZE::DatabaseBackoffError do |e|
      Rails.logger.info("[Republisher #{self.class.name}] Database over capacity: #{e.db_cpu_util}% util")
      response.headers['X-DB-CPU'] = e.db_cpu_util
      head :too_many_requests
    end

    rescue_from NotImplementedError do
      head :service_unavailable
    end

    VIEW_TICKET_ASSOCIATIONS =
      [:assignee, :requester, :submitter, :brand, :group,
       :organization, :shared_tickets, :ticket_form, :ticket_prediction, :ticket_schedule, :translation_locale].freeze

    allow_parameters :index,
      account_id: Parameters.integer,
      ids: Parameters.array(Parameters.integer) | Parameters.nil,
      tombstone_ids: Parameters.array(Parameters.integer) | Parameters.nil

    def index
      # 404 means we lose messages but can roll the consumers out progressively
      return head :not_found unless current_account.has_views_entity_republisher_rollout?
      # 500 means we don't loose messages but block the consumers
      return head :service_unavailable unless current_account.has_views_entity_republisher?

      @publisher = api_publisher

      republish_entities, tombstoned_entites = republish
      response_json = { 'entities' => republish_entities, 'tombstones' => tombstoned_entites }
      render json: response_json, status: :ok
    end

    allow_parameters :update,
      account_id: Parameters.integer,
      ids: Parameters.array(Parameters.integer) | Parameters.nil,
      tombstone_ids: Parameters.array(Parameters.integer) | Parameters.nil

    def update
      # 404 means we lose messages but can roll the consumers out progressively
      return head :not_found unless current_account.has_views_entity_republisher_rollout?
      # 500 means we don't loose messages but block the consumers
      return head :service_unavailable unless current_account.has_views_entity_republisher?

      @publisher = escape_publisher
      republish_entities, _tombstoned_entites = republish

      if (republish_entities&.count || 0) < entity_ids.count
        render json: { 'missing_ids' => 'true' }, status: :ok
      else
        head :ok
      end
    end

    private

    def republish
      current_account.on_shard do
        ActiveRecord::Base.on_master.without_kasket do
          db_cpu = database_sensor.cpu_utilization.to_f
          raise ZE::DatabaseBackoffError, db_cpu if db_cpu > MAX_DB_CPU

          return with_instrumentation { find_entities }
        end
      end
    rescue ActiveRecord::AdapterNotSpecified
      # Account is in a different pod. This shouldn't happen, but if the requester
      # sends the wrong account it's important we return 400, as 500 will block the consumer
      raise ActiveRecord::RecordNotFound
    end

    # Find and emit the entites, return the sets
    def find_entities
      [
        entities,
        tombstones
      ]
    end

    def entities
      entities = lookup(entity_ids)
      @publisher.publish(entities, ignore_changes: true)
    end

    def tombstones
      tombstone_entities = lookup_tombstones(tombstone_ids)
      @publisher.publish(tombstone_entities, force_tombstone: true)
    end

    def lookup(entity_ids)
      return unless entity_ids.present?

      Ticket.unscoped.preload(VIEW_TICKET_ASSOCIATIONS).
        where(account_id: current_account.id, id: entity_ids)
    end

    def lookup_tombstones(tombstone_ids)
      return if tombstone_ids.blank?

      tombstone_ids.map { |id| tombstone_entity.new(current_account.id, id) }
    end

    def escape_publisher
      ViewsObservers::EntityPublisher.new(
        ViewsObservers::TicketObserver::TOPIC,
        'views_entity_stream',
        ViewsEncoders::ViewsTicketProtobufEncoder,
        ViewsObservers::TicketChangedMatcher
      )
    end

    def api_publisher
      ViewsObservers::EntityPublisher.new(
        ViewsObservers::TicketObserver::TOPIC,
        'views_entity_stream',
        ViewsEncoders::ViewsTicketProtobufEncoder,
        ViewsObservers::TicketChangedMatcher,
        ViewsObservers::ApiProducer.new
      )
    end

    def current_account
      @current_account ||= ::Account.find(params[:account_id])
    end

    def tombstone_entity
      Struct.new(:account_id, :id)
    end

    def check_batch_size!
      head 413 if (entity_ids.size + tombstone_ids.size) > 1000
    end

    def entity_ids
      @entity_ids ||= params[:ids]&.uniq&.map(&:to_i) || []
    end

    def tombstone_ids
      @tombstone_ids ||= params[:tombstone_ids]&.uniq&.map(&:to_i) || []
    end

    def with_instrumentation
      published_entites, tombstoned_entities =
        statsd.time('republish_time') do
          yield
        end

      published_count = published_entites&.size.to_i
      tombstoned_count = tombstoned_entities&.size.to_i

      statsd.count('republished_entites', published_count)
      statsd.count('republished_tombstones', tombstoned_count)

      Rails.logger.info "[Republisher #{self.class.name}] published: #{published_count} ids "\
        "#{tombstoned_count} tombstone ids. Missed #{entity_ids.size - published_count} entities"

      [published_entites, tombstoned_entities]
    end

    def statsd
      @statsd ||= Zendesk::StatsD::Client.new(namespace: ['support', 'views_entity_stream'])
    end

    def database_sensor
      @database_sensor ||= ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.new
    end
  end
end
