module Api
  module V2
    module Internal
      class AccountsController < Api::V2::Internal::BaseController
        around_action :allow_locked_accounts, only: [:clear_cache, :sync_support_product]

        skip_before_action :prevent_inactive_account_access, only: :sync_support_product

        ## ### List Accounts
        ## `GET /api/v2/internal/accounts.json`
        ##
        ## This is a special case internal API that allows you to find accounts that
        ## a given support.zendesk.com user may be on
        ##
        ## The special case supported here, is one required by the Zendesk Monitor, such that we
        ## can see if a user has other accounts with us
        ##
        ## #### Allowed For
        ##
        ##  * Subsystem Users
        ##
        ## #### Example Response
        ##
        ## {
        ##   "accounts": [
        ##     { "id": 31, "subdomain": "narnia", "name": "Narnia", "max_agents": 18, "plan": "Regular" },
        ##     ...
        ##   ]
        ## }
        allow_parameters :index, :skip # TODO: make stricter
        def index
          render json: presenter.present(accounts)
        end

        skip_before_action :prevent_missing_account_access, only: [:clear_cache]
        allow_parameters   :clear_cache, account_id: Parameters.integer
        ## ### Account cache clearing
        ## `PUT /api/v2/internal/accounts/clear_cache?account_id=X`
        ##
        ## Lock the account
        ##
        ## #### Allowed for
        ##  * subsystem user: 'zendesk'
        ##
        ## #### Example Response
        ##
        ## 200 OK

        require_oauth_scopes :clear_cache, scopes: [:write], arturo: true
        def clear_cache
          account = ::Account.find(params[:account_id])

          Rails.logger.info("clear_cache clearing kasket cache")
          account.clear_kasket_cache!

          # This request isn't guaranteed to be pod local.
          # Exodus requests cache clears for newly moved accounts on
          # the old pod, meaning we won't always have the current shard
          # available.
          if ActiveRecord::Base.shard_names.include?(account.shard_id)
            ActiveRecord::Base.on_shard(account.shard_id) do
              Rails.logger.info("clear_cache clearing cache on shard")
              # AccountSettings are kasket cached, we need to pull the most recent version
              account.clear_reflections_kasket_cache!
              # A locked account may be in an invalid state, so we shouldn't update it
              unless account.is_locked?
                account.settings.account_cache_version += 1
                account.save(validate: false)
              end
            end
          else
            Rails.logger.info("clear_cache available shards: #{ActiveRecord::Base.shard_names}")
            Rails.logger.info("clear_cache account shard: #{account.shard_id}")
          end

          head :ok
        end

        ## ### Sync Support Product
        ## `POST /api/v2/internal/accounts/sync_support_product?account_id=X`
        ##
        ## #### Allowed for
        ##  * subsystem user: 'zendesk', 'monitor'
        ##
        ## #### Example Response
        ##
        ## 200 OK

        allow_parameters :sync_support_product, account_id: Parameters.integer
        def sync_support_product
          account = ::Account.find(params[:account_id])
          BackfillAccountProducts.perform(account_ids: [account.id], dry_run: false)
          head :ok
        end

        ## ### Sync Account Roles
        ## `POST /api/v2/internal/accounts/sync_account_roles?account_id=X`
        ##
        ## #### Allowed for
        ##  * subsystem user: 'zendesk', 'monitor'
        ##
        ## #### Example Response
        ##
        ## 200 OK

        allow_parameters :sync_account_roles, account_id: Parameters.integer
        def sync_account_roles
          account = ::Account.find(params[:account_id])
          Omnichannel::AccountRolesSyncJob.enqueue(account_id: account.id)
          head :ok
        end

        ## ### Sync Account Users Entitlements
        ## `POST /api/v2/internal/accounts/sync_account_users_entitlements?account_id=X&user_id=Y`
        ##
        ## #### Allowed for
        ##  * subsystem user: 'zendesk', 'monitor'
        ##
        ## #### Example Response
        ##
        ## 200 OK

        allow_parameters :sync_account_users_entitlements, account_id: Parameters.integer, user_id: Parameters.integer
        def sync_account_users_entitlements
          account = ::Account.find(params[:account_id])
          # Sync entitlements for all products
          if params[:user_id]
            user = account.on_shard { ::User.find(params[:user_id]) }
            Omnichannel::EntitlementsSyncJob.enqueue(account_id: account.id, user_id: user.id, products: ['support', 'guide', 'explore', 'talk'])
          else
            Omnichannel::AccountUsersEntitlementsSyncJob.enqueue(account_id: account.id)
            Omnichannel::ExploreAccountSyncJob.enqueue(account_id: account.id)
            Omnichannel::GuideAccountSyncJob.enqueue(account_id: account.id)
            Omnichannel::TalkAccountSyncJob.enqueue(account_id: account.id)
          end

          head :ok
        end

        private

        def account_ids
          @account_ids ||= begin
            return [] if params[:email].blank?
            users = ::User.zendesk_search(
              with: { user_email: params[:email], is_active: 1, user_role: [Role::AGENT.id, Role::ADMIN.id] },
              select: "id, account_id", collapse_filters: true,
              per_page: 100, shard: :all
            )

            users.compact.map(&:account_id).uniq
          end
        end

        def scope
          @scope ||= ::Account.active.exclude_sandbox.exclude_test_expired
        end

        def accounts
          @accounts ||= scope.where(id: account_ids).to_a
        end

        def presenter
          @presenter ||= Api::V2::AccountPresenter.new(
            current_user,
            url_builder: self,
            internal: true,
            includes: []
          )
        end
      end
    end
  end
end
