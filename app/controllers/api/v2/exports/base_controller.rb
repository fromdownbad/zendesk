class Api::V2::Exports::BaseController < Api::V2::BaseController
  before_action :require_admin!
  before_action :check_valid_start_time!

  EXPORT_LIMIT = 1000
  REDUCED_EXPORT_LIMIT = 500
  SAMPLE_EXPORT_LIMIT = 50

  protected

  def throttle_export_request_unless_can_by_pass_rate_limits
    return if can_bypass_rate_limits?
    Prop.throttle!(:incremental_exports, current_account.id, threshold: current_account.api_incremental_exports_rate_limit)
  end

  def segment_request?
    request.env['zendesk.segment.trusted_request']
  end

  def ip_override_request?
    bime_request? || segment_request?
  end

  def can_bypass_rate_limits?
    ip_override_request? || current_user.is_system_user?
  end

  def check_valid_start_time!
    if params[:start_time].blank?
      raise ActionController::ParameterMissing, 'start_time'
    end
  end

  def start_time
    params[:start_time].to_i
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [])
  end

  def instrument_export_with_count(exported_records_key, &block)
    time_taken, exported_data = base_instrument(&block)

    num_exported = exported_data[exported_records_key].count
    statsd_client.count(base_metric_name + ".num_objects", num_exported, tags: request_tags)
    statsd_client.timing(base_metric_name + ".time_per_object", time_taken / num_exported, tags: request_tags)

    # Try to find if we have more than EXPORT_LIMIT on one :generated_timestamp
    if exported_records_key == :tickets && exported_data[exported_records_key].count > EXPORT_LIMIT
      timestamp = exported_data[exported_records_key].last[:generated_timestamp]
      Rails.logger.warn "Incremental export full timestamp #{timestamp} holds more than #{EXPORT_LIMIT} tickets \
        for account #{current_account.id}. This can cause issues and should be fixed by a backfill"
      statsd_client.increment("incremental_api.full_timestamps_found", tags: ["type:tickets"])
    end

    exported_data
  end

  def instrument_export(&block)
    _, exported_data = base_instrument(&block)
    exported_data
  end

  private

  def load_limit_explore_exports(resource)
    if Zendesk::UnicornLoadRateLimiting.unicorn_load_exceeds?(explore_export_load_limit_percent)
      Rails.application.config.statsd.client.increment('explore_export_throttled', tags: ["resource:#{resource}"])

      render json: {
        error: "UnicornLoadAboveThreshold",
        description: "Unicorn utilization is above threshold of #{explore_export_load_limit_percent} for #{resource}"
      }, status: 429
    end
  end

  def explore_export_load_limit_percent
    @explore_export_load_limit_percent ||= Arturo::Feature.find_feature(:explore_export_load_limit_percent).try(:deployment_percentage) || 0
  end

  def base_instrument
    time = Time.now
    exported_data = yield
    time_taken = (Time.now - time) * 1000

    statsd_client.timing(base_metric_name, time_taken, tags: request_tags)

    [time_taken, exported_data]
  end

  def base_metric_name
    controller_path.gsub(/\//, '.')
  end

  def request_tags
    tags = []
    tags << "action:#{action_name}"
    tags << "gooddata:#{gooddata_request?}"
    tags << "status:#{status}"
    tags
  end
end
