class Api::V2::Exports::GooddataController < Api::V2::Exports::BaseController
  skip_before_action :enforce_request_format
  before_action :check_if_allowed
  skip_before_action :check_valid_start_time!, only: [:account_options, :show_many_tickets]

  require_gooddata_user

  EXPORT_LIMIT = 1000
  GOODDATA_CUSTOM_FIELD_LIMIT = 60
  GOODDATA_ENTERPRISE_CUSTOM_FIELD_LIMIT = 200

  ## ### Get information about tickets updated since a given point in time
  ## `GET /api/v2/gooddata/tickets_export.json`
  ##
  ## #### Allowed For
  ##
  ##  * GoodData
  ##
  ## #### Request Parameters
  ##
  ##  * start_time: The time of the oldest ticket you are interested in. Tickets modified on or since this time will be returned.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/gooddata/tickets_export.json?start_time=1332034771
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "end_time": 1332034771,
  ##   "field_headers": {
  ##     "group_name": "Group",
  ##     "nice_id": "Id",
  ##     "created_at": "Created at",
  ##     ...
  ##   },
  ##   "results": [
  ##     {
  ##       "group_name": "Support",
  ##       "nice_id": 2,
  ##       "created_at": "2012-03-17 18:37:46 -0700",
  ##       ...
  ##      },
  ##      ...
  ##   ]
  ## }
  ## ```
  allow_parameters :tickets_export, start_time: Parameters.integer | Parameters.float
  require_oauth_scopes :ticket_export, scopes: [:read], arturo: true
  def tickets_export
    render json: Yajl::Encoder.encode(instrument_export { presenter.present(exporter) })
  end

  allow_parameters :account_options, :skip # TODO: make stricter
  require_oauth_scopes :account_options, scopes: [:read], arturo: true
  def account_options
    render json: Yajl::Encoder.encode(options_presenter.present(current_account))
  end

  ## ### Show Multiple Tickets (includes deleted tickets)
  ## `GET /api/v2/gooddata/show_many_tickets.json?ids={ids}`
  ##
  ## Accepts a comma separated list of ticket ids to return.
  ##
  ## This endpoint will return up to 100 tickets records.
  ##
  ##
  ## #### Allowed For:
  ##
  ##  * Gooddata
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/gooddata/show_many_tickets.json?ids=1,2,3 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## See [Listing Tickets](#example-response)
  allow_parameters :show_many_tickets, ids: Parameters.ids
  require_oauth_scopes :show_many_tickets, scopes: [:read], arturo: true
  def show_many_tickets
    render json: tickets_presenter.present(many_tickets)
  end

  protected

  def options_presenter
    Api::V2::Exports::GooddataAccountOptionsPresenter.new(current_user, url_builder: self)
  end

  def presenter
    @presenter ||= Api::V2::Exports::GooddataTicketPresenter.new(current_user, url_builder: self, limit: EXPORT_LIMIT)
  end

  def tickets_presenter
    @presenter ||= begin
      klass = Api::V2::Tickets::TicketPresenter
      klass.new(current_user, url_builder: self, includes: includes, comment_presenter: comment_presenter)
    end
  end

  def comment_presenter
    @comment_presenter ||= begin
      Api::V2::Tickets::GenericCommentPresenter.new(current_user, url_builder: self)
    end
  end

  def many_tickets
    @many_tickets ||= Ticket.with_deleted do
      current_account.tickets.where(nice_id: many_ids).all_with_archived
    end
  end

  def exporter
    @exporter ||= Zendesk::Export::IncrementalTicketExport.for_account(current_account,
      Time.at(start_time),
      EXPORT_LIMIT,
      show_metrics: true,
      custom_field_limit: custom_fields_limit)
  end

  def check_if_allowed
    allowed = current_account.subscription.has_incremental_export? || current_account.subscription.has_hourly_incremental_export?
    head(:forbidden) unless allowed
  end

  def custom_fields_limit
    return GOODDATA_ENTERPRISE_CUSTOM_FIELD_LIMIT if current_account.subscription.has_hourly_incremental_export?
    GOODDATA_CUSTOM_FIELD_LIMIT
  end

  def check_user_ip_restrictions
    super unless gooddata_request?
  end
end
