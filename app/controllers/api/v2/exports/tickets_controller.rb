class Api::V2::Exports::TicketsController < Api::V2::Exports::BaseController
  before_action :check_start_time_not_too_recent!

  EXPORT_LIMIT = 1000
  SAMPLE_LIMIT = 50

  ## ### Incremental Ticket Export
  ## `GET /api/v2/exports/tickets.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Request Parameter
  ##
  ##  * start_time: The time of the oldest ticket you're interested in, expressed as an
  ## [Unix epoch time](http://www.epochconverter.com/). The request returns tickets modified on or since this time.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/exports/tickets.json?start_time=1332034771 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "end_time": 1332034771,
  ##   "next_page": "https://domain.zendesk.com/api/v2/exports/tickets.json?start_time=1332034771",
  ##   "field_headers": {
  ##     "group_name": "Group",
  ##     "id": "Id",
  ##     "created_at": "Created at",
  ##     ...
  ##   },
  ##   "results": [
  ##     {
  ##       "group_name": "Support",
  ##       "id": 2,
  ##       "created_at": "2012-02-02T04:31:29Z",
  ##       ...
  ##      },
  ##      ...
  ##   ]
  ## }
  ## ```
  ##
  ## #### Rate limit
  ##
  ## You're allowed to make only one API call to this endpoint every minute. We'll return up to 1,000 tickets
  ## per request. See the Sample Incremental Tickets endpoint below for a way to test this API without getting
  ## rate limited all the time. The rate limiting mechanism behaves identically to the one described in our
  ## [API introduction](./introduction). We recommend that you obey the `Retry-After` header values as also
  ## explained in the [API introduction](./introduction).
  ##
  ## Requests with a `start_time` less than 5 minutes old will also be rejected.
  ##
  ## #### Usage Notes
  ##
  ## The API consumer should call this API to initially export a complete list of ticket details from a help desk,
  ## and periodically poll the API to incrementally export ticket details for tickets that have been updated since
  ## the previous poll. This API should not be used to frequently export a full list of all tickets.
  ##
  ## The `start_time` value is compared to the ticket's `generated_timestamp` value, not the ticket's `updated_at` value. The
  ## `generated_timestamp` value is updated more frequently. For example, if somebody changes the requester associated with
  ## the ticket, the ticket's `generated_timestamp` value is updated but not its `updated_at` value.
  ##
  ## The API doesn't protect against duplicate tickets if you make multiple calls to it. The query boils down to
  ## tickets whose `generated_timestamp` is after or equal to the `start_time` parameter. The same ticket can be
  ## included in multiple calls if the ticket is updated after each call and the start time of subsequent calls is
  ## before the update.
  ##
  ## To avoid duplicates in subsequent calls, the endpoint returns the time of the last ticket update as
  ## an `end_time` value and as the `start_time` value in a ``next_page`` URL. You can use these values to poll the endpoint
  ## again without having to calculate the last ticket update returned by the previous call. Example response:
  ##
  ## ```http
  ## {
  ##   "results": [
  ##     ...
  ##     {
  ##       "generated_timestamp": 1405469030,
  ##       ...
  ##     }
  ##   ],
  ##   ...
  ##   "end_time": 1405469030,
  ##   "next_page": "https://{subdomain}.zendesk.com/api/v2/exports/tickets.json?start_time=1405469030"
  ## }
  ## ```
  ##
  ## #### Deleted tickets
  ##
  ## The incremental tickets API also returns deleted tickets. If exporting tickets to another system, you can use the API
  ## to identify deleted tickets and decide whether to include them in the export.
  ##
  allow_parameters :index, start_time: Parameters.integer | Parameters.float
  def index
    if current_account.has_api_disable_v2_ticket_exports?
      head :gone
      return
    end

    unless current_user.is_system_user?
      options = {}
      Prop.throttle!(:incremental_ticket_exports, current_account.id, options)
    end

    render json: instrument_export { presenter.present(exporter) }
  end

  ## ### Sample Incremental Tickets
  ## `GET /api/v2/exports/tickets/sample.json`
  ##
  ## This endpoint is meant to be used only for testing the incremental export format. It's more relaxed in terms of
  ## rate limiting, but will only return up to 50 records. Other than this, it's identical to the above API.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/exports/tickets/sample.json?start_time=1332034771 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  allow_parameters :sample, start_time: Parameters.integer | Parameters.float
  require_oauth_scopes :sample, scopes: [:read], arturo: true
  def sample
    Prop.throttle!(:sample_incremental_ticket_exports, current_account.id)
    render json: sample_presenter.present(sample_exporter)
  end

  protected

  def presenter
    @presenter ||= Api::V2::Exports::TicketPresenter.new(current_user, url_builder: self, limit: EXPORT_LIMIT)
  end

  def exporter
    @exporter ||= Zendesk::Export::IncrementalTicketExport.for_account(current_account, Time.at(start_time), EXPORT_LIMIT, exporter_options)
  end

  def sample_presenter
    @sample_presenter ||= Api::V2::Exports::TicketPresenter.new(current_user, url_builder: self, limit: SAMPLE_LIMIT)
  end

  def sample_exporter
    @sample_exporter ||= Zendesk::Export::IncrementalTicketExport.for_account(current_account, Time.at(start_time), SAMPLE_LIMIT, exporter_options)
  end

  def exporter_options
    {
      show_metrics: true,
      map_nice_id: true,
      additional_columns: [['Assignee id', :assignee_id], ['Assignee external id', :assignee_external_id], ['Group id', :group_id]],
      v2_url: true
    }
  end

  def check_start_time_not_too_recent!
    if Time.now - Time.at(start_time) < 5.minutes
      render json: {error: "InvalidValue", description: "Too recent start_time. Use a start_time older than 5 minutes"}, status: :unprocessable_entity
    end
  end

  def start_time
    params[:start_time].to_i
  end
end
