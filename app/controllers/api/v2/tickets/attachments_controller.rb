class Api::V2::Tickets::AttachmentsController < Api::V2::BaseController
  require_that_user_can! :delete, :ticket, only: [:redact]

  ## ### Redact Comment Attachment
  ## `PUT /api/v2/tickets/{t_id}/comments/{c_id}/attachments/{attachment_id}/redact.json`
  ##
  ## Redaction allows you to permanently remove attachments from an existing comment on a ticket.
  ## Once removed from a comment, the attachment is replaced with an empty "redacted.txt" file.
  ##
  ## The redaction is permanent. It is not possible to undo redaction or see what was removed.
  ## Once a ticket is closed, redacting its attachments is no longer possible.
  ##
  ## #### Allowed for
  ##
  ## * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/comments/{comment_id}/attachments/{id}/redact.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X PUT -d '{}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "attachment": {
  ##     "id": 65432
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## For inline attachments, you can use the `include_inline_images` parameter to obtain the inline attachment ID.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl 'https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/comments.json?include_inline_images=true' \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {  "comments": [
  ##    {
  ##      "attachments": [
  ##        {
  ##          "content_type": "image/png",
  ##          "content_url": "https://{subdomain}.zendesk.com/attachments/token/1234567/?name=example.png",
  ##          "file_name": "example.png",
  ##          "height": 1251,
  ##          "id": 364525328832,
  ##          "inline": true,
  ##          ...
  ##        }
  ##      ]
  ##      "id": 35436,
  ##      "value": "My ticket comment",
  ##      ...
  ##    },
  ##    {
  ##      "id": 35437,
  ##      "value": "My other ticket comment",
  ##      ...
  ##    }
  ##  ]
  ## }
  ## ```
  ##
  ## Use the obtained inline attachment ID to redact the inline image the same as you would a normal attachment comment by inserting the attachment ID into the attachment redaction call.

  allow_parameters :redact, id: Parameters.bigid, comment_id: Parameters.bigid, ticket_id: Parameters.bigid
  require_oauth_scopes :redact, scopes: [:write], arturo: true
  def redact
    if ticket.inoperable?
      render json: { message: I18n.t('txt.api.v2.tickets.attachments.archived_ticket') }, status: :bad_request
      return
    end

    unless redacted_attachment = attachment.redact!
      render json: { message: I18n.t('txt.api.v2.tickets.attachments.cannot_be_redacted') }, status: :bad_request
      return
    end

    ticket.will_be_saved_by(current_user)
    ticket.audit.events << AttachmentRedactionEvent.new(attachment_id: redacted_attachment.id)
    ticket.add_domain_event(
      AttachmentRedactedFromCommentProtobufEncoder.new(attachment, comment).to_safe_object
    )
    ticket.save!

    render json: presenter.present(attachment)
  end

  private

  def presenter
    @presenter ||= Api::V2::AttachmentPresenter.new(current_user, url_builder: self)
  end

  def attachment
    @attachment ||= comment.attachments.find(params[:id])
  end

  def comment
    @comment ||= ticket.comments.find(params[:comment_id])
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end
end
