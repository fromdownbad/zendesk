class Api::V2::Tickets::CommentsController < Api::V2::BaseController
  include Zendesk::Tickets::Comments::EmailCommentSupport

  require_that_user_can! :view_private_content, :ticket
  require_that_user_can! :edit, :ticket, only: :make_private
  require_that_user_can! :delete, :ticket, only: :redact

  allow_subsystem_user :collaboration, only: [:index]
  allow_zopim_user only: [:index]
  allow_cors_from_zendesk_external_domains :index

  allow_parameters :all, ticket_id: Parameters.bigid

  before_action :throttle_full_email_body, only: :full_email_body
  before_action :ensure_has_email_no_delimiter, only: :report

  # Sort lookup must be defined for the api to honor the `sort_order` parameter.
  SORT_LOOKUP = {}.freeze

  ## ### List Comments
  ## `GET /api/v2/tickets/{ticket_id}/comments.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available parameters
  ##
  ## | Name                     | Type                | Required  | Comments
  ## | ------------------------ | --------------------| --------- | -------------------
  ## | `sort_order`             | string              | no        | One of `asc`, `desc`. Defaults to `asc`
  ## | `include_inline_images`  | boolean             | no        | Default is false. When true, inline images are also listed as attachments in the response
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/comments.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "comments": [
  ##     {
  ##       "id": 35436,
  ##       "value": "My ticket comment",
  ##       ...
  ##     },
  ##     {
  ##       "id": 35437,
  ##       "value": "My other ticket comment",
  ##       ...
  ##     }
  ##   ]
  ## }
  ##
  ## ```
  allow_parameters :index, include_inline_images: Parameters.boolean
  def index
    if stale?(etag: [ticket, sort_order], last_modified: ticket.updated_at)
      render json: presenter.present(comments)
    end
  end

  ## ### List Email CCs for a Comment
  ##
  ## `GET /api/v2/tickets/{ticket_id}/comments.json?include=users`
  ##
  ## You can list email CCs by side-loading users.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "comments": [
  ##     {
  ##       "id": 35436,
  ##       "value": "My email ticket comment",
  ##       "via": {
  ##         channel: "email",
  ##         source: {
  ##           from: {
  ##             address: "enduser@example.com",
  ##             name: "End-user",
  ##             original_recipients: [
  ##               "agent@company.zendesk.com",
  ##               "enduser@gmail.com",
  ##               "deletedenduser@example.com"
  ##             ]
  ##           },
  ##           to: {
  ##             name: "Support",
  ##             address: "support@company.zendesk.com"
  ##             email_ccs: [
  ##               847390,
  ##               1234567,
  ##               "deletedenduser@example.com"
  ##             ]
  ##           },
  ##           rel: null
  ##         }
  ##       },
  ##       ...
  ##     },
  ##     {
  ##       "id": 35437,
  ##       "value": "My agent UI ticket comment",
  ##       "via": {
  ##         channel: "web",
  ##         source: {
  ##           from: { },
  ##           to: {
  ##             email_ccs: [
  ##               847390,
  ##               1234567,
  ##               "deletedendusername"
  ##             ]
  ##           },
  ##           rel: null
  ##         }
  ##       },
  ##       ...
  ##     }
  ##   ],
  ##   "users": [
  ##     {
  ##       "id":                    847390,
  ##       "name":                  "Johnny Agent",
  ##       "role":                  "agent",
  ##       ...
  ##     },
  ##     {
  ##       "id":                    1234567,
  ##       "name":                  "Jane Enduser",
  ##       "role":                  "end-user",
  ##       ...
  ##     },
  #      ...
  ##   ]
  ## }
  ##
  ## ```
  ##
  ## <span class="alert alert-block alert-warning"><strong>Note</strong>: If the comment source is email, a deleted user will be represented as the CCd email address. If the comment source is anything else, a deleted user will be represented as the username.</span>

  ## ### Redact String in Comment
  ## `PUT /api/v2/tickets/{ticket_id}/comments/{id}/redact.json`
  ##
  ## Permanently removes words or strings from a ticket comment. Specify
  ## the string to redact in an object with a `text` property. Example: `'{"text": "987-65-4320"}'`.
  ## The characters of the word or string are replaced by the ▇ symbol.
  ##
  ## If the comment was made by email, the endpoint also attempts to redact the string
  ## from the original email retained by Zendesk for audit purposes.
  ##
  ## **Note**: If you use the rich text editor, support for redacting
  ## formatted text (bold, italics, hyperlinks) is limited.
  ##
  ## Redaction is permanent. You can't undo the redaction or see *what* was removed.
  ## Once a ticket is closed, you can no longer redact strings from its comments.
  ##
  ## To use this endpoint, the "Agents can delete tickets" option must be enabled
  ## in the Zendesk Support admin interface at **Admin** > **Settings** > **Agents**.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/comments/{id}/redact.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X PUT \
  ##   -d '{"text": "987-65-4320"}'
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "comment": {
  ##     "id":    35436,
  ##     "value": "My social security number is ▇▇▇▇!",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## ```http
  ## Status: 400 Bad Request
  ##
  ## Text was not found in comment.
  ##
  ## ```
  allow_parameters :redact, id: Parameters.bigid, text: Parameters.string, redact_full_comment: Parameters.boolean
  require_oauth_scopes :redact, scopes: [:write], arturo: true
  def redact
    ticket.transaction do
      return head(:bad_request) unless ticket.redact_comment!(current_user, comment, params[:text], params[:redact_full_comment])
    end
    render json: presenter.present(comment)
  end

  ## ### Make Comment Private
  ## `PUT /api/v2/tickets/{ticket_id}/comments/{id}/make_private.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/comments/{id}/make_private.json \
  ##   -v -u {email_address}:{password} -X PUT -d '{}' -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :make_private, id: Parameters.bigid
  require_oauth_scopes :make_private, scopes: [:write], arturo: true
  def make_private
    comment.ticket.will_be_saved_by(current_user)
    comment.update_attributes!(is_public: false)
    head :ok
  end

  # TODO: Strong Bad - will decide when to include this in public documentation
  # h3. Fetch full email body for an audit event created via the Mail channel
  # `GET /api/v2/tickets/{ticket_id}/comments/{id}/full_email_body.json`
  #
  # h4. Allowed For
  #
  #  * Agents
  #
  # h4. Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/comments/{id}/full_email_body.json \
  #   -H "Content-Type: application/json" -v -u {email_address}:{password}
  # ```
  #
  # h4. Example Response
  # ```http
  # Status: 200 OK
  #
  # {
  #   "comment": {
  #      "full_email_body": "<div class=\"zd-comment\"><span>A Comment</span></div>"
  #    }
  # }
  #
  # ```
  allow_parameters :full_email_body, ticket_id: Parameters.bigid, id: Parameters.bigid
  def full_email_body
    raise ActiveRecord::RecordNotFound unless current_account.has_email_no_delimiter?

    if stale?(etag: [comment], last_modified: comment.updated_at)
      raise ActiveRecord::RecordNotFound unless supported_via_type?
      render json: email_comment_presenter.present(prepare_full_body_from_raw_email)
    end
  end

  # TODO: Strong Bad - will decide when to include this in public documentation
  # h3. Report an issue with reply parsing by creating a z1 ticket
  # `POST /api/v2/tickets/{ticket_id}/comments/{id}/report.json`
  #
  # h4. Allowed For
  #
  #  * Agents
  #
  # h4. Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/comments/{id}/report.json \
  #   -H "Content-Type: application/json" -v -u {email_address}:{password}
  #   -X POST -d '{"description": "ticket description", "message_id": "<message_id>", "json_email_identifier": "json_identifier", "raw_email_identifier": "raw_identifier"}'
  # ```
  #
  # h4. Example Response
  # ```http
  # Status: 200 OK
  #
  # {
  # "ticket": {
  #     "id":      35436,
  #   }
  # }
  #
  # ```
  allow_parameters :report,
    id: Parameters.bigid,
    ticket_id: Parameters.bigid,
    description: Parameters.string,
    message_id: Parameters.string,
    json_email_identifier: Parameters.string,
    raw_email_identifier: Parameters.string
  def report
    return head :bad_request unless ticket.present? && comment.present?
    return head :conflict unless should_report_mail_rendering_issue?

    response = report_mail_rendering_issue

    if response.success?
      json = email_comment_issue_presenter.present(response.body["request"])
      update_audit_metadata!("z1_request_url" => json[:ticket][:z1_request_url])
      create_dev_comment_on_ticket(json[:ticket][:ticket_id])

      render json: json
    else
      head :unprocessable_entity
    end
  end

  private

  def presenter
    @presenter ||= Api::V2::Tickets::CommentPresenter.new(
      current_user,
      url_builder: self,
      includes: includes,
      relative_attachment_urls: lotus_request?,
      include_inline_images: include_inline_images
    )
  end

  def email_comment_presenter
    @email_comment_presenter ||= Api::V2::Tickets::EmailCommentPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def email_comment_issue_presenter
    @email_comment_issue_presenter ||= Api::V2::Tickets::EmailCommentIssuePresenter.new(current_user, url_builder: self, includes: includes)
  end

  def comment
    @comment ||= ticket.comments.find(params[:id])
  end

  def comments
    @comments ||= paginate(ticket.comments)
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  def sort_order
    @sort_order = (params[:sort_order] || "asc").downcase
  end

  def include_inline_images
    @include_inline_images = params[:include_inline_images] || false
  end

  def throttle_full_email_body
    throttle_key = "#{current_user.id}_#{ticket.id}"
    Prop.throttle!(:email_full_body_fetches, throttle_key)
  end

  def ensure_has_email_no_delimiter
    head(:unauthorized) unless current_account.has_no_mail_delimiter_enabled?
  end
end
