module Api
  module V2
    class ActivitiesController < Api::V2::BaseController
      ## ### List Activities
      ## `GET /api/v2/activities.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## Lists ticket activities in the last 30 days affecting the agent making the request.
      ## Also sideloads the following arrays of user records:
      ##
      ## - actors - All actors involved in the listed activities
      ## - users - All users involved in the listed activities
      ##
      ## #### Request Parameters
      ##
      ## You can pass an optional `since` parameter to return the activities since a specific
      ## date. The `since` parameter takes a UTC time in the ISO 8601 format. Example:
      ## "2013-04-03T16:02:46Z".
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/activities.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "activities": [
      ##     {
      ##       "id":          35,
      ##       "url":         "https://company.zendesk.com/api/v2/activities/35.json",
      ##       "verb":        "tickets.assignment",
      ##       "title":       "John Hopeful assigned ticket #123 to you",
      ##       "user":        { ... },
      ##       "actor":       { ... },
      ##       "created_at":  "2019-03-05T10:38:52Z",
      ##       "updated_at":  "2019-03-05T10:38:52Z"
      ##     },
      ##     {
      ##       "id":          45,
      ##       "url":         "https://company.zendesk.com/api/v2/activities/45.json",
      ##       "verb":        "tickets.comment",
      ##       "title":       "John Hopeful commented in ticket #44",
      ##       "user":        { ... },
      ##       "actor":       { ... },
      ##       "created_at":  "2019-03-05T11:32:44Z",
      ##       "updated_at":  "2019-03-05T11:32:44Z"
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, since: Parameters.date_time | Parameters.nil_string
      def index
        render json: presenter.present(activities)
      end

      ## ### Show Activity
      ## `GET /api/v2/activities/{activity_id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## Lists a specific activity.
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/activities/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "activity": {
      ##     "id":          45,
      ##     "url":         "https://company.zendesk.com/api/v2/activities/45.json",
      ##     "verb":        "tickets.comment",
      ##     "title":       "John Hopeful commented in ticket #44",
      ##     "user":        { ... },
      ##     "actor":       { ... },
      ##     "created_at":  "2019-03-05T11:32:44Z",
      ##     "updated_at":  "2019-03-05T11:32:44Z"
      ##   }
      ## }
      ## ```
      resource_action :show

      private

      # Should we ever get more types of activities we'll need to make an activity collection presenter
      def presenter
        @presenter ||= Api::V2::TicketActivityPresenter.new(current_user, url_builder: self)
      end

      def activity
        @activity ||= scope.find(params[:id])
      end

      def scope
        @scope ||= current_user.activities
      end

      def activities
        @activities ||= begin
          activity_scope = scope
          activity_scope = activity_scope.since(params[:since]) if params[:since].present?
          paginate(activity_scope)
        end
      end
    end
  end
end
