module Api
  module V2
    class CustomStatusesController < Api::V2::BaseController
      before_action :require_custom_statuses_feature!

      before_action :require_custom_statuses_enabled!, except: [:index, :show]
      before_action :require_admin!, except: [:index, :show]

      CUSTOM_STATUS_PARAMETER = {
        status_category: Parameters.enum(*CustomStatus::CATEGORY_KEYWORDS),
        active: Parameters.boolean,
        agent_label: Parameters.string,
        end_user_label: Parameters.string,
        description: Parameters.string
      }.freeze

      CUSTOM_STATUS_UPDATE_PARAMETER = CUSTOM_STATUS_PARAMETER.except(:status_category).freeze

      # List Custom Statuses
      #
      # `GET /api/v2/custom_statuses.json`
      allow_parameters :index, {}
      def index
        render json: presenter.present(custom_statuses)
      end

      # Show Custom Status
      #
      # `GET /api/v2/custom_statuses/{custom_status_id}.json`
      resource_action :show

      # Create Custom Status
      #
      # `POST /api/v2/custom_statuses.json`
      allow_parameters :create, custom_status: CUSTOM_STATUS_PARAMETER
      def create
        new_custom_status.save!
        render json: presenter.present(new_custom_status), status: :created, location: presenter.url(new_custom_status)
      end

      # Update Custom Status
      #
      # `PUT /api/v2/custom_statuses.json`
      allow_parameters :update, id: Parameters.bigid, custom_status: CUSTOM_STATUS_UPDATE_PARAMETER
      def update
        updated_custom_status.save!
        render json: presenter.present(updated_custom_status)
      end

      # Delete Custom Status
      #
      # `DELETE /api/v2/custom_statuses/{custom_status_id}.json`
      allow_parameters :destroy, id: Parameters.bigid
      def destroy
        if custom_status.soft_delete
          default_delete_response
        else
          render json: presenter.present_errors(custom_status), status: :unprocessable_entity
        end
      end

      protected

      def custom_status_params
        params.require(:custom_status)
      end

      def new_custom_status
        @new_custom_status ||= current_account.custom_statuses.build(custom_status_params)
      end

      def updated_custom_status
        @updated_custom_status ||= begin
          custom_status.attributes = custom_status_params

          custom_status
        end
      end

      def presenter
        @presenter ||= Api::V2::CustomStatusPresenter.new(current_user, url_builder: self)
      end

      def custom_status
        @custom_status ||= custom_statuses.find(params[:id])
      end

      def custom_statuses
        @custom_statuses ||= current_account.custom_statuses
      end

      def require_custom_statuses_feature!
        return if current_user&.is_system_user?

        deny_access unless current_account.has_custom_statuses?
      end

      def require_custom_statuses_enabled!
        return if current_user&.is_system_user?

        deny_access unless current_account.has_custom_statuses_enabled?
      end
    end
  end
end
