class Api::V2::WorkspacesController < Api::V2::BaseController
  before_action :ensure_manage_permissions, if: -> { current_account.has_permissions_contextual_workspaces_manage? }
  before_action :require_admin!, except: [:index], unless: -> { current_account.has_permissions_contextual_workspaces_manage? }
  before_action :require_admin_or_agent!, only: [:index], unless: -> { current_account.has_permissions_contextual_workspaces_manage? }

  before_action :workspace_available?
  before_action { throttle_endpoint!(:v2_workspaces_controller) }

  CONDITION_PARAMETER = {
    field:    Parameters.string,
    operator: Parameters.string,
    value:    Parameters.anything
  }.freeze

  APP_PARAMETER = {
    id: Parameters.bigid,
    expand: Parameters.boolean,
    position: Parameters.integer
  }.freeze

  WORKSPACE_PARAMS = {
    title:                      Parameters.string,
    description:                Parameters.string,
    macros:                     Parameters.array(Parameters.bigid),
    apps:                       [APP_PARAMETER].freeze,
    ticket_form_id:             Parameters.integer,
    conditions:                 Parameters.map(
      all:      [CONDITION_PARAMETER].freeze,
      any:      [CONDITION_PARAMETER].freeze
    ).freeze,
    activated:                  Parameters.boolean,
    prefer_workspace_app_order: Parameters.boolean
  }.freeze

  ## ### List Workspaces
  ## `GET /api/v2/workspaces.json`

  ## #### Allowed For

  ##  * Admins, Agents

  ## #### Using curl

  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces.json
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces.json?per_page=2
  ## ```

  ## #### Example Response

  ## ```http
  ## Status: 200 OK

  ##   {
  ##      "workspaces": [
  ##   {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/workspaces.json",
  ##     "id": 3133,
  ##     "title": "Test Workspace 1",
  ##     "description": "Test rules",
  ##     "macros": [
  ##       360005374974,
  ##       ...,
  ##     ],
  ##     "macro_ids": [
  ##       360005374974,
  ##       ...,
  ##     ],
  ##     "apps": [
  ##       {
  ##         "id": 360000080413,
  ##         "expand": false,
  ##         "position": 1
  ##       },
  ##       ...
  ##     ],
  ##     "ticket_form_id": 360000014173,
  ##     "position": 1,
  ##     "activated": true,
  ##     "prefer_workspace_app_order": true,
  ##     "selected_macros": [
  ##       {
  ##         "url": "https://{subdomain}.zendesk.com/api/v2/macros/360005374974.json",
  ##         "id": 360005374974,
  ##         "title": "Close and redirect to topics",
  ##         "active": true,
  ##         "updated_at": "2018-11-08T22:27:00Z",
  ##         "created_at": "2018-02-08T23:45:30Z",
  ##         "position": 9999,
  ##         "description": null,
  ##         "actions": [
  ##           {
  ##             "field": "status",
  ##             "value": "solved"
  ##           },
  ##           ...
  ##         ],
  ##         "restriction": {
  ##           "type": "Group",
  ##           "id": 360002226093,
  ##           "ids": [
  ##             360002226093
  ##          ]
  ##         },
  ##         "usage_7d": 0
  ##       },
  ##       ...
  ##     ],
  ##     "updated_at": "2018-12-17T22:37:40Z",
  ##     "created_at": "2018-11-13T19:08:13Z",
  ##     "conditions": {
  ##       "all": [
  ##         {
  ##           "field": "ticket_form_id",
  ##           "operator": "is",
  ##           "value": "360000014173"
  ##         },
  ##       ],
  ##       "any": []
  ##     }
  ##   },
  ## ....
  ## ],
  ##   "next_page": null,
  ##   "previous_page": null,
  ##   "count": 1
  ## }
  ## ```

  allow_parameters :index, no_macros: Parameters.boolean
  def index
    presenter.no_macros = params[:no_macros]
    render json: presenter.present(workspaces)
  end

  ## ### Show Workspace
  ## `GET /api/v2/workspaces/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "workspace": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/workspaces.json",
  ##     "id": 3133,
  ##     "title": "Test Workspace 1",
  ##     "description": "Test rules",
  ##     "macro_ids": [
  ##       360005374974
  ##     ],
  ##     "apps": [],
  ##     "ticket_form_id": 360000014173,
  ##     "position": 1,
  ##     "activated": true,
  ##     "prefer_workspace_app_order": true,
  ##     "selected_macros": [
  ##       {
  ##         "url": "https://{subdomain}.zendesk.com/api/v2/macros/360005374974.json",
  ##         "id": 360005374974,
  ##         "title": "Close and redirect to topics",
  ##         "active": true,
  ##         "updated_at": "2018-11-08T22:27:00Z",
  ##         "created_at": "2018-02-08T23:45:30Z",
  ##         "position": 9999,
  ##         "description": null,
  ##         "actions": [
  ##           {
  ##             "field": "status",
  ##             "value": "solved"
  ##           },
  ##           ...
  ##         ],
  ##         "restriction": {
  ##           "type": "Group",
  ##           "id": 360002226093,
  ##           "ids": [
  ##             360002226093
  ##          ]
  ##         },
  ##         "usage_7d": 0
  ##       }
  ##     ],
  ##     "conditions": {
  ##       "all": [
  ##         {
  ##           "field": "ticket_form_id",
  ##           "operator": "is",
  ##           "value": "360000014173"
  ##         }
  ##       ],
  ##       "any": []
  ##     },
  ##     "updated_at": "2018-12-17T22:37:40Z",
  ##     "created_at": "2018-11-13T19:08:13Z"
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.bigid
  def show
    workspace = current_account.workspaces.where(id: params[:id]).first

    if workspace
      render json: presenter.present(workspace)
    else
      render json: { status: :not_found }
    end
  end

  ## ### Create Workspace
  ## `POST /api/v2/workspaces.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '
  ## {
  ##   "workspace": {
  ##     "title": "Test Workspace 1",
  ##     "description": "Test rules",
  ##     "ticket_form_id": 360000014173,
  ##     "macros": [360005374974],
  ##     "conditions": {
  ##       "all": [
  ##         {
  ##           "field": "ticket_form_id",
  ##           "operator": "is",
  ##           "value": "360000014173"
  ##         },
  ##       ],
  ##       "any": []
  ##     }
  ##   }
  ## }' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/workspaces/{id}.json
  ##
  ## {
  ##   "workspace": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/workspaces.json",
  ##     "id": 3133,
  ##     "title": "Test Workspace 1",
  ##     "description": "Test rules",
  ##     "macro_ids": [
  ##       360005374974
  ##     ],
  ##     "apps": [],
  ##     "ticket_form_id": 360000014173,
  ##     "position": 1,
  ##     "activated": true,
  ##     "prefer_workspace_app_order": true,
  ##     "selected_macros": [
  ##       {
  ##         "url": "https://{subdomain}.zendesk.com/api/v2/macros/360005374974.json",
  ##         "id": 360005374974,
  ##         "title": "Close and redirect to topics",
  ##         "active": true,
  ##         "updated_at": "2018-11-08T22:27:00Z",
  ##         "created_at": "2018-02-08T23:45:30Z",
  ##         "position": 9999,
  ##         "description": null,
  ##         "actions": [
  ##           {
  ##             "field": "status",
  ##             "value": "solved"
  ##           },
  ##           ...
  ##         ],
  ##         "restriction": {
  ##           "type": "Group",
  ##           "id": 360002226093,
  ##           "ids": [
  ##             360002226093
  ##          ]
  ##         },
  ##         "usage_7d": 0
  ##       }
  ##     ],
  ##     "conditions": {
  ##       "all": [
  ##         {
  ##           "field": "ticket_form_id",
  ##           "operator": "is",
  ##           "value": "360000014173"
  ##         }
  ##       ],
  ##       "any": []
  ##     },
  ##     "updated_at": "2018-12-17T22:37:40Z",
  ##     "created_at": "2018-11-13T19:08:13Z"
  ##   }
  ## }
  ## ```
  allow_parameters :create, workspace: WORKSPACE_PARAMS
  def create
    workspace = current_account.workspaces.new

    if workspace.save_with(params[:workspace], user: current_user)
      render json: presenter.present(workspace)
    else
      render json: { errors: workspace.errors.full_messages, status: :unprocessable_entity }
    end
  rescue StandardError => e
    render json: { errors: e.message, status: :unprocessable_entity }
  end

  ## ### Update Workspace
  ## `PUT /api/v2/workspaces/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{"workspace": {
  ##           "title": "Test Workspace 1",
  ##           "description": "Test rules",
  ##           "ticket_form_id": 360000014173,
  ##           "macro_ids": [360005374974],
  ##           "conditions": {
  ##             "all": [
  ##               {
  ##                 "field": "status",
  ##                 "operator": "is",
  ##                 "value": "pending"
  ##               },
  ##             ],
  ##             "any": []
  ##           }
  ##       }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/workspaces/47.json
  ##
  ## {
  ##   "workspace": {
  ##     "id":                    47,
  ##     "url":                   "https://company.zendesk.com/api/v2/workspaces/47.json",
  ##     "title":                 "Test Workspace 1",
  ##     "description":           "Test rules",
  ##     "ticket_form_id":        "360000014173",
  ##     "macro_ids":             [360005374974],
  ##     "position":              4,
  ##     "activated":             true,
  ##     "conditions": {
  ##       "all": [
  ##         {
  ##           "field": "status",
  ##           "operator": "is",
  ##           "value": "pending"
  ##         },
  ##       ],
  ##       "any": []
  ##     }
  ##     "created_at":            "2018-02-08T23:45:30Z",
  ##     "updated_at":            "2018-11-08T22:27:00Z"
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.bigid, workspace: WORKSPACE_PARAMS
  def update
    workspace = current_account.workspaces.where(id: params[:id]).first

    if workspace.save_with(params[:workspace], user: current_user)
      render json: presenter.present(workspace)
    else
      render json: { errors: workspace.errors.full_messages, status: :unprocessable_entity }
    end
  rescue StandardError => e
    render json: { errors: e.message, status: :unprocessable_entity }
  end

  ## ### Delete Workspace
  ## `DELETE /api/v2/workspaces/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    workspace = current_account.workspaces.where(id: params[:id]).first

    if workspace.try(:destroy)
      render json: { head: :ok }
    else
      if workspace.nil?
        render json: { status: :not_found }
      else
        render json: { errors: workspace.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  ## ### Bulk Delete Workspaces
  ## `DELETE /api/v2/workspaces/destroy_many.json`
  ##
  ## Deletes the workspaces corresponding to the provided comma-separated list of
  ## IDs.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces/destroy_many.json?ids=1,2,3 \
  ##   -v -u {email}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  ##
  allow_parameters :destroy_many, ids: Parameters.ids
  def destroy_many
    deleted_workspaces = current_account.workspaces.where(id: many_ids).destroy_all
    if deleted_workspaces.empty?
      render json: { status: :unprocessable_entity }
    else
      render json: { head: :ok }
    end
  end

  ## ### Reorder Workspaces
  ## `PUT /api/v2/workspaces/reorder.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/workspaces/reorder.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{"ids": [12, 32, 48, 60]}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  ##
  allow_parameters :reorder, ids: Parameters.array(Parameters.bigid)
  def reorder
    Workspace.reorder(filter_ids, current_account)
    render json: presenter.present(workspaces)
  rescue StandardError => e
    render json: { errors: e.message, status: :unprocessable_entity }
  end

  allow_parameters :definitions, {}
  def definitions
    render json: definitions_presenter.present(workspace_definitions)
  end

  private

  def filter_ids
    params[:ids].map(&:to_i)
  end

  def workspaces
    current_account.workspaces.includes([selected_macros: :groups])
  end

  def presenter
    @presenter ||= Api::V2::WorkspacePresenter.new(current_user, url_builder: self, includes: includes)
  end

  def require_admin_or_agent!
    deny_access unless current_user.try(:is_admin?) || current_user.try(:is_agent?)
  end

  def workspace_available?
    deny_access unless current_account.has_contextual_workspaces?
  end

  def definitions_presenter
    @definitions_presenter ||= begin
      Api::V2::WorkspaceDefinitionsPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def throttle_endpoint!(controller_identifier)
    Prop.throttle!(controller_identifier, current_account.id,
      threshold: current_account.settings.workspace_controller_threshold)
  end

  def workspace_definitions
    @workspace_definitions ||= begin
      Zendesk::Rules::Definitions.new(
        account:   current_account,
        user:      current_user,
        rule_type: :contextual_workspace
      )
    end
  end

  def ensure_manage_permissions
    if action_name == 'index'
      deny_access unless current_user.is_admin? || current_user.is_agent?
    else
      deny_access unless current_user.can?(:manage, Workspace)
    end
  end
end
