class Api::V2::Integrations::GithubController < Api::V2::BaseController
  GITHUB_ISSUE_EVENTS = ["assigned", "unassigned", "labeled", "unlabeled", "opened", "edited", "milestoned", "demilestoned", "closed", "reopened", "deleted", "created"].freeze

  before_action :verify_master_branch?, only: :create

  # Receives a JSON payload from GitHub that matches a ticket. Turns the payload
  # into a private comment on the ticket (subject to regular restrictions, like status)
  allow_parameters :create, ticket_id: Parameters.bigid, payload: Parameters.anything

  def create
    if tickets.map(&:save).all?
      head(:created)
    else
      Rails.logger.warn("Failed to amend ticket with GitHub update: #{tickets.map { |t| t.errors.full_messages.to_sentence }.join(',')} #{params.inspect}")
      head(:unprocessable_entity)
    end
  end

  private

  def tickets
    @tickets ||= begin
      GithubPayload.new(params).payloads.map do |p|
        github_comment = GithubMessageRenderer.new(p).to_s
        initializer    = Zendesk::Tickets::Initializer.new(
          current_account,
          current_user,
          id: p["ticket_id"].to_i,
          ticket: {via_id: ViaType.GITHUB},
          comment: {value: github_comment, is_public: false}
        )

        initializer.ticket
      end
    end.compact
  end

  def verify_master_branch?
    if params[:payload] && ref = params[:payload]["ref"]
      if ref != "refs/heads/master"
        Rails.logger.info("Payload #{ref} related to non master branch so skipping")
        head(:unprocessable_entity)
      end
    end
  end

  class GithubPayload
    MAX_PAYLOADS = 10
    attr_accessor :payload, :ticket_id

    def initialize(params)
      @payload, @ticket_id = params.values_at(:payload, :ticket_id)
      @payload ||= {}
    end

    def payloads
      @payloads ||= generate_payloads
    end

    private

    def generate_payloads
      if payload.key?("issue")
        [{
           "type"        => "issue",
           "action"      => payload["action"],
           "assignee"    => payload["assignee"],
           "issue"       => payload["issue"],
           "milestone"   => payload["issue"]["milestone"],
           "label"       => payload["label"],
           "user_handle" => payload["sender"]["login"],
           "comment"     => payload["comment"],
           "ticket_id"   => ticket_id
         }]
      elsif payload.key?("pull_request")
        [{
           "type"         => "pull_request",
           "user_handle"  => payload["sender"]["login"],
           "action"       => payload["action"],
           "pull_request" => payload["pull_request"],
           "ticket_id"    => ticket_id
         }]
      elsif payload.key?("comment")
        [{
           "type"        => "comment",
           "user_handle" => payload["sender"]["login"],
           "comment"     => payload["comment"],
           "ticket_id"   => ticket_id
      }]
      else
        commit_payloads
      end
    end

    def commit_payloads
      (payload["commits"] || []).map do |commit|
        next unless commit["message"] =~ /zd#(\d+)/i
        {
          "type"        => "commit",
          "commit"      => commit,
          "comment"     => payload["comment"],
          "ticket_id"   => $1
      }
      end.compact[0..MAX_PAYLOADS]
    end
  end

  class GithubMessageRenderer
    attr_reader :payload

    def initialize(payload)
      @payload = payload || {}
    end

    def to_s
      message =
        case payload["type"]
        when "issue"
          render_issue
        when "pull_request"
          render_pull_request
        when "comment"
          render_comment
        else
          render_commit
        end

      message || begin
        Rails.logger.warn("Unrecognized GitHub structure in payload")
        "Unrecognized GitHub event structure received. Please report this."
      end
    end

    def render_pull_request
      user_handle = payload["user_handle"]
      pull_url    = payload["pull_request"]["html_url"]

      message = "#{user_handle} #{payload["action"]} pull request #{pull_url} referencing this ticket."
      message << "\n\n"
      message << payload["pull_request"]["title"]
      message << "\n\n"
      message << payload["pull_request"]["body"].to_s.strip

      message
    end

    def build_message(user_handle, action, issue_url)
      # Add 'a comment on' for actions from issue_comment events like 'created', 'edited', 'deleted'
      # since those are ambiguous and if make sense for a given action E.g. "created a comment on"
      on_comment = "a comment on " if !payload["comment"].blank? && payload["action"] != "reopened"

      # Build a string for event notification first line
      messages = Array("#{user_handle} #{action} #{on_comment}issue #{issue_url} referencing this ticket.")

      # Add aditional info depending on the action
      messages << payload["issue"]["title"] if action.in? ["opened", "reopened", "created"]
      messages << "Label: #{payload["label"]["name"]}" if action == "labeled"
      messages << "Milestone: #{payload["issue"]["milestone"]["title"]}" if action == "milestoned"
      messages << "Assignee: #{payload["assignee"]["login"]}" if action.in? ["assigned"]
      messages << payload["comment"]["body"] unless payload["comment"].blank?
      messages.compact.join("\n")
    end

    def render_issue
      return unless payload["action"].in? GITHUB_ISSUE_EVENTS
      issue = payload["issue"]

      build_message(payload["user_handle"], payload["action"], issue["html_url"])
    end

    def render_comment
      user_handle  = payload["comment"]["user"]["login"]
      commit_url   = payload["comment"]["html_url"]
      comment_body = payload["comment"]["body"]

      message = "#{user_handle} referenced this ticket a comment on commit #{commit_url}"
      message << "\n\n#{comment_body}"
    end

    def render_commit
      user_handle = payload["commit"]["author"]["username"]
      commit_url  = payload["commit"]["url"]
      commit_body = payload["commit"]["message"]

      message = "#{user_handle} referenced this ticket in commit #{commit_url}"
      message << "\n\n#{commit_body}"
      message
    end
  end
end
