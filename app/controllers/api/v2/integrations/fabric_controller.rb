# This endpoint will be used by the Twitter Fabric integration.

# See https://github.com/twitterdev/3pk/wiki/Provisioning-APIs for details about the requirements
# for the integration.
#
# NOTE: You may require to request access to those docs. If that's the case please contact somebody
# in the Lir team at ask-mobile-services slack channel

class Api::V2::Integrations::FabricController < Api::V2::BaseController
  FABRIC_CLIENT_IDENTIFIER = "twitter_fabric_zendesk".freeze

  include Zendesk::PodRelay
  include Zendesk::Accounts::OfacControllerSupport

  skip_around_action :audit_action
  skip_before_action :authenticate_user,
    :prevent_inactive_account_access,
    :prevent_missing_account_access,
    :require_agent!,
    :verify_authenticity_token

  before_action :require_twitter_token
  before_action :validate_rate_limit

  # ### Creating Account for Fabric
  # `POST https://signup.zendesk.com/api/v2/integrations/fabric.json`
  #
  # #### Allowed For
  #
  #  * Fabric OAuth scope
  #
  # #### Using curl
  #
  # Provisioning a trial account:
  #
  # ```bash
  # curl -X "POST" "https://signup.zendesk.com/api/v2/integrations/fabric" \
  #      -H "Content-Type: application/json" \
  #      -H "Accept: application/json" \
  #      -d '{
  #             "platform": "given-platform",
  #             "name": "Lacota Whitney",
  #             "email": "emailIcwyNEBG3wnG@someplace.com",
  #             "app_name": "my-app",
  #             "bundle_identifier": "com.nlokxvcvsh",
  #             "is_confirmed": "false"
  #          }'
  # ```
  #
  # #### Success Example Response
  #
  # ```http
  # Status: 201 Created
  #
  # {
  #   "id": "100133",
  #     "keys": {
  #       "primary": {
  #         "url": "https://fabric-emailcrazy-domaincom.zendesk-test.com",
  #         "identifier": "7f2825e5dc0c0fa97ac00095252f236aa634e6563649128d",
  #         "client_identifier": "mobile_sdk_client_00de0187505a7a1dd2b4"
  #       }
  #     }
  # }
  # ```
  #
  # * In case the account already exists it'll return HTTP 409 (Conflict)
  # * In case of any error whilst creating the account or the mobile app it'll return HTTP 422 (Unprocessable entity)
  # * In case of unauthorized request it'll return HTTP 403 (Forbidden)
  # * In case of the endpoint is disable (Arturo flag) it'll return HTTP 404 (Not found)
  FABRIC_PARAMETERS = {
    platform: Parameters.string,
    name: Parameters.string,
    email: Parameters.string,
    app_name: Parameters.string,
    bundle_identifier: Parameters.string,
    is_confirmed: Parameters.boolean | Parameters.nil_string
  }.freeze

  allow_parameters :create, FABRIC_PARAMETERS
  require_oauth_scopes :create, scopes: []
  def create
    if Account.exists?(subdomain: generated_subdomain)
      head :conflict
    else
      if new_account.present? && mobile_sdk_app.present?
        render json: presenter.present(mobile_sdk_app, new_account), status: :created
        return
      end
      head :unprocessable_entity
    end
  end

  private

  def require_twitter_token
    unless oauth_access_token && oauth_access_token.global_client.try(:identifier) == FABRIC_CLIENT_IDENTIFIER
      Rails.logger.info "Failed to authenticate Fabric request with params: #{params}"
      head :forbidden
    end
  end

  def generated_subdomain
    @generated_subdomain ||= Accounts::Fabric::SubdomainGenerator.new(params[:email]).subdomain
  end

  def new_account
    @new_account ||= Accounts::Fabric::AccountCreator.create(
      params,
      subdomain: generated_subdomain,
      account_creation_endpoint: api_v2_accounts_path,
      accept_tos_endpoint: api_private_mobile_sdk_tos_accept_path,
      accept_tos: true
    )
  end

  def mobile_sdk_app
    @mobile_sdk_app ||= Accounts::Fabric::MobileSdkAppCreator.create(
      name: params[:app_name],
      account: new_account
    )
  end

  def presenter
    @presenter ||= Api::V2::Integrations::MobileSdkFabricPresenter.new
  end

  def validate_rate_limit
    # Count the requests by the remote ip
    Prop.throttle(:fabric_account_creations, request.remote_ip)
    # Raise an exception if needed, based on Prop.config (config/initializers/prop.rb)
    # The exception is catched on Api::BaseController
    Prop.throttle!(:fabric_account_creations, request.remote_ip)
  end
end
