class Api::V2::Integrations::MsDynamicsPolicyController < Api::V2::BaseController
  skip_before_action :enforce_request_format
  skip_before_action :require_agent!
  skip_before_action :authenticate_user
  before_action :require_enabled_integration!

  ## ### Show Microsoft Dynamics client access policy
  ## `GET /clientaccesspolicy.xml`
  ##
  ## A client access policy must be generated for each Zendesk instance that enables the Microsoft Dynamics integration.
  ## The <domain uri> record must be set to the CRM server address captured in the configuration page.
  ##
  ## #### Allowed For:
  ##
  ##  * All
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/clientaccesspolicy.xml
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ##  <access-policy>
  ##    <cross-domain-access>
  ##      <policy>
  ##        <allow-from http-methods="*" http-request-headers="*">
  ##          <domain uri="ZendeskDynamics.com"/>
  ##        </allow-from>
  ##        <grant-to>
  ##          <resource path="/" include-subpaths="true"/>
  ##        </grant-to>
  ##      </policy>
  ##    </cross-domain-access>
  ##  </access-policy>
  ## ```
  allow_parameters :index, {}
  def index
    render xml: presenter.present_xml(current_account.ms_dynamics_integration)
  end

  private

  def presenter
    @presenter ||= Api::V2::Integrations::MsDynamicsPolicyPresenter.new(current_user, url_builder: self)
  end

  def require_enabled_integration!
    deny_access unless current_account.ms_dynamics_integration.try(:configured?)
  end
end
