class Api::V2::Integrations::JiraController < Api::V2::BaseController
  ## ### Show the JIRA details for a ticket
  ##
  ## `GET /api/v2/tickets/{id}/jira.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/jira.json \
  ##  -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## [{
  ##   "SUMMARY": "Test ticket",
  ##   "KEY": "PROJ-31",
  ##   "URL": "https://project.atlassian.net/browse/PROJ-31",
  ##   ...
  ## }]
  ## ```
  allow_parameters :ticket_details, ticket_id: Parameters.bigid
  require_oauth_scopes :ticket_details, scopes: [:read], arturo: true
  def ticket_details
    render json: presenter.model_json(jira_ticket)
  end

  ## ### Show the JIRA projects for a sharing agreement
  ##
  ## `GET /api/v2/sharing_agreements/{id}/jira.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/sharing_agreements/{id}/jira.json \
  ##  -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## [{
  ##
  ## }]
  ## ```
  allow_parameters :projects, sharing_agreement_id: Parameters.bigid
  require_oauth_scopes :projects, scopes: [:read], arturo: true
  def projects
    render json: presenter.model_json(jira_projects)
  end

  private

  def presenter
    @presenter ||= Api::V2::Integrations::JiraPresenter.new(current_user, url_builder: self)
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  def agreement
    @agreement ||= current_account.sharing_agreements.find(params[:sharing_agreement_id])
  end

  def jira_ticket
    @jira_ticket ||= ticket.shared_with_jira.try(:retrieve_jira_ticket)
  end

  def jira_projects
    @jira_projects ||= agreement.retrieve_jira_projects
  end
end
