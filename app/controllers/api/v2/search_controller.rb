require 'zendesk/search'
require 'faraday'
require 'faraday_middleware'
require 'attr_selector'

class Api::V2::SearchController < Api::V2::BaseController
  # this overrides the sort_order parameter from Api::V2::BaseController to allow 'relevance' (except for export)
  SORT_PARAMS = {
    sort_order: Parameters.enum('ASC', 'asc', 'DESC', 'desc', 'RELEVANCE', 'relevance') | Parameters.nil_string
  }.freeze

  # CBP Constants for Export API to allow filter[type] param
  FILTER_TYPE_PARAMS = {
    filter: (Parameters.integer & Parameters.gte(0)) | Parameters.nil_string | Parameters.map(
      type: Parameters.string
    )
  }.freeze

  include Zendesk::Stats::ControllerMixin
  include Zendesk::Search::ControllerSupport

  skip_before_action :require_agent!, only: :index
  before_action      :require_agent!, unless: :end_user_search_enabled?

  allow_zopim_user only: [:index, :count, :export]
  allow_subsystem_user :knowledge_api, only: [:index, :export]

  before_action :deny_anonymous_access
  before_action :record_search_statistics
  before_action :ensure_api_allow_post_in_search, only: :index
  before_action :ensure_export_api_access, only: :export
  before_action :ensure_valid_query, only: [:index, :export]
  before_action :ensure_deep_paging_limit, except: :export
  before_action :set_search_cache_control_header
  before_action :check_search_short_circuit_limiter
  before_action :export_api_rate_limit, only: :export
  before_action :set_export_span_tags, only: :export

  allow_public_requests :incremental

  treat_as_read_request :index, :count, :export

  allow_cors_from_zendesk_external_domains :index

  def self.allow_sort_parameters(action)
    allow_parameters action, SORT_PARAMS
  end

  def self.allow_filter_type_parameters(action)
    allow_parameters action, FILTER_TYPE_PARAMS
  end

  ## ### List Search Results
  ## `GET /api/v2/search.json?query={search_string}`
  ##
  ## #### Allowed for:
  ##
  ## * Admins, Agents and Light Agents
  ##
  ##
  ## #### Available parameters
  ##
  ## | Name                  | Type                | Required  | Comments
  ## | --------------------- | --------------------| --------- | -------------------
  ## | `query`               | string              | yes       | The search query
  ## | `sort_by`             | string              | no        | One of `updated_at`, `created_at`, `priority`, `status`, or `ticket_type`. Defaults to sorting by relevance
  ## | `sort_order`          | string              | no        | One of `asc` or `desc`. Defaults to `desc`
  ##
  ## Use the ampersand character (&) to append the `sort_by` or `sort_order` parameters to the URL. Example:
  ##
  ## ```
  ## https://subdomain.zendesk.com/api/v2/search.json?query=type:ticket status:closed&sort_by=status&sort_order=desc
  ## ```
  ##
  ## For examples, see [Searching with Zendesk API](https://develop.zendesk.com/hc/en-us/articles/360001074228).
  ##
  ## #### Query
  ##
  ## See [Query basics](#query-basics) above. For details on the query syntax, see the [Zendesk Support search reference](https://support.zendesk.com/hc/en-us/articles/203663226).
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl "https://{subdomain}.zendesk.com/api/v2/search.json" \
  ##   -G --data-urlencode "query=type:ticket status:open" \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## See the [example](#example) above for an example response.
  ##
  allow_parameters :index, query: Parameters.string, disable_helpcenter: Parameters.boolean, by_updated: Parameters.integer, search_elasticsearch: Parameters.boolean, agent_help_center_elasticsearch: Parameters.boolean
  allow_sort_parameters :index
  def index
    query.include_helpcenter = include_help_center?
    query.use_ja_extended_mode = current_user.account.has_search_use_ja_extended_analyzer?
    query.execute(current_user)
    render json: present(query), status: status
  end

  allow_parameters :incremental, query: Parameters.string, disable_helpcenter: Parameters.boolean, by_updated: Parameters.integer, type: Parameters.string, search_elasticsearch: Parameters.boolean, agent_help_center_elasticsearch: Parameters.boolean
  allow_sort_parameters :incremental
  require_oauth_scopes :incremental, scopes: [:read], arturo: true
  def incremental
    query.per_page = DEFAULT_PER_PAGE_FOR_SEARCH_INCREMENTAL unless params[:per_page]
    query.include_helpcenter = current_user.account.help_center_enabled? && params[:disable_helpcenter].nil?
    query.use_ja_extended_mode = current_user.account.has_search_use_ja_extended_analyzer?
    query.use_guide_search_for_articles = current_user.account.has_use_guide_search_in_support?
    query.include_side_conversations = side_conversation_search_enabled?
    query.include_side_conversations_count = side_conversation_search_count_enabled?
    query.facets = true
    query.match_percent = '100%'
    query.problem_bias = 0.6
    query.incremental = true

    query.execute(current_user)
    render json: present(query, :incremental), status: status
  end

  allow_parameters :count, query: Parameters.string, disable_helpcenter: Parameters.boolean, by_updated: Parameters.integer, search_elasticsearch: Parameters.boolean, agent_help_center_elasticsearch: Parameters.boolean
  allow_sort_parameters :count
  def count
    query.include_helpcenter = include_help_center?
    query.use_ja_extended_mode = current_user.account.has_search_use_ja_extended_analyzer?
    query.endpoint = 'things/count'
    query.hl = false

    query.execute(current_user)
    render json: present(query, :count), status: status
  end

  allow_parameters :export, query: Parameters.string
  allow_cursor_pagination_v2_parameters :export
  allow_filter_type_parameters :export
  def export
    query.page_size = params.dig(:page, :size) || DEFAULT_PER_PAGE_FOR_SEARCH
    query.page_after = params.dig(:page, :after)
    query.filter_type = params.dig(:filter, :type)
    query.include_helpcenter = false
    query.use_ja_extended_mode = current_user.account.has_search_use_ja_extended_analyzer?
    query.endpoint = 'export/things'
    query.cursor_based_pagination = true

    query.execute(current_user)
    render json: present(query, :export), status: status
  end

  protected

  def deny_anonymous_access
    deny_access if current_user.is_anonymous_user?
  end

  def present(query, mode = nil)
    if query.success?
      presenter = case mode
                  when :incremental
                    Api::V2::Search::LotusResultsPresenter.new(current_user, default_presenter_options)
                  when :count
                    Api::V2::Search::CountPresenter.new
                  when :export
                    export_presenter_options = default_presenter_options.merge(comment_presenter: comment_presenter)
                    Api::V2::Search::ExportPresenter.new(current_user, export_presenter_options)
                  else
                    result_presenter_options = default_presenter_options.merge(comment_presenter: comment_presenter)
                    Api::V2::Search::ResultsPresenter.new(current_user, result_presenter_options)
      end

      presenter.present(query.result)
    else
      presenter = Api::V2::Search::ErrorsPresenter.new
      presenter.present(query)
    end
  end

  def comment_presenter
    Api::V2::Tickets::GenericCommentPresenter.new(current_user, url_builder: self)
  end

  def status
    if query.success?
      :ok
    elsif query.error.type == :unavailable
      :service_unavailable
    else
      :unprocessable_entity
    end
  end

  def query
    @query ||= super.tap do |query|
      query.per_page = per_page
    end
  end

  def search_params
    super.tap do |params|
      params.order_key = :sort_by
      params.sort_key  = :sort_order
    end
  end

  private

  def include_help_center?
    !current_user.account.has_exclude_articles_from_search_results? &&
      current_user.account.help_center_enabled? &&
      params[:disable_helpcenter].nil?
  end

  def ensure_api_allow_post_in_search
    if request.post?
      head :not_found unless current_account.has_api_allow_post_in_search?
    end
  end

  def ensure_deep_paging_limit
    unless current_account.has_search_return_limit_in_search_service?
      per_page = if params[:per_page].nil?
        params[:action].eql?('incremental') ? DEFAULT_PER_PAGE_FOR_SEARCH_INCREMENTAL : DEFAULT_PER_PAGE_FOR_SEARCH
      else
        params[:per_page]
      end

      if params[:page] && (per_page * params[:page] >= SEARCH_API_PAGINATION_LIMIT)
        query.error = Zendesk::Search::Error.new(ZendeskSearch::QueryError.new("Reached pagination limit"))
        render json: present(query), status: status
      end
    end
  end

  def ensure_export_api_access
    head :forbidden unless current_account.has_search_grant_access_export_api?
  end

  def ensure_valid_query
    if query.string.blank?
      query.error = Zendesk::Search::Error.new(ZendeskSearch::QueryError.new("Please provide a valid search query"))
      render json: present(query), status: status
    end
  end

  def mixed_collection_includes
    @mixed_collection_includes ||=
      if current_account.has_disabled_search_sideload_for_end_users? && current_user.is_end_user?
        {}
      else
        {
          Ticket        => fetch_includes_for(:tickets),
          User          => fetch_includes_for(:users),
          Group         => fetch_includes_for(:groups),
          Organization  => fetch_includes_for(:organizations),
          Entry         => fetch_includes_for(:topics)
        }
      end
  end

  def fetch_includes_for(attr)
    if index = nested_includes.find_index { |item| HashParam.ish?(item) && item.key?(attr) }
      return nested_includes[index][attr]
    end
    []
  end

  def nested_includes
    @nested_includes ||= AttrSelector.parse(params[:include].to_s)
  end

  def default_presenter_options
    {
      url_builder: self,
      includes: includes,
      highlights: query.result.highlights,
      mixed_collection_includes: mixed_collection_includes
    }
  end

  def end_user_search_enabled?
    current_account.has_end_user_search?
  end

  def side_conversation_search_enabled?
    current_user.account.has_include_side_conversations_in_search? &&
      current_user.account.has_side_conversations?
  end

  def side_conversation_search_count_enabled?
    current_user.account.has_include_side_conversations_count_in_search? &&
      current_user.account.has_side_conversations?
  end

  def export_api_rate_limit
    throttle_opts = {}
    throttle_opts[:threshold] = current_account.settings.search_export_api_rate_limit
    Prop.throttle!(:search_export_api, current_account.id, throttle_opts)
  end

  def request_span
    request.env[Datadog::Contrib::Rack::TraceMiddleware::RACK_REQUEST_SPAN] ||
      Datadog.tracer.active_span
  end

  def set_export_span_tags
    if HashParam.ish?(cbp_page = params.dig(:filter)) && cbp_page.dig(:type).present?
      request_span.set_tag('http.request.params.filter.type', cbp_page[:type])
    end
  end
end
