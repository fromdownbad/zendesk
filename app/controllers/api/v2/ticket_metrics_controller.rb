class Api::V2::TicketMetricsController < Api::V2::BaseController
  ## ### List Ticket Metrics
  ## `GET /api/v2/ticket_metrics.json`
  ##
  ## Returns a list of tickets with their metrics.
  ##
  ## Tickets are ordered chronologically by created date, from newest to oldest.
  ## The last ticket listed may not be the absolute oldest ticket in your account
  ## due to [ticket archiving](https://support.zendesk.com/hc/en-us/articles/203657756).
  ##
  ## Archived tickets are not included in the response. See
  ## [About archived tickets](https://support.zendesk.com/hc/en-us/articles/203657756) in
  ## the Support Help Center.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_metrics.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket_metrics": [
  ##     {
  ##       "id": 33,
  ##       "ticket_id": 4343,
  ##       "reopens": 55,
  ##       "replies": 322,
  ##       ...
  ##     }
  ##     {
  ##       "id": 34,
  ##       "ticket_id": 443,
  ##       "reopens": 123,
  ##       "replies": 232,
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    Prop.throttle!(:v2_ticket_metrics_index, current_account.id) if Arturo.feature_enabled_for?(:ticket_metrics_v2_limiter, current_account) && !lotus_request?
    render json: presenter.present(ticket_metrics)
  end

  ## ### Show Ticket Metrics
  ## `GET /api/v2/ticket_metrics/{ticket_metric_id}.json`
  ##
  ## `GET /api/v2/tickets/{ticket_id}/metrics.json`
  ##
  ## Returns a specific metric, or the metrics of a specific ticket.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_metrics/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket_metric": {
  ##     "id": 34,
  ##     "ticket_id": 443,
  ##     "reopens": 123,
  ##     "replies": 232,
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.bigid, ticket_id: Parameters.bigid
  def show
    render json: presenter.present(ticket_metric)
  end

  protected

  def presenter
    @presenter ||= Api::V2::TicketMetricSetPresenter.new(current_user, url_builder: self)
  end

  def ticket_metric
    @ticket_metric ||= Ticket.with_deleted do
      if params[:ticket_id]
        current_account.tickets.find_by_nice_id!(params[:ticket_id]).ticket_metric_set || raise(ActiveRecord::RecordNotFound)
      else
        ticket_metric_sets_scope(include_archived_tickets: true).find(params[:id])
      end
    end
  end

  def ticket_metrics
    request_span = request.env[Datadog::Contrib::Rack::TraceMiddleware::RACK_REQUEST_SPAN]

    @ticket_metrics ||= if current_account.has_ticket_metrics_sets_with_sub_query?
      Rails.logger.info("Getting ticket metric sets with sub-query for account_id: #{current_account.id}")
      request_span&.set_tag("zendesk.ticket_metrics_sets_with_sub_query", true)

      ticket_metric_sets_sub_query
    else
      Rails.logger.info("Getting ticket metric sets without sub-query for account_id: #{current_account.id}")
      request_span&.set_tag("zendesk.ticket_metrics_sets_with_sub_query", false)

      begin
        scope = ticket_metric_sets_scope(include_archived_tickets: false)

        paginate_with_large_count_cache(scope.order("tickets.created_at DESC"))
      end
    end
  end

  def ticket_metric_sets_sub_query
    ticket_query = current_account.tickets.select(:id)
    ticket_ids = paginate_with_large_count_cache(ticket_query.order("tickets.created_at DESC"))

    ticket_metric_sets = current_account.ticket_metric_sets.where(ticket_id: ticket_ids.to_a).order(id: :desc)

    ticket_metric_sets.extend(Zendesk::Tickets::MetricSetsPagination)
    ticket_metric_sets.add_pagination_options(ticket_ids)

    ticket_metric_sets
  end

  def ticket_metric_sets_scope(include_archived_tickets:)
    scope = current_account.ticket_metric_sets
    unless include_archived_tickets
      scope = scope.joins(:ticket)
    end
    scope
  end
end
