class Api::V2::PushNotificationDevicesController < Api::V2::BaseController
  before_action :require_admin!, only: [:destroy_many]

  ## ### Bulk Unregister Push Notification Devices
  ## `POST /api/v2/push_notification_devices/destroy_many.json`
  ##
  ## Unregisters the mobile devices that are receiving push notifications.
  ## Specify the devices as an array of mobile device tokens.
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ##  curl -X "POST" "https://{subdomain}.zendesk.com/api/v2/push_notification_devices/destroy_many.json" \
  ##  -u {email_address}:{password} \
  ##  -H "Content-Type: application/json" \
  ##  -H "Accept: application/json" \
  ##  -d $'{ "push_notification_devices": ["token1", "token2"] }'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :destroy_many, push_notification_devices: Parameters.array(Parameters.string)
  def destroy_many
    push_notification_devices = current_account.device_identifiers.where(token: params[:push_notification_devices]).all
    push_notification_devices.each do |push_notification_device|
      unless push_notification_device.destroy
        Rails.logger.warn "Could not delete push_notification_device: #{push_notification_device.token}"
      end
    end

    head :ok
  end
end
