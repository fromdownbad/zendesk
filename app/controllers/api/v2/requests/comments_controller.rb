class Api::V2::Requests::CommentsController < Api::V2::BaseController
  include Zendesk::Tickets::UnverifiedEmailControllerSupport

  skip_before_action :require_agent!

  require_that_user_can! :view, :ticket
  before_action :ensure_end_user_email_is_verified

  allow_parameters :all, request_id: Parameters.bigid

  # Map the possible `role` params that will filter the comments by commenter
  # role. The filter consider the commenter capability, not the role itself.
  ROLE_LOOKUP = {
    'agent' => [Role::AGENT.id, Role::LEGACY_AGENT.id, Role::ADMIN.id],
    'end_user' => [Role::END_USER.id]
  }.freeze

  ## ### Listing Comments
  ## `GET /api/v2/requests/{id}/comments.json`
  ##
  ## #### Allowed For
  ##
  ## * End Users
  ##
  ## #### Available parameters
  ##
  ## | Name                  | Type                | Required  | Comments
  ## | --------------------- | --------------------| --------- | -------------------
  ## | `sort_by`             | string              | no        | Possible values are `updated_at`, `created_at`
  ## | `sort_order`          | string              | no        | One of `asc`, `desc`. Defaults to `asc`
  ## | `since`               | datetime            | no        | Filters the comments from the given datetime
  ## | `role`                | string              | no        | One of `agent`, `end_user`. If not specified it does not filter
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests/{id}/comments.json \
  ##   -v -u {email_address}/token:{api_token}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "comments": [
  ##     {
  ##       "id": 43,
  ##       "body": "Thanks for your help!",
  ##       ...
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index,
    since: Parameters.date_time | Parameters.nil_string,
    role: Parameters.enum(*ROLE_LOOKUP.keys)
  require_oauth_scopes :index, scopes: %i[requests:read read]
  def index
    render json: presenter.present(comments)
  end

  ## ### Getting Comments
  ## `GET /api/v2/requests/{request_id}/comments/{id}.json`
  ##
  ## #### Allowed For
  ##
  ## * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/requests/{request_id}/comments/{id}.json \
  ##   -v -u {email_address}/token:{api_token}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "comment": {
  ##     "id": 43,
  ##     "body": "Thanks!",
  ##     ...
  ##   }
  ## }
  ## ```
  require_oauth_scopes :show, scopes: %i[requests:read read]
  resource_action :show

  protected

  def presenter
    @presenter ||= Api::V2::Requests::CommentPresenter.new(current_user, url_builder: self, includes: includes, brand: current_brand)
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:request_id])
  end

  def scope
    @scope ||= ticket.public_comments
  end

  def comments
    @comments ||= begin
      comment_scope = scope
      comment_scope = comment_scope.since(params[:since]) if params[:since].present?
      comment_scope = comment_scope.roles(ROLE_LOOKUP[params[:role]]) if params[:role].present?
      paginate(comment_scope)
    end
  end

  def comment
    @comment ||= scope.find(params[:id])
  end

  SORT_LOOKUP = {
    "created_at" => "created_at",
    "updated_at" => "updated_at"
  }.freeze
end
