class Api::V2::AuditsController < Api::V2::BaseController
  allow_parameters :all, ticket_id: Parameters.bigid
  require_that_user_can! :view_private_content, :ticket

  allow_zopim_user only: [:index, :show]

  ## ### List Audits for a Ticket
  ## `GET /api/v2/tickets/{ticket_id}/audits.json`
  ##
  ## Lists the audits for a specified ticket.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/audits.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "audits": [
  ##     {
  ##       "created_at": "2011-09-25T22:35:44Z",
  ##       "via": {
  ##         "channel": "web"
  ##       },
  ##       "metadata": {
  ##         "system": {
  ##           "location": "San Francisco, CA, United States",
  ##           "client": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.186 Safari/535.1",
  ##           "ip_address": "76.218.201.212"
  ##         },
  ##         "custom": {
  ##         }
  ##       },
  ##       "id": 2127301143,
  ##       "ticket_id": 666,
  ##       "events": [
  ##         {
  ##           "html_body": "<p>This is a new private comment</p>",
  ##           "public": false,
  ##           "body": "This is a new private comment",
  ##           "id": 2127301148,
  ##           "type": "Comment",
  ##           "attachments": [
  ##           ]
  ##         },
  ##         {
  ##           "via": {
  ##             "channel": "rule",
  ##             "source": {
  ##               "to": { },
  ##               "from": {
  ##                 "id": 35079792,
  ##                 "title": "Assign to first responder"
  ##               },
  ##               "rel": "trigger"
  ##             }
  ##           },
  ##           "id": 2127301163,
  ##           "value": "open",
  ##           "type": "Change",
  ##           "previous_value": "new",
  ##           "field_name": "status"
  ##         }
  ##       ],
  ##       "author_id": 5246746
  ##     },
  ##     ...
  ##     {
  ##       ...
  ##       "events": [
  ##         ...
  ##       ],
  ##     }
  ##   ],
  ##   "next_page": null,
  ##   "previous_page": null,
  ##   "count": 5
  ## }
  ## ```
  allow_parameters :index, filter_events: Parameters.array(Parameters.string)
  allow_cursor_pagination_parameters :index
  def index
    render json: presenter.present(audits)
  end

  ## ### Show Audit
  ## `GET /api/v2/tickets/{ticket_id}/audits/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/audits/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "audit": {
  ##     "created_at": "2011-09-25T22:35:44Z",
  ##     "via": {
  ##       "channel": "web"
  ##     },
  ##     "metadata": {
  ##       "system": {
  ##         "location": "San Francisco, CA, United States",
  ##         "client": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.186 Safari/535.1",
  ##         "ip_address": "76.218.201.212"
  ##       },
  ##       "custom": {
  ##       }
  ##     },
  ##     "id": 2127301143,
  ##     "ticket_id": 666,
  ##     "events": [
  ##       {
  ##         "html_body": "<p>This is a new private comment</p>",
  ##         "public": false,
  ##         "body": "This is a new private comment",
  ##         "id": 2127301148,
  ##         "type": "Comment",
  ##         "attachments": []
  ##       },
  ##       {
  ##         "via": {
  ##           "channel": "rule",
  ##           "source": {
  ##             "to": { },
  ##             "from": {
  ##               "id": 22472716,
  ##               "title": "Assign to first responder"
  ##               },
  ##             "rel": "trigger"
  ##           }
  ##         },
  ##         "id": 2127301163,
  ##         "value": "open",
  ##         "type": "Change",
  ##         "previous_value": "new",
  ##         "field_name": "status"
  ##       }
  ##     ],
  ##     "author_id": 5246746
  ##   }
  ## }
  ## ```
  resource_action :show

  ## ### Change a comment from public to private
  ## `PUT /api/v2/tickets/{ticket_id}/audits/{id}/make_private.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/audits/{id}/make_private.json \
  ##   -v -u {email_address}:{password} -X PUT -d '{}' -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ## ```

  # deprecated but still used by some customers
  allow_parameters :make_private, id: Parameters.bigid
  require_oauth_scopes :make_private, scopes: ["tickets:write", :write], arturo: true
  def make_private
    comment.ticket.will_be_saved_by(current_user)
    comment.update_attributes!(is_public: false)
    head :ok
  end

  private

  def ticket
    @ticket ||= current_account.tickets.for_user(current_user).find_by_nice_id!(params[:ticket_id])
  end

  def audits
    @audits ||= if with_cursor_pagination?
      # Audits for archived tickets in descending order
      # is an Array without cursor pagination methods.
      # These audits are coming from the archive
      # and can't be paginated.
      if ordered_audit_collection.respond_to?(:paginate_with_cursor)
        ordered_audit_collection.paginate_with_cursor(
          cursor: params[:cursor],
          limit: params[:limit],
          order: params[:sort_order]
        )
      else
        ordered_audit_collection
      end
    else
      paginate(ordered_audit_collection)
    end
  end

  def audit
    @audit ||= ticket.audits.find(params[:id].to_i)
  end

  def presenter
    @presenter ||= begin
      options = {
        url_builder: self,
        includes: includes
      }

      options[:filter_event_types] = params[:filter_events] if params[:filter_events]
      options[:with_cursor_pagination] = with_cursor_pagination?

      Api::V2::Tickets::AuditPresenter.new(current_user, options)
    end
  end

  def comment
    @comment ||= audit.comment
  end

  def ordered_audit_collection
    audits = ticket.audits

    if ticket.archived?
      if params[:sort_order] == "desc"
        audits = audits.to_a.reverse! # proxy needs to be converted to array before reversing
      end
    else
      audits = audits.order(requested_order)
    end

    audits
  end

  def requested_order
    if params[:sort_order] == "desc"
      "events.created_at DESC, events.id DESC"
    else
      "events.created_at ASC, events.id ASC"
    end
  end
end
