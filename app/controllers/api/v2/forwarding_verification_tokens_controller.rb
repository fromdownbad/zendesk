class Api::V2::ForwardingVerificationTokensController < Api::V2::BaseController
  before_action :require_admin!

  ## ### Listing Forwarding Verification tokens
  ## `GET /api/v2/forwarding_verification_tokens.json`
  ##
  ## #### Allowed for
  ##
  ## * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/forwarding_verification_tokens.json?from_email={address} \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "forwarding_verification_tokens": [
  ##     {
  ##       "id":             20,
  ##       "from_email":     "foo@bar.com",
  ##       "to_email":       "support@trial.zendesk.com",
  ##       "email_provider": 1,
  ##       "value"           "123456789",
  ##       ...
  ##     },
  ##    ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index, from_email: Parameters.string
  def index
    tokens = current_account.forwarding_verification_tokens
    if from = params[:from_email]
      tokens = tokens.where(from_email: from)
    end
    render json: presenter.present(tokens)
  end

  protected

  def presenter
    @presenter ||= Api::V2::ForwardingVerificationTokenPresenter.new(current_user, url_builder: self)
  end
end
