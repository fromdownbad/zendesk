class Api::V2::Routing::AttributeValuesController < Api::V2::BaseController
  include Zendesk::Routing::ControllerSupport

  CONDITION_PARAMETER = {
    subject:  Parameters.string,
    operator: Parameters.string,
    value:    Parameters.anything
  }.freeze

  CREATE_ATTRIBUTE_VALUE = {
    name: Parameters.string,
    conditions: Parameters.map(
      all: [CONDITION_PARAMETER].freeze,
      any: [CONDITION_PARAMETER].freeze
    ),
    added_agents: Parameters.array(Parameters.bigid)
  }.freeze

  UPDATE_ATTRIBUTE_VALUE = CREATE_ATTRIBUTE_VALUE.merge(
    removed_agents: Parameters.array(Parameters.bigid)
  ).freeze

  AGENT_TYPE_ID = Zendesk::Deco::Client::TYPE_ID_AGENT

  DEFAULT_LIMIT = 1000

  private_constant :AGENT_TYPE_ID

  before_action :require_feature, except: :show
  before_action :require_admin!,  except: :show

  ## ### List Attribute Values for an Attribute
  ## `GET /api/v2/routing/attributes/{attribute_id}/values.json`
  ##
  ## Returns a list of attribute values for a provided attribute.
  ##
  ## #### Allowed For
  ##
  ## * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}/values.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "attribute_values": [
  ##     {
  ##       "url": "https://{subdomain}.zendesk.com/api/v2/routing/attributes/afa31619-e38b-11e8-a292-5d17513d969b/values/b376b35a-e38b-11e8-a292-e3b6377c5575.json",
  ##       "id": "b376b35a-e38b-11e8-a292-e3b6377c5575",
  ##       "name": "French",
  ##       "created_at": "2018-11-08T19:22:58Z",
  ##       "updated_at": "2018-11-08T19:22:58Z"
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, attribute_id: Parameters.string
  allow_cursor_pagination_parameters :index
  def index
    with_error_handling do
      presented_values = presenter.present(attribute_values_with_conditions)

      render json:
      if params[:cursor]
        present_attribute_values_with_pagination(
          presented_values[:attribute_values]
        )
      else
        presented_values
      end
    end
  end

  ## ### Create Attribute Value
  ## `POST /api/v2/routing/attributes/{attribute_id}/values.json`
  ##
  ## Creates an attribute value.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}/values.json \
  ##   -H "Content-Type: application/json" \
  ##   -d '{"attribute_value": { "name": "Japanese" }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## {
  ##   {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/routing/attributes/afa31619-e38b-11e8-a292-5d17513d969b/values/6ccddacf-e85e-11e8-a292-ad7686bdff67.json",
  ##     "id": "6ccddacf-e85e-11e8-a292-ad7686bdff67",
  ##     "name": "Japanese",
  ##     "created_at": "2018-11-08T19:22:58Z",
  ##     "updated_at": "2018-11-08T19:22:58Z"
  ##   }
  ## }
  ## ```
  allow_parameters :create,
    attribute_id:    Parameters.string,
    attribute_value: CREATE_ATTRIBUTE_VALUE
  def create
    with_error_handling do
      json = begin
        attribute_value_conditions.present? &&
          present_attribute_value(
            created_attribute_value,
            create_conditions(created_attribute_value)
          ) ||
          present_attribute_value(created_attribute_value)
      end

      if adding_agents?
        attribute.value(created_attribute_value.id).bulk_instances(
          create_instances: added_agents,
          type_id:          AGENT_TYPE_ID
        )
      end

      render(
        json:     json,
        status:   :created,
        location: presenter.url(created_attribute_value)
      )
    end
  end

  ## ### Show Attribute Value
  ## `GET /api/v2/routing/attributes/{attribute_id}/values/{attribute_value_id}.json`
  ##
  ## Returns an attribute value.
  ##
  ## #### Allowed For
  ##
  ## * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}/values.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/routing/attributes/afa31619-e38b-11e8-a292-5d17513d969b/values/b376b35a-e38b-11e8-a292-e3b6377c5575.json",
  ##     "id": "b376b35a-e38b-11e8-a292-e3b6377c5575",
  ##     "name": "French",
  ##     "created_at": "2018-11-08T19:22:58Z",
  ##     "updated_at": "2018-11-08T19:22:58Z"
  ##   }
  ## }
  ## ```
  allow_parameters :show, attribute_id: Parameters.string, id: Parameters.string
  def show
    with_error_handling do
      render(
        json: present_attribute_value(
          show_attribute_value,
          attribute_ticket_map.try(:definition)
        )
      )
    end
  end

  ## ### Update Attribute Value
  ## `PATCH /api/v2/routing/attributes/{attribute_id}/values/{attribute_value_id}.json`
  ##
  ## Updates an attribute value.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}/values/{attribute_value_id}.json \
  ##   -X PATCH -H "Content-Type: application/json" \
  ##   -d '{ "name": "German (Advanced)" }' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/routing/attributes/afa31619-e38b-11e8-a292-5d17513d969b/values/b376b35a-e38b-11e8-a292-e3b6377c5575.json",
  ##     "id": "b376b35a-e38b-11e8-a292-e3b6377c5575",
  ##     "name": "German (Advanced)",
  ##     "created_at": "2018-11-14T22:41:28Z",
  ##     "updated_at": "2018-11-14T22:45:01Z"
  ##   }
  ## }
  ## ```
  allow_parameters :update,
    attribute_id:    Parameters.string,
    id:              Parameters.string,
    attribute_value: UPDATE_ATTRIBUTE_VALUE
  def update
    with_error_handling do
      AttributeTicketMap.transaction do
        if attribute_value_conditions.present?
          update_conditions
        else
          attribute_ticket_map.try(:soft_delete)
        end

        if adding_agents? || removing_agents?
          attribute_value.bulk_instances(
            create_instances: added_agents,
            delete_instances: removed_agents,
            type_id:          AGENT_TYPE_ID
          )
        end

        render(
          json: present_attribute_value(
            updated_attribute_value,
            updated_conditions
          )
        )
      end
    end
  end

  ## ### Delete Attribute Value
  ## `DELETE /api/v2/routing/attributes/{attribute_id}/values/{attribute_value_id}.json`
  ##
  ## Deletes an attribute value.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}/values/{attribute_value_id}.json \
  ##   -X DELETE -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy,
    attribute_id: Parameters.string,
    id:           Parameters.string
  def destroy
    with_error_handling do
      AttributeTicketMap.transaction do
        if current_account.has_delayed_routing_attribute_value_deletion?
          return head :unprocessable_entity if attribute_ticket_map && !attribute_ticket_map.try(:soft_delete)

          enqueue_job_with_status(RoutingAttributeValueDeleteJob, params)
        else
          attribute_ticket_map.try(:soft_delete)

          attribute_value.delete
        end
      end

      head :no_content
    end
  end

  allow_parameters :agents,
    attribute_id: Parameters.string,
    id:           Parameters.string
  def agents
    render json: agent_presenter.present(associated_agents)
  end

  allow_parameters :agent_search,
    attribute_id: Parameters.string,
    id:           Parameters.string,
    query:        Parameters.string,
    group_id:     Parameters.bigid
  def agent_search
    render json: agent_search_presenter.present(agent_search_results)
  end

  def self.build_attribute_value(deco_attribute_value, conditions = nil)
    AttributeValue.new(
      deco_attribute_value.attribute_id,
      deco_attribute_value.id,
      deco_attribute_value.name,
      deco_attribute_value.created_at,
      deco_attribute_value.updated_at,
      conditions
    )
  end

  def self.build_attribute_ticket_map(attribute_ticket_maps)
    attribute_ticket_maps.each_with_object({}) do |atm, definitions|
      definitions[atm.attribute_value_id] = atm.definition
    end
  end

  protected

  attr_reader :updated_conditions

  private

  def attribute_values
    @attribute_values ||= attribute.values.index(
      params[:attribute_id],
      cursor: params[:cursor],
      limit:  limit
    )
  end

  def created_attribute_value
    @created_attribute_value ||=
      attribute.values.create(params[:attribute_id], attribute_value_params)
  end

  def show_attribute_value
    @show_attribute_value ||= attribute_value.show
  end

  def updated_attribute_value
    @updated_attribute_value ||=
      attribute_value.update(attribute_value_params)
  end

  def attribute
    @attribute ||= deco.attribute(params[:attribute_id])
  end

  def attribute_value
    @attribute_value ||= attribute.value(params[:id])
  end

  def attribute_value_params
    @attribute_value_params ||= params[:attribute_value].slice(:name)
  end

  def present_attribute_value(deco_attribute_value, conditions = nil)
    presenter.present(
      self.class.build_attribute_value(deco_attribute_value, conditions)
    )
  end

  def attribute_ticket_maps
    @attribute_ticket_maps ||= begin
      self.class.build_attribute_ticket_map(
        current_account.attribute_ticket_maps.where(
          attribute_id:       params[:attribute_id],
          attribute_value_id: attribute_values.map(&:id)
        )
      )
    end
  end

  def attribute_values_with_conditions
    @attribute_values_with_conditions ||=
      attribute_values.map do |deco_attribute_value|
        self.class.build_attribute_value(
          deco_attribute_value,
          attribute_ticket_maps[deco_attribute_value.id]
        )
      end
  end

  def present_attribute_values_with_pagination(values)
    {
      attribute_values: values,
      count: values.length,
      next_page:
        if values.empty?
          nil
        else
          send(
            :api_v2_routing_attribute_values_url,
            format: 'json',
            cursor: values.last[:id],
            limit:  limit
          )
        end
    }
  end

  def limit
    @limit ||= params[:limit] ? params[:limit].to_i : DEFAULT_LIMIT
  end

  def added_agents
    @added_agents ||= agents_from(params[:attribute_value][:added_agents])
  end

  def removed_agents
    @removed_agents ||= agents_from(params[:attribute_value][:removed_agents])
  end

  def associated_agents
    @associated_agents ||= agents_from(agent_associations.map(&:instance_id))
  end

  def agents_from(agent_ids)
    return [] unless agent_ids.present?

    current_account.agents.where(id: agent_ids)
  end

  def agent_associations
    @agent_associations ||= attribute_value.associations(AGENT_TYPE_ID)
  end

  def agent_search_results_with_value
    @agent_search_results_with_value ||= begin
      attribute_value.instances_with_value(
        instances: agent_search_results,
        type_id:   AGENT_TYPE_ID
      ).each_with_object({}) do |attribute_value_association, agents_with_value|
        agents_with_value[attribute_value_association.instance_id.to_i] = true
      end
    end
  end

  def agent_search_results
    @agent_search_results ||= begin
      Zendesk::Users::FinderWithPagination.new(
        current_account,
        current_user,
        {
          query:    params[:query],
          group_id: params[:group_id],
          role:     [Role::AGENT, Role::ADMIN].map(&:id)
        },
        get_page,
        [params.fetch(:per_page, 100), 100].min
      ).users
    end
  end

  def adding_agents?
    @adding_agents ||= params[:attribute_value][:added_agents].present?
  end

  def removing_agents?
    @removing_agents ||= params[:attribute_value][:removed_agents].present?
  end

  def create_conditions(created_attribute_value)
    current_account.attribute_ticket_maps.create! do |atm|
      atm.attribute_id       = created_attribute_value.attribute_id
      atm.attribute_value_id = created_attribute_value.id
      atm.definition         = definition
    end.definition
  end

  def update_conditions
    @updated_conditions ||= begin
      (attribute_ticket_map || new_map).tap do |atm|
        atm.definition = definition

        atm.save!
      end.definition
    end
  end

  def attribute_ticket_map
    @attribute_ticket_map ||= begin
      current_account.attribute_ticket_maps.where(
        attribute_id:       params[:attribute_id],
        attribute_value_id: params[:id]
      ).first
    end
  end

  def new_map
    @new_map ||= begin
      current_account.attribute_ticket_maps.build do |atm|
        atm.attribute_id       = params[:attribute_id]
        atm.attribute_value_id = params[:id]
      end
    end
  end

  def attribute_value_conditions
    @attribute_value_conditions ||=
      normalize_conditions(params[:attribute_value][:conditions])
  end

  def normalize_conditions(conditions)
    return nil unless conditions_present?(conditions)

    {all: conditions.fetch(:all, []), any: conditions.fetch(:any, [])}
  end

  def conditions_present?(conditions)
    conditions.present? &&
      (conditions[:all].present? || conditions[:any].present?)
  end

  def definition
    Definition.build(
      conditions_all: attribute_value_conditions[:all].
        map(&method(:definition_item)),
      conditions_any: attribute_value_conditions[:any].
        map(&method(:definition_item))
    )
  end

  def definition_item(condition)
    Api::V2::Rules::ConditionMappings.map_definition_item!(
      current_account,
      field:    condition[:subject],
      operator: condition[:operator],
      value:    condition[:value]
    )
  end

  def deco
    @deco ||= Zendesk::Deco::Client.new(current_account)
  end

  def presenter
    @presenter ||= begin
      Api::V2::Routing::AttributeValuePresenter.new(
        current_user,
        url_builder: self,
        includes:    includes + [:conditions]
      )
    end
  end

  def agent_presenter
    @agent_presenter ||= begin
      Api::V2::Users::AgentPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def agent_search_presenter
    @agent_search_presenter ||= begin
      Api::V2::Routing::AgentPresenter.new(
        current_user,
        url_builder:       self,
        agents_with_value: agent_search_results_with_value,
        includes:          includes
      )
    end
  end

  def require_feature
    head :not_found unless current_account.has_skill_based_ticket_routing?
  end

  AttributeValue = Struct.new(
    :attribute_id,
    :id,
    :name,
    :created_at,
    :updated_at,
    :conditions
  )

  private_constant :AttributeValue
end
