class Api::V2::Routing::InstanceValuesController < Api::V2::BaseController
  include Zendesk::Routing::ControllerSupport

  before_action :require_feature

  before_action :require_rerun_skill_based_routing_rules_feature,
    only: :rerun_attribute_ticket_mapping_rules

  before_action :verify_edit_access,
    only: %I[update_ticket_instance_values
             rerun_attribute_ticket_mapping_rules]

  before_action :require_closed_ticket_check,
    only: :rerun_attribute_ticket_mapping_rules

  before_action :require_admin!, only: :update_agent_instance_values

  ## ### List Ticket Attribute Values
  ## `GET /api/v2/routing/tickets/{ticket_id}/instance_values.json`
  ##
  ## Returns a list of attributes values for the ticket.
  ##
  ## #### Allowed For
  ##
  ## * Agents and admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/tickets/{ticket_id}/instance_values.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##  "attribute_values": [{
  ##    "id": "fa1131e2-d6cd-11e7-a492-dbdd5500c7e3",
  ##    "name": "Ocean",
  ##    "created_at": "2017-12-01T19:29:41Z",
  ##    "updated_at": "2017-12-01T19:35:45Z",
  ##    "attribute_id": "f4a604b1-d6cd-11e7-a492-657e7928664c"
  ##  }]
  ## }
  ## ```
  allow_parameters :show_ticket_instance_values, id: Parameters.string
  def show_ticket_instance_values
    with_error_handling do
      t = current_account.tickets.find_by_nice_id!(params[:id])
      show_instance_values(Zendesk::Deco::Client::TYPE_ID_TICKET, t.id)
    end
  end

  ## ### Set Ticket Attribute Values
  ## `POST /api/v2/routing/tickets/{ticket_id}/instance_values`
  ##
  ## Adds the specified attributes if no attributes exists, or replaces all existing attributes with the specified attributes.
  ##
  ## Invalid or deleted attributes are ignored.
  ##
  ## #### Allowed For
  ##
  ## * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/tickets/{ticket_id}/instance_values \
  ##   -d "{\"attribute_value_ids\":[\"{attribute_value_id}\"]}" \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##  "attribute_values": [{
  ##    "id": "fa1131e2-d6cd-11e7-a492-dbdd5500c7e3",
  ##    "name": "Ocean",
  ##    "created_at": "2017-12-01T19:29:41Z",
  ##    "updated_at": "2017-12-01T19:35:45Z",
  ##    "attribute_id": "f4a604b1-d6cd-11e7-a492-657e7928664c"
  ##  }]
  ## }
  ## ```
  allow_parameters :update_ticket_instance_values,
    id: Parameters.string,
    attribute_value_ids: Parameters.array(Parameters.string)
  def update_ticket_instance_values
    with_error_handling do
      ticket = current_account.tickets.find_by_nice_id!(params[:id])
      update_instance_values(
        Zendesk::Deco::Client::TYPE_ID_TICKET,
        ticket.id
      ) do |attribute_values|
        begin
          create_ticket_audit(ticket, attribute_values)
        rescue StandardError => e
          Rails.logger.warn(
            "Failed to create ticket audit event #{e.message}"
          )
        end
      end
    end
  end

  ## ### List Agent Attribute Values
  ## `GET /api/v2/routing/agents/{agent_id}/instance_values.json`
  ##
  ## Returns a list of attributes values for the agent.
  ##
  ## #### Allowed For
  ##
  ## * Agents and admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/agents/{agent_id}/instance_values.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##  "attribute_values": [{
  ##    "id": "fa1131e2-d6cd-11e7-a492-dbdd5500c7e3",
  ##    "name": "Ocean",
  ##    "created_at": "2017-12-01T19:29:41Z",
  ##    "updated_at": "2017-12-01T19:35:45Z",
  ##    "attribute_id": "f4a604b1-d6cd-11e7-a492-657e7928664c"
  ##  }]
  ## }
  ## ```
  allow_parameters :show_agent_instance_values, id: Parameters.string
  def show_agent_instance_values
    with_error_handling do
      show_instance_values(Zendesk::Deco::Client::TYPE_ID_AGENT, params[:id])
    end
  end

  ## ### Set Agent Attribute Values
  ## `POST /api/v2/routing/agents/{agent_id}/instance_values`
  ##
  ## Adds the specified attributes if no attributes exists, or replaces all existing attributes with the specified attributes.
  ##
  ## #### Allowed For
  ##
  ## * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/agents/{agent_id}/instance_values \
  ##   -d "{\"attribute_value_ids\":[\"{attribute_value_id}\"]}" \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##  "attribute_values": [{
  ##    "id": "fa1131e2-d6cd-11e7-a492-dbdd5500c7e3",
  ##    "name": "Ocean",
  ##    "created_at": "2017-12-01T19:29:41Z",
  ##    "updated_at": "2017-12-01T19:35:45Z",
  ##    "attribute_id": "f4a604b1-d6cd-11e7-a492-657e7928664c"
  ##  }]
  ## }
  ## ```
  allow_parameters :update_agent_instance_values,
    id: Parameters.string,
    attribute_value_ids: Parameters.array(Parameters.string)
  def update_agent_instance_values
    with_error_handling do
      user = current_account.users.find(params[:id])

      fail 'Updates only allowed for agents' unless user.is_agent?

      update_instance_values(
        Zendesk::Deco::Client::TYPE_ID_AGENT, params[:id]
      ) do |attribute_values|
        begin
          create_user_cia_audit(user, attribute_values)
        rescue StandardError => e
          Rails.logger.warn(
            "Failed to create CIA::AttributeChange event #{e.message}"
          )
        end
      end
    end
  end

  # h3 Rerunning skill based attribute ticket mapping rules
  #       manually on a ticket
  #
  # h3 Publish docs
  # * Jira: https://zendesk.atlassian.net/browse/TR-544
  #
  # `PUT /api/v2/routing/tickets/#{ticket_id}/rules/execute`
  #
  # h3 Allowed For
  #
  # * Admins
  # * Agents with permissions
  #
  # h4 Arturo required
  # * rerun_skill_based_routing_rules
  #
  # h3 Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/routing/tickets/\
  # {ticket_id}/rules/execute -v -u {email_address}:{password} -X PUT
  # ```
  #
  # h3 Example response
  # ```http
  # Status: 200 OK
  #
  # {
  #  "attribute_values": [{
  #    "id": "3d1a0c25-4b43-11e9-bfd1-95ab136eed1b",
  #    "name": "French",
  #    "created_at": "2019-03-21T02:06:17Z",
  #    "updated_at": "2019-03-21T02:07:02Z",
  #    "attribute_id": "2aa82671-4766-11e9-b65b-df4f5d3a10ee"
  #  }]
  # }
  # ```
  # h3
  allow_parameters :rerun_attribute_ticket_mapping_rules,
    id: Parameters.string
  def rerun_attribute_ticket_mapping_rules
    with_error_handling do
      current_ticket = current_account.tickets.find_by_nice_id!(params[:id])
      attribute_value_ids = current_ticket.assign_attribute_values.
        map(&:attribute_value_id)
      update_instance_values(Zendesk::Deco::Client::TYPE_ID_TICKET,
        current_ticket.id, attribute_value_ids)
    end
  end

  private

  def create_user_cia_audit(user, attribute_values)
    actor = CIA.current_actor || User.system
    event = CIA::Event.create!(
      account: current_account,
      actor: actor,
      ip_address: request.remote_ip,
      source: user,
      action: 'update',
      visible: true
    )
    CIA::AttributeChange.create!(
      attribute_name: 'attribute_values',
      event: event,
      source: user,
      old_value: [],
      new_value: attribute_values.map(&:name)
    )
  end

  def create_ticket_audit(ticket, attribute_values)
    return unless attribute_values

    event_attribute_values = attribute_values.map do |av|
      {id: av.id, name: av.name, attribute_id: av.attribute_id}
    end
    associate_att_vals_event = AssociateAttValsEvent.new(
      attribute_values: event_attribute_values
    )
    ticket.will_be_saved_by(current_user)
    ticket.audit.events << associate_att_vals_event
    ticket.save!
  end

  def show_instance_values(type_id, item_id)
    avr = ::Zendesk::Deco::Request::AttributeValues.new(deco)
    vals = avr.for_instance(type_id, item_id)
    render(
      json: presenter.collection_presenter.present(vals)
    )
  end

  def update_instance_values(type_id, item_id, attribute_value_ids = nil)
    attribute_value_ids ||= params[:attribute_value_ids]
    bivr = ::Zendesk::Deco::Request::BulkInstanceValues.new(deco)
    vals = bivr.post_instance_values(
      type_id,
      item_id,
      Array(attribute_value_ids)
    )

    yield(vals) if block_given?

    render(
      json: presenter.collection_presenter.present(vals)
    )
  end

  def deco
    @deco ||= Zendesk::Deco::Client.new(current_account)
  end

  def presenter
    @presenter ||= begin
      Api::V2::Routing::AttributeValuePresenter.new(
        current_user,
        url_builder: self,
        includes:    includes,
        include_attribute_id: true
      )
    end
  end

  def require_rerun_skill_based_routing_rules_feature
    head :not_found unless
      current_account.has_rerun_skill_based_routing_rules?
  end

  def require_feature
    head :not_found unless current_account.has_skill_based_ticket_routing?
  end

  def verify_edit_access
    deny_access unless current_user.can?(:edit_attribute_values, Ticket)
  end

  def require_closed_ticket_check
    head :unprocessable_entity if current_account.tickets.
      find_by_nice_id!(params[:id]).
      closed?
  end
end
