class Api::V2::Routing::AttributesController < Api::V2::BaseController
  include Zendesk::Routing::ControllerSupport

  ATTRIBUTE = {name: Parameters.string}.freeze

  before_action :require_feature, except: :show
  before_action :require_admin!,  except: [:index, :show]

  ## ### List Account Attributes
  ## `GET /api/v2/routing/attributes.json`
  ##
  ## Returns a list of attributes for the account.
  ##
  ## #### Allowed For
  ##
  ## * Agents and admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | attribute_values | The attribute values available on the account
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ##  {
  ##    "attributes": [{
  ##      "id": "15821cba-7326-11e8-b07e-950ba849aa27",
  ##      "name": "Color",
  ##      "created_at": "2017-12-01T19:29:31Z",
  ##      "updated_at": "2017-12-01T19:29:31Z"
  ##    }]
  ##  }
  ## ```
  allow_parameters :index, {}
  def index
    with_error_handling do
      json = attribute_collection_presenter.present(attributes[:attributes])

      if includes.include?(:attribute_values)
        json[:attribute_values] = attribute_values[:attribute_values]
      end

      if includes.include?(:agent_count)
        json[:agent_count] = attributes[:agent_count]
      end

      render json: json
    end
  end

  ## ### Create Attribute
  ## `POST /api/v2/routing/attributes.json`
  ##
  ## Creates an attribute.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes.json \
  ##   -H "Content-Type: application/json" \
  ##   -d '{"attribute": { "name": "Language" }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## {
  ##   "attribute": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/routing/attributes/6e279587-e930-11e8-a292-09cfcdea1b75.json",
  ##     "id": "6e279587-e930-11e8-a292-09cfcdea1b75",
  ##     "name": "Language",
  ##     "created_at": "2018-11-15T23:44:45Z",
  ##     "updated_at": "2018-11-15T23:44:45Z"
  ##   }
  ## }
  ## ```
  allow_parameters :create, attribute: ATTRIBUTE
  def create
    with_error_handling do
      render(
        json:     presenter.present(created_attribute),
        status:   :created,
        location: presenter.url(created_attribute)
      )
    end
  end

  ## ### Show Attribute
  ## `GET /api/v2/routing/attributes/{attribute_id}.json`
  ##
  ## Returns an attribute.
  ##
  ## #### Allowed For
  ##
  ## * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "attribute": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/routing/attributes/6e279587-e930-11e8-a292-09cfcdea1b75.json",
  ##     "id": "6e279587-e930-11e8-a292-09cfcdea1b75",
  ##     "name": "Language",
  ##     "created_at": "2018-11-15T23:44:45Z",
  ##     "updated_at": "2018-11-15T23:44:45Z"
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.string
  def show
    with_error_handling do
      render json: presenter.present(attribute)
    end
  end

  ## ### Update Attribute
  ## `PATCH /api/v2/routing/attributes/{attribute_id}.json`
  ##
  ## Updates an attribute.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}.json \
  ##   -d '{ "name": "Lingua" }' \
  ##   -X PATCH -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "attribute": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/routing/attributes/6e279587-e930-11e8-a292-09cfcdea1b75.json",
  ##     "id": "6e279587-e930-11e8-a292-09cfcdea1b75",
  ##     "name": "Lingua",
  ##     "created_at": "2018-11-15T23:44:45Z",
  ##     "updated_at": "2018-11-15T23:44:45Z"
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.string, attribute: ATTRIBUTE
  def update
    with_error_handling do
      render json: presenter.present(updated_attribute)
    end
  end

  ## ### Delete Attribute
  ## `DELETE /api/v2/routing/attributes/{attribute_id}.json`
  ##
  ## Deletes an attribute.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/{attribute_id}.json \
  ##   -X DELETE -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.string
  def destroy
    with_error_handling do
      AttributeTicketMap.transaction do
        attribute_ticket_maps_for_att.each(&:soft_delete)
        deco.attribute(params[:id]).delete
      end

      head :no_content
    end
  end

  ## ### List Routing Attribute Definitions
  ## `GET /api/v2/routing/attributes/definitions.json`
  ##
  ## Returns the condition definitions that can be configured to apply attributes to a ticket.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/routing/attributes/definitions.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "definitions": {
  ##     "conditions_all": [
  ##        {
  ##           "title": "Number of incidents",
  ##           "subject": "number_of_incidents",
  ##           ...
  ##        }
  ##      ],
  ##     "conditions_any": [...]
  ##   }
  ## }
  ## ```
  allow_parameters :definitions, {}
  def definitions
    render json: definitions_presenter.present(attribute_definitions)
  end

  private

  def attributes
    @attributes ||= deco.attributes(params[:include]).index
  end

  def attribute_values
    m = Api::V2::Routing::AttributeValuesController.build_attribute_ticket_map(
      attribute_ticket_maps
    )
    attribute_value_collection_presenter.present(
      attributes[:attribute_values].map do |deco_attribute_value|
        Api::V2::Routing::AttributeValuesController.build_attribute_value(
          deco_attribute_value,
          m[deco_attribute_value.id]
        )
      end
    )
  end

  def created_attribute
    @created_attribute ||= deco.attributes.create(params[:attribute])
  end

  def attribute
    @attribute ||= deco.attribute(params[:id]).show
  end

  def attribute_ticket_maps
    @attribute_ticket_maps ||= begin
      current_account.attribute_ticket_maps
    end
  end

  def updated_attribute
    @updated_attribute ||=
      deco.attribute(params[:id]).update(params[:attribute])
  end

  def deco
    @deco ||= Zendesk::Deco::Client.new(current_account)
  end

  def presenter
    @presenter ||= begin
      Api::V2::Routing::AttributePresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def attribute_value_presenter
    @attribute_value_presenter ||= begin
      Api::V2::Routing::AttributeValuePresenter.new(
        current_user,
        url_builder:          self,
        includes:             includes + [:conditions],
        include_attribute_id: true
      )
    end
  end

  def attribute_value_collection_presenter
    @attribute_value_collection_presenter ||= begin
      Api::V2::CollectionPresenter.new(
        current_user,
        url_builder:    self,
        item_presenter: attribute_value_presenter
      )
    end
  end

  def attribute_collection_presenter
    @attribute_collection_presenter ||= begin
      Api::V2::CollectionPresenter.new(
        current_user,
        url_builder:    self,
        item_presenter: presenter
      )
    end
  end

  def definitions_presenter
    @definitions_presenter ||= begin
      Api::V2::Routing::AttributeDefinitionsPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def attribute_definitions
    @attribute_definitions ||= begin
      Zendesk::Routing::Attribute::Definitions.new(
        current_account,
        current_user
      )
    end
  end

  def require_feature
    head :not_found unless current_account.has_skill_based_ticket_routing?
  end

  def attribute_ticket_maps_for_att
    @attribute_ticket_maps ||= begin
      current_account.attribute_ticket_maps.where(attribute_id: params[:id])
    end
  end
end
