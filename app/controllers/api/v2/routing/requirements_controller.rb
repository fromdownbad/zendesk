class Api::V2::Routing::RequirementsController < Api::V2::BaseController
  include Zendesk::Routing::ControllerSupport

  before_action :require_feature

  ## ### List Tickets Fulfilled by a User
  ## `GET /api/v2/routing/requirements/fulfilled.json`
  ##
  ## Returns a list of ticket ids that contain attributes matching the current
  ## user's attributes. Accepts a `ticket_ids` parameter for relevant tickets to
  ## check for matching attributes.
  ##
  ## #### Allowed For
  ##
  ## * Agents and admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl 'https://{subdomain}.zendesk.com/api/v2/routing/requirements/fulfilled?ticket_ids\[\]=17&ticket_ids\[\]=1&ticket_ids\[\]=99' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ##  {
  ##    "fulfilled_ticket_ids": [1, 17]
  ##  }
  ## ```
  allow_parameters :fulfilled,
    ticket_ids: Parameters.array(Parameters.bigid) |
                Parameters.ids
  def fulfilled
    with_error_handling do
      render json: {fulfilled_ticket_ids: fulfilled_requirements}
    end
  end

  private

  def fulfilled_requirements
    id_to_nice_id_map = create_ticket_id_map(params[:ticket_ids])
    fulfilled_real_ids = requirements.fulfilled_ticket_ids(
      current_user,
      id_to_nice_id_map.keys
    )
    # Ids returned by Deco are strings, that is why we need to_i
    fulfilled_real_ids.map { |real_id| id_to_nice_id_map[real_id.to_i] }.compact
  end

  def create_ticket_id_map(ticket_nice_ids)
    tickets = current_account.tickets.where(nice_id: ticket_nice_ids)
    tickets.map do |ticket|
      [ticket.id, ticket.nice_id]
    end.to_h
  end

  def requirements
    @requirements ||= Zendesk::Deco::Request::TicketRequirements.new(deco)
  end

  def deco
    @deco ||= Zendesk::Deco::Client.new(current_account)
  end

  def require_feature
    head :not_found unless current_account.has_skill_based_ticket_routing?
  end
end
