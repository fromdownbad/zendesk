require 'zendesk/pod_relay'

module Api
  module V2
    class AccountsCreationController < Api::V2::AccountsController
      before_action :confirm_zendesk_origin, only: :create_new_account
      before_action :redirect_to_pod, only: :create_new_account

      ## ### Internal call to update precreated new accounts or generate new accounts
      ## `PUT signup.zendesk.com/api/v2/internal/new_account.json`
      ##
      ## #### Allowed for
      ##
      ##  * internal zendesk calls with special param
      ##
      ## #### Example Response
      ##
      ## Provisioning a trial account:
      ##
      ## ```bash
      ## curl -v https://signup.zendesk.com/api/v2/internal/new_account.json \
      ## -X POST \
      ## -H "Content-Type: application/json" \
      ## -d '{ "account": {
      ##         "name":      "My Company",
      ##         "subdomain": "company",
      ##         "help_desk_size": "Small Team"
      ##       },
      ##       "owner": {
      ##         "name":  "Lewis Carroll",
      ##         "email": "alice@example.com"
      ##       },
      ##       "address": {
      ##         "phone": "+1-123-456-7890"
      ##       }
      ##       "confirm_origin": "{SPECIAL PARAM}"
      ##    }'
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```
      ## Status: 200
      ## ```
      allow_parameters :create_new_account, {
        account: ACCOUNT_PARAMETER,
        address: ADDRESS_PARAMETER,
        agents: AGENTS_PARAMETER,
        force_api_ratelimit: Parameters.boolean | Parameters.nil_string,
        force_classic: Parameters.anything, # this is ignored now, but leaving it in for API compatibility
        force_pod_id: Parameters.string,
        include: Parameters.string | Parameters.nil_string,
        owner: OWNER_PARAMETER,
        partner: PARTNER_PARAMETER,
        recaptcha_response: Parameters.string,
        confirm_origin: Parameters.enum(CONFIRM_UPDATE),
        creation_channel: Parameters.string | Parameters.nil_string,
        original_remote_ip: Parameters.string,
        region: Parameters.string | Parameters.nil_string
      }.merge(MARKETING_PARAMETER)

      def create_new_account
        ActiveRecord::Base.on_shard(new_account.shard_id) do
          new_account_creation
          tags = ["fast_account_creation:#{new_account.is_fast_account_creation}", "is_serviceable:#{new_account.is_serviceable}"]
          statsd_client.increment('success', tags: tags)

          render json: presenter.present(new_account).merge!(
            success: true,
            owner_verification_link: owner_verification_link
          ), status: :created
        end
      rescue => e
        ZendeskExceptions::Logger.record(e, location: self, message: "[NewAccountCreationFailure] Failed to create an account: #{e.message}", fingerprint: 'f251b5e17f05be9cc7a60e911afe2f702db309fb')

        tags = ["fast_account_creation:#{new_account.is_fast_account_creation}", "error:#{e.class}"]
        statsd_client.increment('failure', tags: tags)

        message = e.respond_to?(:record) ? presenter.present_errors(e.record) : { error: e.message }
        render json: message, status: :unprocessable_entity
      end

      private

      def new_account
        @new_account ||= begin
          Zendesk::Accounts::Initializer.new(params, params[:original_remote_ip], params[:region]).account
        end
      end

      def presenter
        @presenter ||= Api::V2::AccountPresenter.new(new_account.owner, url_builder: self, includes: includes)
      end

      def owner_verification_link
        "#{new_account.url}/verification/challenge/#{new_account.owner.find_or_create_challenge_token(request.remote_ip).value}"
      end

      def redirect_to_pod
        target_pod_id = params[:force_pod_id].to_i
        if target_pod_id == 0
          target_pod_id = ::Account.select_pod(
            region,
            type: params[:account].try(:[], :source),
            subdomain: subdomain
          )
        end
        if target_pod_id != Zendesk::Configuration.fetch(:pod_id) && can_relay_request?
          relay_request_cross_pod(target_pod_id)
        end
      end

      def new_account_creation
        tags = ["fast_account_creation:#{new_account.is_fast_account_creation}"]
        statsd_client.time('period', tags: tags) do
          unless new_account.valid? && validate_new_account
            raise ActiveRecord::RecordInvalid, new_account
          end

          new_account.without_feature_bits_caching do
            Zendesk::SupportAccounts::Product.retrieve(new_account).save!
          end

          add_agents
        end
      end

      def validate_new_account
        if new_account.is_serviceable
          true
        else
          new_account.errors.add :base, "account is not serviceable"
          false
        end
      end

      def add_agents
        agents = params[:agents]

        if agents && params[:account] && via = params[:account][:source]
          agents.each do |x|
            x[:via] = via
            x[:organization_id] = new_account.organizations.first.id
          end
          UserBulkCreateJob.enqueue(account_id: new_account.id, user_id: new_account.owner_id, users: agents)
        end
      end

      def confirm_zendesk_origin
        return true if params[:confirm_origin] == CONFIRM_UPDATE
        Rails.logger.info('[AccountsNewCreation] Not from Zendesk')
        statsd_client.increment('validation_failure', tags: ["reason: confirm_zendesk_origin"])
        render json: { success: false }, status: :forbidden
      end
    end
  end
end
