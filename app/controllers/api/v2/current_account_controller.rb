class Api::V2::CurrentAccountController < Api::V2::BaseController
  before_action :require_owner!, only: [:update]

  allow_subsystem_user :embeddable, only: [:show]
  allow_subsystem_user :billing, only: [:show]
  allow_subsystem_user :guide_rest, only: [:show]
  allow_zopim_user only: [:show]
  allow_bime_user only: [:show]
  skip_before_action :require_owner!, if: :valid_sandbox_orchestrator_request?
  ## ### Show the Current Account
  ## `GET /api/v2/account.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "account": {
  ##     "id":   35436,
  ##     "subdomain": "example",
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :show, {}
  def show
    render json: presenter.present(current_account)
  end

  # ### Update the current account
  # `PUT /api/v2/account.json`
  #
  # #### Allowed For:
  #
  #  * Account owner only
  #
  # #### Example Response
  #
  # ```http
  # Status: 200
  #
  # {
  #   "account": {
  #     "name":        "Zendesk",
  #     "subdomain":   "example",
  #     "owner_id":    35436,
  #     "time_format": 24,
  #     ...
  #   }
  # }
  # ```
  allow_parameters :update, account: {
    name: Parameters.string,
    owner_id: Parameters.bigid,
    time_zone: Parameters.string,
    time_format: Parameters.integer,
    notify_agent_ccs: Parameters.boolean,
    two_factor_authentication: Parameters.boolean
  }
  def update
    account.owner_id  = params[:account][:owner_id] if params[:account][:owner_id].present?
    account.time_zone = params[:account][:time_zone] if params[:account][:time_zone].present?
    account.name      = params[:account][:name] if params[:account][:name].present?
    time_format       = params[:account][:time_format]

    if [12, 24].include?(time_format)
      account.uses_12_hour_clock = time_format == 12
    end

    account.save!

    render json: presenter.present(account)
  end

  protected

  def account
    current_account
  end

  def valid_sandbox_orchestrator_request?
    current_user&.is_system_user? && current_subsystem_name == 'sandbox_orchestrator' && current_account.is_sandbox?
  end

  def presenter
    @presenter ||= Api::V2::AccountPresenter.new(
      current_user,
      url_builder: self,
      includes: includes,
      internal: request.internal_client?
    )
  end

  def new_owner
    @new_owner ||= account.agents.find_by_id(params[:account][:owner_id])
  end
end
