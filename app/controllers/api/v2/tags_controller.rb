module Api
  module V2
    class TagsController < Api::V2::BaseController
      before_action :require_admin!, only: :index
      before_action :validate_safe_update_params, only: [:update, :destroy]

      before_action only: [:index] do
        throttle_endpoint!(:v2_tags_controller)
      end

      allow_zopim_user only: [:update]

      require_capability :count_tags_endpoint, only: :count

      TAGS_UPDATE_PARAMETER = {
        tags: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil,
        safe_update: Parameters.boolean | Parameters.nil_string,
        updated_stamp: Parameters.date_time | Parameters.nil_string,
        via: {
          channel: Parameters.string,
        }.freeze
      }.freeze

      allow_parameters :all, ticket_id: Parameters.bigid, organization_id: Parameters.bigid, user_id: Parameters.bigid

      ## ### List Tags
      ##
      ## `GET /api/v2/tags.json`
      ##
      ## Lists the 500 most popular tags in the last 60 days, in decreasing popularity.
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/tags.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "tags": [
      ##     {
      ##       "name":  "important",
      ##       "count": 47
      ##     },
      ##     {
      ##       "name":  "customer",
      ##       "count": 11
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, {}
      allow_cursor_pagination_v2_parameters :index
      def index
        render json: score_presenter.present(tag_scores)
      end

      ## ### Count Tags
      ##
      ## `GET /api/v2/tags/count.json`
      ##
      ## Returns an approximate count of tags. If the count exceeds 100,000, it is updated every 24 hours.
      ##
      ## The `count[refreshed_at]` property is a timestamp that indicates when the count was last updated.
      ##
      ## **Note**: When the count exceeds 100,000, `count[refreshed_at]` may occasionally be `null`.
      ## This indicates that the count is being updated in the background, and `count[value]` is limited to 100,000 until the update is complete.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/tags/count.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200
      ##
      ## {
      ##    count: {
      ##      value: 102,
      ##      refreshed_at: "2020-04-06T02:18:17Z"
      ##    },
      ##    links: {
      ##      url: "https://{subdomain}.zendesk.com/api/v2/tags/count"
      ##    }
      ## }
      ## ```
      allow_parameters :count, {}
      def count
        render json: { count: record_counter.present, links: { url: request.original_url } }
      end

      ## ### Show Tags
      ## `GET /api/v2/tickets/{id}/tags.json`
      ##
      ## `GET /api/v2/organizations/{id}/tags.json`
      ##
      ## `GET /api/v2/users/{id}/tags.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/tags.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "tags": [
      ##     "important",
      ##     "customer"
      ##   ]
      ## }
      ## ```
      allow_parameters :show, id: Parameters.string
      def show
        render json: presenter.present(model)
      end

      ## ### Set Tags
      ## `POST /api/v2/tickets/{id}/tags.json`
      ##
      ## `POST /api/v2/organizations/{id}/tags.json`
      ##
      ## `POST /api/v2/users/{id}/tags.json`
      ##
      ## Adds the specified tags if no tag exists, or replaces all existing
      ## tags with the specified tags.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/tags.json \
      ##   -X POST -d '{ "tags": ["important"] }' \
      ##   -H "Content-Type: application/json" \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "tags": [
      ##     "important"
      ##   ]
      ## }
      ## ```
      allow_parameters :create, tags: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil
      def create
        return deny_access unless can_modify_tags?(model)

        model.update_attributes!(set_tags: params[:tags])
        render json: presenter.present(model), status: :created, location: presenter.url(model)
      end

      ## ### Add Tags
      ## `PUT /api/v2/tickets/{id}/tags.json`
      ##
      ## `PUT /api/v2/organizations/{id}/tags.json`
      ##
      ## `PUT /api/v2/users/{id}/tags.json`
      ##
      ## You can also add tags to multiple tickets with the
      ## [Update Many Tickets](./tickets#updating-tag-lists) endpoint.
      ##
      ## #### Safe Update
      ##
      ## If the same ticket is updated by multiple API requests at the same time, some tags could be
      ## lost because of ticket update collisions. Include `updated_stamp` and `safe_update` properties
      ## in the request body to make a safe update.
      ##
      ## For `updated_stamp`, retrieve and specify the ticket's latest `updated_at` timestamp. The tag update
      ## only occurs if the `updated_stamp` timestamp matches the ticket's actual `updated_at` timestamp at the
      ## time of the request. If the timestamps don't match (in other words, if the ticket was updated since you
      ## retrieved the ticket's last `updated_at` timestamp), the request returns a 409 Conflict error.
      ##
      ## **Example**
      ##
      ## ```json
      ## {
      ##   "tags": ["customer"],
      ##   "updated_stamp":"2019-09-12T21:45:16Z",
      ##   "safe_update":"true"
      ## }
      ## ```
      ##
      ## For details, see [Protecting against ticket update collisions](./tickets#protecting-against-ticket-update-collisions).
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/tags.json \
      ##   -X PUT -d '{ "tags": ["customer"] }' \
      ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "tags": [
      ##     "important",
      ##     "customer"
      ##   ]
      ## }
      ## ```
      allow_parameters :update, TAGS_UPDATE_PARAMETER
      def update
        return deny_access unless can_modify_tags?(model)

        model.update_attributes!(attributes_with_safe_update(additional_tags: params[:tags]))
        render json: presenter.present(model)
      end

      ## ### Remove Tags
      ## `DELETE /api/v2/tickets/{id}/tags.json`
      ##
      ## `DELETE /api/v2/organizations/{id}/tags.json`
      ##
      ## `DELETE /api/v2/users/{id}/tags.json`
      ##
      ## You can also delete tags from multiple tickets with the
      ## [Update Many Tickets](./tickets#updating-tag-lists) endpoint.
      ##
      ## This endpoint supports safe updates. See [Safe Update](#safe-update).
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/tags.json \
      ##   -X DELETE -d '{ "tags": ["customer"] }' \
      ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "tags": [
      ##     "important"
      ##   ]
      ## }
      ## ```
      allow_parameters :destroy, tags: Parameters.array(Parameters.string) | Parameters.string | Parameters.nil,
        safe_update: Parameters.boolean | Parameters.nil_string,
        updated_stamp: Parameters.date_time | Parameters.nil_string
      def destroy
        return deny_access unless can_modify_tags?(model)

        model.update_attributes!(attributes_with_safe_update(remove_tags: params[:tags]))
        render json: presenter.present(model)
      end

      protected

      def attributes_with_safe_update(attributes)
        attributes[:safe_update_timestamp] = params[:updated_stamp] if params[:safe_update]
        attributes
      end

      def presenter
        @presenter ||= Api::V2::TagsPresenter.new(current_user, url_builder: self)
      end

      def score_presenter
        @score_presenter ||= Api::V2::TagScorePresenter.new(
          current_user,
          url_builder: self,
          with_cursor_pagination: with_cursor_pagination_v2?
        )
      end

      def tag_scores
        @tag_scores ||= begin
          if with_cursor_pagination_v2?
            current_account.tag_scores.paginate_with_cursor(
              cursor_pagination_version: 2,
              page: params[:page]
            )
          else
            current_account.tag_scores.paginate(pagination)
          end
        end
      end

      def model
        @model ||= if params[:ticket_id]
          ticket = current_account.tickets.find_by_nice_id!(params[:ticket_id])
          ticket.will_be_saved_by(
            current_user,
            via_id: Zendesk::Types::ViaType.find(params[:via].try(:[], :channel)) || ViaType.WEB_SERVICE
          )
          ticket
        elsif params[:organization_id]
          current_account.organizations.find(params[:organization_id])
        elsif params[:user_id]
          current_account.users.find(params[:user_id])
        end
      end

      def can_modify_tags?(model)
        return true unless model.class == User

        current_user.can?(:modify_user_tags, model)
      end

      def validate_safe_update_params
        if params[:safe_update] && params[:updated_stamp].nil?
          render json: {
            error: "UpdatedTimeNotProvided",
            description: "updated_stamp must be provided if safe_update parameter is present"
          }, status: :conflict
        end
      end

      def record_counter
        @record_counter ||= Zendesk::RecordCounter::TagScores.new(account: current_account)
      end
    end
  end
end
