class Api::V2::ErrorsController < Api::V2::BaseController
  allow_subsystem_users

  skip_before_action :enforce_request_format

  # Catches all /api/v2/* routes that would otherwise
  # raise an ActionController::RoutingError
  allow_parameters :routing, :skip # TODO: make stricter
  require_oauth_scopes :routing, scopes: [:read], arturo: true
  def routing
    render json: { error: "InvalidEndpoint", description: "Not found" }, status: :not_found
  end
end
