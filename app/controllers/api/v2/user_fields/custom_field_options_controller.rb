class Api::V2::UserFields::CustomFieldOptionsController < Api::V2::BaseCustomFieldOptionsController
  allow_parameters :all, user_field_id: Parameters.bigid
  before_action :ensure_is_dropdown_field

  ## ### List User Field Options
  ##
  ## `GET /api/v2/user_fields/{field_id}/options.json`
  ##
  ## Returns a list of custom User Field Options for the given dropdown User Field.
  ##
  ## #### Pagination
  ##
  ## * Cursor pagination (recommended)
  ## * Offset pagination
  ##
  ## See [Pagination](./introduction#pagination).
  ##
  ## Returns a maximum of 100 records per page.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Stability
  ##
  ##  * Beta
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{field_id}/options.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##     "count": 2,
  ##     "custom_field_options": [
  ##         {
  ##             "id": 10000,
  ##             "name": "Apples",
  ##             "position": 0,
  ##             "raw_name": "Apples",
  ##             "url": "http://{subdomain}.zendesk.com/api/v2/user_fields/1/options/10000.json",
  ##             "value": "apple"
  ##         },
  ##         {
  ##             "id": 10001,
  ##             "name": "Bananas",
  ##             "position": 1,
  ##             "raw_name": "Bananas",
  ##             "url": "http://{subdomain}.zendesk.com/api/v2/user_fields/1/options/10001.json",
  ##             "value": "banana"
  ##         }
  ##      ],
  ##      "next_page": null,
  ##      "previous_page": null
  ## }
  ## ```
  ##

  ## ### Show a User Field Option
  ## `GET /api/v2/user_fields/{field_id}/options/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{field_id}/options/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##     "custom_field_option": {
  ##         "id": 10001,
  ##         "name": "Bananas",
  ##         "position": 1,
  ##         "raw_name": "Bananas",
  ##         "url": "http://{subdomain}.zendesk.com/api/v2/user_fields/1/options/10001.json",
  ##         "value": "banana"
  ##     }
  ## }
  ## ```

  ## ### Create or Update a User Field Option
  ## `POST /api/v2/user_fields/{field_id}/options.json`
  ##
  ##  Creates a new or updates an existing option for the given dropdown User Field.
  ##
  ##  To update an option, include the id of the option in the "custom_field_option"
  ##  object. Example: `{"custom_field_option": {"id": 10002, "name": "Pineapples", ... }`.
  ##  If an option exists for the given ID, the option will be updated. Otherwise, a new
  ##  option will be created.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl (Create)
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{field_id}/options.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '{"custom_field_option": {"name": "Grapes", "position": 2, "value": "grape"}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## {
  ##     "custom_field_option": {
  ##         "id": 10002,
  ##         "name": "Grapes",
  ##         "position": 2,
  ##         "raw_name": "Grapes",
  ##         "url": "http://{subdomain}.zendesk.com/api/v2/user_fields/1/options/10002.json",
  ##         "value": "grape"
  ##     }
  ## }
  ## ```
  ##
  ## #### Using curl (Update)
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{field_id}/options.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '{"custom_field_option": {"id": 10002, "name": "Pineapples", "position": 2, "value": "pineapple"}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##     "custom_field_option": {
  ##         "id": 10002,
  ##         "name": "Pineapples",
  ##         "position": 2,
  ##         "raw_name": "Pineapples",
  ##         "url": "http://{subdomain}.zendesk.com/api/v2/user_fields/1/options/10002.json",
  ##         "value": "pineapple"
  ##     }
  ## }
  ## ```

  ## ### Delete User Field Option
  ## `DELETE /api/v2/user_fields/{field_id}/options/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{field_id}/options/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##

  protected

  def field_item
    @user_field ||= current_account.custom_fields.for_owner('user').find(params[:user_field_id])
  end

  def presenter_options
    options = {
      route: :api_v2_user_field_option_url,
      parent: 'field'
    }

    options[:with_cursor_pagination] = true if current_account.arturo_enabled?('remove_offset_pagination_custom_fields_options_index') || (current_account.arturo_enabled?('cursor_pagination_custom_fields_options_index') && !with_offset_pagination?) || with_cursor_pagination_v2?

    options
  end

  def ensure_is_dropdown_field
    raise ActiveRecord::RecordNotFound unless field_item.is_a?(CustomField::Dropdown)
  end
end
