class Api::V2::Knowledge::SettingsController < Api::V2::BaseController
  before_action :require_agent!

  allow_parameters :show, {}
  def show
    if stale?(etag: etag)
      render json: presenter.present(current_account)
    end
  end

  private

  def etag
    [
      Arturo::Feature.last_updated_at,
      current_account.settings.pluck(:updated_at).max,
      current_account.subscription.try(:cache_key),
      current_account.feature_boost.try(:cache_key),
      current_account.cache_key
    ]
  end

  def presenter
    @presenter ||= Api::V2::Account::KnowledgeSettingsPresenter.new(
      current_user,
      url_builder: self
    )
  end
end
