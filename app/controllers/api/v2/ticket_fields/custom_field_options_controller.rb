class Api::V2::TicketFields::CustomFieldOptionsController < Api::V2::BaseCustomFieldOptionsController
  allow_parameters :all, ticket_field_id: Parameters.bigid
  before_action :ensure_is_field_tagger

  ## ### List Ticket Field Options
  ## `GET /api/v2/ticket_fields/{field_id}/options.json`
  ##
  ##  Returns a list of all custom ticket field options for the given dropdown ticket field.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Stability
  ##
  ##  * Beta
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{field_id}/options.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##     "count": 2,
  ##     "custom_field_options": [
  ##         {
  ##             "id": 10000,
  ##             "name": "Apples",
  ##             "position": 0,
  ##             "raw_name": "Apples",
  ##             "url": "http://{subdomain}.zendesk.com/api/v2/ticket_fields/1/options/10000.json",
  ##             "value": "apple"
  ##         },
  ##         {
  ##             "id": 10001,
  ##             "name": "Bananas",
  ##             "position": 1,
  ##             "raw_name": "Bananas",
  ##             "url": "http://{subdomain}.zendesk.com/api/v2/ticket_fields/1/options/10001.json",
  ##             "value": "banana"
  ##         }
  ##      ],
  ##      "next_page": null,
  ##      "previous_page": null
  ## }
  ## ```
  ##

  ## ### Show Ticket Field Option
  ## `GET /api/v2/ticket_fields/{field_id}/options/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{field_id}/options/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##     "custom_field_option": {
  ##         "id": 10001,
  ##         "name": "Bananas",
  ##         "position": 1,
  ##         "raw_name": "Bananas",
  ##         "url": "http://{subdomain}.zendesk.com/api/v2/ticket_fields/1/options/10001.json",
  ##         "value": "banana"
  ##     }
  ## }
  ## ```

  ## ### Create or Update Ticket Field Option
  ## `POST /api/v2/ticket_fields/{field_id}/options.json`
  ##
  ##  Creates or updates an option for the given dropdown ticket field.
  ##
  ##  To update an option, include the id of the option in the "custom_field_option"
  ##  object. Example:
  ##
  ##  `{"custom_field_option": {"id": 10002, "name": "Pineapples", ... }`
  ##
  ##  If an option exists for the given ID, the option will be updated. Otherwise, a new
  ##  option will be created.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl (Create)
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{field_id}/options.json \
  ##   -d '{"custom_field_option": {"name": "Grapes", "position": 2, "value": "grape"}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## {
  ##     "custom_field_option": {
  ##         "id": 10002,
  ##         "name": "Grapes",
  ##         "position": 2,
  ##         "raw_name": "Grapes",
  ##         "url": "http://{subdomain}.zendesk.com/api/v2/ticket_fields/1/options/10002.json",
  ##         "value": "grape"
  ##     }
  ## }
  ## ```
  ##
  ## #### Using curl (Update)
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{field_id}/options.json \
  ##   -d '{"custom_field_option": {"id": 10002, "name": "Pineapples", "position": 2, "value": "pineapple"}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##     "custom_field_option": {
  ##         "id": 10002,
  ##         "name": "Pineapples",
  ##         "position": 2,
  ##         "raw_name": "Pineapples",
  ##         "url": "http://{subdomain}.zendesk.com/api/v2/ticket_fields/1/options/10002.json",
  ##         "value": "pineapple"
  ##     }
  ## }
  ## ```

  ## ### Delete Ticket Field Option
  ## `DELETE /api/v2/ticket_fields/{field_id}/options/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{field_id}/options/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##

  protected

  def field_item
    @ticket_field ||= current_account.ticket_fields.find(params[:ticket_field_id])
  end

  def presenter_options
    {
      route: :api_v2_ticket_field_option_url,
      parent: 'custom_field'
    }
  end

  def ensure_is_field_tagger
    raise ActiveRecord::RecordNotFound unless field_item.is_a?(FieldTagger)
  end
end
