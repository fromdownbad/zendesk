class Api::V2::Stats::BenchmarksController < Api::V2::BaseController
  include Zendesk::Stats::StatRollup::Benchmarking

  before_action :require_agent!
  before_action :load_benchmarking_stats

  # ### Show Benchmark
  # `GET /api/v2/stats/benchmarks/{category}.json?type={type}`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/stats/benchmarks/{category}.json?type={type} \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "benchmark": {
  #     "category": "industry",
  #     "type":     "software",
  #     "data:": {
  #       "csr":                 0.95,
  #       "first_response_time": 108000,
  #       "created_count":       777
  #     }
  #   }
  # }
  # ```
  allow_parameters :show,
    category: Parameters.enum('industry', 'target_audience', 'employee_count', 'overall'),
    type: Parameters.string | Parameters.nil_string
  def show
    render json: presenter.present(benchmarking_stats)
  end

  private

  def presenter
    @presenter ||= Api::V2::Stats::BenchmarkPresenter.new(
      current_user,
      url_builder: self,
      category: params[:category],
      type: params[:type]
    )
  end
end
