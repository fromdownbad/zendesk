require 'zendesk/offset_pagination_limiter'

module Api
  module V2
    class OrganizationMembershipsController < Api::V2::BaseController
      require_that_user_can! :edit, :organization_membership, only: [:destroy, :make_default]
      require_that_user_can! :edit, :new_organization_membership, only: [:create_many, :create]
      require_that_user_can! :edit, OrganizationMembership, only: [:destroy_many]

      around_action :handle_rate_limiting_on_deep_offset_pagination, only: [:index]

      allow_parameters :all, user_id: Parameters.bigid, organization_id: Parameters.bigid

      organization_membership_parameter = { user_id: Parameters.bigid, organization_id: Parameters.bigid }

      CBP_SORTABLE_FIELDS = {
        'id' => :id,
        'default' => :default
      }.freeze

      ## ### List Memberships
      ##
      ## * `GET /api/v2/organization_memberships.json`
      ## * `GET /api/v2/users/{user_id}/organization_memberships.json`
      ## * `GET /api/v2/organizations/{organization_id}/organization_memberships.json`
      ##
      ## Returns a list of organization memberships for the account, user or organization in question.
      ##
      ## Note: When returning organization memberships for a user, organization memberships are sorted with the default organization first, and then by organization name.
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For:
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organization_memberships.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "organization_memberships": [
      ##     {
      ##       "id":              4,
      ##       "user_id":         29,
      ##       "organization_id": 12,
      ##       "default":         true,
      ##       "created_at":      "2009-05-13T00:07:08Z",
      ##       "updated_at":      "2011-07-22T00:11:12Z"
      ##     },
      ##     {
      ##       "id":              49,
      ##       "user_id":         155,
      ##       "organization_id": 3,
      ##       "default":         null,
      ##       "created_at":      "2012-03-13T22:01:32Z",
      ##       "updated_at":      "2012-03-13T22:01:32Z"
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, {}
      allow_cursor_pagination_v2_parameters :index
      def index
        throttle_index_with_deep_offset_pagination
        if current_account.arturo_enabled?('remove_offset_pagination_organization_memberships_index') || (current_account.arturo_enabled?('cursor_pagination_organization_memberships_index') && !with_offset_pagination?) || with_cursor_pagination_v2?
          sorted_records = if params[:user_id]
            sorted_scope_for_cursor(organization_memberships(skip_pagination: true), CBP_SORTABLE_FIELDS)
          else
            sorted_scope_for_cursor(organization_memberships(skip_pagination: true))
          end

          render json: cursor_presenter.present(paginate_with_cursor(sorted_records))
        else
          render json: presenter.present(organization_memberships)
        end
      end

      ## ### Show Membership
      ## `GET /api/v2/organization_memberships/{id}.json`
      ##
      ## `GET /api/v2/users/{user_id}/organization_memberships/{id}.json`
      ##
      ## Shows a specific organization membership.
      ##
      ## #### Allowed For
      ##
      ##  * End Users
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organization_memberships/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "organization_membership": {
      ##      "id":              4,
      ##      "user_id":         29,
      ##      "organization_id": 12,
      ##      "default":         true,
      ##      "created_at":      "2009-05-13T00:07:08Z",
      ##      "updated_at":      "2011-07-22T00:11:12Z"
      ##   }
      ## }
      ## ```
      resource_action :show

      ## ### Create Membership
      ## `POST /api/v2/organization_memberships.json`
      ##
      ## `POST /api/v2/users/{user_id}/organization_memberships.json`
      ##
      ## Assigns a user to a given organization. Returns an error with status 422 if the user is already assigned to the organization.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organization_memberships.json \
      ##   -X POST -d '{"organization_membership": {"user_id": 72, "organization_id": 88}}' \
      ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/organization_memberships/{id}.json
      ##
      ## {
      ##   "organization_membership": {
      ##      "id":              461,
      ##      "user_id":         72,
      ##      "organization_id": 88,
      ##      "default":         true,
      ##      "created_at":      "2012-04-03T12:34:01Z",
      ##      "updated_at":      "2012-04-03T12:34:01Z"
      ##   }
      ## }
      ## ```
      allow_parameters :create, organization_membership: organization_membership_parameter
      require_oauth_scopes(:create, scopes: [:"organizations:write", :"users:write", :write])
      def create
        # If multiple organizations is disabled and user already has an organization membership,
        # tickets will be moved by the OrganizationMembership after_destroy :move_user_tickets callback
        # when the existing organization membership is destroyed.
        should_move_tickets = new_organization_membership.user && !new_organization_membership.user.organization_memberships.exists?

        new_organization_membership.save!

        # If account does not have the multiple organizations feature, replace organization memberships instead of adding.
        unless current_account.has_multiple_organizations_enabled?
          new_organization_membership.user.organization_memberships.where('id <> ?', new_organization_membership.id).destroy_all
        end

        if should_move_tickets
          new_organization_membership.move_user_tickets_from_any
        end

        render json: presenter.present(new_organization_membership), status: :created, location: presenter.url(new_organization_membership)
      end

      ## ### Create Many Memberships
      ## `POST /api/v2/organization_memberships/create_many.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organization_memberships/create_many.json \
      ##   -H "Content-Type: application/json" \
      ##   -d '{"organization_memberships": [{"user_id": 72, "organization_id": 88}, {"user_id": 27, "organization_id": 88}]}' \
      ##   -v -u {email_address}:{password} -X POST
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :create_many, organization_memberships: [organization_membership_parameter]
      def create_many
        if params[:organization_memberships].blank?
          raise ActionController::ParameterMissing, "Invalid organization_memberships"
        elsif !current_account.settings.multiple_organizations? && !params[:organization_memberships].group_by { |m| m[:user_id] }.select { |_k, v| v.size > 1 }.empty?
          raise ZendeskApi::Controller::Errors::TooManyValuesError.new("organization_memberships", 1)
        end
        render json: job_status_presenter.present(enqueue_job_with_status(OrganizationMembershipBulkCreateJob, params))
      end

      ## ### Delete Membership
      ## `DELETE /api/v2/organization_memberships/{id}.json`
      ##
      ## `DELETE /api/v2/users/{user_id}/organization_memberships/{id}.json`
      ##
      ## Immediately removes a user from an organization and schedules a job to unassign all working tickets
      ## currently assigned to the user and organization combination. The **organization_id** of the
      ## unassigned tickets is set to "null".
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organization_memberships/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      resource_action :destroy

      ## ### Bulk Delete Memberships
      ## `DELETE /api/v2/organization_memberships/destroy_many.json?ids={ids}`
      ##
      ## Immediately removes a user from an organization and schedules a job to unassign all working tickets
      ## currently assigned to the user and organization combination. The **organization_id** of the unassigned tickets
      ## is set to "null".
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organization_memberships/destroy_many.json?ids=1,2,3 \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :destroy_many, ids: Parameters.ids
      def destroy_many
        if many_ids?
          job = enqueue_job_with_status(OrganizationMembershipBulkDeleteJob, params.merge(ids: many_ids))
          render json: job_status_presenter.present(job)
        else
          raise ActionController::ParameterMissing, "Organization membership ids must be present"
        end
      end

      ## ### Set Membership as Default
      ## `PUT /api/v2/users/{id}/organization_memberships/{membership_id}/make_default.json`
      ##
      ## Sets the default organization membership of a given user.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/organization_memberships/{org_membership_id}/make_default.json \
      ##   -v -u {email_address}:{password} -X PUT -d '{}' -H "Content-Type: application/json"
      ## ```
      ##
      ## #### Example Response
      ##
      ## Same as List Memberships
      allow_parameters :make_default, id: Parameters.bigid
      require_oauth_scopes :make_default, scopes: [:write], arturo: true
      def make_default
        organization_membership.update_attributes!(default: true)
        render json: presenter.present(organization_memberships)
      end

      allow_parameters :count, organization_membership_parameter
      def count
        render json: { count: record_counter.present, links: { url: request.original_url } }
      end

      protected

      def max_per_page
        1000
      end

      def presenter
        @presenter ||= Api::V2::OrganizationMembershipPresenter.new(current_user, url_builder: self, includes: includes)
      end

      def cursor_presenter
        @cursor_presenter ||= Api::V2::OrganizationMembershipPresenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
      end

      def organization_memberships(skip_pagination: false)
        @organization_memberships ||= begin
          scope = organization_membership_scope.organization_memberships.active
          options = pagination unless skip_pagination

          if params[:user_id] # not necessary for organization and too slow for account-wide
            scope = scope.joins(:organization)
            # Order SQL: `ORDER BY `organization_memberships`.`default` DESC, `organizations`.`name` ASC`
            scope = scope.order(Organization.arel_table[:name].asc).order(default: :desc)
          end

          skip_pagination ? scope : scope.paginate(options)
        end
      end

      def assignable_organization_memberships
        @assignable_organization_memberships ||= organization_membership_scope.organization_memberships.assignable(current_user).paginate(pagination)
      end

      def organization_membership
        @organization_membership ||= organization_membership_scope.organization_memberships.find(params[:id])
      end

      def new_organization_membership
        @new_organization_membership ||= organization_membership_scope.organization_memberships.new(params[:organization_membership])
      end

      def organization_membership_scope
        @organization_membership_scope ||= finder.membership_scope
      end

      def job_status_presenter
        @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
      end

      private

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: "organization_memberships")
      end

      def throttle_index_with_deep_offset_pagination
        return nil unless params[:page].is_a?(Integer)

        Zendesk::OffsetPaginationLimiter.new(
          entity: controller_name,
          account_id: current_account.id,
          account_subdomain: current_account.subdomain,
          page: params[:page],
          per_page: per_page,
          log_only: true
        ).throttle_index_with_deep_offset_pagination
      end

      def handle_rate_limiting_on_deep_offset_pagination
        yield
      rescue Prop::RateLimited => e
        if e.handle == :index_organization_memberships_with_deep_offset_pagination_ceiling_limit
          Rails.logger.info("[OrganizationMembershipsController] Rate limiting #index by account: #{current_account.subdomain}, page: #{params[:page]}, per_page: #{per_page}, throttle key: index_organization_memberships_with_deep_offset_pagination_ceiling_limit")
          statsd_client.increment("api_v2_organization_memberships_index_ceiling_rate_limit", tags: ["account_id:#{current_account.id}"])
          response.headers['Retry-After'] = e.retry_after.to_s
          render plain: I18n.t("txt.api.v2.organization_memberships.index_with_deep_offset_pagination.rate_limit_by_account") + " per_page: #{params[:per_page]}, page: #{params[:page]}", status: 429, content_type: Mime[:text].to_s
        else
          Rails.logger.warn("Api::V2::OrganizationMembershipsController Rate Limited #{e.message}")
          raise e
        end
      end

      def finder
        @organization_memberships_finder ||= Zendesk::OrganizationMemberships::Finder.new(
          current_account,
          params
        )
      end

      def record_counter
        @record_counter ||= begin
          Zendesk::RecordCounter::OrganizationMemberships.new(
            account: current_account,
            options: {
              user_id: params[:user_id],
              organization_id: params[:organization_id]
            }
          )
        end
      end
    end
  end
end
