module Api
  module V2
    class IdentitiesController < Api::V2::BaseController
      include Zendesk::Users::Identities::ControllerSupport
      allow_subsystem_user :profile_service, only: [:index, :create]

      before_action :ensure_identity_verifiable, only: [:request_verification]

      identity_parameter = {
        type: Parameters.string,
        value: Parameters.string,
        primary: Parameters.boolean,
        verified: Parameters.boolean, # if current_user.is_agent?
        skip_verify_email: Parameters.boolean,
        deliverable_state: Parameters.string
      }

      CBP_SORTABLE_FIELDS = {
        'id' => :id,
        'priority' => :priority
      }.freeze

      allow_parameters :all, user_id: Parameters.bigid

      ## ### List Identities
      ## `GET /api/v2/users/{user_id}/identities.json`
      ##
      ## Returns a list of identities for the given user.
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response:
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "identities": [
      ##     {
      ##       "id":              35436,
      ##       "user_id":         135,
      ##       "type":            "email",
      ##       "value":           "someone@example.com",
      ##       "verified":        true,
      ##       "primary":         true,
      ##       "updated_at":      "2011-07-20T22:55:29Z",
      ##       "created_at":      "2011-07-20T22:55:29Z"
      ##     },
      ##     {
      ##       "id":              77136,
      ##       "user_id":         135,
      ##       "type":            "twitter",
      ##       "value":           "didgeridooboy",
      ##       "verified":        true,
      ##       "primary":         false,
      ##       "updated_at":      "2012-02-12T14:25:21Z",
      ##       "created_at":      "2012-02-12T14:25:21Z"
      ##     },
      ##     {
      ##       "id":              88136,
      ##       "user_id":         135,
      ##       "type":            "phone_number",
      ##       "value":           "+1 555-123-4567",
      ##       "verified":        true,
      ##       "primary":         true,
      ##       "updated_at":      "2012-02-12T14:25:21Z",
      ##       "created_at":      "2012-02-12T14:25:21Z"
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, {}
      allow_cursor_pagination_v2_parameters :index
      def index
        if stale?(etag: user, last_modified: user.updated_at)
          if current_account.arturo_enabled?('remove_offset_pagination_identities_index') || (current_account.arturo_enabled?('cursor_pagination_identities_index') && !with_offset_pagination?) || with_cursor_pagination_v2?
            # reorder needed here for cursor pagination until zendesk_core's order is changed from a string to a symbol
            sorted_records = sorted_scope_for_cursor(identities.native.reorder(:priority), CBP_SORTABLE_FIELDS)
            render json: cursor_presenter.present(paginate_with_cursor(sorted_records))
          else
            render json: presenter.present(identities.native)
          end
        end
      end

      ## ### Show Identity
      ## `GET /api/v2/users/{user_id}/identities/{id}.json`
      ##
      ## Shows the identity with the given id for a given user.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response:
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "identity": {
      ##     "id":         77938,
      ##     "user_id":    13531,
      ##     "type":       "twitter",
      ##     "value":      "cabanaboy",
      ##     "verified":   false,
      ##     "primary":    false,
      ##     "updated_at": "2012-02-12T14:25:21Z",
      ##     "created_at": "2012-02-12T14:25:21Z"
      ##   }
      ## }
      ## ```
      resource_action :show

      ## ### Create Identity
      ## `POST /api/v2/users/{user_id}/identities.json`
      ##
      ## `POST /api/v2/end_users/{user_id}/identities.json`
      ##
      ##  Adds an identity to a user's profile. An agent can add an identity to any user
      ##  profile. An end user can only add an identity to their own user profile.
      ##
      ##  Use the first endpoint if authenticating as an agent. Use the second if authenticating
      ##  as an end user. End users must authenticate with a token.
      ##
      ##  Supported identity types:
      ##
      ## | Type             | Example |
      ## | ---------------- | ------- |
      ## | email            | `{ "type" : "email", "value" : "someone@example.com" }` |
      ## | twitter          | `{ "type" : "twitter", "value" : "screen_name" }` |
      ## | facebook         | `{ "type" : "facebook", "value" : "855769377321" }` |
      ## | google           | `{ "type" : "google", "value" : "example@gmail.com" }` |
      ## | agent_forwarding | `{ "type" : "agent_forwarding", "value" : "+1 555-123-4567" }` |
      ## | phone_number     | `{ "type" : "phone_number", "value" : "+1 555-123-4567" }` |
      ##
      ##  To create an identity without sending out a verification email, include
      ##  a `"verified": true` property.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##  * Verified end users
      ##
      ## #### Using curl (agents only)
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities.json \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -d '{"identity": {"type": "email", "value": "foo@bar.com"}}' -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Using curl (verified end users only)
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/end_users/{user_id}/identities.json \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -d '{"identity": {"type": "email", "value": "foo@bar.com"}}' -v -u {email_address}/token:{api_token}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/users/135/identities/78138.json
      ##
      ## {
      ##   "identity": {
      ##     "id":         77938,
      ##     "user_id":    13531,
      ##     "type":       "twitter",
      ##     "value":      "cabanaboy",
      ##     "verified":   false,
      ##     "primary":    false,
      ##     "updated_at": "2012-02-12T14:25:21Z",
      ##     "created_at": "2012-02-12T14:25:21Z"
      ##   }
      ## }
      ## ```
      allow_parameters :create, identity: identity_parameter, brand_id: Parameters.bigid
      def create
        update_active_brand_id(new_identity)
        new_identity.user.skip_verify_email = should_skip_verification_email?
        new_identity.save!
        render json: presenter.present(new_identity), status: :created, location: presenter.url(new_identity)
      end

      ## ### Update Identity
      ## `PUT /api/v2/users/{user_id}/identities/{id}.json`
      ##
      ##  This endpoint allows you to:
      ##
      ##  * Set the specified identity as verified (but you cannot unverify a verified identity)
      ##  * Update the `value` property of the specified identity
      ##
      ## You can't change an identity's `primary` attribute with this endpoint. You must use the
      ## [Make Identity Primary](#make-identity-primary) endpoint instead.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ##  To update `verified`:
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{id}.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{"identity": {"verified": true}}' -v -u {email_address}:{password}
      ## ```
      ##
      ##  To update `value`:
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{id}.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{"identity": {"value": "foo@bar.com"}}' -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "identity": {
      ##     {
      ##       "id":              35436,
      ##       "user_id":         135,
      ##       "type":            "email",
      ##       "value":           "someone@example.com",
      ##       "verified":        true,
      ##       "primary":         true,
      ##       "updated_at":      "2011-07-20T22:55:29Z",
      ##       "created_at":      "2011-07-20T22:55:29Z",
      ##       "deliverable_state": "deliverable"
      ##     }
      ##   }
      ## }
      ## ```
      allow_parameters :update, id: Parameters.bigid, identity: identity_parameter
      def update
        if params[:identity]
          # Identity was verified
          is_owner_identity = identity.user_id == current_account.owner_id
          should_verify = params[:identity][:verified] && !identity.is_verified? && !is_owner_identity

          value = params[:identity][:value]
          identity.update!(value: value) if value
          identity.verify! if should_verify && params[:identity][:verified]

          # Identity was marked deliverable
          if identity.deliverable_state == Zendesk::Types::DeliverableStateType.UNDELIVERABLE
            deliverable_name = Zendesk::Types::DeliverableStateType[Zendesk::Types::DeliverableStateType.DELIVERABLE].name
            if params[:identity][:deliverable_state] == deliverable_name
              identity.mark_deliverable
            end
          end
        end

        render json: presenter.present(identity), location: api_v2_user_identity_url(user, identity, format: "json")
      end

      ## ### Make Identity Primary
      ## `PUT /api/v2/users/{user_id}/identities/{id}/make_primary`
      ##
      ## `PUT /api/v2/end_users/{user_id}/identities/{id}/make_primary`
      ##
      ##  Sets the specified identity as primary. To change other attributes, use
      ##  the [Update  Identity](#update-identity) endpoint. This is a collection-level
      ##  operation and the correct behavior for an API client is to subsequently reload the
      ##  entire collection.
      ##
      ##  The first endpoint is the preferred option if authenticating as an agent. If authenticating
      ##  as an end user, you can only use the second endpoint. End users must authenticate with a token.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##  * Verified end users
      ##
      ## #### Using curl (agents only)
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{id}/make_primary.json \
      ##   -X PUT -v -u {email_address}:{password} -d '{}' -H "Content-Type: application/json"
      ## ```
      ##
      ## #### Using curl (verified end users only)
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/end_users/{user_id}/identities/{id}/make_primary.json \
      ##   -X PUT -v -u {email_address}:{password} -d '{}' -H "Content-Type: application/json"
      ## ```
      ##
      ## #### Example Response
      ##
      ## Same as [List Identities](#list-identities) above.
      ##
      allow_parameters :make_primary, id: Parameters.bigid
      require_oauth_scopes :make_primary, scopes: [:write], arturo: true
      def make_primary
        identity.make_primary!
        render json: presenter.present(user.identities.native)
      end

      ## ### Verify Identity
      ## `PUT /api/v2/users/{user_id}/identities/{id}/verify`
      ##
      ##  Sets the specified identity as verified.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{id}/verify.json \
      ##   -X PUT -v -u {email_address}:{password} -d '{}' -H "Content-Type: application/json"
      ## ```
      ##
      ## #### Example Response
      ##
      ## Same as [Show Identity](#show-identity) above.
      ##
      allow_parameters :verify, id: Parameters.bigid
      require_oauth_scopes :verify, scopes: [:write], arturo: true
      def verify
        unless identity.is_verified?
          log_unverified_account('verify') unless current_account.owner.is_verified
          identity.verify!
        end

        render json: presenter.present(identity), location: api_v2_user_identity_url(user, identity, format: "json")
      end

      ## ### Request User Verification
      ## `PUT /api/v2/users/{user_id}/identities/{id}/request_verification.json`
      ##
      ##  Sends the user a verification email with a link to verify ownership of the email address.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{id}/request_verification.json \
      ##   -X PUT -v -u {email_address}:{password} -d '{}' -H "Content-Type: application/json"
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## ```
      allow_parameters :request_verification, id: Parameters.bigid, primary: Parameters.boolean, brand_id: Parameters.bigid
      require_oauth_scopes :request_verification, scopes: [:write], arturo: true
      def request_verification
        update_active_brand_id(identity)
        if identity.is_a?(UserEmailIdentity) && params[:primary]
          identity.set_primary_when_verifying!
        end
        identity.send_verification_email
        head :ok
      end

      ## ### Delete Identity
      ## `DELETE /api/v2/users/{user_id}/identities/{id}.json`
      ##
      ##  Deletes the identity for a given user.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{id}.json \
      ##   -X DELETE -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      resource_action :destroy

      allow_parameters :count, {}
      def count
        render json: { count: record_counter.present, links: { url: request.original_url } }
      end

      protected

      def update_active_brand_id(user_identity)
        if params[:brand_id] && current_account.has_multiple_active_brands?
          user_identity.user.active_brand_id = params[:brand_id]
        end
      end

      def presenter
        @presenter ||= Api::V2::IdentityPresenter.new(current_user, url_builder: self)
      end

      def cursor_presenter
        @cursor_presenter ||= Api::V2::IdentityPresenter.new(current_user, url_builder: self, with_cursor_pagination: true)
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'api.v2.identities_controller')
      end

      def log_unverified_account(action)
        Rails.logger.info "api.v2.identities_controller action #{action} by unverified account #{current_account.id}"
        statsd_client.increment('unverified_account', tags: ["action:#{action}"])
      end

      private

      def record_counter
        @record_counter ||= begin
          Zendesk::RecordCounter::Identities.new(
            account: current_account,
            options: { user_id: params[:user_id] }
          )
        end
      end
    end
  end
end
