class Api::V2::AutocompleteController < Api::V2::BaseController
  ## ### Autocomplete Tags
  ## `GET /api/v2/autocomplete/tags.json?name={name}`
  ##
  ## Returns an array of registered and recent tag names that start with the specified name.
  ## The name must be at least 2 characters in length.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/autocomplete/tags.json?name=att \
  ##   -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tags": [ "attention", "attack" ]
  ## }
  ## ```
  before_action :set_search_cache_control_header
  before_action :check_search_short_circuit_limiter
  allow_parameters :tags, name: Parameters.string
  require_oauth_scopes :tags, scopes: [:read], arturo: true
  def tags
    render json: { tags: Thesaurus.complete(params[:name], current_account, current_user) }
  end
end
