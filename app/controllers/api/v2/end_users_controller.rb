class Api::V2::EndUsersController < Api::V2::UsersController
  skip_before_action :require_agent!
  before_action :require_current_user!

  ## ### Show User
  ##
  ## `GET /api/v2/end_users/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * End Users
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/end_users/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "user": {
  ##     "id":   35436,
  ##     "name": "Johnny End User",
  ##     ...
  ##   }
  ## }
  ## ```

  ## ### Update User
  ## `PUT /api/v2/end_users/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/end_users/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT -d '{"user": {"name": "Roger Wilco II"}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "user": {
  ##     "id":   9873843,
  ##     "name": "Roger Wilco II",
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.bigid, user: {
    name: Parameters.string,
    time_zone: Parameters.string,
    email: Parameters.string,
    phone: Parameters.string,
    remote_photo_url: Parameters.string,
    photo: Parameters.anything,
    identities: [Parameters.anything]
  }

  protected

  alias_method :resource, :user

  def presenter
    @presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self, includes: includes)
  end

  def require_current_user!
    deny_access unless user == current_user
  end

  def user
    @user ||= current_account.end_users.verified.find(params[:id]).tap do |user|
      user.current_user = current_user
    end
  end
end
