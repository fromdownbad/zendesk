module Api
  module V2
    class CustomRolesController < Api::V2::BaseController
      before_action :require_custom_roles_availability!, only: :index
      before_action :require_permission_sets!, except: :index

      allow_subsystem_user :metropolis
      allow_subsystem_user :embeddable
      allow_subsystem_user :zdp_metadata, only: [:index]
      allow_cors_from_zendesk_external_domains :index
      allow_cors_from_zendesk_external_domains :show

      CONFIG_PARAMS = {
        assign_tickets_to_any_group:          Parameters.boolean,
        end_user_list_access:                 Parameters.enum('full', 'none'),
        end_user_profile_access:              Parameters.enum('edit', 'edit-within-org', 'full', 'readonly'),
        explore_access:                       Parameters.enum('edit', 'full', 'none', 'readonly'),
        forum_access:                         Parameters.enum('edit-topics', 'full', 'readonly'),
        forum_access_restricted_content:      Parameters.boolean,
        macro_access:                         Parameters.enum('full', 'manage-group', 'manage-personal', 'readonly'),
        manage_business_rules:                Parameters.boolean,
        manage_dynamic_content:               Parameters.boolean,
        manage_extensions_and_channels:       Parameters.boolean,
        manage_facebook:                      Parameters.boolean,
        organization_editing:                 Parameters.boolean,
        report_access:                        Parameters.enum('full', 'none', 'readonly'),
        side_conversation_create:             Parameters.boolean,
        ticket_access:                        Parameters.enum('all', 'assigned-only', 'within-groups', 'within-organization'),
        ticket_bulk_edit:                     Parameters.boolean,
        ticket_comment_access:                Parameters.enum('public', 'none'),
        ticket_deletion:                      Parameters.boolean,
        ticket_editing:                       Parameters.boolean,
        ticket_merge:                         Parameters.boolean,
        ticket_tag_editing:                   Parameters.boolean,
        twitter_search_access:                Parameters.boolean, # also requires account.has_twitter_saved_search?
        user_view_access:                     Parameters.enum('full', 'manage-group', 'manage-personal', 'none', 'readonly'),
        view_access:                          Parameters.enum('full', 'manage-group', 'manage-personal', 'playonly', 'readonly'),
        view_deleted_tickets:                 Parameters.boolean,
        view_ticket_satisfaction_prediction:  Parameters.boolean,
        voice_access:                         Parameters.boolean,
        voice_dashboard_access:               Parameters.boolean
      }.freeze

      ## ### List Custom Roles
      ## `GET /api/v2/custom_roles.json`
      ##
      ## #### Availability
      ##
      ##  * Accounts on the Enterprise plan
      ##  * Accounts on the Professional plan with the Light Agents add-on
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/custom_roles.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "custom_roles": [
      ##     {
      ##       "id":             16,
      ##       "name":           "Advisor",
      ##       "description":    "Advisors manage the workflow and configure the help desk. They create or manage automations, macros, triggers, views, and SLA targets. They also set up channels and extensions. Advisors don't solve tickets, they can only make private comments.",
      ##       "created_at":     "2012-03-12T16:32:22Z",
      ##       "updated_at":     "2012-03-12T16:32:22Z",
      ##       "configuration": {
      ##         "chat_access":                     true,
      ##         ...
      ##         "user_view_access":                "full"
      ##       },
      ##     },
      ##     {
      ##       "id":             6,
      ##       "name":           "Staff",
      ##       "description":    "A Staff agent's primary role is to solve tickets. They can edit tickets within their groups, view reports, and add or edit personal views and macros.",
      ##       "created_at":     "2011-07-20T04:31:29Z",
      ##       "updated_at":     "2012-02-02T10:32:59Z",
      ##       "configuration": {
      ##         "chat_access":                     true,
      ##         ...
      ##         "user_view_access":                "full"
      ##       },
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, {}
      def index
        expires_in(60.seconds) if current_account.has_custom_roles_cache_control_header? && app_request?
        permission_sets = custom_roles_resolver.assignable_permission_sets
        render json: presenter.present(permission_sets)
      end

      ## ### Show Custom Role
      ## `GET /api/v2/custom_roles/{id}.json`
      ##
      ## #### Availability
      ##
      ##  * Accounts on the Enterprise plan
      ##
      ## #### Allowed For
      ##
      ##  * Administrators
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/custom_roles/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ## "custom_role": {
      ##   "id": 10127,
      ##   "name": "sample role",
      ##   "description": "sample description",
      ##   "role_type": 0,
      ##   "created_at": "2018-09-21T18:12:32Z",
      ##   "updated_at": "2018-09-21T18:12:32Z",
      ##   "configuration": {
      ##     "chat_access": true,
      ##     ...
      ##     "user_view_access": "readonly"
      ##   }
      ## }
      ## ```
      allow_parameters :show, id: Parameters.bigid
      def show
        render json: presenter.present(current_permission_set)
      end

      ## ### Create Custom Role
      ## `POST /api/v2/custom_roles.json`
      ##
      ## #### Availability
      ##
      ##  * Accounts on the Enterprise plan
      ##
      ## #### Allowed For
      ##
      ##  * Administrators
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/custom_roles.json \
      ##   -v -u {email_address}:{password} \
      ##   -H "Content-Type: application/json" \
      ##   -d '{ "custom_role": {
      ##     "name": "sample role",
      ##     "description": "sample description",
      ##     "configuration": {
      ##       "chat_access": true,
      ##       ...
      ##       "user_view_access": "readonly"
      ##     }
      ##   }}'
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ## "custom_role": {
      ##   "id": 10127,
      ##   "name": "sample role",
      ##   "description": "sample description",
      ##   "role_type": 0,
      ##   "created_at": "2018-09-21T18:12:32Z",
      ##   "updated_at": "2018-09-21T18:12:32Z",
      ##   "configuration": {
      ##     "chat_access": true,
      ##     ...
      ##     "user_view_access": "readonly"
      ##   }
      ## }
      ## ```
      allow_parameters :create,
        custom_role: {
          name:           Parameters.string,
          description:    Parameters.string,
          configuration:  Parameters.map(CONFIG_PARAMS)
        }
      def create
        custom_role_params, config = atts_and_permissions_from_params
        permission_set = current_account.permission_sets.new(custom_role_params)
        permission_set.permissions.set(config)

        Zendesk::SupportUsers::PermissionSet.new(permission_set).save!(context: 'api_create')

        if current_account.has_permissions_custom_role_policy_dual_save?
          handle_create_policy(permission_set)
        else
          render json: presenter.present(permission_set)
        end
      end

      ## ### Update Custom Role
      ## `PUT /api/v2/custom_roles/{id}.json`
      ##
      ## #### Availability
      ##
      ##  * Accounts on the Enterprise plan
      ##
      ## #### Allowed For
      ##
      ##  * Administrators
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/custom_roles/{id}.json \
      ##   -v -u {email_address}:{password} \
      ##   -H "Content-Type: application/json" \
      ##   -X PUT \
      ##   -d '{ "custom_role": {
      ##     "name": "updated sample role",
      ##     "description": "sample description",
      ##     "configuration": {
      ##       "chat_access": true,
      ##       ...
      ##       "user_view_access": "readonly"
      ##     }
      ##   }}'
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ## "custom_role": {
      ##   "id": 10127,
      ##   "name": "updated sample role",
      ##   "description": "sample description",
      ##   "role_type": 0,
      ##   "created_at": "2018-09-21T18:12:32Z",
      ##   "updated_at": "2018-09-21T18:12:32Z",
      ##   "configuration": {
      ##     "chat_access": true,
      ##     ...
      ##     "user_view_access": "readonly"
      ##   }
      ## }
      ## ```
      allow_parameters :update,
        id: Parameters.bigid,
        custom_role: {
          name:           Parameters.string,
          description:    Parameters.string,
          configuration:  Parameters.map(CONFIG_PARAMS)
        }
      def update
        custom_role_params, config = atts_and_permissions_from_params
        current_permission_set.assign_attributes(custom_role_params)
        current_permission_set.permissions.set(config)
        Zendesk::SupportUsers::PermissionSet.new(current_permission_set).save!(context: 'api_update')
        render json: presenter.present(current_permission_set)
      end

      ## ### Delete Custom Role
      ## `DELETE /api/v2/custom_roles/{id}.json`
      ##
      ## #### Availability
      ##
      ##  * Accounts on the Enterprise plan
      ##
      ## #### Allowed For
      ##
      ##  * Administrators
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/custom_roles/{id}.json \
      ##   -v -u {email_address}:{password} \
      ##   -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ##
      ## ```
      allow_parameters :destroy, id: Parameters.bigid
      def destroy
        permissions_service_failure_handler = lambda { |_| render_failure(status: :internal_server_error, title: 'Internal Server Error', message: 'Internal Server Error') }
        delete_permission_set_handler = lambda { |perm_set| handle_delete_permission_set(perm_set) }

        policies_helper.handle_delete!(current_permission_set, delete_permission_set_handler, permissions_service_failure_handler)
      end

      protected

      def current_permission_set
        @current_permission_set ||= current_account.permission_sets.find(params[:id])
      end

      def presenter
        @presenter ||= Api::V2::Roles::CustomRolePresenter.new(current_user, url_builder: self)
      end

      def custom_roles_resolver
        @custom_roles_resolver ||= Zendesk::Accounts::CustomRolesResolver.new(current_account)
      end

      def require_custom_roles_availability!
        unless custom_roles_resolver.enabled_permission_sets?
          render_failure(status: :payment_required, title: 'Payment Required', message: "Access to this page is restricted. Please contact the account owner of this help desk for further help.")
        end
      end

      def require_permission_sets!
        unless current_account.has_permission_sets?
          render_failure(status: :payment_required, title: 'Payment Required', message: "Access to this API is restricted. Please contact the account owner of this help desk for further help.")
        end
      end

      def atts_and_permissions_from_params
        custom_role_params = params.require(:custom_role)
        config = custom_role_params.delete(:configuration) || {}

        {
          'end_user_list_access' =>           'available_user_lists',
          'end_user_profile_access' =>        'end_user_profile',
          'manage_business_rules' =>          'business_rule_management',
          'manage_extensions_and_channels' => 'extensions_and_channel_management',
          'organization_editing' =>           'edit_organizations',
          'ticket_comment_access' =>          'comment_access',
          'ticket_tag_editing' =>             'edit_ticket_tags',
          'twitter_search_access' =>          'view_twitter_searches',
          'voice_access' =>                   'voice_availability_access'
        }.each do |k, v|
          config[v] = config.delete(k) if config.key?(k)
        end

        # The presenter for `end_user_list_access` shows the string `full` when
        # we store `all` in the backend.  For parity, we need to convert `full`
        # to `all`
        config['available_user_lists'] = 'all' if config['available_user_lists'] == 'full'

        [custom_role_params, config]
      end

      private

      def policies_helper
        @permission_set_policies_helper ||= Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(current_account.subdomain, current_user.id)
      end

      def handle_create_policy(permission_set)
        response = policies_helper.create_policy!(permission_set, {})
        if response.success?
          render json: presenter.present(permission_set)
        else
          undo_then_render(permission_set)
        end
      rescue Kragle::ResponseError, Kragle::ClientError, Kragle::ServerError, Faraday::ClientError, Faraday::ConnectionFailed, Faraday::TimeoutError
        undo_then_render(permission_set)
      end

      def undo_then_render(permission_set)
        Zendesk::SupportUsers::PermissionSet.new(permission_set).destroy(context: 'api_destroy')
        render_failure(status: :internal_server_error, title: 'Internal Server Error', message: 'Internal Server Error')
      end

      def handle_delete_permission_set(permission_set)
        if Zendesk::SupportUsers::PermissionSet.new(permission_set).destroy(context: 'api_destroy')
          default_delete_response
        else
          render json: Api::V2::ErrorsPresenter.new.present(permission_set), status: :unprocessable_entity
        end
      end
    end
  end
end
