class Api::V2::SatisfactionReasonsController < Api::V2::BaseController
  before_action :require_admin!
  before_action :require_account_has_csat_and_reason_codes_enabled!

  ## ### List Reasons for Satisfaction Rating
  ## `GET /api/v2/satisfaction_reasons.json`
  ##
  ## List all reasons for an account
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/satisfaction_reasons.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "reasons": [
  ##     {
  ##       "id":              35436,
  ##       "url":             "https://company.zendesk.com/api/v2/satisfaction_reasons/35436.json",
  ##       "reason_code":     1000,
  ##       "value":           "Agent did not respond quickly.",
  ##       "raw_value":       "{{dc.reason_code_1000}}",
  ##       "updated_at":      "2011-07-20T22:55:29Z",
  ##       "created_at":      "2011-07-20T22:55:29Z",
  ##     },
  ##     ...
  ##     {
  ##       "id":              120447,
  ##       "url":             "https://company.zendesk.com/api/v2/satisfaction_reasons/120447.json",
  ##       "reason_code":     1001,
  ##       "value":           "Issue is not resolved.",
  ##       "raw_value":       "{{dc.reason_code_1001}}",
  ##       "created_at":      "2012-02-01T04:31:29Z",
  ##       "updated_at":      "2012-02-02T10:32:59Z",
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  allow_parameters :index, locale: Parameters.string
  def index
    render json: presenter.present(satisfaction_reasons)
  end

  ## ### Show Reason for Satisfaction Rating
  ## `GET /api/v2/satisfaction_reasons/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/satisfaction_reasons/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "reason": {
  ##     "id":              35121,
  ##     "url":             "https://company.zendesk.com/api/v2/satisfaction_reason/35121.json",
  ##     "reason_code":     1000,
  ##     "value":           "Agent did not respond quickly.",
  ##     "raw_value":       "{{dc.reason_code_1000}}",
  ##     "updated_at":      "2011-07-20T22:55:29Z",
  ##     "created_at":      "2011-07-20T22:55:29Z",
  ##   }
  ## }
  ## ```
  resource_action :show

  private

  def satisfaction_reasons
    @reasons ||= Satisfaction::Reason.with_deleted { current_account.satisfaction_reasons }
  end

  def satisfaction_reason
    @reason ||= Satisfaction::Reason.with_deleted { current_account.satisfaction_reasons.find(params[:id]) }
  end

  def presenter
    presenter_options = {
      brand: current_brand || current_account.default_brand,
      url_builder: self
    }
    if locale = params.fetch(:locale, nil)
      presenter_options[:locale] = locale
    end
    @presenter ||= Api::V2::SatisfactionReasonPresenter.new(current_user, presenter_options)
  end

  def require_account_has_csat_and_reason_codes_enabled!
    deny_access unless current_account.has_customer_satisfaction_enabled? && current_account.has_csat_reason_code_enabled?
  end
end
