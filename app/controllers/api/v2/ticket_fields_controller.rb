require 'zendesk/tickets/ticket_field_manager'

module Api
  module V2
    class TicketFieldsController < Api::V2::BaseController
      before_action :ensure_manage_permissions, except: [:index, :show, :show_many],
        if: -> { current_account.has_permissions_ticket_fields_manage? }
      before_action :require_admin!, except: [:index, :show, :show_many],
        unless: -> { current_account.has_permissions_ticket_fields_manage? }
      before_action :set_creator_options, only: [:index, :show, :show_many], if: proc { params[:creator] }
      allow_gooddata_user only: [:index, :show]
      allow_bime_user only: [:index, :show]
      allow_subsystem_user :embeddable, only: [:index]
      allow_subsystem_user :answer_bot_flow_composer, only: [:index, :show]
      allow_subsystem_user :answer_bot_flow_director, only: [:index, :show]
      allow_cors :show_many
      allow_cors_from_zendesk_external_domains :index

      TICKET_FIELD_PARAMETER = {
        title: Parameters.string,
        raw_title: Parameters.string,
        description: Parameters.string,
        raw_description: Parameters.string,
        position: Parameters.integer,
        active: Parameters.boolean,
        required: Parameters.boolean,
        collapsed_for_agents: Parameters.boolean,
        regexp_for_validation: Parameters.string,
        title_in_portal: Parameters.string,
        raw_title_in_portal: Parameters.string,
        visible_in_portal: Parameters.boolean,
        editable_in_portal: Parameters.boolean,
        required_in_portal: Parameters.boolean,
        tag: Parameters.string,
        sub_type_id: Parameters.integer,
        custom_field_options: [
          {
            id: Parameters.bigid | Parameters.string,
            name: Parameters.string,
            raw_name: Parameters.string,
            value: Parameters.string,
            default: Parameters.boolean
          }
        ],
        agent_description: Parameters.string
      }.freeze

      TYPE_PARAMETER = { type: Parameters.enum(*Zendesk::Tickets::TicketFieldManager.type_map.keys) }.freeze
      CREATE_TICKET_FIELD_PARAMETER = TICKET_FIELD_PARAMETER.merge(TYPE_PARAMETER).freeze

      ## ### List Ticket Fields
      ## `GET /api/v2/ticket_fields.json`
      ##
      ## Returns a list of all system and custom ticket fields in your account.
      ##
      ## The results are not paginated. Every field is returned in the response.
      ##
      ## Fields are returned in the order specified by the position and id.
      ##
      ## For accounts without access to multiple ticket forms,
      ## positions can be changed using the
      ## [Update Ticket Field](./ticket_fields#update-ticket-field) endpoint
      ## or the Ticket Forms page in Zendesk Support
      ## (**Admin** > **Manage** > **Ticket Forms**).
      ## The Ticket Forms page shows the fields for the account.
      ## The order of the fields is used in the different products to show
      ## the field values in the tickets.
      ##
      ## For accounts with access to multiple ticket forms, positions can only be
      ## changed using the [Update Ticket Field](./ticket_fields#update-ticket-field)
      ## endpoint because products use the order defined on each form to show
      ## the field values instead of the general position of the ticket field
      ## in the account.
      ##
      ## Consider caching this resource to use with the [Tickets](./tickets#json-format) API.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_fields": [
      ##     {
      ##       "id":                    34,
      ##       "url":                   "https://company.zendesk.com/api/v2/ticket_fields/34.json",
      ##       "type":                  "subject",
      ##       "title":                 "Subject",
      ##       "raw_title":             "{{dc.my_title}}",
      ##       "description":           "This is the subject field of a ticket",
      ##       "raw_description":       "This is the subject field of a ticket",
      ##       "position":              21,
      ##       "active":                true,
      ##       "required":              true,
      ##       "collapsed_for_agents":  false,
      ##       "regexp_for_validation": null,
      ##       "title_in_portal":       "Subject",
      ##       "raw_title_in_portal":   "{{dc.my_title_in_portal}}",
      ##       "visible_in_portal":     true,
      ##       "editable_in_portal":    true,
      ##       "required_in_portal":    true,
      ##       "tag":                   null,
      ##       "agent_description":     "Agent only description",
      ##       "created_at":            "2009-07-20T22:55:29Z",
      ##       "updated_at":            "2011-05-05T10:38:52Z"
      ##     }
      ##   ]
      ## }
      ## ```
      ##
      allow_parameters :index, creator: Parameters.boolean, only_system_fields: Parameters.boolean
      def index
        if stale_collection?(ticket_fields)
          render json: presenter.present(ticket_fields)
        end
      end

      # h3 Show Many Ticket Fields
      #
      # `GET /api/v2/ticket_fields/show_many.json?ids={ids}`
      #
      # Accepts a comma-separated list of up to 100 ticket field ids.
      #
      # h4 Allowed For:
      #
      #  * Agents
      #
      # h4 Using curl:
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/show_many.json?ids=1,2,3 \
      #   -v -u {email_address}:{password}
      # ```
      #
      # h4 Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # {
      #  "ticket_fields": [
      #    {
      #      "id":    1,
      #      title": "Subject",
      #      ...
      #    },
      #    {
      #      "id":    2,
      #      "title": "Description",
      #      ...
      #    },
      #    ...
      #   ]
      #  ]
      # }
      # ```
      #
      allow_parameters :show_many, ids: Parameters.string, creator: Parameters.boolean
      def show_many
        render json: presenter.present(many_ticket_fields)
      end

      ## ### Show Ticket Field
      ## `GET /api/v2/ticket_fields/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_field": {
      ##     "id":                    34,
      ##     "url":                   "https://company.zendesk.com/api/v2/ticket_fields/34.json",
      ##     "type":                  "subject",
      ##     "title":                 "Subject",
      ##     "raw_title":             "{{dc.my_subject}}",
      ##     "description":           "This is the subject field of a ticket",
      ##     "raw_description":       "This is the subject field of a ticket",
      ##     "position":              21,
      ##     "active":                true,
      ##     "required":              true,
      ##     "collapsed_for_agents":  false,
      ##     "regexp_for_validation": null,
      ##     "title_in_portal":       "Subject",
      ##     "raw_title_in_portal":   "{{dc.my_wp_subject}}",
      ##     "visible_in_portal":     true,
      ##     "editable_in_portal":    true,
      ##     "required_in_portal":    true,
      ##     "tag":                   null,
      ##     "agent_description":     "Agent only description",
      ##     "created_at":            "2009-07-20T22:55:29Z",
      ##     "updated_at":            "2011-05-05T10:38:52Z"
      ##   }
      ## }
      ## ```
      allow_parameters :show, id: Parameters.bigid, creator: Parameters.boolean
      def show
        render json: presenter.present(ticket_field)
      end

      ## ### Create Ticket Field
      ## `POST /api/v2/ticket_fields.json`
      ##
      ##  Creates any of the following custom field types:
      ##
      ##  * `text` (default when no "type" is specified)
      ##  * `textarea`
      ##  * `checkbox`
      ##  * `date`
      ##  * `integer`
      ##  * `decimal`
      ##  * `regexp`
      ##  * `partialcreditcard`
      ##  * `multiselect` (multi-select dropdown menu)
      ##  * `tagger` (single-select dropdown menu)
      ##
      ## See [About custom field types](https://support.zendesk.com/hc/en-us/articles/203661866) in the Zendesk Support Help Center.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Field limits
      ##
      ##  * 400 ticket fields per account if your account doesn't have ticket forms
      ##  * 400 ticket fields per ticket form if your account has ticket forms
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields.json \
      ##   -d '{"ticket_field": {"type": "text", "title": "Age"}}' \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ##
      ## {
      ##   "ticket_field": {
      ##     "id":                    89,
      ##     "url":                   "https://company.zendesk.com/api/v2/ticket_fields/89.json",
      ##     "type":                  "text",
      ##     "title":                 "Age",
      ##     "raw_title":             "Age",
      ##     "description":           "Age",
      ##     "raw_description":       "Age",
      ##     "position":              9999,
      ##     "active":                true,
      ##     "required":              true,
      ##     "collapsed_for_agents":  false,
      ##     "regexp_for_validation": null,
      ##     "title_in_portal":       "Age",
      ##     "raw_title_in_portal":   "Age",
      ##     "visible_in_portal":     false,
      ##     "editable_in_portal":    false,
      ##     "required_in_portal":    false,
      ##     "tag":                   null,
      ##     "agent_description":     "Agent only description"
      ##     "created_at":            "2012-04-02T22:55:29Z",
      ##     "updated_at":            "2012-04-02T22:55:29Z"
      ##   }
      ## }
      ## ```
      allow_parameters :create, ticket_field: CREATE_TICKET_FIELD_PARAMETER
      def create
        new_ticket_field.save!
        render json: presenter.present(new_ticket_field), status: :created, location: presenter.url(new_ticket_field)
      end

      ## ### Update Ticket Field
      ## `PUT /api/v2/ticket_fields/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{id}.json \
      ##   -d '{ "ticket_field": { "title": "Your age" }}' \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_field": {
      ##     "id":                    89,
      ##     "url":                   "https://company.zendesk.com/api/v2/ticket_fields/89.json",
      ##     "type":                  "text",
      ##     "title":                 "Your age",
      ##     "raw_title":             "Your age",
      ##     "description":           "Your age",
      ##     "raw_description":       "Your age",
      ##     "position":              9999,
      ##     "active":                true,
      ##     "required":              true,
      ##     "collapsed_for_agents":  false,
      ##     "regexp_for_validation": null,
      ##     "title_in_portal":       "Your age",
      ##     "raw_title_in_portal":   "Your age",
      ##     "visible_in_portal":     false,
      ##     "editable_in_portal":    false,
      ##     "required_in_portal":    false,
      ##     "tag":                   null,
      ##     "agent_description":     "Agent only description"
      ##     "created_at":            "2012-04-02T22:55:29Z",
      ##     "updated_at":            "2012-04-02T23:11:23Z"
      ##   }
      ## }
      ## ```
      ##
      ## ### Updating drop-down field options
      ##
      ## You can also use the update endpoint to add, update, or remove options in
      ## a drop-down custom field. Updating field options for multi-select fields
      ## works exactly the same as drop-down field options.
      ##
      ## **Important**: Unless you want to remove some options, you must specify
      ##  all existing options in any update request. Omitting an option removes
      ## it from the drop-down field, which removes its values from any tickets
      ## or macros.
      ##
      ## Use the `custom_field_options` attribute to update the options. The
      ## attribute consists of an array of option objects, with each object
      ## consisting of a `name` and `value` property. The properties correspond
      ## to the "Title" and "Tag" text boxes in the admin interface. Example request body:
      ##
      ## ```js
      ## {
      ##   "custom_field_options": [
      ##     {"name": "Apple Pie", "value": "apple"},
      ##     {"name": "Pecan Pie", "value": "pecan"}
      ##   ]
      ## }
      ## ```
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{id}.json \
      ##   -d '{"ticket_field": {"custom_field_options": [{"name": "Apple Pie", "value": "apple"}, {"name": "Pecan Pie", "value": "pecan"}]}}' \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "ticket_field": {
      ##     "id":21938362,
      ##     "type":"tagger",
      ##     "title":"Pies",
      ##     ...
      ##     "custom_field_options": [
      ##       {
      ##         "id":21029772,
      ##         "name":"Apple Pie",
      ##         "raw_name":"Apple Pie",
      ##         "value":"apple",
      ##         "default":false
      ##       },
      ##       ...
      ##     ]
      ##   }
      ## }
      ## ```
      ##
      allow_parameters :update, id: Parameters.bigid, ticket_field: TICKET_FIELD_PARAMETER
      def update
        ticket_field_manager.update(ticket_field, params[:ticket_field])
        ticket_field.save!
        render json: presenter.present(ticket_field)
      end

      ## ### Delete Ticket Field
      ## `DELETE /api/v2/ticket_fields/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/ticket_fields/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      ##
      resource_action :destroy

      protected

      def ensure_manage_permissions
        deny_access unless current_user.can?(:manage, TicketField)
      end

      def presenter
        @presenter ||= Api::V2::TicketFieldPresenter.new(current_user, presenter_options)
      end

      def ticket_field_options
        opts = {}

        if gooddata_request?
          opts[:include_voice_insights] = true
        end

        opts
      end

      def ticket_fields
        @ticket_fields ||= begin
          if params[:only_system_fields]
            current_account.ticket_fields(ticket_field_options).system_fields
          else
            current_account.ticket_fields(ticket_field_options)
          end
        end
      end

      def many_ticket_fields
        ticket_fields.where(id: many_ids)
      end

      def new_ticket_field
        @new_ticket_field ||= ticket_field_manager.build(params[:ticket_field])
      end

      def ticket_field
        @ticket_field ||= current_account.ticket_fields(ticket_field_options).find(params[:id]).tap do |field|
          field.current_user = current_user
        end
      end

      def ticket_field_manager
        Zendesk::Tickets::TicketFieldManager.new(current_account)
      end

      def presenter_options
        @presenter_options ||= { url_builder: self, includes: includes }
      end

      def set_creator_options
        return unless current_account.has_ticket_field_created_by? && current_user.is_admin?
        presenter_options.merge!(creator: true)
      end
    end
  end
end
