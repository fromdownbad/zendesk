class Api::V2::Billing::ServiceConnectionsController < Api::V2::BaseController
  before_action :require_admin!
  before_action :require_nonfraudulent_account!

  # NOTE We allow any param for this endpoint, we simply treat it as
  # pass-through values that we could append as query-params when computing
  # the value returned by #service_url.
  allow_parameters :create, :skip
  def create
    render status: :created, json: presenter.present(service_connection)
  end

  private

  def zuora_subscription
    current_account.subscription.zuora_subscription
  end

  def service_connection
    Billing::ServiceConnection.build(
      current_account,
      current_user,
      base_url: request.base_url,
      params:   request.request_parameters.merge(locale_param)
    )
  end

  def presenter
    Api::V2::Billing::ServiceConnectionPresenter.new(
      current_user,
      url_builder: self,
      includes:    includes
    )
  end

  def require_nonfraudulent_account!
    head(:forbidden) if current_account.abusive?
  end

  def locale_param
    { locale: locale }
  end

  def locale
    current_user.locale
  rescue
    current_account.translation_locale.try(:locale)
  end
end
