class Api::V2::Billing::NotificationsController < Api::V2::Internal::BaseController
  allow_subsystem_user :billing

  before_action :require_valid_issuer

  # NOTE: Whenever a sync occurs in Billing (usually initiated by Zuora) two
  # things happen:
  #
  # - Zuora records are sync'd to Pravda
  # - Billing sends out a notification
  #
  # The notification from Billing targets the create action of this controller
  # which calls this method.
  allow_parameters :create, token: Parameters.string
  def create
    ZBC::Zuora::Jobs::AccountSyncJob.enqueue(payload.billing_id)
    head :created
  end

  private

  # Expects the Billing Service's token payload to be a Hash with at least the
  # following keys:
  #
  #  * master_account_id: The master-account-id of the account that is the
  #    subject of the notification received from Billing. This can be used to
  #    fetch data from the Account Service when generating a sync.
  #
  #  * billing_id: This the record-id for the account's billing record's source
  #    of truth -- which is currently Zuora -- so the value set to this key is
  #    the account's zuora-account-id.
  #
  def payload
    @payload ||= OpenStruct.new(claims[:content])
  end

  def claims
    @claims ||= Billing::JWT.decrypt(token)
  end

  def token
    params.require(:token)
  end

  def require_valid_issuer
    return if claims[:iss] == 'billing'
    head :forbidden
  rescue Billing::JWT::Decryptor::Error
    head :unauthorized
  end
end
