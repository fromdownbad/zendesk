class Api::V2::UploadsController < Api::V2::BaseController
  allow_anonymous_users :create
  allow_subsystem_user :chat_widget_mediator, only: [:create]
  allow_cors :create

  skip_before_action :require_agent!
  skip_before_action :enforce_request_format, only: :create

  require_that_user_can! :create, Attachment, only: :create, unless: :anonymous_uploads_allowed?
  before_action :ensure_attachment_filename, only: :create
  before_action :ensure_valid_attachment, only: :create

  ## ### Upload Files
  ## `POST /api/v2/uploads.json`
  ##
  ## Adding multiple attachments to the same upload is handled by splitting requests and
  ## passing the API token received from the first request to each subsequent request. The
  ## token is valid for 3 days.
  ##
  ## Note: Even if [private attachments](https://support.zendesk.com/hc/en-us/articles/204265396) are enabled in the Zendesk Support
  ## instance, uploaded files are visible to any authenticated user at the `content_URL`
  ## specified in the [JSON response](#json-format) until the upload token is consumed.
  ## Once an attachment is associated with a ticket or post, visibility is
  ## restricted to users with access to the ticket or post with the attachment.
  ##
  ## #### Allowed For
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl "https://{subdomain}.zendesk.com/api/v2/uploads.json?filename=myfile.dat&token={optional_token}" \
  ##   -v -u {email_address}:{password} \
  ##   -H "Content-Type: application/binary" \
  ##   --data-binary @file.dat -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://company.zendesk.com/api/v2/attachments/498483.json
  ##
  ## {
  ##   "upload": {
  ##     "token": "6bk3gql82em5nmf",
  ##     "attachment": {
  ##       "id":           498483,
  ##       "name":         "crash.log",
  ##       "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##       "content_type": "text/plain",
  ##       "size":         2532,
  ##       "thumbnails":   []
  ##     },
  ##     "attachments": [
  ##       {
  ##         "id":           498483,
  ##         "name":         "crash.log",
  ##         "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##         "content_type": "text/plain",
  ##         "size":         2532,
  ##         "thumbnails":   []
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  allow_parameters :create,
    uploaded_data: Parameters.anything,
    token: Parameters.string,
    filename: Parameters.string,
    inline: Parameters.boolean,
    ticket_id: Parameters.bigid,
    via_id: Parameters.bigid
  def create
    if current_user.is_anonymous_user? && !current_account.is_open?
      render json: {}, status: :unauthorized
      return
    end

    if new_upload.save!
      # CRUFT: see https://zendesk.atlassian.net/browse/API-267
      p = presenter(attachment: new_upload.attachment)
      json = p.present(new_upload.token)
      options = { status: :created, location: attachment_location }
    else
      # When trying to save! the attachment record MiniMagick blows trying to look for a tempfile that couldnt read
      # Maybe related to https://github.com/minimagick/minimagick/issues/306
      # Anyway, we should handle this and response with unprocessable entity instead
      json = attachment_unprocessable_error_json
      options = { status: :unprocessable_entity }
    end
    render_with_correct_mime(options, json)
  end

  ## ### Delete Upload
  ## `DELETE /api/v2/uploads/{token}.json`
  ##
  ## #### Allowed For
  ##
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/uploads/{token}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.string
  def destroy
    upload.destroy
    default_delete_response
  end

  private

  def new_upload
    @new_upload ||= Upload::TokenHandler.build(current_account, current_user, params, file_handle)
  end

  def upload
    @upload ||= current_account.upload_tokens.find_by_value!(params[:id])
  end

  # Accepts either a raw POST body or a form multipart with an uploaded_data field.
  # Returns an IO object that responds to :content_type, :original_filename and :size
  def file_handle
    @file_handle ||= if params[:uploaded_data].present?
      # This is either a Tempfile or StringIO
      params[:uploaded_data]
    else
      # API raw body upload
      RestUpload.new(request.raw_post, params[:filename], request.content_type)
    end
  end

  def presenter(options = {})
    @presenter ||= Api::V2::UploadPresenter.new(current_user, options.merge(url_builder: self))
  end

  def ensure_attachment_filename
    if !file_handle.respond_to?(:original_filename)
      errors = attachment_unprocessable_error_json
    elsif file_handle.original_filename.try(:length).try(:>=, 250)
      errors = {
        error:       'AttachmentFilenameTooLong',
        description: I18n.t('txt.errors.attachments.filename_invalid')
      }
    end

    return unless errors

    options = { status: :bad_request }
    render_with_correct_mime(options, errors)
  end

  def ensure_valid_attachment
    if file_handle.size > current_account.max_attachment_size
      render_with_correct_mime(
        { status: :payload_too_large },
        error:       'AttachmentTooLarge',
        description: I18n.t('txt.errors.attachments.size_invalid', limit: current_account.max_attachment_megabytes)
      )
    elsif file_handle.blank?
      render_with_correct_mime(
        { status: :unprocessable_entity },
        attachment_unprocessable_error_json
      )
    end
  end

  def attachment_unprocessable_error_json
    {
      error: 'AttachmentUnprocessable',
      description: I18n.t('txt.errors.attachments.file_unprocessable')
    }
  end

  # Respond with plain text if Accept: application/json not specified for benefit of Lotus under IE9
  # Don't change this unless you specifically test Lotus uploads with IE9 and IE10!
  def render_with_correct_mime(options, json)
    if request.accept.to_s.include?(Mime[:json])
      render options.merge(json: json)
    else
      render options.merge(text: json.to_json, content_type: Mime[:text])
    end
  end

  def attachment_location
    presenter.attachment_presenter.url(new_upload.attachment)
  end

  def current_user
    if from_web_widget?
      current_account.anonymous_user
    else
      super
    end
  end

  def from_web_widget?
    params[:via_id].to_i == ViaType.WEB_WIDGET
  end

  # This is to allow anonymous requests to bypass our CSRF token check for the create method.
  def verified_request?
    (action_name == 'create' && current_user.is_anonymous_user?) || super
  end

  def anonymous_uploads_allowed?
    return false if current_account.has_sse_prevent_anonymous_uploads?

    current_user.is_anonymous_user? && current_account.is_open? && current_account.is_attaching_enabled?
  end
end
