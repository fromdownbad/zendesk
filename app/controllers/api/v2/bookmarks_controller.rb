class Api::V2::BookmarksController < Api::V2::BaseController
  require_that_user_can! :view, :ticket, except: [:index, :destroy]

  ## ### List Bookmarks
  ## `GET /api/v2/bookmarks.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/bookmarks.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "bookmarks": [
  ##     {
  ##       "id":              35436,
  ##       "created_at":      "2012-03-20T22:55:29Z",
  ##       "ticket":          { ...}
  ##     },
  ##     {
  ##       "id":              38416,
  ##       "created_at":      "2012-03-21T12:22:18Z",
  ##       "ticket":          { ...}
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(bookmarks)
  end

  ## ### Create Bookmark
  ## `POST /api/v2/bookmarks.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/bookmarks.json \
  ##   -H "Content-Type: application/json" -X POST -d '{"bookmark": {"ticket_id": 123}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/bookmarks/{id}.json
  ##
  ## {
  ##   "bookmark": {
  ##     "id":              44215,
  ##     "created_at":      "2012-03-21T04:31:29Z",
  ##     "ticket":          { ... }
  ##   }
  ## }
  ## ```
  ##
  ## If the bookmark already exists with the specified ticket_id the response will be
  ## ``` http
  ## Status: 200 OK
  ## ```

  allow_parameters :create, bookmark: {ticket_id: Parameters.bigid}
  def create
    if current_user.bookmarks.exists?(ticket_id: ticket.id)
      head :ok
    else
      new_bookmark.save!
      render json: presenter.present(new_bookmark), status: :created, location: presenter.url(new_bookmark)
    end
  end

  ## ### Delete Bookmark
  ## `DELETE /api/v2/bookmarks/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents (own bookmarks only)
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/bookmarks/{id}.json \
  ##   -X DELETE -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  resource_action :destroy

  protected

  def presenter
    @presenter ||= Api::V2::BookmarkPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:bookmark][:ticket_id])
  end

  def bookmark
    @bookmark ||= current_user.bookmarks.find(params[:id])
  end

  def new_bookmark
    @new_bookmark ||= Ticket::Bookmark.new(user: current_user, ticket: ticket)
  end

  def bookmarks
    @bookmarks ||= paginate(current_user.visible_bookmarks)
  end
end
