module Api
  module V2
    class AccountCreationTokenController < Api::V2::BaseController
      before_action :require_admin!, only: [:create]
      before_action :restrict_zendeskaccounts, only: [:create]

      # ### Create Account Creation Token
      # `POST /api/v2/account_creation_token.json`
      #
      # #### Allowed for
      #
      #  * Admins on the zendeskaccounts(subdomain) account
      #
      # #### Using curl
      #
      # ```bash
      # curl https://zendeskaccounts.zendesk.com/api/v2/account_creation_token \
      #   -H "Content-Type: application/json" -X POST \
      #   -d '{"token_user_name": "token_user_name"}' \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # {
      #   "oauth_token": {
      #     "id":        10041,
      #     "user_id":   10046,
      #     "token":     "705f8ef0f792932b1e86ab3f3e6e5b2c4143074fd4d71dec3fd096f311e3b45d"
      #   }
      # }
      #
      allow_parameters :create, token_user_name: Parameters.string
      def create
        token = generate_or_return_token_for_user
        render json: presenter.present(token)
      end

      private

      def generate_or_return_token_for_user
        token_count = current_account.tokens.where(user_id: account_creation_user.id, scopes: "accounts:write").count
        case token_count
        when 0
          generate_token_for_user
        when 1
          current_account.tokens.where(user_id: account_creation_user.id, scopes: "accounts:write").first
        else
          Rails.logger.warn("multiple account creation tokens present; returning latest token")
          current_account.tokens.where(user_id: account_creation_user.id, scopes: "accounts:write").order("created_at").last
        end
      end

      def generate_token_for_user
        client = current_account.clients.where(identifier: "account_creation").first
        client.tokens.create!(user_id: account_creation_user.id, scopes: "accounts:write", account_id: current_account.id)
      end

      def presenter
        @presenter ||= Api::V2::AccountCreationTokenPresenter.new(current_user, url_builder: self)
      end

      def token_user_name
        params.require(:token_user_name)
      end

      def token_user_email
        "accountcreation+#{token_user_name}@zendesk.com"
      end

      def account_creation_user
        @account_creation_user ||= existing_user || new_user
      end

      def existing_user
        current_account.find_user_by_email(token_user_email)
      end

      def new_user
        new_user_params = { user: { name: token_user_name, email: token_user_email, role: "end-user"} }
        new_user = Zendesk::Users::Initializer.new(current_account, current_user, new_user_params).apply(current_account.users.new)
        new_user.save!
        new_user
      end

      def restrict_zendeskaccounts
        head :not_found unless current_account.subdomain == "zendeskaccounts"
      end
    end
  end
end
