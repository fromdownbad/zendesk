class Api::V2::IncidentsController < Api::V2::BaseController
  include Zendesk::Tickets::ControllerSupport
  require_that_user_can! :view, :problem, only: [:index, :count]

  ## ### Listing Ticket Incidents
  ## `GET /api/v2/tickets/{id}/incidents.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/12345/incidents.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":          33,
  ##       "subject":     "My printer is on fire",
  ##       "description": "The fire is very colorful.",
  ##       "status":      "open",
  ##       ...
  ##     },
  ##     {
  ##       "id":          34,
  ##       "subject":     "The printer is on fire over here too",
  ##       "description": "The fire is very colorful as well!",
  ##       "status":      "pending",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :index, ticket_id: Parameters.bigid
  allow_cursor_pagination_parameters :index
  allow_cursor_pagination_v2_parameters :index

  def index
    render json: presenter.present(incidents)
  end

  allow_parameters :count, ticket_id: Parameters.bigid
  def count
    render json: { count: record_counter.present, links: { url: request.original_url } }
  end

  protected

  def presenter
    @presenter ||= begin
      options = {
        url_builder: self,
        includes: includes
      }

      options[:with_cursor_pagination] = with_cursor_pagination?

      Api::V2::Tickets::TicketPresenter.new(current_user, options)
    end
  end

  def incidents
    @incidents ||= if use_cursor_pagination_v2?
      paginate_with_cursor(sort_tickets_for_cursor(scope))
    elsif with_cursor_pagination?
      scope.paginate_with_cursor(
        cursor: params[:cursor],
        limit: params[:limit],
        order: params[:sort_order]
      )
    else
      paginate(scope, method: "archived_paginate")
    end
  end

  def sort_map
    return unless (map = super)
    @sort_map ||= map.merge("locale" => "FIELD(locale_id,#{current_account.available_languages.map(&:id).join(',')})")
  end

  def problem
    current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  def record_counter
    @record_counter ||= Zendesk::RecordCounter::Incidents.new(account: current_account, options: {current_user_id: current_user.id, problem_nice_id: problem.nice_id})
  end

  def scope
    @scope ||= if current_account.has_cursor_pagination_v2_ticket_incidents_endpoint?
      Zendesk::Incidents::Finder.new(current_account, current_user, problem_nice_id: problem.nice_id).incidents
    else
      problem.incidents.for_user(current_user)
    end
  end

  def use_cursor_pagination_v2?
    # Remove this conditional after we rollout 'cursor_pagination_v2_ticket_incidents_endpoint'.
    #
    # If we receive a '?page[:size]=2' request before we enable the
    # feature, we want to show the same error message we were showing
    # before adding the new parameter types.
    if !current_account.has_cursor_pagination_v2_ticket_incidents_endpoint? &&
          HashParam.ish?(params[:page])
      raise StrongerParameters::InvalidParameter.new(
        StrongerParameters::IntegerConstraint.new.value(params[:page]),
        'page'
      )
    end

    with_cursor_pagination_v2? && current_account.has_cursor_pagination_v2_ticket_incidents_endpoint?
  end

  SORT_LOOKUP = Ticket::SORTABLE_FIELDS
end
