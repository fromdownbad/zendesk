class Api::V2::Account::AddonsController < Api::V2::BaseController
  before_action :require_agent!, only: [:show]

  ## ### Show Addons
  ## `GET /api/v2/account/addons.json`
  ##
  ## Returns a list of all possible addons for the account and whether they are
  ## purchased or boosted.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/addons.json \
  ##   -v -u {email_address}:{password}
  ##
  ## #### Example Response
  ## See addons_presenter.rb
  ##

  allow_parameters :show, {}
  def show
    active_addons.each do |a|
      addon = all_addons.find { |b| b[:name] == a.name.to_sym }
      next unless addon
      addon[:boost_expires_at]   = a.boost_expires_at
      addon[:zuora_rate_plan_id] = a.zuora_rate_plan_id
    end

    render json: addons_presenter.collection_presenter.present(all_addons)
  end

  private

  def addons_presenter
    @addons_presenter ||= Api::V2::Account::AddonsPresenter.new(
      current_user,
      url_builder: self
    )
  end

  def active_addons
    @active_addons ||= current_account.subscription_feature_addons.non_secondary_subscriptions
  end

  def all_addons
    @all_addons ||= catalog.addons_for_plan(plan_type).map do |f|
      {
        name:               f.name,
        boost_expires_at:   nil,
        zuora_rate_plan_id: nil
      }
    end
  end

  def catalog
    @catalog ||= Zendesk::Features::CatalogService.new(current_account).
      current_catalog
  end

  def plan_type
    # Sales and Support need to know addons purchased, regardless
    # of whether an account boost renders those addons (temporarily) functionless
    @plan_type ||= current_account.subscription.plan_type
  end
end
