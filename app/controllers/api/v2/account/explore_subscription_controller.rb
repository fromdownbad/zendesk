module Api::V2::Account
  class ExploreSubscriptionController < Api::V2::BaseController
    before_action :require_admin!,
      only: [:destroy]

    before_action :require_verified_owner!,
      only: [:destroy]

    before_action :require_active_explore_subscription!,
      only: [:destroy]

    ## ### Cancel an Explore Subscription
    ##
    ## `DELETE /api/v2/account/explore_subscription.json`
    ##
    ## Cancels the Explore subscription associated with the current account. This
    ## will delete all the Explore related records (subscription, agents)
    ##
    ## #### Allowed For:
    ##
    ##  * Owner
    ##  * Admins
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/account/explore_subscription.json \
    ## -X DELETE \
    ## -H "Content-Type: application/json" \
    ## -v -u {email_address}:{password}
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 Success
    ## ```

    allow_parameters :destroy, {}
    def destroy
      ZBC::Explore::SubscriptionDeactivator.deactivate!(explore_subscription)
      default_delete_response
    end

    private

    def require_active_explore_subscription!
      return if explore_subscription.try(:subscribed?)

      render status: :unprocessable_entity,
             json: { domain: 'Explore', error: 'ActiveExploreSubscriptionNotFound' }
    end

    def require_verified_owner!
      return if current_user.is_verified? && current_user.is_account_owner?

      render status: :unauthorized,
             json: { domain: 'Explore', error: 'VerifiedAccountOwnerRequired' }
    end

    def explore_subscription
      # memoize since this will hit the account service
      @explore_subscription ||= current_account.explore_subscription
    end
  end
end
