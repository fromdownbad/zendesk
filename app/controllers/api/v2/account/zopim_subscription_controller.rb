module Api::V2::Account
  class ZopimSubscriptionController < Api::V2::BaseController
    before_action :require_admin!,
      only: [:destroy, :deactivate]

    before_action :require_verified_user!,
      only: [:authorize, :destroy, :deactivate]

    before_action :require_zopim_subscription!,
      only: [:show, :authorize, :destroy, :deactivate, :update]

    before_action :require_cancelled_or_no_chat_account!,
      only: [:create, :activate]

    before_action :require_active_chat_account!,
      only: [:destroy, :update]

    before_action :require_serviceable_chat_account!,
      only: [:show, :authorize, :destroy, :deactivate, :update]

    before_action :require_serviceable_chat_identity!,
      only: [:authorize, :destroy, :deactivate]

    before_action :require_chat_admin!,
      only: [:destroy, :deactivate]

    allow_only_subsystem_user :zendesk, only: [:create, :activate]
    allow_zopim_user only: [:show, :update]

    ## ### Activate a Zopim Subscription
    ## `POST /api/v2/account/zopim_subscription.json`
    ##
    ## Provision and associate a Zopim subscription to the current account.
    ## If the current account already has a Zopim subscription but is in a
    ## cancelled status, it will attempt to reactivate the account.
    ##
    ## The owner of the Zendesk account is assigned a chat-identity that is also
    ## designated as the Zopim subscription owner and administrator.
    ##
    ## The user that activates the Zopim subscription (if not the owner) is
    ## also assigned a chat-identity.
    ##
    ## A NOT MODIFIED status is returned if the account is already active. For
    ## all possible errors, an UNPROCESSABLE ENTITY status is returned, and the
    ## response body will contain the reason why the request failed.
    ##
    ## #### Allowed For:
    ##
    ##  * Owner
    ##  * Admins
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/account/zopim_subscription.json \
    ## -X POST \
    ## -H "Content-Type: application/json" \
    ## -v -u {email_address}:{password}
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "zopim_subscription": {
    ##     "zopim_account_id": 6375309,
    ##     "status": "active",
    ##     ....
    ##   }
    ## }
    ## ```
    allow_parameters :create, {}
    allow_parameters :activate, {}
    def create
      zopim_subscription = current_account.create_zopim_subscription!
      if zopim_subscription
        render json: presenter.present(zopim_subscription)
      else
        Rails.logger.warn("Zopim subscription creation failed")
        render status: :unprocessable_entity,
               json: { domain: 'Zopim', error: 'ZopimSubscriptionCreationFailed' }
      end
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: e.message, fingerprint: '994a96d34966b9ec44bcd3721d40a300b7a685fb')
      raise e
    end
    alias_method :activate, :create

    ## ### Cancel a Zopim Subscription
    ##
    ## `DELETE /api/v2/account/zopim_subscription.json`
    ##
    ## Cancels the Zopim subscription associated with the current account. This
    ## will delete all the Zopim related records (subscription, integration,
    ## agents)
    ##
    ## #### Allowed For:
    ##
    ##  * Owner
    ##  * Admins (must also be designated as an admin in the Zopim subscription)
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/account/zopim_subscription.json \
    ## -X DELETE \
    ## -H "Content-Type: application/json" \
    ## -v -u {email_address}:{password}
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 204 No Content
    ## ```
    ## WARNING:
    ## FOR TRIALS AND NON-ZUORA MANAGED ZOPIM ACCOUNTS

    allow_parameters :destroy, {}
    allow_parameters :deactivate, {}
    def destroy
      current_zopim_subscription.destroy
      default_delete_response
    end
    alias_method :deactivate, :destroy

    ## ### Show Zopim Subscription
    ## `GET /api/v2/account/zopim_subscription.json`
    ##
    ## Returns a description of the Zopim subscription associated with the
    ## current account.
    ##
    ## #### Allowed For:
    ##
    ##  * Owner
    ##  * Admins
    ##  * Agents
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/account/zopim_subscription.json \
    ## -H "Content-Type: application/json" \
    ## -v -u {email_address}:{password}
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "zopim_subscription": {
    ##     "zopim_account_id": 6375309,
    ##     "status": "active",
    ##     ....
    ##   }
    ## }
    ## ```
    allow_parameters :show, {}
    def show
      render json: presenter.present(current_zopim_subscription)
    end

    ## ### Requests for SSO authorization from Zopim.
    ## `POST /api/v2/account/zopim_subscription/authorize.json`
    ##
    ## Sends a request for SSO authorization from Zopim on behalf of the current
    ## user (see: http://reseller-docs.zopim.com/api/single-sign-on).
    ##
    ## If successful it sends the authorization details back; the response body
    ## will contain the SSO token (valid for 60 seconds) and a redirect URI that
    ## will log the user into Zopim.
    ##
    ## An UNAUTHORIZED status is sent back if the SSO authorization fails. For
    ## all possible errors, an UNPROCESSABLE ENTITY status is returned, and the
    ## response body will contain the reason why the request failed.
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/account/zopim_subscription/authorize.json \
    ## -X POST \
    ## -H "Content-Type: application/json" \
    ## -v -u {email_address}:{password}
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "authorization": {
    ##     "token": "reseller_sso_token",
    ##     "redirect_uri": "https://reseller.zopim.com/api/sso/authorize?token=reseller_sso_token"
    ##   }
    ## }
    ## ```
    allow_parameters :authorize, {}
    require_oauth_scopes :authorize, scopes: [:write], arturo: true
    def authorize
      payload       = { email: current_zopim_identity.email }
      authorization = ::Zopim::Reseller.client.
        request_authorization(data: payload)
      head(:unauthorized) && return if authorization.blank?
      render json: { authorization: authorization }
    end

    ## ### Update a Zopim Subscription's zopim account id
    ## `PUT /api/v2/account/zopim_subscription.json`
    ##
    ##  Special case that emerged while doing Phase 2 to Phase 3 migrations.
    ##  We need to update the zopim_account_id in the zopim_subscription record
    ##  to prevent bad data from getting re-inserted into Zuora.
    ##
    ##  This action only supports only field, the zopim_account_id
    ##
    ## #### Allowed For:
    ##
    ##  * Zopim system user
    ##    See lib/zendesk/auth/system_bearer_users.rb
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "zopim_subscription": {
    ##     "zopim_account_id": -12345678,
    ##     "status": "active",
    ##     ....
    ##   }
    ## }
    ## ```
    allow_parameters :update, zopim_subscription: {
      zopim_account_id: Parameters.integer
    }
    def update
      zopim_account_id = params[:zopim_subscription]['zopim_account_id'].to_i.abs
      current_zopim_subscription.zopim_account_id = -zopim_account_id
      current_zopim_subscription.save!
      render json: presenter.present(current_zopim_subscription)
    end

    private

    def presenter
      ZopimSubscriptionPresenter.new(
        current_user, url_builder: self, includes: includes
      )
    end

    def require_zopim_subscription!
      return if current_zopim_subscription.present?
      render status: :not_found,
             json: { domain: 'Zopim', error: 'ChatAccountNotFound' }
    end

    def require_serviceable_chat_account!
      return if current_zopim_subscription.try(:is_serviceable?)
      render status: :unprocessable_entity,
             json: { domain: 'Zopim', error: 'ServiceableChatAccountRequired' }
    end

    def require_cancelled_or_no_chat_account!
      return if current_zopim_subscription.blank?
      return if current_zopim_subscription.present? &&
        current_zopim_subscription.is_cancelled?
      head :not_modified
    end

    def require_active_chat_account!
      return if current_zopim_subscription.try(:is_active?)
      head :not_modified
    end

    def require_serviceable_chat_identity!
      return if current_zopim_identity.try(:is_serviceable?)
      render status: :unprocessable_entity,
             json: { domain: 'Zopim', error: 'ServiceableChatIdentityRequired' }
    end

    def require_verified_user!
      return if current_user.is_verified?
      return head :forbidden if current_user.is_system_user?
      render status: :unprocessable_entity,
             json: { domain: 'Zopim', error: 'VerifiedUserRequired' }
    end

    def require_chat_admin!
      return if current_zopim_identity.try(:is_administrator?)
      render status: :unprocessable_entity,
             json: { domain: 'Zopim', error: 'ChatAdminIdentityRequired' }
    end

    def current_zopim_subscription
      current_account.zopim_subscription
    end

    def current_zopim_identity
      current_user.zopim_identity
    end
  end
end
