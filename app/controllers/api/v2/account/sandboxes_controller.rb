class Api::V2::Account::SandboxesController < Api::V2::BaseController
  before_action :require_admin!
  before_action :require_master_account!
  before_action :throttle_sandboxes_indexes, only: [:index]
  before_action :set_current_sandbox, only: [:show, :destroy]
  skip_around_action :audit_action, only: [:create]

  # The one exception is sandbox_orchestrator needs to create sandbox accounts by POSTing to :create
  skip_before_action :check_sandbox_orchestrator_permissions

  require_capability :sandbox

  ## ### List Sandboxes
  ## `GET /api/v2/account/sandboxes`
  ##
  ## This lists the sandboxes for the current account.
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/sandboxes \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "sandboxes": [
  ##     {
  ##       "account": {
  ##       "url":         "https://example.zendesk.com/",
  ##       "name":        "Example Company",
  ##       "subdomain":   "example",
  ##       "sandbox":     true,
  ##       "time_format": 24,
  ##       "owner_id":    16
  ##     }
  ##   ]
  ## }
  ## ```

  allow_parameters :index, {}
  def index
    render json: presenter.present(sandboxes)
  end

  ## ### Show Sandbox
  ## `GET /api/v2/account/sandboxes/{sandbox_subdomain}.json`
  ##
  ## This shows information on a single sandbox for the current account.
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/sandboxes/{sandbox_subdomain}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## "account": {
  ##   "url":         "https://example.zendesk.com/",
  ##   "name":        "Example Company",
  ##   "subdomain":   "example",
  ##   "sandbox":     true,
  ##   "time_format": 24,
  ##   "owner_id":    16
  ## }
  ## ```
  allow_parameters :show, subdomain: Parameters.string
  def show
    render json: presenter.present(sandbox)
  end

  ## ### Deleting Sandbox
  ## `DELETE /api/v2/account/sandboxes/{sandbox_subdomain}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/sandboxes/{sandbox_subdomain}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, subdomain: Parameters.string
  def destroy
    current_account.destroy_sandbox
    default_delete_response
  end

  ## ### Creates Sandbox
  ## `POST /api/v2/account/sandboxes.json`
  ##
  ## This creates a new sandbox.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/sandboxes.json \
  ##   -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## "account": {
  ##   "url":         "https://example.zendesk.com/",
  ##   "name":        "Example Company",
  ##   "subdomain":   "example",
  ##   "sandbox":     true,
  ##   "time_format": 24,
  ##   "owner_id":    16
  ## }
  ## ```
  allow_parameters :create, sandbox_type: Parameters.string
  def create
    created_sandbox = current_account.add_sandbox(sandbox_type: sandbox_type)
    render json: presenter.present(created_sandbox), status: :created
  rescue Account::SandboxLimitError, Account::SandboxTypeError, Account::OperatingOnSandboxError => e
    record_error(e)
    render_failure(status: :unprocessable_entity, message: e.message.to_s)
  rescue StandardError => e
    record_unexpected_error(e)
    render_failure(status: :unprocessable_entity, message: e.message.to_s)
  end

  protected

  def presenter
    Api::V2::Account::SandboxesPresenter.new(current_user, url_builder: self)
  end

  def set_current_sandbox
    current_account.assign_current_sandbox(params[:subdomain])
  end

  def sandbox
    current_account.sandbox || raise(ActiveRecord::RecordNotFound)
  end

  def sandboxes
    current_account.sandboxes
  end

  def require_master_account!
    deny_access if current_account.is_sandbox?
  end

  def sandbox_type
    params[:sandbox_type] || ::Accounts::Sandbox::STANDARD
  end

  private

  def throttle_sandboxes_indexes
    Prop.throttle!(:sandboxes_indexes, [current_account.id])
  end

  def record_error(error)
    count_error(error)
  end

  def record_unexpected_error(error)
    ZendeskExceptions::Logger.record(error, location: self, message: error.message, fingerprint: 'cc50ad634e232e60d5e67dd4dcc6071b6a1031d4')
    count_error(error)
  end

  def count_error(error)
    statsd_client.increment('sandbox.create.error', tags: ["error:#{error_class_tag(error)}"])
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['sandbox'])
  end

  def error_class_tag(error)
    error.class.name.snake_case
  end
end
