module Api::V2::Account
  class ZopimAgentsController < Api::V2::BaseController
    before_action :require_agent!, only: :show

    allow_subsystem_user :zopim

    # NOTE: This is temporary fix (mainly for compatibility), we'll remove this
    # declaration and rely on the base controller's error handler once the
    # Classic and Lotus deploy embargo of 2014 is lifted - JG
    rescue_from ActiveRecord::RecordInvalid do |exception|
      messages = exception.message.split(':').last.split(',').map(&:strip)
      render(
        json: { domain: 'Zopim', error: messages.first },
        status: :unprocessable_entity
      )
    end

    ## ### Fetches the current user's Zopim agent identity.
    ## `GET /api/v2/account/zopim_agents.json`
    ##
    ## Fetches the current user's Zopim agent identity. If the current user
    ## does not have a Zopim agent identity (ie: not assigned as a Zopim agent)
    ## then it responds with a 422 HTTP status.
    ##
    ## #### Allowed For:
    ##
    ##  * Owners
    ##  * Admins
    ##  * Agents
    ##
    ## #### Using curl
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/account/zopim_agents/1.json \
    ## -X GET \
    ## -H "Content-Type: application/json" \
    ## -v -u {email_address}:{password}
    ## ```
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "zopim_identity": {
    ##      "zopim_agent_id":   5678,
    ##      "email":            "trinity@example.com",
    ##      "is_owner":         false,
    ##      "is_enabled":       true,
    ##      "is_administrator": false,
    ##      "display_name":     "trinity",
    ##      "syncstate":        "ready",
    ##      "is_linked":        true,
    ##      "is_serviceable":   true
    ##   }
    ## }
    ## ```
    ##
    allow_parameters :show, id: Parameters.bigid
    def show
      render json: presenter.present(zendesk_user.zopim_identity)
    end

    private

    def presenter
      ZopimAgentPresenter.new(current_user, url_builder: self)
    end

    # Return the Zendesk user that will be the subject of the requested
    # operation.
    def zendesk_user
      @zendesk_user ||= current_account.users.
        find(params[:id] || params[:agent][:user_id])
    end
  end
end
