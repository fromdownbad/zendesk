class Api::V2::Account::SettingsController < Api::V2::BaseController
  include Zendesk::Accounts::SettingsCacheSupport

  before_action :require_admin!, except: :show

  around_action :disable_account_settings_kasket, only: :update

  allow_subsystem_user :embeddable, only: [:show]
  allow_subsystem_user :mobile_sdk_api, only: [:show]
  allow_subsystem_user :knowledge_api, only: [:show]
  allow_zopim_user only: %i[show update]
  allow_pigeon_user only: %i[show]

  ## ### Show Settings
  ## `GET /api/v2/account/settings.json`
  ##
  ## Shows the settings that are available for the account.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/settings.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "settings": {
  ##     "apps": {.. },
  ##     "tickets": { ...  },
  ##     "chat": { ... },
  ##     "twitter": { ... },
  ##     "users": { ... }
  ##   }
  ## }
  ## ```
  allow_parameters :show, {}
  def show
    expires_in(60.seconds) if current_account.has_account_settings_cache_control_header? && app_request?

    render json: presenter.present(current_account)
  end

  ## ### Update Account Settings
  ## `PUT /api/v2/account/settings.json`
  ##
  ## Updates settings for the account. See [JSON Format](#json-format) above for the settings you can update.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/settings.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{ "settings": { "active_features": { "customer_satisfaction": false }}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "settings": {
  ##     ...
  ##     "active_features": {
  ##       "customer_satisfaction": false,
  ##       ...
  ##   }
  ## }
  ## ```
  allow_parameters :update, settings: {
    active_features: {
      user_tagging: Parameters.boolean,
      ticket_tagging: Parameters.boolean,
      customer_satisfaction: Parameters.boolean,
      business_hours: Parameters.boolean,
      automatic_answers: Parameters.boolean,
      allow_ccs: Parameters.boolean
    },
    api: {
      # Cannot be set to false
      accepted_api_agreement: Parameters.enum(true),
      api_password_access: Parameters.boolean,
      api_token_access: Parameters.boolean
    },
    branding: {
      header_color: Parameters.string,
      page_background_color: Parameters.string,
      tab_background_color: Parameters.string,
      text_color: Parameters.string
    },
    brands: {
      default_brand_id: Parameters.integer,
      require_brand_on_new_tickets: Parameters.boolean
    },
    google_apps: {
      has_google_apps_admin: Parameters.boolean
    },
    groups: {
      check_group_name_uniqueness: Parameters.boolean
    },
    ticket_form: {
      ticket_forms_instructions: Parameters.string
    },
    tickets: {
      accepted_new_collaboration_tos:       Parameters.boolean,
      agent_can_change_requester:           Parameters.boolean,
      agent_email_ccs_become_followers:     Parameters.boolean,
      agent_ticket_deletion:                Parameters.boolean,
      auto_updated_ccs_followers_rules:     Parameters.boolean,
      comment_email_ccs_allowed:            Parameters.boolean,
      comments_public_by_default:           Parameters.boolean,
      edit_ticket_skills_permission:        Parameters.integer,
      email_attachments:                    Parameters.boolean,
      emoji_autocompletion:                 Parameters.boolean,
      follower_and_email_cc_collaborations: Parameters.boolean,
      is_first_comment_private_enabled:     Parameters.boolean,
      light_agent_email_ccs_allowed:        Parameters.boolean,
      list_empty_views:                     Parameters.boolean,
      list_newest_comments_first:           Parameters.boolean,
      markdown_ticket_comments:             Parameters.boolean,
      private_attachments:                  Parameters.boolean,
      tagging:                              Parameters.boolean,
      ticket_followers_allowed:             Parameters.boolean,
    },
    agents: {
      agent_workspace: Parameters.boolean,
      focus_mode: Parameters.boolean
    },
    rule: {
      macro_most_used: Parameters.boolean,
      macro_order: Parameters.enum('alphabetical', 'position'),
      skill_based_filtered_views: Parameters.array(Parameters.integer) | Parameters.integer
    },
    user: {
      tagging: Parameters.boolean,
      agent_created_welcome_emails: Parameters.boolean,
      end_user_phone_number_validation: Parameters.boolean,
      have_gravatars_enabled: Parameters.boolean
    },
    onboarding: {
      onboarding_segments: Parameters.string
    },
    automatic_answers: {
      threshold: Parameters.string
    },
    cross_sell: {
      show_chat_tooltip: Parameters.boolean
    },
    localization: {
      locale_ids: Parameters.array(Parameters.integer)
    },
    email: {
      accept_wildcard_emails: Parameters.boolean,
      custom_dkim_domain: Parameters.boolean,
      email_sender_authentication: Parameters.boolean,
      email_template_photos: Parameters.boolean,
      email_template_selection: Parameters.boolean,
      gmail_actions: Parameters.boolean,
      html_mail_template: Parameters.string,
      mail_delimiter: Parameters.string,
      modern_email_template: Parameters.boolean,
      no_mail_delimiter: Parameters.boolean,
      personalized_replies: Parameters.boolean,
      simplified_email_threading: Parameters.boolean,
      rich_content_in_emails: Parameters.boolean,
      send_gmail_messages_via_gmail: Parameters.boolean,
      text_mail_template: Parameters.string
    }
  }
  def update
    if setting_arguments = params[:settings].presence
      Zendesk::Accounts::SettingsControllerSupport.normalize_and_update_settings(setting_arguments, current_account)
      show
    else
      head :bad_request
    end
  end

  protected

  def presenter
    Api::V2::Account::SettingsPresenter.new(current_user, url_builder: self, ssl: request.ssl?)
  end
end
