class Api::V2::Account::FeaturesController < Api::V2::BaseController
  before_action :require_agent!, only: [:show, :status]
  before_action :require_admin!, only: [:update]
  before_action :never_production, only: [:update]

  allow_subsystem_user :mobile_sdk_api, only: [:show]

  ## ### Show Features
  ## `GET /api/v2/account/features.json`
  ## `GET /api/v2/account/features.json?subscription=true`
  ##
  ## Returns a list of features and whether they are enabled for the account.
  ## Checks if a feature (arturo, subscription, custom code block) is available.
  ## If a corresponding setting exists, also checks whether that setting is enabled.
  ##
  ## The optional query-param `subscription` may be supplied and if it evaluates
  ## to a true value (eg: subscription=true) then the list of features is limited
  ## to subscription-backed features only.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/features.json \
  ##   -v -u {email_address}:{password}
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "features": {
  ##     "agent_collision": {
  ##       "enabled": true
  ##     },
  ##     "agent_display_names": {
  ##       "enabled": true
  ##     },
  ##     "api_limit_200_rpm": {
  ##       "enabled": false
  ##     },
  ##     "api_limit_400_rpm": {
  ##       "enabled": false
  ##     },
  ##     "api_limit_700_rpm" : {
  ##       "enabled": true
  ##     },
  ##     "api_limit_700_rpm_legacy": {
  ##       "enabled": false
  ##     },
  ##     "apps_private": {
  ##       "enabled": false
  ##     },
  ##     ...
  ##   }
  ## }
  ##

  allow_parameters :show, subscription: Parameters.boolean
  def show
    render json: features_presenter.present(account_features)
  end

  ## ### Enable/Disable Feature
  ## `PUT /api/v2/account/features.json`
  ##
  ## Adds or removes this subdomain from the features 'external_beta_subdomains'.
  ## Issues an error if the feature isn't in the right phase.
  ## Not available in production.
  ##
  ## ##### Allow For
  ##
  ## * Admins, only
  ##
  ## ##### Using Curl
  ##
  ## ```bash
  ## curl -X PUT https://{subdomain}.zendesk.com/api/v2/account/features.json \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json" \
  ##   --data '{ "feature":"squelch_kittens", "enabled": true }'
  ##
  ## Request Data
  ## {
  ##       "feature": "squelch_kittens",
  ##       "enabled": true
  ## }
  ##
  ## ##### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##       "feature": "squelch_kittens",
  ##       "enabled": true,
  ## }
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##       "feature": "squelch_bears",
  ##       "error": "Something went wrong"
  ## }
  ##
  ##

  allow_parameters :update, feature: Parameters.string, exclude: Parameters.boolean, enabled: Parameters.boolean
  def update
    name = params[:feature]
    feature = Arturo::Feature.find_feature(name)
    anti_subdomain = "-#{current_account.subdomain}"
    unless feature
      render json: { feature: name, error: 'Feature not found' }, status: :not_found
      return
    end

    if params[:enabled] && params[:exclude]
      render json: { feature: name, error: "Cannot both enable and exclude subdomain" }, status: :bad_request
      return
    end

    # fail when user tries to enable but we are not in external_beta phase
    if feature.phase != 'external_beta' && params[:exclude].nil?
      unless feature.external_beta_subdomains.include?(anti_subdomain)
        render json: { feature: name, error: "Feature not in phase 'external_beta'" }, status: :bad_request
        return
      end
    end

    if params[:exclude]
      feature.external_beta_subdomains << anti_subdomain
      feature.save!
    else
      if feature.external_beta_subdomains.include?(anti_subdomain)
        feature.external_beta_subdomains.delete(anti_subdomain)
        feature.save!
      end
    end

    if params[:enabled]
      unless feature.enabled_for?(current_account)
        feature.external_beta_subdomains << current_account.subdomain
        feature.save!
      end
    else
      if feature.external_beta_subdomains.include?(current_account.subdomain)
        feature.external_beta_subdomains.delete(current_account.subdomain)
        feature.save!
      end
    end

    render json: { feature: name, enabled: feature.reload.enabled_for?(current_account) }
  end

  # #### Check if a arturo feature is enabled for an account
  #
  # `GET /api/v2/account/features/{feature_id}/status.json
  #
  # #### Allowed For
  #
  #  * Agent
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/account/features/zendesk_v8/status.json -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200
  # {
  #   "enabled": true
  # }
  # ```
  allow_parameters :status, feature_id: ActionController::Parameters.string
  require_oauth_scopes :status, scopes: [:write], arturo: true
  def status
    render json: { enabled: Arturo.feature_enabled_for?(params[:feature_id], current_account) }
  end

  protected

  def is_production? # rubocop:disable Naming/PredicateName
    Rails.env.production?
  end

  def features_presenter
    @features_presenter ||= Api::V2::Account::FeaturesPresenter.new(
      current_user,
      url_builder: self,
      includes: includes
    )
  end

  def prevent_inactive_account_access
    # Skip this check for only the show action on multiproduct (shell) accounts.
    # See PR #34777 for why `skip_before_action` with `only:` does not work.
    return if allow_shell_accounts?
    super
  end

  def enforce_support_is_active!
    # Skip this check for only the show action on multiproduct (shell) accounts.
    # See PR #34777 for why `skip_before_action` with `only:` does not work.
    return if allow_shell_accounts?
    super
  end

  private

  def account_features
    Account.capabilities.select { |_, c| c.public? }.tap do |features|
      features.select! { |_, c| c.subscription? } if params[:subscription]
    end
  end

  def never_production
    deny_access if is_production?
  end

  def allow_shell_accounts?
    action_name == 'show' &&
      current_account.try(:multiproduct?)
  end
end
