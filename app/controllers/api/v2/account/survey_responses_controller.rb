class Api::V2::Account::SurveyResponsesController < Api::V2::BaseController
  before_action :require_admin!, only: [:create, :update, :destroy]

  survey_response_parameter = {
    agent_count: Parameters.integer,
    customer_count: Parameters.string,
    employee_count: Parameters.string,
    full_time_agents: Parameters.string,
    industry: Parameters.string,
    is_primary_support: Parameters.string,
    job_role: Parameters.string,
    primary_support: Parameters.string,
    support_structure: Parameters.string,
    target_audience: Parameters.string,
    team_count: Parameters.integer | Parameters.nil
  }

  ## ### Show Survey Response
  ## `GET /api/v2/account/survey_response.json`
  ##
  ## This shows the account's benchmark survey response.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/survey_response.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## "survey_response": {
  ##   "id": 62
  ##   "industry": "media",
  ##   "employee_count": "10-99",
  ##   "target_audience": "customers",
  ##   "primary_support": "zendesk",
  ##   "customer_count": "1",
  ##   "support_structure": "employees",
  ##   "agent_count": null, # or number
  ##   "team_count": null # or number
  ## }
  ## ```
  ##
  ## If no survey response is available:
  ## Status: 204 No Content
  allow_parameters :show, {}
  def show
    if survey_response
      render json: presenter.present(survey_response)
    else
      head :no_content
    end
  end

  ## ### Creating Survey Response
  ## `POST /api/v2/account/survey_response.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://zendesk.com/api/v2/account/survey_response.json \
  ##   -d '{"survey_response": {"agent_count": "15"}}'
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## "survey_response": {
  ##   "id": 62
  ##   "industry": "media",
  ##   "employee_count": "10-99",
  ##   "target_audience": "customers",
  ##   "primary_support": "zendesk",
  ##   "customer_count": "1",
  ##   "support_structure": "employees",
  ##   "agent_count": null, # or number
  ##   "team_count": null # or number
  ## }
  ## ```
  allow_parameters :create, survey_response: survey_response_parameter
  def create
    # on creating the survey/submitting the response, this setting should be set to false
    # as this setting implies that the account has opted into the benchmark survey
    if current_account.has_account_settings_benchmark_opt_out?
      current_account.settings.benchmark_opt_out = false
      current_account.settings.save
    end
    survey_response = current_account.create_survey_response(params[:survey_response])
    render json: presenter.present(survey_response), status: :created
  end

  ## ### Updating Survey Response
  ## `PUT /api/v2/account/survey_response.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://zendesk.com/api/v2/account/survey_response.json \
  ##   -d '{"survey_response": {"agent_count": "15"}}'
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## "survey_response": {
  ##   "id": 62
  ##   "industry": "media",
  ##   "employee_count": "10-99",
  ##   "target_audience": "customers",
  ##   "primary_support": "zendesk",
  ##   "customer_count": "1",
  ##   "support_structure": "employees",
  ##   "agent_count": null, # or number
  ##   "team_count": null # or number
  ## }
  ## ```
  allow_parameters :update, id: Parameters.bigid, survey_response: survey_response_parameter
  def update
    survey_response.update_attributes!(params[:survey_response])
    render json: presenter.present(survey_response)
  end

  ## ### Deleting Survey Response
  ## `DELETE /api/v2/account/survey_response.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://zendesk.com/api/v2/account/survey_response.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    # on delete action, this setting should be set to true
    # as this setting implies that the account has opted out of the benchmark survey
    if current_account.has_account_settings_benchmark_opt_out?
      current_account.settings.benchmark_opt_out = true
      current_account.settings.save
    end

    survey_response.destroy if survey_response.present?
    default_delete_response
  end

  protected

  def presenter
    Api::V2::Account::SurveyResponsePresenter.new(current_user, url_builder: self)
  end

  def survey_response
    @survey ||= current_account.survey_response
  end
end
