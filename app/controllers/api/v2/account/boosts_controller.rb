class Api::V2::Account::BoostsController < Api::V2::BaseController
  before_action :require_admin!, only: :show

  # ### Show Boosts
  # `GET /api/v2/account/boosts.json`
  #
  # Returns a list of associated addons for the account including both
  # plan boosts and feature boosts
  #
  # #### Allowed For:
  #
  #  * Admins
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/account/boosts.json \
  #   -v -u {email_address}:{password}
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "boosts": [
  #     {
  #       "name": "test0",
  #       "type": "addon",
  #       "days_remaining": -372
  #     },
  #     {
  #       "name": "Professional",
  #       "type": "plan",
  #       "days_remaining": -372
  #     }
  #   ]
  # }
  # ```

  allow_parameters :show, {}
  def show
    render json: Api::V2::Account::BoostsPresenter.new(current_user, url_builder: self).present(current_account)
  end
end
