require 'zendesk_billing_core/zuora/plan_validator'

class Api::V2::Account::SubscriptionController < Api::V2::BaseController
  include ZBC::Zuora::Logging

  PRODUCT_NAMES = [
    'answer_bot',
    'explore',
    'outbound',
    'tpe',
    'voice',
    'zopim'
  ].freeze

  before_action :require_agent!, only: [:show]
  before_action :require_owner!, except: [:show]
  before_action :require_zuora_managed, except: [:destroy]
  before_action :check_if_assisted_subscription, only: [:update]
  before_action :require_trial, only: [:update_currency]
  before_action :validate_starter, only: [:create, :update]
  before_action :validate_voice_max_agents_limit, only: [:create, :update]
  before_action :validate_agent_add_available, only: [:add_agents]
  before_action :validate_agent_increase, only: [:add_agents]

  allow_subsystem_user :bime, only: [:show]
  skip_before_action :require_zuora_managed, if: :bime_system_user_request?

  # note: this is a hack to work around a Rails bug, documented here:
  #       https://github.com/rails/rails/issues/9703
  # The main idea here is that by default, #enforce_support_is_active! is called
  # before all actions under the Api::V2::BaseController. However, we want to
  # ignore it for the #show action on shell accounts where Support trial has
  # expired. To work around the Rails bug, we 1) ignore the before_action for
  # the #show action in general, and then 2) add it back in all cases *except*
  # where #multiproduct_support_expired? returns true.
  skip_before_action :enforce_support_is_active!, only: :show
  before_action      :enforce_support_is_active!, only: :show, unless: :multiproduct_support_expired?

  after_action :increment_preview_success_counter, only: :preview

  rescue_from ZBC::Zuora::InvalidPlanConfigurationError, AnswerBot::SubscriptionOptions::Error, Outbound::SubscriptionOptions::Error do |exception|
    ZendeskExceptions::Logger.record(exception, location: self, message: exception.message, fingerprint: '625d938362098ea30241df7fe7eecf0e33324ede')
    log(exception.class.name, exception.message)

    description = 'Unable to apply changes to plan, please contact ' \
                  'support@zendesk.com'

    render(
      status: :unprocessable_entity,
      json: {
        error: exception.class.name,
        description: description,
        details: [
          {
            exception: {
              description: description
            }
          }
        ]
      }
    )
  end

  rescue_from ZuoraClient::SubscribeCommandFailure, ZuoraClient::UpdateFailure do |exception|
    tax_error = exception.message.include?('Taxation')

    ZendeskExceptions::Logger.record(exception, location: self, message: exception.message, fingerprint: 'eb94028ec7f4581ccf6b14cb2dc023e8040e5f89') unless tax_error
    log(exception.class.name, exception.message)
    increment_error_counter(exception)

    i18n_key = begin
      base_key        = 'txt.admin.controllers.apiv2.account.subscription'
      tax_sub_key     = 'state_required_us_canada'
      generic_sub_key = 'internal_error'

      sub_key = tax_error ? tax_sub_key : generic_sub_key

      "#{base_key}.#{sub_key}"
    end

    render(
      status: :unprocessable_entity,
      json: {
        error:       exception.class.name,
        description: I18n.t(i18n_key),
        message:     exception.message
      }
    )
  end

  rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do |exception|
    ZendeskExceptions::Logger.record(exception, location: self, message: exception.message, fingerprint: '0ad851b59ff8a2a9dc4f38f700598d2b5fdea18c')
    log(exception.class.name, exception.message)
    increment_error_counter(exception)

    text = 'Database collision - please try again'

    render(
      status:       :conflict,
      text:         text,
      content_type: Mime[:text].to_s
    )
  end

  rescue_from ZBC::Support::BlockingLockWithTimeout::FailedToAcquireLockError do |exception|
    ZendeskExceptions::Logger.record(exception, location: self, message: exception.message, fingerprint: '73288da9d27e9c928c4716c73b39c8f09703fc9f')
    log(exception.class.name, exception.message)
    increment_error_counter(exception)

    text = 'Subscription update already in progress - please refresh the ' \
           'page for latest information.'

    render(
      status:       :conflict,
      text:         text,
      content_type: Mime[:text].to_s
    )
  end

  rescue_from Kragle::ServerError do |exception|
    ZendeskExceptions::Logger.record(exception, location: self, message: exception.message, fingerprint: 'd0976360cd35b6c3e9ccfddc6cfee6504d3df567')
    log(exception.class.name, exception.message)
    render(
      json: {
        error:       exception.class.name,
        description: 'Unable to connect to the account service.',
        message:     exception.message
      },
      status: :bad_gateway
    )
  end

  ## ### Show Subscription
  ## `GET /api/v2/account/subscription.json`
  ##
  ## Returns the current subscription configuration of this account.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/subscription.json?include=voice \
  ##   -v -u {email_address}:{password}
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "subscription": {
  ##     "active": true,
  ##     "trial": false,
  ##     "suspended": false,
  ##     "dunning": "OK",
  ##     "plan_type": "ExtraLarge",
  ##     "max_agents": 5,
  ##     "source": null,
  ##     "help_desk_size": "Small team",
  ##     "created_at": "2012-12-13T19:43:05Z",
  ##     "updated_at": "2012-12-13T19:43:11Z",
  ##     "days_left_in_trial": 0,
  ##     "voice_customer": false,
  ##     "zuora_payment_method_type": "credit card"
  ##   },
  ##   "voice": {
  ##     "phone_numbers": ["+14157499353", "+14157499352"]
  ##   }
  ## }
  ##
  # TODO We might want to get rid of or modify the allow_parameters declaration
  # as it filters out all params including the spec for sideloads.
  allow_parameters :show, {}
  def show
    render json: presenter.present(subscription)
  end

  ## ### Preview Subscription
  ## `GET /api/v2/account/subscription/preview.json`
  ##
  ## Previews a subscription configuration for this account. Note that the `pricing`
  ## sideload is required to view plan cost, discounts, and other computed subscription
  ## properties.
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://trial.zendesk.dev/api/v2/account/subscription/preview.json?include=pricing \
  ##   -H "Content-Type: application/json"                                                                          \
  ##   -d '{"subscription": { "max_agents": 3, "billing_cycle_type": 1, "plan_type": 2, "promo_code": "XYZ123", "zopim_max_agents": 2, "zopim_plan_type": "basic" }}' \
  ##   -v -u admin@zendesk.com:PASSWORD -X GET
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "subscription":
  ##     {
  ##       "account_type": "customer",
  ##       "active": true,
  ##       "trial": false,
  ##       "suspended": false,
  ##       "dunning": "OK",
  ##       "sales_model": "Self-service",
  ##       "plan_type": "Medium",
  ##       "plan_name": "Regular",
  ##       "file_upload_cap": 7,
  ##       "max_agents": 3,
  ##       "source": "",
  ##       "help_desk_size": "Small team",
  ##       "created_at": null,
  ##       "updated_at": null,
  ##       "trial_expires_on": "2014-09-04",
  ##       "days_left_in_trial": 0,
  ##       "voice_customer": false,
  ##       "billing_backend": "zuora",
  ##       "is_elite": false,
  ##       "plans":
  ##         {
  ##           "zendesk":
  ##             {
  ##               "plan_type": "Medium",
  ##               "plan_name": "Regular",
  ##               "max_agents": 3,
  ##               "is_elite": false,
  ##               "trial": false,
  ##               "file_upload_cap": 7,
  ##               "help_desk_size": "Small team",
  ##               "trial_expires_on": "2014-09-04",
  ##               "days_left_in_trial": 0
  ##             },
  ##           "zopim":
  ##             {
  ##               "max_agents": 2,
  ##               "plan_type": "basic"
  ##             },
  ##           "voice":
  ##             {
  ##               "voice_customer": false
  ##             }
  ##         }
  ##     },
  ##   "pricing":
  ##     {
  ##       "version": 5,
  ##       "plan":
  ##         {
  ##           "id": 2,
  ##           "name": "Regular"
  ##         },
  ##       "available_plans":
  ##         [
  ##           {
  ##             "plan_type": 1,
  ##             "plan_type_name": "small",
  ##             "name": "Starter",
  ##             "price": 2,
  ##             "currency": "USD",
  ##             "base": 2,
  ##             "max_agent_floor": 1,
  ##             "max_agent_ceiling": 3,
  ##             "per_agent_cost_with_monthly_billing": 2.0,
  ##             "per_agent_cost_with_quarterly_billing": null,
  ##             "per_agent_cost_with_annual_billing": 1.0,
  ##             "promotional_discount_monthly": 0,
  ##             "promotional_discount_quarterly": null,
  ##             "promotional_discount_annually": 0
  ##           },
  ##           {
  ##             "plan_type": 2,
  ##             "plan_type_name": "medium",
  ##             "name": "Regular",
  ##             "price": 29,
  ##             "currency": "USD",
  ##             "base": 29,
  ##             "max_agent_floor": 1,
  ##             "max_agent_ceiling": 4294967296,
  ##             "per_agent_cost_with_monthly_billing": 29.0,
  ##             "per_agent_cost_with_quarterly_billing": null,
  ##             "per_agent_cost_with_annual_billing": 25.0,
  ##             "promotional_discount_monthly": 0,
  ##             "promotional_discount_quarterly": null,
  ##             "promotional_discount_annually": 0
  ##           },
  ##           {
  ##             "plan_type": 3,
  ##             "plan_type_name": "large",
  ##             "name": "Plus",
  ##             "price": 69,
  ##             "currency": "USD",
  ##             "base": 69,
  ##             "max_agent_floor": 1,
  ##             "max_agent_ceiling": 4294967296,
  ##             "per_agent_cost_with_monthly_billing": 59.0,
  ##             "per_agent_cost_with_quarterly_billing": null,
  ##             "per_agent_cost_with_annual_billing": 49.0,
  ##             "promotional_discount_monthly": 10.0,
  ##             "promotional_discount_quarterly": null,
  ##             "promotional_discount_annually": 10.0
  ##           },
  ##           {
  ##             "plan_type": 4,
  ##             "plan_type_name": "extra_large",
  ##             "name": "Enterprise",
  ##             "price": 139,
  ##             "currency": "USD",
  ##             "base": 139,
  ##             "max_agent_floor": 1,
  ##             "max_agent_ceiling": 4294967296,
  ##             "per_agent_cost_with_monthly_billing": 139.0,
  ##             "per_agent_cost_with_quarterly_billing": null,
  ##             "per_agent_cost_with_annual_billing": 125.0,
  ##             "promotional_discount_monthly": 0,
  ##             "promotional_discount_quarterly": null,
  ##             "promotional_discount_annually": 0
  ##           }
  ##         ],
  ##       "billing_cycle":
  ##         {
  ##           "id": 1,
  ##           "name": "Monthly",
  ##           "discount": 0.0
  ##         },
  ##       "voice":
  ##         {
  ##           "active": false
  ##         },
  ##       "promo":
  ##         {
  ##           "code": "XYZ123",
  ##           "discount": 0.0,
  ##           "ends_at": null,
  ##           "status": "not_valid"
  ##         },
  ##       "charge":
  ##         {
  ##           "currency": "USD",
  ##           "gross": 87.0,
  ##           "net": 87.0
  ##         },
  ##       "agent_cost_summary":
  ##         {
  ##           "monthly_unit_cost": 29.0,
  ##           "monthly_unit_cost_with_annual_billing": 25.0,
  ##           "agents_per_unit": 1,"annual_unit_cost": 300.0
  ##         },
  ##       "pending_changes":
  ##         {
  ##           "term_start_date": "2014-09-05T00:00:00+00:00",
  ##           "plan_type": 3,
  ##           "plan_name": "Plus",
  ##           "max_agents": 1,
  ##           "billing_cycle_type": 1
  ##         }
  ##     }
  ## }
  ## ```
  ##
  allow_parameters :preview, :skip # TODO: make stricter
  require_oauth_scopes :preview, scopes: [:read], arturo: true
  def preview
    quote = subscription.preview!(subscription_options)
    render json: presenter.present(quote)
  end

  ## ### Create Subscription
  ## `POST /api/v2/account/subscription.json`
  ##
  ## Creates the subscription configuration for the account.
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/subscription.json                                                                     \
  ##   -H "Content-Type: application/json" -X PUT                                                                                              \
  ##   -d '{"subscription": { "max_agents": 3, "billing_cycle_type": 1, "plan_type": 2, "payment_method_reference_id": '3alkdj3w4opuioouit'}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  allow_parameters :create, :skip # TODO: make stricter
  def create
    ZBC::Zuora::Commands::Subscribe.new(subscription_options).execute!

    render json: { message: "Subscription created." }
  end

  ## ### Update Subscription
  ## `PUT /api/v2/account/subscription.json`
  ##
  ## Updates the subscription configuration for the account.
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/subscription.json                 \
  ##   -H "Content-Type: application/json" -X PUT                                          \
  ##   -d '{"subscription": { "max_agents": 3, "billing_cycle_type": 1, "plan_type": 2 }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "subscription": {
  ##     "active": true,
  ##     "trial": false,
  ##     "suspended": false,
  ##     "dunning": "OK",
  ##     "plan_type": "Medium",
  ##     "max_agents": 3,
  ##     "source": null,
  ##     "help_desk_size": "Small team",
  ##     "created_at": "2012-12-13T19:43:05Z",
  ##     "updated_at": "2012-12-13T19:43:11Z",
  ##     "days_left_in_trial": 3651,
  ##     "voice_customer": false
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update, :skip # TODO: make stricter
  def update
    command = ZBC::Zuora::Commands::Amend.new(
      subscription_options
    )

    command.synchronizer = ZBC::Zuora::Synchronizer.new(
      zuora_client.zuora_account_id,
      bypass_approval: true,
      locking_enabled: false
    )

    command.execute!

    # Subscription and Zuora::Subscription records get updated during amend
    # and then used in presenter.present below

    ZBC::Zuora::Subscription.connection.uncached do
      render json: presenter.present(current_account.reload.subscription)
    end
  end

  ## ### Update Payment Method
  ## `POST /api/v2/account/subscription/update_payment_method.json`
  ##
  ## Updates the payment method for the account.
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/subscription/update_payment_method.json \
  ##   -H "Content-Type: application/json" -X PUT                                                \
  ##   -d '{"payment_method_reference_id": '3alkdj3w4opuioouit'}'                                \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  allow_parameters :update_payment_method, :skip # TODO: make stricter
  require_oauth_scopes :update_payment_method, scopes: [:write], arturo: true
  def update_payment_method
    refid = params[:subscription][:payment_method_reference_id]
    zuora_adapter.update_cc_payment_method(refid)
    render json: {message: "Payment method updated"}
  end

  ## ### Update currency
  ## `POST /api/v2/account/subscription/update_currency.json`
  ##
  ## Updates the currency type for trial account.
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/subscription/update_currency.json \
  ##   -H "Content-Type: application/json" -X POST                                         \
  ##   -d '{"subscription" : {"currency": "GBP"}}'                                         \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  allow_parameters :update_currency, :skip # TODO: make stricter
  require_oauth_scopes :update_currency, scopes: [:write], arturo: true
  def update_currency
    subscription = current_account.subscription
    currency = params[:subscription][:currency].to_s.strip
    if currency_type = CurrencyType.find(currency)
      subscription.currency_type = currency_type
      subscription.save(validate: false)

      render json: { status: "Subscription currency updated."}
    else
      render json: { status: "Invalid currency value." }, status: :unprocessable_entity
    end
  end

  ## ### Add Agents
  ## `POST /api/v2/account/subscription/add_agents`
  ##
  ## Adds zendesk agents to the subscription
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/add_agents \
  ##   -H "Content-Type: application/json" -X PUT                   \
  ##   -d '{"agents": { "zendesk_agent_add": 2 }}'                  \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "subscription": {
  ##     "active": true,
  ##     "trial": false,
  ##     "suspended": false,
  ##     "dunning": "OK",
  ##     "plan_type": "Medium",
  ##     "max_agents": 5,
  ##     "source": null,
  ##     "help_desk_size": "Small team",
  ##     "created_at": "2012-12-13T19:43:05Z",
  ##     "updated_at": "2012-12-13T19:43:11Z",
  ##     "days_left_in_trial": 3651,
  ##     "voice_customer": false
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :add_agents, agents: {
    zendesk_agent_add: Parameters.integer
  }
  require_oauth_scopes :add_agents, scopes: [:write], arturo: true
  def add_agents
    if Billing::CustomerOrder::Client.post(current_account, current_user, zendesk_max_agents)
      ZBC::Zuora::Synchronizer.synchronize!(current_account.billing_id)
    else
      render json: { status: "Agent Add failed" }, status: :unprocessable_entity
      return
    end

    ZBC::Zuora::Subscription.connection.uncached do
      render json: presenter.present(current_account.reload.subscription)
    end
  end

  ## ### Cancel Subscription
  ## `DELETE /api/v2/account/subscription.json`
  ##
  ## Cancels the account's subscription. Supply the account's current subdomain
  ## in the `confirm` parameter.
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/subscription.json \
  ##   -H "Content-Type: application/json" -X DELETE                       \
  ##   -d '{ "confirm": "{subdomain}" }'                                   \
  ##   -v -u {email_address}:{password}
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## { "status": "Subscription Cancelled" }
  ##
  ## ```
  ##
  allow_parameters :destroy, :skip # TODO: make stricter
  def destroy
    if params[:confirm] == current_account.subdomain
      current_account.cancel!
      render json: { status: "Subscription Cancelled" }
    else
      render json: { status: "Confirmation Required. See API documentation." }, status: :unprocessable_entity
    end
  end

  private

  def require_zuora_managed
    unless current_account.has_zuora_managed_billing_enabled?
      status = 'This API is not available for your account. See API ' \
               'documentation.'

      render(
        status: :unprocessable_entity,
        json: {
          status: status
        }
      )
    end
  end

  def check_if_assisted_subscription
    if zuora_subscription && zuora_subscription.assisted?
      status = "You can only update your subscription settings if the sales " \
               "model is '#{ZBC::States::SalesModel::SELF_SERVICE}'"

      render(
        status: :unprocessable_entity,
        json: {
          status: status
        }
      )

      message = "#{current_account.id} is not self-service and cannot be " \
                "updated"

      ZendeskExceptions::Logger.record(Exception.new, location: self, message: message, fingerprint: 'e9fef13058f6581406a620f7faf7ed265fd54f78')
      log(message)
    end
  end

  def subscription
    @subscription ||= current_account.subscription
  end

  def presenter
    @presenter ||= Api::V2::Account::SubscriptionPresenter.new(
      current_user,
      url_builder: self,
      includes: includes
    )
  end

  def zuora_adapter
    Billing::Zuora::Adapter.new(
      current_account:  current_account,
      zuora_account_id: zuora_subscription.zuora_account_id,
      zuora_client:     zuora_client
    )
  end

  def zuora_client
    @zuora_client ||= ZBC::Zuora::Client.new(
      zuora_subscription.zuora_account_id
    )
  end

  def zuora_subscription
    ZBC::Zuora::Subscription.find_by_account_id(current_account.id)
  end

  def subscription_options
    options = ZBC::Zuora::SubscriptionOptions.new(
      current_account.id,
      params[:subscription].with_indifferent_access
    )
    options = AnswerBot::SubscriptionOptions.build_for(current_account, options)
    Outbound::SubscriptionOptions.build_for(current_account, options)
  end

  def agent_add_params
    core_params.tap do |agent_add_params|
      PRODUCT_NAMES.each do |product_name|
        product_subscription = send(:"#{product_name}_subscription")
        if product_subscription.try(:include_in_easy_agent_add?)
          agent_add_params.merge!(send(:"#{product_name}_params"))
        end
      end
    end
  end

  def core_params
    {
      max_agents:         zendesk_max_agents,
      plan_type:          subscription.plan_type,
      billing_cycle_type: subscription.billing_cycle_type
    }
  end

  def zopim_params
    {
      zopim_max_agents: zopim_subscription.max_agents,
      zopim_plan_type:  zopim_subscription.plan_type
    }
  end

  def voice_params
    {
      voice_max_agents: voice_subscription.max_agents,
      voice_plan_type:  voice_subscription.plan_type.to_i
    }
  end

  def tpe_params
    {
      tpe_max_agents: tpe_subscription.max_agents,
      tpe_plan_type:  tpe_subscription.plan_type.to_i
    }
  end

  def answer_bot_params
    {
      answer_bot_max_resolutions: answer_bot_subscription.max_resolutions,
      answer_bot_plan_type:       answer_bot_subscription.plan_type
    }
  end

  def explore_params
    {
      explore_max_agents: explore_subscription.max_agents,
      explore_plan_type:  explore_subscription.plan_type
    }
  end

  def outbound_params
    {
      outbound_monthly_message_users: outbound_subscription.monthly_messaged_users,
      outbound_plan_type:             outbound_subscription.plan_type
    }
  end

  def zendesk_max_agents
    subscription.max_agents + zendesk_agent_add_amount
  end

  def zendesk_agent_add_amount
    params[:agents][:zendesk_agent_add].to_i
  end

  def zopim_subscription
    @zopim_subscription ||= subscription.zopim_subscription
  end

  def tpe_subscription
    @tpe_subscription ||= subscription.tpe_subscription
  end

  def voice_subscription
    @voice_subscription ||= current_account.voice_subscription
  end

  def answer_bot_subscription
    @answer_bot_subscription ||= current_account.answer_bot_subscription
  end

  def explore_subscription
    @explore_subscription ||= current_account.explore_subscription
  end

  def outbound_subscription
    @outbound_subscription ||= current_account.outbound_subscription
  end

  def validate_agent_increase
    zendesk_agent_adds = params[:agents][:zendesk_agent_add].try(:to_i)

    return if zendesk_agent_adds >= 1

    error_msg = 'You need to add at least one agent'

    raise ZBC::Zuora::InvalidPlanConfigurationError, error_msg
  end

  def validate_agent_add_available
    return if subscription.add_agents_available?

    error_msg = 'Subscription configuration does not allow easy agent add'

    raise ZBC::Zuora::InvalidPlanConfigurationError, error_msg
  end

  def require_trial
    unless current_account.subscription.is_trial?
      status = 'This API is not available for your account. See API ' \
               'documentation.'

      render(
        status: :unprocessable_entity,
        json: {
          status: status
        }
      )
    end
  end

  def validate_starter
    ZBC::Zuora::PlanValidator.new(params[:subscription], subscription).validate
  end

  # TODO: Move to model when base agents is working
  def validate_voice_max_agents_limit
    zendesk_max_agents = params[:subscription][:max_agents].try(:to_i)
    voice_max_agents   = params[:subscription][:voice_max_agents].try(:to_i)

    return if zendesk_max_agents.nil? || voice_max_agents.nil?
    return if voice_max_agents <= zendesk_max_agents

    error_msg = 'Your voice max agents count needs to be less than or equal ' \
                'to your zendesk max agents count'

    raise ZBC::Zuora::InvalidPlanConfigurationError, error_msg
  end

  def bime_system_user_request?
    current_user.is_system_user? && current_subsystem_name == 'bime'
  end

  def increment_error_counter(error)
    statsd_client = ::Zendesk::StatsD::Client.new(namespace: 'billing')
    metric_name   = 'sli.purchase_error'
    metric_tags   = %W[
      app:classic
      controller:#{self.class.to_s.underscore}
      action:#{action_name.downcase}
      error:#{error.class.to_s.underscore}
    ]

    statsd_client.increment(metric_name, tags: metric_tags)
  end

  def increment_preview_success_counter
    statsd_client = ::Zendesk::StatsD::Client.new(namespace: 'billing')
    metric_name   = 'sli.preview_success'
    metric_tags   = %w[
      app:classic
    ]

    statsd_client.increment(metric_name, tags: metric_tags)
  end

  def multiproduct_support_expired?
    # we only want to check pravda for expired state if this is a shell account
    # AND the subscription record exists
    return false if !multiproduct? || current_account.subscription.nil?
    !!support_product.try(:expired?)
  end
end
