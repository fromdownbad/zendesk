class Api::V2::Account::ResolutionsController < Api::V2::BaseController
  skip_before_action :enforce_ssl!
  skip_before_action :require_agent!
  skip_before_action :authenticate_user
  skip_before_action :ensure_proper_protocol

  ## ### Show Settings
  ## `GET /api/v2/account/resolve.json`
  ##
  ## An unauthorized, non-SSL endpoint for detecting the subdomain and fully qualified
  ## domain name of the account.  This is used by our mobile apps to resolve the account domain
  ## when logging in users who enter a non-SSL, host-mapped domain.
  ##
  ## #### Allowed For:
  ##
  ##  * Anyone
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/resolve.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## { "subdomain": "example" }
  ## { "url": "https://example.zendesk.com" }
  ## ```
  allow_parameters :show, {}
  def show
    render json: { subdomain: current_account.subdomain, url: current_account.url(mapped: false) }
  end
end
