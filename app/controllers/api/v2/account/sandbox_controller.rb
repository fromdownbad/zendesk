class Api::V2::Account::SandboxController < Api::V2::BaseController
  before_action :require_admin!
  before_action :require_master_account!
  before_action :throttle_sandbox_creates, only: [:create]
  skip_around_action :audit_action, only: [:create]
  require_capability :sandbox

  ## ### Show Sandbox
  ## `GET /api/v2/account/sandbox.json`
  ##
  ## This shows the sandbox for the current account.
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/sandbox.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## "account": {
  ##   "url":         "https://example.zendesk.com/",
  ##   "name":        "Example Company",
  ##   "subdomain":   "example",
  ##   "sandbox":     true,
  ##   "time_format": 24
  ##   "owner_id":    16
  ## }
  ## ```
  allow_parameters :show, :skip # TODO: make stricter
  def show
    render json: presenter.present(sandbox)
  end

  ## ### Deleting Sandbox
  ## `DELETE /api/v2/account/sandbox.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/sandbox.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, :skip # TODO: make stricter
  def destroy
    current_account.destroy_sandbox
    default_delete_response
  end

  ## ### Creates Sandbox
  ## `POST /api/v2/account/sandbox.json`
  ##
  ## This will remove any existing sandbox and create a new one.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/sandbox.json \
  ##   -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## "account": {
  ##   "url":         "https://example.zendesk.com/",
  ##   "name":        "Example Company",
  ##   "subdomain":   "example",
  ##   "sandbox":     true,
  ##   "time_format": 24
  ##   "owner_id":    16
  ## }
  ## ```
  allow_parameters :create, :skip # TODO: make stricter
  def create
    sandbox = current_account.reset_sandbox
    render json: presenter.present(sandbox), status: :created
  end

  protected

  def presenter
    Api::V2::AccountPresenter.new(current_user, url_builder: self)
  end

  def sandbox
    current_account.sandbox || raise(ActiveRecord::RecordNotFound)
  end

  def require_master_account!
    deny_access if current_account.is_sandbox?
  end

  private

  def throttle_sandbox_creates
    Prop.throttle!(:sandbox_creates, [current_account.id])
  end
end
