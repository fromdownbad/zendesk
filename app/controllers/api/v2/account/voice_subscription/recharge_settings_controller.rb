class Api::V2::Account::VoiceSubscription::RechargeSettingsController < Api::V2::BaseController
  include Zendesk::Voice::RechargeSupport

  before_action :require_agent!, only: :show
  before_action :require_owner!, except: :show

  before_action :require_no_recharge_settings, only: [:create]
  before_action :require_recharge_settings, only: [:update]

  # Customer can't create or update recharge settings on trial
  before_action :require_zuora_account

  ## ### Create the account's Voice Recharge Settings record.
  ##
  ## `POST /api/v2/account/voice_subscription/recharge_settings.json`
  ##
  ## This will create the account's voice recharge settings record.
  ##
  ## #### Allowed for
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/voice_subscription/recharge_settings.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '{ "recharge_settings": { "enabled": true, "amount": 10.00 }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "recharge_settings": {
  ##     "enabled":         true,
  ##     "minimum_balance": 5.0,
  ##     "amount":          50.0,
  ##     "balance":         1000.00,
  ##     "payment_status":  "ok"
  ##   }
  ## }
  ## ```
  allow_parameters :create, recharge_settings: {
    enabled: Parameters.boolean,
    amount: Parameters.float | Parameters.integer
  }
  def create
    update_recharge_settings(create_params)
    render json: presenter.present(recharge_settings), status: :created
  end

  ## ### Show Voice Recharge Settings
  ## `GET /api/v2/account/voice_subscription/recharge_settings.json`
  ##
  ## This shows the voice recharge settings that are available for the account.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/voice_subscription/recharge_settings.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "recharge_settings": {
  ##     "enabled": true,
  ##     "minimum_balance": 5.00,
  ##     "amount": 50,
  ##     "balance": 1000.00,
  ##     "payment_status": "ok"
  ##   }
  ## }
  ## ```
  allow_parameters :show, {}
  def show
    recharge_settings = Voice::RechargeSettings.find_by_account_id(current_account.id)

    expires_in 5.minutes unless recharge_settings.present?

    render json: presenter.present(recharge_settings)
  end

  ## ### Update Account Settings
  ## `PUT /api/v2/account/voice_subscription/recharge_settings.json`
  ##
  ## Only two settings can be updated:
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/account/voice_subscription/recharge_settings.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{ "recharge_settings": { "enabled": true, "amount": 10.00 }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## ```
  allow_parameters :update, recharge_settings: {
    enabled: Parameters.boolean,
    amount: Parameters.float | Parameters.integer
  }
  def update
    if update_recharge_settings(params.require(:recharge_settings))
      render json: presenter.present(recharge_settings)
    else
      render json: presenter.present_errors(recharge_settings), status: :unprocessable_entity
    end
  end

  protected

  def presenter
    @presenter ||= Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter.new(current_user, url_builder: self)
  end
end
