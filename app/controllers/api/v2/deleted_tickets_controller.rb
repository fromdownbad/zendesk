class Api::V2::DeletedTicketsController < Api::V2::BaseController
  prepend_around_action :forbid_nil_parameters, only: [:destroy_many, :restore_many]
  before_action :user_can_delete?, only: [:destroy, :destroy_many]
  before_action :user_can_restore?, only: [:restore, :restore_many]
  before_action :user_can_view_deleted?, only: [:index, :destroy, :destroy_many]
  before_action :ensure_deleted_tickets, only: [:destroy, :destroy_many]

  ## ### List deleted tickets
  ## `GET /api/v2/deleted_tickets.json`
  ##
  ## Returns a maximum of 100 deleted tickets per page. See [Pagination](./introduction#pagination).
  ##
  ## The results includes all deleted (and not yet archived) tickets that
  ## have not yet been [scrubbed](https://help.zendesk.com/hc/en-us/articles/360000976667#topic_fv5_w51_sdb) in the past 30 days. Archived tickets are
  ## not included in the results. See [About archived tickets](https://support.zendesk.com/hc/en-us/articles/203657756)
  ## in the Support Help Center.
  ##
  ## The tickets are ordered chronologically by created date, from oldest to newest.
  ## The first ticket listed may not be the oldest ticket in your
  ## account due to [ticket archiving](https://support.zendesk.com/hc/en-us/articles/203657756).
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available parameters
  ##
  ## | Name                  | Type                | Required  | Comments
  ## | --------------------- | --------------------| --------- | -------------------
  ## | `sort_by`             | string              | no        | Possible values are `id`, `subject`, `deleted_at`
  ## | `sort_order`          | string              | no        | One of `asc`, `desc`. Defaults to `asc`
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_tickets.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##  "deleted_tickets": [
  ##    {
  ##      "id": 581,
  ##      "subject": "Wombat Party",
  ##      "actor": {
  ##          "id": 3946,
  ##          "name": "Taz Wombat"
  ##        },
  ##      "deleted_at": "20140704T15:37:04Z",
  ##      "previous_state": "open"
  ##    }
  ##  ],
  ##  "next_page": null,
  ##  "previous_page": null,
  ##  "count": 1
  ## }
  ## ```
  allow_parameters :index,
    sort_by: Parameters.enum('id', 'subject', 'deleted_at') | Parameters.nil_string,
    sort_order: Parameters.enum('ASC', 'asc', 'DESC', 'desc') | Parameters.nil_string
  require_oauth_scopes :index, scopes: %i[tickets:read read], arturo: true
  def index
    render json: deleted_ticket_presenter.present(tickets)
  end

  ## ### Restore a previously deleted ticket
  ## `PUT /api/v2/deleted_tickets/{id}/restore.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_tickets/{id}/restore.json -X PUT -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :restore, id: Parameters.bigid
  require_oauth_scopes :restore, scopes: %i[tickets:write write], arturo: true
  def restore
    ticket.soft_undelete!(current_user)
    head :ok
  end

  ## ### Restore previously deleted tickets in bulk
  ## `PUT /api/v2/deleted_tickets/restore_many?ids={ids}`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_tickets/restore_many?ids={ids} -X PUT -v -u {email_address}:{password}
  ## ```
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :restore_many, ids: Parameters.ids
  require_oauth_scopes :restore_many, scopes: %i[tickets:write write], arturo: true
  def restore_many
    if many_ids?
      many_tickets.each { |ticket| ticket.soft_undelete!(current_user) }
      head :ok
    else
      head :unprocessable_entity
    end
  end

  ## ### Delete ticket permanently
  ##
  ## `DELETE /api/v2/deleted_tickets/{id}.json`
  ##
  ## Permanently deletes a soft-deleted ticket. See [Soft delete](https://help.zendesk.com/hc/en-us/articles/360000586767#topic_zrm_wbj_1db)
  ## in the Zendesk GDPR docs. To soft delete a ticket, use the [Delete Ticket](#delete-ticket) endpoint.
  ##
  ## This endpoint enqueues a ticket deletion job and returns a payload with the jobs status.
  ##
  ## If the job succeeds, the ticket is permanently deleted. This operation can't be undone.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_tickets/{id}.json \
  ##   -X DELETE -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_status" : {
  ##      "status" : "queued",
  ##      "progress" : null,
  ##      "url" : "https://example.zendesk.com/api/v2/job_statuses/d4239ba02d5f0132c0b2600308a8421a.json",
  ##      "id" : "d4239ba02d5f0132c0b2600308a8421a",
  ##      "total" : null,
  ##      "results" : null,
  ##      "message" : null
  ##   }
  ## }
  ## ```
  allow_parameters :destroy, id: Parameters.bigid, allow_undeleted_tickets: Parameters.string
  require_oauth_scopes :destroy, scopes: %i[tickets:write write], arturo: true
  def destroy
    deletion_job = enqueue_ticket_deletion_job([*ticket.id])
    render json: job_status_presenter.present(deletion_job)
  end

  ## ### Delete multiple tickets permanently
  ##
  ## `DELETE /api/v2/deleted_tickets/destroy_many?ids={ids}`
  ##
  ## Permanently deletes up to 100 soft-deleted tickets. See [Soft delete](https://help.zendesk.com/hc/en-us/articles/360000586767#topic_zrm_wbj_1db)
  ## in the Zendesk GDPR docs. To soft delete tickets, use the [Bulk Delete Tickets](#bulk-delete-tickets) endpoint.
  ##
  ## This endpoint accepts a comma-separated list of up to 100 ticket ids. It enqueues
  ## a ticket deletion job and returns a payload with the jobs status.
  ##
  ## If one ticket fails to be deleted, the endpoint still attempts to delete the others. If the job succeeds,
  ## the tickets that were successfully deleted are permanently deleted. This operation can't be undone.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/deleted_tickets/destroy_many.json?ids=1,2,3 \
  ##   -X DELETE -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_status" : {
  ##      "status" : "queued",
  ##      "progress" : null,
  ##      "url" : "https://example.zendesk.com/api/v2/job_statuses/d4239ba02d5f0132c0b2600308a8421a.json",
  ##      "id" : "d4239ba02d5f0132c0b2600308a8421a",
  ##      "total" : null,
  ##      "results" : null,
  ##      "message" : null
  ##   }
  ## }
  ##
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids, allow_undeleted_tickets: Parameters.string
  require_oauth_scopes :destroy_many, scopes: %i[tickets:write write], arturo: true
  def destroy_many
    if many_ids?
      deletion_job = enqueue_ticket_deletion_job(many_tickets.map(&:id))
      render json: job_status_presenter.present(deletion_job)
    else
      head :unprocessable_entity
    end
  end

  private

  def ticket
    @ticket ||= Ticket.with_deleted do
      if current_account.has_remove_soft_deletion_requirement? && params[:allow_undeleted_tickets] == "unsafe"
        current_account.tickets.find_by_nice_id!(params[:id])
      else
        current_account.deleted_tickets.find_by_nice_id!(params[:id])
      end
    end
  end

  def many_tickets
    @many_tickets ||= Ticket.with_deleted do
      if current_account.has_remove_soft_deletion_requirement? && params[:allow_undeleted_tickets] == "unsafe"
        current_account.tickets.where(nice_id: many_ids).all_with_archived
      else
        current_account.deleted_tickets.where(nice_id: many_ids).all_with_archived
      end
    end
  end

  def tickets
    @tickets ||= Ticket.with_deleted do
      paginate(tickets_scope).to_a
    end
  end

  def tickets_scope
    current_account.deleted_tickets
  end

  def enqueue_ticket_deletion_job(ids)
    enqueue_job_with_status(Zendesk::Maintenance::Jobs::TicketDeletionJob, tickets_ids: ids)
  end

  def job_status_presenter
    @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def deleted_ticket_presenter
    @deleted_presenter ||= Api::Lotus::DeletedTicketPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def user_can_delete?
    deny_access unless current_user.can?(:delete, Ticket)
  end

  def user_can_restore?
    return deny_access unless current_user.can?(:delete, Ticket)

    if current_account.has_ticket_restore_access_improvement?
      # Can restore if user has access to view deleted tickets.
      return if current_user.can?(:view_deleted, Ticket)

      # Can restore if the user can edit all these tickets.
      tickets_to_restore = many_ids? ? many_tickets : [ticket]
      tickets_to_restore.each do |ticket_to_restore|
        return deny_access unless current_user.can?(:edit, ticket_to_restore)
      end
    end
  end

  def user_can_view_deleted?
    deny_access unless current_user.can?(:view_deleted, Ticket)
  end

  def ensure_deleted_tickets
    if (many_ids? && many_tickets.count < many_ids.uniq.count) || (params[:id] && ticket.nil?)
      unfound_ticket_ids = many_ids? ? (many_ids.uniq - many_tickets.map(&:nice_id)) : params[:id]
      if current_account.has_remove_soft_deletion_requirement? && params[:allow_undeleted_tickets] == "unsafe"
        render status: :not_found, json: { error: "Ticket ids must be valid", unfound_ticket_ids: unfound_ticket_ids }
      else
        render status: :not_found, json: { error: "Ticket ids must be valid and belong to deleted tickets", unfound_ticket_ids: unfound_ticket_ids }
      end
    end
  end

  SORT_LOOKUP = {
    "id" => "nice_id",
    "subject" => "subject",
    "deleted_at" => "updated_at"
  }.freeze
end
