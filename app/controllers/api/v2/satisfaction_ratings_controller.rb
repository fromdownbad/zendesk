class Api::V2::SatisfactionRatingsController < Api::V2::BaseController
  SCORE_PARAMS_ENUM = Parameters.enum(*Satisfaction::Rating::SCORE_MAP.keys)

  require_capability :cursor_pagination_v2_satisfaction_ratings_endpoint, only: :count

  skip_before_action :require_agent!

  before_action :require_admin!, except: [:create]
  before_action :require_requester!, only: [:create]

  ## ### List Satisfaction Ratings
  ## `GET /api/v2/satisfaction_ratings.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/satisfaction_ratings.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Filters
  ##
  ## | Parameter  | Value
  ## | ---------- | -----
  ## | score      | offered, unoffered, received, received\_with\_comment, received\_without\_comment,<br/>good, good\_with\_comment, good\_without\_comment,<br/>bad, bad\_with\_comment, bad\_without\_comment
  ## | start_time | Time of the oldest satisfaction rating, as a [Unix epoch time](https://www.epochconverter.com/)
  ## | end_time   | Time of the most recent satisfaction rating, as a [Unix epoch time](https://www.epochconverter.com/)
  ##
  ## If you specify an unqualified score such as `good`, the results include all the records with and without comments.
  ##
  ## Examples:
  ##
  ## * `/api/v2/satisfaction_ratings.json?score=bad`
  ## * `/api/v2/satisfaction_ratings.json?score=bad&start_time=1498151194`
  ## * `/api/v2/satisfaction_ratings.json?start_time=1340384793&end_time=1371920793`
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "satisfaction_ratings": [
  ##     {
  ##       "id":              35436,
  ##       "url":             "https://company.zendesk.com/api/v2/satisfaction_ratings/35436.json",
  ##       "assignee_id":     135,
  ##       "group_id":        44,
  ##       "requester_id":    7881,
  ##       "ticket_id":       208,
  ##       "score":           "good",
  ##       "updated_at":      "2011-07-20T22:55:29Z",
  ##       "created_at":      "2011-07-20T22:55:29Z",
  ##       "comment":         "Awesome support!"
  ##     },
  ##     {
  ##       "id":              120447,
  ##       "created_at":      "2012-02-01T04:31:29Z",
  ##       "updated_at":      "2012-02-02T10:32:59Z",
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  allow_parameters :index,
    score: SCORE_PARAMS_ENUM, use_index: Parameters.string | Parameters.nil,
    start_time: Parameters.integer, end_time: Parameters.integer,
    reason_code: Parameters.array(Parameters.integer)
  allow_cursor_pagination_parameters :index
  allow_cursor_pagination_v2_parameters :index
  require_oauth_scopes :index, scopes: %i[satisfaction_ratings:read read]
  def index
    render json: presenter.present(satisfaction_ratings)
  end

  allow_parameters :count,
    score: SCORE_PARAMS_ENUM, start_time: Parameters.integer,
    end_time: Parameters.integer, reason_code: Parameters.array(Parameters.integer)
  require_oauth_scopes :count, scopes: %i[satisfaction_ratings:read read]
  def count
    render json: { count: record_counter.present, links: { url: request.original_url } }
  end

  allow_parameters :export,
    score: SCORE_PARAMS_ENUM, start_time: Parameters.integer,
    end_time: Parameters.integer, reason_code: Parameters.array(Parameters.integer)
  require_oauth_scopes :export, scopes: %i[satisfaction_ratings:read read write]
  def export
    render json: { export: { status: exported_csv_status } }
  end

  # deprecated: use score=received
  allow_parameters :received, {}
  require_oauth_scopes :received, scopes: %i[satisfaction_ratings:read read write]
  def received
    params[:score] = "received"
    render json: presenter.present(satisfaction_ratings)
  end

  ## ### Show Satisfaction Rating
  ## `GET /api/v2/satisfaction_ratings/{id}.json`
  ##
  ## Returns a specific satisfaction rating. You can get the id from
  ## the [List Satisfaction Ratings](#list-satisfaction-ratings) endpoint.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/satisfaction_ratings/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "satisfaction_rating": {
  ##     "id":              35436,
  ##     "url":             "https://company.zendesk.com/api/v2/satisfaction_ratings/35436.json",
  ##     "assignee_id":     135,
  ##     "group_id":        44,
  ##     "requester_id":    7881,
  ##     "ticket_id":       208,
  ##     "score":           "good",
  ##     "updated_at":      "2011-07-20T22:55:29Z",
  ##     "created_at":      "2011-07-20T22:55:29Z",
  ##     "comment":         { ... }
  ##   }
  ## }
  ## ```
  require_oauth_scopes :show, scopes: %i[satisfaction_ratings:read read]
  resource_action :show

  ## ### Create a Satisfaction Rating
  ## `POST /api/v2/tickets/{ticket_id}/satisfaction_rating.json`
  ##
  ## Creates a CSAT rating for a solved ticket, or for a ticket that was previously
  ## solved and then reopened.
  ##
  ## Only the end user listed as the ticket requester can create a satisfaction rating for the ticket.
  ##
  ## #### Allowed For
  ##
  ##  * End user who requested the ticket
  ##
  ## The end user must be a verified user.
  ##
  ## #### Available parameters
  ##
  ## | Name                  | Type                | Required  | Comments
  ## | --------------------- | --------------------| --------- | -------------------
  ## | `sort_order`          | string              | no        | One of `asc`, `desc`. Defaults to `asc`
  ##
  ## #### Using curl
  ##
  ## Good rating
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/satisfaction_rating.json \
  ##   -X POST -d '{"satisfaction_rating": {"score": "good", "comment": "Awesome support."}}' \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## Bad rating
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/satisfaction_rating.json \
  ##   -X POST -d '{"satisfaction_rating": {"score": "bad", "comment": "Needed more detail.", "reason_code":100}}' \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "satisfaction_rating": {
  ##     "id":              35436,
  ##     "url":             "https://company.zendesk.com/api/v2/satisfaction_ratings/35436.json",
  ##     "assignee_id":     135,
  ##     "group_id":        44,
  ##     "requester_id":    7881,
  ##     "ticket_id":       208,
  ##     "score":           "good",
  ##     "updated_at":      "2011-07-20T22:55:29Z",
  ##     "created_at":      "2011-07-20T22:55:29Z",
  ##     "comment":         { ... }
  ##   }
  ## }
  ## ```
  allow_parameters :create, ticket_id: Parameters.bigid, satisfaction_rating: {
    score: SCORE_PARAMS_ENUM,
    comment: Parameters.string | Parameters.nil,
    reason_code: Parameters.bigid
  }
  require_oauth_scopes :create, scopes: %i[satisfaction_ratings:write write]
  def create
    Zendesk::Tickets::Initializer.set_rating(ticket, satisfaction_rating_params)
    via_id = help_center_request? ? ViaType.HELPCENTER : ViaType.WEB_SERVICE
    ticket.will_be_saved_by(current_user, via_id: via_id)
    ticket.save!
    render json: presenter.present(ticket.satisfaction_rating)
  end

  protected

  def exported_csv_status
    CsatCsvJob.enqueue(current_account.id, current_user.id, filters)
    :enqueued
  rescue Resque::ThrottledError
    :throttled
  end

  def require_requester!
    deny_access unless ticket.can_rate_satisfaction?(current_user)
  end

  def presenter
    @presenter ||= Api::V2::SatisfactionRatingPresenter.new(current_user, url_builder: self, includes: includes, filters: filters, with_cursor_pagination: with_cursor_pagination?)
  end

  def satisfaction_rating
    @satisfaction_rating ||= scope.find(params[:id])
  end

  def scope
    @scope ||=
      if current_account.has_cursor_pagination_v2_satisfaction_ratings_endpoint?
        Zendesk::SatisfactionRatings::Finder.new(current_account, params).satisfaction_ratings
      else
        begin
          current_scope = current_account.satisfaction_ratings.non_deleted_with_archived
          current_scope = add_score_condition(current_scope)
          current_scope = add_reason_code_condition(current_scope)
          current_scope = add_start_time_condition(current_scope)
          current_scope = add_end_time_condition(current_scope)
          current_scope
        end
      end
  end

  def add_score_condition(current_scope)
    return current_scope unless score = params[:score]
    current_scope.where(score: Satisfaction::Rating::SCORE_MAP.fetch(score))
  end

  def add_reason_code_condition(current_scope)
    return current_scope unless current_account.csat_reason_code_enabled?
    return current_scope unless reason_code = params[:reason_code]
    current_scope.where(reason_code: reason_code)
  end

  def add_start_time_condition(current_scope)
    return current_scope unless start_time = params[:start_time]
    current_scope.newer_than(Time.at(start_time))
  end

  def add_end_time_condition(current_scope)
    return current_scope unless end_time = params[:end_time]
    current_scope.older_than(Time.at(end_time))
  end

  def satisfaction_ratings
    @satisfaction_ratings ||= begin
      if use_cursor_pagination_v2?
        paginate_with_cursor(sort_ratings_for_cursor(scope))
      elsif with_cursor_pagination?
        paginated = scope.paginate_with_cursor(params)

        if params[:use_index] && params[:use_index] =~ /\A[a-z_]+\z/
          paginated = scope.from("satisfaction_ratings use index (#{params[:use_index]})")
        end

        paginated
      else
        # Avoid having that SQL query
        # ORDER BY created_at ASC, id ASC, created_at ASC, id ASC,
        # satisfaction_ratings.created_at DESC, satisfaction_ratings.id DESC
        # Default order is sadly created_at ASC, id ASC...
        order = params[:sort_order].try(:downcase) == "desc" ? "DESC" : "ASC"
        table_name = "satisfaction_ratings"

        options = {}
        options[:order] = "#{table_name}.created_at #{order}, #{table_name}.id #{order}"

        paginate_with_large_count_cache(scope, options)
      end
    end
  end

  def use_cursor_pagination_v2?
    # Remove this conditional after we rollout 'cursor_pagination_v2_satisfaction_ratings_endpoint'.
    #
    # If we receive a '?page[:size]=2' request before we enable the
    # feature, we want to show the same error message we were showing
    # before adding the new parameter types.
    if !current_account.has_cursor_pagination_v2_satisfaction_ratings_endpoint? &&
          HashParam.ish?(params[:page])
      raise StrongerParameters::InvalidParameter.new(
        StrongerParameters::IntegerConstraint.new.value(params[:page]),
        'page'
      )
    end

    with_cursor_pagination_v2? && current_account.has_cursor_pagination_v2_satisfaction_ratings_endpoint?
  end

  # For Cursor Based Pagination we support:
  #   * 'created_at'
  #   * '-created_at' (default)
  def sort_ratings_for_cursor(scope)
    sort = params[:sort].to_s
    order = {}

    case sort
    when 'created_at'
      order = { created_at: :asc, id: :asc }
    when '', '-created_at'
      order = { created_at: :desc, id: :desc }
    else
      raise_invalid_sort_for_cursor
    end

    scope.use_index(Satisfaction::Rating::ACCOUNT_AND_CREATED_INDEX).reorder(order)
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  def filters
    params.slice('score', 'start_time', 'end_time', 'reason_code').tap do |f|
      f['start_time'] = Time.at(f['start_time']) if f['start_time'].present?
      f['end_time'] = Time.at(f['end_time']) if f['end_time'].present?
    end
  end

  SORT_LOOKUP = {
    "created_at" => "created_at",
    "updated_at" => "updated_at"
  }.freeze

  def record_counter
    @record_counter ||= Zendesk::RecordCounter::SatisfactionRatings.new(account: current_account)
  end

  private

  def help_center_request?
    request.headers['User-Agent'] == 'HelpCenter'
  end

  def satisfaction_rating_params
    permitted = [:score, :comment]
    permitted.push(:reason_code) if current_account.csat_reason_code_enabled?
    params.require(:satisfaction_rating).permit(*permitted)
  end
end
