class Api::V2::AttachmentsController < Api::V2::BaseController
  before_action :require_agent!

  require_that_user_can! :view,   :attachment, only: [:show]
  require_that_user_can! :delete, :attachment, only: [:destroy]

  ## ### Show Attachment
  ## `GET /api/v2/attachments/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/attachments/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "attachment": {
  ##     "id":           498483,
  ##     "name":         "myfile.dat",
  ##     "content_url":  "https://company.zendesk.com/attachments/myfile.dat",
  ##     "content_type": "application/binary",
  ##     "size":         2532,
  ##     "thumbnails":   [],
  ##     "url":          "https://company.zendesk.com/api/v2/attachments/498483.json",
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.bigid
  def show
    if current_account.has_sse_agent_attachment_view_policy_log_only? &&
        !current_user.can?(:view, attachment&.source&.ticket)
      Rails.logger.info '[Agent attachment policy API] API request would be blocked. Agent cannot view ticket.'
      statsd_client.increment('agent_policy_change.api_request_denied')
    end

    if current_account.has_sse_agent_attachment_view_policy?
      return deny_access unless current_user.can?(:view, attachment&.source&.ticket)
    end

    if attachment.redacted?
      head :gone
    else
      render json: presenter.present(attachment)
    end
  end

  # h3 Delete Attachment
  # `DELETE /api/v2/attachments/{id}.json`
  #
  # Currently, only attachments on forum posts are allowed to be deleted.
  #
  # h4 Allowed For
  #
  #  * Agents
  #
  # h4 Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/attachments/{id}.json \
  #   -v -u {email_address}:{password} -X DELETE
  # ```
  #
  # h4 Example Response
  #
  # ```http
  # Status: 204 No Content
  # ```
  resource_action :destroy

  private

  def attachment
    @attachment ||= begin
      conditions = { account_id: current_account.id, parent_id: nil }
      conditions[:source_type] = ['Entry', 'Token'] if params[:action] == "destroy"

      Attachment.where(conditions).find(params[:id])
    end
  end

  def presenter
    @presenter ||= Api::V2::AttachmentPresenter.new(current_user, url_builder: self)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'attachments')
  end
end
