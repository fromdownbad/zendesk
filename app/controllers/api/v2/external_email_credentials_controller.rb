class Api::V2::ExternalEmailCredentialsController < Api::V2::BaseController
  require_that_user_can! :edit, Access::Settings::Extensions # same as ExternalEmailCredentials and Settings::Email controllers

  ## ### Delete External Email Credential
  ## `DELETE /api/v2/external_email_credentials/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/external_email_credentials/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  allow_parameters :destroy, :skip # TODO: make stricter
  def destroy
    if external_email_credential.destroy
      default_delete_response
    else
      presenter = Api::V2::RecipientAddressPresenter.new(current_user, url_builder: self)
      render json: presenter.present_errors(external_email_credential.recipient_address), status: :unprocessable_entity
    end
  end

  protected

  def external_email_credential
    @external_email_credential ||= current_account.external_email_credentials.find(params[:id])
  end
end
