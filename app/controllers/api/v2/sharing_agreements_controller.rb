module Api
  module V2
    class SharingAgreementsController < Api::V2::BaseController
      allow_subsystem_user :gooddata, only: [:index]
      allow_subsystem_user :collaboration, only: [:index]
      allow_bime_user only: [:index]
      before_action :require_admin!, except: [:index, :show]

      ## ### Show a Sharing Agreement
      ## `GET /api/v2/sharing_agreements/{id}.json`
      ##
      ##  Returns a sharing agreement for your account.
      ##
      ## #### Allowed for
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/sharing_agreements/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "sharing_agreement": {
      ##     "id":    1,
      ##     "name":  "Foo @ Zendesk",
      ##     "type":  "inbound",
      ##     ...
      ##   }
      ## }
      ## ```
      resource_action :show

      ## ### List Sharing Agreements
      ## `GET /api/v2/sharing_agreements.json`
      ##
      ## #### Allowed For:
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/sharing_agreements.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "sharing_agreements": [
      ##     {
      ##       "id":    1,
      ##       "name":  "Foo @ Zendesk",
      ##       "type":  "inbound",
      ##       ...
      ##      },
      ##      ...
      ##   ]
      ## }
      ## ```
      allow_parameters :index, {}
      def index
        presenter.remote_accounts = Sharing::Agreement.remote_accounts_for(agreements)
        render json: presenter.present(agreements)
      end

      ## ### Create Sharing Agreement
      ## `POST /api/v2/sharing_agreements.json`
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/sharing_agreements.json \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -d '{"sharing_agreement": {"remote_subdomain": "Foo"}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/sharing_agreements/{id}.json
      ##
      ## {
      ##   "sharing_agreement": {
      ##     "id":    1,
      ##     "name":  "Foo @ Zendesk",
      ##     "type":  "inbound",
      ##     ...
      ##   }
      ## }
      ## ```
      allow_parameters :create, sharing_agreement: {
        remote_subdomain: Parameters.string,
        sync_tags: Parameters.boolean,
        sync_custom_fields: Parameters.boolean,
        allows_public_comments: Parameters.boolean
      }
      def create
        if params[:sharing_agreement]
          params[:sharing_agreement][:subdomain] = params[:sharing_agreement].delete(:remote_subdomain)
        end
        new_sharing_agreement.attributes = (params[:sharing_agreement] || {})
        new_sharing_agreement.save!
        render json: presenter.present(new_sharing_agreement), status: :created, location: presenter.url(new_sharing_agreement)
      end

      ## ### Update a Sharing Agreement
      ## `PUT /api/v2/sharing_agreements/{id}.json`
      ##
      ##  Returns an updated sharing agreement. Only Status is allowed to be updated.
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/sharing_agreements/{id}.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{"sharing_agreement": {"status": "accepted"}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "sharing_agreement": {
      ##     "id":    1,
      ##     "name":  "Foo @ Zendesk",
      ##     "type":  "inbound",
      ##     ...
      ##   }
      ## }
      ## ```
      allow_parameters :update, id: Parameters.bigid, sharing_agreement: {status: Parameters.string}
      def update
        sharing_agreement.update_attributes!(params[:sharing_agreement])
        render json: presenter.present(sharing_agreement)
      end

      ## ### Delete a Sharing Agreement
      ## `DELETE /api/v2/sharing_agreements/{id}.json`
      ##
      ##  Deletes a sharing agreement.
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/sharing_agreements/{id}.json \
      ##   -X DELETE -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      ##
      allow_parameters :destroy, id: Parameters.bigid
      def destroy
        sharing_agreement.destroy
        default_delete_response
      end

      private

      def new_sharing_agreement
        @new_sharing_agreement ||= agreements.new do |agreement|
          agreement.local_admin = current_user
        end
      end

      def sharing_agreement
        @sharing_agreement ||= agreements.find(params[:id].to_i).tap do |agreement|
          agreement.local_admin = current_user
        end
      end

      def presenter
        @presenter ||= Api::V2::SharingAgreementPresenter.new(current_user, url_builder: self)
      end

      def agreements
        @agreements ||= current_account.sharing_agreements
      end
    end
  end
end
