module Api
  module V2
    class TargetFailuresController < Api::V2::BaseController
      before_action :require_account_has_allow_agents_to_access_targets!

      ## ### List Target Failures
      ##
      ## `GET /api/v2/target_failures`
      ##
      ## Returns the 25 most recent target failures, per target.
      ##
      ## #### Stability
      ##
      ## * Development
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/target_failures.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "target_failures": [
      ##     {
      ##       "id":   1,
      ##       "target_name": "My URL Target",
      ##       "status_code": 401
      ##       ...
      ##     },
      ##     {
      ##       "id":   2,
      ##       "target_name": "My URL Target",
      ##       "status_code": 401
      ##       ...
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, {}
      def index
        render json: presenter.present(target_failures)
      end

      ## ### Show Target Failure
      ##
      ## `GET /api/v2/target_failures/{id}`
      ##
      ## #### Stability
      ##
      ## * Development
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/target_failures/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "target_failure": {
      ##     "id":   1,
      ##     "target_name": "My URL Target",
      ##     "status_code": 401,
      ##     "raw_request": "GET /api/v2/tickets.json HTTP/1.1\r\nUser-Agent: Zendesk Target\r\n ...",
      ##     "raw_response": "HTTP/1.1 401 Unauthorized\r\nServer: nginx\r\n ..."
      ##     ...
      ##   }
      ## }
      ## ```
      allow_parameters :show, id: Parameters.bigid
      def show
        render json: presenter.present(target_failure)
      end

      private

      def presenter
        @presenter ||= Api::V2::TargetFailurePresenter.new(current_user, url_builder: self, action: params[:action])
      end

      def target_failures
        @target_failures ||= paginate(current_account.target_failures.order(created_at: :desc))
      end

      def target_failure
        @target_failure ||= current_account.target_failures.find(params[:id])
      end

      def require_account_has_allow_agents_to_access_targets!
        unless current_account.has_allow_agents_to_access_targets?
          deny_access unless current_user.is_admin? || current_user.is_system_user?
        end
      end
    end
  end
end
