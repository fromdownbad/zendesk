class Api::V2::GooddataIntegrationController < Api::V2::BaseController
  before_action :require_admin!, only: [:create, :update]
  before_action :require_agent!, only: [:show, :version, :re_enable]

  allow_only_subsystem_user :gooddata, only: [:upgrade]

  ## ### Create a new Gooddata integration for this account
  ## `POST /api/v2/gooddata_integration.json
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/gooddata_integration.json \
  ##   -H "Content-Type: application/json" -d "{}"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 202 Accepted
  ## ```
  ##
  allow_parameters :create, {}
  def create
    GooddataIntegrationsJob.enqueue(current_account.id, current_user.id)

    head :accepted
  end

  ## ### Re-enable an existing Gooddata integration that has been disabled
  ## `POST /api/v2/gooddata_integration/re_enable.json
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com//api/v2/gooddata_integration/re_enable.json \
  ##   -H "Content-Type: application/json" -d "{}"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 202 Accepted
  ## ```
  ##
  allow_parameters :re_enable, {}
  require_oauth_scopes :re_enable, scopes: [:write], arturo: true
  def re_enable
    GooddataEnableJob.enqueue(current_account.id, current_user.id)
    head :accepted
  end

  ## ### Show the Gooddata integration for this account
  ## `GET /api/v2/gooddata_integration.json
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/gooddata_integration.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ##   "gooddata_integration":  {
  ##     "id":            '1234'
  ##     "project_id":    'kerjekr3434',
  ##     "status":        'created',
  ##     "admin_id":      '3434883',
  ##     ...
  ##     }
  ## }
  ## ```
  allow_parameters :show, {}
  def show
    render json: presenter.present(gooddata_integration)
  end

  ## ### Update the Gooddata integration for this account
  ## `PUT /api/v2/gooddata_integration.json
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/gooddata_integration.json \
  ##   -d '{"gooddata_integration": {"scheduled_at": "5pm"}}' \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ##   "gooddata_integration":  {
  ##     "id":            '1234',
  ##     "project_id":    'kerjekr3434',
  ##     "status":        'created',
  ##     "admin_id":      '3434883',
  ##     "scheduled_at":  '5pm',
  ##     ...
  ##   }
  ## ```
  allow_parameters :update, gooddata_integration: {scheduled_at: Parameters.string}
  def update
    gooddata_integration.update_attributes!(params[:gooddata_integration])

    render json: presenter.present(gooddata_integration)
  end

  allow_parameters :version, :skip # TODO: make stricter
  require_oauth_scopes :version, scopes: [:read], arturo: true
  def version
    version = if current_integration.present?
      current_integration.version
    else
      2
    end

    render json: { version: version }
  end

  allow_parameters :last_successful_process, :skip # TODO: make stricter
  require_oauth_scopes :last_successful_process, scopes: [:read], arturo: true
  def last_successful_process
    process = gooddata_integration.last_successful_process

    render json: { last_successful_process: process }
  end

  protected

  def presenter
    Api::V2::GooddataIntegrationPresenter.new(current_user, url_builder: self)
  end

  def current_integration
    @current_integration ||= current_account.gooddata_integration
  end

  def gooddata_integration
    current_integration || raise(ActiveRecord::RecordNotFound)
  end
end
