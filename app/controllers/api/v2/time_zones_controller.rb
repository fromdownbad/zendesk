class Api::V2::TimeZonesController < Api::V2::BaseController
  skip_before_action :prevent_missing_account_access,
    :prevent_inactive_account_access, :require_agent!,
    :authenticate_user, :verify_authenticated_session

  after_action :disable_session_set_cookie

  ## ### List Time Zones
  ## `GET /api/v2/time_zones.json`
  ##
  ## List all time zones, along with their *current* offset, in minutes.
  ## For *full* offset information, including historical and future
  ## daylight-savings changes, use `GET /api/v2/time_zones/{id}.json`.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://assets.zendesk.com/api/v2/time_zones.json
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "time_zones": [
  ##     {
  ##       "name":   "Europe/Copenhagen",
  ##       "url": "https://assets.zendesk.com/api/v2/time_zones/Europe%2FCopenhagen.json",
  ##       "offset": 60,
  ##       "formatted_offset": "GMT+01:00"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index, moment: Parameters.boolean
  def index
    if stale?(etag: [time_zones, I18n.locale])
      json = presenter.present(time_zones)
      expires_in 7.days, public: true
      render json: json
    end
  end

  ## ### Show Timezone
  ## `GET /api/v2/timezones/{id}.json`
  ##
  ## Show detailed timezone information for a single zone. The response will
  ## include the timezone's identifier and a full list of daylight-savings
  ## transition data. Each transition includes the timestamp at which it
  ## began (e.g. 1143961200 for 2006-04-02 07:00:00 UTC), the offset
  ## in seconds, whether the transition starts a daylight-savings period or
  ## not, and an abbreviation for the timezone during that period.
  ##
  ## Note that timezone identifiers, like all URL segments, must be
  ## URL-encoded. Thus, "America/New_York" is at
  ## `/api/v2/time_zones/America%2FNew_York.json`.
  ##
  ## #### Allowed For
  ##
  ##  * Anyone
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://assets.zendesk.com/api/v2/timezones/America%2FNew_York.json
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "timezone": {
  ##     "id": "America/New_York",
  ##     "url": "https://assets.zendesk.com/api/v2/timezones/America%2FNew_York.json",
  ##     "transitions": [
  ##       ...,
  ##       { "at": 1143961200, "dst": true, "offset": -14400, "abbreviation": "EDT" }
  ##       ...
  ##     ]
  ##   }
  ## }
  allow_parameters :show,
    id: Parameters.string,
    moment: Parameters.boolean
  def show
    (time_zone = ActiveSupport::TimeZone[params[:id]]) || raise(ActiveRecord::RecordNotFound)
    if stale?(etag: [time_zone, I18n.locale])
      json = presenter.present(time_zone)
      expires_in 7.days, public: true
      render json: json
    end
  end

  private

  def options
    { url_builder: self, moment: params[:moment] }
  end

  def time_zones
    ActiveSupport::TimeZone.all_sorted
  end

  def presenter
    Api::V2::TimeZonePresenter.new(current_user, options)
  end
end
