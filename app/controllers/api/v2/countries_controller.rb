class Api::V2::CountriesController < Api::V2::BaseController
  include CountryHelper

  skip_before_action :require_agent!
  skip_before_action :authenticate_user

  ## ### List Countries and Calling Codes
  ## `GET /api/v2/countries.json`
  ##
  ## #### Allowed For
  ##
  ##  * Anyone
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/countries.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "countries": [
  ##     {
  ##       "id": 1,
  ##       "name": "Afghanistan",
  ##       "code": "AF",
  ##       "region": "APAC",
  ##       "calling_code": "93"
  ##     },
  ##     {
  ##       "id": 2,
  ##       "name": "Albania",
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(sort_countries(Country.all, I18n.locale.to_s))
  end

  ## ### Show Country
  ## `GET /api/v2/countries/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Anyone
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/countries/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "country": {
  ##      "id": 1,
  ##      "name": "Afghanistan",
  ##      "code": "AF",
  ##      "region": "APAC",
  ##      "calling_code": "93"
  ##    }
  ## }
  ## ```
  resource_action :show

  private

  def country
    Country.find(params[:id])
  end

  def presenter
    @presenter ||= Api::V2::CountryPresenter.new(current_user, url_builder: self)
  end
end
