module Api
  module V2
    class SharedTicketsController < Api::V2::BaseController
      ## ### List Shared Tickets
      ## `GET /api/v2/sharing_agreements/{id}/shared_tickets.json`
      ##
      ## #### Allowed For:
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/sharing_agreements/{id}/shared_tickets.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "shared_tickets": [
      ##     {
      ##       "local_ticket_id":  2342,
      ##       "remote_ticket_id":  743,
      ##       ...
      ##      },
      ##     ...
      ##   ]
      ## }
      ## ```
      allow_parameters :index, sharing_agreement_id: Parameters.bigid
      def index
        render json: presenter.present(shared_tickets)
      end

      private

      def max_per_page
        1000
      end

      def presenter
        @presenter ||= Api::V2::SharedTicketPresenter.new(current_user, url_builder: self, agreement: agreement)
      end

      def shared_tickets
        paginate(agreement.shared_tickets.includes(:ticket))
      end

      def agreement
        @agreement ||= current_account.sharing_agreements.find(params[:sharing_agreement_id])
      end
    end
  end
end
