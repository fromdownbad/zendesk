class Api::V2::FeatureUsageMetricsController < Api::V2::BaseController
  before_action :require_agent!

  ## ### Listing Feature Usage Metrics
  ## `GET /api/v2/feature_usage_metrics.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/feature_usage_metrics.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macros": {
  ##     "count": 33,
  ##     "updated_past_day_count": 2,
  ##     "used_past_day_count": 3,
  ##     "most_used_past_week": [
  ##       {"id": 444, "name": "Blah", ...}
  ##       ...
  ##     ],
  ##     "updated_recently: [
  ##       {"id": 444, "name": "Blah", ...}
  ##       ...
  ##     ],
  ##   },
  ##   "automations": {
  ##     ...
  ##   },
  ##   "triggers": {
  ##     ...
  ##   },
  ##   "views": {
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(current_account)
  end

  protected

  def presenter
    @presenter ||= begin
      Api::V2::FeatureUsageMetricsPresenter.new(current_user, url_builder: self)
    end
  end
end
