module Api::V2
  class SkipsController < Api::V2::BaseController
    VALID_ORDER   = %w[asc desc].freeze
    DEFAULT_ORDER = 'asc'.freeze

    before_action :verify_can_view, only: :index

    ## ### Record a new skip for the current user
    ## `POST /api/v2/skips.json`
    ##
    ## #### Allowed For:
    ##
    ##  * Agents
    ##
    ## #### Using curl:
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/skips.json \
    ##   -v -u {email_address}:{password} \
    ##   -H "Content-Type: application/json" -X POST \
    ##   -d '{"skip": {"ticket_id": 123, "reason": "I have no idea."}}'
    ## ```
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 201 Created
    ##
    ## {
    ##   "skip": {
    ##     "id": 1,
    ##     "ticket_id": 123,
    ##     "user_id": 456,
    ##     "ticket": { ... },
    ##     "reason": "I have no idea.",
    ##     "created_at": "2015-09-30T21:44:03Z",
    ##     "updated_at": "2015-09-30T21:44:03Z"
    ##   }
    ## }
    ## ```
    allow_parameters :create, skip: {
      ticket_id: Parameters.bigid,
      reason:    Parameters.string
    }
    def create
      render json: presenter.present(new_skip), status: :created
    end

    ## ### List skips for the current account
    ## `GET /api/v2/skips.json`
    ##
    ## `GET /api/v2/tickets/{ticket_id}/skips.json`
    ##
    ## `GET /api/v2/users/{user_id}/skips.json`
    ##
    ## Archived tickets are not included in the response. See
    ## [About archived tickets](https://support.zendesk.com/hc/en-us/articles/203657756) in
    ## the Support Help Center.
    ##
    ## #### Allowed For:
    ##
    ##  * Agents with read-only or higher report access
    ##  * Agents retrieving their own skips
    ##
    ## #### Available Parameters
    ##
    ## | Name       | Type    | Required | Comments
    ## | ---------  | ------- | -------- | --------
    ## | sort_order | string  | no       | One of `asc` or `desc`. Defaults to `asc`
    ## | ticket_id  | integer | no       | ID of a ticket
    ## | user_id    | integer | no       | User ID of an agent
    ##
    ## #### Using curl:
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/v2/skips.json \
    ##   -v -u {email_address}:{password}
    ## ```
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "skips": [
    ##     {
    ##       "id": 1,
    ##       "ticket_id": 123,
    ##       "user_id": 456,
    ##       "ticket": { ... },
    ##       "reason": "I have no idea.",
    ##       "created_at": "2015-09-30T21:44:03Z",
    ##       "updated_at": "2015-09-30T21:44:03Z"
    ##     },
    ##     {
    ##       "id": 2,
    ##       "ticket_id": 321,
    ##       "user_id": 654,
    ##       "ticket": { ... },
    ##       "reason": "I'm lost.",
    ##       "created_at": "2015-10-01T21:44:03Z",
    ##       "updated_at": "2015-10-01T21:44:03Z"
    ##     },
    ##     ...
    ##   ]
    ## }
    ## ```
    allow_parameters :index,
      sort_order: Parameters.string,
      ticket_id:  Parameters.integer,
      user_id:    Parameters.integer
    def index
      render json: presenter.present(paginate_with_large_count_cache(skips))
    end

    private

    def create_params
      params.
        require(:skip).
        merge(ticket_id: ticket.id, user_id: current_user.id)
    end

    def ticket
      @ticket ||=
        current_account.tickets.find_by_nice_id!(params[:skip][:ticket_id])
    end

    def new_skip
      current_account.skips.create(create_params)
    end

    def skips
      @skips ||= begin
        scope.
          skips.
          joins(:ticket).
          where(tickets: {status_id: Ticket::ACCESSIBLE_STATUSES}).
          order("skips.created_at #{sort_order}")
      end
    end

    def scope
      if params[:user_id].present?
        current_account.users.find(params[:user_id])
      elsif params[:ticket_id].present?
        current_account.tickets.find_by_nice_id!(params[:ticket_id])
      else
        current_account
      end
    end

    def verify_can_view
      deny_access unless current_user.can?(:view, Report) || self_reflection?
    end

    def self_reflection?
      params[:user_id].present? && current_user.id == params[:user_id].to_i
    end

    def sort_order
      VALID_ORDER.find do |order|
        order == params[:sort_order].try(:downcase)
      end || DEFAULT_ORDER
    end

    def presenter
      @presenter ||= SkipPresenter.new(current_user, url_builder: self)
    end
  end
end
