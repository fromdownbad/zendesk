class Api::V2::CurrentUserController < Api::V2::BaseController
  include RadarHelper

  skip_before_action :require_agent!
  skip_before_action :enforce_support_is_active!
  allow_anonymous_users

  allow_cors_from_zendesk_external_domains :show
  after_action :strip_session_response_cookie, only: :radar_token

  ## ### Show the Current User
  ## `GET /api/v2/users/me.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Anonymous users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/me.json \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "user": {
  ##     "id":   35436,
  ##     "name": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :show, {}
  def show
    return head :unauthorized if restrict_unauthenticated_user?

    response.headers["Access-Control-Expose-Headers"] = 'X-Zendesk-API-Warn'
    response.headers['Vary'] = 'origin'

    if !params[:include].present? && app_request? && current_account.has_current_user_show_cache_control_header?
      # Set Cache-Control:max-age=60,private to cut off excessive traffic from apps neither user nor us can control.
      # Until we have capacity and performance for apps review we have this guard and it saved us from incidents, example:
      # https://zendesk.atlassian.net/wiki/spaces/CP/pages/481558838/2018-04-05+-+Top1+consumer+of+Views+API
      # We used to have all requests cached privately at rest (in browser), but recent analysis shows that
      # customers uses this endpoint by various different ways, so only safe source to cache is from the apps.
      expires_in(60.seconds)

      # Render presenter only if HTTP client has a stale response
      render json: presenter.present(current_user) if stale?(etag: current_user.cache_key)
    else
      render json: presenter.present(current_user)
    end
  end

  # Pubsub authentication token renewal
  # Note: Internal API (no double hash characters intentional)
  #
  # `GET /api/v2/users/radar_token.json`
  #
  # Pubsub tokens expire after a certain time, and when they do,
  # the app needs to be able to fetch a new token for the authenticated user.
  allow_parameters :radar_token, _: Parameters.bigid
  require_oauth_scopes :radar_token, scopes: [:write], arturo: true
  def radar_token
    conf = radar_configuration
    # Time left before expiration (with a tiny buffer)
    expiry = conf.delete('expires_at') - Time.now.utc.to_i - 10
    expiry = (expiry / 60).floor # in minutes

    if expiry > 0
      expires_in expiry.minutes, 'private' => true, 'must-revalidate' => true
    end

    Rails.logger.info("Radar token: #{conf}, expiry: #{expiry}, now: #{Time.now}")
    statsd_client.increment('radar_token_api', tags: ["cluster:#{radar_cluster_id}"])

    render json: conf
  end

  protected

  def radar_cluster_id
    Zendesk::Radar::Configuration.cluster_id_for_shard(current_account.shard_id)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['classic', 'radar'])
  end

  def presenter
    @presenter ||= Api::V2::Users::Presenter.new(current_user,
      url_builder: self,
      includes: includes,
      include_authenticity_token: true,
      shared_session: shared_session)
  end

  def restrict_unauthenticated_user?
    current_user.id.nil? && current_account.has_restrict_unauthenticated_user?
  end

  def strip_session_response_cookie
    disable_session_set_cookie if current_account.has_v2_users_radar_token_strip_session_set_cookie?
  end
end
