class Api::V2::Incremental::OrganizationsController < Api::V2::Exports::BaseController
  allow_gooddata_user
  allow_bime_user

  ## ### Incremental Organization Export
  ## `GET /api/v2/incremental/organizations.json?start_time={unix_time}`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Sideloading
  ##
  ## See [Organizations sideloads](./side_loading#supported-endpoints).
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/organizations.json?start_time=1332034771 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## See [Organizations](./organizations) for a detailed example.
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "organizations": [
  ##     {
  ##       "url": "https://{subdomain}.zendesk.com/api/v2/organizations/11.json",
  ##       "id": 11,
  ##       "name": "Dev",
  ##       "tags": ['awesome_customer'],
  ##       ....
  ##       "organization_fields": {
  ##         "numeric_field": 12345,
  ##       }
  ##     }
  ##   ]
  ##   "next_page": "https://{subdomain}.zendesk.com/api/v2/incremental/organizations.json?start_time=1383685952",
  ##   "count": 1,
  ##   "end_of_stream": true,
  ##   "end_time": 1383685952
  ## }
  ## ```
  ##
  allow_parameters :index, start_time: Parameters.integer & Parameters.gte(0)
  def index
    # GoodData takes times in local account time, for now
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        render json: Yajl::Encoder.encode(instrument_export_with_count(:organizations) { updated_organizations(EXPORT_LIMIT) })
      end
    else
      throttle_export_request_unless_can_by_pass_rate_limits
      render json: Yajl::Encoder.encode(instrument_export_with_count(:organizations) { updated_organizations(EXPORT_LIMIT) })
    end
  end

  allow_parameters :sample, start_time: Parameters.integer & Parameters.gte(0)
  require_oauth_scopes :sample, scopes: [:read], arturo: true
  def sample
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        render json: Yajl::Encoder.encode(updated_organizations(SAMPLE_EXPORT_LIMIT))
      end
    else
      Prop.throttle!(:sample_incremental_exports, current_account.id)
      render json: Yajl::Encoder.encode(updated_organizations(SAMPLE_EXPORT_LIMIT))
    end
  end

  protected

  def updated_organizations(limit)
    included_associations = [:taggings, :custom_fields, :custom_field_values]
    presenter_options = {
      url_builder: self,
      includes: includes,
      no_association_preloads: true,
    }
    presenter_options = presenter_options.merge(exclude_non_exportable_fields: true) if gooddata_request?

    incremental_export = Zendesk::Export::IncrementalExport.new(
      # if we don't put some sort of where clause here it goes back to the default scope
      association: Organization.with_deleted { current_account.organizations.reorder(nil) },
      association_includes: included_associations,
      limit: limit,
      start_time: start_time,

      presenter: Api::V2::Organizations::IncrementalPresenter.new(current_user, presenter_options),

      url_builder: self,
      page_url_method: :api_v2_incremental_organizations_url
    )

    Organization.with_deleted do
      incremental_export.as_json
    end
  end
end
