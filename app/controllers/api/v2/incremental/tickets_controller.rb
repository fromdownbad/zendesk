class Api::V2::Incremental::TicketsController < Api::V2::Exports::BaseController
  skip_before_action :check_valid_start_time!, only: :cursor
  before_action :log_if_start_time_too_recent
  before_action :require_start_time_or_cursor, only: :cursor
  before_action :limit_explore_exports, only: :index

  allow_gooddata_user
  allow_bime_user
  allow_outbound_user

  index_params = {
    start_time: Parameters.integer | Parameters.float,
    exclude_deleted: Parameters.boolean,
    per_page: Parameters.integer & Parameters.gt(0)
  }

  ## ### Incremental Ticket Export
  ## `GET /api/v2/incremental/tickets/cursor.json?start_time={unix_time}`
  ##
  ## `GET /api/v2/incremental/tickets.json?start_time={unix_time}`
  ##
  ## Returns the tickets that changed since the start time. For more information,
  ## see [Exporting tickets](#exporting-tickets).
  ##
  ## This endpoint supports both cursor-based incremental exports and time-based incremental exports.
  ## Cursor-based exports are highly encouraged because they provide more consistent performance and
  ## response body sizes. For more information, see:
  ##
  ## - [Cursor-based incremental exports](#cursor-based-incremental-exports)
  ## - [Time-based incremental exports](#time-based-incremental-exports)
  ##
  ## The results include tickets that were updated by the system. See
  ## [Excluding system-updated tickets](#excluding-system-updated-tickets).
  ##
  ## The endpoint can return tickets with an `updated_at` time that's earlier than the
  ## `start_time` time. The reason is that the API compares the `start_time` with the ticket's
  ## `generated_timestamp` value, not its `updated_at` value. The `updated_at` value is
  ## updated only if the update generates a [ticket event](#incremental-ticket-event-export).
  ## The `generated_timestamp` value is updated for all ticket updates, including system
  ## updates. If a system update occurs after a ticket event, the unchanged
  ## `updated_at` time will become earlier relative to the updated `generated_timestamp`
  ## time.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Sideloading
  ##
  ## See [Tickets sideloads](./side_loading#supported-endpoints). For performance reasons,
  ## last_audits sideloads aren't supported.
  ##
  ## #### Using curl
  ##
  ## **Cursor-based export**
  ##
  ## Initial request:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/tickets/cursor.json?start_time=1332034771 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## Subsequent requests:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/tickets/cursor.json?cursor=MTU3NjYxMzUzOS4wfHw0NTF8 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## **Time-based export**
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/tickets.json?start_time=1332034771 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response - Cursor-based Export
  ##
  ## See [Tickets](./tickets) for a detailed example.
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "after_url": "https://{subdomain}.zendesk.com/api/v2/incremental/tickets/cursor.json?cursor=MTU3NjYxMzUzOS4wfHw0Njd8",
  ##   "before_url": null,
  ##   "after_cursor": "MTU3NjYxMzUzOS4wfHw0Njd8",
  ##   "before_cursor": null,
  ##   "end_of_stream": true,
  ##   "tickets": [
  ##     {
  ##       "url": "https://{subdomain}.zendesk.com/api/v2/tickets/1.json",
  ##       "id": 2,
  ##       "created_at": "2012-02-02T04:31:29Z",
  ##       "generated_timestamp": 1390362285
  ##       ...
  ##      },
  ##      ...
  ##   ]
  ## }
  ## ```
  ##
  ## #### Example Response - Time-based Export
  ##
  ## See [Tickets](./tickets) for a detailed example.
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "url": "https://{subdomain}.zendesk.com/api/v2/tickets/1.json",
  ##       "id": 2,
  ##       "created_at": "2012-02-02T04:31:29Z",
  ##       "generated_timestamp": 1390362285
  ##       ...
  ##      },
  ##      ...
  ##   ],
  ##   "next_page": "https://{subdomain}.zendesk.com/api/v2/incremental/tickets.json?per_page=3&start_time=1390362485",
  ##   "count": 2,
  ##   "end_of_stream": true,
  ##   "end_time": 1390362485
  ## }
  ## ```

  allow_parameters :index, **index_params
  def index
    Ticket.with_deleted do
      result = if gooddata_request?
        Time.use_zone(current_account.time_zone) do
          instrument_export_with_count(:tickets) { exporter(export_limit).as_json }
        end
      else
        throttle_export_request_unless_can_by_pass_rate_limits
        instrument_export_with_count(:tickets) { exporter(export_limit).as_json }
      end

      render json: Yajl::Encoder.encode(result)
    end
  end

  allow_parameters :sample, **index_params
  require_oauth_scopes :sample, scopes: [:read], arturo: true
  def sample
    Ticket.with_deleted do
      if gooddata_request?
        result = Time.use_zone(current_account.time_zone) do
          exporter(SAMPLE_EXPORT_LIMIT).as_json
        end
      else
        Prop.throttle!(:sample_incremental_exports, current_account.id)
        result = exporter(SAMPLE_EXPORT_LIMIT).as_json
      end

      render json: Yajl::Encoder.encode(result)
    end
  end

  allow_parameters :cursor,
    start_time: Parameters.integer | Parameters.float,
    cursor: Parameters.string,
    exclude_deleted: Parameters.boolean,
    per_page: Parameters.integer & Parameters.gt(0)
  def cursor
    Ticket.with_deleted do
      render json: export_via_cursor(export_limit)
    end
  rescue ArgumentError => error
    if error.message.include? 'invalid base64'
      render json: invalid_cursor_error, status: :bad_request
    else
      raise
    end
  end

  protected

  def export_via_cursor(limit)
    instrument_export_with_count(:tickets) { exporter(limit, :cursor).as_json }
  end

  def finder(limit, navigation)
    Zendesk::Export::IncrementalExportArchiveFinder.new(
      export_association: ticket_scope,
      includes: [],
      update_ts_field: 'generated_timestamp',
      limit: limit,
      start_time: start_time,
      navigation: navigation,
      cursor: params[:cursor],
      exclude_deleted: params[:exclude_deleted],
      enforce_last_minute_limit: current_account.has_inc_api_enforce_last_minute_limit?
    )
  end

  def includes
    super - [:last_audits, :audit]
  end

  def presenter(navigation)
    presenter_options = {
      url_builder: self,
      includes: includes,
      redacted_fields: redacted_fields,
      use_raw_ticket_field_entries: true,
      include_voice_insights_fields: true,
      expanded_via_types: expanded_via_types?,
      navigation: navigation,
      cursor: params[:cursor],
      reduced_payload_size: reduced_payload_size?
    }
    presenter_options[:exclude_non_exportable_fields] = true if gooddata_request?

    Api::V2::Exports::IncrementalTicketPresenter.new(current_user, presenter_options)
  end

  def exporter(limit, navigation = :time)
    Zendesk::Export::IncrementalExport.new(
      association: ticket_scope,
      association_includes: [],
      limit: limit,
      start_time: start_time,
      presenter: presenter(navigation),
      url_builder: self,
      page_url_method: :api_v2_incremental_tickets_url,
      update_ts_field: 'generated_timestamp',
      custom_finder: finder(limit, navigation),
      navigation: navigation,
      cursor: params[:cursor],
      exclude_deleted: params[:exclude_deleted],
      enforce_last_minute_limit: current_account.has_inc_api_enforce_last_minute_limit?
    )
  end

  def redacted_fields
    return [:recipient] if gooddata_request?
    []
  end

  def log_if_start_time_too_recent
    time_diff = Time.now - Time.at(start_time)
    if time_diff < 1.minute
      Rails.logger.info("Incremental Tickets endpoint: start_time: #{time_diff} less than 1 min. account_id: #{current_account.id} user_id: #{current_user.id}")

      render json: {
        error: "StartTimeTooRecent",
        description: "start_time must be more than 60 seconds ago"
      }, status: 400 if current_account.has_inc_api_enforce_last_minute_limit?
    end
  end

  def check_user_ip_restrictions
    super unless gooddata_request?
  end

  def ticket_scope
    scope = current_account.tickets

    scope = scope.where('status_id != ?', StatusType.ARCHIVED) unless current_account.has_refactor_cursor_inc_export_query?

    scope
  end

  def expanded_via_types?
    request.user_agent == "Voyager API Client"
  end

  def require_start_time_or_cursor
    if params[:cursor].blank? && params[:start_time].blank?
      raise ActionController::ParameterMissing, 'start_time OR cursor'
    end
  end

  def export_limit
    default_export_limit = current_account.has_reduce_incremental_ticket_results? ? REDUCED_EXPORT_LIMIT : EXPORT_LIMIT
    [params[:per_page], default_export_limit].compact.min
  end

  def invalid_cursor_error
    {
      errors: [{
        error:       'InvalidCursor',
        description: 'The cursor should only be constructed via API responses.'
      }]
    }
  end

  def reduced_payload_size?
    bime_request? && current_account.has_reduce_incremental_ticket_api_payload?
  end

  def limit_explore_exports
    return unless current_account.has_load_limit_explore_exports? && bime_request?
    load_limit_explore_exports(:tickets)
  end
end
