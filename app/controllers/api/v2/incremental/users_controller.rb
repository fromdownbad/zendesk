class Api::V2::Incremental::UsersController < Api::V2::Exports::BaseController
  skip_before_action :enforce_request_format

  allow_gooddata_user
  allow_bime_user
  allow_outbound_user

  ## ### Incremental User Export
  ## `GET /api/v2/incremental/users.json?start_time={unix_time}`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Sideloading
  ##
  ## See [Users sideloads](./side_loading#supported-endpoints).
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/users.json?start_time=1332034771 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## See [Users](./Users) for a detailed example.
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "users": [
  ##     {
  ##       "id": 11,
  ##       "url": "http://{subdomain}.zendesk.com/api/v2/users/11.json",
  ##       "name": "Agent Extraordinaire",
  ##       ...
  ##       "user_fields": {
  ##         "field1": 0,
  ##         "field2": 'value2'
  ##       }
  ##     }
  ##   ]
  ##   "next_page": "http://{subdomain}.zendesk.com/api/v2/incremental/users.json?start_time=1383685952",
  ##   "count": 1,
  ##   "end_of_stream": true,
  ##   "end_time": 1383685952
  ## }
  ## ```

  allow_parameters :index, start_time: Parameters.integer & Parameters.gte(0)
  def index
    # GoodData takes times in local account time, for now
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        render json: Yajl::Encoder.encode(instrument_export_with_count(:users) { updated_users(EXPORT_LIMIT) })
      end
    else
      throttle_export_request_unless_can_by_pass_rate_limits
      render json: Yajl::Encoder.encode(instrument_export_with_count(:users) { updated_users(EXPORT_LIMIT) })
    end
  end

  allow_parameters :sample, start_time: Parameters.integer & Parameters.gte(0)
  require_oauth_scopes :sample, scopes: [:read], arturo: true
  def sample
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        render json: Yajl::Encoder.encode(updated_users(SAMPLE_EXPORT_LIMIT))
      end
    else
      Prop.throttle!(:sample_incremental_exports, current_account.id)
      render json: Yajl::Encoder.encode(updated_users(SAMPLE_EXPORT_LIMIT))
    end
  end

  protected

  def redacted_fields
    return [:email, :phone] if gooddata_request?
    []
  end

  def nil_value_fields
    return [:name] if gooddata_request? && current_account.settings.insights_user_name_removal
    []
  end

  def user_scope
    current_account.all_users.from("`users` force index (index_users_account_id_updated_at)")
  end

  def updated_users(limit)
    included_associations = [{account: [:settings]}]
    presenter_options = {
      url_builder: self,
      includes: includes,
      redacted_fields: redacted_fields,
      nil_value_fields: nil_value_fields,
      include_permanently_deleted: true
    }

    presenter_options = presenter_options.merge(exclude_non_exportable_fields: true) if gooddata_request?

    incremental_export = Zendesk::Export::IncrementalExport.new(
      association: user_scope,
      association_includes: included_associations,
      limit: limit,
      start_time: start_time,

      presenter: Api::V2::Users::AgentPresenter.new(current_user, presenter_options),

      url_builder: self,
      page_url_method: :api_v2_incremental_users_url
    )

    incremental_export.as_json
  end
end
