class Api::V2::Incremental::AutomaticAnswersController < Api::V2::Exports::BaseController
  allow_gooddata_user
  allow_bime_user

  allow_parameters :index, start_time: Parameters.integer & Parameters.gte(0)
  def index
    # GoodData takes times in local account time
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        render json: Yajl::Encoder.encode(instrument_export_with_count(:automatic_answers) { updated_deflections(EXPORT_LIMIT) })
      end
    else
      throttle_export_request_unless_can_by_pass_rate_limits
      render json: Yajl::Encoder.encode(instrument_export_with_count(:automatic_answers) { updated_deflections(EXPORT_LIMIT) })
    end
  end

  allow_parameters :sample, start_time: Parameters.integer & Parameters.gte(0)
  require_oauth_scopes :sample, scopes: [:read], arturo: true
  def sample
    Prop.throttle!(:sample_incremental_exports, current_account.id)
    render json: Yajl::Encoder.encode(updated_deflections(SAMPLE_EXPORT_LIMIT))
  end

  protected

  def updated_deflections(limit)
    presenter_options = {
      url_builder: self
    }

    incremental_export = Zendesk::Export::IncrementalExport.new(
      association: current_account.ticket_deflections,
      limit: limit,
      start_time: start_time,

      presenter: Api::V2::AutomaticAnswerPresenter.new(current_user, presenter_options),

      url_builder: self,
      page_url_method: :api_v2_incremental_automatic_answers_url
    )

    incremental_export.as_json
  end
end
