class Api::V2::Incremental::TicketMetricEventsController < Api::V2::Exports::BaseController
  EXPORT_LIMIT = 10_000

  allow_gooddata_user
  allow_bime_user

  ## ### List ticket metric events
  ## `GET /api/v2/incremental/ticket_metric_events.json?start_time={unix_time}`
  ##
  ## Returns [ticket metric events](#metric-event-types) that occurred on or after the start time.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Request Parameters
  ##
  ##  * start_time: The Unix UTC epoch time of the oldest event you're interested in. Example: 1332034771.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ##  curl https://{subdomain}.zendesk.com/api/v2/incremental/ticket_metric_events.json?start_time=1432155323 \
  ##    -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket_metric_events": [
  ##     {
  ##       "id": 1717,
  ##       "ticket_id": 27,
  ##       "metric": "reply_time",
  ##       "instance_id": 1,
  ##       "type": "activate",
  ##       "time": "2015-05-20T20:55:23Z"
  ##     },
  ##    ...
  ##   ]
  ##   "next_page": "https://{subdomain}.zendesk.com/api/v2/incremental/ticket_metric_events.json?start_time=1432155323",
  ##   "count": 10000,
  ##   "end_time": 1432155323
  ## }
  ## ```

  allow_parameters :index, start_time: Parameters.integer & Parameters.gte(0)
  def index
    handle_request do
      render json: Yajl::Encoder.encode(
        instrument_export_with_count(:ticket_metric_events) do
          Zendesk::Export::IncrementalExport.new(
            presenter:       presenter,
            custom_finder:   finder,
            url_builder:     self,
            page_url_method: :api_v2_incremental_ticket_metric_events_url
          ).as_json
        end
      )
    end
  end

  private

  def handle_request
    # GoodData takes times in local account time, for now
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        yield
      end
    else
      yield
    end
  end

  def presenter
    Api::V2::Exports::TicketMetricEventPresenter.new(
      current_user,
      url_builder:      self,
      gooddata_request: gooddata_request?
    )
  end

  def finder
    Zendesk::Export::IncrementalTicketMetricEventsFinder.new(
      account:     current_account,
      association: TicketMetric::Event.where(account_id: current_account.id),
      start_time:  start_time,
      limit:       EXPORT_LIMIT
    )
  end
end
