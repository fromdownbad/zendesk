class Api::V2::Incremental::TicketEventsController < Api::V2::Exports::BaseController
  allow_gooddata_user
  allow_bime_user
  allow_outbound_user

  ## ### Incremental Ticket Event Export
  ## `GET /api/v2/incremental/ticket_events.json?start_time={unix_time}`
  ##
  ## Returns a stream of changes that occurred on tickets. Each event is tied
  ## to an update on a ticket and contains all the fields that were updated in that
  ## change. For more information, see:
  ##
  ## - [Exporting ticket events](#exporting-ticket-events)
  ## - [Time-based incremental exports](#time-based-incremental-exports)
  ##
  ## You can include comments in the event stream by using the `comment_events`
  ## sideload. See Sideloading below. If you don't specify the sideload, any comment
  ## present in the ticket update is described only by Boolean `comment_present`
  ## and `comment_public` object properties in the event's `child_events` array.
  ## The comment itself is not included.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Sideloading
  ##
  ## The endpoint supports the `comment_events` sideload. Any comment present in the ticket
  ## update is listed as an object in the event's `child_events` array. Example:
  ##
  ## ```js
  ## "child_events": [
  ##   {
  ##     "id": 91048994488,
  ##     "via": {
  ##       "channel": "api",
  ##       "source": {"from":{},"to":{},"rel":null}},
  ##     "via_reference_id":null,
  ##     "type": "Comment",
  ##     "author_id": 5031726587,
  ##     "body": "This is a comment",
  ##     "html_body": "&lt;div class="zd-comment"&gt;&lt;p dir="auto"&gt;This is a comment&lt;/p&gt;",
  ##     "public": true,
  ##     "attachments": [],
  ##     "audit_id": 91048994468,
  ##     "created_at": "2009-06-25T10:15:18Z",
  ##     "event_type": "Comment"
  ##   },
  ##   ...
  ## ],
  ## ...
  ## ```
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/ticket_events.json?start_time=1332034771 \
  ##    -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket_events": [
  ##     {
  ##       "id": 1717,
  ##       "ticket_id": 27,
  ##       "timestamp": 138561439,
  ##       "updater_id": -1,
  ##       "via": "Email",
  ##       "child_events": [
  ##         {
  ##           "via": "Email",
  ##           "via_reference_id": 2,
  ##           "status": "solved"
  ##         }
  ##       ]
  ##     },
  ##    ...
  ##   ]
  ##   "next_page": "http://{subdomain}.zendesk.com/api/v2/incremental/ticket_events.json?start_time=1389078385",
  ##   "count": 1000,
  ##   "end_of_stream": true,
  ##   "end_time": 1389078385
  ## }
  ## ```

  allow_parameters :index,
    exclude_deleted: Parameters.boolean,
    include: Parameters.string,
    per_page: Parameters.integer & Parameters.gt(0),
    start_time: (Parameters.integer | Parameters.float) & Parameters.gte(0)
  def index
    # GoodData takes times in local account time, for now
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        render json: Yajl::Encoder.encode(instrument_export_with_count(:ticket_events) { ticket_events })
      end
    else
      throttle_export_request_unless_can_by_pass_rate_limits
      render json: Yajl::Encoder.encode(instrument_export_with_count(:ticket_events) { ticket_events })
    end
  end

  allow_parameters :sample,
    exclude_deleted: Parameters.boolean,
    include: Parameters.string,
    per_page: Parameters.integer & Parameters.gt(0),
    start_time: (Parameters.integer | Parameters.float) & Parameters.gte(0)
  require_oauth_scopes :sample, scopes: [:read], arturo: true
  def sample
    # GoodData takes times in local account time, for now
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        render json: Yajl::Encoder.encode(ticket_events)
      end
    else
      Prop.throttle!(:sample_incremental_exports, current_account.id)
      render json: Yajl::Encoder.encode(ticket_events)
    end
  end

  protected

  def ticket_events
    finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
      account: current_account,
      start_time: start_time,
      limit: export_limit,
      exclude_deleted: params[:exclude_deleted]
    )

    presenter = Api::V2::Exports::TicketEventsPresenter.new(current_user, url_builder: self, include_comment: params[:include].to_s.include?('comment_events'))

    export = Zendesk::Export::IncrementalExport.new(
      start_time: start_time,
      limit: export_limit,
      presenter: presenter,
      custom_finder: finder,
      url_builder: self,
      page_url_method: :api_v2_incremental_ticket_events_url
    )

    export.as_json
  end

  def export_limit
    [params[:per_page], default_export_limit].compact.min
  end

  def default_export_limit
    if action_name == 'index'
      current_account.has_reduce_incremental_event_results? ? REDUCED_EXPORT_LIMIT : EXPORT_LIMIT
    else
      SAMPLE_EXPORT_LIMIT
    end
  end
end
