class Api::V2::Incremental::RoutingController < Api::V2::Exports::BaseController
  include Zendesk::Routing::ControllerSupport

  require_capability :incremental_skill_based_routing_api
  before_action :set_params

  allow_gooddata_user
  allow_bime_user

  TICKET_TYPE_ID = '10'.freeze
  AGENT_TYPE_ID = '20'.freeze
  INSTANCE_TYPE_ID = { TICKET_TYPE_ID => 'ticket', AGENT_TYPE_ID => 'agent' }.freeze
  CREATE_EVENTS = ['associate_agent', 'associate_ticket', 'create'].freeze
  ALLOWED_PARAMETERS = {
    start_time: Parameters.integer & Parameters.gte(0),
    cursor: Parameters.string
  }.freeze

  ## ### Incremental Attributes Export
  ## `GET /api/v2/incremental/routing/attributes.json`
  ##
  ## Returns a stream of changes that occurred on routing attributes.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Parameters
  ##
  ## Optional
  ##
  ## | Name   | Type   | Comment
  ## | ------ | ------ | -------
  ## | cursor | string | The `cursor` parameter is a non-human-readable argument you can use to move forward or backward in time. The cursor is a read-only URL parameter that's only available in API responses. See [Pagination](#pagination).
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/routing/attributes.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "attributes": [
  ##     {
  ##       "id": "15821cba-7326-11e8-b07e-950ba849aa27",
  ##       "name": "Languages",
  ##       "type": "create",
  ##       "time": "2018-06-19T01:33:19Z"
  ##     },
  ##     ...
  ##   ],
  ##   "count": 1200,
  ##   "next_page": "https://{subdomain}.zendesk.com/api/v2/incremental/routing/attributes.json?cursor=7d724c71-3911-11e8-9621-836b8c683dc6",
  ##   "end_time": 1533266020
  ## }
  ## ```
  allow_parameters :attributes, ALLOWED_PARAMETERS
  def attributes
    with_error_handling do
      handle_request do
        render json: Yajl::Encoder.encode(
          instrument_export_with_count(:attributes) do
            present_events(
              'attributes',
              attribute_events_collection_presenter.present(attribute_events)[:attributes]
            )
          end
        )
      end
    end
  end

  ## ### Incremental Attribute Values Export
  ## `GET /api/v2/incremental/routing/attribute_values.json`
  ##
  ## Returns a stream of changes that occurred on routing attribute values.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Parameters
  ##
  ## Optional
  ##
  ## | Name   | Type   | Comment
  ## | ------ | ------ | -------
  ## | cursor | string | The `cursor` parameter is a non-human-readable argument you can use to move forward or backward in time. The cursor is a read-only URL parameter that's only available in API responses. See [Pagination](#pagination).
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/routing/attribute_values.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "attribute_values": [
  ##     {
  ##       "id": "19ed17fb-7326-11e8-b07e-9de44e7e7f20",
  ##       "attribute_id": "15821cba-7326-11e8-b07e-950ba849aa27",
  ##       "name": "English",
  ##       "type": "create",
  ##       "time": "2018-06-19T01:33:26Z"
  ##     },
  ##     ...
  ##   ],
  ##   "count": 1200,
  ##   "next_page": "https://{subdomain}.zendesk.com/api/v2/incremental/routing/attribute_values.json?cursor=7d724c71-3911-11e8-9621-836b8c683dc6",
  ##   "end_time": 1533266020
  ## }
  ## ```
  allow_parameters :attribute_values, ALLOWED_PARAMETERS
  def attribute_values
    with_error_handling do
      handle_request do
        render json: Yajl::Encoder.encode(
          instrument_export_with_count(:attribute_values) do
            present_events(
              'attribute_values',
              attribute_value_events_collection_presenter.present(attribute_values_events)[:attribute_values]
            )
          end
        )
      end
    end
  end

  ## ### Incremental Instance Values Export
  ## `GET /api/v2/incremental/routing/instance_values.json`
  ##
  ## Returns a stream of changes that occurred on routing instance values.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Parameters
  ##
  ## Optional
  ##
  ## | Name   | Type   | Comment
  ## | ------ | ------ | -------
  ## | cursor | string | The `cursor` parameter is a non-human-readable argument you can use to move forward or backward in time. The cursor is a read-only URL parameter that's only available in API responses. See [Pagination](#pagination).
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/incremental/routing/instance_values.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "instance_values": [
  ##     {
  ##       "id": "62055cad-7326-11e8-b07e-73653560136b",
  ##       "attribute_value_id": "19ed17fb-7326-11e8-b07e-9de44e7e7f20",
  ##       "instance_id": "10001",
  ##       "type": "associate_agent",
  ##       "time": "2018-06-19T01:35:27Z"
  ##     },
  ##     ...
  ##   ],
  ##   "count": 1200,
  ##   "next_page": "https://{subdomain}.zendesk.com/api/v2/incremental/routing/instance_values.json?cursor=62055cad-7326-11e8-b07e-73653560136b",
  ##   "end_time": 1533266020
  ## }
  ## ```
  allow_parameters :instance_values, ALLOWED_PARAMETERS
  def instance_values
    with_error_handling do
      handle_request do
        render json: Yajl::Encoder.encode(
          instrument_export_with_count(:instance_values) do
            present_events(
              'instance_values',
              instance_value_events_collection_presenter.present(instance_values_events)[:instance_values]
            )
          end
        )
      end
    end
  end

  protected

  def set_params
    if params[:cursor].present? && params[:start_time].present?
      params[:start_time] = nil
    end
  end

  def check_valid_start_time!
  end

  private

  def present_events(exported_records_key, events)
    {
      exported_records_key.to_sym => events,
      count: events.length,
      end_time: end_time(events)
    }.compact.tap do |presented_events|
      presented_events[:next_page] =
        if events.empty?
          nil
        else
          send(
            "#{exported_records_key}_api_v2_incremental_routing_index_url".to_sym,
            format: 'json',
            cursor: events.last[:id]
          )
        end
    end
  end

  def format_instance_values!(instance_values)
    ticket_instance_ids = ticket_instance_ids(instance_values)

    if ticket_instance_ids.any?
      instance_id_nice_id_map = Ticket.with_deleted do
        current_account.
          tickets.
          where(id: ticket_instance_ids).
          all_with_archived.
          each_with_object({}) { |ticket, initial_map| initial_map[ticket[:id]] = ticket[:nice_id] }
      end

      instance_values.each do |event|
        if event[:type_id] == TICKET_TYPE_ID
          event[:instance_id] = instance_id_nice_id_map[event[:instance_id].to_i].to_s
        end
      end
    end
  end

  def ticket_instance_ids(instance_values)
    instance_values.select { |iv| iv[:type_id] == TICKET_TYPE_ID }.map(&:instance_id).uniq
  end

  def end_time(events)
    return unless events.present?

    last_create_event_index = events.rindex do |event|
      CREATE_EVENTS.include?(event[:type])
    end

    events[last_create_event_index][:time].to_datetime.to_i
  end

  def attribute_events
    deco.attributes.incremental(params[:start_time], params[:cursor])[:attributes].each_with_object([]) do |att, events|
      events.concat(get_events(att))
    end
  end

  def attribute_values_events
    deco.attribute_values.incremental(params[:start_time], params[:cursor]).each_with_object([]) do |attv, events|
      events.concat(get_events(attv))
    end
  end

  def instance_values_events
    instance_values = deco.incremental_instance_values.incremental(params[:start_time], params[:cursor])
    format_instance_values!(instance_values)

    instance_values.each_with_object([]) do |iv, events|
      instance_value_events = get_events(iv).reject { |event| event[:type] == 'update' }.map do |event|
        if event[:type] == 'create'
          event[:type] = "associate_#{INSTANCE_TYPE_ID[iv.type_id]}"
        elsif event[:type] == 'delete'
          event[:type] = "unassociate_#{INSTANCE_TYPE_ID[iv.type_id]}"
        end

        event
      end

      events.concat(instance_value_events)
    end
  end

  def incremental_attribute_presenter
    @incremental_attribute_presenter ||= begin
      Api::V2::Routing::IncrementalAttributePresenter.new(current_user, url_builder: self)
    end
  end

  def incremental_attribute_value_presenter
    @incremental_attribute_value_presenter ||= begin
      Api::V2::Routing::IncrementalAttributeValuePresenter.new(current_user, url_builder: self)
    end
  end

  def incremental_instance_value_presenter
    @incremental_instance_value_presenter ||= begin
      Api::V2::Routing::IncrementalInstanceValuePresenter.new(current_user, url_builder: self)
    end
  end

  def attribute_events_collection_presenter
    @attribute_events_collection_presenter ||= begin
      Api::V2::CollectionPresenter.new(
        current_user,
        url_builder:    self,
        item_presenter: incremental_attribute_presenter
      )
    end
  end

  def attribute_value_events_collection_presenter
    @attribute_value_events_collection_presenter ||= begin
      Api::V2::CollectionPresenter.new(
        current_user,
        url_builder:    self,
        item_presenter: incremental_attribute_value_presenter
      )
    end
  end

  def instance_value_events_collection_presenter
    @instance_value_events_collection_presenter ||= begin
      Api::V2::CollectionPresenter.new(
        current_user,
        url_builder:    self,
        item_presenter: incremental_instance_value_presenter
      )
    end
  end

  def get_events(entity)
    has_cursor = params[:cursor].present?
    start_time = Time.at(params[:start_time].to_i).to_datetime
    event_base = entity.to_h

    [].tap do |events|
      if entity.created_at.to_datetime >= start_time || has_cursor
        events << OpenStruct.new(event_base.merge(type: 'create', time: entity.created_at))
      end

      if entity.updated_at != entity.created_at && (entity.updated_at.to_datetime >= start_time || has_cursor)
        events << OpenStruct.new(event_base.merge(type: 'update', time: entity.updated_at))
      end

      if entity.deleted_at && (entity.deleted_at.to_datetime >= start_time || has_cursor)
        events << OpenStruct.new(event_base.merge(type: 'delete', time: entity.deleted_at))
      end
    end
  end

  def deco
    @deco ||= Zendesk::Deco::Client.new(current_account)
  end

  def handle_request
    if gooddata_request?
      Time.use_zone(current_account.time_zone) do
        yield
      end
    else
      throttle_export_request_unless_can_by_pass_rate_limits
      yield
    end
  end
end
