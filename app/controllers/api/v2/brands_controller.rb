module Api
  module V2
    class BrandsController < Api::V2::BaseController
      before_action :require_multibrand_feature!, except: [:index, :show, :check_host_mapping_for_brand]
      before_action :can_create_brand, only: [:create]
      before_action :require_admin!, except: [:index, :show]

      skip_before_action :enforce_support_is_active!, only: [:show, :index]

      allow_subsystem_user :collaboration, only: [:index]
      allow_subsystem_user :gooddata, only: [:index]
      allow_subsystem_user :embeddable, only: [:index, :show]
      allow_subsystem_user :theming_center, only: [:index, :show]
      allow_subsystem_user :monitor, only: [:index, :show]
      allow_subsystem_user :passport, only: [:index]
      allow_subsystem_user :knowledge_api, only: [:index]
      allow_subsystem_user :answer_bot_flow_composer, only: [:index]
      allow_subsystem_user :answer_bot_flow_director, only: [:index, :show]
      allow_subsystem_user :zopim, only: [:index, :show]
      allow_bime_user only: [:index]

      brand_parameter = {
        name: Parameters.string,
        logo: { uploaded_data: Parameters.anything },
        active: Parameters.boolean,
        subdomain: Parameters.string,
        host_mapping: Parameters.string,
        signature_template: Parameters.string
      }

      ## ### List Brands
      ## `GET /api/v2/brands.json`
      ##
      ##  Returns a list of all brands for your account sorted by name.
      ##
      ## #### Allowed for
      ##
      ##  * Admins, Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/brands.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "brands": [
      ##     {
      ##       "id":                    47,
      ##       "url":                   "https://company.zendesk.com/api/v2/brands/47.json",
      ##       "name":                  "Brand 1",
      ##       "brand_url":             "https://brand1.zendesk.com",
      ##       ...
      ##     },
      ##     {
      ##       "id":                    42,
      ##       "url":                   "https://company.zendesk.com/api/v2/brands/42.json",
      ##       "name":                  "Brand 42",
      ##       "brand_url":             "https://brand42.zendesk.com",
      ##       ...
      ##     }
      ##   ]
      ## }
      ## ```
      ##
      allow_parameters :index, :skip # TODO: make stricter
      def index
        expires_in(60.seconds) if current_account.has_brands_index_cache_control_header? && app_request?
        render json: presenter.present(brands)
      end

      ## ### Show a Brand
      ## `GET /api/v2/brands/{id}.json`
      ##
      ##  Returns a brand for your account.
      ##
      ## #### Allowed for
      ##
      ##  * Admins, Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/brands/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "brand": {
      ##     "id":   47,
      ##     "url":  "https://company.zendesk.com/api/v2/brands/47.json",
      ##     "name": "Brand 1",
      ##     ...
      ##   }
      ## }
      ## ```
      ##
      resource_action :show

      ## ### Create Brand
      ## `POST /api/v2/brands.json`
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/brands.json \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -d '{"brand": {"name": "Brand 1", "subdomain": "brand1"}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/brands/{id}.json
      ##
      ## {
      ##   "brand": {
      ##     "id":        47,
      ##     "url":       "https://company.zendesk.com/api/v2/brands/47.json",
      ##     "name":      "Brand 1",
      ##     "brand_url": "https://brand1.zendesk.com",
      ##     "subdomain": "brand1",
      ##     ...
      ##   }
      ## }
      ## ```
      ##
      allow_parameters :create, brand: brand_parameter
      def create
        new_brand.attributes = params[:brand].except(:logo)
        new_brand.save!
        reload_kasket_route(new_brand)
        render json: presenter.present(new_brand), status: :created, location: presenter.url(new_brand)
      end

      ## ### Update a Brand
      ## `PUT /api/v2/brands/{id}.json`
      ##
      ##  Returns an updated brand.
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/brands/{id}.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{"brand": {"name": "Brand 1", "subdomain": "brand1", "host_mapping": "brand1.com", "active": true}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "brand": {
      ##     "id":                    47,
      ##     "url":                   "https://company.zendesk.com/api/v2/brands/47.json",
      ##     "name":                  "Brand 1",
      ##     "brand_url":             "https://brand1.com",
      ##     "subdomain":             "brand1",
      ##     "host_mapping":          "brand1.com"
      ##     ...
      ##   }
      ## }
      ## ```
      ##
      allow_parameters :update, id: Parameters.bigid, brand: brand_parameter
      def update
        if updated_brand.invalid_subdomain_change?(current_user)
          render_failure(status: :unprocessable_entity, message: I18n.t("txt.multibrand.brand.invalid_subdomain_change")) && return
        end

        updated_brand.save!
        # Reload new route back into kasket
        reload_kasket_route(updated_brand)
        render json: presenter.present(updated_brand)
      end

      ## ### Delete a Brand
      ## `DELETE /api/v2/brands/{id}.json`
      ##
      ##  Deletes a brand.
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/brands/{id}.json \
      ##   -X DELETE -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      ##
      allow_parameters :destroy, id: Parameters.bigid
      def destroy
        if brand.soft_delete
          default_delete_response
        else
          render json: presenter.present_errors(brand), status: :unprocessable_entity
        end
      end

      ## ### Check host mapping validity
      ## `GET /api/v2/brands/check_host_mapping.json?host_mapping={host_mapping}&subdomain={subdomain}`
      ##
      ##  Returns a JSON object determining whether a host mapping is valid for
      ##  a given subdomain.
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl 'https://{subdomain}.zendesk.com/api/v2/brands/check_host_mapping.json?host_mapping={host_mapping}&subdomain={subdomain}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "is_valid": true,
      ##   "cname": "bar.zendesk.com"
      ## }
      ## ```
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "is_valid": false,
      ##   "reason": "wrong_cname",
      ##   "cname": "google.com",
      ##   "expected_cnames": ["bar.zendesk.com"]
      ## }
      ## ```
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "is_valid": false,
      ##   "reason": "not_a_cname",
      ##   "expected_cnames": ["bar.zendesk.com"]
      ## }
      ## ```
      ##
      allow_parameters :check_host_mapping, host_mapping: Parameters.string, subdomain: Parameters.string
      require_oauth_scopes :check_host_mapping, scopes: [:read], arturo: true
      def check_host_mapping
        if Route.find_by_host_mapping(params[:host_mapping])
          return render json: {is_valid: false, reason: :already_taken}
        end

        expected_cnames = [
          "#{params[:subdomain]}.#{Zendesk::Configuration.fetch(:host)}",
        ]

        resolve = Zendesk::Net::CNAMEResolver.resolve?(params[:host_mapping])

        if resolve
          cname = resolve.name.to_s

          if expected_cnames.include?(cname)
            render json: {is_valid: true, cname: cname}
          else
            render json: {
              is_valid: false,
              reason: :wrong_cname,
              cname: cname,
              expected_cnames: expected_cnames
            }
          end
        else
          render json: {
            is_valid: false,
            reason: :not_a_cname,
            expected_cnames: expected_cnames
          }
        end
      end

      ## ### Check host mapping validity for an existing brand
      ## `GET /api/v2/brands/{id}/check_host_mapping.json`
      ##
      ##  Returns a JSON object determining whether a host mapping is valid for
      ##  the given brand.
      ##
      ## #### Allowed for
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl 'https://{subdomain}.zendesk.com/api/v2/brands/{id}/check_host_mapping.json' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "is_valid": true,
      ##   "cname": "bar.zendesk.com"
      ## }
      ## ```
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "is_valid": false,
      ##   "reason": "wrong_cname",
      ##   "cname": "google.com",
      ##   "expected_cnames": ["bar.zendesk.com"]
      ## }
      ## ```
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "is_valid": false,
      ##   "reason": "not_a_cname",
      ##   "expected_cnames": ["bar.zendesk.com"]
      ## }
      ## ```
      ##
      allow_parameters :check_host_mapping_for_brand, id: Parameters.bigid
      require_oauth_scopes :check_host_mapping_for_brand, scopes: [:read], arturo: true
      def check_host_mapping_for_brand
        if brand.host_mapping.nil?
          return render json: { is_valid: false, reason: :not_host_mapped }
        end

        expected_cnames = ["#{brand.subdomain}.#{Zendesk::Configuration.fetch(:host)}"]

        resolve = Zendesk::Net::CNAMEResolver.resolve?(brand.host_mapping)

        if resolve
          cname = resolve.name.to_s.downcase

          if expected_cnames.include?(cname)
            render json: {
              is_valid: true,
              cname: cname
            }
          else
            render json: {
              is_valid: false,
              reason: :wrong_cname,
              cname: cname,
              expected_cnames: expected_cnames
            }
          end
        else
          render json: {
            is_valid: false,
            reason: :not_a_cname,
            expected_cnames: expected_cnames
          }
        end
      end

      ## ### Updating a Brand's Image
      ##
      ## A brand image can be updated by uploading a local file using the update brand endpoint.
      ##
      ## #### Example Request
      ##
      ## ```bash
      ## curl -v -u {email_address}:{password} -X PUT \
      ##   -F "brand[logo][uploaded_data]=@/path/to/image/image.jpg" \
      ##   https://{subdomain}.zendesk.com/api/v2/brands/{id}.json
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "brand": {
      ##     "id":   47,
      ##     "url":  "https://company.zendesk.com/api/v2/brands/47.json",
      ##     "name": "Brand 1",
      ##     "logo": {
      ##       "url":       "https://company.zendesk.com/api/v2/attachments/928374.json",
      ##       "id":        928374,
      ##       "file_name": "image.jpg",
      ##       ...
      ##     }
      ##   }
      ## }
      ## ```

      ## ### Updating the Default Brand
      ##
      ## The default brand is the one that tickets get assigned to if the ticket is generated from a non-branded channel. You can update the default brand using the [update account settings](./account_settings.html#update-account-settings) endpoint.
      ##
      ## #### Example Request
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/brands.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{ "settings": { "brands": { "default_brand_id": 47 }}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## "settings": {
      ##   ...
      ##   "brands": {
      ##     "default_brand_id": 47
      ##   }
      ## }
      ## ```

      protected

      def presenter
        @presenter ||= Api::V2::BrandPresenter.new(current_user, url_builder: self, includes: includes)
      end

      def scope
        if gooddata_request? || bime_request?
          paginate(Brand.unscoped.where(account_id: current_account.id))
        else
          current_account.brands
        end
      end

      def brands
        @brands ||= scope.order(:name)
      end

      def brand
        @brand ||= Brand.with_deleted { current_account.brands.find(params[:id]) }
      end

      def updated_brand
        @updated_brand ||= begin
          brand_params = params[:brand]
          current_account.brands.find(params[:id]).tap do |found_brand|
            found_brand.set_brand_logo(brand_params[:logo]) if brand_params.include?(:logo)
            found_brand.attributes = brand_params.except(:logo)
          end
        end
      end

      def reload_kasket_route(brand_to_reload)
        # Reload new route back into kasket. Sometimes a route is not present despite the brand persisting
        current_account.class.on_master { brand_to_reload.route.reload }
      rescue ActiveRecord::RecordNotFound => e
        Rails.logger.warn("Failed to reload route back into kasket for brand '#{brand_to_reload.subdomain}': #{e.message}")
      end
    end
  end
end
