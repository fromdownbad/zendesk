class Api::V2::Portal::RequestsController < Api::V2::RequestsController
  include Zendesk::Tickets::Anonymous::ControllerSupport

  deprecate_controller with_feature: :sse_deprecate_portal_requests

  allow_anonymous_users
  allow_public_requests :create

  ## ### Creating Requests
  ## `POST /api/v2/portal/requests.json`
  ##
  ## #### Allowed For
  ##
  ##  * Anyone
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/portal/requests.json \
  ##   -d '{"request": {"subject": "Help!", "comment": {"body": "My printer is on fire!"}, "requester": "anonymous@example.com"}}' \
  ##   -X POST -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/requests/{id}.json
  ## ```
  ## _Note: there is no body returned_

  allow_parameters :create, request: {
    type: Parameters.string,
    priority: Parameters.string,
    custom_fields: Parameters.anything,
    subject: Parameters.string,
    requester: Parameters.anything,
    ticket_form_id: Parameters.bigid,
    due_at: Parameters.date_time | Parameters.nil_string,
    comment: {
      body: Parameters.string,
      value: Parameters.string,
      uploads: Parameters.array(Parameters.string) | Parameters.string,
      screencasts: Parameters.anything # legacy field from deprecated functionality
    },
    system_metadata: {
      client: Parameters.string,
      ip_address: Parameters.string
    }
  }
  def create
    initialized_anonymous_ticket.save!
    head :created, location: ticket_location
  end

  protected

  def verified_request?
    true
  end

  def ticket_location
    presenter =
      case initialized_anonymous_ticket
      when Ticket
        if current_user.is_agent?
          Api::V2::Tickets::TicketPresenter
        else
          Api::V2::RequestPresenter
        end
      when SuspendedTicket
        Api::V2::SuspendedTicketPresenter
      end

    presenter.new(current_user, url_builder: self).
      url(initialized_anonymous_ticket)
  end
end
