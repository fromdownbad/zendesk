require 'zendesk_cross_origin/api/controller_mixin'
require 'zendesk_shared_session/session_persistence/controller_mixin'
require 'active_record/comments'

module Api
  module V2
    class BaseController < Api::V2beta::BaseController
      # Cursor pagination constants
      CURSOR_PAGINATION_PARAMS = {
        limit:    (Parameters.integer & Parameters.gt(0)) | Parameters.nil_string,
        cursor:    Parameters.string,
        order:     Parameters.enum('ASC', 'asc', 'DESC', 'desc') | Parameters.nil_string,
      }.freeze

      # Cursor pagination v2 constants
      # Allow `page=3` to keep OBP working and new hash type `page[:size]=3`
      CURSOR_PAGINATION_V2_PARAMS = {
        page: (Parameters.integer & Parameters.gte(0)) | Parameters.nil_string | Parameters.map(
          before: Parameters.string,
          after: Parameters.string,
          size: Parameters.integer
        ),
        sort: Parameters.string,
      }.freeze

      include ZendeskApi::Controller::Errors::V2Handlers
      include ZendeskApi::Controller::ResponseHelper

      include ::AllowedParameters
      include Zendesk::Brands::ControllerSupport
      include ZendeskCrossOrigin::Api::ControllerMixin
      include Zendesk::SharedSession::SessionPersistence::ControllerMixin
      include Zendesk::Auth::SystemBearerUsers
      include Zendesk::Auth::ControllerSessionSupport
      include BrowserPredicatesHelper
      include Zendesk::ResourcefulActions
      include Zendesk::Accounts::Multiproduct
      include Zendesk::Accounts::RateLimiting
      include Zendesk::CursorPagination::ControllerSupport
      include Api::Presentation::ControllerSupport

      extend Zendesk::RequireCapability

      before_action :enforce_ssl!
      before_action :enforce_support_is_active!
      before_action :set_x_frame_options_header
      before_action :prevent_v2_basic_auth
      before_action :log_authorization_header
      before_action :prevent_expired_trial_account_access
      around_action :set_json_time_format

      # DO NOT EXPAND
      #
      # System users should be scoped to the explicit resources they directly need to access. It is
      # a rare case where a system user truly needs to access all the things. The current exceptions
      # are documented below.

      # Legacy support. This was the first and "shared" system user, which maintains access here for
      # backwards compatibility.
      allow_subsystem_user :zendesk

      # Support-Graph renders audit logs and rich conversational data. Due to the nature of an audit
      # log having the potential to include any arbitrary piece of information, we must ensure that
      # support-graph can access it when necessary.
      allow_subsystem_user :support_graph

      # The sandbox orchestrator copies all of an accounts data and settings into a sandbox. Given
      # the nature of "copy all data", it is granted top-level access here.
      allow_sandbox_orchestrator_user

      allow_parameters :all,
        per_page: (Parameters.integer & Parameters.gt(0)) | Parameters.nil_string,
        page: (Parameters.integer & Parameters.gte(0)) | Parameters.nil_string,
        sort_order: Parameters.enum('ASC', 'asc', 'DESC', 'desc') | Parameters.nil_string,
        sort_by: Parameters.string,
        include: Parameters.string,
        # ProfilingMiddleware
        _mode: Parameters.enum('wall', 'cpu', 'object'),
        _profile: Parameters.enum('true', 'flamegraph'),
        _interval: Parameters.integer

      rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do |e|
        Rails.logger.error("KeyConstraintViolation: #{e.message}")
        render json: {
          error: "DatabaseConflict",
          description: "Database collision"
        }, status: :conflict
      end

      rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound do |e|
        ZendeskExceptions::Logger.record(e, location: self, message: e.message, fingerprint: '4bc51411f29974169678aa5e456be3ce6217e239')
        response.headers['Retry-After'] = database_retry_after.to_s
        render json: {
          error: "DatabaseDeadlock",
          description: "Database deadlock found; please retry your transaction"
        }, status: :service_unavailable
      end

      rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::LockWaitTimeout do |e|
        ZendeskExceptions::Logger.record(e, location: self, message: e.message, fingerprint: '027a58c63eae7f36d06d178c9d1d13811361b5a3')
        response.headers['Retry-After'] = database_retry_after.to_s
        render json: {
          error: "DatabaseTimeout",
          description: "Database lock wait timeout; please retry your transaction"
        }, status: :service_unavailable
      end

      rescue_from JobWithStatusTracking::TooManyJobs do |e|
        Rails.application.config.statsd.client.increment('too_many_jobs_rate_limit', tags: ["job:#{e.job_class}"])
        # This is kinda arbitrary but clients with an auto-retry mechanism will want it
        response.headers['Retry-After'] = 60
        render json: {
          error: "TooManyJobs",
          description: e.message,
          current_job_ids: e.job_ids
        }, status: 429
      end

      rescue_from ::Technoweenie::AttachmentFu::NoBackendsAvailableException do
        response.headers['Retry-After'] = 10
        render status: 503, json: {
          error: 'UploadServiceTemporarilyUnavailable',
          description: I18n.t('txt.errors.attachments.upload_service_temporarily_unavailable')
        }
      end

      rescue_from ZendeskArchive::DisabledDatastoreError do
        response.headers['Retry-After'] = 300
        render status: 503, json: {
          error: 'ArchivedTicketsTemporarilyUnavailable',
          description: I18n.t('txt.errors.attachments.ticketarchive_service_temporarily_unavailable')
        }
      end

      # Silently drop errors when blocking tickets for spam
      rescue_from Fraud::SpamDetector::SpamTicketFoundException do
        render json: { success: true }
      end

      rescue_from Event::ValueTooLarge do
        response.headers['Retry-After'] = 1.day
        render status: :request_entity_too_large, json: {
          error: 'PayloadTooLarge',
          description: e.message
        }
      end

      rescue_from Zendesk::CursorPagination::InvalidPaginationParameter do |e|
        render json: {
          error: "InvalidPaginationParameter",
          description: e.message
        }, status: :bad_request
      end

      def self.allow_cursor_pagination_parameters(action)
        allow_parameters action, CURSOR_PAGINATION_PARAMS
      end

      def self.allow_cursor_pagination_v2_parameters(action)
        allow_parameters action, CURSOR_PAGINATION_V2_PARAMS
      end

      protected

      def with_offset_pagination?
        param_keys = params.keys.map(&:to_s).to_set
        param_keys.intersect?(%w[per_page page].to_set) && !HashParam.ish?(params[:page])
      end

      def statsd_increment_api_endpoint_usage
        Rails.application.config.statsd.client.increment(controller_path)
      end

      def database_retry_after
        # Introduce an element of randomness so many clients aren't hitting us in lockstep
        5 + rand(60)
      end

      def prevent_inactive_account_access
        return if current_user.is_system_user?
        super
      end

      def prevent_expired_trial_account_access
        return unless current_account && current_account.has_prevent_expired_trial_account_access?
        super
      end

      def enqueue_job_with_status(klass, options = {})
        options.reverse_merge!(
          request:            request,
          via_id:             ViaType.WEB_SERVICE,
          raise_on_too_many?: !lotus_api_request?
        )

        options = JobWithStatus.options(current_account, current_user, options)
        identifier = Resque::Plugins::Status::Hash.get(klass.enqueue(options))

        Rails.logger.info "[JobWithStatus Enqueue] Enqueued #{klass} -- identifier: #{identifier}"

        identifier
      end

      def stale_collection?(collection)
        includes.any? || stale?(etag: collection.map(&:cache_key).join("-"))
      end

      def prevent_v2_basic_auth
        if current_account && warden.authenticate(scope: warden_scope) && auth_via?(:basic) && current_account.has_prevent_v2_basic_auth?
          render status: :bad_request, text: "API v2 basic auth access is disabled"
          Rails.logger.info "Bad Request: API v2 basic auth access is disabled"
        end
      end

      def log_authorization_header
        if current_account && current_account.has_log_auth_header?
          Rails.logger.info "[auth header] account:#{current_account.subdomain} user_id:#{current_user.try(:id)} auth_method:#{auth_method}"
        end
      end

      def auth_method
        header = request.headers['Authorization']
        if header.blank?
          'none'
        elsif header.starts_with?('Basic')
          'basic'
        elsif header.starts_with?('Bearer')
          "oauth:#{header[0..10]}"
        else
          'other'
        end
      end

      def forbid_nil_parameters
        old_value = ActionController::Parameters.allow_nil_for_everything
        ActionController::Parameters.allow_nil_for_everything = false
        yield
      ensure
        ActionController::Parameters.allow_nil_for_everything = old_value
      end

      def request_params
        @request_params ||= Zendesk::Tickets::HttpRequestParameters.new(request)
      end

      def enforce_support_is_active!
        return if !multiproduct? || multiproduct_support_active?
        Rails.logger.warn "Multiproduct: Denying url: #{request.url} for account: #{current_account.id}"
        render json: {
          error: 'SupportProductInactive',
          description: 'You must have a valid Support account to call this API'
        }, status: :forbidden
      end

      def multiproduct_support_active?
        current_account.subscription.present? && support_product.try(:active?)
      end

      def set_x_frame_options_header
        headers['X-Frame-Options'] = ::SecureHeaders::Configuration.send(:default_config).x_frame_options
      end
    end
  end
end
