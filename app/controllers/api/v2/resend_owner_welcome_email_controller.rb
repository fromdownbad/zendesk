module Api
  module V2
    class ResendOwnerWelcomeEmailController < Api::V2::BaseController
      skip_before_action :verify_authenticity_token
      skip_before_action :authenticate_user
      skip_before_action :require_agent!

      before_action :validate_email
      before_action :restrict_to_trial_accounts
      before_action :restrict_to_unverified_owners

      ## ### Send Owner Verification Welcome email
      ## `POST /api/v2/resend_owner_welcome_email.json`
      ##
      ## #### Allowed For:
      ##
      ##  * Anyone
      ##
      ## Sends owner welcome email with verification token
      ##
      ## #### Using curl:
      ##
      ## ```bash
      ## curl https:{subdomain}//.zendesk.com/api/v2/resend_owner_welcome_email.json\
      ##   -H "Content-Type: application/json" -X POST \
      ##   -d '{"email": "hermione_granger@gryffindor.com"}' \
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## ```

      allow_parameters :resend_owner_welcome_email, email: Parameters.string
      def resend_owner_welcome_email
        Prop.throttle!(:resend_owner_welcome_email, current_account.id)
        @previous_owner_email = current_account.owner.email

        if params[:email] == @previous_owner_email
          deliver_welcome_account_owner!

          statsd_client.increment('welcome_account_owner_email_sent', tags: ['reason:unverified_owner_match'])
        else
          change_owner_email_to_provided_email!

          deliver_owner_email_change!

          # now that the owner email has been updated,this will deliver to the new provided email
          deliver_welcome_account_owner!

          statsd_client.increment('welcome_account_owner_email_sent', tags: ['reason:unverified_owner_change'])
        end

        head :ok
      end

      private

      # At this point only one email identity exists
      def change_owner_email_to_provided_email!
        current_account.owner.identities.first.update_attributes!(value: params[:email])

        Rails.logger.info "Account #{current_account.subdomain} owner email changed from #{@previous_owner_email} to #{account_owner.email}"
      end

      def deliver_owner_email_change!
        AccountsMailer.deliver_owner_email_change(account_owner, @previous_owner_email)
        statsd_client.increment('owner_email_change_email_sent', tags: ['reason:unverified_owner_change'])
      end

      def deliver_welcome_account_owner!
        AccountsMailer.deliver_welcome_account_owner(account_owner, account_url: current_account.url(mapped: false))
      end

      def account_owner
        @account_owner ||= current_account.owner
      end

      def restrict_to_unverified_owners
        if account_owner.is_verified?
          statsd_client.increment('restricted', tags: ['reason:owner_verified'])
          render(
            status: :unprocessable_entity,
            json: {
              error: 'InvalidRequest',
              description: 'Your account owner is already verified'
            }
          )
        end
      end

      def restrict_to_trial_accounts
        unless current_account.is_trial?
          statsd_client.increment('restricted', tags: ['reason:not_trial_account'])
          render(
            status: :unprocessable_entity,
            json: {
              error: 'InvalidRequest',
              description: 'Not accessible to a non-trial account'
            }
          )
        end
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'api.v2.resend_owner_welcome_email_controller')
      end

      def validate_email
        unless Zendesk::Mail::Address.valid_address?(params[:email])
          render(
            status: :unprocessable_entity,
            json: {
              error: 'InvalidRequest',
              description: 'Email format is invalid'
            }
          )
        end
      end
    end
  end
end
