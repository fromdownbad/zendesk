class Api::V2::CollaboratorsController < Api::V2::BaseController
  require_that_user_can! :view, :ticket

  ## ### List Collaborators for a Ticket
  ## `GET /api/v2/tickets/{id}/collaborators.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/collaborators.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "users": [
  ##     {
  ##       "id": 223443,
  ##       "name": "Johnny Agent",
  ##       ...
  ##     },
  ##     {
  ##       "id": 8678530,
  ##       "name": "Peter Admin",
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, ticket_id: Parameters.bigid
  def index
    render json: presenter.present(filtered_collaborators)
  end

  protected

  def presenter
    @presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self)
  end

  def filtered_collaborators
    if current_account.has_email_ccs?
      Collaboration::LegacyTypeTransformer.filtered_collaborators(
        collaborations: ticket.collaborations.with_active_users,
        settings: follower_and_email_cc_settings
      )
    else
      ticket.collaborators
    end
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  private

  def follower_and_email_cc_settings
    {
      comment_email_ccs_allowed_enabled: current_account.has_comment_email_ccs_allowed_enabled?,
      ticket_followers_allowed_enabled: current_account.has_ticket_followers_allowed_enabled?,
      follower_and_email_cc_collaborations_enabled: current_account.has_follower_and_email_cc_collaborations_enabled?
    }
  end
end
