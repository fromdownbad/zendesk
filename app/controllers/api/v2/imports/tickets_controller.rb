require 'zendesk/tickets/importer'

class Api::V2::Imports::TicketsController < Api::V2::BaseController
  before_action :require_admin!
  before_action :validate_many_tickets_limits, only: [:create_many]

  ticket_parameter = Api::V2::TicketsController::TICKET_PARAMETER.merge(
    solved_at: Parameters.date_time | Parameters.nil_string,
    updated_at: Parameters.date_time | Parameters.nil_string,
    created_at: Parameters.date_time | Parameters.nil_string,
    satisfaction_rating: Parameters.map(
      score: Parameters.enum(Api::V2::Tickets::AttributeMappings::PARAMS_SATISFACTION_MAP.keys),
      comment: Parameters.string
    ) | Parameters.nil
  )
  ticket_parameter[:comment] = Api::V2::TicketsController::COMMENT_PARAMETER.merge(
    created_at: Parameters.date_time | Parameters.nil_string,
    metadata: Parameters.anything
  )
  ticket_parameter[:comments] = [ticket_parameter[:comment]]

  ## ### Ticket Import
  ## `POST /api/v2/imports/tickets.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Request body
  #
  ##  The endpoint takes a `ticket` object describing the ticket. Example:
  ##
  ## ```js
  ## {
  ##   "ticket": {
  ##     "requester_id": 827,
  ##     "assignee_id": 19,
  ##     "subject": "Some subject",
  ##     "description": "A description",
  ##     "tags": [ "foo", "bar" ],
  ##     "comments": [
  ##       { "author_id": 827, "value": "This is a comment", "created_at": "2009-06-25T10:15:18Z" },
  ##       { "author_id": 19, "value": "This is a private comment", "public": false }
  ##     ]
  ##   }
  ## }
  ## ```
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/imports/tickets.json \
  ##   -v -u {email_address}:{password} -X POST \
  ##   -d '{"ticket": {"subject": "Help", "comments": [{ "author_id": 19, "value": "This is a comment" }]}}' \
  ##   -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json
  ##
  ## {
  ##   "ticket": {
  ##     {
  ##       "id":      35436,
  ##       "subject": "Help",
  ##       ...
  ##     }
  ##   }
  ## }
  ## ```
  allow_parameters :create, ticket: ticket_parameter, archive_immediately: Parameters.boolean
  def create
    new_ticket = importer.import(params[:ticket])
    render json: presenter.present(new_ticket), status: :created, location: presenter.url(new_ticket)
  rescue Zendesk::Tickets::Importer::InvalidParams => error
    log_error(error)

    if error.record
      render json: presenter.present_errors(error.record), status: :unprocessable_entity
    else
      head :bad_request
    end
  rescue Zendesk::Tickets::Importer::ImportFailed => error
    log_error(error)
    head :unprocessable_entity
  rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation => error
    log_error(error)
    render plain: "Requester creation collision - please try again",
           status: :conflict, content_type: Mime[:text].to_s
  end

  ## ### Ticket Bulk Import
  ## `POST /api/v2/imports/tickets/create_many.json`
  ##
  ## Accepts an array of up to 100 ticket objects with a maximum payload size of 5MB.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Request body
  #
  ## The endpoint takes a `tickets` array of up to 100 ticket objects. Example:
  ##
  ## ```js
  ## {
  ##   "tickets": [
  ##     {
  ##       "requester_id": 827,
  ##       "assignee_id": 19,
  ##       "subject": "Some subject",
  ##       "description": "A description",
  ##       "tags": [ "foo", "bar" ],
  ##       "comments": [
  ##         { "author_id": 827, "value": "This is a comment", "created_at": "2009-06-25T10:15:18Z" },
  ##         { "author_id": 19, "value": "This is a private comment", "public": false }
  ##       ]
  ##     },
  ##     {
  ##       "requester_id": 830,
  ##       "assignee_id": 21,
  ##       "subject": "Some subject",
  ##       "description": "A description",
  ##       "tags": [ "foo", "bar" ],
  ##       "comments": [
  ##         { "author_id": 830, "value": "This is a comment", "created_at": "2009-06-25T10:15:18Z" },
  ##         { "author_id": 21, "value": "This is a private comment", "public": false }
  ##       ]
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/imports/tickets/create_many.json \
  ##   -v -u {email_address}:{password} -X POST \
  ##   -d '{"tickets": [{"subject": "Help!", "comments": [{ "author_id": 19, "value": "This is a comment" }]}, {"subject": "Help!!", "comments": [{ "author_id": 21, "value": "This is a comment" }]}]}' \
  ##   -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  allow_parameters :create_many, tickets: [ticket_parameter], archive_immediately: Parameters.boolean
  def create_many
    raise ActionController::ParameterMissing, "Missing tickets parameters" unless params[:tickets]

    Prop.throttle!(:trial_ticket_bulk_import, current_account.id) if current_account.is_trial? && current_account.has_trial_ticket_bulk_import_rate_limit?

    render json: job_status_presenter.present(enqueue_job_with_status(TicketBulkImportJob, params))
  end

  private

  def presenter
    @presenter ||= Api::V2::Tickets::TicketPresenter.new(current_user, url_builder: self)
  end

  def job_status_presenter
    @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def importer
    @importer ||= Zendesk::Tickets::V2::Importer.new(account: current_account, user: current_user, archive_immediately: params[:archive_immediately])
  end

  def log_error(error)
    logger.error("[TicketImporter] Failed to import ticket: #{error.message}")
  end

  def validate_many_tickets_limits
    if params[:tickets].try(:length).to_i > current_account.settings.import_many_tickets_limit
      raise ZendeskApi::Controller::Errors::TooManyValuesError.new("tickets", current_account.settings.import_many_tickets_limit)
    end

    if params[:tickets].to_s.bytesize > current_account.settings.import_many_tickets_bytesize_limit
      render status: 413, json: { error: "PayloadTooLarge", description: "Payload too large, current max is #{current_account.settings.import_many_tickets_bytesize_limit / 1.megabyte}MB." }
    end
  end
end
