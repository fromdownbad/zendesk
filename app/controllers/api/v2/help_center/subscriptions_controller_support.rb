# Included by ForumSubscriptionsController and TopicSubscriptionsController. Lists the
# forums/topics the requested user subscribes to.
#
# Provides:
#   /api/v2/users/{user_id}/organization_subscriptions
#   /api/v2/users/{user_id}/subscriptions
#   /api/v2/organization_subscriptions # index, create, destroy
module Api::V2::HelpCenter::SubscriptionsControllerSupport
  protected

  def subscription
    @subscription ||= finder.scope.find(params[:id])
  end
  alias_method :resource, :subscription

  def new_subscription
    @new_subscription ||= begin
      source.watchings.new do |w|
        w.user    = user
        w.account = current_account
      end
    end
  end

  def subscriptions(without_pagination: false)
    @subscriptions ||= begin
      records = finder.scope.where(source_type: source_type).order(created_at: :desc, id: :desc)
      without_pagination ? records : paginate(records)
    end
  end

  def subscription_parameters
    params[presenter.class.model_key] || params
  end

  def user
    @user ||= current_account.users.find(subscription_parameters[:user_id])
  end

  def finder
    @organization_subscriptions_finder ||= Zendesk::OrganizationSubscriptions::Finder.new(
      current_account,
      current_user,
      params
    )
  end
end
