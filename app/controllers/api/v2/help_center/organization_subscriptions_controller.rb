class Api::V2::HelpCenter::OrganizationSubscriptionsController < Api::V2::BaseController
  include Api::V2::HelpCenter::SubscriptionsControllerSupport

  # /api/v2/organization_subscriptions.json maps to :index
  # For end users we scope to the end user subscriptions, for agents we return all subscriptions
  require_that_user_can! :list, Organization, only: [:index], if: proc { current_user.is_agent? }

  # /api/v2/organizations/{organization_id}/subscriptions.json maps to :organization_index
  before_action :require_agent!, only: [:organization_index]

  # /api/v2/users/{user_id}/organization_subscriptions.json maps to :user_index
  # either you are listing your own organization or you are an agent with proper permissions
  require_that_user_can! :list, Organization, only: [:user_index], unless: :listing_own?

  before_action :ensure_organization_access, only: [:create]

  CBP_SORTABLE_FIELDS = {
    'id' => :id,
    'created_at' => :created_at
  }.freeze

  ## ### List Organization Subscriptions
  ##
  ## * `GET /api/v2/organizations/{organization_id}/subscriptions.json`
  ## * `GET /api/v2/organization_subscriptions.json`
  ## * `GET /api/v2/users/{user_id}/organization_subscriptions.json`
  ##
  ## #### Pagination
  ##
  ## * Cursor pagination (recommended)
  ## * Offset pagination
  ##
  ## See [Pagination](./introduction#pagination).
  ##
  ## Returns a maximum of 100 records per page.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##  * End users
  ##
  ## For end users, the response will only list the subscriptions created by the requesting end user.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/organization_subscriptions.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "organization_subscriptions": [
  ##     {
  ##       "id":              35436,
  ##       "organization_id": 32,
  ##       "user_id":         482,
  ##       "created_at":      "2009-07-20T22:55:29Z"
  ##     },
  ##     {
  ##       "id":              43681,
  ##       "organization_id": 334,
  ##       "user_id":         9471,
  ##       "created_at":      "2011-08-22T21:12:09Z"
  ##     }
  ##   ]
  ## }
  ## ```
  allow_cursor_pagination_v2_parameters :index
  def index
    if current_account.arturo_enabled?('remove_offset_pagination_organization_subscriptions_index') || (current_account.arturo_enabled?('cursor_pagination_organization_subscriptions_index') && !with_offset_pagination?) || with_cursor_pagination_v2?
      sorted_records = sorted_scope_for_cursor(subscriptions(without_pagination: true), CBP_SORTABLE_FIELDS)
      render json: cursor_presenter.present(paginate_with_cursor(sorted_records))
    else
      render json: presenter.present(subscriptions)
    end
  end

  allow_parameters :organization_index, organization_id: Parameters.bigid
  allow_cursor_pagination_v2_parameters :organization_index
  require_oauth_scopes :organization_index, scopes: %i[organizations:read read]
  alias :organization_index :index

  allow_parameters :user_index, user_id: Parameters.bigid
  allow_cursor_pagination_v2_parameters :user_index
  require_oauth_scopes :user_index, scopes: %i[organizations:read read]
  alias :user_index :index

  ## ### Show Organization Subscription
  ## `GET /api/v2/organization_subscriptions/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##  * End users
  ##
  ## For end users, the response will show only the subscription if it was created by the requesting end user.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/organization_subscriptions/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "organization_subscription": {
  ##     "id":              35436,
  ##     "organization_id": 32,
  ##     "user_id":         482,
  ##     "created_at":      "2009-07-20T22:55:29Z"
  ##   }
  ## }
  ## ```
  allow_parameters :index, id: Parameters.bigid
  allow_parameters :show, :skip # TODO: make stricter
  def show
    render json: presenter.present(subscription)
  end

  ## ### Create Organization Subscription
  ## `POST /api/v2/organization_subscriptions.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##  * End users
  ##
  ## End users can only subscribe to shared organizations in which they're members.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/organization_subscriptions.json \
  ##   -d '{"organization_subscription": {"user_id": 772, "organization_id": 881}}' \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json" -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "organization_subscription": {
  ##     "id":              55436,
  ##     "organization_id": 881,
  ##     "user_id":         772,
  ##     "created_at":      "2012-04-20T22:55:29Z"
  ##   }
  ## }
  ## ```
  allow_parameters :create,
    organization_id: Parameters.bigid,
    user_id: Parameters.bigid,
    organization_subscription: {
      organization_id: Parameters.bigid,
      user_id: Parameters.bigid,
    }
  def create
    new_subscription.save!
    render json: presenter.present(new_subscription), status: :created, location: presenter.url(new_subscription)
  end

  ## ### Delete Organization Subscription
  ## `DELETE /api/v2/organization_subscriptions/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##  * End users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/organization_subscriptions/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  resource_action :destroy

  allow_parameters :count, user_id: Parameters.bigid, organization_id: Parameters.bigid
  def count
    render json: { count: record_counter.present, links: { url: request.original_url } }
  end

  protected

  alias_method :resource, :subscription

  def presenter
    @presenter ||= Api::V2::OrganizationSubscriptionPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def cursor_presenter
    @cursor_presenter ||= Api::V2::OrganizationSubscriptionPresenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
  end

  def source_type
    Organization.name
  end

  def source
    @source ||= finder.organization_scope.find(subscription_parameters[:organization_id])
  end

  def listing_own?
    params[:user_id] == current_user.id
  end

  def ensure_organization_access
    deny_access unless user.can?(:view, source)
  end

  def record_counter
    @record_counter ||= begin
      Zendesk::RecordCounter::OrganizationSubscriptions.new(
        account: current_account,
        options: {
          source_type: source_type,
          current_user_id: current_user.id,
          user_id: subscription_parameters[:user_id],
          organization_id: subscription_parameters[:organization_id]
        }
      )
    end
  end
end
