class Api::V2::SessionsController < Api::V2::BaseController
  skip_before_action :require_agent!
  # CP4 accounts with no Support need to be able to access renew
  skip_before_action :enforce_support_is_active!, only: :renew
  before_action :check_user_id, except: [:renew, :logout]

  after_action :set_cache_no_store

  allow_parameters :all, user_id: Parameters.bigid | Parameters.enum('me')
  allow_cors_from_zendesk_external_domains :renew

  ## ### List Sessions
  ## `GET /api/v2/sessions.json`
  ##
  ## `GET /api/v2/users/{user_id}/sessions.json`
  ##
  ## If authenticated as an admin, returns all the account's sessions. If
  ## authenticated as an agent or end user, returns only the sessions of
  ## the user making the request.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents, End users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/sessions.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "sessions": [
  ##     {
  ##       "id":               3432,
  ##       "url":              "https://company.zendesk.com/api/v2/users/12345/sessions/3432.json",
  ##       "user_id":          12345,
  ##       "authenticated_at": "2014-11-18T17:24:29Z",
  ##       "last_seen_at":     "2014-11-18T17:30:52Z"
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  allow_parameters :index, {}
  def index
    render json: presenter.present(session_records)
  end

  ## ### Show Session
  ## `GET /api/v2/users/{user_id}/sessions/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents, End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/sessions/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "session": [
  ##     {
  ##       "id":               3432,
  ##       "url":              "https://company.zendesk.com/api/v2/users/12345/sessions/3432.json",
  ##       "user_id":          12345,
  ##       "authenticated_at": "2014-11-18T17:24:29Z",
  ##       "last_seen_at":     "2014-11-18T17:30:52Z"
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  require_oauth_scopes :show, scopes: [:read]
  resource_action :show

  ## ### Show the Currently Authenticated Session
  ## `GET /api/v2/users/me/session.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents, End Users
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "session": [
  ##     {
  ##       "id":               3432,
  ##       "url":              "https://company.zendesk.com/api/v2/users/12345/sessions/3432.json",
  ##       "user_id":          12345,
  ##       "authenticated_at": "2014-11-18T17:24:29Z",
  ##       "last_seen_at":     "2014-11-18T17:30:52Z"
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  allow_parameters :show_current, {}
  require_oauth_scopes :show_current, scopes: [:read], arturo: true
  def show_current
    session = current_user.shared_sessions.unexpired.find_by_session_id!(shared_session.id)
    render json: presenter.present(session)
  end

  ## ### Renew the current session
  ## `GET /api/v2/users/me/session/renew`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents, End Users
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  ##
  allow_parameters :renew, {}
  require_oauth_scopes :renew, scopes: [:read]
  def renew
    if Arturo.feature_enabled_for?(:client_session_renewal_via_explicit_api, current_account)
      authentication.keepalive
      set_expires_at_in_headers
    end
    render json: { authenticity_token: form_authenticity_token }, status: :ok
  end

  ## ### Delete Session
  ## `DELETE /api/v2/users/{user_id}/sessions/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents, End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/sessions/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  require_oauth_scopes :destroy, scopes: [:write]
  resource_action :destroy

  ## ### Bulk Deleting Sessions
  ## `DELETE /api/v2/users/{user_id}/sessions.json`
  ##
  ## Deletes all the sessions for a user.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents, End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/sessions.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  require_oauth_scopes :destroy_many, scopes: [:write]
  allow_parameters :destroy_many, {}
  def destroy_many
    session_records.each(&:destroy)
    default_delete_response
  end

  ## ### Delete the Authenticated Session
  ## `DELETE /api/v2/users/me/logout.json`
  ##
  ## Deletes the current session. In practice, this only works when using session auth for
  ## requests, such as client-side requests made from a Zendesk app. When using OAuth or
  ## basic authentication, you don't have a current session so this endpoint has no effect.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents, End Users
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  require_oauth_scopes :logout, scopes: [:write]
  allow_parameters :logout, {}
  def logout
    clear_session_and_logout
    default_delete_response
  end

  private

  def check_user_id
    if params[:user_id] && !current_user.is_admin? && current_user.id != params[:user_id]
      head :not_found
    end
  end

  # Don't let anyone cache information about sessions
  def set_cache_no_store
    response.headers['Cache-Control'] = 'no-store'
    response.headers['ETag'] = nil
    response.headers['Expires'] = '-1'
    response.headers.delete('Last-Modified')
  end

  # needed for resource_action
  def resource_name
    'session_record'
  end

  def session_record
    @session ||= sessions_scope.find(params[:id])
  end

  def session_records
    @sessions ||= paginate(sessions_scope)
  end

  def sessions_scope
    scope = current_account.shared_sessions.unexpired
    if current_user.is_admin? && params[:user_id]
      scope = scope.where(user_id: params[:user_id])
    elsif !current_user.is_admin?
      scope = scope.where(user_id: current_user.id)
    end
    scope.order('id')
  end

  def presenter
    @presenter ||= Api::V2::SessionRecordPresenter.new(current_user, url_builder: self)
  end
end
