class Api::V2::TicketPredictionsController < Api::V2::BaseController
  allow_parameters :all, ticket_id: Parameters.bigid

  ## ### Ticket prediction information
  ## `GET /api/v2/tickets/{id}/ticket_prediction.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/ticket_prediction.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket_prediction": {
  ##     "satisfaction_probability": 0.555555,
  ##     "ticket_id": 4,
  ##     "account_id": 1,
  ##     "created_at": "2015-11-26T00:59:20Z",
  ##     "updated_at": "2015-11-30T06:54:23Z"
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.bigid
  def show
    raise ActiveRecord::RecordNotFound if ticket_prediction.nil?
    render json: presenter.present(ticket_prediction)
  end

  private

  def presenter
    @presenter ||= Api::V2::Tickets::PredictionPresenter.new(current_user, url_builder: self)
  end

  def ticket
    @ticket ||= current_account.tickets.for_user(current_user).find_by_nice_id!(params[:ticket_id])
  end

  def ticket_prediction
    @ticket_prediction ||= ticket.ticket_prediction
  end
end
