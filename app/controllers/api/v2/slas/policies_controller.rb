require 'zendesk/sla'

class Api::V2::Slas::PoliciesController < Api::V2::BaseController
  before_action :check_permissions,
    :verify_feature!

  allow_gooddata_user only: %i[index show]
  allow_bime_user only: %i[index show]

  rescue_from(
    Zendesk::Sla::PolicyManager::PolicyManagerArgumentError,
    ActionController::ParameterMissing,
    with: :render_client_error
  )

  rescue_from(
    Sla::Policy::SlaPolicyLimitExceededError,
    with: :render_limit_error
  )

  ## ### List SLA Policies
  ## `GET /api/v2/slas/policies`
  ##
  ## #### Availability
  ##
  ##  * Accounts on the Professional and Enterprise plans
  ##
  ## #### Allowed For
  ##
  ##  * Administrators
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/slas/policies \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "sla_policies": [
  ##     {
  ##       "url": "https://{subdomain}.zendesk.com/api/v2/slas/policies/36.json",
  ##       "id": 36,
  ##       "title": "Incidents",
  ##       "description": "For urgent incidents, we will respond to tickets in 10 minutes",
  ##       "position": 3,
  ##       "filter": { ... },
  ##       "policy_metrics": [ ... ]
  ##     }
  ##   ],
  ##   "next_page": null,
  ##   "previous_page": null,
  ##   "count": 1
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(sla_policies)
  end

  ## ### Get SLA Policy
  ## `GET /api/v2/slas/policies/{id}`
  ##
  ## #### Availability
  ##
  ##  * Accounts on the Professional and Enterprise plans
  ##
  ## #### Allowed For
  ##
  ##  * Administrators
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/slas/policies/{id} \
  ##    -H "Content-Type: application/json" \
  ##    -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "sla_policy": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/slas/policies/36.json",
  ##     "id": 36,
  ##     "title": "Incidents",
  ##     "description": "For urgent incidents, we will respond to tickets in 10 minutes",
  ##     "position": 3,
  ##     "filter": { ... },
  ##     "policy_metrics": [ ... ]
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.bigid
  def show
    render json: presenter.present(sla_policy)
  end

  ## ### Create SLA Policy
  ## `POST /api/v2/slas/policies`
  ##
  ## #### Availability
  ##
  ##  * Accounts on the Professional and Enterprise plans
  ##
  ## #### Allowed For
  ##
  ##  * Administrators
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/slas/policies \
  ##   -H "Content-Type: application/json" \
  ##   -d '{
  ##         "sla_policy": {
  ##           "title": "Incidents",
  ##           "description": "For urgent incidents, we will respond to tickets in 10 minutes",
  ##           "position": 3,
  ##           "filter": {
  ##             "all": [
  ##               { "field": "type", "operator": "is", "value": "incident" }
  ##             ],
  ##             "any": []
  ##           },
  ##           "policy_metrics": [
  ##             { "priority": "normal", "metric": "first_reply_time", "target": 30, "business_hours": false },
  ##             { "priority": "urgent", "metric": "first_reply_time", "target": 10, "business_hours": false },
  ##             { "priority": "low", "metric": "requester_wait_time", "target": 180, "business_hours": false },
  ##             { "priority": "normal", "metric": "requester_wait_time", "target": 160, "business_hours": false },
  ##             { "priority": "high", "metric": "requester_wait_time", "target": 140, "business_hours": false },
  ##             { "priority": "urgent", "metric": "requester_wait_time", "target": 120, "business_hours": false }
  ##           ]
  ##         }
  ##       }' \
  ##   -v -u {email_address}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## {
  ##   "sla_policy": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/slas/policies/36.json",
  ##     "id": 36,
  ##     "title": "Incidents",
  ##     "description": "For urgent incidents, we will respond to tickets in 10 minutes",
  ##     "position": 3,
  ##     "filter": { ... },
  ##     "policy_metrics": [ ... ]
  ##   }
  ## }
  ## ```
  FILTER_CONDITIONS = Parameters.array(
    Parameters.map(
      field:    Parameters.string,
      operator: Parameters.string,
      value:    Parameters.string |
        Parameters.integer |
        Parameters.array(Parameters.string | Parameters.integer) |
        Parameters.nil
    )
  ) | Parameters.nil

  FILTER_PARAMS = {all: FILTER_CONDITIONS, any: FILTER_CONDITIONS}.freeze

  POLICY_PARAMS = Parameters.map(
    title:          Parameters.string,
    description:    Parameters.string,
    position:       Parameters.integer,
    filter:         Parameters.map(FILTER_PARAMS),
    policy_metrics: Parameters.array(
      Parameters.map(
        priority:         Parameters.string,
        metric:           Parameters.string,
        target:           Parameters.integer,
        business_hours:   Parameters.boolean
      )
    ) | Parameters.nil
  )

  allow_parameters :create, sla_policy: POLICY_PARAMS
  def create
    policy = policy_manager.create(create_params)

    render(
      json:     presenter.present(policy),
      status:   :created,
      location: presenter.url(policy)
    )
  end

  ## ### Update SLA Policy
  ## `PUT /api/v2/slas/policies/{id}`
  ##
  ## Updates the specified policy.
  ##
  ## #### Availability
  ##
  ##  * Accounts on the Professional and Enterprise plans
  ##
  ## #### Allowed For
  ##
  ##  * Administrators
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/slas/policies/{id} \
  ##   -H "Content-Type: application/json" \
  ##   -d '{
  ##         "sla_policy": {
  ##           "title": "Urgent Incidents",
  ##           "description": "For urgent incidents, we will resolve the ticket within 2 hours",
  ##           "position": 3,
  ##           "filter": {
  ##             "all": [
  ##               { "field": "type", "operator": "is", "value": "incident" }
  ##             ],
  ##             "any": []
  ##           },
  ##           "policy_metrics": [
  ##             { "priority": "normal", "metric": "first_reply_time", "target": 30, "business_hours": false },
  ##             { "priority": "urgent", "metric": "first_reply_time", "target": 10, "business_hours": false },
  ##             { "priority": "low", "metric": "requester_wait_time", "target": 180, "business_hours": false },
  ##             { "priority": "normal", "metric": "requester_wait_time", "target": 160, "business_hours": false },
  ##             { "priority": "high", "metric": "requester_wait_time", "target": 140, "business_hours": false },
  ##             { "priority": "urgent", "metric": "requester_wait_time", "target": 120, "business_hours": false }
  ##           ]
  ##         }
  ##       }' \
  ##    -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "sla_policy": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/slas/policies/36.json",
  ##     "id": 36,
  ##     "title": "Urgent Incidents",
  ##     "description": "For urgent incidents, we will resolve the ticket within 2 hours",
  ##     "position": 3,
  ##     "filter": { ... },
  ##     "policy_metrics": [ ... ]
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.bigid, sla_policy: POLICY_PARAMS
  def update
    policy = policy_manager.update(sla_policy, update_params)

    render json: presenter.present(policy), location: presenter.url(policy)
  end

  ## ### Delete SLA Policy
  ## `DELETE /api/v2/slas/policies/{id}`
  ##
  ## #### Availability
  ##
  ##  * Accounts on the Professional and Enterprise plans
  ##
  ## #### Allowed For
  ##
  ##  * Administrators
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/slas/policies/{id} \
  ##    -H "Content-Type: application/json" \
  ##    -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    policy_manager.delete(params[:id])

    default_delete_response
  end

  ## ### Reorder SLA Policies
  ## `PUT /api/v2/slas/policies/reorder.json`
  ##
  ## #### Availability
  ##
  ##  * Accounts on the Professional and Enterprise plans
  ##
  ## #### Allowed For
  ##
  ##  * Administrators
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/slas/policies/reorder.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{"sla_policy_ids": [12, 55]}' \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :reorder, sla_policy_ids: [Parameters.integer]
  require_oauth_scopes :reorder, scopes: [:write], arturo: true
  def reorder
    policy_manager.reorder(params[:sla_policy_ids])

    head :ok
  end

  ## ### Retrieve supported filter definition items
  ## `GET /api/v2/slas/policies/definitions.json`
  ##
  ## #### Availability
  ##
  ##  * Accounts on the Professional and Enterprise plans
  ##
  ## #### Allowed For
  ##
  ##  * Administrators
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/slas/policies/definitions.json \
  ##    -H "Content-Type: application/json" \
  ##    -v -u {email_address}:{password} -X GET
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "definitions": {
  ##     "all": [
  ##        {
  ##          "target": null,
  ##          "title": "Brand",
  ##          "values": {
  ##            "type": "list",
  ##            "list": [
  ##              {
  ##                "value": 10001,
  ##                "title": "Support"
  ##              }
  ##            ]
  ##          },
  ##          "value": "brand_id",
  ##          "operators": [
  ##            {
  ##              "value": "is",
  ##              "title": "Is"
  ##            },
  ##            {
  ##              "value": "is_not",
  ##              "title": "Is not"
  ##            }
  ##          ],
  ##          "group": "ticket"
  ##        },
  ##        ...
  ##     ],
  ##     "any": [
  ##        {
  ##          "target": null,
  ##          "title": "Brand",
  ##          "values": {
  ##            "type": "list",
  ##            "list": [
  ##              {
  ##                "value": 10001,
  ##                "title": "Support"
  ##              }
  ##            ]
  ##          },
  ##          "value": "brand_id",
  ##          "operators": [
  ##            {
  ##              "value": "is",
  ##              "title": "Is"
  ##            },
  ##            {
  ##              "value": "is_not",
  ##              "title": "Is not"
  ##            }
  ##          ],
  ##          "group": "ticket"
  ##        },
  ##        ...
  ##     ]
  ##   }
  ## }
  ## ```
  allow_parameters :definitions, {}
  require_oauth_scopes :definitions, scopes: [:read], arturo: true
  def definitions
    render(
      json: {
        definitions: %i[all any].each_with_object({}) do |type, definitions|
          definitions.store(type, sla_definitions(type))
        end
      }
    )
  end

  private

  def sla_policy
    @sla_policy ||= current_account.sla_policies.active.find(params[:id])
  end

  def sla_policies
    @sla_policies ||= current_account.sla_policies.active
  end

  def policy_manager
    @policy_manager ||= begin
      Zendesk::Sla::PolicyManager.new(current_account, current_user)
    end
  end

  def sla_definitions(type)
    ZendeskRules::Definitions::Conditions.definitions_as_json(
      Zendesk::Rules::Context.new(current_account, current_user,
        component_type: :condition,
        rule_type:      :sla_policy,
        condition_type: type)
    )
  end

  def presenter
    @presenter ||= begin
      Api::V2::Slas::PolicyPresenter.new(current_user,
        url_builder: self,
        includes:    includes)
    end
  end

  def verify_feature!
    deny_access unless current_account.has_service_level_agreements?
  end

  def check_permissions
    deny_access unless current_user.can?(:edit, Access::Settings::BusinessRules)
  end

  def create_params
    params.require(:sla_policy).tap do |params|
      %i[title filter].each(&params.method(:require))
    end.with_indifferent_access
  end

  def update_params
    params.require(:sla_policy).with_indifferent_access
  end

  def render_client_error(error)
    render json: {error: error.message}, status: :unprocessable_entity
  end

  def render_limit_error(error)
    render json: {error: error.message}, status: :forbidden
  end
end
