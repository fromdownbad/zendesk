module Api
  module V2
    class DevicesController < Api::V2::BaseController
      before_action :require_agent!

      allow_parameters :all, user_id: Parameters.bigid | Parameters.enum('me')

      ## ### List Devices
      ## `GET /api/v2/users/me/devices.json`
      ##
      ##  Returns a list of all devices for the current user.
      ##
      ## #### Allowed For
      ##
      ##  * Admins, Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/me/devices.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "devices": [
      ##     {
      ##       "id":           16439,
      ##       "user_id":      293864,
      ##       "name":         "Work Phone",
      ##       "ip":           "209.119.38.228",
      ##       "user_agent":   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) ... Safari/537.31",
      ##       "location":     "San Francisco, USA",
      ##       "created_at":   "2013-02-20T22:55:29Z",
      ##       "updated_at":   "2013-04-05T10:38:52Z"
      ##     }
      ##   ]
      ## }
      ## ```
      ##
      allow_parameters :index, {}
      def index
        render json: presenter.present(devices)
      end

      ## ### Updating Devices
      ## `PUT /api/v2/users/me/devices/{id}.json`
      ##
      ## Allows agents to update the `name` of a device. No other fields
      ## can be updated through the API.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/me/devices/{id}.json \
      ##   -H "Content-Type: application/json" \
      ##   -d '{"device": {"name":"Home Laptop"}}' \
      ##   -v -u {email_address}:{password} -X PUT
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "id":           16439,
      ##   "user_id":      293864,
      ##   "name":         "Home Laptop",
      ##   "ip":           "209.119.38.228",
      ##   "user_agent":   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) ... Safari/537.31",
      ##   "location":     "San Francisco, USA",
      ##   "created_at":   "2013-02-20T22:55:29Z",
      ##   "updated_at":   "2013-05-20T14:19:17Z"
      ## }
      ## ```
      allow_parameters :update, id: Parameters.bigid, device: { name: Parameters.string }
      def update
        device_params = params.require(:device).permit(:name)
        if device.update_attributes(device_params)
          render json: presenter.present(device)
        else
          render json: presenter.present_errors(device), status: :unprocessable_entity
        end
      end

      ## ### Deleting Devices
      ## `DELETE /api/v2/users/me/devices/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/me/devices/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      ##
      resource_action :destroy

      private

      def device
        @device ||= devices.find(params[:id])
      end

      def devices
        invalid_mobile_device_ids = current_user.mobile_devices.where(oauth_token_id: nil, account_id: current_account)

        @devices ||= if current_account.has_filter_mobile_devices?
          paginate(current_user.devices.where.not(id: invalid_mobile_device_ids).where(account_id: current_account).order('last_active_at desc'))
        else
          paginate(current_user.devices.where(account_id: current_account).order('last_active_at desc'))
        end
      end

      def presenter
        @presenter ||= Api::V2::DevicePresenter.new(
          current_user,
          url_builder: self,
          device: current_device
        )
      end
    end
  end
end
