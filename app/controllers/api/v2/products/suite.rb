class Api::V2::Products::Suite
  EXPLORE_INSIGHTS_WHITELIST = ['explore', 'good_data_and_explore'].freeze

  Product = Struct.new(:key, :available, :label)

  attr_reader :account, :user

  def initialize(account, user)
    @account = account
    @user = user
  end

  def available
    products.select(&:available)
  end

  def products
    [
      Product.new(:lotus, true),

      Product.new(:guide, true),

      Product.new(:gather, gather_visible?),

      Product.new(:chat, Arturo.feature_enabled_for?(:chat_product_tray, account)),

      Product.new(:talk, Arturo.feature_enabled_for?(:talk_product_tray, account)),

      Product.new(
        :explore,
        if Arturo.feature_enabled_for?(:explore_product_tray_insights, account)
          explore_new
        else
          explore_old
        end,
        Arturo.feature_enabled_for?(:explore_ga, account) ? nil : 'beta'
      ),

      Product.new(
        :outbound,
        access_to_connect?
      ),

      Product.new(:sell, Arturo.feature_enabled_for?(:sell_product_tray, account)),

      Product.new(:sunshine, Arturo.feature_enabled_for?(:sunshine_product_tray, account)),

      Product.new(:central_admin, access_to_central_admin?)
    ]
  end

  def access_to_central_admin?
    user_is_admin_or_owner?
  end

  def user_is_admin_or_owner?
    user.is_account_owner? || user_is_multiproduct_admin?
  end

  def user_is_multiproduct_admin?
    cache_key = "product_tray/multiproduct/entitlements/#{account.id}/#{user.id}"

    active_entitlements = Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
      statsd.increment('staff_service_entitlement_api_calls')
      Zendesk::StaffClient.new(account).get_entitlements!(user.id)
    end

    support_entitlement = if active_entitlements
      active_entitlements.fetch(:support, nil)
    end

    # A user with a Support custom role, even with an Explore, Guide or Talk admin
    # role, must not be treated as a multiproduct admin
    effective_entitlements = if support_entitlement && support_entitlement.start_with?('custom_')
      active_entitlements.except(:guide, :explore, :talk)
    else
      active_entitlements
    end

    effective_entitlements.values.any?('admin')
  end

  def access_to_connect?
    (user.is_admin? && account_has_outbound_product_tray?) ||
    (user.is_admin? && account_has_outbound_product?) ||
    (user.is_contributor? && account_has_outbound_product?)
  end

  private

  def explore_new
    begin
      support_product = account_products.find { |p| p.name == Zendesk::Accounts::Client::SUPPORT_PRODUCT.to_sym }
    rescue Exception => e # rubocop:disable Lint/RescueException
      Rails.logger.warn("Failed to get support product: #{e.message}")
      return false
    end
    !support_product.nil? && EXPLORE_INSIGHTS_WHITELIST.include?(support_product.plan_settings['insights'])
  end

  def explore_old
    Arturo.feature_enabled_for?(:bime_integration, account) &&
    (user.tags.include?('zdbetafeature_bime_integration') || user.user_seats.explore.exists?)
  end

  def statsd
    @statsd ||= Zendesk::StatsD::Client.new(namespace: 'product_tray')
  end

  def account_has_outbound_product_tray?
    Arturo.feature_enabled_for?(:outbound_product_tray, account)
  end

  def account_has_outbound_product?
    account.products.select { |product| product.name == :outbound }.any?
  end

  def gather_visible?
    !account.spp? || account_products.find { |p| p.name == Zendesk::Accounts::Client::GATHER_PRODUCT.to_sym }&.active?
  end

  def account_products
    @account_products ||= account.products(use_cache: true)
  end
end
