class Api::V2::Ipm::FeatureNotificationsController < Api::V2::BaseController
  ## ### Listing FeatureNotifications
  ## `GET /api/v2/ipm/feature_notifications.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ipm/feature_notifications.json \
  ##   -v -u {email_address}:{token}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "feature_notifications": [
  ##     {
  ##       "id": 2127301143,
  ##       "title": "New Feature!",
  ##       "body": "Brand new feature!",
  ##       "call_to_action_text": "Click me",
  ##       "call_to_action_url": "http://feature_notifications.io",
  ##       "updated_at": "2011/09/25 22:35:44 -0700",
  ##       "created_at": "2011/09/25 22:35:44 -0700"
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(feature_notifications)
  end

  ## ### Dismissing FeatureNotifications
  ## `DELETE /api/v2/ipm/feature_notifications/{id}.json
  ##
  ## #### Allowed For
  ##
  ## * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ipm/feature_notifications/{id}.json \
  ##   -X DELETE \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    if is_assumed?
      head :not_modified
    elsif feature_notification

      # skip dismiss for test_mode ipms
      if feature_notification.test_mode
        head :not_modified
        return
      end

      ::Ipm::FeatureNotificationDismissal.create!(
        feature_notification: feature_notification,
        account: current_account,
        user: current_user
      )
      default_delete_response
    else
      head :not_found
    end
  end

  private

  def is_assumed? # rubocop:disable Naming/PredicateName
    is_assuming_user? || is_assuming_user_from_monitor?
  end

  def feature_notifications
    @feature_notifications ||= ::Ipm::FeatureNotification.for_user(current_user)
  end

  def feature_notification
    @feature_notification ||= begin
      feature_notification = ::Ipm::FeatureNotification.find(params[:id].to_i)
      feature_notification if feature_notification.show_for_user?(current_user)
    end
  end

  def presenter
    @presenter ||= Api::V2::Ipm::FeatureNotificationPresenter.new(current_user, url_builder: self)
  end
end
