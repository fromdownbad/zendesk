require 'zendesk_cross_origin/preflight_controller'

class Api::V2::CorsController < ZendeskCrossOrigin::PreflightController
  include Zendesk::Auth::AuthenticatedSessionMixin

  protected

  def current_user
    env['warden'].user
  end
end
