class Api::V2::ZopimIntegrationsController < Api::V2::BaseController
  before_action :require_absent_integration!,    only: :create
  before_action :require_phase_one_integration!, only: :destroy

  skip_before_action :enforce_support_is_active!, only: :show

  allow_subsystem_user :zopim
  allow_subsystem_user :monitor
  allow_subsystem_user :billing,    only: [:show]
  allow_subsystem_user :embeddable, only: [:show]
  allow_subsystem_user :sell, only: [:show]

  # #### Get Zopim Integration
  #
  # `GET /api/v2/zopim_integration.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/zopim_integration.json -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200
  #
  # {
  #   "zopim_integration": {
  #     "external_zopim_id": 12345678,
  #     "zopim_key": "abcdefg",
  #     "phase":      2,
  #     "agent_sync": true,
  #     "billing":    true,
  #     "created_at": ...
  #     "updated_at": ...
  #   }
  # }
  # ```
  allow_parameters :show, {}
  def show
    render json: presenter.present(current_zopim_integration)
  end

  # #### Create Zopim Integration
  #
  # `POST /api/v2/zopim_integration.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/zopim_integration.json -v -u {email_address}:{password} \
  #   -H "Content-Type: application/json" -X POST -d '{"zopim_integration": {"external_zopim_id": 12345678,"zopim_key": "abcdefg"}}'
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 201 Created
  # Location: /api/v2/zopim_integration.json
  #
  # {
  #   "zopim_integration": {
  #     "external_zopim_id": 12345678,
  #     "zopim_key": "abcdefg",
  #     "phase":      2,
  #     "agent_sync": true,
  #     "billing":    true,
  #     "created_at": ...
  #     "updated_at": ...
  #   }
  # }
  # ```
  allow_parameters :create, zopim_integration: {
    external_zopim_id: Parameters.integer,
    zopim_key: Parameters.string
  }
  def create
    current_account.create_zopim_integration!(params[:zopim_integration])
    render(
      json: presenter.present(current_zopim_integration),
      status: :created,
      location: presenter.url(current_zopim_integration)
    )
  end

  # #### Update Zopim Integration
  #
  # `PUT /api/v2/zopim_integration.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/zopim_integration.json -v -u {email_address}:{password} \
  #   -H "Content-Type: application/json" -X PUT -d '{"zopim_integration": {"external_zopim_id": 87654321,"zopim_key": "abcdefg"}}'
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200
  #
  # {
  #   "zopim_integration": {
  #     "external_zopim_id": 87654321,
  #     "zopim_key": "abcdefg",
  #     "phase":      2,
  #     "agent_sync": true,
  #     "billing":    true,
  #     "created_at": ...
  #     "updated_at": ...
  #   }
  # }
  # ```
  allow_parameters :update, zopim_integration: {
    external_zopim_id: Parameters.integer,
    zopim_key: Parameters.string,
    phase: Parameters.integer
  }
  def update
    current_zopim_integration.update_attributes!(params[:zopim_integration])
    render json: presenter.present(current_zopim_integration)
  end

  # #### Delete Zopim Integration
  #
  # `DELETE /api/v2/zopim_integration.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/zopim_integration.json -v -u {email_address}:{password} -X DELETE
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 204 No Content
  # ```
  allow_parameters :destroy, {}
  def destroy
    current_zopim_integration.destroy
    default_delete_response
  end

  private

  def presenter
    @presenter ||= Api::V2::ZopimIntegrationPresenter.new(current_user, url_builder: self)
  end

  def current_zopim_integration
    @current_zopim_integration ||= current_account.zopim_integration || raise(ActiveRecord::RecordNotFound)
  end

  def require_absent_integration!
    return unless current_account.zopim_integration.present?
    render status: :bad_request, text: 'Zopim integration already exists'
  end

  def require_phase_one_integration!
    return if current_zopim_integration.phase_one?
    render(
      status: :unprocessable_entity,
      json: { domain: 'Zopim', error: 'ZopimPhaseOneRequired' }
    )
  end
end
