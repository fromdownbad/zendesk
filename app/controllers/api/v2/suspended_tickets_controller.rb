require 'zendesk/tickets/recoverer'

class Api::V2::SuspendedTicketsController < Api::V2::BaseController
  before_action :require_unrestricted_agent!

  ## ### List Suspended Tickets
  ## `GET /api/v2/suspended_tickets.json`
  ##
  ## #### Allowed For
  ##
  ##  * Unrestricted agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/suspended_tickets.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Sorting
  ##
  ## You can sort the tickets with the `sort_by` and `sort_order` query string parameters.
  ##
  ## The `sort_by` parameter accepts one of the following values: `author_email`, `cause`, `created_at`, or `subject`.
  ##
  ## The `sort_order` parameter accepts one of the following values: `asc` or `desc`.
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "suspended_tickets": [
  ##     {
  ##       "id":      3436,
  ##       "subject": "Help I need somebody!",
  ##       "cause":   "Detected as spam",
  ##       ...
  ##     },
  ##     {
  ##       "id":      207623,
  ##       "subject": "Not just anybody!",
  ##       "cause":   "Automated response mail",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  allow_cursor_pagination_v2_parameters :index
  def index
    render json: presenter.present(suspended_tickets)
  end

  ## ### Show Suspended Ticket
  ## `GET /api/v2/suspended_tickets/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Unrestricted agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/suspended_tickets/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "suspended_ticket": {
  ##     {
  ##       "id":      35436,
  ##       "subject": "My printer is on fire!",
  ##       "cause":   "Automated response mail",
  ##       ...
  ##     }
  ##   }
  ## }
  ## ```
  resource_action :show

  ## ### Recover Suspended Ticket
  ## `PUT /api/v2/suspended_tickets/{id}/recover.json`
  ##
  ## Note: During recovery, the API sets the requester to the authenticated agent who called the API, not the original requester. This prevents the ticket from being re-suspended after recovery. To preserve the original requester, use the [Recover Multiple Suspended Tickets](#recover-multiple-suspended-tickets) endpoint with the single ticket.
  ##
  ## #### Allowed For
  ##
  ##  * Unrestricted agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/suspended_tickets/{id}/recover.json \
  ##   -X PUT -v -u {email_address}:{password} -d '{}' -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "ticket": {
  ##     {
  ##       "id":      35436,
  ##       "subject": "My printer is on fire!",
  ##       ...
  ##     }
  ##   }
  ## }
  ## ```
  allow_parameters :recover, id: Parameters.bigid, author: {
    id: Parameters.bigid,
    name: Parameters.string,
    email: Parameters.string
  }
  require_oauth_scopes :recover, scopes: [:write], arturo: true
  def recover
    recovering_user = if current_account.has_preserve_recovered_ticket_owner?
      author
    else
      new_ticket_author
    end

    if [SuspensionType.FROM_SUPPORT_ADDRESS, SuspensionType.UNKNOWN_AUTHOR].include? suspended_ticket.cause
      suspended_ticket.from_mail = recovering_user.try(:email) || current_user.email
    end

    ticket_recovered = recoverer.recover_ticket(suspended_ticket, recovering_user)
    render json: recovered_ticket_presenter.present(ticket_recovered)
  rescue Zendesk::Tickets::Recoverer::RecoveryFailed
    head :unprocessable_entity
  end

  # Recover Suspended Ticket Manually
  # `POST /api/v2/suspended_tickets/{id}/attachments.json`
  #
  # Takes a suspended ticket id and makes copies of all the attachments on the original ticket and
  # returns them with a token so that the tokens can be included on the new ticket when it is maually recovered
  #
  # Allowed For
  #
  #  * Unrestricted agents
  #
  # Using curl
  #
  # curl https://{subdomain}.zendesk.com/api/v2/suspended_tickets/{id}/attachments.json \
  #   -X POST -v -u {email_address}:{password}
  #
  # Example Response
  #
  # Status: 200 OK
  #
  # {
  #   "upload": {
  #     "token": "yrznqgjoa24iw2f",
  #     "attachments": [
  #        {
  #          "url":          "http://dev.localhost:3000/api/v2/attachments/367.json",
  #          "id":           367,
  #          "file_name":    "invite.ics",
  #          "content_url":  "http://dev.localhost:3000/attachments/token/dmjeehb91xeyw8i/?name=invite.ics",
  #          "content_type": "application/ics",
  #          "size":         1166,
  #          "thumbnails":   []
  #        }
  #     ]
  #   }
  # }
  allow_parameters :attachments, id: Parameters.bigid
  require_oauth_scopes :attachments, scopes: [:write], arturo: true
  def attachments
    upload_token = suspended_ticket.copy_attachments_to_upload_token
    render json: upload_presenter.present(upload_token)
  end

  ## ### Recover Multiple Suspended Tickets
  ## `PUT /api/v2/suspended_tickets/recover_many.json?ids={id1},{id2}`
  ##
  ## Accepts up to 100 ticket ids. Note that suspended tickets that fail to be recovered
  ## are still included in the response.
  ##
  ## #### Allowed For
  ##
  ##  * Unrestricted agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/suspended_tickets/recover_many.json?ids={id1},{id2} \
  ##   -H "Content-Type: application/json" -X PUT -d '{}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":      35436,
  ##       "subject": "Help I need somebody!",
  ##       ...
  ##     },
  ##     {
  ##       "id":      20057623,
  ##       "subject": "Not just anybody!",
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :recover_many, ids: Parameters.string, author: {
    id: Parameters.bigid,
    email: Parameters.string
  }
  require_oauth_scopes :recover_many, scopes: [:write], arturo: true
  def recover_many
    many_recovered_tickets = recoverer.recover(many_suspended_tickets)

    render json: recovered_ticket_presenter.present(many_recovered_tickets)
  rescue Zendesk::Tickets::Recoverer::RecoveryFailed => e
    render json: presenter.present(e.failed_tickets), status: :unprocessable_entity
  end

  allow_parameters :bulk_recover, ids: Parameters.string, author: {
    id: Parameters.bigid,
    email: Parameters.string
  }
  require_oauth_scopes :bulk_recover, scopes: [:write], arturo: true
  def bulk_recover
    unless params[:ids].present?
      raise Zendesk::UnknownAttributeError, "missing parameters"
    end

    if !(params[:ids] =~ /^[\d+\,]+$/)
      head :unprocessable_entity
    else
      render json: job_status_presenter.present(enqueue_job_with_status(SuspendedTicketsBulkRecoveryJob, params))
    end
  end

  ## ### Delete Suspended Ticket
  ## `DELETE /api/v2/suspended_tickets/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Unrestricted agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/suspended_tickets/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    suspended_ticket.soft_delete!
    default_delete_response
  end

  ## ### Delete Multiple Suspended Tickets
  ## `DELETE /api/v2/suspended_tickets/destroy_many.json?ids={id1},{id2}`
  ##
  ## Accepts up to 100 ticket ids.
  ##
  ## #### Allowed For
  ##
  ##  * Unrestricted aAgents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/suspended_tickets/destroy_many.json?ids={id1},{id2} \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy_many, ids: Parameters.string
  def destroy_many
    SuspendedTicket.bulk_delete(current_account, many_ids) if many_ids?
    default_delete_response
  end

  protected

  def presenter
    @presenter ||= Api::V2::SuspendedTicketPresenter.new(current_user, url_builder: self, with_cursor_pagination: use_cursor_based_pagination?)
  end

  def recovered_ticket_presenter
    @recovered_ticket_presenter ||= Api::V2::Tickets::TicketPresenter.new(current_user, url_builder: self)
  end

  def upload_presenter
    @upload_presenter ||= Api::V2::UploadPresenter.new(current_user, url_builder: self)
  end

  def job_status_presenter
    @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def suspended_tickets
    @suspended_tickets ||= begin
      if use_cursor_based_pagination?
        scope.reorder(id: :asc).paginate_with_cursor(
          cursor_pagination_version: 2,
          page: params[:page]
        )
      else
        paginate(scope)
      end
    end
  end

  def use_cursor_based_pagination?
    # Remove this conditional after we rollout 'email_cursor_pagination_suspended_tickets_index'.
    #
    # If we receive a '?page[:size]=2' request before we enable the
    # feature, we want to show the same error message we were showing
    # before adding the new parameter types.
    if !current_account.has_email_cursor_pagination_suspended_tickets_index? && HashParam.ish?(params[:page])
      raise StrongerParameters::InvalidParameter.new(
        StrongerParameters::IntegerConstraint.new.value(params[:page]),
        'page'
      )
    end

    current_account.has_email_cursor_pagination_suspended_tickets_index? &&
        with_cursor_pagination_v2?
  end

  def suspended_ticket
    @suspended_ticket ||= scope.find(params[:id])
  end

  def many_suspended_tickets
    @many_suspended_tickets ||= scope.where(id: many_ids).to_a
  end

  def scope
    @scope ||= current_account.suspended_tickets
  end

  def recoverer
    @recoverer ||= Zendesk::Tickets::Recoverer.new(user: new_ticket_author, account: current_account)
  end

  def author
    @author ||= (params[:author] && (current_account.users.find_by_id(params[:author][:id].to_i) || current_account.find_user_by_email(params[:author][:email].to_s)))
  end

  def new_ticket_author
    author || current_user
  end

  def require_unrestricted_agent!
    deny_access unless current_user.is_non_restricted_agent?
  end

  def sort_map
    return unless (map = super)
    @sort_map ||= map.merge("cause" => "FIELD(cause,#{SuspensionType.order.join(",")})")
  end

  SORT_LOOKUP = {
    "created_at" => "created_at",
    "subject" => "subject",
    "author" => "from_name",
    "author.email" => "from_mail",
    "author_email" => "from_mail",
  }.freeze
end
