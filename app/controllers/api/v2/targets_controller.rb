class Api::V2::TargetsController < Api::V2::BaseController
  class ExceededTrialAccountTargetLimitError < StandardError; end

  include Zendesk::Targets::Helper

  require_that_user_can! :edit, Target, only: [:create, :update, :destroy]

  require_capability :targets

  possible_plaintext_attributes = Target.types.flat_map(&:setting_writers).uniq - [:url] + [:target_url]
  possible_encrypted_attributes = Target.types.flat_map(&:encrypted_setting_writers).uniq

  possible_attributes = possible_plaintext_attributes + possible_encrypted_attributes

  possible_attributes = possible_attributes.each_with_object({}) do |obj, memo|
    memo[obj] = Parameters.string | Parameters.nil
  end

  TARGET_ATTRIBUTES = possible_attributes.merge(
    title: Parameters.string,
    active: Parameters.boolean,
    type: Parameters.string
  ).freeze

  rescue_from Zendesk::Targets::Initializer::InvalidTargetType do |exception|
    render json: { error: "InvalidTargetType", description: "Invalid target type: #{exception.message}" }, status: :bad_request
  end

  rescue_from ExceededTrialAccountTargetLimitError do |exception|
    render json: { error: "ExceededTrialAccountTargetLimitError", description: "Reached target limit: #{exception.message}" }, status: :bad_request
  end

  ## ### List Targets
  ## `GET /api/v2/targets.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/targets.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "targets": [
  ##     {
  ##       "title":      "Fancy box",
  ##       "type":       "basecamp_target"
  ##       "active":     true
  ##       "created_at": "2009-05-13T00:07:08Z",
  ##       "id":         211
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  require_oauth_scopes :index, scopes: [:"targets:read", :read]
  def index
    presenter = Api::V2::TargetPresenter.new(current_user, url_builder: self)
    render json: presenter.present(current_account.targets)
  end

  ## ### Show Target
  ## `GET /api/v2/targets/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/targets/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "target": {
  ##     "title":      "Fancy box",
  ##     "type":       "basecamp_target"
  ##     "active":     true
  ##     "created_at": "2009-05-13T00:07:08Z",
  ##     "id":         211
  ##   }
  ## }
  ## ```
  require_oauth_scopes :show, scopes: [:"targets:read", :read]
  resource_action :show

  ## ### Create Target
  ## `POST /api/v2/targets.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/targets.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '{"target": {"type": "email_target", "title": "Test Email Target", "email": "hello@example.com", "subject": "Test Target"}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/targets/{id}.json
  ##
  ## {
  ##   "target": {
  ##     "type":        "email_target",
  ##     "title":       "Test Email Target",
  ##     "active":      true,
  ##     "email":       "hello@example.com",
  ##     "subject":     "Test Target",
  ##     "created_at":  "2009-05-13T00:07:08Z"
  ##   }
  ## }
  ## ```
  allow_parameters :create, target: TARGET_ATTRIBUTES
  allow_parameters :exceed_maximum_trial_targets_limit?, account: Parameters.anything
  require_oauth_scopes :create, scopes: [:"targets:write", :write]
  before_action :verify_exceed_trial_limit, only: [:create]
  def create
    params.require(:target)
    target = initializer.build(params[:target])
    target.save!
    render json: presenter.present(target), status: :created
  end

  ## ### Update Target
  ## `PUT /api/v2/targets/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/targets/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{"target": {"email": "roger@example.com"}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "target": {
  ##     "type":        "email_target",
  ##     "title":       "Test Email Target",
  ##     "active":      true,
  ##     "email":       "roger@example.com",
  ##     "subject":     "Test Target",
  ##     "created_at":  "2009-05-13T00:07:08Z"
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.bigid, target: TARGET_ATTRIBUTES
  require_oauth_scopes :update, scopes: [:"targets:write", :write]
  def update
    params.require(:target)
    initializer.set(target, params[:target]).save!
    render json: presenter.present(target)
  end

  ## ### Delete Target
  ## `DELETE /api/v2/targets/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/targets/{id}.json \
  ##   -H "Content-Type: application/json" -X DELETE \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  require_oauth_scopes :destroy, scopes: [:"targets:write", :write]
  resource_action :destroy

  private

  def initializer
    Zendesk::Targets::Initializer.new(account: current_account, via: :api)
  end

  def target
    @target ||= current_account.targets.find(params[:id]).tap do |target|
      target.current_user = current_user
    end
  end

  def presenter
    @presenter ||= Api::V2::TargetPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def verify_exceed_trial_limit
    message = 'Trial accounts are limited to a maximum of two targets'
    raise ExceededTrialAccountTargetLimitError.new, message if exceed_maximum_trial_targets_limit?(current_account)
  end
end
