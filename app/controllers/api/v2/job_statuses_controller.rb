class Api::V2::JobStatusesController < Api::V2::BaseController
  include JobWithStatusTracking

  before_action      :ensure_status, only: :show
  before_action      :ensure_permission, only: :show

  allow_subsystem_user :zopim, only: [:show, :show_many]

  ## ### List Job Statuses
  ## `GET /api/v2/job_statuses.json`
  ##
  ## Shows the current statuses for background jobs running.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/job_statuses.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_statuses": [
  ##     {
  ##       "id": "8b726e606741012ffc2d782bcb7848fe",
  ##       "status": "completed",
  ##       ...
  ##     },
  ##     {
  ##      "id": "e7665094164c498781ebe4c8db6d2af5",
  ##      "status": "completed",
  ##      ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    statuses = many_statuses(current_account_job_ids)
    render json: presenter.present(statuses)
  end

  ## ### Show Job Status
  ## `GET /api/v2/job_statuses/{id}.json`
  ##
  ## Shows the status of a background job.
  ##
  ## A job may no longer exist to query. Zendesk only logs the last 100 jobs. Jobs also expire within an hour.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/job_statuses/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_status": {
  ##     "id":       "8b726e606741012ffc2d782bcb7848fe",
  ##     "url":      "https://company.zendesk.com/api/v2/job_statuses/8b726e606741012ffc2d782bcb7848fe.json",
  ##     "total":    2,
  ##     "progress": 2,
  ##     "status":   "completed",
  ##     "message":  "Completed at Fri Apr 13 02:51:53 +0000 2012",
  ##     "results": [
  ##       {
  ##         "title":   "I accidentally the whole bottle",
  ##         "action":  "update",
  ##         "errors":  "",
  ##         "id":      380,
  ##         "success": true,
  ##         "status":  "Updated"
  ##       },
  ##       ...
  ##     ]
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.string
  def show
    render json: presenter.present(status)
  end

  ## ### Show Many Job Statuses
  ##
  ## `GET /api/v2/job_statuses/show_many.json?ids={ids}`
  ##
  ## Accepts a comma-separated list of job status ids.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/job_statuses/show_many.json?ids=8b726e606741012ffc2d782bcb7848fe,e7665094164c498781ebe4c8db6d2af5,3f3a876fb3354766b74a21066007ffee \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_statuses": [
  ##     {
  ##       "id": "8b726e606741012ffc2d782bcb7848fe",
  ##       "status": "completed",
  ##       ...
  ##     },
  ##     {
  ##      "id": "e7665094164c498781ebe4c8db6d2af5",
  ##      "status": "completed",
  ##      ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :show_many, ids: Parameters.string
  def show_many
    params[:uuids] = params[:ids]
    statuses = many_statuses(many_ids(:uuids))
    if statuses.any? { |status| status.options["account_id"] != current_account.id }
      head :forbidden
    else
      render json: presenter.present(statuses)
    end
  end

  private

  def presenter
    @presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def status
    @status ||= Resque::Plugins::Status::Hash.get(params[:id])
  end

  def many_statuses(ids)
    ids.map { |id| Resque::Plugins::Status::Hash.get(id) }.compact
  end

  def current_account_job_ids
    @job_ids ||= Zendesk::RedisStore.client.lrange(job_status_tracking_redis_key, 0, -1)
  end

  def ensure_status
    head :not_found unless status.present? && status.options.present?
  end

  def ensure_permission
    if status.options["account_id"] != current_account.id
      head :forbidden
    end
  end
end
