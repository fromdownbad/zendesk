class Api::V2::FormatController < Api::V2::BaseController
  include EmojiHelper

  ## ### Format Markdown
  ## `POST /api/v2/format/markdown.json?text={markdown}`
  ##
  ## Returns the input and the HTML rendered markdown.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/format/markdown.json \
  ##   -X POST -d '{"text": "*hello*"}' -H "Content-Type: application/json" \
  ##   -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "markdown": "*hello*",
  ##   "html":     "<p><em>hello</em></p>\n"
  ## }
  ## ```
  allow_parameters :markdown,
    text:           Parameters.string,
    ticket_id:      Parameters.bigid,
    ticket:         Parameters.anything,
    simple_format:  Parameters.boolean
  require_oauth_scopes :markdown, scopes: [:write], arturo: true
  def markdown
    params[:text] ||= ''
    text = original_text = params[:text].strip
    text = Zendesk::Liquid::TicketContext.render(
      text, ticket, current_user, true, locale
    ) if ticket

    html = params[:simple_format] ? text.simple_format : text.markdown
    emojify!(html) if current_account.has_emoji_autocompletion_enabled?

    render json: { markdown: original_text, html: html }
  end

  ## ### Format DynamicContent
  ## `POST /api/v2/format/dc.json?template={dc}&locale_id={locale_id}`
  ##
  ## Returns the input and the rendered dynamic content.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/format/dc.json \
  ##   -X POST -d '{"template": "{{dc.password_help}}", "locale_id": 16}' -H "Content-Type: application/json" \
  ##   -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "template": "{{dc.password_help}}",
  ##   "value":    "Allez ici pour réinitialiser le mot de passe"
  ## }
  ## ```

  allow_parameters :dc,
    template: Parameters.string,
    locale_id: Parameters.bigid,
    locale: Parameters.string
  require_oauth_scopes :dc, scopes: [:read], arturo: true
  def dc
    template = params[:template].strip
    locale   = TranslationLocale.find_by_id(params[:locale_id]) ||
               TranslationLocale.find_by_locale(params[:locale].strip)
    render json: {
      template: template,
      value: Zendesk::Liquid::DcContext.render(
        template, current_account, nil, 'text/plain', locale
      )
    }
  end

  private

  def locale
    (ticket.requester || ticket.account).translation_locale
  end

  def ticket
    @ticket ||= begin
      if params[:ticket]
        ticket_initializer.ticket
      else
        current_account.tickets.find_by_nice_id!(params[:ticket_id]) if params[:ticket_id]
      end
    end
  end

  def ticket_initializer
    @ticket_initializer ||= Zendesk::Tickets::Initializer.new(current_account, current_user, params[:ticket], include: [], request_params: request_params)
  end
end
