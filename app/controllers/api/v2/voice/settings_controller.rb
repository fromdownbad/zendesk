module Api
  module V2
    module Voice
      class SettingsController < Api::V2::BaseController
        allow_parameters :update, settings: Parameters.map(
          voice:                                    Parameters.boolean,
          voice_outbound_enabled:                   Parameters.boolean,
          voice_maximum_queue_size:                 Parameters.integer & Parameters.gte(0) & Parameters.lte(300),
          voice_maximum_queue_wait_time:            Parameters.integer & Parameters.gte(1) & Parameters.lte(120),
          voice_recordings_public:                  Parameters.boolean,
          voice_only_during_business_hours:         Parameters.boolean,
          voice_agent_wrap_up_after_calls:          Parameters.boolean,
          voice_agent_confirmation_when_forwarding: Parameters.boolean
        )
        def update
          current_account.settings.set(params.require(:settings))
          if current_account.settings.save
            head :ok
          else
            head :unprocessable_entity
          end
        end
      end
    end
  end
end
