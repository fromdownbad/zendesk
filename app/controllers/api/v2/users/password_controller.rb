require 'zendesk/users/user_manager'

class Api::V2::Users::PasswordController < Api::V2::BaseController
  skip_before_action :require_agent!, only: [:reset, :update, :requirements]

  require_that_user_can! :set_password, :user, only: :create
  require_that_user_can! :change_password, :user, only: :update

  before_action :set_active_brand_id,            only: [:update]
  before_action :ensure_user_can_reset_password, only: :reset

  rescue_from Zendesk::UserManager::Error do
    I18n.with_locale(@locale_for_exception_rendering) do
      render json: errors_presenter.present(user), status: :unprocessable_entity
    end
  end

  ## ### Set a User's Password
  ##
  ## `POST /api/v2/users/{user_id}/password.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## An admin can set a user's password only if the setting is enabled under
  ## Settings > Security > Global. The setting is off by default. Only the
  ## account owner can access and change this setting in Zendesk Support.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/password.json \
  ##   -X POST -d '{"password": "newpassword"}' -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ## ```
  allow_parameters :create, user_id: Parameters.bigid, password: Parameters.string
  def create
    user_manager.reset_password_for(user, to: params[:password], verify_identity: true)

    head :ok
  end

  ## ### Change Your Password
  ##
  ## `PUT /api/v2/users/{user_id}/password.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##  * End Users
  ##
  ## You can only change your own password. Nobody can change the password
  ## of another user because it requires knowing the user's existing password.
  ## However, an admin can *set* a new password for another user without
  ## knowing the existing password. See "Set a User's Password" above.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/password.json \
  ##   -X PUT -d '{"previous_password": "oldpassword", "password": "newpassword"}' \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ## ```
  allow_parameters :update, user_id: Parameters.bigid, password: Parameters.string, previous_password: Parameters.string, brand_id: Parameters.bigid, session_record_id: Parameters.bigid
  def update
    user_manager.change_password_for(user,
      from: params[:previous_password],
      to: params[:password],
      shared_session_record_id: params[:session_record_id])

    head :ok
  end

  # Reset a User's Password
  #
  # This endpoint sends an email to the user that lets them create or reset a password,
  # depending on whether they already have a password or not.
  #
  # `POST /api/v2/users/{user_id}/password/reset.json`
  #
  # Allowed For
  #
  #  * Agents
  #  * End Users, with restrictions
  #
  # Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/password/reset.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # Example Response
  #
  # ```http
  # Status: 200
  # ```
  allow_parameters :reset, user_id: Parameters.bigid, brand_id: Parameters.bigid
  require_oauth_scopes :reset, scopes: [:write], arturo: true
  def reset
    token = if current_account.has_generate_password_reset_token_subclass?
      user.password_reset_tokens.create
    else
      user.verification_tokens.create
    end

    if user.crypted_password.nil?
      UsersMailer.deliver_admin_password_change(user, token.value, params[:brand_id])
    else
      UsersMailer.deliver_password_reset(user, token.value, params[:brand_id])
    end

    head :ok
  end

  ## ### Get a list of password requirements
  ##
  ## `GET /api/v2/users/{user_id}/password/requirements.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##  * End Users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/password/requirements.json \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "requirements": [
  ##     "must be at least 5 characters",
  ##     "must be different from email address"
  ##   ]
  ## }
  ## ```
  allow_parameters :requirements, user_id: Parameters.bigid
  require_oauth_scopes :requirements, scopes: [:read], arturo: true
  def requirements
    security_requirements = user.security_policy.validations_in_words[:password].compact
    render json: { requirements: security_requirements}
  end

  protected

  def set_active_brand_id
    user.active_brand_id = params[:brand_id]
  end

  def errors_presenter
    @errors_presenter ||= Api::V2::ErrorsPresenter.new
  end

  def user
    @user ||= current_account.users.find(params[:user_id])
  end

  def user_manager
    @user_manager ||= Zendesk::UserManager.new(authentication)
  end

  def ensure_user_can_reset_password
    unless current_user.can?(:request_password_reset, user) || current_user.can?(:create_password, user)
      raise Access::Denied.new("Not authorized!", :request_password_reset, user)
    end
  end
end
