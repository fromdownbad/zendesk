class Api::V2::Users::UserSeatsController < Api::V2::BaseController
  BULK_UPDATE_LIMIT = 50

  NO_ROLE = 0

  before_action :require_admin!, only: [:create, :destroy, :all_user_seats, :voice_assignable_agents]
  before_action :limit_bulk_update, only: [:all_user_seats]

  ## ### Show a list of user seats that exists
  ##
  ## `GET /api/v2/users/:user_id/user_seats.json`
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{:user_id}/user_seats.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Responses
  ## if user is not on the account
  ## ```http
  ## Status: 404
  ## {
  ##   "error":"RecordNotFound",
  ##   "description":"Not found"
  ## }
  ## ```
  ## if user does not have any user_seats
  ## ```http
  ## {
  ##   "user_seats": []
  ## }
  ## ```
  ## if user has user_seats
  ## ```http
  ## Status: 200
  ## {
  ##   "user_seats":
  ##     [
  ##       {
  ##         "seat_type": "voice",
  ##         "role": 0
  ##       }
  ##       {
  ##         "seat_type": "chat",
  ##         "role": 1
  ##       }
  ##     ]
  ## }
  ## ```
  ##
  allow_parameters :index, user_id: Parameters.bigid
  def index
    render json: { user_seats: seats_presenter }
  end

  ## ### Create or update a user_seat for the user_id in the url
  ## `POST /api/v2/users/:user_id/user_seats.json`
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{:user_id}/user_seats.json\
  ## -X POST \
  ## -v -u {email_address}:{password}
  ## -H "Content-Type: application/json"
  ## -d '{"user_seat" : {"seat_type": "connect", "role": 1 }}'
  ## ````
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 SUCCESS
  ## ```
  ##
  allow_parameters :create,
    user_id: Parameters.bigid,
    user_seat: { seat_type: Parameters.string, role: Parameters.integer }
  def create
    if user_seat.present? && role_unchanged?
      # no change
      head :no_content
      return
    end

    status = :ok
    entitlement = Zendesk::SupportUsers::Entitlement.for(user, seat_type)
    if entitlement.grant!
      seat = user.user_seats.where(seat_type: seat_type).first
      content = seat_presenter.present(seat)
    else
      content = seat_presenter.present_errors(entitlement.user_seat)
      status = :unprocessable_entity
    end

    render json: content, status: status
  end

  ## ### Delete user_seat for the user_id and seat type in the url
  ## `DELETE /api/v2/users/:user_id/user_seats.json`
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{:user_id}/user_seats.json\
  ## -X DELETE \
  ## -v -u {email_address}:{password}
  ## -H "Content-Type: application/json"
  ## -d '{"user_seat" : {"seat_type": "voice"}}'
  ## ````
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  allow_parameters :destroy,
    user_id: Parameters.bigid,
    user_seat: { seat_type: Parameters.string }
  def destroy
    if user_seat.blank?
      head :no_content
    else
      entitlement = Zendesk::SupportUsers::Entitlement.for(user, seat_type)
      if entitlement.revoke!
        default_delete_response
      else
        render json: seat_presenter.present_errors(entitlement.user_seat), status: :unprocessable_entity
      end
    end
  end

  allow_parameters :all_user_seats,
    user_ids: Parameters.array(Parameters.integer),
    user_seat: { seat_type: Parameters.string }
  require_oauth_scopes :all_user_seats, scopes: [:write], arturo: true
  def all_user_seats
    first_failing = nil
    seats = nil

    UserSeat.transaction do
      seats = UserSeat.create(seat_hashes)

      if seats.any?(&:invalid?)
        first_failing = seats.first(&:invalid?)
        raise ActiveRecord::Rollback
      end
    end

    if first_failing.nil? && seat_type == 'voice'
      seats.each do |seat|
        Omnichannel::TalkEntitlementSyncJob.enqueue(account_id: seat.account_id, user_id: seat.user_id)
      end
    end

    if first_failing.nil?
      render json: { user_seats: all_seats_presenter }, status: :created
    else
      render json: seat_presenter.present_errors(first_failing), status: :unprocessable_entity
    end
  end

  allow_parameters :voice_assignable_agents, {}
  require_oauth_scopes :voice_assignable_agents, scopes: [:read], arturo: true
  def voice_assignable_agents
    @users = paginate(current_account.assignable_agents, per_page: BULK_UPDATE_LIMIT)
    render json: user_presenter.present(@users)
  end

  private

  def limit_bulk_update
    head :unprocessable_entity if current_account.assignable_agents.count(:all) > BULK_UPDATE_LIMIT
  end

  def user
    @user ||= current_account.users.find(params[:user_id])
  end

  def seat_presenter
    Api::V2::Users::UserSeatPresenter.new(current_user, url_builder: self)
  end

  def user_presenter
    @presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self, includes: includes)
  end

  def seats_presenter
    seat_presenter.collection_presenter.model_json(user_seats)
  end

  def all_seats_presenter
    seat_presenter.collection_presenter.model_json(current_account.user_seats)
  end

  def seat_type
    params[:user_seat].try(:[], :seat_type)
  end

  def role
    return NO_ROLE if seat_type == 'voice'
    params[:user_seat].try(:[], :role)
  end

  def role_unchanged?
    user_seat.role == role
  end

  def user_seat
    @user_seat ||= current_account.user_seats.where(user_id: user.id, seat_type: seat_type).first
  end

  def user_seats
    current_account.user_seats.where(user_id: user.id)
  end

  def agents
    if params[:user_ids]
      current_account.assignable_agents.where(id: params[:user_ids])
    else
      current_account.assignable_agents
    end.includes(:user_seats).select { |u| u.user_seats.voice.blank? }
  end

  def seat_hashes
    agents.map do |agent|
      {
        account_id: current_account.id,
        seat_type:  seat_type,
        user_id:    agent.id
      }
    end
  end
end
