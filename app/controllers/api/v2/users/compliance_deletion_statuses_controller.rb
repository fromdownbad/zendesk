class Api::V2::Users::ComplianceDeletionStatusesController < Api::V2::BaseController
  skip_before_action :enforce_support_is_active!
  require_that_user_can! :delete, :user, unless: :system_user?

  allow_subsystem_users

  ## ### Show Compliance Deletion Statuses
  ##
  ## Returns the GDPR status for each user per area of compliance. A Zendesk area of compliance is typically a product like "support/explore" but can be more fine-grained for areas within the product lines.
  ##
  ## `GET /api/v2/users/{id}/compliance_deletion_statuses.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/compliance_deletion_statuses.json \
  ##   -v -u {email_address}:{password} -d '{"application": "chat"}'
  ## ```
  ##
  ## #### Example Responses
  ##
  ## If user is not on the account
  ##
  ## ```http
  ## Status: 404
  ## {
  ##   "error":"RecordNotFound",
  ##   "description":"Not found"
  ## }
  ## ```
  ##
  ## If user is not deleted
  ##
  ## ```http
  ## {
  ##   "compliance_deletion_statuses": []
  ## }
  ## ```
  ##
  ## If user has compliance_deletion_statuses
  ##
  ## ```http
  ## Status: 200
  ## {
  ##   "compliance_deletion_statuses":
  ##     [
  ##       {
  ##         "action": "request_deletion",
  ##         "application": "all",
  ##         "account_subdomain": "accountABC",
  ##         "executer_id": 2000,
  ##         "user_id": 1,
  ##         "created_at": "2009-07-20T22:55:23Z"
  ##       }
  ##       {
  ##         "action": "started",
  ##         "application": "support",
  ##         "account_subdomain": "accountABC",
  ##         "executer_id": '',
  ##         "user_id": 1,
  ##         "created_at": "2009-07-20T22:55:29Z"
  ##       }
  ##       {
  ##         "action": "complete",
  ##         "application": "support",
  ##         "account_subdomain": "accountABC",
  ##         "executer_id": '',
  ##         "user_id": 1,
  ##         "created_at": "2009-07-20T22:57:02Z"
  ##       }
  ##       {
  ##         "action": "started",
  ##         "application": "chat",
  ##         "account_subdomain": "accountABC",
  ##         "executer_id": '',
  ##         "user_id": 1,
  ##         "created_at": "2009-07-21T02:51:18Z"
  ##       }
  ##     ]
  ## }
  ## ```
  ##
  allow_parameters :index, user_id: Parameters.bigid, application: Parameters.string
  def index
    render json: { compliance_deletion_statuses: statuses_presenter }
  end

  private

  def system_user?
    current_user.is_system_user?
  end

  def user
    @user ||= current_account.all_users.inactive.find(params[:user_id])
  end

  def application
    return ComplianceDeletionStatus::CLASSIC if params[:application] == 'support'

    ComplianceDeletionStatus::APPLICATONS.detect { |app| app == params[:application] }
  end

  def compliance_deletion_statuses
    @compliance_deletion_statuses ||= begin
      initial_request = current_account.compliance_deletion_statuses.where(user: user).first

      if initial_request.nil?
        []
      else
        if application.present?
          initial_request.compliance_deletion_feedback.where(application: application)
        else
          [initial_request] + initial_request.compliance_deletion_feedback
        end
      end
    end
  end

  def statuses_presenter
    status_presenter.collection_presenter.model_json(compliance_deletion_statuses)
  end

  def status_presenter
    Api::V2::Users::ComplianceDeletionStatusPresenter.new(current_user, url_builder: self)
  end
end
