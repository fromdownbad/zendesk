class Api::V2::Users::EntitlementsController < Api::V2::BaseController
  before_action :require_agent!
  before_action :require_admin!, only: [:update, :update_full]

  ## ### Show Staff Entitlements
  ##
  ## `GET /api/v2/users/{id}/entitlements.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/entitlements.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "entitlements": {
  ##     "explore": "viewer",
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :show, user_id: Parameters.bigid
  require_oauth_scopes :show, scopes: %i[users:read read]
  def show
    entitlements = staff_client.get_entitlements!(user.id)
    render json: { entitlements: entitlements }
  rescue Kragle::ResourceNotFound
    raise ActiveRecord::RecordNotFound
  end

  ## ### Update Staff Entitlements
  ## `PUT /api/v2/users/{id}/entitlements.json`
  ##
  ## Note: Only the `explore` entitlement may be updated.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/entitlements.json \
  ##   -d '{"entitlements": {"chat": "agent"}}' \
  ##   -H "Content-Type: application/json" -X PUT  \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "entitlements": {
  ##     "explore": "viewer",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update, user_id: Parameters.bigid, entitlements: { explore: Parameters.string }
  require_oauth_scopes :update, scopes: %i[users:write write read]
  def update
    params.require(:entitlements)
    entitlements = staff_client.update_entitlements!(user.id, params[:entitlements], current_user.id)
    render json: { entitlements: entitlements }
  rescue Kragle::ResourceNotFound
    raise ActiveRecord::RecordNotFound
  rescue Kragle::UnprocessableEntity => e
    render status: :unprocessable_entity, json: { errors: e.response.body['errors'] }
  rescue Kragle::BadRequest => e
    render status: :bad_request, json: { errors: e.response.body['errors'] }
  end

  ## ### Show Full Staff Entitlements
  ##
  ## `GET /api/v2/users/{id}/entitlements/full`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/entitlements/full \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "entitlements": {
  ##     "chat": {
  ##       "name": "admin",
  ##       "is_active": "false"
  ##     },
  ##     "explore": {
  ##       "name": "viewer",
  ##       "is_active": "true"
  ##     },
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :full, user_id: Parameters.bigid
  require_oauth_scopes :full, scopes: %i[users:read read]
  def full
    entitlements = staff_client.get_full_entitlements!(user.id)
    render json: entitlement_presenter.present(entitlements)
  rescue Kragle::ResourceNotFound
    raise ActiveRecord::RecordNotFound
  end

  ## ### Update Staff Entitlements
  ## `PUT /api/v2/users/{id}/entitlements/update_full`
  ##
  ## Note: Only the `explore` entitlement may be updated.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/entitlements/update_full \
  ##   -d '{"entitlements": {"explore": { "name": "admin", "is_active": false }}}}' \
  ##   -H "Content-Type: application/json" -X PUT  \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "entitlements": {
  ##     "chat": {
  ##       "name": "admin",
  ##       "is_active": "false"
  ##     },
  ##     "explore": {
  ##       "name": "viewer",
  ##       "is_active": "true"
  ##     },
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update_full, user_id: Parameters.bigid, entitlements: { explore: { name: Parameters.string, is_active: Parameters.boolean } }
  require_oauth_scopes :update_full, scopes: %i[users:write write read]
  def update_full
    params.require(:entitlements)
    entitlements = staff_client.update_full_entitlements!(user.id, params[:entitlements], skip_validation: false)

    render json: entitlement_presenter.present(entitlements)
  rescue Kragle::ResourceNotFound
    raise ActiveRecord::RecordNotFound
  rescue Kragle::UnprocessableEntity => e
    render status: :unprocessable_entity, json: { errors: e.response.body['errors'] }
  rescue Kragle::BadRequest => e
    render status: :bad_request, json: { errors: e.response.body['errors'] }
  end

  protected

  def user
    current_account.agents.where(id: params[:user_id]).first!
  end

  def entitlement_presenter
    Api::V2::Users::EntitlementPresenter.new(current_user, url_builder: self)
  end

  private

  def staff_client
    @staff_client = Zendesk::StaffClient.new(current_account)
  end
end
