require 'zendesk/revere/has_api_client'

class Api::V2::Users::SettingsController < Api::V2::BaseController
  before_action :require_agent!

  include Zendesk::Revere::HasApiClient

  ## ### Show Settings
  ## `GET /api/v2/users/me/settings.json`
  ##
  ## This shows the settings that are available for the current user.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/me/settings.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## "settings": {
  ##   "lotus": {.. }
  ## }
  ## ```
  allow_parameters :show, {}
  def show
    render json: presenter.present(current_user)
  end

  ## ### Update User Settings
  ## `PUT /api/v2/users/me/settings.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/me/settings.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{ "settings": { "lotus": { "show_welcome_dialog": false }}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## "settings": {
  ##   "lotus": {.. }
  ## }
  ## ```
  allow_parameters :update, settings: {
    shared_views_order: Parameters.array(Parameters.bigid) | Parameters.nil_string,
    lotus: {
      show_reporting_video_tutorial:  Parameters.boolean,
      show_onboarding_tooltips:       Parameters.boolean,
      show_welcome_dialog:            Parameters.boolean,
      show_user_assume_tutorial:      Parameters.boolean,
      show_hc_onboarding_tooltip:     Parameters.boolean,
      show_options_move_tooltip:      Parameters.boolean,
      show_filter_options_tooltip:    Parameters.boolean,
      show_user_nav_tooltip:          Parameters.boolean,
      show_apps_tray:                 Parameters.boolean,
      apps_last_collapsed_states:     Parameters.string,
      show_feature_notifications:     Parameters.boolean,
      device_notification:            Parameters.boolean,
      keyboard_shortcuts_enabled:     Parameters.boolean,
      customer_list_onboarding_state: Parameters.string,
      voice_calling_number:           Parameters.string,
      ticket_action_on_save:          Parameters.string,
      show_insights_onboarding:       Parameters.boolean,
      show_color_branding_tooltip:    Parameters.boolean,
      show_new_search_tooltip:        Parameters.boolean,
      show_onboarding_modal:          Parameters.boolean,
      show_get_started_animation:     Parameters.boolean,
      show_prediction_satisfaction_dashboard: Parameters.boolean,
      show_hc_product_tray_tooltip:   Parameters.boolean,
      show_google_apps_onboarding:    Parameters.boolean,
      show_public_comment_warning:    Parameters.boolean,
      revere_subscription:            Parameters.boolean,
      show_enable_google_apps_modal:  Parameters.boolean,
      show_zero_state_tour_ticket:    Parameters.boolean,
      show_new_comment_filter_tooltip: Parameters.boolean,
      show_multiple_ticket_forms_tutorial: Parameters.boolean,
      show_first_comment_private_tooltip:  Parameters.boolean,
      show_answer_bot_dashboard_onboarding: Parameters.boolean,
      show_answer_bot_trigger_onboarding:   Parameters.boolean,
      show_answer_bot_lotus_onboarding: Parameters.boolean,
      last_lotus_login:                Parameters.string,
      quick_assist_first_dismissal:   Parameters.boolean,
      show_help_panel_intro_tooltip:  Parameters.boolean,
      show_agent_workspace_onboarding:        Parameters.boolean,
      has_seen_channel_switching_onboarding:  Parameters.boolean,
      show_manual_start_translation_tooltip: Parameters.boolean,
      show_manual_stop_translation_tooltip: Parameters.boolean,
      show_composer_will_translate_tooltip: Parameters.boolean,
      show_composer_wont_translate_tooltip: Parameters.boolean
    }
  }
  def update
    if settings = params[:settings].presence
      lotus_settings = settings.delete(:lotus) || {}
      base_settings = settings || {}

      update_revere_subscription(lotus_settings["revere_subscription"]) if lotus_settings.key?("revere_subscription")

      current_user.settings.set(lotus_settings)
      # works around a bug in kasket where we were clobbering cache with an old version
      current_user.settings.each { |s| s.user = current_user }

      if current_account.has_user_level_shared_view_ordering?
        current_user.texts.set(base_settings)
        current_user.texts.each { |s| s.user = current_user }
      end

      current_user.save!
      show
    else
      head :bad_request
    end
  end

  protected

  # Catch Revere Subscription changes, and propogate them to Revere through its API.
  def update_revere_subscription(subscription)
    if subscription != current_user.settings.revere_subscription
      revere_api_client.update_subscription(current_user, subscription)
    end
  end

  def presenter
    Api::V2::Users::SettingsPresenter.new(current_user, url_builder: self)
  end
end
