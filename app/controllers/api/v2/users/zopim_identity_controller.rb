class Api::V2::Users::ZopimIdentityController < Api::V2::BaseController
  before_action :require_agent!
  before_action :require_admin!, only: [:create, :destroy]

  ## ### Show User's Zopim Identity
  ##
  ## `GET /api/v2/users/:user_id/zopim_identity.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Zendesk - Agents, Admins & Chat-Only Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/zopim_identity.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Responses
  ## if user is not on the account
  ## ```http
  ## Status: 404
  ## {
  ##   "error":"RecordNotFound",
  ##   "description":"Not found"
  ## }
  ## ```
  ## if user does not have a zopim identity
  ## ```http
  ## {
  ##   "zopim_agent": {}
  ## }
  ## ```
  ## if user has a zopim_identity
  ## ```http
  ## Status: 200
  ## {
  ##   "zopim_agent":
  ##     {
  ##       "zopim_agent_id":2131977,
  ##       "email":"chessagent7+10022-LFRIEDMANLOCAL@zendesk.com",
  ##       "is_owner":true,
  ##       "is_enabled":true,
  ##       "is_administrator":true,
  ##       "display_name":"Imma Trialler",
  ##       "syncstate":"ready",
  ##       "is_linked":true,
  ##       "is_serviceable":true
  ##     }
  ## }
  ## ```
  ##
  allow_parameters :show, user_id: Parameters.bigid

  def show
    render json: presenter.present(zopim_identity)
  end

  ## ### Provision a Zopim Identity for the user_id in the url
  ## `POST /api/v2/users/:user_id/zopim_identity.json`
  ##
  ## Creates a new Zopim::Agent record and associate it with the selected
  ## Zendesk user (identified by the :user_id field of the request url)
  ##
  ## #### Allowed For:
  ##
  ##  * Owners
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{:user_id}/zopim_identity.json\
  ## -X POST \
  ## The next line is optional creating with a display name and it should not be blank.
  ## -d "{\"zopim_integration\":{\"display_name\":\"Trinity\"}}"\
  ## -v -u {email_address}:{password}
  ## ````
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 CREATED
  ## ```
  ##
  allow_parameters :create, user_id: Parameters.bigid, zopim_identity: { display_name: Parameters.string }

  def create
    if user.zopim_identity.present?
      if zopim_identity.is_enabled?
        render json: presenter.present(zopim_identity.reload), status: :not_modified
      else
        zopim_identity.will_be_saved_by(current_user)
        if zopim_identity.enable!
          render json: presenter.present(zopim_identity.reload), status: :ok
        else
          render json: presenter.present_errors(user.zopim_identity), status: :unprocessable_entity
        end
      end
    else
      user.build_zopim_identity params[:zopim_identity]
      if user.zopim_identity.save
        render json: presenter.present(zopim_identity.reload), status: :created
      else
        render json: presenter.present_errors(user.zopim_identity), status: :unprocessable_entity
      end
    end
  end

  ## ### Update a users Zopim Identity
  ## `PUT /api/v2/users/{:user_id}/zopim_identity.json`
  ##
  ## Update the zopim identity display name
  ##
  ## #### Allowed For:
  ##
  ##  * All users with zopim identities
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{:user_id}/zopim_identity.json\
  ## -X PUT -d "{\"zopim_integration\":{\"display_name\":\"Trinity\"}}"\
  ## -v -u {email_address}:{password}
  ## ````
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  ##
  allow_parameters :update, user_id: Parameters.bigid, zopim_identity: { display_name: Parameters.string }

  def update
    zopim_identity.will_be_saved_by(current_user)
    if zopim_identity.update_attributes(params[:zopim_identity])
      render json: presenter.present(user.reload.zopim_identity), status: :ok
    else
      render json: presenter.present_errors(user.zopim_identity), status: :unprocessable_entity
    end
  end

  ## ### Delete Zopim Identity for the user_id in the url
  ## `DELETE /api/v2/users/:user_id/zopim_identity.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##  * ON all zopim agents
  ##
  ## #### Additional Restrictions for Phase 2:
  ##
  ##  * NOT ALLOWED FOR the zopim owner (user.zopim_identity.is_owner == true)
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{:user_id}/zopim_identity.json\
  ## -X DELETE \
  ## -v -u {email_address}:{password}
  ## ````
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  allow_parameters :destroy, user_id: Parameters.bigid

  def destroy
    zopim_identity.will_be_saved_by(current_user)
    if zopim_identity.disable!
      render json: presenter.present(user.reload.zopim_identity), status: :ok
    else
      render json: presenter.present_errors(user.zopim_identity), status: :unprocessable_entity
    end
  end

  private

  def user
    @user ||= current_account.users.find(params[:user_id])
  end

  def zopim_identity
    user.zopim_identity || raise(ActiveRecord::RecordNotFound)
  end

  def presenter
    Api::V2::Users::ZopimIdentityPresenter.new(current_user, url_builder: self, includes: includes)
  end
end
