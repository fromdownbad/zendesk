module Api
  module V2
    class EndUserIdentitiesController < IdentitiesController
      END_USER_CREATE_IDENTITY_ACCESSIBLE_ATTRS = [:type, :value].freeze

      allow_parameters :all, end_user_id: Parameters.bigid

      skip_before_action :require_agent!, only: [:create, :make_primary]
      before_action :require_current_user!

      protected

      def user
        if current_user.is_end_user?
          params[:identity].slice!(*END_USER_CREATE_IDENTITY_ACCESSIBLE_ATTRS) if params[:identity].present?

          @user ||= current_account.end_users.verified.find(params[:end_user_id])
        else
          # super uses the user_id param, not end_user_id
          params[:user_id] = params[:end_user_id]
          super
        end
      end

      def should_skip_verification_email?
        false
      end

      def require_current_user!
        if current_user.is_end_user? && current_user != user
          deny_access
        end
      end
    end
  end
end
