module Api
  module V2
    class JetpackTasksController < Api::V2::BaseController
      ## ### Listing Jetpack Tasks
      ## `GET  /api/v2/jetpack_tasks.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ##     curl https://{subdomain}.zendesk.com/api/v2/jetpack_tasks.json \
      ##       -v -u {email_address}:{password}
      ##
      ## #### Example Response
      ##
      ##     Status: 200 OK
      ##
      ##     {
      ##       "jetpack_tasks": [
      ##         {
      ##             "status": "complete",
      ##             "key": "configure_email",
      ##             "order": 2,
      ##             "section": "admin",
      ##             "enabled": false
      ##         },
      ##         {
      ##             "status": "new",
      ##             "key": "create_view",
      ##             "order": 3,
      ##             "section": "agent",
      ##             "enabled": true
      ##         }
      ##       ]
      ##     }
      ##
      allow_parameters :index, {}
      def index
        render json: presenter.present(tasks)
      end

      ## ###  Update Jetpack Tasks
      ## `PUT  /api/v2/jetpack_tasks/add_agents.json`
      ##
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/jetpack_tasks/add_agents.json \
      ##   -X PUT -u {email_address}:{password} -H "Content-Type: application/json" \
      ##   -d '{ \"status\": \"started\" }'
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## Location: https://zendesk.com/api/v2/jetpack_tasks/add_agents.json
      ## ```
      ##
      allow_parameters :update, id: Parameters.string, status: Parameters.string
      def update
        task = task_list.update(params[:id], params[:status])
        render json: presenter.present(task)
      end

      protected

      def presenter
        JetpackTaskPresenter.new(current_user, url_builder: self)
      end

      def tasks
        @all ||= task_list.all
      end

      def task_list
        @jetpack_task_list ||= JetpackTaskList.new(current_user)
      end
    end
  end
end
