class Api::V2::BaseCustomFieldOptionsController < Api::V2::BaseController
  CUSTOM_FIELD_OPTION_PARAMETER = {
    id: Parameters.bigid | Parameters.nil,
    name: Parameters.string,
    value: Parameters.string,
    position: Parameters.integer
  }.freeze

  before_action :require_admin!, except: [:index, :show]

  # The specific docs for the `index` method are in the subclasses.
  allow_parameters :index, {}
  allow_cursor_pagination_v2_parameters :index
  def index
    custom_field_options_to_present = if presenter_options[:with_cursor_pagination]
      paginate_with_cursor(sorted_scope_for_cursor(custom_field_options))
    else
      custom_field_options
    end

    render json: presenter.present(custom_field_options_to_present)
  end

  # The specific docs for the `show` method are in the subclasses.
  resource_action :show

  # The specific docs for the `create_or_update` method are in the subclasses.
  allow_parameters :create_or_update, custom_field_option: CUSTOM_FIELD_OPTION_PARAMETER
  def create_or_update
    params.require(:custom_field_option).permit(:id, :name, :value, :position).tap do |cfo_params|
      cfo_params.require(:name)
    end
    option, status = create_or_update_option(params)
    render json: presenter.present(option), status: status, location: presenter.url(option)
  end

  # The specific docs for the `destroy` method are in the subclasses.
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    custom_field_option.mark_as_deleted
    custom_field_option.save!
    expire_field_cache(custom_field_option)

    default_delete_response
  end

  protected

  def create_or_update_option(params)
    option_id = params[:custom_field_option].delete(:id)
    is_creating = false

    option = if option_id
      custom_field_options.find(option_id)
    else
      begin
        custom_field_option_with_deleted(params[:custom_field_option][:value])
      rescue ActiveRecord::RecordNotFound
        is_creating = true
        params[:custom_field_option][:position] ||= custom_field_options.maximum(:position) + 1
        custom_field_options.build(params[:custom_field_option])
      end
    end

    status = if is_creating
      option.save!
      :created
    else
      option.update_attributes!(params[:custom_field_option])
      option.soft_undelete! if option.deleted?
      :ok
    end
    expire_field_cache(option)

    [option, status]
  end

  def field_item
    raise NotImplementedError, "field_item is not implemented"
  end

  def custom_field_option
    @custom_field_option ||= custom_field_options.find(params[:id])
  end

  def custom_field_options
    @custom_field_options ||= field_item.custom_field_options.active
  end

  def presenter
    @presenter ||= Api::V2::CustomFieldOptionPresenter.new(current_user, presenter_options.merge(url_builder: self))
  end

  def presenter_options
    raise NotImplementedError, "presenter_options is not implemented"
  end

  def expire_field_cache(option)
    return unless option.is_a?(CustomFieldOption)

    # Save to expire ticket field scoped caches.
    option.custom_field&.save
  end

  def custom_field_option_with_deleted(value)
    klass = field_item.custom_field_options.klass
    option = klass.with_deleted { field_item.custom_field_options.where(value: value).first }
    raise ActiveRecord::RecordNotFound unless option
    option
  end
end
