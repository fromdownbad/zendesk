class Api::V2::ResourceCollectionsController < Api::V2::BaseController
  skip_before_action :verify_authenticity_token

  before_action :require_admin!
  before_action :require_payload!, only: [:create, :update]
  before_action :require_collection!, only: [:show, :update, :destroy]

  ## ### List Resource Collections
  ## `GET /api/v2/resource_collections.json`
  ##
  ## Lists all resource collections that have been created.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/resource_collections.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "resource_collections": [
  ##     {
  ##       "id":10002,
  ##       "created_at":"2015-09-09T01:57:24Z",
  ##       "updated_at":"2015-09-09T01:57:24Z"
  ##     }
  ##   ],
  ##   "next_page":        null,
  ##   "previous_page":    null,
  ##   "count":            0
  ## }
  ## ```
  allow_parameters :index, :skip # TODO: make stricter
  def index
    render json: presenter.present(resource_collections)
  end

  ## ### Retrieve a Resource Collection
  ## `GET /api/v2/resource_collections/{id}.json`
  ##
  ## Retrieves details of a specified resource collection.
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/resource_collections/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "resource_collection": {
  ##     "id":            10002,
  ##     "created_at":    "2015-09-09T01:57:24Z",
  ##     "updated_at":    "2015-09-09T01:57:24Z"
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.bigid
  def show
    render json: presenter.present(resource_collection)
  end

  ## ### Create a Resource Collection
  ## `POST /api/v2/resource_collections.json`
  ##
  ## Creates a resource collection with a provided payload. The payload is specified the same
  ## way as the content of a requirements.json file in a Zendesk app. See
  ## [Apps Requirements](https://developer.zendesk.com/apps/docs/agent/apps_requirements)
  ## in the Zendesk Apps framework docs.
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/resource_collections.json \
  ##   -X POST -d '{"payload": {payload}}' \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_status": {
  ##     "id":          "0a3e49b038c40133d7380242ac110031",
  ##     "url":         "https://company.zendesk.com/api/v2/job_statuses/0a3e49b038c40133d7380242ac110031.json",
  ##     "total":       null,
  ##     "progress":    null,
  ##     "status":      "queued",
  ##     "message":     null,
  ##     "results":     null
  ##   }
  ## }
  ## ```
  allow_parameters :create, payload: Parameters.map
  def create
    render json: create_job(CollectionResourcesCreateJob)
  end

  ## ### Update a Resource Collection
  ## `PUT /api/v2/resource_collections.json`
  ##
  ## Updates a resource collection with a provided payload. The payload is specified the same
  ## way as the content of a requirements.json file in a Zendesk app. See
  ## [Apps Requirements](https://developer.zendesk.com/apps/docs/agent/apps_requirements)
  ## in the Zendesk Apps framework docs.
  ##
  ## #### Allowed For:
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/resource_collections/{id}.json \
  ##   -X PUT -d '{"payload": {payload}}' \
  ##   -v -u {email_address}:{password} -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_status": {
  ##     "id":          "4555831038d20133d7390242ac110031",
  ##     "url":         "https://company.zendesk.com/api/v2/job_statuses/4555831038d20133d7390242ac110031.json",
  ##     "total":       null,
  ##     "progress":    null,
  ##     "status":      "queued",
  ##     "message":     null,
  ##     "results":     null
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.bigid, payload: Parameters.map
  def update
    render json: create_job(CollectionResourcesUpdateJob, collection_id: params[:id])
  end

  ## ### Delete a Resource Collection
  ## `DELETE /api/v2/resource_collections/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/resource_collections/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "job_status": {
  ##     "id":          "2ee570d0398e0133e26e0242ac110017",
  ##     "url":         "https://company.zendesk.com/api/v2/job_statuses/2ee570d0398e0133e26e0242ac110017.json",
  ##     "total":       null,
  ##     "progress":    null,
  ##     "status":      "queued",
  ##     "message":     null,
  ##     "results":     null
  ##   }
  ## }
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    render json: create_job(CollectionResourcesDeleteJob, collection_id: params[:id])
  end

  private

  def require_payload!
    unless params[:payload]
      render_failure(
        status: :bad_request,
        title: 'Payload missing',
        message: 'Require parameter payload as json'
      )
    end
  end

  def require_collection!
    unless ResourceCollection.for_account(current_account).exists?(id: params[:id])
      render_failure(
        status: :not_found,
        title: 'Collection not found',
        message: "Collection #" + params[:id].to_s + " does not exist" # doc parser picks up double #s
      )
    end
  end

  def create_job(job_class, options = {})
    job_status_presenter.present(
      enqueue_job_with_status(
        job_class,
        options.merge(resources: params[:payload])
      )
    )
  end

  def job_status_presenter
    @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def presenter
    @presenter ||= Api::V2::ResourceCollectionPresenter.new(current_user, url_builder: self)
  end

  def resource_collection
    @resource_collection ||= resource_collections.find(params[:id])
  end

  def resource_collections
    @resource_collections ||= ResourceCollection.for_account(current_account)
  end
end
