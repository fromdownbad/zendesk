module Api
  module V2
    class CustomFieldsController < Api::V2::BaseController
      CUSTOM_FIELD_PARAMETER = {
        type: Parameters.enum(*Zendesk::CustomField::CustomFieldManager.type_map.keys),
        key: Parameters.string,
        title: Parameters.string,
        raw_title: Parameters.string,
        description: Parameters.string,
        raw_description: Parameters.string,
        position: Parameters.integer,
        active: Parameters.boolean,
        regexp_for_validation: Parameters.string,
        tag: Parameters.string,
        is_system: Parameters.boolean,
        custom_field_options: [
          {
            id: Parameters.bigid | Parameters.string,
            name: Parameters.string,
            raw_name: Parameters.string,
            value: Parameters.string
          }
        ]
      }.freeze

      CBP_SORTABLE_FIELDS = {
        'id' => :id,
        'is_active' => :is_active,
        'position' => :position
      }.freeze

      require_capability :user_and_organization_fields, unless: :bime_request?
      before_action :ensure_manage_permissions, except: [:index, :show]
      allow_gooddata_user only: [:index, :show]
      allow_bime_user only: [:index, :show]
      allow_outbound_user only: [:index, :show]
      before_action :ensure_destroy_permissions, only: :destroy

      allow_parameters :index, {}
      allow_cursor_pagination_v2_parameters :index
      def index
        if stale_collection?(custom_fields)
          if current_account.arturo_enabled?('remove_offset_pagination_custom_fields_index') || (current_account.arturo_enabled?('cursor_pagination_custom_fields_index') && !with_offset_pagination?) || with_cursor_pagination_v2?
            render json: cursor_presenter.present(paginate_with_cursor(sorted_scope_for_cursor(custom_fields, CBP_SORTABLE_FIELDS)))
          else
            render json: presenter.present(custom_fields)
          end
        end
      end

      resource_action :show

      allow_parameters :create, {} # subclasses add additional parameters
      def create
        params.require(model_name)
        position = params[model_name].try(:delete, :position)
        new_custom_field = custom_field_manager.build(params[model_name])
        new_custom_field.save!
        set_new_position(new_custom_field, position)
        render json: presenter.present(new_custom_field), status: :created, location: presenter.url(new_custom_field)
      end

      allow_parameters :update, {} # subclasses add additional parameters
      def update
        ActiveRecord::Base.transaction do
          custom_field_manager.update(custom_field, params[model_name]).save!
        end
        render json: presenter.present(custom_field)
      end

      resource_action :destroy

      allow_parameters :reorder, {} # subclasses add additional parameters
      require_oauth_scopes :reorder, scopes: [:write], arturo: true
      def reorder
        reorder_fields
        head :ok
      end

      allow_parameters :check_permissions, {}
      def check_permissions
        if can_manage_fields?
          head :ok
        else
          head :forbidden
        end
      end

      allow_parameters :count, {}
      def count
        render json: { count: record_counter.present, links: { url: request.original_url } }
      end

      private

      def reorder_fields
        custom_field_manager.reorder(params["#{model_name}_ids"], owner)
      end

      def owner
        model_name.to_s.sub("_field", "")
      end

      def set_new_position(field, position)
        current_positions = current_account.custom_fields_for_owner(owner).order(:position).pluck(:id)
        current_positions.delete(field.id)
        params["#{model_name}_ids"] = position.present? ? current_positions.insert(position.to_i, field.id) : (current_positions << field.id)
        reorder_fields
      end

      def model_name
        # :user_field || :organization_field
        raise "Implement in subclass"
      end

      def record_counter
        raise "Implement in subclass"
      end

      def presenter
        @presenter ||= Api::V2::CustomFieldPresenter.new(current_user, presenter_options)
      end

      def cursor_presenter
        @cursor_presenter ||= Api::V2::CustomFieldPresenter.new(current_user, presenter_options.merge(with_cursor_pagination: true))
      end

      def custom_fields
        @custom_fields ||= current_account.custom_fields.for_owner(owner).api_ordering
      end

      def custom_field
        @custom_field ||= current_account.custom_fields.for_owner(owner).find(params[:id]).tap do |custom_field|
          custom_field.current_user = current_user
        end
      end

      alias_method :resource, :custom_field

      def custom_field_manager
        Zendesk::CustomField::CustomFieldManager.new(current_account, current_user, owner.capitalize)
      end

      # Options passed to the CustomFieldPresenter
      def presenter_options
        { model: owner, url_builder: self }
      end

      def ensure_destroy_permissions
        deny_access if custom_field.is_system? && !current_user.is_system_user?
      end

      def ensure_manage_permissions
        deny_access unless can_manage_fields?
      end

      def can_manage_fields?
        if external_permissions_enabled?
          external_permission_to_manage_fields?
        else
          current_user.is_admin? || current_user.is_system_user?
        end
      end

      def external_permissions_enabled?
        raise 'define in subclass'
      end

      def external_permission_to_manage_fields?
        raise 'define in subclass'
      end
    end
  end
end
