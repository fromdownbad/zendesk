class Api::V2::SatisfactionPredictionSurveysController < Api::V2::BaseController
  ## ### Satisfaction Prediction Survey
  ## `POST /api/v2/tickets/{id}/satisfaction_prediction_surveys.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl -X POST -d "reason='whatevs'%20prediction_value=0.0" https://{subdomain}.zendesk.com/api/v2/tickets/{id}/satisfaction_prediction_surveys.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```

  allow_parameters :create, ticket_id: Parameters.bigid, reason: Parameters.string, prediction_value: Parameters.float
  def create
    ticket = current_user.account.tickets.find_by_nice_id!(params[:ticket_id])
    parameters = { reason: params[:reason],
                   prediction_value: params[:prediction_value]}

    survey = ticket.satisfaction_prediction_surveys.new(parameters)
    survey.user = current_user
    if survey.save
      render json:  { message: "Survey Recorded." }, status: 200
    else
      render json:  { message: "Survey could not be recorded." }, status: 422
    end
  end
end
