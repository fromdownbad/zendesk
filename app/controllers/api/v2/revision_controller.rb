class Api::V2::RevisionController < Api::V2::BaseController
  skip_before_action :authenticate_user
  skip_before_action :require_agent!

  before_action :ensure_revision

  ## ### Show current SHA
  ## `GET /api/v2/revision.json`
  ##
  ## Returns the SHA of the currently running version.
  ##
  ## #### Allowed For
  ##
  ##  * Anyone
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/revision.json
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "sha": "2697e247eac9be3f361e0c68316d6f51620c12ef"
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: { sha: sha }
  end

  private

  def ensure_revision
    head(:not_found) unless sha.present?
  end

  def sha
    @@sha ||= (sha_from_file || sha_from_repos)
  end

  def sha_from_file
    file = File.join(Rails.root, "REVISION")

    if File.exist?(file)
      File.read(file).split(/\s/).first.chomp
    end
  end

  def sha_from_repos
    Dir.chdir(Rails.root) { `git rev-parse HEAD` }.chomp
  end
end
