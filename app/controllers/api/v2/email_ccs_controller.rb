## ### List Email CCs for a Ticket
##
## `GET /api/v2/tickets/{id}/email_ccs`
##
## Returns any users cc'd on the ticket.
##
## #### Availability
##
## The [CCs and Followers](https://support.zendesk.com/hc/en-us/articles/203690846) feature must be enabled in Zendesk Support.
##
## If the feature is not enabled, the default CC functionality is used. In that
## case, use [List Collaborators](https://developer.zendesk.com/rest_api/docs/support/tickets#list-collaborators-for-a-ticket)
## to list the users cc'ed on the ticket.
##
## #### Allowed For
##
##  * Agents
##
## #### Using curl
##
## ```bash
## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/email_ccs \
##   -v -u {email_address}:{password}
## ```
##
## #### Example Response
##
## ```http
## Status: 200
##
## {
##   "users": [
##     {
##       "id": "223443",
##       "name": "Johnny Agent",
##       ...
##     },
##     {
##       "id": "8678530",
##       "name": "Peter Admin",
##       ...
##     },
##     {
##       "id": "6748530",
##       "name": "Jane End User",
##       ...
##     }
##   ]
## }
## ```
class Api::V2::EmailCcsController < Api::V2::CollaboratorsController
  allow_pigeon_user only: %i[index]
  require_that_user_can! :view, :ticket

  allow_parameters :index, ticket_id: Parameters.bigid
  def index
    render json: presenter.present(ticket.email_ccs)
  end
end
