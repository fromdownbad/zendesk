class Api::V2::DynamicContent::VariantsController < Api::V2::BaseController
  require_that_user_can! :manage, Cms::Text
  before_action :verify_param_variants_present, only: [:update_many, :create_many]

  allow_subsystem_user :embeddable, only: [:index]

  variant_parameters = {
    locale_id: Parameters.bigid,
    active: Parameters.boolean,
    default: Parameters.boolean,
    content: Parameters.string
  }

  allow_parameters :all, item_id: Parameters.bigid
  ## ### JSON Format for Variants
  ## Variants are represented as JSON objects with the following properties:
  ##
  ## | Name                  | Type       | Read-only | Mandatory | Comment
  ## | --------------------- | ---------- | --------- | --------- | -------
  ## | id                    | integer    | yes       | no        | Automatically assigned when the variant is created
  ## | url                   | string     | yes       | no        | The API url of the variant
  ## | content               | string     | no        | yes       | The content of the variant
  ## | locale_id             | integer    | no        | yes       | An active locale
  ## | outdated              | boolean    | yes       | no        | If the variant is outdated
  ## | active                | boolean    | no        | no        | If the variant is active and useable
  ## | default               | boolean    | no        | no        | If the variant is the default for the item it belongs to
  ## | created_at            | date       | yes       | no        | When the variant was created
  ## | updated_at            | date       | yes       | no        | When the variant was last updated
  ##
  ## A few items are worth noting:
  ##
  ## * `locale_id` - Must be an active locale in the account. To get a list, see [/api/v2/locales](locales#list-locales)
  ##
  ## * `default` - Used as the fallback if Zendesk Support can't find an appropriate variant to match the locale of the user the content is being displayed for
  ##
  ## * `outdated`- Indicates the default variant for this item has been updated, but the other variants were not changed. The content may be out of date
  ##
  ## * `active` - If false, Zendesk Support will not use the variant even if the user's locale matches the variant
  ##
  ## Almost all of the properties are writable.
  ##
  ## ### List Variants
  ## `GET /api/v2/dynamic_content/items/{id}/variants.json`
  ##
  ## Returns all the variants of the specified dynamic content item.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents who have permission to manage dynamic content
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "variants": [
  ##     {
  ##       "id": 23,
  ##       "content": "This is my dynamic content in English",
  ##       "locale_id": 125,
  ##       "outdated": false,
  ##       "active": true,
  ##       "default": true,
  ##       "created_at": "2014-04-09T19:53:23Z",
  ##       "updated_at": "2014-04-09T19:53:23Z",
  ##       "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/23.json"
  ##     },
  ##     {
  ##       "id": 24,
  ##       "content": "Este es mi contenido dinámico en español",
  ##       "locale_id": 126,
  ##       "outdated": true,
  ##       "active": false,
  ##       "default": false,
  ##       "created_at": "2014-04-09T19:53:23Z",
  ##       "updated_at": "2014-04-09T19:53:23Z",
  ##       "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/24.json"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ##
  allow_parameters :index, {}
  require_oauth_scopes :index, scopes: %i[dynamic_content:read read]
  def index
    render json: presenter.present(variants)
  end

  ## ### Show Variant
  ## `GET /api/v2/dynamic_content/items/{id}/variants/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/{id}.json
  ##
  ## {
  ##   "variant": {
  ##     "id": 23,
  ##     "content": "This is my dynamic content in English",
  ##     "locale_id": 125,
  ##     "outdated": false,
  ##     "active": true,
  ##     "default": true,
  ##     "created_at": "2014-04-09T19:53:23Z",
  ##     "updated_at": "2014-04-09T19:53:23Z",
  ##     "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/23.json"
  ##   }
  ## }
  ## ```
  ##
  require_oauth_scopes :show, scopes: %i[dynamic_content:read read]
  resource_action :show

  ## ### Create Variant
  ## `POST /api/v2/dynamic_content/items/{id}/variants.json`
  ##
  ## You can only create one variant for each locale id. If a locale variant already exists, the request is rejected.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants.json \
  ##   -d '{"variant": {"locale_id": 127, "active": true, "default": false, "content": "C\u0027est mon contenu dynamique en français"}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants.json
  ##
  ## {
  ##   "variant": {
  ##     "id": 23,
  ##     "content": "C'est mon contenu dynamique en français",
  ##     "locale_id": 127,
  ##     "outdated": false,
  ##     "active": true,
  ##     "default": false,
  ##     "created_at": "2014-04-09T19:53:23Z",
  ##     "updated_at": "2014-04-09T19:53:23Z",
  ##     "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/23.json"
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :create, variant: variant_parameters
  require_oauth_scopes :create, scopes: %i[dynamic_content:write write]
  def create
    new_variant.save!
    DC::Synchronizer.synchronize_snippet(initializer.item)
    render json: presenter.present(new_variant), status: :created, location: presenter.url(new_variant)
  end

  ## ### Create Many Variants
  ## `POST /api/v2/dynamic_content/items/{id}/variants/create_many.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/create_many.json \
  ##   -d '{"variants": [{"locale_id": 127, "active": true, "default": false, "content": "C\u0027est mon contenu dynamique en français"},{"locale_id": 126, "active": true, "default": false, "content": "Este es mi contenido dinámico en español"}]}'\
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/create_many.json
  ##
  ## {
  ##   "variants": [
  ##     {
  ##       "id": 23,
  ##       "content": "C'est mon contenu dynamique en français",
  ##       "locale_id": 127,
  ##       "outdated": false,
  ##       "active": true,
  ##       "default": false,
  ##       "created_at": "2014-04-09T19:53:23Z",
  ##       "updated_at": "2014-04-09T19:53:23Z",
  ##       "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/23.json"
  ##     },
  ##     {
  ##       "id": 24,
  ##       "content": "Este es mi contenido dinámico en español",
  ##       "locale_id": 126,
  ##       "outdated": false,
  ##       "active": true,
  ##       "default": false,
  ##       "created_at": "2014-04-09T19:53:23Z",
  ##       "updated_at": "2014-04-09T19:53:23Z",
  ##       "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/24.json"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ##
  allow_parameters :create_many, variants: [variant_parameters]
  require_oauth_scopes :create_many, scopes: %i[dynamic_content:write write read]
  def create_many
    render json: job_status_presenter.present(enqueue_job_with_status(VariantsBulkCreateJob, params))
  end

  ## ### Update Variant
  ## `PUT /api/v2/dynamic_content/items/{id}/variants/{id}.json`
  ##
  ## Updates the specified variant. You don't need to include all the properties. If you just want to update content, for example, then include just that.
  ##
  ## You can't switch the active state of the default variant of an item. Similarly, you can't switch the default to false if the
  ## variant is the default. You must make another variant default instead.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/{id}.json \
  ##   -d '{"variant": {"active": false, "default": false, "content": "C\u0027est mon contenu dynamique en français"}}'\
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/{id}.json
  ##
  ## {
  ##   "variant": {
  ##     "id": 23,
  ##     "content": "C'est mon contenu dynamique en français",
  ##     "locale_id": 125,
  ##     "outdated": false,
  ##     "active": false,
  ##     "default": false,
  ##     "created_at": "2014-04-09T19:53:23Z",
  ##     "updated_at": "2014-04-09T19:53:23Z",
  ##     "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/23.json"
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update, id: Parameters.bigid, variant: variant_parameters.slice(:active, :default, :content)
  require_oauth_scopes :update, scopes: %i[dynamic_content:write write]
  def update
    variant.update_attributes!(initializer.normalized_parameters)
    DC::Synchronizer.synchronize_snippet(initializer.item)
    render json: presenter.present(variant)
  end

  ## ### Update Many Variants
  ## `PUT /api/v2/dynamic_content/items/{id}/variants/update_many.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/update_many.json \
  ##   -d '{"variants": [{"id": 23, "active": false, "default": false, "content": "C\u0027est mon contenu dynamique en français"},{"id": 24, "active": false, "default": false, "content": "Este es mi contenido dinámico en español"}]}'\
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/{id}.json
  ##
  ## {
  ##   "variants": [
  ##     {
  ##       "id": 23,
  ##       "content": "C'est mon contenu dynamique en français",
  ##       "locale_id": 127,
  ##       "outdated": false,
  ##       "active": true,
  ##       "default": false,
  ##       "created_at": "2014-04-09T19:53:23Z",
  ##       "updated_at": "2014-04-09T19:53:23Z",
  ##       "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/23.json"
  ##     },
  ##     {
  ##       "id": 24,
  ##       "content": "Este es mi contenido dinámico en español",
  ##       "locale_id": 126,
  ##       "outdated": false,
  ##       "active": true,
  ##       "default": false,
  ##       "created_at": "2014-04-09T19:53:23Z",
  ##       "updated_at": "2014-04-09T19:53:23Z",
  ##       "url": "https://subdomain.zendesk.com/api/v2/dynamic_content/items/3/variants/24.json"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :update_many, id: Parameters.bigid, variants: [variant_parameters.slice(:active, :default, :content).merge(id: Parameters.bigid)]
  require_oauth_scopes :update_many, scopes: %i[dynamic_content:write write read]
  def update_many
    render json: job_status_presenter.present(enqueue_job_with_status(VariantsBulkUpdateJob, params))
  end

  ## ### Delete Variant
  ## `DELETE /api/v2/dynamic_content/items/{id}/variants/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}/variants/{id}.json \
  ##   -X DELETE -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  require_oauth_scopes :destroy, scopes: %i[dynamic_content:write write]
  resource_action :destroy

  protected

  def verify_param_variants_present
    if params[:variants].blank?
      raise Zendesk::UnknownAttributeError, "missing variants parameter"
    end
  end

  def presenter
    @presenter ||= Api::V2::DynamicContent::VariantPresenter.new(current_user, url_builder: self)
  end

  def job_status_presenter
    @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def initializer
    @initializer ||= Zendesk::DynamicContent::Initializer.new(current_account, params)
  end

  def variants
    @variants ||= paginate(paginate_scope)
  end

  def paginate_scope
    if SORT_LOOKUP["outdated"] == sort_by
      initializer.variants.joins(text: :fallback)
    else
      initializer.variants
    end
  end

  def variant
    @variant ||= initializer.variant
  end

  def new_variant
    @new_variant ||= initializer.new_variant
  end

  SORT_LOOKUP = {
    "created_at"  => "created_at",
    "updated_at"  => "updated_at",
    "locale"      => "translation_locale_id",
    "active"      => "active",
    "outdated"    => "cms_variants.updated_at < fallbacks_cms_texts.updated_at"
  }.freeze
end
