class Api::V2::DynamicContent::ItemsController < Api::V2::BaseController
  VARIANT_PARAMETER = {
    locale_id: Parameters.bigid,
    active: Parameters.boolean,
    default: Parameters.boolean,
    content: Parameters.string,
  }.freeze

  allow_subsystem_user :zopim, only: [:index]
  allow_subsystem_user :embeddable, only: [:index, :show_many]

  require_that_user_can! :manage, Cms::Text
  ## ### JSON Format for Items
  ## Dynamic content items are represented as JSON objects which have the following keys:
  ##
  ## | Name                  | Type     | Read-only | Mandatory | Comment
  ## | --------------------- | -------- | --------- | --------- | -------
  ## | id                    | integer  | yes       | no        | Automatically assigned when creating items
  ## | url                   | string   | yes       | no        | The API url of this item
  ## | name                  | string   | no        | yes       | The unique name of the item
  ## | placeholder           | string   | yes       | no        | Automatically generated placeholder for the item, derived from name
  ## | default_locale_id     | integer  | no        | yes       | The default locale for the item. *Must* be one of the [locales the account has active](locales#list-locales).
  ## | outdated              | boolean  | yes       | no        | Indicates the item has outdated variants within it
  ## | created_at            | date     | yes       | no        | When this record was created
  ## | updated_at            | date     | yes       | no        | When this record last got updated
  ## | variants              | array    | no        | yes       | All [variants](dynamic_content#variants) within this item
  ##
  ## ### List Items
  ## `GET /api/v2/dynamic_content/items.json`
  ##
  ##  Returns a list of all dynamic content items for your account if accessed as an admin
  ##  or agents who have permission to manage dynamic content.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "items": [
  ##     {
  ##       "url":               "https://company.zendesk.com/api/v2/dynamic_content/items/47.json",
  ##       "id":                47,
  ##       "name":              "Snowboard Problem",
  ##       "placeholder":       "{{dc.snowboard_problem}}",
  ##       "default_locale_id": 1,
  ##       "outdated":          true,
  ##       "created_at":        "2015-05-13T22:33:12Z",
  ##       "updated_at":        "2015-05-13T22:33:12Z",
  ##       "variants": [
  ##         {
  ##           "url": "https://company.zendesk.com/api/v2/dynamic_content/items/47/variants/47.json",
  ##           "id": 47,
  ##           "content": "C'est mon contenu dynamique en français",
  ##           "locale_id": 1,
  ##           "outdated": false,
  ##           "active": true,
  ##           "default": true,
  ##           "created_at": "2015-05-13T22:33:12Z",
  ##           "updated_at": "2015-05-13T22:33:12Z"
  ##         },
  ##         ...
  ##       ]
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  require_oauth_scopes :index, scopes: %i[dynamic_content:read read]
  def index
    expires_in(60.seconds) if current_account.has_dynamic_content_cache_control_header?

    render json: presenter.present(items)
  end

  ## ### Show Item
  ## `GET /api/v2/dynamic_content/items/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}.json
  ##
  ## {
  ##   "item": {
  ##     "url":               "https://company.zendesk.com/api/v2/dynamic_content/items/47.json",
  ##     "id":                47,
  ##     "name":              "Snowboard Problem",
  ##     "placeholder":       "{{dc.snowboard_problem}}",
  ##     "default_locale_id": 1,
  ##     "outdated":          true,
  ##     "created_at":        "2015-05-13T22:33:12Z",
  ##     "updated_at":        "2015-05-13T22:33:12Z",
  ##     "variants": [
  ##       {
  ##         "url": "https://company.zendesk.com/api/v2/dynamic_content/items/47/variants/47.json",
  ##         "id": 47,
  ##         "content": "C'est mon contenu dynamique en français",
  ##         "locale_id": 1,
  ##         "outdated": false,
  ##         "active": true,
  ##         "default": true,
  ##         "created_at": "2015-05-13T22:33:12Z",
  ##         "updated_at": "2015-05-13T22:33:12Z"
  ##       },
  ##       ...
  ##     ]
  ##   }
  ## }
  ## ```
  require_oauth_scopes :show, scopes: %i[dynamic_content:read read]
  resource_action :show

  ## ### Create Item
  ##
  ## `POST /api/v2/dynamic_content/items.json`
  ##
  ## Create a new content item, with one or more variants in the item's `variants` array. See [Specifying item variants](#specifying-item-variants).
  ##
  ## Note that the `default_locale_id` and variant `locale_id` values *must* be one of the locales the account has active. You can get the list with the [List Locales](locales#list-locales) endpoint.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '{"item": {"name": "Snowboard Problem", "default_locale_id": 16, "variants": [{"locale_id": 16, "default": true, "content": "Voici mon contenu dynamique en français"}, {"locale_id": 2, "default": false, "content": "Este es mi contenido dinámico en español"}]}}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}.json
  ##
  ## {
  ##   "item": {
  ##     "url":               "https://company.zendesk.com/api/v2/dynamic_content/items/47.json",
  ##     "id":                47,
  ##     "name":              "Snowboard Problem",
  ##     "placeholder":       "{{dc.snowboard_problem}}",
  ##     "default_locale_id": 1,
  ##     "outdated":          false,
  ##     "created_at":        "2015-05-13T22:33:12Z",
  ##     "updated_at":        "2015-05-13T22:33:12Z",
  ##     "variants": [
  ##       {
  ##         "url": "https://company.zendesk.com/api/v2/dynamic_content/items/47/variants/47.json",
  ##         "id": 47,
  ##         "content": "Voici mon contenu dynamique en français",
  ##         "locale_id": 16,
  ##         "outdated": false,
  ##         "active": true,
  ##         "default": true,
  ##         "created_at": "2015-05-13T22:33:12Z",
  ##         "updated_at": "2015-05-13T22:33:12Z"
  ##       },
  ##       {
  ##         "url": "https://company.zendesk.com/api/v2/dynamic_content/items/47/variants/48.json",
  ##         "id": 48,
  ##         "content": "Este es mi contenido dinámico en español",
  ##         "locale_id": 2,
  ##         "outdated": false,
  ##         "active": true,
  ##         "default": false,
  ##         "created_at": "2015-05-13T22:33:12Z",
  ##         "updated_at": "2015-05-13T22:33:12Z"
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  allow_parameters :create, item: {
    name: Parameters.string,
    default_locale_id: Parameters.bigid,
    content: Parameters.string,
    variants: [VARIANT_PARAMETER]
  }
  require_oauth_scopes :create, scopes: %i[dynamic_content:write write]
  def create
    new_item.save!

    DC::Synchronizer.synchronize_snippet(new_item)
    render json: presenter.present(new_item), status: :created, location: presenter.url(new_item)
  end

  ## ### Update Item
  ## `PUT /api/v2/dynamic_content/items/{id}.json`
  ##
  ## The only attribute you can change is the name. You can specify
  ## multiple variants in the item's `variants` array. See [Specifying item variants](#specifying-item-variants).
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}.json \
  ##   -H "Content-Type: application/json" -d '{"item": {"name": "New name"}}'\
  ##   -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}.json
  ##
  ## {
  ##   "item": {
  ##     "url":               "https://company.zendesk.com/api/v2/dynamic_content/items/47.json",
  ##     "id":                47,
  ##     "name":              "New name",
  ##     "placeholder":       "{{dc.snowboard_problem}}",
  ##     "default_locale_id": 1,
  ##     "outdated":          false,
  ##     "created_at":        "2015-05-13T22:33:12Z",
  ##     "updated_at":        "2015-05-13T22:33:12Z",
  ##     "variants": [
  ##       {
  ##         "url": "https://company.zendesk.com/api/v2/dynamic_content/items/47/variants/47.json",
  ##         "id": 47,
  ##         "content": "C'est mon contenu dynamique en français",
  ##         "locale_id": 1,
  ##         "outdated": false,
  ##         "active": true,
  ##         "default": true,
  ##         "created_at": "2015-05-13T22:33:12Z",
  ##         "updated_at": "2015-05-13T22:33:12Z"
  ##       },
  ##       ...
  ##     ]
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.bigid, item: {name: Parameters.string }
  require_oauth_scopes :update, scopes: %i[dynamic_content:write write]
  def update
    item.update_attributes!(params[:item])
    render json: presenter.present(item)
  end

  ## ### Delete Item
  ## `DELETE /api/v2/dynamic_content/items/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/{id}.json \
  ##   -X DELETE -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: ActionController::Parameters.bigid
  require_oauth_scopes :destroy, scopes: %i[dynamic_content:write write]
  def destroy
    if item.destroy
      DC::Synchronizer.delete_snippet(item)
      default_delete_response
    else
      render json: presenter.present_errors(item), status: :unprocessable_entity
    end
  end

  ## ### Show Many Items
  ## `GET /api/v2/dynamic_content/items/show_many.json?identifiers={identifiers}`
  ##
  ## #### Stability
  ##
  ## * Development
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/dynamic_content/items/show_many.json?identifiers=item_one,item_two
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "items": [
  ##     {
  ##       "url":               "https://company.zendesk.com/api/v2/dynamic_content/items/47.json",
  ##       "id":                47,
  ##       "name":              "Snowboard Problem",
  ##       "placeholder":       "{{dc.snowboard_problem}}",
  ##       "default_locale_id": 1,
  ##       "outdated":          true,
  ##       "created_at":        "2015-05-13T22:33:12Z",
  ##       "updated_at":        "2015-05-13T22:33:12Z",
  ##       "variants": [
  ##         {
  ##           "url": "https://company.zendesk.com/api/v2/dynamic_content/items/47/variants/47.json",
  ##           "id": 47,
  ##           "content": "C'est mon contenu dynamique en français",
  ##           "locale_id": 1,
  ##           "outdated": false,
  ##           "active": true,
  ##           "default": true,
  ##           "created_at": "2015-05-13T22:33:12Z",
  ##           "updated_at": "2015-05-13T22:33:12Z"
  ##         },
  ##       ...
  ##       ]
  ##     },
  ##     {
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :show_many, identifiers: Parameters.string
  require_oauth_scopes :show_many, scopes: %i[dynamic_content:read read]
  def show_many
    render json: presenter.present(many_items)
  end

  protected

  def presenter
    @presenter ||= Api::V2::DynamicContent::ItemPresenter.new(current_user, url_builder: self)
  end

  def items
    @items ||= paginate(paginate_scope)
  end

  def many_items
    identifiers = params[:identifiers] || ''
    identifiers = identifiers.split(',')
    paginate_scope.where(identifier: identifiers)
  end

  def paginate_scope
    if SORT_LOOKUP["outdated"] == sort_by
      current_account.cms_texts.joins(:variants).includes(:fallback)
    elsif SORT_LOOKUP["default_locale_id"] == sort_by
      current_account.cms_texts.joins(:variants)
    else
      current_account.cms_texts
    end
  end

  def item
    @item ||= current_account.cms_texts.find(params[:id])
  end

  def new_item
    @new_item ||= current_account.cms_texts.new(normalized_parameters)
  end

  def normalized_parameters
    params[:item] ||= {}

    if params[:item][:variants]

      variants_attributes = []

      params[:item][:variants].each do |variant|
        attributes = {
          translation_locale_id: variant[:locale_id],
          value: variant[:content],
          is_fallback: variant[:default] == true,
          nested: true
        }

        if attributes[:is_fallback]
          params[:item][:fallback_attributes] = attributes
        else
          variants_attributes << attributes
        end
      end

      params[:item].delete(:default_locale_id)
      params[:item].delete(:variants)
      params[:item][:variants_attributes] = variants_attributes
    else
      params[:item][:fallback_attributes] = {
        translation_locale_id: params[:item].delete(:default_locale_id),
        value: params[:item].delete(:content),
        is_fallback: true,
        nested: true
      }
    end

    params[:item]
  end

  SORT_LOOKUP = {
    "created_at"        => "created_at",
    "updated_at"        => "updated_at",
    "name"              => "name",
    "default_locale_id" => "cms_variants.translation_locale_id",
    "outdated"          => "updated_at < fallbacks_cms_texts.updated_at"
  }.freeze
end
