module Api
  module V2
    class UserFieldsController < CustomFieldsController
      allow_subsystem_user :embeddable, only: [:index, :create]
      allow_cors_from_zendesk_external_domains :index

      ## ### List User Fields
      ##
      ## `GET /api/v2/user_fields.json`
      ##
      ##  Returns a list of custom User Fields in your account. Fields are returned in the order that you specify
      ##  in your User Fields configuration in Zendesk Support. Clients should cache this resource for the duration of
      ##  their API usage and map the key for each User Field to the values returned under the
      ##  user_fields attribute on the [User](./users) resource.
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/user_fields.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "user_fields": [
      ##     {
      ##       "url": "https://company.zendesk.com/api/v2/user_fields/7.json",
      ##       "id": 7,
      ##       "type": "text",
      ##       "key": "custom_field_1",
      ##       "title": "Custom Field 1",
      ##       "raw_title": "Custom Field 1",
      ##       "description": "Description of Custom Field",
      ##       "raw_description": "{{dc.my_description}}",
      ##       "position": 9999,
      ##       "active": true,
      ##       "regexp_for_validation": null,
      ##       "created_at": "2012-10-16T16:04:06Z",
      ##       "updated_at": "2012-10-16T16:04:06Z"
      ##     }
      ##   ],
      ##   "next_page": null,
      ##   "previous_page": null,
      ##   "count": 1
      ## }
      ## ```
      ##

      def index
        expires_in(60.seconds) if current_account.has_user_fields_cache_control_header? && app_request?
        super
      end

      protected

      ## ### Show User Field
      ## `GET /api/v2/user_fields/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "user_field": {
      ##     "url": "https://company.zendesk.com/api/v2/user_fields/7.json",
      ##     "id": 7,
      ##     "type": "text",
      ##     "key": "custom_field_1",
      ##     "title": "Custom Field 1",
      ##     "raw_title": "Custom Field 1",
      ##     "description": "Description of Custom Field",
      ##     "raw_description": "{{dc.my_description}}",
      ##     "position": 9999,
      ##     "active": true,
      ##     "regexp_for_validation": null,
      ##     "created_at": "2012-10-16T16:04:06Z",
      ##     "updated_at": "2012-10-16T16:04:06Z"
      ##   }
      ## }
      ## ```

      ## ### Create User Fields
      ## `POST /api/v2/user_fields.json`
      ##
      ##  Types of custom fields that can be created are:
      ##
      ##  * `text` (default when no "type" is specified)
      ##  * `textarea`
      ##  * `checkbox`
      ##  * `date`
      ##  * `integer`
      ##  * `decimal`
      ##  * `regexp`
      ##  * `tagger` (custom dropdown)
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/user_fields.json \
      ##   -H "Content-Type: application/json" -X POST \
      ##   -d '{"user_field": {"type": "text", "title": "Support description",
      ##        "description": "This field describes the support plan this user has",
      ##        "position": 0, "active": true, "key": "support_description"}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/user_fields/{id}.json
      ##
      ## {
      ##   "user_field": {
      ##     "url": "https://company.zendesk.com/api/v2/user_fields/75.json",
      ##     "id": 75,
      ##     "type": "text",
      ##     "key": "support_description",
      ##     "title": "Support description",
      ##     "raw_title": "Support description",
      ##     "description": "This field describes the support plan this user has",
      ##     "raw_description": "This field describes the support plan this user has",
      ##     "position": 0,
      ##     "active": true,
      ##     "regexp_for_validation": null,
      ##     "created_at": "2013-02-27T20:35:55Z",
      ##     "updated_at": "2013-02-27T20:35:55Z"
      ##   }
      ## }
      ## ```
      allow_parameters :create, user_field: CUSTOM_FIELD_PARAMETER

      ## ### Update User Fields
      ## `PUT /api/v2/user_fields/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{id}.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{ "user_field": { "title": "Support description" }}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## Location: https://{subdomain}.zendesk.com/api/v2/user_fields/75.json
      ##
      ## {
      ##   "user_field": {
      ##     "url": "https://company.zendesk.com/api/v2/user_fields/75.json",
      ##     "id": 75,
      ##     "type": "text",
      ##     "key": "support_description",
      ##     "title": "Support description",
      ##     "raw_title": "Support description",
      ##     "description": "This field describes the support plan this user has",
      ##     "raw_description": "This field describes the support plan this user has",
      ##     "position": 0,
      ##     "active": true,
      ##     "regexp_for_validation": null,
      ##     "created_at": "2013-02-27T20:35:55Z",
      ##     "updated_at": "2013-02-27T20:35:55Z"
      ##   }
      ## }
      ## ```
      allow_parameters :update, id: Parameters.bigid, user_field: CUSTOM_FIELD_PARAMETER

      ## ### Update a Dropdown (Tagger) Field
      ##
      ## Dropdown fields return an array of `custom_field_options` which specify the name, value and order of the list of dropdown options.
      ## Understand the following behavior when updating a dropdown field:
      ##
      ## - All options must be passed on update. Options that are not passed will be removed; as a result, these values will be removed from any users.
      ## - To create a new option, pass a null `id` along with `name` and `value`.
      ## - To update an existing option, pass its `id` along with `name` and `value`.
      ## - To re-order an option, reposition it in the `custom_field_options` array relative to the other options.
      ## - To remove an option, omit it from the list of options upon update.
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{id}.json \
      ##   -H "Content-Type: application/json" -X PUT \
      ##   -d '{"user_field": {"custom_field_options": [{"id": 124, "name": "Option 2", "value": "option_2"}, {"id": 123, "name": "Option 1", "value": "option_1"}, {"id": 125, "name": "Option 2", "value": "option_3"}]}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ##
      ## ### Delete User Field
      ## `DELETE /api/v2/user_fields/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## ```
      ##
      ##
      ## ### Reorder User Field
      ## `PUT /api/v2/user_fields/reorder.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/user_fields/reorder.json -H "Content-Type: application/json" \
      ##   -v -u {email_address}:{password} -X PUT -d '{ "user_field_ids": [3, 4] }'
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## ```
      ##
      allow_parameters :reorder, user_field_ids: [Parameters.bigid]

      def model_name
        :user_field
      end

      def record_counter
        @record_counter ||= begin
          Zendesk::RecordCounter::UserFields.new(
            account: current_account,
            options: { owner: owner }
          )
        end
      end

      private

      def external_permission_to_manage_fields?
        current_user.can?(:manage_user_fields, CustomField::Field)
      end

      def external_permissions_enabled?
        current_account.has_permissions_user_fields_manage?
      end
    end
  end
end
