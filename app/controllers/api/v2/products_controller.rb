class Api::V2::ProductsController < Api::V2::BaseController
  before_action :require_agent!
  skip_before_action :enforce_support_is_active!

  # ### List all Products availabe in the account
  # #### `GET /api/v2/products`
  #
  # This API endpoint returns a list of all available products.
  # Products that are unavailable are not included
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/products \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200
  #
  # {
  #   "products": [
  #     {
  #       "key": "lotus"
  #     },
  #     {
  #       "key": "guide"
  #     },
  #     {
  #       "key": "chat"
  #     },
  #     {
  #       "key": "talk"
  #     },
  #     {
  #       "key": "explore",
  #       "label": "beta"
  #     },
  #     {
  #       "key": "connect"
  #     }
  #   ]
  # }
  # ```

  allow_parameters :index, host: Parameters.string, trayversion: Parameters.string

  def index
    active_span&.set_tag('zendesk.products.client.trayversion', params[:trayversion])
    active_span&.set_tag('zendesk.products.client.host', params[:host])
    render json: products_presenter.present(products.available)
  end

  private

  def products
    Api::V2::Products::Suite.new(current_account, current_user)
  end

  def products_presenter
    @products_presenter ||= Api::V2::ProductCollectionPresenter.new(current_account, current_user, url_builder: self, includes: includes)
  end

  def active_span
    @active_span ||= Datadog.tracer.active_span&.parent
  end
end
