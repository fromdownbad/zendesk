class Api::V2::AuditLogsController < Api::V2::BaseController
  include Zendesk::AuditLogControllerSupport

  before_action :require_audit_log_access!
  before_action :require_export_enabled!,  only: :export

  rescue_from InvalidCreatedAtFilterError do |_exception|
    render json: {
      error: "InvalidValue",
      description: "Invalid filter[created_at] specified; format is ?filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ&filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ"
    }, status: :bad_request
  end

  ## ### Listing Audit Logs
  ## `GET /api/v2/audit_logs.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins on accounts that have audit-log access
  ##
  ## #### Using curl
  ##
  ## ```bash
  ##  curl https://helpdesk.zendesk.com/api/v2/audit_logs.json \
  ##    -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Filtering
  ##
  ## ```
  ## By source type:
  ## ?filter[source_type]=user
  ## ?filter[source_type]=rule
  ##
  ## By exact source:
  ## ?filter[source_type]=user&filter[source_id]=123
  ##
  ## By actor:
  ## ?filter[actor_id]=123
  ##
  ## By IP address:
  ## ?filter[ip_address]=127.0.0.1
  ##
  ## By time of creation:
  ## ?filter[created_at][]=2013-01-01T00:00:00Z&filter[created_at][]=2013-01-01T23:59:59Z
  ##
  ## Control sort order (default is `sort_by=created_at`, `sort_order=desc`):
  ## ?sort_by=actor_id&sort_order=asc
  ## ```
  ##
  ## ```bash
  ##  curl -g 'https://helpdesk.zendesk.com/api/v2/audit_logs.json?filter[source_type]=user&filter[source_id]=123' \
  ##    -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "audit_logs": [
  ##     {
  ##       "id":           498483,
  ##       "actor_id":     1234,
  ##       "source_id":    3456,
  ##       "source_type":  "user",
  ##       "source_label": "John Doe",
  ##       "action":       "update",
  ##       "change_description": "Role changed from Administrator to End User"
  ##       "ip_address":   "209.119.38.228",
  ##       "created_at":   "2012-03-05T11:32:44Z",
  ##       "url":          "https://company.zendesk.com/api/v2/audit_logs/498483.json",
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index, filter: Parameters.anything
  require_oauth_scopes :index, scopes: [:"auditlogs:read", :read]
  def index
    render json: presenter.present(events)
  end

  ## ### Getting Audit Logs
  ## `GET /api/v2/audit_logs/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins on accounts that have audit-log access
  ##
  ## #### Using curl
  ##
  ## ```bash
  ##  curl https://helpdesk.zendesk.com/api/v2/audit_logs/{id}.json \
  ##    -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "audit_log": {
  ##     "id":           498483,
  ##     "actor_id":     1234,
  ##     "source_id":    3456,
  ##     "source_type":  "user",
  ##     "source_label": "John Doe",
  ##     "action":       "update",
  ##     "changes_description": "Role changed from Administrator to End User"
  ##     "ip_address":   "209.119.38.228",
  ##     "created_at":  "2012-03-05T11:32:44Z",
  ##     "url":          "https://company.zendesk.com/api/v2/audit_logs/498483.json",
  ##   }
  ## }
  ## ```
  require_oauth_scopes :show, scopes: [:"auditlogs:read", :read]
  resource_action :show

  ## ### Exporting Audit Logs
  ## `POST /api/v2/audit_logs/export.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins on accounts that have audit-log access
  ##
  ## #### Using curl
  ##
  ## ```bash
  ##  curl https://helpdesk.zendesk.com/api/v2/audit_logs/export.json \
  ##    -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Filtering
  ##
  ## ```
  ## By source type:
  ## ?filter[source_type]=user
  ## ?filter[source_type]=rule
  ##
  ## By exact source:
  ## ?filter[source_type]=user&filter[source_id]=123
  ##
  ## By actor:
  ## ?filter[actor_id]=123
  ##
  ## By IP address:
  ## ?filter[ip_address]=127.0.0.1
  ##
  ## By time of creation:
  ## ?filter[created_at][]=2013-01-01T00:00:00Z&filter[created_at][]=2013-01-01T23:59:59Z
  ##
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 CREATED
  ## ```
  allow_parameters :export, filter: Parameters.anything
  require_oauth_scopes :export, scopes: [:"auditlogs:read", :read]
  def export
    Prop.throttle!(:audit_logs_csv_export, current_account.id)
    AuditLogsExportJob.enqueue(current_account.id, current_user.id, filter: params[:filter])
    head :accepted
  end

  private

  def event
    cia_events_scope.find(params[:id])
  end

  alias_method :resource, :event

  def events
    @events ||= paginated_cia_events
  end

  def require_audit_log_access!
    deny_access unless current_user.can_see_audit_log?
  end

  def presenter
    @presenter ||= Api::V2::AuditLogPresenter.new(current_user, url_builder: self)
  end

  def require_export_enabled!
    deny_access unless current_account.has_audit_logs_export_api_enabled?
  end
end
