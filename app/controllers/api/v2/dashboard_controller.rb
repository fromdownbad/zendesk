class Api::V2::DashboardController < Api::V2::BaseController
  ## ### Show the Currently Authenticated Agent's Dashboard
  ## `GET /api/v2/dashboard/agent.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "dashboard": {
  ##     "open_tickets":  8,
  ##     "new_and_open_tickets_groups":  212,
  ##     "tickets_solved_this_week":  16,
  ##     "tickets_rated_good_this_week":  2,
  ##     "tickets_rated_bad_this_week":  0,
  ##     "sat_score_last_60_days": 98,
  ##     "account_sat_score_last_60_days":  99
  ##   }
  ## }
  ## ```
  allow_parameters :agent, {}
  require_oauth_scopes :agent, scopes: [:read], arturo: true
  def agent
    # Ref: #inc-2020-04-23-b. High volume of agents (instacart) caused
    # spike if SQL queries related to CSAT. We applied CRL to this API endpoint
    # and it doesn't break the flow, however broke some stats in UI. We will keep
    # it working but serve stale data for up to 5 minutes per agent's browser session

    if current_account.has_dashboard_agent_stats_caching?
      expires_in 5.minutes
    end
    render json: dashboard_presenter.present(current_user)
  end

  protected

  def dashboard_presenter
    @presenter ||= Api::V2::DashboardPresenter.new(current_user, url_builder: self)
  end
end
