class Api::V2::Embeddable::ConfigSetsController < Api::V2::BaseController
  before_action :require_admin!

  # ### Get the configuration set for the default brand
  #
  # `GET /api/v2/embeddable/config_sets/default.json`
  #
  # #### Allowed For:
  #
  # * Admins and owners
  #
  # #### Example Response
  #
  # ```
  # {
  #   "color": "#FF00FF",
  #   "contact_form_button_text": "message",
  #   ...
  # }
  # ```
  allow_parameters :show_default, {}
  require_oauth_scopes :show_default, scopes: [:read], arturo: true
  def show_default
    proxy_embeddable(:get)
  end

  # ### Update the configuration set for the default brand
  #
  # `PUT /api/v2/embeddable/config_sets/default.json`
  #
  # #### Allowed For:
  #
  # * Admins and owners
  #
  # #### Sample Payload
  #
  # ```
  # {
  #   "config_set": {
  #     "color": "#FF00FF",
  #     "contact_form_button_text": "message"
  #   }
  # }
  # ```
  # The contact_form_button_text must be either "message" or "contact".
  #
  # #### Example Response
  #
  # ```
  # {
  #   "color": "#FF00FF",
  #   "contact_form_button_text": "message",
  #   ...
  # }
  # ```
  allow_parameters :update_default,
    config_set: {
      color: Parameters.string,
      contact_form_button_text: Parameters.string,
      chat_enabled: Parameters.integer,
      help_center_enabled: Parameters.integer
    }
  require_oauth_scopes :update_default, scopes: [:write], arturo: true
  def update_default
    proxy_embeddable(:put, params)
  end

  private

  def proxy_embeddable(method, payload = nil)
    response = api_client.connection.send(
      method,
      '/embeddable/api/internal/config_sets/default.json',
      payload
    )
    render json: response.body, status: response.status
  rescue => e
    ZendeskExceptions::Logger.record(e, location: self, message: "Could not #{method} embeddable config set for the default brand of account #{current_account.id}.", fingerprint: '871bbbed5650b886d3332f3427b23044dff7f297')
    statsd_client.increment("#{method}.failure")
    render json: {error: e.message}, status: :internal_server_error
  end

  def api_client
    Zendesk::InternalApi::Client.new(current_account.subdomain, user: 'zendesk')
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['embeddable.config_sets'])
  end
end
