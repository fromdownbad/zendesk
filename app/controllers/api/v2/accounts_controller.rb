# NOTES:
#
# - This controller inherits from Api::V2::BaseController but implements
# its own authentication.
#
# - Unauthenticated requests are not rate limited (see Api::BaseController#throttle_unauthorized_requests)
#
require 'zendesk/pod_relay'

module Api
  module V2
    class AccountsController < Api::V2::BaseController
      # Skips rate limiting for unauthorized requests.
      # We need to do that because that method does not know about the custom-made authentication
      # done in that endpoint.
      skip_before_action :throttle_unauthorized_requests

      skip_before_action :authenticate_user, :require_agent!,
        :prevent_inactive_account_access, :prevent_missing_account_access
      skip_before_action :verify_authenticity_token
      skip_before_action :verify_authenticated_session
      skip_before_action :enforce_support_is_active!
      skip_before_action :check_oauth_scopes, only: [:create, :create_new_account]
      skip_around_action :audit_action
      around_action :additional_logging, only: [:create, :create_new_account]
      before_action :validate_account_creation_token, only: :create
      before_action :validate_account_creation_params, only: :create
      before_action :ensure_subdomain_is_not_being_created, only: [:create, :available]

      # the set_region before_action in LocationControllerSupport needs to happen
      # before redirect to pod since redirect to pod uses the region set by set_region
      include Zendesk::Accounts::LocationControllerSupport

      include Zendesk::PodRelay
      include Zendesk::Accounts::OfacControllerSupport

      CONFIRM_UPDATE = 'zendesk_internal_origin'.freeze
      SUBDOMAIN_LOCKUP_TIMEOUT = 60.seconds

      ACCOUNT_PARAMETER = {
        name: Parameters.string,
        subdomain: Parameters.string,
        help_desk_size: Parameters.string,
        _zd_choose_shard_udupi: Parameters.anything,
        source: Parameters.string | Parameters.nil_string,
        language: Parameters.string | Parameters.nil_string,
        utc_offset: Parameters.integer | Parameters.string | Parameters.nil_string,
        remote_login_url: Parameters.string | Parameters.nil_string,
        currency: Parameters.string | Parameters.nil_string,
        remote_authentication: {
          type: Parameters.string,
          remote_login_url: Parameters.string,
          remote_logout_url: Parameters.string | Parameters.nil_string,
          fingerprint: Parameters.string | Parameters.nil_string
        }.freeze,
        google_apps_domain: Parameters.string | Parameters.nil_string,
        suite_trial: Parameters.boolean,
        multiproduct: Parameters.string | Parameters.boolean | Parameters.nil_string
      }.freeze

      OWNER_PARAMETER = {
        name: Parameters.string,
        email: Parameters.string,
        password: Parameters.string | Parameters.nil_string,
        is_verified: Parameters.string | Parameters.boolean | Parameters.nil_string
      }.freeze

      ADDRESS_PARAMETER = {
        phone: Parameters.integer | Parameters.string,
        street: Parameters.string | Parameters.nil_string,
        city: Parameters.string | Parameters.nil_string,
        zip: Parameters.string | Parameters.nil_string,
        province: Parameters.string | Parameters.nil_string
      }.freeze

      AGENTS_PARAMETER = [
        {
          name: Parameters.string,
          email: Parameters.string,
          role: Parameters.string
        }.freeze
      ].freeze

      PARTNER_PARAMETER = {
        name: Parameters.string,
        url: Parameters.string
      }.freeze

      MARKETING_PARAMETER = {
        elqFormName: Parameters.anything,
        elqCustomerGUID: Parameters.anything,
        elqCookieWrite: Parameters.anything,
        elqSiteID: Parameters.anything,
        elqCampaignId: Parameters.anything,
        Web_Offer_Name__c: Parameters.anything,
        GA_Landing: Parameters.anything,
        GA_Referrer: Parameters.anything,
        GA_Medium: Parameters.anything,
        GA_Source: Parameters.anything,
        Opti_ID: Parameters.anything,
        Opti_Variation_ID: Parameters.anything,
        trial_extras: Parameters.anything,
        ZOBU_1: Parameters.anything,
        ZOBU_2: Parameters.anything,
        ZOBU_3: Parameters.anything,
        FirstName: Parameters.anything,
        LastName: Parameters.anything,
        created: Parameters.anything,
      }.freeze

      ## ### Creating Account
      ## `POST /api/v2/accounts.json`
      ##
      ## #### Allowed For
      ##
      ##  * zendeskaccounts account with proper oauth token
      ##
      ## #### Using curl
      ##
      ## Provisioning a trial account:
      ##
      ## ```bash
      ## curl -v https://zendeskaccounts/zendesk.com/api/v2/accounts.json \
      ## -H "Content-Type: application/json" \
      ## -H "Authorization: Bearer {OAUTH_TOKEN}" \
      ## -d '{ "account": {
      ##         "name": {ACCOUNT NAME},
      ##         "subdomain": {UNIQUE SUBDOMAIN},
      ##         "help_desk_size": "Small Team"
      ##       },
      ##       "owner": {
      ##         "name": "Lewis Carroll",
      ##         "email": "alice@example.com"
      ##       },
      ##       "address": {
      ##         "phone": "+1-123-456-7890"
      ##       }
      ##    }'
      ## ```
      ##
      ## Provisioning with a list of agents:
      ##
      ## ```bash
      ## curl -v https://zendeskaccounts.zendesk.com/api/v2/accounts.json \
      ## -X POST \
      ## -H "Content-Type: application/json" \
      ## -H "Authorization: Bearer {OAUTH_TOKEN}" \
      ## -d '{ "account": {
      ##         "name": "My Company",
      ##         "subdomain": "company",
      ##         "help_desk_size": "Small Team"
      ##       },
      ##       "owner": {
      ##         "name": "Lewis Carroll",
      ##         "email": "alice@example.com"
      ##       },
      ##       "address": {
      ##         "phone": "+1-123-456-7890"
      ##       },
      ##       "agents": [
      ##         { "name": "Tweedledum", "email": "Tweedledum@example.com", "role": "Admin" },
      ##         { "name": "Tweedledee", "email": "tweedledee@example.com", "role": "Agent" }
      ##       ]
      ##    }'
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://zendesk.com/api/v2/accounts/{id}.json
      ##
      ## {
      ##   "account": {
      ##     "url": "https://z2nnonza2lisacf3.zendesk.com",
      ##     "name": "Lisa",
      ##     "sandbox": false,
      ##     "subdomain": "z2nnonza2lisacf3",
      ##     "time_format": 24,
      ##     "time_zone": "America/Los_Angeles",
      ##     "owner_id": 21861904307
      ##   },
      ##   "success": true,
      ##   "owner_verification_link": "https://z2nnonza2lisacf3.zendesk.com/verification/challenge/{TOKEN}",
      ##   "deprecation_warning": "This API endpoint is now deprecated and will soon be disabled. Please contact support@zendesk.com if you are a Zendesk Reseller"
      ## }
      ## ```
      allow_parameters :create, {
        account: ACCOUNT_PARAMETER,
        owner: OWNER_PARAMETER,
        address: ADDRESS_PARAMETER,
        agents: AGENTS_PARAMETER,
        partner: PARTNER_PARAMETER,
        include: Parameters.string | Parameters.nil_string,
        force_classic: Parameters.anything, # this is ignored now, but leaving it in for API compatibility
        confirm_origin: Parameters.string,
        force_pod_id: Parameters.string,
        recaptcha_response: Parameters.string,
        force_api_ratelimit: Parameters.boolean | Parameters.nil_string
      }.merge(MARKETING_PARAMETER)
      require_oauth_scopes :create, scopes: %i[accounts:write]
      def create
        # lock subdomain for SUBDOMAIN_LOCKUP_TIMEOUT period to avoid duplicate subdomain errors
        Rails.cache.write(subdomain_availability_check_key, Time.now.utc.to_i, expires_in: SUBDOMAIN_LOCKUP_TIMEOUT)

        multiproduct = params['account']['multiproduct'].nil? || params['account']['multiproduct'] == '1'
        if Arturo.feature_enabled_for_pod?(:ocp_support_shell_account_creation, Zendesk::Configuration.fetch(:pod_id)) &&
            ((subdomain.to_s.downcase =~ /z3n/).present? || in_rollout?) &&
            multiproduct
          response = internal_account_service_client.post('api/services/accounts', account_creation_params)
          if response.success?
            render json: transform_response(response), status: :created
          else
            head :unprocessable_entity
          end
        else
          response = internal_api_account_create_client.post('api/v2/internal/new_account', update_params)
          if response.success?
            render json: response.body, status: :created
          else
            head :unprocessable_entity
          end
        end
      rescue Kragle::ResponseError => e
        Rails.logger.info "[Account Creation] Failed #{e.message}"
        head kragle_exception_to_status_code(e)
      end

      ## ### Query if a subdomain is available and acceptable for use
      ## `GET /api/v2/accounts/available.json`
      ##
      ## #### Allowed For:
      ##
      ##  * Anyone
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/accounts/available.json \
      ##   -H "Content-Type: application/json" -d "{\"subdomain\":\"mysubdomain\"}"
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "success": "true"
      ## }
      ## ```
      allow_parameters :available, subdomain: Parameters.string
      require_oauth_scopes :available, scopes: [:read], arturo: true
      def available
        render json: { success: true }
      end

      protected

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['accounts', 'creation'])
      end

      def subdomain
        # available for create action
        params[:subdomain] || (params[:account][:subdomain] if params[:account])
      end

      def subdomain_availability_check_key
        if !subdomain && !Rails.env.production?
          raise "subdomain: can not be null."
        end

        "cache_subdomain_for_availability_check:#{subdomain}"
      end

      def gsuite_domain
        params[:account][:google_apps_domain] if params[:account]
      end

      # This checks
      #  - Rails cache to reduce concurrent calls
      #  - database to prevent creation call
      # with a used subdomain
      def ensure_subdomain_is_not_being_created
        success = true
        message = ''

        if !subdomain.present?
          message = "Subdomain is empty: #{params.inspect}"
          success = false
        elsif Rails.cache.read(subdomain_availability_check_key)
          message = "Subdomain is taken: #{subdomain}"
          success = false
        elsif !Zendesk::RoutingValidations.subdomain_available?(subdomain)
          message = "Subdomain is not valid: #{subdomain}"
          success = false
        elsif gsuite_domain.present? && (Route.exists?(gam_domain: gsuite_domain) || gsuite_domain == 'gmail.com')
          message = "G Suite domain is taken: #{gsuite_domain}"
          success = false
        end

        unless success
          Rails.logger.info(message)
          statsd_client.increment('validation_failure', tags: ["reason:#{message}"])
          status = request.post? ? :conflict : :ok
          render json: { success: false, message: message }, status: status
        end
      end

      # Basic validation on account creation params before account model is created
      def validate_account_creation_params
        validator = Zendesk::Accounts::CreationParamsValidator.new(params)
        unless validator.valid?
          render json: Api::V2::ErrorsPresenter.new.present(validator), status: :unprocessable_entity
        end
      end

      def validate_account_creation_token
        if oauth_access_token.blank?
          Rails.logger.info("[ApiV2AccountsCreation] FORBIDDEN oauth_account_creation ON so failure")
          statsd_client.increment('oauth_create.invalid')
          head :forbidden
        else
          if valid_oauth_account_creator? && valid_account_creation_oauth_scope?
            Rails.logger.info("[ApiV2AccountsCreation] SUCCESS oauth valid oauth_token id: #{oauth_access_token.id}")
            statsd_client.increment('oauth_create.valid', tags: ["token:#{oauth_access_token.id}"])
          else
            Rails.logger.info("[ApiV2AccountsCreation] FORBIDDEN INVALID oauth_token id: #{oauth_access_token.id}")
            head :forbidden
            statsd_client.increment('oauth_create.invalid.token', tags: ["token:#{oauth_access_token.id}"])
          end
        end
      end

      def valid_oauth_account_creator?
        oauth_token_generator_account_id == oauth_access_token.account_id
      end

      def oauth_token_generator_account_id
        ::Account.find_by_subdomain('zendeskaccounts').try(:id)
      end

      # after roll out will refactor to use check_oauth_scopes in place of this method
      def valid_account_creation_oauth_scope?
        oauth_access_token.scopes.include?("accounts:write")
      end

      def additional_logging
        yield

        log_pod_relaying_and_status
      end

      def log_pod_relaying_and_status
        # if can_relay_request? == true => 2 options: does not need to relay or is relaying
        #   1) if `headers['X-Accel-Redirect']` exists then relay_request_cross_pod was called
        #   and therefore we relay the request
        #   2) if `headers['X-Accel-Redirect']` does NOT exist, we are on the correct pod and don't need to relay
        #
        # if can_relay_request? == false => was relayed (and it will not be relayed again)

        was_relayed = !can_relay_request?
        relayed_to = headers['X-Accel-Redirect']
        is_relaying = can_relay_request? && relayed_to.present?
        relayed_to_pod = relayed_to && relayed_to.to_s.match(/pod_(\d+)/).to_a.second
        z3n_subdomain = (subdomain.to_s.downcase =~ /z3n/).present?
        force_pod_id = params[:force_pod_id].to_i

        statsd_client.increment(
          'create_action',
          tags: [
            "subdomain:#{subdomain}",
            "z3n_subdomain:#{z3n_subdomain}",
            "was_relayed:#{was_relayed}",
            "relayed_to:pod#{relayed_to_pod}",
            "is_relaying:#{is_relaying}",
            "status:#{response.status}",
            "ssl:#{request.ssl?}",
            "action_name:#{action_name}",
            "force_pod_id:#{force_pod_id}"
          ]
        )
      rescue StandardError => e
        Rails.logger.error("Failed to log pod relaying and status. At pod #{Zendesk::Configuration.fetch(:pod_id)}. Tags: subdomain:#{subdomain}, was_relayed:#{was_relayed}, relayed_to:pod#{relayed_to_pod}, is_relaying:#{is_relaying}, status:#{response.status}, ssl:#{request.ssl?}. Error message: #{e.message}")
      end

      def update_params
        params.except(:controller, :action, :format).merge(
          confirm_origin:     CONFIRM_UPDATE,
          creation_channel:   creation_channel,
          original_remote_ip: original_remote_ip,
          region:             region
        )
      end

      def in_rollout?
        Arturo.feature_enabled_for_pod?(:ocp_support_shell_account_creation_all, Zendesk::Configuration.fetch(:pod_id)) ||
          Arturo.feature_enabled_for_pod?(:ocp_support_shell_account_creation_phase1, Zendesk::Configuration.fetch(:pod_id)) && ('a'..'f').cover?(subdomain[0]) ||
          Arturo.feature_enabled_for_pod?(:ocp_support_shell_account_creation_phase2, Zendesk::Configuration.fetch(:pod_id)) && ('g'..'o').cover?(subdomain[0])
      end

      def account_creation_params
        @account_creation_params ||= params.except(:controller, :action, :format).merge(
          products: [
            {
              name: 'support',
              state: Zendesk::Accounts::Product::NOT_STARTED
            }
          ]
        ).tap do |hsh|
          hsh['trial_extras'] = account_creation_params_trial_extras(hsh['trial_extras'])
          hsh['owner']['is_verified'] = hsh['owner']['is_verified'] == '1'
          hsh['pod_id'] = hsh['force_pod_id'] if hsh['force_pod_id'].present?
          hsh['account']['time_zone'] = ::Account::Localization.utc_to_tz(hsh['account']['utc_offset'])
          hsh['account']['locale'] = hsh['account']['language']
        end
      end

      def account_creation_params_trial_extras(hash)
        (hash || {}).tap do |hsh|
          hsh&.each { |key, value| hsh[key] = value.to_s }
          hsh['confirm_origin']     = CONFIRM_UPDATE
          hsh['creation_channel']   = creation_channel if creation_channel
          hsh['original_remote_ip'] = original_remote_ip if original_remote_ip
          hsh['region']             = region if region
          hsh['source']             = params['account']['source'] if params['account'] && params['account']['source']
          hsh['suite_trial']        = params['account']['suite_trial'] if params['account'] && params['account']['suite_trial']
        end
      end

      def creation_channel
        return if oauth_access_token.nil?
        oauth_access_token.user.try(:name).to_s + oauth_access_token.id.to_s
      end

      def internal_api_account_create_client
        Kragle.new('classic') do |conn|
          conn.url_prefix = "https://signup.#{Zendesk::Configuration['host']}"
          conn.headers['Content-Type'] = 'application/json'
          conn.options.timeout = 300 # open/read timeout in seconds
        end
      end

      def internal_account_service_client
        Kragle.new('account-service') do |conn|
          conn.url_prefix = "https://zendeskaccounts.#{Zendesk::Configuration['host']}"
          conn.headers['Content-Type'] = 'application/json'
          conn.headers['Host'] = "zendeskaccounts.#{ENV['ZENDESK_HOST']}"
          conn.options.timeout = 5 # open/read timeout in seconds
        end
      end

      def kragle_exception_to_status_code(exception)
        code = exception.to_s.to_i
        code.zero? ? 500 : code
      end

      def transform_response(response)
        response_body = {
          success: true,
          owner_verification_link: response.body['owner_verification_link']
        }
        response_body[:account] = {
          url: response.body['owner_verification_link']&.split('/verification/')&.first,
          name: response.body['account']['name'],
          sandbox: response.body['account']['sandbox_master_id'].present?,
          subdomain: response.body['account']['subdomain'],
          time_zone: response.body['account']['time_zone'],
          owner_id: response.body['account']['owner_id'],
          multiproduct: response.body['account']['multiproduct']
        } unless response.body['account'].nil?
        response_body
      end
    end
  end
end
