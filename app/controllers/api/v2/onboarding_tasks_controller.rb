class Api::V2::OnboardingTasksController < Api::V2::BaseController
  before_action :require_admin!

  # ### List Onboarding Tasks
  # `GET /api/v2/onboarding_tasks.json`
  #
  # #### Allowed For:
  #
  #  * Admins
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/onboarding_tasks.json?type=support \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "tasks": [
  #     {
  #       "name":            "setup_channels",
  #       "completed":       true,
  #       "changed":         false
  #     },
  #     ...
  #   ]
  # }
  # ```
  allow_parameters :index, type: Parameters.string

  def index
    type = params.fetch('type')
    return head :not_found unless %w[support suite].include?(type)
    render json: Api::V2::OnboardingTasksPresenter.new(current_user, url_builder: self).present(current_user.onboarding(type))
  end

  # ### Create records for completed onboarding tasks
  # `POST /api/v2/onboarding_tasks.json`
  #
  # #### Allowed For:
  #
  #  * Admins
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/onboarding_tasks.json \
  #   -X POST \
  #   -d '{
  #         "name": "setup_channels",
  #         "completed":     true
  #     }' \
  #   -v -u {email_address}:{password}
  # ```
  #

  allow_parameters :create,
    name:       Parameters.string,
    completed:  Parameters.boolean,
    type:       Parameters.string

  def create
    if params[:completed].nil?
      return render json: { error: "must provide :completed attribute" }, status: :unprocessable_entity
    else
      params['status'] =
        if params.delete(:completed)
          OnboardingTask::CompletionStatus::COMPLETED
        else
          OnboardingTask::CompletionStatus::INCOMPLETED
        end
    end
    type = params.fetch('type', 'checklist')
    tasks = OnboardingTask.from_params(current_user, params, type)
    return render json: tasks, status: :created unless tasks.nil?
    render json: { error: "Task '#{params[:name]}' is not currently supported" }, status: :unprocessable_entity
  end

  # ### Find sample ticket id
  # ### If there is no sample ticket, create one
  # `GET /api/v2/onboarding_tasks/find_sample_ticket_id.json`
  #
  # #### Allowed For:
  #
  #  * Admins
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/onboarding_tasks/find_sample_ticket_id.json \
  #      -v \
  #      -u username:password
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   sample_ticket_id: 1
  # }
  #
  # ```

  allow_parameters :find_sample_ticket_id, {
    ticket_type: Parameters.string
  }
  require_oauth_scopes :find_sample_ticket_id, scopes: [:read], arturo: true

  TICKET_TEMPLATES = {
    "meet_the_ticket" => "meet_the_ticket",
    "suite_trial" => "suite_trial_sample_ticket"
  }.freeze

  def find_sample_ticket_id
    head :bad_request and return if sample_ticket_template.blank?

    ticket = sample_ticket ||
      Ticket.generate_sample_ticket_without_notifications(current_account, current_user, sample_ticket_template)

    render json: {sample_ticket_id: ticket.nice_id}
  end

  # ### Email devs the Web Widget setup instructions
  # `POST /api/v2/onboarding_tasks/web_widget_setup_info.json`
  #
  # #### Allowed For:
  #
  #  * Admins
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/onboarding_tasks/web_widget_setup_info.json \
  #   -X POST \
  #   -d '{
  #         "emails": "agent@zendesk.com,agent2@zendesk.com",
  #         "copy_self": true
  #     }' \
  #   -v -u {email_address}:{password}
  # ```
  #

  allow_parameters :web_widget_setup_info,
    emails:    Parameters.string,
    copy_self: Parameters.boolean,
    product:   Parameters.string

  require_oauth_scopes :web_widget_setup_info, scopes: [:write], arturo: true
  def web_widget_setup_info
    OnboardingMailer.send(
      params[:product] == 'connect' ? :deliver_connect_setup_info : :deliver_web_widget_setup_info,
      current_user,
      domain,
      params[:emails],
      params[:copy_self]
    )
    head :ok
  end

  # ### Show Web Widget snippet
  # `GET /api/v2/onboarding_tasks/web_widget_snippet`
  #
  # #### Allowed For:
  #
  #  * Admins
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/onboarding_tasks/web_widget_snippet \
  #   -X GET -v -u {email_address}:{password}
  # ```

  allow_parameters :web_widget_snippet, {}
  require_oauth_scopes :web_widget_snippet, scopes: [:read], arturo: true
  def web_widget_snippet
    render plain: current_account.brand_code_snippet(current_account.default_brand, domain)
  end

  private

  def sample_ticket_template
    return "meet_the_ticket" if params[:ticket_type].blank?
    TICKET_TEMPLATES.fetch(params[:ticket_type], nil)
  end

  def sample_ticket
    subject = I18n.t("txt.zero_states.sample_ticket.#{sample_ticket_template}.subject")
    current_account.tickets.where(
      via_id:  ViaType.SAMPLE_TICKET,
      subject: subject,
      status_id: [StatusType.NEW, StatusType.OPEN, StatusType.PENDING, StatusType.SOLVED]
    ).first
  end

  def domain
    request.host.split('.')[1..-1].join('.')
  end
end
