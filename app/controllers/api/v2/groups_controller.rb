module Api
  module V2
    class GroupsController < Api::V2::BaseController
      require_that_user_can! :view, Group,
        only: %i[index assignable autocomplete]

      require_that_user_can! :view, :group,     only: :show
      require_that_user_can! :edit, :group,     only: %i[update destroy]
      require_that_user_can! :edit, :new_group, only: :create

      allow_subsystem_user :audit_log_service, only: [:show]
      allow_subsystem_user :gooddata, only: [:index, :count]
      allow_subsystem_user :collaboration, only: [:index, :show, :count]
      allow_subsystem_user :knowledge_api, only: [:index, :count, :show]
      allow_zopim_user only: [:index, :show, :update, :create, :count]
      allow_bime_user only: [:index, :show, :count]
      require_capability :groups, only: :create
      allow_cors_from_zendesk_external_domains :show

      skip_before_action :enforce_support_is_active!, only: [:index, :show]

      CBP_SORTABLE_FIELDS = {
        'id' => :id,
        'name' => :name
      }.freeze

      ## ### List Groups
      ##
      ## * `GET /api/v2/groups.json`
      ## * `GET /api/v2/users/{user_id}/groups.json`
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/groups.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "groups": [
      ##     {
      ##       "name":       "DJs",
      ##       "created_at": "2009-05-13T00:07:08Z",
      ##       "updated_at": "2011-07-22T00:11:12Z",
      ##       "id":         211
      ##     },
      ##     {
      ##       "name":       "MCs",
      ##       "created_at": "2009-08-26T00:07:08Z",
      ##       "updated_at": "2010-05-13T00:07:08Z",
      ##       "id":         122
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, user_id: Parameters.bigid
      allow_cursor_pagination_v2_parameters :index
      def index
        expires_in(60.seconds) if current_account.has_groups_controller_cache_control_header? && app_request?

        if current_account.arturo_enabled?('remove_offset_pagination_groups_index') || (current_account.arturo_enabled?('cursor_pagination_groups_index') && !with_offset_pagination?) || with_cursor_pagination_v2?
          render json: cursor_presenter.present(paginate_with_cursor(sorted_scope_for_cursor(scope, CBP_SORTABLE_FIELDS)))
        else
          render json: presenter.present(groups)
        end
      end

      ## ### Count Groups
      ##
      ## * `GET /api/v2/groups/count.json`
      ## * `GET /api/v2/users/{user_id}/groups/count.json`
      ##
      ## Returns an approximate count of groups. If the count exceeds 100,000, it is updated every 24 hours.
      ##
      ## The `count[refreshed_at]` property is a timestamp that indicates when the count was last updated.
      ##
      ## **Note**: When the count exceeds 100,000, `count[refreshed_at]` may occasionally be `null`.
      ## This indicates that the count is being updated in the background, and `count[value]` is limited to 100,000 until the update is complete.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/groups/count.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## {
      ##    count: {
      ##      value: 102,
      ##      refreshed_at: "2020-04-06T02:18:17Z"
      ##    },
      ##    links: {
      ##      url: "https://{subdomain}.zendesk.com/api/v2/groups/count"
      ##    }
      ## }
      ## ```
      allow_parameters :count, user_id: Parameters.bigid
      def count
        render json: { count: record_counter.present, links: { url: request.original_url } }
      end

      ## ### List assignable groups
      ##
      ## `GET /api/v2/groups/assignable.json`
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/groups/assignable.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "groups": [
      ##     {
      ##       "name":       "DJs",
      ##       "created_at": "2009-05-13T00:07:08Z",
      ##       "updated_at": "2011-07-22T00:11:12Z",
      ##       "id":         211
      ##     },
      ##     {
      ##       "name":       "MCs",
      ##       "created_at": "2009-08-26T00:07:08Z",
      ##       "updated_at": "2010-05-13T00:07:08Z",
      ##       "id":         122
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :assignable, {}
      allow_cursor_pagination_v2_parameters :assignable
      require_oauth_scopes :assignable, scopes: [:read], arturo: true
      def assignable
        expires_in(60.seconds) if app_request? && current_account.has_groups_assignable_cache_control_header?

        if current_account.arturo_enabled?('remove_offset_pagination_groups_assignable') || (current_account.arturo_enabled?('cursor_pagination_groups_assignable') && !with_offset_pagination?) || with_cursor_pagination_v2?
          render json: cursor_presenter.present(paginate_with_cursor(sorted_scope_for_cursor(assignable_groups, CBP_SORTABLE_FIELDS)))
        else
          render json: presenter.present(assignable_groups)
        end
      end

      ## ### Show Group
      ## `GET /api/v2/groups/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/groups/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "group": {
      ##     "name":       "MCs",
      ##     "created_at": "2009-08-26T00:07:08Z",
      ##     "updated_at": "2010-05-13T00:07:08Z",
      ##     "id":         122
      ##   }
      ## }
      ## ```
      resource_action :show

      ## ### Create Group
      ## `POST /api/v2/groups.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/groups.json \
      ##   -H "Content-Type: application/json" -d '{"group": {"name": "My Group"}}'
      ##   -v -u {email_address}:{password} -X POST
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/groups/{id}.json
      ##
      ## {
      ##   "group": {
      ##     "name":       "MCs",
      ##     "created_at": "2009-08-26T00:07:08Z",
      ##     "updated_at": "2010-05-13T00:07:08Z",
      ##     "id":         122,
      ##     "settings": {
      ##        chat_enabled: true
      ##     }
      ##   }
      ## }
      ## ```
      allow_parameters :create, group: {
        name: Parameters.string,
        description: Parameters.string,
        settings: {
          chat_enabled: Parameters.boolean
        }
      }
      def create
        new_group.save!

        render(
          json:     presenter.present(new_group),
          status:   :created,
          location: presenter.url(new_group)
        )
      end

      ## ### Update Group
      ## `PUT /api/v2/groups/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/groups/{id}.json \
      ##   -H "Content-Type: application/json" -d '{"group": {"name": "Interesting Group"}}' \
      ##   -v -u {email_address}:{password} -X PUT
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "group": {
      ##     "name":       "Interesting Group",
      ##     "created_at": "2011-04-20T17:49:00Z",
      ##     "updated_at": "2011-07-20T17:49:00Z",
      ##     "id":         123,
      ##     "settings": {
      ##        "chat_enabled": true
      ##     }
      ##   }
      ## }
      ## ```
      allow_parameters :update, id: Parameters.bigid, group: {
        name: Parameters.string,
        description: Parameters.string,
        settings: {
          chat_enabled: Parameters.boolean
        }
      }
      def update
        group.update_attributes!(params[:group])

        render json: presenter.present(group)
      end

      ## ### Delete Group
      ## `DELETE /api/v2/groups/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/groups/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      allow_parameters :destroy, id: Parameters.bigid
      def destroy
        group.deactivate!

        default_delete_response
      end

      # h3 Autocomplete Groups
      # `GET /api/v2/groups/autocomplete.json?name={name}`
      #
      # Returns an array of groups whose name starts with the value specified in
      # the `name` parameter.
      #
      # h4 Allowed For
      #
      #  * Admins
      #  * Agents
      #
      # h4 Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/v2/groups/autocomplete.json?name=DJs
      #   -u {email_address}:{password}
      # ```
      #
      # h4 Example Response
      #
      # ```http
      # Status: 200
      #
      # {
      #   "groups": [
      #     {
      #       "name": "DJs - Day",
      #       "id":   211,
      #       ...
      #     },
      #     {
      #       "name": "djs - Night",
      #       "id":   232,
      #       ...
      #     }
      #   ]
      # }
      # ```
      allow_parameters :autocomplete, name: Parameters.string
      require_oauth_scopes :autocomplete, scopes: [:read], arturo: true
      def autocomplete
        render json: presenter.present(matching_groups)
      end

      protected

      def max_per_page
        1_000
      end

      def presenter
        @presenter ||= begin
          Api::V2::GroupPresenter.new(
            current_user,
            url_builder: self,
            includes:    includes
          )
        end
      end

      def cursor_presenter
        @cursor_presenter ||= Api::V2::GroupPresenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
      end

      def scope
        if gooddata_request? || bime_request?
          current_account.all_groups.order(name: :asc)
        else
          if params[:user_id].present?
            current_account.users.find(params[:user_id]).groups
          else
            current_account.groups
          end
        end
      end

      def groups
        @groups ||= paginate(scope)
      end

      def assignable_groups
        @assignable_groups ||= begin
          current_account.groups.assignable(current_user).paginate(pagination)
        end
      end

      def group
        @group ||= begin
          current_account.all_groups.find(params[:id].to_i).tap do |group|
            group.current_user = current_user
          end
        end
      end

      def new_group
        @new_group ||= current_account.groups.new.tap do |new_group|
          new_group.attributes   = params[:group] || {}
          new_group.current_user = current_user
        end
      end

      def matching_groups
        @matching_groups ||= begin
          if params[:name].present?
            paginate(current_account.groups.with_name_like(params[:name]).order_for_search(params[:name]))
          else
            []
          end
        end
      end

      def record_counter
        @record_counter ||= begin
          Zendesk::RecordCounter::Groups.new(
            account: current_account,
            options: params
          )
        end
      end
    end
  end
end
