class Api::V2::MobileDevicesController < Api::V2::BaseController
  ## ### List Mobile Devices
  ## `GET /api/v2/mobile_devices.json`
  ##
  ## Lists the active mobile devices registered with Zendesk for push notification support.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## The listing is restricted to devices registered by the inquiring agent
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/mobile_devices.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "mobile_devices": [
  ##     {
  ##       "id":              39,
  ##       "url":             "https://company.zendesk.com/api/v2/mobile_devices/39.json",
  ##       "token":           "5D41402ABC4B2A76B9719D911017C592",
  ##       "device_type:":    "iPhone",
  ##       "mobile_app:":     "com.zendesk.agent",
  ##       "created_at":      "2012/03/05 10:38:52 +1000",
  ##       "updated_at":      "2012/03/05 10:38:52 +1000"
  ##     },
  ##     {
  ##       "id":              72,
  ##       "url":             "https://company.zendesk.com/api/v2/mobile_devices/72.json",
  ##       "token":           "3EBA9A0D2274B11693FD821353A6149F",
  ##       "device_type:":    "Android",
  ##       "mobile_app:":     "com.zendesk.agent",
  ##       "created_at":      "2012/03/05 20:19:45 +1000",
  ##       "updated_at":      "2012/03/05 20:19:45 +1000"
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(device_identifiers)
  end

  ## ### Show Forum
  ## `GET /api/v2/mobile_devices/{id or token}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## Showsing a mobile device is restricted to devices registered by the inquiring agent
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/mobile_devices/{id or token}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "mobile_device": {
  ##     "id":              89,
  ##     "url":             "https://company.zendesk.com/api/v2/mobile_devices/89.json",
  ##     "token":           "3EBA9A0D2",
  ##     "device_type:":    "iPad",
  ##     "mobile_app:":     "com.zendesk.agent",
  ##     "created_at":      "2012/03/05 20:19:45 +1000",
  ##     "updated_at":      "2012/03/05 20:19:45 +1000"
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.string
  def show
    render json: presenter.present(device_identifier)
  end

  ## ### Create or Update Mobile Devices
  ## `POST /api/v2/mobile_devices.json`
  ##
  ## Either creates a new mobile device or executes a registration process on an existing one. The reason for
  ## the ambiguity here is that the client cannot guarantee whether its token is new or not. Users can wipe
  ## and reinstall an application entirely client side.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/mobile_devices.json \
  ##   -H "Content-Type: application/json" -X POST -v -u {email_address}:{password} \
  ##   -d '{"mobile_device": {"device_type": "iPad", "token": "3EBA9A0D2" }}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## If no device was found for the current user with the given token
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/mobile_devices/{id}.json
  ##
  ## {
  ##   "mobile_device": {
  ##     "id":              89,
  ##     "url":             "https://company.zendesk.com/api/v2/mobile_devices/89.json",
  ##     "token":           "3EBA9A0D2",
  ##     "device_type:":    "iPad",
  ##     "mobile_app:":     "com.zendesk.agent",
  ##     "created_at":      "2012/03/05 20:19:45 +1000",
  ##     "updated_at":      "2012/03/05 20:19:45 +1000"
  ##   }
  ## }
  ## ```
  ##
  ## If an existing device was found for the current user with the given token
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "mobile_device": {
  ##     "id":              89,
  ##     "url":             "https://company.zendesk.com/api/v2/mobile_devices/89.json",
  ##     "token":           "3EBA9A0D2",
  ##     "device_type:":    "iPad",
  ##     "mobile_app:":     "com.zendesk.agent",
  ##     "created_at":      "2012/03/05 20:19:45 +1000",
  ##     "updated_at":      "2012/04/02 09:12:38 +1000"
  ##   }
  ## }
  ## ```
  allow_parameters :create, mobile_device: {
    device_type: Parameters.string,
    token: Parameters.string,
    token_type: Parameters.enum(*PushNotifications::DeviceIdentifier::TOKEN_TYPES) | Parameters.nil,
    mobile_app: Parameters.string
  }
  def create
    if existing_device?
      device_identifier.register
      render json: presenter.present(device_identifier), status: :ok
    else
      new_device_identifier.save!
      render json: presenter.present(new_device_identifier), status: :created, location: presenter.url(new_device_identifier)
    end
  end

  ## ### Clear Badge for a Mobile Device
  ## `PUT /api/v2/mobile_devices/{id or token}/clear_badge.json`
  ##
  ## Records the status as viewed and hence sends instructions via push to clear badges
  ##
  ## #### Allowed For
  ##
  ##  * Agent who registered the device
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/mobile_devices/{id or token}.json \
  ##   -v -u {email_address}:{password} -X PUT -d '{}' -H "Content-Type: application/json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :clear_badge, id: Parameters.string
  require_oauth_scopes :clear_badge, scopes: [:write], arturo: true
  def clear_badge
    device_identifier.record_view!
    head :ok
  end

  ## ### Delete Mobile Device
  ## `DELETE /api/v2/mobile_devices/{id or token}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agent who registered the device
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/mobile_devices/{id or token}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.string
  def destroy
    device_identifier.destroy
    default_delete_response
  end

  private

  def presenter
    @presenter ||= Api::V2::MobileDevicePresenter.new(current_user, url_builder: self)
  end

  def new_device_identifier
    mobile_app_identifier = params[:mobile_device][:mobile_app] || 'com.zendesk.agent'
    params[:mobile_device][:mobile_app] = MobileApp.find_by_identifier(mobile_app_identifier)
    params[:mobile_device][:user] = current_user
    @new_device_identifier ||= PushNotifications::FirstPartyDeviceIdentifier.new(params[:mobile_device])
  end

  def device_identifiers
    @device_identifiers ||= current_user.device_identifiers.first_party.active.includes(:mobile_app)
  end

  def device_identifier
    # This is not a bug. The show URL is /api/v2/mobile_devices/{id or token}.json
    @device_identifier ||= begin
      device   = current_user.device_identifiers.first_party.find_by_token(params[:id])
      device ||= current_user.device_identifiers.first_party.find(params[:id])
      device
    end
  end

  def existing_device?
    @device_identifier ||= begin
      if device = current_user.device_identifiers.first_party.find_by_token(params[:mobile_device][:token])
        device.device_type = params[:mobile_device][:device_type]
        device
      end
    end

    @device_identifier.present?
  end
end
