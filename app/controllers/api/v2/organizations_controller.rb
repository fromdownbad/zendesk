require 'zendesk/offset_pagination_limiter'

module Api
  module V2
    class OrganizationsController < Api::V2::BaseController
      allow_subsystem_user :zdp_metadata, only: [:index]

      before_action :extract_custom_fields, only: [:create, :update]
      before_action :check_update_permissions, only: [:update, :update_many]
      before_action :validate_many_organizations_limit, only: [:update_many, :create_many]

      around_action :handle_rate_limiting_on_deep_offset_pagination, only: [:index]

      allow_zopim_user only: [:show]
      allow_cors_from_zendesk_external_domains :show_many

      require_that_user_can! :view, :organization, only: [:show]
      require_that_user_can! :list, Organization,  only: [:index, :count, :autocomplete, :show_many]
      require_that_user_can! :edit, Organization,  only: [:create, :create_many, :destroy, :search, :destroy_many]

      require_capability :organizations, only: [:create, :create_many]

      rescue_from Organization::WriteInProgressError do |e|
        render json: { error: "WriteInProgressError", description: e.message }, status: :unprocessable_entity
      end

      organization_parameter = {
        name: Parameters.string,
        domain_names: Parameters.array(Parameters.string) | Parameters.string,
        details: Parameters.string,
        notes: Parameters.string,
        group_id: Parameters.bigid,
        shared_tickets: Parameters.boolean,
        shared_comments: Parameters.boolean,
        tags: Parameters.array(Parameters.string) | Parameters.string,
        external_id: Parameters.string | Parameters.integer,
        organization_fields: Parameters.map
      }

      organization_batch_parameter = organization_parameter.dup.tap { |o| o[:id] = Parameters.bigid }

      allow_parameters :all, user_id: Parameters.bigid
      ## ### List Organizations
      ##
      ## * `GET /api/v2/organizations.json`
      ## * `GET /api/v2/users/{user_id}/organizations.json`
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "organizations": [
      ##     {
      ##       "id":   35436,
      ##       "name": "One Organization",
      ##       ...
      ##     },
      ##     {
      ##       "id":   20057623,
      ##       "name": "Other Organization",
      ##       ...
      ##     },
      ##   ]
      ## }
      ## ```
      allow_parameters :index, {}
      allow_cursor_pagination_v2_parameters :index
      require_oauth_scopes :index, scopes: [:"organizations:read", :read]
      def index
        throttle_index_with_deep_offset_pagination
        expires_in(current_account.settings.organizations_index_cache_control_header_ttl_seconds.seconds)

        json_to_be_rendered = if current_account.arturo_enabled?('remove_offset_pagination_organizations_index') || (current_account.arturo_enabled?('cursor_pagination_organizations_index') && !with_offset_pagination?) || with_cursor_pagination_v2?
          sorted_records = sorted_scope_for_cursor(organizations)
          cursor_presenter.present(paginate_with_cursor(sorted_records))
        else
          presenter.present(organizations)
        end

        if current_account.has_etag_caching_organizations_index?
          render json: json_to_be_rendered if stale_collection?(organizations)
        else
          render json: json_to_be_rendered
        end
      end

      ## ### Count Organizations
      ##
      ## * `GET /api/v2/organizations/count.json`
      ## * `GET /api/v2/users/{user_id}/organizations/count.json`
      ##
      ## Returns an approximate count of organizations. If the count exceeds 100,000, it is updated every 24 hours.
      ##
      ## The `count[refreshed_at]` property is a timestamp that indicates when the count was last updated.
      ##
      ## **Note**: When the count exceeds 100,000, `count[refreshed_at]` may occasionally be `null`.
      ## This indicates that the count is being updated in the background, and `count[value]` is limited to 100,000 until the update is complete.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/count.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##    count: {
      ##      value: 102,
      ##      refreshed_at: "2020-04-06T02:18:17Z"
      ##    },
      ##    links: {
      ##      url: "https://{subdomain}.zendesk.com/api/v2/organizations/count"
      ##    }
      ## }
      ## ```
      allow_parameters :count, user_id: Parameters.bigid
      require_oauth_scopes :count, scopes: [:"organizations:read", :read]
      def count
        render json: { count: record_counter.present, links: { url: request.original_url } }
      end

      ## ### Autocomplete Organizations
      ## `GET /api/v2/organizations/autocomplete.json?name={name}`
      ##
      ## Returns an array of organizations whose name starts with the value specified
      ## in the `name` parameter.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/autocomplete.json?name=imp \
      ##   -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "organizations": [
      ##     {
      ##       "id":   35436,
      ##       "name": "Important Customers",
      ##       ...
      ##     },
      ##     {
      ##       "id":   20057623,
      ##       "name": "Imperial College",
      ##       ...
      ##     },
      ##   ]
      ## }
      ## ```
      allow_parameters :autocomplete, name: Parameters.string
      require_oauth_scopes :autocomplete, scopes: [:"organizations:read", :read, :write]
      def autocomplete
        render json: presenter.present(matching_organizations)
      end

      ## ### Show Organization's Related Information
      ## `GET /api/v2/organizations/{id}/related.json`
      ##
      ## #### Allowed For:
      ##
      ##  * Agents
      ##
      ## #### Using curl:
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/{id}/related.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200
      ##
      ## {
      ##   "organization_related": {
      ##     "users_count": 4,
      ##     "tickets_count": 12
      ##   }
      ## }
      ## ```
      allow_parameters :related, id: Parameters.bigid
      require_oauth_scopes :related, scopes: [:"organizations:read", :read, :write]
      def related
        render json: organization_related_presenter.present(organization)
      end

      ## ### Show Organization
      ## `GET /api/v2/organizations/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "organization": {
      ##     "id":   35436,
      ##     "name": "My Organization",
      ##     ...
      ##   }
      ## }
      ## ```
      require_oauth_scopes :show, scopes: [:"organizations:read", :read]
      resource_action :show

      ## ### Show Many Organizations
      ## `GET /api/v2/organizations/show_many.json?ids={ids}`
      ##
      ## `GET /api/v2/organizations/show_many.json?external_ids={external_ids}`
      ##
      ## Accepts a comma-separated list of up to 100 organization ids or external ids.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/show_many.json?ids=1,2 \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "organizations": [
      ##     {
      ##       "id":   35436,
      ##       "name": "Important Customers",
      ##       ...
      ##     },
      ##     {
      ##       "id":   20057623,
      ##       "name": "Imperial College",
      ##       ...
      ##     },
      ##   ]
      ## }
      ## ```
      allow_parameters :show_many, ids: Parameters.ids, external_ids: Parameters.string
      require_oauth_scopes :show_many, scopes: [:"organizations:read", :read, :write]
      def show_many
        render json: presenter.present(many_organizations)
      end

      ## ### Create Organization
      ## `POST /api/v2/organizations.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations.json \
      ##   -H "Content-Type: application/json" -d '{"organization": {"name": "My Organization"}}' \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/organizations/{id}.json
      ##
      ## {
      ##   "organization": {
      ##     "id":   35436,
      ##     "name": "My Organization",
      ##     ...
      ##   }
      ## }
      ## ```
      ##
      ## **Note**: You must provide a unique name for each organization. Normally
      ## the system doesn't allow records to be created with identical names.
      ## However, a race condition can occur if you make two or more identical
      ## POSTs very close to each other, causing the records to have identical
      ## organization names.
      ##
      allow_parameters :create, organization: organization_parameter
      require_oauth_scopes :create, scopes: [:"organizations:write", :write]
      def create
        new_organization.save!
        render json: presenter.present(new_organization), status: :created, location: presenter.url(new_organization)
      end

      ## ### Create Many Organizations
      ## `POST /api/v2/organizations/create_many.json`
      ##
      ## Accepts an array of up to 100 organization objects.
      ##
      ## #### Allowed For
      ##
      ##  * Agents, with restrictions applying on certain actions
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/organizations/create_many.json \
      ##   -H "Content-Type: application/json" -X POST -d '{"organizations": [{"name": "Org1"}, {"name": "Org2"}]}'
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :create_many, organizations: [organization_parameter]
      require_oauth_scopes :create_many, scopes: [:"organizations:write", :write, :read]
      def create_many
        unless params[:organizations].present?
          raise Zendesk::UnknownAttributeError, "missing organizations parameter"
        end

        render json: job_status_presenter.present(enqueue_job_with_status(OrganizationBulkCreateJob, params))
      end

      ## ### Create Or Update Organization
      ## `POST /api/v2/organizations/create_or_update.json`
      ##
      ## Creates an organization if it doesn't already exist, or updates an existing organization
      ## identified by ID or external ID. Using this method means one less call to check if an
      ## organization exists before creating it.
      ##
      ## #### Allowed For
      ##
      ##  * Agents, with restrictions on certain actions
      ##
      ## #### Using curl
      ##
      ## Existing organization identified by ID:
      ##
      ## ```bash
      ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/organizations/create_or_update.json \
      ##   -H "Content-Type: application/json" -X POST -d '{"organization": {"id": "123", "name": "My Organization"}}'
      ## ```
      ##
      ## Existing organization identified by external ID:
      ##
      ## ```bash
      ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/organizations/create_or_update.json \
      ##   -H "Content-Type: application/json" -X POST -d '{"organization": {"external_id": "abc_123", "name": "My Organization"}}'
      ## ```
      ##
      ## #### Example Responses
      ##
      ## If the organization was created:
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: /api/v2/organizations/{new-organization-id}.json
      ##
      ## {
      ##   "organization": {
      ##     "id":   123,
      ##     "name": "My Organization",
      ##     ...
      ##   }
      ## }
      ## ```
      ##
      ## If an existing organization was updated:
      ##
      ## ```http
      ## Status: 200 OK
      ## Location: /api/v2/organizations/{existing-organization-id}.json
      ##
      ## {
      ##   "organization": {
      ##     "id":   123,
      ##     "name": "My Organization",
      ##     ...
      ##   }
      ## }
      ## ```
      ##
      allow_parameters :create_or_update, organization: organization_batch_parameter
      def create_or_update
        params.require(:organization)
        organization = Organization.update_existing_or_initialize_new_organization(current_scope, current_user, params)
        is_new_organization = organization.new_record?

        organization.save!

        render json: presenter.present(organization), status: is_new_organization ? :created : :ok, location: presenter.url(organization)
      end

      ## ### Update Organization
      ## `PUT /api/v2/organizations/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## Agents with no permissions restrictions can only update "notes" on organizations.
      ##
      ## #### Example Request
      ##
      ## ```js
      ## {
      ##   "organization": {
      ##     "notes": "Something interesting"
      ##   }
      ## }
      ## ```
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/{id}.json \
      ##   -H "Content-Type: application/json" -d '{"organization": {"notes": "Something interesting"}}' \
      ##   -v -u {email_address}:{password} -X PUT
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "organization": {
      ##     "id":    35436,
      ##     "name":  "My Organization",
      ##     "notes": "Something interesting",
      ##     ...
      ##   }
      ## }
      ## ```
      allow_parameters :update, id: Parameters.bigid, organization: organization_parameter
      require_oauth_scopes :update, scopes: [:"organizations:write", :write]
      def update
        if current_user.can?(:edit, Organization)
          organization.attributes = params[:organization] || {}
        elsif params[:organization].present? && params[:organization]["notes"]
          # if we get here we are an agent with permission for notes so only let agents update notes
          organization[:notes] = params[:organization]["notes"]
        end
        organization.custom_field_values.update_from_hash(@custom_fields)
        organization.save!
        render json: presenter.present(organization)
      end

      ## ### Update Many Organizations
      ## `PUT /api/v2/organizations/update_many.json`
      ##
      ## `PUT /api/v2/organizations/update_many.json?ids={ids}`
      ##
      ## `PUT /api/v2/organizations/update_many.json?external_ids={external_ids}`
      ##
      ## Bulk or batch updates up to 100 organizations.
      ##
      ## #### Bulk update
      ##
      ## To make the same change to multiple organizations, use the following endpoint and data format:
      ##
      ## `https://{subdomain}.zendesk.com/api/v2/organizations/update_many.json?ids=1,2,3`
      ##
      ## ```js
      ## {
      ##   "organization": {
      ##     "notes": "Priority"
      ##   }
      ## }
      ## ```
      ##
      ## #### Batch update
      ##
      ## To make different changes to multiple organizations, use the following endpoint and data format:
      ##
      ## `https://{subdomain}.zendesk.com/api/v2/organizations/update_many.json`
      ##
      ## ```js
      ## {
      ##   "organizations": [
      ##     { "id": 1, "notes": "Priority" },
      ##     { "id": 2, "notes": "Normal" }
      ##   ]
      ## }
      ## ```
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##  * Agents
      ##
      ## Agents with no permissions restrictions can only update "notes" on organizations.
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/update_many.json?ids=1,2 \
      ##   -H "Content-Type: application/json" \
      ##   -v -u {email_address}:{password} -X PUT \
      ##   -d '{"organization": {"notes": "Something interesting"}}'
      ## ```
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/update_many.json \
      ##   -H "Content-Type: application/json" \
      ##   -v -u {email_address}:{password} -X PUT \
      ##   -d '{"organizations": [{"id": "1", "notes": "Something interesting"}, {"external_id": "2", "notes": "Something even more interesting"}]}'
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :update_many, ids: Parameters.ids, external_ids: Parameters.string, organization: organization_parameter, organizations: [organization_batch_parameter]
      require_oauth_scopes :update_many, scopes: [:"organizations:write", :write, :read]
      def update_many
        render json: job_status_presenter.present(update_many_job)
      end

      ## ### Delete Organization
      ## `DELETE /api/v2/organizations/{id}.json`
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      require_oauth_scopes :destroy, scopes: [:"organizations:write", :write]
      resource_action :destroy

      ## ### Bulk Delete Organizations
      ## `DELETE /api/v2/organizations/destroy_many.json?ids={ids}`
      ##
      ## `DELETE /api/v2/organizations/destroy_many.json?external_ids={external_ids}`
      ##
      ## Accepts a comma-separated list of up to 100 organization ids or external ids.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/destroy_many.json?ids=1,2,3 \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :destroy_many, ids: Parameters.ids, external_ids: Parameters.string
      require_oauth_scopes :destroy_many, scopes: [:"organizations:write", :write, :read]
      def destroy_many
        if many_ids?
          job = enqueue_job_with_status(OrganizationBulkDeleteJob, ids: many_ids, key: :id)
        elsif many_ids?(:external_ids)
          job = enqueue_job_with_status(OrganizationBulkDeleteJob, ids: many_ids(:external_ids), key: :external_id)
        else
          raise ActionController::ParameterMissing, "Either ids or external_ids must be present"
        end
        render json: job_status_presenter.present(job)
      end

      ## ### Search Organizations by External ID
      ## `GET /api/v2/organizations/search.json?external_id={external_id}`
      ##
      ## If you set the `external_id` value of an organization to associate it to an
      ## external record, you can use the external id to search for the organization.
      ##
      ## #### Allowed For:
      ##
      ##  * Admins
      ##  * Agents assigned to a custom role with permissions to add or modify organizations (Enterprise only)
      ##
      ## See [Creating custom agent roles](https://support.zendesk.com/hc/en-us/articles/203662026#topic_cxn_hig_bd)
      ## in the Support Help Center.
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/organizations/search.json?external_id={external_id} \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## See [Listing Organizations](#listing-organizations)
      allow_parameters :search, external_id: Parameters.string
      require_oauth_scopes :search, scopes: [:"organizations:write", :write, :read]
      def search
        render json: presenter.present(search_organizations)
      end

      protected

      def check_update_permissions
        deny_access unless current_user.can?(:edit_notes, Organization) || current_user.can?(:edit, Organization)
      end

      def extract_custom_fields
        @custom_fields = params[:organization].delete(:organization_fields) if params[:organization]
      end

      def presenter
        @presenter ||= Api::V2::Organizations::Presenter.new(current_user, url_builder: self, includes: includes)
      end

      def cursor_presenter
        @cursor_presenter ||= Api::V2::Organizations::Presenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
      end

      def job_status_presenter
        @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
      end

      def organization_related_presenter
        @organization_related_presenter ||= Api::V2::OrganizationRelatedPresenter.new(current_user, url_builder: self)
      end

      def organization
        @organization ||= current_scope.organizations.find(params[:id])
      end

      def new_organization
        @new_organization ||= build_organization(params[:organization], @custom_fields)
      end

      def build_organization(params, custom_fields)
        org = current_scope.organizations.build(params)
        org.custom_field_values.update_from_hash(custom_fields)
        org
      end

      def organizations
        @organizations ||= paginate_with_large_count_cache(current_scope.organizations)
      end

      def search_organizations
        @search_organizations ||= Array.wrap(current_scope.organizations.find_by_external_id(params.require(:external_id).to_s))
      end

      def many_organizations
        if many_ids?
          conditions = { id: many_ids }
        elsif many_ids?(:external_ids)
          conditions = { external_id: many_ids(:external_ids) }
        else
          raise ActionController::ParameterMissing, "Either ids or external_ids must be present"
        end
        current_account.organizations.where(conditions)
      end

      def matching_organizations
        @matching_organizations ||= begin
          name = params[:name].to_s
          if name.empty?
            []
          else
            paginate(current_scope.organizations.with_name_like(name))
          end
        end
      end

      def update_many_job
        if params[:organizations]
          enqueue_job_with_status(OrganizationBatchUpdateJob, params)
        elsif params[:organization]
          extract_custom_fields
          if many_ids?
            enqueue_job_with_status(OrganizationBulkUpdateJob, params.merge(ids: many_ids, key: :id, custom_fields: @custom_fields))
          elsif many_ids?(:external_ids)
            enqueue_job_with_status(OrganizationBulkUpdateJob, params.merge(ids: many_ids(:external_ids), key: :external_id, custom_fields: @custom_fields))
          else
            raise ActionController::ParameterMissing, "Both ids/external_ids and organization attributes must be present"
          end
        else
          raise ActionController::ParameterMissing, "Both ids/external_ids and organization attributes must be present"
        end
      end

      def validate_many_organizations_limit
        if (params[:organizations] || many_ids || many_ids(:external_ids)).try(:length).to_i > many_organizations_limit
          raise ZendeskApi::Controller::Errors::TooManyValuesError.new("", many_organizations_limit)
        end
      end

      def many_organizations_limit
        current_account.settings.many_organizations_limit || 100
      end

      def max_per_page
        1000
      end

      def current_scope
        if params[:user_id].present?
          current_account.users.find(params[:user_id])
        else
          current_account
        end
      end

      def record_counter
        @record_counter ||= begin
          Zendesk::RecordCounter::Organizations.new(
            account: current_account,
            options: params
          )
        end
      end

      private

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: "organizations")
      end

      def throttle_index_with_deep_offset_pagination
        return nil unless params[:page].is_a?(Integer)

        Zendesk::OffsetPaginationLimiter.new(
          entity: controller_name,
          account_id: current_account.id,
          account_subdomain: current_account.subdomain,
          page: params[:page],
          per_page: per_page,
          log_only: true
        ).throttle_index_with_deep_offset_pagination
      end

      def handle_rate_limiting_on_deep_offset_pagination
        yield
      rescue Prop::RateLimited => e
        if e.handle == :index_organizations_with_deep_offset_pagination_ceiling_limit
          Rails.logger.info("[OrganizationsController] Rate limiting #index by account: #{current_account.subdomain}, page: #{params[:page]}, per_page: #{per_page}, throttle key: index_organizations_with_deep_offset_pagination_ceiling_limit")
          statsd_client.increment("api_v2_organizations_index_ceiling_rate_limit", tags: ["account_id:#{current_account.id}"])
          response.headers['Retry-After'] = e.retry_after.to_s
          render plain: I18n.t("txt.api.v2.organizations.index_with_deep_offset_pagination.rate_limit_by_account") + " per_page: #{params[:per_page]}, page: #{params[:page]}", status: 429, content_type: Mime[:text].to_s
        else
          Rails.logger.warn("Api::V2::OrganizationsController Rate Limited #{e.message}")
          raise e
        end
      end
    end
  end
end
