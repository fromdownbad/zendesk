class Api::V2::TicketAuditsController < Api::V2::BaseController
  before_action :require_admin!

  ## ### List All Ticket Audits
  ## `GET /api/v2/ticket_audits.json`
  ##
  ## `GET /api/v2/ticket_audits.json?cursor=fDE1MDE1OTE1MzQuMHx8MTEzMjQ4NDI1MQ%3D%3D`
  ##
  ## Archived tickets are not included in the response. See
  ## [About archived tickets](https://support.zendesk.com/hc/en-us/articles/203657756) in
  ## the Support Help Center.
  ##
  ## #### Pagination
  ##
  ## This endpoint uses a variant of cursor pagination with some differences from cursor pagination used in other endpoints.
  ##
  ## The records are ordered sequentially by the created_at timestamp, then by id within duplicate timestamp values.
  ## The first page will return the most recent audits.
  ##
  ## The `cursor` parameter is a non-human-readable argument you can use to move forward or backward in time.
  ##
  ## Each JSON response will contain the following attributes to help you get more results:
  ##
  ## - `after_url` requests more recent results
  ## - `before_url` requests older results
  ## - `after_cursor` is the cursor to build the request yourself
  ## - `before_cursor` is the cursor to build the request yourself.
  ##
  ## The `after_cursor` and `before_cursor` attributes may contain non-URL-safe
  ## characters. Make sure to URL-encode them, or simply use the `after_url` or
  ## `before_url` values to paginate.
  ##
  ## The properties are null if no more records are available.
  ##
  ## You can request a maximum of 1,000 results. The default number of results is 1,000, but you can change it with the `limit` parameter.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Stability
  ##
  ## * Development
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/ticket_audits.json?limit=1000 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "before_url": "https://subdomain.zendesk.com/api/v2/ticket_audits.json?cursor=fDE1MDE1NzUxMjIuMHx8MTM0NzM0MzAxMQ%3D%3D&limit=1000",
  ##   "after_url": "https://subdomain.zendesk.com/api/v2/ticket_audits.json?cursor=MTUwMTYwNzUyMi4wfHwxMzQ3NTMxNjcxfA%3D%3D&limit=1000",
  ##   "before_cursor": "fDE1MDE1NzUxMjIuMHx8MTM0NzM0MzAxMQ==",
  ##   "after_cursor": "MTUwMTYwNzUyMi4wfHwxMzQ3NTMxNjcxfA==",
  ##   "audits": [{
  ##     "created_at": "2011-09-25T22:35:44Z",
  ##     "id": 2127301143,
  ##     "ticket_id": 123,
  ##     ...
  ##   }, {
  ##     "created_at": "2011-09-25T22:35:44Z",
  ##     "id": 2127301144,
  ##     "ticket_id": 123,
  ##     ...
  ##   }]
  ## }
  ## ```
  allow_parameters :index, {}
  allow_cursor_pagination_parameters :index
  allow_cursor_pagination_v2_parameters :index
  def index
    render json: presenter.present(ticket_audits)
  rescue ArgumentError, TypeError
    render status: :bad_request, json: { error: I18n.t('txt.admin.controllers.ticket_audits_controller.invalid_cursor_value') }
  end

  private

  def presenter
    @presenter ||= Api::V2::Tickets::AuditPresenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
  end

  def ticket_audits
    @ticket_audits ||= begin
      scope = current_account.ticket_audits.filtered

      if with_cursor_pagination_v2? && current_account.has_cursor_pagination_v2_ticket_audits_endpoint?
        # Force the same index we've been using for CBPv1, but we also need to add reordering since currently the
        # gem defaults to add the id column in asc order
        if current_account.has_ticket_audit_force_index?
          paginate_with_cursor(scope.force_index('index_account_id_created_at').reorder(created_at: :desc, id: :desc))
        else
          paginate_with_cursor(scope.use_index('index_account_id_created_at').reorder(created_at: :desc, id: :desc))
        end
      else
        scope = scope.paginate_with_cursor(params)

        if current_account.has_ticket_audit_force_index?
          scope.from("events force index (index_account_id_created_at)")
        else
          scope.from("events use index (index_account_id_created_at)")
        end
      end
    end
  end

  def statsd
    @statsd ||= Zendesk::StatsD::Client.new(namespace: 'ticket_audit')
  end
end
