class Api::V2::Rules::TriggerRevisionsController < Api::V2::BaseController
  before_action :require_feature

  ## ### List Trigger Revisions
  ## `GET /api/v2/triggers/{trigger_id}/revisions.json`
  ##
  ## List the revisions associated with a trigger.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name  | Will sideload
  ## | ----- | -------------
  ## | users | The user that authored each revision
  ##
  ## #### Pagination
  ##
  ## This endpoint uses cursor-based pagination. The records are ordered in
  ## descending order by the `created_at` timestamp, then by `id` on duplicate
  ## `created_at` values.
  ##
  ## The `cursor` parameter is a non-human-readable argument you can use to move
  ## forward or backward in time.
  ##
  ## Each JSON response will contain the following attributes to help you get
  ## more results:
  ##
  ## - `after_url` requests more recent results
  ## - `before_url` requests older results
  ## - `after_cursor` is the cursor to build the request yourself
  ## - `before_cursor` is the cursor to build the request yourself
  ##
  ## The properties are `null` if no more records are available.
  ##
  ## You can request a maximum of 1000 records using the `limit` parameter. If
  ## no `limit` parameter is supplied, it will default to 1,000.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/{trigger_id}/revisions.json?limit=20 \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "trigger_revisions": [
  ##     {
  ##       "id": 100,
  ##       "url": "https://{subdomain}.zendesk.com/api/v2/trigger/123/revisions/100.json",
  ##       "author_id": 2,
  ##       "snapshot": {
  ##         "title": "Notify requester of comment update",
  ##         "active": true,
  ##         "conditions": { ... },
  ##         "actions": [ ... ],
  ##         "description": "Notifies requester that a comment was updated"
  ##       },
  ##       "diff": {
  ##         "source_id": 1,
  ##         "target_id": 2,
  ##         "active": [ ... ],
  ##         "title": [ ... ],
  ##         "description": [ ... ],
  ##         "conditions": { ... },
  ##         "actions": [ ... ]
  ##       },
  ##       "created_at": "2016-08-15T16:04:06Z"
  ##     },
  ##     ...
  ##   ],
  ##   count: 3,
  ##   "before_url": "https://{subdomain}.zendesk.com/api/v2/triggers/{trigger_id}/revisions.json?cursor=fDE1MDE1NzUxMjIuMHx8MTM0NzM0MzAxMQ%3D%3D&limit=20",
  ##   "after_url": "https://{subdomain}.zendesk.com/api/v2/triggers/{trigger_id}/revisions.json?cursor=MTUwMTYwNzUyMi4wfHwxMzQ3NTMxNjcxfA%3D%3D&limit=20",
  ##   "before_cursor": "fDE1MDE1NzUxMjIuMHx8MTM0NzM0MzAxMQ==",
  ##   "after_cursor": "MTUwMTYwNzUyMi4wfHwxMzQ3NTMxNjcxfA=="
  ## }
  ## ```
  allow_parameters :index, trigger_id: Parameters.integer
  allow_cursor_pagination_parameters :index
  require_oauth_scopes :index, scopes: %i[triggers:read read]
  def index
    render json: presenter.present(revisions)
  end

  ## ### Getting Revisions
  ## `GET /api/v2/triggers/{trigger_id}/revisions/{revision_id}.json`
  ##
  ## Fetch a revision associated with a trigger.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name  | Will sideload
  ## | ----- | -------------
  ## | users | The user that authored each revision
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/{trigger_id}/revisions/{revision_id}.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "trigger_revision": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/trigger/123/revisions/100.json"
  ##     "id": 100,
  ##     "author_id": 2,
  ##     "snapshot": {
  ##       "title": "Notify requester of comment update",
  ##       "active": true,
  ##       "conditions": { ... },
  ##       "actions": [ ... ],
  ##       "description": "Notifies requester that a comment was updated"
  ##     },
  ##     "created_at": "2016-08-15T16:04:06Z"
  ##   }
  ## }
  ## ```
  allow_parameters :show,
    trigger_id: Parameters.integer,
    id:         Parameters.integer
  require_oauth_scopes :show, scopes: %i[triggers:read read]
  def show
    render json: presenter.present(revision)
  end

  private

  def presenter
    @presenter ||= begin
      Api::V2::Rules::TriggerRevisionPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def trigger
    @trigger ||= begin
      Zendesk::RuleSelection::Scope.for_context(
        Zendesk::RuleSelection::Context::Trigger.new(current_user)
      ).find(params[:trigger_id])
    end
  end

  def revisions
    @revisions ||= begin
      trigger.
        revisions.
        paginate_with_cursor(params.merge(id_column: 'nice_id')).
        includes(:snapshot)
    end
  end

  def revision
    @revision ||= trigger.revisions.where(nice_id: params[:id]).first!
  end

  def require_feature
    deny_access unless current_account.has_trigger_revision_history?
  end
end
