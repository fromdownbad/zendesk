class Api::V2::Rules::MacroAttachmentsController < Api::V2::BaseController
  CREATE_PARAMETERS = {
    macro_id:   Parameters.integer,
    attachment: Parameters.anything,
    filename:   Parameters.string
  }.freeze

  include AttachmentsControllerMixin

  ## ### Create Unassociated Macro Attachment
  ##
  ## `POST /api/v2/macros/attachments.json`
  ##
  ## Allows an attachment to be uploaded that can be associated with a macro at
  ## a later time.
  ##
  ## **Note**: To ensure an uploaded attachment is not lost, associate it with a
  ## macro as soon as possible. From time to time, old attachments that are not
  ## not associated with any macro are purged.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/attachments.json \
  ##   -v -u {email}:{password} -F "attachment=@screenshot.jpg" -F "filename=foobar.jpg"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/macros/attachments/100.json
  ##
  ## {
  ##   "macro_attachment": {
  ##     "id": 100,
  ##     "filename": "foobar.jpg",
  ##     "content_type": "image/jpeg",
  ##     "content_url": "https://company.zendesk.com/api/v2/macros/attachments/100/content",
  ##     "size": 2532,
  ##     "created_at": "2016-08-15T16:04:06Z"
  ##   }
  ## }
  ## ```

  ## ### Create Macro Attachment
  ##
  ## `POST /api/v2/macros/{macro_id}/attachments.json`
  ##
  ## Allows an attachment to be uploaded and associated with a macro at the
  ## same time.
  ##
  ## **Note**: A macro can be associated with up to five attachments.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/123/attachments.json \
  ##   -v -u {email}:{password} -F "attachment=@screenshot.jpg" -F "filename=foobar.jpg"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/macros/attachments/100.json
  ##
  ## {
  ##   "macro_attachment": {
  ##     "id": 100,
  ##     "filename": "foobar.jpg",
  ##     "content_type": "image/jpeg",
  ##     "content_url": "https://company.zendesk.com/api/v2/macros/attachments/100/content",
  ##     "size": 2532,
  ##     "created_at": "2016-08-15T16:04:06Z"
  ##   }
  ## }
  ## ```
  allow_parameters :create, CREATE_PARAMETERS

  def create
    return head :bad_request       unless valid_request?
    return head :payload_too_large unless valid_size?

    rule_attachment = RuleAttachment.create do |attachment|
      attachment.account_id       = current_account.id
      attachment.user_id          = current_user.id
      attachment.uploaded_data    = params[:attachment]
      attachment.display_filename = params[:filename]
    end

    rule_attachment.associate(macro) if params.key?(:macro_id) && macro.present?

    render(
      status:   :created,
      json:     attachment_presenter.present(rule_attachment),
      location: attachment_presenter.url(rule_attachment)
    )
  end

  ## ### List Macro Attachments
  ##
  ## `GET /api/v2/macros/{macro_id}/attachments.json`
  ##
  ## Lists the attachments associated with a macro.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/{macro_id}/attachments.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macro_attachments": [
  ##     {
  ##       "id": 100,
  ##       "filename": "foobar.jpg",
  ##       "content_type": "image/jpeg",
  ##       "content_url": "https://company.zendesk.com/api/v2/macros/attachments/100/content",
  ##       "size": 2532,
  ##       "created_at": "2016-08-15T16:04:06Z"
  ##     },
  ##     {
  ##       "id": 342,
  ##       "filename": "bazbat.jpg",
  ##       "content_type": "image/jpeg",
  ##       "content_url": "https://company.zendesk.com/api/v2/macros/attachments/342/content",
  ##       "size": 5028,
  ##       "created_at": "2016-08-16T12:42:25Z"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :index, macro_id: Parameters.integer

  def index
    render(
      json: attachment_presenter.present(
        macro.attachments.each_with_object(
          []
        ) do |attachment, composite_attachments|
          composite_attachments << attachment
        end
      )
    )
  end

  ## ### Show Macro Attachment
  ##
  ## `GET /api/v2/macros/attachments/{id}.json`
  ##
  ## Shows the properties of the specified macro attachment.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/attachments/{id}.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macro_attachment": {
  ##     "id": 100,
  ##     "filename": "foobar.jpg",
  ##     "content_type": "image/jpeg",
  ##     "content_url": "https://company.zendesk.com/api/v2/macros/attachments/100/content",
  ##     "size": 2532,
  ##     "created_at": "2016-08-15T16:04:06Z"
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.integer

  def show
    return head :not_found unless rule_attachment?

    render json: attachment_presenter.present(rule_attachment)
  end

  allow_parameters :content, id: Parameters.integer
  require_oauth_scopes :content, scopes: %i[read], arturo: true

  def content
    return head :not_found unless rule_attachment?

    if via_nginx?
      response.headers['X-Accel-Redirect'] = s3_redirect_path
      response.headers['X-S3-Storage-Url'] =
        rule_attachment.authenticated_s3_url

      if current_account.has_set_macro_attachment_content_disposition?
        set_content(rule_attachment, determine_disposition(rule_attachment))
      end

      head :ok, content_type: response.headers["Content-Type"]
    else
      send_attachment_content(rule_attachment)
    end
  end

  private

  def valid_request?
    valid_params? && association_allowed?
  end

  def valid_params?
    %i[attachment filename].all? { |param| params[param].present? }
  end

  def association_allowed?
    return true unless params.key?(:macro_id) && macro.present?

    macro.definition.has_rich_comment_action? &&
      macro.attachments.count < RuleAttachment.limit
  end

  def valid_size?
    params[:attachment].size <= current_account.max_attachment_size
  end

  def macro
    @macro ||= begin
      Zendesk::RuleSelection::Scope.for_context(
        Zendesk::RuleSelection::Context::Macro.new(current_user)
      ).find(params[:macro_id])
    end
  end

  def rule_attachment
    @rule_attachment ||=
      RuleAttachment.find_by(account_id: current_account.id, id: params[:id])
  end

  def attachment_presenter
    @attachment_presenter ||= begin
      Api::V2::Rules::MacroAttachmentPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def s3_redirect_path
    "/s3/#{rule_attachment.full_filename}"
  end

  def rule_attachment?
    rule_attachment.present?
  end
end
