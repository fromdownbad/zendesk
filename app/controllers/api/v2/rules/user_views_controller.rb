require 'zendesk/rules/user_view_initializer'

class Api::V2::Rules::UserViewsController < Api::V2::BaseController
  rescue_from(
    Zendesk::Rules::UserViewInitializer::InvalidArgument
  ) do |exception|
    render json: {error: exception.message}, status: :unprocessable_entity
  end

  rescue_from Prop::RateLimited do |e|
    Rails.logger.warn("API2 Rate Limited: #{e.message}")

    response.headers['Retry-After'] = e.retry_after.to_s

    render(
      text:         I18n.t('txt.api.v2.user_views.errors.rate_limit'),
      status:       429,
      content_type: Mime[:text].to_s
    )
  end

  allow_subsystem_user :zendesk_nps

  before_action :require_user_views_accessible
  before_action :require_admin_access, only: :export
  before_action :authorize_preview, only: :preview
  before_action :authorize_access_to_view, only: %i[show execute export destroy]
  before_action :authorize_existing_view_management, only: %i[update destroy]
  before_action :authorize_new_view_management, only: :create
  before_action :require_active_view, only: :execute
  before_action :throttle_view, only: :execute
  before_action :require_assumed_user, only: :index
  before_action :verify_csrf_token, only: :export, if: :logged_via_session?

  condition_parameter = {
    field:    Parameters.string,
    operator: Parameters.string,
    value:    Parameters.anything
  }.freeze

  execution_parameters = Parameters.map(
    columns: Parameters.anything,
    group:   Parameters.anything,
    sort:    Parameters.anything
  ) | Parameters.nil_string

  user_view_parameters = {
    title:       Parameters.string,
    active:      Parameters.boolean,
    restriction: Parameters.anything,
    all:         [condition_parameter],
    any:         [condition_parameter],
    execution:   execution_parameters
  }.freeze

  ## ### List User Views
  ## `GET /api/v2/user_views.json`
  ##
  ## Lists shared and personal User Views for the current user.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_views.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##  "user_views": [
  ##    {
  ##      "url": "http://{subdomain}.zendesk.com/api/v2/user_views/682.json",
  ##      "id": 682,
  ##      "title": "Users created within the last 24 hours",
  ##      "raw_title": "{{dc.last_day_users}}",
  ##      "active": true,
  ##      "updated_at": "2013-02-11T22:27:25Z",
  ##      "created_at": "2013-02-07T23:33:42Z",
  ##      "execution": {
  ##        "group": {
  ##          "id": "organization",
  ##          "order": "asc"
  ##        },
  ##        "sort": {
  ##          "id": "asc",
  ##          "order": null
  ##        },
  ##        "columns": {
  ##          "fields": [
  ##            {
  ##              "id": "id",
  ##              "title": "Id"
  ##            },
  ##            {
  ##              "id": "title",
  ##              "title": "Title"
  ##            },
  ##            {
  ##              "id": "organization",
  ##              "title": "Organization"
  ##            },
  ##            {
  ##              "id": "created",
  ##              "title": "Created"
  ##            }
  ##          ]
  ##        }
  ##      },
  ##      "conditions": {
  ##        "all": [
  ##          {
  ##            "source": "created_at",
  ##            "operator": "within_last_n_days",
  ##            "value": "7"
  ##          }
  ##        ],
  ##        "any": [
  ##        ]
  ##      },
  ##      "restriction": {
  ##        "type": "User",
  ##        "id": 2
  ##      }
  ##    }
  ##  ]
  ## }
  ## ```
  allow_parameters :index, user_id: Parameters.integer
  def index
    render json: presenter.present(views)
  end

  ## ### Get User View
  ## `GET /api/v2/user_views/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_views/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "user_view": {
  ##     "url": "http://{subdomain}.zendesk.com/api/v2/user_views/682.json",
  ##     "id": 682,
  ##     "title": "A view",
  ##     "raw_title": "{{dc.basic_view}}",
  ##     "active": true,
  ##     "updated_at": "2013-02-11T22:27:25Z",
  ##     "created_at": "2013-02-07T23:33:42Z",
  ##     "execution": {
  ##       "group": {
  ##         "id": "organization",
  ##         "order": "asc"
  ##       },
  ##       "sort": {
  ##         "id": "created",
  ##         "order": "asc"
  ##       },
  ##       "columns": {
  ##         "fields": [
  ##           {
  ##             "id": "name",
  ##             "title": "Name"
  ##           },
  ##           {
  ##             "id": "organization",
  ##             "title": "Organization"
  ##           },
  ##           {
  ##             "id": "created",
  ##             "title": "Created"
  ##           }
  ##         ]
  ##       }
  ##     },
  ##     "conditions": {
  ##       "all": [
  ##         {
  ##           "source": "created",
  ##           "operator": "is_within_last_n_days",
  ##           "value": "7"
  ##         }
  ##       ],
  ##       "any": [
  ##       ]
  ##     },
  ##     "restriction": {
  ##       "type": "User",
  ##       "id": 2
  ##     }
  ##   }
  ## }
  ## ```
  resource_action :show

  ## ### Preview User View
  ## `POST /api/v2/user_views/preview.json`
  ##
  ## User Views can be previewed by constructing the conditions in the proper format and nesting them under the 'user_view' key.
  ## The output can also be controlled by passing in any of the following parameters and nesting them under the 'output' key.
  ##
  ## | Name            | Type    | Comment
  ## | --------------- | ------- | -------
  ## | columns         | Array   | The user fields to display. System fields are looked up by name, custom fields by title or id.
  ## | group_by        | String  | When present, the field by which the users are grouped.
  ## | group_order     | String  | The direction the users are grouped. May be one of 'asc' or 'desc'.
  ## | sort_order      | String  | The direction the users are sorted. May be one of 'asc' or 'desc'.
  ## | sort_by         | String  | The user field used for sorting. This will either be a title or a custom field id.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_views/preview.json \
  ##   -X POST -H "Content-Type: application/json" \
  ##   -d '{"user_view": {"all": [{"operator": "is", "value": "Gold", "field": "plan_type"}], \
  ##    "output": {"columns": ["id", "name", "created_at"]}}}' \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "rows": [
  ##     {
  ##       "id": 2,
  ##       "name": "",
  ##       "created_at": "2013-01-09T01:50:03Z"
  ##       ...
  ##     },
  ##     ...
  ##   ],
  ##   "next_page": null,
  ##   "previous_page": null,
  ##   "count": 2
  ## }
  ## ```
  allow_parameters :preview, user_view: {
    all:       [condition_parameter],
    any:       [condition_parameter],
    execution: execution_parameters
  }
  require_oauth_scopes :preview, scopes: [:read], arturo: true
  def preview
    @view       = user_view
    @view.title = 'Preview'

    raise ActiveRecord::RecordInvalid, @view unless @view.valid?

    render json: rows_presenter.present(users)
  end

  ## ### Execute User View
  ## `GET /api/v2/user_views/{id}/execute.json`
  ##
  ## Executing a user view returns users that match the user view conditions and returns results
  ## according to the user view's group and sort settings.
  ##
  ## Sort order can be controlled by passing the sort_by and sort_order parameters.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/user_views/{id}/execute.json -v -u {email_address}:{password}
  ## ```
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "rows": [
  ##     {
  ##       "url": "...",
  ##       "id": 2,
  ##       "name": "",
  ##       "created_at": "2013-01-09T01:50:03Z"
  ##       ...
  ##     },
  ##     ...
  ##   ],
  ##   "next_page": null,
  ##   "previous_page": null,
  ##   "count": 2
  ## }
  ## ```
  allow_parameters :execute, id: Parameters.bigid, sampling: Parameters.boolean
  require_oauth_scopes :execute, scopes: [:read], arturo: true
  def execute
    includes << :user_view

    return raise ActiveRecord::RecordInvalid, user_view unless user_view.valid?

    # TODO: If we plan to eager-load column data, we should pass a new object
    # that encapsulates ExecutionResults to the presenter instead of users.
    render json: rows_presenter.present(users)
  end

  ## ### Create User View
  ## `POST /api/v2/user_views.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u admin@zendesk.com:123456 http://support.localhost:3000/api/v2/user_views.json \
  ## -X POST -H "Content-Type: application/json" \
  ## -d '{ "user_view": {
  ##         "title": "Overseas gold members",
  ##         "all": [
  ##           { "field": "name", "operator": "is", "value": "abcd" },
  ##           { "field": "created", "operator": "less_than", "value": "09/08/2013" }
  ##         ],
  ##         "any": [
  ##           { "field": "custom_fields.plan_type", "operator": "is", "value": "gold" },
  ##           { "field": "locale_id", "operator": "is_not", "value": "en-us" }
  ##         ],
  ##         "restriction": { "type": "Group", "id": 47 }
  ##         "execution": {
  ##           "columns": ["name", "id", "custom_fields.ltv"],
  ##           "group":   { "id": "custom_fields.age_group", "order": "asc"},
  ##           "sort":    { "id": "custom_fields.ltv", "order": "desc" }
  ##         }
  ##      }
  ##   }'
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/user_views/{id}.json
  ##
  ## {
  ##   "user_view": {
  ##     "url": "http://{subdomain}.zendesk.com/api/v2/user_views/21.json",
  ##     "id": 21,
  ##     "title": "Overseas gold members",
  ##     "active": true,
  ##     "updated_at": "2013-02-22T22:42:40Z",
  ##     "created_at": "2013-02-22T22:42:40Z",
  ##     "execution": {
  ##       "group": { "id": "custom_fields.age_group", "order": "asc"},
  ##       "sort": { "id": "custom_fields.ltv", "order": "desc" },
  ##       "columns": {
  ##         "fields": [
  ##           {
  ##             "id": "name",
  ##             "title": "Name"
  ##           },
  ##           {
  ##             "id": "id",
  ##             "title": "Id"
  ##           },
  ##           {
  ##             "id": "custom_fields.plan_type",
  ##             "title": "Plan type"
  ##           }
  ##         ]
  ##       }
  ##     },
  ##     "conditions": {
  ##       "all": [
  ##         { "field": "name", "operator": "is", "value": "abcd" },
  ##         { "field": "created", "operator": "less_than", "value": "09/08/2013" }
  ##       ],
  ##       "any": [
  ##         { "field": "custom_fields.plan_type", "operator": "is", "value": "gold" },
  ##         { "field": "locale_id", "operator": "is_not", "value": "en-us" }
  ##       ]
  ##      },
  ##      "restriction": { "type": "Group", "id": 47 }
  ##   }
  ## }
  ## ```
  allow_parameters :create, user_view: user_view_parameters
  def create
    @view = initializer.user_view.save!

    render(
      json:     presenter.present(initializer.user_view),
      status:   :created,
      location: presenter.url(initializer.user_view)
    )
  end

  ## ### Update User View
  ## `PUT /api/v2/user_views/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##   * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/user_views/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT
  ##   -d '{"user_view": {"title": "Favorite users"} }'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "user_view": {
  ##     "id": 91283,
  ##     "title": "Favorite users",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Request Parameters
  ##
  ## The PUT request takes one parameter, a `user_view` object that lists the values to update. All properties are optional.
  ##
  ## | Name            | Description                                |
  ## | --------        | ------------------------------------------ |
  ## | title           | The title of the user view.                |
  ## | all             | An array of one or more conditions. A user/organization must meet all the conditions to be included in the user view. The PUT request replaces all existing conditions. See [Conditions](#conditions). |
  ## | any             | An array of one or more conditions. A user must meet any of them to be included in the user view. At least one `all` condition must be defined with the `any` conditions. The PUT request replaces all existing `any` conditions. See [Conditions](#conditions). |
  ## | active          | Allowed values are `true` or `false`. Determines if the user view is displayed or not. |
  ## | restriction     | An object that describes who can access the user view. |
  ##
  ## #### Example Request
  ##
  ## ```js
  ## "user_view": {
  ##   "title": "Users with subscritpion expiration after January 1, 2013",
  ##   "all": [{ "field": "custom_fields.subscription_end", "operator": "greater_than", "value": "2013-01-01" }]
  ## }
  ## ```
  ##
  ## #### Note
  ##
  ## Updating a condition updates the containing array, clearing the other conditions. Include all your conditions when updating any condition.
  allow_parameters :update,
    id:        Parameters.bigid,
    user_view: user_view_parameters
  def update
    @view = user_view.save!

    render json: presenter.present(user_view)
  end

  ## ### Delete User View
  ## `DELETE /api/v2/user_views/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/user_views/{id}.json \
  ##   -H "Content-Type: application/json" -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    initializer.user_view.soft_delete(validate: false)

    default_delete_response
  end

  ## ### Reorder User Views
  ## `PUT /api/v2/user_views/reorder.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/user_views/reorder.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{"user_view_ids": [12, 55]}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :reorder, user_view_ids: [Parameters.integer]
  require_oauth_scopes :reorder, scopes: [:write], arturo: true
  def reorder
    return head :forbidden if params[:user_view_ids].blank?
    return head :forbidden if has_mixed_user_view_ids?

    if !current_user.is_admin? && has_shared_user_view_ids?
      return head :forbidden
    end

    current_account.user_views.update_position(user_view_ids)

    head :ok
  end

  allow_parameters :export, :skip # TODO: make stricter
  require_oauth_scopes :export, scopes: [:read], arturo: true
  def export
    render(
      json: {
        export: {user_view_id: user_view.id, status: exported_csv_status}
      }
    )
  end

  allow_parameters :definitions, {}
  require_oauth_scopes :definitions, scopes: [:read], arturo: true
  def definitions
    render(
      json: {definitions: UserView.definitions(current_account, current_user)}
    )
  end

  private

  def max_per_page
    subsystem_nps_request? ? 1_000 : super
  end

  def model_name
    :user_view
  end

  def presenter
    @presenter ||=
      Api::V2::Rules::UserViewPresenter.new(current_user, url_builder: self)
  end

  def rows_presenter
    @rows_presenter ||=
      Api::V2::Rules::UserViewRowsPresenter.new(
        current_user,
        url_builder:    self,
        user_view:      user_view,
        subsystem_name: current_subsystem_name
      )
  end

  def aggregate_user_views
    user_views =
      current_account.
        user_views.
        and_disjunction_of(account_scope, group_scope, personal_scope)

    user_views.order('IF(rules.owner_type = "User", 1, 0), position, title asc')
  end

  def views
    @views ||= aggregate_user_views
  end

  def users
    @users ||=
      Zendesk::UserViews::Executer.users_for_view(
        user_view,
        current_user,
        execution_options
      )
  end

  def execution_options
    pagination.merge(
      sort_by:        params[:sort_by],
      sort_order:     params[:sort_order],
      subsystem_name: current_subsystem_name,
      sampling:       subsystem_nps_request? ? false : params[:sampling]
    )
  end

  def user_view
    @user_view ||= initializer.user_view
  end

  def find_view
    @existing_view ||= current_account.user_views.find(params[:id])
  end

  def initializer
    @initializer ||=
      Zendesk::Rules::UserViewInitializer.new(
        current_account,
        params,
        model_name
      )
  end

  def exported_csv_status
    UserViewCsvJob.enqueue(current_account.id, current_user.id, user_view.id)

    :enqueued
  rescue Resque::ThrottledError
    :throttled
  end

  def require_active_view
    return true if find_view.is_active

    render_failure(
      status:  :forbidden,
      title:   'Forbidden',
      message: I18n.t(
        'txt.api.v2.user_views.errors.executed_inactive_filter',
        view: find_view.title
      )
    )
  end

  def authorize_access_to_view
    deny_access unless Rule.find_for_user(current_user, find_view.id)
  end

  def authorize_new_view_management
    authorize_view_management(user_view)
  end

  def authorize_existing_view_management
    authorize_view_management(find_view)
  end

  def authorize_view_management(view)
    if view.owner_type == 'Group' && current_user.can?(:manage_group, UserView)
      return true
    end

    if view.shared?
      deny_access unless current_user.can?(:manage_shared, UserView)
    else
      deny_access unless current_user.can?(:manage_personal, UserView)
    end
  end

  def authorize_preview
    deny_access unless current_user.can?(:manage_personal, UserView)
  end

  def require_user_views_accessible
    deny_access unless user_views_accessible?
  end

  def user_views_accessible?
    current_account.has_user_views? && current_user.can?(:view, UserView)
  end

  def throttle_view
    # Even trusted requests can be throttled by the query killer, in that case,
    # even if is a trusted request, we want Prop to raise
    return if trusted_request? && !Prop.throttled?(*execute_throttle_args)

    Prop.throttle!(*execute_throttle_args)
  end

  def account_scope
    scope = UserView.account_owned(current_account.id)

    target_user.can?(:manage_shared, UserView) ? scope : scope.active
  end

  def group_scope
    if target_user.is_admin?
      UserView.owned_by(Group)
    else
      in_group_scope = UserView.in_group(target_user.group_ids)

      can_access_group_user_views =
        target_user.can?(:manage_shared, UserView) ||
          target_user.can?(:manage_group, UserView)

      can_access_group_user_views ? in_group_scope : in_group_scope.active
    end
  end

  def personal_scope
    UserView.where('(owner_type = ? AND owner_id = ?)', 'User', target_user.id)
  end

  def target_user
    @target_user ||= user_assumed_by_system_user || current_user
  end

  def user_assumed_by_system_user
    return unless current_user.is_system_user?

    @user_assumed_by_system_user ||= User.find_by_id(params[:user_id])
  end

  def require_assumed_user
    return unless current_user.is_system_user?

    deny_access unless user_assumed_by_system_user
  end

  def require_admin_access
    deny_access unless current_user.can?(:manage_shared, UserView)
  end

  def has_mixed_user_view_ids? # rubocop:disable Naming/PredicateName
    has_shared_user_view_ids? && has_personal_user_view_ids?
  end

  def has_shared_user_view_ids? # rubocop:disable Naming/PredicateName
    (user_view_ids & shared_ids).any?
  end

  def has_personal_user_view_ids? # rubocop:disable Naming/PredicateName
    (user_view_ids & personal_ids).any?
  end

  def user_view_ids
    @user_view_ids ||= params['user_view_ids'].map(&:to_i)
  end

  def shared_ids
    @shared_ids ||=
      current_account.user_views.and_disjunction_of(account_scope, group_scope).pluck(:id)
  end

  def personal_ids
    @personal_ids ||= personal_scope.pluck(:id)
  end

  # If request is via :signed_warden, current user will be the system user
  # itself or the system user on behalf of another user.
  def trusted_request?
    request.headers['X-Zendesk-Lotus-Version'] ||
      current_user.is_system_user? ||
      signed_as_system_user?
  end

  def signed_as_system_user?
    auth_via?(:signed_warden)
  end

  def subsystem_nps_request?
    current_user.is_system_user? && current_subsystem_name == 'zendesk_nps'
  end

  def verify_csrf_token
    csrf_token_match =
      request.headers['HTTP_X_CSRF_TOKEN'] &&
        request.headers['HTTP_X_CSRF_TOKEN'] == form_authenticity_token

    return if csrf_token_match

    Rails.logger.warn(
      "CSRF verification failed: '#{request.headers['HTTP_X_CSRF_TOKEN']}'" \
        "=='#{form_authenticity_token}'"
    )

    raise ActionController::InvalidAuthenticityToken
  end

  def logged_via_session?
    authentication.stored?
  end

  def execute_throttle_args
    if current_account.has_ocp_user_views_new_limit?
      [:execute_user_view_2, [current_account.id, user_view.id]]
    else
      [:execute_user_view, [current_account.id, user_view.id]]
    end
  end
end
