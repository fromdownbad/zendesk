class Api::V2::Rules::MacrosController < Api::V2::Rules::BaseController
  MACRO_PARAMETERS = {
    title:       Parameters.string,
    active:      Parameters.boolean,
    restriction: {
      type: Parameters.string,
      id:   Parameters.integer,
      ids:  Parameters.array(Parameters.integer)
    }.freeze,
    actions:     [
      {field: Parameters.string, value: Parameters.anything}.freeze
    ].freeze,
    attachments: Parameters.array(Parameters.bigid),
    description: Parameters.string
  }.freeze

  NEW_PARAMETERS = {
    macro_id:  Parameters.integer,
    ticket_id: Parameters.integer
  }.freeze

  APPLY_PARAMETERS = {
    ticket_id: Parameters.bigid,
    id:        Parameters.bigid,
    ticket:    Parameters.anything
  }.freeze

  ACTIVE_PARAMETERS = {
    access:     Parameters.string,
    category:   Parameters.string,
    group_id:   Parameters.bigid,
    sort_by:    Parameters.string,
    sort_order: Parameters.string
  }.freeze

  INDEX_PARAMETERS = ACTIVE_PARAMETERS.merge(
    active:        Parameters.boolean,
    only_viewable: Parameters.boolean
  ).freeze

  SEARCH_PARAMETERS = {
    access:        Parameters.string,
    active:        Parameters.boolean,
    category:      Parameters.string,
    group_id:      Parameters.bigid,
    only_viewable: Parameters.boolean,
    query:         Parameters.string,
    sort_by:       Parameters.string,
    sort_order:    Parameters.string
  }.freeze

  BATCH_PARAMETERS = {
    id:       Parameters.integer,
    active:   Parameters.boolean,
    position: Parameters.integer
  }.freeze

  USAGE_SIDELOADS = %i[usage_1h usage_24h usage_7d usage_30d].freeze

  require_that_user_can! :edit, :rule, only: %i[create update]
  require_that_user_can! :edit, :initial_macro, only: %i[update destroy]
  require_that_user_can! :edit, :bulk_macros, only: %i[update_many destroy_many]
  require_that_user_can! :view, Group, only: :groups

  deprecate_action :actions, with_feature: :sunset_macro_actions_endpoint

  whitelisted_params_for_page_url %w[access category only_viewable]

  ## ### List Macros
  ## `GET /api/v2/macros.json`
  ##
  ## Lists all shared and personal macros available to the current user.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name          | Type    | Comment
  ## | ------------- | ------- | -------
  ## | access        | string  | Only macros with given access. Possible values are `personal`, `shared`, or `account`
  ## | active        | boolean | Only active macros if `true`, inactive macros if `false`
  ## | category      | string  | Only macros within given category
  ## | group_id      | integer | Only macros belonging to given group
  ## | only_viewable | boolean | Only macros that can be applied to tickets if `true`, All macros the current user can manage if `false`. Defaults to `false`
  ## | sort_by       | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, `usage_1h`, `usage_24h`, `usage_7d`, or `usage_30d`. Defaults to `alphabetical`
  ## | sort_order    | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported. The usage sideloads are only supported on the Professional and Enterprise plans.
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each macro, if present
  ## | categories       | The macro categories
  ## | permissions      | The permissions for each macro
  ## | usage_1h         | The number of times each macro has been used in the past hour
  ## | usage_24h        | The number of times each macro has been used in the past day
  ## | usage_7d         | The number of times each macro has been used in the past week
  ## | usage_30d        | The number of times each macro has been used in the past thirty days
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macros": [
  ##     {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "active": true,
  ##       "position": 42,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Sets the ticket status to `solved`"
  ##     },
  ##     {
  ##       "id": 26,
  ##       "title": "Assign priority tag",
  ##       "raw_title": "{{dc.assign_priority_tag}}",
  ##       "active": false,
  ##       "position": 42,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Adds a `priority` tag to the ticket"
  ##     }
  ##   ],
  ##   "count": 2,
  ##   "previous_page": null,
  ##   "next_page": null
  ## }
  ## ```
  allow_parameters :index, INDEX_PARAMETERS
  require_oauth_scopes :index, scopes: %i[macros:read read]
  def index
    return unless stale_collection?(macros + [current_user])

    render json: presenter.present(macros)
  end

  ## ### Show Macro
  ## `GET /api/v2/macros/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/{id}.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macro": {
  ##     "id": 25,
  ##     "title": "Close and Save",
  ##     "raw_title": "Close and Save",
  ##     "active": true,
  ##     "position": 42,
  ##     "actions": [ {...}, ... ],
  ##     "restriction": { ... },
  ##     "description": "Sets the ticket status to `solved`"
  ##   }
  ## }
  ## ```
  require_oauth_scopes :show, scopes: %i[macros:read read]
  resource_action :show

  ## ### List Active Macros
  ## `GET /api/v2/macros/active.json`
  ##
  ## Lists all active shared and personal macros available to the current user.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | access     | string  | Only macros with given access. Possible values are `personal`, `shared`, or `account`
  ## | category   | string  | Only macros within given category
  ## | group_id   | integer | Only macros belonging to given group
  ## | sort_by    | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, `usage_1h`, `usage_24h`, or `usage_7d`. Defaults to `alphabetical`
  ## | sort_order | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported. The `usage` sideloads are only supported on the Enterprise plan.
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each macro, if present
  ## | categories       | The macro categories
  ## | permissions      | The permissions for each macro
  ## | usage_1h         | The number of times each macro has been used in the past hour
  ## | usage_24h        | The number of times each macro has been used in the past day
  ## | usage_7d         | The number of times each macro has been used in the past week
  ## | usage_30d        | The number of times each macro has been used in the past thirty days
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/active.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macros": [
  ##     {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "active": true,
  ##       "position": 42,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Sets the ticket status to `solved`"
  ##     },
  ##     {
  ##       "id": 28,
  ##       "title": "Close and redirect to topics",
  ##       "raw_title": "{{dc.close_and_redirect}}",
  ##       "active": true,
  ##       "position": 42,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Solves and marks the ticket as a known issue"
  ##     }
  ##   ],
  ##   "count": 2,
  ##   "previous_page": null,
  ##   "next_page": null
  ## }
  ## ```
  allow_parameters :active, ACTIVE_PARAMETERS
  require_oauth_scopes :active, scopes: %i[macros:read read write]
  def active
    params[:active] = true

    render json: presenter.present(macros)
  end

  ## ### Create Macro
  ## `POST /api/v2/macros.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/macros.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '{"macro": {"title": "Roger Wilco II", "actions": [{"field": "status", "value": "solved"}]}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: /api/v2/macros/{new-macro-id}.json
  ##
  ## {
  ##   "macro": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ## #### Request Parameters
  ##
  ## The POST request takes one parameter, a `macro` object that lists the values to set when the macro is created.
  ##
  ## | Name        | Description
  ## | ----------- | -----------
  ## | actions     | Required. An object describing what the macro will do
  ## | active      | Allowed values are `true` or `false`. Determines if the macro is displayed or not
  ## | attachments | An array of macro attachment IDs to be associated with the macro
  ## | description | The description of the macro
  ## | restriction | An object that describes who can access the macro. To give all agents access to the macro, omit this property
  ## | title       | Required. The title of the macro
  ##
  ## The `restriction` object has the following properties.
  ##
  ## | Name | Comment
  ## | ---- | -------
  ## | type | Allowed values are `Group` or `User`
  ## | id   | The numeric ID of the group or user
  ## | ids  | The numeric IDs of the groups
  allow_parameters :create, macro: MACRO_PARAMETERS
  require_oauth_scopes :create, scopes: %i[macros:write write]
  def create
    return head :bad_request unless params[:macro].present?

    with_attachments -> { rule.save! } do
      render(
        json:     presenter.present(rule),
        status:   :created,
        location: presenter.url(rule)
      )
    end
  end

  ## ### Update Macro
  ## `PUT /api/v2/macros/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/macros/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT -d '{"macro": {"title": "Roger Wilco II"}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macro": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco II",
  ##     ...
  ##   }
  ## }
  ## ```
  ## #### Note
  ##
  ## Updating an action updates the containing array, clearing the other
  ## actions. Include all your actions when updating any action.
  allow_parameters :update, id: Parameters.bigid, macro: MACRO_PARAMETERS
  require_oauth_scopes :update, scopes: %i[macros:write write]
  def update
    return head :bad_request unless params[:macro].present?

    with_attachments -> { rule.save!(validate: validate?) } do
      render json: presenter.present(rule)
    end
  end

  ## ### Update Many Macros
  ## `PUT /api/v2/macros/update_many.json`
  ##
  ## Updates the provided macros with the specified changes.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/update_many.json \
  ##   -v -u {email}:{password} -H "Content-Type: application/json" \
  ##   -X PUT -d '{"macros": [{"id": 123, "position": 8}]}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macros": [
  ##     {
  ##       "id": 26,
  ##       "title": "Assign priority tag",
  ##       "raw_title": "{{dc.assign_priority_tag}}",
  ##       "active": false,
  ##       "position": 419,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Adds a `priority` tag to the ticket"
  ##     },
  ##     {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "active": true,
  ##       "position": 42,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Sets the ticket status to `solved`"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ##
  ## #### Request Parameters
  ##
  ## The PUT request expects a `macros` object that lists the triggers to update.
  ##
  ## Each macro may have the following properties:
  ##
  ## | Name     | Mandatory | Description
  ## | -------- | --------- | -----------
  ## | id       | yes       | The ID of the macro to update
  ## | position | no        | The new position of the macro
  ## | active   | no        | The active status of the macro (true or false)
  ##
  ## #### Example request
  ##
  ## ```js
  ##  {
  ##   "macros": [
  ##     {"id": 25, "active": false},
  ##     {"id": 23, "active": false},
  ##     {"id": 27, "active": false},
  ##     {"id": 22, "active": false}
  ##   ]
  ## }
  ## ```
  ##
  ## ```js
  ## {
  ##   "macros": [
  ##     {"id": 25, "position": 3},
  ##     {"id": 23, "position": 5},
  ##     {"id": 27, "position": 9},
  ##     {"id": 22, "position": 7}
  ##   ]
  ## }
  ## ```
  allow_parameters :update_many, macros: [BATCH_PARAMETERS]
  require_oauth_scopes :update_many, scopes: %i[macros:write write read]
  def update_many
    return head :bad_request unless params[:macros].present?

    Macro.transaction do
      positionless_updates.each do |macro_params|
        update_macro(macro_params[:id], active: macro_params[:active])
      end

      if position_updates.any?
        Zendesk::Rules::PositionUpdate.perform(
          Zendesk::RuleSelection::Context::Macro.new(current_user),
          position_updates
        )
      end
    end

    render json: presenter.present(bulk_macros)
  end

  ## ### Delete Macro
  ## `DELETE /api/v2/macros/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions applying on certain actions
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/macros/{id}.json \
  ##   -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  require_oauth_scopes :destroy, scopes: %i[macros:write write]

  ## ### Bulk Delete Macros
  ## `DELETE /api/v2/macros/destroy_many.json`
  ##
  ## Deletes the macros corresponding to the provided comma-separated list of IDs.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/destroy_many.json?ids=1,2,3 \
  ##   -v -u {email}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  ## #### Request Parameters
  ##
  ## The DELETE request takes one parameter, an `ids` object that lists the macros to delete.
  ##
  ## | Name | Description
  ## | ---- | -----------
  ## | ids  | The IDs of the macros to delete
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ids": "25,23,27,22"
  ## }
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids
  require_oauth_scopes :destroy_many, scopes: %i[macros:write write read]
  def destroy_many
    return head :bad_request unless many_ids?

    bulk_macros.each { |macro| macro.soft_delete(validate: false) }

    head :no_content
  end

  ## ### Show Changes to Ticket
  ## `GET /api/v2/macros/{id}/apply.json`
  ##
  ## Returns the changes the macro would make to a ticket. It doesn't actually
  ## change a ticket. You can use the response data in a subsequent API call
  ## to the [Tickets](./tickets) endpoint to update the ticket.
  ##
  ## The response includes only the ticket fields that would be changed by the
  ## macro. To get the full ticket object after the macro is applied,
  ## see [Show Ticket After Changes](#show-ticket-after-changes) below.
  ##
  ## #### Allowed For
  ##
  ##   * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/{id}/apply.json \
  ##   -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "result": {
  ##     "ticket": {
  ##       "assignee_id": 235323,
  ##       "group_id": 98738,
  ##       "fields": [
  ##         {
  ##           "id": 27642,
  ##           "value": "745"
  ##         }
  ##       ],
  ##       ...
  ##     },
  ##     "comment": {
  ##       "body": "Assigned to Agent Uno.",
  ##       "scoped_body": [["channel:all", "Assigned to Agent Uno."]],
  ##       "public": false
  ##     }
  ##   }
  ## }
  ## ```

  ## ### Show Ticket After Changes
  ## `GET /api/v2/tickets/{ticket_id}/macros/{id}/apply.json`
  ##
  ## Returns the full ticket object as it would be after applying the macro to the ticket.
  ## It doesn't actually change the ticket. You can use the response data in a subsequent
  ## API call to the [Tickets](./tickets) endpoint to update the ticket.
  ##
  ## To get only the ticket fields that would be changed by the macro,
  ## see [Show Changes to Ticket](#show-changes-to-ticket).
  ##
  ## #### Allowed For
  ##
  ##   * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/macros/{id}/apply.json \
  ##   -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "result": {
  ##     "ticket": {
  ##       "id": 35436,
  ##       "url": "https://company.zendesk.com/api/v2/tickets/35436.json",
  ##       "assignee_id": 235323,
  ##       "group_id": 98738,
  ##       "fields": [
  ##         {
  ##           "id": 27642,
  ##           "value": "745"
  ##         }
  ##       ],
  ##       ...
  ##     },
  ##     "comment": {
  ##       "body": "Assigned to Agent Uno.",
  ##       "scoped_body": [["channel:all", "Assigned to Agent Uno."]],
  ##       "public": false
  ##     }
  ##   }
  ## }
  ## ```
  allow_parameters :apply, APPLY_PARAMETERS
  require_oauth_scopes :apply, scopes: %i[macros:read read write]
  def apply
    return head :not_found if params[:ticket_id] && ticket.blank?

    ticket.attributes = params[:ticket] if params[:ticket] && ticket.present?

    render json: app_presenter.present(macro_application)
  end

  ## ### List Macro Categories
  ## `GET /api/v2/macros/categories.json`
  ##
  ## Lists all macro categories available to the current user.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/categories.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "categories": [
  ##     "FAQ",
  ##     "Triage",
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :categories, {}
  require_oauth_scopes :categories, scopes: %i[macros:read read write]
  def categories
    render json: {categories: scope.categories}
  end

  ## ### Search Macros
  ## `GET /api/v2/macros/search.json`
  ##
  ## #### Allowed For
  ##
  ## * Agents
  ##
  ## #### Search Parameters
  ##
  ## Required
  ##
  ## | Name  | Type   | Comment
  ## | ----- | ------ | -------
  ## | query | string | Query string used to find macros with matching titles
  ##
  ## Optional
  ##
  ##  You can use any combination of the following optional parameters.
  ##
  ## | Name          | Type    | Comment
  ## | ------------- | ------- | -------
  ## | access        | string  | List macros with the given access. Possible values are `personal`, `shared`, or `account`
  ## | active        | boolean | List active macros if `true`; inactive macros if `false`
  ## | category      | string  | List macros in the given category
  ## | group_id      | integer | List macros belonging to given group
  ## | only_viewable | boolean | List macros that can be applied to tickets if `true`; list all macros the current user can manage if `false`. Default is `false`
  ## | sort_by       | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, and `position`. If unspecified, the macros are sorted by relevance
  ## | sort_order    | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported. The `usage` sideloads are only supported on the Enterprise plan.
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each macro, if present
  ## | permissions      | The permissions for each macro
  ## | usage_1h         | The number of times each macro has been used in the past hour
  ## | usage_24h        | The number of times each macro has been used in the past day
  ## | usage_7d         | The number of times each macro has been used in the past week
  ## | usage_30d        | The number of times each macro has been used in the past thirty days
  ##
  ## #### Using curl
  ##
  ## ```bash
  ##  curl https://{subdomain}.zendesk.com/api/v2/macros/search.json?query=close&active=true
  ##    -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macros": [
  ##     {
  ##       "id": 25,
  ##       "title": "Save and Close",
  ##       "raw_title": "Save and Close",
  ##       "active": true,
  ##       "position": 42,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Sets the ticket status to solved"
  ##     },
  ##     {
  ##       "id": 28,
  ##       "title": "Close and redirect to topics",
  ##       "raw_title": "{{dc.close_and_redirect}}",
  ##       "active": true,
  ##       "position": 42,
  ##       "actions": [ {...}, ... ],
  ##       "restriction": { ... },
  ##       "description": "Solves and marks the ticket as a known issue"
  ##     }
  ##   ],
  ##   "count": 2,
  ##   "previous_page": null,
  ##   "next_page": null
  ## }
  ## ```
  allow_parameters :search, SEARCH_PARAMETERS
  require_oauth_scopes :search, scopes: %i[macros:read read write]
  def search
    return head :bad_request unless params[:query].present?

    render json: search_presenter.present(search_result.rules)
  end

  ## ### List supported actions for macros
  ## `GET /api/v2/macros/actions.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/actions.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "actions": [
  ##     {
  ##       "target": null,
  ##       "title": "Set subject",
  ##       "values": {
  ##         "type": "text",
  ##         "list": []
  ##       },
  ##       "value": "subject",
  ##       "operators": [
  ##         {
  ##           "value": "is",
  ##           "title": "Is"
  ##         }
  ##       ],
  ##       "group": "ticket",
  ##       "title_for_field": "Set subject",
  ##       "output_key": null
  ##     },
  ##     {
  ##       "target": null,
  ##       "title": "Status",
  ##       "values": {
  ##         "type": "list",
  ##         "list": [
  ##           {
  ##             "value": "open",
  ##             "title": "Open",
  ##             "enabled": true
  ##           },
  ##           {
  ##             "value": "pending",
  ##             "title": "Pending",
  ##             "enabled": true
  ##           },
  ##             "value": "solved",
  ##             "title": "Solved",
  ##             "enabled": true
  ##           }
  ##         ]
  ##       },
  ##       "value": "subject",
  ##       "operators": [
  ##         {
  ##           "value": "is",
  ##           "title": "Is"
  ##         }
  ##       ],
  ##       "group": "ticket",
  ##       "title_for_field": "Set subject",
  ##       "output_key": null
  ##     },
  ##     {
  ##       "title": "Priority",
  ##       "values": {
  ##         "type": "list",
  ##         "list": [
  ##           {
  ##             "value": "low",
  ##             "title": "Low",
  ##             "enabled": false
  ##           },
  ##           {
  ##             "value": "normal",
  ##             "title": "Normal",
  ##             "enabled": true
  ##           },
  ##           {
  ##             "value": "high",
  ##             "title": "High",
  ##             "enabled": true
  ##           },
  ##           {
  ##             "value": "urgent",
  ##             "title": "Urgent",
  ##             "enabled": false
  ##           }
  ##         ]
  ##       },
  ##       "value": "priority",
  ##       "operators": [
  ##         {
  ##           "value": "is",
  ##           "title": "Is"
  ##         }
  ##       ],
  ##       "group": "ticket",
  ##       "title_for_field": "Priority",
  ##       "output_key": null,
  ##       "field": "priority"
  ##     }
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :actions, {}
  require_oauth_scopes :actions, scopes: %i[macros:read read write]
  def actions
    render json: {actions: macro_actions}
  end

  ## ### Show macro replica
  ## `GET /api/v2/macros/new.json`
  ##
  ## Returns an unpersisted macro representation derived from a ticket or macro.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You must pass in one of the following parameters:
  ##
  ## | Name      | Type    | Comment
  ## | --------- | ------- | -------
  ## | macro_id  | integer | ID of the macro to replicate
  ## | ticket_id | integer | ID of the ticket from which to build a macro replica
  ##
  ## If both `macro_id` and `ticket_id` are passed, `macro_id` will be used.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/new.json?macro_id=123 \
  ##   -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "macro": {
  ##     "title": "Close and Save",
  ##     "raw_title": "Close and Save",
  ##     "position": 42,
  ##     "actions": [ {...}, ... ],
  ##     "restriction": { ... },
  ##     "description": "Sets the ticket status to `solved`"
  ##   }
  ## }
  ## ```
  allow_parameters :new, NEW_PARAMETERS
  require_oauth_scopes :new, scopes: %i[macros:read read write]
  def new
    return head :bad_request unless macro_replica.present?

    render json: replica_presenter.present(macro_replica)
  end

  # h3 List groups associated with macros
  # `GET /api/v2/macros/groups.json`
  #
  # Lists active groups associated with at least one macro.
  #
  # h4 Allowed For
  #
  #  * Agents
  #
  # h4 Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/macros/groups.json \
  #   -v -u {email}:{password}
  # ```
  #
  # h4 Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "groups": [
  #     {
  #       "name":       "DJs",
  #       "created_at": "2015-05-13T00:07:08Z",
  #       "updated_at": "2016-07-22T00:11:12Z",
  #       "id":         211
  #     },
  #     {
  #       "name":       "MCs",
  #       "created_at": "2015-08-26T00:07:08Z",
  #       "updated_at": "2016-05-13T00:07:08Z",
  #       "id":         122
  #     },
  #     ...
  #   ]
  # }
  # ```
  allow_parameters :groups, {}
  require_oauth_scopes :groups, scopes: %i[macros:read read write]
  def groups
    render json: group_presenter.present(macro_groups)
  end

  ## ### List Macro Action Definitions
  ## `GET /api/v2/macros/definitions.json`
  ##
  ## Returns the definitions of the actions a macro can perform. For example,
  ## one action can set the status of a ticket. The definition of the action
  ## includes a title ("Status"), a type ("list"), and possible values. For a
  ## list of support actions, see [Action reference](./macros#actions-reference).
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/macros/definitions.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "definitions": {
  ##     "actions": [
  ##       {
  ##         "title": "Status",
  ##         "type": "list",
  ##         "subject": "status",
  ##         "group": "ticket",
  ##         "repeatable": false,
  ##         "nullable": false,
  ##         "values": [
  ##           { "value": "1", "title": "Open", "enabled": true },
  ##           { "value": "2", "title": "Pending", "enabled": true },
  ##           { "value": "3", "title": "Solved", "enabled": true },
  ##           { "value": "4", "title": "Closed", "enabled": true }
  ##         ]
  ##       },
  ##       ...
  ##     ]
  ##   }
  ## }
  ## ```
  require_oauth_scopes :definitions, scopes: %i[macros:read read]
  allow_parameters :definitions, {}
  def definitions
    render json: definitions_presenter.present(macro_definitions)
  end

  private

  def initial_macro
    scope.find(params[:id])
  end

  def macros
    @macros ||= scope.paginate(pagination)
  end

  def macro_ids
    @macro_ids ||= many_ids || params[:macros].map { |macro| macro[:id] }
  end

  def bulk_macros
    scope.find(macro_ids)
  end

  def macro_groups
    @macro_groups ||= begin
      paginate(
        current_account.
          groups.
          joins(:groups_macros).
          joins('JOIN rules ON groups_macros.macro_id = rules.id').
          where('rules.deleted_at IS NULL').
          distinct
      )
    end
  end

  def position_updates
    @position_updates ||= begin
      params[:macros].each_with_object([]) do |macro_params, updates|
        next unless macro_params[:position].present?

        updates << macro_params.slice(:id, :position)
      end
    end
  end

  def positionless_updates
    @positionless_updates ||= begin
      params[:macros].each_with_object([]) do |macro_params, updates|
        next unless macro_params.key?(:active)

        updates << macro_params.slice(:id, :active)
      end
    end
  end

  def context
    @context ||= begin
      Zendesk::RuleSelection::Context::Macro.new(
        current_user,
        params.merge(pagination)
      )
    end
  end

  def scope
    @scope ||= begin
      if params[:only_viewable]
        Zendesk::RuleSelection::Scope.viewable(context)
      else
        Zendesk::RuleSelection::Scope.for_context(context)
      end
    end
  end

  def search_result
    @search_result ||= Zendesk::RuleSelection::Search.for_context(context)
  end

  def presenter
    @presenter ||= begin
      Api::V2::Rules::MacroPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def search_presenter
    @search_presenter ||= begin
      Api::V2::Rules::MacroSearchPresenter.new(
        current_user,
        url_builder: self,
        highlights:  search_result.highlights,
        includes:    includes
      )
    end
  end

  def app_presenter
    @app_presenter ||= begin
      Api::V2::Rules::MacroApplicationPresenter.new(
        current_user,
        url_builder: self,
        ticket:      ticket
      )
    end
  end

  def definitions_presenter
    @definitions_presenter ||= begin
      Api::V2::Rules::MacroDefinitionsPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def replica_presenter
    @replica_presenter ||= begin
      Api::V2::Rules::MacroReplicaPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def group_presenter
    @group_presenter ||= begin
      Api::V2::GroupPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id(params[:ticket_id])
  end

  def macro_application
    @macro_application ||= begin
      Zendesk::Rules::Macro::Application.new(
        macro:  rule,
        ticket: ticket,
        user:   current_user
      )
    end
  end

  def macro_definitions
    @macro_definitions ||=
      Zendesk::Rules::Macro::Definitions.new(current_account, current_user)
  end

  def macro_replica
    @macro_replica ||= begin
      if params[:macro_id].present?
        scope.find_by_id(params[:macro_id])
      elsif params[:ticket_id].present? && ticket.present?
        current_account.macros.build(
          definition: Definition.from_ticket(ticket, current_user)
        )
      end
    end
  end

  def macro_actions
    @macro_actions ||= begin
      Zendesk::Rules::Macro::Actions.new(
        current_account,
        current_user
      ).definitions
    end
  end

  def provided_attachment_ids
    @provided_attachment_ids ||= params[:macro][:attachments] || []
  end

  def rule_attachment_rules
    @rule_attachment_rules ||= begin
      RuleAttachmentRule.where(
        account_id: current_account.id,
        rule_id:    rule.id
      )
    end
  end

  def provided_attachments
    @provided_attachments ||= begin
      RuleAttachment.where(
        account_id: current_account.id,
        id:         provided_attachment_ids
      )
    end
  end

  def valid_attachments?
    return true if provided_attachment_ids.none?

    # TODO: WRK-1859
    # Provide better error messaging for individual scenario failures instead
    # of a single 400 Bad Request if any of them are false
    (rule.definition.has_rich_comment_action? ||
      rule.definition.has_side_conversation_action?) &&
      provided_attachment_ids.count <= RuleAttachment.limit &&
      provided_attachment_ids.count == provided_attachments.count
  end

  def with_attachments(macro_action)
    return head :bad_request unless valid_attachments?

    Macro.transaction do
      macro_action.call

      if params[:macro].key?(:attachments)
        attachments_to_keep, attachments_to_delete = begin
          rule_attachment_rules.partition do |entry|
            provided_attachments.include?(entry[:rule_attachment_id])
          end
        end

        attachments_to_delete.each(&:destroy)

        provided_attachments.each do |a|
          a.associate(rule) if attachments_to_keep.exclude?(a.id)
        end
      end
    end

    yield
  end

  def update_macro(macro_id, macro_params)
    Zendesk::Rules::Initializer.new(
      current_account,
      scope,
      {id: macro_id, macro: macro_params},
      :macro
    ).rule.tap do |updated_macro|
      updated_macro.current_user = current_user

      updated_macro.save!(validate: updated_macro.is_active?)
    end
  end

  def includes
    @includes ||= begin
      return super if current_account.has_rule_usage_stats?

      super.reject { |sideload| USAGE_SIDELOADS.include?(sideload) }
    end
  end
end
