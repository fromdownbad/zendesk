class Api::V2::Rules::AutomationsController < Api::V2::Rules::BaseController
  ACTIVE_PARAMETERS = {
    sort_by:    Parameters.string,
    sort_order: Parameters.string
  }.freeze

  INDEX_PARAMETERS = ACTIVE_PARAMETERS.merge(
    active: Parameters.boolean
  ).freeze

  CONDITION_PARAMETERS = {
    field:    Parameters.string,
    operator: Parameters.string,
    value:    Parameters.string
  }.freeze

  AUTOMATION_PARAMETERS = {
    title:       Parameters.string,
    active:      Parameters.boolean,
    position:    Parameters.integer,
    restriction: Parameters.anything,
    conditions:  Parameters.map(
      all:       [CONDITION_PARAMETERS].freeze,
      any:       [CONDITION_PARAMETERS].freeze
    ),
    all:         [CONDITION_PARAMETERS].freeze,
    any:         [CONDITION_PARAMETERS].freeze,
    actions:     [
      {field: Parameters.string, value: Parameters.anything}.freeze
    ].freeze
  }.freeze

  UPDATE_PARAMETERS = {
    id:         Parameters.bigid,
    automation: AUTOMATION_PARAMETERS
  }.freeze

  SEARCH_PARAMETERS = INDEX_PARAMETERS.merge(
    query:      Parameters.string,
    sort_by:    Parameters.string,
    sort_order: Parameters.string
  ).freeze

  BATCH_PARAMETERS = {
    id:       Parameters.integer,
    active:   Parameters.boolean,
    position: Parameters.integer
  }.freeze

  EDIT_ACTIONS = %i[create destroy destroy_many update update_many].freeze

  USAGE_SIDELOADS = %i[usage_1h usage_24h usage_7d usage_30d].freeze

  require_that_user_can! :edit, Automation, only: EDIT_ACTIONS

  ## ### List Automations
  ## `GET /api/v2/automations.json`
  ##
  ## Lists all automations for the current account
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | active     | boolean | Only active automations if `true`, inactive automations if `false`
  ## | sort_by    | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, `usage_1h`, `usage_24h`, or `usage_7d`. Defaults to `position`
  ## | sort_order | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported. The usage sideloads are only supported on the Professional and Enterprise plans.
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each automation, if present
  ## | permissions      | The permissions for each automation
  ## | usage_1h         | The number of times each automation has been used in the past hour
  ## | usage_24h        | The number of times each automation has been used in the past day
  ## | usage_7d         | The number of times each automation has been used in the past week
  ## | usage_30d        | The number of times each automation has been used in the past thirty days
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/automations.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "automations": [
  ##      {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 8,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       ...
  ##     },
  ##     {
  ##       "id": 26,
  ##       "title": "Assign priority tag",
  ##       "raw_title": "{{dc.assign_priority_tag}}",
  ##       "position": 9,
  ##       "active": false,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       ...
  ##     }
  ##   ],
  ##   "count": 2,
  ##   "previous_page": null,
  ##   "next_page": null
  ## }
  ## ```
  allow_parameters :index, INDEX_PARAMETERS
  require_oauth_scopes :index, scopes: %i[automations:read read]
  def index
    render json: presenter.present(automations)
  end

  ## ### Show Automation
  ## `GET /api/v2/automations/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/automations/{id}.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "automation": {
  ##     "id": 25,
  ##     "title": "Tickets updated <12 Hours",
  ##     "raw_title": "{{dc.tickets_updated_last_12_hours}}",
  ##     "position": 8,
  ##     "active": true,
  ##     "conditions": [ ... ],
  ##     "actions": [ ... ],
  ##     ...
  ##   }
  ## }
  ## ```
  require_oauth_scopes :show, scopes: %i[automations:read read]
  resource_action :show

  ## ### List Active Automations
  ## `GET /api/v2/automations/active.json`
  ##
  ## Lists all active automations
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name       | Type   | Comment
  ## | ---------- | ------ | -------
  ## | sort_by    | string | Possible values are `alphabetical`, `created_at`, `updated_at`, `usage_1h`, `usage_24h`, or `usage_7d`. Defaults to `position`
  ## | sort_order | string | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each automation, if present
  ## | permissions      | The permissions for each automation
  ## | usage_1h         | The number of times each automation has been used in the past hour
  ## | usage_24h        | The number of times each automation has been used in the past day
  ## | usage_7d         | The number of times each automation has been used in the past week
  ## | usage_30d        | The number of times each automation has been used in the past thirty days
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/automations/active.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "automations": [
  ##      {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 8,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       ...
  ##     },
  ##     {
  ##       "id": 28,
  ##       "title": "Close and redirect to topics",
  ##       "raw_title": "{{dc.close_and_redirect}}",
  ##       "position": 9,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       ...
  ##     }
  ##   ],
  ##   "count": 2,
  ##   "previous_page": null,
  ##   "next_page": null
  ## }
  ## ```
  allow_parameters :active, ACTIVE_PARAMETERS
  require_oauth_scopes :active, scopes: %i[automations:read read write]
  def active
    params[:active] = true

    render json: presenter.present(automations)
  end

  ## ### Create Automation
  ## `POST /api/v2/automations.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/automations.json \
  ##   -H "Content-Type: application/json" -X POST -d \
  ##   '{"automation": {"title": "Roger Wilco", "all": [{ "field": "status", "operator": "is", "value": "open" }, { "field": "priority", "operator": "less_than", "value": "high" }], "actions": [{ "field": "priority", "value": "high" }]}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: /api/v2/automation/{new-automation-id}.json
  ##
  ## {
  ##   "automation": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ## #### Note
  ##
  ## The request must include the following conditions in the `all` array:
  ##
  ## - At least one time-based condition
  ## - At least one condition that checks one of the following fields: `status`, `type`, `group_id`, `assignee_id`, or `requester_id`.
  allow_parameters :create, automation: AUTOMATION_PARAMETERS
  require_oauth_scopes :create, scopes: %i[automations:write write]

  ## ### Update Automation
  ## `PUT /api/v2/automations/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/automations/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT -d '{"automation": {"title": "Roger Wilco II"}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "automation": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco II",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Note
  ##
  ## Updating a condition or action updates both the `conditions` and `actions`
  ## arrays, clearing all existing values of both arrays. Include all your
  ## conditions and actions when updating any condition or action.
  allow_parameters :update, UPDATE_PARAMETERS
  require_oauth_scopes :update, scopes: %i[automations:write write]

  ## ### Update Many Automations
  ## `PUT /api/v2/automations/update_many.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/automations/update_many.json \
  ##   -H "Content-Type: application/json" -X PUT -d '{"automations": [{"id": 26, "position": 8}, {"id": 25, "position": 15}]}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "automations": [
  ##     {
  ##       "url": "http://{subdomain}.zendesk.com/api/v2/automations/26.json",
  ##       "id": 26,
  ##       "title": "Assign priority tag",
  ##       "raw_title": "{{dc.assign_priority_tag}}",
  ##       "position": 8,
  ##       "active": false,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     {
  ##       "url": "http://{subdomain}.zendesk.com/api/v2/automations/25.json",
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 15,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ##
  ## #### Request Parameters
  ##
  ## The PUT request expects an `automations` object that lists the automations to update.
  ##
  ## Each automation may have the following properties:
  ##
  ## | Name     | Mandatory | Description
  ## | -------- | --------- | -----------
  ## | id       | yes       | The ID of the automation to update
  ## | position | no        | The new position of the automation
  ## | active   | no        | The active status of the automation (true or false)
  ##
  ## #### Example Request
  ##
  ## ```js
  ## {
  ##   "automations": [
  ##     {"id": 25, "position": 3},
  ##     {"id": 23, "position": 5},
  ##     {"id": 27, "position": 9},
  ##     {"id": 22, "position": 7}
  ##   ]
  ## }
  ## ```
  allow_parameters :update_many, automations: [BATCH_PARAMETERS]
  require_oauth_scopes :update_many, scopes: %i[automations:write write]
  def update_many
    return head :bad_request unless params[:automations].present?

    Automation.transaction do
      positionless_updates.each do |automation_params|
        update_automation(
          automation_params[:id],
          active: automation_params[:active]
        )
      end

      if position_updates.any?
        Zendesk::Rules::PositionUpdate.perform(
          Zendesk::RuleSelection::Context::Automation.new(current_user),
          position_updates
        )
      end
    end

    render json: presenter.present(bulk_automations)
  end

  ## ### Delete Automation
  ## `DELETE /api/v2/automations/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/automations/{id}.json \
  ##   -H "Content-Type: application/json" -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  require_oauth_scopes :destroy, scopes: %i[automations:write write]

  ## ### Bulk Delete Automations
  ## `DELETE /api/v2/automations/destroy_many.json`
  ##
  ## Deletes the automations corresponding to the provided comma-separated list
  ## of IDs.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/automations/destroy_many.json?ids=1,2,3 \
  ##   -v -u {email}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  ## #### Request Parameters
  ##
  ## The DELETE request takes one parameter, an `ids` object that lists the
  ## automations to delete.
  ##
  ## | Name | Description
  ## | ---- | -----------
  ## | ids  | The IDs of the automations to delete
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ids": "25,23,27,22"
  ## }
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids
  require_oauth_scopes :destroy_many, scopes: %i[automations:write write read]
  def destroy_many
    return head :bad_request unless many_ids?

    bulk_automations.each do |automation|
      automation.soft_delete(validate: false)
    end

    head :no_content
  end

  ## ### Search Automations
  ## `GET /api/v2/automations/search.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## Required
  ##
  ## | Name  | Type   | Comment
  ## | ----- | ------ | -------
  ## | query | string | Query string used to find all automations with matching title
  ##
  ## Optional
  ##
  ##  You can use any combination of the following optional parameters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | active     | boolean | Only active automations if `true`, inactive automations if `false`
  ## | sort_by    | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, and `position`. If unspecified, the automations are sorted by relevance
  ## | sort_order | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each automation, if present
  ## | permissions      | The permissions for each automation
  ## | usage_1h         | The number of times each automation has been used in the past hour
  ## | usage_24h        | The number of times each automation has been used in the past day
  ## | usage_7d         | The number of times each automation has been used in the past week
  ## | usage_30d        | The number of times each automation has been used in the past thirty days
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/automations/search.json?query=close \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "automations": [
  ##     {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 9,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     {
  ##       "id": 28,
  ##       "title": "Close and redirect to topics",
  ##       "raw_title": "{{dc.close_and_redirect}}",
  ##       "position": 9,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     }
  ##   ],
  ##   "previous_page": null,
  ##   "next_page": null,
  ##   "count": 2
  ## }
  ## ```
  allow_parameters :search, SEARCH_PARAMETERS
  require_oauth_scopes :search, scopes: %i[automations:read read]
  def search
    return head :bad_request unless params[:query].present?

    render json: search_presenter.present(search_result.rules)
  end

  protected

  def automations
    @automations ||= scope.paginate(pagination)
  end

  def scope
    @scope ||= Zendesk::RuleSelection::Scope.for_context(context)
  end

  def search_result
    @search_result ||= Zendesk::RuleSelection::Search.for_context(context)
  end

  def automation_ids
    @automation_ids ||=
      many_ids || params[:automations].map { |automation| automation[:id] }
  end

  def bulk_automations
    scope.find(automation_ids)
  end

  def presenter
    @presenter ||= begin
      Api::V2::Rules::AutomationPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  private

  def search_presenter
    @search_presenter ||= begin
      Api::V2::Rules::AutomationPresenter.new(
        current_user,
        url_builder: self,
        highlights:  search_result.highlights,
        includes:    includes
      )
    end
  end

  def context
    @context ||= begin
      Zendesk::RuleSelection::Context::Automation.new(
        current_user,
        params.merge(pagination)
      )
    end
  end

  def position_updates
    @position_updates ||= begin
      params[:automations].each_with_object([]) do |automation_params, updates|
        next unless automation_params[:position].present?

        updates << automation_params.slice(:id, :position)
      end
    end
  end

  def positionless_updates
    @positionless_updates ||= begin
      params[:automations].each_with_object([]) do |automation_params, updates|
        next unless automation_params.key?(:active)

        updates << automation_params.slice(:id, :active)
      end
    end
  end

  def update_automation(automation_id, automation_params)
    Zendesk::Rules::Initializer.new(
      current_account,
      scope,
      {id: automation_id, automation: automation_params},
      :automation
    ).rule.tap do |updated_automation|
      updated_automation.current_user = current_user

      updated_automation.save!(validate: updated_automation.is_active?)
    end
  end

  def includes
    @includes ||= begin
      return super if current_account.has_rule_usage_stats?

      super.reject { |sideload| USAGE_SIDELOADS.include?(sideload) }
    end
  end
end
