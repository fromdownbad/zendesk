class Api::V2::Rules::PreviewsController < Api::V2::Rules::ViewsController
  include Zendesk::Rules::ControllerSupport::ViewsRateLimiterSupport
  before_action :rate_limit_find,  only: [:create]
  before_action :rate_limit_count, only: [:count]

  before_action :verify_views_availability

  allow_zopim_user only: [:create]

  condition_parameter = {
    field:    Parameters.string,
    operator: Parameters.string,
    value:    Parameters.string |
              Parameters.integer |
              Parameters.array(Parameters.string | Parameters.integer) |
              Parameters.nil
  }.freeze

  rule_parameter = {
    id:         Parameters.bigid | Parameters.enum('incoming'),
    conditions: {all: [condition_parameter], any: [condition_parameter]},
    all:        [condition_parameter],
    any:        [condition_parameter],
    output:     {
      columns:     [Parameters.string | Parameters.bigid],
      group_by:    Parameters.string,
      group_order: Parameters.string,
      sort_by:     Parameters.string,
      sort_order:  Parameters.string
    }
  }.freeze

  treat_as_read_request :create

  ## ### Previewing Views
  ## `POST /api/v2/views/preview.json`
  ##
  ## Views can be previewed by constructing the conditions in the [proper format](#conditions) and nesting them under the 'view' key.
  ## The output can also be controlled by passing in any of the following parameters and nesting them under the 'output' key.
  ##
  ## | Name            | Type    | Comment
  ## | --------------- | ------- | -------
  ## | columns         | Array   | The ticket fields to display. System fields are looked up by name, custom fields by title or id. See the [View columns](#view-columns) table
  ## | group_by        | String  | When present, the field by which the tickets are grouped
  ## | group_order     | String  | The direction the tickets are grouped. May be one of 'asc' or 'desc'
  ## | sort_order      | String  | The direction the tickets are sorted. May be one of 'asc' or 'desc'
  ## | sort_by         | String  | The ticket field used for sorting. This will either be a title or a custom field id.
  ##
  ## This endpoint is rate limited to 5 requests per minute, per view, per agent.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/preview.json \
  ##   -v -u {email}:{password} -X POST -H "Content-Type: application/json" \
  ##   -d '{"view": {"all": [{"operator": "is", "value": "open", "field": "status"}], "output": {"columns": ["subject"]}}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "rows": [
  ##     {
  ##       "ticket": { ... },
  ##       "subject": "en-US",
  ##       ...
  ##     },
  ##     ...
  ##   ],
  ##   "columns": [
  ##     {
  ##       "id": "subject",
  ##       "title": "Subject"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  allow_parameters :create,
    view: rule_parameter,
    paginate: Parameters.boolean,
    _include_archive: Parameters.boolean
  def create
    with_occam_error_handling do
      render json: row_presenter.present(view_tickets(includes: preview_includes))
    end
  end

  ## ### Preview Count
  ## `POST /api/v2/views/preview/count.json`
  ##
  ## Returns the ticket count for a single preview.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/preview/count.json \
  ##   -v -u {email}:{password} -X POST -H "Content-Type: application/json" \
  ##   -d '{"view": {"all": [{"operator": "is", "value": "open", "field": "status"}]}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "view_count": {
  ##     "view_id": null,
  ##     "url":     null,
  ##     "value":   719,
  ##     "pretty":  "~700",
  ##     "fresh":   true
  ##   }
  ## }
  ## ```
  allow_parameters :count,
    view:             rule_parameter,
    _include_archive: Parameters.boolean
  require_oauth_scopes :count, scopes: [:read], arturo: true
  def count
    render json: count_presenter.present(view_count)
  end

  protected

  def view
    @view ||= begin
      view_preview.tap do |preview|
        preview.include_archived_tickets =
          params.fetch(:_include_archive, false)
      end
    end
  end

  def view_count
    ::CachedRulePreviewTicketCount.lookup(
      view,
      current_user,
      refresh: Rails.env.development? ? nil : :delayed
    )
  end

  def view_preview
    scope.preview(
      params.fetch(:view, {}).merge(owner: current_user, title: 'preview')
    )
  end

  private

  ##
  # @see Api::V2::Rules::ViewsController#rate_limit_find
  #
  def rate_limit_find
    @rate_limiter = rate_limiter(:preview_find, view.preview_id)
    @rate_limiter.throttle!
  end

  ##
  # @see Api::V2::Rules::ViewsController#rate_limit_find
  #
  def rate_limit_count
    @rate_limiter = rate_limiter(:preview_count, view.preview_id)
    @rate_limiter.throttle!
  end

  def preview_includes
    [*DEFAULT_INCLUDES].tap do |fields|
      fields << :latest_chat_message if current_account.has_preview_with_last_chat_message? && includes.include?(:latest_chat_message)
    end
  end
end
