# frozen_string_literal: true

class Api::V2::Rules::Categories::TriggerCategoriesController < Api::V2::BaseController
  include Zendesk::ProductLimit::ControllerSupport

  CATEGORY_PARAMETERS = {
    name: Parameters.string,
    position: Parameters.integer
  }.freeze

  CATEGORY_BATCH_PARAMETERS = {
    id: Parameters.string,
    position: Parameters.integer
  }.freeze

  TRIGGER_BATCH_PARAMETERS = {
    id: Parameters.integer,
    active: Parameters.boolean,
    category_id: Parameters.string,
    position: Parameters.integer,
  }.freeze

  JOBS_PARAMETERS = {
    job: {
      action: Parameters.enum('patch'),
      items: {
        trigger_categories: [CATEGORY_BATCH_PARAMETERS].freeze,
        triggers: [TRIGGER_BATCH_PARAMETERS].freeze
      }.freeze
    }.freeze
  }.freeze

  EDIT_ACTIONS =
    %i[create destroy update jobs].freeze

  PAGINATION_PARAMS =
    %i[meta links after_url before_url after_cursor before_cursor next_page previous_page count].freeze

  require_that_user_can! :edit, RuleCategory, only: EDIT_ACTIONS

  before_action :require_setting_enabled
  before_action :require_trigger_category_param, only: [:create, :update]
  before_action :require_category, only: [:show, :update, :destroy]

  require_oauth_scopes :index, scopes: %i[triggers:read read]
  allow_parameters :index, {}
  allow_cursor_pagination_v2_parameters :index
  def index
    render json: presenter.present(paginated_categories)
  end

  require_oauth_scopes :show, scopes: %i[triggers:read read]
  allow_parameters :show, id: Parameters.bigid
  def show
    render json: presenter.present(trigger_category)
  end

  require_oauth_scopes :create, scopes: %i[triggers:write write]
  allow_parameters :create, trigger_category: CATEGORY_PARAMETERS
  def create
    category = RuleCategory.new(params[:trigger_category].merge(rule_type: Trigger.name, account: current_account))

    if category.save
      update_positions([category])

      category.reload

      render(
        json: presenter.present(category),
        status: :created,
        location: presenter.url(category)
      )
    else
      errors = category.errors.full_messages.map do |error_message|
        { code: 'InvalidTriggerCategory', title: error_message }
      end

      render json: { errors: errors }, status: :bad_request
    end
  end

  require_oauth_scopes :update, scopes: %i[triggers:write write]
  allow_parameters :update, id: Parameters.bigid, trigger_category: CATEGORY_PARAMETERS
  def update
    RuleCategory.transaction do
      if params.dig(:trigger_category, :name)
        trigger_category.update!(name: params[:trigger_category][:name])
      end

      if params.dig(:trigger_category, :position)
        update_positions([params[:trigger_category].merge(id: params[:id])])
      end
    end

    trigger_category.reload

    render(
      json: presenter.present(trigger_category),
      status: :ok,
      location: presenter.url(trigger_category)
    )
  end

  require_oauth_scopes :jobs, scopes: %i[triggers:write write]
  allow_parameters :jobs, JOBS_PARAMETERS
  def jobs
    update_positions(categories_to_update) if categories_to_update.any?
    perform_trigger_updates if triggers_to_update.any?

    results = remove_pagination_keys(presenter.present(bulk_categories)).
      merge!(remove_pagination_keys(triggers_presenter.present(bulk_triggers)))

    render json: {
      status: 'complete',
      results: results
    }, status: 200
  end

  require_oauth_scopes :destroy, scopes: %i[triggers:write write]
  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    triggers_for_category = Trigger.where(rules_category_id: trigger_category.id, account_id: current_account.id)
    active_triggers, inactive_triggers = triggers_for_category.partition(&:is_active)
    if active_triggers.count.zero?
      Trigger.soft_delete_all!(inactive_triggers.map(&:id)) if inactive_triggers.any?
      trigger_category.destroy!
      update_positions([])

      head :no_content
    else
      render json: { errors: [{ code: 'InvalidTriggerCategoryDeletion', title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.not_empty') }] }, status: :bad_request
    end
  end

  private

  def update_positions(updates)
    Zendesk::Rules::Categories::PositionUpdate.update_positions(updates, Trigger.name, current_account)
  end

  def require_setting_enabled
    return if current_account.has_trigger_categories_api_enabled?

    render json: { errors: [{
      code: 'TriggerCategoriesNotEnabled',
      title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.setting_not_enabled')
    }]}, status: :forbidden
  end

  def require_trigger_category_param
    return if params.key?(:trigger_category)

    render json: { errors: [{ code: 'InvalidTriggerCategory', title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.param_required') }] }, status: :bad_request
  end

  def require_category
    return if trigger_category

    render json: { errors: [{
      code: 'TriggerCategoryNotFound',
      title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.does_not_exist')
    }]}, status: :not_found
  end

  def trigger_category
    @trigger_category ||= RuleCategory.trigger_categories(current_account).find_by_id(params[:id])
  end

  def job_params
    params.require(:job).tap do |job|
      job.require(:action)
      job.require(:items).tap do |items|
        if categories = items[:trigger_categories]
          categories.each { |item| %i[id position].each(&item.method(:require)) }
        end
        if triggers = items[:triggers]
          triggers.each { |item| item.require(:id) }
        end
      end
    end.with_indifferent_access
  end

  def presenter
    @presenter ||= Api::V2::Rules::RuleCategoryPresenter.new(current_user, model_key: 'trigger_category', url_builder: self, includes: includes, rule_type: Trigger.name)
  end

  def triggers_presenter
    @triggers_presenter ||= begin
      Api::V2::Rules::TriggerPresenter.new(
        current_user,
        url_builder: self,
        includes: includes,
        with_cursor_pagination: true
      )
    end
  end

  def categories
    RuleCategory.trigger_categories(current_account).order(:position)
  end

  def sortable_fields
    {
      'position' => :position,
      'name' => :name,
      'created_at' => :created_at,
      'updated_at' => :updated_at
    }
  end

  def sorted_categories
    sorted_scope_for_cursor(categories, sortable_fields)
  end

  def paginated_categories
    sorted_categories.paginate_with_cursor(
      cursor_pagination_version: 2,
      page: params[:page]
    )
  end

  def categories_to_update
    @categories_to_update ||= job_params.dig(:items, :trigger_categories) || []
  end

  def triggers_to_update
    @triggers_to_update ||= job_params.dig(:items, :triggers) || []
  end

  def perform_trigger_updates
    positionless_updates, position_updates = Zendesk::Rules::Categories::RuleUpdates.partition(triggers_to_update)

    Trigger.transaction do
      if positionless_updates.any?
        updates = positionless_updates.each_with_object({ ids: [], params: [] }) do |trigger, updates_hash|
          updates_hash[:ids] << trigger[:id]
          updates_hash[:params] << normalize_bulk_trigger_params(trigger)
        end

        Trigger.update(updates[:ids], updates[:params])
      end

      if position_updates.any?
        Zendesk::Rules::PositionUpdateWithCategories.perform(
          Zendesk::RuleSelection::Context::Trigger.new(current_user),
          position_updates
        )
      end
    end
  end

  def normalize_bulk_trigger_params(trigger_params)
    trigger_params[:is_active] = trigger_params.delete(:active) if trigger_params.key?(:active)
    trigger_params[:rules_category_id] = trigger_params.delete(:category_id) if trigger_params.key?(:category_id)

    trigger_params
  end

  def bulk_categories
    ids = categories_to_update.map { |category| category[:id] }

    RuleCategory.where(account: current_account, rule_type: Trigger.name, id: ids).order(:position)
  end

  def trigger_ids
    @trigger_ids ||= triggers_to_update.map { |trigger| trigger[:id] }
  end

  def bulk_triggers
    Trigger.where(account: current_account, id: trigger_ids).joins(:rule_category).order('rules_categories.position', :position, :title)
  end

  def remove_pagination_keys(obj)
    obj.reject { |key, _| PAGINATION_PARAMS.include?(key) }
  end
end
