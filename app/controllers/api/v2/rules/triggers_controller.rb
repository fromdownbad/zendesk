class Api::V2::Rules::TriggersController < Api::V2::Rules::BaseController
  ACTIVE_PARAMETERS = {
    sort_by:    Parameters.string,
    sort_order: Parameters.string
  }.freeze

  INDEX_PARAMETERS = ACTIVE_PARAMETERS.merge(
    active: Parameters.boolean,
    category_id: Parameters.bigid
  ).freeze

  CONDITION_PARAMETER = {
    field:    Parameters.string,
    operator: Parameters.string,
    value:    Parameters.anything
  }.freeze

  TRIGGER_PARAMETERS = {
    title: Parameters.string,
    active: Parameters.boolean,
    category_id: Parameters.bigid,
    category: {
      name: Parameters.string,
      position: Parameters.integer
    }.freeze,
    description: Parameters.string,
    position: Parameters.integer,
    restriction: Parameters.anything,
    group_id: Parameters.bigid,
    conditions: Parameters.map(
      all:       [CONDITION_PARAMETER].freeze,
      any:       [CONDITION_PARAMETER].freeze
    ),
    all: [CONDITION_PARAMETER].freeze,
    any: [CONDITION_PARAMETER].freeze,
    actions: [
      {field: Parameters.string, value: Parameters.anything}.freeze
    ].freeze
  }.freeze

  SEARCH_PARAMETERS = INDEX_PARAMETERS.merge(
    filter:     Parameters.anything,
    query:      Parameters.string,
    sort_by:    Parameters.string,
    sort_order: Parameters.string
  ).freeze

  BATCH_PARAMETERS = {
    id: Parameters.integer,
    active: Parameters.boolean,
    category_id: Parameters.bigid,
    position: Parameters.integer,
  }.freeze

  EDIT_ACTIONS =
    %i[create destroy reorder destroy_many update update_many].freeze

  USAGE_SIDELOADS = %i[usage_1h usage_24h usage_7d usage_30d].freeze

  before_action :ensure_category_params, only: :create
  before_action :restrict_category_params, only: [:create, :update]

  require_that_user_can! :edit, Trigger, only: EDIT_ACTIONS

  ## ### List Triggers
  ## `GET /api/v2/triggers.json`
  ##
  ## Lists all triggers for the current account
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | active     | boolean | Only active triggers if `true`, inactive triggers if `false`
  ## | sort_by    | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, `usage_1h`, `usage_24h`, or `usage_7d`. Defaults to `position`
  ## | sort_order | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported. The usage sideloads are only supported on the Professional and Enterprise plans.
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each trigger, if present
  ## | permissions      | The permissions for each trigger
  ## | usage_1h         | The number of times each trigger has been used in the past hour
  ## | usage_24h        | The number of times each trigger has been used in the past day
  ## | usage_7d         | The number of times each trigger has been used in the past week
  ## | usage_30d        | The number of times each trigger has been used in the past thirty days
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "triggers": [
  ##     {
  ##       "url": "http://{subdomain}.zendesk.com/api/v2/triggers/25.json",
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 8,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "description": "Close and save a ticket",
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     {
  ##       "url": "http://{subdomain}.zendesk.com/api/v2/triggers/26.json",
  ##       "id": 26,
  ##       "title": "Assign priority tag",
  ##       "raw_title": "{{dc.assign_priority_tag}}",
  ##       "position": 9,
  ##       "active": false,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "description": "Assign a ticket with a priority tag",
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     }
  ##   ],
  ##   "previous_page": null,
  ##   "next_page": null,
  ##   "count": 2
  ## }
  ## ```
  allow_parameters :index, INDEX_PARAMETERS
  require_oauth_scopes :index, scopes: %i[triggers:read read]
  def index
    render json: presenter.present(triggers)
  end

  ## ### Getting Triggers
  ## `GET /api/v2/triggers/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/{id}.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "trigger": {
  ##     "id": 25,
  ##     "title": "Tickets updated <12 Hours",
  ##     "raw_title": "{{dc.tickets_updated_last_12_hours}}"
  ##     "position": 8,
  ##     "active": true,
  ##     "conditions": [ ... ],
  ##     "actions": [ ... ],
  ##     "description": "12 hours since the last ticket update",
  ##     "updated_at": "2012-09-25T22:50:26Z",
  ##     "created_at": "2012-09-25T22:50:26Z"
  ##   }
  ## }
  ## ```
  ##
  ## The Via Type value is a number instead of a text string. See [Via Types](#via-types) above for the keys.
  ##
  require_oauth_scopes :show, scopes: %i[triggers:read read]
  resource_action :show

  ## ### List Active Triggers
  ## `GET /api/v2/triggers/active.json`
  ##
  ## Lists all active triggers
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name       | Type   | Comment
  ## | ---------- | ------ | -------
  ## | sort_by    | string | Possible values are `alphabetical`, `created_at`, `updated_at`, `usage_1h`, `usage_24h`, or `usage_7d`. Defaults to `position`
  ## | sort_order | string | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each trigger, if present
  ## | permissions      | The permissions for each trigger
  ## | usage_1h         | The number of times each trigger has been used in the past hour
  ## | usage_24h        | The number of times each trigger has been used in the past day
  ## | usage_7d         | The number of times each trigger has been used in the past week
  ## | usage_30d        | The number of times each trigger has been used in the past thirty days
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/active.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "triggers": [
  ##     {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 8,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "description": "Close and save a ticket",
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     {
  ##       "id": 28,
  ##       "title": "Close and redirect to topics",
  ##       "raw_title": "{{dc.close_and_redirect}}",
  ##       "position": 9,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "description": "Close and redirect a ticket",
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     }
  ##   ],
  ##   "previous_page": null,
  ##   "next_page": null,
  ##   "count": 2
  ## }
  ## ```
  allow_parameters :active, ACTIVE_PARAMETERS
  require_oauth_scopes :active, scopes: %i[triggers:read read write]
  def active
    params[:active] = true

    render json: presenter.present(triggers)
  end

  ## ### Create Trigger
  ## `POST /api/v2/triggers.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/triggers.json \
  ##   -H "Content-Type: application/json" -X POST -d \
  ##   '{"trigger": {"title": "Roger Wilco", "all": [{ "field": "status", "operator": "is", "value": "open" }, { "field": "priority", "operator": "less_than", "value": "high" }], "actions": [{ "field": "group_id", "value": "20455932" }]}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: /api/v2/trigger/{new-trigger-id}.json
  ##
  ## {
  ##   "trigger": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  #  h4 Request Parameters
  #
  #  The POST request takes one parameter, a `trigger` object that lists the values to set when the trigger is created.
  #
  # | Name        | Description
  # | ----------- | -----------
  # | actions     | Required. An object describing what the trigger will do
  # | active      | Allowed values are `true` or `false`. Whether the trigger is active
  # | conditions  | Required. An object describing the conditions under which the trigger will execute
  # | description | The description of the trigger
  # | title       | Required. The title of the trigger

  allow_parameters :create, trigger: TRIGGER_PARAMETERS
  require_oauth_scopes :create, scopes: %i[triggers:write write]

  ## ### Update Trigger
  ## `PUT /api/v2/triggers/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/triggers/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT -d '{"trigger": {"title": "Roger Wilco II"}}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "trigger": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco II",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Note
  ##
  ## Updating a condition or action updates both the `conditions` and `actions`
  ## arrays, clearing all existing values of both arrays. Include all your
  ## conditions and actions when updating any condition or action.
  allow_parameters :update, id: Parameters.bigid, trigger: TRIGGER_PARAMETERS
  require_oauth_scopes :update, scopes: %i[triggers:write write]

  ## ### Update Many Triggers
  ## `PUT /api/v2/triggers/update_many.json`
  ##
  ## Updates the position or the active status of multiple triggers.
  ## Any additional properties are ignored.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/update_many.json \
  ##   -v -u {email}:{password} -H "Content-Type: application/json" \
  ##   -X PUT -d '{"triggers": [{"id": 26, "position": 8}, {"id": 25, "position": 15}]}'
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "triggers": [
  ##     {
  ##       "url": "http://{subdomain}.zendesk.com/api/v2/triggers/26.json",
  ##       "id": 26,
  ##       "title": "Assign priority tag",
  ##       "raw_title": "{{dc.assign_priority_tag}}",
  ##       "position": 8,
  ##       "active": false,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "description": "Assign a ticket with a priority tag",
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     {
  ##       "url": "http://{subdomain}.zendesk.com/api/v2/triggers/25.json",
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 15,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "description": "Close and save a ticket",
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ##
  ## #### Request Parameters
  ##
  ## The PUT request expects a `triggers` object that lists the triggers to update.
  ##
  ## Each trigger may have the following properties:
  ##
  ## | Name     | Mandatory | Description
  ## | -------- | --------- | -----------
  ## | id       | yes       | The ID of the trigger to update
  ## | position | no        | The new position of the trigger
  ## | active   | no        | The active status of the trigger (true or false)
  ##
  ## #### Example Request
  ##
  ## ```js
  ## {
  ##   "triggers": [
  ##     {"id": 25, "position": 3},
  ##     {"id": 23, "position": 5},
  ##     {"id": 27, "position": 9},
  ##     {"id": 22, "position": 7}
  ##   ]
  ## }
  ## ```
  allow_parameters :update_many, triggers: [BATCH_PARAMETERS]
  require_oauth_scopes :update_many, scopes: %i[triggers:write write]
  def update_many
    return head :bad_request unless params[:triggers].present?

    @positionless_updates, @position_updates = Zendesk::Rules::Categories::RuleUpdates.partition(params[:triggers]) if categories_enabled?

    Trigger.transaction do
      positionless_updates.each do |trigger_params|
        update_trigger(
          trigger_params[:id],
          {
            active: trigger_params[:active],
            category_id: trigger_params[:category_id]
          }.compact
        )
      end

      if position_updates.any?
        position_update_class = if categories_enabled?
          Zendesk::Rules::PositionUpdateWithCategories
        else
          Zendesk::Rules::PositionUpdate
        end

        position_update_class.perform(
          Zendesk::RuleSelection::Context::Trigger.new(current_user),
          position_updates
        )
      end
    end

    render json: presenter.present(bulk_triggers)
  end

  ## ### Delete Trigger
  ## `DELETE /api/v2/triggers/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email}:{password} https://{subdomain}.zendesk.com/api/v2/triggers/{id}.json \
  ##   -H "Content-Type: application/json" -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  require_oauth_scopes :destroy, scopes: %i[triggers:write write]

  ## ### Bulk Delete Triggers
  ## `DELETE /api/v2/triggers/destroy_many.json`
  ##
  ## Deletes the triggers corresponding to the provided comma-separated list of
  ## IDs.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/destroy_many.json?ids=1,2,3 \
  ##   -v -u {email}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  ## #### Request Parameters
  ##
  ## The DELETE request takes one parameter, an `ids` object that lists the
  ## triggers to delete.
  ##
  ## | Name | Description
  ## | ---- | -----------
  ## | ids  | The IDs of the triggers to delete
  ##
  ## #### Example request
  ##
  ## ```js
  ## {
  ##   "ids": "25,23,27,22"
  ## }
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids
  require_oauth_scopes :destroy_many, scopes: %i[triggers:write write read]
  def destroy_many
    return head :bad_request unless many_ids?
    return head :bad_request if categories_enabled? && bulk_triggers.any?(&:is_active?)

    bulk_triggers.each { |trigger| trigger.soft_delete(validate: false) }

    head :no_content
  end

  ## ### Reorder Triggers
  ## `PUT /api/v2/triggers/reorder.json`
  ##
  ## Alters the firing order of triggers in the account. See
  ## [Reordering and sorting triggers](https://support.zendesk.com/hc/en-us/articles/115015696088)
  ## in the Zendesk Help Center. The firing order is set in a `trigger_ids` array in the request body.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/reorder.json \
  ##   -d '{"trigger_ids": [324376, 564937, 164318]}' \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :reorder, trigger_ids: [Parameters.bigid]
  require_oauth_scopes :reorder, scopes: %i[triggers:write write read]
  def reorder
    if categories_enabled? && categories.count > 1
      render json: {
        error:       'LimitOneCategory',
        description: 'this action is not permitted with multiple categories'
      }, status: 400
    else
      reorder_triggers

      head :ok
    end
  end

  ## ### Search Triggers
  ## `GET /api/v2/triggers/search.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Search Parameters
  ##
  ## Required
  ##
  ## | Name  | Type   | Comment
  ## | ----- | ------ | -------
  ## | query | string | Query string used to find all triggers with matching title
  ##
  ## Optional
  ##
  ##  You can use any combination of the following optional parameters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | active     | boolean | Only active triggers if `true`, inactive triggers if `false`
  ## | sort_by    | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, and `position`. If unspecified, the triggers are sorted by relevance
  ## | sort_order | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each trigger, if present
  ## | permissions      | The permissions for each trigger
  ## | usage_1h         | The number of times each trigger has been used in the past hour
  ## | usage_24h        | The number of times each trigger has been used in the past day
  ## | usage_7d         | The number of times each trigger has been used in the past week
  ## | usage_30d        | The number of times each trigger has been used in the past thirty days
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/search.json?query=close \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "triggers": [
  ##     {
  ##       "id": 25,
  ##       "title": "Close and Save",
  ##       "raw_title": "Close and Save",
  ##       "position": 9,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "description": "Close and save a ticket",
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     },
  ##     {
  ##       "id": 28,
  ##       "title": "Close and redirect to topics",
  ##       "raw_title": "{{dc.close_and_redirect}}",
  ##       "position": 9,
  ##       "active": true,
  ##       "conditions": [ ... ],
  ##       "actions": [ ... ],
  ##       "updated_at": "2012-09-25T22:50:26Z",
  ##       "created_at": "2012-09-25T22:50:26Z"
  ##     }
  ##   ],
  ##   "previous_page": null,
  ##   "next_page": null,
  ##   "count": 2
  ## }
  ## ```
  allow_parameters :search, SEARCH_PARAMETERS
  require_oauth_scopes :search, scopes: %i[triggers:read read]
  def search
    return head :bad_request unless params[:query].present? || use_new_trigger_search_endpoint?

    render json: search_presenter.present(search_result.rules)
  end
  # h3 Show trigger replica
  # `GET /api/v2/triggers/new.json`
  #
  # Returns an unpersisted trigger representation derived from an existing trigger.
  #
  # h4 Allowed For
  #
  #  * Agents
  #
  # h4 Available Parameters
  #
  # You must pass the following parameter:
  #
  # | Name       | Type    | Comment
  # | ---------- | ------- | -------
  # | trigger_id | integer | ID of the trigger to replicate
  #
  # h4 Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/triggers/new.json?trigger_id=123 \
  #   -u {email}:{password}
  # ```
  #
  # h4 Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "trigger": {
  #     "title": "Notify requester of comment update",
  #     "description": "Notifies requester that a comment was updated.",
  #     "actions": { ... },
  #     "conditions": { ... },
  #     "position": 10
  #   }
  # }
  # ```

  allow_parameters :new, trigger_id: Parameters.integer
  require_oauth_scopes :new, scopes: %i[triggers:read read write]
  def new
    return head :bad_request unless trigger_replica.present?

    render json: replica_presenter.present(trigger_replica)
  end

  ## ### List Trigger Action and Condition Definitions
  ## `GET /api/v2/triggers/definitions.json`
  ##
  ## Returns the definitions of the actions a trigger can perform and the
  ## definitions of the conditions under which a trigger can execute. The
  ## definition of the action includes a title ("Status"), a type ("list"), and
  ## possible values. The definition of the condition includes the same fields
  ## as well as the possible operators.
  ##
  ## For a list of supported actions, see [Action reference](./triggers#actions-reference).
  ## For a list of supported conditions, see [Condition reference](./triggers#conditions-reference)
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/triggers/definitions.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "definitions": {
  ##     "actions": [
  ##       {
  ##         "title": "Status",
  ##         "type": "list",
  ##         "value": "status",
  ##         "group": "ticket",
  ##         "repeatable": false,
  ##         "values": [
  ##           { "value": "1", "title": "Open", "enabled": true },
  ##           { "value": "2", "title": "Pending", "enabled": true },
  ##           { "value": "3", "title": "Solved", "enabled": true },
  ##           { "value": "4", "title": "Closed", "enabled": true }
  ##         ]
  ##       },
  ##       ...
  ##     ],
  ##     "conditions_all": [
  ##       {
  ##         "title": "Type",
  ##         "subject": "type",
  ##         "type": "list",
  ##         "group": "ticket",
  ##         "nullable": true,
  ##         "repeatable": false,
  ##         "operators": [
  ##           { "value": "is", "title": "Is", "terminal": false },
  ##           { "value": "is_not", "title": "Is not", "terminal": false },
  ##           { "value": "changed", "title": "Changed", "terminal": true },
  ##           ...
  ##         ],
  ##         "values": [
  ##           { "value": "__NULL__", "title": "-", "enabled": true },
  ##           { "value": "question", "title": "Question", "enabled": true },
  ##           { "value": "incident", "title": "Incident", "enabled": true }
  ##           ...
  ##         ],
  ##       },
  ##       ...
  ##     ],
  ##     "conditions_any": [...]
  ##   }
  ## }
  ## ```
  allow_parameters :definitions, {}
  require_oauth_scopes :definitions,
    scopes: %i[triggers:read read],
    arturo: true
  def definitions
    render json: definitions_presenter.present(trigger_definitions)
  end

  private

  def triggers
    @triggers ||= scope.paginate(pagination)
  end

  def scope
    @scope ||= Zendesk::RuleSelection::Scope.for_context(context)
  end

  def search_result
    @search_result ||= if use_new_trigger_search_endpoint?
      Zendesk::RuleSelection::Search::Triggers.for_context(context)
    else
      Zendesk::RuleSelection::Search.for_context(context)
    end
  end

  def trigger_ids
    @trigger_ids ||=
      many_ids || params[:triggers].map { |trigger| trigger[:id] }
  end

  def bulk_triggers
    scope.find(trigger_ids)
  end

  def presenter
    @presenter ||= begin
      Api::V2::Rules::TriggerPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def search_presenter
    @search_presenter ||= begin
                            Api::V2::Rules::TriggerPresenter.new(
                              current_user,
                              url_builder: self,
                              highlights: search_result.highlights,
                              includes: includes
                            )
                          end
  end

  def ensure_category_params
    return unless current_account.has_trigger_categories_break_triggers_endpoint? && categories_enabled?

    trigger_params = params.require(:trigger)

    if !trigger_params.key?(:category_id) && !trigger_params.key?(:category)
      render json: {
        error:       'InvalidCategoryParam',
        description: I18n.t('txt.admin.models.rules.rule.rule_type.triggers_missing_category_params')
      }, status: 400
    end
  end

  def restrict_category_params
    return unless categories_enabled?

    trigger_params = params.require(:trigger)

    if trigger_params.key?(:category_id) && trigger_params.key?(:category)
      render json: {
        error:       'InvalidCategoryParam',
        description: I18n.t('txt.admin.models.rules.rule.rule_type.triggers_both_category_params')
      }, status: 400
    end
  end

  def reorder_triggers
    ids = params[:trigger_ids].map(&:to_i)

    unless ids.sort == scope.active.map(&:id).sort
      raise Zendesk::UnknownAttributeError, 'all triggers are not provided'
    end

    current_account.all_rules.update_position(ids)
  end

  def categories
    RuleCategory.trigger_categories(current_account).order(:position)
  end

  def categories_enabled?
    current_account.has_trigger_categories_api_enabled?
  end

  def context
    @context ||= begin
      Zendesk::RuleSelection::Context::Trigger.new(
        current_user,
        params.merge(pagination).merge(trigger_categories_api: categories_enabled?)
      )
    end
  end

  def use_new_trigger_search_endpoint?
    Arturo.feature_enabled_for?(:new_trigger_search, current_account) && params[:filter] && params[:query].nil?
  end

  def definitions_presenter
    @definitions_presenter ||= begin
      Api::V2::Rules::TriggerDefinitionsPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def replica_presenter
    @replica_presenter ||= begin
      Api::V2::Rules::TriggerReplicaPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def trigger_definitions
    @trigger_definitions ||= begin
      Zendesk::Rules::Definitions.new(
        account:   current_account,
        user:      current_user,
        rule_type: :trigger
      )
    end
  end

  def trigger_replica
    @trigger_replica ||= scope.find_by_id(params[:trigger_id])
  end

  def position_updates
    @position_updates ||= begin
      params[:triggers].each_with_object([]) do |trigger_params, updates|
        next unless trigger_params[:position].present?

        updates << trigger_params.slice(:id, :position)
      end
    end
  end

  def positionless_updates
    @positionless_updates ||= begin
      params[:triggers].each_with_object([]) do |trigger_params, updates|
        next if (trigger_params.keys & %w[active category_id]).empty?
        updates << trigger_params.slice(:id, :active, :category_id)
      end
    end
  end

  def update_trigger(trigger_id, trigger_params)
    Zendesk::Rules::Initializer.new(
      current_account,
      scope,
      {id: trigger_id, trigger: trigger_params},
      :trigger
    ).rule.tap do |updated_trigger|
      updated_trigger.current_user = current_user

      updated_trigger.save!(validate: updated_trigger.is_active?)
    end
  end

  def includes
    @includes ||= begin
      return super if current_account.has_rule_usage_stats?

      super.reject { |sideload| USAGE_SIDELOADS.include?(sideload) }
    end
  end
end
