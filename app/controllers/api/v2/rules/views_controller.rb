require 'occam_client/client'

class Api::V2::Rules::ViewsController < Api::V2::Rules::BaseController
  include Zendesk::Rules::ControllerSupport::ViewsRateLimiterSupport
  include Zendesk::Routing::ControllerSupport

  before_action :validate_count_many_api_limit, only: [:count_many]
  before_action :rate_limit_find,  only: [:execute, :filter]
  before_action :rate_limit_play,  only: [:next]
  before_action :rate_limit_count, only: [:count]
  before_action :rate_limit_count_many, only: [:count_many]
  before_action :rate_limit_tickets, only:  [:tickets]
  before_action :verify_views_availability, only: [:count, :count_many, :execute, :filter, :export]

  class ViewsServiceUnavailableError < StandardError
  end

  rescue_from ViewsServiceUnavailableError, with: :handle_views_unavailable

  OCCAM_ERRORS = [
    Occam::Client::ClientError,
    Occam::Client::ClientConnectionError,
    Occam::Client::ClientTimeoutError,
    Occam::Client::QueryTimeoutError
  ].freeze

  ACTIVE_PARAMETERS = {
    access: Parameters.string,
    group_id: Parameters.bigid
  }.freeze

  INDEX_PARAMETERS = ACTIVE_PARAMETERS.merge(
    active: Parameters.boolean
  ).freeze

  SHOW_MANY_PARAMETERS = {
    ids: Parameters.ids,
    active: Parameters.boolean
  }.freeze

  CONDITION_PARAMETER = {
    field:    Parameters.string,
    operator: Parameters.string,
    value:    Parameters.string | Parameters.nil
  }.freeze

  VIEW_PARAMETERS = {
    title: Parameters.string,
    active: Parameters.boolean,
    description: Parameters.string,
    restriction: Parameters.map(
      type: Parameters.string,
      id:   Parameters.integer,
      ids:  Parameters.array(Parameters.integer)
    ).freeze | Parameters.nil,
    conditions: Parameters.map(
      all:         [CONDITION_PARAMETER].freeze,
      any:         [CONDITION_PARAMETER].freeze
    ).freeze,
    all: [CONDITION_PARAMETER].freeze,
    any: [CONDITION_PARAMETER].freeze,
    output: Parameters.map(
      columns:     Parameters.array(Parameters.string | Parameters.bigid),
      group_by:    Parameters.string,
      group_order: Parameters.string,
      sort_by:     Parameters.string,
      sort_order:  Parameters.string
    ).freeze | Parameters.nil
  }.freeze

  BATCH_PARAMETERS = {
    id:       Parameters.integer,
    active:   Parameters.boolean,
    position: Parameters.integer
  }.freeze

  ID_OR_INCOMING_PARAMETERS = begin
    Parameters.bigid |
      Parameters.enum('incoming') |
      Parameters.enum('my') |
      Parameters.enum('my_groups')
  end

  SEARCH_PARAMETERS = INDEX_PARAMETERS.merge(
    query:      Parameters.string,
    sort_by:    Parameters.string,
    sort_order: Parameters.string
  ).freeze

  EXECUTE_PARAMETERS = {
    id:            ID_OR_INCOMING_PARAMETERS,
    group_by:      Parameters.string,
    group_order:   Parameters.enum('ASC', 'asc', 'DESC', 'desc') |
                   Parameters.nil_string,
    hanlon_cache:  Parameters.boolean,
    simulate_slow: Parameters.integer,
    paginate:      Parameters.boolean
  }.freeze

  TICKETS_PARAMETERS = {
    id:          ID_OR_INCOMING_PARAMETERS,
    last_viewed: Parameters.date_time,
    # Keeping these here for legacy but they won't have any effect on the result
    group_by:    Parameters.string,
    group_order: Parameters.enum('ASC', 'asc', 'DESC', 'desc') |
                 Parameters.nil_string
  }.freeze

  NEXT_PARAMETERS = {
    id:          ID_OR_INCOMING_PARAMETERS,
    include:     Parameters.string,
    rewind:      Parameters.boolean,
    debug:       Parameters.boolean,
    group_by:    Parameters.string,
    group_order: Parameters.string,
    sort_by:     Parameters.string,
    sort_order:  Parameters.enum('ASC', 'asc', 'DESC', 'desc') |
                 Parameters.nil_string
  }.freeze

  VIEW_FILTER_PARAMETERS = {
    hanlon_cache:  Parameters.boolean,
    id:            Parameters.bigid,
    simulate_slow: Parameters.integer
  }.freeze

  SKILL_BASED_FILTER_PER_PAGE = 30
  COUNT_MANY_API_LIMIT = 20

  SOURCE_SUPPORT_RULE_ADMIN = 'rule_admin'.freeze
  DEFAULT_REQUEST_SOURCE = 'classic-api-v2'.freeze
  DEFAULT_INCLUDES = [:latest_comment].freeze

  include Zendesk::Tickets::RecentTicketManagement
  include Zendesk::Routing::ViewControllerSupport

  require_that_user_can! :edit, :rule,
    only:   %i[create update],
    unless: :inherited_controller?

  require_that_user_can! :edit, :initial_view,
    only:   %i[update destroy],
    unless: :inherited_controller?

  require_that_user_can! :edit, :bulk_views,
    only:   %i[destroy_many update_many],
    unless: :inherited_controller?

  require_that_user_can! :view, Group, only: :groups

  ## ### List Views
  ## `GET /api/v2/views.json`
  ##
  ## Lists shared and personal views available to the current user.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | access     | string  | Only views with given access. May be "personal", "shared", or "account"
  ## | active     | boolean | Only active views if true, inactive views if false
  ## | group_id   | integer | Only views belonging to given group
  ## | sort_by    | string  | Possible values are "alphabetical", "created_at", or "updated_at". Defaults to "position"
  ## | sort_order | string  | One of "asc" or "desc". Defaults to "asc" for alphabetical and position sort, "desc" for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each view, if present
  ## | permissions      | The permissions for each view
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "views": [
  ##     {
  ##       "id": 25,
  ##       "title": "Tickets updated <12 Hours",
  ##       "raw_title": "{{dc.tickets_updated_last_12_hours}}",
  ##       "description": "View for recent tickets",
  ##       "active": true,
  ##       "position": 3,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     {
  ##       "id": 23,
  ##       "title": "Unassigned tickets",
  ##       "raw_title": "{{dc.unassigned_tickets}}",
  ##       "description": "View for tickets that are not assigned",
  ##       "active": false,
  ##       "position": 7,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     ...
  ##   ],
  ##   "count": 7,
  ##   "next_page": null,
  ##   "previous_page": null
  ## }
  ## ```
  allow_parameters :index, INDEX_PARAMETERS
  def index
    render json: presenter.present(views)
  end

  ## ### List Views by ID
  ## `GET /api/v2/views/show_many.json?ids=1,2,3`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters:
  ##
  ## | Name       | Type        | Comment
  ## | ---------- | ----------- | -------
  ## | ids        | list of id  | List of view's ids separated by commas.
  ## | active     | boolean     | Only active views if true, inactive views if false
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each view, if present
  ## | permissions      | The permissions for each view
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/show_many.json?ids=1,2,3 \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "views": [
  ##     {
  ##       "id": 25,
  ##       "title": "Tickets updated <12 Hours",
  ##       "raw_title": "{{dc.tickets_updated_last_12_hours}}",
  ##       "description": "View for recent tickets",
  ##       "active": true,
  ##       "position": 3,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     {
  ##       "id": 23,
  ##       "title": "Unassigned tickets",
  ##       "raw_title": "{{dc.unassigned_tickets}}",
  ##       "description": "View for tickets that are not assigned",
  ##       "active": false,
  ##       "position": 7,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     ...
  ##   ],
  ##   "count": 7,
  ##   "next_page": null,
  ##   "previous_page": null
  ## }
  ## ```
  allow_parameters :show_many, SHOW_MANY_PARAMETERS
  def show_many
    return head :bad_request unless many_ids?

    render json: presenter.present(views.where(id: many_ids))
  end

  ## ### Update Many Views
  ## `PUT /api/v2/views/update_many.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/update_many.json \
  ##   -d '{"views": [{"id": 123, "position": 8}]}' \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "views": [
  ##     {
  ##       "id": 25,
  ##       "title": "Tickets updated <12 Hours",
  ##       "title": "{{dc.tickets_updated_last_12_hours}}",
  ##       "description": "View for recent tickets",
  ##       "active": true,
  ##       "position": 3,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     {
  ##       "id": 23,
  ##       "title": "Unassigned tickets",
  ##       "title": "{{dc.unassigned_tickets}}",
  ##       "description": "View for tickets that are not assigned",
  ##       "active": false,
  ##       "position": 5,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ## <a name="request-parameters-update-many"></a>
  ## #### Request Parameters
  ##
  ## The PUT request expects a `views` object that lists the views to update.
  ##
  ## Each view may have the following properties:
  ##
  ## | Name     | Mandatory | Description
  ## | -------- | --------- | -----------
  ## | id       | yes       | The ID of the view to update
  ## | position | no        | The new position of the view
  ## | active   | no        | The active status of the view (true or false)
  ##
  ## #### Example Request Body
  ##
  ## ```js
  ## {
  ##   "views": [
  ##     {"id": 25, "position": 3},
  ##     {"id": 23, "position": 5},
  ##     {"id": 27, "position": 9},
  ##     {"id": 22, "position": 7}
  ##   ]
  ## }
  ## ```
  allow_parameters :update_many, views: [BATCH_PARAMETERS]
  def update_many
    return head :bad_request unless params[:views].present?

    View.transaction do
      positionless_updates.each do |view_params|
        update_view(view_params[:id], active: view_params[:active])
      end

      if position_updates.any?
        Zendesk::Rules::PositionUpdate.perform(
          Zendesk::RuleSelection::Context::View.new(current_user),
          position_updates
        )
      end
    end

    render json: presenter.present(bulk_views)
  end

  ## ### List Active Views
  ## `GET /api/v2/views/active.json`
  ##
  ## Lists active shared and personal views available to the current user.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can use any combination of the following optional filters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | access     | string  | Only views with given access. May be "personal", "shared", or "account"
  ## | group_id   | integer | Only views belonging to given group
  ## | sort_by    | string  | Possible values are "alphabetical", "created_at", or "updated_at". Defaults to "position"
  ## | sort_order | string  | One of "asc" or "desc". Defaults to "asc" for alphabetical and position sort, "desc" for all others
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each view, if present
  ## | permissions      | The permissions for each view
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/active.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "views": [
  ##     {
  ##       "id": 25,
  ##       "title": "Tickets updated <12 Hours",
  ##       "raw_title": "{{dc.tickets_updated_last_12_hours}}",
  ##       "description": "View for recent tickets",
  ##       "active": true,
  ##       "position": 3,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     ...
  ##   ],
  ##   "count": 7,
  ##   "next_page": null,
  ##   "previous_page": null
  ## }
  ## ```
  allow_parameters :active, ACTIVE_PARAMETERS
  require_oauth_scopes :active, scopes: [:read], arturo: true
  def active
    params[:active] = true

    render json: presenter.present(views)
  end

  ## ### List Views - Compact
  ## `GET /api/v2/views/compact.json`
  ##
  ## A compacted list of shared and personal views available to the current user.
  ## This endpoint never returns more than 32 records and does not respect the "per_page" option.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/compact.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "views": [
  ##     {
  ##       "id": 25,
  ##       "title": "Tickets updated <12 Hours",
  ##       "raw_title": "{{dc.tickets_updated_last_12_hours}}",
  ##       "description": "View for recent tickets",
  ##       "active": true,
  ##       "position": 3,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     ...
  ##   ],
  ##   "count": 7,
  ##   "next_page": null,
  ##   "previous_page": null
  ## }
  ## ```
  allow_parameters :compact, {}
  require_oauth_scopes :compact, scopes: [:read], arturo: true
  def compact
    if current_account.has_views_compact_cache_control_header?
      expires_in 5.seconds
    end

    render json: presenter.present(compact_views)
  end

  ## ### Show View
  ## `GET /api/v2/views/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/{id}.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "view": {
  ##     "id": 25,
  ##     "title": "Tickets updated <12 Hours",
  ##     "raw_title": "{{dc.tickets_updated_last_12_hours}}",
  ##     "description": "View for recent tickets",
  ##     "active": true,
  ##     "position": 3,
  ##     "execution": { ... },
  ##     "conditions": { ... },
  ##     "restriction": { ... }
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: ID_OR_INCOMING_PARAMETERS
  def show
    render json: presenter.present(view)
  end

  ## ### Create View
  ## `POST /api/v2/views.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views.json \
  ##   -d '{"view": {"title": "Roger Wilco", "all": [{"field": "status", "operator": "is", "value": "open"}, {"field": "priority", "operator": "less_than", "value": "high"}], "any": [{ "field": "current_tags", "operator": "includes", "value": "hello" }]}}' \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email}:{password} -X POST
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: /api/v2/view/{new-view-id}.json
  ##
  ## {
  ##   "view": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## <a name="request-parameters-create"></a>
  ## #### Request Parameters
  ##
  ## The POST request takes one parameter, a `view` object that lists the values
  ## to set when the view is created.
  ##
  ## **Note**: The request must include at least one condition in the `all` array that checks
  ## one of the following fields: `status`, `type`, `group_id`, `assignee_id`, or `requester_id`.
  ##
  ## | Name        | Description
  ## | ----------- | -----------
  ## | title       | Required. The title of the view
  ## | all         | Required. An array of one or more conditions. A ticket must meet all of them to be included in the view. See [Conditions](./views#conditions)
  ## | any         | An array of one or more conditions. A ticket must meet any of them to be included in the view. See [Conditions](./views#conditions)
  ## | description | The description of the view
  ## | active      | Allowed values are true or false. Determines if the view is displayed or not
  ## | output      | An object that specifies the columns to display. Example: `"output": {"columns": ["status", "description", "priority"]}`. See [View columns](#view-columns)
  ## | restriction | An object that describes who can access the view. To give all agents access to the view, omit this property
  ##
  ## The `restriction` object has the following properties.
  ##
  ## | Name | Comment
  ## | ---- | -------
  ## | type | Allowed values are "Group" or "User"
  ## | id   | The numeric ID of a single group or user
  ## | ids  | The numeric IDs of a single or more groups. Recommended for "Group" `type`
  ##
  ## If `type` is "Group", the `ids` property is the preferred method of specifying the group id or ids.
  ##
  ## #### Example Request Body
  ##
  ## ```js
  ## {
  ##   "view": {
  ##     "title": "Kelly's tickets",
  ##     "raw_title": "{{dc.tickets_assigned_to_kelly}}",
  ##     "description": "Tickets that are assigned to Kelly",
  ##     "active": true,
  ##     "position": 3,
  ##     "restriction": {
  ##       "type": "User",
  ##       "id": "213977756"
  ##     },
  ##     "all": [
  ##       {
  ##         "field": "status",
  ##         "operator": "less_than",
  ##         "value": "solved"
  ##       },
  ##       {
  ##         "field": "group_id",
  ##         "operator": "is",
  ##         "value": "24000932"
  ##       },
  ##       {
  ##         "field": "custom_fields_360011872073",
  ##         "operator": "is",
  ##         "value": "Canada"
  ##       },
  ##       ...
  ##     ],
  ##     "output": {
  ##       "columns": ["status", "requester", "assignee"],
  ##       "group_by": "assignee",
  ##       "group_order": "desc",
  ##       "sort_by": "status",
  ##       "sort_order": "desc"
  ##     }
  ##   }
  ## }
  ## ```
  ##
  ## #### View columns
  ##
  ## The `output` request parameter lets you specify what columns to include in the view in
  ## the agent interface. Example: `"output": {"columns": ["status", "description", "priority"]}`.
  ## The following table lists possible columns for views in the agent UI and the corresponding
  ## values in the `columns` array.
  ##
  ## For custom fields, specify the id of the custom field in the `columns` array.
  ##
  ## You can specify a total of 10 columns to a view.
  ##
  ## | View column title in UI     | Value                |
  ## |---------------------------- | -------------------- |
  ## | Assigned                    | `assigned`           |
  ## | Assignee                    | `assignee`           |
  ## | Due Date                    | `due_date`           |
  ## | Group                       | `group`              |
  ## | ID                          | `nice_id`            |
  ## | Updated                     | `updated`            |
  ## | Latest update by assignee   | `updated_assignee`   |
  ## | Assignee updated            | `updated_assignee`   |
  ## | Requester updated           | `updated_requester`  |
  ## | Updater                     | `updated_by_type`    |
  ## | Organization                | `organization`       |
  ## | Priority                    | `priority`           |
  ## | Requested                   | `created`            |
  ## | Requester                   | `requester`          |
  ## | Requester language          | `locale_id`          |
  ## | Satisfaction                | `satisfaction_score` |
  ## | Solved                      | `solved`             |
  ## | Status                      | `status`             |
  ## | Subject                     | `description`        |
  ## | Submitter                   | `submitter`          |
  ## | Ticket form                 | `ticket_form`        |
  ## | Type                        | `type`               |
  ## | Brand                       | `brand`              |
  ##
  ## #### View sorting
  ##
  ## You can group and sort items in the view by adding items to the `output` parameter:
  ##
  ## | Attribute                   | Description
  ## |-----------------------------| -----------
  ## | `group_by`, `sort_by`       | An item from the [View columns](#view-columns) table
  ## | `group_order`, `sort_order` | Either "asc" or "desc"
  allow_parameters :create, view: VIEW_PARAMETERS

  ## ### Update View
  ## `PUT /api/v2/views/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/{id}.json \
  ##  -d '{"view": {"title": "Roger Wilco II"}}' \
  ##  -H "Content-Type: application/json" \
  ##  -v -u {email}:{password} -X PUT
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "view": {
  ##     "id":   9873843,
  ##     "title": "Roger Wilco II",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## <a name="request-parameters-update"></a>
  ## #### Request Parameters
  ##
  ## The PUT request takes one parameter, a `view` object that lists the values to update. All properties are optional.
  ##
  ## **Note**: Updating a condition updates the containing array, clearing the other conditions. Include all your conditions when updating any condition.
  ##
  ## | Name        | Description
  ## | ----------- | -----------
  ## | title       | The title of the view
  ## | all         | An array of one or more conditions. A ticket must meet all the conditions to be included in the view. The PUT request replaces all existing conditions. See [Conditions](./views.html#conditions)
  ## | any         | An array of one or more conditions. A ticket must meet any of them to be included in the view. At least one `all` condition must be defined with the `any` conditions. The PUT request replaces all existing `any` conditions. See [Conditions](./views.html#conditions)
  ## | active      | Allowed values are true or false. Determines if the view is displayed or not
  ## | output      | An object that specifies the columns to display. Example: `"output": {"columns": ["status", "description," "priority"]}`. See [View columns](#view-columns)
  ## | restriction | An object that describes who can access the view. To give all agents access to the view, omit this property
  ##
  ## The `restriction` object has the following properties.
  ##
  ## | Name | Comment
  ## | ---- | -------
  ## | type | Allowed values are "Group" or "User"
  ## | id   | The numeric ID of a single group or user
  ## | ids  | The numeric IDs of a single or more groups. Recommended for "Group" `type`
  ##
  ## If `type` is "Group", the `ids` property is the preferred method of specifying the group id or ids.
  ##
  ## You can also update how items are sorted and grouped. See [View sorting](#view-sorting) in Create View.
  ##
  ## #### Example Request Body
  ##
  ## ```js
  ## {
  ##   "view": {
  ##     "title": "Code red tickets",
  ##     "restriction": {
  ##       "type": "Group",
  ##       "ids": [10052, 10057, 10062, 10002]
  ##     },
  ##     "all": [
  ##       {
  ##         "field": "priority",
  ##         "operator": "is",
  ##         "value": "urgent"
  ##       }
  ##     ],
  ##     "output": {
  ##       "columns": ["status", "requester", "assignee", "updated"]
  ##     }
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update, id: Parameters.bigid, view: VIEW_PARAMETERS

  ## ### Delete View
  ## `DELETE /api/v2/views/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/{id}.json \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```

  ## ### Bulk Delete Views
  ## `DELETE /api/v2/views/destroy_many.json`
  ##
  ## Deletes the views corresponding to the provided list of IDs.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/destroy_many.json?ids=1,2,3 \
  ##   -v -u {email}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  ## <a name="request-parameters-bulk-delete"></a>
  ## #### Request Parameters
  ##
  ## The DELETE request takes one parameter, an `ids` object that lists the views to delete.
  ##
  ## | Name | Description
  ## | ---- | -----------
  ## | ids  | The IDs of the views to delete
  ##
  ## #### Example Request Body
  ##
  ## ```js
  ## {
  ##   "ids": "25,23,27,22"
  ## }
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids
  def destroy_many
    return head :bad_request unless many_ids?

    bulk_views.each { |view| view.soft_delete(validate: false) }

    head :no_content
  end

  ## ### Execute View
  ## `GET /api/v2/views/{id}/execute.json`
  ##
  ## Returns the column titles and the rows of the specified view.
  ##
  ## The `columns` array lists the view's column titles.
  ##
  ## The `rows` array lists the values of each column for each ticket.
  ## Though not dislayed in the view, a partial ticket object is included
  ## with each row object.
  ##
  ## **Note**: To get the full ticket objects for a specified view, use
  ## [List Tickets from a View](#list-tickets-from-a-view).
  ##
  ## This endpoint is rate limited to 5 requests per minute, per view, per agent.
  ##
  ## The view execution system is designed for periodic rather than
  ## high-frequency API usage. In particular, views called very frequently
  ## may be cached by Zendesk. This means that the API client will still
  ## receive a result, but that result may have been computed at any time
  ## within the last 10 minutes.
  ##
  ## Zendesk recommends using the Incremental Ticket Export endpoint to get
  ## the latest changes. You can call it more often, and it returns all the
  ## tickets that changed since the last poll. For details and rate limits,
  ## see [Incremental Exports](./incremental_export).
  ##
  ## View output sorting can be controlled by passing the `sort_by` and `sort_order` parameters in the
  ## format described in the table in [Previewing Views](#previewing-views).
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/{id}/execute.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## With sort options:
  ##
  ##  ```bash
  ## curl 'https://{subdomain}.zendesk.com/api/v2/views/{id}/execute.json?sort_by=id&sort_order=desc' \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "view": {
  ##     "id": 25,
  ##     "url": ...
  ##   },
  ##   "rows": [
  ##     {
  ##       "ticket": { ... },
  ##       "locale": "en-US",
  ##       "group": 1,
  ##       ...
  ##     },
  ##     ...
  ##   ],
  ##   "columns": [
  ##     {
  ##       "id": "locale",
  ##       "title": "Locale"
  ##     },
  ##     {
  ##       "id": 5,
  ##       "title": "Account",
  ##       "url": ...
  ##     },
  ##     ...
  ##   ],
  ##  "groups": [ ... ]
  ## }
  ## ```
  allow_parameters :execute, EXECUTE_PARAMETERS
  require_oauth_scopes :execute, scopes: [:read], arturo: true
  def execute
    # prevents overload Occam under burst of traffic
    # App/API traffic is cached for 5 seconds.
    # Lotus internal requests are allowed uncached.
    if current_account.has_views_cache_control_header? &&
       !lotus_internal_request?
      response.headers['Vary'] =
        Zendesk::Rules::ControllerSupport::ViewsRateLimiterSupport::VARY_HEADERS_FOR_CACHE
      expires_in(5.seconds)
    end

    includes << :view

    with_occam_error_handling do
      render json: row_presenter.present(view_tickets)
    end
  rescue Zendesk::Rules::RuleExecuter::UnprocessableEntity
    render status: :unprocessable_entity, json: {
      error:       'UnprocessableEntity',
      description: 'View could not be executed. Please check if view ' \
                   'definition is valid.'
    }
  rescue Zendesk::Rules::RuleExecuter::UnknownOccamError
    render status: :service_unavailable, json: {
      error:       'ServiceUnavailable',
      description: 'View service unavailable. Please try again later.'
    }
  end

  ## ### List Tickets from a View
  ## `GET /api/v2/views/{id}/tickets.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## You can pass in any combination of the following optional filters, which will
  ## override the view settings.
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | sort_by    | string  | An item from the [View columns](#view-columns) table
  ## | sort_order | string  | One of "asc" or "desc". Defaults to "asc" for alphabetical and position sort, "desc" for all others
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/{id}/tickets.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Responses
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "tickets": [
  ##     {
  ##       "id":      35436,
  ##       "subject": "Help I need somebody!",
  ##       ...
  ##     },
  ##     {
  ##       "id":      20057623,
  ##       "subject": "Not just anybody!",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :tickets, TICKETS_PARAMETERS
  require_oauth_scopes :tickets, scopes: [:read], arturo: true
  def tickets
    # To allow sort_by on the tickets we need to unset the group from the view.
    # See comment inside #view_tickets.
    view.output.group = nil if params[:sort_by]

    with_occam_error_handling do
      render json: ticket_presenter.present(view_tickets)
    end
  end

  # Show Ticket from a View
  # `GET /api/v2/views/{id}/next.json`
  #
  # Allowed For
  #
  #  * Agents
  #
  # Using curl:
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/views/{id}/next.json \
  #   -v -u {email}:{password}
  # ```
  #
  # Example Responses
  #
  # Next ticket from view
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "ticket": {
  #     {
  #       "id":      35436,
  #       "subject": "My printer is on fire!",
  #       ...
  #     }
  #   }
  # }
  # ```
  #
  # View is empty
  #
  # ```http
  # Status: 200 OK
  #
  # {}
  # ```
  #
  # View is currently being refreshed, please wait and retry later
  # Status: 204 No Content
  # {}
  # ```
  allow_parameters :next, NEXT_PARAMETERS
  require_oauth_scopes :next, scopes: [:read], arturo: true
  def next
    debug          = params['debug']
    start_time     = Time.now if debug
    ticket, status = self.class.response_for_next(view, current_user, params)
    end_time       = Time.now if debug
    ticket_json    = ticket_presenter.present(ticket) unless ticket.is_a?(Hash)

    request_span&.set_tag("zendesk.sbr_view", sbr_view?)

    if debug && ticket_json.is_a?(Hash)
      ticket_json['performance'] = (end_time - start_time) * 1000
    end

    render json: ticket_json || ticket, status: status

    update_recent_tickets(ticket)
  end

  ## ### View Counts and Caching
  ##
  ## The view count endpoints lets you estimate how many tickets remain
  ## in a view without having to retrieve the entire view. They're designed
  ## to help estimate view size. From a business perspective, accuracy becomes
  ## less relevant as view size increases.
  ##
  ## To ensure quality of service, these counts are cached more heavily as the
  ## number of tickets in a view grows. For a view with thousands of tickets,
  ## you can expect the count to be cached for 60-90 minutes. As a result,
  ## the count may not reflect the actual number of tickets in your view.
  ##
  ## View counts are represented as JSON objects with the following attributes:
  ##
  ## | Name            | Type        | Comment
  ## | --------------- | ------------| -------
  ## | view_id         | integer     | The id of the view
  ## | url             | string      | The API url of the count
  ## | value           | integer     | The cached number of tickets in the view. Can also be null if the system is loading and caching new data. Not to be confused with 0 tickets
  ## | pretty          | string      | A pretty-printed text approximation of the view count
  ## | fresh           | boolean     | false if the cached data is stale and the system is still loading and caching new data
  ## | active          | boolean     | Only active views if true, inactive views if false, all views if null.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "view_count": {
  ##     "view_id": 25,
  ##     "url":     "https://company.zendesk.com/api/v2/views/25/count.json",
  ##     "value":   719,
  ##     "pretty":  "~700",
  ##     "fresh":   true
  ##   }
  ## }
  ## ```
  ##
  ## If the response indicates that the cached data is not fresh (`"fresh": false`),
  ## then the system is loading and caching new data. During this time, the
  ## count will be null (`"value": null`) and the text approximation will
  ## be "..." (`"pretty": "..."`). Make the request again after a short wait.
  ##
  ## ### Get View Counts
  ## `GET /api/v2/views/count_many.json?ids={view_id},{view_id}`
  ##
  ## Calculates the number of tickets each view will return. Accepts up to 20 view ids per request.
  ##
  ## Only returns values for personal and shared views accessible to the user performing
  ## the request.
  ##
  ## This endpoint is rate limited to 6 requests every 5 minutes.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/count_many.json?ids={view_id} \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "view_counts": [
  ##     {
  ##       "view_id": 25,
  ##       "url":     "https://company.zendesk.com/api/v2/views/25/count.json",
  ##       "value":   719,
  ##       "pretty":  "~700",
  ##       "fresh":   true
  ##     },
  ##     {
  ##       "view_id": 78,
  ##       "url":     "https://company.zendesk.com/api/v2/views/78/count.json",
  ##       "value":   null,
  ##       "pretty":  "...",
  ##       "fresh":   false
  #        "error":   "LimitedViewExecution",
  #        "description": "The view has limited functionality until conditions are updated, including no counts"
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :count_many, SHOW_MANY_PARAMETERS
  require_oauth_scopes :count_many, scopes: [:read], arturo: true
  def count_many
    with_occam_error_handling(force_rescue: true) do
      render json: count_presenter.present(view_counts)
    end
  end

  ## ### Get View Count
  ## `GET /api/v2/views/{id}/count.json`
  ##
  ## Returns the ticket count for a single view.
  ##
  ## This endpoint is rate limited to 5 requests per minute, per view, per agent.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/{id}/count.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "view_count": {
  ##     "view_id": 25,
  ##     "url":     "https://company.zendesk.com/api/v2/views/25/count.json",
  ##     "value":   719,
  ##     "pretty":  "~700",
  ##     "fresh":   true
  #      "error":   "LimitedViewExecution",
  #      "description": "The view has limited functionality until conditions are updated, including no counts"
  ##   }
  ## }
  ## ```
  allow_parameters :count, id: ID_OR_INCOMING_PARAMETERS
  require_oauth_scopes :count, scopes: [:read], arturo: true
  def count
    cached_count = view_count
    Rails.logger.info "Count value: #{cached_count.value.inspect} fresh: #{!cached_count.is_updating?}"

    render json: count_presenter.present(cached_count)
  end

  ## ### Export View
  ## `GET /api/v2/views/{id}/export.json`
  ##
  ## Returns the csv attachment of the specified view if possible.
  ## Enqueues a job to produce the csv if necessary.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/{id}/export.json \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "export": {
  ##     "view_id": 25,
  ##     "status": "starting"
  ##   }
  ## }
  ## ```
  allow_parameters :export, id: Parameters.bigid
  require_oauth_scopes :export, scopes: [:read], arturo: true
  def export
    render json: {export: {view_id: view.id, status: exported_csv_status}}
  end

  ## ### Search Views
  ## `GET /api/v2/views/search.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available Parameters
  ##
  ## **Required**
  ##
  ## | Name  | Type    | Comment
  ## | ----- | ------- | -------
  ## | query | string  | Query string used to find all views with matching title
  ##
  ## **Optional**
  ##
  ##  You can use any combination of the following optional parameters:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | access     | string  | Only views with given access. May be "personal", "shared", or "account"
  ## | active     | boolean | Only active views if true, inactive views if false
  ## | group_id   | integer | Only views belonging to given group
  ## | sort_by    | string  | Possible values are "alphabetical", "created_at", "updated_at", and "position". If unspecified, the views are sorted by relevance
  ## | sort_order | string  | One of "asc" or "desc". Defaults to "asc" for alphabetical and position sort, "desc" for all others
  ##
  ## **Example queries**
  ##
  ## * `/api/v2/views/search.json?query=unassigned`
  ## * `/api/v2/views/search.json?query=sales&group_id=25789188`
  ## * `/api/v2/views/search.json?query=dev&access=shared`
  ##
  ## #### Sideloads
  ##
  ## The following sideloads are supported:
  ##
  ## | Name             | Will sideload
  ## | ---------------- | -------------
  ## | app_installation | The app installation that requires each view, if present
  ## | permissions      | The permissions for each view
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/views/search.json?query=unsolved \
  ##   -v -u {email}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "views": [
  ##     {
  ##       "id": 25,
  ##       "title": "Your unsolved tickets",
  ##       "active": true,
  ##       "position": 3,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     },
  ##     {
  ##       "id": 23,
  ##       "title": "All unsolved tickets",
  ##       "active": true,
  ##       "position": 7,
  ##       "execution": { ... },
  ##       "conditions": { ... },
  ##       "restriction": { ... }
  ##     }
  ##   ],
  ##   "count": 2,
  ##   "next_page": null,
  ##   "previous_page": null
  ## }
  ## ```
  allow_parameters :search, SEARCH_PARAMETERS
  require_oauth_scopes :search, scopes: [:read], arturo: true
  def search
    return head :bad_request unless params[:query].present?

    render json: search_presenter.present(search_result.rules)
  end

  # h3 List groups associated with views
  # `GET /api/v2/views/groups.json`
  #
  # Lists active groups associated with at least one view.
  #
  # h4 Allowed For:
  #
  #  * Agents
  #
  # h4 Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/views/groups.json \
  #   -v -u {email}:{password}
  # ```
  #
  # h4 Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "groups": [
  #     {
  #       "name":       "DJs",
  #       "created_at": "2015-05-13T00:07:08Z",
  #       "updated_at": "2016-07-22T00:11:12Z",
  #       "id":         211
  #     },
  #     {
  #       "name":       "MCs",
  #       "created_at": "2015-08-26T00:07:08Z",
  #       "updated_at": "2016-05-13T00:07:08Z",
  #       "id":         122
  #     },
  #     ...
  #   ]
  # }
  # ```
  allow_parameters :groups, {}
  require_oauth_scopes :groups, scopes: [:read], arturo: true
  def groups
    render json: group_presenter.present(view_groups)
  end

  allow_parameters :definitions, {}
  require_oauth_scopes :definitions, scopes: [:read]
  def definitions
    render json: definitions_presenter.present(view_definitions)
  end

  def self.response_for_next(view, user, params = {})
    view.output = view.output.merge(params)
    next_ticket = view.next_ticket(user, params)

    next_ticket ? [next_ticket, :ok] : [{}, :ok]
  rescue Zendesk::Rules::View::ViewQueue::Locked # currently refilling
    [{}, :no_content]
  end

  # h3 Filter View
  # `GET /api/v2/views/{id}/filter.json`
  #
  # Filtering a view gets the tickets in the view that match the skills of the
  # authenticated agent and the conditions of the view.
  #
  # View output sorting can be controlled by passing the `sort_by` and
  # `sort_order` parameters in the format described in the table under
  # [view previewing](#previewing-views).
  #
  # h4 Allowed For
  #
  #  * Agents
  #
  # h4 Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/views/{id}/filter.json \
  #   -v -u {email}:{password}
  # ```
  #
  # With sort options:
  #
  #  ```bash
  # curl 'https://{subdomain}.zendesk.com/api/v2/views/{id}/filter.json?
  # sort_by=id&sort_order=desc' \ -v -u {email}:{password}
  # ```
  #
  # h4 Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "view": {
  #     "id": 25,
  #     "url": ...
  #   },
  #   "rows": [
  #     {
  #       "ticket": { ... },
  #       "locale": "en-US",
  #       "group": 1,
  #       ...
  #     },
  #     ...
  #   ],
  #   "columns": [
  #     {
  #       "id": "locale",
  #       "title": "Locale"
  #     },
  #     {
  #       "id": 5,
  #       "title": "Account",
  #       "url": ...
  #     },
  #     ...
  #   ],
  #  "groups": [ ... ],
  #  "insuffcient_results": false
  # }
  # ```
  allow_parameters :filter, VIEW_FILTER_PARAMETERS
  require_oauth_scopes :filter, scopes: [:read], arturo: true
  def filter
    includes << :view
    with_views_error_handling do
      render json: filtered_view_rows_presenter.present(filtered_tickets)
    end
  end

  protected

  def presenter
    @presenter ||= begin
      Api::V2::Rules::ViewPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def search_presenter
    @search_presenter ||= begin
      Api::V2::Rules::ViewPresenter.new(
        current_user,
        url_builder: self,
        highlights:  search_result.highlights,
        includes:    includes
      )
    end
  end

  def row_presenter
    @row_presenter ||= begin
      Api::V2::Rules::ViewRowsPresenter.new(
        current_user,
        url_builder:                self,
        view:                       view,
        includes:                   includes,
        shared_tickets_url_builder: shared_tickets_url_builder
      )
    end
  end

  def shared_tickets_url_builder
    Zendesk::Tickets::UrlBuilder.new(
      account:   current_account,
      for_agent: current_user.is_agent?
    )
  end

  def filtered_view_rows_presenter
    @filtered_view_rows_presenter ||= begin
      Api::V2::Rules::FilteredViewRowsPresenter.new(
        current_user,
        url_builder:                self,
        view:                       view,
        includes:                   includes,
        shared_tickets_url_builder: shared_tickets_url_builder
      )
    end
  end

  def filtered_tickets
    if apply_skill_based_filter?
      match_tickets(params[:per_page] || SKILL_BASED_FILTER_PER_PAGE)
    else
      view_tickets
    end
  end

  def count_presenter
    @count_presenter ||= begin
      Api::V2::Rules::ViewCountPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def ticket_presenter
    @ticket_presenter ||= begin
      Api::V2::Tickets::TicketPresenter.new(
        current_user,
        url_builder:       self,
        includes:          includes,
        audit_count_since: params[:last_viewed],
        comment_presenter: comment_presenter
      )
    end
  end

  def comment_presenter
    @comment_presenter ||= begin
      Api::V2::Tickets::GenericCommentPresenter.new(
        current_user,
        url_builder: self
      )
    end
  end

  def group_presenter
    @group_presenter ||= begin
      Api::V2::GroupPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def definitions_presenter
    @definitions_presenter ||= begin
      Api::V2::Rules::ViewDefinitionsPresenter.new(
        current_user,
        url_builder: self,
        includes:    includes
      )
    end
  end

  def views
    @views ||= scope.paginate(pagination)
  end

  def view_definitions
    @view_definitions ||= begin
      Zendesk::Rules::Definitions.new(
        account:   current_account,
        user:      current_user,
        rule_type: :view
      )
    end
  end

  def view_ids
    @view_ids ||= many_ids || params[:views].map { |view| view[:id] }
  end

  def bulk_views
    scope.find(view_ids)
  end

  def view_groups
    @view_groups ||= begin
      view_association = if current_account.has_multiple_group_views_reads?
        :groups_views
      else
        :views
      end
      paginate(current_account.groups.joins(view_association).distinct)
    end
  end

  def position_updates
    @position_updates ||= begin
      params[:views].each_with_object([]) do |view_params, updates|
        next unless view_params[:position].present?

        updates << view_params.slice(:id, :position)
      end
    end
  end

  def positionless_updates
    @positionless_updates ||= begin
      params[:views].each_with_object([]) do |view_params, updates|
        next unless view_params.key?(:active)

        updates << view_params.slice(:id, :active)
      end
    end
  end

  def context
    @context ||= begin
      Zendesk::RuleSelection::Context::View.
        new(current_user, params.merge(pagination))
    end
  end

  def scope
    @scope ||= Zendesk::RuleSelection::Scope.for_context(context)
  end

  def search_result
    @search_result ||= Zendesk::RuleSelection::Search.for_context(context)
  end

  def view
    @view ||= begin
      case params[:id]
      when View::INCOMING  then View.incoming(current_user)
      when View::MY        then View.my(current_user)
      when View::MY_GROUPS then View.my_groups(current_user)
      else scope.find(params[:id])
      end
    end
  end

  def compact_views
    @compact_views ||= begin
      shared_views = begin
        Zendesk::RuleSelection::Scope.viewable(
          Zendesk::RuleSelection::Context::View.new(
            current_user,
            access:  'shared',
            active:  true,
            sort_by: 'user_setting'
          )
        ).limit(current_account.max_views_for_display)
      end

      personal_views = begin
        Zendesk::RuleSelection::Scope.viewable(
          Zendesk::RuleSelection::Context::View.new(
            current_user,
            access: 'personal',
            active: true
          )
        ).limit(current_account.max_private_views_for_display)
      end

      shared_views + personal_views
    end
  end

  def view_count
    ::CachedRuleTicketCount.lookup(
      view, current_user, caller: retrieve_caller, refresh: :delayed
    )
  end

  def occam_view_counts
    deleted   = many_ids.try(:include?, 'deleted')
    suspended = many_ids.try(:include?, 'suspended')

    Zendesk::Rules::RuleExecuter::CountMany.new(
      views:             scope.where(id: many_ids),
      account:           current_account,
      user:              current_user,
      include_deleted:   deleted,
      include_suspended: suspended,
      params: params.merge(
        request_origin: request_origin_details,
        caller: DEFAULT_REQUEST_SOURCE
      )
    ).perform
  end

  def view_counts
    return [] unless many_ids?

    occam_view_counts
  end

  def view_tickets(includes: DEFAULT_INCLUDES)
    raise ActiveRecord::RecordNotFound, 'inactive view' unless view.is_active

    view.request_origin = request_origin_details
    hanlon_cache        = params.delete(:hanlon_cache)
    simulate_slow       = params.delete(:simulate_slow)
    paginate            = params.delete(:paginate) { true }
    # Allows passing group_by, sort_by, etc. as params on execute.
    #
    # Note:
    # There won't work all the time, because if the view has a group, it will
    # still override the sort_by from the params (see
    # ExecutionOptions#ordering_array). This is why in #tickets we explicitly
    # unset the group from the view to allow sort_by to work. See
    # https://zendesk.atlassian.net/browse/API-809.
    view.output = view.output.merge(params)

    request_span&.set_tag("zendesk.sbr_view", sbr_view?)
    Zendesk::SliRequestTagger.set_tag('view_sla', sla_view?)

    options = {
      output_type: :table,
      include:     includes,
      paginate:    paginate
    }

    if options[:paginate]
      options.reverse_merge!(pagination)
    else
      options[:limit] = params[:per_page].presence
    end

    merge_view_caching_options(options, retrieve_caller) # for logging
    options[:simulate_slow]   = simulate_slow
    options[:hanlon_cache]    = hanlon_cache
    options[:timeout]         = current_account.settings.views_find_timeout

    view.find_tickets(current_user, options).tap do |t|
      if options[:paginate] && t.out_of_range?
        raise(ActiveRecord::RecordNotFound, 'page out of range')
      end
    end
  end

  def exported_csv_status
    # ZD#425181 View CSV Exports should always include the status
    ViewCsvJob.enqueue(
      current_account.id, current_user.id, view.id, default_columns: %i[status]
    )

    :enqueued
  rescue Resque::ThrottledError
    :throttled
  end

  # Store ticket in "recent tickets" list
  def update_recent_tickets(ticket)
    recent_ticket_manager.add_ticket(ticket) if real_ticket?(ticket)
  end

  def inherited_controller?
    self.class != Api::V2::Rules::ViewsController
  end

  private

  def initial_view
    scope.find(params[:id])
  end

  def real_ticket?(ticket)
    ticket.present? && ticket.is_a?(Ticket) && ticket.nice_id.present?
  end

  def update_view(view_id, view_params)
    Zendesk::Rules::Initializer.new(
      current_account,
      scope,
      {id: view_id, view: view_params},
      :view
    ).rule.tap do |updated_view|
      updated_view.current_user = current_user

      updated_view.save!(validate: updated_view.is_active?)
    end
  end

  def apply_skill_based_filter?
    skill_based_filtered_view? && current_account.has_skill_based_view_filters?
  end

  def skill_based_filtered_view?
    current_account.settings.skill_based_filtered_views.include?(view.id)
  end

  def with_occam_error_handling(force_rescue: false)
    yield
  rescue *OCCAM_ERRORS => e
    if !force_rescue && !current_account.has_views_api_rescue_occam_errors?
      raise
    end

    tags = %W[error:#{e.class.name.underscore} action:#{action_name}]

    if e.respond_to?(:wrapped_exception)
      tags << "wrapped_exception:#{e.wrapped_exception.class.name.underscore}"
    end

    Rails.application.config.statsd.client.
      increment('rules.view_api_occam_error', tags: tags)

    render status: :service_unavailable, json: {
      error:       'ServiceUnavailable',
      description: 'View service unavailable. Please try again later.'
    }
  rescue JohnnyFive::CircuitTrippedError
    render status: :service_unavailable, json: {
      error:       'ServiceUnavailable',
      description: "View could not be processed, try again later."
    }
  end

  def request_origin_details
    @request_origin_details ||= Zendesk::RequestOriginDetails.new(request.env)
  end

  def validate_count_many_api_limit
    return if exclude_from_count_many_api_limit?

    raise ZendeskApi::Controller::Errors::TooManyValuesError.new('ids', COUNT_MANY_API_LIMIT) if invalid_requested_count?
  end

  def exclude_from_count_many_api_limit?
    Rails.env.development? ||
      request_origin_details.lotus? ||
      request_origin_details.zendesk_mobile_client? ||
      !current_account.has_occam_count_many_api_limit?
  end

  def invalid_requested_count?
    COUNT_MANY_API_LIMIT < (many_ids&.length || 0)
  end

  ##
  # Assigning @rate_limiter is required since it
  # may be used by the exception handler during a
  # request cycle.
  #
  def rate_limit_find
    @rate_limiter = rate_limiter(:view_find, params[:id])
    @rate_limiter.throttle!
  end

  ##
  # @see #rate_limit_find
  #
  def rate_limit_play
    @rate_limiter = rate_limiter(:view_play, params[:id])
    @rate_limiter.throttle!
  end

  ##
  # @see #rate_limit_find
  #
  def rate_limit_count
    @rate_limiter = rate_limiter(:view_count, params[:id])
    @rate_limiter.throttle!
  end

  ##
  # @see #rate_limit_find
  #
  def rate_limit_count_many
    @rate_limiter = rate_limiter(:count_many)
    @rate_limiter.throttle!
  end

  ##
  # @see #rate_limit_find
  #
  def rate_limit_tickets
    @rate_limiter = rate_limiter(:view_tickets, params[:id])
    @rate_limiter.throttle!
  end

  def sbr_view?
    view.definition.conditions_all.map(&:source).include? "skills_match"
  end

  def sla_view?
    view.output.columns.include?("sla_next_breach_at")
  end

  def request_span
    request.env[Datadog::Contrib::Rack::TraceMiddleware::RACK_REQUEST_SPAN] ||
      Datadog.tracer.active_span
  end

  def retrieve_caller
    request_origin_details.rule_admin? ? SOURCE_SUPPORT_RULE_ADMIN : DEFAULT_REQUEST_SOURCE
  end

  def verify_views_availability
    return true unless current_account.has_views_disable_all_features?

    raise ViewsServiceUnavailableError
  end

  def handle_views_unavailable
    Rails.logger.error '[views-kill-switch] Not processing the request'

    render status: :service_unavailable, json: {
      error:       'ServiceUnavailable',
      description: 'View service unavailable. Please try again later.'
    }
  end
end
