class Api::V2::Rules::BaseController < Api::V2::BaseController
  include Zendesk::ProductLimit::ControllerSupport

  after_action :record_admin_ui_request

  def create
    rule.save!

    render(
      json:     presenter.present(rule),
      status:   :created,
      location: presenter.url(rule)
    )
  end

  def update
    rule.save!(validate: validate?)

    render json: presenter.present(rule)
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    rule.soft_delete(validate: false)

    default_delete_response
  end

  protected

  def rule
    @rule ||=
      initializer.rule.tap { |rule| rule.current_user = current_user }
  end
  alias resource rule

  def max_per_page
    1_000
  end

  def initializer
    @initializer ||= begin
      Zendesk::Rules::Initializer.new(
        current_account,
        scope,
        params,
        presenter.model_key
      )
    end
  end

  def validate?
    rule.is_active? || !(rule.is_active_changed? && rule.changed.one?)
  end

  private

  def statsd_client
    @statsd_client ||= begin
      Zendesk::StatsD::Client.new(namespace: %w[rule-admin api])
    end
  end

  def record_admin_ui_request
    return unless request.headers['X-Zendesk-Rule-Admin-Version'].present?

    statsd_client.increment(
      'rule.admin.ui.api.requests',
      tags: [
        "controller:#{controller_name}",
        "action:#{action_name}",
        "version:#{request.headers['X-Zendesk-Rule-Admin-Version']}",
        response.successful? ? 'success' : 'error',
        "status:#{response.status}"
      ]
    )
  end
end
