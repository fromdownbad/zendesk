## ### List Followers for a Ticket
##
## `GET /api/v2/tickets/{id}/followers`
##
## Returns any users who follow the ticket.
##
## #### Availability
##
## The [CCs and Followers](https://support.zendesk.com/hc/en-us/articles/203690846)
## feature must be enabled in Zendesk Support.
##
## #### Allowed For
##
##  * Agents
##
## #### Using curl
##
## ```bash
## curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}/followers \
##   -v -u {email_address}:{password}
## ```
##
## #### Example Response
##
## ```http
## Status: 200
##
## {
##   "users": [
##     {
##       "id": "223443",
##       "name": "Johnny Agent",
##       ...
##     },
##     {
##       "id": "8678530",
##       "name": "Peter Admin",
##       ...
##     }
##   ]
## }
## ```
class Api::V2::FollowersController < Api::V2::CollaboratorsController
  allow_pigeon_user only: %i[index]
  require_that_user_can! :view, :ticket

  allow_parameters :index, ticket_id: Parameters.bigid
  def index
    render json: presenter.present(ticket.followers)
  end
end
