class Api::V2::CrmDataController < Api::V2::BaseController
  before_action :require_enabled_integration!

  ## ### Show User CRM Information
  ## `GET /api/v2/users/{id}/crm_data.json`
  ##
  ## Returns User's CRM information (Salesforce or Sugar CRM) as defined in Settings / Extensions / CRM.
  ##
  ## The end point will show the CRM data on a given user. If the data for this user is stale, a new job
  ## will get kicked off to update the data, and a separate response is given.
  ##
  ## #### Availability
  ##
  ##  * Accounts with a CRM integration enabled
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/crm_data.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response if data is recent
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "crm_data": {
  ##     "id":              14215,
  ##     "status":          "ok",
  ##     "type":            "sugar_crm_data",
  ##     "records":         []
  ##     "created_at":      "2012-02-02T04:31:29Z",
  ##     "updated_at":      "2012-03-10T14:51:08Z"
  ##   }
  ## }
  ## ```
  ##
  ## #### Example Response if data is stale
  ##
  ## ```http
  ## Status: 202 Accepted
  ##
  ## {
  ##   "crm_data": {
  ##     "id":              14215,
  ##     "status":          "pending",
  ##     "type":            "sugar_crm_data",
  ##     "records":         []
  ##     "created_at":      "2012-02-02T04:31:29Z",
  ##     "updated_at":      "2012-03-10T14:51:08Z"
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :show, id: Parameters.bigid, ticket_id: Parameters.bigid, user_id: Parameters.bigid
  def show
    if crm_data.needs_sync?
      crm_data.start_sync(crm_object)
      render json: presenter.present(crm_data), status: :accepted, location: status_api_v2_user_crm_data_url(user, url_options)
    else
      render json: presenter.present(crm_data), status: :ok
    end
  end

  ## ### Show Status on User's CRM Data Update
  ## `GET /api/v2/users/{id}/crm_data/status.json`
  ##
  ## #### Availability
  ##
  ##  * Accounts with a CRM integration enabled
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/crm_data/status.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Description
  ##
  ## This end point gives the state of the CRM data associated with the user. There are three possible responses
  ## as elaborated below.
  ##
  ## #### Example Response when Retrieving Data
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/users/{id}/crm_data/status.json
  ##
  ## {
  ##   "crm_data": {
  ##     "id":              14215,
  ##     "status":          "pending",
  ##     "type":            "sugar_crm_data",
  ##     "records":         []
  ##     "created_at":      "2012-02-02T04:31:29Z",
  ##     "updated_at":      "2012-03-10T14:51:08Z"
  ##   }
  ## }
  ## ```
  ##
  ## #### Example Response when Data Retrieval Failed
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "crm_data": {
  ##     "id":              14215,
  ##     "status":          "errored",
  ##     "type":            "sugar_crm_data",
  ##     "records":         []
  ##     "created_at":      "2012-02-02T04:31:29Z",
  ##     "updated_at":      "2012-03-10T14:51:08Z"
  ##   }
  ## }
  ## ```
  ##
  ## #### Example Response when Data Retrieval Succeeded
  ##
  ## ```http
  ## Status: 303 See Other
  ## Location: https://{subdomain}.zendesk.com/api/v2/users/{id}/crm_data/status.json
  ##
  ## {
  ##   "crm_data": {
  ##     "id":              14215,
  ##     "status":          "ok",
  ##     "type":            "sugar_crm_data",
  ##     "records":         []
  ##     "created_at":      "2012-02-02T04:31:29Z",
  ##     "updated_at":      "2012-03-10T14:51:08Z"
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :status, id: Parameters.bigid, ticket_id: Parameters.bigid, user_id: Parameters.bigid
  require_oauth_scopes :status, scopes: [:read], arturo: true
  def status
    if crm_data.sync_errored?
      render json: presenter.present(crm_data), status: :ok
    elsif crm_data.sync_pending?
      render json: presenter.present(crm_data), status: :ok, location: status_api_v2_user_crm_data_url(user, url_options)
    else
      render json: presenter.present(crm_data), status: :see_other, location: api_v2_user_crm_data_url(user, url_options)
    end
  end

  allow_parameters :destroy, id: Parameters.bigid, ticket_id: Parameters.bigid, user_id: Parameters.bigid
  def destroy
    delete_single_cached_crm_data
    head :no_content
  end

  allow_parameters :all_active, id: Parameters.bigid, ticket_id: Parameters.bigid, user_id: Parameters.bigid
  def all_active
    delete_single_cached_crm_data
    enqueue_job_with_status(CrmDataBulkDeleteJob, account_id: crm_object.account_id)
    head :no_content
  end

  require_oauth_scopes :url_options, scopes: [:read], arturo: true
  def url_options
    if salesforce_configuration_enabled?
      { format: "json", ticket_id: ticket.nice_id }
    else
      { format: "json" }
    end.merge(host: request.host, protocol: request.protocol)
  end

  protected

  def delete_single_cached_crm_data
    crm_data.delete_cache if crm_data
  end

  def presenter
    @presenter ||= Api::V2::CrmDataPresenter.new(current_user, url_builder: self)
  end

  def user
    @user ||= current_account.users.find(params[:user_id])
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  def salesforce_configuration_enabled?
    params[:ticket_id].present? && current_account.salesforce_configuration_enabled?
  end

  def crm_object
    salesforce_configuration_enabled? ? ticket : user
  end

  def crm_data
    @crm_data ||= crm_object.crm_data(true)
  end

  def require_enabled_integration!
    deny_access unless current_account.crm_integration_enabled?
  end
end
