require 'zendesk/search'
require 'zendesk/offset_pagination_limiter'

class Api::V2::UsersController < Api::V2::BaseController
  include Zendesk::Users::ControllerSupport
  include Zendesk::Search::ControllerSupport

  # TODO: Remove account_fraud_service access to this controller as part of PA-1392
  allow_subsystem_user :account_fraud_service, only: [:index, :count]
  allow_subsystem_user :chat,       only: %i[show search create]
  allow_subsystem_user :app_market, only: [:show]
  allow_subsystem_user :billing, only: [:show]
  allow_subsystem_user :collaboration, only: [:show, :create_or_update, :search, :show_many]
  allow_subsystem_user :metropolis, only: [:create, :update, :create_or_update]
  allow_subsystem_user :hawaii, only: [:create_or_update, :search]
  allow_subsystem_user :embeddable, only: [:create_or_update, :search]
  allow_subsystem_user :outbound, only: [:index, :count]
  allow_subsystem_user :knowledge_api, only: [:show_many]
  allow_subsystem_user :profile_service, only: [:show, :show_many, :create, :update]
  allow_subsystem_user :zdp_metadata, only: [:show]
  allow_zopim_user only: [:index, :count, :show, :update, :destroy, :merge]

  allow_cors_from_zendesk_external_domains :autocomplete
  allow_cors_from_zendesk_external_domains :show_many

  require_that_user_can! :list,   User,      only: [:index, :count]
  require_that_user_can! :view,   :user,     only: :show
  require_that_user_can! :create, :new_user, only: :create
  require_that_user_can! :edit,   :user,     only: :update
  require_that_user_can! :delete, :user,     only: :destroy
  require_that_user_can! :search, User,      only: :search

  skip_before_action :enforce_support_is_active!
  skip_before_action :require_agent!, only: :merge_self
  skip_before_action :verify_authenticity_token, only: :merge_self # needed for end-users to merge themselves via API
  skip_before_action :enforce_request_format, only: :update, if: :msie?

  prepend_around_action :forbid_nil_parameters, only: :create_many
  before_action :restrict_bulk_import_from_risky_accounts, only: [:create_many, :create_or_update_many]
  before_action :validate_query_word_count, only: [:autocomplete]
  before_action :ensure_user_is_verified, only: :merge_self
  before_action :require_admin!, only: %i[merge destroy_many seats_remaining]
  before_action :ensure_user_is_present, only: %i[merge merge_self create_or_update]
  before_action :ensure_users_is_present, only: %i[create_many create_or_update_many]
  before_action :ensure_user_create_many_limit, only: %i[create_many create_or_update_many]
  before_action :throttle_create_or_update, only: :create_or_update
  before_action :set_search_cache_control_header, only: [:search, :autocomplete]
  before_action :throttle_writes_leaky_bucket, only: [:create, :create_or_update, :update]
  before_action :validate_custom_role_id, only: [:create_or_update, :update, :create_or_update_many, :update_many]

  around_action :handle_rate_limiting_on_deep_offset_pagination, only: [:index]

  # A bug in older versions of zendesk_api_client_rb sent the role as
  # a hash with either an id or a name with the right value.
  # It got fixed in v1.0.7 but older versions are still in the wild.
  # TODO: As of Nov 18, 2020, v1.0.7 is no longer seen in logs. This
  # should be removed, but needs more study due to the addition of the
  # case-insensitivity.
  class RoleConstraint < StrongerParameters::Constraint
    ALLOWED_VALUES = ActionController::Parameters.enum('end-user', 'agent', 'admin', 'legacy agent').freeze

    def value(value)
      string_value = if HashParam.ish?(value)
        value[:name] || value[:id]
      else
        value
      end

      string_value = string_value.downcase if string_value.respond_to?(:downcase)

      ALLOWED_VALUES.value(string_value)
    end
  end

  USER_PARAMETER = {
    id:                    Parameters.bigid,
    name:                  Parameters.string,
    alias:                 Parameters.string,
    verified:              Parameters.boolean,
    locale_id:             Parameters.bigid | Parameters.nil_string,
    locale:                Parameters.string,
    time_zone:             Parameters.string,
    email:                 Parameters.string,
    phone:                 Parameters.string,
    signature:             Parameters.string | Parameters.nil,
    html_signature:        Parameters.string | Parameters.nil,
    details:               Parameters.string,
    notes:                 Parameters.string,
    organization:          Parameters.map(name: Parameters.string),
    organization_name:     Parameters.string,
    organization_id:       Parameters.bigid | Parameters.nil_string,
    organization_ids:      [Parameters.bigid],
    role:                  RoleConstraint.new,
    custom_role_id:        Parameters.bigid,
    moderator:             Parameters.boolean,
    ticket_restriction:    Parameters.string,
    only_private_comments: Parameters.boolean | Parameters.nil_string,
    tags:                  Parameters.array(Parameters.string) | Parameters.string,
    remote_photo_url:      Parameters.string,
    photo:                 Parameters.map(uploaded_data: Parameters.anything),
    external_id:           Parameters.string | Parameters.integer,
    suspended:             Parameters.boolean,
    via:                   Parameters.string,
    user_fields:           Parameters.map,
    skip_verify_email:     Parameters.boolean,
    default_group_id:      Parameters.bigid,
    active_brand_id:       Parameters.bigid,
    shared_phone_number:   Parameters.boolean,
    identities:            [
      {
        type:     Parameters.string,
        value:    Parameters.string,
        verified: Parameters.boolean,
        primary:  Parameters.boolean
      }.freeze
    ].freeze
  }.freeze

  USER_BATCH_PARAMETER  = USER_PARAMETER.merge(id: Parameters.bigid)
  CREATE_USER_PARAMETER = USER_PARAMETER.merge(group_ids: Parameters.array(Parameters.bigid))

  rescue_from Zopim::SynchronizationError do
    render json: {domain: 'Zopim', error: 'SyncStateError'}, status: :unprocessable_entity
  end

  rescue_from 'Users::OrganizationMemberships::OrganizationNotFound' do |e|
    render json: { error: 'RecordNotFound', description: e.message }, status: :unprocessable_entity
  end

  PAGE_THRESHOLD_FOR_USERS_INDEX = 50

  rescue_from Prop::RateLimited do |e|
    if e.handle == :create_or_update_user
      Rails.logger.warn("Api::V2::UsersController#create_or_update Rate Limited: #{e.message}")
      response.headers['Retry-After'] = e.retry_after.to_s
      render plain: I18n.t('txt.api.v2.users.create_or_update.rate_limit'), status: 429, content_type: Mime[:text].to_s
    elsif e.handle == :create_or_update_on_unique_user
      Rails.logger.warn("Api::V2::UsersController#create_or_update Rate Limited: #{e.message}, user email: #{params[:user][:email]} id: #{params[:user][:id]} external_id: #{params[:user][:external_id]}")
      response.headers['Retry-After'] = e.retry_after.to_s
      render plain: I18n.t("txt.api.v2.users.create_or_update.rate_limit_by_unique_user") + " user email: #{params[:user][:email]} id: #{params[:user][:id]} external_id: #{params[:user][:external_id]}", status: 429, content_type: Mime[:text].to_s
    elsif e.handle == :user_write_endpoints
      Rails.logger.warn("Api::V2::UsersController write endpoints Rate Limited: #{e.message}")
      response.headers['Retry-After'] = e.retry_after.to_s
      render plain: 'user writes Rate Limited', status: 429, content_type: Mime[:text].to_s
    else
      Rails.logger.warn("Api::V2::UsersController Rate Limited #{e.message}")
      raise e
    end
  end

  treat_as_read_request :show_many, :count

  allow_parameters :all, group_id: Parameters.bigid, organization_id: Parameters.bigid

  ## ### List Users
  ##
  ## * `GET /api/v2/users.json`
  ## * `GET /api/v2/groups/{id}/users.json`
  ## * `GET /api/v2/organizations/{id}/users.json`
  ##
  ## #### Pagination
  ##
  ## * Cursor pagination (recommended)
  ## * Offset pagination
  ##
  ## See [Pagination](./introduction#pagination).
  ##
  ## Returns a maximum of 100 records per page.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents and Light Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Filters
  ##
  ## * `role={role}` for a single role. Possible values are "end-user", "agent", or "admin"
  ## * `role[]={role}&role[]={role}` for more than one role
  ## * `permission_set={role_id}` for custom roles in the Enterprise plan. You can only filter by one role id per request
  ##
  ## Example:
  ##
  ## `/api/v2/users.json?role[]=admin&role[]=end-user`
  ##
  ## **Note**: If filtering by multiple roles in curl, make sure to include
  ## the `-g` flag to prevent curl from interpreting the square brackets
  ## as globbing characters. Also enclose the URL in quotes. Example:
  ##
  ## ```bash
  ## curl -g 'https://{subdomain}.zendesk.com/api/v2/users.json?role[]=admin&role[]=end-user' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "users": [
  ##     {
  ##       "id": 223443,
  ##       "name": "Johnny Agent",
  ##       ...
  ##     },
  ##     {
  ##       "id": 8678530,
  ##       "name": "James A. Rosen",
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  allow_parameters :index,
    role: Parameters.string | Parameters.array(Parameters.string),
    permission_set: Parameters.anything,
    query: Parameters.anything
  allow_cursor_pagination_parameters :index
  allow_cursor_pagination_v2_parameters :index
  require_oauth_scopes :index, scopes: %i[users:read read]
  def index
    throttle_index_with_deep_offset_pagination

    if params[:query]
      # Probably due to an accident of the implementation, query can be passed in which results in search results getting returned. Search results don't work with cursor pagination yet.
      users_index_obp
    elsif current_account.arturo_enabled?('cursor_pagination_default_users')
      if with_offset_pagination? && !current_account.arturo_enabled?('remove_offset_pagination_users')
        users_index_obp
      else
        render json: cursor_presenter.present(users(cursor_pagination: true, cursor_pagination_v2: with_cursor_pagination_v2?))
      end
    else
      if with_cursor_pagination? || current_account.arturo_enabled?('remove_offset_pagination_users')
        render json: cursor_presenter.present(users(cursor_pagination: true, cursor_pagination_v2: with_cursor_pagination_v2?))
      else
        users_index_obp
      end
    end
  end

  ## ### Count Users
  ##
  ## * `GET /api/v2/users/count.json`
  ## * `GET /api/v2/groups/{id}/users/count.json`
  ## * `GET /api/v2/organizations/{id}/users/count.json`
  ##
  ## Returns an approximate count of users. If the count exceeds 100,000, it is updated every 24 hours.
  ##
  ## The `count[refreshed_at]` property is a timestamp that indicates when the count was last updated.
  ##
  ## **Note**: When the count exceeds 100,000, `count[refreshed_at]` may occasionally be `null`.
  ## This indicates that the count is being updated in the background, and `count[value]` is limited to 100,000 until the update is complete.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents and Light Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/count.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Filters
  ##
  ## Supports the same filters as the [List Users](#list-users) endpoint.
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##    count: {
  ##      value: 102,
  ##      refreshed_at: "2020-04-06T02:18:17Z"
  ##    },
  ##    links: {
  ##      url: "https://{subdomain}.zendesk.com/api/v2/users/count"
  ##    }
  ## }
  ## ```
  allow_parameters :count, role: Parameters.string | Parameters.array(Parameters.string),
    permission_set: Parameters.anything,
    query: Parameters.anything
  require_oauth_scopes :count, scopes: %i[users:read read]
  def count
    render json: { count: record_counter.present, links: { url: request.original_url } }
  end

  allow_parameters :show_many, ids: Parameters.string, external_ids: Parameters.string
  require_oauth_scopes :show_many, scopes: %i[users:read read write]
  def show_many
    key = many_ids?(:external_ids) ? :external_ids : :ids
    render json: presenter.present(many_users(many_ids(key), key: key, per_page: per_page, include_inactive: true))
  end

  ## ### Show User
  ##
  ## `GET /api/v2/users/{id}.json`
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "user": {
  ##     "id":   35436,
  ##     "name": "Johnny Agent",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## ### Show Many Users
  ##
  ## `GET /api/v2/users/show_many.json?ids={ids}`
  ##
  ## `GET /api/v2/users/show_many.json?external_ids={external_ids}`
  ##
  ## Accepts a comma-separated list of up to 100 user ids or external ids.
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/show_many.json?ids=1,2,3 \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##  "users": [
  ##    {
  ##      "id": 345678,
  ##      "name": "Johnny Appleseed",
  ##      ...
  ##    },
  ##    {
  ##      "id": 901234,
  ##      "name": "Rupert Root",
  ##      ...
  ##    }
  ##  ]
  ## }
  ## ```
  allow_parameters :show, id: ActionController::Parameters.bigid
  require_oauth_scopes :show, scopes: %i[users:read read]
  def show
    # prevent Users API overload from very frequent use more than one request per 5 seconds (per client)
    # the cache key is the unique URL with params and we use client's cache not per account
    # set "Cache-control:max-age=5,private" very similar how we protected Occam service
    # more details https://zendesk.atlassian.net/wiki/spaces/CP/pages/481558838
    if current_account.has_user_show_sideloads_cache_control_header?
      # heavy lifting sideloads with no cache key are being cached for longer
      expires_in(5.seconds)
    end

    render json: presenter.present(user)
  end

  ## ### User related information
  ##
  ## `GET /api/v2/users/{id}/related.json`
  ##
  ## See [JSON Format for User Related Information](#json-format-for-user-related-information).
  ##
  ## #### Allowed For:
  ##
  ##  * Agents
  ##
  ## #### Using curl:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/related.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "user_related": {
  ##     "assigned_tickets": 5,
  ##     "requested_tickets": 10,
  ##     "ccd_tickets": 3,
  ##     "topics": 2,
  ##     "topic_comments": 7,
  ##     "votes": 3,
  ##     "subscriptions": 4,
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :related, id: Parameters.bigid
  require_oauth_scopes :related, scopes: %i[users:read read write]
  def related
    render json: user_related_presenter.present(user)
  end

  ## ### Create User
  ## `POST /api/v2/users.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions on certain actions
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users.json \
  ##   -d '{"user": {"name": "Roger Wilco", "email": "roge@example.org"}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## If you don't specify a `role` parameter, the new user is assigned the role of end-user.
  ##
  ## If you need to create users without sending out a verification email, pass a `"verified": true` parameter.
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: /api/v2/users/{new-user-id}.json
  ##
  ## {
  ##   "user": {
  ##     "id":   9873843,
  ##     "name": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## **Note:** If you need to create agents with a specific role, the `role`
  ## parameter only accepts three possible values: "end-user", "agent", and
  ## "admin". Therefore, set `role` to "agent" as well as add a new
  ## parameter called `custom_role_id` and give it the actual desired
  ## role ID from your Zendesk Support account. This applies to the
  ## built-in light agent role of Zendesk Support as well.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users.json \
  ##   -d '{"user": {"name": "Roger Wilco", "email": "roge@example.org", "role": "agent", "custom_role_id": 123456}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## The user can also be added to a named organization.
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/create_or_update.json \
  ##   -d '{"user": {"name": "Roger Wilco", "email": "roge@example.org", "organization": {"name": "VIP Customers"}}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## ### Create User with Multiple Identities
  ##
  ## If you have a user with multiple identities, such as email addresses and Twitter accounts, you can also include
  ## these values at creation time. This is especially useful when importing users from another system.
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users.json \
  ##   -d '{"user": {"name": "Roger Wilco", "identities": [{ "type": "email", "value": "test@user.com"}, {"type": "twitter", "value": "tester84" }]}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  allow_parameters :create, user: CREATE_USER_PARAMETER
  require_oauth_scopes :create, scopes: %i[users:write write]
  def create
    user = new_user
    user.active_brand_id ||= current_brand.id
    Zendesk::SupportUsers::User.new(user).save!

    render json: presenter.present(user), status: :created, location: presenter.url(user)
  end

  ## ### Create Or Update User
  ## `POST /api/v2/users/create_or_update.json`
  ##
  ## Creates a user if the user does not already exist, or updates an existing user
  ## identified by e-mail address or external ID.
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions on certain actions
  ##
  ## #### Using curl
  ##
  ## Existing user identified by e-mail address:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/create_or_update.json \
  ##   -d '{"user": {"name": "Roger Wilco", "email": "roge@example.org"}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## Existing user identified by external ID:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/create_or_update.json \
  ##   -d '{"user": {"external_id": "account_12345", "name": "Roger Wilco", "email": "new.email@example.org"}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## The user can also be added to a named organization.
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/create_or_update.json \
  ##   -d '{"user": {"name": "Roger Wilco", "email": "roge@example.org", "organization": {"name": "VIP Customers"}}}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## If you don't specify a `role` parameter, the new user is assigned the role of end-user.
  ##
  ## If you need to create users without sending out a verification email, pass a `"verified": true` parameter.
  ##
  ## #### Example Response
  ##
  ## If the user was created:
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: /api/v2/users/{new-user-id}.json
  ##
  ## {
  ##   "user": {
  ##     "id":   9873843,
  ##     "name": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## If an existing user was updated:
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: /api/v2/users/{existing-user-id}.json
  ##
  ## {
  ##   "user": {
  ##     "id":   9873843,
  ##     "name": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :create_or_update, user: USER_PARAMETER
  require_oauth_scopes :create_or_update, scopes: %i[users:write write]
  def create_or_update
    user = current_account.update_existing_or_initialize_new_user(current_user, params)

    # If update_existing_or_initialize_new_user returns nil, we didn't do any work
    return head :accepted if user.nil?

    is_new_user = user.new_record?

    return deny_access unless current_user.can?(is_new_user ? :create : :edit, user)

    throttle_create_or_update_on_unique_user(user) unless is_new_user

    user.active_brand_id ||= current_brand.id if is_new_user
    Zendesk::SupportUsers::User.new(user).save!

    render json: presenter.present(user), status: is_new_user ? :created : :ok, location: presenter.url(user)
  end

  ## ### Create Or Update Many Users
  ## `POST /api/v2/users/create_or_update_many.json`
  ##
  ## Accepts an array of up to 100 user objects. For each user, the user is created if it does not
  ## already exist, or the existing user is updated.
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions on certain actions
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/create_or_update_many.json \
  ##   -d '{"users": [{"name": "Roger Wilco", "email": "roge@example.org", "role": "agent"}, {"external_id": "account_54321", "name": "Woger Rilco", "email": "woge@example.org", "role": "admin"}]}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## Each individual user object can identify an existing user by `email` or by `external_id`.
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  allow_parameters :create_or_update_many, users: [USER_PARAMETER]
  require_oauth_scopes :create_or_update_many, scopes: %i[users:write write read]
  def create_or_update_many
    params[:create_or_update] = true
    params[:request_brand_id] = current_brand.id
    render json: job_status_presenter.present(enqueue_job_with_instrument(UserBulkCreateJob, params))
  end

  ## ### Merge Self With Another User
  ## `PUT /api/v2/users/me/merge.json`
  ##
  ## The current user will be merged into the existing user provided in the parameters.
  ## Users can only merge themselves into another user.
  ##
  ## #### Allowed For
  ##
  ##  * Verified end users
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/me/merge.json \
  ##   -d '{"user": {"password": "foo1234", "email": "roge@example.org"}}' \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -v -u {email_address}:{password}
  ##
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: /api/v2/users/{user-id}.json
  ##
  ## {
  ##   "user": {
  ##     "id":   9873843,
  ##     "name": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :merge_self, user: {email: Parameters.string, password: Parameters.string}
  require_oauth_scopes :merge_self, scopes: %i[users:write write read]
  def merge_self
    @loser  = current_user
    @winner = current_account.find_user_by_email(params[:user][:email])

    merge_users_with_auth!(@winner, @loser, params[:user][:password])
    render_user(@winner)
  rescue UserMergeError
    render_merge_error
  rescue UserAuthenticationError
    head :unauthorized
  end

  ## ### Merge End Users
  ## `PUT /api/v2/users/{id}/merge.json`
  ##
  ## The end user whose id is provided in the URL will be merged into the
  ## existing end user provided in the params. You can merge any two arbitrary
  ## end users with the exception of end users created by sharing agreements.
  ## Agents and admins cannot be merged.
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}/merge.json \
  ##   -d '{"user": {"id": 1234}}' \
  ##   -H "Content-Type: application/json" -X PUT  \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: /api/v2/users/{id}.json
  ##
  ## {
  ##   "user": {
  ##     "id":   9873843,
  ##     "name": "Roger Wilco",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :merge, id: Parameters.bigid, user: {id: Parameters.bigid}
  require_oauth_scopes :merge, scopes: %i[users:write write read]
  def merge
    @loser  = current_account.users.find(params[:id])
    @winner = current_account.users.find(params[:user][:id])

    merge_users!(@winner, @loser)
    render_user(@winner)
  rescue UserMergeError
    render_merge_error
  end

  ## ### Create Many Users
  ## `POST /api/v2/users/create_many.json`
  ##
  ## Accepts an array of up to 100 user objects.
  ##
  ## **Note**: Access to this endpoint is limited. You may not be able to use
  ## it to bulk-import users or organizations. If you're unable, a 403 Forbidden
  ## error is returned. If you need access and can't use this endpoint,
  ## contact us at <a href="mailto:support@zendesk.com">support@zendesk.com</a>
  ## for assistance.
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions on certain actions
  ##
  ## #### Specifying an organization
  ##
  ## You can assign a user to an existing organization by setting an
  ## `organization_id` property in the user object. Example:
  ##
  ## ```json
  ## {
  ##   "users": [
  ##     {
  ##       "name": "Roger Wilco",
  ##       "email": "roge@example.org",
  ##       "organization_id": 567812345
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/create_many.json \
  ##   -d '{"users": [{"name": "Roger Wilco", "email": "roge@example.org", "role": "agent"}, {"name": "Woger Rilco", "email": "woge@example.org", "role": "admin"}]}' \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  allow_parameters :create_many, users: [USER_PARAMETER]
  require_oauth_scopes :create_many, scopes: %i[users:write write read]
  def create_many
    params[:request_brand_id] = current_brand.id
    render json: job_status_presenter.present(enqueue_job_with_instrument(UserBulkCreateJob, params))
  end

  ## ### Update User
  ## `PUT /api/v2/users/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions on certain actions
  ##
  ## Agents can only update end users. Administrators can update end users, agents, and administrators.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}.json \
  ##   -d '{"user": {"name": "Roger Wilco II"}}' \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "user": {
  ##     "id":   9873843,
  ##     "name": "Roger Wilco II",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### Specifying email and verified attributes
  ##
  ##  * If both `email` and `verified` attributes are provided, the provided email is added to the user as a
  ##    secondary email with the `verified` property set to the specified value. The primary email remains unmodified
  ##  * If only `email` is provided, the provided email is added to the user as a secondary email with the
  ##    `verified` property set property set to false. The primary email remains unmodified
  ##  * If only `verified` is provided, the `verified` property of the primary email identity is set to the
  ##     given value
  ##
  allow_parameters :update, id: Parameters.bigid, user: USER_PARAMETER
  require_oauth_scopes :update, scopes: %i[users:write write]
  def update
    user_initializer.apply(user)
    return deny_access unless current_user.can?(:edit, user)

    throttle_create_or_update_on_unique_user(user)

    Zendesk::SupportUsers::User.new(user).save!

    if [Mime[:json], '*/*'].include?(request.accept)
      render json: presenter.present(user), content_type: Mime[:json].to_s
    else
      render plain: presenter.present(user).to_json, content_type: Mime[:text].to_s
    end
  end

  ## ### Update Many Users
  ##
  ## `PUT /api/v2/users/update_many.json`
  ##
  ## `PUT /api/v2/users/update_many.json?ids={ids}`
  ##
  ## `PUT /api/v2/users/update_many.json?external_ids={external_ids}`
  ##
  ## Bulk or batch updates up to 100 users.
  ##
  ## #### Bulk update
  ##
  ## To make the same change to multiple users, use the following endpoint and data format:
  ##
  ## `https://{subdomain}.zendesk.com/api/v2/users/update_many.json?ids=1,2,3`
  ##
  ## ```js
  ## {
  ##   "user": {
  ##     "organization_id": 1
  ##   }
  ## }
  ## ```
  ##
  ## #### Batch update
  ##
  ## To make different changes to multiple users, use the following endpoint and data format:
  ##
  ## `https://{subdomain}.zendesk.com/api/v2/users/update_many.json`
  ##
  ## ```js
  ## {
  ##   "users": [
  ##     { "id": 10071, "name": "New Name", "organization_id": 1 },
  ##     { "id": 12307, "verified": true }
  ##   ]
  ## }
  ## ```
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions
  ##
  ## Agents can only update end users. Administrators can update end users, agents, and administrators.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/update_many.json?ids=1,2,3 \
  ##   -d '{"user": {"organization_id": 1}}' \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/update_many.json \
  ##   -d '{"users": [{"id": 10071, "name": "New Name", "organization_id": 1}, {"external_id": "123", "verified": true}]}' \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
  ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  allow_parameters :update_many, ids: Parameters.ids, external_ids: Parameters.string, user: USER_PARAMETER, users: [USER_BATCH_PARAMETER]
  require_oauth_scopes :update_many, scopes: %i[users:write write read]
  def update_many
    render json: job_status_presenter.present(update_many_job)
  end

  ## ### Bulk Deleting Users
  ##
  ## `DELETE /api/v2/users/destroy_many.json?ids={ids}`
  ##
  ## `DELETE /api/v2/users/destroy_many.json?external_ids={external_ids}`
  ##
  ## Accepts a comma-separated list of up to 100 user ids.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/destroy_many.json?ids=1,2,3 \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :destroy_many, ids: Parameters.ids, external_ids: Parameters.string
  require_oauth_scopes :destroy_many, scopes: %i[users:write write read]
  def destroy_many
    render json: job_status_presenter.present(destroy_many_job)
  end

  ## ### Suspending a User
  ##
  ## You can suspend a user by setting its `suspended` attribute to `true`.
  ##
  ## When a user is suspended, the user is not allowed to sign in to Help Center and
  ## all further tickets are suspended.
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}.json \
  ##   -d '{"user": {"suspended": true}}' \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```js
  ## {
  ##   "user": {
  ##     "id":        9873843,
  ##     "name":      "Roger Wilco II",
  ##     "suspended": true,
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## ### Delete User
  ## `DELETE /api/v2/users/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents, with restrictions on certain actions
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}.json \
  ##   -v -u {email_address}:{password} \
  ##   -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "user": {
  ##     "id":     9873843,
  ##     "name":   "Roger Wilco II",
  ##     "active": false,
  ##     ...
  ##   }
  ## }
  ## ```
  allow_parameters :destroy, id: Parameters.bigid
  require_oauth_scopes :destroy, scopes: %i[users:write write]
  def destroy
    return deny_access if user == current_user

    Zendesk::SupportUsers::User.new(user).delete!

    render json: presenter.present(user)
  end

  ## ### Search Users
  ## `GET /api/v2/users/search.json?query={query}`
  ##
  ## `GET /api/v2/users/search.json?external_id={external_id}`
  ##
  ## Returns an array of users who meet the search criteria.
  ##
  ## The `query` parameter can specify a partial or full value of any
  ## user property, including name, email address, notes, or phone. Example:
  ## `query="jdoe"`.
  ##
  ## The `query` parameter supports the Zendesk search syntax for more advanced
  ## user searches. See the [Search API](http://developer.zendesk.com/documentation/rest_api/search.html).
  ##
  ## The `external_id` parameter does not support the search syntax. It only accepts ids.
  ##
  ## #### Allowed For
  ##
  ##  * Admins, Agents and Light Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/search.json?query=gil \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "users": [
  ##     {
  ##       "id":   35436,
  ##       "name": "Robert Jones",
  ##       "notes": "sigil issue"
  ##       ...
  ##     },
  ##     {
  ##       "id":   9873843,
  ##       "name": "Terry Gilliam",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :search,
    query: Parameters.string,
    external_id: Parameters.string,
    role: Parameters.string | Parameters.array(Parameters.string) | Parameters.nil
  require_oauth_scopes :search, scopes: %i[users:write write read]
  def search
    render json: presenter.present(users)
  rescue ZendeskSearch::QueryError => e
    render json: {error: 'Query Error', description: e.message}, status: :bad_request
  end

  ## ### Autocomplete Users
  ## `GET /api/v2/users/autocomplete.json?name={name}`
  ##
  ## Returns an array of users whose name starts with the value specified in the `name` paramater.
  ## It only returns users with no foreign identities.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/autocomplete.json?name=gil \
  ##   -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200
  ##
  ## {
  ##   "users": [
  ##     {
  ##       "id":   35436,
  ##       "name": "Giles Winters",
  ##       ...
  ##     },
  ##     {
  ##       "id":   9873843,
  ##       "name": "Gillian Summers",
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :autocomplete, name: Parameters.string, phone: Parameters.string, filter: Parameters.string | Parameters.nil
  require_oauth_scopes :autocomplete, scopes: %i[users:read read write]
  def autocomplete
    if current_account.has_ocp_users_autocomplete_query? && params[:filter].present?
      autocomplete_filtered_users
    else
      autocomplete_users
    end
  end

  ## ### Update a User's Profile Image
  ##
  ## You can update a user's profile image by uploading a local file or by
  ## referring to an image hosted on a different website. The second option
  ## may take a few minutes to process.
  ##
  ## #### Using curl
  ##
  ## Uploading a local file:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}.json \
  ##   -F "user[photo][uploaded_data]=@/path/to/profile/image.jpg" \
  ##   -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## Setting a remote image URL:
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/{id}.json \
  ##   -d '{"user": {"remote_photo_url": "http://link.to/profile/image.png"}}' \
  ##   -H "Content-Type: application/json" \
  ##   -v -u {email_address}:{password} -X PUT
  ## ```
  ##
  ## ### Request User Create
  ## `POST /api/v2/users/request_create.json`
  ##
  ##  Sends the owner a reminder email to update their subscription so more agents can be created.
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/users/request_create.json \
  ##   -d '{"user": {"name": "Roger Wilco", "email": "roge@example.org"}}' \
  ##   -H "Content-Type: application/json" \
  ##   -X POST -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  allow_parameters :request_create, low_seat_count_notification: Parameters.boolean | Parameters.nil, user: USER_PARAMETER
  require_oauth_scopes :create, scopes: %i[users:write write]
  def request_create
    send_update_subscription_request_email(params)
    head :ok
  end

  # TODO: When the Staff Service can accept calls from Lotus remove this endpoint
  #       https://github.com/zendesk/zendesk/pull/40724#issuecomment-581567425
  #
  # h3 Remaining agent seats
  #
  # `GET /api/v2/users/seats_remaining.json`
  #
  # h4 Allowed For
  #
  #  * Admins
  #
  # h4 Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/users/seats_remaining.json \
  #      -v -u {email_address}:{password}
  # ```
  #
  # h4 Example Response
  #
  # ```http
  # Status: 200 OK
  # { "seats_remaining": { "support": 5 } }
  # ```
  allow_parameters :seats_remaining, {}
  require_oauth_scopes :seats_remaining, scopes: %i[users:read read]
  def seats_remaining
    if current_account.has_allow_seats_remaining?
      support_seats_remaining = if current_account.multiproduct || current_account.spp?
        staff_service_client.seats_remaining_support_or_suite!
      else
        current_account.subscription.max_agents - current_account.billable_agent_count
      end

      if support_seats_remaining == 0
        Rails.logger.info(
          "No agent seats remaining for:
            accountid: #{current_account.id},
            subdomain: #{current_account.subdomain},
            ownerid: #{current_account.owner_id},
            userid: #{current_user.id}".squish
        )
      end

      # starting with support seats only; add other products as needed
      render json: { seats_remaining: { support: support_seats_remaining } }
    else
      head :bad_request
    end
  end

  protected

  def send_update_subscription_request_email(params)
    if params[:low_seat_count_notification]
      AccountsMailer.deliver_low_seat_count_notification(current_user.account)

      Rails.logger.info(
        "Low seat count notification email sent:
          owner_email: #{current_user.account.owner.email},
          ownerid: #{current_user.account.owner.id},
          cpgid: lowseatcount_email,
          userid: #{current_user.id}".squish
      )
    else
      AccountsMailer.deliver_update_subscription_request(
        current_user.account,
        params[:user][:email],
        params[:user][:name],
        params[:user][:role],
        current_user.name
      )
    end
  end

  def ensure_user_is_verified
    deny_access unless current_user.is_verified?
  end

  def ensure_user_is_present
    unless params[:user].present?
      raise Zendesk::UnknownAttributeError, 'missing user parameter'
    end
  end

  def ensure_users_is_present
    unless params[:users].present?
      raise Zendesk::UnknownAttributeError, 'missing users parameter'
    end
  end

  def ensure_user_create_many_limit
    if params[:users].length > create_many_users_limit
      render json: {
        error:       'TooManyValues',
        description: "Maximum #{create_many_users_limit} users may be created per call"
      }, status: :bad_request
    end
  end

  def presenter
    @presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self, includes: includes)
  end

  # The related side_load is required to build the user profile on the mobile
  # agent app (Scarlett). This way, to avoid unnecessary N+1 on the other
  # action, this side_load is allowed only on the `show` action.
  def includes
    values = super
    values.delete(:related) unless action_name.eql?('show')

    values
  end

  def cursor_presenter
    @cursor_presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
  end

  def highlighting_presenter(highlights)
    Api::V2::Users::Presenter.new(current_user, url_builder: self, includes: includes, highlights: highlights)
  end

  def cursor_highlighting_presenter(highlights)
    Api::V2::Users::Presenter.new(current_user, url_builder: self, includes: includes, highlights: highlights, with_cursor_pagination: true)
  end

  def job_status_presenter
    @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
  end

  def user_related_presenter
    @user_related_presenter ||= Api::V2::UserRelatedPresenter.new(current_user, url_builder: self)
  end

  def query_string
    params[:phone] ? params[:phone] : params[:name]
  end

  def autocomplete_users
    query.string = query_string
    query.endpoint = "users/autocomplete"
    query.incremental = true
    query.source = 'autocomplete'
    query.highlight_key = :users
    results = query.execute(current_user)
    exception = results.exception

    if exception.class == ZendeskSearch::QueryError
      render json: {error: 'Query Error', description: exception.message}, status: :bad_request
    elsif exception
      render json: {error: 'Unavailable', description: exception.message}, status: :internal_server_error
    else
      render json: highlighting_presenter(results.highlights).present(results)
    end
  end

  def autocomplete_filtered_users
    if params[:filter] == 'assignable'
      filtered_users = User.assignable(current_account).with_name_like(params[:name])
      render json: presenter.present(paginate(filtered_users))
    elsif params[:filter] == 'requester'
      filtered_users = User.requesters(current_account).with_name_like(params[:name])
      render json: presenter.present(paginate(filtered_users))
    else
      render json: {error: 'Invalid value', description: 'Valid filter values: [assignable, requester]'}, status: :bad_request
    end
  end

  def user
    # Explanation: We need to do an unscoped search for users (ie: include is_active=false)
    # because lotus needs access to deleted users to do lookups on ccs, audits and such.
    @user ||= current_account.all_users.find(params[:id]).tap do |user|
      user.current_user = current_user
    end
  end

  def render_user(user)
    render json: presenter.present(user), location: presenter.url(user)
  end

  def update_many_job
    if params[:users]
      return enqueue_job_with_instrument(UserBatchUpdateJob, params.merge(from_api: true))
    elsif params[:user]
      if many_ids?
        return enqueue_job_with_instrument(UserBulkUpdateJobV2, params.merge(user_ids: many_ids, key: :id, from_api: true))
      elsif many_ids?(:external_ids)
        return enqueue_job_with_instrument(UserBulkUpdateJobV2, params.merge(user_ids: many_ids(:external_ids), key: :external_id, from_api: true))
      end
    end
    raise ActionController::ParameterMissing, 'Both ids/external_ids and users attributes must be present'
  end

  def destroy_many_job
    if many_ids?
      enqueue_job_with_instrument(UserBulkUpdateJobV2, params.merge(user_ids: many_ids, submit_type: 'delete', key: :id))
    elsif many_ids?(:external_ids)
      enqueue_job_with_instrument(UserBulkUpdateJobV2, params.merge(user_ids: many_ids(:external_ids), submit_type: 'delete', key: :external_id))
    else
      raise ActionController::ParameterMissing, 'Either ids or external_ids must be present'
    end
  end

  def restrict_bulk_import_from_risky_accounts
    return if current_account.trusted_bulk_uploader?
    head(:forbidden)
  end

  def validate_query_word_count
    return unless query_string
    if query_string.split(" ").size > 64
      render json: {error: 'Query Error', description: 'Number of search words exceeds the limit of 64'}, status: :bad_request
    end
  end

  def throttle_create_or_update
    Prop.throttle!(:create_or_update_user, current_account.id) if current_account.has_throttle_user_create_or_update?
  end

  def throttle_writes_leaky_bucket
    Zendesk::Users::UsersLeakyBucket.configure_rate_limit(current_account.default_api_rate_limit)
    if current_account.has_throttle_user_writes_leaky_bucket?
      Prop.throttle!(:user_write_endpoints, current_account.id)
    else
      if Prop.throttle(:user_write_endpoints, current_account.id)
        Rails.logger.info("Would have rate limited writes by leaky bucket: #{current_account.subdomain} action #{action_name} throttle key: create_leaky_bucket")
        statsd_leaky_bucket_client.increment('would_have_rate_limited', tags: ["account_id:#{current_account.id}"])
      end
    end
  end

  def throttle_create_or_update_on_unique_user(user)
    if current_account.has_rate_limit_create_or_update_on_unique_user?
      Prop.throttle!(:create_or_update_on_unique_user, [current_account.id, user.id])
    else
      throttled = Prop.throttle(:create_or_update_on_unique_user, [current_account.id, user.id])
      if throttled
        Rails.logger.info("Would have rate limited create_or_updates by user account: #{current_account.subdomain}, user: #{user.try(:id)}, throttle key: create_or_update_on_unique_user")
        current_account.upserting_statsd_client.increment('would_have_rate_limited', tags: ["account_id:#{current_account.id}"])
      end
    end
  end

  def validate_custom_role_id_user(user)
    if user[:custom_role_id].present? && !current_account.permission_sets.exists?(user[:custom_role_id])
      render json: {error: 'Invalid custom role id', description: 'Custom role id does not exist'}, status: :bad_request
    end
  end

  def validate_custom_role_id
    if params[:users]
      params[:users].each do |user|
        validate_custom_role_id_user(user)
      end
    elsif params[:user]
      validate_custom_role_id_user(params[:user])
    end
  end

  def statsd_client
    Zendesk::StatsD::Client.new(namespace: 'bulk_change_instrumentation')
  end

  def statsd_leaky_bucket_client
    Zendesk::StatsD::Client.new(namespace: ['account', 'leaky_bucket'])
  end

  def enqueue_job_with_instrument(klass, options = {})
    identifier = enqueue_job_with_status(klass, options)

    if options[:users]
      tags = ["source:#{request.referrer}", "users_count:#{params[:users].count}"]
    elsif many_ids?
      tags = ["source:#{request.referrer}", "users_count:#{many_ids.count}"]
    elsif many_ids?(:external_ids)
      tags = ["source:#{request.referrer}", "users_count:#{many_ids(:external_ids).count}"]
    end

    statsd_client.increment(options[:action], tags: tags)
    identifier
  end

  def render_merge_error
    case @merge_status
    when :winner_is_loser
      render json: { error: I18n.t("txt.admin.controllers.people.user_merge.cannot_merge_user_to_itself", user_name: @winner.name) }, status: :unprocessable_entity
    when :loser_non_enduser
      render json: { error: I18n.t("txt.admin.controllers.people.user_merge.can_only_merge_end_user", user_name: @loser.name) }, status: :unprocessable_entity
    when :winner_non_enduser
      render json: { error: I18n.t("txt.admin.controllers.people.user_merge.can_only_merge_end_user", user_name: @winner.name) }, status: :unprocessable_entity
    when :loser_has_sso_external_id
      render json: { error: I18n.t('txt.admin.controllers.people.user_merge.cannot_merge_with_external_id', user_name: @loser.name) }, status: :unprocessable_entity
    else
      head :unprocessable_entity
    end
  end

  def staff_service_client
    Zendesk::StaffClient.new(current_account)
  end

  def throttle_index_with_deep_offset_pagination
    return nil unless params[:page].is_a?(Integer)

    Zendesk::OffsetPaginationLimiter.new(
      entity: controller_name,
      account_id: current_account.id,
      account_subdomain: current_account.subdomain,
      page: params[:page],
      per_page: per_page,
      log_only: !current_account.has_bolt_throttle_index_with_deep_pagination_users?
    ).throttle_index_with_deep_offset_pagination
  end

  def statsd_client_users
    @statsd_client_users ||= Zendesk::StatsD::Client.new(namespace: "users")
  end

  def handle_rate_limiting_on_deep_offset_pagination
    yield
  rescue Prop::RateLimited => e
    if e.handle == :index_users_with_deep_offset_pagination_ceiling_limit
      Rails.logger.info("[UsersController] Rate limiting #index by account: #{current_account.subdomain}, page: #{params[:page]}, per_page: #{per_page}, throttle key: index_users_with_deep_offset_pagination_ceiling_limit")
      statsd_client_users.increment("api_v2_users_index_ceiling_rate_limit", tags: ["account_id:#{current_account.id}"])
      response.headers['Retry-After'] = e.retry_after.to_s
      render plain: I18n.t("txt.api.v2.users.index_with_deep_offset_pagination.rate_limit_by_account") + " per_page: #{params[:per_page]}, page: #{params[:page]}", status: 429, content_type: Mime[:text].to_s
    else
      Rails.logger.warn("Api::V2::UsersController Rate Limited #{e.message}")
      raise e
    end
  end

  def record_counter
    @record_counter ||= begin
      Zendesk::RecordCounter::Users.new(
        account: current_account,
        options: params
      )
    end
  end

  def users_index_obp
    collection = users(per_page: per_page)
    if stale_collection?(collection)
      render json: presenter.present(collection)
    end
  end
end
