# Internal API for an ReactApp for the Mobile SDK admin settings
# FIXME: Not yet in use
class Api::V2::MobileSdkAppsController < Api::V2::BaseController
  include Zendesk::MobileSdk::AccountsSupport

  before_action :require_admin!

  MOBILE_SDK_APP_PARAMETER = {
    title: Parameters.string,
    brand_id: Parameters.bigid,
    signup_source: Parameters.string,
    settings: {
      contact_us_tags: Parameters.string,
      conversations_enabled: Parameters.boolean,
      rma_enabled: Parameters.boolean,
      rma_visits: Parameters.integer,
      rma_duration: Parameters.integer,
      rma_delay: Parameters.integer,
      rma_tags: Parameters.string,
      helpcenter_enabled: Parameters.boolean,
      rma_android_store_url: Parameters.string,
      rma_ios_store_url: Parameters.string,
      authentication: Parameters.string,
      push_notifications_enabled: Parameters.boolean,
      push_notifications_callback: Parameters.string,
      push_notifications_type: Parameters.string,
      urban_airship_key: Parameters.string,
      urban_airship_master_secret: Parameters.string,
      connect_enabled: Parameters.boolean,
      blips: Parameters.boolean,
      required_blips: Parameters.boolean,
      behavioural_blips: Parameters.boolean,
      pathfinder_blips: Parameters.boolean,
      collect_only_required_data: Parameters.boolean,
      support_never_request_email: Parameters.boolean,
      help_center_article_voting_enabled: Parameters.boolean,
      support_show_closed_requests: Parameters.boolean,
      support_show_referrer_logo: Parameters.boolean,
      answer_bot: Parameters.boolean,
      support_show_csat: Parameters.boolean
    },
    mobile_sdk_auth: {
      endpoint: Parameters.string,
    }
  }.freeze

  ## ### List mobile SDK apps
  ## `GET /api/v2/mobile_sdk_apps.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admin
  ##
  ## #### Using curl
  ##
  ## Listing mobile SDK apps
  ##
  ## ```bash
  ## curl -X GET -H "Content-Type: application/json" \
  ## -H "Authorization: Basic YWRtaW5AemVuZGVzay5jb206MTIzNDU2" \
  ## -H "Accept: application/json" \
  ## "https://{{domain}}/api/v2/mobile_sdk_apps.json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "mobile_sdk_apps": [
  ##     {
  ##       "title": "Foo",
  ##       "brand_id": 1,
  ##       "signup_source": "twitter_fabric"
  ##       "identifier": "loo2",
  ##       "settings": {
  ##         "contact_us_tags": [
  ##           "foo",
  ##           "bar"
  ##         ],
  ##         "conversations_enabled": true,
  ##         "rma_enabled": true,
  ##         "rma_visits": 1,
  ##         "rma_duration": 1,
  ##         "rma_delay": 1,
  ##         "rma_tags": [],
  ##         "helpcenter_enabled": true,
  ##         "rma_android_store_url": "http://android.com/store/foo",
  ##         "rma_ios_store_url": "http://apple.com/store/foo",
  ##         "authentication": "jwt",
  ##         "push_notifications_enabled": true,
  ##         "push_notifications_callback": "http://foo.com/foo/push",
  ##         "push_notifications_type": "webhook",
  ##         "urban_airship_key": "ua_key",
  ##         "urban_airship_master_secret": "ua_secret",
  ##         "connect_enabled": true,
  ##         "collect_only_required_data": false,
  ##         "support_never_request_email": false,
  ##         "help_center_article_voting_enabled": true,
  ##         "support_show_closed_requests": false,
  ##         "support_show_referrer_logo": true,
  ##         "answer_bot": true,
  ##         "csat": true
  ##       },
  ##       "mobile_sdk_auth": {
  ##         "endpoint": "https://foo.com/foo/webhook",
  ##         "shared_secret": "ClV0MLKcujrcutDEM2EmzPOz5eQxlzshsOkhqelZ6G4TydDw"
  ##       },
  ##       "url": "https://support.zd-dev.com/api/v2/mobile_sdk_apps/10016.json"
  ##     }
  ##   ],
  ##   "next_page": null,
  ##   "previous_page": null,
  ##   "count": 1
  ## }
  ## ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(mobile_sdk_apps)
  end

  ## ### Show a mobile SDK app
  ## `GET /api/v2/mobile_sdk_apps/{mobile_sdk_app_id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admin
  ##
  ## #### Using curl
  ##
  ## Show a mobile SDK app:
  ##
  ## ```bash
  ## curl -X GET -H "Content-Type: application/json" \
  ## -H "Authorization: Basic YWRtaW5AemVuZGVzay5jb206MTIzNDU2" \
  ## -H "Accept: application/json" \
  ## "https://{{domain}}/api/v2/mobile_sdk_apps/a95b4d8a77b6ed72e87e843f3002b0d70a3e8c8249eec47d.json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "mobile_sdk_app": {
  ##     "title": "Foo",
  ##     "brand_id": 1,
  ##     "signup_source": "twitter_fabric",
  ##     "identifier": "a95b4d8a77b6ed72e87e843f3002b0d70a3e8c8249eec47d",
  ##     "settings": {
  ##       "contact_us_tags": [
  ##         "foo",
  ##         "bar"
  ##       ],
  ##       "conversations_enabled": true,
  ##       "rma_enabled": true,
  ##       "rma_visits": 1,
  ##       "rma_duration": 1,
  ##       "rma_delay": 1,
  ##       "rma_tags": [],
  ##       "helpcenter_enabled": true,
  ##       "rma_android_store_url": "http://android.com/store/foo",
  ##       "rma_ios_store_url": "http://apple.com/store/foo",
  ##       "authentication": "jwt",
  ##       "push_notifications_enabled": true,
  ##       "push_notifications_callback": "http://foo.com/foo/push",
  ##       "push_notifications_type": "webhook",
  ##       "urban_airship_key": "ua_key",
  ##       "urban_airship_master_secret": "ua_secret",
  ##       "connect_enabled": true,
  ##       "collect_only_required_data": false,
  ##       "support_never_request_email": false,
  ##       "help_center_article_voting_enabled": false
  ##       "support_show_closed_requests": false,
  ##       "support_show_referrer_logo": true,
  ##       "answer_bot": true,
  ##       "csat": true
  ##     },
  ##     "mobile_sdk_auth": {
  ##       "endpoint": null,
  ##       "shared_secret": "kCHCwLaMwOz1NmV7vVvOSAa86MiSevWjmm93NG3ySeDiDGac"
  ##     },
  ##     "url": "https://support.zd-dev.com/api/v2/mobile_sdk_apps/a95b4d8a77b6ed72e87e843f3002b0d70a3e8c8249eec47d.json"
  ##   }
  ## }
  ## ```
  allow_parameters :show, id: Parameters.string
  def show
    render json: presenter.present(mobile_sdk_app)
  end

  ## ### Create a mobile SDK app
  ## `POST /api/v2/mobile_sdk_apps.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admin
  ##
  ## #### Using curl
  ##
  ## Create a mobile SDK app:
  ##
  ## ```bash
  ## curl -X POST -H "Content-Type: application/json" \
  ## -H "Authorization: Basic YWRtaW5AemVuZGVzay5jb206MTIzNDU2" \
  ## -H "Accept: application/json" \
  ## -d '{
  ##   "mobile_sdk_app": {
  ##     "title": "Foo",
  ##     "brand_id": 1,
  ##     "signup_source": "twitter_fabric",
  ##     "settings": {
  ##       "contact_us_tags": "foo, bar",
  ##       "conversations_enabled": true,
  ##       "rma_enabled": true,
  ##       "rma_visits": 1,
  ##       "rma_duration": 1,
  ##       "rma_delay": 1,
  ##       "rma_tags": "",
  ##       "helpcenter_enabled": true,
  ##       "rma_android_store_url": "http://android.com/store/foo",
  ##       "rma_ios_store_url": "http://apple.com/store/foo",
  ##       "authentication": "jwt",
  ##       "push_notifications_enabled": true,
  ##       "push_notifications_callback": "http://foo.com/foo/push",
  ##       "push_notifications_type": "webhook",
  ##       "urban_airship_key": "ua_key",
  ##       "urban_airship_master_secret": "ua_secret",
  ##       "connect_enabled": true,
  ##       "support_never_request_email": false,
  ##       "help_center_article_voting_enabled": true,
  ##       "support_show_closed_requests": false,
  ##       "support_show_referrer_logo": true,
  ##       "answer_bot": true,
  ##       "csat": true
  ##     },
  ##     "mobile_sdk_auth": {
  ##       "endpoint": "https://foo.com/foo/webhook",
  ##     }
  ##   }
  ## }' "https://support.zd-dev.com/api/v2/mobile_sdk_apps.json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ##
  ## {
  ##   "mobile_sdk_app": {
  ##     "title": "Foo",
  ##     "brand_id": 1,
  ##     "signup_source": "twitter_fabric",
  ##     "identifier": "48858f1902454e62737ba86db7d4807dfcd39165249f631f",
  ##     "settings": {
  ##       "contact_us_tags": [
  ##         "foo",
  ##         "bar"
  ##       ],
  ##       "conversations_enabled": true,
  ##       "rma_enabled": true,
  ##       "rma_visits": 1,
  ##       "rma_duration": 1,
  ##       "rma_delay": 1,
  ##       "rma_tags": [],
  ##       "helpcenter_enabled": true,
  ##       "rma_android_store_url": "http://android.com/store/foo",
  ##       "rma_ios_store_url": "http://apple.com/store/foo",
  ##       "authentication": "jwt",
  ##       "push_notifications_enabled": true,
  ##       "push_notifications_callback": "http://foo.com/foo/push",
  ##       "push_notifications_type": "webhook",
  ##       "urban_airship_key": "ua_key",
  ##       "urban_airship_master_secret": "ua_secret",
  ##       "connect_enabled": true,
  ##       "collect_only_required_data": false,
  ##       "support_never_request_email": false,
  ##       "help_center_article_voting_enabled": true,
  ##       "support_show_closed_requests": false,
  ##       "support_show_referrer_logo": true,
  ##       "answer_bot": true,
  ##       "csat": true
  ##     },
  ##     "mobile_sdk_auth": {
  ##       "endpoint": "https://foo.com/foo/webhook",
  ##       "shared_secret": "HxnrKr1yBDpBEOyLG9P1dy6YzG0UM6x17RQJuwKMhCRmhzaR"
  ##     },
  ##     "url": "https://support.zd-dev.com/api/v2/mobile_sdk_apps/48858f1902454e62737ba86db7d4807dfcd39165249f631f.json"
  ##   }
  ## }
  ## ```
  allow_parameters :create, mobile_sdk_app: MOBILE_SDK_APP_PARAMETER
  def create
    params.require(:mobile_sdk_app).permit(
      :title,
      :brand_id,
      :signup_source,
      settings: MOBILE_SDK_APP_PARAMETER[:settings].keys,
      mobile_sdk_auth: MOBILE_SDK_APP_PARAMETER[:mobile_sdk_auth].keys
    )

    settings_params = params[:mobile_sdk_app].delete(:settings)
    # Please refer to capabilities file to see what `mobile_sdk_rma_tab_removal` flag means
    settings_params[:rma_enabled] = false if current_account.has_mobile_sdk_rma_tab_removal?

    # This setting should default to `false` for existing apps and `true` for new ones
    if current_account.has_mobile_sdk_help_center_article_voting_enabled?
      settings_params[:help_center_article_voting_enabled] = true
    end

    auth_params = params[:mobile_sdk_app].delete(:mobile_sdk_auth)

    app = mobile_sdk_apps.build(params[:mobile_sdk_app])

    app.identifier = app.generate_identifier
    app.update_attributes(settings: process_settings(settings_params))

    if settings_params[:authentication].eql?(MobileSdkAuth::TYPE_JWT)
      build_auth_with_jwt(app, auth_params)
    else
      build_auth(app)
    end
    app.save!
    render json: presenter.present(app), status: :created
  end

  ## ### Update a mobile SDK app
  ## `PUT /api/v2/mobile_sdk_apps/{identifier}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admin
  ##
  ## #### Using curl
  ##
  ## Update a mobile SDK app:
  ##
  ## ```bash
  ## curl -X PUT -H "Content-Type: application/json" \
  ## -H "Authorization: Basic YWRtaW5AemVuZGVzay5jb206MTIzNDU2" \
  ## -H "Accept: application/json" \
  ## -d '{
  ##   "mobile_sdk_app": {
  ##     "title": "Foo",
  ##     "brand_id": 1,
  ##     "signup_source": "twitter_fabric",
  ##     "settings": {
  ##       "contact_us_tags": "foo, bar",
  ##       "conversations_enabled": true,
  ##       "rma_enabled": true,
  ##       "rma_visits": 1,
  ##       "rma_duration": 1,
  ##       "rma_delay": 1,
  ##       "rma_tags": "",
  ##       "helpcenter_enabled": true,
  ##       "rma_android_store_url": "http://android.com/store/foo",
  ##       "rma_ios_store_url": "http://apple.com/store/foo",
  ##       "authentication": "jwt",
  ##       "push_notifications_enabled": true,
  ##       "push_notifications_callback": "http://foo.com/foo/push",
  ##       "push_notifications_type": "webhook",
  ##       "urban_airship_key": "ua_key",
  ##       "urban_airship_master_secret": "ua_secret",
  ##       "connect_enabled": true,
  ##       "collect_only_required_data": false,
  ##       "support_never_request_email": false,
  ##       "help_center_article_voting_enabled": false
  ##       "support_show_closed_requests": false,
  ##       "support_show_referrer_logo": true,
  ##       "answer_bot": true,
  ##       "csat": true
  ##     },
  ##     "mobile_sdk_auth": {
  ##       "endpoint": "https://foo.com/foo/webhook",
  ##     }
  ##   }
  ## }' "https://support.zd-dev.com/api/v2/mobile_sdk_apps/48858f1902454e62737ba86db7d4807dfcd39165249f631f.json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "mobile_sdk_app": {
  ##     "title": "Foo",
  ##     "brand_id": 1,
  ##     "identifier": "48858f1902454e62737ba86db7d4807dfcd39165249f631f",
  ##     "settings": {
  ##       "contact_us_tags": [
  ##         "foo",
  ##         "bar"
  ##       ],
  ##       "conversations_enabled": true,
  ##       "rma_enabled": true,
  ##       "rma_visits": 1,
  ##       "rma_duration": 1,
  ##       "rma_delay": 1,
  ##       "rma_tags": [],
  ##       "helpcenter_enabled": true,
  ##       "rma_android_store_url": "http://android.com/store/foo",
  ##       "rma_ios_store_url": "http://apple.com/store/foo",
  ##       "authentication": "jwt",
  ##       "push_notifications_enabled": true,
  ##       "push_notifications_callback": "http://foo.com/foo/push",
  ##       "push_notifications_type": "webhook",
  ##       "urban_airship_key": "ua_key",
  ##       "urban_airship_master_secret": "ua_secret",
  ##       "connect_enabled": true,
  ##       "collect_only_required_data": false,
  ##       "support_never_request_email": false,
  ##       "help_center_article_voting_enabled": false,
  ##       "support_show_closed_requests": false
  ##       "support_show_referrer_logo": true,
  ##       "answer_bot": true,
  ##       "csat": true
  ##     },
  ##     "mobile_sdk_auth": {
  ##       "endpoint": "https://foo.com/foo/webhook",
  ##       "shared_secret": "HxnrKr1yBDpBEOyLG9P1dy6YzG0UM6x17RQJuwKMhCRmhzaR"
  ##     },
  ##     "url": "https://support.zd-dev.com/api/v2/mobile_sdk_apps/48858f1902454e62737ba86db7d4807dfcd39165249f631f.json"
  ##   }
  ## }
  ## ```
  allow_parameters :update, id: Parameters.string, mobile_sdk_app: MOBILE_SDK_APP_PARAMETER
  def update
    params.require(:mobile_sdk_app).permit(
      :title,
      :brand_id,
      :signup_source,
      settings: MOBILE_SDK_APP_PARAMETER[:settings].keys,
      mobile_sdk_auth: MOBILE_SDK_APP_PARAMETER[:mobile_sdk_auth].keys
    )

    settings_params = params[:mobile_sdk_app].delete(:settings)

    # Please refer to capabilities file to see what `mobile_sdk_rma_tab_removal` flag means
    if current_account.has_mobile_sdk_rma_tab_removal?
      # If user has already enabled the RMA setting,
      # we are allowing to keep the rma_enabled param as is
      settings_params[:rma_enabled] = false unless mobile_sdk_app.settings.rma_enabled
    end

    auth_params = params[:mobile_sdk_app].delete(:mobile_sdk_auth)

    mobile_sdk_app.update_attributes(
      title: params[:mobile_sdk_app][:title],
      brand_id: params[:mobile_sdk_app][:brand_id],
      signup_source: params[:mobile_sdk_app][:signup_source],
      settings: process_settings(settings_params)
    )

    auth = mobile_sdk_app.mobile_sdk_auth
    auth.update_attribute(:endpoint, auth_params[:endpoint])
    mobile_sdk_app.save!

    render json: presenter.present(mobile_sdk_app), status: :ok
  end

  ## ### Delete a mobile SDK app
  ## `DELETE /api/v2/mobile_sdk_apps/{identifier}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admin
  ##
  ## #### Using curl
  ##
  ## Delete a mobile SDK app:
  ##
  ## ```bash
  ## curl -X DELETE -H "Content-Type: application/json" \
  ## -H "Authorization: Basic YWRtaW5AemVuZGVzay5jb206MTIzNDU2" \
  ## "https://support.zd-dev.com/api/v2/mobile_sdk_apps/48858f1902454e62737ba86db7d4807dfcd39165249f631f.json"
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204
  ##
  ## ```
  allow_parameters :destroy, id: Parameters.string
  def destroy
    mobile_sdk_app.destroy
    default_delete_response
  end

  private

  def presenter
    @presenter ||= Api::V2::MobileSdkAppPresenter.new(current_user, url_builder: self)
  end

  def mobile_sdk_apps
    @mobile_sdk_apps ||= current_account.mobile_sdk_apps
  end

  def mobile_sdk_app
    @mobile_sdk_apps ||= current_account.mobile_sdk_apps.find_by_identifier!(params[:id])
  end

  def build_auth(app)
    app.mobile_sdk_auth = current_account.mobile_sdk_auths.build.tap do |auth|
      auth.name = "Identity Provider for #{app.title}"
      auth.generate_shared_secret
      auth.client = current_account.clients.build.tap do |client|
        client.name = "Client for #{app.title} App"[0..250]
        client.identifier = random_client_identifier
        client.generate_secret
      end
    end
  end

  def build_auth_with_jwt(app, auth_params)
    auth = build_auth app
    auth.endpoint = auth_params[:endpoint]
    auth
  end

  def process_settings(settings)
    # TODO: will be deleted when the view will use proper tag fields
    settings[:contact_us_tags] = serialize_tags(settings[:contact_us_tags])
    settings[:rma_tags] = serialize_tags(settings[:rma_tags])

    settings[:push_notifications_enabled] = [MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP, MobileSdkApp::PUSH_NOTIF_TYPE_WEBHOOK].include?(settings[:push_notifications_type])

    settings[:push_notifications_type] = nil unless settings[:push_notifications_enabled]

    if account_cannot_hide_closed_requests?(current_account)
      settings.delete(:support_show_closed_requests)
    end

    # Hiding the referrer logo it's only available to Enterprise
    if account_cannot_hide_referrer_logo?(current_account)
      settings.delete(:support_show_referrer_logo)
    end

    unless current_account.has_mobile_sdk_answer_bot?
      settings.delete(:answer_bot)
    end

    unless current_account.has_customer_satisfaction_enabled?
      settings.delete(:support_show_csat)
    end

    settings
  end

  def random_client_identifier
    "mobile_sdk_client_#{SecureRandom.hex(10)}"
  end

  def serialize_tags(text)
    text.to_s.split(/[,\s]+/).map(&:strip).uniq
  end
end
