module Api::V2
  class Tracking::SupportUserPropertiesController < BaseController
    before_action :require_agent!

    # `Get /api/v2/tracking/support_user_properties.json`
    #
    # #### Allowed For
    #  * Admins
    #  * Agents
    #
    # #### Using Curl
    #
    # ```bash
    # curl http://trial.localhost:3000/api/v2/tracking/support_user_properties.json \
    #   -H "Content-Type: application/json" \
    #   -v -u username:password
    # ```
    #
    # #### Example Response For Success
    #
    # ```http
    # Status: 200 OK
    # {
    #   "user_id":"10027",
    #   "support_language":"English",
    #   "support_locale_id":1,
    #   "support.locale": "en-US",
    #   "support_is_admin":true,
    #   "support_is_end_user":false,
    #   "support_is_owner":true,
    #   "support_user_role":"Admin",
    #   "support_assuming":false
    # }
    # ```

    allow_parameters :index, {}

    def index
      prop = current_account.support_user_properties(current_user.id)
      prop[:support_assuming] = is_assuming_user? || is_assuming_user_from_monitor?
      render json: prop
    end
  end
end
