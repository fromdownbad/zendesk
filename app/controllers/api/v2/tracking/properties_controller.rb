module Api::V2
  class Tracking::PropertiesController < BaseController
    before_action :require_agent!

    # `Get /api/v2/tracking/properties.json`
    #
    # #### Allowed For
    #
    #  * Admins
    #  * Agents
    #
    # #### Using curl
    #
    # ```bash
    # curl http://trial.localhost:3000/api/v2/tracking/properties.json \
    #   -H "Content-Type: application/json" \
    #   -v -u username:password
    # ```
    #
    # #### Example Response For Success
    #
    # ```http
    # Status: 200 OK
    # {
    #   "subdomain":"trial",
    #   "help_desk_size":"Small team",
    #   "customer_type":"VSB",
    #   "account_created":"2015-04-23T18:36:16",
    #   "account_created_week":"2015-16W",
    #   "account_created_month":"2015Apr",
    #   "locale_id":1,
    #   "language":"English",
    #   "account_id":44,
    #   "user_id":10027,
    #   "signup_source":"mobile",
    #   "is_partner":false,
    #   "identity":"44_10027",
    #   "user_role":"Admin",
    #   "created":"2015-04-23T18:36:16",
    #   "created_week":"2015-16W",
    #   "created_month":"2015Apr",
    #   "is_owner":true,
    #   "is_end_user":false,
    #   "account_type":"trial",
    #   "plan_type":3,
    #   "plan_name":"Plus",
    #   "is_trial":true,
    #   "max_agents":5,
    #   "bc_type":4,
    #   "ab_select_plan_at_checkout":"selectable",
    #   "ab_select_plan_at_checkout_finished":false,
    #   "convertroId":"FakeOne",
    #   "assuming":false,
    #   "pod":1234
    # }
    # ```

    allow_parameters :index, {}

    def index
      props = current_account.
        basic_properties(current_user.id).
        merge!(current_account.subscription_properties).
        merge!(current_account.split_testing_properties).
        merge!(current_account.trial_extra_properties).
        merge!(current_account.landing_utm_properties).
        merge!(current_account.voice_subscription_properties).
        merge!(
          assuming: is_assuming
        )

      props[:pod] = current_account.pod_id

      render json: props
    end

    private

    def is_assuming # rubocop:disable Naming/PredicateName
      is_assuming_user? || is_assuming_user_from_monitor?
    end
  end
end
