module Api::V2
  class Tracking::SubscriptionPropertiesController < BaseController
    before_action :require_agent!

    # `Get /api/v2/tracking/properties.json`
    #
    # #### Allowed For
    #
    #  * Admins
    #  * Agents
    #
    # #### Using curl
    #
    # ```bash
    # curl http://trial.localhost:3000/api/v2/tracking/subscription_properties.json \
    #   -H "Content-Type: application/json" \
    #   -v -u username:password
    # ```
    #
    # #### Example Response For Success
    #
    # ```http
    # Status: 200 OK
    # {
    #   "talk_created_at":"2017-12-04T14:55:25",
    #   "talk_created_week":"2017-49W",
    #   "talk_created_month":"2017Dec",
    #   "talk_max_agents":4,
    #   "talk_plan_name":"advanced",
    #   "talk_plan_type":2,
    #   "support_created_at":"2015-04-23T18:36:16",
    #   "support_created_week":"2015-16W",
    #   "support_created_month":"2015Apr",
    #   "support_account_type":"trial",
    #   "support_max_agents":5,
    #   "support_plan_name":3,
    #   "support_plan_type":"Plus",
    #   "guide_created_at":"2015-04-23T18:47:16",
    #   "guide_created_week":"2015-16W",
    #   "guide_created_month":"2015Apr",
    #   "guide_max_agents":5,
    #   "guide_plan_name":"Lite",
    #   "guide_plan_type":1,
    #   "chat_created_at":"2015-04-23T18:50:16",
    #   "chat_created_week":"2015-16W",
    #   "chat_created_month":"2015Apr",
    #   "chat_max_agents":5,
    #   "chat_plan_name":"Lite",
    #   "chat_plan_type":1
    # }
    # ```

    allow_parameters :index, {}

    def index
      props = current_account.support_subscription_properties.
        merge!(current_account.guide_subscription_properties).
        merge!(current_account.talk_subscription_properties).
        merge!(current_account.chat_subscription_properties)

      render json: props
    end
  end
end
