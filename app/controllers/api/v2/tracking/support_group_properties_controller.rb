module Api::V2
  class Tracking::SupportGroupPropertiesController < BaseController
    before_action :require_agent!

    # `Get /api/v2/tracking/support_group_properties.json`
    #
    # #### Allowed For
    #  * Admins
    #  * Agents
    #
    # #### Using Curl
    #
    # ```bash
    # curl http://trial.localhost:3000/api/v2/tracking/support_group_properties.json \
    #   -H "Content-Type: application/json" \
    #   -v -u username:password
    # ```
    #
    # #### Example Response For Success
    #
    # ```http
    # Status: 200 OK
    # {
    #   "account_id": 2,
    #   "account_name": "dev",
    #   "language": "English",
    #   "signup_source": "mobile",
    #   "subdomain": "dev",
    #   "company_size": "Small team"
    # }
    # ```

    allow_parameters :index, {}

    def index
      render json: current_account.support_group_properties
    end
  end
end
