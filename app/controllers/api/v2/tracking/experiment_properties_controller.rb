module Api::V2
  class Tracking::ExperimentPropertiesController < BaseController
    before_action :require_agent!

    # `Get /api/v2/tracking/experiment_properties.json`
    #
    # #### Allowed For
    #  * Admins
    #  * Agents
    #
    # #### Using Curl
    #
    # ```bash
    # curl http://trial.localhost:3000/api/v2/tracking/experiment_properties.json \
    #   -H "Content-Type: application/json" \
    #   -v -u username:password
    # ```
    #
    # #### Example Response For Success
    #
    # ```http
    # Status: 200 OK
    # {
    #   "ab_select_plan_at_checkout":"selectable",
    #   "ab_select_plan_at_checkout_finished":false,
    # }
    # ```

    allow_parameters :index, {}

    def index
      expires_in(30.seconds) if current_account.has_experiment_properties_cache_control_header?

      render json: current_account.split_testing_properties
    end
  end
end
