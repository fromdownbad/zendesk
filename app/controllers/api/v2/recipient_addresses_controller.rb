class Api::V2::RecipientAddressesController < Api::V2::BaseController
  allow_subsystem_user :collaboration, only: [:index]
  before_action :check_channels_permission, except: [:index, :show]

  ## ### List Support Addresses
  ## `GET /api/v2/recipient_addresses.json`
  ##
  ## Lists all the support addresses for the account.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "recipient_addresses": [
  ##     {
  ##       "id":          33,
  ##       "email":       "sales@omniwear.zendesk.com",
  ##       "name":        "Sales",
  ##       "default":     true,
  ##       "brand_id":    123,
  ##       ...
  ##     },
  ##     {
  ##       "id":          34,
  ##       "email":       "marketing@omniwear.com",
  ##       "name":        "Marketing",
  ##       "default":     false,
  ##       "brand_id":    123,
  ##       ...
  ##     },
  ##   ]
  ## }
  ## ```
  allow_parameters :index, {}
  allow_cursor_pagination_v2_parameters :index

  def index
    render json: presenter.present(recipient_addresses)
  end

  ## ### Create Support Address
  ## `POST /api/v2/recipient_addresses.json`
  ##
  ## Adds a Zendesk or external support address to your account.
  ##
  ## To add a Zendesk address, use the following syntax: `{local-part}@{accountname}.zendesk.com`.
  ## Example: 'sales-team@omniwear.zendesk.com'. The [local-part](https://en.wikipedia.org/wiki/Email_address#Local-part)
  ## can be anything you like.
  ##
  ## To add an external email address such as help@omniwearshop.com, the email must
  ## already exist and you must set up forwarding on your email server. The exact steps
  ## depend on your mail server. See
  ## [Forwarding incoming email to Zendesk Support](https://support.zendesk.com/hc/en-us/articles/203663266).
  ## After setting up forwarding, run the [Verify Support Address Forwarding](#verify-support-address-forwarding)
  ## endpoint. The address won't work in Zendesk Support until it's been verified.
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents with permission to manage channels and extensions. See the system permissions in [Creating custom roles and assigning agents (Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662026-Creating-custom-roles-and-assigning-agents-Enterprise-#topic_cxn_hig_bd) in the Support Help Center
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses.json \
  ##   -H "Content-Type: application/json" -X POST \
  ##   -d '{"recipient_address": {"name": "Sales", "email": "help@omniwearshop.com", "default": false, "brand_id": 123 }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 201 Created
  ## Location: https://{subdomain}.zendesk.com/api/v2/recipient_addresses/{id}.json
  ##
  ## {
  ##   "recipient_address": {
  ##     "id":           33,
  ##     "email":        "help@omniwearshop.com",
  ##     "name":         "Sales",
  ##     "default":      false,
  ##     "brand_id":     123,
  ##     "forwarding_status": "waiting",
  ##     "spf_status":   "verified",
  ##     "cname_status": "verified",
  ##     "created_at":   "2017-04-02T22:55:29Z",
  ##     "updated_at":   "2017-04-02T22:55:29Z"
  ##   }
  ## }
  ## ```
  allow_parameters :create, recipient_address: {
    email: Parameters.string,
    name: Parameters.string,
    default: Parameters.boolean,
    brand_id: Parameters.integer,
  }
  def create
    new_recipient_address.save!
    render json: presenter.present(new_recipient_address), status: :created, location: presenter.url(new_recipient_address)
  end

  ## ### Show Support Address
  ## `GET /api/v2/recipient_addresses/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses/{id}.json \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "recipient_address": {
  ##     "id":           33,
  ##     "email":        "help@omniwearshop.com",
  ##     "name":         "Sales",
  ##     "default":      true,
  ##     "brand_id":     123,
  ##     "forwarding_status": "waiting",
  ##     "spf_status":   "unknown",
  ##     "cname_status": "unknown",
  ##     "created_at":   "2017-04-02T22:55:29Z",
  ##     "updated_at":   "2017-04-02T22:55:29Z"
  ##   }
  ## }
  ## ```
  resource_action :show

  ## ### Update Support Address
  ## `PUT /api/v2/recipient_addresses/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents with permission to manage channels and extensions. See the system permissions in [Creating custom roles and assigning agents (Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662026-Creating-custom-roles-and-assigning-agents-Enterprise-#topic_cxn_hig_bd) in the Support Help Center
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses/{id}.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{"recipient_address": {"email": "name2@dmain.com" }}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## Location: https://{subdomain}.zendesk.com/api/v2/recipient_addresses/47.json
  ##
  ## {
  ##   "recipient_address": {
  ##     "id":          33,
  ##     "email":       "help@omniwearshop.com",
  ##     "name":        "Sales",
  ##     "default":     true,
  ##     "brand_id":    123,
  ##     "forwarding_status": "verified",
  ##     "created_at":  "2017-04-02T22:55:29Z",
  ##     "updated_at":  "2017-05-01T27:32:53Z"
  ##   }
  ## }
  ## ```
  ##
  allow_parameters :update, id: Parameters.bigid, recipient_address: {
    email: Parameters.string,
    name: Parameters.string,
    default: Parameters.boolean,
    brand_id: Parameters.integer,
  }
  def update
    recipient_address.update_attributes!(params[:recipient_address])
    render json: presenter.present(recipient_address)
  end

  ## ### Verify Support Address Forwarding
  ## `PUT /api/v2/recipient_addresses/{id}/verify.json`
  ##
  ## Sends a test email to the specified support address to verify that
  ## email forwarding for the address works. An external support address won't
  ##  work in Zendesk Support until it's verified.
  ##
  ## **Note**: You don't need to verify Zendesk support addresses.
  ##
  ## The endpoint takes the following body parameter: `{"type": "forwarding"}`.
  ##
  ## Use this endpoint after [adding](#create-support-address) an external support
  ## address to Zendesk Support and setting up forwarding on your email server.
  ## See [Forwarding incoming email to Zendesk Support](https://support.zendesk.com/hc/en-us/articles/203663266).
  ##
  ## The endpoint doesn't return the results of the test. Instead, use the
  ## [Show Support Address](#show-support-address) endpoint to check that the
  ## `forwarding_status` property is "verified".
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents with permission to manage channels and extensions. See the system permissions in [Creating custom roles and assigning agents (Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662026-Creating-custom-roles-and-assigning-agents-Enterprise-#topic_cxn_hig_bd) in the Support Help Center
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses/{id}/verify.json \
  ##   -H "Content-Type: application/json" -X PUT \
  ##   -d '{"type": "forwarding"}' \
  ##   -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ## ```
  ##
  allow_parameters :verify,
    id: Parameters.bigid,
    type: Parameters.enumeration("forwarding", "spf", "ses", "dns")
  require_oauth_scopes :verify, scopes: [:write], arturo: true
  def verify
    case params[:type]
    when "spf" then recipient_address.verify_spf_status!
    when "dns" then recipient_address.verify_dns_status!
    else recipient_address.verify_forwarding_status!
    end
    render json: {}
  end

  # h3 Determine mail provider for a given email, for example Google / Outlook / Yahoo / Other
  # `GET /api/v2/recipient_addresses/lookup.json?domain={domain}
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses/lookup.json?domain=zendesk.com \
  #   -v -u {email_address}:{password} -X GET
  # ```
  #
  # h4 Example Response
  #
  # ```http
  # Status: 200 OK
  # Location: https://{subdomain}.zendesk.com/api/v2/recipient_addresses/lookup.json?domain=zendesk.com
  #
  # {
  #   "provider": "google"
  # }
  # ```
  allow_parameters :lookup, domain: Parameters.string
  require_oauth_scopes :lookup, scopes: [:read], arturo: true
  def lookup
    provider = mx_provider(params[:domain])
    render json: {provider: provider}
  end

  ## ### Delete Recipient Address
  ## `DELETE /api/v2/recipient_addresses/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##  * Agents with permission to manage channels and extensions. See the system permissions in [Creating custom roles and assigning agents (Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662026-Creating-custom-roles-and-assigning-agents-Enterprise-#topic_cxn_hig_bd) in the Support Help Center
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses/{id}.json \
  ##   -v -u {email_address}:{password} -X DELETE
  ## ```
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 204 No Content
  ## ```
  ##
  resource_action :destroy

  protected

  def presenter
    @presenter ||= Api::V2::RecipientAddressPresenter.new(
      current_user,
      url_builder: self,
      includes: includes,
      with_cursor_pagination: use_cursor_based_pagination?
    )
  end

  def recipient_addresses
    @recipient_addresses ||= begin
      if current_account.has_email_cursor_pagination_recipient_addresses_index?
        if current_account.has_email_recipient_address_enforce_sorting? && use_cursor_based_pagination?
          scope.not_collaboration.order(:created_at).reorder(id: :asc).paginate_with_cursor(
            cursor_pagination_version: 2,
            page: params[:page]
          )
        else
          paginate(scope.not_collaboration)
        end
      else
        if current_account.has_email_recipient_address_enforce_sorting?
          paginate(scope.not_collaboration.order(:created_at))
        else
          paginate(scope.not_collaboration)
        end
      end
    end
  end

  def use_cursor_based_pagination?
    return with_cursor_pagination_v2? if current_account.has_email_cursor_pagination_recipient_addresses_index?
    raise StrongerParameters::InvalidParameter.new(StrongerParameters::IntegerConstraint.new.value(params[:page]), 'page') if HashParam.ish?(params[:page])
  end

  def recipient_address
    @recipient_address ||= current_account.recipient_addresses.not_collaboration.find(params[:id])
  end

  def new_recipient_address
    @new_recipient_address ||= current_account.recipient_addresses.new(params[:recipient_address])
  end

  def scope
    @scope ||= current_account.recipient_addresses
  end

  private

  def check_channels_permission
    deny_access unless current_user.can?(:edit, Access::Settings::Extensions)
  end

  def mx_provider(domain)
    mx_handlers = Resolv::DNS.open.getresources(domain, Resolv::DNS::Resource::IN::MX)
    mx_handlers.each do |host|
      case host.exchange.to_s
      when /google\.com/ then return 'google'
      when /hotmail\.com|outlook\.com/ then return 'outlook'
      when /yahoo/ then return 'yahoo'
      end
    end
    return 'other'
  rescue StandardError
    nil
  end
end
