require 'zendesk/radar_factory'

class Api::V2::AgentsController < Api::V2::BaseController
  include Zendesk::Users::ControllerSupport

  before_action :require_lotus_agent_availability!
  before_action :statsd_increment_api_endpoint_usage, only: :index

  caches_action :index, expires_in: 1.minutes

  allow_parameters :index, :skip # TODO: make stricter
  def index
    # Allows to have 2 data points, one for all requests (in the before_action)
    # and the other one for requests that do not hit the cache (somewhere once
    # every minute for every host, in theory).
    Rails.application.config.statsd.client.increment("#{controller_path}/cache_miss")

    log_lotus_agent_availability_info
    render json: presenter.present(agents_with_shared_sessions)
  end

  private

  def require_lotus_agent_availability!
    deny_access unless current_account.has_lotus_agent_availability?
  end

  def radar_client
    @radar_client ||= ::RadarFactory.create_radar_client(current_account)
  end

  def online_agents
    @online_agents ||= radar_client.presence('agents').get
  end

  def agents
    return [] if online_agents.empty?

    @agents ||= begin
      paginate(many_users(online_agents.keys))
    end
  end

  def sessions_by_user_id
    return {} if online_agents.empty?

    @sessions_by_user_id ||= current_account.shared_sessions.unexpired.where(user_id: agents.map(&:id)).each_with_object({}) do |session, sessions|
      sessions[session.user_id] = true
    end
  end

  def agents_with_shared_sessions
    agents.select do |agent|
      sessions_by_user_id[agent.id]
    end
  end

  def log_lotus_agent_availability_info
    agents_ids = agents.map(&:id)
    shared_session_agents_ids = sessions_by_user_id.keys
    rejected_ids = agents_ids - shared_session_agents_ids

    Rails.logger.info "[LotusAgentAvailability] Radar: " + log_ids(agents_ids)
    Rails.logger.info "[LotusAgentAvailability] Radar & Shared Sessions: " + log_ids(shared_session_agents_ids)
    Rails.logger.info "[LotusAgentAvailability] Rejected: " + log_ids(rejected_ids)
  end

  def log_ids(ids)
    ids.empty? ? 'none' : ids.join(',')
  end

  def presenter
    @presenter ||= Api::V2::AgentAvailabilityPresenter.new(current_user, url_builder: self)
  end
end
