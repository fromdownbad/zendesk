class Api::V2::GooddataUsersController < Api::V2::BaseController
  after_action :track_show, only: [:show]

  ## ### Create a GooddataUser
  ## `POST /api/v2/gooddata_user.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email_address}:{password} -X POST https://{subdomain}.zendesk.com/api/v2/gooddata_user.json \
  ##   -H "Content-Type: application/json" -d "{}"
  ## ```
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "gooddata_user": {
  ##     "integration": {
  ##       "state": "fully_provisioned",
  ##       "portal_sso_url": "https:\/\/secure.gooddata.com\/gdc\/account\/customerlogin?sessionId=0A&targetURL=%2F%23s%3D%2Fgdc%2Fprojects%2Ftz29sbxi0emxhg2yyzobn9g8bktleibp%7CprojectDashboardPage%7C"
  ##       "dashboards": [
  ##         {
  ##           "name": "Advanced Metrics",
  #            "personal": false,
  #            "identifier": "fd98a7",
  ##           "sso_url": "https:\/\/secure.gooddata.com\/gdc\/account\/customerlogin?sessionId=0A&targetURL=%2Fdashboard.html%23project%3D%2Fgdc%2Fprojects%2Ftz29sbxi0emxhg2yyzobn9g8bktleibp%26dashboard%3D%2Fgdc%2Fmd%2Ftz29sbxi0emxhg2yyzobn9g8bktleibp%2Fobj%2F1"
  ##         }
  ##       ]
  ##     },
  ##     "id": 0,
  ##     "account_id": 1,
  ##     "user_id": 6,
  ##     "gooddata_user_id": "-3",
  ##     "gooddata_project_id": "-4",
  ##     "created_at": "2014-02-20T08:21:20Z",
  ##     "updated_at": "2014-02-20T08:21:20Z",
  ##     "url": "https:\/\/support.zendesk.dev\/api\/v2\/gooddata_user"
  ##   }
  ## }
  ## ```
  allow_parameters :create, {}
  def create
    if gooddata_user.present?
      head :conflict
    elsif !gooddata_integration_complete?
      head :precondition_failed
    else
      gooddata_user_provisioning.create_gooddata_user(current_user)

      render json: presenter.present(gooddata_user)
    end
  end

  ## ### Get the GooddataUser, along with current integration status, for the current user
  ## `GET /api/v2/gooddata_user.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/gooddata_user.json \
  ##   -H "Content-Type: application/json" -d "{}"
  ## ```
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "gooddata_user": {
  ##     "integration": {
  ##       "state": "fully_provisioned",
  ##       "portal_sso_url": "https:\/\/secure.gooddata.com\/gdc\/account\/customerlogin?sessionId=0A&targetURL=%2F%23s%3D%2Fgdc%2Fprojects%2Ftz29sbxi0emxhg2yyzobn9g8bktleibp%7CprojectDashboardPage%7C"
  ##       "dashboards": [
  ##         {
  ##           "name": "Advanced Metrics",
  ##           "sso_url": "https:\/\/secure.gooddata.com\/gdc\/account\/customerlogin?sessionId=0A&targetURL=%2Fdashboard.html%23project%3D%2Fgdc%2Fprojects%2Ftz29sbxi0emxhg2yyzobn9g8bktleibp%26dashboard%3D%2Fgdc%2Fmd%2Ftz29sbxi0emxhg2yyzobn9g8bktleibp%2Fobj%2F1"
  ##         }
  ##       ]
  ##     },
  ##     "id": 0,
  ##     "account_id": 1,
  ##     "user_id": 6,
  ##     "gooddata_user_id": "-3",
  ##     "gooddata_project_id": "-4",
  ##     "created_at": "2014-02-20T08:21:20Z",
  ##     "updated_at": "2014-02-20T08:21:20Z",
  ##     "url": "https:\/\/support.zendesk.dev\/api\/v2\/gooddata_user"
  ##   }
  ## }
  ## ```
  allow_parameters :show, {}
  def show
    render json: presenter.present(gooddata_user)
  rescue
    track_show(500)
    raise
  end

  protected

  def track_show(status = 200)
    statsd_client.increment("#{controller_path.gsub(%r{/}, '.')}.show", tags: ["status:#{status}"])
  end

  def presenter
    Api::V2::GooddataUserPresenter.new(current_user, url_builder: self, last_successful_process: last_successful_process)
  end

  def gooddata_user
    GooddataUser.for_user(current_user)
  end

  def gooddata_integration
    current_user.account.try(:gooddata_integration)
  end

  def gooddata_integration_complete?
    gooddata_integration.present? && gooddata_integration.version == 2 && gooddata_integration.complete?
  end

  def last_successful_process
    gooddata_integration.last_successful_process if gooddata_integration_complete?
  end

  def gooddata_user_provisioning
    @gooddata_user_provisioning ||=
      Zendesk::Gooddata::UserProvisioning.new(current_account)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [])
  end
end
