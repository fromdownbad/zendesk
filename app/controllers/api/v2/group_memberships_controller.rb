require 'zendesk/offset_pagination_limiter'

module Api
  module V2
    class GroupMembershipsController < Api::V2::BaseController
      require_that_user_can! :edit, :membership, only: [:destroy, :make_default]
      require_that_user_can! :edit, :new_membership, only: [:create]
      require_that_user_can! :edit, Membership, only: [:destroy_many, :create_many]

      before_action :validate_default_membership, only: [:create_many]

      around_action :handle_rate_limiting_on_deep_offset_pagination, only: [:index]

      allow_zopim_user only: [:index, :show, :create, :create_many, :destroy]
      allow_subsystem_user :bime, only: [:index]
      allow_subsystem_user :knowledge_api, only: [:index]

      group_membership_parameter = {
        user_id: Parameters.bigid,
        group_id: Parameters.bigid,
        default: Parameters.boolean
      }

      ## ### List Memberships
      ##
      ## * `GET /api/v2/group_memberships.json`
      ## * `GET /api/v2/users/{user_id}/group_memberships.json`
      ## * `GET /api/v2/groups/{group_id}/memberships.json`
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For:
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/group_memberships.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "group_memberships": [
      ##     {
      ##       "id":         4,
      ##       "user_id":    29,
      ##       "group_id":   12,
      ##       "default":    true,
      ##       "created_at": "2009-05-13T00:07:08Z",
      ##       "updated_at": "2011-07-22T00:11:12Z"
      ##     },
      ##     {
      ##       "id":         49,
      ##       "user_id":    155,
      ##       "group_id":   3,
      ##       "default":    false,
      ##       "created_at": "2012-03-13T22:01:32Z",
      ##       "updated_at": "2012-03-13T22:01:32Z"
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, user_id: Parameters.bigid, group_id: Parameters.bigid
      allow_cursor_pagination_v2_parameters :index
      def index
        throttle_index_with_deep_offset_pagination
        if current_account.arturo_enabled?('remove_offset_pagination_group_memberships_index') || (current_account.arturo_enabled?('cursor_pagination_group_memberships_index') && !with_offset_pagination?) || with_cursor_pagination_v2?
          if stale_collection?(memberships(paginate_with_offset: false))
            sorted_records = sorted_scope_for_cursor(memberships(paginate_with_offset: false))
            render json: cursor_presenter.present(paginate_with_cursor(sorted_records))
          end
        else
          render json: presenter.present(memberships) if stale_collection?(memberships)
        end
      end

      ## ### List Assignable Memberships
      ##
      ## * `GET /api/v2/group_memberships/assignable.json`
      ## * `GET /api/v2/groups/{group_id}/memberships/assignable.json`
      ##
      ## Returns a maximum of 100 group memberships per page.
      ##
      ## #### Pagination
      ##
      ## * Cursor pagination (recommended)
      ## * Offset pagination
      ##
      ## See [Pagination](./introduction#pagination).
      ##
      ## Returns a maximum of 100 records per page.
      ##
      ## #### Allowed For:
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/group_memberships/assignable.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "group_memberships": [
      ##     {
      ##       "id":         4,
      ##       "user_id":    29,
      ##       "group_id":   12,
      ##       "default":    true,
      ##       "created_at": "2009-05-13T00:07:08Z",
      ##       "updated_at": "2011-07-22T00:11:12Z"
      ##     },
      ##     {
      ##       "id":         49,
      ##       "user_id":    155,
      ##       "group_id":   3,
      ##       "default":    false,
      ##       "created_at": "2012-03-13T22:01:32Z",
      ##       "updated_at": "2012-03-13T22:01:32Z"
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :assignable, group_id: Parameters.bigid
      allow_cursor_pagination_v2_parameters :assignable
      require_oauth_scopes :assignable, scopes: [:read], arturo: true
      def assignable
        if current_account.arturo_enabled?('remove_offset_pagination_group_memberships_assignable') || (current_account.arturo_enabled?('cursor_pagination_group_memberships_assignable') && !with_offset_pagination?) || with_cursor_pagination_v2?
          # We reorder by id here since the secondary ordering by name is unused in this scenario and the CBP gem does not currently support it.
          sorted_records = sorted_scope_for_cursor(assignable_memberships.reorder(id: :asc))
          render json: cursor_presenter.present(paginate_with_cursor(sorted_records))
        else
          render json: presenter.present(assignable_memberships.paginate(pagination))
        end
      end

      ## ### Show Membership
      ## `GET /api/v2/group_memberships/{id}.json`
      ##
      ## `GET /api/v2/users/{user_id}/group_memberships/{id}.json`
      ##
      ## The 'id' is the group membership id, not a group id.
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/group_memberships/{id}.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "group_membership": {
      ##      "id":         4,
      ##      "user_id":    29,
      ##      "group_id":   12,
      ##      "default":    true,
      ##      "created_at": "2009-05-13T00:07:08Z",
      ##      "updated_at": "2011-07-22T00:11:12Z"
      ##   }
      ## }
      ## ```
      allow_parameters :show, id: Parameters.bigid, user_id: Parameters.bigid, group_id: Parameters.bigid
      def show
        render json: presenter.present(membership)
      end

      ## ### Create Membership
      ## `POST /api/v2/group_memberships.json`
      ##
      ## `POST /api/v2/users/{user_id}/group_memberships.json`
      ##
      ## Assigns an agent to a given group.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/group_memberships.json \
      ##   -X POST -d '{"group_membership": {"user_id": 72, "group_id": 88}}' \
      ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 201 Created
      ## Location: https://{subdomain}.zendesk.com/api/v2/group_memberships/{id}.json
      ##
      ## {
      ##   "group_membership": {
      ##      "id":         461,
      ##      "user_id":    72,
      ##      "group_id":   88,
      ##      "default":    true,
      ##      "created_at": "2012-04-03T12:34:01Z",
      ##      "updated_at": "2012-04-03T12:34:01Z"
      ##   }
      ## }
      ## ```
      allow_parameters :create, group_membership: group_membership_parameter
      def create
        new_membership.save!
        render json: presenter.present(new_membership), status: :created, location: presenter.url(new_membership)
      end

      ## ### Bulk Create Memberships
      ## `POST /api/v2/group_memberships/create_many.json`
      ##
      ## Assigns up to 100 agents to given groups.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/group_memberships/create_many.json \
      ##   -X POST -d '{"group_memberships": [{"user_id": 72, "group_id": 88}, {"user_id": 73, "group_id": 88}]}' \
      ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :create_many, group_memberships: [group_membership_parameter]
      def create_many
        raise ActionController::ParameterMissing, "Group memberships attributes must be present" unless params[:group_memberships]

        job = enqueue_job_with_status(GroupMembershipBulkCreateJob, params)
        render json: job_status_presenter.present(job)
      end

      ## ### Delete Membership
      ## `DELETE /api/v2/group_memberships/{id}.json`
      ##
      ## `DELETE /api/v2/users/{user_id}/group_memberships/{id}.json`
      ##
      ## Immediately removes a user from a group and schedules a job to unassign all working tickets
      ## that are assigned to the given user and group combination.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/group_memberships/{id}.json \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 204 No Content
      ## ```
      allow_parameters :destroy, id: Parameters.bigid, user_id: Parameters.bigid, group_id: Parameters.bigid
      def destroy
        UnassignTicketsJob.enqueue(current_account.id, membership.group_id, membership.user_id, current_user.id, ViaType.GROUP_CHANGE)
        membership.destroy
        default_delete_response
      end

      ## ### Bulk Delete Memberships
      ## `DELETE /api/v2/group_memberships/destroy_many.json?ids={group_membership_ids}`
      ##
      ## Immediately removes users from groups and schedules a job to unassign all working tickets
      ## that are assigned to the given user and group combinations.
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/group_memberships/destroy_many.json?ids=1,2,3 \
      ##   -v -u {email_address}:{password} -X DELETE
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :destroy_many, ids: Parameters.ids
      def destroy_many
        if many_ids?
          job = enqueue_job_with_status(GroupMembershipBulkDeleteJob, params.merge(ids: many_ids))
          render json: job_status_presenter.present(job)
        else
          raise ActionController::ParameterMissing, "Group membership ids must be present"
        end
      end

      ## ### Set Membership as Default
      ## `PUT /api/v2/users/{user_id}/group_memberships/{membership_id}/make_default.json`
      ##
      ## #### Allowed For
      ##
      ##  * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/group_memberships/{membership_id}/make_default.json \
      ##   -v -u {email_address}:{password} -X PUT -d '{}' -H "Content-Type: application/json"
      ## ```
      ##
      ## #### Example Response
      ##
      ## Same as [List Memberships](#list-memberships).
      allow_parameters :make_default, id: Parameters.bigid, user_id: Parameters.bigid, group_id: Parameters.bigid
      require_oauth_scopes :make_default, scopes: [:write], arturo: true
      def make_default
        membership.update_attributes!(default: true)
        render json: presenter.present(memberships)
      end

      allow_parameters :count, user_id: Parameters.bigid, group_id: Parameters.bigid
      def count
        render json: { count: record_counter.present, links: { url: request.original_url } }
      end

      protected

      def max_per_page
        1000
      end

      def presenter
        @presenter ||= Api::V2::GroupMembershipPresenter.new(current_user, url_builder: self, includes: includes)
      end

      def cursor_presenter
        @cursor_presenter ||= Api::V2::GroupMembershipPresenter.new(current_user, url_builder: self, includes: includes, with_cursor_pagination: true)
      end

      def job_status_presenter
        @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
      end

      def memberships(paginate_with_offset: true)
        # The scope for group memberships a sql join. It has no default sort. We have to specify it here...
        unpaginated_memberships = membership_scope.memberships.active(current_account.id).order(id: :asc)
        @memberships ||= paginate_with_offset ? unpaginated_memberships.paginate(pagination) : unpaginated_memberships
      end

      def assignable_memberships
        # ...and here too.
        @assignable_memberships ||= membership_scope.memberships.assignable(current_user).order(id: :asc)
      end

      def membership
        @membership ||= membership_scope.memberships.active(current_account.id).find(params[:id])
      end

      def new_membership
        @new_membership ||= membership_scope.memberships.new(params[:group_membership])
      end

      def membership_scope
        @membership_scope ||= finder.membership_scope
      end

      def validate_default_membership
        if params[:group_memberships].try(:select) { |m| m[:default] }.try(:uniq!) { |u| u[:user_id] }
          render json: {
            error: "DuplicateValue",
            description: "default memberships is not unique"
          }, status: :bad_request
        end
      end

      private

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: "group_memberships")
      end

      def throttle_index_with_deep_offset_pagination
        return nil unless params[:page].is_a?(Integer)

        Zendesk::OffsetPaginationLimiter.new(
          entity: controller_name,
          account_id: current_account.id,
          account_subdomain: current_account.subdomain,
          page: params[:page],
          per_page: per_page,
          log_only: true
        ).throttle_index_with_deep_offset_pagination
      end

      def handle_rate_limiting_on_deep_offset_pagination
        yield
      rescue Prop::RateLimited => e
        if e.handle == :index_group_memberships_with_deep_offset_pagination_ceiling_limit
          Rails.logger.info("[GroupMembershipsController] Rate limiting #index by account: #{current_account.subdomain}, page: #{params[:page]}, per_page: #{per_page}, throttle key: index_group_memberships_with_deep_offset_pagination_ceiling_limit")
          statsd_client.increment("api_v2_group_memberships_index_ceiling_rate_limit", tags: ["account_id:#{current_account.id}"])
          response.headers['Retry-After'] = e.retry_after.to_s
          render plain: I18n.t("txt.api.v2.group_memberships.index_with_deep_offset_pagination.rate_limit_by_account") + " per_page: #{params[:per_page]}, page: #{params[:page]}", status: 429, content_type: Mime[:text].to_s
        else
          Rails.logger.warn("Api::V2::GroupMembershipsController Rate Limited #{e.message}")
          raise e
        end
      end

      def finder
        @group_memberships_finder ||= Zendesk::GroupMemberships::Finder.new(
          current_account,
          params
        )
      end

      def record_counter
        @record_counter ||= begin
          Zendesk::RecordCounter::GroupMemberships.new(
            account: current_account,
            options: {
              user_id: params[:user_id],
              group_id: params[:group_id]
            }
          )
        end
      end
    end
  end
end
