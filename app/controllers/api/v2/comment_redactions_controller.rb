class Api::V2::CommentRedactionsController < Api::V2::BaseController
  include Zendesk::Tickets::Comments::EmailCommentSupport

  require_that_user_can! :delete, :ticket, only: :update

  before_action :omni_redaction_arturo?

  allow_parameters :update, id: Parameters.bigid, text: Parameters.string, ticket_id: Parameters.bigid, external_attachment_urls: Parameters.array(Parameters.string)
  require_oauth_scopes :update, scopes: [:write], arturo: true
  def update
    return head(:bad_request) unless ticket.omniredact_comment!(current_user, comment, params[:text], params[:external_attachment_urls] || [])
    render json: presenter.present(comment)
  end

  private

  def omni_redaction_arturo?
    return head(:forbidden) unless current_account.has_agent_workspace_omni_redaction?
  end

  def presenter
    @presenter ||= Api::V2::Tickets::CommentPresenter.new(
      current_user,
      url_builder: self,
      includes: includes,
      relative_attachment_urls: lotus_request?,
      include_inline_images: false
    )
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  def comment
    @comment ||= ticket.comments.find(params[:id])
  end
end
