class Api::V2::MailInlineImagesController < Api::V2::BaseController
  include Zendesk::MailInlineImagesControllerSupport

  require_capability :email_no_delimiter, only: :show

  allow_parameters :show,
    message_id: Parameters.string,
    filename: Parameters.string
  def show
    remote_file = retrieve_remote_file

    return head :not_found unless remote_file.present?

    send_data(
      remote_file.content,
      type: remote_file.content_type,
      filename: remote_file_filename(remote_file)
    )
  end
end
