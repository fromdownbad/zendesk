class Api::V2::AuthBilling::TokensController < Api::V2::BaseController
  before_action :require_admin!

  before_action :increment_statds, only: :create
  before_action :throttle_tokens,  only: :create

  allow_parameters :create, {}
  def create
    render json: presenter.present(base64_encrypt)
  end

  private

  # NOTE: this needs to be a shared key between Classic and Billing. The Zuora
  # API security key for the hosted page is a good candidate, but deploying a
  # new key reserved for this usage (encryption) would be a better choice,
  # however, we are pressed by time to use something readily available.
  SHARED_KEY = ZBC::Zuora::Configuration.page['api_security_key'].freeze

  def base64_encrypt
    @base64_encrypt ||= {
      payload: Base64.urlsafe_encode64(encrypt.encrypted),
      nonce:   Base64.urlsafe_encode64(encrypt.random_iv)
    }
  end

  def encrypt
    @encrypt ||= ZBC::Zendesk::HostedPage::Encrypt.call(
      shared_key: SHARED_KEY,
      subdomain:  current_account.subdomain
    )
  end

  def presenter
    Api::V2::Billing::TokenPresenter.new(
      current_user,
      url_builder: self,
      includes:    includes
    )
  end

  def increment_statds
    statsd_client.increment(
      'generated',
      tags: ["subdomain:#{current_account.subdomain}"]
    )
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(
      namespace: ['credit_card', 'tokens']
    )
  end

  def throttle_tokens
    Prop.throttle!(:zuora_credit_card_tokens, current_account.id)
  rescue Prop::RateLimited
    throttling_enabled = Arturo.feature_enabled_for?(
      :throttle_zuora_credit_card_tokens,
      current_account
    )

    statsd_client.increment(
      'throttled',
      tags: ["subdomain:#{current_account.subdomain}"]
    )

    raise if throttling_enabled
  end
end
