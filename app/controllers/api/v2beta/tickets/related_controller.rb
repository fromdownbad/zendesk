class Api::V2beta::Tickets::RelatedController < Api::V2beta::BaseController
  include ::AllowedParameters

  require_that_user_can! :view, :requested_ticket, only: [:show]

  allow_parameters :show,
    ticket_id: Parameters.integer
  def show
    related = Ticket::Related.new(requested_ticket)
    render json: related.to_json
  end

  protected

  def requested_ticket
    @requested_ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end
end
