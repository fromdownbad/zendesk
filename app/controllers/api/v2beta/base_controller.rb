module Api
  module V2beta
    class BaseController < Api::BaseController
      include ZendeskApi::Controller::FormatEnforcement
      include ZendeskApi::Controller::Sideloading
      include ZendeskApi::Controller::Errors::V2betaHandlers
      include Zendesk::SessionManagement

      skip_before_action :skip_session_persistence
      before_action :require_http_delete!, :only => [ :destroy ]
      before_action :prevent_x_on_behalf_of!
      before_action :require_agent!
      before_action :set_exception_locale

      rescue_from ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation do
        render plain: "Database collision - please try again", status: :conflict
      end

      rescue_from ActiveRecord::RecordInvalid do |exception|
        raise exception if performed? #Helpful in seeing the proper validation error instead of Double Render error

        Rails.logger.error "Model validation error. ActiveRecord::RecordInvalid exception was raised. message:'#{exception.message}', errors:'#{exception.record.errors.full_messages}'"
        Rails.logger.error "Broken record: #{exception.record.class.name} #{exception.record.attributes.map{|k,v| [k, v.to_s.first(32)] }.to_h}"

        locale = if request.headers['X-Force-Exception-Locale']
          ENGLISH_BY_ZENDESK
        else
          @locale_for_exception_rendering
        end

        I18n.with_locale(locale) do
          if exception.record.errors.include?(:safe_update_timestamp)
            render :json => { :error => "UpdateConflict", :description => exception.message }, :status => :conflict
          elsif respond_to?(:presenter, true)
            render :json => presenter.present_errors(exception.record), :status => :unprocessable_entity
          else
            render :json => exception.record.errors.to_json(:version => 2), :status => :unprocessable_entity
          end
        end
      end

      protected

      def restrict_to_change_password
        nil
      end

      def prevent_x_on_behalf_of!
        return if oauth_access_token

        if !!request.headers["X-On-Behalf-Of"]
          render plain: "This API version does not support the X-On-Behalf-Of header. Please use an API token.", status: :bad_request
          Rails.logger.info "Bad Request: This API version does not support the X-On-Behalf-Of header. Please use an API token"
        end
      end

      def require_http_delete!
        head(:method_not_allowed, :allow => 'DELETE') unless request.delete?
      end

      def ensure_proper_protocol
        if !request.ssl? && current_account.try(:ssl_should_be_used?) && !Rails.env.development?
          render plain: "This is an HTTPS only API", status: :forbidden
          Rails.logger.info "403 Forbidden: This is an HTTPS only API"
        end
      end

      def set_exception_locale
        @locale_for_exception_rendering ||= I18n.locale
      end

      def self.require_that_user_can!(ability, class_or_method_name, options = {})
        before_action(options) do |controller|
          obj_or_objs = class_or_method_name.is_a?(Symbol) ? controller.send(class_or_method_name) : class_or_method_name
          current_user = controller.send(:current_user)
          Array(obj_or_objs).each do |o|
            raise Access::Denied.new("Not authorized!", ability, o) unless current_user.can?(ability, o)
          end
        end
      end

      module TicketRendering
        DEFAULT_ORDER = "`tickets`.status_id ASC, `tickets`.created_at DESC"
        DEFAULT_ORDER_WITH_STATUS_HOLD = "FIELD(tickets.status_id, #{Zendesk::Types::StatusType.order.join(',')}) ASC, `tickets`.created_at DESC"

        protected

        # Expects params[:sort_by] and optionally params[:sort_order]
        def order_clause
          Output.new.set_order(params) || (current_account.use_status_hold? ? DEFAULT_ORDER_WITH_STATUS_HOLD : DEFAULT_ORDER)
        end
      end
      include TicketRendering

    end
  end
end
