class Api::V2beta::CrmController < Api::V2beta::BaseController
  include ::AllowedParameters

  before_action :check_integration_enabled!
  before_action :lookup_user_data

  ## ### Show User's CRM information if data isn't outdated OR
  ## ### Show User's outdated CRM information and starts sync job if data is outdated
  ## #### `GET /api/v2/crm/{id}`, `GET /api/v2/crm/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents (account should have CRM integration enabled)
  ##
  ## #### Description
  ##
  ## Returns User's CRM information (Salesforce or Sugar CRM) as defined in Settings -> Extensions -> CRM.
  ##
  ## If data is stale (updated more than one hour ago), status code 202 is returned with stale data and a link
  ## to watch the status of the started sync job to update user's information.
  ##
  ## #### Using curl
  ##
  ##     curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/crm/{id}.json \
  ##       -H "Accept: application/json"
  ##
  ## #### Example JSON Response if data isn't outdated
  ##
  ##     Status: 200 OK
  ##
  ##     {
  ##       "records": [... { ... user's record info ... }, { ... user's record info ... } ...],
  ##     }
  ##
  ## #### Example JSON Response if data isn't outdated
  ##
  ##     Status: 202 Accepted
  ##
  ##     {
  ##       "records": [... { ... user's oudated record info ... }, { ... user's oudated record info ... } ...],
  ##       "message": "Starting request to sync user.",
  ##       "link": "https://{subdomain}.zendesk.com/api/v2/crm/{id}/sync_user_info",
  ##       "state": "pending"
  ##     }
  ##
  allow_parameters :show,
    id: Parameters.bigid
  def show
    if @crm_data.needs_sync?
      @crm_data.start_sync(@user)
      link    = "#{sync_user_info_api_v2beta_crm_url(@user)}.json"
      message = "Starting request to sync user."
      render :json => @crm_data.as_json(:version => 2, :link => link, :message => message), :status => :accepted
    else
      render :json => { :records => @crm_data.records }, :status => :ok
    end
  end

  ## ### Show User's sync job current status
  ## #### `GET /api/v2/crm/{id}/sync_user_info`, `GET /api/v2/crm/{id}/sync_user_info.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents (account should have CRM integration enabled)
  ##
  ## #### Description
  ##
  ## Status of job to update User's CRM information.
  ##
  ## If job is currently being executed 'pending' state is returned. If there was an error,
  ## 'failed' state is returned. If job is finished, status code 303 is returned with a link to the resource
  ## in the body and a location header pointing to the same link.
  ##
  ## #### Using curl
  ##
  ##     curl -v -u {email_address}:{password} https://{subdomain}.zendesk.com/api/v2/crm/{id}/sync_user_info.json \
  ##       -H "Accept: application/json"
  ##
  ## #### Example JSON Response (pending)
  ##
  ##     Status: 200 OK
  ##
  ##     {
  ##       "records": [... { ... user's oudated record info ... }, { ... user's oudated record info ... } ...],
  ##       "message": "Your request is currently being processed.",
  ##       "link": "https://{subdomain}.zendesk.com/api/v2/crm/{id}/sync_user_info",
  ##       "state": "pending"
  ##     }
  ##
  ## #### Example JSON Response (error)
  ##
  ##     Status: 200 Accepted
  ##
  ##     {
  ##       "records": [... { ... user's oudated record info ... }, { ... user's oudated record info ... } ...],
  ##       "message": "Failed to complete the request.",
  ##       "link": "https://{subdomain}.zendesk.com/api/v2/crm/{id}/sync_user_info",
  ##       "state": "failed"
  ##     }
  ##
  ## Note: status code is 200, as state 'failed' is related to the sync job and not the request (whic itself returned OK).
  ##
  ## #### Example JSON Response (done)
  ##
  ##     Status: 303 See Other
  ##     Location: "https://{subdomain}.zendesk.com/api/v2/crm/{id}"
  ##
  ##     {
  ##       "records": [... { ... user's record info ... }, { ... user's record info ... } ...],
  ##       "message": "Your request has been proccessed.",
  ##       "link": "https://{subdomain}.zendesk.com/api/v2/crm/{id}",
  ##       "state": "done"
  ##     }
  ##
  ## Note: if the browser respects the Location header, it should redirect the request and returns status code 200
  ## with the user's current CRM information (the expected result of "https://{subdomain}.zendesk.com/api/v2/crm/{id}").
  ##
  require_oauth_scopes :sync_user_info, scopes: [:"users:read", :read], arturo: true
  allow_parameters :sync_user_info,
    id: Parameters.bigid
  def sync_user_info
    if @crm_data.sync_errored? || @crm_data.sync_pending?
      link = "#{sync_user_info_api_v2beta_crm_url(@user)}.json"
      render :json => @crm_data.as_json(:version => 2, :link => link), :status => :ok
    else
      link = "#{api_v2beta_crm_url(@user)}.json"
      render :json => @crm_data.as_json(:version => 2, :link => link), :location => link, :status => :see_other
    end
  end

  private

  def lookup_user_data
    @user     = current_account.users.find(params[:id])
    @crm_data = @user.crm_data(true) # true means create if doesn't exist
  end

  def check_integration_enabled!
    deny_access unless current_account.crm_integration_enabled?
  end
end
