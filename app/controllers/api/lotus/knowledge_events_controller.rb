class Api::Lotus::KnowledgeEventsController < Api::V2::BaseController
  require_that_user_can! :view, :ticket, only: :create
  allow_parameters :all, ticket_id: Parameters.bigid

  ARTICLE_PARAMS = {
    id: Parameters.bigid,
    title: Parameters.string,
    html_url: Parameters.string,
    url: Parameters.string,
    locale: Parameters.string
  }.freeze

  TEMPLATE_PARAMS = {
    id: Parameters.bigid,
    title: Parameters.string,
    html_url: Parameters.string,
    url: Parameters.string
  }.freeze

  EVENT_TYPES = %w[
    KnowledgeLinked
    KnowledgeCaptured
    KnowledgeFlagged
    KnowledgeLinkAccepted
    KnowledgeLinkRejected
  ].freeze

  allow_parameters :create, article: ARTICLE_PARAMS, knowledge_event: Parameters.enum(*EVENT_TYPES), template: TEMPLATE_PARAMS

  # RAILS5UPGRADE: Remove after upgrading to Rails 5.
  # `to_h` returns a Hash in Rails 4 and a HashWithIndifferentAccess in Rails 5
  def create
    knowledge_event =
      if RAILS4
        ticket.add_knowledge_event event, params[:article], current_user, params[:template]
      else
        ticket.add_knowledge_event event, params[:article].to_h, current_user, params[:template].to_h
      end
    render json: presenter.present(knowledge_event)
  end

  private

  def event
    params[:knowledge_event]
  end

  def presenter
    @presenter ||= Api::Lotus::KnowledgeEventPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end
end
