module Api
  module Lotus
    class ManifestsController < Api::V2::BaseController
      before_action :authorize_version_specification

      # ### Show Lotus Manifest
      # `GET /api/lotus/manifests/:id.json`
      #
      # #### Allowed For:
      #
      #  * Agents
      #
      # Show Lotus Manifest for a given sha (:id). 'current' shows the most recent version
      #
      # #### Request Parameters
      #
      #  * id
      #
      # #### Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/lotus/manifests/current.json \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # {
      #   "createdAt": "2019-02-04T23:08:53Z",
      #   "css": [
      #     {
      #       "name": "vendor",
      #       "path": "/agent/assets/vendor-967a8f6281d02f826b7ee4324c123a9d.css"
      #     }
      #   ],
      #   "js": [
      #     {
      #       "name": "vendor",
      #       "path": "/agent/assets/vendor-4ffa210f27e5867f4d470fc982676558.js"
      #     }
      #   ],
      #   "sha": "4451cd492157de4ee5a43d4d9ce2fa05d6aaa62a",
      #   "version": 2972
      # },
      # ```
      allow_parameters :show, id: Parameters.string
      def show
        render json: manifest_manager.find(params[:id] || :current).manifest
      end

      # ### Show Lotus Versions
      # `GET /api/lotus/manifests/versions.json`
      #
      # #### Allowed For:
      #
      #  * Internal IPs & Integration Network
      #
      # #### Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/lotus/manifests/versions.json \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # [
      #   {
      #     "createdAt": "2019-02-04T23:08:53Z",
      #     "css": [
      #       {
      #         "name": "vendor",
      #         "path": "/agent/assets/vendor-967a8f6281d02f826b7ee4324c123a9d.css"
      #       }
      #     ],
      #     "js": [
      #       {
      #         "name": "vendor",
      #         "path": "/agent/assets/vendor-4ffa210f27e5867f4d470fc982676558.js"
      #       }
      #     ],
      #     "sha": "4451cd492157de4ee5a43d4d9ce2fa05d6aaa62a",
      #     "version": 2972
      #   },
      #   {
      #     "createdAt": "2019-02-04T22:46:17Z",
      #     "css": [
      #       {
      #         "name": "vendor",
      #         "path": "/agent/assets/vendor-967a8f6281d02f826b7ee4324c123a9d.css"
      #       }
      #     ],
      #     "js": [
      #       {
      #         "name": "vendor",
      #         "path": "/agent/assets/vendor-4ffa210f27e5867f4d470fc982676558.js"
      #       }
      #     ],
      #     "sha": "1eecefae35ab72ee1b897625088d84b5147d7a94",
      #     "version": 2971
      #   },
      # ]
      # ```
      allow_parameters :versions, all: Parameters.string
      def versions
        render json: manifest_manager.versions
      end

      private

      def manifest_manager
        @manifest_manager ||= ::Lotus::ManifestManager.new(current_account)
      end

      def authorize_version_specification
        if !Rails.env.development? && (params[:action] == 'versions' || params[:sha].present?)
          authorize_integration_network
        end
      end

      # Brought in from ApplicationController
      def authorize_integration_network
        logger.info "authorize_integration_network: #{request.ip} (#{request.remote_ip})"
        head :forbidden unless internal_request? || integration_network?
      end

      def internal_request?
        Zendesk::Net::IPTools.internal_ip_whitelist_bypass?(request.remote_ip)
      end

      def integration_network?
        Zendesk::Net::IPTools.whitelist_handles_ip?(request.remote_ip, Zendesk::Net.config.integration_networks)
      end
    end
  end
end
