class Api::Lotus::ChatMigrationsController < Api::V2::BaseController
  before_action :require_admin!

  allow_parameters :departments_options, {}
  def departments_options
    response = api_client.get_migration_options(:departments)
    render json: response.body, status: response.status
  end

  allow_parameters :departments_migrate, option: Parameters.string
  def departments_migrate
    response = api_client.start_migration(:departments, departments_migrate_options)
    render json: response.body, status: response.status
  end

  private

  def departments_migrate_options
    { option: params.require(:option) }
  end

  def api_client
    Zopim::InternalApiClient.new(current_account.id)
  end
end
