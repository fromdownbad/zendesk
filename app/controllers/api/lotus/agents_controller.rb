module Api
  module Lotus
    class AgentsController < Api::V2::BaseController
      require_that_user_can! :list, User, only: :assignable

      before_action :require_group

      # ### Show assignable users
      # `GET /api/lotus/groups/:group_id/agents/assignable.json`
      #
      # #### Allowed For
      #
      #  * Agents
      #
      # #### Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/lotus/groups/1234/agents/assignable.json \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # {
      #   "agents": [
      #     {
      #       "id":       11,
      #       "name":     "Agent Extraordinaire"
      #     },
      #     {
      #       "id":       21,
      #       "name":     "Sally Agent 1"
      #     }
      #   ]
      #   next_page: null,
      #   previous_page: null,
      #   "count": 2
      # }
      # ```
      allow_parameters :assignable, group_id: Parameters.bigid

      def assignable
        if stale?(etag: presenter.cache_key(group))
          render json: presenter.present(agents)
        end
      end

      private

      def group
        @group ||= current_account.
          groups.
          assignable(current_user).
          where(id: params[:group_id]).
          first
      end

      def agents
        agents = User.active.agents.
          select('users.id, users.account_id, users.name, users.roles, users.permission_set_id').
          joins(:memberships).
          where(memberships: { group_id: group.id }).
          includes(permission_set: :permissions, account: {})

        current_user.non_light_agent_users(agents)
      end

      def presenter
        @presenter = Api::Lotus::AgentCollectionPresenter.new(current_user, url_builder: self, includes: includes)
      end

      def require_group
        raise ActiveRecord::RecordNotFound if group.nil?
      end
    end
  end
end
