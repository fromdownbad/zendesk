class Api::Lotus::ConversationsController < Api::V2::BaseController
  require_that_user_can! :view_private_content, :ticket

  allow_parameters :all, ticket_id: Parameters.bigid

  # Sort lookup must be defined for the api to honor the `sort_order` parameter.
  SORT_LOOKUP = {}.freeze

  ## ### List Comments and other ticket events which should appear under the conversation
  ## `GET /api/lotus/tickets/{ticket_id}/conversations.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Available parameters
  ##
  ## | Name                  | Type                | Required  | Comments
  ## | --------------------- | --------------------| --------- | -------------------
  ## | `sort_order`          | string              | no        | One of `asc`, `desc`. Defaults to `asc`
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/lotus/tickets/{ticket_id}/conversations.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "conversations": [
  ##     {
  ##       "id": 35436,
  ##       "value": "My ticket comment",
  ##       "type": "Comment",
  ##       ...
  ##     },
  ##     {
  ##       "id": 35437,
  ##       "value": "{ article_ids: [1, 2, 3] }",
  ##       "type": "AutomaticAnswerSend"
  ##       ...
  ##     }
  ##   ]
  ## }
  ##
  ## ```
  allow_parameters :index,
    exclude_chat: Parameters.boolean,
    ticket_id: Parameters.string
  allow_cursor_pagination_parameters :index
  def index
    if stale?(etag: [ticket], last_modified: ticket.updated_at)
      render json: presenter.present(conversation)
    end
  end

  ## ### Show a single comments or other ticket events which appears under the conversation
  ## `GET /api/lotus/tickets/{ticket_id}/conversations/{id}.json`
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl https://{subdomain}.zendesk.com/api/lotus/tickets/{ticket_id}/conversations/{id}.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Example Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "conversation_item": {
  ##     "id": 35436,
  ##     "value": "My ticket comment",
  ##     "type": "Comment",
  ##     ...
  ##   }
  ## }
  ##
  ## ```
  allow_parameters :show, id: Parameters.bigid
  def show
    if stale?(conversation_item)
      render json: presenter.present(conversation_item)
    end
  end

  private

  def presenter
    @presenter ||=
      Api::Lotus::ConversationItemPresenter.new(
        current_user,
        url_builder: self,
        includes: includes,
        with_cursor_pagination: with_cursor_pagination?
      )
  end

  def conversation
    if with_cursor_pagination?
      conversation_items.paginate_with_cursor(
        cursor: params[:cursor],
        limit: params[:limit],
        order: params[:sort_order]
      )
    else
      paginate(conversation_items)
    end
  end

  def conversation_items
    items = ticket.conversation_items

    ## For omnichannel Chat in Support:
    ## Chat comments are excluded from conversations payload (as long as the user has chat entitlements)
    ## since the chat history will be fetched from the Chat GQL endpoint to be rendered in a separate
    ## conversation log.
    if params[:exclude_chat] && chat_entitlements
      items = items.where.not(via_id: ViaType.CHAT)
    end

    items
  end

  def conversation_item
    @conversation_item ||= conversation_items.find(params[:id])
  end

  def chat_entitlements
    @chat_entitlements ||= Zendesk::SupportUsers::Entitlement.for(current_user, Zendesk::Accounts::Client::CHAT_PRODUCT).role
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end
end
