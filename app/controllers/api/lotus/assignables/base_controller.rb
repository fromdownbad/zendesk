class Api::Lotus::Assignables::BaseController < Api::V2::BaseController
  ###########################
  # Api::Lotus::Assignables::BaseController
  #
  # Assignable resources are used in Lotus by the "Assign To"
  # dropdown menu element on the ticket page. This dropdown
  # requires Group and Agent data based on the current user
  # making the request. It presents only groups the current
  # user has permission to assign to and the group's member
  # agents.
  #
  # These endpoints were created to address performance issues
  # being experienced by customers with tens of thousands
  # of agents when rendering the "Assign To" dropdown menu.
  #
  # ####
  #
  # Arturo
  #
  # There is an Arturo flag:
  # `lotus_feature_high_volume_assignable_agents`
  #
  # When false:
  # We currently use `api/lotus/groups_controller#assignable`
  # to render a list of all assignable groups and their
  # agents into the dom on boot.
  #
  # When true:
  # Use `api/lotus/assignables/base#groups` to render a
  # list of all assignable groups (no agents) into the dom
  # on boot.
  #
  # Use `api/lotus/assignables/base#group_agents to send
  # json to the assignables dropdown enabling lazy loading
  # of agents as their groups are selected.
  #
  # ####
  #
  # A Note About REST
  #
  # This diverges with RESTful practices in order to
  # ensapsulate logic related to the "Assign To"
  # dropdown and to address specifc customer performance
  # needs.
  #
  # * `/api/lotus/assignables/groups`
  #   Returns all assignable groups
  #
  # * `/api/lotus/assignables/groups/:id/agents`
  #   Returns a list of all assignable agents who are
  #   members of a specified group.
  #

  include Zendesk::Search::ControllerSupport

  require_that_user_can! :view, Group, only: [:group_agents, :autocomplete]

  before_action :require_group, only: [:group_agents]

  # ### Show assignable groups
  # `GET /api/lotus/assignables/groups.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/lotus/assignables/groups.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "groups": [
  #     {
  #       "id": 10001,
  #       "name": "Support"
  #     },
  #     {
  #       "id": 10002,
  #       "name": "Engineering"
  #     }
  #   ],
  #   next_page: null,
  #   previous_page: null
  #   "count": 2
  # }
  # ```
  allow_parameters :groups, {}

  def groups
    render json: groups_presenter.present(assignable_groups)
  end

  # ### Show a single assignable group
  # `GET /api/lotus/assignables/groups/:group_id/agents`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/lotus/assignables/groups/11/agents.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "agents": [
  #     {
  #       "id":       20,
  #       "name":     "Agent Extraordinaire"
  #     },
  #     {
  #       "id":       21,
  #       "name":     "Sally Agent 1"
  #     }
  #   ],
  #   "next_page": null,
  #   "previous_page": null,
  #   "count": 2
  # }
  # ```
  allow_parameters :group_agents, id: Parameters.bigid

  def group_agents
    render json: agents_presenter.present(group.users.assignable_agents)
  end

  # ### Show assignable groups and agents based on query matched against name
  # `GET /api/lotus/assignables/autocomplete?name={name}.json`
  #
  # #### Allowed For
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/lotus/assignables/autocomplete?name=t.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "agents": [
  #     {
  #       "id":    11,
  #       "name":  "Agent Extraordinaire",
  #       "group": "Support"
  #     },
  #     {
  #       "id":    21,
  #       "name":  "Sally Agent 1",
  #       "group": "Engineering"
  #     }
  #   ],
  #   "groups": [
  #     {
  #       "id": 10001,
  #       "name": "Support"
  #     }
  #   ],
  #   "count": 3
  # }
  # ```
  allow_parameters :autocomplete, name: Parameters.string

  def autocomplete
    if name.empty?
      render json: autocomplete_presenter.present([])
    else
      matching_assignable_agents = agent_query.execute(current_user)
      exception = matching_assignable_agents.exception
      if exception
        render json: {error: 'Unavailable', description: exception.message}, status: :internal_server_error
      else
        render json: autocomplete_presenter.present(matching_assignable_agents + matching_assignable_groups)
      end
    end
  end

  private

  def group
    @group ||= assignable_groups.where(id: params[:id]).first
  end

  def assignable_groups
    @assignable_groups ||= current_user.assignable_groups
  end

  def agents_presenter
    @agents_presenter ||= Api::Lotus::AgentCollectionPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def groups_presenter
    @groups_presenter ||= Api::Lotus::Assignables::GroupsPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def autocomplete_presenter
    @presenter ||= Api::Lotus::Assignables::AutocompletePresenter.new(current_user, url_builder: self, includes: includes)
  end

  def name
    # strip out "." from name
    # https://zendesk.atlassian.net/browse/HAR-1568
    params[:name].tr('.', ' ')
  end

  def agent_query
    query.string = "name:#{name}*" + ' role:admin role:agent -role:"Chat-only agent" -role:"Light agent" -role:"Contributor" group:none ' + query_string_group_ids
    query.endpoint = "users/autocomplete"
    query.incremental = true
    query.source = 'autocomplete'
    query
  end

  def matching_assignable_groups
    assignable_groups.where("name LIKE ?", "%#{name}%")
  end

  def query_string_group_ids
    assignable_groups.map { |group| "group:#{group.id}" }.join(' ')
  end

  def require_group
    raise ActiveRecord::RecordNotFound if group.nil?
  end
end
