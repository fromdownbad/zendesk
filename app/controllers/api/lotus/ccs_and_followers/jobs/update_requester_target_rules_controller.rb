module Api::Lotus
  module CcsAndFollowers::Jobs
    class UpdateRequesterTargetRulesController < Api::V2::BaseController
      before_action :require_admin!

      ## ### Create
      ## `POST /api/lotus/ccs_and_followers/jobs/update_requester_target_rules.json
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Available parameters
      ##
      ## | Name                   | Type                | Required  | Comments
      ## | ---------------------- | --------------------| --------- | -------------------
      ## | `dry_run`              | boolean             | no        | Defaults to true
      ## | `onboarding_completed` | boolean             | no        | Defaults to false
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl -X POST https://{subdomain}.zendesk.com/api/lotus/ccs_and_followers/jobs/update_requester_target_rules.json \
      ##   -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and enqueues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :create, dry_run: Parameters.boolean, onboarding_completed: Parameters.boolean
      def create
        render json: job_status_presenter.present(enqueue_update_requester_target_rules_job(params[:dry_run], params[:onboarding_completed]))
      end

      ## ###
      ## `Get /api/lotus/ccs_and_followers/jobs/update_requester_target_rules.json
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/lotus/ccs_and_followers/jobs/update_requester_target_rules.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "result": {
      ##     "url": "http://{subdomain}.zendesk.com/expirable_attachments/token/hqPNBrDFhLlcwQPbSAKj504Mc/?name=affected_rules_list.zip"
      ##   }
      ## }
      ## ```
      allow_parameters :index, {}
      def index
        render json: presenter.present(affected_rules_list)
      end

      private

      def job_status_presenter
        @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
      end

      def presenter
        @presenter ||= Api::Lotus::CcsAndFollowers::Jobs::UpdateRequesterTargetRulesPresenter.new(current_user.id, url_builder: self, includes: includes)
      end

      def enqueue_update_requester_target_rules_job(dry_run = true, onboarding_completed = false)
        enqueue_job_with_status(::CcsAndFollowers::UpdateRequesterTargetRulesJob, account_id: current_account.id, admin_id: current_user.id, dry_run: dry_run, onboarding_completed: onboarding_completed)
      end

      def affected_rules_list
        current_account.expirable_attachments.latest(::CcsAndFollowers::UpdateRequesterTargetRulesJob.name).first
      end
    end
  end
end
