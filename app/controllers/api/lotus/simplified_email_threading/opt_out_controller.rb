module Api::Lotus
  module SimplifiedEmailThreading
    class OptOutController < Api::V2::BaseController
      include Api::Lotus::SimplifiedEmailThreading::FeedbackSupport

      before_action :require_admin!

      ## ### Create
      ## `POST /api/lotus/simplified_email_threading/opt_out.json
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Available parameters
      ##
      ## | Name                   | Type                | Required  | Comments
      ## | ---------------------- | --------------------| --------- | -------------------
      ## | `dry_run`              | boolean             | no        | Defaults to true
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl -X POST https://{subdomain}.zendesk.com/api/lotus/simplified_email_threading/opt_out.json \
      ##   -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and enqueues a background job to do the work.
      ## Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
      allow_parameters :create, dry_run: Parameters.boolean
      def create
        render json: job_status_presenter.present(enqueue_opt_out_job(params[:dry_run]))
      end

      ## ###
      ## `Get /api/lotus/simplified_email_threading/opt_out.json
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/lotus/simplified_email_threading/opt_out.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "result": {
      ##     "url": "http://{subdomain}.zendesk.com/expirable_attachments/token/hqPNBrDFhLlcwQPbSAKj504Mc/?name=opt_out_affected_rules_list.txt"
      ##   }
      ## }
      ## ```
      allow_parameters :index, {}
      def index
        render json: presenter.present(affected_rules_list)
      end

      ## ###
      ## `POST /api/lotus/simplified_email_threading/opt_out/feedback.json
      ##
      ## #### Allowed For
      ##
      ##  * Admins
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl -v https://{subdomain}.zendesk.com/api/lotus/simplified_email_threading/opt_out/feedback.json \
      ## -X POST \
      ## -u {email_address}:{password} \
      ## -H "Content-Type: application/json" \
      ## -d '{ "feedback": "I cannot change my workflow at this time" }'
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ## ```
      allow_parameters :feedback, feedback: Parameters.string
      def feedback
        response = report_simplified_email_threading_opt_out_feedback

        if response.success?
          json = feedback_presenter.present(response.body["request"])
          render json: json
        else
          head :unprocessable_entity
        end
      rescue StandardError
        head :unprocessable_entity
      end

      private

      def feedback_presenter
        @feedback_presenter ||= Api::Lotus::SimplifiedEmailThreading::FeedbackPresenter.new(current_user, url_builder: self, includes: includes)
      end

      def job_status_presenter
        @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
      end

      def presenter
        @presenter ||= Api::Lotus::SimplifiedEmailThreading::AffectedRulesPresenter.new(current_user.id, url_builder: self, includes: includes)
      end

      def enqueue_opt_out_job(dry_run = true)
        enqueue_job_with_status(::SimplifiedEmailThreading::OptOutJob, account_id: current_account.id, admin_id: current_user.id, dry_run: dry_run)
      end

      def affected_rules_list
        current_account.expirable_attachments.latest(::SimplifiedEmailThreading::OptOutJob.name).first
      end
    end
  end
end
