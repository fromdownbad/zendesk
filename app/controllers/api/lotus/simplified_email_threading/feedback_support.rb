module Api::Lotus::SimplifiedEmailThreading::FeedbackSupport
  REQUEST_TIMEOUT = 30.seconds.freeze

  protected

  def report_simplified_email_threading_opt_out_feedback
    internal_api_client.post(
      'api/v2/requests',
      request: {
        subject: simplified_email_threading_feedback_subject,
        comment: { body: params[:feedback] },
        requester: current_user,
        tags: "simplified_email_threading_feedback"
      }
    )
  end

  def internal_api_client
    Kragle.new('classic') do |conn|
      conn.url_prefix = "https://support.#{Zendesk::Configuration['host']}"
      conn.headers['Content-Type'] = 'application/json'
      conn.options.timeout = REQUEST_TIMEOUT
    end
  end

  def simplified_email_threading_feedback_subject
    "Simplified Email Threading Feedback (#{current_account.subdomain})"
  end
end
