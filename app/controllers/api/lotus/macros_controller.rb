module Api
  module Lotus
    class MacrosController < Api::V2::BaseController
      APPLY_PARAMS = {
        id:          Parameters.integer,
        macro_id:    Parameters.bigid,
        ticket:      Parameters.anything,
        active_chat: Parameters.boolean
      }.freeze

      # ### Show available macros
      # `GET /api/lotus/macros.json`
      #
      # #### Allowed For
      #
      #  * Agents
      #
      # #### Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/lotus/macros.json \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # {
      #   "count": 3,
      #   "macros": [
      #     {
      #       "title": "Mark as Incident",
      #       "raw_title": "{{dc.mark_as_incident}}",
      #       "id": 110,
      #       "availability_type": "everyone"
      #     },
      #     {
      #       "title": "Assign",
      #       "raw_title" "Assign",
      #       "id": 106,
      #       "availability_type": "personal"
      #     },
      #     {
      #       "title": "Close and redirect to topics",
      #       "raw_title": "{{dc.close_and_redirect}}",
      #       "id": 68,
      #       "availability_type": "personal"
      #     }
      #   ],
      #   "most_used": [
      #     {
      #       "title": "Close and redirect to topics",
      #       "raw_title": "{{dc.close_and_redirect}}",
      #       "id": 68,
      #       "availability_type": "personal"
      #     }
      #   ]
      #   next_page: null,
      #   previous_page: null
      # }
      # ```
      allow_parameters :index, {}
      def index
        render json: presenter.present(macros)
      end

      ## ### Apply Macros
      ## `POST /api/lotus/macros/{macro_id}/apply.json`
      ##
      ## Applies a macro to a specific ticket, or to all applicable tickets. Ticket#id is specified by params[:id]
      ##
      ## #### Allowed For:
      ##
      ##   * Agents
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl -X POST https://{subdomain}.zendesk.com/api/lotus/macros/{macro_id}/apply.json \
      ##   -u {email}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```json
      ## {
      ##   "result": {
      ##     "ticket": {
      ##       "id":               35436,
      ##       "url":              "https://company.zendesk.com/api/v2/tickets/35436.json",
      ##       "assignee_id":      235323,
      ##       "group_id":         98738,
      ##       "fields": [
      ##         {
      ##           "id":    27642,
      ##           "value": "745"
      ##         }
      ##       ],
      ##       ...
      ##     },
      ##     "comment": {
      ##       "body": "Assigned to Agent Uno.",
      ##       "scoped_body": [["channel:all", "Assigned to Agent Uno."]],
      ##       "public": false
      ##     }
      ##   }
      ## }
      ## ```
      allow_parameters :apply, APPLY_PARAMS
      require_oauth_scopes :apply, scopes: %i[macros:write write]
      def apply
        if current_user.account.has_authorize_macro_apply? && !bulk_ticket_update? && !ticket.new_record?
          # Need to check user auth against the ticket before changes have been applied in ticket initializer
          clean_ticket = current_user.account.tickets.find_by_nice_id!(params[:id])

          return render json: {}, status: :forbidden unless current_user.can?(:view, clean_ticket)
        end

        render json: app_presenter.present(macro_application)
      end

      private

      def presenter
        @presenter ||= begin
          Api::Lotus::MacroPresenter.new(current_user,
            url_builder: self,
            includes:    includes)
        end
      end

      def app_presenter
        @app_presenter ||= begin
          Api::Lotus::MacroApplicationPresenter.new(current_user,
            url_builder: self,
            ticket: ticket)
        end
      end

      def ticket_initializer
        @ticket_initializer ||= begin
          Zendesk::Tickets::Initializer.new(
            current_account,
            current_user,
            params,
            include: [],
            request_params: request_params
          )
        end
      end

      def ticket
        @ticket ||= bulk_ticket_update? ? nil : ticket_initializer.ticket
      end

      def macro
        @macro ||= scope.find(params[:macro_id])
      end

      def scope
        @scope ||= Zendesk::RuleSelection::Scope.for_context(context)
      end

      def macros
        @macros ||= Zendesk::RuleSelection::Scope.viewable(context)
      end

      def macro_application
        @macro_application ||= begin
          Zendesk::Rules::Macro::Application.new(
            macro:  macro,
            ticket: ticket,
            user:   current_user,
            for_chat: params[:active_chat]
          )
        end
      end

      def context
        @context ||= Zendesk::RuleSelection::Context::Macro.new(current_user)
      end

      def bulk_ticket_update?
        params[:id].to_i == -1
      end
    end
  end
end
