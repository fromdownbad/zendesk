class Api::Lotus::TriggerCategoriesMigrationController < Api::V2::BaseController
  before_action :require_admin!
  before_action :require_trigger_categories, only: :migrate
  before_action :cannot_migrate_if_already_migrated, only: :migrate
  before_action :require_trigger_categories_api, only: :revert

  allow_parameters :migrate, {}
  def migrate
    ActiveRecord::Base.transaction do
      new_category = RuleCategory.create!(
        account: current_account,
        name: I18n.t('txt.admin.trigger.categories.initial_category'),
        rule_type: Trigger,
        position: 1
      )

      current_account.all_triggers.update_all(rules_category_id: new_category.id)

      toggle_setting(true)
    end

    render json: { success: true }
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e,
      location: self.class,
      message: "Trigger categories migration error for account: #{current_account.id}",
      fingerprint: '5522d801364aeb3ba9e671c48d30c17127e86661')

    render json: { errors: [{
      code: 'TriggerCategoriesMigrationFailed',
      title: e
    }]}, status: :unprocessable_entity
  end

  allow_parameters :revert, {}
  def revert
    ActiveRecord::Base.transaction do
      relative_order = current_account.triggers_with_category_position.pluck(:id)

      RuleCategory.trigger_categories(current_account).destroy_all
      current_account.all_triggers.update_all(['position = FIELD(id, ?), rules_category_id = NULL', relative_order])
      toggle_setting(false)
    end

    render json: { success: true }
  rescue StandardError => e
    ZendeskExceptions::Logger.record(e,
      location: self.class,
      message: "Trigger categories revert error for account: #{current_account.id}",
      fingerprint: '0c75dbe44bddf81c0b9692afd68fbad0f4c3b2ab')

    render json: { errors: [{
      code: 'TriggerCategoriesRevertFailed',
      title: e
    }]}, status: :unprocessable_entity
  end

  private

  def toggle_setting(state)
    current_account.update_attributes!(settings: { trigger_categories_api: state })
  end

  def require_trigger_categories
    return if current_account.has_trigger_categories?

    render json: { errors: [{
      code: 'TriggerCategoriesNotEnabled',
      title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.setting_not_enabled')
    }]}, status: :forbidden
  end

  def cannot_migrate_if_already_migrated
    return unless current_account.has_trigger_categories_api_enabled?

    render json: { errors: [{
      code: 'TriggerCategoriesAlreadyMigrated',
      title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.setting_already_enabled')
    }]}, status: :forbidden
  end

  def require_trigger_categories_api
    return if current_account.has_trigger_categories_api_enabled?

    render json: { errors: [{
      code: 'TriggerCategoriesOptedOut',
      title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.not_opted_in')
    }]}, status: :forbidden
  end
end
