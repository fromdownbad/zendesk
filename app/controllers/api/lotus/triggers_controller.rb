module Api::Lotus
  class TriggersController < Api::V2::BaseController
    SEARCH_PARAMETERS = {
      active:     Parameters.boolean,
      filter:     Parameters.string,
      sort_by:    Parameters.string,
      sort_order: Parameters.string
    }.freeze

    ## ### Search Triggers
    ## `GET /api/lotus/triggers/search.json`
    ##
    ## #### Allowed For
    ##
    ##  * Agents
    ##
    ## #### Search Parameters
    ##
    ## Required
    ##
    ## |  Name  | Type   | Comment
    ## | ------ | ------ | -------
    ## | filter | string | Query string used to find all triggers
    ##
    ## Optional
    ##
    ##  You can use any combination of the following optional parameters:
    ##
    ## | Name       | Type    | Comment
    ## | ---------- | ------- | -------
    ## | active     | boolean | Only active triggers if `true`, inactive triggers if `false`
    ## | sort_by    | string  | Possible values are `alphabetical`, `created_at`, `updated_at`, and `position`. If unspecified, the triggers are sorted by relevance
    ## | sort_order | string  | One of `asc` or `desc`. Defaults to `asc` for alphabetical and position sort, `desc` for all others
    ##
    ## #### Sideloads
    ##
    ## The following sideloads are supported:
    ##
    ## | Name             | Will sideload
    ## | ---------------- | -------------
    ## | app_installation | The app installation that requires each trigger, if present
    ## | permissions      | The permissions for each trigger
    ## | usage_1h         | The number of times each trigger has been used in the past hour
    ## | usage_24h        | The number of times each trigger has been used in the past day
    ## | usage_7d         | The number of times each trigger has been used in the past week
    ## | usage_30d        | The number of times each trigger has been used in the past thirty days
    ##
    ## #### Using curl:
    ##
    ## ```bash
    ## curl https://{subdomain}.zendesk.com/api/lotus/triggers/search.json?filter={\"title\":\"close\"} \
    ##   -v -u {email}:{password}
    ## ```
    ##
    ## #### Example Response
    ##
    ## ```http
    ## Status: 200 OK
    ##
    ## {
    ##   "triggers": [
    ##     {
    ##       "id": 25,
    ##       "title": "Close and Save",
    ##       "raw_title": "Close and Save",
    ##       "position": 9,
    ##       "active": true,
    ##       "conditions": [ ... ],
    ##       "actions": [ ... ],
    ##       "description": "Close and save a ticket",
    ##       "updated_at": "2012-09-25T22:50:26Z",
    ##       "created_at": "2012-09-25T22:50:26Z",
    #        "highlights": [ ... ]
    ##     },
    ##     {
    ##       "id": 28,
    ##       "title": "Close and redirect to topics",
    ##       "raw_title": "{{dc.close_and_redirect}}",
    ##       "position": 10,
    ##       "active": true,
    ##       "conditions": [ ... ],
    ##       "actions": [ ... ],
    ##       "updated_at": "2012-09-25T22:50:26Z",
    ##       "created_at": "2012-09-25T22:50:26Z",
    #        "highlights": [ ... ]
    ##     }
    ##   ],
    ##   "previous_page": null,
    ##   "next_page": null,
    ##   "count": 2
    ## }
    ## ```
    allow_parameters :search, SEARCH_PARAMETERS
    require_oauth_scopes :search, scopes: %i[triggers:read read]
    def search
      return head :forbidden unless lotus_request?
      return head :bad_request unless params[:filter].present?

      render json: search_presenter.present(search_result.rules)
    end

    private

    def search_result
      if Arturo.feature_enabled_for?(:new_trigger_search, current_account)
        @search_result ||= Zendesk::RuleSelection::Search::Triggers.for_context(context)
      else
        params[:query] = 'Notify'
        @search_result ||= Zendesk::RuleSelection::Search.for_context(context)
      end
    end

    def search_presenter
      @search_presenter ||= begin
        Api::V2::Rules::TriggerPresenter.new(
          current_user,
          url_builder: self,
          highlights:  search_result.highlights,
          includes:    includes
        )
      end
    end

    def context
      @context ||= begin
        Zendesk::RuleSelection::Context::Trigger.new(
          current_user,
          params.merge(pagination)
        )
      end
    end

    def includes
      @includes ||= begin
        return super if current_account.has_rule_usage_stats?

        super.reject { |sideload| USAGE_SIDELOADS.include?(sideload) }
      end
    end
  end
end
