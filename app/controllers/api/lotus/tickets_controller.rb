module Api
  module Lotus
    class TicketsController < Api::V2::BaseController
      include Zendesk::Tickets::RecentTicketManagement

      MANY_TICKETS_CONDITIONS = "nice_id in (?)".freeze

      # ### Show recent tickets
      # `GET /api/lotus/tickets/recent.json`
      #
      # #### Allowed For
      #
      #  * Agents
      #
      # #### Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/lotus/tickets/recent.json \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # {
      #   "count": 2,
      #   "tickets": [{
      #     "id": 4,
      #     "requester_name":  "Agent Extraordinaire",
      #     "status":          "new",
      #     "subject":         "The most recent of tickets"
      #   },
      #   {
      #     "id": 2,
      #     "requester_name":  "End User",
      #     "status":          "pending",
      #     "subject":         "My printer is on fire!"
      #   })],
      #   "next_page": null,
      #   "previous_page": null
      # }
      # ```
      allow_parameters :recent, {}
      require_oauth_scopes :recent, scopes: %i[tickets:read read], arturo: true
      def recent
        render json: recent_ticket_presenter.present(recent_tickets)
      end

      private

      def ticket
        @ticket ||= Ticket.with_deleted do
          current_account.tickets.find_by_nice_id!(params[:id])
        end
      end

      def tickets
        @tickets ||= Ticket.with_deleted do
          paginate(tickets_scope).to_a
        end
      end

      def tickets_scope
        current_account.deleted_tickets
      end

      def job_status_presenter
        @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
      end

      def recent_ticket_presenter
        @presenter ||= Api::Lotus::RecentTicketPresenter.new(current_user, url_builder: self, includes: includes)
      end

      SORT_LOOKUP = {
        "id" => "nice_id",
        "subject" => "subject",
        "deleted_at" => "updated_at"
      }.freeze
    end
  end
end
