class Api::Lotus::ChatSettingsController < Api::V2::BaseController
  # ### List Chat Settings from Pravda
  # `GET /api/lotus/chat_settings.json`
  #
  # #### Allowed For:
  #
  #  * Agents
  #
  # Lists the account chat settings which are present in Pravda (Account Service).
  #
  # #### Request Parameters
  #
  #  * None
  #
  # #### Using curl
  #
  # ```bash
  # curl https://{subdomain}.zendesk.com/api/lotus/chat_settings.json \
  #   -v -u {email_address}:{password}
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "chat_settings":
  #     {
  #      "phase":      4,
  #      "suite":      false
  #     }
  # }
  # ```
  allow_parameters :index, {}
  def index
    render json: presenter.present(chat_settings)
  end

  private

  def presenter
    @presenter ||= Api::Lotus::ChatSettingsPresenter.new(current_user, url_builder: self, includes: includes)
  end

  def chat_settings
    account_client.product!(Zendesk::Accounts::Client::CHAT_PRODUCT).try(:plan_settings)
  rescue Kragle::ResponseError, Faraday::Error => e
    Rails.logger.error("Unable to fetch chat product record: #{e.message}.")
    nil
  end

  def account_client
    @account_client ||= Zendesk::Accounts::Client.new(current_account)
  end
end
