module Api
  module Lotus
    class ActivitiesController < Api::V2::BaseController
      ## ### List Activities
      ## `GET /api/lotus/activities.json`
      ##
      ## #### Allowed For:
      ##
      ##  * Agents
      ##
      ## Lists activities pertaining to the user performing the request.
      ##
      ## #### Request Parameters
      ##
      ##  * None
      ##
      ## #### Using curl
      ##
      ## ```bash
      ## curl https://{subdomain}.zendesk.com/api/lotus/activities.json \
      ##   -v -u {email_address}:{password}
      ## ```
      ##
      ## #### Example Response
      ##
      ## ```http
      ## Status: 200 OK
      ##
      ## {
      ##   "activities": [
      ##     {
      ##       "id":          35,
      ##       "url":         "https://company.zendesk.com/api/lotus/activities/35.json",
      ##       "verb":        "tickets.assignment",
      ##       "actor_id":    10001,
      ##       "title":       "John Hopeful assigned ticket #123 to you",
      ##       "created_at":  "2012-03-05T10:38:52Z",
      ##       "updated_at":  "2012-03-05T10:38:52Z"
      ##     },
      ##     {
      ##       "id":          45,
      ##       "url":         "https://company.zendesk.com/api/lotus/activities/45.json",
      ##       "verb":        "tickets.comment",
      ##       "actor_id":    10007,
      ##       "title":       "John Hopeful commented in ticket #44",
      ##       "created_at":  "2012-03-05T11:32:44Z",
      ##       "updated_at":  "2012-03-05T11:32:44Z"
      ##     }
      ##   ]
      ## }
      ## ```
      allow_parameters :index, :skip # TODO: make stricter
      def index
        render json: presenter.present(activities)
      end

      private

      def presenter
        @presenter ||= Api::Lotus::TicketActivityPresenter.new(current_user, url_builder: self)
      end

      def activities
        @activities ||= paginate(current_user.activities)
      end
    end
  end
end
