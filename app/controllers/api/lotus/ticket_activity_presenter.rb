class Api::Lotus::TicketActivityPresenter < Api::V2::Presenter
  self.model_key = :activity
  self.url_param = :id

  ## ### JSON Format
  ## Ticket activity events are represented as JSON objects which have the following keys:
  ##
  ## | Name             | Type                   | Read-only | Mandatory | Comment
  ## | ---------------- | ---------------------- | --------- | --------- | -------
  ## | id               | integer                | yes       | no        | Automatically assigned upon creation
  ## | url              | string                 | yes       | no        | The API url of this activity
  ## | verb             | string                 | yes       | yes       | The type of activity. Can be `tickets.assignment`, `tickets.comment`, or  `tickets.priority_increase`
  ## | actor_id         | integer                | yes       | yes       | The id of the actor causing the creation of the activity
  ## | created_at       | date                   | yes       | no        | When this record was created
  ## | updated_at       | date                   | yes       | no        | When this record last got updated
  ## | object           | object                 | yes       | no        | The content of this activity. Can be a ticket, comment, or change.
  ## | target           | object                 | yes       | no        | The target of this activity, a ticket.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":          35,
  ##   "url":         "https://company.zendesk.com/api/lotus/activities/35.json",
  ##   "verb":        "tickets.assignment",
  ##   "actor_id":    10001,
  ##   "title":       "John Hopeful assigned ticket #123 to you",
  ##   "created_at":  "2012-03-05T10:38:52Z",
  ##   "updated_at":  "2012-03-05T10:38:52Z"
  ## }
  ## ```
  def model_json(activity)
    return unless activity.activity_target
    return unless activity.activity_object

    super.merge!(
      id: activity.id,
      verb: activity.verb,
      actor_id: activity.actor_id,
      updated_at: activity.updated_at,
      created_at: activity.created_at,
      object: activity_object(activity),
      target: activity_target(activity)
    )
  end

  def association_preloads
    {
      activity_object: {},
      activity_target: {},
    }
  end

  def side_loads(activities)
    activities = Array(activities).select(&:activity_object)

    actors = side_load_association(activities, :actor, user_presenter)

    minimal_actors = actors.map do |actor|
      {
        id: actor[:id],
        name: actor[:name],
        photo: actor[:photo]
      }
    end

    { actors: minimal_actors }
  end

  private

  def activity_object(activity)
    case activity.verb
    when "tickets.assignment"
      { ticket: { id: activity.activity_object.nice_id, subject: activity.activity_object.title(120) } }
    when "tickets.comment"
      { comment: { value: activity.activity_object.body, public: activity.activity_object.is_public } }
    when "tickets.priority_increase"
      { change: { value_previous: PriorityType[activity.activity_object.value_previous].to_s, value: PriorityType[activity.activity_object.value].to_s } }
    end
  end

  def activity_target(activity)
    { ticket: { id: activity.activity_target.nice_id, subject: activity.activity_target.title(120) } }
  end
end
