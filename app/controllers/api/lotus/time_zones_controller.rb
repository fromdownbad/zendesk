class Api::Lotus::TimeZonesController < Api::V2::TimeZonesController
  # ### List Time Zones
  # `GET /api/lotus/time_zones.json`
  #
  # List all time zones, along with their *current* offset, in minutes.
  # For *full* offset information, including historical and future
  # daylight-savings changes, use `GET /api/v2/time_zones/{id}.json`.
  #
  # The output of this endpoint differs from /api/v2/time_zones.json
  # in that the `url` properties are omitted.
  #
  # #### Allowed For:
  #
  #  * Agents
  #
  # #### Using curl
  #
  # ```bash
  # curl https://assets.zendesk.com/api/lotus/time_zones.json
  # ```
  #
  # #### Example Response
  #
  # ```http
  # Status: 200 OK
  #
  # {
  #   "time_zones": [
  #     {
  #       "name":   "Europe/Copenhagen",
  #       "offset": 60,
  #       "formatted_offset": "GMT+01:00"
  #     },
  #     ...
  #   ]
  # }
  # ```

  private

  def presenter
    Api::Lotus::TimeZonePresenter.new(current_user, options)
  end
end
