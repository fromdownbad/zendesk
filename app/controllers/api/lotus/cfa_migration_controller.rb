class Api::Lotus::CfaMigrationController < Api::V2::BaseController
  before_action :require_admin!
  before_action :require_migration_state, only: :migrate
  before_action :detect_muliple_enabled_installations, only: :migrate
  before_action :require_exit_state, only: :exit

  ## ### Enable Conditional Ticket Fields and migrate conditions from Conditional Fields app
  ## `PUT /api/lotus/cfa_migration/migrate.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -X PUT https://{subdomain}.zendesk.com/api/lotus/cfa_migration/migrate.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Migration Successful Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "success": true
  ## }
  ##
  ## #### Migration Failure Response
  ## ```http
  ## Status: 402 Unprocessable entity
  ##
  ## {
  ##   "description": "Could not migrate from the Conditional Fields app",
  ##   "failed": [
  ##     "message": "Invalid condition. Ticket Form: '11'. Parent Field: '21'. Child Field: '31'. Value: 'some_value'",
  ##     "errors": [
  ##       "Ticket form is invalid",
  ##       "Parent field is invalid",
  ##       "Child field is invalid"
  ##     ],
  ##     "message": "Invalid condition. Ticket Form: '12'. Parent Field: '22'. Child Field: '32'. Value: 'some_value'",
  ##     "errors": [
  ##       "Value is not a value of the parent field.",
  ##       "Parent field is not in the ticket form.",
  ##       "Child field cannot be the same as the parent field."
  ##     ],
  ##     ...
  ##   ]
  ## }
  ##
  ## #### Connection Error Response
  ## ```http
  ## Status: 402 Unprocessable entity
  ##
  ## {
  ##   "description": "We were unable to complete your request. Please try again later. If the problem persists, contact our customer service team.",
  ##   "failed": []
  ## }
  ##
  ## ```
  allow_parameters :migrate, {}
  def migrate
    results = current_account.migrate_cfa_to_ctf
    failed_conditions = results[:failed]

    if failed_conditions.present?
      message = I18n.t('txt.error_message.migrate_cfa_to_ctf.migration_error')
      migration_failed(message, MigrationError.new(message), failed_conditions)
    else
      disable_cfa
      mark_as_migrated
      migration_successful
    end
  rescue StandardError => e
    migration_failed(I18n.t('txt.error_message.migrate_cfa_to_ctf.unknown_error'), e)
  end

  ## ### Exit the Conditional Ticket Fields Early Access Program and re-enable Conditional Fields app
  ## `PUT /api/lotus/cfa_migration/exit.json`
  ##
  ## #### Allowed For
  ##
  ##  * Admins
  ##
  ## #### Using curl
  ##
  ## ```bash
  ## curl -X PUT https://{subdomain}.zendesk.com/api/lotus/cfa_migration/exit.json \
  ##   -H "Content-Type: application/json" -v -u {email_address}:{password}
  ## ```
  ##
  ## #### Exit Successful Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "success": true
  ## }
  ##
  ## #### Exit Failed Response
  ## ```http
  ## Status: 200 OK
  ##
  ## {
  ##   "description": "We were unable to complete your request. Please try again later. If the problem persists, contact our customer service team."
  ## }
  ##
  ## ```
  allow_parameters :exit, {}
  def exit
    enable_cfa
    mark_as_abandoned
    exit_successful
  rescue StandardError => e
    exit_failed(I18n.t('txt.error_message.migrate_cfa_to_ctf.unknown_error'), e)
  end

  private

  def migration_failed(description, exception, failed_conditions = [])
    enable_cfa if cfa_successfully_disabled?
    mark_as_failed

    json = {
      description: description,
      failed: FailedConditionsPresenter.new(failed_conditions).present
    }

    ZendeskExceptions::Logger.record(exception,
      location: self,
      message: "CTF Migration error on account #{current_account.id}: #{json}",
      fingerprint: 'fff622eedaa3a9d07d2d6001351efae4192bbf01')
    statsd_client.increment("migration.failed")

    render_error(json)
  end

  def exit_failed(description, exception)
    ZendeskExceptions::Logger.record(exception,
      location: self,
      message: "CTF exit EAP error on account #{current_account.id}: #{description}",
      fingerprint: '99b805070bfbc4dd446d61238d29d292afc3b03a')
    statsd_client.increment("exit.failed")

    render_error(description: description)
  end

  def migration_successful
    statsd_client.increment("migration.successful")
    render json: { success: true }
  end

  def exit_successful
    statsd_client.increment("exit.successful")
    render json: { success: true }
  end

  def enable_cfa
    update_cfa_installation(true)
  end

  def disable_cfa
    if current_account.cfa_enabled?
      update_cfa_installation(false)

      @cfa_disabled = true
    end
  end

  def cfa_successfully_disabled?
    @cfa_disabled == true
  end

  def update_cfa_installation(enabled)
    response = app_market_client.update_installation(current_account.cfa_installation_id, enabled: enabled)

    # Check that the new state is the one we want
    json = response.body
    unless json["enabled"] == enabled
      # internal message, no need to translate
      raise StandardError, "Could not change Conditional Fields app state to: #{enabled}"
    end

    json
  end

  def mark_as_failed
    update_account_settings(
      native_conditional_fields_migrated: false,
      native_conditional_fields_migration_failed: true
    )
  end

  def mark_as_migrated
    update_account_settings(native_conditional_fields_migrated: true)
  end

  def mark_as_abandoned
    update_account_settings(native_conditional_fields_abandoned: true)
  end

  def update_account_settings(settings)
    current_account.update_attributes!(settings: settings)
  end

  def require_migration_state
    unless current_account.has_native_conditional_fields_migration_needed?
      render_error(
        description: I18n.t('txt.error_message.migrate_cfa_to_ctf.not_ready_to_migrate'),
        failed: []
      )
    end
  end

  def require_exit_state
    unless current_account.has_native_conditional_fields_enabled? && current_account.has_conditional_fields_app_installed?
      render_error(description: I18n.t('txt.error_message.migrate_cfa_to_ctf.not_ready_to_exit'))
    end
  end

  def detect_muliple_enabled_installations
    if current_account.multiple_enabled_cfa_installations?
      render_error(description: I18n.t('txt.error_message.migrate_cfa_to_ctf.multiple_enabled_cfa_installations'))
    end
  end

  def render_error(json)
    render json: json, status: :unprocessable_entity
  end

  def app_market_client
    @app_market_client ||= Zendesk::AppMarketClient.new(current_account.subdomain)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['cfa_migration'])
  end

  class MigrationError < StandardError; end

  class FailedConditionsPresenter
    def initialize(failed_conditions)
      @failed_conditions = failed_conditions
    end

    def present
      @failed_conditions.map { |condition| error(condition) }
    end

    private

    def error(condition)
      message = I18n.t('txt.error_message.migrate_cfa_to_ctf.invalid_condition',
        ticket_form_id: condition.ticket_form_id,
        parent_field_id: condition.parent_field_id,
        child_field_id: condition.child_field_id,
        value: condition.value)

      { message: message, errors: condition.errors.full_messages }
    end
  end
end
