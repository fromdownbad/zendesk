module Api
  module Lotus
    class GroupsController < Api::V2::BaseController
      require_that_user_can! :view, Group, only: [:assignable, :show]

      before_action :require_group, only: [:show]

      # ### Show a single assignable group
      # `GET /api/lotus/groups/:id?include=agents`
      #
      # #### Allowed For
      #
      #  * Agents
      #
      # #### Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/lotus/groups/11 \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # {
      #   "count": 1,
      #   "group": {
      #     "id": 11,
      #     "name": "Support",
      #     "deleted": false,
      #     "created_at": "2015-11-02T21:10:05Z",
      #     "updated_at": "2015-11-02T21:10:05Z",
      #   },
      #   "agents": [
      #     {
      #       "id":       20,
      #       "name":     "Agent Extraordinaire"
      #     },
      #     {
      #       "id":       21,
      #       "name":     "Sally Agent 1"
      #     }
      #   ]
      # }
      # ```
      allow_parameters :show, id: Parameters.bigid

      def show
        render json: presenter.present(group)
      end

      # ### Show assignable groups
      # `GET /api/lotus/groups/assignable.json`
      #
      # #### Allowed For
      #
      #  * Agents
      #
      # #### Using curl
      #
      # ```bash
      # curl https://{subdomain}.zendesk.com/api/lotus/groups/assignable.json \
      #   -v -u {email_address}:{password}
      # ```
      #
      # #### Example Response
      #
      # ```http
      # Status: 200 OK
      #
      # {
      #   "count": 1,
      #   "groups": [{
      #     "id": 11,
      #     "name": "Support",
      #     "agents": [
      #       {
      #         "id":       11,
      #         "name":     "Agent Extraordinaire",
      #         "group_id": 11
      #       },
      #       {
      #         "id":       21,
      #         "name":     "Sally Agent 1",
      #         "group_id": 11
      #       }
      #     ]
      #   }],
      #   next_page: null,
      #   previous_page: null
      # }
      # ```
      allow_parameters :assignable, {}
      def assignable
        if stale?(etag: collection_presenter.cache_key(groups))
          render json: collection_presenter.present(groups)
        end
      end

      private

      def group
        @group ||= groups.where(id: params[:id]).first
      end

      def groups
        @groups ||= current_user.assignable_groups
      end

      def presenter
        @presenter ||= Api::Lotus::GroupPresenter.new(current_user, url_builder: self, includes: includes)
      end

      def collection_presenter
        @collection_presenter ||= Api::Lotus::GroupCollectionPresenter.new(current_user, url_builder: self, includes: includes)
      end

      def require_group
        raise ActiveRecord::RecordNotFound if group.nil?
      end
    end
  end
end
