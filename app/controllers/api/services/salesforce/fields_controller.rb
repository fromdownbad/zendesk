require_dependency 'zendesk/salesforce/api_controller_support'
require_dependency 'zendesk/salesforce/zendesk_fields'
require_dependency 'zendesk/salesforce/app_format_fields'
require_dependency 'zendesk/salesforce/controller_support'

class Api::Services::Salesforce::FieldsController < Api::V2::BaseController
  include Zendesk::Salesforce::ApiControllerSupport
  include Zendesk::Salesforce::ZendeskFields
  include Zendesk::Salesforce::AppFormatFields

  before_action :require_salesforce_configuration!
  before_action :require_one_param!
  around_action :handle_kragle_exception

  allow_parameters :show, type: Parameters.string, object: Parameters.string, relationship_name: Parameters.string
  def show
    render json: { fields: fields }.to_json, status: :ok
  end

  protected

  def object
    params[:object]
  end

  def relationship_name
    params[:relationship_name]
  end

  def type
    params[:type]
  end

  def salesforce_support
    @salesforce_support ||= begin
      Zendesk::Salesforce::ControllerSupport.new(account: current_account, params: params)
    end
  end

  def salesforce_fields?
    (type.present? && type == 'salesforce') || type.blank?
  end

  def salesforce_fields
    return app_format_related_fields if relationship_name.present?
    app_format_fields
  end

  def fields
    return salesforce_fields if salesforce_fields?
    zendesk_fields
  end

  def zendesk_fields
    mappable_zendesk_fields
  end

  def require_one_param!
    raise_bad_request unless params[:object] || (params[:relationship_name] && params[:object]) || params[:type]
  end

  def raise_bad_request
    head :bad_request
  end
end
