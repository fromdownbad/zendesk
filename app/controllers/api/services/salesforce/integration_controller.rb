require_dependency 'zendesk/salesforce/api_controller_support'

class Api::Services::Salesforce::IntegrationController < Api::V2::BaseController
  include Zendesk::Salesforce::ApiControllerSupport

  around_action :handle_kragle_exception

  allow_parameters :status, {}
  def status
    render json: { enabled: ngsfdc.configured_and_enabled? }, status: :ok
  end

  private

  def ngsfdc
    @ngsfdc ||= Salesforce::NextGeneration::Integration.new(current_account)
  end
end
