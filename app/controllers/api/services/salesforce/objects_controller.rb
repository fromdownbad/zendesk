require_dependency 'zendesk/salesforce/api_controller_support'
require_dependency 'zendesk/salesforce/controller_support'

class Api::Services::Salesforce::ObjectsController < Api::V2::BaseController
  include Zendesk::Salesforce::ApiControllerSupport

  before_action :require_salesforce_configuration!
  around_action :handle_kragle_exception

  allow_parameters :index, {}
  def index
    render json: presenter.present(objects), status: :ok
  end

  protected

  def presenter
    @presenter ||= Api::Services::Salesforce::ObjectsPresenter.new(current_user, url_builder: self)
  end

  def salesforce_support
    @salesforce_support ||= begin
      Zendesk::Salesforce::ControllerSupport.new(account: current_account, params: params)
    end
  end

  def objects
    salesforce_support.objects.sort_by { |k| k["label"] }
  end
end
