require_dependency 'zendesk/salesforce/api_controller_support'
require_dependency 'zendesk/salesforce/app_format_configuration'

class Api::Services::Salesforce::ConfigurationController < Api::V2::BaseController
  include Zendesk::Salesforce::ApiControllerSupport
  include Zendesk::Salesforce::AppFormatConfiguration

  before_action :require_salesforce_configuration!
  before_action :require_extensions_permission!

  MAPPING_PARAMETER = {
    zd: {
      key: Parameters.string,
      label: Parameters.string
    }.freeze,
    sf: {
      api_name: Parameters.string,
      label: Parameters.string
    }.freeze
  }.freeze

  FIELDS_PARAMETER = [
    {
      api_name: Parameters.string,
      label: Parameters.string
    }.freeze
  ].freeze

  RELATED_OBJECTS_PARAMETER = [
    {
      label: Parameters.string,
      relationship: Parameters.string,
      object: Parameters.string,
      type: Parameters.string,
      fields: FIELDS_PARAMETER
    }.freeze
  ].freeze

  FILTER_FIELDS_PARAMETER = [
    {
      api_name: Parameters.string,
      data_type: Parameters.string,
      operator: Parameters.string,
      value: Parameters.string
    }.freeze
  ].freeze

  allow_parameters :show, {}
  def show
    render json: presenter.present(app_format), status: :ok
  end

  allow_parameters :save_object, object: {
    api_name: Parameters.string,
    label: Parameters.string,
    mapping: MAPPING_PARAMETER,
    fields: FIELDS_PARAMETER,
    related_objects: RELATED_OBJECTS_PARAMETER
  }
  def save_object
    params.require(:object)
    save_app_format_object(params[:object])
    render json: presenter.present(app_format), status: :ok
  end

  allow_parameters :remove_object, object_name: Parameters.string
  def remove_object
    params.require(:object_name)
    configuration.remove(params[:object_name])
    render json: presenter.present(app_format), status: :ok
  end

  allow_parameters :reorder_objects, objects: [
    {
      api_name: Parameters.string
    }
  ]
  def reorder_objects
    params.require(:objects)
    reorder_app_format_objects(params[:objects])
    render json: presenter.present(app_format), status: :ok
  end

  allow_parameters :save_object_filter, filters: {
    parent: Parameters.string,
    relationship: Parameters.string,
    limit: Parameters.integer,
    filter_fields: FILTER_FIELDS_PARAMETER
  }
  def save_object_filter
    params.require(:filters)
    save_app_format_object_filter(params[:filters])
    render json: presenter.present(app_format), status: :ok
  end

  private

  def configuration
    current_account.salesforce_integration.configuration
  end

  def presenter
    @presenter ||= Api::Services::Salesforce::ConfigurationPresenter.new(current_user, url_builder: self)
  end

  def require_extensions_permission!
    deny_access unless current_user.can?(:edit, ::Access::Settings::Extensions)
  end
end
