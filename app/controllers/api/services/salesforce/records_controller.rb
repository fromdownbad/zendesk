require_dependency 'zendesk/salesforce'
require_dependency 'zendesk/salesforce/controller_support'

class Api::Services::Salesforce::RecordsController < Api::V2::BaseController
  require_capability :salesforce_record_creation
  before_action :require_enabled_salesforce_integration!

  SUCCESS_MSG = "Successfully synced user".freeze
  DUPE_RESULT = "Duplicate records were found".freeze
  RECORD_ALREADY_EXISTING = "Record already exists.".freeze
  SFDC_ENDPOINT_UNAVAILABLE = "Salesforce endpoint unavailable".freeze
  ALLOWED_OBJECT_NAMES = %w[leads contacts].freeze

  ## ### Create contact or lead record in salesforce
  ## `POST /api/services/salesforce/records.json?ticket_id={ticketId}&object_name={object_name}`
  ##
  ## Calls salesforce endpoint to create contact or lead record based ticket's requester
  ##
  ## #### Allowed For
  ##
  ##  * Agents
  ##
  ## #### Example Response
  ##
  ## ```http
  ## Status: 200 OK
  ##
  ## Status: 409 Conflict
  ## Body: Duplicate records were found
  ##
  ## Status: 412 Precondition Failed
  ## Body: Salesforce endpoint unavailable
  ##
  ## Status: 422 Unprocessable Entity
  ## ```

  allow_parameters :create_record, ticket_id: Parameters.bigid, object_name: Parameters.enum(*ALLOWED_OBJECT_NAMES)
  def create_record
    result = send_json(params[:object_name])
    if !result || result.body.blank?
      head :unprocessable_entity
    elsif result.status == 404
      render plain: SFDC_ENDPOINT_UNAVAILABLE, status: :precondition_failed
    elsif result.body.include?(DUPE_RESULT) || result.body.include?(RECORD_ALREADY_EXISTING)
      render plain: DUPE_RESULT, status: :conflict
    elsif result.body.include? SUCCESS_MSG
      head :ok
    else
      render plain: result.body, status: :unprocessable_entity
    end
  end

  protected

  def send_json(object_name)
    json = ticket_json_for_salesforce(ticket)
    endpoint = "/services/apexrest/Zendesk/api/v1/#{object_name}"
    options = {
      headers: { 'Content-Type' => 'application/json' },
      body: json
    }
    current_account.salesforce_integration.http_post(endpoint, options)
  end

  def ticket_json_for_salesforce(ticket)
    original_as_config = ActiveSupport.use_standard_json_time_format
    ActiveSupport.use_standard_json_time_format = true

    Time.use_zone("UTC") { json_presenter.present(ticket).to_json }
  ensure
    ActiveSupport.use_standard_json_time_format = original_as_config
  end

  def json_presenter
    Api::V2::SalesforcePresenter.new(current_user, url_builder: self)
  end

  def ticket
    @ticket ||= current_account.tickets.find_by_nice_id!(params[:ticket_id])
  end

  def require_enabled_salesforce_integration!
    deny_access unless current_account.salesforce_configuration_enabled?
  end
end
