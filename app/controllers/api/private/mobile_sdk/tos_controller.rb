class Api::Private::MobileSdk::TosController < Api::V2::BaseController
  allow_subsystem_user :classic
  before_action :require_admin!

  # ### Allows to accept the mobile SDK Terms of Service
  # `POST api/private/mobile_sdk/tos/accept.json`
  #
  # Allows to accept the mobile SDK Terms of Service. This API endpoint is only meant to be
  # used internally by the mobile SDK Twitter's Fabric integration.
  #
  # #### Allowed For:
  #
  #  * Admins
  #  * Zendesk Internal API system user
  #
  # #### Using curl
  #
  # ```bash
  # curl -X "POST" "https://support.zendesk.com/api/private/mobile_sdk/tos/accept.json" \
  #  -H "Content-Type: application/json" \
  #  -H "Accept: application/json" \
  #  -H "Authorization: Bearer 51b8f8c894514abab0cf4705d414ffd2760589a5dcdb9d2bc812ca0635b402ec"
  #  ```
  # #### Example Response
  #
  # ```http
  # Status: 20O OK
  # ```
  #
  allow_parameters :accept, {}
  def accept
    current_account.settings.allowed_mobile_sdk = true

    if current_account.save(validate: false)
      head :ok
    else
      render json: errors_presenter.present(current_account), status: :unprocessable_entity
    end
  end

  protected

  def account_owner
    current_account.owner
  end

  def errors_presenter
    @errors_presenter ||= Api::V2::ErrorsPresenter.new
  end
end
