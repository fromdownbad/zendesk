# Used by Mobile Core SDK boot process
# Mobile Support SDK 1.X uses `Api::Mobile::Sdk::SettingsController`
class Api::Private::MobileSdk::SettingsV2Controller < Api::Mobile::Sdk::SettingsController
  log_or_throttle_sdk_action_by_account [:show], :mobile_sdk_limit_medium

  protected

  def presenter
    @presenter ||= Api::Private::MobileSdk::SettingsPresenter.new(current_user, presenter_options)
  end

  def presenter_options
    {
      url_builder: self,
      params: params,
      session: session,
      request: request,
      locale: locale
    }
  end

  def locale
    http_accept_language_str = request.env["HTTP_ACCEPT_LANGUAGE"] || request.env["Accept-Language"]
    http_accept_language = HttpAcceptLanguage::Parser.new(http_accept_language_str)

    available_translation_locales = current_account.available_languages
    settle = Zendesk::I18n::LanguageSettlement.new(http_accept_language, available_translation_locales)
    settle.find_matching_zendesk_locale || current_user.translation_locale || current_account.translation_locale
  end
end
