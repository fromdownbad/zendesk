require 'api/v2/users_controller'
require 'zendesk/crm/zendesk_users'

module Api
  module Crm
    module Private
      class UsersController < Api::V2::BaseController
        OLD_ORG_CREATE_PARAM = Api::V2::UsersController::CREATE_USER_PARAMETER.merge(old_organization_id: Parameters.bigid)
        allow_parameters :create_or_update, user: OLD_ORG_CREATE_PARAM
        require_oauth_scopes :create_or_update, scopes: %i[users:write write]
        def create_or_update
          user, is_new_user, was_updated = Zendesk::Crm::ZendeskUsers.update_existing_or_initialize_new_user(current_account, current_user, params[:user] || {})
          return deny_access unless current_user.can?(is_new_user ? :create : :edit, user)

          user.active_brand_id ||= current_brand.id if is_new_user

          user.save!

          output = presenter.present(user)
          output[:user][:was_updated] = was_updated ? 'true' : 'false'

          render json: output, status: is_new_user ? :created : :ok, location: presenter.url(user)
        end

        allow_parameters :create_or_update_many, users: [Api::V2::UsersController::USER_PARAMETER]
        require_oauth_scopes :create_or_update_many, scopes: %i[users:write write read]
        def create_or_update_many
          params[:request_brand_id] = current_brand.id
          render json: job_status_presenter.present(enqueue_job_with_status(CrmZendeskUserBulkCreateJob, params))
        end

        allow_parameters :presenter, {}
        def presenter
          @presenter ||= Api::V2::Users::Presenter.new(current_user, url_builder: self)
        end

        allow_parameters :job_status_presenter, {}
        def job_status_presenter
          @job_status_presenter ||= Api::V2::JobStatusPresenter.new(current_user, url_builder: self)
        end
      end
    end
  end
end
