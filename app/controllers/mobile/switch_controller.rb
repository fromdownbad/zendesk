class Mobile::SwitchController < ApplicationController
  include ::AllowedParameters
  allow_anonymous_users

  allow_parameters :index, return_to: Parameters.string
  def index
    request.toggle_mobile_session!
    params[:return_to] = account_home unless current_account.has_mobile_switch_enabled?
    redirect_to_return_to(account_home)
  end
end
