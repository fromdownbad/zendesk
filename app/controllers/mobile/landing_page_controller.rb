class Mobile::LandingPageController < ApplicationController
  include ::AllowedParameters

  allow_parameters :index, {}
  def index
    flash[:notice] = nil # prevent flash message from being shown
  end
end
