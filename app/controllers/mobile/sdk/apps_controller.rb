# Classic UI for the Mobile SDK admin settings
class Mobile::Sdk::AppsController < Mobile::Sdk::BaseController
  include Zendesk::MobileSdk::AccountsSupport

  before_action :require_admin!
  before_action :check_onboard, only: :index
  helper_method :param_name

  layout 'mobile_sdk'

  allow_parameters :check_onboard, :skip # TODO: make stricter
  def check_onboard
    if scope.empty? && current_account.settings.allowed_mobile_sdk == false
      redirect_to action: 'onboarding'
    end
  end

  def self.param_name
    @param_name ||= ActiveModel::Naming.singular_route_key(MobileSdkApp)
  end

  include Zendesk::LotusEmbeddable

  ssl_required :new, :onboarding, :create, :edit, :update, :destroy, :index, :accept_tos

  app_params = {
    param_name => {
      title: Parameters.string,
      brand_id: Parameters.bigid,
      identifier: Parameters.string,
      settings: {
        contact_us_tags: Parameters.string,
        conversations_enabled: Parameters.boolean,
        rma_enabled: Parameters.boolean,
        rma_visits: Parameters.integer,
        rma_duration: Parameters.integer,
        rma_delay: Parameters.integer,
        rma_tags: Parameters.string,
        helpcenter_enabled: Parameters.boolean,
        rma_android_store_url: Parameters.string,
        rma_ios_store_url: Parameters.string,
        authentication: Parameters.enum(MobileSdkAuth::TYPE_ANONYMOUS, MobileSdkAuth::TYPE_JWT),
        push_notifications_enabled: Parameters.boolean,
        push_notifications_callback: Parameters.string,
        push_notifications_type: Parameters.string,
        urban_airship_key: Parameters.string,
        urban_airship_master_secret: Parameters.string,
        connect_enabled: Parameters.boolean,
        collect_only_required_data: Parameters.boolean,
        support_never_request_email: Parameters.boolean,
        help_center_article_voting_enabled: Parameters.boolean,
        support_show_closed_requests: Parameters.boolean,
        support_show_referrer_logo: Parameters.boolean,
        support_system_message_type: Parameters.string,
        support_system_message: Parameters.string,
        answer_bot: Parameters.boolean,
        support_show_csat: Parameters.boolean,
        talk_enabled: Parameters.boolean,
        talk_button_id: Parameters.string,
        talk_group_id: (Parameters.integer | Parameters.nil_string),
        talk_phone_number_id: (Parameters.integer | Parameters.nil_string)
      },
    },
    :mobile_sdk_auth => {
      endpoint: Parameters.string,
      shared_secret: Parameters.string
    },
    :agree_mobile_sdk_tos => Parameters.boolean
  }

  allow_parameters :onboarding, :skip # TODO: make stricter
  def onboarding
  end

  allow_parameters :accept_tos, agree_mobile_sdk_tos: Parameters.boolean
  def accept_tos
    save_accept_tos
    redirect_to action: 'index'
  end

  allow_parameters :index, :skip # TODO: make stricter
  def index
    @apps = scope

    # Create blank Auths and OAuth Clients to migrate apps on Save
    if current_account.has_mobile_sdk_old_support?
      scope.each do |app|
        unless app.mobile_sdk_auth
          app.mobile_sdk_auth = create_auth(app)
        end
      end
    end

    @blank_app = MobileSdkApp.new
    @blank_app.identifier = @blank_app.generate_identifier
    @blank_app.mobile_sdk_auth = MobileSdkAuth.new
    @blank_app.mobile_sdk_auth.generate_shared_secret
    @blank_app.mobile_sdk_auth.client = current_account.clients.build
    @blank_app.mobile_sdk_auth.client.identifier = random_client_identifier
    # Enable the Help Center feature if HC is setup
    @blank_app.settings.helpcenter_enabled = help_center?
    @blank_app.settings.connect_enabled = false
    @blank_app.settings.conversations_enabled = true
    @blank_app.settings.rma_enabled = false
    @blank_app.brand_id = current_brand.id
    @blank_app.settings.help_center_article_voting_enabled = true if current_account.has_mobile_sdk_help_center_article_voting_enabled?
    @blank_app.settings.support_system_message_type = MobileSdkApp::SYSTEM_MESSAGE_TYPE_DEFAULT

    # Talk call groups and phone numbers
    @groups_phone_numbers = talk_groups_phone_numbers if talk_settings_enabled?
  end

  allow_parameters :create, app_params.merge(blank_client_identifier: Parameters.string)
  def create
    save_accept_tos

    settings = app_instance_params.delete("settings")
    # Please refer to capabilities file to see what `mobile_sdk_rma_tab_removal` flag means
    settings[:rma_enabled] = false if current_account.has_mobile_sdk_rma_tab_removal?

    if settings[:help_center_article_voting_enabled] && !current_account.has_mobile_sdk_help_center_article_voting_enabled?
      settings[:help_center_article_voting_enabled] = false
    end

    @app = scope.build(app_instance_params)

    settings = process_settings(settings)

    if talk_settings_enabled?
      # Do not create app if POST request fails.
      settings = create_talk_config(settings) if talk_settings_set?(settings)

      unless settings
        flash[:error] = flash_error(
          I18n.t('txt.admin.controllers.mobile_sdk.app.error.create'),
          'Talk SDK configuration cannot be saved.'
        )
        (redirect_to(main_app.mobile_sdk_apps_path) && return)
      end
    end

    @auth = create_auth(@app, params["blank_client_identifier"])
    if settings[:authentication] == MobileSdkAuth::TYPE_JWT
      @auth.shared_secret = auth_instance_params["shared_secret"]
      @auth.endpoint = auth_instance_params["endpoint"]
    end
    @auth.client.save!

    if !@auth.save
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.mobile_sdk.app.error.create'), @app.mobile_sdk_auth)
    elsif @app.save
      @app.update_attributes(settings: settings)
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.mobile_sdk.app.error.create'), @app)
    end
    redirect_to mobile_sdk_apps_path
  end

  allow_parameters :edit, id: Parameters.bigid
  def edit
    app
  end

  allow_parameters :update, app_params.merge(id: Parameters.bigid, anchor: Parameters.string)
  def update
    save_accept_tos

    if (settings = app_instance_params.delete("settings"))
      settings = process_settings(settings)

      if talk_settings_set?(settings) && talk_settings_enabled?
        if app.settings.talk_embeddable_id.nil?
          settings = create_talk_config(settings)
        else
          settings[:talk_embeddable_id] = app.settings.talk_embeddable_id
          settings = update_talk_config(settings)
        end

        unless settings
          flash[:error] = flash_error(
            I18n.t('txt.admin.controllers.mobile_sdk.error.update'),
            'Talk SDK configuration cannot be saved.'
          )
          (redirect_to(main_app.mobile_sdk_apps_path) && return)
        end
      else
        settings = settings.reject { |name, _| name.starts_with? 'talk' }
      end
      # Please refer to capabilities file to see what this flag means
      if current_account.has_mobile_sdk_rma_tab_removal?
        # If user has already enabled the RMA,
        # we are allowing to keep the rma_enabled param as is
        settings[:rma_enabled] = false unless app.settings.rma_enabled
      end
      app.update_attributes(settings: settings)
    end
    app.attributes = app_instance_params
    if current_account.has_mobile_sdk_old_support?
      unless app.mobile_sdk_auth
        auth = create_auth(app, params["blank_client_identifier"])
        if settings[:authentication] == MobileSdkAuth::TYPE_JWT
          auth.shared_secret = auth_instance_params["shared_secret"]
          auth.endpoint = auth_instance_params["endpoint"]
        end
      end
    end
    app.mobile_sdk_auth.endpoint = auth_instance_params["endpoint"]

    if !app.mobile_sdk_auth.save
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.mobile_sdk.error.update'), app.mobile_sdk_auth)
    elsif !app.save
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.mobile_sdk.error.update'), app)
    end
    redirect_to main_app.mobile_sdk_apps_path
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    if app.settings.talk_embeddable_id
      unless delete_talk_config(app.settings.talk_embeddable_id)
        flash[:error] = flash_error(
          I18n.t('txt.admin.controllers.mobile_sdk.error.delete'),
          'Talk SDK configuration cannot be deleted.'
        )
        (redirect_to(main_app.mobile_sdk_apps_path) && return)
      end
    end

    app.destroy
    redirect_to mobile_sdk_apps_path
  end

  protected

  def app
    @app ||= scope.find(params[:id])
  end

  def scope
    @scope ||= current_account.mobile_sdk_apps
  end

  def param_name
    self.class.param_name
  end

  def app_instance_params
    @app_instance_params ||= params[param_name]
  end

  def auth_instance_params
    @auth_instance_params ||= params["mobile_sdk_auth"]
  end

  # We get a comma separated list, we should try to convert them to array
  def serialize_tags(text)
    text.split(/[,\s]+/).map(&:strip).uniq
  end

  def help_center?
    current_account.help_center_enabled? && !current_account.help_center_restricted?
  end

  def create_auth(app, client_identifier = nil)
    app.mobile_sdk_auth = current_account.mobile_sdk_auths.build
    app.mobile_sdk_auth.name = "Identity Provider for #{app.title}"
    app.mobile_sdk_auth.generate_shared_secret
    app.mobile_sdk_auth.client = current_account.clients.build
    app.mobile_sdk_auth.client.name = "Client for #{app.title} App"[0..250]
    app.mobile_sdk_auth.client.identifier = client_identifier.presence || random_client_identifier
    app.mobile_sdk_auth.client.generate_secret
    app.mobile_sdk_auth
  end

  def create_talk_config(settings)
    if talk_settings_set?(settings)
      res = talk_client.create_sdk_embeddable(
        app_id: @app.identifier,
        snapcall_button_id: settings[:talk_button_id],
        group_id: settings[:talk_group_id],
        phone_number_id: settings[:talk_phone_number_id],
        enabled: settings[:talk_enabled]
      )

      return nil if res.status != 200

      settings[:talk_embeddable_id] = res.body['talk_embeddable']['id']
      settings
    end
  rescue StandardError => err
    Rails.logger.error "Could not create SDK Talk embeddable: #{err}"
    nil
  end

  def update_talk_config(settings)
    if talk_settings_changed?(settings)
      res = talk_client.update_sdk_embeddable(
        settings[:talk_embeddable_id],
        enabled: settings[:talk_enabled],
        group_id: settings[:talk_group_id],
        snapcall_button_id: settings[:talk_button_id],
        phone_number_id: settings[:talk_phone_number_id]
      )
      return nil if res.status != 200
    end
    settings
  rescue StandardError => err
    Rails.logger.error "Could not update SDK Talk embeddable: #{err}"
    nil
  end

  def delete_talk_config(id)
    res = talk_client.delete_sdk_embeddable(id)
    res.status == 200
  rescue StandardError => err
    Rails.logger.error "Could not delete SDK Talk embeddable: #{err}"
    false
  end

  def talk_settings_changed?(settings)
    (app.settings.talk_enabled != settings[:talk_enabled])     ||
    (app.settings.talk_group_id != settings[:talk_group_id])   ||
    (app.settings.talk_button_id != settings[:talk_button_id]) ||
    (app.settings.talk_phone_number_id != settings[:talk_phone_number_id])
  end

  def talk_client
    @talk_client ||= Zendesk::Voice::InternalApiClient.new(current_account.subdomain)
  end

  def talk_settings_enabled?
    @talk_settings_enabled ||= current_account.settings.voice? && current_account.has_mobile_sdk_talk_settings?
  end

  def talk_settings_set?(settings)
    !!(settings[:talk_button_id] && settings[:talk_group_id] && settings[:talk_phone_number_id])
  end

  def talk_groups_phone_numbers
    @talk_groups_phone_numbers ||= talk_client.fetch_groups_phone_numbers.body['groups']
  rescue StandardError => err
    Rails.logger.error("Unable to fetch groups and phone numbers from Talk: #{err}")
    []
  end

  def random_client_identifier
    "mobile_sdk_client_#{SecureRandom.hex(10)}"
  end

  def save_accept_tos
    if params["agree_mobile_sdk_tos"] == true
      current_account.settings.allowed_mobile_sdk = true
      current_account.save
    end
  end

  def process_settings(settings)
    # TODO: will be deleted when the view will use proper tag fields
    settings[:contact_us_tags] = serialize_tags(settings[:contact_us_tags]) if settings[:contact_us_tags]
    settings[:rma_tags] = serialize_tags(settings[:rma_tags]) if settings[:rma_tags]

    settings[:push_notifications_enabled] = [MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP, MobileSdkApp::PUSH_NOTIF_TYPE_WEBHOOK].include?(settings[:push_notifications_type])

    settings[:push_notifications_type] = nil unless settings[:push_notifications_enabled]

    if account_cannot_hide_closed_requests?(current_account)
      settings.delete(:support_show_closed_requests)
    end

    if account_cannot_hide_referrer_logo?(current_account)
      settings.delete(:support_show_referrer_logo)
    end

    unless current_account.has_mobile_sdk_answer_bot?
      settings.delete(:answer_bot)
    end

    unless current_account.has_customer_satisfaction_enabled?
      settings.delete(:support_show_csat)
    end

    if talk_settings_enabled? && settings[:talk_button_id]
      settings.delete(:talk_button_id) if settings[:talk_button_id].length != 32
    end

    settings
  end
end
