require 'stronger_parameters'

class Mobile::Sdk::BaseController < Settings::BaseController
  helper Rails.application.routes.url_helpers

  # This should eventually go away
  # when parameter_whitelist is in core_application
  begin
    require 'zendesk/parameter_whitelist'
    include ::AllowedParameters
  rescue LoadError
    # We override allow_parameters in test
  end

  allow_parameters :all,
    utf8: Parameters.string,
    _method: Parameters.string,
    commit: Parameters.string

  protected

  def require_admin!
    deny_access unless current_user.try(:is_admin?)
  end
end
