class Mobile::Sdk::AuthsController < Mobile::Sdk::BaseController
  before_action :require_admin!
  helper_method :param_name

  def self.param_name
    @param_name ||= ActiveModel::Naming.singular_route_key(MobileSdkAuth)
  end

  include Zendesk::LotusEmbeddable

  ssl_required :new, :create, :edit, :update, :destroy, :index

  auth_params = {
    param_name => {
      name: Parameters.string,
      endpoint: Parameters.string,
      shared_secret: Parameters.string
    },
    :client => {
      secret: Parameters.string
    },
    :return_to => Parameters.string
  }

  update_auth_params = {
    param_name => {
      name: Parameters.string,
      endpoint: Parameters.string
    },
    :return_to => Parameters.string
  }

  allow_parameters :index, {}
  def index
    @auths = scope
  end

  allow_parameters :new, {}
  def new
    @auth = scope.build
    @auth.generate_shared_secret
    @auth.client = current_account.clients.build
    @auth.client.generate_secret
  end

  allow_parameters :create, auth_params
  def create
    @auth = scope.build(auth_instance_params)

    @auth.client = current_account.clients.build(client_instance_params)
    @auth.client.name = "Client for #{@auth.name}"
    @auth.client.identifier = @auth.client.name.downcase.gsub(/\s/, "_")

    # We need to make sure we are creating a unique identifier
    while !@auth.client.valid? && @auth.client.errors[:identifier]
      @auth.client.identifier << rand(100).to_s
    end

    @auth.client.save!
    if @auth.save
      redirect_to params["return_to"] || edit_mobile_sdk_auth_path(@auth)
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.mobile_sdk.auth.error.create'), @auth)
      render :new
    end
  end

  allow_parameters :edit, id: Parameters.bigid, return_to: Parameters.string
  def edit
    auth
  end

  allow_parameters :generate_secret, id: Parameters.bigid
  def generate_secret
    render plain: auth.generate_shared_secret!, content_type: Mime[:text].to_s
  end

  allow_parameters :update, update_auth_params.merge(id: Parameters.bigid)
  def update
    auth.attributes = auth_instance_params

    if auth.save
      redirect_to params["return_to"] || edit_mobile_sdk_auth_path(auth)
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.mobile_sdk.auth.error.update'), auth)
      render :new
    end
  end

  allow_parameters :destroy, id: Parameters.bigid
  def destroy
    auth.destroy
    redirect_to mobile_sdk_auths_path
  end

  protected

  def auth
    @auth ||= scope.find(params[:id])
  end

  def scope
    @scope ||= current_account.mobile_sdk_auths
  end

  def param_name
    self.class.param_name
  end

  def auth_instance_params
    @auth_instance_params ||= params[param_name]
  end

  def client_instance_params
    @client_instance_params ||= params[ActiveModel::Naming.singular_route_key(Zendesk::OAuth::Client)]
  end
end
