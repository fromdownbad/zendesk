require 'zendesk/tickets/recoverer'

class SuspendedTicketsController < ApplicationController
  include ::AllowedParameters

  before_action :authorize_unrestricted_agent
  before_action :get_bulk_ids, only: [:bulk, :destroy, :recover]

  layout 'agent'

  allow_parameters :index,
    order_by: Parameters.string,
    page: Parameters.string,
    return_to_page: Parameters.string
  def index
    respond_to do |format|
      format.html do
        @suspended_tickets = current_account.suspended_tickets.order(order).paginate(page: get_page)

        if page = return_to_page(@suspended_tickets)
          redirect_to action: 'index', page: page, order_by: params[:order_by]
        end
      end

      format.rss do
        @suspended_tickets = rss_suspended_tickets
        render action: 'index', formats: [:xml], layout: false
      end

      format.mobile_v2 do
        request.toggle_mobile_session!
        redirect_to suspended_tickets_path
      end
    end
  end

  allow_parameters :bulk,
    bulk_ids: Parameters.array(Parameters.bigid),
    id: Parameters.bigid,
    order_by: Parameters.string,
    return_to_page: Parameters.string,
    recover_as: Parameters.string,
    submit_type: Parameters.string
  def bulk
    if params[:submit_type] && params[:submit_type].include?('delete')
      destroy
    else
      recover
    end
  end

  allow_parameters :recover,
    bulk_ids: Parameters.array(Parameters.bigid),
    id: Parameters.bigid,
    order_by: Parameters.string,
    return_to_page: Parameters.string,
    recover_as: Parameters.string,
    submit_type: Parameters.string
  def recover
    if @bulk_ids.blank?
      flash[:error] = flash_error(::I18n.t('txt.admin.controllers.suspended_tickets_controller.select_one_suspended_ticket_to_recover'))
      redirect_to suspended_tickets_path(return_to_page: params[:return_to_page], order_by: params[:order_by])
    elsif recover_as_new_ticket?
      redirect_to new_ticket_path(suspended: @bulk_ids.first)
    else
      suspended_tickets = current_account.suspended_tickets.where(id: @bulk_ids)

      begin
        # In bulk, recover as user MUST be nil.
        recovered = recoverer.recover(suspended_tickets, recover_as_user: recover_as_user)
        suspended_ticket = suspended_tickets.first

        flash[:notice] = if suspended_ticket && recover_as_user
          ::I18n.t('txt.admin.controllers.suspended_tickets_controller.verified_user_recovered_tickets', user_name: recover_as_user.name, user_email: suspended_ticket.from_mail)
        elsif recovered.any?
          ::I18n.t('txt.admin.controllers.suspended_tickets_controller.suspended_tickets_recovered', count: recovered.size)
        else
          ::I18n.t('txt.admin.controllers.suspended_tickets_controller.no_tickets_recovered')
        end
      rescue Zendesk::Tickets::Recoverer::RecoveryFailed => e
        failed = e.failed_tickets
        flash[:error] = render_to_string(partial: 'failed', locals: { failed: failed })
      end

      redirect_to suspended_tickets_path(return_to_page: params[:return_to_page], order_by: params[:order_by])
    end
  end

  allow_parameters :destroy,
    bulk_ids: Parameters.array(Parameters.bigid),
    id: Parameters.bigid,
    order_by: Parameters.string,
    return_to_page: Parameters.string
  def destroy
    if @bulk_ids.blank?
      flash[:error] = flash_error(::I18n.t('txt.admin.controllers.suspended_tickets_controller.select_one_to_delete'))
    else
      SuspendedTicket.bulk_delete(current_account, @bulk_ids)
      flash[:notice] = ::I18n.t('txt.admin.controllers.suspended_tickets_controller.suspended_tickets_deleted', count: @bulk_ids.size)
    end

    redirect_to suspended_tickets_path(return_to_page: params[:return_to_page], order_by: params[:order_by])
  end

  allow_parameters :show, id: Parameters.bigid
  def show
    @suspended_ticket = current_account.suspended_tickets.find(params[:id])

    respond_to do |format|
      format.html { render }
      format.mobile_v2 do
        request.toggle_mobile_session!
        redirect_to suspended_ticket_path(@suspended_ticket.id)
      end
    end
  end

  allow_parameters :assign_to_requester, id: Parameters.bigid
  def assign_to_requester
    @suspended_ticket = current_account.suspended_tickets.find(params[:id])
    if (ticket = @suspended_ticket.ticket)
      # change the author
      @suspended_ticket.author = ticket.requester
      @suspended_ticket.from_name = ticket.requester.name
      @suspended_ticket.from_mail = ticket.requester.email
      @suspended_ticket.save
      # recover it
      @bulk_ids = Array(@suspended_ticket.id)
      recover
    else
      flash[:error] = ::I18n.t('txt.admin.controllers.suspended_tickets_controller.suspended_ticket_no_associated_ticket', ticket_id: @suspended_ticket.id)
      redirect_to action: 'index'
    end
  end

  private

  def recoverer
    @recoverer ||= Zendesk::Tickets::Recoverer.new(user: current_user, account: current_account)
  end

  def all_suspended_ids
    current_account.suspended_tickets.pluck(:id)
  end

  def get_bulk_ids # rubocop:disable Naming/AccessorMethodName
    @bulk_ids = if params[:submit_type] && params[:submit_type].include?('all')
      all_suspended_ids
    elsif params[:id] && !params[:bulk_ids]
      Array(params[:id])
    else
      params[:bulk_ids]
    end
  end

  def recover_as_new_ticket?
    params[:recover_as] && params[:recover_as].include?('new_ticket')
  end

  def recover_as_user
    if params[:recover_as] && params[:recover_as].include?('user:')
      current_account.users.find_by_id(params[:recover_as].split(':')[1])
    end
  end

  def return_to_page(paged_collection)
    return_to_page = params[:return_to_page].to_i

    return nil if return_to_page == 0
    return nil if return_to_page == get_page

    return_to_page > paged_collection.total_pages ? paged_collection.total_pages : return_to_page
  end

  def rss_suspended_tickets
    since = http_if_modified_since(1.month.ago)
    current_account.suspended_tickets.where('created_at > ?', since).limit(100)
  end

  def order
    case params['order_by']
    when 'subject' then { subject: :asc, created_at: :desc }
    when 'sender'  then { from_mail: :asc, created_at: :desc }
    when 'cause'   then "FIELD(cause,#{SuspensionType.order.join(',')})"
    else
      { created_at: :desc } # defaults to 'date'
    end
  end
end
