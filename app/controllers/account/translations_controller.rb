class Account::TranslationsController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_admin
  before_action :authorize_plan

  allow_parameters :index, {}
  def index
    @locales = current_account.available_languages

    respond_to do |format|
      format.xml  { render xml: @locales }
    end
  end

  private

  def authorize_plan
    return if current_account.has_individual_language_selection?

    render_failure(status: :forbidden, title: I18n.t('txt.admin.controllers.account.translations_controller.plan_doesnt_include_individual_language_selection'))
  end
end
