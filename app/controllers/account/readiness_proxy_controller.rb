require 'zendesk/pod_relay'

class Account::ReadinessProxyController < ApplicationController
  include ::AllowedParameters

  skip_before_action :authenticate_user
  skip_before_action :verify_current_account
  skip_before_action :ensure_support_is_active!
  skip_before_action :set_ip_address_for_current_user
  skip_before_action :verify_authenticated_session
  skip_before_action :log_session_info
  skip_before_action :set_mobile_format
  skip_before_action :prepare_payment_notification

  layout false

  helper_method :jwt

  STATE_COMPLETE = 'complete'.freeze
  STATE_PENDING = 'pending'.freeze

  SERVER_CHECK_CACHE_EXPIRY = 10.minutes
  READINESS_CHECK_CACHE_EXPIRY = 1.day

  SCHEME = 'http://'.freeze

  allow_parameters :ready, jwt: Parameters.string, id: Parameters.integer
  def ready
    respond_to do |format|
      format.json { render json: { state: account_readiness_state }, status: :ok }
    end
  end

  private

  def account_readiness_state
    if Rails.cache.read(account_readiness_check_cache_key)
      STATE_COMPLETE
    else
      current_state = STATE_PENDING

      if account_ready?
        Rails.logger.info("Account creation readiness check: completed for #{idsub}")
        Rails.cache.write(account_readiness_check_cache_key, true, expires_in: READINESS_CHECK_CACHE_EXPIRY)
        current_state = STATE_COMPLETE
      end

      statsd_client.increment("account_setup.state.#{current_state}")
      current_state
    end
  end

  def account_ready?
    res = account_service_client.get(account_readiness_path) do |req|
      req.headers['Host'] = "#{subdomain}.#{zendesk_host}"
    end
    res.body['state'] == STATE_COMPLETE
  rescue Kragle::ClientError, Kragle::ResponseError, Faraday::Error => e
    Rails.logger.warn("#{idsub} - Account service #{account_service_server} fetch readiness result: #{e}")
    false
  end

  def account_service_client
    @account_serivce_client ||= Kragle.new('account-service') do |faraday|
      faraday.url_prefix = "#{SCHEME}#{account_service_server}"
      faraday.options.timeout = 5
    end
  end

  def account_service_server
    Zendesk::Configuration.servers('account-service', [], pod_local: true).first
  end

  def jwt
    params[:jwt]
  end

  def account_id
    params[:id]
  end

  def subdomain
    request.host.split('.').first
  end

  def account_readiness_check_cache_key
    "#{account_id}-account-service-readiness-check"
  end

  def account_readiness_path
    "/api/services/accounts/#{account_id}/ready?jwt=#{jwt}"
  end

  def zendesk_host
    @zendesk_host ||= Zendesk::Configuration.fetch(:host)
  end

  def statsd_client
    Rails.application.config.statsd.client
  end

  def idsub
    "#{account_id}:#{subdomain}"
  end
end
