class Account::SubscriptionController < Account::BaseController
  include Zendesk::LotusEmbeddable

  before_action :authorize_admin
  before_action :redirect_to_sponsorship_page, only: [:show, :upgrade_from_trial]
  skip_before_action :ensure_support_is_active!

  ssl_allowed :all

  include AccountHelper::SubscriptionHelper

  allow_parameters :show, {}
  def show
    redirect_to subscription_page_path
  end

  allow_parameters :sponsorship, {}
  def sponsorship
    # NADA
  end

  allow_parameters :upgrade_from_trial, subscription: {
    plan_type: Parameters.integer,
    billing_cycle_type: Parameters.integer,
    max_agents: Parameters.integer
  }
  def upgrade_from_trial
    redirect_to subscription_settings_path
  end

  private

  def redirect_to_sponsorship_page
    subscription = current_account.subscription
    # only redirect to sponsorship page if subscription exists and is NOT editable
    if subscription.present? && !subscription.editable_by_account_owner?
      redirect_to action: :sponsorship
    end
  end

  def subscription_page_path
    current_account.multiproduct? ? '/admin/billing' : subscription_settings_path
  end
end
