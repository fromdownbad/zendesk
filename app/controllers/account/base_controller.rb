class Account::BaseController < ApplicationController
  include ::AllowedParameters
  before_action :authorize_admin
  before_action :set_account_instance

  layout 'agent'
  tab :account

  # There are many potential parameters that could be passed to the account update.  Since it's unknown how and how much this controller
  # is being used, only the parameters from the test is included so we can capture some data while running in simulate mode
  allow_parameters :update,
    account: {
      settings: {
        no_short_url_tweet_append: Parameters.integer
      }
    },
    return_to: Parameters.string
  def update
    if @account.update_attributes(params[:account])
      flash[:notice] = I18n.t('txt.admin.controllers.account.base_controller.account_settings_updated_label')
    else
      flash[:error] = flash_error(I18n.t('txt.admin.controllers.account.base_controller.failed_to_update_account_label'), @account)
    end

    redirect_to_return_to(account_home)
  end

  private

  # Such that we can make form_for(:account)
  def set_account_instance
    @account = current_account
  end
end
