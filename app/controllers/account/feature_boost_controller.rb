class Account::FeatureBoostController < Account::BaseController
  before_action :authorize_owner
  ssl_allowed

  allow_parameters :show, {}
  def show
    redirect_to subscription_settings_path if current_account.feature_boost.present?
  end

  allow_parameters :create,
    is_active: Parameters.boolean,
    activated_at: Parameters.string,
    valid_until: Parameters.string,
    boost_level: Parameters.integer
  def create
    attribs = params.slice(:is_active, :activated_at, :valid_until, :boost_level)
    current_account.create_feature_boost(attribs) unless current_account.feature_boost.present?
    redirect_to subscription_settings_path
  end
end
