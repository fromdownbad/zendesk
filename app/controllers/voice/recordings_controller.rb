class Voice::RecordingsController < Voice::BaseController
  skip_before_action :authenticate_user, only: [:show]
  before_action :find_comment, only: [:show]

  allow_parameters :show,
    ticket_id: Parameters.bigid,
    call_id: Parameters.bigid,
    comment_id: Parameters.bigid,
    sid: Parameters.string
  def show
    redirect_to @recording_url
  end

  private

  def find_comment
    @recording_url = nil

    if params[:ticket_id].present? && params[:comment_id].present?
      ticket = current_account.tickets.find_by_id(params[:ticket_id])
      comment = ticket && ticket.comments.find_by_id(params[:comment_id])

      if comment && comment.recording && comment.recording.matches_sid?(params[:sid])
        @recording_url = comment.internal_recording_url(VoiceComment::FORMAT)
      end
    elsif params[:call_id].present?
      call = current_account.calls.find_by_id(params[:call_id])
      if call
        @recording_url = "#{call.recording_url}.#{VoiceComment::FORMAT}"
      end
    end

    render_failure(status: :not_found, title: '404 Not Found', message: 'No such recording') unless @recording_url

    true
  end
end
