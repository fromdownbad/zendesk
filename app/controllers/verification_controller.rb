require 'zendesk/tickets/recoverer'

class VerificationController < ApplicationController
  include ::AllowedParameters
  include HelpCenterRedirection

  layout 'user'

  skip_before_action :authenticate_user
  before_action      :find_user_for_token, only: [:details, :email]
  before_action      :find_ticket_for_token, only: [:ticket]
  after_action       :save_trial_extras, :save_plan_redirect, only: [:challenge]
  skip_before_action :ensure_support_is_active!, only: [:email, :challenge, :details]

  ssl_required :details

  helper :captcha

  helper_method :current_brand, :current_route

  allow_parameters :index, {} # untested
  def index
    render_failure(
      status: :not_found,
      title: I18n.t('txt.errors.not_found.title'),
      message: I18n.t('txt.errors.not_found.message')
    )
  end

  NEW_OWNER_AUTO_LOGIN_MAX_TIME = 7.days

  # new agents and admins get an email pointing to this action
  # once the agent is verified we use UnverifiedEmailAddressesController to verify new identities
  allow_parameters :email,
    make_primary: Parameters.string,
    token: Parameters.string
  def email
    if @identity.is_verified? && !@user.crypted_password.blank?
      flash[:error] = flash_error(I18n.t('txt.verification.already_verified'))
      redirect_to login_path
    elsif @user.crypted_password.blank?
      verify_user
    else
      if (Time.now - current_account.created_at) < NEW_OWNER_AUTO_LOGIN_MAX_TIME &&
        @user.is_account_owner? && @user.last_login.nil? && @identity.primary?
        self.current_user = @user
        verify_email_and_redirect
      else
        warden.authenticate

        if logged_in?
          if @user == current_user
            verify_email_and_redirect
          else
            render_token_failure
          end
        elsif !@user.is_verified?
          # TODO: Temporary workaround: Unverified users cannot log in with their password. Verify the email first
          # so that they can then log in with their password.
          verify_email_and_redirect
        else
          flash[:notice] = I18n.t('txt.verification.sign_in_to_finish_verifying_email')
          session[:sign_in_to_verify_email_notice] = I18n.t('txt.verification.sign_in_to_finish_verifying_email')
          redirect_to login_path(return_to: request.path)
        end
      end
    end
  end

  allow_parameters :challenge,
    causeware: Parameters.string,
    features: Parameters.string,
    plan_redirect: Parameters.string,
    token: Parameters.string
  def challenge
    @token = current_account.challenge_tokens.find_by_value(params[:token])
    if @token && self.current_user = @token.source
      device_manager.params.merge!(user_agent: request.user_agent, ip: request.remote_ip)
      device_manager.track(notify: false)
      @token.destroy
    else
      return render_failure(
        status: :bad_request,
        title: '400 Bad Request',
        message: 'Invalid login challenge'
      )
    end

    plan_redirect = params[:plan_redirect]

    if params[:causeware].present?
      redirect_to "/agent/dashboard/plan_selection/small"
    elsif plan_redirect.present? && ["small", "medium", "large", "extra_large"].include?(plan_redirect)
      redirect_to "/agent/dashboard/plan_selection/#{plan_redirect}"
    else
      session[:show_direct_login_notice] = true
      url = current_account.onboarding_url

      if current_account.has_google_apps?
        gam_params = { login_hint: current_user.email }
        gam_params[:return_to] = "#{current_account.authentication_domain}#{url}" if url
        redirect_to Zendesk::Auth::Warden::GoogleAppMarketOAuth2Strategy.gam_initiation_url(gam_params)
      elsif url
        redirect_to_return_to url
      else
        redirect_after_verification
      end
    end
  end

  allow_parameters :ticket, token: Parameters.string
  def ticket
    @identity = find_email_identity(@suspended_ticket.from_mail)
    @user     = @identity ? @identity.user : nil

    if @user.nil?
      # Create unverified user
      @user = User.new(email: @suspended_ticket.from_mail, name: @suspended_ticket.from_name)
      @user.account = current_account
      @user.skip_password_validation = true
      @user.save!
    end

    @identity = @user.identities.first

    recoverer = Zendesk::Tickets::Recoverer.new(account: current_account, user: @user)
    begin
      @ticket = recoverer.recover_ticket(@suspended_ticket, @user)
      flash[:notice] = I18n.t('txt.requests.new.verified', request_title: "##{@ticket.nice_id} \"#{ERB::Util.h(@ticket.title)}\"")
    rescue Zendesk::Tickets::Recoverer::RecoveryFailed
      flash[:error] = flash_error("Failed to recover suspended ticket")
      redirect_to account_home
      return
    end

    if @user.is_verified?
      redirect_after_ticket_verification(@ticket)
    else
      @token = @identity.verification_tokens.first || @identity.verification_tokens.create(source: @identity)
      params[:token] = @token.value

      verify_user_from_ticket
      @identity.verify! unless @identity.is_verified?
    end
  end

  allow_parameters :details,
    auth_origin: Parameters.string, # From code, untested
    password: Parameters.string,
    return_to: Parameters.string,
    return_to_on_failure: Parameters.string,
    token: Parameters.string,
    user: {
      name: Parameters.string,
      password: Parameters.string,
      roles: Parameters.integer # Array? Omit?
    },
    user_security_policy_id: Parameters.bigid
  def details
    return render_failure(status: :bad_request, title: '400 Bad Request', message: 'Request must be POST') unless request.post?
    return render_failure(status: :forbidden, title: '403 Forbidden', message: 'Attempting a prohibited action') unless @identity.primary?

    if HashParam.ish?(params[:user])
      @user.name     = params[:user][:name]     if params[:user].key?(:name)
      @user.password = params[:user][:password] if params[:user].key?(:password)
    end

    Rails.logger.info("ORCA_RECAPTCHA_VERIFICATION_CONTROLLER, enabled: #{current_account.has_verification_captcha_enabled?}")

    if current_account.has_verification_captcha_enabled? && !verify_recaptcha && !current_account.whitelists?(@user.email)
      flash[:error] = flash_error(I18n.t('txt.requests.new.captcha_error_v2'), @user)
      render_details
      return
    end

    if !@user.can?(:edit, @user) && @user.changed.include?("name")
      flash[:error] = flash_error(I18n.t("txt.auth.v2.verification.illegal_user_change"))
      render_details
      return
    end

    CIA.audit(actor: @user, effective_actor: @user) do
      unless @user.save
        @user.password = nil
        @confirm_name  = !@user.name.blank?

        flash[:error] = flash_error(I18n.t('txt.users.update.failed'), @user)
        render_details
        return
      end

      @identity.verify!

      @user.update_last_login unless authenticate_after_verification?
    end

    flash[:notice] = I18n.t('txt.verification.verified')

    if authenticate_after_verification?
      redirect_to zendesk_auth.v2_login_url(auth_origin: params[:auth_origin]), notice: flash[:notice]
    else
      self.current_user = @user
      redirect_after_verification
    end
  end

  private

  def redirect_after_ticket_verification(ticket)
    if current_account.help_center_enabled?
      ticket_link = help_center_requests_path(ticket.nice_id)
      if logged_in?
        redirect_to ticket_link
      else
        flash[:notice] = I18n.t('txt.requests.new.successfully_verified')
        redirect_to login_url(theme: 'hc', return_to: ticket_link)
      end
    else
      redirect_to account_home
    end
  end

  def redirect_after_verification(redirect_as_user: current_user)
    if redirect_as_user.is_end_user? && current_account.help_center_disabled?
      # TODO: Update or remove this block as /home/index is in the process of
      # being removed, see #28231 and #38847.
      redirect_to '/home/index'
    else
      if current_account.multiproduct?
        if Arturo.feature_enabled_for?(:sell_only_after_verification, current_account)
          redirect_to root_url(after_verification: "1")
        else
          redirect_to root_url
        end
      else
        redirect_to_return_to(root_url)
      end
    end
  end

  def authenticate_after_verification?
    @user.is_agent? && current_account.settings.two_factor_enforce
  end

  def find_ticket_for_token
    @suspended_ticket = current_account.suspended_tickets.find_by_token(params[:token].to_s)
    return true if @suspended_ticket

    # If we have a verified user logged in at this point, don't bother failing, just send them along.
    # This means that Warden has already verified the user and caused a suspended ticket recovery.
    if current_user_exists? && current_user.is_verified?
      redirect_to_return_to(account_home)
    else
      flash[:notice] = I18n.t('txt.verification_controller.thanks_for_verifying_your_request')
      redirect_to '/access/help'
    end
  end

  def find_user_for_token
    @token = current_account.verification_tokens.find_by_value(params[:token])
    if @token.nil? || @token.source.nil?
      render_token_failure
    else
      if @token.source.is_a?(User)
        @user = @token.source
        @identity = @user.identities.first
      elsif @token.source.is_a?(UserIdentity)
        @identity = @token.source
        @user = @identity.user
      end
      logger.info("User #{@user.id} identified by token #{@token.value}")
    end
  end

  def authenticate_warden
    if use_remote_authentication? && @user.login_allowed?(:remote)
      warden.authenticate!
    end
  end

  def verify_user_from_ticket
    authenticate_warden
    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: {sso_action: 'email_verification'}, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'email_verification'} }
    end
  end

  def verify_user
    authenticate_warden
    render_details
  end

  def render_details
    if redirect_to_failure_return_to
    elsif request.is_mobile?
      render 'auth/sso_v2_index', locals: {sso_action: 'email_verification'}
    else
      render 'auth/sso_v2_index', locals: {sso_action: 'email_verification'}, layout: 'help_center'
    end
  end

  def save_plan_redirect
    plan_redirect = params[:causeware].present? ? 'small' : params[:plan_redirect]
    if plan_redirect.present? && ["small", "medium", "large", "extra_large"].include?(plan_redirect)
      if plan_type = SubscriptionPlanType.list.find { |sub| sub[0] == plan_redirect.camelcase }.try(:[], 1)
        current_account.trial_extras << TrialExtra.from_hash(
          'mixpanel_location' => 'buy from marketing',
          'mixpanel_subscription_plan_name' => ZBC::Zendesk::PlanType.
            plan_name(current_account.subscription.pricing_model_revision, plan_type).capitalize
        )
        current_account.save!
      end
    end
  end

  def save_trial_extras
    features = params[:features]
    return if features.blank?

    te = current_account.trial_extras.new(key: "features", value: features)
    te.save!
  end

  def verify_email_and_redirect
    CIA.audit(actor: @user, effective_actor: @user) do
      @identity.verify!
      @identity.make_primary_and_destroy_former! if params[:make_primary]
    end

    flash[:notice] = I18n.t('txt.verification.verified')

    if @user.is_account_owner? && @user.last_login.nil? && (url = current_account.onboarding_url)
      # Owner email verification after account creation
      # redirects to product specific page if available
      redirect_to_return_to url
    else
      redirect_after_verification(redirect_as_user: @user)
    end
  end

  def render_token_failure
    if current_account.help_center_enabled?
      redirect_to '/hc/signin', flash: { error: I18n.t('txt.admin.controllers.verification_controller.the_activation_link_you_followed_to_get_here_is_no_longer_valid_v2', reset_password_url: '/auth/v2/login/password_reset') }
    else
      render_failure(
        status: :bad_request, title: I18n.t('txt.admin.controllers.verification_controller.no_user_found'),
        message: I18n.t('txt.admin.controllers.verification_controller.the_activation_link_you_followed_to_get_here_is_no_longer_valid')
      )
    end
  end
end
