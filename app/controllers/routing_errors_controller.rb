class RoutingErrorsController < ActionController::Base
  include PageNotFoundRenderingSupport
  include ZendeskCrossOrigin::Api::ControllerMixin
  include ::AllowedParameters

  # protect_from_forgery removed as it's not needed when all we have here
  # is a show-action rendering the 404-page. Also in Rails4, it would cause
  # rendering of the 404 to break when a non-existing .js-asset was requested.

  allow_cors_from_zendesk_external_domains :show

  allow_parameters :show, :skip
  def show
    render_404_page
  end
end
