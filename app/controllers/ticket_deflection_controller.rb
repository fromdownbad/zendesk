class TicketDeflectionController < ApplicationController
  include ::AllowedParameters
  include ZendeskCrossOrigin::Helper
  include AutomaticAnswers::AuthenticationHelper

  skip_before_action :authenticate_user

  before_action { head :forbidden unless valid_auth_token?(params) }
  before_action :deflection_available?
  before_action :load_ticket

  EMBED_ARTICLE_TICKET_SOLVED = 1
  EMBED_ARTICLE_TICKET_NOT_SOLVED = 0

  shared_parameters = {
    article_id: Parameters.bigid,
    auth_token: Parameters.string
  }

  allow_parameters :article, **shared_parameters
  def article
    return head :not_found unless decoded_params["articles"].include?(Integer(params[:article_id]))

    record_stats('article_viewed', TicketDeflection::EMAIL_RESOLUTION_CHANNEL)
    redirect_to_article_url(solved: EMBED_ARTICLE_TICKET_NOT_SOLVED)
  end

  allow_parameters :solve_ticket_and_redirect, **shared_parameters
  def solve_ticket_and_redirect
    return head :ok if request.head?

    ticket_deflector.solve(
      article_id: params[:article_id]
    )

    redirect_to_article_url(solved: EMBED_ARTICLE_TICKET_SOLVED)
  end

  allow_parameters :article_feedback_redirect, **shared_parameters
  def article_feedback_redirect
    ticket_deflector.process_deflection_rejection(
      article_id: params[:article_id]
    )

    record_stats('article_feedback', TicketDeflection::EMAIL_RESOLUTION_CHANNEL)
    redirect_to_article_url(article_feedback: 1)
  end

  private

  def load_ticket
    @ticket = current_account.tickets.find_by_nice_id!(decoded_params['ticket_id'])
  end

  def decoded_params
    jwt_token_params(params)
  end

  def ticket_deflector
    @ticket_deflector ||= TicketDeflector.new(
      account: current_account,
      deflection: @ticket.ticket_deflection,
      resolution_channel_id: ViaType.MAIL
    )
  end

  def redirect_to_article_url(params)
    article = fetch_hc_article
    query_params = { auth_token: auth_token }.merge(params)
    redirect_to url_with_query_params(article[:html_url], query_params)
  rescue
    # if article is not found in help center.
    render_404_page
  end

  def auth_token
    AutomaticAnswersJwtToken.encrypted_token(
      @ticket.account.id,
      @ticket.requester_id,
      @ticket.nice_id,
      @ticket.ticket_deflection.id,
      @ticket.ticket_deflection.ticket_deflection_articles.map(&:article_id).map(&:to_i)
    )
  end

  def url_with_query_params(url, query_params)
    url = URI.parse(url)

    query_arr = if url.query.blank?
      query_params.to_a
    else
      URI.decode_www_form(url.query) + query_params.to_a
    end
    url.query = URI.encode_www_form(query_arr)

    url.to_s
  end

  def deflection_available?
    render_404_page unless current_account.has_automatic_answers_enabled?
  end

  def fetch_hc_article
    client = Zendesk::HelpCenter::InternalApiClient.new(account: current_account, requester: @ticket.requester, brand: @ticket.ticket_deflection.brand)
    article_id = params[:article_id]
    article_locale = TicketDeflectionArticle.locale(@ticket.ticket_deflection, article_id)
    client.fetch_article(article_id, article_locale)
  end

  def record_stats(key, channel)
    tags = ["subdomain:#{current_account.subdomain}", "channel:#{channel}"]

    statsd_client.increment(key, tags: tags)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket_deflection'])
  end
end
