class AssetsController < ApplicationController
  include ::AllowedParameters
  allow_zendesk_mobile_app_access

  include Zendesk::Assets::S3
  include Zendesk::Auth::ControllerSessionSupport

  skip_before_action :authenticate_user
  skip_before_action :ensure_support_is_active!
  skip_before_action :verify_authenticated_session # skip all the session handling too

  prepend_before_action :encode_params_to_utf8
  before_action :normalize_uri
  after_action :strip_response_cookies
  allow_anonymous_users
  ssl_allowed :all

  attr_reader :path
  private :path

  allow_parameters :show,
    id: Parameters.string,
    default: Parameters.string,
    timestamp: Parameters.string
  def show
    unless path.uri && set_headers
      return render_failure(
        status: :not_found,
        title: I18n.t("txt.errors.not_found.title"),
        message: I18n.t("txt.errors.not_found.message")
      )
    end
    head :ok
  end

  private

  OBJECT_CLASSES = {
    'logos' => Logo,
    'photos' => Photo,
    'voice/uploads' => Voice::Upload,
    'o_auth/logos' => Zendesk::OAuth::Logo,
    'brands' => BrandLogo,
  }.freeze

  def strip_response_cookies
    if path&.object_type == 'voice/uploads'
      disable_session_set_cookie if current_account.has_assets_strip_response_cookies_voice_uploads?
    else
      disable_session_set_cookie if current_account.has_assets_strip_response_cookies_photos_and_logos?
    end
  end

  def set_headers
    klass = OBJECT_CLASSES.fetch(path.object_type)
    @asset = klass.where(id: path.asset_id, account_id: current_account.id).first

    return false if @asset.nil?
    set_x_accel_redirect
    set_s3_headers
    true
  end

  def set_x_accel_redirect
    redirect = "/s3/#{path.uri}"
    Rails.logger.debug "Setting X-Accel-Redirect to: #{redirect}"

    response.sending_file = true
    response.headers['X-Accel-Redirect'] = redirect
  end

  # Non-development environments is getting ASCII-8BIT encoding instead of
  # UTF-8, so this ensures that we're using the right encoding.
  def encode_params_to_utf8
    params[:id].to_utf8! if params[:id].is_a?(String)
  end

  def normalize_uri
    @path = Zendesk::Assets::Path.new(params[:id])
  end
end
