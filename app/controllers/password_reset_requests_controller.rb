class PasswordResetRequestsController < ApplicationController
  include ::AllowedParameters
  include HelpCenterRedirection
  include CrossDomainXFrameBypass
  include Zendesk::Auth::ControllerSupport

  skip_before_action :authenticate_user

  before_action :prepare_email,                 only: :create
  before_action :validate_rate_limit,           only: :create
  skip_before_action :ensure_proper_protocol,   only: :index
  skip_before_action :ensure_support_is_active!
  before_action :redirect_to_index_with_error,  only: :create
  before_action :redirect_to_mapped_host,       only: :index, if: :host_is_incorrect?

  helper_method :current_brand, :current_route

  ssl_allowed :create

  user_type = Parameters.map(email: Parameters.string)

  allow_parameters :index,
    user: user_type,
    flash_digest: Parameters.string

  def index
    @user = User.new(params[:user].try(:slice, :email))
    respond_to do |format|
      format.html { render 'auth/sso_v2_index', locals: { sso_action: 'password_reset' }, layout: 'help_center' }
      format.mobile_v2 { render 'auth/sso_v2_index', locals: {sso_action: 'password_reset' } }
    end
  end

  allow_parameters :create,
    auth_origin: Parameters.string,
    brand_id: Parameters.bigid,
    email: Parameters.string,
    flash_digest: Parameters.string,
    role: Parameters.string,
    theme: Parameters.string,
    return_to: Parameters.string,
    return_to_on_failure: Parameters.string,
    user: user_type

  def create
    send_password_reset_email_to_user!
    redirect_to zendesk_auth.password_reset_end_page_v2_login_url(render_params)
  end

  private

  def redirect_to_index_with_error
    error = if !identity_present?
      true
    elsif user.deleted?
      log_preventing_password_reset("user is deleted")
      true
    elsif !user_can_reset_password?
      log_preventing_password_reset("user is not capable of resetting password")
      true
    end

    redirect_to zendesk_auth.password_reset_end_page_v2_login_url(render_params) if error
  end

  def send_password_reset_email_to_user!
    user.active_brand_id = params[:brand_id]

    if identity.is_verified?
      token = if current_account.has_generate_password_reset_token_subclass?
        user.password_reset_tokens.create
      else
        user.verification_tokens.create
      end

      UsersMailer.deliver_password_reset(user, token.value, user.active_brand_id)
    else
      identity.send_verification_email
    end
  end

  def render_params
    { brand_id: params[:brand_id], auth_origin: params[:auth_origin], theme: params[:theme], role: params[:role] }
  end

  def identity
    @identity ||= current_account.user_identities.email.find_by_value(params[:email])
  end

  def user
    @user ||= identity.user
  end

  def prepare_email
    params[:email] ||= params[:user] && params[:user][:email]

    if params[:email].blank?
      flash.now[:error] = I18n.t('txt.access.help.blank_email')

      redirect_to_failure_return_to
    end
  end

  def identity_present?
    identity.present? && !identity.user.suspended?
  end

  def validate_rate_limit
    Prop.throttle!(:password_reset_requests, [current_account.id, params[:email]])
    Prop.throttle!(:password_reset_requests, [current_account.id, request.remote_ip])
  end

  def user_can_reset_password?
    if user.is_end_user?
      current_account.role_settings.end_user_password_allowed? && current_account.is_end_user_password_change_visible?
    else
      current_account.role_settings.agent_password_allowed? &&
        (!current_account.has_use_user_policy_for_password_resets? || user.can?(:request_password_reset, user))
    end
  end

  def log_preventing_password_reset(reason)
    if identity_present?
      Rails.logger.info(
        "Preventing password reset email for " \
        "account_id: #{user.account_id}, " \
        "user_id: #{user.id}, " \
        "email: #{user.email.inspect}, " \
        "reason: #{reason}."
      )
    end
  end
end
