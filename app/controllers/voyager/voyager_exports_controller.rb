require 'time_difference'

module Voyager
  class VoyagerExportsController < ApplicationController
    include ::AllowedParameters

    ssl_required :create, :download
    before_action :authorize_export
    before_action :validate_limits, only: [:create]
    EXPORT_SIZE_LIMITS = {
      'json' => 1_000_000
    }.freeze
    EXPORT_RANGE_LIMIT = 31 # days

    allow_parameters :create,
      end:           Parameters.integer | Parameters.float,
      export_format: Parameters.string,
      export_type:   Parameters.string,
      metrics:       Parameters.anything, # no clue what this is
      start:         Parameters.integer

    def create
      status = params[:export_format] == 'json' ? enqueue_json_export : enqueue_csv_export
      Rails.logger.info("status is #{status}")
      respond_to do |format|
        format.html do
          if status == 201
            statsd_client.increment('request.create', tags: ['response:201'])
            flash[:notice] = I18n.t('txt.admin.controllers.jobs_controller.job_submitted_email_message')
          elsif status == 202
            statsd_client.increment('request.create', tags: ['response:202'])
            flash[:error] = I18n.t('txt.admin.controllers.csv_exports_controller.job_already_submitted')
          else
            statsd_client.increment('request.create', tags: ['response:error'])
            flash[:error] = I18n.t('txt.admin.controllers.csv_exports_controller.generic_error')
          end

          redirect_to reports_path + "#export"
        end

        format.any(:json, :xml, :js) { head status }
      end
    end

    allow_parameters :download,
      id: Parameters.string # I think it's a URL... see #authorized_url_response

    def download
      return send_file(URI(authorized_url_response.url).path) if authorized_url_response && Rails.env.development?
      if is_assuming_user_from_monitor?
        render plain: "Assuming monitor users cannot download export files", status: :forbidden
      elsif authorized_url_response
        statsd_client.increment('request.download', tags: ['response:200'])
        set_s3_headers
        head :ok
      else
        statsd_client.increment('request.download', tags: ['response:404'])
        render_failure(status: :not_found, title: I18n.t('txt.errors.not_found.title'), message: I18n.t('txt.errors.not_found.message'))
      end
    end

    private

    def validate_limits
      statsd_client.increment('validate_limits')
      return true unless export_limits_enabled?
      limit = EXPORT_SIZE_LIMITS[params[:export_format]]
      statsd_client.increment('validate_limits.enabled')
      if params[:export_type] == 'tickets' && limit.present?
        Rails.logger.info "Checking if we should enforce export size limits of #{limit} tickets"
        # get the total ticket count
        total_tickets = Rails.cache.fetch(ticket_count_cache, expires_in: 1.day) do
          current_account.tickets.count(:all) + current_account.ticket_archive_stubs.count(:all)
        end
        # get the day range for the export
        export_days = TimeDifference.between(Time.at(start_time.to_i), Time.at((end_time || Time.now).to_i)).in_days
        # If the account has more tickets than the max limit, we ask them to export only EXPORT_RANGE_LIMIT days at a time
        Rails.logger.info "Total tickets: #{total_tickets}. Requested #{limit} tickets on an export for #{export_days} days"
        statsd_client.increment('validate_limits.check')
        if (total_tickets > limit) && (export_days > EXPORT_RANGE_LIMIT)
          statsd_client.increment('validate_limits.enforced')
          Rails.logger.info "Enforcing export size limit of #{limit} #{params[:export_type]} on an export for #{export_days} days"
          flash[:error] = I18n.t('txt.admin.controllers.csv_exports_controller.export_limit_reached', max_days: EXPORT_RANGE_LIMIT)
          redirect_to reports_path + "#export"
        end
      end
    end

    def authorize_export
      grant_access export_policy.accessible_to_user?
    end

    def export_policy
      ImportExportJobPolicy.new(current_account, current_user)
    end

    def voyager_client
      @voyager_client ||= Zendesk::VoyagerClientBuilder.build(current_account)
    end

    def set_s3_headers
      response.headers['Content-Type'] = authorized_url_response.content_type
      response.headers['Content-Disposition'] = "attachment; filename=\"#{authorized_url_response.filename}\""
      response.headers['X-Accel-Redirect'] = "/s3/#{authorized_url_response.filename}"
      response.headers['X-S3-Storage-Url'] = authorized_url_response.url
      response.headers['X-Accel-Buffering'] = "yes"
    end

    def authorized_url_response
      @authorized_url ||= begin
        response = voyager_client.authorized_url(params[:id])
        response.successful? ? response : false
      end
    end

    def start_time
      (params[:start] && Time.at(params[:start].to_i)) || default_start_time
    end

    def end_time
      (params[:end] && Time.at(params[:end].to_i))
    end

    def enqueue_csv_export
      options = {
          export_type: :tickets,
          format_type: :csv,
          start_time: start_time.to_i,
          user_id: current_user.id,
          legacy_metrics: params[:metrics],
          raw_export: true
      }
      enqueue_export(options)
    end

    def enqueue_json_export
      options = {
          export_type: params[:export_type],
          format_type: :json,
          start_time: start_time.to_i,
          user_id: current_user.id,
          raw_export: false
      }
      enqueue_export(options)
    end

    def enqueue_export(options)
      options[:end_time] = end_time.to_i if end_time.present?
      Rails.logger.info("Creating voyager export with #{options.inspect}")
      voyager_client.create_export(options).status
    rescue => error
      Rails.logger.error("Could not create export #{error.message} " + error.backtrace.join("\n"))
      ZendeskExceptions::Logger.record(error, location: self, message: error.message, fingerprint: 'dead31e9f91f075e50a810e361eeb5ef540431d9')
      500
    end

    def default_start_time
      current_account.created_at
    end

    def export_limits_enabled?
      ::Arturo.feature_enabled_for?(:voyager_export_limits, current_account)
    end

    def ticket_count_cache
      "exports-ticket-count-#{current_account.id}"
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['classic.voyager_exports'])
    end
  end
end
