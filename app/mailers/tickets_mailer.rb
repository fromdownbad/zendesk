class TicketsMailer < ApplicationMailer
  #
  # These methods are only used by Twitter, the customer
  # For all other accounts, when an email is sent to a closed ticket, we create a new ticket and link it to the old ticket instead
  #

  def closed_bounce(account, mail_from, mail_subject, ticket_id)
    set_headers(account, X_DELIVERY_CONTEXT => "closed-bounce-#{ticket_id}")
    email_locale = recipient_locale(mail_from, account)
    title = I18n.t("txt.email.closed_ticket.title", mail_subject: mail_subject.to_s, locale: email_locale)
    bounce(account, "closed_ticket", mail_from, title, email_locale)
  end

  def support_bounce(account, mail_from)
    set_headers(account, X_DELIVERY_CONTEXT => "support-bounce-#{account.id}")
    email_locale = recipient_locale(mail_from, account)
    title = I18n.t("txt.email.no_support.title", locale: email_locale)
    bounce(account, "no_support", mail_from, title, email_locale)
  end

  private

  def bounce(account, key, mail_from, title, email_locale)
    content = I18n.t("txt.email.#{key}.message", locale: email_locale)
    mail(
      subject: title,
      to: mail_from,
      from: from_address(account)
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: title,
          content: content,
          footer: footer(account, true, nil, email_locale),
          account: account
        )
      end
      format.html do
        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: title,
          content: content,
          footer: footer(account, false, nil, email_locale),
          account: account
        )
      end
    end
  end
end
