class TrialLimitMailer < ApplicationMailer
  def trial_limit_exceeded_notice(user, key, _limit)
    account = user.account

    I18n.with_locale(recipient_locale(user)) do
      content = I18n.t(
        "txt.email.trial_limit_mailer.#{key}_limit_exceeded_notice_v2.body",
        first_name: user.first_name,
        subdomain: account.subdomain
      )

      subject = I18n.t("txt.email.trial_limit_mailer.#{key}_limit_exceeded_notice_v2.subject")

      set_headers(
        user.account,
        X_DELIVERY_CONTEXT => 'trial-limit-exceeded',
        X_ZENDESK_PRIORITY_MAIL => 'Trial Limit Exceeded Notice'
      )

      mail(
        from: Zendesk::Mailer::ZendeskAddress.new('Zendesk', 'suspensions@support.zendesk.com').from,
        reply_to: 'suspensions@zendesk.com',
        to: user.email,
        subject: subject
      ) { |format| render_email_parts account, subject, content, format }
    end
  end
end
