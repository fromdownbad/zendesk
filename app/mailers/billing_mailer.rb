class BillingMailer < ApplicationMailer
  def voice_account_suspension_notification(account)
    ActiveRecord::Base.on_shard(account.shard_id) do
      voice_account = account.voice_sub_account
      set_headers(
        account,
        X_DELIVERY_CONTEXT => "voice-account-suspension-#{account.id}-#{voice_account.id}",
        X_ZENDESK_PRIORITY_MAIL => "Voice Account Suspension Notification"
      )

      email_locale  = recipient_locale(account.owner, account)
      payments_url  = "#{account.url(mapped: false)}/account/payments/"
      content       = I18n.t("txt.email.voice_account_suspension_notification.message", locale: email_locale, payments_url: payments_url, account_name: account.name, account_owner: account.owner.name)
      title         = I18n.t("txt.email.voice_account_suspension_notification.title",   locale: email_locale)

      mail(
        subject: title,
        to: account.owner.email,
        from: Zendesk::Mailer::ZendeskAddress.new.from
      ) do |format|
        format.text { content.strip_tags }
        format.html { zendesk_templated_body(title, content, email_locale) }
      end
    end
  end
end
