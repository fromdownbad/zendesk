require 'forums_mailer_presenters/base'

module ForumsMailerPresenters
  class PostPresenter < Base
    attr_reader :post

    def initialize(post, watching, options = {})
      super(watching, options)
      @post = post
    end

    def unsubscribe_only_comments_from_watching_url
      "#{account.url}/watchings/unsubscribe_comments/#{watching.token}"
    end

    protected

    def html
      <<~HTML
        <p>
          #{I18n.t('txt.watchings.mail.new_comment_with_parameters', post_entry: entry_link(post.entry), locale: @email_locale)}
        </p>
        <ul>
          <li>
            <b>#{post.user.safe_name(recipient.is_agent?)}</b><br/>
            <p>#{body}</p>
          </li>
        </ul>
        <p>#{notification_explanation}</p>
        <p>#{unsubscription_partial}</p>
      HTML
    end

    def entry_link(entry)
      "<a href=\"#{entry.account.url}/entries/#{entry.id}\">#{entry.title}</a>"
    end

    def notification_explanation
      I18n.t('txt.watchings.mail.new_comment_explanation', forum_or_topic_name: @watching.source.name, locale: @email_locale)
    end

    def unsubscription_partial
      return separate_unsubscriptions if watching.source.is_a? Forum
      unsubscribe_whole_watching_fragment
    end

    def separate_unsubscriptions
      link_to(unsubscribe_only_comments_from_watching_url, I18n.t('txt.watchings.mail.unsubscribe_only_comments', locale: @email_locale)) +
      " " +
      I18n.t('txt.watchings.mail.unsubscribe_or', locale: @email_locale) +
      " " +
      link_to(unsubscribe_whole_watching_url, I18n.t('txt.watchings.mail.unsubscribe_completely', locale: @email_locale))
    end

    def presented_obj
      post
    end
  end
end
