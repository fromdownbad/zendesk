module ForumsMailerPresenters
  class Base
    attr_reader :recipient, :watching, :account

    def initialize(watching, options = {})
      @watching  = watching
      @recipient = watching.user
      @account   = recipient.account
      @email_locale = (@recipient.locale_id.present? ? @recipient.translation_locale : @account.translation_locale)

      @logger = options[:logger] || Rails.logger
    end

    def message
      ensure_valid_lenght_of(html)
    end

    def body
      presented_obj.body.gsub('/attachments/', "#{account.url}/attachments/").gsub("#{account.url}#{account.url}", account.url.to_s)
    end

    def plain_text_variant
      # TODO: we might want to add some better regex Kung-Fu to make it looks better
      # This removes all links, convertrs some tags to new lines and remove all tags
      message.strip_tags
    end

    def unsubscribe_whole_watching_fragment
      link_to(unsubscribe_whole_watching_url, I18n.t('txt.watchings.mail.unsubscribe', locale: @email_locale))
    end

    def ensure_valid_lenght_of(content)
      if content.size > 8_000
        @logger.info("Truncating watching mail of length #{content.size}")
        return content.truncate(8_000)
      end
      content
    end

    def unsubscribe_whole_watching_url
      "#{account.url}/watchings/unsubscribe/#{watching.token}"
    end

    def link_to(url, name)
      "<a href=\"#{url}\">#{name}</a>"
    end
  end
end
