require 'forums_mailer_presenters/base'

module ForumsMailerPresenters
  class EntryPresenter < Base
    attr_reader :entry

    def initialize(entry, watching, options = {})
      super(watching, options)
      @entry = entry
    end

    protected

    def html
      <<~HTML
        <p>
          #{I18n.t('txt.watchings.mail.new_topic_with_category', topic_name_with_link: entry_link, forum_name: forum.name, locale: @email_locale)}
        </p>
        <ul>
          <li>
            <b>#{entry.submitter.safe_name(recipient.is_agent?)}</b><br/>
            <p>#{body}</p>
          </li>
        </ul>
        <p>#{notification_explanation}</p>
        <p>#{unsubscribe_whole_watching_fragment}</p>
      HTML
    end

    def entry_link
      %(<a href="#{entry.url}">#{entry.title}</a>)
    end

    def forum
      entry.forum
    end

    def notification_explanation
      I18n.t('txt.watchings.mail.new_topic_explanation', forum_name: entry.forum.name, locale: @email_locale)
    end

    def presented_obj
      entry
    end
  end
end
