class CloudmarkFeedbackMailer < ApplicationMailer
  def header_feedback(params)
    params  = params.with_indifferent_access
    account = Account.find(params[:account_id])
    set_headers(account, X_DELIVERY_CONTEXT => delivery_context(params))

    classification = params[:classification]
    content = build_analysis_email(params[:analysis_header])

    mail(
      from: "#{user_name(params)}@system.zendesk.com",
      to: "zendesk-#{classification}-headers@feedback.cloudmark.com",
      subject: "#{classification} feedback",
      'Return-Path' => 'noreply@system.zendesk.com'
    ) do |f|
      f.text { render plain: content }
      f.html { render plain: "REMOVE_ME" }
    end
    @_message.parts.pop
    @_message.parts.first.content_type = 'message/rfc822'
  end

  protected

  def delivery_context(params)
    [params[:account_id], params[:user_id], params[:identifier], "cloudmark_#{params[:classification]}"].join('/')
  end

  def user_name(params)
    "cloudmark.#{params[:account_id]}.#{params[:user_id]}"
  end

  def build_analysis_email(analysis_header)
    "X-CNFS-Analysis: #{analysis_header}\r\n\r\n"
  end
end
