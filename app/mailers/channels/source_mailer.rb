module Channels
  class SourceMailer < ApplicationMailer
    include Rails.application.routes.url_helpers
    include Zendesk::Mailer::ReadOnly

    def facebook_page_unauthorized_notification(account, page_id, facebook_page_name)
      link = '/agent/admin/facebook'

      locale = recipient_locale(recipients(account), account)

      content = I18n.t(
        'txt.email.facebook_page_unauthorized.html_body',
        name: facebook_page_name,
        page_link: "#{account.url(mapped: false)}#{link}",
        learn_more_link: I18n.t('txt.email.facebook_page_unauthorized.learn_more_link', locale: locale),
        locale: locale
      )
      set_headers(account, X_DELIVERY_CONTEXT => "facebook-page-unauthorized-notification-#{page_id}")
      subject = I18n.t('txt.email.facebook_page_unauthorized.subject', name: facebook_page_name, locale: locale)
      render_email(account, subject, content, locale)
    end

    def twitter_handle_unauthorized_notification(account, handle_id, twitter_screen_name)
      link = '/agent/admin/twitter'

      locale = recipient_locale(recipients(account), account)
      content = I18n.t(
        'txt.email.twitter_handle_unauthorized.html_body',
        name: twitter_screen_name,
        page_link: "#{account.url(mapped: false)}#{link}",
        learn_more_link: I18n.t('txt.email.twitter_handle_unauthorized.learn_more_link', locale: locale),
        locale: locale
      )

      set_headers(account, X_DELIVERY_CONTEXT => "twitter-handle-unauthorized-notification-#{handle_id}")
      subject = I18n.t('txt.email.twitter_handle_unauthorized.subject', name: twitter_screen_name, locale: locale)
      render_email(account, subject, content, locale)
    end

    def twitter_handle_deactivated_by_twitter_notification(account, handle_id, twitter_screen_name)
      link = '/agent/admin/twitter'

      locale = recipient_locale(recipients(account), account)
      content = I18n.t(
        'txt.email.twitter_handle_deactivated_by_twitter.html_body',
        name: twitter_screen_name,
        page_link: "#{account.url(mapped: false)}#{link}",
        locale: locale
      )

      set_headers(account, X_DELIVERY_CONTEXT => "twitter-handle-deactivated_by_twitter-notification-#{handle_id}")
      subject = I18n.t('txt.email.twitter_handle_deactivated_by_twitter.subject', name: twitter_screen_name, locale: locale)
      render_email(account, subject, content, locale)
    end

    def any_channel_instance_deactivate_notification(
      account,
      integration_service_instance_id,
      integration_service_instance_name,
      registered_integration_service_id,
      registered_integration_service_name
    )
      locale = recipient_locale(recipients(account), account)

      page_link = "#{account.url(mapped: false)}/agent/admin/registered_integration_services/#{registered_integration_service_id}?tab=accounts&filter=inactive"

      content = I18n.t(
        'txt.email.any_channel_instance_deactivate_notification.html_body',
        registered_integration_service_name: registered_integration_service_name,
        integration_service_instance_name: integration_service_instance_name,
        page_link: page_link,
        locale: locale
      )

      set_headers(
        account,
        X_DELIVERY_CONTEXT => "any-channel-integration-deactivated-notification-#{integration_service_instance_id}"
      )

      subject = I18n.t(
        'txt.email.any_channel_instance_deactivate_notification.subject',
        registered_integration_service_name: registered_integration_service_name,
        integration_service_instance_name: integration_service_instance_name,
        locale: locale
      )

      render_email(account, subject, content, locale)
    end

    def any_channel_instance_needs_reinitialization_notification(
      account,
      integration_service_instance_id,
      integration_service_instance_name,
      registered_integration_service_id,
      registered_integration_service_name
    )
      locale = recipient_locale(recipients(account), account)

      page_link = "#{account.url(mapped: false)}/agent/admin/registered_integration_services/#{registered_integration_service_id}?tab=accounts"

      content = I18n.t(
        'txt.email.any_channel_instance_needs_reinitialization_notification.html_body',
        registered_integration_service_name: registered_integration_service_name,
        integration_service_instance_name: integration_service_instance_name,
        page_link: page_link,
        locale: locale
      )

      set_headers(
        account,
        X_DELIVERY_CONTEXT => "any-channel-integration-needs-reinitialization-notification-#{integration_service_instance_id}"
      )

      subject = I18n.t(
        'txt.email.any_channel_instance_needs_reinitialization_notification.subject',
        registered_integration_service_name: registered_integration_service_name,
        integration_service_instance_name: integration_service_instance_name,
        locale: locale
      )

      render_email(account, subject, content, locale)
    end

    private

    def recipients(account)
      account.admins.includes(:identities).map(&:email)
    end

    def render_email(account, subject, content, locale)
      from = "Zendesk Support <support@support.#{Zendesk::Configuration.fetch(:host)}>"
      content_type = 'multipart/alternative'

      render_email_parts(account, subject, content, from, content_type, locale)
    end

    def render_email_parts(account, subject, content, from, _content_type, locale)
      mail_info = {
        subject: subject,
        from: from,
        to: recipients(account)
      }

      template_params = {
        'header' => subject,
        'footer_link' => footer_link(account, locale),
        'delimiter' => '',
        'attributes' => '',
        'styles' => ''
      }

      mail(mail_info) do |format|
        format.text do
          Liquid::Template.parse(account.text_mail_template).render(
            template_params.merge(
              'content' => content,
              'footer' => footer(account, locale, true)
            )
          )
        end
        format.html do
          Liquid::Template.parse(account.html_mail_template).render(
            template_params.merge(
              'content' => add_direction(account, locale, :content, content.format_and_link),
              'footer' => add_direction(account, locale, :footer, footer(account, locale))
            )
          )
        end
      end
    end

    def footer(account, locale, with_delimiter = false)
      text = I18n.t('txt.email.footer', account_name: account.name, locale: locale)
      with_delimiter ? "--------------------------------\n#{text}" : text
    end

    def footer_link(account, locale)
      params = { utm_medium: 'poweredbyzendesk', utm_source: 'email-notification', utm_campaign: 'text' }
      params[:company_name] = account.name if account
      email_url = add_tracking_params(I18n.t('txt.email.email_landing_page_url', locale: locale), params)
      zendesk_url = '<a href="%s" style="color:black" target="_blank">Zendesk</a>' % [email_url]
      I18n.t('txt.email.branded_delivered_by_url', url: zendesk_url, locale: locale)
    end

    # Adds GA tracking parameters and company name parameter to links
    # Ex: "?utm_medium=poweredbyzendesk&utm_source=email-notification&utm_campaign=text&company=Acme"
    def add_tracking_params(url, params)
      url + (url.include?("?") ? "&" : "?") + params.to_query
    end
  end
end
