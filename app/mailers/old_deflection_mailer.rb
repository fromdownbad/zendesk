class OldDeflectionMailer < ApplicationMailer
  include Zendesk::Mailer::ReadOnly

  def answer_bot_email(ticket, rule_id, ccs = [])
    trigger = AnswerBot::Trigger.config(ticket.account, rule_id)
    initialize_state(ticket, trigger[:subject_template], trigger[:body_template], ccs)
    set_headers(account, base_mail_headers(ticket))

    @comment = ticket.comments.first
    @outbound_attachments = attach_outbound_attachments

    deferred = mail(deflection_headers) do |format|
      format.text { text_body }
      format.html { html_body }
    end

    if ticket.requester.machine?
      message.perform_deliveries = false
    else
      log_deflection_sent
    end

    deferred
  end

  protected

  attr_reader :account, :ticket, :for_agent, :to, :ccs, :locale, :user, :recipients, :subject_template, :body_template, :outbound_attachments

  private

  def initialize_state(ticket, subject_template, body_template, ccs = [])
    @account = ticket.account
    @ticket  = ticket
    @user = ticket.requester
    @ccs = ccs
    @for_agent = ticket.requester.is_agent?
    @subject_template = subject_template
    @body_template = body_template
    @locale = recipient_locale(@user, @account)
    @outbound_attachments = []
  end

  def ticket_address
    @ticket_address ||= Zendesk::Mailer::TicketAddress.new(account: account, ticket: ticket)
  end

  def template_renderer(template, format: Mime[:html].to_s)
    Zendesk::Liquid::TicketContext.render(template, ticket, ticket.requester, for_agent, locale, format, include_tz: true, skip_attachments: outbound_attachments)
  end

  def html_body
    body = Zendesk::Liquid::MailContext.render(
      template: account.html_mail_template, header: deflection_headers, content: template_renderer(body_template),
      footer: footer(account, false, nil, nil, @ticket.brand), account: account, delimiter: delimiter,
      translation_locale: locale
    )

    body = convert_html_inline_attachments(body) if account.send_real_attachments?
    body
  end

  def text_body
    Zendesk::Liquid::MailContext.render(
      template: account.text_mail_template, header: deflection_headers, content: template_renderer(body_template, format: Mime[:text].to_s),
      footer: footer(account, false, nil, nil, @ticket.brand), account: account, delimiter: delimiter,
      translation_locale: locale
    )
  end

  def deflection_headers
    {
      from: ticket_address.from(for_agent: false),
      reply_to: ticket_reply_to_address,
      to: user.email_address_with_name,
      cc: recipient_ccs,
      subject: template_renderer(subject_template, format: Mime[:text].to_s)
    }
  end

  def base_mail_headers(ticket)
    {
      "Message-Id" => ticket.generate_message_id(true),
      "In-Reply-To" => ticket.generate_message_id(false),
      X_DELIVERY_CONTEXT => "automatic-answer-#{ticket.id}"
    }
  end

  def delimiter
    if account.mail_delimiter.include?("{{txt.email.delimiter}}")
      "##- #{I18n.t("txt.email.delimiter", locale: locale)} -##"
    else
      account.mail_delimiter
    end
  end

  def ticket_reply_to_address
    ticket_address.reply_to(user.is_agent?)
  end

  def log_deflection_sent
    statsd_client.increment("email_sent", tags: ["subdomain:#{account.subdomain}"])
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket_deflection'])
  end

  def recipient_ccs
    return [] unless read_only { account.has_comment_email_ccs_allowed_enabled? }

    ccs.map(&method(:user_to_email))
  end

  def user_to_email(user)
    user.is_a?(User) ? user.email_address_with_name : user.to_s
  end
end
