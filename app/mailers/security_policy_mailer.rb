class SecurityPolicyMailer < ApplicationMailer
  include Zendesk::Mailer::ReadOnly

  def expiring_password_warning(user)
    account = user.account
    set_headers(account, X_DELIVERY_CONTEXT => "security-policy-#{user.security_policy.id}-#{user.id}")

    I18n.with_locale(recipient_locale(user)) do
      content = generate_expiring_password_warning_content(user)

      title = I18n.t('txt.email.password_expiring.subject', account_name: account.name)
      mail(
        subject: title,
        to: user.email,
        from: from_address(account)
      ) do |format|
        format.text do
          Zendesk::Liquid::MailContext.render(
            template: account.text_mail_template,
            header: title,
            content: content,
            footer: footer(account, true, user),
            account: account
          )
        end
        format.html do
          Zendesk::Liquid::MailContext.render(
            template: account.html_mail_template,
            header: title,
            content: content.format_and_link,
            footer: footer(account, false, user),
            account: account
          )
        end
      end
    end
  end

  def high_security_policy_warning(account)
    set_headers(account, X_DELIVERY_CONTEXT => "high-security-policy-warning-#{account.id}")

    owner = account.owner

    I18n.with_locale(recipient_locale(owner)) do
      content = <<~CONTENT
        #{I18n.t('txt.email.security_policy_email.high_security_email.body_1', owner_name: owner.name)}

        #{I18n.t('txt.email.security_policy_email.high_security_email.body_2')}

        #{I18n.t('txt.email.security_policy_email.high_security_email.body_3', account_url: account.url)}

        #{I18n.t('txt.email.security_policy_email.high_security_email.body_4')}

        #{I18n.t('txt.email.security_policy_email.high_security_email.body_5')}
      CONTENT

      mail(
        subject: I18n.t('txt.email.security_policy_email.high_security_email.subject'),
        to: owner.email,
        from: Zendesk::Mailer::ZendeskAddress.new.from
      ) do |format|
        format.text { content.strip_tags }
        format.html { content.format_and_link }
      end
    end
  end

  private

  def generate_expiring_password_warning_content(user)
    account = user.account
    security_settings_url = "#{account.url(ssl: true)}/agent/users/#{user.id}/security_settings"

    mail_version = read_only { account.has_new_password_expired_email? } ? '_v2' : ''

    expires_at_in_words = if user.password_expires_at_in_days <= 1
      I18n.t('txt.email.security_policy.expiring_password_warning.set_to_expire_today' + mail_version, account_url: account.url)
    else
      I18n.t('txt.email.security_policy.expiring_password_warning.set_to_expire_in_days' + mail_version, count: user.password_expires_at_in_days, account_url: account.url)
    end

    if read_only { account.has_new_password_expired_email? }
      <<~CONTENT
              #{I18n.t('txt.email.security_policy.expiring_password_warning.hello_user_name', user_name: user.name)}

        #{expires_at_in_words} #{I18n.t("txt.email.security_policy.expiring_password_warning.please_sign_in")}

        #{I18n.t("txt.email.security_policy.expiring_password_warning.instructions")}

        #{security_settings_url}

        #{I18n.t('txt.email.security_policy.expiring_password_warning.thank_you')}
              #{account.name}
      CONTENT
    else
      <<~CONTENT
              #{I18n.t('txt.email.security_policy.expiring_password_warning.hello_user_name', user_name: user.name)}

        #{expires_at_in_words}

        #{I18n.t('txt.email.security_policy.expiring_password_warning.thank_you')}
              #{account.name}
      CONTENT
    end
  end
end
