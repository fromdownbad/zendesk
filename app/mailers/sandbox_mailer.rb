class SandboxMailer < ApplicationMailer
  def success_notification(account, user, sandbox_subdomain)
    locale = recipient_locale(user)
    set_headers(account, X_DELIVERY_CONTEXT => "success-notification-#{account.id}")
    subject = I18n.t("txt.email.sandbox_mailer.success_subject_v2", locale: locale)
    message = I18n.t("txt.email.sandbox_mailer.success_body_v2", sandbox_subdomain: sandbox_subdomain, locale: locale)
    content = sandbox_template(user.name, message, locale)
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: subject
    ) do |format|
      simple_multipart account, subject, content, format
    end
  end

  def ready_notification(account, user, sandbox_subdomain)
    locale = recipient_locale(user)
    set_headers(account, X_DELIVERY_CONTEXT => "ready-notification-#{account.id}")
    subject = I18n.t("txt.email.sandbox_mailer.ready_subject", locale: locale)
    message = I18n.t("txt.email.sandbox_mailer.ready_body", sandbox_subdomain: sandbox_subdomain, locale: locale)
    content = sandbox_template(user.name, message, locale)
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: subject
    ) do |format|
      simple_multipart account, subject, content, format
    end
  end

  def failure_notification(account, user)
    locale = recipient_locale(user)
    set_headers(account, X_DELIVERY_CONTEXT => "failure-notification-#{account.id}")
    subject = I18n.t("txt.email.sandbox_mailer.failure_subject", locale: locale)
    message = I18n.t("txt.email.sandbox_mailer.failure_body", locale: locale)
    content = sandbox_template(user.name, message, locale)
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: subject
    ) do |format|
      simple_multipart account, subject, content, format
    end
  end

  private

  def sandbox_greeting(admin_name, locale)
    I18n.t("txt.email.sandbox_mailer.greeting_body", admin_name: admin_name, locale: locale)
  end

  def sandbox_template(admin_name, message, locale)
    "#{sandbox_greeting(admin_name, locale)}#{message}#{sandbox_ending(locale)}"
  end

  def sandbox_ending(locale)
    I18n.t("txt.email.sandbox_mailer.ending_body", locale: locale)
  end

  def simple_multipart(account, subject, content, format)
    format.text do
      Zendesk::Liquid::MailContext.render(
        template: account.text_mail_template,
        header: subject,
        content: content.strip_tags,
        footer: footer(account, true),
        account: account
      )
    end

    format.html do
      Zendesk::Liquid::MailContext.render(
        template: account.html_mail_template,
        header: subject,
        content: content,
        footer: footer(account),
        account: account
      )
    end
  end
end
