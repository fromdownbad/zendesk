module Devices
  class Mailer < ApplicationMailer
    def new_device_notification(device)
      @device = device
      @user = device.user
      @account = @user.account
      @locale = recipient_locale(@user)
      @devices_url = user_devices_url(@account, @user)
      @subject = I18n.t('txt.admin.models.devices.mailer.new_device_notification.subject', locale: @locale)

      set_headers(@device.account, X_DELIVERY_CONTEXT => "device-#{@device.id}")

      mail(
        from: Zendesk::Mailer::ZendeskAddress.new.from,
        to: @device.user.email,
        subject: @subject
      )
    end

    private

    def user_devices_url(account, user)
      "https://#{account.host_name}/agent/users/#{user.id}/devices_and_apps"
    end
  end
end
