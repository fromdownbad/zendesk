class EmailForwardingMailer < ApplicationMailer
  def forwarding_complete_notice(user, forward_from_email)
    I18n.with_locale(recipient_locale(user)) do
      account = user.account

      content = I18n.t('txt.zero_states.email_forward_complete_ticket.body_1', email: forward_from_email)
      subject = I18n.t('txt.zero_states.email_forward_complete_ticket.subject')

      set_headers(account, X_DELIVERY_CONTEXT => 'email-forwarding-complete')

      mail(
        from: from_address(account),
        to: forward_from_email,
        subject: subject
      ) { |format| render_email_parts account, subject, content, format }
    end
  end
end
