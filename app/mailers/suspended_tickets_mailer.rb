class SuspendedTicketsMailer < ApplicationMailer
  include Zendesk::Mailer::ReadOnly

  def suspended_notification(account, suspended_tickets, suspended_tickets_count)
    ActiveRecord::Base.on_shard(account.shard_id) do
      set_headers(account, X_DELIVERY_CONTEXT => "suspended-notice-#{account.id}")

      email_locale = account.translation_locale

      suspended_tickets_list = "<ul>"
      suspended_tickets.each do |suspended_ticket|
        suspended_tickets_list << "<li>#{CGI.escapeHTML(suspended_ticket.title)}</li>"
      end
      suspended_tickets_list << "</ul>"
      suspended_tickets_url = if read_only { account.has_email_agent_non_hostmapped_urls? }
        "<a href=\"#{account.url(mapped: false, ssl: true)}/agent/filters/suspended\">"
      else
        "<a href=\"#{account.url}/agent/filters/suspended\">"
      end
      content = I18n.t("txt.email.suspended_notification.message", suspended_tickets_count: suspended_tickets_count, suspended_tickets_list: suspended_tickets_list, suspended_tickets_url: suspended_tickets_url, locale: email_locale)
      title = I18n.t("txt.email.suspended_notification.title", account_name: account.name, suspended_tickets_count: suspended_tickets_count, locale: email_locale)

      mail(
        subject: title,
        to: account.suspended_ticket_notification.email_list.tokenize,
        from: "<noreply@#{account.default_host}>"
      ) do |format|
        format.text { content.strip_tags }
        format.html do
          Zendesk::Liquid::MailContext.render(
            template: account.html_mail_template,
            header: title,
            content: content.auto_link,
            footer: footer(account, false, nil, email_locale),
            account: account
          )
        end
      end
    end
  end
end
