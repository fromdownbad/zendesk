class AccountsMailer < ApplicationMailer
  def remote_authentication_activated(account, authentication)
    set_headers(account, X_DELIVERY_CONTEXT => "remote-auth-activated-#{account.id}")
    owner   = account.owner
    subject = I18n.t('txt.email.remote_authentication_activated.subject', locale: recipient_locale(owner))
    mail(
      from: 'support@support.zendesk.com',
      to: owner.email,
      subject: subject
    ) do |format|
      content = <<~HTML
        <h3>#{I18n.t('txt.email.remote_authentication_activated.specific_remote_has_been_activated', name: authentication.name, locale: recipient_locale(owner))}</h3>

        <p>#{I18n.t('txt.email.remote_authentication_activated.remember_you_can_always_go_to', account_url: account.url, locale: recipient_locale(owner))}</p>
      HTML

      simple_multipart account, subject, content, format
    end
  end

  # Welcome email for account owner
  #
  # Do not take `account_url` off args. We cannot use `owner.account.url` b/c
  # it often has an out of sync value of `z3nprecreated*`.
  #
  # `account_url` is made optional for backward compatibility.
  #
  # We cannot use keyword args in any instance method of our mailers without
  # first making changes to `Zendesk::OutgoingMail::Mailer.method_missing` to
  # take double splat in these locations:
  # - deprecated_mailer_api.rb
  # - https://github.com/eac/action_mailer-enqueable/blob/master/lib/action_mailer/enqueable.rb
  def welcome_account_owner(user, options = {})
    account = user.account

    headers = { X_DELIVERY_CONTEXT => "welcome-owner-#{user.id}" }

    if options.with_indifferent_access[:priority_mail]
      headers[X_ZENDESK_PRIORITY_MAIL] = options.with_indifferent_access[:priority_mail]
    end

    set_headers(account, headers)

    account_url = options.with_indifferent_access[:account_url] || account.url

    Rails.logger.info("AccountsMailer#welcome_account_owner account url string val #{options['account_url']}, derived val #{account.url}.")

    email_locale = recipient_locale(user)
    link = account_url + user.email_verification_path

    if account.trial_extras_buy_now?('zendesk_support') || account.zendesk_suite_trial_extras_buy_now?
      content = welcome_email_for_buy_now_support(link, user)
      title = I18n.t('txt.email.accounts_mailer.welcome_account_owner_direct_buy_support.v1.subject_of_email', locale: email_locale)
    else
      content = welcome_email(link, user, account_url)
      title = I18n.t('txt.email.accounts_mailer.welcome_account_owner.subject_of_email', locale: email_locale)
    end

    mail(
      subject: title,
      to: user.email,
      from: Zendesk::Mailer::ZendeskAddress.new.from
    ) do |format|
      format.text { content.strip_tags }
      format.html { zendesk_templated_body(title, content, email_locale) }
    end
  end

  def owner_email_change(user, previous_owner_email)
    account = user.account

    set_headers(account, X_DELIVERY_CONTEXT => "owner-email-change-#{user.id}")
    email_locale = recipient_locale(user)

    content = "#{I18n.t('txt.email.accounts_mailer.owner_email_change.v1.email_body_1', subdomain: account.subdomain, locale: email_locale)}<br><br>\n\n\n\n"
    content << "#{I18n.t('txt.email.accounts_mailer.welcome_account_owner.v3.email_body_6', locale: email_locale)}<br><bodyr>\n\n"

    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: previous_owner_email,
      subject: I18n.t('txt.email.accounts_mailer.owner_email_change.subject_of_email', locale: email_locale),
      content_type: Mime[:html].to_s,
      body: content
    )
  end

  def external_email_credential_test(account, user, external_email_credential)
    email_locale = recipient_locale(user)
    settings_link = %(<a href="#{account.url}#{account.email_settings_path}">#{I18n.t('txt.email.accounts_mailer.external_email_credential_test.settings_page', locale: email_locale)}</a>)
    subject = I18n.t('txt.admin.views.settings.email._settings.external_email_credentials.header_v2', locale: email_locale)

    set_headers account

    mail(
      from: noreply_address(account),
      to: external_email_credential.email,
      subject: subject
    ) do |format|
      body = I18n.t(
        'txt.email.accounts_mailer.external_email_credential_test.body_v2',
        locale: email_locale,
        settings_link: settings_link,
        email: external_email_credential.email,
        user: user.safe_name(true)
      )
      simple_multipart account, subject, body, format
    end
  end

  def forwarding_status_verification(account, recipient_address)
    set_headers account
    headers 'X-Zendesk-Verification' => 'ZFVTest' # make mail_parsing_queue let us through
    subject = I18n.t("txt.email.accounts_mailer.forwarding_status_verification.subject", locale: account.translation_locale)

    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      reply_to: "support@support.zendesk.com",
      to: recipient_address.email,
      subject: subject
    ) do |format|
      body = I18n.t(
        "txt.email.accounts_mailer.forwarding_status_verification.body_v2",
        email: recipient_address.brand.default_recipient_address.email,
        locale: account.translation_locale,
        code: %(<span style="color:white">#{recipient_address.forwarding_code}</span>)
      )

      simple_multipart account, subject, body, format
    end
  end

  # Email sent to accout admins when forwarding verification email from gmail is received
  def gmail_forwarding_verification_notification(account, user, content)
    locale = recipient_locale(user)

    set_headers account
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: I18n.t('txt.email.accounts_mailer.gmail_forwarding_verification.email_subject', locale: locale),
      content_type: 'text/plain',
      body: I18n.t(
        'txt.email.accounts_mailer.gmail_forwarding_verification.someone_from_your',
        locale: locale,
        token: content
      )
    )
  end

  # we sent too many emails via api, have to fall back to forwarding for the day
  def gmail_sending_rate_limit_reached(account, email)
    user = account.owner
    locale = recipient_locale(user)

    set_headers account
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: I18n.t(
        'txt.email.accounts_mailer.gmail_sending_rate_limit_reached.subject',
        locale: locale,
        email: email
      ),
      content_type: 'text/html',
      body: I18n.t(
        'txt.email.accounts_mailer.gmail_sending_rate_limit_reached.body',
        locale: locale,
        email: email,
        subdomain: account.subdomain
      )
    )
  end

  # user signed up wanted gmail api sending but the api is disabled
  def gmail_sending_api_disabled(account, email)
    user = account.owner
    locale = recipient_locale(user)

    set_headers account
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: I18n.t(
        'txt.email.accounts_mailer.gmail_sending_api_disabled.subject',
        locale: locale
      ),
      content_type: 'text/html',
      body: I18n.t(
        'txt.email.accounts_mailer.gmail_sending_api_disabled.body',
        locale: locale,
        email: email,
        subdomain: account.subdomain
      )
    )
  end

  def update_subscription_request(account, new_user_email, new_user_name, new_user_role, agent_name)
    user = account.owner
    locale = recipient_locale(user)
    billing_url = "https://#{domain_name account}.com/admin/billing/subscription?ajs_event=ClickedEmailLink&ajs_prop_source=UpdateSubscription"

    subject = I18n.t(
      'txt.email.accounts_mailer.update_subscription.subject',
      locale: locale,
      account_name: account.name
    )

    html_content = I18n.t(
      'txt.email.accounts_mailer.update_subscription.html_body_v2',
      locale:         locale,
      new_user_email: new_user_email,
      new_user_name:  new_user_name,
      new_user_role:  new_user_role,
      agent_name:     agent_name,
      owner_name:     user.name,
      billing_url:    billing_url
    )

    txt_content = I18n.t(
      'txt.email.accounts_mailer.update_subscription.txt_body',
      locale:         locale,
      new_user_email: new_user_email,
      new_user_name:  new_user_name,
      new_user_role:  new_user_role,
      agent_name:     agent_name,
      owner_name:     user.name,
      billing_url:    billing_url
    )

    content = {html: html_content, text: txt_content}

    set_headers account
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: subject
    ) { |format| render_email_parts account, subject, content, format, nil, locale }
  end

  def low_seat_count_notification(account)
    user = account.owner
    locale = recipient_locale(user)

    if account.subscription.sales_model_value == ZBC::States::SalesModel::ASSISTED
      link = I18n.t(
        "txt.email.accounts_mailer.contact_zendesk_customer_support_url",
        locale: locale
      )
      cta_target = link
      html_content_key = "txt.email.accounts_mailer.low_seat_count_notification.sales_assisted.html_body_v3"
      text_content_key = "txt.email.accounts_mailer.low_seat_count_notification.sales_assisted.text_body_v2"

      html_content = I18n.t(
        html_content_key,
        link_content:   view_context.link_to(I18n.t('txt.email.accounts_mailer.contact_your_sales_rep'), link),
        account_name:   account.name,
        locale:         locale,
        owner_name:     user.name
      )
    else
      qs = {
        ajs_event: 'ClickedEmailLink',
        ajs_prop_source: 'LowSeatCount'
      }.tap do |h|
        if account.has_allow_seats_remaining_suite?
          h[:zsub] = account.subdomain.to_s
          h[:cpgid] = 'lowseatcount_email'
        end
      end.to_query

      cta_target = if account.has_allow_seats_remaining_suite?
        environment = Zendesk::Configuration.fetch(:host)
        "https://billing.#{environment}/billing/manage?#{qs}"
      else
        "https://#{domain_name account}.com/admin/billing/subscription?#{qs}"
      end

      html_content_key = "txt.email.accounts_mailer.low_seat_count_notification.self_serve.html_body_v2"
      text_content_key = "txt.email.accounts_mailer.low_seat_count_notification.self_serve.text_body"
      html_content = I18n.t(
        html_content_key,
        link_content:   view_context.link_to(I18n.t('txt.email.accounts_mailer.subscription_page'), cta_target),
        account_name:   account.name,
        locale:         locale,
        owner_name:     user.name
      )
    end

    subject = I18n.t(
      "txt.email.accounts_mailer.low_seat_count_notification.subject",
      account_name: account.name,
      locale:       locale
    )

    text_content = I18n.t(
      text_content_key,
      account_name:   account.name,
      cta_target:     cta_target,
      locale:         locale,
      owner_name:     user.name
    )

    content = { html: html_content, text: text_content }

    set_headers account
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: user.email,
      subject: subject
    ) { |format| render_email_parts account, subject, content, format, nil, locale }
  end

  private

  def domain_name(account)
    Rails.env.staging? ? "#{account.subdomain}.zendesk-staging" : "#{account.subdomain}.zendesk"
  end

  def welcome_email(link, user, account_url)
    email_locale = recipient_locale(user)
    "#{I18n.t(
      'txt.email.accounts_mailer.welcome_account_owner.v3.email_body_1',
      locale: email_locale
    )}<br><br>\n\n\n\n
      #{I18n.t(
        'txt.email.accounts_mailer.welcome_account_owner.v3.email_body_2',
        locale: email_locale
      )}<br><br>\n\n\n\n
      #{I18n.t(
        'txt.email.accounts_mailer.welcome_account_owner.v6.email_body_3',
        link: link,
        account_url: account_url,
        locale: email_locale
      )}<br><br>\n\n\n\n
      #{I18n.t(
        'txt.email.accounts_mailer.welcome_account_owner.v4.email_body_4',
        link: account_url,
        locale: email_locale
      )}<br><br>\n\n\n\n
      #{I18n.t(
        'txt.email.accounts_mailer.welcome_account_owner.v3.email_body_5',
        locale: email_locale
      )}<br>\n\n
      #{I18n.t(
        'txt.email.accounts_mailer.welcome_account_owner.v3.email_body_6',
        locale: email_locale
      )}<br>\n\n"
  end

  def welcome_email_for_buy_now_support(link, user)
    email_locale = recipient_locale(user)
    "<style>
      h2,a,p {
        padding-left: 30px;
        max-width: 500px;
      }
    </style>
    #{I18n.t(
      'txt.email.accounts_mailer.welcome_account_owner_direct_buy_support.v2.email_body_1',
      locale: email_locale
    )}<br>\n\n
    #{I18n.t(
      'txt.email.accounts_mailer.welcome_account_owner_direct_buy_support.v1.email_body_4',
      locale: email_locale
    )}<br>\n\n
    #{I18n.t(
      'txt.email.accounts_mailer.welcome_account_owner_direct_buy_support.v1.email_body_7',
      link: link,
      locale: email_locale
    )}<br><br>\n\n\n\n
    #{I18n.t(
      'txt.email.accounts_mailer.welcome_account_owner_direct_buy_support.v1.email_body_5',
      locale: email_locale
    )}\n
    #{I18n.t(
      'txt.email.accounts_mailer.welcome_account_owner_direct_buy_support.v1.email_body_6',
      locale: email_locale
    )}<br>"
  end

  def simple_multipart(account, subject, content, format)
    format.text do
      Zendesk::Liquid::MailContext.render(
        template: account.text_mail_template,
        header: subject,
        content: content.strip_tags,
        footer: footer(account, true),
        account: account
      )
    end

    format.html do
      Zendesk::Liquid::MailContext.render(
        template: account.html_mail_template,
        header: subject,
        content: content,
        footer: footer(account),
        account: account
      )
    end
  end
end
