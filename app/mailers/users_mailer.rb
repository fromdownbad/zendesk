require 'zendesk/outgoing_mail/variables'

class UsersMailer < ApplicationMailer
  include Zendesk::Mailer::ReadOnly

  SUPPORT_EMAIL = "support@support.#{Zendesk::Configuration.fetch(:host)}".freeze

  # Verifying users or tickets
  def verify(identity, user_verify_email_text = nil, user_verify_email_subject = nil, token = nil, brand_id = nil, make_primary = false)
    token ||= identity.verification_token
    user = identity.user
    account = identity.account

    return verify_multiproduct_staff(identity, make_primary) if verify_multiproduct_staff?(account, user)

    locale       = recipient_locale(user, account)
    brand_used   = brand(brand_id, account)
    placeholders = { account_name: brand_used.name, locale: locale }
    new_agent    = false

    link = "#{brand_used.url}/verification/email/#{token}"
    link += "?make_primary=true" if make_primary

    set_headers(
      account,
      X_DELIVERY_CONTEXT => "verify-email-#{identity.id}",
      X_ZENDESK_PRIORITY_MAIL => "Verification Email"
    )

    if user_verify_email_text.present?
      text = user_verify_email_text
      subject_text = user_verify_email_subject
    elsif Arturo.feature_enabled_for?(:restrict_identity_verification_email, account) && user.identities.reload.size > 1 && user.identities.first.is_verified?
      text = account.verify_email_text
      subject_text = I18n.t('txt.email.verification.subject', placeholders)
    elsif !Arturo.feature_enabled_for?(:restrict_identity_verification_email, account) && user.identities.reload.size > 1
      text = account.verify_email_text
      subject_text = I18n.t('txt.email.verification.subject', placeholders)
    else
      if user.is_end_user?
        text         = account.signup_email_text
        subject_text = I18n.t('txt.email.welcome_subject.end_user', placeholders)
      else
        video_url     = I18n.t('txt.onboarding.url.how_zendesk_work', locale: locale)
        intro_content = I18n.t('txt.onboarding.intro.video_on_how_zendesk_works', video_url: video_url, locale: locale)

        extended_placeholders = placeholders.merge(
          name: user.name,
          verify_link: link,
          zendesk_url: "<span>#{account.url}</span>",
          intro_content: locale.english? ? intro_content : ''
        )

        is_google_login_service = account.role_settings.only_login_service_allowed_for_user?(:google, user)
        text = if is_google_login_service
          I18n.t('txt.email.welcome_body.no_verification.agent', extended_placeholders)
        else
          I18n.t('txt.email.welcome_body.agent', extended_placeholders)
        end
        subject_text = I18n.t('txt.email.welcome_subject.agent', placeholders)
        new_agent    = true
      end

      @block_deliveries = true if user.machine?
    end

    mail(
      subject: subject_text,
      to: identity.value,
      from: from_address(account, brand: brand_used)
    ) do |format|
      render_verify_email(
        account,
        text: Zendesk::Liquid::DcContext.render(text, account, user, Mime[:text].to_s, locale),
        html: Zendesk::Liquid::DcContext.render(text, account, user, Mime[:html].to_s, locale),
        link: link, user: user, locale: locale, new_agent: new_agent, format: format, subject: subject_text, brand: brand_used
      )
    end

    message.perform_deliveries = false if @block_deliveries
  end

  def verify_multiproduct_staff?(account, user)
    (account.multiproduct? && !user.is_end_user?) || user.is_contributor?
  end

  def verify_multiproduct_staff(identity, make_primary = false)
    account = identity.account
    user    = identity.user
    locale  = recipient_locale(user, account)

    link = "#{account.url}/verification/email/#{identity.verification_token}"
    link += "?make_primary=true" if make_primary

    placeholders = {
      account_name: account.name,
      locale: locale,
      name: user.name,
      verify_link: link,
      zendesk_url: account.url
    }

    set_headers(
      account,
      X_DELIVERY_CONTEXT => "verify-email-#{identity.id}",
      X_ZENDESK_PRIORITY_MAIL => "Verification Email"
    )

    if user.identities.reload.size > 1 # Verifying second identity
      subject = I18n.t('txt.email.verification.subject', placeholders)
      text = I18n.t('txt.default.verify_email_text', placeholders)
      new_agent = false
    else
      subject = I18n.t('txt.email.welcome_subject.multiproduct.agent', placeholders)
      text = if account.role_settings.only_login_service_allowed_for_role?(:google, :agent)
        I18n.t('txt.email.welcome_body.multiproduct.no_verification.agent', placeholders)
      else
        I18n.t('txt.email.welcome_body.multiproduct.agent', placeholders)
      end
      new_agent = true
    end

    @block_deliveries = true if user.machine?

    mail(
      subject: subject,
      to: identity.value,
      from: from_address(account)
    ) do |format|
      render_verify_email(
        account,
        text: Zendesk::Liquid::DcContext.render(text, account, user, Mime[:text].to_s, locale),
        html: Zendesk::Liquid::DcContext.render(text, account, user, Mime[:html].to_s, locale),
        link: link,
        user: user,
        locale: locale,
        new_agent: new_agent,
        format: format,
        subject: subject,
        text_template: Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE,
        html_template: Zendesk::OutgoingMail::Variables::ACCESSIBLE_HTML_MAIL_TEMPLATE
      )
    end

    message.perform_deliveries = false if @block_deliveries
  end

  def verify_email_address(unverified_email, brand_id)
    return verify_multiproduct_email_address(unverified_email) if unverified_email.account.multiproduct?

    account = unverified_email.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "verify-email-address-#{unverified_email.id}",
      X_ZENDESK_PRIORITY_MAIL => "Verify Email Address"
    )

    link = "#{brand(brand_id, account).url}/verify/email/#{unverified_email.token}"
    locale = recipient_locale(unverified_email.user)
    brand_used = brand(brand_id, account)
    subject = I18n.t('txt.email.verification.subject', account_name: brand_used.name, locale: locale)

    mail(
      from: from_address(account, brand: brand_used),
      to: unverified_email.email,
      subject: subject
    ) do |format|
      render_verify_email(
        account,
        text: Zendesk::Liquid::DcContext.render(account.verify_email_text, account, unverified_email.user),
        html: Zendesk::Liquid::DcContext.render(account.verify_email_text, account, unverified_email.user, Mime[:html].to_s),
        link: link, user: unverified_email.user, locale: locale, format: format, subject: subject, brand: brand_used
      )
    end
  end

  def verify_multiproduct_email_address(unverified_email)
    account = unverified_email.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "verify-email-address-#{unverified_email.id}",
      X_ZENDESK_PRIORITY_MAIL => "Verify Email Address"
    )

    link = "#{account.url}/verify/email/#{unverified_email.token}"
    locale = recipient_locale(unverified_email.user)
    subject = I18n.t('txt.email.verification.subject', account_name: account.name, locale: locale)
    placeholders = {
      account_name: account.name,
      locale: locale,
      verify_link: link,
      zendesk_url: account.url
    }
    text = I18n.t('txt.default.verify_email_text', placeholders)

    mail(
      from: from_address(account),
      to: unverified_email.email,
      subject: subject
    ) do |format|
      render_verify_email(
        account,
        text: Zendesk::Liquid::DcContext.render(text, account, unverified_email.user),
        html: Zendesk::Liquid::DcContext.render(text, account, unverified_email.user, Mime[:html].to_s),
        link: link, user: unverified_email.user, locale: locale, format: format, subject: subject,
        text_template: Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE,
        html_template: Zendesk::OutgoingMail::Variables::ACCESSIBLE_HTML_MAIL_TEMPLATE
      )
    end
  end

  def multiproduct_admin_password_change(user, token)
    account = user.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "new-password-#{user.id}",
      X_ZENDESK_PRIORITY_MAIL => "Admin Password Change"
    )

    I18n.with_locale(user.translation_locale) do
      content = "#{I18n.t('txt.mailers.users_mailers.admin_password_change.content_1', account_name: account.name)}\n\n"
      content << "#{account.url}/password/create/#{token}/"
      subject = I18n.t('txt.mailers.users_mailers.admin_password_change.title', account_name: account.name)

      mail(
        from: from_address(account),
        to: user.email,
        subject: subject
      ) do |format|
        render_email_parts(
          account,
          subject,
          content,
          format
        )
      end
    end
  end

  def multiproduct_password_reset(user, token)
    account = user.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "new-password-#{user.id}",
      X_ZENDESK_PRIORITY_MAIL => "Password Reset"
    )

    locale = recipient_locale(user, account)
    reset_url = "#{account.url}/password/reset/#{token}?locale=#{locale.id}"
    content = I18n.t("txt.email.password_reset.message", reset_url: reset_url, account_url: account.url, locale: locale)
    plain_content = content
    html_content = content.format_and_link
    subject = I18n.t("txt.email.password_reset.title", account_name: account.name, locale: locale)

    mail(
      from: from_address(account),
      to: user.email,
      subject: subject
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE,
          header: subject,
          content: plain_content,
          footer: footer(account, true, nil, locale),
          account: account
        )
      end
      format.html do
        Zendesk::Liquid::MailContext.render(
          template: Zendesk::OutgoingMail::Variables::ACCESSIBLE_HTML_MAIL_TEMPLATE,
          header: subject,
          content: add_direction(account, locale, :content, html_content),
          footer: add_direction(account, locale, :footer, footer(account, false, nil, locale)),
          account: account
        )
      end
    end
  end

  def password_reset(user, token, brand_id = nil)
    return multiproduct_password_reset(user, token) if user.account.multiproduct?
    account = user.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "new-password-#{user.id}",
      X_ZENDESK_PRIORITY_MAIL => "Password Reset"
    )

    locale = recipient_locale(user, account)
    reset_url = generate_reset_link(brand_id, account, token, locale)
    brand_used = brand(brand_id, account)
    content = I18n.t("txt.email.password_reset.message", reset_url: reset_url, account_url: brand_used.url, locale: locale)
    plain_content = content + brand_portals(Mime[:text].to_s, locale, user)
    html_content = content.format_and_link + brand_portals(Mime[:html].to_s, locale, user)
    subject = I18n.t("txt.email.password_reset.title", account_name: brand_used.name, locale: locale)

    mail(
      from: from_address(account, brand: brand_used),
      to: user.email,
      subject: subject
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: subject,
          content: plain_content,
          footer: footer(account, true, nil, locale, brand_used),
          account: account
        )
      end
      format.html do
        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: subject,
          content: add_direction(account, locale, :content, html_content),
          footer: add_direction(account, locale, :footer, footer(account, false, nil, locale, brand_used)),
          account: account
        )
      end
    end
  end

  def admin_password_reset(user, token)
    account = user.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "new-password-#{user.id}",
      X_ZENDESK_PRIORITY_MAIL => "Admin Password Reset"
    )

    I18n.with_locale(recipient_locale(user)) do
      content = "#{I18n.t('txt.email.users_mailer.admin_password_reset.this_email_has_been_Sent_because_someone_reset_your_password')}\n\n"
      content << "#{I18n.t('txt.email.users_mailer.admin_password_reset.visit_the_following_url_to_set_new_password')}\n\n"
      content << "#{brand(nil, account).url}/password/reset/#{token}"
      subject = I18n.t('txt.email.users_mailer.admin_password_reset.subject', account_name: account.name)

      mail(
        from: from_address(account, brand: brand(nil, account)),
        to: user.email,
        subject: subject
      ) do |format|
        render_email_parts(account, subject, content, format)
      end
    end
  end

  def admin_password_change(user, token, brand_id = nil)
    account = user.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "new-password-#{user.id}",
      X_ZENDESK_PRIORITY_MAIL => "Admin Password Change"
    )

    I18n.with_locale(user.translation_locale) do
      brand_used = brand(brand_id, account)
      content = "#{I18n.t('txt.mailers.users_mailers.admin_password_change.content_1', account_name: brand_used.name)}\n\n"
      content << "#{brand_used.url}/password/create/#{token}/"
      subject = I18n.t('txt.mailers.users_mailers.admin_password_change.title', account_name: brand_used.name)

      mail(
        from: from_address(account, brand: brand_used),
        to: user.email,
        subject: subject
      ) do |format|
        render_email_parts(account, subject, content, format, brand_used)
      end
    end
  end

  def reminder(email, _unused_account_for_delivery, accounts)
    set_headers(
      support_account,
      X_DELIVERY_CONTEXT => "reminder-#{email}",
      X_ZENDESK_PRIORITY_MAIL => "Password Reminder"
    )

    locale = recipient_locale(email, accounts.first)
    account_list = "<p><ul><li>#{accounts.compact.collect(&:url).join('</li><li>')}</li></ul></p>"
    content = I18n.t("txt.email.recover_zendesk_url.message", account_list: account_list, locale: locale)
    subject = I18n.t("txt.email.recover_zendesk_url.title", locale: locale)

    mail(
      from: Zendesk::Mailer::ZendeskAddress.new("Jennifer Hansen").from,
      to: email,
      subject: subject
    ) do |format|
      format.text { content.strip_tags }
      format.html { zendesk_templated_body(subject, content, locale) }
    end
  end

  def reminder_no_accounts(email, locale, _unused_account_for_delivery)
    I18n.with_locale(locale) do
      set_headers(
        support_account,
        X_DELIVERY_CONTEXT => "reminder-#{email}"
      )

      content = I18n.t("txt.email.recover_zendesk_url.message_nothing_found")
      subject = I18n.t("txt.email.recover_zendesk_url.title")

      mail(
        from: Zendesk::Mailer::ZendeskAddress.new("Jennifer Hansen").from,
        to: email,
        subject: subject
      ) do |format|
        format.text { content.strip_tags }
        format.html { zendesk_templated_body(subject, content, locale) }
      end
    end
  end

  def suspend_verify(suspended_ticket)
    account = suspended_ticket.account
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "suspended-ticket-verification-#{suspended_ticket.id}",
      X_ZENDESK_PRIORITY_MAIL => __method__.to_s.titleize
    )

    new_user = account.users.new(locale_id: suspended_ticket.properties[:locale_id]) if suspended_ticket.properties && suspended_ticket.properties[:locale_id]
    brand_used = brand(suspended_ticket.brand_id, account)
    locale = recipient_locale(new_user, account)
    text = Zendesk::Liquid::DcContext.render(account.signup_email_text, account, new_user, Mime[:text].to_s, locale)
    html = Zendesk::Liquid::DcContext.render(account.signup_email_text, account, new_user, Mime[:html].to_s, locale)
    link = "#{brand_used.url}/verification/ticket/#{suspended_ticket.token}/"
    subject = I18n.t('txt.email.welcome.subject', account_name: account.name, locale: locale)

    mail(
      from: from_address(account, brand: brand_used),
      to: suspended_ticket.from_mail,
      subject: subject
    ) do |format|
      render_verify_email(
        account, subject: subject, text: text, html: html, link: link, user: nil, locale: locale, format: format
      )
    end
  end

  private

  def generate_reset_link(brand_id, account, token, locale)
    base_url = brand(brand_id, account).url
    "#{base_url}/password/reset/#{token}?locale=#{locale.id}"
  end

  def brand(brand_id, account)
    agent_brand = account.route.brand

    user_brand = account.brands.find_by_id(brand_id) if brand_id
    return user_brand if user_brand

    # If agent route has HC, choose the agent_route brand
    if account.route.brand.try(:help_center_enabled?)
      agent_brand
    # If agent route does not have HC, choose the default brand
    elsif account.default_brand.try(:help_center_enabled?)
      account.default_brand
    # If neither default nor agent route has HC, choose the oldest brand and use the agent_route brand as a fallback
    else
      hc_brands = account.brands.active.select(&:help_center_enabled?)
      sorted_brands = hc_brands.sort_by(&:created_at)
      sorted_brands.empty? ? agent_brand : sorted_brands.first
    end
  end

  def support_account
    # NOTE: is_serviceable? => true is required to deliver emails without X_ZENDESK_PRIORITY_MAIL header.

    @support_account ||= OpenStruct.new(
      backup_email_address: SUPPORT_EMAIL,
      is_serviceable?: true
    )
  end

  def brand_portals(content_type, locale, user)
    return '' unless account.settings.list_help_centers_in_account_emails?
    return '' unless user.try(:is_end_user?)
    brands_with_hc = account.brands.active.select(&:help_center_enabled?)
    return '' unless brands_with_hc.count > 1

    text = I18n.t("txt.email.multiple_hc_portals.message", account_name: account.name, locale: locale)

    brand_urls = brands_with_hc.map(&:url)
    content_type == Mime[:text].to_s ? render_brand_urls_plain(brand_urls, text) : render_brand_urls_html(brand_urls, text)
  end

  def render_brand_urls_html(brand_urls, text)
    brand_list = brand_urls.map { |url| "<li>#{url}</li>" }.join('')
    "<p>#{text}<ul>#{brand_list}</ul></p>"
  end

  def render_brand_urls_plain(brand_urls, text)
    brand_list = brand_urls.map { |url| "#{url}\n" }.join('')
    text + brand_list
  end

  def render_verify_email(account, link:, user:, locale:, new_agent: false, text:, html:, format:, subject:, brand: nil, text_template: nil, html_template: nil)
    format.text do
      Zendesk::Liquid::MailContext.render(
        template: text_template || account.text_mail_template,
        header: subject,
        content: new_agent ? text : "#{text}\n\n#{link}\n\n#{brand_portals(Mime[:text].to_s, locale, user)}",
        footer: new_agent ? '' : footer(account, true, user, locale, brand),
        account: account,
        translation_locale: locale
      )
    end
    format.html do
      Zendesk::Liquid::MailContext.render(
        template: html_template || account.html_mail_template,
        header: subject,
        content: new_agent ? html.to_s : "#{html}\r\n<p><a href=\"#{link}\">#{link}</a></p>#{brand_portals(Mime[:html].to_s, locale, user)}",
        footer: new_agent ? '' : footer(account, false, user, locale, brand),
        account: account,
        translation_locale: locale
      )
    end
  end
end
