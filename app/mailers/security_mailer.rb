#
# All security related change notifications should be implemented in this mailer.
# Take care to group related notifications or implement new ones under existing groups.
#
class SecurityMailer < ApplicationMailer
  include SecurityNotifications::AccountAssumption
  include SecurityNotifications::AccountSettingsChanged
  include SecurityNotifications::CertificateStatusChanged
  include SecurityNotifications::PasswordStrategy
  include SecurityNotifications::UserIdentityChanged
  include SecurityNotifications::UserProfileChanged

  # options:
  #   :exclude - User to exclude (all user email identities are removed)
  #
  def security_notifications_recipient_emails(account, options = {})
    addresses(account) - excluded_addresses(options[:exclude])
  end

  private

  def brand(account, brand_id)
    account.brands.find_by_id(brand_id)
  end

  def formatted_brands(brands)
    brands.map { |brand| "· #{brand.url}\n" }.join("")
  end

  def addresses(account)
    Zendesk::Accounts::Security::Notifications.new(account).recipient_emails
  end

  def excluded_addresses(excluded_user)
    return [] unless excluded_user.present?

    excluded_user.identities.email.map(&:value)
  end

  def formatted_actor_details(actor, options = {})
    return unless actor

    if options[:account].try(:has_rtl_lang_html_attr?) && I18n.translation_locale.rtl?
      "#{actor.name} \u202B(#{actor.email})\u202C"
    else
      "#{actor.name} (#{actor.email})"
    end
  end
end
