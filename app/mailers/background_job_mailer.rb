class BackgroundJobMailer < ApplicationMailer
  def failure_alert(account, job_name, recipients)
    set_headers(account, X_DELIVERY_CONTEXT => "backgroundjob-failure-#{job_name}")
    content = "Hi #{recipients},\n\n"
    content << "#{job_name} failed at #{Time.now} on pod #{Zendesk::Configuration.fetch(:pod_id)}"

    mail(
      subject: "#{job_name} FAILURE",
      to: recipients,
      from: Zendesk::Mailer::ZendeskAddress.new("Jennifer Hansen").from,
      content_type: Mime[:text].to_s,
      body: content.strip_tags
    )
  end

  def completion_alert(account, job_name, recipients)
    set_headers(account, X_DELIVERY_CONTEXT => "backgroundjob-completion-#{job_name}")
    content = "Hi #{recipients},\n\n"
    content << "#{job_name} completed at #{Time.now} on pod #{Zendesk::Configuration.fetch(:pod_id)}"

    mail(
      subject: "#{job_name} Completion",
      to: recipients,
      from: Zendesk::Mailer::ZendeskAddress.new("Jennifer Hansen").from,
      content_type: Mime[:text].to_s,
      body: content.strip_tags
    )
  end
end
