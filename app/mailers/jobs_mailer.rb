class JobsMailer < ApplicationMailer
  def job_complete_to_multiple_recipients(users, identifier, title, content, options = {})
    options = options.with_indifferent_access
    emails = users.map(&:email).compact
    account = users.first.account
    set_headers(account, X_DELIVERY_CONTEXT => "job-complete-#{identifier}")
    translation_locale = options.fetch(:translation_locale, nil)

    mail(
      subject: title,
      to: emails,
      from: from_address(account)
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: title,
          content: content,
          footer: footer(account, true, users.first),
          account: account,
          translation_locale: translation_locale
        )
      end
      format.html do
        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: title,
          content: content.format_and_link,
          footer: footer(account, false, users.first),
          account: account,
          translation_locale: translation_locale
        )
      end
    end
  end

  def job_complete(user, identifier, title, content)
    # TODO: handle this case in a better way than returning early. JIRA: EM-2840
    return unless user.email

    set_headers(user.account, X_DELIVERY_CONTEXT => "job-complete-#{identifier}")
    account = user.account

    mail(
      subject: title,
      to: user.email,
      from: from_address(account)
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: title,
          content: content,
          footer: footer(account, true, user),
          account: account
        )
      end
      format.html do
        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: title,
          content: content.format_and_link,
          footer: footer(account, false, user),
          account: account
        )
      end
    end
  end

  def job_notification(mail_to, account, identifier, title, content)
    set_headers(account, X_DELIVERY_CONTEXT => "job-notification-#{identifier}")

    mail(
      from: from_address(account),
      to: mail_to,
      subject: title
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: title,
          content: content,
          footer: footer(account, true),
          account: account
        )
      end
      format.html do
        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: title,
          content: content.format_and_link,
          footer: footer(account),
          account: account
        )
      end
    end
  end
end
