class WelcomeAgentMailer < ApplicationMailer
  def google_verify(user_email_identity)
    @account      = user_email_identity.account
    @user         = user_email_identity.user
    @owner        = @account.owner
    @email_locale = recipient_locale(@owner, @account)
    @google_url   = access_google_url(
      host: @account.authentication_host,
      protocol: @account.authentication_scheme,
      google_domain: @account.authentication_domain,
      profile: 'google'
    )

    set_headers(@account, X_DELIVERY_CONTEXT => "google-verify-#{user_email_identity.id}")

    mail(
      from: Zendesk::Mailer::ZendeskAddress.new("Jennifer Hansen").from,
      to: user_email_identity.value,
      subject: I18n.t("txt.email.controllers.getting_started_controllers.agent_invitation_text.subject", current_user_name: @owner.name, locale: @email_locale)
    )
  end
end
