module SecurityNotifications
  module UserProfileChanged
    include Zendesk::Mailer::ReadOnly

    def profile_crypted_password_changed(user, actor, options = {})
      active_brand = brand(user.account, options['brand_id'])
      locale = recipient_locale(user, user.account)

      I18n.with_locale(locale) do
        subject = I18n.t("txt.email.password_changed.title")
        content = content_email_crypted_password_change(user, actor, active_brand)

        user_profile_change_notice(user,
          from: from_address(user.account, brand: active_brand, for_agent: user.is_agent?),
          context: :"user-profile-crypted-password-changed",
          subject: subject,
          content: content)
      end
    end

    # remove the following block when cleaning up arturo email_send_profile_admin_added_notice_only_once
    def profile_admin_user_added_v2(user, recipients, locale, actor)
      I18n.with_locale(locale) do
        subject = I18n.t("txt.email.admin_user_added.subject")
        content = I18n.t("txt.email.admin_user_added.content_v2",
          changed_by: formatted_actor_details(actor, account: user.account),
          admin_user_name: user.name,
          admin_user_email: user.email)

        user_profile_change_notice(user,
          context: :"user-profile-admin-user-added",
          recipients: recipients,
          subject: subject,
          content: content)
      end
    end

    def profile_admin_user_added(new_admin_user, recipient, bcc, actor)
      locale = recipient.translation_locale

      I18n.with_locale(locale) do
        subject = I18n.t("txt.email.admin_user_added.subject")
        content = I18n.t("txt.email.admin_user_added.content_v2",
          changed_by: formatted_actor_details(actor, account: new_admin_user.account),
          admin_user_name: new_admin_user.name,
          admin_user_email: new_admin_user.email)

        user_profile_change_notice(recipient,
          context: :"user-profile-admin-user-added",
          recipients: bcc,
          subject: subject,
          content: content)
      end
    end

    def profile_two_factor_auth_enabled(user)
      locale = recipient_locale(user, user.account)

      I18n.with_locale(locale) do
        subject = I18n.t("txt.email.two_factor_auth.enabled.subject_v2")

        content = I18n.t("txt.email.two_factor_auth.enabled.content_v5",
          user_name: user.name,
          recovery_url: "#{user.account.url(mapped: false, ssl: true)}/two_factor_authentication/recovery_codes.txt",
          hc_link: I18n.t('txt.two_factor_authentication.hc_link'))

        user_profile_change_notice(user,
          context: :"user-profile-two-factor-auth-enabled",
          subject: subject,
          content: content)
      end
    end

    def profile_two_factor_auth_disabled(user)
      locale = recipient_locale(user, user.account)

      I18n.with_locale(locale) do
        subject = I18n.t("txt.email.two_factor_auth.disabled.subject_v2")

        content = I18n.t("txt.email.two_factor_auth.disabled.content_v2",
          user_name: user.name,
          configuration_url: "#{user.account.url(ssl: true)}/agent/users/#{user.id}/security_settings")

        user_profile_change_notice(user,
          context: :"user-profile-two-factor-auth-disabled",
          subject: subject,
          content: content)
      end
    end

    private

    def user_profile_change_notice(user, options = {})
      account = user.account

      set_headers(user.account, Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT => "#{options.fetch(:context)}-#{user.id}")

      mail(
        from: options[:from] || from_address(account),
        to: user.email,
        bcc: Array(options[:recipients]) - [user.email],
        subject: options.fetch(:subject)
      ) { |format| render_email_parts account, options.fetch(:subject), options.fetch(:content), format }
    end

    def from_address(account, options = {})
      for_agent = options.fetch(:for_agent, false)
      account_address = Zendesk::Mailer::AccountAddress.new(options.merge(account: account))

      account_address.from(for_agent: for_agent)
    end

    def content_email_crypted_password_change(user, actor, active_brand)
      options = {
        sign_in_url: sign_in_url(user, actor, active_brand),
        password_was_updated_by: password_change_actor(user, actor),
        multiple_brands: multiple_brands(user)
      }

      I18n.t('txt.email.password_changed.content_v5_generic', options)
    end

    def multiple_brands(user)
      account = user.account

      return unless account.settings.list_help_centers_in_account_emails?
      brands_with_hc = account.brands.active.select(&:help_center_enabled?)
      return unless brands_with_hc.count > 1

      options = {
        brands: formatted_brands(brands_with_hc),
        account_name: user.account.name,
      }

      I18n.t('txt.email.password_changed.content_multiple_portals', options)
    end

    def password_change_actor(user, actor)
      if user == actor
        I18n.t('txt.email.password_changed.content_v5_by_you')
      else
        I18n.t('txt.email.password_changed.content_v5_by_admin')
      end
    end

    def sign_in_url(user, actor, active_brand)
      account = user.account

      domain = if account.multiproduct? && account.subscription.nil?
        account.url
      elsif user == actor && user.is_end_user? && active_brand
        active_brand.url
      elsif user.is_agent?
        account.authentication_domain
      else
        account.default_brand.url
      end

      "#{domain}/login"
    end
  end
end
