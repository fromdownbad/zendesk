module SecurityNotifications
  module UserIdentityChanged
    def user_identity_added(user, actor, options = {})
      options[:context] = :"user-identity-added"

      I18n.with_locale(user.translation_locale) do
        user_identity_change_notice(user, actor, options)
      end
    end

    def user_identity_removed(user, actor, options = {})
      options[:context] = :"user-identity-removed"

      I18n.with_locale(user.translation_locale) do
        user_identity_change_notice(user, actor, options)
      end
    end

    def user_primary_email_changed(user, actor, options = {})
      I18n.with_locale(user.translation_locale) do
        previous_email = options.fetch('previous_primary_email', user.previous_primary_email)
        subject        = I18n.t("txt.email.primary_email_changed.subject")
        profile_link   = user.account.multiproduct? ? "#{user.account.url}/admin/staff/#{user.id}" : "#{user.account.url}/users/#{user.id}"

        content = I18n.t(
          "txt.email.primary_email_changed.content_v2",
          actor: formatted_actor_details(actor, account: user.account),
          current: user.email,
          previous: previous_email.to_s,
          profile_link: profile_link
        )

        options.merge!(
          context: :"user-identity-updated",
          recipients: previous_email.to_s,
          subject: subject,
          content: content
        )

        user_identity_change_notice(user, actor, options)
      end
    end

    alias_method :user_voice_forwarding_number_added,   :user_identity_added
    alias_method :user_external_account_added,          :user_identity_added
    alias_method :user_auxillary_email_added,           :user_identity_added

    alias_method :user_voice_forwarding_number_removed, :user_identity_removed
    alias_method :user_external_account_removed,        :user_identity_removed
    alias_method :user_auxillary_email_removed,         :user_identity_removed

    private

    def i18n_key(identity_type, context)
      idx = context.rindex("-")
      "txt.email." + context.insert(idx, "_#{identity_type}").tr('-', '_')
    end

    def user_identity_change_notice(user, actor, options = {})
      account = user.account

      options.symbolize_keys!
      identity_type  = options.fetch(:identity_type)
      identity_value = options.fetch(:identity_value)

      # This ensures that phone numbers for this type of email keep the + sign
      # to the left of the number, even in RTL languages.
      # The email content is in `txt.email.user_identity_agent_forwarding_added.content_v2`
      # and `txt.email.user_identity_agent_forwarding_removed.content_v2`
      identity_value = "\u202D#{identity_value}\u202C" if identity_type == 'agent_forwarding'

      set_headers(account, Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT => "#{options.fetch(:context)}-#{user.id}")

      prefix  = i18n_key(identity_type, options.fetch(:context).to_s)
      title   = options[:subject] || I18n.t("#{prefix}.subject")
      content = options[:content] || I18n.t(
        "#{prefix}.content_v2",
        actor: formatted_actor_details(actor, account: account),
        identity_value: identity_value,
        profile_link: "#{account.url}/users/#{user.id}"
      )

      mail(
        from: from_address(account),
        to: user.email,
        bcc: Array(options[:recipients]) - [user.email],
        subject: title
      ) { |format| render_email_parts account, title, content, format }
    end
  end
end
