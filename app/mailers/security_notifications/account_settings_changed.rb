module SecurityNotifications
  module AccountSettingsChanged
    def ip_restriction_changed(setting, actor, options = {})
      I18n.with_locale(setting.account.translation_locale) do
        value_was = options.with_indifferent_access[:value_was]
        subject   = I18n.t("txt.email.settings.ip_restriction_changed.subject")
        content   = I18n.t(
          "txt.email.settings.ip_restriction_changed.content_v2",
          previous: value_was.blank? ? I18n.t("txt.email.settings.value_was_blank") : value_was,
          current: setting.value,
          changed_by: formatted_actor_details(actor, account: setting.account)
        )

        account_settings_change_notice(
          setting,
          context: :"account-texts-changed",
          subject: subject,
          content: content
        )
      end
    end

    def api_token_changed(setting, actor)
      I18n.with_locale(setting.account.translation_locale) do
        subject = I18n.t("txt.email.settings.api_token_changed.subject")
        content = I18n.t(
          "txt.email.settings.api_token_changed.content_v2",
          changed_by: formatted_actor_details(actor, account: setting.account)
        )

        account_settings_change_notice(
          setting,
          context: :"account-settings-changed",
          subject: subject,
          content: content
        )
      end
    end

    def api_token_access_enabled(setting, actor)
      I18n.with_locale(setting.account.translation_locale) do
        subject = I18n.t("txt.email.settings.api_token_access_enabled.subject")
        content = I18n.t(
          "txt.email.settings.api_token_access_enabled.content_v2",
          changed_by: formatted_actor_details(actor, account: setting.account)
        )

        account_settings_change_notice(
          setting,
          context: :"account-settings-changed",
          subject: subject,
          content: content
        )
      end
    end

    def api_token_access_disabled(setting, actor)
      I18n.with_locale(setting.account.translation_locale) do
        subject = I18n.t("txt.email.settings.api_token_access_disabled.subject")
        content = I18n.t(
          "txt.email.settings.api_token_access_disabled.content_v2",
          changed_by: formatted_actor_details(actor, account: setting.account)
        )

        account_settings_change_notice(
          setting,
          context: :"account-settings-changed",
          subject: subject,
          content: content
        )
      end
    end

    def api_password_access_enabled(setting, actor)
      I18n.with_locale(setting.account.translation_locale) do
        subject = I18n.t("txt.email.settings.api_password_access_enabled.subject")
        content = I18n.t(
          "txt.email.settings.api_password_access_enabled.content_v2",
          changed_by: formatted_actor_details(actor, account: setting.account)
        )

        account_settings_change_notice(
          setting,
          context: :"account-settings-changed",
          subject: subject,
          content: content
        )
      end
    end

    def api_password_access_disabled(setting, actor)
      I18n.with_locale(setting.account.translation_locale) do
        subject = I18n.t("txt.email.settings.api_password_access_disabled.subject")
        content = I18n.t("txt.email.settings.api_password_access_disabled.content_v2", changed_by: formatted_actor_details(actor, account: setting.account))

        account_settings_change_notice(
          setting,
          context: :"account-settings-changed",
          subject: subject,
          content: content
        )
      end
    end

    def assumption_expiration(setting, action, expiration_date = nil)
      translation_locale = setting.account.translation_locale

      I18n.with_locale(translation_locale) do
        subject = I18n.t("txt.email.settings.assumption_expiration_#{action}.subject_v1")
        zendesk_cldr_dates = Zendesk::Cldr::DateTime.new(translation_locale)
        expires_on = I18n.localize(
          expiration_date.to_datetime,
          format: zendesk_cldr_dates.single('datetime.standard.full')
        ) unless expiration_date.nil?
        content = assumption_expiration_content(action, expires_on)

        account_settings_change_notice(
          setting,
          context: :"account-settings-changed",
          subject: subject,
          content: content
        )
      end
    end

    private

    def account_settings_change_notice(setting, options = {})
      account = setting.account

      set_headers(account, Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT => "#{options.fetch(:context)}-#{setting.id}")

      mail(
        from: from_address(account),
        bcc: security_notifications_recipient_emails(account),
        subject: options.fetch(:subject)
      ) { |format| render_email_parts account, options.fetch(:subject), options.fetch(:content), format }
    end

    def assumption_expiration_content(action, expires_on = nil)
      case action
      when :disabled
        I18n.t(
          'txt.email.settings.assumption_expiration_disabled.content_v1',
          support_article: I18n.t('txt.email.settings.assumption_expiration.support_article')
        )
      when :permanent
        I18n.t(
          'txt.email.settings.assumption_expiration_permanent.content_v1',
          support_article: I18n.t('txt.email.settings.assumption_expiration.support_article')
        )
      else
        I18n.t(
          "txt.email.settings.assumption_expiration_#{action}.content_v1",
          expiration_date: expires_on,
          support_article: I18n.t('txt.email.settings.assumption_expiration.support_article')
        )
      end
    end
  end
end
