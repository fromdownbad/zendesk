module SecurityNotifications
  module CertificateStatusChanged
    def certificate_approval(certificate)
      host_name = certificate.account.host_name

      render_notification(certificate, context: :"certificate-approved") do
        subject = I18n.t("txt.email.certificate_approval.subject", domain: host_name)

        body = if certificate.sni_enabled?
          I18n.t(
            "txt.email.certificate_approval.sni_content",
            domain:            host_name,
            expire_date:       certificate.valid_until,
            host_mapping:      certificate.account.host_mapping
          )
        else
          I18n.t(
            "txt.email.certificate_approval.content_v2",
            domain:            host_name,
            expire_date:       certificate.valid_until,
            host_mapping:      certificate.account.host_mapping,
            cname_for_address: "#{certificate.account.subdomain}.ssl.#{Zendesk::Configuration.fetch(:host)}"
          )
        end

        { subject: subject, body: body }
      end
    end

    def lets_encrypt_enabled(certificate)
      render_notification(certificate, context: :"certificate-approved") do
        body = I18n.t(
          "txt.email.lets_encrypt_enabled.content_v2",
          help_center_url: I18n.t("txt.admin.controllers.settings.security_controller.ssl.lets_encrypt_learn_more_url")
        )

        { subject: I18n.t("txt.email.lets_encrypt_enabled.subject"), body: body }
      end
    end

    def certificate_expiration_warning(certificate)
      render_notification(certificate, context: :"certificate-expiring") do
        days_remaining_string = day_string(days_until_expiry(certificate))

        subject = I18n.t("txt.email.certificate_expiring.subject_v2",
          x_days: days_remaining_string)

        body = I18n.t(
          "txt.email.certificate_expiring.content_v5",
          expire_date:      certificate.valid_until,
          domains:          list_of_domains(certificate),
          replacement_date: (certificate.valid_until - Certificate::REPLACE_DAYS),
          help_center_url: I18n.t('txt.email.certificate_upload_help_center_url')
        )

        { subject: subject, body: body }
      end

      set_notification_sent_at(certificate)
      certificate.save!
    end

    def certificate_replacement_warning(certificate)
      render_notification(certificate, context: :"certificate-replacement") do
        subject = I18n.t("txt.email.certificate_replacement.subject")

        body = I18n.t(
          "txt.email.certificate_expiring.content_v5",
          expire_date:      certificate.valid_until,
          domains:          list_of_domains(certificate),
          replacement_date: (certificate.valid_until - Certificate::REPLACE_DAYS),
          help_center_url: I18n.t('txt.email.certificate_upload_help_center_url')
        )

        { subject: subject, body: body }
      end

      set_notification_sent_at(certificate)
      certificate.save!
    end

    private

    def set_notification_sent_at(certificate) # rubocop:disable Naming/AccessorMethodName
      certificate.notification_sent_at = Time.now.to_date
    end

    def render_notification(certificate, options = {})
      account      = certificate.account
      to_addresses = account.owner.email

      ActiveRecord::Base.on_shard(account.shard_id) do
        set_headers(certificate.account, Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT => "#{options.fetch(:context)}-#{certificate.id}")

        I18n.with_locale(account.translation_locale) do
          content = yield
          mail(from: from_address(account), bcc: to_addresses, subject: content.fetch(:subject)) do |format|
            render_email_parts(account, content.fetch(:subject), content.fetch(:body), format)
          end
        end
      end
    end

    def day_string(days)
      I18n.t('datetime.distance_in_words.x_days', count: days)
    end

    # Returns the number of days until the certificate expires
    #
    # certificate - the Certificate model
    #
    # Example:
    #
    #   days_until_expiry(<Certificate valid_until: '2015-03-30' ..>)
    #   # => 2
    #
    def days_until_expiry(certificate)
      (certificate.valid_until.to_date - Time.now.to_date).to_i
    end

    # Returns a string consisting of domains for the account
    #
    # Example:
    #
    #   list_of_domains(certificate)
    #   # => Domain: help.mycompany.com
    #
    #   list_of_domains(certificate)
    #   # => Domains:
    #        - help.mycompany.com
    #        - support.othercompany.com
    #
    def list_of_domains(certificate)
      host_mappings = certificate.brand_host_mappings

      if host_mappings.count > 1
        [
          I18n.t('txt.email.certificate_expiring.x_domains'),
          host_mappings.join("\n")
        ].join("\n")
      else
        [
          I18n.t('txt.email.certificate_expiring.one_domain'),
          host_mappings.first
        ].join(' ')
      end
    end
  end
end
