module SecurityNotifications
  module AccountAssumption
    def assumption_bypass(account)
      set_headers(account, Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT => "account-assumption-bypass-#{account.id}")

      I18n.with_locale(account.translation_locale) do
        content = I18n.t("txt.email.account_assumption.assumption_bypass.content")
        subject = I18n.t("txt.email.account_assumption.assumption_bypass.subject")
        mail(
          from: from_address(account),
          bcc: security_notifications_recipient_emails(account),
          subject: subject
        ) { |format| render_email_parts account, subject, content, format }
      end
    end
  end
end
