module SecurityNotifications
  module PasswordStrategy
    def maximum_login_attempts_exceeded(user)
      subject = I18n.t("txt.email.maximum_login_attempts_exceeded.subject",
        agent_name: user.name)
      content = I18n.t("txt.email.maximum_login_attempts_exceeded.content_v2",
        agent_name: user.name,
        agent_email: user.email,
        agent_profile_url: "#{user.account.url(mapped: false)}/users/#{user.id}")

      options = (user == user.account.owner) ? {} : { exclude: user }
      password_strategy_notice(user,
        context: :"user-account-throttled",
        recipients: security_notifications_recipient_emails(user.account, options),
        subject: subject,
        content: content)
    end

    private

    def password_strategy_notice(user, options = {})
      account = user.account

      set_headers(user.account, Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT => "#{options.fetch(:context)}-#{user.id}")

      mail(
        from: from_address(account),
        to: user.email,
        bcc: Array(options[:recipients]) - [user.email],
        subject: options.fetch(:subject)
      ) { |format| render_email_parts account, options.fetch(:subject), options.fetch(:content), format }
    end
  end
end
