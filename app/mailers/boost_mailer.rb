class BoostMailer < ApplicationMailer
  include Zendesk::Mailer::ReadOnly

  def boost_expiry(account, days)
    ActiveRecord::Base.on_shard(account.shard_id) do
      set_headers(account, X_DELIVERY_CONTEXT => "boost-expiry-#{account.id}-#{days}")

      owner          = account.owner
      email_locale   = recipient_locale(owner, account)
      boost_name     = account.feature_boost.boost_plan_name
      expiry_subject = (days == 1 ? I18n.t('txt.email.boost_mailer.boost_expiry.email_subject_1', locale: email_locale).to_s : I18n.t('txt.email.boost_mailer.boost_expiry.email_subject_2', days: days, locale: email_locale).to_s)
      expiry_body    = (days == 1 ? I18n.t('txt.email.boost_mailer.boost_expiry.email_body_2', plan_name: boost_name, locale: email_locale) : I18n.t('txt.email.boost_mailer.boost_expiry.email_body_3', plan_name: boost_name, days: days, locale: email_locale))

      content = "<p>#{expiry_body}</p>"
      content << "<p>#{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_4', locale: email_locale)}</p>"
      content << if read_only { account.has_email_agent_non_hostmapped_urls? }
        "<p>#{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_5', url: "<a href=\"#{account.url(mapped: false, ssl: true)}\">#{account.url(mapped: false, ssl: true)}</a>", locale: email_locale)}</p>"
      else
        "<p>#{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_5', url: "<a href=\"#{account.url}\">#{account.url}</a>", locale: email_locale)}</p>"
      end
      content << "#{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_8', locale: email_locale)}</p>"
      content << keep_features_going_link(account, email_locale)

      mail(
        subject: expiry_subject,
        to: account.owner.email,
        from: Zendesk::Mailer::ZendeskAddress.new("Jennifer Hansen").from
      ) do |format|
        format.text { content.strip_tags }
        format.html { zendesk_templated_body(I18n.t('txt.email.boost_mailer.boost_expiry.email_body_10', locale: email_locale), content, email_locale) }
      end
    end
  end

  private

  def keep_features_going_link(account, email_locale)
    if read_only { account.has_email_agent_non_hostmapped_urls? }
      <<-HTML
        <table cellpadding="3" cellspacing="0" border="0" style="background-color: #84A216;">
          <tr>
            <td>
              <a href="#{account.url}/agent/admin/subscription" style="color: #ffffff;">#{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_9', locale: email_locale)}</a>
            </td>
          </tr>
        </table>
      HTML
    else
      <<-HTML
        <table cellpadding="3" cellspacing="0" border="0" style="background-color: #84A216;">
          <tr>
            <td>
              <a href="#{account.url(mapped: false, ssl: true)}/agent/admin/subscription" style="color: #ffffff;">#{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_9', locale: email_locale)}</a>
            </td>
          </tr>
        </table>
      HTML
    end
  end
end
