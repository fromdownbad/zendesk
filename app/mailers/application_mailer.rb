require "mail"
require "action_mailer/enqueable"
require "action_mailer/enqueable/deferred"
require "zendesk/outgoing_mail/mailer"
require "zendesk/outgoing_mail/variables"

class ApplicationMailer < Zendesk::OutgoingMail::Mailer
  include AccountsHelper
  extend  ActionMailer::Enqueable
  include Zendesk::Mailer::ReadOnly
  include Zendesk::Mailer::DeprecatedMailerAPI
  include Zendesk::Mailer::GmailSending

  # NOTE: Outbound limit was chosen to match the old inbound attachment size
  # limit for Team plans. Currently all plans have an inbound & outbound
  # attachment size limit of 50MB.
  #
  # Gmail allows up to 50MB for inbound attachments and up to 25MB for outbound
  # attachments. Linked attachments let us get around Gmail's 25MB outbound
  # attachment limit.
  MAX_ATTACHMENTS_SIZE_OUTBOUND = 7.megabytes
  MAX_ATTACHMENTS_TOTAL_SIZE_OUTBOUND = 10.megabytes

  self.queue = MailRenderingJob::Deferred

  attr_reader :account

  helper_method :zendesk_templated_body

  def generic(account, sender, recipient, title, body)
    set_headers(account, X_DELIVERY_CONTEXT => "generic")

    mail(
      from: sender,
      to: recipient,
      subject: title
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: title,
          content: body.strip_tags,
          footer: footer(account, true),
          account: account
        )
      end

      format.html do
        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: title,
          content: body.html_safe,
          footer: footer(account),
          account: account
        )
      end
    end
  end

  def use_email_as_sender?(from)
    account.recipient_addresses && account.recipient_addresses.find_by_email(from).try(:spf_valid?)
  end

  def use_email_outbound_blocker?
    return false if account.nil?

    if account.multiproduct?
      !account.is_serviceable?
    else
      account.is_trial? && read_only { account.has_email_outbound_blocker? }
    end
  end

  def invoke_outbound_account_circuit_breaker?
    read_only { account.has_email_outbound_account_circuit_breaker? }
  end

  def allow_logged_with_email_id_smtp_delivery_method?
    read_only { account.has_email_allow_logged_with_email_id_smtp_delivery_method? }
  end

  def fallback_to_logged_smtp_delivery_method?
    read_only { account.has_email_fallback_to_logged_smtp_delivery_method? }
  end

  def self.encode_address_name(string, account = nil)
    if account&.has_email_encode_address_name_fix?
      name = string.gsub(/(\\+)?"|\n|\r\n?/, '').strip

      # Remove trailing full stops, as some email clients choke on this.
      name.sub!(/(\s*\.+)*$/, '') if name.last == '.'
    else
      name = string.gsub(/(\\+)?"|\n|\r\n?/, '')

      # Remove trailing full stops, as some email clients choke on this.
      name.sub!(/\.+$/, '') if name.last == '.'
    end

    # Takes a name like 'Agent (",)\\' and returns '"Agent (\",)\\\\"'
    name = (name.gsub(/([\\"])/) { |c| "\\#{c}" }).to_s

    "\"#{Mail::Encodings.address_encode(name)}\""
  end

  protected

  def render_email_parts(account, title, content, format, brand = nil, locale = nil)
    unless HashParam.ish?(content)
      content = Hash[text: content, html: content]
    end

    content[:html] = add_direction(account, locale || I18n.translation_locale, :content, content[:html].format_and_link)

    text_template = if account.multiproduct? && account.subscription.nil?
      Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE
    else
      account.text_mail_template
    end

    html_template = if account.multiproduct? && account.subscription.nil?
      Zendesk::OutgoingMail::Variables::ACCESSIBLE_HTML_MAIL_TEMPLATE
    else
      account.html_mail_template
    end

    text = Zendesk::Liquid::MailContext.render(
      template: text_template,
      header: title,
      content: content[:text],
      footer: footer(account, true, nil, locale, brand),
      account: account
    )

    html = Zendesk::Liquid::MailContext.render(
      template: html_template,
      header: title,
      content: content[:html],
      footer: footer(account, false, nil, locale, brand),
      account: account
    )

    format.text { text }
    format.html { html }
  end

  # All destination addresses. Utilized by encoders
  def destinations
    [recipients, cc, bcc].flatten.compact
  end

  def noreply_address(account)
    account_address = Zendesk::Mailer::AccountAddress.new(account: account)
    account_address.noreply
  end

  def from_address(account, options = {})
    account_address = Zendesk::Mailer::AccountAddress.new(options.merge(account: account))
    account_address.from
  end

  def footer(account, with_delimiter = false, user = nil, locale = nil, brand = nil)
    # Use the users locale if possible.
    # TODO: Refactor to use I18n.translation_locale, by setting the locale at the individual
    # mailer level using I18n.with_locale. See: https://zendesk.atlassian.net/browse/LOCAL-1721
    locale = user.present? ? recipient_locale(user, account) : recipient_locale(account) unless locale
    name = (brand || account).name

    text = I18n.t('txt.email.footer', account_name: name, locale: locale)
    with_delimiter ? "--------------------------------\n#{text}" : text
  end

  def zendesk_templated_body(title, content, locale)
    # NOTE: The template still renders a parent div for the {{delimiter}}.
    Zendesk::Liquid::MailContext.render(
      template: Zendesk::OutgoingMail::Variables::SUPPORT_HTML_MAIL_TEMPLATE_V2,
      header: title,
      content: content.auto_link,
      translation_locale: locale,
      footer: copyright_footer(locale),
      account: account,
      delimiter: '' # ..but there is no delimiter set
    ).html_safe
  end

  def copyright_footer(locale)
    I18n.t('txt.email.mailers.application_mailer.copyright', locale: locale)
  end

  def recipient_locale(item, account = nil)
    if locale = choose_best_locale(item, account)
      Rails.logger.info("Chose locale #{locale.id} for #{item.class}")
      locale
    else
      Rails.logger.info("Unable to determine locale for item: #{item.inspect}. Falling back to english")
      ENGLISH_BY_ZENDESK
    end
  end

  def choose_best_locale(item, account)
    if item.respond_to?(:translation_locale)
      item.translation_locale
    elsif item.is_a?(UserIdentity)
      item.try(:user).translation_locale
    elsif account.present?
      locale_for_account(item, account)
    end
  end

  def locale_for_account(item, account)
    locale = if item.is_a?(String) && user = user_from_recipient(item, account)
      user.translation_locale
    elsif item.is_a?(Array)
      best_locale_for_group(item, account)
    end
    locale || account.translation_locale
  end

  def user_from_recipient(recipient, account)
    email =
      if recipient.match?(EMAIL_PATTERN)
        recipient
      elsif /<(.*)>/.match?(recipient)
        recipient.match(/<(.*)>/)[1]
      end
    account.find_user_by_email(email) if email
  end

  def best_locale_for_group(recipients, account)
    locales = locales_sorted_by_usage(recipients, account)
    winner = if locales.second == account.translation_locale
      locales.second
    else
      locales.first
    end
    Rails.logger.info("Account ##{account.id} group notification locale is ##{winner.id}")
    winner
  end

  def user_locale(user, account)
    return I18n.translation_locale if user.blank?
    return user.translation_locale if user.is_a? User
    user = user_from_recipient(user, account)
    user.present? ? user.translation_locale : I18n.translation_locale
  end

  def locales_sorted_by_usage(recipients, account)
    recipients = recipients.first(recipients.size / 2) if recipients.size > 3 # performance hack
    recipients.
      group_by { |recipient| user_locale(recipient, account) }.
      sort_by { |_locale, group| group.size }.
      map(&:first).
      reverse
  end

  def locale_is_rtl?(user)
    user.present? && read_only { user.account.has_rtl_lang_html_attr? } && user.translation_locale.rtl?
  end

  def add_direction(account, locale, type, text)
    if read_only { account.has_rtl_lang_html_attr? }
      if locale.try(:rtl?)
        direction = "right"
        rtl = "direction:rtl;"
      else
        direction = "left"
        rtl = ""
      end

      case type
      when :content
        "<div style='text-align:#{direction};#{rtl}'>#{text}</div>"
      when :footer
        "<div style='display: inline-block; float:#{direction};#{rtl}'>#{text}</div>"
      end
    else
      text
    end
  end

  def attach_outbound_attachments
    return [] unless @comment
    return [] unless account.send_real_attachments?
    return [] unless @comment.is_public? || for_agent

    @attachments_size = 0

    select_ordered_attachments.each do |attachment|
      if can_add_attachment?(attachment)
        add_attachment_to_email(attachment)
        @attachments_size += attachment.size
      end
    end
  end

  def can_add_attachment?(attachment)
    attachment.size <= MAX_ATTACHMENTS_SIZE_OUTBOUND && @attachments_size + attachment.size < MAX_ATTACHMENTS_TOTAL_SIZE_OUTBOUND
  end

  # Converts inline image urls to inline attachments if available
  def convert_html_inline_attachments(text)
    @outbound_inline_attachments ||= {}
    @outbound_inline_attachments.each do |remote_url, attachment|
      img_src_url_regexp = if account.has_no_mail_delimiter_enabled?
        /(?:http.*\/api\/v2\/mail_resources\/.*\/images\/#{Regexp.escape(attachment[:filename])}|#{Regexp.escape(remote_url)})/
      else
        Regexp.escape(remote_url)
      end
      text.gsub!(Regexp.new("(<img [^>]*src=[\"'])#{img_src_url_regexp}", Regexp::IGNORECASE), '\1' + attachment[:cid_url])
    end
    text
  end

  def add_attachment_to_email(attachment)
    attachment_data = statsd.time('attachment_download.time') do
      attachment.current_data.to_s
    end

    statsd.count('attachment_download.bytes', attachment_data.bytesize)
    statsd.increment('attachment_download.attachments')

    if attachment.inline?
      begin
        attachments.inline[attachment.display_filename] = {
            mime_type: attachment.content_type,
            encoding: 'base64',
            content: Base64.encode64(attachment_data)
        }

        if (inline_attachment = attachments.inline[attachment.display_filename])
          cid_url = if read_only { account.has_email_handle_inline_attachments_with_same_filename? }
            attachments.inline.last.filename == attachment.display_filename ? attachments.inline.last.url : inline_attachment.url
          else
            inline_attachment.url
          end

          @outbound_inline_attachments ||= {}
          @outbound_inline_attachments[attachment.url] = { cid_url: cid_url, filename: attachment.filename }
          statsd.increment('attachment_download.attachments.inline')
        else
          Rails.logger.warn(
              "Inline attachment was not properly generated by ActionMailer for ticket_id: " \
          "#{ticket.id}, account_id: #{account.id}"
            )
          statsd.increment('attachment_download.attachments.inline.missing')
        end
      rescue NameError
        statsd.increment('attachment_errors', tags: ["attachment:#{attachment.display_filename}"])
        Rails.logger.info("unable to find methods for #{attachment.display_filename} on #{attachments}")
      end
    else
      attachments[attachment.display_filename] = {
        mime_type: attachment.content_type,
        encoding: 'base64',
        content: Base64.encode64(attachment_data)
      }

      statsd.increment('attachment_download.attachments.appended')
    end
  end

  def select_ordered_attachments
    if reject_appended_attachments?
      @comment.attachments.select(&:inline).sort_by(&:size)
    else
      @comment.attachments.sort_by(&:size)
    end
  end

  def reject_appended_attachments?
    return true if @comment.attachments.reject(&:inline).max_by(&:size).try(:size).to_i > MAX_ATTACHMENTS_SIZE_OUTBOUND
    return true if @comment.attachments.sum(:size) > MAX_ATTACHMENTS_TOTAL_SIZE_OUTBOUND

    false
  end

  def statsd
    @statsd ||= Rails.application.config.statsd.client
  end
end
