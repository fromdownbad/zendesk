class OnboardingMailer < ApplicationMailer
  def web_widget_setup_info(user, domain, emails, copy_self)
    @subject = I18n.t('txt.email.onboarding_mailer.web_widget.setup_info.subject', locale: user.account.translation_locale)
    send_snippet_setup(user, domain, emails, copy_self)
  end

  def connect_setup_info(user, domain, emails, copy_self)
    @activation_snippet = user.account.connect_activation_snippet
    @subject = I18n.t('txt.email.onboarding_mailer.connect.setup_info.subject', locale: user.account.translation_locale)
    send_snippet_setup(user, domain, emails, copy_self)
  end

  private

  def send_snippet_setup(user, domain, emails, copy_self)
    @account = user.account
    @user = user
    @snippet = @account.brand_code_snippet(@account.default_brand, domain)

    set_headers @account
    mail(
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      to: emails.split(',').map(&:strip),
      cc: copy_self ? @user.email : '',
      subject: @subject
    )
  end

  helper_method :footer
  def footer(account)
    I18n.t('txt.email.footer', account_name: account.name, locale: account.translation_locale)
  end
end
