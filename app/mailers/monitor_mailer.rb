class MonitorMailer < ApplicationMailer
  OCEAN_RECIPIENTS = ["ocean@zendesk.com"].freeze
  OCEAN_FROM = "ocean@support.zendesk.com".freeze

  def uptime_notification(options)
    account = Account.find_by_id(Account.system_account_id)
    set_headers(account, X_DELIVERY_CONTEXT => "uptime-notification")

    pod = if options['shard_id'].blank?
      ''
    else
      "[Pod#{Zendesk::Accounts::DataCenter.pod_id_for_shard(options['shard_id'])}]"
    end

    mail(
      subject: "[Uptime][#{Rails.env}]#{pod} No #{options['facility']} conversions in the past 30 minutes",
      to: OCEAN_RECIPIENTS,
      from: OCEAN_FROM,
      content_type: "text/html",
      body: options['message'].to_s
    )
  end

  def user_view_query_test_report(account, csv_string)
    pod = "Pod#{Zendesk::Accounts::DataCenter.pod_id_for_shard(account.shard_id)}"
    set_headers(account, X_DELIVERY_CONTEXT => "user-view-query-test-report")

    attachments["User View Query Test Report #{pod} #{Time.now}.csv"] = csv_string
    mail(
      subject: "[Report][#{Rails.env}][#{pod}] User view query tester results",
      to: OCEAN_RECIPIENTS,
      from: OCEAN_FROM,
      body: ""
    )
  end

  def rate_limit_notification(account, options)
    set_headers(account, X_DELIVERY_CONTEXT => "conditional-rate-limits-removal-notification")

    mail(
      subject: "conditional rate limit <#{options['prop_key']}> has been removed",
      to: options['owner_email'],
      from: Zendesk::Mailer::ZendeskAddress.new.from,
      content_type: "text/html",
      body: "This conditional rate limit has been removed.\n" + "key: #{options['prop_key']}\nsubdomain: #{options['subdomain']}\nexpired: #{options['expires_on']}"
    )
  end
end
