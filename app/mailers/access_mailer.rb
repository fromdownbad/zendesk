class AccessMailer < ApplicationMailer
  def token(user, token)
    I18n.with_locale(user.translation_locale) do
      set_headers(user.account, X_DELIVERY_CONTEXT => "access-token-#{user.id}")
      account     = user.account
      auth_domain = account.authentication_domain

      one_time_link = "#{auth_domain}/access/access_token/#{token}/"

      subject = I18n.t('txt.mailers.access_mailers.token.subject_v2', account_name: account.name)
      content = "#{I18n.t('txt.mailers.access_mailers.token.content_2', one_time_link: one_time_link)}\n\n"

      options = {
        from: from_address(account, brand: account.default_brand),
        to: user.email,
        subject: subject
      }

      mail(options) { |format| render_email_parts(account, subject, content, format) }
    end
  end
end
