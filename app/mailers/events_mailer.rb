require 'cld'

class EventsMailer < ApplicationMailer
  include ImagePathHelper
  include Zendesk::Mailer::ReadOnly
  include Zendesk::Mailer::SimplifiedEmailThreading

  MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth
  TITLE_TRUNCATION_LENGTH = 250
  MAX_REFERENCES = 10
  NOTIFICATION_EVENT_TYPES = %w[
    Cc
    Notification
    NotificationWithCcs
    OrganizationActivity
  ].freeze

  def self.add_direction_tag(rendered_html_part, account)
    parsed_body = Nokogiri::HTML5.parse(rendered_html_part, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)
    dir = detect_text_directionality(parsed_body.text, account)
    Rails.logger.info("EventsMailer.add_direction_tag: Detected '#{dir}' directionality for text: '#{parsed_body.text.truncate(200)}' (Sample size: #{parsed_body.text.size})")

    parsed_body.css('p').attr('dir', dir)
    parsed_body.to_xhtml
  rescue StandardError => error
    Rails.logger.warn("EventsMailer.add_direction_tag #{error.class}: #{error.message}")
    rendered_html_part
  end

  def self.detect_text_directionality(text, account)
    detected_language = CLD.detect_language(text)
    Rails.logger.info("EventsMailer.detect_text_directionality: Detected language: #{detected_language}")

    # Fallback to `dir=auto` if it can't detect the language with confidence.
    return :auto unless detected_language || detected_language[:reliable] != 'true'

    # Limit the scope to languages that are available for the Account.
    comment_language = account.available_languages.detect do |available_language|
      available_language.locale_code == detected_language[:code]
    end

    # Fallback to `dir=auto` if the language is not enabled
    unless comment_language
      Rails.logger.info("EventsMailer.detect_text_directionality: Detected language not available for account")
      return :auto
    end

    comment_language.right_to_left? ? :rtl : :ltr
  rescue StandardError => error
    Rails.logger.warn("EventsMailer.detect_text_directionality #{error.class}: #{error.message}")
    :auto
  end

  # Evaluate the liquid in the notification body in context of the given ticket.
  # Send it to the `to` array of recipients email addresses.
  def rule(notification, ticket, to, for_agent = false, ccs = [])
    initialize_state(ticket, notification, to, for_agent)

    subject = if suppress_subject_title?
      rule_notification_title(notification.subject.gsub(/{{\s*ticket.title\s*}}/, ''))
    else
      rule_notification_title(notification.subject)
    end

    body = notification.body.to_s

    single_comments_for_machines!(to, body)

    @comment = notification.audit.comment || ticket.comments.last

    @placeholder_suppression = should_suppress? && account.has_email_placeholder_suppress_with_liquid?

    @email_ccs_added = email_ccs_added?(notification)

    if account.has_email_redact_attachments_when_public_comment_placeholders_not_exists?
      @outbound_attachments = attach_outbound_attachments unless @placeholder_suppression || redact_attachments?(body)
    else
      @outbound_attachments = attach_outbound_attachments unless should_suppress?
    end

    construct_event_email(
      notification.id,
      subject,
      body,
      ccs: ccs.map(&method(:user_to_email))
    )
  end

  def follower_notification(ticket, to, for_agent, cc, cc_id)
    initialize_state(ticket, cc, to, for_agent)

    subject, body = if read_only { ticket.account.has_follower_and_email_cc_collaborations_enabled? }
      [ticket.account.follower_subject_template, ticket.account.follower_email_template.to_s]
    else
      [ticket.account.cc_subject_template, ticket.account.cc_email_template.to_s]
    end

    single_comments_for_machines!(to, body)

    @comment = begin
      cc.audit.comment
    rescue NoMethodError => e
      Rails.logger.warn("nil_audit_for_event cc: #{cc.inspect}, ticket_id: #{ticket.id}, account: #{@account.id}")
      raise e
    end

    @outbound_attachments = begin
      if account.has_email_redact_attachments_when_public_comment_placeholders_not_exists? && redact_attachments?(body)
        []
      else
        attach_outbound_attachments
      end
    end

    @placeholder_suppression = should_suppress? && account.has_email_placeholder_suppress_with_liquid?
    @email_ccs_added = email_ccs_added?(cc)

    construct_event_email(cc_id, follower_notification_title(subject), body, recipients_as_ccs: true)
  end

  def organization_activity_notification(ticket, to, comment, organization_activity_id)
    initialize_state(ticket, comment, to, false)

    subject = organization_activity_notification_title
    body = ticket.account.organization_activity_email_template.to_s

    @outbound_attachments = attach_outbound_attachments

    construct_event_email(organization_activity_id, subject, body)
  end

  protected

  attr_reader :account,
    :author,
    :brand,
    :current_user,
    :event,
    :for_agent,
    :host,
    :locale,
    :outbound_attachments,
    :placeholder_suppression,
    :recipients,
    :ticket,
    :to

  def initialize_state(ticket, event, to, for_agent)
    @current_user = if to.is_a?(User)
      to
    elsif to.is_a?(Array) && to.size == 1 && to.first.is_a?(User)
      to.first
    end

    @ticket = ticket
    @account = ticket.account
    @brand = ticket.brand
    @event = event
    @author = event.author
    @for_agent = for_agent
    @host = url_builder.base_url(ticket.brand)
    @recipients = Array.wrap(to)
    @to = @recipients.map(&method(:user_to_email))
    @locale = recipient_locale(to, account)
    @outbound_attachments = []
    @placeholder_suppression = false

    if event.is_a?(Notification)
      event.message_id = ticket_message_id
    end
  end

  def single_comments_for_machines!(to, body)
    if Array(to).all? { |u| u.is_a?(User) && u.machine? }
      body.sub!(/\{\{\s*ticket\.comments_formatted\s*\}\}/, '{{ticket.latest_comment_formatted}}')
    end
  end

  def suppress_subject_title?
    return false unless account.has_email_placeholder_suppress_subject? && should_suppress?

    Rails.logger.info("Subject updated to remove {{ticket.title}} for ticket nice_id: #{ticket.nice_id}, account id: #{account.id}")
    statsd.increment('events_mailer.placeholder_suppressed_subject')
    true
  end

  def should_suppress?
    @should_suppress ||= begin
      author_is_end_user? &&
        recipients.any? { |u| u.is_a?(User) && u.is_end_user? } &&
        event.audit.comment&.first? && ticket.comments.size == 1 &&
        account.is_open? && !account.is_signup_required?
    end
  end

  def author_is_end_user?
    return event.author.is_end_user? unless account.has_email_placeholder_check_ticket_author?
    ticket.audits.first.author.is_end_user?
  end

  def single_recipient
    the_recipients = Array.wrap(recipients)

    return nil if the_recipients.many?
    return nil unless the_recipients.first.is_a?(User)

    the_recipients.first
  end

  def construct_event_email(event_id, subject, body, options = {})
    headers = construct_headers(event_id, subject, body, options)

    mail(headers) do |format|
      format.text { text_part(body) }
      format.html { html_part(body) }
    end
  end

  def construct_headers(event_id, subject, body, recipients_as_ccs: false, ccs: [])
    set_headers(account, event_headers(event_id, body))

    headers = {
      from: event_from_address,
      reply_to: event_reply_to_address,
      subject: subject
    }.merge(recipient_headers(recipients_as_ccs, ccs))

    if read_only { account.has_bcc_archiving_enabled? }
      headers[:bcc] = account.settings.bcc_archive_address
    end

    headers
  end

  def recipient_headers(recipients_as_ccs, ccs)
    recipients_as_ccs ? {cc: to} : {to: to, cc: ccs}
  end

  def ticket_message_id
    @ticket_message_id ||= ticket.generate_message_id
  end

  def ticket_reply_to_id
    @ticket_reply_to_id ||= ticket.generate_message_id(false)
  end

  def event_headers(event_id, body)
    Rails.logger.info("Message ID: #{ticket_message_id}")

    headers = {
      X_DELIVERY_CONTEXT => "event-id-#{event_id}",
      "Message-Id" => ticket_message_id,
    }

    if for_agent
      headers["X-Priority"] = PriorityType.to_mail(ticket.priority_id)
    end

    inbound_emails_limit = MAX_REFERENCES - 1
    emails = ticket.inbound_emails.limit(inbound_emails_limit).order("created_at DESC")
    initial_email = emails.last

    if initial_email
      headers["References"] = build_references(emails)
    end

    default_in_reply_to = initial_email ? initial_email.message_id : ticket_reply_to_id
    headers["In-Reply-To"] = in_reply_tos(default_in_reply_to, body)

    statsd.increment 'outbound_mail.event_headers'
    statsd.increment 'outbound_mail.event_headers_for_satisfaction' if satisfaction_mail?(body)
    headers
  end

  def build_references(emails)
    message_ids = emails.map(&:message_id)
    headers = emails.flat_map { |e| e.references << e.in_reply_to }
    references = ([ticket_reply_to_id] + message_ids + headers).uniq.compact
    references = references[0...MAX_REFERENCES]
    references[1..-1] = references[1..-1].reverse # structure is supposed to be [origin, oldest, newest]
    references
  end

  def event_from_address
    # If you're the author and single recipient, send from the default account address instead of yourself
    if to.size == 1 && to.first == author.email_address_with_name
      ticket_address.author = nil
    end

    ticket_address.from(for_agent: for_agent)
  end

  def event_reply_to_address
    ticket_address.reply_to(for_agent)
  end

  def ticket_address
    @ticket_address ||= Zendesk::Mailer::TicketAddress.new(account: account, ticket: ticket, author: author)
  end

  def ticket_url
    ticket.url(user: single_recipient, for_agent: @for_agent)
  end

  def assignee_status_changed?
    ticket.assignee.present? && ticket.events.where(type: "Change", parent_id: event.parent_id, value_reference: "assignee_id").exists?
  end

  def event_part_heading(type)
    params = {ticket_id: ticket.nice_id, locale: locale}
    if type == Mime[:html]
      params[:ticket_title] = CGI.escapeHTML(ticket.title_for_role(for_agent, TITLE_TRUNCATION_LENGTH))
      "<a href=\"#{ticket_url}\" class=\"zd_link\">#{I18n.t("txt.email.notification.message", params)}</a>".html_safe
    else
      params[:ticket_title] = ticket.title_for_role(for_agent, TITLE_TRUNCATION_LENGTH)
      I18n.t("txt.email.notification.message", params)
    end
  end

  def use_agent_footer?
    for_agent && account.settings.modern_email_template?
  end

  def event_part_footer_title
    <<-TEXT
      <!-- agent footer -->
      <p style="font-family: Helvetica,Arial,sans-serif; margin-bottom: 15px; font-size: 80%; color:gray;">
        #{I18n.t('txt.email.rule_notification.agent_footer.title', ticket_url: ticket.url(for_agent: true), locale: locale)}
      </p>
    TEXT
  end

  def event_part_footer(type)
    if type == Mime[:html]
      text = ""

      if use_agent_footer?
        if account.has_email_simplified_threading?
          text << ticket_properties_for_agent_v2
        else
          text << event_part_footer_title
          text << ticket_properties_for_agent
        end
      end

      text << footer(account, false, to, locale, brand)

      text
    else
      footer(account, true, to, locale, brand).strip_tags
    end
  end

  def text_part(event_body)
    Zendesk::Liquid::Context.time_and_record('events_mailer', ['part:text_part']) do
      text_part_render(event_body)
    end
  end

  def text_part_render(event_body)
    text_email_header = event_part_heading(Mime[:text])
    text_email_footer = event_part_footer(Mime[:text])
    text_email_body = event_body

    text_ticket_content = render_ticket_context(text_email_body, Mime[:text])

    text_email_body = if read_only { account.has_no_mail_delimiter_enabled? }
      text_ticket_content
    else
      "#{delimiter}\n\n#{text_ticket_content}"
    end

    text_complete_body = Zendesk::Liquid::MailContext.render(
      template: account.text_mail_template,
      header: text_email_header,
      content: text_email_body,
      footer: text_email_footer,
      account: account,
      translation_locale: locale_for_liquid,
      suppress_placeholder: placeholder_suppression
    )

    text_complete_body = inject_plain_text_message(text_complete_body)

    text_complete_body
  end

  def html_part(event_body)
    Zendesk::Liquid::Context.time_and_record('events_mailer', ['part:html_part']) do
      rendered_html_part = html_part_render(event_body)

      unless account.has_email_notification_body_with_dir_auto_tags?
        return rendered_html_part
      end

      statsd.time('add_direction_tag.time') do
        EventsMailer.add_direction_tag(rendered_html_part, account)
      end
    end
  end

  def html_part_render(event_body)
    header = event_part_heading(Mime[:html])
    footer = event_part_footer(Mime[:html])
    event_body = event_body.to_s

    body = render_ticket_context(event_body, Mime[:html])

    # It would be great to move this to Zendesk::Liquid::Context#autolink,
    # but no concept of `Account` there, hence no concept of `host`.
    ticket_path = url_builder.path_for(nil)

    if ticket_path
      body = body.auto_link_tickets(host: host, path: ticket_path)
    end

    # Render the full email body template.
    body = Zendesk::Liquid::MailContext.render(
      template: account.html_mail_template,
      header: header,
      content: body,
      footer: footer,
      account: account,
      delimiter: delimiter,
      translation_locale: locale_for_liquid,
      attributes: "lang='#{locale_for_liquid.locale.downcase}'",
      suppress_placeholder: placeholder_suppression
    )

    body = inject_message(body)

    if add_gmail_markup?(event_body)
      if read_only { account.has_email_fix_gmail_markup? }
        if (index = body.index(/<\/body>/i))
          body.insert(index, gmail_markup)
        else
          body << gmail_markup
        end
      else
        body << gmail_markup
      end
    end

    if account.send_real_attachments?
      body = convert_html_inline_attachments(body)
    end

    body
  end

  def render_ticket_context(template, type)
    template_body = template.dup
    options = {
      ticket_url: ticket_url,
      current_user: to,
      updater: author,
      include_tz: true,
      skip_attachments: outbound_attachments,
      show_last_three_comments: show_last_three_comments?(template_body),
      suppress_placeholder: placeholder_suppression
    }

    user = (current_user || author)
    Zendesk::Liquid::TicketContext.render(template_body, ticket, user, for_agent, locale_for_liquid, type.to_s, options)
  end

  def redact_attachments?(body)
    @redact_attachments ||= begin
      if account.has_email_remove_attachments_if_no_public_comment_placeholder?
        author_is_end_user? &&
          recipients.any? { |u| u.is_a?(User) && u.is_end_user? } &&
          !check_for_public_comment_placeholders(body)
      else
        author_is_end_user? &&
          recipients.any? { |u| u.is_a?(User) && u.is_end_user? } &&
          event.audit.comment&.first? && ticket.public_comments.count == 1 &&
          !check_for_public_comment_placeholders(body)
      end
    end
  end

  def check_for_public_comment_placeholders(body)
    template_body = body.dup
    user = (current_user || author)
    nesting_level = 0
    options = { detect_public_comment_placeholder: true }

    ticket_context = Zendesk::Liquid::TicketContext.new(template_body, ticket, user, for_agent, locale_for_liquid, Mime[:text].to_s, nesting_level, options)
    ticket_context.render
    ticket_context.public_comment_placeholder
  end

  def add_gmail_markup?(body)
    @ticket &&
      @ticket.url(for_agent: for_agent) &&
      account.settings.gmail_actions &&
      !satisfaction_mail?(body)
  end

  def gmail_markup
    <<~HTML.delete("\n")
      <div itemscope itemtype="http://schema.org/EmailMessage" style="display:none">
        <div itemprop="action" itemscope itemtype="http://schema.org/ViewAction">
          <link itemprop="url" href="#{ticket.url(for_agent: for_agent)}" />
          <meta itemprop="name" content="#{I18n.t('txt.events_mailer.gmail_markup.view_ticket.button_v2')}"/>
        </div>
      </div>
    HTML
  end

  def unverified_email_identity
    @unverified_email_identity ||= current_user.identities.email.unverified.first
  end

  def locale_for_liquid
    return @locale if @locale.present?

    if to.is_a?(String)
      user = account.find_user_by_email(email)
      user.translation_locale
    elsif to.is_a?(User)
      to.translation_locale
    elsif @account.present?
      @account.translation_locale
    else
      ENGLISH_BY_ZENDESK
    end
  end

  # Puts a notice on sandbox accounts, a hidden representation of the encoded id into an HTML part, a ticket nice ID and
  # account subdomain name if for an agent, and a Message-Id for uniqueness if in the `email_invisible_message_id` beta feature.
  def inject_message(text)
    message_slug = if account.is_sandbox?
      slug_text = I18n.t("txt.admin.views.access.index.this_is_a_sandbox_account_user_for_testing_only", locale: locale)
      "<div style='color:#000000; background-color:#FFFFFF; font-size:12px; margin-top:15px;'>#{slug_text}</div>"
    else
      ""
    end

    message_slug << hidden_span_tag("[#{ticket.encoded_id}]")
    if for_agent
      message_slug << hidden_span_tag("Ticket-Id:#{ticket.nice_id}")
      message_slug << hidden_span_tag("Account-Subdomain:#{account.subdomain}")
    end

    message_slug << hidden_span_tag("Message-Id:#{event.message_id}".gsub(/[<>@]/, '')) if read_only { account.has_email_invisible_message_id? } && event.message_id

    message_slug = message_slug.html_safe

    if (index = text.index(/<\/body>/i))
      text.insert(index, message_slug)
    else
      text << message_slug
    end

    text
  end

  def hidden_span_tag(content)
    if read_only { account.has_email_invisible_metadata_span_tags? }
      "<span style='color:#FFFFFF;visibility:hidden' aria-hidden='true'>#{content}</span>"
    else
      "<span style='color:#FFFFFF' aria-hidden='true'>#{content}</span>"
    end
  end

  # Puts a hidden representation of the encoded id into an plain text part
  def inject_plain_text_message(text)
    message = "\n\n\n\n\n\n\n\n\n\n[#{ticket.encoded_id}]"
    return text if text.end_with?(message)

    text + message
  end

  def rule_notification_title(title)
    params = {}
    params[:ticket_title] = render_title(title)
    params[:locale] = locale

    # Docomo strips out everything we add to try to find a ticket. That's why we are enforcing the id in the subject.
    if !delivering_to_docomo?
      params[:ticket_title]
    elsif for_agent
      I18n.t("txt.email.rule_notification.title.for_agent", params.merge(ticket_id: ticket.encoded_id))
    elsif title.index('#{{ticket.id}}').present? || !delivering_to_docomo?
      params[:ticket_title]
    else
      I18n.t("txt.email.rule_notification.title.for_end_user", params.merge(ticket_id: ticket.nice_id))
    end
  end

  def delivering_to_docomo?
    to.any? { |address| address =~ /@docomo\.ne\.jp/ }
  end

  def follower_notification_title(title = nil)
    options = { locale: locale }

    options[:ticket_title] = render_title(title)

    # Docomo strips out everything we add to try to find a ticket. That's why we are enforcing the id in the subject.
    if !delivering_to_docomo?
      options[:ticket_title]
    elsif for_agent
      I18n.t("txt.email.rule_notification.title.for_agent", options.merge(ticket_id: ticket.encoded_id))
    else
      I18n.t("txt.email.rule_notification.title.for_end_user", options.merge(ticket_id: ticket.nice_id))
    end
  end

  def organization_activity_notification_title
    params = {}

    params[:ticket_title] = ticket.title_for_role(for_agent, TITLE_TRUNCATION_LENGTH)
    params[:locale] = locale

    if use_personalized_address?(account, author)
      params[:account_name] = subject_account_name
      I18n.t("txt.email.organization_activity_notification.title.for_end_user", params)
    else
      I18n.t('txt.email.organization_activity_notification.title.with_no_account_for_end_user', params)
    end
  end

  def delimiter
    if account.mail_delimiter.include?("{{txt.email.delimiter}}")
      "##- #{I18n.t("txt.email.delimiter", locale: locale)} -##"
    else
      account.mail_delimiter
    end
  end

  def ticket_properties_for_agent
    <<-TEXT
      <table cellpadding="1" cellspacing="0" border="0" role="presentation">
        #{ticket_properties_row(I18n.t('txt.email.rule_notification.agent_footer.ticket_id', locale: locale), ticket.nice_id.to_s)}
        #{ticket_properties_row(I18n.t('txt.default.fields.status.title', locale: locale), I18n.t('txt.admin.helpers.ticket_helper.status_' + ticket.status.downcase, locale: locale))}
        #{ticket_properties_row(I18n.t('ticket_fields.requester.label', locale: locale), ticket.requester_name_for_display)}
        #{ticket_properties_row(I18n.t('txt.admin.models.rules.rule_dictionary.ccs_label', locale: locale), ticket_ccs_for_agent) if display_ticket_ccs_for_agent?}
        #{ticket_properties_row(I18n.t('txt.email.rule_notification.agent_footer.followers', locale: locale), ticket_followers_for_agent) if display_ticket_followers_for_agent?}
        #{ticket_properties_row(I18n.t('txt.default.fields.group.title', locale: locale), group_title, 'group_name')}
        #{ticket_properties_row(I18n.t('txt.default.fields.assignee.title', locale: locale), ticket.assignee.present? ? ticket.assignee.name : '-')}
        #{ticket_properties_row(I18n.t('txt.default.fields.priority.title', locale: locale), ticket.priority != '-' ? I18n.t('type.priority.' + ticket.priority.downcase, locale: locale) : '-')}
        #{ticket_properties_row(I18n.t('txt.default.fields.type.title', locale: locale), I18n.t('type.ticket.' + ticket.ticket_type.downcase, locale: locale))}
        #{ticket_properties_row(I18n.t('txt.email.rule_notification.agent_footer.channel', locale: locale), ticket_via_description)}
      </table>
      <p>&nbsp;</p>
    TEXT
  end

  def ticket_properties_for_agent_v2
    Zendesk::Liquid::Footer::FooterDrop.new({
      ticket_status: ticket.status.downcase,
      ticket_id: ticket.nice_id.to_s,
      ticket_url: ticket_url,
      requester_image_url: photo_thumb_path_for_email(ticket.requester),
      requester_name: ticket.requester_name_for_display,
      assignee_image_url: ticket.assignee.present? ? photo_thumb_path_for_email(ticket.assignee) : '',
      assignee_name: ticket.assignee.present? ? ticket.assignee.name : '',
      assignee_status_changed: assignee_status_changed?,
      display_ticket_ccs_for_agent: display_ticket_ccs_for_agent?,
      ticket_ccs_for_agent: ticket_ccs_for_agent,
      display_ticket_followers_for_agent: display_ticket_followers_for_agent?,
      ticket_followers_for_agent: ticket_followers_for_agent,
      group_title: group_title,
      ticket_type: I18n.t("type.ticket.#{ticket.ticket_type.downcase}", locale: locale),
      channel: ticket_via_description,
      priority: ticket.priority != '-' ? I18n.t("type.priority.#{ticket.priority.downcase}", locale: locale) : '-',
      locale: locale,
    }).render
  end

  def group_title
    return if account.subscription.plan_type <= ZBC::Zendesk::PlanType::Essential.plan_type

    ticket.group.present? ? ticket.group.name : '-'
  end

  def display_ticket_ccs_for_agent?
    read_only do
      !(account.has_follower_and_email_cc_collaborations_enabled? && !account.has_comment_email_ccs_allowed_enabled?)
    end
  end

  def display_ticket_followers_for_agent?
    read_only { account.has_ticket_followers_allowed_enabled? }
  end

  def ticket_ccs_for_agent
    if read_only { account.has_follower_and_email_cc_collaborations_enabled? }
      return "-" unless read_only { account.has_comment_email_ccs_allowed_enabled? }
      ticket.email_cc_names.blank? ? "-" : ticket.email_cc_names
    elsif account.is_collaboration_enabled?
      ticket.cc_names.blank? ? "-" : ticket.cc_names
    else
      "-"
    end
  end

  def ticket_followers_for_agent
    ticket.follower_names.blank? ? "-" : ticket.follower_names
  end

  def ticket_properties_row(key, value, class_name = '')
    return nil unless value.present?

    alignment = locale_is_rtl?(current_user) ? "left" : "right"

    str = "<td #{class_name.present? ? "class='#{class_name}'" : ''}style='vertical-align:top; font-size:12px; color:#333333; line-height:18px; text-align:#{alignment}; padding-#{alignment}:10px; font-family:\"Lucida Grande\",\"Lucida Sans Unicode\",\"Tahoma\",Verdana,sans-serif;' valign='top'>"

    str << '<strong>' + key + '</strong>'
    str << '</td><td style="vertical-align:top; font-size:12px; color:#777; line-height:18px; font-family:\'Lucida Grande\',\'Lucida Sans Unicode\',\'Tahoma\',Verdana,sans-serif;" valign="top">'
    str << value
    str << '</td>'
    '<tr>' + str + '</tr>'
  end

  # In ticket.via_description we show a blank value for tickets created from a web form.
  # That's because we only want to show a description in special cases like 'by Email' or 'by Web Service'.
  # Here in emails we do want to show that channel: 'Channel: Web Form'.
  def ticket_via_description
    if ticket.via?(:web_form)
      I18n.t("txt.via_types.web_form", locale: locale)
    else
      ticket.via_description.blank? ? '-' : ticket.via_description(locale)
    end
  end

  def subject_account_name
    name = ticket_address.account_name
    return "" if name.blank?
    "[#{name}]"
  end

  def url_builder
    @url_builder ||= Zendesk::Tickets::UrlBuilder.new(account: account, for_agent: for_agent)
  end

  def use_personalized_address?(account, author)
    address = Zendesk::Mailer::AccountAddress.new(account: account, author: author)
    address.personalize?
  end

  def render_title(title)
    rendered_title = Zendesk::Liquid::TicketContext.render(title, ticket, current_user || author, for_agent, locale_for_liquid, Mime[:text], updater: author, include_tz: true, skip_attachments: outbound_attachments)
    # Render any placeholders/DC in the ticket.title
    if /\{\{|\{\%/.match?(rendered_title)
      rendered_title = Zendesk::Liquid::TicketContext.render(rendered_title, ticket, current_user || author, for_agent, locale_for_liquid, Mime[:text], updater: author, include_tz: true, skip_attachments: outbound_attachments)
    end
    rendered_title.to_s.squish.truncate(TITLE_TRUNCATION_LENGTH)
  end

  # this fetches message IDs for the most recent outbound emails for the current recipients
  def in_reply_tos(default_in_reply_to, body)
    return [default_in_reply_to] if satisfaction_mail?(body)
    return @in_reply_tos if @in_reply_tos

    @in_reply_tos = [default_in_reply_to]

    current_recipient_user_ids = recipient_user_ids

    notifications = ticket.events.where(type: NOTIFICATION_EVENT_TYPES).order("created_at DESC")
    message_ids = OutboundEmail.for_ticket(ticket).each_with_object({}) { |email, h| h[email.notification_id] = email.message_id }

    return @in_reply_tos unless message_ids.any?

    notifications.each do |notification|
      if (notification.recipients & current_recipient_user_ids).any?
        id = message_ids[notification.id]

        unless [nil, ticket_message_id].include?(id)
          @in_reply_tos.push(id)
          current_recipient_user_ids -= notification.recipients
        end
      end

      break if current_recipient_user_ids.empty? || @in_reply_tos.size >= MAX_REFERENCES
    end

    @in_reply_tos
  end

  def recipient_user_ids
    @recipient_user_ids ||= Array.wrap(recipients).map do |recipient|
      if recipient.is_a?(User)
        recipient.id
      else
        account.user_email_identities.where(value: recipient).first.try(:user_id)
      end
    end.uniq.compact
  end

  def satisfaction_mail?(body)
    @contains_satisfaction_placeholder ||= body.include?("{{satisfaction.rating_section}}")
  end

  private

  def user_to_email(user)
    user.is_a?(User) ? user.email_address_with_name : user.to_s
  end
end
