class DeflectionMailer < ApplicationMailer
  include Zendesk::Mailer::ReadOnly

  MAX_REFERENCES = 10

  def answer_bot_email(ticket, event, rule_id, ccs = [])
    trigger = AnswerBot::Trigger.config(ticket.account, rule_id)
    subject_template = trigger[:subject_template]
    body_template = trigger[:body_template]

    initialize_state(ticket, event, subject_template, body_template, ccs)

    @comment = ticket.comments.first

    placeholder_suppression = should_suppress? && account.has_email_placeholder_body_deflection_suppression?
    if account.has_email_deflection_mailer_redact_attachments?
      @outbound_attachments = attach_outbound_attachments unless placeholder_suppression || redact_attachments?(body_template)
    else
      @outbound_attachments = attach_outbound_attachments unless placeholder_suppression
    end

    deferred = construct_answer_bot_email(account, ticket)

    if ticket.requester.machine?
      message.perform_deliveries = false
    else
      log_deflection_sent
    end

    deferred
  end

  protected

  attr_reader :account, :event, :ticket, :for_agent, :to, :ccs, :locale, :user, :subject_template, :body_template, :outbound_attachments, :host

  private

  def initialize_state(ticket, event, subject_template, body_template, ccs = [])
    @account = ticket.account
    @event = event
    @ticket = ticket
    @user = ticket.requester
    @ccs = ccs
    @for_agent = ticket.requester.is_agent?
    @host = url_builder.base_url(ticket.brand)
    @subject_template = if suppress_subject_title?
      subject_template.gsub(/{{\s*ticket\.title\s*}}/, '')
    else
      subject_template
    end
    @body_template = body_template
    @locale = recipient_locale(@user, @account)
    @outbound_attachments = []
  end

  def ticket_address
    @ticket_address ||= Zendesk::Mailer::TicketAddress.new(account: account, ticket: ticket)
  end

  def suppress_subject_title?
    return false unless account.has_email_placeholder_subject_deflection_suppression? && should_suppress?

    Rails.logger.info(
      "Subject updated to remove {{ticket.title}} for ticket " \
      "nice_id: #{ticket.nice_id}, account id: #{account.id}"
    )
    statsd.increment('deflection_mailer.placeholder_suppressed_subject')

    true
  end

  def should_suppress?
    @should_suppress ||= begin
      author_is_end_user? &&
        (user.is_a?(User) && user.is_end_user?) &&
        event.audit.comment&.first? && ticket.comments.size == 1 &&
        account.is_open? && !account.is_signup_required?
    end
  end

  def author_is_end_user?
    return event.author.is_end_user? unless account.has_email_placeholder_check_ticket_author?
    ticket.audits.first.author.is_end_user?
  end

  def template_renderer(template, format: Mime[:html].to_s)
    Zendesk::Liquid::TicketContext.render(
      template,
      ticket,
      ticket.requester,
      for_agent,
      locale,
      format,
      include_tz: true,
      skip_attachments: outbound_attachments,
      suppress_placeholder: should_suppress? && account.has_email_placeholder_body_deflection_suppression?
    )
  end

  def html_body
    ticket_path = url_builder.path_for(nil)

    body_content = template_renderer(body_template)

    if ticket_path
      body_content = body_content.auto_link_tickets(host: host, path: ticket_path)
    end

    body = Zendesk::Liquid::MailContext.render(
      template: account.html_mail_template,
      header: deflection_headers,
      content: body_content,
      footer: footer(account, false, nil, nil, @ticket.brand),
      account: account,
      delimiter: delimiter,
      translation_locale: locale,
      suppress_placeholder: should_suppress? && account.has_email_placeholder_body_deflection_suppression?
    )

    body = convert_html_inline_attachments(body) if account.send_real_attachments?
    body
  end

  def text_body
    Zendesk::Liquid::MailContext.render(
      template: account.text_mail_template,
      header: deflection_headers,
      content: template_renderer(body_template, format: Mime[:text].to_s),
      footer: footer(account, false, nil, nil, @ticket.brand),
      account: account,
      delimiter: delimiter,
      translation_locale: locale,
      suppress_placeholder: should_suppress? && account.has_email_placeholder_body_deflection_suppression?
    )
  end

  def construct_answer_bot_email(account, ticket)
    headers = construct_headers(account, ticket)

    mail(headers) do |format|
      format.text { text_body }
      format.html { html_body }
    end
  end

  def construct_headers(account, ticket)
    set_headers(account, base_mail_headers(ticket))

    deflection_headers
  end

  def deflection_headers
    {
      from: ticket_address.from(for_agent: false),
      reply_to: ticket_reply_to_address,
      to: user.email_address_with_name,
      cc: recipient_ccs,
      subject: template_renderer(subject_template, format: Mime[:text].to_s)
    }
  end

  def base_mail_headers(ticket)
    headers = {
      X_DELIVERY_CONTEXT => "automatic-answer-#{ticket.id}",
      "Message-Id" => ticket_message_id
    }

    inbound_emails_limit = MAX_REFERENCES - 1
    emails = ticket.inbound_emails.limit(inbound_emails_limit).order("created_at DESC")
    initial_email = emails.last

    if initial_email
      headers["References"] = build_references(emails)
    end

    default_in_reply_to = initial_email ? initial_email.message_id : ticket_reply_to_id
    headers["In-Reply-To"] = default_in_reply_to

    headers
  end

  def build_references(emails)
    message_ids = emails.map(&:message_id)
    headers = emails.flat_map { |e| e.references << e.in_reply_to }
    references = ([ticket_reply_to_id] + message_ids + headers).uniq.compact
    references = references[0...MAX_REFERENCES]
    references[1..-1] = references[1..-1].reverse # structure is supposed to be [origin, oldest, newest]
    references
  end

  def delimiter
    if account.mail_delimiter.include?("{{txt.email.delimiter}}")
      "##- #{I18n.t("txt.email.delimiter", locale: locale)} -##"
    else
      account.mail_delimiter
    end
  end

  def ticket_reply_to_address
    ticket_address.reply_to(user.is_agent?)
  end

  def log_deflection_sent
    statsd_client.increment("email_sent", tags: ["subdomain:#{account.subdomain}"])
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket_deflection'])
  end

  def recipient_ccs
    return [] unless read_only { account.has_comment_email_ccs_allowed_enabled? }

    ccs.map(&method(:user_to_email))
  end

  def ticket_message_id
    @ticket_message_id ||= ticket.generate_message_id(true)
  end

  def ticket_reply_to_id
    @ticket_reply_to_id ||= ticket.generate_message_id(false)
  end

  def user_to_email(user)
    user.is_a?(User) ? user.email_address_with_name : user.to_s
  end

  def url_builder
    @url_builder ||= Zendesk::Tickets::UrlBuilder.new(account: account, for_agent: for_agent)
  end

  def redact_attachments?(body_template)
    @redact_attachments ||= begin
      if account.has_email_remove_attachments_if_no_public_comment_placeholder?
        author_is_end_user? &&
          (user.is_a?(User) && user.is_end_user?) &&
          !check_for_public_comment_placeholders(body_template)
      else
        author_is_end_user? &&
          (user.is_a?(User) && user.is_end_user?) &&
          event.audit.comment&.first? && ticket.public_comments.count == 1 &&
          !check_for_public_comment_placeholders(body_template)
      end
    end
  end

  def check_for_public_comment_placeholders(body, format: Mime[:html].to_s)
    template_body = body.dup
    nesting_level = 0
    options = { detect_public_comment_placeholder: true }

    ticket_context = Zendesk::Liquid::TicketContext.new(template_body, ticket, ticket.requester, for_agent, locale, format, nesting_level, options)
    ticket_context.render
    ticket_context.public_comment_placeholder
  end
end
