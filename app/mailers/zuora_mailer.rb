class ZuoraMailer < ApplicationMailer
  def approval_mail(account)
    set_headers(
      account,
      X_DELIVERY_CONTEXT => "Zuora-PendingApprovals-#{account.id}",
      X_ZENDESK_PRIORITY_MAIL => "Zuora Subscription Approval Mail"
    )

    content = "Hi Billing Managers\n\n"
    content << "There is an update to subscription for account #{account.id} that needs your approval."
    content << "Please go to https://monitor.#{Zendesk::Configuration[:host]}/zuora_subscriptions \n"
    content << "to approve the changes."

    mail(
      from: Zendesk::Mailer::ZendeskAddress.new("Jennifer Hansen").from,
      to: "billing-approvals@zendesk.com",
      subject: "Zuora Changes Pending Approval (#{Rails.env})",
      content_type: Mime[:text].to_s,
      body: content.strip_tags
    )
  end
end
