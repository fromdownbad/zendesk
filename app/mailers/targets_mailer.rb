class TargetsMailer < ApplicationMailer
  include Zendesk::Mailer::ReadOnly

  def target_disabled(target)
    set_headers(target.account, X_DELIVERY_CONTEXT => "#{target.account_id}-target-disabled-#{target.id}")

    account = target.account
    subject = I18n.t(
      "txt.email.accounts_mailer.targets.target_disabled.subject",
      locale: account.translation_locale,
      target: target.title
    )
    content = I18n.t(
      "txt.email.accounts_mailer.targets.target_disabled.body",
      locale: account.translation_locale,
      target: target.title
    )
    admin_emails = account.admins.verified.uniq.map(&:email)
    Rails.logger.info("Sending target deactivation email for account id: #{account.id} target id: #{target.id} to admin addresses: #{admin_emails}")
    mail(
      subject: subject,
      bcc: admin_emails,
      from: noreply_address(account)
    ) do |format|
      format.text do
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: subject,
          content: content,
          footer: footer(account, true),
          account: account
        )
      end
      format.html do
        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: subject,
          content: "<p>#{content}</p>",
          footer: footer(account),
          account: account
        )
      end
    end
  end

  def target_message(account, recipient, subj, content, event)
    content ||= event.value_reference
    ticket    = event.ticket
    brand     = ticket.brand
    author    = event.author

    for_agent = false
    if recipient_user = account.find_user_by_email(recipient)
      for_agent = !recipient_user.is_end_user?
    end
    host = account.url.to_s
    subject = Zendesk::Liquid::TicketContext.render(subj, ticket, author, for_agent, account.translation_locale)

    set_headers(account, X_DELIVERY_CONTEXT => "target-message-#{event.id}")

    mail(
      subject: subject,
      to: recipient,
      from: from_address(account, brand: brand)
    ) do |format|
      format.text do
        # first liquidize ticket placeholders etc.
        liquidized_ticket_content = Zendesk::Liquid::TicketContext.render(content, ticket, author, for_agent, account.translation_locale, Mime[:text].to_s)
        Zendesk::Liquid::MailContext.render(
          template: account.text_mail_template,
          header: subject,
          content: liquidized_ticket_content.try(:strip_tags),
          footer: footer(account, true, nil, nil, brand),
          account: account,
          translation_locale: account.translation_locale
        )
      end
      format.html do
        liquidized_ticket_content = Zendesk::Liquid::TicketContext.render(content, ticket, author, for_agent, account.translation_locale, Mime[:html].to_s)
        liquidized_ticket_content = liquidized_ticket_content.html_safe

        ticket_path = Zendesk::Tickets::UrlBuilder.new(account: account, for_agent: for_agent).path_for(nil)
        if ticket_path
          liquidized_ticket_content = liquidized_ticket_content.auto_link.auto_link_tickets(host: host, path: ticket_path)
        end

        Zendesk::Liquid::MailContext.render(
          template: account.html_mail_template,
          header: subject,
          content: liquidized_ticket_content,
          footer: footer(account, false, nil, nil, brand),
          account: account,
          translation_locale: account.translation_locale
        )
      end
    end
  end
end
