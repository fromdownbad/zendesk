class AgreementMailer < ApplicationMailer
  def new_from_partner(agreement)
    subject = I18n.t('txt.admin.models.sharing.agreement_mailer.you_have_received_a_new_ticket_sharing', locale: agreement.account.translation_locale)
    base_message(agreement, subject)
  end

  def status_change(agreement, changed_by)
    @changed_by = changed_by
    subject = I18n.t('txt.admin.models.sharing.agreement_mailer.sharing_agreement_is_now_' + agreement.status.to_s.downcase, locale: agreement.account.translation_locale)
    base_message(agreement, subject)
  end

  def invite_revoked(agreement)
    subject = I18n.t('txt.admin.models.sharing.agreement_mailer.your_ticket_sharing_has_been_revoked', locale: agreement.account.translation_locale)
    base_message(agreement, subject)
  end

  def sharing_agreement_deleted(agreement)
    subject = I18n.t('txt.admin.models.sharing.agreement_mailer.your_ticket_sharing_agreement_has_been_deleted', locale: agreement.account.translation_locale)
    base_message(agreement, subject)
  end

  def activation_change(agreement)
    subject = I18n.t('txt.admin.models.sharing.agreement_mailer.your_ticket_sharing_agreement_is_now_' + activation_status(agreement), locale: agreement.account.translation_locale)
    base_message(agreement, subject)
  end

  private

  def base_message(agreement, subject)
    set_headers(agreement.account, X_DELIVERY_CONTEXT => "agreement-#{agreement.id}")
    @agreement = agreement
    @changed_by ||= agreement.changed_by

    mail(
      from: agreement.account.reply_address,
      to: agreement.account.admins.map(&:email),
      subject: subject
    )
  end

  def activation_status(agreement)
    agreement.status == :accepted ? 'reactivated' : 'deactivated'
  end
  helper_method :activation_status

  def footer(account)
    I18n.t('txt.email.footer', account_name: account.name, locale: account.translation_locale)
  end
  helper_method :footer

  def build_lotus_url(options)
    "#{options[:host]}/agent/admin/tickets/ticket_sharing/#{options[:agreement_id]}"
  end
  helper_method :build_lotus_url
end
