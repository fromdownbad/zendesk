require 'zendesk_stats'

module ForumObserver
  class << self
    def included(base)
      base.after_create do |obj|
        unless obj.respond_to?(:deleted?) && obj.deleted?
          ForumObserver.store_event(obj)
        end
      end
    end

    def store_event(obj)
      base = {ts: Time.now.to_i, account_id: obj.account.id, shard_id: obj.account.shard_id}
      case obj
      when Entry
        store_forum_event("entry_create", base.merge(entry_id: obj.id, forum_id: obj.forum_id, category_id: obj.forum.category_id, user_id: obj.submitter_id))
      when Vote
        store_forum_event("vote_create", base.merge(entry_id: obj.entry_id, forum_id: obj.entry.forum_id, category_id: obj.entry.forum.category_id, user_id: obj.user_id))
      when Post
        store_forum_event("post_create", base.merge(entry_id: obj.entry_id, forum_id: obj.entry.forum_id, category_id: obj.entry.forum.category_id, user_id: obj.user_id))
      when Watching
        if obj.source.is_a?(Entry)
          store_forum_event("watching_entry_create", base.merge(entry_id: obj.source.id, forum_id: obj.source.forum_id, category_id: obj.source.forum.category_id, user_id: obj.user_id))
        elsif obj.source.is_a?(Forum)
          store_forum_event("watching_forum_create", base.merge(forum_id: obj.source.id, category_id: obj.source.category_id, user_id: obj.user_id))
        end
      end
    end

    private

    def store_forum_event(type, hash)
      Zendesk::ClassicStats::ForumEvent.store(hash.merge(type: type))
    end
  end
end
