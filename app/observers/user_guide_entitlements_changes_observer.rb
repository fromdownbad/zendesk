module UserGuideEntitlementsChangesObserver
  extend ActiveSupport::Concern

  COLUMNS_TO_INSTRUMENT = %w[is_moderator roles permission_set_id].freeze
  SOURCE_OF_INSTRUMENT = 'guide_user_change'.freeze

  included do
    after_save :instrument_guide_column_changes
  end

  private

  def instrument_guide_column_changes
    Zendesk::ModelChangeInstrumentation.new(
      model: self,
      exec_context: binding,
      columns_to_instrument: COLUMNS_TO_INSTRUMENT,
      source_of_instrument: SOURCE_OF_INSTRUMENT
    ).perform
  end
end
