module UserSeatChangesObserver
  extend ActiveSupport::Concern

  COLUMNS_TO_INSTRUMENT = ['is_active', 'seat_type', 'user_id'].freeze
  SOURCE_OF_INSTRUMENT = 'user_seat_change'.freeze

  included do
    after_commit :instrument_user_seat_changes
  end

  private

  def instrument_user_seat_changes
    Zendesk::ModelChangeInstrumentation.new(
      model: self,
      exec_context: binding,
      columns_to_instrument: COLUMNS_TO_INSTRUMENT,
      source_of_instrument: SOURCE_OF_INSTRUMENT
    ).perform
  end
end
