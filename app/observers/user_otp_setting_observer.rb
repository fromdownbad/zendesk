module UserOtpSettingObserver
  extend ActiveSupport::Concern

  included do
    after_update :deliver_email_enabled
    after_destroy :deliver_email_disabled
  end

  private

  def deliver_email_enabled
    if confirmed_changed? && confirmed?
      SecurityMailer.profile_two_factor_auth_enabled(user).deliver
    end
  end

  def deliver_email_disabled
    return if user.account.multiproduct?
    SecurityMailer.profile_two_factor_auth_disabled(user).deliver
  end
end
