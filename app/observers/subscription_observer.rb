module SubscriptionObserver
  class << self
    def included(base)
      this = self
      base.after_save ->(subscription) { this.after_save(subscription) }
    end

    def after_save(subscription)
      if churned?(subscription)
        Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(subscription.account)
      end
    end

    private

    def churned?(subscription)
      subscription.account_status == 'churned'
    end
  end
end
