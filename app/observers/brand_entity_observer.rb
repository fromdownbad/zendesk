module BrandEntityObserver
  extend ActiveSupport::Concern

  included do
    after_save :publish_brand_or_tombstone
  end

  private

  def publish_brand_or_tombstone
    return unless account.has_publish_brand_entities?
    publisher = BrandPublisher.new(account_id: account_id)
    if deleted?
      publisher.publish_tombstone(brand_id: id)
    elsif changed?
      publisher.publish_for_brand(brand_id: id)
    end
  end
end
