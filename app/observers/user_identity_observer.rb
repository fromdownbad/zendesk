module UserIdentityObserver
  def self.included(base)
    base.after_commit :send_notification
  end

  def send_notification
    return unless user
    return unless user.is_admin? || user.is_agent?
    return unless monitored?
    return if skip_notification?
    return if transaction_include_any_action?([:create]) && !is_verified?

    if transaction_include_any_action?([:update]) # just a regular update
      if primary_email_changed? && !previous_changes['id']
        Zendesk::Gooddata::UserProvisioning.sync_for_user(user)
      end
      return unless user_email_identity_update?
      if primary_email_changed?
        return if unverified_admin_on_trial_accounts?
        SecurityMailer.deliver_user_primary_email_changed(user, CIA.current_actor, notification_options_for)
      end

      return unless auxillary_email_verified?
    end

    SecurityMailer.send(identity_notification_for, user, CIA.current_actor, notification_options_for)
  end

  private

  def unverified_admin_on_trial_accounts?
    user.is_admin? && !is_verified? && user.account.is_trial?
  end

  def notification_options_for
    options = {
      identity_type: identity_type,
      identity_value: to_s,
    }
    options[:previous_primary_email] =
      previous_primary_email if previous_primary_email

    options
  end

  def skip_notification?
    # skip notification for temporary pre-account creation identity or if specifically used for account sandbox replication
    value == PreAccountCreation::OWNER_EMAIL_PLACEHOLDER ||
    user.account.has_email_suppress_email_identity_update_notifications? ||
    user.account.settings.sandbox_creation_in_progress == true
  end

  def monitored?
    is_a?(UserEmailIdentity) || is_a?(UserVoiceForwardingIdentity)
  end

  def primary_email_changed?
    is_a?(UserEmailIdentity) &&
      primary?               &&
      !google?               &&
      (previous_changes['value'] || previous_changes['priority'])
  end

  def previous_primary_email
    if primary_email_changed?
      previous_changes['value'].first if previous_changes['value']
    end
  end

  def auxillary_email_verified?
    is_a?(UserEmailIdentity) &&
      previous_changes['is_verified'] &&
      is_verified? &&
      !primary?
  end

  def identity_notification_for
    action = if transaction_include_any_action?([:destroy])
      :removed
    else
      :added
    end

    infix =
      case self
      when UserVoiceForwardingIdentity
        :voice_forwarding_number
      when UserEmailIdentity
        (google? ? :external_account : :auxillary_email)
      else
        :external_account
      end
    :"deliver_user_#{infix}_#{action}"
  end

  def user_email_identity_update?
    is_a?(UserEmailIdentity) &&
      !previous_changes['id']
  end
end
