module PermissionExploreEntitlementsChangesObserver
  extend ActiveSupport::Concern

  EXPLORE_PERMISSIONS = ['explore_access'].freeze
  COLUMNS_TO_INSTRUMENT = ['value'].freeze
  SOURCE_OF_INSTRUMENT = 'explore_permissions_change'.freeze

  included do
    after_save :instrument_explore_permission_changes
  end

  private

  def instrument_explore_permission_changes
    return unless EXPLORE_PERMISSIONS.include? name
    Zendesk::ModelChangeInstrumentation.new(
      model: self,
      exec_context: binding,
      columns_to_instrument: COLUMNS_TO_INSTRUMENT,
      source_of_instrument: SOURCE_OF_INSTRUMENT
    ).perform
  end
end
