module BrandLogoObserver
  extend ActiveSupport::Concern

  included do
    after_save :publish_brand
    after_destroy :publish_brand
  end

  private

  def publish_brand
    return unless account && account.has_publish_brand_entities?
    return unless changed? || destroyed?

    BrandPublisher.new(account_id: account.id).publish_for_brand(brand_id: brand_id)
  end
end
