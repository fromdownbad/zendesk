module GroupEntityObserver
  def self.included(base)
    base.send :after_commit, :publish_group_or_tombstone, on: %i[create update destroy]
  end

  private

  def publish_group_or_tombstone
    return unless account&.has_publish_group_entities?

    publisher = GroupPublisher.new(account_id: account_id)

    if destroyed? || !is_active?
      publisher.publish_tombstone(group_id: id)
    else
      publisher.publish(group: self)
    end
  end
end
