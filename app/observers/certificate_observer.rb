module CertificateObserver
  extend ActiveSupport::Concern

  included do
    after_save :publish_brand
    after_destroy :publish_brand
  end

  private

  def publish_brand
    return unless account.has_publish_brand_entities?
    return unless changed? || destroyed?

    BrandPublisher.new(account_id: account.id).publish_for_account
  end
end
