# Observer class for permission sets. Used to sync gooddata
# users when the definition of their assigned permission set changes.
module PermissionSetObserver
  class << self
    def included(base)
      this = self
      base.after_save ->(permission_set) { this.after_save(permission_set) }
    end

    def after_save(permission_set)
      return unless report_access_changed?(permission_set)

      permission_set.users.each do |user|
        Zendesk::Gooddata::UserProvisioning.sync_for_user(user)
      end
    end

    private

    def report_access_changed?(permission_set)
      permission_set.permissions.any? { |permission| permission.name == 'report_access' && !permission.previous_changes.empty? }
    end
  end
end
