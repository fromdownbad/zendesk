module GooddataSyncObserver
  def self.included(base)
    base.after_commit on: :create do
      Zendesk::Gooddata::UserProvisioning.create_for_user(self) if is_agent?
    end
    base.after_commit on: :destroy do
      Zendesk::Gooddata::UserProvisioning.destroy_for_user(self)
    end

    base.after_commit :sync_gooddata_user, on: :update
  end

  def sync_gooddata_user
    # When a role changes in Zendesk, we want to remember to update GoodData to match.
    # Same when the permissions set changes, since the GoodData role may be based on
    # either the Zendesk role or the Zendesk permissions set.
    if previous_changes.include?(:roles) ||
       previous_changes.include?(:permission_set_id) ||
       (is_agent? && previous_changes.include?(:name))
      Zendesk::Gooddata::UserProvisioning.sync_for_user(self)
    end
  end
end
