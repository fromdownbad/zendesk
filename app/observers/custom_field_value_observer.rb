module CustomFieldValueObserver
  extend ActiveSupport::Concern

  included do
    after_save :emit_event
    after_destroy :emit_event
  end

  private

  def emit_event
    previous, current = destroyed? ? [value, nil] : changes[:value]
    return unless previous != current

    case field.owner
    when 'Organization'
      owner.add_domain_event(OrganizationCustomFieldChangedProtobufEncoder.new(field, previous, current).to_safe_object)
      owner.publish_organization_events_to_bus!
    end
  end
end
