require 'zendesk/current_account_integration'

# Raised if a record from a different account is loaded or saved.
#
class AccountRestrictionError < RuntimeError
end

# Observes all models, raising an error if a record from a different account is
# loaded or saved.
#
module AccountRestrictionObserver
  class << self
    attr_accessor :silenced

    def included(base)
      this = self
      base.after_find ->(record) { this.after_find(record) }
      base.after_save ->(record) { this.after_save(record) }
    end

    def silence
      old = silenced
      self.silenced = true
      yield
    ensure
      self.silenced = old
    end

    def after_find(record)
      if !silenced && AccountRestrictor.deny?(record)
        message = "cannot load record #{record.inspect} - not on account ##{Zendesk.current_account.id}"

        if enforce_on_read?
          raise AccountRestrictionError, message
        else
          Rails.logger.warn("#{message}:#{caller.join("\n")}")
        end
      end
    end

    def after_save(record)
      if AccountRestrictor.deny?(record)
        message = "cannot save record #{record.inspect} - not on account ##{Zendesk.current_account.id}"
        raise AccountRestrictionError, message
      end
    end

    def enforce_on_read?
      !Rails.env.production? || Zendesk.current_account.has_account_restriction_on_read?
    end
  end
end

ActiveRecord::Base.include(AccountRestrictionObserver)
