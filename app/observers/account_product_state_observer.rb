module AccountProductStateObserver
  extend ActiveSupport::Concern

  COLUMNS_TO_INSTRUMENT = ['is_active', 'is_serviceable', 'sandbox_master_id', 'deleted_at'].freeze
  SOURCE_OF_INSTRUMENT = 'account_product_state_change'.freeze

  included do
    after_save :instrument_product_changes
  end

  private

  def instrument_product_changes
    Zendesk::ModelChangeInstrumentation.new(
      model: self,
      exec_context: binding,
      source_pattern: 'lib\/zendesk\/support_accounts|app\/controllers\/api\/v2\/accounts_creation|app\/models\/jobs\/support_creation|lib\/zendesk_billing_core|app\/services\/provisioning',
      prevent_unknown_update_from: [:console],
      columns_to_instrument: COLUMNS_TO_INSTRUMENT,
      source_of_instrument: SOURCE_OF_INSTRUMENT
    ).perform
  end
end
