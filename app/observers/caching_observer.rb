module CachingObserver
  EXEMPTED_USER_SETTINGS = [
    :last_lotus_login
  ].freeze

  # Besides this, there are 4 other places where cache invalidation can occur:
  #   * automation.rb/run - batch job running once per hour
  #   * inbound_mailer.rb - when account receives ticket updates per mail
  #   * Verification_controller
  #   * Accounts_controller
  #   * Proxy controller - when adding a push event for a ticket, the "all events" view should be cleared.
  class << self
    def included(base)
      base.after_commit ->(record) { CachingObserver.expire_fragments_on_save_or_destroy(record) }, prepend: true
    end

    def expire_fragments_on_save_or_destroy(record)
      # Avoid caching_observer calls if the change is because of a bulk import
      return if record.respond_to?(:is_bulk_updated) && record.is_bulk_updated

      case record
      when Activity
        record.user.expire_scoped_cache_key(:activity)

      when Membership
        Rails.logger.info('Expiring groups_agents scoped cache key because membership changed')
        record.account.expire_scoped_cache_key(:groups_agents)

      when Sharing::Agreement
        record.account.expire_scoped_cache_key(:sharing_agreements)

      when Ticket
        record.account.expire_scoped_cache_key(:views)

      when TicketPrediction
        expiry_for(record.ticket.account).expire_radar_cache_key("tickets/#{record.ticket.nice_id}/ticket_prediction")

      when VoiceApiComment
        expiry_for(record.account).expire_radar_cache_key("tickets/#{record.ticket.nice_id}/comments")

      when TicketField
        record.expire_scoped_caches
      when TicketForm
        record.account.expire_scoped_cache_key(:ticket_forms)
        # this is to update the ticket_form_ids that we send in the brands endpoint
        record.account.expire_scoped_cache_key(:brands)

      when TicketFormBrandRestriction
        record.account.expire_scoped_cache_key(:brands)
        record.account.expire_scoped_cache_key(:ticket_forms)

      when TicketFieldCondition
        record.account.expire_scoped_cache_key(:ticket_forms)

      when User
        # As user changes can cause unassignment of tickets, also clears browser .js cache for users
        record.account.expire_scoped_cache_key(:users)
        if record.is_agent? || record.is_light_agent? || record.previous_changes["roles"].try(:first).to_i > 0

          expiry_for(record.account).expire_radar_cache_key(:users) unless record.previous_changes["id"] || skip_users_radar_expiry?(record)

          record.account.expire_scoped_cache_key(:views)
          if expiring_changes = (record.previous_changes.keys & ["name", "is_active", "roles", "permission_set", "permission_set_id"]).presence
            Rails.logger.info("Expiring groups_agents scoped cache key because of changes to user #{expiring_changes}")
            record.account.expire_scoped_cache_key(:groups_agents)
          end
        end

      when UserIdentity
        record.user.try(:smart_touch)

      when Group
        expiry_for(record.account).expire_radar_cache_key(:groups)
        record.account.expire_scoped_cache_key(:views)
        record.account.expire_scoped_cache_key(:macros)
        if record.previous_changes["name"] || record.previous_changes["is_active"]
          Rails.logger.info('Expiring groups_agents scoped cache key because group changed')
          record.account.expire_scoped_cache_key(:groups_agents)
        end

      when Entry
        record.forum.account.expire_scoped_cache_key(:forums)

      when Forum, Category
        record.account.expire_scoped_cache_key(:forums)

      when SuspendedTicket
        record.account.try(:expire_scoped_cache_key, :views)

      when View
        expiry = expiry_for(record.account)
        expiry.expire_radar_cache_key(:views)
        expiry.expire_radar_cache_key("views/#{record.id}/tickets")
        record.account.expire_scoped_cache_key(:views)
        record.account.expire_scoped_cache_key(:rules)
        record.account.expire_scoped_cache_key(:rule_analysis) unless record.owner_type == 'User'

      when Macro
        expiry_for(record.account).expire_radar_cache_key(:macros)
        record.account.expire_scoped_cache_key(:macros)
        record.account.expire_scoped_cache_key(:rules)
        record.account.expire_scoped_cache_key(:rule_analysis) unless record.owner_type == 'User'

      when Automation
        record.account.expire_scoped_cache_key(:automations)
        record.account.expire_scoped_cache_key(:rules)
        record.account.expire_scoped_cache_key(:rule_analysis)

      when Trigger
        record.account.expire_scoped_cache_key(:triggers)
        record.account.expire_scoped_cache_key(:rules)
        record.account.expire_scoped_cache_key(:rule_analysis)

      when Account # only for update_plan
        record.expire_scoped_cache_key(:views)
        record.expire_scoped_cache_key(:forums)

      when RoleSettings
        record.account.expire_scoped_cache_key(:role_settings)

      when UserSetting
        if record.name == "suspended" && record.user.is_agent?
          record.account.expire_scoped_cache_key(:groups_agents)
        end

      when AccountSetting, AccountText
        record.account.expire_scoped_cache_key(:settings)

        case record.name.to_sym
        when :forum_title, :display_pinned_entries, :display_pinned_entry_content
          record.account.expire_scoped_cache_key(:forums)
        when :voice, :chat, :voice_outbound_enabled
          expiry_for(record.account).expire_radar_cache_key(:settings)
        when :macro_order
          expiry_for(record.account).expire_radar_cache_key(:macros)
        end

      when AccountPropertySet
        record.account.expire_scoped_cache_key(:settings)

      when Vote
        record.entry.smart_touch
        record.entry.forum.smart_touch

      when Post
        record.entry.expire_scoped_cache_key(:posts)

      when Rule
        expiry_for(record.account).expire_radar_cache_key(:views)

      when Organization
        expiry_for(record.account).expire_radar_cache_key(:organizations)

      when PermissionSet
        expiry_for(record.account).expire_radar_cache_key(:users)

        # Changing a permission set can change assignable agents and groups
        Rails.logger.info('Expiring groups_agents scoped cache key because permission set changed')
        record.account.expire_scoped_cache_key(:groups_agents)

      when JetpackTask
        expiry_for(record.account).expire_radar_cache_key(:jetpack_tasks)

      when CustomField::Field
        cache_key = record.owner == "User" ? :user_fields : :organization_fields
        expiry_for(record.account).expire_radar_cache_key(cache_key)

      when Brand
        record.account.expire_scoped_cache_key(:brands)
        expiry_for(record.account).expire_radar_cache_key(:brands)

      when Cms::Text, Cms::Variant
        record.account.expire_scoped_cache_key(:ticket_fields)
        record.account.expire_scoped_cache_key(:ticket_field_custom_field_options)

        if record.account.has_cache_dynamic_content_account_content? && record.is_a?(Cms::Variant)
          record.account.expire_scoped_cache_key(:"dynamic_content/#{record.translation_locale.locale}")
        end
      end
    rescue Exception => e # rubocop:disable Lint/RescueException
      Rails.logger.info("Exception in #expire_fragments_on_save_or_destroy: #{e.message}")
      raise e
    end

    private

    def skip_clear_account_settings_cache?(account)
      account.address.nil? ||
        account.address.new_record? ||
        account.new_record? ||
        account.pre_account_creation.try(:is_binding?) ||
        account.route.nil?
    end

    def skip_users_radar_expiry?(user)
      updated_user_settings = user.settings.select { |setting| setting.previous_changes.present? }

      (updated_user_settings.map { |s| s.name.to_sym } - EXEMPTED_USER_SETTINGS).empty? && user.previous_changes.empty?
    end

    def expiry_for(account)
      Zendesk::RadarExpiry.get(account)
    end
  end
end
