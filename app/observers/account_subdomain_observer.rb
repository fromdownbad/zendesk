module AccountSubdomainObserver
  def self.included(base)
    base.after_commit do |account|
      queue_gooddata_jobs(account)
    end
  end

  private

  def queue_gooddata_jobs(account)
    return unless account.previous_changes.include?(:subdomain) && account.gooddata_integration

    GooddataConfigurationJob.enqueue(account.id)
    GooddataFullReloadJob.enqueue(account.id)
  end
end
