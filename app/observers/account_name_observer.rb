module AccountNameObserver
  extend ActiveSupport::Concern

  included do
    before_update :propagate_name
  end

  def propagate_name
    AccountNamePropagator.call(self)
  end
end
