module PermissionGuideEntitlementsChangesObserver
  extend ActiveSupport::Concern

  GUIDE_PERMISSIONS = ['forum_access', 'forum_access_restricted_content'].freeze
  COLUMNS_TO_INSTRUMENT = ['value'].freeze
  SOURCE_OF_INSTRUMENT = 'guide_permission_change'.freeze

  included do
    after_save :instrument_guide_permission_changes
  end

  private

  def instrument_guide_permission_changes
    return unless GUIDE_PERMISSIONS.include? name
    Zendesk::ModelChangeInstrumentation.new(
      model: self,
      exec_context: binding,
      columns_to_instrument: COLUMNS_TO_INSTRUMENT,
      source_of_instrument: SOURCE_OF_INSTRUMENT
    ).perform
  end
end
