module SubscriptionProductStateObserver
  extend ActiveSupport::Concern

  COLUMNS_TO_INSTRUMENT = ['plan_type', 'max_agents', 'trial_expires_on', 'is_trial', 'churned_on', 'payment_method_type'].freeze
  SOURCE_OF_INSTRUMENT = 'subscription_product_change'.freeze

  included do
    after_save :instrument_product_changes
  end

  private

  def instrument_product_changes
    Zendesk::ModelChangeInstrumentation.new(
      model: self,
      exec_context: binding,
      source_pattern: 'lib\/zendesk\/support_accounts|app\/controllers\/api\/v2|app\/models\/jobs|lib\/zendesk_billing_core|app\/services\/provisioning',
      prevent_unknown_update_from: [:console],
      columns_to_instrument: COLUMNS_TO_INSTRUMENT,
      source_of_instrument: SOURCE_OF_INSTRUMENT
    ).perform
  end
end
