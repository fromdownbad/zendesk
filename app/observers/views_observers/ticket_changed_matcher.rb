module ViewsObservers
  class TicketChangedMatcher < EntityChangedMatcher
    SCHEDULE_ASSIGNMENT = 'ScheduleAssignment'.freeze
    STATISFACTION_PROBABILITY = 'satisfaction_probability'.freeze

    # The attributes of Ticket that, on update, trigger entity publication
    # We can't re-use the map used to build the Ticket protobuf object as the field names vary

    ## TODO: The following related fields will need to trigger Ticket publication on save
    # This is not even a complete set, just the fields that are not directly accessible
    # from the ticket model itself
    #:assignee_name,
    #:brand_name,
    #:group_name,
    #:organization_name,
    #:requester_name
    #:requester_role,
    #:sharing_agreement_ids,
    FIELDS =
      [
        :account_id,
        :assigned_at,
        :assignee_id,
        :assignee_updated_at,
        :brand_id,
        :created_at,
        :current_tags,
        :description,
        :due_date,
        :group_id,
        :id,
        :initially_assigned_at,
        :is_public,
        :locale_id,
        :nice_id,
        :organization_id,
        :priority,
        :recipient,
        :requester_id,
        :requester_name,
        :requester_timestamps,
        :requester_updated_at,
        :satisfaction_probability,
        :satisfaction_reason_code,
        :satisfaction_score,
        :sla_breach_status,
        :solved_at,
        :status_id,
        :status_updated_at,
        :ticket_form_id,
        :ticket_type_id,
        :updated_at,
        :updated_by_type_id,
        :via_id,
        :via_reference_id
      ].freeze
    private_constant :FIELDS

    def matched?
      super || related_entity_changed?
    end

    def fields
      FIELDS
    end

    private

    def related_entity_changed?
      ticket_prediction_changed? || schedule_changed?
    end

    def schedule_changed?
      entity.audits.last.events.any? do |event|
        event.type == SCHEDULE_ASSIGNMENT
      end
    end

    def ticket_prediction_changed?
      entity.delta_changes.key?(STATISFACTION_PROBABILITY)
    end
  end
end
