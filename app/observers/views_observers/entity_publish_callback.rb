module ViewsObservers
  # Provides a way to publish Entities via Escape
  # ViewsObservers::EntityAfterSave intended to be a callback object
  # https://apidock.com/rails/v4.2.1/ActiveRecord/Callbacks
  class EntityPublishCallback
    attr_reader :model, :publisher, :namespace, :encoder_klass

    # @param [String] topic The topic to write too
    # @param [String] namespace The DD namespace, prefixed with 'support'
    # @param [#to_proto] encoder_klass The entity protobuf encoder Class
    # @param [EntityChangeMatcher] matcher_klass The EntityChangeMatcher class, responds_to #matched? and #tombstone?.
    #
    # See app/observers/views_observers/ticket_observer.rb for an example
    def initialize(topic:, namespace:, encoder_klass:, matcher_klass:)
      @namespace = namespace
      @encoder_klass = encoder_klass
      @publisher = EntityPublisher.new(topic, namespace, encoder_klass, matcher_klass)
    end

    def after_save(model)
      @model = model

      return unless enabled?

      statsd.time('after_save_publish_time', tags: tags) do
        publisher.publish(model)
      end
    end

    private

    def enabled?
      account.has_publish_ticket_entities_to_bus?
    end

    def account
      model.account
    end

    def statsd
      @statsd ||= Zendesk::StatsD::Client.new(namespace: ['support', namespace])
    end

    def tags
      ["entity:#{encoder_klass.name.demodulize}"]
    end
  end
end
