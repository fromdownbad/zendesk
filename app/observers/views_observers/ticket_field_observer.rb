module ViewsObservers::TicketFieldObserver
  extend ActiveSupport::Concern
  included do
    after_save ViewsObservers::EntityPublishCallback.new(
      topic: 'support.views.ticket_fields',
      namespace: 'views_entity_stream',
      encoder_klass: ViewsEncoders::TicketFieldProtobufEncoder,
      matcher_klass: ViewsObservers::TicketFieldChangedMatcher
    )
  end
end
