module ViewsObservers
  class TicketFieldChangedMatcher < EntityChangedMatcher
    FIELDS = [
      :value
    ].freeze
    private_constant :FIELDS

    def fields
      FIELDS
    end

    def tombstone?
      # Currently, we have no way of knowing when a ticket field entry
      # gets deleted
      false
    end
  end
end
