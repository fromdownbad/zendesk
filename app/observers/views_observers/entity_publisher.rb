require 'activerecord-import'
require "activerecord-import/base"
require 'zendesk_protobuf_clients/zendesk/protobuf/escape/escape_metadata_pb'
ActiveRecord::Import.require_adapter('mysql2')

module ViewsObservers
  G = ::Google::Protobuf
  E = ::Zendesk::Protobuf::Escape

  # Publishes entites to escape
  class EntityPublisher
    attr_reader :topic, :namespace, :encoder_klass, :matcher_klass, :entity_producer

    def initialize(topic, namespace, encoder_klass, matcher_klass, entity_producer = EscapeProducer.new)
      @topic = topic
      @namespace = namespace
      @encoder_klass = encoder_klass
      @matcher_klass = matcher_klass
      @entity_producer = entity_producer
    end

    # Publish entites to Escape
    #
    # @params entities A single entity or list of entites
    # I need more than forcing tombstoning, I need to force fullstop
    def publish(entities, force_tombstone: false, ignore_changes: false)
      return if entities.blank?

      messages = Array(entities).each_with_object([]) do |entity, memo|
        if force_tombstone
          memo << message(entity, force_tombstone)
        else
          matcher = matcher_klass.new(entity)
          memo << message(entity, matcher.tombstone?) unless skip_entity?(ignore_changes, matcher)
        end
      end

      entity_producer.write(messages)
    rescue StandardError => e
      # For now, prefer failing silently over crashing Ticket save.
      Rails.logger.error(e)
      statsd.increment('errors', tags: tags + ["exception:#{e.class}"])
    end

    private

    # This just takes an entity and a tombstone flag
    def message(entity, tombstone)
      value = tombstone ? nil : encode(entity)
      {
        account_id: entity.account_id,
        topic: topic,
        key: key(entity),
        value: value,
        metadata: metadata
      }
    end

    def skip_entity?(ignore_changes, matcher)
      return false if ignore_changes

      !matcher.matched?
    end

    def key(entity)
      "#{entity.account_id}/#{entity.id}"
    end

    def encode(entity)
      encoder_klass.new(entity).to_proto
    end

    def statsd
      @statsd ||= Zendesk::StatsD::Client.new(namespace: ['support', namespace])
    end

    def tags
      ["entity:#{encoder_klass.name.demodulize}"]
    end

    def metadata
      now = DateTime.now.strftime('%Q') # Outputs milliseconds since epoch
      E::Metadata.new(headers: {'ZENDESK_PUBLICATION_TIMESTAMP' => G::BytesValue.new(value: now)}).to_proto
    end
  end
end
