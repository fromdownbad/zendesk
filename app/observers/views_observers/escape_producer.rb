module ViewsObservers
  class EscapeProducer
    def write(messages)
      domain_event_publisher.bulk_import!(messages)
      messages
    end

    def domain_event_publisher
      @domain_event_publisher || EscKafkaMessage
    end
  end
end
