# Check if entity.changes [ActiveModel::Dirty] match
# any of the given fields or if entity has been deleted
module ViewsObservers
  class EntityChangedMatcher
    attr_reader :entity

    def initialize(entity)
      @entity = entity
    end

    def matched?
      tombstone? || changed_fields.any?
    end

    def tombstone?
      entity.try(:deleted?)
    end

    # @return [Array(Symbol)] Changed fields of interest
    def fields
      raise NotImplementedError
    end

    private

    def changed_fields
      entity.changes.symbolize_keys.keys & fields
    end
  end
end
