module ViewsObservers::TicketObserver
  TOPIC = 'support.views.tickets'.freeze

  extend ActiveSupport::Concern
  included do
    after_save ViewsObservers::EntityPublishCallback.new(
      topic: TOPIC,
      namespace: 'views_entity_stream',
      encoder_klass: ViewsEncoders::ViewsTicketProtobufEncoder,
      matcher_klass: ViewsObservers::TicketChangedMatcher
    )
  end
end
