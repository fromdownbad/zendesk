module ViewsObservers
  class ApiProducer
    def write(messages)
      # TODO: This should be a GRPC endpoint, not this horrible nonsense
      messages.map do |message|
        if message[:value]
          message.merge(value: Base64.encode64(message[:value]))
        else
          message
        end
      end
    end
  end
end
