module ViewsObservers
  class TruthyMatcher < EntityChangedMatcher
    attr_reader :entity, :force_tombstone

    def matched?
      true
    end
  end
end
