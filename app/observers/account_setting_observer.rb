module AccountSettingObserver
  # See discussion in https://zendesk.atlassian.net/browse/APPSEC-371
  # on why we are not delivering notification for changes in:
  # - api_token_access
  # - api_token
  OBSERVED_SETTINGS = %w[api_password_access].freeze

  class InvalidSettingError < StandardError
  end

  class << self
    def included(base)
      this = self

      base.after_save do |setting|
        this.send_security_email_notifications(setting)
        this.ensure_satisfaction_reasons_exist(setting)
        this.logout_all_mobile_devices_when_mobile_app_disabled(setting)
        this.send_account_assumption_email(setting) if setting.account.has_account_assumption_control?
        this.kickoff_acme_certificate_job(setting)
        this.track_changes_in_datadog(setting)
        this.track_changes_to_default_brand(setting)
        this.handle_polaris_radar_notification(setting)
        this.handle_polaris_chat_app_installation(setting)
        this.notify_chat_of_polaris_settings_changed(setting)
        this.track_changes_in_kafka(setting)
        this.handle_email_radar_notification(setting)
        true
      end

      base.after_commit do |setting|
        this.notify_chat_of_ip_settings_changed(setting)
      end
    end

    def logout_all_mobile_devices_when_mobile_app_disabled(setting)
      return unless setting.name == 'mobile_app_access' && setting.value == "0" && setting_changed?(setting)
      LogoutAllMobileDevicesJob.enqueue(setting.account.id)
    end

    def kickoff_acme_certificate_job(setting)
      return unless setting.name == 'automatic_certificate_provisioning' && setting.value == "1" && setting_changed?(setting)
      AcmeCertificateJobStatus.enqueue_with_status!(setting.account.id)
    end

    def send_security_email_notifications(setting)
      return unless OBSERVED_SETTINGS.include?(setting.name)
      return unless has_security_email_notifications_feature?(setting)
      return unless notification_required?(setting)

      notification = setting_notification_for(setting)
      SecurityMailer.send(notification, setting, CIA.current_actor)
    end

    def ensure_satisfaction_reasons_exist(setting)
      return unless setting.name == 'csat_reason_code'
      return unless setting.value == '1'
      return unless setting.account.satisfaction_reasons.none?

      Satisfaction::Reason.create_system_reasons_for(setting.account)
    end

    def send_account_assumption_email(setting)
      return unless setting.name == 'assumption_expiration'
      action = assumption_expiration_action(setting)
      return if action.nil? || action == :nochange
      SecurityMailer.deliver_assumption_expiration(setting, action, setting.value)
    end

    def notify_chat_of_ip_settings_changed(setting)
      return unless Arturo.feature_enabled_for?(:notify_settings_change_to_zopim, setting.account)
      return unless ['enable_agent_ip_restrictions', 'ip_restriction_enabled'].include?(setting.name)
      return unless setting.account.multiproduct? && Zendesk::Accounts::Client.new(setting.account).chat_product

      Zopim::InternalApiClient.new(setting.account_id).notify_account_settings_change('ip_settings_changed')
    end

    def track_changes_in_datadog(setting)
      return unless setting.name == 'ticket_threads'
      return unless setting_changed?(setting)

      Rails.application.config.statsd.client.increment("account_setting.#{setting.name}.#{setting.true? ? 'enabled' : 'disabled'}")
    end

    def track_changes_in_kafka(setting)
      return unless AccountSettingProtobufEncoder.supported?(setting.name) && setting_changed?(setting)

      EscKafkaMessage.create!(
        account_id: setting.account_id,
        topic: "support.account_setting_events",
        key: "#{setting.account_id}/#{setting.name}",
        value: AccountSettingProtobufEncoder.new(setting).to_proto,
        partition_key: setting.account_id.to_s
      )
    end

    def track_changes_to_default_brand(setting)
      return unless setting.account.has_publish_brand_entities?
      return unless setting.name == 'default_brand_id'
      return unless setting_changed?(setting)

      BrandPublisher.new(account_id: setting.account.id).tap do |publisher|
        publisher.publish_for_brand(brand_id: setting.value_was) # Update the previous default
        publisher.publish_for_brand(brand_id: setting.value)     # Update the new default
      end
    end

    def handle_polaris_radar_notification(setting)
      return unless setting.name == 'polaris' && notification_required?(setting)

      alert_type = setting.value == '1' ? 'polaris_on' : 'polaris_off'

      account = setting.account
      ::RadarFactory.create_radar_notification(
        account,
        "growl/account/#{account.subdomain}/polaris"
      ).send(alert_type, '')
    end

    def handle_polaris_chat_app_installation(setting)
      return unless setting.name == 'polaris' && setting.value == '1'
      client = Zendesk::Accounts::Client.new(setting.account)
      return unless client.account_from_account_service(false)
      return unless client.chat_product

      Apps::ChatAppInstallation.new(
        app_market_client: Zendesk::AppMarketClient.new(setting.account.subdomain)
      ).handle_disable_installation
    end

    def notify_chat_of_polaris_settings_changed(setting)
      return unless setting.name == 'polaris' && notification_required?(setting)
      client = Zendesk::Accounts::Client.new(setting.account)
      return unless client.account_from_account_service(false)
      return unless client.chat_product

      # Notify Chat for `polaris` settings change
      Zopim::InternalApiClient.
        new(setting.account_id).
        notify_account_settings_change('polaris_settings_changed')
    end

    def handle_email_radar_notification(setting)
      return unless setting.name == 'simplified_email_threading' && notification_required?(setting)

      notification_type = setting.value == '0' ? 'simplified_email_threading_off' : 'simplified_email_threading_on'

      account = setting.account
      ::RadarFactory.create_radar_notification(
        account,
        "growl/account/#{account.subdomain}/simplified_email_threading"
      ).send(notification_type, '')
    end

    private

    def assumption_expiration_action(setting)
      expiration_date     = setting.value.try(:to_time)
      was_expiration_date = setting.value_was.try(:to_time)
      if expiration_date == was_expiration_date
        :nochange
      elsif was_expiration_date.nil?
        expiration_date == Account::AssumptionSupport::ALWAYS_ASSUMABLE ? :permanent : :enabled
      elsif expiration_date.nil? && was_expiration_date >= Time.now
        :disabled
      elsif expiration_date == Account::AssumptionSupport::ALWAYS_ASSUMABLE
        :permanent
      elsif expiration_date && expiration_date > Time.now && was_expiration_date >= Time.now
        :change
      elsif was_expiration_date < Time.now && expiration_date && expiration_date >= Time.now
        :enabled
      elsif expiration_date.nil? && was_expiration_date < Time.now
        Rails.logger.warn("Turning off assumption on an already expired state: #{was_expiration_date}")
        nil
      else
        Rails.logger.warn("Unhandled account assumption expiration setting change from: #{was_expiration_date} to s#{expiration_date}")
        nil
      end
    end

    def setting_changed?(setting)
      setting.value != setting.value_was
    end

    def setting_notification_for(setting)
      suffix = (setting.value == '0') ? :disabled : :enabled
      :"deliver_#{setting.name}_#{suffix}"
    end

    def has_security_email_notifications_feature?(setting) # rubocop:disable Naming/PredicateName
      setting.account.has_security_email_notifications_for_account_settings?
    rescue Zendesk::Accounts::Locking::LockedException
      false
    end

    def notification_required?(setting)
      setting_changed?(setting) &&
        !(setting.value_was.blank? && setting.value.blank?) &&
        !(setting.value_was.blank? && OBSERVED_SETTINGS.include?(setting.name))
    end
  end
end
