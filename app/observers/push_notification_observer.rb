module PushNotificationObserver
  class << self
    def after_create(activity)
      devices = activity.user.device_identifiers.first_party.active
      return if devices.empty?

      I18n.with_locale(activity.user.translation_locale) do
        devices.group_by(&:mobile_app).each do |mobile_app, devices_for_app|
          devices_for_app.group_by(&:gateway).each do |gateway, devices_for_gateway|
            PushNotificationSendJob.enqueue(
              account_id: activity.account_id,
              device_ids: devices_for_gateway.collect(&:id),
              gateway: gateway,
              mobile_app_identifier: mobile_app.identifier,
              payload: push_payload(activity)
            )
          end
        end
      end
    end

    private

    def push_payload(activity)
      translation_key = "txt.activities.#{activity.verb}"

      msg = I18n.t(translation_key, activity.title_options)
      msg_short = I18n.t(translation_key + '_short', activity.title_options.slice(:ticket_nice_id, :new_priority, :actor_name))

      {
        user_id: activity.user_id,
        msg: msg,
        msg_short: msg_short,
        detail: activity.detail,
        ticket_id: activity.activity_target.nice_id
      }
    end
  end
end
