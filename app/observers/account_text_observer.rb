module AccountTextObserver
  OBSERVED_SETTINGS = %w[ip_restriction].freeze

  class << self
    def included(base)
      this = self
      base.after_save do |setting|
        this.send_security_email_notifications(setting)
        true
      end

      base.after_commit do |setting|
        this.notify_chat_of_ip_settings_changed(setting)
      end
    end

    def send_security_email_notifications(setting)
      return unless OBSERVED_SETTINGS.include?(setting.name)
      return unless has_security_email_notifications_feature?(setting)
      return unless notification_required?(setting)

      notification = setting_notification_for(setting)
      SecurityMailer.send(notification, setting, CIA.current_actor,
        value_was: setting.value_was)
    end

    def notify_chat_of_ip_settings_changed(setting)
      return unless Arturo.feature_enabled_for?(:notify_settings_change_to_zopim, setting.account)
      return unless setting.name == 'ip_restriction'

      Zopim::InternalApiClient.
        new(setting.account_id).
        notify_account_settings_change("ip_settings_changed")
    end

    private

    def setting_notification_for(setting)
      :"deliver_#{setting.name}_changed"
    end

    def has_security_email_notifications_feature?(setting) # rubocop:disable Naming/PredicateName
      setting.account.has_security_email_notifications_for_account_settings?
    rescue Zendesk::Accounts::Locking::LockedException
      false
    end

    def notification_required?(setting)
      (setting.value != setting.value_was) &&
        !(setting.value_was.blank? && setting.value.blank?)
    end
  end
end
