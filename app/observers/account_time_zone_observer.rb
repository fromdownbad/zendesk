module AccountTimeZoneObserver
  def self.included(base)
    base.after_save do |account|
      if need_to_update_time_zone?(account)
        GooddataConfigurationJob.enqueue(account.id)
      end
    end
  end

  private

  def need_to_update_time_zone?(account)
    account.time_zone_changed? &&
    account.gooddata_integration.present?
  end
end
