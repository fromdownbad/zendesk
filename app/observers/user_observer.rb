module UserObserver
  OBSERVED_FIELDS = %w[crypted_password].freeze

  class << self
    def included(base)
      this = self
      base.after_commit ->(user) { this.after_commit(user) }
    end

    def after_commit(user)
      return unless user.account # during account destruction ... test-case only
      return if user.account.pre_account_creation || user.skip_verification

      if new_user?(user)
        for_new_user(user)
      else
        for_existing_user(user)
      end
    end

    private

    def for_existing_user(user)
      deliver_profile_admin_user_added(user) if created_or_promoted_to_admin?(user)
      deliver_profile_fields_changed(user)   if user.is_active?
    end

    def for_new_user(user)
      deliver_profile_admin_user_added(user) if created_or_promoted_to_admin?(user)
    end

    def deliver_profile_admin_user_added(user)
      unless actor = CIA.current_actor
        message = "No current_actor was found, cannot audit"
        ZendeskExceptions::Logger.record(StandardError.new(message), location: self.class, message: message, reraise: !Rails.env.production?, fingerprint: 'dd82bf3b9de35dea191646d0137bc198af4b07a8')
      end

      recipients = Zendesk::Accounts::Security::Notifications.new(user.account).recipients

      if !user.account.has_email_notification_profile_admin_added_duplicates_fix?
        (recipients - [user]).group_by(&:translation_locale).each do |locale, locale_recipients|
          SecurityMailer.deliver_profile_admin_user_added_v2(user, locale_recipients.map(&:email), locale, actor)
        end

      elsif recipients.group_by(&:translation_locale).size == 1
        # If there's only one locale at play, we can deliver all in one go
        SecurityMailer.deliver_profile_admin_user_added(user, user, recipients.map(&:email) - [user.email], actor)
      else
        # If there's more locales at play we need to deliver once for each admin
        recipients.uniq.each do |recipient|
          SecurityMailer.deliver_profile_admin_user_added(user, recipient, nil, actor)
        end
      end
    end

    def deliver_profile_fields_changed(user)
      return unless user.account.settings.email_agent_when_sensitive_fields_changed?
      return unless user.send_password_change_email?
      return if password_added?(user)

      modified_fields(user).each do |field|
        SecurityMailer.send(:"deliver_profile_#{field}_changed", user, CIA.current_actor, brand_id: user.active_brand_id)
        statsd_client.increment("deliver_profile_changed", tags: [field])
      end
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['security'])
    end

    def new_user?(user)
      user.previous_changes["id"]
    end

    def password_added?(user)
      user.previous_changes["crypted_password"] && user.previous_changes["crypted_password"][0].nil?
    end

    def modified_fields(user)
      user.previous_changes.keys & OBSERVED_FIELDS
    end

    def created_or_promoted_to_admin?(user)
      user.previous_changes["roles"] && user.is_admin? && !user.is_account_owner? && user.account.owner_id
    end
  end
end
