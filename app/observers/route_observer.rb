module RouteObserver
  extend ActiveSupport::Concern

  included do
    after_save :publish_brand
    after_save :should_provision_certificate?
    after_commit :provision_certificate, on: [:create, :update], if: :should_provision_certificate
    after_commit :reload_route, if: -> { account }
  end

  attr_reader :should_provision_certificate

  private

  def publish_brand
    return unless account && account.has_publish_brand_entities?
    return unless changed?

    BrandPublisher.new(account_id: account.id).publish_for_route(route_id: id)
  end

  def provision_certificate
    return if transaction_include_any_action?([:destroy])

    if account.settings.automatic_certificate_provisioning?
      AcmeCertificateJobStatus.enqueue_with_status!(account_id)
    end
  end

  def reload_route
    account.reset_url_generator
    account.route.reload if account.route_id
  end

  def should_provision_certificate?
    Rails.logger.info "[HOST MAPPING UPDATE] account: #{account.try(:id)} route changes: #{changes}"
    @should_provision_certificate = (host_mapping.present? && changes.include?(:host_mapping))
  end
end
