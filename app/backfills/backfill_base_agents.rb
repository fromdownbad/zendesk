class BackfillBaseAgents < BackfillBase
  attr_reader :start, :batch_size, :conditions

  def initialize(options = {})
    super
    @start      = options.fetch(:start, 1)
    @batch_size = options.fetch(:batch_size, 10_000)
    @conditions = 'base_agents IS NULL OR base_agents != max_agents'
  end

  def perform
    Rails.logger.info "BackfillBaseAgents: start; dry_run: #{dry_run}"

    Subscription.where(conditions).find_in_batches(start: start, batch_size: batch_size) do |batch|
      subscription_ids = batch.map(&:id)
      Rails.logger.info "Updating 'base_agents' for subscription id #{subscription_ids.first} to #{subscription_ids.last}"

      next if dry_run
      Subscription.where(id: subscription_ids).update_all('base_agents = max_agents')
    end
  end
end
