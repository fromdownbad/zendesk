class BackfillAccountProducts < BackfillBase
  include BackfillAccountProductsQuery

  attr_reader :skip_subscribed

  def initialize(options = {})
    super(options)

    @skip_subscribed = options.fetch(:skip_subscribed, false)
    initialize_account_query_options(options)
  end

  def perform
    account_query.find_in_batches do |batch|
      batch.each do |account|
        next if skip_subscribed && account.zuora_account_id.present?

        product = Zendesk::Accounts::SupportProductMapper.derive_product(account)

        if product.nil?
          Rails.logger.info "Skipping backfill for account #{account.idsub}. No product is required."
        elsif product[:state] == :unknown
          Rails.logger.info "Skipping backfill for account #{account.idsub}. Derived product state is unknown."
        else
          Rails.logger.info "Backfilling support product record for account #{account.idsub} with #{product}"
          Zendesk::Accounts::Client.new(account).
            update_or_create_product!('support', {product: product}, include_deleted_account: true, context: "backfill_account_products") unless dry_run
        end
      end
    end
  end
end
