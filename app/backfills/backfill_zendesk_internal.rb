class UpdatableGlobalClient < Zendesk::OAuth::GlobalClient
  def readonly?
    false
  end
end

class BackfillZendeskInternal < BackfillBase
  def perform
    Rails.logger.info "BackfillZendeskInternal: start; dry_run: #{dry_run}"

    data.each do |c|
      client = UpdatableGlobalClient.find(c[:id])

      Rails.logger.info "Updating Global OAuth Client: id: #{c[:id]}, identifier: #{c[:identifier]}, zendesk_internal: #{client.zendesk_internal}"

      client.zendesk_internal = true

      return if dry_run
      client.save!
    end
  end

  private

  def data
    [
      {
        id: 13,
        identifier: 'zendesk_mobile_ios'
      },
      {
        id: 23,
        identifier: 'zendesk_mobile_android'
      },
      {
        id: 33,
        identifier: 'zendesk_mobile_wp'
      },
      {
        id: 77,
        identifier: 'zopim'
      },
      {
        id: 99,
        identifier: 'zopim_for_zendesk'
      },
      {
        id: 127,
        identifier: 'zendesk_inbox'
      },
      {
        id: 140,
        identifier: 'satisfaction_prediction_prod'
      },
      {
        id: 254,
        identifier: 'zendesk_web_widget'
      }
    ]
  end
end
