class DeleteOrphanOauthTokens < BackfillBase
  def perform
    on_shards { delete_orphan_tokens }
  end

  private

  def delete_orphan_tokens
    soft_deleted_clients.each do |client|
      orphan_tokens = client.tokens

      if dry_run
        puts "Total tokens: #{orphan_tokens.count}"
        return
      end

      orphan_tokens.delete_all
    end
  end

  def soft_deleted_clients
    @soft_deleted_clients ||= Zendesk::OAuth::Client.with_deleted do
      Zendesk::OAuth::Client.where("deleted_at IS NOT NULL").to_a
    end
  end
end
