class ReconfigureSpammyZ3nAccountSettings < BackfillBase
  attr_reader :account_id

  DEFAULT_NOTIFY_TRIGGER = 'Notify requester of received request'.freeze

  def initialize(options = {})
    @account_id = options.fetch(:account_id, nil)
    super
  end

  def perform
    Rails.logger.info("ReconfigureSpammyZ3nAccountSettings: Starting dry_run=#{dry_run}")
    account_id.present? ? run_on_account(account_id) : run_on_pod
  end

  def run_on_account(account_id)
    backfill_account(account_id)
  end

  def backfill_account(account_id)
    account = Account.find(account_id)
    return unless account.locale_id == 1 # English only for now
    return unless account.subdomain.include?('z3n') || account.name.include?('z3n')

    # Change the old trigger values of trigger "Notify requester of received request" to the updated versions
    trigger = account.triggers.find_by(title: DEFAULT_NOTIFY_TRIGGER, is_active: true)
    if should_change_trigger?(trigger)
      Rails.logger.info("ReconfigureSpammyZ3nAccountSettings: Previous: account_id:#{account_id} TriggerSubject:#{trigger.definition.actions.first.value[1]}, TriggerBody:#{trigger.definition.actions.first.value[2]}")
      unless dry_run
        trigger.definition.actions.first.value[1] = I18n.t('txt.default.triggers.notify_requester_received.subject_v3')
        trigger.definition.actions.first.value[2] = I18n.t('txt.default.triggers.notify_requester_received.body_v4')
        trigger.save!
        Rails.logger.info("ReconfigureSpammyZ3nAccountSettings: Changed: account_id:#{account_id} TriggerSubject:#{trigger.definition.actions.first.value[1]}, TriggerBody:#{trigger.definition.actions.first.value[2]}")
      end
    end

    # Enable sender auth if not already enabled
    unless account.has_email_sender_authentication?
      Rails.logger.info("ReconfigureSpammyZ3nAccountSettings: Previous: account_id:#{account_id} SenderAuth:#{account.has_email_sender_authentication?}")
      unless dry_run
        account.settings.enable(:email_sender_authentication)
        account.settings.save! && account.save!
        Rails.logger.info("ReconfigureSpammyZ3nAccountSettings: Changed: account_id:#{account_id} SenderAuth:#{account.has_email_sender_authentication?}:")
      end
    end
  end

  def run_on_pod
    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    account_ids = Account.shard_local.where('is_active is true AND is_serviceable is true').pluck(:id)
    account_ids.each do |account_id|
      begin
        backfill_account(account_id)
      rescue StandardError => exception
        Rails.logger.info("backfill_shard failed for account_id: #{account_id} with error: #{exception}")
        next
      end
    end
  end

  private

  def should_change_trigger?(trigger)
    trigger.present? &&
      (trigger.definition.actions.first.value[1] != I18n.t('txt.default.triggers.notify_requester_received.subject_v3') ||
        trigger.definition.actions.first.value[2] != I18n.t('txt.default.triggers.notify_requester_received.body_v4'))
  end
end
