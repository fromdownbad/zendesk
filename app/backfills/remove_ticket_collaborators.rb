class RemoveTicketCollaborators < BackfillBase

  # What:
  # Deletes excess end users collaboration records limiting 10000 tickets after a given time stamp.
  #
  # Why:
  # When tickets are shared via social media channels, excess end user collaborators can build up. Tickets generally have a
  # 25 collaborator limit. However, this is not enforced for social channels.
  # These slow down the incremental tickets export, resulting in a 502 error (web servers times out after 60 seconds)
  # ie. the Gooddata Insights dashboard cannot load data.

  attr_reader :account, :start_time, :end_user_collaborator_threshold
  TICKETS_LIMIT = 10_000
  END_USER_COLLABORATOR_THRESHOLD = 50

  def initialize(options = {})
    @account = Account.find_by_subdomain(options.fetch(:subdomain))
    @start_time = options.fetch(:start_time)
    super
  end

  def perform
    account.on_shard do
      remove_collaborators_in_batches
    end
  end

  private

  def remove_collaborators_in_batches
    problem_tickets.each_slice(100).each do |batch|
      batch.each do |t|
        remove_ticket_collaborators(t)
      end
    end
  end

  def tickets_after_start_time
    account.tickets.where("generated_timestamp >= '#{Time.at(start_time)}'").limit(TICKETS_LIMIT)
  end

  def problem_tickets
    @problem_tickets ||= tickets_after_start_time.select { |ticket| ticket.collaborator_ids.count > END_USER_COLLABORATOR_THRESHOLD }
  end

  def end_user_collaborators(ticket)
    ticket.collaborators.where(:roles => Role::END_USER.id).map(&:id)
  end

  def collaboration_scope(ticket)
    Collaboration.where(
      :account_id => ticket.account.id,
      :ticket_id => ticket.id,
      :user_id => end_user_collaborators(ticket)
    )
  end

  def remove_ticket_collaborators(ticket)
    begin
      scope = collaboration_scope(ticket)
      ids = scope.map(&:id)

      unless dry_run
        scope.destroy_all
      end

      log("Removed the following End User collaborators for ticket.id #{ticket.id}: \n#{ids}")
    rescue => e
      log("Failed to remove End User collaborators for ticket.id #{ticket.id} with exception:\n #{e.inspect}")
    end
  end

  def log(message)
    Rails.logger.info(
      "[#{self.class} | Pod: #{pod} | Dry Run: #{dry_run}]: #{message}"
    )
  end
end
