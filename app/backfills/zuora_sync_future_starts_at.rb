class ZuoraSyncFutureStartsAt < BackfillBase
  BATCH_SIZE = 100

  def initialize(options = {})
    @max_accounts = options[:max_accounts]
    super(options)
  end

  def perform
    Rails.logger.info("FixZuoraFutureSubscriptions: Starting (dry_run=#{dry_run}, pod=#{pod})")
    backfill
    Rails.logger.info("FixZuoraFutureSubscriptions: Done")
  end

  def backfill
    total_counter, batch_counter = 0, 0
    ZendeskBillingCore::Zuora::Subscription.where("future_subscription_starts_at IS NOT NULL").find_each do |zs|
      return if (@max_accounts.present? && @max_accounts.is_a?(Integer) && total_counter == @max_accounts)

      if batch_counter == batch_size
        batch_counter = 0
        sleep(30)
      end
      sync_account(zs)
      batch_counter += 1
      total_counter += 1
    end
  end

  def sync_account(zuora_subscription)
    Rails.logger.info("FixZuoraFutureSubscription: Starting Zuora synchronization for account #{zuora_subscription.zuora_account_id}")
    synchronizer = ZendeskBillingCore::Zuora::RemoteSynchronizerClient.new(zuora_subscription.zuora_account_id, zuora_subscription.account.subdomain)
    synchronizer.synchronize! unless dry_run
  end

  def batch_size
    BATCH_SIZE
  end
end