class FixInvalidAccountStates < BackfillBase
  def perform
    undo_soft_delete_for_prevent_deletion_if_churned
    soft_delete_active_sandboxes_with_deleted_masters
    drop_active_on_deleted_accounts
    drop_serviceable_on_inactive_unbilled_accounts
    create_missing_deletion_audits
  end

  # AccountPruningJob prevents soft_deletion of account with this flag. We need
  # to be able to move these accounts out of decomissioned pods, which is
  # impossible if they are deleted. Rollback the deleted_at.
  #
  # If they have any DataDeletionAudits, the AccountDeletionManager also check
  # this Arturo flag and will fail the audit.
  def undo_soft_delete_for_prevent_deletion_if_churned
    feature = Arturo::Feature.find_feature(:prevent_deletion_if_churned)
    Account.
      unscoped.
      without_arsi.
      where(shard_id: shards, subdomain: feature.external_beta_subdomains).
      where('deleted_at IS NOT NULL').find_in_batches do |batch|
        batch.each do |account|
          Rails.logger.info "undo_soft_delete_for_prevent_deletion_if_churned: #{account.idsub}"
        end
        Account.unscoped.where(id: batch.map(&:id)).update_all(deleted_at: nil, is_active: 0, is_serviceable: 0) unless dry_run
      end
  end

  # Deleted accounts should not be active or serviceable
  def drop_active_on_deleted_accounts
    Account.
      unscoped.
      where(shard_id: shards).
      where('(is_serviceable = 1 OR is_active = 1) AND deleted_at IS NOT NULL').find_each do |account|
        Rails.logger.info "drop_active_on_deleted_accounts: #{account.idsub}"
        Account.unscoped.where(id: account.id).update_all(is_serviceable: false, is_active: false) unless dry_run
      end
  end

  # This may be a bug in the billing code related to trials. Maybe it's a Kasket
  # issue. I haven't spotted anything that would cause this yet. These accounts
  # are not accessible (due to not being active), but aren't picked up by deletion.
  def drop_serviceable_on_inactive_unbilled_accounts
    Account.
      unscoped.
      joins('LEFT JOIN zuora_subscriptions ON account_id = accounts.id').
      where(shard_id: shards).
      where(is_serviceable: true, is_active: false).
      where('zuora_subscriptions.id IS NULL').find_each do |account|
        Rails.logger.info "drop_serviceable_on_inactive_unbilled_accounts: #{account.idsub}"
        Account.unscoped.where(id: account.id).update_all(is_serviceable: false) unless dry_run
      end
  end

  # AccountPruningJob normally soft deletes all sandboxes for an account when
  # its master account is soft deleted. These sandboxes are still live for some
  # reason.
  def soft_delete_active_sandboxes_with_deleted_masters
    Account.
      unscoped.
      joins('INNER JOIN accounts AS masters ON accounts.sandbox_master_id = masters.id').
      where('masters.deleted_at IS NOT NULL').
      where(shard_id: shards).
      where('accounts.sandbox_master_id IS NOT NULL').
      where(deleted_at: nil).find_each do |account|
        Rails.logger.info "soft_delete_active_sandboxes_with_deleted_masters: #{account.idsub}"
        Account.
          unscoped.
          joins('INNER JOIN accounts AS masters ON accounts.sandbox_master_id = masters.id').
          where(id: account.id).
          update_all('accounts.deleted_at = masters.deleted_at') unless dry_run
        end
  end

  # Anytime an account is soft deleted (deleted_at exists) , a "cancellation"
  # data_deletion_audit should be created to facilitate hard deletion. Accounts
  # without an audit are presumed to not be hard-deleted.
  #
  # Creating the audit will execute a hard-deletion by AccountPruningJob 30 days
  # after the accounts.deleted_at value.
  def create_missing_deletion_audits
    Account.
      unscoped.
      joins("left join data_deletion_audits on account_id = accounts.id and reason = 'canceled'").
      where("accounts.deleted_at IS NOT NULL").
      where("data_deletion_audits.id IS NULL").
      where(shard_id: shards).
      find_in_batches do |batch|
        batch.each do |account|
          Rails.logger.info "create_missing_deletion_audits: #{account.id}/#{account.subdomain}"
          account.data_deletion_audits << DataDeletionAudit.build_for_cancellation unless dry_run
        end
      end
  end
end
