class DisableEndUserSocialLogin < BackfillBase
  def perform
    Rails.logger.info("DisableEndUserSocialLogin: Start; dry_run:#{dry_run}")

    account_ids = []

    account_ids << RoleSettings.where("end_user_remote_login = ? AND end_user_facebook_login = ?", true, true).
      joins(:account).select { |rs| rs.account.is_active? && rs.account.is_serviceable? }.map(&:account_id)

    account_ids << RoleSettings.where("end_user_remote_login = ? AND end_user_google_login = ?", true, true).
      joins(:account).select { |rs| rs.account.is_active? && rs.account.is_serviceable? }.map(&:account_id)

    account_ids << RoleSettings.where("end_user_remote_login = ? AND end_user_twitter_login = ?", true, true).
      joins(:account).select { |rs| rs.account.is_active? && rs.account.is_serviceable? }.map(&:account_id)

    account_ids << RoleSettings.where("end_user_remote_login = ? AND end_user_office_365_login = ?", true, true).
      joins(:account).select { |rs| rs.account.is_active? && rs.account.is_serviceable? }.map(&:account_id)

    unique_account_ids = account_ids.flatten.uniq

    unique_account_ids.each do |account_id|
      account = Account.find(account_id)
      if dry_run
        Rails.logger.info("Account #{account_id} all end user social role_settings to be disabled")
      else
        Rails.logger.info("Disabling end user social logins for #{account_id}")
        account.role_settings.update_attributes(
          end_user_google_login: false,
          end_user_twitter_login: false,
          end_user_facebook_login: false,
          end_user_office_365_login: false
        )
      end
    end
  end
end
