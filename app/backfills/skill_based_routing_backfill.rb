class SkillBasedRoutingBackfill < BackfillBase
  def perform
    Rails.logger.info("SkillBasedRoutingBackfill: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    Rails.logger.info("SkillBasedRoutingBackfill: backfill shard #{ActiveRecord::Base.current_shard_id}")
    Account.shard_local.includes(:subscription).where("subscriptions.plan_type" => SubscriptionPlanType.ExtraLarge).find_each { |account| backfill_account(account) }
  end

  def backfill_account(account)
    return unless needs_backfill?(account)

    Rails.logger.info("SkillBasedRoutingBackfill: fixing account #{account.id} -- #{account.subdomain}")

    return if dry_run

    Zendesk::Features::SubscriptionFeatureService.new(account).execute!
  end

  def needs_backfill?(account)
    (account.subscription.active_features.map(&:name) & ['skill_based_attribute_ticket_mapping', 'skill_based_ticket_routing']).count < 2
  end
end
