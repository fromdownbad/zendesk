class DeleteSessions < BackfillBase
  def initialize(options = {})
    super
    @time_key = options.fetch(:time_key, 'created_at')
    @time_period = options.fetch(:period_before_now, 0.seconds)

    @remove_all = !options.key?(:time_key) && !options.key?(:period_before_now)
  end

  def perform
    on_shards { delete_sessions }
  end

  def delete_sessions
    now = Time.now
    limit = 500
    @sessions = Zendesk::SharedSession::SessionRecord

    unless @remove_all
      @sessions = @sessions.where("#{@time_key} <= ?", now - @time_period)
    end

    if dry_run
      Rails.logger.info "This would remove #{@sessions.count} sessions"
    else
      deleted_sessions = [1]
      while deleted_sessions.any?
        Rails.logger.info "Removing sessions"

        deleted_sessions = @sessions.limit(limit).destroy_all
      end
    end
  end
end
