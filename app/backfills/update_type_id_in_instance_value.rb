class UpdateTypeIdInInstanceValue < BackfillBase
  attr_reader :account_id, :rollback, :use_subtype_id

  STATUS_LIST = [StatusType.CLOSED, StatusType.ARCHIVED, StatusType.DELETED].freeze

  TICKET_TYPE = 10
  TICKET_SKILLS_CLOSED_TYPE = 11
  TICKET_CLOSED_SUBTYPE = 1

  class InstanceValue < ActiveRecord::Base
    scope :not_deleted, -> { where(deleted_at: nil) }
    scope :tickets, -> { not_deleted.where(type_id: TICKET_TYPE) }
    scope :tickets_to_rollback, -> { not_deleted.where(type_id: TICKET_SKILLS_CLOSED_TYPE) }
    scope :all_tickets, -> { not_deleted.where(type_id: [TICKET_TYPE, TICKET_SKILLS_CLOSED_TYPE]) }
  end

  def initialize(options = {})
    super
    @account_id = options.fetch(:account_id)
    @rollback = options.fetch(:rollback, false)
    @use_subtype_id = options.fetch(:use_subtype_id, false)
  end

  def perform
    Rails.logger.info("UpdateTypeIdInInstanceValue: Starting dry_run=#{dry_run}")

    account = Account.find_by(id: @account_id)

    return unless account;

    shard = account.shard_id

    ActiveRecord::Base.on_shard(shard) do
      total_registers = ActiveRecord::Base.on_slave do
        if use_subtype_id?
          all_inactive_tickets.count
        else
          !rollback? ? inactive_tickets.count : tickets_to_rollback.count
        end
      end

      Rails.logger.info "UpdateTypeIdInInstanceValue: account_id (#{@account_id}), rollback (#{@rollback}), use_subtype_id (#{use_subtype_id}): will modify #{total_registers} registers"

      return if dry_run

      ActiveRecord::Base.on_slave do
        if use_subtype_id?
          replace_subtype_id
        else
          replace_type_id
        end
      end

      Rails.logger.info "UpdateTypeIdInInstanceValue: account_id (#{@account_id}), rollback (#{@rollback}), use_subtype_id (#{use_subtype_id}): modified registers #{total_registers}"
      statsd_client.increment("backfill_completed", tags: ["subdomain:#{account.subdomain}, backfill_type:#{backfill_type}, total_registers:#{total_registers}"])
    end
  end

  private

  def inactive_tickets
    UpdateTypeIdInInstanceValue::InstanceValue.tickets.
      joins("LEFT JOIN `tickets` ON `tickets`.`id` = `instance_values`.`instance_id`").
      where("`instance_values`.`account_id` = #{@account_id} "\
        "AND (`tickets`.`status_id` IN (#{STATUS_LIST.join(', ')}) OR `tickets`.`status_id` IS NULL)")
  end

  def tickets_to_rollback
    UpdateTypeIdInInstanceValue::InstanceValue.tickets_to_rollback.
      where(account_id: @account_id)
  end

  def all_inactive_tickets
    UpdateTypeIdInInstanceValue::InstanceValue.all_tickets.
      joins("LEFT JOIN `tickets` ON `tickets`.`id` = `instance_values`.`instance_id`").
      where("`instance_values`.`account_id` = #{@account_id} AND `instance_values`.`subtype_id` = 0 "\
      "AND (`tickets`.`status_id` IN (#{STATUS_LIST.join(', ')}) OR `tickets`.`status_id` IS NULL)")
  end

  def replace_type_id
    if rollback?
      tickets_to_rollback.find_each do |instance|
        ActiveRecord::Base.on_master do
          instance.update_column :type_id, TICKET_TYPE
        end
      end
    else
      inactive_tickets.find_each do |instance|
        ActiveRecord::Base.on_master do
          instance.update_column :type_id, TICKET_SKILLS_CLOSED_TYPE
        end
      end
    end
  end

  def replace_subtype_id
    all_inactive_tickets.find_each do |instance|
      ActiveRecord::Base.on_master do
        instance.update_columns(type_id: TICKET_TYPE, subtype_id: TICKET_CLOSED_SUBTYPE)
      end
    end
  end

  def rollback?
    @rollback
  end

  def use_subtype_id?
    @use_subtype_id
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['UpdateTypeIdInInstanceValue'])
  end

  def backfill_type
    if rollback?
      'rollback'
    else
      use_subtype_id? ? 'subtype_id' : 'type_id'
    end
  end
end
