class RemoveTalkPartnerEditionAccountsBackfill < BackfillBase

  def perform
    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    Voice::PartnerEditionAccount.find_each_for_shard(batch_size: 100) { |vpea| backfill_account(vpea) }
  end

  def backfill_account(vpea)
    account = vpea.account
    return if vpea.plan_type == Voice::PartnerEditionAccount::PLAN_TYPE[:legacy]
    return if account.has_talk_cti_partner?

    puts "Removing account_id: #{account.id}, partner_edition_account_id:#{vpea.id}"
    vpea.destroy unless dry_run
  end
end
