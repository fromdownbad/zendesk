class BackfillDeleteUserSeatsForDeletedUsers < BackfillBase

  attr_reader :account_id

  def initialize(options = {})
    super
    @account_id = options.fetch(:account_id)
  end

  def perform
    account = Account.find(account_id)

    UserSeat.voice.where(account_id: account.id).each do |user_seat|

      if !user_seat.user.is_active?
        user_seat.destroy unless dry_run
        Rails.logger.info("BackfillDeleteUserSeatsForDeletedUsers: Deleted one User Seats for inactive user: #{user_seat.user.id}.")
      end
    end
  end
end
