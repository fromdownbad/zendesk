module AccountQueryable
  extend ActiveSupport::Concern

  included do
    attr_reader :start_account_id,
      :account_ids,
      :pricing_model_revision,
      :subscription_plan_type,
      :is_active,
      :is_serviceable
  end

  private

  def initialize_account_queryable_options(options = {})
    @account_ids            = Array.wrap(options.fetch(:account_ids, nil))
    @start_account_id       = options.fetch(:start_account_id, nil)
    @pricing_model_revision = options.fetch(:pricing_model_revision, nil)
    @subscription_plan_type = options.fetch(:subscription_plan_type, nil)
    @is_active              = options.fetch(:is_active, true)
    @is_serviceable         = options.fetch(:is_serviceable, true)
  end

  def account_query
    query = Account.shard_local.includes(:subscription).references(:subscription)

    return query.where(:id => account_ids) if account_ids.present?

    query = query.where(:is_active => is_active)                                            if !is_active.nil?
    query = query.where(:is_serviceable => is_serviceable)                                  if !is_serviceable.nil?
    query = query.where("accounts.id >= #{start_account_id}")                               if start_account_id
    query = query.where("subscriptions.pricing_model_revision = #{pricing_model_revision}") if pricing_model_revision
    query = query.where("subscriptions.plan_type = #{subscription_plan_type}")              if subscription_plan_type
    query
  end
end
