class DeleteAccountGoogleIdentities < BackfillBase
  # This is used to wipe out all the google profiles
  # for a specified account.
  # If an account has users that use google to signin, a
  # google profile will be created for that identity
  # and the user lists will not show their email under
  # the email column but rather the google column

  attr_reader :account_id

  def initialize(options = {})
    super
    @account_id = options.fetch(:account_id)
  end

  def perform
    account = Account.find @account_id
    return if account.role_settings.agent_google_login || account.role_settings.end_user_google_login
    puts "Found #{account_google_identities.size} records"

    account_google_identities.find_each do |identity|
      if @dry_run
        puts "AccountGoogleIdentity ##{identity.id}"
        next
      end
      identity.destroy
    end
    Rails.logger.info("DeleteAccountGoogleIdentities: Destroyed #{account_google_identities.count} google profiles. Dry Run: #{@dry_run}")
  end

  private

  def account_google_identities
    GoogleProfile.where(account_id: @account_id)
  end
end
