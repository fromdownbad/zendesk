# Helper class for enabling resumable backfills
# Usage:
#  status = BackfillStatus.new("your_backfill_#{tag}", redis)
#
#  BackfillHelper.on_all_shards_in_pod(:current) do |shard|
#    next if status.shard_processed?(shard)
#
#    do_stuff_on_shard
#
#    status.mark_shard_as_processed(shard)
#  end

class BackfillStatus
  attr_reader :backfill_name

  def initialize(backfill_name, redis)
    @backfill_name = backfill_name
    @redis = redis

    unless @redis.exists(backfill_name)
      @redis.sadd(backfill_name, 0)
      @redis.expire(backfill_name, 1.month.to_i)

      @redis.sadd("#{backfill_name}_skipped_shards", 0)
      @redis.expire("#{backfill_name}_skipped_shards", 1.month.to_i)

      @redis.sadd("#{backfill_name}_processed_accounts", 0)
      @redis.expire("#{backfill_name}_processed_accounts", 1.month.to_i)

      @redis.sadd("#{backfill_name}_skipped_accounts", 0)
      @redis.expire("#{backfill_name}_skipped_accounts", 1.month.to_i)
    end
  end

  def mark_shard_as_processed(shard)
    @redis.sadd(backfill_name, shard.to_s)
  end

  def mark_account_as_skipped(account_id)
    @redis.sadd("#{backfill_name}_skipped_accounts", account_id.to_s)
  end

  def mark_account_as_processed(account_id)
    @redis.sadd("#{backfill_name}_processed_accounts", account_id.to_s)
  end

  def mark_shard_as_skipped(shard)
    @redis.sadd("#{backfill_name}_skipped_shards", shard.to_s)
  end

  def shard_processed?(shard)
    shard.in?(processed_shards)
  end

  def account_processed?(account_id)
    account_id.to_s.in?(processed_accounts)
  end

  def status
    @redis.smembers(backfill_name)
  end

  def skipped_shards
    @redis.smembers("#{backfill_name}_skipped_shards")
  end

  def skipped_accounts
    @redis.smembers("#{backfill_name}_skipped_accounts")
  end

  def processed_accounts
    @redis.smembers("#{backfill_name}_processed_accounts")
  end

  def set_last_processed_user(account_id:, user_id:)
    @redis.set("#{backfill_name}_#{account_id}", user_id.to_s)
  end

  def last_processed_user(account_id:)
    @redis.get("#{backfill_name}_#{account_id}").to_i
  end

  def processed_shards
    status.map(&:to_i)
  end
end
