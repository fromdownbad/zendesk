require 'set'

class DeleteAccountAttachments < BackfillBase
  ATTACHMENT_BATCH_SIZE = 5000
  TICKET_BATCH_SIZE     = 1000
  LOG_SAMPLE_RATE       = 500

  # This backfill deletes attachments from a given account (snapchat)
  # during a specified time range. A list of tickets can be provided
  # to exclude associated attachments.
  def initialize(options = {})
    super
    @account = options.fetch(:account, nil)
    @start_time = options.fetch(:start_time, nil)
    @end_time = options.fetch(:end_time, nil)
    @exclusion_file = options.fetch(:exclusion_csv, nil)
    @excluded_nice_ids = options.fetch(:excluded_tickets, nil)
    @minimum_backoff = options.fetch(:minimum_backoff, 0.2).seconds
    @max_backoff = options.fetch(:max_backoff, 30).seconds
    @throttle_rate = options.fetch(:throttle_rate, 100)
    @smart_sleep_enabled = options.fetch(:smart_sleep, true)
  end


  def perform
    delete_attachments(excluded_ticket_ids)
  end
  
  private
  
  def excluded_ticket_ids
    excluded_ticket_ids = Set.new
    unless @exclusion_file
      return excluded_ticket_ids.merge(convert_ticket_ids(@excluded_nice_ids))
    else
      csv_filename = Rails.root.join('app', 'backfills', @exclusion_file)
      nice_ids = []
      
      CSV.foreach(csv_filename, headers: true) do |row|
        nice_ids << row.first.last.to_i
        # Convert the given nice_ids to ticket ids in batches
        if nice_ids.size == TICKET_BATCH_SIZE
          excluded_ticket_ids.merge(convert_ticket_ids(nice_ids))
          nice_ids.clear
        end
      end

      # Process any leftover ticket ids
      return excluded_ticket_ids.merge(convert_ticket_ids(nice_ids))
    end
  end

  def convert_ticket_ids(nice_ids)
    @account.tickets.where(nice_id: nice_ids).all_with_archived.map(&:id)
  end

  def delete_attachments(excluded_tickets)
    targeted_attachments.
      find_each(batch_size: ATTACHMENT_BATCH_SIZE).with_index do |attachment, attachment_index|
        log("#{(attachment_index.to_f/attachment_count*100).round(2)}% -- "\
            "#{attachment_index} / #{attachment_count} attachments processed") if attachment_index % LOG_SAMPLE_RATE == 0
        begin
          # Destroy attachment if attachment.ticket_id is not in exclusion set
          unless excluded_tickets.include? attachment.ticket_id
            attachment.destroy unless dry_run
            log_attachment_deletion(attachment, @account)
            throttle_deletions if attachment_index % @throttle_rate == 0
          else
            log_attachment_exclusion(attachment, @account)
          end
        rescue StandardError => e
          log_deletion_error(attachment, @account, e)
        end
    end
  end

  def targeted_attachments
    Attachment.
      where(account_id: @account.id).
      where("attachments.created_at > ?", @start_time).
      where("attachments.created_at < ?", @end_time)
  end

  def attachment_count
    @attachment_count ||= targeted_attachments.count
  end

  def throttle_deletions
    backoff_duration = [database_backoff.backoff_duration, @max_backoff].min
    if verbose
      log("[DeleteAccountAttachments] backing off deletes for #{backoff_duration} seconds") 
      log("[DeleteAccountAttachments] #{database_backoff.to_hash}")
    end
    @smart_sleep_enabled ? smart_sleep(backoff_duration) : sleep(backoff_duration)
  end
  
  def smart_sleep(duration)
    sleep(duration.remainder(1.0))
    duration.floor.times do |i|
      sleep(1.0)
      current_backoff = @database_backoff.backoff_duration
      Rails.logger.info @database_backoff.to_hash
      if current_backoff < i
        Rails.logger.info "Breaking backoff after #{i} seconds" if verbose
        break
      end
    end
  end

  def database_backoff
    @database_backoff ||= Zendesk::DatabaseBackoff.new(minimum: @minimum_backoff)
  end

  def log_attachment_deletion(attachment, account)
    statsd_client.increment("#{account.subdomain}.attachment_deletion")
  end

  def log_deletion_error(attachment, account, e)
    statsd_client.increment("#{account.subdomain}.attachment_deletion_error", tags: ["attachment_id:#{attachment.id}"])
    ZendeskExceptions::Logger.record(e, location: self, message: error_message(attachment, e))
  end

  def log_attachment_exclusion(attachment, account)
    statsd_client.increment("#{account.subdomain}.attachment_exclusion", tags: ["attachment_id:#{attachment.id}", "ticket_id:#{attachment.ticket_id}"])
  end

  def error_message(attachment, error)
    "Error in #{self.class.name.demodulize} processing attachment_id: #{attachment.id} - #{error.message}"
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['DeleteAccountAttachments'])
  end

  def log(message)
    Rails.logger.info(message)
  end
end
