class RenameDeletedOrganizations < BackfillBase
  def perform
    Rails.logger.info("RenameDeletedOrganizations: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards { backfill_shard }

    Rails.logger.info("RenameDeletedOrganizations: Finished dry_run=#{dry_run}, pod=#{pod}")
  end

  private

  def backfill_shard
    Rails.logger.info("RenameDeletedOrganizations: backfill shard #{ActiveRecord::Base.current_shard_id}")

    Organization.with_deleted do
      Organization.where("name NOT LIKE ? AND deleted_at IS NOT NULL", "%_deleted_%").find_each do |organization|
        backfill_organization(organization)
      end
    end
  end

  def backfill_organization(organization)
    Rails.logger.info("RenameDeletedOrganizations: backfill organization #{organization.id}")
    organization.name = "#{organization.id}_deleted_#{organization.name.slice(0,215)}"
    begin
      organization.save! unless dry_run
    rescue StandardError => e
      Rails.logger.debug(e.to_s)
    end
  end
end
