class DeleteCertificatesFromExpiredAccounts < BackfillBase
  def perform
    puts "DeleteCertificatesFromExpiredAccounts: Starting dry_run=#{dry_run}, pod=#{pod}"

    accounts_with_certificates = Certificate.active.pluck(:account_id)
    current_accounts = Account.where(id: accounts).pluck(:id)
    to_delete = accounts_with_certificates - current_accounts

    Certificate.active.where(account_id: to_delete).each do |cert|
      puts "Revoking cert #{cert.id}"
      cert.revoke
      cert.save!
    end
  end
end
