class BackfillCustomFieldOptionValues < BackfillBase
  # This backfill is used to refill the value column for a cfo after the arturo special_chars_in_custom_field_options is switched off
  attr_reader :account_id

  def initialize(**args)
    @account_id = args[:account_id]
    super args
  end

  def perform
    log("Starting dry_run=#{dry_run}, pod=#{pod}")

    if account_id.present?
      shard = Account.find(account_id).shard_id
      ActiveRecord::Base.on_shard(shard) { backfill_shard(shard) }
    else
      on_shards { |s| backfill_shard(s) }
    end
  end

  private

  def backfill_shard(shard)
    log("Processing shard #{shard}")

    accounts = account_id.present? ? Account.where(id: account_id) : Account

    accounts.find_each do |account|
      fix_cfo(account)
    end
  end

  def fix_cfo(account)
    CustomFieldOption.
      with_deleted { account.custom_field_options.where(value: nil) }.
      find_each do |cfo|
        process_batch(cfo)
      end
  end

  def process_batch(cfo)
    cfo.value = cfo[:enhanced_value]
    return if dry_run
    log("Failure saving option #{cfo.id}, Errors: #{cfo.errors.full_messages.join}") unless cfo.save
  end

  def log(string)
    Rails.logger.info("EnhancedValueReversal: #{string}")
  end
end
