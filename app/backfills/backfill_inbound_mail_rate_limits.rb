class BackfillInboundMailRateLimits < BackfillBase
  LEGACY_GLOBAL_RATE_LIMITS = [
    { email: "ebay@ebay.de", rate_limit: 1000, account_id: 121104, description: "ZD: 301983", is_sender: true, deleted_at: nil },
    { email: "dferry@gilt.com", rate_limit: 150, account_id: 80487, description: "ZD: 266912, the account was deleted.", is_sender: true, deleted_at: '2019-04-03 11:01:19' },
    { email: "automated@airbnb.com", rate_limit: 200, account_id: 31570, description: "1st ZD: 266526 => 100, 2nd ZD: 333083 => 200", is_sender: true, deleted_at: nil },
    { email: "no-reply@olark.com", rate_limit: 120, account_id: 23745, description: "ZD: 293394, the account was deleted.", is_sender: true, deleted_at: '2015-04-30 04:11:21' },
    { email: "femapemicfdprod@mapfre.com.mx", rate_limit: 120, account_id: 472413, description: "ZD: 945468, the account was deleted.", is_sender: true, deleted_at: '2019-03-26 11:24:34' },
    { email: "hello@delighted.com", rate_limit: 120, account_id: 547534, description: "ZD: 894501", is_sender: true, deleted_at: nil },
    { email: "dmca.support@redbubble.com", rate_limit: 2500, account_id: 558695, description: "ZD: 909015", is_sender: true, deleted_at: nil },
    { email: "auftragvestel@vangerow.de", rate_limit: 800, account_id: 191264, description: "ZD: 394276", is_sender: true, deleted_at: nil },
    { email: "support@nationalcrimecheck.com.au", rate_limit: 400, account_id: 34989, description: "ZD: 1264064", is_sender: true, deleted_at: nil },
    { email: "ticketschedule@wework.com", rate_limit: 200, account_id: 533533, description: "ZD: 3008292", is_sender: true, deleted_at: nil },
    { email: "donotreply@niagarawater.com", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-2263", is_sender: true, deleted_at: nil },
    { email: "motor@kleinschmidt.com", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-2263", is_sender: true, deleted_at: nil },
    { email: "motorcs@kleinschmidt.com'", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-2263", is_sender: true, deleted_at: nil },
    { email: "srvi2prod1@anheuser-busch.com", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-2263", is_sender: true, deleted_at: nil },
    { email: "kleinschmidt_pm@kleinschmidt.com", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-2263", is_sender: true, deleted_at: nil },
    { email: "noreply@tms.blujaysolutions.net", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-2263", is_sender: true, deleted_at: nil },
    { email: "motorkli@kleinschmidt.com", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-3360", is_sender: true, deleted_at: nil },
    { email: "motorcskli@kleinschmidt.com", rate_limit: 1000, account_id: 1470967, description: "JIRA: EM-3367", is_sender: true, deleted_at: nil },
    { email: "workdayhr@pfizer.com", rate_limit: 1000, account_id: 682888, description: "JIRA: EM-2792", is_sender: true, deleted_at: nil },
    { email: "noreply@shopee.com", rate_limit: 500, account_id: 2165099, description: "JIRA: EM-2956", is_sender: true, deleted_at: nil },
    { email: "noreply@support.lazada.co.th", rate_limit: 500, account_id: 2165099, description: "JIRA: EM-2956", is_sender: true, deleted_at: nil },
    { email: "reportingservices@15below.com", rate_limit: 400, account_id: 2126249, description: "JIRA: EM-2996, ZD: 3957552", is_sender: true, deleted_at: nil },
    { email: "reportingservices@15below.com", rate_limit: 400, account_id: 2148950, description: "JIRA: EM-2996, ZD: 3957552", is_sender: true, deleted_at: nil },
    { email: "sending@geldfuerflug.de", rate_limit: 400, account_id: 2126249, description: "JIRA: EM-2996, ZD: 3957552", is_sender: true, deleted_at: nil },
    { email: "sending@geldfuerflug.de", rate_limit: 400, account_id: 2148950, description: "JIRA: EM-2996, ZD: 3957552", is_sender: true, deleted_at: nil },
    { email: "customersupport@commercehub.com", rate_limit: 500, account_id: 1979552, description: "JIRA: EM-4105", is_sender: true, deleted_at: nil },
    { email: "automated@poco-kundendienst-b2b.zendesk.com", rate_limit: 100, account_id: 1919391, description: "JIRA: EM-2973", is_sender: false, deleted_at: nil },
    { email: "copyright@pinterest.com", rate_limit: 100, account_id: 139822, description: "ZD: 3085985", is_sender: false, deleted_at: nil },
    { email: "KD.b2b@poco.de", rate_limit: 100, account_id: 1919391, description: "JIRA: EM-2973", is_sender: false, deleted_at: nil },
    { email: "website@guildmortgage.zendesk.com", rate_limit: 200, account_id: 1147644, description: "ZD: 3096962", is_sender: false, deleted_at: nil }
  ].freeze

  def perform
    to_create = LEGACY_GLOBAL_RATE_LIMITS.each_with_object([]) do |legacy_global_rate_limit, global_rate_limit_backfill|
      existing_rate_limit = InboundMailRateLimit.with_deleted do
        InboundMailRateLimit.find_by(
          account_id: legacy_global_rate_limit[:account_id],
          is_sender: legacy_global_rate_limit[:is_sender],
          email: legacy_global_rate_limit[:email]
        )
      end

      global_rate_limit_backfill << legacy_global_rate_limit unless existing_rate_limit.present?

      global_rate_limit_backfill
    end

    Rails.logger.info("Backfill will create #{to_create.length} new entries")

    return if dry_run

    to_create.each do |rate_limit|
      begin
        inbound_mail_rate_limit = InboundMailRateLimit.new(rate_limit)
        inbound_mail_rate_limit.account_id = rate_limit[:account_id]
        inbound_mail_rate_limit.audit_message = "This inbound mail rate limit was created by the BackfillInboundMailRateLimits backfill."
        inbound_mail_rate_limit.save!
      rescue => e
        Rails.logger.info("There was an error backfilling inbound mail rate limit account_id: #{rate_limit[:account_id]}, email: #{rate_limit[:email]}: #{e.message}")
      end
    end
  end
end
