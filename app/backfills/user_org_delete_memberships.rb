class UserOrgDeleteMemberships < BackfillBase
  # This backfill deletes some memberships that should have been deleted when their organization was soft deleted
  attr_reader :account_id, :shard_id

  def initialize(**args)
    @account_id = args[:account_id]
    @shard_id = args[:shard_id]
    super args
  end

  def perform
    if account_id.present?
      backfill_account(account_id)
    elsif shard_id.present?
      backfill_shard(shard_id)
    else
      backfill_pod
    end
  end

  private

  def backfill_account(account_id)
    log("Starting run for account #{account_id}, dry_run=#{dry_run}")
    account = Account.find(account_id)
    account.on_shard do
      organization_scope = account.organizations
      fix_deleted_organization_memberships(organization_scope)
    end
    log("Finishing run for account #{account_id}, dry_run=#{dry_run}")
  end

  def backfill_shard(shard)
    log("Starting run for shard #{shard}, dry_run=#{dry_run}")
    ActiveRecord::Base.on_shard(shard) do
      organization_scope = Organization
      fix_deleted_organization_memberships(organization_scope)
    end
    log("Finishing run for shard #{shard}, dry_run=#{dry_run}")
  end

  def backfill_pod
    log("Starting run for pod, dry_run=#{dry_run}")
    on_shards do |s|
      backfill_shard(s)
    end
    log("Finishing run for pod, dry_run=#{dry_run}")
  end

  def fix_deleted_organization_memberships(organization_scope)
    return if @fix_deleted_completed
    destroy_default_memberships(organization_scope)
    delete_remaining_memberships(organization_scope)
    @fix_deleted_completed = true
  end

  # Destroying the default organization memberships will result in the users being assigned a new
  # valid default organization via callbacks
  def destroy_default_memberships(organization_scope)
    process_memberships_for_deleted_organizations(organization_scope) do |organization|
      begin
        destroyed = []
        organization.organization_memberships.where(default: true).find_each do |membership|
          membership.destroy unless dry_run
          destroyed << membership.user_id
        end
      ensure
        log_deleted(__method__, organization, destroyed)
      end
    end
  end

  def delete_remaining_memberships(organization_scope)
    process_memberships_for_deleted_organizations(organization_scope) do |organization|
      begin
        deleted = []
        organization.organization_memberships.find_each do |membership|
          membership.delete unless dry_run
          deleted << membership.user_id
        end
      ensure
        log_deleted(__method__, organization, deleted)
      end
    end
  end

  def log_deleted(method, organization, deleted)
    log("Deleted #{deleted.size} memberships for organization #{organization.id}, users: #{deleted}") if deleted.any?
  end

  def process_memberships_for_deleted_organizations(organization_scope)
    Organization.with_deleted do
      organizations = organization_scope.where("organizations.deleted_at IS NOT NULL").includes(:account).select{ |org| org&.account&.is_serviceable? }
      organizations.each_slice(1000) do |organizations|
        organizations.each do |organization|
          yield organization
        end
      end
    end
  end

  def log(string)
    Rails.logger.info("UserOrgDeleteMemberships: #{string}")
  end
end
