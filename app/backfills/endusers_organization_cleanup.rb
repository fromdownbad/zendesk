class EndusersOrganizationCleanup < BackfillBase
  # This backfill sets organization id to nil for endusers without organization memberships

  attr_reader :account_id

  def initialize(**args)
    @account_id = args[:account_id]
    super
  end

  def perform
    Rails.logger.info("EndusersOrganizationCleanup: Starting dry_run=#{dry_run}, pod=#{pod}")
    on_shards { |s| backfill_shard(s) }
  end

  private

  def backfill_shard(shard)
    scope = account_id.present? ? User.where(account_id: account_id) : User
    end_users = scope.joins('LEFT JOIN organization_memberships om ON users.id = om.user_id').where('om.id IS NULL').where('users.organization_id IS NOT NULL AND users.roles = 0')
    fixed = 0
    end_users.find_each do |end_user|
      next unless end_user.account.try(&:is_active?)
      organization_id = end_user.organization_id
      unless dry_run
        end_user.organization = nil
        saved = end_user.save # this would still trigger callbacks
      end
      if saved || dry_run
        fixed += 1
        Rails.logger.info("EndUser organization removed, user_id: #{end_user.id} organization_id: #{organization_id}")
      else
        Rails.logger.info("Could not save end user #{end_user.id}, Errors: #{end_user.errors.full_messages.join}")
      end
    end
    Rails.logger.info("Shard: #{shard}, fixed: #{fixed}")
  end
end
