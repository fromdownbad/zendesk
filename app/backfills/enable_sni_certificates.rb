class EnableSniCertificates < BackfillBase

  attr_reader :limit

  def initialize(options = {})
    @limit = options.fetch(:limit)
    super
  end

  def perform
    Rails.logger.info("EnableSniCertificates: Start; dry_run:#{dry_run}")
    Rails.logger.info("Started job, non-SNI certs count: #{non_sni_certs_count}")

    Certificate.active.where(sni_enabled: false).limit(limit).each do |c|
      Rails.logger.info("enabling SNI on certificate: #{c.id} from account: #{c.account_id}")
      c.convert_to_sni! unless dry_run
    end
  end

  private

  def non_sni_certs_count
    Certificate.active.where(sni_enabled: false).count
  end
end
