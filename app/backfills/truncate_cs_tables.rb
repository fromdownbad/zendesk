class TruncateCsTables < BackfillBase

  LIMIT = 1000.freeze

  def initialize(options = {})
    super
  end

  def perform
    Rails.logger.info("TruncateCsTables: Starting dry_run=#{dry_run}, pods=#{pods.join(', ')}")

    # added protection, go through the PR process to update date if you need to run again
    if Time.now > Time.parse("Sep 1 2020")
      raise("running truncation after GA date")
    end

    on_shards { truncate_tables }
    nil
  end

  private

  def truncate_tables
    %w(cs_articles cs_guide_aggregations_article_viewed_hourly).each do |tablename|
      total_records = get_num_records(tablename)
      Rails.logger.info("TruncateCsTables: removing #{total_records} records from #{tablename} on shard #{ActiveRecord::Base.current_shard_id}")
      num_removed = 0
      ((total_records / 1000) + 1).times do
        remove_records(tablename) unless dry_run
        num_removed += 1000
        Rails.logger.info("TruncateCsTables: removed (#{num_removed}/#{total_records}) records from #{tablename} on shard #{ActiveRecord::Base.current_shard_id}")
        sleep 1
      end
    end
  end

  def get_num_records(tablename)
    ActiveRecord::Base.connection.select_value("SELECT COUNT(1) FROM #{tablename}")
  end

  def remove_records(tablename)
    sql = "DELETE FROM #{tablename} LIMIT #{LIMIT}"
    ActiveRecord::Base.connection.execute(sql)
  end

end
