class DeleteOrganizationTicketStats < BackfillBase

  ##
  # This backfill will delete all organization ticket stats on all shards or
  # specified shards for specified pod.
  #
  # All rate controlling logic used has been taken from:
  # https://github.com/zendesk/zendesk/blob/master/lib/zendesk/archive/archiver.rb
  # https://github.com/zendesk/zendesk/blob/master/app/models/jobs/ticket_field_entry_delete_job.rb
  ##

  def initialize(options = {})
    super(options)
    @batch_size = options.fetch(:batch_size, 500)
    @batch_minimum_sleep = options.fetch(:batch_minimum_sleep, 1)
  end

  # Perform backfill for shards specified or all shards on pod
  def perform
    log("begin backfill dry_run=#{dry_run}, pod=#{pod}, shards=#{shards}")

    on_shards { delete_organization_ticket_stats }
  end

  private

  # Delete all organization ticket stats on shard in batches
  def delete_organization_ticket_stats
    log("begin backfill dry_run=#{dry_run}, shard #{current_shard_id}")

    slave_delay = ZendeskDatabaseSupport::SlaveDelay.new

    total_deleted = 0
    organization_ticket_stats.find_in_batches(batch_size: @batch_size) do |batch|
      StatRollup::Ticket.where(id: batch).delete_all unless dry_run

      total_deleted += batch.size
      log("deleted=#{!dry_run} organization ticket stats with id in #{batch.first.id}-#{batch.last.id}, total_deleted=#{total_deleted}")

      Kernel.sleep [@batch_minimum_sleep, slave_delay.max_slave_delay(current_shard_id) || 0].max
    end
  end

  # Organization ticket stats
  def organization_ticket_stats
    StatRollup::Ticket.where('organization_id != 0').select('id')
  end

  # Returns the current shard being backfilled
  def current_shard_id
    ActiveRecord::Base.current_shard_id.to_i
  end

  def log(m)
    Rails.logger.info("#{self.class.name}: #{m}")
  end

end
