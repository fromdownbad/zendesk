class HcBrandsBackfill < BackfillBase
  def initialize(options = {})
    super
    @batch_size = options.fetch(:batch_size, 500)
  end

  def perform_on_pod
    on_shards do
      Account.shard_local.select(:id).find_in_batches(batch_size: @batch_size) do |accounts|
        perform(account_ids: accounts.map(&:id))
      end
    end
  end

  def perform_for_accounts(account_ids:)
    perform(account_ids: account_ids)
  end

  def perform_for_account(account_id)
    perform(account_id: account_id)
  end

  private

  def perform(account_id: nil, account_ids: [account_id])
    log_info("Starting. dry_run=#{dry_run}")
    account_ids.each do |account_id|
      log_info("Publishing brands for account ID: #{account_id}")
      BrandPublisher.new(account_id: account_id).publish_for_account unless dry_run
    end
  end
end
