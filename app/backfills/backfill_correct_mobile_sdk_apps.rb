class BackfillCorrectMobileSdkApps < BackfillBase
  def perform
    impacted_accounts_with_corrections.each do |account_id, corrections|
      corrections.each do |current_mobile_app_id, new_mobile_app_id|
        correct_mobile_sdk_app(account_id, current_mobile_app_id, new_mobile_app_id)
      end
    end

    nil
  end

  def correct_mobile_sdk_app(account_id, current_mobile_app_id, new_mobile_app_id)
    if @dry_run
      Kernel.puts "Account #{account_id} MobileSdkApp identifier: '#{current_mobile_app_id}' gonna be corrected with: '#{new_mobile_app_id}'"
    else
      account = Account.where(id: account_id).first
      if account
        Rails.logger.info("BackfillCorrectMobileSdkApps: Account: #{account_id} found.")
        # In test, this method does not seem to be available.
        # In production it should be there. I consider it safe to do this anyways as the backfill will just fail.
        account.shard if account.respond_to?(:shard)
      else
        Rails.logger.info("BackfillCorrectMobileSdkApps: Account: #{account_id} not found.")
        return
      end

      mobile_sdk_app = account.mobile_sdk_apps.where(identifier: current_mobile_app_id).first
      if mobile_sdk_app
        Rails.logger.info("BackfillCorrectMobileSdkApps: MobileSdkApp: #{current_mobile_app_id} found.")
      else
        Rails.logger.info("BackfillCorrectMobileSdkApps: MobileSdkApp: #{current_mobile_app_id} not found.")
        return
      end

      Rails.logger.info("BackfillCorrectMobileSdkApps: Updating: MobileSdkApp: #{mobile_sdk_app.id} identifier from '#{mobile_sdk_app.identifier}' to '#{new_mobile_app_id}'")
      mobile_sdk_app.update_attribute(:identifier, new_mobile_app_id)
    end
  end

  def impacted_accounts_with_corrections
    {
      1479612 => { "mobile_sdk_client_d7d0667f47cc4c52f76b" => "5774dc4374e2960cae4e0710fa3c81f839e70eb7af1bba26" },
      110412 => { "mobile_sdk_client_25dcc0198427f932d689" => "fdbd40f4e95ceb82172e339328203b758526abd593418eb6" },
      1106236 => { "mobile_sdk_client_c4ff4bf9ac9e5c7adf11" => "ed008ab50b1ef360d73dbf1323e32dcad63437c43d70f57b" },
      1487623 => { "mobile_sdk_client_b3c81f7d476ba35e6dd5" => "b87393af85caa8f793dab46ea54cf3937285f58f357fba23" },
      582520 => { "mobile_sdk_client_a5c8c6cffe836a1c8797" => "7aaa6a5e0ae3851b548096661fa61161851d703066c2c6a4" },
      1507740 => { "mobile_sdk_client_fa3a561fb9452a184630" => "d47fe0207e7e660fbd7541d822ddbbfa95deeccd07eba9d2" },
      1056936 => { "mobile_sdk_client_671e4dbfd63df63b72cb" => "4b82ee143d6dcb9241718cfbd4f9f1bda39f4ac1e1b681e3" },
      1492450 => { "mobile_sdk_client_54cd2b7a7200062f833b" => "4b4432c2561b8358ee42a3950e06fb5e1f89e215ce90d5c1" },
      230759 => { "mobile_sdk_client_5fdec53c92e090765a81" => "a88b075625ce4b7b4b67c821cb559dc87321dcb4762afeff" },
      1519879 => { "mobile_sdk_client_43dafe6870b0d2eac6cb" => "ac88646f9f5f1a841049338bbc88a96a6b360293e2744804" },
      1498409 => { "mobile_sdk_client_c12404250af089b2698a" => "b98f90b097683682c78bea523b3d65321c88cc8e5d229e58" },
      1455484 => { "mobile_sdk_client_145287812c80e4b28583" => "3e85e84bb188882f0206ad907bfd7439eeceb475ffb21522" },
      1204184 => { "mobile_sdk_client_16d72f045a425f8757a3" => "0c8c069476fb2fb5d0408e15ce7c50808ba77a36d07972f9" },
      1521493 => { "mobile_sdk_client_270a31c76efb5dfeb923" => "12fca9d9ed02f1964d248ab8de2ec35961d5c0283342bb31" },
      1524212 => { "mobile_sdk_client_81f60a9d633cc33a2c94" => "5f2458d3178a91cb7464daab57c00dd1eb0c2d90631c4f18" },
      1204177 => { "mobile_sdk_client_100b7323dd556688b8ec" => "80335b153c2723258e632444f865d1e043e4b997934c2933" },
      1380624 => { "mobile_sdk_client_3e927d2a805f484dd78e" => "42a2819634c7aa164d262b959c7430f611ba44c3e6820c67" },
      697501 => { "mobile_sdk_client_0401deb0cffc081d0763" => "258eaca48337cf46af7410b6b200963d10b768bf2ccaace1" },
      1508338 => { "mobile_sdk_client_4575d936361a80330318" => "6c8ef3343585cd7e33f3424442c29b47d05186303a2a0595" },
      1091629 => {
        "mobile_sdk_client_4ec99b540fce03d9ec18" => "209848aa661982c92b57f7c84e67f96b12fb9def61eec38e",
        "mobile_sdk_client_ba0ef3d80f66d9bb1f6e" => "0ca49318969f24f2ebfed27b2d7b39a646a4f002641d2eb8",
        "mobile_sdk_client_dae69de06c3187aed941" => "8cc3a7fc6c390a3b18205c9c9f2dcacb6687d81ecffc51e3"
      },
      1490710 => {
        "mobile_sdk_client_0a3ab152c2ae8efaf631" => "a66416a7cdf8089818d00b27f5548fe62b4f7b33b604945d",
        "mobile_sdk_client_14a1f03a5947ccdb3f55" => "85fae5c8dba9ae66ea30628eb139427ff477c01251ea44dc"
      },
      1505463 => { "mobile_sdk_client_05d60e5a43052af60499" => "3b83cfcd5fdd76eb35d5db1e7624a2c03b46db1dd5d5b58a" },
      376075 => {
        "mobile_sdk_client_f641a4d1f6c982e8e65c" => "bf7153f4bdf2df7f34aab2c52cd4e36965e9693334a9bd2f",
        "mobile_sdk_client_48e65d3dec4aaac69970" => "5154ad681da463f7d0589e5852ce08b58a802723f1d1e616"
      },
      1517678 => { "mobile_sdk_client_5dc05ce12f4c3030ea54" => "ad6f626449ed94a3862081d97c95b41c571175fe30bbe55a" },
      464093 => { "mobile_sdk_client_83e8b71804261b997639" => "24cac6d962f2215558a0aafd9527259fddcd4930689c82e6" },
      186784 => {
        "mobile_sdk_client_20ff0423411671167d0c" => "517e2937154259b47aff325ddb3777491d48abb851bc847c",
        "mobile_sdk_client_aecfe23a7a084375aeab" => "898e5d842bcd7f7953761bb6ac07785266946fcae3339cfc"
      }
    }
  end
end
