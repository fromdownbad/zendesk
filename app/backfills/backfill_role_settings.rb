class BackfillRoleSettings < BackfillBase
  def perform
    puts "BackfillRoleSettings: Starting dry_run=#{dry_run}, pod=#{pod}"

    Account.pod_local.shard_unlocked.where(subdomain: feature_subdomains).where('is_active = ? OR is_serviceable = ?', 1, 1).includes(:role_settings).find_each do |account|
      work(account)
    end
  end

  def feature_subdomains
    Arturo::Feature.where(symbol: 'access_token_sso_bypass').first.external_beta_subdomains
  end

  def work(account)
    role_settings = account.role_settings
    return if !account.has_access_token_sso_bypass? || role_settings.nil?

    if !account.settings.sso_bypass?
      Rails.logger.info("Disabling agent_remote_bypass for #{account.id}")
      role_settings.agent_remote_bypass = RoleSettings::REMOTE_BYPASS_DISABLED
    end

    Rails.logger.info("Disabling agent_password_allowed for #{account.id}")
    role_settings.agent_password_allowed = false

    if !role_settings.end_user_zendesk_login?
      Rails.logger.info("Disabling end_user_password_allowed for #{account.id}")
      role_settings.end_user_password_allowed = false
    end

    if role_settings.changed?
      Rails.logger.info("Changes for account #{account.id}: #{role_settings.changes}")
      return if dry_run
      unless role_settings.save(validate: false)
        puts "failed to save account:#{account.id} errors: #{role_settings.errors.full_messages.inspect}"
      end
    end
  end
end
