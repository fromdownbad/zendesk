class BackfillExpireOldOauthTokens < BackfillBase
  def perform
    Rails.logger.info("ExpireOldOauthTokens: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards { backfill_shard }

    Rails.logger.info("ExpireOldOauthTokens: Finished dry_run=#{dry_run}, pod=#{pod}")
  end

  private

  def backfill_shard
    Rails.logger.info("ExpireOldOauthTokens: backfill shard #{ActiveRecord::Base.current_shard_id}")

    # Zendesk::OAuth::Token includes a default_scope here filtering out expired tokens
    while (invalid_token_ids = Zendesk::OAuth::Token.where("created_at < ?", Time.parse('2016-11-01')).limit(1000).pluck(:id)) && invalid_token_ids.count > 0
      Zendesk::OAuth::Token.where(id: invalid_token_ids).without_arsi.update_all(expires_at: (Time.now - 1.day)) unless dry_run
      Rails.logger.info("ExpireOldOauthTokens: delete #{invalid_token_ids.count} tokens #{invalid_token_ids}")
    end
  end
end
