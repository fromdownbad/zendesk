class FacebookPsidMigrateToExternalId < BackfillBase
  attr_reader :account_id, :shard_number

  OUTPUT_FILE = 'fb_psid_profile_fix_error_log.json'.freeze

  MYSQL_BATCH_SIZE = 1000

  def initialize(options = {})
    super
    @account_id = options.fetch(:account_id, nil)
  end

  def perform
    log_info("facebookpsidmigratetoexternalid: starting dry_run=#{dry_run}")
    account_id.present? ? run_on_account(account_id) : run_on_pod
  end

  private

  def run_on_account(account_id)
    backfill_account(account_id)
  end

  def run_on_pod
    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    log_to_stdout_file("FacebookPsidMigrateToExternalId: backfill shard #{ActiveRecord::Base.current_shard_id}")

    accounts = Account.shard_local.pluck(:id)
    fbidentities = UserFacebookIdentity.where(account_id: accounts)
    fbaccounts = accounts & fbidentities.map(&:account_id)
    fbaccounts.each { |account_id| backfill_account(account_id) }
  end

  def backfill_account(account_id)
    log_to_stdout_file("FacebookPsidMigrateToExternalId: fixing account #{account_id}")

    # Get pages for the Account.
    pages = Facebook::Page.where(account_id: account_id).active

    pages.each do |page|
      # identify the profiles with the external_id like "page.id-"
      ActiveRecord::Base.on_slave { Channels::FacebookUserProfile.where(account_id: account_id).where('external_id Like ?', "#{page.id}-%") }.
        find_in_batches(batch_size: MYSQL_BATCH_SIZE) do |user_profiles|
        user_profiles.each do |p|
          begin
            Channels::FacebookUserProfile.transaction do |_t|
              # delete duplicate FacebookUserProfile
              Channels::FacebookUserProfile.where(account_id: account_id, external_id: p.metadata['page_scoped_id']).destroy_all unless dry_run

              # Update User Identity value
              UserFacebookIdentity.where(
                  account_id: account_id,
                  value: p.external_id
                ).each { |i| dry_run ? log_update(i.class.name, i.id) : i.update_attribute(:value, p.metadata['page_scoped_id']) }

              # update User Profile External Id
              dry_run ? log_update(p.class.name, p.id) : p.update_attribute(:external_id, p.metadata['page_scoped_id'])
            end
          rescue Exception => exception # rubocop:disable Lint/RescueException
            log_update_failure(account_id, p.id, exception)
            next
          end
        end
      end
    end
  end

  def log_update_failure(account_id, fb_user_profile_id, exception)
    output = {}
    output[:account_id] = account_id
    output[:fb_user_external_id] = fb_user_profile_id
    output[:error] = exception.to_s
    log_info("FacebookUserProfile.external_id could not be updated for Account ID #{account_id} / FacebookUserProfile.external_id: #{fb_user_profile_id}")
    log_info(exception.backtrace)

    write_to_file(output.to_json)
    output.clear
  end

  def log_update(entity, id)
    log_info("#{entity} ID: #{id} will be updated")
  end

  def write_to_file(output)
    File.open(output_path, "a") do |file|
      file << "#{output}\n"
    end
  end

  def output_path
    @output_path = File.join(ENV['HOME'], OUTPUT_FILE)
  end

  def log_to_stdout_file(output)
    Rails.logger.info(output)
    write_to_file(output)
  end
end
