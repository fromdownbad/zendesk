class TagAffectedTickets < BackfillBase
  attr_reader :account, :ticket_ids, :identification_tag
  def initialize(**args)
    @account = Account.find(args[:account_id])
    @ticket_ids = args[:ticket_ids]
    @identification_tag = args[:identification_tag]
    super
  end

  def perform
    Rails.logger.info("TagAffectedTickets: Starting dry_run=#{dry_run}, account=#{account.subdomain}")
    account.on_shard do
      ticket_ids.in_groups_of(200, false) do |ticket_id_group|
        account.tickets.where(nice_id: ticket_id_group).find_each do |ticket|
          handle_closed_ticket(ticket) do
            ticket.disable_triggers = true
            ticket.will_be_saved_by(User.system)
            ticket.additional_tags = identification_tag
            Rails.logger.info("TagAffectedTickets: tagging dry_run=#{dry_run}, ticket=#{ticket.nice_id}, tags=#{ticket.current_tags}")
            ticket.save! unless dry_run
          end
        end
      end
    end
  end

  def handle_closed_ticket(ticket)
    if ticket.status_id == StatusType.CLOSED && !dry_run
      silent_status_update(StatusType.SOLVED, ticket)
      yield
      silent_status_update(StatusType.CLOSED, ticket)
    else
      yield
    end
  end

  def silent_status_update(new_status, ticket)
    old_status = ticket.status_id
    if old_status != new_status
      ticket.will_be_saved_by(User.system)
      # silently update the status without mucking with the status_updated_at and therefore triggering SLAs
      Ticket.where(status_id: old_status, id: ticket.id).update_all(status_id: new_status)
      status_change = Change.new(ticket: ticket, author_id: User.system.id, is_public: false, via_id: ViaType.BATCH,
        value_reference: "status_id", value_previous: old_status, value: new_status)
      ticket.create_silent_change_event(status_change)
      ticket.reload
    end
  end
end
