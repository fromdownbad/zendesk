require 'guide_plans'
require 'uuidtools'
require 'zendesk_database_support/sensors/aurora_cpu_sensor'

class BackfillTicketTags < BackfillBase
  attr_reader :account_id, :shard_id

  def initialize(options = {})
    @dry_run = options.fetch(:dry_run, true)
    @account_id = options.fetch(:account_id, nil)
    @shard_id = options.fetch(:shard_id, nil)
    super
  end

  def perform
    if account_id.present?
      update_ticket_for_account(account_id)
    else
      backfill_pod
    end
  end

  private

  def backfill_pod
    log("Starting run for pod, dry_run=#{dry_run}")
    on_shards do
      accounts_for_backfill = accounts_to_backfill
      next if accounts_for_backfill.nil?
      accounts_to_backfill.each do |account_id|
        update_ticket_for_account(account_id)
      end
    end
    log("Finishing run for pod, dry_run=#{dry_run}")
  end

# Get list of account ids with activation record in signals
  def activated_signals_accounts_query
    <<-SQL
      SELECT account_id
      FROM gs_snapshots gs
      WHERE gs.aggregate_type = "content_cue_activation"
    SQL
  end

  def activated_accounts_in_signals
    ActiveRecord::Base.connection.execute(activated_signals_accounts_query)
  end

# Check each ID is active and servicable
# Check guide is active and content cues available
# Add to array of account_ids if conditions are met
  def accounts_to_backfill
    actual_accounts = []
    activated_accounts_in_signals.each do |account_id|
      acc_client = Zendesk::Accounts::Client.new(account_id.first)
      guide_plan = acc_client.product("guide")
      account_data = acc_client.account_from_account_service(true)

      actual_accounts.append(account_id.first) if active_and_servicable?(account_data) &&
        guide_and_content_cues?(guide_plan)
    end
    actual_accounts
  end

  def update_ticket_for_account(account_id)
    snapshots = visible_snapshots_for_account(account_id)
    snapshots.each do |snapshot|
      ticket_nice_ids = JSON.parse(snapshot.first).fetch("related_ticket_tuples").map { |tuple| tuple[0] }
      uuid = UUIDTools::UUID.parse_raw(snapshot.second).to_s
      tag = "content_cue_#{uuid}"
      tickets = Ticket.where(account_id: account_id, nice_id: ticket_nice_ids)
      next if dry_run
      tickets.each do |ticket|
        next if ticket.current_tags.nil? ? false : ticket.current_tags.include?(tag)
        log("tagging tickets: #{ticket_nice_ids} with tag: #{tag}")
        ticket.will_be_saved_by(User.system, disable_triggers: true)
        ticket.additional_tags = tag
        back_off unless (database_sensor.cpu_utilization.to_f / 100) < 0.5
        ticket.save
        sleep 1
      end
    end
  end

  def visible_snapshots_for_account_query(account_id)
    <<-SQL
      SELECT data, aggregate_id
      FROM gs_snapshots gs
      WHERE gs.aggregate_type = "content_cue" AND gs.account_id = #{account_id} AND gs.visible = 1
    SQL
  end

  def visible_snapshots_for_account(account_id)
    ActiveRecord::Base.connection.execute(visible_snapshots_for_account_query(account_id))
  end

  def active_and_servicable?(account_data)
    return false unless account_data["is_active"] && account_data["is_serviceable"]
    true
  end

  def guide_and_content_cues?(guide_plan)
    return false unless guide_plan.active? && content_cues?(guide_plan.plan_settings["plan_type"])
    true
  end

  def content_cues?(guide_plan)
    GuidePlans::Features.features(plan_type: Integer(guide_plan)).
      include?(:content_cues)
  end

  def back_off
    sleep_inc = 1.2
    sleep_time = 1
    while database_sensor.cpu_utilization.to_f / 100 > 0.5
      log("high CPU utilization, sleeping for #{sleep_time} seconds before retry")
      sleep sleep_time
      sleep_time *= sleep_inc
    end
  end

  def database_sensor
    @database_sensor ||= ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.new
  end

  def log(string)
    Rails.logger.info("BackfillTicketTags: #{string}")
  end
end
