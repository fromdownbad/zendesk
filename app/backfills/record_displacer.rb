class RecordDisplacer < BackfillBase
  # Initially created for https://support.zendesk.com/agent/tickets/3265955
  # An infinite number of records may be created at one timestamp, breaking the api.
  # This backfill ameliorates the issue by spreading them out over time
  LIMIT_PER_TIMESTAMP = 1000

  attr_reader :account_id, :timestamp, :limit_per_timestamp

  def initialize(**args)
    @account_id = args[:account_id]
    @timestamp = args[:timestamp]
    @klass = args[:klass]
    @time_attribute = args[:time_attribute] || :generated_timestamp
    @limit_per_timestamp = args[:limit_per_timestamp] || LIMIT_PER_TIMESTAMP
    super args
  end

  # RecordDisplacer.new(dry_run: true, account_id: account.id, timestamp: 1514817000, klass: TicketMetric::Event, time_attribute: :time).perform
  # RecordDisplacer.new(dry_run: true, account_id: 209418, timestamp: 1487158960, klass: TicketArchiveStub).perform
  def perform
    log("Starting dry_run=#{dry_run}, pod=#{pod}")

    Account.find(account_id).on_shard do
      count = records_at(timestamp).size
      next_timestamp = timestamp + 1

      log("Found #{count} #{@klass} records at time #{timestamp}")

      while count > limit_per_timestamp
        records_at_next_timestamp = records_at(next_timestamp).size

        available_at_next_timestamp = limit_per_timestamp - records_at_next_timestamp
        remaining_events_to_move = count - limit_per_timestamp
        number_to_update = [available_at_next_timestamp, remaining_events_to_move].min

        number_updated = update_records(number_to_update, Time.at(next_timestamp))

        count -= number_updated
        next_timestamp += 1
      end
      remaining_count = records_at(timestamp).size
      log("#{remaining_count} #{@klass} at time #{timestamp}")
      log("#{@klass} #{dry_run ? 'will be' : 'were'} spread until time #{next_timestamp}")
    end
  end

  def too_klassy
    # Find all timestamps with too many #{klass} on a given acct
    freq = @klass.where(account_id: account_id).group(@time_attribute).count
    ts = {}
    # convert to timestamps
    freq.each { |date, count| ts[date.to_i] = count if count > limit_per_timestamp }
    ts
  end

  private

  def records_at(time)
    @klass.where(Hash[:account_id, account_id, @time_attribute, Time.at(time)])
  end

  def update_records(number, time)
    return 0 unless number > 0
    return number if dry_run
    records_at(timestamp).limit(number).update_all(Hash[@time_attribute, time])
  end

  def log(string)
    Rails.logger.info("RecordDisplacer: #{string}")
  end
end
