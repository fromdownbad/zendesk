class BackfillNilTicketOrganization < BackfillBase

  ###
  # This backfill will find all unclosed tickets in an account which have nil
  # organization_id, but where the ticket requester does have an organization,
  # and updates the ticket to have the same organization.
  #
  # It is assumed that the user-org relationships are correct, however an issue
  # which causes this issue may also cause user-org relationships to be in a
  # bad state. You may want to run UserOrganizations backfill first.
  ###

  def initialize(**args)
    @account_id = args[:account_id]
    @shard_id = args[:shard_id]
    @update_closed = args[:update_closed]
    @start_time = args[:start_time]
    super
  end

  def perform
    if @account_id
      backfill_account(@account_id)
    elsif @shard_id
      backfill_shard(@shard_id)
    else
      backfill_pod
    end
  end

  private

  def backfill_pod
    log("Starting run for pod, dry_run=#{dry_run}")
    on_shards do |s|
      backfill_shard(s)
    end
    log("Finishing run for pod, dry_run=#{dry_run}")
  end

  def backfill_shard(s)
    log("Starting run for shard #{s}, dry_run=#{dry_run}")

    ActiveRecord::Base.on_shard(s) do
      scope = @update_closed ? Ticket : Ticket.not_closed
      tickets = get_tickets(scope)
      backfill_tickets(tickets)
    end

  log("Finishing run for shard #{s}, dry_run=#{dry_run}")
  end

  def backfill_account(account_id)
    account = Account.find(account_id)

    account.on_shard do
      log("Starting run for account #{account.id}, dry_run=#{dry_run}")

      scope = @update_closed ? account.tickets : account.tickets.not_closed
      tickets = get_tickets(scope)
      backfill_tickets(tickets)
    end

    log("Finished run for account #{account.id}")
  end

  def backfill_tickets(tickets)
    log("Found #{tickets.size} tickets")

    tickets.find_each do |ticket|
      log("Found ticket #{ticket.id}")
      next if dry_run

      requester_org = ticket.requester.organization_id
      ticket.update_column(:organization_id, requester_org)
      log("Updated ticket #{ticket.id} with organization #{requester_org}")
    end
  end

  def get_tickets(scope)
    scope.where(tickets: { organization_id: nil, created_at: (@start_time || Time.at(0))..Time.now }).joins(:requester).where('users.organization_id IS NOT NULL')
  end

  def log(string)
    Rails.logger.info("BackfillNilTicketOrganization: #{string}")
  end
end
