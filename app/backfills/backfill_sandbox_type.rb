class BackfillSandboxType < BackfillBase
  attr_reader :account_id, :sandbox_type

  def initialize(options = {})
    @account_id = options.fetch(:account_id, nil)
    @sandbox_type = options.fetch(:sandbox_type, nil)
    super
  end

  def perform
    Rails.logger.info("BackfillSandboxType: Start; dry_run:#{dry_run}")

    sandbox = Account.find(account_id)
    raise "Account must be a sandbox" unless sandbox.is_sandbox?
    raise "#{sandbox_type} is not a valid sandbox_type" unless ::Accounts::Sandbox::SANDBOX_TYPES.include?(sandbox_type)

    sandbox.on_shard do
      if dry_run
        Rails.logger.info("Sandbox Account: #{sandbox.id} setting sandbox type to #{sandbox_type}")
      else
        sandbox.settings.sandbox_type = sandbox_type
        sandbox.settings.save!
        Rails.logger.info("Sandbox Account: #{sandbox.id} setting sandbox type to #{sandbox_type}")
      end
    end
  end
end
