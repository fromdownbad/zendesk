class BackfillRemoteAuthenticationPriorities < BackfillBase
  def perform
    Rails.logger.info("BackfillRemoteAuthPriorities: Starting dry_run=#{dry_run}, pod=#{pod}")
    on_shards { backfill_shard }
    Rails.logger.info("BackfillRemoteAuthPriorities: Finished dry_run=#{dry_run}, pod=#{pod}")
  end

  private

  def backfill_shard
    Rails.logger.info("BackfillRemoteAuthPriorities: backfill shard #{ActiveRecord::Base.current_shard_id}")
    remote_auths = RemoteAuthentication.where('priority > 1')
    Rails.logger.info("Total count for RemoteAuthPriorities > 1=#{remote_auths.count}")
    Rails.logger.info("Total count for RemoteAuthPriorities for an account =#{remote_auths.distinct.count(:account_id)}")
    if remote_auths.count == remote_auths.distinct.count(:account_id)
      remote_auths.update_all(priority: 1) unless dry_run
      Rails.logger.info("BackfillRemoteAuthPriorities: Successfully corrected #{remote_auths.count} records")
    end
  end
end
