class TicketSubmitter < BackfillBase
  # This backfill updates invalid ticket.submitters where the submitter doesn't belong to the customer's account
  attr_reader :account_id, :submitter_id

  def initialize(**args)
    @account_id = args[:account_id]
    @submitter_id = args[:submitter_id]
    super args
  end

  def perform
    log("Starting dry_run=#{dry_run}, pod=#{pod}")
    account = Account.find(@account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      submitter = account.users.find(submitter_id)
      log("On shard: #{account.shard_id}")
      tickets = tickets_with_invalid_submitter_ids_for(account)
      process(tickets, submitter)
    end
  end

  private


  def tickets_with_invalid_submitter_ids_for(account)
    account.tickets.not_closed.includes(:submitter)
      .joins('INNER JOIN users submitters ON tickets.submitter_id = submitters.id')
      .where('submitters.account_id != tickets.account_id')
  end

  def process(tickets, submitter)
    log("Changing submitter ids to #{submitter_id} for account #{account_id}")
    incorrect_accounts_count = Hash.new(0)
    modified_tickets = Hash.new
    tickets.find_each do |ticket|
      submitter_account_id = ticket.submitter.account_id
      if submitter_account_id != account_id
        modified_tickets[ticket.id] = [submitter_account_id, ticket.submitter.id]
        incorrect_accounts_count[submitter_account_id] += 1
        ticket.update_column(:submitter_id, submitter.id) unless dry_run
      else
        log("skipping ticket #{ticket.id}, submitter.account_id is equal to account_id")
      end
    end
  ensure
    if modified_tickets.any?
      log('Dumping old { ticket_id => [ticket.submitter.account_id, ticket.submitter.id] }:')
      log(modified_tickets)
    end
    incorrect_accounts_count.each do |incorrect_account_id, count|
      incorrect_account = Account.find(incorrect_account_id)
      log("Updated #{count} incorrect submitters who belonged to account:")
      log(incorrect_account.inspect)
    end
  end

  def log(string)
    Rails.logger.info("TicketSubmitterBackfill: #{string}")
  end
end
