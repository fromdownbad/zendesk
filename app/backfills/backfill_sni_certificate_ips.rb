class BackfillSniCertificateIps < BackfillBase
  def perform
    puts "BackfillSniCertificateIps: Starting dry_run=#{dry_run}, pod=#{pod}"

    Certificate.active.where(sni_enabled: true).includes(:certificate_ips).find_each do |cert|
      if cert.certificate_ips.empty?
        cert.build_sni_pod_record
        cert.save! unless @dry_run
      end
    end
  end
end
