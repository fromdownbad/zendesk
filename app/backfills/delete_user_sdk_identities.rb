class DeleteUserSdkIdentities < BackfillBase
  attr_reader :account_id, :user_id 

  def perform
    return if @account_id.blank? || @user_id.blank?
    user_sdk_identities.each do |identity|
      if (@dry_run)
        puts "UserSdkIdentity ##{identity.id}"
        next
      end
      identity.device_identifier.destroy if identity.device_identifier
      identity.destroy
    end
  end

  def initialize(options = {})
    super
    @user_id = options.fetch(:user_id)
    @account_id = options.fetch(:account_id)
  end

  private
  def user_sdk_identities
    account = Account.find(@account_id)
    account.users.where(id: @user_id).first.identities.sdk
  end
end