require 'csv'

class RevertTicketFieldUpdates < BackfillBase
  attr_reader :account, :ticket_ids, :field_ids, :identification_tag, :csv_output_path
  def initialize(**args)
    @account = Account.find(args[:account_id])
    @ticket_ids = args[:ticket_ids]
    @field_ids = args[:field_ids]
    @identification_tag = args[:identification_tag]
    @csv_output_path = args[:csv_path]
    super
  end

  def perform
    Rails.logger.info("RevertTicketFieldUpdates: Starting dry_run=#{dry_run}, account=#{account.subdomain}")
    account.on_shard do
      ticket_ids.in_groups_of(200, false) do |ticket_id_group|
        account.tickets.where(nice_id: ticket_id_group).find_each do |ticket|
          field_updates = field_ids.map do |field_id|
            update_field(ticket, field_id)
          end.compact.inject({}) {|fields, update| fields.merge(update)}
          unless field_updates.empty? || dry_run
            handle_closed_ticket(ticket) do
              ticket.disable_triggers = true
              ticket.will_be_saved_by(User.system)
              ticket.fields = field_updates
              ticket.additional_tags = identification_tag
              ticket.save!
            end
          end
        end
      end
    end
    if csv_output_path
      csv_file.close
      log_untouched_tickets
    end
  end

  def log_untouched_tickets
    ticket_ids.in_groups_of(200, false) do |ticket_id_group|
      account.tickets.where(nice_id: ticket_id_group).find_each do |ticket|
        untouched_csv << [ticket.nice_id] unless ticket.current_tags.include? identification_tag
      end
    end
    untouched_csv.close
  end

  def update_field(ticket, field_id)
    field_history = ticket.events.where(value_reference: field_id)
    first_value = field_history.first.try(:value)
    latest_value = field_history.last.try(:value)
    csv_file << [ticket.nice_id, field_id, latest_value, first_value] if csv_output_path
    unless field_history.empty? || first_value == latest_value
      return { field_id => first_value}
    end
  end

  def handle_closed_ticket(ticket)
    if ticket.status_id == StatusType.CLOSED && !dry_run
      silent_status_update(StatusType.SOLVED, ticket)
      yield
      silent_status_update(StatusType.CLOSED, ticket)
    else
      yield
    end
  end

  def silent_status_update(new_status, ticket)
    old_status = ticket.status_id
    if old_status != new_status
      ticket.will_be_saved_by(User.system)
      # silently update the status without mucking with the status_updated_at and therefore triggering SLAs
      Ticket.where(status_id: old_status, id: ticket.id).update_all(status_id: new_status)
      status_change = Change.new(ticket: ticket, author_id: User.system.id, is_public: false, via_id: ViaType.BATCH,
        value_reference: "status_id", value_previous: old_status, value: new_status)
      ticket.create_silent_change_event(status_change)
      ticket.reload
    end
  end

  def untouched_csv
    @updated_csv ||= begin
      csv_file = CSV.open(File.join(csv_output_path, "untouched.csv"), 'w')
      csv_file << ["ticket_id", "field_id", "previous_value", "updated_value"]
      csv_file
    end
  end

  def updated_csv
    @updated_csv ||= begin
      csv_file = CSV.open(File.join(csv_output_path, "updated.csv"), 'w')
      csv_file << ["ticket_id", "field_id", "previous_value", "updated_value"]
      csv_file
    end
  end
end
