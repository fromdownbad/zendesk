class AddFacebookPageScopedId < BackfillBase
  attr_reader :account_id, :output_dir, :ticket_created_after

  FB_NUM_RETRIES = 3
  FB_INVALID_ASID_MESSAGE = '(#100) There are one or more invalid App Scoped User IDs for current App.'
  FB_PSID_BATCH_LIMIT = 200

  HTTP_STATUS_OK = "200"

  MYSQL_ID_LIMIT = 100000
  MYSQL_COMMENT_LIMIT = 20

  TICKET_DATE_LIMIT = 1.year.ago.to_s

  OUTPUT_FILE = 'fb_psid_migration_log.json'

  def initialize(options = {})
    @account_id = options.fetch(:account_id, nil)
    @ticket_created_after = options.fetch(:ticket_created_after, TICKET_DATE_LIMIT)
    @ticket_created_after = Time.parse(@ticket_created_after)
    super
  end

  def perform
    Rails.logger.info("AddFacebookPageScopedId: Starting dry_run=#{dry_run}")
    account_id.present? ? run_on_account(account_id) : run_on_pod
  end

  def run_on_account(account_id)
    backfill_account(account_id)
  end

  def run_on_pod
    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    log_to_stdout_file("AddFacebookPageScopedId: backfill shard #{ActiveRecord::Base.current_shard_id}")

    # Load accounts on this shard.
    account_ids = Account.shard_local.pluck(:id)

    # Load all Facebook identities on this shard.
    identities = UserFacebookIdentity.where(account_id: account_ids)

    # Filter down to accounts that have Facebook identities.
    account_ids = account_ids & identities.map(&:account_id)

    # Perform fix on each account.
    account_ids.each do |account_id|
      begin
        backfill_account(account_id)
      rescue Exception => exception # rubocop:disable Lint/RescueException
        output_failed_account(account_id, exception)
        next
      end
    end
  end

  def backfill_account(account_id)
    log_to_stdout_file("AddFacebookPageScopedId: fixing account #{account_id}")
    begin
       migration_to_page_scoped_id(account_id)
    rescue Exception => exception # rubocop:disable Lint/RescueException
      Rails.logger.info("AddFacebookPageScopedId: exception #{exception}")
      output_failed_account(account_id, exception)
      raise exception
     end
  end

  # Establish the mappig between the user_id and corresponding ASID
  def page_user_asids(account_id, page)
    # Load all FB tickets brought in via the page.
    ticket_ids = ActiveRecord::Base.on_slave do
      Ticket.where(account_id: account_id, via_reference_id: page.id, via_id: [ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE])
        .where('created_at > ?', ticket_created_after).pluck(:id)
    end

    # Load all users active on these tickets. We only want comments brought in from Facebook, not via web or other.
    user_ids = []
    ActiveRecord::Base.on_slave do
      ticket_ids.each_slice(MYSQL_ID_LIMIT) do |ticket_ids_batch|
        ids = Event.where(type: 'Comment', account_id: account_id, ticket_id: ticket_ids_batch, via_id: [ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE])
          .limit(MYSQL_COMMENT_LIMIT).pluck(:author_id)
        user_ids += ids
      end
    end

    # Remove duplicate users.
    user_ids = user_ids.uniq

    # Get FB ASIDs for each of these users.
    user_asids = []
    ActiveRecord::Base.on_slave do
      user_ids.each_slice(MYSQL_ID_LIMIT) do |user_ids_batch|
        ids = UserFacebookIdentity.where(account_id: account_id, user_id: user_ids_batch).pluck(:value)
        user_asids += ids
      end
    end

    # We need to remove the user which corresponds to the Page. The ID of a Page user will not be changed during the
    # Facebook ASID -> PSID change.
    page_asid = page.graph_object_id
    user_asids.delete(page_asid)

    user_asids
  end

  # Returns a map from ASIDs to PSIDs.

  def fetch_psids_map(page, user_asids)
    # Build the FB API request.
    uri, request, options = build_request(page, user_asids)

    # Try the request up to 3 times in case of intermittent failure.
    response = nil
    FB_NUM_RETRIES.times do
      response = Net::HTTP.start(uri.hostname, uri.port, options) do |http|
        http.request(request)
      end

      # If request was successful, break out of retry loop.
      break if response.code == HTTP_STATUS_OK

      # See if the response indicates invalid ASIDs.
      invalid_asids = invalid_asids_from_response(response)

      # Remove those invalid ASIDs and re-build the request.
      if invalid_asids.any?
        user_asids = user_asids - invalid_asids
        uri, request, options = build_request(page, user_asids)
      end
    end

    # If there's still a Facebook issue after 3 tries, skip these ASIDs.
    if response.code != HTTP_STATUS_OK
      Rails.logger.error("Facebook Error. Code: #{response.code}, Body: #{response.body}.")
      return {}
    end

    # Parse response body.
    body = JSON.parse(response.body)

    # If FB tells us about an error (other than invalid ASIDs), log the error and move on.
    if body['error']
      Rails.logger.error("Facebook API Error. Body: #{response.body}.")
      return {}
    end

    return body
  end

  def build_request(page, user_asids)
    uri = URI.parse("https://graph.facebook.com/v3.2/pages_id_mapping")
    request = Net::HTTP::Post.new(uri)
    appsecret_proof = OpenSSL::HMAC.hexdigest('SHA256', Zendesk::Configuration.dig!(:facebook_integration, :secret), page.access_token)
    request.set_form_data(
      "access_token" => page.access_token,
      "appsecret_proof" => appsecret_proof,
      "user_ids" => user_asids.join(',')
    )
    options = {
      use_ssl: uri.scheme == "https"
    }

    [uri, request, options]
  end

  def invalid_asids_from_response(response)
    return [] unless response.body

    # Parse the response body.
    body = JSON.parse(response.body)

    # Return unless the error pertains to invalid ASIDs.
    return [] unless body.try(:[], 'error').try(:[], 'message') == FB_INVALID_ASID_MESSAGE

    # FB tells us which ASIDs it didn't like.
    invalid_asids = body.try(:[], 'error').try(:[], 'error_data').try(:values).map(&:to_s)

    invalid_asids || []
  end

  ##
  # Creates or updates the User identity corresponding to the link between a Zendesk user and an FB Page.

  def create_or_update_user_identity(account_id, page, user_id, user_asid)
    value = page.id.to_s + '-' + user_asid
    identity = UserFacebookIdentity.where(account_id: account_id, user_id: user_id, value: [user_asid, value]).first_or_initialize
    identity.value = value
    identity.is_verified = 1
    identity.save!
  end

  ##
  # Creates or updates the User profile, storing the PSID in metadata.

  def create_or_update_fb_user_profile(account_id, page, user_id, user_asid, user_psid)
    external_id = page.id.to_s + '-' + user_asid
    profile = Channels::FacebookUserProfile.where(account_id: account_id, user_id: user_id, external_id: [user_asid, external_id]).first_or_initialize
    profile.external_id = external_id
    profile.name = User.find_by(account_id: account_id, id: user_id).name
    profile.source_id = page.id

    # If the metadata is empty, use the metadata from the first profile for this user.
    if profile.metadata.nil?
      metadata = Channels::FacebookUserProfile.where(account_id: account_id, user_id: user_id).first.try(:metadata)
      profile.metadata = metadata || {}
    end

    # Add the all-important PSID.
    profile.metadata["page_scoped_id"] = user_psid
    profile.save!
  end

  ##
  # Adds the PSID to profile metadata for each Channels::FacebookUserProfile & Facebook::Page on this account.

  def migration_to_page_scoped_id(account_id)
    # Load account record.
    account = Account.find(account_id)
    return unless account.is_active? && account.is_serviceable?

    # Get pages for the Account.
    pages = Facebook::Page.where(account_id: account_id).where("state = #{Facebook::Page::State::ACTIVE}")

    # Apply PSID to each user via the FB page.
    pages.each do |page|
      Rails.logger.info("Processing Page: #{page.id}")

      # Get all the ASIDs for users that have been active on this Page. These may be page-prefixed as the user
      # may have been active on a page that was already processed.
      user_asids = page_user_asids(account_id, page)

      # Fetch PSIDs in batches of 200 as restricted by the FB API.
      user_asids.each_slice(FB_PSID_BATCH_LIMIT) do |user_asids_batch|
        # Remove page-prefix for sending to FB API.
        non_prefixed_user_asids = user_asids_batch.map do |user_asid|
          user_asid.include?('-') ? user_asid.split('-')[1] : user_asid
        end

        # Remove duplicates. We may have these because many ASIDs could have different page-prefixes.
        non_prefixed_user_asids.uniq

        # Get map from ASIDs to PSIDs.
        user_asids_psids_map = fetch_psids_map(page, non_prefixed_user_asids.uniq)

        # Skip the update if dry run.
        next if dry_run

        # Update using the PSID results.
        user_asids_psids_map.each do |user_asid, user_psid|
          # Load the user to assign the identities too.
          existing_user_asid = user_asids_batch.find { |asid| asid.include?(user_asid) }
          user_id = UserFacebookIdentity.where(account_id: account_id, value: existing_user_asid).first.user_id

          ActiveRecord::Base.transaction do
            create_or_update_user_identity(account_id, page, user_id, user_asid)
            create_or_update_fb_user_profile(account_id, page, user_id, user_asid, user_psid)
          end
        end
      end
    end
  end

  def log_to_stdout_file(output)
    Rails.logger.info(output)
    write_to_file(output)
  end

  def output_failed_account(account_id, exception)
    output = {}
    output[:account_id] = account_id
    output[:error] = exception.to_s
    Rails.logger.info("AddFacebookPageScopedId:#{exception.backtrace}")
    write_to_file(output.to_json)
    output.clear
  end

  def write_to_file(output)
    File.open(output_path, "a") do |file|
      file << "#{output}\n"
    end
  end

  def output_path
    @output_path = File.join(ENV['HOME'], OUTPUT_FILE)
  end
end
