class UserOrgFixMemberships < BackfillBase
  # This backfill finds and updates users where their organization_id and
  # organization_memberships are not in agreement
  attr_reader :account_id, :shard_id, :force, :total_affected_users

  def initialize(**args)
    @account_id = args[:account_id]
    @shard_id = args[:shard_id]
    @force = args[:force]
    super args
  end

  def perform
    @total_affected_users = 0

    if account_id.present?
      backfill_account(account_id)
    elsif shard_id.present?
      backfill_shard(shard_id)
    else
      backfill_pod
    end
  end

  private

  def backfill_account(account_id)
    log("Starting run for account #{account_id}, dry_run=#{dry_run}")
    account = Account.find(account_id)
    account.on_shard do
      users_scope = account.users.active
      fix_organizations_for(users_scope)
    end
    log("Finishing run for account #{account_id}, dry_run=#{dry_run}, total affected users=#{total_affected_users}")
  end

  def backfill_shard(shard)
    log("Starting run for shard #{shard}, dry_run=#{dry_run}")
    ActiveRecord::Base.on_shard(shard) do
      active_account_ids = Account.where(shard_id: shard).active.pluck(:id)
      users_scope = User.where(account_id: active_account_ids).active
      fix_organizations_for(users_scope)
    end
    log("Finishing run for shard #{shard}, dry_run=#{dry_run}, total affected users=#{total_affected_users}")
  end

  def backfill_pod
    log("Starting run for pod, dry_run=#{dry_run}")
    on_shards do |s|
      backfill_shard(s)
    end
    log("Finishing run for pod, dry_run=#{dry_run}, total affected users=#{total_affected_users}")
  end

  def fix_organizations_for(users_scope)
    # find users where user.organization_id does not exist in user.organization_memberships
    fix_users(users_scope)

    # find users where user.organization_id is present and user.organization_memberships is empty
    fix_users_without_memberships(users_scope)
  end

  def fix_users(users_scope)
    users = ActiveRecord::Base.on_slave do
      users_scope
        .joins('INNER JOIN organization_memberships omj ON omj.user_id = users.id AND omj.account_id = users.account_id')
        .where( 'NOT EXISTS (SELECT 1 FROM organization_memberships om where om.organization_id = users.organization_id AND om.user_id = users.id)')
    end

    log("#{__method__}: #{users.count} users found with an inconsistent state")
    @total_affected_users += users.count

    users.find_each do |user|
      incorrect_id = user.organization_id
      default_organization_id = find_default_organization_for(user).try(:id)
      if default_organization_id
        user.update_column(:organization_id, default_organization_id) unless dry_run
      else
        user.organization_id = nil
        user.organization_memberships.first.send(:promote_successor_to_default) unless dry_run #implicitly saves
      end
      log("#{__method__}: User #{user.id} organization_id was: #{incorrect_id}, is: #{user.organization_id}")
    end
  end

  def fix_users_without_memberships(users_scope)
    users_without_memberships = ActiveRecord::Base.on_slave do
      users_scope
        .joins('LEFT JOIN organization_memberships om ON users.id = om.user_id AND om.account_id = users.account_id')
        .where('om.id IS NULL')
        .where('users.organization_id IS NOT NULL AND users.roles = 0')
    end

    log("#{__method__}: #{users_without_memberships.count} users found with an inconsistent state")
    @total_affected_users += users_without_memberships.count

    return if dry_run
    users_without_memberships.find_each do |end_user|
      next unless end_user.account.try(&:is_active?)
      organization_id = end_user.organization_id
      end_user.organization_id = nil
      if end_user.save # callbacks try to set a default organization for the user
        log("#{__method__}: EndUser organization removed, user_id: #{end_user.id} organization_id: #{organization_id}")
      elsif force
        end_user.update_column(:organization_id, nil)
        log("#{__method__}: EndUser organization force removed, user_id: #{end_user.id} organization_id: #{organization_id}")
      else
        log("#{__method__}: Could not save end user #{end_user.id}, Errors: #{end_user.errors.full_messages.join}")
      end
    end
  end

  def find_default_organization_for(user)
    user.organization_memberships.where(default: true).first.try(:organization)
  end

  def log(string)
    Rails.logger.info("UserOrgFixMemberships: #{string}")
  end
end
