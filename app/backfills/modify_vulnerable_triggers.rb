class ModifyVulnerableTriggers < BackfillBase
  DEFAULT_NOTIFY_REQUESTER_TRIGGER_KEY = "txt.default.triggers.notify_requester_received".freeze
  NEW_PROACTIVE_AGENT_TRIGGER_KEY = "txt.default.triggers.notify_requester_proactive_ticket".freeze

  # TO run: ModifyVulnerableTriggers.new(account_ids: account_ids, dry_run: true).perform
  def initialize(options = {})
    @account_ids = options.fetch(:account_ids, nil)
    super
  end

  def perform
    Rails.logger.info("ModifyVulnerableTriggers: Starting dry_run=#{dry_run}")
    @account_ids.each do |account_id|
      begin
        modify_vulnerable_triggers(account_id)
      rescue StandardError => exception
        Rails.logger.info("ModifyVulnerableTriggers_Error failed for account_id: #{account_id} with error: #{exception}")
        statsd_client_trigger_actions.increment("failure")
        next
      end
    end
  end

  def modify_vulnerable_triggers(account_id)
    account = Account.find(account_id)
    account.shard

    translated_notify_requester_trigger_title_value = I18n.t("#{DEFAULT_NOTIFY_REQUESTER_TRIGGER_KEY}.title", locale: account.translation_locale)
    notify_requester_trigger = account.triggers.find_by(title: translated_notify_requester_trigger_title_value, is_active: true)
    subject = notify_requester_trigger.definition.actions.first.value[1]
    body = notify_requester_trigger.definition.actions.first.value[2]

    Rails.logger.info("ModifyVulnerableTriggers: Starting Backfill -- Dry_run:#{dry_run}, account_id: #{account_id}, Subject: #{subject}, Body: #{body}")
    statsd_client_trigger_actions.increment("starting", tags: ["dry_run:#{dry_run}"])

    unless dry_run
      modify_old_notify_requester_trigger(notify_requester_trigger)
      add_new_proactive_trigger(account)
      Zendesk::SupportAccounts::Product.retrieve(account).save!

      translated_proactive_trigger_title_value = I18n.t("#{NEW_PROACTIVE_AGENT_TRIGGER_KEY}.title", locale: account.translation_locale)
      Rails.logger.info("ModifyVulnerableTriggers_Success: account_id: #{account_id}, notify_requester_trigger: #{account.reload.triggers.find_by(title: translated_notify_requester_trigger_title_value, is_active: true).inspect}, new_proactive_trigger: #{account.reload.triggers.find_by(title: translated_proactive_trigger_title_value, is_active: false).inspect}")
      statsd_client_trigger_actions.increment("success")
    end
  end

  # gsub taken from zendesk/app/models/accounts/base.rb:432
  def modify_old_notify_requester_trigger(notify_requester_trigger)
    notify_requester_trigger.definition.actions.first.value[1] = I18n.t("#{DEFAULT_NOTIFY_REQUESTER_TRIGGER_KEY}.subject_v3")
    notify_requester_trigger.definition.actions.first.value[2] = I18n.t("#{DEFAULT_NOTIFY_REQUESTER_TRIGGER_KEY}.body_v4").gsub("<br />", "\n")

    new_notify_requester_trigger_item = DefinitionItem.new("role", "is", ["end_user"])
    notify_requester_trigger.definition.conditions_all.push(new_notify_requester_trigger_item)
    notify_requester_trigger.save!
  end

  def add_new_proactive_trigger(account)
    definition = Definition.new
    proactive_trigger_coniditions1 = DefinitionItem.new("update_type", "is", "Create")
    proactive_trigger_coniditions2 = DefinitionItem.new("ticket_is_public", "is", ["public"])
    proactive_trigger_coniditions3 = DefinitionItem.new("role", "is", ["agent"])
    definition.conditions_all.push(
      proactive_trigger_coniditions1,
      proactive_trigger_coniditions2,
      proactive_trigger_coniditions3
    )

    requester_value = account.has_follower_and_email_cc_collaborations_enabled? ? "requester_and_ccs" : "requester_id"

    # gsub taken from zendesk/app/models/accounts/base.rb:432
    definition.actions.push(DefinitionItem.build(
      operator: "is",
      source: "notification_user",
      value: [requester_value, I18n.t("#{NEW_PROACTIVE_AGENT_TRIGGER_KEY}.subject"), I18n.t("#{NEW_PROACTIVE_AGENT_TRIGGER_KEY}.body").gsub("<br />", "\n")]
    ))

    account.triggers.create!(
      title: I18n.t("#{NEW_PROACTIVE_AGENT_TRIGGER_KEY}.title"),
      description: I18n.t("#{NEW_PROACTIVE_AGENT_TRIGGER_KEY}.description"),
      definition: definition,
      position: 0,
      account: account,
      is_active: false
    )
  end

  def statsd_client_trigger_actions
    @statsd_client_trigger_actions ||= Zendesk::StatsD::Client.new(namespace: ["orca_trigger_backfill"])
  end
end
