class CleanUpUrlTargetFailures < BackfillBase
  def perform
    log("Starting on pod #{Zendesk::Configuration.fetch(:pod_id)}")
    on_shards do
      backfill_shard
    end
  end

  private

  def backfill_shard
    target_ids,total_records = find_ids_and_count

    log("Cleaning up #{total_records} records on shard #{ActiveRecord::Base.current_shard_id}")

    process_records(target_ids) if target_ids.any?
  end

  def find_ids_and_count
    rows = UrlTargetFailure
      .select(:url_target_v2_id)
      .group(:url_target_v2_id)
      .having("count(url_target_v2_id) > 25")
      .count
    rows.reduce([[],0]) { |out,row| [out[0] << row[0], out[1]+(row[1]-25)] }
  end

  def process_records(ids)
    UrlTargetV2.find(ids).each do |target|
      failures_to_clean = target.url_target_failures.order('created_at DESC').offset(25)
      log("Cleaning #{failures_to_clean.count} failures from url target #{target.id}")
      destroy_records(failures_to_clean)
    end
  end

  def destroy_records(records)
    records.each do |r|
      begin
        r.destroy! unless dry_run
        log("Destroyed #{r.class} #{r.id}")
      rescue Exception => e
        log("Failed to destroy #{r.class} #{r.id}: #{e.to_s}")
      end
    end
  end

  def log(msg)
    Rails.logger.info("RemoveOrphanedTargetFailures: " + msg + "#{dry_run ? ' (dry run)' : ''}")
  end
end
