require 'set'

class ProductSyncReportExporter < BackfillBase
  include BackfillAccountProductsQuery
  attr_reader :target_dir, :data_csv_path

  DATA_CSV_FILENAME = '_product_sync_data.csv'
  DEFAULT_RESULT_TYPE_FILTER_OPTIONS = [:pass, :fail, :aborted, :no_product_record]

  CSV_OPTIONS = {
    write_headers: true,
    headers: [
      'timestamp',
      'account_id',
      'account_subdomain',
      'account_name',
      'account_created_at',
      'billing_id',
      'result',
      'state_result',
      'state_updated_at_result',
      'trial_expires_at_result',
      'plan_type_result',
      'max_agents_result',
      'light_agents_result',
      'expected_state',
      'actual_state',
      'expected_state_updated_at',
      'actual_state_updated_at',
      'expected_trial_expires_at',
      'actual_trial_expires_at',
      'expected_plan_type',
      'actual_plan_type',
      'expected_max_agents',
      'actual_max_agents',
      'expected_light_agents',
      'actual_light_agents',
      'support_product_created_at',
      'support_product_updated_at',
      'multiproduct',
      'is_serviceable',
      'is_active',
      'churned_on',
      'deleted_at',
      'is_trial',
      'trial_expired',
      'zuora_managed',
      'multiproduct_billing_participant',
      'sales_model',
      'account_type',
      'account_status',
      'payment_method_type',
      'manual_discount',
      'pod_id',
      'shard_id'
    ]
  }

  # Optionally filter result set by account_ids, created_after_date, or result type (Array).
  # result_type_filter options are :pass, :fail, :aborted, or :no_product_record. Example:
  # options = { account_ids: [1,2,3], result_type_filter: [:fail, :no_product_record] }
  def initialize(options = {})
    super(options)

    @target_dir = options.fetch(:target_dir) { raise ArgumentError.new('specify target_dir option') }
    @data_csv_path = generate_timestamped_csv_path
    @result_type_filter_options = options[:result_type_filter]
    @light_agents = options[:light_agents]
    initialize_account_query_options(options)
  end

  def perform
    stats = { pass: 0, fail: 0, aborted: 0, no_product_record: 0, total: 0 }

    Rails.logger.info "Exporting product sync validation data for result type(s): #{result_type_filter.to_a.join(', ')}"

    CSV.open(data_csv_path, 'wb', CSV_OPTIONS) do |data_csv|
      account_query_for_exporter.find_in_batches do |batch|
        batch.each do |account|
          validation_data = Zendesk::Accounts::ProductSyncValidator.validate(account, light_agents: @light_agents)
          next unless validation_data
          data_csv << row_data(validation_data) if result_type_filter.include?(validation_data[:result])
          stats[validation_data[:result]] += 1
          print '.'
        end
      end
    end

    stats[:total] = stats.values.reduce(:+)
    Rails.logger.info "Finished export to #{data_csv_path}. Stats: #{stats.inspect}"
  end

  private

  def generate_timestamped_csv_path
    datetime = Time.now.strftime('%Y-%m-%dT%H-%M-%S')
    data_csv_path = File.join(target_dir, datetime + DATA_CSV_FILENAME)
  end

  def result_type_filter
    @result_type_filter ||= begin
      filter_selections = @result_type_filter_options || DEFAULT_RESULT_TYPE_FILTER_OPTIONS
      raise ArgumentError unless filter_selections.length &&
        filter_selections.length > 0 &&
        filter_selections.all? { |filter| DEFAULT_RESULT_TYPE_FILTER_OPTIONS.include?(filter) }

      Set.new(filter_selections)
    end
  end

  def row_data(validation_data)
    [
      validation_data[:timestamp],
      validation_data[:account].id,
      validation_data[:account].subdomain,
      validation_data[:account].name,
      validation_data[:account].created_at.try(:iso8601),
      validation_data[:account].billing_id ||
        validation_data[:account].zuora_subscription.try(:zuora_account_id),
      validation_data[:result],
      validation_data[:comparison_results][:state],
      validation_data[:comparison_results][:state_updated_at],
      validation_data[:comparison_results][:trial_expires_at],
      validation_data[:comparison_results][:plan_type],
      validation_data[:comparison_results][:max_agents],
      validation_data[:comparison_results][:light_agents],
      validation_data[:expected_product_attributes][:state],
      validation_data[:actual_product_attributes][:state],
      validation_data[:expected_product_attributes][:state_updated_at],
      validation_data[:actual_product_attributes][:state_updated_at],
      validation_data[:expected_product_attributes][:trial_expires_at],
      validation_data[:actual_product_attributes][:trial_expires_at],
      validation_data[:expected_product_attributes][:plan_type],
      validation_data[:actual_product_attributes][:plan_type],
      validation_data[:expected_product_attributes][:max_agents],
      validation_data[:actual_product_attributes][:max_agents],
      validation_data[:expected_product_attributes][:light_agents],
      validation_data[:actual_product_attributes][:light_agents],
      validation_data[:support_product].try(:created_at),
      validation_data[:support_product].try(:updated_at),
      validation_data[:account].multiproduct?,
      validation_data[:account].is_serviceable?,
      validation_data[:account].is_active?,
      validation_data[:account].subscription.churned_on.try(:iso8601),
      validation_data[:account].deleted_at.try(:iso8601),
      validation_data[:account].subscription.is_trial?,
      validation_data[:account].subscription.trial_expired?,
      validation_data[:account].subscription.zuora_managed?,
      multiproduct_billing_participant(validation_data),
      validation_data[:account].zuora_subscription.try(:sales_model),
      validation_data[:account].subscription.account_type,
      validation_data[:account].subscription.account_status,
      validation_data[:account].subscription.payment_method_type,
      validation_data[:account].subscription.manual_discount,
      validation_data[:account].pod_id,
      validation_data[:account].shard_id
    ]
  end

  def multiproduct_billing_participant(validation_data)
    # This call uses Zuora so may throw exceptions which need to be caught
    validation_data[:account].billing_multi_product_participant?
  rescue StandardError => ex
    ex.message.gsub(",", "-")
  end
end
