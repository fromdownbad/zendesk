class BackfillCsatStatusId < BackfillBase
  LIMIT   = 100
  WARNING = LIMIT * 10
  ERROR   = LIMIT * 100

  def perform
    Rails.logger.info("BackfillCsatStatusId: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    Rails.logger.info("BackfillCsatStatusId: backfill shard")
    Account.shard_local.find_each { |account| backfill_account(account) }
  end

  def backfill_account(account)
    Rails.logger.info("BackfillCsatStatusId: backfill account #{account.id}")
    return unless account.satisfaction_ratings.where(:status_id => nil).exists?

    [0, 1, 2, 3, 4, 6].each do |status_id|
      Rails.logger.info("BackfillCsatStatusId: backfill all tickets where status_id=#{status_id}")
      backfill_tickets(account.tickets.where(:status_id => status_id), status_id)

      Rails.logger.info("BackfillCsatStatusId: backfill all archived tickets where status_id=#{status_id}")
      backfill_tickets(account.ticket_archive_stubs.where(:status_id => status_id), status_id)
    end
  end

  def backfill_tickets(tickets, status_id)
    offset = 0

    while true do
      ids = tickets.limit(LIMIT).offset(offset).pluck(:id)
      offset += LIMIT
      break if ids.empty?

      Rails.logger.info("BackfillCsatStatusId: backfill #{ids.count} tickets where ids in #{ids}")

      ratings = Satisfaction::Rating.where(ticket_id: ids)
      backfill(ratings, status_id)
    end
  end

  def backfill(ratings, status_id)
    return if dry_run

    count = ratings.update_all(status_id: status_id)
    Rails.logger.info("BackfillCsatStatusId: backfilled #{count} ratings with status_id=#{status_id}")
    Rails.logger.warn("BackfillCsatStatusId: backfilled more than #{WARNING} ratings") if count > WARNING
    fail "Backfilled more than #{ERROR} ratings" if count > ERROR
  end
end
