class RemovePasswords < BackfillBase
  def initialize(account_id)
    @account = Account.find(account_id)
    @shard   = @account.shard_id
  end

  def reset_end_users_password
    perform { reset_passwords(@account.end_users) }
  end

  def reset_agents_password
    perform { reset_passwords(@account.agents) }
  end

  def perform(&block)
    ActiveRecord::Base.on_shard(@shard) do
      yield
    end
  end

  private

  def reset_passwords(users)
    users.update_all(salt: nil)
    users.update_all(crypted_password: nil)
  end
end
