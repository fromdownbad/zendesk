class BackfillJwtTokensRedact < BackfillBase
  def perform
    Rails.logger.info("BackfillJwtTokensRedact: Starting dry_run = #{dry_run}, pod = #{pod}")
    on_shards { backfill_shard }
    Rails.logger.info("BackfillJwtTokensRedact: Finished dry_run = #{dry_run}, pod = #{pod}")
  end

  private

  def backfill_shard
    Rails.logger.info("BackfillJwtTokensRedact: backfill shard #{ActiveRecord::Base.current_shard_id}")

    RemoteAuthentication.jwt.select(:id).find_in_batches do |batch|
      rids = batch.map(&:id)
      jwt_tokens = CIA::AttributeChange.where(source_type: 'RemoteAuthentication',
                                            attribute_name: 'token',
                                            source_id: rids)
      if jwt_tokens.blank?
        Rails.logger.info('BackfillJwtTokensRedact: Total records for JWT tokens = 0')
        return
      end

      Rails.logger.info("BackfillJwtTokensRedact: Total records for JWT tokens = #{jwt_tokens.count}")

      @updated_tokens = 0
      jwt_tokens.each do |jwt_token|
        old_value = jwt_token.old_value
        new_value = jwt_token.new_value
        redacted_old_value = redact_token(old_value)
        redacted_new_value = redact_token(new_value)
        unless dry_run
          if redacted_old_value != old_value && redacted_new_value != new_value
            jwt_token.update(old_value: redacted_old_value,
                             new_value: redacted_new_value)
          elsif redacted_old_value != old_value
            jwt_token.update(old_value: redacted_old_value)
          else
            jwt_token.update(new_value: redacted_new_value)
          end
        end
        Rails.logger.info("Redacted token: old_value to #{redacted_old_value}") if redacted_old_value != old_value
        Rails.logger.info("Redacted token: new_value to #{redacted_new_value}") if redacted_new_value != new_value
      end
      Rails.logger.info("BackfillJwtTokensRedact: Successfully corrected #{@updated_tokens} tokens")
    end
  end

  def already_redacted(token)
    redacted = token[6..token.size]
    redacted == "*" * redacted.size
  end

  def redact_token(token)
    return token if token.blank?
    return token if already_redacted(token)
    @updated_tokens += 1
    token[(0..5)] + '*' * (token.size - 6)
  end
end
