class ConvertTriggersToAnswerBot < BackfillBase
  attr_reader :account_ids

  ARTICLE_COUNT_EXP = /\bautomatic_answers\.article_count\b/
  ARTICLE_LIST_EXP = /\bautomatic_answers\.article_list\b/
  FIRST_ARTICLE_BODY_EXP = /\bautomatic_answers\.first_article_body\b/
  ANSWER_BOT_ARTICLE_COUNT = 'answer_bot.article_count'.freeze
  ANSWER_BOT_ARTICLE_LIST = 'answer_bot.article_list'.freeze
  ANSWER_BOT_FIRST_ARTICLE_BODY = 'answer_bot.first_article_body'.freeze
  EMAIL_BODY_POSITION = 2

  def initialize(options)
    @account_ids = options.fetch(:account_ids)
    super
  end

  def perform
    account_ids.each do |account_id|
      account = Account.find(account_id)
      begin
        account.on_shard do
          triggers = active_deflection_triggers(account)
          convert_triggers(triggers, account)
        end
      rescue => e
        log("Failed to convert answer_bot placeholder for account_id: #{account.id}, error: #{e}.");
      end
    end
  end

  private

  def convert_triggers(triggers, account)
    triggers.each do |trigger|
      actions = deflection_actions_for_trigger(trigger)
      actions.each do |action|
        email_body = action.value[EMAIL_BODY_POSITION]
        next unless placeholders_exist?(email_body)

        action.value[EMAIL_BODY_POSITION] = convert_email_body(email_body)

        unless dry_run
          trigger.save!
        end

        log("Successfully converted to answer_bot placeholders for trigger_id: #{trigger.id}, account_id: #{account.id}.");
      end
    end
  end

  def convert_email_body(email_body)
    if article_count?(email_body)
      email_body = substitute_placeholder(email_body, ARTICLE_COUNT_EXP, ANSWER_BOT_ARTICLE_COUNT)
    end

    if article_list?(email_body)
      email_body = substitute_placeholder(email_body, ARTICLE_LIST_EXP, ANSWER_BOT_ARTICLE_LIST)
    end

    if first_article_body?(email_body)
      email_body = substitute_placeholder(email_body, FIRST_ARTICLE_BODY_EXP, ANSWER_BOT_FIRST_ARTICLE_BODY)
    end

    email_body
  end

  def placeholders_exist?(email_body)
    article_count?(email_body) || article_list?(email_body) || first_article_body?(email_body)
  end

  def article_count?(email_body)
    !!(ARTICLE_COUNT_EXP =~ email_body)
  end

  def article_list?(email_body)
    !!(ARTICLE_LIST_EXP =~ email_body)
  end

  def first_article_body?(email_body)
    !!(FIRST_ARTICLE_BODY_EXP =~ email_body)
  end

  def substitute_placeholder(email_body, match, substitute)
    email_body.gsub(match, substitute)
  end

  def active_deflection_triggers(account)
    account.triggers.select do |trigger|
      !deflection_actions_for_trigger(trigger).empty?
    end
  end

  def deflection_actions_for_trigger(trigger)
    trigger.definition.actions.select do |action|
      action.source == 'deflection'
    end
  end

  def log(message)
    Rails.logger.info(
      "[#{self.class} | Pod: #{pod} | Dry Run: #{dry_run}]: #{message}"
    )
  end
end
