class UpdatableGlobalClient < Zendesk::OAuth::GlobalClient
  def readonly?
    false
  end
end

class BackfillShopifyRedirect < BackfillBase
  BASE_URL = 'https://shopify.zendesk-integrations.com'
  CLIENT_ID = 336
  URLS = [
    "#{BASE_URL}/support/zendesk/saveAuth",
    "#{BASE_URL}/support/zendesk/oAuth",
    "#{BASE_URL}/chat/zendesk/saveAuth",
    "#{BASE_URL}/chat/zendesk/oAuth"
  ]

  def perform
    Rails.logger.info "BackfillShopifyRedirecti: start; dry_run #{dry_run}"
    client = UpdatableGlobalClient.find CLIENT_ID

    Rails.logger.info "Updating Global OAuth Client: id #{CLIENT_ID}"
    client.redirect_uri = client.redirect_uri.concat URLS

    return if dry_run
    client.save!
  end
end
