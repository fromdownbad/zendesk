# The `purchased_at` field is initially filled when a Zuora to Zendesk sync
# occurs, however some accounts might not have triggered this sync (it doesn't
# happen for all accounts if no changes where made) hence the need for backfill.

class BackfillZopimSubscriptionPurchasedAt < BackfillBase
  def perform
    Rails.logger.info(%Q{ BackfillZopimSubscriptionPurchasedAt: Starting
      dry_run=#{dry_run}, pod=#{pod} }.squish)

    clause = "purchased_at IS NULL AND plan_type != :plan_type"
    args   = { plan_type: ZendeskBillingCore::Zopim::PlanType::Trial.name }

    ZendeskBillingCore::Zopim::Subscription.where(clause, args).find_each do |sub|
      if sub.account.zuora_managed?
        zid = sub.account.subscription.zuora_subscription.zuora_account_id

        Rails.logger.info(%Q{ BackfillZopimSubscriptionPurchasedAt: Starting
          Zuora synchronization for account #{sub.account.id} }.squish)

        ZendeskBillingCore::Zuora::Jobs::AccountSyncJob.enqueue(zid) unless dry_run
      end
    end
  end
end
