class BackfillSandboxEndUserSecurityPolicyId < BackfillBase
  def perform
    Rails.logger.info("BackfillSandboxEndUserSecurityPolicyId: Start; dry_run:#{dry_run}")

    account_ids = RoleSettings.where(end_user_security_policy_id: 400).joins(:account).select { |rs| rs.account.is_serviceable? && rs.account.is_active? }.sort_by { |rs| rs.account.pod_id }.map(&:account_id)

    count = 0
    Account.where(id: account_ids).each do |account|
      # after a brief investigation, only 3 accounts out of ~300 were not sandboxes, so we can ignore them until some breaking ticket comes in.
      if account.is_sandbox?
        if dry_run
          Rails.logger.info("Account #{account.id} end_user_security_policy_id to be updated from sandbox_master")
        else
          sandbox_master_end_user_security_policy_id = account.sandbox_master.role_settings.end_user_security_policy_id
          account.role_settings.update_attributes(end_user_security_policy_id: sandbox_master_end_user_security_policy_id)

          count += 1
        end
      end
    end
    Rails.logger.info("Total number of sandbox end user security policy ids updated = #{count}")
  end
end
