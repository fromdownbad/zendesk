class BackfillTicketDeflectionViaIdFromTicket < BackfillBase

  def initialize(options)
    @account_ids = options.fetch(:account_ids)
    super
  end

  def perform
    Account.pod_local.where(id: @account_ids).each do |account|
      begin
        puts "Starting on account id: #{account.id}"
        account.on_shard do
          account.ticket_deflections.each do |ticket_deflection|
            update_via_id(account, ticket_deflection)
          end
        end
        puts "Done account id: #{account.id}"
      rescue => e
        Rails.logger.info("Failed to backfill via_id for account_id: #{account.id}, error: #{e}.");
      end
    end
  end

  private

  def update_via_id(account, ticket_deflection)
    return if ticket_deflection.ticket_id.nil? || ticket_deflection.via_id

    ticket = Ticket.find_by_id(ticket_deflection.ticket_id) # force it to look at archived ticket
    ticket.ticket_deflection.via_id = ticket.via_id
    ticket.ticket_deflection.save!
  rescue => e
    Rails.logger.info("Failed to set ticket_deflection via_id for ticket_deflection_id: #{ticket_deflection.id} account_id: #{account.id}, error: #{e}.")
  end
end
