class UserEntityTopicBackfill < BackfillBase
  attr_reader :tag, :redis, :logger

  def initialize(options = {})
    super
    @batch_size = options.fetch(:batch_size, 50)
    @user_batch_size = options.fetch(:batch_size, 100)
    @user_counter = options.fetch(:user_counter, 30000)
    @redis = options.fetch(:redis, Zendesk::RedisStore.client)
    @tag = options[:tag]
    @logger = options.fetch(:logger, Rails.logger)
  end

  def perform(account_ids_to_skip: [])
    unless @dry_run || tag.present?
      raise "Tag must be present when not in dry run."
    end

    shards.each do |shard|
      next if backfill_status.shard_processed?(shard)

      ActiveRecord::Base.on_shard(shard) do
        Account.pod_local.where(shard_id: shard).select(:id).find_in_batches(batch_size: @batch_size) do |accounts|
          logger.info("Publishing users for accounts with IDs in range: #{accounts.first.id}-#{accounts.last.id}")
          accounts.each do |account|
            account_id = account.id
            next if account_ids_to_skip.include?(account_id)

            publish_account_users(account_id)
          end
        end
      end
      backfill_status.mark_shard_as_processed(shard)
    rescue StandardError => e
      logger.warn("skipping the shard #{shard}. Exception #{e.inspect}")
      backfill_status.mark_shard_as_skipped(shard)
    end
    logger.info("Finished running UserEntityTopicBackfill")
    logger.info("backfill failed for accounts #{skipped_accounts}") unless skipped_accounts.empty?
  end

  def perform_for_accounts(account_id: nil, account_ids: [account_id])
    account_ids.each do |account_id|
      ActiveRecord::Base.default_shard = Account.find_by(id: account_id).shard_id
      publish_account_users(account_id)
    end
  end

  def skipped_shards
    backfill_status.skipped_shards
  end

  def processed_shards
    backfill_status.processed_shards
  end

  def skipped_accounts
    backfill_status.skipped_accounts
  end

  def processed_accounts
    backfill_status.processed_accounts
  end

  def last_processed_user(account_id)
    backfill_status.last_processed_user(account_id: account_id)
  end

  def backfill_keys
    redis.keys("zendesk_user_support_entity_*")
  end

  def destroy_history
    keys = backfill_keys
    return unless keys.present?

    redis.del(*keys)
  end

  private

  def publish_account_users(account_id)
    return if dry_run
    return if backfill_status.account_processed?(account_id)

    UserEntityPublisher.new(account_id: account_id).
      publish_for_account(batch_size: @user_batch_size, backfill_status: backfill_status, user_counter: @user_counter)
    backfill_status.mark_account_as_processed(account_id)
  rescue StandardError => e
    logger.info("Skipping account: #{account_id}, DB connection lost exception #{e.inspect}")
    backfill_status.mark_account_as_skipped(account_id)
  end

  def backfill_status
    @backfill_status ||= BackfillStatus.new("zendesk_user_support_entity_#{tag}", redis)
  end
end
