module DurableBackfill
  class Task
    class MissingWorkImplementation < StandardError; end
    class RescheduleTask < StandardError; end
    class ExecutionFailed < StandardError; end

    class << self
      attr_accessor :title
      attr_writer :abstract

      def audits
        return [] if abstract?
        DurableBackfill::BackfillAudit.for_task(title)
      end

      # Creates the BackfillAudit records for each account & title.
      #   These records will later be worked off by the {DurableBackfill::Runner}
      #   `title` is an attr_accessor on the Task class that is required to be set.
      #
      # @return [none]
      def create_audits!
        return if abstract?

        workable_account_ids = Account.
          shard_local.
          where('`created_at` < ?', DurableBackfill::BackfillAudit.waterline_date(title)).
          where.not(id: Account.system_account_id).
          pluck(:id)

        audited_account_ids = audits.pluck(:account_id)

        (workable_account_ids - audited_account_ids).each_slice(1000) do |group|
          DurableBackfill::BackfillAudit.transaction do
            group.each do |account_id|
              DurableBackfill::BackfillAudit.create!(task: title, account_id: account_id)
            end
          end
        end
      end

      def abstract?
        !!@abstract
      end
    end

    def initialize(audit)
      @audit = audit
    end

    def reschedule!
      raise RescheduleTask.new, "#{title} instance rescheduled"
    end

    def title
      self.class.title
    end

    def abstract?
      self.class.abstract?
    end

    # Should be called rather than using the `work` method directly.  Goes through the following 3 steps:
    #
    # - saves started_at
    # - calls `work`
    # - saves completed_at
    #
    # @return boolean indicating success
    def work_audit
      unless @audit.account(true)
        logger.info("deleting #{title} for account_id: #{@audit.account_id} because account does not exist")
        @audit.destroy
        return true
      end

      # During an account move, an account may not be shard local, if so don't work on the account
      reschedule! unless @audit.account.shard_local?

      statsd.time('execute.time', tags: ["task:#{title}"]) do
        logger.info("starting #{title} for account_id: #{@audit.account_id}")

        @audit.start!
        work(@audit.account)
        @audit.complete!

        logger.info("completed #{title} for account_id: #{@audit.account_id}")
        statsd.increment('execute.success', tags: ["task:#{title}"])
      end

      true
    rescue RescheduleTask
      # Leave the audit untouched
      logger.info("rescheduling #{title} for account_id: #{@audit.account_id}")
      statsd.increment('execute.reschedule', tags: ["task:#{title}"])

      false
    rescue MissingWorkImplementation
      # reraise this error instead of converting it to ExecutionFailed
      raise
    rescue StandardError => e
      statsd.increment('execute.error', tags: ["task:#{title}", "error:#{e.class}"])
      logger.error("failed #{title} for account_id: #{@audit.account_id}")
      raise ExecutionFailed, "#{e.class}, #{e.message}"
    end

    private

    def work(*)
      raise MissingWorkImplementation, 'Must be implemented by subclass'
    end

    def statsd
      @statsd ||= DurableBackfill::Manager.statsd
    end

    def logger
      @logger ||= DurableBackfill::Manager.logger
    end

  end
end
