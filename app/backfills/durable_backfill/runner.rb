module DurableBackfill
  class Runner

    # One runner is executing per DB cluster, but rather than lookup shards for the cluster again
    # we can just start with the shard list.
    def initialize(shard_list)
      @shard_list = shard_list
    end

    def run
      # Randomize the order on shards so deploys don't create spikey patterns
      shard_list.shuffle.each do |shard|
        ActiveRecord::Base.on_shard(shard) do
          DurableBackfill::Manager.tasks.each do |task|
            execute_task(task, shard)
          end
        end
      end
    end

    private

    attr_reader :shard_list

    def report_task_progress(shard, task)
      progress = DurableBackfill::BackfillAudit.for_task(task.title).check_backfill_progress
      tags = ["task:#{task.title}", "shard_id:#{shard}"]
      statsd.gauge('completed', progress[:completed], tags: tags)
      statsd.gauge('left', progress[:left], tags: tags)
    end

    def statsd
      DurableBackfill::Manager.statsd
    end

    def execute_task(task, shard)
      task.audits.incomplete.find_in_batches do |batch|
        report_task_progress(shard, task)
        batch.each do |audit|
          run_task_against_one_audit(task, audit)
        end
      end
    end

    def run_task_against_one_audit(task, audit)
      ZendeskAPM.trace('durable_backfill.runner.run_task_against_one_audit', service: 'classic-durable-backfill') do |span|
        span.set_tag('zendesk.account_id', audit.account_id)
        span.set_tag('zendesk.account_subdomain', audit.account&.subdomain)
        span.set_tag('zendesk.durable_backfill.task_title', task.title)

        task.new(audit).work_audit
      end
    rescue StandardError => e
      failed_message = "#{task.title} failed for account_id: #{audit.account_id}"
      ZendeskExceptions::Logger.record(e, location: self, message: failed_message, fingerprint: 'cafedb0de6bb50d679482276e53059580234289c')
    end
  end
end
