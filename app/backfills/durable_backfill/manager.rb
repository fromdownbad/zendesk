require 'parallel'
require_relative 'task_list'

module DurableBackfill
  module Manager
    class << self
      # DurableBackfill::Manager.run
      # This is a singleton service (1 per pod) that runs the backfilling script.
      #
      # Script starts by creating BackfillAudit records (unless they are already created)
      # then the non-complete backfills are continually processed. The work is performed
      # by a DurableBackfill::Runner instance and there is one runner per pod.
      #
      # If an account whose backfill has not completed is moved into the shard it will start to be
      # processed within 3 minutes.
      #
      # The `single_run` argument is primarily a test hook. It allows the test to prevent an infinite
      # loop without monkey-patching.
      def run(single_run = false)
        loop do
          run_once
          sleep RUN_LOOP_PAUSE_DURATION
          break if single_run
        end
      end

      def run_once
        unless Arturo.feature_enabled_for_pod?(:durable_backfill_kill_switch, Zendesk::Configuration.fetch(:pod_id))
          generate_audits
          run_tasks
        end
      end

      # Some subclasses of Task are further abstractions on Task itself, and not a runnable task so
      # we filter those out when building the run list.
      def tasks
        @tasks ||= DurableBackfill::Task.descendants.reject { |task| task.abstract? }
      end

      def statsd
        @statsd ||= Zendesk::StatsD::Client.new(namespace: 'durable_backfill')
      end

      def logger
        @logger ||= ActiveSupport::TaggedLogging.new(Logger.new(STDOUT))
      end

      private

      RUN_LOOP_PAUSE_DURATION = 5.seconds

      private_constant :RUN_LOOP_PAUSE_DURATION

      def generate_audits
        tasks.each do |task|
          logger.info("generating audits for task #{task.title}")

          statsd.time('generate_audits.time', tags: ["task:#{task.title}"]) do
            ActiveRecord::Base.on_all_shards do |shard_id|
              task.create_audits!
            end
          end

          report_task_progress(task)
        end
      end

      def report_task_progress(task)
        statsd.gauge(
          'complete_percentage',
          DurableBackfill::BackfillAudit.for_task(task.title).complete_percentage,
          tags: ["task:#{task.title}"]
        )
      end

      def run_tasks
        DurableBackfill::Runner.new(db_shards).run
      end

      def db_shards
        Zendesk::Configuration::DBConfig.shards_to_clusters.keys
      end
    end
  end
end
