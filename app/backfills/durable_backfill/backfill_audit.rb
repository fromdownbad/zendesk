# BackfillAudit is an audit log of when a backfill task starts & finishes for every account

module DurableBackfill
  class BackfillAudit < ActiveRecord::Base
    WATERLINE_ACCOUNT_ID = -10

    default_scope { where("account_id != #{WATERLINE_ACCOUNT_ID}") }

    scope :for_task, ->(task_name) { where(task: task_name) }
    scope :incomplete, -> { where(completed_at: nil) }

    belongs_to :account

    # Is the datetime when the first time a task runs for any account + 1.day
    #   This time is used to ensure any account created after this point doesn't run the backfill.
    #
    #   The 1.day gives a buffer to ensure no conditions exist between pods deploying at different times and account moves
    #
    # @param [String] name of the task stored in the backfill_audits.task field
    # @return [Time] Time of the first task that ran for this job + 1.day
    def self.waterline_date(task)
      unscoped.where(account_id: WATERLINE_ACCOUNT_ID, task: task).first_or_create.created_at + 1.day
    end

    def self.mark_as_shard_completed(task)
      waterline_audit = unscoped.where(account_id: WATERLINE_ACCOUNT_ID, task: task).first
      waterline_audit.complete! if waterline_audit
    end

    # Returns number of tasks with different completion states in current DB shard, usually called for after specific task scope `for_task`
    #
    # @example
    #   DurableBackfill::BackfillAudit.for_task('some_task').check_backfill_progress #=> { completed: 500, in_progress: 50, left: 4000 }
    #
    # @return [Hash] { completed: @number_of_completed_tasks, in_progress: @number_of_tasks_tried_at_least_once_but_not_completed, left: @number_of_tasks_never_tried ]
    def self.check_backfill_progress
      {
        completed: where("completed_at is not null").count,
        in_progress: where("started_at is not null and completed_at is null").count,
        left: where("completed_at is null").count
      }
    end

    def self.complete_percentage
      total = 0
      completed = 0

      on_all_shards do |shard_id|
        status = check_backfill_progress
        completed += status[:completed]
        total += (status[:completed] + status[:left])
      end

      if total.zero?
        0
      else
        (completed.to_f/total*100).floor
      end
    end

    def self.print_backfill_progress
      width = 20
      row = "| %#{width}s | %#{width}s | %#{width}s | %#{width}s |\n"
      table = row % ['Shard', 'Complete', 'In Progress', 'Remaining']
      table.concat row % Array.new(4).map { |_| '-' * width }

      result = {}
      on_all_shards do |shard_id|
        status = check_backfill_progress
        table.concat row % [shard_id, status[:completed], status[:in_progress], status[:left]]
      end

      puts table
    end

    def self.accounts_not_yet_completed
      account_ids = []

      on_all_shards do |shard_id|
        account_ids_for_shard = where("completed_at is null").pluck(:account_id)
        account_ids += account_ids_for_shard
      end

      account_ids
    end

    def self.accounts_in_progress
      account_ids = []

      on_all_shards do |shard_id|
        account_ids_for_shard = where("started_at is not null and completed_at is null").pluck(:account_id)
        account_ids += account_ids_for_shard
      end

      account_ids
    end

    def self.accounts_not_yet_started
      account_ids = []

      on_all_shards do |shard_id|
        account_ids_for_shard = where("started_at is null and completed_at is null").pluck(:account_id)
        account_ids += account_ids_for_shard
      end

      account_ids
    end

    def start!
      update!(started_at: Time.now)
    end

    def complete!
      update!(completed_at: Time.now)
    end
  end
end
