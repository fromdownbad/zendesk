module DurableBackfill
  class BackfillJwtTokensRedact < Task
    self.title = 'backfill_jwt_tokens_redact'

    def work(account)
      raise RescheduleTask unless account.has_jwt_redact_backfill?
      redact_jwt_audits(account)
    end

    private

    def tokens_to_redact(account, rid)
      CIA::AttributeChange.where(source_type: 'RemoteAuthentication',
                                              attribute_name: 'token',
                                              source_id: rid,
                                              account_id: account.id)
    end

    def redact_jwt_audits(account)
      RemoteAuthentication.jwt.where(account_id: account.id).select(:id).find_in_batches do |batch|
        rids = batch.map(&:id)
        jwt_tokens = tokens_to_redact(account, rids)
        if jwt_tokens.blank?
          Rails.logger.info('BackfillJwtTokensRedact: Total records for JWT tokens = 0')
          return
        end

        Rails.logger.info("BackfillJwtTokensRedact: Total records for JWT tokens = #{jwt_tokens.count}")

        @updated_tokens = 0
        jwt_tokens.each do |jwt_token|
          old_value = jwt_token.old_value
          new_value = jwt_token.new_value
          redacted_old_value = redact_token(old_value)
          redacted_new_value = redact_token(new_value)
          jwt_token.update!(old_value: redacted_old_value,
                           new_value: redacted_new_value)
          Rails.logger.info("Redacted token: old_value to #{redacted_old_value}") if redacted_old_value != old_value
          Rails.logger.info("Redacted token: new_value to #{redacted_new_value}") if redacted_new_value != new_value
        end
        Rails.logger.info("BackfillJwtTokensRedact: Successfully corrected #{@updated_tokens} tokens")
      end
    end

    def already_redacted(token)
      redacted = token[6..token.size]
      redacted == "*" * redacted.size
    end

    def redact_token(token)
      return token if token.blank?
      return token if already_redacted(token)
      @updated_tokens += 1
      token[(0..5)] + '*' * (token.size - 6)
    end
  end
end
