module DurableBackfill
  class BackfillCustomRoleExplorePermission < Task
    self.title = 'backfill_custom_role_explore_permission'

    def work(account)
      raise RescheduleTask unless account.has_ocp_custom_role_explore_permission_backfill?
      return unless account.has_permission_sets?

      PermissionSetPermission.where(
        permission_set_id: account.permission_sets.pluck(:id),
        name: 'explore_access',
        value: 'none'
      ).update_all(value: 'readonly')
    end
  end
end
