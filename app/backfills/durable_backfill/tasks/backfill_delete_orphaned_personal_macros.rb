module DurableBackfill
  class BackfillDeleteOrphanedPersonalMacros < Task
    self.title = 'backfill_delete_orphaned_personal_macros'

    FINGERPRINT = self.title

    def work(account)
      reschedule! unless account.has_backfill_delete_orphaned_personal_macros?
      delete_orphaned_macros(account)
    end

    private

    def orphaned_personal_macros(account)
      Macro.joins(:user).where(
        users: {is_active: 0, account_id: account.id},
        rules: {account_id: account.id, owner_type: 'User'}
      )
    end

    def delete_orphaned_macros(account)
      macros_to_delete = orphaned_personal_macros(account)

      if macros_to_delete.any?
        begin
          macros_owner_id = macros_to_delete.pluck(:owner_id).uniq

          Macro.soft_delete_all!(macros_to_delete)
          Rails.logger.info("Owner IDs of deleted macros on account #{account.id}: #{macros_owner_id}")
        rescue StandardError => exception
          log_exception(account, exception)
        end
      else
        Rails.logger.info("Account #{account.id} has no orphaned macros")
      end

    end

    def log_exception(account, exception)
      ZendeskExceptions::Logger.record(
         exception,
         location: self,
         message: "Exception while running #{self.class.title} for account #{account.id}: #{exception}",
         fingerprint: FINGERPRINT
      )
    end
  end
end
