module DurableBackfill
  class BackfillSubscribeMonitoredTwitterHandlesViaProxyV4 < Task
    self.title = 'backfill_subscribe_monitored_twitter_handles_via_proxy_v4'

    def work(account)
      return true unless account.has_twitter_backfill_monitored_twitter_handles?

      MonitoredTwitterHandle.where(account_id: account.id).active.each do |handle|
        next unless handle.mention_autoconversion_enabled?

        Channels::Maintenance::SubscribeMonitoredTwitterHandleJob.work(handle.id)
        sleep 2
      end
    end
  end
end
