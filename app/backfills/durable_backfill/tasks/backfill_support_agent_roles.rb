module DurableBackfill
  class BackfillSupportAgentRoles < Task
    self.title = 'backfill_support_agent_roles'

    def work(account)
      raise RescheduleTask unless account.has_ocp_support_agent_roles_backfill?
      return unless account.has_permission_sets?

      staff_client(account).toggle_role!(
        name: Zendesk::SupportAccounts::Role::System::AGENT_ROLE_NAME,
        assignable: true
      )
    end
    
    private

    def staff_client(account)
      Zendesk::StaffClient.new(account)
    end
  end
end
