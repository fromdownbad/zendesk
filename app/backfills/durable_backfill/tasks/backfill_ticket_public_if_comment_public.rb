module DurableBackfill
  class BackfillTicketPublicIfCommentPublic < Task
    self.title = 'backfill_ticket_public_if_comment_public'

    def work(account)
      # Using the Create Many Tickets API intermittently sets the is_public value
      # of tickets to false even if a public comment is present.
      # In https://support.zendesk.com/agent/tickets/5046054 the account "cpanel"
      # had this problem and we need to backfill/fix the bad tickets
      # until we investigate and find the problem.
      return unless account.id == 9277979
      # The account has only 3926 tickets marked as private.
      account.tickets.where(is_public: false).find_in_batches do |batch|
        batch.each do |ticket|
          # Double check that the ticket is private
          # and that it has public comments.
          if ticket.is_private? && ticket.public_comments.exists?
            # This is one of the tickets with problems because
            # private tickets should never have public comments.
            # Let's fix the problem and set the correct value in the tickets table.
            ticket.update_column(:is_public, true)
          end
        end
      end
    end
  end
end
