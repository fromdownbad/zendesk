module DurableBackfill
  class BackfillFailedSpfVerification < Task
    self.title = 'backfill_failed_spf_verification'

    VERIFIED_SPF_STATUS = RecipientAddress::SPF_STATUS.fetch(:verified)
    # https://zendesk.slack.com/archives/C0184DJLTFS/p1595301526068900?thread_ts=1595294968.055100&cid=C0184DJLTFS
    INCIDENT_START_TIME = Time.at(1594229765) # 2020-07-08 17:36:05 +0000
    # https://zendesk.slack.com/archives/C0184DJLTFS/p1595301451067600?thread_ts=1595293996.050100&cid=C0184DJLTFS
    INCIDENT_END_TIME = Time.at(1595301438) # 2020-07-21 03:17:18 +0000

    def work(account)
      recipient_addresses = account.recipient_addresses

      return unless recipient_addresses.present?

      unverified_recipient_addresses = recipient_addresses.
        where('updated_at BETWEEN ? AND ?', INCIDENT_START_TIME, INCIDENT_END_TIME).
        where.not(spf_status_id: VERIFIED_SPF_STATUS)

      return unless unverified_recipient_addresses.present?

      unverified_recipient_addresses.each(&:verify_spf_status!)
    end
  end
end
