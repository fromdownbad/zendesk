module DurableBackfill
  class BackfillTicketDeflectionUpdatedAt < Task
    self.title = 'backfill_ticket_deflection_updated_at'

    def work(account)
      account_id = account.id

      logger.info("Starting backfill deflection updated_at for #{account_id}")
      stale_deflections(account).find_each(&method(:update))
      logger.info("Finished backfill deflection updated_at for #{account_id}")
    end

    private

    def update(deflection)
      deflection.update_column('updated_at', deflection.recent_article_updated_at)

      logger.info("Succeeded to backfill for deflection: #{deflection.id}")
    rescue StandardError => error
      logger.error("Failed to backfill for deflection: #{deflection.id} - #{error.message}")
    end

    def stale_deflections(account)
      account.ticket_deflections
        .joins(:ticket_deflection_articles)
        .select(
          'ticket_deflections.*',
          'MAX(ticket_deflection_articles.updated_at) AS recent_article_updated_at'
        )
        .group('ticket_deflections.id')
        .having('recent_article_updated_at > ticket_deflections.updated_at')
    end

    def logger
      @logger ||= DurableBackfill::Manager.logger
    end
  end
end
