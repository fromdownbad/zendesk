module DurableBackfill
  class BackfillAccountAttributeTicketMapCleanup < Task
    self.title = 'backfill_account_attribute_ticket_map_cleanup'

    def work(account)
      unless account.has_backfill_account_attribute_ticket_map_cleanup?
        reschedule!
      end

      deco = deco_client(account)

      atms = invalid_attribute_ticket_maps(account, deco).pluck(:id)
      delete_attribute_ticket_maps(account, atms)
    end

    private

    def deco_client(account)
      Zendesk::Deco::Client.new(account)
    end

    def delete_attribute_ticket_maps(account, atms)
      logger.info(
        "Attempting to delete invalid AttributeTicketMap(s) for account: #{account.id}."
      )

      AttributeTicketMap.soft_delete_all!(atms)
    rescue ActiveRecord::RecordInvalid => e
      logger.error(
        "Failed to delete invalid attribute values for account: #{account.id}."\
        " RecordInvalid: #{e.message}."
      )
    rescue StandardError => e
      logger.error("Exception while running #{self.class.title} for account #{account.id}: #{e.message}.")
    end

    # This is a request to deco to retrieve all attribute value ids
    # of the given account. While this is an N+1 query, we limit an account to up to
    # 10 attributes. Attribute values are limited to 1000. We'll be making an overall
    # of 10 requests to Deco per account. With the exception of a few that are allowed
    # to bypass this 10 attributes limit.
    # 
    def deco_attribute_value_ids(deco)
      deco.attributes.index[:attributes].map do |attribute|
        deco.attribute_values.index(attribute.id).map(&:id)
      end.flatten.uniq
    end

    def invalid_attribute_ticket_maps(account, deco)
      valid_attribute_value_ids = deco_attribute_value_ids(deco)

      account.attribute_ticket_maps.where.not(attribute_value_id: valid_attribute_value_ids)
    end

    def logger
      @logger ||= DurableBackfill::Manager.logger
    end
  end
end
