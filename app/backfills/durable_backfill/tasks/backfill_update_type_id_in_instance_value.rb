module DurableBackfill
  class BackfillUpdateTypeIdInInstanceValue < Task
    self.title = 'backfill_update_type_id_in_instance_value'
    FINGERPRINT = '218ef3be38e244e390ae6cb3d4fce537'.freeze

    # Logical constants
    STATUS_LIST = [StatusType.CLOSED, StatusType.ARCHIVED, StatusType.DELETED].freeze
    TICKET_TYPE = 10
    TICKET_SKILLS_CLOSED_TYPE = 11
    TICKET_CLOSED_SUBTYPE = 1
    BATCH_SIZE = 500

    # Throttle constants
    MINIMUM_BACKOFF = 0.1.seconds
    REENQUEUE_THRESHOLD = 8.minutes

    class InstanceValue < ActiveRecord::Base
      scope :all_tickets, -> { where(deleted_at: nil, type_id: [TICKET_TYPE, TICKET_SKILLS_CLOSED_TYPE]) }
    end

    def work(account)
      raise RescheduleTask unless account.has_sbrv2_instance_value_durable_backfill?
      return unless account.is_active? && account.has_skill_based_ticket_routing?

      shard = account.shard_id
      ActiveRecord::Base.on_shard(shard) do
        ActiveRecord::Base.on_slave do
          total_registers = all_inactive_tickets(account.id).count
          replace_subtype_id(account)

          statsd_client.increment("durable_backfill_completed", tags: ["subdomain:#{account.subdomain}", "total_registers:#{total_registers}"])
        end
      end
    end

    def killswitch_enabled(account)
      !Arturo.feature_enabled_for?(:sbrv2_instance_value_durable_backfill, account)
    end

    private

    def all_inactive_tickets(account_id)
      BackfillUpdateTypeIdInInstanceValue::InstanceValue.all_tickets.
        joins("LEFT JOIN `tickets` ON `tickets`.`id` = `instance_values`.`instance_id`").
        where("`instance_values`.`account_id` = #{account_id} AND `instance_values`.`subtype_id` = 0 "\
        "AND (`tickets`.`status_id` IN (#{STATUS_LIST.join(', ')}) OR `tickets`.`status_id` IS NULL)")
    end

    def replace_subtype_id(account)
      all_inactive_tickets(account.id).find_in_batches(batch_size: BATCH_SIZE).with_index do |instances, batch_index|
        raise RescheduleTask if killswitch_enabled(account)

        throttle_sql_call(account) if batch_index.even?
        instance_ids = instances.map(&:id)
        ActiveRecord::Base.on_master do
          InstanceValue.where(id: instance_ids).update_all(type_id: TICKET_TYPE, subtype_id: TICKET_CLOSED_SUBTYPE)
        end
      end
    rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotSaved => exception
      log_exception(account.id, exception)
      statsd_client.increment("durable_backfill_failed", tags: ["subdomain:#{account.subdomain}"])
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['BackfillUpdateTypeIdInInstanceValue'])
    end

    def log_exception(account_id, exception)
      ZendeskExceptions::Logger.record(
        exception,
        location: self,
        message: "Exception while running #{self.class.title} for account #{account_id}: #{exception}",
        fingerprint: FINGERPRINT
      )
    end

    def throttle_sql_call(account)
      @database_backoff ||= Zendesk::DatabaseBackoff.new(minimum: MINIMUM_BACKOFF)
      duration = @database_backoff.backoff_duration
      if duration > REENQUEUE_THRESHOLD
        sleep(REENQUEUE_THRESHOLD)
        statsd_client.increment("database_backoff", tags: ["subdomain:#{account.subdomain}", "duration:#{REENQUEUE_THRESHOLD}"])
      else
        statsd_client.increment("database_backoff", tags: ["subdomain:#{account.subdomain}", "duration:#{duration}"])
        sleep(duration)
      end
    end
  end
end
