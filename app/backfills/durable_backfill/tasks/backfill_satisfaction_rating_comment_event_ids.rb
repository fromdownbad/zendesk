module DurableBackfill
  class BackfillSatisfactionRatingCommentEventIds < Task
    self.title = 'backfill_satisfaction_rating_comment_event_ids'

    COMMENT_EVENT_ID_FIX_DATE = Time.zone.parse('2013-01-01Z')

    def work(account)
      raise RescheduleTask unless account.has_backfill_satisfaction_rating_comment_event_ids?

      return if account.created_at > COMMENT_EVENT_ID_FIX_DATE

      find_satisfaction_ratings(account) do |satisfaction_rating|
        comment_event_id = find_comment_event_id(satisfaction_rating)

        if comment_event_id
          satisfaction_rating.update_attribute(:comment_event_id, comment_event_id)
          logger.info("Backfilled satisfaction_rating #{satisfaction_rating.id} comment_event_id #{comment_event_id}")
        else
          logger.info("Did not backfill satisfaction_rating #{satisfaction_rating.id}")
        end
      end
    end

    private

    def find_satisfaction_ratings(account, &block)
      # TODO: Causes N+1 MySQL and Riak queries
      account.satisfaction_ratings
        .where('created_at < ? ', COMMENT_EVENT_ID_FIX_DATE)
        .where(comment_event_id: nil)
        .find_each(&block)
    end

    def find_comment_event_id(satisfaction_rating)
      ticket = satisfaction_rating.ticket
      event = satisfaction_rating.event

      if ticket && ticket.archived?
        event = ticket.events.detect { |e| e.id == satisfaction_rating.event_id }

        comment = ticket.events.detect do |c|
          c.is_a?(Change) && c.parent_id == event.parent_id && c.value_reference == 'satisfaction_comment'
        end
      elsif event
        parent_id = event.parent_id

        comment = Change.where(
          parent_id: parent_id,
          ticket_id: satisfaction_rating.ticket_id,
          value_reference: 'satisfaction_comment',
          account_id: satisfaction_rating.account_id
        ).first
      else
        # We have a shitty SatisfactionRating with no ticket and no event, do nothing
      end

      comment.try(:id)
    end
  end
end
