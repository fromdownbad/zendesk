module DurableBackfill
  class BackfillTicketDeflectionEnquiry < Task
    self.title = 'backfill_ticket_deflection_enquiry'

    def work(account)
      account.ticket_deflections.find_each do |ticket_deflection|
        return unless ticket_deflection.ticket_id # WW, SDK and API deflections wont have a ticket associated

        ticket = Ticket.find_by_id(ticket_deflection.ticket_id)

        ticket_deflection.update(enquiry: 'SCRUBBED') unless ticket
      end
    rescue StandardError => e
      logger.info("Failed to backfill enquiry for account_id: #{account.id}, error: #{e}.");
    end

    private

    def logger
      @logger ||= DurableBackfill::Manager.logger
    end
  end
end
