module DurableBackfill
  class BackfillUpdateCfcRisManifestV3 < Task
    self.title = 'backfill_update_cfc_ris_manifest'

    MANIFEST_URL = 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/manifest.json'.freeze

    def work(account)
      registered_integration_services = Channels::AnyChannel::RegisteredIntegrationService.
        where(account_id: account.id).
        where('manifest_url LIKE ?', '%connector.zendesk.ext.smooch.io%')

      registered_integration_services.each do |ris|
        manifest, = Channels::AnyChannelApi::Client.get_manifest(MANIFEST_URL)
        extended_manifest = ris.class.extend_manifest_urls(MANIFEST_URL, manifest)
        new_attributes = ris.class.manifest_attributes(extended_manifest, MANIFEST_URL, account.id)
        ris.update_attributes!(new_attributes)
      end
    rescue StandardError => e
      logger.info("Could not update manifest for account #{account.id}: #{e.message}")
    end
  end
end
