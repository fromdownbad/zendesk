module DurableBackfill
  class BackfillConvertFailedIncomingConversions < Task
    self.title = 'backfill_convert_failed_incoming_conversions'

    INCIDENT_START_TIME = Time.new(2020, 8, 04, 23, 00, 00)
    INCIDENT_END_TIME = Time.new(2020, 8, 05, 03, 05, 00)

    def work(account)
      # Fetch errored conversions to re-run
      conversions = Channels::IncomingConversion.where(
        account_id: account.id,
        state: 30
      ).where(
        'created_at BETWEEN ? AND ?', 
        INCIDENT_START_TIME,
        INCIDENT_END_TIME
      )

      conversions.each do |c|
        # udpate the state and reset the conversion attemps
        c.state = ::Channels::Converter::StateManager::UNCONVERTED
        c.metadata['conversion_attempt_number'] = 0
        c.save!

        # re-enqueue the incoming conversion
        c.enqueue_job('BackfillConvertFailedIncomingConversions#work')
      end
    rescue StandardError => e
      logger.info("Could not process incoming conversion for #{account.id}: #{e.message}")
    end
  end
end
