module DurableBackfill
  class BackfillExploreEntitlementsV2 < Task
    self.title = 'backfill_explore_entitlements_v2'

    def work(account)
      raise RescheduleTask unless account.has_ocp_explore_entitlements_backfill?
      return if !account.is_active? || account_client(account).product!(Zendesk::Accounts::Client::EXPLORE_PRODUCT).nil?
      sync_entitlements(account)
    rescue Kragle::ResourceNotFound
      Rails.logger.info "Explore product not active for #{account.id}, skipping"
    end

    def sync_entitlements(account)
      Omnichannel::ExploreAccountSyncJob.perform(account_id: account.id)
    end

    def account_client(account)
      Zendesk::Accounts::Client.new(account)
    end
  end
end
