module DurableBackfill
  class BackfillTargetsDeletePlaintextCredentialsV2 < Task
    self.title = 'backfill_targets_delete_plaintext_credentials_v2'

    ATTRIBUTES_TO_DELETE = [:password, :token, :api_token, :secret].freeze

    def work(account)
      delete_plaintext_credentials(account)
    end

    private

    def delete_plaintext_credentials(account)
      account.targets.each do |target|
        delete_credentials(target)
        if target.changed?
          target.save!(validate: false)
        end
      end
    end

    def delete_credentials(target)
      data = target.settings
      ATTRIBUTES_TO_DELETE.each { |k| data.delete(k) }
      target.settings=(data)
    end
  end
end
