module DurableBackfill
  class BackfillDeactivateUnusedTwitterHandlesV2 < Task
    self.title = 'backfill_channels_twitter_count_maintenance_v2'

    def work(account)
      unsubscribe_handles account, accounts_and_handles
    end

    private

    def accounts_and_handles
      @@accounts_handles_ids ||= read_handles_file
    end

    def read_handles_file
      file_path = Rails.root.join('app', 'backfills', 'backfill_deactivate_unused_handles_data_v2.csv')
      accounts_handles_ids = []
  
      # Run through each file line and get the account ID and Twitter handle ID
      CSV.foreach(file_path, headers: true) do |row|
        accounts_handles_ids << { account_id: row[1], twitter_user_id: row[2] }
      end
  
      accounts_handles_ids
    end
    
    def unsubscribe_handles(account, accounts_handles)
      accounts_handles.each do |acc_mth|
        next unless acc_mth[:account_id].to_s == account.id.to_s
        next unless account.pod_local?

        logger.info("Starting to check account id: #{acc_mth[:account_id]}, twitter_user_id: #{acc_mth[:twitter_user_id]}")
  
        begin          
          ActiveRecord::Base.on_shard account.shard_id do
            handle = MonitoredTwitterHandle.where(account_id: account.id, twitter_user_id: acc_mth[:twitter_user_id]).first
            if handle.nil?
              logger.info("Could not find a Monitored Twitter Handle for account_id: #{account.id}")
              next
            end
          
            logger.info("Deactivating and unsubscribing Monitored Twitter Handle with ID: #{handle.twitter_user_id} for Account: #{account.id}")
            # Deactivate the handle
            handle.deactivate!
            # Call unsubscribe job
            Channels::Maintenance::UnsubscribeMonitoredTwitterHandleJob.work(handle.id)
          end
        rescue ActiveRecord::RecordNotFound, ActiveRecord::AdapterNotSpecified => e
          logger.info("Could not deactivate handle for account with account_id: #{acc_mth[:account_id]}\nWith Error: #{e}")
        end
      end
  
      true
    end
  end
end
