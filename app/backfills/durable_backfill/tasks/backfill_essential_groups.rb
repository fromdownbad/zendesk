module DurableBackfill
  class BackfillEssentialGroups < Task
    self.title = 'backfill_essential_groups'

    def work(account)
      raise RescheduleTask unless account.has_backfill_essential_groups?
      return unless account.is_active?
      return unless account.subscription.patagonia? && account.subscription.plan_type == SubscriptionPlanType.Small

      Zendesk::Features::SubscriptionFeatureService.new(account).execute!
    end
  end
end
