module DurableBackfill
  class BackfillFacebookMonitorFeedMetadata < Task
    self.title = 'backfill_facebook_monitor_feed_metadata'

    def work(account)
      raise RescheduleTask unless account.has_facebook_metadata_backfill?
      resources_to_backfill = []

      shard = account.shard_id

      ActiveRecord::Base.on_shard(shard) do
        ActiveRecord::Base.on_slave do
          resources_to_backfill = Channels::Resource.joins('INNER JOIN incoming_channels_conversions i '\
            'ON i.account_id = channels_resources.account_id '\
              'AND i.resource_type = channels_resources.resource_type '\
              'AND i.external_id = channels_resources.external_id')
            .where(account_id: account.id, resource_type: Channels::Constants::FACEBOOK_POST)
            .where('channels_resources.external_id = channels_resources.thread_id')
            .where("i.metadata LIKE '%monitor: :feed%'")
            .all
        end
      end

      if resources_to_backfill.count < 1
        logger.info("No Channels resources to backfill in account #{account.id}")
        return
      end

      resources_to_backfill.each do |res|
        res.metadata = {} if res.metadata.nil?
        res.update_attributes!(metadata: res.metadata.merge({ 'monitor' => :feed }))
      rescue ActiveRecord::ActiveRecordError => e
        logger.info("ActiveRecordError: Could not add { 'monitor' => :feed } to metadata attribute in Channels resource #{res.id} in account #{res.account_id}: #{e.message}")
      end
    end
  end
end
