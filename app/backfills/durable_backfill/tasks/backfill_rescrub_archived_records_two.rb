module DurableBackfill
  class BackfillRescrubArchivedRecordsTwo < Task
    self.title = 'backfill_rescrub_archived_records_two'

    MINIMUM_BACKOFF = 0.2.seconds
    REENQUEUE_THRESHOLD = 8.minutes

    # This backfill rescrub archived ticket records that could have
    # ticket_field_entries that weren't scrubbed properly
    def work(account)
      rescrub_archived_records(account)
    end

    private

    def rescrub_archived_records(account)
      reschedule! unless account.has_rescrub_archived_records?
      return unless account.is_active?
      TicketArchiveStub.on_slave do
        TicketArchiveStub.unscoped.where(
          "ticket_archive_stubs.account_id = ? AND ticket_archive_stubs.status_id IN (?)",
          account.id,
          [StatusType.DELETED]
        ).
          where("ticket_archive_stubs.generated_timestamp > ?", DateTime.parse("12/12/2016")).
          where("ticket_archive_stubs.generated_timestamp < ?", DateTime.parse("13/01/2019")).
          find_each(batch_size: 100).with_index do |ticket_archive_stub, index|
          throttle_deletions if index % 10 == 0
          begin
            log_rescrub_record(ticket_archive_stub)
            ticket = Ticket.with_deleted { Ticket.find ticket_archive_stub.id }
            Zendesk::Scrub.scrub_ticket_and_specific_association(ticket, TicketFieldEntry)
          rescue StandardError => e
            statsd_client.increment("scrubber.error_rescrub_record", tags: ["ticket_id:#{ticket_archive_stub.id}"])
            ZendeskExceptions::Logger.record(e, location: self, message: error_message(ticket_archive_stub, e), fingerprint: '3407B944B4E0412B26E59F56A8D9E6773358FBB8')
          end
        end
      end

      log(account)
    end

    def throttle_deletions
      @database_backoff ||= Zendesk::DatabaseBackoff.new(minimum: MINIMUM_BACKOFF)
      duration = @database_backoff.backoff_duration
      Rails.logger.info @database_backoff.to_hash
      if duration > REENQUEUE_THRESHOLD
        sleep(REENQUEUE_THRESHOLD)
      else
        sleep(duration)
      end
    end

    def log_rescrub_record(ticket_archive_stub)
      statsd_client.increment("scrubber.rescrubbing_ticket")
    end

    def log(account)
      puts "Re-scrubbing complete for #{account.subdomain}"
    end

    def error_message(ticket_archive_stub, error)
      "Error in #{self.class.name.demodulize} processing ticket_id: #{ticket_archive_stub.id} - #{error.message}"
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['BackfillRescrubArchivedRecords'])
    end
  end
end
