module DurableBackfill
  class BackfillChannelsTwitterOauth2RefreshTokenV2 < Task
    self.title = 'backfill_channels_twitter_oauth2_refresh_token_v2'

    def work(account)
      logger.info "Starting backfill task to encrypt Twitter credentials for: #{account.id}"

      MonitoredTwitterHandle.where({
        account_id: account.id,
        encryption_key_name: nil
      }).find_each(batch_size: 100) do |handle|
        logger.info "Encrypting Twitter credentials for handle: #{handle.twitter_user_id}, account_id: #{handle.account_id}"

        handle.encrypted_access_token_value = handle.access_token
        handle.encrypted_secret_value = handle.secret
        handle.encrypt!
        handle.secret_updated_at = Time.now
        handle.save!
      end

      logger.info "Finished backfill task to encrypt Twitter credentials for: #{account.id}"
    end
  end
end
