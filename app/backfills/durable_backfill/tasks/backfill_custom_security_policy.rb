module DurableBackfill
  class BackfillCustomSecurityPolicy < Task
    self.title = 'backfill_custom_security_policy'

    FINGERPRINT = 'a8a6f913-d18c-488d-bda2-7d35054920d6'

    def work(account)
      raise RescheduleTask unless account.has_custom_security_policy_backfill?
      return if !account.is_active?

      begin
        if update_custom_security_policy_record?(account)        
          Rails.logger.info("Attempting to create a custom_security_policy for #{account.id}")

          # This will build from high in our case
          CustomSecurityPolicy.build_from_current_policy(account).save!
        end
      rescue StandardError => e
        log_exception(account, e)
      end
    end

    private

    def update_custom_security_policy_record?(account)
      account.role_settings.security_policy_id_for_role(:agent) == Zendesk::SecurityPolicy::Custom.id &&
        account.custom_security_policy.nil?
    end

    def log_exception(account, exception)
      ZendeskExceptions::Logger.record(
         exception,
         location: self,
         message: "Exception while running #{self.class.title} for account #{account.id}: #{exception}",
         fingerprint: FINGERPRINT
      )
    end
  end
end
