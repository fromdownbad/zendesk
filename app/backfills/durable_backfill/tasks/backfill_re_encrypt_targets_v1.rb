module DurableBackfill
  class BackfillReEncryptTargetsV1 < Task
    self.title = 'backfill_re_encrypt_targets_v1'

    def work(account)
      reschedule! unless account.has_backfill_rotate_target_encrypted_values?

      Rails.logger.info "Starting backfill task to re-encrypt targets for: #{account.id}"

      ::ReEncryptTargetCredentialsJob.work(account.id)

      Rails.logger.info "Finished backfill task to re-encrypt the targets for: #{account.id}"
    end
  end
end
