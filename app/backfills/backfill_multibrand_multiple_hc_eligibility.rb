class BackfillMultibrandMultipleHcEligibility < BackfillBase
  include ZendeskBillingCore::Zuora::Session

  attr_reader :start, :batch_size, :shard_number

  def initialize(options = {})
    super
    @batch_size   = options.fetch(:batch_size, 10000)
    @start        = options.fetch(:start, 1)
    @shard_number = options.fetch(:shard_number, nil)
  end

  def perform
    @updated                  = 0
    @needs_updating           = 0
    @failed_to_update         = 0
    @total_accounts_inspected = 0

    shard_number.present? ? run_on_shard(shard_number) : run_on_pod

    logger.info <<~TXT
      Backfill completed!
        Pod: #{pod}
        Accounts found for update: #{@needs_updating}
        Accounts actually updated: #{@updated}
        Accounts failed to update: #{@failed_to_update}
        Total accounts inspected:  #{@total_accounts_inspected}
        On shard?: #{shard_number.present?}
        On pod?: #{shard_number.nil?}
        Dry run?: #{dry_run}
    TXT
  end

  def run_on_shard(shard)
    ActiveRecord::Base.on_shard(shard) do
      backfill_multibrand_multiple_help_centers
    end
  end

  def run_on_pod
    on_shards do
      backfill_multibrand_multiple_help_centers
    end
  end

  def backfill_multibrand_multiple_help_centers
    @list_of_accounts = active_purchased_accounts

    @list_of_accounts.find_in_batches(start: start, batch_size: batch_size) do |batch|
      batch.each do |account|
        validate_and_update_accounts(account)
      end
    end

    @total_accounts_inspected += @list_of_accounts.count
  end

  def validate_and_update_accounts(account)
    @log = "dry_run=#{dry_run} account_id=#{account.id} in pod=#{pod} "
    if account_aligned?(account)
      logger.info @log << "is being skipped since it's already up-to-date"
    else
      @needs_updating += 1
      if dry_run
        logger.info @log << "needs updating but is being skipped since this is a dry run"
      else
        update_zuora_customfield_and_account_setting(account)
      end
    end
  end

  def account_aligned?(account)
    @multihc_field_value = needs_grandfathering?(account)
    account.settings.multibrand_includes_help_centers == @multihc_field_value
  end

  def update_zuora_customfield_and_account_setting(account)
    return unless update_zuora_multi_hc_custom_field(account)
    query_params = { name: 'multibrand_includes_help_centers' }
    account.settings.where(query_params).first_or_initialize.tap do |setting|
      setting.value = @multihc_field_value
      setting.save!
    end
  end

  def active_purchased_accounts
    Account.shard_local.joins(:subscription).where(subscription_query_options).where(account_query_options)
  end

  def update_zuora_multi_hc_custom_field(account)
    zuora_account_id = zuora_account_id(account)
    zuora_session.update(
      zuora_account_id,
      multi_brand_includes_multi_hc__c: @multihc_field_value
    )
    logger.info @log << "has been updated_to=#{@multihc_field_value} for zuora_account_id=#{zuora_account_id}"
    @updated += 1
    true
  rescue ZuoraClient::UpdateFailure, ZuoraSubscriptionNotPresent => e
    logger.error @log << "failed to update for zuora_account_id=#{zuora_account_id} error_obtained=#{e}"
    @failed_to_update += 1
    false
  end

  def subscription_query_options
    {
      subscriptions: {
        is_trial: false,
        payment_method_type: Zendesk::Types::PaymentMethodType.Zuora
      }
    }
  end

  def account_query_options
    {
      accounts: {
        is_active: true,
        is_serviceable: true
      }
    }
  end

  def needs_grandfathering?(account)
    eligible_plan_types.include?(account.subscription.plan_type)
  end

  def eligible_plan_types
    professional_plan_type = ZBC::Zendesk::PlanType::Professional.plan_type
    enterprise_plan_type = ZBC::Zendesk::PlanType::Enterprise.plan_type
    [professional_plan_type, enterprise_plan_type]
  end

  def zuora_session
    @session ||= session.account
  end

  def zuora_account_id(account)
    zuora_account_id = account.subscription.zuora_subscription.try(:zuora_account_id)
    zuora_account_id ? zuora_account_id : (raise ZuoraSubscriptionNotPresent)
  end

  def logger
    @logger ||= Logger.new(Dir.pwd + "/log/backfill_multibrand_multiple_hc_eligibility.log")
  end
end

class ZuoraSubscriptionNotPresent < StandardError; end
