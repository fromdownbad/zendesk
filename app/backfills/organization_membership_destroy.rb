class OrganizationMembershipDestroy < BackfillBase
  # This backfill deletes some memberships that should have been deleted when their organization was soft deleted
  attr_reader :organization_stats, :account_id

  def initialize(**args)
    @account_id = args[:account_id]
    super args
  end

  def perform
    Rails.logger.info("OrganizationMembershipDestroy: Starting dry_run=#{dry_run}, pod=#{pod}")
    on_shards { |s| backfill_shard(s) }
  end

  private

  def backfill_shard(shard)
    Rails.logger.info("OrganizationMembershipDestroy: Processing shard #{shard}")
    destroy_default_memberships
    delete_remaining_memberships
  end

  # Destroying the default organization memberships will result in the users being assigned a new
  # valid default organization via callbacks
  def destroy_default_memberships
    process_memberships do |organization|
      organization.organization_memberships.where(default: true).find_each do |membership|
        Rails.logger.info("Destroying #{membership.inspect}")
        membership.destroy unless dry_run
      end
    end
  end

  def delete_remaining_memberships
    process_memberships do |organization|
      deleted = []
      organization.organization_memberships.find_each do |membership|
        membership.delete unless dry_run
        deleted << membership.user_id
      end
      Rails.logger.info("OrganizationMembershipDestroy: Deleted #{deleted.size} memberships for organization #{organization.id}, users: #{deleted}") if deleted.any?
    end
  end

  def process_memberships(&block)
    Organization.with_deleted do
      scope = Organization
      scope = scope.where(account_id: account_id) if account_id.present?
      scope.where("organizations.deleted_at IS NOT NULL").find_each(&block)
    end
  end
end
