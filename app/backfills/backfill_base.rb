class BackfillBase

  attr_reader :dry_run,
              :verbose,
              :pod,
              :pods,
              :shards

  def self.perform(options = {})
    new(options).perform
  end

  def initialize(options = {})
    @dry_run = options.fetch(:dry_run, :true)
    @verbose = options.fetch(:verbose, :true)
    @pod     = options.fetch(:pod, Zendesk::Configuration.fetch(:pod_id))
    @pods    = options.fetch(:pods, [pod])
    @shards  = options.fetch(:shards) { pod_shards }
  end

  def perform
    fail NotImplementedError
  end

  def on_shards
    shards.each do |shard|
      ActiveRecord::Base.on_shard(shard) do
        yield shard
      end
    end
  end

  private

  def pod_shards
    pods.inject([]) do |shards, pod|
      shards.concat(shards_in_pod(pod))
    end
  end

  def shards_in_pod(pod)
    Zendesk::Configuration::PodConfig.shards_in_pod(pod)
  end

  def log_info(message)
    Rails.logger.info "#{self.class}: #{message}"
  end
end
