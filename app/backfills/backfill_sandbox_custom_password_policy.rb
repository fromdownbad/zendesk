class BackfillSandboxCustomPasswordPolicy < BackfillBase
  def perform
    Rails.logger.info("BackfillSandboxCustomPasswordPolicy: Start; dry_run:#{dry_run}")

    account_ids = RoleSettings.where(agent_security_policy_id: 400).joins(:account).select { |rs| rs.account.is_serviceable? && rs.account.is_active? }.sort_by { |rs| rs.account.pod_id }.map(&:account_id)

    Account.where(id: account_ids).each do |account|
      if account.is_sandbox?
        ActiveRecord::Base.on_shard account.shard_id do
          unless account.custom_security_policy
            if dry_run
              Rails.logger.info("Sandox Account: #{account.id} custom password to be created from its sandbox master")
            else
              account.custom_security_policy = account.sandbox_master.custom_security_policy
              account.custom_security_policy.save!
              Rails.logger.info("Sandox Account: #{account.id} custom password policy was created from sandbox master")
            end
          end
        end
      end
    end
  end
end
