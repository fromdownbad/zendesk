class MonitorAssumeEventTicketRequiredBackfill < BackfillBase

  class MonitorAccountAssumeEvent < ActiveRecord::Base
    not_sharded
  end

  def perform
    Rails.logger.info("MonitorAssumeEventTicketRequired: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards do
      owners_on_this_shard = Account.shard_local.pluck(:owner_id).compact

      test_accounts_on_this_shard = User.where(id: owners_on_this_shard)
        .joins(:identities).where(user_identities: { type: 'UserEmailIdentity' })
        .where("`user_identities`.`value` like '%@zendesk.com'")
        .group('`users`.`account_id`')
        .pluck('`users`.`account_id`')

      update_scope = MonitorAccountAssumeEvent.where(account_id: test_accounts_on_this_shard)

      events_count = update_scope.count
      Rails.logger.info("MonitorAssumeEventTicketRequired will update #{events_count} records. Account IDs: #{test_accounts_on_this_shard.join(', ')}")

      unless dry_run
        updated_records = update_scope.update_all(ticket_required: false)
        Rails.logger.info("MonitorAssumeEventTicketRequired updated #{updated_records} records")
      end
    end
  end
end
