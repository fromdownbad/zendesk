class BackfillBlankedSettings < BackfillBase

  def initialize(options = {})
    super(options)
    @account_ids = options.fetch(:account_ids, nil)
  end

  def perform
    @account_ids.each do |account_id|
      account = Account.find account_id
      account.shard

      # Ignore deleted accounts
      next if !account.is_active?

      # Retrieve audit events since start of incident
      events = account.cia_events.where("created_at > ? AND created_at < ?", Time.new(2020, 03, 30), Time.new(2020,04,02))

      events.each do |event|
        # Only look at ones that updated Account service
        next unless event.message&.starts_with?('Update') && event.message.ends_with?('account product via Account Service')

        # Get product they are related to and skip Support.  Fixed in seperate backfill
        product = event.message.split.second.downcase
        next if product == Zendesk::Accounts::Client::SUPPORT_PRODUCT

        # Look for changes where settings were wiped ignore events where it didn't happen
        changes = event.attribute_changes.where(new_value: "")
        next if changes.empty?

        # Build plan_settings hash of old values except insights that Craig already fixed
        plan_settings = {}
        changes.each do |change|
          plan_settings[change.attribute_name] = change.old_value unless change.attribute_name == 'insights'
        end

        # Update account service
        account_client(account).update_product!(product, { product: { plan_settings: plan_settings } }, context: 'backfill_blanked_settings', include_deleted_account: true)
      end
    end
  end

  def account_client(account)
    Zendesk::Accounts::Client.new(account.id, account.subdomain)
  end
end
