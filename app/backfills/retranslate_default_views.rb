class RetranslateDefaultViews < BackfillBase

  PLACEHOLDERS = {
    'txt.default.views.your_unsolved.title'    => '{{zd.your_unsolved_tickets}}',
    'txt.default.views.group_unsolved.title'   => '{{zd.unsolved_tickets_in_group}}',
    'txt.default.views.new_in_groups.title'    => '{{zd.new_tickets_in_group}}',
    'txt.default.views.recently_solved.title'  => '{{zd.recently_solved_tickets}}',
    'txt.default.views.all_unsolved.title'     => '{{zd.all_unsolved_tickets}}',
    'txt.default.views.unassigned.title'       => '{{zd.unassigned_tickets}}',
    'txt.default.views.recently_updated.title' => '{{zd.recently_updated_tickets}}',
    'txt.default.views.pending.title'          => '{{zd.pending_tickets}}',
    'txt.default.views.current_tasks.title'    => '{{zd.current_tasks}}',
    'txt.default.views.overdue_tasks.title'    => '{{zd.overdue_tasks}}',
    'txt.models.account.customer_satisfaction_support.default_view_name' => '{{zd.recently_rated_tickets}}'
  }.freeze

  VIEW_NAME_KEYS = PLACEHOLDERS.keys.freeze

  BATCH_SIZE = 500.freeze

  def initialize(options = {})
    super(options)
    @account_id = options[:account_id].presence
  end

  def perform
    if @account_id
      Account.find(@account_id).on_shard do |account|
        perform_on_account(account)
      end
    else
      perform_on_all_accounts
    end
  end

  private

  def perform_on_all_accounts
    on_shards do |shard|
      in_each_locale do |locale|
        for_each_view_title(locale) do |old_title, new_title|
          update_views_matching_title(old_title, new_title)
        end
      end
      update_setting_for_accounts_on_shard
    end
  end

  def perform_on_account(account)
    ActiveRecord::Base.transaction do
      in_each_locale do |locale|
        for_each_view_title(locale) do |old_title, new_title|
          update_views_matching_title(old_title, new_title, account.id)
        end
      end
    end
  end

  def in_each_locale
    TranslationLocale.find_each do |locale|
      yield(locale)
    end
  end

  def for_each_view_title(locale)
    VIEW_NAME_KEYS.each do |key|
      old_title = I18n.t(key, locale: locale)
      new_title = PLACEHOLDERS[key]
      yield(old_title, new_title)
    end
  end

  def update_views_matching_title(title, new_title, account_id = nil)
    View.with_deleted do
      views = View.where(title: title)
      views = views.where(account_id: account_id) if account_id

      count = views.count(:all)

      Rails.logger.info "Found #{count} views for account #{account_id || 'all accounts'} to update from '#{title}' to '#{new_title}'"

      return if dry_run || count == 0

      ActiveRecord::Base.transaction do
        views.pluck(:id).each_slice(BATCH_SIZE) do |ids|
          View.unscoped.where(id: ids).update_all(title: new_title)
        end

        account_ids = (Array(account_id) + views.pluck(:account_id)).uniq
        update_setting_for_accounts(account_ids)
      end
    end
  end

  def update_setting_for_accounts_on_shard
    Account.shard_local.shard_unlocked.find_each do |account|
      update_account_setting(account)
    end
  end

  def update_setting_for_accounts(account_ids)
    account_ids.each do |account_id|
      update_account_setting Account.find(account_id)
    end
  end

  def update_account_setting(account)
    return if dry_run
    account.save!(validate: false)
  end
end
