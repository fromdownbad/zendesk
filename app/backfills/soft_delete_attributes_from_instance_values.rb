class SoftDeleteAttributesFromInstanceValues < BackfillBase
  attr_reader :account_id, :rollback

  def initialize(options = {})
    super
    @account_id = options.fetch(:account_id)
    @rollback = options.fetch(:rollback, false)
    @attribute_name = options.fetch(:attribute_name, nil)
    @attribute_id = options.fetch(:attribute_id, nil)
    @attribute_value_name = options.fetch(:attribute_value_name, nil)
    @attribute_value_id = options.fetch(:attribute_value_id, nil)
    @uuid = options.fetch(:uuid, nil)
    @table = options.fetch(:table, nil)
    @deleted_at = DateTime.parse(options.fetch(:deleted_at)).to_time
  end

  # SoftDeleteAttributesFromInstanceValues.new(account_id: account.id, table: 'attributes',
  # attribute_id: '11EABC706F7355D1BE5D11A308FAE173', deleted_at: '2020-07-06 12:00:00', dry_run: false).perform
  # deleted_at is a required parameter
  # set rollback: true if you have to rollback the backfill
  def perform
    Rails.logger.info("SoftDeleteAttributesFromInstanceValues: Starting dry_run=#{dry_run}")
    account = Account.find_by(id: @account_id)

    return unless account;

    return if dry_run

    shard = account.shard_id
    ActiveRecord::Base.on_shard(shard) do
      case @table
      when 'attributes'
        update_attributes
      when 'attribute_values'
        update_attribute_values
      else
        Rails.logger.info("UpdateTypeIdInInstanceValue: Invalid Table")
      end
    end
  end

  def uuid_to_hex(uuid)
    return unless uuid.size == 36 && uuid.count('-') == 4
    uuid_splited = uuid.split('-')
    "#{uuid_splited[2]}#{uuid_splited[1]}#{uuid_splited[0]}#{uuid_splited[3]}#{uuid_splited[4]}"
  end

  private

  def update_attributes
    conditions = {}
    conditions[:id] = "UNHEX('#{@attribute_id}')" if @attribute_id
    hex_str = uuid_to_hex(@uuid) if @uuid
    conditions[:id] = "UNHEX('#{hex_str}')" if conditions[:id].nil? && hex_str
    conditions[:name] = @attribute_name if @attribute_name

    return if conditions.empty?

    conditions[:account_id] = @account_id

    results = prepare SoftDeleteAttributesFromInstanceValues::Attribute, conditions
    update results
  end

  def update_attribute_values
    conditions = {}
    conditions[:id] = "UNHEX('#{@attribute_value_id}')" if @attribute_value_id
    hex_str = uuid_to_hex(@uuid) if @uuid
    conditions[:id] = "UNHEX('#{hex_str}')" if conditions[:id].nil? && hex_str
    conditions[:name] = @attribute_value_name if @attribute_value_name
    conditions[:attribute_id] = "UNHEX('#{@attribute_id}')" if @attribute_id

    return if conditions.empty?

    conditions[:account_id] = @account_id

    results = prepare SoftDeleteAttributesFromInstanceValues::AttributeValue, conditions
    update results
  end

  def prepare(query, conditions)
    conditions.each do |key, value|
      query = if [:account_id, :id].include?(key)
        query.where("#{key} = #{value}")
      else
        query.where("#{key} = ?", value)
      end
    end
    query
  end

  def update(results)
    if results.size != 1
      Rails.logger.info("UpdateTypeIdInInstanceValue: more than one result")
    elsif @deleted_at <= results.first.created_at.to_time
      Rails.logger.info("UpdateTypeIdInInstanceValue: invalid deleted_at")
    else
      if @rollback
        results.first.restore(@deleted_at)
      else
        results.first.soft_delete(@deleted_at)
      end
      results.first.update_attribute_ticket_maps(@deleted_at)
    end
  end

  class BaseClass < ActiveRecord::Base
    self.abstract_class = true

    attr_accessor :related
    def uuid
      @uuid ||= begin
        hex_str = id.unpack('H*')
        hex_str = hex_str.first
        "#{hex_str[8...16]}-#{hex_str[4...8]}-#{hex_str[0...4]}-#{hex_str[16...20]}-#{hex_str[20...hex_str.size]}"
      end
    end

    def related?
      related.count > 0
    end

    def soft_delete(time_of_deletion)
      self.deleted_at = time_of_deletion unless deleted_at
      save!
      update_related(time_of_deletion)
    end

    def restore(time_of_deletion)
      return unless deleted_at.nil? || time_of_deletion == deleted_at
      self.deleted_at = nil
      save!
      update_related(time_of_deletion)
    end

    def update_related(time_of_deletion)
      return unless related?
      Rails.logger.info("#{self.class} Changing #{related.size} related records")
      if deleted_at
        related.find_each { |record| record.soft_delete(time_of_deletion) }
      else
        related.find_each { |record| record.restore(time_of_deletion) }
      end
    end

    def update_attribute_ticket_maps(time_of_deletion)
      return if attribute_ticket_maps.empty?
      Rails.logger.info("#{self.class} Changing #{attribute_ticket_maps.size} related records in 'attribute_ticket_maps'")
      if deleted_at
        attribute_ticket_maps.find_each { |record| record.soft_delete(time_of_deletion) }
      else
        attribute_ticket_maps.find_each { |record| record.restore(time_of_deletion) }
      end
    end
  end

  class Attribute < SoftDeleteAttributesFromInstanceValues::BaseClass
    has_many :attribute_values
    scope :not_deleted, -> { where(deleted_at: nil) }
    scope :deleted, -> { where.not(deleted_at: nil) }
    alias :related :attribute_values

    def attribute_ticket_maps
      @attribute_ticket_maps ||= begin
        SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.where(attribute_id: uuid, account_id: account_id)
      end
    end
  end

  class AttributeValue < SoftDeleteAttributesFromInstanceValues::BaseClass
    has_many :instance_values
    scope :not_deleted, -> { where(deleted_at: nil) }
    scope :deleted, -> { where.not(deleted_at: nil) }
    alias :related :instance_values

    def attribute_ticket_maps
      @attribute_ticket_maps ||= begin
        SoftDeleteAttributesFromInstanceValues::AttributeTicketMap.where(attribute_value_id: uuid, account_id: account_id)
      end
    end
  end

  class InstanceValue < SoftDeleteAttributesFromInstanceValues::BaseClass
    scope :not_deleted, -> { where(deleted_at: nil) }
    scope :deleted, -> { where.not(deleted_at: nil) }
    def related?
      false
    end
  end

  class AttributeTicketMap < SoftDeleteAttributesFromInstanceValues::BaseClass
    scope :not_deleted, -> { where(deleted_at: nil) }
    scope :deleted, -> { where.not(deleted_at: nil) }
    def related?
      false
    end
  end
end
