require 'subscription'

class FixNonBoostableSubscriptionFeatures < BackfillBase
  include AccountQueryable

  attr_reader :names

  def initialize(options = {})
    super
    initialize_account_queryable_options(options)

    @names = options[:names] || raise('Must init with names')
  end

  def perform
    Rails.logger.info("FixNonBoostableSubscriptionFeatures: Starting dry_run=#{dry_run}, pods=#{pods.join(', ')}, names=#{names}")
    return if dry_run
    on_shards { backfill_shard }
    nil
  end

  private

  def backfill_shard
    account_query.find_each { |account| backfill_account(account) }
  end

  def backfill_account(account)
    return unless account.use_feature_framework_persistence?

    account.subscription.features.where(name: names).update_all(value: '1')
    Zendesk::Features::SubscriptionFeatureService.new(account).execute!
  end
end
