class SetTrialExpirationDateForOpenEndedTpeTrials < BackfillBase
  def perform
    puts "SetTrialExpirationDateForOpenEndedTpeTrials: Starting dry_run=#{dry_run}, pod=#{pod}"

    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    puts "SetTrialExpirationDateForOpenEndedTpeTrials: backfill shard #{ActiveRecord::Base.current_shard_id}"

    Voice::PartnerEditionAccount.find_each_for_shard(batch_size: 100) { |vpea| backfill_account(vpea) }
  end

  def backfill_account(vpea)
    account = vpea.account
    trial = false
    trial_expiration_date = vpea.trial_expires_at

    if !vpea.legacy? && !account.has_talk_cti_partner? && !account.tpe_subscription
      trial = true

      if vpea.active
        trial_expiration_date = trial_expiration_date || 30.days.from_now.to_date
      else
        trial_expiration_date = nil
      end
    end

    if vpea.trial_expires_at != trial_expiration_date
      puts "SetTrialExpirationDateForOpenEndedTpeTrials, Account #{account.id}-- #{account.subdomain}: \
        Changing trial_expiration_date: #{vpea.trial_expires_at || 'null'} -> #{trial_expiration_date}"
    end

    if vpea.trial != trial
      puts "SetTrialExpirationDateForOpenEndedTpeTrials, Account #{account.id}-- #{account.subdomain}: \
        Changing trial: #{vpea.trial || 'null'} -> #{trial}"
    end

    vpea.update_attributes(trial_expires_at: trial_expiration_date, trial: trial) unless dry_run
  end
end
