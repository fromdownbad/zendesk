class BackfillMobileAppAccess < BackfillBase

  def perform
    puts "BackfillMobileAppAccess: Starting dry_run=#{dry_run}, pod=#{pod}"

    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    shard = ActiveRecord::Base.current_shard_id
    puts "BackfillMobileAppAccess: backfilling shard #{shard}"

    accounts_to_update = AccountSetting.where(name: 'api_password_access', value: '0').pluck(:account_id)

    accounts = Account.where(id: accounts_to_update, shard_id: shard)
    puts "Found #{accounts.size} accounts on shard #{shard} to backfill"
    accounts.each {|a| backfill_account(a) }
    nil
  end

  def backfill_account(account)
    if account.settings.api_password_access == "0"
      account.settings.mobile_app_access = false
      account.settings.save! unless dry_run
    end
  end
end
