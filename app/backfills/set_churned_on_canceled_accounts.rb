class SetChurnedOnCanceledAccounts < BackfillBase

  ##
  # Set Churned On date for all accounts that have already been canceled.
  # So that they match new cancelled account moving forward. All of which will also get a Churned On date.
  #
  # To be run once per pod.
  ##

  def perform

    canceled_accounts.find_in_batches do |batch|
      batch.each do |account|
        account.on_shard do
          if account.pod_local? && ! account.is_serviceable? && ! account.is_active && account.subscription.canceled_on.nil?
            Rails.logger.info "Setting churned on date for previously canceled account #{account.idsub} (dry_run: #{dry_run}"
            set_churned_on(account.subscription)
          end
        end
      end
    end
  end

  private

  def shard_ids
    Account.send(:current_pod_shard_ids)
  end

  def canceled_accounts
    Account.joins(:subscription).where(shard_id: shard_ids, is_active: false, is_serviceable: false)
  end

  def set_churned_on(subscription)
    subscription.update_column(:churned_on, Time.now) unless dry_run
  end
end
