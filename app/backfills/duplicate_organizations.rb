require 'csv'

class DuplicateOrganizations < BackfillBase
  # This backfill is needed to deal with organizations with duplicate names.
  attr_reader :organization_stats, :destroyed, :renamed

  def initialize(**args)
    @destroyed = @renamed = 0
    @organization_stats = []
    super args
  end

  def perform
    Rails.logger.info("DuplicateOrganizations: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards { backfill_shard }

    csv_path = Rails.root.join('log', 'duplicate_orgs.csv')
    CSV.open(csv_path, 'w', col_sep: ",", force_quotes: true) do |csv|
      csv << %w(duplicate count tickets tickets(avg) users users(avg))
      organization_stats.each { |stats| csv << stats.to_a }
    end

    Rails.logger.info("DuplicateOrganizations: Renamed #{@renamed}")
    Rails.logger.info("DuplicateOrganizations: Saved CSV to #{csv_path}")

    Rails.logger.info("DuplicateOrganizations: Finished dry_run=#{dry_run}, pod=#{pod}")
  end

  private

  def backfill_shard
    DuplicateOrganization.with_deleted do
      duplicate_count.each do |(name, account_id), _count|
        duplicates = DuplicateOrganization.where(name: name, account_id: account_id).to_a.sort!
        primary_organization = duplicates.first
        duplicates.each_with_index do |organization, i|
          update_stats_for(organization, i)
          next if i == 0 # Do not process primary organization
          process_duplicate(organization, primary_organization, i)
        end
      end
    end
  end

  # { ['name', account_id] => count }
  def duplicate_count
    Organization.with_deleted do
      Organization.group(:name, :account_id).having('count(organizations.id) > 1').count
    end
  end

  def process_duplicate(organization, primary_organization, index)
    return if dry_run
    organization.rename_duplicate!(index) and @renamed += 1
    Rails.logger.info("DuplicateOrganizations: Renamed Organization #{organization.id}: #{organization.name}")
  end

  # Keep stats for each duplicate organization in this form:
  # [
  #   OrganizationsStats, # The primary organizations
  #   OrganizationsStats, # The duplicate organizations
  #   OrganizationStats, # If there are 2 duplicates, the second duplicate
  #   ...
  # ]
  def update_stats_for(organization, index)
    stats = organization_stats_for(index)
    stats.increment_count
    stats.add_users(organization.users_count)
    stats.add_tickets(organization.tickets_count)
  end

  def organization_stats_for(i)
    @organization_stats[i] ||= OrganizationStats.new(i)
  end

  class DuplicateOrganization < ::Organization
    def rename_duplicate!(index)
      added_characters = "\u200b" * index
      update_column(:name, "#{name}#{added_characters}")
    end

    # Sort by the highest total entries down to least
    def <=>(other_duplicate_organization)
      other_duplicate_organization.total_entries <=> total_entries
    end

    def tickets_count
      Ticket.with_deleted do
        tickets.size
      end
    end

    def users_count
      organization_memberships.size
    end

    def total_entries
      tickets_count + users_count
    end
  end

  class OrganizationStats
    attr_reader :duplicate_index, :organizations_count, :total_user_count, :total_ticket_count

    def initialize(index)
      @duplicate_index = index
      @organizations_count = @total_user_count = @total_ticket_count = 0
    end

    def increment_count(count = 1)
      @organizations_count += count
    end

    def add_tickets(ticket_number)
      @total_ticket_count += ticket_number
    end

    def add_users(user_number)
      @total_user_count += user_number
    end

    def to_a
      [duplicate_index, organizations_count, total_ticket_count, average_ticket_count, total_user_count, average_user_count]
    end

    def average_ticket_count
      organizations_count > 0 ? (total_ticket_count / organizations_count.to_f).round(5) : 0
    end

    def average_user_count
      organizations_count > 0 ? (total_user_count / organizations_count.to_f).round(5) : 0
    end
  end
end
