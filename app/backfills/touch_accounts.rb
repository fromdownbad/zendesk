class TouchAccounts < BackfillBase
  def initialize(options = {})
    super
    @batch_size = options.fetch(:batch_size, 500)
  end

  def perform
    Account.find_in_batches(batch_size: @batch_size) do |accounts|
      puts "Updating accounts with IDs in range: #{accounts.first.id}-#{accounts.last.id}"
      accounts.each do |account|
        touch(account)
      end
    end
  end

  private

  def touch(account, timestamp = Time.now)
    message = {
      dry_run: dry_run,
      account_name: account.name,
      account_id: account.id,
      updated_at: {
        previous_value: account.updated_at,
        new_value: timestamp
      }
    }
    account.update_column(:updated_at, timestamp) unless dry_run
    puts message.to_json
  end
end
