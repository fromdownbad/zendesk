# This job was created just to repurpose the 'accounts.multiproduct' field to denote shell accounts only.
class BackfillAccountMultiproductField < BackfillBase

  def initialize(options = {})
    super(options)
    @account_ids = options.fetch(:account_ids, nil)
  end

  def perform
    # Only pickup accounts that have a subscription record, as initially, any shell account created won't have support
    # This job should NOT be run after shell accounts are a default thing.
    affected = query.update_all(multiproduct: false) unless dry_run
    Rails.logger.info "Affected #{affected.to_i} accounts during this backfill"
  end

  private

  def query
    @query ||= begin
      base = Account.unscoped.joins(:subscription).where(multiproduct: true)
      if @account_ids
        base.where(id: @account_ids)
      else
        base.where(shard_id: shards)
      end
    end
  end
end
