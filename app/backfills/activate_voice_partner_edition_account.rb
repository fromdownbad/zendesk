class ActivateVoicePartnerEditionAccount < BackfillBase

  def initialize(options = {})
    super(options)
    @account_ids = options.fetch(:account_ids, nil)
  end

  def perform
    return unless @account_ids

    @account_ids.each do |account_id|
      account = Account.find(account_id)

      Rails.logger.info("Activating voice_partner_edition_account for account: #{account_id}")
      account.on_shard do
        account.voice_partner_edition_account.subscribe! unless dry_run
      end
    end
  end
end
