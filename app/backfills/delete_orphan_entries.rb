class DeleteOrphanEntries < BackfillBase
  def perform
    on_shards { delete_orphan_entries }
  end

  private

  def delete_orphan_entries
    soft_deleted_forums.each do |forum|
      if dry_run
        puts "Total entries for forum id=#{forum.id}: #{forum.entries.count}"
        next
      end

      Forum.with_deleted do
        entry_ids = forum.entries.pluck(:id)
        entry_ids.each_slice(1000) do |ids|
          Entry.soft_delete_all!(ids)
        end
      end
    end
  end

  def soft_deleted_forums
    @soft_deleted_forums ||= Forum.with_deleted do
      Forum.where("deleted_at IS NOT NULL").to_a
    end
  end
end
