class BackfillRemoveOldApiTokens < BackfillBase
  def perform
    Rails.logger.info("RemoveOldApiTokens: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards { backfill_shard }

    Rails.logger.info("RemoveOldApiTokens: Finished dry_run=#{dry_run}, pod=#{pod}")
  end

  private

  def backfill_shard
    Rails.logger.info("RemoveOldApiTokens: backfill shard #{ActiveRecord::Base.current_shard_id}")

    while (invalid_token_ids = ApiToken.where("created_at < ?", Time.parse('2016-11-01')).limit(1000).pluck(:id)) && invalid_token_ids.count > 0
      ApiToken.where(id: invalid_token_ids).delete_all unless dry_run
      Rails.logger.info("RemoveOldApiTokens: delete #{invalid_token_ids.count} tokens #{invalid_token_ids}")
    end
  end
end
