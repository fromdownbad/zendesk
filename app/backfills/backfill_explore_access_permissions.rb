class BackfillExploreAccessPermissions < BackfillBase
  # This backfill adds the default explore permission sets to existing default
  # permission sets

  def perform
    Rails.logger.info("BackfillExploreAccessPermissions: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards do
      backfill_shard
    end
  end

  def backfill_shard
    Rails.logger.info("BackfillExploreAccessPermissions: backfill shard")
    add_explore_permissions_to_permission_sets
  end

  private

  def add_explore_permissions_to_permission_sets
    TranslationLocale.all.each do |locale|
      roles = role_info(locale)
      roles.each do |role|
        next if role[:name].blank?
        permission_set = PermissionSet.where(name: role[:name], description: role[:description])
        next if permission_set.blank?
        permission_set.each do |ps|
          permission = ps.permissions.where(name: :explore_access, value: role[:explore_access])
          next if permission.present?
          unless dry_run
            begin
              ps.permissions.new(name: :explore_access, value: role[:explore_access]).save!
            rescue StandardError => e
              Rails.logger.info "Did not backfill permission_set: #{ps.id} due to error: #{e}"
              next if e
            end
            statsd_client.increment("permissions.explore_access", tags: ["#{role[:name]}:#{role[:explore_access]}"])
          end
          Rails.logger.info "Added explore_access permission to pemission_set_id: #{ps.id} account: #{ps.account_id}"
        end
      end
    end
  end

  def role_info(locale)
    [
      {
        name:           I18n.t('txt.default.roles.staff.name', locale: locale),
        description:    I18n.t('txt.default.roles.staff.description', locale: locale),
        explore_access: 'edit'
      },
      {
        name:           I18n.t('txt.default.roles.team_leader.name', locale: locale),
        description:    I18n.t('txt.default.roles.team_leader.description', locale: locale),
        explore_access: 'full'
      },
      {
        name:           I18n.t('txt.default.roles.advisor.name', locale: locale),
        description:    I18n.t('txt.default.roles.advisor.description', locale: locale),
        explore_access: 'none'
      },
      {
        name:           I18n.t('txt.default.roles.contributor.name', locale: locale),
        description:    I18n.t('txt.default.roles.contributor.description', locale: locale),
        explore_access: 'none'
      },
      {
        name:           I18n.t('txt.default.roles.chat_agent.name', locale: locale),
        description:    I18n.t('txt.default.roles.chat_agent.description', locale: locale),
        explore_access: 'none'
      }
    ]
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['BackfillExploreAccessPermissions'])
  end
end
