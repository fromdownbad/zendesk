class BackfillRemoveVoiceSeatsOffInactiveUsers < BackfillBase
  def perform
    Rails.logger.info("BackfillRemoveVoiceSeatsOffInactiveUsers: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards do
      backfill_shard
    end
  end

  private

  def backfill_shard
    Rails.logger.info("BackfillRemoveVoiceSeatsOffInactiveUsers: backfill shard")

    User.joins(:user_seats)
      .where('users.is_active = ?', false)
      .where('user_seats.seat_type = ?', 'voice')
      .find_each do |inactive_user|
        remove_voice_seat(inactive_user) if inactive_user.account.present?
    end
  end

  def remove_voice_seat(inactive_user)
    Rails.logger.info("BackfillRemoveVoiceSeatsOffInactiveUsers: remove voice_seat, user: #{inactive_user.id}, account: #{inactive_user.account.id}")
    return if dry_run

    if inactive_user.account.has_voice_delete_number_on_downgrade?
      inactive_user.user_seats.voice.destroy_all
    else
      inactive_user.user_seats.voice.delete_all
    end
  end
end
