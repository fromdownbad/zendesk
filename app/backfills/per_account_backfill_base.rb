class PerAccountBackfillBase < BackfillBase

  OCCASIONAL_LOG_PERIODICITY = 10.seconds

  # TODO: Override this in subclass
  def backfill_account(account)
    fail 'Override the backfill_account method in subclass.  Remember to respect the @dry_run flag!'
  end

  # TODO: Optionally override this in subclass to iterate over a subset of the accounts.  Takes in an
  # ActiveRecord::Relation, returns an ActiveRecord::Relation
  def constrain_accounts(arel)
    arel
  end

  # TODO: Optionally override in subclass to give a more detailed summary of the backfill job.
  def summary
    "Successfully completed backfill of #{@backfilled_accounts.size} accounts in #{shards.length} shards"
  end

  def initialize(options = {})
    super
    @backfilled_accounts = {}
    @start = Time.now
  end

  def perform
    log("*** #{@dry_run ? 'DRY' : 'LIVE'} RUN ***")
    unless @dry_run # a few seconds for the user to say "oh crap, I wanted a dry run, cancel, cancel!"
      start_delay = 10
      (0...start_delay).each do |i|
        log "Starting in #{start_delay - i} seconds"
        sleep 1
      end
    end

    log_if_verbose('First pass- backfilling all accounts')
    while perform_one_pass > 0 do
      log_if_verbose('Followup pass- backfilling moved accounts')
    end

    log summary
  end

  def perform_one_pass
    num_accounts_processed = 0
    shard_index = 0
    on_shards do |shard|
      accts_arel = constrain_accounts(Account.shard_local)
      accts_in_shard = accts_arel.count

      acct_index = 0
      accts_arel.find_each do |account|
        if !@backfilled_accounts[account.id] && account.created_at < @start
          log_occasionally("Backfilling account number #{acct_index + 1} of #{accts_in_shard} on shard #{shard} " \
                           "(shard  number #{shard_index + 1} of #{shards.length})")
          log_if_verbose("Backfilling account #{account.subdomain}")
          backfill_account(account)
          @backfilled_accounts[account.id] = true
          acct_index += 1
          num_accounts_processed += 1
        end
      end
      shard_index += 1
    end

    num_accounts_processed
  end

  def log(string)
    puts "#{string}"
  end

  def log_if_verbose(string)
    log(string) if @verbose
  end

  def log_occasionally(string)
    now = Time.now
    @next_occasional_log_time ||= now
    if @next_occasional_log_time <= now
      log_if_verbose(string)
      @next_occasional_log_time = now + OCCASIONAL_LOG_PERIODICITY
    end
  end
end
