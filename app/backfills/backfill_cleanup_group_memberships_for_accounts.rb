class BackfillCleanupGroupMembershipsForAccounts < BackfillBase
  def initialize(**args)
    @account_ids = args.fetch(:account_ids, {})
    super args
  end

  def perform
    @account_ids.each do |account_id|
      backfill_cleanup_group_memberships_for_account(account_id)
    end
  end

  private

  def backfill_cleanup_group_memberships_for_account(account_id)
    log_info("Starting run for cleaning up group membership in account #{account_id}, dry_run=#{dry_run}")
    account = Account.find_by(id: account_id)
    if account && account.pod_id == pod
      log_info("BackfillCleanupGroupMembershipsForAccounts: Account: #{account_id} found.")
      fix_group_memberships_for_account(account)
    else
      log_info("BackfillCleanupGroupMembershipsForAccounts: Account: #{account_id} not found in pod #{pod}.")
    end
    log_info("Finishing run for cleaning up group membership in account  #{account_id} , dry_run=#{dry_run}")
  end

  def fix_group_memberships_for_account(account)
    account.on_shard do
      joinblock = <<-JOINBLOCK
        left outer join `users` on `memberships`.account_id = `users`.account_id and `memberships`.user_id = `users`.id 
        left outer join `groups` on `memberships`.account_id = `groups`.account_id and `memberships`.group_id = `groups`.id
      JOINBLOCK

      whereblock = <<-WHEREBLOCK
      (`users`.account_id is null or `users`.id is null or `groups`.account_id is null or `groups`.id is null) 
      or (`users`.account_id = `memberships`.account_id and `groups`.account_id = `memberships`.account_id and 
          (`groups`.is_active = 0 or `users`.is_active = 0 or `users`.roles = 0))
      WHEREBLOCK

      account.memberships.joins(joinblock).where(whereblock).find_each do |um|
        log_info("BackfillCleanupGroupMembershipsForAccounts: dry_run:#{dry_run}, Account: #{account.id}, user_id:#{um.user_id}, group_id:#{um.group_id}")
        um.delete unless dry_run
      end
    end
  end
end
