class WriteSideConversationSlackSetting < BackfillBase
  def initialize(options)
    super
    @account_id = options.fetch(:account_id)
  end

  def perform
    Rails.logger.info "Running WriteSideConversationSlackSetting dry_run: #{dry_run} account: #{account.subdomain} STARTING"
    account.on_shard { write_setting }
  end

  private

  def account
    @account ||= Account.find(@account_id)
  end

  def write_setting
    if account.settings.any? { |s| s.name == 'side_conversations_slack' }
      Rails.logger.info "Ignoring since account already has the side_conversations_slack setting set (#{account.settings.side_conversations_slack})"
      return
    end

    has_arturo = Arturo.feature_enabled_for?(:collaboration_slack, account)
    has_setting = account.settings.ticket_threads
    
    if has_arturo && has_setting
      Rails.logger.info("Setting side_conversations_slack to true")

      if !dry_run
        account.settings.side_conversations_slack = true
        account.save!
        Rails.logger.info("- Success!")
      else
        Rails.logger.info("- Dry-run, no change made")
      end
    else
      Rails.logger.info("Doing nothing as criteria isn't met - arturo: #{has_arturo} setting: #{has_setting}")
    end

    Rails.logger.info "WriteSideConversationSlackSetting account: #{account.subdomain} DONE!"
  end
end
