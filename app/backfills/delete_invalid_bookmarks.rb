class DeleteInvalidBookmarks < BackfillBase
  def perform
    Rails.logger.info("DeleteInvalidBookmarks: Starting dry_run=#{dry_run}, pod=#{pod}")

    on_shards { backfill_shard }

    Rails.logger.info("DeleteInvalidBookmarks: Finished dry_run=#{dry_run}, pod=#{pod}")
  end

  private

  def backfill_shard
    Rails.logger.info("DeleteInvalidBookmarks: backfill shard #{ActiveRecord::Base.current_shard_id}")

    invalid_bookmarks = Ticket::Bookmark.select {|bookmark| !bookmark.ticket }.map(&:id)
    Rails.logger.info("DeleteInvalidBookmarks: backfill bookmarks #{invalid_bookmarks.inspect}")
    Ticket::Bookmark.without_arsi.delete_all(id: invalid_bookmarks) unless dry_run
  end
end
