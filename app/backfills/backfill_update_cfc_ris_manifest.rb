class BackfillUpdateCfcRisManifest < BackfillBase
  MANIFEST_URL = 'https://connector.zendesk.ext.smooch.io/zendesk/social-messaging/manifest.json'.freeze

  def perform
    Rails.logger.info("BackfillUpdateCfcRisManifest: Starting dry_run = #{dry_run}, pod = #{pod}")
    on_shards { backfill_shard }
    Rails.logger.info("BackfillUpdateCfcRisManifest: Finished dry_run = #{dry_run}, pod = #{pod}")
  end

  def backfill_shard
    Rails.logger.info("BackfillUpdateCfcRisManifest: Starting backfill for shard #{ActiveRecord::Base.current_shard_id}")

    registered_integration_services = []

    # Find RISs with old CFC manifest on shard
    ActiveRecord::Base.on_slave do
      registered_integration_services = Channels::AnyChannel::RegisteredIntegrationService.where(
        push_client_id: 'smooch',
        external_id: 'whatsapp'
      )
    end

    return if registered_integration_services.count < 1

    # Update the RIS manifests
    registered_integration_services.each do |ris|
      manifest, = Channels::AnyChannelApi::Client.get_manifest(MANIFEST_URL)
      extended_manifest = ris.class.extend_manifest_urls(MANIFEST_URL, manifest)
      new_attributes = ris.class.manifest_attributes(extended_manifest, MANIFEST_URL, ris.account_id)
      ris.update_attributes!(new_attributes) unless dry_run
      Rails.logger.info("Updated manifest for CFC RIS in account #{ris.account_id}")
    rescue StandardError => e
      Rails.logger.info("Could not update manifest for CFC RIS in account #{ris.account_id}: #{e.message}")
    end

    Rails.logger.info("BackfillUpdateCfcRisManifest: Finished backfill for shard #{ActiveRecord::Base.current_shard_id}")
  end
end
