module BackfillAccountProductsQuery
  extend ActiveSupport::Concern

  included do
    attr_reader :account_ids,
      :created_after_date,
      :is_deleted,
      :start_account_id,
      :include_sandbox,
      :include_inactive,
      :include_inbox
  end

  def initialize_account_query_options(options = {})
    @account_ids = options.fetch(:account_ids, nil)
    @start_account_id = options.fetch(:start_account_id, nil)
    @is_deleted = options.fetch(:is_deleted, nil)
    @created_after_date = options.fetch(:created_after_date, nil)
    @include_sandbox = options.fetch(:include_sandbox, false)
    @include_inactive = options.fetch(:include_inactive, false)
    @include_inbox = options.fetch(:include_inbox, false)

    raise ArgumentError, "Both start_account_id and created_after_date provided. Specify one." if start_account_id.present? && created_after_date.present?
  end

  def account_query
    query = Account.unscoped.joins(:subscription).preload(:subscription).preload(:zuora_subscription)
    return query.where(id: account_ids) unless account_ids.nil?

    query = query.where(shard_id: shards)
    query = query.where(is_active: true) unless include_inactive
    query = query.where('`accounts`.`deleted_at` IS NOT NULL') if is_deleted.present?
    query = query.where(deleted_at: nil) if !is_deleted.nil? && !is_deleted
    query = query.where(sandbox_master_id: nil) unless include_sandbox
    query = query.where('`subscriptions`.`plan_type` != ?', SubscriptionPlanType.Inbox) unless include_inbox

    if created_after_date.present? && start_account_id.nil?
      @start_account_id = Account.unscoped
        .where('`accounts`.`created_at` >= ?', Time.parse(created_after_date))
        .limit(1).pluck(:id).first
    end

    query = query.where('`accounts`.`id` >= ?', start_account_id) if start_account_id
    query
  end

  def account_query_for_exporter
    account_query.where('subscriptions.updated_at < ?', 5.minutes.ago)
  end
end
