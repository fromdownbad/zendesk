class DeleteDataDeletionJobFailedAudits < BackfillBase
  def perform
    Rails.logger.info "DeleteDataDeletionJobFailedAudits: Starting dry_run=#{dry_run}"

    DataDeletionAuditJob.where(status: "failed").find_in_batches do |batch|
      ids_in_batch = batch.map(&:id)
      Rails.logger.info "Deleting datadeletionauditjob with id's #{ids_in_batch}"
      DataDeletionAuditJob.delete(ids_in_batch) unless @dry_run
      sleep(10) if Rails.env.production?
    end
  end
end
