class UpdateExpiredTrialsToPatagonia < BackfillBase
  def perform
    Rails.logger.info("UpdateExpiredTrialsToPatagonia: Starting dry_run=#{dry_run}")
    Account.joins(:subscription).where(is_active: 1, is_serviceable: 0).where('subscriptions.pricing_model_revision < 7 AND subscriptions.is_trial = 0 AND subscriptions.payment_method_type = 0').find_each do |account|
      Rails.logger.info("UpdateExpiredTrialsToPatagonia: updating expired trial for account #{account.id} - #{account.subdomain}")

      # Can't query against shard_id IS NOT NULL because pod_id is not immediately derived against that, but rather is a bit more complex
      # see gem zendesk_core/lib/zendesk/models/accounts/pod_support.rb for more details
      next if dry_run || account.pod_id.nil?
      begin
        account.subscription.update_column(:pricing_model_revision, 7)
      rescue => e
        Rails.logger.error("UpdateExpiredTrialsToPatagonia: Error for account #{account.id} - #{account.subdomain}: #{e}")
      end
    end
  end
end
