class DisableGooddataProjects < BackfillBase

  # What:
  # Disable Gooddata(Insights) projects.
  #
  # Note:
  # The gooddata project IDs is a mandatory attribute for this backfill job. Please check and ensure the project
  # IDs are correct (and they are the ones you wish to disable) before running the job
  #
  # Why:
  # There are a large number of Gooddata projects that are inactive for 6 months or longer. GoodData still
  # charges Zendesk for these un-used projects. Disabling the projects would save some money.
  # The projects can be re-enable again by the customers.

  attr_reader :project_ids, :start_time

  def initialize(options = {})
    @project_ids = options.fetch(:project_ids)
    @start_time = options.fetch(:start_time)
    @gooddata_client = Zendesk::Gooddata::Client.v2
    super
  end

  def perform
    local_shard_ids.each do |shard_id|
      ActiveRecord::Base.on_shard(shard_id) do
        log("Disabling projects on shard_id:#{shard_id}")
        disable_gooddata_projects
      end
    end
  end

  def gooddata_client
    @gooddata_client ||  Zendesk::Gooddata::Client.v2
  end

  private

  def local_shard_ids
    Zendesk::Configuration::PodConfig.local_shards
  end

  def disable_gooddata_projects
    project_ids.each do |project_id|
      begin
        integration = GooddataIntegration.where(project_id: project_id).first
        if integration
          disable_gooddata_integration(integration, project_id)
        end
      rescue => e
        log("Failed to disabled gooddata project for project_id #{project_id} with exception:\n #{e.inspect}")
      end
    end
  end

  def disable_gooddata_integration(integration, project_id)
    unless dry_run
      deactivate_gooddata_project(project_id)
      integration.disabled!
    end
    log("Disabled gooddata integration for project_id:#{project_id} for account_id: #{integration.account.id}")
  end


  def deactivate_gooddata_project(project_id)
    template_version = Zendesk::Gooddata::IntegrationProvisioning.integration_template_version(project_id)

    gooddata_client.project(:id => project_id)
      .integration(:connector => Zendesk::Gooddata::IntegrationProvisioning::CONNECTOR)
      .update(
        :active           => false,
        :template_name    => Zendesk::Gooddata::IntegrationProvisioning::TEMPLATE_NAME,
        :template_version => template_version
      )
  end

  def log(message)
    Rails.logger.info(
      "[#{self.class} | Pod: #{pod} | Dry Run: #{dry_run}]: #{message}"
    )
  end
end
