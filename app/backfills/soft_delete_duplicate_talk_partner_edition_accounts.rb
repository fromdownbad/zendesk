class SoftDeleteDuplicateTalkPartnerEditionAccounts < BackfillBase
  def perform
    on_shards do
      backfill_accounts(accounts_with_duplicate_rows)
    end
  end

  def backfill_accounts(account_ids)
    Voice::PartnerEditionAccount.
      where(account_id: account_ids).
      find_each_for_shard(batch_size: 100) { |vpea| soft_delete_duplicate_voice_partner_edition_account(vpea) }
  end

  def soft_delete_duplicate_voice_partner_edition_account(vpea)
    account = vpea.account
    latest_active_vpea = Voice::PartnerEditionAccount.
      where(account_id: account.id).
      order('created_at desc').last

    return if vpea.id == latest_active_vpea.id

    puts "Soft-delete duplicate account_id: #{account.id}, partner_edition_account_id:#{vpea.id}"
    vpea.soft_delete! unless dry_run
  rescue NoMethodError => e
    puts "An error occurred while trying to soft-delete duplicate account #{e.message}"
  end

  def accounts_with_duplicate_rows
    Voice::PartnerEditionAccount.
      unscoped.
      select(:account_id).
      where(deleted_at: nil).
      group(:account_id).
      having('count(id) > 1').
      pluck(:account_id)
  end
end
