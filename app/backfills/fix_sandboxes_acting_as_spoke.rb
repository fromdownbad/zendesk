class FixSandboxesActingAsSpoke < BackfillBase
  def perform
    Rails.logger.info("FixSandboxesActingAsSpoke: Starting dry_run=#{dry_run}")

    if dry_run
      scope.each do |subscription|
        Rails.logger.info("This backfill would change hub_account_id from #{subscription.hub_account_id} to nil for account #{subscription.account.subdomain}")
      end
    else
      scope.without_arsi.update_all(hub_account_id: nil)
    end
  end

  def scope
    Subscription
      .joins(:account)
      .where(accounts: { subdomain: %w[dropbox1304824455 hrandpayrolldirect1420031608 z3ndinosaur1465413090] })
  end
end
