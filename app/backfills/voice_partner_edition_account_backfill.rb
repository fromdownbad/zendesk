class VoicePartnerEditionAccountBackfill < BackfillBase
  def initialize(options = {})
    @path = options[:path]
  end

  def perform
    File.open(@path, 'r') do |f|
      f.each_line do |account_id|
        account = Account.find(account_id)
        ActiveRecord::Base.on_shard(account.shard_id) do
          partner_edition_account = Voice::PartnerEditionAccount.find_or_create_by_account(account)
          puts "Backfilling voice partner edition account for account_id:#{account.id}"
          partner_edition_account.update_attributes(active: true, plan_type: Voice::PartnerEditionAccount::PLAN_TYPE[:legacy])
        end
      end
    end
  end
end
