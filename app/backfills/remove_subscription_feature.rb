require 'subscription'

class RemoveSubscriptionFeature < BackfillBase
  attr_reader :name

  def initialize(options = {})
    super

    @name = options[:name] || raise('Must init with name')
  end

  def perform
    Rails.logger.info("RemoveSubscriptionFeature: Starting dry_run=#{dry_run}, pods=#{pods.join(', ')}, name=#{name}")
    return if dry_run
    on_shards { backfill_shard }
    nil
  end

  private

  def backfill_shard
    SubscriptionFeature.where(name: name).destroy_all
  end
end
