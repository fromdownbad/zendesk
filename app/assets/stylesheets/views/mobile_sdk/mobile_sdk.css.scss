/*
 ADMIN STYLESHEETS:
 *= require ../admin/base
 *= require_tree ../admin/vendor/admin_assets

 MOBILE SDK STYLESHEETS:
 *= require vendor/select2
 */

@import "../admin/misc/_colors.css.scss";
@import "../admin/misc/_fonts.css.scss";

@mixin border-radius($radius) {
  -moz-border-radius: $radius;
  -webkit-border-radius: $radius;
  border-radius: $radius;
}

body{
  font-family: $system
}

.sdk-talk-warning,
.rma-warning {
  @include border-radius(4px);

  padding: 10px;
  margin-top: 10px;
  border: 1px solid #FFA100;
  display: block;
  min-height: 86px;
  max-height: 136px;

  .col-lg-1 {
    width: 25px;
    height: 18x;
    display: inline-block;
    float: left;
  }

  .col-lg-11 {
    display: inline-block;
    width: 95%;
    max-width: 720px;
    min-width: 120px;
  }

  .sdk-talk-warning-header,
  .rma-warning-header {
    color: #FFA100;
    font-weight: bold;
  }

  .sdk-talk-warning-icon,
  .rm-warning-icon {
    background-image: url(/classic/images/validation/warning.svg);
    display: inline-block;
    height: 16px;
    width: 16px;
    float: left;
    margin-top: 3px;
  }

  .sdk-talk-warning-body,
  .rm-warning-body {
    color: #999;
    display: inline-block;
  }
}

.flash {
  display: block !important;
  margin: 30px 30px 0 30px;
  max-width: 889px;
  min-width: 569px;

  .error {
    font-size: 14px;
    font-weight: bold;
    border: 1px solid #eb6651;
    border-radius: 4px;
    background: hsla(8,79%,62%,.1);
    padding: 20px;
    overflow: hidden;

    &::before {
      content: url(/classic/images/validation/error.svg);
      width: 15px;
      display: inline-block;
      position: relative;
      top: 2px;
      left: -5px;
    }

    ul {
      font-size: 12px;
      font-weight: normal;
      margin-left: 25px;
      margin-top: 5px;
      list-style: disc;
      list-style-position: inside;
    }
  }
}

.admin-container h2 {
  font-size: 22px;
  margin: 20px 0;
}

#blankApp.closed {
  display: none !important;
}

.mobile-app-tos {
  text-align: left;
  margin: 25px 0;
  font-size: 12px;
  max-width: 450px;

  label {
    display: inline;
  }
  input {
    display: inline;
    margin-right: 5px;
  }
}

.zero-state {
  padding-top: 50px;
  text-align: center;

  img {
    height: 86px;
    width: 51px;
  }

  span {
    display: block;
    font-size: 13px;
    margin: 30px 0;
  }
}

.delete-form {
  display: none;
}

bdo[dir="rtl"] {
  form .field-wrapper {
    input,
    select,
    .switch,
    .create-helpcenter {
      float: left;
    }

    textarea[readonly="readonly"] {
      direction: ltr;
    }
  }

  .jwt-url-wrapper,
  .sdk-talk-button-id-wrapper,
  .system-message-custom-text-wrapper,
  .pn-webhook-url-wrapper {
    float: left;
    clear: left;
  }

  .capsule .button-section > div,
  .capsule-header .dropdown {
    float: left;
  }

  .capsule-header .dropdown .dropdown-menu {
    right: auto;
    left: 0;

    li {
      text-align: right;
    }
  }

  .rma-warning {
    .col-lg-1,
    .rm-warning-icon {
      float: right;
    }
  }

  .tabbed_container .tab_links a,
  .capsule-header .capsule-title {
    float: right;
  }

  .mobile-app-tos {
    text-align: right;
  }

  .validation-message::before {
    right: -27px;
  }

  .capsule .button-section button[type=submit] {
    margin-right: 5;
    margin-left: 0;
  }
}

form {
  .field-wrapper {
    margin: 25px 0 0 0 !important;
    overflow: hidden;

    label {
      display: block;
    }

    .info {
      color: $gray;
      display: inline-block;
      width: 450px;
    }

    input {
      float: right;
      height: 34px !important;
      margin-bottom: 0;
      width: 268px !important;

      &[readonly] {
        background-color: #F7F7F7;
      }

      &.under {
        display: block;
        float: none;
        margin-top: 10px;
      }

      &.small {
        width: 150px !important;
      }

      &.large {
        width: 357px !important;
      }

      &.switch-checkbox {
        display: none !important;
      }
    }

    .switch-content {
      text-transform: uppercase;
    }

    select {
      float: right;
      height: 34px !important;
      line-height: 34px !important;
      margin-bottom: 0;
      width: 150px;
    }

    .select2-container {
      float: right;

      a {
        height: 32px;
        line-height: 32px;
        width: 150px;
      }
    }

    textarea {
      display: block;
      font-family: monospace;
      margin-top: 10px;
      padding: 10px;
      width: 100%;

      &[readonly] {
        background-color: #F8F8F8;
      }
    }

    .switch {
      float: right;
    }

    .create-helpcenter {
      @include border-radius(4px);

      border: 1px solid #DDD;
      color: #555;
      float: right;
      font-weight: bold;
      height: 40px;
      line-height: 40px;
      padding: 0 20px;

      &:hover {
        text-decoration: none;
      }
    }
  }
}

.jwt-url-wrapper,
.sdk-talk-button-id-wrapper,
.system-message-custom-text-wrapper,
.pn-webhook-url-wrapper {
  float: right;
  width: 268px;
  clear: right;
}

.validation {
  .validation-message {
    display: none;
    margin: 40px 0 0;

    .length-error,
    .format-error {
      display: none;
    }
  }
}

.error {
  .validation-message {
    display: block !important;
    color: #bf3026;
  }

  &.length-error .length-error {
    display: block !important;
  }

  &.format-error .format-error {
    display: block !important;
  }

  .jwt-url-wrapper,
  .sdk-talk-button-id-wrapper,
  .system-message-custom-text-wrapper {
    min-height: 75px;
  }

  .system-message-custom-text-wrapper {
    .validation-message::before {
      top: 0;
    }
  }
}

.system-message-custom-text-wrapper .validation-message {
  margin-top: 5px;
}

.capsule .capsule-body form {
  .button-section {
    button {
      height: 40px;
      line-height: 40px;
      padding: 0 20px;

      &[type=submit] {
        background-color: #333;
      }

      &[disabled] {
        background-color: #DDD;
        color: #FFF;
      }
    }
  }
}

.capsule-body {
  .textarea-code {
    margin-top: 0;
  }

  .label-lang {
    text-align: right;
    color: #999;
    margin-top: 10px;
    padding-right: 4px;
  }
}

.select2-result-label {
  height: 34px;
  line-height: 34px;

  img {
    margin-right: 3px;
    width: 34px;
  }
}

.sdk-talk-phone-number-wrapper {
  display: none;

  &.enabled {
    display: block;
  }
}

.auth-wrapper {
  display: none;

  &.enabled {
    display: block;
  }

  .generate-shared-secret {
    cursor: pointer;
  }
}

.auth-warning {
  border-bottom: 1px solid #F7F7F7;
  margin: 0 auto;
  padding: 10px 0 20px;
  width: 460px;

  span {
    display: block;
    text-align: center;
  }

  .icon {
    background: #FFF url(/classic/images/warning_new.png) no-repeat center;
    height: 16px;
  }

  .title {
    font-weight: bold;
    margin-top: 15px;
  }

  &.hide {
    display: none;
  }
}

.push-webhook-wrapper, .push-urban-airship-wrapper, .system-message-custom-wrapper {
  display: none;

  &.enabled {
    display: block;
  }
}

/* Tabs */
.tabbed_container {
  margin: 0;

  .tabs_content {
    border-top: 1px solid #EFEFEF;
    padding: 5px 0;

    .tabs_canvas > div.default_tab {
      display: block;
    }
    .tabs_canvas > div{
      display: none;
    }
  }

  .tab_links {
    a {
      color: #343434;
      float: left;
      font-size: 13px;
      padding: 0 0 5px 0;
      margin: 0 30px 0 0;
      border-bottom: 3px solid transparent;
      text-align: center;

      &:hover {
        border-bottom: 3px solid #D4D4D4;
        text-decoration: none;
      }

      &::after {
        display: block;
        content: attr(data-title-value);
        font-weight: bold;
        visibility: hidden;
        overflow: hidden;
        height: 1px;
        margin-right: -2px;
      }

      &.current {
        font-weight: bold;
        border-bottom: 3px solid #649800;
      }
    }
  }
}
