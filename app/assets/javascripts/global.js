/* Usage

ADD A NEW MODULE FOR A PAGE OR PARTIAL
$z.defModule('home/index', { // THE NAME HAS TO MATCH THE VIEW TITLE UP TO THE FIRST PERIOD, SO app/views/home/index.html.erb invokes 'home/index' regardless of what file it's defined in
  // if an initialize() method exists, js_init will call it
  initialize: function (any parameters passed to js_init from the view){
    // do stuff like add event handlers
  },
  foobar: function (){
    // can be called from e.g. initialize() as this.foobar() or $z('home/index').foobar() if calling from other modules
  }
});

ADD A NEW MODULE THAT SERVES AS A NAMESPACE, BUT CANNOT ACTUALLY BE INITIALIZED BY js_init
$z.defModule('home', {
  do_something_awesome: function (){} // could be called from e.g. 'home/index' as $z('home').do_something_awesome();
});

THIS WOULD BE INSTANTIATED FROM e.g. app/views/home/index.html.erb with:
  <%= js_init 'hello', 'world' -%> # ultimately calls $z('home/index').initialize('hello', 'world');

TO PASS ARBITRARY DATA FROM A VIEW YOU CAN DO
js_data

*/

var $j = jQuery.noConflict();

var $z = (function (){
  var modules = {}; // Example of a private object, per http://javascript.crockford.com/private.html

  var $z = function (key_path){
    if (typeof(modules[key_path]) === 'object'){
      var method_accessor_constructor = function (){};
      method_accessor_constructor.prototype = modules[key_path];
      return(new method_accessor_constructor()); // pass a copy of of the module, so the original can't be modified outside of its scope by other modules
    }
  };

  $z.defModule = function (key_path, module){
    if (typeof(modules[key_path]) !== 'object'){ // will not overwrite previous definition - each class should only be defined once
      modules[key_path] = module;
    }
  };

  $z.initializeModules = function (instances){
    $j(document).ready(function (event) {
      $j(instances).each(function (index, instance) { // an instance is e.g. ["shared/_macro_list", [arg1, arg2, arg3]]
        if (typeof(modules[instance[0]]) === 'object' && typeof(modules[instance[0]].initialize) === 'function'){ // e.g. if modules["shared/_macro_list"] is {initialize: function () {...}, ...}
          var instantiator = function () {
            this.initialize.apply(this, instance[1]);
            delete this['initialize']; // this only deletes the initialize function on this instance, which is a _copy_ of the module definition
          };
          instantiator.prototype = modules[instance[0]];
          var new_instance = new instantiator(); // asking for a new instantiator gives us an object that "inherits" all the functions of the module
        }
      });
    });
  };

  return($z);
})();
var Zendesk = $z;
