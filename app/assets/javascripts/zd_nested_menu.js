/*globals Zendesk*/
Zendesk.NS('UI', this.jQuery, function($) {
  /*
  Markup:

  <ul>
    <li><span>Top Level Label</span>
      <ul>
        <li>Top Level Option 1</li>
        <li>Top Level Option 2</li>
        <li>Top Level Option 3</li>
        <li>Top Level Option 4
          <ul>
            <li>Second Level Option 1</li>
            <li>Second Level Option 2</li>
            <li>Second Level Option 3</li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  */

  this.NestedMenu = function(selector) {
    this._menu = $(selector);
    var self = this;

    this.initialize = function() {
      $(document).bind('click', function(evt) {
        if($(evt.target).parents().index(self._menu) < 0) {
          self.collapse();
        }
      });

      var top = self._menu.find("li:eq(0)");
      top.click(function() {
        top.find("> ul").show();
        self._menu.css('z-index', '15');
      });

      self._menu.find('ul > li').each(function(i, li) {
        var nested_list = $(li);
        nested_list.mouseover(function() {
          Zendesk.UI.NestedMenu.close_submenu(nested_list);
          nested_list.parent().data('selection', $(li));
          nested_list.find("> ul").show();
        });

      });

      return self;
    };

    this.collapse = function() {

      self._menu.find("li").each(function(i, li) {
        $(li).find("ul").hide();
        self._menu.css('z-index', '');
      });

      return self;
    };
  };

  this.NestedMenu.close_submenu = function(nested_list) {
    var open_li = nested_list.parent().data('selection');
    if(open_li) {
      $(open_li).find("> ul").hide();
    }
  };
});
