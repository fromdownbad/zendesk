jQuery(document).ready(function($) {
  var CSAT = {
    Score: {
      unoffered: 0,
      offered: 1,
      poor:    4,
      good:    16
    },

    ScoreValue: {
      '0':  'unoffered',
      '1':  'offered',
      '4':  'poor',
      '5':  'poor',
      '16': 'good',
      '17': 'good'
    },

    ALERT_DURATION: 8000,
    ALERT_DELAY: 750,

    rating: null,
    alertTimer: null,
    $alert: $('.alert'),
    $ratingInputs: $('input[name="satisfaction_rating"]'),
    $ratingButtons: $('#satisfaction-rating-buttons button.btn.rating'),
    $reasonCode: $('select[name="ticket[satisfaction_reason_code]"]'),
    $comment: $('textarea[name="ticket[satisfaction_comment]"]'),
    $ratingScore: $('input[name="ticket[satisfaction_score]"]'),
    $form: $('form'),
    $formSubmit: $('button[type="submit"]'),

    init: function() {
      this.initRatingButtons();
      this.initComment();
      this.initForm();
      this.initCurrentRatingData();
    },

    updateRatingScore: function() {
      var ratingScore = this.Score[this.rating];
      var commentScore = (this.$comment.val() !== '') ? 1 : 0;

      if (ratingScore === 0 || ratingScore === 1) {
        this.$ratingScore.val(ratingScore);
      } else {
        this.$ratingScore.val(ratingScore + commentScore);

        this.enableCommentBox();
        this.enableSubmitButton();
      }
    },

    setRating: function(rating) {
      this.rating = rating;
      this.$ratingInputs.each(function(index, el) {
        if (el.value == rating) {
         $(el).prop('checked', true).change();
          return false;
        }
      });
    },

    toggleReasonCode: function() {
      if (this.rating == 'poor') {
        this.$reasonCode.parent().show(300);
      } else {
        this.$reasonCode.prop('selectedIndex', -1);
        this.$reasonCode.parent().hide(300);
      }
    },

    enableSubmitButton: function() {
      this.$formSubmit.attr('disabled', false);
    },

    enableCommentBox: function() {
      this.$comment.attr('disabled', false);
    },

    displayAlert: function(message, alertType) {
      alertType = alertType || 'info';
      this.timeoutAlert();
      this.$alert.find('.message').html(message);
      this.$alert.addClass(alertType);
      this.$alert.removeClass('hidden');
      this.timer = setTimeout(this.timeoutAlert.bind(this), this.ALERT_DURATION);
    },

    timeoutAlert: function() {
      if (this.timer !== null) {
        clearTimeout(this.timer);
        this.timer = null;
      }

      this.$alert.removeClass().addClass('alert hidden');
    },

    handleRatingButtonPress: function(buttonElement) {
      buttonElement.className === 'btn rating good' ? this.setRating('good') : this.setRating('poor');
    },

    initRatingButtons: function() {
      this.$ratingButtons.click(function(e) {
        this.handleRatingButtonPress(e.currentTarget);
      }.bind(this));

      this.$ratingButtons.keydown(function(e) {
        if (e.keyCode == 13 || e.keyCode == 32) {
          this.handleRatingButtonPress(e.currentTarget);
        }
      }.bind(this));

      this.$ratingInputs.change(function(e) {
        this.rating = e.currentTarget.value;
        this.toggleReasonCode();
        this.updateRatingScore();
      }.bind(this));
    },

    initComment: function() {
      this.$comment.on('focusout', this.updateRatingScore.bind(this));
    },

    initCurrentRatingData: function() {
      var rating    = this.ScoreValue[this.$ratingScore.val()],
          flashMsg = jQuery.trim(this.$alert.find('.message').html());

      this.setRating(rating);
      this.toggleReasonCode();
      this.updateRatingScore();

      if (flashMsg.length > 0) {
        setTimeout(function() {
          this.displayAlert(flashMsg);
        }.bind(this), this.ALERT_DELAY);
      }
    },

    initForm: function() {
      var _this = this;
      this.$form.on('submit', function(e) {
        e.preventDefault();
        _this.$formSubmit.prop('disabled', true);
        _this.timeoutAlert.call(_this);

        jQuery.ajax({
          url: _this.$form.attr('action'),
          type: _this.$form.attr('method').toUpperCase(),
          data: _this.$form.serialize(),
          dataType: 'json'
        }).then(function(request, textStatus, errorThrown) {
            _this.displayAlert(I18n.strings.success, 'info');
          }, function(data, textStatus, XMLHttpRequest) {
            _this.displayAlert(I18n.strings.error, 'warning');
          }
        ).always(function() {
            _this.$formSubmit.prop('disabled', false);
          }
        );
      });
    }
  };

  CSAT.init();
});
