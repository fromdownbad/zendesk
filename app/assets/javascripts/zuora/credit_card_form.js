function ZdZuora() {}

ZdZuora.lotusMessageCallback = function(event) {
  var checkbox = $('input#terms_agree');
  var button   = $('button#submit-credit-card');

  if (event.data === 'disablePurchaseButton') {
    checkbox.prop('disabled', true);
    button.prop('disabled', true);
  } else if (event.data === 'enablePurchaseButton') {
    checkbox.prop('disabled', false);

    if (checkbox.prop('checked')) {
      button.prop('disabled', false);
    }
  } else if (event.data === 'submitCreditCard') {
    button.click();
  }
};

ZdZuora.CreditCardForm = {
  reset: function(settings) {
    jQuery(settings.submitCreditCard).attr('disabled', 'disabled').removeClass('save');
    jQuery(settings.termsAgree).removeAttr('disabled', 'disabled').addClass('save');

    jQuery(settings.termsAgree).
      change(function(){
        if (jQuery(this).is(':checked')) {
          jQuery(settings.submitCreditCard).removeAttr('disabled', 'disabled').addClass('save');
        } else {
          jQuery(settings.submitCreditCard).attr('disabled', 'disabled').removeClass('save');
        }
      });
    ZXD.receiveMessage(ZdZuora.lotusMessageCallback, settings.accountUrl);
  },

  accountUrl: function(location) {
    return decodeURIComponent(location).match(/\[account_url\]=([^&]*)/)[1];
  },

  cancelLotus: function(settings) {
    ZXD.postMessage('cancelCreditCardUpdateZuora', settings.accountUrl, settings.target);
  },

  lock: function(settings) {
    var message = {'type': 'purchase', 'data': { 'vat': settings.vat }};

    if(settings.hasPlanSelection) {
      // Show processing overlay in Lotus
      ZXD.postMessage(JSON.stringify(message), settings.accountUrl, settings.target);
    }

    jQuery(settings.submitCreditCard).attr('disabled', 'disabled').removeClass('save');
    jQuery(settings.termsAgree).attr('disabled', 'disabled').removeClass('save');

    if (typeof settings.onlock === 'function')
      settings.onlock.call();
  },

  didLoad: function(settings) {
    ZXD.postMessage('didLoadZuoraHostedPaymentPage', settings.accountUrl, settings.target);
  }
};
