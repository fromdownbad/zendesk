/*global require, $, I18n */

var Validation = require('admin/validation'),
    utils = require('admin/utils');

var validationTimeout = 500; // milliseconds

function HostMappingField(element, options) {
  this.element = $(element);
  this.options = $.extend({}, {
    paddingRight: 0
  }, options);

  var self = this;

  this.element.on('input', function(event) {
    self.removeInvalidCharacters();
    self.validate();
  });

  $(window).load(function() {
    var paddingLeft = parseInt(self.element.css('paddingLeft'), 10) || 0;
    self.element.css('padding-right', (paddingLeft + self.options.paddingRight) + 'px');
  });
}

utils.mixin(HostMappingField.prototype, Validation);

/*
 *  prevent user from hitting an invalid character
 */
HostMappingField.prototype.removeInvalidCharacters = function() {
  var val = this.element.val();
  var newVal = val.replace(/[^a-z0-9.\-]/gi, '');

  if (val !== newVal) {
    this.element.val(newVal);
  }
};

HostMappingField.prototype.validate = function() {
  clearTimeout(this.timer);
  var self = this;

  this.timer = setTimeout(function() {
    self._validate();
  }, validationTimeout);
};

HostMappingField.prototype._validate = function() {
  var val = this.element.val(),
      self = this;

  if (val.length === 0 || val === this.element.attr('value')) {
    this.clear();
  } else {
    this.element.closest('.field-wrapper').addClass('loading');
    $.ajax({
      type: 'GET',
      url: '/api/v2/brands/check_host_mapping.json',
      data: {
        subdomain: this.options.subdomain.call(this),
        host_mapping: val
      },
      success: function(response) {
        var errorMessage;

        self.element.closest('.field-wrapper').removeClass('loading');
        if (response.is_valid) {
          self.success({
            message: I18n.t('txt.admin.multibrand.validations.host_mapping.valid_cname')
          });
        } else {
          if (response.reason === 'already_taken') {
            errorMessage = I18n.t('txt.admin.multibrand.validations.host_mapping.already_taken');
          } else {
            errorMessage = I18n.t('txt.admin.multibrand.validations.host_mapping.invalid_cname', {
              expectedCname: response.expected_cnames[0],
            });
          }
          self.error({
            message: errorMessage
          });
        }
      }
    });
  }
};

module.exports = HostMappingField;
