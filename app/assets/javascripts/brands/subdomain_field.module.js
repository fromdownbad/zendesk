/*global require, $, I18n */

var Validation = require('admin/validation'),
    utils = require('admin/utils');

var successOptions = {
      message: I18n.t('txt.admin.multibrand.validations.subdomain.success')
    },
    invalidOptions = {
      message: I18n.t('txt.admin.multibrand.validations.subdomain.invalid')
    },
    unavailableOptions = {
      message: I18n.t('txt.admin.multibrand.validations.subdomain.unavailable')
    };

var validationTimeout = 500; // milliseconds

function SubdomainField(element, options) {
  this.element = $(element);
  this.options = $.extend({}, {
    text: '',
    paddingRight: 0
  }, options);

  this.element.wrap('<div style="position:relative;"></div>');
  this.container = this.element.parent();
  this.suffix = $('<div>', {
    text    : this.options.text,
    "class" : "suffix",
    css     : {
      pointerEvents: 'none',
      position: 'absolute',
      left: this.element.css('paddingLeft')
    }
  });

  this.container.append(this.suffix);

  var paddingLeft = parseInt(this.element.css('paddingLeft'), 10) || 0;
  var shadowWidth = measureWidth(this.suffix.text(), this.suffix);
  this.paddingRight = (paddingLeft + shadowWidth + this.options.paddingRight) + 'px';
  this.element.css('float', 'left');

  var self = this;

  this.element.on('input', function(event) {
    self.removeInvalidCharacters(event);
    self.repositionSuffix();
    self.validate();
  });

  this.element.closest('form').on('reset', function(event) {
    setTimeout(function () {
      self.repositionSuffix();
      self.validate();
    });
  });

  this.suffix.click(function() {
    focusInputEnd(self.element);
  });

  $(window).load(function() {
    self.repositionSuffix();

    self.container.height(self.element.height());
    self._validate();
  });
}

utils.mixin(SubdomainField.prototype, Validation);

function focusInputEnd(input) {
  input.focus();
  var tmpStr = input.val();
  input.val('');
  input.val(tmpStr);
}

function measureWidth(str, refEl) {
  var width;
  var phantom = $('<span>', {
    text: str,
    css: {
      position: 'absolute',
      left: '-9999em',
      font: refEl.css('font')
    }
  });

  phantom.appendTo($('body'));
  width = phantom.width();
  phantom.remove();
  return width;
}

SubdomainField.prototype.removeInvalidCharacters = function(event) {
  var val = this.element.val();
  var newVal = val.toLowerCase().replace(/[^a-z0-9\-]/g, '');

  if (val !== newVal) {
    this.element.val(newVal);
  }
};

SubdomainField.prototype.repositionSuffix = function() {
  var val = this.element.val();

  var rawWidth = measureWidth(val, this.element),
      width    = rawWidth +
                 parseInt(this.element.css('borderLeftWidth'), 10) +
                 parseInt(this.element.css('paddingLeft'), 10) +
                 parseInt(this.element.css('marginLeft'), 10),
      adjusted = Math.min(width, this.element.width() + 10);

  if (val.length > 0) {
    this.suffix.show().css('left', adjusted + 'px');
    this.element.css('paddingRight', this.paddingRight);
  } else {
    this.suffix.hide();
    this.element.css('paddingRight', '');
  }
};

SubdomainField.prototype.validate = function() {
  clearTimeout(this.timer);
  var self = this;

  this.timer = setTimeout(function() {
    self._validate();
  }, validationTimeout);
};

SubdomainField.prototype._validate = function() {
  var val = this.element.val(),
      self = this;

  if (val.length === 0) {
    this.silentError();
  } else if (val === this.element.attr('value')) {
    this.clear();
  } else if (!/^[a-z0-9][a-z0-9\-]{1,}[a-z0-9]$/.test(val)) {
    this.error(invalidOptions);
  } else {
    this.container.closest('.field-wrapper').addClass('loading');
    $.ajax({
      type: 'GET',
      url: '/api/v2/accounts/available.json',
      data: {
        subdomain: val
      },
      success: function(response) {
        self.container.closest('.field-wrapper').removeClass('loading');
        response.success ? self.success(successOptions) : self.error(unavailableOptions);
      }
    });
  }
};

module.exports = SubdomainField;
