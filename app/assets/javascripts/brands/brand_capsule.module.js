var LotusClient = require('admin/lotus_client');

var BrandCapsule = function(element, options) {
  var capsule       = $(element).closest('.capsule'),
      isDefault     = capsule.hasClass('default'),
      hasAgentRoute = capsule.hasClass('agent'),
      isActive      = !capsule.hasClass('inactive'),
      menuOptions   = [];

  var makeDefault = {
        element: '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + I18n.t('txt.admin.multibrand.dropdown.make_default') + '</a></li>',
        onClick: function(event) {
          var form = capsule.find('form.make-default');

          if (form.data('show-modal') === true) {
            LotusClient.showConfirmationModal({
              title:        I18n.t('txt.multibrand.brand.make_default_modal.title'),
              message:      I18n.t('txt.multibrand.brand.make_default_modal.body'),
              confirmLabel: I18n.t('txt.multibrand.brand.make_default_modal.confirm_label'),
              cancelLabel:  I18n.t('txt.multibrand.brand.make_default_modal.cancel_label')
            })
            .then(function() {
              form.trigger('submit');
            });
          } else {
            form.trigger('submit');
          }
        }
      },
      deactivate = {
        element: '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + I18n.t('txt.admin.multibrand.dropdown.deactivate') + '</a></li>',
        onClick: function(event) {
          var brandName = capsule.find(".capsule-title.name").html();
          LotusClient.showConfirmationModal({
            title: I18n.t('txt.multibrand.brand.deactivate_modal.title'),
            message: I18n.t('txt.multibrand.brand.deactivate_modal.body', {brand_name: brandName}),
            confirmLabel: I18n.t('txt.multibrand.brand.deactivate_modal.confirm_label'),
            cancelLabel: I18n.t('txt.multibrand.brand.deactivate_modal.cancel_label')
          }).then(function() {
            capsule.find('form.toggle').trigger('submit');
          }, function () {/*noop*/});
        }
      }
      activate = {
        element: '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + I18n.t('txt.admin.multibrand.dropdown.activate') + '</a></li>',
        onClick: function(event) {
          capsule.find('form.toggle').trigger('submit');
        }
      },
      line = { element: '<li role="presentation" class="divider"></li>' },
      _delete = {
        element: '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + I18n.t('txt.admin.multibrand.dropdown.delete') + '</a></li>',
        onClick: function(event) {
          var brandName = capsule.find(".capsule-title.name").html();
          var defaultBrandName = $('.capsule.default').find(".capsule-title.name").html();
          var brandSubdomain = capsule.find(".subdomain-field").attr('value');
          LotusClient.showConfirmationModal({
            title: I18n.t('txt.multibrand.brand.destroy_modal.title'),
            message: I18n.t('txt.multibrand.brand.destroy_modal.body', {brand_name: brandName, brand_subdomain: brandSubdomain, default_brand_name: defaultBrandName}),
            confirmLabel: I18n.t('txt.multibrand.brand.destroy_modal.confirm_label'),
            cancelLabel: I18n.t('txt.multibrand.brand.destroy_modal.cancel_label')
          }).then(function() {
            capsule.find('form.destroy').trigger('submit');
          }, function () {/*noop*/});
        }
      };

  if(isActive) {
    if(!isDefault) {
      menuOptions.push(makeDefault);
      if(!hasAgentRoute) {
        menuOptions.push(deactivate, line, _delete);
      }
    }
  } else {
    var form = capsule.find('form.toggle');
    if (form.data('can-activate') === true) {
      menuOptions.push(activate);
    }
    menuOptions.push(line, _delete);
  }

  var capsuleOptions = {
    dropdown: {
      options: menuOptions,
      openText: I18n.t('txt.admin.multibrand.dropdown.edit'),
      closeText: I18n.t('txt.admin.multibrand.dropdown.close')
    }
  }
  if (options && options.onOpen) { capsuleOptions.onOpen = options.onOpen; }
  if (options && options.onClose) { capsuleOptions.onClose = options.onClose; }

  return $(element).capsule(capsuleOptions);
};

module.exports = BrandCapsule;
