/*global require, $, I18n */

var Validation = require('admin/validation'),
    utils = require('admin/utils'),
    ie9 = utils.ie9();

var maxLogoSize = 2 * 1048576; // 2MB

var successOptions = {
      message: I18n.t('txt.admin.multibrand.validations.logo.success')
    },
    wrongTypeOptions = {
      message: I18n.t('txt.admin.multibrand.validations.logo.wrong_type')
    },
    tooLargeOptions = {
      message: I18n.t('txt.admin.multibrand.validations.logo.too_large')
    };

function UploadField(element, options) {
  this.element = $(element);
  this.fieldElement = this.element.find('input');
  this.hiddenField = $('<input type="hidden" name="' + this.fieldElement.attr('name') + '"></input>');
  this.headerLogo = this.element.closest('.capsule').find('.capsule-header img.logo');

  this.buildMarkup(ie9);

  if (ie9) {
    this.fieldElement.on('change', this.ie9Preview.bind(this));
  } else {
    this.element.on('dragover',  this.addFill.bind(this));
    this.element.on('dragleave', this.removeFill.bind(this));
    this.element.on('drop',      this.drop.bind(this));

    this.fieldElement.on('change', this.imagePreview.bind(this));
  }

  this.element.find('.unset-logo').on('click', this.unsetLogo.bind(this));
  this.element.closest('form').on('reset', this.reset.bind(this));
}

utils.mixin(UploadField.prototype, Validation);

UploadField.prototype.buildMarkup = function(ie9) {
  var upload = this.element.find('.upload-child'),
      preview = this.element.find('.preview-child');

  if (ie9) {
    upload.append('<span>' + I18n.t('txt.admin.multibrand.brand.click_logo_prompt_ie9') + '</span>');
  } else {
    upload.append('<span>' + I18n.t('txt.admin.multibrand.brand.drag_and_drop_logo_prompt') + '</span>');
    upload.append('<span class="drag-hint">' + I18n.t('txt.admin.multibrand.brand.dragging_logo_prompt') + '</span>');
  }

  preview.append($('<img/>').attr('src', this.element.data('url')));
  preview.append('<div class="filename"></div>');
  preview.append('<div class="filesize"></div>');

  this._resetIcons();
  this._resetFileInfo();
};

UploadField.prototype.reset = function() {
  this.clear();
  this._resetIcons();
  this._resetFileInfo();
  this.hiddenField.remove();
};

UploadField.prototype._resetIcons = function() {
  var imgUrl = this.element.data('url');

  if (imgUrl) {
    this.element.addClass('preview');
  } else {
    this.element.removeClass('preview');
  }

  this.fieldImage(imgUrl || 'images/logo_placeholder.png');
};

UploadField.prototype._resetFileInfo = function() {
  var name = this.element.data('name') ? this.element.data('name') : '',
      size = this.element.data('size') ? '(' + humanFileSize(this.element.data('size')) + ')' : '';

  this.element.find('.filename').text(name);
  this.element.find('.filesize').text(size);
};

UploadField.prototype.addFill = function(event) {
  this.element.addClass('dragging');
};

UploadField.prototype.removeFill = function(event) {
  this.element.removeClass('dragging');
};

UploadField.prototype.unsetLogo = function() {
  this.clear();

  this.fieldElement.prepend(this.hiddenField);
  this.fieldElement.val("");

  this.fieldImage("images/logo_placeholder.png");

  this.element.find('.filename').text('');
  this.element.find('.filesize').text('');

  this.element.removeClass('preview');
};

UploadField.prototype.preview = function(file) {
  if (!file) {
    return this.unsetLogo();
  }

  var reader = new FileReader();
  this.showFileInfo(file);

  var self = this;

  this.hiddenField.detach();

  reader.onload = function(event) {
    self.fieldImage(event.target.result);
  };

  reader.target = this.element;
  reader.readAsDataURL(file);
  this.element.addClass('preview');
};

UploadField.prototype.fieldImage = function(src) {
  this.element.find('img').attr('src', src);
  this.headerLogo.attr('src', src);
};

UploadField.prototype.showFileInfo = function(file) {
  this.element.find('.filename').text(file.name);
  this.element.find('.filesize').text('(' + humanFileSize(file.size) + ')');
};

UploadField.prototype.drop = function(event) {
  this.removeFill();
  event.target.files = event.originalEvent.dataTransfer.files[0];
};

UploadField.prototype.imagePreview = function(event) {
  var file = event.target.files[0];

  if (this.validate(file)) {
    this.preview(file);
  } else {
    event.target.value = null;
  }
};

UploadField.prototype.ie9Preview = function(event) {
  var file = event.target.files[0];

  if (this.validate(file)) {
    this.element.find('.filename').text(event.target.value.match(/[^\/\\]+$/)[0]);
    this.element.addClass('preview');
  } else {
    event.target.value = null;
  }
};

UploadField.prototype.validate = function(file) {
  if (this.isTooLarge(file)) {
    this.warning(tooLargeOptions);
    return false;
  } else if (this.isTypeInvalid(file)) {
    this.warning(wrongTypeOptions);
    return false;
  }

  this.success(successOptions);
  return true;
};

UploadField.prototype.isTooLarge = function(file) {
  return file.size > maxLogoSize;
};

UploadField.prototype.isTypeInvalid = function(file) {
  return ['image/jpeg', 'image/png', 'image/gif'].indexOf(file.type) === -1;
};

/*
 *  http://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable
 */
function humanFileSize(bytes) {
  var thresh = 1024;
  if(bytes < thresh) return bytes + ' B';
  var units = ['Kb','Mb','Gb','Tb','Pb','Eb','Zb','Yb'];
  var u = -1;
  do {
    bytes /= thresh;
    ++u;
  } while(bytes >= thresh);
  return bytes.toFixed(1)+' '+units[u];
}

module.exports = UploadField;
