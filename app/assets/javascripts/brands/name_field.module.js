/*global require, $ */

var Validation = require('admin/validation'),
    utils = require('admin/utils');

function NameField(element) {
  var self = this;

  this.element = $(element);
  this.headerName = this.element.closest('.capsule').find('.capsule-title.name');
  this.originalName = this.headerName.text();

  this.element.on('input', this.validate.bind(this));
  this.element.on('input', this.updateHeaderName.bind(this));
  this.element.closest('form').on('reset', function(event) {
    setTimeout(function () {
      self.headerName.text(self.originalName);
      self.validate();
    });
  });
  this.validate();
}

NameField.prototype.validate = function() {
  if (this.element.val()) {
    this.clear();
  } else {
    this.silentError();
  }
};

NameField.prototype.updateHeaderName = function() {
  this.headerName.text(this.element.val());
};

utils.mixin(NameField.prototype, Validation);

module.exports = NameField;
