/*globals $, require, I18n*/
module.exports = function() {
  $(document).ready(function() {

  var ENTERPRISE_PLAN = 4;

  var LotusClient      = require('admin/lotus_client'),
      SubdomainField   = require('brands/subdomain_field'),
      UploadField      = require('brands/upload_field'),
      NameField        = require('brands/name_field'),
      SignatureField   = require('brands/signature_field'),
      HostMappingField = require('brands/host_mapping_field'),
      BrandForm        = require('brands/brand_form'),
      BrandCapsule     = require('brands/brand_capsule');

  $(document).on('click', 'a[target="_lotus"]', function(event) {
    var $el = $(event.currentTarget);
    var href = $el.attr('href').replace(/^\/agent\//, '/');
    var brandName = $el.closest('.capsule').find('.capsule-title.name').html();

    event.preventDefault();

    if ($el.closest('form').hasClass('changed')) {
      LotusClient.showConfirmationModal({
        title: I18n.t('txt.multibrand.brand.unsaved_changes_modal.title'),
        message: I18n.t('txt.multibrand.brand.unsaved_changes_modal.body', {brand_name: brandName}),
        confirmLabel: I18n.t('txt.multibrand.brand.unsaved_changes_modal.discard_changes_label'),
        cancelLabel: I18n.t('txt.multibrand.brand.unsaved_changes_modal.keep_editing_label')
      }).then(function () {
        LotusClient.transitionTo(href);
      });
    } else {
      LotusClient.transitionTo(href);
    }
  });

  $('.mb-landing').on('click', 'button.show-brands', function(e) {
    e.stopPropagation();
    dismissLandingPage();
  });

  $('.mb-landing').on('click', 'button.request-brands', function(e) {
    e.stopPropagation();
    requestBrands();
  });

  $('form').on('change', function(event) {
    $(this).addClass('changed');
  });

  $('form').on('reset', function(event) {
    $(this).removeClass('changed');
  });

  $('.capsule-container').on('click', 'button.create', function(event) {
    var el = $(event.target);

    LotusClient.showHelpCenterWithBrand({brandId: this.dataset.brandId}).then(function() {
      el.text(I18n.t('txt.admin.multibrand.buttons.manage'));
      el.removeClass('create').addClass('manage');
    }, function() {});
  });

  $('.capsule-container').on('click', 'button.manage', function(event) {
    var el = $(event.target);
    window.open(el.data('brand-url') + '/hc/signin');
  });

  function requestBrands() {
    var authToken = $('meta[name="csrf-token"]').attr('content');

    $('.request-brands').addClass('loading')
      .text(I18n.t('txt.admin.multibrand.hub_and_spoke.loading'))
      .prop('disabled', 'disabled');

    $.ajax({
      url: "/brands/request_multibrand?authenticity_token=" + encodeURIComponent(authToken),
      type: 'POST'
    }).then(onRequestBrandsComplete);
  }

  function switchRequireBrand(checked) {
    var authToken = $('meta[name="csrf-token"]').attr('content');

    var data = 'settings[require_brand_on_new_tickets]=' + $switchRequireBrandCheckbox.is(':checked');

    $.ajax({
       type: 'PUT',
       url: "/brands/switch_require_brand?authenticity_token=" + encodeURIComponent(authToken),
       data: data
     }).error(function() {
       LotusClient.growl(I18n.t('txt.admin.multibrand.require_brand.error'), 'error');
     }).success(function() {
       LotusClient.growl(I18n.t('txt.admin.multibrand.require_brand.success'));
     });
  }

  function onRequestBrandsComplete() {
    $('.request-brands').addClass('disabled')
    .removeClass('get-started request-brands')
    .text(I18n.t('txt.admin.multibrand.hub_and_spoke.ticket_sent'));
  }

  function dismissLandingPage() {
    $('.mb-landing').hide();
    $('.brands').show();
  }

  function isZeroState() {
    return $('.zero-state').length > 0;
  }

  function isChecklistState() {
    return $('.checklist').length > 0;
  }

  function isHubAndSpoke() {
    return $('.hub-spoke').length > 0;
  }

  if (isZeroState()) {
    $('.brands').hide();
    $('.capsule-container > .capsule').each(function() {
      new BrandCapsule(this, {
        onOpen: function(event) {
          this.element.trigger('capsule.open');
          if (!this.isNew()) {
            $.ztooltip.show('header');
            event.stopPropagation();
          }
        },
        onClose: function(event) {
          $('div.ztooltip.z-tooltip-anchor.onboarding').removeData('ztooltip')
          $('div.ztooltip.z-tooltip-anchor.onboarding').unbind();
          $.ztooltip.hideAll();
        }
      });
    });
  } else {
    $('.capsule-container > .capsule').each(function() {
      new BrandCapsule(this, {
        onOpen: function(event) {
          this.element.trigger('capsule.open');
        }
      });
    });
  }

  if (isChecklistState()) {
    $('.checklist').show();

    $('.mb-checklist .checklist-item:first-child').addClass('btn btn--secondary btn--pill');

    $('.mb-checklist .checklist-item.btn.switch-to-new-zendesk').on('click', function (e) {
      e.preventDefault();

      parent.location = "/switch?redirect_uri=/agent/admin/brands";
    });

    $('.mb-checklist .checklist-item.btn.create-help-center').on('click', function (e) {
      e.preventDefault();

      LotusClient.showHelpCenter().then(function() {
        window.location.reload();
      }, function () { /* noop */ });
    });

    $('.mb-checklist .checklist-item.btn.activate-help-center').on('click', function (e) {
      e.preventDefault();

      window.open('/hc/admin/general_settings', '_blank');
    });
  }

  if (isHubAndSpoke()) {
    $('.hub-spoke').show();
  }

  $('#tour').click(function(event) {
    $('.capsule.default').each(function() {
      this.trigger('open', event);
    });
  });

  var spinner = $('.spinner'),
      spinnerWidth = (parseInt(spinner.css('right'), 10) * 2) + spinner.width();

  $.fn.subdomainField = function(options) {
    this.each(function() {
      var $el = $(this);
      var subdomainField = new SubdomainField($el, options);

      $el.closest('.capsule').on('capsule.open', function () {
        subdomainField.repositionSuffix();
      });
    });
    return this;
  };

  $('.subdomain-field').subdomainField({
    text: '.zendesk.com',
    paddingRight: spinnerWidth
  });

  $.fn.uploadField = function(options) {
    this.each(function() {
      new UploadField($(this), options);
    });
    return this;
  };

  $('.upload-field').uploadField();

  $.fn.signatureField = function(options) {
    this.each(function() {
      new SignatureField($(this), options);
    });
    return this;
  };

  $('.signature-template-field').signatureField();

  $.fn.nameField = function(options) {
    this.each(function() {
      new NameField($(this), options);
    });
    return this;
  };

  $('.name-field').nameField();

  $.fn.hostMappingField = function(options) {
    this.each(function() {
      var $el = $(this);
      var hostMappingField = new HostMappingField($el, options);
      var $subdomainField = $el.closest('.capsule').find('.subdomain-field');

      $subdomainField.on('input', function () {
        hostMappingField.validate();
      });
    });
    return this;
  };

  $('.host-mapping-field').hostMappingField({
    subdomain: function () {
      return this.element.closest('.capsule').find('.subdomain-field').val();
    },
    paddingRight: spinnerWidth
  });

  $.fn.brandForm = function(options) {
    this.each(function() {
      new BrandForm($(this), options);
    });
    return this;
  };

  $('.brand-form').brandForm();

  $('.add-brand:not(.disabled)').click(function() {
    $('.capsule.new').show().trigger('open');
  });

  var $switchRequireBrandCheckbox = $('.brands .switch input[type=checkbox]');
  $switchRequireBrandCheckbox.change(function() {
    switchRequireBrand();
  });

  var flashToGrowl = require('admin/flash_to_growl');
  flashToGrowl.processMessages(".flash > div");


  var goToName = function(action) {
    $.ztooltip.show('name');
  };

  var goToSubdomain = function(action) {
    $.ztooltip.show('subdomain');
  };

  var goToSignatureTemplate = function(action) {
    $.ztooltip.show('signaturetemplate');
  };

  var goToHostMapping = function(action) {
    $.ztooltip.show('hostmapping');
  };

  var goToHelpCenter = function(action) {
    $.ztooltip.show('helpcenter');
  };

  var finish = function(action) {
    $.ztooltip.hideAll();
  };

  // define the actions
  $.ztooltip
    .on('goToName', goToName)
    .on('goToSubdomain', goToSubdomain)
    .on('goToSignatureTemplate', goToSignatureTemplate)
    .on('goToHostMapping', goToHostMapping)
    .on('goToHelpCenter', goToHelpCenter)
    .on('finish', finish);
  });
};
