/*globals $, I18n, require*/
var lotusClient = require('admin/lotus_client');

function subdomainChangedConfirmation(form, event, subdomainField) {
  var capsule = form.closest('.capsule');
  var message = I18n.t("txt.admin.brands.confirmation_modal_message", { brand_name: capsule.find(".capsule-title.name").html(), original_subdomain: subdomainField.data('original-value') });

  if (capsule.hasClass('agent')) {
    message = I18n.t("txt.admin.brands.confirmation_modal_message_for_agent_route") + message;
  }

  var options = {
    title: I18n.t("txt.admin.brands.confirmation_modal_title"),
    message: message,
    confirmLabel: I18n.t("txt.admin.brands.confirmation_modal_confirm_label"),
    cancelLabel: I18n.t("txt.admin.brands.confirmation_modal_cancel_label")
  };

  lotusClient.showConfirmationModal(options).then(function() {
    form.data('subdomain-change-confirmed', true);
    form.submit();
  },
  function() {
    form.data('subdomain-change-confirmed', false);
  });

  event.preventDefault();
}

function BrandForm(element) {
  var form = $(element);

  form.on('reset', function(event) {
    form.find('.validation').removeClass('success warning error error-silent');
    form.find('.validation').find('.validation-message').remove();
    if (form.closest('.capsule').hasClass('new')) {
      form.find('button[type="submit"]').attr('disabled', true);
    } else {
      form.find('button[type="submit"]').removeAttr('disabled');
    }
  });

  form.submit(function(event) {
    if(form.data('subdomain-change-confirmed')) { return; }

    var subdomainField = form.find(".subdomain-field");
    if(subdomainField.attr('value') === undefined) { return; }
    if(subdomainField.attr('value') !== subdomainField.val()) {
      subdomainChangedConfirmation(form, event, subdomainField);
    }
  });
}

module.exports = BrandForm;
