/*global require, $, I18n */

var Validation = require('admin/validation'),
    utils = require('admin/utils');

var invalidOptions = {
      message: I18n.t('txt.admin.multibrand.validations.signature_template.invalid')
    };

var signatureValidationTimeout = 1000; // milliseconds

function SignatureField(element) {
  var self = this;
  this.element = $(element);
  this.element.on('input', this.validate.bind(this));

  this.validate();
}

utils.mixin(SignatureField.prototype, Validation);

SignatureField.prototype.validate = function() {
  clearTimeout(this.timer);
  var self = this;

  if (!this.currentlyValid()) {
    this.timer = setTimeout(function() {
      self.error(invalidOptions);
    }, signatureValidationTimeout);
  } else {
    this.clear();
  }
};

SignatureField.prototype.currentlyValid = function() {
  var val = this.element.val();
  // Regex matches any `{{` that doesn't have a matching `}}`
  return !val.match(/\{\{(?![^{]*\}\})/)
};

module.exports = SignatureField;
