/*global Zendesk, jQuery, $:false*/
/*
 * Defined functions in this file will automatically be called
 * for controller actions if they they are defined on
 * window.mobile and follow this naming convention:
 *
 * <controller_name>_<action_name>
 *
 * See app/views/layouts/user.mobile_v2.erb
 */

;(function(w, $) {

  // defining view-specific initializers
  w.mobile = $.extend(w.mobile || {}, {

    // Basic feature detection
    features: {

      input: function() {
        var _input = {};

        var input_detector = function(name) {
          var i = document.createElement("input");
          i.setAttribute("type", name);
          return i.type !== "text";
        }

        var input_types = [
          "color", "date",
          "datetime", "datetime-local",
          "email", "month",
          "number", "range",
          "search", "tel",
          "time", "url",
          "week"
        ];

        for (var i = 0; i < input_types.length; i++) {
          _input["supports_" + input_types[i]] = input_detector(input_types[i]);
        };

        return _input;
      }(),

      platform: {
        is_newer_ios: function() {
          var ios = /(iPad|iPhone|iPod)/i.test(navigator.userAgent);
          var v1 = /CPU like Mac OS X/i.test(navigator.userAgent);
          var v2_4 = /OS [2-4]_\d(_\d)? like Mac OS X/i.test(navigator.userAgent);
          return ios && !(v1 || v2_4);
        }()
      },

      orientation: function() {
        var _orientation = function() {
         return window.innerWidth > window.innerHeight ? "landscape" : "portrait";
        };

        $(window).resize(function() {
          w.mobile.features.orientation = _orientation();
        });

        return _orientation();
      }()
    },

    parseDate : function(input, format) {
      format = format || 'yyyy-mm-dd'; // default format
      var parts = input.match(/(\d+)/g), i = 0, fmt = {};
      // extract date-part indexes from the format
      format.replace(/(yyyy|dd|mm)/g, function(part) { fmt[part] = i++; });

      return new Date(parts[fmt['yyyy']], parts[fmt['mm']]-1, parts[fmt['dd']]);
    },

    // Utility functions for the flash
    show_flash: function(message, type) {
      $("div#flash > div").attr("class", type).html(message);
      $("div#flash").show();
    },

    hide_flash: function() {
      $("div#flash").hide();
    },

    forums_index: function() {
      // Pinned entries
      $("body ul.list-view i").click(function() {
        $(this).toggleClass("collapse");
        $(this).parent().siblings(".pinned-entry").toggleClass("hidden");
      });


      Zendesk.MobileV2.IOS4ScrollFix.init();
    },

    highlight_new_ticket: function() {
      $('.list-view .item-highlight')
        .css("background-color", "rgba(255, 249, 217, 1.0)")
        .animate({ "background-color": "transparent" }, 3000, "swing");
    },

    requests_new: function() {
      $("input.new_date_picker").blur(function() {
        var val = $(this).val();
        if (w.mobile.features.platform.is_newer_ios) {
          // val is in the form 'yyyy-mm-dd' (ISO8601) but IOS doesn't
          // instantiate this format correctly with either new Date('yyyy-mm-dd')
          // or Date.parse('yyyy-mm-dd')
          var date = w.mobile.parseDate(val);

          // Skipping input validation here, since it is not possible to input an
          // invalid date with the native datepicker on IOS
        } else {
          var date = new Date(val);

          if (isNaN(date.getTime())) {
            $(this).siblings("div#date-flash").show();
            $(this).addClass("error");
          } else {
            $(this).siblings("div#date-flash").hide();
            $(this).removeClass("error");
            var altField = $(this).siblings("#" + $(this).data("field"));
            altField.val(date.toISOString().split("T")[0]);
          }
        }
      });
    },

    // Onload page specific functions
    "requests/anonymous_new": function() {
      w.mobile.requests_new();
    },

    "requests/portal_new": function() {
      w.mobile.requests_new();
    },

    "requests/anonymous_create": function() {
      w.mobile.requests_new();
    },

    "requests/portal_create": function() {
      w.mobile.requests_new();
    },

    "requests/portal_index": function() {
      w.mobile.highlight_new_ticket();
    },

    "requests/portal_show": function() {
      Zendesk.MobileV2.Comments = new Zendesk.MobileV2.AjaxCalls({
        container : "div#content div.events",
        submitBtn : "div#content a[data-ajax-button][data-form-submit]"
      });
    },

    "forums_show": function() {
      // Ajax Buttons
      Zendesk.MobileV2.Comments = new Zendesk.MobileV2.AjaxCalls({
        container : "div#content ul.list-view",
        loadBtn   : "div#content a[data-ajax-button]"
      });
    },

    registration_index: function() {
      var img_icon = $("div#recaptcha_widget div#refresh a img.icon");

      img_icon.bind('webkitAnimationEnd', function() {
        $(this).css('webkitAnimationName', '');
      });

      img_icon.click(function() {
        $(this).css('webkitAnimationName', 'rotate360');
      });
    },

    "mobile/landing_page_index": function( test ) {
      var $openApp = $("a#openApp"),
          $downloadApp = $("a#downloadApp"),
          $fullSite = $("a#fullSite"),
          account = $openApp.data("account-name"),
          ticketID = $openApp.data("ticket-id"),
          gn = Zendesk.MobileV2.GoNative,
          userAgent = gn.userAgent();

      if ( userAgent === "iOS" ) {

        /*
         * Linking to the native App doesn't work!
         * It opens the app but it doesn't take the
         * user to the ticket page.
         * We're hidding this link for now until
         * the native app gets updated

        if ( gn.hasCookie() ) gn.openIOSApp( account, ticketID );

        $openApp
          .click(function() {
            gn.setCookie();
            gn.openIOSApp( account, ticketID );
          });

        */

        $openApp.hide();

        $downloadApp
          .click(function() {
            gn.deleteCookie();
            gn.openItunes();
          });

        $fullSite
          .click(function() {
            gn.deleteCookie();
          });

      } else if ( userAgent === "android" ) {

        /*
         * Linking to the native App doesn't work
         * on Android.
         * We're hiding this link for now until thise
         * feature is supported by the OS
         */

        $openApp.hide();

        $downloadApp
          .click(function() {
            gn.openMarketplace();
          });

      }

    },

    entries_show: function() {
      // Ajax Button
      Zendesk.MobileV2.Comments = new Zendesk.MobileV2.AjaxCalls({
        container : "div#content ul.comments",
        submitBtn : "div#content a[data-ajax-button][data-form-submit]",
        loadBtn   : "div#content a[data-ajax-button]:not([data-form-submit])"
      });


      // Openning and scalling images
      var $container = $("div.content"),
          $images = $("div.content img, li.comment img");

      $images
        .scaleTo( $container )
        .click(function() {
          window.location = $(this).attr('src');
        });

    },

    // Shamelessly plucked from application.js
    toggleDueDate: function() {
      if ($("#ticket_ticket_type_id").val() === "4") {
        $('#ticket_date').show();
      } else {
        $('#ticket_date').hide();
      }
    },

    callbacks: {}
  });

  $(function() {

    //Search Load More Btn
    new Zendesk.MobileV2.AjaxCalls({
      container : "div#search-results ul.list-view",
      loadBtn   : "div#search-results a[data-ajax-button]"
    });

    // Hide the address-bar without
    // breaking the footer's links
    setTimeout(function(){
      var $footer = $("footer");
      $footer.css("position", "relative");
      window.scrollTo(0, 0);
      $footer.css("position", "fixed");
    }, 0);

    // Close flash message
    $("div#flash")
      .find("span#close-icon")
      .click(function() {
        $(this)
          .closest("#flash")
          .slideUp("fast");
      });

    // Open iOS App / iTunes / marketplace
    // from the flash message's link
    var $flashNotice = $("div#flash div.notice"),
        $openIOSAppLink = $flashNotice.find("a[href='#openIOSApp']"),
        $getIOSAppLink = $flashNotice.find("a[href='#getIOSApp']"),
        $getAndroidAppLink = $flashNotice.find("a[href='#getAndroidApp']"),
        gn = Zendesk.MobileV2.GoNative,
        userAgent = gn.userAgent();

    if (userAgent === "iOS") {

      $openIOSAppLink
        .click(function() {
          gn.setCookie();
          gn.openIOSApp();
          return false;
        });

      $getIOSAppLink
        .click(function() {
          gn.deleteCookie();
          gn.openItunes();
          return false;
        });

    } else if (userAgent === "android") {

      $getAndroidAppLink
        .click(function() {
          gn.openMarketplace();
          return false;
        });

    }

  });

}(this, jQuery));
