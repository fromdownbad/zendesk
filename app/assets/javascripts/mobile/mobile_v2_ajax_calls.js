/*global Zendesk, jQuery*/
(function($) {
  Zendesk.NS('MobileV2');

  Zendesk.MobileV2.AjaxCalls = function(settings) {
    this.cache(settings);
    this.events();
  };

  Zendesk.MobileV2.AjaxCalls.prototype = {
    cache : function(settings) {
      this.settings = settings;
      this.$container = $(settings.container);
      this.$submitBtn = $(settings.submitBtn);
      this.$loadBtn = $(settings.loadBtn);
      this.$form = this.$submitBtn.closest("form");
      this.$counter = $("h2.comments-title span#count");
      this.$textarea = this.$form.find("textarea");
      this.$errorFlash = $("div#comment-flash");
      this.$checkbox = this.$form.find("input[type=checkbox]"); // Tickets form only
      this.$checkboxLabel = this.$form.find("label#resolved"); // Tickets form only
    },

    events : function() {
      var _this = this;

      this.$loadBtn
        .live("click", function() {
          var $btn = $(this);
          if (_this.isDisabled($btn)) return false;
          _this.hideSpinner($btn);
          _this.disable($btn);
          _this.getData($btn);
          return false;
        });

      this.$submitBtn
        .click(function() {
          var $btn = $(this);
          if (_this.isDisabled($btn)) return false;
          _this.showSpinner($btn);
          _this.disable($btn);
          _this.submitForm();
          return false;
          // Where are new comments being appended to the list?
          // app/views/posts/create.js.erb
        });

      this.$form
        .ajaxSuccess(function() {
          if (_this.isDisabled(_this.$submitBtn)) {
            _this.hideSpinner(_this.$submitBtn);
            _this.enable(_this.$submitBtn);
          }
        });

    },

    isDisabled : function($btn) {
      return $btn.attr("data-disabled") === "true";
    },

    showSpinner : function($btn) {
      $btn.children("span.text").hide();
      $btn.children("span.spinner").show();
    },

    hideSpinner : function($btn) {
      $btn.children("span.spinner").hide();
      $btn.children("span.text").show();
    },

    disable : function($btn) {
      $btn.attr("data-disabled", "true");
    },

    enable : function($btn, $data) {
      // If $data is passed, we check if there is an item with the data
      // attribute "last" set to true, and in that case we don't enable
      // the button because there aren't any more items to fetch
      if ( $data && $data.filter("[data-last='true']").length ) return;
      $btn.attr("data-disabled", "false");
    },

    getUrlFrom : function($btn) {
      return $btn.attr("data-url");
    },

    getData : function($btn) {
      var _this = this,
          url = this.getUrlFrom($btn);

      $.get(url, { page: $btn.attr("data-ajax-page") })
        .done(function(data) {
          var $data = $($.trim(data));
          _this.updateContainer();
          _this.appendLoaded($data);
          _this.hideSpinner($btn);
          _this.updateUrl($btn);
          _this.enable($btn, $data);
        });
    },

    updateContainer : function() {
      // When we use search, the first response brings back the the items
      // wrapped inside the container.
      // If we click cancel, the container is remove from the DOM.
      this.$container = $(this.settings.container);
    },

    appendLoaded : function($data) {
      var $submittedData = this.$container.find("[data-submitted='true']");
      if ($submittedData.length) $submittedData.first().before($data);
      else this.$container.append($data);
      this.ensureUnique($data, $submittedData);
    },

    ensureUnique : function($data, $submittedData) {
      $submittedData.each(function() {
        var $item = $(this),
            id = $item.attr("data-id");
        if ($data.filter("[data-id='" + id + "']").length) $item.remove();
      });
    },

    appendSubmitted : function($data) {
      $data.attr("data-submitted", "true");
      this.$container
        .append($data)
        .show()
        .parent()
        .show();
    },

    updateUrl : function($btn) {
      var currentPage = $btn.attr("data-ajax-page"),
          newPage = parseInt(currentPage, 10) + 1;
      $btn.attr("data-ajax-page", newPage);
    },

    submitForm : function() {
      this.$form.submit();
    },

    updateCounter : function() {
      var count = this.$counter.text(),
          updated = parseInt( count, 10 ) + 1;
      this.$counter.text( updated );
    },

    highlight : function($item) {
      $item
        .css("background-color", "rgba(255, 249, 217, 1.0)")
        .animate({ "background-color": "transparent" }, 3000, "swing");
    },

    displayError : function(msg) {
      this.$textarea.addClass("error");
      this.$errorFlash
        .html(msg)
        .addClass("error")
        .show();
    },

    resetForm : function() {
      this.$textarea
        .removeClass("error")
        .val("");

      this.$errorFlash
        .html("")
        .removeClass("error")
        .hide();

      this.$checkbox
        .attr('checked', false);
    },

    updateCheckbox : function(label) {
      var currentLabel = this.$checkboxLabel.html();
      if (label !== currentLabel) this.$checkboxLabel.html(label);
    }

  };


}(jQuery));