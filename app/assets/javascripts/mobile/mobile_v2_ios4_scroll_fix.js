/*global Zendesk, jQuery*/
(function ( $ ) {
  Zendesk.NS('MobileV2');

  Zendesk.MobileV2.IOS4ScrollFix = {

    init : function() {
      this._footer = $("#content footer");
      this._footerHeight = this._footer.outerHeight();
      this._scrollArea = $([]);

      if ( !this.isTouchDevice() || this.supportsScrolling() || this.isAndroidDevice() ) return;
      this.setScrollableWrapper();
      this.setWrapperHeight();
      this.preventNativeScrolling();
      this.setCustomScrolling();
    },

    isTouchDevice : function () {
      try {
        document.createEvent("TouchEvent");
        return true;
      } catch ( e ) {
        return false;
      }
    },

    supportsScrolling : function () {
      return "webkitOverflowScrolling" in document.body.style;
    },

    isAndroidDevice : function () {
      var matcher = /Android/;
      var user_agent = navigator.userAgent;

      return user_agent.match(matcher);
    },

    setScrollableWrapper : function () {
      this._scrollArea =
        $("body")
          .children("div")
          .wrapAll("<div/>")
          .parent()
          .css({ overflow : "scroll" });
    },

    setWrapperHeight : function () {
      this._windowHeight = $(window).height();

      this._scrollArea.css({
        height : (this._windowHeight - this._footerHeight + "px")
      });
    },

    preventNativeScrolling : function () {
      document.body.addEventListener( "touchmove", function ( event ) {
        event.preventDefault();
      }, false );
    },

    setCustomScrolling : function () {
      var scrollStartPos = 0;

      this._scrollArea[0].addEventListener( "touchstart", function ( event ) {
        scrollStartPos = this.scrollTop + event.touches[0].pageY;
      }, false );

      this._scrollArea[0].addEventListener( "touchmove", function ( event ) {
        this.scrollTop = scrollStartPos - event.touches[0].pageY;
      }, false );

    }


  };

}(jQuery));
