(function ( $ ) {

  $.fn.scaleTo = function( container ) {
    var $imgs = this,
        $win = $( window ),
        $container = $( container );

    function storeDimensions( img ) {
      $(img).data({
        width : img.width,
        height : img.height
      });
    }

    function scale( img ) {
        var $img = $(img),
            width = $img.data("width"),
            height = $img.data("height"),
            attrWidth = parseInt( $img.attr("width"), 10 ),
            attrHeight = parseInt( $img.attr("height"), 10 ),
            frame = $container.width();

        if ( width < frame ) return;

        $img.width( frame );

        if ( !attrHeight ) return;

        var ratio = ( frame * 100 / width ) * 0.01,
            newHeight = ( height * ratio ).toFixed();

        $img.height( newHeight );
    }

    $imgs
      .bind("load", function() {
        storeDimensions( this );
        scale( this );
      })
      .each(function() {
        if ( this.complete ) $(this).load();
      });

    $win.resize(function() {
      $imgs.each(function() {
        scale( this );
      });
    });

    return this;

  };

} ( jQuery ) );