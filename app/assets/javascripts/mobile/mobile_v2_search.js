/*global Zendesk, jQuery*/
(function($) {
  Zendesk.NS('MobileV2');

  Zendesk.MobileV2.Search = {
    headerWrapper: null,
    searchForm: null,
    searchHeader: null,
    searchButton: null,
    searchCancelButton: null,
    searchInput: null,
    searchInputResetButton: null,
    searchInputContainer: null,

    hideContent: function() {
      if($('#flash').is(":visible")) {
        this.flashHasBeenHidden = true;
        this.flashMessages.hide();
      }
      $('#flash').hide();
      $('#content').hide();
    },

    init: function() {
      var self = this;
      this.headerWrapper = $('#mobile-header-wrapper');
      this.searchForm = $('#mobile-header-wrapper form#searchform');
      this.searchHeader = $('#mobile-header-wrapper #search');
      this.searchButton = $('#mobile-header-wrapper #search-button');
      this.searchCancelButton = $('#mobile-header-wrapper .cancel');
      this.searchInput = $('#mobile-header-wrapper #search input[type="search"]');
      this.searchInputResetButton = $('#mobile-header-wrapper #search .reset');
      this.searchInputContainer = $('#mobile-header-wrapper #search .input-container');
      this.searchIcon = $('#mobile-header-wrapper #search .icon');
      this.flashMessages = $('#flash');
      this.flashHasBeenHidden = false;
      this.is_android_mobile_safari = navigator.userAgent.match(/Android.+(Mobile Safari)/);

      // Add bindings
      this.searchButton.click(function(e) {
        e.preventDefault();
        self.searchHeader.show();
        self.searchInput.focus();
      });

      this.searchCancelButton.click(function(e) {
        e.preventDefault();
        self.searchHeader.hide();
        self._removeSearchResults();
        self._searchInputReset();
        $('#content').show();
        if(self.flashHasBeenHidden) {
          $('#flash').show();
        }
        self._clearForm();
      });

      this.searchInput.keydown(function(e) {
        self.hideContent();
        self.searchInputResetButton.show();
        self._removeSearchResults();
      });

      this.searchInput.blur(function(e) {
        if(self.searchInput.val() === "") {
          self.searchInputResetButton.hide();
        }
      });

      this.searchInputResetButton.click(function(e) {
        self._searchInputReset();
        self._removeSearchResults();
      });

      this.searchForm.submit(function(e) {
        e.preventDefault();

        var requestUrl = self.searchForm.attr('action');
        var query = self.searchInput.val();
        var perPage = 5;

        self.searchIcon.toggleClass("searching", true);

        self._showContentSearchSpinnerForAndroidMobileSafari();

        $.ajax({
          url: requestUrl,
          data: {
            query: query,
            incremental: true,
            format: "mobile_v2",
            page: 1,
            per_page: perPage,
            xhr: true
          }
        }).done(function(data) {
          // This handles an edge case:
          // 1. Go from mobile -> desktop site via linke, then hit the back button.
          //    Browser page is not refreshed in some browsers. Mobile site is shown but
          //    a preference for desktop site is in session.
          // 2. Do a search. (ajax)
          // 3. Search returns results rendered for desktop site.
          if (data.match(/<!-- MOBILE -->/)) {
            var parsedResults = $(data);
            parsedResults.attr("id", "search-results");
            self.searchInput.blur(); // Dismiss the iPhone keyboard
            self.searchIcon.toggleClass("searching", false);
            self._hideContentSearchSpinner();
            self.headerWrapper.after(parsedResults);
          } else {
            // Page refresh and save the search query to user after the page refresh
            window.location.search = 'is_mobile=true&search_refresh=' + query;
          }
        });

      });

      // Fetch query params
      var params = window.location.search.substring(1).split('&');
      var paramsObject = {};
      for (var i = 0; i < params.length; i++){
        var tmp = params[i].split('=');
        paramsObject[tmp[0]] = tmp[1];
      }

      // If search_refresh is set, assume it's to handle above mentioned
      // edge case and thus do a new search
      if (paramsObject.search_refresh) {
        this.searchHeader.show();
        this.hideContent();
        this.searchInput.val(paramsObject.search_refresh);
        this.searchForm.submit();
      }

    },

    _removeSearchResults: function() {
      $('#search-results').remove();
    },

    _showContentSearchSpinnerForAndroidMobileSafari: function() {
      console.log(navigator.userAgent);
      if(this.is_android_mobile_safari) {
        $('html').addClass('searchSpinner');
      }
    },

    _hideContentSearchSpinner: function() {
      $('html').removeClass('searchSpinner');
    },

    _clearForm: function() {
      this.searchInput.val('');
    },

    _searchInputReset: function() {
      this._clearForm();
      this.searchInputResetButton.hide();
    },

    _toggleSearching: function() {
      this.searchInput.toggleClass("searching");
    }

  };

}(jQuery));
