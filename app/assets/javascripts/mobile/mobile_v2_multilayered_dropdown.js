/*global Zendesk, jQuery*/
(function ( $ ) {
  Zendesk.NS('MobileV2');

  Zendesk.MobileV2.MultiLayeredDropDown = {

    init : function () {
      this.prepare();
      this.cache();
      this.overlay();
      this.events();
      return this;
    },

    cache : function () {
      this.$doc = $(document);
      this.$body = $(document.body);
      this.$overlay = $("<div id='overlay'/>"); // wrapper
      this.$layer = $("<div id='layer'/>"); // dark layer
      this.$header = $("div#mobile-header-wrapper");
      this.$field = $("div.field-dropdown");
      this.$box = this.$field.children("div.input-box");
      this.$input = this.$field.children("input");
      this.$current;
      this.offset;

      this.css3 = this.css3TransformSupport( this.$overlay[0] );
      this.duration = 400;
    },

    prepare : function () {
      $("div.field-dropdown")
        .each(function() {
          var $field = $(this),
              $ul1 = $field.children("ul.drop-list");

          if ( !$ul1.length ) return;

          var $ul2 = $ul1.find("ul.first-drop"),
              $lis = $ul2.find("li"),
              $box = $("<div class='input-box'></div>"),
              val = $field.children("input").val(),
              txt = val && val.length ?
                $lis.filter(function() {
                  return $(this).data("value") === val;
                }).text() : "-";

          $lis
            .contents()
            .filter(function(){
              return this.nodeType === 3 &&
                ( this.textContent || this.innerText )
                .replace(/\s/g, "").length;
            })
            .wrap("<a href='#' />");

          $lis
            .first()
            .children()
            .text("-");

          $ul1.replaceWith( $ul2 );
          $field.append( $box );
          $box.text( txt );
        });
    },

    css3TransformSupport : function( el ) {
      var vendors = ["", "Khtml", "Ms", "Moz", "Webkit", "O"],
          i = 0,
          prop;

      for ( i; i < vendors.length; i++ ) {
          prop = vendors[i] + 'Transition';
          if ( prop in el.style ) return true;
      }

      return false;
    },

    overlay : function () {
      if ( this.css3 ) this.css3Ready();
      this.$layer.appendTo( this.$overlay );
      this.$body.append( this.$overlay );
    },

    css3Ready : function() {
      this.$overlay.css({
        "opacity" : 0,
        "transition" : "opacity " + this.duration + "ms",
        "-moz-transition" : "opacity " + this.duration + "ms",
        "-webkit-transition" : "opacity " + this.duration + "ms",
        "-o-transition" : "opacity " + this.duration + "ms",
        "backface-visibility": "hidden",
        "-webkit-backface-visibility": "hidden",
        "-moz-backface-visibility": "hidden",
        "-ms-backface-visibility": "hidden",
        "-o-backface-visibility": "hidden"
      });
    },

    events : function () {
      var _this = this;

      this.$box
        .click(function(e) {
          var $field = $(this).parent();
          _this.docOffset();
          _this.open( $field );
        });

      this.$field
        .find("li a")
        .live("click", function(e) {
          e.preventDefault();
          var $li = $(this).parent();
          if ( _this.collapsable( $li ) ) {
            return _this.collapse( $li );
          } else if ( _this.expandable( $li ) ) {
            return _this.expand( $li );
          } else {
            _this.select( $li );
            _this.close();
          }
        })
        .end()
        .find("h3")
        .live("click", function() {
          _this.close();
        });

      this.$layer
        .click(function() {
          _this.close();
        });

      this.$header
        .click(function() {
          _this.close();
        });

    },

    docOffset : function () {
      this.offset = this.$doc.scrollTop();
    },

    open : function ( $field ) {
      var $clone = this.clone( $field );
      this.track( $field );
      this.position( $clone );
      this.append( $clone );
      this.slideIn();
      this.mask();
      return $clone;
    },

    clone : function ( $field ) {
      var $clone = $field.clone();

      $clone
        .children()
        .not("ul, h3")
        .remove();

      return $clone;
    },

    track : function ( $field ) {
      this.$current = $field;
    },

    position : function ( $clone ) {
      $clone.css({ top : this.$current.offset().top - 10 + "px" });
    },

    append : function ( $clone) {
      this.$overlay.append( $clone );
    },

    slideIn : function () {
      this.$body.animate({
        scrollTop : this.$current.offset().top - 10
      }, this.duration);
    },

    mask : function () {
      this.$overlay.css({
        height : this.$doc.height(),
        opacity : 1
      });

      if ( !this.css3 ) this.$overlay.fadeIn("fast");
    },

    expandable : function ( $li ) {
      return $li.find("ul").length && !$li.hasClass("open");
    },

    expand : function ( $li ) {
      $li
        .closest(".open")
        .children("a")
        .hide();

      $li
        .addClass("open")
        .siblings()
        .hide();

      $li
        .children("ul")
        .show();
    },

    collapsable : function ( $li ) {
      return $li.find("ul").length && $li.hasClass("open");
    },

    collapse : function ( $li ) {
      $li
        .children("ul")
        .hide();

      $li
        .removeClass("open")
        .siblings()
        .show();

      $li
        .closest(".open")
        .children("a")
        .show();
    },

    select : function ( $li ) {
      var value = $li.data("value"),
          text = $li.text(),
          $box = this.$current.find( this.$box ),
          $input = this.$current.find( this.$input );

      $box.text( text );
      $input.val( value );
    },

    close : function () {
      if ( this.closed() ) return;
      var _this = this;
      this
        .slideOut()
        .done(function() {
          _this.unmask();
        });
    },

    slideOut : function () {
      return $.when(
        this.$body.animate({
          scrollTop : this.offset
        }, this.duration)
      );
    },

    closed : function () {
      return !this.$overlay.is(":visible");
    },

    unmask : function() {
      if ( this.css3 ) this.$overlay.css({ opacity : 0 });
      else this.$overlay.fadeOut( this.duration );

      var _this = this;
      setTimeout(function() {
        _this.$overlay
          .css({ height : 0 })
          .children()
          .not( _this.$layer )
          .remove();
      }, this.duration);
    }

  };

}(jQuery));
