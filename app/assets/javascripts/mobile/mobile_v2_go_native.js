/*global Zendesk, jQuery*/
( function ( window, $ ) {

  Zendesk.NS( "MobileV2" );

  Zendesk.MobileV2.GoNative = {

    userAgent : function() {
      var agent = navigator.userAgent,
          android = /Android/i,
          iPhone = /iPhone/i,
          iPod = /iPod/i;

      if ( agent.match( iPhone ) || agent.match( iPod ) ) return "iOS";
      else if ( agent.match( android ) ) return "android";
    },

    hasCookie : function() {
      return Zendesk.SettingsCookie.get("appInstalled") ? true : false;
    },

    setCookie : function() {
      Zendesk.SettingsCookie.set("appInstalled", true);
    },

    deleteCookie : function() {
      Zendesk.SettingsCookie.erase("appInstalled");
    },

    openItunes : function() {
      var link = "itms-apps://itunes.com/apps/zendesk/zendesk",
          _this = this;
      setTimeout(function() {
        _this.redirect( link );
      }, 50);
    },

    openIOSApp : function( account, ticketID ) {
      var link = "zendesk://";
      link += account && ticketID ? account + "/tickets/" + ticketID : "home";
      this.redirect( link );
    },

    redirect : function( link ) {
      window.location = link;
    },

    openMarketplace : function() {
      this.redirect("market://details?id=com.zendesk.android");
    }

  };

} ( window, jQuery ) );