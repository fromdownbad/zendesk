(function() {
  // From https://github.com/zendesk/zendesk_console/blob/master/app/assets/javascripts/lib/autoresize.module.js
  // Tailored to work with Classic, may not work on IE8

  var initialized = false;
  var timeouts = {};
  var clones = {};

  function cloneOf(el) {
    if (clones[el]) { return clones[el]; }

    var clone = el.cloneNode(false);
    clone.removeAttribute('id');
    clone.className = 'autoResize-clone';
    clone.tabIndex= '-1';
    clone.style.height = 'auto';
    el.parentNode.insertBefore(clone, el);
    clones[el] = clone;
    return clone;
  }

  function resize(el) {
    var clone = cloneOf(el), $el = $j(el), $clone = $j(clone);
    $clone.width($el.width());
    $clone.val($el.val());
    $el.height((clone.scrollTop + clone.scrollHeight + 20) + 'px');
  }

  function delayedResize(e) {
    var t = e.target;
    // doing this to scoop up events fired against the document (e.g. 'macro:applied')
    if (t && t.parentNode == null) { t = null; }
    if (!t) {
      $j('textarea.autoResize').each(function(idx,el){ delayedResize({target: el}); });
      return;
    }

    if (timeouts[t]) { window.clearTimeout(timeouts[t]); }
    timeouts[t] = window.setTimeout(function(){ resize(t); }, 150);
  }

  function initialize() {
    if (initialized) return;
    initialized = true;

    if ($j('style#autoResize-clone-style').length === 0) {
      $j('<style id="autoResize-clone-style">.autoResize-clone {position: absolute; top: -9999px; left: -9999px; z-index: -9999; opacity: 0; overflow-x: hidden; overflow-y: scroll;}</style>').appendTo('body');
    }

    $j('body')
      .delegate('textarea.autoResize', 'change.autoResize',  delayedResize)
      .delegate('textarea.autoResize', 'cut.autoResize',     delayedResize)
      .delegate('textarea.autoResize', 'drop.autoResize',    delayedResize)
      .delegate('textarea.autoResize', 'keydown.autoResize', delayedResize)
      .delegate('textarea.autoResize', 'paste.autoResize',   delayedResize);

    $(document).observe('macro:applied', delayedResize);

    $j('textarea.autoResize').each(function(idx,el){ resize(el); });
  }

  $j(initialize);

}());
