/*globals $, $$, $j, Event, Zendesk, _, Element, Class, Ajax, Hash*/
var InputTracking = {
  trackedElements: [],
  enabled: true,

  trackElement: function(e) {
    if(!InputTracking.trackedElements.include(this)) {
      if (typeof(this.value) === 'undefined') {
        this.originalValue = this.innerHTML;
      } else {
        this.originalValue = this.value;
      }
      InputTracking.trackedElements.push(this);
      Event.stopObserving(this, 'keydown', InputTracking.trackElement);
    }
  },

  disable: function(e) {
    InputTracking.enabled = false;
    return true;
  },

  fixSubmit: function(element, eventName) {
    //I would do it like "Event.observe(form, 'submit', InputTracking.disable)",
    //but that does not work in IE
    var eventHandler = element[eventName];
    if (Object.isString(eventHandler)) {
      element[eventName] = "InputTracking.disable() && " + eventHandler;
    } else if (Object.isFunction(eventHandler)) {
      element.onsubmitWithoutInputTracking = eventHandler;
      element[eventName] = function(e) {
        return InputTracking.disable() && element.onsubmitWithoutInputTracking();
      };
    } else {
      element[eventName] = InputTracking.disable;
    }
  }
};

var CountrySelectField = Class.create({
  initialize: function(selectElement) {
    this.element = selectElement;
    this.container = new Element('div', {className: 'country_select_field'});
    this.closedView = new Element('div', {className: 'closed'});
    this.container.insert(this.closedView);
    this.openedView = new Element('div', {className: 'opened', style: 'display: none;'}).update('<ul/>');
    this.container.insert(this.openedView);
    this.renderOpenedView();
    this.updateOpenedView();
    this.updateClosedView();
    this.element.hide();
    this.element.insert({after: this.container});

    this.closedView.observe('click', this.toggle.bind(this));
  },

  renderOpenedView: function() {
    var list = this.openedView.down('ul');
    list.update();
    var item;
    this.element.select('option').each(function(option) {
      item = new Element('li', {country: option.value});
      this.renderOption(item, option);
      list.insert(item);
      item.observe('click', this.clickOption.bindAsEventListener(this));
    }, this);
  },

  updateOpenedView: function() {
    this.openedView.select('li.selected').each(function(item) {
      item.removeClassName('selected');
    });

    var selectedOption = this.element.down("option[value=" + this.element.getValue() + "]");

    this.openedView.down('li[country=' + selectedOption.value + ']').addClassName('selected');
  },

  updateClosedView: function() {
    var selectedOption = this.element.down("option[value=" + this.element.getValue() + "]");
    this.renderOption(this.closedView, selectedOption);
  },

  renderOption: function(element, option) {
    element.update();
    element.insert(new Element('img', {src: '/images/flags/' + option.value + '.gif'}));
    element.insert(' ' + option.innerHTML);
  },

  open: function() {
    //this.closedView.hide();
    this.openedView.show();
    this.element.focus();
  },

  close: function() {
    this.openedView.hide();
    //this.closedView.show();
  },

  toggle: function() {
    this.openedView.toggle();
    this.element.focus();
  },

  clickOption: function(e) {
    this.element.down("option[value=" + e.findElement('li').getAttribute('country').toLowerCase() + "]").selected = true;
    this.updateOpenedView();
    this.updateClosedView();
    this.close();
  }
});

var DelayedRuleCounts = Class.create(Hash, {
  initialize: function($super, options) {
    $super();
    this.updateUiFromResponse = this.updateUiFromResponse.bind(this);
    this.requestUpdate = this.requestUpdate.bind(this);
    this.ticket_show_empty_views = options.ticket_show_empty_views || false;
    this.delay = 5;
  },

  // public

  // call this when you are showing new rule counters
  newRulesDisplayed: function(){
    if(this.anyCountUnknown()){
      this.update();
    } else {
      this.updateUi();
    }
  },

  // private

  anyCountUnknown: function(){
    var keys = this.keys();
    return _.detect(this.displayedRules(), function(e){ return $j.inArray(e, keys) == -1; });
  },

  requestUpdate: function(ruleIds) {
    new Ajax.Request('/rules/count', {
      method: 'GET',
      parameters: {'rule_ids[]': ruleIds},
      onSuccess: this.updateUiFromResponse
    });
  },

  updateUiFromJson: function(data) {
    var value = this.get(data.ruleId) || {};
    value.value = data.value;
    value.fresh = value.fresh || data.fresh;

    this.set(data.ruleId, value);
    this.updateUiForRule(data.ruleId);
  },

  updateUiFromResponse: function(response) {
    response.responseJSON.each(this.updateUiFromJson, this);

    var allFresh = this.values().all(function(data) {
      return data.fresh;
    });

    if (!allFresh) {
      this.update(this.delay);
      this.delay = this.delay * 1.5;
    }
  },

  // TODO deprecated, replace with update and remove
  updateNow: function() {
    this.update();
  },

  update: function(delay) {
    var displayedRules = this.displayedRules();
    if (displayedRules.length > 0) {
      if (delay) {
        this.requestUpdate.delay(delay, displayedRules);
      } else {
        this.requestUpdate(displayedRules);
      }
    }
  },

  displayedRules: function(){
    return $$('span.r_count[data-fresh=false]').map(function(elm) {return elm.readAttribute('data-rule-id'); }).uniq();
  },

  updateUiForRule: function(ruleId) {
    var data = this.get(ruleId);
    var elements = $j('span.r_count[data-rule-id=' + ruleId + ']'); // selector blows up in prototype's $$ ZD#243349
    var self = this;
    elements.each(function(){
      var elm = $j(this);
      elm.html('(' + data.value + ')');
      elm.data('fresh', data.fresh);

      if (data.fresh) {
        if (data.value === '0') {
          elm.closest('a').addClass('empty');
          if (!self.ticket_show_empty_views) {
            $(elm.closest('li')[0]).slideUp(); // use prototype slideUp effect
          }
        } else {
          elm.closest('a').removeClass('empty');
        }
      }
    });
  },

  updateUi: function() {
    this.keys().each(this.updateUiForRule, this);
  }
});

Event.observe(document, 'dom:loaded', function() {
  $$('textarea').each(function(textarea) {
    Event.observe(textarea, 'keydown', InputTracking.trackElement);
  });

  $$('form').each(function(form) {
    InputTracking.fixSubmit(form, 'onsubmit');
  });

  $$('form input[type=button]').each(function(button) {
    InputTracking.fixSubmit(button, 'onclick');
  });

  $$('select.country_select_field').each(function(selectElement) {
    new CountrySelectField(selectElement);
  });
});

$j(function() {
  // if the browser doesn't support the placeholder attribute, fake it:
  $j('input[placeholder], textarea[placeholder]').placeholder();
});

//Somehow this does not work when done with Event.observe
window.onbeforeunload = function() {
  if (InputTracking.enabled) {
    for (var i = 0; i < InputTracking.trackedElements.length; i++) {
      var new_value;
      if (typeof(InputTracking.trackedElements[i].value) === 'undefined'){
        new_value = InputTracking.trackedElements[i].innerHTML;
      } else {
        new_value = InputTracking.trackedElements[i].value;
      }
      if (new_value !== InputTracking.trackedElements[i].originalValue && InputTracking.trackedElements[i].id !== 'comment_value') {
        return "You have entered some text on this page. If you choose to proceed you will lose your changes.";
      }
    }
  }
};
