/*globals Autocompleter, Event, $j*/
(function() {

  Autocompleter.MultiValue.addMethods({
    beforeFormSubmit: function() {
      this.addNewValueFromSearchField(this.searchField);
    }
  });

  // The blur event that FastAutocompleter binds to the search input is
  // firing on click events on the suggestions, causing that click event
  // to cancel.
  function correctBlurEvent(autocompleter) {
    Event.stopObserving(autocompleter.searchField, 'blur');

    $j(document.body).bind('click', function(event) {
      var $input  = $j(autocompleter.searchField),
          $output = $j(autocompleter.choicesHolder),
          isAutocompleteOpen = $output.is(':visible'),
          isEventFromOutside = $output.find(event.target).length === 0 &&
                               $input.add($output).filter(event.target).length === 0;

      if (isAutocompleteOpen && isEventFromOutside) {
        autocompleter.hide();
      }

      // This is so clicking away from a tag field automatically turns the
      // text into a tag.
      if (isEventFromOutside) {
        autocompleter.addNewValueFromSearchField(autocompleter.searchField);
      }
    });
  }

  var originalInit = Autocompleter.MultiValue.prototype.initialize;

  Autocompleter.MultiValue.prototype.initialize = function() {
    originalInit.apply(this, arguments);
    correctBlurEvent(this);
  };

  Autocompleter.MultiValue.prototype.hide = function() {
    var self = this;
    self.stopIndicator();

    // Something is keeping the choicesHolder container visible through the
    // rest of this event loop. Set a short timeout to have it hidden soon.
    setTimeout(function() {
      $j(self.choicesHolder).hide();
    }, 5);
  };

}());
