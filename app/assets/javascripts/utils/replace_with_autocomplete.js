/*globals jQuery*/

var LARGE_SELECT_LENGTH = 70; // number of <option>s at which it becomes unwieldy

jQuery.fn.extend({
  autocompleteFromSelectIfLarge: function(options) {
    var tooLarge = LARGE_SELECT_LENGTH;
    jQuery(this).each(function(i, select) {
      if (jQuery('option', select).length > tooLarge) {
        jQuery(select).autocompleteFromSelect(options);
      }
    });
  }
});

// Usage: jQuery(selector).autocompleteFromSelect({ options })
// @option tabIndex: the tabindex of the new <input>; defaults to
//         that of the underlying <select>
// @option placeholder: the HTML5 placeholder attribute of the
//         new <input>; defaults to that of the underlying <select>
// @option maxResults: the maximum number of results to show for
//         any query; defaults to 15
// @option combobox: whether to make the autocomplete into a
//         combobox, i.e. to add a down-arrow button; defaults to true
jQuery.widget("ui.autocompleteFromSelect", {
  // defaults:
  options: {
    tabIndex: null,
    placeholder: null,
    maxResults: 15,
    combobox: true
  },
  _create: function() {
    var self = this,
        select = this.element,
        selected = select.children(":selected"),
        selectWasHidden = select.is(':hidden'),
        value = selected.val() ? selected.text() : null,
        tabIndex = this.options.tabIndex || select.attr('tabindex'),
        placeholder = this.options.placeholder || select.attr('placeholder'),
        noResultsTest = select.attr('data-no-matches-message') || 'Sorry, no results found.';

    select.hide();
    select.wrap("<span />");
    if(selectWasHidden) select.parent().hide();

    var input = jQuery("<input type='text' />")
        .addClass("ui-widget ui-widget-content")
        .focus(function() { jQuery(this).select(); })
        .mouseup(function(event) { event.preventDefault(); })
        .autocomplete({
          delay: 0,
          minLength: 0,
          source: function(request, response) {
            var matcher = new RegExp(jQuery.ui.autocomplete.escapeRegex(request.term), "i");
            var results = select.children("option:enabled").map(function() {
              var text = jQuery(this).text(),
                  email = jQuery(this).attr('data-email');
              if (email) {
                text = text + ' ' + email;
              }
              if (this.value && (!request.term || matcher.test(text))) {
                return {
                  label: text.replace(
                    new RegExp(
                      "(?![^&;]+;)(?!<[^<>]*)(" +
                      jQuery.ui.autocomplete.escapeRegex(request.term) +
                      ")(?![^<>]*>)(?![^&;]+;)", "gi"
                   ), "<strong>$1</strong>"),
                  value: text,
                  option: this
                };
              }
            });
            if (results.length === 0) {
              results = [{
                label: noResultsTest,
                value: null,
                option: null,
                disabled: true
              }];
            }
            if (self.options.maxResults && results.length > self.options.maxResults) {
              var extra = results.length - self.options.maxResults;
              var label = ""
              //TODO - Revisit this solution once we have better support for pluralization
              if (extra == 1) {
                label = I18n.t("txt.javascript.utils.replace_with_autocomplete.one");
              } else {
                label = I18n.t("txt.javascript.utils.replace_with_autocomplete.other", {count:extra});
              }
              var refine = {
                label: label,
                value: null,
                option: null,
                disabled: true
              };
              results = results.slice(0, self.options.maxResults);
              results.push(refine);
            }
            response(results);
          },
	  select: function(event, ui) {
	    if (ui.item && !ui.item.disabled) {
	      ui.item.option.selected = true;
	      self._trigger("selected", event, {
		item: ui.item.option
	      });
	      input.data('searching', false);
	    } else {
	      // reset to initial selected value, as 'Try refining your search' isn't a valid option
	      jQuery(this).val(selected.text());
	      select.val(selected.val());
	      return false;
	    }
	  },
	  change: function(event, ui) {
            if (!ui.item) {
              var matcher = new RegExp("^" + jQuery.ui.autocomplete.escapeRegex(jQuery(this).val()) + "$", "i"),
                valid = false;
              select.children("option:enabled").each(function() {
                if (this.value.match(matcher) || jQuery(this).text().match(matcher)) {
                  this.selected = valid = true;
                  return false;
                }
              });
              if (!valid) {
                // reset to initial selected value, as it didn't match anything
                jQuery(this).val(selected.text());
                select.val(selected.val());
                return false;
              }
            }
          },
          close: function(event, ui) {
            input.data('searching', false);
          },
          search: function(event, ui) {
            input.data('searching', true);
          }

          // It would be nice to be able to not render the "no items found"
          // with an <a>, but it doesn't seem possible to override _renderItem.
          // If it were possible, it would look like this:
          // _renderItem: function( ul, item) {
          //   var result = jQuery( "<li></li>" )
          //     .data( "item.autocomplete", item );
          //   if (item.disabled) {
          //     result.append( jQuery( "<a></a>" ).text( item.label ) );
          //   } else {
          //     result.text(item.label);
          //   }
          //   return result.appendTo( ul );
          // }
          // Instead, see the overridden #select that ignores disabled items.
        });


    // jQuery UI 1.8.14 includes a new version of the `_renderItem` method.
    // The new version of the method uses `.text()` which html escapes the
    // label. Here, we're monkeypatching in the old version of `_renderItem`
    // so we can render the search string in bold
    input.data('autocomplete')._renderItem = function(ul, item) {
      return jQuery( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
    };


    if (tabIndex) { input.attr('tabindex', tabIndex); }
    if (placeholder) { input.attr('placeholder', placeholder); }
    if (value) { input.val(value); }

    input.insertBefore(select);

    if (self.options.combobox) {
      var comboButton = jQuery("<input type='button' class='ui-widget combobox-button' value='▼' />")
        .click(function() {
          if (input.autocomplete("widget").is(":visible")) {
            input.autocomplete("close");
          } else {
            // pass empty string as value to search for, displaying all results
            input.autocomplete("search", "");
            input.focus();
          }
        });
      input
        .addClass('combobox')
        .after(comboButton);
    }

    select
      .removeAttr('tabindex')
      .change(function() {
        var selected = jQuery(':selected', this);
        if (selected.length && !input.data('searching')) {
          input
            .val(selected.text())
            .change();
        } else if (input.data('searching')) {
          input.autocomplete("search", input.val());
        }
      });
  }
});
