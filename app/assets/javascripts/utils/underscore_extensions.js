(function(_) {
    var split = function(array, size) {
        return _.reduce(array,
                        function(acc, curr) {
                            var last = _(acc).last();
                            if(last.length >= size) {
                                acc.push([curr]);
                            } else {
                                last.push(curr);
                            }
                            return acc;
                        },
                        [[]]);
    };

    _.mixin({split: split});
}(window._));