/*globals Zendesk, PeriodicWorker, $j*/

Zendesk.NS('Utils');

Zendesk.Utils.Poller = function(ajaxOptions, wait, maxWait, successCallback, errorCallback, timeoutCallback) {
  this.wait = wait;
  this.maxWait = maxWait;
  this.ajaxOptions = {
    url: ajaxOptions.url,
    method: ajaxOptions.method || 'get',
    data: ajaxOptions.data
  };

  this.successCallback = successCallback;
  this.errorCallback = errorCallback;
  this.timeoutCallback = timeoutCallback;
  this.periodicWorker = new PeriodicWorker(wait);
  this.waited = -wait;
};

Zendesk.Utils.Poller.prototype = {
  start: function() {
    this.periodicWorker.jobs = [this._poll.bind(this)];
    this.periodicWorker.start();
  },

  stop: function() {
    this.periodicWorker.stop();
  },

  _poll: function() {
    this.waited = this.waited + this.wait;
    if(this.waited > this.maxWait) {
      this.stop();
      if(this.timeoutCallback) {
        this.timeoutCallback();
      }
      return;
    }

    $j.ajax({
      type: this.ajaxOptions.method,
      url: this.ajaxOptions.url,
      data: this.ajaxOptions.data,
      success: function(response) {
        if (!this.successCallback || !this.successCallback(response)) {
          this.stop();
        }
      }.bind(this),
      error: function(response) {
        if (!this.errorCallback || !this.errorCallback(response)) {
          this.stop();
        }
      }.bind(this)
    });
  }
};
