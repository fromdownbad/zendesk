(function($, exports) {
  if (!exports.Zendesk) { exports.Zendesk = {}; }
  var Zendesk = exports.Zendesk;
  if (!!Zendesk.NS) { return; }

  var slice = Array.prototype.slice;

  function applyFunctionIfApplicable(namespace, additionalArgs) {
    if (additionalArgs.length > 0) {
      // pull off the function from the end:
      var fn = additionalArgs.pop();

      if ($.isFunction(fn)) {
        fn.apply(namespace, additionalArgs);
      } else if (!!fn) {
        throw new Error(fn + ' is not a function');
      }
    }
  }

  Zendesk.NS = function(namespaceString) {
    var nesting   = namespaceString.split("."),
        currentNS = Zendesk,
        i;

    for(i = 0; i < nesting.length; i++) {
      var namespace = nesting[i];
      if(typeof(currentNS[namespace]) === 'undefined') {
        currentNS[namespace] = {};
      }
      currentNS = currentNS[namespace];
    }

    applyFunctionIfApplicable(currentNS, slice.call(arguments, 1, arguments.length));

    return currentNS;
  };

  Zendesk.NS.extend = function(namespaceString, addIn) {
    $.extend(Zendesk.NS(namespaceString), addIn);
  };

}(this.jQuery, this));
