Zendesk.NS.extend('Text', {
  autoLink: function(string) {
    return string.replace(/\b(https?:\/\/[^\s"]+)\b/g, '<a href="$1" target="_blank">$1</a>');
  }
});
