/*global Zendesk, _, $j, Base64*/
// context has a currentUser and currentAccount object
(function($, context) {

  // jQuery 1.6+ uses .prop instead of .attr for DOM properties
  function isForm($node) {
    return $node &&
           $node.length &&
           $node.prop &&
           /^form$/i.test($node.prop('tagName'));
  }

  function isTracked($node) { return !!$node.data('zd-tracking-enabled'); }
  function markAsTracked($node) { $node.data('zd-tracking-enabled', true); }

  $.fn.extend({

    // Adds ToTango tracking to the current element.
    // If the element is a form, adds an onSubmit handler;
    // Otherwise, adds an onClick handler.
    instrumentTracking: function() {
      return $(this).each(function(i, node) {
        node = $(node);
        if (isTracked(node)) return;
        if (isForm(node)) {
          node.submit(function (e) {
            var form = $(this);
            form.trackEvent();
            e.preventDefault();
            window.setTimeout(function () {
              if (!e.isPropagationStopped()) {
                // call DOM element's submit method to skip jQuery submit bindings
                form[0].submit();
              }
            }, 500);
          });
        } else {
          node.click(function() { $(this).trackEvent(); });
        }
        markAsTracked(node);
      });
    },

    // Send a tracking event to either the Zendesk metrics collector
    // or to ToTango.
    trackEvent: function(activity, module) {
      $(this).trackZendeskEvent(activity, module);
    },

    // Send a tracking event to Zendesk's metrics collector if applicable.
    // @param [String] activity; optional, defaults to the closest data-metric-activity attribute
    // @param [String] module; optional, defaults to the closest data-metric-module attribute
    trackZendeskEvent: function(activity, module) {
      activity = activity || $(this).data('metric-activity');
      module   = module   || $(this).data('metric-module');
      if (activity && module) {
        Zendesk.Instrumentation.track(activity, module);
      }
      return $(this);
    }
  });

  // ToTango tracking:

  Zendesk.NS.extend('Instrumentation', {

    BROWSER_MAP: {
      Chrome:  navigator.userAgent.match(/Chrome/),
      Safari:  (navigator.vendor || '').match(/Apple/),
      Firefox: navigator.userAgent.match(/Firefox/),
      IE:      navigator.userAgent.match(/MSIE/)
    },

    currentBrowser: function() {
      return _(Zendesk.Instrumentation.BROWSER_MAP).find(function(browser, is) {
        return is ? browser : undefined;
      }).first();
    },

    // Track an event with Zendesk's metric library.
    track: function(activity, module) {
      return;
      // var url = '/stats/';
      // if (context.currentAccount && context.currentAccount.id) {
      //   url += context.currentAccount.id + '/';
      // }
      // url += module + ':' + activity + '.gif';
      // var img = new Image();
      // img.src = url;
    },

    // instruments method invocation
    on: function(obj, selector, activitySpec, module) {
      if (obj[selector].isTracking) return;
      var tracker = function(/*func, args*/) {
        var allArgs = _.toArray(arguments),
            funcArgs = allArgs.slice(1),
            func = allArgs[0],
            funcResult = func.apply(this, funcArgs);
        if (activitySpec && module) {
          var activity = _.isFunction(activitySpec) ?
            activitySpec.apply(this, funcArgs) : activitySpec;
          if (activity) {
            Zendesk.Instrumentation.track(activity, module);
          }
        }
        return funcResult;
      };
      obj[selector] = _.wrap(obj[selector], tracker);
      obj[selector].isTracking = true;
    }
  });

  //after the DOM has loaded...
  $(function() {
    // set up tracking on all tracked elements:
    $('.tracked').instrumentTracking();
  });

}(this.jQuery, this));
