/*globals Zendesk, _*/
/*
<p class="player">
  <span class="play" />
  <span class="seekbar">
    <span class="loading" />
    <span class="handle" class="ui-slider-handle" />
  </span>
  <span class="time_remaining" />
  <div class='clearfix shim'>&nbsp;</div>
</p>

*/

(function($) {

  // Format `ms` miliseconds as mm:ss
  function formatTimeRemaining(ms) {
    var seconds = Math.ceil(ms / 1000);
    var minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    if (seconds < 10) { seconds = '0' + seconds; }
    if (minutes < 10) { minutes = '0' + minutes; }
    return minutes + ':' + seconds;
  }

  // A map of pixel offsets within the volume control to volume settings.
  var volumeControls = _([
    // [maxX, volume]
       [3,    0],
       [7,    20],
       [12,   40],
       [17,   60],
       [22,   80],
       [27,   100]
  ]);

  // Calculate the volume selected by `event`, triggered on a volume control
  // element `target`. Returns `null` if the event doesn't correspond to
  // a volume selector.
  function calculateNewVolume(event) {
    // starting at the event's x-coordinate, subtract off the x-offset
    // of every element in the "offsetParent" chain up to <body>:
    var xOffset = event.pageX;
    for(var cur = event.target; cur; cur = cur.offsetParent) {
      xOffset -= cur.offsetLeft;
    }
    // map that to a volume in 0-100:
    return (volumeControls.find(function(control) {
      return xOffset <= control[0];
    }) || [])[1];
  }

  Zendesk.AudioPlayer = function(sound, playerElement) {
    this.sound = sound;
    this.playerElement = playerElement;
    this.seekbar = this.playerElement.find(".seekbar");
    this.timeRemaining = this.playerElement.find('.time_remaining');
    this.playButton = this.playerElement.find(".play");
    this.volumeButton = this.playerElement.find('.volume');
    this.seeking = false;
  };

  Zendesk.AudioPlayer.prototype = {
    init: function() {
      this.soundObj = this.sound.sound;
      this.soundObj._player = this;

      var self = this;

      if(this.sound.playState() !== 0) {
        this.playButton.addClass("playing");
      }

      this.playButton.click(function() {
        self.playButton.toggleClass("playing");
        if(self.playButton.attr("data-state") === "paused") {
          self.playButton.attr("data-state", "playing");
          self.play();
        } else {
          self.playButton.attr("data-state", "paused");
          self.pause();
        }
      });

      this.volumeButton.click(function(e) {
        self.updateVolume(calculateNewVolume(e));
      });

      this.seekbar.slider({
        value: 0,
        step: 50,
        max: this.sound.duration(),
        slide: function(e, ui) {
          // called during a slider drag
          self.seeking = true;
        },
        change: function(e, ui) {
          // called when a slider drag finishes or when setting the value
          // programatically
          if(e.originalEvent) {
            // i.e., if this was a DOM event (click/keyboard)
            self.sound.seekTo(ui.value);
            self.updateTimeRemaining();
          }
          self.seeking = false;
        }
      });
      this.updateTimeRemaining();
      return this;
    },

    play: function() {
      this.sound.play({
        whileplaying: this.timeUpdate,
        onfinish: this.finishedPlaying
      });
    },

    pause: function() {
      this.sound.pause();
    },

    // Called every so often during playback in the context of the
    // underlying SoundManager2 object. (`this._player` is a reference
    // to the `Zendesk.AudioPlayer` object.)
    timeUpdate: function() {
      var self = this._player;
      if(self.seeking) { return; }
      self.seekbar.slider("value", self.sound.position());
      self.updateTimeRemaining();
    },

    // Called at the end of playback in the context of the underlying
    // SoundManager2 object. (`this._player` is a reference
    // to the `Zendesk.AudioPlayer` object.)
    finishedPlaying: function() {
      var self = this._player;
      self.seekbar.slider("value", 0);
      self.playButton.attr("data-state", "paused");
      self.playButton.removeClass("playing");
      self.sound.seekTo(0);
      self.pause();
      self.updateTimeRemaining();
    },

    updateTimeRemaining: function() {
      this.timeRemaining
        .html(formatTimeRemaining(this.sound.duration() - this.sound.position()));
    },

    // Update the volume of the sound and on the volume control.
    updateVolume: function(volume) {
      if (volume !== null) {
        this.sound.volume(volume);
        this.volumeButton
          .removeClass('v0 v20 v40 v60 v80 v100')
          .addClass('v' + volume);
      }
    }

  };
}(this.jQuery));
