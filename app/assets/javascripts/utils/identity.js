/*globals Zendesk, _, $j*/

function identityTypeListElementId(identityType) {
  return '#' + identityType + '_identity_list';
}

// Zendesk.Identity
//
// A user identity model containing attributes required
// for the UI and for ajax.
//
// CONFIGURABLES:
// singleIdentityLimit:  list of identity types that can only have a single identity
//         unverifieds:  list of identity types that require verification
//           primaries:  list of identity types that can be used as primary identities
Zendesk.Identity = function(userId, identity) {

  this.singleIdentityLimit = ['facebook'];

  this.unverifieds         = ['email', 'unverified_email', 'google'];

  this.primaries           = ['email', 'google'];

  this.externalIdentities  = ['facebook', 'twitter'];

  this.identityType        = identity.identity_type;

  this.id                  = identity.id;

  this.isVerified          = identity.is_verified;

  this.isPrimary           = false;

  this.isExternalIdentity  = _(this.externalIdentities).include(this.identityType);

  this.listElmId           = identityTypeListElementId(this.identityType);

  this.elmId               = (identity.identity_type + '_identity_' + identity.id);

  this.removeUrl           = '/users/' + userId +
                             (identity.identity_type === 'unverified_email' ? '/unverified_email_addresses/' : '/user_identities/') +
                             identity.id + '.json';

  if (_(this.primaries).include(identity.identity_type)) {
    this.primaryUrl        = '/users/' + userId +
                             '/user_identities/' + identity.id + '/make_primary.json';
  }


  switch(identity.identity_type)
  {
    case 'twitter':
      this.name      = "@" + identity.screen_name;
      break;
    case 'facebook':
      // temporory name value. Awaiting the actual facebook name to be available in JS
      this.name      = identity.name;
      break;
    // Types: email, google, unverified_email
    default:
      this.isPrimary = (identity.priority == 1);
      this.name      = identity.value;
  }
};

Zendesk.Identity.prototype.isUnverified = function() {
  return ( _(this.unverifieds).include(this.identityType) && !this.isVerified );
};

Zendesk.Identity.prototype.isPrimaryCandidate = function() {
  return ( _(this.primaries).include(this.identityType) && this.isVerified );
};



// Zendesk.IdentitiesManager
//
// Manages a list of Zendesk.Identity objects for the UI.
// Primary functionality includes removing a user identity
// and making a user identity primary.
Zendesk.IdentitiesManager = function(userId, onUpdateCallback) {

  var parseIdentityData = function(userId) {
    var identityData = $j.parseJSON($j('#all_user_identities').html()) || [];
    var unverifiedEmailAddressesData = $j.parseJSON($j('#unverified_email_addresses').html()) || [];

    return $j.merge(identityData, unverifiedEmailAddressesData).map(function(data) {
      return new Zendesk.Identity(userId, data);
    });
  };

  this.registerOnUpdate(onUpdateCallback);

  this.identities = parseIdentityData(userId);
};

Zendesk.IdentitiesManager.prototype.find = function(elmId) {
  return _(this.identities).find(function(identity) {
    return identity.elmId == elmId;
  });
};

Zendesk.IdentitiesManager.prototype.remove = function(identity, callbacks) {

  if (typeof identity == 'undefined') {
    return $j.deferred().reject('No identity provided to be removed');
  }

  return $j.ajax({
           url: identity.removeUrl,
           type: 'DELETE',
           dataType : 'html'
         })
         .done($j.proxy(function() {
           this.identities = _(this.identities).without(identity).compact();
           this.onUpdate();
         }, this));

};

Zendesk.IdentitiesManager.prototype.registerOnUpdate = function(callback) {
  if (typeof callback === 'function') {
    this.onUpdate = callback;
  }
};

Zendesk.IdentitiesManager.prototype.makePrimary = function(identity) {

  if (typeof identity.primaryUrl == 'undefined') {
    return $j.deferred().reject('The identity type ' + identity.identityType + ' cannot be primary');
  }

  return $j.ajax({
           url:  identity.primaryUrl,
           type: 'POST'
         })
         .done($j.proxy(function() {
           var oldPrimary = this.primary();
           if (oldPrimary) oldPrimary.isPrimary = false;
           identity.isPrimary = true;
         }, this));

};

Zendesk.IdentitiesManager.prototype.empty = function() {
  return this.identities.length === 0;
};

Zendesk.IdentitiesManager.prototype.limitToSingleIdentity = function(identity) {
  return _(this.singleIdentityLimit).include(identity.identityType);
};

Zendesk.IdentitiesManager.prototype.allPrimaryCandidates = function() {
  return _(this.identities).select(function(identity) {
    return identity.isPrimaryCandidate();
  });
};

Zendesk.IdentitiesManager.prototype.primary = function() {
  return _(this.allPrimaryCandidates()).detect(function(identity) {
    return identity.isPrimary;
  });
};
