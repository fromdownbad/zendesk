/*globals window*/

(function($, Zendesk) {

  function replaceBodyWithIFrame(href) {
    $('#contentcolumn')
      .html('<iframe src="' + href + '" width="100%" height="500px"></iframe>');
  }

  function createLink(args) {
    var link = $('<a />')
      .html(args.text)
      .attr('href', args.href);
    if (args.target) {
      link.attr('target', args.target);
    }
    if (args.iframe) {
      link.click(function() {
        replaceBodyWithIFrame(args.href);
        return false;
      });
    }
    return link;
  }

  function createListItem(args) {
    args = args || {};
    if (!args.text) {
      throw("Property 'text' is required for creating a navigation element");
    }
    var result = $('<li />');
    if (args.href) {
      result.html(createLink(args));
    } else {
      result.html($('<span />').html(args.text));
    }
    return result;
  }

  function Section(args) {
    this.element = createListItem(args);
    this.element.addClass('main clazz');
    this.element.find('a,span').addClass('tab');
    this.element.append('<ul class="menu-drop" />');
  }

  Section.prototype = {
    // Adds an element to a section.
    // @api public
    // See Zendesk.TopNav.addSection for options
    addElement: function(args) {
      this.element.find('ul').append(createListItem(args));
      return this;
    }
  };

  // This is mostly meant to be used in custom Javascript
  // widgets to manipulate the #top_nav menu.
  // @api public
  // @example
  //   Zendesk.TopNav
  //     .addSection({ text: 'Other' })
  //       .addElement({ text: 'Finance', href: 'http://example.org/finance', target: '_blank' })
  //       .addElement({ text: 'Ordering', href: 'http://example.org/ordering', iframe: true });
  Zendesk.NS.extend('TopNav', {
    Section: Section,
    // @parameter [String] text: the section's text; required
    // @parameter [String] href: the link href; optional
    // @parameter [String target: the link target; optional
    // @parameter [true,false] iframe: whether the link should be opened in
    //                                 an iframe on this page rather than
    //                                 replacing this window or opening in a
    //                                 new one
    addSection: function(args) {
      var section = new Section(args);
      section.element.appendTo('#top-menu ul.agent-tabs');
      return section;
    }
  });

}(window.jQuery, window.Zendesk));
