/*globals Zendesk, _*/
(function($) {
  Zendesk.Pager = function(container, prevLink, nextLink) {
    this.container = container;
    this.nextLink = nextLink;
    this.prevLink = prevLink;
    this.pages = $(container).find(".page");
    this.currentPage = 0;
    this.prevLink.css('visibility', 'hidden');
    if(this.pages.length < 2) { this.nextLink.css('visibility', 'hidden'); }
  };

  Zendesk.Pager.prototype = {
    paginate: function(callback) {
      this.showPage(this.currentPage);
      var self = this;
      $(this.prevLink).click(function(e) {
        if(self.currentPage > 0) {
          self.nextLink.css('visibility', 'visible');
          self.currentPage = self.currentPage - 1;
          self.showPage(self.currentPage);
          if(self.currentPage === 0) {
            self.prevLink.css('visibility', 'hidden');
          }
        }
        if(e) { e.preventDefault(); }
      });

      $(this.nextLink).click(function(e) {
        if(self.currentPage < self.pages.length - 1) {
          self.prevLink.css('visibility', 'visible');
          self.currentPage = self.currentPage + 1;
          self.showPage(self.currentPage);
          if(self.currentPage === self.pages.length - 1) {
            self.nextLink.css('visibility', 'hidden');
          }
        }
        if(e) { e.preventDefault(); }
      });

      if(_.isFunction(callback)) {
        callback();
      }
    },

    showPage: function(page) {
      _(this.pages).each(function(elem, pageNum) {
        if(pageNum === page) {
          $(elem).show();
        } else {
          $(elem).hide();
        }
      });
    }
  };
}(this.jQuery));
