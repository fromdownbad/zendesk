/*globals Zendesk, _, $j*/

Zendesk.JobStatus = function(statusUrl, options) {
  this.statusUrl = statusUrl;

                  // /job_statuses/[ the ID ].json?foo=bar, where
                  // .json and ?foo=bar are optional, but must
                  // come at the end if present.
  var idFromURL = /\/job_statuses\/([^\.\?]+)/;
  this.id = (statusUrl.match(idFromURL) || [])[1];
  if (this.id == null && console && console.warn) { console.warn("Could not parse Job ID from " + statusUrl); }

  this.options = _.extend({
    title: '',
    interval: 1000
  }, options);

  _.bindAll(this);

  this.createFlashNotice();
  this.updateProgress();
};

Zendesk.JobStatus.prototype = {
  createFlashNotice: function() {
    if (!this.flash) {
      this.flash = $j('<div id="notice" class="processing"><span class="progress"/> <span>' + this.options.title + ' </span></div>');
      $j('#flash_messages div').hide();
      $j('#flash_messages').append($j('<div id="flash" class="background"></div>').append(this.flash));
      $j(document).scrollTop($j('#flash_messages').position().top);
    }
    return this.flash;
  },

  updateProgress: function() {
    var self = this;
    $j.getJSON(this.statusUrl, function(jobStatus) {
      jobStatus = jobStatus['job_status'];
      jobStatus.job = self;
      if (jobStatus.status !== 'completed') {
        if (jobStatus.total) {
          self.flash.find('.progress').html('' + jobStatus.progress + ' / ' + jobStatus.total);
        }
        setTimeout(self.updateProgress, self.options.interval);
      } else {
        var result = self.options.renderResult(jobStatus);
        self.flash.find('.progress').html(result.title);
        self.flash.append(result.body);
        self.flash.removeClass('processing');
        self.flash.addClass('done_processing');
      }
    });
  }
};
