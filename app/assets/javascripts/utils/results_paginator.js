/*global JSON*/

(function(Zendesk, $, _, console) {

  var SEARCH_RESULTS_DATA_SELECTOR = 'script.search_results_data',
      warn = function(msg) {};

  if (console && console.warn) { warn = _.bind(console.warn, console); }

  function parseSearchResultsDataFrom($content) {
    var $script = $content.find(SEARCH_RESULTS_DATA_SELECTOR).last();
    if (!$script.exists()) {
      warn('Paginator received search results with no script.search_results_data element');
      return {};
    }
    var data = {};
    try {
      data = JSON.parse($script.html());
    } catch(e) {
      // leave blank
    }
    return data;
  }

  // An object that manages the pagination UI for one paginated section
  // on a page.
  //
  // ### Parameters
  //
  //  * containerSelector -- the name of the container into which new results
  //                         will be appended; required
  var ResultsPaginator = function(containerSelector) {
    if (!containerSelector) { throw "Cannot paginate results for empty containerSelector"; }
    this.$container = $(containerSelector);
    _.bindAll(this, 'onMoreClicked', 'onMoreRetrieved');
    this.resetPage().bindMoreButton();
  };

  ResultsPaginator.parseNextPageFrom = function($content) {
    return parseSearchResultsDataFrom($content).nextPage;
  };

  ResultsPaginator.prototype = {

    resetPage: function() {
      this.nextPage = 2;
      return this;
    },

    onMoreClicked: function() {
      this.loadMore();
      return false;
    },

    loadMore: function() {
      var moreURL = this.moreButton().attr('href');
      this.spinner().show();
      $.ajax(moreURL, {page: this.nextPage}).done(this.onMoreRetrieved);
    },

    onMoreRetrieved: function(results) {
      this.spinner().hide();
      this.removeCurrentMoreButton()
          .appendNewResults(results)
          .applySearchResultData(results);
    },

    spinner: function() {
      return this.$container.find('.progress');
    },

    removeCurrentMoreButton: function() {
      this.$container.find('.show_more_bar').remove();
      return this;
    },

    moreButton: function() {
      return this.$container.find('.show_more_bar a');
    },

    bindMoreButton: function() {
      // Set up this binding once as a delegate so that if something else
      // messes with the internals and puts in a new more button, it's still
      // bound.
      if (!this.$container.data('moreIsBound')) {
        this.$container.data('moreIsBound', true);
        this.$container.delegate('.show_more_bar a', 'click', this.onMoreClicked);
      }
      return this;
    },

    appendNewResults: function(content) {
      $(".item.nobottom").removeClass("nobottom");
      if (this.onContentRetrievedCallback) {
        this.onContentRetrievedCallback(this, content);
      } else {
        this.$container.append(content);
      }
      // FIXME this is needed for every user of the paginator,
      // move that into onContentRetrievedCallback
      $(".forum_tabs").trigger("events-loaded.entries.zendesk");
      return this;
    },

    applySearchResultData: function() {
      this.nextPage = ResultsPaginator.parseNextPageFrom(this.$container);
      this.$container.find(SEARCH_RESULTS_DATA_SELECTOR).remove();
      if (!this.nextPage) {
        this.removeCurrentMoreButton();
      }
      return this;
    }

  };

  // ## $.fn.paginateResults
  //
  // A jQuery utility function that initializes a `Zendesk.PaginatedResults`
  // instance for the given container if none already exists. Exposes
  // the PaginatedResults instance under the `resultsPaginator` data key.
  //
  // ### Parameters
  //
  //  * retrievedEventName -- as for Zendesk.PaginatedResults; required.
  $.fn.paginateResults = function(retrievedEventName) {
    var $this = $(this);
    if ( $this.data('resultsPaginator') != null ) { return; }
    $this.data('resultsPaginator', new ResultsPaginator(this, retrievedEventName));
    return $this;
  };

  // Automatically set up pagination for elements with the
  // paginates_with_more class:
  $(function() { $('.paginates_with_more').paginateResults(); });

  // Export as Zendesk.ResultsPaginator:
  Zendesk.ResultsPaginator = ResultsPaginator;

}(this.Zendesk, this.jQuery, this._, this.console));
