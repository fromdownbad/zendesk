/*globals window, jQuery, _*/

jQuery(function($) {

  var assignableAgents = $('#ticket_assignee_id');
  var bulk_update      = assignableAgents.data('bulk-update');
  var ticketGroup = $('#ticket_group_id');

  if (!assignableAgents.exists() || !assignableAgents.data('do-dynamic-filtering')) {
    // support the old version if assignable-agent filtering is OFF
    ticketGroup.change(function() {
      window.assigneeSelect($(this).val());
    });
    return;
  }

  // A dictionary of { group ID : list of agents }, cached for the lifetime
  // of the page.
  //var cachedLookups = {'-1': {id: -1, name: 'foobar'}};
  var cachedLookups = {};

  // Fetch a list of agents for the group, using the above cache if
  // possible. Return a deferred that resolves with the list of agents.
  function fetchAgentsForGroup(groupID) {
    if (cachedLookups[groupID]) {
      return new $.Deferred().resolve(cachedLookups[groupID]);
    }
    else {
      return $.getJSON(assignableAgents.data('refresh-url'), {
        group_id: groupID
      }).done(function(agents) { cachedLookups[groupID] = agents; });
    }
  }

  // Clear the <select> and add the agents as <option>s.
  function updateAssignableAgentList(agents) {
    assignableAgents.attr("disabled", false);
    var originalValue = assignableAgents.val();

    // Remove the old <option>s and add a default one:
    if (bulk_update) {
      assignableAgents.html('<option value="-1">'+I18n.t('control.dropdown.value.no_change')+'</option><option value="">-</option>');
    }
    else {
      assignableAgents.html('<option></option>');
    }
    _(agents).each(function(agent) {
      assignableAgents.append("<option value='" + agent.id + "'>" + agent.name + "</option>");
    });
    // set the value back to the original if it's still there:
    assignableAgents.val(originalValue).change();
  }

  ticketGroup
    .change(function() {
      assignableAgents.attr("disabled", true); // let users not select from the outdated / soon to be replaced select
      fetchAgentsForGroup(ticketGroup.val())
        .done(updateAssignableAgentList);
    })
    .change();

  // Set the group (and optionally the user) on the ticket form.
  window.assigneeSelect = function(groupID, userID) {
      groupID = groupID || ticketGroup.val();
      return fetchAgentsForGroup(groupID)
               .done(updateAssignableAgentList)
               .done(function() {
                 userID !== undefined && userID !== assignableAgents.val() && assignableAgents.val(userID).change();
                 ticketGroup.add(assignableAgents).blur();
               });
  };

  // Set it to the current value if that exists:
  window.assigneeSelect(null, assignableAgents.data('value'));

});
