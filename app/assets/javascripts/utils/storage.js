/*globals Zendesk*/
(function(Zendesk) {
  Zendesk.Storage = {
    isSupported: function() {
      try {
        return 'localStorage' in window && window.localStorage !== null;
      } catch (e) {
        return false;
      }
    },

    handle: function() {
      return window.localStorage;
    }
  };
}(Zendesk));