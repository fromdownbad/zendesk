Zendesk.NS.extend('Channels', {
  setupBrandsDropdown: function(dropdown_selector){
    // if in classic, the require will throw
    try {
      var defaultBrandImg = window.top.require('lib/image_assets').brandFallbackIconUrl;
      var idToBrandImg = {};
      $j(dropdown_selector + " option").each(function(ind, elem){
        idToBrandImg[elem.value] = $j(elem).data('brand-img') || defaultBrandImg;
      });

      var format = function(state){
        // escape brand name
        var brandName = $j('<div/>').text(state.text).html();
        return '<img src="' + idToBrandImg[state.id] + '" class="brandIcon"/>' + brandName;
      };

      $j(dropdown_selector).select2({
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function(m) { return m; }
      });
    } catch(e) { // classic, do not show the brand select
      $j(dropdown_selector).parents('.form_element').hide();
    }
  }
});
