(function($) {

  //on-change handler for selectInteraction plugin
  function selectInteractions() {

    var selected    = $(this).find('[value=' + $(this).val() + ']');
    var interaction = selected.data('select-interaction') || $(this).data('default-interaction');

    if (interaction instanceof Function) {
      interaction = [interaction];
    }

    if (interaction instanceof Array) {

      $(interaction).each( $j.proxy(function(i, item) {
        if (item instanceof Function) { item.call(this); }
      }, this));

    }

  }


  $.fn.extend({

    // selectInteraction jQuery plugins
    //
    // Registers callbacks with HTML <option> values of a <select>
    // which are called when that <option> is selected.
    //
    // Takes an object:
    // key:         value of <option>
    // value:       callback or [callback, ...]
    //
    // $j('#my_select).selectInteraction({value: callback, ..., 'default-interaction': callback})
    //
    // Any callback assigned the key 'default-interaction' will be called
    // for any <option> that does not have an assigned callback.
    selectInteraction: function(values) {
      if ($(this).prop('nodeName').toLowerCase() !== 'select' || typeof values === 'undefined') {
        return $(this);
      }

      if (values['default-interaction']) {
        $(this).data('default-interaction', values['default-interaction']);
      }

      $(this).change(selectInteractions);

      return $(this)
        .find('option')
        .each(function(i, node) {

          node = $(node)
          var optionVal = node.val();

          if (values[optionVal]) {
            node.data('select-interaction', values[optionVal]);
          }

        })
        .change();
    }

  });

}(window.jQuery));