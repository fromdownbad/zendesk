/*globals window*/

(function($) {

  // on-change handler for checkable elements
  function checkableTogglerCallback() {
    var target;
    if ($(this).attr('data-toggles')) {
      target = $($(this).attr('data-toggles'));
      if ($(this).is(':checked')) {
        target.slideDown();
      } else {
        target.slideUp();
      }
    }
    if ($(this).attr('data-toggles-inverse')) {
      target = $($(this).attr('data-toggles-inverse'));
      if ($(this).is(':checked')) {
        target.slideUp();
      } else {
        target.slideDown();
      }
    }
    if ($(this).is(':checked') && $(this).attr('data-toggles-show')) {
      target = $($(this).attr('data-toggles-show'));
      target.slideDown();
    }
    if ($(this).is(':checked') && $(this).attr('data-toggles-hide')) {
      target = $($(this).attr('data-toggles-hide'));
      target.slideUp();
    }
  }

  // on-click handler for non-checkable elements:
  function nonCheckableTogglerCallback() {
    $($(this).attr('data-toggles')).toggle();
  }

  $.fn.extend({

    isToggler: function() {
      return $(this)
        .filter('[data-toggles],[data-toggles-show],[data-toggles-hide],[data-toggles-inverse]')
        .each(function(i, node) {
          node = $(node);
          var type = node.attr('type');
          if (type === 'checkbox' || type === 'radio') {
            node.change(checkableTogglerCallback);
          } else {
            node.click(nonCheckableTogglerCallback);
          }
        });
    }

  });

  var toggleElements = [
    '#contentwrapper [data-toggles]',
    '#contentwrapper [data-toggles-show]',
    '#contentwrapper [data-toggles-hide]',
    '#contentwrapper [data-toggles-inverse]'
  ].join(',');

  $(function() {
    $(toggleElements)
      .isToggler()
      .change();
  });
}(window.jQuery));
