/*globals Zendesk*/
(function($) {
  var currentValue = $.cookie('_zendesk_settings');
  $.cookie('_zendesk_settings', null, {path: "current"});
  if (!$.cookie('_zendesk_settings')) {
    $.cookie('_zendesk_settings', currentValue, { path: "/", expires: 365 });
  }

  Zendesk.SettingsCookie = {
    get: function(key) {
      return Zendesk.SettingsCookie._data()[key];
    },

    set: function(key, value) {
      var data = Zendesk.SettingsCookie._data();
      data[key] = value;
      Zendesk.SettingsCookie._save();
      return value;
    },

    erase: function(key) {
      var data = Zendesk.SettingsCookie._data();
      delete data[key];
      Zendesk.SettingsCookie._save();
    },

    _data: function() {
      Zendesk.SettingsCookie._data_object = Zendesk.SettingsCookie._data_object || JSON.parse($.cookie('_zendesk_settings') || '{}');
      return Zendesk.SettingsCookie._data_object;
    },

    _save: function() {
      var data = Zendesk.SettingsCookie._data();
      $.cookie('_zendesk_settings', JSON.stringify(data), { path: '/', expires: 365 });
    }
  };

  window.Cookie = Zendesk.SettingsCookie;
}(this.jQuery));
