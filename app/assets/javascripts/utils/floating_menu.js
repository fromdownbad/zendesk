/*globals document, $j*/

(function($) {

  $.widget('ui.floating_menu', {

    options: {
      menuText: I18n.t('txt.users.show.actions')
    },

    _create: function() {
      var action;

      $.extend(this.options, {
        menuText: $(this.element).attr('data-menu-text')
      });

      $(this.element).wrap( $('<div>').addClass('floating_menu_ui inactive') );

      action = $('<span>', { html: this.options.menuText }).addClass('floating_menu_action');

      action.insertBefore(this.element);

      this.menu = this.element.parent('.floating_menu_ui');

      action.bind('mouseover', $.proxy(this.show, this));
      this.menu.bind('mouseleave', $.proxy(this.hide, this));
    },

    show: function(e) {
      this.menu.removeClass('inactive');
    },

    hide: function(e) {
      this.menu.addClass('inactive');
    }

  });

}(this.jQuery));


$j( function() { $j('.floating_menu').floating_menu(); } );