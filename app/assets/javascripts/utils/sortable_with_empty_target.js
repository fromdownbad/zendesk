(function($) {

  var defaultOptions = {
    // the text for the empty target item
    emptyTargetText: "Drop items here",

    // classes for the empty target element (in addition to "emptyTarget",
    // which is always added)
    emptyTargetClass: null,

    items: '> :not(.emptyTarget)'
  };

  // Hides the empty target element in this sortable container. To be called
  // in the context of a sortable container when something is dragged into
  // it.
  function onDragInto(evt, ui) {
    $('li.emptyTarget', this).hide();
  }

  // Shows the empty target element in this sortable container. To be called
  // in the context of a sortable container when something is dragged into a
  // connected sortable container.
  function onDragIntoOther(evt, ui) {
    var otherLIs = $('li:not(.emptyTarget)', this);
    if (ui && ui.item) { otherLIs = otherLIs.not(ui.item); }
    if (otherLIs.length === 0) { $('li.emptyTarget', this).show(); }
  }

  // Acts just like $.ui.sortable, but adds a target <li> to the list
  // when there are no other elements.
  // To be called on a <ul> or <ol>. Passes all options
  // except emptyTargetText and emptyTargetClass to $.ui.sortable.
  $.fn.sortableWithEmptyTarget = function(options) {
    options = $.extend({}, defaultOptions, options || {});
    var emptyTargetText  = options.emptyTargetText,
        emptyTargetClass = ['emptyTarget', options.emptyTargetClass].join(' ');
    delete options.emptyTargetText;
    delete options.emptyTargetClass;

    $(this).each(function() {
      var $this = $(this);

      // append an empty target option to the list:
      $('<li class="' + emptyTargetClass + '">' + emptyTargetText + '</li>')
        .toggle($this.find('li').length === 0)
        .appendTo($this);

      $this
        .sortable(options)
        .bind('sortover', $.proxy(onDragInto, $this))
        .sortable('option', 'connectWith') // get connected containers
          .bind('sortover', $.proxy(onDragIntoOther, $this));
    });
  };

}(this.jQuery));
