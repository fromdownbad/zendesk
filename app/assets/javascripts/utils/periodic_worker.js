/*globals window*/
(function($, setInterval, clearTimeout, exports) {

  // Runs every X milliseconds
  function PeriodicWorker(duration) {
    this.duration = duration;
    this.jobs     = [];
  }

  PeriodicWorker.prototype = {
    id:        null,
    duration:  null,
    jobs:      null,

    // Starts the worker if it is stopped.
    // Calling start() on a running worker does nothing.
    start: function() {
      if (!this.id) {
        this._work();
        this.id = setInterval(this._work.bind(this), this.duration);
      }
    },

    // Stops the worker if it is running.
    // Calling stop() on a running worker does nothing.
    stop: function() {
      if (this.id) {
        clearTimeout(this.id);
        this.id = null;
      }
    },

    _work: function() {
      $.each(this.jobs, function(i, job) {
        job();
      });
    }

  };

  exports.PeriodicWorker = PeriodicWorker;

}(window.jQuery, window.setInterval, window.clearTimeout, window));
