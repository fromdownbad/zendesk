// Only very small extensions here, please. Larger ones should go in their
// own files and be referenced here.
//
// ### Others:
//  * replace_with_autocomplete.js
(function($) {
  $.fn.extend({
    // ## `jQuery.fn.exists`
    // Returns `true` iff at least one element matching the selector
    // is on the page.
    exists: function() {
      return this.length > 0;
    }
  });

  // show loading animation and errors inside the container that is being replaced
  $.fn.responsiveLoad = function(url, callback){
    var loading = '<center><img src="/images/ajax-loader.gif" alt="Loading"/></center>';
    var $container = $(this);
    var height = $container.height();
    $container.html(loading).css("height", height + "px").load(url, function(response, status, xhr){
      $container.css("height", "");
      if (status == "error") {
        $container.html("Error:" + xhr.status + " " + xhr.statusText);
      } else {
        if(callback) callback(response, status, xhr);
      }
    });
  };

  // make selected links in a container replace the container
  $.fn.pjaxContainer = function(selector){
    selector = selector || 'a';
    var $container = $(this);
    $(selector, $container).live('click', function(){
      $container.responsiveLoad($(this).attr("href"));
      return false;
    });
  };
}(this.jQuery));
