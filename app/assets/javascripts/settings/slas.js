/**
 * slas.js
 *
 * version: v0.10.31
 * rev: 2154e276ab769fc35daf182ff171da45e27a9047
 */
/*
 * classList.js: Cross-browser full element.classList implementation.
 * 2014-07-23
 *
 * By Eli Grey, http://eligrey.com
 * Public Domain.
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 */

/*global self, document, DOMException */

/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js*/

if ("document" in self) {

// Full polyfill for browsers with no classList support
if (!("classList" in document.createElement("_"))) {

(function (view) {

"use strict";

if (!('Element' in view)) return;

var
	  classListProp = "classList"
	, protoProp = "prototype"
	, elemCtrProto = view.Element[protoProp]
	, objCtr = Object
	, strTrim = String[protoProp].trim || function () {
		return this.replace(/^\s+|\s+$/g, "");
	}
	, arrIndexOf = Array[protoProp].indexOf || function (item) {
		var
			  i = 0
			, len = this.length
		;
		for (; i < len; i++) {
			if (i in this && this[i] === item) {
				return i;
			}
		}
		return -1;
	}
	// Vendors: please allow content code to instantiate DOMExceptions
	, DOMEx = function (type, message) {
		this.name = type;
		this.code = DOMException[type];
		this.message = message;
	}
	, checkTokenAndGetIndex = function (classList, token) {
		if (token === "") {
			throw new DOMEx(
				  "SYNTAX_ERR"
				, "An invalid or illegal string was specified"
			);
		}
		if (/\s/.test(token)) {
			throw new DOMEx(
				  "INVALID_CHARACTER_ERR"
				, "String contains an invalid character"
			);
		}
		return arrIndexOf.call(classList, token);
	}
	, ClassList = function (elem) {
		var
			  trimmedClasses = strTrim.call(elem.getAttribute("class") || "")
			, classes = trimmedClasses ? trimmedClasses.split(/\s+/) : []
			, i = 0
			, len = classes.length
		;
		for (; i < len; i++) {
			this.push(classes[i]);
		}
		this._updateClassName = function () {
			elem.setAttribute("class", this.toString());
		};
	}
	, classListProto = ClassList[protoProp] = []
	, classListGetter = function () {
		return new ClassList(this);
	}
;
// Most DOMException implementations don't allow calling DOMException's toString()
// on non-DOMExceptions. Error's toString() is sufficient here.
DOMEx[protoProp] = Error[protoProp];
classListProto.item = function (i) {
	return this[i] || null;
};
classListProto.contains = function (token) {
	token += "";
	return checkTokenAndGetIndex(this, token) !== -1;
};
classListProto.add = function () {
	var
		  tokens = arguments
		, i = 0
		, l = tokens.length
		, token
		, updated = false
	;
	do {
		token = tokens[i] + "";
		if (checkTokenAndGetIndex(this, token) === -1) {
			this.push(token);
			updated = true;
		}
	}
	while (++i < l);

	if (updated) {
		this._updateClassName();
	}
};
classListProto.remove = function () {
	var
		  tokens = arguments
		, i = 0
		, l = tokens.length
		, token
		, updated = false
		, index
	;
	do {
		token = tokens[i] + "";
		index = checkTokenAndGetIndex(this, token);
		while (index !== -1) {
			this.splice(index, 1);
			updated = true;
			index = checkTokenAndGetIndex(this, token);
		}
	}
	while (++i < l);

	if (updated) {
		this._updateClassName();
	}
};
classListProto.toggle = function (token, force) {
	token += "";

	var
		  result = this.contains(token)
		, method = result ?
			force !== true && "remove"
		:
			force !== false && "add"
	;

	if (method) {
		this[method](token);
	}

	if (force === true || force === false) {
		return force;
	} else {
		return !result;
	}
};
classListProto.toString = function () {
	return this.join(" ");
};

if (objCtr.defineProperty) {
	var classListPropDesc = {
		  get: classListGetter
		, enumerable: true
		, configurable: true
	};
	try {
		objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
	} catch (ex) { // IE 8 doesn't support enumerable:true
		if (ex.number === -0x7FF5EC54) {
			classListPropDesc.enumerable = false;
			objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
		}
	}
} else if (objCtr[protoProp].__defineGetter__) {
	elemCtrProto.__defineGetter__(classListProp, classListGetter);
}

}(self));

} else {
// There is full or partial native classList support, so just check if we need
// to normalize the add/remove and toggle APIs.

(function () {
	"use strict";

	var testElement = document.createElement("_");

	testElement.classList.add("c1", "c2");

	// Polyfill for IE 10/11 and Firefox <26, where classList.add and
	// classList.remove exist but support only one argument at a time.
	if (!testElement.classList.contains("c2")) {
		var createMethod = function(method) {
			var original = DOMTokenList.prototype[method];

			DOMTokenList.prototype[method] = function(token) {
				var i, len = arguments.length;

				for (i = 0; i < len; i++) {
					token = arguments[i];
					original.call(this, token);
				}
			};
		};
		createMethod('add');
		createMethod('remove');
	}

	testElement.classList.toggle("c3", false);

	// Polyfill for IE 10 and Firefox <24, where classList.toggle does not
	// support the second argument.
	if (testElement.classList.contains("c3")) {
		var _toggle = DOMTokenList.prototype.toggle;

		DOMTokenList.prototype.toggle = function(token, force) {
			if (1 in arguments && !this.contains(token) === !force) {
				return force;
			} else {
				return _toggle.call(this, token);
			}
		};

	}

	testElement = null;
}());

}

}


/*
 * raf.js
 * https://github.com/ngryman/raf.js
 *
 * original requestAnimationFrame polyfill by Erik Möller
 * inspired from paul_irish gist and post
 *
 * Copyright (c) 2013 ngryman
 * Licensed under the MIT license.
 */

(function(window) {
	var lastTime = 0,
		vendors = ['webkit', 'moz'],
		requestAnimationFrame = window.requestAnimationFrame,
		cancelAnimationFrame = window.cancelAnimationFrame,
		i = vendors.length;

	// try to un-prefix existing raf
	while (--i >= 0 && !requestAnimationFrame) {
		requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
		cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'];
	}

	// polyfill with setTimeout fallback
	// heavily inspired from @darius gist mod: https://gist.github.com/paulirish/1579671#comment-837945
	if (!requestAnimationFrame || !cancelAnimationFrame) {
		requestAnimationFrame = function(callback) {
			var now = +new Date(), nextTime = Math.max(lastTime + 16, now);
			return setTimeout(function() {
				callback(lastTime = nextTime);
			}, nextTime - now);
		};

		cancelAnimationFrame = clearTimeout;
	}

	// export to window
	window.requestAnimationFrame = requestAnimationFrame;
	window.cancelAnimationFrame = cancelAnimationFrame;
}(window));

/* (The MIT License)
 *
 * Copyright (c) 2012 Brandon Benvie <http://bbenvie.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the 'Software'), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included with all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// Original WeakMap implementation by Gozala @ https://gist.github.com/1269991
// Updated and bugfixed by Raynos @ https://gist.github.com/1638059
// Expanded by Benvie @ https://github.com/Benvie/harmony-collections

void function(global, undefined_, undefined){
  var getProps = Object.getOwnPropertyNames,
      defProp  = Object.defineProperty,
      toSource = Function.prototype.toString,
      create   = Object.create,
      hasOwn   = Object.prototype.hasOwnProperty,
      funcName = /^\n?function\s?(\w*)?_?\(/;


  function define(object, key, value){
    if (typeof key === 'function') {
      value = key;
      key = nameOf(value).replace(/_$/, '');
    }
    return defProp(object, key, { configurable: true, writable: true, value: value });
  }

  function nameOf(func){
    return typeof func !== 'function'
          ? '' : 'name' in func
          ? func.name : toSource.call(func).match(funcName)[1];
  }

  // ############
  // ### Data ###
  // ############

  var Data = (function(){
    var dataDesc = { value: { writable: true, value: undefined } },
        datalock = 'return function(k){if(k===s)return l}',
        uids     = create(null),

        createUID = function(){
          var key = Math.random().toString(36).slice(2);
          return key in uids ? createUID() : uids[key] = key;
        },

        globalID = createUID(),

        storage = function(obj){
          if (hasOwn.call(obj, globalID))
            return obj[globalID];

          if (!Object.isExtensible(obj))
            throw new TypeError("Object must be extensible");

          var store = create(null);
          defProp(obj, globalID, { value: store });
          return store;
        };

    // common per-object storage area made visible by patching getOwnPropertyNames'
    define(Object, function getOwnPropertyNames(obj){
      var props = getProps(obj);
      if (hasOwn.call(obj, globalID))
        props.splice(props.indexOf(globalID), 1);
      return props;
    });

    function Data(){
      var puid = createUID(),
          secret = {};

      this.unlock = function(obj){
        var store = storage(obj);
        if (hasOwn.call(store, puid))
          return store[puid](secret);

        var data = create(null, dataDesc);
        defProp(store, puid, {
          value: new Function('s', 'l', datalock)(secret, data)
        });
        return data;
      }
    }

    define(Data.prototype, function get(o){ return this.unlock(o).value });
    define(Data.prototype, function set(o, v){ this.unlock(o).value = v });

    return Data;
  }());


  var WM = (function(data){
    var validate = function(key){
      if (key == null || typeof key !== 'object' && typeof key !== 'function')
        throw new TypeError("Invalid WeakMap key");
    }

    var wrap = function(collection, value){
      var store = data.unlock(collection);
      if (store.value)
        throw new TypeError("Object is already a WeakMap");
      store.value = value;
    }

    var unwrap = function(collection){
      var storage = data.unlock(collection).value;
      if (!storage)
        throw new TypeError("WeakMap is not generic");
      return storage;
    }

    var initialize = function(weakmap, iterable){
      if (iterable !== null && typeof iterable === 'object' && typeof iterable.forEach === 'function') {
        iterable.forEach(function(item, i){
          if (item instanceof Array && item.length === 2)
            set.call(weakmap, iterable[i][0], iterable[i][1]);
        });
      }
    }


    function WeakMap(iterable){
      if (this === global || this == null || this === WeakMap.prototype)
        return new WeakMap(iterable);

      wrap(this, new Data);
      initialize(this, iterable);
    }

    function get(key){
      validate(key);
      var value = unwrap(this).get(key);
      return value === undefined_ ? undefined : value;
    }

    function set(key, value){
      validate(key);
      // store a token for explicit undefined so that "has" works correctly
      unwrap(this).set(key, value === undefined ? undefined_ : value);
    }

    function has(key){
      validate(key);
      return unwrap(this).get(key) !== undefined;
    }

    function delete_(key){
      validate(key);
      var data = unwrap(this),
          had = data.get(key) !== undefined;
      data.set(key, undefined);
      return had;
    }

    function toString(){
      unwrap(this);
      return '[object WeakMap]';
    }

    try {
      var src = ('return '+delete_).replace('e_', '\\u0065'),
          del = new Function('unwrap', 'validate', src)(unwrap, validate);
    } catch (e) {
      var del = delete_;
    }

    var src = (''+Object).split('Object');
    var stringifier = function toString(){
      return src[0] + nameOf(this) + src[1];
    };

    define(stringifier, stringifier);

    var prep = { __proto__: [] } instanceof Array
      ? function(f){ f.__proto__ = stringifier }
      : function(f){ define(f, stringifier) };

    prep(WeakMap);

    [toString, get, set, has, del].forEach(function(method){
      define(WeakMap.prototype, method);
      prep(method);
    });

    return WeakMap;
  }(new Data));

  var defaultCreator = Object.create
    ? function(){ return Object.create(null) }
    : function(){ return {} };

  function createStorage(creator){
    var weakmap = new WM;
    creator || (creator = defaultCreator);

    function storage(object, value){
      if (value || arguments.length === 2) {
        weakmap.set(object, value);
      } else {
        value = weakmap.get(object);
        if (value === undefined) {
          value = creator(object);
          weakmap.set(object, value);
        }
      }
      return value;
    }

    return storage;
  }


  if (typeof module !== 'undefined') {
    module.exports = WM;
  } else if (typeof exports !== 'undefined') {
    exports.WeakMap = WM;
  } else if (!('WeakMap' in global)) {
    global.WeakMap = WM;
  }

  WM.createStorage = createStorage;
  if (global.WeakMap)
    global.WeakMap.createStorage = createStorage;
}((0, eval)('this'));

/*
 * Copyright 2012 The Polymer Authors. All rights reserved.
 * Use of this source code is goverened by a BSD-style
 * license that can be found in the LICENSE file.
 */

(function(global) {

  var registrationsTable = new WeakMap();

  // We use setImmediate or postMessage for our future callback.
  var setImmediate = window.msSetImmediate;

  // Use post message to emulate setImmediate.
  if (!setImmediate) {
    var setImmediateQueue = [];
    var sentinel = String(Math.random());
    window.addEventListener('message', function(e) {
      if (e.data === sentinel) {
        var queue = setImmediateQueue;
        setImmediateQueue = [];
        queue.forEach(function(func) {
          func();
        });
      }
    });
    setImmediate = function(func) {
      setImmediateQueue.push(func);
      window.postMessage(sentinel, '*');
    };
  }

  // This is used to ensure that we never schedule 2 callas to setImmediate
  var isScheduled = false;

  // Keep track of observers that needs to be notified next time.
  var scheduledObservers = [];

  /**
   * Schedules |dispatchCallback| to be called in the future.
   * @param {MutationObserver} observer
   */
  function scheduleCallback(observer) {
    scheduledObservers.push(observer);
    if (!isScheduled) {
      isScheduled = true;
      setImmediate(dispatchCallbacks);
    }
  }

  function wrapIfNeeded(node) {
    return window.ShadowDOMPolyfill &&
        window.ShadowDOMPolyfill.wrapIfNeeded(node) ||
        node;
  }

  function dispatchCallbacks() {
    // http://dom.spec.whatwg.org/#mutation-observers

    isScheduled = false; // Used to allow a new setImmediate call above.

    var observers = scheduledObservers;
    scheduledObservers = [];
    // Sort observers based on their creation UID (incremental).
    observers.sort(function(o1, o2) {
      return o1.uid_ - o2.uid_;
    });

    var anyNonEmpty = false;
    observers.forEach(function(observer) {

      // 2.1, 2.2
      var queue = observer.takeRecords();
      // 2.3. Remove all transient registered observers whose observer is mo.
      removeTransientObserversFor(observer);

      // 2.4
      if (queue.length) {
        observer.callback_(queue, observer);
        anyNonEmpty = true;
      }
    });

    // 3.
    if (anyNonEmpty)
      dispatchCallbacks();
  }

  function removeTransientObserversFor(observer) {
    observer.nodes_.forEach(function(node) {
      var registrations = registrationsTable.get(node);
      if (!registrations)
        return;
      registrations.forEach(function(registration) {
        if (registration.observer === observer)
          registration.removeTransientObservers();
      });
    });
  }

  /**
   * This function is used for the "For each registered observer observer (with
   * observer's options as options) in target's list of registered observers,
   * run these substeps:" and the "For each ancestor ancestor of target, and for
   * each registered observer observer (with options options) in ancestor's list
   * of registered observers, run these substeps:" part of the algorithms. The
   * |options.subtree| is checked to ensure that the callback is called
   * correctly.
   *
   * @param {Node} target
   * @param {function(MutationObserverInit):MutationRecord} callback
   */
  function forEachAncestorAndObserverEnqueueRecord(target, callback) {
    for (var node = target; node; node = node.parentNode) {
      var registrations = registrationsTable.get(node);

      if (registrations) {
        for (var j = 0; j < registrations.length; j++) {
          var registration = registrations[j];
          var options = registration.options;

          // Only target ignores subtree.
          if (node !== target && !options.subtree)
            continue;

          var record = callback(options);
          if (record)
            registration.enqueue(record);
        }
      }
    }
  }

  var uidCounter = 0;

  /**
   * The class that maps to the DOM MutationObserver interface.
   * @param {Function} callback.
   * @constructor
   */
  function JsMutationObserver(callback) {
    this.callback_ = callback;
    this.nodes_ = [];
    this.records_ = [];
    this.uid_ = ++uidCounter;
  }

  JsMutationObserver.prototype = {
    observe: function(target, options) {
      target = wrapIfNeeded(target);

      // 1.1
      if (!options.childList && !options.attributes && !options.characterData ||

          // 1.2
          options.attributeOldValue && !options.attributes ||

          // 1.3
          options.attributeFilter && options.attributeFilter.length &&
              !options.attributes ||

          // 1.4
          options.characterDataOldValue && !options.characterData) {

        throw new SyntaxError();
      }

      var registrations = registrationsTable.get(target);
      if (!registrations)
        registrationsTable.set(target, registrations = []);

      // 2
      // If target's list of registered observers already includes a registered
      // observer associated with the context object, replace that registered
      // observer's options with options.
      var registration;
      for (var i = 0; i < registrations.length; i++) {
        if (registrations[i].observer === this) {
          registration = registrations[i];
          registration.removeListeners();
          registration.options = options;
          break;
        }
      }

      // 3.
      // Otherwise, add a new registered observer to target's list of registered
      // observers with the context object as the observer and options as the
      // options, and add target to context object's list of nodes on which it
      // is registered.
      if (!registration) {
        registration = new Registration(this, target, options);
        registrations.push(registration);
        this.nodes_.push(target);
      }

      registration.addListeners();
    },

    disconnect: function() {
      this.nodes_.forEach(function(node) {
        var registrations = registrationsTable.get(node);
        for (var i = 0; i < registrations.length; i++) {
          var registration = registrations[i];
          if (registration.observer === this) {
            registration.removeListeners();
            registrations.splice(i, 1);
            // Each node can only have one registered observer associated with
            // this observer.
            break;
          }
        }
      }, this);
      this.records_ = [];
    },

    takeRecords: function() {
      var copyOfRecords = this.records_;
      this.records_ = [];
      return copyOfRecords;
    }
  };

  /**
   * @param {string} type
   * @param {Node} target
   * @constructor
   */
  function MutationRecord(type, target) {
    this.type = type;
    this.target = target;
    this.addedNodes = [];
    this.removedNodes = [];
    this.previousSibling = null;
    this.nextSibling = null;
    this.attributeName = null;
    this.attributeNamespace = null;
    this.oldValue = null;
  }

  function copyMutationRecord(original) {
    var record = new MutationRecord(original.type, original.target);
    record.addedNodes = original.addedNodes.slice();
    record.removedNodes = original.removedNodes.slice();
    record.previousSibling = original.previousSibling;
    record.nextSibling = original.nextSibling;
    record.attributeName = original.attributeName;
    record.attributeNamespace = original.attributeNamespace;
    record.oldValue = original.oldValue;
    return record;
  };

  // We keep track of the two (possibly one) records used in a single mutation.
  var currentRecord, recordWithOldValue;

  /**
   * Creates a record without |oldValue| and caches it as |currentRecord| for
   * later use.
   * @param {string} oldValue
   * @return {MutationRecord}
   */
  function getRecord(type, target) {
    return currentRecord = new MutationRecord(type, target);
  }

  /**
   * Gets or creates a record with |oldValue| based in the |currentRecord|
   * @param {string} oldValue
   * @return {MutationRecord}
   */
  function getRecordWithOldValue(oldValue) {
    if (recordWithOldValue)
      return recordWithOldValue;
    recordWithOldValue = copyMutationRecord(currentRecord);
    recordWithOldValue.oldValue = oldValue;
    return recordWithOldValue;
  }

  function clearRecords() {
    currentRecord = recordWithOldValue = undefined;
  }

  /**
   * @param {MutationRecord} record
   * @return {boolean} Whether the record represents a record from the current
   * mutation event.
   */
  function recordRepresentsCurrentMutation(record) {
    return record === recordWithOldValue || record === currentRecord;
  }

  /**
   * Selects which record, if any, to replace the last record in the queue.
   * This returns |null| if no record should be replaced.
   *
   * @param {MutationRecord} lastRecord
   * @param {MutationRecord} newRecord
   * @param {MutationRecord}
   */
  function selectRecord(lastRecord, newRecord) {
    if (lastRecord === newRecord)
      return lastRecord;

    // Check if the the record we are adding represents the same record. If
    // so, we keep the one with the oldValue in it.
    if (recordWithOldValue && recordRepresentsCurrentMutation(lastRecord))
      return recordWithOldValue;

    return null;
  }

  /**
   * Class used to represent a registered observer.
   * @param {MutationObserver} observer
   * @param {Node} target
   * @param {MutationObserverInit} options
   * @constructor
   */
  function Registration(observer, target, options) {
    this.observer = observer;
    this.target = target;
    this.options = options;
    this.transientObservedNodes = [];
  }

  Registration.prototype = {
    enqueue: function(record) {
      var records = this.observer.records_;
      var length = records.length;

      // There are cases where we replace the last record with the new record.
      // For example if the record represents the same mutation we need to use
      // the one with the oldValue. If we get same record (this can happen as we
      // walk up the tree) we ignore the new record.
      if (records.length > 0) {
        var lastRecord = records[length - 1];
        var recordToReplaceLast = selectRecord(lastRecord, record);
        if (recordToReplaceLast) {
          records[length - 1] = recordToReplaceLast;
          return;
        }
      } else {
        scheduleCallback(this.observer);
      }

      records[length] = record;
    },

    addListeners: function() {
      this.addListeners_(this.target);
    },

    addListeners_: function(node) {
      var options = this.options;
      if (options.attributes)
        node.addEventListener('DOMAttrModified', this, true);

      if (options.characterData)
        node.addEventListener('DOMCharacterDataModified', this, true);

      if (options.childList)
        node.addEventListener('DOMNodeInserted', this, true);

      if (options.childList || options.subtree)
        node.addEventListener('DOMNodeRemoved', this, true);
    },

    removeListeners: function() {
      this.removeListeners_(this.target);
    },

    removeListeners_: function(node) {
      var options = this.options;
      if (options.attributes)
        node.removeEventListener('DOMAttrModified', this, true);

      if (options.characterData)
        node.removeEventListener('DOMCharacterDataModified', this, true);

      if (options.childList)
        node.removeEventListener('DOMNodeInserted', this, true);

      if (options.childList || options.subtree)
        node.removeEventListener('DOMNodeRemoved', this, true);
    },

    /**
     * Adds a transient observer on node. The transient observer gets removed
     * next time we deliver the change records.
     * @param {Node} node
     */
    addTransientObserver: function(node) {
      // Don't add transient observers on the target itself. We already have all
      // the required listeners set up on the target.
      if (node === this.target)
        return;

      this.addListeners_(node);
      this.transientObservedNodes.push(node);
      var registrations = registrationsTable.get(node);
      if (!registrations)
        registrationsTable.set(node, registrations = []);

      // We know that registrations does not contain this because we already
      // checked if node === this.target.
      registrations.push(this);
    },

    removeTransientObservers: function() {
      var transientObservedNodes = this.transientObservedNodes;
      this.transientObservedNodes = [];

      transientObservedNodes.forEach(function(node) {
        // Transient observers are never added to the target.
        this.removeListeners_(node);

        var registrations = registrationsTable.get(node);
        for (var i = 0; i < registrations.length; i++) {
          if (registrations[i] === this) {
            registrations.splice(i, 1);
            // Each node can only have one registered observer associated with
            // this observer.
            break;
          }
        }
      }, this);
    },

    handleEvent: function(e) {
      // Stop propagation since we are managing the propagation manually.
      // This means that other mutation events on the page will not work
      // correctly but that is by design.
      e.stopImmediatePropagation();

      switch (e.type) {
        case 'DOMAttrModified':
          // http://dom.spec.whatwg.org/#concept-mo-queue-attributes

          var name = e.attrName;
          var namespace = e.relatedNode.namespaceURI;
          var target = e.target;

          // 1.
          var record = new getRecord('attributes', target);
          record.attributeName = name;
          record.attributeNamespace = namespace;

          // 2.
          var oldValue =
              e.attrChange === MutationEvent.ADDITION ? null : e.prevValue;

          forEachAncestorAndObserverEnqueueRecord(target, function(options) {
            // 3.1, 4.2
            if (!options.attributes)
              return;

            // 3.2, 4.3
            if (options.attributeFilter && options.attributeFilter.length &&
                options.attributeFilter.indexOf(name) === -1 &&
                options.attributeFilter.indexOf(namespace) === -1) {
              return;
            }
            // 3.3, 4.4
            if (options.attributeOldValue)
              return getRecordWithOldValue(oldValue);

            // 3.4, 4.5
            return record;
          });

          break;

        case 'DOMCharacterDataModified':
          // http://dom.spec.whatwg.org/#concept-mo-queue-characterdata
          var target = e.target;

          // 1.
          var record = getRecord('characterData', target);

          // 2.
          var oldValue = e.prevValue;


          forEachAncestorAndObserverEnqueueRecord(target, function(options) {
            // 3.1, 4.2
            if (!options.characterData)
              return;

            // 3.2, 4.3
            if (options.characterDataOldValue)
              return getRecordWithOldValue(oldValue);

            // 3.3, 4.4
            return record;
          });

          break;

        case 'DOMNodeRemoved':
          this.addTransientObserver(e.target);
          // Fall through.
        case 'DOMNodeInserted':
          // http://dom.spec.whatwg.org/#concept-mo-queue-childlist
          var target = e.relatedNode;
          var changedNode = e.target;
          var addedNodes, removedNodes;
          if (e.type === 'DOMNodeInserted') {
            addedNodes = [changedNode];
            removedNodes = [];
          } else {

            addedNodes = [];
            removedNodes = [changedNode];
          }
          var previousSibling = changedNode.previousSibling;
          var nextSibling = changedNode.nextSibling;

          // 1.
          var record = getRecord('childList', target);
          record.addedNodes = addedNodes;
          record.removedNodes = removedNodes;
          record.previousSibling = previousSibling;
          record.nextSibling = nextSibling;

          forEachAncestorAndObserverEnqueueRecord(target, function(options) {
            // 2.1, 3.2
            if (!options.childList)
              return;

            // 2.2, 3.3
            return record;
          });

      }

      clearRecords();
    }
  };

  global.JsMutationObserver = JsMutationObserver;

  if (!global.MutationObserver)
    global.MutationObserver = JsMutationObserver;


})(this);

/*!
 * @overview es6-promise - a tiny implementation of Promises/A+.
 * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
 * @license   Licensed under MIT license
 *            See https://raw.githubusercontent.com/jakearchibald/es6-promise/master/LICENSE
 * @version   2.3.0
 */

(function() {
    "use strict";
    function lib$es6$promise$utils$$objectOrFunction(x) {
      return typeof x === 'function' || (typeof x === 'object' && x !== null);
    }

    function lib$es6$promise$utils$$isFunction(x) {
      return typeof x === 'function';
    }

    function lib$es6$promise$utils$$isMaybeThenable(x) {
      return typeof x === 'object' && x !== null;
    }

    var lib$es6$promise$utils$$_isArray;
    if (!Array.isArray) {
      lib$es6$promise$utils$$_isArray = function (x) {
        return Object.prototype.toString.call(x) === '[object Array]';
      };
    } else {
      lib$es6$promise$utils$$_isArray = Array.isArray;
    }

    var lib$es6$promise$utils$$isArray = lib$es6$promise$utils$$_isArray;
    var lib$es6$promise$asap$$len = 0;
    var lib$es6$promise$asap$$toString = {}.toString;
    var lib$es6$promise$asap$$vertxNext;
    var lib$es6$promise$asap$$customSchedulerFn;

    var lib$es6$promise$asap$$asap = function asap(callback, arg) {
      lib$es6$promise$asap$$queue[lib$es6$promise$asap$$len] = callback;
      lib$es6$promise$asap$$queue[lib$es6$promise$asap$$len + 1] = arg;
      lib$es6$promise$asap$$len += 2;
      if (lib$es6$promise$asap$$len === 2) {
        // If len is 2, that means that we need to schedule an async flush.
        // If additional callbacks are queued before the queue is flushed, they
        // will be processed by this flush that we are scheduling.
        if (lib$es6$promise$asap$$customSchedulerFn) {
          lib$es6$promise$asap$$customSchedulerFn(lib$es6$promise$asap$$flush);
        } else {
          lib$es6$promise$asap$$scheduleFlush();
        }
      }
    }

    function lib$es6$promise$asap$$setScheduler(scheduleFn) {
      lib$es6$promise$asap$$customSchedulerFn = scheduleFn;
    }

    function lib$es6$promise$asap$$setAsap(asapFn) {
      lib$es6$promise$asap$$asap = asapFn;
    }

    var lib$es6$promise$asap$$browserWindow = (typeof window !== 'undefined') ? window : undefined;
    var lib$es6$promise$asap$$browserGlobal = lib$es6$promise$asap$$browserWindow || {};
    var lib$es6$promise$asap$$BrowserMutationObserver = lib$es6$promise$asap$$browserGlobal.MutationObserver || lib$es6$promise$asap$$browserGlobal.WebKitMutationObserver;
    var lib$es6$promise$asap$$isNode = typeof process !== 'undefined' && {}.toString.call(process) === '[object process]';

    // test for web worker but not in IE10
    var lib$es6$promise$asap$$isWorker = typeof Uint8ClampedArray !== 'undefined' &&
      typeof importScripts !== 'undefined' &&
      typeof MessageChannel !== 'undefined';

    // node
    function lib$es6$promise$asap$$useNextTick() {
      var nextTick = process.nextTick;
      // node version 0.10.x displays a deprecation warning when nextTick is used recursively
      // setImmediate should be used instead instead
      var version = process.versions.node.match(/^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/);
      if (Array.isArray(version) && version[1] === '0' && version[2] === '10') {
        nextTick = setImmediate;
      }
      return function() {
        nextTick(lib$es6$promise$asap$$flush);
      };
    }

    // vertx
    function lib$es6$promise$asap$$useVertxTimer() {
      return function() {
        lib$es6$promise$asap$$vertxNext(lib$es6$promise$asap$$flush);
      };
    }

    function lib$es6$promise$asap$$useMutationObserver() {
      var iterations = 0;
      var observer = new lib$es6$promise$asap$$BrowserMutationObserver(lib$es6$promise$asap$$flush);
      var node = document.createTextNode('');
      observer.observe(node, { characterData: true });

      return function() {
        node.data = (iterations = ++iterations % 2);
      };
    }

    // web worker
    function lib$es6$promise$asap$$useMessageChannel() {
      var channel = new MessageChannel();
      channel.port1.onmessage = lib$es6$promise$asap$$flush;
      return function () {
        channel.port2.postMessage(0);
      };
    }

    function lib$es6$promise$asap$$useSetTimeout() {
      return function() {
        setTimeout(lib$es6$promise$asap$$flush, 1);
      };
    }

    var lib$es6$promise$asap$$queue = new Array(1000);
    function lib$es6$promise$asap$$flush() {
      for (var i = 0; i < lib$es6$promise$asap$$len; i+=2) {
        var callback = lib$es6$promise$asap$$queue[i];
        var arg = lib$es6$promise$asap$$queue[i+1];

        callback(arg);

        lib$es6$promise$asap$$queue[i] = undefined;
        lib$es6$promise$asap$$queue[i+1] = undefined;
      }

      lib$es6$promise$asap$$len = 0;
    }

    function lib$es6$promise$asap$$attemptVertex() {
      try {
        var r = require;
        var vertx = r('vertx');
        lib$es6$promise$asap$$vertxNext = vertx.runOnLoop || vertx.runOnContext;
        return lib$es6$promise$asap$$useVertxTimer();
      } catch(e) {
        return lib$es6$promise$asap$$useSetTimeout();
      }
    }

    var lib$es6$promise$asap$$scheduleFlush;
    // Decide what async method to use to triggering processing of queued callbacks:
    if (lib$es6$promise$asap$$isNode) {
      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useNextTick();
    } else if (lib$es6$promise$asap$$BrowserMutationObserver) {
      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useMutationObserver();
    } else if (lib$es6$promise$asap$$isWorker) {
      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useMessageChannel();
    } else if (lib$es6$promise$asap$$browserWindow === undefined && typeof require === 'function') {
      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$attemptVertex();
    } else {
      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useSetTimeout();
    }

    function lib$es6$promise$$internal$$noop() {}

    var lib$es6$promise$$internal$$PENDING   = void 0;
    var lib$es6$promise$$internal$$FULFILLED = 1;
    var lib$es6$promise$$internal$$REJECTED  = 2;

    var lib$es6$promise$$internal$$GET_THEN_ERROR = new lib$es6$promise$$internal$$ErrorObject();

    function lib$es6$promise$$internal$$selfFullfillment() {
      return new TypeError("You cannot resolve a promise with itself");
    }

    function lib$es6$promise$$internal$$cannotReturnOwn() {
      return new TypeError('A promises callback cannot return that same promise.');
    }

    function lib$es6$promise$$internal$$getThen(promise) {
      try {
        return promise.then;
      } catch(error) {
        lib$es6$promise$$internal$$GET_THEN_ERROR.error = error;
        return lib$es6$promise$$internal$$GET_THEN_ERROR;
      }
    }

    function lib$es6$promise$$internal$$tryThen(then, value, fulfillmentHandler, rejectionHandler) {
      try {
        then.call(value, fulfillmentHandler, rejectionHandler);
      } catch(e) {
        return e;
      }
    }

    function lib$es6$promise$$internal$$handleForeignThenable(promise, thenable, then) {
       lib$es6$promise$asap$$asap(function(promise) {
        var sealed = false;
        var error = lib$es6$promise$$internal$$tryThen(then, thenable, function(value) {
          if (sealed) { return; }
          sealed = true;
          if (thenable !== value) {
            lib$es6$promise$$internal$$resolve(promise, value);
          } else {
            lib$es6$promise$$internal$$fulfill(promise, value);
          }
        }, function(reason) {
          if (sealed) { return; }
          sealed = true;

          lib$es6$promise$$internal$$reject(promise, reason);
        }, 'Settle: ' + (promise._label || ' unknown promise'));

        if (!sealed && error) {
          sealed = true;
          lib$es6$promise$$internal$$reject(promise, error);
        }
      }, promise);
    }

    function lib$es6$promise$$internal$$handleOwnThenable(promise, thenable) {
      if (thenable._state === lib$es6$promise$$internal$$FULFILLED) {
        lib$es6$promise$$internal$$fulfill(promise, thenable._result);
      } else if (thenable._state === lib$es6$promise$$internal$$REJECTED) {
        lib$es6$promise$$internal$$reject(promise, thenable._result);
      } else {
        lib$es6$promise$$internal$$subscribe(thenable, undefined, function(value) {
          lib$es6$promise$$internal$$resolve(promise, value);
        }, function(reason) {
          lib$es6$promise$$internal$$reject(promise, reason);
        });
      }
    }

    function lib$es6$promise$$internal$$handleMaybeThenable(promise, maybeThenable) {
      if (maybeThenable.constructor === promise.constructor) {
        lib$es6$promise$$internal$$handleOwnThenable(promise, maybeThenable);
      } else {
        var then = lib$es6$promise$$internal$$getThen(maybeThenable);

        if (then === lib$es6$promise$$internal$$GET_THEN_ERROR) {
          lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$GET_THEN_ERROR.error);
        } else if (then === undefined) {
          lib$es6$promise$$internal$$fulfill(promise, maybeThenable);
        } else if (lib$es6$promise$utils$$isFunction(then)) {
          lib$es6$promise$$internal$$handleForeignThenable(promise, maybeThenable, then);
        } else {
          lib$es6$promise$$internal$$fulfill(promise, maybeThenable);
        }
      }
    }

    function lib$es6$promise$$internal$$resolve(promise, value) {
      if (promise === value) {
        lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$selfFullfillment());
      } else if (lib$es6$promise$utils$$objectOrFunction(value)) {
        lib$es6$promise$$internal$$handleMaybeThenable(promise, value);
      } else {
        lib$es6$promise$$internal$$fulfill(promise, value);
      }
    }

    function lib$es6$promise$$internal$$publishRejection(promise) {
      if (promise._onerror) {
        promise._onerror(promise._result);
      }

      lib$es6$promise$$internal$$publish(promise);
    }

    function lib$es6$promise$$internal$$fulfill(promise, value) {
      if (promise._state !== lib$es6$promise$$internal$$PENDING) { return; }

      promise._result = value;
      promise._state = lib$es6$promise$$internal$$FULFILLED;

      if (promise._subscribers.length !== 0) {
        lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publish, promise);
      }
    }

    function lib$es6$promise$$internal$$reject(promise, reason) {
      if (promise._state !== lib$es6$promise$$internal$$PENDING) { return; }
      promise._state = lib$es6$promise$$internal$$REJECTED;
      promise._result = reason;

      lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publishRejection, promise);
    }

    function lib$es6$promise$$internal$$subscribe(parent, child, onFulfillment, onRejection) {
      var subscribers = parent._subscribers;
      var length = subscribers.length;

      parent._onerror = null;

      subscribers[length] = child;
      subscribers[length + lib$es6$promise$$internal$$FULFILLED] = onFulfillment;
      subscribers[length + lib$es6$promise$$internal$$REJECTED]  = onRejection;

      if (length === 0 && parent._state) {
        lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publish, parent);
      }
    }

    function lib$es6$promise$$internal$$publish(promise) {
      var subscribers = promise._subscribers;
      var settled = promise._state;

      if (subscribers.length === 0) { return; }

      var child, callback, detail = promise._result;

      for (var i = 0; i < subscribers.length; i += 3) {
        child = subscribers[i];
        callback = subscribers[i + settled];

        if (child) {
          lib$es6$promise$$internal$$invokeCallback(settled, child, callback, detail);
        } else {
          callback(detail);
        }
      }

      promise._subscribers.length = 0;
    }

    function lib$es6$promise$$internal$$ErrorObject() {
      this.error = null;
    }

    var lib$es6$promise$$internal$$TRY_CATCH_ERROR = new lib$es6$promise$$internal$$ErrorObject();

    function lib$es6$promise$$internal$$tryCatch(callback, detail) {
      try {
        return callback(detail);
      } catch(e) {
        lib$es6$promise$$internal$$TRY_CATCH_ERROR.error = e;
        return lib$es6$promise$$internal$$TRY_CATCH_ERROR;
      }
    }

    function lib$es6$promise$$internal$$invokeCallback(settled, promise, callback, detail) {
      var hasCallback = lib$es6$promise$utils$$isFunction(callback),
          value, error, succeeded, failed;

      if (hasCallback) {
        value = lib$es6$promise$$internal$$tryCatch(callback, detail);

        if (value === lib$es6$promise$$internal$$TRY_CATCH_ERROR) {
          failed = true;
          error = value.error;
          value = null;
        } else {
          succeeded = true;
        }

        if (promise === value) {
          lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$cannotReturnOwn());
          return;
        }

      } else {
        value = detail;
        succeeded = true;
      }

      if (promise._state !== lib$es6$promise$$internal$$PENDING) {
        // noop
      } else if (hasCallback && succeeded) {
        lib$es6$promise$$internal$$resolve(promise, value);
      } else if (failed) {
        lib$es6$promise$$internal$$reject(promise, error);
      } else if (settled === lib$es6$promise$$internal$$FULFILLED) {
        lib$es6$promise$$internal$$fulfill(promise, value);
      } else if (settled === lib$es6$promise$$internal$$REJECTED) {
        lib$es6$promise$$internal$$reject(promise, value);
      }
    }

    function lib$es6$promise$$internal$$initializePromise(promise, resolver) {
      try {
        resolver(function resolvePromise(value){
          lib$es6$promise$$internal$$resolve(promise, value);
        }, function rejectPromise(reason) {
          lib$es6$promise$$internal$$reject(promise, reason);
        });
      } catch(e) {
        lib$es6$promise$$internal$$reject(promise, e);
      }
    }

    function lib$es6$promise$enumerator$$Enumerator(Constructor, input) {
      var enumerator = this;

      enumerator._instanceConstructor = Constructor;
      enumerator.promise = new Constructor(lib$es6$promise$$internal$$noop);

      if (enumerator._validateInput(input)) {
        enumerator._input     = input;
        enumerator.length     = input.length;
        enumerator._remaining = input.length;

        enumerator._init();

        if (enumerator.length === 0) {
          lib$es6$promise$$internal$$fulfill(enumerator.promise, enumerator._result);
        } else {
          enumerator.length = enumerator.length || 0;
          enumerator._enumerate();
          if (enumerator._remaining === 0) {
            lib$es6$promise$$internal$$fulfill(enumerator.promise, enumerator._result);
          }
        }
      } else {
        lib$es6$promise$$internal$$reject(enumerator.promise, enumerator._validationError());
      }
    }

    lib$es6$promise$enumerator$$Enumerator.prototype._validateInput = function(input) {
      return lib$es6$promise$utils$$isArray(input);
    };

    lib$es6$promise$enumerator$$Enumerator.prototype._validationError = function() {
      return new Error('Array Methods must be provided an Array');
    };

    lib$es6$promise$enumerator$$Enumerator.prototype._init = function() {
      this._result = new Array(this.length);
    };

    var lib$es6$promise$enumerator$$default = lib$es6$promise$enumerator$$Enumerator;

    lib$es6$promise$enumerator$$Enumerator.prototype._enumerate = function() {
      var enumerator = this;

      var length  = enumerator.length;
      var promise = enumerator.promise;
      var input   = enumerator._input;

      for (var i = 0; promise._state === lib$es6$promise$$internal$$PENDING && i < length; i++) {
        enumerator._eachEntry(input[i], i);
      }
    };

    lib$es6$promise$enumerator$$Enumerator.prototype._eachEntry = function(entry, i) {
      var enumerator = this;
      var c = enumerator._instanceConstructor;

      if (lib$es6$promise$utils$$isMaybeThenable(entry)) {
        if (entry.constructor === c && entry._state !== lib$es6$promise$$internal$$PENDING) {
          entry._onerror = null;
          enumerator._settledAt(entry._state, i, entry._result);
        } else {
          enumerator._willSettleAt(c.resolve(entry), i);
        }
      } else {
        enumerator._remaining--;
        enumerator._result[i] = entry;
      }
    };

    lib$es6$promise$enumerator$$Enumerator.prototype._settledAt = function(state, i, value) {
      var enumerator = this;
      var promise = enumerator.promise;

      if (promise._state === lib$es6$promise$$internal$$PENDING) {
        enumerator._remaining--;

        if (state === lib$es6$promise$$internal$$REJECTED) {
          lib$es6$promise$$internal$$reject(promise, value);
        } else {
          enumerator._result[i] = value;
        }
      }

      if (enumerator._remaining === 0) {
        lib$es6$promise$$internal$$fulfill(promise, enumerator._result);
      }
    };

    lib$es6$promise$enumerator$$Enumerator.prototype._willSettleAt = function(promise, i) {
      var enumerator = this;

      lib$es6$promise$$internal$$subscribe(promise, undefined, function(value) {
        enumerator._settledAt(lib$es6$promise$$internal$$FULFILLED, i, value);
      }, function(reason) {
        enumerator._settledAt(lib$es6$promise$$internal$$REJECTED, i, reason);
      });
    };
    function lib$es6$promise$promise$all$$all(entries) {
      return new lib$es6$promise$enumerator$$default(this, entries).promise;
    }
    var lib$es6$promise$promise$all$$default = lib$es6$promise$promise$all$$all;
    function lib$es6$promise$promise$race$$race(entries) {
      /*jshint validthis:true */
      var Constructor = this;

      var promise = new Constructor(lib$es6$promise$$internal$$noop);

      if (!lib$es6$promise$utils$$isArray(entries)) {
        lib$es6$promise$$internal$$reject(promise, new TypeError('You must pass an array to race.'));
        return promise;
      }

      var length = entries.length;

      function onFulfillment(value) {
        lib$es6$promise$$internal$$resolve(promise, value);
      }

      function onRejection(reason) {
        lib$es6$promise$$internal$$reject(promise, reason);
      }

      for (var i = 0; promise._state === lib$es6$promise$$internal$$PENDING && i < length; i++) {
        lib$es6$promise$$internal$$subscribe(Constructor.resolve(entries[i]), undefined, onFulfillment, onRejection);
      }

      return promise;
    }
    var lib$es6$promise$promise$race$$default = lib$es6$promise$promise$race$$race;
    function lib$es6$promise$promise$resolve$$resolve(object) {
      /*jshint validthis:true */
      var Constructor = this;

      if (object && typeof object === 'object' && object.constructor === Constructor) {
        return object;
      }

      var promise = new Constructor(lib$es6$promise$$internal$$noop);
      lib$es6$promise$$internal$$resolve(promise, object);
      return promise;
    }
    var lib$es6$promise$promise$resolve$$default = lib$es6$promise$promise$resolve$$resolve;
    function lib$es6$promise$promise$reject$$reject(reason) {
      /*jshint validthis:true */
      var Constructor = this;
      var promise = new Constructor(lib$es6$promise$$internal$$noop);
      lib$es6$promise$$internal$$reject(promise, reason);
      return promise;
    }
    var lib$es6$promise$promise$reject$$default = lib$es6$promise$promise$reject$$reject;

    var lib$es6$promise$promise$$counter = 0;

    function lib$es6$promise$promise$$needsResolver() {
      throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
    }

    function lib$es6$promise$promise$$needsNew() {
      throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
    }

    var lib$es6$promise$promise$$default = lib$es6$promise$promise$$Promise;
    /**
      Promise objects represent the eventual result of an asynchronous operation. The
      primary way of interacting with a promise is through its `then` method, which
      registers callbacks to receive either a promise's eventual value or the reason
      why the promise cannot be fulfilled.

      Terminology
      -----------

      - `promise` is an object or function with a `then` method whose behavior conforms to this specification.
      - `thenable` is an object or function that defines a `then` method.
      - `value` is any legal JavaScript value (including undefined, a thenable, or a promise).
      - `exception` is a value that is thrown using the throw statement.
      - `reason` is a value that indicates why a promise was rejected.
      - `settled` the final resting state of a promise, fulfilled or rejected.

      A promise can be in one of three states: pending, fulfilled, or rejected.

      Promises that are fulfilled have a fulfillment value and are in the fulfilled
      state.  Promises that are rejected have a rejection reason and are in the
      rejected state.  A fulfillment value is never a thenable.

      Promises can also be said to *resolve* a value.  If this value is also a
      promise, then the original promise's settled state will match the value's
      settled state.  So a promise that *resolves* a promise that rejects will
      itself reject, and a promise that *resolves* a promise that fulfills will
      itself fulfill.


      Basic Usage:
      ------------

      ```js
      var promise = new Promise(function(resolve, reject) {
        // on success
        resolve(value);

        // on failure
        reject(reason);
      });

      promise.then(function(value) {
        // on fulfillment
      }, function(reason) {
        // on rejection
      });
      ```

      Advanced Usage:
      ---------------

      Promises shine when abstracting away asynchronous interactions such as
      `XMLHttpRequest`s.

      ```js
      function getJSON(url) {
        return new Promise(function(resolve, reject){
          var xhr = new XMLHttpRequest();

          xhr.open('GET', url);
          xhr.onreadystatechange = handler;
          xhr.responseType = 'json';
          xhr.setRequestHeader('Accept', 'application/json');
          xhr.send();

          function handler() {
            if (this.readyState === this.DONE) {
              if (this.status === 200) {
                resolve(this.response);
              } else {
                reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
              }
            }
          };
        });
      }

      getJSON('/posts.json').then(function(json) {
        // on fulfillment
      }, function(reason) {
        // on rejection
      });
      ```

      Unlike callbacks, promises are great composable primitives.

      ```js
      Promise.all([
        getJSON('/posts'),
        getJSON('/comments')
      ]).then(function(values){
        values[0] // => postsJSON
        values[1] // => commentsJSON

        return values;
      });
      ```

      @class Promise
      @param {function} resolver
      Useful for tooling.
      @constructor
    */
    function lib$es6$promise$promise$$Promise(resolver) {
      this._id = lib$es6$promise$promise$$counter++;
      this._state = undefined;
      this._result = undefined;
      this._subscribers = [];

      if (lib$es6$promise$$internal$$noop !== resolver) {
        if (!lib$es6$promise$utils$$isFunction(resolver)) {
          lib$es6$promise$promise$$needsResolver();
        }

        if (!(this instanceof lib$es6$promise$promise$$Promise)) {
          lib$es6$promise$promise$$needsNew();
        }

        lib$es6$promise$$internal$$initializePromise(this, resolver);
      }
    }

    lib$es6$promise$promise$$Promise.all = lib$es6$promise$promise$all$$default;
    lib$es6$promise$promise$$Promise.race = lib$es6$promise$promise$race$$default;
    lib$es6$promise$promise$$Promise.resolve = lib$es6$promise$promise$resolve$$default;
    lib$es6$promise$promise$$Promise.reject = lib$es6$promise$promise$reject$$default;
    lib$es6$promise$promise$$Promise._setScheduler = lib$es6$promise$asap$$setScheduler;
    lib$es6$promise$promise$$Promise._setAsap = lib$es6$promise$asap$$setAsap;
    lib$es6$promise$promise$$Promise._asap = lib$es6$promise$asap$$asap;

    lib$es6$promise$promise$$Promise.prototype = {
      constructor: lib$es6$promise$promise$$Promise,

    /**
      The primary way of interacting with a promise is through its `then` method,
      which registers callbacks to receive either a promise's eventual value or the
      reason why the promise cannot be fulfilled.

      ```js
      findUser().then(function(user){
        // user is available
      }, function(reason){
        // user is unavailable, and you are given the reason why
      });
      ```

      Chaining
      --------

      The return value of `then` is itself a promise.  This second, 'downstream'
      promise is resolved with the return value of the first promise's fulfillment
      or rejection handler, or rejected if the handler throws an exception.

      ```js
      findUser().then(function (user) {
        return user.name;
      }, function (reason) {
        return 'default name';
      }).then(function (userName) {
        // If `findUser` fulfilled, `userName` will be the user's name, otherwise it
        // will be `'default name'`
      });

      findUser().then(function (user) {
        throw new Error('Found user, but still unhappy');
      }, function (reason) {
        throw new Error('`findUser` rejected and we're unhappy');
      }).then(function (value) {
        // never reached
      }, function (reason) {
        // if `findUser` fulfilled, `reason` will be 'Found user, but still unhappy'.
        // If `findUser` rejected, `reason` will be '`findUser` rejected and we're unhappy'.
      });
      ```
      If the downstream promise does not specify a rejection handler, rejection reasons will be propagated further downstream.

      ```js
      findUser().then(function (user) {
        throw new PedagogicalException('Upstream error');
      }).then(function (value) {
        // never reached
      }).then(function (value) {
        // never reached
      }, function (reason) {
        // The `PedgagocialException` is propagated all the way down to here
      });
      ```

      Assimilation
      ------------

      Sometimes the value you want to propagate to a downstream promise can only be
      retrieved asynchronously. This can be achieved by returning a promise in the
      fulfillment or rejection handler. The downstream promise will then be pending
      until the returned promise is settled. This is called *assimilation*.

      ```js
      findUser().then(function (user) {
        return findCommentsByAuthor(user);
      }).then(function (comments) {
        // The user's comments are now available
      });
      ```

      If the assimliated promise rejects, then the downstream promise will also reject.

      ```js
      findUser().then(function (user) {
        return findCommentsByAuthor(user);
      }).then(function (comments) {
        // If `findCommentsByAuthor` fulfills, we'll have the value here
      }, function (reason) {
        // If `findCommentsByAuthor` rejects, we'll have the reason here
      });
      ```

      Simple Example
      --------------

      Synchronous Example

      ```javascript
      var result;

      try {
        result = findResult();
        // success
      } catch(reason) {
        // failure
      }
      ```

      Errback Example

      ```js
      findResult(function(result, err){
        if (err) {
          // failure
        } else {
          // success
        }
      });
      ```

      Promise Example;

      ```javascript
      findResult().then(function(result){
        // success
      }, function(reason){
        // failure
      });
      ```

      Advanced Example
      --------------

      Synchronous Example

      ```javascript
      var author, books;

      try {
        author = findAuthor();
        books  = findBooksByAuthor(author);
        // success
      } catch(reason) {
        // failure
      }
      ```

      Errback Example

      ```js

      function foundBooks(books) {

      }

      function failure(reason) {

      }

      findAuthor(function(author, err){
        if (err) {
          failure(err);
          // failure
        } else {
          try {
            findBoooksByAuthor(author, function(books, err) {
              if (err) {
                failure(err);
              } else {
                try {
                  foundBooks(books);
                } catch(reason) {
                  failure(reason);
                }
              }
            });
          } catch(error) {
            failure(err);
          }
          // success
        }
      });
      ```

      Promise Example;

      ```javascript
      findAuthor().
        then(findBooksByAuthor).
        then(function(books){
          // found books
      }).catch(function(reason){
        // something went wrong
      });
      ```

      @method then
      @param {Function} onFulfilled
      @param {Function} onRejected
      Useful for tooling.
      @return {Promise}
    */
      then: function(onFulfillment, onRejection) {
        var parent = this;
        var state = parent._state;

        if (state === lib$es6$promise$$internal$$FULFILLED && !onFulfillment || state === lib$es6$promise$$internal$$REJECTED && !onRejection) {
          return this;
        }

        var child = new this.constructor(lib$es6$promise$$internal$$noop);
        var result = parent._result;

        if (state) {
          var callback = arguments[state - 1];
          lib$es6$promise$asap$$asap(function(){
            lib$es6$promise$$internal$$invokeCallback(state, child, callback, result);
          });
        } else {
          lib$es6$promise$$internal$$subscribe(parent, child, onFulfillment, onRejection);
        }

        return child;
      },

    /**
      `catch` is simply sugar for `then(undefined, onRejection)` which makes it the same
      as the catch block of a try/catch statement.

      ```js
      function findAuthor(){
        throw new Error('couldn't find that author');
      }

      // synchronous
      try {
        findAuthor();
      } catch(reason) {
        // something went wrong
      }

      // async with promises
      findAuthor().catch(function(reason){
        // something went wrong
      });
      ```

      @method catch
      @param {Function} onRejection
      Useful for tooling.
      @return {Promise}
    */
      'catch': function(onRejection) {
        return this.then(null, onRejection);
      }
    };
    function lib$es6$promise$polyfill$$polyfill() {
      var local;

      if (typeof global !== 'undefined') {
          local = global;
      } else if (typeof self !== 'undefined') {
          local = self;
      } else {
          try {
              local = Function('return this')();
          } catch (e) {
              throw new Error('polyfill failed because global object is unavailable in this environment');
          }
      }

      var P = local.Promise;

      if (P && Object.prototype.toString.call(P.resolve()) === '[object Promise]' && !P.cast) {
        return;
      }

      local.Promise = lib$es6$promise$promise$$default;
    }
    var lib$es6$promise$polyfill$$default = lib$es6$promise$polyfill$$polyfill;

    var lib$es6$promise$umd$$ES6Promise = {
      'Promise': lib$es6$promise$promise$$default,
      'polyfill': lib$es6$promise$polyfill$$default
    };

    /* global define:true module:true window: true */
    if (typeof define === 'function' && define['amd']) {
      define(function() { return lib$es6$promise$umd$$ES6Promise; });
    } else if (typeof module !== 'undefined' && module['exports']) {
      module['exports'] = lib$es6$promise$umd$$ES6Promise;
    } else if (typeof this !== 'undefined') {
      this['ES6Promise'] = lib$es6$promise$umd$$ES6Promise;
    }

    lib$es6$promise$polyfill$$default();
}).call(this);


(function() {
  'use strict';

  if (self.fetch) {
    return
  }

  function normalizeName(name) {
    if (typeof name !== 'string') {
      name = String(name)
    }
    if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
      throw new TypeError('Invalid character in header field name')
    }
    return name.toLowerCase()
  }

  function normalizeValue(value) {
    if (typeof value !== 'string') {
      value = String(value)
    }
    return value
  }

  function Headers(headers) {
    this.map = {}

    if (headers instanceof Headers) {
      headers.forEach(function(value, name) {
        this.append(name, value)
      }, this)

    } else if (headers) {
      Object.getOwnPropertyNames(headers).forEach(function(name) {
        this.append(name, headers[name])
      }, this)
    }
  }

  Headers.prototype.append = function(name, value) {
    name = normalizeName(name)
    value = normalizeValue(value)
    var list = this.map[name]
    if (!list) {
      list = []
      this.map[name] = list
    }
    list.push(value)
  }

  Headers.prototype['delete'] = function(name) {
    delete this.map[normalizeName(name)]
  }

  Headers.prototype.get = function(name) {
    var values = this.map[normalizeName(name)]
    return values ? values[0] : null
  }

  Headers.prototype.getAll = function(name) {
    return this.map[normalizeName(name)] || []
  }

  Headers.prototype.has = function(name) {
    return this.map.hasOwnProperty(normalizeName(name))
  }

  Headers.prototype.set = function(name, value) {
    this.map[normalizeName(name)] = [normalizeValue(value)]
  }

  Headers.prototype.forEach = function(callback, thisArg) {
    Object.getOwnPropertyNames(this.map).forEach(function(name) {
      this.map[name].forEach(function(value) {
        callback.call(thisArg, value, name, this)
      }, this)
    }, this)
  }

  function consumed(body) {
    if (body.bodyUsed) {
      return Promise.reject(new TypeError('Already read'))
    }
    body.bodyUsed = true
  }

  function fileReaderReady(reader) {
    return new Promise(function(resolve, reject) {
      reader.onload = function() {
        resolve(reader.result)
      }
      reader.onerror = function() {
        reject(reader.error)
      }
    })
  }

  function readBlobAsArrayBuffer(blob) {
    var reader = new FileReader()
    reader.readAsArrayBuffer(blob)
    return fileReaderReady(reader)
  }

  function readBlobAsText(blob) {
    var reader = new FileReader()
    reader.readAsText(blob)
    return fileReaderReady(reader)
  }

  var support = {
    blob: 'FileReader' in self && 'Blob' in self && (function() {
      try {
        new Blob();
        return true
      } catch(e) {
        return false
      }
    })(),
    formData: 'FormData' in self,
    arrayBuffer: 'ArrayBuffer' in self
  }

  function Body() {
    this.bodyUsed = false


    this._initBody = function(body) {
      this._bodyInit = body
      if (typeof body === 'string') {
        this._bodyText = body
      } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
        this._bodyBlob = body
      } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
        this._bodyFormData = body
      } else if (!body) {
        this._bodyText = ''
      } else if (support.arrayBuffer && ArrayBuffer.prototype.isPrototypeOf(body)) {
        // Only support ArrayBuffers for POST method.
        // Receiving ArrayBuffers happens via Blobs, instead.
      } else {
        throw new Error('unsupported BodyInit type')
      }
    }

    if (support.blob) {
      this.blob = function() {
        var rejected = consumed(this)
        if (rejected) {
          return rejected
        }

        if (this._bodyBlob) {
          return Promise.resolve(this._bodyBlob)
        } else if (this._bodyFormData) {
          throw new Error('could not read FormData body as blob')
        } else {
          return Promise.resolve(new Blob([this._bodyText]))
        }
      }

      this.arrayBuffer = function() {
        return this.blob().then(readBlobAsArrayBuffer)
      }

      this.text = function() {
        var rejected = consumed(this)
        if (rejected) {
          return rejected
        }

        if (this._bodyBlob) {
          return readBlobAsText(this._bodyBlob)
        } else if (this._bodyFormData) {
          throw new Error('could not read FormData body as text')
        } else {
          return Promise.resolve(this._bodyText)
        }
      }
    } else {
      this.text = function() {
        var rejected = consumed(this)
        return rejected ? rejected : Promise.resolve(this._bodyText)
      }
    }

    if (support.formData) {
      this.formData = function() {
        return this.text().then(decode)
      }
    }

    this.json = function() {
      return this.text().then(JSON.parse)
    }

    return this
  }

  // HTTP methods whose capitalization should be normalized
  var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT']

  function normalizeMethod(method) {
    var upcased = method.toUpperCase()
    return (methods.indexOf(upcased) > -1) ? upcased : method
  }

  function Request(input, options) {
    options = options || {}
    var body = options.body
    if (Request.prototype.isPrototypeOf(input)) {
      if (input.bodyUsed) {
        throw new TypeError('Already read')
      }
      this.url = input.url
      this.credentials = input.credentials
      if (!options.headers) {
        this.headers = new Headers(input.headers)
      }
      this.method = input.method
      this.mode = input.mode
      if (!body) {
        body = input._bodyInit
        input.bodyUsed = true
      }
    } else {
      this.url = input
    }

    this.credentials = options.credentials || this.credentials || 'omit'
    if (options.headers || !this.headers) {
      this.headers = new Headers(options.headers)
    }
    this.method = normalizeMethod(options.method || this.method || 'GET')
    this.mode = options.mode || this.mode || null
    this.referrer = null

    if ((this.method === 'GET' || this.method === 'HEAD') && body) {
      throw new TypeError('Body not allowed for GET or HEAD requests')
    }
    this._initBody(body)
  }

  Request.prototype.clone = function() {
    return new Request(this)
  }

  function decode(body) {
    var form = new FormData()
    body.trim().split('&').forEach(function(bytes) {
      if (bytes) {
        var split = bytes.split('=')
        var name = split.shift().replace(/\+/g, ' ')
        var value = split.join('=').replace(/\+/g, ' ')
        form.append(decodeURIComponent(name), decodeURIComponent(value))
      }
    })
    return form
  }

  function headers(xhr) {
    var head = new Headers()
    var pairs = xhr.getAllResponseHeaders().trim().split('\n')
    pairs.forEach(function(header) {
      var split = header.trim().split(':')
      var key = split.shift().trim()
      var value = split.join(':').trim()
      head.append(key, value)
    })
    return head
  }

  Body.call(Request.prototype)

  function Response(bodyInit, options) {
    if (!options) {
      options = {}
    }

    this._initBody(bodyInit)
    this.type = 'default'
    this.status = options.status
    this.ok = this.status >= 200 && this.status < 300
    this.statusText = options.statusText
    this.headers = options.headers instanceof Headers ? options.headers : new Headers(options.headers)
    this.url = options.url || ''
  }

  Body.call(Response.prototype)

  Response.prototype.clone = function() {
    return new Response(this._bodyInit, {
      status: this.status,
      statusText: this.statusText,
      headers: new Headers(this.headers),
      url: this.url
    })
  }

  Response.error = function() {
    var response = new Response(null, {status: 0, statusText: ''})
    response.type = 'error'
    return response
  }

  var redirectStatuses = [301, 302, 303, 307, 308]

  Response.redirect = function(url, status) {
    if (redirectStatuses.indexOf(status) === -1) {
      throw new RangeError('Invalid status code')
    }

    return new Response(null, {status: status, headers: {location: url}})
  }

  self.Headers = Headers;
  self.Request = Request;
  self.Response = Response;

  self.fetch = function(input, init) {
    return new Promise(function(resolve, reject) {
      var request
      if (Request.prototype.isPrototypeOf(input) && !init) {
        request = input
      } else {
        request = new Request(input, init)
      }

      var xhr = new XMLHttpRequest()

      function responseURL() {
        if ('responseURL' in xhr) {
          return xhr.responseURL
        }

        // Avoid security warnings on getResponseHeader when not allowed by CORS
        if (/^X-Request-URL:/m.test(xhr.getAllResponseHeaders())) {
          return xhr.getResponseHeader('X-Request-URL')
        }

        return;
      }

      xhr.onload = function() {
        var status = (xhr.status === 1223) ? 204 : xhr.status
        if (status < 100 || status > 599) {
          reject(new TypeError('Network request failed'))
          return
        }
        var options = {
          status: status,
          statusText: xhr.statusText,
          headers: headers(xhr),
          url: responseURL()
        }
        var body = 'response' in xhr ? xhr.response : xhr.responseText;
        resolve(new Response(body, options))
      }

      xhr.onerror = function() {
        reject(new TypeError('Network request failed'))
      }

      xhr.open(request.method, request.url, true)

      if (request.credentials === 'include') {
        xhr.withCredentials = true
      }

      if ('responseType' in xhr && support.blob) {
        xhr.responseType = 'blob'
      }

      request.headers.forEach(function(value, name) {
        xhr.setRequestHeader(name, value)
      })

      xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit)
    })
  }
  self.fetch.polyfill = true
})();

(function() {
  'use strict';

  var globals = typeof window === 'undefined' ? global : window;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};
  var aliases = {};
  var has = ({}).hasOwnProperty;

  var endsWith = function(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  };

  var _cmp = 'components/';
  var unalias = function(alias, loaderPath) {
    var start = 0;
    if (loaderPath) {
      if (loaderPath.indexOf(_cmp) === 0) {
        start = _cmp.length;
      }
      if (loaderPath.indexOf('/', start) > 0) {
        loaderPath = loaderPath.substring(start, loaderPath.indexOf('/', start));
      }
    }
    var result = aliases[alias + '/index.js'] || aliases[loaderPath + '/deps/' + alias + '/index.js'];
    if (result) {
      return _cmp + result.substring(0, result.length - '.js'.length);
    }
    return alias;
  };

  var _reg = /^\.\.?(\/|$)/;
  var expand = function(root, name) {
    var results = [], part;
    var parts = (_reg.test(name) ? root + '/' + name : name).split('/');
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function expanded(name) {
      var absolute = expand(dirname(path), name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';
    path = unalias(name, loaderPath);

    if (has.call(cache, path)) return cache[path].exports;
    if (has.call(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has.call(cache, dirIndex)) return cache[dirIndex].exports;
    if (has.call(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  require.alias = function(from, to) {
    aliases[to] = from;
  };

  require.register = require.define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has.call(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  require.list = function() {
    var result = [];
    for (var item in modules) {
      if (has.call(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  require.brunch = true;
  require._cache = cache;
  globals.require = require;
})();

require.define({"type-detect": function(exports, require, module) {
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.typeDetect = factory());
}(this, (function () { 'use strict';

/* !
 * type-detect
 * Copyright(c) 2013 jake luer <jake@alogicalparadox.com>
 * MIT Licensed
 */
var promiseExists = typeof Promise === 'function';

/* eslint-disable no-undef */
var globalObject = typeof self === 'object' ? self : global; // eslint-disable-line id-blacklist

/*
 * All of these attributes must be available on the global object for the current environment
 * to be considered a DOM environment (browser)
 */
var isDom = typeof window === 'object' &&
  'document' in window &&
  'navigator' in window &&
  'HTMLElement' in window;
/* eslint-enable */

var symbolExists = typeof Symbol !== 'undefined';
var mapExists = typeof Map !== 'undefined';
var setExists = typeof Set !== 'undefined';
var weakMapExists = typeof WeakMap !== 'undefined';
var weakSetExists = typeof WeakSet !== 'undefined';
var dataViewExists = typeof DataView !== 'undefined';
var symbolIteratorExists = symbolExists && typeof Symbol.iterator !== 'undefined';
var symbolToStringTagExists = symbolExists && typeof Symbol.toStringTag !== 'undefined';
var setEntriesExists = setExists && typeof Set.prototype.entries === 'function';
var mapEntriesExists = mapExists && typeof Map.prototype.entries === 'function';
var setIteratorPrototype = setEntriesExists && Object.getPrototypeOf(new Set().entries());
var mapIteratorPrototype = mapEntriesExists && Object.getPrototypeOf(new Map().entries());
var arrayIteratorExists = symbolIteratorExists && typeof Array.prototype[Symbol.iterator] === 'function';
var arrayIteratorPrototype = arrayIteratorExists && Object.getPrototypeOf([][Symbol.iterator]());
var stringIteratorExists = symbolIteratorExists && typeof String.prototype[Symbol.iterator] === 'function';
var stringIteratorPrototype = stringIteratorExists && Object.getPrototypeOf(''[Symbol.iterator]());
var toStringLeftSliceLength = 8;
var toStringRightSliceLength = -1;
/**
 * ### typeOf (obj)
 *
 * Uses `Object.prototype.toString` to determine the type of an object,
 * normalising behaviour across engine versions & well optimised.
 *
 * @param {Mixed} object
 * @return {String} object type
 * @api public
 */
function typeDetect(obj) {
  /* ! Speed optimisation
   * Pre:
   *   string literal     x 3,039,035 ops/sec ±1.62% (78 runs sampled)
   *   boolean literal    x 1,424,138 ops/sec ±4.54% (75 runs sampled)
   *   number literal     x 1,653,153 ops/sec ±1.91% (82 runs sampled)
   *   undefined          x 9,978,660 ops/sec ±1.92% (75 runs sampled)
   *   function           x 2,556,769 ops/sec ±1.73% (77 runs sampled)
   * Post:
   *   string literal     x 38,564,796 ops/sec ±1.15% (79 runs sampled)
   *   boolean literal    x 31,148,940 ops/sec ±1.10% (79 runs sampled)
   *   number literal     x 32,679,330 ops/sec ±1.90% (78 runs sampled)
   *   undefined          x 32,363,368 ops/sec ±1.07% (82 runs sampled)
   *   function           x 31,296,870 ops/sec ±0.96% (83 runs sampled)
   */
  var typeofObj = typeof obj;
  if (typeofObj !== 'object') {
    return typeofObj;
  }

  /* ! Speed optimisation
   * Pre:
   *   null               x 28,645,765 ops/sec ±1.17% (82 runs sampled)
   * Post:
   *   null               x 36,428,962 ops/sec ±1.37% (84 runs sampled)
   */
  if (obj === null) {
    return 'null';
  }

  /* ! Spec Conformance
   * Test: `Object.prototype.toString.call(window)``
   *  - Node === "[object global]"
   *  - Chrome === "[object global]"
   *  - Firefox === "[object Window]"
   *  - PhantomJS === "[object Window]"
   *  - Safari === "[object Window]"
   *  - IE 11 === "[object Window]"
   *  - IE Edge === "[object Window]"
   * Test: `Object.prototype.toString.call(this)``
   *  - Chrome Worker === "[object global]"
   *  - Firefox Worker === "[object DedicatedWorkerGlobalScope]"
   *  - Safari Worker === "[object DedicatedWorkerGlobalScope]"
   *  - IE 11 Worker === "[object WorkerGlobalScope]"
   *  - IE Edge Worker === "[object WorkerGlobalScope]"
   */
  if (obj === globalObject) {
    return 'global';
  }

  /* ! Speed optimisation
   * Pre:
   *   array literal      x 2,888,352 ops/sec ±0.67% (82 runs sampled)
   * Post:
   *   array literal      x 22,479,650 ops/sec ±0.96% (81 runs sampled)
   */
  if (
    Array.isArray(obj) &&
    (symbolToStringTagExists === false || !(Symbol.toStringTag in obj))
  ) {
    return 'Array';
  }

  if (isDom) {
    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/browsers.html#location)
     * WhatWG HTML$7.7.3 - The `Location` interface
     * Test: `Object.prototype.toString.call(window.location)``
     *  - IE <=11 === "[object Object]"
     *  - IE Edge <=13 === "[object Object]"
     */
    if (obj === globalObject.location) {
      return 'Location';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/#document)
     * WhatWG HTML$3.1.1 - The `Document` object
     * Note: Most browsers currently adher to the W3C DOM Level 2 spec
     *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-26809268)
     *       which suggests that browsers should use HTMLTableCellElement for
     *       both TD and TH elements. WhatWG separates these.
     *       WhatWG HTML states:
     *         > For historical reasons, Window objects must also have a
     *         > writable, configurable, non-enumerable property named
     *         > HTMLDocument whose value is the Document interface object.
     * Test: `Object.prototype.toString.call(document)``
     *  - Chrome === "[object HTMLDocument]"
     *  - Firefox === "[object HTMLDocument]"
     *  - Safari === "[object HTMLDocument]"
     *  - IE <=10 === "[object Document]"
     *  - IE 11 === "[object HTMLDocument]"
     *  - IE Edge <=13 === "[object HTMLDocument]"
     */
    if (obj === globalObject.document) {
      return 'Document';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/webappapis.html#mimetypearray)
     * WhatWG HTML$8.6.1.5 - Plugins - Interface MimeTypeArray
     * Test: `Object.prototype.toString.call(navigator.mimeTypes)``
     *  - IE <=10 === "[object MSMimeTypesCollection]"
     */
    if (obj === (globalObject.navigator || {}).mimeTypes) {
      return 'MimeTypeArray';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/webappapis.html#pluginarray)
     * WhatWG HTML$8.6.1.5 - Plugins - Interface PluginArray
     * Test: `Object.prototype.toString.call(navigator.plugins)``
     *  - IE <=10 === "[object MSPluginsCollection]"
     */
    if (obj === (globalObject.navigator || {}).plugins) {
      return 'PluginArray';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/webappapis.html#pluginarray)
     * WhatWG HTML$4.4.4 - The `blockquote` element - Interface `HTMLQuoteElement`
     * Test: `Object.prototype.toString.call(document.createElement('blockquote'))``
     *  - IE <=10 === "[object HTMLBlockElement]"
     */
    if (obj instanceof globalObject.HTMLElement && obj.tagName === 'BLOCKQUOTE') {
      return 'HTMLQuoteElement';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/#htmltabledatacellelement)
     * WhatWG HTML$4.9.9 - The `td` element - Interface `HTMLTableDataCellElement`
     * Note: Most browsers currently adher to the W3C DOM Level 2 spec
     *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-82915075)
     *       which suggests that browsers should use HTMLTableCellElement for
     *       both TD and TH elements. WhatWG separates these.
     * Test: Object.prototype.toString.call(document.createElement('td'))
     *  - Chrome === "[object HTMLTableCellElement]"
     *  - Firefox === "[object HTMLTableCellElement]"
     *  - Safari === "[object HTMLTableCellElement]"
     */
    if (obj instanceof globalObject.HTMLElement && obj.tagName === 'TD') {
      return 'HTMLTableDataCellElement';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/#htmltableheadercellelement)
     * WhatWG HTML$4.9.9 - The `td` element - Interface `HTMLTableHeaderCellElement`
     * Note: Most browsers currently adher to the W3C DOM Level 2 spec
     *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-82915075)
     *       which suggests that browsers should use HTMLTableCellElement for
     *       both TD and TH elements. WhatWG separates these.
     * Test: Object.prototype.toString.call(document.createElement('th'))
     *  - Chrome === "[object HTMLTableCellElement]"
     *  - Firefox === "[object HTMLTableCellElement]"
     *  - Safari === "[object HTMLTableCellElement]"
     */
    if (obj instanceof globalObject.HTMLElement && obj.tagName === 'TH') {
      return 'HTMLTableHeaderCellElement';
    }
  }

  /* ! Speed optimisation
  * Pre:
  *   Float64Array       x 625,644 ops/sec ±1.58% (80 runs sampled)
  *   Float32Array       x 1,279,852 ops/sec ±2.91% (77 runs sampled)
  *   Uint32Array        x 1,178,185 ops/sec ±1.95% (83 runs sampled)
  *   Uint16Array        x 1,008,380 ops/sec ±2.25% (80 runs sampled)
  *   Uint8Array         x 1,128,040 ops/sec ±2.11% (81 runs sampled)
  *   Int32Array         x 1,170,119 ops/sec ±2.88% (80 runs sampled)
  *   Int16Array         x 1,176,348 ops/sec ±5.79% (86 runs sampled)
  *   Int8Array          x 1,058,707 ops/sec ±4.94% (77 runs sampled)
  *   Uint8ClampedArray  x 1,110,633 ops/sec ±4.20% (80 runs sampled)
  * Post:
  *   Float64Array       x 7,105,671 ops/sec ±13.47% (64 runs sampled)
  *   Float32Array       x 5,887,912 ops/sec ±1.46% (82 runs sampled)
  *   Uint32Array        x 6,491,661 ops/sec ±1.76% (79 runs sampled)
  *   Uint16Array        x 6,559,795 ops/sec ±1.67% (82 runs sampled)
  *   Uint8Array         x 6,463,966 ops/sec ±1.43% (85 runs sampled)
  *   Int32Array         x 5,641,841 ops/sec ±3.49% (81 runs sampled)
  *   Int16Array         x 6,583,511 ops/sec ±1.98% (80 runs sampled)
  *   Int8Array          x 6,606,078 ops/sec ±1.74% (81 runs sampled)
  *   Uint8ClampedArray  x 6,602,224 ops/sec ±1.77% (83 runs sampled)
  */
  var stringTag = (symbolToStringTagExists && obj[Symbol.toStringTag]);
  if (typeof stringTag === 'string') {
    return stringTag;
  }

  var objPrototype = Object.getPrototypeOf(obj);
  /* ! Speed optimisation
  * Pre:
  *   regex literal      x 1,772,385 ops/sec ±1.85% (77 runs sampled)
  *   regex constructor  x 2,143,634 ops/sec ±2.46% (78 runs sampled)
  * Post:
  *   regex literal      x 3,928,009 ops/sec ±0.65% (78 runs sampled)
  *   regex constructor  x 3,931,108 ops/sec ±0.58% (84 runs sampled)
  */
  if (objPrototype === RegExp.prototype) {
    return 'RegExp';
  }

  /* ! Speed optimisation
  * Pre:
  *   date               x 2,130,074 ops/sec ±4.42% (68 runs sampled)
  * Post:
  *   date               x 3,953,779 ops/sec ±1.35% (77 runs sampled)
  */
  if (objPrototype === Date.prototype) {
    return 'Date';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-promise.prototype-@@tostringtag)
   * ES6$25.4.5.4 - Promise.prototype[@@toStringTag] should be "Promise":
   * Test: `Object.prototype.toString.call(Promise.resolve())``
   *  - Chrome <=47 === "[object Object]"
   *  - Edge <=20 === "[object Object]"
   *  - Firefox 29-Latest === "[object Promise]"
   *  - Safari 7.1-Latest === "[object Promise]"
   */
  if (promiseExists && objPrototype === Promise.prototype) {
    return 'Promise';
  }

  /* ! Speed optimisation
  * Pre:
  *   set                x 2,222,186 ops/sec ±1.31% (82 runs sampled)
  * Post:
  *   set                x 4,545,879 ops/sec ±1.13% (83 runs sampled)
  */
  if (setExists && objPrototype === Set.prototype) {
    return 'Set';
  }

  /* ! Speed optimisation
  * Pre:
  *   map                x 2,396,842 ops/sec ±1.59% (81 runs sampled)
  * Post:
  *   map                x 4,183,945 ops/sec ±6.59% (82 runs sampled)
  */
  if (mapExists && objPrototype === Map.prototype) {
    return 'Map';
  }

  /* ! Speed optimisation
  * Pre:
  *   weakset            x 1,323,220 ops/sec ±2.17% (76 runs sampled)
  * Post:
  *   weakset            x 4,237,510 ops/sec ±2.01% (77 runs sampled)
  */
  if (weakSetExists && objPrototype === WeakSet.prototype) {
    return 'WeakSet';
  }

  /* ! Speed optimisation
  * Pre:
  *   weakmap            x 1,500,260 ops/sec ±2.02% (78 runs sampled)
  * Post:
  *   weakmap            x 3,881,384 ops/sec ±1.45% (82 runs sampled)
  */
  if (weakMapExists && objPrototype === WeakMap.prototype) {
    return 'WeakMap';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-dataview.prototype-@@tostringtag)
   * ES6$24.2.4.21 - DataView.prototype[@@toStringTag] should be "DataView":
   * Test: `Object.prototype.toString.call(new DataView(new ArrayBuffer(1)))``
   *  - Edge <=13 === "[object Object]"
   */
  if (dataViewExists && objPrototype === DataView.prototype) {
    return 'DataView';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%mapiteratorprototype%-@@tostringtag)
   * ES6$23.1.5.2.2 - %MapIteratorPrototype%[@@toStringTag] should be "Map Iterator":
   * Test: `Object.prototype.toString.call(new Map().entries())``
   *  - Edge <=13 === "[object Object]"
   */
  if (mapExists && objPrototype === mapIteratorPrototype) {
    return 'Map Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%setiteratorprototype%-@@tostringtag)
   * ES6$23.2.5.2.2 - %SetIteratorPrototype%[@@toStringTag] should be "Set Iterator":
   * Test: `Object.prototype.toString.call(new Set().entries())``
   *  - Edge <=13 === "[object Object]"
   */
  if (setExists && objPrototype === setIteratorPrototype) {
    return 'Set Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%arrayiteratorprototype%-@@tostringtag)
   * ES6$22.1.5.2.2 - %ArrayIteratorPrototype%[@@toStringTag] should be "Array Iterator":
   * Test: `Object.prototype.toString.call([][Symbol.iterator]())``
   *  - Edge <=13 === "[object Object]"
   */
  if (arrayIteratorExists && objPrototype === arrayIteratorPrototype) {
    return 'Array Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%stringiteratorprototype%-@@tostringtag)
   * ES6$21.1.5.2.2 - %StringIteratorPrototype%[@@toStringTag] should be "String Iterator":
   * Test: `Object.prototype.toString.call(''[Symbol.iterator]())``
   *  - Edge <=13 === "[object Object]"
   */
  if (stringIteratorExists && objPrototype === stringIteratorPrototype) {
    return 'String Iterator';
  }

  /* ! Speed optimisation
  * Pre:
  *   object from null   x 2,424,320 ops/sec ±1.67% (76 runs sampled)
  * Post:
  *   object from null   x 5,838,000 ops/sec ±0.99% (84 runs sampled)
  */
  if (objPrototype === null) {
    return 'Object';
  }

  return Object
    .prototype
    .toString
    .call(obj)
    .slice(toStringLeftSliceLength, toStringRightSliceLength);
}

return typeDetect;

})));

}});

require.define({"deep-eql": function(exports, require, module) {
(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.deepEqual = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* globals Symbol: false, Uint8Array: false, WeakMap: false */
/*!
 * deep-eql
 * Copyright(c) 2013 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var type = require('type-detect');
function FakeMap() {
  this._key = 'chai/deep-eql__' + Math.random() + Date.now();
}

FakeMap.prototype = {
  get: function getMap(key) {
    return key[this._key];
  },
  set: function setMap(key, value) {
    if (Object.isExtensible(key)) {
      Object.defineProperty(key, this._key, {
        value: value,
        configurable: true,
      });
    }
  },
};

var MemoizeMap = typeof WeakMap === 'function' ? WeakMap : FakeMap;
/*!
 * Check to see if the MemoizeMap has recorded a result of the two operands
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {MemoizeMap} memoizeMap
 * @returns {Boolean|null} result
*/
function memoizeCompare(leftHandOperand, rightHandOperand, memoizeMap) {
  // Technically, WeakMap keys can *only* be objects, not primitives.
  if (!memoizeMap || isPrimitive(leftHandOperand) || isPrimitive(rightHandOperand)) {
    return null;
  }
  var leftHandMap = memoizeMap.get(leftHandOperand);
  if (leftHandMap) {
    var result = leftHandMap.get(rightHandOperand);
    if (typeof result === 'boolean') {
      return result;
    }
  }
  return null;
}

/*!
 * Set the result of the equality into the MemoizeMap
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {MemoizeMap} memoizeMap
 * @param {Boolean} result
*/
function memoizeSet(leftHandOperand, rightHandOperand, memoizeMap, result) {
  // Technically, WeakMap keys can *only* be objects, not primitives.
  if (!memoizeMap || isPrimitive(leftHandOperand) || isPrimitive(rightHandOperand)) {
    return;
  }
  var leftHandMap = memoizeMap.get(leftHandOperand);
  if (leftHandMap) {
    leftHandMap.set(rightHandOperand, result);
  } else {
    leftHandMap = new MemoizeMap();
    leftHandMap.set(rightHandOperand, result);
    memoizeMap.set(leftHandOperand, leftHandMap);
  }
}

/*!
 * Primary Export
 */

module.exports = deepEqual;
module.exports.MemoizeMap = MemoizeMap;

/**
 * Assert deeply nested sameValue equality between two objects of any type.
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Object} [options] (optional) Additional options
 * @param {Array} [options.comparator] (optional) Override default algorithm, determining custom equality.
 * @param {Array} [options.memoize] (optional) Provide a custom memoization object which will cache the results of
    complex objects for a speed boost. By passing `false` you can disable memoization, but this will cause circular
    references to blow the stack.
 * @return {Boolean} equal match
 */
function deepEqual(leftHandOperand, rightHandOperand, options) {
  // If we have a comparator, we can't assume anything; so bail to its check first.
  if (options && options.comparator) {
    return extensiveDeepEqual(leftHandOperand, rightHandOperand, options);
  }

  var simpleResult = simpleEqual(leftHandOperand, rightHandOperand);
  if (simpleResult !== null) {
    return simpleResult;
  }

  // Deeper comparisons are pushed through to a larger function
  return extensiveDeepEqual(leftHandOperand, rightHandOperand, options);
}

/**
 * Many comparisons can be canceled out early via simple equality or primitive checks.
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @return {Boolean|null} equal match
 */
function simpleEqual(leftHandOperand, rightHandOperand) {
  // Equal references (except for Numbers) can be returned early
  if (leftHandOperand === rightHandOperand) {
    // Handle +-0 cases
    return leftHandOperand !== 0 || 1 / leftHandOperand === 1 / rightHandOperand;
  }

  // handle NaN cases
  if (
    leftHandOperand !== leftHandOperand && // eslint-disable-line no-self-compare
    rightHandOperand !== rightHandOperand // eslint-disable-line no-self-compare
  ) {
    return true;
  }

  // Anything that is not an 'object', i.e. symbols, functions, booleans, numbers,
  // strings, and undefined, can be compared by reference.
  if (isPrimitive(leftHandOperand) || isPrimitive(rightHandOperand)) {
    // Easy out b/c it would have passed the first equality check
    return false;
  }
  return null;
}

/*!
 * The main logic of the `deepEqual` function.
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Object} [options] (optional) Additional options
 * @param {Array} [options.comparator] (optional) Override default algorithm, determining custom equality.
 * @param {Array} [options.memoize] (optional) Provide a custom memoization object which will cache the results of
    complex objects for a speed boost. By passing `false` you can disable memoization, but this will cause circular
    references to blow the stack.
 * @return {Boolean} equal match
*/
function extensiveDeepEqual(leftHandOperand, rightHandOperand, options) {
  options = options || {};
  options.memoize = options.memoize === false ? false : options.memoize || new MemoizeMap();
  var comparator = options && options.comparator;

  // Check if a memoized result exists.
  var memoizeResultLeft = memoizeCompare(leftHandOperand, rightHandOperand, options.memoize);
  if (memoizeResultLeft !== null) {
    return memoizeResultLeft;
  }
  var memoizeResultRight = memoizeCompare(rightHandOperand, leftHandOperand, options.memoize);
  if (memoizeResultRight !== null) {
    return memoizeResultRight;
  }

  // If a comparator is present, use it.
  if (comparator) {
    var comparatorResult = comparator(leftHandOperand, rightHandOperand);
    // Comparators may return null, in which case we want to go back to default behavior.
    if (comparatorResult === false || comparatorResult === true) {
      memoizeSet(leftHandOperand, rightHandOperand, options.memoize, comparatorResult);
      return comparatorResult;
    }
    // To allow comparators to override *any* behavior, we ran them first. Since it didn't decide
    // what to do, we need to make sure to return the basic tests first before we move on.
    var simpleResult = simpleEqual(leftHandOperand, rightHandOperand);
    if (simpleResult !== null) {
      // Don't memoize this, it takes longer to set/retrieve than to just compare.
      return simpleResult;
    }
  }

  var leftHandType = type(leftHandOperand);
  if (leftHandType !== type(rightHandOperand)) {
    memoizeSet(leftHandOperand, rightHandOperand, options.memoize, false);
    return false;
  }

  // Temporarily set the operands in the memoize object to prevent blowing the stack
  memoizeSet(leftHandOperand, rightHandOperand, options.memoize, true);

  var result = extensiveDeepEqualByType(leftHandOperand, rightHandOperand, leftHandType, options);
  memoizeSet(leftHandOperand, rightHandOperand, options.memoize, result);
  return result;
}

function extensiveDeepEqualByType(leftHandOperand, rightHandOperand, leftHandType, options) {
  switch (leftHandType) {
    case 'String':
    case 'Number':
    case 'Boolean':
    case 'Date':
      // If these types are their instance types (e.g. `new Number`) then re-deepEqual against their values
      return deepEqual(leftHandOperand.valueOf(), rightHandOperand.valueOf());
    case 'Promise':
    case 'Symbol':
    case 'function':
    case 'WeakMap':
    case 'WeakSet':
    case 'Error':
      return leftHandOperand === rightHandOperand;
    case 'Arguments':
    case 'Int8Array':
    case 'Uint8Array':
    case 'Uint8ClampedArray':
    case 'Int16Array':
    case 'Uint16Array':
    case 'Int32Array':
    case 'Uint32Array':
    case 'Float32Array':
    case 'Float64Array':
    case 'Array':
      return iterableEqual(leftHandOperand, rightHandOperand, options);
    case 'RegExp':
      return regexpEqual(leftHandOperand, rightHandOperand);
    case 'Generator':
      return generatorEqual(leftHandOperand, rightHandOperand, options);
    case 'DataView':
      return iterableEqual(new Uint8Array(leftHandOperand.buffer), new Uint8Array(rightHandOperand.buffer), options);
    case 'ArrayBuffer':
      return iterableEqual(new Uint8Array(leftHandOperand), new Uint8Array(rightHandOperand), options);
    case 'Set':
      return entriesEqual(leftHandOperand, rightHandOperand, options);
    case 'Map':
      return entriesEqual(leftHandOperand, rightHandOperand, options);
    default:
      return objectEqual(leftHandOperand, rightHandOperand, options);
  }
}

/*!
 * Compare two Regular Expressions for equality.
 *
 * @param {RegExp} leftHandOperand
 * @param {RegExp} rightHandOperand
 * @return {Boolean} result
 */

function regexpEqual(leftHandOperand, rightHandOperand) {
  return leftHandOperand.toString() === rightHandOperand.toString();
}

/*!
 * Compare two Sets/Maps for equality. Faster than other equality functions.
 *
 * @param {Set} leftHandOperand
 * @param {Set} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function entriesEqual(leftHandOperand, rightHandOperand, options) {
  // IE11 doesn't support Set#entries or Set#@@iterator, so we need manually populate using Set#forEach
  if (leftHandOperand.size !== rightHandOperand.size) {
    return false;
  }
  if (leftHandOperand.size === 0) {
    return true;
  }
  var leftHandItems = [];
  var rightHandItems = [];
  leftHandOperand.forEach(function gatherEntries(key, value) {
    leftHandItems.push([ key, value ]);
  });
  rightHandOperand.forEach(function gatherEntries(key, value) {
    rightHandItems.push([ key, value ]);
  });
  return iterableEqual(leftHandItems.sort(), rightHandItems.sort(), options);
}

/*!
 * Simple equality for flat iterable objects such as Arrays, TypedArrays or Node.js buffers.
 *
 * @param {Iterable} leftHandOperand
 * @param {Iterable} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function iterableEqual(leftHandOperand, rightHandOperand, options) {
  var length = leftHandOperand.length;
  if (length !== rightHandOperand.length) {
    return false;
  }
  if (length === 0) {
    return true;
  }
  var index = -1;
  while (++index < length) {
    if (deepEqual(leftHandOperand[index], rightHandOperand[index], options) === false) {
      return false;
    }
  }
  return true;
}

/*!
 * Simple equality for generator objects such as those returned by generator functions.
 *
 * @param {Iterable} leftHandOperand
 * @param {Iterable} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function generatorEqual(leftHandOperand, rightHandOperand, options) {
  return iterableEqual(getGeneratorEntries(leftHandOperand), getGeneratorEntries(rightHandOperand), options);
}

/*!
 * Determine if the given object has an @@iterator function.
 *
 * @param {Object} target
 * @return {Boolean} `true` if the object has an @@iterator function.
 */
function hasIteratorFunction(target) {
  return typeof Symbol !== 'undefined' &&
    typeof target === 'object' &&
    typeof Symbol.iterator !== 'undefined' &&
    typeof target[Symbol.iterator] === 'function';
}

/*!
 * Gets all iterator entries from the given Object. If the Object has no @@iterator function, returns an empty array.
 * This will consume the iterator - which could have side effects depending on the @@iterator implementation.
 *
 * @param {Object} target
 * @returns {Array} an array of entries from the @@iterator function
 */
function getIteratorEntries(target) {
  if (hasIteratorFunction(target)) {
    try {
      return getGeneratorEntries(target[Symbol.iterator]());
    } catch (iteratorError) {
      return [];
    }
  }
  return [];
}

/*!
 * Gets all entries from a Generator. This will consume the generator - which could have side effects.
 *
 * @param {Generator} target
 * @returns {Array} an array of entries from the Generator.
 */
function getGeneratorEntries(generator) {
  var generatorResult = generator.next();
  var accumulator = [ generatorResult.value ];
  while (generatorResult.done === false) {
    generatorResult = generator.next();
    accumulator.push(generatorResult.value);
  }
  return accumulator;
}

/*!
 * Gets all own and inherited enumerable keys from a target.
 *
 * @param {Object} target
 * @returns {Array} an array of own and inherited enumerable keys from the target.
 */
function getEnumerableKeys(target) {
  var keys = [];
  for (var key in target) {
    keys.push(key);
  }
  return keys;
}

/*!
 * Determines if two objects have matching values, given a set of keys. Defers to deepEqual for the equality check of
 * each key. If any value of the given key is not equal, the function will return false (early).
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Array} keys An array of keys to compare the values of leftHandOperand and rightHandOperand against
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */
function keysEqual(leftHandOperand, rightHandOperand, keys, options) {
  var length = keys.length;
  if (length === 0) {
    return true;
  }
  for (var i = 0; i < length; i += 1) {
    if (deepEqual(leftHandOperand[keys[i]], rightHandOperand[keys[i]], options) === false) {
      return false;
    }
  }
  return true;
}

/*!
 * Recursively check the equality of two Objects. Once basic sameness has been established it will defer to `deepEqual`
 * for each enumerable key in the object.
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function objectEqual(leftHandOperand, rightHandOperand, options) {
  var leftHandKeys = getEnumerableKeys(leftHandOperand);
  var rightHandKeys = getEnumerableKeys(rightHandOperand);
  if (leftHandKeys.length && leftHandKeys.length === rightHandKeys.length) {
    leftHandKeys.sort();
    rightHandKeys.sort();
    if (iterableEqual(leftHandKeys, rightHandKeys) === false) {
      return false;
    }
    return keysEqual(leftHandOperand, rightHandOperand, leftHandKeys, options);
  }

  var leftHandEntries = getIteratorEntries(leftHandOperand);
  var rightHandEntries = getIteratorEntries(rightHandOperand);
  if (leftHandEntries.length && leftHandEntries.length === rightHandEntries.length) {
    leftHandEntries.sort();
    rightHandEntries.sort();
    return iterableEqual(leftHandEntries, rightHandEntries, options);
  }

  if (leftHandKeys.length === 0 &&
      leftHandEntries.length === 0 &&
      rightHandKeys.length === 0 &&
      rightHandEntries.length === 0) {
    return true;
  }

  return false;
}

/*!
 * Returns true if the argument is a primitive.
 *
 * This intentionally returns true for all objects that can be compared by reference,
 * including functions and symbols.
 *
 * @param {Mixed} value
 * @return {Boolean} result
 */
function isPrimitive(value) {
  return value === null || typeof value !== 'object';
}

},{"type-detect":2}],2:[function(require,module,exports){
(function (global){
'use strict';

/* !
 * type-detect
 * Copyright(c) 2013 jake luer <jake@alogicalparadox.com>
 * MIT Licensed
 */
var promiseExists = typeof Promise === 'function';
var globalObject = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : self; // eslint-disable-line
var isDom = 'location' in globalObject && 'document' in globalObject;
var symbolExists = typeof Symbol !== 'undefined';
var mapExists = typeof Map !== 'undefined';
var setExists = typeof Set !== 'undefined';
var weakMapExists = typeof WeakMap !== 'undefined';
var weakSetExists = typeof WeakSet !== 'undefined';
var dataViewExists = typeof DataView !== 'undefined';
var symbolIteratorExists = symbolExists && typeof Symbol.iterator !== 'undefined';
var symbolToStringTagExists = symbolExists && typeof Symbol.toStringTag !== 'undefined';
var setEntriesExists = setExists && typeof Set.prototype.entries === 'function';
var mapEntriesExists = mapExists && typeof Map.prototype.entries === 'function';
var setIteratorPrototype = setEntriesExists && Object.getPrototypeOf(new Set().entries());
var mapIteratorPrototype = mapEntriesExists && Object.getPrototypeOf(new Map().entries());
var arrayIteratorExists = symbolIteratorExists && typeof Array.prototype[Symbol.iterator] === 'function';
var arrayIteratorPrototype = arrayIteratorExists && Object.getPrototypeOf([][Symbol.iterator]());
var stringIteratorExists = symbolIteratorExists && typeof String.prototype[Symbol.iterator] === 'function';
var stringIteratorPrototype = stringIteratorExists && Object.getPrototypeOf(''[Symbol.iterator]());
var toStringLeftSliceLength = 8;
var toStringRightSliceLength = -1;
/**
 * ### typeOf (obj)
 *
 * Uses `Object.prototype.toString` to determine the type of an object,
 * normalising behaviour across engine versions & well optimised.
 *
 * @param {Mixed} object
 * @return {String} object type
 * @api public
 */
module.exports = function typeDetect(obj) {
  /* ! Speed optimisation
   * Pre:
   *   string literal     x 3,039,035 ops/sec ±1.62% (78 runs sampled)
   *   boolean literal    x 1,424,138 ops/sec ±4.54% (75 runs sampled)
   *   number literal     x 1,653,153 ops/sec ±1.91% (82 runs sampled)
   *   undefined          x 9,978,660 ops/sec ±1.92% (75 runs sampled)
   *   function           x 2,556,769 ops/sec ±1.73% (77 runs sampled)
   * Post:
   *   string literal     x 38,564,796 ops/sec ±1.15% (79 runs sampled)
   *   boolean literal    x 31,148,940 ops/sec ±1.10% (79 runs sampled)
   *   number literal     x 32,679,330 ops/sec ±1.90% (78 runs sampled)
   *   undefined          x 32,363,368 ops/sec ±1.07% (82 runs sampled)
   *   function           x 31,296,870 ops/sec ±0.96% (83 runs sampled)
   */
  var typeofObj = typeof obj;
  if (typeofObj !== 'object') {
    return typeofObj;
  }

  /* ! Speed optimisation
   * Pre:
   *   null               x 28,645,765 ops/sec ±1.17% (82 runs sampled)
   * Post:
   *   null               x 36,428,962 ops/sec ±1.37% (84 runs sampled)
   */
  if (obj === null) {
    return 'null';
  }

  /* ! Spec Conformance
   * Test: `Object.prototype.toString.call(window)``
   *  - Node === "[object global]"
   *  - Chrome === "[object global]"
   *  - Firefox === "[object Window]"
   *  - PhantomJS === "[object Window]"
   *  - Safari === "[object Window]"
   *  - IE 11 === "[object Window]"
   *  - IE Edge === "[object Window]"
   * Test: `Object.prototype.toString.call(this)``
   *  - Chrome Worker === "[object global]"
   *  - Firefox Worker === "[object DedicatedWorkerGlobalScope]"
   *  - Safari Worker === "[object DedicatedWorkerGlobalScope]"
   *  - IE 11 Worker === "[object WorkerGlobalScope]"
   *  - IE Edge Worker === "[object WorkerGlobalScope]"
   */
  if (obj === globalObject) {
    return 'global';
  }

  /* ! Speed optimisation
   * Pre:
   *   array literal      x 2,888,352 ops/sec ±0.67% (82 runs sampled)
   * Post:
   *   array literal      x 22,479,650 ops/sec ±0.96% (81 runs sampled)
   */
  if (
    Array.isArray(obj) &&
    (symbolToStringTagExists === false || !(Symbol.toStringTag in obj))
  ) {
    return 'Array';
  }

  if (isDom) {
    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/browsers.html#location)
     * WhatWG HTML$7.7.3 - The `Location` interface
     * Test: `Object.prototype.toString.call(window.location)``
     *  - IE <=11 === "[object Object]"
     *  - IE Edge <=13 === "[object Object]"
     */
    if (obj === globalObject.location) {
      return 'Location';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/#document)
     * WhatWG HTML$3.1.1 - The `Document` object
     * Note: Most browsers currently adher to the W3C DOM Level 2 spec
     *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-26809268)
     *       which suggests that browsers should use HTMLTableCellElement for
     *       both TD and TH elements. WhatWG separates these.
     *       WhatWG HTML states:
     *         > For historical reasons, Window objects must also have a
     *         > writable, configurable, non-enumerable property named
     *         > HTMLDocument whose value is the Document interface object.
     * Test: `Object.prototype.toString.call(document)``
     *  - Chrome === "[object HTMLDocument]"
     *  - Firefox === "[object HTMLDocument]"
     *  - Safari === "[object HTMLDocument]"
     *  - IE <=10 === "[object Document]"
     *  - IE 11 === "[object HTMLDocument]"
     *  - IE Edge <=13 === "[object HTMLDocument]"
     */
    if (obj === globalObject.document) {
      return 'Document';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/webappapis.html#mimetypearray)
     * WhatWG HTML$8.6.1.5 - Plugins - Interface MimeTypeArray
     * Test: `Object.prototype.toString.call(navigator.mimeTypes)``
     *  - IE <=10 === "[object MSMimeTypesCollection]"
     */
    if (obj === (globalObject.navigator || {}).mimeTypes) {
      return 'MimeTypeArray';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/webappapis.html#pluginarray)
     * WhatWG HTML$8.6.1.5 - Plugins - Interface PluginArray
     * Test: `Object.prototype.toString.call(navigator.plugins)``
     *  - IE <=10 === "[object MSPluginsCollection]"
     */
    if (obj === (globalObject.navigator || {}).plugins) {
      return 'PluginArray';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/webappapis.html#pluginarray)
     * WhatWG HTML$4.4.4 - The `blockquote` element - Interface `HTMLQuoteElement`
     * Test: `Object.prototype.toString.call(document.createElement('blockquote'))``
     *  - IE <=10 === "[object HTMLBlockElement]"
     */
    if (obj instanceof HTMLElement && obj.tagName === 'BLOCKQUOTE') {
      return 'HTMLQuoteElement';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/#htmltabledatacellelement)
     * WhatWG HTML$4.9.9 - The `td` element - Interface `HTMLTableDataCellElement`
     * Note: Most browsers currently adher to the W3C DOM Level 2 spec
     *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-82915075)
     *       which suggests that browsers should use HTMLTableCellElement for
     *       both TD and TH elements. WhatWG separates these.
     * Test: Object.prototype.toString.call(document.createElement('td'))
     *  - Chrome === "[object HTMLTableCellElement]"
     *  - Firefox === "[object HTMLTableCellElement]"
     *  - Safari === "[object HTMLTableCellElement]"
     */
    if (obj instanceof HTMLElement && obj.tagName === 'TD') {
      return 'HTMLTableDataCellElement';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/#htmltableheadercellelement)
     * WhatWG HTML$4.9.9 - The `td` element - Interface `HTMLTableHeaderCellElement`
     * Note: Most browsers currently adher to the W3C DOM Level 2 spec
     *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-82915075)
     *       which suggests that browsers should use HTMLTableCellElement for
     *       both TD and TH elements. WhatWG separates these.
     * Test: Object.prototype.toString.call(document.createElement('th'))
     *  - Chrome === "[object HTMLTableCellElement]"
     *  - Firefox === "[object HTMLTableCellElement]"
     *  - Safari === "[object HTMLTableCellElement]"
     */
    if (obj instanceof HTMLElement && obj.tagName === 'TH') {
      return 'HTMLTableHeaderCellElement';
    }
  }

  /* ! Speed optimisation
  * Pre:
  *   Float64Array       x 625,644 ops/sec ±1.58% (80 runs sampled)
  *   Float32Array       x 1,279,852 ops/sec ±2.91% (77 runs sampled)
  *   Uint32Array        x 1,178,185 ops/sec ±1.95% (83 runs sampled)
  *   Uint16Array        x 1,008,380 ops/sec ±2.25% (80 runs sampled)
  *   Uint8Array         x 1,128,040 ops/sec ±2.11% (81 runs sampled)
  *   Int32Array         x 1,170,119 ops/sec ±2.88% (80 runs sampled)
  *   Int16Array         x 1,176,348 ops/sec ±5.79% (86 runs sampled)
  *   Int8Array          x 1,058,707 ops/sec ±4.94% (77 runs sampled)
  *   Uint8ClampedArray  x 1,110,633 ops/sec ±4.20% (80 runs sampled)
  * Post:
  *   Float64Array       x 7,105,671 ops/sec ±13.47% (64 runs sampled)
  *   Float32Array       x 5,887,912 ops/sec ±1.46% (82 runs sampled)
  *   Uint32Array        x 6,491,661 ops/sec ±1.76% (79 runs sampled)
  *   Uint16Array        x 6,559,795 ops/sec ±1.67% (82 runs sampled)
  *   Uint8Array         x 6,463,966 ops/sec ±1.43% (85 runs sampled)
  *   Int32Array         x 5,641,841 ops/sec ±3.49% (81 runs sampled)
  *   Int16Array         x 6,583,511 ops/sec ±1.98% (80 runs sampled)
  *   Int8Array          x 6,606,078 ops/sec ±1.74% (81 runs sampled)
  *   Uint8ClampedArray  x 6,602,224 ops/sec ±1.77% (83 runs sampled)
  */
  var stringTag = (symbolToStringTagExists && obj[Symbol.toStringTag]);
  if (typeof stringTag === 'string') {
    return stringTag;
  }

  var objPrototype = Object.getPrototypeOf(obj);
  /* ! Speed optimisation
  * Pre:
  *   regex literal      x 1,772,385 ops/sec ±1.85% (77 runs sampled)
  *   regex constructor  x 2,143,634 ops/sec ±2.46% (78 runs sampled)
  * Post:
  *   regex literal      x 3,928,009 ops/sec ±0.65% (78 runs sampled)
  *   regex constructor  x 3,931,108 ops/sec ±0.58% (84 runs sampled)
  */
  if (objPrototype === RegExp.prototype) {
    return 'RegExp';
  }

  /* ! Speed optimisation
  * Pre:
  *   date               x 2,130,074 ops/sec ±4.42% (68 runs sampled)
  * Post:
  *   date               x 3,953,779 ops/sec ±1.35% (77 runs sampled)
  */
  if (objPrototype === Date.prototype) {
    return 'Date';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-promise.prototype-@@tostringtag)
   * ES6$25.4.5.4 - Promise.prototype[@@toStringTag] should be "Promise":
   * Test: `Object.prototype.toString.call(Promise.resolve())``
   *  - Chrome <=47 === "[object Object]"
   *  - Edge <=20 === "[object Object]"
   *  - Firefox 29-Latest === "[object Promise]"
   *  - Safari 7.1-Latest === "[object Promise]"
   */
  if (promiseExists && objPrototype === Promise.prototype) {
    return 'Promise';
  }

  /* ! Speed optimisation
  * Pre:
  *   set                x 2,222,186 ops/sec ±1.31% (82 runs sampled)
  * Post:
  *   set                x 4,545,879 ops/sec ±1.13% (83 runs sampled)
  */
  if (setExists && objPrototype === Set.prototype) {
    return 'Set';
  }

  /* ! Speed optimisation
  * Pre:
  *   map                x 2,396,842 ops/sec ±1.59% (81 runs sampled)
  * Post:
  *   map                x 4,183,945 ops/sec ±6.59% (82 runs sampled)
  */
  if (mapExists && objPrototype === Map.prototype) {
    return 'Map';
  }

  /* ! Speed optimisation
  * Pre:
  *   weakset            x 1,323,220 ops/sec ±2.17% (76 runs sampled)
  * Post:
  *   weakset            x 4,237,510 ops/sec ±2.01% (77 runs sampled)
  */
  if (weakSetExists && objPrototype === WeakSet.prototype) {
    return 'WeakSet';
  }

  /* ! Speed optimisation
  * Pre:
  *   weakmap            x 1,500,260 ops/sec ±2.02% (78 runs sampled)
  * Post:
  *   weakmap            x 3,881,384 ops/sec ±1.45% (82 runs sampled)
  */
  if (weakMapExists && objPrototype === WeakMap.prototype) {
    return 'WeakMap';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-dataview.prototype-@@tostringtag)
   * ES6$24.2.4.21 - DataView.prototype[@@toStringTag] should be "DataView":
   * Test: `Object.prototype.toString.call(new DataView(new ArrayBuffer(1)))``
   *  - Edge <=13 === "[object Object]"
   */
  if (dataViewExists && objPrototype === DataView.prototype) {
    return 'DataView';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%mapiteratorprototype%-@@tostringtag)
   * ES6$23.1.5.2.2 - %MapIteratorPrototype%[@@toStringTag] should be "Map Iterator":
   * Test: `Object.prototype.toString.call(new Map().entries())``
   *  - Edge <=13 === "[object Object]"
   */
  if (mapExists && objPrototype === mapIteratorPrototype) {
    return 'Map Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%setiteratorprototype%-@@tostringtag)
   * ES6$23.2.5.2.2 - %SetIteratorPrototype%[@@toStringTag] should be "Set Iterator":
   * Test: `Object.prototype.toString.call(new Set().entries())``
   *  - Edge <=13 === "[object Object]"
   */
  if (setExists && objPrototype === setIteratorPrototype) {
    return 'Set Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%arrayiteratorprototype%-@@tostringtag)
   * ES6$22.1.5.2.2 - %ArrayIteratorPrototype%[@@toStringTag] should be "Array Iterator":
   * Test: `Object.prototype.toString.call([][Symbol.iterator]())``
   *  - Edge <=13 === "[object Object]"
   */
  if (arrayIteratorExists && objPrototype === arrayIteratorPrototype) {
    return 'Array Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%stringiteratorprototype%-@@tostringtag)
   * ES6$21.1.5.2.2 - %StringIteratorPrototype%[@@toStringTag] should be "String Iterator":
   * Test: `Object.prototype.toString.call(''[Symbol.iterator]())``
   *  - Edge <=13 === "[object Object]"
   */
  if (stringIteratorExists && objPrototype === stringIteratorPrototype) {
    return 'String Iterator';
  }

  /* ! Speed optimisation
  * Pre:
  *   object from null   x 2,424,320 ops/sec ±1.67% (76 runs sampled)
  * Post:
  *   object from null   x 5,838,000 ops/sec ±0.99% (84 runs sampled)
  */
  if (objPrototype === null) {
    return 'Object';
  }

  return Object
    .prototype
    .toString
    .call(obj)
    .slice(toStringLeftSliceLength, toStringRightSliceLength);
};

module.exports.typeDetect = module.exports;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}]},{},[1])(1)
});
}});

require.define({"moment": function(exports, require, module) {
//! moment.js
//! version : 2.19.3
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global.moment = factory()
}(this, (function () { 'use strict';

var hookCallback;

function hooks () {
    return hookCallback.apply(null, arguments);
}

// This is done to register the method called with moment()
// without creating circular dependencies.
function setHookCallback (callback) {
    hookCallback = callback;
}

function isArray(input) {
    return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
}

function isObject(input) {
    // IE8 will treat undefined and null as object if it wasn't for
    // input != null
    return input != null && Object.prototype.toString.call(input) === '[object Object]';
}

function isObjectEmpty(obj) {
    if (Object.getOwnPropertyNames) {
        return (Object.getOwnPropertyNames(obj).length === 0);
    } else {
        var k;
        for (k in obj) {
            if (obj.hasOwnProperty(k)) {
                return false;
            }
        }
        return true;
    }
}

function isUndefined(input) {
    return input === void 0;
}

function isNumber(input) {
    return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
}

function isDate(input) {
    return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
}

function map(arr, fn) {
    var res = [], i;
    for (i = 0; i < arr.length; ++i) {
        res.push(fn(arr[i], i));
    }
    return res;
}

function hasOwnProp(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b);
}

function extend(a, b) {
    for (var i in b) {
        if (hasOwnProp(b, i)) {
            a[i] = b[i];
        }
    }

    if (hasOwnProp(b, 'toString')) {
        a.toString = b.toString;
    }

    if (hasOwnProp(b, 'valueOf')) {
        a.valueOf = b.valueOf;
    }

    return a;
}

function createUTC (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, true).utc();
}

function defaultParsingFlags() {
    // We need to deep clone this object.
    return {
        empty           : false,
        unusedTokens    : [],
        unusedInput     : [],
        overflow        : -2,
        charsLeftOver   : 0,
        nullInput       : false,
        invalidMonth    : null,
        invalidFormat   : false,
        userInvalidated : false,
        iso             : false,
        parsedDateParts : [],
        meridiem        : null,
        rfc2822         : false,
        weekdayMismatch : false
    };
}

function getParsingFlags(m) {
    if (m._pf == null) {
        m._pf = defaultParsingFlags();
    }
    return m._pf;
}

var some;
if (Array.prototype.some) {
    some = Array.prototype.some;
} else {
    some = function (fun) {
        var t = Object(this);
        var len = t.length >>> 0;

        for (var i = 0; i < len; i++) {
            if (i in t && fun.call(this, t[i], i, t)) {
                return true;
            }
        }

        return false;
    };
}

function isValid(m) {
    if (m._isValid == null) {
        var flags = getParsingFlags(m);
        var parsedParts = some.call(flags.parsedDateParts, function (i) {
            return i != null;
        });
        var isNowValid = !isNaN(m._d.getTime()) &&
            flags.overflow < 0 &&
            !flags.empty &&
            !flags.invalidMonth &&
            !flags.invalidWeekday &&
            !flags.weekdayMismatch &&
            !flags.nullInput &&
            !flags.invalidFormat &&
            !flags.userInvalidated &&
            (!flags.meridiem || (flags.meridiem && parsedParts));

        if (m._strict) {
            isNowValid = isNowValid &&
                flags.charsLeftOver === 0 &&
                flags.unusedTokens.length === 0 &&
                flags.bigHour === undefined;
        }

        if (Object.isFrozen == null || !Object.isFrozen(m)) {
            m._isValid = isNowValid;
        }
        else {
            return isNowValid;
        }
    }
    return m._isValid;
}

function createInvalid (flags) {
    var m = createUTC(NaN);
    if (flags != null) {
        extend(getParsingFlags(m), flags);
    }
    else {
        getParsingFlags(m).userInvalidated = true;
    }

    return m;
}

// Plugins that add properties should also add the key here (null value),
// so we can properly clone ourselves.
var momentProperties = hooks.momentProperties = [];

function copyConfig(to, from) {
    var i, prop, val;

    if (!isUndefined(from._isAMomentObject)) {
        to._isAMomentObject = from._isAMomentObject;
    }
    if (!isUndefined(from._i)) {
        to._i = from._i;
    }
    if (!isUndefined(from._f)) {
        to._f = from._f;
    }
    if (!isUndefined(from._l)) {
        to._l = from._l;
    }
    if (!isUndefined(from._strict)) {
        to._strict = from._strict;
    }
    if (!isUndefined(from._tzm)) {
        to._tzm = from._tzm;
    }
    if (!isUndefined(from._isUTC)) {
        to._isUTC = from._isUTC;
    }
    if (!isUndefined(from._offset)) {
        to._offset = from._offset;
    }
    if (!isUndefined(from._pf)) {
        to._pf = getParsingFlags(from);
    }
    if (!isUndefined(from._locale)) {
        to._locale = from._locale;
    }

    if (momentProperties.length > 0) {
        for (i = 0; i < momentProperties.length; i++) {
            prop = momentProperties[i];
            val = from[prop];
            if (!isUndefined(val)) {
                to[prop] = val;
            }
        }
    }

    return to;
}

var updateInProgress = false;

// Moment prototype object
function Moment(config) {
    copyConfig(this, config);
    this._d = new Date(config._d != null ? config._d.getTime() : NaN);
    if (!this.isValid()) {
        this._d = new Date(NaN);
    }
    // Prevent infinite loop in case updateOffset creates new moment
    // objects.
    if (updateInProgress === false) {
        updateInProgress = true;
        hooks.updateOffset(this);
        updateInProgress = false;
    }
}

function isMoment (obj) {
    return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
}

function absFloor (number) {
    if (number < 0) {
        // -0 -> 0
        return Math.ceil(number) || 0;
    } else {
        return Math.floor(number);
    }
}

function toInt(argumentForCoercion) {
    var coercedNumber = +argumentForCoercion,
        value = 0;

    if (coercedNumber !== 0 && isFinite(coercedNumber)) {
        value = absFloor(coercedNumber);
    }

    return value;
}

// compare two arrays, return the number of differences
function compareArrays(array1, array2, dontConvert) {
    var len = Math.min(array1.length, array2.length),
        lengthDiff = Math.abs(array1.length - array2.length),
        diffs = 0,
        i;
    for (i = 0; i < len; i++) {
        if ((dontConvert && array1[i] !== array2[i]) ||
            (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
            diffs++;
        }
    }
    return diffs + lengthDiff;
}

function warn(msg) {
    if (hooks.suppressDeprecationWarnings === false &&
            (typeof console !==  'undefined') && console.warn) {
        console.warn('Deprecation warning: ' + msg);
    }
}

function deprecate(msg, fn) {
    var firstTime = true;

    return extend(function () {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(null, msg);
        }
        if (firstTime) {
            var args = [];
            var arg;
            for (var i = 0; i < arguments.length; i++) {
                arg = '';
                if (typeof arguments[i] === 'object') {
                    arg += '\n[' + i + '] ';
                    for (var key in arguments[0]) {
                        arg += key + ': ' + arguments[0][key] + ', ';
                    }
                    arg = arg.slice(0, -2); // Remove trailing comma and space
                } else {
                    arg = arguments[i];
                }
                args.push(arg);
            }
            warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
            firstTime = false;
        }
        return fn.apply(this, arguments);
    }, fn);
}

var deprecations = {};

function deprecateSimple(name, msg) {
    if (hooks.deprecationHandler != null) {
        hooks.deprecationHandler(name, msg);
    }
    if (!deprecations[name]) {
        warn(msg);
        deprecations[name] = true;
    }
}

hooks.suppressDeprecationWarnings = false;
hooks.deprecationHandler = null;

function isFunction(input) {
    return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
}

function set (config) {
    var prop, i;
    for (i in config) {
        prop = config[i];
        if (isFunction(prop)) {
            this[i] = prop;
        } else {
            this['_' + i] = prop;
        }
    }
    this._config = config;
    // Lenient ordinal parsing accepts just a number in addition to
    // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
    // TODO: Remove "ordinalParse" fallback in next major release.
    this._dayOfMonthOrdinalParseLenient = new RegExp(
        (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
            '|' + (/\d{1,2}/).source);
}

function mergeConfigs(parentConfig, childConfig) {
    var res = extend({}, parentConfig), prop;
    for (prop in childConfig) {
        if (hasOwnProp(childConfig, prop)) {
            if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                res[prop] = {};
                extend(res[prop], parentConfig[prop]);
                extend(res[prop], childConfig[prop]);
            } else if (childConfig[prop] != null) {
                res[prop] = childConfig[prop];
            } else {
                delete res[prop];
            }
        }
    }
    for (prop in parentConfig) {
        if (hasOwnProp(parentConfig, prop) &&
                !hasOwnProp(childConfig, prop) &&
                isObject(parentConfig[prop])) {
            // make sure changes to properties don't modify parent config
            res[prop] = extend({}, res[prop]);
        }
    }
    return res;
}

function Locale(config) {
    if (config != null) {
        this.set(config);
    }
}

var keys;

if (Object.keys) {
    keys = Object.keys;
} else {
    keys = function (obj) {
        var i, res = [];
        for (i in obj) {
            if (hasOwnProp(obj, i)) {
                res.push(i);
            }
        }
        return res;
    };
}

var defaultCalendar = {
    sameDay : '[Today at] LT',
    nextDay : '[Tomorrow at] LT',
    nextWeek : 'dddd [at] LT',
    lastDay : '[Yesterday at] LT',
    lastWeek : '[Last] dddd [at] LT',
    sameElse : 'L'
};

function calendar (key, mom, now) {
    var output = this._calendar[key] || this._calendar['sameElse'];
    return isFunction(output) ? output.call(mom, now) : output;
}

var defaultLongDateFormat = {
    LTS  : 'h:mm:ss A',
    LT   : 'h:mm A',
    L    : 'MM/DD/YYYY',
    LL   : 'MMMM D, YYYY',
    LLL  : 'MMMM D, YYYY h:mm A',
    LLLL : 'dddd, MMMM D, YYYY h:mm A'
};

function longDateFormat (key) {
    var format = this._longDateFormat[key],
        formatUpper = this._longDateFormat[key.toUpperCase()];

    if (format || !formatUpper) {
        return format;
    }

    this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
        return val.slice(1);
    });

    return this._longDateFormat[key];
}

var defaultInvalidDate = 'Invalid date';

function invalidDate () {
    return this._invalidDate;
}

var defaultOrdinal = '%d';
var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

function ordinal (number) {
    return this._ordinal.replace('%d', number);
}

var defaultRelativeTime = {
    future : 'in %s',
    past   : '%s ago',
    s  : 'a few seconds',
    ss : '%d seconds',
    m  : 'a minute',
    mm : '%d minutes',
    h  : 'an hour',
    hh : '%d hours',
    d  : 'a day',
    dd : '%d days',
    M  : 'a month',
    MM : '%d months',
    y  : 'a year',
    yy : '%d years'
};

function relativeTime (number, withoutSuffix, string, isFuture) {
    var output = this._relativeTime[string];
    return (isFunction(output)) ?
        output(number, withoutSuffix, string, isFuture) :
        output.replace(/%d/i, number);
}

function pastFuture (diff, output) {
    var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
    return isFunction(format) ? format(output) : format.replace(/%s/i, output);
}

var aliases = {};

function addUnitAlias (unit, shorthand) {
    var lowerCase = unit.toLowerCase();
    aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
}

function normalizeUnits(units) {
    return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
}

function normalizeObjectUnits(inputObject) {
    var normalizedInput = {},
        normalizedProp,
        prop;

    for (prop in inputObject) {
        if (hasOwnProp(inputObject, prop)) {
            normalizedProp = normalizeUnits(prop);
            if (normalizedProp) {
                normalizedInput[normalizedProp] = inputObject[prop];
            }
        }
    }

    return normalizedInput;
}

var priorities = {};

function addUnitPriority(unit, priority) {
    priorities[unit] = priority;
}

function getPrioritizedUnits(unitsObj) {
    var units = [];
    for (var u in unitsObj) {
        units.push({unit: u, priority: priorities[u]});
    }
    units.sort(function (a, b) {
        return a.priority - b.priority;
    });
    return units;
}

function zeroFill(number, targetLength, forceSign) {
    var absNumber = '' + Math.abs(number),
        zerosToFill = targetLength - absNumber.length,
        sign = number >= 0;
    return (sign ? (forceSign ? '+' : '') : '-') +
        Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
}

var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

var formatFunctions = {};

var formatTokenFunctions = {};

// token:    'M'
// padded:   ['MM', 2]
// ordinal:  'Mo'
// callback: function () { this.month() + 1 }
function addFormatToken (token, padded, ordinal, callback) {
    var func = callback;
    if (typeof callback === 'string') {
        func = function () {
            return this[callback]();
        };
    }
    if (token) {
        formatTokenFunctions[token] = func;
    }
    if (padded) {
        formatTokenFunctions[padded[0]] = function () {
            return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
        };
    }
    if (ordinal) {
        formatTokenFunctions[ordinal] = function () {
            return this.localeData().ordinal(func.apply(this, arguments), token);
        };
    }
}

function removeFormattingTokens(input) {
    if (input.match(/\[[\s\S]/)) {
        return input.replace(/^\[|\]$/g, '');
    }
    return input.replace(/\\/g, '');
}

function makeFormatFunction(format) {
    var array = format.match(formattingTokens), i, length;

    for (i = 0, length = array.length; i < length; i++) {
        if (formatTokenFunctions[array[i]]) {
            array[i] = formatTokenFunctions[array[i]];
        } else {
            array[i] = removeFormattingTokens(array[i]);
        }
    }

    return function (mom) {
        var output = '', i;
        for (i = 0; i < length; i++) {
            output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
        }
        return output;
    };
}

// format date using native date object
function formatMoment(m, format) {
    if (!m.isValid()) {
        return m.localeData().invalidDate();
    }

    format = expandFormat(format, m.localeData());
    formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

    return formatFunctions[format](m);
}

function expandFormat(format, locale) {
    var i = 5;

    function replaceLongDateFormatTokens(input) {
        return locale.longDateFormat(input) || input;
    }

    localFormattingTokens.lastIndex = 0;
    while (i >= 0 && localFormattingTokens.test(format)) {
        format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
        localFormattingTokens.lastIndex = 0;
        i -= 1;
    }

    return format;
}

var match1         = /\d/;            //       0 - 9
var match2         = /\d\d/;          //      00 - 99
var match3         = /\d{3}/;         //     000 - 999
var match4         = /\d{4}/;         //    0000 - 9999
var match6         = /[+-]?\d{6}/;    // -999999 - 999999
var match1to2      = /\d\d?/;         //       0 - 99
var match3to4      = /\d\d\d\d?/;     //     999 - 9999
var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
var match1to3      = /\d{1,3}/;       //       0 - 999
var match1to4      = /\d{1,4}/;       //       0 - 9999
var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

var matchUnsigned  = /\d+/;           //       0 - inf
var matchSigned    = /[+-]?\d+/;      //    -inf - inf

var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

// any word (or two) characters or numbers including two/three word month in arabic.
// includes scottish gaelic two word and hyphenated months
var matchWord = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i;


var regexes = {};

function addRegexToken (token, regex, strictRegex) {
    regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
        return (isStrict && strictRegex) ? strictRegex : regex;
    };
}

function getParseRegexForToken (token, config) {
    if (!hasOwnProp(regexes, token)) {
        return new RegExp(unescapeFormat(token));
    }

    return regexes[token](config._strict, config._locale);
}

// Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
function unescapeFormat(s) {
    return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
        return p1 || p2 || p3 || p4;
    }));
}

function regexEscape(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

var tokens = {};

function addParseToken (token, callback) {
    var i, func = callback;
    if (typeof token === 'string') {
        token = [token];
    }
    if (isNumber(callback)) {
        func = function (input, array) {
            array[callback] = toInt(input);
        };
    }
    for (i = 0; i < token.length; i++) {
        tokens[token[i]] = func;
    }
}

function addWeekParseToken (token, callback) {
    addParseToken(token, function (input, array, config, token) {
        config._w = config._w || {};
        callback(input, config._w, config, token);
    });
}

function addTimeToArrayFromToken(token, input, config) {
    if (input != null && hasOwnProp(tokens, token)) {
        tokens[token](input, config._a, config, token);
    }
}

var YEAR = 0;
var MONTH = 1;
var DATE = 2;
var HOUR = 3;
var MINUTE = 4;
var SECOND = 5;
var MILLISECOND = 6;
var WEEK = 7;
var WEEKDAY = 8;

// FORMATTING

addFormatToken('Y', 0, 0, function () {
    var y = this.year();
    return y <= 9999 ? '' + y : '+' + y;
});

addFormatToken(0, ['YY', 2], 0, function () {
    return this.year() % 100;
});

addFormatToken(0, ['YYYY',   4],       0, 'year');
addFormatToken(0, ['YYYYY',  5],       0, 'year');
addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

// ALIASES

addUnitAlias('year', 'y');

// PRIORITIES

addUnitPriority('year', 1);

// PARSING

addRegexToken('Y',      matchSigned);
addRegexToken('YY',     match1to2, match2);
addRegexToken('YYYY',   match1to4, match4);
addRegexToken('YYYYY',  match1to6, match6);
addRegexToken('YYYYYY', match1to6, match6);

addParseToken(['YYYYY', 'YYYYYY'], YEAR);
addParseToken('YYYY', function (input, array) {
    array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
});
addParseToken('YY', function (input, array) {
    array[YEAR] = hooks.parseTwoDigitYear(input);
});
addParseToken('Y', function (input, array) {
    array[YEAR] = parseInt(input, 10);
});

// HELPERS

function daysInYear(year) {
    return isLeapYear(year) ? 366 : 365;
}

function isLeapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

// HOOKS

hooks.parseTwoDigitYear = function (input) {
    return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
};

// MOMENTS

var getSetYear = makeGetSet('FullYear', true);

function getIsLeapYear () {
    return isLeapYear(this.year());
}

function makeGetSet (unit, keepTime) {
    return function (value) {
        if (value != null) {
            set$1(this, unit, value);
            hooks.updateOffset(this, keepTime);
            return this;
        } else {
            return get(this, unit);
        }
    };
}

function get (mom, unit) {
    return mom.isValid() ?
        mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
}

function set$1 (mom, unit, value) {
    if (mom.isValid() && !isNaN(value)) {
        if (unit === 'FullYear' && isLeapYear(mom.year()) && mom.month() === 1 && mom.date() === 29) {
            mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value, mom.month(), daysInMonth(value, mom.month()));
        }
        else {
            mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
        }
    }
}

// MOMENTS

function stringGet (units) {
    units = normalizeUnits(units);
    if (isFunction(this[units])) {
        return this[units]();
    }
    return this;
}


function stringSet (units, value) {
    if (typeof units === 'object') {
        units = normalizeObjectUnits(units);
        var prioritized = getPrioritizedUnits(units);
        for (var i = 0; i < prioritized.length; i++) {
            this[prioritized[i].unit](units[prioritized[i].unit]);
        }
    } else {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units](value);
        }
    }
    return this;
}

function mod(n, x) {
    return ((n % x) + x) % x;
}

var indexOf;

if (Array.prototype.indexOf) {
    indexOf = Array.prototype.indexOf;
} else {
    indexOf = function (o) {
        // I know
        var i;
        for (i = 0; i < this.length; ++i) {
            if (this[i] === o) {
                return i;
            }
        }
        return -1;
    };
}

function daysInMonth(year, month) {
    if (isNaN(year) || isNaN(month)) {
        return NaN;
    }
    var modMonth = mod(month, 12);
    year += (month - modMonth) / 12;
    return modMonth === 1 ? (isLeapYear(year) ? 29 : 28) : (31 - modMonth % 7 % 2);
}

// FORMATTING

addFormatToken('M', ['MM', 2], 'Mo', function () {
    return this.month() + 1;
});

addFormatToken('MMM', 0, 0, function (format) {
    return this.localeData().monthsShort(this, format);
});

addFormatToken('MMMM', 0, 0, function (format) {
    return this.localeData().months(this, format);
});

// ALIASES

addUnitAlias('month', 'M');

// PRIORITY

addUnitPriority('month', 8);

// PARSING

addRegexToken('M',    match1to2);
addRegexToken('MM',   match1to2, match2);
addRegexToken('MMM',  function (isStrict, locale) {
    return locale.monthsShortRegex(isStrict);
});
addRegexToken('MMMM', function (isStrict, locale) {
    return locale.monthsRegex(isStrict);
});

addParseToken(['M', 'MM'], function (input, array) {
    array[MONTH] = toInt(input) - 1;
});

addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
    var month = config._locale.monthsParse(input, token, config._strict);
    // if we didn't find a month name, mark the date as invalid.
    if (month != null) {
        array[MONTH] = month;
    } else {
        getParsingFlags(config).invalidMonth = input;
    }
});

// LOCALES

var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
function localeMonths (m, format) {
    if (!m) {
        return isArray(this._months) ? this._months :
            this._months['standalone'];
    }
    return isArray(this._months) ? this._months[m.month()] :
        this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
}

var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
function localeMonthsShort (m, format) {
    if (!m) {
        return isArray(this._monthsShort) ? this._monthsShort :
            this._monthsShort['standalone'];
    }
    return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
        this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
}

function handleStrictParse(monthName, format, strict) {
    var i, ii, mom, llc = monthName.toLocaleLowerCase();
    if (!this._monthsParse) {
        // this is not used
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
        for (i = 0; i < 12; ++i) {
            mom = createUTC([2000, i]);
            this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
            this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'MMM') {
            ii = indexOf.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'MMM') {
            ii = indexOf.call(this._shortMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._longMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeMonthsParse (monthName, format, strict) {
    var i, mom, regex;

    if (this._monthsParseExact) {
        return handleStrictParse.call(this, monthName, format, strict);
    }

    if (!this._monthsParse) {
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
    }

    // TODO: add sorting
    // Sorting makes sure if one month (or abbr) is a prefix of another
    // see sorting in computeMonthsParse
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        if (strict && !this._longMonthsParse[i]) {
            this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
            this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
        }
        if (!strict && !this._monthsParse[i]) {
            regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
            this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
            return i;
        } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
            return i;
        } else if (!strict && this._monthsParse[i].test(monthName)) {
            return i;
        }
    }
}

// MOMENTS

function setMonth (mom, value) {
    var dayOfMonth;

    if (!mom.isValid()) {
        // No op
        return mom;
    }

    if (typeof value === 'string') {
        if (/^\d+$/.test(value)) {
            value = toInt(value);
        } else {
            value = mom.localeData().monthsParse(value);
            // TODO: Another silent failure?
            if (!isNumber(value)) {
                return mom;
            }
        }
    }

    dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
    mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
    return mom;
}

function getSetMonth (value) {
    if (value != null) {
        setMonth(this, value);
        hooks.updateOffset(this, true);
        return this;
    } else {
        return get(this, 'Month');
    }
}

function getDaysInMonth () {
    return daysInMonth(this.year(), this.month());
}

var defaultMonthsShortRegex = matchWord;
function monthsShortRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsShortStrictRegex;
        } else {
            return this._monthsShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsShortRegex')) {
            this._monthsShortRegex = defaultMonthsShortRegex;
        }
        return this._monthsShortStrictRegex && isStrict ?
            this._monthsShortStrictRegex : this._monthsShortRegex;
    }
}

var defaultMonthsRegex = matchWord;
function monthsRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsStrictRegex;
        } else {
            return this._monthsRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsRegex')) {
            this._monthsRegex = defaultMonthsRegex;
        }
        return this._monthsStrictRegex && isStrict ?
            this._monthsStrictRegex : this._monthsRegex;
    }
}

function computeMonthsParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom;
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        shortPieces.push(this.monthsShort(mom, ''));
        longPieces.push(this.months(mom, ''));
        mixedPieces.push(this.months(mom, ''));
        mixedPieces.push(this.monthsShort(mom, ''));
    }
    // Sorting makes sure if one month (or abbr) is a prefix of another it
    // will match the longer piece.
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 12; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
    }
    for (i = 0; i < 24; i++) {
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._monthsShortRegex = this._monthsRegex;
    this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
}

function createDate (y, m, d, h, M, s, ms) {
    // can't just apply() to create a date:
    // https://stackoverflow.com/q/181348
    var date = new Date(y, m, d, h, M, s, ms);

    // the date constructor remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
        date.setFullYear(y);
    }
    return date;
}

function createUTCDate (y) {
    var date = new Date(Date.UTC.apply(null, arguments));

    // the Date.UTC function remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
        date.setUTCFullYear(y);
    }
    return date;
}

// start-of-first-week - start-of-year
function firstWeekOffset(year, dow, doy) {
    var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
        fwd = 7 + dow - doy,
        // first-week day local weekday -- which local weekday is fwd
        fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

    return -fwdlw + fwd - 1;
}

// https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
    var localWeekday = (7 + weekday - dow) % 7,
        weekOffset = firstWeekOffset(year, dow, doy),
        dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
        resYear, resDayOfYear;

    if (dayOfYear <= 0) {
        resYear = year - 1;
        resDayOfYear = daysInYear(resYear) + dayOfYear;
    } else if (dayOfYear > daysInYear(year)) {
        resYear = year + 1;
        resDayOfYear = dayOfYear - daysInYear(year);
    } else {
        resYear = year;
        resDayOfYear = dayOfYear;
    }

    return {
        year: resYear,
        dayOfYear: resDayOfYear
    };
}

function weekOfYear(mom, dow, doy) {
    var weekOffset = firstWeekOffset(mom.year(), dow, doy),
        week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
        resWeek, resYear;

    if (week < 1) {
        resYear = mom.year() - 1;
        resWeek = week + weeksInYear(resYear, dow, doy);
    } else if (week > weeksInYear(mom.year(), dow, doy)) {
        resWeek = week - weeksInYear(mom.year(), dow, doy);
        resYear = mom.year() + 1;
    } else {
        resYear = mom.year();
        resWeek = week;
    }

    return {
        week: resWeek,
        year: resYear
    };
}

function weeksInYear(year, dow, doy) {
    var weekOffset = firstWeekOffset(year, dow, doy),
        weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
    return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
}

// FORMATTING

addFormatToken('w', ['ww', 2], 'wo', 'week');
addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

// ALIASES

addUnitAlias('week', 'w');
addUnitAlias('isoWeek', 'W');

// PRIORITIES

addUnitPriority('week', 5);
addUnitPriority('isoWeek', 5);

// PARSING

addRegexToken('w',  match1to2);
addRegexToken('ww', match1to2, match2);
addRegexToken('W',  match1to2);
addRegexToken('WW', match1to2, match2);

addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
    week[token.substr(0, 1)] = toInt(input);
});

// HELPERS

// LOCALES

function localeWeek (mom) {
    return weekOfYear(mom, this._week.dow, this._week.doy).week;
}

var defaultLocaleWeek = {
    dow : 0, // Sunday is the first day of the week.
    doy : 6  // The week that contains Jan 1st is the first week of the year.
};

function localeFirstDayOfWeek () {
    return this._week.dow;
}

function localeFirstDayOfYear () {
    return this._week.doy;
}

// MOMENTS

function getSetWeek (input) {
    var week = this.localeData().week(this);
    return input == null ? week : this.add((input - week) * 7, 'd');
}

function getSetISOWeek (input) {
    var week = weekOfYear(this, 1, 4).week;
    return input == null ? week : this.add((input - week) * 7, 'd');
}

// FORMATTING

addFormatToken('d', 0, 'do', 'day');

addFormatToken('dd', 0, 0, function (format) {
    return this.localeData().weekdaysMin(this, format);
});

addFormatToken('ddd', 0, 0, function (format) {
    return this.localeData().weekdaysShort(this, format);
});

addFormatToken('dddd', 0, 0, function (format) {
    return this.localeData().weekdays(this, format);
});

addFormatToken('e', 0, 0, 'weekday');
addFormatToken('E', 0, 0, 'isoWeekday');

// ALIASES

addUnitAlias('day', 'd');
addUnitAlias('weekday', 'e');
addUnitAlias('isoWeekday', 'E');

// PRIORITY
addUnitPriority('day', 11);
addUnitPriority('weekday', 11);
addUnitPriority('isoWeekday', 11);

// PARSING

addRegexToken('d',    match1to2);
addRegexToken('e',    match1to2);
addRegexToken('E',    match1to2);
addRegexToken('dd',   function (isStrict, locale) {
    return locale.weekdaysMinRegex(isStrict);
});
addRegexToken('ddd',   function (isStrict, locale) {
    return locale.weekdaysShortRegex(isStrict);
});
addRegexToken('dddd',   function (isStrict, locale) {
    return locale.weekdaysRegex(isStrict);
});

addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
    var weekday = config._locale.weekdaysParse(input, token, config._strict);
    // if we didn't get a weekday name, mark the date as invalid
    if (weekday != null) {
        week.d = weekday;
    } else {
        getParsingFlags(config).invalidWeekday = input;
    }
});

addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
    week[token] = toInt(input);
});

// HELPERS

function parseWeekday(input, locale) {
    if (typeof input !== 'string') {
        return input;
    }

    if (!isNaN(input)) {
        return parseInt(input, 10);
    }

    input = locale.weekdaysParse(input);
    if (typeof input === 'number') {
        return input;
    }

    return null;
}

function parseIsoWeekday(input, locale) {
    if (typeof input === 'string') {
        return locale.weekdaysParse(input) % 7 || 7;
    }
    return isNaN(input) ? null : input;
}

// LOCALES

var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
function localeWeekdays (m, format) {
    if (!m) {
        return isArray(this._weekdays) ? this._weekdays :
            this._weekdays['standalone'];
    }
    return isArray(this._weekdays) ? this._weekdays[m.day()] :
        this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
}

var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
function localeWeekdaysShort (m) {
    return (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
}

var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
function localeWeekdaysMin (m) {
    return (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
}

function handleStrictParse$1(weekdayName, format, strict) {
    var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._minWeekdaysParse = [];

        for (i = 0; i < 7; ++i) {
            mom = createUTC([2000, 1]).day(i);
            this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
            this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
            this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'dddd') {
            ii = indexOf.call(this._weekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'dddd') {
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._minWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeWeekdaysParse (weekdayName, format, strict) {
    var i, mom, regex;

    if (this._weekdaysParseExact) {
        return handleStrictParse$1.call(this, weekdayName, format, strict);
    }

    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._minWeekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._fullWeekdaysParse = [];
    }

    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already

        mom = createUTC([2000, 1]).day(i);
        if (strict && !this._fullWeekdaysParse[i]) {
            this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
            this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
            this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
        }
        if (!this._weekdaysParse[i]) {
            regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
            this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
            return i;
        }
    }
}

// MOMENTS

function getSetDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
    if (input != null) {
        input = parseWeekday(input, this.localeData());
        return this.add(input - day, 'd');
    } else {
        return day;
    }
}

function getSetLocaleDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
    return input == null ? weekday : this.add(input - weekday, 'd');
}

function getSetISODayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }

    // behaves the same as moment#day except
    // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
    // as a setter, sunday should belong to the previous week.

    if (input != null) {
        var weekday = parseIsoWeekday(input, this.localeData());
        return this.day(this.day() % 7 ? weekday : weekday - 7);
    } else {
        return this.day() || 7;
    }
}

var defaultWeekdaysRegex = matchWord;
function weekdaysRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysStrictRegex;
        } else {
            return this._weekdaysRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            this._weekdaysRegex = defaultWeekdaysRegex;
        }
        return this._weekdaysStrictRegex && isStrict ?
            this._weekdaysStrictRegex : this._weekdaysRegex;
    }
}

var defaultWeekdaysShortRegex = matchWord;
function weekdaysShortRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysShortStrictRegex;
        } else {
            return this._weekdaysShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysShortRegex')) {
            this._weekdaysShortRegex = defaultWeekdaysShortRegex;
        }
        return this._weekdaysShortStrictRegex && isStrict ?
            this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
    }
}

var defaultWeekdaysMinRegex = matchWord;
function weekdaysMinRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysMinStrictRegex;
        } else {
            return this._weekdaysMinRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysMinRegex')) {
            this._weekdaysMinRegex = defaultWeekdaysMinRegex;
        }
        return this._weekdaysMinStrictRegex && isStrict ?
            this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
    }
}


function computeWeekdaysParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom, minp, shortp, longp;
    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, 1]).day(i);
        minp = this.weekdaysMin(mom, '');
        shortp = this.weekdaysShort(mom, '');
        longp = this.weekdays(mom, '');
        minPieces.push(minp);
        shortPieces.push(shortp);
        longPieces.push(longp);
        mixedPieces.push(minp);
        mixedPieces.push(shortp);
        mixedPieces.push(longp);
    }
    // Sorting makes sure if one weekday (or abbr) is a prefix of another it
    // will match the longer piece.
    minPieces.sort(cmpLenRev);
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 7; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._weekdaysShortRegex = this._weekdaysRegex;
    this._weekdaysMinRegex = this._weekdaysRegex;

    this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
}

// FORMATTING

function hFormat() {
    return this.hours() % 12 || 12;
}

function kFormat() {
    return this.hours() || 24;
}

addFormatToken('H', ['HH', 2], 0, 'hour');
addFormatToken('h', ['hh', 2], 0, hFormat);
addFormatToken('k', ['kk', 2], 0, kFormat);

addFormatToken('hmm', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
});

addFormatToken('hmmss', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

addFormatToken('Hmm', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2);
});

addFormatToken('Hmmss', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

function meridiem (token, lowercase) {
    addFormatToken(token, 0, 0, function () {
        return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
    });
}

meridiem('a', true);
meridiem('A', false);

// ALIASES

addUnitAlias('hour', 'h');

// PRIORITY
addUnitPriority('hour', 13);

// PARSING

function matchMeridiem (isStrict, locale) {
    return locale._meridiemParse;
}

addRegexToken('a',  matchMeridiem);
addRegexToken('A',  matchMeridiem);
addRegexToken('H',  match1to2);
addRegexToken('h',  match1to2);
addRegexToken('k',  match1to2);
addRegexToken('HH', match1to2, match2);
addRegexToken('hh', match1to2, match2);
addRegexToken('kk', match1to2, match2);

addRegexToken('hmm', match3to4);
addRegexToken('hmmss', match5to6);
addRegexToken('Hmm', match3to4);
addRegexToken('Hmmss', match5to6);

addParseToken(['H', 'HH'], HOUR);
addParseToken(['k', 'kk'], function (input, array, config) {
    var kInput = toInt(input);
    array[HOUR] = kInput === 24 ? 0 : kInput;
});
addParseToken(['a', 'A'], function (input, array, config) {
    config._isPm = config._locale.isPM(input);
    config._meridiem = input;
});
addParseToken(['h', 'hh'], function (input, array, config) {
    array[HOUR] = toInt(input);
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
    getParsingFlags(config).bigHour = true;
});
addParseToken('Hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
});
addParseToken('Hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
});

// LOCALES

function localeIsPM (input) {
    // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
    // Using charAt should be more compatible.
    return ((input + '').toLowerCase().charAt(0) === 'p');
}

var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
function localeMeridiem (hours, minutes, isLower) {
    if (hours > 11) {
        return isLower ? 'pm' : 'PM';
    } else {
        return isLower ? 'am' : 'AM';
    }
}


// MOMENTS

// Setting the hour should keep the time, because the user explicitly
// specified which hour he wants. So trying to maintain the same hour (in
// a new timezone) makes sense. Adding/subtracting hours does not follow
// this rule.
var getSetHour = makeGetSet('Hours', true);

// months
// week
// weekdays
// meridiem
var baseConfig = {
    calendar: defaultCalendar,
    longDateFormat: defaultLongDateFormat,
    invalidDate: defaultInvalidDate,
    ordinal: defaultOrdinal,
    dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
    relativeTime: defaultRelativeTime,

    months: defaultLocaleMonths,
    monthsShort: defaultLocaleMonthsShort,

    week: defaultLocaleWeek,

    weekdays: defaultLocaleWeekdays,
    weekdaysMin: defaultLocaleWeekdaysMin,
    weekdaysShort: defaultLocaleWeekdaysShort,

    meridiemParse: defaultLocaleMeridiemParse
};

// internal storage for locale config files
var locales = {};
var localeFamilies = {};
var globalLocale;

function normalizeLocale(key) {
    return key ? key.toLowerCase().replace('_', '-') : key;
}

// pick the locale from the array
// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
function chooseLocale(names) {
    var i = 0, j, next, locale, split;

    while (i < names.length) {
        split = normalizeLocale(names[i]).split('-');
        j = split.length;
        next = normalizeLocale(names[i + 1]);
        next = next ? next.split('-') : null;
        while (j > 0) {
            locale = loadLocale(split.slice(0, j).join('-'));
            if (locale) {
                return locale;
            }
            if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                //the next array item is better than a shallower substring of this one
                break;
            }
            j--;
        }
        i++;
    }
    return null;
}

function loadLocale(name) {
    var oldLocale = null;
    // TODO: Find a better way to register and load all the locales in Node
    if (!locales[name] && (typeof module !== 'undefined') &&
            module && module.exports) {
        try {
            oldLocale = globalLocale._abbr;
            var aliasedRequire = require;
            aliasedRequire('./locale/' + name);
            getSetGlobalLocale(oldLocale);
        } catch (e) {}
    }
    return locales[name];
}

// This function will load locale and then set the global locale.  If
// no arguments are passed in, it will simply return the current global
// locale key.
function getSetGlobalLocale (key, values) {
    var data;
    if (key) {
        if (isUndefined(values)) {
            data = getLocale(key);
        }
        else {
            data = defineLocale(key, values);
        }

        if (data) {
            // moment.duration._locale = moment._locale = data;
            globalLocale = data;
        }
    }

    return globalLocale._abbr;
}

function defineLocale (name, config) {
    if (config !== null) {
        var parentConfig = baseConfig;
        config.abbr = name;
        if (locales[name] != null) {
            deprecateSimple('defineLocaleOverride',
                    'use moment.updateLocale(localeName, config) to change ' +
                    'an existing locale. moment.defineLocale(localeName, ' +
                    'config) should only be used for creating a new locale ' +
                    'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
            parentConfig = locales[name]._config;
        } else if (config.parentLocale != null) {
            if (locales[config.parentLocale] != null) {
                parentConfig = locales[config.parentLocale]._config;
            } else {
                if (!localeFamilies[config.parentLocale]) {
                    localeFamilies[config.parentLocale] = [];
                }
                localeFamilies[config.parentLocale].push({
                    name: name,
                    config: config
                });
                return null;
            }
        }
        locales[name] = new Locale(mergeConfigs(parentConfig, config));

        if (localeFamilies[name]) {
            localeFamilies[name].forEach(function (x) {
                defineLocale(x.name, x.config);
            });
        }

        // backwards compat for now: also set the locale
        // make sure we set the locale AFTER all child locales have been
        // created, so we won't end up with the child locale set.
        getSetGlobalLocale(name);


        return locales[name];
    } else {
        // useful for testing
        delete locales[name];
        return null;
    }
}

function updateLocale(name, config) {
    if (config != null) {
        var locale, tmpLocale, parentConfig = baseConfig;
        // MERGE
        tmpLocale = loadLocale(name);
        if (tmpLocale != null) {
            parentConfig = tmpLocale._config;
        }
        config = mergeConfigs(parentConfig, config);
        locale = new Locale(config);
        locale.parentLocale = locales[name];
        locales[name] = locale;

        // backwards compat for now: also set the locale
        getSetGlobalLocale(name);
    } else {
        // pass null for config to unupdate, useful for tests
        if (locales[name] != null) {
            if (locales[name].parentLocale != null) {
                locales[name] = locales[name].parentLocale;
            } else if (locales[name] != null) {
                delete locales[name];
            }
        }
    }
    return locales[name];
}

// returns locale data
function getLocale (key) {
    var locale;

    if (key && key._locale && key._locale._abbr) {
        key = key._locale._abbr;
    }

    if (!key) {
        return globalLocale;
    }

    if (!isArray(key)) {
        //short-circuit everything else
        locale = loadLocale(key);
        if (locale) {
            return locale;
        }
        key = [key];
    }

    return chooseLocale(key);
}

function listLocales() {
    return keys(locales);
}

function checkOverflow (m) {
    var overflow;
    var a = m._a;

    if (a && getParsingFlags(m).overflow === -2) {
        overflow =
            a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
            a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
            a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
            a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
            a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
            a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
            -1;

        if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
            overflow = DATE;
        }
        if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
            overflow = WEEK;
        }
        if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
            overflow = WEEKDAY;
        }

        getParsingFlags(m).overflow = overflow;
    }

    return m;
}

// Pick the first defined of two or three arguments.
function defaults(a, b, c) {
    if (a != null) {
        return a;
    }
    if (b != null) {
        return b;
    }
    return c;
}

function currentDateArray(config) {
    // hooks is actually the exported moment object
    var nowValue = new Date(hooks.now());
    if (config._useUTC) {
        return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
    }
    return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
}

// convert an array to a date.
// the array should mirror the parameters below
// note: all values past the year are optional and will default to the lowest possible value.
// [year, month, day , hour, minute, second, millisecond]
function configFromArray (config) {
    var i, date, input = [], currentDate, yearToUse;

    if (config._d) {
        return;
    }

    currentDate = currentDateArray(config);

    //compute day of the year from weeks and weekdays
    if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
        dayOfYearFromWeekInfo(config);
    }

    //if the day of the year is set, figure out what it is
    if (config._dayOfYear != null) {
        yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

        if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
            getParsingFlags(config)._overflowDayOfYear = true;
        }

        date = createUTCDate(yearToUse, 0, config._dayOfYear);
        config._a[MONTH] = date.getUTCMonth();
        config._a[DATE] = date.getUTCDate();
    }

    // Default to current date.
    // * if no year, month, day of month are given, default to today
    // * if day of month is given, default month and year
    // * if month is given, default only year
    // * if year is given, don't default anything
    for (i = 0; i < 3 && config._a[i] == null; ++i) {
        config._a[i] = input[i] = currentDate[i];
    }

    // Zero out whatever was not defaulted, including time
    for (; i < 7; i++) {
        config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
    }

    // Check for 24:00:00.000
    if (config._a[HOUR] === 24 &&
            config._a[MINUTE] === 0 &&
            config._a[SECOND] === 0 &&
            config._a[MILLISECOND] === 0) {
        config._nextDay = true;
        config._a[HOUR] = 0;
    }

    config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
    // Apply timezone offset from input. The actual utcOffset can be changed
    // with parseZone.
    if (config._tzm != null) {
        config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
    }

    if (config._nextDay) {
        config._a[HOUR] = 24;
    }

    // check for mismatching day of week
    if (config._w && typeof config._w.d !== 'undefined' && config._w.d !== config._d.getDay()) {
        getParsingFlags(config).weekdayMismatch = true;
    }
}

function dayOfYearFromWeekInfo(config) {
    var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

    w = config._w;
    if (w.GG != null || w.W != null || w.E != null) {
        dow = 1;
        doy = 4;

        // TODO: We need to take the current isoWeekYear, but that depends on
        // how we interpret now (local, utc, fixed offset). So create
        // a now version of current config (take local/utc/offset flags, and
        // create now).
        weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
        week = defaults(w.W, 1);
        weekday = defaults(w.E, 1);
        if (weekday < 1 || weekday > 7) {
            weekdayOverflow = true;
        }
    } else {
        dow = config._locale._week.dow;
        doy = config._locale._week.doy;

        var curWeek = weekOfYear(createLocal(), dow, doy);

        weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

        // Default to current week.
        week = defaults(w.w, curWeek.week);

        if (w.d != null) {
            // weekday -- low day numbers are considered next week
            weekday = w.d;
            if (weekday < 0 || weekday > 6) {
                weekdayOverflow = true;
            }
        } else if (w.e != null) {
            // local weekday -- counting starts from begining of week
            weekday = w.e + dow;
            if (w.e < 0 || w.e > 6) {
                weekdayOverflow = true;
            }
        } else {
            // default to begining of week
            weekday = dow;
        }
    }
    if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
        getParsingFlags(config)._overflowWeeks = true;
    } else if (weekdayOverflow != null) {
        getParsingFlags(config)._overflowWeekday = true;
    } else {
        temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
        config._a[YEAR] = temp.year;
        config._dayOfYear = temp.dayOfYear;
    }
}

// iso 8601 regex
// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

var isoDates = [
    ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
    ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
    ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
    ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
    ['YYYY-DDD', /\d{4}-\d{3}/],
    ['YYYY-MM', /\d{4}-\d\d/, false],
    ['YYYYYYMMDD', /[+-]\d{10}/],
    ['YYYYMMDD', /\d{8}/],
    // YYYYMM is NOT allowed by the standard
    ['GGGG[W]WWE', /\d{4}W\d{3}/],
    ['GGGG[W]WW', /\d{4}W\d{2}/, false],
    ['YYYYDDD', /\d{7}/]
];

// iso time formats and regexes
var isoTimes = [
    ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
    ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
    ['HH:mm:ss', /\d\d:\d\d:\d\d/],
    ['HH:mm', /\d\d:\d\d/],
    ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
    ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
    ['HHmmss', /\d\d\d\d\d\d/],
    ['HHmm', /\d\d\d\d/],
    ['HH', /\d\d/]
];

var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

// date from iso format
function configFromISO(config) {
    var i, l,
        string = config._i,
        match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
        allowTime, dateFormat, timeFormat, tzFormat;

    if (match) {
        getParsingFlags(config).iso = true;

        for (i = 0, l = isoDates.length; i < l; i++) {
            if (isoDates[i][1].exec(match[1])) {
                dateFormat = isoDates[i][0];
                allowTime = isoDates[i][2] !== false;
                break;
            }
        }
        if (dateFormat == null) {
            config._isValid = false;
            return;
        }
        if (match[3]) {
            for (i = 0, l = isoTimes.length; i < l; i++) {
                if (isoTimes[i][1].exec(match[3])) {
                    // match[2] should be 'T' or space
                    timeFormat = (match[2] || ' ') + isoTimes[i][0];
                    break;
                }
            }
            if (timeFormat == null) {
                config._isValid = false;
                return;
            }
        }
        if (!allowTime && timeFormat != null) {
            config._isValid = false;
            return;
        }
        if (match[4]) {
            if (tzRegex.exec(match[4])) {
                tzFormat = 'Z';
            } else {
                config._isValid = false;
                return;
            }
        }
        config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
        configFromStringAndFormat(config);
    } else {
        config._isValid = false;
    }
}

// RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
var rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;

function extractFromRFC2822Strings(yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
    var result = [
        untruncateYear(yearStr),
        defaultLocaleMonthsShort.indexOf(monthStr),
        parseInt(dayStr, 10),
        parseInt(hourStr, 10),
        parseInt(minuteStr, 10)
    ];

    if (secondStr) {
        result.push(parseInt(secondStr, 10));
    }

    return result;
}

function untruncateYear(yearStr) {
    var year = parseInt(yearStr, 10);
    if (year <= 49) {
        return 2000 + year;
    } else if (year <= 999) {
        return 1900 + year;
    }
    return year;
}

function preprocessRFC2822(s) {
    // Remove comments and folding whitespace and replace multiple-spaces with a single space
    return s.replace(/\([^)]*\)|[\n\t]/g, ' ').replace(/(\s\s+)/g, ' ').trim();
}

function checkWeekday(weekdayStr, parsedInput, config) {
    if (weekdayStr) {
        // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
        var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr),
            weekdayActual = new Date(parsedInput[0], parsedInput[1], parsedInput[2]).getDay();
        if (weekdayProvided !== weekdayActual) {
            getParsingFlags(config).weekdayMismatch = true;
            config._isValid = false;
            return false;
        }
    }
    return true;
}

var obsOffsets = {
    UT: 0,
    GMT: 0,
    EDT: -4 * 60,
    EST: -5 * 60,
    CDT: -5 * 60,
    CST: -6 * 60,
    MDT: -6 * 60,
    MST: -7 * 60,
    PDT: -7 * 60,
    PST: -8 * 60
};

function calculateOffset(obsOffset, militaryOffset, numOffset) {
    if (obsOffset) {
        return obsOffsets[obsOffset];
    } else if (militaryOffset) {
        // the only allowed military tz is Z
        return 0;
    } else {
        var hm = parseInt(numOffset, 10);
        var m = hm % 100, h = (hm - m) / 100;
        return h * 60 + m;
    }
}

// date and time from ref 2822 format
function configFromRFC2822(config) {
    var match = rfc2822.exec(preprocessRFC2822(config._i));
    if (match) {
        var parsedArray = extractFromRFC2822Strings(match[4], match[3], match[2], match[5], match[6], match[7]);
        if (!checkWeekday(match[1], parsedArray, config)) {
            return;
        }

        config._a = parsedArray;
        config._tzm = calculateOffset(match[8], match[9], match[10]);

        config._d = createUTCDate.apply(null, config._a);
        config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);

        getParsingFlags(config).rfc2822 = true;
    } else {
        config._isValid = false;
    }
}

// date from iso format or fallback
function configFromString(config) {
    var matched = aspNetJsonRegex.exec(config._i);

    if (matched !== null) {
        config._d = new Date(+matched[1]);
        return;
    }

    configFromISO(config);
    if (config._isValid === false) {
        delete config._isValid;
    } else {
        return;
    }

    configFromRFC2822(config);
    if (config._isValid === false) {
        delete config._isValid;
    } else {
        return;
    }

    // Final attempt, use Input Fallback
    hooks.createFromInputFallback(config);
}

hooks.createFromInputFallback = deprecate(
    'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
    'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
    'discouraged and will be removed in an upcoming major release. Please refer to ' +
    'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
    function (config) {
        config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
    }
);

// constant that refers to the ISO standard
hooks.ISO_8601 = function () {};

// constant that refers to the RFC 2822 form
hooks.RFC_2822 = function () {};

// date from string and format string
function configFromStringAndFormat(config) {
    // TODO: Move this to another part of the creation flow to prevent circular deps
    if (config._f === hooks.ISO_8601) {
        configFromISO(config);
        return;
    }
    if (config._f === hooks.RFC_2822) {
        configFromRFC2822(config);
        return;
    }
    config._a = [];
    getParsingFlags(config).empty = true;

    // This array is used to make a Date, either with `new Date` or `Date.UTC`
    var string = '' + config._i,
        i, parsedInput, tokens, token, skipped,
        stringLength = string.length,
        totalParsedInputLength = 0;

    tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

    for (i = 0; i < tokens.length; i++) {
        token = tokens[i];
        parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
        // console.log('token', token, 'parsedInput', parsedInput,
        //         'regex', getParseRegexForToken(token, config));
        if (parsedInput) {
            skipped = string.substr(0, string.indexOf(parsedInput));
            if (skipped.length > 0) {
                getParsingFlags(config).unusedInput.push(skipped);
            }
            string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
            totalParsedInputLength += parsedInput.length;
        }
        // don't parse if it's not a known token
        if (formatTokenFunctions[token]) {
            if (parsedInput) {
                getParsingFlags(config).empty = false;
            }
            else {
                getParsingFlags(config).unusedTokens.push(token);
            }
            addTimeToArrayFromToken(token, parsedInput, config);
        }
        else if (config._strict && !parsedInput) {
            getParsingFlags(config).unusedTokens.push(token);
        }
    }

    // add remaining unparsed input length to the string
    getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
    if (string.length > 0) {
        getParsingFlags(config).unusedInput.push(string);
    }

    // clear _12h flag if hour is <= 12
    if (config._a[HOUR] <= 12 &&
        getParsingFlags(config).bigHour === true &&
        config._a[HOUR] > 0) {
        getParsingFlags(config).bigHour = undefined;
    }

    getParsingFlags(config).parsedDateParts = config._a.slice(0);
    getParsingFlags(config).meridiem = config._meridiem;
    // handle meridiem
    config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

    configFromArray(config);
    checkOverflow(config);
}


function meridiemFixWrap (locale, hour, meridiem) {
    var isPm;

    if (meridiem == null) {
        // nothing to do
        return hour;
    }
    if (locale.meridiemHour != null) {
        return locale.meridiemHour(hour, meridiem);
    } else if (locale.isPM != null) {
        // Fallback
        isPm = locale.isPM(meridiem);
        if (isPm && hour < 12) {
            hour += 12;
        }
        if (!isPm && hour === 12) {
            hour = 0;
        }
        return hour;
    } else {
        // this is not supposed to happen
        return hour;
    }
}

// date from string and array of format strings
function configFromStringAndArray(config) {
    var tempConfig,
        bestMoment,

        scoreToBeat,
        i,
        currentScore;

    if (config._f.length === 0) {
        getParsingFlags(config).invalidFormat = true;
        config._d = new Date(NaN);
        return;
    }

    for (i = 0; i < config._f.length; i++) {
        currentScore = 0;
        tempConfig = copyConfig({}, config);
        if (config._useUTC != null) {
            tempConfig._useUTC = config._useUTC;
        }
        tempConfig._f = config._f[i];
        configFromStringAndFormat(tempConfig);

        if (!isValid(tempConfig)) {
            continue;
        }

        // if there is any input that was not parsed add a penalty for that format
        currentScore += getParsingFlags(tempConfig).charsLeftOver;

        //or tokens
        currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

        getParsingFlags(tempConfig).score = currentScore;

        if (scoreToBeat == null || currentScore < scoreToBeat) {
            scoreToBeat = currentScore;
            bestMoment = tempConfig;
        }
    }

    extend(config, bestMoment || tempConfig);
}

function configFromObject(config) {
    if (config._d) {
        return;
    }

    var i = normalizeObjectUnits(config._i);
    config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
        return obj && parseInt(obj, 10);
    });

    configFromArray(config);
}

function createFromConfig (config) {
    var res = new Moment(checkOverflow(prepareConfig(config)));
    if (res._nextDay) {
        // Adding is smart enough around DST
        res.add(1, 'd');
        res._nextDay = undefined;
    }

    return res;
}

function prepareConfig (config) {
    var input = config._i,
        format = config._f;

    config._locale = config._locale || getLocale(config._l);

    if (input === null || (format === undefined && input === '')) {
        return createInvalid({nullInput: true});
    }

    if (typeof input === 'string') {
        config._i = input = config._locale.preparse(input);
    }

    if (isMoment(input)) {
        return new Moment(checkOverflow(input));
    } else if (isDate(input)) {
        config._d = input;
    } else if (isArray(format)) {
        configFromStringAndArray(config);
    } else if (format) {
        configFromStringAndFormat(config);
    }  else {
        configFromInput(config);
    }

    if (!isValid(config)) {
        config._d = null;
    }

    return config;
}

function configFromInput(config) {
    var input = config._i;
    if (isUndefined(input)) {
        config._d = new Date(hooks.now());
    } else if (isDate(input)) {
        config._d = new Date(input.valueOf());
    } else if (typeof input === 'string') {
        configFromString(config);
    } else if (isArray(input)) {
        config._a = map(input.slice(0), function (obj) {
            return parseInt(obj, 10);
        });
        configFromArray(config);
    } else if (isObject(input)) {
        configFromObject(config);
    } else if (isNumber(input)) {
        // from milliseconds
        config._d = new Date(input);
    } else {
        hooks.createFromInputFallback(config);
    }
}

function createLocalOrUTC (input, format, locale, strict, isUTC) {
    var c = {};

    if (locale === true || locale === false) {
        strict = locale;
        locale = undefined;
    }

    if ((isObject(input) && isObjectEmpty(input)) ||
            (isArray(input) && input.length === 0)) {
        input = undefined;
    }
    // object construction must be done this way.
    // https://github.com/moment/moment/issues/1423
    c._isAMomentObject = true;
    c._useUTC = c._isUTC = isUTC;
    c._l = locale;
    c._i = input;
    c._f = format;
    c._strict = strict;

    return createFromConfig(c);
}

function createLocal (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, false);
}

var prototypeMin = deprecate(
    'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other < this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

var prototypeMax = deprecate(
    'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other > this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

// Pick a moment m from moments so that m[fn](other) is true for all
// other. This relies on the function fn to be transitive.
//
// moments should either be an array of moment objects or an array, whose
// first element is an array of moment objects.
function pickBy(fn, moments) {
    var res, i;
    if (moments.length === 1 && isArray(moments[0])) {
        moments = moments[0];
    }
    if (!moments.length) {
        return createLocal();
    }
    res = moments[0];
    for (i = 1; i < moments.length; ++i) {
        if (!moments[i].isValid() || moments[i][fn](res)) {
            res = moments[i];
        }
    }
    return res;
}

// TODO: Use [].sort instead?
function min () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isBefore', args);
}

function max () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isAfter', args);
}

var now = function () {
    return Date.now ? Date.now() : +(new Date());
};

var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

function isDurationValid(m) {
    for (var key in m) {
        if (!(indexOf.call(ordering, key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
            return false;
        }
    }

    var unitHasDecimal = false;
    for (var i = 0; i < ordering.length; ++i) {
        if (m[ordering[i]]) {
            if (unitHasDecimal) {
                return false; // only allow non-integers for smallest unit
            }
            if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                unitHasDecimal = true;
            }
        }
    }

    return true;
}

function isValid$1() {
    return this._isValid;
}

function createInvalid$1() {
    return createDuration(NaN);
}

function Duration (duration) {
    var normalizedInput = normalizeObjectUnits(duration),
        years = normalizedInput.year || 0,
        quarters = normalizedInput.quarter || 0,
        months = normalizedInput.month || 0,
        weeks = normalizedInput.week || 0,
        days = normalizedInput.day || 0,
        hours = normalizedInput.hour || 0,
        minutes = normalizedInput.minute || 0,
        seconds = normalizedInput.second || 0,
        milliseconds = normalizedInput.millisecond || 0;

    this._isValid = isDurationValid(normalizedInput);

    // representation for dateAddRemove
    this._milliseconds = +milliseconds +
        seconds * 1e3 + // 1000
        minutes * 6e4 + // 1000 * 60
        hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
    // Because of dateAddRemove treats 24 hours as different from a
    // day when working around DST, we need to store them separately
    this._days = +days +
        weeks * 7;
    // It is impossible to translate months into days without knowing
    // which months you are are talking about, so we have to store
    // it separately.
    this._months = +months +
        quarters * 3 +
        years * 12;

    this._data = {};

    this._locale = getLocale();

    this._bubble();
}

function isDuration (obj) {
    return obj instanceof Duration;
}

function absRound (number) {
    if (number < 0) {
        return Math.round(-1 * number) * -1;
    } else {
        return Math.round(number);
    }
}

// FORMATTING

function offset (token, separator) {
    addFormatToken(token, 0, 0, function () {
        var offset = this.utcOffset();
        var sign = '+';
        if (offset < 0) {
            offset = -offset;
            sign = '-';
        }
        return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
    });
}

offset('Z', ':');
offset('ZZ', '');

// PARSING

addRegexToken('Z',  matchShortOffset);
addRegexToken('ZZ', matchShortOffset);
addParseToken(['Z', 'ZZ'], function (input, array, config) {
    config._useUTC = true;
    config._tzm = offsetFromString(matchShortOffset, input);
});

// HELPERS

// timezone chunker
// '+10:00' > ['10',  '00']
// '-1530'  > ['-15', '30']
var chunkOffset = /([\+\-]|\d\d)/gi;

function offsetFromString(matcher, string) {
    var matches = (string || '').match(matcher);

    if (matches === null) {
        return null;
    }

    var chunk   = matches[matches.length - 1] || [];
    var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
    var minutes = +(parts[1] * 60) + toInt(parts[2]);

    return minutes === 0 ?
      0 :
      parts[0] === '+' ? minutes : -minutes;
}

// Return a moment from input, that is local/utc/zone equivalent to model.
function cloneWithOffset(input, model) {
    var res, diff;
    if (model._isUTC) {
        res = model.clone();
        diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
        // Use low-level api, because this fn is low-level api.
        res._d.setTime(res._d.valueOf() + diff);
        hooks.updateOffset(res, false);
        return res;
    } else {
        return createLocal(input).local();
    }
}

function getDateOffset (m) {
    // On Firefox.24 Date#getTimezoneOffset returns a floating point.
    // https://github.com/moment/moment/pull/1871
    return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
}

// HOOKS

// This function will be called whenever a moment is mutated.
// It is intended to keep the offset in sync with the timezone.
hooks.updateOffset = function () {};

// MOMENTS

// keepLocalTime = true means only change the timezone, without
// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
// +0200, so we adjust the time as needed, to be valid.
//
// Keeping the time actually adds/subtracts (one hour)
// from the actual represented time. That is why we call updateOffset
// a second time. In case it wants us to change the offset again
// _changeInProgress == true case, then we have to adjust, because
// there is no such time in the given timezone.
function getSetOffset (input, keepLocalTime, keepMinutes) {
    var offset = this._offset || 0,
        localAdjust;
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    if (input != null) {
        if (typeof input === 'string') {
            input = offsetFromString(matchShortOffset, input);
            if (input === null) {
                return this;
            }
        } else if (Math.abs(input) < 16 && !keepMinutes) {
            input = input * 60;
        }
        if (!this._isUTC && keepLocalTime) {
            localAdjust = getDateOffset(this);
        }
        this._offset = input;
        this._isUTC = true;
        if (localAdjust != null) {
            this.add(localAdjust, 'm');
        }
        if (offset !== input) {
            if (!keepLocalTime || this._changeInProgress) {
                addSubtract(this, createDuration(input - offset, 'm'), 1, false);
            } else if (!this._changeInProgress) {
                this._changeInProgress = true;
                hooks.updateOffset(this, true);
                this._changeInProgress = null;
            }
        }
        return this;
    } else {
        return this._isUTC ? offset : getDateOffset(this);
    }
}

function getSetZone (input, keepLocalTime) {
    if (input != null) {
        if (typeof input !== 'string') {
            input = -input;
        }

        this.utcOffset(input, keepLocalTime);

        return this;
    } else {
        return -this.utcOffset();
    }
}

function setOffsetToUTC (keepLocalTime) {
    return this.utcOffset(0, keepLocalTime);
}

function setOffsetToLocal (keepLocalTime) {
    if (this._isUTC) {
        this.utcOffset(0, keepLocalTime);
        this._isUTC = false;

        if (keepLocalTime) {
            this.subtract(getDateOffset(this), 'm');
        }
    }
    return this;
}

function setOffsetToParsedOffset () {
    if (this._tzm != null) {
        this.utcOffset(this._tzm, false, true);
    } else if (typeof this._i === 'string') {
        var tZone = offsetFromString(matchOffset, this._i);
        if (tZone != null) {
            this.utcOffset(tZone);
        }
        else {
            this.utcOffset(0, true);
        }
    }
    return this;
}

function hasAlignedHourOffset (input) {
    if (!this.isValid()) {
        return false;
    }
    input = input ? createLocal(input).utcOffset() : 0;

    return (this.utcOffset() - input) % 60 === 0;
}

function isDaylightSavingTime () {
    return (
        this.utcOffset() > this.clone().month(0).utcOffset() ||
        this.utcOffset() > this.clone().month(5).utcOffset()
    );
}

function isDaylightSavingTimeShifted () {
    if (!isUndefined(this._isDSTShifted)) {
        return this._isDSTShifted;
    }

    var c = {};

    copyConfig(c, this);
    c = prepareConfig(c);

    if (c._a) {
        var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
        this._isDSTShifted = this.isValid() &&
            compareArrays(c._a, other.toArray()) > 0;
    } else {
        this._isDSTShifted = false;
    }

    return this._isDSTShifted;
}

function isLocal () {
    return this.isValid() ? !this._isUTC : false;
}

function isUtcOffset () {
    return this.isValid() ? this._isUTC : false;
}

function isUtc () {
    return this.isValid() ? this._isUTC && this._offset === 0 : false;
}

// ASP.NET json date format regex
var aspNetRegex = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

// from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
// and further modified to allow for strings containing both week and day
var isoRegex = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

function createDuration (input, key) {
    var duration = input,
        // matching against regexp is expensive, do it on demand
        match = null,
        sign,
        ret,
        diffRes;

    if (isDuration(input)) {
        duration = {
            ms : input._milliseconds,
            d  : input._days,
            M  : input._months
        };
    } else if (isNumber(input)) {
        duration = {};
        if (key) {
            duration[key] = input;
        } else {
            duration.milliseconds = input;
        }
    } else if (!!(match = aspNetRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : 1;
        duration = {
            y  : 0,
            d  : toInt(match[DATE])                         * sign,
            h  : toInt(match[HOUR])                         * sign,
            m  : toInt(match[MINUTE])                       * sign,
            s  : toInt(match[SECOND])                       * sign,
            ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
        };
    } else if (!!(match = isoRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : (match[1] === '+') ? 1 : 1;
        duration = {
            y : parseIso(match[2], sign),
            M : parseIso(match[3], sign),
            w : parseIso(match[4], sign),
            d : parseIso(match[5], sign),
            h : parseIso(match[6], sign),
            m : parseIso(match[7], sign),
            s : parseIso(match[8], sign)
        };
    } else if (duration == null) {// checks for null or undefined
        duration = {};
    } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
        diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

        duration = {};
        duration.ms = diffRes.milliseconds;
        duration.M = diffRes.months;
    }

    ret = new Duration(duration);

    if (isDuration(input) && hasOwnProp(input, '_locale')) {
        ret._locale = input._locale;
    }

    return ret;
}

createDuration.fn = Duration.prototype;
createDuration.invalid = createInvalid$1;

function parseIso (inp, sign) {
    // We'd normally use ~~inp for this, but unfortunately it also
    // converts floats to ints.
    // inp may be undefined, so careful calling replace on it.
    var res = inp && parseFloat(inp.replace(',', '.'));
    // apply sign while we're at it
    return (isNaN(res) ? 0 : res) * sign;
}

function positiveMomentsDifference(base, other) {
    var res = {milliseconds: 0, months: 0};

    res.months = other.month() - base.month() +
        (other.year() - base.year()) * 12;
    if (base.clone().add(res.months, 'M').isAfter(other)) {
        --res.months;
    }

    res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

    return res;
}

function momentsDifference(base, other) {
    var res;
    if (!(base.isValid() && other.isValid())) {
        return {milliseconds: 0, months: 0};
    }

    other = cloneWithOffset(other, base);
    if (base.isBefore(other)) {
        res = positiveMomentsDifference(base, other);
    } else {
        res = positiveMomentsDifference(other, base);
        res.milliseconds = -res.milliseconds;
        res.months = -res.months;
    }

    return res;
}

// TODO: remove 'name' arg after deprecation is removed
function createAdder(direction, name) {
    return function (val, period) {
        var dur, tmp;
        //invert the arguments, but complain about it
        if (period !== null && !isNaN(+period)) {
            deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
            'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
            tmp = val; val = period; period = tmp;
        }

        val = typeof val === 'string' ? +val : val;
        dur = createDuration(val, period);
        addSubtract(this, dur, direction);
        return this;
    };
}

function addSubtract (mom, duration, isAdding, updateOffset) {
    var milliseconds = duration._milliseconds,
        days = absRound(duration._days),
        months = absRound(duration._months);

    if (!mom.isValid()) {
        // No op
        return;
    }

    updateOffset = updateOffset == null ? true : updateOffset;

    if (months) {
        setMonth(mom, get(mom, 'Month') + months * isAdding);
    }
    if (days) {
        set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
    }
    if (milliseconds) {
        mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
    }
    if (updateOffset) {
        hooks.updateOffset(mom, days || months);
    }
}

var add      = createAdder(1, 'add');
var subtract = createAdder(-1, 'subtract');

function getCalendarFormat(myMoment, now) {
    var diff = myMoment.diff(now, 'days', true);
    return diff < -6 ? 'sameElse' :
            diff < -1 ? 'lastWeek' :
            diff < 0 ? 'lastDay' :
            diff < 1 ? 'sameDay' :
            diff < 2 ? 'nextDay' :
            diff < 7 ? 'nextWeek' : 'sameElse';
}

function calendar$1 (time, formats) {
    // We want to compare the start of today, vs this.
    // Getting start-of-today depends on whether we're local/utc/offset or not.
    var now = time || createLocal(),
        sod = cloneWithOffset(now, this).startOf('day'),
        format = hooks.calendarFormat(this, sod) || 'sameElse';

    var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

    return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
}

function clone () {
    return new Moment(this);
}

function isAfter (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() > localInput.valueOf();
    } else {
        return localInput.valueOf() < this.clone().startOf(units).valueOf();
    }
}

function isBefore (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() < localInput.valueOf();
    } else {
        return this.clone().endOf(units).valueOf() < localInput.valueOf();
    }
}

function isBetween (from, to, units, inclusivity) {
    inclusivity = inclusivity || '()';
    return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) &&
        (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
}

function isSame (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input),
        inputMs;
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(units || 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() === localInput.valueOf();
    } else {
        inputMs = localInput.valueOf();
        return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
    }
}

function isSameOrAfter (input, units) {
    return this.isSame(input, units) || this.isAfter(input,units);
}

function isSameOrBefore (input, units) {
    return this.isSame(input, units) || this.isBefore(input,units);
}

function diff (input, units, asFloat) {
    var that,
        zoneDelta,
        delta, output;

    if (!this.isValid()) {
        return NaN;
    }

    that = cloneWithOffset(input, this);

    if (!that.isValid()) {
        return NaN;
    }

    zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

    units = normalizeUnits(units);

    switch (units) {
        case 'year': output = monthDiff(this, that) / 12; break;
        case 'month': output = monthDiff(this, that); break;
        case 'quarter': output = monthDiff(this, that) / 3; break;
        case 'second': output = (this - that) / 1e3; break; // 1000
        case 'minute': output = (this - that) / 6e4; break; // 1000 * 60
        case 'hour': output = (this - that) / 36e5; break; // 1000 * 60 * 60
        case 'day': output = (this - that - zoneDelta) / 864e5; break; // 1000 * 60 * 60 * 24, negate dst
        case 'week': output = (this - that - zoneDelta) / 6048e5; break; // 1000 * 60 * 60 * 24 * 7, negate dst
        default: output = this - that;
    }

    return asFloat ? output : absFloor(output);
}

function monthDiff (a, b) {
    // difference in months
    var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
        // b is in (anchor - 1 month, anchor + 1 month)
        anchor = a.clone().add(wholeMonthDiff, 'months'),
        anchor2, adjust;

    if (b - anchor < 0) {
        anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor - anchor2);
    } else {
        anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor2 - anchor);
    }

    //check for negative zero, return zero if negative zero
    return -(wholeMonthDiff + adjust) || 0;
}

hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

function toString () {
    return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
}

function toISOString() {
    if (!this.isValid()) {
        return null;
    }
    var m = this.clone().utc();
    if (m.year() < 0 || m.year() > 9999) {
        return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    }
    if (isFunction(Date.prototype.toISOString)) {
        // native implementation is ~50x faster, use it when we can
        return this.toDate().toISOString();
    }
    return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
}

/**
 * Return a human readable representation of a moment that can
 * also be evaluated to get a new moment which is the same
 *
 * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
 */
function inspect () {
    if (!this.isValid()) {
        return 'moment.invalid(/* ' + this._i + ' */)';
    }
    var func = 'moment';
    var zone = '';
    if (!this.isLocal()) {
        func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
        zone = 'Z';
    }
    var prefix = '[' + func + '("]';
    var year = (0 <= this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
    var datetime = '-MM-DD[T]HH:mm:ss.SSS';
    var suffix = zone + '[")]';

    return this.format(prefix + year + datetime + suffix);
}

function format (inputString) {
    if (!inputString) {
        inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
    }
    var output = formatMoment(this, inputString);
    return this.localeData().postformat(output);
}

function from (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function fromNow (withoutSuffix) {
    return this.from(createLocal(), withoutSuffix);
}

function to (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function toNow (withoutSuffix) {
    return this.to(createLocal(), withoutSuffix);
}

// If passed a locale key, it will set the locale for this
// instance.  Otherwise, it will return the locale configuration
// variables for this instance.
function locale (key) {
    var newLocaleData;

    if (key === undefined) {
        return this._locale._abbr;
    } else {
        newLocaleData = getLocale(key);
        if (newLocaleData != null) {
            this._locale = newLocaleData;
        }
        return this;
    }
}

var lang = deprecate(
    'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
    function (key) {
        if (key === undefined) {
            return this.localeData();
        } else {
            return this.locale(key);
        }
    }
);

function localeData () {
    return this._locale;
}

function startOf (units) {
    units = normalizeUnits(units);
    // the following switch intentionally omits break keywords
    // to utilize falling through the cases.
    switch (units) {
        case 'year':
            this.month(0);
            /* falls through */
        case 'quarter':
        case 'month':
            this.date(1);
            /* falls through */
        case 'week':
        case 'isoWeek':
        case 'day':
        case 'date':
            this.hours(0);
            /* falls through */
        case 'hour':
            this.minutes(0);
            /* falls through */
        case 'minute':
            this.seconds(0);
            /* falls through */
        case 'second':
            this.milliseconds(0);
    }

    // weeks are a special case
    if (units === 'week') {
        this.weekday(0);
    }
    if (units === 'isoWeek') {
        this.isoWeekday(1);
    }

    // quarters are also special
    if (units === 'quarter') {
        this.month(Math.floor(this.month() / 3) * 3);
    }

    return this;
}

function endOf (units) {
    units = normalizeUnits(units);
    if (units === undefined || units === 'millisecond') {
        return this;
    }

    // 'date' is an alias for 'day', so it should be considered as such.
    if (units === 'date') {
        units = 'day';
    }

    return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
}

function valueOf () {
    return this._d.valueOf() - ((this._offset || 0) * 60000);
}

function unix () {
    return Math.floor(this.valueOf() / 1000);
}

function toDate () {
    return new Date(this.valueOf());
}

function toArray () {
    var m = this;
    return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
}

function toObject () {
    var m = this;
    return {
        years: m.year(),
        months: m.month(),
        date: m.date(),
        hours: m.hours(),
        minutes: m.minutes(),
        seconds: m.seconds(),
        milliseconds: m.milliseconds()
    };
}

function toJSON () {
    // new Date(NaN).toJSON() === null
    return this.isValid() ? this.toISOString() : null;
}

function isValid$2 () {
    return isValid(this);
}

function parsingFlags () {
    return extend({}, getParsingFlags(this));
}

function invalidAt () {
    return getParsingFlags(this).overflow;
}

function creationData() {
    return {
        input: this._i,
        format: this._f,
        locale: this._locale,
        isUTC: this._isUTC,
        strict: this._strict
    };
}

// FORMATTING

addFormatToken(0, ['gg', 2], 0, function () {
    return this.weekYear() % 100;
});

addFormatToken(0, ['GG', 2], 0, function () {
    return this.isoWeekYear() % 100;
});

function addWeekYearFormatToken (token, getter) {
    addFormatToken(0, [token, token.length], 0, getter);
}

addWeekYearFormatToken('gggg',     'weekYear');
addWeekYearFormatToken('ggggg',    'weekYear');
addWeekYearFormatToken('GGGG',  'isoWeekYear');
addWeekYearFormatToken('GGGGG', 'isoWeekYear');

// ALIASES

addUnitAlias('weekYear', 'gg');
addUnitAlias('isoWeekYear', 'GG');

// PRIORITY

addUnitPriority('weekYear', 1);
addUnitPriority('isoWeekYear', 1);


// PARSING

addRegexToken('G',      matchSigned);
addRegexToken('g',      matchSigned);
addRegexToken('GG',     match1to2, match2);
addRegexToken('gg',     match1to2, match2);
addRegexToken('GGGG',   match1to4, match4);
addRegexToken('gggg',   match1to4, match4);
addRegexToken('GGGGG',  match1to6, match6);
addRegexToken('ggggg',  match1to6, match6);

addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
    week[token.substr(0, 2)] = toInt(input);
});

addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
    week[token] = hooks.parseTwoDigitYear(input);
});

// MOMENTS

function getSetWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input,
            this.week(),
            this.weekday(),
            this.localeData()._week.dow,
            this.localeData()._week.doy);
}

function getSetISOWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input, this.isoWeek(), this.isoWeekday(), 1, 4);
}

function getISOWeeksInYear () {
    return weeksInYear(this.year(), 1, 4);
}

function getWeeksInYear () {
    var weekInfo = this.localeData()._week;
    return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
}

function getSetWeekYearHelper(input, week, weekday, dow, doy) {
    var weeksTarget;
    if (input == null) {
        return weekOfYear(this, dow, doy).year;
    } else {
        weeksTarget = weeksInYear(input, dow, doy);
        if (week > weeksTarget) {
            week = weeksTarget;
        }
        return setWeekAll.call(this, input, week, weekday, dow, doy);
    }
}

function setWeekAll(weekYear, week, weekday, dow, doy) {
    var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
        date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

    this.year(date.getUTCFullYear());
    this.month(date.getUTCMonth());
    this.date(date.getUTCDate());
    return this;
}

// FORMATTING

addFormatToken('Q', 0, 'Qo', 'quarter');

// ALIASES

addUnitAlias('quarter', 'Q');

// PRIORITY

addUnitPriority('quarter', 7);

// PARSING

addRegexToken('Q', match1);
addParseToken('Q', function (input, array) {
    array[MONTH] = (toInt(input) - 1) * 3;
});

// MOMENTS

function getSetQuarter (input) {
    return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
}

// FORMATTING

addFormatToken('D', ['DD', 2], 'Do', 'date');

// ALIASES

addUnitAlias('date', 'D');

// PRIOROITY
addUnitPriority('date', 9);

// PARSING

addRegexToken('D',  match1to2);
addRegexToken('DD', match1to2, match2);
addRegexToken('Do', function (isStrict, locale) {
    // TODO: Remove "ordinalParse" fallback in next major release.
    return isStrict ?
      (locale._dayOfMonthOrdinalParse || locale._ordinalParse) :
      locale._dayOfMonthOrdinalParseLenient;
});

addParseToken(['D', 'DD'], DATE);
addParseToken('Do', function (input, array) {
    array[DATE] = toInt(input.match(match1to2)[0], 10);
});

// MOMENTS

var getSetDayOfMonth = makeGetSet('Date', true);

// FORMATTING

addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

// ALIASES

addUnitAlias('dayOfYear', 'DDD');

// PRIORITY
addUnitPriority('dayOfYear', 4);

// PARSING

addRegexToken('DDD',  match1to3);
addRegexToken('DDDD', match3);
addParseToken(['DDD', 'DDDD'], function (input, array, config) {
    config._dayOfYear = toInt(input);
});

// HELPERS

// MOMENTS

function getSetDayOfYear (input) {
    var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
    return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
}

// FORMATTING

addFormatToken('m', ['mm', 2], 0, 'minute');

// ALIASES

addUnitAlias('minute', 'm');

// PRIORITY

addUnitPriority('minute', 14);

// PARSING

addRegexToken('m',  match1to2);
addRegexToken('mm', match1to2, match2);
addParseToken(['m', 'mm'], MINUTE);

// MOMENTS

var getSetMinute = makeGetSet('Minutes', false);

// FORMATTING

addFormatToken('s', ['ss', 2], 0, 'second');

// ALIASES

addUnitAlias('second', 's');

// PRIORITY

addUnitPriority('second', 15);

// PARSING

addRegexToken('s',  match1to2);
addRegexToken('ss', match1to2, match2);
addParseToken(['s', 'ss'], SECOND);

// MOMENTS

var getSetSecond = makeGetSet('Seconds', false);

// FORMATTING

addFormatToken('S', 0, 0, function () {
    return ~~(this.millisecond() / 100);
});

addFormatToken(0, ['SS', 2], 0, function () {
    return ~~(this.millisecond() / 10);
});

addFormatToken(0, ['SSS', 3], 0, 'millisecond');
addFormatToken(0, ['SSSS', 4], 0, function () {
    return this.millisecond() * 10;
});
addFormatToken(0, ['SSSSS', 5], 0, function () {
    return this.millisecond() * 100;
});
addFormatToken(0, ['SSSSSS', 6], 0, function () {
    return this.millisecond() * 1000;
});
addFormatToken(0, ['SSSSSSS', 7], 0, function () {
    return this.millisecond() * 10000;
});
addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
    return this.millisecond() * 100000;
});
addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
    return this.millisecond() * 1000000;
});


// ALIASES

addUnitAlias('millisecond', 'ms');

// PRIORITY

addUnitPriority('millisecond', 16);

// PARSING

addRegexToken('S',    match1to3, match1);
addRegexToken('SS',   match1to3, match2);
addRegexToken('SSS',  match1to3, match3);

var token;
for (token = 'SSSS'; token.length <= 9; token += 'S') {
    addRegexToken(token, matchUnsigned);
}

function parseMs(input, array) {
    array[MILLISECOND] = toInt(('0.' + input) * 1000);
}

for (token = 'S'; token.length <= 9; token += 'S') {
    addParseToken(token, parseMs);
}
// MOMENTS

var getSetMillisecond = makeGetSet('Milliseconds', false);

// FORMATTING

addFormatToken('z',  0, 0, 'zoneAbbr');
addFormatToken('zz', 0, 0, 'zoneName');

// MOMENTS

function getZoneAbbr () {
    return this._isUTC ? 'UTC' : '';
}

function getZoneName () {
    return this._isUTC ? 'Coordinated Universal Time' : '';
}

var proto = Moment.prototype;

proto.add               = add;
proto.calendar          = calendar$1;
proto.clone             = clone;
proto.diff              = diff;
proto.endOf             = endOf;
proto.format            = format;
proto.from              = from;
proto.fromNow           = fromNow;
proto.to                = to;
proto.toNow             = toNow;
proto.get               = stringGet;
proto.invalidAt         = invalidAt;
proto.isAfter           = isAfter;
proto.isBefore          = isBefore;
proto.isBetween         = isBetween;
proto.isSame            = isSame;
proto.isSameOrAfter     = isSameOrAfter;
proto.isSameOrBefore    = isSameOrBefore;
proto.isValid           = isValid$2;
proto.lang              = lang;
proto.locale            = locale;
proto.localeData        = localeData;
proto.max               = prototypeMax;
proto.min               = prototypeMin;
proto.parsingFlags      = parsingFlags;
proto.set               = stringSet;
proto.startOf           = startOf;
proto.subtract          = subtract;
proto.toArray           = toArray;
proto.toObject          = toObject;
proto.toDate            = toDate;
proto.toISOString       = toISOString;
proto.inspect           = inspect;
proto.toJSON            = toJSON;
proto.toString          = toString;
proto.unix              = unix;
proto.valueOf           = valueOf;
proto.creationData      = creationData;

// Year
proto.year       = getSetYear;
proto.isLeapYear = getIsLeapYear;

// Week Year
proto.weekYear    = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;

// Quarter
proto.quarter = proto.quarters = getSetQuarter;

// Month
proto.month       = getSetMonth;
proto.daysInMonth = getDaysInMonth;

// Week
proto.week           = proto.weeks        = getSetWeek;
proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
proto.weeksInYear    = getWeeksInYear;
proto.isoWeeksInYear = getISOWeeksInYear;

// Day
proto.date       = getSetDayOfMonth;
proto.day        = proto.days             = getSetDayOfWeek;
proto.weekday    = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear  = getSetDayOfYear;

// Hour
proto.hour = proto.hours = getSetHour;

// Minute
proto.minute = proto.minutes = getSetMinute;

// Second
proto.second = proto.seconds = getSetSecond;

// Millisecond
proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
proto.utcOffset            = getSetOffset;
proto.utc                  = setOffsetToUTC;
proto.local                = setOffsetToLocal;
proto.parseZone            = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST                = isDaylightSavingTime;
proto.isLocal              = isLocal;
proto.isUtcOffset          = isUtcOffset;
proto.isUtc                = isUtc;
proto.isUTC                = isUtc;

// Timezone
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;

// Deprecations
proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

function createUnix (input) {
    return createLocal(input * 1000);
}

function createInZone () {
    return createLocal.apply(null, arguments).parseZone();
}

function preParsePostFormat (string) {
    return string;
}

var proto$1 = Locale.prototype;

proto$1.calendar        = calendar;
proto$1.longDateFormat  = longDateFormat;
proto$1.invalidDate     = invalidDate;
proto$1.ordinal         = ordinal;
proto$1.preparse        = preParsePostFormat;
proto$1.postformat      = preParsePostFormat;
proto$1.relativeTime    = relativeTime;
proto$1.pastFuture      = pastFuture;
proto$1.set             = set;

// Month
proto$1.months            =        localeMonths;
proto$1.monthsShort       =        localeMonthsShort;
proto$1.monthsParse       =        localeMonthsParse;
proto$1.monthsRegex       = monthsRegex;
proto$1.monthsShortRegex  = monthsShortRegex;

// Week
proto$1.week = localeWeek;
proto$1.firstDayOfYear = localeFirstDayOfYear;
proto$1.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
proto$1.weekdays       =        localeWeekdays;
proto$1.weekdaysMin    =        localeWeekdaysMin;
proto$1.weekdaysShort  =        localeWeekdaysShort;
proto$1.weekdaysParse  =        localeWeekdaysParse;

proto$1.weekdaysRegex       =        weekdaysRegex;
proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

// Hours
proto$1.isPM = localeIsPM;
proto$1.meridiem = localeMeridiem;

function get$1 (format, index, field, setter) {
    var locale = getLocale();
    var utc = createUTC().set(setter, index);
    return locale[field](utc, format);
}

function listMonthsImpl (format, index, field) {
    if (isNumber(format)) {
        index = format;
        format = undefined;
    }

    format = format || '';

    if (index != null) {
        return get$1(format, index, field, 'month');
    }

    var i;
    var out = [];
    for (i = 0; i < 12; i++) {
        out[i] = get$1(format, i, field, 'month');
    }
    return out;
}

// ()
// (5)
// (fmt, 5)
// (fmt)
// (true)
// (true, 5)
// (true, fmt, 5)
// (true, fmt)
function listWeekdaysImpl (localeSorted, format, index, field) {
    if (typeof localeSorted === 'boolean') {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    } else {
        format = localeSorted;
        index = format;
        localeSorted = false;

        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    }

    var locale = getLocale(),
        shift = localeSorted ? locale._week.dow : 0;

    if (index != null) {
        return get$1(format, (index + shift) % 7, field, 'day');
    }

    var i;
    var out = [];
    for (i = 0; i < 7; i++) {
        out[i] = get$1(format, (i + shift) % 7, field, 'day');
    }
    return out;
}

function listMonths (format, index) {
    return listMonthsImpl(format, index, 'months');
}

function listMonthsShort (format, index) {
    return listMonthsImpl(format, index, 'monthsShort');
}

function listWeekdays (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
}

function listWeekdaysShort (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
}

function listWeekdaysMin (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
}

getSetGlobalLocale('en', {
    dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
    ordinal : function (number) {
        var b = number % 10,
            output = (toInt(number % 100 / 10) === 1) ? 'th' :
            (b === 1) ? 'st' :
            (b === 2) ? 'nd' :
            (b === 3) ? 'rd' : 'th';
        return number + output;
    }
});

// Side effect imports
hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

var mathAbs = Math.abs;

function abs () {
    var data           = this._data;

    this._milliseconds = mathAbs(this._milliseconds);
    this._days         = mathAbs(this._days);
    this._months       = mathAbs(this._months);

    data.milliseconds  = mathAbs(data.milliseconds);
    data.seconds       = mathAbs(data.seconds);
    data.minutes       = mathAbs(data.minutes);
    data.hours         = mathAbs(data.hours);
    data.months        = mathAbs(data.months);
    data.years         = mathAbs(data.years);

    return this;
}

function addSubtract$1 (duration, input, value, direction) {
    var other = createDuration(input, value);

    duration._milliseconds += direction * other._milliseconds;
    duration._days         += direction * other._days;
    duration._months       += direction * other._months;

    return duration._bubble();
}

// supports only 2.0-style add(1, 's') or add(duration)
function add$1 (input, value) {
    return addSubtract$1(this, input, value, 1);
}

// supports only 2.0-style subtract(1, 's') or subtract(duration)
function subtract$1 (input, value) {
    return addSubtract$1(this, input, value, -1);
}

function absCeil (number) {
    if (number < 0) {
        return Math.floor(number);
    } else {
        return Math.ceil(number);
    }
}

function bubble () {
    var milliseconds = this._milliseconds;
    var days         = this._days;
    var months       = this._months;
    var data         = this._data;
    var seconds, minutes, hours, years, monthsFromDays;

    // if we have a mix of positive and negative values, bubble down first
    // check: https://github.com/moment/moment/issues/2166
    if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
            (milliseconds <= 0 && days <= 0 && months <= 0))) {
        milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
        days = 0;
        months = 0;
    }

    // The following code bubbles up values, see the tests for
    // examples of what that means.
    data.milliseconds = milliseconds % 1000;

    seconds           = absFloor(milliseconds / 1000);
    data.seconds      = seconds % 60;

    minutes           = absFloor(seconds / 60);
    data.minutes      = minutes % 60;

    hours             = absFloor(minutes / 60);
    data.hours        = hours % 24;

    days += absFloor(hours / 24);

    // convert days to months
    monthsFromDays = absFloor(daysToMonths(days));
    months += monthsFromDays;
    days -= absCeil(monthsToDays(monthsFromDays));

    // 12 months -> 1 year
    years = absFloor(months / 12);
    months %= 12;

    data.days   = days;
    data.months = months;
    data.years  = years;

    return this;
}

function daysToMonths (days) {
    // 400 years have 146097 days (taking into account leap year rules)
    // 400 years have 12 months === 4800
    return days * 4800 / 146097;
}

function monthsToDays (months) {
    // the reverse of daysToMonths
    return months * 146097 / 4800;
}

function as (units) {
    if (!this.isValid()) {
        return NaN;
    }
    var days;
    var months;
    var milliseconds = this._milliseconds;

    units = normalizeUnits(units);

    if (units === 'month' || units === 'year') {
        days   = this._days   + milliseconds / 864e5;
        months = this._months + daysToMonths(days);
        return units === 'month' ? months : months / 12;
    } else {
        // handle milliseconds separately because of floating point math errors (issue #1867)
        days = this._days + Math.round(monthsToDays(this._months));
        switch (units) {
            case 'week'   : return days / 7     + milliseconds / 6048e5;
            case 'day'    : return days         + milliseconds / 864e5;
            case 'hour'   : return days * 24    + milliseconds / 36e5;
            case 'minute' : return days * 1440  + milliseconds / 6e4;
            case 'second' : return days * 86400 + milliseconds / 1000;
            // Math.floor prevents floating point math errors here
            case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
            default: throw new Error('Unknown unit ' + units);
        }
    }
}

// TODO: Use this.as('ms')?
function valueOf$1 () {
    if (!this.isValid()) {
        return NaN;
    }
    return (
        this._milliseconds +
        this._days * 864e5 +
        (this._months % 12) * 2592e6 +
        toInt(this._months / 12) * 31536e6
    );
}

function makeAs (alias) {
    return function () {
        return this.as(alias);
    };
}

var asMilliseconds = makeAs('ms');
var asSeconds      = makeAs('s');
var asMinutes      = makeAs('m');
var asHours        = makeAs('h');
var asDays         = makeAs('d');
var asWeeks        = makeAs('w');
var asMonths       = makeAs('M');
var asYears        = makeAs('y');

function clone$1 () {
    return createDuration(this);
}

function get$2 (units) {
    units = normalizeUnits(units);
    return this.isValid() ? this[units + 's']() : NaN;
}

function makeGetter(name) {
    return function () {
        return this.isValid() ? this._data[name] : NaN;
    };
}

var milliseconds = makeGetter('milliseconds');
var seconds      = makeGetter('seconds');
var minutes      = makeGetter('minutes');
var hours        = makeGetter('hours');
var days         = makeGetter('days');
var months       = makeGetter('months');
var years        = makeGetter('years');

function weeks () {
    return absFloor(this.days() / 7);
}

var round = Math.round;
var thresholds = {
    ss: 44,         // a few seconds to seconds
    s : 45,         // seconds to minute
    m : 45,         // minutes to hour
    h : 22,         // hours to day
    d : 26,         // days to month
    M : 11          // months to year
};

// helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
    return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
}

function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
    var duration = createDuration(posNegDuration).abs();
    var seconds  = round(duration.as('s'));
    var minutes  = round(duration.as('m'));
    var hours    = round(duration.as('h'));
    var days     = round(duration.as('d'));
    var months   = round(duration.as('M'));
    var years    = round(duration.as('y'));

    var a = seconds <= thresholds.ss && ['s', seconds]  ||
            seconds < thresholds.s   && ['ss', seconds] ||
            minutes <= 1             && ['m']           ||
            minutes < thresholds.m   && ['mm', minutes] ||
            hours   <= 1             && ['h']           ||
            hours   < thresholds.h   && ['hh', hours]   ||
            days    <= 1             && ['d']           ||
            days    < thresholds.d   && ['dd', days]    ||
            months  <= 1             && ['M']           ||
            months  < thresholds.M   && ['MM', months]  ||
            years   <= 1             && ['y']           || ['yy', years];

    a[2] = withoutSuffix;
    a[3] = +posNegDuration > 0;
    a[4] = locale;
    return substituteTimeAgo.apply(null, a);
}

// This function allows you to set the rounding function for relative time strings
function getSetRelativeTimeRounding (roundingFunction) {
    if (roundingFunction === undefined) {
        return round;
    }
    if (typeof(roundingFunction) === 'function') {
        round = roundingFunction;
        return true;
    }
    return false;
}

// This function allows you to set a threshold for relative time strings
function getSetRelativeTimeThreshold (threshold, limit) {
    if (thresholds[threshold] === undefined) {
        return false;
    }
    if (limit === undefined) {
        return thresholds[threshold];
    }
    thresholds[threshold] = limit;
    if (threshold === 's') {
        thresholds.ss = limit - 1;
    }
    return true;
}

function humanize (withSuffix) {
    if (!this.isValid()) {
        return this.localeData().invalidDate();
    }

    var locale = this.localeData();
    var output = relativeTime$1(this, !withSuffix, locale);

    if (withSuffix) {
        output = locale.pastFuture(+this, output);
    }

    return locale.postformat(output);
}

var abs$1 = Math.abs;

function sign(x) {
    return ((x > 0) - (x < 0)) || +x;
}

function toISOString$1() {
    // for ISO strings we do not use the normal bubbling rules:
    //  * milliseconds bubble up until they become hours
    //  * days do not bubble at all
    //  * months bubble up until they become years
    // This is because there is no context-free conversion between hours and days
    // (think of clock changes)
    // and also not between days and months (28-31 days per month)
    if (!this.isValid()) {
        return this.localeData().invalidDate();
    }

    var seconds = abs$1(this._milliseconds) / 1000;
    var days         = abs$1(this._days);
    var months       = abs$1(this._months);
    var minutes, hours, years;

    // 3600 seconds -> 60 minutes -> 1 hour
    minutes           = absFloor(seconds / 60);
    hours             = absFloor(minutes / 60);
    seconds %= 60;
    minutes %= 60;

    // 12 months -> 1 year
    years  = absFloor(months / 12);
    months %= 12;


    // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
    var Y = years;
    var M = months;
    var D = days;
    var h = hours;
    var m = minutes;
    var s = seconds ? seconds.toFixed(3).replace(/\.?0+$/, '') : '';
    var total = this.asSeconds();

    if (!total) {
        // this is the same as C#'s (Noda) and python (isodate)...
        // but not other JS (goog.date)
        return 'P0D';
    }

    var totalSign = total < 0 ? '-' : '';
    var ymSign = sign(this._months) !== sign(total) ? '-' : '';
    var daysSign = sign(this._days) !== sign(total) ? '-' : '';
    var hmsSign = sign(this._milliseconds) !== sign(total) ? '-' : '';

    return totalSign + 'P' +
        (Y ? ymSign + Y + 'Y' : '') +
        (M ? ymSign + M + 'M' : '') +
        (D ? daysSign + D + 'D' : '') +
        ((h || m || s) ? 'T' : '') +
        (h ? hmsSign + h + 'H' : '') +
        (m ? hmsSign + m + 'M' : '') +
        (s ? hmsSign + s + 'S' : '');
}

var proto$2 = Duration.prototype;

proto$2.isValid        = isValid$1;
proto$2.abs            = abs;
proto$2.add            = add$1;
proto$2.subtract       = subtract$1;
proto$2.as             = as;
proto$2.asMilliseconds = asMilliseconds;
proto$2.asSeconds      = asSeconds;
proto$2.asMinutes      = asMinutes;
proto$2.asHours        = asHours;
proto$2.asDays         = asDays;
proto$2.asWeeks        = asWeeks;
proto$2.asMonths       = asMonths;
proto$2.asYears        = asYears;
proto$2.valueOf        = valueOf$1;
proto$2._bubble        = bubble;
proto$2.clone          = clone$1;
proto$2.get            = get$2;
proto$2.milliseconds   = milliseconds;
proto$2.seconds        = seconds;
proto$2.minutes        = minutes;
proto$2.hours          = hours;
proto$2.days           = days;
proto$2.weeks          = weeks;
proto$2.months         = months;
proto$2.years          = years;
proto$2.humanize       = humanize;
proto$2.toISOString    = toISOString$1;
proto$2.toString       = toISOString$1;
proto$2.toJSON         = toISOString$1;
proto$2.locale         = locale;
proto$2.localeData     = localeData;

// Deprecations
proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
proto$2.lang = lang;

// Side effect imports

// FORMATTING

addFormatToken('X', 0, 0, 'unix');
addFormatToken('x', 0, 0, 'valueOf');

// PARSING

addRegexToken('x', matchSigned);
addRegexToken('X', matchTimestamp);
addParseToken('X', function (input, array, config) {
    config._d = new Date(parseFloat(input, 10) * 1000);
});
addParseToken('x', function (input, array, config) {
    config._d = new Date(toInt(input));
});

// Side effect imports


hooks.version = '2.19.3';

setHookCallback(createLocal);

hooks.fn                    = proto;
hooks.min                   = min;
hooks.max                   = max;
hooks.now                   = now;
hooks.utc                   = createUTC;
hooks.unix                  = createUnix;
hooks.months                = listMonths;
hooks.isDate                = isDate;
hooks.locale                = getSetGlobalLocale;
hooks.invalid               = createInvalid;
hooks.duration              = createDuration;
hooks.isMoment              = isMoment;
hooks.weekdays              = listWeekdays;
hooks.parseZone             = createInZone;
hooks.localeData            = getLocale;
hooks.isDuration            = isDuration;
hooks.monthsShort           = listMonthsShort;
hooks.weekdaysMin           = listWeekdaysMin;
hooks.defineLocale          = defineLocale;
hooks.updateLocale          = updateLocale;
hooks.locales               = listLocales;
hooks.weekdaysShort         = listWeekdaysShort;
hooks.normalizeUnits        = normalizeUnits;
hooks.relativeTimeRounding  = getSetRelativeTimeRounding;
hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
hooks.calendarFormat        = getCalendarFormat;
hooks.prototype             = proto;

return hooks;

})));

}});

require.define({"pikaday": function(exports, require, module) {
/*!
 * Pikaday
 *
 * Copyright © 2014 David Bushell | BSD & MIT license | https://github.com/dbushell/Pikaday
 */

(function (root, factory)
{
    'use strict';

    var moment;
    if (typeof exports === 'object') {
        // CommonJS module
        // Load moment.js as an optional dependency
        try { moment = require('moment'); } catch (e) {}
        module.exports = factory(moment);
    } else if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(function (req)
        {
            // Load moment.js as an optional dependency
            var id = 'moment';
            moment = req.defined && req.defined(id) ? req(id) : undefined;
            return factory(moment);
        });
    } else {
        root.Pikaday = factory(root.moment);
    }
}(this, function (moment)
{
    'use strict';

    /**
     * feature detection and helper functions
     */
    var hasMoment = typeof moment === 'function',

    hasEventListeners = !!window.addEventListener,

    document = window.document,

    sto = window.setTimeout,

    addEvent = function(el, e, callback, capture)
    {
        if (hasEventListeners) {
            el.addEventListener(e, callback, !!capture);
        } else {
            el.attachEvent('on' + e, callback);
        }
    },

    removeEvent = function(el, e, callback, capture)
    {
        if (hasEventListeners) {
            el.removeEventListener(e, callback, !!capture);
        } else {
            el.detachEvent('on' + e, callback);
        }
    },

    fireEvent = function(el, eventName, data)
    {
        var ev;

        if (document.createEvent) {
            ev = document.createEvent('HTMLEvents');
            ev.initEvent(eventName, true, false);
            ev = extend(ev, data);
            el.dispatchEvent(ev);
        } else if (document.createEventObject) {
            ev = document.createEventObject();
            ev = extend(ev, data);
            el.fireEvent('on' + eventName, ev);
        }
    },

    trim = function(str)
    {
        return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g,'');
    },

    hasClass = function(el, cn)
    {
        return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
    },

    addClass = function(el, cn)
    {
        if (!hasClass(el, cn)) {
            el.className = (el.className === '') ? cn : el.className + ' ' + cn;
        }
    },

    removeClass = function(el, cn)
    {
        el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
    },

    isArray = function(obj)
    {
        return (/Array/).test(Object.prototype.toString.call(obj));
    },

    isDate = function(obj)
    {
        return (/Date/).test(Object.prototype.toString.call(obj)) && !isNaN(obj.getTime());
    },

    isLeapYear = function(year)
    {
        // solution by Matti Virkkunen: http://stackoverflow.com/a/4881951
        return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
    },

    getDaysInMonth = function(year, month)
    {
        return [31, isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    },

    setToStartOfDay = function(date)
    {
        if (isDate(date)) date.setHours(0,0,0,0);
    },

    compareDates = function(a,b)
    {
        // weak date comparison (use setToStartOfDay(date) to ensure correct result)
        return a.getTime() === b.getTime();
    },

    extend = function(to, from, overwrite)
    {
        var prop, hasProp;
        for (prop in from) {
            hasProp = to[prop] !== undefined;
            if (hasProp && typeof from[prop] === 'object' && from[prop].nodeName === undefined) {
                if (isDate(from[prop])) {
                    if (overwrite) {
                        to[prop] = new Date(from[prop].getTime());
                    }
                }
                else if (isArray(from[prop])) {
                    if (overwrite) {
                        to[prop] = from[prop].slice(0);
                    }
                } else {
                    to[prop] = extend({}, from[prop], overwrite);
                }
            } else if (overwrite || !hasProp) {
                to[prop] = from[prop];
            }
        }
        return to;
    },


    /**
     * defaults and localisation
     */
    defaults = {

        // bind the picker to a form field
        field: null,

        // automatically show/hide the picker on `field` focus (default `true` if `field` is set)
        bound: undefined,

        // position of the datepicker, relative to the field (default to bottom & left)
        // ('bottom' & 'left' keywords are not used, 'top' & 'right' are modifier on the bottom/left position)
        position: 'bottom left',

        // the default output format for `.toString()` and `field` value
        format: 'YYYY-MM-DD',

        // the initial date to view when first opened
        defaultDate: null,

        // make the `defaultDate` the initial selected value
        setDefaultDate: false,

        // first day of week (0: Sunday, 1: Monday etc)
        firstDay: 0,

        // the minimum/earliest date that can be selected
        minDate: null,
        // the maximum/latest date that can be selected
        maxDate: null,

        // number of years either side, or array of upper/lower range
        yearRange: 10,

        // used internally (don't config outside)
        minYear: 0,
        maxYear: 9999,
        minMonth: undefined,
        maxMonth: undefined,

        isRTL: false,

        // Additional text to append to the year in the calendar title
        yearSuffix: '',

        // Render the month after year in the calendar title
        showMonthAfterYear: false,

        // how many months are visible (not implemented yet)
        numberOfMonths: 1,

        // internationalization
        i18n: {
            previousMonth : 'Previous Month',
            nextMonth     : 'Next Month',
            months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
            weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
            weekdaysShort : ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        },

        // callback function
        onSelect: null,
        onOpen: null,
        onClose: null,
        onDraw: null
    },


    /**
     * templating functions to abstract HTML rendering
     */
    renderDayName = function(opts, day, abbr)
    {
        day += opts.firstDay;
        while (day >= 7) {
            day -= 7;
        }
        return abbr ? opts.i18n.weekdaysShort[day] : opts.i18n.weekdays[day];
    },

    renderDay = function(i, isSelected, isToday, isDisabled, isEmpty)
    {
        if (isEmpty) {
            return '<td class="is-empty"></td>';
        }
        var arr = [];
        if (isDisabled) {
            arr.push('is-disabled');
        }
        if (isToday) {
            arr.push('is-today');
        }
        if (isSelected) {
            arr.push('is-selected');
        }
        return '<td data-day="' + i + '" class="' + arr.join(' ') + '"><button class="pika-button" type="button">' + i + '</button>' + '</td>';
    },

    renderRow = function(days, isRTL)
    {
        return '<tr>' + (isRTL ? days.reverse() : days).join('') + '</tr>';
    },

    renderBody = function(rows)
    {
        return '<tbody>' + rows.join('') + '</tbody>';
    },

    renderHead = function(opts)
    {
        var i, arr = [];
        for (i = 0; i < 7; i++) {
            arr.push('<th scope="col"><abbr title="' + renderDayName(opts, i) + '">' + renderDayName(opts, i, true) + '</abbr></th>');
        }
        return '<thead>' + (opts.isRTL ? arr.reverse() : arr).join('') + '</thead>';
    },

    renderTitle = function(instance)
    {
        var i, j, arr,
            opts = instance._o,
            month = instance._m,
            year  = instance._y,
            isMinYear = year === opts.minYear,
            isMaxYear = year === opts.maxYear,
            html = '<div class="pika-title">',
            monthHtml,
            yearHtml,
            prev = true,
            next = true;

        for (arr = [], i = 0; i < 12; i++) {
            arr.push('<option value="' + i + '"' +
                (i === month ? ' selected': '') +
                ((isMinYear && i < opts.minMonth) || (isMaxYear && i > opts.maxMonth) ? 'disabled' : '') + '>' +
                opts.i18n.months[i] + '</option>');
        }
        monthHtml = '<div class="pika-label">' + opts.i18n.months[month] + '<select class="pika-select pika-select-month">' + arr.join('') + '</select></div>';

        if (isArray(opts.yearRange)) {
            i = opts.yearRange[0];
            j = opts.yearRange[1] + 1;
        } else {
            i = year - opts.yearRange;
            j = 1 + year + opts.yearRange;
        }

        for (arr = []; i < j && i <= opts.maxYear; i++) {
            if (i >= opts.minYear) {
                arr.push('<option value="' + i + '"' + (i === year ? ' selected': '') + '>' + (i) + '</option>');
            }
        }
        yearHtml = '<div class="pika-label">' + year + opts.yearSuffix + '<select class="pika-select pika-select-year">' + arr.join('') + '</select></div>';

        if (opts.showMonthAfterYear) {
            html += yearHtml + monthHtml;
        } else {
            html += monthHtml + yearHtml;
        }

        if (isMinYear && (month === 0 || opts.minMonth >= month)) {
            prev = false;
        }

        if (isMaxYear && (month === 11 || opts.maxMonth <= month)) {
            next = false;
        }

        html += '<button class="pika-prev' + (prev ? '' : ' is-disabled') + '" type="button">' + opts.i18n.previousMonth + '</button>';
        html += '<button class="pika-next' + (next ? '' : ' is-disabled') + '" type="button">' + opts.i18n.nextMonth + '</button>';

        return html += '</div>';
    },

    renderTable = function(opts, data)
    {
        return '<table cellpadding="0" cellspacing="0" class="pika-table">' + renderHead(opts) + renderBody(data) + '</table>';
    },


    /**
     * Pikaday constructor
     */
    Pikaday = function(options)
    {
        var self = this,
            opts = self.config(options);

        self._onMouseDown = function(e)
        {
            if (!self._v) {
                return;
            }
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (!target) {
                return;
            }

            if (!hasClass(target, 'is-disabled')) {
                if (hasClass(target, 'pika-button') && !hasClass(target, 'is-empty')) {
                    self.setDate(new Date(self._y, self._m, parseInt(target.innerHTML, 10)));
                    if (opts.bound) {
                        sto(function() {
                            self.hide();
                        }, 100);
                    }
                    return;
                }
                else if (hasClass(target, 'pika-prev')) {
                    self.prevMonth();
                }
                else if (hasClass(target, 'pika-next')) {
                    self.nextMonth();
                }
            }
            if (!hasClass(target, 'pika-select')) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                    return false;
                }
            } else {
                self._c = true;
            }
        };

        self._onChange = function(e)
        {
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (!target) {
                return;
            }
            if (hasClass(target, 'pika-select-month')) {
                self.gotoMonth(target.value);
            }
            else if (hasClass(target, 'pika-select-year')) {
                self.gotoYear(target.value);
            }
        };

        self._onInputChange = function(e)
        {
            var date;

            if (e.firedBy === self) {
                return;
            }
            if (hasMoment) {
                date = moment(opts.field.value, opts.format);
                date = (date && date.isValid()) ? date.toDate() : null;
            }
            else {
                date = new Date(Date.parse(opts.field.value));
            }
            self.setDate(isDate(date) ? date : null);
            if (!self._v) {
                self.show();
            }
        };

        self._onInputFocus = function()
        {
            self.show();
        };

        self._onInputClick = function()
        {
            self.show();
        };

        self._onInputBlur = function()
        {
            if (!self._c) {
                self._b = sto(function() {
                    self.hide();
                }, 50);
            }
            self._c = false;
        };

        self._onClick = function(e)
        {
            e = e || window.event;
            var target = e.target || e.srcElement,
                pEl = target;
            if (!target) {
                return;
            }
            if (!hasEventListeners && hasClass(target, 'pika-select')) {
                if (!target.onchange) {
                    target.setAttribute('onchange', 'return;');
                    addEvent(target, 'change', self._onChange);
                }
            }
            do {
                if (hasClass(pEl, 'pika-single')) {
                    return;
                }
            }
            while ((pEl = pEl.parentNode));
            if (self._v && target !== opts.trigger) {
                self.hide();
            }
        };

        self.el = document.createElement('div');
        self.el.className = 'pika-single' + (opts.isRTL ? ' is-rtl' : '');

        addEvent(self.el, 'mousedown', self._onMouseDown, true);
        addEvent(self.el, 'change', self._onChange);

        if (opts.field) {
            if (opts.bound) {
                document.body.appendChild(self.el);
            } else {
                opts.field.parentNode.insertBefore(self.el, opts.field.nextSibling);
            }
            addEvent(opts.field, 'change', self._onInputChange);

            if (!opts.defaultDate) {
                if (hasMoment && opts.field.value) {
                    opts.defaultDate = moment(opts.field.value, opts.format).toDate();
                } else {
                    opts.defaultDate = new Date(Date.parse(opts.field.value));
                }
                opts.setDefaultDate = true;
            }
        }

        var defDate = opts.defaultDate;

        if (isDate(defDate)) {
            if (opts.setDefaultDate) {
                self.setDate(defDate, true);
            } else {
                self.gotoDate(defDate);
            }
        } else {
            self.gotoDate(new Date());
        }

        if (opts.bound) {
            this.hide();
            self.el.className += ' is-bound';
            addEvent(opts.trigger, 'click', self._onInputClick);
            addEvent(opts.trigger, 'focus', self._onInputFocus);
            addEvent(opts.trigger, 'blur', self._onInputBlur);
        } else {
            this.show();
        }

    };


    /**
     * public Pikaday API
     */
    Pikaday.prototype = {


        /**
         * configure functionality
         */
        config: function(options)
        {
            if (!this._o) {
                this._o = extend({}, defaults, true);
            }

            var opts = extend(this._o, options, true);

            opts.isRTL = !!opts.isRTL;

            opts.field = (opts.field && opts.field.nodeName) ? opts.field : null;

            opts.bound = !!(opts.bound !== undefined ? opts.field && opts.bound : opts.field);

            opts.trigger = (opts.trigger && opts.trigger.nodeName) ? opts.trigger : opts.field;

            var nom = parseInt(opts.numberOfMonths, 10) || 1;
            opts.numberOfMonths = nom > 4 ? 4 : nom;

            if (!isDate(opts.minDate)) {
                opts.minDate = false;
            }
            if (!isDate(opts.maxDate)) {
                opts.maxDate = false;
            }
            if ((opts.minDate && opts.maxDate) && opts.maxDate < opts.minDate) {
                opts.maxDate = opts.minDate = false;
            }
            if (opts.minDate) {
                setToStartOfDay(opts.minDate);
                opts.minYear  = opts.minDate.getFullYear();
                opts.minMonth = opts.minDate.getMonth();
            }
            if (opts.maxDate) {
                setToStartOfDay(opts.maxDate);
                opts.maxYear  = opts.maxDate.getFullYear();
                opts.maxMonth = opts.maxDate.getMonth();
            }

            if (isArray(opts.yearRange)) {
                var fallback = new Date().getFullYear() - 10;
                opts.yearRange[0] = parseInt(opts.yearRange[0], 10) || fallback;
                opts.yearRange[1] = parseInt(opts.yearRange[1], 10) || fallback;
            } else {
                opts.yearRange = Math.abs(parseInt(opts.yearRange, 10)) || defaults.yearRange;
                if (opts.yearRange > 100) {
                    opts.yearRange = 100;
                }
            }

            return opts;
        },

        /**
         * return a formatted string of the current selection (using Moment.js if available)
         */
        toString: function(format)
        {
            return !isDate(this._d) ? '' : hasMoment ? moment(this._d).format(format || this._o.format) : this._d.toDateString();
        },

        /**
         * return a Moment.js object of the current selection (if available)
         */
        getMoment: function()
        {
            return hasMoment ? moment(this._d) : null;
        },

        /**
         * set the current selection from a Moment.js object (if available)
         */
        setMoment: function(date, preventOnSelect)
        {
            if (hasMoment && moment.isMoment(date)) {
                this.setDate(date.toDate(), preventOnSelect);
            }
        },

        /**
         * return a Date object of the current selection
         */
        getDate: function()
        {
            return isDate(this._d) ? new Date(this._d.getTime()) : null;
        },

        /**
         * set the current selection
         */
        setDate: function(date, preventOnSelect)
        {
            if (!date) {
                this._d = null;
                return this.draw();
            }
            if (typeof date === 'string') {
                date = new Date(Date.parse(date));
            }
            if (!isDate(date)) {
                return;
            }

            var min = this._o.minDate,
                max = this._o.maxDate;

            if (isDate(min) && date < min) {
                date = min;
            } else if (isDate(max) && date > max) {
                date = max;
            }

            this._d = new Date(date.getTime());
            setToStartOfDay(this._d);
            this.gotoDate(this._d);

            if (this._o.field) {
                this._o.field.value = this.toString();
                fireEvent(this._o.field, 'change', { firedBy: this });
            }
            if (!preventOnSelect && typeof this._o.onSelect === 'function') {
                this._o.onSelect.call(this, this.getDate());
            }
        },

        /**
         * change view to a specific date
         */
        gotoDate: function(date)
        {
            if (!isDate(date)) {
                return;
            }
            this._y = date.getFullYear();
            this._m = date.getMonth();
            this.draw();
        },

        gotoToday: function()
        {
            this.gotoDate(new Date());
        },

        /**
         * change view to a specific month (zero-index, e.g. 0: January)
         */
        gotoMonth: function(month)
        {
            if (!isNaN( (month = parseInt(month, 10)) )) {
                this._m = month < 0 ? 0 : month > 11 ? 11 : month;
                this.draw();
            }
        },

        nextMonth: function()
        {
            if (++this._m > 11) {
                this._m = 0;
                this._y++;
            }
            this.draw();
        },

        prevMonth: function()
        {
            if (--this._m < 0) {
                this._m = 11;
                this._y--;
            }
            this.draw();
        },

        /**
         * change view to a specific full year (e.g. "2012")
         */
        gotoYear: function(year)
        {
            if (!isNaN(year)) {
                this._y = parseInt(year, 10);
                this.draw();
            }
        },

        /**
         * change the minDate
         */
        setMinDate: function(value)
        {
            this._o.minDate = value;
        },

        /**
         * change the maxDate
         */
        setMaxDate: function(value)
        {
            this._o.maxDate = value;
        },

        /**
         * refresh the HTML
         */
        draw: function(force)
        {
            if (!this._v && !force) {
                return;
            }
            var opts = this._o,
                minYear = opts.minYear,
                maxYear = opts.maxYear,
                minMonth = opts.minMonth,
                maxMonth = opts.maxMonth;

            if (this._y <= minYear) {
                this._y = minYear;
                if (!isNaN(minMonth) && this._m < minMonth) {
                    this._m = minMonth;
                }
            }
            if (this._y >= maxYear) {
                this._y = maxYear;
                if (!isNaN(maxMonth) && this._m > maxMonth) {
                    this._m = maxMonth;
                }
            }

            this.el.innerHTML = renderTitle(this) + this.render(this._y, this._m);

            if (opts.bound) {
                this.adjustPosition();
                if(opts.field.type !== 'hidden') {
                    sto(function() {
                        opts.trigger.focus();
                    }, 1);
                }
            }

            if (typeof this._o.onDraw === 'function') {
                var self = this;
                sto(function() {
                    self._o.onDraw.call(self);
                }, 0);
            }
        },

        adjustPosition: function()
        {
            var field = this._o.trigger, pEl = field,
            width = this.el.offsetWidth, height = this.el.offsetHeight,
            viewportWidth = window.innerWidth || document.documentElement.clientWidth,
            viewportHeight = window.innerHeight || document.documentElement.clientHeight,
            scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop,
            left, top, clientRect;

            if (typeof field.getBoundingClientRect === 'function') {
                clientRect = field.getBoundingClientRect();
                left = clientRect.left + window.pageXOffset;
                top = clientRect.bottom + window.pageYOffset;
            } else {
                left = pEl.offsetLeft;
                top  = pEl.offsetTop + pEl.offsetHeight;
                while((pEl = pEl.offsetParent)) {
                    left += pEl.offsetLeft;
                    top  += pEl.offsetTop;
                }
            }

            // default position is bottom & left
            if (left + width > viewportWidth ||
                (
                    this._o.position.indexOf('right') > -1 &&
                    left - width + field.offsetWidth > 0
                )
            ) {
                left = left - width + field.offsetWidth;
            }
            if (top + height > viewportHeight + scrollTop ||
                (
                    this._o.position.indexOf('top') > -1 &&
                    top - height - field.offsetHeight > 0
                )
            ) {
                top = top - height - field.offsetHeight;
            }
            this.el.style.cssText = [
                'position: absolute',
                'left: ' + left + 'px',
                'top: ' + top + 'px'
            ].join(';');
        },

        /**
         * render HTML for a particular month
         */
        render: function(year, month)
        {
            var opts   = this._o,
                now    = new Date(),
                days   = getDaysInMonth(year, month),
                before = new Date(year, month, 1).getDay(),
                data   = [],
                row    = [];
            setToStartOfDay(now);
            if (opts.firstDay > 0) {
                before -= opts.firstDay;
                if (before < 0) {
                    before += 7;
                }
            }
            var cells = days + before,
                after = cells;
            while(after > 7) {
                after -= 7;
            }
            cells += 7 - after;
            for (var i = 0, r = 0; i < cells; i++)
            {
                var day = new Date(year, month, 1 + (i - before)),
                    isDisabled = (opts.minDate && day < opts.minDate) || (opts.maxDate && day > opts.maxDate),
                    isSelected = isDate(this._d) ? compareDates(day, this._d) : false,
                    isToday = compareDates(day, now),
                    isEmpty = i < before || i >= (days + before);

                row.push(renderDay(1 + (i - before), isSelected, isToday, isDisabled, isEmpty));

                if (++r === 7) {
                    data.push(renderRow(row, opts.isRTL));
                    row = [];
                    r = 0;
                }
            }
            return renderTable(opts, data);
        },

        isVisible: function()
        {
            return this._v;
        },

        show: function()
        {
            if (!this._v) {
                if (this._o.bound) {
                    addEvent(document, 'click', this._onClick);
                }
                removeClass(this.el, 'is-hidden');
                this._v = true;
                this.draw();
                if (typeof this._o.onOpen === 'function') {
                    this._o.onOpen.call(this);
                }
            }
        },

        hide: function()
        {
            var v = this._v;
            if (v !== false) {
                if (this._o.bound) {
                    removeEvent(document, 'click', this._onClick);
                }
                this.el.style.cssText = '';
                addClass(this.el, 'is-hidden');
                this._v = false;
                if (v !== undefined && typeof this._o.onClose === 'function') {
                    this._o.onClose.call(this);
                }
            }
        },

        /**
         * GAME OVER
         */
        destroy: function()
        {
            this.hide();
            removeEvent(this.el, 'mousedown', this._onMouseDown, true);
            removeEvent(this.el, 'change', this._onChange);
            if (this._o.field) {
                removeEvent(this._o.field, 'change', this._onInputChange);
                if (this._o.bound) {
                    removeEvent(this._o.trigger, 'click', this._onInputClick);
                    removeEvent(this._o.trigger, 'focus', this._onInputFocus);
                    removeEvent(this._o.trigger, 'blur', this._onInputBlur);
                }
            }
            if (this.el.parentNode) {
                this.el.parentNode.removeChild(this.el);
            }
        }

    };

    return Pikaday;

}));

}});

require.define({"zendesk-menus": function(exports, require, module) {
!function(e){if("object"==typeof exports)module.exports=e();else if("function"==typeof define&&define.amd)define(e);else{var n;"undefined"!=typeof window?n=window:"undefined"!=typeof global?n=global:"undefined"!=typeof self&&(n=self),n.ZendeskMenus=e()}}(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

var _utilsMixinsOptions_mixin = _dereq_('../utils/mixins/options_mixin');

var _utilsMixinsOptions_mixin2 = _interopRequireDefault(_utilsMixinsOptions_mixin);

var _utilsMixinsObservable_mixin = _dereq_('../utils/mixins/observable_mixin');

var _utilsMixinsObservable_mixin2 = _interopRequireDefault(_utilsMixinsObservable_mixin);

var _utilsMixinsPositionable_mixin = _dereq_('../utils/mixins/positionable_mixin');

var _utilsMixinsPositionable_mixin2 = _interopRequireDefault(_utilsMixinsPositionable_mixin);

var _utilsMixinsCss_scoping_mixin = _dereq_('../utils/mixins/css_scoping_mixin');

var _utilsMixinsCss_scoping_mixin2 = _interopRequireDefault(_utilsMixinsCss_scoping_mixin);

var _utilsDom_helpersVertical_menu_positioner = _dereq_('../utils/dom_helpers/vertical_menu_positioner');

var _utilsDom_helpersVertical_menu_positioner2 = _interopRequireDefault(_utilsDom_helpersVertical_menu_positioner);

var _utilsDom_helpersDom_position_observer = _dereq_('../utils/dom_helpers/dom_position_observer');

var _utilsDom_helpersDom_position_observer2 = _interopRequireDefault(_utilsDom_helpersDom_position_observer);

var _utilsDatasourcesFiltering_datasource = _dereq_('../utils/datasources/filtering_datasource');

var _utilsDatasourcesFiltering_datasource2 = _interopRequireDefault(_utilsDatasourcesFiltering_datasource);

var _utilsParsersLabel_concatenator = _dereq_('../utils/parsers/label_concatenator');

var _utilsParsersLabel_concatenator2 = _interopRequireDefault(_utilsParsersLabel_concatenator);

var _utilsRenderersHighlighting_renderer = _dereq_('../utils/renderers/highlighting_renderer');

var _utilsRenderersHighlighting_renderer2 = _interopRequireDefault(_utilsRenderersHighlighting_renderer);

var _utilsDom_helpersCss_class_state_machine = _dereq_('../utils/dom_helpers/css_class_state_machine');

var _utilsDom_helpersCss_class_state_machine2 = _interopRequireDefault(_utilsDom_helpersCss_class_state_machine);

var _menuMenu = _dereq_('../menu/menu');

var _menuMenu2 = _interopRequireDefault(_menuMenu);

var _combo_select_menu_states = _dereq_('./combo_select_menu_states');

var _combo_select_menu_states2 = _interopRequireDefault(_combo_select_menu_states);

var _utilsDom_helpersDocument_clicks_monitor = _dereq_('../utils/dom_helpers/document_clicks_monitor');

var _utilsDom_helpersDocument_clicks_monitor2 = _interopRequireDefault(_utilsDom_helpersDocument_clicks_monitor);

var defaultOptions = {
  defaultValue: '',
  defaultValueLabel: '-',
  domHolderSelector: null,
  preferredPosition: null,
  filteredField: 'label',

  disabled: false,

  clsSelectMenuScope: 'zd-combo-selectmenu',
  clsSelectMenuRoot: 'zd-combo-selectmenu zd-combo-selectmenu-root',

  clsDefault: 'zd-state-default',
  clsDisabled: 'zd-state-disabled',
  clsFocused: 'zd-state-focus',
  clsOpen: 'zd-state-open',
  clsHover: 'zd-state-hover',

  clsPositionUp: 'zd-state-position-up',

  clsBaseButton: 'zd-selectmenu-base',
  clsBaseContent: 'zd-selectmenu-base-content',
  clsBaseArrow: 'zd-selectmenu-base-arrow zd-icon-arrow-down',

  clsSearch: 'zd-searchmenu-base',

  clsHighlight: 'zd-highlight',
  maxSearchResults: Infinity,

  keyboardCue: false,
  enableHtmlEscape: true
};

var SOURCE_IN_MAP = {
  'select:click': 1,
  'select:keyboard': 1,
  'select:keyboardCue': 1,
  'search:click': 1,
  'search:keyboard': 1,
  'search:keyboardCue': 1
};

var ComboSelectMenu = (function (_mixin) {
  _inherits(ComboSelectMenu, _mixin);

  function ComboSelectMenu(options) {
    _classCallCheck(this, ComboSelectMenu);

    _get(Object.getPrototypeOf(ComboSelectMenu.prototype), 'constructor', this).call(this, options);

    this.type = 'ComboSelectMenu';

    this.id = (0, _utilsCore_utils.getUniqId)();
    this.baseId = (0, _utilsCore_utils.getUniqId)();
    this.baseContentId = (0, _utilsCore_utils.getUniqId)();
    this.searchId = (0, _utilsCore_utils.getUniqId)();

    this.dom = null;
    this.domBase = null;
    this.domBaseContent = null;
    this.domSearch = null;
    this.domMenuHolder = null;
    this.domProxy = null;

    this.isKeyboardCaptured = false;
    this.areDomEventsSet = false;
    this.isComposingInput = false;

    this.isOpen = false;
    this.isFocused = false;
    this.isInDom = false;
    this.isDestroyed = false;
    this.state = null; // ['display' | 'displaySearch' | 'fullSearch']

    this.selectMenu = null;
    this.searchMenu = null;

    this.typeToClassMapBase = null;
    this.typeToClassMapSearch = null;
    this.typeToClassMapSelect = null;

    // serves to position the menu in the window according
    // to available space and positioning requirements
    this.verticalMenuPositioner = new _utilsDom_helpersVertical_menu_positioner2['default']();

    // detects change in coordinates and fires a registered callback
    this.domBasePositionObserver = new _utilsDom_helpersDom_position_observer2['default']();

    this.setOptions(options, defaultOptions);

    this.hasProxy = this.options.proxyName || this.options.proxyId;
    this.proxyName = this.options.proxyName;
    this.proxyId = this.options.proxyId || (0, _utilsCore_utils.getUniqId)();

    if (this.domHolder || this.domHolderSelector) {
      if (!this.domHolder && this.domHolderSelector) {
        this.domHolder = document.querySelector(this.domHolderSelector);
      }
      this.appendTo(this.domHolder);
    }
  }

  _createClass(ComboSelectMenu, [{
    key: 'appendTo',
    value: function appendTo(target) {
      var _this = this;

      if (this.isInDom || !target) return;

      this.domHolder = target;
      _menuMenu2['default'].registerInstance(this);

      this.renderItemContentForBase = this.renderItemContentForBase || this.defaultRenderItemContentForBase;

      this.filteringDataSource = this.filteringDataSource || new _utilsDatasourcesFiltering_datasource2['default'](this.maxSearchResults, this.filteredField);
      this.modelParser = this.modelParser || new _utilsParsersLabel_concatenator2['default']();

      this.htmlBuffer = this.render();
      this.buildDomMenuHolder();
      this.buildCssClassStateManager();
      this.putInDom();

      this.initSelectMenu();

      this.searchDataSource = this.filteringDataSource;

      var highlightRenderer = new _utilsRenderersHighlighting_renderer2['default']();

      this.highlighter = function (str) {
        var filterWord = _this.searchDataSource.filterWord || '';

        if (filterWord === '') {
          return _this.enableHtmlEscape ? (0, _utilsCore_utils.escapeHtml)(str) : str;
        }

        return highlightRenderer.render(str, _this.searchDataSource.filterWord, _this.clsHighlight);
      };

      this.initSearchMenu();
      this.postDomInsertionSetup();
      this.setState('display');
      new _utilsDom_helpersDocument_clicks_monitor2['default'](this);
    }
  }, {
    key: 'buildDomMenuHolder',
    value: function buildDomMenuHolder() {
      this.domMenuHolder = document.createElement('div');
      this.domMenuHolder.className = this.clsSelectMenuScope;
      document.body.appendChild(this.domMenuHolder);
    }
  }, {
    key: 'buildCssClassStateManager',
    value: function buildCssClassStateManager() {
      var _this2 = this;

      this.clsStateManager = new _utilsDom_helpersCss_class_state_machine2['default']({
        dom: function dom() {
          return _this2.dom;
        },
        domAux: function domAux() {
          return _this2.domMenuHolder;
        },
        clsZero: this.clsDefault,
        clsStates: [this.clsDisabled, this.clsOpen, this.clsFocused, this.clsHover]
      });
    }
  }, {
    key: '_prepareOptions',
    value: function _prepareOptions() {
      this.options.domHolder = this.domMenuHolder;
      this.options.isVisible = false;
      this.options.domRef = null;
      this.options.proxyValue = null;
      this.options.proxyId = null;
      this.options.disabled = false;
    }
  }, {
    key: 'initSelectMenu',
    value: function initSelectMenu() {
      var _this3 = this;

      this._prepareOptions();

      if (this.renderItemContentForSelect) {
        this.options.renderItemContent = this.renderItemContentForSelect;
      }
      this.options.typeToClassMap = this.typeToClassMapSelect;

      this.options.postParser = function (rootItem) {
        return _this3.modelParser.parse(rootItem);
      };

      var selectMenu = this.selectMenu = new _menuMenu2['default'](this.options);
      selectMenu.container = this;

      delete this.options.renderItemContent;
      delete this.options.typeToClassMap;
      delete this.options.postParser;

      selectMenu.onChange = function (data) {
        return _this3.setValue(data.value, 'select:' + (data.source == null ? '' : data.source));
      };

      selectMenu.onChangeRequest = function (data) {
        if (data.source === 'click' || data.source === 'keyboard') {
          selectMenu.hide();
          _this3.setState('display');
        }
      };

      selectMenu.setMenuSizes = function () {
        this.dom && (this.dom.style.width = this.container.domBase.offsetWidth + 'px');
      };

      selectMenu.onDataReady = function () {
        _this3.value = _menuMenu2['default'].defaultOptions.defaultValue;
      };

      selectMenu.onShow = function () {
        return _this3.onMenuShow(selectMenu, _this3.domBase);
      };
    }
  }, {
    key: 'initSearchMenu',
    value: function initSearchMenu() {
      var _this4 = this;

      this._prepareOptions();

      if (this.renderItemContentForSearch) {
        this.options.renderItemContent = this.renderItemContentForSearch;
      }

      this.options.typeToClassMap = this.typeToClassMapSearch;
      this.options.data = [];

      var searchMenu = this.searchMenu = new _menuMenu2['default'](this.options);
      searchMenu.container = this;

      delete this.options.renderItemContent;
      delete this.options.typeToClassMap;

      searchMenu.highlighter = this.highlighter;

      var origRenderItemContent = searchMenu.renderItemContent;
      searchMenu.renderItemContent = function (item, highlighter, escapeHtml) {
        return origRenderItemContent(item.data.sourceItem, highlighter, escapeHtml);
      };

      searchMenu.onChangeRequest = function (data) {
        if (data.source === 'click' || data.source === 'keyboard') {
          _this4.setState('display');
          _this4.setValue(data.value, 'search:' + (data.source == null ? '' : data.source));
        }
      };

      this.searchDataSource.onDataReady = function (data) {
        searchMenu.loadData(data);
        _this4.setState(data.length ? 'fullSearch' : 'displaySearch');
      };

      searchMenu.setMenuSizes = function () {
        this.dom && (this.dom.style.width = this.container.domBase.offsetWidth + 'px');
      };

      searchMenu.onShow = function () {
        return _this4.onMenuShow(searchMenu, _this4.domSearch);
      };
    }
  }, {
    key: 'loadData',
    value: function loadData(data) {
      this.selectMenu.loadData(data);
      this.onDataReady();
    }
  }, {
    key: 'loadPartialData',
    value: function loadPartialData(menu, data) {
      this.selectMenu.loadPartialData(menu, data);
    }
  }, {
    key: 'getItemByValue',
    value: function getItemByValue(value) {
      return this.selectMenu.getItemByValue(value);
    }
  }, {
    key: 'onMenuShow',
    value: function onMenuShow(menu, base) {
      var _this5 = this;

      setTimeout(function () {
        return _this5.domSearch && _this5.domSearch.focus();
      });
    }
  }, {
    key: 'setValue',
    value: function setValue(value, source) {
      if (value == null) {
        value = _menuMenu2['default'].defaultOptions.defaultValue;
      }

      if (this.value === value) return;
      if (this.selectMenu.setValue(value) === false) return;

      var oldValue = this.value;
      this.value = this.selectMenu.value;

      this.setState('display');

      this.syncDomBaseContentWithValue();
      this.syncProxy();

      var eventData = {
        oldValue: oldValue,
        value: this.value,
        source: source,
        userInitiated: SOURCE_IN_MAP.hasOwnProperty(source)
      };
      this.onChange(eventData);
      this.trigger('change', eventData);
    }
  }, {
    key: 'syncDomBaseContentWithValue',
    value: function syncDomBaseContentWithValue() {
      var item = this.selectMenu.hashValues[this.value];
      var typeClass;

      if (item && item.type && this.typeToClassMapBase && this.typeToClassMapBase[item.type]) {
        typeClass = this.typeToClassMapBase[item.type];
      }

      if (this.lastBaseTypeClass) {
        (0, _utilsDom_helpersDom_utils.removeClass)(this.domBase, this.lastBaseTypeClass);
      }

      if (typeClass) {
        (0, _utilsDom_helpersDom_utils.addClass)(this.domBase, this.typeToClassMapBase[item.type]);
      }

      this.lastBaseTypeClass = typeClass;

      var html = this.renderItemContentForBase(item || { label: this.defaultValueLabel });
      this.domBaseContent.innerHTML = html;
    }
  }, {
    key: 'syncProxy',
    value: function syncProxy() {
      this.hasProxy && (this.domProxy.value = this.value);
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.trigger('beforeDestroy');
      this.selectMenu.destroy();
      this.searchMenu.destroy();
      this.off();
      _menuMenu2['default'].unregisterInstance(this);
      this.destroyUI();
      this.domBasePositionObserver.destroy();
      this.domBasePositionObserver = null;
      (0, _utilsCore_utils.cleanObject)(this);
      this.isDestroyed = true;
    }
  }, {
    key: 'destroyUI',
    value: function destroyUI() {
      if (this.isInDom) {
        this.teardownDomEvents();
        this.dom.parentNode.removeChild(this.dom);
        this.domMenuHolder.parentNode.removeChild(this.domMenuHolder);
      }
      this.isInDom = false;
      this.domHolder = null;

      this.dom = null;
      this.domBase = null;
      this.domBaseContent = null;
      this.domSearch = null;
      this.domRef = null;
      this.domProxy = null;
    }
  }, {
    key: 'resetUI',
    value: function resetUI() {
      if (this.isInDom) {
        this.blur();
        (0, _utilsDom_helpersDom_utils.removeClass)(this.dom, this.clsHover);

        this.selectMenu.resetUI();
        this.searchMenu.resetUI();
      }
    }

    // -------------------- Rendering and DOM Manipulation -----------------------

  }, {
    key: 'postDomInsertionSetup',
    value: function postDomInsertionSetup() {
      this.syncDomBaseContentWithValue();
      this.syncProxy();
      this.disabled ? this.disable() : this.setupDomEvents();
    }
  }, {
    key: 'setupDomEvents',
    value: function setupDomEvents() {
      var _this6 = this;

      if (!this.isInDom || this.areDomEventsSet) {
        return;
      }

      var onSearchInput = function onSearchInput(e) {
        return _this6.onSearchInput(e);
      };

      this.onDom(this.domBase, 'click', function (e) {
        return _this6.onBaseClick(e);
      }).onDom(this.domBase, 'mousedown', function (e) {
        return _this6.onBaseMouseDown(e);
      }).onDom(this.domBase, 'focus', function (e) {
        return _this6.onBaseFocus(e);
      }).onDom(this.domBase, 'blur', function (e) {
        return _this6.onBaseBlur(e);
      }).onDom(this.domSearch, 'focus', function (e) {
        return _this6.onSearchFocus(e);
      }).onDom(this.domSearch, 'click', function (e) {
        return _this6.onSearchClick(e);
      }).onDom(this.domSearch, 'input', onSearchInput).onDom(this.domSearch, 'compositionstart', function (e) {
        return _this6.onCompositionStart(e);
      }).onDom(this.domSearch, 'compositionend', function (e) {
        _this6.onCompositionEnd(e);onSearchInput(e);
      }).onDom(this.dom, 'mouseenter', function (e) {
        return _this6.onRootMouseEnter(e);
      }).onDom(this.dom, 'mouseleave', function (e) {
        return _this6.onRootMouseLeave(e);
      });

      if (_utilsCore_utils.userAgent.ie9) {
        this.onDom(this.domSearch, 'keyup', onSearchInput).onDom(this.domSearch, 'cut', onSearchInput);
      }

      var onMouseDown = function onMouseDown(e) {
        return _this6.onDocumentMouseDown(e);
      };

      this.on('open', function () {
        return _this6.onDom(document, 'mousedown', onMouseDown);
      });
      this.on('close', function () {
        return _this6.offDom(document, 'mousedown', onMouseDown);
      });

      this.areDomEventsSet = true;
    }
  }, {
    key: 'teardownDomEvents',
    value: function teardownDomEvents() {
      if (!this.isInDom) {
        return;
      }

      this.offDom();
      this.releaseKeyboard();
      this.areDomEventsSet = false;
    }
  }, {
    key: 'render',
    value: function render() {
      var html = '<div id="' + this.id + '" class="' + this.clsSelectMenuRoot + ' ' + this.clsDefault + '">\n                 ' + (this.hasProxy ? '<input type="hidden" name="' + this.proxyName + '" id="' + this.proxyId + '">' : '') + '\n                 <button id="' + this.baseId + '" class="' + this.clsBaseButton + '" role="button" tabindex="0" type="button">\n                   <span class="' + this.clsBaseArrow + '"></span>\n                   <span id="' + this.baseContentId + '" class="' + this.clsBaseContent + '"></span>\n                 </button>\n                 <input id="' + this.searchId + '" class="' + this.clsSearch + '" tabindex="0">\n               </div>';
      return html;
    }
  }, {
    key: 'defaultRenderItemContentForBase',
    value: function defaultRenderItemContentForBase(item) {
      return this.enableHtmlEscape && item.enableHtmlEscape ? (0, _utilsCore_utils.escapeHtml)(item.label) : item.label;
    }
  }, {
    key: 'putInDom',
    value: function putInDom() {
      if (this.isInDom) {
        return;
      }
      if (this.domRef) {
        this.domRef.insertAdjacentHTML('beforebegin', this.htmlBuffer);
        this.domRef.parentNode.removeChild(this.domRef);
      } else {
        this.domHolder.insertAdjacentHTML('beforeend', this.htmlBuffer);
      }
      this.isInDom = true;

      this.dom = this.domHolder.querySelector('#' + this.id);
      this.domBase = this.domHolder.querySelector('#' + this.baseId);
      this.domBaseContent = this.domHolder.querySelector('#' + this.baseContentId);
      this.domSearch = this.domHolder.querySelector('#' + this.searchId);
      this.domProxy = this.domHolder.querySelector('#' + this.proxyId);
    }

    // --------------------       STATES      -----------------------

  }, {
    key: 'setState',
    value: function setState(state) {
      if (this.state === state) {
        return;
      }
      this.oldState = this.state;
      this.state = state;

      this.states[state].enter.call(this);
      this.onStateChange();
    }
  }, {
    key: 'commonKeyDown',
    value: function commonKeyDown(e) {
      if (e.keyCode === _utilsCore_utils.keyCodes.ESCAPE) {
        this.setState('display');
        return false;
      }

      return true;
    }

    // -------------------- Common public methods -----------------------

  }, {
    key: 'focus',
    value: function focus() {
      if (this.isFocused) {
        return;
      }

      this.isFocused = true;
      this.clsStateManager.addState(this.clsFocused);

      this.captureKeyboard();

      _menuMenu2['default'].registerAsActive(this.id);
      this.onFocus();
      this.trigger('focus');
    }
  }, {
    key: 'blur',
    value: function blur() {
      if (!this.isFocused) {
        return;
      }

      this.isFocused = false;
      this.clsStateManager.removeState(this.clsFocused);

      this.releaseKeyboard();

      this.selectMenu.blur();
      this.searchMenu.blur();
      this.domBase.blur();

      this.onBlur();
      this.trigger('blur');
    }
  }, {
    key: 'open',
    value: function open() {
      this.setState('displaySearch');
    }
  }, {
    key: 'close',
    value: function close() {
      this.setState('display');
    }
  }, {
    key: 'disable',
    value: function disable() {
      this.setDisableState(true);
    }
  }, {
    key: 'enable',
    value: function enable() {
      this.setDisableState(false);
    }
  }, {
    key: 'setDisableState',
    value: function setDisableState(isDisabled) {
      this.disabled = isDisabled;

      if (this.disabled) {
        this.blur();
        this.close();
        this.teardownDomEvents();
        this.clsStateManager.addState(this.clsDisabled);
        this.domBase.disabled = true;
        this.domSearch.disabled = true;
        if (this.hasProxy) {
          this.domProxy.disabled = true;
        }
      } else {
        this.setupDomEvents();
        this.domBase.disabled = false;
        this.domSearch.disabled = false;
        if (this.hasProxy) {
          this.domProxy.disabled = false;
        }
        this.clsStateManager.removeState(this.clsDisabled);
      }

      this.onDisabledChanged();
      this.trigger('disableChanged');
    }
  }, {
    key: 'show',
    value: function show() {
      this.dom.style.display = '';
      this.trigger('show');
    }
  }, {
    key: 'hide',
    value: function hide() {
      this.dom.style.display = 'none';
      this.trigger('hide');
    }
  }, {
    key: 'getDomBaseForPositioning',
    value: function getDomBaseForPositioning() {
      return this.domSearch;
    }
  }, {
    key: 'captureKeyboard',
    value: function captureKeyboard() {
      var _this7 = this;

      if (this.isKeyboardCaptured) {
        return;
      }
      this.isKeyboardCaptured = true;
      this.onDom(document, 'keypress', function (e) {
        return _this7.onKeyPress(e);
      }).onDom(document, 'keydown', function (e) {
        return _this7.onKeyDown(e);
      });
    }
  }, {
    key: 'releaseKeyboard',
    value: function releaseKeyboard() {
      this.isKeyboardCaptured = false;
      this.offDom(document, 'keypress').offDom(document, 'keydown');
    }

    // ----------------------   Hooks   -------------------------

  }, {
    key: 'onChange',
    value: function onChange() {}
  }, {
    key: 'onFocus',
    value: function onFocus() {}
  }, {
    key: 'onBlur',
    value: function onBlur() {}
  }, {
    key: 'onDataReady',
    value: function onDataReady() {}
  }, {
    key: 'onDisabledChanged',
    value: function onDisabledChanged() {}
  }, {
    key: 'onDestroy',
    value: function onDestroy() {}
  }, {
    key: 'onStateChange',
    value: function onStateChange() {}

    // -------------------- Event Handlers -----------------------

  }, {
    key: 'onKeyPress',
    value: function onKeyPress(e) {
      this.lastKeyPressed = e.which;
    }
  }, {
    key: 'onKeyDown',
    value: function onKeyDown(e) {
      if (this.commonKeyDown(e)) {
        this.states[this.state].keyDown.call(this, e);
      }
    }
  }, {
    key: 'onDocumentMouseDown',
    value: function onDocumentMouseDown(e) {
      var _this8 = this;

      if (!this.isInDom || !e.target) {
        return;
      }

      var isInDom = this.dom.contains(e.target) || this.dom === e.target;
      var isInMenu = this.domMenuHolder.contains(e.target) || this.domMenuHolder === e.target;

      if (isInDom || isInMenu) {
        setTimeout(function () {
          return _this8.domSearch && _this8.domSearch.focus();
        });
      } else {
        this.blur();
        this.close();
        // IE and Edge require this as hiding a focused element such as domSearch
        // messes up with the focus logic. Explicitly blurring domSearch though
        // doesn't solve the problem.
        e.target.focus();
      }
    }
  }, {
    key: 'onBaseClick',
    value: function onBaseClick(e) {}
  }, {
    key: 'onBaseMouseDown',
    value: function onBaseMouseDown() {
      (0, _utilsDom_helpersDom_utils.positionDomIntoView)(this.domBase);
      this.focus();
      this.selectMenu.syncViewWithValue();
      this.open();
    }
  }, {
    key: 'onBaseFocus',
    value: function onBaseFocus() {
      this.focus();
    }
  }, {
    key: 'onBaseBlur',
    value: function onBaseBlur() {
      this.blur();
    }
  }, {
    key: 'onSearchFocus',
    value: function onSearchFocus() {
      this.focus();
    }
  }, {
    key: 'onCompositionStart',
    value: function onCompositionStart() {
      this.isComposingInput = true;
    }
  }, {
    key: 'onCompositionEnd',
    value: function onCompositionEnd() {
      this.isComposingInput = false;
    }
  }, {
    key: 'onSearchInput',
    value: function onSearchInput(e) {
      this.setState('fullSearch');

      if (this.isComposingInput) return;

      this.searchDataSource.filter(this.selectMenu, this.domSearch.value);
      this.positionMenu(this.searchMenu, this.domSearch, this.preferredPosition);
    }
  }, {
    key: 'onSearchClick',
    value: function onSearchClick() {
      this.close();
    }
  }, {
    key: 'onRootMouseEnter',
    value: function onRootMouseEnter(e) {
      this.clsStateManager.addState(this.clsHover);
    }
  }, {
    key: 'onRootMouseLeave',
    value: function onRootMouseLeave(e) {
      this.clsStateManager.removeState(this.clsHover);
    }
  }, {
    key: 'states',
    get: function get() {
      return _combo_select_menu_states2['default'];
    }
  }]);

  return ComboSelectMenu;
})((0, _utilsCore_utils.mixin)(_utilsMixinsOptions_mixin2['default'], _utilsMixinsObservable_mixin2['default'], _utilsMixinsPositionable_mixin2['default'], _utilsMixinsCss_scoping_mixin2['default']));

exports['default'] = ComboSelectMenu;
module.exports = exports['default'];
},{"../menu/menu":4,"../utils/core_utils":11,"../utils/datasources/filtering_datasource":12,"../utils/dom_helpers/css_class_state_machine":17,"../utils/dom_helpers/document_clicks_monitor":18,"../utils/dom_helpers/dom_position_observer":19,"../utils/dom_helpers/dom_utils":20,"../utils/dom_helpers/vertical_menu_positioner":22,"../utils/mixins/css_scoping_mixin":26,"../utils/mixins/observable_mixin":27,"../utils/mixins/options_mixin":28,"../utils/mixins/positionable_mixin":29,"../utils/parsers/label_concatenator":30,"../utils/renderers/highlighting_renderer":32,"./combo_select_menu_states":2}],2:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

exports['default'] = {
  display: {
    enter: function enter() {
      this.isOpen = false;
      (0, _utilsDom_helpersDom_utils.addClass)(this.dom, this.clsSelectMenuRoot);
      this.clsStateManager.removeState(this.clsOpen);
      this.stopObservingDomBasePosition();
      this.setPosition('down');

      this.searchMenu.hide();
      this.domSearch.value = '';
      this.domSearch.style.display = 'none';
      this.searchDataSource.reset();

      this.domBase.style.visibility = '';
      if (this.isFocused) {
        this.domBase.focus();
      }
      this.selectMenu.hide();
      this.trigger('close');
    },

    keyDown: function keyDown(e) {
      var _this = this;

      var key = String.fromCharCode(e.keyCode);

      // This covers the case when the user opens the widget
      // using a keyboard stroke that is reflected in the search box.
      // The widget uses the input event on the search box to track
      // for searched keywords.
      if (!this.isOpen && (key >= '0' && key <= '9' || key >= 'A' && key <= 'z') && e.keyCode !== 91) {
        // 91 - 'Command' key
        // For keydown events the keyCode is restricted only to ASCII and it
        // does not reflect the real character entered. That is why we are
        // waiting for the keypress event to fire so that we know what was the
        // character that was entered. We use to log it in this.lastKeyPressed.

        this.setState('fullSearch');

        setTimeout(function () {
          _this.domSearch.value = String.fromCharCode(_this.lastKeyPressed);
          _this.searchDataSource.filter(_this.selectMenu, _this.domSearch.value);
          _this.positionMenu(_this.searchMenu, _this.domSearch, _this.preferredPosition);
        });

        return false;
      }

      switch (e.keyCode) {
        case _utilsCore_utils.keyCodes.HOME:
        case _utilsCore_utils.keyCodes.END:
        case _utilsCore_utils.keyCodes.PAGE_DOWN:
        case _utilsCore_utils.keyCodes.PAGE_UP:
        case _utilsCore_utils.keyCodes.DOWN:
        case _utilsCore_utils.keyCodes.UP:
        case _utilsCore_utils.keyCodes.LEFT:
        case _utilsCore_utils.keyCodes.ENTER:
        case _utilsCore_utils.keyCodes.NUMPAD_ENTER:
        case _utilsCore_utils.keyCodes.RIGHT:
          if (!this.isVisible) {
            this.setState('displaySearch');
            e.preventDefault();
          }
          return;
        case _utilsCore_utils.keyCodes.TAB:
          return;
      }
      this.selectMenu.onKeyDown(e);
    }
  },

  displaySearch: {
    enter: function enter() {
      var _this2 = this;

      var wasOpen = this.isOpen;
      this.isOpen = true;
      (0, _utilsDom_helpersDom_utils.addClass)(this.dom, this.clsSelectMenuRoot);
      this.clsStateManager.addState(this.clsOpen);
      this.stopObservingDomBasePosition();
      this.searchMenu.hide();
      if (this.oldState !== 'fullSearch') {
        this.domSearch.value = '';
      }
      this.domSearch.style.display = '';
      setTimeout(function () {
        return _this2.domSearch.focus();
      });
      this.domBase.style.visibility = 'hidden';

      this.selectMenu.show();
      this.observeDomBasePositionOnce(function () {
        return _this2.close();
      });
      this.positionMenu(this.selectMenu, this.domSearch, this.preferredPosition);

      !wasOpen && this.trigger('open');
    },

    keyDown: function keyDown(e) {
      var isLeftRight = e.keyCode === _utilsCore_utils.keyCodes.LEFT || e.keyCode === _utilsCore_utils.keyCodes.RIGHT;
      if (isLeftRight && this.domSearch.value.length > 0) {
        return;
      }
      this.selectMenu.onKeyDown(e);
    }
  },

  fullSearch: {
    enter: function enter() {
      var _this3 = this;

      var wasOpen = this.isOpen;
      this.isOpen = true;
      (0, _utilsDom_helpersDom_utils.addClass)(this.dom, this.clsSelectMenuRoot);
      this.clsStateManager.addState(this.clsOpen);
      this.stopObservingDomBasePosition();
      this.searchMenu.dom.style.visibility = 'hidden';
      this.domBase.style.visibility = 'hidden';
      this.searchMenu.show();
      this.domSearch.style.display = '';
      this.domSearch.focus();

      this.selectMenu.hide();

      setTimeout(function () {
        _this3.observeDomBasePositionOnce(function () {
          return _this3.close();
        });
        _this3.positionMenu(_this3.searchMenu, _this3.domSearch, _this3.preferredPosition);
        _this3.searchMenu.dom.style.visibility = '';
      });

      !wasOpen && this.trigger('open');
    },

    keyDown: function keyDown(e) {
      if (e.keyCode === _utilsCore_utils.keyCodes.LEFT || e.keyCode === _utilsCore_utils.keyCodes.RIGHT) {
        return;
      }
      this.searchMenu.onKeyDown(e);
    }
  }
};
module.exports = exports['default'];
},{"../utils/core_utils":11,"../utils/dom_helpers/dom_utils":20}],3:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _utilsDom_helpersShims = _dereq_('./utils/dom_helpers/shims');

var _utilsDom_helpersShims2 = _interopRequireDefault(_utilsDom_helpersShims);

var _menuMenu = _dereq_('./menu/menu');

var _menuMenu2 = _interopRequireDefault(_menuMenu);

var _select_menuSelect_menu = _dereq_('./select_menu/select_menu');

var _select_menuSelect_menu2 = _interopRequireDefault(_select_menuSelect_menu);

var _combo_select_menuCombo_select_menu = _dereq_('./combo_select_menu/combo_select_menu');

var _combo_select_menuCombo_select_menu2 = _interopRequireDefault(_combo_select_menuCombo_select_menu);

var _search_menuSearch_menu = _dereq_('./search_menu/search_menu');

var _search_menuSearch_menu2 = _interopRequireDefault(_search_menuSearch_menu);

var _tag_menuTag_menu = _dereq_('./tag_menu/tag_menu');

var _tag_menuTag_menu2 = _interopRequireDefault(_tag_menuTag_menu);

var _utilsMake_jquery_plugin = _dereq_('./utils/make_jquery_plugin');

var _utilsMake_jquery_plugin2 = _interopRequireDefault(_utilsMake_jquery_plugin);

// renders

var _utilsRenderersHighlighting_renderer = _dereq_('./utils/renderers/highlighting_renderer');

var _utilsRenderersHighlighting_renderer2 = _interopRequireDefault(_utilsRenderersHighlighting_renderer);

// datasources

var _utilsDatasourcesSimple_menu_datasource = _dereq_('./utils/datasources/simple_menu_datasource');

var _utilsDatasourcesSimple_menu_datasource2 = _interopRequireDefault(_utilsDatasourcesSimple_menu_datasource);

var _utilsDatasourcesRemote_search_datasource = _dereq_('./utils/datasources/remote_search_datasource');

var _utilsDatasourcesRemote_search_datasource2 = _interopRequireDefault(_utilsDatasourcesRemote_search_datasource);

var _utilsDatasourcesFiltering_datasource = _dereq_('./utils/datasources/filtering_datasource');

var _utilsDatasourcesFiltering_datasource2 = _interopRequireDefault(_utilsDatasourcesFiltering_datasource);

// dom helpers

var _utilsDom_helpersVertical_menu_positioner = _dereq_('./utils/dom_helpers/vertical_menu_positioner');

var _utilsDom_helpersVertical_menu_positioner2 = _interopRequireDefault(_utilsDom_helpersVertical_menu_positioner);

//parsers

var _utilsParsersLabel_concatenator = _dereq_('./utils/parsers/label_concatenator');

var _utilsParsersLabel_concatenator2 = _interopRequireDefault(_utilsParsersLabel_concatenator);

(0, _utilsDom_helpersShims2['default'])();

if ('jQuery' in window) {
  (0, _utilsMake_jquery_plugin2['default'])('zdMenu', _menuMenu2['default']);
  (0, _utilsMake_jquery_plugin2['default'])('zdSelectMenu', _select_menuSelect_menu2['default']);
  (0, _utilsMake_jquery_plugin2['default'])('zdComboSelectMenu', _combo_select_menuCombo_select_menu2['default']);
  (0, _utilsMake_jquery_plugin2['default'])('zdSearchMenu', _search_menuSearch_menu2['default']);
  (0, _utilsMake_jquery_plugin2['default'])('zdTagMenu', _tag_menuTag_menu2['default']);
}

var MenuUtils = {
  VerticalMenuPositioner: _utilsDom_helpersVertical_menu_positioner2['default'],
  HighlightingRenderer: _utilsRenderersHighlighting_renderer2['default'],
  FilteringDataSource: _utilsDatasourcesFiltering_datasource2['default'],
  SimpleMenuDataSource: _utilsDatasourcesSimple_menu_datasource2['default'],
  RemoteSearchDataSource: _utilsDatasourcesRemote_search_datasource2['default'],
  LabelConcatenator: _utilsParsersLabel_concatenator2['default']
};

exports.Menu = _menuMenu2['default'];
exports.MenuUtils = MenuUtils;
exports.SelectMenu = _select_menuSelect_menu2['default'];
exports.ComboSelectMenu = _combo_select_menuCombo_select_menu2['default'];
exports.SearchMenu = _search_menuSearch_menu2['default'];
exports.TagMenu = _tag_menuTag_menu2['default'];
},{"./combo_select_menu/combo_select_menu":1,"./menu/menu":4,"./search_menu/search_menu":6,"./select_menu/select_menu":7,"./tag_menu/tag_menu":10,"./utils/datasources/filtering_datasource":12,"./utils/datasources/remote_search_datasource":14,"./utils/datasources/simple_menu_datasource":15,"./utils/dom_helpers/shims":21,"./utils/dom_helpers/vertical_menu_positioner":22,"./utils/make_jquery_plugin":25,"./utils/parsers/label_concatenator":30,"./utils/renderers/highlighting_renderer":32}],4:[function(_dereq_,module,exports){
// EVENTS
// --------------------------------------------------------
//
// Triggers the following events through
// dom element this.domHolder:
// --------------------------------------------------------
// - change,         params: source, oldValue, value, userInitiated
// - focus,          params: source
// - blur,           params: source
// - disableChanged, params: source
// - show,           params: source
// - hide,           params: source
// - keyDown         params: source, domEvent

// MENU MODEL
// --------------------------------------------------------
//
// Item interface:
// --------------------------------------------------------
// - id         -> internal field
// - label      -> from data source
// - value      -> from data source
// - menu       -> array holding child items
// - role       -> internal field
// - parentItem -> internal field
// - parentMenu -> internal field
// - index      -> internal field
// - isInDom    -> internal field

// Menu interface:
// --------------------------------------------------------
// - id         -> internal field
// - parentItem -> internal field
// - parentMenu -> internal field
// - isInDom    -> internal field

// Item value rules:
// --------------------------------------------------------
// Values explicitly defined as 'null' or 'undefined'
// are converted to the defaultValue value which should
// not be null itself.
// The value can be set to null when there is no value
// specified explicitly.
// The value cannot be set to null from parsed data.
// The value can only be set to null internally.
// The value of an item with children/menu is ignored.

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

var _menu_transitions = _dereq_('./menu_transitions');

var _menu_transitions2 = _interopRequireDefault(_menu_transitions);

var _utilsMixinsOptions_mixin = _dereq_('../utils/mixins/options_mixin');

var _utilsMixinsOptions_mixin2 = _interopRequireDefault(_utilsMixinsOptions_mixin);

var _utilsMixinsObservable_mixin = _dereq_('../utils/mixins/observable_mixin');

var _utilsMixinsObservable_mixin2 = _interopRequireDefault(_utilsMixinsObservable_mixin);

var _utilsParsersMenu_data_parser = _dereq_('../utils/parsers/menu_data_parser');

var _utilsParsersMenu_data_parser2 = _interopRequireDefault(_utilsParsersMenu_data_parser);

var _utilsIteratorsHash_iterator = _dereq_('../utils/iterators/hash_iterator');

var _utilsIteratorsHash_iterator2 = _interopRequireDefault(_utilsIteratorsHash_iterator);

var START = 1;
var END = 2;

var defaultOptions = {
  value: null,
  defaultValue: '', // when initializing the control, all null values are converted to this value
  defaultValueLabel: '-',
  backLinkLabel: 'Back',

  domHolderSelector: null,

  disabled: false,

  transitionMode: 'sliding', // ['direct' | 'stacking' | 'sliding']
  transitionDuration: 300,
  transitionEasing: 'easeOutQuad', // easing function, borrowed from jQuery UI

  clsRoot: 'zd-menu-root',
  clsMenuPanelRoot: 'zd-menu-panel-root',

  clsMenuPanelHolder: 'zd-menu-panel-holder',
  clsListHolder: 'zd-menu-list-holder',
  clsItem: 'zd-menu-item',
  clsBackLink: 'zd-menu-back-link',
  clsMenuLink: 'zd-menu-link',
  clsLabel: 'zd-menu-label',
  clsBaseArrow: 'zd-selectmenu-base-arrow zd-icon-arrow-down',
  clsItemArrow: 'zd-menu-item-arrow zd-icon-arrow-right',
  clsBackArrow: 'zd-menu-item-arrow zd-icon-arrow-left',
  clsMenuItemIcon: 'zd-menu-item-icon',

  clsDisabled: 'zd-state-disabled',
  clsFocused: 'zd-state-focus',
  clsHover: 'zd-state-hover',
  clsZeroState: 'zd-state-zero',
  clsItemFocused: 'zd-item-focus',
  clsItemDisabled: 'zd-item-disabled',
  clsAutofitMode: 'zd-menu-autofit-mode',

  isVisible: true,
  autofitMode: true, // usually used for drop down menus flat model structure
  keyboardCue: false,
  keyboardCueAction: 'select', // can be ['select' | 'focus'],
  enableMenuItemIcons: false,
  enableHtmlEscape: true,

  goToStartAfterReachingEnd: false,
  goToEndAfterReachingStart: false,

  zeroStateMessgae: 'No items',
  hasZeroState: false
};

var instances = {};

var Menu = (function (_mixin) {
  _inherits(Menu, _mixin);

  _createClass(Menu, null, [{
    key: 'registerInstance',
    value: function registerInstance(instance) {
      this.instances[instance.id] = instance;
    }
  }, {
    key: 'unregisterInstance',
    value: function unregisterInstance(instance) {
      delete this.instances[instance.id];
    }
  }, {
    key: 'count',
    value: function count() {
      var c = 0;
      for (var i in this.instances) {
        if (this.instances.hasOwnProperty(i)) {
          c++;
        }
      }
      return c;
    }
  }, {
    key: 'zombies',
    value: function zombies() {
      var instance,
          zombies = [];

      for (var i in this.instances) {
        if (!this.instances.hasOwnProperty(i)) continue;
        instance = this.instances[i];
        if (instance.dom && !document.body.contains(instance.dom)) {
          zombies.push(instance);
        }
      }

      return zombies;
    }
  }, {
    key: 'registerAsActive',
    value: function registerAsActive(menuId) {
      this.activeMenuId = menuId;
      this.onActiveMenuSet();
    }
  }, {
    key: 'unregisterAsActive',
    value: function unregisterAsActive(menuId) {
      this.activeMenuId = this.activeMenuId === menuId ? null : this.activeMenuId;
    }
  }, {
    key: 'onActiveMenuSet',
    value: function onActiveMenuSet() {
      var _this = this;

      if (this._observesWindowResize) return;

      this._observesWindowResize = true;
      window.addEventListener('resize', function () {
        if (!_this.activeMenuId) return;
        var instance = _this.instances[_this.activeMenuId];
        if (!instance) return;

        instance.onWindowResize ? instance.onWindowResize() : instance.close();
      }, false);
    }
  }, {
    key: 'defaultOptions',
    get: function get() {
      return defaultOptions;
    }
  }, {
    key: 'instances',
    get: function get() {
      return instances;
    }
  }]);

  function Menu(options) {
    _classCallCheck(this, Menu);

    _get(Object.getPrototypeOf(Menu.prototype), 'constructor', this).call(this, options);

    this.type = 'zdMenu';
    this.rootItem = null;
    this.activeItem = null;
    this.focusedItem = null;

    this.isFocused = false;
    this.isInDom = false;
    this.isDestroyed = false;
    this.isVisible = null;
    this.isZeroState = false;
    this.isKeyboardCaptured = false;
    this.isKeyboardNavigation = false; // used to disable mouse selection while keyboard navigating
    this.navigationMode = 'mouse'; //['mouse' | 'keyboard']
    this.inTransition = false;

    this.lastKey = null;
    this.lastIndex = -1;
    this.lastActiveMenu = null;

    this.typeToClassMap = null;
    this.roleToClassMap = null; // stylable roles: "uiBackLink", "uiMenuLink"

    this.highlighter = null;

    // All classes subclassing this one should have premerged their default options.
    // Option merging is done only by the last subclass
    this.setOptions(options, this.defaultOptions);

    this.parser = new _utilsParsersMenu_data_parser2['default'](this, this.postParser);
    this.container = this.options.container;

    this.roleToClassMap = this.roleToClassMap || {};
    this.roleToClassMap.uiBackLink = this.roleToClassMap.uiBackLink || this.clsBackLink;
    this.roleToClassMap.uiMenuLink = this.roleToClassMap.uiMenuLink || this.clsMenuLink;
    this.roleToClassMap.uiLabel = this.roleToClassMap.uiLabel || this.clsLabel;

    this.id = (0, _utilsCore_utils.getUniqId)();
    this.domMenuPanelId = (0, _utilsCore_utils.getUniqId)();
    this.hasChrome = !!options.renderMenuPanel;

    this.rootItem = { role: 'root' };
    this.hashIds = {};
    this.hashValues = {};
    this.lastValue = null;

    this.renderMenuPanel = this.options.renderMenuPanel || this.renderMenuPanel;
    this.renderDisplayValue = this.options.renderDisplayValue;
    this.renderItemContent = this.options.renderItemContent || this.renderItemContent;
    this.renderItemIcon = this.options.renderItemIcon || this.renderItemIcon;
    this.renderZeroState = this.options.renderZeroState || this.renderZeroState;

    if (this.hasChrome) {
      this.clsRoot = this.clsRoot + ' ' + this.clsMenuPanelRoot;
    }

    if (this.domHolder) {
      // compatibility with jquery
      if (typeof this.domHolder === 'string') {
        this.domHolderSelector = this.domHolder;
        this.domHolder = document.querySelector(this.domHolderSelector);
      } else {
        this.domHolder = this.domHolder[0] ? this.domHolder[0] : this.domHolder;
      }
    }

    if (this.domHolder || this.domHolderSelector) {
      this.appendTo(this.domHolder || document.querySelector(this.domHolderSelector));
    }
  }

  _createClass(Menu, [{
    key: 'appendTo',
    value: function appendTo(target) {
      if (this.isInDom || !target) return;

      this.domHolder = target;

      Menu.registerInstance(this);
      this.onSetup && this.onSetup();

      this.htmlBuffer = this.render();
      this.putInDom();
      this.postDomInsertionSetup();

      this.data && this.loadData(this.data);
      this.value = this.options.value != null ? this.options.value : this.defaultValue;
      this.activeItem = this.hashValues[this.value];
      this.syncViewWithValue(true);
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.trigger('beforeDestroy');
      this.destroyUI();
      this.off();
      Menu.unregisterInstance(this);
      this.onDestroy();
      (0, _utilsCore_utils.cleanObject)(this);
      this.isDestroyed = true;
    }
  }, {
    key: 'reset',
    value: function reset() {
      this.setValue(this.options.value);
    }
  }, {
    key: 'resetFull',
    value: function resetFull() {
      this.resetUI();

      this.rootItem = { role: 'root' };
      this.data = {};
      this.hashIds = {};
      this.hashValues = {};

      this.value = this.defaultValue;
      this.lastValue = null;
      this.activeItem = null;
      this.focusedItem = null;
    }

    // ----------------------------------------------
    //                      Model
    // ----------------------------------------------

  }, {
    key: 'loadData',
    value: function loadData(data) {
      this.onBeforeLoad && this.onBeforeLoad();
      var isFocused = this.isFocused;
      this.resetFull();

      this.data = data;
      this.parser.parse(this.data);
      this.onDataReady();

      this.lastValue = null;
      this.focusedItem = null;

      this.resolveAutofitMode();
      this.resolveZeroState();

      this.syncViewWithZeroState();
      this.syncViewWithMode();
      this.syncViewWithValue();

      isFocused && this.focus();
      this.onLoad && this.onLoad();
      this.trigger('dataLoaded');
    }
  }, {
    key: 'loadPartialData',
    value: function loadPartialData(menu, data) {
      this.parser.parsePartial(menu, data);
      this.syncViewWithZeroState();
      this.syncViewWithMode();

      if (this.activeMenu === menu && this.isVisible) {
        var menuDom = this.putMenuInDom(menu);
        menuDom.setAttribute('aria-expanded', 'true');
        $(menuDom).show();
        this.forceTrueScrollHeightForMenu(menuDom);
      }
    }
  }, {
    key: 'getValue',
    value: function getValue() {
      return this.value;
    }

    // source can be - ['click' | 'keyboard' | 'keyboardCue']

  }, {
    key: 'setValue',
    value: function setValue(value, source, activeItem) {
      var item = this.getItemByValue(value);

      var eventData = {
        oldValue: this.lastValue,
        value: value,
        userInitiated: source === 'click' || source === 'keyboard' || source === 'keyboardCue',
        source: source,
        record: item
      };

      if (this.onChangeRequest(eventData) === false) return false;

      if (this.itemIsDisabled(item)) return false;

      if (value == null || !item) {
        value = this.defaultValue;
      }

      this.value = value;

      if (this.lastValue !== this.value) {
        this.lastValue = this.value;
        this.activeItem = this.hashValues[this.value] || activeItem;
        this.syncViewWithValue();
        this.onChange(eventData);
        this.trigger('change', eventData);
      }
    }
  }, {
    key: 'getDisplayValue',
    value: function getDisplayValue() {
      if (this.activeItem) {
        if (this.options.renderDisplayValue) {
          return this.options.renderDisplayValue(this.activeItem, _utilsCore_utils.escapeHtml);
        } else {
          return this.enableHtmlEscape && this.activeItem.enableHtmlEscape ? (0, _utilsCore_utils.escapeHtml)(this.activeItem.label) : this.activeItem.label;
        }
      }
      return this.defaultValueLabel;
    }
  }, {
    key: 'getParentMenu',
    value: function getParentMenu(item) {
      var parentItem = item.parentItem;
      if (parentItem == null || parentItem && parentItem.role === 'root') {
        return null;
      }
      return parentItem.parentMenu;
    }
  }, {
    key: 'getItemByValue',
    value: function getItemByValue(value) {
      return this.hashValues ? this.hashValues[value] : null;
    }
  }, {
    key: 'getItemIndex',
    value: function getItemIndex(item) {
      if (!item.parentMenu) return -1;
      for (var i = 0, menu = item.parentMenu, len = menu.length; i < len; i++) {
        if (menu[i] === item) return i;
      }
      return -1;
    }
  }, {
    key: 'getItemIterator',
    value: function getItemIterator() {
      if (!this._itemIterator) {
        this._itemIterator = new _utilsIteratorsHash_iterator2['default']();
      }
      this._itemIterator.data = this.hashValues || {};
      return this._itemIterator;
    }
  }, {
    key: 'domToItem',
    value: function domToItem(dom) {
      return this.hashIds[typeof dom === 'string' ? dom : dom.id];
    }
  }, {
    key: 'itemToDom',
    value: function itemToDom(item) {
      return (0, _utilsDom_helpersDom_utils.domById)(item.id);
    }
  }, {
    key: 'itemHasMenu',
    value: function itemHasMenu(item) {
      return !!(item.menu && item.menu.length);
    }
  }, {
    key: 'itemIsDisabled',
    value: function itemIsDisabled(item) {
      return !!(item && item.hasOwnProperty('enabled') && item.enabled === false);
    }
  }, {
    key: 'itemIsSelectable',
    value: function itemIsSelectable(item) {
      return item != null && item.role !== 'uiLabel' && !this.itemIsDisabled(item);
    }
  }, {
    key: 'itemIsBackHelper',
    value: function itemIsBackHelper(item) {
      return item.role === 'uiBackLink';
    }
  }, {
    key: 'firstSelectableMenuItem',
    value: function firstSelectableMenuItem(menu) {
      return menu && this._getNextSelectableItem(menu, 0);
    }
  }, {
    key: 'resolveAutofitMode',
    value: function resolveAutofitMode() {
      if (this.rootItem && this.rootItem.menu) {
        var rootMenu = this.rootItem.menu;
        for (var i = 0; i < rootMenu.length; i++) {
          if (rootMenu[i].menu) {
            this.autofitMode = false;
            return;
          }
        }
      }
      this.autofitMode = true;
    }

    // ----------------------------------------------
    //                      View
    // ----------------------------------------------

  }, {
    key: 'destroyUI',
    value: function destroyUI() {
      if (this.isInDom) {
        this.dom.parentNode.removeChild(this.dom);
        this.offDom();
      }
      this.isInDom = false;
      this.domHolder = null;
      this.domRef = null;
      this.domMenuPanel = null;
    }
  }, {
    key: 'resetUI',
    value: function resetUI() {
      this.activeItem = null;
      this.focusedItem = null;
      if (this.isInDom) {
        this.blur();
        var elements = this.domMenuPanel.querySelectorAll(':not(.zd-zero-state-holder)');
        [].forEach.call(elements, function (el) {
          return el.parentNode.removeChild(el);
        });
        (0, _utilsDom_helpersDom_utils.removeClass)(this.dom, this.clsHover);
      }
    }

    // -------------------- Rendering and DOM Manipulation -----------------------

  }, {
    key: 'render',
    value: function render() {
      return '<div id="' + this.id + '" class="' + this.clsRoot + '"' + (this.isVisible ? '' : ' style="display:none"') + '>' + this.renderZeroState() + this.renderMenuPanel() + '</div>';
    }
  }, {
    key: 'renderMenuPanel',
    value: function renderMenuPanel() {
      return '';
    }
  }, {
    key: 'renderZeroState',
    value: function renderZeroState() {
      if (!this.hasZeroState) return '';
      return '<div class="zd-zero-state-holder">' + this.zeroStateMessgae + '</div>';
    }
  }, {
    key: 'renderMenu',
    value: function renderMenu(menu) {
      if (!menu) {
        return '';
      }

      var html = ['<ul id="' + menu.id + '" class="' + this.clsListHolder + '" role="menu">'];

      for (var i = 0, len = menu.length; i < len; i++) {
        html[html.length] = this.renderItem(menu[i]);
      }

      html[html.length] = '</ul>';
      return html.join('');
    }
  }, {
    key: 'renderItem',
    value: function renderItem(item) {
      var arrowHtml;

      var itemClass = this.clsItem;

      if (this.typeToClassMap && item.type && this.typeToClassMap[item.type]) {
        itemClass += ' ' + this.typeToClassMap[item.type];
      }
      if (this.roleToClassMap && item.role && this.roleToClassMap[item.role]) {
        itemClass += ' ' + this.roleToClassMap[item.role];
      }
      if (this.itemIsDisabled(item)) {
        itemClass += ' ' + this.clsItemDisabled;
      }

      if (item.menu) {
        arrowHtml = '<span class="' + this.clsItemArrow + '"></span>';
      } else if (item.role === 'uiBackLink') {
        item.label = this.backLinkLabel;
        arrowHtml = '<span class="' + this.clsBackArrow + '"></span>';
      } else {
        arrowHtml = '';
      }

      var menuItemIconHtml = this.enableMenuItemIcons ? this.renderItemIcon(item) : '';

      return '<li id="' + item.id + '" class="' + itemClass + '" role="presentation">' + arrowHtml + menuItemIconHtml + '<a tabindex="-1" role="menuitem">' + this.renderItemContent(item, this.highlighter, _utilsCore_utils.escapeHtml) + '</a>' + '</li>';
    }
  }, {
    key: 'renderItemIcon',
    value: function renderItemIcon(item) {
      return '<span class="' + this.clsMenuItemIcon + '"></span>';
    }
  }, {
    key: 'renderItemContent',
    value: function renderItemContent(item, highlighter, escapeHtml) {
      if (highlighter) {
        return highlighter(item.label);
      } else {
        return escapeHtml && this.enableHtmlEscape && item.enableHtmlEscape ? escapeHtml(item.label) : item.label;
      }
    }
  }, {
    key: 'putInDom',
    value: function putInDom() {
      if (this.isInDom) {
        return;
      }
      if (this.domRef) {
        this.domRef.insertAdjacentHTML('beforebegin', this.htmlBuffer);
        this.domRef.parentNode.removeChild(this.domRef);
      } else {
        this.domHolder.insertAdjacentHTML('beforeend', this.htmlBuffer);
      }
      this.dom = this.domHolder.querySelector('#' + this.id);

      this.domMenuPanel = this.domHolder.querySelector('#' + this.domMenuPanelId);

      if (!this.domMenuPanel) {
        if (this.hasChrome) {
          throw new Error('The provided renderMenuPanel function does not render an element with id: ', this.domMenuPanelId);
        }
        this.domMenuPanel = this.dom;
        this.domMenuPanelId = this.id;
      }

      this.isInDom = true;
      this.syncViewWithMode();
      this.syncViewWithZeroState();
      this.syncViewWithDisabledState();
      this.htmlBuffer = '';
    }
  }, {
    key: 'putMenuInDom',
    value: function putMenuInDom(menu) {
      if (!menu) {
        return null;
      }

      var menuDom = (0, _utilsDom_helpersDom_utils.domById)(menu.id);

      if (menuDom) {
        menuDom.insertAdjacentHTML('beforebegin', this.renderMenu(menu));
        menuDom.parentNode.removeChild(menuDom);
        menuDom = (0, _utilsDom_helpersDom_utils.domById)(menu.id);
      } else {
        this.domMenuPanel.insertAdjacentHTML('beforeend', this.renderMenu(menu));
      }
      menu.isInDom = true;

      for (var i = 0, len = menu.length; i < len; i++) {
        menu[i].isInDom = true;
      }

      menuDom = (0, _utilsDom_helpersDom_utils.domById)(menu.id);
      menuDom.style.display = 'none';

      return menuDom;
    }
  }, {
    key: 'postDomInsertionSetup',
    value: function postDomInsertionSetup() {
      var _this2 = this;

      this.onDom(this.domMenuPanel, 'mouseup', function (e) {
        return _this2.onRootMouseUp(e);
      }).onDom(this.domMenuPanel, 'mousemove', function (e) {
        return _this2.onRootMouseMove(e);
      }).onDom(this.domMenuPanel, 'mouseover', function (e) {
        return _this2.onRootMouseOver(e);
      }).onDom(this.domMenuPanel, 'mouseout', function (e) {
        return _this2.onRootMouseOut(e);
      }).onDom(this.dom, 'mouseenter', function (e) {
        return _this2.onRootMouseEnter(e);
      }).onDom(this.dom, 'mouseleave', function (e) {
        return _this2.onRootMouseLeave(e);
      }).onDom(document, 'click', function (e) {
        return _this2.onDocumentClick(e);
      });
    }
  }, {
    key: 'setMenuSizes',
    value: function setMenuSizes(refDom) {
      if (!this.dom) return;

      // position the wrapper below the base;
      refDom = refDom || this.domHolder;
      this.dom.style.width = refDom.offsetWidth + 'px';
    }
  }, {
    key: 'syncViewWithMode',
    value: function syncViewWithMode() {
      if (this.isInDom) {
        (0, _utilsDom_helpersDom_utils.toggleClass)(this.dom, this.clsAutofitMode, this.autofitMode);
      }
    }
  }, {
    key: 'syncViewWithValue',
    value: function syncViewWithValue(withFocus) {
      withFocus = withFocus == null ? true : withFocus;

      if (this.isInDom && this.isVisible) {
        if (this.activeItem) {
          this.showMenu(this.activeItem.parentMenu, withFocus ? this.activeItem : null);
        } else {
          this.showMenu(this.rootItem.menu, withFocus ? this.firstSelectableMenuItem(this.rootItem.menu) : null);
        }
      }
    }

    // -------------------- General Menu Operations -----------------------

  }, {
    key: 'focus',
    value: function focus() {
      if (this.isFocused || this.disabled) {
        return;
      }

      this.isFocused = true;

      (0, _utilsDom_helpersDom_utils.addClass)(this.dom, this.clsFocused);
      this.captureKeyboard();

      this.onFocus();
      this.trigger('focus');
    }
  }, {
    key: 'blur',
    value: function blur() {
      if (!this.isFocused) {
        return;
      }
      this.trigger('beforeBlur');
      this.isFocused = false;
      (0, _utilsDom_helpersDom_utils.removeClass)(this.dom, this.clsFocused);
      this.releaseKeyboard();
      this.blurItem(this.focusedItem);
      this.focusedItem = null;

      this.onBlur();
      this.trigger('blur');
    }
  }, {
    key: 'disable',
    value: function disable() {
      this.setDisableState(true);
    }
  }, {
    key: 'enable',
    value: function enable() {
      this.setDisableState(false);
    }
  }, {
    key: 'setDisableState',
    value: function setDisableState(isDisabled) {
      if (isDisabled === this.disabled) return;

      this.disabled = isDisabled;
      this.syncViewWithDisabledState();
      this.onDisabledChanged();
      this.trigger('disableChanged');
    }
  }, {
    key: 'syncViewWithDisabledState',
    value: function syncViewWithDisabledState() {
      if (this.disabled) {
        this.blur();
      }
      (0, _utilsDom_helpersDom_utils.toggleClass)(this.dom, this.clsDisabled, this.disabled);
    }
  }, {
    key: 'show',
    value: function show(syncWithValue, adjustMenuSize) {
      if (this.isVisible) {
        return;
      }

      syncWithValue = syncWithValue == null ? true : syncWithValue;
      adjustMenuSize = adjustMenuSize == null ? true : adjustMenuSize;

      this.isVisible = true;
      this.isKeyboardNavigation = false;
      adjustMenuSize && this.setMenuSizes();
      this.syncViewWithMode();
      this.dom.style.display = '';
      this.syncViewWithValue(syncWithValue);
      this.onShow();
      this.trigger('show');
    }
  }, {
    key: 'hide',
    value: function hide() {
      if (!this.isVisible) {
        return;
      }
      this.isVisible = false;
      this.dom.style.display = 'none';
      this.blurItem(this.focusedItem);
      this.onHide();
      this.trigger('hide');
    }
  }, {
    key: 'toggle',
    value: function toggle() {
      this.isVisible ? this.hide() : this.show();
    }
  }, {
    key: 'showMenu',
    value: function showMenu(menu, itemToFocus, transition, navMethod) {
      var _this3 = this;

      var menuDom;
      navMethod = navMethod || 'none'; // can be ['keyboard' | 'click' | 'none']
      if (!menu || this.inTransition) return;

      itemToFocus = itemToFocus && itemToFocus.role !== 'root' ? itemToFocus : null;

      this.trigger('beforeShowMenu', { menu: menu });

      if (this.activeMenu === menu) {
        this._resizeToParentWidth((0, _utilsDom_helpersDom_utils.domById)(menu.id));
        itemToFocus && this.focusItem(itemToFocus, true);
      } else {
        menuDom = this.putMenuInDom(menu);
        if (!menuDom) {
          return;
        }
        menuDom.setAttribute('aria-expanded', 'true');

        menuDom.style.display = '';
        this._resizeToParentWidth(menuDom);
        this.forceTrueScrollHeightForMenu(menuDom);

        if (navMethod === 'click') {
          this.positionItemInView(itemToFocus);
        } else if (itemToFocus) {
          this.focusItem(itemToFocus, true);
        }

        (transition || this.transitions.direct).call(this, menu, this.activeMenu, function () {
          return _this3._resizeToParentWidth(menuDom);
        });

        this.activeMenu = menu;
      }
    }

    // Necessary because Chrome does not set the width of a block inner element
    // to the client width of the parent. This happens when the scroll element
    // sizes are tweaked through CSS.
  }, {
    key: '_resizeToParentWidth',
    value: function _resizeToParentWidth(dom) {
      if (!dom || !dom.parentNode) return;
      dom.style.width = dom.parentNode.clientWidth + 'px';
    }
  }, {
    key: 'forceTrueScrollHeightForMenu',
    value: function forceTrueScrollHeightForMenu(menuDom) {
      if (!this.domMenuShim) {
        this.domMenuShim = document.createElement('div');
        this.domMenuShim.style.cssText = 'position:absolute;top:0;left:0;width:1px';
        this.domMenuPanel.insertBefore(this.domMenuShim, this.domMenuPanel.firstChild);
      }
      this.domMenuShim.style.height = menuDom.offsetHeight + 'px';
    }
  }, {
    key: 'hideMenu',
    value: function hideMenu(menu) {
      if (!menu) {
        return;
      }
      var menuDom = (0, _utilsDom_helpersDom_utils.domById)(menu.id);

      if (menuDom) {
        menuDom.setAttribute('aria-expanded', 'false');
        menuDom.style.display = 'none';
      }
    }
  }, {
    key: 'activateItem',
    value: function activateItem(item, source) {
      if (item == null || this.itemIsDisabled(item) || this.disabled) {
        return;
      }

      if (this.inTransition) {
        return;
      }

      this.navigationMode = source || 'mouse';

      if (this.itemHasMenu(item)) {
        this.showMenu(item.menu, this.firstSelectableMenuItem(item.menu), this.getTransition('Left'), source);
      } else if (this.itemIsBackHelper(item)) {
        this.showMenu(item.parentMenu.parentMenu, item.parentMenu.parentItem, this.getTransition('Right'), source);
      } else {
        this.setValue(item.value, source, item);
      }
    }
  }, {
    key: 'focusItem',
    value: function focusItem(item, shouldPositionToItem) {
      if (this.disabled) return;
      if (this.focusedItem) {
        this.blurItem(this.focusedItem);
      }

      if (!item) return;

      var domItem = (0, _utilsDom_helpersDom_utils.domById)(item.id);
      if (!domItem) return;

      this.focusedItem = item;

      (0, _utilsDom_helpersDom_utils.addClass)(domItem, this.clsItemFocused);
      shouldPositionToItem && this.positionItemInView(item);
      this.trigger('itemFocused', this.focusedItem);
    }
  }, {
    key: 'blurItem',
    value: function blurItem(item) {
      if (!item) {
        return;
      }

      var domItem = (0, _utilsDom_helpersDom_utils.domById)(item.id);
      domItem && (0, _utilsDom_helpersDom_utils.removeClass)(domItem, this.clsItemFocused);
    }
  }, {
    key: 'resolveZeroState',
    value: function resolveZeroState() {
      var oldIsZeroState = this.isZeroState;
      this.isZeroState = !(this.rootItem.menu && this.rootItem.menu.length > 0);
      if (oldIsZeroState !== this.isZeroState) {
        this.trigger('zeroStateChanged');
      }
    }
  }, {
    key: 'syncViewWithZeroState',
    value: function syncViewWithZeroState() {
      if (this.dom) {
        (0, _utilsDom_helpersDom_utils.toggleClass)(this.dom, this.clsZeroState, this.isZeroState);
      }
    }
  }, {
    key: 'positionItemInView',
    value: function positionItemInView(item) {
      if (!item) {
        return;
      }

      var domMenuPanel = this.domMenuPanel;
      var domItem = (0, _utilsDom_helpersDom_utils.domById)(item.id);

      if (!domItem) {
        return;
      }

      var itemOffsetTop = domItem.offsetTop;
      var scrollTop = itemOffsetTop - domMenuPanel.offsetHeight / 2 + domItem.offsetHeight / 2;
      domMenuPanel.scrollTop = scrollTop;
    }
  }, {
    key: 'getNaturalCoords',
    value: function getNaturalCoords() {
      if (!this.isInDom) return 0;

      var restrictedHeight = this.dom.offsetHeight;
      this.resetHeight();
      var coords = this.dom.getBoundingClientRect();
      this.setHeight(restrictedHeight);
      return coords;
    }
  }, {
    key: 'setHeight',
    value: function setHeight(height) {
      this.dom.style.height = height + 'px';
    }
  }, {
    key: 'resetHeight',
    value: function resetHeight() {
      this.dom.style.height = '';
      this.domMenuPanel.style.height = '';
    }

    // -------------------- Helper Functions -----------------------

  }, {
    key: '_manageNavigationMode',
    value: function _manageNavigationMode(e) {
      if (this.isKeyboardNavigation && this._lastClientX === e.clientX && this._lastClientY === e.clientY) {} else {
        this.isKeyboardNavigation = false;
      }
      this._lastClientX = e.clientX;
      this._lastClientY = e.clientY;
    }
  }, {
    key: 'resolveItemFromDom',
    value: function resolveItemFromDom(domNode) {
      var wrapper = this.domMenuPanel;

      while (domNode !== wrapper) {
        if (domNode.tagName === 'LI') {
          return this.domToItem(domNode);
        }
        domNode = domNode.parentNode;
      }
      return null;
    }

    // ----------------------   Hooks   -------------------------

  }, {
    key: 'onDataReady',
    value: function onDataReady() {}
  }, {
    key: 'onChange',
    value: function onChange() {}
  }, {
    key: 'onChangeRequest',
    value: function onChangeRequest() {}
  }, {
    key: 'onFocus',
    value: function onFocus() {}
  }, {
    key: 'onBlur',
    value: function onBlur() {}
  }, {
    key: 'onShow',
    value: function onShow() {}
  }, {
    key: 'onHide',
    value: function onHide() {}
  }, {
    key: 'onDisabledChanged',
    value: function onDisabledChanged() {}
  }, {
    key: 'onDestroy',
    value: function onDestroy() {}
  }, {
    key: 'onCueMatch',
    value: function onCueMatch() {}

    // -------------------- Event Handlers -----------------------

  }, {
    key: 'onDocumentClick',
    value: function onDocumentClick(e) {
      if (!this.dom) return;

      if (!this.dom.contains(e.target)) {
        this.blur();
      }
    }
  }, {
    key: 'onRootMouseEnter',
    value: function onRootMouseEnter(e) {
      (0, _utilsDom_helpersDom_utils.addClass)(this.dom, this.clsHover);
    }
  }, {
    key: 'onRootMouseLeave',
    value: function onRootMouseLeave(e) {
      (0, _utilsDom_helpersDom_utils.removeClass)(this.dom, this.clsHover);
    }
  }, {
    key: 'onRootMouseOver',
    value: function onRootMouseOver(e) {
      if (this.isKeyboardNavigation) {
        return;
      }

      var item = this.resolveItemFromDom(e.target);
      item && item.role !== 'uiLabel' && this.focusItem(item);
      // maybe also speculatively prerender the next menu if not rendered
    }
  }, {
    key: 'onRootMouseOut',
    value: function onRootMouseOut(e) {
      if (e.target.tagName === 'LI') {
        var item = this.domToItem(e.target);
        item && item.role !== 'uiLabel' && this.blurItem(item);
      }
      // maybe blur the currently focused item?
    }
  }, {
    key: 'onRootMouseUp',
    value: function onRootMouseUp(e) {
      var item = this.resolveItemFromDom(e.target);
      item && item.role !== 'uiLabel' && this.activateItem(item, 'click');
    }
  }, {
    key: 'onRootMouseMove',
    value: function onRootMouseMove(e) {
      this._manageNavigationMode(e);
    }
  }, {
    key: 'onWindowResize',
    value: function onWindowResize() {
      this.close();
    }

    // -------------------- Menu Transitions -----------------------

    // direction can be ['Left' | 'Right']
  }, {
    key: 'getTransition',
    value: function getTransition(direction) {
      return this.transitionMode === 'direct' ? this.transitions.direct : this.transitions[this.transitionMode + direction];
    }
  }, {
    key: 'captureKeyboard',

    // -------------------- Keyboard Navigation -----------------------

    value: function captureKeyboard() {
      var _this4 = this;

      if (this.isKeyboardCaptured || this.disabled) {
        return;
      }
      this.isKeyboardCaptured = true;
      this.onDom(document, 'keydown', function (e) {
        return _this4.onKeyDown(e);
      });
      if (this.keyboardCue) {
        this.onDom(document, 'keypress', function (e) {
          return _this4.onKeyPress(e);
        });
      }
      this.trigger('keyboardCaptured');
    }
  }, {
    key: 'releaseKeyboard',
    value: function releaseKeyboard() {
      this.isKeyboardCaptured = false;
      this.offDom(document, 'keydown');
      if (this.keyboardCue) {
        this.offDom(document, 'keypress');
      }
      this.trigger('keyboardReleased');
    }
  }, {
    key: 'cueMatch',
    value: function cueMatch(menu, key, from, to) {
      menu = menu || this._getActiveMenu();
      if (!menu) return false;

      from = from || 0;
      to = to || menu.length;

      for (var i = from; i < to; i++) {
        if (menu[i].role === 'uiBackLink' || menu[i].role === 'uiLabel') continue;

        if (menu[i].label.charAt(0).toLowerCase() === key) {
          this.lastIndex = i;
          if (!this.isVisible) {
            this.show(false);
          }

          if (this.keyboardCueAction === 'focus') {
            this.focusItem(menu[i], true);
          } else {
            this.setValue(menu[i].value, 'keyboardCue');
          }
          this.onCueMatch(menu[i]);
          return true;
        }
      }
      return false;
    }
  }, {
    key: '_getActiveMenu',
    value: function _getActiveMenu() {
      return this.activeMenu || this.activeItem && this.activeItem.parentMenu || this.rootItem.menu;
    }
  }, {
    key: 'onKeyPress',
    value: function onKeyPress(e) {
      var key = String.fromCharCode(e.charCode).toLowerCase(),
          activeMenu = this._getActiveMenu();

      if (!activeMenu) return;

      if (e.charCode === 91) return;

      if (this.lastActiveMenu !== activeMenu || this.lastKey !== key) {
        this.lastIndex = -1;
      }

      this.lastActiveMenu = activeMenu;
      this.lastKey = key;

      if (this.cueMatch(activeMenu, key, this.lastIndex + 1, activeMenu.length)) return;
      if (this.cueMatch(activeMenu, key, 0, this.lastIndex + 1)) return;
    }
  }, {
    key: 'onKeyDown',
    value: function onKeyDown(e) {
      if (!this.activeMenu) return;

      switch (e.keyCode) {
        case _utilsCore_utils.keyCodes.HOME:
          this.moveToStart();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.END:
          this.moveToEnd();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.PAGE_DOWN:
          this.pgDown();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.PAGE_UP:
          this.pgUp();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.DOWN:
          this.moveDown();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.UP:
          this.moveUp();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.LEFT:
          this.moveToLeft();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.ENTER:
        case _utilsCore_utils.keyCodes.NUMPAD_ENTER:
        case _utilsCore_utils.keyCodes.RIGHT:
        case _utilsCore_utils.keyCodes.TAB:
          this.activateItem(this.focusedItem, 'keyboard');e.preventDefault();break;
      }

      this.trigger('keyDown', { domEvent: e });
    }
  }, {
    key: 'getItemsPerPage',
    value: function getItemsPerPage() {
      if (!this.activeMenu) return 0;
      if (this.activeMenu.length === 0) return 0;
      var domWrapper = this.domMenuPanel;
      var domFirstItem = this.itemToDom(this.activeMenu[0]);

      return Math.floor(domWrapper.offsetHeight / domFirstItem.offsetHeight);
    }
  }, {
    key: '_moveToItem',
    value: function _moveToItem(offset, outOfRangePosition) {
      this.navigationMode = 'keyboard';
      if (!this.activeMenu) return;

      var itemToFocus;

      if (this.focusedItem) {
        var startIndex = this.getItemIndex(this.focusedItem) + offset;
        if (offset > 0) {
          itemToFocus = this._getNextSelectableItem(this.activeMenu, startIndex);

          if (!itemToFocus && outOfRangePosition === START) {
            itemToFocus = this._getNextSelectableItem(this.activeMenu, 0);
          }
        } else {
          itemToFocus = this._getPrevSelectableItem(this.activeMenu, startIndex);

          if (!itemToFocus && outOfRangePosition === END) {
            itemToFocus = this._getPrevSelectableItem(this.activeMenu, this.activeMenu.length - 1);
          }
        }
      } else {
        itemToFocus = this._getNextSelectableItem(this.activeMenu, 0);
      }

      this.isKeyboardNavigation = true;
      if (itemToFocus) {
        this.focusItem(itemToFocus, true);
      }
    }
  }, {
    key: '_getNextSelectableItem',
    value: function _getNextSelectableItem(menu, from) {
      var item;
      from = from || 0;
      if (!menu) return;

      for (var i = from; i < menu.length; i++) {
        item = menu[i];
        if (this.itemIsSelectable(item)) return item;
      }
    }
  }, {
    key: '_getPrevSelectableItem',
    value: function _getPrevSelectableItem(menu, from) {
      var item;

      for (var i = from; i >= 0; i--) {
        item = menu[i];
        if (this.itemIsSelectable(item)) return item;
      }
    }
  }, {
    key: 'pgDown',
    value: function pgDown() {
      this._moveToItem(this.getItemsPerPage(), this.goToStartAfterReachingEnd ? START : END);
    }
  }, {
    key: 'pgUp',
    value: function pgUp() {
      this._moveToItem(-this.getItemsPerPage(), this.goToEndAfterReachingStart ? END : START);
    }
  }, {
    key: 'moveDown',
    value: function moveDown() {
      this._moveToItem(1, this.goToStartAfterReachingEnd ? START : END);
    }
  }, {
    key: 'moveUp',
    value: function moveUp() {
      this._moveToItem(-1, this.goToEndAfterReachingStart ? END : START);
    }
  }, {
    key: 'moveToStart',
    value: function moveToStart() {
      if (!this.activeMenu) {
        return;
      }
      this.isKeyboardNavigation = true;
      var itemToFocus = this._getNextSelectableItem(this.activeMenu, 0);
      itemToFocus && this.focusItem(itemToFocus, true);
    }
  }, {
    key: 'moveToEnd',
    value: function moveToEnd() {
      if (!this.activeMenu) {
        return;
      }
      this.isKeyboardNavigation = true;
      var itemToFocus = this._getPrevSelectableItem(this.activeMenu, this.activeMenu.length - 1);
      itemToFocus && this.focusItem(itemToFocus, true);
    }
  }, {
    key: 'moveToLeft',
    value: function moveToLeft() {
      if (!this.activeMenu || !this.focusedItem || !this.focusedItem.parentMenu) {
        return;
      }
      this.showMenu(this.focusedItem.parentMenu.parentMenu, this.focusedItem.parentMenu.parentItem, this.getTransition('Right'), 'keyboard');
    }
  }, {
    key: 'defaultOptions',
    get: function get() {
      return defaultOptions;
    }
  }, {
    key: 'transitions',
    get: function get() {
      return _menu_transitions2['default'];
    }
  }]);

  return Menu;
})((0, _utilsCore_utils.mixin)(_utilsMixinsOptions_mixin2['default'], _utilsMixinsObservable_mixin2['default']));

exports['default'] = Menu;
module.exports = exports['default'];
},{"../utils/core_utils":11,"../utils/dom_helpers/dom_utils":20,"../utils/iterators/hash_iterator":24,"../utils/mixins/observable_mixin":27,"../utils/mixins/options_mixin":28,"../utils/parsers/menu_data_parser":31,"./menu_transitions":5}],5:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

var _utilsDom_helpersAnimation = _dereq_('../utils/dom_helpers/animation');

var _utilsDom_helpersAnimation2 = _interopRequireDefault(_utilsDom_helpersAnimation);

exports['default'] = {

  direct: function direct(menuToShow, menuToHide, onComplete) {
    var domMenuToShow = (0, _utilsDom_helpersDom_utils.domById)(menuToShow.id);
    domMenuToShow.style.display = '';
    domMenuToShow.style.left = 0;

    if (menuToShow !== menuToHide && menuToHide) {
      this.hideMenu(menuToHide);
    }
    onComplete && onComplete();
  },

  stackingLeft: function stackingLeft(menuToShow, menuToHide, onComplete) {
    this.transitions._stacking.call(this, menuToShow, menuToHide, onComplete, 'Left');
  },

  stackingRight: function stackingRight(menuToShow, menuToHide, onComplete) {
    this.transitions._stacking.call(this, menuToShow, menuToHide, onComplete, 'Right');
  },

  _stacking: function _stacking(menuToShow, menuToHide, _onComplete, mode) {
    var _this = this;

    if (menuToShow === menuToHide || menuToHide == null) {
      return this.transitions.direct.call(this, menuToShow, menuToHide);
    }

    var menuToShowDom = (0, _utilsDom_helpersDom_utils.domById)(menuToShow.id);
    var menuToHideDom = (0, _utilsDom_helpersDom_utils.domById)(menuToHide.id);

    var minHeight = menuToHideDom.parentNode.clientHeight;
    menuToHideDom.style.minHeight = minHeight + 'px';
    menuToShowDom.style.minHeight = minHeight + 'px';

    if (mode === 'Right') {
      menuToHideDom.style.top = menuToHideDom.parentNode.scrollTop + 'px';
    } else {
      menuToShowDom.style.top = menuToShowDom.parentNode.scrollTop + 'px';
    }

    menuToShowDom.style.zIndex = mode === 'Right' ? 1 : 2;
    menuToShowDom.style.display = '';

    menuToHideDom.style.zIndex = mode === 'Right' ? 2 : 1;
    menuToHideDom.style.display = '';

    var offset = this.domMenuPanel.offsetWidth;

    this.inTransition = true;

    (0, _utilsDom_helpersAnimation2['default'])({
      element: mode === 'Right' ? menuToHideDom : menuToShowDom,
      from: mode === 'Right' ? 0 : -offset,
      to: mode === 'Right' ? -offset : 0,
      property: 'left',
      duration: this.transitionDuration,
      easing: this.transitionEasing,
      onComplete: function onComplete() {
        _this.hideMenu(menuToHide);
        _this.inTransition = false;
        _onComplete && _onComplete();
      }
    });
  },

  slidingLeft: function slidingLeft(menuToShow, menuToHide, onComplete) {
    this.transitions._sliding.call(this, menuToShow, menuToHide, onComplete, 'Left');
  },

  slidingRight: function slidingRight(menuToShow, menuToHide, onComplete) {
    this.transitions._sliding.call(this, menuToShow, menuToHide, onComplete, 'Right');
  },

  _sliding: function _sliding(menuToShow, menuToHide, _onComplete2, mode) {
    var _this2 = this;

    if (menuToShow === menuToHide || menuToHide == null) {
      return this.transitions.direct.call(this, menuToShow, menuToHide);
    }

    var fromToShow = undefined,
        toToShow = undefined,
        fromToHide = undefined,
        toToHide = undefined;
    var offset = this.domMenuPanel.offsetWidth;
    var menuToShowDom = (0, _utilsDom_helpersDom_utils.domById)(menuToShow.id);
    var menuToHideDom = (0, _utilsDom_helpersDom_utils.domById)(menuToHide.id);

    menuToShowDom.style.zIndex = mode === 'Right' ? 1 : 2;
    menuToShowDom.style.display = '';

    menuToHideDom.style.zIndex = mode === 'Right' ? 2 : 1;
    menuToHideDom.style.display = '';

    if (mode === 'Right') {
      fromToShow = -offset;
      toToShow = 0;
      fromToHide = 0;
      toToHide = offset;
    } else {
      fromToShow = offset;
      toToShow = 0;
      fromToHide = 0;
      toToHide = -offset;
    }

    this.inTransition = true;

    (0, _utilsDom_helpersAnimation2['default'])({
      animations: [{ element: menuToShowDom, from: fromToShow, to: toToShow }, { element: menuToHideDom, from: fromToHide, to: toToHide }],
      property: 'left',
      duration: this.transitionDuration,
      easing: this.transitionEasing,
      onComplete: function onComplete() {
        _this2.hideMenu(menuToHide);
        _this2.inTransition = false;
        _onComplete2 && _onComplete2();
      }
    });
  }
};
module.exports = exports['default'];
},{"../utils/dom_helpers/animation":16,"../utils/dom_helpers/dom_utils":20}],6:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

var _utilsMixinsOptions_mixin = _dereq_('../utils/mixins/options_mixin');

var _utilsMixinsOptions_mixin2 = _interopRequireDefault(_utilsMixinsOptions_mixin);

var _utilsMixinsObservable_mixin = _dereq_('../utils/mixins/observable_mixin');

var _utilsMixinsObservable_mixin2 = _interopRequireDefault(_utilsMixinsObservable_mixin);

var _utilsMixinsPositionable_mixin = _dereq_('../utils/mixins/positionable_mixin');

var _utilsMixinsPositionable_mixin2 = _interopRequireDefault(_utilsMixinsPositionable_mixin);

var _utilsMixinsCss_scoping_mixin = _dereq_('../utils/mixins/css_scoping_mixin');

var _utilsMixinsCss_scoping_mixin2 = _interopRequireDefault(_utilsMixinsCss_scoping_mixin);

var _utilsDom_helpersVertical_menu_positioner = _dereq_('../utils/dom_helpers/vertical_menu_positioner');

var _utilsDom_helpersVertical_menu_positioner2 = _interopRequireDefault(_utilsDom_helpersVertical_menu_positioner);

var _utilsDom_helpersDom_position_observer = _dereq_('../utils/dom_helpers/dom_position_observer');

var _utilsDom_helpersDom_position_observer2 = _interopRequireDefault(_utilsDom_helpersDom_position_observer);

var _utilsRenderersHighlighting_renderer = _dereq_('../utils/renderers/highlighting_renderer');

var _utilsRenderersHighlighting_renderer2 = _interopRequireDefault(_utilsRenderersHighlighting_renderer);

var _utilsDom_helpersCss_class_state_machine = _dereq_('../utils/dom_helpers/css_class_state_machine');

var _utilsDom_helpersCss_class_state_machine2 = _interopRequireDefault(_utilsDom_helpersCss_class_state_machine);

var _utilsDatasourcesFiltering_datasource = _dereq_('../utils/datasources/filtering_datasource');

var _utilsDatasourcesFiltering_datasource2 = _interopRequireDefault(_utilsDatasourcesFiltering_datasource);

var _menuMenu = _dereq_('../menu/menu');

var _menuMenu2 = _interopRequireDefault(_menuMenu);

var _utilsDom_helpersDocument_clicks_monitor = _dereq_('../utils/dom_helpers/document_clicks_monitor');

var _utilsDom_helpersDocument_clicks_monitor2 = _interopRequireDefault(_utilsDom_helpersDocument_clicks_monitor);

var defaultOptions = {
  domHolderSelector: null,
  preferredPosition: null, // ['up' | 'down']

  disabled: false,

  clsSearchMenuScope: 'zd-searchmenu',
  clsSearchMenuRoot: 'zd-searchmenu zd-searchmenu-root',
  clsBaseInput: 'zd-searchmenu-base',

  clsDefault: 'zd-state-default',
  clsDisabled: 'zd-state-disabled',
  clsFocused: 'zd-state-focus',
  clsOpen: 'zd-state-open',
  clsHover: 'zd-state-hover',
  clsSearch: 'zd-state-search',

  clsPositionUp: 'zd-state-position-up',

  clsHighlight: 'zd-highlight',

  keyboardCue: false,
  maxSearchResults: 10,
  enableHtmlEscape: true,
  searchField: 'label',
  openOnFocus: false,
  basePlaceholder: '',
  selectFirstOnOpen: false
};

var SearchMenu = (function (_mixin) {
  _inherits(SearchMenu, _mixin);

  function SearchMenu(options) {
    _classCallCheck(this, SearchMenu);

    _get(Object.getPrototypeOf(SearchMenu.prototype), 'constructor', this).call(this, options);

    this.setOptions(options, defaultOptions);

    this.type = 'SearchMenu';

    this.id = (0, _utilsCore_utils.getUniqId)();
    this.baseId = (0, _utilsCore_utils.getUniqId)();

    this.dom = null;
    this.domBase = null;

    this.isFocused = false;
    this.isInDom = false;
    this.isDestroyed = false;

    this.isKeyboardCaptured = false;
    this.areDomEventsSet = false;
    this.isComposingInput = false;

    this.filterWord = '';
    this.value = this.value || '';

    this.typeToClassMapBase = null;
    this.typeToClassMapMenu = null;

    this.container = this.options.container;

    if (this.domHolder || this.domHolderSelector) {
      this.appendTo(this.domHolder || document.querySelector(this.domHolderSelector));
    }
  }

  _createClass(SearchMenu, [{
    key: 'appendTo',
    value: function appendTo(target) {
      var _this = this;

      if (this.isInDom || !target) return;

      this.domHolder = target;

      _menuMenu2['default'].registerInstance(this);

      this.setupHighlighter();

      this.renderItemContentForBase = this.renderItemContentForBase || this.renderItemContentForBaseDefault;
      this.getMenuPositionRefDom = this.options.getMenuPositionRefDom || this.getMenuPositionRefDom;

      this.setupDataSources();

      if (Array.isArray(this.data)) {
        this.data = this._normalizeData(this.data);
        delete this.options.data;
        this.loadData(this.data);
      }

      // serves to position the menu in the window according
      // to available space and positioning requirements
      this.verticalMenuPositioner = new _utilsDom_helpersVertical_menu_positioner2['default']();

      // detects change in coordinates and fires a registered callback
      this.domBasePositionObserver = new _utilsDom_helpersDom_position_observer2['default']();

      this.htmlBuffer = this.render();
      this.buildDomMenuHolder();
      this.buildCssClassStateManager();
      this.putInDom();
      this.initMenu();
      this.postDomInsertionSetup();

      this.setState('display');
      new _utilsDom_helpersDocument_clicks_monitor2['default'](this);

      if (this.selectFirstOnOpen) {
        var selectFirst = function selectFirst(e, data) {
          setTimeout(function () {
            var firstItem = _this.menu.activeMenu[0];
            firstItem && _this.menu.focusItem(firstItem);
          }, 10);
        };
        this.on('open', selectFirst).on('load', selectFirst);
      }
    }
  }, {
    key: 'buildDomMenuHolder',
    value: function buildDomMenuHolder() {
      this.domMenuHolder = document.createElement('div');
      this.domMenuHolder.className = this.clsSearchMenuScope;
      document.body.appendChild(this.domMenuHolder);
    }
  }, {
    key: 'buildCssClassStateManager',
    value: function buildCssClassStateManager() {
      var _this2 = this;

      this.clsStateManager = new _utilsDom_helpersCss_class_state_machine2['default']({
        dom: function dom() {
          return _this2.dom;
        },
        domAux: function domAux() {
          return _this2.domMenuHolder;
        },
        clsZero: this.clsDefault,
        clsStates: [this.clsDisabled, this.clsOpen, this.clsFocused, this.clsHover]
      });
    }
  }, {
    key: 'loadData',
    value: function loadData(data) {
      if (!this.menu) {
        return;
      }
      this.menu.loadData(data);
      this.onDataLoaded(data);
      this.trigger('load');
    }
  }, {
    key: 'onDataLoaded',
    value: function onDataLoaded(data) {
      this.clearMenuSelection();
      if (this.openOnFocus) return;
      data.length ? this.open() : this.close();
    }
  }, {
    key: 'clearMenuSelection',
    value: function clearMenuSelection() {
      this.menu.focusItem(null);
      this.menu.focusedItem = null;
    }
  }, {
    key: 'setupHighlighter',
    value: function setupHighlighter() {
      var _this3 = this;

      var highlightRenderer = new _utilsRenderersHighlighting_renderer2['default']();

      this.highlighter = function (str) {
        var filterWord = _this3.searchDataSource.filterWord || '';

        if (filterWord === '') {
          return _this3.enableHtmlEscape ? (0, _utilsCore_utils.escapeHtml)(str) : str;
        }

        return highlightRenderer.render(str, _this3.searchDataSource.filterWord, _this3.clsHighlight);
      };
    }
  }, {
    key: 'setupDataSources',
    value: function setupDataSources() {
      var _this4 = this;

      this.searchDataSource = this.searchDataSource || new _utilsDatasourcesFiltering_datasource2['default'](this.maxSearchResults, this.searchField);

      this.searchDataSource.onDataReady = function (data) {
        return _this4.loadData(data || []);
      };

      if (this.searchDataSource.on) {
        this.searchDataSource.on('beforeFetch', function () {
          return _this4.syncViewWithSearchState('beforeFetch');
        });
        this.searchDataSource.on('fetch', function () {
          return _this4.syncViewWithSearchState('fetch');
        });
      }
    }
  }, {
    key: 'syncViewWithSearchState',
    value: function syncViewWithSearchState(context) {
      if (!this.dom) return;
      if (context === 'beforeFetch') {
        (0, _utilsDom_helpersDom_utils.addClass)(this.dom, this.clsSearch);
      } else if (context === 'fetch') {
        (0, _utilsDom_helpersDom_utils.removeClass)(this.dom, this.clsSearch);
      }
    }
  }, {
    key: 'initMenu',
    value: function initMenu() {
      var _this5 = this;

      this.options.domHolder = this.domMenuHolder;
      this.options.isVisible = false;
      this.options.domRef = null;
      this.options.proxyValue = null;
      this.options.proxyId = null;
      this.options.disabled = false;
      this.options.container = this;

      if (this.renderItemContentForMenu) {
        this.options.renderItemContent = this.renderItemContentForMenu;
      }

      var menu = this.menu = new _menuMenu2['default'](this.options);

      delete this.options.renderItemContent;

      menu.container = this;
      menu.highlighter = this.highlighter;
      menu.onChange = function (data) {
        return _this5.onMenuChange(data);
      };
      menu.onChangeRequest = function (data) {
        return _this5.onMenuChangeRequest(data);
      };

      var nativeOnKeyDown = menu.onKeyDown;

      menu.onKeyDown = function (e) {
        if (_this5.onMenuKeyDown(e) === false) return;
        if (_this5.isOpen) {
          nativeOnKeyDown.call(menu, e);
        }
      };

      menu.setMenuSizes = function () {
        if (!this.dom) return;
        this.dom.style.width = this.container.getMenuPositionRefDom().offsetWidth + 'px';
      };

      var onMenuShow = function onMenuShow() {
        return _this5.onMenuShow();
      };
      menu.on('dataLoaded', onMenuShow);
      menu.on('show', onMenuShow);
    }
  }, {
    key: 'onMenuShow',
    value: function onMenuShow() {
      if (!this.menu.isVisible) return;
      this.positionMenu(this.menu, this.getMenuPositionRefDom(), this.preferredPosition);
    }
  }, {
    key: 'onMenuChange',
    value: function onMenuChange(data) {
      if (data.source === 'init') {
        return;
      }
      this.setValue(data.value, data.source);
    }
  }, {
    key: 'onMenuChangeRequest',
    value: function onMenuChangeRequest(data) {
      if (data.source === 'click' || data.source === 'keyboard' || data.source === 'keyboardCue') {
        this.setState('display');
      }
    }
  }, {
    key: 'onMenuKeyDown',
    value: function onMenuKeyDown(e) {
      if (e.keyCode === _utilsCore_utils.keyCodes.ESCAPE) {
        this.setState('display');
      }
      if (e.keyCode === _utilsCore_utils.keyCodes.LEFT || e.keyCode === _utilsCore_utils.keyCodes.RIGHT) return false;

      var focusedItem = this.menu.focusedItem;
      var hasValidFocusedItem = focusedItem && this.menu.hashValues[focusedItem.value];

      if (e.keyCode === _utilsCore_utils.keyCodes.TAB && this.isOpen && !hasValidFocusedItem) {
        if (this.onBeforeTabbingAway() !== false) {
          var value = this.getBaseContent();
          if (value && value.length > 0) {
            this.setValue(value, "keyboard");
          }
        }
        this.blur();
        return false;
      }
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.trigger('beforeDestroy');
      this.menu.destroy();
      _menuMenu2['default'].unregisterInstance(this);
      this.off();
      this.destroyUI();
      this.verticalMenuPositioner = null;
      this.domBasePositionObserver.destroy();
      this.domBasePositionObserver = null;
      (0, _utilsCore_utils.cleanObject)(this);
      this.isDestroyed = true;
      this.lastSelectedMenuItem = null;
    }
  }, {
    key: 'destroyUI',
    value: function destroyUI() {
      if (this.isInDom) {
        this.teardownDomEvents();
        this.dom.parentNode.removeChild(this.dom);
        this.domMenuHolder.parentNode.removeChild(this.domMenuHolder);
      }
      this.isInDom = false;
      this.domHolder = null;
      this.domRef = null;
    }
  }, {
    key: 'resetUI',
    value: function resetUI() {
      if (this.isInDom) {
        this.close();
        this.blur();
        (0, _utilsDom_helpersDom_utils.removeClass)(this.dom, this.clsHover);

        this.setBaseContent('');
        this.menu.resetUI();
      }
    }
  }, {
    key: '_normalizeData',
    value: function _normalizeData(data) {
      var newData = [];

      for (var i = 0, len = data.length; i < len; i++) {
        if (data[i] == null) continue;
        if (typeof data[i] === 'object') {
          newData.push(data[i]);
        } else {
          newData.push({ value: data[i], label: data[i] });
        }
      }

      return newData;
    }

    // -------------------- Rendering and DOM Manipulation -----------------------

  }, {
    key: 'render',
    value: function render() {
      var html = '<div id="' + this.id + '" class="' + this.clsSearchMenuRoot + ' ' + this.clsDefault + '">\n        <input id="' + this.baseId + '" class="' + this.clsBaseInput + '"\n          tabindex="0" placeholder="' + (0, _utilsCore_utils.escapeHtml)(this.basePlaceholder) + '">\n        <span class="icon"></span>\n      </div>';
      return html;
    }
  }, {
    key: 'renderItemContentForBaseDefault',
    value: function renderItemContentForBaseDefault(value) {
      return value;
    }
  }, {
    key: 'putInDom',
    value: function putInDom() {
      if (this.isInDom) {
        return;
      }
      if (this.domRef) {
        this.domRef.insertAdjacentHTML('beforebegin', this.htmlBuffer);
        this.domRef.parentNode.removeChild(this.domRef);
      } else {
        this.domHolder.insertAdjacentHTML('beforeend', this.htmlBuffer);
      }
      this.isInDom = true;

      this.dom = this.domHolder.querySelector('#' + this.id);
      this.domBase = this.domHolder.querySelector('#' + this.baseId);
    }
  }, {
    key: 'postDomInsertionSetup',
    value: function postDomInsertionSetup() {
      this.disabled ? this.disable() : this.setupDomEvents();
    }
  }, {
    key: 'setupDomEvents',
    value: function setupDomEvents() {
      var _this6 = this;

      if (!this.isInDom || this.areDomEventsSet) {
        return;
      }

      var onBaseInput = function onBaseInput(e) {
        return _this6.onBaseInput(e);
      };

      this.onDom(this.domBase, 'focus', function (e) {
        return _this6.onBaseFocus(e);
      }).onDom(this.domBase, 'blur', function (e) {
        return _this6.onBaseBlur(e);
      }).onDom(this.domBase, 'input', onBaseInput).onDom(this.domBase, 'compositionstart', function (e) {
        return _this6.onCompositionStart(e);
      }).onDom(this.domBase, 'compositionend', function (e) {
        _this6.onCompositionEnd(e);onBaseInput(e);
      }).onDom(this.dom, 'mouseenter', function (e) {
        return _this6.onRootMouseEnter(e);
      }).onDom(this.dom, 'mouseleave', function (e) {
        return _this6.onRootMouseLeave(e);
      });

      if (_utilsCore_utils.userAgent.ie9) {
        this.onDom(this.domBase, 'keyup', onBaseInput).onDom(this.domBase, 'cut', onBaseInput);
      }

      var onMouseDown = function onMouseDown(e) {
        return _this6.onDocumentMouseDown(e);
      };

      this.on('open', function () {
        return _this6.onDom(document, 'mousedown', onMouseDown);
      });
      this.on('close', function () {
        return _this6.offDom(document, 'mousedown', onMouseDown);
      });

      this.areDomEventsSet = true;
    }
  }, {
    key: 'teardownDomEvents',
    value: function teardownDomEvents() {
      if (!this.isInDom) {
        return;
      }

      this.offDom();
      this.areDomEventsSet = false;
    }
  }, {
    key: 'getBaseContent',
    value: function getBaseContent() {
      return this.domBase.value;
    }
  }, {
    key: 'setBaseContent',
    value: function setBaseContent(content) {
      this.domBase.value = content;
    }
  }, {
    key: 'syncViewWithValue',
    value: function syncViewWithValue() {
      this.setBaseContent(this.renderItemContentForBase(this.value, this.lastSelectedMenuItem));
    }

    // -------------------- General Menu Operations -----------------------

  }, {
    key: 'setValue',
    value: function setValue(value, source) {
      if (this.value === value) {
        return;
      }

      var lastSelectedMenuItem = this.menu.activeItem || this.menu.hashValues[value];

      var eventData = {
        oldValue: this.value,
        value: value,
        source: source,
        userInitiated: source === 'click' || source === 'keyboard' || source === 'keyboardCue',
        menuItem: lastSelectedMenuItem
      };

      if (this.onBeforeChange(eventData) === false) {
        return;
      }

      this.value = value;
      this.lastSelectedMenuItem = lastSelectedMenuItem;

      this.syncViewWithValue();

      this.setState('display');

      this.onChange(eventData);
      this.trigger('change', eventData);
    }
  }, {
    key: 'setState',
    value: function setState(state, source) {
      if (this.state === state) return;
      this.state = state;

      switch (this.state) {
        case 'search':
          this.menu.captureKeyboard();
          break;
        case 'display':
          this.searchDataSource.resetFilter();
          this.syncViewWithValue();
          if (this.openOnFocus) {
            this.menu.captureKeyboard();
            this.loadData([]);
            break;
          }
          this.menu.releaseKeyboard();
          this.searchDataSource.resetFilter();
          this.close();
          break;
        case 'displayUnconditional':
          this.searchDataSource.resetFilter();
          this.syncViewWithValue();
          this.menu.releaseKeyboard();
          this.close();
      }

      this.onStateChange();
    }
  }, {
    key: 'focus',
    value: function focus() {
      (0, _utilsDom_helpersDom_utils.positionDomIntoView)(this.domBase);
      if (this.isFocused) {
        return;
      }

      this.isFocused = true;
      this.setState('search');
      this.setBaseContent('');
      this.clsStateManager.addState(this.clsFocused);
      if (this.openOnFocus) this.open();
      this.domBase.focus();

      _menuMenu2['default'].registerAsActive(this.id);
      this.onFocus();
      this.trigger('focus');
    }
  }, {
    key: 'blur',
    value: function blur() {
      if (!this.isFocused) {
        return;
      }

      this.onBeforeBlur(this.domBase.value);
      this.trigger('beforeBlur');

      this.isFocused = false;
      this.searchDataSource && this.searchDataSource.abort && this.searchDataSource.abort();
      this.setState('displayUnconditional');
      this.clsStateManager.removeState(this.clsFocused);

      this.onBlur();
      this.trigger('blur');
    }
  }, {
    key: 'open',
    value: function open() {
      var _this7 = this;

      if (this.isOpen) {
        return;
      }

      this.isOpen = true;
      this.menu.show(false);
      this.domBase.focus();
      this.focus();
      this.clsStateManager.addState(this.clsOpen);
      this.positionMenu(this.menu, this.getMenuPositionRefDom(), this.preferredPosition);

      if (this.openOnFocus) {
        this.observeDomBasePosition(function () {
          _this7.menu.show();
          _this7.positionMenu(_this7.menu, _this7.getMenuPositionRefDom(), _this7.preferredPosition);
        });
      } else {
        this.observeDomBasePositionOnce(function () {
          return _this7.close();
        });
      }

      this.onOpen();
      this.trigger('open');
    }
  }, {
    key: 'close',
    value: function close() {
      if (!this.isOpen) {
        return;
      }
      this.isOpen = false;
      this.stopObservingDomBasePosition();
      this.setPosition('down');
      this.menu.hide();
      this.loadData([]);
      this.clsStateManager.removeState(this.clsOpen);
      this.onClose();
      this.trigger('close');
    }
  }, {
    key: 'toggle',
    value: function toggle() {
      this.isOpen ? this.close() : this.open();
    }
  }, {
    key: 'disable',
    value: function disable() {
      this.setDisableState(true);
    }
  }, {
    key: 'enable',
    value: function enable() {
      this.setDisableState(false);
    }
  }, {
    key: 'setDisableState',
    value: function setDisableState(isDisabled) {
      this.disabled = isDisabled;

      if (this.disabled) {
        this.blur();
        this.close();
        this.teardownDomEvents();
        this.clsStateManager.addState(this.clsDisabled);
        this.domBase.disabled = true;
      } else {
        this.setupDomEvents();
        this.domBase.disabled = false;
        this.clsStateManager.removeState(this.clsDisabled);
      }
      this.onDisabledChanged();
      this.trigger('disableChanged');
    }
  }, {
    key: 'show',
    value: function show() {
      this.dom.style.display = '';
      this.trigger('show');
    }
  }, {
    key: 'hide',
    value: function hide() {
      this.dom.style.display = 'none';
      this.trigger('hide');
    }
  }, {
    key: 'getMenuPositionRefDom',
    value: function getMenuPositionRefDom() {
      return this.domBase;
    }
  }, {
    key: 'getDomBaseForPositioning',
    value: function getDomBaseForPositioning() {
      return this.domBase;
    }
  }, {
    key: 'getItemByValue',
    value: function getItemByValue(value) {
      return this.menu.getItemByValue(value);
    }

    // ----------------------   Hooks   -------------------------

  }, {
    key: 'onBeforeChange',
    value: function onBeforeChange() {}
  }, {
    key: 'onChange',
    value: function onChange() {}
  }, {
    key: 'onFocus',
    value: function onFocus() {}
  }, {
    key: 'onBeforeBlur',
    value: function onBeforeBlur() {}
  }, {
    key: 'onBlur',
    value: function onBlur() {}
  }, {
    key: 'onOpen',
    value: function onOpen() {}
  }, {
    key: 'onClose',
    value: function onClose() {}
  }, {
    key: 'onDataReady',
    value: function onDataReady() {}
  }, {
    key: 'onDisabledChanged',
    value: function onDisabledChanged() {}
  }, {
    key: 'onDestroy',
    value: function onDestroy() {}
  }, {
    key: 'onBeforeTabbingAway',
    value: function onBeforeTabbingAway() {}

    // -------------------- Event Handlers -----------------------

  }, {
    key: 'onStateChange',
    value: function onStateChange() {}
  }, {
    key: 'onDocumentMouseDown',
    value: function onDocumentMouseDown(e) {
      var _this8 = this;

      if (!this.isInDom || !e.target) return;

      var isInDom = this.dom.contains(e.target) || this.dom === e.target;
      var isInMenu = this.menu.dom.contains(e.target) || this.menu.dom === e.target;

      if (isInDom || isInMenu) return;

      setTimeout(function () {
        if (!_this8.domBase) return;
        if (document.activeElement === _this8.domBase) return;
        _this8.blur();
        _this8.close();
      });
    }
  }, {
    key: 'onCompositionStart',
    value: function onCompositionStart() {
      this.isComposingInput = true;
    }
  }, {
    key: 'onCompositionEnd',
    value: function onCompositionEnd() {
      this.isComposingInput = false;
    }
  }, {
    key: 'onBaseInput',
    value: function onBaseInput(e) {
      if (this.state === 'display') {
        this.setState('search');
      }

      if (this.isComposingInput) return;

      this.searchDataSource.filter(this, this.domBase.value);
      this.trigger('input', e);
    }
  }, {
    key: 'onBaseFocus',
    value: function onBaseFocus() {
      this.focus();
    }
  }, {
    key: 'onBaseBlur',
    value: function onBaseBlur() {
      !this.isOpen && this.blur();
    }
  }, {
    key: 'onRootMouseEnter',
    value: function onRootMouseEnter(e) {
      this.clsStateManager.addState(this.clsHover);
    }
  }, {
    key: 'onRootMouseLeave',
    value: function onRootMouseLeave(e) {
      this.clsStateManager.removeState(this.clsHover);
    }
  }, {
    key: 'onWindowResize',
    value: function onWindowResize() {
      if (this.openOnFocus) {
        this.positionMenu(this.menu, this.getMenuPositionRefDom(), this.preferredPosition);
      } else {
        this.close();
      }
    }
  }]);

  return SearchMenu;
})((0, _utilsCore_utils.mixin)(_utilsMixinsOptions_mixin2['default'], _utilsMixinsObservable_mixin2['default'], _utilsMixinsPositionable_mixin2['default'], _utilsMixinsCss_scoping_mixin2['default']));

exports['default'] = SearchMenu;
module.exports = exports['default'];
},{"../menu/menu":4,"../utils/core_utils":11,"../utils/datasources/filtering_datasource":12,"../utils/dom_helpers/css_class_state_machine":17,"../utils/dom_helpers/document_clicks_monitor":18,"../utils/dom_helpers/dom_position_observer":19,"../utils/dom_helpers/dom_utils":20,"../utils/dom_helpers/vertical_menu_positioner":22,"../utils/mixins/css_scoping_mixin":26,"../utils/mixins/observable_mixin":27,"../utils/mixins/options_mixin":28,"../utils/mixins/positionable_mixin":29,"../utils/renderers/highlighting_renderer":32}],7:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

var _utilsMixinsOptions_mixin = _dereq_('../utils/mixins/options_mixin');

var _utilsMixinsOptions_mixin2 = _interopRequireDefault(_utilsMixinsOptions_mixin);

var _utilsMixinsObservable_mixin = _dereq_('../utils/mixins/observable_mixin');

var _utilsMixinsObservable_mixin2 = _interopRequireDefault(_utilsMixinsObservable_mixin);

var _utilsMixinsPositionable_mixin = _dereq_('../utils/mixins/positionable_mixin');

var _utilsMixinsPositionable_mixin2 = _interopRequireDefault(_utilsMixinsPositionable_mixin);

var _utilsMixinsCss_scoping_mixin = _dereq_('../utils/mixins/css_scoping_mixin');

var _utilsMixinsCss_scoping_mixin2 = _interopRequireDefault(_utilsMixinsCss_scoping_mixin);

var _utilsDom_helpersVertical_menu_positioner = _dereq_('../utils/dom_helpers/vertical_menu_positioner');

var _utilsDom_helpersVertical_menu_positioner2 = _interopRequireDefault(_utilsDom_helpersVertical_menu_positioner);

var _utilsDom_helpersDom_position_observer = _dereq_('../utils/dom_helpers/dom_position_observer');

var _utilsDom_helpersDom_position_observer2 = _interopRequireDefault(_utilsDom_helpersDom_position_observer);

var _utilsDom_helpersCss_class_state_machine = _dereq_('../utils/dom_helpers/css_class_state_machine');

var _utilsDom_helpersCss_class_state_machine2 = _interopRequireDefault(_utilsDom_helpersCss_class_state_machine);

var _menuMenu = _dereq_('../menu/menu');

var _menuMenu2 = _interopRequireDefault(_menuMenu);

var _utilsDom_helpersDocument_clicks_monitor = _dereq_('../utils/dom_helpers/document_clicks_monitor');

var _utilsDom_helpersDocument_clicks_monitor2 = _interopRequireDefault(_utilsDom_helpersDocument_clicks_monitor);

var defaultOptions = {
  defaultValue: '',
  defaultValueLabel: '-',
  domHolderSelector: null,
  preferredPosition: null, // ['up' | 'down']
  disabled: false,

  clsSelectMenuScope: 'zd-selectmenu',
  clsSelectMenuRoot: 'zd-selectmenu zd-selectmenu-root',
  clsBaseButton: 'zd-selectmenu-base',
  clsBaseContent: 'zd-selectmenu-base-content',
  clsBaseArrow: 'zd-selectmenu-base-arrow zd-icon-arrow-down',

  clsDefault: 'zd-state-default',
  clsDisabled: 'zd-state-disabled',
  clsFocused: 'zd-state-focus',
  clsOpen: 'zd-state-open',
  clsHover: 'zd-state-hover',

  clsPositionUp: 'zd-state-position-up',

  keyboardCue: true,
  enableHtmlEscape: true
};

var SelectMenu = (function (_mixin) {
  _inherits(SelectMenu, _mixin);

  function SelectMenu(options) {
    _classCallCheck(this, SelectMenu);

    _get(Object.getPrototypeOf(SelectMenu.prototype), 'constructor', this).call(this, options);

    this.type = 'SelectMenu';

    this.setOptions(options, defaultOptions);

    this.id = (0, _utilsCore_utils.getUniqId)();
    this.baseId = (0, _utilsCore_utils.getUniqId)();
    this.baseContentId = (0, _utilsCore_utils.getUniqId)();

    this.hasProxy = this.options.proxyName || this.options.proxyId;
    this.proxyName = this.options.proxyName;
    this.proxyId = this.options.proxyId || (0, _utilsCore_utils.getUniqId)();

    this.dom = null;
    this.domBase = null;
    this.domBaseContent = null;
    this.domProxy = null;
    this.domMenuHolder = null;

    this.isFocused = false;
    this.isInDom = false;
    this.isDestroyed = false;
    this.isKeyboardCaptured = false;
    this.areDomEventsSet = false;
    this.container = this.options.container;

    if (this.domHolder || this.domHolderSelector) {
      this.appendTo(this.domHolder || document.querySelector(this.domHolderSelector));
    }
  }

  _createClass(SelectMenu, [{
    key: 'appendTo',
    value: function appendTo(target) {
      if (this.isInDom || !target) return;

      this.domHolder = target;

      this.renderItemContentForBase = this.renderItemContentForBase || this.defaultRenderItemContentForBase;

      // serves to position the menu in the window according
      // to available space and positioning requirements
      this.verticalMenuPositioner = new _utilsDom_helpersVertical_menu_positioner2['default']();

      // detects change in coordinates and fires a registered callback
      this.domBasePositionObserver = new _utilsDom_helpersDom_position_observer2['default']();

      _menuMenu2['default'].registerInstance(this);

      this.htmlBuffer = this.render();
      this.buildDomMenuHolder();
      this.buildCssClassStateManager();
      this.putInDom();
      this.initMenu();
      this.postDomInsertionSetup();
      new _utilsDom_helpersDocument_clicks_monitor2['default'](this);
    }
  }, {
    key: 'loadData',
    value: function loadData(data) {
      if (!this.menu) {
        return;
      }
      this.menu.loadData(data);
      this.onDataReady();
    }
  }, {
    key: 'getItemByValue',
    value: function getItemByValue(value) {
      return this.menu.getItemByValue(value);
    }
  }, {
    key: 'initMenu',
    value: function initMenu() {
      var _this = this;

      this.options.domHolder = this.domMenuHolder;
      this.options.isVisible = false;
      this.options.keyboardCue = this.keyboardCue;
      this.options.keyboardCueAction = 'focus';
      this.options.domRef = null;
      this.options.proxyValue = null;
      this.options.proxyId = null;
      this.options.disabled = false;

      var menu = this.menu = new _menuMenu2['default'](this.options);
      menu.container = this;

      menu.onChange = function (data) {
        return _this.onMenuChange(data);
      };
      menu.onChangeRequest = function (data) {
        return _this.onMenuChangeRequest(data);
      };
      menu.onCueMatch = function (item) {
        return _this.onMenuCueMatch(item);
      };

      var nativeOnKeyDown = menu.onKeyDown;

      menu.onKeyDown = function (e) {
        var eventHandlingResult = _this.onMenuKeyDown(e);
        if (_this.isOpen && eventHandlingResult !== false) {
          nativeOnKeyDown.call(menu, e);
        }
      };

      menu.setMenuSizes = function () {
        if (!menu.dom) return;
        menu.dom.style.width = _this.domBase.offsetWidth + 'px';
      };

      menu.onDataReady = function () {
        _this.value = _menuMenu2['default'].defaultOptions.defaultValue;
      };
    }
  }, {
    key: 'onMenuChange',
    value: function onMenuChange(data) {
      this.setValue(data.value, data.source);
    }
  }, {
    key: 'onMenuChangeRequest',
    value: function onMenuChangeRequest(data) {
      if (data.source === 'click' || data.source === 'keyboard') {
        this.close();
      }
      this.onChangeRequest(data);
    }
  }, {
    key: 'onMenuCueMatch',
    value: function onMenuCueMatch(item) {
      if (!this.isOpen) {
        this.open(false);
      }
    }
  }, {
    key: 'onMenuKeyDown',
    value: function onMenuKeyDown(e) {
      switch (e.keyCode) {
        case _utilsCore_utils.keyCodes.HOME:
        case _utilsCore_utils.keyCodes.END:
        case _utilsCore_utils.keyCodes.PAGE_DOWN:
        case _utilsCore_utils.keyCodes.PAGE_UP:
        case _utilsCore_utils.keyCodes.DOWN:
        case _utilsCore_utils.keyCodes.UP:
        case _utilsCore_utils.keyCodes.LEFT:
        case _utilsCore_utils.keyCodes.ENTER:
        case _utilsCore_utils.keyCodes.NUMPAD_ENTER:
        case _utilsCore_utils.keyCodes.RIGHT:
          if (!this.isOpen) {
            e.preventDefault();
            this.open();
            return false; // stops keyboard event handling by this.menu
          }
          break;
        case _utilsCore_utils.keyCodes.TAB:
          if (this.isOpen && !this.menu.focusedItem) {
            e.preventDefault();
            this.close();
            return;
          }
          break;
        case _utilsCore_utils.keyCodes.ESCAPE:
          this.close();
          break;
      }
    }
  }, {
    key: 'setValue',
    value: function setValue(value, source) {
      if (this.value === value) return;
      if (this.menu.setValue(value) === false) return;

      var oldValue = this.value;
      this.value = this.menu.value;

      this.setBaseContent();
      this.syncProxy();

      var eventData = {
        oldValue: oldValue,
        value: this.value,
        source: source,
        userInitiated: source === 'click' || source === 'keyboard' || source === 'keyboardCue'
      };
      this.onChange(eventData);
      this.trigger('change', eventData);
    }
  }, {
    key: 'buildDomMenuHolder',
    value: function buildDomMenuHolder() {
      this.domMenuHolder = document.createElement('div');
      this.domMenuHolder.className = this.clsSelectMenuScope;
      document.body.appendChild(this.domMenuHolder);
    }
  }, {
    key: 'buildCssClassStateManager',
    value: function buildCssClassStateManager() {
      var _this2 = this;

      this.clsStateManager = new _utilsDom_helpersCss_class_state_machine2['default']({
        dom: function dom() {
          return _this2.dom;
        },
        domAux: function domAux() {
          return _this2.domMenuHolder;
        },
        clsZero: this.clsDefault,
        clsStates: [this.clsDisabled, this.clsOpen, this.clsFocused, this.clsHover]
      });
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.trigger('beforeDestroy');
      _menuMenu2['default'].unregisterInstance(this);
      this.menu.destroy();
      this.off();
      this.destroyUI();
      this.verticalMenuPositioner = null;
      this.domBasePositionObserver.destroy();
      this.domBasePositionObserver = null;
      (0, _utilsCore_utils.cleanObject)(this);
      this.isDestroyed = true;
    }
  }, {
    key: 'destroyUI',
    value: function destroyUI() {
      if (this.isInDom) {
        this.teardownDomEvents();
        this.dom.parentNode.removeChild(this.dom);
        this.domMenuHolder.parentNode.removeChild(this.domMenuHolder);
      }
      this.isInDom = false;
      this.domHolder = null;
      this.dom = null;
      this.domBase = null;
      this.domBaseContent = null;
      this.domMenuHolder = null;
      this.domRef = null;
      this.domProxy = null;
    }
  }, {
    key: 'resetUI',
    value: function resetUI() {
      if (this.isInDom) {
        this.close();
        this.blur();
        (0, _utilsDom_helpersDom_utils.removeClass)(this.dom, this.clsHover);
        this.domBaseContent.innerHTML = '';
        this.menu.resetUI();
      }
    }
  }, {
    key: 'syncViewWithValue',
    value: function syncViewWithValue() {
      this.setBaseContent();
    }

    // -------------------- Rendering and DOM Manipulation -----------------------

  }, {
    key: 'render',
    value: function render() {
      var html = '<div id="' + this.id + '" class="' + this.clsSelectMenuRoot + ' ' + this.clsDefault + '">\n        ' + (this.hasProxy ? '<input type="hidden" name="' + this.proxyName + '" id="' + this.proxyId + '">' : '') + '\n         <button id="' + this.baseId + '" class="' + this.clsBaseButton + '" role="button" tabindex="0" type="button">\n          <span class="' + this.clsBaseArrow + '"></span>\n          <span id="' + this.baseContentId + '" class="' + this.clsBaseContent + '"></span>\n         </button>\n       </div>';
      return html;
    }
  }, {
    key: 'defaultRenderItemContentForBase',
    value: function defaultRenderItemContentForBase(item) {
      return this.menu.getDisplayValue();
    }
  }, {
    key: 'putInDom',
    value: function putInDom() {
      if (this.isInDom) {
        return;
      }
      if (this.domRef) {
        this.domRef.insertAdjacentHTML('beforebegin', this.htmlBuffer);
        this.domRef.parentNode.removeChild(this.domRef);
      } else {
        this.domHolder.insertAdjacentHTML('beforeend', this.htmlBuffer);
      }

      this.isInDom = true;

      this.dom = this.domHolder.querySelector('#' + this.id);
      this.domBase = this.domHolder.querySelector('#' + this.baseId);
      this.domBaseContent = this.domHolder.querySelector('#' + this.baseContentId);
      this.domProxy = this.domHolder.querySelector('#' + this.proxyId);
    }
  }, {
    key: 'postDomInsertionSetup',
    value: function postDomInsertionSetup() {
      this.syncViewWithValue();
      this.syncProxy();
      this.disabled ? this.disable() : this.setupDomEvents();
    }
  }, {
    key: 'setupDomEvents',
    value: function setupDomEvents() {
      var _this3 = this;

      if (!this.isInDom || this.areDomEventsSet || this.disabled) {
        return;
      }

      this.onDom(this.domBase, 'click', function (e) {
        return _this3.onBaseClick(e);
      }).onDom(this.domBase, 'mousedown', function (e) {
        return _this3.onBaseMouseDown(e);
      }).onDom(this.domBase, 'focus', function (e) {
        return _this3.onBaseFocus(e);
      }).onDom(this.domBase, 'blur', function (e) {
        return _this3.onBaseBlur(e);
      }).onDom(this.dom, 'mouseenter', function (e) {
        return _this3.onRootMouseEnter(e);
      }).onDom(this.dom, 'mouseleave', function (e) {
        return _this3.onRootMouseLeave(e);
      });

      var onMouseDown = function onMouseDown(e) {
        return _this3.onDocumentMouseDown(e);
      };

      this.on('open', function () {
        return _this3.onDom(document, 'mousedown', onMouseDown, true);
      });
      this.on('close', function () {
        return _this3.offDom(document, 'mousedown', onMouseDown, true);
      });

      this.areDomEventsSet = true;
    }
  }, {
    key: 'teardownDomEvents',
    value: function teardownDomEvents() {
      if (!this.isInDom) {
        return;
      }

      this.offDom();
      this.areDomEventsSet = false;
    }
  }, {
    key: 'setBaseContent',
    value: function setBaseContent() {
      var item = this.menu.hashValues[this.value];
      var html = this.renderItemContentForBase(item || { label: this.defaultValueLabel });
      this.domBaseContent.innerHTML = html;
    }
  }, {
    key: 'syncProxy',
    value: function syncProxy() {
      this.hasProxy && (this.domProxy.value = this.value);
    }

    // -------------------- General Menu Operations -----------------------

  }, {
    key: 'focus',
    value: function focus() {
      var _this4 = this;

      if (this.isFocused) {
        return;
      }

      this.isFocused = true;
      this.clsStateManager.addState(this.clsFocused);

      setTimeout(function () {
        return _this4.isFocused && _this4.menu.captureKeyboard();
      });

      _menuMenu2['default'].registerAsActive(this.id);
      this.onFocus();
      this.trigger('focus');
    }
  }, {
    key: 'blur',
    value: function blur() {
      if (!this.isFocused) {
        return;
      }

      this.isFocused = false;
      this.clsStateManager.removeState(this.clsFocused);
      this.menu.releaseKeyboard();
      this.onBlur();
      this.trigger('blur');
    }
  }, {
    key: 'open',
    value: function open(syncWithValue) {
      var _this5 = this;

      if (this.isOpen) {
        return;
      }
      this.isOpen = true;

      this.menu.show(syncWithValue, true);
      this.clsStateManager.addState(this.clsOpen);
      this.positionMenu(this.menu, this.domBase, this.preferredPosition);
      this.isFocused && this.menu.captureKeyboard();
      this.observeDomBasePositionOnce(function () {
        return _this5.close();
      });

      this.focus();

      this.onOpen();
      this.trigger('open');
    }
  }, {
    key: 'close',
    value: function close() {
      if (!this.isOpen) {
        return;
      }
      this.isOpen = false;
      this.stopObservingDomBasePosition();
      this.setPosition('down');

      this.menu.hide();
      this.clsStateManager.removeState(this.clsOpen);
      this.onClose();
      this.trigger('close');
    }
  }, {
    key: 'toggle',
    value: function toggle() {
      this.isOpen ? this.close() : this.open(true);
    }
  }, {
    key: 'disable',
    value: function disable() {
      this.setDisableState(true);
    }
  }, {
    key: 'enable',
    value: function enable() {
      this.setDisableState(false);
    }
  }, {
    key: 'setDisableState',
    value: function setDisableState(isDisabled) {
      this.disabled = isDisabled;
      if (!this.isInDom) {
        return;
      }

      if (this.disabled) {
        this.close();
        this.blur();
        this.teardownDomEvents();
        this.clsStateManager.addState(this.clsDisabled);
        this.domBase.disabled = true;
        if (this.hasProxy) {
          this.domProxy.disabled = true;
        }
      } else {
        this.setupDomEvents();
        this.domBase.disabled = false;
        if (this.hasProxy) {
          this.domProxy.disabled = false;
        }
        this.clsStateManager.removeState(this.clsDisabled);
      }
      this.onDisabledChanged();
      this.trigger('disableChanged');
    }
  }, {
    key: 'show',
    value: function show() {
      this.dom.style.display = '';
      this.trigger('show');
    }
  }, {
    key: 'hide',
    value: function hide() {
      this.dom.style.display = 'none';
      this.trigger('hide');
    }
  }, {
    key: 'getDomBaseForPositioning',
    value: function getDomBaseForPositioning() {
      return this.domBase;
    }

    // ----------------------   Hooks   -------------------------

  }, {
    key: 'onDataReady',
    value: function onDataReady() {}
  }, {
    key: 'onChange',
    value: function onChange() {}
  }, {
    key: 'onChangeRequest',
    value: function onChangeRequest() {}
  }, {
    key: 'onOpen',
    value: function onOpen() {}
  }, {
    key: 'onClose',
    value: function onClose() {}
  }, {
    key: 'onFocus',
    value: function onFocus() {}
  }, {
    key: 'onBlur',
    value: function onBlur() {}
  }, {
    key: 'onDisabledChanged',
    value: function onDisabledChanged() {}
  }, {
    key: 'onDestroy',
    value: function onDestroy() {}

    // -------------------- Event Handlers -----------------------

  }, {
    key: 'onDocumentMouseDown',
    value: function onDocumentMouseDown(e) {
      var _this6 = this;

      if (!this.isInDom || !e.target) return;

      var isInDom = this.dom.contains(e.target) || this.dom === e.target;
      var isInMenu = this.menu.dom.contains(e.target) || this.menu.dom === e.target;

      if (isInDom || isInMenu) {
        setTimeout(function () {
          return _this6.domBase.focus();
        });
      } else {
        this.blur();
        this.close();
      }
    }
  }, {
    key: 'onBaseClick',
    value: function onBaseClick(e) {}
  }, {
    key: 'onBaseMouseDown',
    value: function onBaseMouseDown(e) {
      var _this7 = this;

      (0, _utilsDom_helpersDom_utils.positionDomIntoView)(this.domBase);
      this.menu.syncViewWithValue();
      this.toggle();
      setTimeout(function () {
        return _this7.domBase.focus();
      });
    }
  }, {
    key: 'onBaseFocus',
    value: function onBaseFocus() {
      this.focus();
    }
  }, {
    key: 'onBaseBlur',
    value: function onBaseBlur() {
      !this.isOpen && this.blur();
    }
  }, {
    key: 'onRootMouseEnter',
    value: function onRootMouseEnter(e) {
      this.clsStateManager.addState(this.clsHover);
    }
  }, {
    key: 'onRootMouseLeave',
    value: function onRootMouseLeave(e) {
      this.clsStateManager.removeState(this.clsHover);
    }
  }]);

  return SelectMenu;
})((0, _utilsCore_utils.mixin)(_utilsMixinsOptions_mixin2['default'], _utilsMixinsObservable_mixin2['default'], _utilsMixinsPositionable_mixin2['default'], _utilsMixinsCss_scoping_mixin2['default']));

exports['default'] = SelectMenu;
module.exports = exports['default'];
},{"../menu/menu":4,"../utils/core_utils":11,"../utils/dom_helpers/css_class_state_machine":17,"../utils/dom_helpers/document_clicks_monitor":18,"../utils/dom_helpers/dom_position_observer":19,"../utils/dom_helpers/dom_utils":20,"../utils/dom_helpers/vertical_menu_positioner":22,"../utils/mixins/css_scoping_mixin":26,"../utils/mixins/observable_mixin":27,"../utils/mixins/options_mixin":28,"../utils/mixins/positionable_mixin":29}],8:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _search_menuSearch_menu = _dereq_('../search_menu/search_menu');

var _search_menuSearch_menu2 = _interopRequireDefault(_search_menuSearch_menu);

var defaultOptions = {
  baseZeroStatePlaceholder: '',
  basePlaceholder: '',
  delimiters: [' ', ',']
};

var SearchTagEditor = (function (_SearchMenu) {
  _inherits(SearchTagEditor, _SearchMenu);

  function SearchTagEditor() {
    _classCallCheck(this, SearchTagEditor);

    _get(Object.getPrototypeOf(SearchTagEditor.prototype), 'constructor', this).apply(this, arguments);
  }

  _createClass(SearchTagEditor, [{
    key: 'appendTo',
    value: function appendTo(target) {
      var _this = this;

      _get(Object.getPrototypeOf(SearchTagEditor.prototype), 'appendTo', this).call(this, target);

      this.onDom(this.domBase, 'paste', function (e) {
        return _this.onPaste(e);
      });
      this.setOptions(this.options, defaultOptions);
      this.resolvePlaceholder();
    }
  }, {
    key: 'resolvePlaceholder',
    value: function resolvePlaceholder() {
      var isZeroState = this.container.isZeroState;
      var placeholder = isZeroState ? this.baseZeroStatePlaceholder : this.basePlaceholder;
      this.domBase.setAttribute('placeholder', placeholder);
    }
  }, {
    key: 'clearEditor',
    value: function clearEditor() {
      this.domBase.value = '';
      !this.openOnFocus && this.close();
    }
  }, {
    key: 'getEditorValue',
    value: function getEditorValue() {
      return this.domBase.value;
    }
  }, {
    key: 'getEditorContext',
    value: function getEditorContext() {
      return this.menu.activeItem;
    }
  }, {
    key: 'setEditorValue',
    value: function setEditorValue(value) {
      return this.setValue(value);
    }
  }, {
    key: 'onBeforeChange',
    value: function onBeforeChange(e) {
      var _this2 = this;

      setTimeout(function () {
        _this2.trigger('tagEntered', {
          value: e.value,
          context: e.menuItem
        });
      });
      return false;
    }
  }, {
    key: 'getMenuPositionRefDom',
    value: function getMenuPositionRefDom() {
      return this.container.dom;
    }
  }, {
    key: 'captureKeyboard',
    value: function captureKeyboard() {
      var _this3 = this;

      if (this.isKeyboardCaptured) {
        return;
      }
      this.isKeyboardCaptured = true;
      this.onDom(document, 'keydown', function (e) {
        return _this3.onKeyDown(e);
      });
      this.onDom(document, 'keypress', function (e) {
        return _this3.onKeyPress(e);
      });
    }
  }, {
    key: 'releaseKeyboard',
    value: function releaseKeyboard() {
      this.isKeyboardCaptured = false;
      this.offDom(document, 'keydown');
      this.offDom(document, 'keypress');
    }
  }, {
    key: 'onKeyPress',
    value: function onKeyPress(e) {
      if (this.delimiters.indexOf(String.fromCharCode(e.which)) > -1) {
        e.preventDefault();
        this.trigger('tagEntered', { value: this.getEditorValue() });
      }
    }
  }, {
    key: 'onPaste',
    value: function onPaste(e) {
      var _this4 = this;

      var re = new RegExp(this.delimiters.map(function (d) {
        return '\\' + d;
      }).join('|'), "gi");
      setTimeout(function () {
        var value = _this4.getEditorValue();
        var parts = value.split(re);
        if (parts.length > 1) {
          parts.filter(function (part) {
            return Boolean(part);
          }).forEach(function (part) {
            return _this4.trigger('tagEntered', { value: part.trim() });
          });
          _this4.domBase.prop('value', '');
        }
      });
    }
  }, {
    key: 'onKeyDown',
    value: function onKeyDown(e) {
      if (this.onBeforeKeyDown && this.onBeforeKeyDown(e)) return;

      switch (e.keyCode) {
        case _utilsCore_utils.keyCodes.LEFT:
        case _utilsCore_utils.keyCodes.BACKSPACE:
          this.getEditorValue().length === 0 && this.trigger('leftExit');break;
        case _utilsCore_utils.keyCodes.ENTER:
        case _utilsCore_utils.keyCodes.NUMPAD_ENTER:
          e.preventDefault();
          this._tryToTriggerTagEntered();
          break;
        case _utilsCore_utils.keyCodes.TAB:
          if (this.getEditorValue().length) {
            e.preventDefault();
            this._tryToTriggerTagEntered();
          }
          break;
        case _utilsCore_utils.keyCodes.ESCAPE:
          this.clearEditor();break;
      }
    }
  }, {
    key: '_tryToTriggerTagEntered',
    value: function _tryToTriggerTagEntered() {
      if (this.isOpen && this.menu.focusedItem) return;
      this.trigger('tagEntered', { value: this.getEditorValue() });
    }
  }, {
    key: 'onBaseInput',
    value: function onBaseInput(e) {
      _get(Object.getPrototypeOf(SearchTagEditor.prototype), 'onBaseInput', this).call(this, e);
      var code = e.keyCode;

      // these checks are necessary because of IE9. It has a buggy oninput event
      // and the system uses onkeyup to compensate. Here all relevant
      // non-alphanumeric input coming from onkeyup is filtered out.

      if (code === _utilsCore_utils.keyCodes.BACKSPACE || code === _utilsCore_utils.keyCodes.DELETE || code === _utilsCore_utils.keyCodes.LEFT || code === _utilsCore_utils.keyCodes.RIGHT || code === _utilsCore_utils.keyCodes.UP || code === _utilsCore_utils.keyCodes.DOWN || code === _utilsCore_utils.keyCodes.PAGE_DOWN || code === _utilsCore_utils.keyCodes.PAGE_UP) return;

      this.trigger('regularInput', e);
    }
  }]);

  return SearchTagEditor;
})(_search_menuSearch_menu2['default']);

exports['default'] = SearchTagEditor;
module.exports = exports['default'];
},{"../search_menu/search_menu":6,"../utils/core_utils":11}],9:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

var _utilsMixinsOptions_mixin = _dereq_('../utils/mixins/options_mixin');

var _utilsMixinsOptions_mixin2 = _interopRequireDefault(_utilsMixinsOptions_mixin);

var _utilsMixinsObservable_mixin = _dereq_('../utils/mixins/observable_mixin');

var _utilsMixinsObservable_mixin2 = _interopRequireDefault(_utilsMixinsObservable_mixin);

var defaultOptions = {
  basePlaceholder: '',
  baseZeroStatePlaceholder: '',
  delimiters: [' ', ',']
};

var SimpleTagEditor = (function (_mixin) {
  _inherits(SimpleTagEditor, _mixin);

  function SimpleTagEditor(options) {
    _classCallCheck(this, SimpleTagEditor);

    _get(Object.getPrototypeOf(SimpleTagEditor.prototype), 'constructor', this).call(this, options);

    this.id = (0, _utilsCore_utils.getUniqId)();
    this.domHolder = options.domHolder || document.querySelector(options.domHolderSelector);
    this.setOptions(options, defaultOptions);
    this.initUI();
  }

  _createClass(SimpleTagEditor, [{
    key: 'clearEditor',
    value: function clearEditor() {
      this.dom.value = '';
    }
  }, {
    key: 'getEditorValue',
    value: function getEditorValue() {
      return this.dom.value.trim();
    }
  }, {
    key: 'setEditorValue',
    value: function setEditorValue(value) {
      this.dom.value = value;
    }
  }, {
    key: 'render',
    value: function render() {
      return '<input id="' + this.id + '" class="zd-tag-editor" type="text">';
    }
  }, {
    key: 'initUI',
    value: function initUI() {
      var _this = this;

      this.domHolder.insertAdjacentHTML('beforeend', this.render());
      this.dom = (0, _utilsDom_helpersDom_utils.domById)(this.id);

      var onInput = function onInput(e) {
        return _this.onInput(e);
      };

      this.onDom(this.dom, 'focusin', function (e) {
        return _this.onDomFocus(e);
      }).onDom(this.dom, 'focusout', function (e) {
        return _this.onDomBlur(e);
      }).onDom(this.dom, 'paste', function (e) {
        return _this.onPaste(e);
      }).onDom(this.dom, 'input', onInput);

      if (_utilsCore_utils.userAgent.ie9) {
        this.onDom(this.dom, 'keyup', onInput).onDom(this.dom, 'cut', onInput);
      }

      this.resolvePlaceholder();
    }
  }, {
    key: 'onDomFocus',
    value: function onDomFocus(e) {
      this.focus(e);
    }
  }, {
    key: 'onDomBlur',
    value: function onDomBlur(e) {
      this.blur(e);
    }
  }, {
    key: 'resolvePlaceholder',
    value: function resolvePlaceholder() {
      var isZeroState = this.container.isZeroState;
      var placeholder = isZeroState ? this.baseZeroStatePlaceholder : this.basePlaceholder;
      this.dom.setAttribute('placeholder', placeholder);
    }
  }, {
    key: 'focus',
    value: function focus(e) {
      this.isFocused = true;
      if (document.activeElement !== this.dom) {
        this.dom.focus();
      }
      this.captureKeyboard();
      this.trigger('focus');
    }
  }, {
    key: 'blur',
    value: function blur(e) {
      this.trigger('beforeBlur');
      this.isFocused = false;
      if (this.container.isZeroState) return;
      this.trigger('blur');
    }
  }, {
    key: 'captureKeyboard',
    value: function captureKeyboard() {
      var _this2 = this;

      if (this.isKeyboardCaptured) {
        return;
      }
      this.isKeyboardCaptured = true;
      this.onDom(document, 'keydown', function (e) {
        return _this2.onKeyDown(e);
      }).onDom(document, 'keypress', function (e) {
        return _this2.onKeyPress(e);
      });
    }
  }, {
    key: 'releaseKeyboard',
    value: function releaseKeyboard() {
      this.isKeyboardCaptured = false;
      this.offDom(document, 'keydown').offDom(document, 'keypress');
    }
  }, {
    key: 'onKeyPress',
    value: function onKeyPress(e) {
      if (this.delimiters.indexOf(String.fromCharCode(e.which)) > -1) {
        e.preventDefault();
        this.trigger('tagEntered', { value: this.getEditorValue() });
      }
    }
  }, {
    key: 'onPaste',
    value: function onPaste(e) {
      var _this3 = this;

      var re = new RegExp(this.delimiters.map(function (d) {
        return '\\' + d;
      }).join('|'), "gi");

      setTimeout(function () {
        var parts = _this3.getEditorValue().split(re);
        if (parts.length > 1) {
          parts.filter(function (part) {
            return Boolean(part);
          }).forEach(function (part) {
            return _this3.trigger('tagEntered', { value: part.trim() });
          });
          _this3.clearEditor();
        }
      });
    }
  }, {
    key: 'onKeyDown',
    value: function onKeyDown(e) {
      if (this.onBeforeKeyDown && this.onBeforeKeyDown(e)) return;

      switch (e.keyCode) {
        case _utilsCore_utils.keyCodes.LEFT:
        case _utilsCore_utils.keyCodes.BACKSPACE:
          this.getEditorValue().length === 0 && this.trigger('leftExit');break;
        case _utilsCore_utils.keyCodes.ESCAPE:
          this.clearEditor();break;
        case _utilsCore_utils.keyCodes.ENTER:
        case _utilsCore_utils.keyCodes.NUMPAD_ENTER:
          e.preventDefault();
          this.trigger('tagEntered', { value: this.getEditorValue() });
          break;
        case _utilsCore_utils.keyCodes.TAB:
          if (this.getEditorValue().length) {
            e.preventDefault();
            this.trigger('tagEntered', { value: this.getEditorValue() });
          }
          break;
      }

      this.trigger('keyDown', { domEvent: e });
    }
  }, {
    key: 'onInput',
    value: function onInput(e) {
      var code = e.keyCode;

      // these checks are necessary because of IE9. It has a buggy oninput event
      // and the system uses onkeyup to compensate. Here all relevant
      // non-alphanumeric input coming from onkeyup is filtered out.

      if (code === _utilsCore_utils.keyCodes.BACKSPACE || code === _utilsCore_utils.keyCodes.DELETE || code === _utilsCore_utils.keyCodes.LEFT || code === _utilsCore_utils.keyCodes.RIGHT || code === _utilsCore_utils.keyCodes.UP || code === _utilsCore_utils.keyCodes.DOWN || code === _utilsCore_utils.keyCodes.PAGE_DOWN || code === _utilsCore_utils.keyCodes.PAGE_UP) return;

      this.trigger('regularInput', e);
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.releaseKeyboard();
      this.off();
      this.offDom();
      this.dom.parentNode.removeChild(this.dom);
    }
  }]);

  return SimpleTagEditor;
})((0, _utilsCore_utils.mixin)(_utilsMixinsObservable_mixin2['default'], _utilsMixinsOptions_mixin2['default']));

exports['default'] = SimpleTagEditor;
module.exports = exports['default'];
},{"../utils/core_utils":11,"../utils/dom_helpers/dom_utils":20,"../utils/mixins/observable_mixin":27,"../utils/mixins/options_mixin":28}],10:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsCore_utils = _dereq_('../utils/core_utils');

var _utilsDom_helpersDom_utils = _dereq_('../utils/dom_helpers/dom_utils');

var _menuMenu = _dereq_('../menu/menu');

var _menuMenu2 = _interopRequireDefault(_menuMenu);

var _simple_tag_editor = _dereq_('./simple_tag_editor');

var _simple_tag_editor2 = _interopRequireDefault(_simple_tag_editor);

var _search_tag_editor = _dereq_('./search_tag_editor');

var _search_tag_editor2 = _interopRequireDefault(_search_tag_editor);

var defaultOptions = (0, _utilsCore_utils.extend)({}, _menuMenu2['default'].defaultOptions, {
  clsRoot: 'zd-tag-menu-root',
  clsItem: 'zd-tag-item',
  initialTagCount: Infinity,
  useUniqueValues: false,
  ignoreCase: false,
  enableEditing: true,
  hiddenTagsTemplate: '%@ more...',
  validateEditorValue: function validateEditorValue() {
    return true;
  },
  enterValueOnEditorBlur: false,
  getContentForClipboard: function getContentForClipboard(item) {
    return item.label;
  }
});

var TagMenu = (function (_Menu) {
  _inherits(TagMenu, _Menu);

  function TagMenu() {
    _classCallCheck(this, TagMenu);

    _get(Object.getPrototypeOf(TagMenu.prototype), 'constructor', this).apply(this, arguments);
  }

  _createClass(TagMenu, [{
    key: 'onBeforeLoad',
    value: function onBeforeLoad() {
      if (!this.editor) return;
      this.editor.dom.parentNode.removeChild(this.editor.dom);
      this.editor.close && this.editor.close();
    }
  }, {
    key: 'onLoad',
    value: function onLoad() {
      if (!this.editor) {
        this.createEditor();
        return;
      }

      var domEditorHolder = this.dom.querySelector(".zd-tag-editor-holder");
      domEditorHolder.innerHTML = '';
      domEditorHolder.appendChild(this.editor.dom);

      if (this.isFocused) {
        this.editor.domBase && this.editor.domBase.focus();
        this.editor.open && this.editor.open();
      }

      this.releaseKeyboard();
      this.captureEditorKeyboard();
    }
  }, {
    key: 'onSetup',
    value: function onSetup(options) {
      var _this = this;

      this.type = 'zdTagMenu';
      this.shouldShortenTags = true;
      this.editorFactory = this.options.editorFactory;

      if (!this.editorFactory) {
        if (this.options.searchEditorOptions) {
          this.editorFactory = this.searchEditorFactory;
        } else {
          this.editorFactory = this.simpleEditorFactory;
        }
      }
      this.on('zeroStateChanged', function () {
        return _this.onZeroStateChanged();
      });
    }
  }, {
    key: 'onZeroStateChanged',
    value: function onZeroStateChanged() {
      if (this.editor) {
        this.editor.resolvePlaceholder();
      }
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.releaseKeyboard();
      this.editor && this.editor.destroy();
      _get(Object.getPrototypeOf(TagMenu.prototype), 'destroy', this).call(this);
    }
  }, {
    key: 'syncViewWithValue',
    value: function syncViewWithValue(withFocus) {
      if (this.isInDom && this.isVisible) {
        this.showMenu(this.rootItem.menu, null);
      }
    }
  }, {
    key: 'postParser',
    value: function postParser(rootItem) {
      var menu;
      if (!rootItem.menu) {
        menu = [];
        menu.id = (0, _utilsCore_utils.getUniqId)();
        menu.isInDom = false;
        menu.parentItem = rootItem;
        rootItem.menu = menu;
      }
    }
  }, {
    key: 'getTags',
    value: function getTags() {
      return this.getValues().map(function (value) {
        return value.label;
      });
    }
  }, {
    key: 'getValues',
    value: function getValues() {
      return [].concat(this.rootItem.menu);
    }
  }, {
    key: 'setValues',
    value: function setValues(values) {
      values = [].concat(values || []);
      var item = undefined,
          i = undefined,
          len = values.length;
      for (i = 0; i < len; i++) {
        item = values[i];
        if (item == null) continue;
        if (typeof item !== 'object') {
          item = { value: String(item) };
          values[i] = item;
        }
        if (item.label == null) {
          item.label = item.value;
        }
      }
      this.loadData(values);
    }
  }, {
    key: 'renderMenu',
    value: function renderMenu(menu) {
      if (!menu) {
        return '';
      }

      var html = ['<ul id="' + menu.id + '" class="' + this.clsListHolder + '" role="menu">'];
      var len = this.shouldShortenTags ? Math.min(menu.length, this.initialTagCount) : menu.length;

      for (var i = 0; i < len; i++) {
        html.push(this.renderItem(menu[i]));
      }

      if (this.shouldShortenTags && menu.length - this.initialTagCount > 0) {
        html.push(this.renderTagShortener(menu.length - this.initialTagCount));
      }
      if (this.enableEditing) {
        html.push(this.renderTagEditor());
      }
      html.push('</ul>');
      return html.join('');
    }
  }, {
    key: 'renderItem',
    value: function renderItem(item) {
      var arrowHtml = undefined;
      var itemClass = this.clsItem;

      if (this.typeToClassMap && item.type && this.typeToClassMap[item.type]) {
        itemClass += ' ' + this.typeToClassMap[item.type];
      }
      if (this.roleToClassMap && item.role && this.roleToClassMap[item.role]) {
        itemClass += ' ' + this.roleToClassMap[item.role];
      }
      if (this.itemIsDisabled(item)) {
        itemClass += ' ' + this.clsItemDisabled;
      }

      if (item.menu) {
        arrowHtml = '<span class="' + this.clsItemArrow + '"></span>';
      } else if (item.role === 'uiBackLink') {
        item.label = this.backLinkLabel;
        arrowHtml = '<span class="' + this.clsBackArrow + '"></span>';
      } else {
        arrowHtml = '';
      }

      var menuItemIconHtml = this.enableMenuItemIcons ? this.renderItemIcon(item) : '';

      return '<li id="' + item.id + '" class="' + itemClass + '" role="presentation">' + (arrowHtml + menuItemIconHtml) + '\n              <a tabindex="-1" role="menuitem">' + this.renderItemContent(item, this.highlighter, _utilsCore_utils.escapeHtml) + '</a>\n              <span class="zd-tag-close">\n                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">\n                  <path stroke="currentColor" d="M4 10l6-6M4 4l6 6" stroke-linecap="round"/>\n                </svg>\n              </span>\n            </li>';
    }
  }, {
    key: 'renderTagShortener',
    value: function renderTagShortener(hiddenTagsCount) {
      var hiddenTagsLabel;
      if (typeof this.hiddenTagsTemplate === 'function') {
        hiddenTagsLabel = this.hiddenTagsTemplate(hiddenTagsCount);
      } else {
        hiddenTagsLabel = (0, _utilsCore_utils.fmt)(this.hiddenTagsTemplate, [hiddenTagsCount]);
      }
      return '<li class="zd-tag-shortener"><a>' + hiddenTagsLabel + '</a></li>';
    }
  }, {
    key: 'renderTagEditor',
    value: function renderTagEditor(hiddenTagsCount) {
      return '<li class="zd-tag-editor-holder"><button class="zd-menu-focus-shim"></button></li>';
    }
  }, {
    key: 'postDomInsertionSetup',
    value: function postDomInsertionSetup() {
      var _this2 = this;

      var onDomClick = function onDomClick(e) {
        if ((0, _utilsDom_helpersDom_utils.closest)(e.target, '.zd-tag-close', _this2.dom)) {
          _this2.onTagCloseClick(e);
        }
      };

      this.onDom(this.domMenuPanel, 'click', function (e) {
        return _this2.onRootClick(e);
      }).onDom(this.dom, 'mouseenter', function (e) {
        return _this2.onRootMouseEnter(e);
      }).onDom(this.dom, 'mouseleave', function (e) {
        return _this2.onRootMouseLeave(e);
      }).onDom(this.dom, 'click', onDomClick).onDom(this.dom, 'keydown', function (e) {
        return _this2.onKeyDownForCopy(e);
      }).onDom(document, 'click', function (e) {
        return _this2.onDocumentClick(e);
      });
    }
  }, {
    key: 'focus',
    value: function focus() {
      if (this.isFocused || this.disabled) {
        return;
      }
      _get(Object.getPrototypeOf(TagMenu.prototype), 'focus', this).call(this);

      this.expandItems();
      this.createEditor();
      this.focusEditor();
    }
  }, {
    key: 'blur',
    value: function blur() {
      _get(Object.getPrototypeOf(TagMenu.prototype), 'blur', this).call(this);
      this.clearEditor();
      this.releaseEditorKeyboard();
      this.releaseKeyboard();
    }
  }, {
    key: 'expandItems',
    value: function expandItems() {
      if (this.shouldShortenTags && this.activeMenu && this.activeMenu.length > this.initialTagCount) {
        var tagShortener = this.dom.querySelector('.zd-tag-shortener');
        tagShortener && (tagShortener.style.display = 'none');
        this.addItemsToDom(this.activeMenu.slice(this.initialTagCount));
      }
      this.shouldShortenTags = false;
    }
  }, {
    key: 'addItem',
    value: function addItem(value, label, options, skipUniqueueCheck) {
      if (!skipUniqueueCheck && this.useUniqueValues && this.getMatchingValues(value).length > 0) return false;

      options = options || {};
      var hasOwnProperty = Object.prototype.hasOwnProperty;

      var data = {
        value: value,
        label: label || value,
        enabled: hasOwnProperty.call(options, 'enabled') ? options.enabled : true,
        context: options.context
      };

      if (this.activeMenu) {
        var newItem = {
          data: data,
          enabled: data.enabled,
          label: data.label,
          value: data.value,
          isInDom: true,
          parentItem: this.activeMenu.parentItem,
          parentMenu: this.activeMenu,
          type: options.type,
          id: (0, _utilsCore_utils.getUniqId)(),
          enableHtmlEscape: hasOwnProperty.call(options, 'enableHtmlEscape') ? options.enableHtmlEscape : true
        };

        this.activeMenu.push(newItem);
        this.hashValues[newItem.value] = newItem;
        this.hashIds[newItem.id] = newItem;

        this.resolveZeroState();
        this.syncViewWithZeroState();
        this.addItemToDom(newItem);
        this.trigger('tagsChanged', { mode: 'add', tag: newItem });
        return true;
      }
    }
  }, {
    key: 'addHtmlToDomForItems',
    value: function addHtmlToDomForItems(items, html) {
      if (items[0].parentMenu.length === items.length) {
        (0, _utilsDom_helpersDom_utils.domById)(items[0].parentMenu.id).insertAdjacentHTML('afterbegin', html);
      } else {
        var domItems = this.dom.querySelectorAll('.' + this.clsItem);
        if (domItems.length) {
          domItems[domItems.length - 1].insertAdjacentHTML('afterend', html);
        }
      }
    }
  }, {
    key: 'addItemToDom',
    value: function addItemToDom(item) {
      this.addHtmlToDomForItems([item], this.renderItem(item));
    }
  }, {
    key: 'addItemsToDom',
    value: function addItemsToDom(items) {
      var _this3 = this;

      var html = items.map(function (item) {
        return _this3.renderItem(item);
      }).join('');
      this.addHtmlToDomForItems(items, html);
    }
  }, {
    key: 'removeItem',
    value: function removeItem(item) {
      this.removeItemFromModel(item);
      this.removeItemFromDom(item);
      this.resolveZeroState();
      this.syncViewWithZeroState();
      this.trigger('tagsChanged', { mode: 'remove', tag: item });
    }
  }, {
    key: 'removeItemFromModel',
    value: function removeItemFromModel(item) {
      var itemToDelete,
          id = item.id,
          indexToDelete;

      if (!this.activeMenu) return;

      this.activeMenu.some(function (item, index) {
        itemToDelete = item;
        indexToDelete = index;
        return item.id === id;
      });

      if (!itemToDelete) return;
      // remove item from current menu
      this.activeMenu.splice(indexToDelete, 1);

      delete this.hashValues[itemToDelete.value];
      delete this.hashIds[itemToDelete.id];
      return itemToDelete;
    }
  }, {
    key: 'removeItemFromDom',
    value: function removeItemFromDom(item) {
      var domToDelete = (0, _utilsDom_helpersDom_utils.domById)(item.id);
      domToDelete && domToDelete.parentNode.removeChild(domToDelete);
    }
  }, {
    key: 'onTagCloseClick',
    value: function onTagCloseClick(e) {
      var domItem = (0, _utilsDom_helpersDom_utils.closest)(e.target, '.' + this.clsItem, this.dom);

      if (domItem) {
        var item = this.domToItem(domItem);
        item && this.removeItem(item);
      }
      // needed in order to keep focus
      e.stopPropagation();
    }
  }, {
    key: 'focusLastItem',
    value: function focusLastItem() {
      var itemToFocus = this._getPrevSelectableItem(this.activeMenu, this.activeMenu.length - 1);
      itemToFocus && this.focusItem(itemToFocus);
    }
  }, {
    key: 'removeFocusedItem',
    value: function removeFocusedItem() {
      var menu = this.focusedItem.parentMenu;
      var index = this.getItemIndex(this.focusedItem);
      var itemToFocus = this._getNextSelectableItem(menu, index + 1) || this._getPrevSelectableItem(menu, index - 1);

      this.removeItem(this.focusedItem);

      if (itemToFocus !== this.focusedItem) {
        this.focusItem(itemToFocus);
      }
      if (itemToFocus == null) {
        this.focusedItem = null;
      }
    }
  }, {
    key: 'addItemFromEditor',
    value: function addItemFromEditor(value, context) {
      var _this4 = this;

      if (value.length === 0) return;
      context = context || {};

      if (!this.validateEditorValue(value, context, this.getItemIterator())) return;

      if (this.useUniqueValues) {
        var matchingItems = this.getMatchingValues(value);
        matchingItems.forEach(function (item) {
          return (0, _utilsDom_helpersDom_utils.blinkElement)(_this4.itemToDom(item));
        });

        if (matchingItems.length > 0) return;
      }

      this.addItem(value, context.label || value, { context: context }, true);
      this.clearEditor();
    }
  }, {
    key: 'getMatchingValues',
    value: function getMatchingValues(value) {
      var items = [],
          lowerCaseValue = String(value).toLowerCase();

      if (!this.useUniqueValues) return items;

      if (this.ignoreCase) {
        this.getItemIterator().forEach(function (menuItem, menuItemValue) {
          if (lowerCaseValue === String(menuItemValue).toLowerCase()) {
            items.push(menuItem);
          }
        });
      } else {
        var item = this.hashValues[value];
        item && items.push(item);
      }
      return items;
    }
  }, {
    key: 'clearFocusedItem',
    value: function clearFocusedItem() {
      if (this.focusedItem) {
        this.blurItem(this.focusedItem);
        this.focusedItem = null;
      }
    }
  }, {
    key: 'onKeyDownForCopy',
    value: function onKeyDownForCopy(e) {
      if (e.keyCode === 67 && (e.metaKey || e.ctrlKey)) {
        if (!this.focusedItem) return;
        var prevActiveElement = document.activeElement;
        var input = document.createElement('input');
        input.style.cssText = "position:absolute;top:-700px;left:10px;";
        input.value = this.getContentForClipboard(this.focusedItem);
        document.body.appendChild(input);
        input.select();

        setTimeout(function () {
          prevActiveElement.focus();
          document.body.removeChild(input);
        }, 10);
      }
    }
  }, {
    key: 'onKeyDown',
    value: function onKeyDown(e) {
      if (!this.activeMenu) return;
      if (!this.isFocused) {
        this.blur();
        return;
      }

      switch (e.keyCode) {
        case _utilsCore_utils.keyCodes.DELETE:
        case _utilsCore_utils.keyCodes.BACKSPACE:
          this.focusedItem && this.removeFocusedItem();break;

        case _utilsCore_utils.keyCodes.HOME:
          this.moveToStart();e.preventDefault();break;
        case _utilsCore_utils.keyCodes.END:
          this.moveToEnd();e.preventDefault();break;

        case _utilsCore_utils.keyCodes.PAGE_DOWN:
        case _utilsCore_utils.keyCodes.PAGE_UP:
          e.preventDefault();break;

        case _utilsCore_utils.keyCodes.DOWN:
        case _utilsCore_utils.keyCodes.RIGHT:
          this.moveDown();e.preventDefault();break;

        case _utilsCore_utils.keyCodes.UP:
        case _utilsCore_utils.keyCodes.LEFT:
          this.focusedItem ? this.moveUp() : this.focusLastItem(), e.preventDefault();break;

        case _utilsCore_utils.keyCodes.ENTER:
        case _utilsCore_utils.keyCodes.NUMPAD_ENTER:
        case _utilsCore_utils.keyCodes.SPACE:
        case _utilsCore_utils.keyCodes.COMMA:
          e.preventDefault();break;

        default:
          this.clearFocusedItem();
          this.releaseKeyboard();
          this.captureEditorKeyboard();
      }
      this.trigger('keyDown', { domEvent: e });
    }
  }, {
    key: 'onRootClick',
    value: function onRootClick(e) {
      var item = this.resolveItemFromDom(e.target);
      if (item && item.role !== 'uiLabel' && this.itemIsSelectable(item)) {
        this.focusItem(item);
        this.releaseEditorKeyboard();
        this.captureKeyboard();
      }
      this.expandItems();
      this.focusEditor();
    }

    // Editor related
    // --------------------------------------------------

  }, {
    key: 'createEditor',
    value: function createEditor() {
      var _this5 = this;

      if (this.editor) return;

      var domHolder = this.dom.querySelector(".zd-tag-editor-holder");
      domHolder.innerHTML = '';
      this.editor = this.editorFactory(domHolder, this);
      this.editor.on('leftExit', function () {
        return _this5.onEditorLeftExit();
      }).on('tagEntered', function (e, data) {
        return _this5.onEditorTagEntered(data);
      }).on('regularInput', function () {
        return _this5.onEditorRegularInput();
      }).on('focus', function () {
        return _this5.onEditorFocus();
      }).on('blur', function () {
        return _this5.onEditorBlur();
      });

      if (this.enterValueOnEditorBlur) {
        this.editor.on('beforeBlur', function () {
          return _this5.onEditorBeforeBlur();
        });
      }

      this.editor.onBeforeKeyDown = function () {
        if (this.container && !this.container.isFocused) {
          this.container.blur();
          return true;
        }
      };

      this.onEditorCreated && this.onEditorCreated(this.editor);
    }
  }, {
    key: 'simpleEditorFactory',
    value: function simpleEditorFactory(domHolder, container) {
      var editorOptions = (0, _utilsCore_utils.extend)({}, this.options.simpleEditor || {}, {
        domHolder: domHolder,
        container: container || this
      });
      return new _simple_tag_editor2['default'](editorOptions);
    }
  }, {
    key: 'searchEditorFactory',
    value: function searchEditorFactory(domHolder, container) {
      var options = this.options.searchEditorOptions;
      options.domHolder = domHolder;
      options.container = container || this;

      return new _search_tag_editor2['default'](options);
    }
  }, {
    key: 'onEditorFocus',
    value: function onEditorFocus() {
      this.focus();
    }
  }, {
    key: 'onEditorBlur',
    value: function onEditorBlur() {
      this.blur();
    }
  }, {
    key: 'onEditorBeforeBlur',
    value: function onEditorBeforeBlur() {
      var _this6 = this;

      var editorValue = this.getEditorValue().trim();
      if (!editorValue) return;
      setTimeout(function () {
        return !_this6.isFocused && _this6.addItemFromEditor(editorValue);
      }, 200);
    }
  }, {
    key: 'onEditorLeftExit',
    value: function onEditorLeftExit() {
      if (this.activeMenu.length === 0) return;
      this.focusLastItem();
      this.releaseEditorKeyboard();
      this.captureKeyboard();
    }
  }, {
    key: 'onEditorRegularInput',
    value: function onEditorRegularInput() {
      this.clearFocusedItem();
    }
  }, {
    key: 'onEditorTagEntered',
    value: function onEditorTagEntered(data) {
      this.addItemFromEditor(data.value, data.context);
      // TODO: this needs abstraction
      if (this.editor.domBase) {
        this.editor.domBase.focus();
      }
    }
  }, {
    key: 'captureEditorKeyboard',
    value: function captureEditorKeyboard() {
      if (this.disabled) return;
      this.releaseKeyboard();
      this.editor && this.editor.captureKeyboard();
    }
  }, {
    key: 'releaseEditorKeyboard',
    value: function releaseEditorKeyboard() {
      this.editor && this.editor.releaseKeyboard();
    }
  }, {
    key: 'clearEditor',
    value: function clearEditor() {
      this.editor && this.editor.clearEditor();
    }
  }, {
    key: 'getEditorValue',
    value: function getEditorValue() {
      return this.editor ? this.editor.getEditorValue() : '';
    }
  }, {
    key: 'focusEditor',
    value: function focusEditor() {
      if (this.disabled) return;
      this.captureEditorKeyboard();
      this.editor && this.editor.focus();
    }
  }, {
    key: 'defaultOptions',
    get: function get() {
      return defaultOptions;
    }
  }]);

  return TagMenu;
})(_menuMenu2['default']);

exports['default'] = TagMenu;
module.exports = exports['default'];
},{"../menu/menu":4,"../utils/core_utils":11,"../utils/dom_helpers/dom_utils":20,"./search_tag_editor":8,"./simple_tag_editor":9}],11:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

exports.K = K;
exports.defaults = defaults;
exports.extend = extend;
exports.mixin = mixin;
exports.cleanObject = cleanObject;
exports.escapeHtml = escapeHtml;
exports.fmt = fmt;
exports.escapeRegExp = escapeRegExp;
exports.getUniqId = getUniqId;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function K() {
  return this;
}

function defaults(obj) {
  if (arguments.length < 2) return obj;

  for (var key, source, i = 1; i < arguments.length; i++) {
    source = arguments[i];
    for (key in source) {
      if (!source.hasOwnProperty(key)) continue;
      if (obj[key] != null) continue;
      obj[key] = source[key];
    }
  }
  return obj;
}

function extend(obj) {
  if (arguments.length < 2) return obj;

  for (var key, source, i = 1; i < arguments.length; i++) {
    source = arguments[i];
    for (key in source) {
      if (!source.hasOwnProperty(key)) continue;
      obj[key] = source[key];
    }
  }
  return obj;
}

function mixin() {
  var fn = function fn() {};
  var args = [{}].concat(Array.prototype.slice.call(arguments));
  fn.prototype = extend.apply(null, args);
  return fn;
}

function cleanObject(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      obj[key] = typeof obj[key] === 'function' ? K : null;
    }
  }
}

// borrowed from Handlebars

var escapeMap = {
  "<": "&lt;",
  ">": "&gt;",
  '"': "&quot;",
  "'": "&#x27;",
  "`": "&#x60;"
};

var badChars = /&(?!\w+;)|[<>"'`]/g;
var possible = /[&<>"'`]/;

function escapeBadChar(chr) {
  return escapeMap[chr] || "&amp;";
}

function escapeHtml(str) {
  return possible.test(str) ? str.replace(badChars, escapeBadChar) : str;
}

// borrowed from Ember

function fmt(str, formats) {
  // first, replace any ORDERED replacements.
  var idx = 0; // the current index for non-numerical replacements
  return str.replace(/%@([0-9]+)?/g, function (s, argIndex) {
    argIndex = argIndex ? parseInt(argIndex, 0) - 1 : idx++;
    s = formats[argIndex];
    return (s === null ? '(null)' : s === undefined ? '' : s).toString();
  });
}

function escapeRegExp(str) {
  return str.replace(/[\-\[\]\{\}\(\)\*\+\?\.\,\\\^\$\|#\s]/g, "\\$&");
}

var IdGenerator = (function () {
  function IdGenerator(idPrefix) {
    _classCallCheck(this, IdGenerator);

    this.idPrefix = idPrefix || 'zd_mn_';
    this.counter = 0;
  }

  _createClass(IdGenerator, [{
    key: "getUniqId",
    value: function getUniqId(prefix) {
      return (prefix || this.idPrefix) + this.counter++;
    }
  }]);

  return IdGenerator;
})();

var idGenerator = new IdGenerator();

function getUniqId() {
  return idGenerator.getUniqId('mn_');
}

var userAgent = {
  ie9: navigator.userAgent.indexOf('MSIE 9.0') > -1
};

exports.userAgent = userAgent;
var keyCodes = {
  DOWN: 40,
  END: 35,
  ENTER: 13,
  ESCAPE: 27,
  HOME: 36,
  LEFT: 37,
  NUMPAD_ENTER: 108,
  PAGE_DOWN: 34,
  PAGE_UP: 33,
  RIGHT: 39,
  SPACE: 32,
  SHIFT: 16,
  TAB: 9,
  UP: 38,
  COMMA: 188,
  BACKSPACE: 8,
  DELETE: 46
};
exports.keyCodes = keyCodes;
},{}],12:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _core_utils = _dereq_('../core_utils');

var _iteratorsHash_iterator = _dereq_('../iterators/hash_iterator');

var _iteratorsHash_iterator2 = _interopRequireDefault(_iteratorsHash_iterator);

var _iteratorsArray_iterator = _dereq_('../iterators/array_iterator');

var _iteratorsArray_iterator2 = _interopRequireDefault(_iteratorsArray_iterator);

function isInvalidItem(item, filteredField) {
  return item.role === 'uiBackLink' || item.role === 'uiLabel' || item.role === 'root' || item[filteredField] == null || item.value == null;
}

var FilteringDataSource = (function () {
  _createClass(FilteringDataSource, null, [{
    key: 'buildFilter',
    value: function buildFilter(condition) {
      var filter = function filter(filterer, source, data) {
        var filterRe = filterer.getWordRegExp(),
            filteredField = filterer.filteredField,
            valueHash = Object.create(null);

        var iterator;

        if (source.hashIds) {
          iterator = new _iteratorsHash_iterator2['default'](function () {
            return source.hashIds;
          });
        } else if (Array.isArray(source.data)) {
          iterator = new _iteratorsArray_iterator2['default'](function () {
            return source.data;
          });
        }
        if (!iterator) return data;

        iterator.forEach(function (item) {
          if (data.length >= filterer.maxRecords) {
            return false; // break the loop
          }

          if ('enabled' in item && item.enabled === false) return;

          if (isInvalidItem(item, filteredField)) return;

          // making sure we do not include items with same value e.g. group items
          // have same value but purticipate in the menu 2 times
          if (valueHash[item.value] === 1) return;
          valueHash[item.value] = 1;

          if (condition && condition(item, filterer, source, data) !== true) return;

          if (filterRe.test(item[filteredField])) {
            var newItem = data[data.length] = {
              value: item.value,
              label: item.label,
              type: item.type,
              sourceItem: item
            };
            newItem[filteredField] = item[filteredField];
          }
        });

        return data;
      };

      return filter;
    }
  }]);

  function FilteringDataSource(maxRecords, filteredField, filterQueue) {
    _classCallCheck(this, FilteringDataSource);

    this.reset();
    this.maxRecords = maxRecords || Infinity;
    this.filteredField = filteredField || 'label';
    this.filterQueue = (typeof filterQueue === 'function' ? [filterQueue] : filterQueue) || [this.constructor.buildFilter()];
  }

  _createClass(FilteringDataSource, [{
    key: 'loadData',
    value: function loadData(data) {
      this.data = data;
      this.onDataReady(data);
    }
  }, {
    key: 'filter',
    value: function filter(source, filterWord) {
      var _this = this;

      if (filterWord == null || filterWord === '') {
        this.filterWord = '';
        this.loadData([]);
        return;
      }

      if (filterWord === this.filterWord) return;
      this.filterWord = filterWord;

      if (!this.filterWord) {
        return;
      }
      var data = [];

      this.filterQueue.forEach(function (filter) {
        return filter(_this, source, data);
      });

      this.loadData(data);
    }
  }, {
    key: 'onDataReady',
    value: function onDataReady() {}
  }, {
    key: 'getWordRegExp',
    value: function getWordRegExp() {
      return this.getRegExp(this.filterWord);
    }
  }, {
    key: 'getRegExp',
    value: function getRegExp(word) {
      return new RegExp((0, _core_utils.escapeRegExp)(word), 'i');
    }
  }, {
    key: 'reset',
    value: function reset() {
      this.resetFilter();
      this.data = [];
    }
  }, {
    key: 'resetFilter',
    value: function resetFilter() {
      this.filterWord = '';
    }
  }]);

  return FilteringDataSource;
})();

exports['default'] = FilteringDataSource;
module.exports = exports['default'];
},{"../core_utils":11,"../iterators/array_iterator":23,"../iterators/hash_iterator":24}],13:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

var emptyFn = function emptyFn() {};

function ajax(options) {
  var _options$success = options.success;
  var success = _options$success === undefined ? emptyFn : _options$success;
  var _options$error = options.error;
  var error = _options$error === undefined ? emptyFn : _options$error;
  var _options$always = options.always;
  var always = _options$always === undefined ? emptyFn : _options$always;
  var context = options.context;
  var _options$type = options.type;
  var type = _options$type === undefined ? 'GET' : _options$type;
  var _options$headers = options.headers;
  var headers = _options$headers === undefined ? [] : _options$headers;

  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function () {
    var data = undefined;
    if (xhr.readyState === 4) {
      if (xhr.status !== 200) {
        error.call(context, xhr);
        always.call(context, xhr);
        return;
      }

      try {
        data = JSON.parse(xhr.responseText);
      } catch (e) {
        error.call(context, xhr);
        always.call(context, xhr);
        return;
      }
      success.call(context, data);
      always.call(context, xhr, data);
    }
  };

  xhr.open(type, options.url, true);
  headers.forEach(function (pair) {
    return xhr.setRequestHeader.apply(xhr, _toConsumableArray(pair));
  });
  xhr.send();

  return xhr;
}

exports['default'] = { ajax: ajax };
module.exports = exports['default'];
},{}],14:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x3, _x4, _x5) { var _again = true; _function: while (_again) { var object = _x3, property = _x4, receiver = _x5; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x3 = parent; _x4 = property; _x5 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _core_utils = _dereq_('../core_utils');

var _mixinsObservable_mixin = _dereq_('../mixins/observable_mixin');

var _mixinsObservable_mixin2 = _interopRequireDefault(_mixinsObservable_mixin);

var _json_transport = _dereq_('./json_transport');

var _json_transport2 = _interopRequireDefault(_json_transport);

var RemoteSearchDataSource = (function (_mixin) {
  _inherits(RemoteSearchDataSource, _mixin);

  function RemoteSearchDataSource(maxRecords, url) {
    var options = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

    _classCallCheck(this, RemoteSearchDataSource);

    _get(Object.getPrototypeOf(RemoteSearchDataSource.prototype), 'constructor', this).call(this, maxRecords, url, options);

    this.filterWord = '';
    this.data = [];
    this.url = url;
    this.maxRecords = maxRecords || 100;
    this.cache = {};
    this.xhr = null;
    this.parse = options.parse || this.parse;
    this.dataFilter = options.dataFilter || this.dataFilter;
    this.httpMethod = options.httpMethod || 'GET';
    this.transport = options.transport || _json_transport2['default'];
    this.performCaching = options.performCaching || false;
  }

  _createClass(RemoteSearchDataSource, [{
    key: 'onDataReady',
    value: function onDataReady() {}
  }, {
    key: 'dataFilter',
    value: function dataFilter(data) {
      return data;
    }
  }, {
    key: 'loadData',
    value: function loadData(json, word) {
      var data = this.dataFilter(json);
      if (word && this.performCaching) {
        this.cache[word] = json;
      }
      this.data = this.parse(data);
      this.onDataReady(this.data);
    }
  }, {
    key: 'filter',
    value: function filter(source, filterWord) {
      if (filterWord == null || filterWord === '') {
        this.resetFilter();
        this.abort();
        this.loadData();
        return;
      }

      filterWord += '';
      if (filterWord === this.filterWord) {
        return;
      }
      this.filterWord = filterWord;

      if (!this.filterWord) {
        return;
      }

      this.fetch(this.filterWord);
    }
  }, {
    key: 'fetch',
    value: function fetch(word) {
      this.abort();

      if (this.performCaching && this.cache.hasOwnProperty(word)) {
        this.trigger('fetch', {
          term: word,
          data: this.cache[word],
          cached: true,
          status: 'success'
        });
        this.loadData(this.cache[word]);
        return;
      }

      this.trigger('beforeFetch', { term: word });

      this.xhr = this.transport.ajax({
        type: this.httpMethod,
        url: (0, _core_utils.fmt)(this.url, [encodeURIComponent(this.filterWord), this.maxRecords]),
        context: this,
        success: function success(data) {
          this.trigger('fetch', {
            term: word,
            data: data,
            cached: false,
            status: 'success'
          });
          this.loadData(data, word);
        },
        error: function error(xhr) {
          this.loadData();
        }
      });
    }
  }, {
    key: 'abort',
    value: function abort() {
      this.xhr && this.xhr.abort();
    }
  }, {
    key: 'parse',
    value: function parse() {
      var data = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

      return data.slice(0, this.maxRecords);
    }
  }, {
    key: 'resetFilter',
    value: function resetFilter() {
      this.filterWord = '';
    }
  }, {
    key: 'invalidateCache',
    value: function invalidateCache() {
      this.cache = {};
    }
  }]);

  return RemoteSearchDataSource;
})((0, _core_utils.mixin)(_mixinsObservable_mixin2['default']));

exports['default'] = RemoteSearchDataSource;
module.exports = exports['default'];
},{"../core_utils":11,"../mixins/observable_mixin":27,"./json_transport":13}],15:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SimpleMenuDataSource = (function () {
  function SimpleMenuDataSource(options) {
    _classCallCheck(this, SimpleMenuDataSource);

    this.options = options || {};
    this.parser = this.options.parser;
    this.maxRecords = this.options.maxRecords || Infinity;
  }

  _createClass(SimpleMenuDataSource, [{
    key: "loadData",
    value: function loadData(data) {
      if (this.parser) {
        data = this.parser.parse(data);
      }
      if (data && data.length > this.maxRecords) {
        data = data.slice(0, this.maxRecords);
      }
      this.onDataReady(data);
    }
  }, {
    key: "onDataReady",
    value: function onDataReady(data) {}
  }]);

  return SimpleMenuDataSource;
})();

exports["default"] = SimpleMenuDataSource;
module.exports = exports["default"];
},{}],16:[function(_dereq_,module,exports){
/*
----------------------------------------
Simple animation library
----------------------------------------

This library can animate only numeric style properties of dom elements such as
width, height, left, top, etc. It is possible to animate multiple objects in
sync where each object can have its own animation properties.

Additional features:

- Animate two or more numeric style properties on the same element by specifying
  multiple animation options for the same dom element but for different
  "property" option.

- Schedule multiple animations for the same element in time. The options
  "startTime" and "duration" need to be specified for each animation.

Example for the general case:
----------------------------------------
let animation = animate({
  animations: [{
    element: document.getElementById('element1'),
    from: 10,
    to: 300,
    duration: 1000
  }, {
    element: document.getElementById('element2'),
    from: 10,
    to: 400
  }, {
    element: document.getElementById('element3'),
    from: 10,
    to: 500
  }],
  // element: ...          // optional - used when animating only one element for one property
  duration: 1000,          // mandatory (in ms)
  property: 'left',        // mandatory unless provided in each animation option
  unit: 'px',              // optional, default is 'px'
  easing: 'easeOutQuad',   // optional, default is 'easeOutQuad'. Also an easing function can be provided
  onComplete: function() { // optional callback on animation of all objects done
    console.log("Done");
  }
});

Animation starts immediately after invokation of "animate".
The easing functions used are the same as ones used in jQuery.

The animation can be cancelled, by calling animation.cancel();
*/

// t: current time, b: begining value, c: change (delta) in value, d: duration

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

exports['default'] = animate;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var EASINGS = {
  easeOutQuad: function easeOutQuad(x, t, b, c, d) {
    return -c * (t /= d) * (t - 2) + b;
  }
};

var emptyFn = function emptyFn() {};
var timer = undefined;

function Stepper(duration, onComplete) {
  onComplete = onComplete || emptyFn;
  var animations = [];
  var startTime = null;

  function step(timestamp) {
    if (!startTime) startTime = timestamp;

    var curTime = timestamp - startTime;

    animations.forEach(function (callback) {
      return callback(curTime);
    });

    if (curTime < duration) {
      timer = window.requestAnimationFrame(step);
    } else {
      onComplete();
    }
  }

  return {
    onStep: function onStep(callback) {
      animations.push(callback);
    },

    start: function start() {
      timer = window.requestAnimationFrame(step);
      return {
        cancel: function cancel() {
          window.cancelAnimationFrame(timer);
        }
      };
    }
  };
}

var Animation = (function () {
  function Animation(options, defaults) {
    var _this = this;

    _classCallCheck(this, Animation);

    defaults = defaults || options;

    this.element = options.element || defaults.element;
    this.property = options.property || defaults.property;
    this.to = options.to !== undefined ? options.to : defaults.to;
    this.from = options.from !== undefined ? options.from : defaults.from;
    this.easing = options.easing || defaults.easing;
    this.unit = options.unit ? String(options.unit) : defaults.unit;
    this.stepper = options.stepper || defaults.stepper;
    this.timeStart = options.timeStart || 0;
    this.duration = options.duration || defaults.duration - this.timeStart;

    var isFirstStep = true;

    this.stepper.onStep(function (curTime) {
      // animation hasn't started
      if (curTime < _this.timeStart) return;

      curTime -= _this.timeStart;
      // animation ended
      if (curTime > _this.duration) {
        return;
      }

      var value = undefined;
      var offset = 0;
      var from = _this.from;
      var to = _this.to;

      // set the specified "from" value and do initial setup
      if (isFirstStep) {
        isFirstStep = false;
        _this.initOnStart();
      }
      // compensate for negative values as the easing equations work only with
      // positive values
      if (_this.from < 0 || _this.to < 0) {
        offset = Math.min(_this.from, _this.to) * -1;
        from += offset;
        to += offset;
      }

      if (from > to) {
        value = Math.max(from - _this.easing(null, curTime, 0, Math.abs(to - from), _this.duration), to);
      } else {
        value = Math.min(_this.easing(null, curTime, from, Math.abs(to - from), _this.duration), to);
      }

      _this.element.style[_this.property] = value - offset + _this.unit;
    });
  }

  _createClass(Animation, [{
    key: 'initOnStart',
    value: function initOnStart() {
      this.element.style[this.property] = this.from + this.unit;
    }
  }]);

  return Animation;
})();

function animate(options) {
  var stepper = Stepper(options.duration, options.onComplete);

  var defaults = {
    element: options.element,
    property: options.property,
    to: options.to,
    from: options.from,
    duration: options.duration,
    timeStart: options.timeStart,
    easing: options.easing === "function" ? options.easing : EASINGS[options.easing || 'easeOutQuad'],
    unit: options.unit ? String(options.unit) : 'px',
    stepper: stepper
  };

  if (options.element) {
    new Animation(defaults);
  }

  if (options.animations) {
    options.animations.forEach(function (animOptions) {
      return new Animation(animOptions, defaults);
    });
  }

  stepper.start();
}

module.exports = exports['default'];
},{}],17:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var CssClsStateMachine = (function () {
  function CssClsStateMachine(options) {
    _classCallCheck(this, CssClsStateMachine);

    this.dom = options.dom;
    this.domAux = options.domAux;
    this.clsZero = options.clsZero;
    this.state = 0;
    this.bitMap = {};
    this.setup(options.clsStates);
  }

  _createClass(CssClsStateMachine, [{
    key: 'setup',
    value: function setup(states) {
      this.bitMap = {};
      for (var i = 0, bitMask = 1; i < states.length; i++, bitMask *= 2) {
        this.bitMap[states[i]] = bitMask;
      }
    }
  }, {
    key: 'addState',
    value: function addState(state) {
      if (!this.bitMap.hasOwnProperty(state)) {
        return;
      }
      this.state = this.state | this.bitMap[state];
      this.dom().classList.add(state);
      this.domAux() && this.domAux().classList.add(state);
      this.postStateChange();
    }
  }, {
    key: 'removeState',
    value: function removeState(state) {
      if (!this.bitMap.hasOwnProperty(state)) {
        return;
      }
      this.state = this.state & ~this.bitMap[state];
      this.dom().classList.remove(state);
      this.domAux() && this.domAux().classList.remove(state);
      this.postStateChange();
    }
  }, {
    key: 'postStateChange',
    value: function postStateChange() {
      this.dom().classList[this.state === 0 ? 'add' : 'remove'](this.clsZero);
      this.domAux() && this.domAux().classList[this.state === 0 ? 'add' : 'remove'](this.clsZero);
    }
  }]);

  return CssClsStateMachine;
})();

exports['default'] = CssClsStateMachine;
module.exports = exports['default'];
},{}],18:[function(_dereq_,module,exports){
// The purpose of this is to setup document level events
// only when the target widget is active i.e. open.

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var DocumentClicksMonitor = (function () {
  function DocumentClicksMonitor(context) {
    var _this = this;

    _classCallCheck(this, DocumentClicksMonitor);

    this.context = context;
    context.on('open', function () {
      return _this.monitorGlobalClicks();
    });
    context.on('close', function () {
      return _this.releaseGlobalClicksMonitor();
    });
    context.on('beforeDestroy', function () {
      return _this.onContextDestroy();
    });
  }

  _createClass(DocumentClicksMonitor, [{
    key: 'monitorGlobalClicks',
    value: function monitorGlobalClicks() {
      var _this2 = this;

      if (this.eventListener) return;

      this.eventListener = function (e) {
        return _this2.context.onDocumentMouseDown(e);
      };
      document.addEventListener('mousedown', this.eventListener, true);
    }
  }, {
    key: 'releaseGlobalClicksMonitor',
    value: function releaseGlobalClicksMonitor() {
      if (this.eventListener) {
        document.removeEventListener('mousedown', this.eventListener, true);
        this.eventListener = null;
      }
    }
  }, {
    key: 'onContextDestroy',
    value: function onContextDestroy() {
      this.releaseGlobalClicksMonitor();
      this.context = null;
    }
  }]);

  return DocumentClicksMonitor;
})();

exports['default'] = DocumentClicksMonitor;
module.exports = exports['default'];
},{}],19:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MONITOR_INTERVAL = 100;

var DomPositionObserver = (function () {
  function DomPositionObserver() {
    var options = arguments.length <= 0 || arguments[0] === undefined ? { monitoringInterval: MONITOR_INTERVAL } : arguments[0];

    _classCallCheck(this, DomPositionObserver);

    this._monitoringTimer = null;
    this.monitoringInterval = options.monitoringInterval;
    this.dom = options.dom;
    this.onChange = options.onChange;
  }

  _createClass(DomPositionObserver, [{
    key: "observeOnce",
    value: function observeOnce(dom, onChange) {
      return this.observe(dom, onChange, true);
    }
  }, {
    key: "observe",
    value: function observe(dom, onChange, isOnce) {
      var _this = this;

      this.dom = this.dom || dom;
      this.onChange = onChange || this.onChange;

      if (!this.dom) {
        this.stopObserving();
        return;
      }

      this.refClientRect = this.dom.getBoundingClientRect();

      clearTimeout(this._monitoringTimer);

      this._monitoringTimer = setTimeout(function () {
        if (_this._isChanged()) {
          onChange();
          if (isOnce) {
            _this.stopObserving();
            return;
          }
        }
        _this.observe(dom, onChange, isOnce);
      }, MONITOR_INTERVAL);
    }
  }, {
    key: "_isChanged",
    value: function _isChanged() {
      var refCoords = this.refClientRect;
      var coords = this.dom.getBoundingClientRect();

      return refCoords.top !== coords.top || refCoords.left !== coords.left || refCoords.right !== coords.right || refCoords.bottom !== coords.bottom;
    }
  }, {
    key: "stopObserving",
    value: function stopObserving() {
      clearTimeout(this._monitoringTimer);
    }
  }, {
    key: "destroy",
    value: function destroy() {
      this.stopObserving();
      this.dom = null;
      this.onChange = null;
      this.refClientRect = null;
    }
  }]);

  return DomPositionObserver;
})();

exports["default"] = DomPositionObserver;
module.exports = exports["default"];
},{}],20:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.domById = domById;
exports.addClass = addClass;
exports.removeClass = removeClass;
exports.toggleClass = toggleClass;
exports.positionDomIntoView = positionDomIntoView;
exports.setOuterWidth = setOuterWidth;
exports.setOuterHeight = setOuterHeight;
exports.blinkElement = blinkElement;
exports.closest = closest;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function domById(domId) {
  return document.getElementById(domId);
}

function addClass(dom, cls) {
  var _dom$classList;

  cls = Array.isArray(cls) ? cls : cls.split(/\s+/);
  (_dom$classList = dom.classList).add.apply(_dom$classList, _toConsumableArray(cls));
}

function removeClass(dom, cls) {
  var _dom$classList2;

  cls = Array.isArray(cls) ? cls : cls.split(/\s+/);
  (_dom$classList2 = dom.classList).remove.apply(_dom$classList2, _toConsumableArray(cls));
}

function toggleClass(dom, cls, condition) {
  cls = Array.isArray(cls) ? cls : cls.split(/\s+/);
  if (condition) {
    var _dom$classList3;

    (_dom$classList3 = dom.classList).add.apply(_dom$classList3, _toConsumableArray(cls));
  } else {
    var _dom$classList4;

    (_dom$classList4 = dom.classList).remove.apply(_dom$classList4, _toConsumableArray(cls));
  }
}

function positionDomIntoView(dom) {
  if (!dom) return;

  if (dom.scrollIntoViewIfNeeded) {
    // Why can't DOM APIs be as easy as this !!!
    dom.scrollIntoViewIfNeeded();
    return;
  }
  // Simulating ``scrollIntoViewIfNeeded`` ... sigh ...
  var rect = dom.getBoundingClientRect();
  var isTopOverlaped = document.elementFromPoint(rect.left + rect.width / 2 | 0, rect.top + 1) !== dom;
  var isBottomOverlaped = document.elementFromPoint(rect.left + rect.width / 2 | 0, rect.bottom - 2) !== dom;

  if (isTopOverlaped) {
    dom.scrollIntoView(true);
  } else if (isBottomOverlaped) {
    dom.scrollIntoView(false);
  }
}

function setOuterDimension(dimension, target, value) {
  // dimension = ['width' | 'height']
  var origDisplay = getComputedStyle(target).getPropertyValue('display');
  var origVisibility = target.style.visibility;

  if (origDisplay === 'none') {
    target.style.visibility = 'hidden';
    target.style.display = '';
  }

  var outerDim = target[dimension === 'width' ? 'offsetWidth' : 'offsetHeight'];
  var innerDim = parseInt(getComputedStyle(target).getPropertyValue(dimension)) || 0;

  target.style[dimension] = value + innerDim - outerDim + 'px';

  target.style.visibility = origVisibility;
  target.style.display = origDisplay;
}

function setOuterWidth(target, width) {
  setOuterDimension('width', target, width);
}

function setOuterHeight(target, height) {
  setOuterDimension('height', target, height);
}

function blinkElement(el) {
  var times = arguments.length <= 1 || arguments[1] === undefined ? 3 : arguments[1];
  var interval = arguments.length <= 2 || arguments[2] === undefined ? 100 : arguments[2];

  var c = 0;
  if (el._blinkerTimer) return;

  el._blinkerTimer = setInterval(function () {
    el.style.visibility = c % 2 ? '' : 'hidden';
    if (c === times) {
      clearInterval(el._blinkerTimer);
      delete el._blinkerTimer;
    }
    c++;
  }, interval);
}

function closest(el, selector) {
  var domScope = arguments.length <= 2 || arguments[2] === undefined ? document : arguments[2];

  if (el.closest) {
    var domClosest = el.closest(selector);
    return domClosest && domScope.contains(domClosest) ? domClosest : null;
  }
  var matches = el.matches || el.msMatchesSelector || el.webkitMatchesSelector || el.mozMatchesSelector;
  if (!matches) return;
  if (matches.call(el, selector)) return el;

  while (el = el.parentNode && el !== domScope) {
    if (matches.call(el, selector)) return el;
  }
  return null;
}
},{}],21:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = applyShims;
function classList() {
  if ("classList" in document.documentElement) return;

  Object.defineProperty(Element.prototype, "classList", {
    get: function get() {
      var self = this;
      var rspaces = /\s+/g;
      var implement;

      function classlist() {
        return self.className.trim().split(rspaces);
      }

      function update(fn) {
        return function (value) {
          var classes = classlist();
          var index = classes.indexOf(value);

          fn(classes, index, value);
          self.className = classes.join(" ");

          implement.length = classes.length;
        };
      }

      implement = {
        length: (function () {
          return classlist().length;
        })(),

        add: update(function (classes, index, value) {
          ~index || classes.push(value);
        }),

        remove: update(function (classes, index) {
          ~index && classes.splice(index, 1);
        }),

        toggle: update(function (classes, index, value) {
          ~index ? classes.splice(index, 1) : classes.push(value);
        }),

        contains: function contains(value) {
          return !! ~classlist().indexOf(value);
        },

        item: function item(i) {
          return classlist()[i] || null;
        }
      };

      return implement;
    }
  });
}

function requestAnimationFrame() {
  var lastTime = 0;

  if (window.requestAnimationFrame) return;

  window.requestAnimationFrame = function (callback, element) {
    var currTime = Date.now();
    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
    var id = window.setTimeout(function () {
      callback(currTime + timeToCall);
    }, timeToCall);
    lastTime = currTime + timeToCall;
    return id;
  };

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
}

function applyShims() {
  // IE9 simulation of classList
  classList();
  requestAnimationFrame();
}

module.exports = exports["default"];
},{}],22:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _dom_utils = _dereq_('./dom_utils');

function visitSiblings(node, visitor) {
  var curNode = node;
  while (curNode = curNode.nextSibling) visitor(curNode);
  curNode = node;
  while (curNode = curNode.previousSibling) visitor(curNode);
}

var VerticalMenuPositioner = (function () {
  function VerticalMenuPositioner() {
    _classCallCheck(this, VerticalMenuPositioner);
  }

  _createClass(VerticalMenuPositioner, [{
    key: 'positionMenu',
    value: function positionMenu(menu, domRef, preferredPosition) {
      var availableSpaceAbove = this.getAvailableSpaceAbove(domRef),
          availableSpaceBelow = this.getAvailableSpaceBelow(domRef);

      var position = preferredPosition || this.determinePosition(menu, availableSpaceAbove, availableSpaceBelow);
      var availableSpace = position === 'down' ? availableSpaceBelow : availableSpaceAbove;
      this.adjustMenuSize(menu, availableSpace);
      this.adjustMenuWidth(menu);
      this.positionMenuDom(menu.dom, domRef, position);
      return position;
    }
  }, {
    key: 'getAvailableSpaceAbove',
    value: function getAvailableSpaceAbove(dom) {
      return dom.getBoundingClientRect().top;
    }
  }, {
    key: 'getAvailableSpaceBelow',
    value: function getAvailableSpaceBelow(dom) {
      return window.innerHeight - dom.getBoundingClientRect().bottom;
    }
  }, {
    key: 'adjustMenuSize',
    value: function adjustMenuSize(menu, availableSpace) {
      if (menu.domMenuPanel) {
        menu.domMenuPanel.style.height = '';
      }
      var offset = 0;
      var naturalMenuHeight = menu.getNaturalCoords().height;
      if (naturalMenuHeight > availableSpace) {
        menu.setHeight(availableSpace);
        visitSiblings(menu.domMenuPanel, function (sibling) {
          return offset += sibling.offsetHeight;
        });
        (0, _dom_utils.setOuterHeight)(menu.domMenuPanel, menu.dom.clientHeight - offset);
      } else {
        menu.resetHeight();
      }
    }
  }, {
    key: 'adjustMenuWidth',
    value: function adjustMenuWidth(menu) {
      if (!menu.activeMenu) return;
      var menuDom = (0, _dom_utils.domById)(menu.activeMenu.id);
      if (menuDom && menuDom.parentNode) {
        menuDom.style.width = menuDom.parentNode.clientWidth + 'px';
      }
    }
  }, {
    key: 'determinePosition',
    value: function determinePosition(menu, availableSpaceAbove, availableSpaceBelow) {
      var naturalMenuHeight = menu.getNaturalCoords().height;

      if (naturalMenuHeight <= availableSpaceBelow) {
        return 'down';
      } else if (naturalMenuHeight <= availableSpaceAbove) {
        return 'up';
      } else {
        return availableSpaceBelow > availableSpaceAbove ? 'down' : 'up';
      }
    }
  }, {
    key: 'positionMenuDom',
    value: function positionMenuDom(domMenu, domRef, position) {
      var offsetTop = document.body.scrollTop || document.documentElement.scrollTop,
          offsetLeft = document.body.scrollLeft || document.documentElement.scrollLeft,
          domRefCoords = domRef.getBoundingClientRect(),
          domRefLeft = Math.round(domRefCoords.left) + offsetLeft,
          domRefBottom = Math.round(domRefCoords.bottom) + offsetTop;

      domMenu.style.left = domRefLeft + 'px';

      if (position === 'down') {
        domMenu.style.top = domRefBottom + 'px';
        domMenu.style.bottom = '';
      } else {
        domMenu.style.top = '';
        domMenu.style.bottom = window.innerHeight - domRefBottom + domRef.offsetHeight + 'px';
      }
    }
  }]);

  return VerticalMenuPositioner;
})();

exports['default'] = VerticalMenuPositioner;
module.exports = exports['default'];
},{"./dom_utils":20}],23:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var ArrayIterator = (function () {
  function ArrayIterator(data) {
    _classCallCheck(this, ArrayIterator);

    this.data = data;
  }

  _createClass(ArrayIterator, [{
    key: 'forEach',
    value: function forEach(callback, context) {
      var data = typeof this.data === 'function' ? this.data() : this.data;
      for (var i = 0, len = data.length; i < len; i++) {
        if (callback.call(context, data[i], i, data) === false) break;
      }
    }
  }]);

  return ArrayIterator;
})();

exports['default'] = ArrayIterator;
module.exports = exports['default'];
},{}],24:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var HashIterator = (function () {
  function HashIterator(data) {
    _classCallCheck(this, HashIterator);

    this.data = data;
  }

  _createClass(HashIterator, [{
    key: 'forEach',
    value: function forEach(callback, context) {
      var hasOwnProperty = Object.prototype.hasOwnProperty;
      var data = typeof this.data === 'function' ? this.data() : this.data;
      for (var i in data) {
        if (hasOwnProperty.call(data, i)) {
          if (callback.call(context, data[i], i, data) === false) break;
        }
      }
    }
  }, {
    key: 'some',
    value: function some(callback, context) {
      var hasSome = false;
      this.forEach(function (value, key, data) {
        hasSome = callback.call(context, value, key, data);
        return !hasSome;
      });
      return hasSome;
    }
  }]);

  return HashIterator;
})();

exports['default'] = HashIterator;
module.exports = exports['default'];
},{}],25:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports['default'] = makePlugin;

var _core_utils = _dereq_('./core_utils');

var $ = window.$;

function buildMenuFromSelect(el, constructor, options, pluginName) {
  options.data = options.data ? [].concat(options.data) : [];

  var instance,
      selOption,
      cfgOptionsFromDom = $(el).data();

  // Resolve value
  options.value = el.value || options.value;

  // Resolve disabled
  options.disabled = el.hasAttribute('disabled') || options.disabled;

  // Add the menu data
  for (var i = 0, len = el.options.length; i < len; i++) {
    selOption = el.options[i];

    options.data[options.data.length] = {
      value: selOption.value,
      label: selOption.text,
      enabled: !selOption.disabled
    };
  }

  // Inject the element's class name
  if (el.className) {
    if (pluginName === 'zdSelectMenu') {
      if (options.clsSelectMenuRoot) {
        options.clsSelectMenuRoot += " " + el.className;
      } else {
        options.clsSelectMenuRoot = 'zd-selectmenu zd-selectmenu-root ' + el.className;
      }
    } else if (pluginName === 'zdComboSelectMenu') {
      if (options.clsSelectMenuRoot) {
        options.clsSelectMenuRoot += " " + el.className;
      } else {
        options.clsSelectMenuRoot = 'zd-combo-selectmenu zd-combo-selectmenu-root ' + el.className;
      }
    } else if (pluginName === 'zdMenu') {
      options.clsRoot = 'zd-menu-root ' + el.className;
    } else if (pluginName === 'zdTagMenu') {
      options.clsRoot = 'zd-tag-menu-root ' + el.className;
    }
  }

  // Add hidden input data
  options.proxyName = el.name || null;
  options.proxyId = el.id || null;

  // Add configuration options from the dom data attributes
  var cfgValue;
  for (var cfgName in cfgOptionsFromDom) {
    if (!cfgOptionsFromDom.hasOwnProperty(cfgName)) continue;
    cfgValue = $.trim(cfgOptionsFromDom[cfgName]);

    if (cfgValue === 'true') {
      cfgOptionsFromDom[cfgName] = true;
    } else if (cfgValue === 'false') {
      cfgOptionsFromDom[cfgName] = false;
    } else if (!isNaN(cfgValue)) {
      cfgOptionsFromDom[cfgName] = Number(cfgValue);
    }
  }

  (0, _core_utils.extend)(options, cfgOptionsFromDom);

  // Indicate the widget is to be insert by dom replacement of the element
  options.domRef = el;

  // Create the widget
  instance = new constructor(options);

  $(instance.dom).data(pluginName, instance);
  return instance;
}

function defaultBuilder(el, constructor, options, pluginName) {
  options.domHolder = el;
  var instance = new constructor(options);

  instance.on('destroy', function () {
    $(el).data(pluginName, null);
  });

  $(el).data(pluginName, instance);
  return instance;
}

function makePlugin(pluginName, constructor) {
  $.fn[pluginName] = function (options) {
    var instanceDoms;
    if (this.length === 0) return this;

    options = options == null ? {} : options;

    // Request either method invocation or collecting of property values.
    if (typeof options === 'string') {
      var args = arguments;
      var result;
      var calledAsGetter = false;

      this.each(function (index, el) {
        var instance = $(el).data(pluginName);
        if (!instance) return;

        if (typeof instance[options] === 'function') {
          // method invocation
          instance[options].apply(instance, [].slice.call(args, 1));
        } else if (args.length > 1) {
          // property setter
          instance[options] = args[1];
        } else if (args.length === 1) {
          // property getter
          result = instance[options];
          calledAsGetter = true;
          return false; // exit the loop to get the value of the first element
        }
      });

      return calledAsGetter ? result : this;

      // Request instantiation
    } else if (Object.prototype.toString.call(options) === "[object Object]") {
        instanceDoms = this.map(function (index, el) {
          if ($(el).data(pluginName)) return;

          var newOptions = (0, _core_utils.extend)({}, options); // clone options
          var isMenu = pluginName === "zdSelectMenu" || pluginName === "zdComboSelectMenu" || pluginName === "zdMenu" || pluginName === "zdTagMenu";

          if (!(newOptions.domHolder && newOptions.domHolderSelector)) {
            newOptions.domHolder = document.body;
          }

          if (el.tagName === "SELECT" && isMenu) {
            return buildMenuFromSelect(el, constructor, newOptions, pluginName).dom;
          } else {
            return defaultBuilder(el, constructor, newOptions, pluginName).dom;
          }
        });

        [].slice.call(instanceDoms).filter(Boolean);
      }

    return instanceDoms.length === 0 ? this : $(instanceDoms);
  };
}

module.exports = exports['default'];
},{"./core_utils":11}],26:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _dom_helpersDom_utils = _dereq_('../dom_helpers/dom_utils');

exports['default'] = {
  addScopeClass: function addScopeClass(cls) {
    (0, _dom_helpersDom_utils.addClass)(this.domHolder, cls);
    (0, _dom_helpersDom_utils.addClass)(this.dom, cls);
  },

  removeScopeClass: function removeScopeClass(cls) {
    (0, _dom_helpersDom_utils.removeClass)(this.domHolder, cls);
    (0, _dom_helpersDom_utils.removeClass)(this.dom, cls);
  },

  toggleScopeClass: function toggleScopeClass(cls, mod) {
    (0, _dom_helpersDom_utils.toggleClass)(this.domHolder, cls, mod);
    (0, _dom_helpersDom_utils.toggleClass)(this.dom, cls, mod);
  }
};
module.exports = exports['default'];
},{"../dom_helpers/dom_utils":20}],27:[function(_dereq_,module,exports){
// Examples:
//
// for DOM elements:
// this.onDom(document, 'click', e => {...}, true);
//
// for custom object events:
// this.on('update', (...args) => {...});

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

var ObservableMixin = {
  on: function on(event, callback) {
    this._observerRegistry = this._observerRegistry || {};
    var registry = this._observerRegistry;

    registry[event] = registry[event] || [];
    registry[event].push(callback);
    return this;
  },

  off: function off(event, callback) {
    if (!this._observerRegistry) return;

    if (event) {
      var eventRegistry = this._observerRegistry[event];
      if (!eventRegistry || eventRegistry && eventRegistry.length === 0) return;

      if (callback) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = eventRegistry[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var _step$value = _slicedToArray(_step.value, 2);

            var i = _step$value[0];
            var record = _step$value[1];

            if (record === callback) {
              eventRegistry.splice(i, 1);
              break;
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator['return']) {
              _iterator['return']();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      } else {
        this._observerRegistry[event] = null;
      }
    } else {
      this._observerRegistry = null;
    }
    return this;
  },

  onDom: function onDom(el, event, callback, phase) {
    phase = !!phase;
    this._observerDomRegistry = this._observerDomRegistry || [];

    el.addEventListener(event, callback, phase);
    this._observerDomRegistry.push([el, event, callback, phase]);
    return this;
  },

  offDom: function offDom(el, event, callback, phase) {
    if (!this._observerDomRegistry) return this;

    this._observerDomRegistry = this._observerDomRegistry.filter(function (record) {
      phase = el && event && callback ? !!phase : undefined;

      var hasMatch = record[0] === (el || record[0]) && record[1] === (event || record[1]) && record[2] === (callback || record[2]) && record[3] === (phase === undefined ? record[3] : phase);

      if (hasMatch) {
        record[0].removeEventListener(record[1], record[2], record[3]);
        return false;
      }
      return true;
    });
    return this;
  },

  trigger: function trigger(event, data) {
    var _this = this;

    if (!this._observerRegistry) return this;
    if (!this._observerRegistry[event]) return this;

    var triggerEvent = { type: event };
    this._observerRegistry[event].forEach(function (record) {
      return record.call(_this, triggerEvent, data);
    });
    if (this._observerRegistry['*']) {
      this._observerRegistry['*'].forEach(function (record) {
        return record.call(_this, triggerEvent, data);
      });
    }
    return this;
  }
};

exports['default'] = ObservableMixin;
module.exports = exports['default'];
},{}],28:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _core_utils = _dereq_('../core_utils');

exports['default'] = {
  setOptions: function setOptions(options, defaultOptions) {
    if (!this.options) {
      this.options = options || {};
      (0, _core_utils.defaults)(this, this.options);
    }
    (0, _core_utils.defaults)(this, defaultOptions);
  }
};
module.exports = exports['default'];
},{"../core_utils":11}],29:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _dom_helpersDom_utils = _dereq_('../dom_helpers/dom_utils');

exports['default'] = {
  // required
  getDomBaseForPositioning: function getDomBaseForPositioning() {},
  dom: null,
  position: null,
  clsPositionUp: null,
  verticalMenuPositioner: null,
  domBasePositionObserver: null,

  setPosition: function setPosition(position) {
    this.position = position;
    this.updatePositionCssClass();
  },

  updatePositionCssClass: function updatePositionCssClass() {
    var isUp = this.position === 'up';
    (0, _dom_helpersDom_utils.toggleClass)(this.dom, this.clsPositionUp, isUp);
    (0, _dom_helpersDom_utils.toggleClass)(this.domMenuHolder, this.clsPositionUp, isUp);
  },

  positionMenu: function positionMenu(menu, domRef, preferredPosition) {
    var position = this.verticalMenuPositioner.positionMenu(menu, domRef, preferredPosition);
    this.setPosition(position);
  },

  observeDomBasePositionOnce: function observeDomBasePositionOnce(onChange) {
    this.domBasePositionObserver.observeOnce(this.getDomBaseForPositioning(), onChange);
  },

  observeDomBasePosition: function observeDomBasePosition(onChange) {
    this.domBasePositionObserver.observe(this.getDomBaseForPositioning(), onChange);
  },

  stopObservingDomBasePosition: function stopObservingDomBasePosition() {
    this.domBasePositionObserver.stopObserving();
  }
};
module.exports = exports['default'];
},{"../dom_helpers/dom_utils":20}],30:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var LabelConcatenator = (function () {
  function LabelConcatenator(delimiter, fieldName) {
    _classCallCheck(this, LabelConcatenator);

    this.delimiter = delimiter || ' > ';
    this.fieldName = fieldName || 'concatenatedLabel';
  }

  _createClass(LabelConcatenator, [{
    key: 'parse',
    value: function parse(rootItem) {
      this._processItem(rootItem, '');
    }
  }, {
    key: '_processItem',
    value: function _processItem(item, concatLabelPrefix) {
      var childItem;

      if (item.role === 'uiBackLink' && item.role === 'uiLabel') {
        return;
      }

      item[this.fieldName] = concatLabelPrefix + item.label;

      if (item.menu) {
        for (var i = 0, len = item.menu.length; i < len; i++) {
          childItem = item.menu[i];
          if (!childItem.label) {
            continue;
          }

          this._processItem(childItem, item.role === 'root' ? '' : concatLabelPrefix + item.label + this.delimiter);
        }
      }
    }
  }]);

  return LabelConcatenator;
})();

exports['default'] = LabelConcatenator;
module.exports = exports['default'];
},{}],31:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _core_utils = _dereq_('../core_utils');

var _label_concatenator = _dereq_('./label_concatenator');

var _label_concatenator2 = _interopRequireDefault(_label_concatenator);

var menuModelParsers = {
  LabelConcatenator: _label_concatenator2['default']
};

var MenuDataParser = (function () {
  _createClass(MenuDataParser, null, [{
    key: 'menuModelParsers',
    get: function get() {
      return menuModelParsers;
    }
  }]);

  function MenuDataParser(targetObject, postParser, preParseFilter) {
    _classCallCheck(this, MenuDataParser);

    this.targetObject = targetObject || this;
    this.hashIds = {};
    this.hashValues = {};
    this.postParser = postParser;
    this.preParseFilter = preParseFilter || this.preParseFilter;
  }

  _createClass(MenuDataParser, [{
    key: 'reset',
    value: function reset() {
      this.hashIds = {};
      this.hashValues = {};
      this.rootItem = null;
    }

    // to be overriden if needed
  }, {
    key: 'preParseFilter',
    value: function preParseFilter(data) {
      return data;
    }
  }, {
    key: 'parse',
    value: function parse(treeData) {
      this.reset();

      treeData = this.preParseFilter(treeData);
      this.parseItem({
        role: 'root',
        children: Object.prototype.toString.call(treeData) === '[object Array]' ? treeData : [treeData]
      });
      if (this.postParser) {
        this.postParser(this.rootItem);
      }
      this.targetObject.hashIds = this.hashIds;
      this.targetObject.hashValues = this.hashValues;
      this.targetObject.rootItem = this.rootItem;
    }
  }, {
    key: 'parsePartial',
    value: function parsePartial(menu, children) {
      menu.length = 0;
      this.parseItem({ children: children }, menu.parentItem, menu);
    }
  }, {
    key: 'parseItem',
    value: function parseItem(optionItem, item, menu) {
      if (!optionItem) {
        return;
      }
      var newItem, enabled;

      if (optionItem.role === 'root') {
        this.rootItem = item = {
          role: 'root',
          id: (0, _core_utils.getUniqId)()
        };
      }

      if (!(optionItem.children && optionItem.children.length)) return;

      menu = menu || [];

      if (optionItem.role !== 'root') {
        newItem = {
          id: (0, _core_utils.getUniqId)(),
          role: 'uiBackLink',
          parentMenu: menu,
          parentItem: item,
          index: 0,
          enableHtmlEscape: true
        };
        this.hashIds[newItem.id] = newItem;
        menu[0] = newItem;

        item.role = "uiMenuLink";
      }

      item.menu = menu;
      menu.id = menu.id || item.id + '_mn';
      menu.parentItem = menu.parentItem || item;
      menu.parentMenu = menu.parentMenu || item.parentMenu;
      menu.data = menu.data || optionItem;

      var offset = menu.length;
      var childOptionItem;

      for (var i = offset, len = optionItem.children.length + offset; i < len; i++) {
        childOptionItem = optionItem.children[i - offset];
        if (!childOptionItem) {
          continue;
        }

        enabled = true;
        if (childOptionItem.hasOwnProperty('enabled')) enabled = childOptionItem.enabled !== false;

        newItem = {
          id: (0, _core_utils.getUniqId)(),
          data: childOptionItem,
          type: childOptionItem.type,
          label: childOptionItem.label,
          enabled: enabled,
          index: i,
          parentMenu: menu,
          parentItem: item,
          enableHtmlEscape: childOptionItem.enableHtmlEscape !== false
        };

        menu[i] = newItem;

        this.hashIds[newItem.id] = newItem;

        if ('id' in childOptionItem || childOptionItem.value !== undefined) {
          var value = childOptionItem.value == null ? childOptionItem.id : childOptionItem.value;
          newItem.value = value == null ? this.defaultValue : value;
          this.hashValues[newItem.value] = newItem;
        }

        childOptionItem.children && childOptionItem.children.length && this.parseItem(childOptionItem, newItem);

        if (newItem.role == null && newItem.value === undefined) {
          newItem.role = "uiLabel";
        }
      }
    }
  }]);

  return MenuDataParser;
})();

exports['default'] = MenuDataParser;
module.exports = exports['default'];
},{"../core_utils":11,"./label_concatenator":30}],32:[function(_dereq_,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _core_utils = _dereq_('../core_utils');

var HighlightingRenderer = (function () {
  function HighlightingRenderer() {
    _classCallCheck(this, HighlightingRenderer);

    this.reHighlightWord = null;
    this.lastWord = null;
  }

  _createClass(HighlightingRenderer, [{
    key: 'render',
    value: function render(str, highlightWord, clsHighlight) {
      var _this = this;

      highlightWord = highlightWord || '';

      if (this.lastWord !== highlightWord) {
        this.reHighlightWord = new RegExp('(' + (0, _core_utils.escapeRegExp)(highlightWord) + ')', 'ig');
      }

      if (!this.reHighlightWord.test(str)) {
        return str;
      }

      return str.split(this.reHighlightWord).map(function (word) {
        if (_this.reHighlightWord.test(word)) {
          return '<span class="' + clsHighlight + '">' + (0, _core_utils.escapeHtml)(word) + '</span>';
        } else {
          return (0, _core_utils.escapeHtml)(word);
        }
      }).join('');
    }
  }]);

  return HighlightingRenderer;
})();

exports['default'] = HighlightingRenderer;
module.exports = exports['default'];
},{"../core_utils":11}]},{},[3])
(3)
});
}});

(function(global){var babelHelpers=global.babelHelpers={};babelHelpers.inherits=function(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass)}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)subClass.__proto__=superClass};babelHelpers.defaults=function(obj,defaults){var keys=Object.getOwnPropertyNames(defaults);for(var i=0;i<keys.length;i++){var key=keys[i];var value=Object.getOwnPropertyDescriptor(defaults,key);if(value&&value.configurable&&obj[key]===undefined){Object.defineProperty(obj,key,value)}}return obj};babelHelpers.createClass=function(){function defineProperties(target,props){for(var key in props){var prop=props[key];prop.configurable=true;if(prop.value)prop.writable=true}Object.defineProperties(target,props)}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor}}();babelHelpers.createComputedClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var prop=props[i];prop.configurable=true;if(prop.value)prop.writable=true;Object.defineProperty(target,prop.key,prop)}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor}}();babelHelpers.applyConstructor=function(Constructor,args){var instance=Object.create(Constructor.prototype);var result=Constructor.apply(instance,args);return result!=null&&(typeof result=="object"||typeof result=="function")?result:instance};babelHelpers.taggedTemplateLiteral=function(strings,raw){return Object.freeze(Object.defineProperties(strings,{raw:{value:Object.freeze(raw)}}))};babelHelpers.taggedTemplateLiteralLoose=function(strings,raw){strings.raw=raw;return strings};babelHelpers.interopRequire=function(obj){return obj&&obj.__esModule?obj["default"]:obj};babelHelpers.toArray=function(arr){return Array.isArray(arr)?arr:Array.from(arr)};babelHelpers.toConsumableArray=function(arr){if(Array.isArray(arr)){for(var i=0,arr2=Array(arr.length);i<arr.length;i++)arr2[i]=arr[i];return arr2}else{return Array.from(arr)}};babelHelpers.slicedToArray=function(arr,i){if(Array.isArray(arr)){return arr}else if(Symbol.iterator in Object(arr)){var _arr=[];for(var _iterator=arr[Symbol.iterator](),_step;!(_step=_iterator.next()).done;){_arr.push(_step.value);if(i&&_arr.length===i)break}return _arr}else{throw new TypeError("Invalid attempt to destructure non-iterable instance")}};babelHelpers.objectWithoutProperties=function(obj,keys){var target={};for(var i in obj){if(keys.indexOf(i)>=0)continue;if(!Object.prototype.hasOwnProperty.call(obj,i))continue;target[i]=obj[i]}return target};babelHelpers.hasOwn=Object.prototype.hasOwnProperty;babelHelpers.slice=Array.prototype.slice;babelHelpers.bind=Function.prototype.bind;babelHelpers.defineProperty=function(obj,key,value){return Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true})};babelHelpers.asyncToGenerator=function(fn){return function(){var gen=fn.apply(this,arguments);return new Promise(function(resolve,reject){var callNext=step.bind(null,"next");var callThrow=step.bind(null,"throw");function step(key,arg){try{var info=gen[key](arg);var value=info.value}catch(error){reject(error);return}if(info.done){resolve(value)}else{Promise.resolve(value).then(callNext,callThrow)}}callNext()})}};babelHelpers.interopRequireWildcard=function(obj){return obj&&obj.__esModule?obj:{"default":obj}};babelHelpers._typeof=function(obj){return obj&&obj.constructor===Symbol?"symbol":typeof obj};babelHelpers._extends=Object.assign||function(target){for(var i=1;i<arguments.length;i++){var source=arguments[i];for(var key in source){if(Object.prototype.hasOwnProperty.call(source,key)){target[key]=source[key]}}}return target};babelHelpers.get=function get(object,property,receiver){var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined}else{return get(parent,property,receiver)}}else if("value"in desc&&desc.writable){return desc.value}else{var getter=desc.get;if(getter===undefined){return undefined}return getter.call(receiver)}};babelHelpers.set=function set(object,property,value,receiver){var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent!==null){return set(parent,property,value,receiver)}}else if("value"in desc&&desc.writable){return desc.value=value}else{var setter=desc.set;if(setter!==undefined){return setter.call(receiver,value)}}};babelHelpers.classCallCheck=function(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function")}};babelHelpers.objectDestructuringEmpty=function(obj){if(obj==null)throw new TypeError("Cannot destructure undefined")};babelHelpers.temporalUndefined={};babelHelpers.temporalAssertDefined=function(val,name,undef){if(val===undef){throw new ReferenceError(name+" is not defined - temporal dead zone")}return true};babelHelpers.selfGlobal=typeof global==="undefined"?self:global})(typeof global==="undefined"?self:global);
require.define({"z-component": function(exports, require, module) {
"use strict";

var _slicedToArray = function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { var _arr = []; for (var _iterator = arr[Symbol.iterator](), _step; !(_step = _iterator.next()).done;) { _arr.push(_step.value); if (i && _arr.length === i) break; } return _arr; } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } };

var _prototypeProperties = function (child, staticProps, instanceProps) { if (staticProps) Object.defineProperties(child, staticProps); if (instanceProps) Object.defineProperties(child.prototype, instanceProps); };

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var slice = Array.prototype.slice;

function addListeners(targets, eventName, listener, useCapture) {
  var eventListeners = this._eventListeners;

  targets.forEach(function (target) {
    eventListeners.push([target, eventName, listener, useCapture]);
    target.addEventListener(eventName, listener, useCapture);
  });
}

/**
 * Zendesk component template.
 *
 * @constructor
 *
 * @param {DocumentFragment} fragment Template fragment for content
 */
function Template(fragment) {
  this.fragment = fragment;
}

Template.prototype = Object.defineProperties({}, {
  content: {
    get: function () {
      return this.fragment.cloneNode(true);
    },
    enumerable: true,
    configurable: true
  }
});

/**
 * Zendesk base component
 */

var ZComponent = (function () {
  /**
   * @constructor
   *
   * @param {object} options Options for component.
   * @param {Node} [options.el] Base element for component.
   */

  function ZComponent() {
    var options = arguments[0] === undefined ? {} : arguments[0];

    _classCallCheck(this, ZComponent);

    var el = options.el;

    this.id = options.id;
    this.classList = this.classList.concat(options.classList || []);

    if (options.children) {
      this.children = options.children;
    }

    if (!el) {
      el = this.constructor.template.content.firstChild;
    }

    this.setElement(el);
  }

  _prototypeProperties(ZComponent, {
    template: {

      /**
       * Gets template for component.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        var fragment = document.createDocumentFragment(),
            template;

        fragment.appendChild(document.createElement(this.tagName));
        template = new Template(fragment);

        return template;
      },
      configurable: true
    },
    tagName: {

      /**
       * Gets tag name for component.
       *
       * @type {string[]};
       */

      get: function () {
        return "div";
      },
      configurable: true
    },
    classList: {

      /**
       * List of default class names for component.
       *
       * @abstract
       * @static
       * @type {string[]}
       */

      get: function () {
        return [];
      },
      configurable: true
    }
  }, {
    $: {

      /**
       * Alias to querySelector.
       *
       * @param {string} selector Selector string
       * @type {Node}
       */

      value: function $() {
        return this.el.querySelector.apply(this.el, arguments);
      },
      writable: true,
      configurable: true
    },
    $$: {

      /**
       * Alias to querySelectorAll.
       *
       * @param {string} selector Selector string
       * @type {NodeList}
       */

      value: function $$() {
        return this.el.querySelectorAll.apply(this.el, arguments);
      },
      writable: true,
      configurable: true
    },
    id: {

      /**
       * Gets id for component.
       */

      get: function () {
        return this._id || "";
      },

      /**
       * Sets id for component.
       *
       * @type {string}
       */
      set: function (val) {
        this._id = val;

        if (this.el && (val || this.el.id)) {
          this.el.id = val;
        }
      },
      configurable: true
    },
    classList: {

      /**
       * Gets list class names for component.
       *
       * @type {string[]}
       */

      get: function () {
        return this._classList || [];
      },

      /**
       * Sets list of class names for component. Concatenates to default list.
       *
       * @param {string[]} val Array of class names.
       */
      set: function (val) {
        this._classList = this.constructor.classList.concat(val || []);
      },
      configurable: true
    },
    events: {

      /**
       * DOM events mapping to event handlers.
       *
       * @abstract
       * @type {object}
       */

      get: function () {
        return {};
      },
      configurable: true
    },
    children: {

      /**
       * Gets children for component.
       *
       * @abstract
       * @type {NodeList}
       */

      get: function () {
        return this._children;
      },

      /**
       * Sets children for component.
       *
       * @abstract
       * @type {HTMLCollection|Node[]}
       */
      set: function () {
        var val = arguments[0] === undefined ? [] : arguments[0];

        this._children = Array.isArray(val) ? val : [].slice.call(val);
      },
      configurable: true
    },
    setElement: {

      /**
       * Sets up component element.
       *
       * @param {Node} el Component element.
       */

      value: function setElement(el) {
        var classList = el.classList;

        this.undelegateEvents();
        this.el = el;
        this.el.id = this.id;
        classList.add.apply(classList, this.classList);
        this.delegateEvents();
      },
      writable: true,
      configurable: true
    },
    delegateEvents: {

      /**
       * Delegates events (keys) from events property to event handlers (values).
       */

      value: function delegateEvents() {
        var eventListeners = this._eventListeners,
            events = this.events,
            el = this.el;

        if (!eventListeners) {
          eventListeners = this._eventListeners = [];
        }

        for (var name in events) {
          var splat = name.split(" "),
              eventName = splat[0],
              selector = splat[1],
              targets = selector ? slice.call(this.$$(selector)) : [el],
              listener = this[events[name]],
              useCapture = false;

          if (!listener) {
            continue;
          }

          if (eventName[eventName.length - 1] === "^") {
            eventName = eventName.substring(0, eventName.length - 1);
            useCapture = true;
          }

          listener = listener.bind(this);
          addListeners.call(this, targets, eventName, listener, useCapture);
        }
      },
      writable: true,
      configurable: true
    },
    undelegateEvents: {

      /**
       * Undelegates all events from events property.
       */

      value: function undelegateEvents() {
        if (!this._eventListeners) {
          return;
        }

        while (this._eventListeners.length > 0) {
          var data = this._eventListeners.pop();
          var _data = _slicedToArray(data, 4);

          var element = _data[0];
          var eventName = _data[1];
          var handler = _data[2];
          var useCapture = _data[3];

          element.removeEventListener(eventName, handler, useCapture);
        }
      },
      writable: true,
      configurable: true
    },
    appendTo: {

      /**
       * Appends component to target element.
       *
       * @param {Node} target Target element for appending.
       */

      value: function appendTo(target) {
        target.appendChild(this.el);
      },
      writable: true,
      configurable: true
    },
    prependTo: {

      /**
       * Prepends component to target element.
       *
       * @param {Node} target Target element for prepending.
       */

      value: function prependTo(target) {
        target.insertBefore(this.el, target.firstChild);
      },
      writable: true,
      configurable: true
    },
    remove: {

      /**
       * Removes component from DOM.
       */

      value: function remove() {
        var el = this.el;

        this.undelegateEvents();

        if (el.parentNode) {
          el.parentNode.removeChild(el);
        }
      },
      writable: true,
      configurable: true
    }
  });

  return ZComponent;
})();

module.exports = ZComponent;

/**
 * Gets template contents (clone of template fragment).
 *
 * @type {DocumentFragment}
 */

}});

require.define({"lotus-client": function(exports, require, module) {
/*globals $ */


var Promise = window.Promise || function (cb) {
  var promise = $.Deferred();
  cb(promise.resolve, promise.reject);
  return promise.promise();
};

function origin(location) {
  return location.origin || location.protocol + "//" + location.hostname + (location.port ? ':' + location.port : '');
}

function sendXFrameMessage(msg) {
  window.top.postMessage(JSON.stringify(msg), origin(window.location));
}

function isValidOrigin(origin) {
  var loc = window.location;
  var expectedOrigin = loc.protocol + '//' + loc.host;
  return origin === expectedOrigin;
}

function growl(message, action, options) {
  sendXFrameMessage({
    target: 'growl',
    memo: {
      action:  action,
      message: message,
      options: options
    }
  });
}

function transitionTo(link) {
  sendXFrameMessage({
    target: 'route',
    source: 'admin-iframe',
    memo: {
      hash: link
    }
  });
}




var showingModal = false,
    deferred = null;

function confirmationResponseHandler(resolve, reject, event) {
  if(!isValidOrigin(event.origin) || !event.data) {
    return;
  }

  var data = null;
  try {
    data = JSON.parse(event.data);
  } catch(e) {
    return;
  }

  if(data.target !== "confirmation-modal") {
    return;
  }

  if(data.confirmed) {
    resolve();
  } else {
    reject();
  }

  showingModal = false;
}

function showConfirmationModal(options) {

  if(showingModal) {
    throw new Error("Can't show multiple confirmation modals");
  }

  if (window.top === window) {
    if (confirm(options.message)) {
      return Promise.resolve("confirmed");
    } else {
      return Promise.reject("rejected");
    }
  }

  showingModal = true;

  sendXFrameMessage({
    target: 'confirmation-modal',
    memo: {
      title: options.title,
      message: options.message,
      confirmLabel: options.confirmLabel,
      cancelLabel: options.cancelLabel
    }
  });

  var handler;
  return new Promise(function(resolve, reject) {
    handler = confirmationResponseHandler.bind(null, resolve, reject);
    window.addEventListener('message', handler, false);
  }).then(function() {
    window.removeEventListener('message', handler, false);
  });
}

module.exports = {
  configure: function(options) {
    if(options.Promise) {
      Promise = options.Promise;
    }
  },
  showConfirmationModal: showConfirmationModal,
  growl: growl,
  transitionTo: transitionTo
};

}});

require.define({"index": function(exports, require, module) {
"use strict";

var LotusClient = babelHelpers.interopRequire(require("lotus-client"));
var Page = babelHelpers.interopRequire(require("components/page"));
var api = babelHelpers.interopRequire(require("lib/api"));
var I18n = babelHelpers.interopRequire(require("lib/i18n"));
var store = babelHelpers.interopRequire(require("lib/store"));
var DefinitionSet = babelHelpers.interopRequire(require("models/definition-set"));
var Policies = babelHelpers.interopRequire(require("models/policies"));
var Priorities = babelHelpers.interopRequire(require("models/priorities"));
var Settings = babelHelpers.interopRequire(require("models/settings"));
module.exports = {
  init: function init() {
    var meta = document.querySelector("meta[name=\"csrf-token\"]");

    ES6Promise.polyfill();
    LotusClient.configure({ Promise: Promise });

    if (meta) api.CSRF_TOKEN = meta.getAttribute("content");

    this.store = store;

    store.register("definition-set", DefinitionSet);
    store.register("policies", Policies);
    store.register("priorities", Priorities);
    store.register("settings", Settings);

    store.lookup("definition-set").fetch();
    store.lookup("priorities").fetch();

    var wrapper = document.createElement("div");

    wrapper.className = "wrapper";

    var settings = store.lookup("settings");

    settings.fetch().then(function () {
      /* jshint ignore:start */
      if (settings.get("active_features").business_hours) {
        wrapper.classList.add("has-business-hours");
      }
      /* jshint ignore:end */
    });

    var page = this.page = new Page({
      link: {
        href: I18n.t("txt.admin.slas.page_link"),
        target: "_blank",
        text: I18n.t("txt.admin.learn_more")
      }
    });

    page.appendTo(wrapper);

    document.body.appendChild(wrapper);
  }
};
}});

require.define({"components/page": function(exports, require, module) {
"use strict";

var ZComponent = babelHelpers.interopRequire(require("z-component"));
var I18n = babelHelpers.interopRequire(require("./../lib/i18n"));
var Section = babelHelpers.interopRequire(require("./section"));

var Page = (function (_ZComponent) {
  function Page(options) {
    babelHelpers.classCallCheck(this, Page);

    var link = options.link;

    babelHelpers.get(Object.getPrototypeOf(Page.prototype), "constructor", this).call(this, options);
    this.title = I18n.t("txt.admin.slas.page_heading");
    this.description = I18n.t("txt.admin.slas.page_description");

    if (link) {
      var anchor = document.createElement("a");

      anchor.classList.add("page__description__link");
      anchor.href = link.href;
      anchor.target = link.target;
      anchor.textContent = link.text;

      this.$(".page__description").appendChild(anchor);
    }

    this.section = new Section({
      classList: ["page__section"]
    });
    this.section.appendTo(this.el);
  }

  babelHelpers.inherits(Page, _ZComponent);
  babelHelpers.createClass(Page, {
    title: {
      get: function () {
        return this._title || "";
      },
      set: function (val) {
        this._title = val;
        this.$(".page__heading").textContent = this._title;
      }
    },
    description: {
      get: function () {
        return this._description || "";
      },
      set: function (val) {
        this._description = val;
        this.$(".page__description").textContent = this._description;
      }
    }
  }, {
    classList: {
      get: function () {
        return ["page"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template;

        template.fragment.firstChild.innerHTML = "<h1 class=\"page__heading u-kilo u-mb u-fg-oil\"></h1>\n       <p class=\"page__description\"></p>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return Page;
})(ZComponent);

module.exports = Page;
}});

require.define({"components/section": function(exports, require, module) {
"use strict";

var LotusClient = babelHelpers.interopRequire(require("lotus-client"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var I18n = babelHelpers.interopRequire(require("lib/i18n"));
var store = babelHelpers.interopRequire(require("lib/store"));
var Policy = babelHelpers.interopRequire(require("models/policy"));
var ZLoader = babelHelpers.interopRequire(require("./z/loader"));
var ZJumbotron = babelHelpers.interopRequire(require("./z/jumbotron"));
var PolicyCapsules = babelHelpers.interopRequire(require("./policy/capsules"));

var Section = (function (_ZComponent) {
  function Section(options) {
    var _this = this;

    babelHelpers.classCallCheck(this, Section);

    var priorities = this.priorities = store.lookup("priorities"),
        policies = this.policies = store.lookup("policies");

    this._definitions = store.lookup("definition-set");

    babelHelpers.get(Object.getPrototypeOf(Section.prototype), "constructor", this).call(this, options);

    this.heading = this.$(".section__heading");
    this.title = I18n.t("txt.admin.slas.policies_heading");
    this.button = this.$(".section__cta");
    this.cta = I18n.t("txt.admin.slas.add_policy");

    this.loader = new ZLoader({
      classList: ["loader--small"]
    });
    this.loader.loading = true;
    this.loader.appendTo(this.heading);

    policies.on("remove", this.updatePoliciesState, this);

    priorities.fetch().then(function () {
      return _this.prioritiesDidLoad();
    })["catch"](function () {
      return _this.didError();
    });
  }

  babelHelpers.inherits(Section, _ZComponent);
  babelHelpers.createClass(Section, {
    events: {
      get: function () {
        return {
          "click .section__cta": "onAddPolicy"
        };
      }
    },
    title: {
      get: function () {
        return this._title || "";
      },
      set: function (val) {
        this._title = val;
        this.heading.firstChild.textContent = this.title;
      }
    },
    cta: {
      get: function () {
        return this._cta || "";
      },
      set: function (val) {
        this._cta = val;
        this.button.textContent = this.cta;
      }
    },
    onAddPolicy: {
      value: function onAddPolicy() {
        this.policies.add(new Policy());
      }
    },
    disableAdd: {
      value: function disableAdd(policy) {
        var _this = this;

        var didSave;

        this.button.setAttribute("disabled", "true");

        didSave = function () {
          _this.button.removeAttribute("disabled");
          policy.off("saved", didSave);
          policy.off("destroyed", didSave);
        };

        policy.on("saved", didSave);
        policy.on("destroyed", didSave);
      }
    },
    gotoTicketFields: {
      value: function gotoTicketFields() {
        LotusClient.transitionTo("/admin/ticket_fields");
      }
    },
    showAsEnabled: {
      value: function showAsEnabled() {
        var _this = this;

        this.capsules = new PolicyCapsules({
          items: this.policies
        });
        this.capsules.appendTo(this.el);

        this.button.setAttribute("disabled", "true");

        this._definitions.fetch().then(function () {
          _this.policies.fetch().then(function () {
            return _this.policiesDidLoad();
          })["catch"](function () {
            return _this.didError();
          });
        });
      }
    },
    showAsDisabled: {
      value: function showAsDisabled() {
        this.loader.loading = false;

        this.setJumbotron({
          image: "z-jumbotron__image--clock",
          heading: I18n.t("txt.admin.slas.not_setup_heading"),
          subheading: I18n.t("txt.admin.slas.no_priorities_subheading"),
          cta: I18n.t("txt.admin.slas.activate_priority"),
          onClick: this.gotoTicketFields
        });
      }
    },
    updatePoliciesState: {
      value: function updatePoliciesState() {
        var hasPolicies = this.policies.length > 0;

        this.el.classList[hasPolicies ? "add" : "remove"]("section--active");
      }
    },
    setJumbotron: {
      value: function setJumbotron(options) {
        var jumbotron = this.jumbotron;

        if (jumbotron) {
          jumbotron.reset(options);
          jumbotron.off();
        } else {
          jumbotron = this.jumbotron = new ZJumbotron(options);

          jumbotron.appendTo(this.el);
        }

        jumbotron.on("click", options.onClick);
      }
    },
    remove: {
      value: function remove() {
        this.policies.off("remove", this.updatePoliciesState, this);
        this.policies.off("add", this.disableAdd, this);

        if (this.jumbotron) {
          this.jumbotron.remove();
        }

        babelHelpers.get(Object.getPrototypeOf(Section.prototype), "remove", this).call(this);
      }
    },
    prioritiesDidLoad: {
      value: function prioritiesDidLoad() {
        if (this.priorities.active) {
          this.showAsEnabled();
        } else {
          this.showAsDisabled();
        }
      }
    },
    policiesDidLoad: {
      value: function policiesDidLoad() {
        var _this = this;

        var didClick;

        this.updatePoliciesState();

        this.button.removeAttribute("disabled");
        this.loader.loading = false;

        didClick = function () {
          _this.onAddPolicy();
          _this.updatePoliciesState();
        };

        this.policies.on("add", this.disableAdd, this);

        this.setJumbotron({
          image: "z-jumbotron__image--clock",
          heading: I18n.t("txt.admin.slas.not_setup_heading"),
          subheading: I18n.t("txt.admin.slas.no_policies_subheading"),
          cta: I18n.t("txt.admin.slas.add_policy"),
          onClick: didClick
        });
      }
    },
    didError: {
      value: function didError() {
        this.loader.loading = false;
      }
    }
  }, {
    tagName: {
      get: function () {
        return "section";
      }
    },
    classList: {
      get: function () {
        return ["section"];
      }
    },
    template: {
      get: function () {
        var template = Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);

        template.fragment.firstChild.innerHTML = "<h3 class=\"section__heading u-delta u-semibold\"><span></span></h3>\n       <button class=\"c-btn section__cta\"></button>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return Section;
})(ZComponent);

module.exports = Section;
}});

require.define({"lib/api": function(exports, require, module) {
"use strict";

var errors = babelHelpers.interopRequire(require("models/errors"));

function errorHandler(e) {
  console.error(e);

  return Promise.reject(e);
}

var api = {
  request: function request(method, url, body) {
    var options = {
      method: method,
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-CSRF-Token": this.CSRF_TOKEN
      }
    };

    if (body) {
      options.body = JSON.stringify(body);
    }

    return fetch(url, options).then(function (res) {
      var status = res.status,
          HTTPError;

      if (!res.ok) {
        HTTPError = errors[status] || errors.unknown;

        return Promise.reject(new HTTPError(res.url, res));
      }

      return res.text();
    }).then(function (text) {
      return text.length > 1 ? JSON.parse(text) : null;
    })["catch"](errorHandler);
  },

  transport: {
    ajax: function ajax(options) {
      var promise = api.request(options.type, options.url).then(options.success);

      /**
       * Cancellable requests are still being discussed for the WhatWG fetch
       * standard.
       *
       * References:
       * - https://github.com/github/fetch/issues/33
       * - https://github.com/slightlyoff/ServiceWorker/issues/625
       */
      promise.abort = function () {};

      return promise;
    }
  }
};

module.exports = api;
}});

require.define({"lib/i18n": function(exports, require, module) {
"use strict";

function interpolate(str, options) {
  return str.replace(/\{{3}(\w+)\}{3}|\{\{(\w+)\}\}|\{(\w+)\}/g, function (match, three, two, one) {
    var submatch = three || two || one;

    return options[submatch];
  });
}

module.exports = {
  translations: {},

  t: function t(key, options) {
    var str = this.translations[key];

    return options ? interpolate(str, options) : str;
  }
};
}});

require.define({"lib/insertion-observer": function(exports, require, module) {
"use strict";

var slice = Array.prototype.slice;

var queue = [],
    observer;

module.exports = {
  observe: function observe(component, callback) {
    var _this = this;

    if (!observer) {
      observer = new MutationObserver(function (mutations) {
        return _this.handleMutations(mutations);
      });
      observer.observe(document.body, { subtree: true, childList: true });
    }

    queue.push([component, callback]);
  },

  checkInsert: function checkInsert(node) {
    queue = queue.reduce(function (_q, item) {
      var _item = babelHelpers.slicedToArray(item, 2);

      var component = _item[0];
      var callback = _item[1];

      if (node.hasChildNodes() && node.contains(component.el)) {
        callback.call(component);
      } else {
        _q.push(item);
      }

      return _q;
    }, []);

    if (observer && queue.length === 0) {
      observer.disconnect();
      observer = null;
    }
  },

  handleMutations: function handleMutations(mutations) {
    var _this = this;

    mutations.forEach(function (mutation) {
      return _this.handleMutation(mutation);
    });
  },

  handleMutation: function handleMutation(mutation) {
    var _this = this;

    var added = mutation.addedNodes;

    if (!added || added.length === 0) {
      return;
    }

    slice.call(added).forEach(function (node) {
      return _this.checkInsert(node);
    });
  }
};
}});

require.define({"lib/store": function(exports, require, module) {
"use strict";

var registry = {};

var store = {
  register: function register(name, Klass) {
    var instance;

    Object.defineProperty(registry, name, {
      get: function get() {
        if (!instance) {
          instance = new Klass();
        }

        return instance;
      },
      configurable: true,
      enumerable: true
    });
  },

  lookup: function lookup(name) {
    return registry[name];
  },

  unregister: function unregister(name) {
    var _this = this;

    if (typeof name === "undefined") {
      Object.keys(registry).forEach(function (name) {
        return _this.unregister(name);
      });

      return;
    }

    Object.defineProperty(registry, name, {
      value: null,
      configurable: true
    });
  }
};

module.exports = store;
}});

require.define({"lib/utils": function(exports, require, module) {
"use strict";

exports.copy = copy;
exports.mixin = mixin;
exports.isEmpty = isEmpty;
Object.defineProperty(exports, "__esModule", {
  value: true
});

function copy(obj) {
  var type = Object.prototype.toString.call(obj);
  var copied = obj;
  var propertyDescriptor;

  if (type === "[object Object]") {
    copied = Object.getOwnPropertyNames(obj).reduce(function (c, key) {
      propertyDescriptor = Object.getOwnPropertyDescriptor(obj, key);
      propertyDescriptor.value = copy(propertyDescriptor.value);

      Object.defineProperty(c, key, propertyDescriptor);

      return c;
    }, {});
  } else if (type === "[object Array]") {
    copied = obj.map(function (item) {
      return copy(item);
    });
  }

  return copied;
}

function mixin() {
  var args = Array.prototype.slice.call(arguments),
      obj = args.shift();

  args.forEach(function (props) {
    Object.keys(props).forEach(function (property) {
      Object.defineProperty(obj, property, Object.getOwnPropertyDescriptor(props, property));
    });
  });

  return obj;
}

function isEmpty() {
  for (var _len = arguments.length, objs = Array(_len), _key = 0; _key < _len; _key++) {
    objs[_key] = arguments[_key];
  }

  return objs.every(function (obj) {
    return obj === null || obj === undefined || obj.length === 0;
  });
}
}});

require.define({"models/account": function(exports, require, module) {
"use strict";

var Model = babelHelpers.interopRequire(require("./model"));

var mixin = require("lib/utils").mixin;

var Fetchable = babelHelpers.interopRequire(require("mixins/fetchable"));

var Account = (function (_Model) {
  function Account() {
    babelHelpers.classCallCheck(this, Account);

    babelHelpers.get(Object.getPrototypeOf(Account.prototype), "constructor", this).apply(this, arguments);

    this.isNew = false;
  }

  babelHelpers.inherits(Account, _Model);
  babelHelpers.createClass(Account, {
    url: {
      get: function () {
        return "/api/v2/account.json";
      }
    },
    sideloads: {
      get: function () {
        return ["deployments"];
      }
    },
    parse: {
      value: function parse() {
        var json = arguments[0] === undefined ? {} : arguments[0];

        return json.account;
      }
    }
  });
  return Account;
})(Model);

mixin(Account.prototype, Fetchable, {
  didFetch: function didFetch(json) {
    Fetchable.didFetch.call(this, json);

    var features = json.deployments;
    var data = mixin({ features: features }, this.parse(json));

    return this.reset(data);
  }
});

module.exports = Account;
}});

require.define({"models/collection": function(exports, require, module) {
"use strict";

var mixin = require("./../lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("./../mixins/eventable"));
var Fetchable = babelHelpers.interopRequire(require("mixins/fetchable"));
var Model = babelHelpers.interopRequire(require("./model"));

/**
 * Basic collection.
 *
 * @class
 */

var Collection = (function (_Array) {
  /**
   * @constructor
   * @param {object[]} [models]
   */

  function Collection() {
    var models = arguments[0] === undefined ? [] : arguments[0];
    babelHelpers.classCallCheck(this, Collection);

    this.add.apply(this, models);
  }

  babelHelpers.inherits(Collection, _Array);
  babelHelpers.createClass(Collection, {
    url: {

      /**
       * URL for collection API.
       *
       * @abstract
       * @type {string}
       */

      get: function () {
        throw new Error("#url is abstract property");
      }
    },
    sideloads: {
      get: function () {
        return [];
      }
    },
    add: {

      /**
       * Adds model(s) to collection.
       *
       * @param {...Model|object} models
       * @type {Model[]}
       */

      value: function add() {
        var _this = this;

        var CollectionModel = this.constructor.Model || Model;
        var models = this.slice.call(arguments).map(function (model) {
          if (!(model instanceof CollectionModel)) {
            model = new CollectionModel(model, false);
          }

          _this._cache = _this._cache || {};
          _this._cache[model.get("id")] = model;

          model.collection = _this;
          model.on("destroyed", _this.remove, _this);
          _this.push(model);
          _this.emit("add", model);

          return model;
        });

        return models;
      }
    },
    remove: {

      /**
       * Removes model from collection if it exists.
       *
       * @type {Model}
       */

      value: function remove(model) {
        var index = this.indexOf(model),
            removed;

        if (index === -1) {
          return;
        }

        removed = this.splice(index, 1);
        removed.collection = null;
        this.emit("remove", removed);

        return removed;
      }
    },
    clear: {

      /**
       * Prunes collection.
       */

      value: function clear() {
        var model;

        while (this.length > 0) {
          model = this.pop();
          model.collection = null;
          model.isDestroyed = true;
          model.destroy();
        }
      }
    },
    load: {

      /**
       * Reloads collection with API data.
       *
       * @type {Model[]}
       */

      value: function load(data) {
        var models;

        this.clear();
        models = this.add.apply(this, this.parse(data));
        this.emit("load", this);

        return models;
      }
    },
    parse: {

      /**
       * Parses API response.
       *
       * @abstract
       * @type {object}
       */

      value: function parse(json) {
        return json;
      }
    },
    lookup: {
      value: function lookup(id) {
        return this._cache && this._cache[id];
      }
    }
  }, {
    Model: {

      /**
       * Collection model type.
       *
       * @static
       * @type {Model}
       */

      get: function () {
        return Model;
      }
    }
  });
  return Collection;
})(Array);

mixin(Collection.prototype, Eventable, Fetchable, {
  didFetch: function didFetch(json) {
    Fetchable.didFetch.apply(this, arguments);
    this.req = null;

    return this.load(json);
  }
});

module.exports = Collection;
}});

require.define({"models/definition-set": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var Fetchable = babelHelpers.interopRequire(require("mixins/fetchable"));
var Definitions = babelHelpers.interopRequire(require("models/definitions"));

function DefinitionSet() {
  this.all = new Definitions();
  this.any = new Definitions();
}

mixin(DefinitionSet.prototype, Eventable, Fetchable, Object.defineProperties({

  didFetch: function didFetch(json) {
    var definitions = json.definitions || {};

    this.all.clear();
    this.any.clear();
    this.all.push.apply(this.all, definitions.all);
    this.any.push.apply(this.any, definitions.any);

    return Fetchable.didFetch.apply(this, arguments);
  }
}, {
  url: {
    get: function () {
      return "/api/v2/slas/policies/definitions.json";
    },
    configurable: true,
    enumerable: true
  }
}));

module.exports = DefinitionSet;
}});

require.define({"models/definition": function(exports, require, module) {
"use strict";

var I18n = babelHelpers.interopRequire(require("lib/i18n"));

var EXCLUDED_OPERATORS = ["within_previous_n_days", "within_next_n_days"];

var PRESENCE_OPERATORS = Object.freeze([{
  value: "present",
  label: I18n.t("txt.admin.models.rules.rule_dictionary.present_label_cap")
}, {
  value: "not_present",
  label: I18n.t("txt.admin.models.rules.rule_dictionary.not_present_label_cap")
}]);

function Definition(data) {
  var _this = this;

  var value = this.value = data.value;
  var type = this.type = data.values.type;
  var isOrganization = value === "organization_id";

  this.label = data.title || data.title_for_field; // jshint ignore:line
  this.group = data.group;
  this.operators = data.operators.reduce(function (operators, item) {
    if (EXCLUDED_OPERATORS.indexOf(item.value) === -1) {
      operators.push({
        value: item.value,
        label: item.title
      });
    }

    return operators;
  }, []);

  if (type === "list") {
    (function () {
      var hasPresenceOperators = false;

      _this.values = data.values.list.reduce(function (values, _ref) {
        var value = _ref.value;
        var title = _ref.title;

        if (value != null) {
          // jshint ignore:line
          values.push({
            value: value,
            label: title
          });
        } else if (!hasPresenceOperators) {
          hasPresenceOperators = true;
          _this.operators = _this.operators.concat(PRESENCE_OPERATORS);
        }

        return values;
      }, []);
    })();
  } else if (type === "autocomplete") {
    if (isOrganization) {
      this.url = "/api/v2/organizations/autocomplete";
      this.operators = this.operators.concat(PRESENCE_OPERATORS);
    } else {
      this.url = data.values.url;
    }
  }
}

module.exports = Definition;
}});

require.define({"models/definitions": function(exports, require, module) {
"use strict";

var Definition = babelHelpers.interopRequire(require("./definition"));

var SUPPORTED_TYPES = Object.freeze(["autocomplete", "list", "tags", "date", "datetime"]);

var Definitions = (function (_Array) {
  function Definitions() {
    babelHelpers.classCallCheck(this, Definitions);

    if (_Array != null) {
      _Array.apply(this, arguments);
    }
  }

  babelHelpers.inherits(Definitions, _Array);
  babelHelpers.createClass(Definitions, {
    lookup: {
      value: function lookup(value) {
        return this._cache[value];
      }
    },
    push: {
      value: function push() {
        var _this2 = this;

        for (var _len = arguments.length, items = Array(_len), _key = 0; _key < _len; _key++) {
          items[_key] = arguments[_key];
        }

        var _this = this;

        items.forEach(function (item) {
          var type = item.values.type;

          if (item.value === "current_tags") {
            type = item.values.type = "tags";
          }

          if (SUPPORTED_TYPES.indexOf(type) === -1) {
            return;
          }

          var definition = new Definition(item);

          _this2._cache = _this2._cache || {};
          _this2._cache[definition.value] = definition;

          babelHelpers.get(Object.getPrototypeOf(Definitions.prototype), "push", _this).call(_this, definition);
        });
      }
    },
    clear: {
      value: function clear() {
        this._cache = {};

        while (this.length > 0) {
          this.pop();
        }
      }
    }
  });
  return Definitions;
})(Array);

module.exports = Definitions;
}});

require.define({"models/errors": function(exports, require, module) {
"use strict";

var BadRequestError = babelHelpers.interopRequire(require("./errors/bad-request"));
var ForbiddenError = babelHelpers.interopRequire(require("./errors/forbidden"));
var NotFoundError = babelHelpers.interopRequire(require("./errors/not-found"));
var UnprocessableEntityError = babelHelpers.interopRequire(require("./errors/unprocessable-entity"));
var InternalServerError = babelHelpers.interopRequire(require("./errors/internal-server"));
var BadGatewayError = babelHelpers.interopRequire(require("./errors/bad-gateway"));
var ServiceUnavailableError = babelHelpers.interopRequire(require("./errors/service-unavailable"));
var UnknownError = babelHelpers.interopRequire(require("./errors/unknown"));
module.exports = {
  400: BadRequestError,
  403: ForbiddenError,
  404: NotFoundError,
  422: UnprocessableEntityError,
  500: InternalServerError,
  502: BadGatewayError,
  503: ServiceUnavailableError,
  unknown: UnknownError
};
}});

require.define({"models/model": function(exports, require, module) {
"use strict";

var eql = babelHelpers.interopRequire(require("deep-eql"));

var _libUtils = require("./../lib/utils");

var copy = _libUtils.copy;
var mixin = _libUtils.mixin;
var isEmpty = _libUtils.isEmpty;
var api = babelHelpers.interopRequire(require("lib/api"));
var Eventable = babelHelpers.interopRequire(require("./../mixins/eventable"));

/**
 * Basic model.
 *
 * @class
 */

var Model = (function () {
  /**
   * @constructor
   * @param {object} data model data
   * @param {boolean} [isNew] flag for new/existing model
   */

  function Model(data) {
    var isNew = arguments[1] === undefined ? true : arguments[1];
    babelHelpers.classCallCheck(this, Model);

    this.isDestroying = this.isDestroyed = false;
    this.isNew = isNew;
    this.reset(data);
  }

  babelHelpers.createClass(Model, {
    url: {

      /**
       * URL for model API.
       *
       * @abstract
       * @type {string}
       */

      get: function () {
        var id;

        if (this.isNew) {
          return "";
        } else {
          id = this.get("id");

          return "/" + id;
        }
      }
    },
    parse: {

      /**
       * Parses API response.
       *
       * @abstract
       * @type {object}
       */

      value: function parse(json) {
        return json;
      }
    },
    toJSON: {

      /**
       * Serializes model for API request.
       *
       * @abstract
       * @type {object}
       */

      value: function toJSON() {
        return this.attributes;
      }
    },
    get: {

      /**
       * Retrieves model attribute value.
       *
       * @param {string} key attribute key
       * @type {*}
       */

      value: function get(key) {
        return this.attributes[key];
      }
    },
    set: {

      /**
       * Sets model attribute value.
       *
       * @param {string} key attribute key
       * @param {*} value attribute value
       */

      value: function set(key, value) {
        var attrs = this.attributes,
            _attrs = this._attributes,
            isDirty = !eql(_attrs[key], value);

        if (isEmpty(_attrs[key], value)) {
          value = _attrs[key];
          isDirty = false;
        }

        if (eql(attrs[key], value) && !isDirty) {
          return;
        }

        attrs[key] = value;
        this.isDirty = isDirty || !eql(attrs, _attrs);
        this.emit("changed", this, key, value);
      }
    },
    reset: {

      /**
       * Resets model to given state.
       *
       * @param {object} [attrs] attributes for reset state
       */

      value: function reset() {
        var _this = this;

        var attrs = arguments[0] === undefined ? {} : arguments[0];

        if (this.isNew) {
          this._attributes = {};
          this.attributes = this.attributes || copy(attrs);
          this.isDirty = true;
        } else {
          this._attributes = attrs;
          this.attributes = this.attributes || {};

          Object.keys(attrs).forEach(function (key) {
            _this.attributes[key] = attrs[key] && copy(attrs[key]);
          });

          this.isDirty = this.isDestroyed = false;
        }

        return this.emit("reset");
      }
    },
    save: {

      /**
       * Saves model.
       *
       * @type {Promise}
       */

      value: function save() {
        var _this = this;

        var method = this.isNew ? "POST" : "PUT",
            req = api.request(method, this.url, this.toJSON());

        req.then(function (json) {
          return _this.didSave(json);
        });
        req["catch"](function (e) {
          return _this.didError(e);
        });
        this.emit("save", req);

        return req;
      }
    },
    destroy: {

      /**
       * Destroys model.
       */

      value: function destroy() {
        var _this = this;

        var req;

        if (this.isNew || this.isDestroyed) {
          return this.didDestroy();
        }

        this.isDestroying = true;
        req = api.request("DELETE", this.url);
        req.then(function () {
          return _this.didDestroy();
        });
        req["catch"](function (e) {
          return _this.didError(e);
        });
        this.emit("destroying", req);

        return req;
      }
    },
    didError: {
      value: function didError(err) {
        this.isDestroying = false;
        this.emit("error", err);

        return err;
      }
    },
    didSave: {
      value: function didSave(json) {
        var attrs = this.parse(json);

        this.isNew = false;
        this.reset(attrs);
        this.emit("saved");

        return json;
      }
    },
    didDestroy: {
      value: function didDestroy() {
        this.isDestroying = false;
        this.isDestroyed = true;
        this.isDirty = !this.isNew;
        this.emit("destroyed", this);
        this.off();
      }
    }
  });
  return Model;
})();

mixin(Model.prototype, Eventable);

module.exports = Model;
}});

require.define({"models/organizations": function(exports, require, module) {
"use strict";

var Collection = babelHelpers.interopRequire(require("./collection"));

var Organizations = (function (_Collection) {
  function Organizations() {
    babelHelpers.classCallCheck(this, Organizations);

    if (_Collection != null) {
      _Collection.apply(this, arguments);
    }
  }

  babelHelpers.inherits(Organizations, _Collection);
  babelHelpers.createClass(Organizations, {
    url: {
      get: function () {
        return "/api/v2/organizations.json";
      }
    },
    parse: {
      value: function parse(json) {
        return json.organizations || []; // jshint ignore:line
      }
    }
  });
  return Organizations;
})(Collection);

module.exports = Organizations;
}});

require.define({"models/policies": function(exports, require, module) {
"use strict";

var api = babelHelpers.interopRequire(require("lib/api"));
var store = babelHelpers.interopRequire(require("lib/store"));

var copy = require("lib/utils").copy;

var Collection = babelHelpers.interopRequire(require("./collection"));
var Organizations = babelHelpers.interopRequire(require("./organizations"));
var Policy = babelHelpers.interopRequire(require("./policy"));

var DEFINITION_MAP = undefined;

var WITH_PRESENCE_OPERATORS = Object.freeze(["group_id", "organization_id", "assignee_id"]);
var TERMINAL_OPERATORS = Object.freeze(["present", "not_present"]);

function isTerminal(condition) {
  if (condition.value != null) {
    return false;
  } // jshint ignore:line

  return WITH_PRESENCE_OPERATORS.indexOf(condition.field) !== -1 || TERMINAL_OPERATORS.indexOf(condition.operator) !== -1;
}

function generateDefinitionMap(definitions) {
  return definitions.reduce(function (memo, definition) {
    memo[definition.value] = definition;

    return memo;
  }, {});
}

var Policies = (function (_Collection) {
  function Policies() {
    babelHelpers.classCallCheck(this, Policies);

    Collection.apply(this, arguments);

    this.organizations = new Organizations();
  }

  babelHelpers.inherits(Policies, _Collection);
  babelHelpers.createClass(Policies, {
    url: {
      get: function () {
        return "/api/v2/slas/policies.json";
      }
    },
    sideloads: {
      get: function () {
        return ["organizations"];
      }
    },
    parse: {
      value: function parse(json) {
        var _this = this;

        if (!DEFINITION_MAP) {
          var definitions = store.lookup("definition-set");

          DEFINITION_MAP = {
            ALL: generateDefinitionMap(definitions.all),
            ANY: generateDefinitionMap(definitions.any)
          };
        }

        var policies = json.sla_policies || []; // jshint ignore:line
        var organizations = this.organizations;

        organizations.load(json);
        policies.forEach(function (policy) {
          var filter = policy.filter;

          filter.all = _this._reduceFilterConditions(DEFINITION_MAP.ALL, filter.all);
          filter.any = _this._reduceFilterConditions(DEFINITION_MAP.ANY, filter.any);
        });

        return policies;
      }
    },
    reorder: {
      value: function reorder(ids) {
        var _this = this;

        var req = api.request("PUT", "/api/v2/slas/policies/reorder.json", {
          sla_policy_ids: ids // jshint ignore:line
        });

        req.then(function () {
          return _this.didReorder(ids);
        });
        req["catch"](function () {
          return _this.forEach(function (item) {
            return item.reset();
          });
        });
        req["catch"](function (e) {
          return _this.didError(e);
        });
        this.emit("reorder");

        return req;
      }
    },
    fetch: {
      value: function fetch() {
        var req = this.req;

        if (req) {
          return req;
        }

        req = this.req = babelHelpers.get(Object.getPrototypeOf(Policies.prototype), "fetch", this).call(this);

        return req;
      }
    },
    didReorder: {
      value: function didReorder(ids) {
        this.forEach(function (item) {
          return item.reset(copy(item.attributes));
        });
        this.sort(function (a, b) {
          return a.get("position") > b.get("position") ? 1 : -1;
        });
        this.emit("reordered", ids);
      }
    },
    _reduceFilterConditions: {
      value: function _reduceFilterConditions(definitions, conditions) {
        var organizations = this.organizations;

        return conditions.reduce(function (conditions, condition) {
          var field = condition.field,
              definition = definitions[field],
              org = undefined;

          if (!definition) {
            return conditions;
          }

          if (isTerminal(condition)) {
            conditions.push(condition);

            return conditions;
          }

          if (definition.type === "list") {
            var _ret = (function () {
              var value = condition.value,
                  hasValue = undefined;

              hasValue = definition.values.some(function (item) {
                if (typeof item.value !== typeof value) {
                  // This can be removed if we manage to clean up policies created
                  // with dropdown condition value saved as a string.
                  return item.value.toString() === value.toString();
                }
                return item.value === value;
              });

              if (!hasValue) {
                return {
                  v: conditions
                };
              }
            })();

            if (typeof _ret === "object") return _ret.v;
          }

          if (field === "organization_id") {
            org = organizations.lookup(condition.value);

            if (!org) {
              return conditions;
            }

            Object.defineProperty(condition, "meta", {
              enumerable: false,

              value: {
                label: org ? org.get("name") : ""
              }
            });
          }

          conditions.push(condition);

          return conditions;
        }, []);
      }
    }
  }, {
    Model: {
      get: function () {
        return Policy;
      }
    }
  });
  return Policies;
})(Collection);

module.exports = Policies;
}});

require.define({"models/policy": function(exports, require, module) {
/* jshint camelcase: false */

"use strict";

var I18n = babelHelpers.interopRequire(require("lib/i18n"));

var copy = require("lib/utils").copy;

var Model = babelHelpers.interopRequire(require("./model"));

var Policy = (function (_Model) {
  function Policy() {
    var data = arguments[0] === undefined ? {} : arguments[0];
    var isNew = arguments[1] === undefined ? true : arguments[1];
    babelHelpers.classCallCheck(this, Policy);

    Model.call(this, data, isNew);
  }

  babelHelpers.inherits(Policy, _Model);
  babelHelpers.createClass(Policy, {
    url: {
      get: function () {
        return "/api/v2/slas/policies" + babelHelpers.get(Object.getPrototypeOf(Policy.prototype), "url", this) + ".json";
      }
    },
    parse: {
      value: function parse() {
        var json = arguments[0] === undefined ? {} : arguments[0];

        return json.sla_policy;
      }
    },
    clone: {
      value: function clone() {
        var collection = this.collection,
            attrs;

        if (!collection) {
          return;
        }

        attrs = copy(this._attributes);
        attrs.title = I18n.t("txt.admin.slas.clone_title", { title: attrs.title });
        attrs.position = undefined;

        collection.add(new this.constructor(attrs));
      }
    },
    toJSON: {
      value: function toJSON() {
        if (this.isNew) {
          return {
            sla_policy: this.attributes
          };
        } else {
          return {
            sla_policy: {
              title: this.get("title"),
              description: this.get("description"),
              position: this.get("position"),
              filter: this.get("filter"),
              policy_metrics: this.get("policy_metrics")
            }
          };
        }
      }
    }
  });
  return Policy;
})(Model);

module.exports = Policy;
}});

require.define({"models/priorities": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var Fetchable = babelHelpers.interopRequire(require("mixins/fetchable"));

var Priorities = (function (_Array) {
  function Priorities() {
    babelHelpers.classCallCheck(this, Priorities);

    if (_Array != null) {
      _Array.apply(this, arguments);
    }
  }

  babelHelpers.inherits(Priorities, _Array);
  babelHelpers.createClass(Priorities, {
    clear: {
      value: function clear() {
        while (this.length > 0) {
          this.pop();
        }
      }
    },
    url: {
      get: function () {
        return "/api/v2/ticket_fields.json";
      }
    }
  });
  return Priorities;
})(Array);

mixin(Priorities.prototype, Eventable, Fetchable, {
  didFetch: function didFetch(json) {
    var _this = this;

    var ticketFields = json.ticket_fields || [],
        // jshint ignore:line
    ticketField,
        type;

    for (var i = 0, len = ticketFields.length; i < len; i++) {
      ticketField = ticketFields[i];
      type = ticketField.type;

      if (type === "priority" || type === "basic_priority") {
        break;
      }
    }

    this.clear();

    this.active = ticketField.active;

    ticketField.system_field_options.forEach(function (priority) {
      return _this.unshift(priority.value);
    }); // jshint ignore:line

    this.emit("fetched");

    return this;
  }
});

module.exports = Priorities;
}});

require.define({"models/settings": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Fetchable = babelHelpers.interopRequire(require("mixins/fetchable"));
var Model = babelHelpers.interopRequire(require("models/model"));

var Settings = (function (_Model) {
  function Settings() {
    babelHelpers.classCallCheck(this, Settings);

    if (_Model != null) {
      _Model.apply(this, arguments);
    }
  }

  babelHelpers.inherits(Settings, _Model);
  babelHelpers.createClass(Settings, {
    url: {
      get: function () {
        return "/api/v2/account/settings.json";
      }
    },
    parse: {
      value: function parse() {
        var json = arguments[0] === undefined ? {} : arguments[0];

        return json.settings || {};
      }
    }
  });
  return Settings;
})(Model);

mixin(Settings.prototype, Fetchable, {
  didFetch: function didFetch(json) {
    Fetchable.didFetch.apply(this, arguments);
    this.isNew = false;

    return this.reset(this.parse(json));
  }
});

module.exports = Settings;
}});

require.define({"mixins/composable": function(exports, require, module) {
"use strict";

module.exports = Object.defineProperties({

  addChildView: function addChildView(item) {
    var view = new this.constructor.Item(item);

    view.appendTo(this.el);
    this.views.push(view);

    return view;
  },

  removeChildView: function removeChildView(view) {
    this.views = this.views.reduce(function (active, v) {
      if (view !== v) {
        active.push(v);
      }

      return active;
    }, []);

    view.remove();
  },

  clear: function clear() {
    this.views.forEach(function (view) {
      return view.remove();
    });

    this.views = [];
    this._items = [];
  }
}, {
  views: {
    get: function () {
      return this._views || [];
    },
    set: function (val) {
      this._views = val;
    },
    configurable: true,
    enumerable: true
  },
  items: {
    get: function () {
      return this._items || [];
    },
    set: function () {
      var _this = this;

      var val = arguments[0] === undefined ? [] : arguments[0];

      this.clear();
      this._items = val;
      val.forEach(function (item) {
        return _this.addChildView(item);
      });
    },
    configurable: true,
    enumerable: true
  }
});
}});

require.define({"mixins/eventable": function(exports, require, module) {
"use strict";

module.exports = {
  on: function on(name, callback, context) {
    if (!this._events) {
      this._events = {};
    }

    var listeners = this._events[name];

    if (!listeners) {
      listeners = this._events[name] = [];
    }

    listeners.push({
      callback: callback,
      context: context || this
    });

    return this;
  },

  once: function once(name, callback, context) {
    var self = this;

    function wrapped() {
      callback.apply(context, arguments);

      self.off(name, wrapped, context);
    }

    this.on(name, wrapped, context);
  },

  off: function off(name, callback, context) {
    if (arguments.length === 0) {
      this._events = {};

      return this;
    }

    var listeners = this._events && this._events[name];

    if (!listeners) {
      return this;
    }

    context = context || this;

    listeners = listeners.reduce(function (memo, listener) {
      if (listener.callback !== callback || listener.context !== context) {
        memo.push(listener);
      }

      return memo;
    }, []);
    this._events[name] = listeners;

    return this;
  },

  emit: function emit(name) {
    var listeners = this._events && this._events[name],
        args;

    if (!listeners) {
      return this;
    }

    args = Array.prototype.slice.call(arguments, 1);

    listeners.forEach(function (listener) {
      listener.callback.apply(listener.context, args);
    });

    return this;
  }
};
}});

require.define({"mixins/fetchable": function(exports, require, module) {
"use strict";

var api = babelHelpers.interopRequire(require("lib/api"));

/**
 * Mixin for fetch behavior.
 *
 * NOTE: Assumes use of mixins/eventable.
 */
module.exports = {
  /**
   * Fetches an API resource.
   *
   * Requires `url` property. Optionally can fetch with sideloads (if a
   * `sideloads` property is present on the model).
   *
   * @return Promise
   */
  fetch: function fetch() {
    var _this = this;

    var req = this.req;

    if (req) {
      return req;
    }

    var url = this.url;
    var sideloads = this.sideloads;

    if (sideloads && sideloads.length >= 1) {
      url += "?include=" + sideloads.join(",");
    }

    req = this.req = api.request("GET", url);
    req.then(function (json) {
      return _this.didFetch(json);
    });
    req["catch"](function (e) {
      return _this.didError(e);
    });
    this.emit("fetch", req);

    return req;
  },

  /**
   * Callback for a successful fetch.
   *
   * @param {object} json JSON response body
   *
   * @returns `this`
   */
  didFetch: function didFetch() {
    this.emit("fetched");

    return this;
  },

  /**
   * Callback for a fetch error.
   *
   * @param {Error} e error
   */
  didError: function didError(e) {
    this.emit("error", e);

    return Promise.reject(e);
  }
};
/*json*/
}});

require.define({"mixins/sortable": function(exports, require, module) {
"use strict";

var _Array$prototype = Array.prototype;
var forEach = _Array$prototype.forEach;
var indexOf = _Array$prototype.indexOf;

var DRAG_HANDLERS = ["onMouseDown", "onDragStart", "onDragEnd", "onDragOver"];

function reset() {
  this.isMovingDown = false;
  this.dropzone = this.position = this.margin = null;
  this.newIndex = this.oldIndex = -1;
  this.isDirty = false;
}

/**
  Observes and handles drag/drop events for a given element.

  @constructor
  @param {Node} el
  @param {object} [options]
  @param {function} [options.onUpdate] Handler for update events.
 */
function DragObserver(el) {
  var _this = this;

  var options = arguments[1] === undefined ? {} : arguments[1];

  this.el = el;
  reset.call(this);

  DRAG_HANDLERS.forEach(function (name) {
    return _this[name] = _this[name].bind(_this);
  });

  this.marker = (function () {
    var el = document.createElement("div");
    var tack = document.createElement("div");
    var uptick = document.createElement("div");
    var downtick = document.createElement("div");

    el.className = "z-sortable__marker";
    tack.className = "z-sortable__marker__tack";
    uptick.className = "z-sortable__marker__tick z-sortable__marker__tick--up";
    downtick.className = "z-sortable__marker__tick z-sortable__marker__tick--down";

    tack.appendChild(uptick);
    tack.appendChild(downtick);
    el.appendChild(tack);

    return el;
  })();

  this.onUpdate = options.onUpdate;
}

DragObserver.prototype = {
  /**
    Sets drag event observation.
     Adds mousedown, dragover, drop event listeners to each child. Listeners are
    removed in #unobserve.
   */
  observe: function observe() {
    var _this = this;

    forEach.call(this.el.children, function (el) {
      el.addEventListener("mousedown", _this.onMouseDown, false);
      el.addEventListener("dragover", _this.onDragOver, false);
    });
  },

  /**
    Tears down drag event observation.
     Removes all event listeners for each child. Also resets observer to default
    state.
   */
  unobserve: function unobserve() {
    var _this = this;

    forEach.call(this.el.children, function (el) {
      el.draggable = null;

      el.removeEventListener("mousedown", _this.onMouseDown, false);
      el.removeEventListener("dragover", _this.onDragOver, false);
      el.removeEventListener("dragstart", _this.onDragStart, false);
      el.removeEventListener("dragend", _this.onDragEnd, false);
    });

    reset.call(this);
  },

  /**
    Updates placement of drag marker and tracks drop position.
   */
  update: function update() {
    var _this = this;

    if (!this.dropzone) {
      return;
    }

    // Run in a request animation loop to avoid jank.
    window.requestAnimationFrame(function () {
      return _this.update();
    });

    // Only update when dirty. Definition of dirty in this case is when the
    // marker position should update.
    if (!this.isDirty) {
      return;
    }

    this.isDirty = false;

    var _ref = this;

    var dropzone = _ref.dropzone;
    var margin = _ref.margin;
    var marker = _ref.marker;
    var offsetHeight = dropzone.offsetHeight;
    var offsetTop = dropzone.offsetTop;

    // Calculates margin once. If the spacing between children is uneven, this
    // may result in poor positioning.
    if (typeof margin !== "number") {
      var _window$getComputedStyle = window.getComputedStyle(dropzone);

      var marginBottom = _window$getComputedStyle.marginBottom;
      var marginTop = _window$getComputedStyle.marginTop;

      margin = this.margin = (parseInt(marginBottom, 10) + parseInt(marginTop, 10)) / 4;
    }

    if (!marker.parentNode) {
      marker.style.top = "" + (Math.round(offsetTop + offsetHeight) + margin) + "px";
      this.el.appendChild(marker);
    }

    var oldIndex = this.oldIndex;
    var index = indexOf.call(this.el.children, dropzone);

    if (this.isMovingDown) {
      marker.style.top = "" + (Math.round(offsetTop + offsetHeight) + margin) + "px";
      this.newIndex = index < oldIndex ? index + 1 : index;
    } else {
      marker.style.top = "" + (offsetTop - margin) + "px";
      this.newIndex = index > oldIndex ? index - 1 : index;
    }
  },

  /**
    Mouse down listener for each child.
     Responsible for starting a drag session.
     Enables drag behavior for child, setting "draggable" attribute and adding
    event listeners for dragstart and mouseup. Listeners are removed in
    #onDragEnd.
     @param {MouseEvent} e
    @listens #mousedown
   */
  onMouseDown: function onMouseDown(e) {
    var el = e.currentTarget;
    var target = e.target;
    var handle = el.querySelector(".z-grabber");

    // Do nothing when target is not drag handle.
    if (target !== handle && !handle.contains(target)) {
      return;
    }

    el.draggable = true;
    el.addEventListener("dragstart", this.onDragStart, false);

    this.newIndex = this.oldIndex = indexOf.call(this.el.children, el);
  },

  /**
    Drag start listener for each child.
     Sets up drag image and styling for dragged child.
     Adds event listeners for dragend, which is removed in #onDragEnd.
     @param {DragEvent} e
    @listens #dragstart
   */
  onDragStart: function onDragStart(e) {
    var el = e.currentTarget;
    var node = el.querySelector(".z-capsule__header").cloneNode(true);

    node.style.top = "" + e.y + "px";
    node.style.left = "" + e.x + "px";
    node.style.position = "absolute";
    node.classList.add("z-sortable__dragimage");

    // Changing position of DOM element on dragstart event causes WebKit and
    // Chromium browsers to emit dragend event right away. Note that we don't
    // see this behavior with capsules right now. The workaround is to add
    // node to lower element-stack.
    //
    // Ref:
    // https://bugs.chromium.org/p/chromium/issues/detail?id=445641
    // https://bugs.webkit.org/show_bug.cgi?id=134212
    // http://jsfiddle.net/j9q66ja6/1/
    node.style.zIndex = -100;

    document.body.appendChild(node);
    e.dataTransfer.setData("text/plain", "");
    e.dataTransfer.setDragImage(node, 10, 10);

    setTimeout(function () {
      document.body.removeChild(node);
    }, 0);

    el.classList.add("z-sortable--dragging");
    el.addEventListener("dragend", this.onDragEnd, false);
    this.dropzone = el;

    this.update();
  },

  /**
    Drag end listener for each child.
     Cleans up marker and updates dragged child's position in list. Once
    updated, if an `onUpdate` handler is configured and the child's position
    has changed, the handler will be invoked with old and new indices.
     @param {DragEvent} e
    @listens #dragend
   */
  onDragEnd: function onDragEnd(e) {
    var _ref = this;

    var el = _ref.el;
    var newIndex = _ref.newIndex;
    var oldIndex = _ref.oldIndex;
    var marker = _ref.marker;

    var target = e.currentTarget;

    this.dropzone = null;
    target.classList.remove("z-sortable--dragging");

    if (marker.parentNode) {
      el.removeChild(marker);
      marker.style.top = "auto";
    }

    target.draggable = false;
    target.removeEventListener("dragend", this.onDragEnd, false);
    target.removeEventListener("dragstart", this.onDragStart, false);
    reset.call(this);

    if (newIndex === oldIndex) {
      return;
    }

    var ref = el.children[newIndex];

    if (newIndex > oldIndex) {
      ref = ref.nextSibling;
    }

    el.insertBefore(target, ref);

    if (this.onUpdate) {
      this.onUpdate({
        target: target,
        oldIndex: oldIndex,
        newIndex: newIndex
      });
    }
  },

  /**
    Drag over listener for each child.
     This listener is added from #observe. Its main responsibility is to store
    position, so the marker can be properly positioned within the list (since
    its positioning is absolute).
     In addition, the handler tracks directionality of movement and "dirtiness"
    (whether a repaint is required for the marker).
     @param {DragEvent} e
    @listens #dragover
   */
  onDragOver: function onDragOver(e) {
    this.dropzone = e.currentTarget;

    e.preventDefault();
    e.dataTransfer.dropEffect = "move";

    var position = this.position;
    var clientY = this.position = e.clientY;

    if (position !== clientY) {
      this.isMovingDown = position < clientY;
      this.isDirty = true;
    }
  }
};

module.exports = {
  /**
    Makes an element's children "sortable". Primary prerequisite is that the
    current context have an `el` property which refers to the target element.
     @param {object} [options] See `DragObserver` for details on options.
   */
  sortable: function sortable() {
    var options = arguments[0] === undefined ? {} : arguments[0];

    var dragObserver = this.dragObserver;

    if (dragObserver) {
      dragObserver.unobserve();
    }

    dragObserver = this.dragObserver = new DragObserver(this.el, options);
    dragObserver.observe();
  },

  /**
    Resets an element to not be "sortable". Inverse of #sortable, basically.
   */
  unsortable: function unsortable() {
    var dragObserver = this.dragObserver;

    if (!dragObserver) {
      return;
    }

    dragObserver.unobserve();
    this.dragObserver = null;
  }
};
}});

require.define({"components/policy/capsule": function(exports, require, module) {
"use strict";

var I18n = babelHelpers.interopRequire(require("lib/i18n"));
var LotusClient = babelHelpers.interopRequire(require("lotus-client"));
var ZCapsule = babelHelpers.interopRequire(require("./../z/capsule"));
var ZLoader = babelHelpers.interopRequire(require("./../z/loader"));
var ZStates = babelHelpers.interopRequire(require("./../z/form/states"));
var PolicyForm = babelHelpers.interopRequire(require("./form"));

function configureForm() {
  var form = this.form = new PolicyForm({ policy: this.policy });

  form.appendTo(this.body);

  form.on("submitted", this.onSaved, this);
  form.on("cancel", this.onReset, this);
}

var PolicyCapsule = (function (_ZCapsule) {
  function PolicyCapsule(options) {
    var _this = this;

    babelHelpers.classCallCheck(this, PolicyCapsule);

    var policy = options.item;
    var loader = new ZLoader({
      classList: ["loader--small"]
    });
    var states = this.states = new ZStates({
      states: [{
        name: I18n.t("txt.admin.slas.states.saved"),
        value: "submitted"
      }, {
        name: I18n.t("txt.admin.slas.states.unsaved"),
        value: "error"
      }, {
        name: I18n.t("txt.admin.slas.states.saving"),
        value: "destroying",
        icon: loader.el
      }]
    });

    this.policy = policy;
    policy.on("changed", this.onChange, this);
    policy.on("error", this.onError, this);
    policy.on("destroying", this.onDestroying, this);

    var collection = policy.collection,
        didLoad;

    if (collection) {
      didLoad = function () {
        collection.on("add", _this.disableClone, _this);
        collection.off("load", didLoad, _this);
      };

      this.policies = collection;
      collection.on("load", didLoad, this);
    }

    options.item = {
      title: policy.get("title"),
      subtitle: policy.get("description"),
      isNew: policy.isNew
    };

    babelHelpers.get(Object.getPrototypeOf(PolicyCapsule.prototype), "constructor", this).call(this, options);

    states.appendTo(this.$(".z-capsule__header"));
  }

  babelHelpers.inherits(PolicyCapsule, _ZCapsule);
  babelHelpers.createClass(PolicyCapsule, {
    menu: {
      get: function () {
        return [{ id: "del", title: I18n.t("txt.admin.slas.policy_delete_option") }, { id: "clone", title: I18n.t("txt.admin.slas.policy_clone_option") }];
      }
    },
    setElement: {
      value: function setElement(el) {
        babelHelpers.get(Object.getPrototypeOf(PolicyCapsule.prototype), "setElement", this).call(this, el);

        if (this.policy.isNew) configureForm.call(this);
      }
    },
    addTo: {
      value: function addTo(target) {
        return this.appendTo(target);
      }
    },
    remove: {
      value: function remove() {
        var _ref = this;

        var form = _ref.form;
        var policies = _ref.policies;
        var policy = _ref.policy;

        if (form) form.remove();

        babelHelpers.get(Object.getPrototypeOf(PolicyCapsule.prototype), "remove", this).call(this);

        policy.off("changed", this.onChange, this);
        policy.off("error", this.onError, this);
        policy.off("destroying", this.onDestroying, this);

        if (policies) {
          policies.off("add", this.disableClone, this);
        }

        this.policy = null;
      }
    },
    onChange: {
      value: function onChange(policy, key, val) {
        if (key === "title") {
          this.title = val;
        } else if (key === "description") {
          this.subtitle = val;
        }
      }
    },
    onClick: {
      value: function onClick(event) {
        if (!this.form) configureForm.call(this);

        babelHelpers.get(Object.getPrototypeOf(PolicyCapsule.prototype), "onClick", this).call(this, event);
      }
    },
    onMenuSelect: {
      value: function onMenuSelect(option) {
        if (option === "clone") {
          this.onClone();
        } else if (option === "del") {
          this.onDelete();
        }

        babelHelpers.get(Object.getPrototypeOf(PolicyCapsule.prototype), "onMenuSelect", this).call(this, option);
      }
    },
    onDelete: {
      value: function onDelete() {
        var _this = this;

        LotusClient.showConfirmationModal({
          title: I18n.t("txt.admin.slas.delete_modal.title"),
          message: I18n.t("txt.admin.slas.delete_modal.body"),
          confirmLabel: I18n.t("txt.admin.slas.delete_modal.confirm"),
          cancelLabel: I18n.t("txt.admin.slas.delete_modal.cancel")
        }).then(function () {
          return _this.policy.destroy();
        }).then(function () {
          return _this.remove();
        })["catch"](function () {});
      }
    },
    onClone: {
      value: function onClone() {
        this.policy.clone();
      }
    },
    onSaved: {
      value: function onSaved() {
        var _this = this;

        this.active = false;
        this.isNew = this.item.isNew = this.policy.isNew;
        this.states.current = "submitted";
        setTimeout(function () {
          _this.states.current = null;
        }, 1000);
      }
    },
    onError: {
      value: function onError() {
        this.states.current = "error";
      }
    },
    onDestroying: {
      value: function onDestroying() {
        this.states.current = "destroying";

        var form = this.form;

        if (form) form.stateManager.transitionTo("saving");
      }
    },
    onReset: {
      value: function onReset() {
        var policy = this.policy;

        if (policy.isNew) {
          policy.destroy();
          this.remove();
        } else {
          this.title = policy.get("title");
          this.subtitle = policy.get("description");
        }
      }
    },
    disableClone: {
      value: function disableClone(policy) {
        var _this = this;

        var didSave;

        this.dropdown.disable("clone");

        didSave = function () {
          _this.dropdown.enable("clone");
          policy.off("saved", didSave);
          policy.off("destroyed", didSave);
        };

        policy.on("saved", didSave);
        policy.on("destroyed", didSave);
      }
    }
  }, {
    classList: {
      get: function () {
        return ZCapsule.classList;
      }
    },
    template: {
      get: function () {
        return ZCapsule.template;
      }
    },
    EMPTY_HEADING: {
      get: function () {
        return I18n.t("txt.admin.slas.policy_heading_placeholder");
      }
    }
  });
  return PolicyCapsule;
})(ZCapsule);

module.exports = PolicyCapsule;
}});

require.define({"components/policy/capsules": function(exports, require, module) {
"use strict";

var ZCapsules = babelHelpers.interopRequire(require("./../z/capsules"));
var PolicyCapsule = babelHelpers.interopRequire(require("./capsule"));

var classList = ZCapsules.classList.concat(["policy-capsules"]);

var PolicyCapsules = (function (_ZCapsules) {
  function PolicyCapsules() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, PolicyCapsules);

    options.sortable = true;

    ZCapsules.call(this, options);
  }

  babelHelpers.inherits(PolicyCapsules, _ZCapsules);
  babelHelpers.createClass(PolicyCapsules, {
    items: {
      get: function () {
        return babelHelpers.get(Object.getPrototypeOf(PolicyCapsules.prototype), "items", this);
      },
      set: function () {
        var val = arguments[0] === undefined ? [] : arguments[0];

        babelHelpers.set(Object.getPrototypeOf(PolicyCapsules.prototype), "items", val, this);

        if (val && val.on) {
          val.on("add", this.addChildView, this);
        }
      }
    },
    clear: {
      value: function clear() {
        if (this.items && this.items.off) {
          this.items.off("add", this.addChildView, this);
        }

        return babelHelpers.get(Object.getPrototypeOf(PolicyCapsules.prototype), "clear", this).call(this);
      }
    },
    onReorder: {
      value: function onReorder(e) {
        var _this = this;

        var target = e.target;
        var oldIndex = e.oldIndex;
        var newIndex = e.newIndex;

        var items = this.items;
        var dir = undefined;
        var max = undefined;
        var min = undefined;

        // Firefox does not fire a blur event after a drag operation.
        target.classList.remove("z-capsule--focused");
        this.el.classList.add("z-capsules--disabled");

        if (newIndex > oldIndex) {
          dir = 1;
          max = newIndex;
          min = oldIndex + 1;
        } else {
          dir = -1;
          max = oldIndex - 1;
          min = newIndex;
        }

        var ids = items.reduce(function (ids, item, index) {
          if (index === oldIndex) {
            index = newIndex;
          } else if (index <= max && index >= min) {
            index = index - dir;
          }

          ids[index] = item.get("id");
          item.set("position", index + 1);

          return ids;
        }, []);

        var req = items.reorder(ids);
        req.then(function () {
          return _this.didReorder(ids);
        });
        req["catch"](function (e) {
          return _this.didError(e);
        });

        return req;
      }
    },
    didReorder: {
      value: function didReorder() {
        this.el.classList.remove("z-capsules--disabled");
      }
    },
    didError: {
      value: function didError() {
        this.el.classList.remove("z-capsules--disabled");
        this.items = this.items;
      }
    }
  }, {
    Capsule: {
      get: function () {
        return PolicyCapsule;
      }
    },
    classList: {
      get: function () {
        return classList;
      }
    },
    tagName: {
      get: function () {
        return "div";
      }
    },
    template: {
      get: function () {
        return Object.getOwnPropertyDescriptor(ZCapsules, "template").get.call(this);
      }
    }
  });
  return PolicyCapsules;
})(ZCapsules);

module.exports = PolicyCapsules;
/*ids*/ /*e*/
}});

require.define({"components/policy/form": function(exports, require, module) {
"use strict";

var I18n = babelHelpers.interopRequire(require("lib/i18n"));
var PolicyPopover = babelHelpers.interopRequire(require("../policy/popover"));
var PolicyMatrix = babelHelpers.interopRequire(require("../policy/matrix"));
var ZCondition = babelHelpers.interopRequire(require("../z/condition"));
var ZConditionsFieldset = babelHelpers.interopRequire(require("../z/conditions-fieldset"));
var ZDropdown = babelHelpers.interopRequire(require("../z/dropdown"));
var ZDuration = babelHelpers.interopRequire(require("../z/duration"));
var ZDateInput = babelHelpers.interopRequire(require("../z/fields/date"));
var ZForm = babelHelpers.interopRequire(require("../z/form"));

var SCHEMA = Object.seal([{
  fields: [{
    type: "TextInput",
    name: "title",
    label: I18n.t("txt.admin.slas.policy_name_label"),
    attributes: {
      maxlength: 255,
      placeholder: I18n.t("txt.admin.slas.policy_name_placeholder"),
      autocomplete: "off",
      spellcheck: "false"
    },
    required: true,
    width: "5/12"
  }, {
    type: "TextInput",
    name: "description",
    label: I18n.t("txt.admin.slas.policy_description_label"),
    width: "5/12",
    attributes: {
      maxlength: 255,
      placeholder: I18n.t("txt.admin.slas.policy_description_placeholder"),
      autocomplete: "off",
      spellcheck: "false"
    }
  }]
}, {
  title: I18n.t("txt.admin.slas.conditions_heading"),
  description: I18n.t("txt.admin.slas.conditions_description"),
  fields: [{
    type: "ConditionsFieldset",
    name: "filter"
  }]
}, {
  title: I18n.t("txt.admin.slas.targets_heading"),
  description: I18n.t("txt.admin.slas.targets_description"),
  fields: [{
    type: "PolicyMatrix",
    name: "policy_metrics"
  }]
}]);

PolicyPopover.labels = {
  newLabel: I18n.t("txt.admin.slas.ticket.status.mnemonic_new"),
  openLabel: I18n.t("txt.admin.slas.ticket.status.mnemonic_open"),
  pendingLabel: I18n.t("txt.admin.slas.ticket.status.mnemonic_pending"),
  holdLabel: I18n.t("txt.admin.slas.ticket.status.mnemonic_hold")
};

PolicyMatrix.headers = {
  urgent: I18n.t("txt.admin.slas.target_urgent_heading"),
  high: I18n.t("txt.admin.slas.target_high_heading"),
  normal: I18n.t("txt.admin.slas.target_normal_heading"),
  low: I18n.t("txt.admin.slas.target_low_heading")
};

PolicyMatrix.SCHEMA = [{
  header: I18n.t("txt.admin.slas.target_first_reply_time_heading"),
  popover: {
    heading: I18n.t("txt.admin.slas.target_first_reply_time_heading"),
    description: I18n.t("txt.admin.slas.metric_tooltip.first_reply_time_description"),
    href: I18n.t("txt.admin.slas.page_link"),
    linkText: I18n.t("txt.admin.slas.metric_tooltip.learn_more_link_text"),
    classList: ["z-popover--first-reply"]
  },
  changeEvent: "change",
  Field: ZDuration
}, {
  header: I18n.t("txt.admin.slas.target_requester_wait_time_heading"),
  popover: {
    heading: I18n.t("txt.admin.slas.target_requester_wait_time_heading"),
    description: I18n.t("txt.admin.slas.metric_tooltip.requester_wait_time_description"),
    href: I18n.t("txt.admin.slas.page_link"),
    linkText: I18n.t("txt.admin.slas.metric_tooltip.learn_more_link_text"),
    classList: ["z-popover--requester-wait"]
  },
  changeEvent: "change",
  Field: ZDuration
}, {
  header: I18n.t("txt.admin.slas.target_agent_work_time_heading"),
  popover: {
    heading: I18n.t("txt.admin.slas.target_agent_work_time_heading"),
    description: I18n.t("txt.admin.slas.metric_tooltip.agent_work_time_description"),
    href: I18n.t("txt.admin.slas.page_link"),
    linkText: I18n.t("txt.admin.slas.metric_tooltip.learn_more_link_text"),
    classList: ["z-popover--agent-work"]
  },
  changeEvent: "change",
  Field: ZDuration
}, {
  header: I18n.t("txt.admin.slas.target_next_reply_time_heading"),
  popover: {
    heading: I18n.t("txt.admin.slas.target_next_reply_time_heading"),
    description: I18n.t("txt.admin.slas.metric_tooltip.next_reply_time_description"),
    href: I18n.t("txt.admin.slas.page_link"),
    linkText: I18n.t("txt.admin.slas.metric_tooltip.learn_more_link_text"),
    classList: ["z-popover--next-reply"]
  },
  changeEvent: "change",
  Field: ZDuration
}, {
  header: I18n.t("txt.admin.slas.target_periodic_update_time_heading"),
  popover: {
    heading: I18n.t("txt.admin.slas.target_periodic_update_time_heading"),
    description: I18n.t("txt.admin.slas.metric_tooltip.periodic_update_time_description"),
    href: I18n.t("txt.admin.slas.page_link"),
    linkText: I18n.t("txt.admin.slas.metric_tooltip.learn_more_link_text"),
    classList: ["z-popover--periodic-update"]
  },
  changeEvent: "change",
  Field: ZDuration
}, {
  header: I18n.t("txt.admin.slas.target_pausable_update_time_heading"),
  popover: {
    heading: I18n.t("txt.admin.slas.target_pausable_update_time_heading"),
    description: I18n.t("txt.admin.slas.metric_tooltip.pausable_update_time_description"),
    href: I18n.t("txt.admin.slas.page_link"),
    linkText: I18n.t("txt.admin.slas.metric_tooltip.learn_more_link_text"),
    classList: ["z-popover--pausable-update"]
  },
  changeEvent: "change",
  Field: ZDuration
}, {
  header: I18n.t("txt.admin.slas.target_hours_of_operation_heading"),
  fieldOptions: {
    items: [{ id: "false", value: false, title: I18n.t("txt.admin.slas.target_calendar_hours_option") }, { id: "true", value: true, title: I18n.t("txt.admin.slas.target_business_hours_option") }],
    selectable: true,
    selected: "false",
    classList: ["z-dropdown--hours", "u-br"]
  },
  changeEvent: "selected",
  Field: ZDropdown,
  classList: ["z-matrix__row--hours"]
}];

ZCondition.labels = ["organization", "requester", "ticket"].reduce(function (labels, type) {
  labels[type] = I18n.t("txt.admin.definition_groups." + type);

  return labels;
}, {});

ZCondition.PLACEHOLDER = I18n.t("txt.admin.slas.conditions_placeholder");

ZConditionsFieldset.labels = {
  all: I18n.t("txt.admin.slas.conditions_all"),
  any: I18n.t("txt.admin.slas.conditions_any") };

ZDateInput.FORMAT = I18n.t("date.business_hours.format.long");

ZDuration.labels = {
  placeholder: I18n.t("txt.admin.slas.target_placeholder"),
  hours: I18n.t("txt.admin.slas.target_hours_label"),
  mins: I18n.t("txt.admin.slas.target_mins_label"),
  hoursInitial: I18n.t("duration.hour.narrow.one") || I18n.t("duration.hour.narrow.other"),
  minsInitial: I18n.t("duration.minute.narrow.one") || I18n.t("duration.minute.narrow.other")
};

var PolicyForm = (function (_ZForm) {
  function PolicyForm() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, PolicyForm);

    this.policy = options.policy;

    options.schema = SCHEMA;
    options.data = this.policy.attributes;
    options.submitText = I18n.t("txt.admin.form.actions.save");
    options.cancelText = I18n.t("txt.admin.form.actions.cancel");
    options.submittingText = I18n.t("txt.admin.slas.states.saving");
    options.errorText = I18n.t("txt.admin.slas.states.unsaved");

    babelHelpers.get(Object.getPrototypeOf(PolicyForm.prototype), "constructor", this).call(this, options);
  }

  babelHelpers.inherits(PolicyForm, _ZForm);
  babelHelpers.createClass(PolicyForm, {
    onChange: {
      value: function onChange(field, value) {
        var policy = this.policy,
            name = field.name;

        policy.set(name, value);
        babelHelpers.get(Object.getPrototypeOf(PolicyForm.prototype), "onChange", this).call(this, field, value);

        if (policy.isDirty) {
          this.stateManager.transitionTo(this.isValid ? "valid" : "invalid");
        } else {
          this.stateManager.transitionTo("initial");
        }
      }
    },
    onSubmit: {
      value: function onSubmit(e) {
        var _this = this;

        var sm = this.stateManager,
            policy = this.policy;

        sm.transitionTo("saving");
        policy.save().then(function () {
          if (!policy.isDirty) {
            _this._invalid = {};
            sm.transitionTo("initial");
            _this.didSubmit();
          }
        })["catch"](function () {
          sm.transitionTo("errored");
        });

        return babelHelpers.get(Object.getPrototypeOf(PolicyForm.prototype), "onSubmit", this).call(this, e);
      }
    },
    onCancel: {
      value: function onCancel(e) {
        this.policy.reset(this.policy._attributes);
        this.reset(this.policy.attributes);
        this.stateManager.transitionTo("initial");

        return babelHelpers.get(Object.getPrototypeOf(PolicyForm.prototype), "onCancel", this).call(this, e);
      }
    },
    reset: {
      value: function reset(data) {
        babelHelpers.get(Object.getPrototypeOf(PolicyForm.prototype), "reset", this).call(this, data);

        if (!this.policy.isNew) {
          return;
        }

        this.stateManager.transitionTo(this.isValid ? "valid" : "invalid");
      }
    }
  }, {
    classList: {
      get: function () {
        return ZForm.classList;
      }
    },
    template: {
      get: function () {
        return ZForm.template;
      }
    }
  });
  return PolicyForm;
})(ZForm);

module.exports = PolicyForm;
}});

require.define({"components/policy/matrix": function(exports, require, module) {
/* jshint camelcase: false */

"use strict";

var store = babelHelpers.interopRequire(require("lib/store"));
var ZMatrix = babelHelpers.interopRequire(require("components/z/matrix"));
var PolicyPopover = babelHelpers.interopRequire(require("./popover"));

var METRICS = Object.freeze(["first_reply_time", "requester_wait_time", "agent_work_time", "next_reply_time", "periodic_update_time", "pausable_update_time"]);

var SORTED_METRICS = Object.freeze(["requester_wait_time", "periodic_update_time", "pausable_update_time", "next_reply_time", "first_reply_time", "agent_work_time"]);

var headers = {};
var popovers = {};

var classList = ZMatrix.classList.concat(["policy-matrix"]);

var PolicyMatrix = (function (_ZMatrix) {
  function PolicyMatrix() {
    var _this = this;

    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, PolicyMatrix);

    var priorities = this.priorities = store.lookup("priorities");

    options.headers = PolicyMatrix.headers;
    options.Popover = PolicyPopover;
    options.popovers = PolicyMatrix.popovers;
    options.schema = PolicyMatrix.SCHEMA;

    babelHelpers.get(Object.getPrototypeOf(PolicyMatrix.prototype), "constructor", this).call(this, options);

    this.name = options.name;

    priorities.fetch().then(function () {
      return _this.didLoad();
    })["catch"](function (e) {
      return _this.didError(e);
    });
  }

  babelHelpers.inherits(PolicyMatrix, _ZMatrix);
  babelHelpers.createClass(PolicyMatrix, {
    name: {
      get: function () {
        return this._name;
      },
      set: function (val) {
        if (val) {
          this.el.setAttribute("name", val);
        } else {
          this.el.removeAttribute("name");
        }

        this._name = val;
      }
    },
    value: {
      get: function () {
        var rows = this.rows;
        var hours = rows[rows.length - 1].fields.map(function (field) {
          return field.selected.value;
        });
        var priorities = this.priorities;
        var metricTargets = [];

        SORTED_METRICS.forEach(function (metric) {
          var rowIndex = METRICS.indexOf(metric);
          var row = rows[rowIndex].fields;
          var val = [];

          row.forEach(function (target, colIndex) {
            var value = target.value;

            if (value <= 0) {
              return;
            }

            var item = {
              business_hours: hours[colIndex],
              metric: metric,
              priority: priorities[colIndex],
              target: value
            };

            val.push(item);
          });

          metricTargets = metricTargets.concat(val.reverse());
        });

        return metricTargets;
      },
      set: function () {
        var _this = this;

        var val = arguments[0] === undefined ? [] : arguments[0];

        if (this.populated) {
          (function () {
            var rows = _this.rows;
            var hoursRow = rows[rows.length - 1].fields;
            var priorities = _this.priorities;

            _this.clear();

            val.forEach(function (target) {
              var rowIndex = METRICS.indexOf(target.metric);
              var colIndex = priorities.indexOf(target.priority);
              var row = rows[rowIndex];
              var metricTarget = row.fields[colIndex];
              var hoursOpt = hoursRow[colIndex];

              if (metricTarget) {
                metricTarget.value = target.target;
                hoursOpt.selected = target.business_hours.toString();
              }
            });
          })();
        } else {
          this._stored = val;
        }
      }
    },
    validate: {
      value: function validate() {
        var isValid = undefined;

        if (this.loaded) {
          isValid = this.isValid = this.value.length;
        } else {
          isValid = this.isValid = false;
        }

        return isValid;
      }
    },
    clear: {
      value: function clear() {
        var rows = this.rows;
        var hoursRow = rows[rows.length - 1];

        rows.forEach(function (row) {
          if (row === hoursRow) {
            return;
          }

          row.fields.forEach(function (field) {
            field.value = 0;
          });
        });

        hoursRow.fields.forEach(function (hoursOpt) {
          hoursOpt.selected = "false";
        });
      }
    },
    onChange: {
      value: function onChange() {
        this.validate();

        babelHelpers.get(Object.getPrototypeOf(PolicyMatrix.prototype), "onChange", this).call(this);
      }
    },
    populate: {
      value: function populate() {
        var _this = this;

        if (this.loaded) {
          (function () {
            var priorities = _this.priorities;
            var headers = _this.headerOptions.headers;

            Object.keys(headers).forEach(function (priority) {
              if (priorities.indexOf(priority) === -1) {
                delete headers[priority];
              }
            });

            babelHelpers.get(Object.getPrototypeOf(PolicyMatrix.prototype), "populate", _this).call(_this);
            _this.populated = true;
          })();
        }
      }
    },
    didLoad: {
      value: function didLoad() {
        this.loaded = true;

        this.populate();

        var stored = this._stored;

        if (stored) {
          this.value = stored;
        }

        this.onChange();
      }
    },
    didError: {
      value: function didError(e) {
        console.error(e.stack);
      }
    }
  }, {
    classList: {
      get: function () {
        return classList;
      }
    },
    template: {
      get: function () {
        return ZMatrix.template;
      }
    }
  });
  return PolicyMatrix;
})(ZMatrix);

PolicyMatrix.headers = headers;
PolicyMatrix.popovers = popovers;

module.exports = PolicyMatrix;
}});

require.define({"components/policy/popover": function(exports, require, module) {
"use strict";

var ZPopover = babelHelpers.interopRequire(require("./../z/popover"));

var PolicyPopover = (function (_ZPopover) {
  function PolicyPopover(options) {
    babelHelpers.classCallCheck(this, PolicyPopover);

    var labels = PolicyPopover.labels;

    options.body = ("<h4 class=\"z-popover__heading\">" + options.heading + "</h4>\n       <p class=\"z-popover__description\">" + options.description + "</p>\n       <div class=\"status-boxes\">\n         <div class=\"status-box status-box--new\">\n           <div class=\"status-box__color\">\n             <div class=\"status-box__checkmark\"></div>\n           </div>\n           <span class=\"status-box__label\">" + labels.newLabel + "</span>\n         </div>\n         <div class=\"status-box status-box--open\">\n           <div class=\"status-box__color\">\n             <div class=\"status-box__checkmark\"></div>\n           </div>\n           <span class=\"status-box__label\">" + labels.openLabel + "</span>\n         </div>\n         <div class=\"status-box status-box--pending\">\n           <div class=\"status-box__color\">\n             <div class=\"status-box__checkmark\"></div>\n           </div>\n           <span class=\"status-box__label\">" + labels.pendingLabel + "</span>\n         </div>\n         <div class=\"status-box status-box--hold\">\n           <div class=\"status-box__color\">\n             <div class=\"status-box__checkmark\"></div>\n           </div>\n           <span class=\"status-box__label\">" + labels.holdLabel + "</span>\n         </div>\n       </div>").replace(/>\s+/, ">");

    options.footer = ("<a class='z-popover__footer__link' href=" + options.href + " target='_blank'>" + options.linkText + "</a>").replace(/>\s+/, ">");

    babelHelpers.get(Object.getPrototypeOf(PolicyPopover.prototype), "constructor", this).call(this, options);
  }

  babelHelpers.inherits(PolicyPopover, _ZPopover);
  babelHelpers.createClass(PolicyPopover, null, {
    classList: {
      get: function () {
        return ZPopover.classList;
      }
    },
    template: {
      get: function () {
        return ZPopover.template;
      }
    }
  });
  return PolicyPopover;
})(ZPopover);

PolicyPopover.labels = {};

module.exports = PolicyPopover;
}});

require.define({"components/z/capsule": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var ZDropdown = babelHelpers.interopRequire(require("components/z/dropdown"));
var ZGrabber = babelHelpers.interopRequire(require("components/z/grabber"));

var ZCapsule = (function (_ZComponent) {
  function ZCapsule(options) {
    babelHelpers.classCallCheck(this, ZCapsule);

    babelHelpers.get(Object.getPrototypeOf(ZCapsule.prototype), "constructor", this).call(this, options);

    this.item = options.item;

    if (!this.title) {
      this.heading.textContent = this.headingPlaceholder;
    }
  }

  babelHelpers.inherits(ZCapsule, _ZComponent);
  babelHelpers.createClass(ZCapsule, {
    events: {
      get: function () {
        return {
          "focus^": "onFocus",
          "blur^": "onBlur",
          "click .z-capsule__header": "onClick",
          keydown: "onKeyDown"
        };
      }
    },
    item: {
      get: function () {
        return this._item;
      },
      set: function () {
        var val = arguments[0] === undefined ? {} : arguments[0];

        var item = val,
            isNew = !!item.isNew;

        this.isNew = isNew;
        this.title = item.title;
        this.subtitle = item.subtitle;
        this.active = isNew;

        this._item = item;
      }
    },
    title: {
      get: function () {
        return this._title || "";
      },
      set: function (val) {
        this._title = val;

        if (val) {
          this.heading.classList.remove("z-capsule__heading--empty");
          this.heading.textContent = val;
        } else {
          this.heading.classList.add("z-capsule__heading--empty");
          this.heading.textContent = this.headingPlaceholder;
        }
      }
    },
    subtitle: {
      get: function () {
        return this._subtitle || "";
      },
      set: function (val) {
        this._subtitle = val;
        this.subheading.textContent = this.subtitle;
      }
    },
    headingPlaceholder: {
      get: function () {
        return this.isNew ? this.constructor.EMPTY_HEADING : "";
      }
    },
    menu: {
      get: function () {
        return [];
      }
    },
    active: {
      get: function () {
        return this._active;
      },
      set: function (val) {
        var _this = this;

        this._active = val;

        if (this.active) {
          this.body.style.display = "block";
          setTimeout(function () {
            _this.el.classList.add("z-capsule--open");
            _this.focusInput();
          }, 0);
        } else {
          this.el.classList.remove("z-capsule--open");
          setTimeout(function () {
            if (_this._active) {
              return;
            }

            _this.body.style.display = "none";
          }, 200);
        }
      }
    },
    isNew: {
      get: function () {
        return !!this._isNew;
      },
      set: function (val) {
        this.el.classList[val ? "add" : "remove"]("z-capsule--new");
        this._isNew = val;
      }
    },
    focusInput: {
      value: function focusInput() {
        var input = this.$("input");

        if (input) {
          input.focus();
        }
      }
    },
    setElement: {
      value: function setElement(el) {
        var dropdown = this.dropdown = new ZDropdown({
          items: this.menu,
          classList: ["z-capsule__options", "z-dropdown--right"]
        });

        this.dropdown.on("selected", this.onMenuSelect, this);

        babelHelpers.get(Object.getPrototypeOf(ZCapsule.prototype), "setElement", this).call(this, el);

        this.el.setAttribute("tabindex", "0");
        this.heading = this.$(".z-capsule__heading");
        this.subheading = this.$(".z-capsule__subheading");
        this.body = this.$(".z-capsule__body");

        dropdown.appendTo(this.$(".z-capsule__header"));
        this.grabber = new ZGrabber();
        this.grabber.appendTo(this.$(".z-capsule__header"));
      }
    },
    addTo: {
      value: function addTo(target) {
        return this[this.isNew ? "prependTo" : "appendTo"](target);
      }
    },
    onClick: {
      value: function onClick(e) {
        var target = e.target,
            dropdown = this.dropdown.el,
            grabber = this.grabber.el;

        if (dropdown === target || dropdown.contains(target)) {
          return;
        }

        if (grabber === target || grabber.contains(target)) {
          return;
        }

        this.active = !this.active || !!this.isNew;
      }
    },
    onKeyDown: {
      value: function onKeyDown(e) {
        var active = this.active,
            keyCode = e.keyCode,
            target = e.target;

        if (keyCode === 13 && !active && !this.dropdown.el.contains(target)) {
          this.active = true;
        } else if (keyCode === 27 && active && target === this.el) {
          this.active = false;
        }
      }
    },
    onFocus: {
      value: function onFocus() {
        this.el.classList.add("z-capsule--focused");
      }
    },
    onBlur: {
      value: function onBlur() {
        var el = this.el;

        setTimeout(function () {
          var activeElement = document.activeElement;

          if (el === activeElement || el.contains(activeElement)) {
            return;
          }

          el.classList.remove("z-capsule--focused");
        }, 0);
      }
    },
    onMenuSelect: {
      value: function onMenuSelect(option) {
        this.emit("selected", option);
      }
    },
    remove: {
      value: function remove() {
        this.dropdown.remove();
        babelHelpers.get(Object.getPrototypeOf(ZCapsule.prototype), "remove", this).call(this);

        if (this.parentView) {
          this.parentView.removeChildView(this);
        }
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-capsule", "u-br-lg"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template;

        template.fragment.firstChild.innerHTML = "<header class=\"z-capsule__header\">\n        <h5 class=\"z-capsule__heading\"></h5>\n        <span class=\"z-capsule__subheading\"></span>\n      </header>\n      <div class=\"z-capsule__body\"></div>".replace(/>\s+/, ">");

        return template;
      }
    }
  });
  return ZCapsule;
})(ZComponent);

mixin(ZCapsule.prototype, Eventable);

module.exports = ZCapsule;
}});

require.define({"components/z/capsules": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Sortable = babelHelpers.interopRequire(require("mixins/sortable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var ZCapsule = babelHelpers.interopRequire(require("./capsule"));

var ZCapsules = (function (_ZComponent) {
  function ZCapsules() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZCapsules);

    babelHelpers.get(Object.getPrototypeOf(ZCapsules.prototype), "constructor", this).call(this, options);

    this.views = [];
    this.items = options.items;

    this._sorted = !!options.sortable;
  }

  babelHelpers.inherits(ZCapsules, _ZComponent);
  babelHelpers.createClass(ZCapsules, {
    items: {
      get: function () {
        return this._items;
      },
      set: function () {
        var _this = this;

        var val = arguments[0] === undefined ? [] : arguments[0];

        this.clear();
        this._items = val;
        val.forEach(function (item) {
          return _this.addChildView(item);
        });
      }
    },
    addChildView: {
      value: function addChildView(item) {
        var _this = this;

        var view = new this.constructor.Capsule({
          item: item,
          classList: ["z-capsules__capsule"]
        });

        view.parentView = this;
        view.addTo(this.el);

        if (view.active) {
          view.focusInput();
        }

        if (this._sorted) {
          this.sortable({
            onUpdate: function (e) {
              return _this.onReorder(e);
            }
          });
        }

        this.views.push(view);

        return view;
      }
    },
    removeChildView: {
      value: function removeChildView(view) {
        var views = this.views;

        this.views = views.reduce(function (active, v) {
          if (view !== v) {
            active.push(v);
          }

          return active;
        }, []);
      }
    },
    clear: {
      value: function clear() {
        this.views.forEach(function (view) {
          return view.remove();
        });
        this.views = [];
        this._items = [];
      }
    },
    remove: {
      value: function remove() {
        if (this._sorted) {
          this.unsortable();
        }

        this.clear();
        babelHelpers.get(Object.getPrototypeOf(ZCapsules.prototype), "remove", this).call(this);
      }
    },
    onReorder: {
      value: function onReorder() {
        throw new Error("ZCapsules#onReorder is unimplemented.");
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-capsules"];
      }
    },
    template: {
      get: function () {
        return Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);
      }
    },
    Capsule: {
      get: function () {
        return ZCapsule;
      }
    }
  });
  return ZCapsules;
})(ZComponent);

mixin(ZCapsules.prototype, Sortable);

module.exports = ZCapsules;
}});

require.define({"components/z/condition": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var observer = babelHelpers.interopRequire(require("lib/insertion-observer"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var Expression = babelHelpers.interopRequire(require("./condition/expression"));
var Parts = babelHelpers.interopRequire(require("./condition/parts"));

var slice = Array.prototype.slice;
var TERMINAL_OPERATORS = Object.freeze(["present", "not_present"]);
var Search = Parts.autocomplete;
var List = Parts.list;

var ZCondition = (function (_ZComponent) {
  function ZCondition() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZCondition);

    var labels = ZCondition.labels,
        definitions = this.definitions = options.definitions,
        groups = [],
        cache = {};

    this.views = {};
    this.inDOM = false;
    this.expression = options.expression;

    slice.call(definitions || []).reduce(function (groups, definition) {
      var type = definition.group,
          group = cache[type];

      if (group) {
        group.push(definition);
      } else {
        group = cache[type] = [definition];
        groups.push([{ label: labels[type] || type }], group);
      }

      return groups;
    }, groups);
    this.fields = Array.prototype.concat.apply([], groups);

    babelHelpers.get(Object.getPrototypeOf(ZCondition.prototype), "constructor", this).call(this, options);
  }

  babelHelpers.inherits(ZCondition, _ZComponent);
  babelHelpers.createClass(ZCondition, {
    events: {
      get: function () {
        return {
          "click .z-condition__btn--remove": "remove",
          "focus^": "onFocus",
          "blur^": "onBlur"
        };
      }
    },
    active: {
      get: function () {
        return this._active;
      },
      set: function (val) {
        this._active = val;

        this.el.classList[val ? "add" : "remove"]("z-condition--active");
      }
    },
    expression: {
      get: function () {
        return this._expr;
      },
      set: function (val) {
        if (!(val instanceof Expression)) {
          val = new Expression(val);
        }

        this._expr = val;
        val.delegate = this;
      }
    },
    definition: {
      get: function () {
        return this._definition;
      },
      set: function (val) {
        var definition = this._definition = this.definitions.lookup(val);

        if (!definition) {
          this.type = null;
          this.operators = [];
          this.values = [];
          return;
        }

        this.type = definition.type;
        this.operators = definition.operators;
        this.values = definition.values;
      }
    },
    appendTo: {
      value: function appendTo(target) {
        babelHelpers.get(Object.getPrototypeOf(ZCondition.prototype), "appendTo", this).call(this, target);

        if (document.body.contains(target)) {
          this.didInsertElement();
        } else {
          observer.observe(this, this.didInsertElement);
        }
      }
    },
    load: {
      value: function load() {
        var _this = this;

        var expr = this.expression,
            views = this.views;

        if (expr.isEmpty) {
          views.field.onChange = function (item) {
            return _this.fieldDidChange(item);
          };
          return;
        }

        Object.keys(expr).forEach(function (name) {
          var view = views[name],
              value = expr[name];

          if (value == null || !view) {
            return;
          } // jshint ignore:line

          if (view instanceof Search) {
            view.menu.activeItem = {
              label: expr.meta ? expr.meta.label : value,
              value: value
            };
            view.setValue(value);
          } else if (view instanceof List || view instanceof Parts.tags) {
            view.setValue(value);
          } else {
            view.value = value;
          }

          view.onChange = function (item) {
            return _this[name + "DidChange"](item);
          };
        });
      }
    },
    remove: {
      value: function remove() {
        var views = this.views;

        Object.keys(views).forEach(function (name) {
          return views[name] && views[name].remove();
        });
        babelHelpers.get(Object.getPrototypeOf(ZCondition.prototype), "remove", this).call(this);
        this.emit("remove", this);
      }
    },
    didInsertElement: {
      value: function didInsertElement() {
        var _this = this;

        var field = this.views.field = new List({
          data: this.fields,
          clsSelectMenuScope: "zd-combo-selectmenu z-condition__field__menu",
          placeholder: this.constructor.PLACEHOLDER,
          onChange: function (item) {
            return _this.loadField(item, _this.loadOperator.bind(_this));
          }
        });

        field.appendTo(this.$(".z-condition__field"));

        this.load();
        this.inDOM = true;
      }
    },
    loadField: {
      value: function loadField(selected, onChange) {
        this.expression.field = selected.value;
        this.definition = selected.value;

        var views = this.views,
            operator = views.operator,
            value = views.value;

        if (value) {
          value.remove();
          this.expression.value = views.value = null;
        }

        if (!operator) {
          operator = this.views.operator = new List({
            data: this.operators,
            clsSelectMenuScope: "zd-combo-selectmenu z-condition__operator__menu",
            onChange: function (item) {
              return onChange(item);
            }
          });
          operator.appendTo(this.$(".z-condition__operator"));
        } else {
          operator.loadData(this.operators);
        }
      }
    },
    fieldDidChange: {
      value: function fieldDidChange(selected) {
        this.loadField(selected, this.operatorDidChange.bind(this));

        this.views.operator.open();
      }
    },
    loadOperator: {
      value: function loadOperator(selected) {
        var _this = this;

        var operator = this.expression.operator = selected.value,
            views = this.views,
            values = this.values,
            value = views.value,
            definition = this.definition;

        if (value) {
          value.remove();
        }

        if (TERMINAL_OPERATORS.indexOf(operator) !== -1) {
          this.expression.value = views.value = null;
          return;
        }

        value = views.value = new Parts[definition.type]({
          data: values,
          url: definition.url,
          onChange: function (item) {
            return _this.valueDidChange(item);
          }
        });
        value.appendTo(this.$(".z-condition__value"));
      }
    },
    operatorDidChange: {
      value: function operatorDidChange(selected) {
        this.loadOperator(selected);

        var views = this.views,
            value = views.value;

        if (this.definition.type === "list" && this.inDOM) {
          setTimeout(function () {
            return value && value.open();
          }, 0);
        } else {
          views.operator.blur();
          value.focus();
        }
      }
    },
    valueDidChange: {
      value: function valueDidChange(selected) {
        this.expression.value = selected.value;
      }
    },
    onChange: {
      value: function onChange(name, value) {
        this.emit("change", name, value);
      }
    },
    onFocus: {
      value: function onFocus() {
        this.active = true;
      }
    },
    onBlur: {
      value: function onBlur() {
        var _this = this;

        setTimeout(function () {
          if (_this.el.contains(document.activeElement)) {
            return;
          }
          _this.active = false;
        }, 0);
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-condition"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template;

        template.fragment.firstChild.innerHTML = "<div class=\"z-condition__part z-condition__field\"></div>\n       <div class=\"z-condition__part z-condition__operator\"></div>\n       <div class=\"z-condition__part z-condition__value\"></div>\n       <button class=\"c-btn z-condition__btn--remove\" type=\"button\">×</button>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return ZCondition;
})(ZComponent);

ZCondition.Parts = Parts;
ZCondition.PLACEHOLDER = "";
ZCondition.labels = {};

mixin(ZCondition.prototype, Eventable);

module.exports = ZCondition;
}});

require.define({"components/z/conditions-fieldset": function(exports, require, module) {
"use strict";

var store = babelHelpers.interopRequire(require("lib/store"));

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var ZConditions = babelHelpers.interopRequire(require("./conditions"));

function Label(text) {
  var el = this.el = document.createElement("label");

  el.className = "z-conditions__label";
  el.innerHTML = text;
}

var ZConditionsFieldset = (function (_ZComponent) {
  function ZConditionsFieldset() {
    var _this = this;

    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZConditionsFieldset);

    var definitions = this.definitions = store.lookup("definition-set");

    babelHelpers.get(Object.getPrototypeOf(ZConditionsFieldset.prototype), "constructor", this).call(this, options);

    this.name = options.name;

    definitions.fetch().then(function () {
      return _this.didLoad();
    })["catch"](function (e) {
      return _this.didError(e);
    });
  }

  babelHelpers.inherits(ZConditionsFieldset, _ZComponent);
  babelHelpers.createClass(ZConditionsFieldset, {
    name: {
      get: function () {
        return this._name;
      },
      set: function (val) {
        if (val) {
          this.el.setAttribute("name", val);
        } else {
          this.el.removeAttribute("name");
        }

        this._name = val;
      }
    },
    value: {
      get: function () {
        var all = this.allConditions.value,
            any = this.anyConditions.value;

        return { all: all, any: any };
      },
      set: function (val) {
        if (this.loaded) {
          this.allConditions.value = val.all;
          this.anyConditions.value = val.any;
        } else {
          this._stored = val;
        }
      }
    },
    validate: {
      value: function validate() {
        this.isValid = true;

        return this.isValid;
      }
    },
    remove: {
      value: function remove() {
        this.allConditions.remove();
        this.anyConditions.remove();
        babelHelpers.get(Object.getPrototypeOf(ZConditionsFieldset.prototype), "remove", this).call(this);
      }
    },
    onChange: {
      value: function onChange() {
        this.validate();

        this.emit("change", this, this.value);
      }
    },
    didLoad: {
      value: function didLoad() {
        var el = this.el,
            labels = ZConditionsFieldset.labels,
            definitions = this.definitions,
            label;

        this.allConditions = new ZConditions({
          definitions: definitions.all
        });

        this.anyConditions = new ZConditions({
          definitions: definitions.any
        });

        this.allConditions.on("change", this.onChange, this);
        this.anyConditions.on("change", this.onChange, this);

        label = new Label(labels.all);
        el.appendChild(label.el);
        this.allConditions.appendTo(el);

        label = new Label(labels.any);
        el.appendChild(label.el);
        this.anyConditions.appendTo(el);

        this.loaded = true;

        if (this._stored) {
          this.allConditions.value = this._stored.all;
          this.anyConditions.value = this._stored.any;
        }

        this.onChange();
      }
    },
    didError: {
      value: function didError(e) {
        throw e;
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-conditions-fieldset"];
      }
    },
    tagName: {
      get: function () {
        return "div";
      }
    },
    template: {
      get: function () {
        return Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);
      }
    }
  });
  return ZConditionsFieldset;
})(ZComponent);

mixin(ZConditionsFieldset.prototype, Eventable);

ZConditionsFieldset.labels = {};

module.exports = ZConditionsFieldset;
}});

require.define({"components/z/conditions": function(exports, require, module) {
"use strict";

var _libUtils = require("lib/utils");

var mixin = _libUtils.mixin;
var isEmpty = _libUtils.isEmpty;
var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var ZCondition = babelHelpers.interopRequire(require("./condition"));

var ZConditions = (function (_ZComponent) {
  function ZConditions() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZConditions);

    this.views = [];
    this.definitions = options.definitions;

    babelHelpers.get(Object.getPrototypeOf(ZConditions.prototype), "constructor", this).call(this, options);

    this.value = options.value;
  }

  babelHelpers.inherits(ZConditions, _ZComponent);
  babelHelpers.createClass(ZConditions, {
    value: {
      get: function () {
        return this.views.reduce(function (serialized, view) {
          if (!view.expression.isValid) {
            return serialized;
          }

          serialized.push(view.expression.toJSON());

          return serialized;
        }, []);
      },
      set: function (val) {
        var _this = this;

        var views = this.views;

        views.forEach(function (view) {
          view.off();
          view.remove();
        });
        views = this.views = [];

        if (!isEmpty(val)) {
          views = val.map(function (data) {
            return _this.add(data);
          });
          this.add();
        }

        if (views.length === 0) {
          this.add();
        }
      }
    },
    add: {
      value: function add() {
        var expr = arguments[0] === undefined ? {} : arguments[0];

        var view = new ZCondition({
          expression: expr,
          definitions: this.definitions
        });

        view.appendTo(this.el);
        view.on("change", this.onChange, this);
        view.on("remove", this.onRemove, this);

        this.views.push(view);

        return view;
      }
    },
    remove: {
      value: function remove() {
        this.views.forEach(function (view) {
          return view.remove();
        });

        babelHelpers.get(Object.getPrototypeOf(ZConditions.prototype), "remove", this).call(this);
      }
    },
    onChange: {
      value: function onChange() {
        var isComplete;

        this.emit("change");
        isComplete = this.views.every(function (view) {
          return view.expression.isValid;
        });

        if (isComplete) {
          this.add();
        }
      }
    },
    onRemove: {
      value: function onRemove(view) {
        var index = this.views.indexOf(view);

        this.views.splice(index, 1);
        this.onChange();
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-conditions"];
      }
    },
    template: {
      get: function () {
        return Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);
      }
    }
  });
  return ZConditions;
})(ZComponent);

ZConditions.Condition = ZCondition;

mixin(ZConditions.prototype, Eventable);

module.exports = ZConditions;
}});

require.define({"components/z/datepicker": function(exports, require, module) {
"use strict";

var I18n = babelHelpers.interopRequire(require("lib/i18n"));
var moment = babelHelpers.interopRequire(require("moment"));
var Pikaday = babelHelpers.interopRequire(require("pikaday"));

var toString = Object.prototype.toString;

var DAYS_MAP = {
  sun: 0,
  mon: 1,
  tue: 2,
  wed: 3,
  thu: 4,
  fri: 5,
  sat: 6
};

var FIRST_DAY = DAYS_MAP[I18n.t("first_day_of_the_week")];

var TRANSLATIONS = {
  previousMonth: "",
  nextMonth: "",
  months: [I18n.t("months.wide.1"), I18n.t("months.wide.2"), I18n.t("months.wide.3"), I18n.t("months.wide.4"), I18n.t("months.wide.5"), I18n.t("months.wide.6"), I18n.t("months.wide.7"), I18n.t("months.wide.8"), I18n.t("months.wide.9"), I18n.t("months.wide.10"), I18n.t("months.wide.11"), I18n.t("months.wide.12")],
  weekdays: [I18n.t("days.wide.7"), I18n.t("days.wide.1"), I18n.t("days.wide.2"), I18n.t("days.wide.3"), I18n.t("days.wide.4"), I18n.t("days.wide.5"), I18n.t("days.wide.6")],
  weekdaysShort: [I18n.t("days.abbreviated.7"), I18n.t("days.abbreviated.1"), I18n.t("days.abbreviated.2"), I18n.t("days.abbreviated.3"), I18n.t("days.abbreviated.4"), I18n.t("days.abbreviated.5"), I18n.t("days.abbreviated.6")]
};

var STRFTIME_TO_MOMENT = {
  a: "ddd",
  A: "dddd",
  b: "MMM",
  B: "MMMM",
  "-d": "D",
  d: "DD",
  "-H": "H",
  H: "HH",
  "-I": "h",
  I: "hh",
  j: "DDDD",
  "-k": "H",
  k: "HH", // should be blank padded
  "-l": "h",
  l: "hh", // should be blank padded
  m: "MM",
  "-m": "M",
  M: "mm",
  p: "A",
  S: "ss",
  Z: "z",
  w: "d",
  y: "YY",
  Y: "YYYY",
  "%": "%"
};

moment.fn.strftime = function (format) {
  var self = this;

  return format.replace(/%(-?.)/g, function (str, substr) {
    return self.format(STRFTIME_TO_MOMENT[substr]);
  });
};

var Datepicker = (function (_Pikaday) {
  function Datepicker() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, Datepicker);

    var onSelect = options.onSelect;

    options.setDefaultDate = !!options.defaultDate;
    options.firstDay = Datepicker.FIRST_DAY;
    options.i18n = Datepicker.TRANSLATIONS;

    if (onSelect) {
      options.onSelect = function (date) {
        var d = moment(date);

        onSelect(d);
      };
    }

    this.format = options.format;
    Pikaday.call(this, options);
  }

  babelHelpers.inherits(Datepicker, _Pikaday);
  babelHelpers.createClass(Datepicker, {
    adjustPosition: {
      value: function adjustPosition() {
        var field = this._o.trigger,
            pEl = field,
            width = this.el.offsetWidth,
            height = this.el.offsetHeight,
            viewportWidth = window.innerWidth || document.documentElement.clientWidth,
            viewportHeight = window.innerHeight || document.documentElement.clientHeight,
            scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop,
            left,
            top,
            clientRect,
            positionClasses;

        if (typeof field.getBoundingClientRect === "function") {
          clientRect = field.getBoundingClientRect();
          left = clientRect.left + window.pageXOffset;
          top = clientRect.bottom + window.pageYOffset;
        } else {
          left = pEl.offsetLeft;
          top = pEl.offsetTop + pEl.offsetHeight;
          while (pEl = pEl.offsetParent) {
            left += pEl.offsetLeft;
            top += pEl.offsetTop;
          }
        }

        positionClasses = ["bottom", "left"];

        // default position is bottom & left
        if (left + width > viewportWidth || this._o.position.indexOf("right") > -1 && left - width + field.offsetWidth > 0) {
          left = left - width + field.offsetWidth;
          positionClasses[1] = "right";
        } else {
          left += field.offsetWidth - width / 2;
        }

        if (top + height > viewportHeight + scrollTop || this._o.position.indexOf("top") > -1 && top - height - field.offsetHeight > 0) {
          top = top - height - field.offsetHeight;
          positionClasses[0] = "top";
        }

        this.el.classList.remove("pika-top-left", "pika-top-right", "pika-bottom-left", "pika-bottom-right");
        this.el.classList.add("pika-" + positionClasses.join("-"));
        this.el.style.cssText = ["position: absolute", "left: " + left + "px", "top: " + top + "px"].join(";");
      }
    },
    toString: {
      value: (function (_toString) {
        var _toStringWrapper = function toString() {
          return _toString.apply(this, arguments);
        };

        _toStringWrapper.toString = function () {
          return _toString.toString();
        };

        return _toStringWrapper;
      })(function () {
        if (toString.call(this._d) === "[object Date]") {
          return moment(this._d).strftime(this.format);
        } else {
          return this._d.toDateString();
        }
      })
    }
  });
  return Datepicker;
})(Pikaday);

Datepicker.FIRST_DAY = FIRST_DAY;
Datepicker.TRANSLATIONS = TRANSLATIONS;

module.exports = Datepicker;
}});

require.define({"components/z/dropdown": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var ZDropdownMenu = babelHelpers.interopRequire(require("components/z/dropdown/menu"));

var KEYCODES = {
  ENTER: 13,
  ESCAPE: 27,
  SPACEBAR: 32,
  UP: 38,
  DOWN: 40
};

function onFocus(e) {
  var _this = this;

  /**
   * WebKit unfortunately changes focus on click differently from other
   * engines, not focusing an interactive element on click. See this gist for
   * details on how this behavior is handled across browsers,
   * https://gist.github.com/cvrebert/68659d0333a578d75372.
   *
   * ref: https://bugs.webkit.org/show_bug.cgi?id=22261
   */
  if (this._clickfocus) {
    this.toggle.focus();
    this._clickfocus = false;
    return;
  }

  var toggle = this.toggle,
      menu = this.menu;

  if (!(e.target instanceof Node) || e.target === toggle || menu.el.contains(e.target)) {
    return;
  }

  setTimeout(function () {
    _this.open = false;
  }, 0);
}

function onClickOut(e) {
  var target = e.target;

  if (target === this.toggle || target.classList.contains("z-dropdown__menuitem--disabled")) {
    e.stopPropagation();
    return;
  }

  this.open = false;
}

/**
 * A dropdown component.
 *
 * @class
 */

var ZDropdown = (function (_ZComponent) {
  /**
   * @constructor
   */

  function ZDropdown() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZDropdown);

    var el = options.el,
        menu = this.menu = new ZDropdownMenu(options.items);

    if (!el) {
      el = options.el = this.constructor.template.content.firstChild;
    }

    el.appendChild(menu.el);
    this.toggle = el.querySelector(".z-dropdown__toggle");
    this.selectable = !!options.selectable;
    this.placeholder = options.placeholder;
    this._clickfocus = false;

    if (this.selectable) {
      this.selected = options.selected;
    }

    babelHelpers.get(Object.getPrototypeOf(ZDropdown.prototype), "constructor", this).call(this, options);
  }

  babelHelpers.inherits(ZDropdown, _ZComponent);
  babelHelpers.createClass(ZDropdown, {
    events: {

      /**
       * DOM events mapping to event handlers.
       *
       * @type {object}
       */

      get: function () {
        return {
          "focus .z-dropdown__toggle": "onToggleFocus",
          "mousedown .z-dropdown__toggle": "onToggleClick",
          "click .z-dropdown__menu": "onSelect",
          keydown: "onKeyDown"
        };
      }
    },
    open: {

      /**
       * Gets open state for menu.
       *
       * @type {boolean}
       */

      get: function () {
        return this.menu.open;
      },

      /**
       * Sets open state for menu.
       *
       * @param {boolean} val Open/close state.
       */
      set: function (val) {
        this.menu.open = val;
        this.el.classList[val ? "add" : "remove"]("z-dropdown--open");
        window.removeEventListener("click", this._onClickOut, true);
        window.removeEventListener("focus", this._onFocus, true);
        this._onClickOut = this._onFocus = null;

        if (val) {
          this._onClickOut = onClickOut.bind(this);
          this._onFocus = onFocus.bind(this);
          window.addEventListener("click", this._onClickOut, true);
          window.addEventListener("focus", this._onFocus, true);
        }
      }
    },
    placeholder: {
      get: function () {
        return this._placeholder || "-";
      },
      set: function (val) {
        this._placeholder = val;
      }
    },
    selected: {

      /**
       * Gets selected item.
       *
       * @type {object}
       */

      get: function () {
        return this._selected;
      },

      /**
       * Sets selected item.
       *
       * @param {object} val Selected item.
       */
      set: function (val) {
        if (!this.selectable) {
          return;
        }

        var menu = this.menu;

        menu.select(val);
        this._selected = menu.selected;
        this.toggle.innerHTML = val ? this._selected.title : this.placeholder;
      }
    },
    navigateUp: {

      /**
       * Navigates up in dropdown menu.
       */

      value: function navigateUp() {
        this._navigate(-1);
      }
    },
    navigateDown: {

      /**
       * Navigates down in dropdown menu.
       */

      value: function navigateDown() {
        this._navigate(1);
      }
    },
    _navigate: {

      /**
       * Navigates directions in dropdown menu.
       *
       * @private
       *
       * @param {number} dir Direction 1 (down) or -1 (up)
       */

      value: function _navigate(dir) {
        var activeElement = document.activeElement,
            asc = dir < 0,
            sibling,
            stop;

        if (activeElement) {
          sibling = activeElement["" + (asc ? "previous" : "next") + "Sibling"];
        }

        if (activeElement !== this.toggle && sibling) {
          stop = sibling;
        } else {
          stop = this.menu.el["" + (asc ? "last" : "first") + "Child"];
        }

        stop.focus();

        if (stop.classList.contains("z-dropdown__menuitem--disabled")) {
          this._navigate(dir);
        }
      }
    },
    disable: {

      /**
       * Disables item in dropdown menu.
       *
       * @param {string} item Item id
       */

      value: function disable(item) {
        this.menu.toggle(item, false);
      }
    },
    enable: {

      /**
       * Enables item in dropdown menu.
       *
       * @param {string} item Item id
       */

      value: function enable(item) {
        this.menu.toggle(item, true);
      }
    },
    remove: {
      value: function remove() {
        this.open = false;
        this.menu.remove();
        babelHelpers.get(Object.getPrototypeOf(ZDropdown.prototype), "remove", this).call(this);
      }
    },
    onKeyDown: {

      /**
       * Keydown handler for toggle and menu.
       *
       * @param {KeyboardEvent} e 'keydown' event.
       */

      value: function onKeyDown(e) {
        if (e.shiftKey || e.metaKey || e.altKey) {
          return;
        }

        switch (e.keyCode) {
          case KEYCODES.SPACEBAR:
          case KEYCODES.ENTER:
            if (e.target !== this.toggle) {
              this.onSelect({
                target: document.activeElement
              });
              this.open = false;
            } else {
              this.onToggleClick(e);
            }

            e.preventDefault();

            break;
          case KEYCODES.ESCAPE:
            this.open = false;
            break;
          case KEYCODES.UP:
            this.open = true;
            e.preventDefault();
            this.navigateUp();
            break;
          case KEYCODES.DOWN:
            this.open = true;
            e.preventDefault();
            this.navigateDown();
            break;
          default:
        }
      }
    },
    onToggleFocus: {
      value: function onToggleFocus() {
        if (!this.open) {
          this.open = true;
        }
      }
    },
    onToggleClick: {

      /**
       * Toggle handler for click/focus events.
       *
       * @param {MouseEvent|FocusEvent} e Click/focus event from toggle.
       */

      value: function onToggleClick(e) {
        var open = !this.open;

        if (open) {
          this._clickfocus = true;
        } else {
          e.preventDefault();
        }

        this.open = open;
      }
    },
    onSelect: {

      /**
       * Select handler for click events from menu.
       *
       * @param {MouseEvent} e Click event from menu.
       */

      value: function onSelect(e) {
        var target = e.target,
            classList = target.classList,
            selected;

        if (!classList.contains("z-dropdown__menuitem") || classList.contains("z-dropdown__menuitem--disabled") || classList.contains("z-dropdown__menuitem--selected")) {
          return;
        }

        selected = target.getAttribute("data-selection-id");

        if (this.selectable) {
          this.selected = selected;
        }

        this.emit("selected", selected);
      }
    }
  }, {
    template: {

      /**
       * Gets template for component.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        var template = ZComponent.template,
            el = template.fragment.firstChild,
            toggle = document.createElement("button");

        toggle.classList.add("z-dropdown__toggle");
        toggle.setAttribute("type", "button");
        toggle.setAttribute("aria-has-popup", "true");
        el.appendChild(toggle);

        return template;
      }
    },
    classList: {

      /**
       * List of default class names for dropdown component.
       *
       * @type {string[]}
       */

      get: function () {
        return ["z-dropdown"];
      }
    }
  });
  return ZDropdown;
})(ZComponent);

mixin(ZDropdown.prototype, Eventable);

module.exports = ZDropdown;
}});

require.define({"components/z/duration": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));

function interpolate(str, value) {
  return str.replace(/\{(\w)\}/g, value);
}

var ZDuration = (function (_ZComponent) {
  function ZDuration() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZDuration);

    var labels = ZDuration.labels;

    babelHelpers.get(Object.getPrototypeOf(ZDuration.prototype), "constructor", this).call(this, options);

    this._defaultPlaceholder = labels.placeholder;
    this.placeholder = this.formattedValue;
    this.$(".z-duration__hours + .z-duration__label__text").textContent = labels.hours;
    this.$(".z-duration__mins + .z-duration__label__text").textContent = labels.mins;
  }

  babelHelpers.inherits(ZDuration, _ZComponent);
  babelHelpers.createClass(ZDuration, {
    events: {
      get: function () {
        return {
          "blur .z-duration__input": "onBlur",
          "focus .z-duration__placeholder": "activateInputs",
          keydown: "onKeydown"
        };
      }
    },
    placeholder: {
      get: function () {
        return this._placeholder || this._defaultPlaceholder;
      },
      set: function (val) {
        var placeholder = this.$(".z-duration__placeholder");

        this._placeholder = val;
        placeholder.textContent = this.placeholder;
        placeholder.classList[val ? "remove" : "add"]("z-duration__placeholder--empty");
      }
    },
    active: {
      get: function () {
        return this._active;
      },
      set: function (val) {
        this.el.classList[val ? "add" : "remove"]("z-duration--active");
        this._active = val;
      }
    },
    value: {
      get: function () {
        return this._value || null;
      },
      set: function (val) {
        if (this._value === val) {
          return;
        }

        this._value = val;
        this.placeholder = ZDuration.formatValue(val);
      }
    },
    formattedValue: {
      get: function () {
        return ZDuration.formatValue(this.value);
      }
    },
    updateValue: {
      value: function updateValue() {
        var value = Math.floor(Number(this.$(".z-duration__hours").value)) * 60 + Math.floor(Number(this.$(".z-duration__mins").value)) || null;

        if (this.value === value) {
          return;
        }

        this.value = value;
        this.emit("change", this, value);
      }
    },
    activateInputs: {
      value: function activateInputs() {
        var val = this.value;

        this.$(".z-duration__hours").value = Math.floor(val / 60) || "";
        this.$(".z-duration__mins").value = Math.floor(val % 60) || "";

        this.active = true;
        this.$(".z-duration__hours").focus();
      }
    },
    hideInputs: {
      value: function hideInputs() {
        this.updateValue();
        this.active = false;
      }
    },
    delegateEvents: {
      value: function delegateEvents() {
        babelHelpers.get(Object.getPrototypeOf(ZDuration.prototype), "delegateEvents", this).call(this);

        this._onBlur = this.onBlur.bind(this);

        this.el.addEventListener("blur", this._onBlur, true);
      }
    },
    undelegateEvents: {
      value: function undelegateEvents() {
        babelHelpers.get(Object.getPrototypeOf(ZDuration.prototype), "undelegateEvents", this).call(this);

        if (!this.el) {
          return;
        }

        this.el.removeEventListener("blur", this._onBlur, true);
      }
    },
    onBlur: {
      value: function onBlur() {
        var _this = this;

        setTimeout(function () {
          var activeElement = document.activeElement;

          if (_this.el.contains(activeElement)) {
            return;
          }

          _this.hideInputs();
        }, 0);
      }
    },
    onKeydown: {
      value: function onKeydown(e) {
        var _this = this;

        var keyCode = e.keyCode,
            onKeyup;

        if (this.active) {
          var allowedKeys = [13],
              modifier = e.ctrlKey || e.metaKey;

          for (var i = 48; i < 58; i++) {
            allowedKeys.push(i);
            allowedKeys.push(i + 48);
          }

          if (keyCode === 0) {
            keyCode = e.charCode;
          } else if (keyCode !== e.charCode) {
            allowedKeys = allowedKeys.concat([8, 9, 37, 38, 39, 40]);
          }

          if (allowedKeys.indexOf(keyCode) < 0 && !modifier) {
            e.preventDefault();
            return;
          }

          onKeyup = function () {
            _this.updateValue();
            _this.el.removeEventListener("keyup", onKeyup, false);
          };

          this.el.addEventListener("keyup", onKeyup, false);
        }
      }
    }
  }, {
    formatValue: {
      value: function formatValue() {
        var value = arguments[0] === undefined ? 0 : arguments[0];

        var hours = Math.floor(value / 60),
            mins = Math.floor(value % 60),
            formattedValue = "",
            labels = this.labels;

        if (hours > 0) {
          formattedValue += "" + interpolate(labels.hoursInitial, hours) + " ";
        }

        if (mins > 0) {
          formattedValue += "" + interpolate(labels.minsInitial, mins);
        }

        return formattedValue.trim();
      }
    },
    classList: {
      get: function () {
        return ["z-duration"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template;

        template.fragment.firstChild.innerHTML = "<div class=\"z-duration__placeholder u-br\" tabindex=\"0\"></div>\n       <label class=\"z-duration__label u-1/2\"><input type=\"number\" min=\"0\" step=\"any\" class=\"z-duration__input z-duration__hours u-br-l\"><span class=\"z-duration__label__text\"></span></label>\n       <label class=\"z-duration__label u-1/2\"><input type=\"number\" min=\"0\" step=\"any\" class=\"z-duration__input z-duration__mins u-br-r\"><span class=\"z-duration__label__text\"></span></label>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return ZDuration;
})(ZComponent);

mixin(ZDuration.prototype, Eventable);

ZDuration.labels = {};

module.exports = ZDuration;
}});

require.define({"components/z/form": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var ZActions = babelHelpers.interopRequire(require("./form/actions"));
var ZFieldset = babelHelpers.interopRequire(require("./form/fieldset"));

/**
 * State manager.
 *
 * @constructor
 * @param {object} states
 */
function StateManager(states) {
  this.states = states;
}

/**
 * Sets current state to declared initial state.
 */
StateManager.prototype.start = function () {
  var state = this.initialState = this.currentState = this.states.initial;

  state.enter();
};

/**
 * Transitions to another state.
 *
 * @param {string} state key for next state
 */
StateManager.prototype.transitionTo = function (state) {
  var to = this.states[state];

  if (!to) {
    throw new Error("Invalid transition state: " + state);
  }

  var from = this.currentState,
      transitions = from.transitions,
      transition = transitions && transitions["to:" + state];

  if (from.exit) {
    from.exit(to);
  }

  this.currentState = to;

  if (transition) {
    transition();
  }

  if (to.enter) {
    to.enter(from);
  }
};

/**
 * Form component.
 *
 * @class
 * @mixes Eventable
 */

var ZForm = (function (_ZComponent) {
  /**
   * @constructor
   *
   * @param {object} [options]
   * @param {object} [options.schema] definition for building form fields
   * @param {object} [options.data] data to load in fields
   * @param {string} [options.submitText] text for submit button
   * @param {string} [options.cancelText] text for cancel button
   * @param {string} [options.submittingText] text for submitting state
   * @param {string} [options.errorText] text for error state
   */

  function ZForm() {
    var _this = this;

    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZForm);

    var actions = this.actions = new ZActions({
      classList: ["z-form__actions"],
      submitText: options.submitText,
      cancelText: options.cancelText,
      submittingText: options.submittingText,
      errorText: options.errorText
    });

    babelHelpers.get(Object.getPrototypeOf(ZForm.prototype), "constructor", this).call(this, options);

    this.stateManager.start();
    this.body = this.$(".z-form__body");
    this.fieldsets = [];
    this.fields = {};
    this._invalid = {};

    options.schema.forEach(function (item) {
      return _this.addFieldset(item);
    });

    actions.appendTo(this.el);

    if (options.data) {
      this.reset(options.data);
    }

    actions.on("submit", this.onSubmit, this);
    actions.on("cancel", this.onCancel, this);
  }

  babelHelpers.inherits(ZForm, _ZComponent);
  babelHelpers.createClass(ZForm, {
    events: {

      /**
       * DOM events to event handlers.
       *
       * @type {object}
       */

      get: function () {
        return {
          submit: "onSubmit"
        };
      }
    },
    states: {

      /**
       * States to pass to state manager.
       *
       * @type {object}
       */

      get: function () {
        var actions = this.actions;

        return {
          initial: {
            enter: function enter() {
              actions.states.current = null;
              actions.update(false, false);
            }
          },

          valid: {
            enter: function enter() {
              actions.update(true, true);
            }
          },

          invalid: {
            enter: function enter() {
              actions.update(true, false);
            }
          },

          saving: {
            enter: function enter() {
              actions.states.current = "submitting";
              actions.update(false, false);
            }
          },

          errored: {
            enter: function enter() {
              actions.states.current = "error";
              actions.update(true, true);
            }
          }
        };
      }
    },
    stateManager: {

      /**
       * Form state manager.
       *
       * @type {StateManager}
       */

      get: function () {
        var sm = this._stateManager;

        if (!sm) {
          sm = this._stateManager = new StateManager(this.states);
        }

        return sm;
      }
    },
    addFieldset: {

      /**
       * Adds fieldset to form.
       *
       * @param {object} options
       * @param {string} [options.title]
       * @param {string} [options.description]
       * @param {object} options.fields see components/z/form/fieldset for details
       */

      value: function addFieldset(options) {
        var _this = this;

        var fieldset = new ZFieldset({
          title: options.title,
          description: options.description,
          fields: options.fields,
          classList: ["z-form__fieldset"]
        });

        fieldset.fields.forEach(function (field) {
          _this.fields[field.name] = field;

          if (!field.isValid) {
            _this._invalid[field.name] = field;
          }

          field.on("change", _this.onChange, _this);
        });

        fieldset.appendTo(this.body);
        this.fieldsets.push(fieldset);
      }
    },
    reset: {

      /**
       * Resets form to use passed data.
       *
       * @param {object} data form field data
       */

      value: function reset(data) {
        var isValid = true;

        this._invalid = this.fieldsets.reduce(function (invalid, fieldset) {
          fieldset.fields.forEach(function (field) {
            var name = field.name,
                value = data[name],
                isFieldValid;

            if (value != null) {
              // jshint ignore:line
              field.value = data[name];
            }

            isFieldValid = field.validate();

            if (!isFieldValid) {
              invalid[name] = field;
              isValid = false;
            }
          });

          return invalid;
        }, {});

        this.isValid = isValid;
      }
    },
    remove: {

      /**
       * Removes component from DOM.
       */

      value: function remove() {
        this.fieldsets.forEach(function (fieldset) {
          return fieldset.remove();
        });
        babelHelpers.get(Object.getPrototypeOf(ZForm.prototype), "remove", this).call(this);
        this.off();
      }
    },
    onChange: {

      /**
       * Event handler for field change events
       *
       * @fires ZForm#onChange
       */

      value: function onChange(field, value) {
        var name = field.name;

        if (!field.isValid) {
          this.isValid = false;
          this._invalid[name] = field;
          this.stateManager.transitionTo("invalid");
        } else if (this._invalid[name]) {
          delete this._invalid[name];
        }

        this.isValid = Object.keys(this._invalid).length === 0;

        if (this.isValid) {
          this.stateManager.transitionTo("valid");
        }

        this.emit("change", name, value);
      }
    },
    onSubmit: {

      /**
       * Submit event handler.
       *
       * @fires ZForm#onSubmit
       */

      value: function onSubmit(e) {
        if (e) {
          e.preventDefault();
        }

        this.emit("submit", this);
      }
    },
    onCancel: {

      /**
       * Cancel event handler.
       *
       * @fires ZForm#onCancel
       */

      value: function onCancel() {
        this.emit("cancel", this);
      }
    },
    didSubmit: {
      value: function didSubmit() {
        this.emit("submitted", this);
      }
    }
  }, {
    tagName: {

      /**
       * Gets tag name for form.
       *
       * @type {string}
       */

      get: function () {
        return "form";
      }
    },
    classList: {

      /**
       * List of default class names for form.
       *
       * @abstract
       * @static
       * @type {string[]}
       */

      get: function () {
        return ["z-form"];
      }
    },
    template: {

      /**
       * Gets template for form.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        var template = Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);

        template.fragment.firstChild.innerHTML = "<div class=\"z-form__body\"></div>";

        return template;
      }
    }
  });
  return ZForm;
})(ZComponent);

mixin(ZForm.prototype, Eventable);

module.exports = ZForm;
}});

require.define({"components/z/grabber": function(exports, require, module) {
"use strict";

var ZComponent = babelHelpers.interopRequire(require("z-component"));

var ZGrabber = (function (_ZComponent) {
  function ZGrabber() {
    babelHelpers.classCallCheck(this, ZGrabber);

    if (_ZComponent != null) {
      _ZComponent.apply(this, arguments);
    }
  }

  babelHelpers.inherits(ZGrabber, _ZComponent);
  babelHelpers.createClass(ZGrabber, null, {
    classList: {
      get: function () {
        return ["z-grabber"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template,
            el = template.fragment.firstChild,
            num = 6,
            dot;

        while (num--) {
          dot = document.createElement("div");
          dot.className = "z-grabber__dot";

          el.appendChild(dot);
        }

        return template;
      }
    }
  });
  return ZGrabber;
})(ZComponent);

module.exports = ZGrabber;
}});

require.define({"components/z/jumbotron": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));

var ZJumbotron = (function (_ZComponent) {
  function ZJumbotron() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZJumbotron);

    babelHelpers.get(Object.getPrototypeOf(ZJumbotron.prototype), "constructor", this).call(this, options);

    this.reset(options);
  }

  babelHelpers.inherits(ZJumbotron, _ZComponent);
  babelHelpers.createClass(ZJumbotron, {
    events: {
      get: function () {
        return {
          "click .z-jumbotron__cta": "onClick"
        };
      }
    },
    onClick: {
      value: function onClick() {
        this.emit("click");
      }
    },
    reset: {
      value: function reset(options) {
        this.$(".z-jumbotron__image").classList.add(options.image);
        this.$(".z-jumbotron__heading").textContent = options.heading;
        this.$(".z-jumbotron__subheading").textContent = options.subheading;
        this.$(".z-jumbotron__cta").textContent = options.cta;
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-jumbotron"];
      }
    },
    template: {
      get: function () {
        var template = Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);

        template.fragment.firstChild.innerHTML = "<span class=\"z-jumbotron__image\"></span>\n       <h2 class=\"z-jumbotron__heading u-gamma u-semibold u-mb\"></h2>\n       <h3 class=\"z-jumbotron__subheading u-delta u-fg-aluminum u-mb\"></h3>\n       <button class=\"c-btn c-btn--large c-btn--primary z-jumbotron__cta\">\n       </button>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return ZJumbotron;
})(ZComponent);

mixin(ZJumbotron.prototype, Eventable);

module.exports = ZJumbotron;
}});

require.define({"components/z/loader": function(exports, require, module) {
"use strict";

var ZComponent = babelHelpers.interopRequire(require("z-component"));

var HAS_CSS3_ANIMATION = (function () {
  var style = document.body.style,
      hasCSS3Animation = style.animationName !== undefined,
      domPrefixes = "Webkit Moz O ms Khtml".split(" ");

  if (!hasCSS3Animation) {
    for (var i = 0, len = domPrefixes.length; i < len; i++) {
      if (style[domPrefixes[i] + "AnimationName"] !== undefined) {
        hasCSS3Animation = true;
        break;
      }
    }
  }

  return hasCSS3Animation;
})();

var ZLoader = (function (_ZComponent) {
  function ZLoader() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZLoader);

    babelHelpers.get(Object.getPrototypeOf(ZLoader.prototype), "constructor", this).call(this, options);
    this.loading = !!options.loading;
  }

  babelHelpers.inherits(ZLoader, _ZComponent);
  babelHelpers.createClass(ZLoader, {
    loading: {
      get: function () {
        return this._loading;
      },
      set: function (val) {
        this._loading = val;

        this.el.classList[val ? "add" : "remove"]("loader--loading");
      }
    }
  }, {
    classList: {
      get: function () {
        var classList = ["loader"];

        if (!HAS_CSS3_ANIMATION) {
          classList.push("loader--img");
        }

        return classList;
      }
    },
    tagName: {
      get: function () {
        return "span";
      }
    },
    template: {
      get: function () {
        if (!HAS_CSS3_ANIMATION) {
          return ZComponent.template;
        }

        var template = ZComponent.template,
            wrapper = template.fragment.firstChild,
            numDots = 3,
            dot;

        while (numDots--) {
          dot = document.createElement("span");
          dot.className = "loader__dot";
          wrapper.appendChild(dot);
        }

        return template;
      }
    }
  });
  return ZLoader;
})(ZComponent);

module.exports = ZLoader;
}});

require.define({"components/z/matrix": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var Row = babelHelpers.interopRequire(require("components/z/matrix/row"));

var ZMatrix = (function (_ZComponent) {
  function ZMatrix() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZMatrix);

    babelHelpers.get(Object.getPrototypeOf(ZMatrix.prototype), "constructor", this).call(this, options);

    this.rows = [];
    this.schema = options.schema;
    this.headerOptions = {
      headers: options.headers,
      Popover: options.Popover,
      popovers: options.popovers
    };
    Row.Popover = options.Popover;

    this.populate();
  }

  babelHelpers.inherits(ZMatrix, _ZComponent);
  babelHelpers.createClass(ZMatrix, {
    value: {
      get: function () {
        return this.rows.map(function (row) {
          return row.value;
        });
      },
      set: function () {
        var val = arguments[0] === undefined ? [] : arguments[0];

        var rows = this.rows;

        this.clear();

        val.forEach(function (value, index) {
          rows[index].value = value;
        });
      }
    },
    addRow: {
      value: function addRow(item) {
        var row = new Row(item);

        row.appendTo(this.el);
        row.on("change", this.onChange, this);
        this.rows.push(row);
      }
    },
    clear: {
      value: function clear() {
        throw new Error("#clear is abstract property");
      }
    },
    remove: {
      value: function remove() {
        this.popoverViews.forEach(function (popover) {
          return popover.remove();
        });
        this.rows.forEach(function (row) {
          return row.remove();
        });
        babelHelpers.get(Object.getPrototypeOf(ZMatrix.prototype), "remove", this).call(this);
      }
    },
    onChange: {
      value: function onChange() {
        this.emit("change", this, this.value);
      }
    },
    populate: {
      value: function populate() {
        var _this = this;

        this.setupHeaders();
        this.schema.forEach(function (item) {
          return _this.addRow(item);
        });
      }
    },
    setupHeaders: {
      value: function setupHeaders() {
        var options = this.headerOptions;
        var row = this.$(".z-matrix__headers");
        var headers = options.headers;
        var Popover = options.Popover;
        var popovers = options.popovers;
        var popoverViews = this.popoverViews = [];
        var numCol = 0;

        Object.keys(headers).forEach(function (header) {
          var col = document.createElement("div");
          var span = document.createElement("span");
          var popover = popovers[header];

          col.classList.add("z-matrix__col", "z-matrix__header", "z-matrix__header--col");
          span.classList.add("z-matrix__header__text");
          span.textContent = headers[header];

          if (popover) {
            span.classList.add("z-matrix__header__text--has-popover");

            popover = new Popover(popover);
            popover.appendTo(span);
            popoverViews.push(popover);

            var onMouseOver = function () {
              popover.active = true;
            };
            var onMouseLeave = function () {
              popover.active = false;
            };

            span.addEventListener("mouseover", onMouseOver, false);
            span.addEventListener("mouseleave", onMouseLeave, false);
          }

          numCol = numCol + 1;
          col.appendChild(span);
          row.appendChild(col);
        });

        Row.COLUMNS = numCol;
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-matrix"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template;

        template.fragment.firstChild.innerHTML = "<div class=\"z-matrix__row z-matrix__headers\">\n         <div class=\"z-matrix__col z-matrix__header z-matrix__header--empty\"></div>\n       </div>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return ZMatrix;
})(ZComponent);

mixin(ZMatrix.prototype, Eventable);

module.exports = ZMatrix;
}});

require.define({"components/z/popover": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));

var ZPopover = (function (_ZComponent) {
  function ZPopover(options) {
    babelHelpers.classCallCheck(this, ZPopover);

    babelHelpers.get(Object.getPrototypeOf(ZPopover.prototype), "constructor", this).call(this, options);

    this.$(".z-popover__body").innerHTML = options.body;
    this.$(".z-popover__footer").innerHTML = options.footer;
  }

  babelHelpers.inherits(ZPopover, _ZComponent);
  babelHelpers.createClass(ZPopover, {
    active: {
      get: function () {
        return this._active;
      },
      set: function (val) {
        this._active = val;

        this.el.classList[val ? "add" : "remove"]("z-popover--active");
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-popover"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template;

        template.fragment.firstChild.innerHTML = "<div class=\"z-popover__body\"></div>\n       <div class=\"z-popover__footer\"></div>".replace(/>\s+/, ">");

        return template;
      }
    }
  });
  return ZPopover;
})(ZComponent);

mixin(ZPopover.prototype, Eventable);

module.exports = ZPopover;
}});

require.define({"models/errors/bad-gateway": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var BadGatewayError = (function (_HTTPError) {
  function BadGatewayError() {
    babelHelpers.classCallCheck(this, BadGatewayError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(BadGatewayError, _HTTPError);
  babelHelpers.createClass(BadGatewayError, {
    name: {
      get: function () {
        return "BadGatewayError";
      }
    },
    statusCode: {
      get: function () {
        return 502;
      }
    }
  });
  return BadGatewayError;
})(HTTPError);

module.exports = BadGatewayError;
}});

require.define({"models/errors/bad-request": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var BadRequestError = (function (_HTTPError) {
  function BadRequestError() {
    babelHelpers.classCallCheck(this, BadRequestError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(BadRequestError, _HTTPError);
  babelHelpers.createClass(BadRequestError, {
    name: {
      get: function () {
        return "BadRequestError";
      }
    },
    statusCode: {
      get: function () {
        return 400;
      }
    }
  });
  return BadRequestError;
})(HTTPError);

module.exports = BadRequestError;
}});

require.define({"models/errors/forbidden": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var ForbiddenError = (function (_HTTPError) {
  function ForbiddenError() {
    babelHelpers.classCallCheck(this, ForbiddenError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(ForbiddenError, _HTTPError);
  babelHelpers.createClass(ForbiddenError, {
    name: {
      get: function () {
        return "ForbiddenError";
      }
    },
    statusCode: {
      get: function () {
        return 403;
      }
    }
  });
  return ForbiddenError;
})(HTTPError);

module.exports = ForbiddenError;
}});

require.define({"models/errors/http": function(exports, require, module) {
/**
 * HTTP error class.
 */
"use strict";

var HTTPError = (function (_Error) {
  function HTTPError(url, response) {
    babelHelpers.classCallCheck(this, HTTPError);

    this.url = url;
    this.response = response;
    this.stack = new Error().stack;
  }

  babelHelpers.inherits(HTTPError, _Error);
  babelHelpers.createClass(HTTPError, {
    statusCode: {

      /**
       * Gets status code for error.
       *
       * @type {number}
       */

      get: function () {
        return -1;
      }
    },
    message: {

      /**
       * Error message, see Error.prototype.message for more details.
       *
       * @type {string}
       */

      get: function () {
        var statusCode = this.statusCode,
            url = this.url;

        return "" + statusCode + " status for " + url;
      }
    }
  });
  return HTTPError;
})(Error);

module.exports = HTTPError;
}});

require.define({"models/errors/internal-server": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var InternalServerError = (function (_HTTPError) {
  function InternalServerError() {
    babelHelpers.classCallCheck(this, InternalServerError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(InternalServerError, _HTTPError);
  babelHelpers.createClass(InternalServerError, {
    name: {
      get: function () {
        return "InternalServerError";
      }
    },
    statusCode: {
      get: function () {
        return 500;
      }
    }
  });
  return InternalServerError;
})(HTTPError);

module.exports = InternalServerError;
}});

require.define({"models/errors/not-found": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var NotFoundError = (function (_HTTPError) {
  function NotFoundError() {
    babelHelpers.classCallCheck(this, NotFoundError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(NotFoundError, _HTTPError);
  babelHelpers.createClass(NotFoundError, {
    name: {
      get: function () {
        return "NotFoundError";
      }
    },
    statusCode: {
      get: function () {
        return 404;
      }
    }
  });
  return NotFoundError;
})(HTTPError);

module.exports = NotFoundError;
}});

require.define({"models/errors/service-unavailable": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var ServiceUnavailableError = (function (_HTTPError) {
  function ServiceUnavailableError() {
    babelHelpers.classCallCheck(this, ServiceUnavailableError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(ServiceUnavailableError, _HTTPError);
  babelHelpers.createClass(ServiceUnavailableError, {
    name: {
      get: function () {
        return "ServiceUnavailableError";
      }
    },
    statusCode: {
      get: function () {
        return 503;
      }
    }
  });
  return ServiceUnavailableError;
})(HTTPError);

module.exports = ServiceUnavailableError;
}});

require.define({"models/errors/unknown": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var UnknownError = (function (_HTTPError) {
  function UnknownError() {
    babelHelpers.classCallCheck(this, UnknownError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(UnknownError, _HTTPError);
  babelHelpers.createClass(UnknownError, {
    name: {
      get: function () {
        return "UnknownError";
      }
    },
    statusCode: {
      get: function () {
        return this.response.status;
      }
    }
  });
  return UnknownError;
})(HTTPError);

module.exports = UnknownError;
}});

require.define({"models/errors/unprocessable-entity": function(exports, require, module) {
"use strict";

var HTTPError = babelHelpers.interopRequire(require("./http"));

var UnprocessableEntityError = (function (_HTTPError) {
  function UnprocessableEntityError() {
    babelHelpers.classCallCheck(this, UnprocessableEntityError);

    if (_HTTPError != null) {
      _HTTPError.apply(this, arguments);
    }
  }

  babelHelpers.inherits(UnprocessableEntityError, _HTTPError);
  babelHelpers.createClass(UnprocessableEntityError, {
    name: {
      get: function () {
        return "UnprocessableEntityError";
      }
    },
    statusCode: {
      get: function () {
        return 422;
      }
    }
  });
  return UnprocessableEntityError;
})(HTTPError);

module.exports = UnprocessableEntityError;
}});

require.define({"components/z/condition/expression": function(exports, require, module) {
"use strict";

var isEmpty = require("lib/utils").isEmpty;

var WITH_PRESENCE_OPERATORS = Object.freeze(["group_id", "organization_id", "assignee_id"]);
var CONDITION_PARTS = Object.freeze(["field", "operator", "value"]);
var TERMINAL_OPERATORS = Object.freeze(["present", "not_present"]);

function ZConditionExpression(props) {
  var _this = this;

  var field = props.field;
  var operator = props.operator;
  var value = props.value;

  if (WITH_PRESENCE_OPERATORS.indexOf(field) !== -1 && value == null) {
    // jshint ignore:line
    props.operator = operator === "is_not" ? "present" : "not_present";
    delete props.value;
  }

  CONDITION_PARTS.forEach(function (name) {
    var _value = props[name];

    Object.defineProperty(_this, name, {
      enumerable: true,

      get: function get() {
        return _value;
      },

      set: function set(val) {
        _value = val;
        this.validate();

        if (this.delegate) {
          this.delegate.onChange(name, _value);
        }
      }
    });
  });

  Object.defineProperties(this, {
    isValid: {
      enumerable: false,
      writable: true,
      value: false
    },

    isEmpty: {
      enumerable: false,
      get: function get() {
        return isEmpty(this.field);
      }
    },

    delegate: {
      enumerable: false,
      writable: true,
      value: null
    },

    meta: {
      enumerable: false,
      writable: true,
      value: props.meta
    },

    validate: {
      enumerable: false,
      value: function value() {
        var _this2 = this;

        var isTerminal = TERMINAL_OPERATORS.indexOf(this.operator) !== -1;

        if (isTerminal) {
          this.isValid = !isEmpty(this.field);
          return;
        }

        this.isValid = CONDITION_PARTS.every(function (name) {
          return !isEmpty(_this2[name]);
        });
      }
    }
  });
}

ZConditionExpression.prototype.toJSON = function () {
  var _ref = this;

  var field = _ref.field;
  var operator = _ref.operator;
  var value = _ref.value;

  if (WITH_PRESENCE_OPERATORS.indexOf(field) !== -1 && value == null) {
    // jshint ignore:line
    operator = operator === "not_present" ? "is" : "is_not";
    value = null;
  }

  return {
    field: field,
    operator: operator,
    value: value
  };
};

module.exports = ZConditionExpression;
}});

require.define({"components/z/dropdown/menu": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Composable = babelHelpers.interopRequire(require("mixins/composable"));
var ZDropdownMenuItem = babelHelpers.interopRequire(require("components/z/dropdown/menuitem"));

/**
 * A menu for the dropdown component.
 *
 * @class
 */

var ZDropdownMenu = (function () {
  /**
   * @constructor
   */

  function ZDropdownMenu() {
    var items = arguments[0] === undefined ? [] : arguments[0];
    babelHelpers.classCallCheck(this, ZDropdownMenu);

    var el = this.el = document.createElement("ul");

    el.classList.add("z-dropdown__menu");
    el.setAttribute("role", "menu");

    this.items = items;
  }

  babelHelpers.createClass(ZDropdownMenu, {
    select: {

      /**
       * Selects menu item.
       *
       * @param {string} id Selected id.
       */

      value: function select(id) {
        var prev = this.selected,
            next = this.lookup(id);

        if (prev) {
          prev = this.lookup(prev.id);
          prev.selected = false;
        }

        if (next) {
          this.selected = next.data;
          next.selected = true;
        }
      }
    },
    toggle: {

      /**
       * Enables/Disables menu item.
       *
       * @param {string, boolean} id Menu item id, boolean Enable/Disable state.
       */

      value: function toggle(id, enable) {
        var item = this.lookup(id);

        if (item) {
          item.disabled = !enable;
        }
      }
    },
    remove: {
      value: function remove() {
        this.clear();
      }
    },
    open: {

      /**
       * Gets open state.
       *
       * @type {boolean}
       */

      get: function () {
        return !!this._open;
      },

      /**
       * Sets open state.
       *
       * @param {boolean} val Open/close state.
       */
      set: function (val) {
        this.el.classList[val ? "add" : "remove"]("z-dropdown__menu--open");
        this._open = val;
      }
    },
    selected: {

      /**
       * Gets selected menu item.
       *
       * @type {string}
       */

      get: function () {
        return this._selected;
      },

      /**
       * Sets selected menu item.
       *
       * @param {string} val Selected id.
       */
      set: function (val) {
        this._selected = val;
      }
    }
  });
  return ZDropdownMenu;
})();

ZDropdownMenu.Item = ZDropdownMenuItem;

mixin(ZDropdownMenu.prototype, Composable, Object.defineProperties({

  lookup: function lookup(id) {
    return this.cache[id];
  },

  addChildView: function addChildView(item) {
    var view = Composable.addChildView.call(this, item);

    this.cache[item.id] = view;

    return view;
  },

  removeChildView: function removeChildView(view) {
    Composable.removeChildView.call(this, view);

    delete this.cache[view.data.id];
  },

  clear: function clear() {
    Composable.clear.call(this);

    this.cache = {};
  }
}, {
  cache: {
    get: function () {
      return this._cache || {};
    },
    set: function (val) {
      this._cache = val;
    },
    configurable: true,
    enumerable: true
  }
}));

module.exports = ZDropdownMenu;
}});

require.define({"components/z/dropdown/menuitem": function(exports, require, module) {
"use strict";

var ZComponent = babelHelpers.interopRequire(require("z-component"));

/**
 * A menu item for the dropdown component.
 *
 * @class
 */

var ZDropdownMenuItem = (function (_ZComponent) {
  /**
   * @constructor
   */

  function ZDropdownMenuItem() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZDropdownMenuItem);

    babelHelpers.get(Object.getPrototypeOf(ZDropdownMenuItem.prototype), "constructor", this).call(this, options);

    var el = this.el;

    el.textContent = options.title;
    this.data = { id: options.id, title: options.title, value: options.value };
    this.disabled = options.disabled;
    this.selected = options.selected;

    el.setAttribute("data-selection-id", options.id);
    el.setAttribute("tabindex", "-1");
  }

  babelHelpers.inherits(ZDropdownMenuItem, _ZComponent);
  babelHelpers.createClass(ZDropdownMenuItem, {
    data: {
      get: function () {
        return this._data || {};
      },
      set: function (val) {
        this._data = val;
      }
    },
    disabled: {
      get: function () {
        return this._disabled;
      },
      set: function (val) {
        this.el.classList[val ? "add" : "remove"]("z-dropdown__menuitem--disabled");
        this._disabled = val;
      }
    },
    selected: {
      get: function () {
        return this._selected;
      },
      set: function (val) {
        this.el.classList[val ? "add" : "remove"]("z-dropdown__menuitem--selected");
        this._selected = val;
      }
    }
  }, {
    tagName: {

      /**
       * Gets tag name for menu item.
       *
       * @type {string}
       */

      get: function () {
        return "li";
      }
    },
    classList: {

      /**
       * List of default class names for menu item component.
       *
       * @type {string[]}
       */

      get: function () {
        return ["z-dropdown__menuitem"];
      }
    },
    template: {

      /**
       * Gets template for component.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        return Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);
      }
    }
  });
  return ZDropdownMenuItem;
})(ZComponent);

module.exports = ZDropdownMenuItem;
}});

require.define({"components/z/fields/date": function(exports, require, module) {
"use strict";

var ZField = babelHelpers.interopRequire(require("./field"));
var ZDatepicker = babelHelpers.interopRequire(require("./../datepicker"));

var classList = ZField.classList.concat(["z-field--date"]);
var FORMAT = "%a %b %d, %Y";
var YEAR_FIRST = false;
var YEAR_SUFFIX = "";

var ZDateInput = (function (_ZField) {
  function ZDateInput() {
    babelHelpers.classCallCheck(this, ZDateInput);

    if (_ZField != null) {
      _ZField.apply(this, arguments);
    }
  }

  babelHelpers.inherits(ZDateInput, _ZField);
  babelHelpers.createClass(ZDateInput, {
    events: {
      /**
       * DOM events mapping to event handlers.
       *
       * @type {object}
       */

      get: function () {
        return {
          "focus^": "onFocus"
        };
      }
    },
    value: {

      /**
       * Gets input value.
       *
       * @type {string}
       */

      get: function () {
        return this._value || "";
      },

      /**
       * Sets input value.
       *
       * Only sets value when it's different from current value.
       *
       * @type {string}
       */
      set: function (val) {
        var value = this.parse(val);

        if (this._value === value) {
          return;
        }

        this._value = value;

        if (value.strftime) {
          this.input.value = value.strftime(FORMAT);
        }
      }
    },
    focus: {
      value: function focus() {
        var input = this.input;

        if (!input.offsetWidth || !input.offsetHeight) {
          return;
        }

        this.input.focus();
      }
    },
    parse: {
      value: function parse(val) {
        return val;
      }
    },
    remove: {
      value: function remove() {
        var datepicker = this.datepicker;

        if (datepicker) {
          datepicker.destroy();
        }

        babelHelpers.get(Object.getPrototypeOf(ZDateInput.prototype), "remove", this).call(this);
      }
    },
    setElement: {
      value: function setElement(el) {
        babelHelpers.get(Object.getPrototypeOf(ZDateInput.prototype), "setElement", this).call(this, el);

        this.el.setAttribute("tabindex", "0");
        this.input = this.el.firstChild;
      }
    },
    onFocus: {
      value: function onFocus() {
        if (!this.datepicker) {
          this.datepicker = new ZDatepicker({
            field: this.input,
            format: FORMAT,
            showMonthAfterYear: YEAR_FIRST,
            yearSuffix: YEAR_SUFFIX,
            defaultDate: this.value && this.value.toDate(),
            onSelect: this.onSelect.bind(this),
            onClose: this.onClose.bind(this)
          });
        }

        this.datepicker.show();
      }
    },
    onSelect: {
      value: function onSelect(date) {
        this.value = date;
      }
    },
    onClose: {
      value: function onClose() {
        var datepicker = this.datepicker;

        this.input.blur();

        if (datepicker) {
          this.datepicker = null;
          datepicker.destroy();
        }
      }
    }
  }, {
    classList: {

      /**
       * List of default class names for input.
       *
       * @abstract
       * @static
       * @type {string[]}
       */

      get: function () {
        return classList;
      }
    },
    template: {

      /**
       * Gets template for input.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        var template = Object.getOwnPropertyDescriptor(ZField, "template").get.call(this);

        template.fragment.firstChild.innerHTML = "<input type=\"text\" spellcheck=\"false\" readonly=\"true\" class=\"z-field--date__input\">";

        return template;
      }
    }
  });
  return ZDateInput;
})(ZField);

Object.defineProperty(ZDateInput, "FORMAT", {
  get: function get() {
    return FORMAT;
  },

  set: function set(val) {
    var match = /^%Y([^\s\.-]?)/.exec(val);

    FORMAT = val;
    YEAR_FIRST = !!match;
    YEAR_SUFFIX = match && match[1] || "";
  }
});

module.exports = ZDateInput;
}});

require.define({"components/z/fields/field": function(exports, require, module) {
"use strict";

var _libUtils = require("lib/utils");

var mixin = _libUtils.mixin;
var isEmpty = _libUtils.isEmpty;
var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));

/**
 * Basic field component.
 *
 * @class
 * @mixes Eventable
 */

var ZField = (function (_ZComponent) {
  /**
   * @constructor
   * @param {object} [options] component options
   * @param {string} [options.name] name attribute (unique)
   * @param {string} [options.label] label text
   * @param {string} [options.width] grid width, e.g. '1/2'
   * @param {boolean} [options.required] required
   * @param {object} [options.attributes] component attributes
   */

  function ZField() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZField);

    var attributes = options.attributes || {};

    babelHelpers.get(Object.getPrototypeOf(ZField.prototype), "constructor", this).call(this, options);

    this.name = options.name;
    this.label = options.label;
    this.width = options.width;
    this.required = options.required;

    for (var name in attributes) {
      this.el.setAttribute(name, attributes[name]);
    }

    this.validations = [];
    if (options.validate) {
      this.validations.push(options.validate);
    }

    this.validate();
  }

  babelHelpers.inherits(ZField, _ZComponent);
  babelHelpers.createClass(ZField, {
    name: {

      /**
       * Gets field name.
       *
       * @type {string}
       */

      get: function () {
        return this._name;
      },

      /**
       * Sets field name, updates name attribute.
       *
       * @type {string}
       */
      set: function (val) {
        if (val) {
          this.el.setAttribute("name", val);
        } else {
          this.el.removeAttribute("name");
        }

        this._name = val;
      }
    },
    label: {

      /**
       * Gets field label.
       *
       * @type {string}
       */

      get: function () {
        return this._label;
      },

      /**
       * Sets field label.
       *
       * When setting the label from a falsy value, the field will be wrapped in a
       * <label> element (which is then assigned to `this.wrapper`). Conversely,
       * when unsetting the label, the field will be unwrapped.
       *
       * @type {string}
       */
      set: function (val) {
        if (!val) {
          this._unwrap();
        } else {
          this._wrap();
          this.wrapper.firstChild.textContent = val;
        }

        this._label = val || "";
      }
    },
    value: {

      /**
       * Gets field value.
       *
       * @abstract
       */

      get: function () {
        return this._value;
      },

      /**
       * Sets field value.
       *
       * @abstract
       */
      set: function (val) {
        this._value = val;
      }
    },
    width: {

      /**
       * Gets field width.
       *
       * @type {string}
       */

      get: function () {
        return this._width;
      },

      /**
       * Sets field width.
       *
       * By setting field width, a class will be added for the width, which can
       * then be targeted for css layout. Valid values are strings representing
       * numerical fractions, e.g. '3/4', '1/8', and must be less than 1 to apply.
       *
       * @type {string}
       */
      set: function (value) {
        var el = this.label ? this.wrapper : this.el;
        var _split$map = (value || "").split("/").map(parseFloat);

        var _split$map2 = babelHelpers.slicedToArray(_split$map, 2);

        var numerator = _split$map2[0];

        var denominator = _split$map2[1];
        var num = numerator / denominator;
        var widthClass = "u-" + value;

        if (isNaN(num)) {
          return;
        }

        this._width = widthClass;
        el.classList[num <= 1 ? "add" : "remove"](widthClass);
      }
    },
    required: {

      /**
       * Gets required state.
       *
       * @type {boolean}
       */

      get: function () {
        return this._required;
      },

      /**
       * Sets required state.
       *
       * This attribute is used to denote that a field is required and allows the
       * label to be styled and represented it as such.
       *
       * @type {boolean}
       */
      set: function (val) {
        var required = !!val;

        this._required = required;

        if (!this.label) {
          return;
        }

        if (val) {
          this.wrapper.classList.add("z-field__label--required");
        } else {
          this.wrapper.classList.remove("z-field__label--required");
        }
      }
    },
    appendTo: {

      /**
       * Appends the field to the target element.
       *
       * @param {Node} target target element
       *
       * @returns {ZField} `this`
       */

      value: function appendTo(target) {
        if (this.label) {
          return target.appendChild(this.wrapper);
        }

        return babelHelpers.get(Object.getPrototypeOf(ZField.prototype), "appendTo", this).call(this, target);
      }
    },
    validate: {

      /**
       * Validates field.
       *
       * When `required` is true, the base validation is requirement. Otherwise,
       * all other validation is dependent on configuration, i.e. passing a
       * `validate` function to the constructor.
       *
       * @returns {boolean}
       */

      value: function validate() {
        this.isValid = this.validations.every(function (validation) {
          return validation();
        });

        if (this.isValid && this.required) {
          this.isValid = !isEmpty(this.value);
        }

        return this.isValid;
      }
    },
    remove: {

      /**
       * Removes field from its parent node. Also handles removal of wrapper.
       */

      value: function remove() {
        var wrapper = this.wrapper;

        babelHelpers.get(Object.getPrototypeOf(ZField.prototype), "remove", this).call(this);

        if (wrapper && wrapper.parentNode) {
          wrapper.parentNode.removeChild(wrapper);
        }
      }
    },
    _wrap: {
      value: function _wrap() {
        var wrapper = this.wrapper,
            el = this.el,
            parentNode = el.parentNode,
            classes = [];

        if (!this.wrapper) {
          wrapper = this.wrapper = document.createElement("label");
          wrapper.classList.add("z-field__label");

          wrapper.innerHTML = "<span class=\"z-field__label__text\"></span>";
        }

        if (this.required) {
          classes.push("z-field__required");
        }

        if (this.width) {
          classes.push(this.width);
        }

        if (!wrapper.parentNode && parentNode !== wrapper) {
          parentNode.replaceChild(wrapper, el);
        }

        classes.forEach(function (className) {
          wrapper.classList.add(className);
          el.classList.remove(className);
        });

        wrapper.appendChild(el);
      }
    },
    _unwrap: {
      value: function _unwrap() {
        if (!this.wrapper) {
          return;
        }

        var wrapper = this.wrapper,
            el = this.el,
            classes = [];

        if (this.required) {
          classes.push("z-field__required");
        }

        if (this.width) {
          classes.push(this.width);
          el.classList.add(this.width);
        }

        if (wrapper.parentNode) {
          wrapper.parentNode.replaceChild(this.el, wrapper);
        }

        wrapper.classList.remove.apply(wrapper.classList, classes);

        wrapper.removeChild(this.el);
      }
    }
  }, {
    classList: {

      /**
       * List of default class names for field.
       *
       * @abstract
       * @static
       * @type {string[]}
       */

      get: function () {
        return ["z-field"];
      }
    },
    template: {

      /**
       * Gets template for field.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        return Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);
      }
    }
  });
  return ZField;
})(ZComponent);

mixin(ZField.prototype, Eventable);

module.exports = ZField;
}});

require.define({"components/z/fields/text-input": function(exports, require, module) {
"use strict";

var ZField = babelHelpers.interopRequire(require("./field"));

var classList = ZField.classList.concat(["z-field--input--text"]);

/**
 * Text input component.
 *
 * @constructor
 * @extends ZField
 */

var ZTextInput = (function (_ZField) {
  /**
   * @constructor
   *
   * @param {object} options
   */

  function ZTextInput() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZTextInput);

    babelHelpers.get(Object.getPrototypeOf(ZTextInput.prototype), "constructor", this).call(this, options);
  }

  babelHelpers.inherits(ZTextInput, _ZField);
  babelHelpers.createClass(ZTextInput, {
    events: {

      /**
       * DOM events mapping to event handlers.
       *
       * @type {object}
       */

      get: function () {
        return {
          cut: "onChange",
          input: "onChange",
          keyup: "onChange",
          paste: "onChange"
        };
      }
    },
    value: {

      /**
       * Gets input value.
       *
       * @type {string}
       */

      get: function () {
        return this._value || "";
      },

      /**
       * Sets input value.
       *
       * Only sets value when it's different from current value.
       *
       * @type {string}
       */
      set: function (val) {
        if (this._value === val) {
          return;
        }

        this._value = val;
        this.el.value = this.value;
      }
    },
    onChange: {

      /**
       * change/input event handler for input.
       *
       * @fires TextInput#onInput
       */

      value: function onChange() {
        var value = this.el.value;

        if (this._value === value) {
          return;
        }

        this._value = value;

        this.validate();

        /**
         * Input change event.
         *
         * @event TextInput#onInput
         */
        this.emit("change", this, value);
      }
    }
  }, {
    tagName: {

      /**
       * Gets tag name for input.
       *
       * @type {string}
       */

      get: function () {
        return "input";
      }
    },
    classList: {

      /**
       * List of default class names for input.
       *
       * @abstract
       * @static
       * @type {string[]}
       */

      get: function () {
        return classList;
      }
    },
    template: {

      /**
       * Gets template for input.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        return Object.getOwnPropertyDescriptor(ZField, "template").get.call(this);
      }
    }
  });
  return ZTextInput;
})(ZField);

module.exports = ZTextInput;
}});

require.define({"components/z/form/actions": function(exports, require, module) {
"use strict";

var mixin = require("lib/utils").mixin;

var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZStates = babelHelpers.interopRequire(require("./states"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));
var ZLoader = babelHelpers.interopRequire(require("components/z/loader"));

var ZActions = (function (_ZComponent) {
  function ZActions() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZActions);

    var loader = new ZLoader({
      classList: ["loader--small"]
    });

    var states = this.states = new ZStates({
      states: [{
        name: options.submittingText || "Submitting",
        value: "submitting",
        icon: loader.el
      }, {
        name: options.errorText || "Error",
        value: "error"
      }],

      classList: ["z-actions__states"]
    });

    babelHelpers.get(Object.getPrototypeOf(ZActions.prototype), "constructor", this).call(this, options);

    var buttons = this.buttons = [this.$(".z-actions__cancel"), this.$(".z-actions__submit")];
    buttons.cancel = buttons[0];
    buttons.submit = buttons[1];
    buttons.cancel.textContent = options.cancelText || "Cancel";
    buttons.submit.textContent = options.submitText || "Submit";

    states.prependTo(this.el);
  }

  babelHelpers.inherits(ZActions, _ZComponent);
  babelHelpers.createClass(ZActions, {
    events: {
      get: function () {
        return {
          "click .z-actions__submit": "onSubmit",
          "click .z-actions__cancel": "onCancel"
        };
      }
    },
    update: {
      value: function update() {
        for (var _len = arguments.length, states = Array(_len), _key = 0; _key < _len; _key++) {
          states[_key] = arguments[_key];
        }

        var buttons = this.buttons;

        states.forEach(function (state, index) {
          var button = buttons[index];

          if (state) {
            button.removeAttribute("disabled");
          } else {
            button.setAttribute("disabled", "true");
          }
        });
      }
    },
    remove: {
      value: function remove() {
        babelHelpers.get(Object.getPrototypeOf(ZActions.prototype), "remove", this).call(this);
        this.off();
      }
    },
    onSubmit: {
      value: function onSubmit(e) {
        e.preventDefault();

        this.states.current = "submitting";
        this.emit("submit");
      }
    },
    onCancel: {
      value: function onCancel() {
        this.current = null;

        this.emit("cancel");
      }
    },
    onError: {
      value: function onError() {
        this.states.current = "error";
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-actions"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template,
            cancelClasses = "c-btn z-actions__cancel",
            submitClasses = "c-btn c-btn--primary z-actions__submit";

        template.fragment.firstChild.innerHTML = ("<button class=\"" + cancelClasses + "\" type=\"button\"></button>\n       <button class=\"" + submitClasses + "\" type=\"submit\"></button>").replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return ZActions;
})(ZComponent);

mixin(ZActions.prototype, Eventable);

module.exports = ZActions;
}});

require.define({"components/z/form/fields": function(exports, require, module) {
"use strict";

var PolicyMatrix = babelHelpers.interopRequire(require("components/policy/matrix"));
var ConditionsFieldset = babelHelpers.interopRequire(require("components/z/conditions-fieldset"));
var TextInput = babelHelpers.interopRequire(require("components/z/fields/text-input"));
module.exports = { PolicyMatrix: PolicyMatrix, ConditionsFieldset: ConditionsFieldset, TextInput: TextInput };
}});

require.define({"components/z/form/fieldset": function(exports, require, module) {
"use strict";

var ZComponent = babelHelpers.interopRequire(require("z-component"));
var FIELDS = babelHelpers.interopRequire(require("./fields"));

/**
 * Fieldset component.
 *
 * @class
 */

var ZFieldset = (function (_ZComponent) {
  /**
   * @constructor
   *
   * @param {object} [options]
   * @param {string} [options.title]
   * @param {string} [options.description]
   * @param {object[]} [options.fields]
   *
   * For `options.fields`, the format will determine the type and
   * characteristics of each field within the fieldset.
   *
   * Each item in the array will have a `type`, e.g.
   *
   * ```
   * [
   *   {
   *     type: 'TextInput',
   *     name: 'my-input',
   *     label: 'My input'
   *   }
   * ]
   * ```
   *
   * The type determines which field class to use (see
   * components/z/form/fields.js for a list of all supported fields).
   *
   * The rest of the properties are passed to the field constructor for
   * instantiation.
   */

  function ZFieldset() {
    var _this = this;

    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZFieldset);

    babelHelpers.get(Object.getPrototypeOf(ZFieldset.prototype), "constructor", this).call(this, options);

    this.title = options.title;
    this.description = options.description;
    this.fields = (options.fields || []).reduce(function (fields, list) {
      if (!Array.isArray(list)) {
        list = [list];
      }

      var len = list.length;

      list.forEach(function (props) {
        var field = new FIELDS[props.type](props);

        if (!field.width && len > 1) {
          field.width = "1/" + len;
        }

        field.appendTo(_this.el);
        fields.push(field);
      });

      return fields;
    }, []);
  }

  babelHelpers.inherits(ZFieldset, _ZComponent);
  babelHelpers.createClass(ZFieldset, {
    title: {

      /**
       * Gets fieldset title.
       *
       * @type {string}
       */

      get: function () {
        return this._title;
      },

      /**
       * Sets fieldset title.
       *
       * The value will be displayed in the legend heading.
       *
       * @type {string}
       */
      set: function () {
        var val = arguments[0] === undefined ? "" : arguments[0];

        this._title = val;
        this.$(".z-fieldset__legend__heading").textContent = val;
      }
    },
    description: {

      /**
       * Gets fieldset description.
       *
       * @type {string}
       */

      get: function () {
        return this._description;
      },

      /**
       * Sets fieldset description.
       *
       * The value will be displayed in the legend description.
       *
       * @type {string}
       */
      set: function () {
        var val = arguments[0] === undefined ? "" : arguments[0];

        this._description = val;
        this.$(".z-fieldset__legend__description").textContent = val;
      }
    }
  }, {
    tagName: {

      /**
       * Gets tag name for fieldset.
       *
       * @type {string}
       */

      get: function () {
        return "fieldset";
      }
    },
    classList: {

      /**
       * List of default class names for fieldset.
       *
       * @abstract
       * @static
       * @type {string[]}
       */

      get: function () {
        return ["z-fieldset"];
      }
    },
    template: {

      /**
       * Gets template for fieldset.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        var template = Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);

        template.fragment.firstChild.innerHTML = "<div class=\"z-fieldset__legend\">\n         <h3 class=\"z-fieldset__legend__heading u-delta u-semibold\"></h3>\n         <p class=\"z-fieldset__legend__description\"></p>\n       </div>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return ZFieldset;
})(ZComponent);

module.exports = ZFieldset;
}});

require.define({"components/z/form/states": function(exports, require, module) {
"use strict";

var ZComponent = babelHelpers.interopRequire(require("z-component"));

/**
 * Generic component for displaying state.
 *
 * @class
 */

var ZStates = (function (_ZComponent) {
  /**
   * @constructor
   *
   * @param {object} [options]
   * @param {object} [options.states] declaration of various states
   *
   * Each state can include an icon as will which will be prepended to its
   * respective element.
   *
   * ```
   * var icon = document.createElement('span');
   *
   * icon.className = 'example-icon';
   *
   * var states = new ZStates({
   *   states: [
   *     {
   *       name: 'Example',
   *       value: 'example',
   *       icon: icon
   *     }
   *   ]
   * });
   *
   * states.appendTo(document.body);
   *
   * console.log(states.el);
   * // <span class="z-states">
   * //   <span class="z-states__state z-states__state--example">
   * //     <span class="example-icon">
   * //     Example
   * //   </span>
   * // </span>
   *
   * states.current = 'example';
   *
   * console.log(states.el.className);
   * // 'z-states z-states--example'
   *
   * console.log(states.current);
   * // 'example'
   * ```
   */

  function ZStates() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZStates);

    var states = options.states || [];

    babelHelpers.get(Object.getPrototypeOf(ZStates.prototype), "constructor", this).call(this, options);

    states.forEach(this.addState, this);
  }

  babelHelpers.inherits(ZStates, _ZComponent);
  babelHelpers.createClass(ZStates, {
    current: {

      /**
       * Gets current state.
       *
       * @type {string}
       */

      get: function () {
        return this._current;
      },

      /**
       * Sets current state.
       *
       * @type {string}
       */
      set: function (val) {
        if (val === this.current) {
          return;
        }

        var state = this.states[val],
            el = this.el,
            className = this.classList.join(" ");

        if (!state) {
          el.className = className;
          this._current = null;

          return;
        }

        el.className = "" + className + " z-states--" + val;
        this._current = val;
      }
    },
    states: {

      /**
       * Store for all possible states.
       *
       * @readonly
       */

      get: function () {
        if (!this._states) {
          this._states = {};
        }

        return this._states;
      }
    },
    addState: {

      /**
       * Adds a state to the component. Idempotent.
       *
       * @param {object} state
       * @param {string} state.name name used as text content for state
       * @param {string} state.value value/id for state
       * @param {Node|string} state.icon icon to use for state
       */

      value: function addState(state) {
        var states = this.states,
            name = state.name,
            value = state.value,
            icon = state.icon,
            span;

        if (states[name]) {
          return;
        }

        span = document.createElement("span");
        span.className = "z-states__state z-states__state--" + value;
        span.textContent = name;

        if (icon instanceof Node) {
          span.insertBefore(icon, span.firstChild);
        } else if (typeof icon === "string") {
          span.className += " " + icon;
        }

        states[value] = span;
        this.el.appendChild(span);

        return span;
      }
    },
    removeState: {

      /**
       * Removes a state from component.
       *
       * @param {string} value state value to match for removal
       */

      value: function removeState(value) {
        var states = this.states,
            span = states[value];

        if (!span) {
          return;
        }

        delete states[value];
        this.el.removeChild(span);

        return span;
      }
    }
  }, {
    tagName: {

      /**
       * Gets tag name.
       *
       * @type {string}
       */

      get: function () {
        return "span";
      }
    },
    classList: {

      /**
       * List of default class names.
       *
       * @abstract
       * @static
       * @type {string[]}
       */

      get: function () {
        return ["z-states"];
      }
    },
    template: {

      /**
       * Gets template.
       *
       * @type {DocumentFragment}
       */

      get: function () {
        return Object.getOwnPropertyDescriptor(ZComponent, "template").get.call(this);
      }
    }
  });
  return ZStates;
})(ZComponent);

module.exports = ZStates;
}});

require.define({"components/z/matrix/row": function(exports, require, module) {
"use strict";

var _libUtils = require("lib/utils");

var copy = _libUtils.copy;
var mixin = _libUtils.mixin;
var Eventable = babelHelpers.interopRequire(require("mixins/eventable"));
var ZComponent = babelHelpers.interopRequire(require("z-component"));

var ZMatrixRow = (function (_ZComponent) {
  function ZMatrixRow() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZMatrixRow);

    babelHelpers.get(Object.getPrototypeOf(ZMatrixRow.prototype), "constructor", this).call(this, options);

    var header = this.header = this.$(".z-matrix__header__text");
    var popover = options.popover;
    var Field = options.Field;
    var fieldOptions = options.fieldOptions;
    var columns = ZMatrixRow.COLUMNS;

    header.textContent = options.header;

    if (popover) {
      this.setupPopover(popover);
    }

    this.fields = [];

    while (columns > 0) {
      this.addField(Field, copy(fieldOptions), options.changeEvent);
      columns--;
    }
  }

  babelHelpers.inherits(ZMatrixRow, _ZComponent);
  babelHelpers.createClass(ZMatrixRow, {
    value: {
      get: function () {
        return this.fields.map(function (field) {
          return field.value;
        });
      },
      set: function (val) {
        var fields = this.fields;

        val.forEach(function (value, index) {
          fields[index].value = value;
        });
      }
    },
    remove: {
      value: function remove() {
        this.fields.forEach(function (field) {
          return field.remove();
        });
        babelHelpers.get(Object.getPrototypeOf(ZMatrixRow.prototype), "remove", this).call(this);
      }
    },
    onChange: {
      value: function onChange() {
        this.emit("change", this, this.value);
      }
    },
    addField: {
      value: function addField(Klass, options, changeEvent) {
        var div = document.createElement("div");
        var field = new Klass(options);
        var fields = this.fields;

        div.classList.add("z-matrix__col");
        field.appendTo(div);
        field.on(changeEvent, this.onChange, this);
        fields.push(field);
        this.el.appendChild(div);
      }
    },
    setupPopover: {
      value: function setupPopover(options) {
        var header = this.header;
        var popover = this.popover = new ZMatrixRow.Popover(options);
        var onMouseOver = function () {
          popover.active = true;
        };
        var onMouseLeave = function () {
          popover.active = false;
        };

        popover.appendTo(header);
        header.classList.add("z-matrix__header__text--has-popover");
        header.addEventListener("mouseover", onMouseOver, false);
        header.addEventListener("mouseleave", onMouseLeave, false);
      }
    }
  }, {
    classList: {
      get: function () {
        return ["z-matrix__row"];
      }
    },
    template: {
      get: function () {
        var template = ZComponent.template;

        template.fragment.firstChild.innerHTML = "<div class=\"z-matrix__col z-matrix__header z-matrix__header--row\">\n        <span class=\"z-matrix__header__text\"></span>\n      </div>".replace(/>\s+/g, ">");

        return template;
      }
    }
  });
  return ZMatrixRow;
})(ZComponent);

mixin(ZMatrixRow.prototype, Eventable);

module.exports = ZMatrixRow;
}});

require.define({"components/z/condition/parts/date": function(exports, require, module) {
"use strict";

var I18n = babelHelpers.interopRequire(require("lib/i18n"));
var moment = babelHelpers.interopRequire(require("moment"));
var ZDate = babelHelpers.interopRequire(require("components/z/fields/date"));

var TRANSLATIONS = {
  monthsShort: [I18n.t("months.abbreviated.1"), I18n.t("months.abbreviated.2"), I18n.t("months.abbreviated.3"), I18n.t("months.abbreviated.4"), I18n.t("months.abbreviated.5"), I18n.t("months.abbreviated.6"), I18n.t("months.abbreviated.7"), I18n.t("months.abbreviated.8"), I18n.t("months.abbreviated.9"), I18n.t("months.abbreviated.10"), I18n.t("months.abbreviated.11"), I18n.t("months.abbreviated.12")],
  weekdaysShort: [I18n.t("days.abbreviated.7"), I18n.t("days.abbreviated.1"), I18n.t("days.abbreviated.2"), I18n.t("days.abbreviated.3"), I18n.t("days.abbreviated.4"), I18n.t("days.abbreviated.5"), I18n.t("days.abbreviated.6")]
};

moment.locale("moment", TRANSLATIONS);

var ZConditionDate = (function (_ZDate) {
  function ZConditionDate(options) {
    babelHelpers.classCallCheck(this, ZConditionDate);

    this._onChange = options.onChange;

    babelHelpers.get(Object.getPrototypeOf(ZConditionDate.prototype), "constructor", this).call(this, options);
  }

  babelHelpers.inherits(ZConditionDate, _ZDate);
  babelHelpers.createClass(ZConditionDate, {
    formatted: {
      get: function () {
        return this.value.format("YYYY-MM-DD");
      }
    },
    parse: {
      value: function parse(val) {
        return moment(val);
      }
    },
    onSelect: {
      value: function onSelect(date) {
        babelHelpers.get(Object.getPrototypeOf(ZConditionDate.prototype), "onSelect", this).call(this, date);

        this._onChange({
          value: this.formatted
        });
      }
    }
  });
  return ZConditionDate;
})(ZDate);

module.exports = ZConditionDate;
}});

require.define({"components/z/condition/parts/datetime": function(exports, require, module) {
"use strict";

var ZConditionDate = babelHelpers.interopRequire(require("components/z/condition/parts/date"));

var ZConditionDateTime = (function (_ZConditionDate) {
  function ZConditionDateTime() {
    babelHelpers.classCallCheck(this, ZConditionDateTime);

    if (_ZConditionDate != null) {
      _ZConditionDate.apply(this, arguments);
    }
  }

  babelHelpers.inherits(ZConditionDateTime, _ZConditionDate);
  babelHelpers.createClass(ZConditionDateTime, {
    formatted: {
      get: function () {
        return this.value.format("YYYY-MM-DD 00:00");
      }
    }
  });
  return ZConditionDateTime;
})(ZConditionDate);

module.exports = ZConditionDateTime;
}});

require.define({"components/z/condition/parts/index": function(exports, require, module) {
"use strict";

var ConditionDate = babelHelpers.interopRequire(require("./date"));
var ConditionDateTime = babelHelpers.interopRequire(require("./datetime"));
var ConditionList = babelHelpers.interopRequire(require("./list"));
var ConditionSearch = babelHelpers.interopRequire(require("./search"));
var ConditionTags = babelHelpers.interopRequire(require("./tags"));
module.exports = {
  autocomplete: ConditionSearch,
  date: ConditionDate,
  datetime: ConditionDateTime,
  list: ConditionList,
  tags: ConditionTags
};
}});

require.define({"components/z/condition/parts/list": function(exports, require, module) {
"use strict";

var ComboSelectMenu = require("zendesk-menus").ComboSelectMenu;

var ZConditionList = (function (_ComboSelectMenu) {
  function ZConditionList() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZConditionList);

    options.defaultValueLabel = options.placeholder || "-";

    babelHelpers.get(Object.getPrototypeOf(ZConditionList.prototype), "constructor", this).call(this, options);

    this.data = options.data;
    this.onChange = options.onChange;
  }

  babelHelpers.inherits(ZConditionList, _ComboSelectMenu);
  babelHelpers.createClass(ZConditionList, {
    appendTo: {
      value: function appendTo(target) {
        babelHelpers.get(Object.getPrototypeOf(ZConditionList.prototype), "appendTo", this).call(this, target);

        this.loadData(this.data);

        return this;
      }
    },
    remove: {
      value: function remove() {
        this.destroy();
      }
    },
    setValue: {
      value: function setValue(value, source) {
        babelHelpers.get(Object.getPrototypeOf(ZConditionList.prototype), "setValue", this).call(this, value, source);

        var label = this.selectMenu.activeItem.label;
        this.domBase.setAttribute("title", label);
      }
    }
  });
  return ZConditionList;
})(ComboSelectMenu);

module.exports = ZConditionList;
}});

require.define({"components/z/condition/parts/search": function(exports, require, module) {
"use strict";

var _zendeskMenus = require("zendesk-menus");

var MenuUtils = _zendeskMenus.MenuUtils;
var SearchMenu = _zendeskMenus.SearchMenu;

var RemoteSearchDataSource = MenuUtils.RemoteSearchDataSource;

var ZConditionSearch = (function (_SearchMenu) {
  function ZConditionSearch() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZConditionSearch);

    options.searchDataSource = new RemoteSearchDataSource(30, options.url + "?name=%@", {
      httpMethod: "GET",
      dataFilter: function (data) {
        return data ? data.organizations : [];
      },
      parse: function (data) {
        return data.map(function (item) {
          return { label: item.name, value: item.id };
        });
      }
    });

    babelHelpers.get(Object.getPrototypeOf(ZConditionSearch.prototype), "constructor", this).call(this, options);

    this.onChange = options.onChange;
  }

  babelHelpers.inherits(ZConditionSearch, _SearchMenu);
  babelHelpers.createClass(ZConditionSearch, {
    renderItemContentForBase: {
      value: function renderItemContentForBase(value, item) {
        return item ? item.label : value;
      }
    },
    remove: {
      value: function remove() {
        this.destroy();
      }
    }
  });
  return ZConditionSearch;
})(SearchMenu);

module.exports = ZConditionSearch;
}});

require.define({"components/z/condition/parts/tags": function(exports, require, module) {
"use strict";

var _zendeskMenus = require("zendesk-menus");

var TagMenu = _zendeskMenus.TagMenu;
var MenuUtils = _zendeskMenus.MenuUtils;

var RemoteSearchDataSource = MenuUtils.RemoteSearchDataSource;

var ZConditionTags = (function (_TagMenu) {
  function ZConditionTags() {
    var options = arguments[0] === undefined ? {} : arguments[0];
    babelHelpers.classCallCheck(this, ZConditionTags);

    options.searchEditorOptions = {
      searchDataSource: new RemoteSearchDataSource(30, "/api/v2/autocomplete/tags.json?name=%@", {
        httpMethod: "GET",
        dataFilter: function (data) {
          return data ? data.tags : [];
        },
        parse: function (data) {
          return data.map(function (item) {
            return { label: item, value: item };
          });
        },
        performCaching: true
      })
    };

    babelHelpers.get(Object.getPrototypeOf(ZConditionTags.prototype), "constructor", this).call(this, options);

    this.data = options.data;
    this.onChange = options.onChange;
  }

  babelHelpers.inherits(ZConditionTags, _TagMenu);
  babelHelpers.createClass(ZConditionTags, {
    appendTo: {
      value: function appendTo(target) {
        babelHelpers.get(Object.getPrototypeOf(ZConditionTags.prototype), "appendTo", this).call(this, target);

        this.loadData(this.data);

        this.on("tagsChanged", this.onTagsChanged);
      }
    },
    focus: {
      value: function focus() {
        var _this = this;

        setTimeout(function () {
          return babelHelpers.get(Object.getPrototypeOf(ZConditionTags.prototype), "focus", _this).call(_this);
        });
      }
    },
    setValue: {
      value: function setValue(value) {
        if (typeof value !== "string") {
          value = value.toString();
        }

        this.setValues(value.split(/\s+/));
      }
    },
    onTagsChanged: {
      value: function onTagsChanged() {
        this.onChange({ value: this.getTags().join(" ") });
      }
    },
    remove: {
      value: function remove() {
        this.destroy();
      }
    }
  });
  return ZConditionTags;
})(TagMenu);

module.exports = ZConditionTags;
}});
