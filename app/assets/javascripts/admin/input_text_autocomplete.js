/*globals jQuery, LiquidPlaceholders*/

(function($) {
  var initialized = false;

  function Selectable() {
    this.reset();
  }

  Selectable.prototype.reset = Selectable.prototype.update = function(items) {
    var previous = this.active;
    this.selected = 0;
    this.items = items || [];
    this.active = this.items.length > 0;

    if (previous !== this.active) {
      if (this.active) {
        this.activated();
      } else {
        this.hide();
      }
    }
  };

  Selectable.prototype.keyMap = {
    tab: 9,
    enter: 13,
    esc: 27,
    up: 38,
    down: 40
  };

  Selectable.prototype.keyMapReverse = [9, 13, 27, 38, 40];

  Selectable.prototype.attach = function(textInput) {
    var self = this;

    if (!initialized) {
      $('body').append('<div id="suggest"></div>');
      initialized = true;
    }

    $(textInput).unbind('.selectable').bind('keydown.selectable',  function(e) {
      self.keyDown(this, e);
    });
  };

  Selectable.prototype.detach = function(textInput) {
    $(textInput).unbind('keydown.selectable');
    this.reset();
    this.hide();
  };

  Selectable.prototype.keyDown = function(element, event) {
    if (!this.active) {
      return;
    }

    var prevent = true,
      key = Selectable.prototype.keyMap;

    switch(event.keyCode) {
      case key.up:
        this.selected = Math.max(this.selected - 1, 0);
        break;
      case key.down:
        this.selected = Math.min(this.selected + 1, this.items.length - 1);
        break;
      case key.tab:
      case key.enter:
        this.complete();
        break;
      case key.esc:
        this.reset();
        break;
      default:
        prevent = false;
    }

    if (prevent) {
      event.preventDefault();
      this.render();
    }
  };

  Selectable.prototype.isNavigationEvent = function(event) {
    return ($.inArray(event.keyCode, Selectable.prototype.keyMapReverse) > -1);
  };

  Selectable.prototype.render = function() {
    if (!this.items) {
      return;
    }

    var start = this.selected,
      end = start + 5; // max of 5 autocomplete suggestions at a time

    if (start > 0) {
      start--;
      end--;
    }

    var views = $.map(this.items, this.renderItem).slice(start, end);
    $("#suggest").html('<ul>' + views.join('') + '</ul>');

    return this;
  };

  Selectable.prototype.hide = function() {
    this.active = false;
    $("#suggest").hide();

    return this;
  };

  Selectable.prototype.show = function() {
    $("#suggest").show();

    return this;
  };

  Selectable.prototype.move = function(pos) {
    $("#suggest").css(pos);

    return this;
  };

  function InputSuggest(textInput) {
    var self = this;

    textInput.bind('focusin', function(e) {
      self.attach(this, e);
    });

    textInput.bind('focusout', function(e) {
      self.detach(this, e);
    });

    this.re = /\{\{[^}]*$/;
    this.lastToken = '';

    this.suggestions = LiquidPlaceholders();
    this.selectable = new Selectable();
    this.selectable.complete = function() {
      self.complete();
    };

    this.selectable.renderItem = function(item, index) {
      return '<li class="suggest-li '
        + (index === self.selectable.selected ? 'suggest-li-focus' : '')
        + '">' + item + '<div class="suggest-detail">' + self.suggestions[item] + '</div></li>';
    };

    this.selectable.activated = function() {
      var sel = self.getTokenPos(self.getText(), self.getCaretPos()),
        pos = self.getInputTextCoordsByIndex(self.textInput, sel.start),
        cpos = $(self.textInput).position();
      self.selectable.move({top: pos.top + cpos.top, left: pos.left + cpos.left}).show();
    };
  }

  InputSuggest.prototype.match = function(search) {
    if (!this.re.test(search)) {
      return [];
    }

    var filtered = [];

    for (var key in this.suggestions) {
      if (search.length < key.length
        && this.suggestions.hasOwnProperty(key)
        && key.substr(0, search.length) === search) {
        filtered.push(key);
      }
    }

    return filtered;
  };

  InputSuggest.prototype.getToken = function() {
    var content = this.getText(),
      pos = this.getTokenPos(content, this.getCaretPos());

    return content.substring(pos.start, pos.end);
  };

  InputSuggest.prototype.getTokenPos = function(content, caretPos) {
    var index = content.search(this.re);

    return {start: index, end: caretPos};
  };

  InputSuggest.prototype.keyUp = function(element, event) {
    var token = this.getToken();

    if (this.lastToken !== token) {
      this.selectable.update(this.match(token));
      this.selectable.render();
      this.lastToken = token;
    }
  };

  InputSuggest.prototype.complete = function() {
    var content = this.getText(),
      pos = this.getTokenPos(content, this.getCaretPos()),
      placeholderEnd = content.substr(0, pos.start) + this.selectable.items[this.selectable.selected];

    this.textInput.value = placeholderEnd + content.substr(pos.end);
    this.selectable.reset();
    this.setCaretToPos(this.textInput, placeholderEnd.length);
  };

  InputSuggest.prototype.attach = function(el) {
    var self = this;

    $(el).unbind('.suggest').bind('keyup.suggest', function(ev) {
      self.keyUp(this, ev);
    });

    this.textInput = el;
    this.selectable.attach(el);
  };

  InputSuggest.prototype.detach = function(el) {
    $(el).unbind('keyup.suggest');
    this.textInput = null;
    this.selectable.detach(el);
  };

  InputSuggest.prototype.getText = function() {
    return this.textInput.value;
  };

  InputSuggest.prototype.getCaretPos = function() {
    return this.textInput.selectionEnd;
  };

  InputSuggest.prototype.getInputTextCoordsByIndex = function(inputText, index){
    if (index === null) {
      return;
    }

    var properties = ["height", "width", "padding-top", "padding-right", "padding-bottom", "padding-left",
        "lineHeight", "textDecoration", "letterSpacing", "font-family", "font-size", "font-style", "font-variant",
        "font-weight"],
      elProp = {position: "absolute", overflow: "auto", "white-space": "pre-wrap", top: 0, left: -9999},
      span = document.createElement('span'),
      el = document.createElement('div'),
      jEl = $(el),
      pos;

    for (var i = 0, len = properties.length; i < len; i++) {
      elProp[properties[i]] = $(inputText).css(properties[i]);
    }

    span.innerHTML = '&nbsp;';
    jEl.css(elProp).text(inputText.value.substring(0, index)).insertAfter(inputText);
    el.scrollTop = el.scrollHeight;
    el.appendChild(span);
    pos = $(span).position();
    jEl.remove();

    return pos;
  };

  InputSuggest.prototype.setSelectionRange = function(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
      var range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', selectionEnd);
      range.moveStart('character', selectionStart);
      range.select();
    }
  };

  InputSuggest.prototype.setCaretToPos = function(input, pos) {
    this.setSelectionRange(input, pos, pos);
  };

  window.InputSuggest = InputSuggest;
})(jQuery);
