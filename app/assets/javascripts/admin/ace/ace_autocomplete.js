/*globals jQuery, LiquidPlaceholders*/

(function($) {
  var initialized = false;

  function AceSelectable(editor) {
    this.editor = editor;
    this.attach();
    this.reset();
  }

  AceSelectable.prototype.reset = AceSelectable.prototype.update = function(items) {
    var previous = this.active;
    this.selected = 0;
    this.items = items || [];
    this.active = this.items.length > 0;
    // if something changed
    if (previous != this.active) {
      if (this.active) {
        this.activated();
      } else {
        this.hide();
        this.addCommands();
      }
    }
  };

  AceSelectable.prototype.keyMap = {
    tab: 9,
    enter: 13,
    esc: 27,
    up: 38,
    down: 40
  };

  AceSelectable.prototype.attach = function() {
    var self = this;
    if (!initialized) {
      $('body').append('<div id="suggest"></div>');
      initialized = true;
    }

   this.getTextArea()
     .unbind('.selectable')
     .bind('keydown.selectable', function(e) {
       self.keyDown(this, e);
     });
  };

  AceSelectable.prototype.addCommands = function() {
    this.editor.commands.addCommands([
      {
        name: "golineup",
        bindKey: {win: "Up", mac: "Up|Ctrl-P"},
        exec: function(editor, args) {
          editor.navigateUp(args.times);
        },
        multiSelectAction: "forEach",
        readOnly: true
      },
      {
        name: "golinedown",
        bindKey: {win: "Down", mac: "Down|Ctrl-N"},
        exec: function (editor, args) {
          editor.navigateDown(args.times);
        },
        multiSelectAction: "forEach",
        scrollIntoView: "cursor",
        readOnly: true
      }
    ]);
  };

  AceSelectable.prototype.detach = function() {
    this.getTextArea().unbind('keydown.selectable');
    this.reset();
    this.addCommands();
    this.hide();
  };

  AceSelectable.prototype.getTextArea = function() {
    return $(this.editor.renderer.getTextAreaContainer()).find(".ace_text-input");
  };

  // keyDown is needed to prevent defaults
  AceSelectable.prototype.keyDown = function(element, event) {
    if (!this.active) return;

    var prevent = true,
      key = AceSelectable.prototype.keyMap;

    switch(event.keyCode) {
      case key.up:
        this.selected = Math.max(this.selected - 1, 0);
        break;
      case key.down:
        this.selected = Math.min(this.selected + 1, this.items.length - 1);
        break;
      case key.tab:
      case key.enter:
        this.complete();
        break;
      case key.esc:
        this.reset();
        break;
      default:
        prevent = false;
    }

    if (prevent) {
      event.preventDefault();
      this.render();
    }
  };

  AceSelectable.prototype.render = function() {
    if (!this.items) {
      return;
    }

    var start = this.selected,
      end = start + 5,
      views;

    if (start > 0) {
      start--;
      end--;
    }

    views = $.map(this.items, this.renderItem).slice(start, end);
    $('#suggest').html('<ul>' + views.join('') + '</ul>');

    return this;
  };

  AceSelectable.prototype.hide = function() {
    this.active = false;
    $('#suggest').hide();
  };

  AceSelectable.prototype.show = function() {
    $('#suggest').show();
  };

  AceSelectable.prototype.move = function(position) {
    $('#suggest').css(position);

    return this;
  };

  function AceSuggest(editor, placeholderType) {
    var self = this;

    this.suggestions = LiquidPlaceholders(placeholderType);
    this.editor = editor;
    this.re = /\{\{[^}]*$/;
    this.lastToken = '';
    this.selectable = new AceSelectable(editor);

    this.selectable.complete = function() {
      self.complete();
    };

    this.selectable.renderItem = function(item, index) {
      return '<li class="suggest-li '
        + (index == self.selectable.selected ? 'suggest-li-focus' : '')
        + '">' + item + '<div class="suggest-detail">' + self.suggestions[item] + '</div></li>';
    };

    this.selectable.activated = function() {
      var currentPosition = this.editor.getCursorPosition(),
        screenCoordinates = self.editor.renderer.textToScreenCoordinates(currentPosition.row, currentPosition.column);

      self.selectable.move({
        top: screenCoordinates.pageY + $(document).scrollTop(),
        left: screenCoordinates.pageX
      }).show();

      self.editor.commands.removeCommands([{name: 'golineup'}, {name: 'golinedown'}]);
    };
  }

  AceSuggest.prototype.match = function(search) {
    // exclude any strings that don't match the full start of the string
    var filtered = [],
      key;
    // check if the token could even match
    if (!this.re.test(search)) {
      return [];
    }

    for (key in this.suggestions) {
      if (search.length < key.length // search should be shorter than any token we try
        && this.suggestions.hasOwnProperty(key)
        && key.substr(0, search.length) === search) {
        filtered.push(key);
      }
    }

    return filtered;
  };

  AceSuggest.prototype.getToken = function() {
    var currentPosition = this.editor.getCursorPosition(),
      line = this.editor.getSession().getLine(currentPosition.row),
      beforeCursor = line.substr(0, currentPosition.column),
      token;

    if (beforeCursor.lastIndexOf("{{") === -1) {
      token = beforeCursor;
    } else {
      token = beforeCursor.substr(beforeCursor.lastIndexOf("{{"));
    }

    return token;
  };

  AceSuggest.prototype.keyUp = function() {
    var token = this.getToken();
    // do not update if the token does not change
    if (this.lastToken != token) {
      this.selectable.update(this.match(token));
      // only render if we update (navigation events are rendered by selectable)
      this.selectable.render();
      this.lastToken = token;
    }
  };

  AceSuggest.prototype.complete = function() {
    var content = this.getText(),
      currentPosition = this.editor.getCursorPosition(),
      currentRow = currentPosition.row,
      currentColumn = currentPosition.column,
      lines = this.editor.getSession().getLines(0, currentRow),
      placeholder = this.selectable.items[this.selectable.selected],
      totalLength = 0,
      start,
      end;

    if (lines.length > 0) {
      for (var i = 0, l = lines.length; i < l - 1; i++) {
        totalLength = totalLength + lines[i].length + 1
      }

      start = totalLength + lines[currentRow].lastIndexOf("{{");
      end = totalLength + currentColumn;
    } else {
      start = lines[0].lastIndexOf("{{");
      end = currentColumn;
    }

    this.editor.setValue(content.substr(0, start) + placeholder + content.substr(end));
    this.editor.clearSelection();
    this.editor.moveCursorTo(currentRow, lines[currentRow].lastIndexOf("{{") + placeholder.length);
    this.selectable.reset();
  };

  AceSuggest.prototype.attach = function() {
    this.selectable.attach(this.editor);
  };

  AceSuggest.prototype.detach = function() {
    this.editor = null;
    this.selectable.detach();
  };

  AceSuggest.prototype.getText = function() {
    return this.editor.getValue();
  };

  window.AceSuggest = AceSuggest;
})(jQuery);
