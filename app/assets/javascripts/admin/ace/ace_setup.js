/*globals jQuery, ace, AceSuggest, LiquidPlaceholders*/

//= require_tree ../vendor/ace
//= require admin/ace/ace_autocomplete
//= require admin/ace/ace_config
//= require admin/liquid_placeholders

(function($) {
  function AceSetup(id, resizableContainer, textArea, mode, maxWidth, placeholderPrefix) {
    var editor = ace.edit(id);
    editor.setTheme("ace/theme/chrome");
    editor.setShowPrintMargin(false);

    if (placeholderPrefix) {
      var suggest = new AceSuggest(editor, placeholderPrefix);

      var keyUpFunction = function() {
        suggest.keyUp();
      };

      editor.commands.on("afterExec", keyUpFunction);
      editor.selection.on("changeCursor", keyUpFunction);
      editor.on("focus", keyUpFunction);
      editor.on("blur", function() {
        suggest.selectable.reset();
      });
    }

    var Mode = ace.require("ace/mode/" + mode).Mode;

    var session = editor.getSession();
    session.setMode(new Mode());
    session.setValue(textArea.val());
    session.setTabSize(2);
    session.on("change", function() {
      textArea.val(session.getValue());
    });

    resizableContainer.resizable({
      resize: function() {
        editor.resize();
        $.colorbox.resize();
      },
      alsoResize: "#" + id,
      minHeight: resizableContainer.height(),
      maxWidth: maxWidth,
      minWidth: maxWidth,
      handles: "se"
    });

    return editor;
  }

  window.AceSetup = AceSetup;
})(jQuery);
