/*

How to use

-------------- HTML --------------

<button data-ztooltip-header="Awesome header"
        data-ztooltip-body="Nice <b>tooltip</b> body"
        data-ztooltip-dir="left"
        data-ztooltip-animated="true">Tooltip anchor</button>

------------ Javascript ------------

$(function() {
  $('button').ztooltip();
});


Alternatively:

-------------- HTML --------------

<button>Tooltip anchor</button>

------------ Javascript ------------

$(function() {
  $('button').ztooltip({
    header: "Awesome header",
    body: "Nice <b>tooltip</b> body",
    dir: "left",
    animated: true
  });
});

*/

(function() {

  var types = {};

  var hoverContentTemplate =
    '<span class="close">×</span>' +
    '<div class="z-header">' +
      '<h3>{header}</h3>' +
    '</div>' +
    '<div class="z-body">{body}</div>';

  var tooltipId = 1;

  function emptyFn(){}

  function ZTooltip(to, options) {
    this.to = $(to);
    if (this.to.data('ztooltip')) return;

    this.id = 'ztooltip' + tooltipId++;
    this.el = null;

    if (options == null) {
      var type = this.to.data('ztooltip-type');
      options = type && types[type];
    } else if (typeof options === 'string') {
      options = types[options];
    }
    options = options || {};

    this.name               = this.to.data('ztooltip-name') || options.name;
    this.animationDuration  = options.animationDuration == null ? 500 : options.animationDuration;
    this.alignmentOffset    = this.to.data('ztooltip-alignment-offset') || (options.alignmentOffset == null ? 20 : options.alignmentOffset);
    this.proximityOffset    = this.to.data('ztooltip-proximity-offset') || (options.proximityOffset == null ? 15 : options.proximityOffset);
    this.dir                = this.to.data('ztooltip-dir') || options.dir || 'up';
    this.header             = getHeaderFromDom(this.to) || options.header || '';
    this.body               = getBodyFromDom(this.to) || options.body || '';
    this.showEvent          = options.showEvent || 'mouseenter';
    this.hideEvent          = options.hideEvent || 'mouseleave';
    this.onShow             = options.onShow || emptyFn;
    this.onHide             = options.onHide || emptyFn;
    this.isVisible          = false;
    this.hideTimeout        = this.to.data('ztooltip-hide-timeout') || options.hideTimeout || 0;
    this.destroyOnHide      = options.hasOwnProperty('destroyOnHide') ? options.destroyOnHide : false;
    this.animated           = this.to.data('ztooltip-animated') || (options.hasOwnProperty('animated') ? options.animated : false);
    this.animation          = this.to.data('ztooltip-animation') || options.animation || 'fade';
    this.className          = this.to.data('ztooltip-class') || options.className || '';
    this.closeOnOutsideClick= options.hasOwnProperty('closeOnOutsideClick') ? options.closeOnOutsideClick : true;
    this.showEventToggles   = options.hasOwnProperty('showEventToggles') ? options.showEventToggles : false;
    this.template           = getHtmlFromDom(this.to) || options.template || hoverContentTemplate;

    if (!this.animated) {
      this.animationDuration = 0;
    }

    if (this.to.data('ztooltip')) return;

    if (this.showEvent) {
      this.to.on(this.showEvent + '.' + this.id, function(e) {
        if (this.showEventToggles) {
          e.stopPropagation();
          this.isVisible ? this.hide() : this.show();
        } else {
          this.show();
        }
      }.bind(this));
    }

    this.hideEvent && this.to.on(this.hideEvent + '.' + this.id, this.hide.bind(this));

    $(document).on('ztooltip.show ztooltip.closeAll', function(e, data) {
      if (data === this) return;
      this.hide();
    }.bind(this));

    this.to.data('ztooltip', this);
  }

  function getHeaderFromDom(to) {
    var attr = to.data('ztooltip-header');
    if (attr) return attr;

    return to.find('> script[type="text/x-ztooltip-header"]').text();
  }

  function getBodyFromDom(to) {
    var attr = to.data('ztooltip-body');
    if (attr) return attr;

    return to.find('> script[type="text/x-ztooltip-body"]').text();
  }

  function getHtmlFromDom(to) {
    return to.find('> script[type="text/x-ztooltip-html"]').text();
  }

  function position(zt) {
    var offset = zt.to.offset(),
        top, left;

    switch (zt.dir) {
      case 'down':
        left = offset.left - zt.alignmentOffset;
        top  = offset.top + zt.to[0].offsetHeight + zt.proximityOffset;
        break;
      case 'up':
        left = offset.left - zt.alignmentOffset;
        top  = offset.top - zt.el[0].offsetHeight - zt.proximityOffset;
        break;
      case 'right':
        left = offset.left + zt.to[0].offsetWidth + zt.proximityOffset;
        top  = offset.top - zt.alignmentOffset;
        break;
      case 'left':
        left = offset.left - zt.el[0].offsetWidth - zt.proximityOffset;
        top  = offset.top - zt.alignmentOffset;
        break;
    }

    zt.el.css({
      top:  top + 'px',
      left: left + 'px'
    });
  }

  var _data, matchingRe = /\{([^\}]+)\}/g;

  function replacer(match, token) {
    return _data[token];
  }

  function interpolate(template, data) {
    _data = data;
    var result = template.replace(matchingRe, replacer);
    _data = null;
    return result;
  }

  function render(zt) {
    return interpolate(zt.template, {
      header: zt.header,
      body: zt.body
    });
  }

  function insertIntoDom(zt) {
    zt.el = $('<div>', {
              'class': ['z-tooltip',
                      zt.className,
                      zt.dir,
                      (zt.animated ? 'animated' : ''),
                      zt.animation].join(' '),
              html: render(zt)
            })
            .appendTo(document.body)
            .on('click', '.close', function(e) {
              zt.hide();
              e.stopPropagation();
            })
            .attr('id', zt.id);

    if (zt.closeOnOutsideClick) {
      $(document).on('click.' + zt.id, onDocumentClick.bind(zt));
    }
    zt.el.data('ztooltip', zt);
  }

  function withinTooltip(el) {
    return $(el).closest('.z-tooltip').length > 0;
  }

  function onDocumentClick(e) {
    if (withinTooltip(e.target)) { return; }

    var isOpener = this.to[0].contains(e.target);
    if (e.target !== this.el[0] && !isOpener && !this.el[0].contains(e.target)) {
      this.hide();
    }
  }

  function animateIn(zt) {
    if (!zt.el) return;
    zt.el.addClass('active');

    return animate(zt, zt.onShow.bind(zt));
  }

  function animateOut(zt, onDone) {
    if (!zt.el) return;
    zt.el.removeClass('active');

    return animate(zt, function() {
      zt.onHide();
      onDone && onDone();
    });
  }

  function animate(zt, callback) {
    setTimeout(callback, zt.animationDuration);
  }

  ZTooltip.prototype.rerender = function() {
    this.el.html(render(this));
  };

  ZTooltip.prototype.show = function() {
    var self = this;
    if (this.isVisible) return;
    this.isVisible = true;
    if (!this.el) {
      insertIntoDom(this);
    }
    this.el.show();
    position(this);

    $(document).trigger('ztooltip.show', this);
    $(window).on('resize.' + this.id, this.hide.bind(this));
    setTimeout(animateIn.bind(null, this));
    if (this.hideTimeout) {
      this.hideTimer && clearTimeout(this.hideTimer);
      this.hideTimer = setTimeout(this.hide.bind(this), this.hideTimeout * 1000);
    }
  };

  ZTooltip.prototype.hide = function() {
    $(window).off('resize.' + this.id);
    this.isVisible = false;
    this.hideTimer && clearTimeout(this.hideTimer);

    animateOut(this, function() {
      this.el.hide();
      this.destroyOnHide && this.destroy();
    }.bind(this));
  };

  ZTooltip.prototype.destroy = function() {
    if (this.isDestroyed) return;
    this.el.remove();
    this.to.data('ztooltip', null);
    this.to.off(this.showEvent + '.' + this.id);
    this.hideEvent && this.to.off(this.hideEvent + '.' + this.id);

    this.to = this.el = null;
    this.onShow = this.onHide = emptyFn;
    this.isDestroyed = true;

    $(document).off('.' + this.id);
    $(document).off('ztooltip.show');
  };

  var events = {};

  $.ztooltip = {
    registerType: function(type, options) {
      options = options || {};
      types[type] = options;
      var showEvent = options.showEvent || 'mouseenter';
      if (events[showEvent]) return;
      events[showEvent] = showEvent;

      $(document).on(showEvent, '[data-ztooltip-type]' , function(e) {
        if ($(this).data('ztooltip')) return;

        var t, domType = this.getAttribute('data-ztooltip-type');

        var options = types[domType];
        if (options && options.showEvent === e.type) {
          e.stopPropagation();
          t = new ZTooltip(this, types[domType]);
          t.show();
        }
      });

    },

    on: function(action, callback) {
      $(document).off('ztooltip.action.' + action);
      $(document).on('ztooltip.action.' + action, function(e, tooltip) {callback.call(tooltip, action);});
      return this;
    }
  };

  $.ztooltip.registerType('hover', {
    animated: true
  });

  $.ztooltip.registerType('clickjack', {
    showEvent: 'click',
    hideEvent: 'noop',
    showEventToggles: true,
    animated: true,
    className: 'generic'
  });

  $.ztooltip.registerType('actionable', {
    proximityOffset: 5,
    alignmentOffset: 0,
    animated: true,
    className: 'actionable',
    showEvent: 'click',
    hideEvent: 'noop',
    showEventToggles: true
  });

  // bootsrap actions

  $(document).on('click', '[data-ztooltip-action]', function(e) {
    var tooltip = $(e.target).parents('.z-tooltip').eq(0).data('ztooltip');
    $(document).trigger('ztooltip.action.' + $(e.target).data('ztooltip-action'), tooltip);
  });

  // boostrap jQuery factory

  $.fn.ztooltip = function(options) {
    options = options || {};
    return this.each(function() {
      if ($(this).data('ztooltip')) return;
      var ztype = $(this).data('ztooltip-type');
      if (ztype && types[ztype]) {
        options = $.extend({}, types[ztype], options);
      }
      new ZTooltip(this, options);
    });
  };

  $.ztooltip.hideAll = function() {
    $(document).trigger('ztooltip.closeAll');
  };

  $.ztooltip.show = function(name) {
    $('[data-ztooltip-name=' + name + ']:visible')
      .ztooltip()
      .each(function() {
        $(this).data('ztooltip').show();
      });
  };

  $.ztooltip.hide = function(name) {
    $('[data-ztooltip-name=' + name + ']').each(function() {
      var tooltip = $(this).data('ztooltip');
      tooltip && tooltip.hide();
    });
  };

}());
