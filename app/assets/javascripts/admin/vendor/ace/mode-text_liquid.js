ace.define("ace/mode/text_liquid_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var TextLiquidHighlightRules = function() {
  TextHighlightRules.call(this);
    var functions = (
        "date|capitalize|downcase|upcase|first|last|join|sort|map|size|escape|" +
         "escape_once|strip_html|strip_newlines|newline_to_br|replace|replace_first|" +
         "truncate|truncatewords|prepend|append|minus|plus|times|divided_by|split"
    );

    var keywords = (
        "capture|endcapture|case|endcase|when|comment|endcomment|" +
        "cycle|for|endfor|in|reversed|if|endif|else|elsif|include|endinclude|unless|endunless|" +
        "style|text|image|widget|plugin|marker|endmarker|tablerow|endtablerow"
    );

    var builtinVariables = 'forloop|tablerowloop';

    var definitions = ("assign");

    var keywordMapper = this.createKeywordMapper({
        "variable.language": builtinVariables,
        "keyword": keywords,
        "support.function": functions,
        "keyword.definition": definitions
    }, "identifier");
    for (var rule in this.$rules) {
        this.$rules[rule].unshift({
            token : "variable",
            regex : "{%",
            push : "liquid-start"
        }, {
            token : "variable",
            regex : "{{",
            push : "liquid-start"
        });
    }

    this.addRules({
        "liquid-start" : [{
            token: "variable",
            regex: "}}",
            next: "pop"
        }, {
            token: "variable",
            regex: "%}",
            next: "pop"
        }, {
            token : "string", // single line
            regex : '["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]'
        }, {
            token : "string", // single line
            regex : "['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']"
        }, {
            token : "constant.numeric", // hex
            regex : "0[xX][0-9a-fA-F]+\\b"
        }, {
            token : "constant.numeric", // float
            regex : "[+-]?\\d+(?:(?:\\.\\d*)?(?:[eE][+-]?\\d+)?)?\\b"
        }, {
            token : "constant.language.boolean",
            regex : "(?:true|false)\\b"
        }, {
            token : keywordMapper,
            regex : "[a-zA-Z_$][a-zA-Z0-9_$]*\\b"
        }, {
            token : "keyword.operator",
            regex : "\/|\\*|\\-|\\+|=|!=|\\?\\:"
        }, {
            token : "paren.lparen",
            regex : /[\[\({]/
        }, {
            token : "paren.rparen",
            regex : /[\])}]/
        }, {
            token : "text",
            regex : "\\s+"
        }]
    });

    this.normalizeRules();
};
oop.inherits(TextLiquidHighlightRules, TextHighlightRules);

exports.TextLiquidHighlightRules = TextLiquidHighlightRules;
});

ace.define("ace/mode/matching_brace_outdent",["require","exports","module","ace/range"], function(require, exports, module) {
"use strict";

var Range = require("../range").Range;

var MatchingBraceOutdent = function() {};

(function() {

    this.checkOutdent = function(line, input) {
        if (! /^\s+$/.test(line))
            return false;

        return /^\s*\}/.test(input);
    };

    this.autoOutdent = function(doc, row) {
        var line = doc.getLine(row);
        var match = line.match(/^(\s*\})/);

        if (!match) return 0;

        var column = match[1].length;
        var openBracePos = doc.findMatchingBracket({row: row, column: column});

        if (!openBracePos || openBracePos.row == row) return 0;

        var indent = this.$getIndent(doc.getLine(openBracePos.row));
        doc.replace(new Range(row, 0, row, column-1), indent);
    };

    this.$getIndent = function(line) {
        return line.match(/^\s*/)[0];
    };

}).call(MatchingBraceOutdent.prototype);

exports.MatchingBraceOutdent = MatchingBraceOutdent;
});

ace.define("ace/mode/text_liquid",["require","exports","module","ace/lib/oop","ace/mode/text","ace/mode/text_liquid_highlight_rules","ace/mode/matching_brace_outdent","ace/range"], function(require, exports, module) {

var oop = require("../lib/oop");
var TextMode = require("./text").Mode;
var TextLiquidHighlightRules = require("./text_liquid_highlight_rules").TextLiquidHighlightRules;
var MatchingBraceOutdent = require("./matching_brace_outdent").MatchingBraceOutdent;
var Range = require("../range").Range;

var Mode = function() {
    this.HighlightRules = TextLiquidHighlightRules;
    this.$outdent = new MatchingBraceOutdent();
};
oop.inherits(Mode, TextMode);

(function() {
    this.getNextLineIndent = function(state, line, tab) {
        var indent = this.$getIndent(line);

        var tokenizedLine = this.getTokenizer().getLineTokens(line, state);
        var tokens = tokenizedLine.tokens;
        var endState = tokenizedLine.state;

        if (tokens.length && tokens[tokens.length-1].type == "comment") {
            return indent;
        }

        if (state == "start") {
            var match = line.match(/^.*[\{\(\[]\s*$/);
            if (match) {
                indent += tab;
            }
        }

        return indent;
    };

    this.checkOutdent = function(state, line, input) {
        return this.$outdent.checkOutdent(line, input);
    };

    this.autoOutdent = function(state, doc, row) {
        this.$outdent.autoOutdent(doc, row);
    };

    this.$id = "ace/mode/text_liquid";
}).call(Mode.prototype);

exports.Mode = Mode;
});
