/**
 * admin.js
 *
 * repo: zendesk/admin_assets
 * version: 0.10.0
 * sha: 9a587d3ce492317faf3f7c99957384f6bd788db7
 */
(function() {
  if (!this.require) {
    var modules = {}, cache = {};

    var require = function(name, root) {
      var path = expand(root, name), indexPath = expand(path, './index'), module, fn;
      module   = cache[path] || cache[indexPath];
      if (module) {
        return module;
      } else if (fn = modules[path] || modules[path = indexPath]) {
        module = {id: path, exports: {}};
        cache[path] = module.exports;
        fn(module.exports, function(name) {
          return require(name, dirname(path));
        }, module);
        return cache[path] = module.exports;
      } else {
        throw 'module ' + name + ' not found';
      }
    };

    var expand = function(root, name) {
      var results = [], parts, part;
      // If path is relative
      if (/^\.\.?(\/|$)/.test(name)) {
        parts = [root, name].join('/').split('/');
      } else {
        parts = name.split('/');
      }
      for (var i = 0, length = parts.length; i < length; i++) {
        part = parts[i];
        if (part == '..') {
          results.pop();
        } else if (part != '.' && part != '') {
          results.push(part);
        }
      }
      return results.join('/');
    };

    var dirname = function(path) {
      return path.split('/').slice(0, -1).join('/');
    };

    this.require = function(name) {
      return require(name, '');
    };

    this.require.define = function(bundle) {
      for (var key in bundle) {
        modules[key] = bundle[key];
      }
    };

    this.require.modules = modules;
    this.require.cache   = cache;
  }

  return this.require;
}).call(this);
;(function( $ ) {
  function zeroState() {
    return $('.zero-state').length !== 0;
  }


  // Begin Capsule plugin
  function Capsule(element, options) {
    this.element = $(element);
    options = $.extend({}, options);
    this.activeHeaderElements = this.element.find(".clickable-header-item");
    if (options.dropdown) {
      this.openText = options.dropdown.openText || "Edit";
      this.closeText = options.dropdown.closeText || "Close";
    }

    var capsule = this,
        menuOptions = [
          {
            element: "<li role='presentation' class='edit-action'><a role='menuitem' tabindex='-1' href='#''>" + capsule.openText + "</a></li>",
            onClick: function(event) {
              capsule.element.closest('.capsule').trigger('open');
            }
          },
          {
            element: "<li role='presentation' class='close-action'><a role='menuitem' tabindex='-1' href='#''>" + capsule.closeText + "</a></li>",
            onClick: function(event) {
              capsule.element.closest('.capsule').trigger('close');
            }
          }
        ];

    if (options.dropdown && options.dropdown.options) {
      menuOptions = menuOptions.concat( options.dropdown.options );
    }

    // Set up bindings to initialize capsule
    this.element.find('.capsule-header').click(function(event) {
      var target = $(event.target);
      if(target.is(capsule.activeHeaderElements) ||
         capsule.activeHeaderElements.find(target).length) {
        return;
      }
      if (!capsule.isDisabled() && !capsule.isNew()) {
        capsule.toggle(event);
      }
    });

    this.element.find('button[type="reset"]').click(function() {
      capsule.close();
      if ( capsule.isNew() ) {
        capsule.element.hide();
      }
    });

    this.element.on('open', this.open.bind(this));
    this.element.on('close', this.close.bind(this));

    this.onOpen = options.onOpen;
    this.onClose = options.onClose;

    if (options.dropdown) {
      capsule.dropdown(menuOptions);
    }
  }

  Capsule.prototype.isDisabled = function() {
    return this.element.hasClass('disabled');
  };

  Capsule.prototype.toggle = function(event) {
    return this.element.hasClass('closed') ? this.open(event) : this.close(event);
  };

  Capsule.prototype.open = function(event) {
    this.element.removeClass('closed');
    if (this.onOpen) { this.onOpen(event); }
    return this.element;
  };

  Capsule.prototype.close = function(event) {
    this.element.addClass('closed');
    if (this.onClose) { this.onClose(event); }
    return this.element;
  };

  Capsule.prototype.isNew = function() {
    return this.element.hasClass('new');
  };

  Capsule.prototype.isDefault = function() {
    return this.element.hasClass('default');
  };

  Capsule.prototype.dropdown = function(options) {
    var $menu = this.element.find('.dropdown-menu');
    $(options).each(function() {
      var newElement = $(this.element);
      newElement.click(this.onClick);
      $menu.append(newElement);
    });
  };


  $.fn.capsule = function(options) {
    this.each(function() {
      new Capsule($(this), options);
    });
    return this;
  };

}( jQuery ));
;/*!
 * .closestDescendant( selector [, findAll ] )
 * https://github.com/tlindig/jquery-closest-descendant
 *
 * v0.1.2 - 2014-02-17
 *
 * Copyright (c) 2014 Tobias Lindig
 * http://tlindig.de/
 *
 * License: MIT
 *
 * Author: Tobias Lindig <dev@tlindig.de>
 */
(function($) {

    /**
     * Get the first element(s) that matches the selector by traversing down
     * through descendants in the DOM tree level by level. It use a breadth
     * first search (BFS), that mean it will stop search and not going deeper in
     * the current subtree if the first matching descendant was found.
     *
     * @param  {selectors} selector -required- a jQuery selector
     * @param  {boolean} findAll -optional- default is false, if true, every
     *                           subtree will be visited until first match
     * @return {jQuery} matched element(s)
     */
    $.fn.closestDescendant = function(selector, findAll) {

        if (!selector || selector === '') {
            return $();
        }

        findAll = findAll ? true : false;

        var resultSet = $();

        this.each(function() {

            var $this = $(this);

            // breadth first search for every matched node,
            // go deeper, until a child was found in the current subtree or the leave was reached.
            var queue = [];
            queue.push($this);
            while (queue.length > 0) {
                var node = queue.shift();
                var children = node.children();
                for (var i = 0; i < children.length; ++i) {
                    var $child = $(children[i]);
                    if ($child.is(selector)) {
                        resultSet.push($child[0]); //well, we found one
                        if (!findAll) {
                            return false; //stop processing
                        }
                    } else {
                        queue.push($child); //go deeper
                    }
                }
            }
        });

        return resultSet;
    };
})(jQuery);
;require.define({"admin/flash_to_growl":function(exports, require, module){
/* This module will convert flash messages that are in the DOM on a page and turn them into growl notifications.
 * processMessages expects the element on the page that contains flash element that has a class with the type
 * of notification and a message.
 * For example:
 * <div class="flash" style="display:none;">
      <div class="notice">message.sanitize</div>
    </div>
 * would be converted with:
 * var flashToGrowl = require('admin/flash_to_growl');
 * processMessages(".flash > div");
 */
var lotusClient = require('admin/lotus_client');

function processMessages(selector) {
  $(selector).each(function() {
    lotusClient.growl($(this).html(), $(this).attr('class'));
  });
}

module.exports = Object.freeze({
  processMessages: processMessages
});

;}});
;require.define({"admin/lotus_client":function(exports, require, module){
var PromiseKlass = window.Promise;

var mutex = false;

function origin(location) {
  return location.origin || location.protocol + "//" + location.hostname + (location.port ? ':' + location.port : '');
}

function sendXFrameMessage(msg) {
  window.top.postMessage(JSON.stringify(msg), origin(window.location));
}

function modalXFrameMessage(name, memo) {
  if (mutex) {
    throw new Error("Cannot generate promise for multiple modal XFrame messages");
  }

  mutex = true;

  sendXFrameMessage({
    target: name,
    source: "admin-iframe",
    memo: memo
  });

  return getPromise(name);
}

function isValidOrigin(origin) {
  var loc = window.location;
  var expectedOrigin = loc.protocol + '//' + loc.host;
  return origin === expectedOrigin;
}

function xFrameResponseHandler(name, resolve, reject, event) {
  if(!isValidOrigin(event.origin) || !event.data) {
    return;
  }

  var data = null;
  try {
    data = JSON.parse(event.data);
  } catch(e) {
    return;
  }

  if(data.target !== name ||
     data.source === "admin-iframe") {
    return;
  }

  if(data.confirmed) {
    resolve(data);
  } else {
    reject(data);
  }

  mutex = false;
}

function getPromise(name) {
  var handler;

  return new PromiseKlass(function(resolve, reject) {
    handler = xFrameResponseHandler.bind(null, name, resolve, reject);
    window.addEventListener('message', handler, false);
  }).then(function(data) {
    window.removeEventListener('message', handler, false);
    return data;
  });
}

function growl(message, action, options) {
  sendXFrameMessage({
    target: 'growl',
    source: 'admin-iframe',
    memo: {
      action:  action,
      message: message,
      options: options
    }
  });
}

function transitionTo(link) {
  sendXFrameMessage({
    target: 'route',
    source: 'admin-iframe',
    memo: {
      hash: link
    }
  });
}

function closeModal(confirmed, data) {
  sendXFrameMessage({
    target: 'modal-close',
    source: 'admin-iframe',
    memo: data,
    confirmed: !!confirmed
  });
}

function openModal(type, options) {
  if (type === 'iframe' && !options.src) {
    throw new Error("Iframe modal requires a 'src' to be defined");
  }

  return modalXFrameMessage('modal-open', { type: type, options: options });
}

function showConfirmationModal(options) {
  return modalXFrameMessage('confirmation-modal', {
    title: options.title,
    message: options.message,
    confirmLabel: options.confirmLabel,
    cancelLabel: options.cancelLabel
  });
}

function showHelpCenter(options) {
  return modalXFrameMessage('show_help_center', {
    brandId: options.brandId
  });
}

function showHelpCenterWithBrand(options) {
  return modalXFrameMessage('show_help_center_for_brand', {
    brandId: options.brandId
  });
}

function reactPostMessage(type, options) {
  return modalXFrameMessage('lotusReact', { type: type, options: options });
}

module.exports = {
  configure: function(options) {
    if(options.Promise) {
      PromiseKlass = options.Promise;
    }
  },
  reactPostMessage: reactPostMessage,
  showHelpCenter: showHelpCenter,
  showHelpCenterWithBrand: showHelpCenterWithBrand,
  showConfirmationModal: showConfirmationModal,
  growl: growl,
  transitionTo: transitionTo,
  openModal: openModal,
  closeModal: closeModal
};

;}});
;require.define({"admin/onboarding":function(exports, require, module){
var lotusClient = require('admin/lotus_client');

var OnboardingMixin = {

  getSteps: function() {
    return $('.onboarding-steps .step');
  },

  closeModal: function(confirmed, data) {
    lotusClient.closeModal(confirmed, data);
  },

  changeStep: function(direction) {
    var steps = this.getSteps();
    var currentStep = steps.filter('.current');
    var newStep;
    steps.removeClass('current');

    if (direction === 'next') {
      newStep = currentStep.next();
    } else if (direction === 'prev') {
      newStep = currentStep.prev();
    }

    newStep.addClass('current');
    this.updateStepper();
  },

  nextStep: function() {
    this.changeStep('next');
  },

  previousStep: function() {
    this.changeStep('prev');
  },

  updateStepper: function() {
    //if stepper div
    //update stepper styles to select current step marker
  }
};

module.exports = OnboardingMixin;

;}});
;(function($) {
  function Tabs(container, options) {
    options = $.extend({}, options);

    this.container         = $(container);
    this.linksContainer    = container.closestDescendant('.tab-links');
    this.links             = $('a', this.linksContainer);
    this.contentContainersContainer = container.closestDescendant('.tabs-content');
    this.contentContainers = $(' > div', this.contentContainersContainer);
    this.sharedContent     = $(this.contentContainersContainer).siblings();
    var self = this;

    this.links.click(function(e) {
      self.selectTab($(this));
      e.preventDefault();
    });
  }

  Tabs.prototype.selectTab = function(link) {
    var otherLinks = this.links.not(link);
    var tabID = link.attr('href');
    var newlySelectedTab = this.contentContainers.filter(tabID);
    var otherTabs = this.contentContainers.not(newlySelectedTab);

    otherLinks.removeClass('current');
    otherTabs.hide();
    link.addClass('current');
    newlySelectedTab.trigger("tab.show");
    newlySelectedTab.show();
  };

  $.fn.tabs = function(options) {
    this.each(function() {
      new Tabs($(this), options);
    });
    return this;
  };
}(jQuery));
;require.define({"admin/utils":function(exports, require, module){
module.exports = {
  mixin: function() {
    var args = Array.prototype.slice.call(arguments),
        obj = args.shift();

    args.forEach(function(props) {
      Object.keys(props).forEach(function(prop) {
        obj[prop] = props[prop];
      }, this);
    }, this);
  },

  ie9: function() {
    return $('html.ie9').length > 0;
  }
};

;}});
;require.define({"admin/validation":function(exports, require, module){
module.exports = {
  clear: function() {
    this.element.closest('.validation').removeClass('success warning error error-silent');
    this.element.closest('.validation').find('.validation-message').remove();
    this.recountErrors();
  },

  success: function(options) {
    this.clear();
    this.element.closest('.validation').addClass('success');
    this.validationMessage(options.message);
  },

  warning: function(options) {
    this.clear();
    this.element.closest('.validation').addClass('warning');
    this.validationMessage(options.message);
    this.recountErrors();
  },

  error: function(options) {
    this.clear();
    this.element.closest('.validation').addClass('error');
    this.validationMessage(options.message);
    this.recountErrors();
  },

  silentError: function(options) {
    this.clear();
    this.element.closest('.validation').addClass('error-silent');
    this.recountErrors();
  },

  validationMessage: function (message) {
    $("<div class='validation-message'></div>").text(message).appendTo(this.element.closest('.validation'));
  },

  disableSubmit: function() {
    this.element.closest('form').find('button[type="submit"]').attr('disabled', 'disabled');
  },

  enableSubmit: function() {
    this.element.closest('form').find('button[type="submit"]').removeAttr('disabled');
  },

  recountErrors: function() {
    var form = this.element.closest('form');

    if (form.find('.validation.error, .validation.error-silent').length > 0) {
      this.disableSubmit();
    } else {
      this.enableSubmit();
    }
  }
};

;}});
