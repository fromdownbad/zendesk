/*globals jQuery, I18n, currentAccount*/

(function($) {
  function isAutomaticAnswersPlaceholder(key) {
    var placeholders = [
      'placeholder.answer_bot.article_list',
      'placeholder.answer_bot.article_count',
      'placeholder.answer_bot.first_article_body'
    ];

    return _.contains(placeholders, key);
  }

  function isExplicitlyExcludedPlaceholder(key) {
    var placeholders = [
      'placeholder.ticket.automatic_answers',
      'placeholder.automatic_answers.article_list',
      'placeholder.automatic_answers.article_count',
      'placeholder.automatic_answers.first_article_body'
    ];

    return _.contains(placeholders, key);
  }

  function LiquidPlaceholders(placeholderPrefix) {
    var placeholders = {},
      keys,
      key,
      regex,
      prefix,
      substringLength;

    prefix = placeholderPrefix || 'placeholder.';

    regex = RegExp('^' + prefix);
    substringLength = prefix.length;

    // Get all the keys whose prefixes begin with `prefix`
    keys = $.grep(Object.keys(I18n.translations), function(key) {
      return regex.test(key);
    });

    for (var i = 0, l = keys.length; i < l; i++) {
      key = keys[i];

      if (key === 'placeholder.ticket.brand.name' && currentAccount && !currentAccount.hasFeature("multibrand")) {
        continue;
      }

      if (key === 'placeholder.ticket.current_holiday_name' && currentAccount && !currentAccount.hasFeature("businessHours")) {
        continue;
      }

      if (key === 'placeholder.current_user.tags' && currentAccount && !currentAccount.hasFeature("userAndOrganizationTags")) {
        continue;
      }

      if (key === 'placeholder.account.talk_incoming_phone_number_id') {
        continue;
      }

      if (key === 'placeholder.account.incoming_phone_number_ID' && currentAccount ) {
        placeholders['{{' + key.substring(substringLength) + '}}'] = I18n.translations['placeholder.account.talk_incoming_phone_number_id'];
        continue;
      }

      if (isAutomaticAnswersPlaceholder(key) && currentAccount && !currentAccount.hasFeature("automaticAnswers")) {
        continue;
      }

      if (isExplicitlyExcludedPlaceholder(key)) {
        continue;
      }

      // the I18n keys all start with "placeholder." which has a length of 12
      placeholders['{{' + key.substring(substringLength) + '}}'] = I18n.translations[key];
    }

    return placeholders;
  }

  window.LiquidPlaceholders = LiquidPlaceholders;
})(jQuery);
