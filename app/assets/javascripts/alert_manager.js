(function(Cookie, $, Zendesk) {

  Zendesk.AlertManager = function(selector, key) {
    this._selector = '' + selector;
    this._key = '' + key;

    $(selector + ' a.close').click(this.hide.bind(this));
  };

  Zendesk.AlertManager.prototype = {
    /* Show the alert if the user doesn't have the cookie saying
     * to ignore it.
     */
    show: function() {
      if(Zendesk.SettingsCookie.get(this._key) == null) {
        var element = $(this._selector);
        var alertId = element.data('ipm-alert-id');

        element.show();

        if (alertId) {
          Zendesk.Instrumentation.track(alertId, 'ipm-alert:load');
          element.find('a.link').click(function () { // learn more link
            Zendesk.Instrumentation.track(alertId, 'ipm-alert:link');
          });
        }
      }
    },

    /*
     * Hide the alert and set a cookie to keep it from reappearing.
     * @param [Event] event the event that triggered the hide; optional
     * @return [false] false
     */
    hide: function(event) {
      event && event.preventDefault && event.preventDefault();
      var alertId = $(this._selector).hide().data('ipm-alert-id');
      if (alertId) {
        $.ajax({
          type: "delete",
          url:  "/api/v1/ipm/alerts/" + alertId + ".json"
        });
        Zendesk.Instrumentation.track(alertId, 'ipm-alert:dismiss');
      } else {
        Zendesk.SettingsCookie.set(this._key, '1');
      }
      return false;
    }
  };

}(this.Cookie, this.jQuery, this.Zendesk));
