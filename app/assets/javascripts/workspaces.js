/*globals ZendeskMenus,__workspaceConfig,NestedHashConverter,macrosMenu,appsMenu,Set,selectedAppIds*/
window.macrosMenu = new ZendeskMenus.TagMenu({
  domHolder: '.workspace_macros',
  useUniqueValues: true,
  ignoreCase: true,
  data: __workspaceConfig.selectedMacros.map(function(macro){ return { "label": macro.label.split("::").pop(), "value": macro.value }; }),
  comboSelectEditorOptions: {
    data: __workspaceConfig.macros.reduce(function(converter, macro) {converter.add(macro.label, macro.value); return converter;}, new NestedHashConverter()).toList(),
    hasCheckboxes: true,
    keepOpen: true,
    searchZeroStatePlaceholder: 'Search',
    searchPlaceholder: 'Enter keywords to search'
  }
});

window.ticketFormControl = new ZendeskMenus.SelectMenu({
  data: __workspaceConfig.ticketForms,
  domHolderSelector: '.workspace_ticket_form',
  proxyName: 'workspace[ticket_form_id]',
  value: __workspaceConfig.selectedTicketFormId
});

macrosMenu.on('tagsChanged', function(e, data) {
  var macrosDomEl = document.getElementById("workspace_macros");
  var listOfMacros = macrosMenu.getValues().map(function(el) { return el.value; });
  if (listOfMacros.length === 0) {
    macrosDomEl.value = "";
  } else {
    macrosDomEl.value = listOfMacros;
  }
});

window.appsMenu = new ZendeskMenus.TagMenu({
  domHolder: '.workspace_apps',
  useUniqueValues: true,
  ignoreCase: true,
  data: __workspaceConfig.selectedApps,
  comboSelectEditorOptions: {
    data: [{label: 'Apps are being loaded...'}],
    hasCheckboxes: true,
    keepOpen: true,
    searchZeroStatePlaceholder: 'Search',
    searchPlaceholder: 'Enter keywords to search'
  }
});

appsMenu.on('tagsChanged', function(e, data) {
  var appsDomEl = document.getElementById("workspace_apps");
  appsDomEl.value = appsMenu.getValues().map(function(el) { return el.value; }).join(",");
});

function getTicketSidebarAppsIds(locations) {
  var appIds = [];

  var appIdSet = new Set(locations.reduce(function(ids, location) {
    var name = location.location_name;
    if (name === "ticket_sidebar" || name === "new_ticket_sidebar") {
      return ids.concat(location.installations);
    } else {
      return ids;
    }
  }, []));

  appIdSet.forEach(function(v) { return appIds.push(v); });
  return appIds;
}

function isEnabledAppInSidebar (locations) {
  return function(app) {
    var ticketSidebarAppIds = getTicketSidebarAppsIds(locations);
    var isTicketSidebarApp = ticketSidebarAppIds.indexOf(app.id) > -1;
    return isTicketSidebarApp && app.enabled;
  };
}

function toZendeskMenuItem(app) {
  return { label: app.settings.name, value: app.id };
}

function populateAppsDropdown() {
  var locations = (JSON.parse(locationsReq.responseText)).location_installations;
  var installs = (JSON.parse(installsReq.responseText)).installations;
  var isValidApp = isEnabledAppInSidebar(locations);

  if (window.selectedAppIds) {
    var selectedApps = installs.filter(function(app) {
      return isValidApp(app) && selectedAppIds.indexOf(app.id) > -1;
    }).map(toZendeskMenuItem);
    appsMenu.loadData(selectedApps);
  }

  var apps = installs.filter(isValidApp).map(toZendeskMenuItem);
  appsMenu.editor.loadData(apps);
}

function bothRequestsFinished() {
  var bothFinished = (installsReq.readyState === 4 && locationsReq.readyState === 4);
  if (bothFinished) {
    populateAppsDropdown();
  }
}

var installsReq = new XMLHttpRequest();
installsReq.open("GET", "/api/v2/apps/installations.json");
installsReq.onreadystatechange = bothRequestsFinished;
installsReq.send();

var locationsReq = new XMLHttpRequest();
locationsReq.onreadystatechange = bothRequestsFinished;
locationsReq.open("GET", "/api/v2/apps/location_installations.json");
locationsReq.send();