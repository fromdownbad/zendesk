/*globals currentAccount, currentUser, Zendesk*/

(function($) {

  /* What does this do?
  /
  /  This redirects classic links clicked inside an iframe to the appropriate Lotus links.
  /
  */

  Zendesk.embeddedInLotusAdmin = function() {
    return window.name == 'zendesk-clean-admin';
  };

  Zendesk.embeddedInLotusLaunchpad = function() {
    return window.name == 'lotus-launchpad';
  };

  var document        = this.document,
      embeddedInLotus = Zendesk.embeddedInLotusAdmin();

  // a map of regular expressions for classic HREFs to the corresponding
  // Lotus versions.
  var lotusHrefs = {
    '^/rules/(\\d+)$':                          '#/filters/$1',
    '^/tickets/(\\d+)':                         '#/tickets/$1',
    '^/users/new':                              '#/users/new',
    // For users and organizations, go directly to /tickets to bypass problematic
    // replaceWith redirect that replaces existing history entry (USERV-422)
    '^/users/(\\d+)$':                          '#/users/$1/tickets',
    '^/users/(\\d+)/edit':                      '#/users/$1/tickets',
    '^/organizations/(\\d+)(/edit)?':           '#/organizations/$1/tickets',
    '^/organizations/new':                      '#/organizations/new',
    '^/settings/account#subscription/lotus':    '#/admin/subscription',
    '^/agent/admin/business_hours':             '#/admin/business_hours',
    '^/agent/admin/widget':                     '#/admin/widget',
    '^/agent/admin/brands':                     '#/admin/brands',
    '^/agent/admin/automations':                '#/admin/automations',
    '^/agent/admin/macros':                     '#/admin/macros',
    '^/agent/admin/triggers':                   '#/admin/triggers',
    '^/agent/admin/views':                      '#/admin/views'
  };

  var rewrittenLotusHrefs = [
    /#\/filters\/\d+/,
    /#\/tickets\/\d+/,
    /#\/users\/new/,
    /#\/users\/\d+/,
    /#\/organizations\/\d+/,
    /#\/organizations\/new/,
    /#\/admin\/subscription/,
    /#\/admin\/business_hours/,
    /#\/admin\/widget/,
    /#\/admin\/brands/,
    /#\/admin\/automations/,
    /#\/admin\/macros/,
    /#\/admin\/triggers/,
    /#\/admin\/views/
  ];

  var forumHref = new RegExp('^/entries/');
  var chatHref = new RegExp('/chat');

  function lotusHref(href) {
    var regex;
    for (var r in lotusHrefs) {
      if (lotusHrefs.hasOwnProperty(r)) {
        regex = new RegExp(r);
        if (regex.test(href)) {
          return href.replace(regex, lotusHrefs[r]);
        }
      }
    }

    for (var i = 0; i < rewrittenLotusHrefs.length; i++) {
      if (rewrittenLotusHrefs[i].test(href)) {
        var split = href.split('#');
        return '#' + split[split.length - 1];
      }
    }

    return null;
  }

  function appendLotusQueryParams(elem) {
    var href = elem.attr("href");
    var param = "lotus=true";
    var join_with;

    if(href.indexOf(param) > -1) return;

    if(href.indexOf("?") > -1 ) {
      join_with = "&";
    } else {
      join_with = "?";
    }

    elem.attr("href", href + join_with + param);
    elem.prop("href", href + join_with + param);
  }

  // To support command-click/ctrl-click and right-click, we need to rewrite
  // lotus links on mouse down
  function rewriteHrefOnMouseDown(evt) {
    var $a = $(this),
        lotusLink = lotusHref($a.attr('href')),
        forumLink = forumHref.test($a.attr('href')),
        chatLink = chatHref.test($a.attr('href'));

    // if the link looks like a Lotus link
    if (lotusLink) {
      // and we're embedded in an iframe inside Lotus
      if (embeddedInLotus) {
        // update the href
        rewriteLinkToLotus($a, '_top');
      // and we're not inside Lotus, but still want to open a link in Lotus (e.g. profile link from a forum topic)
      } else if (currentUser.isAgent && currentAccount.doAgentsPreferLotus) {
        // update the href and set target to _blank
        rewriteLinkToLotus($a, '_blank');
      }
    } else if ( embeddedInLotus && chatLink ) {
      appendLotusQueryParams($a);
    } else if ( embeddedInLotus && forumLink ) {
      $a.attr('target', '_blank');
    }
  }

  // For links in classic that have a parallel in lotus, clicking them in
  // an embedded classic should instead tell the lotus (parent) frame about
  // the link.
  function handlePossibleLotusClick(evt) {
    if (evt.metaKey || evt.ctrlKey) {
      return;
    }

    var $a = $(this),
        lotusLink = lotusHref($a.attr('href')),
        forumLink = forumHref.test($a.attr('href')),
        chatLink = chatHref.test($a.attr('href'));

    // if the link looks like a Lotus link
    if (lotusLink) {
      // and we're embedded in an iframe inside Lotus
      if (embeddedInLotus) {
        // this function will trigger the change via the Lotus router
        transitionToLotus($a, evt);
      // and we're not inside Lotus, but still want to open a link in Lotus (e.g. profile link from a forum topic)
      } else if (currentUser.isAgent && currentAccount.doAgentsPreferLotus) {
        // open the Lotus link in a new window
        rewriteLinkToLotus($a, '_blank');
      }
    } else if ( embeddedInLotus && chatLink ) {
      appendLotusQueryParams($a);
    } else if ( embeddedInLotus && forumLink ) {
      $a.attr('target', '_blank');
    }
  }

  function origin(location) {
    return location.origin || location.protocol + "//" + location.hostname + (location.port ? ':' + location.port : '');
  }

  function transitionToLotus(element, event) {
    var $element = $(element),
        split = lotusHref($element.attr('href')).split('#'),
        href = split[split.length - 1];

    event.preventDefault();

    var data = JSON.stringify({
      "target": "route",
      "memo": {hash: href}
    });
    window.parent.postMessage(data, origin(location));
  }

  function rewriteLinkToLotus(elem, target) {
    var $elem      = $(elem),
        envIsDev   = (currentAccount.domain == 'localhost') ? true : false,
        topDomain  = (envIsDev) ? 'localhost:3000' : currentAccount.domain,
        protocol   = (envIsDev) ? 'http://' : 'https://',
        accountUrl = protocol + currentAccount.subdomain + '.' + topDomain + '/agent/',
        lotus      = accountUrl + lotusHref($elem.attr('href'));

    $elem.attr('target', target)
      .attr('href', lotus)
      .prop('href', lotus); // because we're already in the click event,
                            // we can't just set the HTML attr,
                            // we also have to set the actual DOM object
                            // property, which is what the
                            // browser will use to determine where to go.
  }

  $(function() {
    // TODO: When we're going to jQuery 1.7+, .on() is preferred to .delegate().
    $(document).delegate('a', 'click', handlePossibleLotusClick);

    $(document).delegate('a', 'mouseover', rewriteHrefOnMouseDown);

    if(Zendesk.embeddedInLotusAdmin()) {
      $("body").addClass("embedded_in_lotus");
    }
  });

}(this.jQuery));
