/*global Zendesk, $j, Screenr, I18n */
Zendesk.NS('Screenr');

Zendesk.Screenr.Helper = {
  // Public: Interpolate the url to Screenr's assets on their servers.
  //
  // Returns a String containing a URL.
  urlToScreenrLibray: function(domainUrl){
    return (Zendesk.Screenr.Helper._host_with_protocol(domainUrl) + '/api/recorder');
  },

  // Internal: Interpolate url to Screenrs domain with proper protocol (http or https)
  //
  // Example
  //   Given a normal http connection
  //   _host_with_protocol('https://test123.screencast.com')
  //   # => 'http://test123.screencast.com'
  //
  // Returns a String.
  _host_with_protocol: function(domainUrl){
    var hostname = domainUrl.replace(/.*?:\/\//g, ""),
        protocol = (("https:" == Zendesk.Screenr.Helper._protocol()) ? "https://" : "http://");

    return protocol + hostname;
  },

  // Internal: Detect the used protocol.
  //
  // Returns a String.
  _protocol: function(){
    // This is here mainly to test/spec purposes
    return document.location.protocol;
  },

  // Internal: Interpolate Tenant domain.
  //
  // Returns a String.
  _tenant_domain: function(domainUrl){
    return domainUrl.match(/([^.]+)/g)[0];
  },

  // Public: Interpolate Tenant Status URL.
  //
  // Returns a String containing a URL.
  publicUrlToTenantStatus: function(recorderId, domainUrl, screenrBaseUri){
    return this._host_with_protocol(screenrBaseUri) +
           "/tenants/"                              +
           this._tenant_domain(domainUrl)           +
           "/recorders/"                            +
           recorderId                               +
           "?callback=?";
  }
};