/*global Zendesk, jQuery, Screenr, I18n */
(function($) {
  Zendesk.NS('Screenr');

  Zendesk.Screenr.BaseRecorder = {
    recorder: null,
    screencasts: null,
    counter: 0,
    domainUrl: null,
    removeI18n: "",
    screencastI18nName: "",

    // Geometry Constants
    SCREENCAST_IFRAME_WIDTH: 570,
    SCREENCAST_IFRAME_HEIGHT: 340,
    COLORBOX_WIDTH: 620,
    COLORBOX_HEIGHT: 425,
    COMMENT_HEIGHT: 115,

    // Public: Initialize Screenr recorder.
    // It takes care of I18n setup and fires an afterInit function.
    //
    // recorderId - The String containing the id of the Screenr recorder to be used.
    // domainUrl  - The String of the Screenr domain provided to this Zendesk account.
    // screencastName - The String containing the word defining a screencast E.g. Screencast
    // removeText - The String containing the Label used in the remove recorded Screencast link. E.g. remove
    //
    // A recorder is unique container of screencasts recorded by a Tenant.
    // The domainUrl is needed since we are loading Screenr assets from there.
    //
    // Examples
    //   <script>
    //     Zendesk.Screenr.TicketRecorder.init('33bf8916f0c04268a1d23b22c3b2ee6c',
    //                                         'support46.sfssdev.com',
    //                                         'Screencast',
    //                                         'remove');
    //   </script>
    //
    init: function(recorderId, domainUrl, screencastName, removeText) {
      this.domainUrl          = domainUrl;
      this.screencasts        = [];
      this.screencastI18nName = screencastName;
      this.removeI18n         = removeText;

      this._retrieveScreencastsPreviouslyRecorded();

      // Load the Screen library
      $.getScript(Zendesk.Screenr.Helper.urlToScreenrLibray(domainUrl))
      .done(this._onScreenrLoadedFunction(recorderId));

      // IE9 treats href='javascript:void' in a different way then other browswer.
      // It signals that the document is about to be unloaded even if it's not.
      // The following code prevent this issue.
      $("a[href='javascript:void']").click(function(event) {
        event.preventDefault();
        event.returnValue = false;
      });

      this.afterInit();
    },

    // Internal: In case of a failed validation it renders previously recorded Screencasts
    //
    // Returns nothing.
    _retrieveScreencastsPreviouslyRecorded: function(){
      var self                = this,
          recordedScreencasts = Zendesk.Screenr.RecordedScreencastsOnFailedValidation;

      if(recordedScreencasts != null) {
        // We iterate on the array Rails has populated before init.
        $.each(recordedScreencasts, function(index, s) {
          // For each element we simulate _onRecordingCompleted
          self._onRecordingCompleted({'id':s.id, 'position':s.position, 'embedUrl':s.url, 'thumbnailUrl':s.thumbnail});
        });
      };
    },

    // Internal: Interpolate the Function to be called once Screenr external script is loaded.
    //
    // recorderId - The String containing the id of the Screenr recorder to be used.
    //
    // Returns a Fuction (a curried for the $j.getScript.done above).
    _onScreenrLoadedFunction: function(recorderId){
      var self = this;
      return function(script, textStatus) {
        self.setUpScreenr(recorderId);
      };
    },

    // Internal: Setup the Screenr recorder, attach the recorder to #screenr_link,
    // and set which callback to be called once the recording is completed.
    //
    // recorderId - The String containing the id of the Screenr recorder to be used.
    //
    // Returns nothing.
    setUpScreenr: function(recorderId){
      // TODO: Remove this quick debuging code
      // var self = this;
      // $('#screenr_link').click( function() {
      //   self._onRecordingCompleted({ 'id': Date.now() });
      // });
      var recorder,
          base_params = {id: recorderId, hideAllFields: true, scrollToTop: true};

      recorder = Screenr.Recorder($.extend(base_params, this._extraParamsForScreenr()));
      recorder.attachTo("screenr_link");

      recorder.setUserName("", false); //sets and hides field
      recorder.setUserEmail("", false);
      recorder.setSubject("", false);
      recorder.setDescription("", false);

      recorder.setOnCancel(function(screencast) {
        // TODO: handle this one with proper UI if needed.
        // alert("You cancelled the recording");
      });

      recorder.setOnComplete(function(screencast) {
        this._onRecordingCompleted(screencast);
      }.bind(this));

      this.recorder = recorder;
    },

    // Internal: Callback used by Screen's screencast recorder.
    // builds up the UI styled button and populate the screencasts array.
    // Eventually add the Screencast button to #screencasts_list.
    //
    // screenrScreencast - a Screenr's screencast object.
    //
    // Returns nothing.
    _onRecordingCompleted: function(screenrScreencast) {
      var id,
          listItem,
          screencast;

      this.counter++;
      // Extract only the needed info from screenrScreencast and create a screencast hash.
      screencast = {'id'        : screenrScreencast.id,
                    'url'       : screenrScreencast.embedUrl,
                    'thumbnail' : screenrScreencast.thumbnailUrl,
                    'position'  : this.counter};
      // Update the screencasts array.
      this.screencasts.push(screencast);

      listItem = this._UIstyledListItemButton(screencast, null, true);
      listItem.appendTo($('#screencasts_list'));

      // If effects are available we use them
      try {
        listItem.effect("highlight", {}, 800);
      } catch(err) {}

    },

    // Internal: Create a list item (<li>) that rappresent the UI styled button and
    // attach to it a click event that will open the modal preview window.
    // Note: In list of already recorded screencasts we set with_remove to false.
    //
    // screencast - an Hash rapresenting a Screencast.
    // comment - a jQuery node containing the Ticket comment.
    // withRemove - a Boolean describing if we want the remove link (default: false)
    //
    // Returns jQuery node (<li>).
    _UIstyledListItemButton: function(screencast, comment, withRemove){
      var id = screencast.id,
          listItem = $('<li/>'),
          link = $('<a href="javascript:void" class="screencast_preview"/>'),
          removeLink = $('<a href="javascript:void" class="remove"/>');

      withRemove = withRemove || false;

      listItem.attr('id', 'screencast-' + id);
      listItem.append(link);

      link
        .text(this.screencastI18nName + screencast.position)
        .click(function(evt) {
          this.showScreencast(screencast.url, comment);
        }.bind(this));

      if (withRemove) {
        removeLink
          .html(this.removeI18n)
          .click(function(evt) {
            this.removeScreencast(id);
          }.bind(this));
        listItem.append(removeLink);
      }
      return listItem;
    },

    // Internal: Remove a Screencast from the list associated to a comment.
    // Keeps the internal array of Screencasts and DOM up to date.
    // (we remove Screencast only before a ticket has been created)
    //
    // id - an unique identifier rapresenting a screencast.
    //
    // Returns nothing.
    removeScreencast: function(id){
      var filtered_array = [];

      // First we keep the local array up to date
      $.each(this.screencasts, function(index, screencast) {
        if(screencast.id != id) filtered_array.push(screencast);
      });
      this.screencasts = filtered_array;


      // Then we remove from DOM
      $("#screencast-" + id).remove();
    },

    // Internal: Inject (append after an element) an hidden field containing a list of screencasts as a string.
    //
    // element - a jQuery node containing an element to append the hidden field(s) to.
    //
    // Example:
    //   <input name="ticket[screencasts][][position]" value="5" type="hidden"/>
    //   <input name="ticket[screencasts][][id]" value="test" type="hidden"/>
    //   <input name="ticket[screencasts][][url]" value="http://test.screenr.com/embed/abc1234" type="hidden"/>
    //   <input name="ticket[screencasts][][thumbnail]" value="http://test.screenr.com/abc1234.jpg" type="hidden"/>
    //
    // Returns nothing.
    _injectScreencasts: function(element){
      var name_prefix         = 'ticket[screencasts][]',
          attributes_to_send  = Array('position', 'id', 'url', 'thumbnail');

      $.each(this.screencasts, function(index, screencast) {
        $.each(attributes_to_send, function(index, attribute) {
          $('<input type="hidden" />')
            .attr('name', name_prefix + '[' + attribute + ']')
            .attr('value', screencast[attribute])
            .appendTo(element);
        });
      });
    },

    // Internal: Interpolate base domain.
    //
    // Example
    //   this.domainUrl = 'https://test123.screencast.com';
    //   _screenrBaseDomain();
    //   # => 'screencast.com'
    //
    // Returns a String
    _screenrBaseDomain: function(){
      return (this.domainUrl.replace(/^[^.]+./g, ""));
    },

    // Internal: Define Screenr extra init parameters.
    //
    // The main idea behind this function is to make it possible to redefine it
    // in objects that 'extend' the BaseRecorder.
    //
    // Returns an hash of parameters
    _extraParamsForScreenr: function(){
      return {};
    },

    afterInit: function(){ /* To be overridden */ },
    showScreencast: function(url, comment){ /* To be overridden */ }
  };
}(jQuery));