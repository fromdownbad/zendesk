/*global Zendesk, jQuery, Screenr, I18n */
(function($) {
  Zendesk.NS('Screenr');

  Zendesk.Screenr.TicketRecorder = $.extend({}, Zendesk.Screenr.BaseRecorder, {
    afterInit: function(){
      // Hook into submitTicketForm (logged in users)
      var originalSubmitTicketForm = submitTicketForm;
      submitTicketForm = function() {
        Zendesk.Screenr.TicketRecorder._injectScreencasts($('#ticket-chat'));
        originalSubmitTicketForm();
      };

      // Hook into submitRequestForm (anonymous requests)
      var originalSubmitRequestForm = submitRequestForm;
      submitRequestForm = function() {
        Zendesk.Screenr.TicketRecorder._injectScreencasts($('#ticketform'));
        originalSubmitRequestForm();
      };
    },

    // Internal: Display a dialog window showing the Screencast (as a video) and
    // the body of the comment if available (only for already saved tickets)
    //
    // id - an unique identifier rapresenting a screencast.
    // comment - a jQuery node containing the Ticket comment.
    //
    // Returns nothing.
    showScreencast: function(url) {
      var iframe  = $('<iframe frameborder="0"/>'),
          preview = $('<div id="screencast_preview" />'),
          wrapper = $('<div id="screencast_preview_wrapper" style="display:none"/>'),
          comment_wrapper = $('<div id="comment_wrapper" />'),
          video_wrapper = $('<div id="video_wrapper" />'),
          comment = $('#comment_for_screencasts_list').clone().show(),
          colorbox_height;

      iframe.attr('src', url);
      iframe.attr('width', this.SCREENCAST_IFRAME_WIDTH);
      iframe.attr('height', this.SCREENCAST_IFRAME_HEIGHT);
      // Style is needed by some browsers (ex: Firefox).
      iframe.attr('style', "width:" + this.SCREENCAST_IFRAME_WIDTH + "px;" +
                           "height:" + this.SCREENCAST_IFRAME_HEIGHT + "px;");
      iframe.appendTo(video_wrapper);
      video_wrapper.appendTo(preview);

      if (comment == null) {
        colorbox_height = this.COLORBOX_HEIGHT;
      } else {
        colorbox_height = this.COLORBOX_HEIGHT + this.COMMENT_HEIGHT;
        comment.appendTo(comment_wrapper);
        comment_wrapper.appendTo(preview);
      }

      preview.appendTo(wrapper);
      wrapper.appendTo($('body'));

      $.colorbox({
        inline: true,
        href: '#screencast_preview',
        width:  this.COLORBOX_WIDTH+'px',
        height: colorbox_height+'px',
        onClosed: function() { $("#screencast_preview_wrapper").remove(); }
      });
    }

  });
}(jQuery));
