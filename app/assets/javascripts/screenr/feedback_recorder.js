/*global Zendesk, jQuery, Screenr, I18n */
(function($) {
  Zendesk.NS('Screenr');

  Zendesk.Screenr.FeedbackRecorder = $.extend({}, Zendesk.Screenr.BaseRecorder, {
    afterInit: function(){
      $('#dropbox_submit').click(function(event) {
        Zendesk.Screenr.FeedbackRecorder._injectScreencasts($('#screencasts'));
      });
    },

    // Internal: Define Screenr extra init parameters.
    //
    // Returns an hash of parameters
    _extraParamsForScreenr: function(){
      return {isZendeskDropbox: true};
    }
  });
}(jQuery));
