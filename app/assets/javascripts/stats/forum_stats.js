Zendesk.NS("Stats");

Zendesk.Stats.ForumUI = (function () {
  this._statsInfo = function(statsObject) {
    return { statsObject: statsObject,
             objectType: statsObject.attr("object_type"),
             objectId: statsObject.attr("object_id"),
             windowStart: statsObject.attr("window_start"),
             pageType: statsObject.attr("page_type") }
  };

  this.setup = function(app, options) {
    options       = options || {};
    var container = options.container;

    $j("#detailed_stats_graph_container", container).hide();

    this._drawSparklines(options);
    this._bindSparklineHandlers(app, options);
  }

  this._bindSparklineHandlers = function(app, options) {
    options       = options || {};
    var container = options.container,
        statsURL  = (options.statsURL || '#stats') + '/';

    // handle mouse click on sparkline graph
    $j(".sparkline", container).click(function() {
      if ($j(this).hasClass("active")) { return false; }

      app.setLocation(statsURL + $j(this).attr("id"));
    });
  },

  this._drawSparklines = function(options) {
    options         = options || {};
    var container   = options.container,
        statsInfo   = Zendesk.Stats.ForumUI._statsInfo($j("#stats_object", container));

    // a list of the different sparkline graphs to display
    // div: an id in which to draw the canvas on the page
    // statName: which type of stat to look at
    var sparklineGraphs = [
      {div: "#forum_entry_create_sparkline", statName: "entry_create"},
      {div: "#forum_entry_view_sparkline", statName: "entry_view"},
      {div: "#forum_post_create_sparkline", statName: "post_create"},
      {div: "#forum_watching_entry_create_sparkline", statName: "watching_entry_create"},
      {div: "#forum_vote_create_sparkline", statName: "vote_create"}
    ];

    // do not call .draw() unless the feature is enabled (i.e., the element is rendered on the page)
    if ($j("#stats_summary_container", container).exists()) {
      // draw each sparkline graph
      for (var i=0; i < sparklineGraphs.length; i++) {
        // viewing stats at the entry/topic level means "number of entries created for entry" doesn't make sense
        if (statsInfo.pageType == "entry" && sparklineGraphs[i]["statName"] == "entry_create") {
          continue;
        } else {
          var sparkline = new Zendesk.Stats.Graph($j(sparklineGraphs[i]["div"], container), {
            graphType: "sparkline",
            resource: "forum",
            objectType: statsInfo.objectType,
            objectId: statsInfo.objectId,
            statName: sparklineGraphs[i]["statName"],
            start: statsInfo.windowStart,
            totalCountDiv: $j(sparklineGraphs[i]["div"], container).parent().parent().find(".stat_total")
          });
          sparkline.draw();
        }
      }
    }
  },


  this.showDetailGraph = function(sparkline, options) {
    options       = options || {};
    var container = options.container,
        statName  = $j(sparkline, container).attr('id'),
        statsInfo = Zendesk.Stats.ForumUI._statsInfo($j("#stats_object", container));

    $j("#comments_section", container).hide();
    $j("#stats_summary_container", container).show();

    $j(".sparkline.active", container).removeClass("active");
    $j(sparkline, container).addClass("active");

    var graph = new Zendesk.Stats.Graph($j("#detailed_stats_graph", container), {
      graphType: "area",
      resource: "forum",
      objectType: statsInfo.objectType,
      objectId: statsInfo.objectId,
      statName: statName,
      start: statsInfo.windowStart
    });
    graph.draw();

    $j(window).resize(function() {
      graph.draw();
    });

    $j("#detailed_stats_graph_container", container).show();

    // fetches and displays Top-N lists for the different stat types
    // stats on the entry page are different since they do not display top-n lists
    if ( statsInfo.objectType != "entry" ) {
      var data = {start: statsInfo.windowStart, stat_name: statName};
      data[statsInfo.objectType + "_id"] = statsInfo.objectId;

      $j("#content_entries .frame:first", container).html('<div id="topic_search_loading" class="loading"></div>');

      $j.ajax({
        url: '/entries/top_entries_by_stat',
        data: data
      }).done(function(body) {
        $j("#content_entries .frame:first", container).html(body);
      });
    }

    // fetch and display account-wide aggregate average (except at account-level)
    if (statsInfo.objectType != "account") {
      $j.ajax({
        url: "/api/v1/stats/aggregate_average/account/0/forum_stats_by_" + statsInfo.objectType + "/" + statName + "?start=" + statsInfo.windowStart
      }).done(function(total) {
        var formattedTotal = Zendesk.Stats.formatNumber("0,0", total);
        $j("#account_wide_total_section", container).html(I18n.t('txt.admin.public.javascript.stats.forum_stats.vs_for_average_topic', {total_stats: '<span id="account_wide_total">' + formattedTotal + '</span>'}));
      });
    }

    $j("#forum_nav_comments", container).click(function() {
      $j("#detailed_stats_graph_container", container).hide();
    });
  };

  return this;
})();
