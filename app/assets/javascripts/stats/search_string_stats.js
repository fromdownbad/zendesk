/*globals Zendesk, $j, encodeURIComponent*/
Zendesk.NS("Stats");

Zendesk.Stats.SearchStatsOptions = function (options){

  if ((options === undefined) ||
      (options.divTable === undefined) ||
      (options.statsType === undefined)){
    throw "Expected both divTable & statsType options.";
  }

  if ((options.statsType != 'all' &&
       options.statsType != 'feedback_tab' &&
       options.statsType != 'portal_tab')){
    throw "Unknown handle type: " + options.statsType;
  }

  this.divTable             = options.divTable;
  this.statsType            = options.statsType;
  this.orderField           = options.orderField;
  this.fieldSorts           = { searches: true, "string": true, avg_results: false, ctr: false, tickets: true };
  this.appendData           = options.appendData || false;
  this.container            = options.container;
  this.divPaginationTotal   = options.divPaginationTotal;
  this.page                 = options.currentPage || 1;
  this.pageSize             = options.pageSize || 15;
  this.searchStatsTemplate  = options.searchStatsTemplate;

};

Zendesk.Stats.SearchStatsOptions.prototype = {

  getStatsAPIEndpoint: function(pageOptions) {
    var apiEndPoint = '/api/v1/stats/search/account/search_stats';
    var origin = {
      all:          "all",
      feedback_tab: "dropbox",
      portal_tab:   "portal"
    }[this.statsType];

    var page;
    var pageSize;

    if (pageOptions === undefined) {
      page = this.page;
      pageSize = this.pageSize;
    } else {
      page = pageOptions.page || this.page;
      pageSize = pageOptions.pageSize || this.pageSize;
    }

    apiEndPoint += "?origin=" + origin + "&page=" + page + "&per_page=" + pageSize + "&duration=2592000";

    if (this.orderField !== undefined) {
       apiEndPoint += '&order=' + this.orderField;
       apiEndPoint += "&desc=" + this.fieldSorts[this.orderField];
    }
    return apiEndPoint;
  }
};

Zendesk.Stats.SearchStatsTable = function(options) {

  this.statOptions  = new Zendesk.Stats.SearchStatsOptions(options);

};

Zendesk.Stats.SearchStatsTable.prototype = {

  $: function(selector) {
    return $j(selector, this.statOptions.container);
  },

  _saveOptions: function(statOptions) {
    $j.extend(this.statOptions, statOptions);
  },

  displayFirstPage: function(statOptions) {
    this._saveOptions(statOptions);
    this._fetchData(this.statOptions.getStatsAPIEndpoint());
  },

  nextPage: function() {
    this.statOptions.appendData = true;
    return (this.statOptions.page += 1);
  },

  setSort: function(stat, sortDirection) {
    var opts = this.statOptions,
        desc = (sortDirection === 'desc');

    opts.fieldSorts[stat] = desc;
    opts.orderField = stat;
  },

  _getSortDirection: function(sort) {
    return (sort === false ? 'asc' : 'desc');
  },

  getCurrentSortDirection: function(stat) {
    var opts = this.statOptions,
        sort = opts.fieldSorts[stat];

    return this._getSortDirection(sort);
  },

  getNextSortDirection: function(stat) {
    var opts = this.statOptions,
        newSort = opts.fieldSorts[stat];

    if (opts.orderField === stat)
      newSort = !newSort;

    return this._getSortDirection(newSort);
  },

  modifyDisplayed: function(statOptions, table, filterCallback) {
    this._saveOptions(statOptions || {});
    var apiEndPoint;

    if (this.statOptions.appendData === true) {
      // Clicking more, don't need to change sort.
      apiEndPoint = this.statOptions.getStatsAPIEndpoint();
    } else {
      // calculate the currently displayed number of rows so that you know how many rows to show in sorted data.
      // Intentionally not changing the current page so that when user clicks on "More" button we fetch the correct page
      var currentDisplayedRows = this.statOptions.pageSize * this.statOptions.page;
      apiEndPoint = this.statOptions.getStatsAPIEndpoint({ page: 1, pageSize: currentDisplayedRows });
    }

    this._fetchData(apiEndPoint, this.statOptions.orderField, table, filterCallback);
  },

  _fetchData: function(url, stat, table, filterCallback) {
    var self = this;

    $j.ajax({
      url: url,
      dataType: 'json',
      success: function(tableData) {
        // once we have top entry details, display the table
        if (tableData.data.length > 0) {
          self._fetchTopEntries(tableData, stat, table, filterCallback);
        } else {
          self.statOptions.appendData = false;
        }
      }
    });
  },

  _fetchTopEntries: function(entriesInfo, stat, table, filterCallback) {
    var self      = this;
    var tableData = entriesInfo.data;
    var entryIds  = [];
    $j.each(tableData, function(index, value) {
      if (value.top_entry_id !== null)
        entryIds.push(value.top_entry_id);
    });

    var joinedEntryIds     = entryIds.join(",");
    var topEntryDetailsUrl = '/api/v1/entries/show_many/';
    $j.ajax({
      type: 'POST', // POST, because GET has a lower params limit
      url: topEntryDetailsUrl,
      data: { ids: joinedEntryIds, only: 'title'},
      dataType: 'json',
      success: function(topEntriesData) {
        // once we have top entry details, display the table
        self._mergeData(topEntriesData, tableData);

        // display "More" button only if there is more data.
        var moreButton = self.$(self.statOptions.divTable + ' #show_more');
        if (entriesInfo.page < entriesInfo.total_pages) {
          moreButton.show();
        } else {
          moreButton.hide();
        }

        if (filterCallback)
          filterCallback.call(table, stat);

        // When filterCallback is called, some entries can be hidden, adjust total accordingly.
        self._displayTotal(entriesInfo.page, entriesInfo.total_entries);
      }
    });
  },

  _mergeData: function(topEntriesData, tableData){
    /* Merge data1 and data2 to get data3 so that we have complete data to render in table Mustache template
       Note that this merge is needed since Mustache does not seem to allow nested tags like {{ {{top_entry_id}}_title }}
     data1:
     [{"id":123,..other details..,"top_entry_id":52},
      {"id":234,..other details..,"top_entry_id":57},
      {"id":1222345,..other details..,"top_entry_id":52}]

    data2:
     [{"title":"Yo 1","id":52},{"title":"Yo 2","id":57}]

    data3:
     [{"id":123,..other details..,"top_entry_id":52, "title": "Yo 1"},
      {"id":234,..other details..,"top_entry_id":57, "title": "Yo 2"},
      {"id":1222345,..other details..,"top_entry_id":52, "title": "Yo 1"}]
    */

    /*
       For faster merging in O(m+n) of data1 of size m and data2 of size n, a hash is used to keep track of
       table rows for a top_entry_id so that O(1) lookup can be done when updating the title.
    */
    var entryRowMap = {}; // Hash to find tableData rows for a given top_entry_id
    $j.each(tableData, function(index, row) {
      if (entryRowMap[row.top_entry_id] === undefined){
        entryRowMap[row.top_entry_id] = [];
      }
      entryRowMap[row.top_entry_id].push(index);
      // truncate the search string to 26 if its too big
      var search_string = row.string;
      if (search_string !== null && search_string.length >= 26){
        row.string = search_string.substring(0,23) + '...';
      }
      // to be used as a query parameter
      row.escaped_string = encodeURIComponent(search_string);
      row.searches = Zendesk.Stats.formatNumber("0", row.searches);

      row.ctr = row.ctr * 100.0;
      // clamp ctr to 100, stop showing dirty laundry.
      if ( row.ctr > 100 ) {
        row.ctr = 100.0;
      }

      row.ctr = Zendesk.Stats.formatNumber("0.0", row.ctr);
      row.avg_results = Zendesk.Stats.formatNumber("0,0.0", row.avg_results);
      row.tickets = Zendesk.Stats.formatNumber("0", row.tickets);
    });

    // for each top_entry, find the corresponding table rows and update their title
    $j.each(topEntriesData, function(index, topEntryRow) {
      var tableDataIndexes = entryRowMap[topEntryRow.id];
      // truncate the title to 26 if its too big
      var link = topEntryRow.title;
      if (link !== null && link.length >= 26){
        link = link.substring(0,23) + '...';
      }
      $j.each(tableDataIndexes, function(index, tableIndex) {
        var row = tableData[tableIndex];
        if (row !== undefined) {
          row.title = topEntryRow.title;
          row.link  = link;
        }
      });
    });

    this._displayTable({statEntries: tableData});

  },

  _displayTable: function(data) {
    var table      = this.$(this.statOptions.divTable + ' tbody');
    var template   = this.$(this.statOptions.searchStatsTemplate).html();
    var table_html = $j.mustache(template, data);

    if (this.statOptions.appendData === false) {
      // if resetting the table, don't care about updating the current page since it will always be 1
      table.empty();
    } else {
      this.statOptions.appendData = false;
    }

    table.append(table_html);
  },

  _displayTotal: function(page, totalEntries) {
    // When sorting, we ask for more data than page size.
    var currentDisplayedRows  = this.$("table.tickets tbody tr:visible").length;
    var pageTotalDiv          = this.$(this.statOptions.divPaginationTotal);
    var from                  = (currentDisplayedRows === 0 ? '0' : '1');
    var total                 = (this.$(".active").attr("id") === "searches") ? totalEntries : currentDisplayedRows;

    pageTotalDiv.html($j.mustache(I18n.t("txt.admin.views.search.stats_summary.currently_displayed_search_results_out_of_total"),
          {"from": from, "currentDisplayedRows": currentDisplayedRows, "total": total}));
  },

  isFeedbackData: function(context, callback) {
    var statOptions = new Zendesk.Stats.SearchStatsOptions({
      divTable: 'search_string_stats',
      statsType: 'feedback_tab'
    });

    $j.ajax({
      url: statOptions.getStatsAPIEndpoint(),
      dataType: 'json',
      success: function(tableData) {
        // once we have top entry details, display the table
        callback.call(context, tableData.data.length > 0);
      }
    });
  }

};
