/*globals Zendesk*/
Zendesk.NS("Stats");

/* Library for creating flot graphs and drawing them
 *
 *     // defaults to 30 days ago from today
 *     var graph = new Zendesk.Stats.Graph("#special", {
 *       graphType: "area",
 *       resource: "forum",
 *       objectType: "forum",
 *       objectId: 123,
 *       statName: "entry_views"
 *     });
 *
 *     // .draw() is the one public method on a Zendesk.StatsGraph.Graph
 *     graph.draw();
 *
*/
Zendesk.Stats.Graph = function(container, options) {
  this.graphData = {};

  this.container      = $j(container);
  this.searchStatURL  = options.searchStatURL;
  this.resource       = options.resource;
  this.statName       = options.statName;
  this.objectId       = options.objectId   || 0;
  this.objectType     = options.objectType || "account";
  this.graphType      = options.graphType;
  this.totalCountDiv  = options.totalCountDiv;
  this.nocache        = options.nocache;
  // passing data will cause the graph to render without fetching from the API
  this.data           = options.data;

  // sets default time slice to (30 days ago - today)
  if ( options.start ) {
    this.startTime = options.start;
  } else {
    var today = parseInt(new Date() / 1000);
    this.startTime = today - (60 * 60 * 24 * 30);
  }
  this.statsURL = this._statsURL();

  this.flotOptions = Zendesk.Stats.FlotOptions[this.graphType];
};

Zendesk.Stats.Graph.prototype = {

  // Build a stats URL for a resource.
  _statsURL: function() {
    if (this.data) { return false; }

    if (this.searchStatURL) {
      return "/api/v1/stats/graph_data" + this.searchStatURL + "/" + this.statName;
    }
    var base = "/api/v1/stats/graph_data/" + this.objectType + "/";
    if ( this.objectId ) {
      base += this.objectId + "/";
    }
    // forum_stats_by_[forum, event, account]
    base += this.resource + "_stats_by_" + this.objectType;

    base += "/" + this.statName;
    return base;
  },

  // public method to fetch stats and render graph
  draw: function() {
    if (this.graphData[this.statsURL] || this.data) {
      this._updateDetailGraphTitle();
      this._render();
      this._addPlotHover();
    } else {
      var data = {start: this.startTime, interval: 86400};
      if ( this.nocache ) {
        data.nocache = this.nocache;
      }

      $j.getJSON(this.statsURL, data)
        .done($j.proxy(this._onFetchGraphData, this, this.statsURL));
    }
  },

  // actually draws graph, calling into Flot
  _render: function() {
    var data = this.data || this.graphData[this.statsURL].data;
    $j.plot(this.container, [{data: data}], this.flotOptions);
  },

  // callback when stats data is fetched
  _onFetchGraphData: function(url, data) {
    //Show nulls as zeroes
    for(i=0; i< data.data.length; i++) {
      point = data.data[i];
      if(point[1] == null) {
        point[1] = 0;
      }
    }
    this.container.data("graph_data", data); // stash for computing an average graph
    this.graphData[url] = data;

    this._updateTotalCount(url);
    this._updateDetailGraphTitle();
    this._render();
    this._addPlotHover();
  },

  _updateDetailGraphTitle: function() {
    var title = $j(".sparkline.active").parent().children(".sparkline_title").html();
    $j("#detailed_stats_graph_title").html(title);
  },

  _updateTotalCount: function(url) {
    if ( $j(this.totalCountDiv).is(':empty') ) {
      var data = this.graphData[this.statsURL].data, totalCount;
      totalCount = _.inject(data, function(memo, d) { return memo + d[1]; }, 0);
      $j(this.totalCountDiv).append(Zendesk.Stats.formatNumber("0,0", totalCount));
    }
  },

  _addPlotHover: function() {
    var prev = null,
        self = this;
    $j(this.container).bind("plothover", function(event, pos, item) {
      if (item) {
        if (prev !== (item.pageX * item.datapoint[1])) {
          prev = item.pageX * item.datapoint[1];
          $j("#stats_point_value").remove();
          var total = Zendesk.Stats.formatNumber("0,0", item.datapoint[1]);
          self._displayPointValue(item.pageX, item.pageY, total);
        }
      } else {
        $j("#stats_point_value").remove();
        prev = null;
      }
    });
  },

  // tooltip when hovering on the area graph's data points
  _displayPointValue: function(x, y, contents) {
    $j('<div id="stats_point_value">' + contents + "</div>").css( {
      position: "absolute",
      display: "none",
      top: y - 29,
      left: x - 9,
      border: "2px solid #000",
      "-webkit-border-radius": "2px",
      "-moz-border-radius": "2px",
      "font-size": "12px",
      padding: "4px",
      color: "#fff",
      "background-color": "#000",
      opacity: 0.80
    }).appendTo("body").fadeIn(200);
  }
};

// See http://flot.googlecode.com/svn/trunk/API.txt for available flot options
Zendesk.Stats.FlotOptions = {
  // Area graph (line graph with fill)
  area: {
    colors: ["#137ac6"],
    grid: {
      backgroundColor: "#fff",
      borderWidth: 0,
      hoverable: true,
      mouseActiveRadius: "240",
      autoHighlight: false,
      minBorderMargin: "20"
    },
    yaxis: {
      min: 0, // prevents flot from having negative numbers on y-axis when there are zero stats collected
      tickFormatter: function(val, _) {
        // we want only whole numbers on the y-axis
        return Zendesk.Stats.formatNumber("0,0", Math.round(val));
      },
      maxFloor: 5
    },
    xaxis: {
      mode: "time",
      monthNames: I18n.t('date.abbr_month_names').slice(1),
      timeformat: I18n.t('date.formats.short')
    },
    legend: {
      show: false
    },
    series: {
      lines: {
        show: true,
        fill: true,
        fillColor: "#e6f2fa",
        lineWidth: "1"
      },
      points: {
        show: true,
        fill: true,
        fillColor: "#137ac6",
        radius: "1.5",
        symbol: "circle"
      },
      shadowSize: 0
    }
  },

  // Area graph (line graph with fill)
  percentage: {
    colors: ["#137ac6"],
    grid: {
      backgroundColor: "#fff",
      borderWidth: 0,
      hoverable: true,
      mouseActiveRadius: "240",
      autoHighlight: false,
      minBorderMargin: "20"
    },
    yaxis: {
      min: 0,
      maxFloor: 100,
      tickSize: 25,
      tickFormatter: function(val, _) {
        return String(val) + "%";
      }
    },
    xaxis: {
      mode: "time"
    },
    legend: {
      show: false
    },
    series: {
      lines: {
        show: true,
        fill: true,
        fillColor: "#e6f2fa",
        lineWidth: "1"
      },
      points: {
        show: true,
        fill: true,
        fillColor: "#137ac6",
        radius: "1.5",
        symbol: "circle"
      },
      shadowSize: 0
    }
  },

  // small at-a-glance line graph (sparkline)
  sparkline: {
    colors: ["#137ac6"],
    shadowSize: 0,
    grid: {
      show: false
    },
    yaxis: { min: 0 },
    xaxis: {
      mode: "time"
    },
    legend: {
      show: false
    },
    series: {
      lines: {
        show: true,
        fill: true,
        fillColor: "#e6f2fa",
        lineWidth: 1
      }
    }
  }
};

/*
* Formats the number according to the 'format' string;
* adherses to the american number standard where a comma
* is inserted after every 3 digits.
*  note: there should be only 1 contiguous number in the format,
* where a number consists of digits, period, and commas
*        any other characters can be wrapped around this number, including '$', '%', or text
*        examples (123456.789):
*          '0′ - (123456) show only digits, no precision
*          '0.00′ - (123456.78) show only digits, 2 precision
*          '0.0000′ - (123456.7890) show only digits, 4 precision
*          '0,000′ - (123,456) show comma and digits, no precision
*          '0,000.00′ - (123,456.78) show comma and digits, 2 precision
*          '0,0.00′ - (123,456.78) shortcut method, show comma and digits, 2 precision
*
* @method format
* @param format {string} the way you would like to format this text
* @return {string} the formatted number
* @public
*/

Zendesk.Stats.formatNumber = function(format, num) {
  if (typeof format !== "string" ) {
    return("");
  }

  if ( num === null || typeof num === "undefined" ) {
    num = 0;
  }

  var hasComma = -1 < format.indexOf(','),
    psplit = format.split('.');

  // compute precision
  if (1 < psplit.length) {
    // fix number precision
    num = num.toFixed(psplit[1].length);
  }
  // error: too many periods
  else if (2 < psplit.length) {
    throw('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
  }
  // remove precision
  else {
    num = num.toFixed(0);
  }

  // get the string now num precision is correct
  var fnum = num.toString();

  // format has comma, then compute commas
  if (hasComma) {
    // remove precision for computation
    psplit = fnum.split('.');

    var cnum = psplit[0],
      parr = [],
      j = cnum.length,
      m = Math.floor(j / 3),
      n = cnum.length % 3 || 3; // n cannot be ZERO or causes infinite loop

    // break the number into chunks of 3 digits; first chunk may be less than 3
    for (var i = 0; i < j; i += n) {
      if (i !== 0) {n = 3;}
      parr[parr.length] = cnum.substr(i, n);
      m -= 1;
    }

    // put chunks back together, separated by comma
    fnum = parr.join(',');

    // add the precision back in
    if (psplit[1]) {fnum += '.' + psplit[1];}
  }

  // replace the number portion of the format with fnum
  return format.replace(/[\d,?\.?]+/, fnum);
};
