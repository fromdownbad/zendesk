/*! @preserve ZRC@4.0.2, RC@^0.15.4 */
var ZendeskRadarClient =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {var root = typeof window === 'undefined' ? global : window
	var wrapper = __webpack_require__(1)
	var logger = __webpack_require__(35)('zendesk_radar_client:deprecated')

	module.exports = wrapper

	// Legacy / deprecated API support
	var MicroEE = __webpack_require__(4)
	var callback_tracker = __webpack_require__(44)
	var tracable = __webpack_require__(45)

	root.RadarClient = wrapper // legacy behavior: alias global RadarClient == ZendeskRadarClient
	root.MicroEE = MicroEE

	// Legacy / deprecated:
	module.exports.Tracable = function () {
	  logger.debug('DEPRECATED: ZendeskRadarClient.Tracable')
	  return tracable.apply(null, arguments)
	}
	module.exports.Tracker = {
	  create: function () {
	    logger.debug('DEPRECATED: ZendeskRadarClient.Tracker.create')
	    return callback_tracker.create.apply(null, arguments)
	  },
	  logger: function () {
	    logger.debug('DEPRECATED: ZendeskRadarClient.Tracker.logger')
	    return callback_tracker.logger.apply(null, arguments)
	  }
	}

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/* globals window */
	var root = typeof window === 'undefined' ? global : window
	root.navigator = root.navigator || {userAgent: 'ignore'}

	var immediate = typeof setImmediate === 'undefined' ? function (fn) { return setTimeout(fn, 1) } : setImmediate

	var radarClientInstance = __webpack_require__(2)
	var RadarClient = radarClientInstance.constructor
	var logger = radarClientInstance._log('zendesk_radar_client:wrapper')
	var Retriever = __webpack_require__(43)

	RadarClient.prototype.getRetriever = function () {
	  if (root.RADAR_CLIENT_DEBUGGING) {
	    if (root.alert) {
	      root.alert("You should not be accessing Zendesk's RadarClient internals unless you are debugging")
	    }

	    return this._retriever
	  }
	}

	RadarClient.prototype._configListeners = function () {
	  var self = this
	  if (!this._retriever) {
	    // copy backoff and _log
	    this.Backoff = radarClientInstance.Backoff
	    this._log = radarClientInstance._log

	    this._retriever = new Retriever(this)
	    this.on('err', function (message) {
	      if (message.value === 'auth' && self._retriever.lastTry < self._retriever.tenMinutesAgo()) {
	        logger.debug('Calling getConfiguration')
	        self._retriever.expireToken(0) // Force token retrieval
	        self._retriever.getConfiguration()
	      }
	    })

	    this.manager.removeAllListeners('authenticate')
	    this.manager.on('authenticate', function authenticated () {
	      self._retriever.getConfiguration('activateRadarClient')
	    })

	    this.removeAllListeners('authenticateMessage')
	    this.on('authenticateMessage', function authenticateMessage (message) {
	      self._retriever.getConfiguration('authenticateMessage', [message])
	    })
	  }
	}

	RadarClient.prototype.detach = function () {
	  logger.info({ action: 'detach' })
	  var reason
	  if (arguments.length) {
	    reason = Array.prototype.slice.call(arguments).join(' ')
	    logger.error({detach: reason})
	  }

	  this._retriever.configuration.auth = ''
	  this._isConfigured = false
	  this._restoreRequired = true
	  this._waitingForConfigure = true
	  this.manager.close()
	}

	var oldConfigure = RadarClient.prototype.configure
	RadarClient.prototype.configure = function (configuration) {
	  configuration.path = '/engine.io-1.4.2'
	  // prevent use of Blob in partial implementation browsers/phantomjs
	  configuration.forceBase64 = configuration.hasOwnProperty('forceBase64') ? configuration.forceBase64 : true
	  return oldConfigure.apply(this, arguments)
	}

	RadarClient.prototype.reattach = function (done) {
	  logger.info({ action: 'reattach' })
	  this._retriever.getConfiguration('reattach', [done])
	}

	RadarClient.prototype.setAPIHost = function (apiHost) {
	  this._apiHost = apiHost
	}

	RadarClient.prototype.initialize = function (done) {
	  var self = this
	  logger.info({ action: 'initialize' })
	  if (this.initialized) {
	    this._retriever.getConfiguration('whenReady', [done])
	  } else {
	    this._storeServerAttributes()
	    this._configListeners()
	    this.initialized = true

	    this._retriever.initializing = true
	    this._retriever.apiHost = this._apiHost
	    this._retriever.getConfiguration('initialize', [function () {
	      self._setupReconfigureListener() // must be called after client is configured
	      if (done) { done() }
	    }])
	  }
	}
	RadarClient.prototype._setupReconfigureListener = function () {
	  var self = this
	  self.manager.once('activate', function () {
	    self.status('reconfigure').on(function () {
	      logger.info('client_reconnect message')
	      self.reconnect()
	    })
	    .subscribe(function () {
	      logger.info('subscribed to reconfigure')
	    })
	  })
	}

	RadarClient.prototype.reconnect = function () {
	  var self = this

	  immediate(function () {
	    self.detach()
	  })

	  self.once('close', function () {
	    logger.info('client_reconnect REATTACH')
	    self.reattach()
	  })
	}

	// Store attributes of the server instance to which this client is connected
	RadarClient.prototype._storeServerAttributes = function () {
	  var self = this

	  self.on('message:in', function (message) {
	    if (message && message.op === 'send_server_attributes') {
	      logger.debug('Received send_server_attributes message: store server attributes in the client.')
	      self.server_attributes = message
	    }
	  })
	}

	RadarClient.Retriever = Retriever

	var radarClient = new RadarClient()
	radarClient.on('message:in', function (message) {
	  logger.debug({ incoming: message })
	})

	radarClient.on('message:out', function (message) {
	  logger.debug({ outgoing: message })
	})

	module.exports = radarClient
	// legacy: for modules that expect radarClient to require a call to main()
	root.radarClient = {main: function () { return radarClient }}

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var Client = __webpack_require__(3)
	var instance = new Client()
	var Backoff = __webpack_require__(36)

	instance._log = __webpack_require__(35)
	instance.Backoff = Backoff

	// This module makes radar_client a singleton to prevent multiple connections etc.

	module.exports = instance


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/* globals setImmediate */
	var MicroEE = __webpack_require__(4)
	var eio = __webpack_require__(5)
	var Scope = __webpack_require__(33)
	var StateMachine = __webpack_require__(34)
	var immediate = typeof setImmediate !== 'undefined' ? setImmediate : function (fn) { setTimeout(fn, 1) }
	var getClientVersion = __webpack_require__(38)
	var Request = __webpack_require__(39).Request
	var Response = __webpack_require__(39).Response

	function Client (backend) {
	  this.logger = __webpack_require__(35)('radar_client')
	  this._ackCounter = 1
	  this._channelSyncTimes = {}
	  this._uses = {}
	  this._presences = {}
	  this._subscriptions = {}
	  this._restoreRequired = false
	  this._queuedRequests = []
	  this._identitySetRequired = true
	  this._isConfigured = false

	  this._createManager()
	  this.configure(false)
	  this._addListeners()

	  // Allow backend substitution for tests
	  this.backend = backend || eio
	}

	MicroEE.mixin(Client)

	// Public API

	// Each use of the client is registered with "alloc", and a given use often
	// persists through many connects and disconnects.
	// The state machine - "manager" - handles connects and disconnects
	Client.prototype.alloc = function (useName, callback) {
	  var self = this
	  if (!this._uses[useName]) {
	    this.logger().info('alloc: ', useName)
	    this.once('ready', function () {
	      self.logger().info('ready: ', useName)
	    })

	    this._uses[useName] = true
	  }

	  callback && this.once('ready', function () {
	    if (self._uses.hasOwnProperty(useName)) {
	      callback()
	    }
	  })

	  if (this._isConfigured) {
	    this.manager.start()
	  } else {
	    this._waitingForConfigure = true
	  }

	  return this
	}

	// When done with a given use of the client, unregister the use
	// Only when all uses are unregistered do we disconnect the client
	Client.prototype.dealloc = function (useName) {
	  this.logger().info({ op: 'dealloc', useName: useName })

	  delete this._uses[useName]

	  var stillAllocated = false
	  var key

	  for (key in this._uses) {
	    if (this._uses.hasOwnProperty(key)) {
	      stillAllocated = true
	      break
	    }
	  }
	  if (!stillAllocated) {
	    this.logger().info('closing the connection')
	    this.manager.close()
	  }
	}

	Client.prototype.currentState = function () {
	  return this.manager.current
	}

	Client.prototype.configure = function (hash) {
	  var configuration = hash || this._configuration || { accountName: '', userId: 0, userType: 0 }
	  configuration.userType = configuration.userType || 0
	  this._configuration = this._me = configuration
	  this._isConfigured = this._isConfigured || !!hash

	  if (this._isConfigured && this._waitingForConfigure) {
	    this._waitingForConfigure = false
	    this.manager.start()
	  }

	  return this
	}

	Client.prototype.configuration = function (configKey) {
	  if (configKey in this._configuration) {
	    return JSON.parse(JSON.stringify(this._configuration[configKey]))
	  } else {
	    return null
	  }
	}

	Client.prototype.currentUserId = function () {
	  return this._configuration && this._configuration.userId
	}

	Client.prototype.currentClientId = function () {
	  return this._socket && this._socket.id
	}

	// Return the chainable scope object for a given message type

	Client.prototype.message = function (scope) {
	  return new Scope('message', scope, this)
	}

	Client.prototype.presence = function (scope) {
	  return new Scope('presence', scope, this)
	}

	Client.prototype.status = function (scope) {
	  return new Scope('status', scope, this)
	}

	Client.prototype.stream = function (scope) {
	  return new Scope('stream', scope, this)
	}

	Client.prototype.control = function (scope) {
	  return new Scope('control', scope, this)
	}

	// Operations

	Client.prototype.nameSync = function (scope, options, callback) {
	  var request = Request.buildNameSync(scope, options)
	  return this._write(request, callback)
	}

	Client.prototype.push = function (scope, resource, action, value, callback) {
	  var request = Request.buildPush(scope, resource, action, value)
	  return this._write(request, callback)
	}

	Client.prototype.set = function (scope, value, clientData, callback) {
	  var request

	  callback = _chooseFunction(clientData, callback)
	  clientData = _nullIfFunction(clientData)

	  request = Request.buildSet(scope, value,
	    this._configuration.userId, this._configuration.userType,
	    clientData)

	  return this._write(request, callback)
	}

	Client.prototype.publish = function (scope, value, callback) {
	  var request = Request.buildPublish(scope, value)
	  return this._write(request, callback)
	}

	Client.prototype.subscribe = function (scope, options, callback) {
	  callback = _chooseFunction(options, callback)
	  options = _nullIfFunction(options)

	  var request = Request.buildSubscribe(scope, options)

	  return this._write(request, callback)
	}

	Client.prototype.unsubscribe = function (scope, callback) {
	  var request = Request.buildUnsubscribe(scope)
	  return this._write(request, callback)
	}

	// sync returns the actual value of the operation
	Client.prototype.sync = function (scope, options, callback) {
	  var request, onResponse, v1Presence

	  callback = _chooseFunction(options, callback)
	  options = _nullIfFunction(options)

	  request = Request.buildSync(scope, options)

	  v1Presence = !options && request.isPresence()
	  onResponse = function (message) {
	    var response = new Response(message)
	    if (response && response.isFor(request)) {
	      if (v1Presence) {
	        response.forceV1Response()
	      }
	      if (callback) {
	        callback(response.getMessage())
	      }
	      return true
	    }
	    return false
	  }

	  this.when('get', onResponse)

	  // sync does not return ACK (it sends back a data message)
	  return this._write(request)
	}

	// get returns the actual value of the operation
	Client.prototype.get = function (scope, options, callback) {
	  var request

	  callback = _chooseFunction(options, callback)
	  options = _nullIfFunction(options)

	  request = Request.buildGet(scope, options)

	  var onResponse = function (message) {
	    var response = new Response(message)
	    if (response && response.isFor(request)) {
	      if (callback) {
	        callback(response.getMessage())
	      }
	      return true
	    }
	    return false
	  }

	  this.when('get', onResponse)

	  // get does not return ACK (it sends back a data message)
	  return this._write(request)
	}

	// Private API

	var _chooseFunction = function (options, callback) {
	  return typeof (options) === 'function' ? options : callback
	}

	var _nullIfFunction = function (options) {
	  if (typeof (options) === 'function') {
	    return null
	  }
	  return options
	}

	Client.prototype._addListeners = function () {
	  // Add authentication data to a request message; _write() emits authenticateMessage
	  this.on('authenticateMessage', function (message) {
	    var request = new Request(message)
	    request.setAuthData(this._configuration)

	    this.emit('messageAuthenticated', request.getMessage())
	  })

	  // Once the request is authenticated, send it to the server
	  this.on('messageAuthenticated', function (message) {
	    var request = new Request(message)
	    this._sendMessage(request)
	  })
	}

	Client.prototype._write = function (request, callback) {
	  var self = this

	  if (callback) {
	    request.setAttr('ack', this._ackCounter++)

	    // Wait ack
	    this.when('ack', function (message) {
	      var response = new Response(message)
	      self.logger().debug('ack', response)
	      if (!response.isAckFor(request)) { return false }
	      callback(request.getMessage())

	      return true
	    })
	  }

	  this.emit('authenticateMessage', request.getMessage())

	  return this
	}

	Client.prototype._batch = function (response) {
	  var to = response.getAttr('to')
	  var value = response.getAttr('value')
	  var time = response.getAttr('time')

	  if (!response.isValid()) {
	    this.logger().info('response is invalid:', response.getMessage())
	    return false
	  }

	  var index = 0
	  var data
	  var length = value.length
	  var newest = time
	  var current = this._channelSyncTimes[to] || 0

	  for (; index < length; index = index + 2) {
	    data = JSON.parse(value[index])
	    time = value[index + 1]

	    if (time > current) {
	      this.emitNext(to, data)
	    }
	    if (time > newest) {
	      newest = time
	    }
	  }
	  this._channelSyncTimes[to] = newest
	}

	Client.prototype._createManager = function () {
	  var self = this
	  var manager = this.manager = StateMachine.create()

	  manager.on('enterState', function (state) {
	    self.emit(state)
	  })

	  manager.on('event', function (event) {
	    self.emit(event)
	  })

	  manager.on('connect', function (data) {
	    var socket = self._socket = new self.backend.Socket(self._configuration)

	    socket.once('open', function () {
	      self.logger().debug('socket open', socket.id)
	      manager.established()
	    })

	    socket.once('close', function (reason, description) {
	      self.logger().debug('socket closed', socket.id, reason, description)
	      socket.removeAllListeners('message')
	      self._socket = null

	      // Patch for polling-xhr continuing to poll after socket close (HTTP:POST
	      // failure).  socket.transport is in error but not closed, so if a subsequent
	      // poll succeeds, the transport remains open and polling until server closes
	      // the socket.
	      if (socket.transport) {
	        socket.transport.close()
	      }

	      if (!manager.is('closed')) {
	        manager.disconnect()
	      }
	    })

	    socket.on('message', function (message) {
	      self._messageReceived(message)
	    })

	    manager.removeAllListeners('close')
	    manager.once('close', function () {
	      socket.close()
	    })
	  })

	  manager.on('activate', function () {
	    self._identitySet()
	    self._restore()
	    self.emit('ready')
	  })

	  manager.on('authenticate', function () {
	    // Can be overridden in order to establish an authentication protocol
	    manager.activate()
	  })

	  manager.on('disconnect', function () {
	    self._restoreRequired = true
	    self._identitySetRequired = true
	  })
	}

	// Memorize subscriptions and presence states; return "true" for a message that
	// adds to the memorized subscriptions or presences
	Client.prototype._memorize = function (request) {
	  var op = request.getAttr('op')
	  var to = request.getAttr('to')
	  var value = request.getAttr('value')

	  switch (op) {
	    case 'unsubscribe':
	      // Remove from queue
	      if (this._subscriptions[to]) {
	        delete this._subscriptions[to]
	      }
	      return true

	    case 'sync':
	    case 'subscribe':
	      // A catch for when *subscribe* is called after *sync*
	      if (this._subscriptions[to] !== 'sync') {
	        this._subscriptions[to] = op
	      }
	      return true

	    case 'set':
	      if (request.isPresence()) {
	        if (value !== 'offline') {
	          this._presences[to] = value
	        } else {
	          delete this._presences[to]
	        }
	        return true
	      }
	  }

	  return false
	}

	Client.prototype._restore = function () {
	  var item
	  var to
	  var counts = { subscriptions: 0, presences: 0, messages: 0 }
	  if (this._restoreRequired) {
	    this._restoreRequired = false

	    for (to in this._subscriptions) {
	      if (this._subscriptions.hasOwnProperty(to)) {
	        item = this._subscriptions[to]
	        this[item](to)
	        counts.subscriptions += 1
	      }
	    }

	    for (to in this._presences) {
	      if (this._presences.hasOwnProperty(to)) {
	        this.set(to, this._presences[to])
	        counts.presences += 1
	      }
	    }

	    while (this._queuedRequests.length) {
	      this._write(this._queuedRequests.shift())
	      counts.messages += 1
	    }

	    this.logger().debug('restore-subscriptions', counts)
	  }
	}

	Client.prototype._sendMessage = function (request) {
	  var memorized = this._memorize(request)
	  var ack = request.getAttr('ack')

	  this.emit('message:out', request.getMessage())

	  if (this._socket && this.manager.is('activated')) {
	    this._socket.sendPacket('message', request.payload())
	  } else if (this._isConfigured) {
	    this._restoreRequired = true
	    this._identitySetRequired = true
	    if (!memorized || ack) {
	      this._queuedRequests.push(request)
	    }
	    this.manager.connectWhenAble()
	  }
	}

	Client.prototype._messageReceived = function (msg) {
	  var response = new Response(JSON.parse(msg))
	  var op = response.getAttr('op')
	  var to = response.getAttr('to')

	  this.emit('message:in', response.getMessage())

	  switch (op) {
	    case 'err':
	    case 'ack':
	    case 'get':
	      this.emitNext(op, response.getMessage())
	      break

	    case 'sync':
	      this._batch(response)
	      break

	    default:
	      this.emitNext(to, response.getMessage())
	  }
	}

	Client.prototype.emitNext = function () {
	  var self = this
	  var args = Array.prototype.slice.call(arguments)
	  immediate(function () { self.emit.apply(self, args) })
	}

	Client.prototype._identitySet = function () {
	  if (this._identitySetRequired) {
	    this._identitySetRequired = false

	    if (!this.name) {
	      this.name = this._uuidV4Generate()
	    }

	    // Send msg that associates this.id with current name
	    var association = { id: this._socket.id, name: this.name }
	    var clientVersion = getClientVersion()
	    var options = { association: association, clientVersion: clientVersion }
	    var self = this

	    this.control('clientName').nameSync(options, function (message) {
	      self.logger('nameSync message: ' + JSON.stringify(message))
	    })
	  }
	}

	// Variant (by Jeff Ward) of code behind node-uuid, but avoids need for module
	var lut = []
	for (var i = 0; i < 256; i++) { lut[i] = (i < 16 ? '0' : '') + (i).toString(16) }
	Client.prototype._uuidV4Generate = function () {
	  var d0 = Math.random() * 0xffffffff | 0
	  var d1 = Math.random() * 0xffffffff | 0
	  var d2 = Math.random() * 0xffffffff | 0
	  var d3 = Math.random() * 0xffffffff | 0
	  return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
	  lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
	  lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
	  lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff]
	}

	Client.setBackend = function (lib) { eio = lib }

	module.exports = Client


/***/ },
/* 4 */
/***/ function(module, exports) {

	function M() { this._events = {}; }
	M.prototype = {
	  on: function(ev, cb) {
	    this._events || (this._events = {});
	    var e = this._events;
	    (e[ev] || (e[ev] = [])).push(cb);
	    return this;
	  },
	  removeListener: function(ev, cb) {
	    var e = this._events[ev] || [], i;
	    for(i = e.length-1; i >= 0 && e[i]; i--){
	      if(e[i] === cb || e[i].cb === cb) { e.splice(i, 1); }
	    }
	  },
	  removeAllListeners: function(ev) {
	    if(!ev) { this._events = {}; }
	    else { this._events[ev] && (this._events[ev] = []); }
	  },
	  emit: function(ev) {
	    this._events || (this._events = {});
	    var args = Array.prototype.slice.call(arguments, 1), i, e = this._events[ev] || [];
	    for(i = e.length-1; i >= 0 && e[i]; i--){
	      e[i].apply(this, args);
	    }
	    return this;
	  },
	  when: function(ev, cb) {
	    return this.once(ev, cb, true);
	  },
	  once: function(ev, cb, when) {
	    if(!cb) return this;
	    function c() {
	      if(!when) this.removeListener(ev, c);
	      if(cb.apply(this, arguments) && when) this.removeListener(ev, c);
	    }
	    c.cb = cb;
	    this.on(ev, c);
	    return this;
	  }
	};
	M.mixin = function(dest) {
	  var o = M.prototype, k;
	  for (k in o) {
	    o.hasOwnProperty(k) && (dest.prototype[k] = o[k]);
	  }
	};
	module.exports = M;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	
	module.exports =  __webpack_require__(6);


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	
	module.exports = __webpack_require__(7);

	/**
	 * Exports parser
	 *
	 * @api public
	 *
	 */
	module.exports.parser = __webpack_require__(15);


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/**
	 * Module dependencies.
	 */

	var transports = __webpack_require__(8);
	var Emitter = __webpack_require__(23);
	var debug = __webpack_require__(26)('engine.io-client:socket');
	var index = __webpack_require__(30);
	var parser = __webpack_require__(15);
	var parseuri = __webpack_require__(31);
	var parsejson = __webpack_require__(32);
	var parseqs = __webpack_require__(24);

	/**
	 * Module exports.
	 */

	module.exports = Socket;

	/**
	 * Noop function.
	 *
	 * @api private
	 */

	function noop(){}

	/**
	 * Socket constructor.
	 *
	 * @param {String|Object} uri or options
	 * @param {Object} options
	 * @api public
	 */

	function Socket(uri, opts){
	  if (!(this instanceof Socket)) return new Socket(uri, opts);

	  opts = opts || {};

	  if (uri && 'object' == typeof uri) {
	    opts = uri;
	    uri = null;
	  }

	  if (uri) {
	    uri = parseuri(uri);
	    opts.host = uri.host;
	    opts.secure = uri.protocol == 'https' || uri.protocol == 'wss';
	    opts.port = uri.port;
	    if (uri.query) opts.query = uri.query;
	  }

	  this.secure = null != opts.secure ? opts.secure :
	    (global.location && 'https:' == location.protocol);

	  if (opts.host) {
	    var pieces = opts.host.split(':');
	    opts.hostname = pieces.shift();
	    if (pieces.length) opts.port = pieces.pop();
	  }

	  this.agent = opts.agent || false;
	  this.hostname = opts.hostname ||
	    (global.location ? location.hostname : 'localhost');
	  this.port = opts.port || (global.location && location.port ?
	       location.port :
	       (this.secure ? 443 : 80));
	  this.query = opts.query || {};
	  if ('string' == typeof this.query) this.query = parseqs.decode(this.query);
	  this.upgrade = false !== opts.upgrade;
	  this.path = (opts.path || '/engine.io').replace(/\/$/, '') + '/';
	  this.forceJSONP = !!opts.forceJSONP;
	  this.jsonp = false !== opts.jsonp;
	  this.forceBase64 = !!opts.forceBase64;
	  this.enablesXDR = !!opts.enablesXDR;
	  this.timestampParam = opts.timestampParam || 't';
	  this.timestampRequests = opts.timestampRequests;
	  this.transports = opts.transports || ['polling', 'websocket'];
	  this.readyState = '';
	  this.writeBuffer = [];
	  this.callbackBuffer = [];
	  this.policyPort = opts.policyPort || 843;
	  this.rememberUpgrade = opts.rememberUpgrade || false;
	  this.open();
	  this.binaryType = null;
	  this.onlyBinaryUpgrades = opts.onlyBinaryUpgrades;
	}

	Socket.priorWebsocketSuccess = false;

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Socket.prototype);

	/**
	 * Protocol version.
	 *
	 * @api public
	 */

	Socket.protocol = parser.protocol; // this is an int

	/**
	 * Expose deps for legacy compatibility
	 * and standalone browser access.
	 */

	Socket.Socket = Socket;
	Socket.Transport = __webpack_require__(14);
	Socket.transports = __webpack_require__(8);
	Socket.parser = __webpack_require__(15);

	/**
	 * Creates transport of the given type.
	 *
	 * @param {String} transport name
	 * @return {Transport}
	 * @api private
	 */

	Socket.prototype.createTransport = function (name) {
	  debug('creating transport "%s"', name);
	  var query = clone(this.query);

	  // append engine.io protocol identifier
	  query.EIO = parser.protocol;

	  // transport name
	  query.transport = name;

	  // session id if we already have one
	  if (this.id) query.sid = this.id;

	  var transport = new transports[name]({
	    agent: this.agent,
	    hostname: this.hostname,
	    port: this.port,
	    secure: this.secure,
	    path: this.path,
	    query: query,
	    forceJSONP: this.forceJSONP,
	    jsonp: this.jsonp,
	    forceBase64: this.forceBase64,
	    enablesXDR: this.enablesXDR,
	    timestampRequests: this.timestampRequests,
	    timestampParam: this.timestampParam,
	    policyPort: this.policyPort,
	    socket: this
	  });

	  return transport;
	};

	function clone (obj) {
	  var o = {};
	  for (var i in obj) {
	    if (obj.hasOwnProperty(i)) {
	      o[i] = obj[i];
	    }
	  }
	  return o;
	}

	/**
	 * Initializes transport to use and starts probe.
	 *
	 * @api private
	 */
	Socket.prototype.open = function () {
	  var transport;
	  if (this.rememberUpgrade && Socket.priorWebsocketSuccess && this.transports.indexOf('websocket') != -1) {
	    transport = 'websocket';
	  } else if (0 == this.transports.length) {
	    // Emit error on next tick so it can be listened to
	    var self = this;
	    setTimeout(function() {
	      self.emit('error', 'No transports available');
	    }, 0);
	    return;
	  } else {
	    transport = this.transports[0];
	  }
	  this.readyState = 'opening';

	  // Retry with the next transport if the transport is disabled (jsonp: false)
	  var transport;
	  try {
	    transport = this.createTransport(transport);
	  } catch (e) {
	    this.transports.shift();
	    this.open();
	    return;
	  }

	  transport.open();
	  this.setTransport(transport);
	};

	/**
	 * Sets the current transport. Disables the existing one (if any).
	 *
	 * @api private
	 */

	Socket.prototype.setTransport = function(transport){
	  debug('setting transport %s', transport.name);
	  var self = this;

	  if (this.transport) {
	    debug('clearing existing transport %s', this.transport.name);
	    this.transport.removeAllListeners();
	  }

	  // set up transport
	  this.transport = transport;

	  // set up transport listeners
	  transport
	  .on('drain', function(){
	    self.onDrain();
	  })
	  .on('packet', function(packet){
	    self.onPacket(packet);
	  })
	  .on('error', function(e){
	    self.onError(e);
	  })
	  .on('close', function(){
	    self.onClose('transport close');
	  });
	};

	/**
	 * Probes a transport.
	 *
	 * @param {String} transport name
	 * @api private
	 */

	Socket.prototype.probe = function (name) {
	  debug('probing transport "%s"', name);
	  var transport = this.createTransport(name, { probe: 1 })
	    , failed = false
	    , self = this;

	  Socket.priorWebsocketSuccess = false;

	  function onTransportOpen(){
	    if (self.onlyBinaryUpgrades) {
	      var upgradeLosesBinary = !this.supportsBinary && self.transport.supportsBinary;
	      failed = failed || upgradeLosesBinary;
	    }
	    if (failed) return;

	    debug('probe transport "%s" opened', name);
	    transport.send([{ type: 'ping', data: 'probe' }]);
	    transport.once('packet', function (msg) {
	      if (failed) return;
	      if ('pong' == msg.type && 'probe' == msg.data) {
	        debug('probe transport "%s" pong', name);
	        self.upgrading = true;
	        self.emit('upgrading', transport);
	        if (!transport) return;
	        Socket.priorWebsocketSuccess = 'websocket' == transport.name;

	        debug('pausing current transport "%s"', self.transport.name);
	        self.transport.pause(function () {
	          if (failed) return;
	          if ('closed' == self.readyState) return;
	          debug('changing transport and sending upgrade packet');

	          cleanup();

	          self.setTransport(transport);
	          transport.send([{ type: 'upgrade' }]);
	          self.emit('upgrade', transport);
	          transport = null;
	          self.upgrading = false;
	          self.flush();
	        });
	      } else {
	        debug('probe transport "%s" failed', name);
	        var err = new Error('probe error');
	        err.transport = transport.name;
	        self.emit('upgradeError', err);
	      }
	    });
	  }

	  function freezeTransport() {
	    if (failed) return;

	    // Any callback called by transport should be ignored since now
	    failed = true;

	    cleanup();

	    transport.close();
	    transport = null;
	  }

	  //Handle any error that happens while probing
	  function onerror(err) {
	    var error = new Error('probe error: ' + err);
	    error.transport = transport.name;

	    freezeTransport();

	    debug('probe transport "%s" failed because of error: %s', name, err);

	    self.emit('upgradeError', error);
	  }

	  function onTransportClose(){
	    onerror("transport closed");
	  }

	  //When the socket is closed while we're probing
	  function onclose(){
	    onerror("socket closed");
	  }

	  //When the socket is upgraded while we're probing
	  function onupgrade(to){
	    if (transport && to.name != transport.name) {
	      debug('"%s" works - aborting "%s"', to.name, transport.name);
	      freezeTransport();
	    }
	  }

	  //Remove all listeners on the transport and on self
	  function cleanup(){
	    transport.removeListener('open', onTransportOpen);
	    transport.removeListener('error', onerror);
	    transport.removeListener('close', onTransportClose);
	    self.removeListener('close', onclose);
	    self.removeListener('upgrading', onupgrade);
	  }

	  transport.once('open', onTransportOpen);
	  transport.once('error', onerror);
	  transport.once('close', onTransportClose);

	  this.once('close', onclose);
	  this.once('upgrading', onupgrade);

	  transport.open();

	};

	/**
	 * Called when connection is deemed open.
	 *
	 * @api public
	 */

	Socket.prototype.onOpen = function () {
	  debug('socket open');
	  this.readyState = 'open';
	  Socket.priorWebsocketSuccess = 'websocket' == this.transport.name;
	  this.emit('open');
	  this.flush();

	  // we check for `readyState` in case an `open`
	  // listener already closed the socket
	  if ('open' == this.readyState && this.upgrade && this.transport.pause) {
	    debug('starting upgrade probes');
	    for (var i = 0, l = this.upgrades.length; i < l; i++) {
	      this.probe(this.upgrades[i]);
	    }
	  }
	};

	/**
	 * Handles a packet.
	 *
	 * @api private
	 */

	Socket.prototype.onPacket = function (packet) {
	  if ('opening' == this.readyState || 'open' == this.readyState) {
	    debug('socket receive: type "%s", data "%s"', packet.type, packet.data);

	    this.emit('packet', packet);

	    // Socket is live - any packet counts
	    this.emit('heartbeat');

	    switch (packet.type) {
	      case 'open':
	        this.onHandshake(parsejson(packet.data));
	        break;

	      case 'pong':
	        this.setPing();
	        break;

	      case 'error':
	        var err = new Error('server error');
	        err.code = packet.data;
	        this.emit('error', err);
	        break;

	      case 'message':
	        this.emit('data', packet.data);
	        this.emit('message', packet.data);
	        break;
	    }
	  } else {
	    debug('packet received with socket readyState "%s"', this.readyState);
	  }
	};

	/**
	 * Called upon handshake completion.
	 *
	 * @param {Object} handshake obj
	 * @api private
	 */

	Socket.prototype.onHandshake = function (data) {
	  this.emit('handshake', data);
	  this.id = data.sid;
	  this.transport.query.sid = data.sid;
	  this.upgrades = this.filterUpgrades(data.upgrades);
	  this.pingInterval = data.pingInterval;
	  this.pingTimeout = data.pingTimeout;
	  this.onOpen();
	  // In case open handler closes socket
	  if  ('closed' == this.readyState) return;
	  this.setPing();

	  // Prolong liveness of socket on heartbeat
	  this.removeListener('heartbeat', this.onHeartbeat);
	  this.on('heartbeat', this.onHeartbeat);
	};

	/**
	 * Resets ping timeout.
	 *
	 * @api private
	 */

	Socket.prototype.onHeartbeat = function (timeout) {
	  clearTimeout(this.pingTimeoutTimer);
	  var self = this;
	  self.pingTimeoutTimer = setTimeout(function () {
	    if ('closed' == self.readyState) return;
	    self.onClose('ping timeout');
	  }, timeout || (self.pingInterval + self.pingTimeout));
	};

	/**
	 * Pings server every `this.pingInterval` and expects response
	 * within `this.pingTimeout` or closes connection.
	 *
	 * @api private
	 */

	Socket.prototype.setPing = function () {
	  var self = this;
	  clearTimeout(self.pingIntervalTimer);
	  self.pingIntervalTimer = setTimeout(function () {
	    debug('writing ping packet - expecting pong within %sms', self.pingTimeout);
	    self.ping();
	    self.onHeartbeat(self.pingTimeout);
	  }, self.pingInterval);
	};

	/**
	* Sends a ping packet.
	*
	* @api public
	*/

	Socket.prototype.ping = function () {
	  this.sendPacket('ping');
	};

	/**
	 * Called on `drain` event
	 *
	 * @api private
	 */

	Socket.prototype.onDrain = function() {
	  for (var i = 0; i < this.prevBufferLen; i++) {
	    if (this.callbackBuffer[i]) {
	      this.callbackBuffer[i]();
	    }
	  }

	  this.writeBuffer.splice(0, this.prevBufferLen);
	  this.callbackBuffer.splice(0, this.prevBufferLen);

	  // setting prevBufferLen = 0 is very important
	  // for example, when upgrading, upgrade packet is sent over,
	  // and a nonzero prevBufferLen could cause problems on `drain`
	  this.prevBufferLen = 0;

	  if (this.writeBuffer.length == 0) {
	    this.emit('drain');
	  } else {
	    this.flush();
	  }
	};

	/**
	 * Flush write buffers.
	 *
	 * @api private
	 */

	Socket.prototype.flush = function () {
	  if ('closed' != this.readyState && this.transport.writable &&
	    !this.upgrading && this.writeBuffer.length) {
	    debug('flushing %d packets in socket', this.writeBuffer.length);
	    this.transport.send(this.writeBuffer);
	    // keep track of current length of writeBuffer
	    // splice writeBuffer and callbackBuffer on `drain`
	    this.prevBufferLen = this.writeBuffer.length;
	    this.emit('flush');
	  }
	};

	/**
	 * Sends a message.
	 *
	 * @param {String} message.
	 * @param {Function} callback function.
	 * @return {Socket} for chaining.
	 * @api public
	 */

	Socket.prototype.write =
	Socket.prototype.send = function (msg, fn) {
	  this.sendPacket('message', msg, fn);
	  return this;
	};

	/**
	 * Sends a packet.
	 *
	 * @param {String} packet type.
	 * @param {String} data.
	 * @param {Function} callback function.
	 * @api private
	 */

	Socket.prototype.sendPacket = function (type, data, fn) {
	  if ('closing' == this.readyState || 'closed' == this.readyState) {
	    return;
	  }

	  var packet = { type: type, data: data };
	  this.emit('packetCreate', packet);
	  this.writeBuffer.push(packet);
	  this.callbackBuffer.push(fn);
	  this.flush();
	};

	/**
	 * Closes the connection.
	 *
	 * @api private
	 */

	Socket.prototype.close = function () {
	  if ('opening' == this.readyState || 'open' == this.readyState) {
	    this.readyState = 'closing';

	    var self = this;

	    function close() {
	      self.onClose('forced close');
	      debug('socket closing - telling transport to close');
	      self.transport.close();
	    }

	    function cleanupAndClose() {
	      self.removeListener('upgrade', cleanupAndClose);
	      self.removeListener('upgradeError', cleanupAndClose);
	      close();
	    }

	    function waitForUpgrade() {
	      // wait for upgrade to finish since we can't send packets while pausing a transport
	      self.once('upgrade', cleanupAndClose);
	      self.once('upgradeError', cleanupAndClose);
	    }

	    if (this.writeBuffer.length) {
	      this.once('drain', function() {
	        if (this.upgrading) {
	          waitForUpgrade();
	        } else {
	          close();
	        }
	      });
	    } else if (this.upgrading) {
	      waitForUpgrade();
	    } else {
	      close();
	    }
	  }

	  return this;
	};

	/**
	 * Called upon transport error
	 *
	 * @api private
	 */

	Socket.prototype.onError = function (err) {
	  debug('socket error %j', err);
	  Socket.priorWebsocketSuccess = false;
	  this.emit('error', err);
	  this.onClose('transport error', err);
	};

	/**
	 * Called upon transport close.
	 *
	 * @api private
	 */

	Socket.prototype.onClose = function (reason, desc) {
	  if ('opening' == this.readyState || 'open' == this.readyState || 'closing' == this.readyState) {
	    debug('socket close with reason: "%s"', reason);
	    var self = this;

	    // clear timers
	    clearTimeout(this.pingIntervalTimer);
	    clearTimeout(this.pingTimeoutTimer);

	    // clean buffers in next tick, so developers can still
	    // grab the buffers on `close` event
	    setTimeout(function() {
	      self.writeBuffer = [];
	      self.callbackBuffer = [];
	      self.prevBufferLen = 0;
	    }, 0);

	    // stop event from firing again for transport
	    this.transport.removeAllListeners('close');

	    // ensure transport won't stay open
	    this.transport.close();

	    // ignore further transport communication
	    this.transport.removeAllListeners();

	    // set ready state
	    this.readyState = 'closed';

	    // clear session id
	    this.id = null;

	    // emit close event
	    this.emit('close', reason, desc);
	  }
	};

	/**
	 * Filters upgrades, returning only those matching client transports.
	 *
	 * @param {Array} server upgrades
	 * @api private
	 *
	 */

	Socket.prototype.filterUpgrades = function (upgrades) {
	  var filteredUpgrades = [];
	  for (var i = 0, j = upgrades.length; i<j; i++) {
	    if (~index(this.transports, upgrades[i])) filteredUpgrades.push(upgrades[i]);
	  }
	  return filteredUpgrades;
	};

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/**
	 * Module dependencies
	 */

	var XMLHttpRequest = __webpack_require__(9);
	var XHR = __webpack_require__(12);
	var JSONP = __webpack_require__(27);
	var websocket = __webpack_require__(28);

	/**
	 * Export transports.
	 */

	exports.polling = polling;
	exports.websocket = websocket;

	/**
	 * Polling transport polymorphic constructor.
	 * Decides on xhr vs jsonp based on feature detection.
	 *
	 * @api private
	 */

	function polling(opts){
	  var xhr;
	  var xd = false;
	  var xs = false;
	  var jsonp = false !== opts.jsonp;

	  if (global.location) {
	    var isSSL = 'https:' == location.protocol;
	    var port = location.port;

	    // some user agents have empty `location.port`
	    if (!port) {
	      port = isSSL ? 443 : 80;
	    }

	    xd = opts.hostname != location.hostname || port != opts.port;
	    xs = opts.secure != isSSL;
	  }

	  opts.xdomain = xd;
	  opts.xscheme = xs;
	  xhr = new XMLHttpRequest(opts);

	  if ('open' in xhr && !opts.forceJSONP) {
	    return new XHR(opts);
	  } else {
	    if (!jsonp) throw new Error('JSONP disabled');
	    return new JSONP(opts);
	  }
	}

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	// browser shim for xmlhttprequest module
	var hasCORS = __webpack_require__(10);

	module.exports = function(opts) {
	  var xdomain = opts.xdomain;

	  // scheme must be same when usign XDomainRequest
	  // http://blogs.msdn.com/b/ieinternals/archive/2010/05/13/xdomainrequest-restrictions-limitations-and-workarounds.aspx
	  var xscheme = opts.xscheme;

	  // XDomainRequest has a flow of not sending cookie, therefore it should be disabled as a default.
	  // https://github.com/Automattic/engine.io-client/pull/217
	  var enablesXDR = opts.enablesXDR;

	  // XMLHttpRequest can be disabled on IE
	  try {
	    if ('undefined' != typeof XMLHttpRequest && (!xdomain || hasCORS)) {
	      return new XMLHttpRequest();
	    }
	  } catch (e) { }

	  // Use XDomainRequest for IE8 if enablesXDR is true
	  // because loading bar keeps flashing when using jsonp-polling
	  // https://github.com/yujiosaka/socke.io-ie8-loading-example
	  try {
	    if ('undefined' != typeof XDomainRequest && !xscheme && enablesXDR) {
	      return new XDomainRequest();
	    }
	  } catch (e) { }

	  if (!xdomain) {
	    try {
	      return new ActiveXObject('Microsoft.XMLHTTP');
	    } catch(e) { }
	  }
	}


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	
	/**
	 * Module dependencies.
	 */

	var global = __webpack_require__(11);

	/**
	 * Module exports.
	 *
	 * Logic borrowed from Modernizr:
	 *
	 *   - https://github.com/Modernizr/Modernizr/blob/master/feature-detects/cors.js
	 */

	try {
	  module.exports = 'XMLHttpRequest' in global &&
	    'withCredentials' in new global.XMLHttpRequest();
	} catch (err) {
	  // if XMLHttp support is disabled in IE then it will throw
	  // when trying to create
	  module.exports = false;
	}


/***/ },
/* 11 */
/***/ function(module, exports) {

	
	/**
	 * Returns `this`. Execute this without a "context" (i.e. without it being
	 * attached to an object of the left-hand side), and `this` points to the
	 * "global" scope of the current JS execution.
	 */

	module.exports = (function () { return this; })();


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/**
	 * Module requirements.
	 */

	var XMLHttpRequest = __webpack_require__(9);
	var Polling = __webpack_require__(13);
	var Emitter = __webpack_require__(23);
	var inherit = __webpack_require__(25);
	var debug = __webpack_require__(26)('engine.io-client:polling-xhr');

	/**
	 * Module exports.
	 */

	module.exports = XHR;
	module.exports.Request = Request;

	/**
	 * Empty function
	 */

	function empty(){}

	/**
	 * XHR Polling constructor.
	 *
	 * @param {Object} opts
	 * @api public
	 */

	function XHR(opts){
	  Polling.call(this, opts);

	  if (global.location) {
	    var isSSL = 'https:' == location.protocol;
	    var port = location.port;

	    // some user agents have empty `location.port`
	    if (!port) {
	      port = isSSL ? 443 : 80;
	    }

	    this.xd = opts.hostname != global.location.hostname ||
	      port != opts.port;
	    this.xs = opts.secure != isSSL;
	  }
	}

	/**
	 * Inherits from Polling.
	 */

	inherit(XHR, Polling);

	/**
	 * XHR supports binary
	 */

	XHR.prototype.supportsBinary = true;

	/**
	 * Creates a request.
	 *
	 * @param {String} method
	 * @api private
	 */

	XHR.prototype.request = function(opts){
	  opts = opts || {};
	  opts.uri = this.uri();
	  opts.xd = this.xd;
	  opts.xs = this.xs;
	  opts.agent = this.agent || false;
	  opts.supportsBinary = this.supportsBinary;
	  opts.enablesXDR = this.enablesXDR;
	  return new Request(opts);
	};

	/**
	 * Sends data.
	 *
	 * @param {String} data to send.
	 * @param {Function} called upon flush.
	 * @api private
	 */

	XHR.prototype.doWrite = function(data, fn){
	  var isBinary = typeof data !== 'string' && data !== undefined;
	  var req = this.request({ method: 'POST', data: data, isBinary: isBinary });
	  var self = this;
	  req.on('success', fn);
	  req.on('error', function(err){
	    self.onError('xhr post error', err);
	  });
	  this.sendXhr = req;
	};

	/**
	 * Starts a poll cycle.
	 *
	 * @api private
	 */

	XHR.prototype.doPoll = function(){
	  debug('xhr poll');
	  var req = this.request();
	  var self = this;
	  req.on('data', function(data){
	    self.onData(data);
	  });
	  req.on('error', function(err){
	    self.onError('xhr poll error', err);
	  });
	  this.pollXhr = req;
	};

	/**
	 * Request constructor
	 *
	 * @param {Object} options
	 * @api public
	 */

	function Request(opts){
	  this.method = opts.method || 'GET';
	  this.uri = opts.uri;
	  this.xd = !!opts.xd;
	  this.xs = !!opts.xs;
	  this.async = false !== opts.async;
	  this.data = undefined != opts.data ? opts.data : null;
	  this.agent = opts.agent;
	  this.isBinary = opts.isBinary;
	  this.supportsBinary = opts.supportsBinary;
	  this.enablesXDR = opts.enablesXDR;
	  this.create();
	}

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Request.prototype);

	/**
	 * Creates the XHR object and sends the request.
	 *
	 * @api private
	 */

	Request.prototype.create = function(){
	  var xhr = this.xhr = new XMLHttpRequest({ agent: this.agent, xdomain: this.xd, xscheme: this.xs, enablesXDR: this.enablesXDR });
	  var self = this;

	  try {
	    debug('xhr open %s: %s', this.method, this.uri);
	    xhr.open(this.method, this.uri, this.async);
	    if (this.supportsBinary) {
	      // This has to be done after open because Firefox is stupid
	      // http://stackoverflow.com/questions/13216903/get-binary-data-with-xmlhttprequest-in-a-firefox-extension
	      xhr.responseType = 'arraybuffer';
	    }

	    if ('POST' == this.method) {
	      try {
	        if (this.isBinary) {
	          xhr.setRequestHeader('Content-type', 'application/octet-stream');
	        } else {
	          xhr.setRequestHeader('Content-type', 'text/plain;charset=UTF-8');
	        }
	      } catch (e) {}
	    }

	    // ie6 check
	    if ('withCredentials' in xhr) {
	      xhr.withCredentials = true;
	    }

	    if (this.hasXDR()) {
	      xhr.onload = function(){
	        self.onLoad();
	      };
	      xhr.onerror = function(){
	        self.onError(xhr.responseText);
	      };
	    } else {
	      xhr.onreadystatechange = function(){
	        if (4 != xhr.readyState) return;
	        if (200 == xhr.status || 1223 == xhr.status) {
	          self.onLoad();
	        } else {
	          // make sure the `error` event handler that's user-set
	          // does not throw in the same tick and gets caught here
	          setTimeout(function(){
	            self.onError(xhr.status);
	          }, 0);
	        }
	      };
	    }

	    debug('xhr data %s', this.data);
	    xhr.send(this.data);
	  } catch (e) {
	    // Need to defer since .create() is called directly fhrom the constructor
	    // and thus the 'error' event can only be only bound *after* this exception
	    // occurs.  Therefore, also, we cannot throw here at all.
	    setTimeout(function() {
	      self.onError(e);
	    }, 0);
	    return;
	  }

	  if (global.document) {
	    this.index = Request.requestsCount++;
	    Request.requests[this.index] = this;
	  }
	};

	/**
	 * Called upon successful response.
	 *
	 * @api private
	 */

	Request.prototype.onSuccess = function(){
	  this.emit('success');
	  this.cleanup();
	};

	/**
	 * Called if we have data.
	 *
	 * @api private
	 */

	Request.prototype.onData = function(data){
	  this.emit('data', data);
	  this.onSuccess();
	};

	/**
	 * Called upon error.
	 *
	 * @api private
	 */

	Request.prototype.onError = function(err){
	  this.emit('error', err);
	  this.cleanup();
	};

	/**
	 * Cleans up house.
	 *
	 * @api private
	 */

	Request.prototype.cleanup = function(){
	  if ('undefined' == typeof this.xhr || null === this.xhr) {
	    return;
	  }
	  // xmlhttprequest
	  if (this.hasXDR()) {
	    this.xhr.onload = this.xhr.onerror = empty;
	  } else {
	    this.xhr.onreadystatechange = empty;
	  }

	  try {
	    this.xhr.abort();
	  } catch(e) {}

	  if (global.document) {
	    delete Request.requests[this.index];
	  }

	  this.xhr = null;
	};

	/**
	 * Called upon load.
	 *
	 * @api private
	 */

	Request.prototype.onLoad = function(){
	  var data;
	  try {
	    var contentType;
	    try {
	      contentType = this.xhr.getResponseHeader('Content-Type').split(';')[0];
	    } catch (e) {}
	    if (contentType === 'application/octet-stream') {
	      data = this.xhr.response;
	    } else {
	      if (!this.supportsBinary) {
	        data = this.xhr.responseText;
	      } else {
	        data = 'ok';
	      }
	    }
	  } catch (e) {
	    this.onError(e);
	  }
	  if (null != data) {
	    this.onData(data);
	  }
	};

	/**
	 * Check if it has XDomainRequest.
	 *
	 * @api private
	 */

	Request.prototype.hasXDR = function(){
	  return 'undefined' !== typeof global.XDomainRequest && !this.xs && this.enablesXDR;
	};

	/**
	 * Aborts the request.
	 *
	 * @api public
	 */

	Request.prototype.abort = function(){
	  this.cleanup();
	};

	/**
	 * Aborts pending requests when unloading the window. This is needed to prevent
	 * memory leaks (e.g. when using IE) and to ensure that no spurious error is
	 * emitted.
	 */

	if (global.document) {
	  Request.requestsCount = 0;
	  Request.requests = {};
	  if (global.attachEvent) {
	    global.attachEvent('onunload', unloadHandler);
	  } else if (global.addEventListener) {
	    global.addEventListener('beforeunload', unloadHandler);
	  }
	}

	function unloadHandler() {
	  for (var i in Request.requests) {
	    if (Request.requests.hasOwnProperty(i)) {
	      Request.requests[i].abort();
	    }
	  }
	}

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Module dependencies.
	 */

	var Transport = __webpack_require__(14);
	var parseqs = __webpack_require__(24);
	var parser = __webpack_require__(15);
	var inherit = __webpack_require__(25);
	var debug = __webpack_require__(26)('engine.io-client:polling');

	/**
	 * Module exports.
	 */

	module.exports = Polling;

	/**
	 * Is XHR2 supported?
	 */

	var hasXHR2 = (function() {
	  var XMLHttpRequest = __webpack_require__(9);
	  var xhr = new XMLHttpRequest({ xdomain: false });
	  return null != xhr.responseType;
	})();

	/**
	 * Polling interface.
	 *
	 * @param {Object} opts
	 * @api private
	 */

	function Polling(opts){
	  var forceBase64 = (opts && opts.forceBase64);
	  if (!hasXHR2 || forceBase64) {
	    this.supportsBinary = false;
	  }
	  Transport.call(this, opts);
	}

	/**
	 * Inherits from Transport.
	 */

	inherit(Polling, Transport);

	/**
	 * Transport name.
	 */

	Polling.prototype.name = 'polling';

	/**
	 * Opens the socket (triggers polling). We write a PING message to determine
	 * when the transport is open.
	 *
	 * @api private
	 */

	Polling.prototype.doOpen = function(){
	  this.poll();
	};

	/**
	 * Pauses polling.
	 *
	 * @param {Function} callback upon buffers are flushed and transport is paused
	 * @api private
	 */

	Polling.prototype.pause = function(onPause){
	  var pending = 0;
	  var self = this;

	  this.readyState = 'pausing';

	  function pause(){
	    debug('paused');
	    self.readyState = 'paused';
	    onPause();
	  }

	  if (this.polling || !this.writable) {
	    var total = 0;

	    if (this.polling) {
	      debug('we are currently polling - waiting to pause');
	      total++;
	      this.once('pollComplete', function(){
	        debug('pre-pause polling complete');
	        --total || pause();
	      });
	    }

	    if (!this.writable) {
	      debug('we are currently writing - waiting to pause');
	      total++;
	      this.once('drain', function(){
	        debug('pre-pause writing complete');
	        --total || pause();
	      });
	    }
	  } else {
	    pause();
	  }
	};

	/**
	 * Starts polling cycle.
	 *
	 * @api public
	 */

	Polling.prototype.poll = function(){
	  debug('polling');
	  this.polling = true;
	  this.doPoll();
	  this.emit('poll');
	};

	/**
	 * Overloads onData to detect payloads.
	 *
	 * @api private
	 */

	Polling.prototype.onData = function(data){
	  var self = this;
	  debug('polling got data %s', data);
	  var callback = function(packet, index, total) {
	    // if its the first message we consider the transport open
	    if ('opening' == self.readyState) {
	      self.onOpen();
	    }

	    // if its a close packet, we close the ongoing requests
	    if ('close' == packet.type) {
	      self.onClose();
	      return false;
	    }

	    // otherwise bypass onData and handle the message
	    self.onPacket(packet);
	  };

	  // decode payload
	  parser.decodePayload(data, this.socket.binaryType, callback);

	  // if an event did not trigger closing
	  if ('closed' != this.readyState) {
	    // if we got data we're not polling
	    this.polling = false;
	    this.emit('pollComplete');

	    if ('open' == this.readyState) {
	      this.poll();
	    } else {
	      debug('ignoring poll - transport state "%s"', this.readyState);
	    }
	  }
	};

	/**
	 * For polling, send a close packet.
	 *
	 * @api private
	 */

	Polling.prototype.doClose = function(){
	  var self = this;

	  function close(){
	    debug('writing close packet');
	    self.write([{ type: 'close' }]);
	  }

	  if ('open' == this.readyState) {
	    debug('transport open - closing');
	    close();
	  } else {
	    // in case we're trying to close while
	    // handshaking is in progress (GH-164)
	    debug('transport not open - deferring close');
	    this.once('open', close);
	  }
	};

	/**
	 * Writes a packets payload.
	 *
	 * @param {Array} data packets
	 * @param {Function} drain callback
	 * @api private
	 */

	Polling.prototype.write = function(packets){
	  var self = this;
	  this.writable = false;
	  var callbackfn = function() {
	    self.writable = true;
	    self.emit('drain');
	  };

	  var self = this;
	  parser.encodePayload(packets, this.supportsBinary, function(data) {
	    self.doWrite(data, callbackfn);
	  });
	};

	/**
	 * Generates uri for connection.
	 *
	 * @api private
	 */

	Polling.prototype.uri = function(){
	  var query = this.query || {};
	  var schema = this.secure ? 'https' : 'http';
	  var port = '';

	  // cache busting is forced
	  if (false !== this.timestampRequests) {
	    query[this.timestampParam] = +new Date + '-' + Transport.timestamps++;
	  }

	  if (!this.supportsBinary && !query.sid) {
	    query.b64 = 1;
	  }

	  query = parseqs.encode(query);

	  // avoid port if default for schema
	  if (this.port && (('https' == schema && this.port != 443) ||
	     ('http' == schema && this.port != 80))) {
	    port = ':' + this.port;
	  }

	  // prepend ? to query
	  if (query.length) {
	    query = '?' + query;
	  }

	  return schema + '://' + this.hostname + port + this.path + query;
	};


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Module dependencies.
	 */

	var parser = __webpack_require__(15);
	var Emitter = __webpack_require__(23);

	/**
	 * Module exports.
	 */

	module.exports = Transport;

	/**
	 * Transport abstract constructor.
	 *
	 * @param {Object} options.
	 * @api private
	 */

	function Transport (opts) {
	  this.path = opts.path;
	  this.hostname = opts.hostname;
	  this.port = opts.port;
	  this.secure = opts.secure;
	  this.query = opts.query;
	  this.timestampParam = opts.timestampParam;
	  this.timestampRequests = opts.timestampRequests;
	  this.readyState = '';
	  this.agent = opts.agent || false;
	  this.socket = opts.socket;
	  this.enablesXDR = opts.enablesXDR;
	}

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Transport.prototype);

	/**
	 * A counter used to prevent collisions in the timestamps used
	 * for cache busting.
	 */

	Transport.timestamps = 0;

	/**
	 * Emits an error.
	 *
	 * @param {String} str
	 * @return {Transport} for chaining
	 * @api public
	 */

	Transport.prototype.onError = function (msg, desc) {
	  var err = new Error(msg);
	  err.type = 'TransportError';
	  err.description = desc;
	  this.emit('error', err);
	  return this;
	};

	/**
	 * Opens the transport.
	 *
	 * @api public
	 */

	Transport.prototype.open = function () {
	  if ('closed' == this.readyState || '' == this.readyState) {
	    this.readyState = 'opening';
	    this.doOpen();
	  }

	  return this;
	};

	/**
	 * Closes the transport.
	 *
	 * @api private
	 */

	Transport.prototype.close = function () {
	  if ('opening' == this.readyState || 'open' == this.readyState) {
	    this.doClose();
	    this.onClose();
	  }

	  return this;
	};

	/**
	 * Sends multiple packets.
	 *
	 * @param {Array} packets
	 * @api private
	 */

	Transport.prototype.send = function(packets){
	  if ('open' == this.readyState) {
	    this.write(packets);
	  } else {
	    throw new Error('Transport not open');
	  }
	};

	/**
	 * Called upon open
	 *
	 * @api private
	 */

	Transport.prototype.onOpen = function () {
	  this.readyState = 'open';
	  this.writable = true;
	  this.emit('open');
	};

	/**
	 * Called with data.
	 *
	 * @param {String} data
	 * @api private
	 */

	Transport.prototype.onData = function(data){
	  var packet = parser.decodePacket(data, this.socket.binaryType);
	  this.onPacket(packet);
	};

	/**
	 * Called with a decoded packet.
	 */

	Transport.prototype.onPacket = function (packet) {
	  this.emit('packet', packet);
	};

	/**
	 * Called upon close.
	 *
	 * @api private
	 */

	Transport.prototype.onClose = function () {
	  this.readyState = 'closed';
	  this.emit('close');
	};


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/**
	 * Module dependencies.
	 */

	var keys = __webpack_require__(16);
	var sliceBuffer = __webpack_require__(17);
	var base64encoder = __webpack_require__(18);
	var after = __webpack_require__(19);
	var utf8 = __webpack_require__(20);

	/**
	 * Check if we are running an android browser. That requires us to use
	 * ArrayBuffer with polling transports...
	 *
	 * http://ghinda.net/jpeg-blob-ajax-android/
	 */

	var isAndroid = navigator.userAgent.match(/Android/i);

	/**
	 * Current protocol version.
	 */

	exports.protocol = 3;

	/**
	 * Packet types.
	 */

	var packets = exports.packets = {
	    open:     0    // non-ws
	  , close:    1    // non-ws
	  , ping:     2
	  , pong:     3
	  , message:  4
	  , upgrade:  5
	  , noop:     6
	};

	var packetslist = keys(packets);

	/**
	 * Premade error packet.
	 */

	var err = { type: 'error', data: 'parser error' };

	/**
	 * Create a blob api even for blob builder when vendor prefixes exist
	 */

	var Blob = __webpack_require__(22);

	/**
	 * Encodes a packet.
	 *
	 *     <packet type id> [ <data> ]
	 *
	 * Example:
	 *
	 *     5hello world
	 *     3
	 *     4
	 *
	 * Binary is encoded in an identical principle
	 *
	 * @api private
	 */

	exports.encodePacket = function (packet, supportsBinary, utf8encode, callback) {
	  if ('function' == typeof supportsBinary) {
	    callback = supportsBinary;
	    supportsBinary = false;
	  }

	  if ('function' == typeof utf8encode) {
	    callback = utf8encode;
	    utf8encode = null;
	  }

	  var data = (packet.data === undefined)
	    ? undefined
	    : packet.data.buffer || packet.data;

	  if (global.ArrayBuffer && data instanceof ArrayBuffer) {
	    return encodeArrayBuffer(packet, supportsBinary, callback);
	  } else if (Blob && data instanceof global.Blob) {
	    return encodeBlob(packet, supportsBinary, callback);
	  }

	  // Sending data as a utf-8 string
	  var encoded = packets[packet.type];

	  // data fragment is optional
	  if (undefined !== packet.data) {
	    encoded += utf8encode ? utf8.encode(String(packet.data)) : String(packet.data);
	  }

	  return callback('' + encoded);

	};

	/**
	 * Encode packet helpers for binary types
	 */

	function encodeArrayBuffer(packet, supportsBinary, callback) {
	  if (!supportsBinary) {
	    return exports.encodeBase64Packet(packet, callback);
	  }

	  var data = packet.data;
	  var contentArray = new Uint8Array(data);
	  var resultBuffer = new Uint8Array(1 + data.byteLength);

	  resultBuffer[0] = packets[packet.type];
	  for (var i = 0; i < contentArray.length; i++) {
	    resultBuffer[i+1] = contentArray[i];
	  }

	  return callback(resultBuffer.buffer);
	}

	function encodeBlobAsArrayBuffer(packet, supportsBinary, callback) {
	  if (!supportsBinary) {
	    return exports.encodeBase64Packet(packet, callback);
	  }

	  var fr = new FileReader();
	  fr.onload = function() {
	    packet.data = fr.result;
	    exports.encodePacket(packet, supportsBinary, true, callback);
	  };
	  return fr.readAsArrayBuffer(packet.data);
	}

	function encodeBlob(packet, supportsBinary, callback) {
	  if (!supportsBinary) {
	    return exports.encodeBase64Packet(packet, callback);
	  }

	  if (isAndroid) {
	    return encodeBlobAsArrayBuffer(packet, supportsBinary, callback);
	  }

	  var length = new Uint8Array(1);
	  length[0] = packets[packet.type];
	  var blob = new Blob([length.buffer, packet.data]);

	  return callback(blob);
	}

	/**
	 * Encodes a packet with binary data in a base64 string
	 *
	 * @param {Object} packet, has `type` and `data`
	 * @return {String} base64 encoded message
	 */

	exports.encodeBase64Packet = function(packet, callback) {
	  var message = 'b' + exports.packets[packet.type];
	  if (Blob && packet.data instanceof Blob) {
	    var fr = new FileReader();
	    fr.onload = function() {
	      var b64 = fr.result.split(',')[1];
	      callback(message + b64);
	    };
	    return fr.readAsDataURL(packet.data);
	  }

	  var b64data;
	  try {
	    b64data = String.fromCharCode.apply(null, new Uint8Array(packet.data));
	  } catch (e) {
	    // iPhone Safari doesn't let you apply with typed arrays
	    var typed = new Uint8Array(packet.data);
	    var basic = new Array(typed.length);
	    for (var i = 0; i < typed.length; i++) {
	      basic[i] = typed[i];
	    }
	    b64data = String.fromCharCode.apply(null, basic);
	  }
	  message += global.btoa(b64data);
	  return callback(message);
	};

	/**
	 * Decodes a packet. Changes format to Blob if requested.
	 *
	 * @return {Object} with `type` and `data` (if any)
	 * @api private
	 */

	exports.decodePacket = function (data, binaryType, utf8decode) {
	  // String data
	  if (typeof data == 'string' || data === undefined) {
	    if (data.charAt(0) == 'b') {
	      return exports.decodeBase64Packet(data.substr(1), binaryType);
	    }

	    if (utf8decode) {
	      try {
	        data = utf8.decode(data);
	      } catch (e) {
	        return err;
	      }
	    }
	    var type = data.charAt(0);

	    if (Number(type) != type || !packetslist[type]) {
	      return err;
	    }

	    if (data.length > 1) {
	      return { type: packetslist[type], data: data.substring(1) };
	    } else {
	      return { type: packetslist[type] };
	    }
	  }

	  var asArray = new Uint8Array(data);
	  var type = asArray[0];
	  var rest = sliceBuffer(data, 1);
	  if (Blob && binaryType === 'blob') {
	    rest = new Blob([rest]);
	  }
	  return { type: packetslist[type], data: rest };
	};

	/**
	 * Decodes a packet encoded in a base64 string
	 *
	 * @param {String} base64 encoded message
	 * @return {Object} with `type` and `data` (if any)
	 */

	exports.decodeBase64Packet = function(msg, binaryType) {
	  var type = packetslist[msg.charAt(0)];
	  if (!global.ArrayBuffer) {
	    return { type: type, data: { base64: true, data: msg.substr(1) } };
	  }

	  var data = base64encoder.decode(msg.substr(1));

	  if (binaryType === 'blob' && Blob) {
	    data = new Blob([data]);
	  }

	  return { type: type, data: data };
	};

	/**
	 * Encodes multiple messages (payload).
	 *
	 *     <length>:data
	 *
	 * Example:
	 *
	 *     11:hello world2:hi
	 *
	 * If any contents are binary, they will be encoded as base64 strings. Base64
	 * encoded strings are marked with a b before the length specifier
	 *
	 * @param {Array} packets
	 * @api private
	 */

	exports.encodePayload = function (packets, supportsBinary, callback) {
	  if (typeof supportsBinary == 'function') {
	    callback = supportsBinary;
	    supportsBinary = null;
	  }

	  if (supportsBinary) {
	    if (Blob && !isAndroid) {
	      return exports.encodePayloadAsBlob(packets, callback);
	    }

	    return exports.encodePayloadAsArrayBuffer(packets, callback);
	  }

	  if (!packets.length) {
	    return callback('0:');
	  }

	  function setLengthHeader(message) {
	    return message.length + ':' + message;
	  }

	  function encodeOne(packet, doneCallback) {
	    exports.encodePacket(packet, supportsBinary, true, function(message) {
	      doneCallback(null, setLengthHeader(message));
	    });
	  }

	  map(packets, encodeOne, function(err, results) {
	    return callback(results.join(''));
	  });
	};

	/**
	 * Async array map using after
	 */

	function map(ary, each, done) {
	  var result = new Array(ary.length);
	  var next = after(ary.length, done);

	  var eachWithIndex = function(i, el, cb) {
	    each(el, function(error, msg) {
	      result[i] = msg;
	      cb(error, result);
	    });
	  };

	  for (var i = 0; i < ary.length; i++) {
	    eachWithIndex(i, ary[i], next);
	  }
	}

	/*
	 * Decodes data when a payload is maybe expected. Possible binary contents are
	 * decoded from their base64 representation
	 *
	 * @param {String} data, callback method
	 * @api public
	 */

	exports.decodePayload = function (data, binaryType, callback) {
	  if (typeof data != 'string') {
	    return exports.decodePayloadAsBinary(data, binaryType, callback);
	  }

	  if (typeof binaryType === 'function') {
	    callback = binaryType;
	    binaryType = null;
	  }

	  var packet;
	  if (data == '') {
	    // parser error - ignoring payload
	    return callback(err, 0, 1);
	  }

	  var length = ''
	    , n, msg;

	  for (var i = 0, l = data.length; i < l; i++) {
	    var chr = data.charAt(i);

	    if (':' != chr) {
	      length += chr;
	    } else {
	      if ('' == length || (length != (n = Number(length)))) {
	        // parser error - ignoring payload
	        return callback(err, 0, 1);
	      }

	      msg = data.substr(i + 1, n);

	      if (length != msg.length) {
	        // parser error - ignoring payload
	        return callback(err, 0, 1);
	      }

	      if (msg.length) {
	        packet = exports.decodePacket(msg, binaryType, true);

	        if (err.type == packet.type && err.data == packet.data) {
	          // parser error in individual packet - ignoring payload
	          return callback(err, 0, 1);
	        }

	        var ret = callback(packet, i + n, l);
	        if (false === ret) return;
	      }

	      // advance cursor
	      i += n;
	      length = '';
	    }
	  }

	  if (length != '') {
	    // parser error - ignoring payload
	    return callback(err, 0, 1);
	  }

	};

	/**
	 * Encodes multiple messages (payload) as binary.
	 *
	 * <1 = binary, 0 = string><number from 0-9><number from 0-9>[...]<number
	 * 255><data>
	 *
	 * Example:
	 * 1 3 255 1 2 3, if the binary contents are interpreted as 8 bit integers
	 *
	 * @param {Array} packets
	 * @return {ArrayBuffer} encoded payload
	 * @api private
	 */

	exports.encodePayloadAsArrayBuffer = function(packets, callback) {
	  if (!packets.length) {
	    return callback(new ArrayBuffer(0));
	  }

	  function encodeOne(packet, doneCallback) {
	    exports.encodePacket(packet, true, true, function(data) {
	      return doneCallback(null, data);
	    });
	  }

	  map(packets, encodeOne, function(err, encodedPackets) {
	    var totalLength = encodedPackets.reduce(function(acc, p) {
	      var len;
	      if (typeof p === 'string'){
	        len = p.length;
	      } else {
	        len = p.byteLength;
	      }
	      return acc + len.toString().length + len + 2; // string/binary identifier + separator = 2
	    }, 0);

	    var resultArray = new Uint8Array(totalLength);

	    var bufferIndex = 0;
	    encodedPackets.forEach(function(p) {
	      var isString = typeof p === 'string';
	      var ab = p;
	      if (isString) {
	        var view = new Uint8Array(p.length);
	        for (var i = 0; i < p.length; i++) {
	          view[i] = p.charCodeAt(i);
	        }
	        ab = view.buffer;
	      }

	      if (isString) { // not true binary
	        resultArray[bufferIndex++] = 0;
	      } else { // true binary
	        resultArray[bufferIndex++] = 1;
	      }

	      var lenStr = ab.byteLength.toString();
	      for (var i = 0; i < lenStr.length; i++) {
	        resultArray[bufferIndex++] = parseInt(lenStr[i]);
	      }
	      resultArray[bufferIndex++] = 255;

	      var view = new Uint8Array(ab);
	      for (var i = 0; i < view.length; i++) {
	        resultArray[bufferIndex++] = view[i];
	      }
	    });

	    return callback(resultArray.buffer);
	  });
	};

	/**
	 * Encode as Blob
	 */

	exports.encodePayloadAsBlob = function(packets, callback) {
	  function encodeOne(packet, doneCallback) {
	    exports.encodePacket(packet, true, true, function(encoded) {
	      var binaryIdentifier = new Uint8Array(1);
	      binaryIdentifier[0] = 1;
	      if (typeof encoded === 'string') {
	        var view = new Uint8Array(encoded.length);
	        for (var i = 0; i < encoded.length; i++) {
	          view[i] = encoded.charCodeAt(i);
	        }
	        encoded = view.buffer;
	        binaryIdentifier[0] = 0;
	      }

	      var len = (encoded instanceof ArrayBuffer)
	        ? encoded.byteLength
	        : encoded.size;

	      var lenStr = len.toString();
	      var lengthAry = new Uint8Array(lenStr.length + 1);
	      for (var i = 0; i < lenStr.length; i++) {
	        lengthAry[i] = parseInt(lenStr[i]);
	      }
	      lengthAry[lenStr.length] = 255;

	      if (Blob) {
	        var blob = new Blob([binaryIdentifier.buffer, lengthAry.buffer, encoded]);
	        doneCallback(null, blob);
	      }
	    });
	  }

	  map(packets, encodeOne, function(err, results) {
	    return callback(new Blob(results));
	  });
	};

	/*
	 * Decodes data when a payload is maybe expected. Strings are decoded by
	 * interpreting each byte as a key code for entries marked to start with 0. See
	 * description of encodePayloadAsBinary
	 *
	 * @param {ArrayBuffer} data, callback method
	 * @api public
	 */

	exports.decodePayloadAsBinary = function (data, binaryType, callback) {
	  if (typeof binaryType === 'function') {
	    callback = binaryType;
	    binaryType = null;
	  }

	  var bufferTail = data;
	  var buffers = [];

	  var numberTooLong = false;
	  while (bufferTail.byteLength > 0) {
	    var tailArray = new Uint8Array(bufferTail);
	    var isString = tailArray[0] === 0;
	    var msgLength = '';

	    for (var i = 1; ; i++) {
	      if (tailArray[i] == 255) break;

	      if (msgLength.length > 310) {
	        numberTooLong = true;
	        break;
	      }

	      msgLength += tailArray[i];
	    }

	    if(numberTooLong) return callback(err, 0, 1);

	    bufferTail = sliceBuffer(bufferTail, 2 + msgLength.length);
	    msgLength = parseInt(msgLength);

	    var msg = sliceBuffer(bufferTail, 0, msgLength);
	    if (isString) {
	      try {
	        msg = String.fromCharCode.apply(null, new Uint8Array(msg));
	      } catch (e) {
	        // iPhone Safari doesn't let you apply to typed arrays
	        var typed = new Uint8Array(msg);
	        msg = '';
	        for (var i = 0; i < typed.length; i++) {
	          msg += String.fromCharCode(typed[i]);
	        }
	      }
	    }

	    buffers.push(msg);
	    bufferTail = sliceBuffer(bufferTail, msgLength);
	  }

	  var total = buffers.length;
	  buffers.forEach(function(buffer, i) {
	    callback(exports.decodePacket(buffer, binaryType, true), i, total);
	  });
	};

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 16 */
/***/ function(module, exports) {

	
	/**
	 * Gets the keys for an object.
	 *
	 * @return {Array} keys
	 * @api private
	 */

	module.exports = Object.keys || function keys (obj){
	  var arr = [];
	  var has = Object.prototype.hasOwnProperty;

	  for (var i in obj) {
	    if (has.call(obj, i)) {
	      arr.push(i);
	    }
	  }
	  return arr;
	};


/***/ },
/* 17 */
/***/ function(module, exports) {

	/**
	 * An abstraction for slicing an arraybuffer even when
	 * ArrayBuffer.prototype.slice is not supported
	 *
	 * @api public
	 */

	module.exports = function(arraybuffer, start, end) {
	  var bytes = arraybuffer.byteLength;
	  start = start || 0;
	  end = end || bytes;

	  if (arraybuffer.slice) { return arraybuffer.slice(start, end); }

	  if (start < 0) { start += bytes; }
	  if (end < 0) { end += bytes; }
	  if (end > bytes) { end = bytes; }

	  if (start >= bytes || start >= end || bytes === 0) {
	    return new ArrayBuffer(0);
	  }

	  var abv = new Uint8Array(arraybuffer);
	  var result = new Uint8Array(end - start);
	  for (var i = start, ii = 0; i < end; i++, ii++) {
	    result[ii] = abv[i];
	  }
	  return result.buffer;
	};


/***/ },
/* 18 */
/***/ function(module, exports) {

	/*
	 * base64-arraybuffer
	 * https://github.com/niklasvh/base64-arraybuffer
	 *
	 * Copyright (c) 2012 Niklas von Hertzen
	 * Licensed under the MIT license.
	 */
	(function(chars){
	  "use strict";

	  exports.encode = function(arraybuffer) {
	    var bytes = new Uint8Array(arraybuffer),
	    i, len = bytes.length, base64 = "";

	    for (i = 0; i < len; i+=3) {
	      base64 += chars[bytes[i] >> 2];
	      base64 += chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)];
	      base64 += chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)];
	      base64 += chars[bytes[i + 2] & 63];
	    }

	    if ((len % 3) === 2) {
	      base64 = base64.substring(0, base64.length - 1) + "=";
	    } else if (len % 3 === 1) {
	      base64 = base64.substring(0, base64.length - 2) + "==";
	    }

	    return base64;
	  };

	  exports.decode =  function(base64) {
	    var bufferLength = base64.length * 0.75,
	    len = base64.length, i, p = 0,
	    encoded1, encoded2, encoded3, encoded4;

	    if (base64[base64.length - 1] === "=") {
	      bufferLength--;
	      if (base64[base64.length - 2] === "=") {
	        bufferLength--;
	      }
	    }

	    var arraybuffer = new ArrayBuffer(bufferLength),
	    bytes = new Uint8Array(arraybuffer);

	    for (i = 0; i < len; i+=4) {
	      encoded1 = chars.indexOf(base64[i]);
	      encoded2 = chars.indexOf(base64[i+1]);
	      encoded3 = chars.indexOf(base64[i+2]);
	      encoded4 = chars.indexOf(base64[i+3]);

	      bytes[p++] = (encoded1 << 2) | (encoded2 >> 4);
	      bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2);
	      bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63);
	    }

	    return arraybuffer;
	  };
	})("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");


/***/ },
/* 19 */
/***/ function(module, exports) {

	module.exports = after

	function after(count, callback, err_cb) {
	    var bail = false
	    err_cb = err_cb || noop
	    proxy.count = count

	    return (count === 0) ? callback() : proxy

	    function proxy(err, result) {
	        if (proxy.count <= 0) {
	            throw new Error('after called too many times')
	        }
	        --proxy.count

	        // after first error, rest are passed to err_cb
	        if (err) {
	            bail = true
	            callback(err)
	            // future error callbacks will go to error handler
	            callback = err_cb
	        } else if (proxy.count === 0 && !bail) {
	            callback(null, result)
	        }
	    }
	}

	function noop() {}


/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module, global) {/*! http://mths.be/utf8js v2.0.0 by @mathias */
	;(function(root) {

		// Detect free variables `exports`
		var freeExports = typeof exports == 'object' && exports;

		// Detect free variable `module`
		var freeModule = typeof module == 'object' && module &&
			module.exports == freeExports && module;

		// Detect free variable `global`, from Node.js or Browserified code,
		// and use it as `root`
		var freeGlobal = typeof global == 'object' && global;
		if (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal) {
			root = freeGlobal;
		}

		/*--------------------------------------------------------------------------*/

		var stringFromCharCode = String.fromCharCode;

		// Taken from http://mths.be/punycode
		function ucs2decode(string) {
			var output = [];
			var counter = 0;
			var length = string.length;
			var value;
			var extra;
			while (counter < length) {
				value = string.charCodeAt(counter++);
				if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
					// high surrogate, and there is a next character
					extra = string.charCodeAt(counter++);
					if ((extra & 0xFC00) == 0xDC00) { // low surrogate
						output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
					} else {
						// unmatched surrogate; only append this code unit, in case the next
						// code unit is the high surrogate of a surrogate pair
						output.push(value);
						counter--;
					}
				} else {
					output.push(value);
				}
			}
			return output;
		}

		// Taken from http://mths.be/punycode
		function ucs2encode(array) {
			var length = array.length;
			var index = -1;
			var value;
			var output = '';
			while (++index < length) {
				value = array[index];
				if (value > 0xFFFF) {
					value -= 0x10000;
					output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
					value = 0xDC00 | value & 0x3FF;
				}
				output += stringFromCharCode(value);
			}
			return output;
		}

		/*--------------------------------------------------------------------------*/

		function createByte(codePoint, shift) {
			return stringFromCharCode(((codePoint >> shift) & 0x3F) | 0x80);
		}

		function encodeCodePoint(codePoint) {
			if ((codePoint & 0xFFFFFF80) == 0) { // 1-byte sequence
				return stringFromCharCode(codePoint);
			}
			var symbol = '';
			if ((codePoint & 0xFFFFF800) == 0) { // 2-byte sequence
				symbol = stringFromCharCode(((codePoint >> 6) & 0x1F) | 0xC0);
			}
			else if ((codePoint & 0xFFFF0000) == 0) { // 3-byte sequence
				symbol = stringFromCharCode(((codePoint >> 12) & 0x0F) | 0xE0);
				symbol += createByte(codePoint, 6);
			}
			else if ((codePoint & 0xFFE00000) == 0) { // 4-byte sequence
				symbol = stringFromCharCode(((codePoint >> 18) & 0x07) | 0xF0);
				symbol += createByte(codePoint, 12);
				symbol += createByte(codePoint, 6);
			}
			symbol += stringFromCharCode((codePoint & 0x3F) | 0x80);
			return symbol;
		}

		function utf8encode(string) {
			var codePoints = ucs2decode(string);

			// console.log(JSON.stringify(codePoints.map(function(x) {
			// 	return 'U+' + x.toString(16).toUpperCase();
			// })));

			var length = codePoints.length;
			var index = -1;
			var codePoint;
			var byteString = '';
			while (++index < length) {
				codePoint = codePoints[index];
				byteString += encodeCodePoint(codePoint);
			}
			return byteString;
		}

		/*--------------------------------------------------------------------------*/

		function readContinuationByte() {
			if (byteIndex >= byteCount) {
				throw Error('Invalid byte index');
			}

			var continuationByte = byteArray[byteIndex] & 0xFF;
			byteIndex++;

			if ((continuationByte & 0xC0) == 0x80) {
				return continuationByte & 0x3F;
			}

			// If we end up here, it’s not a continuation byte
			throw Error('Invalid continuation byte');
		}

		function decodeSymbol() {
			var byte1;
			var byte2;
			var byte3;
			var byte4;
			var codePoint;

			if (byteIndex > byteCount) {
				throw Error('Invalid byte index');
			}

			if (byteIndex == byteCount) {
				return false;
			}

			// Read first byte
			byte1 = byteArray[byteIndex] & 0xFF;
			byteIndex++;

			// 1-byte sequence (no continuation bytes)
			if ((byte1 & 0x80) == 0) {
				return byte1;
			}

			// 2-byte sequence
			if ((byte1 & 0xE0) == 0xC0) {
				var byte2 = readContinuationByte();
				codePoint = ((byte1 & 0x1F) << 6) | byte2;
				if (codePoint >= 0x80) {
					return codePoint;
				} else {
					throw Error('Invalid continuation byte');
				}
			}

			// 3-byte sequence (may include unpaired surrogates)
			if ((byte1 & 0xF0) == 0xE0) {
				byte2 = readContinuationByte();
				byte3 = readContinuationByte();
				codePoint = ((byte1 & 0x0F) << 12) | (byte2 << 6) | byte3;
				if (codePoint >= 0x0800) {
					return codePoint;
				} else {
					throw Error('Invalid continuation byte');
				}
			}

			// 4-byte sequence
			if ((byte1 & 0xF8) == 0xF0) {
				byte2 = readContinuationByte();
				byte3 = readContinuationByte();
				byte4 = readContinuationByte();
				codePoint = ((byte1 & 0x0F) << 0x12) | (byte2 << 0x0C) |
					(byte3 << 0x06) | byte4;
				if (codePoint >= 0x010000 && codePoint <= 0x10FFFF) {
					return codePoint;
				}
			}

			throw Error('Invalid UTF-8 detected');
		}

		var byteArray;
		var byteCount;
		var byteIndex;
		function utf8decode(byteString) {
			byteArray = ucs2decode(byteString);
			byteCount = byteArray.length;
			byteIndex = 0;
			var codePoints = [];
			var tmp;
			while ((tmp = decodeSymbol()) !== false) {
				codePoints.push(tmp);
			}
			return ucs2encode(codePoints);
		}

		/*--------------------------------------------------------------------------*/

		var utf8 = {
			'version': '2.0.0',
			'encode': utf8encode,
			'decode': utf8decode
		};

		// Some AMD build optimizers, like r.js, check for specific condition patterns
		// like the following:
		if (
			true
		) {
			!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
				return utf8;
			}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		}	else if (freeExports && !freeExports.nodeType) {
			if (freeModule) { // in Node.js or RingoJS v0.8.0+
				freeModule.exports = utf8;
			} else { // in Narwhal or RingoJS v0.7.0-
				var object = {};
				var hasOwnProperty = object.hasOwnProperty;
				for (var key in utf8) {
					hasOwnProperty.call(utf8, key) && (freeExports[key] = utf8[key]);
				}
			}
		} else { // in Rhino or a web browser
			root.utf8 = utf8;
		}

	}(this));

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(21)(module), (function() { return this; }())))

/***/ },
/* 21 */
/***/ function(module, exports) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },
/* 22 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(global) {/**
	 * Create a blob builder even when vendor prefixes exist
	 */

	var BlobBuilder = global.BlobBuilder
	  || global.WebKitBlobBuilder
	  || global.MSBlobBuilder
	  || global.MozBlobBuilder;

	/**
	 * Check if Blob constructor is supported
	 */

	var blobSupported = (function() {
	  try {
	    var b = new Blob(['hi']);
	    return b.size == 2;
	  } catch(e) {
	    return false;
	  }
	})();

	/**
	 * Check if BlobBuilder is supported
	 */

	var blobBuilderSupported = BlobBuilder
	  && BlobBuilder.prototype.append
	  && BlobBuilder.prototype.getBlob;

	function BlobBuilderConstructor(ary, options) {
	  options = options || {};

	  var bb = new BlobBuilder();
	  for (var i = 0; i < ary.length; i++) {
	    bb.append(ary[i]);
	  }
	  return (options.type) ? bb.getBlob(options.type) : bb.getBlob();
	};

	module.exports = (function() {
	  if (blobSupported) {
	    return global.Blob;
	  } else if (blobBuilderSupported) {
	    return BlobBuilderConstructor;
	  } else {
	    return undefined;
	  }
	})();

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 23 */
/***/ function(module, exports) {

	
	/**
	 * Expose `Emitter`.
	 */

	module.exports = Emitter;

	/**
	 * Initialize a new `Emitter`.
	 *
	 * @api public
	 */

	function Emitter(obj) {
	  if (obj) return mixin(obj);
	};

	/**
	 * Mixin the emitter properties.
	 *
	 * @param {Object} obj
	 * @return {Object}
	 * @api private
	 */

	function mixin(obj) {
	  for (var key in Emitter.prototype) {
	    obj[key] = Emitter.prototype[key];
	  }
	  return obj;
	}

	/**
	 * Listen on the given `event` with `fn`.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.on =
	Emitter.prototype.addEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};
	  (this._callbacks[event] = this._callbacks[event] || [])
	    .push(fn);
	  return this;
	};

	/**
	 * Adds an `event` listener that will be invoked a single
	 * time then automatically removed.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.once = function(event, fn){
	  var self = this;
	  this._callbacks = this._callbacks || {};

	  function on() {
	    self.off(event, on);
	    fn.apply(this, arguments);
	  }

	  on.fn = fn;
	  this.on(event, on);
	  return this;
	};

	/**
	 * Remove the given callback for `event` or all
	 * registered callbacks.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.off =
	Emitter.prototype.removeListener =
	Emitter.prototype.removeAllListeners =
	Emitter.prototype.removeEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};

	  // all
	  if (0 == arguments.length) {
	    this._callbacks = {};
	    return this;
	  }

	  // specific event
	  var callbacks = this._callbacks[event];
	  if (!callbacks) return this;

	  // remove all handlers
	  if (1 == arguments.length) {
	    delete this._callbacks[event];
	    return this;
	  }

	  // remove specific handler
	  var cb;
	  for (var i = 0; i < callbacks.length; i++) {
	    cb = callbacks[i];
	    if (cb === fn || cb.fn === fn) {
	      callbacks.splice(i, 1);
	      break;
	    }
	  }
	  return this;
	};

	/**
	 * Emit `event` with the given args.
	 *
	 * @param {String} event
	 * @param {Mixed} ...
	 * @return {Emitter}
	 */

	Emitter.prototype.emit = function(event){
	  this._callbacks = this._callbacks || {};
	  var args = [].slice.call(arguments, 1)
	    , callbacks = this._callbacks[event];

	  if (callbacks) {
	    callbacks = callbacks.slice(0);
	    for (var i = 0, len = callbacks.length; i < len; ++i) {
	      callbacks[i].apply(this, args);
	    }
	  }

	  return this;
	};

	/**
	 * Return array of callbacks for `event`.
	 *
	 * @param {String} event
	 * @return {Array}
	 * @api public
	 */

	Emitter.prototype.listeners = function(event){
	  this._callbacks = this._callbacks || {};
	  return this._callbacks[event] || [];
	};

	/**
	 * Check if this emitter has `event` handlers.
	 *
	 * @param {String} event
	 * @return {Boolean}
	 * @api public
	 */

	Emitter.prototype.hasListeners = function(event){
	  return !! this.listeners(event).length;
	};


/***/ },
/* 24 */
/***/ function(module, exports) {

	/**
	 * Compiles a querystring
	 * Returns string representation of the object
	 *
	 * @param {Object}
	 * @api private
	 */

	exports.encode = function (obj) {
	  var str = '';

	  for (var i in obj) {
	    if (obj.hasOwnProperty(i)) {
	      if (str.length) str += '&';
	      str += encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]);
	    }
	  }

	  return str;
	};

	/**
	 * Parses a simple querystring into an object
	 *
	 * @param {String} qs
	 * @api private
	 */

	exports.decode = function(qs){
	  var qry = {};
	  var pairs = qs.split('&');
	  for (var i = 0, l = pairs.length; i < l; i++) {
	    var pair = pairs[i].split('=');
	    qry[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
	  }
	  return qry;
	};


/***/ },
/* 25 */
/***/ function(module, exports) {

	
	module.exports = function(a, b){
	  var fn = function(){};
	  fn.prototype = b.prototype;
	  a.prototype = new fn;
	  a.prototype.constructor = a;
	};

/***/ },
/* 26 */
/***/ function(module, exports) {

	
	/**
	 * Expose `debug()` as the module.
	 */

	module.exports = debug;

	/**
	 * Create a debugger with the given `name`.
	 *
	 * @param {String} name
	 * @return {Type}
	 * @api public
	 */

	function debug(name) {
	  if (!debug.enabled(name)) return function(){};

	  return function(fmt){
	    fmt = coerce(fmt);

	    var curr = new Date;
	    var ms = curr - (debug[name] || curr);
	    debug[name] = curr;

	    fmt = name
	      + ' '
	      + fmt
	      + ' +' + debug.humanize(ms);

	    // This hackery is required for IE8
	    // where `console.log` doesn't have 'apply'
	    window.console
	      && console.log
	      && Function.prototype.apply.call(console.log, console, arguments);
	  }
	}

	/**
	 * The currently active debug mode names.
	 */

	debug.names = [];
	debug.skips = [];

	/**
	 * Enables a debug mode by name. This can include modes
	 * separated by a colon and wildcards.
	 *
	 * @param {String} name
	 * @api public
	 */

	debug.enable = function(name) {
	  try {
	    localStorage.debug = name;
	  } catch(e){}

	  var split = (name || '').split(/[\s,]+/)
	    , len = split.length;

	  for (var i = 0; i < len; i++) {
	    name = split[i].replace('*', '.*?');
	    if (name[0] === '-') {
	      debug.skips.push(new RegExp('^' + name.substr(1) + '$'));
	    }
	    else {
	      debug.names.push(new RegExp('^' + name + '$'));
	    }
	  }
	};

	/**
	 * Disable debug output.
	 *
	 * @api public
	 */

	debug.disable = function(){
	  debug.enable('');
	};

	/**
	 * Humanize the given `ms`.
	 *
	 * @param {Number} m
	 * @return {String}
	 * @api private
	 */

	debug.humanize = function(ms) {
	  var sec = 1000
	    , min = 60 * 1000
	    , hour = 60 * min;

	  if (ms >= hour) return (ms / hour).toFixed(1) + 'h';
	  if (ms >= min) return (ms / min).toFixed(1) + 'm';
	  if (ms >= sec) return (ms / sec | 0) + 's';
	  return ms + 'ms';
	};

	/**
	 * Returns true if the given mode name is enabled, false otherwise.
	 *
	 * @param {String} name
	 * @return {Boolean}
	 * @api public
	 */

	debug.enabled = function(name) {
	  for (var i = 0, len = debug.skips.length; i < len; i++) {
	    if (debug.skips[i].test(name)) {
	      return false;
	    }
	  }
	  for (var i = 0, len = debug.names.length; i < len; i++) {
	    if (debug.names[i].test(name)) {
	      return true;
	    }
	  }
	  return false;
	};

	/**
	 * Coerce `val`.
	 */

	function coerce(val) {
	  if (val instanceof Error) return val.stack || val.message;
	  return val;
	}

	// persist

	try {
	  if (window.localStorage) debug.enable(localStorage.debug);
	} catch(e){}


/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {
	/**
	 * Module requirements.
	 */

	var Polling = __webpack_require__(13);
	var inherit = __webpack_require__(25);

	/**
	 * Module exports.
	 */

	module.exports = JSONPPolling;

	/**
	 * Cached regular expressions.
	 */

	var rNewline = /\n/g;
	var rEscapedNewline = /\\n/g;

	/**
	 * Global JSONP callbacks.
	 */

	var callbacks;

	/**
	 * Callbacks count.
	 */

	var index = 0;

	/**
	 * Noop.
	 */

	function empty () { }

	/**
	 * JSONP Polling constructor.
	 *
	 * @param {Object} opts.
	 * @api public
	 */

	function JSONPPolling (opts) {
	  Polling.call(this, opts);

	  this.query = this.query || {};

	  // define global callbacks array if not present
	  // we do this here (lazily) to avoid unneeded global pollution
	  if (!callbacks) {
	    // we need to consider multiple engines in the same page
	    if (!global.___eio) global.___eio = [];
	    callbacks = global.___eio;
	  }

	  // callback identifier
	  this.index = callbacks.length;

	  // add callback to jsonp global
	  var self = this;
	  callbacks.push(function (msg) {
	    self.onData(msg);
	  });

	  // append to query string
	  this.query.j = this.index;

	  // prevent spurious errors from being emitted when the window is unloaded
	  if (global.document && global.addEventListener) {
	    global.addEventListener('beforeunload', function () {
	      if (self.script) self.script.onerror = empty;
	    });
	  }
	}

	/**
	 * Inherits from Polling.
	 */

	inherit(JSONPPolling, Polling);

	/*
	 * JSONP only supports binary as base64 encoded strings
	 */

	JSONPPolling.prototype.supportsBinary = false;

	/**
	 * Closes the socket.
	 *
	 * @api private
	 */

	JSONPPolling.prototype.doClose = function () {
	  if (this.script) {
	    this.script.parentNode.removeChild(this.script);
	    this.script = null;
	  }

	  if (this.form) {
	    this.form.parentNode.removeChild(this.form);
	    this.form = null;
	    this.iframe = null;
	  }

	  Polling.prototype.doClose.call(this);
	};

	/**
	 * Starts a poll cycle.
	 *
	 * @api private
	 */

	JSONPPolling.prototype.doPoll = function () {
	  var self = this;
	  var script = document.createElement('script');

	  if (this.script) {
	    this.script.parentNode.removeChild(this.script);
	    this.script = null;
	  }

	  script.async = true;
	  script.src = this.uri();
	  script.onerror = function(e){
	    self.onError('jsonp poll error',e);
	  };

	  var insertAt = document.getElementsByTagName('script')[0];
	  insertAt.parentNode.insertBefore(script, insertAt);
	  this.script = script;

	  var isUAgecko = 'undefined' != typeof navigator && /gecko/i.test(navigator.userAgent);
	  
	  if (isUAgecko) {
	    setTimeout(function () {
	      var iframe = document.createElement('iframe');
	      document.body.appendChild(iframe);
	      document.body.removeChild(iframe);
	    }, 100);
	  }
	};

	/**
	 * Writes with a hidden iframe.
	 *
	 * @param {String} data to send
	 * @param {Function} called upon flush.
	 * @api private
	 */

	JSONPPolling.prototype.doWrite = function (data, fn) {
	  var self = this;

	  if (!this.form) {
	    var form = document.createElement('form');
	    var area = document.createElement('textarea');
	    var id = this.iframeId = 'eio_iframe_' + this.index;
	    var iframe;

	    form.className = 'socketio';
	    form.style.position = 'absolute';
	    form.style.top = '-1000px';
	    form.style.left = '-1000px';
	    form.target = id;
	    form.method = 'POST';
	    form.setAttribute('accept-charset', 'utf-8');
	    area.name = 'd';
	    form.appendChild(area);
	    document.body.appendChild(form);

	    this.form = form;
	    this.area = area;
	  }

	  this.form.action = this.uri();

	  function complete () {
	    initIframe();
	    fn();
	  }

	  function initIframe () {
	    if (self.iframe) {
	      try {
	        self.form.removeChild(self.iframe);
	      } catch (e) {
	        self.onError('jsonp polling iframe removal error', e);
	      }
	    }

	    try {
	      // ie6 dynamic iframes with target="" support (thanks Chris Lambacher)
	      var html = '<iframe src="javascript:0" name="'+ self.iframeId +'">';
	      iframe = document.createElement(html);
	    } catch (e) {
	      iframe = document.createElement('iframe');
	      iframe.name = self.iframeId;
	      iframe.src = 'javascript:0';
	    }

	    iframe.id = self.iframeId;

	    self.form.appendChild(iframe);
	    self.iframe = iframe;
	  }

	  initIframe();

	  // escape \n to prevent it from being converted into \r\n by some UAs
	  // double escaping is required for escaped new lines because unescaping of new lines can be done safely on server-side
	  data = data.replace(rEscapedNewline, '\\\n');
	  this.area.value = data.replace(rNewline, '\\n');

	  try {
	    this.form.submit();
	  } catch(e) {}

	  if (this.iframe.attachEvent) {
	    this.iframe.onreadystatechange = function(){
	      if (self.iframe.readyState == 'complete') {
	        complete();
	      }
	    };
	  } else {
	    this.iframe.onload = complete;
	  }
	};

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Module dependencies.
	 */

	var Transport = __webpack_require__(14);
	var parser = __webpack_require__(15);
	var parseqs = __webpack_require__(24);
	var inherit = __webpack_require__(25);
	var debug = __webpack_require__(26)('engine.io-client:websocket');

	/**
	 * `ws` exposes a WebSocket-compatible interface in
	 * Node, or the `WebSocket` or `MozWebSocket` globals
	 * in the browser.
	 */

	var WebSocket = __webpack_require__(29);

	/**
	 * Module exports.
	 */

	module.exports = WS;

	/**
	 * WebSocket transport constructor.
	 *
	 * @api {Object} connection options
	 * @api public
	 */

	function WS(opts){
	  var forceBase64 = (opts && opts.forceBase64);
	  if (forceBase64) {
	    this.supportsBinary = false;
	  }
	  Transport.call(this, opts);
	}

	/**
	 * Inherits from Transport.
	 */

	inherit(WS, Transport);

	/**
	 * Transport name.
	 *
	 * @api public
	 */

	WS.prototype.name = 'websocket';

	/*
	 * WebSockets support binary
	 */

	WS.prototype.supportsBinary = true;

	/**
	 * Opens socket.
	 *
	 * @api private
	 */

	WS.prototype.doOpen = function(){
	  if (!this.check()) {
	    // let probe timeout
	    return;
	  }

	  var self = this;
	  var uri = this.uri();
	  var protocols = void(0);
	  var opts = { agent: this.agent };

	  this.ws = new WebSocket(uri, protocols, opts);

	  if (this.ws.binaryType === undefined) {
	    this.supportsBinary = false;
	  }

	  this.ws.binaryType = 'arraybuffer';
	  this.addEventListeners();
	};

	/**
	 * Adds event listeners to the socket
	 *
	 * @api private
	 */

	WS.prototype.addEventListeners = function(){
	  var self = this;

	  this.ws.onopen = function(){
	    self.onOpen();
	  };
	  this.ws.onclose = function(){
	    self.onClose();
	  };
	  this.ws.onmessage = function(ev){
	    self.onData(ev.data);
	  };
	  this.ws.onerror = function(e){
	    self.onError('websocket error', e);
	  };
	};

	/**
	 * Override `onData` to use a timer on iOS.
	 * See: https://gist.github.com/mloughran/2052006
	 *
	 * @api private
	 */

	if ('undefined' != typeof navigator
	  && /iPad|iPhone|iPod/i.test(navigator.userAgent)) {
	  WS.prototype.onData = function(data){
	    var self = this;
	    setTimeout(function(){
	      Transport.prototype.onData.call(self, data);
	    }, 0);
	  };
	}

	/**
	 * Writes data to socket.
	 *
	 * @param {Array} array of packets.
	 * @api private
	 */

	WS.prototype.write = function(packets){
	  var self = this;
	  this.writable = false;
	  // encodePacket efficient as it uses WS framing
	  // no need for encodePayload
	  for (var i = 0, l = packets.length; i < l; i++) {
	    parser.encodePacket(packets[i], this.supportsBinary, function(data) {
	      //Sometimes the websocket has already been closed but the browser didn't
	      //have a chance of informing us about it yet, in that case send will
	      //throw an error
	      try {
	        self.ws.send(data);
	      } catch (e){
	        debug('websocket closed before onclose event');
	      }
	    });
	  }

	  function ondrain() {
	    self.writable = true;
	    self.emit('drain');
	  }
	  // fake drain
	  // defer to next tick to allow Socket to clear writeBuffer
	  setTimeout(ondrain, 0);
	};

	/**
	 * Called upon close
	 *
	 * @api private
	 */

	WS.prototype.onClose = function(){
	  Transport.prototype.onClose.call(this);
	};

	/**
	 * Closes socket.
	 *
	 * @api private
	 */

	WS.prototype.doClose = function(){
	  if (typeof this.ws !== 'undefined') {
	    this.ws.close();
	  }
	};

	/**
	 * Generates uri for connection.
	 *
	 * @api private
	 */

	WS.prototype.uri = function(){
	  var query = this.query || {};
	  var schema = this.secure ? 'wss' : 'ws';
	  var port = '';

	  // avoid port if default for schema
	  if (this.port && (('wss' == schema && this.port != 443)
	    || ('ws' == schema && this.port != 80))) {
	    port = ':' + this.port;
	  }

	  // append timestamp to URI
	  if (this.timestampRequests) {
	    query[this.timestampParam] = +new Date;
	  }

	  // communicate binary support capabilities
	  if (!this.supportsBinary) {
	    query.b64 = 1;
	  }

	  query = parseqs.encode(query);

	  // prepend ? to query
	  if (query.length) {
	    query = '?' + query;
	  }

	  return schema + '://' + this.hostname + port + this.path + query;
	};

	/**
	 * Feature detection for WebSocket.
	 *
	 * @return {Boolean} whether this transport is available.
	 * @api public
	 */

	WS.prototype.check = function(){
	  return !!WebSocket && !('__initialize' in WebSocket && this.name === WS.prototype.name);
	};


/***/ },
/* 29 */
/***/ function(module, exports) {

	
	/**
	 * Module dependencies.
	 */

	var global = (function() { return this; })();

	/**
	 * WebSocket constructor.
	 */

	var WebSocket = global.WebSocket || global.MozWebSocket;

	/**
	 * Module exports.
	 */

	module.exports = WebSocket ? ws : null;

	/**
	 * WebSocket constructor.
	 *
	 * The third `opts` options object gets ignored in web browsers, since it's
	 * non-standard, and throws a TypeError if passed to the constructor.
	 * See: https://github.com/einaros/ws/issues/227
	 *
	 * @param {String} uri
	 * @param {Array} protocols (optional)
	 * @param {Object) opts (optional)
	 * @api public
	 */

	function ws(uri, protocols, opts) {
	  var instance;
	  if (protocols) {
	    instance = new WebSocket(uri, protocols);
	  } else {
	    instance = new WebSocket(uri);
	  }
	  return instance;
	}

	if (WebSocket) ws.prototype = WebSocket.prototype;


/***/ },
/* 30 */
/***/ function(module, exports) {

	
	var indexOf = [].indexOf;

	module.exports = function(arr, obj){
	  if (indexOf) return arr.indexOf(obj);
	  for (var i = 0; i < arr.length; ++i) {
	    if (arr[i] === obj) return i;
	  }
	  return -1;
	};

/***/ },
/* 31 */
/***/ function(module, exports) {

	/**
	 * Parses an URI
	 *
	 * @author Steven Levithan <stevenlevithan.com> (MIT license)
	 * @api private
	 */

	var re = /^(?:(?![^:@]+:[^:@\/]*@)(http|https|ws|wss):\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?((?:[a-f0-9]{0,4}:){2,7}[a-f0-9]{0,4}|[^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/;

	var parts = [
	    'source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'anchor'
	];

	module.exports = function parseuri(str) {
	    var src = str,
	        b = str.indexOf('['),
	        e = str.indexOf(']');

	    if (b != -1 && e != -1) {
	        str = str.substring(0, b) + str.substring(b, e).replace(/:/g, ';') + str.substring(e, str.length);
	    }

	    var m = re.exec(str || ''),
	        uri = {},
	        i = 14;

	    while (i--) {
	        uri[parts[i]] = m[i] || '';
	    }

	    if (b != -1 && e != -1) {
	        uri.source = src;
	        uri.host = uri.host.substring(1, uri.host.length - 1).replace(/;/g, ':');
	        uri.authority = uri.authority.replace('[', '').replace(']', '').replace(/;/g, ':');
	        uri.ipv6uri = true;
	    }

	    return uri;
	};


/***/ },
/* 32 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(global) {/**
	 * JSON parse.
	 *
	 * @see Based on jQuery#parseJSON (MIT) and JSON2
	 * @api private
	 */

	var rvalidchars = /^[\],:{}\s]*$/;
	var rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
	var rvalidtokens = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
	var rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g;
	var rtrimLeft = /^\s+/;
	var rtrimRight = /\s+$/;

	module.exports = function parsejson(data) {
	  if ('string' != typeof data || !data) {
	    return null;
	  }

	  data = data.replace(rtrimLeft, '').replace(rtrimRight, '');

	  // Attempt to parse using the native JSON parser first
	  if (global.JSON && JSON.parse) {
	    return JSON.parse(data);
	  }

	  if (rvalidchars.test(data.replace(rvalidescape, '@')
	      .replace(rvalidtokens, ']')
	      .replace(rvalidbraces, ''))) {
	    return (new Function('return ' + data))();
	  }
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 33 */
/***/ function(module, exports) {

	function Scope (typeName, scope, client) {
	  this.client = client
	  this.prefix = this._buildScopePrefix(typeName, scope, client.configuration('accountName'))
	}

	var props = [ 'set', 'get', 'subscribe', 'unsubscribe', 'publish', 'push', 'sync',
	  'on', 'once', 'when', 'removeListener', 'removeAllListeners', 'nameSync']

	var init = function (name) {
	  Scope.prototype[name] = function () {
	    var args = Array.prototype.slice.apply(arguments)
	    args.unshift(this.prefix)
	    this.client[name].apply(this.client, args)
	    return this
	  }
	}

	for (var i = 0; i < props.length; i++) {
	  init(props[i])
	}

	Scope.prototype._buildScopePrefix = function (typeName, scope, accountName) {
	  return typeName + ':/' + accountName + '/' + scope
	}

	module.exports = Scope


/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(35)('radar_state')
	var MicroEE = __webpack_require__(4)
	var Backoff = __webpack_require__(36)
	var Machine = __webpack_require__(37)

	function create () {
	  var backoff = new Backoff()
	  var machine = Machine.create({
	    error: function (name, from, to, args, type, message, err) {
	      log.warn('state-machine-error', arguments)

	      if (err) {
	        throw err
	      }
	    },

	    events: [
	      { name: 'connect', from: [ 'opened', 'disconnected' ], to: 'connecting' },
	      { name: 'established', from: 'connecting', to: 'connected' },
	      { name: 'authenticate', from: 'connected', to: 'authenticating' },
	      { name: 'activate', from: [ 'authenticating', 'activated' ], to: 'activated' },
	      { name: 'disconnect', from: Machine.WILDCARD, to: 'disconnected' },
	      { name: 'close', from: Machine.WILDCARD, to: 'closed' },
	      { name: 'open', from: [ 'none', 'closed' ], to: 'opened' }
	    ],

	    callbacks: {
	      onevent: function (event, from, to) {
	        log.debug('before-' + event + ', from: ' + from + ', to: ' + to,
	          Array.prototype.slice.call(arguments))

	        this.emit('event', event)
	        this.emit(event, arguments)
	      },

	      onstate: function (event, from, to) {
	        log.debug('event-state-' + event + ', from: ' + from + ', to: ' + to,
	          Array.prototype.slice.call(arguments))

	        this.emit('enterState', to)
	        this.emit(to, arguments)
	      },

	      onconnecting: function () {
	        this.startGuard()
	      },

	      onestablished: function () {
	        this.cancelGuard()
	        backoff.success()
	        this.authenticate()
	      },

	      onclose: function () {
	        this.cancelGuard()
	      },

	      ondisconnected: function (event, from, to) {
	        backoff.increment()

	        if (this._timer) {
	          clearTimeout(this._timer)
	          delete this._timer
	        }

	        var time = backoff.get()
	        log.debug('reconnecting in ' + time + 'msec')
	        this._timer = setTimeout(function () {
	          delete machine._timer
	          if (machine.is('disconnected')) {
	            machine.connect()
	          }
	        }, time)

	        if (backoff.isUnavailable()) {
	          log.info('unavailable')
	          this.emit('unavailable')
	        }
	      }
	    }
	  })

	  // For testing
	  machine._backoff = backoff
	  machine._connectTimeout = 10000

	  for (var property in MicroEE.prototype) {
	    if (MicroEE.prototype.hasOwnProperty(property)) {
	      machine[property] = MicroEE.prototype[property]
	    }
	  }

	  machine.open()

	  machine.start = function () {
	    if (this.is('closed')) {
	      this.open()
	    }

	    if (this.is('activated')) {
	      this.activate()
	    } else {
	      this.connectWhenAble()
	    }
	  }

	  machine.startGuard = function () {
	    machine.cancelGuard()
	    machine._guard = setTimeout(function () {
	      log.info('startGuard: disconnect from timeout')
	      machine.disconnect()
	    }, machine._connectTimeout)
	  }

	  machine.cancelGuard = function () {
	    if (machine._guard) {
	      clearTimeout(machine._guard)
	      delete machine._guard
	    }
	  }

	  machine.connectWhenAble = function () {
	    if (!(this.is('connected') || this.is('activated'))) {
	      if (this.can('connect')) {
	        this.connect()
	      } else {
	        this.once('enterState', function () {
	          machine.connectWhenAble()
	        })
	      }
	    }
	  }

	  return machine
	}

	module.exports = { create: create }


/***/ },
/* 35 */
/***/ function(module, exports) {

	module.exports = Minilog;

/***/ },
/* 36 */
/***/ function(module, exports) {

	function Backoff () {
	  this.failures = 0
	}

	Backoff.durations = [1000, 2000, 4000, 8000, 16000, 32000] // seconds (ticks)
	Backoff.fallback = 60000

	Backoff.prototype.get = function () {
	  return Backoff.durations[this.failures] || Backoff.fallback
	}

	Backoff.prototype.increment = function () {
	  this.failures++
	}

	Backoff.prototype.success = function () {
	  this.failures = 0
	}

	Backoff.prototype.isUnavailable = function () {
	  return Backoff.durations.length <= this.failures
	}

	module.exports = Backoff


/***/ },
/* 37 */
/***/ function(module, exports) {

	/*

	  Javascript State Machine Library - https://github.com/jakesgordon/javascript-state-machine

	  Copyright (c) 2012, 2013 Jake Gordon and contributors
	  Released under the MIT license - https://github.com/jakesgordon/javascript-state-machine/blob/master/LICENSE

	*/

	var StateMachine = StateMachine = module.exports = {

	    //---------------------------------------------------------------------------

	    VERSION: '2.2.0',

	    //---------------------------------------------------------------------------

	    Result: {
	      SUCCEEDED:    1, // the event transitioned successfully from one state to another
	      NOTRANSITION: 2, // the event was successfull but no state transition was necessary
	      CANCELLED:    3, // the event was cancelled by the caller in a beforeEvent callback
	      PENDING:      4  // the event is asynchronous and the caller is in control of when the transition occurs
	    },

	    Error: {
	      INVALID_TRANSITION: 100, // caller tried to fire an event that was innapropriate in the current state
	      PENDING_TRANSITION: 200, // caller tried to fire an event while an async transition was still pending
	      INVALID_CALLBACK:   300 // caller provided callback function threw an exception
	    },

	    WILDCARD: '*',
	    ASYNC: 'async',

	    //---------------------------------------------------------------------------

	    create: function(cfg, target) {

	      var initial   = (typeof cfg.initial == 'string') ? { state: cfg.initial } : cfg.initial; // allow for a simple string, or an object with { state: 'foo', event: 'setup', defer: true|false }
	      var terminal  = cfg.terminal || cfg['final'];
	      var fsm       = target || cfg.target  || {};
	      var events    = cfg.events || [];
	      var callbacks = cfg.callbacks || {};
	      var map       = {};
	      var name;

	      var add = function(e) {
	        var from = (e.from instanceof Array) ? e.from : (e.from ? [e.from] : [StateMachine.WILDCARD]); // allow 'wildcard' transition if 'from' is not specified
	        map[e.name] = map[e.name] || {};
	        for (var n = 0 ; n < from.length ; n++)
	          map[e.name][from[n]] = e.to || from[n]; // allow no-op transition if 'to' is not specified
	      };

	      if (initial) {
	        initial.event = initial.event || 'startup';
	        add({ name: initial.event, from: 'none', to: initial.state });
	      }

	      for(var n = 0 ; n < events.length ; n++)
	        add(events[n]);

	      for(name in map) {
	        if (map.hasOwnProperty(name))
	          fsm[name] = StateMachine.buildEvent(name, map[name]);
	      }

	      for(name in callbacks) {
	        if (callbacks.hasOwnProperty(name))
	          fsm[name] = callbacks[name];
	      }

	      fsm.current = 'none';
	      fsm.is      = function(state) { return (state instanceof Array) ? (state.indexOf(this.current) >= 0) : (this.current === state); };
	      fsm.can     = function(event) { return !this.transition && (map[event].hasOwnProperty(this.current) || map[event].hasOwnProperty(StateMachine.WILDCARD)); };
	      fsm.cannot  = function(event) { return !this.can(event); };
	      fsm.error   = cfg.error || function(name, from, to, args, error, msg, e) { throw e || msg; }; // default behavior when something unexpected happens is to throw an exception, but caller can override this behavior if desired (see github issue #3 and #17)

	      fsm.isFinished = function() { return this.is(terminal); };

	      if (initial && !initial.defer)
	        fsm[initial.event]();

	      return fsm;

	    },

	    //===========================================================================

	    doCallback: function(fsm, func, name, from, to, args) {
	      if (func) {
	        try {
	          return func.apply(fsm, [name, from, to].concat(args));
	        }
	        catch(e) {
	          return fsm.error(name, from, to, args, StateMachine.Error.INVALID_CALLBACK, 'an exception occurred in a caller-provided callback function', e);
	        }
	      }
	    },

	    beforeAnyEvent:  function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm.onbeforeevent,                       name, from, to, args); },
	    afterAnyEvent:   function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm.onafterevent || fsm.onevent,      name, from, to, args); },
	    leaveAnyState:   function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm.onleavestate,                        name, from, to, args); },
	    enterAnyState:   function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm.onenterstate || fsm.onstate,      name, from, to, args); },
	    changeState:     function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm.onchangestate,                       name, from, to, args); },

	    beforeThisEvent: function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm['onbefore' + name],                     name, from, to, args); },
	    afterThisEvent:  function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm['onafter'  + name] || fsm['on' + name], name, from, to, args); },
	    leaveThisState:  function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm['onleave'  + from],                     name, from, to, args); },
	    enterThisState:  function(fsm, name, from, to, args) { return StateMachine.doCallback(fsm, fsm['onenter'  + to]   || fsm['on' + to],   name, from, to, args); },

	    beforeEvent: function(fsm, name, from, to, args) {
	      if ((false === StateMachine.beforeThisEvent(fsm, name, from, to, args)) ||
	          (false === StateMachine.beforeAnyEvent( fsm, name, from, to, args)))
	        return false;
	    },

	    afterEvent: function(fsm, name, from, to, args) {
	      StateMachine.afterThisEvent(fsm, name, from, to, args);
	      StateMachine.afterAnyEvent( fsm, name, from, to, args);
	    },

	    leaveState: function(fsm, name, from, to, args) {
	      var specific = StateMachine.leaveThisState(fsm, name, from, to, args),
	          general  = StateMachine.leaveAnyState( fsm, name, from, to, args);
	      if ((false === specific) || (false === general))
	        return false;
	      else if ((StateMachine.ASYNC === specific) || (StateMachine.ASYNC === general))
	        return StateMachine.ASYNC;
	    },

	    enterState: function(fsm, name, from, to, args) {
	      StateMachine.enterThisState(fsm, name, from, to, args);
	      StateMachine.enterAnyState( fsm, name, from, to, args);
	    },

	    //===========================================================================

	    buildEvent: function(name, map) {
	      return function() {

	        var from  = this.current;
	        var to    = map[from] || map[StateMachine.WILDCARD] || from;
	        var args  = Array.prototype.slice.call(arguments); // turn arguments into pure array

	        if (this.transition)
	          return this.error(name, from, to, args, StateMachine.Error.PENDING_TRANSITION, 'event ' + name + ' inappropriate because previous transition did not complete');

	        if (this.cannot(name))
	          return this.error(name, from, to, args, StateMachine.Error.INVALID_TRANSITION, 'event ' + name + ' inappropriate in current state ' + this.current);

	        if (false === StateMachine.beforeEvent(this, name, from, to, args))
	          return StateMachine.Result.CANCELLED;

	        if (from === to) {
	          StateMachine.afterEvent(this, name, from, to, args);
	          return StateMachine.Result.NOTRANSITION;
	        }

	        // prepare a transition method for use EITHER lower down, or by caller if they want an async transition (indicated by an ASYNC return value from leaveState)
	        var fsm = this;
	        this.transition = function() {
	          fsm.transition = null; // this method should only ever be called once
	          fsm.current = to;
	          StateMachine.enterState( fsm, name, from, to, args);
	          StateMachine.changeState(fsm, name, from, to, args);
	          StateMachine.afterEvent( fsm, name, from, to, args);
	          return StateMachine.Result.SUCCEEDED;
	        };
	        this.transition.cancel = function() { // provide a way for caller to cancel async transition if desired (issue #22)
	          fsm.transition = null;
	          StateMachine.afterEvent(fsm, name, from, to, args);
	        };

	        var leave = StateMachine.leaveState(this, name, from, to, args);
	        if (false === leave) {
	          this.transition = null;
	          return StateMachine.Result.CANCELLED;
	        }
	        else if (StateMachine.ASYNC === leave) {
	          return StateMachine.Result.PENDING;
	        }
	        else {
	          if (this.transition) // need to check in case user manually called transition() but forgot to return StateMachine.ASYNC
	            return this.transition();
	        }

	      };
	    }

	  }; // StateMachine


/***/ },
/* 38 */
/***/ function(module, exports) {

	// Auto-generated file, overwritten by scripts/add_package_version.js

	function getClientVersion () { return '0.15.4' }

	module.exports = getClientVersion


/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	var Request = __webpack_require__(40),
	    Response = __webpack_require__(41),
	    Batch = __webpack_require__(42),
	    RadarMessage = function() {};

	RadarMessage.Batch = Batch;
	RadarMessage.Request = Request;
	RadarMessage.Response = Response;

	module.exports = RadarMessage;


/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	var logger = __webpack_require__(35)('message:request');

	var opTable = {
	  control: ['nameSync', 'disconnect'],
	  message: ['publish', 'subscribe', 'sync', 'unsubscribe'],
	  presence: ['get', 'set', 'subscribe', 'sync', 'unsubscribe'],
	  status: ['get', 'set', 'subscribe', 'sync', 'unsubscribe'],
	  stream: ['get', 'push', 'subscribe', 'sync', 'unsubscribe']
	};

	var Request = function (message) {
	  this.message = message;

	  if (!this._isValid()) {
	    logger.error('invalid request. op: ' + this.message.op + '; to: ' + this.message.to);
	    this.message = {};
	  }
	};

	Request.buildGet = function (scope, options) {
	  var message = { op: 'get', to: scope};
	  return new Request(message).setOptions(options);
	};

	Request.buildPublish = function (scope, value) {
	  var message = { op: 'publish', to: scope};
	  var request = new Request(message);
	  request.setAttr('value', value);

	  return request;
	};

	Request.buildPush = function (scope, resource, action, value) {
	  var message = { op: 'push', to: scope};
	  var request = new Request(message);
	  request.setAttr('resource', resource);
	  request.setAttr('action', action);
	  request.setAttr('value', value);

	  return request;
	};

	Request.buildNameSync = function (scope, options) {
	  var message = { op: 'nameSync', to: scope};
	  return new Request(message).setOptions(options);
	};

	Request.buildSet = function (scope, value, key, userType, clientData) {
	  var message = { op: 'set', to: scope};
	  var request = new Request(message);
	  request.setAttr('value', value);
	  request.setAttr('key', key);
	  request.setAttr('type', userType);
	  if (clientData) {
	    request.setAttr('clientData', clientData);
	  }

	  return request;
	};

	Request.buildSync = function (scope, options) {
	  var message = { op: 'sync', to: scope};
	  var request = new Request(message).setOptions(options);
	  if (request.isPresence()) {
	    request.forceV2Sync(options);
	  }
	  return request;
	};

	Request.buildSubscribe = function (scope, options) {
	  var message = { op: 'subscribe', to: scope};
	  return new Request(message).setOptions(options);
	};

	Request.buildUnsubscribe = function (scope) {
	  var message = { op: 'unsubscribe', to: scope};
	  return new Request(message);
	};

	// Instance methods

	Request.prototype.forceV2Sync = function (options) {
	  options = options || {};
	  options.version = 2;
	  this.setAttr('options', options);
	};

	Request.prototype.setAuthData = function (configuration) {
	  this.setAttr('userData', configuration.userData);
	  if (configuration.auth) {
	    this.setAttr('auth', configuration.auth);
	    this.setAttr('userId', configuration.userId);
	    this.setAttr('userType', configuration.userType);
	    this.setAttr('accountName', configuration.accountName);
	  }
	};

	Request.prototype.getMessage = function () {
	  return this.message;
	};

	Request.prototype.setOptions = function (options) {
	  // Keep check for options, since it is sometimes purposefully null
	  if (options) {
	    this.setAttr('options', options);
	  }

	  return this;
	};

	Request.prototype.isPresence = function () {
	  return this.type === 'presence';
	};

	Request.prototype.setAttr = function (keyName, keyValue) {
	  this.message[keyName] = keyValue;
	};

	Request.prototype.getAttr = function (keyName) {
	  return this.message[keyName];
	};

	Request.prototype.payload = function () {
	  return JSON.stringify(this.getMessage());
	};

	Request.prototype.getType = function () {
	  return this.type;
	};

	// Private methods

	Request.prototype._isValid = function () {
	  if (!this.message.op || !this.message.to) {
	    return false;
	  }

	  var type = this._getType();
	  if (type) {
	    if (this._isValidType(type) && this._isValidOperation(type)) {
	      this.type = type;
	      return true;
	    }
	  } else {
	    logger.error('missing type');
	  }
	  return false;
	};

	Request.prototype._isValidType = function (type) {
	  for (var key in opTable) {
	    if (opTable.hasOwnProperty(key) && key === type) {
	      return true;
	    }
	  }
	  this.errMsg = 'invalid type: ' + type;
	  logger.error(this.errMsg);
	  return false;
	};

	Request.prototype._isValidOperation = function (type) {
	  var ops = opTable[type];

	  var isValid = ops && ops.indexOf(this.message.op) >= 0;
	  if (!isValid) {
	    this.errMsg = 'invalid operation: ' + this.message.op + ' for type: ' + type;
	    logger.error(this.errMsg);
	  }
	  return isValid;
	};

	Request.prototype._getType = function () {
	  return this.message.to.substring(0, this.message.to.indexOf(':'));
	};

	module.exports = Request;


/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	var logger = __webpack_require__(35)('message:response');

	function Response (message) {
	  this.message = message;

	  if (!this._validate()) {
	    logger.error('invalid response. message: ' + JSON.stringify(message));
	    this.message = {};
	  }
	}

	Response.prototype.getMessage = function () {
	  return this.message;
	};

	Response.prototype._validate = function () {
	  if (!this.message.op) {
	    this.errMsg = 'missing op';
	    return false;
	  }

	  switch(this.message.op) {
	    case 'ack':
	      if (!this.message.value) {
	        this.errMsg = 'missing value';
	        logger.error(this.errMsg);
	        return false;
	      }
	      break;

	    default:
	      if (this.message.op !== 'err' && !this.message.to) {
	        this.errMsg = 'missing to';
	        logger.error(this.errMsg);
	        return false;
	      }
	  }

	  return true;
	};

	Response.prototype.isValid = function () {
	  return !!this.message.to && !!this.message.value && !!this.message.time;
	};

	Response.prototype.isFor = function (request) {
	  return this.getAttr('to') === request.getAttr('to');
	};

	Response.prototype.isAckFor = function (request) {
	  return this.getAttr('value') === request.getAttr('ack');
	};

	Response.prototype.getAttr = function (attr) {
	  return this.message[attr];
	};

	Response.prototype.forceV1Response = function () {
	  // Sync v1 for presence scopes is inconsistent: the result should be a 'get'
	  // message, but instead is an 'online' message.  Take a v2 response and
	  // massage it to v1 format prior to returning to the caller.
	  var message = this.message, value = {}, userId;
	  for (userId in message.value) {
	    if (message.value.hasOwnProperty(userId)) {
	      // Skip when not defined; causes exception in FF for 'Work Offline'
	      if (!message.value[userId]) { continue; }
	      value[userId] = message.value[userId].userType;
	    }
	  }
	  message.value = value;
	  message.op = 'online';

	  this.message = message;
	};

	module.exports = Response;


/***/ },
/* 42 */
/***/ function(module, exports) {

	function Batch () {
		var messages = Array.prototype.slice.call(arguments)
		this.value = messages
	}

	Batch.prototype.op = 'batch'

	Object.defineProperty(Batch.prototype, 'length', {
		get: function () {
			return this.value.length
		}
	})

	Batch.prototype.add = function (message) {
		this.value.push(message)
	}

	Batch.prototype.toJSON = function () {
		return {
			op: this.op,
			length: this.length,
			value: this.value
		}
	}

	module.exports = Batch


/***/ },
/* 43 */
/***/ function(module, exports) {

	/* globals location, jQuery */
	var immediate = typeof setImmediate === 'undefined' ? function (fn) { return setTimeout(fn, 1) } : setImmediate
	var slice = Array.prototype.slice
	var bind = function (fn, context) {
	  if (fn.bind) {
	    return fn.bind.apply(fn, slice.call(arguments, 1))
	  }

	  var args = slice.call(arguments, 2)

	  return function () {
	    return fn.apply(context, args.concat(slice.call(arguments, 0)))
	  }
	}

	function Retriever (radarClient) {
	  this.initializing = true
	  this.logger = radarClient._log('zendesk_radar_client:retriever')
	  this.radarClient = radarClient
	  this.apiHost = null
	  this.listeners = []
	  this.configuration = {}
	  this.lastTry = 0
	  this.lastRetrieval = 0
	  this.retrieving = false
	  this.waiting = false
	  this.tenMinutes = 10 * 60 * 1000
	  this.backoff = new radarClient.Backoff()
	  this.now = function () {
	    return Date.now ? Date.now() : new Date().getTime()
	  }
	}

	/* public */
	Retriever.prototype.getConfiguration = function (method, args) {
	  this.logger.debug('getConfiguration', method, args)

	  this.listeners.push({ method: method || 'empty', args: args || [] })

	  if (this.isExpired()) {
	    this._retrieveConfiguration()
	  } else {
	    this._scheduleListeners()
	  }
	}

	/* internal */
	Retriever.prototype._scheduleListeners = function () {
	  // snapshot listeners
	  var listeners = this.listeners
	  this.listeners = []

	  immediate(bind(this._processListeners, this, listeners))
	}

	Retriever.prototype._processListeners = function (listeners) {
	  this.logger.debug('processListeners', listeners.length)
	  while (listeners.length) {
	    var listener = listeners.shift()
	    this[listener.method].apply(this, listener.args)
	  }
	}

	Retriever.prototype._retrieveConfiguration = function () {
	  var self = this
	  this.logger.debug('retrieveConfiguration', self.retrieving, self.waiting)

	  if (!self.retrieving && !self.waiting) {
	    self.retrieving = true
	    self._contactAPI().then(function onResolved (data) {
	      self.onRetrieved(data, 'success')
	    }, function onRejected (data, status) {
	      self.onRetrieved(data, status)
	    })
	  }
	}

	Retriever.prototype._contactAPI = function (done) {
	  var host = ''
	  var options = {
	    dataType: 'json',
	    cache: false
	  }

	  if (location.protocol !== 'https:') {
	    if (!this.apiHost && this.radarClient.configuration('accountName')) {
	      this.apiHost = this.radarClient.configuration('accountName') + (/^[\w-]+(\..*)$/.test(this.radarClient.configuration('host')) ? RegExp.$1 : '.zendesk.com')
	    }
	    if (this.apiHost) {
	      host = 'https://' + this.apiHost
	    } else {
	      this.logger.warn('Warning: no apiHost found for http site, falling back to location.host')
	      host = 'https://' + location.host
	    }
	    options.xhrFields = { withCredentials: true }
	    options.crossDomain = true
	  }

	  options.url = host + '/api/v2/users/radar_token.json'
	  this.logger.debug('contactAPI', options)
	  this.radarClient.emit('configurationAttempt')
	  var response = jQuery.ajax(options)
	  // support callback
	  if (typeof done === 'function') {
	    response.then(done, done)
	  }
	  return response
	}

	/* utilities */
	Retriever.prototype.expireToken = function (time, token) {
	  this.lastRetrieval = time !== undefined ? time : this.lastRetrieval

	  if (token) {
	    this.configuration.auth = token
	  } else {
	    delete this.configuration.auth
	  }
	}

	Retriever.prototype.isExpired = function () {
	  return !this.configuration.auth || this.lastRetrieval < this.tenMinutesAgo()
	}

	Retriever.prototype.tenMinutesAgo = function () {
	  return this.now() - this.tenMinutes
	}

	Retriever.prototype._identityChanged = function (newConfig) {
	  return this.configuration.accountName !== newConfig.accountName ||
	    (this.configuration.userType !== 0 &&
	     (this.configuration.userId !== newConfig.userId ||
	      this.configuration.userType !== newConfig.userType))
	}

	/* promise handlers */
	Retriever.prototype.onRetrieved = function (configuration, status) {
	  this.logger.info('onRetrieved', status, configuration)
	  this.retrieving = false

	  if (status === 'success' && configuration.auth) {
	    this.backoff.success()
	    this.lastRetrieval = this.now()

	    if (!this.initializing && this._identityChanged(configuration)) {
	      this.radarClient.detach('Identity changed', configuration.accountName, configuration.userId, configuration.userType)
	      return
	    }

	    this.configuration = configuration
	    var server = configuration.servers && configuration.servers[Math.floor(Math.random() * configuration.servers.length)]

	    this.configuration.host = configuration.host || server.host
	    this.configuration.port = configuration.port || server.port
	    this.radarClient._configuration.auth = configuration.auth

	    this._scheduleListeners()
	    this.radarClient.emit('configurationUpdated')
	  } else {
	    this.waiting = true
	    setTimeout(bind(this.delayed, this), this.backoff.get())
	    this.backoff.increment()
	    this.radarClient.emit('configurationFailure')
	  }
	}

	Retriever.prototype.delayed = function () {
	  this.logger.info('configuration retrieval delayed')
	  this.waiting = false
	  this._retrieveConfiguration()
	}

	/* callbacks */
	Retriever.prototype.authenticateMessage = function (message) {
	  message.auth = this.radarClient.configuration('auth')
	  message.userId = this.radarClient.configuration('userId')
	  message.userType = this.radarClient.configuration('userType')
	  message.accountName = this.radarClient.configuration('accountName')
	  message.userData = this.radarClient.configuration('userData')
	  this.logger.debug('Retriever.authenticateMessage', message)
	  this.radarClient.emit('messageAuthenticated', message)
	}

	Retriever.prototype.empty = function () {}

	Retriever.prototype.activateRadarClient = function () {
	  this.radarClient.manager.activate()
	}

	Retriever.prototype.reattach = function (done) {
	  this.initializing = false
	  if (done) this.radarClient.once('ready', done)
	  this.radarClient.configure(this.configuration)
	}

	Retriever.prototype.initialize = function (done) {
	  this.initializing = false
	  this.radarClient.configure(this.configuration)
	  if (done) done()
	}

	Retriever.prototype.whenReady = function (done) {
	  done()
	}

	module.exports = Retriever


/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	var Tracker = {
	  create: function(name, done) {
	    var tracker = function(subname, handler) {
	      ++tracker.outstanding;

	      var subtracker = function() {
	        Tracker.logger.debug('subtracker', subname, 'is done');

	        if (subtracker.handler) {
	          subtracker.handler.apply(this, arguments);
	        }

	        tracker.complete();
	      };

	      subtracker.handler = handler;

	      return subtracker;
	    };

	    tracker.outstanding = 0;
	    tracker.done = done;

	    tracker.complete = function() {
	      Tracker.logger.debug('complete', name, tracker.outstanding);
	      if (--tracker.outstanding < 1 && tracker.done) {
	        tracker.done();
	      }
	    };

	    return tracker;
	  },

	  logger: __webpack_require__(35)('tracker')
	};

	module.exports = Tracker;



/***/ },
/* 45 */
/***/ function(module, exports) {

	module.exports = function(fn, name, message) {
	  var error = new Error(message || 'tracable-stack-trace');
	  error.name = name || 'TracableStackTrace';
	  return function() {
	    try {
	      return fn.apply(this, arguments);
	    } catch (e) {
	      console.error('(tracable-stack-trace)', error.stack);
	      throw e;
	    }
	  };
	};


/***/ }
/******/ ]);