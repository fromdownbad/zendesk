//= require mobile/jquery.color
//= require mobile/rails
//= require mobile/mobile_v2
//= require utils/namespace
//= require mobile/mobile_v2_search
//= require mobile/mobile_v2_multilayered_dropdown
//= require vendor/jquery.cookie
//= require utils/settings_cookie
//= require mobile/mobile_v2_go_native
//= require mobile/jquery.scale
//= require mobile/mobile_v2_ajax_calls
//= require mobile/mobile_v2_ios4_scroll_fix
