/* User */
Autocompleter.lookupUser = function(multi_user, searchTerm, callback) {
  var url = multi_user ? '/users/multivalue_autocomplete' : '/users/autocomplete';
  var parameters = {name: searchTerm, rand: (new Date()).getTime()};
  new Ajax.Request(url, {
    method: "GET",
    parameters: parameters,
    onSuccess: function(response) {
      callback(response.responseJSON);
    }
  });
};

Autocompleter.UserCache = new Autocompleter.Cache(Autocompleter.lookupUser.curry(false));
Autocompleter.cachedLookupUser = Autocompleter.UserCache.lookup.bind(Autocompleter.UserCache);

Autocompleter.MultiUserCache = new Autocompleter.Cache(Autocompleter.lookupUser.curry(true));
Autocompleter.cachedLookupMultiUser = Autocompleter.MultiUserCache.lookup.bind(Autocompleter.MultiUserCache);

/* Tag */
Autocompleter.lookupTag = function(searchTerm, callback) {
  new Ajax.Request('/tags/autocomplete.json', {
    method: "GET",
    parameters: {name: searchTerm, rand: (new Date()).getTime()},
    onSuccess: function(response) {
      callback(response.responseJSON);
    }
  });
};

Autocompleter.TagCache = new Autocompleter.Cache(Autocompleter.lookupTag);
Autocompleter.cachedLookupTag = Autocompleter.TagCache.lookup.bind(Autocompleter.TagCache);

/* Organization */
Autocompleter.lookupOrganization = function(searchTerm, callback) {
  new Ajax.Request('/organizations/autocomplete', {
    method: "GET",
    parameters: {name: searchTerm, rand: (new Date()).getTime()},
    onSuccess: function(response) {
      callback(response.responseJSON);
    }
  });
};
Autocompleter.OrganizationCache = new Autocompleter.Cache(Autocompleter.lookupOrganization);
Autocompleter.cachedLookupOrganization = Autocompleter.OrganizationCache.lookup.bind(Autocompleter.OrganizationCache);

