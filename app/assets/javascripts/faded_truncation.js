jQuery.fn.truncateViaFade = function(){
  this.each(function(){
    var t = $j(this);
    t.css('display', 'block');
    if (t.css('position') != 'absolute'){
      t.css('position', 'relative');
    };
    t.append($j('<span class="faded_truncation"></span>').css('height', t.height()));
  });
};