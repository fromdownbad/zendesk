//= require prototype/prototype
//= require vendor/unobtrusive_jquery
//= require utils/namespace

(function($) {

  $(function() {
    // Dismiss notifications

    $(".notification-dismiss").click(function() {
      $(this).closest(".notification").hide();
    });

    // Language Selector Dropdown

    function dismiss() {
      $(".dropdown").removeClass("dropdown_open");
    }

    function toggle() {
      $(this)
        .closest(".dropdown")
        .toggleClass("dropdown_open");

      return false;
    }

    $(document)
      .on("click", dismiss)
      .on("click", ".dropdown_toggle", toggle);

  });

})(jQuery);
