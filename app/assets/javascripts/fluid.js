document.observe('dom:loaded', function() {
  if(window.fluid) {
    document.body.insert("<div id='notifications' style='display:none'></div>");

    //Synchronous poller
    new Ajax.PeriodicalUpdater('notifications', '/notifications/status', {
      asynchronous: false, method: 'get', frequency: 120, decay: 10,
      requestHeaders: {Accept: 'application/json'},
      onSuccess: function(transport) {
        var json = transport.responseText.evalJSON();

        if(json.count != null && json.count > 0) {
          window.fluid.dockBadge = json.count;
        }

        if(json.message != null) {
          window.fluid.showGrowlNotification({
            title: "Zendesk update",
            description: json.message.toString(),
            priority: 1,
            sticky: false,
            identifier: "Zendesk " + Math.random() * 10000
          });
        }
      }
    });
  }
});
