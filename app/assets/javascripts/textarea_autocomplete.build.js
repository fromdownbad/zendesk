;(function($) {
var initialized = false;

function Selectable() {
  this.reset();
  this.elId = '#suggest';
}

Selectable.prototype.reset = Selectable.prototype.update = function(items) {
  var previous = this.active;
  this.selected = 0;
  this.items = items || [];
  this.active = !!(this.items.length > 0);
  // if something changed
  if(previous != this.active) {
    if (!this.active) {
      this.hide();
    } else {
      this.activated();
    }
  }
};

Selectable.prototype.keyMap = { tab: 9, enter: 13, esc: 27, up: 38, down: 40 };
Selectable.prototype.keyMapReverse = [ 9, 13, 27, 38, 40];

Selectable.prototype.attach = function(textArea) {
  var self = this;
  if(!initialized) {
    $('body').append('<div id="suggest"></div>');
    initialized = true;
  }
  $(textArea).unbind('.selectable').bind('keydown.selectable',  function(e) { self.keyDown(this, e); });
};

Selectable.prototype.detach = function(textArea) {
  $(textArea).unbind('.selectable');
  this.reset();
  this.hide();
};

// keyDown is needed to prevent defaults
Selectable.prototype.keyDown = function(element, event) {
  if (!this.active) return;
  var prevent = true, key = Selectable.prototype.keyMap;
  switch(event.keyCode) {
    case key.up:
      this.selected = Math.max(this.selected - 1, 0);
      break;
    case key.down:
      this.selected = Math.min(this.selected + 1, this.items.length - 1 );
      break;
    case key.tab:
    case key.enter:
      this.complete();
      break;
    case key.esc:
      this.reset();
      break;
    default:
      prevent = false;
  }
  if (prevent) {
    event.preventDefault();
    this.render();
  }
};

Selectable.prototype.isNavigationEvent = function(event) {
  return ($.inArray(event.keyCode, Selectable.prototype.keyMapReverse) > -1);
};

Selectable.prototype.render = function() {
  if(!this.items) { return; }
  var start = this.selected,
      end = this.selected + 5;
  if(start > 0) { start--; end--; }
  var views = $.map(this.items, this.renderItem).slice(start, end);
  $(this.elId).html('<ul>'+views.join('')+'</ul>');
  return this;
};

Selectable.prototype.hide = function() { this.active = false; $(this.elId).hide(); return this; };
Selectable.prototype.show = function() { $(this.elId).show(); return this; };
Selectable.prototype.move = function(pos) { $(this.elId).css(pos); return this; };
function Suggest() {
  var self = this;
  this.re = new RegExp('^{{.*');
  this.suggestions = {};
  this.lastToken = '';

  this.selectable = new Selectable();
  this.selectable.complete = function() { self.complete(); };
  this.selectable.renderItem = function(item, index) {
    return '<li class="suggest-li '
        + (index == self.selectable.selected ? 'suggest-li-focus' : '')
        + '">'+item+'<div class="suggest-detail">'+self.suggestions[item]+'</div></li>';
  };
  this.selectable.activated = function() {
    // move in place on first activation
    // always position at the start of the token, not where the caret is
    var sel = self.getTokenPos(self.getText(), self.getCaretPos()),
        pos = self.getTextAreaCoordsByIndex(self.textArea, sel.start),
        cpos = $(self.textArea).position();
    self.selectable.move( { top: pos.top + cpos.top, left: pos.left + cpos.left } ).show();
  };
}

Suggest.prototype.match = function(search) {
  // exclude any strings that don't match the full start of the string
  var filtered = [];
  // check if the token could even match
  if(!this.re.test(search)) {
    return [];
  }
  for(var key in this.suggestions) {
    if (search.length < key.length // search should be shorter than any token we try
        && this.suggestions.hasOwnProperty(key)
        && key.substr(0, search.length) == search) {
      filtered.push(key);
    }
  }
  return filtered;
};

Suggest.prototype.getToken = function() {
  var content = this.getText(),
      pos = this.getTokenPos(content, this.getCaretPos());
  return content.substring(pos.start, pos.end);
};

Suggest.prototype.getTokenPos = function(content, caretPos) {
  var space = /\s/,
      len = content.length,
      start = caretPos,
      end = caretPos - 1;
  // get the token at the caret position by finding the preceding space or beginning of content
  for(;start > 0; start--) {
    if(space.test(content[start-1])) break;
  }
  // we also get the characters from here till the end of the string
  for(; end < len; end++) {
    if(space.test(content[end+1])) break;
  }
  return { start: start, end: end + 1 };
};

// KeyUp handler for textarea
Suggest.prototype.keyUp = function(element, event) {
  var token = this.getToken();
  // do not update if the token does not change
  if(this.lastToken != token) {
    this.selectable.update(this.match(token));
    // only render if we update (navigation events are rendered by selectable)
    this.selectable.render();
    this.lastToken = token;
  }
};

Suggest.prototype.complete = function() {
  var content = this.getText(),
      pos = this.getTokenPos(content, this.getCaretPos());
  this.textArea.value = content.substr(0, pos.start)
          + this.selectable.items[this.selectable.selected] + ' '
          + content.substr(pos.end);
  this.selectable.reset();
};

// Bind attach and detact to focusIn / focusOut
Suggest.prototype.attach = function(el) {
  var self = this;
  $(el).unbind('.suggest').bind('keyup.suggest', function(ev) { self.keyUp(this, ev); });
  this.textArea = el;
  this.selectable.attach(el);
};

Suggest.prototype.detach = function(el) {
  $(el).unbind('.suggest');
  this.textArea = null;
  this.selectable.detach(el);
};

Suggest.prototype.getText = function() {
  return this.textArea.value;
};
Suggest.prototype.getCaretPos = function() {
  return this.textArea.selectionEnd; // todo IE?
};

Suggest.prototype.getTextAreaCoordsByIndex = function(textarea, index){
  var properties = ["height", "width",
                    "padding-top", "padding-right", "padding-bottom", "padding-left",
                    "lineHeight", "textDecoration", "letterSpacing",
                    "font-family", "font-size", "font-style", "font-variant", "font-weight"],
      elProp = { position: "absolute", overflow: "auto", "white-space": "pre-wrap", top: 0, left: -9999 },
      len, el, span, pos;
  if(!$(textarea).is('textarea') || index == null) {
    return;
  }

  for(var i = 0, len = properties.length; i < len; i++) { elProp[properties[i]] = $(textarea).css(properties[i]); }

  span = document.createElement('span');
  span.innerHTML = '&nbsp;';
  el = document.createElement('div');
  $(el).css(elProp).text(textarea.value.substring(0, index)).insertAfter(textarea);
  el.scrollTop = el.scrollHeight;
  el.appendChild(span);
  pos = $(span).position();
  $(el).remove();
  return pos;
};
window.Suggest = Suggest;
})(jQuery);
