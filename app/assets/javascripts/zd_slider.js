Zendesk.NS('Slider', this.jQuery, function($) {

  /*
   * This is a wrapper around the prototype slider, for using it include at least
   *  - public/javascripts/vendor/jquery-1.6.1.min.js
   *  - public/javascripts/prototype/prototype.js
   *  - public/javascripts/prototype/scriptaculous.js
   *  - public/javascripts/prototype/effects.js
   *  - public/javascripts/prototype/slider.js
   *  - public/javascripts/utils/namespace.js
   *  - public/javascripts/prototype/zd_slider.js
   *
   * Here is some markup it can be used with:
   *
   * <div id="slider" class="slider">
   *   <div class="ticks"></div>
   *   <div class="handle"></div>
   * </div>
   *
   * (ticks are optional)
   *
   * to initialize:
   *
   * Zendesk.Slider.setup('#slider', {onSliderChange: window.alert, range: [0, 100], value: 50, step: 10, showTicks: true});

   * TODO how to use?
   */

  function min(args) { return Math.min.apply(Math, args) }
  function max(args) { return Math.max.apply(Math, args) }

  function instrumentSlider(sliderElement, sliderElementHandle, range, value, values, callback) {
    new Control.Slider(sliderElementHandle, sliderElement, {
      range: new ObjectRange(range[0], range[1]),
      sliderValue: value,
      onSlide: callback,
      onChange: callback,
      values: values
    });
  }


  function alignLabel(slider, sliderHandle) {
    var $slider = $(slider),
        $handle = $(sliderHandle),
        $label = $slider.find('.label');

    if ($label.length === 0) return;

    var $arrow = $slider.find('.label-arrow-up'),
        halfArrowWidth = $arrow.length > 0 ? $arrow.outerWidth() / 2 : 0;

    // center label beneath the handle arrow
    var posCorrection = -1 * $label.outerWidth() / 2;
    posCorrection += $handle.width() / 2;
    $label.css({marginLeft: posCorrection});

    // in case the centered label overlaps the slider borders to the left
    // or to the right than move the label directly under the slider
    var leftOverlap = $slider.offset().left + $label.offset().left;
    if ($label.offset().left < $slider.offset().left) {
      posCorrection += $slider.offset().left - $label.offset().left;
      posCorrection -= halfArrowWidth;
    }
    var labelRight = $label.offset().left + $label.outerWidth(),
        sliderRight = $slider.offset().left + $slider.outerWidth();
    if (labelRight > sliderRight) {
      posCorrection -= labelRight - sliderRight;
      posCorrection += halfArrowWidth;
    }
    $label.css({marginLeft: posCorrection});
  }

  function addTicks(slider, values, range) {
    // display elements "#slider .ticks div" for the values of the slider
    var ticks = $(slider).find('.ticks');
    if (ticks.length == 0) {
      ticks = $('<div class="ticks"/>').appendTo(slider);
    }

    var protoTick = $('<div />').appendTo(ticks), // for measuring CSS values
        tickWidth = protoTick.width(),
        sliderHandleWidth = $(slider).find('.handle').width(),
        sliderHandleOuterWidth = $(slider).find('.handle').outerWidth(),
        maxValue = max(range),
        sliderWidth = $(slider).innerWidth(),
        tickPositions = values.collect(function(value) {
          return ((sliderWidth - sliderHandleWidth) / maxValue * value) + (sliderHandleOuterWidth/2 - tickWidth/2);
        })

    protoTick.remove();
    tickPositions.each(function(pos) {
      $('<div/>').appendTo(ticks).css({position: "absolute", left: pos + 'px'});
    });

  }


  function setup(sliderSelector, options) {
    var value = options.value || 0,
        step = options.step,
        values = options.values,
        relativePositions = options.relativePositions || values,
        startPos = relativePositions ? relativePositions[values.indexOf(value)] : value,
        range = options.range
             || (relativePositions ? [min(relativePositions), max(relativePositions)] : [0, 100]),
        showTicks = options.showTicks;

    if (!values && step) {
      values = relativePositions = new ObjectRange(range[0], range[1]).toArray().select(function(ea) { return ea % step === 0 });
    }

    var slider = $(sliderSelector)[0];
    if (!slider) {
      throw new Error('Trying to initialize slider but element matching '
                     + sliderSelector + ' cannot be found');
    }

    var sliderHandle = $(sliderSelector + ' .handle')[0];
    if (!sliderHandle) {
      throw new Error('Trying to get sliderHandle but no .handle element '
                      + 'found inside ' + slider);
    }

    function onSliderChange(relativePos) {
      if (typeof options.onSliderChange === "function") {
        if (relativePositions) {
          var idx = relativePositions.indexOf(relativePos) || 0;
          value = values[idx];
        } else {
          value = relativePos;
        }
        options.onSliderChange(value);
      }
      alignLabel(slider, sliderHandle);
    }

    instrumentSlider(slider, sliderHandle, range, startPos, relativePositions, onSliderChange);

    if (values && showTicks) {
      addTicks(slider, relativePositions, range);
    };

    onSliderChange(startPos);
  }


  // exports
  this.setup = setup;

});
