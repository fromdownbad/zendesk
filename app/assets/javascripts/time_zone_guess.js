 /*** Time zone ***/
 var tzMap = new Array()
 tzMap['-11']  = 'Midway Island';
 tzMap['-10']  = 'Hawaii';
 tzMap['-9']  = 'Alaska';
 tzMap['-8']  = 'Pacific Time (US &amp; Canada)';
 tzMap['-7']  = 'Mountain Time (US &amp; Canada)';
 tzMap['-6']  = 'Central Time (US &amp; Canada)';
 tzMap['-5']  = 'Eastern Time (US &amp; Canada)';
 tzMap['-4']  = 'Atlantic Time (Canada)';
 tzMap['-3.5']  = 'Newfoundland';
 tzMap['-3']    = 'Buenos Aires';
 tzMap['-2']    = 'Mid-Atlantic';
 tzMap['-1']    = 'Azores';
 tzMap['0']     = 'London';
 tzMap['1']     = 'Copenhagen';
 tzMap['2']     = 'Athens';
 tzMap['3']     = 'Moscow';
 tzMap['3.5']   = 'Tehran';
 tzMap['4']     = 'Baku';
 tzMap['4.5']   = 'Kabul';
 tzMap['5']     = 'Islamabad';
 tzMap['5.5']   = 'New Delhi';
 tzMap['6']     = 'Dhaka,';
 tzMap['7']     = 'Bangkok';
 tzMap['8']     = 'Hong Kong';
 tzMap['9']     = 'Tokyo';
 tzMap['9.5']   = 'Darwin';
 tzMap['10']    = 'Melbourne';
 tzMap['11']    = 'New Caledonia';
 tzMap['12']    = 'Wellington';
 
 
 function isDST() {
   var today = new Date();
   var jan = new Date(today.getFullYear(), 0, 1, 0, 0, 0, 0);
   var jul = new Date(today.getFullYear(), 6, 1, 0, 0, 0, 0);
   var temp = jan.toGMTString();
   var jan_local = new Date(temp.substring(0, temp.lastIndexOf(" ")-1));
   var temp = jul.toGMTString();
   var jul_local = new Date(temp.substring(0, temp.lastIndexOf(" ")-1));
   var hoursDiffStdTime = (jan - jan_local) / (1000 * 60 * 60);
   var hoursDiffDaylightTime = (jul - jul_local) / (1000 * 60 * 60);

   return hoursDiffDaylightTime != hoursDiffStdTime;
 }

function guessTimeZone(element) {
 	var date = new Date();
 	var zone = tzMap[(-1 * date.getTimezoneOffset()/60 - (isDST() ? 1 : 0)).toString()];

 	var options = element.options;

 	for(var i = 0; i < options.length; i++) {
 	  if(options[i].value == zone) {
 	    options[i].selected = true;
 	    return
 	  }
 	}
}
