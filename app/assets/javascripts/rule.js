/*globals Element,I18n,currentAccount,_,$j,jQuery,Zendesk,$,$$,Autocompleter,items,sources,rule_type,account_root_url,getSourceType,sourcePickList*/

//TODO this uses the globals sources, types, rule_type, account_root_url, getSourceType, sourcePickList

// THIS FILE IS FUNCTIONALLY DEAD AND WILL BE DELETED SOON.

/*
 Rule JavaScript functions.

 A rule is defined as having a conditions part, and some rules have an action part.
 These parts define the criteria in which the rule "lives" and what to do when the
 criteria are fullfilled.

 Combined the lines making up rules and actions are called a "set". This allows for
 definition of multiple rules on the same page, as used for reports.

 There are 2 kinds of methods in this file, builders and inserters. The builders
 always return a string, the inserters always call a builder and subsequently insert
 */

var containerRowCounter = [];

function increaseCount(set, type) {
  if(containerRowCounter[set] == null) {
    containerRowCounter[set] = [];
  }

  containerRowCounter[set][type] = containerRowCounter[set][type] == null ? 1 : containerRowCounter[set][type] + 1;
  return containerRowCounter[set][type];
}

var frameCount = 0;

function decreaseFrameCount() {
  if(frameCount > 0) {
    frameCount--;
  }
}

function increaseFrameCount() {
  frameCount++;
}

function buildFrameDeleteLink(setId) {
  //TODO: Rewrite Element.remove to remove this.parent?
  var e = new Element('a', { style: 'float:right; color:#B65151', href: '#' });
  e.onclick = function() { Element.remove(setId); decreaseFrameCount(); return false; };

  return e.update(I18n.t('txt.admin.public.javascript.zd_rule.remove_label'));
}

function buildContainer(parent, setId, containerType, source) {
  parent.appendChild(new Element('div', { id: setId+'_'+containerType }));

  var fields = new Element('fieldset', { className: 'conditions add' });
  fields.appendChild(new Element('h3', { className: 'light' }).update(translateContainerTypeAdd(containerType)));

  var elm = new Element('a', { className: 'plus-button', href: '#' });
  elm.onclick = function() { insertSelectionRow(setId+"_"+containerType, setId, containerType, source); return false; };
  fields.appendChild(elm);

  parent.appendChild(fields);
}

function translateContainerTypeAdd(containerType) {
  if(containerType == 'condition') {
    return I18n.t('txt.admin.public.javascript.zd_rule.add_condition_label');
  }
  return 'Add '+ containerType;
}

function buildReportStateSelect(setId, initialValue) {
  var reportState = new Element('fieldset', { className: 'conditions' });
  var presetState = (initialValue == null ? 'created_at' : initialValue);

  var stateField = new Element('select', { name:  'sets['+setId+'][state]',  style: 'width:225px' });

  stateField.appendChild(new Element('option', { value: 'created_at', selected: 'created_at' == presetState }).update(I18n.t('txt.admin.public.javascript.zd_rule.created_tickets_label')));
  stateField.appendChild(new Element('option', { value: 'solved_at', selected: 'solved_at' == presetState }).update(I18n.t('txt.admin.public.javascript.zd_rule.resolved_tickets_label')));
  stateField.appendChild(new Element('option', { value: 'working', selected: 'working' == presetState }).update(I18n.t('txt.admin.public.javascript.zd_rule.unsolved_tickets_label')));
  stateField.appendChild(new Element('option', { value: 'any_closed', selected: 'any_closed' == presetState }).update(I18n.t('txt.admin.public.javascript.zd_rule.old_tickets_label')));
  stateField.appendChild(new Element('option', { value: 'any', selected: 'any' == presetState }).update(I18n.t('txt.admin.public.javascript.zd_rule.all_tickets_label')));

  reportState.appendChild(stateField);
  return reportState;
}

//Use this function to add a "frame" containing selection boxes
function buildFrame(frameTitle, hasConditions, hasActions, setId) {

  if(setId == null) {
    alert("Missing frame id");
    return;
  }

  var containerFrame = new Element('div', { className: 'frame', id: setId, style: 'padding-top:20px'});
  containerFrame.appendChild(buildFrameDeleteLink(setId));
  containerFrame.appendChild(
    new Element('input', {
      type: 'text',
      onclick: "if(this.value == I18n.t('txt.admin.public.javascript.zd_rule.legend_for_this_data_series_label')) this.value = ''; return false",
      className: 'reportLegend',
      name: 'sets['+setId+'][legend]'
    }).setValue(frameTitle));

  if(hasConditions) {
    buildContainer(containerFrame, setId, 'condition', 'PICKCONDITION');
  }

  if(hasActions) {
    buildContainer(containerFrame, setId, 'action', 'PICKACTION');
  }

  increaseFrameCount();
  return containerFrame;
}

function insertSelectionRow(containerId, setId, elementType, source) {
  Element.insert($(containerId), { bottom: buildSelectionRow(setId, elementType, source) });
}

function buildSelectionRow(setId, elementType, source, operator, value) {
  var count = increaseCount(setId, elementType);
  var fieldKey = [ setId, elementType, count ].join('_');
  var elementName = elementType+'s';
  var selectName  = nestedName(['sets', setId, elementName, count, 'source']);

  var sourceSelect = new Element('select', {
    name: selectName, id: selectName, style: 'width:225px'
  });

  sourceSelect.onchange = function() {
    updateSelectionRow(this, setId, elementType, count); return false;
  };

  var sourceList = sourcePickList[elementType];

  // Remove all "hours since" conditions for ANY conditions (set 2) for views and automations
  if (setId == 2 && (rule_type == "View" || rule_type == "Automation")) {

    sourceList = (sourceList || []).findAll(function(f, i) {
      // somehow, an "undefined" value was sneaking in here in IE7, causing
      // source[f][2] to blow up.
      return f && sources[f] && !['fieldtagger', 'fieldcheckbox', 'numeric', 'hours'].member(sources[f][2]);
    });

    // Remove double instances of delimiters
    (sourceList || []).each(function(v, i) {
      if (sourceList[i-1] == 'DELIMITER' && v == 'DELIMITER')
        delete sourceList[i-1];
    });

    if (sourceList.last() == 'DELIMITER') {
      sourceList.pop();
    }

    sourceList = _(sourceList).compact();
  }

  //Build the options tags
  //sourcePickList[elementType].each(function(item) {
  sourceList.each(function(item) {
    var index = (elementType == 'action' && sources[item][1] != null) ? 1 : 0;
    var title = sources[item][index];

    var item_clean = item.split('#').first();
    sourceSelect.appendChild(new Element('option', { value: item, selected: selected(source == item || source == item_clean) }).update(title));
  });


  var fieldSet = new Element('fieldset', { className: 'conditions', id: fieldKey });

  var elm = new Element('a', { className: 'minus-button', href: '#' });
  elm.onclick = function() { Element.remove(fieldKey); return false; };

  fieldSet.appendChild(elm);
  fieldSet.appendChild(sourceSelect);

  var targetSpan = new Element('span', { id: fieldKey+'_target' });

  buildArgument(targetSpan, setId, elementType, source, count, operator, value);

  fieldSet.appendChild(targetSpan);

  return fieldSet;
}

function updateSelectionRow(element, setId, elementType, count) {
  var source = element.options[element.selectedIndex].value;
  var target = [setId, elementType, count, 'target'].join('_');

  // When a textarea is added to this span it totally breaks the layout.
  // Recreating the "broken" span element here so subsequent selects display
  // properly.
  var newSpan = $j('<span id="' + target + '"></span>');
  $j(('#' + target).replace(/\./g, '\\.')).replaceWith(newSpan);

  buildArgument($(target), setId, elementType, source, count);

  // if it's an assignee <select> and too long, make the updated input a combobox
  if (/^assignee/i.test($j(element).val())) {
    $j(('#' + target + ' select').replace(/\./g, '\\.'))
      .autocompleteFromSelectIfLarge({ placeholder: 'Type agent name...' });
  }

  // if it's an organization <select> and too long, make the updated input a combobox
  if (/^organization/i.test($j(element).val())) {
    $j(('#' + target + ' select').replace(/\./g, '\\.')).autocompleteFromSelectIfLarge();
  }
}

/*
 Converts an array to a Rails nested element name, eg. ['foo', 'bar', 'baz'] becomes 'foo[bar][baz]'
 */
function nestedName(keys) {
  var resultBuffer = '';

  for(var i=0; i<keys.length; i++) {
    resultBuffer += (i === 0 ? keys[i] : '[' + keys[i] + ']');
  }

  return resultBuffer;
}

function buildArgument(container, setId, elementType, source, count, operator, value) {
  if (sources[source] ===  undefined) return;
  var elementName  = (elementType === 'condition' ? 'conditions' : 'actions');
  var sourceType   = getSourceType(source);

  switch (sourceType){
    case "numeric": case "hours":
      if (!currentAccount.hasFeature('businessHours') && operator) {
        operator = operator.replace('_business_hours', '');
      }
      container.appendChild(buildSelect("operator", sourceType, operator));
      container.appendChild(buildInput("value", "text", value));
      break;
    case "simpletext":
      container.appendChild(buildInput("value", "text", value));
      break;
    case "tags":
      if (elementType == "action") {
        container.appendChild(buildInput("value", "text", value));
        switch (source) {
          case "set_tags":
          case "current_tags":
          case "remove_tags":
            container.appendChild(tagActionDescription(source));
        }
      } else {
        container.appendChild(buildSelect("operator", sourceType, operator));
        container.appendChild(buildInput("value", "text", value));
      }
      break;
    case "text":
      if (elementType != "action") {
        container.appendChild(buildSelect("operator", sourceType, operator));
      }
      container.appendChild(buildInput("value", "text", value));
      break;
    case "textarea":
      container.appendChild(buildText(I18n.t('txt.admin.public.javascript.zd_rule.text_label')));
      container.appendChild(buildTextarea("value", value));

      container.appendChild(placeholderList(count)); // FIXME
      break;
    case "update_type":  case "comment_is_public":
      container.appendChild(buildInput("operator", "hidden", "is"));
      container.appendChild(buildSelect("value", sourceType, value));
      break;
    case "role":
      container.appendChild(buildSelect("operator", "key_simple", operator));
      container.appendChild(buildSelect("value", sourceType, value));
      break;
    case "bool":
      container.appendChild(buildInput("operator", "hidden", "is"));
      container.appendChild(buildInput("value", "hidden", "on"));
      break;
    case "flag":
      container.appendChild(buildInput("operator", "hidden", "is"));
      container.appendChild(buildSelect("value", sourceType, value));
      break;
    case "via":
      container.appendChild(buildSelect("operator", "key_simple", operator));
      container.appendChild(buildSelect("value", sourceType, value));
      break;
    case "macro_id":
      container.appendChild(buildInput("operator", "hidden", "is"));
      container.appendChild(buildSelect("value", sourceType, value));
      break;
    case "recipient":
      container.appendChild(buildInput("value", "text", value, null, I18n.t('txt.admin.public.javascript.zd_rule.dummy_email_address'), "recipientTextField"));
      break;
    case "fieldtagger":
      if (elementType=="condition") {
        container.appendChild(buildSelect("operator", "key_simple", operator));
      } else {
        container.appendChild(buildInput("operator", "hidden", "is"));
      }

      var selectField = new Element('select', { name: name('value', 0) });

      if (!['View', 'Automation', 'Report'].member(rule_type) || elementType != "condition")
        selectField.appendChild(new Element('option', { value: ''}).update('-'));

      var field_id = source.replace('ticket_fields_','');
      items.taggers.find(function(t) { return t.id == field_id; }).custom_field_options.each(function(item, index){
        selectField.appendChild(new Element('option', { value: item.id,
          selected: selected(value == item.id)
        }).update(item.name));
      });

      container.appendChild(selectField);
      break;
    case "fieldcheckbox":
      container.appendChild(buildInput("operator", "hidden", "is"));
      container.appendChild(buildSelect("value", 'flag', value));
      break;
    case "notification_target":
      container.appendChild(buildInput("operator", "hidden", "is"));

      //This will get called when a target is selected. Hide/Show the message box as needed.
      var targetOnChange = function(sel) {
        var opt = $j("option:selected", sel);
        if(opt.attr('data-no-message') == 'true') {
          $j("textarea.text, label.newline, div.placeholder-info", sel.parent()).hide();
        } else {
          $j("textarea.text, label.newline, div.placeholder-info", sel.parent()).show();
        }
      };

      var targetSelect = $j(buildSelect("value", 'targets', value, false, targetOnChange));

      //Add a data attribute to each option indicating whether a message box is needed.
      $j("option", targetSelect).each(function(i, opt) {
        $j(opt).attr("data-no-message", items.targets[i].no_message);
      });

      container.appendChild(targetSelect[0]);
      container.appendChild(buildText(I18n.t('txt.admin.public.javascript.zd_rule.message_label')));
      container.appendChild(buildTextarea("value", value, 1));

      container.appendChild(placeholderList(count)); // FIXME
      targetOnChange(targetSelect);
      break;
    case "tweet_requester":
      container.appendChild(buildInput("operator", "hidden", "is"));
      container.appendChild(buildText(I18n.t('txt.admin.public.javascript.zd_rule.message_label')));
      container.appendChild(buildTextarea("value", value));
      container.appendChild(buildTip(I18n.t('txt.admin.public.javascript.zd_rule.mention_keep_it_short')));
      break;
    case "locale_id":
      if (elementType=="condition")
        container.appendChild(buildSelect("operator", "key_simple", operator));
      else
        container.appendChild(buildInput("operator", "hidden", "is"));

      container.appendChild(buildSelect("value", sourceType, value));
      break;
    case "notification_user": case "notification_group":
    container.appendChild(buildInput("operator", "hidden", "is"));
    var tType = (sourceType == 'notification_user' ? 'all_agents' : 'group_id#current_groups');
    container.appendChild(buildSelect("value", tType, value, true));
    container.appendChild(buildText(I18n.t('txt.admin.public.javascript.zd_rule.email_subject_label')));
    container.appendChild(buildInput("value", "text", value, 1));
    container.appendChild(buildText(I18n.t('txt.admin.public.javascript.zd_rule.email_body_label')));
    container.appendChild(buildTextarea("value", value, 2));

    container.appendChild(placeholderList(count)); // FIXME
    break;
    case 'satisfaction_score':
      var operatorKeys       = ((rule_type != "Automation") || (elementType == "condition")) ? "key_expanded" : "key_simple";
      var adjustedSourceType = (elementType == "condition") ? "satisfaction_conditions" : "satisfaction_actions";
      container.appendChild(buildSelect("operator", operatorKeys, operator));
      var argument = container.appendChild(buildSelect("value", adjustedSourceType, value));

      if (!valueSelectNeeded(operator)) argument.hide();
      break;
    case "na":
      break;
    default:
      if (source === "organization_name") {
        operator = buildSelect("operator", "key", operator);
        container.appendChild(operator);
        var org = ($j.grep(items.organizations, function(element, index){return(element.value == value); })[0] || {text: null}).text; // replaces items['organizations'][value], which can be null
        var autocomplete_field     = container.appendChild(buildInput("value", "text", org, 0));
        var autocomplete_container = container.appendChild(new Element('div', { className: 'autocomplete' }));
        new Autocompleter.Json(autocomplete_field, autocomplete_container, Autocompleter.cachedLookupOrganization,
          { frequency: 0.4, minChars: 1, choices: 50 }
        );
        return;
      }
      if (elementType=="condition") {
        operator = buildSelect("operator", (sourceType=="priority" || sourceType=="status" ? "key_expanded" : "key"), operator);
        container.appendChild(operator);
      }
      else {
        container.appendChild(buildInput("operator", "hidden","is"));
      }
      var argument = buildSelect("value", sourceType, value);
      if (elementType=="condition") {

        if (!valueSelectNeeded(operator.value)) argument.hide();
      }
      container.appendChild(argument);
      break;
  }

  return;

  /*
   Nested functions below here
   */

  function valueSelectNeeded(operator){
    return operator != 'changed' && operator != 'not_changed';
  }

  function name(part, valueId) {
    if(part == 'value') {
      return nestedName(['sets', setId, elementName, count, part, valueId]);
    }
    else {
      return nestedName(['sets', setId, elementName, count, part]);
    }
  }

  function buildSpan(className, value) {
    return new Element('span', { className: className}).update(value);
  }

  function buildSelect(part, itemType, value, skipFirst, changeCallback) {
    var safeValueId = 0;
    var selectField = new Element('select', { name: name(part, safeValueId) });
    var exclude = itemType.split('#').reverse();
    var stype = exclude.pop();

    var isSelected, hasSelected = false;

    //ZD252827: Remove action "Status -> New" as status can never be reset to New
    if (stype === 'status' && elementType === 'action') {
      exclude = [0];
    }

    items[stype].each(function(item, index){
      if (!(index === 0 && skipFirst) && (jQuery.inArray(item.value,exclude) == -1)) {
        isSelected = selected(getValue(value, safeValueId) == item.value);
        //Agent exists in the dropdown
        if ( isSelected ) { hasSelected = true; }

        selectField.appendChild(new Element('option', { value: item.value,
          selected: isSelected
        }).update(item.text));
      }
    });

    // If the agent doesn't exist, display the below text - ZD206455
    if (stype === 'all_agents' && value != undefined && !hasSelected) {
      selectField.appendChild(new Element('option', { value: 'inactive_agent',
        selected: true
      }).update(I18n.t('txt.admin.public.javascript.zd_rule.inactive_agent')));
    }

    selectField.onchange = function() {
      if(changeCallback) {
        changeCallback($j(selectField));
      }
      if (part == 'operator' && this.next()) {
        valueSelectNeeded(this.value) ? this.next().show() : this.next().hide();
      }
    };

    return selectField;
  }

  function buildInput(part, type, value, valueId, placeholder, inputID) {
    var safeValueId = valueId || 0;
    var options = {type: type,
                   name: name(part, safeValueId),
                   className: (safeValueId > 0 ? 'text large' : 'text')};
    if(placeholder){
      options['placeholder'] = placeholder;
    }
    if(inputID){
      options['id'] = inputID;
    }

    return new Element('input', options).setValue(getValue(value, safeValueId));
  }

  function buildTextarea(part, value, valueId) {
    var safeValueId = valueId || 0;

    return new Element('textarea', {
      className: 'text large', name: name(part, safeValueId), style: 'width: 95%'
    }).setValue(getValue(value, safeValueId));
  }

  function buildText(value){
    return new Element('label', { className: 'newline' }).update(value);
  }

  function buildTip(value){
    return new Element('label', { className: 'tip' }).update(value);
  }

  function getValue(value, valueId) {
    if (value) {
      return typeof(value) == 'object' ? value[valueId] : value;
    }
    else {
      return '';
    }
  }

  function placeholderList(count) {
    var placeholderId = 'placeholder-list-'+count;
    var placeholder = new Element('div', {className: "placeholder-info"});

    var elm = new Element('a', { href: '#' }).update(I18n.t('txt.admin.public.javascript.zd_rule.view_available_placeholders_label'));
    elm.onclick = function() {$j('#' + this.next(0).id).toggle(); return false; };
    placeholder.appendChild(elm);

    placeholder.appendChild(new Element('div', {
      id: placeholderId, className: 'placeholder-list', style: 'display:none'
    }).update($('placeholder-list').innerHTML));

    return placeholder;
  }

  function tagActionDescription(tagAction) {
    var description = new Element('p', {className: "tag-action-info"});
    description.update(I18n.t('txt.admin.public.javascript.zd_rule.' + tagAction + '_description'));
    return description;
  }
}

function selected(arg) {
  return arg ? 'selected' : null;
}
