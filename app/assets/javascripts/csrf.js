$j(document).ready(function() {
  var CSRFToken = $j('meta[name=shared-csrf-token]').attr('content');

  $j.ajaxPrefilter(function(options, originalOptions, xhr) {
    if (!options.crossDomain) {
      if (CSRFToken) {
        xhr.setRequestHeader('X-Shared-CSRF-Token', CSRFToken);
      }
    }
  });
});
