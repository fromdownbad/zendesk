/*globals $, confirmString, require */
//= require ./admin/base
//= require_tree ./admin/vendor/admin_assets
//= require vendor/select2.min

$(document).ready(function() {

  var LotusClient = require('admin/lotus_client');
  var SYSTEM_MESSAGE_MAX_SIZE = 255;

  $('form').each(function() {
    disableSave($(this));
  });

  $("input[type=checkbox][name*='helpcenter_enabled']").each(function () {
    var $element = $(this);

    if ($element.attr('disabled') === 'disabled') {
      $element.attr('checked', false);
    }
  });

  $("input[type=checkbox][name*='support_show_csat']").each(function () {
    var $element = $(this);

    if ($element.attr('disabled') === 'disabled') {
      $element.attr('checked', false);
    }
  });

  $('#blankApp.capsule').capsule({
    dropdown: {
      closeText: I18n.t('txt.admin.views.mobile_sdk.actions.close_app'),
    },
    onClose: function() {
      if ($('li.capsule').length === 1) {
        $('.zero-state').show();
      }
    }
  });

  $('.current-app.capsule').each(function() {
    var capsule = $(this);
    var deleteText = I18n.t('txt.admin.views.mobile_sdk.actions.delete_app');

    capsule.capsule({
      dropdown: {
        options: [
          {
            element: "<li role='presentation'><a role='menuitem' tabindex='-1' href='#'>" +
                     deleteText +
                     "</a></li>",
            onClick: function() {
              if (confirm(confirmString)) { //get string from HTML
                var form = $(".delete-form", capsule)[0];
                $(form).submit();
              }
            }
          }
        ],
        closeText: I18n.t('txt.admin.views.mobile_sdk.actions.close_app'),
        openText: I18n.t('txt.admin.views.mobile_sdk.actions.edit_app')
      }
    });
  });

  function addApp() {
    $('.zero-state').hide();
    $('#blankApp').show();
    $('#blankApp .name-field').trigger('change');
    $('#blankApp').trigger('open');
  }

  $('#addApp').click(function(){
    addApp();
  });

  $("#zeroStateAddApp").click(function() {
    $('.zero-state').hide();
    addApp();
  });

  $('.capsule-body .name-field').on("keyup change", function() {
    var capsule = $(this).parents('.capsule');
    var header = $('.capsule-header h5', capsule);

    header.text($(this).val());
  });

  function isTalkWarningDisplayed() {
    return !!$('.sdk-talk-warning').length
  }

  function isTalkTabDisplayed(form) {
    return !!$('#mobile_sdk_tabs_talk', form).length
  }

  function shouldRunTalkValidation() {
    return !isTalkWarningDisplayed() && isTalkTabDisplayed();
  }

  // The warning only happens when there is an error interacting with the Talk backend.
  // There is no point in running validations, if we know the data is incomplete and cannot be saved. 
  if (isTalkWarningDisplayed()) {
    $('.sdk-talk-enabled').each(function() {
      $(this).attr('disabled', 'disabled')
    });

    $('.sdk-talk-button-id').each(function() {
      $(this).attr('disabled', 'disabled')
    });

    $('.sdk-talk-group-select').each(function() {
      $(this).attr('disabled', 'disabled')
    });

    $('.sdk-talk-phone-number-select').each(function() {
      $(this).attr('disabled', 'disabled')
    });
  }


  $('.tab_links a').click(function() {
    var link = $(this);
    var parent = link.parents('.tabbed_container');
    var links = $('.tab_links a', parent);
    var otherLinks = links.not(link);
    var tabID = link.attr('href');

    var contentContainers = $('.tabs_content .tabs_canvas > div', parent);
    var newlySelectedTab = contentContainers.filter(tabID);
    var otherTabs = contentContainers.not(newlySelectedTab);

    otherLinks.removeClass('current');
    otherTabs.hide();
    link.addClass('current');
    newlySelectedTab.trigger("tab.show");
    newlySelectedTab.show();
  });

  $('#agree_mobile_sdk_tos').on('click', function(){
    var checkbox = $('#agree_mobile_sdk_tos');
    var button   = $('.get_started');

    if(checkbox.is(':checked')) {
      button.addClass('active');
    } else {
      button.removeClass('active');
    }
  });

  $('.generate-shared-secret').on('click', function() {
    generateSecret($(this).parents('form'));
  });

  $('.sdk-auth-select').on('change', function() {
    var authType = $(this).val();
    var form = $(this).parents('form');

    switch (authType) {
      case "jwt":
        $('.auth-wrapper', form).addClass("enabled");
        break;
      case "anonymous":
        $('.auth-wrapper', form).removeClass("enabled");
        break;
      default:
        $('.auth-wrapper', form).removeClass("enabled");
        break;
    }
  });

  $('.sdk-push-select').on('change', function() {
    var pushType = $(this).val();
    var form = $(this).parents('form');

    switch (pushType) {
      case "webhook":
        $('.push-webhook-wrapper', form).addClass("enabled");
        $('.push-urban-airship-wrapper', form).removeClass("enabled");
        break;
      case "urban_airship":
        $('.push-webhook-wrapper', form).removeClass("enabled");
        $('.push-urban-airship-wrapper', form).addClass("enabled");
        cleanInvalidWebhookUrl(form);
        break;
      case "empty":
        $('.push-webhook-wrapper', form).removeClass("enabled");
        $('.push-urban-airship-wrapper', form).removeClass("enabled");
        cleanInvalidWebhookUrl(form);
        break;
    }
  });

  $('.sdk-talk-group-select').on('change', function() {
    var groupId = $(this).val();
    var form = $(this).parents('form');
    var phoneNumberWrapper = $('.sdk-talk-phone-number-wrapper', form);
    var phoneNumberSelect = $('.sdk-talk-phone-number-select', form);

    if (!isEmpty(groupId)) {
      groupsPhoneNumbers = phoneNumberSelect.data('groupsPhoneNumbers')
      var select_one = $('<option></option>')
        .attr('value', '')
        .text(phoneNumberSelect.data('selectOne'));

      options = groupsPhoneNumbers.find(g => g.id == groupId)
        .phone_numbers
        .map(p => $('<option></option>').attr('value', p.id).text(p.display_number));

      phoneNumberSelect.empty().append([select_one].concat(options));
      phoneNumberWrapper.addClass('enabled');
    } else {
      phoneNumberSelect.empty().append(select_one);
      phoneNumberWrapper.removeClass('enabled');
    }
  });

  $('.system-message-select').on('change', function() {
    var systemMessageType = $(this).val();
    var form = $(this).parents('form');

    $('.system-message-custom-wrapper', form).toggleClass("enabled", systemMessageType === "custom");
    clearSystemMessageString(form, systemMessageType);
  });

  $('.system-message-custom-text').on('change keyup', function() {
    validateSystemMessageString($(this).parents('form'));
  });

  $(`
    .jwt-url,
    .agree_mobile_sdk_tos,
    .pn-webhook-url,
    .pn-urban-key,
    .pn-urban-master-secret,
    .pn-enabled,
    .system-message-select,
    .system-message-custom-text,
    .sdk-push-select,
    .sdk-auth-select,
    .sdk-talk-enabled,
    .sdk-talk-button-id,
    .sdk-talk-group-select,
    .sdk-talk-phone-number-select
  `).on('change keyup', function() {
    disableSave($(this).parents('form'));
  });

  $('.jwt-url').on('change keyup', function() {
    validateJwtUrl($(this).parents('form'));
  });

  $('.sdk-talk-button-id').on('change keyup', function() {
    validateTalkButtonId($(this).parents('form'));
  });

  $('.pn-webhook-url').on('change keyup', function() {
    validateWebhookUrl($(this).parents('form'));
  });

  $('button[type=reset]').on('click', function() {
    resetToDefaults($(this).parents('form'));
  });

  var jwtUrlPattern = /^https([!#$&-;=?-\[\]_a-z~]|%[0-9a-fA-F]{2})+$/;
  var webhookUrlPattern = /^http([!#$&-;=?-\[\]_a-z~]|%[0-9a-fA-F]{2})+$/;

  // Ensure that Talk button ID (Snapcall) is a UUID of 32 characters long.
  var talkButtonIdPattern = /^[0-9a-fA-F]{32}$/;

  function disableSave(form) {
    var authSelectVal = $('.sdk-auth-select', form).val();

    var tosCheckbox = $('.agree_mobile_sdk_tos');

    if (authSelectVal === 'select_one'
        || (authSelectVal === 'jwt' && !$('.jwt-url', form).val().match(jwtUrlPattern))
        || (shouldRunTalkValidation() && !areTalkSettingsValid(form))
        || !arePushNotificationSettingsValid(form)
        || !isSystemMessageSettingValid(form)
        || (tosCheckbox.length > 0 && !tosCheckbox.is(':checked'))
        ) {
      $('button[type=submit]', form).attr('disabled', true);
    } else {
      $('button[type=submit]', form).attr('disabled', false);
    }
  }

  function resetToDefaults(form) {
    var select = $('.system-message-select', form);
    select.val(select.data('chosen')).trigger('change');

    var talkPhoneNumberSelect = $('.sdk-talk-phone-number-select', form);
    var chosenPhoneNumber = talkPhoneNumberSelect.data('chosen')

    if (!!chosenPhoneNumber) {
      setTimeout(function(){
        talkPhoneNumberSelect.val(talkPhoneNumberSelect.data('chosen'))
      }, 1)
    } else {
      $('.sdk-talk-phone-number-wrapper', form).removeClass('enabled')
    }

    var talkButtonId = $('.sdk-talk-button-id', form);
    $(talkButtonId).parents('.validation').removeClass("error");
  }

  function isSystemMessageSettingValid(form) {
    var systemMessageSelectVal = $('.system-message-select', form).val();
    var systemMessageCustomTextVal = $('.system-message-custom-text', form).val();

    if (systemMessageSelectVal === 'empty') {
      return false;
    } else if (systemMessageSelectVal === 'custom' &&
      (systemMessageCustomTextVal.length < 0 ||
      !systemMessageHasValidFormat(systemMessageCustomTextVal) ||
      !systemMessageHasValidLength(systemMessageCustomTextVal))) {
      return false;
    } else {
      return true;
    }
  }

  function arePushNotificationSettingsValid(form) {
    var pushType = $('.sdk-push-select', form).val();
    if (pushType === undefined) {
      // Old Config
      var pushEnabled = $('.pn-enabled', form).is(':checked');
      return !pushEnabled || $('.pn-webhook-url', form).val().match(webhookUrlPattern);
    } else {
      return (pushType === 'empty'
          || (pushType === 'webhook' && !!$('.pn-webhook-url', form).val().match(webhookUrlPattern))
          || (pushType === 'urban_airship'
              && !( $('.pn-urban-key', form).val().length === 0 ||
                    $('.pn-urban-master-secret', form).val().length === 0
                 )
             )
          );
    }
  }

  function validateJwtUrl(form) {
    var jwtInput = $('.jwt-url', form);
    var jwtUrl = $(jwtInput).val();
    var jwtFieldWrapper = $(jwtInput).parents('.validation');

    if (jwtUrl.length < 5 || !!jwtUrl.match(jwtUrlPattern)) {
      jwtFieldWrapper.removeClass("error");
    } else {
      jwtFieldWrapper.addClass("error");
    }
  }

  function isTalkButtonIdValid(form) {
    var talkButtonId = $('.sdk-talk-button-id', form).val();
    return ((talkButtonId !== undefined) && (isEmpty(talkButtonId) || !!talkButtonId.match(talkButtonIdPattern)));
  }

  function areTalkSettingsValid(form) {
    var talkEnabled = $('.sdk-talk-enabled', form).is(':checked');
    var talkButtonId = $('.sdk-talk-button-id', form).val();
    var talkGroupId = $('.sdk-talk-group-select', form).val();
    var talkPhoneNumberId = $('.sdk-talk-phone-number-select', form).val();

    // Return `true` if all settings are empty (e.g customer does not want Talk SDK) OR they've all been filled out correctly.
    return (
      (talkButtonId !== undefined) && (
        (!talkEnabled && isEmpty(talkButtonId) && isEmpty(talkGroupId) && isEmpty(talkPhoneNumberId)) ||
        (!!talkButtonId.match(talkButtonIdPattern) && !isEmpty(talkGroupId) && !isEmpty(talkPhoneNumberId))
      )
    )
  }

  function validateTalkButtonId(form) {
    var buttonIdInput = $('.sdk-talk-button-id', form);
    var buttonId = $(buttonIdInput).val();
    var buttonFieldWrapper = $(buttonIdInput).parents('.validation');

    if (isTalkButtonIdValid(form)) {
      buttonFieldWrapper.removeClass("error");
    } else {
      buttonFieldWrapper.addClass("error");
    }
  }

  function clearSystemMessageString(form, systemMessageType) {
    if (systemMessageType !== "custom") {
      var systemMessageCustomInput = $('.system-message-custom-text', form);
      var systemMessageCustomWrapper = systemMessageCustomInput.parents('.validation');

      systemMessageCustomInput.val('');
      systemMessageCustomWrapper.removeClass('error');
    }
  }

  function validateSystemMessageString(form) {
    var systemMessageCustomInput = $('.system-message-custom-text', form);
    var systemMessageCustomText = systemMessageCustomInput.val();
    var systemMessageCustomWrapper = systemMessageCustomInput.parents('.validation');

    var formatError = !systemMessageHasValidFormat(systemMessageCustomText);
    var lengthError = !systemMessageHasValidLength(systemMessageCustomText);

    systemMessageCustomWrapper.toggleClass('error', formatError || lengthError);
    systemMessageCustomWrapper.toggleClass('format-error', formatError);
    systemMessageCustomWrapper.toggleClass('length-error', lengthError);
  }

  function systemMessageHasValidFormat(systemMessageCustomText) {
    if (systemMessageCustomText.length <= 0) {
      return true;
    } else {
      var openingMatches = systemMessageCustomText.match(/{{/g);
      var closingMatches = systemMessageCustomText.match(/}}/g);
      var openingCount = openingMatches ? openingMatches.length : 0;
      var closingCount = closingMatches ? closingMatches.length : 0;

      return openingCount === closingCount;
    }
  }

  function systemMessageHasValidLength(systemMessageCustomText) {
    return systemMessageCustomText.length <= SYSTEM_MESSAGE_MAX_SIZE;
  }

  function isWebhookUrlValid(form) {
    var webhookInput = $('.pn-webhook-url', form);
    var webhookUrl = $(webhookInput).val();

    return (webhookUrl.length < 5 || !!webhookUrl.match(webhookUrlPattern));
  }

  function validateWebhookUrl(form) {
    var webhookInput = $('.pn-webhook-url', form);
    var webhookFieldWrapper = $(webhookInput).parents('.validation');

    if (isWebhookUrlValid(form)) {
      webhookFieldWrapper.removeClass("error");
    } else {
      webhookFieldWrapper.addClass("error");
    }
  }

  function cleanInvalidWebhookUrl(form) {
    if (!isWebhookUrlValid(form)) {
      $('.pn-webhook-url', form).val("");
      validateWebhookUrl(form);
      disableSave(form);
    }
  }

  function generateSecret(form) {
    var href = $('.generate-shared-secret', form).data('href');
    var authToken = $('input[name=authenticity_token]', form).val();
    var secretField = $('.shared-secret', form);

    $.ajax({
      method: 'PUT',
      url: href + "?authenticity_token=" + encodeURIComponent(authToken),
      success: function(secret) {
        $(secretField).val(secret);
      }
    });
  }

  function isEmpty(item) {
    switch (item) {
      case '':
      case null:
        return true;
      default:
        return false;
    }
  }

});
