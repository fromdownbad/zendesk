// This should be sammyfied/backboned
$z.defModule("feeds/index", {
  initialize: function() {},

  renderLatestComment: function(nice_id){
    $j.ajax({
      url: '/tickets/' + nice_id + '/events?for=feed&filter=latest',
      dataType: "html",
      context: $j('#' + nice_id + '-reply'),
      success: function(transport){
          $j('#' + nice_id + '-ticket-form').hide();
          $j('#ticket-form').appendTo($j('#ticket-form-home'));
          this.replaceWith(transport);
        }
    });
  },

  renderRemainingComments: function(nice_id){
    this.remoteCall('/tickets/' + nice_id + '/events?for=feed&filter=remaining', 'li#' + nice_id + '-all');
  },

  renderCommentBody: function(comment_id){
    this.remoteCall('/events/' + comment_id + '?for=feed', 'span#' + comment_id + '-comment-body');
  },

  remoteCall: function(url, context){
    $j.ajax({
      url: url,
      dataType: "html",
      context: $j(context),
      success: function(transport){
          this.replaceWith(transport);
        }
    });
  },

  renderTicketForm: function(nice_id, status_id, priority_id, group_id, assignee_id){
    $j('.fake-text-area').show();
    $j('#show-ticket-properties').show();
    $j('#ticket-properties').hide();

    $j('#ticket-status-new').prop("disabled", status_id != '0');

    $j('#' + nice_id + '-fake-reply').hide();
    $j('#ticket-form').appendTo($j('#' + nice_id + '-ticket-form'));
    $j("#ticket_status_id").val(status_id);
    $j("#ticket_priority_id").val(priority_id);
    $j("#ticket_group_id").val(group_id);
    $j("#ticket_assignee_id").val('');
    $j("#comment_value").val('');
    $j("#comment_is_public").prop('checked', false);
    updateProperties(assignee_id);
    $j('#' + nice_id + '-ticket-form').show();
    $j('#ticket-form').show();
    $j('#comment_value').focus();
  }

});
