/*globals jQuery, InputSuggest*/

//= require admin/input_text_autocomplete
//= require admin/liquid_placeholders

(function($) {
  $(document).ready(function() {
    $("#target_method").change(function() {
      if ($(this).val() !== 'get') {
        $(".content-type").show();
      } else {
        $(".content-type").hide();
      }
    }).trigger("change");

    var options = function(link, type) {
      return {
        href: "#" + link.find(".raw_http_" + type).attr("id"),
        inline: true,
        width: '780px'
      };
    };

    $(".open_raw_request").colorbox(options($(this), "request"));
    $(".open_raw_response").colorbox(options($(this), "response"));

    new InputSuggest($("#target_url"));

    var passwordField = $("#target_password"),
      passwordPlaceholder = passwordField.attr("placeholder");

    passwordField.focus(function() {
      $(this).removeAttr("placeholder");
    });

    passwordField.blur(function() {
      $(this).attr("placeholder", passwordPlaceholder);
    });

    var basicAuthSubsetting = $('#sub_setting_basic_auth'),
      basicAuthCheckbox = $('#basic_auth_checkbox');

    basicAuthCheckbox.change(function() {
      if (basicAuthCheckbox.is(':checked')) {
        basicAuthSubsetting.slideDown();
      } else {
        basicAuthSubsetting.slideUp();
      }
    });

    if (basicAuthCheckbox.is(':checked')) {
      basicAuthSubsetting.addClass("enabled");
    }
  });
}(jQuery));
