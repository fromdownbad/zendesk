/*globals Zendesk, $j*/
Zendesk.NS("Reports");

Zendesk.Reports.ForumAnalytics = function(options) {
  options               = options || {};
  this.container        = options.container;
  this.sammyApp         = this._extendSammyApp(options.app);

  this.routes = {
    TAB:    "#forum_analytics",
    STATS:  "#forum_analytics/stats/:statName"
  }
};

Zendesk.Reports.ForumAnalytics.prototype = {
  $: function(selector) {
    return $j(selector, this.container);
  },

  _extendSammyApp: function(app) {
    var self = this;

    return app.bind('tab.changed_and_loaded', function(e, data) {
      if (data.tabID !== 'forum_analytics') { return; }
      if ( !self.$('#stats_summary_container').length ) { return; } // No permissions to see content, no html, just leave

      self._initUI();
      self._loadDefaultState();

      var params;
      if ( this.matchPath(data.path, self.routes.TAB) ) { self._getRouteTab(); }
      else if ( params = this.matchPath(data.path, self.routes.STATS) ) { self._getRouteStats(params); }
    });
  },

  _getRouteStats: function(params) {
    var sparkline = this.$(".sparkline#" + params.statName);
    if ( sparkline.hasClass('active') ) { return; }

    Zendesk.Stats.ForumUI.showDetailGraph(sparkline, { container: this.container });
  },

  _getRouteTab: function() {
    this._getRouteStats({ statName: this.$('.sparkline:first').attr('id') });
  },

  _loadDefaultState: function() {
    if ( !this.$(".sparkline > div:not(:empty)").length ) { Zendesk.Stats.ForumUI._drawSparklines({ container: this.container }); }
  },

  _initUI: function() {
    if(this._initialized) return;
    this._initialized = true;
    Zendesk.Stats.ForumUI._bindSparklineHandlers(this.sammyApp, { statsURL: '#forum_analytics/stats', container: this.container });
  }
};
