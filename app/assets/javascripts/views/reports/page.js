/*globals Zendesk, $j*/
Zendesk.NS("Reports.Page");

Zendesk.Reports.Page.init = function() {
  var tabbedContainer = new Zendesk.TabbedContainer.SammyApp();
  new Zendesk.Reports.Overview({        app: tabbedContainer.app, container: '#overview' });
  new Zendesk.Reports.ForumAnalytics({  app: tabbedContainer.app, container: '#forum_analytics' });
  new Zendesk.Reports.SearchAnalytics({ app: tabbedContainer.app, container: '#search_analytics' });
  new Zendesk.Reports.Exports({         app: tabbedContainer.app, container: '#export' });

  // load tab content before initializing
  tabbedContainer.app.bind("tab.change", function(e, data){
    var tab = $j('#' + data.tabID);
    var trigger = function(){ tabbedContainer.app.trigger('tab.changed_and_loaded', data); };

    if(tab.data('loaded')){
      trigger();
    } else {
      tab.data('loaded', true);
      tab.responsiveLoad("/reports?tab="+data.tabID, trigger);
    }
  });

  tabbedContainer.init();
};
