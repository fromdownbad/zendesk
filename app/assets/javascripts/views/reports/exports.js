/*globals Zendesk, $j, I18n*/
Zendesk.NS("Reports");

Zendesk.Reports.Exports = function(options) {
  options = options || {};
  this.container = options.container;
  this.sammyApp = this._extendSammyApp(options.app);

  // This causes SammyJS to ignore form submits, otherwise it tries to match these URLs against its routing tables.
  this.sammyApp.post('/csv_exports', function() {
    return true;
  });
  this.sammyApp.put('/settings/export_configuration/update_whitelisted_domain', function() {
    return true;
  });
};

Zendesk.Reports.Exports.prototype = {
  $: function(selector, container) {
    return $j(selector, container || this.container);
  },

  _extendSammyApp: function(app) {
    var self = this;

    return app.bind('tab.changed_and_loaded', function(e, data) {
      if (data.tabID !== 'export') {
        return;
      }
      self._initUI();
    });
  },

  _createDatepickers: function() {
    var self = this;

    this.$('.date_picker').each(function() {
      self.$(this).datepicker({
        maxDate: "+0D"
      });
      self._setDate(this);
    });
  },

  _setDate: function(elem) {
    var date = new Date(),
      dataFieldName = this.$(elem).data('field'),
      dataField = this.$("#" + dataFieldName);
    if (dataFieldName && dataField) {
      date.setTime(this.$(dataField).val() * 1000);
      this.$(elem).datepicker('setDate', (dataFieldName == 'json_start_date') ? '-1M':date);
    }
  },

  _createExportTimeBind: function() {
    var self = this;

    self.$('.js_has_date_picker').each(function() {
      var elem = self.$(this);

      elem.click(function(){
        var start_date = self.$('.date_picker.start_date', elem).datepicker('getDate');
        var end_date = self.$('.date_picker.end_date', elem).datepicker('getDate');
        start_date.setHours(0, 0, 0);
        end_date.setHours(23, 59, 59);

        var start_date_utc = self._utcDate(start_date);
        var end_date_utc = self._utcDate(end_date);

        if (end_date_utc.getTime() > Date.now()) {
          end_date_utc.setTime(Date.now());
        }

        self.$('.start_date_field', elem).val(start_date_utc.getTime() / 1000);
        self.$('.end_date_field', elem).val(end_date_utc.getTime() / 1000);
      })
    });
  },

  _utcDate: function(date) {
    return new Date(Date.UTC(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes(),
      date.getSeconds()));
  },

  _initUI: function() {
    this._createDatepickers();
    this._createExportTimeBind();
  }
};
