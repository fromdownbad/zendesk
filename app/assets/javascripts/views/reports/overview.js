/*globals Zendesk, $j*/
Zendesk.NS("Reports");

Zendesk.Reports.Overview = function(options) {
  options         = options || {};
  this.container  = options.container;
  this.sammyApp   = this._extendSammyApp(options.app);
};

Zendesk.Reports.Overview.prototype = {
  $: function(selector) {
    return $j(selector, this.container);
  },

  _bindSparklineHandlers: function() {
    var self = this, sparklines;

    sparklines = [
      { context: '.ticket_stats .search_sparkline', path: '#reports' },
      { context: '#forum_views',                    path: '#forum_analytics/stats/entry_view' },
      { context: '#forum_topics',                   path: '#forum_analytics' },
      { context: '#search_total_searches',          path: '#search_analytics' },
      { context: '#search_tickets_created',         path: '#search_analytics/stats/tickets' }
    ];

    $j(sparklines).each(function(index, el) {
      $j(el.context).bind("click", function() {
        //send users to the reporting dashboard only when using lotus
        if (el.context == '.ticket_stats .search_sparkline' && Zendesk.embeddedInLotusAdmin && Zendesk.embeddedInLotusAdmin()) {
          window.parent.Zd.router.transitionTo('reporting.index');
        } else {
          self.sammyApp.setLocation(el.path);
        }
      });
    });
  },

  _drawAllSparklines: function() {
    var self = this, sparklineGraphs;

    sparklineGraphs = [
      { div: "#tickets_created_sparkline",              statName: "created_count",        draw: this._drawTicketSparklines },
      { div: "#tickets_solved_sparkline",               statName: "solve_count",          draw: this._drawTicketSparklines },
      { div: "#tickets_first_response_time_sparkline",  statName: "first_response_time",  draw: this._drawTicketSparklines },
      { div: "#tickets_satisfaction_sparkline",         statName: "csr",                  draw: this._drawTicketSparklines },
      { div: "#forum_views_sparkline",                  statName: "entry_view",           draw: this._drawForumSparklines },
      { div: "#forum_topics_sparkline",                 statName: "entry_create",         draw: this._drawForumSparklines },
      { div: "#search_total_searches_sparkline",        statName: "searches",             draw: this._drawSearchSparklines },
      { div: "#search_tickets_created_sparkline",       statName: "tickets",              draw: this._drawSearchSparklines }
    ];

    $j(sparklineGraphs).each(function(index, graph) {
      if ( $j(graph.div).length ) {
        graph.draw.call(self, graph);
      }
    });
  },

  _drawForumSparklines: function(graph) {
    var graphDiv = this.$(graph.div), sparkline, statsInfo = Zendesk.Stats.ForumUI._statsInfo(this.$("#stats_object"));

    sparkline = new Zendesk.Stats.Graph(graphDiv, {
      graphType:      "sparkline",
      resource:       "forum",
      objectType:     statsInfo.objectType,
      objectId:       statsInfo.objectId,
      statName:       graph.statName,
      start:          statsInfo.windowStart,
      totalCountDiv:  graphDiv.closest('.sparkline_container').find(".stat_total")
    });
    sparkline.draw();
  },

  _drawSearchSparklines: function(graph) {
    var graphDiv = this.$(graph.div), sparkline;

    sparkline = new Zendesk.Stats.Graph(graphDiv, {
      graphType:     "sparkline",
      searchStatURL: "/account/search_account_stats",
      statName:      graph.statName,
      totalCountDiv: graphDiv.closest('.sparkline_container').find(".stat_total")
    });
    sparkline.draw();
  },

  _drawTicketSparklines: function(graph) {
    var graphDiv = this.$(graph.div), sparkline;

    sparkline = new Zendesk.Stats.Graph(graphDiv, {
      graphType:     "sparkline",
      searchStatURL: "/account/0/ticket_stats_by_account",
      statName:      graph.statName,
      totalCountDiv: graphDiv.closest('.sparkline_container').find(".stat_total")
    });

    $j.getJSON(sparkline.statsURL, { start: sparkline.startTime, interval: 86400 })
      .done($j.proxy(this._drawSparkline, sparkline, sparkline.statsURL, this._calculateTotal));
  },

  _extendSammyApp: function(app) {
    var self = this;

    return app.bind('tab.changed_and_loaded', function(e, data) {
      if (data.tabID !== 'overview') { return; }
      if ( !self.$('.stats_summary_container').length ) { return; } // No permissions to see content, no html, just leave

      self._initUI();

      var sparklines = self.$(".search_sparkline > div:not(:empty)");
      if(sparklines.length) return;

      self._drawAllSparklines();
    });
  },

  /** These functions have the sparkline as context **/
  _drawSparkline: function(url, calculateTotal, data) {
    this.data = data.data;
    this.draw();

    // Show total
    var format = '0,0', total = calculateTotal(data.data, data.counts);

    if (this.statName === 'csr') { format = '0,0%'; total *= 100; } // percentage
    if (this.statName === 'first_response_time') { total = total/3600; } // seconds to hours

    $j(this.totalCountDiv).append(Zendesk.Stats.formatNumber(format, total));
  },

  _calculateTotal: function(data, counts) {
    var points = 0, total = 0;

    // If counts, this is an average type of stats. For each data point (represents average in a timeframe), count is the number of entries that generated the average.
    // Average (data[i][1]) multiplied by count (counts[i]) will give the total for that timeframe. Sum all subtotals and divide by total of elements to get
    // the correct average total.
    if (counts && counts.length === data.length) {
      for (var i=0; i < data.length; i++) {
        if (counts[i] === undefined) { continue; }
        total   += data[i][1] * counts[i];
        points  += counts[i];
      }
      total /= points;
    } else {
      for (var j=0; j < data.length; j++) {
        total += data[j][1];
      }
    }

    return total;
  },

  _initUI: function() {
    if(this._initialized) return;
    this._initialized = true;
    this._bindSparklineHandlers();
  }
};
