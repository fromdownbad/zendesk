/*globals Zendesk, $j, I18n*/
Zendesk.NS("Reports");

Zendesk.Reports.SearchAnalytics = function(options) {
  options                 = options || {};
  this.container          = options.container;
  this.sammyApp           = this._extendSammyApp(options.app);

  this.routes = {
    STATS:  "#search_analytics/stats/:statName",
    TAB:    "#search_analytics",
    TABLE:  "#search_analytics/table/:orderField/:sortDirection/page/:page"
  };
};

Zendesk.Reports.SearchAnalytics.prototype = {
  $: function(selector) {
    return $j(selector, this.container);
  },

  _extendSammyApp: function(app) {
    var self = this;

    return app.bind('tab.changed_and_loaded', function(e, data) {
      self.searchStatsTable = self._initializeTable();
      if ( data.tabID !== 'search_analytics' ) { return; }
      if ( !self.$('.stats_summary_container').length ) { return; } // No permissions to see content, no html, just leave

      self._initUI();
      self._loadDefaultState();

      var params;
      if ( this.matchPath(data.path, self.routes.TAB) ) { self._getRouteTab(); }
      else if ( params = this.matchPath(data.path, self.routes.TABLE) ) { self._getRouteTable(params); }
      else if ( params = this.matchPath(data.path, self.routes.STATS) ) { self._getRouteStats(params); }
    });
  },

  _getRouteStats: function(params) {
    var statName  = params.statName,
        sparkline = this.$(".search_sparkline#" + statName);
    if ( sparkline.hasClass('active') ) { return; }

    this._drawDetailGraph(sparkline);       // 1) draw big graph of data
    this._setTableDescription(statName);    // 2) set table's description
    this._fetchTableForSparkline(statName); // 3) fetch relevant table data sorted by sparkline stat

    // hide percentage graph just in case
    this.$("#percentage_stats_graph").hide();
    this.$("#detailed_stats_graph").show();

    // all graphs except "total searches" have a percentage view
    if (statName !== "searches") {
      this._addPercentageSelectMenu(statName);
    } else {
      this.$("#stats_graph_select_menu").empty();
    }
  },

  _getRouteTab: function() {
    this._getRouteStats({ statName: this.$(".search_sparkline:first").attr('id') });
  },

  _getRouteTable: function(params) {
    this._loadDefaultGraph();

    // Table order field with sort direction.
    var orderField    = params.orderField,
        page          = parseInt(params.page, 10),
        sortDirection = params.sortDirection;

    this.$('#search_string_stats table th img').hide();
    this._sortTopStats(this.$('#search_string_stats table th.' + orderField), sortDirection, page);
  },

  _initializeTable: function() {
    if ( !this.$('.stats_summary_container').length ) { return undefined; }

    // fill sortable table
    var options = {
      divTable:             '#search_string_stats',
      statsType:            'all',
      orderField:           'searches',
      searchStatsTemplate:  '#search_stats_template',
      divPaginationTotal:   '#pagination_total',
      container:            this.container
    };

    this.$('#search_string_feedback_checkbox').hide();
    this.$(options.divTable + ' #show_more').hide();
    var searchStatsTable = new Zendesk.Stats.SearchStatsTable(options);
    this._displayFeedbackCheckbox(searchStatsTable);

    return searchStatsTable;
  },

  _loadDefaultGraph: function() {
    if ( this.$("#detailed_stats_graph").is(':empty') ) { this._drawDetailGraph(this.$('.search_sparkline:first')); }
  },

  _loadDefaultState: function() {
    if ( !this.$(".search_sparkline > div:not(:empty)").length ) { this._drawSparklines(); }
  },

  _setFeedbackCheckbox: function(isFeedbackData) {
    if (isFeedbackData === true)
      this.$('#search_string_feedback_checkbox').show();
  },

  _displayFeedbackCheckbox: function(table) {
    // check if we need to display feedback check box. We are going to show this check box if
    // there is any feedback stats in the database. This will take are of following corner cases:
    // - feedback tab might have been installed on a non zendesk domain.
    // - even though feedback tab was installed on a zendesk domain, it might have been removed recently
    //   and there was still stats data from previous stats which admin/user might want to see
    table.isFeedbackData(this, this._setFeedbackCheckbox);

    var checkbox = this.$('#search_string_stats #feedback_tag');
    // When the user reloads the page, checkbox keeps state, but we always load all data, so uncheck it.
    // If we want to change the data we load, be careful as table.isFeedbackData doesn an ajax (asynchronous) call.
    checkbox.prop('checked', false);
    checkbox.click(function() {
      var statsType = ($j(this).is(':checked') ? 'feedback_tab' : 'all');

      table.modifyDisplayed({ statsType: statsType });
    });
  },

  _bindSparklineHandlers: function() {
    var self = this;

    this.$(".search_sparkline").bind("click", function() {
      if (($j(this).hasClass("active"))) {
        return false;
      } else {
        self.sammyApp.setLocation('#search_analytics/stats/' + $j(this).attr("id"));
      }
    });
  },

  _sortByColumn: function() {
    var self = this;
    var sortableHeaders = this.$("table.tickets th").slice(0,-1);

    sortableHeaders.css({cursor: "pointer"});

    sortableHeaders.click(function() {
      var orderField    = $j(this).attr('class'),
          page          = self.searchStatsTable.statOptions.page,
          sortDirection = self.searchStatsTable.getNextSortDirection(orderField);

      self.sammyApp.setLocation('#search_analytics/table/' + orderField + '/' +  sortDirection + '/page/' +  page);
      return false;
    });
  },

  _bindTableHandlers: function() {
    this._sortByColumn();

    var self = this;
    this.$('#search_string_stats #show_more a').click(function(){
      self.$("table.tickets tr:hidden").show();
      var table = self.searchStatsTable,
        orderField = table.statOptions.orderField,
        page = table.nextPage(),
        sortDirection = table.getCurrentSortDirection(orderField);

      self.sammyApp.setLocation('#search_analytics/table/' + orderField + '/' +  sortDirection + '/page/' + page);
      return false;
    });
  },

  _bindSelectMenuHandler: function() {
    var self = this;
    this.$("#stats_graph_select_menu select").live("change", function() {
      var selected = $j(this).find("option:selected").val();
      if (selected === "percentage") {
        self._showPercentageGraph();
      } else {
        self._showOriginal();
      }
    });
  },

  _showOriginal: function() {
    this.$("#percentage_stats_graph").hide();
    this.$("#detailed_stats_graph").show();
  },

  _showPercentageGraph: function() {
    var total   = this.$("#searches_sparkline").data("graph_data").data;
    var current = this.$(".active").find("div").data("graph_data").data;
    var average = [];

    for (var i=0; i < total.length; i++) {
      var percentage = parseInt((parseInt(current[i][1], 10) * 100) / parseInt(total[i][1], 10), 10); // calculate percentage
      average.push([total[i][0], (isNaN(percentage) ? 0 : percentage)]);                  // watch those divide by zero errors
    }

    this.$("#detailed_stats_graph").hide();
    this.$("#percentage_stats_graph").show();
    var graph = new Zendesk.Stats.Graph(this.$("#percentage_stats_graph"), {
      graphType: "percentage",
      data:      average
    });
    graph.draw();
  },

  _drawSparklines: function() {
    var sparklineGraphs = [
      { div: "#searches_sparkline",    statName: "searches" },
      { div: "#no_results_sparkline",  statName: "no_results" },
      { div: "#no_clicks_sparkline",   statName: "no_clicks" },
      { div: "#tickets_sparkline",     statName: "tickets" }
    ];

    for (var i=0; i < sparklineGraphs.length; i++) {
      var sparkline = new Zendesk.Stats.Graph(this.$(sparklineGraphs[i].div), {
        graphType:     "sparkline",
        searchStatURL: "/account/search_account_stats",
        statName:      sparklineGraphs[i].statName,
        totalCountDiv: this.$(sparklineGraphs[i].div).parent().parent().find(".stat_total")
      });
      sparkline.draw();
    }
  },

  _drawDetailGraph: function(sparkline) {
    this.$(".search_sparkline.active").removeClass("active");
    this.$(sparkline).addClass("active");

    var statName = this.$(sparkline).prop('id');

    var graph = new Zendesk.Stats.Graph(this.$("#detailed_stats_graph"), {
      graphType:     "area",
      searchStatURL: "/account/search_account_stats",
      statName:      statName
    });
    graph.draw();

    $j(window).resize(function() {
      graph.draw();
    });
  },

  _setTableDescription: function(stat) {
    var tableDescription = {
      searches:   { title: I18n.t('txt.admin.public.javascript.views.reports.top_500_searches'), description: "" },
      no_results: {
        title: I18n.t('txt.admin.public.javascript.views.reports.search.top_500_searches_no_results_label'),
        description: I18n.t('txt.admin.public.javascript.views.reports.search.search.top_500_searches_no_results_description')
      },
      no_clicks: {
        title: I18n.t('txt.admin.public.javascript.views.reports.search.top_500_searches_no_clicks_label'),
        description: I18n.t('txt.admin.public.javascript.views.reports.search.top_500_searches_no_clicks_description')
      },
      tickets: {
        title: I18n.t('txt.admin.public.javascript.views.reports.search.top_500_searches_tickets_created_label'),
        description: I18n.t('txt.admin.public.javascript.views.reports.search.top_500_searches_tickets_created_description')
      }
    }[stat];

    this.$(".stats_summary_description").empty();
    this.$(".stats_summary_description").append(tableDescription.title);

    this.$(".stats_summary_extra_description").empty();
    this.$(".stats_summary_extra_description").append(tableDescription.description);
  },

  _fetchTableForSparkline: function(stat) {
    var statData = {
      searches:   ["searches", 'desc'],
      no_results: ["avg_results", 'asc'],
      no_clicks:  ["ctr", 'asc'],
      tickets:    ["tickets", 'desc']
    }[stat];

    // Uncheck 'feedback tab' if any other than search sparkline clicked (will be hidden later)
    if ( stat !== 'searches' ) {
      this.$('#search_string_feedback_checkbox input[type=checkbox]').prop('checked', false);
      this.searchStatsTable.statOptions.statsType = 'all';
    }

    this._sortTopStats(this.$("th." + statData[0]), statData[1], 1, this._filterCallback);
  },

  _addPercentageSelectMenu: function(stat) {
    var selectMenuDiv = this.$("#stats_graph_select_menu");
    var statTitle     = this.$("#" + stat).parent().find(".stat_title").html().toLowerCase();
    var selectMenu    = [];
    var originalTitle = this._getPercentageSelectMenuTranslation(statTitle);
    selectMenu.push('<select>');
    selectMenu.push('<option value="original">' + originalTitle + '</option>');
    selectMenu.push('<option value="percentage">' + I18n.t('txt.admin.public.javascript.views.reports.search.percent_of_total_searches_option') + '</option>');
    selectMenu.push('</select>');

    selectMenuDiv.empty();
    selectMenuDiv.append(selectMenu.join());
  },

  _getPercentageSelectMenuTranslation: function(statTitle) {
    if (statTitle == I18n.t('txt.admin.views.reports.tabs.search.with_no_results_label').toLowerCase()) {
      return I18n.t('txt.admin.views.reports.tabs.search.number_with_no_results_label');
    } else if (statTitle == I18n.t('txt.admin.views.reports.tabs.search.with_no_clicks_label').toLowerCase()) {
      return I18n.t('txt.admin.views.reports.tabs.search.number_with_no_clicks_label');
    } else if (statTitle == I18n.t('txt.admin.views.reports.tabs.search.tickets_created_label').toLowerCase()) {
      return I18n.t('txt.admin.views.reports.tabs.search.number_tickets_created_label');
    } else {
      return 'Number ' + statTitle;
    }
  },

  _filterCallback: function(stat) {
    this.$("table.tickets td." + stat).each(function() {
      if (stat == "ctr" || stat == "avg_results") {
        if (parseFloat($j(this).html()) > 0.05)
          $j(this).parent().hide();
      }

      if (stat == "tickets") {
        if (parseFloat($j(this).html()) < 1)
          $j(this).parent().hide();
      }
    });

    if (this.$(".active").attr("id") !== "searches") {
      // sort visible (filtered) rows in descending order by number of searches
      var sorted = this.$("table.tickets tbody tr:visible").sort(function(a, b) {
        return parseInt($j(b).find(".searches").html(), 10) - parseInt($j(a).find(".searches").html(), 10);
      });
      this.$("table.tickets tbody").empty();
      this.$("table.tickets tbody").append(sorted);
      this.$("#show_more").hide();
      this.$(".view_sort").remove();
      this.$("th.searches").children('div.title').append('<img alt="Table-arrow" class="view_sort" src="/images/table-arrow.png"/>');
      this.$("table.tickets th").css({cursor: "default"});
      this.$("table.tickets th").unbind("click");

      if (this.$('#search_string_feedback_checkbox').is(":visible")) {
        this.$('#search_string_feedback_checkbox').hide();
        this.$('#search_string_feedback_checkbox').data("show_again", true);
      }
    } else {
      if (this.$('#search_string_feedback_checkbox').data("show_again")) {
        this.$('#search_string_feedback_checkbox').show();
        this.$('#search_string_feedback_checkbox').data("show_again", false);
      }
      this._sortByColumn();
    }
  },

  _setSortImage: function(column, sortDirection) {
    this.$(".view_sort").remove();
    var sortImage = (sortDirection === 'desc' ? "/images/table-arrow.png" : "/images/table-arrow1.png");

    column.children('div.title').append('<img alt="Table-arrow" class="view_sort" src="'+ sortImage + '"/>');
  },

  _sortTopStats: function(column, sortDirection, page, callback) {
    var orderField  = column.prop('class'),
        table       = this.searchStatsTable,
        pageSize    = (this.$(".active").attr("id") === "searches") ? 15 : 100;

    table.setSort(orderField, sortDirection);
    table.modifyDisplayed({ page: page, pageSize: pageSize }, this, callback);
    this._setSortImage(column, sortDirection);
  },

  _initUI: function() {
    if(this._initialized) return;
    this._initialized = true;
    this._bindSparklineHandlers();
    this._bindTableHandlers();
    this._bindSelectMenuHandler();
  }
};
