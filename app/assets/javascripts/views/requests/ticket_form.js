/*globals jQuery, $j, _, Zendesk, setVisibility*/
(function($) {

  var $ticketFormData;
  var ticketFormMarkup = {};

  function getForm(formId) {
    var form = _.find($ticketFormData, function(form) { return form.id === formId; });
    if (form == null) { throw new Error("No such form: " + formId); }

    return form;
  }

  function visibleFieldsForForm(formId) {
    var form = getForm(formId);
    return form.ticket_fields;
  }

  function hideTicketSection() {
    $('#ticketfields').hide();
    $('#attachment-friends').hide();
  }

  function showTicketSection() {
    $('#ticketfields').show();
    $('#attachment-friends').show();
    $('#ticket-field-friends div').not('#date-flash').show();
  }

  function showTicketFormFields(formID) {
    var fieldsToShow = visibleFieldsForForm(formID);
    var friends = $('#ticket-field-friends');

    friends.empty();

    _.each(fieldsToShow, function(field, index) {
      var fieldMarkup = ticketFormMarkup[field.id];
      friends.append(fieldMarkup);
    });

    if ($('.new_date_picker')) {
      rebuildDatepickers();
    }

    showTicketSection();
    setVisibility("#ticket_date", ($j("#ticket_ticket_type_id").val() === "4"));
    setupNestedDropdowns();
    $('#suggestions_for_new_topic').hide();
    if (Zendesk && Zendesk.bindRelatedTopics) {
      Zendesk.bindRelatedTopics();
    }
  }

  function rebuildDatepickers() {
    var datepickers = $('.new_date_picker');
    datepickers.removeClass('hasDatepicker');
    $('.ui-datepicker-trigger').remove();

    datepickers.datepicker({
      showOn:             "both",
      buttonImage:        "/images/calendar_date_select/calendar.gif",
      buttonImageOnly:    true,
      showAnim:           "slideDown",
      defaultDate:        +7,
      onSelect: function() {
        var dateObject = $(this).datepicker('getDate');
        var dataField = this.getAttribute("data-field");
        $('#' + dataField).val(getDateString(dateObject));
      }
    });
  }

  function getDateString(dateObject) {
    return dateObject.getFullYear() + "-" +
      ("0" + (dateObject.getMonth() + 1)).slice(-2) + "-" +
      ("0" + dateObject.getDate()).slice(-2);
  }

  function setupNestedDropdowns() {
    $('ul.drop-list').each(function(i, elem) {
      new Zendesk.UI.NestedMenu(elem).initialize();
    });
    $('.field-tagger').ticketTagger();
  }

  function onTicketFormSelected(e) {
    var formID = parseInt($(e.target).val(), 10);
    if (formID === -1) {
      hideTicketSection();
      return;
    }

    showTicketFormFields(formID);
  }

  function renderDefaultForm() {
    var formID = $ticketFormData[0].id;

    showTicketFormFields(formID);
  }

  $(function init() {
    var $el = $('#ticket_forms_data');
    if ($el.length === 0) { return; }

    $ticketFormData = JSON.parse($el.html());
    if ($ticketFormData.length === 0) { return; }

    $('#ticket-field-friends div[data-field-id]').each(function(index, markup) {
      var id = $(markup).data('field-id');
      ticketFormMarkup[id] = markup;
    });

    if ($ticketFormData.length === 1) {
      renderDefaultForm();
      return;
    }

    hideTicketSection();

    $('#ticket_ticket_form_id')
      .change(onTicketFormSelected)
      .change();
  });

}(jQuery));
