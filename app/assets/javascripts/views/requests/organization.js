/*globals jQuery*/
(function($) {

  function onOrganizationSelected(e) {
    $('#ticket_organization_id').val($(this).val());
  }

  $(function init() {
    $('#request_organization_id').change(onOrganizationSelected);
  });

}(jQuery));
