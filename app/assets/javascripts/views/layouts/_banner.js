if (!this.Zendesk) { this.Zendesk = {}; }

(function($, console, Zendesk, _) {

  Zendesk.Banner = {

    /* Static methods: */

    /*
      The selector for the container into which Banners are placed.
      @api public
     */
    containerSelector: '#banners',

    /*
      @return [jQuery DOM Element] the container into which Banners are placed
      @api public
     */
    container: function() {
      var self = Zendesk.Banner;
      var container = $(self.containerSelector);
      if (container.length <= 0) {
        console.error('Could not find a banner container. Do you have an ' + self.containerSelector + ' element?');
      } else {
        return self._bindIgnoreEventListenerIfNeeded(container);
      }
    },

    /*
      The selector for the Banner template.
      @api public
     */
    templateSelector: '#banner-item-template',

    /*
      @return [String] the template for new Banners
      @api public
     */
    template: function() {
      // load the template, then cache it
      var self = Zendesk.Banner;
      if (!self._template) {
        var templateContainer = $(self.templateSelector).first();
        if (templateContainer.length <= 0) {
          console.error("Could not find a Banner template. Do you have an " + self.templateSelector + " element?");
        } else {
          self._template = templateContainer.html();
        }
      }
      return self._template;
    },

    /*
      Whether or not the event listener for ignore.zd.banner events
      has been bound to the banners container.
      @api private
     */
    _ignoreEventListenerBound: false,

    /*
      Bind the ignore.zd.banner event listener to the container if needed.
      @api private
     */
    _bindIgnoreEventListenerIfNeeded: function(container) {
      var self = Zendesk.Banner;
      if (!self._ignoreEventListenerBound) {
        container.bind('ignore.zd.banner', function(event, banner) {
          banner.disappear();
          return true; // allow event to bubble up
        });
        container.bind('reload.zd.banner', function(event, banner) {
          window.location.reload(true);
          return true; // allow event to bubble up
        });
        self._ignoreEventListenerBound = true;
      }
      return container;
    },

    /*
      Create a new banner element.

      Usage:

      To create a new banner and have it slide down:

          Zendesk.Banner.create({ text: 'Some content' }).appear();

      The Banner object is an enhanced jQuery DOM Element, so you can
      modify it as such. For exmaple, to create a new Banner with no
      reload button, add a class and ID, then show it:

          new Zendesk.Banner({ text: 'Some content', reload: false })
            .addClass('foo')
            .attr('id', 'bar')
            .appear();

      Banners are hidden on creation by default. To have them visible
      immediately (with no effects), pass { visible: true }:

          Zendesk.Banner.create({ text: 'Some content', visible: true });

      Events:

      The Zendesk.Banner library defined a number of custom jQuery events.
      Each event passes the Banner in question as the sole argument (after
      the required first argument that is the event itself).

      disappear.zd.banner triggered after a banner has finished disappearing

      appear.zd.banner    triggered after a banner has finished showing

      ignore.zd.banner    triggered when a banner's ignore button is clicked.
                          The banner container will catch this event and
                          hide the banner; you can prevent this behavior
                          by binding a listening to the banner itself and
                          calling preventDefault on the Event.

      reload.zd.banner    triggered when a banner's reload button is clicked.
                          The banner container will catch this event and
                          reload the page; you can prevent this behavior
                          by binding a listening to the banner itself and
                          calling preventDefault on the Event.
     */
      create: function(options) {
        var self = Zendesk.Banner;
        var banner = $.extend({}, self._buildLI(options || {}), self._enhancements);
        $(banner).find('.ignore').click(function() {
          $(banner).trigger('ignore.zd.banner', [banner]);
        });
        $(banner).find('.reload a').click(function() {
          $(banner).trigger('reload.zd.banner', [banner]);
          return false;
        });
        return banner.appendTo(self.container());
      },

      // @api private
      _buildLI: function(options) {
        var li = $($.mustache(Zendesk.Banner.template(), { text: (options.text || '') }));
        if (!(options.visible)) { li.hide(); }
        ['icon', 'ignore', 'reload'].each(function(property) {
          if (options[property] === false) {
            li.find('.' + property).hide();
          }
        });
        return li;
      },

    // @api private, though some of its contents are public
    _enhancements: {

      toString: function() {
        return '<banner text=' + this.content().text() + '>';
      },

      /*
        Set the text.
        @param [String] the new text.
        @return [jQuery DOM Element] this banner
        @api public
       */
      setContent: function(content) {
        this.content().html(content);
        return this;
      },

      /*
        Show the banner with fun animations. Triggers an 'appear.zd.banner' event
        before optionally calling the callback.
        @param [Function] callback to be called after the animations finish; optional
        @return [jQuery DOM Element] this banner
        @api public
       */
      appear: function(callback) {
        var self = this;
        self.slideDown(null, function() {
          self.trigger('appear.zd.banner', [self]);
          callback && callback();
        });
        return self;
      },

      /*
        Hide the banner with fun animations. Triggers a 'disappear.zd.banner' event
        before optionally calling the callback.
        @param [Function] callback to be called after the animations finish; optional
        @return [jQuery DOM Element] this banner
        @api public
       */
      disappear: function(callback) {
        var self = this;
        self.slideUp(null, function() {
          self.trigger('disappear.zd.banner', [self]);
          callback && callback();
        });
        return self;
      },

      /*
        @return [true, false] whether or not the banner is visible.
        @api public
       */
      visible: function() {
        return this.filter(':visible').length > 0;
      },

      /*
        @return [true, false] whether or not the banner is hidden.
        @api public
       */
      hidden: function() {
        return this.filter(':hidden').length > 0;
      }

    }

  };

  /* Getters for the various subcomponents of a banner: */

  _([ 'icon', 'content', 'ignore', 'reload' ]).each(function(component) {
    /*
      Get the jQuery DOM Element that contains the
      Banner's [icon|content|ignore button|reload button].
      @return [jQuery DOM Element]
      @api public
     */
    Zendesk.Banner._enhancements[component] = function() {
      return $(this).find('.'+component);
    };
  });

})(this.jQuery, this.console, this.Zendesk, this._);
