$z.defModule('entries/show', {
  initialize: function (params) {
    if(typeof(currentUser) !== "undefined") {
      $j("input[name='authenticity_token']").each(function() {
        $j(this).attr('value', currentUser.authenticityToken);
      });
    }

    // use sammy forum navigation
    $z("forums/index").initialize();
  },

  // Display the div containing the edit form
  showFormFor: function(postId) {
    $j('#view-post-'+postId).hide();
    $j('#edit-post-'+postId).show();
    tinyMCE.execCommand('mceAddControl', false, "post-edit-field-"+postId);
    InputTracking.fixSubmit($("post-edit-form-"+postId), 'onsubmit');
    $j('#edit-link-for-'+postId).removeAttr('onclick');
    $j('#edit-link-for-'+postId).click(function() {
      $j('#view-post-'+postId).hide();
      $j('#edit-post-'+postId).show();
      return false;
    });
  },

  // Display the div containing the view
  hideFormFor: function(postId) {
    $j('#edit-post-'+postId).hide();
    $j('#view-post-'+postId).show();
    InputTracking.trackedElements = [];
  }
});
