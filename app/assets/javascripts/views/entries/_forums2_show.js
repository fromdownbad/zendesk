$z.defModule('entries/_forums2_show', {
  initialize: function (params) {
    var self = this;
    self.zd_label_answered = $j('div.frame > div.entry span.zd_label.answered');
    $j('div#history.frame > div.item').each(function (index, element) {
      var post_id = $j(element).attr('data-post_id');
      if (params['is_moderator?'] === true) {
        $j(element).find('div.user_formatted span.label_theanswer').click(self.set_the_answer_toggle_option(post_id));
      }
    });

    $j("a[property='is_locked']").click(function(event) {
      $j("div.commenting-controls div.control").toggle();
    });
  },

  // Moderator functions
  
  set_the_answer_toggle_option: function (post_id) { // generates and returns an onClick handler, so that it's easy to refactor this to return a closure with e.g. the post ID for that particular element
    var self = this;
    return(function (event) {
      var answer_span = $j(this);
      answer_span.toggleClass('answered').addClass('ajax');
      var set_answer_label = self.set_answered_label(answer_span.hasClass('answered'));
      $j.post(answer_span.attr('data-update_answer_path'), {is_answer: answer_span.hasClass('answered')}, function(){
        $j.ajax({
          url: answer_span.attr('data-is_answered_path'),
          dataType: 'json',
          type: "GET",
          success: set_answer_label,
          complete: function(){ answer_span.removeClass('ajax');}
        });
      });
    });
  },

  set_answered_label: function (is_answered){
    var self = this;
    if (is_answered === true){
      self.zd_label_answered.addClass('selected');
    } else {
      return function(msg){
        self.zd_label_answered.toggleClass('selected', msg);
      }
    }
    this.zd_label_answered.toggleClass('selected', is_answered);
  },

  set_moderator_toggle_option_for: function (property, on) {
    $(property + '_option').children[0].className = (on ? 'selected' : 'deselected'); 
    $(property + '_option').children[0].className = (on ? 'deselected' : 'selected');
  },

  set_moderator_idea_label: function (flag) {
    $('flag_option').children[0].className = (flag == 1 ? 'selected' : 'deselected'); 
    $('flag_option').children[1].className = (flag == 200 ? 'selected' : 'deselected'); 
    $('flag_option').children[2].className = (flag == 201 ? 'selected' : 'deselected'); 
  }
});