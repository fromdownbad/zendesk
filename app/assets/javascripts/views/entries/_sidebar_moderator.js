$z.defModule('entries/_sidebar_moderator', {
  initialize: function (params) {
    var self = this;
    self.moderatorBox = $j("#moderator_box");
    self.actionDiv = $j('#contentcolumn > div.content > div.action');
    self.adminLabels = $j('#moderator_box > p.labels');
    self.zdLabels = $j('div.entry');
    self.setModeratorToggleOptionFor();
    self.renderModeratorIdeaLabelFor();
  },

  setModeratorToggleOptionFor: function () {
    var self = this;
    self.moderatorBox.find("ul.actions > li > a.mod_option").click(self.selectToggle());
  },

  renderModeratorIdeaLabelFor: function () {
    var self = this;
    self.moderatorBox.find('p.labels > a').click(self.selectToggle());
  },

  selectToggle: function (){
    var self = this;
    var ajaxOptions = {
      url: $j(this).attr('data-deselected_entry_path'),
      data: {authenticity_token: Zendesk.currentUser.authenticityToken},
      dataType: 'json',
      type: 'put'
    };

    var successFunc = function(state, property) {
      return function() {
        self.updatePage(state, property);
      }
    };

    return function(event){
      if ($j(this).hasClass('selected')){
        $j(this).removeClass('selected');
        ajaxOptions.success = successFunc('deselected', $j(this).attr('property'));
        ajaxOptions.url = $j(this).attr('data-deselected_entry_path');
      } else {
        $j(this).addClass('selected');
        ajaxOptions.success = successFunc('selected', $j(this).attr('property'));
        ajaxOptions.url = $j(this).attr('data-selected_entry_path');
      }

      $j.ajax(ajaxOptions);
    }
  },

  updatePage: function(state, property){
    var self = this;
    switch (property) {
      case 'planned':
        if(state === 'selected'){
          self.adminLabels.find('a.topic_label_done').removeClass('selected');
          self.adminLabels.find('a.topic_label_not_planned').removeClass('selected');
          self.zdLabels.find('span.done').removeClass('selected');
          self.zdLabels.find('span.not_planned').removeClass('selected');
          self.zdLabels.find('span.planned').addClass('selected');
        } else {
          self.adminLabels.find('a.topic_label_planned').removeClass('selected');
          self.zdLabels.find('span.planned').removeClass('selected');
          self.zdLabels.find('span.planned').removeClass('selected');
        }
        break;
      case 'done':
        if(state === 'selected'){
          self.adminLabels.find('a.topic_label_planned').removeClass('selected');
          self.adminLabels.find('a.topic_label_not_planned').removeClass('selected');
          self.zdLabels.find('span.planned').removeClass('selected');
          self.zdLabels.find('span.not_planned').removeClass('selected');
          self.zdLabels.find('span.done').addClass('selected');
        } else {
          self.adminLabels.find('a.topic_label_done').removeClass('selected');
          self.adminLabels.find('span.done').removeClass('done');
          self.zdLabels.find('span.done').removeClass('selected');
        }
        break;
      case 'not_planned':
        if(state === 'selected'){
          self.adminLabels.find('a.topic_label_planned').removeClass('selected');
          self.adminLabels.find('a.topic_label_done').removeClass('selected');
          self.zdLabels.find('span.planned').removeClass('selected');
          self.zdLabels.find('span.done').removeClass('selected');
          self.zdLabels.find('span.not_planned').addClass('selected');
        } else {
          self.adminLabels.find('a.topic_label_not_planned').removeClass('selected');
          self.adminLabels.find('span.not_planned').removeClass('done');
          self.zdLabels.find('span.not_planned').removeClass('selected');
        }
        break;
      case 'answered':
        if(state === 'selected'){
          self.zdLabels.find('span.answered').addClass('selected');
        } else {
          self.zdLabels.find('span.answered').removeClass('selected');
        }
      break;
    }
  }

})
