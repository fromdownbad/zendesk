$z.defModule('entries/edit', {
  initialize: function (params) {
    $('entry_title').focus();
    $j('form#entryform').submit(function () {
      if (typeof(entryTagField) != 'undefined') {
        entryTagField.beforeFormSubmit();
      }
      if (typeof(entryPhrasesField) != 'undefined') {
        entryPhrasesField.beforeFormSubmit();
      }
    });
  },
  applyTagsToField: function (field, id, tags, allow_spaces, undefined) {
    var self = this;
    if (self.fields === undefined) {
      self.fields = {};
    }
    if (self.fields[field] !== undefined) {
    }
    else {
      self.fields[field] = null;
    }
    $j(function () {
      function valueTest(value, event) {
        // use "," and <Enter> to separate tags when whitespace is allowed
        if (allow_spaces == true) {
          return event.keyCode == 188 || event.keyCode == 13;
        } else {
          return true;
        }
      }

      self.fields[field] = new Autocompleter.MultiValue(
        id,
        Autocompleter.cachedLookupTag,
        tags,
        { frequency:0.3, minChars:2, acceptNewValues:true, newValueChecker:valueTest }
      );
    });
  }
});
