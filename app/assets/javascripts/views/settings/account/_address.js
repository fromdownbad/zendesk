$j(function() {
  var $unitedStatesStateField = $j("#united-states-state-field"),
      $canadaStateField       = $j("#canada-state-field"),
      $othersStateField       = $j("#others-state-field"),
      $addressCountrySelect   = $j("#address_country_id"),
      $currentStateField      = enableStateField($addressCountrySelect.val());

  $unitedStatesStateField.find('select').select2({
    width:"310px"
  });

  $canadaStateField.find('select').select2({
    width:"310px"
  });

  $addressCountrySelect.change(function(e) {
    $currentStateField.hide();
    $currentStateField.find('input').prop('disabled', true);
    $currentStateField.find('select').prop('disabled', true);
    $currentStateField.find('select').select2('disable');
    $currentStateField = enableStateField(e.val);
  });

  // Show corresponding state field div and enables its select or text field
  function enableStateField(countryId) {
    if (countryId == 156) {
      $unitedStatesStateField.show();
      $unitedStatesStateField.find('select').prop('disabled', false);
      $unitedStatesStateField.find('select').select2('enable');
      return $unitedStatesStateField;
    } else if (countryId == 29) {
      $canadaStateField.show();
      $canadaStateField.find('select').prop('disabled', false);
      $canadaStateField.find('select').select2('enable');
      return $canadaStateField;
    } else {
      $othersStateField.show();
      $othersStateField.find('input').prop('disabled', false);
      return $othersStateField;
    }
  }
});