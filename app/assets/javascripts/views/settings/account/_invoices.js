Zendesk.NS('Zuora');
Zendesk.Zuora.InvoicePage = function() {};

Zendesk.Zuora.InvoicePage.prototype = {
  show: function() {
    var self = this;

    $j('button[data-invoice-id]').click(function(){
      var currentElement   = $j(this);
      var invoiceItemRowId = 'invoice-items-for-' + currentElement.data('invoice-id');
      var invoiceItemRow   = function() { return $j("#" + invoiceItemRowId).first() };

      currentElement.find('span').html(invoiceItemRow().is(':visible') ? 'Details' : 'Hide');

      if(_.any(invoiceItemRow())) {
        invoiceItemRow().toggle();
        return;
      }

      $j(currentElement)
        .addClass('activity')
        .prop('disabled', true);

      $j.ajax({
        url: "/zuora/invoice/invoice_items.json?invoice_id=" + $j(currentElement).data('invoice-id'),
        success: function(data) {
          var invoiceItemRows = self.prepareInvoiceItems(data);

          var invoiceItemTable = $j('<table class="invoice-items">');
          invoiceItemTable.append('<tr><th>Description</th><th class="count">Quantity</th><th class="money">Amount</th><th class="money">Discount</th><th class="money">Total</th></tr>')

          _.each(invoiceItemRows, function(invoiceItem) {
            var discountTotal = invoiceItem.discountAmount() + invoiceItem.discountPercentage();

            var isVoice = invoiceItem.description() == "Voice Usage";
            var quantityContent;

            if(isVoice) {
              quantityContent = "--";
            } else {
              quantityContent = invoiceItem.quantity() + " Agent(s)";
            };

            var invoiceItemRow = $j('<tr>')
              .append($j('<td>').html(invoiceItem.description())
              .append($j('<div>').html(invoiceItem.serviceStartDate() + " to " + invoiceItem.serviceEndDate())))
              .append($j('<td>').addClass("count").html(quantityContent))
              .append($j('<td>').addClass("money").html("$" + invoiceItem.chargeAmount()))
              .append($j('<td>').addClass("money").html("$" + discountTotal))
              .append($j('<td>').addClass("money").html("$" + invoiceItem.totalAmount()));

            invoiceItemTable.append(invoiceItemRow);
          });

          var currentRow = $j(currentElement).closest('tr');
          currentRow.after(
            $j('<tr>')
              .attr('id', invoiceItemRowId)
              .append($j('<td colspan=5>')
                .css('background-color', currentRow.find('td').first().css('background-color'))
                .append(invoiceItemTable)));

          $j(currentElement)
            .removeClass('activity')
            .prop('disabled', false);

        }
      })
    })
  },

  prepareInvoiceItems: function(invoiceItems) {
    var knownStartDates = _.uniq(_.pluck(invoiceItems, 'service_start_date'));

    /*
     * Assume that overlapping charges are in some way related to each other.
     * For example, discounts over the standard rate.
     * Need to confirm this.
     */
    var dateAlignedCharges = _.map(knownStartDates, function(startDate) {
      return _.filter(invoiceItems, function(item) {
        return item.service_start_date == startDate;
      })
    })

    return _.map(dateAlignedCharges, function(charges) {
      return new Zendesk.Zuora.InvoiceItemRow(charges)
    });
  }
};

/* A wrapper around each invoice item row, consisting of at least
 * one InvoiceItem, possibly two if there's a cooresponding
 * discount.
 */
Zendesk.Zuora.InvoiceItemRow = function(attributeArray) {
  this.rawInvoiceItems = attributeArray;

};

Zendesk.Zuora.InvoiceItemRow.prototype = {
  description: function() {
    return this._charge().charge_name;
  },

  serviceStartDate: function() {
    return $j.datepicker.formatDate("mm-dd-yy", new Date(this.rawInvoiceItems[0].service_start_date));
  },

  serviceEndDate: function() {
    return $j.datepicker.formatDate("mm-dd-yy", new Date(this.rawInvoiceItems[0].service_end_date));
  },

  hasDiscount: function() {
    return _.any(this.rawInvoiceItems, function(invoiceItem) {
      return invoiceItem.processing_type == "1";
    });
  },

  discountAmount: function() {
    if(!this.hasDiscount()) { return 0; }
    return parseFloat(this._discount().charge_amount);
  },

  discountPercentage: function() {
    if(!this.hasDiscount()) { return 0; }

    return " (" + this._discount().unit_price + "%)";
  },

  totalAmount: function() {
    return (parseFloat(this.chargeAmount()) + parseFloat(this.discountAmount())).toFixed(2);
  },

  chargeAmount: function() {
    return parseFloat(this._charge().charge_amount).toFixed(2);
  },

  perUnitCharge: function() {
    return parseFloat(this._charge().unit_price).toFixed(2);
  },

  quantity: function() {
    return this._charge().quantity;
  },

  _charge: function() {
    // Assume one charge per invoice item row.
    return _.find(this.rawInvoiceItems, function(invoiceItem) {
      return invoiceItem.processing_type == "0";
    });
  },

  _discount: function() {
    // Assume one discount per invoice item row.
    return _.find(this.rawInvoiceItems, function(invoiceItem) {
      return invoiceItem.processing_type == "1";
    });
  }

};
