;(function($z, $j, _, I18n) {

  // set in initialize
  var options = {
      searchBoostSliderSelector: null,
      onlyEnduserSliderLabel: '',
      onlyAgentsSliderLabel: ''
  };

  function initializeBoostCuratedContentSlider() {
    var valueMappings = {
      "10:1": -0.9,
      "4:1": -0.75,
      "2:1": -0.5,
      "1:1": 0.0,
      "1:2": 0.5,
      "1:4": 0.75,
      "1:10": 0.9
    };
    valueMappings[options.onlyAgentsSliderLabel] = -1;
    valueMappings[options.onlyEnduserSliderLabel] = 1;

    function keyOfValue(value) {
      var key,
          found = _.find(valueMappings, function(eachValue, eachKey) {
            key = eachKey;
            return value === eachValue;
          });
      return found !== undefined && key;
    }

    function onSliderChange(value) {
      // st the hidden input for submit
      $j('#account_settings_boost_curated_content').val(value.toFixed(2));
      // set slider label
      var $label = $j('#curated-search-slider .label'),
          key = keyOfValue(value);
      $label.text(key);
    }

    // create the slider background
    function setupBackGround() {
      var bg = $j("div.slider-bg");
      bg.html("<div/><div/><div/>");
      var divs = $j("div.slider-bg > div"),
          sliderWidth = divs.parent().width(),
          smallWidth = 10,
          spacing = 6;
      divs.filter(':nth-child(1)').css({width: smallWidth, left: 0});
      divs.filter(':nth-child(2)').css({
        width: sliderWidth - smallWidth - smallWidth - 2 * spacing,
        left: smallWidth + spacing
      });
      divs.filter(':nth-child(3)').css({width: smallWidth, left: sliderWidth - smallWidth});
    }

    // this maps the boost_curated_content values to relative slider positions
    // (the absolute position is calculated from the relative position and the slider width)
    // from 10:1 to 1:10 we have a distribution from 0-10
    // since the end points should not be 0 but noticably set apart we
    // extend the whole range to 0...1 + 10 + 1 = 0...12
    var relativePositions = {
      "10:1": 1 + 10 * 0.01,
      "4:1":  1 + 10 * 0.3,
      "2:1":  1 + 10 * 0.4,
      "1:1":  1 + 10 * 0.5,
      "1:2":  1 + 10 * 0.6,
      "1:4":  1 + 10 * 0.7,
      "1:10": 1 + 10 * 0.99
    };
    relativePositions[options.onlyAgentsSliderLabel] = 1 - 0.9;
    relativePositions[options.onlyEnduserSliderLabel] = 1 + 10 + 0.9;
    function valuePosition(val) { return relativePositions[keyOfValue(val)] }

    setupBackGround();
    var values = _.values(valueMappings),
        value = parseFloat($j('#account_settings_boost_curated_content').val()) || 0,
        valuePositions = values.collect(function(value) { return valuePosition(value); });
    $z.Slider.setup(options.searchBoostSliderSelector, {
      onSliderChange: onSliderChange,
      range: [0, 12],
      relativePositions: valuePositions,
      values: values,
      value: value,
      showTicks: true
    });
  }

  $j(function() {
    function showTOS(){
      var tos_confirmed = (confirm(I18n.t('txt.admin.spam_filter.tos_v2'))) ? "sf" : "cm"
      var radio_id = "#account_spam_prevention_" + tos_confirmed;
      // Since we now offer the ability to choose content moderation or
      // Aksimet Spam filter, not confirming the checkbox should simply select the other option.
      $j(radio_id).prop("checked", true);
    }

    $j('#account_spam_prevention_sf').click(function(e){
      showTOS();
    });

    $j("#account_settings_spam_prevention").click(function(e) {
      if($j("#account_settings_spam_prevention").is(":checked")){
        showTOS();
      }
    });
  });

  $z.defModule("settings/portal/_settings", {
    initialize: function(params) {
      options.searchBoostSliderSelector = params && params.sliderId;
      if (!options.searchBoostSliderSelector) { return; }
      options.onlyEnduserSliderLabel = params.enduser_string;
      options.onlyAgentsSliderLabel = params.agents_string;
      $j(initializeBoostCuratedContentSlider);
    }

  });

})(this.Zendesk, this.jQuery, this._, this.I18n);
