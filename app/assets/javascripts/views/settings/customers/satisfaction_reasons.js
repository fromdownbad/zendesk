//= require admin/vendor/admin_assets/admin.js

var lotusClient = require('admin/lotus_client');

$z.defModule('settings/customers/_satisfaction_reasons', {
  inputStyles: '',
  reasons: {},
  maximum: 5,
  minimum: 2,
  nextReasonId: -1,
  dynamicContentRegEx: /{{dc.*}}/g,
  dom: {
    helpers: {
      toStringAttrs: function(attrs) {
        return Object.keys(attrs || {})
          .map(function(key) {
            return key + '="' + attrs[key] + '"';
          })
          .join(' ');
      },
      li: function(str, attrs) {
        return '<li ' + this.toStringAttrs(attrs) + '>' + str + '</li>';
      },
      ul: function(str) {
        return '<ul>' + str + '</ul>';
      },
      strong: function(str) {
        return '<strong>' + str + '</strong>';
      },
      span: function(str, attrs) {
        return '<span ' + this.toStringAttrs(attrs) + '>' + str + '</span>';
      },
      icon: function(attrs) {
        return '<i ' + this.toStringAttrs(attrs) + '>' + '</i>';
      },
      input: function(attrs) {
        return '<input ' + this.toStringAttrs(attrs) + '/>';
      },
      reason: function(attrs) {
        return this.li(
          this.span(attrs.value, { 'class': 'reason_text' }) +
          this.span(
            this.icon({ 'class': 'icon-pencil edit_reason reason_icon' }) +
            this.icon({ 'class': 'icon-trash delete_reason reason_icon' }),
            { 'class': 'reason_icons' }
          ) +
          this.input({
            'data-reason-id': attrs.id,
            'class': 'satisfaction_reason_input non_draggable edit_reason_input',
            style: attrs.inputStyles
          }),
          {
            'class': 'sortable',
            'data-reason-id': attrs.id
          }
        );
      }
    }
  },

  initialize: function (reasons, locale_id, inputStyles) {
    this.inputStyles = inputStyles;
    this.locale_id = locale_id || 1;

    this.initReasons(reasons);
    this.initDom();
    this.initSortables();
    this.initListeners();
    this.render();
  },

  createReason: function(value, translated_value) {
    var reason = {
      id: this.nextReasonId,
      active: false,
      value: value,
      translated_value: translated_value || value,
      deleted: false,
      edited: false
    };
    this.nextReasonId--;

    return reason;
  },

  initDom: function() {
    this.dom.reasonsContainer = $j('#reasons_container');
    this.dom.activeList = this.dom.reasonsContainer.find('.active');
    this.dom.inactiveColumn = this.dom.reasonsContainer.find('.inactive');
    this.dom.inactiveList = this.dom.inactiveColumn.find('ul');
    this.dom.placeholderText = this.dom.reasonsContainer.find('.emptylist_hint_text');
    this.dom.spacesAvailableCount = this.dom.reasonsContainer.find('.spaces_available_count');
    this.dom.satisfactionReasons = this.dom.reasonsContainer.find('#account_satisfaction_reasons');
    this.dom.addReasonButton = this.dom.reasonsContainer.find('.btn._tooltip');
    this.dom.addReasonInput = this.dom.reasonsContainer.find('#satisfaction_reason_input');
    this.dom.addReasonInputHelpText = this.dom.reasonsContainer.find('.satisfaction_reason_input_hint_text');
    this.dom.addReasonInput = this.dom.reasonsContainer.find('.add_reason_input');
    this.dom.addReasonInputHelpText = this.dom.reasonsContainer.find('.add_reason_input_hint_text');
    this.dom.editReasonIcon = this.dom.reasonsContainer.find('.edit_reason');
    this.dom.editReasonInput = this.dom.reasonsContainer.find('.edit_reason_input');
    this.dom.deleteReasonIcon = this.dom.reasonsContainer.find('.delete_reason');
  },

  initSortables: function() {
    this.dom.activeList.find('.sortablelist').sortable({
      placeholder: 'sortable target',
      connectWith: this.dom.inactiveColumn.find('.sortablelist'),
      receive: this.handleActiveSortReceive.bind(this)
    });

    this.dom.inactiveColumn.find('.sortablelist').sortable({
      placeholder: 'sortable target',
      connectWith: this.dom.activeList.find('.sortablelist'),
      receive: this.handleInactiveSortReceive.bind(this),
      over: this.handleInactiveSortover.bind(this),
      cancel: '#reasons_container .non_draggable'
    });

    this.dom.activeList
      .find('.icon-trash.delete_reason')
      .hide();
  },

  initListeners: function() {
    this.dom.addReasonButton.click(this.handleAddReasonButtonClick.bind(this));
    this.dom.addReasonInput.blur(this.handleAddReasonInputBlur.bind(this));
    this.dom.editReasonIcon.click(this.handleShowEditReason.bind(this));
    this.dom.editReasonInput.blur(this.handleEditReasonInputBlur.bind(this));
    this.dom.deleteReasonIcon.click(this.handleDeleteReasonClick.bind(this));
    this.dom.addReasonInput.keydown(this.handleInputKeyDown({
      escape: this.handleEscapeAddReason.bind(this),
      enter: this.handleEnterAddReason.bind(this)
    }));
    this.dom.editReasonInput.keydown(this.handleInputKeyDown({
      escape: this.handleEscapeEditReason.bind(this),
      enter: this.handleEnterEditReason.bind(this)
    }));
  },

  render: function() {
    this.updateSpacesAvailable();
    this.togglePlaceholderText();
    this.serializeReasons();
  },

  getActiveReasons: function () {
    return _.filter(this.reasons, function(reason){
      return reason.active && !reason.deleted;
    });
  },

  getInactiveReasons: function () {
    return _.filter(this.reasons, function(reason){
      return !reason.active && !reason.deleted;
    });
  },

  getSpacesAvailable: function() {
    return _.max([0, this.maximum - this.getActiveReasons().length ]);
  },

  togglePlaceholderText: function() {
    if (this.getInactiveReasons().length === 0) {
      this.dom.placeholderText.show();
    } else {
      this.dom.placeholderText.hide();
    }
  },

  updateSpacesAvailable: function () {
    var newSpacesAvailable;

    switch (this.getSpacesAvailable()) {
      case 0:
        newSpacesAvailable = I18n.t(
          'txt.admin.views.settings.customers._satisfaction.spaces_available.zero',
          { count: this.getSpacesAvailable() }
        );
        break;
      case 1:
        newSpacesAvailable = I18n.t(
          'txt.admin.views.settings.customers._satisfaction.spaces_available.one',
          { count: this.getSpacesAvailable() }
        );
        break;
      default:
        newSpacesAvailable = I18n.t(
          'txt.admin.views.settings.customers._satisfaction.spaces_available.other',
          { count: this.getSpacesAvailable() }
        );
    }
    this.dom.spacesAvailableCount.text(newSpacesAvailable);
  },

  serializeReasons: function() {
    var remappedReasons = _.map(this.reasons, function(reason) {
      return JSON.stringify(reason);
    });
    // the Array.prototype.toJSON has been replaced with a custom function
    // this results in wonky output.
    // the easiest solution to getting normal output for an array is below.
    this.dom.satisfactionReasons.val('[' + remappedReasons + ']');
  },

  setRecordActiveTo: function(value, item) {
    var reasonId = $j(item).data('reason-id');

    _.find(this.reasons, function(reason) {
      return reason.id === reasonId;
    }).active = value;
  },

  errorGrowl: function(heading, error) {
    var growlHeading = this.dom.helpers.strong(heading);
    var growlBody = this.dom.helpers.ul(this.dom.helpers.li(error));

    lotusClient.growl(growlHeading + growlBody, 'error');
  },

  handleDeleteReasonClick: function(e) {
    var $li = $j(e.target).closest('li');
    var innerMessageText = I18n.t('txt.admin.views.settings.customers._satisfaction.delete_reason_modal_label');
    var message = this.dom.helpers.strong(innerMessageText);

    message = message + this.dom.helpers.span($li.find('.reason_text').text().trim(), { style: 'padding-left: 60px' });

    lotusClient.showConfirmationModal({
      message: message,
      cancelLabel: I18n.t('txt.admin.views.settings.customers._satisfaction.delete_reason_modal_cancel_button'),
      confirmLabel: I18n.t('txt.admin.views.settings.customers._satisfaction.delete_reason_modal_delete_button'),
      title: I18n.t('txt.admin.views.settings.customers._satisfaction.delete_reason_modal_heading'),
    }).then(function(){
      this.deleteReason($li.data('reason-id'));
    }.bind(this));
  },

  handleInputKeyDown: function(options) {
    return function(e) {
      options = options || {};
      var supportedKeys = {
        '27': 'escape',
        '13': 'enter'
      };
      var keyPressed = supportedKeys[e.which];
      var functionToBeExecuted = options[keyPressed];

      if (functionToBeExecuted) {
        if (keyPressed === 'enter') {
          e.preventDefault();
        }

        functionToBeExecuted(e);
      }
    };
  },

  handleEscapeAddReason: function() {
    this.hideAndClearAddReasonInput();
    this.showAddReasonButton();
  },

  handleEnterAddReason: function(e) {
    this.addReason(e.target.value.escapeHTML());
    this.hideAndClearAddReasonInput();
    this.showAddReasonButton();
  },

  handleEscapeEditReason: function(e) {
    var $input = $j(e.target);
    var $li = $input.closest('li');
    var $hintText = $li.find('.edit_reason_input_hint_text');

    $input.val('').hide();
    $hintText.hide();
    $li.find('span').show().removeAttr('style');

    this.showAddReasonButton();
    $li.addClass('sortable');
  },

  handleEnterEditReason: function(e) {
    this.editReason($j(e.target).data('reason-id'), e.target.value.escapeHTML());
    this.handleEscapeEditReason(e);
  },

  handleShowEditReason: function(e) {
    var $li = $j(e.target).closest('li[data-reason-id]');
    var $hintText = $li.find('.edit_reason_input_hint_text');
    var $input = $li.find('.edit_reason_input');
    var reasonId = $li.data('reason-id');

    $li.find('span').hide();
    $li.removeClass('sortable');
    $input
      .val(this.reasons[reasonId].value.trim())
      .show()
      .focus();

    $hintText.show();
    this.hideAddReasonButton();
  },

  deleteReason: function(id) {
    if (!this.reasons[id]) { return; }

    if (id < 0) {
      delete this.reasons[id];
    } else {
      this.reasons[id].deleted = true;
    }

    this.dom.reasonsContainer
      .find('li[data-reason-id=' + id + ']')
      .remove();
    this.serializeReasons();
    this.render();
  },

  addReason: function(value) {
    if (!value.length) { return; }

    var reason = this.createReason(value);
    var li = this.dom.helpers.reason({
      id: reason.id,
      value: reason.value.escapeHTML(),
      inputStyles: this.inputStyles
    });
    var $newReason = this.dom.inactiveList.append(li);

    this.reasons[reason.id] = reason;

    $newReason.find('.edit_reason').click(this.handleShowEditReason.bind(this));
    $newReason.find('.edit_reason_input').blur(this.handleEscapeEditReason.bind(this));
    $newReason.find('.edit_reason_input').keydown(this.handleInputKeyDown({
      escape: this.handleEscapeEditReason.bind(this),
      enter: this.handleEnterEditReason.bind(this)
    }));
    $newReason.find('.delete_reason').click(this.handleDeleteReasonClick.bind(this));

    this.serializeReasons();
    this.render();

    if (this.hasDynamicContent(value)) {
      this.translateDynamicContent(reason);
    }
  },

  editReason: function(id, value) {
    var reason = this.reasons[id];
    if ((!value.length || !reason) || value === reason.value.escapeHTML()) { return; }

    reason.value = value;
    reason.translated_value = value;
    reason.edited = true;

    this.updateReasonTextInDom(reason);
    this.serializeReasons();
    this.render();

    if (this.hasDynamicContent(value)) {
      this.translateDynamicContent(reason);
    }
  },

  updateReasonTextInDom: function(reason) {
    this.dom.reasonsContainer
      .find('li[data-reason-id=' + reason.id + '] .reason_text')
      .text(reason.translated_value || reason.value);
  },

  handleAddReasonInputBlur: function(e) {
    this.hideAndClearAddReasonInput();
    this.showAddReasonButton();
    this.togglePlaceholderText();
  },

  handleEditReasonInputBlur: function(e) {
    this.hideAndClearAddReasonInput();
    this.togglePlaceholderText();
  },

  handleAddReasonButtonClick: function(e) {
    this.showAddReasonButton();
    this.showAndFocusAddReasonInput();
    this.hideAddReasonButton();
  },

  handleActiveSortReceive: function(event, ui) {
    if (this.getActiveReasons().length >= this.maximum) {
      $j(ui.sender).sortable('cancel');

      this.errorGrowl(
        I18n.t('txt.admin.views.settings.customers._satisfaction.error_heading_adding_reason'),
        I18n.t('txt.admin.views.settings.customers._satisfaction.error_adding_reason', { count: this.maximum })
      );

      return;
    }
    this.hideDeleteReasonIcon(ui.item);
    this.setRecordActiveTo(true, _.first(ui.item));
    this.render();
  },

  handleInactiveSortReceive: function(event, ui) {
    if (this.getActiveReasons().length <= this.minimum) {
      $j(ui.sender).sortable('cancel');

      this.errorGrowl(
        I18n.t('txt.admin.views.settings.customers._satisfaction.error_heading_removing_reason'),
        I18n.t('txt.admin.views.settings.customers._satisfaction.error_removing_reason', { count: this.minimum })
      );

      return;
    }
    this.showDeleteReasonIcon(ui.item);
    this.setRecordActiveTo(false, _.first(ui.item));
    this.render();
  },

  handleInactiveSortover: function() {
    this.dom.placeholderText.hide();
  },

  showAndFocusAddReasonInput: function() {
    this.dom.addReasonInput.show().focus();
    this.dom.addReasonInputHelpText.show();
  },

  hideAndClearAddReasonInput: function() {
    this.dom.addReasonInput.hide().val('');
    this.dom.addReasonInputHelpText.hide();
  },

  showAddReasonButton: function() {
    this.dom.addReasonButton.show();
  },

  hideAddReasonButton: function() {
    this.dom.addReasonButton.hide();
  },

  hideDeleteReasonIcon: function(item) {
    item.find('.icon-trash.delete_reason').hide();
  },

  showDeleteReasonIcon: function(item) {
    item.find('.icon-trash.delete_reason').show();
  },

  hasDynamicContent: function(str) {
    return str.search(this.dynamicContentRegEx) > -1;
  },

  translateDynamicContent: function(reason) {
    this.fetchDynamicContent(reason.value)
      .then(function(response) {
        reason.translated_value = response.value;
        this.updateReasonTextInDom(reason);
        this.serializeReasons();
      }.bind(this));
  },

  fetchDynamicContent: _.memoize(function(template) {
    var api = '/api/v2/format/dc';
    return $j.post(api, { template: template, locale_id: this.locale_id });
  }),

  initReasons: function(reasonsArray) {
    reasonsArray.forEach(function(reason) {
      this.reasons[reason.id] = reason;
    }.bind(this));
  }
});
