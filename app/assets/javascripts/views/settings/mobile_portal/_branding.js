//= require_tree ../../color_preview
/*globals YAHOO, $j, jQuery, Zendesk, require, Suggest, currentAccount, I18n, $z */
(function($) {
  Zendesk.NS("Branding");
  Zendesk.Branding = {
    init: function ( branding, dynamicContent ) {
      this.branding = branding; // default branding colors
      this.dynamicContent = dynamicContent;
      this.colorCalc = require("views/color_preview/color_calc");
      this.hasLotusPreview = Zendesk.embeddedInLotusAdmin() && $("#inner_branding_styles").length > 0;
      
      this.cache();
      this.imgloader();
      this.autocomplete();
      this.events();
      this.xFrameInit();
      
      if (this.hasLotusPreview) {
        this.preview = require("views/color_preview/preview_client").init();
      }
    },

    cache: function () {
      this.$el = {
        previewHeaderWrapper : $("div#mobile-settings #settings_mobile_preview"),
        previewHeader        : $("div#mobile-settings #mobile-header-wrapper header"),
        previewHeaderTitle   : $("div#mobile-settings #settings_mobile_preview h1"),
        previewHeaderLogo    : $("div#mobile-settings #logo-wrapper img"),
        previewHeaderSearch  : $("div#mobile-settings #search-button"),
        inputTitle           : $("#account_mobile_title"),
        inputFileLogo        : $("#mobile_logo_uploaded_data:file"),
        pageHeaderDarker     : $("#branding_tab_background_color"),
        revertColorsLink     : $("#revert_colors_link"),
        lotusPreviewLink     : $("#preview_color_link"),
        headerColor          : $("#branding_header_color"),
        pageBackgroundColor  : $("#branding_page_background_color"),
        sideboxColor         : $("#branding_sidebox_color"),
        tabBackgroundColor   : $("#branding_tab_background_color"),
        tabHoverColor        : $("#branding_tab_hover_color"),
        textColor            : $("#branding_text_color"),
        classicLogo          : $("#image-block-header_logo img"),
        mobileLogo           : $("#image-block-mobile_logo img"),
        saveButton           : $("#settings_account_branding_save_button"),
        innerBrandingStyles  : $("#inner_branding_styles")
      };
    },

    xFrameInit: function () {
      var globalEvents = require('views/color_preview/global_events');
      require('views/color_preview/x_frame_com').setup();
      globalEvents.on('@action:preview_state_response', this.previewStateHandler.bind(this));
    },

    imgloader: function () {
      this.fileReader = window.FileReader ? new FileReader() : false;
    },

    autocomplete: function () {
      if ( currentAccount.hasFeature("placeholderSuggestions") ) {
        this.suggest = new Suggest();
        this.suggest.suggestions = this.placeholders();
      } else this.suggest = false;
    },

    placeholders: function () {
      var dc, placeholders = {}, placeholder;

      for ( dc in this.dynamicContent ) {
        placeholder = this.dynamicContent[dc];
        placeholders["{{" + dc + "}}"] = placeholder.name;
      }

      return placeholders;
    },

    calculateLogoDimension: function(size) {
      if(size > 55) {
        size = parseInt((size / 114) * 55, 10);
      }
      else
      {
        size = null;
      }

      return size;
    },

    events: function () {
      var _this = this;

      if ( this.fileReader ) {
        this.$el.inputFileLogo.change(function() {
          if ( this.files && this.files[0] ) {
            _this.fileReader.readAsDataURL( this.files[0] );
          }
        });

        this.fileReader.onload = function ( event ) {
            var img = $("<img src='"+event.target.result+"' />").load(function(){
              $(img).attr({
                width : _this.calculateLogoDimension(this.width),
                height : _this.calculateLogoDimension(this.height)
              });

              _this.updateLogo.call(_this, img);
            });
        };
      }

      if ( this.suggest ) {
        this.$el.inputTitle.bind({
          focusin : function ( event ) { _this.suggest.attach( this, event ); },
          focusout : function ( event ) { _this.suggest.detach( this, event ); }
        });
      }

      if ( !Zendesk.embeddedInLotusAdmin() ) {
        this.$el.lotusPreviewLink.remove();
      }

      this.$el.inputTitle.keyup(function() {
        _this.updateTitle();
      });

      this.$el.headerColor.change(function() {
        if (!Zendesk.embeddedInLotusAdmin()) {
          _this.updateHeaderColors();
        }
      });

      this.$el.pageBackgroundColor.change(function() {
        _this.updatePageBgColor();
      });

      this.$el.revertColorsLink.click(function() {
        _this.revertColors();
        _this.updateHeaderColors();
      });

      this.$el.lotusPreviewLink.click(function(){
        _this.preview.startLotusPreview(_this.$el.headerColor.val(), 'preview_manager_start');
      });


      this.$el.previewHeaderWrapper.click(function() {
        return false;
      });

      this.$el.saveButton.click(function(event) {
        if ( _this.doubleCheck() || !( _this.colorElementsPresent() ) ) {
          if (_this.hasLotusPreview) {
            _this.preview.setBranding(_this.$el.headerColor.val());
          }
          return true;
        }
        var msg = I18n.t("txt.admin.views.settings.account._branding.hex_not_valid");
        alert( msg );
        event.preventDefault();
        return false;
      });
    },

    updateLogo: function ( img ) {
      this.$el.previewHeaderLogo.replaceWith(img);
      this.$el.previewHeaderLogo = img;
    },

    updateTitle: function () {
        var title = this.$el.inputTitle.val(),
            dynamicTitle = this.dynamicTitle( title );
        this.$el.previewHeaderTitle.text( dynamicTitle );
    },

    dynamicTitle: function ( title ) {
      var reg = new RegExp("\{\{([^\}]{2,})\}\}", "gi"),
          _this = this;
      return title.replace( reg, function ( placeholder ) {
        var key = placeholder.substring( 2, placeholder.length-2 );
        return _this.dynamicContent[key].value;
      });
    },

    colorElementsPresent: function () {
      return this.$el.pageBackgroundColor.length && this.$el.headerColor.length;
    },

    updatePageBgColor: function () {
      var color = this.$el.pageBackgroundColor.val(),
          hex = this.colorCalc.sanitize( color );

      if ( !hex ) return false;
      this.$el.pageBackgroundColor.val( hex ); // ensures valid hex color
      return true;
    },

    updateHeaderColors: function () {
      var color = this.$el.headerColor.val();
      var colors = this.colorCalc.calculateColors(color);
      if ( !colors ) return false;

      this.updateInputFields( colors.bgColor, colors.tabBgColor, colors.tabHoverColor, colors.textColor );
      this.updateHeaderPreview( colors.bgColor, colors.tabBgColor, colors.borderColor, colors.textColor );
      this.updateLogoBackgrounds( colors.bgColor );

      return true;
      //TODO: Ask PM's about text and border color
    },

    updateInputFields: function ( bgColor, tabBgColor, tabHoverColor, textColor ) {
      this.$el.headerColor.val( bgColor );
      this.$el.tabBackgroundColor.val( tabBgColor );
      this.$el.tabHoverColor.val( tabHoverColor );
      this.$el.textColor.val( textColor );
    },

    updateHeaderPreview: function ( bgColor, bgColorDarker, borderColor, textColor ) {
      this.$el.previewHeader
        .css({ "background-color" : "#" + bgColor })
        .css({ "background-image" : "-webkit-linear-gradient(top, #" + bgColor + ", #" + bgColorDarker + ")" })
        .css({ "background-image" :    "-moz-linear-gradient(top, #" + bgColor + ", #" + bgColorDarker + ")" })
        .css({ "background-image" :     "-ms-linear-gradient(top, #" + bgColor + ", #" + bgColorDarker + ")" })
        .css({ "background-image" :      "-o-linear-gradient(top, #" + bgColor + ", #" + bgColorDarker + ")" })
        .css({ "background-image" :         "linear-gradient(top, #" + bgColor + ", #" + bgColorDarker + ")" });

      this.$el.previewHeaderSearch.css({ "background-color" : "#" + bgColorDarker });
      this.$el.previewHeader.css({ "border-bottom-color" : "#" + borderColor });
      this.$el.previewHeaderTitle.css({
        "color" : "#" + textColor,
        "text-shadow" : textColor === "FFFFFF" ? "0px -1px rgba(0, 0, 0, 0.5)" : "none"
      });
    },

    previewStateHandler: function (data) {
      if (data.memo.previewing){
        this.$el.headerColor.val(data.memo.previewing);
      }
    },

    revertColors: function () {
      this.$el.sideboxColor.val( this.branding.sidebox_color );

      this.$el.headerColor
        .css( "background-color", "#" + this.branding.header_color )
        .val( this.branding.header_color );

      this.$el.pageBackgroundColor
        .css("background-color", "#" + this.branding.page_background_color )
        .val( this.branding.page_background_color );
    },

    updateLogoBackgrounds: function ( bgColor ) {
      this.$el.classicLogo.css({ "background-color" : "#" + bgColor });
      this.$el.mobileLogo.css({ "background-color" : "#" + bgColor });
    },

    doubleCheck: function () {
      // run the color updates again to make sure everything is ok
      return this.updateHeaderColors() && this.updatePageBgColor();
    }
  };
}(jQuery));

$z.defModule("settings/account/_branding", {
  initialize: function ( branding, dynamicContent ) {
    Zendesk.Branding.init( branding, dynamicContent );
  }
});
