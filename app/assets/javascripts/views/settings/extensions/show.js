/*global $j, $z, I18n, Zendesk */

$z.defModule('settings/extensions/show', {
  initialize: function (params) {
    $j('#crm_integration').change(function() {
      var value = $j(this).val();
      if(value == "SalesforceIntegration") {
        $j('#sugar, #ms_dynamics').hide();
        $j('#salesforce').show();
      } else if(value == "SugarCrmIntegration") {
        $j('#salesforce, #ms_dynamics').hide();
        $j('#sugar').show();
      } else if(value == "MsDynamicsIntegration") {
        $j('#salesforce, #sugar').hide();
        $j('#ms_dynamics').show();
      }
    });

    $j('#ms_dynamics_integration_type_code').change(function() {
      var value = $j(this).val();
      if (value === "0") {
        $j('.ms_dynamics_on_premise').hide();
        $j('.ms_dynamics_on_ifd').hide();
        $j('.ms_dynamics_on_cloud').show();
      } else if (value === "1") {
        $j('.ms_dynamics_on_cloud').hide();
        $j('.ms_dynamics_on_ifd').hide();
        $j('.ms_dynamics_on_premise').show();
      } else {
        $j('.ms_dynamics_on_cloud').hide();
        $j('.ms_dynamics_on_premise').hide();
        $j('.ms_dynamics_on_ifd').show();
      }
    });

    $j('#ms_dynamics_integration_type_code').change();

    $j('#settings_extensions_ms_dynamics_save_button').click(function() {
      $j(this).val(I18n.t('txt.admin.public.javascript.views.settings.extensions.testing_connection_please_wait'))
              .prop("disabled", true);
      $j("#ms_dynamics_form").submit();
    });

    $j('#settings_extensions_sugar_crm_save_button').click(function() {
      $j(this).val(I18n.t('txt.admin.public.javascript.views.settings.extensions.testing_connection_please_wait'))
              .prop("disabled", true);
      $j("#sugar_crm_form").submit();
    });

    $j('#settings_extensions_salesforce_connect_button').click(function() {
      this.href = "/settings/extensions/connect_to_salesforce?salesforce_environment=" + $j('select#salesforce_environment option:selected').val();
      Zendesk.Instrumentation.track("Enabling salesforce.com", "CRM");
    });

    Zendesk.SalesforceConfiguration.init();
  }
});
