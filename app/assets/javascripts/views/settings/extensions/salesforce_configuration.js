/*global Zendesk, jQuery, I18n, Ordering, submit_sortable_list, alert */
(function($) {

  Zendesk.SalesforceConfiguration = {

    init: function() {
      this.cache();
      this.events();
    },

    cache: function() {
      this.$openEditorLnk   = $(".edit_salesforce_object");
      this.$openFilterLnk   = $(".edit_object_filter");
      this.$expandLnk       = $(".expand_link");
      this.$contractLnk     = $(".contract_link");
      this.$selectLnk       = $(".add_link a");
      this.$deselectLnk     = $(".remove_link a");
      this.$objectForm      = $("#salesforce_object_form");
      this.$filterForm      = $("#salesforce_filter_form");
      this.$resetLnk        = $("#reset_fields");
      this.$confSelector    = $('#account_settings_use_salesforce_configuration');
      this.$configuration   = $('#salesforce_configuration');
      this.$objectContainer = $('#salesforce_objects_configuration');
      this.$lookupChx       = $('#account_settings_salesforce_integration');
      this.$lookupField     = $('#lookup_field');
      this.$confFields      = $('#conf_fields');
      this.boxWidth         = "820px";
      this.boxHeight        = "700px";
      this.sortTarget       = "salesforce_selected_objects";
      this.sortContainer    = "salesforce_selected_objects_sort";
      this.sortList         = "salesforce_selected_objects_sort_list";
    },

    events: function() {
      var _this = this;

      this.$openEditorLnk.live("click", function() {
        $.colorbox({
          href: $(this).attr('href'),
          width: _this.boxWidth,
          height: _this.boxHeight,
          onComplete: function() {
            _this.objSelector().change();
          }
        });
        return false;
      });

      this.$openFilterLnk.live("click", function() {
        $.colorbox({
          href: $(this).attr('href'),
          width: _this.boxWidth,
          height: "380px",
          onComplete: function() {
            $('#date_filter_value').datepicker();
            $("#regular_filter_field, #date_filter_field").change();
          }
        });
        return false;
      });

      $("#regular_filter_field, #date_filter_field").live("change", function() {
        if ($(this).val() === "") {
          $(this).addClass("empty");
        } else {
          $(this).removeClass("empty");
        }
      });

      this.$expandLnk.live("click", function() {
        var folder, children;

        folder = $(this).closest(".folder");
        children = _this.getChildren(folder);

        if (folder.data("loaded") === false) {
          _this.loadFolder(folder, children, $(this));
        }

        _this.expandFolder(folder, children);

        return false;
      });

      this.$contractLnk.live("click", function() {
        var folder, children;

        folder = $(this).closest(".folder");
        children = _this.getChildren(folder);

        _this.contractFolder(folder, children);

        return false;
      });

      this.$selectLnk.live("click", function() {
        var field = $(this).closest(".field").data("field");

        _this.selectField(field);

        return false;
      });

      this.$deselectLnk.live("click", function() {
        var field = $(this).closest("li").data("field");

        _this.deselectField(field);

        return false;
      });

      this.$objectForm.live("submit", function() {
        if ($("#mapping_salesforce_field option").length === 0) {
          alert(I18n.t("txt.admin.views.settings.extensions._salesforce3.select_some_field_alert"));
          return false;
        }

        var formData, labels, relationships;

        formData = $(this).serializeArray();
        labels = {};
        relationships = {};

        // Append selected fields
        $('.selected_fields li.field.selected').each(function() {
          var field, title, folder;

          field = $(this).data("field");
          title = $(this).data("title");
          formData.push({ name: "fields[]", value: field });
          labels[field] = title;

          folder = _this.getFolder($(this));
          if (folder.exists()) {
            relationships[folder.data("relationship")] = { object: folder.data("object"), type: folder.data("type") };
          }
        });

        // Append object labels and relationships
        formData.push({ name: "object_label", value: _this.selectedObjectLabel() });

        formData.push({ name: "labels", value: JSON.stringify(labels) });
        formData.push({ name: "relationships", value: JSON.stringify(relationships) });

        // Append mapped salesfroce field type
        formData.push({ name: "mapping_salesforce_field_type", value: $("#mapping_salesforce_field option:selected").data("type") });

        // Append authenticity token
        formData.push({ name: "authenticity_token", value: Zendesk.currentUser.authenticityToken });

        $.post(_this.saveObjectUrl(), $.param(formData))
          .done(function(data) {
            _this.$objectContainer.html(data);
          })
          .fail(function(jqXHR) {
            _this.$objectContainer.prepend(jqXHR.responseText);
          })
          .always(function() {
            _this.setupSort();
            $.colorbox.close();
          });
        return false;
      });

      this.$filterForm.live("submit", function() {
        var formData, filters, regularFilter, dateFilter;

        formData = [];

        formData.push({ name: "object", value: $("#object_name").val() });
        formData.push({ name: "relationship_name", value: $("#relationship_name").val() });
        formData.push({ name: "limit", value: $("#limit").val() });

        regularFilter = {
          "field": $("#regular_filter_field").val(),
          "type": $("#regular_filter_field option:selected").data("type"),
          "operator": $("#regular_filter_operator").val(),
          "value": $("#regular_filter_value").val()
        };

        dateFilter = {
          "field": $("#date_filter_field").val(),
          "type": $("#date_filter_field option:selected").data("type"),
          "operator": $("#date_filter_operator").val(),
          "value": $("#date_filter_value").val()
        };

        formData.push({ name: "filters[]", value: JSON.stringify(regularFilter) });
        formData.push({ name: "filters[]", value: JSON.stringify(dateFilter) });

        // Append authenticity token
        formData.push({ name: "authenticity_token", value: Zendesk.currentUser.authenticityToken });

        $.post(_this.saveFilterUrl(), $.param(formData))
          .done(function(data) {
            // Nothing
          })
          .fail(function(jqXHR) {
            $('#salesforce_configuration').prepend(jqXHR.responseText);
          })
          .always(function() {
            $.colorbox.close();
          });

        return false;
      });

      this.$resetLnk.live("click", function() {
        $("#regular_filter_field, #regular_filter_value, #date_filter_field, #date_filter_value").val("");
        $("#limit").val("5");
        $("#regular_filter_field, #date_filter_field").change();
        return false;
      });

      this.objSelector().live("change", function() {
        var fieldsContainer, loadingMessage, object, label;

        object = $(this).val() || "";
        label = _this.selectedObjectLabel();

        fieldsContainer = $('#salesforce_fields');
        loadingMessage = _this.loadingImage() + "  " + I18n.t("txt.admin.views.settings.extensions._salesforce3.loading_fields", { object_name: label });

        fieldsContainer.html('<div class="salesforce_item">' + loadingMessage + '</div>');
        fieldsContainer.load(_this.fieldsUrl(object), function() {
          _this.setupFieldsSort();
          _this.addMappingSalesforceFields();
        });
      });

      this.testForm().find(":submit").live("click", function() {
        var fields, testData, crmApp;

        fields = _this.testForm().find("input[type=text]");
        testData = fields.serializeArray();

        crmApp = new Zendesk.CRM.Application(new Zendesk.CRM.SidebarRenderer());
        crmApp.fetchAndRenderData(_this.testUrl() + "?format=json&" + $.param(testData));

        return false;
      });

      this.$confSelector.change(function() {
        var value = $(this).val();
        if (value === "0") {
          _this.$configuration.slideUp();
        } else {
          _this.$configuration.slideDown();
        }
      });

      this.$confSelector.change();

      this.$lookupChx.change(function() {
        if ($(this).is(':checked')) {
          _this.$confFields.slideDown();
        } else {
          _this.$confFields.slideUp();
        }
      });

      this.$lookupChx.change();

      $("a#cancel_sorting").live("click", function() {
        Ordering.cancelOrdering("#" + _this.sortTarget);
        return false;
      });

      $("a#start_sorting").live("click", function() {
        Ordering.SetOrder("#" + _this.sortTarget);
        return false;
      });

      this.setupSort();
    },

    selectField: function(field) {
      var fieldElement, folder;

      fieldElement = $('li[data-field="' + field + '"]');
      folder = this.getFolder(fieldElement);

      fieldElement.removeClass("non_selected");
      fieldElement.closest("li").addClass("selected");

      folder.removeClass("non_selected");
      folder.closest("li").addClass("selected");

      // Add option in the Salesforce mapping pulldown
      this.addMappingSalesforceField(fieldElement);
    },

    deselectField: function(field) {
      var fieldElement, folder;

      fieldElement = $('li[data-field="' + field + '"]');
      fieldElement.removeClass("selected");
      fieldElement.closest("li").addClass("non_selected");

      folder = this.getFolder(fieldElement);

      if (this.getChildren(folder).find("li.field.selected").length === 0) {
        folder.removeClass("selected");
        folder.closest("li").addClass("non_selected");
      }

      // Remove the option from the mapping pulldown
      this.mappingSalesforceField().find("option[value='" + field + "']").remove();
    },

    expandFolder: function(folder, children) {
      children.show();
      folder.addClass("expanded");
      folder.removeClass("contracted");
    },

    contractFolder: function(folder, children) {
      children.hide();
      folder.addClass("contracted");
      folder.removeClass("expanded");
    },

    addMappingSalesforceField: function(fieldElement) {
      var field, type, title, selected;

      field = fieldElement.data("field");
      type = fieldElement.data("type");
      title = fieldElement.data("title");

      selected = (field === this.mappingSalesforceField().data("selected")) ? "selected=selected" : "";

      if (!field.include("::") && type !== "textarea") {
        this.mappingSalesforceField().append('<option value="' + field + '" data-type="' + type + '" ' + selected + '>' + title + '</option>');
      }
    },

    addMappingSalesforceFields: function() {
      var _this = this;

      $('.selected_fields li.field.selected').each(function() {
        _this.addMappingSalesforceField($(this));
      });
    },

    getChildren: function(folder) {
      return folder.parent().find(".children");
    },

    getFolder: function(field) {
      return field.parents(".folder_container").find(".folder");
    },

    fieldsUrl: function(object) {
      var params = { object: object };
      return "extensions/edit_salesforce_fields?" + $.param(params);
    },

    relatedFieldsUrl: function(folder) {
      var params = { object: folder.data("object"), relationship_name: folder.data("relationship") };
      return "extensions/salesforce_related_fields?" + $.param(params);
    },

    saveObjectUrl: function() {
      return "extensions/save_salesforce_object";
    },

    saveFilterUrl: function() {
      return "extensions/save_object_filter";
    },

    testUrl: function() {
      return "/crm/sync_ticket_info";
    },

    setupSort: function() {
      $("#" + this.sortList).sortable();
      submit_sortable_list(this.sortTarget,
                           this.sortContainer,
                           this.sortList,
                           "extensions/sort_salesforce_objects");
    },

    setupFieldsSort: function() {
      var fields = $(".selected_fields").data("fields");

      $('.selected_fields .primary.selected').sortElements(function(a, b) {
        return fields.indexOf($(a).data("field")) > fields.indexOf($(b).data("field")) ? 1 : -1;
      });

      $('.selected_fields .folder.selected').each(function() {
        $(this).parent().find(".field.selected").sortElements(function(a, b) {
          return fields.indexOf($(a).data("field")) > fields.indexOf($(b).data("field")) ? 1 : -1;
        });
      });

      $(".selected_fields .primary_sortable_fields").sortable({
        cursor: "move",
        items: "li.selected.field.primary"
      }).disableSelection();

      $(".selected_fields .related_sortable_fields").sortable({
        cursor: "move",
        items: "li.selected.field.related"
      }).disableSelection();
    },

    // Folders are lazy. This is to get the folder fields from server
    loadFolder: function(folder, children) {
      var _this, contractLink, contractImage;

      _this = this;
      contractLink = folder.find(".contract_link");
      contractImage = contractLink.html();

      contractLink.html(this.loadingImage);

      children.load(_this.relatedFieldsUrl(folder), function() {
        folder.data("loaded", true);
        _this.copyFolder(folder.data("relationship"), children);
        contractLink.html(contractImage);
        _this.setupFieldsSort();
      });
    },

    loadingImage: function() {
      return '<img src="/images/ajax_loader_small.gif" style="width:9px">';
    },

    // Copy folder content and current state to selected tree
    copyFolder: function(relationship, children) {
      var selectedFolder = this.selectedFields().find('li[data-relationship="' + relationship + '"]');
      selectedFolder.data("loaded", true);
      selectedFolder.addClass("expanded");
      selectedFolder.removeClass("contracted");
      this.getChildren(selectedFolder).html(children.html());
    },

    selectedObjectLabel: function() {
      return this.objSelector().find("option:selected").html();
    },

    selectedFields: function() {
      return $(".selected_fields");
    },

    testForm: function() {
      return $("#crm_test_form");
    },

    objSelector: function() {
      return $("#salesforce_object");
    },

    mappingSalesforceField: function() {
      return $('#mapping_salesforce_field');
    }

  };


}(jQuery));
