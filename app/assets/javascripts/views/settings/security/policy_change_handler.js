;(function(exports, $) {

  var roleMapping = {
        'agents':    'agent_zendesk_login',
        'end_users': 'end_user_zendesk_login'
      };

  var PolicyChangeHandler = function PolicyChangeHandler(root, currentPolicyID) {
    this.scope           = $('#' + root + ' .' + roleMapping[root]);
    this.currentPolicyID = currentPolicyID;

    var radioButtons = this.scope.find('input[type="radio"]');

    radioButtons.click($.proxy(this.displayWarningBox, this));
    radioButtons.click($.proxy(this.toggleCustomPolicyForm, this));
  };

  PolicyChangeHandler.prototype.displayWarningBox = function displayWarningBox(e) {
    var clickedButton    = $(e.currentTarget),
        selectedPolicyID = parseInt(clickedButton.val()),
        warningBox       = this.scope.find('.warning');

    if (selectedPolicyID > this.currentPolicyID) {
      warningBox.show();
    } else {
      warningBox.hide();
    }
  };

  PolicyChangeHandler.prototype.toggleCustomPolicyForm = function toggleCustomPolicyForm(e) {
    var customPolicyRadioButton = $('#account_role_settings_agent_security_policy_id_400'),
        target                  = $('#custom_security_policy_options');

    if (customPolicyRadioButton.is(':checked')) {
      target.slideDown();
    } else {
      target.slideUp();
    }
  };

  exports.PolicyChangeHandler = PolicyChangeHandler;

})(window.Zendesk, jQuery);
