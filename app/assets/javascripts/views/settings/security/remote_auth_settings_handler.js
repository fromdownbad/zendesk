;(function(exports, $) {

  var nextToken,
      authModes = { 1: 'native', 3: 'jwt' },
      roots     = [ '#agents', '#end_users' ];

  var RemoteAuthSettingsHandler = function RemoteAuthSettingsHandler(authObj, generatedToken) {
    this.authObj = authObj;
    nextToken = generatedToken;
    this.initializeFormDisplay();
  };

  RemoteAuthSettingsHandler.prototype.initializeFormDisplay = function initializeFormDisplay() {
    var remoteAuthType = authModes[this.authObj.auth_mode];

    $('.remote_auth_selection').click(this.toggleRemoteAuthType);
    $(roots).each(function(i, root) {
      $(root).find('.option.' + remoteAuthType + ' .generate_remote_auth_token')
        .click(this.displayNewSharedSecret);
    }.bind(this));
  };

  RemoteAuthSettingsHandler.prototype.displayActiveForm = function displayActiveForm() {
    var authMode = this.settings.auth_mode,
        inactiveForm = (authMode === 2) ? '.zendesk_remote_auth' : '.saml_remote_auth';

    this.root.find(inactiveForm).hide();
  };

  RemoteAuthSettingsHandler.prototype.displayNewSharedSecret = function displayNewSharedSecret() {
    var target = $(this).closest('.option');

    target.find('.new_shared_secret input').val(nextToken);
    target.find('.existing_shared_secret').hide();
    target.find('.new_shared_secret').show();
    return false;
  };

  RemoteAuthSettingsHandler.prototype.toggleRemoteAuthType = function toggleRemoteAuthType(e) {
    var checkbox = $(this);
        target   = checkbox.closest('.option').find('.remote_auth');

    if (checkbox.is(':checked')) {
      target.slideDown();
    } else {
      target.slideUp();
    }
  };

  exports.RemoteAuthSettingsHandler = RemoteAuthSettingsHandler;

})(window.Zendesk, jQuery);
