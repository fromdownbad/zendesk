;(function(exports, $) {

  var DISPLAY_NAMES = {
        account_remote_authentications_saml_is_active:
        I18n.t('txt.admin.views.settings.security._authentication.sam_label'),
        account_remote_authentications_jwt_is_active:
        I18n.t('txt.admin.views.settings.security._authentication.jwt_label')
      };

  var numberOfSSOMethodsSelected = function numberOfSSOMethodsSelected(checkboxes) {
        var count = 0;

        $(checkboxes).each(function(i, checkbox) {
          if ($(checkbox).is(':checked')) {
            count += 1;
          }
        });

        return count;
      };

  var hasAnySSOMethodsSelected = function hasAnySSOMethodsSelected(checkboxes) {
    return numberOfSSOMethodsSelected(checkboxes) >= 1;
  };

  var hasMultipleSSOMethodsSelected = function hasMultipleSSOMethodsSelected(checkboxes) {
    return numberOfSSOMethodsSelected(checkboxes) > 1;
  };

  var SSOSettings = function SSOSettings(selectedPrimarySSO) {
    this.selectedPrimarySSO = selectedPrimarySSO.toString();
    this.initializeFormDisplay();
  };

  SSOSettings.prototype.initializeFormDisplay = function initializeFormDisplay() {
    $('.primary_sso_select').change(this.updateSelectedPrimarySSO.bind(this));
    $('.remote_auth_selection').click(this.togglePrimarySSOSection.bind(this));
    $('input[type=checkbox].password_allowed').change(this.toggleSSOBypass.bind(this));
    $('.login_service').live('login_service.change', this.toggleSSOBypassForm.bind(this));
  };

  SSOSettings.prototype.updateSelectedPrimarySSO = function updateSelectedPrimarySSO(e) {
    var select = $(e.target);
    this.selectedPrimarySSO = select.val();
  };

  SSOSettings.prototype.toggleSSOBypass = function toggleSSOBypass(e) {
    var $checkbox       = $(e.target),
        $ssoSection     = $checkbox.closest('.login_service'),
        $ssoBypass      = $ssoSection.find('div.sso_bypass');

    if ($checkbox[0].checked) {
      $ssoBypass.show();
    } else {
      $ssoBypass.hide();
    }
  };

  SSOSettings.prototype.toggleSSOBypassForm = function toggleSSOBypassForm(e) {
    var $loginService = $(e.target);
    var isVisible = $loginService.hasClass('selected');
    $loginService.find('.zendesk_passwords select, .zendesk_passwords input').prop('disabled', !isVisible);
  };

  SSOSettings.prototype.togglePrimarySSOSection = function togglePrimarySSOSection(e) {
    var checkbox       = $(e.target),
        ssoSection     = checkbox.closest('.login_service'),
        allCheckboxes  = ssoSection.find('.remote_auth_selection'),
        primarySSO     = ssoSection.find('.primary_sso'),
        alternateLogin = ssoSection.find('.alternate_login');

    if (hasMultipleSSOMethodsSelected(allCheckboxes)) {
      this.setPrimarySSOOptions(ssoSection);
      primarySSO.removeClass('hidden');
    } else {
      primarySSO.addClass('hidden');
    }

    if (hasAnySSOMethodsSelected(allCheckboxes)) {
      alternateLogin.removeClass('hidden');
    } else {
      alternateLogin.addClass('hidden');
    }
  };

  SSOSettings.prototype.setPrimarySSOOptions = function setPrimarySSOOptions(ssoSection) {
    var allCheckboxes = ssoSection.find('.remote_auth_selection'),
        selectBox     = ssoSection.find('.primary_sso_select'),
        selectedValue = selectBox.val(),
        selectOptions = '';

    $(allCheckboxes).each(function(i, checkboxElm) {
      var checkbox    = $(checkboxElm),
          checkboxId  = checkbox.siblings('.remote_auth_id').val();

      if (checkbox.is(':checked')) {
        var isPrimarySSO = this.selectedPrimarySSO === checkboxId,
            elmOpts      = {
              value: checkboxId,
              html: DISPLAY_NAMES[checkbox.attr('id')]
            };

        if (isPrimarySSO) { $.extend(elmOpts, {selected: 'selected'}); }

        selectOptions += $('<option>', elmOpts).prop('outerHTML');
      }
    }.bind(this));

    if (selectOptions) { selectBox.html(selectOptions); }
  };

  exports.SSOSettings = SSOSettings;

})(window.Zendesk, jQuery);
