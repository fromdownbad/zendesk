;(function(exports, $) {

  var TRUE  = 1,
      FALSE = 0,
      ZENDESK_LOGIN_REGEX = /zendesk_login$/
      ;

  function displayEnabledLoginServices(root, loginServices) {
    var target;

    $.each(loginServices, function(loginService, enabled) {
      target = root.find('.login_service.' + loginService);
      if (target) {
        if (enabled && (!ZENDESK_LOGIN_REGEX.match(loginService) || shouldDisplayZendeskLoginService(loginServices))) {
          target.addClass('selected');
          target.find('.options').show();
        }
        target.trigger('login_service.change');
      }
    });
  }

  function shouldDisplayZendeskLoginService(loginServices) {
    if (loginServices.agent_zendesk_login !== undefined) {
      return loginServices.agent_zendesk_login && !loginServices.agent_google_login && !loginServices.agent_office_365_login && !loginServices.agent_remote_login;
    } else {
      return loginServices.end_user_zendesk_login && !loginServices.end_user_remote_login;
    }
  }

  function updateLoginServiceSelectionValues(elm, root) {
    var serviceChoices = root.find('.service_choice');

    serviceChoices.each(function(i, serviceChoice) {
      $(serviceChoice).val(FALSE);
    });
    elm.find('.service_choice').val(TRUE);
    $(serviceChoices).change();
  }

  var LoginSettingsHandler = function LoginSettingsHandler(root, settings) {
    this.settings = settings;
    this.root = $('#' + root);
    this.initializeFormDisplay();
  };

  LoginSettingsHandler.prototype.initializeFormDisplay = function initializeFormDisplay() {
    displayEnabledLoginServices(this.root, this.settings);
    this.root.find('.login_service .head').click($.proxy(this.selectLoginService, this));
  };

  LoginSettingsHandler.prototype.selectLoginService = function selectLoginService(e) {
    var clickedElm = $(e.currentTarget),
        loginServiceElm;

    this.root.find('.login_service .head').each($.proxy(function(i, elm) {
      if (elm === clickedElm[0]) { return; }
      loginServiceElm = $(elm).closest('.login_service');
      loginServiceElm.removeClass('selected');
      loginServiceElm.trigger('login_service.change');
      loginServiceElm.find('.options').slideUp();
    }, this));

    loginServiceElm = clickedElm.closest('.login_service');
    loginServiceElm.addClass('selected');
    loginServiceElm.trigger('login_service.change');
    loginServiceElm.find('.options').slideDown();

    updateLoginServiceSelectionValues(loginServiceElm, this.root);
  };

  exports.LoginSettingsHandler = LoginSettingsHandler;

})(window.Zendesk, jQuery);
