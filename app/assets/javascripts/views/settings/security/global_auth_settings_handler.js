;(function(exports, $) {
  var GlobalAuthSettingsHandler = function GlobalAuthSettingsHandler(settings) {
    this.settings = settings;
    this.initializeGlobalSettings();

    $('#account_settings_ip_restriction_enabled').change(this.enableIPRestrictions);
    $('#enable_account_assumption').change(this.enableAccountAssumption);
    $('#account_settings_assumption_duration').change(this.setupAccountAssumption);
    $('input.button.save').click(this.validateAndSubmit);
  };

  GlobalAuthSettingsHandler.prototype.initializeGlobalSettings = function initializeGlobalSettings() {
    if (this.settings.ip_restriction_enabled === 'true') {
      $('#sub_setting_ip_restrictions').addClass('enabled');
    }

    var assumptionSelect = $('.assumption-selectbox'),
        assumptionCheckbox = $('#enable_account_assumption'),
        audioeyeCheckbox = $('#account_settings_agreed_to_audioeye_tos');

    if (audioeyeCheckbox) {
      new AudioEyeTOSModal(audioeyeCheckbox);
    }

    assumptionCheckbox.is(':checked') ? assumptionSelect.show() : assumptionSelect.hide();
  };

  GlobalAuthSettingsHandler.prototype.enableAccountAssumption = function enableAccountAssumption() {
    var enabled = $(this).is(':checked'),
        assumptionSelectSection = $('.assumption-selectbox');
        assumptionSelectValue = $('#account_settings_assumption_duration :selected')

    if (enabled) {
      assumptionSelectSection.show()
    } else {
      assumptionSelectValue.val('off');
      assumptionSelectSection.hide();
    }
  };

  GlobalAuthSettingsHandler.prototype.validateAndSubmit = function validateAndSubmit(e) {
    var assumptionCheckbox = $('#enable_account_assumption'),
        assumptionSelectValue = $('#account_settings_assumption_duration :selected').val(),
        assumptionErrorMessage = $('.assumption_error');

    if (assumptionCheckbox.is(':checked') && (assumptionSelectValue == 'not_selected')) {
      e.preventDefault();
      showFlash(I18n.t('txt.admin.views.settings.security_policy.show.global.account_assumption.error_message'), 'error');
    }
  };

  GlobalAuthSettingsHandler.prototype.enableIPRestrictions = function enableIPRestrictions() {
    var enabled = $(this).is(':checked'),
        target  = $('#sub_setting_ip_restrictions');

    enabled ? target.slideDown() : target.slideUp();
  };

  exports.GlobalAuthSettingsHandler = GlobalAuthSettingsHandler;

  var AudioEyeTOSModal = function(checkBox) {
    this.$root = checkBox.parents('.enable_audioeye_checkbox');
    this.$checkBox = checkBox;
    this.$checkBox.click(this.displayModal.bind(this, false));

    $('button.enable-audioeye').live('click', this.acceptTOS.bind(this));
    this.$root.find('a.audioeye-notice').click(this.displayModal.bind(this, true));
  };

  AudioEyeTOSModal.prototype = {
    displayModal: function(displayAsNotice, event) {
      // we should only display the modal if the checkbox is unchecked
      if (!displayAsNotice && !this.$checkBox[0].checked) {
        return;
      }

      event.preventDefault();
      var modalContent = this.$root.find('.audioeye_tos');

      if (displayAsNotice) {
        modalContent.find('button.enable-audioeye').hide();
      } else {
        modalContent.find('button.enable-audioeye').show();
      }

      this.modal = jQuery.colorbox({
        html: modalContent.html()
      });
    },

    acceptTOS: function(event) {
      event.preventDefault();
      this.$checkBox[0].checked = true;
      $.colorbox.close();
    }
  };
})(window.Zendesk, jQuery);
