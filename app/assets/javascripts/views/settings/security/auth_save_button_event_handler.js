;(function(exports, $) {

  var AuthSaveButtonEventHandler = function AuthSaveButtonEventHandler() {
    var agents   = $('#agents'),
        endUsers = $('#end_users');

    agents.find('.remote_auth_selection')
      .click(this.toggleSaveButton.bind(this, agents));
    endUsers.find('.remote_auth_selection')
      .click(this.toggleSaveButton.bind(this, endUsers));
    $('#account_role_settings_agent_remote_login')
      .bind('change', this.toggleSaveButton.bind(this, agents));
    $('#account_role_settings_end_user_remote_login')
      .bind('change', this.toggleSaveButton.bind(this, endUsers));
  };

  AuthSaveButtonEventHandler.prototype.toggleSaveButton = function toggleSaveButton(root) {
    var checkboxes    = $(root).find('.remote_auth_selection'),
        saveButton    = $(root).find('.save_button'),
        serviceChoice = $(checkboxes[0]).closest('.login_service').find('.service_choice').val();

    saveButton.attr('disabled', 'disabled');
    saveButton.removeClass('button').removeClass('save').addClass('button_disabled');

    checkboxes.each(function(i, checkbox) {
      if (serviceChoice === "0" || $(checkbox).is(':checked')) {
        saveButton.removeAttr('disabled');
        saveButton.removeClass('button_disabled').addClass('button').addClass('save');
      }
    });
  };

  exports.AuthSaveButtonEventHandler = AuthSaveButtonEventHandler;

})(window.Zendesk, jQuery);
