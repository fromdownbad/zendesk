/*globals jQuery*/
;(function(exports, $) {
  var SSLSettingsHandler = function SSLSettingsHandler(initialState) {
    this.root                          = $('#ssl-certificates');
    this.$newCertificateActionButtons  = this.root.find('#new-certificate-action-buttons');
    this.initialDNSCheckStarted        = false;
    this.provisioningStatusChecker     = null;
    this.root.find('#have-a-certificate-button').click(this.showHaveACertificate.bind(this));
    this.root.find('#do-not-have-a-certificate-button').click(this.doNotHaveACertificate.bind(this));

    this.root.find('a.reset-certificate-form').click(this.resetCertificateForm.bind(this));
    this.root.find('#replace-certificate-link a').click(this.toggleCertificateForm.bind(this));
    this.root.find('.file-with-label input[type=file]').change(this.updateFileLabel.bind(this));

    $('#account_automatic_certificate_provisioning').click(this.toggleLE.bind(this));

   this.zendeskProvisionedSSLChecker = new ZendeskProvisionedSSLChecker(this.root.find('#zendesk-provisioned-ssl-dns-check')[0]);

    if (initialState == 'have_a_certificate') {
      this.showHaveACertificate();
    } else if (initialState == 'do_not_have_a_certificate') {
      this.doNotHaveACertificate();
    }

    this.root.find('.dns-check-link').live('click', function(e) {
      e.preventDefault();
      var element = $(e.target).parents('.dns-check')[0];
      new DNSChecker(element, true);
    }.bind(this));

    $('#ssl').live('tab.show', function() {
      //tab.show is fired twice during a page load when this tab
      //is the current tab
      if (!this.initialDNSCheckStarted) {
        this.initialDNSCheckStarted = true;
        this.root.find('#ssl-current-certificate .dns-check').each(function(i, element) {
          new DNSChecker(element, true);
        }.bind(this));
      }

      if (this.provisioningStatusChecker) {
        this.provisioningStatusChecker.start();
      } else {
        var throbber = this.root.find('.provisioning.ssl-throbber').first();
        if (throbber.size() == 1) {
          this.provisioningStatusChecker = new ProvisioningStatusChecker(throbber);
        }
      }
    }.bind(this));

    $('#ssl').live('tab.hide', function() {
      if (this.provisioningStatusChecker) {
        this.provisioningStatusChecker.stop();
      }
    }.bind(this));

  };

  SSLSettingsHandler.prototype.toggleLE = function(e) {
    if ($('#account_automatic_certificate_provisioning')[0].checked) {
      this.resetCertificateForm();
      this.root.find('#self-manage-certificate').hide();
      this.root.find('#zendesk-provisioned-ssl').addClass('enabled').removeClass('disabled');
      this.zendeskProvisionedSSLChecker.startCheck();
    } else {
      this.root.find('#self-manage-certificate').show();
      this.root.find('#zendesk-provisioned-ssl').addClass('disabled').removeClass('enabled');
      this.zendeskProvisionedSSLChecker.stopCheck();
    }
  };

  SSLSettingsHandler.prototype.showHaveACertificate = function(e) {
    if (e) { e.preventDefault(); }
    this.root.find('#hosted_ssl_form_state')[0].value = 'have_a_certificate';
    this.$newCertificateActionButtons.hide();
    this.root.find('#i-have-a-certificate-section').show();
    this.root.find('#do-not-have-a-certificate-section').hide();
    this.root.find('#new-certificate-form').show();
    this.root.find('#certificate-private-key').show();
  };

  SSLSettingsHandler.prototype.doNotHaveACertificate = function(e) {
    if (e) { e.preventDefault(); }
    this.root.find('#hosted_ssl_form_state')[0].value = 'do_not_have_a_certificate';
    this.$newCertificateActionButtons.hide();
    this.root.find('#i-have-a-certificate-section').hide();
    this.root.find('#do-not-have-a-certificate-section').show();
    this.root.find('#new-certificate-form').show();
    this.root.find('#certificate-private-key').hide();
  };

  SSLSettingsHandler.prototype.resetCertificateForm = function(e) {
    if (e) { e.preventDefault(); }
    this.root.find('#hosted_ssl_form_state')[0].value = '';
    this.$newCertificateActionButtons.show();
    this.root.find('#i-have-a-certificate-section').hide();
    this.root.find('#do-not-have-a-certificate-section').hide();
    this.root.find('#new-certificate-form').hide();
  };

  SSLSettingsHandler.prototype.toggleCertificateForm = function(e) {
    e.preventDefault();

    this.root.find('#add-certificate-section').toggle();
  };

  SSLSettingsHandler.prototype.updateFileLabel = function(e) {
    e.preventDefault();
    var selectedFile = e.target;
    var selectedFileName = this.extractFileName(selectedFile);
    var label = $(selectedFile).siblings('.file-label');

    if (selectedFileName == null) {
      label.text(label.data('no-file-string'));
    } else {
      label.text(selectedFileName);
    }
  };

  SSLSettingsHandler.prototype.extractFileName = function(fileField) {
    if (fileField.value === "") {
      return null;
    }

    if (fileField.files) {
      return fileField.files[0].name;
    } else {
      return fileField.value.split(/(\\|\/)/g).pop();
    }
  };

  exports.SSLSettingsHandler = SSLSettingsHandler;

  var ZendeskProvisionedSSLChecker = function(root) {
    this.enabled = root != null;

    if (this.enabled) {
      this.$root = $(root);
      this.$errorMessage = this.$root.find('p.certificate-warning-alert');
      this.$errorMessage.hide();
      this.$throbber = this.$root.find('.ssl-throbber');
      this.$results = this.$root.find('#zendesk-provisioned-ssl-dns-check-results');
      this.$results.hide();
      this.dnsChecks = [];
      this.$root.find('.dns-check').each(function(i, element) {
        var checker = new DNSChecker(element, false);
        checker.showCheckStatus = false;
        checker.onCheckFinish = this.checkFinished.bind(this);
        this.dnsChecks.push(checker);
      }.bind(this));
    }
  };

  ZendeskProvisionedSSLChecker.prototype = {
    startCheck: function() {
      if (!this.enabled) { return; }

      this.$results.hide();
      this.displayThrobber();
      this.$root.show();
      this.disableSave();
      this.dnsChecks.each(function(c) { c.start(); } );
    },

    displayThrobber: function() {
      this.$root.find('.check-in-progress').show();
      this.$throbber.show();
    },

    hideThrobber: function() {
      this.$root.find('.check-in-progress').hide();
      this.$throbber.hide();
    },

    enableSave: function() {
     $("#ssl input.button.save").prop('disabled', false);
    },

    disableSave: function() {
      $("#ssl input.button.save").prop('disabled', true);
    },

    checkFinished: function() {
      if (this.failedChecks().any()) {
        this.hideThrobber();
        this.$results.show();
        this.$errorMessage.show();
      } else if (this.allChecksPassed()) {
        this.hideThrobber();
        this.enableSave();
        this.$results.hide();
      }
    },

    failedChecks: function() {
      return this.dnsChecks.select(function(c) { return c.didCheckFinish && !c.didCheckPassed; });
    },

    allChecksPassed: function() {
      return this.dnsChecks.select(function(c) { return !c.didCheckFinish || !c.didCheckPassed; }).size() === 0;
    },

    stopCheck: function() {
      if (!this.enabled) { return; }
      this.$root.hide();
      this.enableSave();
    }
  };

  var ProvisioningStatusChecker = function(throbber) {
    this.$throbber = throbber;
    this.checkURL = throbber.data('check-url');
    this.checkEvery = 10;
    this.stopAfter = 10 * 60;
    this.failures = 0;
    this.maxFailures = 5;
    this.checksRemaining = Math.floor(this.stopAfter / this.checkEvery);
    this.intervalID = null;
    this.start();
  };

  ProvisioningStatusChecker.prototype = {
    start: function() {
      if (!this.intervalID) {
        this.$throbber.show();
        this.intervalID = window.setInterval(this.requestStatus.bind(this), this.checkEvery * 1000);
      }
    },

    stop: function() {
      this.$throbber.hide();
      if (this.intervalID) {
        window.clearInterval(this.intervalID);
        this.intervalID = null;
      }
    },

    requestStatus: function() {
      this.checksRemaining -= 1;

      if (this.checksRemaining > 0 && this.failures < this.maxFailures) {
        $.ajax({
          method: 'get',
          url: this.checkURL,
          dataType: 'json',
          success: this.checkFinished.bind(this),
          error: this.checkFailed.bind(this)
        });
      } else {
        this.stop();
      }
    },

    checkFinished: function(data, status, jqXHR) {
      if (data.provisioning_status == 'success' || data.provisioning_status == 'failed') {
        this.stop();
        document.location.reload();
      }
    },

    checkFailed: function(data, status, jqXHR) {
      this.failures += 1;
    }
  };

  var DNSChecker = function(element, autoStart) {
    this.$element = $(element);
    this.brandId = this.$element.data('brand-id');
    this.checkPath = this.$element.data('brand-check-path');
    this.subdomain = this.$element.data('brand-subdomain');
    this.$throbber = this.$element.find('.dns-check-throbber');
    this.$results = this.$element.find('div.dns-check-results');
    this.didCheckFinish = false;
    this.didCheckPassed = false;
    this.showCheckStatus = true;

    if (autoStart) {
      this.start();
    }
  };

  DNSChecker.prototype = {
    start: function() {
      this.didCheckFinish = false;
      this.didCheckPassed = false;
      this.$throbber.show();
      this.clearResults();
      $.ajax({
        url: this.checkPath,
        dataType: 'json',
        success: this.checkFinished.bind(this)
      });
    },

    clearResults: function() {
      var cell = this.$results.parents('td');
      cell.height(cell.height());
      cell.width(cell.width());
      this.$results.html('');
    },

    checkFinished: function(data, status, jqXHR) {
      this.$throbber.hide();

      if (data.is_valid) {
        this.checkPassed(data);
      } else {
        this.checkFailed(data);
      }
      this.didCheckFinish = true;
      if (this.onCheckFinish) { this.onCheckFinish(); }
      this.$results.show();
    },

    checkPassed: function(data) {
      this.didCheckPassed = true;
      this.$element.removeClass('error').addClass('verified');
      var html = '<img class="dns-check-icon" src="/images/icons/success.png">';
      html += I18n.t('txt.admin.controllers.settings.security_controller.ssl.current_certificate.verified');

      this.$results.html(html);
    },

    checkFailed: function(data) {
      this.$element.removeClass('verified').addClass('error');

      var html = '<span class="invalid"><img class="dns-check-icon" src="/images/icons/error.png">';
      html += I18n.t('txt.admin.controllers.settings.security_controller.ssl.current_certificate.error_match_cname');
      html += '</span>';

      html += '<table class="dns-check-results"><thead><tr>';
      html += '<th>' + I18n.t('txt.admin.controllers.settings.security_controller.ssl.current_certificate.host_record') + '</th>';
      html += '<th>' + I18n.t('txt.admin.controllers.settings.security_controller.ssl.current_certificate.points_to') + '</th>';
      html += '<th>' + I18n.t('txt.admin.controllers.settings.security_controller.ssl.current_certificate.ttl') + '</th>';
      html += '</tr></thead>';
      html += '<tbody><tr>';
      html += '<td>';
      html += this.subdomain;
      html += '</td>';
      html += '<td>';
      html += data.expected_cnames[0];
      html += '</td>';
      html += '<td>14400</td>';
      html += '</tbody></table>';

      if (this.showCheckStatus) {
        html += '<a href="#" class="dns-check-link">';
        html += I18n.t('txt.admin.controllers.settings.security_controller.ssl.current_certificate.check_status');
        html += '</a>';
      }

      this.$results.html(html);

      var cell = this.$results.parents('td');
      cell.height('auto');
      cell.width('auto');
    }
  };

})(window.Zendesk, jQuery);
