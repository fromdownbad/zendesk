/*global Zendesk, $j, _, I18n */
Zendesk.NS('Screenr');

Zendesk.Screenr.Provisioning = {
  // Public: Provide a tenant account on Screenr.
  //
  // domain - The String of the request domain to be sent to Screenr's API.
  // triggeringCheckBoxId - The String containing the id of the checkbox that is triggerin the provision flow.
  //
  // Examples
  //   <input id="account_settings_screencasts_for_tickets" type="checkbox" value="1" />
  //   <script>
  //     Zendesk.Screenr.Provisioning.init('mydomain', 'account_settings_screencasts_for_tickets');
  //  </script>
  //
  init: function(domain, triggeringCheckBoxId, scope) {
    Zendesk.Screenr.Provisioning.requestedDomain = domain;
    Zendesk.Screenr.Provisioning.scope = scope;
    this.bindCheckBox(triggeringCheckBoxId);
    this.bindProvisioningForm();
  },

  requestedDomain: "",
  triggeringCheckBoxId: "",
  scope: "",

  // Internal: Bind the checkbox to the provisioning flow.
  //
  // checkBoxId - The String containing the id of the checkbox that is triggerin the provision flow.
  //
  // Returns nothing.
  bindCheckBox: function(checkBoxId) {
    $j(checkBoxId).click(function(event) {
      this.showProvisioningModal();
      event.preventDefault(); // We check the checkbox on success provisioning.
    }.bind(this));
    // We save the checkbox to check later on success.
    Zendesk.Screenr.Provisioning.triggeringCheckBoxId = checkBoxId;
  },

  // Internal: Display the provisioning dialog window.
  //
  // NOTE: it relays on colorbox.
  //
  // Returns nothing.
  showProvisioningModal: function() {
    $j.colorbox({
      inline: true,
      href: '#screenr_provisioning',
      width: '620px',
      onComplete: function() { $j('#screenr_provisioning  #domain').focus(); },
      onClosed: function() {
        // cleanups any error when closing modal window
        Zendesk.Screenr.Provisioning._removeError();
      }
    });
  },


  // Internal: Bind the provisioning form to the onSubmit function.
  //
  // Returns nothing.
  bindProvisioningForm: function() {
    $j("#screenr_provisioning form").submit(function(event) {
      event.preventDefault();
      this.onSubmit();
    }.bind(this));
  },

  // Internal: Handle the submission of a provisionin request
  //
  // Returns nothing.
  onSubmit: function(){
    Zendesk.Screenr.Provisioning._disableSubmitButton();
    Zendesk.Screenr.Provisioning._removeError();
    Zendesk.Screenr.Provisioning._enqueueProvisioning();
  },

  // Internal: Enqueue a provisioning request to /screenr_tenants
  //
  // Provisioning request are made asyncronosly via a Resque Job.
  //
  // Returns nothing.
  _enqueueProvisioning: function() {
    if(this.poller) { this.poller.stop(); }

    $j("#provisioning_spinner").show();

    $j.ajax({
      type: 'POST',
      url: '/screenr_tenants',
      data: {domain: Zendesk.Screenr.Provisioning.requestedDomain,
             scope: Zendesk.Screenr.Provisioning.scope},
      success: this._startPoller.bind(this)
    });
  },

  // Internal: Start a poller that ask for a job staus.
  //
  // response - The Object returned by a POST on /screenr_tenants.
  //
  // See: _enqueueProvisioning.
  //
  // The response includes the status_url to query.
  // It checks every 1000ms for a max of 20.000ms.
  //
  // If the request finishes succesfully it triggers _pollingCallback.
  // If the request fails it triggers _errorCallback.
  // If the request times out it triggers _timeoutCallback.
  //
  // Returns nothing.
  _startPoller: function(response) {
    var ajaxParams = { url: response.status_url, type: 'GET', dataType: 'json' };
    this.poller = new Zendesk.Utils.Poller(ajaxParams,
                                           1000,
                                           20000,
                                           this._pollingCallback.bind(this),
                                           this._errorCallback.bind(this),
                                           this._timeoutCallback.bind(this));
    this.poller.start();
  },

  // Internal: Parse the result of a polling result.
  //
  // response - The Object returned by a GET on the job with status url.
  //
  // In case Screenr has returned something it triggers _onCompletedPolling and stop polling.
  // In case something failed triggers _errorCallback.
  // Otherwise reiterate a new polling cycle.
  //
  // Returns nothing.
  _pollingCallback: function(response) {
    switch (response.job_status.status) {
      case "completed":
        // We have talked with Screenr let's see what they said
        this._onCompletedPolling(response.job_status.results);
        break;
      case "failed":
        // Something went wrong we display error message
        this._errorCallback(this);
        break;
      case "killed":
        // Someone killed it.
        this._errorCallback(this);
        break;
      default:
        // This could be "working" or "queued" statuses
        // We restart the poller, and try again by returning true
        return true;
    }
  },

  // Internal: Parse the result of a Screenr API result.
  //
  // results - The Object returned by a completed Job.
  //
  // In case API returned provisioned trigger _onSucessfulProvisioning
  // In case API returned validation_failed trigger _onValidationFailed
  //
  // TODO: Remove this function when deprecated, see _onValidationFailed.
  //
  // Returns nothing.
  _onCompletedPolling: function(results) {
    switch (results.api_status) {
      case 'provisioned':
        this._onSucessfulProvisioning();
        break;
      case 'validation_failed':
        this._onValidationFailed(results.message, results.error_on);
        break;
      default:
        // Why are we here?
        // Something went wrong let's inform the user with a generic error.
        this._errorCallback(this);
    }
  },

  // Internal: Take care of hadling a succesful provisioning.
  //
  // Returns nothing.
  _onSucessfulProvisioning: function() {
    $j("#provisioning_spinner").hide();

    var account_created_text = I18n.t('public.javascripts.views.settings.screencasts.screenr_account_created'),
        close_text           = I18n.t('public.javascripts.views.settings.screencasts.close_window_button'),
        final_feedback_text  = I18n.t('public.javascripts.views.settings.screencasts.final_feedback'),
        feedback_modal       = $j('<div class="info">'),
        feedback_main        = $j('<p>'),
        close_bt_wrapper     = $j('<div class="submit">'),
        close_bt             = $j('<input class="button" name="commit" type="submit"/>');

    // Clean Up the stage.
    $j("#screenr_provisioning").html("");

    // Display the feedback text in the modal window
    feedback_modal
      .html("&#10004;&nbsp; " + account_created_text)
      .appendTo($j("#screenr_provisioning"));

    // Display the feedback text in the main window
    feedback_main
      .html(final_feedback_text)
      .appendTo($j("#screencasts_wrapper"));

    // Closing Button
    close_bt
      .attr('value', close_text)
      .appendTo(close_bt_wrapper)
      .click(function(event){
        $j.colorbox.close();
        // when closing the modal feedback text gets highlighted.
        feedback_main.effect("highlight", {}, 3000);
      }.bind(this));

    close_bt_wrapper
      .appendTo($j("#screenr_provisioning"));

    // Set triggering checkbox as checked and disable it's functionality
    Zendesk.Screenr.Provisioning._setTriggeringCheckboxAsChecked(Zendesk.Screenr.Provisioning.triggeringCheckBoxId);

    // Resize modal window
    $j.colorbox.resize();
    _.defer(function() { $j.colorbox.resize(); });
  },

  // Internal: Take care of marking the checkbox as checked.
  //
  // checkBoxId - The String containing the id of the checkbox that is triggerin the provision flow.
  //
  // Examples
  //
  //   _setTriggeringCheckboxAsChecked('#thePoniesCheckBox');
  //
  // Returns nothing.
  _setTriggeringCheckboxAsChecked: function(checkBoxId) {
    $j(checkBoxId)
      .attr('checked', true)
      .unbind()
      .click(function(event) {
        // after provisioning it shouldn't be possible to uncheck
        event.preventDefault();
      });
  },

  // Internal: Display a validation error.
  //
  // message - The String containing the message received from API.
  // error_on - The String containing what is failing (ex: domain, first_name, last_name, email)
  //
  // Most likely this will never be used, since valid parameters
  // are sent to API by Screenr::Integration (ruby).
  //
  // Leave it here for the Beta Phase.
  // TODO: Remove this function when deprecated.
  //
  // Returns nothing.
  _onValidationFailed: function(message, error_on){
    this._displayError(message);
    $j("#provisioning_spinner").hide();
    $j("#downgrading_spinner").hide();
    this._enableSubmitButton();
    this._enableDowngradingButton();
  },

  // Internal: Display a general error.
  //
  // response - The Object binded to this event
  //
  // Returns nothing.
  _errorCallback: function(response) {
    this._displayError(I18n.t('public.javascripts.views.settings.screencasts.screenr_unavailable'));
    $j("#provisioning_spinner").hide();
    $j("#downgrading_spinner").hide();
    this._enableSubmitButton();
    this._enableDowngradingButton();
  },

  // Internal: Display a timeout error.
  //
  // response - The Object binded to this event
  //
  // Returns nothing.
  _timeoutCallback: function(response) {
    this._displayError(I18n.t('public.javascripts.views.settings.screencasts.screenr_unavailable'));
    $j("#provisioning_spinner").hide();
    $j("#downgrading_spinner").hide();
    this._enableSubmitButton();
    this._enableDowngradingButton();
  },

  // Internal: Display an error on the UI.
  //
  // message - The String containing the message to be displayed.
  //
  // Returns nothing.
  _displayError: function(message){
    $j(".error")
      .html(message)
      .show();
    $j.colorbox.resize();
    _.defer(function() { $j.colorbox.resize(); });
  },

  // Internal: Removes an error from the UI.
  //
  // See: _displayError().
  //
  // Returns nothing.
  _removeError: function(){
    $j(".error").hide();
    $j.colorbox.resize();
    _.defer(function() { $j.colorbox.resize(); });
  },

  // Internal: Disable the submit button.
  //
  // Returns nothing.
  _disableSubmitButton: function(){
    $j("#screenr_provisioning .button").attr('disabled', 'disabled');
  },

  // Internal: Enable the submit button.
  //
  // Returns nothing.
  _enableSubmitButton: function(){
    $j("#screenr_provisioning .button").removeAttr("disabled");
  },


  //// DOWNGRADING ////
  recorderId: null,
  host: null,
  screenrBaseUri: null,
  confirmState: false,

  // Public: Set up the downgrading process.
  //
  // recorderId - The forums recorded id (it's needed to verify Tenant status).
  // host - The tenant Screenr host (ex: support87.sfssdev.com)
  // screenrBaseUri - Base url for interrogating Screenr API for Tenant status (ex: "https://api.sfssdev.com")
  //
  // Returns nothing.
  setupDowngrade: function(recorderId, host, screenrBaseUri) {
    this.recorderId     = recorderId;
    this.host           = host;
    this.screenrBaseUri = screenrBaseUri;
    this._bindDowngradeLink();
  },

  // Internal: Binds the link in UI and strarts a Tenant look up
  //
  // Returns nothing.
  _bindDowngradeLink: function() {
    $j("#screenr_downgrade").click(function(event) {
      this._checkTenantStatus();
      event.preventDefault();
    }.bind(this));
  },

  // Internal: Makes an API call and checks Tenant Status.
  //
  // Returns nothing.
  _checkTenantStatus: function(){
    var checkUrl = Zendesk.Screenr.Helper.publicUrlToTenantStatus(this.recorderId, this.host, this.screenrBaseUri);
    $j.ajax({
      type: 'GET',
      url: checkUrl,
      success: this._parseTenantStatus.bind(this),
      error: this._errorCallback.bind(this)
    });
  },

  // Internal: Based on response Status and ErrorCode returned from the endpoint
  // it diverge to proper action.
  //
  // response - Response returned by endpoint.
  //
  // Returns nothing.
  _parseTenantStatus: function(data){
    var response = data;

    // FF patch: it returns a string instead of an object.
    if (typeof response == "string") response = response.evalJSON();

    switch (response.StatusCode) {
      case 200: // UPGRADED
        this._displayDowngrade();
        break;
      case 403: // NOT UPGRADED
        this._displayImpossibleToDowngrade();
        break;
      default:
        // If something else is returned it means something
        // went wrong hence we prompt a general error
        this._errorCallback();
    }
  },

  // Internal: display a dialog that informs user downgrade in not possible.
  //
  // Returns nothing.
  _displayImpossibleToDowngrade: function() {
    var modal = $j('#screenr_downgrading');

    modal.find('#no_downgrade').show();
    modal.find('#close_modal')
         .click(function(event){
           $j.colorbox.close();
           event.preventDefault();
         }.bind(this));

    this._showDowngradingModal();
  },

  // Internal: display a dialog where user can start a downgrade process.
  //
  // Returns nothing.
  _displayDowngrade: function() {
    var modal = $j('#screenr_downgrading');
    modal.find('#downgrade').show();

    modal.find('#downgrade_button')
         .click(function(event){
           if (!this.confirmState) {
             this.confirmState = true;
             this._onDowngradeClick();
           }
           event.preventDefault();
         }.bind(this));

    modal.find('#close_modal')
         .click(function(event){
           $j.colorbox.close();
           event.preventDefault();
         }.bind(this));

    this._showDowngradingModal();
    this.confirmState = false;
  },

  // Internal: display a JS confirm box and enqueues downgrading id answers is yes.
  //
  // Returns nothing.
  _onDowngradeClick: function() {
    var answer = confirm(I18n.t('public.javascripts.views.settings.screencasts.sure_you_want_downgrade'));
    if (answer){
      this._enqueueDowngrading();
    } else {
      $j.colorbox.close();
    }
  },

  // Internal: Enqueue a downgrading request to /screenr_tenant_downgrades
  //
  // Downgrading requests are made asyncronosly via a Resque Job.
  //
  // Returns nothing.
  _enqueueDowngrading: function() {
    if(this.poller) { this.poller.stop(); }
    $j("#downgrading_spinner").show();
    this._disableDowngradingButton();

    $j.ajax({
      type: 'POST',
      url: '/screenr_tenant_downgrades',
      success: this._startDowngradingPoller.bind(this)
    });
  },

  // Internal: Start a poller that ask for a downgrading job status.
  //
  // response - The Object returned by a POST on /screenr_tenant_downgrades.
  //
  // See: _enqueueDowngrading.
  //
  // The response includes the status_url to query.
  // It checks every 1000ms for a max of 20.000ms.
  //
  // If the request finishes succesfully it triggers _pollingDowngradingCallback.
  // If the request fails it triggers _errorCallback.
  // If the request times out it triggers _timeoutCallback.
  //
  // Returns nothing.
  _startDowngradingPoller: function(response) {
    var ajaxParams = { url: response.status_url, type: 'GET', dataType: 'json' };
    this.poller = new Zendesk.Utils.Poller(ajaxParams,
                                           1000,
                                           20000,
                                           this._pollingDowngradingCallback.bind(this),
                                           this._errorCallback.bind(this),
                                           this._timeoutCallback.bind(this));
    this.poller.start();
  },

  // Internal: Parse the result of a polling result.
  //
  // response - The Object returned by a GET on the job with status url.
  //
  // In case Screenr has returned something it triggers _onCompletedDowngradingPolling and stop polling.
  // In case something failed triggers _errorCallback.
  // Otherwise reiterate a new polling cycle.
  //
  // Returns nothing.
  _pollingDowngradingCallback: function(response) {
    switch (response.job_status.status) {
      case "completed":
        // We have talked with Screenr let's see what they said
        this._onCompletedDowngradingPolling(response.job_status.results);
        break;
      case "failed":
        // Something went wrong we display error message
        this.confirmState = false;
        this._errorCallback(this);
        break;
      case "killed":
        // Someone killed it.
        this.confirmState = false;
        this._errorCallback(this);
        break;
      default:
        // This could be "working" or "queued" statuses
        // We restart the poller, and try again by returning true
        return true;
    }
  },

  // Internal: Parse the result of a Screenr API result.
  //
  // results - The Object returned by a completed Job.
  //
  // In case API returned provisioned trigger _onSucessfulDowngrading
  // In case API returned validation_failed trigger _onValidationFailed
  //
  // TODO: Remove this function when deprecated, see _onValidationFailed.
  //
  // Returns nothing.
  _onCompletedDowngradingPolling: function(results) {
    switch (results.api_status) {
      case 'downgraded':
        this._onSucessfulDowngrading();
        break;
      case 'validation_failed':
        this.confirmState = false;
        this._onValidationFailed(I18n.t('public.javascripts.views.settings.screencasts.screenr_account_impossible_to_downgrade'), '');
        break;
      default:
        // Why are we here?
        // Something went wrong let's inform the user with a generic error.
        this._errorCallback(this);
    }
  },

  // Internal: shows a dialog informing user that downgrade was sucessful.
  //
  // Returns nothing.
  _onSucessfulDowngrading: function() {
    var modal,
        button;

    modal = $j('<div class="info">' + I18n.t('public.javascripts.views.settings.screencasts.screenr_account_downgraded') + '</div>'  +
               '<form>'                                                                                                              +
                 '<fieldset>'                                                                                                        +
                   '<input id="close_button" class="button" name="commit" type="submit" />'                                          +
                 '</fieldset>'                                                                                                       +
               '</form>');

    button = modal.find('#close_button');
    button.attr('value', I18n.t('public.javascripts.views.settings.screencasts.screenr_account_downgraded_close_button'))
          .click(function(event){
            event.preventDefault();
            $j.colorbox.close();
          }.bind(this));

    $j("#provisioning_spinner").hide();
    $j("#screenr_downgrading").empty();
    modal.appendTo($j("#screenr_downgrading"));

    $j.colorbox.resize();
    _.defer(function() { $j.colorbox.resize(); });
  },

  // Internal: Disable the downgrading button.
  //
  // Returns nothing.
  _disableDowngradingButton: function(){
    $j("#screenr_downgrading .button").attr('disabled', 'disabled');
  },

  // Internal: Enable the submit button.
  //
  // Returns nothing.
  _enableDowngradingButton: function(){
    $j("#screenr_downgrading .button").removeAttr("disabled");
  },

  // Internal: Show the downgrade base modal div
  //
  // Returns nothing.
  _showDowngradingModal: function() {
    $j.colorbox({
      inline: true,
      href: '#screenr_downgrading',
      width: '620px',
      onComplete: function() { },
      onClosed: function() {
        // cleanups any error when closing modal window
        Zendesk.Screenr.Provisioning._removeError();
      }
    });
  }
};
