/*globals _, Zendesk, jQuery*/
Zendesk.NS('Email');
Zendesk.Email.RecipientAddress = {
  init: function(scope) {
    this.ADD_ADDRESS_SELECTOR = ".add.colorbox";
    this.DELETE_SELECTOR = ".delete.colorbox_inline";
    this.EDIT_SELECTOR = ".edit_this.colorbox";
    this.MAKE_DEFAULT_SELECTOR = ".make_default";

    scope = scope || jQuery("#recipient_addresses")[0];

    this.initBinding(scope);
  },

  initBinding: function(scope) {
    this.bindTooltip(scope);
    this.bindMakeDefault(scope);
    this.bindRefreshForwarding(scope);
    this.bindAddAddress(scope);
    this.bindDelete(scope);
    this.bindEdit(scope);
  },

  // calls lotus email setup modal
  bindAddAddress: function(scope) {
    var self = this;
    jQuery(this.ADD_ADDRESS_SELECTOR, scope).click(function(event) {
      event.preventDefault();
      self.inLotus(
        function() {
          self.triggerLotusEmailSetupModal(event);
        },
        function() {
          self.triggerColorbox(event, function(){
            self.bindTooltip();
            self.bindSubmit();
          });
        }
      );
      return false;
    });
  },

  bindEdit: function(scope){
    var self = this;
    jQuery(this.EDIT_SELECTOR, scope).click(function(event) {
      event.preventDefault();
      self.triggerColorbox(event, function(){
        self.inLotus(function(){
          var $row = jQuery(event.target).closest('.item.recipient_address');
          jQuery("#colorbox input[type=submit]").click(function(editEvent) {
            editEvent.preventDefault();
            self.handleEdit(editEvent, $row);
            return false;
          });
        }, function(){
          self.bindTooltip();
          self.bindSubmit();
        });
      });
      return false;
    });
  },

  bindDelete: function(scope) {
    var self = this;
    jQuery(this.DELETE_SELECTOR, scope).click(function(event){
      event.preventDefault();
      self.triggerColorbox(event, function(){
        self.inLotus(function(){
          // bind the button in the colorbox modal
          var $row = jQuery(event.target).closest('.item.recipient_address');
          jQuery("#colorbox .button.save[data-method=delete]").click(function(colorboxEvent){
            colorboxEvent.preventDefault();
            self.handleDelete(colorboxEvent, $row);
            return false;
          });
        });
      });

      return false;
    });
  },

  handleDelete: function(event, $row){
    var id = jQuery(event.target).attr('data-id');
    var self = this;
    jQuery.ajax({
      type:     'DELETE',
      url:      jQuery(event.target).attr('href'),
      dataType: "json",
      success: function(){
        jQuery.colorbox.close();

        $row.css({
          'background-color': '#f2fafd',
          'transition': 'opacity 0.5s ease-in-out'
        });
        setTimeout(function(){
          $row.css('opacity', 0);
          $row.slideUp({duration: 500});
        }, 200);
      },
      error: function(response){
        self.handleError(response);
      }
    });
  },

  triggerColorbox: function(event, onComplete){
    var $target  = jQuery(event.target);
    var url     = $target.attr('href');
    var options = {};
    var self = this;

    if($target.attr('class').match("colorbox_inline")) {
      options.html = jQuery(url).html();
    } else {
      options.href       = url;
      options.innerWidth = 395;
    }

    if (onComplete) {
      options.onComplete = function() {
        onComplete.call(self);
      };
    }

    jQuery.colorbox(options);
  },

  inLotus: function(newFn, oldFn) {
    if (window.parent !== window) {
      newFn && newFn.call(this);
    }
    else {
      oldFn && oldFn.call(this);
    }
  },

  getEmailSetupModalStartPage: function(event) {
    var isAddZendeskAddress = jQuery(event.target).hasClass('add-zendesk-address');
    var isConnectOther = jQuery(event.target).hasClass('connect-other');
    var startPage = 'emailForwarding';
    if (isAddZendeskAddress) {
      startPage = 'newSupportAddress';
    }
    else if (isConnectOther) {
      startPage = 'otherForwarding';
    }
    return startPage;
  },

  triggerLotusEmailSetupModal: function(event) {
    var self = this;
    var startPage = this.getEmailSetupModalStartPage(event)


    var data = { target: "email_setup_modal:show",
      options: {
        brandId: parseInt(jQuery(event.target).attr('data_brand_id')),
        startPage: startPage,
        makeDefault: false
      }
    };
    if (window.frameElement && window.frameElement.id) {
      data.options.iFrameId = window.frameElement.id;
    }

    // posts message to lotus window to open email setup modal
    self.postToParent(data);

    var handler = function(messageEvent) {
      self.emailSetupSuccess(event, messageEvent);
      window.removeEventListener('message', handler);
    };

    window.addEventListener('message', handler);
  },

  emailSetupSuccess: function(addAddressEvent, messageEvent){
    // handler on email setup success
    // retrive row from backend and display row
    var message = JSON.parse(messageEvent.data);
    var self = this;
    var recipientAddressId;
    if (message.event === "email_setup_modal:close" && (recipientAddressId = message.recipientAddressId)) {
      var $row = jQuery("[data-id=" + message.recipientAddressId + "]");
      if ($row.length > 0) {
        self.highlightRow($row);
        return;
      }

      jQuery.ajax("/settings/recipient_addresses/" + recipientAddressId).done(function(response){
        $row = jQuery(response).insertAfter(jQuery(addAddressEvent.target).parents("div.item"));
        self.initBinding($row);
        $row.css({
          opacity: 0,
          'background-color': '#f2fafd',
          'transition': 'opacity 1s ease-in-out'
        });
        setTimeout(function() {
          $row.css("opacity", 1);
        },200);
      });
    }
  },

  refreshRecipientAddressRow: function(id, $row) {
    var self = this;
    jQuery.ajax("/settings/recipient_addresses/" + id).done(function(response){
      var html = jQuery(response).html();
      $row.html(html);
      self.initBinding($row);
      self.highlightRow($row);
    });
  },

  highlightRow: function($row) {
    $row.css('transition', 'background-color 1s ease-in-out');
    setTimeout(function() {
      $row.css('background-color', '#f2fafd');
    },200);
  },

  handleEdit: function(event, $row) {
    var self = this;
    var $form = jQuery("#recipient_address_form");
    var $error = $form.find("#recipient_address_email_error");
    $error.html("&nbsp;"); // clear previous error message

    jQuery.ajax({
      type:     $form.attr('method'),
      url:      $form.attr('action'),
      data:     $form.serialize(),
      dataType: "json",
      success: function(response){
        jQuery.colorbox.close();
        self.sendToGrowl('notice', I18n.t('txt.controllers.flash.notice.saved'));
        var id = response.id;
        self.refreshRecipientAddressRow(id, $row);
      },
      error: function(response) {
        self.handleError(response);
        $error.html(response);
      }
    });
    return false;
  },

  bindSubmit: function() {
    var $form = jQuery("#recipient_address_form");
    var self = this;

    $form.find("input[type=submit]").click(function() {
      var $error = $form.find("#recipient_address_email_error");
      $error.html("&nbsp;"); // clear previous error message

      jQuery.ajax({
        type:     $form.attr('method'),
        url:      $form.attr('action'),
        data:     $form.serialize(),
        dataType: "json",
        success: function(){
          jQuery.colorbox.close();
          self.reloadPage();
        },
        error: function(response) {
          $error.html(self.parseErrors(response).join("<br>"));
        }
      });
      return false;
    });
  },

  bindTooltip: function(scope) {
    jQuery("a[rel=tooltip]", scope).tooltip({
      tooltipClass: "gmail_recipient_address",
      show: true,
      position: {
        my: "right top",
        at: "right bottom"
      }
    });
  },

  bindMakeDefault: function(scope) {
    var self = this;
    jQuery(this.MAKE_DEFAULT_SELECTOR, scope).click(function(event) {
      event.preventDefault();
      self.makeDefault(jQuery(event.target).data('id'));
    });
  },

  makeDefault: function(id) {
    var self = this;
    jQuery.ajax({
      type:     'PUT',
      url:      '/settings/recipient_addresses/' + id,
      dataType: 'json',
      data:     { 'recipient_address': { 'default': true } },
      success:  this.reloadPage,
      error:    function(response){
        self.handleError(response);
      }
    });
  },

  // called from the form page
  livePreview: function(){
    // update live preview from url + form params after stopping to type for a short time
    jQuery(".live_preview").each(function(){
      var self = jQuery(this);
      var $form = jQuery(self.data("form"));
      var preview_url = self.data("url");
      var lastTimeout;
      var update = function(){
        var $disabled = $form.find(':input:disabled').removeAttr('disabled'); // gmail connector email field is disabled
        var data = $form.serialize();
        $disabled.attr('disabled','disabled');
        jQuery("#cboxLoadedContent").css("height", "auto"); // resize colorbox without losing focus
        self.load(preview_url + "?" + data);
      };
      $form.find(":input").keyup(function(){
        if(lastTimeout) clearTimeout(lastTimeout);
        lastTimeout = setTimeout(update, 500);
      });
    });
  },

  // called from the form page
  bindEmailBrandSelect: function(brands){
    var $email = jQuery("#recipient_address_email");
    var $brand = jQuery("#recipient_address_brand_id");
    var change = function(){
      var domain = $email.val().split("@")[1];
      var brand_id = brands[domain];
      if(brand_id) {
        $brand.val(brand_id);
        $brand.attr('disabled', 'disabled');
      } else {
        $brand.removeAttr('disabled');
      }
    };

    $email.change(change);
    change();
  },


  reloadPage: function(){
    jQuery('#recipient_addresses').responsiveLoad("/settings/recipient_addresses/table", function(){
      Zendesk.Email.RecipientAddress.init();
    });
  },

  bindRefreshForwarding: function(scope){
    var self = this;
    jQuery('.refresh_status', scope).click(function(event) {
      event.preventDefault();
      var $link = jQuery(this);
      var recipientAddressId = $link.data('id');
      self.sendToGrowl("progress", I18n.t('txt.controllers.flash.progress.verification'));
      jQuery.ajax({
        type:     'PUT',
        url:      '/api/v2/recipient_addresses/' + recipientAddressId + '/verify',
        data:     {'type': $link.data('type')},
        dataType: 'json',
        success:  function(){
          self.inLotus(
            function(){
              self.sendToGrowl('notice', I18n.t('txt.controllers.flash.notice.saved'));
              var $row = jQuery(event.target).parents('.item.recipient_address');
              self.refreshRecipientAddressRow(recipientAddressId, $row);
            },
            function(){
              self.reloadPage();
            }
          );
        },
        error: function(response) {
          self.handleError(response);
        }
      });
    });
  },

  postToParent: function(data){
    if (data != null && typeof data === 'object') {
      data = JSON.stringify(data);
    }
    var loc = window.location;
    var origin = loc.protocol + '//' + loc.host;
    window.top.postMessage(data, origin);
  },

  handleError: function(response){
    // try to parse error base on standard error response schema in classic
    var errorMessages = this.parseErrors(response);
    this.sendToGrowl('error', errorMessages.join('<br>'));
  },

  sendToGrowl: function(action, msg) {
    var data = {
      target: 'growl',
      memo: {
        action: action,
        message: msg,
        options: {
          sticky: false,
          life: 2000
        }
      }
    };
    this.postToParent(data);
  },

  parseErrors: function(response) {
    var responseJSON = JSON.parse(response.responseText);
    if (responseJSON && responseJSON.details) {
      var errorMessages = [];
      for(var attr in responseJSON.details) { // loop through each
        errorMessages = errorMessages.concat(responseJSON.details[attr][0].description);
      }
      return errorMessages;
    }
    return [I18n.t('txt.controllers.flash.error.saved')];
  }


};
