$z.defModule('access/index', {
  initialize: function() {
    $j('#password_help').data('originalURL', $j('#password_help').attr('href'));
    this.addEmailToPasswordHelpLink();
    $j("#user_email").change(function() {
      $z('access/index').addEmailToPasswordHelpLink();
    });
  },

  addEmailToPasswordHelpLink: function() {
    var passwordHelpLink = $j('#password_help');
    var email = $j("#user_email").val();
    var originalURL = $j('#password_help').data('originalURL');
    if ($j.trim(email) === '') {
      passwordHelpLink.attr("href", originalURL);
    } else {
      email = encodeURIComponent(email);
      passwordHelpLink.attr("href", originalURL + '?email=' + email);
    }
  }
});

