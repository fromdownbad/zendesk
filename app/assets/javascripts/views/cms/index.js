$j("#cms_variant_is_fallback").change(function() {
  if($j("#cms_variant_is_fallback").attr('checked') == 'checked') {
    $j("#cms_variant_active_true").attr('checked', true);
    $j("#cms_variant_active_false").attr('disabled', true);
  } else {
    $j("#cms_variant_active_false").attr('disabled', false);
  }
});

$j("#get_more_macros").click(function() {
  $j("#get_more_macros").hide();
  $j(".macros_reference").fadeIn();
});

$j("#get_more_triggers").click(function() {
  $j("#get_more_triggers").hide();
  $j(".triggers_reference").fadeIn();
});

$j("#get_more_automations").click(function() {
  $j("#get_more_automations").hide();
  $j(".automations_reference").fadeIn();
});
