/*globals jQuery*/

$j(document).ready(function() {
  $j('#cms_texts_filter_form').change(function() {
    $j(this).closest('form').submit();
  });

  new InputSuggest($j(".cms_content"));
});
