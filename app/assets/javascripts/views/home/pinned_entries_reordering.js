/*globals document, alert, $z*/
(function($, _) {

  var pinnedEntriesReorderingTemplate, // the mustache template for the widget
      $showLink,                      // the link to show the widget
      $thingsToHideWhileReordering,   // things that should be hidden when reordering pinned entries
      $widgetContainer,               // the container for the widget
      $pinnedEntriesReorderer,         // the actual widget
      $spinner;                       // a loading spinner

  function onSaveSuccess() {
    document.location.href = '/home';
  }

  function onSaveError() {
    alert('Sorry, there was an error saving your changes.');
  }

  function saveOrder() {
    var $this = $(this);
    var idsAsQuery = $pinnedEntriesReorderer
                       .find('ol')
                       .sortable('serialize', { key: 'ids[]' });

    if(idsAsQuery == "ids[]=") {
      idsAsQuery = ""
    } else {
      idsAsQuery = idsAsQuery + "&";
    }

    $.ajax({
      url:     $this.attr('action'),
      type:    $this.attr('method'),
      data:    idsAsQuery + $this.serialize(),
      success: onSaveSuccess,
      error:   onSaveError
    });
    return false;
  }

  function hidePinnedEntriesReorderer() {
    $widgetContainer.hide();
    $thingsToHideWhileReordering.show();
    $pinnedEntriesReorderer.remove();
    $pinnedEntriesReorderer = null;
  }

  function addReorderingBehavior() {
    $pinnedEntriesReorderer
      .find('a.cancel').click(function() {
        hidePinnedEntriesReorderer();
        return false;
      }).end()
      .find('form').submit(saveOrder);

    var lists = $pinnedEntriesReorderer.find('ul,ol');
    lists.each(function() {
      $(this).sortableWithEmptyTarget({
        emptyTargetText:  I18n.t('txt.admin.views.home._reorder_pinned_entries.drag_topics_here_label'),
        emptyTargetClass: 'item sortable',
        axis:             'y',
        placeholder:      'sortable item target',
        connectWith:      lists.not(this)
      });
    });
  }

  function hasPinnedIndex(entry) { return !!entry.pinned_index; }

  function renderPinnedEntriesReorderer() {
    if ($pinnedEntriesReorderer) { return; }

    $.getJSON($showLink.attr('href'), null, function(pinnedEntries) {
      pinnedEntries = _(pinnedEntries);
      $pinnedEntriesReorderer = $($.mustache(pinnedEntriesReorderingTemplate, {
        orderedEntries:   pinnedEntries.select(hasPinnedIndex),
        unorderedEntries: pinnedEntries.reject(hasPinnedIndex)
      })).appendTo($widgetContainer);
      $spinner.hide();
      addReorderingBehavior();
    });
  }

  function showPinnedEntriesReorderer() {
    $thingsToHideWhileReordering.hide();
    $widgetContainer.show();
    renderPinnedEntriesReorderer();
  }

  function initialize() {
    pinnedEntriesReorderingTemplate = $('#reorder_pinned_entries_template').html();
    $widgetContainer = $('<div class="frame"></div>').hide().insertAfter($('#pinned_topics_title'));
    $spinner = $('<div class="loading">&nbsp;</div>').appendTo($widgetContainer);
    $showLink = $('#show_pinned_entries_reordering').click(function() {
      showPinnedEntriesReorderer();
      return false;
    });
    $thingsToHideWhileReordering = $('#pinned-entries-frame, #entries_pagination').add($showLink);
  }

  // Expose it all as a $z module:
  $z.defModule('home/reorder_pinned_entries', {
    initialize: initialize
  });

}(this.jQuery, this._));
