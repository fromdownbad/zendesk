$z.defModule('home/index', {
  initialize: function() {
    if(currentUser.isAgent) {
      $j('#dashboard').ready(function () {
        $j('#dashboard').show();

        $j.get('/home/dashboard', function(data) {
          $j('#dashboard').html(data).show();
        });
      });
    }
    if (currentUser.isAdmin) {
      new IntroductoryTextForm();
    }

    //ZD269108: Any agent with permission to manage pinned entries should be able to do it
    if (currentUser.managePinnedOrder) {
      $z('home/reorder_pinned_entries').initialize();
    }

    // use sammy forum navigation
    $z("forums/index").initialize();
  }
});
