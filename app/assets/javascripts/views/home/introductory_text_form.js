(function(window, $) {

  var IntroductoryTextForm = window.IntroductoryTextForm = function() {
    this.form = $("#introductory_text .introductory_text_form");
    this.display = $("#introductory_text .introductory_display_texts");
    this.editLink = this.display.find(".edit_this");
    this.cancelLink = this.form.find(".cancel_link");

    this.setup();
  };

  IntroductoryTextForm.prototype = {
    setup: function() {
      var scope = this;

      this.form.hide();

      this.editLink.click(function() {
        scope.display.hide();
        scope.form.show();
      });

      this.cancelLink.click(function() {
        scope.form.hide();
        scope.display.show();
      });
    }
  };

}(window, $j));

