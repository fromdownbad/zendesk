/*global $j, $z, Zendesk */
/*jshint lastsemic: true, expr: true, whitespace: true*/

(function() {

  var forumsSearch = Zendesk.NS('Forums.Search');

  $z.defModule("home/_home_page_search_box", {

    // ---- intializing ----
    initialize: function(params) {
      params = params || {};
      params.homePageSearch = true;
      forumsSearch.Autocomplete.setup(params);
    }

  });
})();
