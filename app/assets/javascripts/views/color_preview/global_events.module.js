var GlobalEvents = (function() {
  var queues = {};

  return {
    trigger: function(eventName, data) {
      if (!queues.hasOwnProperty(eventName)) return;
      for (var i=0; i < queues[eventName].length; i++) {
        queues[eventName][i](data);
      }
    },

    on: function(eventName, listener) {
      if (!queues.hasOwnProperty(eventName)) {
        queues[eventName] = [];
      }
      queues[eventName].push(listener);
    },

    off: function(eventName, listener) {
      if (!eventName) queues = {};
      if (!queues.hasOwnProperty(eventName)) return;
      if (!listener && eventName) {
        queues[eventName] = [];
      }
      if (listener && eventName) {
        var index = queues[eventName].indexOf(listener);
        if (index === -1) return;
        queues[eventName].splice(index, 1);
      }
    }
  };
}());

module.exports = Object.freeze(GlobalEvents);
