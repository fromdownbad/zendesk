/*globals require, $j */
var PreviewClient = (function() {
  var originalBrandingHtml,
      colorCalc = require("views/color_preview/color_calc"),
      previewStateCallback = function (data) {
        if (data.memo.previewing) {
          $j("#inner_branding_styles")[0].outerHTML = data.memo.classic_styles;
        }
      },
      previewEndedCallback = function () {
        $j("#inner_branding_styles")[0].outerHTML = (originalBrandingHtml);
      };

  return {
    init: function (){
      this.xFrameInit();
      this.checkPreviewState();
      return this;
    },

    xFrameInit: function () {
      var globalEvents = require('views/color_preview/global_events');
      originalBrandingHtml = $j("#inner_branding_styles")[0].outerHTML;
      require('views/color_preview/x_frame_com').setup();
      globalEvents.on('@action:reset_branding', previewEndedCallback.bind(this));
      globalEvents.on('@action:preview_state_response', previewStateCallback.bind(this));
    },

    checkPreviewState:  function () {
      var xFrameClient = require('views/color_preview/x_frame_client');
      xFrameClient({
        target: 'check_preview_state',
        source: 'branding_check_preview'
      });
    },

    setBranding: function (color) {
      this.startLotusPreview(color, 'save_branding');
    },

    startLotusPreview: function (color, target) {
      var xFrameClient = require('views/color_preview/x_frame_client');
      var colors = colorCalc.calculateColors(color);
      $j.ajax({
        url: '/settings/account/preview_branding',
        type: 'POST',
        data: colors
      }).done(
        function(data){
          $j("#inner_branding_styles")[0].outerHTML = (data.classic_styles);
          xFrameClient({
            target: target,
            source: 'branding_preview',
            memo: {
              styles: data,
              currentClassic: originalBrandingHtml,
              borderColor: colors.bgColor,
              bodyClass: colors.bodyClass
            }
          });
        }
      );
    },

    endLotusPreview: function () {
      var xFrameClient = require('views/color_preview/x_frame_client');
      xFrameClient({
        target: 'preview_manager_stop',
        source: 'branding_end_preview'
      });
    }
  };
}());

module.exports = PreviewClient;
