module.exports = function(msg) {
  if (msg != null && typeof msg === 'object') {
    msg = JSON.stringify(msg);
  }
  var loc = window.location;
  var origin = loc.protocol + '//' + loc.host;
  window.top.postMessage(msg, origin);
};
