/*globals YAHOO */
var ColorCalc = {
  validHexRegEx: /^#?[0-9a-f]{3,6}$/i,

  calculateColors: function (primaryColor) {
    var hex = this.sanitize( primaryColor );
    if ( !hex ) return false;

    var rgb = YAHOO.util.Color.hex2rgb( hex ),
        hsv = YAHOO.util.Color.rgb2hsv( rgb[0], rgb[1], rgb[2] ),
        tabBgColor = this.getTabBgHexFrom( hsv );


    return {
      tabBgColor: tabBgColor,
      tabHoverColor: this.getTabHoverHexFrom( hsv ),
      textColor: this.getTextHexFrom( hsv ),
      borderColor: this.getBorderColor( tabBgColor ),
      bgColor: hex,
      bodyClass: this.getBodyClass( hsv )
    };
  },

  isValid: function ( hex ) {
    return this.validHexRegEx.test( hex );
  },

  sanitize: function ( hex ) {
    if ( this.isValid( hex ) ) {
      hex = this.removeHashFrom( hex );
      hex = this.sixDigit( hex );
      return hex;
    } else return false;
  },

  removeHashFrom: function ( hex ) {
    return hex.replace(/#/gi, "");
  },

  sixDigit: function ( hex ) {
    return hex.length === 6 ?
      hex : hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  },

  themeIsLight: function ( hsv ) {
    return hsv[2] > 0.85;
  },

  getBodyClass: function ( hsv ) {
    return this.themeIsLight(hsv) ? "custom-branding-light" : "custom-branding-dark";
  },

  getTabHoverHexFrom: function ( hsv ) {
    var h = hsv[0],
        s = hsv[1] * 0.62,
        v = hsv[2] * 1.02 > 1.0 ? 1.0 : hsv[2] * 1.02,
        hsvNew = this.hsvToHex( [h, s, v] );
    return hsvNew === "FFFFFF" ? "E8E8E8" : hsvNew;
  },

  hsvToHex: function ( hsv ) {
    var rgb = YAHOO.util.Color.hsv2rgb( hsv[0], hsv[1], hsv[2] );
    return YAHOO.util.Color.rgb2hex( rgb[0], rgb[1], rgb[2] );
  },

  getTextHexFrom: function ( hsv ) {
    return this.themeIsLight(hsv) ? "2A2A2A" : "FFFFFF";
  },

  getTabBgHexFrom: function ( hsv ) {
    var h = hsv[0] * 1.03 > 1 ? 1 : hsv[0] * 1.03,
        s = hsv[1] * 0.87,
        v = hsv[2] * 0.83;

    return this.hsvToHex( [h, s, v] );
  },

  getBorderColor: function ( hex ) {
    var rgb = YAHOO.util.Color.hex2rgb( hex ),
        hsv = YAHOO.util.Color.rgb2hsv( rgb[0], rgb[1], rgb[2] );

    if ( hsv[2] < 0.5 ) hsv[2] *= 1.2;
    else hsv[2] *= 0.8;

    rgb = YAHOO.util.Color.hsv2rgb( hsv[0], hsv[1], hsv[2] );
    return YAHOO.util.Color.rgb2hex( rgb[0], rgb[1], rgb[2] );
  }
};
module.exports = ColorCalc;