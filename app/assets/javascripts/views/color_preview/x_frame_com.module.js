/*globals require */
var GlobalEvents = require('views/color_preview/global_events');

var isValidOrigin = function(origin) {
  var loc = this.location;
  var expectedOrigin = loc.protocol + '//' + loc.host;
  return origin === expectedOrigin;
};

var xFrameMessageHandler = function(event) {
  // data JSON structure:
  // {
  //    "target": "test",
  //    "source": "module", - optional
  //    "memo": {} - the data payload - optional
  // }

  if (!isValidOrigin(event.origin)) return;

  var data;
  try {
    data = event.data && JSON.parse(event.data) || {};
  } catch (e) {return;}

  if (!data.target) return;

  GlobalEvents.trigger('@action:' + data.target, data);
};

module.exports = Object.freeze({
  setup: function() {
    window.top.addEventListener('message', xFrameMessageHandler, false);
  }
});
