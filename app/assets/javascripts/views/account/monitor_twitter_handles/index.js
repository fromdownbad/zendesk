$z.defModule('account/monitored_twitter_handles/index', {
  initialize: function() {
    var addMonitorLink = $j('p.add_monitor_handle a#add-twitter');
    if(arguments[0] == true) {
      addMonitorLink.click(function(){
        $j('div.help-bubble').slideDown("200");
      });
    } else {
      addMonitorLink.click(this.addTwitterMonitorHandle());
    }

    MTH.initialize();
  },
  addTwitterMonitorHandle : function() {
    $j.colorbox({href: '/account/monitored_twitter_handles/new'})
  }
});

var MTH = {
  initialize: function() {
    $j('input.handle_master').each(function(index, checkbox) {
      $j(checkbox).click(MTH.setPrimary);
    });
  },

  setPrimary: function(event) {
    var checkbox = $j(event.target);
    var id       = checkbox.attr('id').split('_').last();
    var checked  = checkbox.attr('checked');

    if (checked) {
      MTH.uncheckSiblings(checkbox);
    }

    MTH.togglePrimaryFlag(id, checked);
    MTH.requestUpdate(id, checked);
  },

  showSpinner: function(id) {
    $j('#spinner-' + id).show();
  },

  hideSpinner: function(id) {
    $j('#spinner-' + id).hide();
  },

  uncheckSiblings: function(checkbox) {
    $j('input.handle_master').prop('checked', false);
    checkbox.attr('checked', true);
  },

  togglePrimaryFlag: function(id, checked) {
    $j('.primary-selector').hide();

    if (checked) {
      $j('#primary-selector-' + id).show();
    }
  },

  requestUpdate: function(id, checked) {
    MTH.showSpinner(id);

    $j.ajax({
      type: 'PUT',
      url: '/account/channels/' + id,
      data: { 'handle': { 'master': checked } },
      success: function() {
        MTH.hideSpinner(id);
      }
    });
  }
};
