var HeaderRenderer = {};

HeaderRenderer.hasRenderedTopRight = false;
HeaderRenderer.renderTopRight = function() {
  var topRight = $j('#top-right');
  if (!topRight || HeaderRenderer.hasRenderedTopRight) {
    return;
  };

  HeaderRenderer.hasRenderedTopRight = true;

  var menuItems = [];

  if (currentUser) {
    if (!currentUser.isAnonymous) {
      var name = currentUser.first_name;

      if (currentAccount.isSandbox) {
        name = name + ' (SANDBOX)';
      }

      if (currentUser.isAgent || currentAccount.showUserProfile) {
        menuItems.push($j('<a />', { id: 'top-right-name', href: '/users/' + currentUser.id, html: name }));
      } else {
        menuItems.push($j('<span/>', { id: 'top-right-name', html: name }));

        if (currentAccount.showChangePassword) {
          menuItems.push($j('<a/>', {href: '/password', text: I18n.t('txt.layout.change_pwd')}));
        }
      }

      if (currentUser.localeIsDifferent) {
        menuItems.push($j('<a />', {href: '?locale=' + currentUser.locale_id, text: currentUser.localeIsDifferent}));
      }
    }

    if (currentUser.isAdmin || (currentUser.isAgent && (currentAccount.isLotusVisibleToAgents || currentAccount.doAgentsPreferLotus))) {
      var linkText = (currentAccount.doAgentsPreferLotus) ? I18n.t('txt.views.shared.header.back_to_agent') : I18n.t('txt.views.shared.header.try_the_new_zendesk'),
          linkUrl = "https://" + currentAccount.subdomain + '.' + currentAccount.domain + "/agent/";

      if (window.ticket_id) {
        linkUrl += "#/tickets/" + window.ticket_id;
      }

      menuItems.push('<a href="/?return_to=' + escape(linkUrl) + '">' + linkText + '</a>');
    }

    if (currentUser.isAgent && currentAccount.lastTrialDay) {
      menuItems.push(I18n.t('txt.admin.public.javascript.header_renderer.days_left_in_trial_label_with_params', {count: currentAccount.daysLeftInTrial}));
      jQuery('#expires_badge .count').text(currentAccount.daysLeftInTrial);
    };

    if (currentUser.isAdmin) {
      menuItems.push('<a href="http://support.zendesk.com">'+ I18n.t('txt.admin.public.javascript.header_renderer.help') +'</a>');
    };

    if (currentUser.assumed) {
      menuItems.push('<a href="/users/revert">' + I18n.t('txt.javascript.views.agent_console.revert_identity') + '</a>');
    };

    if (currentUser.isAnonymous) {
      var return_to = document.location.href.replace(/\/home\/?$/, "/"); // zd645961 let AccessController decide where to redirect
      menuItems.push($j('<a/>', {href: '/login?return_to=' + escape(return_to), text: I18n.t('txt.layout.login')}));

      if (currentAccount && currentAccount.isOpen && !currentAccount.hasRemoteAuthentication) {
        menuItems.push($j('<a/>', {href: '/registration', text: I18n.t('txt.layout.signup')}));
      };
    } else {
      menuItems.push($j('<a/>', {href: '/access/logout', text: I18n.t('txt.layout.logout')}));
    };
  }

  if (Zendesk.viewedOnMobileDevice && currentAccount.hasFeature('mobile')) {
    var returnToUrl = document.location.href;
    menuItems.push('<a href="/mobile/switch?return_to=' + escape(returnToUrl) + '">' + I18n.t('txt.views.shared.header.go_to_mobile_site') + '</a>');
  }

  if (menuItems.length > 0) {
    topRight.append(menuItems.shift());
    menuItems.each(function(item) {
      topRight.append(' | ');
      topRight.append(item);
    });
  };
};

function buildOrganizationLink(organization, klass) {
  var path = currentAccount.urlPrefix + '/organizations/' + organization.id + '/requests';
  var attributes = { href: path, text: organization.name.truncate(35) };
  if (klass) {
    attributes['class'] = klass;
  }
  return $j('<a>', attributes)[0];
};

HeaderRenderer.hasUpdatedTabs = false;
HeaderRenderer.updateTabs = function() {
  var topMenu = $j('#top-menu > ul#green').first();
  if (!topMenu || HeaderRenderer.hasUpdatedTabs) {
    return;
  };

  HeaderRenderer.hasUpdatedTabs = true;

  if (currentUser) {
    if (currentUser.isAnonymous) {
      if (!currentAccount.isOpen) { // the account is not open so we remove all tabs (Except Home so that Forums tab will be visible if there are any forums the user can see)
        topMenu.find('> li:not(.right, .tab_home)').remove();
      } else if (!currentAccount.hasRemoteAuthentication && !currentUser.hasEmail) { // open, but has remote auth
        topMenu.find('> li.tab_new a').attr('href', '/anonymous_requests/new');
      };
    };

    if (currentUser.isEndUser) {
      var previousTab = topMenu.find("> li:not(.right)").last();
      var organizationTab = $j('<li class="main tab_organization"/>');

      if (currentUser.canViewMultipleOrganizations) { //has multiple shared orgs
        //add organization tab with dropdown of orgs under it
        organizationTab.append($j('<a/>', {'class': 'tab', href: '#', text: I18n.t('txt.layout.organization_requests')}));
        var temp = $j('<ul class="menu-drop">');

        currentUser.sharedOrganizations.each(function(org, index) {
          var organizationListItem = $j('<li>').append(buildOrganizationLink(org))[0];
          temp.append(organizationListItem);
          });
        organizationTab.append(temp);
      } else if (currentUser.canViewOrganization) { //does not have multiple shared, but default org is shared
        //add the organization tab for the default org
        organizationTab.append(buildOrganizationLink(currentUser.organization, 'tab'));
      } else if (currentUser.sharedOrganizations.length) { //default org is not shared, but has a shared org
        //add the organization tab for the shared org
        organizationTab.append(buildOrganizationLink(currentUser.sharedOrganizations[0], 'tab'));
      };
      previousTab.after(organizationTab);
    };
  }

  topMenu.find('> li.first').removeClass('first');
  topMenu.find('> li.active').removeClass('active');
  topMenu.find('> li').first().addClass('first');
  topMenu.find('> li').addClass('main');
  var activeTab = topMenu.find('> li.tab_' + Zendesk.tab);
  activeTab.removeClass('main').addClass('active');
  activeTab.next().addClass('first');
};

if (typeof(currentUser) !== 'undefined') {
  HeaderRenderer.renderTopRight();
  HeaderRenderer.updateTabs();
};
