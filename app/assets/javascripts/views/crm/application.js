
Zendesk.NS('CRM')

Zendesk.CRM.Application = function(renderer) {
  this.renderer = renderer;
  this.initialDelay = 5000;
  this.backoff = 0;
}

Zendesk.CRM.Application.prototype = {
  renderData: function(records, status) {
    if(status) {
      this.renderer.render(records, status);
    } else {
      this.renderer.render(records, "ok");
    }
  },

  fetchAndRenderData: function(url) {
    this.url = url;
    this._setupPoller();
    this.renderer.renderWaiting();
    this.poller.start();
  },

  _setupPoller: function() {
    var self = this;
    this.poller = new Zendesk.CRM.Poller(function(poller) {
      self._pollerCallback(self, poller);
    }, {initialDelay: this.initialDelay, backoff: this.backoff});
  },

  _pollerCallback: function(self, poller) {
    $j.ajax({
      type: 'get',
      url: this.url,
      success: function(response) {
        if (response.status === "ok") {
          self.poller.stop();
          self.renderData(response.records);
        } else if(response.status == "errored") {
          self.poller.stop();
          self.renderData(response.records, response.status);
        } else {
          self.poller.resume();
        }
      },
      error: function() {
        self.poller.stop();
        self.renderData([], "errored");
      }
    });
  }


}