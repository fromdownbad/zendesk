
Zendesk.NS('CRM')

Zendesk.CRM.ProfileRenderer = function() {
  this.element = $j('#crm_user_data');
  this.success_template = $j('#crm_template').html();
  this.error_template = $j('#crm_error_template').html();
  this.loading = $j('#crm_loading');
}

Zendesk.CRM.ProfileRenderer.prototype = {
  renderWaiting: function() {
    this.loading.show();
  },

  render: function (records, status) {
    if(status == "ok") {
      this._renderSuccess(records);
    } else if(status == "pending") {
      this.renderWaiting();
    } else {
      this._renderError();
    }
  },

  _renderSuccess: function(records) {
    var self = this;
    _(records).each(function(record) {
      record.fields = self._splitFields(record.fields);
    });

    var html = $j.mustache(this.success_template, {records: records});
    this.element.html(html);
    this.loading.hide();
  },

  _renderError: function() {
    this.element.html(this.error_template);
    this.loading.hide();
  },

  _splitFields: function(fields) {
    var arr = [];
    var record1, record2;
    for(var i=0; i < fields.length; i++) {
      record1 = fields[i];
      if (fields[i+1]) {
        record2 = fields[i+1];
        i = i+1;
      } else {
        record2 = {label:"&nbsp;", value:"&nbsp;", empty:true}
      }
      arr.push({record1: record1, record2: record2});
    }

    return arr;
  }
}