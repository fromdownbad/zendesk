
Zendesk.NS('CRM')

Zendesk.CRM.SidebarRenderer = function(profile_url) {
  this.element = $j('#crm_user_data');
  this.success_template = $j('#crm_template').html();
  this.error_template = $j('#crm_error_template').html();
  this.loading = $j('#crm_loading');
  this.profile_url = profile_url;
  this.fieldsToShow = 10;
}

Zendesk.CRM.SidebarRenderer.prototype = {
  renderWaiting: function() {
    this.loading.show();
  },

  render: function (records, status) {
    if(status == "ok") {
      this._renderSuccess(records);
    } else if(status == "pending") {
      this.renderWaiting();
    } else {
      this._renderError();
    }
  },

  _renderSuccess: function(records) {
    if(records.length > 0) {
      var mainRecord = this._addMoreLink(records[0]);
      var self = this;
      var subRecords = _(records.slice(1)).map(function(record) { return self._addMoreLink(record);});
      var showMore = subRecords.length;
    } else {
      var mainRecord = null;
      var subRecords = null;
      var showMore = null;
    }

    var html = $j.mustache(this.success_template, {mainRecord: mainRecord, subRecords: subRecords, showMore: showMore});

    this.element.html(html);
    this.loading.hide();

    var self = this;
    $j(".crm-records-toggle", self.element).click(function(event) {
      $j("#crm-sub-records", self.element).slideToggle(function() {
      $j(".crm-records-toggle", self.element).toggle();
    });
      event.stopPropagation();
      event.preventDefault();
    });
  },

  _addMoreLink: function(record) {
    if(record.fields && record.fields.length > this.fieldsToShow) {
      record.fields = record.fields.slice(0, this.fieldsToShow);
      record.moreLink = this.profile_url;
    }

    return record;
  },

  _renderError: function() {
    this.element.html(this.error_template);
    this.loading.hide();
  }
}