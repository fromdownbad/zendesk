
Zendesk.NS('CRM')

Zendesk.CRM.Poller = function(callback, options) {
  this.initialDelay	    = options.initialDelay || 10000;
  this.backoff		      = options.backoff || 5000;
  this.waitForCallback  = options.waitForCallback || false;
  this.callback		      = callback;
  this.state            = "stopped"; //["stopped", "paused", "polling", "resetting", "resuming"]
  this.currentDelay     = this.initialDelay;
  this.waitDelay        = options.waitDelay || 2000;
  this.debug            = !!options.debug;
  this.trace            = "";
};

Zendesk.CRM.Poller.prototype = {
  start: function() {
    this.state = "polling";
    this._schedulePoll(this.initialDelay);
    this._trace("[started]");
  },

  stop: function() {
    this.state = "stopped";
  },

  reset: function() {
    this.state = "resetting";
  },

  pause: function() {
    this.state = "paused";
    this._trace("[paused]");
  },

  resume: function() {
    if(this.state == "paused") {
      this._trace("[resuming]");
      this.state = "resuming";
    }
  },

  _schedulePoll: function(after) {
    var self = this;
    setTimeout(function() {
      self._pollerCallback();
    }, after);
  },

  _pollerCallback: function() {
    if(this.state == "polling") {
      this.currentDelay = this.currentDelay + this.backoff;
      var delay = this.currentDelay;
      if(this.waitForCallback) {
        this._trace("[pausing]");
        this.pause();
        delay = this.waitDelay;
      }
      this._trace("[callback]");
      this.callback(this);
      this._schedulePoll(delay);
    } else if(this.state == "resetting") {
      this.state = "polling";
      this.currentDelay = this.initialDelay;
      this._schedulePoll(this.initialDelay);
    } else if(this.state == "paused") {
      this._trace(".");
      this._schedulePoll(this.waitDelay);
    } else if(this.state == "resuming") {
      this._trace("[resumed]");
      this.state = "polling";
      this._schedulePoll(this.currentDelay);
    }
  },

  _trace: function(msg) {
    if(this.debug) {
      this.trace = this.trace + msg;
    }
  }

}
