if(!Zendesk) { Zendesk = {} };
if(!Zendesk.UI) { Zendesk.UI = {} };

Zendesk.UI.MacroList = {
  MAX_RESULTS: 20,
  initialize: function (macros, macro_search_enabled, id) {
    var self = this;
    self.root = $j(id);
    self.macro_search_enabled = macro_search_enabled;

    $j(document).bind('keydown', 'ctrl+m', self.toggle_macro_menu_with_key_combo()); // ctrl-m key combo opens macro menu; keypress has some bugs with current version of Hotkeys
    $j('form#ticket-chat').find('input,textarea,select').bind('keydown', 'ctrl+m', self.toggle_macro_menu_with_key_combo());

    $j(document).keydown(self.escape_close()); // escape closes macro menu even there's no macro search input

    if (macro_search_enabled) {
      self.search_results = self.root.find('ul.search_results');
      self.nested_macros =  self.root.find('ul.first-drop > li');
      self.root.data('cache', {'': {results: [], max_index: 0}});
      self.root.data('full_results', $j.map(macros, function(macro, index){return(index);}));
      self.macros = macros;
      self.root.data('macro_list', self.root.find('ul.search_results li.search_result'));
      self.add_apply_macro();
      self.root.data('input', $j(id + '_search'));
      self.root.find('ul.drop-list li ul.first-drop div.search img.clear').click(function (event) {self.clear();});
      self.root.find('ul.drop-list li ul.first-drop ul.search_results li.no_results div.explain p.try_again a.clear_search').click(function (event) {self.clear();});
      self.root.data('input').bind('keydown', self.escape_clear()).bind('keyup change', self.on_text_changed(id, self.root.data('cache'), self.root.data('full_results'), macros, self.root.data('macro_list'))); // keypress doesn't catch escape
      self.root.find('ul.drop-list > li').click(function(event){self.open_macro_menu();}); // focus to the search input when the user clicks on "Apply macro"
      self.root.data('input').keydown(self.advance_on_key(38, -1)).keydown(self.advance_on_key(40, 1)).keydown(self.select_with_enter());
    }
  },
  select_with_enter: function () {
    var self = this;
    return(function (event) {
      var selection = self.search_results.data('selection');
      if (event.keyCode === 13 && (selection !== null && selection !== undefined)) {
        event.preventDefault();
        event.stopPropagation();
        self.root.data('macro_list').eq(selection).click();
      }
    });
  },
  advance_on_key: function (key_code, steps) {
    var self = this;
    return(function (event) {
      if (event.keyCode === key_code) {
        event.stopPropagation();
        event.preventDefault();
        self.advance_selection(steps);
      }
    });
  },
  advance_selection: function (steps) {
    var self = this;
    var search_term = (typeof(self.root.data('search_term')) !== 'undefined') ? self.root.data('search_term') : '';
    var cache = self.root.data('cache')[search_term];

    if (cache.results.length > 0) {
      steps %= cache.max_index;
      var selection_index = ((typeof (self.search_results.data('selection_index')) === 'undefined') ? 0 : self.search_results.data('selection_index') + steps + cache.max_index) % cache.max_index;
      self.select(selection_index);
    }
  },
  select: function (selection_index) {
    var self = this;
    var previous_selection = self.search_results.data('selection');
    var search_term = (typeof(self.root.data('search_term')) !== 'undefined') ? self.root.data('search_term') : '';
    var cached_results = self.root.data('cache')[search_term].results;

    if (typeof(previous_selection) !== 'undefined') {
      self.root.data('macro_list').eq(previous_selection).removeClass('selected');
    }
    if (typeof (selection_index) === 'number' && selection_index >= 0) {
      self.search_results.data('selection_index', selection_index);
      self.search_results.data('selection', cached_results[selection_index]);
      self.root.data('macro_list').eq(cached_results[selection_index]).addClass('selected');
    } else {
      self.search_results.data('selection', null);
    }
  },
  toggle_macro_menu_with_key_combo: function () {
    var self = this;
    var menu = self.root.find('ul.first-drop');

    return(function (event) {
      event.stopPropagation();
      event.preventDefault(); // if the browser uses ctrl-m, tell it not to - we're overriding it
      if (menu.css('display') === 'none') {
        self.open_macro_menu();
      } else {
        self.close_macro_menu();
      }
    });
  },
  close_macro_menu: function () {
    var self = this;
    self.root.find('ul.first-drop').hide();
    if (self.macro_search_enabled) { self.clear(); }
  },
  open_macro_menu: function () {
    var self = this;
    self.root.find('ul.first-drop').show();
    if (self.macro_search_enabled) { self.root.data('input').focus(); }
  },
  clear: function () {
    var self = this;

    self.root.data('input').val('');
    self.root.data('input').change();
  },
  escape_close: function(){
    var self = this;
    return(function(event){
      if(event.keyCode === 27) /* escape key pressed */ { self.close_macro_menu(); }
    });
  },
  escape_clear: function(){
    var self = this;
    return(function(event){
      if(event.keyCode === 27){ // escape key pressed
        event.preventDefault();
        if($j.trim($j(this).val()) !== ''){ // close the menu
          event.stopPropagation(); // don't let the event bubble up to trigger the close
          self.clear(); // clear the text
        }
      }
    });
  },
  on_text_changed: function(id, cache, full_results, macros, macro_list) {
    // This code is EXTREMELY optimized. Please be careful if you change it!
    var self = this;

    var no_results = self.root.find('ul.search_results li.no_results');
    var display_term = no_results.find('span.search_term');
    var search_img = self.root.find('ul.drop-list li ul.first-drop div.search img.search');
    var clear_img = self.root.find('ul.drop-list li ul.first-drop div.search img.clear');

    var highlight = function (index, regex) {
      return(macros[index].label.replace(regex, '<span class="highlight">$1</span>'));
    };
    var update_macro = function (index, new_html) { macro_list.eq(index).find(' > a').html(new_html); };
    return (function (event) {
      var previous_search_term = (typeof(self.root.data('search_term')) !== 'undefined') ? self.root.data('search_term') : '';
      var previous_cache = cache[previous_search_term];
      var search_term = $j.trim($j(this).val());
      self.root.data('search_term', search_term);

      var cachify = function(superset, hide_previous_results) {
        if (typeof(hide_previous_results) === 'undefined') { hide_previous_results = false; }
        var regex_escaped_search_term = $j.ui.autocomplete.escapeRegex(search_term);

        var regex = RegExp(regex_escaped_search_term, 'i');
        var highlight_regex = RegExp('(' + regex_escaped_search_term + ')', 'gi');
        var highlighted;
        var superset_index = 0;
        var subset = [];
        var highlights = [];
        if (hide_previous_results) {
          for (var i = 0; i < previous_cache.max_index; i++) { macro_list.eq(previous_cache.results[i]).hide(); }
          for (; subset.length < self.MAX_RESULTS && superset_index < superset.length; superset_index++) {
            if (macros[superset[superset_index]].label !== (highlighted = highlight(superset[superset_index], highlight_regex))) { // string was changed, because there were a substitution because it contained the query
              subset.push(superset[superset_index]);
              highlights.push(highlighted);
              update_macro(superset[superset_index], highlighted);
              macro_list.eq(superset[superset_index]).show();
            }
          }
        } else {
          for (; subset.length < self.MAX_RESULTS && superset_index < superset.length; superset_index++) {
            if (macros[superset[superset_index]].label === (highlighted = highlight(superset[superset_index], highlight_regex))) { // string was unchanged, because there were no substitutions because it didn't contain the query
              macro_list.eq(superset[superset_index]).hide();
            } else {
              subset.push(superset[superset_index]);
              highlights.push(highlighted);
              update_macro(superset[superset_index], highlighted);
              macro_list.eq(superset[superset_index]).show();
            }
          }
          for (; superset[superset_index] < previous_cache.results[previous_cache.max_index] ; superset_index++) {
            macro_list.eq(superset[superset_index]).hide(); // hiding everything that could possibly be visible
            if (regex.test(macros[superset[superset_index]].label)) {
              subset.push(superset[superset_index]);
            }
          }
        }
        for (; superset_index < superset.length; superset_index++) { // todo: take out this entire for loop, and change the code above to start looking at the full result set once it's out of superset results = big efficiency boost
          if (regex.test(macros[superset[superset_index]].label)) {
            subset.push(superset[superset_index]);
          }
        }
        cache[search_term] = {max_index: Math.min.apply(Math, [subset.length, self.MAX_RESULTS]), regex: regex, results: subset, highlights: highlights};
      };

      if (search_term !== previous_search_term) {
        if (search_term.length === 0) {
          clear_img.hide();
          search_img.show();
          self.search_results.hide();
          self.nested_macros.show();
          for (var i = 0; i < previous_cache.max_index; i++) { macro_list.eq(previous_cache.results[i]).hide(); }
        } else {
          if (previous_search_term.length === 0) {
            search_img.hide();
            clear_img.show();
            self.nested_macros.hide();
            self.search_results.show();
          }
          var previous_index = 0, current_index = 0;
          if (previous_search_term.length > 0 && search_term.length > previous_search_term.length && previous_cache.regex.test(search_term)) { // drill down into a subset of what's already selected
            if (typeof(cache[search_term]) === 'object' && cache[search_term].max_index > 0) {
              if (previous_cache.results[previous_cache.max_index - 1] < cache[search_term].results[cache[search_term].max_index - 1]) {
                for (; previous_index < previous_cache.max_index ; previous_index++) {
                  if (previous_cache.results[previous_index] < cache[search_term].results[current_index]) {
                    macro_list.eq(previous_cache.results[previous_index]).hide();
                  } else {
                    update_macro(cache[search_term].results[current_index], cache[search_term].highlights[current_index]);
                    current_index++;
                  }
                }
                for (; current_index < cache[search_term].max_index ; current_index++) {
                  update_macro(cache[search_term].results[current_index], cache[search_term].highlights[current_index]);
                  macro_list.eq(cache[search_term].results[current_index]).show();
                }
              } else {
                for (; cache[search_term].results[current_index] <= previous_cache.results[previous_cache.max_index - 1]; current_index++) {
                  update_macro(cache[search_term].results[current_index], cache[search_term].highlights[current_index]);
                  for (; previous_cache.results[previous_index] < cache[search_term].results[current_index]; previous_index++) {
                    macro_list.eq(previous_cache.results[previous_index]).hide();
                  }
                  if (previous_cache.results[previous_index] === cache[search_term].results[current_index]) {
                    previous_index++;
                  }
                }
                for (; previous_index < previous_cache.max_index ; previous_index++) {
                  macro_list.eq(previous_cache.results[previous_index]).hide();
                }
              }
            } else {
              cachify(previous_cache.results, false);
            }
          } else {
            if (typeof(cache[search_term]) === 'object') {
              for (previous_index = 0, current_index = 0; previous_index < previous_cache.max_index && current_index < cache[search_term].max_index;) {
                if (previous_cache.results[previous_index] < cache[search_term].results[current_index]) {
                  macro_list.eq(previous_cache.results[previous_index]).hide();
                  previous_index++;
                } else if (previous_cache.results[previous_index] > cache[search_term].results[current_index]) {
                  update_macro(cache[search_term].results[current_index], cache[search_term].highlights[current_index]);
                  macro_list.eq(cache[search_term].results[current_index]).show();
                  current_index++;
                }
                else {
                  update_macro(cache[search_term].results[current_index], cache[search_term].highlights[current_index]);
                  current_index++;
                  previous_index++;
                }
              }
              if (previous_index === previous_cache.max_index) {
                for (; current_index < cache[search_term].max_index; current_index++) {
                  update_macro(cache[search_term].results[current_index], cache[search_term].highlights[current_index]);
                  macro_list.eq(cache[search_term].results[current_index]).show();
                }
              }
              if (current_index === cache[search_term].max_index) {
                for (; previous_index < previous_cache.max_index; previous_index++) {
                  macro_list.eq(previous_cache.results[previous_index]).hide();
                }
              }
            } else {
              cachify(full_results, true);
            }
          }
          if (cache[search_term].results.length > 0) {// indicate that there were no matches
            self.select(0);
            if (previous_cache.results.length === 0) {
              no_results.hide();
            }
          } else {
            self.select();
            display_term.html(search_term);
            if (previous_cache.results.length > 0 || previous_search_term.length === 0) {
              no_results.show();
            }
          }
        }
      }
    });
  },
  add_apply_macro: function(index) {
    var self = this;

    self.root.find('ul.search_results li.search_result').each(function (index, element) {
      var case_id = self.macros[index].value;
      if (Number(case_id) >= 0) {
        $j(element).click(function (event) {
    var ticketForm = $j("#ticket-chat");
    if(typeof(ticket_id) === 'undefined') {
      var this_id = 0;
    } else {
      var this_id = ticket_id;
    }

    if(jQuery("#tickets_to_bulk_update", ticketForm).length > 0) {
      new Zendesk.API.Macro(case_id).applyToTicket(null, ticketForm);
    } else {
      new Zendesk.API.Macro(case_id).applyToTicket(this_id, ticketForm);
    }

          $j(this).fadeOut('fast').fadeIn('fast', function () {
            self.clear();
            self.root.find('ul.drop-list li ul.first-drop').hide();
          });
        });
      }
    });
  }
};


$z.defModule('shared/_macro_list', {
  initialize: function (macros, macroSearchEnabled, id) {
    Zendesk.UI.MacroList.initialize(macros, macroSearchEnabled, id);
  }
});
