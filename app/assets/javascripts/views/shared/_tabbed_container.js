/*globals window, Zendesk, $j*/
(function($, Zendesk) {
  var container,
      linksContainer,
      contentContainers,
      sharedContent;

  function selectTab(link) {
    var otherLinks = links().not(link);
    var tabID = link.attr('href');
    var newlySelectedTab = contentContainers.filter(tabID);
    var otherTabs = contentContainers.not(newlySelectedTab);

    otherLinks.removeClass('current');
    otherTabs.filter(':visible').trigger('tab.hide');
    otherTabs.hide();

    link.addClass('current');
    newlySelectedTab.trigger("tab.show");
    newlySelectedTab.show();
    sharedContent.forceRedraw();
  }

  function selectInitialTab() {
    $.history.init(function(hash) {
      var tab = links().filter('[href="#' + hash + '"]');
      if (tab.length === 0) {
        tab = links().first();
      }
      selectTab(tab);
    });
  }

  function addHandlers() {
    $(window).bind('popstate:tabs', function(event) {
      selectInitialTab();
    });
    links().click(function(event) {
      var hash = $(this).attr('href').replace(/^.*#/, '');
      $.history.load(hash);
      event.preventDefault();
    });
  }

  function init(root) {
    container         = $(root || '.tabbed_container');
    linksContainer    = $('.tab_links', container);
    contentContainers = $('.tabs_content .tabs_canvas > div', container);
    sharedContent     = $('.tabs_content .tabs_canvas', container).siblings();
    addHandlers();
    selectInitialTab();
  }

  function links() {
    return $('.tab_links a', container);
  }

  Zendesk.NS.extend('TabbedContainer', {
    init: function(root) {
      init(root);
    },

    destroy: function() {
      $(window).unbind('popstate:tabs');
    }
  });

  $(document).ready(function(){
    if ($("div.tabbed_container[mode!='static']").length > 0) {
      Zendesk.TabbedContainer.init();
    }
  });
}(window.jQuery, window.Zendesk));

Zendesk.TabbedContainer.SammyApp = function(options) {
  options                 = options || {};
  this.options            = options;
  this.container          = options.root || '.sammy_tabbed_container';
  this.links              = $j('.tab_links a', this.container);
  this.contentContainers  = $j('.tabs_content .tabs_canvas > div', this.container);
  this.app                = this._createSammyApp();
  this.PATH_NAME_MATCHER  = /:([\w\d]+)/g;
  this.PATH_REPLACER      = "([^\/]+)";
};

Zendesk.TabbedContainer.SammyApp.prototype = {
  init: function() {
    this.app.disable_push_state = true;
    this.app.run();
  },

  extractParams: function(path, pathParams, paramNames) {
    var self = this, params = {};

    $j.each(pathParams, function(i, param) {
      if (paramNames[i]) {
        params[paramNames[i].slice(1)] = self._decode(param);
      } else {
        if (!params.splat) { params.splat = []; }
        params.splat.push(self._decode(param));
      }
    });

    return params;
  },

  selectTab: function(tab) {
    var id = '#' + tab,
        link = this.links.filter('[href="' + id + '"]');

    if (link.length === 0 || (link.hasClass('current'))) { return; }

    this.links.removeClass('current');
    link.addClass('current');
    this.contentContainers.hide().filter(id).show();
  },

  _createSammyApp: function() {
    var self = this;

    return $j.sammy(this.container, function(app) {

      this.helpers({
        // Tries to behave as close to Sammy.js as possible when matching routes. Retunrs params (similar to this.params passed to any Sammy route).
        // ``path`` is any arbitrary url, ``routePath`` is Sammy like path.
        // Any string starting with : in a ``routePath`` will be pulled out and converted to a named param. Regex is accepted as well.
        // Ie., route = '/reports#forum_analytics/stats/csr/epa', routePath = '#forum_analytics/stats/:statName/:duh',
        // this.matchPath(route, routePath) returns { statName: 'csr', duh: 'epa' }. When routePath is a Regex, it returns something like { splat: [p1, p2...] }.
        matchPath: function(path, routePath) {
          var paramNames = routePath.match(self.PATH_NAME_MATCHER) || [], pathParams;

          routePath = new RegExp(routePath.replace(self.PATH_NAME_MATCHER, self.PATH_REPLACER) + "$");
          if ( !(pathParams = path.match(routePath)) ) { return; }
          return self.extractParams(path, pathParams.slice(1), paramNames);
        }
      });

      this.get(/\#([\w\d]*)[\/]?(.*)/, function() {
        var subPath = this.params.splat[1], tabID = this.params.splat[0];
        self.selectTab(tabID);
        this.trigger('tab.change', { tabID: tabID, path: this.path, subPath: subPath });
      });

      this.get('', function() {
        app.setLocation(self.links.first().attr('href'));
      });

    });
  },

  _decode: function( str ) { return decodeURIComponent((str || '').replace(/\+/g, ' ')); }
};
