$z.defModule("shared/_help_container", {
  initialize: function() {

    $j("span.question-link").click(function() {
      $j(this).nextAll('.help-bubble').slideToggle(50);
    });
    
    $j("span.question-link img").hover(function(){
        $j(this).attr('src', '/images/question-hover.png');
      },
      function(){
      $j(this).attr('src', '/images/question.png');
    });
  }
 
});
zd.jsInitializers.push(["shared/_help_container",[]]);
