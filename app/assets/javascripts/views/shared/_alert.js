Zendesk.NS('Alerts');

Zendesk.Alerts = {
  showTwitterAuthorization: function() {
    var alertType = "twitter_authorization_failure";
    var unauthorizedAccounts = _.select(currentAccount.twitterAccounts, function(twitterAccount) {
      if(twitterAccount.screen_name != null)
        return twitterAccount.authorized == false;
    });

    if(_.size(unauthorizedAccounts) > 0) {
      $j('#flash').append(this.renderTwitterAuthorization(unauthorizedAccounts));
      new Zendesk.AlertManager('#' + alertType, this.cookieName(alertType)).show();
    };
  },

  showFacebookAuthorization: function() {
    var alertType = "facebook_authorization_failure";
    var unauthorizedPages = _.select(currentAccount.facebookPages, function(page) {
      return page.unauthorized;
    });

    if(_.size(unauthorizedPages) > 0) {
      $j('#flash').append(this.renderFacebookAuthorization(unauthorizedPages));
      new Zendesk.AlertManager('#' + alertType, this.cookieName(alertType)).show();
    };
  },

  showPasswordExpiration: function() {
    var alertType = 'password_expiration';
    if(currentUser && currentUser.isPasswordExpiring()) {
      $j('#flash').append(this.renderPasswordExpiration(alertType));
      new Zendesk.AlertManager('#' + alertType, this.cookieName(alertType)).show();
    }
  },

  showWebPortalDeprecationNotice: function() {
    var alertType = 'wp_deprecation';
    if (currentUser.isAgent) {
      $j('#flash').append(this.renderWebPortalDeprecationNotice(alertType));
      new Zendesk.AlertManager('#' + alertType, this.cookieName(alertType)).show();
    }
  },

  showUserAssumeNotice: function() {
    var alertType = 'user_assume';
    if (currentUser.assumed) {
      $j('#flash').append(this.renderUserAssumeNotice(alertType));
      new Zendesk.AlertManager('#' + alertType, this.cookieName(alertType)).show();
    }
  },

  showSystemNotice: function(alertName) {
    new Zendesk.AlertManager('#' + alertName, alertName).show();
  },

  cookieName: function(alertType) {
    return 'accounts.' + currentAccount.id + '.' + alertType;
  },

  renderWebPortalDeprecationNotice: function() {
    var url      = I18n.t('txt.admin.public.javascripts.views.shared._alert.web_portal_deprecation_learn_more_url'),
        link     = I18n.t('txt.admin.public.javascripts.views.shared._alert.web_portal_deprecation_learn_more', { url: url }),
        message  = I18n.t('txt.admin.public.javascripts.views.shared._alert.web_portal_message', { learn_more: link }),
        template = '<div id="wp_deprecation" class="alert">{{{message}}}</div>';

        return jQuery.mustache(template, { message: message });
  },

  renderTwitterAuthorization: function(twitterAccounts) {
    var i18n_first_part = I18n.t('txt.admin.public.javascripts.views.shared._alert.your_account_no_longer');
	  var i18n_second_part = I18n.t('txt.admin.public.javascripts.views.shared._alert.reauthorize_twitter')
    var template = '<div id="twitter_authorization_failure" style="display:none" class="alert">' + i18n_first_part +
      '<ul>\
        {{#accounts}}\
        <li>{{screen_name}}</li>\
        {{/accounts}}\
      </ul>' + i18n_second_part;
    return jQuery.mustache(template, { accounts: twitterAccounts });
  },

  renderFacebookAuthorization: function(facebookPages) {
		var i18n_first_part = I18n.t('txt.admin.public.javascripts.views.shared._alert.your_account_not_longer_permissions_facebook')
		var i18n_second_part = I18n.t('txt.admin.public.javascripts.views.shared._alert.reauthorize_facebook')
    var template = '<div id="facebook_authorization_failure" style="display:none" class="alert">' + i18n_first_part +
      '<ul>\
        {{#pages}}\
        <li>{{name}} - <span class="facebook_authorization_failure_reason">{{reason_for_unauthorization}}</span></li>\
        {{/pages}}\
      </ul>' + i18n_second_part +
      '</div>';
    return jQuery.mustache(template, { pages: facebookPages });
  },

  renderPasswordExpiration: function(id) {
    var template = '<div id="password_expiration" style="display:none" class="alert">{{{message}}}\
      <a style="margin-left:10px" id="hide_expiration" class="close" href="#">'+ I18n.t('txt.public.javascripts.views.shared._alert.hide_this_notice') +'</a></div>';

    var message = I18n.t("txt.security_policy.expiration_alert", {
      count: i18n_distance_of_time_in_words(currentUser.passwordExpiresAt, new Date()),
      url: "/password"
    });

    return jQuery.mustache(template, { message: message });
  },

  renderUserAssumeNotice: function(elementId) {
    var i18nPrimaryMessage = I18n.t('txt.admin.public.javascripts.views.shared._alert.user_assume_notice_primary_text'),
	      i18nSecondaryMessage = I18n.t('txt.admin.public.javascripts.views.shared._alert.user_assume_notice_secondar_text'),
	      i18nRevertIdentityLinkText = I18n.t('txt.admin.public.javascripts.views.shared._alert.revert_identity_link_text'),
	      template = '<div id="{{elementId}}" style="display:none" class="alert">\
          <span class="preamble"> {{primaryMessage}} </span>\
          <span> {{secondaryMessage}} </span>\
          <a href="{{revertIdentityUrl}}"> {{revertIdentityLinkText}} </a>\
        </div>';

    return jQuery.mustache(template, {
      elementId: elementId,
      primaryMessage: i18nPrimaryMessage,
      secondaryMessage: i18nSecondaryMessage,
      revertIdentityLinkText: i18nRevertIdentityLinkText,
      revertIdentityUrl: '/users/revert'
    });
  }
};
