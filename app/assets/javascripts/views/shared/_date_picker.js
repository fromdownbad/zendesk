/*globals window*/
(function($, Zendesk) {

  var firstDay           = Number(I18n.t('date.datepicker.first_day'));
  var isRTL              = Boolean(Number(I18n.t('date.datepicker.is_rtl')));
  var showMonthAfterYear = Boolean(Number(I18n.t('date.datepicker.show_month_after_year')));
  var defaultDateFormat  = 'yy-mm-dd';

  function localizeCalendarDates() {
    $('.new_date_picker').each(function() {
      try {
        var parsed = $.datepicker.parseDate(defaultDateFormat, $(this).val());
        var formatted = $.datepicker.formatDate(I18n.t('date.datepicker.date_format'), parsed);
        $(this).val(formatted);
      }
      catch(exception) {
        // silently fail if there's an exception parsing
        // this exception can happen in the bulk ticket view, when date is "- No change -"
      }

      var altFieldSelector = "#" + $(this).data("field");
      if ($(altFieldSelector).exists()) {
        var dateFieldWas = $(this).val();
        $(this).datepicker( "option", "altField", altFieldSelector );
        $(this).datepicker( "option", "altFormat", defaultDateFormat );
        if (dateFieldWas != $(this).val()) {
          // forcefully set the date and altField back as necessary, i.e. when date is "- No change -"
          $(this).val(dateFieldWas);
          $(altFieldSelector).val(dateFieldWas);
        }
      }
    });
  }

  $(document).ready(function(){
    $.datepicker.regional['localized_datepicker'] = {
      closeText:          I18n.t('date.datepicker.close_text'),  // Display text for close link
      dayNamesMin:        I18n.t('date.min_day_names'),
      prevText:           I18n.t('date.datepicker.prev_text'),   // Display text for previous month link
      nextText:           I18n.t('date.datepicker.next_text'),   // Display text for next month link
      currentText:        I18n.t('date.datepicker.current_text'), // Display text for current month link
      monthNames:         I18n.t('date.month_names').slice(1), // Names of months for drop-down and formatting
      monthNamesShort:    I18n.t('date.abbr_month_names').slice(1), // For formatting
      dayNames:           I18n.t('date.min_day_names'), // For formatting
      dayNamesShort:      I18n.t('date.abbr_day_names'), // For formatting
      weekHeader:         I18n.t('date.datepicker.week_header'), // Column header for week of the year
      dateFormat:         I18n.t('date.datepicker.date_format'), // See format options on parseDate
      firstDay:           firstDay, // The first day of the week, Sun = 0, Mon = 1, ...
      isRTL:              isRTL, // True if right-to-left language, false if left-to-right
      showMonthAfterYear: showMonthAfterYear, // True if the year select precedes month, false for month then year
      yearSuffix:         I18n.t('date.datepicker.year_suffix') // Additional text to append to the year in the month headers
    };

    $.datepicker.setDefaults($.datepicker.regional['localized_datepicker']);

    $('.new_date_picker').datepicker({
      showOn:             "both",
      buttonImage:        "/images/calendar_date_select/calendar.gif",
      buttonImageOnly:    true,
      showAnim:           "slideDown",
      defaultDate:        +7
    });

    // Let users clear the date with the delete/backspace key if allowed
    $('.new_date_picker').keyup(function(e) {
      if ((e.keyCode == 46 || e.keyCode == 8) && $(this).data("clear-allowed")) {
        $.datepicker._clearDate($(this));
      }
    });

    localizeCalendarDates();
  });
}(window.jQuery, window.Zendesk));
