$z.defModule('rules/index', {
  initialize: function(params) {
    var sortByHtml = function(a, b) {
      a = $j(a).html().strip().toLowerCase();
      b = $j(b).html().strip().toLowerCase();
      return (a < b) ? -1 : ((a > b) ? 1 : 0);
    }

    var setOriginalList = function(sorted) {
      var itemList = [];
      var originalList = $j('#active-rules div.item').not(".reorder");
      $j.each(sorted, function(index, div) {
        var itemTemplate = originalList[index];
        var actions = $j(div).find('div.actions');
        $j(itemTemplate).find('div.title div').html(div.innerHTML);
        if (actions.length > 0) { $j(itemTemplate).find('div.item-actions').replaceWith(actions.html()); }
        itemList[index] = itemTemplate;
      });
      originalList.remove();
      $j("#active-rules").prepend(itemList);
    };

    var sortRules = function(direction) {
      var sortedRules = $j("ul#active-rules-sort-list li.item.sortable").sort(function(a, b) {
        if(direction == "desc") return sortByHtml(b, a);
        return sortByHtml(a, b);
      });
      setOriginalList(sortedRules);
      $j("ul#active-rules-sort-list li.item.sortable").remove();
      $j("ul#active-rules-sort-list").prepend(sortedRules);
    }

    $j("div#active-rules_sort input.sort_asc").click(function(event) {
      sortRules("asc");
    });

    $j("div#active-rules_sort input.sort_desc").click(function(event) {
      sortRules("desc");
    });

    $j("a.cancel-sorting").click(function(event) {
      Ordering.cancelOrdering("div#active-rules");
      setOriginalList($j("ul#active-rules-sort-list li.item.sortable"));
    });

    $j("select#rule-select, select#rule-sort").change(function(event) {
      var sortSelected = $j("select#rule-sort option:selected").val();
      var groupSelected = $j("select#rule-select option:selected").val();

      var query  = '/rules?';
      var queryParams = jQuery.queryParameters();
      var rawParams = { filter: queryParams.filter };
      if(typeof(sortSelected) != "undefined") { rawParams.sort = sortSelected; }
      if(typeof(groupSelected) != "undefined"){ rawParams.select = groupSelected; }

      window.location.href = $j(location).attr('protocol') + '//' + $j(location).attr('host') + query + $j.param(rawParams);
    });
  }
});
