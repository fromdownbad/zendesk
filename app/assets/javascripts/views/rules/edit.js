$z.defModule('rules/edit', {
  initialize: function(){
    $j('fieldset.conditions span select').autocompleteFromSelectIfLarge();

    function confirmDangerousAction(event) {
      var confirmMessage;

      switch($j('#submit_type').val()) {
        case 'deactivate':
          confirmMessage = I18n.t('txt.admin.views.rules.confirm_deactivate');
          break;
        case 'delete':
          confirmMessage = I18n.t('txt.admin.views.rules.confirm_delete');
          break;
        default:
          return true;
      }

      var $form = $j(this);

      if ($form.data('is-recently-confirmed')) {
        $form.data('is-recently-confirmed', null);
        return true;
      } else {
        // the following is to prevent instrumentation from submitting the form in Firefox
        event.stopImmediatePropagation();
        event.preventDefault();
        if (confirm(confirmMessage)) {
          $form.data('is-recently-confirmed', true)
            .trigger('submit');
        }
        return false;
      }
    }

    function verifyColumnsForViews() {
      var columns = $$('ul#result_columns li').pluck('id');

      if (columns.length > 1) {
        $('output_columns').value = columns.join(',').gsub('sort_', '');
        return true;
      } else {
        alert('Please select at least two columns to include in table');
        return false;
      }
    }

    var $form = $j('#viewform');

    switch($form.data('ruleType')) {
      case 'View':
        $form.submit(verifyColumnsForViews);
        break;
      case 'Automation':
      case 'Trigger':
        $form.submit(confirmDangerousAction);
    }
  }
});
