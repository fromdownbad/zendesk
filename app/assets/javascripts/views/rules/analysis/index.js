// This file should be deleted as part of cleanup of arturo use_get_for_rules_analysis_show
$j(document).ready(function(){
  $j("a.nube1").click(function(){
    // we need a form submit to make the url params safe from content spoofing
    var tempForm = jQuery('<form>', {
      'action': $j(this).attr('href'),
      'method': 'post',
      'accept-charset': 'UTF-8'
    }).append(jQuery('<input>', {
      'name': 'utf8',
      'value': '✓',
      'type': 'hidden'
    })).append(jQuery('<input>', {
      'name': 'authenticity_token',
      'value': $j('meta[name="csrf-token"]').attr('content'),
      'type': 'hidden'
    }));

    if ($j(this).data('key') !== undefined) {
      tempForm.append(jQuery('<input>', {
        'name': 'key',
        'value': $j(this).data('key'),
        'type': 'hidden'
      }));
    }

    $j('body').append(tempForm);
    tempForm.submit();

    return false;
  });
});

