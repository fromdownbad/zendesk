$j(document).ready(function(){
  $j("select#analysis-filter").change(function(event) {
    // we need a form submit to make the url params safe from content spoofing
    var tempForm = jQuery('<form>', {
      'action': $j(location).attr('pathname'),
      'method': 'post',
      'accept-charset': 'UTF-8'
    }).append(jQuery('<input>', {
      'name': 'utf8',
      'value': '✓',
      'type': 'hidden'
    })).append(jQuery('<input>', {
      'name': 'filter',
      'value': $j("select#analysis-filter option:selected").val(),
      'type': 'hidden'
    })).append(jQuery('<input>', {
      'name': 'authenticity_token',
      'value': $j('meta[name="csrf-token"]').attr('content'),
      'type': 'hidden'
    }));

    if ($j(this).data("select") !== undefined) {
      tempForm.append(jQuery('<input>', {
        'name': 'select',
        'value':  $j(this).data("select"),
        'type': 'hidden'
      }));
    }

    if ($j(this).data("key") !== undefined) {
      tempForm.append(jQuery('<input>', {
        'name': 'key',
        'value':  $j(this).data("key"),
        'type': 'hidden'
      }));
    }

    $j('body').append(tempForm);
    tempForm.submit();

    return false;
    });

  $j("div#rules_analysis_tab_links a").click(function(){
    // we need a form submit to make the url params safe from content spoofing
    var tempForm = jQuery('<form>', {
      'action': $j(location).attr('pathname'),
      'method': 'post',
      'accept-charset': 'UTF-8'
    }).append(jQuery('<input>', {
      'name': 'utf8',
      'value': '✓',
      'type': 'hidden'
    })).append(jQuery('<input>', {
      'name': 'select',
      'value': $j(this).data('select'),
      'type': 'hidden'
    })).append(jQuery('<input>', {
      'name': 'authenticity_token',
      'value': $j('meta[name="csrf-token"]').attr('content'),
      'type': 'hidden'
    }));

    if ($j(this).data("key") !== undefined) {
      tempForm.append(jQuery('<input>', {
        'name': 'key',
        'value':  $j(this).data("key"),
        'type': 'hidden'
      }));
    }

    $j('body').append(tempForm);
    tempForm.submit();

    return false;
  });
});
