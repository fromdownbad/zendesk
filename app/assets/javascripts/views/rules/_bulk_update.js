$z.defModule('rules/_bulk_update', {
  initialize: function() {
    var bulkForm = $j('#ticket-chat');

    bulkForm.submit(function(e) {
      $j.ajax({
        url:  bulkForm.attr('action'),
        type: 'POST',
        data: bulkForm.serialize() + '&background=true',
        dataType: 'json',
        success: function(job) {
          var jobStatus = new Zendesk.JobStatus(job.status_url, {
            title: I18n.t('txt.public.javascripts.views.rules._bulk_update.processing_tickets'),
            renderResult: function(jobStatus) {
              var output = {
                title: I18n.t('txt.public.javascripts.views.rules._bulk_update.tickets_processed', {count:jobStatus.results.length}),
                body: $j('<ul/>')
              };

              if (jobStatus.status === 'completed') {
                document.location = '/tickets/bulk_result?background=' + jobStatus.job.id + '&return_to=' + escape(document.location);
              }
              return output;
            }
          });
        }
      });

      $j('#bulk-update').hide();
      $j('table.tickets th.checkbox input').remove();
      $j('table.tickets input.tickets_to_bulk_update').remove();

      return false;
    });

  }
});
