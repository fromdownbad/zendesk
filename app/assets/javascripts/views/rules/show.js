$z.defModule('rules/show', {
  initialize: function(){
    var selector = "textarea[id*='ticket_fields'], input[id*='ticket_fields']";
    var defaultColor = $j(selector).css("color");
    $j(selector).css("color", "gray");
    $j(selector).click(function() {
      $j(this).css("color", defaultColor);
    });
    $j(selector).blur(function() {
      if ($j(this).val() == I18n.t('control.dropdown.value.no_change')) {
        $j(this).css("color", "gray");
      }
    });

    var subjectTh = $j("th:contains('Subject')");
    if (subjectTh.width() > 50) {
      subjectTh.css('width', subjectTh.width());
    } else {
      subjectTh.css('width', '50px');
    }

    var comment_id = "textarea#comment_value";
    var placeholder_warning_id = "p#placeholders_in_comment_notice";

    $(document).observe('macro:applied', function() {
      if (($j(comment_id).val().indexOf('{{') > -1) || ($j(comment_id).val().indexOf('{%') > -1)) {
        $j(placeholder_warning_id).show();
      }
      else {
        $j(placeholder_warning_id).hide();
      }
    });

  }
});

