/*globals jQuery*/
jQuery(function($) {

  var lists = $('#view_output_columns').find('.sortablelist');

  if (!lists.exists()) { return; }

  lists.each(function() {
    $(this).sortableWithEmptyTarget({
      emptyTargetClass: 'item sortable',
      placeholder:      'sortable target',
      connectWith:      lists.not(this)
    });
  });

  // if #result_columns reach 10 items, it stops accepting other items
  $j("#result_columns").bind('sortreceive', function(event, ui) {
    if ( $j(this).find('li').size() > 11 ) {
      $j(ui.sender).sortable('cancel');
    }
  });

});
