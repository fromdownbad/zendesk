Zendesk.NS.extend('Satisfaction', {
  Score: {
    offered: 1,
    poor:    4,
    good:    16
  },

  ScoreValue: {
    '1':  'offered',
    '4':  'poor',
    '5':  'poor',
    '16': 'good',
    '17': 'good'
  },

  Input: {
    init: function() {
      if ($j(this._rootSelector).length) {
        this._satisfactionButtons = $j('#satisfaction_rating form .rating');
        this._currentRatingContainer = $j('#current_rating');
        this._currentRating = this._parseCurrentRating();
        this._ratingTemplate = $j('#current_rating_template').html();
        this._form = $j('#satisfaction_rating form');
        this._reasonCode = $j('#satisfaction_reason_code');
        this._setHashFromIntention();
        this._app = this._buildSammyApplication(jQuery, this._rootSelector);
      }
    },

    _scoreSpanTemplate: '<span class="rating selected {{scoreKey}}">{{scoreText}}</span>',

    _ensureVisible: function() {
      if ($j(this._rootSelector).is(':hidden')) {
        if ((this._currentRating && this._currentRating.score > 1)) {
          if ($j('#satisfaction_rating').data('csr-token-retention') && this._currentRating['can_modify?']) {
            this._hideCurrentRatingAndShowForm();
          } else {
            this._form.hide();
            this._renderRating(this._currentRating).show();
          }
        } else {
          this._hideCurrentRatingAndShowForm();
        }
        $j(this._rootSelector).show();
      }
    },

    _hideCurrentRatingAndShowForm: function() {
      this._currentRatingContainer.hide();
      this._form.show();
    },

    _parseCurrentRating: function() {
      var encodedHTML = $j('#current_rating_data').html();
      var decodedHTML = $j('<div />').html(encodedHTML).text();
      return JSON.parse(decodedHTML);
    },

    _renderRating: function(rating) {
      // turn a 0, 1, etc. into "good", "poor", etc.:
      var scoreKey;
      if(rating.updated_score !== null && rating.updated_score !== undefined) {
        scoreKey = Zendesk.Satisfaction.ScoreValue[rating.updated_score];
      } else {
        scoreKey = Zendesk.Satisfaction.ScoreValue[rating.score];
      }

      // build the colored span:
      var scoreText = $j.mustache(this._scoreSpanTemplate, {
        scoreKey:   scoreKey,
        scoreText:  I18n.t('txt.satisfaction.score.' + scoreKey)
      });

      // build the whole box:
      var presenter = {
        header:                 I18n.t('txt.satisfaction.score.current', { score: scoreText } ),
        commentHeader:          I18n.t('txt.satisfaction.header.comment'),
        showComment:            I18n.t('txt.satisfaction.comment.show'),
        hideComment:            I18n.t('txt.satisfaction.comment.hide'),
        modifyRating:           I18n.t('txt.satisfaction.rating.modify'),
        anonymousModifyRating:  I18n.t('txt.satisfaction.rating.anonymous_modify'),
        comment:                rating.comment,
        canModify:              rating['can_modify?'],
        isAnonymous:            currentUser.isAnonymous
      };
      return this._currentRatingContainer
              .html($j.mustache(this._ratingTemplate, presenter));
    },

    _rootSelector: '#satisfaction_rating',

    _setHashFromIntention: function(){
      var intention = this._currentRatingContainer.data("intention");
      var score = Zendesk.Satisfaction.ScoreValue[intention];
      var noHashGiven = window.location.hash.replace("#", "") === "";
      if (score && noHashGiven) window.location.hash = "#/satisfaction/new/" + score;
    },

    _buildSammyApplication: function($, root) {
      var app = $.sammy(root, function() {
        this.debug = true;

        this.before(function() {
          Zendesk.Satisfaction.Input._ensureVisible();
          return true;
        });

        // Page Load
        this.get('#/satisfaction', function() {
        });

        // Submit Rating
        this.post(/^\/requests\/.+\/satisfaction/, function(context) {
          var form = Zendesk.Satisfaction.Input._form;
          form.find('input[type="submit"]').prop('disabled', true);
          jQuery.ajax({
            url:      form.attr('action'),
            type:     form.attr('method').toUpperCase(),
            data:     form.serialize(),
            dataType: 'json',
            error:    function(request, textStatus, errorThrown) {
              context.redirect('#/satisfaction/error');
            },
            success:  function(data, textStatus, XMLHttpRequest) {
              Zendesk.Satisfaction.Input._currentRating = data;
              context.trigger('ticket.satisfaction.created', data);
              context.redirect('#/satisfaction/success');
            }
          });
        });

        this.get('#/satisfaction/success', function() {
          Zendesk.Satisfaction.Input._form.hide();
          Zendesk.Satisfaction.Input
            ._renderRating(Zendesk.Satisfaction.Input._currentRating)
            .show();
          this.redirect('#/satisfaction');
        });

        // Failed Save
        this.get('#/satisfaction/error', function() {
        });

        // Cancel Rating
        this.get('#/satisfaction/cancel', function() {
          Zendesk.Satisfaction.Input
            ._satisfactionButtons
            .removeClass('selected');

          if (Zendesk.Satisfaction.Input._currentRating) {
            Zendesk.Satisfaction.Input._form.hide();
            Zendesk.Satisfaction.Input._currentRatingContainer.show();
          } else {
            $j('#satisfaction_form_body')
              .slideUp(500);
          }
        });

        // Hide Comment
        this.get('#/satisfaction/comment/hide', function() {
          Zendesk.Satisfaction.Input._currentRatingContainer
            .find('.comment').slideUp().end()
            .find('a.hide_comment').hide().end()
            .find('a.show_comment').show();
        });

        // Show Comment
        this.get('#/satisfaction/comment/show', function() {
          Zendesk.Satisfaction.Input._currentRatingContainer
            .find('.comment').slideDown().end()
            .find('a.show_comment').hide().end()
            .find('a.hide_comment').show();
        });

        this.before('#/satisfaction/new', function() {
          Zendesk.Satisfaction.Input._currentRatingContainer.hide();
          Zendesk.Satisfaction.Input._form.show();
        });

        // Re-Rate
        this.get('#/satisfaction/new', function() {
          var form = Zendesk.Satisfaction.Input._form;
          form.find('input[type="submit"]').prop('disabled', false);
        });

        this.before('#/satisfaction/new/poor', function() {
          Zendesk.Satisfaction.Input._reasonCode.show();
        });

        this.before('#/satisfaction/new/good', function() {
          Zendesk.Satisfaction.Input._reasonCode.hide();
        });

        // Rate Good/Poor
        // @see SatisfactionRatingDrop#rating_url_hash
        this.get('#/satisfaction/new/:ratingName', function() {
          var ratingName = this.params.ratingName;
          var selected = Zendesk.Satisfaction.Input._satisfactionButtons.filter('.' + ratingName);
          if (!selected.hasClass('selected')) {
            Zendesk.Satisfaction.Input
              ._satisfactionButtons.not(selected)
              .removeClass('selected');
            selected.addClass('selected');
            $j('#satisfaction_form_body:hidden')
              .slideDown(500);
            $j('#ticket_satisfaction_score').val(Zendesk.Satisfaction.Score[ratingName]);
          }
        });
      });
      app.run('#/satisfaction');
      return app;
    }
  }
});

jQuery(function() {
  Zendesk.Satisfaction.Input.init();
});
