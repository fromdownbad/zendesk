/*global $j, $z, _, JSON, Zendesk */
/*jshint lastsemic: true, expr: true, whitespace: true*/

// http://archive.plugins.jquery.com/plugin-tags/bounding-absolute-utility
// extended by us with show, bounds, functions, margin and padding
(function($) {
  function showBounds(bounds) {
    var div = $('<div/>').css({
      position: 'absolute',
      left: bounds.left + 'px',
      top: bounds.top + 'px',
      width: (bounds.right - bounds.left) + 'px',
      height: (bounds.bottom - bounds.top) + 'px',
      outline: 'red 1px solid'
    });
    $('body').append(div);
    window.setTimeout(function() { div.remove(); }, 1000);
  }

  $.fn.bounds = function(withPadding, withMargin) {

    withPadding = withPadding || withMargin;

    var bounds = {
      left: Number.POSITIVE_INFINITY,
      top: Number.POSITIVE_INFINITY,
      right: Number.NEGATIVE_INFINITY,
      bottom: Number.NEGATIVE_INFINITY,
      width: function() { return this.right - this.left; },
      height: function() { return this.bottom - this.top; },
      show: function() { showBounds(this); return this; },
      toString: function() { return JSON.stringify(this); }
    };

    this.each(function (i,el) {
      var elQ = $(el),
          off = elQ.offset();
      if (withPadding) {
        off.right = off.left + $(elQ).outerWidth(withMargin);
        off.bottom = off.top + $(elQ).outerHeight(withMargin);
      } else {
        off.right = off.left + $(elQ).width();
        off.bottom = off.top + $(elQ).height();
      }

      if (off.left < bounds.left)
        bounds.left = off.left;

      if (off.top < bounds.top)
        bounds.top = off.top;

      if (off.right > bounds.right)
        bounds.right = off.right;

      if (off.bottom > bounds.bottom)
        bounds.bottom = off.bottom;
    });
    return bounds;
  };

})($j);

(function($) {
  $.fn.filterOrFind = function(selector) {
    var $this = $(this);
    return $this.find(selector).add($this.filter(selector));
  };
})($j);


(function() {

  var forumsSearch = Zendesk.NS('Forums.Search');

  $z.defModule("forums/_search", {

    // ---- intializing ----
    initialize: function(params) {
      params = params || {};
      params.forumsSearch = true;
      forumsSearch.Instant.setup(params);
      forumsSearch.Autocomplete.setup(params);
    }

  });
})();
