$z.defModule("forums/form", {
  initialize: function(params) {
    $j("input#forum_visibility_restriction_id_1, input#forum_visibility_restriction_id_2").click(function() {
      $j("#forum_is_locked_false").prop("disabled", false);

      $j(".forum-language-selector").show("slow");
    });

    $j("input#forum_visibility_restriction_id_3").click(function() {
      $j("#forum_is_locked_false").prop("disabled", true);
      $j("#forum_is_locked_true").prop("checked", true);

      $j(".forum-language-selector").hide();
      $j("#forum_translation_locale_id").val("");
    });

    $j('form.edit_forum').submit(function() {
      if(typeof(forumTagField) != 'undefined') {
        forumTagField.beforeFormSubmit();
      }
    });

  }
});
