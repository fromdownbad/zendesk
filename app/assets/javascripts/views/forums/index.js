/*globals $j, Zendesk, $z, categoryTopRightEdit, makeCategorizedForumsDraggable, addCategoryReordering, categoryDescription */
// This same module is initialized as both forums/forums1_index and forums/forums2_index:
(function() {

  // sets class="active" to the nav link you want
  function setNav(active) {
    // navigating away from stats tab should deactivate any sparklines
    if (active != "stats")
      $j(".sparkline").removeClass("active");

    $j(".forum-nav a.active").removeClass("active");

    if (active) {
      $j("#forum_nav_" + active).addClass("active");
    } else {
      $j(".forum-nav a:first").addClass("active");
    }
  }

  function setUpCategoryHeaders() {
    if ($j(".category-header").attr("id")) {
      $j(".category-header").map(function(x, el) {
        categoryTopRightEdit($j(el).attr("id"));
        makeCategorizedForumsDraggable($j(el).parent().find(".category").attr("id"));
      });

      $j(".buttons-right .reorder").show();
      addCategoryReordering();

      categoryDescription();
      $j(".category-header").show();
    } else {
      return false;
    }
  }

  // simply fetches content at .attr("url") of a particular section
  function fetchContent(section) {
    $j(".buttons-right .reorder").hide();
    $j("#detailed_stats_graph_container").hide();
    $j("#content_entries .frame:first").html('<div id="topic_search_loading" class="loading"></div>');
    $j.ajax({
      url: $j("#forum_nav_" + section).attr("url"),
      data: { xhr: true }
    }).done(function(body) {
      $j("#content_entries").html(body);
      $j('#search-result').paginateResults();
      $j(".forum_tabs").trigger("events-loaded.entries.zendesk");

      if (section == "overview")
        setUpCategoryHeaders();
    });
    setNav(section);
  }

  function showCommentSection() {
    $j("#comments_section").show();
    setNav("comments");
  }

  var forum_module = {
    initialize: function(params) {
      params = params || {};
      params.scopeSammyTo = params.scopeSammyTo || ".forum_tabs";

      $j(".category li span").truncateViaFade();

      this.sammy_app = $j.sammy(params.scopeSammyTo, function() {

        //--------------------------
        // Routes
        //--------------------------

        // particular stat
        this.get("#stats/:statName", function() {
          $j(".buttons-right .reorder").hide();
          $j("#comments_section").hide();
          var statName = this.params.statName;
          Zendesk.Stats.ForumUI.showDetailGraph(".sparkline#" + statName);
          setNav("stats");
        });

        // top-level stats
        this.get("#stats", function() {
          $j(".buttons-right .reorder").hide();
          $j("#comments_section").hide();
          Zendesk.Stats.ForumUI.showDetailGraph(".sparkline:first");
          setNav("stats");
        });

        this.get("#overview", function() {
          fetchContent("overview");
        });

        this.get("#recent", function() {
          fetchContent("recent");
        });

        this.get("#popular", function() {
          fetchContent("popular");
        });

        this.get("#unanswered", function() {
          fetchContent("unanswered");
        });

        this.get("#answered", function() {
          fetchContent("answered");
        });

        this.get("#planned", function() {
          fetchContent("planned");
        });

        this.get("#not_planned", function() {
          fetchContent("not_planned");
        });

        this.get("#done", function() {
          fetchContent("done");
        });

        this.get("#comments", function() {
          showCommentSection();
        });

        this.get("", function() {
          $j("#detailed_stats_graph_container").hide();
          // activate first link in forum nav
          var firstTab = $j('.forum-nav a:first').attr('href');
          if (firstTab) {
            if (firstTab == "#comments") {
              showCommentSection();
              return false;
            }

            setNav(firstTab.replace("#", ""));
            setUpCategoryHeaders();
            $j(".forum_tabs").trigger("events-loaded.entries.zendesk");
          }
        });

      });

      Zendesk.Stats.ForumUI.setup(this.sammy_app);
      this.sammy_app.disable_push_state = true;
      this.sammy_app.run();
    }
  };

  $z.defModule("forums/index", forum_module);
  $z.defModule("forums/show", forum_module);
  $z.defModule("forums/forums1_index", forum_module);
  $z.defModule("forums/forums2_index", forum_module);
}());
