$z.defModule('archived_tickets/index', {
  initialize: function(){
    this.updateUI();
    $j('select#filter_by').change(this.updateUI);
  },

  updateUI: function() {
    ['requester', 'organization'].each(function(item) {
      if (item == $j('select#filter_by').val()) {
        $j('#' + item).show();
      } else {
        $j('#' + item).hide();
      };
    });
  }
});