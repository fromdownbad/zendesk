$z.defModule('people/organizations/edit', {
  initialize: function(organizationId) {
    $j('form#account-settings').submit(function() {
      if(typeof(organizationTagField) != 'undefined') {
        organizationTagField.beforeFormSubmit();
      }
    });
  }
});
