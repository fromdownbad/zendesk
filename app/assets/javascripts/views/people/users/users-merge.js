$z.defModule('people/users/merge', {
  initialize: function(){
    /* This part is sadly never reached for lightboxes... */
  },

  switch_to_password_form : function(){
    $j("#user_merge_email_form").hide();
    $j("#user_merge_password_form").show();
    $j.colorbox.resize();
    $j("#user_merge_email_for_password_form").val($j("#user_merge_email").val());
    $j("#user_merge_password_email_display").html($j("#user_merge_email").val());
  },

  on_email_submit : function(){
    if (this.email_validate($j("#user_merge_email").val())) {
      this.toggle_form('email');
      return false;
    } else {
      alert(I18n.t('txt.user.merge.enter_valid_email'));
      return true;
    }
  },

  on_password_submit : function(){
    if ($j("#user_merge_password").val() !== '') {
      this.toggle_form('password');
      return false;
    } else {
      alert(I18n.t('txt.user.merge.enter_valid_password'));
      return true;
    }
  },

  email_validate : function (address) {
    var reg = /^([A-Za-z0-9_\+\-\.])+(\+[0-9]+)?\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return reg.test(address);
  },

  password_does_not_match : function (text) {
    this.toggle_form('password');
    alert(text);
  },

  toggle_form : function (type) {
    $j("#user_merge_waiting_for_" + type).toggle();
    $j("#user_merge_submit_" + type).toggle();
  }

});
