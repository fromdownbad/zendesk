/*global $z, $j, clearFlash, showFlash, _*/

$z.defModule('people/search/index', {
  initialize: function() {

    if ($j('#bulk_update').exists()) {
      this.setupBulkUpdate();
    }

    if($j('#seat-type-filter').exists()) {
      this.setupSeatTypeFilter();
    }
  },

  setupSeatTypeFilter: function() {
    $j('#seat-type-filter select').change(function() {
      window.location.href = window.location.search.replace(/\bpage=\d+/ig,'page=1') + '&seat_type=' + $j(this).val();
    });
  },

  setupBulkUpdate: function() {

    var allCheckboxContent = $j('.individual_bulk_checkbox');

    this.activeCheckboxes  = $j(_(allCheckboxContent.find('.checkbox')).select(function(checkbox) {
                               return !$j(checkbox).prop('disabled');
                             }));

    if (this.activeCheckboxes.length < 1) {
      $j('#bulk_update').hide();
      allCheckboxContent.hide();
    }
    else {

      this.bulkSubmitElm = $j('#bulk_submit');

      this.toggleBulkSubmit();
      $j('#bulk_check').change($j.proxy(this.manageBulkUpdate, this));
      this.activeCheckboxes.change($j.proxy(this.toggleBulkSubmit, this));
      $j('#bulk_submit').click($j.proxy(this.submitBulkForm, this));
    }

  },


  manageBulkUpdate: function(e) {

    var bulkCheckSelected = $j('#bulk_check').prop('checked');

    this.activeCheckboxes.not('.disabled').prop('checked', bulkCheckSelected);
    this.toggleBulkSubmit();
  },


  submitBulkForm: function(e) {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    if (!this.anyChecked()) {
      return;
    }

    $j('#bulk_check').hide();
    $j('#bulk_spinner').show();

    $j.ajax( {
      type: 'POST',
      url:  '/users/select_bulk_action',
      data: ($j('#bulk_form').serialize()),

      success: function(response) {
        clearFlash();
        $j.colorbox({ html: response });
        $j('#bulk_spinner').hide();
        $j('#bulk_check').show();
      },

      error: function(response) {
        showFlash("Error:" + response, 'error');
        $j('#bulk_spinner').hide();
        $j('#bulk_check').show();
      }
    });

  },


  toggleBulkSubmit: function(e) {

    if (this.anyChecked()) {

      this.bulkSubmitElm.prop('disabled', false);
      this.bulkSubmitElm.removeClass('button_disabled');
      this.bulkSubmitElm.addClass('button');

    }
    else {

      this.bulkSubmitElm.prop('disabled', true);
      this.bulkSubmitElm.removeClass('button');
      this.bulkSubmitElm.addClass('button_disabled');

    }
  },


  anyChecked: function() {

    return _(this.activeCheckboxes).any(function(checkbox) {

      return $j(checkbox).prop('checked');

    });

  }
});
