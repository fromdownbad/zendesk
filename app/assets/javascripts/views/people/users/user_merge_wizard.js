function UserMergeWizard(redirectToWinner) {
  this.redirectToWinner = redirectToWinner;
  this.registerEvents();
}

UserMergeWizard.prototype = {
  redirectToWinner: false,
  lookupUser: function(loser, searchTerm, callback) {
    new Ajax.Request('/users/' + loser + '/merge/autocomplete', {
      method: "GET",
      parameters: {name: searchTerm, rand: (new Date()).getTime()},
      onSuccess: function(response) {
        callback(response.responseJSON);
      }
    });
  },

  registerEvents: function() {
    var self = this;

    $j('#merge_link').live('click', function() {
      $j.colorbox({href: $j(this).attr('href'),
                   onComplete: function() {
                     var loser = self.winnerForm().attr("data-loser");
                     var cache = new Autocompleter.Cache(self.lookupUser.curry(loser));
                     new Autocompleter.Json("winner", "winner_auto_complete", cache.lookup.bind(cache), {frequency: 0.1, minChars: 3});
                     $j('input[placeholder]').placeholder();
                   }});
      return false;
    });

    self.winnerLinks().live('click', function() {
      $j.colorbox({href: $j(this).attr('href')});
      return false;
    });

    self.winnerForm().live('submit', function() {
      $j.ajax({
        type: $j(this).attr('method'),
        data: $j(this).serialize(),
        url: $j(this).attr('action'),
        success: function(response) {
          $j.colorbox({html: response});
        },
        error: function(request, textStatus, errorThrown) {
          $j.colorbox({html: request.responseText});
        }
      });
      return false;
    });

    self.confirmForm().live('submit', function() {
      var button = self.confirmButton();
      button.val(I18n.t('txt.user.merge.merging_users'));
      button.prop("disabled", true);

      var form = self.confirmForm();
      var accountId = form.data('account-id');
      var winnerId = form.data('winner-id');
      var loserId = form.data('loser-id');
      var radarKeyName = "user_merge/" + accountId + "/" + winnerId + "/" + loserId;
      // user_merge_job notifies radar when merge completed successfully or unsuccessfully
      RadarClient.initialize(function(){
        RadarClient.alloc("user_merge",function() {
          RadarClient.status(radarKeyName).on(function(msg) {
            var newForm;
            // we need a form submit to make the url params safe from content spoofing
            if(msg.value === true) {
                newForm = jQuery('<form>', {
                    'action': '/users/' + winnerId + '/merge_complete',
                    'method': 'post',
                    'accept-charset': 'UTF-8'
                }).append(jQuery('<input>', {
                    'name': 'merge_status',
                    'value': 1,
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'utf8',
                    'value': '✓',
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'loser',
                    'value': form.data('loser-name'),
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'authenticity_token',
                    'value': $j('meta[name="csrf-token"]').attr('content'),
                    'type': 'hidden'
                }));
            } else { // accept-charset
                newForm = jQuery('<form>', {
                    'action': '/users/' + loserId + '/merge_complete',
                    'method': 'post',
                    'accept-charset': 'UTF-8'
                }).append(jQuery('<input>', {
                    'name': 'merge_status',
                    'value': 0,
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'utf8',
                    'value': '✓',
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'authenticity_token',
                    'value': $j('meta[name="csrf-token"]').attr('content'),
                    'type': 'hidden'
                }));
            }
            newForm.appendTo("body").submit();

          }).subscribe(function(){

            $j.ajax({
              data: form.serialize(),
              type: "POST",
              url: form.attr('action') + "?format=js",
              error: function(request, textStatus, errorThrown) {
                $j.colorbox({html: request.responseText});
                button.val(I18n.t('txt.user.merge.confirm_and_merge'));
                button.prop("disabled", false);
              }
            });

          });
        });
      });

      return false;
    });

  },

  winnerForm: function() {
    return $j("#winner_form");
  },

  winnerField: function() {
    return this.winnerForm().find("input[type='text']");
  },

  continueButton: function() {
    return this.winnerForm().find("input[type='submit']");
  },

  winnerLinks: function() {
    return $j(".winner_selector");
  },

  confirmForm: function() {
    return $j("#confirm_merge_form");
  },

  confirmButton: function() {
    return this.confirmForm().find("input[type='submit']");
  }
};
