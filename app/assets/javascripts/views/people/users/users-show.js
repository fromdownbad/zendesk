$z.defModule('people/users/show', {
  initialize: function(userId) {
    this.userId        = userId;

    var suspendUserElm = $j('#suspend_access');
    this.userSuspended = (suspendUserElm.attr('data-user-suspended') !== 'false');

    if (this.userSuspended) {
      $j('#suspended').show();
    }

    $j('a.toggle-twitter-data').click(function() {
      $j(this).closest("p.user-twitter").next('.twitter-properties').slideToggle(50);
    });

    $j(".visibility-controls").click(function() {
      $j("a.toggle-twitter-data", this).toggle();
    });

    $j("#make_direct_number_link").click(function(e) {
      $j.colorbox({inline: true, href: "#make_direct_number_layer"});
      if(e) {e.preventDefault();}
    });

    $j("#edit_number_link").click(function(e) {
      $j.colorbox({inline: true, href: "#edit_number_layer"});
      if(e) {e.preventDefault();}
    });

    new UserMergeWizard(true);

    this.setSuspensionActionText();
    suspendUserElm.click( $j.proxy(this.setSuspensionStatus, this) );
  },


  setSuspensionStatus: function(e) {
    e.preventDefault();

    if (this.settingStatus === true) {
      return;
    }
    this.settingSuspensionStatus = true;

    var suspendAccessUrl = '/users/' + this.userId;

    $j.ajax({
      url: suspendAccessUrl,
      type: 'PUT',
      dataType: "json",
      dataFilter: function(data) {
          trimmed_data = $j.trim(data)
          if (trimmed_data == "") {
              return "{}";
          }else {
              return data;
          }
      },

      data: {
        authenticity_token: currentUser.authenticityToken,
        user: { suspended: !this.userSuspended }
      },

      success: $j.proxy(function() {
        this.userSuspended = !this.userSuspended;

        this.setSuspensionActionText();
        $j('#suspended').toggle(this.userSuspended);
        showFlash((this.userSuspended ? I18n.t('txt.admin.javascrips.users_show.user_suspended_label') : I18n.t('txt.admin.javascrips.users_show.user_unsuspended_label')), 'notice');
        this.settingSuspensionStatus = false;
      }, this),

      error: function(response) {
        var action = this.userSuspended ? 'suspend' : 'unsuspend';
				if (action == 'suspend'){
					message = I18n.t('txt.admin.javascrips.users_show.suspend_user_label');
				}else {
					message = I18n.t('txt.admin.javascrips.users_show.unsuspend_user_label');
				}
        showFlash(message, 'error');
        this.settingSuspensionStatus = false;
      }
    })
  },


  setSuspensionActionText: function() {
    $j('#suspend_access').html( this.userSuspended ? I18n.t('txt.admin.javascrips.users_show.unsusped_access_label') : I18n.t('txt.admin.javascrips.users_show.suspend_access_label'));
  }
});
