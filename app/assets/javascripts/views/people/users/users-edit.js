/*globals $j, I18n, $z, userTagField, clearFlash, showFlash, Zendesk, _ */
$z.defModule('people/users/edit', {
  initialize: function(userId) {
    var self = this;

    this.confirmationMessages = {
      leavingLegacy:   I18n.t('txt.admin.public.javascripts.views.people.users_edit.assigning_new_role'),
      downgradingRole: I18n.t('txt.admin.public.javascripts.views.people.users_edit.downgrading_role')
    };

    var $userVoiceNumber = $j('#user_voice_number'),
        $callButton      = $j('#test_call_button');

    if ($userVoiceNumber.exists()) {

      if ($j.trim($userVoiceNumber.val()) !== '') {
        $callButton.removeAttr("disabled");
      }

      $userVoiceNumber.live('keyup', function(event) {
        if ($j.trim($j(event.currentTarget).val()) === ''){
          $callButton.attr("disabled", "disabled");
        } else {
          $callButton.removeAttr("disabled");
        }
      });
    }

    if($j('#original_roles').exists()) {
      this.setupNonEnterpriseRoles(userId);

      $j('#user_default_group_id').live('change', function() {
        var selectedGroupId = $j(this).val();
        $j('#user_groups_' + selectedGroupId).prop('checked', true);
      });
    } else {
      // What a mess.
      $j('#user_default_group_id').change(function() {
        self.enableGroup($j("#group_" + $j(this).val()));
      });
      self.enableGroup($j("#group_" + $j('#user_default_group_id').val()));
    }

    var roleGroupsForm  = $j('#role_and_groups_form');
    this.formElm = roleGroupsForm.exists() ? roleGroupsForm : $j('#user-form');

    // Role & Groups Management for new Permission Set roles
    if ($j('.role_groups').exists()) {

      $j('.user_type, #user_role_or_permission_set').change( $j.proxy(this.manageRoleDisplay, this) );
      this.manageRoleDisplay();

      $j('#group_tiles .tile').click(function(evt) {
        var clickedGroupElm = $j(evt.target).hasClass('tile') ? $j(evt.target) : $j(evt.target).parents('.tile');
        self.selectGroup(clickedGroupElm);
      });

      this.formElm.submit($j.proxy(this.confirmSaveForEnterprise, this));
    } else {
      this.formElm.submit({ userId: userId }, $j.proxy(this.confirmSave, this));
    }

    $j("form#user-form [type=submit]").bind("click", function(e){
      if(typeof(userTagField) != 'undefined') {
        userTagField.beforeFormSubmit();
      }
    });

    // Identities Management
    if ($j('#identities').exists()) {

      $j('.email.unverified_email .add_identity, .twitter .add_identity, .google .add_identity, .facebook .add_identity')
        .colorbox({onComplete: function() { $j('#focus').focus(); }});

      this.identitiesManager = new Zendesk.IdentitiesManager(userId, $j.proxy(this.setupPrimary, this));
      this.renderIdentities(userId);
    }

    // Tests Agent Forwarding Number
    // possible states of call: queued, ringing, in-progress, completed, failed, busy or no-answer.
    // test_call_status will return 'retry' when twilios server has updated its server with the most recent calls
    $j('#status_spinner').addClass('spinner small').hide();
    $j('#test_call_button').click(function(event) {
      event.preventDefault();
      $j('#test_call_status').removeClass('test_call_error');
      if ($j('#test_call_button').val() === I18n.t('txt.admin.views.people.users.basic_info.test_call')){
        var number = $j('#user_voice_number').val();
        $j('#test_call_button').val(I18n.t('txt.admin.assets.javascripts.views.people.users.users-edit.calling') + '...');
        $j('#test_call_status').html(I18n.t('txt.admin.assets.javascripts.views.people.users.users-edit.dialing', {number: number}));
        var data = self.prepCallData(number, $j('#user_voice_extension').val());

        $j('#status_spinner').fadeIn('slow');
        var call_sid = $j.post('/api/v2/channels/voice/agent_verification/call', data, function(sid) {return sid});
        function checkStatus() {
          var status = $j.get('/api/v2/channels/voice/agent_verification/status', {call_sid: call_sid.responseText}, function(current_status){return current_status});
          status.always(function() {
            var state = status.responseText;
            if (state === 'queued' || state === 'ringing' || state === 'retry') {
              setTimeout(checkStatus, 500);
            } else if (state === 'in-progress') {
              $j('#test_call_status').html(I18n.t('txt.admin.assets.javascripts.views.people.users.users-edit.forward_success', {number: number}));
              setTimeout(checkStatus, 500);
            } else if (state === 'completed') {
              $j('#test_call_status').html(I18n.t('txt.admin.assets.javascripts.views.people.users.users-edit.test_success'));
              $j('#test_call_button').val(I18n.t('txt.admin.views.people.users.basic_info.test_call'));
              $j('#status_spinner').fadeOut('slow');
            } else {
              $j('#test_call_status').html(I18n.t('txt.admin.assets.javascripts.views.people.users.users-edit.test_error', {number: number}));
              $j('#test_call_status').addClass('test_call_error');
              $j('#test_call_button').val(I18n.t('txt.admin.views.people.users.basic_info.test_call'));
              $j('#status_spinner').fadeOut('slow');
            }
          });
        }
      };
      call_sid.always(function() {
        setTimeout(checkStatus, 1500);
      });
    });

  },

  prepCallData: function(number, extension) {
    var data    = {number: number},
        pattern = /^\s*(w*\d+#?)\s*$/,
        match   = pattern.exec(extension);

    if (match) {
      data.ext = match[1];
    }
    return data;
  },

  setupNonEnterpriseRoles: function(userId) {
    if ($j('#user_roles_4').is(':checked')) {
      $j('#agent_groups').html($j('#group-block').html());
    }

    if ($j('#user_roles_2').is(':checked')) {
      $j('#admin_groups').html($j('#group-block').html());
    }

    $j('#user-radio').click(function() {
      $j('#agent_groups, #admin_groups').empty();
      $j('#end_user_block').show();
      $j('#agent_block, #admin_groups').hide();
      $j('#user_restriction_id_4' ).click();
      $j('#display_name').hide();
    });

    $j('#user_roles_4').click(function() {
      $j('#agent_groups').html($j('#group-block').html());
      $j('#admin_groups').empty();
      $j('#agent_block').show();
      $j('#admin_groups, #end_user_block').hide();
      $j('#user_restriction_id_0').click();
      $j('#display_name').show();
    });

    $j('#user_roles_2').click(function() {
      $j('#admin_groups').html($j('#group-block').html());
      $j('#agent_groups').empty();
      $j('#agent_block, #end_user_block').hide();
      $j('#admin_groups').show();
      $j('#display_name').show();
    });
  },


  manageRoleDisplay: function(e) {

    this.roleData                = this.roleData || $j.parseJSON($j('#role_data').html());

    var agentRoleRadioElm        = $j('#roles_agent'),
        agentRoleDetailsElm      = $j('#agent_details'),
        agentRoleDescriptionElm  = agentRoleDetailsElm.find('.description'),
        agentRoleHeadcountElm    = agentRoleDetailsElm.find('.headcount'),
        enduserRoleDetailsElm    = $j('#enduser_details'),
        endUserRestrictionElms   = $j('#enduser_details input[name="user[restriction_id]"]'),
        endUserSelected          = $j('#roles_enduser').prop('checked'),
        selectedPermissionSetElm = $j('#user_role_or_permission_set'),
        selectedPermissionSet    = _(this.roleData).find(function(permission_set) { return permission_set.id == selectedPermissionSetElm.val(); });


    selectedPermissionSetElm.prop('disabled', endUserSelected);

    enduserRoleDetailsElm.toggle(endUserSelected);
    agentRoleDetailsElm.toggle(!endUserSelected);

    if (!endUserSelected) {
      // Stops value from being submitted
      endUserRestrictionElms.prop('disabled', true);

      // Dynamically switch roles value based on whether the selected
      // agent role is 'admin' or some permission set
      // In role.rb, 2 is admin & 4 is agent
      agentRoleRadioElm.val( selectedPermissionSet.id == 'admin' ? 2 : 4);

      agentRoleDescriptionElm.html(selectedPermissionSet.description);
      agentRoleHeadcountElm.html(selectedPermissionSet.headcount);
    }
    else {
      endUserRestrictionElms.prop('disabled', false);
    }

    this.displayGroups(endUserSelected);

  },

  displayGroups: function(endUserSelected) {
    $j('#groups_unavailable').toggle(endUserSelected);
    $j('#groups_available').toggle(!endUserSelected);

    if (endUserSelected) {

      $j('#group_tiles input').prop('disabled', true);

    }
    else {

      $j('#group_tiles input.empty').prop('disabled', false);

      _($j('#group_tiles .tile')).each(function(tileElm) {
        tileElm = $j(tileElm);
        if (tileElm.hasClass('selected')) {
          $j(tileElm.find('input')).prop('disabled', false);
        }
      });

    }

  },

  selectGroup: function(selectedGroupElm) {
    if(selectedGroupElm.hasClass('selected')) {
      this.disableGroup(selectedGroupElm);
    } else {
      this.enableGroup(selectedGroupElm);
    }
  },

  enableGroup: function(selectedGroupElm) {
    selectedGroupElm.addClass('selected');
    $j('input', selectedGroupElm).prop('disabled', false);
  },

  disableGroup: function(selectedGroupElm) {
    selectedGroupElm.removeClass('selected');
    $j('input', selectedGroupElm).prop('disabled', true);
  },

  confirmSaveForEnterprise: function(e) {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    var isLegacyAgent          = $j('#user_role_or_permission_set option:first-child').val() === '';
    var legacyAgentNotSelected = $j('#user_role_or_permission_set').val() !== '';
    var agentRoleSelected      = $j('#roles_agent').prop('checked');

    if (isLegacyAgent && agentRoleSelected && legacyAgentNotSelected) {
      if (!confirm(this.confirmationMessages.leavingLegacy)) { return false; }
    } else if (!this.confirmDemotion($j('#roles_enduser').prop('checked'))) { return false; }

    this.formElm.unbind('submit');
    this.formElm.submit();
  },

  confirmSave: function(e) {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    if (!this.confirmDemotion($j('#user-radio').prop('checked'))) { return false; }

    // Track event
    var userType = $j('#user_roles_4').is(':checked') ? 'Agent' :
                     ($j('#user_roles_2').is(':checked') ? 'Admin' : 'End-User');
    Zendesk.Instrumentation.track(userType, 'User - Save');

    $j('#submit-button')
      .val(e.data.userId ? I18n.t('txt.users.edit.updating') : I18n.t('txt.users.edit.creating'))
      .prop('disabled', true);

    this.formElm.unbind('submit');
    this.formElm.submit();
  },

  confirmDemotion: function(toEndUser) {
    var was_agent = this.formElm.data('is-agent');
    if(was_agent && toEndUser) {
      return confirm(this.confirmationMessages.downgradingRole);
    } else {
      return true;
    }
  },

  renderIdentities: function(userId) {

    var removeLink, identityNameElm, primaryLabelElm,
        unverifiedLabelElm, primaryIdentity, primaryIdentityElm;

    var hasPrimaryIdentity = this.identitiesManager.allPrimaryCandidates().length !== 0;

    $j(this.identitiesManager.identities).each($j.proxy(function(i, identity) {
      removeLink          = $j('<a>', { href: 'javascript: void(0);', html: I18n.t('txt.users.edit.delete_identity') })
                              .click($j.proxy(this.deleteIdentity, this))
                              .data('elmId', identity.elmId)
                              .addClass('remove_link');

      identityNameElm     = $j('<span>', { html: identity.name }).addClass('identity_name');

      primaryLabelElm     = $j('<span>', { html: '(' + I18n.t('txt.users.edit.primary') + ')', style: 'display: none'})
                             .addClass('primary_label');

      unverifiedLabelElm  = $j('<span>', { html: '(' + I18n.t('txt.users.edit.email_not_verified') + ')' })
                              .addClass('status unverified');


      $j('<li>', {id: identity.elmId })
        .append(identityNameElm)
        .append(identity.isUnverified() ? unverifiedLabelElm : '')
        .append( (identity.isPrimaryCandidate() && !identity.isUnverified()) ? primaryLabelElm : '')
        .append(removeLink)
        .appendTo( $j(identity.listElmId) );


      if (this.identitiesManager.limitToSingleIdentity(identity)) {
        $j('.' + identity.identity_type + ' .add_identity').hide();
      }

      // If no primary identity exists, disallow the deletion of external identities
      if (!hasPrimaryIdentity && identity.isExternalIdentity) {
        $j('#' + identity.elmId).find('.remove_link').hide();
      }

    }, this));

    primaryIdentity = this.identitiesManager.primary();
    if (typeof primaryIdentity !== 'undefined') {
      primaryIdentityElm = $j('#' + primaryIdentity.elmId);
      primaryIdentityElm.find('.primary_label').show();
      primaryIdentityElm.find('.remove_link').hide();
    }

    this.setupPrimary();
  },


  setupPrimary: function() {

    var candidates = this.identitiesManager.allPrimaryCandidates();
    if (candidates.length > 1 || (candidates.length == 1 && !candidates[0].isPrimary)) {
      $j('#primary_section').show();
    } else {
      $j('#primary_section').hide();
      return;
    }

    var selectElm = $j('#primary_identity');

    this.renderPrimary(this.identitiesManager.primary());

    selectElm.unbind('change', $j.proxy(this.updatePrimary, this));
    selectElm.bind('change', $j.proxy(this.updatePrimary, this));
  },


  renderPrimary: function(primaryIdentity) {

    var selectElm = $j('#primary_identity');

    selectElm.html('');

    if (primaryIdentity == undefined) {
      selectElm.append( $j('<option>', { html: '&mdash;' }) );
    } else {
      selectElm.append( $j('<option>', { value: primaryIdentity.elmId, html: primaryIdentity.name + ' (' + I18n.t('txt.users.edit.primary') + ')' }) );
    }

    _(this.identitiesManager.allPrimaryCandidates()).each(function(identity) {
      if (identity != primaryIdentity) {
        selectElm.append( $j('<option>', { value: identity.elmId, html: identity.name }) );
      }
    });

  },


  updatePrimary: function(e) {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    // Only display primary identity drop-down if there is more than one primary identity
    var candidates = this.identitiesManager.allPrimaryCandidates();
    if (candidates.length < 1 || (candidates.length == 1 && candidates[0].isPrimary)) {
      $j('#primary_section').hide();
      return;
    }

    var selectElm        = $j('#primary_identity');

    var selectedIdentity = this.identitiesManager.find(selectElm.val());

    var spinner          = $j('<div>').addClass('spinner small').insertAfter(selectElm);

    this.identitiesManager.makePrimary(selectedIdentity)

      .done(function() { spinner.remove(); }) // TODO: in jQuery 1.6, combine done and fail to always
      .fail(function() { spinner.remove(); })

      .done($j.proxy(function() {
        this.renderPrimary(selectedIdentity);
        // Replaces the remove linke with the 'primary' label
        // for the new primary identity and display others
        $j('.remove_link').show();
        $j('#' + selectedIdentity.elmId).find('.remove_link').hide();
        $j('.primary_label').hide();
        $j('#' + selectedIdentity.elmId).find('.primary_label').show();

        clearFlash();
      }, this))

      .fail($j.proxy(function(response) {
        this.setupPrimary();
        showFlash(response, 'error');
      }, this));

  },


  deleteIdentity: function(e) {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    var targetElm = $j(e.target);

    var identity  = this.identitiesManager.find(targetElm.data('elmId'));

    if (!confirm(I18n.t('txt.users.edit.remove_account_confirmation', {identityName: identity.name}))) {
      return false;
    }

    // beforeSend
    var spinner   = $j('<div>').addClass('spinner small').insertAfter(targetElm);
    targetElm.hide();


    this.identitiesManager.remove(identity)
      .done(function() { spinner.remove(); }) // TODO: in jQuery 1.6, combine done and fail to always
      .fail(function() { spinner.remove(); })

      .done(function() {
        // Display the 'add identity' link that might have
        // been hidden to accommodate singleIdentityLimits
        $j('.' + identity.identityType + ' .add_identity').show();
        $j('#' + identity.elmId).remove();
        clearFlash();
      })

      .fail(function(response) {
        var json = JSON.parse(response.responseText);
        json.error && showFlash(json.error[0], 'error');
      });

  }
});
