/*global Zendesk, $j */
Zendesk.NS('Groups');

Zendesk.Groups.DeletionVerifier = function(containerSelector) {
  this.form = $j(containerSelector);
};

Zendesk.Groups.DeletionVerifier.prototype = {
  init: function() {
    this.attachHandlers();
  },

  attachHandlers: function() {
    var self = this;
    this.form.find('#delete-group').click(function(e) {
      self.showModal();
    });
  },

  showModal: function() {
    var self = this;

    $j.colorbox({
      href: './confirm_delete',
      inline: false,
      onComplete: function(){
        self.attachModalHandlers();
      }});
  },

  attachModalHandlers: function() {
    $j('#group-delete-confirm #cancel-delete').click(this.closeModal);
  },

  closeModal: function() {
    $j.colorbox.close();
  }
};