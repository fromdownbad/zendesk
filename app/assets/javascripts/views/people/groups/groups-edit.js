/*global $z, Zendesk */
$z.defModule('people/groups/edit', {
  initialize: function() {
    this.verifier = new Zendesk.Groups.DeletionVerifier("#account-settings");
    this.verifier.init();
  }
});