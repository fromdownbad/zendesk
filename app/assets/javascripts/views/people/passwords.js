Zendesk.NS('People.Passwords', function($j) {
  var sentSequenceNumber = 0;
  var recvSequenceNumber = 0;

  var showPasswordValidations = function(data) {
    var incomingSequenceNumber = data.sequence;

    // skip out of sequence packets
    if (incomingSequenceNumber < recvSequenceNumber) return;

    recvSequenceNumber = incomingSequenceNumber;

    var errors = _(data.errors);
    var passwordRequirements = $j('#password_requirements ul li');

    _(passwordRequirements).each(function(e) {
      var element   = $j(e);
      var errorText = $j.trim(element.text());

      if(errors.contains(errorText)) {
        element.
          addClass('invalid').
          removeClass('valid');
      } else {
        element.
          addClass('valid').
          removeClass('invalid');
      }
    });
  };

  $j(document).ready(function() {
    $j('.validate_password').keyup(function(event) {
      var url = currentAccount.secureUrlPrefix + '/password/validate_password';

      $j.ajax({
        type: 'POST',
        url: url,
        data: {
          password: $j(this).val(),
          user_security_policy_id: $j('#user_security_policy_id').val(),
          sequence: sentSequenceNumber += 1
        },
        success: showPasswordValidations
      });

    });
  });

}, jQuery);

