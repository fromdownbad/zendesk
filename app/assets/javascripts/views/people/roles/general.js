/*global Zendesk, document, jQuery*/

Zendesk.NS('People.Roles', function($j) {

  var ticketAccess = $j('#permission_set_permissions_ticket_access');
  var forumAccess  = $j('#permission_set_permissions_forum_access');
  var peopleAccess = $j('#permission_set_permissions_end_user_profile');
  var peopleListAccess = $j('#permission_set_permissions_available_user_lists');
  var form         = $j("#permission_set_form");
  var isLightAgent = form.data("light-agent");


  function setupAssignedTicketsOnlyDependencies(ticketAccess) {

    var ticketAccessMessage = $j('#ticket_access_message');
    var assignToAnyGroup    = $j('#assign_to_any_group');
    var ticketEditing       = $j('#permission_set_permissions_ticket_editing');
    var ticketEditingLabel  = $j("label[for='permission_set_permissions_ticket_editing']");

    var disableDependency = function() {
      var tooltip = "To deselect this option, you must change ticket access selection";
      ticketEditing
        .prop('checked', true)
        .prop('disabled', true)
        .prop('title', tooltip)
        .change();
      ticketEditingLabel.prop('title', tooltip);
    };

    var enableDependency = function() {
      // don't enable dependency in light agent role
      ticketEditing
        .prop('disabled', isLightAgent)
        .removeAttr('title');
      ticketEditingLabel.removeAttr('title');
    };

    var showGroupMessage = function() {
      ticketAccessMessage
        .html(I18n.t('txt.admin.javascrips.views.people.roles.user_belongs_group'))
        .show();
    };

    var showAssignToAnyGroup = function() {
      assignToAnyGroup.show();
    };

    var showOrganizationMessage = function() {
      ticketAccessMessage
        .html(I18n.t('txt.admin.javascrips.views.people.roles.user_belongs_organization'))
        .show();
    };

    var hideMessage = function() {
      ticketAccessMessage.hide();
    };

    var hideAssignToAnyGroup = function() {
      assignToAnyGroup.hide();
    };

    var interactions = {
      'assigned-only':       [hideMessage, hideAssignToAnyGroup, disableDependency],
      'within-groups':       [showGroupMessage, showAssignToAnyGroup, enableDependency],
      'within-organization': [showOrganizationMessage, hideAssignToAnyGroup, enableDependency],
      'all':                 [hideMessage, hideAssignToAnyGroup, enableDependency]
    };

    if (ticketEditing.exists()) {
      ticketAccess.selectInteraction(interactions);
    }

  }

  function setupForumFullAccessDependencies(forumAccess) {

    var forumRestrictedContent      = $j('#permission_set_permissions_forum_access_restricted_content');
    var forumRestrictedContentLabel = $j("label[for='permission_set_permissions_forum_access_restricted_content']");
    var manageHelpCenter            = $j("#permission_set_manage_help_center");

    var lockFullAccess = function() {
      var tooltip = "To deselect this option, you must change forums editing permission";
      forumRestrictedContent
        .prop('checked', true)
        .prop('disabled', true)
        .prop('title', tooltip);
      forumRestrictedContentLabel.prop('title', tooltip);
    };

    var unlockFullAccess = function() {
      forumRestrictedContent
        .prop('disabled', false)
        .removeAttr('title');
      forumRestrictedContentLabel.removeAttr('title');
    };

    var interactions = {
      full: lockFullAccess,
      'default-interaction': unlockFullAccess
    };

    if (forumRestrictedContent.exists()) {
      forumAccess.selectInteraction(interactions);
    }

    if (manageHelpCenter.exists()) {
      manageHelpCenter.prop('disabled', isLightAgent);
    }
  }

  function setupPeopleListDependencies(peopleListAccess) {

    var customerListAccess = $j('#user_view_access');

    var showDependency = function() {
      customerListAccess.slideDown();
    };

    var hideDependency = function() {
      customerListAccess.slideUp();
    };

    var interactions = {
      none:                  hideDependency,
      all:                   showDependency,
      'default-interaction': hideDependency
    };

    $j(peopleListAccess).selectInteraction(interactions);

  }

  function setupPeopleDependencies(peopleAccess) {

    var peopleDependencies  = $j('#people_editing_options');
    var groupOrgRestriction = $j('#permission_set_permissions_edit_organizations');

    var showDependency = function() {
      peopleDependencies.slideDown();
      groupOrgRestriction.prop('disabled', false);
    };

    var hideDependency = function() {
      peopleDependencies.slideUp();
      groupOrgRestriction.prop('disabled', true);
    };

    var interactions = {
      edit:                  showDependency,
      full:                  showDependency,
      'default-interaction': hideDependency
    };

    $j(peopleAccess).selectInteraction(interactions);

  }


  function setupPermissionDependencies() {

    if (ticketAccess.exists()) {
      setupAssignedTicketsOnlyDependencies(ticketAccess);
    }

    if (forumAccess.exists()) {
      setupForumFullAccessDependencies(forumAccess);
    }

    if (peopleAccess.exists()) {
      setupPeopleDependencies(peopleAccess);
    }

    if(peopleListAccess.exists()) {
      setupPeopleListDependencies(peopleListAccess);
    }

  }

  // disable all elements except ticket, reporting access and view ticket satisfaction prediction
  function setupLightAgentForm() {
    var assignToAnyGroupField = $j("[name='permission_set[permissions][assign_tickets_to_any_group]']"),
        reportAccessField = $j("[name='permission_set[permissions][report_access]']"),
        viewSatisfactionPrediction = $j("[name='permission_set[permissions][view_ticket_satisfaction_prediction]']");

    reportAccessField.find("option[value='full']").remove();

    form.find(":input").
      not(":submit, [name='_method'], [name='authenticity_token']").
      not(ticketAccess).
      not(assignToAnyGroupField).
      not(reportAccessField).
      not(viewSatisfactionPrediction).
      prop("disabled", true);
  }

  $j(document).ready(function() {
    setupPermissionDependencies();

    if (isLightAgent) {
      setupLightAgentForm();
    }
  });

}, this.jQuery);
