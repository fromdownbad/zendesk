jQuery(document).ready(function($) {
  var form         = $("#custom_role_form"),
      isLightAgent = form.data("light-agent");

  function setupActionsMenu() {
    var actionsMenu = $('header .actions'),
        button = actionsMenu.find('button'),
        menu = actionsMenu.find('nav.c-menu');

    button.on('click', function(event) {
      event.stopPropagation();
      if (menu.is(':visible')) {
        button.removeClass('active');
        menu.hide();
      } else {
        button.addClass('active');
        menu.show();
      }
    });

    $(document).on('click', function(event) {
      if (button !== event.target && !button.has(event.target).length) {
        button.removeClass('active');
        menu.hide();
      }
    });
  }

  function setupTicketAccessDependencies() {
    var assignTicketsToAnyGroup = $('#assign_tickets'),
        ticketAccess     = $("input[name='permission_set[permissions][ticket_access]']"),
        ticketEditing    = $("input[name='permission_set[permissions][ticket_editing]']");

    var disableTicketEditingField = function() {
      ticketEditing
        .prop('checked', true)
        .prop('disabled', true)
        .change();
    };

    var enableTicketEditingField = function() {
      // don't enable field in light agent role
      ticketEditing
        .prop('disabled', isLightAgent);
    };

    ticketAccess.on('change', function(event) {
      if (event.target.value === 'assigned-only') {
        disableTicketEditingField();
      } else {
        enableTicketEditingField();
      }

      if (event.target.value === 'within-groups') {
        assignTicketsToAnyGroup.show();
      } else {
        assignTicketsToAnyGroup.hide();
      }
    });
  }

  function setupTicketEditingDependencies() {
    var ticketEditing            = $("input[name='permission_set[permissions][ticket_editing]']"),
        ticketEditingSubsettings = $("#sub_setting_ticket_editing_options");
    ticketEditing.on('change', function(event) {
      if (event.target.checked) {
        ticketEditingSubsettings.show();
      } else {
        ticketEditingSubsettings.hide();
      }
    });
  }

  function setupTicketDeletionDependencies() {
    var ticketDeletion     = $("input[name='permission_set[permissions][ticket_deletion]']"),
        viewDeletedTickets = $("#sub_setting_ticket_deletion");
    ticketDeletion.on('change', function(event) {
      if (event.target.checked) {
        viewDeletedTickets.show();
      } else {
        viewDeletedTickets.hide();
      }
    });
  }

  function setupEndUserProfileAccessDependencies() {
    var endUserProfileAcess  = $("input[name='permission_set[permissions][end_user_profile]']"),
        peopleEditingOptions = $("#people_editing_options");
    endUserProfileAcess.on('change', function(event) {
      if (event.target.value === 'full') {
        peopleEditingOptions.show();
      } else {
        peopleEditingOptions.hide();
      }
    });
  }

  function setupUserListAccessDependencies() {
    var userListAccess     = $("input[name='permission_set[permissions][available_user_lists]']"),
        customerListPermissions = $("#customer_list_permissions");
    userListAccess.on('change', function(event) {
      if (event.target.value === 'all') {
        customerListPermissions.show();
      } else {
        customerListPermissions.hide();
      }
    });
  }

  function setupLightAgentForm() {
    var assignToAnyGroupField      = $("[name='permission_set[permissions][assign_tickets_to_any_group]']"),
        reportAccessField          = $("[name='permission_set[permissions][report_access]']"),
        ticketAccess               = $("input[name='permission_set[permissions][ticket_access]']"),
        viewSatisfactionPrediction = $("[name='permission_set[permissions][view_ticket_satisfaction_prediction]']");

    reportAccessField.find("option[value='full']").remove();

    form.find(":input").
      not(":submit, [name='_method'], [name='authenticity_token']").
      not(ticketAccess).
      not(assignToAnyGroupField).
      not(reportAccessField).
      not(viewSatisfactionPrediction).
      prop("disabled", true);
  }

  $(document).ready(function() {
    if (form.length > 0) {
      setupActionsMenu();
      setupTicketAccessDependencies();
      setupTicketEditingDependencies();
      setupTicketDeletionDependencies();
      setupEndUserProfileAccessDependencies();
      setupUserListAccessDependencies();

      if (isLightAgent) {
        setupLightAgentForm();
      }
    }
  });
});
