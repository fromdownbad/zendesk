/*global $z, $j, clearFlash, showFlash, _*/

$z.defModule('people/bulk_delete/index', {
  initialize: function() {
    this.form = $j('#bulk_form');
    this.check = $j('#bulk_check_all');
    this.submitBtn = $j('#bulk_submit');
    this.checkboxes = $j('#bulk_form .bulk_check_one');
    this.filterCheckBox = $j('.filter-check-box');

    this.toggleBulkSubmit();
    this.check.change($j.proxy(this.manageBulkUpdate, this));
    this.checkboxes.change($j.proxy(this.toggleBulkSubmit, this));
    this.submitBtn.click($j.proxy(this.submitBulkForm, this));
    this.filterCheckBox.change($j.proxy(this.filterAction, this));
  },

  filterAction: function(e) {
    var $checkbox = $j(e.target);
    var url = $checkbox.data('href');
    window.location = url;
  },

  manageBulkUpdate: function(e) {
    var bulkCheckSelected = this.check.prop('checked');
    this.checkboxes.prop('checked', bulkCheckSelected);
    this.toggleBulkSubmit();
  },

  submitBulkForm: function(e) {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    if (!this.anyChecked()) return;

    $j('#bulk_spinner').show();

    $j.ajax( {
      type: this.form.attr('method'),
      url:  this.form.attr('action'),
      data: this.form.serialize(),

      success: function(response) {
        clearFlash();
        $j.colorbox({ html: response });
        $j('#bulk_spinner').hide();
      },

      error: function(response) {
        showFlash("Error:" + response, 'error');
        $j('#bulk_spinner').hide();
      }
    });

  },

  toggleBulkSubmit: function(e) {
    var checked = this.anyChecked();
    this.submitBtn.prop('disabled', !checked);
    this.submitBtn.toggleClass('button_disabled', checked);
  },

  anyChecked: function() { return $j('#bulk_form .bulk_check_one:checked').length > 0; }

});
