/*globals jQuery, $j, Zendesk, I18n*/
var TicketForm = function() {};

TicketForm.prototype = {

  _cacheFollowing: function(mth, requester, value) {
    TicketForm.twitterFollowingCache = TicketForm.twitterFollowingCache || {};

    var cache = TicketForm.twitterFollowingCache;
    var cacheKey = [mth.screenName, requester.screenName].join(',');
    if(value !== undefined) {
      return cache[cacheKey] = value;
    } else {
      return cache[cacheKey];
    }
  },

  isFollowing: function(mth, requester) {
    var self = this;
    var cached = this._cacheFollowing(mth, requester);
    if(cached !== undefined) {
      jQuery("#comment_channel_back option[value='dm']").prop('disabled', (cached !== true));
      return;
    }

    jQuery.ajax({
      url: Zendesk.Proxy.domain() + '/twitter/api/' + mth.id + '/1.1/friendships/lookup.json',
      data: { 'screen_name': requester.screenName },
      type: 'GET',
      success: function(data) { self.isFollowingCallback(data, mth, requester); }
    });
  },

  isFollowingCallback: function(data, mth, requester) {
    var following = !!data.find(function(d) { return requester.id === d.id && d.connections.include('followed_by'); });
    this._cacheFollowing(mth, requester, following);
    jQuery("#comment_channel_back option[value='dm']").prop('disabled', following !== true);
  },

  updateBalloon: function() {
    var checkbox = jQuery('#comment_is_public');
    var balloon  = jQuery('#comment_type');

    if (checkbox.attr('checked')) {
      balloon.removeClass('private');
      balloon.addClass('public');
    } else {
      balloon.removeClass('public');
      balloon.addClass('private');
    }
  },

  updateTwitterControls: function() {
    var checkbox = jQuery('#comment_is_public');
    var controls = jQuery('#twitter_controls');

    (checkbox.attr('checked')) ? controls.show() : controls.hide();
  },

  updateTwitterCounter: function() {
    var checkbox = jQuery('#comment_is_public');
    var counter  = jQuery('#charcounter');
    var channel  = jQuery('#comment_channel_back').val();
    var dm       = jQuery('#comment_channel_back_dm');
    counter.toggle(checkbox.is(':checked') && (channel == "1" || channel == "dm"));
  },

  updateMthSelector: function() {
    var checkbox = jQuery('#comment_is_public');
    var selector = jQuery('#monitored_twitter_handle_selection');

    (checkbox.attr('checked')) ? selector.show() : selector.hide();
  },

  bindEditRequesterLink: function() {
    $j('#edit_requester_link a').bind('click', function(event) {
      $j('#edit_requester').show();
      $j('#dynamic_requester').hide();
      $j('#static_requester').show();
      $j('#edit_requester_link').hide();

      return false;
    });
  },

  bindCheckbox: function() {
    var self = this;

    jQuery('#comment_is_public').change(function() {
      self.updateBalloon();
      self.updateMthSelector();
      self.updateTwitterControls();
      self.updateTwitterCounter();
    });
  },

  bindRadioButtons: function() {
    var self = this;

    jQuery('#twitter_controls input[type="radio"]').click(function() {
      self.updateTwitterCounter();
    });
  }

};
