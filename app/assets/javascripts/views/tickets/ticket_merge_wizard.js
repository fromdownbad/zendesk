function TicketMergeWizard() {
  this.url = '/merge/new?source_ids=' + this.sources() + "&unchecked=" + this.unchecked() + '&background=true';
  this.registerEvents();
  this.show();
}

TicketMergeWizard.prototype = {
  url: null,

  registerEvents: function() {
    var self = this;

    self.winnerLinks().live('click', function(e) {
      $j("#target_id").val($j(this).data('target-id'));
      self.winnerForm().submit();
      return false;
    });

    $j(document).bind('cbox_closed', function(){
      $j('#submit-button').val('Submit').prop('disabled', false);
    });
  },

  show: function() {
    var self = this;
    $j.colorbox({href: this.url,
                 onComplete: function() {
                   if(self.loaded) { return }
                   self.winnerForm().submit(function() {
                     var niceId = $j.trim($j('#target_id').val());
                     if (niceId === '') {
                       alert('Please enter a ticket ID');
                     } else {
                       $j.ajax({
                         type: $j(this).attr('method'),
                         data: $j(this).serialize(),
                         url: $j(this).attr('action'),
                         success: function(response) {
                           $j.colorbox({html: response});
                           self.appendComment();
                         },
                         error: function(request, textStatus, errorThrown) {
                           $j.colorbox({html: request.responseText});
                         }
                       });
                     }
                     return false;
                   });
                 }
                });
  },

  appendComment: function() {
    if ($j.trim($j('#comment_value').val()) !== '') {
      var text = '\n\nComment during merge: ' + $j('#comment_value').val();
      $j('#target_comment').append(text);
      $j('#source_comment').append(text);
    }
  },

  sources: function() {
    if ($j("#ticket-chat").attr("data-ticket") !== undefined) {
      return $j("#ticket-chat").attr("data-ticket");
    } else {
      return this.checked();
    }
  },

  checked: function() {
    return this.values("input.tickets_to_bulk_update:checked");
  },

  unchecked: function() {
    return this.values("input.tickets_to_bulk_update:not(:checked)");
  },

  values: function(filter) {
    var array = [];
    $j(filter).each(function() {
      array.push($j(this).val());
    });
    return array.join(",");
  },

  winnerForm: function() {
    return $j("#merge_form");
  },

  winnerField: function() {
    return this.winnerForm().find("input[type='text']");
  },

  continueButton: function() {
    return this.winnerForm().find("input[type='submit']");
  },

  winnerLinks: function() {
    return $j(".winner_selector");
  }
}
