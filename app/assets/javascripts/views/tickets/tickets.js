$z.defModule('tickets', {
  addSubmitBindings: function(){
    function submit(event){
      event.preventDefault();
      if ($j('input#submit-button').is(':disabled')) {
        return;
      }
      $j('input#submit-button').click();
    }

    function submitAndNext(event) {
      $j("form#submit_form select#submit_type option[value='next']").prop("selected", true);
      submit(event);
    }

    // This is a hack to get around the fact that jQuery.hotkeys does not
    // support live events on input elements.
    (function($) {
      var s = 83;

      function submitOnKeyEvents(event) {
        if (event.ctrlKey && event.which === s) {
          if (event.shiftKey) {
            submitAndNext(event);
          } else {
            submit(event);
          }
        }
      }

      $(document).bind('keydown.zendesk.keyboard-shortcut', submitOnKeyEvents);
    })(jQuery);
  },

  // Since the currently selected group may have few agents, but another group
  // may have _many_, we can't use autocompleteFromSelectIfLarge. Instead,
  // we upgrade to autocomplete iff the account has many agents.
  autoselectAssigneesIfManyAgents: function() {
    if (typeof(agents) !== 'undefined' && agents.length > LARGE_SELECT_LENGTH) {
      $j('select.assignee').autocompleteFromSelect();
    }
  },

  monitorRequesterChange: function() {
    var re = new RegExp(/<(.*)>/);
    $j("#ticket_requester_name").observe_field(3, function( ) {
      if (re.test($j(this).val())) {
        $z("tickets").requesterChanged($j(this));
      }
    });
  },

  requesterChanged: function(ticketForm) {
    $j.ajax({
      url: '/tickets/requester_change',
      data: ticketForm.serialize(),
      beforeSend: function(){ $j("#user_details").fadeOut( "fast" ); $j("#add_widget_button").hide(); },
      success: function(){ $j("#user_details").fadeIn( "fast" ); $j("#add_widget_button").show(); }
    });
  }
});
