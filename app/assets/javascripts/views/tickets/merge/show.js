$z.defModule("tickets/merge/show", {
  initialize: function(){
    /* This part is sadly never reached for lightboxes... */
  },

  showMergeJobStatus: function(request) {
    $j.colorbox.close();

    var job = request.responseJSON;
    var jobStatus = new Zendesk.JobStatus(job.status_url, {
      title: I18n.t('txt.public.javascripts.views.tickets.merge.merging_tickets'),
      renderResult: function(jobStatus) {
        var output = {
          title: '',
          body: $j('<ul/>')
        };

        if (jobStatus.status === 'completed') {
          document.location = '/merge/result?background=' + jobStatus.job.id;
        }
        return output;
      }
    });
  }
});
