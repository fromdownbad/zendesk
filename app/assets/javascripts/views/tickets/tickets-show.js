$z.defModule('tickets/show', {
  initialize: function() {
    $j('textarea#comment_value').focus(); // focus on the requester field on page load

    // navigate in the right order
    $z('tabindex').addtabindexes(
      '#edit_cc input',
      'form#ticket-chat div.selects div.select > input[readonly != readonly][attribute_name != additional_tags], form#ticket-chat div.selects div.select > select[readonly != readonly], form#ticket-chat div.selects div.select > textarea[readonly != readonly]',
      '#ticket_tags', // tags input, before autocomplete changes it
      '.selects .multi_value_field .search_field_item input', // tags input, after autocomplete changes it
      'textarea#comment_value',
      'input#comment_is_public',
      'select#submit_type',
      'input#submit_type',
      'input#submit-button'
    );

    $z('tickets').addSubmitBindings(); // adds ctrl+s ticket submission, but won't work with current version of jQuery Hotkeys
    $z('tickets').monitorRequesterChange();
    $z('tickets').autoselectAssigneesIfManyAgents();
    if ( $j('#ticket_linked_id').attr('auto-complete') ) {
      $j('#ticket_linked_id').autocompleteFromSelect();
    }

    $j(".visibility-controls").click(function() {
      $j("a.toggle-twitter-data", this).toggle();
    });

    $j('a.toggle-twitter-data').click(function() {
      $j('.twitter-properties').toggle();
      return false;
    });

    $j("#comment_full .link, #comment_partial .link").click(function() {
      $j('#comment_full, #comment_partial, #comment_up, #comment_down').toggle();
      return false;
    });

    $j('#sharing_with :first-child').hover(
      function() { $j('#shared_tickets_list').fadeIn(300); },
      function() { $j('#shared_tickets_list').fadeOut(100); }
    );

    if ($j('body.tickets-show').length && currentAccount.daysLeftInTrial) {
      Zendesk.Instrumentation.track('View Ticket', 'Trial');
    }

    new UserMergeWizard(false);
    new NewUserFromTicket();

    $j('input, select, textarea', '#ticket_properties.read_only').prop("disabled", true);

    Zendesk.Ticket.commentDrafts = new Zendesk.Ticket.CommentDrafts(ticket_id, currentUser.id, "#comment_value", "#comment_is_public");
    Zendesk.Ticket.commentDrafts.startWatching();
    new Zendesk.Twitter.TicketCreationForm()._updateCounter();
  },

  setupCommentCharacterCounter: function(options) {
    options = options || {}
    options.ticketPage = true;
    new Zendesk.Twitter.TicketCreationForm().setupTweetCounter(options);
  }
});
