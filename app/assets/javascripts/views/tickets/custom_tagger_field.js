/*globals jQuery, $, selection_feedback*/
(function($j) {

  $j.widget('zd.ticketTagger', {

    _create: function() {
      var fieldID = this.element.data('field-id');
      this.$input  = this.element.parent().find('#ticket_fields_' + fieldID);
      this.$output = this.element.find('#title-tagger-' + fieldID);
      this._addBindings();
    },

    // If called without a parameter, returns the value.
    // If called with a parameter, sets the value, keeping the UI in sync.
    val: function(newValue) {
      if (arguments.length === 1) {
        this.$input.val(newValue).change();
        return this;
      } else {
        return this.$input.val();
      }
    },

    _addBindings: function() {
      var widget = this;

      this.element.delegate('li.link', 'click.ticketTagger', function() {
        widget.$input.val($j(this).data('value')).trigger('change.ticketTagger');
        selection_feedback($(this));
        return false;
      });

      this.$input.bind('change.ticketTagger', function(event, source) {
        var $li = widget._findLIByValue($j(this).val());
        widget.$output.html($li.data('full-title'));
      });
    },

    _findLIByValue: function(value) {
      return this.element.find('li.link[data-value="' + value + '"]');
    }

  });

  $j('.field-tagger').ticketTagger();
}(jQuery));
