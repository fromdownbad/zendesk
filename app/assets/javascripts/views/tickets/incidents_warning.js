function IncidentsWarning() {
  this._registerEvents();
}

IncidentsWarning.instance = null;

IncidentsWarning.getInstance = function() {
  if (IncidentsWarning.instance === null) {
    IncidentsWarning.instance = new IncidentsWarning();
  }
  return IncidentsWarning.instance;
};

IncidentsWarning.prototype = {
  confirmed: false,

  isShowingWarning: function() {
    var self = this;
    if ($j('#associated_incidents_warning').length !== 0 && self._updating() && self._solving() && !self.confirmed) {
      $j.colorbox({inline:true, href:"#associated_incidents_warning"});
      return true;
    } else {
      return false;
    }
  },

  _registerEvents: function() {
    var self = this;

    $j('#confirm_incidents_warning').click(function(event) {
      $j.colorbox.close();
      self.confirmed = true;
      submitTicketForm();
    });

    $j('#cancel_incidents_warning').click(function(event) {
      $j.colorbox.close();
    });
  },

  _updating: function() {
    var type = $j('#submit_type').val();
    return type === '' || type === 'macro' || type === 'entry';
  },

  _solving: function() {
    return $j('#ticket_status_id').val() === '3';
  },

  _addingPublicComment: function() {
    return $j.trim($j("#comment_value").val()).length !== 0 && $j('#comment_is_public').is(':checked');
  }

};
