/*globals jQuery*/
jQuery(function($) {
  var projectIDElm, projectID, projectOptions, issueIDElm, issueOptions, projectIssues, jiraProjects;
  var selectedAgreementElm, jiraAgreementIDs, agreementID;
  var agreementIDElm = $('#ticket_agreement_id');

  if (!agreementIDElm.length) { return; }

  projectIDElm = $('#jira_project_id');
  issueIDElm = $('#issue_type_id');

  jiraProjects = {};

  function isJiraAgreementSelected(agreementID) {
    jiraAgreementIDs = agreementIDElm.data('jira-agreement-ids');

    return($.inArray(agreementID, jiraAgreementIDs) !== -1);
  }

  function updateIssues() {
    if (!jiraProjects) { return; }

    projectID = $('#jira_project_id option:selected').val();
    if (!projectID) { return; }

    issueOptions = '';
    projectIssues = jiraProjects[projectID].issueTypes;

    for (var issue in projectIssues) {
      if (projectIssues.hasOwnProperty(issue)) {
        issueOptions += '<option value="' + projectIssues[issue].id + '">' + projectIssues[issue].name + '</option>';
      }
    }

    issueIDElm.html(issueOptions);
  }

  function updateProjects() {
    projectOptions = '';

    for (var project in jiraProjects) {
      if (jiraProjects.hasOwnProperty(project)) {
        projectOptions += '<option value="' + project + '">' + jiraProjects[project].name + '</option>';
      }
    }

    projectIDElm.html(projectOptions);
  }

  function getJIRAInfo() {
    if (!jiraProjects) { return; }

    $.ajax({ url: '/sharing_agreements/' + agreementID + '/jira_projects' })

    .done(function(data) {
      data.each(function(project) {
        jiraProjects[project.id] = project;
      });
      updateProjects();
      updateIssues();
    })

    .fail(function() {
      $('#jira_projects').html("Unable to load projects from JIRA");
      $('#jira_issues').html("Unable to load issues types from JIRA");
    });
  }

  function showColorbox() {
    $.colorbox({
      width: '550px',
      inline: true,
      href: '#jira_sharing_options',
      onComplete: getJIRAInfo,
      onLoad: function() {$('#cboxClose').remove();},
      overlayClose: false,
      escKey: false
    });
  }

  function saveAndClose() {
    $.colorbox.close();
  }

  function clearOptions() {
    projectIDElm.val('');
    issueIDElm.val('');
    $('#jira_issue_key').val('');
  }

  function agreementChanged(e) {
    agreementID = parseInt(e.target.value, 10);

    if (isJiraAgreementSelected(agreementID)) {
      showColorbox();
    }
    else {
      clearOptions();
    }
  }

  function cancel() {
    clearOptions();
    $.colorbox.close();
  }

  $('#jira_sharing_options a').click(function(event) { event.preventDefault(); });
  $('#cancel_jira_sharing_options').click(cancel);
  $('#save_jira_sharing_options').click(saveAndClose);

  agreementIDElm.change(agreementChanged);
  projectIDElm.change(updateIssues);

});
