/*globals $j, _, Zendesk */

Zendesk.NS('Ticket', function() {
  this.CommentDrafts = function(niceID, userID, commentSelector, commentPrivacySelector) {
    this.niceID                 = niceID;
    this.userID                 = userID;
    this.key                    = "drafts/" + this.niceID + "/" + this.userID;
    this.timestampKey           = this.key + "/ts";
    this.privacyKey             = this.key + "/privacy";
    this.translatedVersion      = this.key + "/translated";
    this.translatedHash         = this.key + "/translatedHash";
    this.commentSelector        = commentSelector;
    this.commentPrivacySelector = commentPrivacySelector;
    this.translationSelector    = "#translated_flag";
    this._keyHandler            = _.debounce(this._keyHandler, 300);
  };

  this.CommentDrafts.STALE_TIMEOUT_SECONDS = 8*60*60;

  this.CommentDrafts.prototype = {
    clear: function() {
      if(!Zendesk.Storage.isSupported()) {
        return;
      }

      this.localStorage = Zendesk.Storage.handle();

      return this._clear();
    },

    startWatching: function() {

      if(!Zendesk.Storage.isSupported()) {
        return;
      }

      this.localStorage = Zendesk.Storage.handle();
      this.commentInput = $j(this.commentSelector);
      this.commentPrivacyInput = $j(this.commentPrivacySelector);
      this.translationLink = jQuery(this.translationSelector);

      if(this._isStale()) {
        this._clear();
      }

      var savedComment = this._load();

      if(savedComment.comment) {
        this.comment = savedComment.comment;
        this.commentInput.val(this.comment);
        this._renderDraftNotification(I18n.t('comment_draft.recovered_a_comment_from_draft'));
      } else {
        this.comment = this.commentInput.val();
      }

      if(savedComment.commentPrivacy) {
        this.commentPrivacy = savedComment.commentPrivacy;
        this.commentPrivacyInput.prop('checked', this.commentPrivacy === "public").change();
      } else {
        this.commentPrivacy = this.commentPrivacyInput.prop('checked') ? 'public' : 'private';
      }

      this._bindEvents();
    },

    _bindEvents: function() {
      $j(this.commentInput).keyup($j.proxy(function(e) {
        this._keyHandler(e);
      }, this));

      var proxiedHandler = $j.proxy(function(e) {
        this._updateDraftFromDom(e);
      }, this);

      $j(this.commentInput).change(proxiedHandler);

      $j(document).bind("applied.macro.zendesk", proxiedHandler);

      $j(this.commentPrivacyInput).change(proxiedHandler);

      $j(this.translationLink).bind('saveTranslated', proxiedHandler);
    },

    _keyHandler: function(evt) {
      this._updateDraftFromDom(evt);
    },

    _updateDraftFromDom: function(event) {
      var privacy = (this.commentPrivacyInput.prop('checked') ? 'public' : 'private');

      if((this.comment != this.commentInput.val()) || (this.commentPrivacy != privacy)) {
        if(!$j(event.target).is(this.commentPrivacyInput)) {
          this._renderDraftSaveNotification();
        }
        this._store(this.commentInput.val(), privacy);
      }
    },

    _renderDraftSaveNotification: function() {
      var message = I18n.t('txt.public.javascript.views.tickets.comment_drafts.comment_draft_saved');
      var displayed_message = message + ' (<abbr id="draft_timestamp" title="' + this._iso8601Timestamp(new Date()) + '"></abbr>)';
      this._renderDraftNotification(displayed_message);
      $j("#draft_timestamp").timeago();
    },

    _store: function(str, privacy) {
      this.comment = str;
      this.commentPrivacy = privacy;

      this.localStorage.setItem(this.timestampKey, (new Date()).toString());
      this.localStorage.setItem(this.key, this.comment);
      this.localStorage.setItem(this.privacyKey, this.commentPrivacy);
    },

    _renderDraftNotification: function(message) {
      this.commentInput.css({backgroundPosition: 'bottom center', paddingBottom: '25px'});

      if ($j("#draft_status").length === 0) {
        this.commentInput.after('<div id="draft_status"></div>');
      }

      $j("#draft_status").html(message);

    },

    _zeropad: function(num) {
      return ((num < 10) ? '0' : '') + num;
    },

    _iso8601Timestamp: function(date) {
      return date.getUTCFullYear() + "-" +
        this._zeropad(date.getUTCMonth() + 1) + "-" +
        this._zeropad(date.getUTCDate()) + "T" +
        this._zeropad(date.getUTCHours()) + ":" +
        this._zeropad(date.getUTCMinutes()) + ":" +
        this._zeropad(date.getUTCSeconds()) + "Z";
    },

    _load: function() {
      return {
        comment: this.localStorage.getItem(this.key),
        commentPrivacy: this.localStorage.getItem(this.privacyKey)
      };
    },

    _clear: function() {
      this.localStorage.removeItem(this.key);
      this.localStorage.removeItem(this.timestampKey);
      this.localStorage.removeItem(this.privacyKey);
    },

    _isStale: function() {
      var timestamp = this.localStorage.getItem(this.timestampKey);
      if(!timestamp) {
        return false;
      }

      var curTime = (new Date()).getTime();
      timestamp = new Date(timestamp).getTime();

      return (curTime - timestamp) > Zendesk.Ticket.CommentDrafts.STALE_TIMEOUT_SECONDS*1000;
    }
  };
});
