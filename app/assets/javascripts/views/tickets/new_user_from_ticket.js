function NewUserFromTicket() {
  this.registerEvents();
}

NewUserFromTicket.prototype = {
  type: null,

  registerEvents: function() {
    var self = this;

    self.link().click(function() {
      self.type = $j(this).attr('data-type');
      $j.colorbox({href: $j(this).attr('href'),
                   onComplete: function() {
                     self.emailField().focus();
                   }});
      return false;
    });

    self.form().live('submit', function() {
      var email = $j.trim(self.emailField().val());
      if (email === '') {
        alert(I18n.t('txt.public.javascripts.views.tickets.new_user_from_ticket.please_enter_an_email_address_new_user'));
        self.emailField().focus();
      } else {
        $j.ajax({
          type: $j(this).attr('method'),
          data: $j(this).serialize(),
          url: $j(this).attr('action'),
          beforeSend: function(request) {
            self.submitButton().val(I18n.t('txt.public.javascripts.views.tickets.new_user_from_ticket.creating')).prop("disabled", true);
          },
          success: function(response) {
            var value = null;

            if (response.email != null && response.email !== "") {
              value = response.name + " <" + response.email + ">";
            }

            if (self.type === 'requester') {
              $j('#ticket_requester_name').val(value);
              $j('#ticket_requester_name').effect("highlight", {}, 3000);
            } else {
              collaboratorsInput.addEntry(response.id, value);
              $j('div#edit_cc li[choice_id='+response.id+']').effect("highlight", {}, 3000);
            }
            $j.colorbox.close();
          },
          error: function(request, textStatus, errorThrown) {
            self.submitButton().val(I18n.t('txt.public.javascripts.views.tickets.new_user_from_ticket.create')).prop("disabled", false);
            if (request.responseText.include("The organization")) {
              self.organizationField().focus();
              new Effect.Highlight(self.organizationField().attr("id"), { duration: 3 });
            } else {
              self.emailField().focus();
              new Effect.Highlight(self.emailField().attr("id"), { duration: 3 });
            }
          }
        });
      }
      return false;
    });
  },

  link: function() {
    return $j("a.new_user_from_ticket");
  },

  form: function() {
    return $j("#new_user_form");
  },

  emailField: function() {
    return this.form().find("#user_email");
  },

  organizationField: function() {
    return this.form().find("#user_organization_name");
  },

  submitButton: function() {
    return this.form().find("input[type='submit']");
  }
}
