$j(function() {
  var jiraElement = $j('#jira_details');

  if (jiraElement.length) {
    var ticketID = jiraElement.data("ticketid");

    $j.ajax({ url: '/tickets/' + ticketID + '/jira_ticket_details' })
      .done(function(jira) {
        jira = $j.makeArray(jira)[0];
        var resolution = (jira.RESOLUTION || ' - ');
        var assignee = (jira.ASSIGNEE || ' - ');
        var string = '<ul>';
        string = string + '<li><strong>Issue ID</strong>: <a href=' + jira.URL + '>' + jira.KEY + '</a></li>';
        string = string + '<li><strong>Resolution</strong>: ' + resolution + '</li>';
        string = string + '<li><strong>Assignee</strong>: ' + assignee + '</li>';
        string = string + '</ul>';
        $j('#jira_ticket #jira_details').html(string);
      })
      .fail(function() {
        $j('#jira_ticket #jira_details').html("Unable to retrieve issue details from JIRA.");
      });
  }
});
