/*globals jQuery Zendesk I18n*/

(function(window, $, Zd) {

  var switchToEditMode = function(instance) {
    if (instance.mode === 'edit') {
      return false;
    }

    instance.mode = 'edit';

    instance.previewModeLink.removeClass('disabled');
    instance.editModeLink.addClass('disabled');

    instance.markdownContainer.hide();

    instance.commentField.show().focus();
    $(instance.draftStatusSelector).show();
  };

  var switchToPreviewMode = function(instance) {
    if (instance.mode === 'preview') {
      return false;
    }

    instance.mode = 'preview';
    instance.commentFieldHeight = instance.commentField.height();

    instance.editModeLink.removeClass('disabled');
    instance.previewModeLink.addClass('disabled');

    instance.markdownContainer.html('<center><img src="/images/ajax_loader_small.gif" alt="Loading"/></center>').css('min-height', instance.commentFieldHeight).show();

    instance.commentField.hide();
    $(instance.draftStatusSelector).hide();

    var request = jQuery.ajax({
      url : '/api/v1/format/markdown.json',
      type: 'POST',
      data: {
        text: instance.commentField.val()
      }
    });

    request.done(function(data) {
      instance.markdownContainer.html(data.html);
    });

    request.fail(function(){
      instance.markdownContainer.html(I18n.t('txt.admin.helpers.tickets_helper.preview_mode_error'));
    });
  };

  Zd.MarkdownPreview = function(options) {
    options = options || {};
    this.previewModeLinkSelector   = options.previewModeLinkSelector   || '#comment_preview_mode_link';
    this.editModeLinkSelector      = options.editModeLinkSelector      || '#comment_edit_mode_link';
    this.markdownContainerSelector = options.markdownContainerSelector || '#markdown_preview';
    this.commentFieldSelector      = options.commentFieldSelector      || '#comment_value';
    this.draftStatusSelector       = options.draftStatusSelector       || '#draft_status';

    this.init(this);
  };

  Zd.MarkdownPreview.prototype.init = function(instance) {
    instance.previewModeLink   = $(instance.previewModeLinkSelector);
    instance.editModeLink      = $(instance.editModeLinkSelector);
    instance.markdownContainer = $(instance.markdownContainerSelector);
    instance.commentField      = $(instance.commentFieldSelector);

    instance.editModeLink.click(function() {
      switchToEditMode(instance);
    });

    instance.previewModeLink.click(function(){
      switchToPreviewMode(instance);
    });

    switchToEditMode(instance);
  };

}(this, jQuery, Zendesk));
