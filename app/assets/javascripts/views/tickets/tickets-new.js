$z.defModule('tickets/new', {
  initialize: function(){
    $j('input#ticket_requester_name').focus(); // focus on the requester field on page load

    // navigate in the right order
    $z('tabindex').addtabindexes(
      'input#ticket_requester_name',
      '#edit_cc input',
      'form#ticket-chat div.selects div.select > input[readonly != readonly][attribute_name != additional_tags],form#ticket-chat div.selects div.select > select[readonly != readonly],form#ticket-chat div.selects div.select > textarea[readonly != readonly]',
      'textarea#comment_value',
      '#ticket_tags', // tags input, before autocomplete changes it
      '.selects .multi_value_field .search_field_item input', // tags input, after autocomplete changes it
      'select#submit_type',
      'input#submit_type',
      'input#submit-button'
    );

    $z('tickets').addSubmitBindings(); // adds ctrl+s ticket submission, but won't work with current version of jQuery Hotkeys
    $z('tickets').monitorRequesterChange();
    $z('tickets').autoselectAssigneesIfManyAgents();

    if ( $j('#ticket_linked_id').attr('auto-complete') ) {
      $j('#ticket_linked_id').autocompleteFromSelect();
    }

    new NewUserFromTicket();
  }
});
