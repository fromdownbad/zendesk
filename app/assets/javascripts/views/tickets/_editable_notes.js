$z.defModule("tickets/_editable_notes", {
  initialize: function(field, url) {
    button = $j('#'+field+'-submit');
    button.click(function(event) {
      data = $j('#'+field+'-form').serialize();
      data['_method'] = "put";
      jQuery.post(url, data);
      InputTracking.trackedElements = [];
    });

    $j('#'+field+'-link-show').click(function() {
      $j('div#'+field+'-edit').toggle();
      $j('div#'+field+'-show').toggle();
    });
  }
});
