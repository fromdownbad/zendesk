$z.defModule('ticket_fields/_field_tagger', {
  /*
   * @param [Array[Object]] options of the form [ { id: String, name: String, value: String], ...]
   */
  initialize: function(options) {
    var self = $z('ticket_fields/_field_tagger');

    $j('fieldset.conditions a').live('click', function(event) {
      event.preventDefault();
      $j(this).closest('fieldset.conditions').remove();
      return false;
    });

    $j('.customFieldName').live('blur', function() {
      var valueField = $j(this).closest('fieldset.conditions').find('.customFieldValue');
      if (valueField.val() === '') {
        //We are using this Regex library: http://xregexp.com/
        //With the unicode pluging: http://blog.stevenlevithan.com/archives/xregexp-unicode-plugin
        //Yes, this sucks. Unfortunately XRegExp Unicode doesn't support character classes or double negation.
        var matchAlphaNumeric = new XRegExp("[\\p{L}\\p{N}]");
        valueField.val(self.substituteNotAcceptedCharacters($j(this).val(), matchAlphaNumeric));
      }
    });

    $j('fieldset.conditions.add').click(function(event) {
      event.preventDefault();
      self.insertOptionRow('', '', '');
      return false;
    });

    options && options.each(function(field) {
      self.insertOptionRow(field);
    });

    // doing this to counteract Mustache over-escaping backslashes during the template name insertion
    $j("fieldset.conditions .customFieldName").each(function(i,field) {
      field.value = field.value.replace(/\\\\/g, "\\");
    });

    //need this because unchecked boxes aren't submitted as false in update forms
    var hdf = $j('.customFieldHiddenDefault:first').clone();

    $j('.customFieldDefault:checked').each(function() {
      $j(this).siblings().remove('.customFieldHiddenDefault');
    });

    $j('.customFieldDefault').live("change", function() {
      if (this.checked) {
        $j('.customFieldDefault:checked').not(this).each(function() {
          $j(this).click();
        });
        $j(this).siblings().remove('.customFieldHiddenDefault');
      } else {
        hdf.clone().insertBefore(this);
      }
    });
  },

  substituteNotAcceptedCharacters: function(tag, regex){
    var tagWithAlphaAndNumeric = "";
    for (var i=0; i < tag.length; i++) {
      var characterBeingTested = tag.charAt(i);
      if (XRegExp.test(characterBeingTested, regex) == true){
        tagWithAlphaAndNumeric += characterBeingTested.toLowerCase();
      }else {
        tagWithAlphaAndNumeric += "_";
      }
    }
    return tagWithAlphaAndNumeric;
  },

  insertOptionRow: function(field) {
    var self = $z('ticket_fields/_field_tagger');

    field.index = self._nextIndex();
    var newOptionRow = $j.mustache(self.optionRowTemplate(), field);
    self.optionsContainer().append(newOptionRow);
  },

  optionsContainer: function() {
    return $j('#optionsContainer');
  },

  optionRowTemplate: function() {
    var self = $z('ticket_fields/_field_tagger');
    if (self._optionRowTemplate) {
      return self._optionRowTemplate;
    }
    self._optionRowTemplate = $j('#custom-field-fieldset-template').html();
    return self._optionRowTemplate;
  },

  _nextIndex: function() {
    this._nextFieldsetIndex = (this._nextFieldsetIndex || -1) + 1;
    return this._nextFieldsetIndex;
  }
});
