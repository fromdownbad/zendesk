$z.defModule("tags/show", {
  initialize: function(){},

  showTagJobStatus: function(request) {
    var job = request.responseJSON;
    var jobStatus = new Zendesk.JobStatus(job.status_url, {
      title: I18n.t('txt.js.tags.processing_tags'),
      renderResult: function(jobStatus) {
        var output = {
          title: '' + I18n.t('txt.js.tags.processed_tags', {number:jobStatus.results.total}),
          body: $j('<ul/>')
        };

        if (jobStatus.status === 'completed') {
          document.location = '/tags/bulk_result?background=' + jobStatus.job.id;
        }
        return output;
      }
    });
  }
});