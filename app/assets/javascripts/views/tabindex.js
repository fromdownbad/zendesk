$z.defModule('tabindex', {
  addtabindexes: function(){ // easily set the order of tab navigation in a single call
    var args = Array.prototype.slice.call(arguments); // get an array from the arguments object
    var tabindex = args.slice(-1)[0];
    if(typeof(tabindex) === 'number'){
      args.pop();
    } else {
      tabindex = 1;
    }
    $j(args).each(function(index, selector){
      $j(selector).each(function(index, element){
        $j(element).attr({tabindex: tabindex++});
      });
    });
  }
});