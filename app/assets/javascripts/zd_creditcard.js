var ZdCreditCard = {
  submitCreditCardForm : function() {
    $('credit-card-error').hide();
    if(ZdCreditCard.validate()) {
      ZdCreditCard.serializeExpiry();
      $j('#transparent-redirect').submit();
      $('submit-credit-card').disabled = true;
      $('submit-credit-card').value = I18n.t('txt.admin.javascript.zd_creditcard');
    } else {
      $('credit-card-error').show();
    }
    return false;
  },

  validate : function() {
    var country = $F('country');
    var state   = $F('state');
    var zip     = $F('zip');

    if( zip.blank() || country.blank() || zip.blank() ) { return false; }
    if( country == "prompt") { return false; }
    if( country == "US") { if( state == "prompt") { return false; } }
    return true;
  },

  resetCreditCardForm : function() {
    var submitCreditCard = $('submit-credit-card');
    var submitCreditCardInTrial = $$('input#submit-credit-card.trial')[0];
    var termsAgree = $('terms_agree');
    submitCreditCard.disabled = false;

    // for trial form
    if (submitCreditCardInTrial){
      submitCreditCardInTrial.value = I18n.t('txt.public.javascript.zd_creditcard.purchase_my_subscription');
      submitCreditCard.disabled = true;
      termsAgree.observe('click', function(event){
        if(termsAgree.checked === true) {
          submitCreditCard.disabled = false;
        }
        else if (termsAgree.checked === false){
          submitCreditCard.disabled = true;
        }
      });
    }
    else {
      submitCreditCard.value = I18n.t('txt.admin.views.account.credit_card._form.submit_credit_card_button_label');
    }
  },

  showCvvHelper : function() {
    $j.colorbox({href: '/account/subscription/cvv'});
  },

  serializeExpiry : function() {
    var month = $F('expiry_month');
    var year  = $F('expiry_year');
    $('ccexp').value = month + "" + year;
  },

  preloadImages : function() {
   image = new Image();
   image.src = "/images/find-the-cvv.png";
  },

  toggleStateSelect : function() {
    if($('country').value == 'US') {
      $('state-select-row').show();
      $('state').disabled = false;
    } else {
      $('state-select-row').hide();
      $('state').disabled = true;
    };
  }
};
