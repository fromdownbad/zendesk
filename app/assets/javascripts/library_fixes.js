/*globals $, jQuery, Zendesk, Prototype, Effect, Element*/

(function ($) {
  // fix for https://prototype.lighthouseapp.com/projects/8886/tickets/365-elementgetstyle-problem-with-ie-6-7

  if (typeof(Element) !== 'undefined') {
    Element.Methods.originalGetStyle = Element.Methods.getStyle;

    Element.addMethods({
      getStyle: function(element, style) {
        try {
          return Element.Methods.originalGetStyle(element, style);
        } catch(e) {
          return 'static';
        }
      }
    });
  }

  // Turn off Prototype effects for IE browsers
  if (Prototype && Prototype.Browser && Prototype.Browser.IE && Effect) {
    Effect.Fade   = Effect.BlindUp   = Effect.SlideUp   = function(element) { return $(element).hide(); };
    Effect.Appear = Effect.BlindDown = Effect.SlideDown = function(element) { return $(element).show(); };
  }

}(jQuery));
