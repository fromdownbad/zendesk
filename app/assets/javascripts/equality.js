var arrayEquals = function(a, b) {
  if (a.length != b.length) { return false; }
  for (var i = a.length; --i >= 0; ) {
    if ((a[i] instanceof Date) && (b[i] instanceof Date)) {
      if (!dateEquals(a[i], b[i])) {
        return false;
      }
    } else if (a[i] !== b[i]) {
      return false;
    }
  }
  return true;
};

var dateEquals = function(a, b) {
  return (a instanceof Date) &&
         (b instanceof Date) &&
         (a >= b) &&
         (a <= b);
};
