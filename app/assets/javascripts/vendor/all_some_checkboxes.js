/*
  All/Some Checkboxes
  Version 1.0
  
  For use with an "all" checkbox among a group of others.
  
  Example:
  
      <fieldset id='myCheckBoxGroup'>
        <legend>Format</legend>
        <input type='checkbox' name='formats' value='all' class='all' /> All
        <input type='checkbox' name='formats[]' value='vinyl' /> Vinyl
        <input type='checkbox' name='formats[]' value='8track' /> 8-Track
        <input type='checkbox' name='formats[]' value='Tape' /> Tape
        <input type='checkbox' name='formats[]' value='CD' /> <abbr title='Compact Disc'>CD</abbr>
        <input type='checkbox' name='formats[]' value='mp3' /> <abbr title='MPEG-1 Audio Layer 3'>MP3</abbr>
      </fieldset>
      
      $('#myCheckBoxGroup').someAllCheckboxes();   

  LICENSE:
  
  (The MIT License)
  
  Copyright © 2010 Zendesk
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

(function($) {
  
  /* Apply the some/all checkbox functionality a container.
     The container MUST have at least one checkbox
     matching the +allSelector+ and one or more other checkboxes.
     
     @param [Object] options
     
     @option [String] allSelector the selector for the "all"
             checkbox(es) inside the container. Defaults to '.all'
   */
  $.fn.someAllCheckboxes = function(options) {
    var settings = $.extend({
      allSelector:  '.all'
    }, options || {});
    
    // must apply each individually so they don't interact:
    $(this).each(function(index, group) {
      var checkBoxGroup = $(group);
      var allCheckBoxes = checkBoxGroup.find(settings.allSelector + ':checkbox');
      var singleCheckBoxes = checkBoxGroup.find(':not(' + settings.allSelector + '):checkbox');

      checkBoxGroup.bind('someAllCheckboxes:allChecked', function() {
        singleCheckBoxes.attr('checked', false);
      });

      checkBoxGroup.bind('someAllCheckboxes:singleChecked', function() {
        allCheckBoxes.attr('checked', false);
      })

      allCheckBoxes.change(function() {
        if ($(this).is(':checked')) { checkBoxGroup.trigger('someAllCheckboxes:allChecked'); }
      });

      singleCheckBoxes.change(function() {
        if ($(this).is('checked')) { checkBoxGroup.trigger('someAllCheckboxes:singleChecked'); }
      });
    });
    return this;
  };
  
})(jQuery);
