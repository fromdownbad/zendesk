/*global Minilog, navigator, localStorage */

(function() {
  var bb = new Minilog.backends.jQuery({
      interval: 10000,
      url: '/api/logging'
    }),
    now = new Date().getTime(),
    levels = ['info','warn','error'],
    logger = new Minilog('zendesk_minilog'),
    formatter = new Minilog.Transform(),
    min = Math.pow(36, 5),
    max = Math.pow(36, 6);

  Minilog.bb = bb;

  bb.enabled = false;

  bb.extras.cid = Math.floor(Math.random() * (max - min) + min).toString(36);

  formatter.write = function(name, level, args) {
    args = args || [];
    var i, found;
    for(i = 0; i<levels.length; i++) {
      if(levels[i] == level) {
        found = true;
        break;
      }
    }

    if(!found) {
      return;
    }

    var source = args.length > 1 ? { args: args } : args[0];

    if(typeof source !== 'object') {
      source = { message: source };
    }

    source.t = new Date().getTime();

    this.emit('item', name, level, source);
  };

  Minilog
    .pipe(formatter)
    .pipe(bb);

  Minilog.zendeskConfiguration = function(options) {
    bb.jQuery = options.jQuery;
    bb.extras.account = options.subdomain;
    bb.extras.user = options.userId;
    bb.extras.userType = options.userType;
    bb.enabled = true;

    if (options.debug) {
      levels.push('debug');
    }

    if(typeof navigator !== 'undefined' && navigator.userAgent) {
      logger.debug('user agent: '+navigator.userAgent);
    }
  };

  try {
    if (localStorage.minilogSettings) {
      if (localStorage.minilogExpiration) {
        if (localStorage.minilogExpiration < now) {
          delete localStorage.minilogSettings;
          delete localStorage.minilogExpiration;
        }
      } else {
        localStorage.minilogExpiration = now + (24 * 60 * 60 * 1000);
      }
    }
  } catch(e) {}
}());


