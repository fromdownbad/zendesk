(function($) {
  Sammy.ActiveTemplate = function(app) {

    // Render the template using the provided data, and then bind all the DOM
    // events mentioned declaratively in the template.
    var renderTemplate = function(templateElement, data, targetSel) {
      var targetSelector = targetSel || templateElement.attr('data-target');
      var renderContext = this.render(templateElement, data).replace(targetSelector);
      renderContext.then(function() {
        bindDOMEvents(this.$element().find(targetSelector), this);
      }.bind(this));
      return renderContext;
    };

    // Look for declarations of DOM event bindings in markup and create event
    // bindings for each of them. Bindings are declared on DOM elements as
    // "data-bind" attributes on them, the value of the attribute being in the format:
    // `<dom event> <app event to trigger when this dom event happens>`
    var bindDOMEvents = function(container, context) {
      var boundChildren = container.find("*[data-bind]");
      boundChildren.each(function(i, elem) {
        var eventDescriptor = $(elem).attr('data-bind');
        var domEvent = eventDescriptor.split(' ')[0];
        var appEvent = eventDescriptor.split(' ')[1];
        $(elem).bind(domEvent, function(e) {
          context.trigger(appEvent, {element: $(elem), domEvent: e});
        });
      });
    };

    // Same as renderTemplate, but for collections. Here `data` is expected to be
    // an array of objects, each to be interpolated into the passed template in order.
    var renderCollectionTemplate = function(templateElement, data, targetSel) {
      var targetSelector = targetSel || templateElement.attr('data-target');
      var renderContext = this.renderEach(templateElement, data).replace(targetSelector);
      renderContext.then(function() {
        bindDOMEvents(this.$element().find(targetSelector), this);
      }.bind(this));
      return renderContext;
    };

    app.helper('renderTemplate', renderTemplate);
    app.helper('renderCollectionTemplate', renderCollectionTemplate);
    app.helper('bindDOMEvents', bindDOMEvents);

  };
})(jQuery);