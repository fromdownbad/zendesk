(function($) {
  $.widget( "ui.iosMenu", {
    options: {
      backText:         'Back',
      backTextFunction: function(submenu) { return null; },
      slideDuration:    50,
      slideEasing:      'linear',
      click:            function(iosMenu) { iosMenu.toggle(); return false; },
      search:           false
    },

    toggle: function() {
      if(this.container.is(':visible')) {
        this.hide();
      } else {
        this.show();
      }
    },

    hide: function() {
      this.container.hide();
      this.element.hide();
    },

    hideAll: function() {
      $('.ios-menu-container').each(function(idx, element) {
        var widget = $(element).data('iosMenu');
        if(widget) {
          widget.hide();
        }
      });
    },

    show: function() {
      this.element.show();
      this.container.show();
      this._reposition();
    },

    _reposition: function() {
      var title = this.container.parent().find(".ios-menu-title");

      // First, position menu exactly where we want it.
      this.container
        .css('top', title.position().top + 5)
        .css('left', title.position().left + 5);

      if($('#ios-menu-spacer').length == 0) {
        // After positioning, check to see if we're pushing towards the bottom of the window.
        var footer = $('div#footer');
        var bodyHeight = footer.position().top + footer.height();
        var heightAtContainerBottom = this.container.position().top + this.container.height();
        if(heightAtContainerBottom > bodyHeight) {
          // If we've extended beyond the page footer, extend the page's height a bit.
          footer.after(
            $('<div>&nbsp;</div>')
              .attr('id', 'ios-menu-spacer')
              .css('padding-top', 2 * (heightAtContainerBottom - bodyHeight))
          );
        }
      }
    },

    _insertSearchField: function() {
      this.searchField = $('<input class="ios-menu-search-field" type="text"/>');
      $('<span class="ios-menu-search"></span>')
        .append(this.searchField)
        .insertBefore(this.element);

      return this;
    },

    _insertSearchResults: function() {
      this.searchResults = $('<ul class="ios-menu-search-results menu" style="display:none;"></ul>')
        .insertAfter(this.element);
      this.searchResults.menu();

      return this;
    },

    _insertBackButtons: function() {
      var iosMenu = this;

      $(this.element.find('li ul, li ol')).each(function(idx, submenu) {
        var backButton = $( '<li>' +
           '<span class="ui-icon ui-icon-carat-1-w ios-menu-back-image"></span>' +
           '<a href="#menu-back" class="ios-menu-back-link">' +
           (iosMenu.options.backTextFunction(submenu) || iosMenu.options.backText) +
           '</a>' +
            '</li>'
        );
        $(submenu).prepend(backButton);
      });
      return this;
    },

    // Create an encapsulating container for the prerendered nested list.
    //   Insert the dialog's title if configured to do so.
    _createContainer: function() {
      var iosMenu = this;
      var container = this.container = $('<div class="ios-menu-container" />')
        .append(
          $('<div>')
            .addClass("ios-menu-title activated")
            .html(this.options.title)
            .append(
              $('<a>')
                .addClass('button')
                .click(function() { iosMenu.toggle() })
                .css("float", "right")
                .css("padding", "0 5px")
                .css("margin",  "0 5px")
                .html('cancel')));

      container
        .insertBefore(this.element)
        .append(this.element.detach())
        .data('ios-menu', this)
        .hide();

      if(this.options.title) {
        $('<div class="ios-menu-title"/>')
          .append($('<a>').html(this.options.title)
            .click(function(event) {
              iosMenu.hideAll();
              iosMenu.toggle();
            }))
          .insertBefore(container)

         container.css("top", "-15px");
      };
      return this;
    },

    // Wrap <li> text content in anchor tags,
    // attach onclick event handler.
    _createLinks: function() {
      var iosMenu = this;

      iosMenu.element.find('li').each(function(idx, listItem) {
        listItem = $(listItem);

        var nestedList = $(listItem).find('> ul').detach();
        var linkText   = listItem.text();

        listItem
          .empty()
          .append($('<a/>').append(linkText).click(function() {
            if(nestedList.length > 0) {
              return true;
            } else {
              return iosMenu.options.click(iosMenu, listItem);
            }
          }))
          .append(nestedList)
      });
      return this;
    },

    _create: function( ) {
      var iosMenu = this;

      iosMenu
        ._createContainer()
        ._createLinks();

      if(this.options.search) {
        iosMenu
          ._insertSearchField()
          ._insertSearchResults();
      }

      if(this.options.renderSearchResult) {
        this.renderSearchResult = this.options.renderSearchResult;
      } else {
        this.renderSearchResult = function(hit) {
          return "<li><a>" + hit + "</a></li>";
        };
      }

      iosMenu
        ._insertBackButtons()
      .element
        .addClass( 'ios-style' )
        .menu({
          // When a submenu shows up, place it just to the right
          // of the current menu. Later, we'll slide it into view.
          position: {
            my: 'left top',
            at: 'right top',
            of: iosMenu.element
          }
        });

      $j(window).resize(function() { iosMenu._reposition() } );
      var menu = iosMenu.element.data( 'menu' );

      // Override menu#select to account for nesting and back buttons:
      menu.select = function( event ) {
        event.preventDefault();

        var validTarget = $(event.target).is('a');
        if( validTarget && menu.active && menu.active.find( '> .ios-menu-back-link' ).length ) {
          // if you selected "back", go back:
          menu.focus( event, menu.active );
          if ( menu.left( event ) ) {
            event.stopImmediatePropagation();
          }
        } else if ( validTarget && menu.active && menu.active.find( '> ul' ).length ) {

          // if you selected something with children, show the children:
          menu.focus( event, menu.active );
          if ( menu.right( event ) ) {
            event.stopImmediatePropagation();
          }

          menu.active.closest('ul').css('overflow-y', 'hidden');

        } else {
          menu._trigger( 'select', event, { item: menu.active } );
        }
      };

      //Override right to manage menu heights and overflows.
      var right = menu.right;

      menu.right = function(event) {

        if(right.call( menu, event )) {
          event.stopImmediatePropagation();
        }

        menu.active.closest('ul')
          .css('overflow-y', 'hidden');

        menu.active.find('ul')
          .height(menu.active.parent().height());
      };

      // Override menu#left to enable sliding behavior:
      menu.left = function( event ) {
        var newItem = this.active && this.active.parents( 'li:not(.ui-menubar-item) ').first(),
        self= this,
        parent;
        if ( newItem && newItem.length ) {
          newItem.find( '> a' ).addClass( 'ui-state-focus' ).removeClass( 'ui-state-active' );
          parent = this.active.parent();
          parent
            .attr( 'aria-hidden', 'true' )
            .attr( 'aria-expanded', 'false' )
            .animate({
              left: self.element.css( 'width' )
            }, iosMenu.options.slideDuration, iosMenu.options.slideEasing, function() {
              parent.hide();
              self.focus( event, newItem );
            });

          newItem.closest('ul').css('overflow-y', 'auto');

          return true;
        } else if ( event && event.which === $.ui.keyCode.ESCAPE ) {
          // #left gets called both for left-arrow and escape. If it's the
          // latter and we're at the top, fire a "close" event:
          self._trigger( 'close', event );
        }
      };

      // Override menu#_open to enable sliding behavior:
      var menuOpenWithoutSliding = menu._open;
      menu._open = function ( submenu ) {
        menuOpenWithoutSliding.call( this, submenu );
        submenu.animate({
          left: 0
        }, iosMenu.options.slideDuration, iosMenu.options.slideEasing);
      };

      // Override menu#_startOpening so that hovering doesn't
      // initiate the sliding:
      menu._startOpening = function() {
        clearTimeout( this.timer );
      };

      // Override default _move to trigger events when at the top/bottom of a menu.
      menu._move = function(direction, edge, filter, event) {
        if ( !this.active ) {
          this.focus( event, this.activeMenu.children(edge)[filter]() );
          return;
        }
        var next = this.active[ direction + "All" ]( ".ui-menu-item" ).eq( 0 );
        if ( next.length ) {
          this.focus( event, next );
        } else {
          var currentActive = this.active;
          this.focus( event, this.activeMenu.children(edge)[filter]() );
          this._trigger('wrap', event, {
            direction: direction,
            from: currentActive,
            menu: this
          });
        }
      };

      menu.focusFirst = function() {
        this.focus({}, this.activeMenu.find('>li:first'));
      };

      if(this.options.search) {
        var resultsMenu = this.searchResults.data('menu');
        resultsMenu._move = menu._move;
        resultsMenu.focusFirst = menu.focusFirst;

        this.searchField
          .keyup(function(event) {
            switch(event.keyCode) {
            case $.ui.keyCode.DOWN:
              iosMenu.visibleMenu.focus();
              iosMenu.visibleMenu.data('menu').focusFirst();
              return false;
            case $.ui.keyCode.ESCAPE:
              iosMenu.searchField.val('');
              iosMenu.showMenu();
              return false;
            }
          })
          .fastAutocomplete({
            lookup: this.doSearch,
            render: this.showSearchResults,
            context: this,
            frequency: 0.1
          });

        this.visibleMenu = this.element;

        this.element.add(this.searchResults).bind('menuwrap', function(event, data) {
          if(data.direction === "prev") {
            data.menu.focus({}, data.from);
            data.menu.blur();
            iosMenu.searchField.focus();
          }
        });
      }

    },

    doSearch: function(query, callback) {
      if(this.options.search) {
        callback(this.options.search(query));
      }
    },

    showSearchResults: function(hits) {
      var iosMenu = this;

      if(hits.length === 0) {
        return this.showMenu();
      }

      var len = hits.length;
      var content = [];
      for(var idx = 0; idx < len; idx++) {
        content.push(this.renderSearchResult(hits[idx]));
      }

      this.searchResults
        .empty()
        .html(content.join(''))
        .data('menu').refresh();

      this.searchResults.find('li a').each(function(element) {
        element.click(this.options.click(iosMenu, element.parent));
      });

      this.element.hide();
      this.searchResults.show();
      this.visibleMenu = this.searchResults;

      return this;
    },

    showMenu: function() {
      this.searchResults.hide();
      this.element.show();
      this.visibleMenu = this.element;
    },

    destroy: function() {
      var menu = this.element && this.element.data( 'menu' );
      if(menu) { menu.destroy(); }

      menu = this.searchResults && this.searchResults.data( 'menu' );
      if(menu) { menu.destroy(); }

    }
  });
}(jQuery));
