(function($) {
  Sammy.Mustache = function(app, method_alias) {

    // *Helper* Uses jquery.mustache.js to parse a template and interpolate and work with the passed data
    //
    // ### Arguments
    //
    // * `template` A String template. {{}} Tags are evaluated and interpolated by Mustache.js
    // * `data` An Object containing the replacement values for the template.
    //   data is extended with the <tt>EventContext</tt> allowing you to call its methods within the template.
    // * `partials` An Object containing one or more partials (String templates
    //   that are called from the main template).
    //
    var mustache = function(template, data, partials) {
      data     = $.extend({}, this, data);
      partials = $.extend({}, data.partials, partials);
      return $.mustache(template, data, partials);
    };

    // set the default method name/extension
    if (!method_alias) method_alias = 'mustache';
    app.helper(method_alias, mustache);

  };
})(jQuery);