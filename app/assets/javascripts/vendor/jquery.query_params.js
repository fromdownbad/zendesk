jQuery.extend({

  // Usage:
  //   var params = jQuery.queryParameters();
  //   if (params.filter === "foo") { ... }
  //
  // @see http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html:
  queryParameters: function() {
    var result = {},
        queryString = ( document.location.search || '' ).slice( 1 ),
        keyValuePairs = queryString.split( '&' ),
        keyAndValue, key, value;

    for ( var i = 0; i < keyValuePairs.length; i++ ) {
      keyAndValue = keyValuePairs[ i ].split( '=' );
      key = keyAndValue[ 0 ];
      value = decodeURIComponent((keyAndValue[ 1 ] || '').replace( /\+/g, " " ));
      result[ key ] = value;
    }

    return result;
  }

});
