/*global window, jQuery, $, Minilog */
// Browser implementation for reporting metrics

(function() {

  var logger = new Minilog('zendesk_reporter');

  var jquery;
  if(typeof jQuery !== 'undefined') {
    jquery = jQuery;
  } else if(typeof $ !== 'undefined') {
    jquery = $;
  } else {
    logger.error('Could not find jQuery. Reporting is disabled.');
  }

  function Batch(tags, flushInterval, url) {
    logger.debug('new batch');
    this._tags = tags;
    this._url = url;
    this._metrics = [];

    if(!Batch.batches[url]) {
      Batch.batches[url] = {
        flushInterval: flushInterval,
        list: []
      };
    }

    Batch.batches[url].list.push(this);

    if(flushInterval) {
      Batch.registerInterval(this._url, flushInterval);
    }
    Batch.planForNextFlush(this._url);

  }

  Batch.prototype.tags = function() {
    return this._tags || [];
  };

  Batch.prototype.metrics = function() {
    return this._metrics;
  };

  /** register a new interval
   *
   * since there could be multiple reporters instantiated and sending to the same URL, it is necessary to not
   * flush the data multiple times but to batch both reporters' data and send them at the minimum registered
   * interval.
   *
   * @param url
   * @param interval in milliseconds
   */
  Batch.registerInterval = function(url, interval) {

    if(!Batch.intervals) {
      Batch.intervals = {};
    }

    if(!Batch.intervals[url]) {
      Batch.intervals[url] = [];
    }

    Batch.intervals[url].push(interval);

    // only need to compare with the existing interval which should already be the min of the registered ones
    // or replace the default with the new value
    if(Batch.intervals[url].length > 1) {
      Batch.batches[url].flushInterval = Math.min.apply(this, Batch.intervals[url]);
      logger.debug('registerInterval', Batch.batches[url].flushInterval, Batch.intervals[url]);
    } else {
      Batch.batches[url].flushInterval = interval;
      logger.debug('registerInterval', Batch.batches[url].flushInterval);
    }

  };

  Batch.prototype.add = function(metric) {
    this._metrics.push(metric);
    if(!this.currentFlushTimeoutId) {
      Batch.planForNextFlush(this._url);
    }
  };

  Batch.prototype.flush = function() {
    Batch.flush(this._url);
  };

  Batch.DEFAULT_INTERVAL = 10 * 1000;
  Batch.batches = {};

  Batch.planForNextFlush = function(url) {
    var batch = Batch.batches[url];

    var flushInterval = batch && batch.flushInterval || Batch.DEFAULT_INTERVAL;

    if(batch && !batch.currentFlushTimeoutId) {
      logger.debug('flushing ['+url+'] in '+flushInterval+'ms');
      batch.currentFlushTimeoutId = setTimeout(function() {
        batch.currentFlushTimeoutId = null;
        logger.debug('flushing ['+url+']');
        Batch.flush(url);
      }, flushInterval);
    }
  };

  /** take the batched metrics and send it to the server */
  Batch.flush = function(url) {
    var batchInfo = Batch.batches[url];
    logger.debug('batch ['+url+'] has ['+batchInfo.list.length+'] items');
    if(jquery && batchInfo && batchInfo.list && batchInfo.list.length > 0) {

      var payload = '';
      var hasMetrics = false;

      for(var i = 0 ; i < batchInfo.list.length ; i++) {
        var batchItem = batchInfo.list[i];

        payload += JSON.stringify({tags: batchItem.tags()}) + '\n';

        while(batchItem.metrics().length > 0) {
          hasMetrics = true;
          var metric = batchItem.metrics().shift();
          payload += JSON.stringify(metric) + '\n';
        }
      }
      logger.debug('batch ['+url+'] contains ' + payload);

      if(hasMetrics) {
        jquery.post(url, payload)
          .fail(function(xhr) {
            logger.error('reporting request failed with status ['+xhr.status+'] and error ['+xhr.responseText+']');
          })
          .always(function() {
            Batch.planForNextFlush(url);
          });
      } else {
        logger.debug('batch ['+url+'] has no metrics');
      }
    }
  };

  function ZendeskReporter(tags, flushInterval, url) {

    if(!url) {
      url = '/api/reporting';
    }

    var batch = new Batch(tags, flushInterval, url);

    this.increment = function(name, value, tags) {
      this.queue('increment', name, value, tags);
    };

    this.gauge = function(name, value, tags) {
      this.queue('gauge', name, value, tags);
    };

    this.histogram = function(name, value, tags) {
      this.queue('histogram', name, value, tags);
    };

    this.timing = function(name, value, tags) {
      this.queue('timing', name, value, tags);
    };

    this.queue = function(type, name, value, tags) {
      if(jquery) {
        logger.debug('queue ' + type + ' ' + name + ' ' + value + ' ' + tags);
        batch.add([type, name, value, tags]);
      }
    };

    this.flush = function() {
      batch.flush();
    };

  }

  if(typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = ZendeskReporter;
  } else {
    window.ZendeskReporter = ZendeskReporter;
  }

}());
