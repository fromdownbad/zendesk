/*globals Zendesk, I18n, _, AceSetup, InputSuggest, ruleEditingCapability, ruleActionEditingCapability*/

//= require admin/ace/ace_setup
//= require admin/input_text_autocomplete

Zendesk.NS('RuleUI', this.jQuery, this.ace, function($, ace) {
  this.ruleSetCounter = 0;

  this.unescapeHtml = function(unsafe) {
    return unsafe
      .replace(/&amp;/g, "&")
      .replace(/&lt;/g, "<")
      .replace(/&gt;/g, ">")
      .replace(/&quot;/g, "\"")
      .replace(/&#0?39;/g, "'");
  };

  this.nestedName = function(keys) {
    var resultBuffer = '';
    var i;
    for(i=0; i<keys.length; i++) {
      resultBuffer += (i === 0 ? keys[i] : '[' + keys[i] + ']');
    }

    return resultBuffer;
  };

  this.RuleSet = function(conditionDefs, setID) {
    if ( setID === undefined ) {
      setID = ++Zendesk.RuleUI.ruleSetCounter;
    }
    this.setID = setID;
    this.conditionDefs = conditionDefs;
    this.conditionRowCount = 0;
    this.actionRowCount = 0;
  };

  this.ComponentRow = function(ruleSet, index, target, operator, values) {
    this.index = index;
    this.ruleSet = ruleSet;

    if ( target ) {
      this.target = target.split("#")[0];
    } else {
      this.target = null;
    }

    this.operator = operator;

    if ( values ) {
      for(var i=0 ; i < values.length ; i++) {
        if( typeof values[i] == "string" ) {
          values[i] = Zendesk.RuleUI.unescapeHtml(values[i]);
        }
      }
      this.value = values[0];
      this.values = values;
    } else {
      this.value = null;
      this.values = [];
    }

    this.targetSelect = null;
    this.operatorSelect = null;
    this.valueContainer = null;
  };

  $.extend(this.ComponentRow.prototype, {
    conditionDefinitionFromSelect: function(select) {
      if ( !select )
        return null;

      if ( select.attr("type") === "hidden" )
        return select.data('ruleDef');
      else
        return select.find("option:selected").first().data('ruleDef');
    },

    stashValues: function() {
      if ( this.operatorSelect )
        this.operator = this.operatorSelect.val();
      this.values = [];
      this.value = null;
    },

    buildTargetSelect: function() {
      this.targetSelect = $("<select class='chosen_dropdown'>").attr('name', this.elementBaseName + "[source]").css("width", "40%");
      if(!ruleEditingCapability)
        this.targetSelect.attr('disabled', 'disabled');

      var currentGroup = null;
      var currentOptions = null;

      _(this.ruleSet.conditionDefs).each(function(condition) {
        if(condition.group != currentGroup) {
          if(currentGroup) this.targetSelect.append(currentOptions);
          currentGroup = condition.group;
          this.targetSelect.append($('<option>').text("--------------------------").attr('disabled','disabled'));
        }

        var option = $('<option>')
          .val(condition.value)
          .prop('selected', condition.value === this.target)
          .data('ruleDef', condition);

        if(currentGroup) {
          var currentGroupTitle = I18n.t("txt.admin.public.javascript.new_rule.groups." + currentGroup) || "Group";
          option.text(currentGroupTitle + ': ' + condition.title);
        } else {
          option.text(condition.title);
        }
        this.targetSelect.append(option);
      }.bind(this));
      this.targetSelect.append(currentOptions);

      this.targetSelect.change(function() {
        this.stashValues();

        if ( this.operatorSelect )
          this.operatorSelect.remove();

        if ( this.valueContainer )
          this.valueContainer.remove();

        var def = this.conditionDefinitionFromSelect(this.targetSelect);
        this.targetSelect.after(this.buildOperatorSelect(def));
        this.operatorSelect.trigger('change');
      }.bind(this));

      if ( this.target == null ) {
        this.targetSelect.children().first().prop('selected', true);
      }
      return this.targetSelect;
    },

    buildOperatorSelect: function(conditionDef) {
      var el;

      if ( !conditionDef.operators || conditionDef.operators.length === 0 ) {
        return;
      }

      if ( conditionDef.operators.length == 1 ) {
        var op = conditionDef.operators[0];

        if ( !op.values )
          op.values = conditionDef.values;

        el = $("<input type='hidden'>").attr("name", this.elementBaseName + "[operator]").val(op.value);
        el.data('ruleDef', op);
      } else {
        el = $("<select>").attr("name", this.elementBaseName + "[operator]");

        _(conditionDef.operators).each(function(op) {
          // propogate down values from the condition
          if ( !op.values )
            op.values = conditionDef.values;

          el.append($('<option>').val(op.value).prop('selected', this.operator == op.value).data('ruleDef', op).text(op.title));
        }.bind(this));
      }

      el.change(function() {
        this.stashValues();

        if ( this.valueContainer )
          this.valueContainer.remove();

        var def = this.conditionDefinitionFromSelect(this.operatorSelect);
        this.operatorSelect.after(this.buildValueElement(def, this.value));

        this.buildEmailAceEditors();

        var actionContainer =  this.operatorSelect.parent();

        if (this.targetSelect.find(':selected').val() === 'comment_value') {
          new InputSuggest(actionContainer.find('textarea'));
        }

        actionContainer.find('.target_select').change();
        actionContainer.find('.parameter input').each(function() {
          new InputSuggest($(this));
        });
      }.bind(this));
      if(!ruleEditingCapability)
        el.attr('disabled', 'disabled');

      this.operatorSelect = el;
      return el;
    },

    buildEmailAceEditors: function() {
      $(".email_editor").each(function() {
        var textArea = $(this),
          subjectInput = textArea.siblings("input.text").first();

        new InputSuggest(subjectInput);
        new InputSuggest(textArea);
      });
    },

    buildValueSelect: function(operatorDef) {
      var el, span;
      var elName = this.elementBaseName + "[value][0]";
      var anySelected = false;

      if ( !operatorDef || operatorDef.values.list.length === 0 ) {
        return;
      } else {
        span = $("<span>");
        el = $("<select>").attr("name", elName);

        if(!ruleEditingCapability)
          el.attr('disabled', 'disabled');

        _(operatorDef.values.list).each(function(value) {
          var unescapedValueTitle = Zendesk.RuleUI.unescapeHtml(value.title);
          var selected = (this.value == value.value);
          anySelected = anySelected || selected;

          el.append(
            $("<option>")
              .val(value.value)
              .prop("selected", selected)
              .text(unescapedValueTitle));
        }.bind(this));

        this.addMissingCurrentOptionToSelect(el, anySelected);

        span.append(el);
        el.autocompleteFromSelectIfLarge();

        // autocompleteFromSelect believes we're hidden and so hides itself.  <shrug>
        span.children("span").show();
      }

      this.valueContainer = span;

      return span;
    },

    addMissingCurrentOptionToSelect: function(el, anySelected) {
      if(!anySelected && this.value && this.value !== "") {
        var option = $("<option>").
          prop("selected", true).
          text(I18n.t('txt.admin.public.javascript.zd_rule.inactive_agent'));
        el.append(option);
      }
    },

    buildValueTextInput: function(operatorDef) {
      var span = $("<span>");
      var input = $('<input type="text" class="text">').attr("name", this.elementBaseName + "[value][0]").val(this.value);

      if(!ruleEditingCapability)
        input.attr('disabled', 'disabled');

      span.append(input);

      if ( operatorDef.values.placeholder ) {
        input.attr('placeholder', operatorDef.values.placeholder);
      }

      if ( operatorDef.values.css_class ) {
        input.addClass(operatorDef.values.css_class);
      }

      this.valueContainer = span;
      return span;
    },

    buildValueDateInput: function(operatorDef) {
      var span = this.buildValueTextInput(operatorDef);
      if(!operatorDef.field_type) {
        span.find('input').datepicker({ dateFormat: "yy-mm-dd" });
      }
      return span;
    },

    buildValueCustomDateInput: function(operatorDef) {
      var span;
      var textBox = $('<input type="text" class="text">').attr("name", this.elementBaseName + "[value][1]").val(this.values[1]);

      operatorDef.values.list = [
        {
          title: I18n.t('txt.admin.public.javascript.new_rule.specific_date'),
          value: "specific_date"
        },
        {
          title: I18n.t('txt.admin.public.javascript.new_rule.days_from_now'),
          value: "days_from_now"
        }
      ];

      span = this.buildValueSelect(operatorDef);

      $(span).find('select').change(function(eventObj) {
        if ($(eventObj.currentTarget).val() === 'days_from_now') {
          textBox.datepicker('destroy');
        } else {
          textBox.datepicker({ dateFormat: "yy-mm-dd" });
        }
      })
      .trigger('change');

      span.append(textBox);
      this.valueContainer = span;

      return span;
    },

    buildValueAutocomplete: function(operatorDef) {
      // for autocomplete, we expect a uniform endpoint that can respond to either "term=" or "id=" queries
      // and returns a value/label pair.

      var span = $('<span>').attr('class', 'ui-widget');
      var input = $('<input>');
      var hidden = $("<input type='hidden'>").attr("name", this.elementBaseName + "[value][0]");

      input.blur(function() {
        var keyEvent = $.Event("keydown");
        keyEvent.keyCode = $.ui.keyCode.ENTER;
        $(this).trigger(keyEvent);
      })
      .autocomplete({
        autoFocus: true,
        source: operatorDef.values.url,
        select: function(event, ui) {
          hidden.val(ui.item.value);
          input.val(ui.item.label);
          return false;
        },
        focus: function(event, ui) {
          if (event.charCode !== undefined) {
            hidden.val(ui.item.value);
            input.val(ui.item.label);
          }
          return false;
        }
      });

      if ( this.value ) {
        var cachedVal;
        var cacheKey = {url: operatorDef.values.url, id: this.value};
        if ( this.autoCompleteCacheByID && (cachedVal = this.autoCompleteCacheByID[cacheKey]) ) {
          hidden.val(this.value);
          input.val(cachedVal);
        } else {
          $.getJSON(operatorDef.values.url, {id: this.value}, function(data) {
            hidden.val(data.value);
            input.val(data.label);
            if ( !this.autoCompleteCacheByID )
              this.autoCompleteCacheByID = {};

            this.autoCompleteCacheByID[cacheKey] = data.label;
          }.bind(this));
        }
      }

      span.append(input).append(hidden);
      this.valueContainer = span;

      return this.valueContainer;
    },

    buildMinusButton: function() {
      var el = $("<a>").addClass("minus-button").prop("href", "#");
      el.click(function() {
        this.container.remove();
        return false;
      }.bind(this));

      return el;
    },

    build: function() {
      this.container = $('<fieldset>').addClass('conditions').attr("id", this.containerName);
      if(ruleEditingCapability)
        this.container.append(this.buildMinusButton());


      this.container.append(this.buildTargetSelect(this.target))
                    .append(this.buildOperatorSelect(this.conditionDefinitionFromSelect(this.targetSelect), this.operator))
                    .append(this.buildValueElement(this.conditionDefinitionFromSelect(this.operatorSelect), this.value));

      return this.container;
    }
  });

  this.ConditionRow = function(ruleSet, index, target, operator, values) {
    Zendesk.RuleUI.ComponentRow.call(this, ruleSet, index, target, operator, values);
    this.containerName = [ this.ruleSet.setID, "condition", this.index ].join('_');
    this.elementBaseName = Zendesk.RuleUI.nestedName(['sets', this.ruleSet.setID, "conditions", this.index]);
  };

  $.extend(this.ConditionRow.prototype, this.ComponentRow.prototype, {
    extractLabel: function(operatorDef) {
      var container = (operatorDef.values.label || operatorDef.values.labels);
      //resolve labels by i18n if possible
      if ( container ) {
        var lb = container[operatorDef.field_type || "default"];
        return {
          value: ( I18n.translations[lb] !== undefined ) ? I18n.t(lb) : lb,
          wrapper: "span"
        };
      }
    },

    handleBlockLabel: function(label) {
      var is_block  = /^html_block /;

      if ( label.value.match(is_block) ) {
        label.wrapper = "p";
        label.value   = label.value.replace(is_block, "");
      }
      return label;
    },

    buildElementLabel: function(operatorDef) {
      var label = this.extractLabel(operatorDef);
      if ( label && label.value ) {
        label = this.handleBlockLabel(label);
        return $("<" + label.wrapper + " class='rule-input-label'>").html(label.value);
      }
    },

    buildValueElement: function(operatorDef) {
      if ( !operatorDef || !operatorDef.values )
        return;

      var container;
      switch ( operatorDef.values.type ) {
        case "list":
          container = this.buildValueSelect(operatorDef);
          break;
        case "text":
          container = this.buildValueTextInput(operatorDef);
          break;
        case "date":
          container = this.buildValueDateInput(operatorDef);
          break;
        case "autocomplete":
          container = this.buildValueAutocomplete(operatorDef);
          break;
        default:
          return;
      }

      if (container) { return container.append(this.buildElementLabel(operatorDef)); }
    }
  });

  this.ActionRow = function(ruleSet, index, target, values) {
    Zendesk.RuleUI.ComponentRow.call(this, ruleSet, index, target, null, values);
    this.containerName = [ this.ruleSet.setID, "action", this.index ].join('_');
    this.elementBaseName = Zendesk.RuleUI.nestedName(['sets', this.ruleSet.setID, "actions", this.index]);

  };

  $.extend(this.ActionRow.prototype, this.ComponentRow.prototype, {
    buildTextArea: function(index) {
      var textArea = $('<textarea class="text large" dir="auto">')
        .attr("name", this.elementBaseName + "[value][" + index + "]");

      if (!(this.values[index] instanceof Array)) {
        textArea.val(this.values[index]);
      }

      if(!ruleEditingCapability)
        textArea.attr('disabled', true);

      return textArea;
    },

    buildTargetAceEditor: function(textArea, targetInput, contentType) {
      var actionIndex = this.elementBaseName.replace(/\D/g, '');
      var resizableContainer = $('<div class="resizable">');
      var aceEditor = $('<pre id="editor' + actionIndex + '" class="target_editor editor">')
        .data("content-type", contentType);

      resizableContainer.append(aceEditor);
      targetInput.append(resizableContainer);

      if ($('#editor' + actionIndex).length !== 0) {
        var mode;
        if (contentType === "application/xml") {
          mode = "zendesk_xml";
        } else {
          mode = "zendesk_json";
        }

        AceSetup(aceEditor.attr('id'), resizableContainer, textArea, mode, resizableContainer.parent().parent().width(), 'placeholder.');
      }
    },

    buildRemoveParameterButton: function() {
      return $("<a class='remove-parameter'>")
        .html("-")
        .click(function() {
          $(this).parent().remove();
          $(".parameter").each(function(index) {
            $(this).children(".text").each(function(){
              var input =  $(this);
              var newName = input.attr("name").replace(/\d+\]\[[01]\]$/i, function(match) {
                return match.replace(/^\d+/, index.toString());
              });
              input.attr("name", newName);
              input.data("parameter-index", index);
            });
          });
        });
    },

    buildParameterDiv: function(parameterIndex, values) {
      var div =  $("<div class='parameter'>");
      var keyInput = $('<input type="text" class="parameter-key text">')
        .data("parameter-index", parameterIndex)
        .attr("name", this.elementBaseName + "[value][1][" + parameterIndex + "][0]")
        .attr("placeholder", I18n.t("txt.admin.public.javascript.zd_rule.url_parameters_key"));
      var valueInput = $('<input type="text" class="parameter-value text">')
        .data("parameter-index", parameterIndex)
        .attr("name", this.elementBaseName + "[value][1][" + parameterIndex + "][1]")
        .attr("placeholder", I18n.t("txt.admin.public.javascript.zd_rule.url_parameters_value"));

      if (values) {
        valueInput.val(Zendesk.RuleUI.unescapeHtml(values[parameterIndex][1]));
        keyInput.val(Zendesk.RuleUI.unescapeHtml(values[parameterIndex][0]));
      }

      div.append(keyInput);
      div.append(valueInput);
      div.append(this.buildRemoveParameterButton());
      return div;
    },

    buildParameterInput: function(parameterIndex, buildFromValues) {
      var values = this.values[1];
      var parameters = $("<div class='parameters'>");
      if (buildFromValues && values && values instanceof Array) {
        for (var i = 0, l = values.length; i < l; i++) {
          parameters.append(this.buildParameterDiv(i, values));
        }
      } else {
        parameters.append(this.buildParameterDiv(parameterIndex, null));
      }

      var self = this;
      this.addParameterButton.click(function(){
        parameters.append(self.buildParameterDiv($(".parameter").length, null));
        return false;
      });

      return parameters;
    },

    buildLabel: function(i18nKey)  {
      return $("<label class='newline'>").html(I18n.t(i18nKey));
    },

    buildPlaceHolder: function() {
      var content = $("#placeholder-list").html();
      var div = $("<div class='placeholder-info'>");
      var anchor = $("<a>").html(I18n.t('txt.admin.public.javascript.zd_rule.view_available_placeholders_label'));
      var infoDiv = $("<div class='placeholder-list'>").html(content);

      anchor.click(function() {
        infoDiv.toggle();
      });

      div.append(anchor);
      div.append(infoDiv);
      return div;
    },

    buildValueTextAreaInput: function (operatorDef, index) {
      var span = $("<span>");
      var selectBox = $("<select>");
      index = index || this.values.length - 1; // a textarea value is always last

      span.append(this.buildLabel('txt.admin.public.javascript.zd_rule.text_label'));
      span.append(this.buildTextArea(index));
      span.append(this.buildPlaceHolder());

      this.valueContainer = span;
      return span;
    },

    buildValueSelectAndTextAreaInput: function(operatorDef){
      var span = $("<span>");

      if(this.values.length == 1) { // this action previously had no select
        this.values.unshift(operatorDef.values.list[0].value);
        this.value = this.values[0];
      }

      // remove disabled values *unless* they are currently selected
      operatorDef.values.list = operatorDef.values.list.filter(function(value){
        return value.enabled || value.value == this.value;
      }.bind(this));

      // render hidden if the number of options is 2 or less
      // 2 options would be "All" and "Web", unnecessary choice.
      if (operatorDef.values.list.length <= 2) {
        span.append($('<span class="hidden">').append(this.buildValueSelect(operatorDef)));
      } else {
        span.append(this.buildValueSelect(operatorDef));
      }

      span.append(this.buildValueTextAreaInput(operatorDef, 1));

      this.valueContainer = span;

      return span;
    },

    buildEmailInput: function (operatorDef) {
      var span = $("<span>");
      var selectBox = this.buildValueSelect(operatorDef);

      var subjectInput = $('<input type="text" class="text">')
        .attr("name", this.elementBaseName + "[value][1]")
        .val(this.values[1]);

      var bodyInput = this.buildTextArea(2);
      bodyInput.addClass("email_editor");

      if (ruleActionEditingCapability) {
        bodyInput.removeAttr('disabled');
      }

      if(!ruleEditingCapability && !ruleActionEditingCapability) {
        subjectInput.attr('disabled', 'disabled');
        bodyInput.attr('disabled', true);
      }

      span.append(selectBox);
      span.append(this.buildLabel('txt.admin.public.javascript.zd_rule.email_subject_label'));

      span.append(subjectInput);
      span.append(this.buildLabel('txt.admin.public.javascript.zd_rule.email_body_label'));
      span.append(bodyInput);
      span.append(this.buildPlaceHolder());

      this.valueContainer = span;
      return span;
    },

    buildSmsInput: function (operatorDef) {
      var span = $('<span>');

      $.ajax({
        url: '/api/v2/channels/voice/phone_numbers.json?sms_enabled=true',
        dataType: 'json',
        async: false,
        success: function(data) {
          var selectBox = this.buildValueSelect(operatorDef);
          var fromNumberSelectBox = $('<select></select>')
                .attr('name', this.elementBaseName + '[value][1]');

          $.each(data.phone_numbers, function(index,phone_number) {
            var option = $('<option></option>')
                  .attr('value', phone_number.id)
                  .text(phone_number.name);

            if (phone_number.id == this.values[1]) {
              option.attr('selected', 'selected');
            }

            fromNumberSelectBox.append(option);
          }.bind(this));

          var bodyInput = this.buildTextArea(2);
          bodyInput.addClass('email_editor');

          if (ruleActionEditingCapability) {
            bodyInput.removeAttr('disabled');
          }

          if(!ruleEditingCapability && !ruleActionEditingCapability) {
            bodyInput.attr('disabled', true);
          }

          span.append(selectBox);
          span.append(this.buildLabel('txt.admin.public.javascript.zd_rule.sms_from_label'));
          span.append(fromNumberSelectBox);
          span.append(this.buildLabel('txt.admin.public.javascript.zd_rule.sms_new_body_label'));
          span.append(bodyInput);
          span.append(this.buildPlaceHolder());
        }.bind(this)
      });

      this.valueContainer = span;
      return span;
    },

    buildAddParameterButton: function() {
      this.addParameterButton = $("<button class='add-parameter button btn-default'>")
        .html(I18n.t("txt.admin.public.javascript.zd_rule.url_parameters_add_parameter"));
      return this.addParameterButton;
    },

    buildTargetInputSelect: function (operatorDef, messageDiv) {
      var targetSelect = $("<select class='target_select'>").attr("name", this.elementBaseName + "[value][0]");
      var anySelected = false;

      _(operatorDef.values.list).each(function(value) {
        var unescapedValueTitle = Zendesk.RuleUI.unescapeHtml(value.title);
        var selected = (this.value == value.value);
        anySelected = anySelected || selected;
        var option = $("<option>")
          .val(value.value)
          .prop("selected", selected)
          .text(unescapedValueTitle)
          .data("showMessage", !!value.include_message)
          .data("v2", !!value.v2)
          .data("method", value.method)
          .data("content-type", value.content_type);

        targetSelect.append(option);
      }.bind(this));

      this.addMissingCurrentOptionToSelect(targetSelect, anySelected);

      var self = this;
      targetSelect.change(function() {
        var target = $("option:selected", targetSelect);
        var label = messageDiv.children("label.newline");
        var targetInput = messageDiv.children(".target-input");

        if (target.data("v2")) {
          var method = target.data("method");
          var contentType = target.data("content-type");

          if ((method !== "get" && contentType !== "application/x-www-form-urlencoded")) {
            var textArea = self.buildTextArea(1).hide();
            targetInput.html(textArea);
            self.buildTargetAceEditor(textArea, targetInput, contentType);

            label.html(I18n.t('txt.admin.public.javascript.zd_rule.request_' + contentType + '_body_label'));
          } else {
            self.buildAddParameterButton();
            targetInput.html(self.buildParameterInput(0, true))
              .append(self.addParameterButton);
            if (method === "get") {
              label.html(I18n.t('txt.admin.public.javascript.zd_rule.url_parameters_label'));
            } else {
              label.html(I18n.t('txt.admin.public.javascript.zd_rule.request_parameters_x-www-form-urlencoded_label'));
            }
          }
        } else {
          var textArea = self.buildTextArea(1);
          new InputSuggest(textArea);
          targetInput.html(textArea);
          label.html(I18n.t('txt.admin.public.javascript.zd_rule.message_label'));
        }
        messageDiv.toggle(target.data("showMessage"));
      });

      targetSelect.trigger("change");
      return targetSelect;
    },

    buildTargetInput: function (operatorDef) {
      var span = $("<span>");
      var targetMessageDiv = $("<div class='target-data'>");

      targetMessageDiv.append(this.buildLabel('txt.admin.public.javascript.zd_rule.message_label'));
      targetMessageDiv.append($("<div class='target-input'>"));
      targetMessageDiv.append(this.buildPlaceHolder());

      var selectBox = this.buildTargetInputSelect(operatorDef, targetMessageDiv);
      span.append(selectBox);
      span.append(targetMessageDiv);
      this.valueContainer = span;
      return span;
    },

    buildTwitterInput: function (operatorDef) {
      var span = $("<span>");
      span.append(this.buildLabel("txt.admin.public.javascript.zd_rule.message_label"));
      span.append(this.buildTextArea(0));
      span.append(this.buildLabel("txt.admin.public.javascript.zd_rule.mention_keep_it_short"));
      this.valueContainer = span;
      return span;
    },

    handleSpecialCaseActionDisplay: function(span) {
      switch ( this.targetSelect.val() ) {
        case "set_tags":
        case "current_tags":
        case "remove_tags":
          var description = I18n.t('txt.admin.public.javascript.zd_rule.' + this.targetSelect.val() + '_description');

          span.append($("<p class='tag-action-info'>").html(description));
      }
    },

    buildAutomaticAnswersArticleLabelsInput: function() {
      var MAX_LABELS = 10;
      var MAX_COMMAS = MAX_LABELS - 1;
      var articleLabelsInput = $('<input type="text" class="text">')
        .attr("name", this.elementBaseName + "[value][3]")
        .attr("placeholder", I18n.t('txt.admin.public.javascript.zd_rule.answer_bot_trigger_input_placeholder'))
        .val(this.values[3]);

      // restrict number of article labels entered by user
      articleLabelsInput.keypress(function(e) {
        var commasCount = ((e.target.value).match(/,/g) || []).length;
        var commaKeyDown = (e.which == 44) ? true : false;

        if(commasCount >= MAX_COMMAS && commaKeyDown) {
          e.preventDefault();
        }
      });

      return articleLabelsInput;
    },

    buildAutomaticAnswersInput: function(operatorDef) {
      var span = this.buildValueSelect(operatorDef);
      span.append(this.buildEmailInput());
      span.append(this.buildLabel('txt.admin.public.javascript.zd_rule.answer_bot_label'));
      span.append(this.buildAutomaticAnswersArticleLabelsInput());

      this.valueContainer = span;
      return span;
    },

    buildValueElement: function(operatorDef) {
      if ( !operatorDef || !operatorDef.values )
        return;

      var container;
      switch ( operatorDef.values.type ) {
        case "multiselect_list":
        case "list":
          container = this.buildValueSelect(operatorDef);
          break;
        case "text":
          container = this.buildValueTextInput(operatorDef);
          break;
        case "date":
          container = this.buildValueDateInput(operatorDef);
          break;
        case "textarea":
          container = this.buildValueTextAreaInput(operatorDef);
          break;
        case "list_with_textarea":
          container = this.buildValueSelectAndTextAreaInput(operatorDef);
          break;
        case "email":
          container = this.buildEmailInput(operatorDef);
          break;
        case "automatic_answers":
          container = this.buildAutomaticAnswersInput(operatorDef);
          break;
        case "sms":
          container = this.buildSmsInput(operatorDef);
          break;
        case "target":
          container = this.buildTargetInput(operatorDef);
          break;
        case "twitter":
          container = this.buildTwitterInput(operatorDef);
          break;
        case "custom_date":
          container = this.buildValueCustomDateInput(operatorDef);
          break;
      }

      this.handleSpecialCaseActionDisplay(container);
      return container;
    }
  });

  $.extend(this.RuleSet.prototype, {
    buildConditionRow: function(target, operator, values) {
      var rowIndex = ++this.conditionRowCount;
      var row = new Zendesk.RuleUI.ConditionRow(this, rowIndex, target, operator, values);
      return row.build();
    },

    buildActionRow: function(target, values) {
      var rowIndex = ++this.actionRowCount;
      var row = new Zendesk.RuleUI.ActionRow(this, rowIndex, target, values);
      return row.build();
    }
  });

  // report support follows, oy.
  this.ReportDataSeries = function(name, state, condDefs) {
    var legendI18n = I18n.t('txt.admin.public.javascript.zd_rule.legend_for_this_data_series_label');

    this.ruleSet = new Zendesk.RuleUI.RuleSet(condDefs);
    this.state = state;

    var frame = this.frame = $("<div class='frame' style='padding-top: 20px'>");
    frame.append($("<a style='float: right;' color: #B65151 href='#'>")
                    .click(function() { frame.remove(); } )
                    .html(I18n.t('txt.admin.public.javascript.zd_rule.remove_label')));

    frame.append($("<input type='text' class='reportLegend'>")
         .attr("name", "sets[" + this.ruleSet.setID + "][legend]")
         .val(name || legendI18n)
         .click(function() { if ( this.value == legendI18n ) this.value = ''; return false; }));

    this.fieldSet = $("<fieldset class='conditions'>");
    this.fieldSet.append(this.buildReportStateSelect());

    frame.append(this.fieldSet);

    if(ruleEditingCapability) {
      frame.append($("<div id='conditions-add-div'>"));
      frame.append($("<fieldset class='conditions add'>")
        .append( $("<h3 class='light'>").html(I18n.t("txt.admin.public.javascript.zd_rule.add_condition_label")) )
        .append( $("<a class='plus-button' href='#'>")
                    .click(function() {
                      this.addConditionRow();
                    }.bind(this)))
      );
    }
  };

  $.extend(this.ReportDataSeries.prototype, {
    _reportStateSelectOptions: [
      ['created_at', 'txt.admin.public.javascript.zd_rule.created_tickets_label'],
      ['solved_at', 'txt.admin.public.javascript.zd_rule.resolved_tickets_label'],
      ['working', 'txt.admin.public.javascript.zd_rule.unsolved_tickets_label'],
      ['any_closed', 'txt.admin.public.javascript.zd_rule.old_tickets_label'],
      ['any', 'txt.admin.public.javascript.zd_rule.all_tickets_label']
    ],

    buildReportStateSelect: function() {
      var stateField = $("<select style: 'width:225px'>")
                          .attr("name", 'sets[' + this.ruleSet.setID + '][state]');

      _(this._reportStateSelectOptions).each(function(array) {
        stateField.append($("<option>").attr("value", array[0]).html(I18n.t(array[1])));
      });

      stateField.val(this.state || 'created_at');
      return stateField;
    },

    addConditionRow: function(target, operator, values) {
      this.frame.children('#conditions-add-div').before(this.ruleSet.buildConditionRow(target, operator, values));
    }
  });

  this.buildReportDataSeries = function(name, state, conditions) {
    var series = new Zendesk.RuleUI.ReportDataSeries(name, state, conditions);
    return series;
  };
});
