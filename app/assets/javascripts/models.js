/*globals _, Class*/
var Account = Class.create({
  initialize: function(attributes) {
    if (attributes.lastTrialDay) {
      attributes.lastTrialDay = new Date(attributes.lastTrialDay);
    } else {
      delete attributes.daysLeftInTrial;
    }
    if (attributes.creationDate) {
      attributes.creationDate = new Date(attributes.creationDate);
    }
    _.extend(this, attributes);
  },

  hasFeature: function(name) {
    return _(this.features).contains(name);
  }
});

var User = Class.create({
  initialize: function(attributes) {
    attributes.passwordExpiresAt = new Date(attributes.passwordExpiresAt);

    switch(attributes.role) {
      case User.roles.endUser:
        this.isAnonymous = (typeof(attributes.id) === "undefined") || (attributes.id === null);
        this.isEndUser = true;
        this.isAgent = false;
        this.isAdmin = false;
        break;
      case User.roles.admin:
        this.isAnonymous = false;
        this.isEndUser = false;
        this.isAgent = true;
        this.isAdmin = true;
        break;
      case User.roles.agent:
        this.isAnonymous = false;
        this.isEndUser = false;
        this.isAgent = true;
        this.isAdmin = false;
        break;
      default:
        break;
    }

    _.extend(this, attributes);
  },

  isPasswordExpiring: function() {
    var now           = (new Date()).valueOf();
    var staleDuration = 5 * 24 * 60 * 60 * 1000;

    return (this.passwordExpiresAt > now) &&
      (this.passwordExpiresAt < now + staleDuration);
  },

  isTagged: function(tag) {
    return _(this.tags).contains(tag);
  }
});
User.roles = {endUser: 0, admin: 2, agent: 4};
User.restrictions = {none: 0, groups: 1, organization: 2, assigned: 3, requested: 4};

var Organization = Class.create({
  initialize: function(attributes) {
    _.extend(this, attributes);
  }
});

var currentUser = null;
var currentAccount = null;

User.load = function(data) {
  Zendesk.ToTangoID            = data.misc.toTangoID;
  Zendesk.viewedOnMobileDevice = data.misc.isMobileAgent;

  currentUser = Zendesk.currentUser = new User(data.user);
  currentUser.account = currentAccount = Zendesk.currentAccount = new Account(data.account);
  currentUser.organization = new Organization(data.organization);

  currentUser.canViewMultipleOrganizations = currentAccount.hasFeature('multipleOrganizations') && (currentUser.sharedOrganizations.length > 1 || currentUser.restriction === 2 && currentUser.organization);
  currentUser.canViewOrganization = currentUser.organization.name != undefined && (!currentUser.isEndUser || currentUser.restriction === User.restrictions.organization || currentUser.organization.isShared);

  if(currentUser.isAdmin) {
    Zendesk.NS('Voice');
    Zendesk.Voice.Config = {
      AccountSid: data.voice.account_sid,
      ApiVersion: data.voice.api_version
    };
  }
};
