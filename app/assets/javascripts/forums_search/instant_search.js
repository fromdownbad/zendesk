/*global $z, _, Zendesk */
/*jshint lastsemic: true, expr: true, whitespace: true*/

/*globals Zendesk, currentAccount*/
Zendesk.NS('Forums.Search.Instant', this.jQuery, this.I18n, function($j, I18n) {

  var options = {perPage: 5, focusOnLoad: false, searchScopes: [], tracking: true};

  var KEYCODE_MAP = {
    ENTER: 13,
    ESC: 27,
    DOWN: 40,
    UP: 38
  };

  function isKey(evt, name) { return KEYCODE_MAP[name] === evt.keyCode; }
  function evtHandled(evt) { evt.stopPropagation(); evt.preventDefault(); return true; }

  var inputController = {

    // accessing
    element: function() { return this.form().find('#query'); },
    val: function() { return this.element().val(); },
    form: function() { return $j('#searchform'); },
    searchUrl: function() { return this.form().attr('action'); },
    icon: function() { return this.form().find('#icon'); },
    scopeSelector: function() { return this.form().find('div.search_scope_selector'); },

    // initializing
    setup: function() {
      $j(window).bind('keydown', this.onWindowKeyDown.bind(this));
      var input = this.element();
      input.keydown(this.onInputKeyDown.bind(this));
      input.keyup(this.onInputKeyUp.bind(this));
      input.attr('autocomplete', 'off');

      this.addIcon();
      if (this.searchScopes().length > 0) { this.addScopeSelector(); }

      if (options.focusOnLoad) { input.focus(); }
      // wait for my container to change size, check twice for slow load
      (function() { inputController.fixInputLayout(); }).delay(0.2);
      (function() { inputController.fixInputLayout(); }).delay(1);
      $j(window).resize(function(evt) { inputController.fixInputLayout(); });
    },

    offset: function() { return 5; },

    addIcon: function() {
      var input = this.element(),
          icon = $j('<div id="icon" title="Press <ESC> to clear input."/>');

      input.after(icon);

      // align icon so that it is inside search input field
      // intentionally not inside stylesheet
      icon.css({
        marginLeft: -icon.width() + 'px',
        left: -1 * this.offset() + 'px'
      }).addClass("clear_icon").hide();

      icon.bind('click', this.resetInput.bind(this));
    },

    // ---- search scope ----
    addScopeSelector: function() {
      var scopeSelector = $j('<div class="search_scope_selector">' +
                             '  <span class="scope_text"></span>' +
                             '  <span class="arrow">▾</span>' +
                             '  <ul class="dropdown"/>' +
                             '</div>');

      this.icon().after(scopeSelector);
      scopeSelector.
        attr("tabindex", 0).
        keydown(this.onScopeSelectorKeyDown.bind(this)).
        keyup(this.onScopeSelectorKeyUp.bind(this)).
        click(function(evt) {
          inputController.openSearchScopeMenu('true'); evtHandled(evt); }).
        hover(function() { inputController.openSearchScopeMenu(); },
              function() { inputController.closeSearchScopeMenu(); }).
        focus(function() {
          inputController.openSearchScopeMenu();
          inputController.selectSearchScopeMenuItem(0); }).
        blur(function() { inputController.closeSearchScopeMenu(); });

      scopeSelector.find('.arrow').css({position: 'absolute', right: '0px'});
      scopeSelector.find('span').hover(function() { inputController.selectSearchScopeMenuItem(0); });

      this.populateScopeItems();
    },

    populateScopeItems: function() {
      var searchUrl = this.searchUrl(),
          scopes = this.searchScopes(),
          defaultScope = scopes.detect(function(ea) { return ea.url === searchUrl; });

      // set the right size for the menu so that the scope selector will fit
      // all items in the list, note that we have to deal with the arrow
      // manually because it is positioned absolute
      var arrowWidth = this.scopeSelector().find(".arrow").width() + 3 * this.offset(),
          width = scopes.inject(0, function(width, scope) {
            this.setScope(scope);
            return Math.max(width, this.scopeSelector().find(".scope_text").width());
          }, this);
      this.scopeSelector().css({width: width + arrowWidth + "px"});

      if (!defaultScope) return;
      this.setScope(defaultScope);
      this.setScopeList(scopes.without(defaultScope));
    },

    setScope: function(scope) {
      this.scopeSelector().find('.scope_text').
        text(I18n.t('txt.instant_search.in_forum_name', {forum_name:scope.name})).
        data('searchScope', scope);
    },

    setScopeList: function(scopes) {
      var list = this.scopeSelectorMenu();
      this.closeSearchScopeMenu();
      list.find('li').remove();
      scopes.forEach(function(ea, i) {
        $j('<li>' + I18n.t('txt.instant_search.in_forum_name', {forum_name:ea.name}) + '</li>').
          appendTo(list).
          data('searchScope', scopes[i]).
          click(function(evt) {
            inputController.changeScope($j(this).data('searchScope'), true);
            list.hide();
            evtHandled(evt);
          }).
          hover(function() { inputController.selectSearchScopeMenuItem(i + 1); });
      });
    },

    scopeSelectorMenu: function() { return this.scopeSelector().find('.dropdown'); },
    scopeSelectorMenuItems: function() { return this.scopeSelector().find('.scope_text, li'); },

    hasSelectedSearchScopeMenuItem: function() {
      return this.getSelectedSearchScopeMenuItem().length > 0;
    },

    getSelectedSearchScopeMenuItem: function() {
      return this.scopeSelectorMenuItems().filter('.selected');
    },

    openSearchScopeMenu: function() { this.scopeSelectorMenu().show(); },

    closeSearchScopeMenu: function() {
      this.scopeSelectorMenu().hide();
      this.deselectAllSearchScopeMenuItems();
    },

    selectSearchScopeMenuItem: function(idx) {
      this.deselectAllSearchScopeMenuItems();
      var all = this.scopeSelectorMenuItems();
      idx = Math.abs(idx) % all.length;
      this.selectSearchScopeElement($j(all.get(idx)));
    },

    selectSearchScopeElement: function(selected) {
      if (selected.hasClass('scope_text')) {
        selected = selected.add(selected.parent());
      }
      selected.addClass('selected');
    },

    selectNextSearchScopeMenuItem: function(next) {
      var idx = this.scopeSelectorMenuItems().index(this.getSelectedSearchScopeMenuItem());
      this.selectSearchScopeMenuItem(idx + (next ? 1 : -1));
    },

    deselectAllSearchScopeMenuItems: function() {
      this.form().find('.selected').removeClass('selected');
    },

    getSelectedSearchScope: function() {
      return this.getSelectedSearchScopeMenuItem().data('searchScope');
    },

    searchScopes: function() {
      return options.searchScopes;
    },

    changeScope: function(scope, byMouse) {
      this.form().attr('action', scope.url);
      this.populateScopeItems();
      this.fixInputLayout();
      this.element().focus();
      searchController.searchNow();
    },

    // ---- layouting ----
    fixInputLayout: function() {
      var icon = this.icon(),
          input = this.element(),
          form = this.form(),
          button = form.find("#buttonsubmit"),
          scopeSelector = this.scopeSelector();

      if (scopeSelector.length > 0) {
        this.layoutScopeSelector(scopeSelector, input, icon);
      }

      this.layoutInput(input, form, button);
    },

    layoutScopeSelector: function(scopeSelector, input, icon) {
      var scopeSelWidth = scopeSelector.width();

      // set the width of the scope selector box
      scopeSelector.css({
        marginLeft: -1 * scopeSelWidth + 'px',
        marginRight: -3 * this.offset() + 'px',
        left: -3 * this.offset() + 'px',
        paddingLeft: 2 * this.offset() + 'px'
      });

      scopeSelector.find('.arrow').css({
        marginRight: this.offset() + 'px'
      });

      // adapt the dropdowm of the scope sel box accordingly
      var scopeSelWidthWithPadding = scopeSelector.bounds(true).width(),
      dropdown = scopeSelector.find('.dropdown');
      dropdown.css({left: '0px',
                    width: scopeSelWidthWithPadding + 'px'});
      icon.css({left: -1 * (scopeSelWidth + icon.width() + this.offset()) + 'px'});

      // and add some padding to the input so that input stops before icon and scope selector
      var iconAndScopeSelWidth = scopeSelWidthWithPadding + icon.width() + 2 * this.offset();
      input.css({paddingRight: iconAndScopeSelWidth + 'px' });
    },

    layoutInput: function(input, form, button) {
      // now fix the width of the whole input by filling out the width of the form

      // if the window is already more narrow then the input, the button will be linebreaked
      // to underneath the input. In this case the calculation using the bounds of both
      // input and button below will not work. to fix this make the input very narrow at first
      input.css({width: '20px'});
      var inputAndButtonBounds = input.add(button).bounds(true),
          formBounds = form.bounds(true),
          formParentBounds = form.parent().bounds(true),
          formMarginLeft = formBounds.left - formParentBounds.left,
          deltaX = formBounds.width() - inputAndButtonBounds.width() - formMarginLeft;
      input.css({width: input.width() + deltaX + 'px'});
    },

    // ---- updating ----
    updateIcon: function() {
      if (!searchController.searchInitiated) {
        this.toggleClearIconDisplay();
        return;
      }

      if (this.val().length > 0) {
        this.signalLoading();
      } else {
        this.stopSignalLoading();
      }
    },

    toggleClearIconDisplay: function() {
      var icon = this.icon();
      icon.removeClass('loading_icon');
      if (this.val() === "") {
        icon.removeClass('clear_icon');
        icon.hide();
      } else {
        icon.addClass('clear_icon');
        icon.show();
      }
    },

    resetInput: function(byMouse) {
      this.element().val("");
      this.updateIcon();
      resultController.removeIncrementalSearchResults();
    },

    signalLoading: function() {
      inputController.icon().show().removeClass("clear_icon").addClass("loading_icon");
    },

    stopSignalLoading: function() {
      inputController.icon().addClass("clear_icon").removeClass("loading_icon");
      this.toggleClearIconDisplay();
    },

    // ---- event handling ----
    onInputKeyDown: function(evt) {
      this.prevVal = this.val();

      // esc needs to be handled in keydown for IE
      if (!isKey(evt, 'ESC')) return undefined;
      var input = this.element();
      if (this.val() === "") {
        input.blur();
      } else {
        this.resetInput();
        if ($j.browser.msie) { // special IE needs
          window.setTimeout((function() { input.focus(); }).bind(this), 400);
        }
      }
      return evtHandled(evt);
    },

    onInputKeyUp: function(evt) {
      if (isKey(evt, 'ENTER')) {
        var selectedResultItem = resultController.selectedResultItem();
        if (selectedResultItem && this.hasSelectedSearchScopeMenuItem()) {
          resultController.visitResultItem(selectedResultItem);
          return evtHandled(evt);
        }
      }

      if (isKey(evt, 'ESC')) {
        return evtHandled(evt);
      }
      if (!isKey(evt, 'UP') && !isKey(evt, 'DOWN')) {
        var self = this;
        // wait a bit
        _.delay(function() {
          if (self.prevVal === self.val() && self.prevVal !== "") return;
          searchController.initSearchRequest();
        });
      }

      return undefined;
    },

    onScopeSelectorKeyUp: function(evt) {
      if (isKey(evt, 'ESC')) {
        this.element().focus(0);
        return evtHandled(evt);
      }
      return undefined;
    },

    onScopeSelectorKeyDown: function(evt) {
      if (isKey(evt, 'ENTER')) {
        this.changeScope(this.getSelectedSearchScope());
        this.closeSearchScopeMenu();
        return evtHandled(evt);
      }

      if (isKey(evt, 'DOWN') || isKey(evt, 'UP')) {
        this.selectNextSearchScopeMenuItem(isKey(evt, 'DOWN'));
        return evtHandled(evt);
      }

      return undefined;
    },

    onWindowKeyDown: function(evt) {
      // FIXME somehow evt.stopPropagation in input isn't working
      if (this.hasSelectedSearchScopeMenuItem()) { return undefined; }
      if (isKey(evt, 'UP') || isKey(evt, 'DOWN')) {
        resultController.changeSelection(isKey(evt, 'DOWN') ? 1 : -1);
        return evtHandled(evt);
      }
      if (isKey(evt, 'ENTER')) {
        var selected = resultController.selectedResultItem();
        if (selected) {
          resultController.visitResultItem(selected);
          return evtHandled(evt);
        }
      }
      return undefined;
    }

  };


  var resultController = {

    // ---- accessing  ----
    container: function() { return $j("#incremental_search"); },
    notFoundElement: function() { return $j("#not_found_hint"); },
    output: function() { return $j("#incremental_search_result"); },
    hasResults: function(html) { return html.find('.item-info').length > 0; },
    resultItems: function() { return this.output().find('.item'); },
    result_snippets: function() { return this.output().find('.item_search_snippet'); },
    selectedResultItem: function() { return this.resultItems()[this.selectedResultItem_idx]; },
    result_titles: function(html) { return html.find('a[title]'); },
    resultDataSelector: '.item, #show_more',
    loadMoreScriptSelector: '.search_results_data',
    resultSelector: function() { return this.resultDataSelector + '. ' + this.loadMoreScriptSelector; },
    notFoundText: I18n.t('txt.search.we_could_find_results'),

    // initializing
    setup: function() {
      this.showExistingResultsIfPresent();
    },

    // result items
    visitURL: function(url) {
      document.location = url;
    },

    visitResultItem: function(item, isMouseAction) {
      if (item) {
        this.visitURL($j(item).find(".item-info a").attr('href'));
      }
    },

    // processing results
    prepareHtml: function(html, query) {
      paginatorController.paginate(html);
      this.highlight(html, query);
      this.prepareResultItemsForSelection();
    },

    highlight: function(html, query) {
      if (!query) {
        // 'Cannot highlight search results without query'
        return;
      }
      var titles = resultController.result_titles(html),
          snippets = resultController.result_snippets(html);
      titles.removeHighlight();
      snippets.removeHighlight();
      query.split(" ").forEach(function(term) {
        titles.highlight(term);
        snippets.highlight(term);
      });
    },

    processResult: function(query, data) {
      this.container().show();
      var parsedData = $j(data);
      if (!this.hasResults(parsedData)) {
        this.removeIncrementalSearchResults();
        this.showNotFoundHint();
        return;
      }

      this.notFoundElement().hide();
      this.output().show();
      var animationSpec = this.findElementsToAnimate(this.output(), parsedData);
      animationSpec.slideDown.hide();
      if (animationSpec.slideUp.length > 0) {
        animationSpec.slideUp.slideUp('fast', this.embedNew.bind(this).curry(animationSpec, query));
      } else {
        this.embedNew(animationSpec, query);
      }
    },

    embedNew: function(spec, query) {
      var output = this.output();
      this.container().show();
      if (spec.replaceOld.length === 0) {
        output.append(spec.replaceNew);
      } else {
        spec.replaceOld.replaceWith(spec.replaceNew);
      }
      spec.slideUp.remove();
      var insertAfterItem = output.find('.item').last(),
          insertBeforeItem = output.find(':first-child').first();
      if (insertAfterItem.length > 0) {
        spec.slideDown.insertAfter(insertAfterItem);
      } else if (insertBeforeItem.length > 0) {
        spec.slideDown.insertBefore(insertBeforeItem);
      } else {
        spec.slideDown.appendTo(output);
      }
      spec.slideDown.slideDown('fast', this.prepareHtml.bind(this).curry(output, query));
      this.prepareHtml(output, query);
    },

    removeIncrementalSearchResults: function() {
      var output = this.output();
      output.hide('fast', 'swing', function() { output.html(""); });
    },

    showExistingResultsIfPresent: function() {
      var existingResultContainer = $j(".hidden_incremental_search_results");
      if (existingResultContainer.length === 0) return;
      if (!this.hasResults(existingResultContainer)) {
        this.showNotFoundHint();
      }
      var animSpec = this.findElementsToAnimate(null, existingResultContainer, true);
      this.embedNew(animSpec, this.getQuery());
      inputController.updateIcon();
    },

    showNotFoundHint: function() {
      var elem = this.notFoundElement();
      if (elem.length === 0) {
        elem = $j('<div id="not_found_hint">' +
                  '<h2>' + this.notFoundText + '</h2>' +
                  '</div>');
        this.container().find('#incremental_search_result').after(elem);
      }
      return elem.show();
    },

    getQuery: function() {
      return inputController.element().val();
    },

    // ---- result replacement
    parseResults: function(html) {
      var $html = $j(html);
      return {
        items: $html.filterOrFind('.item'),
        moreButton: $html.filterOrFind('#show_more'),
        paginatorScript: $html.filterOrFind(".search_results_data")
      };
    },

    findElementsToAnimate: function(oldResults, newResults, noAnimation) {
      var old = this.parseResults(oldResults),
          _new = this.parseResults(newResults);
      if (noAnimation) {
        return {
          remove: old.paginatorScript,
          slideUp: $j(), slideDown: $j(),
          replaceOld: old.items.add(old.moreButton),
          replaceNew: _new.paginatorScript.add(_new.items).add(_new.moreButton)
        };
      }
      return {
        remove: old.paginatorScript,
        slideUp: old.items.slice(_new.items.length),
        slideDown: _new.items.slice(old.items.length),
        replaceOld: old.items.slice(0, _new.items.length).add(old.moreButton),
        replaceNew: _new.paginatorScript.add(_new.items.slice(0, old.items.length)).add(_new.moreButton)
      };
    },

    // ---- item selection ----
    prepareResultItemsForSelection: function() {
      this.selectedResultItem_idx = -1;
      var self = this, resultItems = this.resultItems();
      resultItems.bind('mousemove', function() {
        var idx = resultItems.index(this);
        if (idx !== -1) self.selectResultItem(idx);
      });
      resultItems.bind('click', function() { self.visitResultItem(this, true); });
    },

    changeSelection: function(delta) {
      this.selectResultItem(this.selectedResultItem_idx + delta);
    },

    selectResultItem: function(idx) {
      if (paginatorController.is_loading_more) return;
      var resultItems = this.resultItems(),
          result_count = resultItems.length;
      resultItems.removeClass('selected');
      if (result_count === 0) { return; }

      var at_end = idx === result_count;
      if (at_end && paginatorController.next_page()) {
        paginatorController.load_next_page_select_idx = idx;
        paginatorController.loadNextPage();
        return;
      }

      if (idx < 0) {
        idx = result_count - 1;
      } else if (idx >= result_count) {
        idx = 0;
      }
      var next = resultItems.get(idx);
      if (next.scrollIntoViewIfNeeded) { next.scrollIntoViewIfNeeded(); }
      $j(next).addClass("selected");
      this.selectedResultItem_idx = idx;
    }

  };


  var paginatorController = {

    itemsPerPage: function() {
      return options.perPage;
    },

    paginator: function() {
      return resultController.output().data('resultsPaginator');
    },

    setup: function() {
      var proto = Zendesk.ResultsPaginator.prototype;
      proto.onContentRetrievedCallback = function(paginator, content) {
        this.slide_in_paginator_content(paginator, content);
        this.on_paginator_page_load(paginator);
        resultController.prepareResultItemsForSelection();
        this.is_loading_more = false;
        if (this.load_next_page_select_idx) {
          resultController.selectResultItem(this.load_next_page_select_idx);
          delete this.load_next_page_select_idx;
        }
      }.bind(this);
    },

    reset_paginator: function() {
      resultController.output().data('resultsPaginator', null);
    },

    paginate: function(element) {
      this.reset_paginator();
      element.paginateResults();
      // for some reason paginator nextPage is set to 2 on paginator creation
      // even if there is no page 2, however we need this info for selecting
      // result items
      var paginator = this.paginator();
      if (paginator && paginator.nextPage === 2) {
        paginator.nextPage = Zendesk.ResultsPaginator.parseNextPageFrom(element);
      }
    },

    slide_in_paginator_content: function(paginator, content) {
      content = $j(content).hide();
      paginator.$container.append(content);
      content.each(function(ea) { $j(this).slideDown('fast'); });
    },

    on_paginator_page_load: function(paginator) {
      var query = searchController.last_query;
      if (query) {
        resultController.highlight(paginator.$container, query);
      }
    },

    next_page: function() {
      var paginator = this.paginator();
      return paginator && paginator.nextPage;
    },

    loadNextPage: function() {
      var paginator = this.paginator();
      if (paginator && paginator.nextPage) {
        this.is_loading_more = true;
        paginator.loadMore(true/*triggered by key*/);
      }
    }

  };


  var searchController = {

    searchDelay: 370,

    initSearchRequest: function() {
      this.searchInitiated = true;
      inputController.updateIcon();

      // we have to save the callback since debounce won't work if we recreate every time
      if (!this.searchLater) {
        this.searchLater = _.debounce(this.searchNow.bind(this), this.searchDelay);
      }
      this.searchLater();
    },

    searchNow: function() {
      var query = this.createSearchQuery(inputController.val());
      if (query && query.length > 0) {
        this.searchInitiated = true;
        this.doIncrementalSearch(inputController.searchUrl(), query);
      } else {
        this.searchInitiated = false;
        resultController.removeIncrementalSearchResults();
      }
      inputController.updateIcon();
    },

    createSearchQuery: function(inputString) {
      // processing for clean queries, probably needs to be exented
      var punctuation = "[\\.,\\+]";
      return inputString
             .replace(new RegExp('^' + punctuation + '|' + punctuation + '$'), '')
             .replace(new RegExp(punctuation), ' ')
             .replace(/\s+/, ' ');
    },

    processResult: function(query, result) {
      query = query || this.last_query;
      // save query for highlighting pagination
      this.last_query = query;

      this.searchInitiated = false;
      inputController.updateIcon();

      return resultController.processResult(query, result);
    },

    doIncrementalSearch: function(request_url, query) {
      $j.ajax({
        url: request_url,
        data: {
          query: query,
          incremental: true,
          format: "html",
          page: 1,
          per_page: paginatorController.itemsPerPage(),
          xhr: true
        }
      }).done(this.processResult.bind(this).curry(query));
    }

  };

  var trackingController = {

    trackingModule: 'Incremental Forum Search',

    track: function(activity) {
      Zendesk.Instrumentation.track(activity, this.trackingModule);
    },

    setupTracking: function() {
      var i = Zendesk.Instrumentation;

      /* search activated */
      i.on(searchController, 'searchNow',
           'search activated', this.trackingModule);

      /* on search result item clicked / enter key pressed */
      i.on(resultController, 'visitResultItem',
           function(item, byMouse) {
             return "topic selected by " + (byMouse ? "mouse" : "key:enter") },
           this.trackingModule);

      /* search input cleared */
      i.on(inputController, 'resetInput',
           function(byMouse) {
             return "clear search by " + (byMouse ? "mouse" : "key:esc") },
           this.trackingModule);

      /* search submit, we want to seperate triggering by mouse / enter */
      var form = inputController.form(),
          byMouseText = 'search submitted by mouse',
          byEnterKeyText = 'search submitted by key:enter';
      form.
        addClass('tracked').
        data('tracking-activity', byMouseText).
        data('tracking-module', this.trackingModule).
        instrumentTracking().
        keydown(function(evt) {
          if (isKey(evt, 'ENTER')) {
            inputController.form().data('tracking-activity', byEnterKeyText);
          }
        }).
        keyup(function(evt) {
          if (isKey(evt, 'ENTER')) {
            inputController.form().data('tracking-activity', byMouseText);
          }
        });

      /* tracking deflect clicks */
      $j('.deflect.tickets').add('.deflect.ideas').add('.deflect.questions').
        addClass('tracked').
        data('tracking-module', this.trackingModule).
        instrumentTracking();
      $j('.deflect.tickets').data('tracking-activity', "'Get in touch' selected");
      $j('.deflect.ideas').data('tracking-activity', "'Submit an idea' selected");
      $j('.deflect.questions'). data('tracking-activity', "'Ask a question' selected");

      /* search scope selector */
      i.on(inputController, 'openSearchScopeMenu',
           function(byMouse) {
             return "scope selector: opened by " + (byMouse ? "mouse" : "key:tab") },
           this.trackingModule);

      i.on(inputController, "changeScope",
           function(_, byMouse) {
             return "scope selector: new scope selected by " + (byMouse ? "mouse" : "key:tab") },
           this.trackingModule);

      i.on(Zendesk.ResultsPaginator.prototype, "loadMore",
           function(byKey) { return "see more button selected by " + (byKey ? "key" : "mouse")},
           this.trackingModule);

      i.on(resultController, "embedNew",
           function(spec) {
             var moreBtnFound = spec.replaceNew.filterOrFind('.show_more_bar').add(
               spec.slideDown.filterOrFind('.show_more_bar')).length > 0;
             return moreBtnFound ? 'see more button visible' : null; },
           this.trackingModule);

      if (options.searchScopes && options.searchScopes.length > 0) {
        this.track("scope selector visible");
      }

    }

  }


  function setupOptionsFromParams(params) {
    // only activate when search result markup can be found
    options.instantSearchEnabled = resultController.container().length > 0;

    if (params && params.per_page) {
      options.perPage = params.per_page;
    }

    if (params && params.focus) {
      options.focusOnLoad = params.focus;
    }

    if (params && params.search_scopes) {
      options.searchScopes = params.search_scopes;
      if (params.model_name) {
        options.searchScopes.push({
          name: params.model_name,
          url: inputController.searchUrl()
        });
      }
    }

    if (params && params.tracking !== undefined) {
      options.tracking = params.tracking;
    }
  }


  // exports
  this.inputController     = inputController;
  this.resultController    = resultController;
  this.paginatorController = paginatorController;
  this.searchController    = searchController;
  this.trackingController  = trackingController;

  this.setup = function(params) {
    setupOptionsFromParams(params);
    if (!options.instantSearchEnabled) return;

    $j(function() {
      paginatorController.setup();
      inputController.setup();
      resultController.setup();
      if (options.tracking) { trackingController.setupTracking(); }
    });
  }

});
