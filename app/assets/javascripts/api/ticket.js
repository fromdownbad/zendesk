/*globals Zendesk*/
Zendesk.NS('API', this.jQuery, function($) {
  this.TicketForm = function(formElement) {
    this.ticketForm = $(formElement);
  };

  this.TicketForm.prototype = {
    createTicket: function(ticketOptions) {
      return this.createTwitterTicket(ticketOptions);
    },

    createTwitterTicket: function(ticketOptions) {
      var data = this.mergeIntoDefaults(ticketOptions);
      return $.ajax({
        url: "/api/v2/channels/twitter/tickets.json",
        type: 'POST',
        data: data
      });
    },

    mergeIntoDefaults: function(ticketOptions) {
      var defaults = this.formDefaults();
      defaults.each(function(defaultValueElement) {
        var attributeName = defaultValueElement.name;
        ticketOptions.each(function(newValueElement) {
          if(typeof(newValueElement[attributeName]) != "undefined") {
            defaultValueElement.value = newValueElement[attributeName];
          }
        });
      });
      return $.param(defaults);
    },

    formDefaults: function() {
      return this.ticketForm.serializeArray();
    },

    hasComment: function() {
      var value = $.trim(this.ticketForm.find('[name="comment[value]"]').val());
      return value !== '';
    }
  };
});
