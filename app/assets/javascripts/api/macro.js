/*globals _, $, $j, Zendesk, ticketTagField, setSelectedForSelect, assigneeSelect*/
Zendesk.NS('API', function() {

  function highlightFields($fields) {
    return $fields.css('color', 'green');
  }

  this.Macro = function(macroID) {
    this.macro_id = macroID;

    var self = this;
    this._ticketTransforms = {
      comment: function(macro_result, ticketForm) {
        //Comment Content
        var value = macro_result.comment && macro_result.comment.scoped_value &&
            macro_result.comment.scoped_value.filter(function(comment){
          return comment[0] == "channel:web" || comment[0] == "channel:all";
        }).map(function(comment){
          return comment[1];
        }).join("");

        var txt = $j("#comment_value", ticketForm);
        if(value && txt && (txt.val() != value)) {
          txt.val(txt.val() + value);
          highlightFields(txt);
        }

        //Comment Public checkbox
        value = macro_result.comment.is_public;
        var chk = $j("#comment_is_public", ticketForm);
        if(value) {
          if(value == 'true') {
            $j('#comment_type', ticketForm).attr('class', 'say public');
            chk.attr('checked', true);
            $j('#twitter_controls', ticketForm).show();
          } else {
            $j('#comment_type', ticketForm).attr('class', 'say private');
            chk.attr('checked', false);
            $j('#twitter_controls', ticketForm).hide();
          }
        }
      },

      current_tags: function(macro_result, ticketForm) {
        if(typeof(ticketTagField) == "undefined") { return; }
        ticketTagField.clear();
        self._addTags(macro_result.current_tags);
      },

      remove_tags: function(macro_result, ticketForm) {
        //This is used only for the bulk ticket update form.
        self._addRemoveTagsInput(macro_result.remove_tags, ticketForm);
      },

      _default: function(attribute, macro_result, ticketForm) {

        var elem_id = attribute.split("#")[0];
        var elem = null;

        if (elem_id.match(/ticket_fields_/)) { //Custom field
          elem = $j("#" + elem_id, ticketForm);
          self._applyToCustomField(elem, macro_result[attribute]);
        } else {
          elem = $j("#ticket_" + elem_id, ticketForm);
          if(elem) {
            setSelectedForSelect($('ticket_' + elem_id), macro_result[attribute]);
            highlightFields(elem);
          }
        }

      }
    };

    this._applyToCustomField = function(elem, value) {
      if(elem.is("input:checkbox")) {
        elem.prop('checked', value);
        return;
      }
      if(elem.is("select")) {
        //On bulk ticket pages, checkboxes become selects with on/off values.
        if (value === true)  { value = '1'; }
        if (value === false) { value = '0'; }
      }
      elem.val(value).change();
    };

    this._addTags = function(tag_string) {
      _(tag_string.split(' ')).each(function(tag){
        if(tag.length > 0) {
          ticketTagField.addEntry(tag);
        }
      });
    };

    this._addRemoveTagsInput = function(tagString, ticketForm) {
      $j("#ticket_remove_tags", ticketForm).val(tagString);
      var tagSwatches = _(tagString.split(' ')).map(function(tag) { return "<span class='tag'>" + tag + "</span>";});
      $j("#ticket_remove_tags_message", ticketForm).html("Removing tags: " + tagSwatches.join('')).show();
    };

    this._fillTicketForm = function(macro_result, ticketForm) {

      var groupID    = macro_result.group_id,
          assigneeID = macro_result.assignee_id;

      delete macro_result.group_id;
      delete macro_result.assignee_id;

      var attributes = _(macro_result).keys();

      if(groupID || assigneeID) {
        groupID && $j("#ticket_group_id").val(groupID);
        assigneeSelect(groupID, assigneeID)
          .done(function() {
            highlightFields($j('#ticket_group_id, #ticket_assignee_id'));
          });
      }

      for(var i=0, len=attributes.length; i < len; i++) {
        if(this._ticketTransforms[attributes[i]]) {
          this._ticketTransforms[attributes[i]](macro_result, ticketForm);
        } else {
          this._ticketTransforms._default(attributes[i], macro_result, ticketForm);
        }
      }

      $j("#macro_applied").val(this.macro_id);
    };
  };

  this.Macro.prototype = {
    evaluate: function(opts) {
      var self = this;
      var params = {};
      if(opts.ticketForm) {
        //Remove _method param from serialized form, if present.
        params = _($j(opts.ticketForm).serializeArray()).select(function(elem) {
          return elem.name != '_method';
        });
      }

      var url;
      if(opts.ticketID !== null) {
        url = '/api/v1/macros/' + this.macro_id + '/apply.json?ticket_id=' + opts.ticketID;
      } else {
        url = '/api/v1/macros/' + this.macro_id + '/apply.json';
      }

      $j.ajax({
        url: url,
        type: 'post',
        data: $j.param(params),
        dataType: 'json',
        success: function(macro_result, text_status, request) {
          if(opts.successCallback) {
            opts.successCallback(macro_result);
          }
        },
        error: function(req, textStatus) {
          if(opts.errorCallback) {
            opts.errorCallback(textStatus, req.responseText);
          }
        }
      });
    },

    applyToTicket: function(ticketID, ticketForm, callback) {
      var self = this;
      this.evaluate({ticketID: ticketID,
                     ticketForm: ticketForm,
                     successCallback: function(macro_result) {
                       self._fillTicketForm(macro_result, ticketForm);

                       //Jiggery-pokery to make cursor go to the end. Should use setSelectionRange but it doesn't work in IE.
                       var comment = $j("textarea#comment_value").val();
                       $j("textarea#comment_value").val('').focus().val(comment);

                       Zendesk.Twitter && Zendesk.Twitter.TicketCreationForm && (new Zendesk.Twitter.TicketCreationForm()._updateCounter());

                       $(document).fire('macro:applied');
                       $j(document).trigger('applied.macro.zendesk', macro_result);
                       if(callback) {
                         callback(macro_result);
                       }
                     }
                    });

    }

  };

  $j(document).bind('applied.macro.zendesk', function(event, macroResult) {
    Zendesk.Instrumentation.track('Applied', 'Macro');
  });
});
