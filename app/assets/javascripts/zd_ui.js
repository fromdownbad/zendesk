$(document).observe('dom:loaded',  function(){
  // Mouse over for nested category menues
  jQuery("ul.drop-list").each(function(i, elem) {
    new Zendesk.UI.NestedMenu(jQuery(elem)).initialize();
  });

  if (!window.XMLHttpRequest) {
    startList(); // Only for IE6
  }
});

function createRef(instance,method) {
  return function(){  method.apply(instance,arguments); };
}

function category_set(element, visibility, z_index, position) {
  var elm = element.down('ul');
  if (elm) {
    elm.style.display = visibility;
  }
}

var selection_feedback = function(li){
  new Effect.Highlight(li, {
    queue: 'end',
    beforeStart: function(){
        li.store('selection_feedback.style', li.readAttribute('style')); // make sure to set it to an empty string if it's null, otherwise you get problems in e.g. Safari
        li.setStyle({color: li.getStyle('background-color')}); // to temporarily invert the colors
    },
    startcolor: rgbToHex(li.getStyle('color')),
    duration: 0.2,
    afterFinish: function(){
      li.writeAttribute('style', li.retrieve('selection_feedback.style') || '');
    }
  });
  new Effect.Fade(li.up('ul.first-drop'), { queue: 'end', duration: 0.3, afterFinish: hideMacroLi(li) });
};

var categoryDescription = function(){
  if ($j("#category-description-link").exists()) {
    $j("#category-description-link").live("click", function(){
      $j("#category-description").toggle();
    });
  }
};

var categoryTopRightEdit = function(id){
  var category_top_right = $(id).down('.category-top-right');
  var edit_options_list = $(id).down('ul.edit_options');
  var show = function(){
    category_top_right.removeClassName('inactive');
    edit_options_list.removeClassName('inactive');
  };
  var hide = function(){
    edit_options_list.addClassName('inactive');
    category_top_right.addClassName('inactive');
  };

  $(id).down('span.edit_this').observe('mouseover', show);
  $(id).down('.category-top-right').observe('mouseleave', hide);

  var categorySelectionFeedback = function(li){
    new Effect.Highlight(li, {
      queue: 'end',
      beforeStart: function(){
        li.store('selection_feedback.style', li.readAttribute('style')); // make sure to set it to an empty string if it's null, otherwise you get problems in e.g. Safari
        li.setStyle({color: li.getStyle('background-color')}); // to temporarily invert the colors
      },
      startcolor: rgbToHex(li.getStyle('color')),
      duration: 0.2,
      afterFinish: function(){
        li.writeAttribute('style', li.retrieve('selection_feedback.style') || '');
        hide();
      }
    });
  };

  edit_options_list.childElements('li').each(function(li){
    li.childElements('a').each(function(anchor){
      anchor.observe('click', function(e){
        categorySelectionFeedback(li);
      });
    });
  });
};

var makeCategorizedForumsDraggable = function(category_id) {
  var alternateLeftRight = function(elements) {
    elements.each(function(index, element) {
      var isEven = index % 2 === 0;
      $j(element).toggleClass('left', isEven).toggleClass('right', !isEven);
    });
  };

  var categoryContainer = $j('#' + category_id);
  categoryContainer.find('.column').each(function(index, forum_element) { $j(forum_element).data('position', index); });
  $j('#' + category_id).parent().find(".category-header .category-top-right ul.edit_options li.reorder_forums a").click(function() {
    categoryContainer
      .addClass('reordering')
      .find('.reorder a').show().end()
      .sortable({ items: '.column' })
      .bind('sortchange', function(event, ui) {
        // alternate the stable ones and the placeholder, but not the one being moved:
        alternateLeftRight(categoryContainer.find('div.column').not(ui.item));
      })
      .bind('sortupdate', function(event, ui) {
        categoryContainer.data('changed', true);
        // placeholder's gone, so alternate all of the (now settled) divs:
        alternateLeftRight(categoryContainer.find('.column'));
      });

    return false;
  });

  categoryContainer.find('.column a').click(function() {
    if (categoryContainer.hasClass('reordering')) {
      return false;
    }
  });

  categoryContainer.find('.reorder a').click(function() {
    categoryContainer
      .removeClass('reordering')
      .sortable('destroy');

    if (categoryContainer.data('changed')) {
      categoryContainer
        .find('.column')
        .each(function(index, forum) {
          forum = $j(forum);
          if (forum.data('original_position') !== index) {
            $j.ajax(forum.data('forum_path'), {
              type: 'POST',
              data: {
                _method: 'put',
                'forum[position]': index,
                authenticity_token: currentUser.authenticityToken
              }
            });
            forum.data('original_position', index);
          }
        });
      categoryContainer.data('changed', false);
    }
    return false;
  })
};

var addCategoryReordering = function(){
  var reorderLink = $j('.buttons-right .reorder a');
  var sortedList = $j('#sortedlist');
  var container = sortedList.closest('.frame.columns');

  sortedList
    .children()
    .each(function(index, list) {
      $j(list).data('original_position', index);
    });

  var showReorder = function() {
    reorderLink
      .data('reorder_text', reorderLink.html())
      .html(reorderLink.data('done_text'));
    container.addClass('reordering');
    sortedList
      .sortable({ items: '.sortable' })
      .bind('sortupdate', function() { sortedList.data('changed', true); });
  }

  var finishReorder = function() {
    var originalText = reorderLink.data('reorder_text');
    reorderLink.html(originalText);
    container.removeClass('reordering');
    sortedList.sortable('destroy');
    if (sortedList.data('changed')) {
      sortedList.children().each(function(index, list) {
        list = $j(list);
        var id = list.data('item_id'),
            category_header_element = $j('#category_header_' + id),
            category_element        = $j('#category_' + id);
        container
          .append(category_header_element)
          .append(category_element);
        if (index !== list.data('original_position')){
          $j.ajax(category_element.data('category_path'), {
            type: 'POST',
            data: {
              _method: 'put',
              'category[position]': index
            }
          });
          list.data('original_position', index);
        }
      });
      sortedList.data('changed', false);
    }
  }

  reorderLink.toggle(showReorder, finishReorder);
};

var submit_sortable_list = function(display_container, sort_container, sort_list, url){
  var originalLink = $j('#' + display_container + '_reorder_done_link');
  var originalDestination = originalLink.prop('href');

  sort_list = $j('#' + sort_list);
  sort_list.find('> li.sortable.item').each(function(index, list) {
    $j(list).data('real_element', $j('#' + display_container + ' div.item')[index]);
  });
  $j('#' + display_container + '_reorder_done_link').click(function(event){
    event.preventDefault();
    var list = sort_list.find('> li.sortable').get().reverse();
    _(list).each(function(list_item, index){
      list_item = $j(list_item);
      var realElement = $j(list_item.data('real_element'));
      realElement
        .prependTo(realElement.parent())
        .toggleClass('nobottom', index === 0);
      list_item.data("zendesk-original-position", (list.length - index - 1));
    });
    $j('#' + sort_container).hide();
    $j('#' + display_container).show();
    var sortable = sort_list.sortable('serialize', { key: sort_list.prop('id') + '[]' });
    var authToken = "authenticity_token=" + encodeURIComponent(Zendesk.currentUser.authenticityToken);
    $j.ajax(url, {
      type: 'POST',
      data: sortable + "&" + authToken
    })
    .success(function() {
      window.location.replace(originalDestination);
    });
  });
};

/* Make IE 6 do (h)over */
function startList() {
  if (!navigator.userAgent.match(/msie 6/i)) {return;}
  iterate_startList($("green"));
  if (document.getElementById("gray")) {
    iterate_startList($("gray"));}
}

function iterate_startList(navRoot) {
  var node;
  if (!navRoot) {return;}
  for (var i=0; i<navRoot.childNodes.length; i++) {
    node = navRoot.childNodes[i];
    if (node.nodeName=="LI") {
      node.onmouseover=function() {
        this.className+=" over";
        if (this.childNodes[2] && this.childNodes[2].nodeName=='UL') {
          ie6_show_iframe_mask(this.childNodes[2]);
        }
      };
      node.onmouseout=function() {
        this.className=this.className.replace(" over", "");
        ie6_hide_iframe_mask(this.childNodes[2]);
      };
    }
  }
}

function ie6_show_iframe_mask(el) {
  if (el) {
    el.style.display = 'block';
  }
  //if (!window.XMLHttpRequest) {
  var ieMat = document.createElement('iframe');
  if(document.location.protocol == "https:") {
    ieMat.src="//0";
  } else if(window.opera != "undefined") {
    ieMat.src="";
  } else {
    ieMat.src="javascript:false";
  }

  ieMat.id = 'mIframe';
  ieMat.scrolling="no";
  ieMat.frameBorder="0";
  ieMat.style.top = -1;
  ieMat.style.left= -1;
  ieMat.style.width= el.offsetWidth+"px";
  ieMat.style.height= el.offsetHeight+"px";
  ieMat.style.zIndex= -1;
  ieMat.style.filter='alpha(opacity=0)';
  el.insertBefore(ieMat, el.childNodes[0]);
  ieMat.style.position = 'absolute';
  //}
}

function ie6_hide_iframe_mask(el) {
  if (el) {
    el.style.display = 'none';
  }
  var iframe = document.getElementById('mIframe');
  if (iframe) {
    iframe.style.display = 'none';
  }
}
