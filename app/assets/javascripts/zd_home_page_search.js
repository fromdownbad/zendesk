var ZendeskSearchCursor = Class.create({
  initialize: function(options) {
    this.pages            = new Object();
    this.pages.length     = options.pageCount + 1;
    this.currentPageIndex = options.pageIndex;
  }
});

var ZendeskSearcher = Class.create({
  initialize: function(options) {
    self.latestQuery = null;
    this.options     = options;
    if (typeof(this.options.per_page) == "undefined")
      { this.options.per_page = 3; }
  },
  executeSearch: function(query, page) {
    this.latestQuery = query;

    new Ajax.Request('/suggestions/search.json', {
      requestHeaders: { Accept: 'application/json' },
      method:'get',
      parameters: {
        query: query,
        per_page: this.options.per_page,
        page: page
      },
      onSuccess: function(transport) {
        var searchResult = transport.responseText.evalJSON();
        this.results     = searchResult.results;

        // Rewrite the results to the expected format for rendering
        this.results.each(function(result) {
          result.titleNoFormatting = result.title;
          result.content           = result.body;
          result.url               = '/entries/'+result.id;
        });

        this.cursor = new ZendeskSearchCursor({ pageCount: searchResult.pages, pageIndex: searchResult.page });
        this.callbackMethod();
      }.bind(this),
      onFailure: function() {
        $('topic_suggestions').update(new Element('h2').update(this.options.noResults));
        $('topic_suggestions').removeClassName('loading');
      }.bind(this)
    });
  },
  execute: function(query) {
    return this.executeSearch(query, 1);
  },
  gotoPage: function(page) {
    if(this.latestQuery) {
      return this.executeSearch(this.latestQuery, page);
    }
    else {
      return false;
    }
  },
  setSearchCompleteCallback: function(callbackScope, callbackMethod, args) {
    this.callbackScope  = callbackScope;
    this.callbackMethod = callbackMethod.bind(callbackScope);
  }
});

var HomePageSearch = Class.create({
  initialize: function(options) {
    this.options = options;

    var moreLink  = new Element('a').update(this.options.more);
    this.more     = new Element('li', {id: 'more_suggestions'}).update(moreLink);
    this.showMore = options.showMore;
  },

  searchComplete: function() {
    $('suggestion_submit').enable();
    $('topic_suggestions').removeClassName('loading');

    Try.these(this.more.remove);

    //clear if this is a new search
    if (!this.searcher.cursor || this.searcher.cursor.currentPageIndex == 0) {
      $('topic_suggestions').update('');
    }

    if (this.searcher.results && this.searcher.results.length > 0) {
      var list = $('suggestions');
      if (!list) {
        //$('topic_suggestions').insert(new Element('h2').update(this.options.header));
        list = new Element('ul', {id: 'suggestions'});
        $('topic_suggestions').insert(list);
      };

      this.searcher.results.each(function(result) {
        //removes the account name prefix in titles
        var title = result.titleNoFormatting.gsub(/.* :: /, '');
        var link = new Element('a', {href: result.url}).update(title);

        var item = new Element('li', {className: 'suggestion'});
        item.insert(new Element('h3').update(link));
        item.insert(new Element('div').update(result.content.stripTags().truncate(160)));

        this.insert(item);
      }, list);

      if(this.searcher.cursor && this.searcher.cursor.currentPageIndex < this.searcher.cursor.pages.length - 1) {
        list.insert(this.more);
      }

    }
    else {
      if(this.options.emptyResultRedirect) {
        window.location = this.options.emptyResultRedirect + escape($F('suggestions_query'));
        return false;
      }
      else {
        $('topic_suggestions').insert(new Element('h2').update(this.options.noResults));
      }
    };
  },

  onSubmit: function() {
    $('suggestion_submit').disable();
    $('topic_suggestions').update();
    $('topic_suggestions').addClassName('loading');

    this.searcher.execute($F('suggestions_query'));

    return false;
  },

  onShowMore: function() {
    this.more.remove();
    $('topic_suggestions').addClassName('loading');
    this.searcher.gotoPage(this.searcher.cursor.currentPageIndex + 1);
    return false;
  },

  setup: function() {
    this.searcher = new ZendeskSearcher(this.options);
    this.searcher.setSearchCompleteCallback(this, this.searchComplete, null);

    $('suggest_form').onsubmit = this.onSubmit.bind(this);

    if(this.options.moreLink) {
      this.more.observe('click', function() {
        window.location = this.options.moreLink + escape($F('suggestions_query'));
      }.bind(this));
    }
    else {
      this.more.observe('click', this.onShowMore.bind(this));
    }
  }
});
