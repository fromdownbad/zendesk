// THIS FILE IS FUNCTIONALLY DEAD AND WILL BE DELETED SOON.
//**********************************
// Sources - format: sources[db field] = [condition title, action title, argument type]
//**********************************

function organizationSource() {
  if (typeof(many_organizations) !== 'undefined' && many_organizations === true) {
    return 'organization_name';
  } else {
    return 'organization_id';
  }
}

var metrics = {
  reopens: [I18n.t('txt.admin.models.rules.rule_dictionary.reopens_label'), null, "numeric"],
  replies: [I18n.t('txt.admin.models.rules.rule_dictionary.agent_replies_label'), null, "numeric"],
  group_stations: [I18n.t('txt.admin.models.rules.rule_dictionary.group_stations_label'), null, "numeric"],
  assignee_stations: [I18n.t('txt.admin.models.rules.rule_dictionary.assignee_stations_label'), null, "numeric"],
  first_reply_time_in_minutes: [I18n.t('txt.admin.models.rules.rule_dictionary.first_reply_time_in_hours_label'), null, "hours"],
  first_resolution_time_in_minutes: [I18n.t('txt.admin.models.rules.rule_dictionary.first_reply_time_in_hours'), null, "hours"],
  full_resolution_time_in_minutes: [I18n.t('txt.admin.models.rules.rule_dictionary.full_resolution_time_in_hours_label'), null, "hours"],
  agent_wait_time_in_minutes: [I18n.t('txt.admin.models.rules.rule_dictionary.agent_wait_time_hours_label'), null, "hours"],
  requester_wait_time_in_minutes: [I18n.t('txt.admin.models.rules.rule_dictionary.requester_wait_time_hours_label'), null, "hours"]
};

if(currentAccount.hasFeature('useStatusHold')) {
  metrics['on_hold_time_in_minutes'] = [I18n.t('txt.admin.models.rules.rule_dictionary.on_hold_time_hours_label'), null, "hours"];
}

var sources = {};

sources.current_via_id             = [I18n.t('txt.admin.models.rules.rule_dictionary.ticket_update_via_label'), null, "via"];
sources.via_id                     = [I18n.t('txt.admin.models.rules.rule_dictionary.ticket_channel_label'), null, "via"];
sources.subject                    = [I18n.t('txt.admin.models.rules.rule_dictionary.set_subject_label'), null, "text"];
sources.status_id                  = [I18n.t('txt.admin.models.rules.rule_dictionary.status_label'), null, "status"];
sources.priority_id                = [I18n.t('txt.admin.models.rules.rule_dictionary.priority_label'), null, "priority"];
sources.ticket_type_id             = [I18n.t('txt.admin.models.rules.rule_dictionary.type_label'), null, "ticket_type"];
sources.group_id                   = [I18n.t('txt.admin.models.rules.rule_dictionary.group_label'), null, "group_id"];
sources['group_id#current_groups'] = [I18n.t('txt.admin.models.rules.rule_dictionary.group_label'), null, "group_id#current_groups"];
sources.assignee_id                = [I18n.t('txt.admin.models.rules.rule_dictionary.assignee_label_cap'), null, "users"];
sources['assignee_id#all_agents']  = [I18n.t('txt.admin.models.rules.rule_dictionary.assignee_label_cap'), null, "users#all_agents"];
sources.cc                         = [I18n.t('txt.admin.models.rules.rule_dictionary.ccs_label'), I18n.t('txt.admin.models.rules.rule_dictionary.add_cc_label'), "all_agents#all_agents#requester_id#assignee_id"];
sources.set_tags                   = [I18n.t('txt.admin.helpers.rules_helper.tags_label'), I18n.t('txt.admin.models.rules.rule_dictionary.set_tags_label'), "tags"];
sources.current_tags               = [I18n.t('txt.admin.helpers.rules_helper.tags_label'), I18n.t('txt.admin.models.rules.rule_dictionary.add_tags_label'), "tags"];
sources.remove_tags                = [I18n.t('txt.admin.helpers.rules_helper.tags_label'), I18n.t('txt.admin.models.rules.rule_dictionary.remove_tags_label'), "tags"];
sources.requester_id               = [I18n.t('txt.admin.models.rules.rule_dictionary.requester_label_cap'), null, "users"];
sources['requester_id#all_agents'] = [I18n.t('txt.admin.models.rules.rule_dictionary.requester_label_cap'), null, "users#all_agents"];
sources[organizationSource()]      = [I18n.t('txt.admin.models.rules.rule_dictionary.organization_label'), null, "organizations"];
sources.role                       = [I18n.t('txt.admin.models.rules.rule_dictionary.current_user_label_cap'), null, "role"];
sources.comment_is_public          = [I18n.t('txt.admin.models.rules.rule_dictionary.comment_is_label'), null, "comment_is_public"];
sources.comment_includes_word      = [I18n.t('txt.admin.models.rules.rule_dictionary.comment_text_label'), null, "text"];
sources.description_includes_word  = [I18n.t('txt.admin.models.rules.rule_dictionary.description_label'), null, "text"];
sources.ticket_is_public           = [I18n.t('txt.admin.models.rules.rule_dictionary.ticket_is_public_label'), null, "ticket_is_public"];
sources.update_type                = [I18n.t('txt.admin.models.rules.rule_dictionary.ticket_is_label'), null, "update_type"];
//sources.linked_id                = ["Link status", null, "link_status"];
sources.recipient                  = [I18n.t('txt.admin.models.rules.rule_dictionary.ticket_received_at_label'), null, "recipient"];
sources.resolution_time            = [I18n.t('txt.admin.models.rules.rule_dictionary.resolution_time_in_hours_label'), null, "numeric"];
sources.in_business_hours          = [I18n.t('txt.admin.models.rules.rule_dictionary.within_business_hours_label'), null, "flag"];
/*sources.has_attachments          = ["Has Attachment", null, "na"];*/
if(currentAccount.hasFeature('customerSatisfaction')) {
  sources.satisfaction_score         = [I18n.t('txt.admin.models.rules.rule_dictionary.ticket_satisfaction_label'), null, "satisfaction_score"];
}

sources.NEW                        = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_created_label'), null, "hours"];
sources.OPEN                       = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_opened_label'), null, "hours"];
sources.PENDING                    = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_pending_label'), null, "hours"];
sources.HOLD                       = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_hold_label'), null, "hours"];
sources.SOLVED                     = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_solved_label'), null, "hours"];
sources.CLOSED                     = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_closed_label'), null, "hours"];

sources.assigned_at                = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_assigned_label'), null, "hours"];
sources.updated_at                 = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_update_label'), null, "hours"];
sources.requester_updated_at       = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_requester_update_label'), null, "hours"];
sources.assignee_updated_at        = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_assignee_update_label'), null, "hours"];
sources.due_date                   = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_since_due_date_label'), null, "hours"];

sources.until_due_date             = [I18n.t('txt.admin.models.rules.rule_dictionary.hours_until_due_date_label'), null, "hours"];

sources.notification_user          = [null, I18n.t('txt.admin.models.rules.rule_dictionary.email_user_label'), "notification_user"];
sources.notification_group         = [null, I18n.t('txt.admin.models.rules.rule_dictionary.email_group_label'), "notification_group"];
sources.notification_target        = [null, I18n.t('txt.admin.models.rules.rule_dictionary.notify_target_label'), "notification_target"];
sources.comment_value              = [null, I18n.t('txt.admin.models.rules.rule_dictionary.comment_description_label'), "textarea"];
sources.comment_mode_is_public     = [null, I18n.t('txt.admin.models.rules.rule_dictionary.comment_mode_label'), "comment_mode_is_public"];
sources.tweet_requester            = [null, I18n.t('txt.admin.models.rules.rule_dictionary.tweet_requester_label'), "tweet_requester"];
sources.share_ticket               = [null, I18n.t('txt.admin.models.rules.rule_dictionary.share_ticket_with_label'), 'sharing_agreements'];


sources.macro_id                   = [I18n.t('txt.admin.models.rules.rule_dictionary.apply_macro_label'), null, "macro_id"];
sources.macro_id_after             = [I18n.t('txt.admin.models.rules.rule_dictionary.apply_macro_after_update_label'), null, "macro_id"];

sources.reopens                    = [I18n.t('txt.admin.models.rules.rule_dictionary.reopens_label'), null, "numeric"];
sources.replies                    = [I18n.t('txt.admin.models.rules.rule_dictionary.agent_replies_label'), null, "numeric"];
sources.agent_stations             = [I18n.t('txt.admin.models.rules.rule_dictionary.assignee_stations_label'), null, "numeric"];
sources.group_stations             = [I18n.t('txt.admin.models.rules.rule_dictionary.group_stations_label'), null, "numeric"];


sources.locale_id                         = [I18n.t('txt.admin.models.rules.rule_dictionary.requesters_language_label'), null, "locale_id"];
sources.requester_twitter_followers_count = [I18n.t('txt.admin.models.rules.rule_dictionary.twitter_followers_are_label'), null, "numeric"];
sources.requester_twitter_statuses_count  = [I18n.t('txt.admin.models.rules.rule_dictionary.number_of_tweets_is_label'), null, "numeric"];
sources.requester_twitter_verified        = [I18n.t('txt.admin.models.rules.rule_dictionary.is_verified_by_twitter_label'), null, "bool"];

$j.each(metrics, function(key, value) {
  sources[key] = value;
});

sources.DELIMITER                  = ["-----------------------", null, "na"];
sources.PICKCONDITION              = ["-- " + I18n.t('txt.admin.public.javascript.zd_rule_data.click_to_select_condition_label')+ " --", null, "na"];
sources.PICKACTION                 = ["-- " + I18n.t('txt.admin.public.javascript.zd_rule_data.click_to_select_action_label') + " --", null, "na"];

function getSourceConditionTitle(key) {
  return sources[key][0];
}

function getSourceActionTitle(key) {
  return sources[key][1];
}

function getSourceType(key) {
  return sources[key][2];
}

//**********************************
// Source lists
//**********************************

var sourcePickList = {};
pickListConditions = [
    "PICKCONDITION"
  , "status_id"
  , "ticket_type_id"
  , "priority_id"
  , "group_id"
  , "assignee_id"
  , "requester_id"
  , organizationSource()
  , "current_tags"
  , "description_includes_word"
  , "via_id"
  , "recipient"
  , "locale_id"
  , currentAccount.hasFeature('customerSatisfaction') ? "satisfaction_score" : nil
  , "DELIMITER"
  , "NEW"
  , "OPEN"
  , "PENDING"
  , currentAccount.hasFeature('useStatusHold') ? "HOLD" : nil
  , "SOLVED"
  , "CLOSED"
  , "assigned_at"
  , "updated_at"
  , "requester_updated_at"
  , "assignee_updated_at"
  , "due_date"
  , "until_due_date"
].compact();

pickListActions    = [
    "PICKACTION"
  , "status_id"
  , "priority_id"
  , "ticket_type_id"
  , "group_id#current_groups"
  , "assignee_id#all_agents"
  , currentAccount.hasFeature('customerSatisfaction') ? "satisfaction_score" : nil
  , "DELIMITER"
  , "set_tags"
  , "current_tags"
  , "remove_tags"
  , "DELIMITER"
  , "notification_user"
  , "notification_group"
  , "notification_target"
  , "tweet_requester"
  , currentAccount.hasFeature('collaboration') ? 'cc' : null
  , currentAccount.hasFeature('ticketSharingTriggers') ? 'share_ticket' : null
].compact();



switch (window.rule_type) {
  case "View":
    sourcePickList.condition = pickListConditions;
    break;
  case "Automation":
    sourcePickList.condition = pickListConditions;
    sourcePickList.action    = pickListActions;
    if (currentAccount.hasFeature('cms')) {
      sourcePickList.action = sourcePickList.action.concat(["DELIMITER", "locale_id"]);
    }
    if (currentAccount.rulesCanReferenceMacros) {
      sourcePickList.action = sourcePickList.action.concat(["DELIMITER", "macro_id"]);
    }
    break;
  case "Trigger":
    conditions = [
        "PICKCONDITION"
      , "status_id"
      , "ticket_type_id"
      , "priority_id"
      , "group_id#current_groups"
      , "assignee_id#all_agents"
      , "requester_id#all_agents"
      , organizationSource()
      , "current_tags"
      , "via_id"
      , "current_via_id"
      , "role"
      , currentAccount.hasFeature('customerSatisfaction') ? "satisfaction_score" : nil
      , "DELIMITER"
      , "update_type"
      , "recipient"
      , "ticket_is_public"
      , "comment_is_public"
      , "comment_includes_word"
    ].compact();
    if (currentAccount.showExtendedTicketMetrics) {
      conditions = conditions.concat([
          "DELIMITER"
        , "reopens"
        , "replies"
        , "agent_stations"
        , "group_stations"
      ]);
    }
    if (currentAccount.hasFeature('businessHours')) {
      conditions = conditions.concat(["in_business_hours"]);
    }
    sourcePickList.condition = conditions.concat([
        "DELIMITER"
      , "locale_id"
      , "requester_twitter_followers_count"
      , "requester_twitter_statuses_count"
      , "requester_twitter_verified"
    ]);
    sourcePickList.action = pickListActions;
    if (currentAccount.hasFeature('cms')) {
      sourcePickList.action = sourcePickList.action.concat(["DELIMITER", "locale_id"]);
    }
    if (currentAccount.rulesCanReferenceMacros) {
      sourcePickList.action = sourcePickList.action.concat(["DELIMITER", "macro_id_after"]);
    }
    break;
  case "Macro":
    sourcePickList.action = [
        "PICKACTION"
      , "subject"
      , "status_id"
      , "priority_id"
      , "ticket_type_id"
      , "group_id"
      , "assignee_id#all_agents"
      , "DELIMITER"
      , "set_tags"
      , "current_tags"
      , "remove_tags"
      , "DELIMITER"
      , "comment_value"
      , "comment_mode_is_public"
    ];
    break;
  case "Report":
    sourcePickList.condition = [
        "PICKCONDITION"
      , "priority_id"
      , "ticket_type_id"
      , "group_id"
      , "assignee_id"
      , organizationSource()
      , "current_tags"
      , "via_id"
      , "resolution_time"
      , currentAccount.hasFeature('customerSatisfaction') ? "satisfaction_score" : nil
      , "locale_id"
    ].compact();
    if (currentAccount.showExtendedTicketMetrics) {
      sourcePickList.condition.push("DELIMITER");
      $j.each(metrics, function(key, value) {
        sourcePickList.condition.push(key);
      });
    }
    break;
}

//**********************************
// Argument drop downs - i.e. items
//**********************************

items.tags = [
  {value: 'includes', text: I18n.t('txt.admin.models.rules.rule_dictionary.contains_at_least_one_of_the_following_label')},
  {value: 'not_includes', text: I18n.t('txt.admin.models.rules.rule_dictionary.contains_none_of_the_following_label')}
];

items.text = [
  {value: 'includes', text: I18n.t('txt.admin.models.rules.rule_dictionary.contains_at_least_one_word_label')},
  {value: 'not_includes', text: I18n.t('txt.admin.models.rules.rule_dictionary.contains_none_of_following_words__label')},
  {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.contains_following_string_label')},
  {value: 'is_not', text: I18n.t('txt.admin.models.rules.rule_dictionary.contains_not_following_string_label')}
];

items.numeric = [
  {value: 'less_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.less_than_label_cap')},
  {value: 'greater_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.greater_than_label_cap')},
  {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_label_cap')}
];

if (currentAccount.hasFeature('businessHours')) {
  items.hours = [
    {value: 'less_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.calendar_less_than_label')},
    {value: 'greater_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.calendar_greater_than_label')},
    {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.calendar_is_label')},
    {value: 'less_than_business_hours', text: I18n.t('txt.admin.models.rules.rule_dictionary.business_less_than_label')},
    {value: 'greater_than_business_hours', text: I18n.t('txt.admin.models.rules.rule_dictionary.business_greater_than_label')},
    {value: 'is_business_hours', text: I18n.t('txt.admin.models.rules.rule_dictionary.business_is_label')}
  ];
} else {
  items.hours = [
    {value: 'less_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.less_than_label_cap')},
    {value: 'greater_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.greater_than_label_cap')},
    {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_label_cap')}
  ];
}

if (window.rule_type !== 'Trigger') {
  items.key = [
    {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_label_cap')},
    {value: 'is_not', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_not_label_cap')}
  ];

  items.key_expanded = [
    {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_label_cap')},
    {value: 'is_not', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_not_label_cap')},
    {value: 'less_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.less_than_label_cap')},
    {value: 'greater_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.greater_than_label_cap')}
  ];
}
else {
  items.key = [
    {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_label_cap')},
    {value: 'is_not', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_not_label_cap')},
    {value: 'changed', text: I18n.t('txt.admin.models.rules.rule_dictionary.changed_label_cap')},
    {value: 'value', text: I18n.t('txt.admin.models.rules.rule_dictionary.changed_to_label_cap')},
    {value: 'value_previous', text: I18n.t('txt.admin.models.rules.rule_dictionary.changed_from_label_cap')},
    {value: 'not_changed', text: I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_label')},
    {value: 'not_value', text: I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_to_label_cap')},
    {value: 'not_value_previous', text: I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_from_label_cap')}
  ];

  items.key_expanded = [
    {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_label_cap')},
    {value: 'is_not', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_not_label_cap')},
    {value: 'less_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.less_than_label_cap')},
    {value: 'greater_than', text: I18n.t('txt.admin.models.rules.rule_dictionary.greater_than_label_cap')},
    {value: 'changed', text: I18n.t('txt.admin.models.rules.rule_dictionary.changed_label_cap')},
    {value: 'value', text: I18n.t('txt.admin.models.rules.rule_dictionary.changed_to_label_cap')},
    {value: 'value_previous', text: I18n.t('txt.admin.models.rules.rule_dictionary.changed_from_label_cap')},
    {value: 'not_changed', text: I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_label')},
    {value: 'not_value', text: I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_to_label_cap')},
    {value: 'not_value_previous', text: I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_from_label_cap')}
  ];
}

items.key_simple = [
  {value: 'is', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_label_cap')},
  {value: 'is_not', text: I18n.t('txt.admin.models.rules.rule_dictionary.is_not_label_cap')}
];

items.comment_is_public = [
  {value: 'true', text: I18n.t('txt.admin.models.rules.rule_dictionary.public_label')},
  {value: 'false', text: I18n.t('txt.admin.models.rules.rule_dictionary.private_label')},
  {value: 'not_relevant', text: I18n.t('txt.admin.models.rules.rule_dictionary.presente_label_caps')},
  {value: 'requester_can_see_comment', text: I18n.t('txt.admin.models.rules.rule_dictionary.present_requester_can_see_label_caps')}
];

items.comment_mode_is_public = [
  {value: 'true', text: I18n.t('txt.admin.models.rules.rule_dictionary.public_label')},
  {value: 'false', text: I18n.t('txt.admin.models.rules.rule_dictionary.private_label')}
];

items.update_type = [
  {value: 'Create', text: I18n.t('txt.admin.models.rules.rule_dictionary.created_label')},
  {value: 'Change', text: I18n.t('txt.admin.models.rules.rule_dictionary.updated_label')}
];

items.flag = [
  {value: 'true', text: I18n.t('txt.admin.models.rules.rule_dictionary.yes_label')},
  {value: 'false', text: I18n.t('txt.admin.models.rules.rule_dictionary.no_label')}
];

items.via = [
  {value: '0', text: I18n.t('txt.admin.models.rules.rule_dictionary.web_form_label')},
  {value: '4', text: I18n.t('txt.admin.models.rules.rule_dictionary.email_label')},
  {value: '29', text: I18n.t('txt.admin.models.rules.rule_dictionary.chat_label')},
  {value: '30', text: I18n.t('txt.admin.models.rules.rule_dictionary.twitter_label')},
  {value: '26', text: I18n.t('txt.admin.models.rules.rule_dictionary.twitter_dm_label')},
  {value: '23', text: I18n.t('txt.admin.models.rules.rule_dictionary.twitter_favorites_label')},
  {value: '33', text: I18n.t('txt.admin.models.rules.rule_dictionary.voicemail_label')},
  {value: '34', text: I18n.t('txt.admin.models.rules.rule_dictionary.phone_call_label')},
  {value: '35', text: I18n.t('txt.admin.models.rules.rule_dictionary.phone_call_outbound_label')},
  {value: '44', text: I18n.t('txt.admin.models.rules.rule_dictionary.cti_voicemail_label')},
  {value: '45', text: I18n.t('txt.admin.models.rules.rule_dictionary.cti_inbound_label')},
  {value: '46', text: I18n.t('txt.admin.models.rules.rule_dictionary.cti_outbound_label')},
  {value: '16', text: I18n.t('txt.admin.models.rules.rule_dictionary.get_satisfaction_label')},
  {value: '17', text: I18n.t('txt.admin.models.rules.rule_dictionary.feedback_tab_label')},
  //{value: '18', text: 'JIRA Widget'},
  {value: '5', text: I18n.t('txt.admin.models.rules.rule_dictionary.web_service_api_label')},
  {value: '8', text: I18n.t('txt.admin.models.rules.rule_dictionary.automation_label')},
  {value: '24', text: I18n.t('txt.admin.models.rules.rule_dictionary.forum_topic_label')},
  {value: '27', text: I18n.t('txt.admin.models.rules.rule_dictionary.closed_ticket_label')},
  {value: '31', text: I18n.t('txt.admin.models.rules.rule_dictionary.ticket_sharing_label')},
  {value: '38', text: I18n.t('txt.admin.models.rules.rule_dictionary.facebook_post_label')},
  {value: '41', text: I18n.t('txt.admin.models.rules.rule_dictionary.facebook_message_label')}
];

// Remove choices from accounts that do not have features enabled.
items.via = _(items.via).reject(function(i) {
  return i.feature && !Zendesk.currentAccount.hasFeature(i.feature);
});

items.satisfaction_actions = [
  { value: '1',  text: I18n.t('txt.admin.models.rules.rule_dictionary.offered_to_requester_label') }
];

items.satisfaction_conditions = [
  { value: '0',   text: I18n.t('txt.admin.models.rules.rule_dictionary.unoffered_label') },
  { value: '1',   text: I18n.t('txt.admin.models.rules.rule_dictionary.offered_label') },
  { value: '4',   text: I18n.t('txt.admin.models.rules.rule_dictionary.bad_label') },
  { value: '5',   text: I18n.t('txt.admin.models.rules.rule_dictionary.bad_with_comment_label')},
  { value: '16',  text: I18n.t('txt.admin.models.rules.rule_dictionary.good_label') },
  { value: '17',  text: I18n.t('txt.admin.models.rules.rule_dictionary.good_with_comment_label')}
];

//**********************************
//**********************************
function initializeCustomFieldsForRules() {
  fields = [];
  if ($j.isArray(items.taggers))    fields = fields.concat(items.taggers);
  if ($j.isArray(items.checkboxes)) fields = fields.concat(items.checkboxes);

  if (!fields.any()) return;

  fields.each(function(field) {
    sources['ticket_fields_' + field.id] = [field.title, null, field.type.toLowerCase()];
  });

  addToPickList(sourcePickList.action, fields);
  addToPickList(sourcePickList.condition, fields);

  function addToPickList(list, fields) {
    if (list == undefined) return;
    list.push('DELIMITER');
    fields.each(function(item) { list.push('ticket_fields_' + item.id); });
  }
}

initializeCustomFieldsForRules();
