alert("maybe minislas cannot be removed...");
var SlaChecker = Class.create();
SlaChecker.prototype = {
	initialize: function() { },
	startPolling: function() {
		this.started = true;
		this.pe      = new PeriodicalExecuter(this.poll, 3)
	},
	poll: function(pe) {
		if (!this.polling_started) {
			this.polling_started = true;
			new Ajax.Request('/reports/get_minislagraphs_if_created', {asynchronous:true})
			this.polling_started = false;
		}
	},
	stopPolling: function() {
		this.pe.stop();
		this.started = false;
	}
}
