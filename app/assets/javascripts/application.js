// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
// Busting caches

var zd = { // used by new modular JS, but needs to be defined before data is pushed into it within the HTML body
  jsInitializers: [],
  jsData: {}
};

$j(document).ready(function(){
  $j("a.colorbox").live('click', function() {
    var options = {}
    var url = $j(this).attr('href');

    if($j(this).data('colorbox-force-photo')){
      options["photo"] = true;
    }

    if($j(this).data('colorbox-max-width')){
      options["maxWidth"] = $j(this).data('colorbox-max-width')
    }

    // point href to a id or selector and load it inline (hide it with display: none)
    if($j(this).data('colorbox-inline')) {
      options["html"] = $j(url).html(); // just the inner without the display:none container
    } else {
      options["href"] = url;
    }

    $j.colorbox(options);
    return false;
  });

  $j('[data-learn-more-block]').click(function() {
    $j(this).hide();
    $j($j(this).attr('data-learn-more-block')).slideDown();
    return false;
  });

  // Firefox has a bug related to the change of location.hash value, the favicon disappears after that.
  // With this we refresh the favicon link after any change in the location.hash value.
  $j(window).hashchange( function() {
    var favicon = $j("head link[rel='shortcut icon']").remove();
    $j('<link href="'+ favicon.attr("href") +'" rel="shortcut icon" type="'+ favicon.attr("type") +'" />').appendTo('head');
  });

  // Convert all Screencasts links in forums into an iframe showing the video.
  $j('p.zendesk-embedded-screencast').replaceWith(function() {
    return $j('<iframe width="570" height="340"/>').attr('src', $j(this).find('a').attr('href'));
  });

  $j(".only-if-solved").hide();
});

var items = {}; // For rule data

// Ticket functions - create/edit

function renderPerType() {
  if ($j("#ticket_ticket_type_id").length) {
    if ($j("#ticket_linked_id").length) {
      var showProblemSelect = ($j("#ticket_ticket_type_id").val() === "2");
      setVisibility("#ticket_link, #ticket_link > span", showProblemSelect);
    }
    if ($j("#ticket_date").length) {
      setVisibility("#ticket_date", ($j("#ticket_ticket_type_id").val() === "4"));
      if ($j("#ticket_due_date").val() === "") {
        var today = new Date();
        today.setDate(today.getDate() + 7);
        var endUserFormatted = jQuery.datepicker.formatDate(I18n.t('date.datepicker.date_format'), today);
        $j("#ticket_due_date").val(endUserFormatted);
        var rubyFormatted = jQuery.datepicker.formatDate('yy-mm-dd', today);
        $j("#hidden_date").val(rubyFormatted);
      }
    }
  }
}
var render_per_type = renderPerType;

function toggleRequiredFields() {
  if ($j("#ticket_status_id").val() === "3") {
    $j(".only-if-solved").show();
  } else {
    $j(".only-if-solved").hide();
  }
}

function hideNavigationLinks() {
  $j('#top-menu li').hide();
  $j('#top-right').hide();
  $j('#add_widget_button').hide();
  document.observe('widgets:load', function() {
    $j('#add_widget_button').hide();
  });
}

// Assignee select - ticket create/edit
function assigneeSelect(groupId, userId) {

  var assigneeSelectElm = $j('#ticket_assignee_id');

  assigneeSelectElm.html($j('<option>')); // clear current assignees in the list, but keep a blank option

  if (!assigneeSelectElm.exists() || groupId === '' || typeof groupId === 'undefined') {
    return $j.when();
  }

  var group, groupAgentIDs;
  groupId = Number(groupId);

  if (groupId >= 0) {

    group = _(window.groups).find(function(group) {
      return $j.when(group.id === groupId);
    });

    groupAgentIDs = _(group.users);

    _(window.agents)
      .select(function(agent) {
        return $j.when(groupAgentIDs.contains(agent.id));
      })
      .each(function(agentInGroup) {
        assigneeSelectElm.append(
          $j('<option />')
            .val("" + agentInGroup.id)
            .html("" + agentInGroup.name)
            .prop('selected', agentInGroup.id === Number(userId))
        );
      });

  } else {

    assigneeSelectElm.get(0).selectedIndex = 0;
    assigneeSelectElm.prop('enabled', false);

  }

  assigneeSelectElm
    .change()
    .blur();

  return $j.when([]);
}
var assignee_select = assigneeSelect;

function updateProperties(assignee_id) {
  // Highlight missing properties on ticket form
  var groupSelector = $j("#ticket_group_id");
  if (groupSelector.exists() &&
      $j.trim(groupSelector.val()) !== '' &&
      $j.trim($j("#ticket_assignee_id").val()) === '') {
    assigneeSelect(groupSelector.val(), assignee_id);
  }
  renderPerType();
  return true;
}

// Highlights agents in ticket cc list
function highlightAgents(){
  // Parse CC-List
  $j('#facebook-list li.choice').each(function(i, ccUser) {
    ccUser = $j(ccUser);
    var choiceID = ccUser.attr('choice_id');
    if (_(agents).any(function(agent) { return agent.id === Number(choiceID); })) {
      ccUser.addClass('agent');
    }

    if (_(window.lightAgents).any(function(agent) { return agent.id === Number(choiceID); })) {
      ccUser.addClass('agent');
    }
  });
}

function setupHighlightAgents()
{
  highlightAgents();
  $j('#facebook-list ul.multi_value_field').first().bind('DOMNodeInserted', highlightAgents);
}

// Generic functions

function setVisibility(selector, want_visible) {
  if (want_visible) {
    $j(selector).show();
  } else {
    $j(selector).hide();
  }
}

function submitUpload() {
  var filename = $j.trim($j('#attachment_content').val());
  if (filename !== '') {
    $j('#uploadform').submit();
    $j('#uploadinput').hide();
    $j('#uploadbar').show();
  }
}

function notifyOnError(name) {
  alert("Failed to upload file '"+name+"', verify the path");
  resetView();
}

var attachmentLists = [];

function registerAttachmentList(listId, size) {
  if (size === null) { attachmentLists[listId] = $j(".attach_item").length; }
  else { attachmentLists[listId] = size; }
}

function deleteFromAttachmentList(listId, itemId) {
  $j('#' + itemId).remove();
  attachmentLists[listId] = attachmentLists[listId]-1;
  if (attachmentLists[listId] <= 0) {
    $j('#' + listId).hide();
    delete attachmentLists[listId];
  }
}

function showFlash(flash, messageType) {
  messageType = (typeof messageType == 'undefined') ? 'notice' : messageType;
  var jsFlash = $j('<div>', { id: messageType, html: flash });

  $j('#flash').html(jsFlash);
  new Effect.Highlight('flash', { duration: 2 });
}

function clearFlash() {
  $j('#flash').html('');
}

// **** Upload photo stuff
function pictureDeleted(key) {
  $j('#cancel-block-' + key).hide();
  if (key === 'header_logo') { $j('#sub_setting_website_url').hide(); }
  selectPicture(key);
}

function selectPicture(key) {
  $j('#image-block-' + key).hide();
  $j('#upload-block-' + key).show();
  $j('#ignore-upload-' + key).val(0);
}

function resetPicture(key) {
  $j('#upload-block-' + key).hide();
  $j('#image-block-' + key).show();
  $j('#ignore-upload-' + key).val(1);
}

function checkTicketDelete() {
  if ($j('#submit_type').val() === 'delete') {
    alert(I18n.t('txt.public.javascripts.application.warning_deleted_tickets_cannot_be_recovered'));
  }
}

function check_ticket(ticket_id) {
  // show lightbox warning if we're solving a problem ticket that has associated incident tickets
  if (IncidentsWarning.getInstance().isShowingWarning()) {
    return false;
  }

  $('submit-button').value = I18n.t('txt.admin.views.suspended_tickets.index.submitting_label');
  $('submit-button').disabled = true;

  if (typeof(collaboratorList) !== 'undefined')
    collaboratorList.update();

  // Remove requester/cc if it is not visible - so we don't need to update it in the ticket model
  if ($F('submit_type') !== 'merge' && $('edit_requester').style.display === 'none')
    $('edit_requester').remove();

  return true;
}

function copySubmitType() {
  $('ticket-chat').submit_type.value = $('submit_form').submit_type.value;
}

function submitTicketForm() {
  if (check_ticket()) {
    // strip null characters from input fields
    // to put a bad string in the pasteboard: `echo -ne 'Bad\0\0\0\0 message' | pbcopy`
    $j('#ticket-chat :input').each(function() {
      this.value = this.value.replace(/\0/g,'');
    });
    copySubmitType();
    if ($F('submit_type') === 'merge') {
      new TicketMergeWizard();
    } else {
      if (typeof(ticketTagField) !== 'undefined') {
        ticketTagField.beforeFormSubmit();
      }
      // TODO: if we can get the ticket form event handlers to all
      // use jQuery instead of Prototype, we can change
      // $('ticket-chat').submit() $j('#ticket-chat').submit()
      // and use $j('#ticket-chat').instrumentTracking() and
      // data attributes instead of this manual tracking call.
      if ($j('#ticket_status_id').val() === '3') {
        Zendesk.Instrumentation.track('Solved', 'Ticket');
      }
      Zendesk.Instrumentation.track('Save', 'Ticket');

      $j(document).trigger("ticket.submit.zendesk");
      $('ticket-chat').submit();
    }
  }
}

function submitBulkUpdateForm() {
  if (fetchTicketsToBulkupdate()) {
    if ($j('#submit_type').val() === 'merge') {
      new TicketMergeWizard();
    } else {
      if ($j('#ticket_ticket_type_id').val() !== "4") {
        $j('#ticket_date').remove(); // Remove due date if the ticket is not a task
      }
      $j('#ticket-chat').submit();
    }
  }
}

function copySolvedState() {
  if ( $('submit_form').solved_true ) {
    $('ticketform').ticket_force_status_change.value = $('submit_form').solved_true.checked;
  }
}

function submitRequestForm() {
  $j('#submit-button').prop('disabled', true);
  // TODO: change this to jQuery at the same time you change event handlers
  $('ticketform').submit();
}

function submitAttachForm(uploading_msg) {
  if ($j('#uploads_attribute').length) {
    $j('#token').val($j('#uploads_attribute').val());
  }
  $j('#submit-button').val(uploading_msg);
  $j('#submit-button').prop('disabled', true);
  $j('#attach_form').submit();
  $j('#attach_link').hide();
  $j('#uploading_message').show();
}

function setSelectedForSelect(select, selectedValue) {
  $j(select).val(selectedValue).change();
}

// Autotagging for ticket
function autotagTicket(element) {
  var value = $j(element).val();
  if ($j.trim(value) !== '' && ticketTagField && ticketTagField.selectedEntries().length === 0) {
    new Ajax.Request('/tags/autotag', {
      method: 'GET',
      parameters: { text: value, target: 'ticketTagField' }
    });
  }
}
var autotag_ticket = autotagTicket;

// ***** jQuery UI Tooltip


function createTooltip(elementId, title, body, options) {
  var content = "<div class='title clearfix " + options.title + "'>" + title + "</div>" +
                "<div class='content clearfix " + options.title + "'>" + body + "</div>";
  $j('#' + elementId)
    .addClass('has-tooltip')
    .tooltip({
      tooltipClass: 'zd_comment ' + options['class'],
      items: '.has-tooltip',
      position: options.position,
      content: function() { return content; }
    })
    .unbind('focusin'); // some browsers trigger these when the window refocuses
}

function commentTip(elementId, title, body) {
  var options = [];
  options.position = { my: 'left top', at: 'center bottom', offset: '50px 10px' };
  createTooltip(elementId, title, body, options);
}
var comment_tip = commentTip;

function cmsCommentTip(elementId, title, body) {
  var options = [];
  options.title    = "cms_tooltip_title";
  options["class"] = "cms_tooltip";
  options.position = { offset: '50px 10px' };
  createTooltip(elementId, title, body, options);
}

// *****

function reverseTruncate(string, length) {
  return string.split('').reverse().join('').truncate(length).split('').reverse().join('');
}

// ***** Macro stuff

function apply_macro(case_id, evt) {
  if (case_id !== '') {
    if (typeof(ticket_id) === 'undefined') {
      this_id = '0';
    } else {
      this_id = ticket_id;
    }

    var ticketForm = jQuery("#ticket-chat");
    if (jQuery("#tickets_to_bulk_update", ticketForm).length > 0) {
      new Zendesk.API.Macro(case_id).applyToTicket(null, ticketForm);
    } else {
      new Zendesk.API.Macro(case_id).applyToTicket(this_id, ticketForm);
    }

    var e = (evt) ? evt: window.event;
    Event.stop(e);
    var li = e.findElement();
    selection_feedback(li);
    return false;
  }
}

var hideMacroLi = function(li) {
  // using element storage to retrieve the currently open li and close it
  Zendesk.UI && Zendesk.UI.NestedMenu && Zendesk.UI.NestedMenu.close_submenu($j(li));
};

function showOrHideBulkForm() {
  fetchTicketsToBulkupdate();
  if ($j.trim($j('#tickets_to_bulk_update').val()) === '') {
    $j('#bulk-update').hide();
  } else {
    $j('#bulk-update').show();
  }
}

function fetchTicketsToBulkupdate() {
  var value = _($j('input.tickets_to_bulk_update'))
    .select(function(input) { return input.checked; })
    .map(function(input) { return $j(input).val(); })
    .join(',');
  $j('#tickets_to_bulk_update').val(value);
  return true;
}

function selectAllTicketsForBulkUpdate(elm) {
  var state = elm.checked;
  if ($j('#bulk-update').length) {
    if (state) {
      $j('#bulk-update').show();
    } else {
      $j('#bulk-update').hide();
    }
    $j('input.tickets_to_bulk_update').each(function(s) { $j(this).prop('checked', state); });
  }
}

// Color picking and calculations for account
var rgbToHex = function(str) { // e.g. rgb(0, 122, 255)
  var hexColor = '#';
  str.match(/\d+/g).each(function(val) {
    hexColor += Number(val).toColorPart();
  });
  return hexColor;
};


// ********* Sorting
Ordering = {
  SetOrder: function(table){
    $j(table).hide();
    $j(table + '_sort')
      .sortable({ axis: 'y' })
      .show();
  },

  cancelOrdering: function(listOfItems) {
    var sortableList = $j(listOfItems + "_sort");
    $j(listOfItems).show();

    sortableList.hide().sortable('destroy');
    var sortByOriginalPosition = function(a, b) {
      var attrName = "data-zendesk-original-position";
      return ($j(a).attr(attrName) - $j(b).attr(attrName));
    };

    var sortedRules = $j("ul li.item.sortable", sortableList).sort(function(a, b) {
      return sortByOriginalPosition(a, b);
    });
    $j("ul li.item.sortable", sortableList).remove();
    $j("ul", sortableList).prepend(sortedRules);
  }
};

/* Textarea auto-resize. http://www.felgall.com/jstip45.htm */
// This can maybe go away, no internal dependencies on this anymore at least
function textareaResize() {
  var t = this;
  var a = t.value.split('\n');
  var b=1;
  for (x=0;x < a.length; x++) {
    if (a[x].length >= t.cols) { b+= Math.floor(a[x].length/t.cols); }
  }
  b += a.length;
  if (b > t.rows) { t.rows = b; }
}
var textarea_resize = textareaResize;

//Javacript difference in time in words - http://nullstyle.com/2007/06/02/caching-time_ago_in_words/
function time_ago_in_words(from) {
  return distance_of_time_in_words(new Date(), new Date(from));
}

function i18n_time_ago_in_words(from) {
  return i18n_distance_of_time_in_words(new Date(), new Date(from));
}

function distance_of_time_in_words(to, from) {
  var seconds_ago = ((to  - from) / 1000);
  var minutes_ago = Math.floor(seconds_ago / 60);

  if(minutes_ago <= 0) { return "less than a minute";}
  if(minutes_ago === 1) { return "a minute";}
  if(minutes_ago < 45) { return minutes_ago + " minutes";}
  if(minutes_ago < 90) { return "1 hour";}
  var hours_ago  = Math.round(minutes_ago / 60);
  if(minutes_ago < 1439) { return hours_ago + " hours";}
  if(minutes_ago < 2879) { return "1 day";}
  var days_ago  = Math.round(minutes_ago / 1440);
  if(minutes_ago < 43199) { return days_ago + " days";}
  if(minutes_ago < 86399) { return "1 month";}
  var months_ago  = Math.round(minutes_ago / 43200);
  if(minutes_ago < 525960) { return months_ago + " months";}
  if(minutes_ago < 1051920) { return "1 year";}
  var years_ago  = Math.round(minutes_ago / 525960);
  return "over " + years_ago + " years";
}

/*
  Object factory. Used to allow multiple occurences of the same object type on the same page
  Use like so:

  ObjectFactory.create(TextWidget, {
    id: <%= @widget.id %>, params: <%= @widget.params.to_json %>
  });

  This instantiates and returns a new instance of the class specified. The instance gets initialized
  with an args parameter, containing the property list (eg. { id: 4, params: 'horse' }). The id must be
  set as it is used to lookup the instance later. So, to get the instance reference:

  ObjectFactory.get(<%= @widget.id%>).dance_the_funky_chicken_dance();
*/
var ObjectFactory       = Class.create({});
ObjectFactory.instances = new Hash();

ObjectFactory.create    = function(type, args) {
  instance = new type(args);
  ObjectFactory.instances.set(args.id, instance);
  return instance;
};

ObjectFactory.get = function(id) {
  return ObjectFactory.instances.get(id);
};

ObjectFactory.remove = function(id) {
  return ObjectFactory.instances.unset(id);
};

document.getUrlParameter = function(name) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(window.location.href);
  return results ? unescape(results[1]) : null;
};

// Will format strings in human readable format... addCommas(1234567890) -> 1,234,567,890
function addCommas(nStr) {
  nStr += '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(nStr)) {
    nStr = nStr.replace(rgx, '$1' + ',' + '$2');
  }
  return nStr;
}

function showSortingForForumType(type_id) {
  $j("#div_ordering").toggle(type_id == "1");
}
