class AppLinkPresenter
  def present_for_apple(app_links)
    {
      applinks: {
        apps: [],
        details: app_links.map do |app|
          {
            appID: app.app_id,
            paths: app.paths
          }
        end
      }
    }
  end

  def present_for_android(app_links)
    app_links.map do |app|
      {
        relation: ['delegate_permission/common.handle_all_urls'],
        target: {
          namespace: 'android_app',
          package_name: app.package_name,
          sha256_cert_fingerprints: app.fingerprints
        }
      }
    end
  end
end
