class Api::V1::StatsPresenter
  def initialize(stat_name, scope)
    @stat_name = stat_name
    @scope = scope
  end

  def to_json(_options = {})
    return { data: @scope.all } if @scope.grouped_by_type?(@stat_name)
    return (@scope.first.try(:value) || 0) if @scope.vertical?
    @scope.first[@scope.base_stats(@stat_name).name]
  end
end
