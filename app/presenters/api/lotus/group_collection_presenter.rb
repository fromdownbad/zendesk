require 'set'

class Api::Lotus::GroupCollectionPresenter < Api::V2::CollectionPresenter
  # ### JSON Format
  # Groups are represented as JSON objects which have the following keys:
  #
  # | Name            | Type    | Read-only | Mandatory | Comment
  # | --------------- | ------- | --------- | --------- | -------
  # | id              | integer | yes       | no        | Automatically assigned when creating groups
  # | name            | string  | no        | yes       | The name of the group
  # | agent_ids       | Array   | yes       | no        | IDs of the assignable members of the group
  #
  # Agents are __always__ sideloaded and are ordered by name.
  #
  # #### Example
  # ```js
  # {
  #   "groups": [
  #     {
  #       "id": 11,
  #       "name": "Support",
  #       "agent_ids": [21, 11]
  #     },
  #     {
  #       "id": 12,
  #       "name": "Sales",
  #       "agent_ids": [13]
  #     }
  #   ],
  #   "agents": [
  #     {
  #       "id":         21,
  #       "name":       "Agent Extraordinaire",
  #       "avatar_url": "https://dev.zd-dev.com/system/photos/0001/0021/cat.jpeg"
  #     },
  #     {
  #       "id":         13,
  #       "name":       "Mick Staugaard",
  #       "avatar_url": null
  #     },
  #     {
  #       "id":         11,
  #       "name":       "Sally Agent 1",
  #       "avatar_url": null
  #     }
  #   ]
  # }
  # ```
  def as_json(groups)
    Rails.cache.fetch(cache_key(groups), compress: true) do
      as_json_uncached(groups)
    end
  end

  def as_json_uncached(groups)
    memberships = Membership.where(group_id: groups.map(&:id)).to_a

    agents = User.active.agents.where(id: memberships.map(&:user_id).uniq).select('id, account_id, name, roles, permission_set_id')
    agents = user.non_light_agent_users(agents.includes(permission_set: :permissions, account: {}))

    agents_ids = agents.map(&:id)
    agent_id_set = Set.new(agents_ids)
    # rejecting the memberships of the agents that were rejected by non_light_agent_users
    memberships.select! { |membership| agent_id_set.include?(membership.user_id) }

    group_membership_map = memberships.group_by(&:group_id)

    groups_json_array = groups.map do |group|
      group_memberships = group_membership_map[group.id] || []

      {
        id: group.id,
        name: group.name,
        # we do the `agents_ids &` in order to maintain the ordering of agents_ids
        agent_ids: agents_ids & group_memberships.map(&:user_id)
      }
    end

    agents_json_array = agents.map do |user|
      {
        id: user.id,
        name: user.name,
        avatar_url: avatar_url(user)
      }
    end

    {
      groups: groups_json_array,
      agents: agents_json_array,
      next_page: nil,
      previous_page: nil,
      count: groups.count
    }
  end

  def cache_key(groups)
    ActiveSupport::Cache.expand_cache_key(
      [
        'lotus_groups_presenter',
        '2',
        user.account.scoped_cache_key(:groups_agents),
        groups.map(&:cache_key)
      ]
    )
  end

  private

  def avatar_url(user)
    return nil unless user.photo.present?

    user.large_photo_url
  end
end
