class Api::Lotus::TimeZonePresenter < Api::V2::TimeZonePresenter
  self.model_key = :time_zone

  # This presenter is identical to the API v2 presenter, except that the
  # url property is omitted.
  #
  # ### JSON Format
  # Time Zones have the following attributes
  #
  # | Name                | Type      | Comment
  # | ------------------- | --------- | -------
  # | name                | string    | Timezone name in ActiveSupport
  # | iana_name           | string    | Timezone name in IANA database
  # | url                 | string    |
  # | offset              | integer   | current timezone offset in minutes
  # | formatted_offset    | string    | current timezone abbreviation including offset e.g. "GMT+2:00"
  # | moment_packed       | string    | Timezone data for the moment.js library
  # #### Example
  # ```js
  # {
  #   "name":              "Copenhagen",
  #   "iana_name"          "Europe/Copenhagen",
  #   "offset":            120,
  #   "formatted_offset":  "GMT+2:00",
  #   "moment_packed":     "Europe/Copenhagen|CET CEST|-10 -20|010...|1dAN0 1qM0 WM0..."
  # }
  # ```

  def model_json(time_zone)
    json = super
    json.delete(:url)
    json
  end
end
