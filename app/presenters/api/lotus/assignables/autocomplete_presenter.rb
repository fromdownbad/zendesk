class Api::Lotus::Assignables::AutocompletePresenter < Api::V2::CollectionPresenter
  def as_json(assignables)
    agents_array = []
    groups_array = []

    assignables.each do |assignable|
      if assignable.is_a? User
        model_agent(assignable).each { |agent| agents_array << agent }
      else
        groups_array << model_group(assignable)
      end
    end

    {
      agents: agents_array,
      groups: groups_array,
      count: agents_array.count + groups_array.count
    }
  end

  private

  def model_agent(agent)
    agent.groups.map do |group|
      {
        id: agent.id,
        name: agent.name,
        group_id: group.id,
        group: group.name,
        photo_url: agent_photo_url(agent)
      }
    end
  end

  def agent_photo_url(agent)
    return nil unless agent.photo
    ContentUrlBuilder.new.url_for(agent.photo)
  end

  def model_group(group)
    {
      id: group.id,
      name: group.name
    }
  end
end
