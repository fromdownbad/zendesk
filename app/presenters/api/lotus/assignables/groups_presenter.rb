class Api::Lotus::Assignables::GroupsPresenter < Api::V2::CollectionPresenter
  # ### JSON Format
  # Groups are represented as JSON objects which have the following keys:
  #
  # | Name            | Type    | Read-only | Mandatory | Comment
  # | --------------- | ------- | --------- | --------- | -------
  # | id              | integer | yes       | no        | Automatically assigned when creating groups
  # | name            | string  | no        | yes       | The name of the group
  # | description     | string  | no        | no        | The description of the group
  #
  # #### Example
  # ```js
  # {
  #   "groups": [
  #     {
  #       "id": 11,
  #       "name": "Support",
  #       "description": "Support description"
  #     },
  #     {
  #       "id": 12,
  #       "name": "Sales",
  #       "description": "Sales description"
  #     }
  #   ],
  #   "next_page": null,
  #   "previous_page": null,
  #   "count": 2
  # }
  # ```
  def as_json(groups)
    Rails.cache.fetch(cache_key(groups), compress: true) do
      ActiveRecord::Base.on_slave do
        as_json_uncached(groups)
      end
    end
  end

  def as_json_uncached(groups)
    groups_json = groups.map do |group|
      {
        id: group.id,
        name: group.name,
        description: group.description
      }
    end

    {
      groups: groups_json,
      next_page: nil,
      previous_page: nil,
      count: groups.count
    }
  end

  def cache_key(groups)
    ActiveSupport::Cache.expand_cache_key(
      [
        'lotus_assignable_groups_presenter',
        '1',
        user.account.scoped_cache_key(:groups_agents),
        groups.map(&:cache_key)
      ]
    )
  end
end
