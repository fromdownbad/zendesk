module Api
  module Lotus
    class ConversationCursorCollectionPresenter < Api::Presentation::CursorCollectionPresenter
      # Conversations have special handling for metadata so we need to let that handle the
      # `model_json` call, but we need to inherit from the CursorCollectionPresenter to get all of
      # the cursor parsing and building. We accomplish this "multi-inheritance" by inheriting from
      # the Cursor presenter while also creating our conversation specific presenter and delegating
      # the special methods to it.
      delegate :model_json, :account, :user, :side_loads, :side_load?, :user_presenter,
        to: :conversation_collection_presenter

      def initialize(*args)
        @conversation_collection_presenter = ConversationCollectionPresenter.new(*args)

        super
      end

      private

      attr_reader :conversation_collection_presenter
    end
  end
end
