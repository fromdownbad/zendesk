class Api::Lotus::ChatSettingsPresenter < Api::V2::Presenter
  self.model_key = :account
  ## ### JSON Format
  ## Chat Settings are represented as simple flat JSON objects which have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | phase           | integer | yes       | no        | The chat phase
  ## | suite           | boolean | yes       | no        |

  ##
  ## #### Example
  ## ```js
  ## {
  ##   "phase":      4,
  ##   "suite":      false
  ## }
  ## ```
  def model_json(chat_settings)
    return nil unless chat_settings

    json = {
      phase:      chat_settings["phase"],
      suite:      chat_settings["suite"]
    }
    super.merge(json)
  end
end
