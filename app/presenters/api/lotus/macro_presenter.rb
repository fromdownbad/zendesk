class Api::Lotus::MacroPresenter < Api::V2::Presenter
  self.model_key = :macro

  # ### JSON Format
  # Macros are represented as simple flat JSON objects which have the following keys.
  #
  # | Name              | Type    | Read-only | Mandatory | Comment
  # | ----------------- | ------- | --------- | --------- | -------
  # | id                | integer | yes       | no        | Automatically assigned when creating macros
  # | title             | string  | yes       | yes       | The title of the macro
  # | availability_type | string  | yes       | yes       | The scope of availability of this macro
  #
  # #### Example
  # ```js
  # {
  #   "macro": {
  #     "id": 25,
  #     "title": "Tickets updated <12 Hours",
  #     "availability_type": "everyone"
  #   }
  # }
  # ```
  def model_json(macro)
    {
      id:                macro.id,
      title:             macro.title,
      description:       macro.description,
      availability_type: macro.availability_type
    }.tap do |json|
      if account.has_dc_in_macro_titles?
        json.merge!(
          title:     render_dynamic_content(macro.title),
          raw_title: macro.title
        )
      end
    end
  end

  def collection_presenter
    @collection_presenter ||= begin
      Api::Lotus::MacroCollectionPresenter.new(user,
        options.merge(item_presenter: self, includes: includes))
    end
  end
end
