class Api::Lotus::CcsAndFollowers::Jobs::UpdateRequesterTargetRulesPresenter < Api::V2::Presenter
  self.model_key = :result

  ## ### JSON Format
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | url             | string  | no        | no        | The download url for the latest generated affected rules list
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "url":               "http://company.zendesk.com/expirable_attachments/token/hqPNBrDFhLlcwQPbSAKj504Mc/?name=affected_rules_list.zip"
  ## }
  ## ```
  def model_json(expirable_attachment)
    {
      url: expirable_attachment.try(:url) || ""
    }
  end
end
