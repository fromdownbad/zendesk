class Api::Lotus::DeletedTicketPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings
  self.model_key = :deleted_tickets

  # ### JSON Format
  # Deleted tickets are represented as JSON objects with the following keys:
  # | Name           | Type     | Read-only | Mandatory | Comment                                         |
  # | id             | integer  | yes       | yes       | The nice_id of the deleted ticket               |
  # | subject        | string   | yes       | yes       | The subject of the deleted ticket               |
  # | description    | string   | yes       | yes       | The description of the deleted ticket           |
  # | actor          | string   | yes       | yes       | The agent/user who deleted the ticket           |
  # | deleted_at     | datetime | yes       | yes       | When the ticket was deleted                     |
  # | previous state | string   | yes       | yes       | The status of the ticket beforing being deleted |
  #
  # #### Example
  # ```js
  # {
  #  "deleted_tickets": [
  #    {
  #      "id": 581,
  #      "subject": "That dentist takes a test",
  #      "description": "That dentist!",
  #      "actor": {
  #        "id": 3946,
  #          "name": "Keitha Parekh"
  #        },
  #      "deleted_at": "20140704T15:37:04Z",
  #      "previous_state": "open"
  #    }
  #  ],
  #  "next_page": null,
  #  "previous_page": null,
  #  "count": 1
  # }
  def model_json(ticket)
    {
      id: ticket.nice_id,
      subject: ticket.subject,
      description: render_dynamic_content(ticket.description, ticket: ticket),
      actor: actor_for_ticket_event(ticket.soft_deletion_event),
      deleted_at: ticket.updated_at, # weird but true, updated_at and status_id = 5 == deleted_at
      previous_state: STATUS_MAP[ticket.previous_status.to_s]
    }
  end

  def present(tickets)
    preload_associations(tickets)
    super
  end

  def association_preloads
    { requester: {} }
  end

  def actor_for_ticket_event(event)
    return {} unless event
    {
      id: event.author.id,
      name: event.author.name
    }
  end
end
