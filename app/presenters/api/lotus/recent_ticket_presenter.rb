class Api::Lotus::RecentTicketPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings
  self.model_key = :ticket

  # ### JSON Format
  # Tickets are represented as simple flat JSON objects which have the following keys:
  #
  # | Name            | Type    | Read-only | Mandatory | Comment
  # | --------------- | ------- | --------- | --------- | -------
  # | id              | integer | yes       | no        | Automatically assigned when creating tickets
  # | subject         | string  | no        | no        | The value of the subject field for this ticket
  # | status          | string  | no        | no        | The state of the ticket, "new", "open", "pending", "hold", "solved", "closed"
  # | requester_name  | integer | no        | yes       | The name of the user who requested this ticket
  #
  # #### Example
  # ```js
  # {
  #   "id":               35436,
  #   "subject":          "Help, my printer is on fire!",
  #   "status":           "open",
  #   "requester_name":   "End User"
  # }
  # ```
  def model_json(ticket)
    {
      id: ticket.nice_id,
      title: ticket.title,
      status: STATUS_MAP[ticket.status_id.to_s],
      requester_name: ticket.requester.name
    }
  end

  def present(tickets)
    preload_associations(tickets)
    super
  end

  def association_preloads
    { requester: {} }
  end
end
