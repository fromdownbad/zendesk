class Api::Lotus::CollaborationEventPresenter < Api::V2::Tickets::AuditEventPresenter
  def model_json(event)
    attributes = {
      type: event.type,
      author_id: event.author_id,
      subject: event.subject,
      thread_id: event.thread_id,
      public: event.is_public?,
      recipient_count: event.recipient_count
    }

    super.merge!(attributes)
  end
end
