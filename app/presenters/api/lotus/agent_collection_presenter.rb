class Api::Lotus::AgentCollectionPresenter < Api::V2::CollectionPresenter
  # self.model_key = :agent

  # ### JSON Format
  # Agents are represented as JSON objects which have the following keys:
  #
  # | Name            | Type    | Read-only | Mandatory | Comment
  # | --------------- | ------- | --------- | --------- | -------
  # | id              | integer | yes       | no        | Automatically assigned when creating users
  # | name            | string  | no        | yes       | The name of the assignable agent
  # | avatar_url      | string  | yes       | no        | The url of the assignable agent's avatar
  #
  # #### Example
  # ```js
  # {
  #   "agents": [
  #     {
  #       "id":         11,
  #       "name":       "Agent Extraordinaire",
  #       "avatar_url": "https://dev.zd-dev.com/system/photos/0001/0021/cat.jpeg"
  #     },
  #     {
  #       "id":         21,
  #       "name":       "Sally Agent 1",
  #       "avatar_url": null
  #     }
  #   ],
  #   "next_page": null,
  #   "previous_page": null,
  #   "count": 2
  # }
  # ```

  def model_json(agents)
    agents.map do |user|
      {
        id: user.id,
        name: user.name,
        avatar_url: avatar_url(user)
      }
    end
  end

  def as_json(agents)
    {
      agents: model_json(agents),
      next_page: nil,
      previous_page: nil,
      count: (agents.is_a?(Array) ? agents.count : agents.count(:all))
    }
  end

  def cache_key(group)
    ActiveSupport::Cache.expand_cache_key(
      [
        'lotus_agents_presenter',
        '2',
        user.account.scoped_cache_key(:agents),
        group.cache_key
      ]
    )
  end

  private

  def avatar_url(user)
    return nil unless user.photo.present?

    user.large_photo_url
  end
end
