class Api::Lotus::GroupPresenter < Api::V2::Presenter
  self.model_key = :group

  ## ### JSON Format
  ## Groups are represented as simple flat JSON objects which have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned when creating groups
  ## | url             | string  | yes       | no        | The API url of this group
  ## | name            | string  | no        | yes       | The name of the group
  ## | description     | string  | no        | no        | The description of the group
  ## | deleted         | boolean | yes       | no        | Deleted groups get marked as such
  ## | created_at      | date    | yes       | no        | The time the group was created
  ## | updated_at      | date    | yes       | no        | The time of the last update of the group
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         3432,
  ##   "url":        "https://company.zendesk.com/api/Lotus/groups/3432.json",
  ##   "deleted",    false,
  ##   "name":       "First Level Support",
  ##   "created_at": "2009-07-20T22:55:29Z",
  ##   "updated_at": "2011-05-05T10:38:52Z"
  ## }
  ## ```
  def model_json(group)
    json = {
      id: group.id,
      name: group.name,
      description: group.description,
      deleted: group.deleted?,
      created_at: group.created_at,
      updated_at: group.updated_at
    }

    super.merge!(json)
  end

  def side_loads(group)
    return {} unless side_load?(:agents)

    { agents: agents_as_json(group) }
  end

  def agents_as_json(group)
    assignable_agents(group).map do |agent|
      {
        id: agent.id,
        name: agent.name
      }
    end
  end

  def assignable_agents(group)
    agents = User.active.agents.
      select('users.id, users.account_id, users.name, users.roles, users.permission_set_id').
      joins(:memberships).
      where(memberships: { group_id: group.id }).
      includes(permission_set: :permissions, account: {})

    user.non_light_agent_users(agents)
  end
end
