class Api::Lotus::ConversationCollectionPresenter < Api::V2::Tickets::AuditEventCollectionPresenter
  include Api::V2::Tickets::EventViaPresenter

  def model_json(collection)
    Array(collection).map do |item|
      json = presenter_for(item.class).model_json(item).merge(
        via: serialized_via_object(item.audit),
        created_at: item.created_at
      )

      # When we export comments from Api::V2::Exports::TicketEventsPresenter
      # we don't want metadata in each comment because they are part of the Audit

      unless options[:without_metadata]
        # This guard is to prevent an exception when bad child and/or parent event data
        # is present (eg. events with all fields set to nil or 0)
        json[:metadata] = item.audit.try(:metadata) || {}
        if item.event_decoration.present?
          json[:metadata] = json[:metadata].merge(decoration: item.event_decoration.data)
        end
      end

      json
    end
  end

  def side_loads(conversation_items)
    json = {}
    conversation_items = Array(conversation_items)

    if side_load?(:users)
      author_and_recipient_ids = conversation_items.flat_map do |conversation|
        ids = [conversation.author_id, conversation.audit.try(:author_id)]

        # We need to include the agent info (e.g. agent's avatar) into the user-sideload
        # for the Omnilog in lotus to render the chat events correctly
        has_chat_history = conversation['type'] == 'ChatStartedEvent' &&
                          conversation['value'] &&
                          conversation['value']['history'].is_a?(Array)

        if has_chat_history
          agent_ids = conversation['value']['history'].map { |hist| hist['actor_id'] if hist['actor_type'] == 'agent' }
          ids += agent_ids
        end

        if account.has_comment_email_ccs_allowed_enabled?
          ids += conversation.audit.try(:active_recipients_ids).to_a
        end
        ids
      end

      author_and_recipient_ids.compact!
      author_and_recipient_ids.uniq!

      users = account.all_users.where(id: author_and_recipient_ids).to_a
      user_presenter.preload_associations(users)
      json[:users] = user_presenter.collection_presenter.model_json(users)
    end

    json
  end

  def user_presenter
    @user_presenter ||= options[:user_presenter] ||= Api::V2::Users::Presenter.new(user, options)
  end
end
