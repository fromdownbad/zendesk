class Api::Lotus::ConversationItemPresenter < Api::V2::Tickets::AuditEventPresenter
  self.model_key = :conversation_item

  ## ### Ticket Conversation
  ## Ticket conversation represent the audit events like ticket comments and
  ## other system generated events( like auto answer events ), which can appear under the lotus conversation
  ## tab.
  ##
  ## ### JSON Format
  ##
  ## Ticket conversation is represented as JSON objects with the following properties:
  ##
  ## | Name                  | Type    | Read-only | Comment
  ## | --------------------- | ------- | --------- | -------
  ## | id                    | integer | yes       | Automatically assigned when the item is created
  ## | type                  | string  | yes       | `Comment` or `VoiceComment` or 'AutomaticAnswerSend' or 'AutomaticAnswerSolve' or 'AutomaticAnswerViewed'
  ## | body                  | string  | yes       | The conversation item content to be displayed as string
  ## | html_body             | string  | yes       | The conversation item content formatted as HTML, fallbacks to body if not applicable
  ## | plain_body            | string  | yes       | The conversation item content formatted as plain text, fallbacks to body if not applicable
  ## | public                | boolean | no        | true if the conversation item should be be public; false incase of an internal note and other private events
  ## | author_id             | integer | yes       | The id of the author
  ## | attachments           | array   | yes       | Attachments, if any. See [Attachment](./attachments)
  ## | via                   | object  | yes       | How the item was created. See [Via Object](./ticket_audits#the-via-object)
  ## | metadata              | object  | yes       | System information (web client, IP address, etc.)
  ## | created_at            | date    | yes       | The time the item was created
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":        1274,
  ##   "type":      "Comment"
  ##   "body":      "Thanks for your help!",
  ##   "public":    true,
  ##   "created_at": "2009-07-20T22:55:29Z",
  ##   "author_id": 123123,
  ##   "attachments": [
  ##     {
  ##       "id":           498483,
  ##       "file_name":    "crash.log",
  ##       "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##       "content_type": "text/plain",
  ##       "size":         2532,
  ##       "thumbnails":   []
  ##     }
  ##   ],
  ##   "metadata": {
  ##     ...
  ##   },
  ##   "via": {
  ##     ...
  ##   },
  ## }
  ## ```

  def collection_key
    :conversations
  end

  def model_json(conversation_item)
    attributes = {
      type:        conversation_item.type,
      author_id:   conversation_item.author_id,
      body:        conversation_item.body,
      public:      conversation_item.is_public?,
      audit_id:    conversation_item.audit.try(:id)
    }

    if conversation_item.is_a?(Comment)
      attributes.merge!(
        attachments: attachment_presenter.collection_presenter.model_json(conversation_item.attachments),
        html_body:   conversation_item.html_body,
        plain_body:  conversation_item.plain_body
      )
    end

    if conversation_item.is_a?(AnswerBotNotification)
      attributes[:recipients] = conversation_item.recipients
    end

    super.merge!(attributes)
  end

  def collection_presenter
    @collection_presenter ||= begin
      klass = if options[:with_cursor_pagination]
        Api::Lotus::ConversationCursorCollectionPresenter
      else
        Api::Lotus::ConversationCollectionPresenter
      end

      klass.new(
        user,
        options.merge(item_presenter: self, includes: includes)
      )
    end
  end

  def association_preloads
    audit_preload = {}
    audit_preload[:notifications_with_ccs] = {} if account.has_email_ccs_preload_notifications_with_ccs?

    {
      audit: audit_preload
    }
  end
end
