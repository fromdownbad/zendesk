class Api::Lotus::KnowledgeEventPresenter < Api::V2::Tickets::AuditEventPresenter
  self.model_key = :knowledge_event

  ## ### KnowledgeEventPresenter
  ## KnowledgeEventPresenter represents events like KnowledgeLinked, KnowledgeCaptured which appear only under audits.
  ##
  ## ### JSON Format
  ##
  ## KnowledgeEventPresenter is represented as JSON objects with the following properties:
  ##
  ## | Name                  | Type    | Read-only | Comment
  ## | --------------------- | ------- | --------- | -------
  ## | id                    | integer | yes       | Automatically assigned when the item is created
  ## | type                  | string  | yes       | `KnowledgeLinked', 'KnowledgeCaptured', 'KnowledgeFlagged', 'KnowledgeLinkAccepted', or 'KnowledgeLinkRejected'
  ## | body                  | string  | yes       | The knowledge event's content to be displayed as string
  ## | html_body             | string  | yes       | The knowledge event's content formatted as HTML, fallbacks to body if not applicable
  ## | plain_body            | string  | yes       | The knowledge event's content  formatted as plain text, fallbacks to body if not applicable
  ## | public                | boolean | no        | true if the knowledge event should be be public; false incase of an internal note and other private events
  ## | author_id             | integer | yes       | The id of the author
  ## | via                   | object  | yes       | How the item was created. See [Via Object](./ticket_audits#the-via-object)
  ## | metadata              | object  | yes       | System information (web client, IP address, etc.)
  ## | created_at            | date    | yes       | The time the item was created
  ##
  ## #### Example
  ## ```js
  ## {
  ##  "id": 10936,
  ##  "type": "KnowledgeLinked",
  ##  "author_id": 10001,
  ##  "body": {
  ##    "id": 1,
  ##    "title": "title 1",
  ##    "html_url": "http://sample.article.link/1",
  ##    "url": "http://sample.article.link/1.json",
  ##    "locale": "en-US"
  ##  },
  ##  "public": false,
  ##  "audit_id": 10931
  ## }
  ## ```

  def model_json(knowledge_item)
    attributes = {
      type:        knowledge_item.type,
      author_id:   knowledge_item.author_id,
      body:        knowledge_item.body,
      public:      knowledge_item.is_public?,
      audit_id:    knowledge_item.audit.try(:id)
    }

    super.merge!(attributes)
  end

  def association_preloads
    {
      audit: {}
    }
  end
end
