class Api::Lotus::SimplifiedEmailThreading::FeedbackPresenter < Api::V2::Presenter
  self.model_key = :result

  def model_json(ticket)
    {
      z1_request_url: "https://#{domain}/hc/requests/#{ticket["id"]}"
    }
  end

  private

  def domain
    @domain ||= "support.#{ENV.fetch('ZENDESK_HOST')}"
  end
end
