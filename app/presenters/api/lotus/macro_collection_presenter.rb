class Api::Lotus::MacroCollectionPresenter < Api::V2::CollectionPresenter
  def as_json(macros)
    super.tap do |json|
      json[:most_used] = model_json(
        scope.with_ordered_ids(macro_usage.most_used(10))
      )
    end
  end

  private

  def macro_usage
    @macro_usage ||= Zendesk::RuleSelection::MacroUsage.new(user)
  end

  def scope
    @scope ||= begin
      Zendesk::RuleSelection::Scope.viewable(
        Zendesk::RuleSelection::Context::Macro.new(user)
      )
    end
  end
end
