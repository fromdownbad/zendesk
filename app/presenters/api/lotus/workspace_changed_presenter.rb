class Api::Lotus::WorkspaceChangedPresenter < Api::V2::Tickets::AuditEventPresenter
  # TODO: remove this when the `updated_workspace_changed_presenter` arturo is removed
  self.model_key = :workspace_changed

  def model_json(event)
    attributes = if event.account.has_updated_workspace_changed_presenter?
      updated_model_json(event)
    else
      legacy_model_json(event)
    end

    super.merge!(attributes)
  end

  private

  # Present this event like many other change events, where value is the current ID and
  # previous_value is the last ID. In the case of adding a workspace, previous_value may be null,
  # and in the case of deleting a workspace, value may be null.
  def updated_model_json(event)
    attributes = if event.action == :DELETE
      # Delete operations should only have previous attributes, but that isn't how it's stored
      {
        value: nil,
        previous_value: event.workspace_id,
        title: nil,
        previous_title: event.workspace_title
      }
    else
      {
        value: event.workspace_id,
        previous_value: event.previous_workspace_attributes[:id],
        # Add some extra data
        title: event.workspace_title,
        previous_title: event.previous_workspace_attributes[:title]
      }
    end

    attributes.merge!(type: 'WorkspaceChanged', action: event.action)
  end

  def legacy_model_json(event)
    {
      url: url(event),
      type: 'WorkspaceChanged',
      id: event.workspace_id,
      value: event.workspace_title,
      value_previous: event&.previous_workspace_attributes,
      action: event.action
    }
  end

  # TODO: remove this when the `updated_workspace_changed_presenter` arturo is removed
  def url(event)
    url_builder.send(:api_v2_workspaces_url, event.workspace_id, format: :json)
  end
end
