class Api::Lotus::MacroApplicationPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings

  self.model_key = :result

  # ### JSON Format
  #
  # Macros are read-only and represented as JSON objects which have the following keys:
  #
  # | Name            | Type    | Comment
  # | --------------- | ------- | -------
  # | ticket          | Object  | Contains entire ticket with fields that have changed if presenting for a specific ticket, otherwise, just the changed fields.
  # | comment         | Object  | Changed values for ticket comment.
  #
  # #### Example
  #
  # When applying macro to specific ticket:
  #
  # ```js
  # {
  #   "result": {
  #     "tags":    ['foo', 'bar'],
  #     "comment": {
  #       "body":      "Assigned to **Agent Uno** for review.",
  #       "html_body": "<p>Assigned to <strong>Agent Uno</strong> for review.</p>",
  #       "public":    false
  #     }
  #   }
  # }
  # ```
  #
  # For bulk application:
  #
  # ```js
  # {
  #   "result": {
  #     "assignee_id": 235323,
  #     "group_id":    98738
  #   }
  # }
  # ```
  def model_json(macro_application)
    result = macro_application.result.with_indifferent_access

    fields = fields_json(result)
    result[:custom_fields] = fields if fields.any?

    if result[:current_tags]
      result[:tags] = TagManagement.normalize_tags(result.delete(:current_tags), account)
    end

    result[:comment] = comment_json(result[:comment]) if result.key?(:comment)

    if macro_application.upload_token.present?
      result.merge!(upload_presenter.present(macro_application.upload_token))
    end

    map_ticket_attributes!(result)
    result
  end

  private

  def comment_json(comment)
    applied_comment(comment).tap do |json|
      json[:public] = comment[:is_public] if comment.key?(:is_public)
    end
  end

  def applied_comment(comment)
    if comment.key?(:html_value) && comment.key?(:value)
      hybrid_comment(comment)
    elsif comment.key?(:html_value)
      html_comment(comment)
    elsif comment.key?(:value)
      basic_comment(comment)
    else
      {}
    end
  end

  def hybrid_comment(comment)
    {body: comment[:value], html_body: comment[:html_value]}
  end

  def html_comment(comment)
    {body: comment[:html_value], html_body: comment[:html_value]}
  end

  def basic_comment(comment)
    {
      body:        comment[:value],
      scoped_body: comment[:scoped_value],
      html_body:   ZendeskText::Markdown.html(comment[:value])
    }
  end

  def fields_json(result)
    result.
      select { |key, _| key.start_with?('ticket_fields_') }.
      map do |name, _|
        {
          id:    name.sub('ticket_fields_', '').to_i,
          value: result.delete(name)
        }
      end
  end

  def upload_presenter
    @upload_presenter ||= Api::V2::UploadPresenter.new(user, options)
  end
end
