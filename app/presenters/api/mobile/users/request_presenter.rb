class Api::Mobile::Users::RequestPresenter < Api::V2::Users::RequestPresenter
  self.model_key = :user

  ## #### Request Users
  ## Request users have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating users
  ## | name            | string  | yes       | The name of the user
  ## | photo           | object  | yes       | The user's profile picture represented as an [Attachment](./attachments) object
  ## | agent           | boolean | yes       | Whether this user is an agent
  ## | organization_id | integer | yes       | The organization this user belongs to
  ## | tags            |  array  | yes       | The tags assigned to this user
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":     1274,
  ##   "name":   "Johnny Agent",
  ##   "agent":  true,
  ##   "organization_id": 1,
  ##   "photo": {
  ##     "id":           928374,
  ##     "name":         "my_funny_profile_pic.png",
  ##     "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic.png",
  ##     "content_type": "image/png",
  ##     "size":         166144,
  ##     "thumbnails": [
  ##       {
  ##         "id":           928375,
  ##         "name":         "my_funny_profile_pic_thumb.png",
  ##         "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic_thumb.png",
  ##         "content_type": "image/png",
  ##         "size":         58298,
  ##       }
  ##     ]
  ##  },
  ##  "tags": ["foo", "bar"]
  ## }
  ## ```
  def model_json(user)
    super.merge!(
      tags: user.tags
    )
  end
end
