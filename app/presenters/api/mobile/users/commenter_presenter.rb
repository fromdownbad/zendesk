class Api::Mobile::Users::CommenterPresenter < Api::V2::Users::EndUserPresenter
  self.model_key = :user

  def initialize(actor, options)
    options[:user_url_builder] ||= options[:url_builder]
    super(actor, options)
  end

  ## ### JSON Format for End-user Requests
  ## When an end-user makes the request, the user is returned with the following attributes:
  ##
  ## | Name                  | Type                           | Read-only | Mandatory | Comment
  ## | --------------------- | ------------------------------ | --------- | --------- | -------
  ## | tags                  | array                          | no        | no        | The user's tags.
  ## | user_fields           | hash                           | no        | no        | The user's custom fields hash.
  ##
  ## #### Example
  ## ```js
  ## {
  ##  "id": 123,
  ##  "name": "Jose Narvaez",
  ##  "photo": {
  ##      "url": "api_v2_attachment_url",
  ##      "id": 100025,
  ##      "file_name": "large_2.png",
  ##      "content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2.png",
  ##      "mapped_content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2.png",
  ##      "content_type": "image/png",
  ##      "size": 165982,
  ##      "width": 80,
  ##      "height": 2180,
  ##      "inline": false,
  ##      "thumbnails": [
  ##        {
  ##          "url": "api_v2_attachment_url",
  ##          "id": 100026,
  ##          "file_name": "large_2_thumb.png",
  ##          "content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2_thumb.png",
  ##          "mapped_content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2_thumb.png",
  ##          "content_type": "image/png",
  ##          "size":130,
  ##          "width":1,
  ##          "height":32,
  ##          "inline":false
  ##        }
  ##      ]
  ##    }
  ## }
  ## ```
  def model_json(user, _options = {})
    {
      id: user.id,
      # NB self.user = current_user
      name: user.safe_name(self.user.is_agent?), # using safe_name to replicate the behaviour of Users::RequestPresenter
      photo: photo(user)
    }
  end

  def photo_url(user)
    user.try(:photo).try(:content_url)
  end

  def photo(user)
    return nil unless user.photo.present?

    attachment_presenter.model_json(user.photo)
  end
end
