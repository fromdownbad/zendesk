class Api::Mobile::Users::EndUserPresenter < Api::V2::Users::EndUserPresenter
  self.model_key = :user

  def initialize(actor, options)
    options[:user_url_builder] ||= options[:url_builder]
    super(actor, options)
    @custom_fields = actor.account.custom_fields_for_owner("User")
  end

  ## ### JSON Format for End-user Requests
  ## When an end-user makes the request, the user is returned with the following attributes:
  ##
  ## | Name                  | Type                           | Read-only | Mandatory | Comment
  ## | --------------------- | ------------------------------ | --------- | --------- | -------
  ## | tags                  | array                          | no        | no        | The user's tags.
  ## | user_fields           | hash                           | no        | no        | The user's custom fields hash.
  ##
  ## #### Example
  ## ```js
  ## {
  ##  "tags": ["foo", "bar"],
  ##  "user_fields": {
  ##    "user_integer_field": 1,
  ##    "user_decimal_field": 1.0,
  ##    "user_checkbox_field": false,
  ##    "user_text_field": "bar"
  ##   }
  ## }
  ## ```

  def model_json(user, _options = {})
    {
      tags: user.tags,
      user_fields: custom_fields_hash(user)
    }
  end

  def custom_fields_hash(user)
    user.custom_field_values.as_json(
      custom_fields: @custom_fields,
      account: user.account
    )
  end
end
