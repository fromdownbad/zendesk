# Used by Mobile Support SDK 1.X boot process
# Mobile Core SDK uses `Api::Private::MobileSdk::SettingsPresenter`
class Api::Mobile::Sdk::SettingsPresenter < Api::V2::Presenter
  self.model_key = :sdk

  # ### Format
  # Presents the settings for a Mobile SDK app
  #
  # ### Features
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | identifier              | string  | The app's unique identifier
  # | rma                     | array   | App Settings for the Rate My App feature
  # | conversations           | array   | App Settings for the Conversations feature
  # | contact_us              | array   | App Settings for the Contact Us feature
  # | brand_id                | string  | The brand assigned to the app
  # | updated_at              | string  | The updated_at stamp for the app and all it's settings
  #
  # ### Rate My App
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | enabled                 | boolean | If Rate My App is enabled
  # | visits                  | integer | Number of visits before showing the RMA dialog
  # | duration                | integer | The time (in days) it will take before showing the RMA dialog
  # | delay                   | integer | The delay (in seconds) after we will show the RMA dialog when the conditions to show were met
  # | tags                    | array   | Tags to be attached to the ticket
  # | ios_store_url           | string  | iOS App Store URL of the app
  # | android_store_url       | string  | Android Store URL of the app
  #
  # ### Conversations
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | enabled                 | boolean | If Conversations is enabled
  #
  # ### Contact Us
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | tags                    | array   | Tags to be attached to the ticket created
  #
  # #### Example
  #
  # ```js
  # {
  #   "sdk": {
  #     "identifier": "c06c0b5a10621911b4ce7930510cb3b84e165ccb8e99c65f",
  #     "rma": {
  #       "enabled": true,
  #       "visits": 14,
  #       "duration": 50,
  #       "delay": 3,
  #       "tags": [],
  #       "ios_store_url": "https://itunes.apple.com/us/app/zendesk/id368796007?mt=8",
  #       "android_store_url": null
  #     },
  #     "conversations": {
  #       "enabled": false
  #     },
  #     "contact_us": {
  #       "tags": []
  #     },
  #     "help_center": {
  #       "enabled": false
  #     },
  #     "blips": {
  #       "permissions": {
  #         "required": true,
  #         "behavioural": true,
  #         "pathfinder": true
  #       },
  #       "collect_only_required_data": false
  #     },
  #     "brand_id": 2,
  #     "updated_at": "2014-09-11T14:49:49Z"
  #   }
  # }
  # ```
  def model_json(mobile_sdk_app)
    app_help_center_settings = {
      enabled: mobile_sdk_app.settings.helpcenter_enabled,
      help_center_article_voting_enabled: mobile_sdk_app.settings.help_center_article_voting_enabled
    }.merge(mobile_sdk_app.help_center_settings)

    {
      identifier: mobile_sdk_app.identifier,
      authentication: mobile_sdk_app.settings.authentication,
      rma: {
        enabled: mobile_sdk_app.settings.rma_enabled,
        visits: mobile_sdk_app.settings.rma_visits,
        duration: mobile_sdk_app.settings.rma_duration,
        delay: mobile_sdk_app.settings.rma_delay,
        tags: mobile_sdk_app.settings.rma_tags,
        ios_store_url: mobile_sdk_app.settings.rma_ios_store_url,
        android_store_url: mobile_sdk_app.settings.rma_android_store_url
      },
      conversations: {
        enabled: mobile_sdk_app.conversations_enabled?
      },
      contact_us: {
        tags: mobile_sdk_app.settings.contact_us_tags
      },
      help_center: app_help_center_settings,
      ticket_forms: {
        available: mobile_sdk_app.account.has_ticket_forms?
      },
      blips: blips_settings(mobile_sdk_app),
      brand_id: mobile_sdk_app.brand_id,
      updated_at: last_updated_at(mobile_sdk_app)
    }
  end

  def side_loads(mobile_sdk_app)
    { account: account_settings(mobile_sdk_app.account) }
  end

  protected

  def account_settings(account)
    {
      attachments: {
        enabled: account.is_attaching_enabled,
        max_attachment_size: account.max_attachment_size
      }
    }
  end

  def blips_settings(mobile_sdk_app)
    json = {
      permissions: {
         required: mobile_sdk_app.required_blips_enabled?,
         behavioural: mobile_sdk_app.behavioural_blips_enabled?,
         pathfinder: mobile_sdk_app.pathfinder_blips_enabled?
      }
    }

    if mobile_sdk_app.account.has_mobile_sdk_blip_collection_control?
      json[:collect_only_required_data] = mobile_sdk_app.settings.collect_only_required_data
    end

    json
  end

  def last_updated_at(mobile_sdk_app)
    mobile_sdk_app.settings.map(&:updated_at).append(mobile_sdk_app.updated_at).max
  end
end
