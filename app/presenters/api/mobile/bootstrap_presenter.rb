class Api::Mobile::BootstrapPresenter < Api::V2::Presenter
  self.model_key = :current_user

  # ### Format
  # Boostraps a user login for a mobile app
  #
  #
  # #### Example
  #
  # ```js
  # {
  # }
  # ```
  def model_json(_user)
    {}
  end

  def side_loads(user)
    json = {}

    account_data = account_presenter.present(user.account)
    json[:account_configuration] = account_data[:account]
    json[:account_settings] = account_data[:settings]
    json[:account_subscription] = account_data[:subscription]

    user_data = user_presenter.present(user)
    json[:current_user] = user_data[:user]
    json[:current_user_organizations] = user_data[:organizations]
    json[:current_user_identities] = user_data[:identities]
    json[:current_user_roles] = user_data[:roles]
    json[:current_user_abilities] = user_data[:abilities]

    json[:brands] = if user.account.has_multibrand?
      brand_presenter.present(user.account.brands)[:brands]
    end

    json[:ticket_forms] = if user.account.has_ticket_forms?
      ticket_form_presenter.present(user.account.ticket_forms)[:ticket_forms]
    end

    json[:ticket_fields] = ticket_field_presenter.present(ticket_fields(user))[:ticket_fields]
    json[:macros] = macro_presenter.present(user.shared_and_personal_macros.active)[:macros]
    json[:organizations] = organization_presenter.present(user.account.organizations)[:organizations]
    json[:assignable_groups] = assignable_group_presenter.present(user.preloaded_assignable_groups)[:groups]
    json
  end

  protected

  def user_presenter
    sideload_options = {includes: [:organizations, :identities, :roles, :abilities]}
    @user_presenter ||= Api::V2::Users::Presenter.new(user, options.merge(sideload_options))
  end

  def account_presenter
    sideload_options = {includes: [:settings, :subscription]}
    @account_presenter ||= Api::V2::AccountPresenter.new(user, options.merge(sideload_options))
  end

  def ticket_fields(user)
    @ticket_fields ||= user.account.ticket_fields
  end
end
