class Api::Mobile::Account::GroupPresenter < Api::V2::Presenter
  self.model_key = :group

  # ### JSON Format
  # Groups are represented as simple flat JSON objects which have the following keys:
  #
  # | Name            | Type    | Read-only | Mandatory | Comment
  # | --------------- | ------- | --------- | --------- | -------
  # | id              | integer | yes       | no        | Automatically assigned when creating groups
  # | name            | string  | no        | yes       | The name of the group
  # | description     | string  | no        | no        | The description of the group
  # | deleted         | boolean | yes       | no        | Deleted groups get marked as such
  # | agents          | Array   | yes       | no        | Assignable members of the group
  #
  # #### Example
  # ```js
  # [{
  #   "id": 11,
  #   "name": "Support",
  #   "description": "Support description",
  #   "deleted": false,
  #   "agents": [
  #     {
  #       "id": 11,
  #       "name": "Agent #1",
  #       "email": "agent1@company.com",
  #       "default_membership": true,
  #       "photo": null,
  #     },
  #     {
  #       "id": 21,
  #       "name": "Agent #2",
  #       "email": "agent2@company.com",
  #       "default_membership": false,
  #       "photo": "https://subdomain.zendesk.com/system/photos/0000/0026/headshot.jpg"
  #     }
  #   ]
  # }]
  # ```
  def model_json(group)
    {
      id: group.id,
      name: group.name,
      description: group.description,
      deleted: group.deleted?,
      agents: agents(group)
    }
  end

  private

  def photo(user)
    return nil unless user.photo.present? && user.photo.is_a?(Photo)
    account.url(mapped: false) + user.photo.public_filename
  end

  def agents(group)
    return [] unless group.is_active?
    group.memberships.
      to_a.
      lazy.
      select { |m| m.user.is_active? && (m.user.is_admin? || !cache_light_agents_ids.include?(m.user.permission_set_id)) }.
      map do |membership|
      {
        id: membership.user_id,
        name: membership.user.name,
        role: membership.user.role.downcase,
        email: membership.user.email,
        photo: photo(membership.user),
        default_membership: membership.default
      }
    end
  end

  def cache_light_agents_ids
    @light_agents ||= Set.new(
      if account.has_light_agents? && account.has_permission_sets?
        account.permission_sets.where(role_type: PermissionSet::Type::LIGHT_AGENT).pluck(:id)
      else
        []
      end
    )
  end

  def preload_associations(model)
    cache_light_agents_ids
    super(model)
  end
end
