class Api::Mobile::Account::LookupPresenter < Api::V2::Presenter
  self.model_key = :lookup

  # ### Format
  # Presents basic details about the account, including login options for the agents
  #
  # ### Features
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | subdomain               | string  | The current account's subdomain
  # | url                     | string  | The curent account's URL
  # | name                    | string  | The curent account's name
  # | agent_logins            | array   | Array of the possible Login options
  #
  # ### Login
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | service                 | string  | the name of the login service
  # | url                     | string  | URL to the login page
  #
  # #### Example
  #
  # ```js
  # {
  #     "subdomain": "example",
  #     "url": "https://example.zendesk.com",
  #     "name": "Example Ltd.",
  #     "agent_logins": [
  #         {
  #             "service": "zendesk",
  #             "url": "https://example.zendesk.com/access/oauth_mobile"
  #         },
  #         {
  #             "service": "google",
  #             "url": "https://example.zendesk.dev/access/google",
  #             "play_url": "https://example.zendesk.dev/access/google_play"
  #         }
  #     ]
  # }
  # ```
  def model_json(account)
    {
      subdomain: account.subdomain,
      url: account.url(mapped: false, protocol: 'https'),
      name: account.name,
      active_products: @options.fetch(:active_products, []),
      agent_logins: @options.fetch(:agent_logins)
    }
  end
end
