class Api::Mobile::PushNotificationDevicePresenter < Api::V2::Presenter
  self.model_key = :push_notification_device

  # ### Format
  # Presents basic details about a mobile SDK push notification device identifier
  #
  # ### Features
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | id                      | string  | The push notification device identifier
  # | identifier              | string  | The push notification device token
  # | device_type             | string  | The push notification device type
  # | active                  | boolean | The push notification device status
  # | url                     | string  | The push notification device URL
  # | created_at              | string  | The push notification device creation date
  # | updated_at              | string  | The push notification device last update date
  #
  # #### Example
  #
  # ```js
  # {
  #   "push_notification_device": {
  #     "id": 100164,
  #     "identifier": "foo",
  #     "device_type": "iphone",
  #     "token_type": "urban_airship_channel_id",
  #     "created_at": "2015-02-24T13:53:13Z",
  #     "updated_at": "2015-02-24T13:53:13Z",
  #     "active": true,
  #     "url": "https://minimumsdk.zendesk-test.com/api/mobile/push_notification_devices/100164.json"
  #   }
  # }
  # ```
  def model_json(push_notification_device)
    {
      id: push_notification_device.id,
      identifier: push_notification_device.token,
      device_type: push_notification_device.device_type,
      token_type: push_notification_device.token_type,
      created_at: push_notification_device.created_at,
      updated_at: push_notification_device.updated_at,
      active: push_notification_device.active,
      url: url(push_notification_device)
    }
  end

  def url(push_notification_device)
    url_builder.send(:api_mobile_push_notification_device_url, push_notification_device.id, format: :json)
  end
end
