class Api::Mobile::Requests::CommentPresenter < Api::V2::Requests::CommentPresenter
  self.model_key = :comment

  def model_json(comment)
    super.merge!(
      request_id: request_identifier(comment.ticket)
    )
  end

  def url(comment)
    url_builder.send(:api_mobile_request_comment_url, request_identifier(comment.ticket), comment, format: :json)
  end

  def request_identifier(request)
    request.token || request.nice_id.to_s
  end

  def user_presenter
    @user_presenter ||= options[:user_presenter] ||= Api::Mobile::Users::RequestPresenter.new(user, options)
  end
end
