class Api::Mobile::RequestPresenter < Api::V2::RequestPresenter
  COMMENTING_AGENTS_LIMIT = 5

  self.model_key = :request

  ## ### JSON Format
  ## Requests are represented as JSON objects which have the following keys:
  ##
  ## | Name                | Type                                     | Read-only | Mandatory | Comment
  ## | ----------------    | ---------------------------------------- | --------- | --------- | -------
  ## | id                  | integer                                  | yes       | no        | The requests token if available nice_id otherwise
  ## | url                 | string                                   | yes       | no        | The API url of this request
  ## | subject             | string                                   | no        | yes       | The value of the subject field for this request if the subject field is visible to end users; a truncated version of the description otherwise
  ## | description         | string                                   | yes       | yes       | The first comment on the request
  ## | status              | string                                   | no        | no        | The state of the request, "new", "open", "pending", "hold", "solved", "closed"
  ## | priority            | string                                   | no        | no        | The priority of the request, "low", "normal", "high", "urgent"
  ## | type                | string                                   | no        | no        | The type of the request, "question", "incident", "problem", "task"
  ## | custom_fields       | Array                                    | no        | no        | The fields and entries for this request
  ## | organization_id     | integer                                  | yes       | no        | The organization of the requester
  ## | requester_id        | integer                                  | yes       | no        | The id of the requester
  ## | assignee_id         | integer                                  | yes       | no        | The id of the assignee if the field is visible to end users
  ## | group_id            | integer                                  | yes       | no        | The id of the assigned group if the field is visible to end users
  ## | collaborator_ids    | array                                    | no        | no        | Who are currently CC'ed on the ticket
  ## | comment_count       | integer                                  | yes       | no        | The count of comments associated to the request
  ## | via                 | [Via](ticket_audits.html#the-via-object) | yes       | no        | This object explains how the request was created
  ## | due_at              | date                                     | no        | no        | When the task is due (only applies if the request is of type "task")
  ## | can_be_solved_by_me | boolean                                  | yes       | no        | If true, end user can mark request as solved.
  ## | solved              | boolean                                  | no        | no        | Whether or not request is solved (an end user can set this if "can_be_solved_by_me", above, is true for that user)
  ## | ticket_form_id      | integer                                  | no        | no        | The numeric id of the ticket form associated with this request if the form is visible to end users - only applicable for enterprise accounts
  ## | created_at          | date                                     | yes       | no        | When this record was created
  ## | updated_at          | date                                     | yes       | no        | When this record last got updated
  ## | followup_source_id  | integer                                  | yes       | no        | The id of the original ticket if this request is a follow-up ticket
  ##
  ## You can also include a `last_comment` property in the JSON objects returned by PUT and POST requests by sideloading it. Example:
  ##
  ##  `POST /api/mobile/requests.json?include=last_comment`
  ##  `PUT /api/mobile/requests/:id.json?include=last_comment`
  ##
  ## Also, the `last_commenting_agents` sideload is supported. When used, a `last_commenting_agents` key will be added to the JSON payload root
  ## containing an array of agents that have made the last 5 public comments on the involved requests. Example:
  ##
  ##  `GET /api/mobile/requests.json?include=last_commenting_agents`
  ##
  ## #### Example
  ## ```js
  ## "requests": {
  ##   "url": "https://support.zd-dev.com/api/mobile/requests/UHF0CU0VsnnY3bR9jDSC1We1w.json",
  ##   "id": "UHF0CU0VsnnY3bR9jDSC1We1w",
  ##   "status": "open",
  ##   "priority": null,
  ##   "type": null,
  ##   "subject": "App Ticket",
  ##   "description": "Hi hi hi",
  ##   "organization_id": null,
  ##   "via": {
  ##     "channel": "mobile_sdk",
  ##     "source": {
  ##       "from": {},
  ##       "to": {},
  ##       "rel": "mobile_sdk"
  ##     }
  ##   },
  ##   "custom_fields": [],
  ##   "requester_id": 10112,
  ##   "collaborator_ids": [],
  ##   "due_at": null,
  ##   "can_be_solved_by_me": true,
  ##   "created_at": "2015-09-11T13:41:30Z",
  ##   "updated_at": "2015-09-11T13:44:05Z",
  ##   "assignee_id": 10002,
  ##   "ticket_form_id": 10002,
  ##   "fields": [],
  ##   "comment_count": 2
  ##   "last_commenting_agents": [10002, 10006],
  ##   "csat": {
  ##     "id": 38004561,
  ##     "score": "bad",
  ##     "updated_at": "2018-12-11T11:53:07Z",
  ##     "created_at": "2018-12-11T11:53:07Z",
  ##     "comment": "Needed more detail."
  ##   }
  ## }
  ## "last_commenting_agents": [
  ##  {
  ##    "id": 10002,
  ##    "name": "Admin Extraordinaire",
  ##    "photo": {
  ##      "url": "api_v2_attachment_url",
  ##      "id": 100025,
  ##      "file_name": "large_2.png",
  ##      "content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2.png",
  ##      "mapped_content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2.png",
  ##      "content_type": "image/png",
  ##      "size": 165982,
  ##      "width": 80,
  ##      "height": 2180,
  ##      "inline": false,
  ##      "thumbnails": [
  ##        {
  ##          "url": "api_v2_attachment_url",
  ##          "id": 100026,
  ##          "file_name": "large_2_thumb.png",
  ##          "content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2_thumb.png",
  ##          "mapped_content_url": "https://minimumsdk.zendesk-test.com/system/photos/0010/0025/large_2_thumb.png",
  ##          "content_type": "image/png",
  ##          "size":130,
  ##          "width":1,
  ##          "height":32,
  ##          "inline":false
  ##        }
  ##      ]
  ##    }
  ##  }
  ## ]
  ## ```
  #
  def model_json(request)
    raise(ArgumentError, "Passing a list to this presenter may cause an N + 1") if request.is_a?(Array)

    json = super.merge!(
      id: request_identifier(request),
      comment_count: comment_count(request)
    )

    # For both first and last comment sideloads, we're first checking if there are any public comments in case of all comments having been made private
    # Also, for consistency on the client side, we want to always show the first/last comment key if they're requested
    if side_load?(:first_comment)
      first_comment = request.public_comments.first
      json[:first_comment] = first_comment ? comment_presenter.model_json(first_comment) : nil
    end

    if side_load?(:last_comment)
      last_comment = request.public_comments.last
      json[:last_comment] = last_comment ? comment_presenter.model_json(last_comment) : nil
    end

    json[:last_commenting_agents_ids] = if account.has_mobile_sdk_support_last_commenting_agents?
      last_commenting_agents_ids(request)
    else
      []
    end

    json[:csat] = csat_enabled? ? csat_rating(request) : nil

    json
  end

  def public_updated_at(request)
    if account.has_mobile_sdk_support_public_updated_at_using_created_at?
      request.events.where(is_public: true).maximum(:created_at)
    else
      super
    end
  end

  def side_loads(request)
    json = {}
    requests = Array(request)

    json[:last_commenting_agents] = if account.has_mobile_sdk_support_last_commenting_agents? && side_load?(:last_commenting_agents)
      last_commenting_agents = last_commenting_agents(requests)
      commenter_presenter.collection_presenter.model_json(last_commenting_agents)
    else
      []
    end

    json
  end

  def url(request)
    url_builder.send(:api_mobile_request_url, request_identifier(request), format: :json)
  end

  def request_identifier(request)
    RequestToken.where(account_id: account.id, source_id: request.id).limit(1).pluck(:value).first ||
    request.nice_id.to_s
  end

  def comment_count(request)
    request.public_comments.size
  end

  def attachment_presenter
    @attachment_presenter ||= options[:attachment_presenter] ||= Api::V2::Requests::AttachmentPresenter.new(user, options)
  end

  def commenter_presenter
    @commenter_presenter ||= options[:commenter_presenter] ||= Api::Mobile::Users::CommenterPresenter.new(user, options)
  end

  def comment_presenter
    Api::Mobile::Requests::CommentPresenter.new(user, options)
  end

  def last_commenting_agents_ids(request)
    # Ideally we should be using `SQL DISTINCT` on the pluck and avoid the `uniq` call.
    # But once I do that the remotion of the duplicates done by the DB messes
    # up the order while `uniq` does the trick preserving order as it uses a Hash
    # in it's implementation which should guarantee ordering of insertion.
    # See the https://bugs.ruby-lang.org/issues/11323.
    Comment.joins(:author).where(
      account_id: account.id,
      ticket_id: request.id
    ).is_public.
      where(users: Role::AGENT.to_conditions).
      order('events.created_at DESC, events.id DESC').
      pluck(:author_id).uniq
  end

  def last_commenting_agents(requests)
    account.agents.where(id: Comment.where(
      account_id: account.id,
      ticket_id: requests.map(&:id)
    ).is_public.select('DISTINCT events.author_id')).to_a
  end

  def csat_enabled?
    options[:csat_enabled]
  end

  def csat_rating(request)
    rating = request.satisfaction_ratings.last
    return nil unless rating
    {
      id: rating.id,
      score: score_attribute(rating.score),
      updated_at: rating.updated_at,
      created_at: rating.created_at,
      comment: rating.comment ? rating.comment.value : ""
    }
  end

  def score_attribute(satisfaction_score)
    ticket_attribute_value(:satisfaction_score, satisfaction_score.to_s, account)
  end
end
