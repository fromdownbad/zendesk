require 'johnny_five'

class Api::V2::TargetFailurePresenter < Api::V2::Presenter
  BLANK_VALUE = '-'.freeze

  self.model_key = :target_failure

  ## ### JSON Format
  ##
  ## Target failures have the following properties:
  ##
  ## | Name                      |Type     | Comment
  ## | ------------------------- | ------- | -------
  ## | id                        | integer | The ID of the target failure
  ## | url                       | string  | The API url of the failure record
  ## | target_name               | string  | Name of the target failure
  ## | status_code               | string  | HTTP status code of the target failure
  ## | message                   | string  | The message returned with the status code
  ## | created_at                | string  | Time of the failure
  ## | consecutive_failure_count | integer | Number of times the target failed
  ## | raw_request               | string  | The raw message of the target request
  ## | raw_response              | string  | The raw response of the failure
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id": 6001326,
  ##   "url":  "https://example.zendesk.com/api/v2/target_failures/6001326.json",
  ##   "target_name": "My URL Target",
  ##   "status_code": 401,
  ##   "message": "Unauthorized",
  ##   "created_at": "2017-09-05T10:38:52Z",
  ##   "consecutive_failure_count": 1,
  ##   "raw_request": "GET /api/v2/tickets.json HTTP/1.1\r\nUser-Agent: Zendesk Target\r\n ...",
  ##   "raw_response": "HTTP/1.1 401 Unauthorized\r\nServer: nginx\r\n ..."
  ## }
  ## ```
  def model_json(target_failure)
    json = super.merge!(build_failure_json(target_failure))

    if options[:action] == 'show'
      json[:raw_request] = target_failure.raw_request
      json[:raw_response] = target_failure.raw_response

      if circuit_tripped?(target_failure.exception)
        json[:raw_request] = "#{I18n.t('txt.admin.views.targets.edit.failures.circuit_tripped_error')} : #{I18n.t('txt.admin.views.targets.edit.failures.circuit_tripped_error_details')}"
      end
    end

    json
  end

  def url(target_failure)
    url_builder.send(:api_v2_target_failure_url, target_failure, format: :json)
  end

  def association_preloads
    { target: {} }
  end

  private

  def build_failure_json(failure)
    failure_json = {
      id: failure.id,
      target_name: failure.target.title,
      message: I18n.t("txt.admin.views.targets.edit.failures.circuit_tripped_error"),
      created_at: failure.created_at,
      # assign blank value here since zendesk_api_dashboard is broken
      # we should be able to come back remove the consecutive_failure_count
      # once zendesk_api_dashboard is fixed and move blank value logic to dashboard UI.
      # Github reference: https://github.com/zendesk/zendesk_api_dashboard/pull/156
      consecutive_failure_count: BLANK_VALUE
    }

    unless circuit_tripped?(failure.exception)
      failure_json[:consecutive_failure_count] = failure.consecutive_failure_count
      failure_json[:status_code] = failure.status_code
      failure_json[:message] = failure.message
    end

    failure_json
  end

  def circuit_tripped?(exception)
    !exception.nil? && exception == ::JohnnyFive::CircuitTrippedError.name
  end
end
