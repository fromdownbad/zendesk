class Api::V2::CustomFieldOptionPresenter < Api::V2::Presenter
  include Zendesk::CustomField::FieldHelper

  self.model_key = :custom_field_option

  def model_json(field_option)
    json = super.merge!(
      id:       field_option.id,
      name:     rendered_field_option_name(field_option),
      raw_name: field_option.name,
      position: field_option.position,
      value:    field_option.value
    )

    json
  end

  def rendered_field_option_name(option)
    if option.is_a?(::CustomField::Dropdown)
      render_dc_or_i18n(option.send(options[:parent]), option.name)
    else
      render_dynamic_content(option.name)
    end
  end

  def url(custom_field_option)
    url_builder.send(options[:route], custom_field_option.send(options[:parent]), custom_field_option, format: :json)
  end
end
