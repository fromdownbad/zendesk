class Api::V2::IdentityPresenter < Api::V2::Presenter
  self.model_key = :identity

  ## ### JSON Format
  ## User identities are represented as JSON objects with the following properties:
  ##
  ## | Name                | Type    | Read-only | Mandatory | Comment
  ## | ------------------- | ------- | --------- | --------- | -------
  ## | id                  | integer | yes       | no        | Automatically assigned on creation
  ## | url                 | string  | yes       | no        | The API url of this identity
  ## | user_id             | integer | yes       | yes       | The id of the user
  ## | type                | string  | yes       | yes       | One of "email", "twitter", "facebook", "google", "phone_number", "agent_fowarding", "any_channel", "foreign", or "sdk"
  ## | value               | string  | yes       | yes       | The identifier for this identity, such as an email address
  ## | verified            | boolean | no        | no        | If the identity has been verified
  ## | primary             | boolean | no*       | no        | If the identity is the primary identity. *Writable only when creating, not when updating. Use the [Make Identity Primary](#make-identity-primary) endpoint instead
  ## | created_at          | date    | yes       | no        | The time the identity was created
  ## | updated_at          | date    | yes       | no        | The time the identity was updated
  ## | undeliverable_count | integer | yes       | no        | The number of times a soft-bounce response was received at that address
  ## | deliverable_state   | string  | yes       | no        | Email identity type only. Indicates if Zendesk sends notifications to the email address. See [Deliverable state](deliverable-state)
  ##
  ## If the identity is of type "phone_number", the phone number must be a direct line, not a shared phone number. See [Phone Number](./users#phone-number) in the Users API.
  ##
  ## #### Deliverable state
  ##
  ## If the identity is an email address, the "deliverable_state" property indicates whether Zendesk sends email notifications to the address. If the value is "deliverable", Zendesk sends notifications to the address. Zendesk does not send notifications if the value is one of the following:
  ##
  ## | Value                  | Description
  ## | ---------------------- | -----------
  ## | undeliverable          | Email address marked as undeliverable
  ## | mailing_list           | Email address used for mailing lists with multiple individual recipients. Considered undeliverable to prevent email loops
  ## | ticket_sharing_partner | Email address used by a Ticket Sharing Partner integration between two Zendesk instances. Considered undeliverable to prevent email loops
  ## | reserved_example       | Email address used for documentation and testing only. Includes @example.com, @example.net, @example.org, and @example.edu. Considered undeliverable because it's a reserved example domain
  ## | mailer_daemon          | Email address reserved for delivery notifications agents. Includes mailer-daemon@domain.com and @mailer-daemon.domain.com. Considered undeliverable because it's a machine address
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "url":             "https://company.zendesk.com/api/v2/users/135/identities/35436.json",
  ##   "user_id":         135,
  ##   "type":            "email",
  ##   "value":           "someone@example.com",
  ##   "verified":        true,
  ##   "primary":         true,
  ##   "updated_at":      "2011-07-20T22:55:29Z",
  ##   "created_at":      "2011-07-20T22:55:29Z",
  ##   "deliverable_state": "deliverable"
  ## }
  ## ```
  ##
  def model_json(identity)
    data = super.merge!(
      id: identity.id,
      user_id: identity.user_id,
      type: identity.identity_type,
      value: identity.value,
      verified: identity.is_verified,
      primary: identity.primary?,
      created_at: identity.created_at,
      updated_at: identity.updated_at
    )

    if identity.type == 'UserEmailIdentity'
      data[:undeliverable_count] = identity.undeliverable_count
      data[:deliverable_state] = identity.deliverable_state_name
    end

    if identity.type == 'UserPhoneNumberIdentity'
      data[:value] = data[:value].to_s + identity.extension.to_s
      if account.has_voice_formatted_phone_numbers?
        data[:formatted_value] = identity.formatted_phone_number
      end
    end

    data[:value] = identity.screen_name if identity.respond_to?(:screen_name)

    data[:subtype_name] = identity.subtype_name if identity.respond_to?(:subtype_name)
    data
  end

  def url(identity)
    url_builder.send(:api_v2_user_identity_url, identity.user_id, identity, format: :json) if identity.id
  end
end
