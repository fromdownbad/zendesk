class Api::V2::SatisfactionRatingStatisticsPresenter
  def initialize(filters = {})
    @filters = filters
  end

  ## ### JSON Format
  ##
  ## | Name          | Type    | Read-only | Mandatory | Comment
  ## | ------------- | ------- | --------- | --------- | -------
  ## | good_ratings  | integer | yes       | no        | Total count of good satisfaction ratings
  ## | bad_ratings   | integer | yes       | no        | Total count of bad satisfaction ratings
  ## | surveys_sent  | integer | yes       | no        | Total count of satisfaction rating surveys sent
  ## | response_rate | integer | yes       | no        | Percent of surveys sent that received a response
  ## | score         | integer | yes       | no        | Overall satisfaction rating score
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "good_ratings":    6,
  ##   "bad_ratings":     4
  ##   "surveys_sent":    100,
  ##   "response_rate":   10,
  ##   "score":           40,
  ## }
  ## ```
  def model_json(account)
    {
      good_ratings: Satisfaction::Rating.positive_rated_ticket_count(account, @filters),
      bad_ratings: Satisfaction::Rating.negative_rated_ticket_count(account, @filters),
      surveys_sent: Satisfaction::Rating.surveys_sent(account, @filters),
      response_rate: Satisfaction::Rating.response_rate(account, @filters),
      score: Satisfaction::Rating.current_score(account, @filters)
    }
  end
end
