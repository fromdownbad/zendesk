class Api::V2::TagScorePresenter < Api::V2::Presenter
  self.model_key = :tag

  ## ### JSON Format
  ## Tags have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | name            | string  | yes       | yes       | The value of the tag
  ## | count           | integer | yes       | yes       | The number of times this tag has been applied
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "name":    "important",
  ##   "count":   559
  ## }
  ## ```
  def model_json(tag_score)
    {
      name: tag_score.tag_name,
      count: tag_score.score
    }
  end
end
