class Api::V2::WorkspacePresenter < Api::V2::Presenter
  self.model_key = :workspace

  attr_accessor :no_macros

  def model_json(workspace)
    attributes = {
      id: workspace.id,
      title: workspace.title,
      description: workspace.description,
      macro_ids: macro_ids(workspace),
      ticket_form_id: workspace.ticket_form_id,
      apps: apps(workspace),
      position: workspace.position,
      activated: workspace.activated,
      conditions: conditions_presenter.model_json(workspace.definition),
      updated_at: workspace.updated_at,
      created_at: workspace.created_at
    }

    unless no_macros
      attributes[:selected_macros] = selected_macros(workspace)
    end

    super.merge!(attributes)
  end

  private

  def conditions_presenter
    @conditions_presenter ||= begin
      Api::V2::Rules::ConditionsPresenter.new(user, options)
    end
  end

  def url(workspace)
    url_builder.send(:api_v2_workspaces_url, workspace, format: :json)
  end

  def apps(workspace)
    workspace.workspace_elements.select(&:app?).map(&:app_info_for_admin_page)
  end

  def macro_ids(workspace)
    workspace.workspace_elements.select(&:macro?).map(&:element_id)
  end

  def selected_macros(workspace)
    return [] unless workspace

    workspace.selected_macros.map do |macro|
      {
        id: macro.id,
        title: macro.title,
        active: macro.is_active?,
        usage_7d: macro.usage.weekly,
        restriction: restriction_for(macro)
      }
    end
  end

  # This logic is copied from macro_presenter.rb
  def restriction_for(macro)
    case macro.owner_type
    when 'Group'
      {type: 'Group'}.tap do |restriction|
        restriction[:id]  = macro.group_ids.first
        restriction[:ids] = macro.group_ids
      end
    when 'User'
      {type: 'User', id: macro.owner.id}
    end
  end
end
