class Api::V2::BrandPresenter < Api::V2::Presenter
  def self.new(user, options)
    klass = if user.is_admin? || user.is_system_user?
      Api::V2::AdminBrandPresenter
    else
      Api::V2::AgentBrandPresenter
    end
    klass.new(user, options)
  end
end
