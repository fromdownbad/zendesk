class Api::V2::AdminBrandPresenter < Api::V2::AgentBrandPresenter
  self.model_key = :brand

  ## ### JSON Format
  ## Brands are represented as JSON objects which have the following keys:
  ##
  ## | Name             | Type          | Read-only | Mandatory | Comment
  ## | ---------------- | ------------- | --------- | --------- | -------
  ## | url              | string        | yes       | no        | The API url of this brand
  ## | id               | integer       | yes       | no        | Automatically assigned when the brand is created
  ## | name             | string        | no        | yes       | The name of the brand
  ## | brand_url        | string        | no        | no        | The url of the brand
  ## | has_help_center  | boolean       | no        | no        | If the brand has a Help Center
  ## | help_center_state| string        | yes       | no        | The state of the Help Center: enabled, disabled, or restricted
  ## | active           | boolean       | no        | no        | If the brand is set as active
  ## | default          | boolean       | no        | no        | Is the brand the default brand for this account
  ## | logo             | Attachment    | no        | no        | Logo image for this brand
  ## | ticket_form_ids  | array         | yes       | no        | The ids of ticket forms that are available for use by a brand
  ## | created_at       | date          | yes       | no        | The time the brand was created
  ## | updated_at       | date          | yes       | no        | The time of the last update of the brand
  ## | subdomain        | string        | no        | yes       | The subdomain of the brand
  ## | host_mapping     | string        | no        | no        | The hostmapping to this brand, if any (only admins view this key)
  ## | signature_template | string      | no        | no        | The signature template for a brand
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                    47,
  ##   "url":                   "https://company.zendesk.com/api/v2/brands/47.json",
  ##   "name":                  "Brand 1",
  ##   "brand_url":             "https://brand1.com",
  ##   "has_help_center":       true,
  ##   "help_center_state":     "enabled",
  ##   "active":                true,
  ##   "default":               true,
  ##   "logo": {
  ##     "url":                   "https://company.zendesk.com/api/v2/attachments/928374.json",
  ##     "id":                    928374,
  ##     "file_name":             "brand1_logo.png",
  ##     "content_url":           "https://company.zendesk.com/logos/brand1_logo.png",
  ##     "mapped_content_url":    "https://company.com/logos/brand1_logo.png",
  ##     "content_type":          "image/png",
  ##     "size":                  166144,
  ##     "thumbnails": [
  ##       {
  ##         "url":                 "https://company.zendesk.com/api/v2/attachments/928375.json",
  ##         "id":                  928375,
  ##         "file_name":           "brand1_logo_thumb.png",
  ##         "content_url":         "https://company.zendesk.com/photos/brand1_logo_thumb.png",
  ##         "mapped_content_url":  "https://company.com/photos/brand1_logo_thumb.png",
  ##         "content_type":        "image/png",
  ##         "size":                58298,
  ##       },
  ##       {
  ##         "url":                 "https://company.zendesk.com/api/v2/attachments/928376.json",
  ##         "id":                  928376,
  ##         "file_name":           "brand1_logo_small.png",
  ##         "content_url":         "https://company.zendesk.com/photos/brand1_logo_small.png",
  ##         "mapped_content_url":  "https://company.com/photos/brand1_logo_small.png",
  ##         "content_type":        "image/png",
  ##         "size":                58298,
  ##       }
  ##     ]
  ##   },
  ##   "ticket_form_ids":       [ 47, 33 22 ]
  ##   "created_at":            "2012-04-02t22:55:29z",
  ##   "updated_at":            "2012-04-02t22:55:29z",
  ##   "subdomain":             "brand1",
  ##   "host_mapping":          "brand1.com",
  ##   "signature_template":    "{{agent.signature}}"
  ## }
  ## ```

  def model_json(brand)
    super.merge!(
      host_mapping: brand.host_mapping
    )
  end
end
