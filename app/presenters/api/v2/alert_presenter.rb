class Api::V2::AlertPresenter < Api::V2::Presenter
  self.model_key = :alert

  ## ### JSON Format
  ## Alerts have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | value           | string  | yes       | no        | The value/message of this alert
  ## | link_url        | string  | yes       | no        | The url for this alert
  ## | created_at      | date    | yes       | no        | The time the identity got created
  ## | updated_at      | date    | yes       | no        | The time the identity got updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "link_url":        "http://foo.bar",
  ##   "value":           "Please ignore this test alert",
  ##   "interfaces":      "all",
  ##   "updated_at":      "2011-07-20T22:55:29Z",
  ##   "created_at":      "2011-07-20T22:55:29Z"
  ## }
  ## ```
  def model_json(alert)
    alert.attributes.slice("id", "link_url", "value", "interfaces", "created_at", "updated_at")
  end
end
