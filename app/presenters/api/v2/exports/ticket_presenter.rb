class Api::V2::Exports::TicketPresenter < Api::V2::Presenter
  ## ### JSON Format
  ## The tickets updated since a given point in time are represented as simple flat JSON objects with these attributes:
  ##
  ## | Name                  | Type    | Read-only | Mandatory | Comment
  ## | ---------------       | ------- | --------- | --------- | -------
  ## | end_time              | date    | yes       | no        | The most recent time present in this result set in Unix epoch time; this should be used as the next start_time
  ## | next_page             | string  | yes       | no        | The URL that should be called to get the next set of results
  ## | results               | array   | yes       | no        | An array of hashes, one per ticket. Each hash contains key/value pairs corresponding to ticket attributes
  ## | field_headers         | array   | yes       | no        | A hash of field keys and their human-readable names
  ## | options               | hash    | yes       | no        | Contains the timezone of the account and the time offset in hours after midnight for the next sync
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "end_time": 1332034771,
  ##   "next_page": "https://domain.zendesk.com/api/v2/exports/tickets.json?start_time=1332034771",
  ##   "field_headers": {
  ##     "group_name": "Group",
  ##     "id": "Id",
  ##     "created_at": "Created at",
  ##     ...
  ##   },
  ##   "results": [
  ##     {
  ##       "group_name": "Support",
  ##       "id": 2,
  ##       "created_at": "2012-02-02T04:31:29Z",
  ##       ...
  ##      },
  ##      ...
  ##   ],
  #    "options" : {
  #       "timezone": "America/Los_Angeles",
  #       "hour_offset": "08"
  #    }
  ## }
  ## ```
  def model_json(exporter)
    start_time = exporter.min_ts.to_i
    limit      = options[:limit]
    tickets    = exporter.tickets
    opts       = exporter.options

    hash = { results: tickets, field_headers: exporter.field_map(has_multiple_ticket_forms: account.ticket_forms_is_active?), options: opts }
    unless tickets.empty?
      end_time = tickets.last['generated_timestamp']
      log_exported_ticket_info("model_json", exporter.account.id, tickets.length, start_time, end_time, opts)
      # for groupon, who has 2.3k tickets updated in the exact same second
      if end_time == start_time && tickets.size == limit
        Rails.logger.info("number of tickets for single second >= limit, need to bypass limit")
        exporter.exact_ts = true
        exporter.limit = nil
        hash[:results] = exporter.tickets
        end_time += 1
      end

      hash[:end_time] = end_time

      add_next_page(hash)
    end

    hash
  end

  # We don't want the top level key in the result
  def as_json(model)
    model_json(model)
  end

  def add_next_page(hash)
    hash[:next_page] = next_page(hash[:end_time])
  end

  def next_page(start_time)
    url_builder.send(:api_v2_exports_tickets_url, start_time: start_time, format: :json)
  end

  private

  def log_exported_ticket_info(_scope, account_id, count, start_time, end_time, others)
    Rails.logger.info do
      [
        "Api::V2::Exports::GooddataTicketPresenter",
        Time.now.utc.strftime("%Y-%m-%dT%H:%M:%S"),
        account_id,
        count,
        start_time,
        end_time,
        *others
      ].join(",")
    end
  end
end
