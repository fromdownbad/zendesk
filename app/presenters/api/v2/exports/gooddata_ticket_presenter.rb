class Api::V2::Exports::GooddataTicketPresenter < Api::V2::Exports::TicketPresenter
  # We don't need next page attribute for GoodData
  def add_next_page(hash)
  end
end
