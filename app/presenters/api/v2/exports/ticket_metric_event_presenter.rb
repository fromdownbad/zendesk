class Api::V2::Exports::TicketMetricEventPresenter < Api::V2::Presenter
  ## ### JSON Format
  ## Ticket metric events are represented as a list of events with the following properties:
  ##
  ## | Name            | Type         | Read-only | Mandatory | Comment
  ## | --------------- | ------------ | --------- | --------- | -------
  ## | id              | integer      | yes       | no        | Automatically assigned when the record is created
  ## | ticket_id       | integer      | yes       | no        | Id of the associated ticket
  ## | metric          | string       | yes       | no        | One of the following: `agent_work_time`, `pausable_update_time`, `periodic_update_time`, `reply_time`, `requester_wait_time`, or `resolution_time`
  ## | instance_id     | integer      | yes       | no        | The instance of the metric associated with the event. See [instance_id](#instance_id)
  ## | type            | string       | yes       | no        | One of the following: `activate`, `pause`, `fulfill`, `apply_sla`, `breach`, or `update_status`. See [Metric event types](#metric-event-types)
  ## | time            | date         | yes       | no        | The time the event occurred
  ##
  ## In addition to the general properties above, additional properties may be available depending on the event `type`:
  ##
  ## | Name            | Type         | Read-only | Mandatory | Comment
  ## | --------------- | ------------ | --------- | --------- | -------
  ## | sla             | object       | yes       | no        | Available if `type` is `apply_sla`. The SLA policy and target being enforced on the ticket and metric in question, if any. See [sla](#sla)
  ## | status          | object       | yes       | no        | Available if `type` is `update_status`. Minutes since the metric has been open. See [status](#status)
  ## | deleted         | boolean      | yes       | no        | Available if `type` is `breach`. In general, you can ignore any breach event when `deleted` is true. See [deleted](#deleted)
  ##
  ## #### Example
  ##
  ## ```js
  ## {
  ##   "ticket_metric_events": [
  ##     {
  ##       "id": 123457,
  ##       "ticket_id": 987,
  ##       "metric": "requester_wait_time",
  ##       "instance_id": 1,
  ##       "type": "apply_sla",
  ##       "time": "2015-08-27T08:00:00Z",
  ##       "sla": {
  ##         "target": 60,
  ##         "business_hours": true,
  ##         "policy": {
  ##           "id": 531,
  ##           "title": "Main Policy",
  ##           "description": "Measure all tickets"
  ##         }
  ##       }
  ##     },
  ##     {
  ##       ...
  ##     }
  ##   ]
  ## }
  ## ```
  ##
  ## #### Metric event types
  ##
  ## ##### activate
  ## Each metric has an activate event that represents when the clock starts
  ## running for that particular metric. In the case of the `reply_time`
  ## metric when `instance_id` is 1, as well as the `agent_work_time` and
  ## `requester_wait_time` metrics, the time usually corresponds to the time
  ## the ticket was created.
  ##
  ## Activate events are also used to indicate that a ticket metric is no
  ## longer paused or, in some cases, fulfilled.
  ##
  ## You'll see an activate event for all metrics, regardless of whether the
  ## ticket is being measured by an SLA policy or whether the metric has a set
  ## target.
  ##
  ## Example:
  ##
  ## ```js
  ## {
  ##   "id": 123456,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "activate",
  ##   "time": "2015-08-27T08:00:00Z"
  ## }
  ## ```
  ##
  ## ##### pause
  ## A pause event represents a ticket metric that hasn't been fulfilled yet
  ## but was paused according to the metric's conditions. Examples:
  ##
  ## * the `requester_wait_time` metric is paused when the ticket status is pending
  ## * the `agent_work_time` metric is paused when the ticket status is pending or on-hold
  ##
  ## When a ticket metric is un-paused (for example, when the ticket goes back
  ## into the open status), another activate event is created.
  ##
  ## Pause events are recorded for all metrics, regardless of whether the
  ## ticket is being measured by an SLA policy or whether the metric has a set
  ## target.
  ##
  ## The following example shows a pause event followed by an activate event:
  ##
  ## ```js
  ## {
  ##   "id": 123466,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "pause",
  ##   "time": "2015-08-27T08:30:00Z"
  ## },
  ## {
  ##   "id": 123476,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "activate",
  ##   "time": "2015-08-27T09:00:00Z"
  ## }
  ## ```
  ##
  ## ##### fulfill
  ## A fulfill event represents a ticket that was updated in such a way that
  ## the end state of that metric has been fulfilled. For example, for the
  ## `reply_time` metric, a fulfill event occurs when a public agent replies
  ## for the first time by adding a comment to a ticket. For the
  ## `requester_wait_time` metric, a fulfill event occurs when the ticket's
  ## status is changed to solved. A fulfill event doesn't mean that the ticket
  ## achieved any SLAs. It simply means that the metric's end conditions have
  ## been satisfied.
  ##
  ## It's possible for a ticket metric to be fulfilled and then un-fulfilled.
  ## For example, the `requester_wait_time` metric is fulfilled when the ticket
  ## is solved. If the ticket is reopened, the `requester_wait_time` metric
  ## starts where it left off.
  ##
  ## Fulfill events are recorded for all metrics, regardless of whether the
  ## ticket is being measured by an SLA policy or whether the metric has a set
  ## target.
  ##
  ## The following example shows a sequence of fulfill and activate events for
  ## a ticket when its status was set to solved, to open, and then back to
  ## solved:
  ##
  ## ```js
  ## {
  ##   "id": 123486,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "fulfill",
  ##   "time": "2015-08-27T10:30:00Z"
  ## },
  ## {
  ##   "id": 123496,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "activate",
  ##   "time": "2015-08-27T11:00:00Z"
  ## },
  ## {
  ##   "id": 123506,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "fulfill",
  ##   "time": "2015-08-27T12:00:00Z"
  ## }
  ## ```
  ##
  ## ##### apply_sla
  ## The `apply_sla` event type designates that a particular metric on a
  ## particular ticket is now measured by a target on an SLA policy. If the
  ## `apply_sla` event is not present for a ticket and metric, the metric isn't
  ## currently measured by an SLA policy. An `apply_sla` event occurs when an
  ## SLA policy is applied to a ticket or when a SLA policy or target changes
  ## on a ticket.
  ##
  ## Each `apply_sla` event includes a `sla` property describing the applied
  ## policy. See [sla](#sla) below.
  ##
  ## The following example shows a typical `apply_sla` event:
  ##
  ## ```js
  ## {
  ##   "id": 123457,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "apply_sla",
  ##   "time": "2015-08-27T08:00:00Z",
  ##   "sla": {
  ##     "target": 60,
  ##     "business_hours": true,
  ##     "policy": {
  ##       "id": 531,
  ##       "title": "Main Policy",
  ##       "description": "Measure all tickets"
  ##     }
  ##   }
  ## }
  ## ```
  ##
  ## ##### breach
  ## You'll never receive a breach event if you didn't first receive an
  ## `apply_sla` event. Metrics not measured by an SLA policy will never have
  ## a breach event.
  ##
  ## Breach events are recorded before a breach, so all breach events include
  ## a `deleted` property (see [deleted](#deleted) below):
  ##
  ## * When `deleted` is false, the breach event indicates that the ticket
  ## metric breached its SLA target at the time of the event or, if the time
  ## of the event is in the future, that the SLA breach will occur at that time
  ## if nothing else about the ticket changes.
  ## * When `deleted` is true, the breach event indicates that the ticket would
  ## have breached its SLA target at that time, but did not. In general, you
  ## should simply ignore breach events whose `deleted` property is true.
  ##
  ## The following example shows a typical breach event:
  ##
  ## ```js
  ## {
  ##   "id":  123479,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "breach",
  ##   "time": "2015-08-27T09:30:00Z",
  ##   "deleted": false
  ## }
  ## ```
  ##
  ## ##### update_status
  ## The `update_status` event typically occurs alongside fulfill events. These
  ## events are used to provide information on the current value of the metric
  ## at the time of the fulfill event. To do this, each `update_status` event
  ## includes a `status` property. See [status](#status) below.
  ##
  ## The following example shows a typical `update_status` event:
  ##
  ## ```js
  ## {
  ##   "id": 123480,
  ##   "ticket_id": 987,
  ##   "metric": "requester_wait_time",
  ##   "instance_id": 1,
  ##   "type": "update_status",
  ##   "time": "2015-08-27T09:30:00Z",
  ##   "status": {
  ##     "calendar": 60,
  ##     "business": 60
  ##   }
  ## }
  ## ```
  ##
  ## #### Other event properties
  ##
  ## ##### instance_id
  ## Use the `instance_id` property to track each instance of a metric event
  ## that can occur more than once per ticket, such as the `reply_time` event.
  ## The value increments over the lifetime of the ticket.
  ##
  ## ##### sla
  ## Optional. The `sla` property provides key information about the SLA policy
  ## and target being enforced on the ticket and metric in question. The target
  ## time is provided in minutes, along with whether the target is being
  ## measured in business or calendar hours. Policy information is also provided,
  ## including the ID, title, and description of the policy currently applied to
  ## the ticket.
  ##
  ## ##### status
  ## Optional. The `status` property provides the number of minutes in both
  ## business and calendar hours for which the metric has been open. The `status`
  ## property is only updated for a `fulfill` event. Any ticket metric that
  ## hasn't breached yet or fulfilled at least once won't have a calculated status.
  ##
  ## ##### deleted
  ## Optional. The `deleted` property is only used to indicate whether or not
  ## a breach event should be ignored. In general, you can ignore any breach
  ## event where `deleted` is true.
  self.model_key = :ticket_metric_event

  def model_json(event)
    {
      id:          event.id,
      ticket_id:   event.ticket.try(:nice_id),
      metric:      metric_name(event),
      instance_id: event.instance_id,
      type:        event.type,
      time:        event.time
    }.tap { |result| result.merge!(event.metadata) }
  end

  private

  def metric_name(event)
    if event.metric?(:reply_time) && options[:gooddata_request]
      event.instance_id == 1 ? 'first_reply_time' : 'next_reply_time'
    else
      event.metric
    end
  end
end
