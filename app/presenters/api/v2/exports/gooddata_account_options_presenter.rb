# Present options gooddata needs to know about an account, such as
# ticket load frequency, timezone
#
class Api::V2::Exports::GooddataAccountOptionsPresenter < Api::V2::Presenter
  self.model_key = 'options'

  include Zendesk::Export::TicketExportOptions
  ## ### JSON Format
  ## Account options for Gooddata
  ##
  ## | Name                  | Type    | Read-only | Mandatory | Comment
  ## | ---------------       | ------- | --------- | --------- | -------
  ## | options               | hash    | yes       | no        | Contains the timezone of the account and the hour offset in hours after midnight for the next sync
  ##
  ## #### Example
  ## ```js
  ## {
  #    "options" : {
  #       "timezone": "America/Los_Angeles",
  #       "hour_offset": "08"
  #    }
  ## }
  ## ```
  def model_json(account)
    self.class.build_options_hash(account).merge(
      plan_name: plan_name(account),
      deployments: deployments(account),
      settings: settings(account)
    )
  end

  def plan_name(account)
    if has_feature_boost?(account)
      account.feature_boost.boost_plan_score
    elsif account.subscription.plan_type == SubscriptionPlanType.Large
      # ensure we send GD the right string until they integrate 'Professional'
      'Plus'
    else
      account.subscription.plan_name
    end
  end

  def deployments(account)
    Api::V2::Account::DeploymentsPresenter.new(user, options).model_json(account)
  end

  def settings(account)
    Api::V2::Account::SettingsPresenter.new(user, options).model_json(account)
  end

  def has_feature_boost?(account) # rubocop:disable Naming/PredicateName
    account.feature_boost.present? && account.feature_boost.is_active
  end
end
