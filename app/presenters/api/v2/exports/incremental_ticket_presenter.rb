class Api::V2::Exports::IncrementalTicketPresenter < Api::V2::CollectionPresenter
  self.model_key = :ticket

  MAX_PROCESSING_TIME = Integer(ENV['INC_API_MAX_PROCESSING_SECONDS'] || 30).seconds

  def initialize(user, options = {})
    item_presenter = options[:item_presenter] || default_item_presenter(user, options)
    options[:item_presenter] = item_presenter

    @navigation = options[:navigation]
    @redacted_fields = options[:redacted_fields] || []
    super
  end

  def default_item_presenter(user, options)
    Api::V2::Tickets::TicketPresenter.new(user, options)
  end

  def model_json(tickets)
    map_with_time_limit(tickets) do |ticket|
      # has_prevent_deletion_if_churned? == account_is_under_government_subpoena?
      # Tickets not under government subpoena are already fully scrubbed,
      #   hence no need to change to FakeScrubbedTicket
      if ticket.scrubbed? && account.has_prevent_deletion_if_churned?
        ticket = ticket.becomes(FakeScrubbedTicket)
      end

      item_presenter.model_json(ticket).tap do |json|
        json[:generated_timestamp] = ticket.generated_timestamp.to_i
        @redacted_fields.each { |f| json.delete(f) }
      end
    end
  end

  def as_json(tickets)
    json = { model_key => model_json(tickets) }

    if @navigation == :cursor
      json.merge!(cursor_attributes(json[model_key]))
    else
      json[:count] = total(tickets)
    end

    json.merge(item_presenter.side_loads(tickets))
  end

  def association_preloads
    item_presenter.association_preloads
  end

  def present(tickets)
    item_presenter.preload_associations(tickets)
    super
  end

  private

  def map_with_time_limit(collection)
    start = clock_time

    limited_collection = collection.each_with_object([]) do |item, results|
      results << yield(item)

      break results if stop_processing_records?(start)
    end

    time_limit_exceeded = limited_collection.size < collection.size
    metric_tags = ["time_limit_exceeded:#{time_limit_exceeded}"]

    statsd_client.gauge(
      'incremental_ticket_presenter.model_json',
      clock_time - start,
      tags: metric_tags
    )

    statsd_client.gauge(
      'incremental_ticket_presenter.num_records',
      limited_collection.size,
      tags: metric_tags
    )

    limited_collection
  end

  def cursor_attributes(presented_collection)
    # Copied and stylistically modified from zendesk/api_presentation#cursor_collection_presenter.rb
    # TODO: fix the inheritance hierarchy to make sure this logic is not duplicated in instead
    # composable inside a CollectionPresenter
    after_cursor = after_cursor(presented_collection)

    # incremental APIs are based on time and change from request to request, so
    # scrolling backwards doesn't make sense
    {
      after_url: cursor_url(after_cursor),
      before_url: nil,
      after_cursor: after_cursor.try(:to_s),
      before_cursor: nil,
    }
  end

  def after_cursor(collection)
    return nil if collection.empty?

    Zendesk::CursorPagination::Cursor.new(
      min_timestamp: max_timestamp(collection),
      min_id: max_id(collection)
    ).base64
  end

  def cursor_param_name
    Zendesk::CursorPagination::CURSOR_PARAM_NAME
  end

  def cursor_url(cursor_val)
    # the new cursor URLs should not have start time, just the next cursor
    @url_builder.params.delete(:start_time)

    return unless cursor_val

    params = @url_builder.params.merge(cursor_param_name => cursor_val)
    @url_builder.url_for(params)
  end

  def max_timestamp(collection)
    @max_timestamp ||= collection.last[:generated_timestamp].to_f
  end

  def max_id(collection)
    @max_id ||= collection.last[:id]
  end

  def stop_processing_records?(start_time)
    @navigation == :cursor &&
      account.has_time_bound_incremental_api_processing? &&
      clock_time - start_time > MAX_PROCESSING_TIME
  end

  def clock_time
    Process.clock_gettime(Process::CLOCK_MONOTONIC, :second)
  end

  def statsd_client
    Rails.application.config.statsd.client
  end
end
