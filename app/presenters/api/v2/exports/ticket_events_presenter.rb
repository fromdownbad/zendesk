# Presents audits and all of their children events
# rolled up for export. Expects raw data, not AR objects
# Some selection is done regarding which attributes we chose to include,
# which may not be good for all customers - it was originally only built for
# GoodData exports.
# Now if you want to add comments to the exports, you can use the option include_comment,
# which will also include all metadata available.
# In the API v3 this will be the default behavior.
class Api::V2::Exports::TicketEventsPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings
  self.model_key = :ticket_events

  IGNORED_VALUES = %w[satisfaction_comment current_collaborators].to_set.freeze

  def initialize(user, options = {})
    super

    @preloaded_rules          = {}
    @preloaded_rule_revisions = {}
  end

  # Triggered by #present and not #model_json
  def preload_associations(audit_hashes)
    audit_hashes = [audit_hashes] unless audit_hashes.is_a?(Array)
    preload_comments(audit_hashes) if options[:include_comment]
    preload_via_references(audit_hashes)
    nil
  end

  def model_json(audit)
    json = {}

    events = Array(audit[:events]).select { |e| presentable_change?(e) }
    events.map! do |event_hash|
      event_hash_with_comment = @comments_hash.try(:[], event_hash[:id])
      # We have 2 hashes because in #child_information and then #comment_information,
      # we may or may not merge them depending on the account settings.
      # So we need to keep both around.
      child_information(event_hash, event_hash_with_comment)
    end

    events.sort_by! { |e| e[:id] }
    json[:child_events] = events

    json.merge(audit_information(audit))
  end

  protected

  attr_accessor :preloaded_rules, :preloaded_rule_revisions

  def preload_comments(audit_hashes)
    comment_ids = extract_comment_ids(audit_hashes)
    return if comment_ids.empty?

    # Finds comments from the DB - it only works for non archived tickets.
    comments = Comment.where(id: comment_ids).to_a

    # For other tickets we'll use the data we already have in the audits
    # to instantiate new Comment objects and present them nicely.
    comment_ids_from_archive = (comment_ids - comments.map(&:id)).to_set

    audit_hashes.each do |audit_hash|
      ticket_nice_id = audit_hash[:ticket][:nice_id]

      # We'll need the Audit in CommentCollectionPresenter#model_json
      # so we transform the Hash we have into an actual object
      if account.has_ticket_events_presenter_preload_ticket?
        # instantiating audit and ticket object only if required, and delegating it to nested block
        audit_object = nil
        ticket_object = nil
      else
        audit_object = Audit.instantiate_from_archive(audit_hash.dup.stringify_keys)
      end

      audit_hash[:events].each do |event_hash|
        # We filter the events on their :type property as well as the id from the
        # comment_ids_from_archive list above.
        next unless comment_event?(event_hash) && comment_ids_from_archive.include?(event_hash[:id])

        event_hash[:ticket_id] = ticket_nice_id
        comment = Comment.instantiate_from_archive(event_hash.dup.stringify_keys)

        if account.has_ticket_events_presenter_preload_ticket?
          audit_object ||= Audit.instantiate_from_archive(
            audit_hash.dup.stringify_keys
          )
          comment.association(:audit).target = audit_object

          ticket_object ||= Ticket.instantiate_from_archive(
            audit_hash[:ticket].dup.stringify_keys
          ) if audit_hash[:ticket]
          comment.association(:ticket).target = ticket_object if ticket_object
        else
          comment.association(:audit).target = audit_object
        end

        comments << comment
      end
    end

    comments.compact!

    if comments.empty?
      nil
    else
      cache_presented_comments(comments)
    end
  end

  def extract_comment_ids(audit_hashes)
    events = audit_hashes.flat_map { |h| h[:events] }
    events.map { |e| e[:id] if comment_event?(e) }.compact
  end

  def cache_presented_comments(comments)
    presenter_klass = Api::V2::Tickets::CommentCollectionPresenter
    options = {
      url_builder: url_builder,
      without_metadata: true,
      deleted_tickets_lookup: true
    }

    comments.map(&:audit).pre_load(audit_association_preloads) if account.has_email_ccs_preload_notifications_with_ccs?

    decorated_comments = presenter_klass.new(user, options).model_json(comments)

    @comments_hash = Hash[decorated_comments.map do |comment_hash|
      [comment_hash[:id], comment_hash]
    end]
  end

  def audit_information(audit)
    json = {
      id: audit[:id],
      ticket_id: audit[:ticket][:nice_id],
      timestamp: audit[:created_at].to_i,
      created_at: audit[:created_at],
      updater_id: updater_id(audit),
      via: ViaType.to_s(audit[:via_id]),
      system: system_metadata(audit)
    }

    if options[:include_comment]
      json[:metadata] = metadata(audit)
    end

    json.merge!(event_type(audit))
    json.merge!(merged_ticket_ids(audit)) if is_merge_audit?(audit)

    json
  end

  def updater_id(audit)
    if comment = audit[:events].detect { |event| comment_event?(event) }
      comment[:author_id]
    else
      audit[:author_id]
    end
  end

  def comment_event?(event)
    %w[FacebookComment VoiceComment VoiceApiComment Comment].include?(event[:type])
  end

  def child_information(event_hash, event_hash_with_comment = nil)
    json = {
      id: event_hash[:id],
      via: ViaType.to_s(event_hash[:via_id]),
      via_reference_id: event_hash[:via_reference_id]
    }

    value_reference = event_hash[:value_reference]

    if comment_event?(event_hash)
      json.merge!(comment_information(event_hash, event_hash_with_comment))

    elsif ticket_sharing_event?(event_hash)
      json.merge!(ticket_sharing_event_information(event_hash))

    elsif is_number?(value_reference)
      json[:custom_ticket_fields] ||= {}
      json[:custom_ticket_fields][value_reference.to_i] = event_hash[:value]

    elsif !ignored_value?(value_reference)
      json.merge!(value_information(value_reference, event_hash[:value], event_hash[:value_previous]))
    end

    json.merge!(
      relation_json(event_hash[:via_reference_id], event_hash[:via_id])
    )

    json.merge!(event_type(event_hash))

    json
  end

  def merged_ticket_ids(event)
    ticket_ids = if event[:value].nil?
      []
    else
      event[:value].split(',').map(&:to_i)
    end

    {
      merged_ticket_ids: ticket_ids
    }
  end

  def is_merge_audit?(event) # rubocop:disable Naming/PredicateName
    event[:type] == "TicketMergeAudit"
  end

  def comment_information(event_hash, event_hash_with_comment = nil)
    return event_hash_with_comment if event_hash_with_comment

    {
      comment_present: true,
      comment_public: event_hash[:is_public] == 1
    }
  end

  def ticket_sharing_event_information(event_hash)
    # modeled after Api::V2::Tickets::TicketSharingEventPresenter
    {
      id: event_hash[:id],
      type: "TicketSharingEvent",
      agreement_id: event_hash[:value].to_i,
      action: event_hash[:type] == "TicketSharingEvent" ? "shared" : "unshared"
    }
  end

  def value_information(value_reference, value, value_previous)
    case sym_reference = value_reference.to_sym

    when :current_tags
      tag_changes_for(value, value_previous)

    when :ticket_type_id, :priority_id, :status_id, :satisfaction_score, :subject
      { ticket_attribute_name(value_reference) => ticket_attribute_value(value_reference, value, account) }

    when :requester_id, :group_id, :assignee_id
      { sym_reference => value.to_i }

    else
      { sym_reference => value }
    end
  end

  def tag_changes_for(value, value_previous)
    current_tags = TagManagement.normalize_tags(value, account)
    previous_tags = TagManagement.normalize_tags(value_previous, account)

    added_tags = current_tags - previous_tags
    removed_tags = previous_tags - current_tags

    {
      tags: current_tags,
      added_tags: added_tags,
      removed_tags: removed_tags
    }
  end

  def presentable_change?(event)
    comment_event?(event) ||
      is_number?(event[:value_reference]) ||
      !ignored_value?(event[:value_reference])
  end

  def ticket_sharing_event?(event)
    %w[TicketSharingEvent TicketUnshareEvent].include?(event[:type])
  end

  def ignored_value?(value_reference)
    IGNORED_VALUES.include?(value_reference)
  end

  def is_number?(val) # rubocop:disable Naming/PredicateName
    val.to_i.to_s == val
  end

  def event_type(event)
    result = {}

    result[:event_type] = event[:type] if event[:type]

    if event[:type] == "Change" && event[:value_reference] != 'current_tags'
      previous_value = ticket_attribute_value(event[:value_reference], event[:value_previous], account) if event[:value_reference]
      result[:previous_value] = previous_value.present? ? previous_value : event[:value_previous]
    end

    result
  end

  def metadata(audit)
    AuditMetadata.parse_metadata(audit[:value_previous])
  end

  def system_metadata(audit)
    system_metadata = metadata(audit).fetch('system', {})

    {
      client: system_metadata['client'],
      location: system_metadata['location'],
      latitude: system_metadata['latitude'],
      longitude: system_metadata['longitude']
    }
  end

  private

  def preload_via_references(audit_hashes)
    rule_ids          = []
    rule_revision_ids = []

    audit_hashes.flat_map { |audit| audit[:events] }.map do |event|
      case event[:via_id]
      when Zendesk::Types::ViaType.RULE
        rule_ids << event[:via_reference_id]
      when Zendesk::Types::ViaType.RULE_REVISION
        rule_revision_ids << event[:via_reference_id]
      end
    end

    preload_rules(rule_ids)
    preload_rule_revisions(rule_revision_ids)
  end

  def preload_rules(rule_ids)
    return unless rule_ids.any? && preloaded_rules.empty?

    self.preloaded_rules = begin
      account.
        rules.
        where(id: rule_ids).
        each_with_object({}) do |rule, rule_map|
          rule_map[rule.id] = rule
        end
    end
  end

  def preload_rule_revisions(revision_ids)
    return unless revision_ids.any? && preloaded_rule_revisions.empty?

    self.preloaded_rule_revisions = begin
      Trigger.with_deleted do
        TriggerRevision.
          where(account_id: account.id, id: revision_ids).
          includes(:trigger).
          each_with_object({}) do |revision, rule_revisions_map|
            rule_revisions_map[revision.id] = revision
          end
      end
    end
  end

  def relation_json(reference_id, via_id = nil)
    case via_id
    when ViaType.RULE
      rule_relation(reference_id)
    when ViaType.RULE_REVISION
      rule_revision_relation(reference_id)
    else
      {}
    end
  end

  def rule_relation(reference_id)
    return {} unless preloaded_rules.include?(reference_id)

    {rel: preloaded_rules[reference_id].rule_type}
  end

  def rule_revision_relation(reference_id)
    return {} unless preloaded_rule_revisions.include?(reference_id)

    rule_revision = preloaded_rule_revisions[reference_id]

    {
      via:              ViaType.to_s(ViaType.RULE),
      via_reference_id: rule_revision.trigger.id,
      rel:              rule_revision.trigger.rule_type,
      revision_id:      rule_revision.nice_id
    }
  end

  def audit_association_preloads
    {
      ticket: {},
      notifications_with_ccs: {},
      account: {}
    }
  end
end
