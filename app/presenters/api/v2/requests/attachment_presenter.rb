class Api::V2::Requests::AttachmentPresenter < Api::V2::AttachmentPresenter
  self.model_key = :attachment

  ## ### JSON Format
  ## Attachments are represented as JSON objects with the following keys:
  ##
  ## | Name               | Type             | Read-only | Comment
  ## | ------------------ | ---------------- | --------- | -------
  ## | id                 | integer          | yes       | Automatically assigned when created
  ## | file_name          | string           | yes       | The name of the image file
  ## | content_url        | string           | yes       | A full unhostmapped URL where the attachment image file can be downloaded
  ## | mapped_content_url | string           | yes       | A full hostmapped URL where the attachment image file can be downloaded
  ## | content_type       | string           | yes       | The content type of the image. Example value: `image/png`
  ## | size               | integer          | yes       | The size of the image file in bytes
  ## | thumbnails         | array            | yes       | An array of [Photo](#attachments) objects. Note that thumbnails do not have thumbnails.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                 928374,
  ##   "file_name":          "my_funny_profile_pic.png",
  ##   "content_url":        "https://company.zendesk.com/attachments/my_funny_profile_pic.png",
  ##   "mapped_content_url": "https://support.company.com/attachments/my_funny_profile_pic.png",
  ##   "content_type":       "image/png",
  ##   "size":               166144,
  ##   "thumbnails": [
  ##     {
  ##       "id":                 928375,
  ##       "file_name":          "my_funny_profile_pic_thumb.png",
  ##       "content_url":        "https://company.zendesk.com/attachments/my_funny_profile_pic_thumb.png",
  ##       "mapped_content_url": "https://support.company.com/attachments/my_funny_profile_pic_thumb.png",
  ##       "content_type":       "image/png",
  ##       "size":               58298
  ##     }
  ##   ]
  ## }
  ## ```
  def content_url(attachment, options = {})
    url_source = (options[:mapped] == false) ? account : self.options[:brand] || account
    url_source.url(options) + ContentUrlBuilder.new.filename(attachment)
  end
end
