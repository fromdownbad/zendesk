class Api::V2::Requests::OrganizationPresenter < Api::V2::Presenter
  self.model_key = :organization

  ## #### Request Organizations
  ## Request organizations have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating organizations
  ## | name            | string  | yes       | The name of this organization
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":     1274,
  ##   "name":   "My Organization"
  ## }
  def model_json(organization)
    {
      id: organization.id,
      name: organization.name
    }
  end
end
