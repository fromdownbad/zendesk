class Api::V2::Requests::GroupPresenter < Api::V2::Presenter
  self.model_key = :group

  ## #### Request Groups
  ## Request groups have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating groups
  ## | name            | string  | yes       | The name of this group
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":     1290,
  ##   "name":   "My Group"
  ## }
  def model_json(group)
    {
      id: group.id,
      name: group.name
    }
  end
end
