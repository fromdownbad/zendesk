class Api::V2::Requests::ErrorsPresenter < Api::V2::ErrorsPresenter
  protected

  def present_attribute_error(record, attribute, message)
    json = super

    if message.respond_to?(:details)
      json.merge!(message.details)
    end

    json
  end
end
