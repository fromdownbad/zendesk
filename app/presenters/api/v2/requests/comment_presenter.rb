class Api::V2::Requests::CommentPresenter < Api::V2::Presenter
  self.model_key = :comment

  ## #### Request Comments
  ## Comments represent the public conversation between requesters, collaborators and agents on a request.
  ##
  ## Request comments have the following properties:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the comment is created
  ## | type            | string  | yes       | `Comment` or `VoiceComment`
  ## | request_id      | integer | yes       | The id of the request
  ## | body            | string  | no        | The actual comment made by the author
  ## | html_body       | string  | yes       | The actual comment made by the author formatted as HTML
  ## | plain_body      | string  | yes       | The comment formatted as plain text
  ## | public          | boolean | yes       | If true, the comment is public
  ## | author_id       | integer | yes       | The id of the author
  ## | attachments     | array   | yes       | Read-only list of attachments to the comment. See [Attaching files](#https://developer.zendesk.com/rest_api/docs/support/tickets#attaching-files)
  ## | uploads         | array   | no*       | \*On create only. List of tokens received after [uploading files](https://developer.zendesk.com/rest_api/docs/support/attachments#upload-files) to attach
  ## | created_at      | date    | yes       | When this comment was created
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id": 1274,
  ##   "type": "Comment",
  ##   "body": "Thanks for your help!",
  ##   "html_body": "<p>Thanks for your help!</p>",
  ##   "author_id": 1,
  ##   "attachments": [
  ##     {
  ##       "id":           498483,
  ##       "name":         "crash.log",
  ##       "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##       "content_type": "text/plain",
  ##       "size":         2532,
  ##       "thumbnails":   []
  ##     }
  ##   ],
  ##   "created_at": "2009-07-20T22:55:29Z"
  ## }
  ## ```
  def model_json(comment)
    json = super.merge!(
      id: comment.id,
      type: comment.type,
      request_id: comment.ticket.nice_id,
      body: comment.body,
      html_body: comment.html_body(for_agent: user.is_agent?),
      plain_body: comment.plain_body,
      public: comment.is_public?,
      author_id: comment.author_id,
      attachments: attachment_presenter.collection_presenter.model_json(comment.attachments),
      created_at: comment.created_at
    )

    if side_load?(:screencasts)
      json[:screencasts] = screencast_presenter.collection_presenter.model_json(comment.screencasts)
    end

    json
  end

  def url(comment)
    url_builder.send(:api_v2_request_comment_url, comment.ticket, comment, format: :json)
  end

  def side_loads(comments)
    comments = Array(comments)

    side_loaded = {
      users:         side_load_association(comments, :author, user_presenter),
      # author is preloaded above
      organizations: side_load_association(comments.map(&:author).compact.uniq, :organization, organization_presenter)
    }

    if side_load?(:requests)
      side_loaded[:requests] = side_load_association(comments, :ticket, request_presenter)
    end

    side_loaded
  end

  def association_preloads
    {
      attachments: attachment_presenter.association_preloads,
      ticket:      (side_load?(:requests) ? request_presenter.association_preloads : {})
    }
  end

  protected

  def request_presenter
    @request_presenter ||= options[:request_presenter] ||= Api::V2::RequestPresenter.new(user, options)
  end

  def user_presenter
    @user_presenter ||= options[:user_presenter] ||= Api::V2::Users::RequestPresenter.new(user, options)
  end

  def organization_presenter
    @organization_presenter ||= options[:organization_presenter] ||= Api::V2::Requests::OrganizationPresenter.new(user, options)
  end

  def attachment_presenter
    @attachment_presenter ||= options[:attachment_presenter] ||= Api::V2::Requests::AttachmentPresenter.new(user, options)
  end
end
