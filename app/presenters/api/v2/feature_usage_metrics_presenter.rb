class Api::V2::FeatureUsageMetricsPresenter < Api::V2::Presenter
  self.model_key = :feature_usage_metrics

  ## ### JSON Format
  ## Feature usage metrics are read-only.
  ##
  ## ### Macros
  ##
  ## | Name                   | Type   | Comment
  ## | ---------------------- | ------ | -------
  ## | count                  | number | Total count of active shared macros
  ## | used_past_day_count    | number | Total count of active shared macros used within past 24 hours
  ## | updated_past_day_count | number | Total count of active shared macros updated within past 24 hours
  ## | most_used_past_week    | array  | Array of objects representing 5 active shared macros used most in past week
  ## | updated_recently       | array  | Array of objects representing 5 active shared macros updated most recently
  ##
  ## ### Automations
  ##
  ## | Name                   | Type   | Comment
  ## | ---------------------- | ------ | -------
  ## | count                  | number | Total count of active automations
  ## | used_past_day_count    | number | Total count of active automations used within past 24 hours
  ## | updated_past_day_count | number | Total count of active automations updated within past 24 hours
  ## | most_used_past_week    | array  | Array of objects representing 5 active automations used most in past week
  ## | updated_recently       | array  | Array of objects representing 5 active automations updated most recently
  ##
  ## ### Triggers
  ##
  ## | Name                   | Type   | Comment
  ## | ---------------------- | ------ | -------
  ## | count                  | number | Total count of active triggers
  ## | used_past_day_count    | number | Total count of active triggers used within past 24 hours
  ## | updated_past_day_count | number | Total count of active triggers updated within past 24 hours
  ## | most_used_past_week    | array  | Array of objects representing 5 active triggers used most in past week
  ## | updated_recently       | array  | Array of objects representing 5 active triggers updated most recently
  ##
  ## ### Views
  ##
  ## | Name                   | Type   | Comment
  ## | ---------------------- | ------ | -------
  ## | count                  | number | Total count of active views
  ## | updated_past_day_count | number | Total count of active views updated within past 24 hours
  ## | updated_recently       | array  | Array of objects representing 5 active views updated most recently
  ##
  ## #### Example
  ##
  ## ```js
  ## {
  ##   "macros": {
  ##     "count": 33,
  ##     "updated_past_day_count": 2,
  ##     "used_past_day_count": 3,
  ##     "most_used_past_week": [
  ##       {"id": 444, "name": "Blah", ...}
  ##       ...
  ##     ],
  ##     "updated_recently: [
  ##       {"id": 444, "name": "Blah", ...}
  ##       ...
  ##     ],
  ##   },
  ##   "automations": {
  ##     ...
  ##   },
  ##   "triggers": {
  ##     ...
  ##   },
  ##   "views": {
  ##     ...
  ##   }
  ## }
  ## ```
  def model_json(account)
    options = @options.merge(day_ago: 24.hours.ago, week_ago: 7.days.ago)

    {
      automations: present_automations(account, options),
      macros:      present_macros(account, options),
      triggers:    present_triggers(account, options),
      views:       present_views(account, options)
    }
  end

  protected

  def execution_counts(rule_type)
    executions_from_counter(
      Zendesk::RuleSelection::ExecutionCounter.new(
        account: account,
        type:    rule_type
      )
    )
  end

  def executions_from_counter(counter)
    {
      executed_past_day: counter.all_execution_counts(:daily).keys.to_set,
      weekly_usage:      counter.all_execution_counts(:weekly)
    }
  end

  def present_automations(account, options)
    present_with_usages(
      account.automations.active.to_a,
      options.merge(execution_counts(:automation))
    )
  end

  def present_macros(account, options)
    present_with_usages(
      account.shared_macros.active.to_a,
      options.merge(execution_counts(:macro))
    )
  end

  def present_triggers(account, options)
    present_with_usages(
      account.triggers.active.to_a,
      options.merge(execution_counts(:trigger))
    )
  end

  def present_views(account, options)
    present_without_usages(
      account.views.active.to_a,
      options.merge(account: account)
    )
  end

  def present_with(klass, resource, options = {})
    klass.new(user, @options.merge(options)).model_json(resource)
  end

  def present_with_usages(resource, options)
    present_with(RulesMetricsWithUsageDataPresenter, resource, options)
  end

  def present_without_usages(resource, options)
    present_with(RulesMetricsWithoutUsageDataPresenter, resource, options)
  end

  class RulesMetricsWithoutUsageDataPresenter < Api::V2::Presenter
    self.model_key = :rules_metrics

    def model_json(active_rules)
      {
        count:                  active_rules.count,
        updated_past_day_count: updated_past_day_count(active_rules),
        updated_recently:       present(recently_updated(active_rules))
      }
    end

    private

    def recently_updated(rules)
      rules.sort_by(&:updated_at).last(5).reverse
    end

    def present(rules)
      RulePresenter.new(user, @options).collection_presenter.model_json(rules)
    end

    def updated_past_day_count(rules)
      rules.find_all { |rule| rule.updated_at > @options[:day_ago] }.count
    end

    class RulePresenter < Api::V2::Presenter
      self.model_key = :rule

      def model_json(rule)
        {
          id:         rule.id,
          title:      dc_renderer.render(rule.title),
          updated_at: rule.updated_at
        }
      end

      private

      def dc_renderer
        @dc_renderer ||= Zendesk::DynamicContent::Renderer.new(account)
      end
    end
  end

  class RulesMetricsWithUsageDataPresenter < Api::V2::Presenter
    self.model_key = :rules_metrics

    def model_json(active_rules)
      {
        count:                  active_rules.count,
        updated_past_day_count: updated_past_day_count(active_rules),
        used_past_day_count:    used_past_day_count(active_rules),
        most_used_past_week:    present(most_used_past_week(active_rules)),
        updated_recently:       present(recently_updated(active_rules))
      }
    end

    private

    def most_used_past_week(rules)
      weekly_usage = @options[:weekly_usage]

      used_in_past_week = rules.find_all { |rule| weekly_usage.include?(rule.id) }

      used_in_past_week.sort_by! { |rule| weekly_usage[rule.id] }.reverse!.first(5)
    end

    def present(rules)
      RuleWithExecutionCountPresenter.new(user, @options).
        collection_presenter.
        model_json(rules)
    end

    def recently_updated(rules)
      rules.sort_by(&:updated_at).last(5).reverse
    end

    def updated_past_day_count(rules)
      rules.find_all { |rule| rule.updated_at > @options[:day_ago] }.count
    end

    def used_past_day_count(rules)
      rules.count { |rule| @options[:executed_past_day].include?(rule.id) }
    end

    class RuleWithExecutionCountPresenter < Api::V2::Presenter
      self.model_key = :rule

      def model_json(rule)
        {
          id:                        rule.id,
          title:                     rule.title,
          execution_count_past_week: @options[:weekly_usage][rule.id] || 0,
          updated_at:                rule.updated_at
        }
      end
    end
  end
end
