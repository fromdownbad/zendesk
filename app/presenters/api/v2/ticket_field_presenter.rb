require 'zendesk/tickets/ticket_field_options'

class Api::V2::TicketFieldPresenter < Api::V2::Presenter
  include Api::V2::Tickets::TicketFieldHelper
  include Zendesk::Tickets::TicketFieldOptions

  self.model_key = :ticket_field

  ## ### JSON Format
  ## Custom and system ticket fields have the following properties.
  ##
  ## | Name                  | Type    | Read-only | Mandatory | Comment
  ## | --------------------- | ------- | --------- | --------- | -------
  ## | id                    | integer | yes       | no        | Automatically assigned when created
  ## | url                   | string  | yes       | no        | The URL for this resource
  ## | type                  | string  | no*       | yes       | System or custom field type. *Editable for custom field types and only on creation. See [Create Ticket Field](#create_ticket_field)
  ## | title                 | string  | no        | yes       | The title of the ticket field
  ## | raw_title             | string  | no        | no        | The dynamic content placeholder if present, or the "title" value if not. See [Dynamic Content](./dynamic_content)
  ## | description           | string  | no        | no        | Describes the purpose of the ticket field to users
  ## | raw_description       | string  | no        | no        | The dynamic content placeholder if present, or the "description" value if not. See [Dynamic Content](./dynamic_content)
  ## | position              | integer | no        | no        | The relative position of the ticket field on a ticket. Note that for accounts with ticket forms, positions are controlled by the different forms
  ## | active                | boolean | no        | no        | Whether this field is available
  ## | required              | boolean | no        | no        | If true, agents must enter a value in the field to change the ticket status to solved
  ## | collapsed_for_agents  | boolean | no        | no        | If true, the field is shown to agents by default. If false, the field is hidden alongside infrequently used fields. Classic interface only
  ## | regexp_for_validation | string  | no        | no        | For "regexp" fields only. The validation pattern for a field value to be deemed valid
  ## | title_in_portal       | string  | no        | no        | The title of the ticket field for end users in Help Center
  ## | raw_title_in_portal   | string  | no        | no        | The dynamic content placeholder if present, or the "title_in_portal" value if not. See [Dynamic Content](./dynamic_content)
  ## | visible_in_portal     | boolean | no        | no        | Whether this field is visible to end users in Help Center
  ## | editable_in_portal    | boolean | no        | no        | Whether this field is editable by end users in Help Center
  ## | required_in_portal    | boolean | no        | no        | If true, end users must enter a value in the field to create the request
  ## | tag                   | string  | no        | no        | For "checkbox" fields only. A tag added to tickets when the checkbox field is selected
  ## | created_at            | date    | yes       | no        | The time the custom ticket field was created
  ## | updated_at            | date    | yes       | no        | The time the custom ticket field was last updated
  ## | system_field_options  | array   | yes       | no        | Presented for a system ticket field of type "tickettype", "priority" or "status"
  ## | custom_field_options  | array   | no        | yes       | Required and presented for a custom ticket field of type "multiselect" or "tagger"
  ## | sub_type_id           | integer | no        | no        | For system ticket fields of type "priority" and "status". Defaults to 0. A "priority" sub type of 1 removes the "Low" and "Urgent" options. A "status" sub type of 1 adds the "On-Hold" option
  ## | removable             | boolean | yes       | no        | If false, this field is a system field that must be present on all tickets
  ## | agent_description     | string  | no        | no        | A description of the ticket field that only agents can see
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                    34,
  ##   "url":                   "https://company.zendesk.com/api/v2/ticket_fields/34.json",
  ##   "type":                  "subject",
  ##   "title":                 "Subject",
  ##   "raw_title":             "{{dc.my_title}}",
  ##   "description":           "This is the subject field of a ticket",
  ##   "raw_description":       "This is the subject field of a ticket",
  ##   "position":              21,
  ##   "active":                true,
  ##   "required":              true,
  ##   "collapsed_for_agents":  false,
  ##   "regexp_for_validation": null,
  ##   "title_in_portal":       "Subject",
  ##   "raw_title_in_portal":   "{{dc.my_title_in_portal}}",
  ##   "visible_in_portal":     true,
  ##   "editable_in_portal":    true,
  ##   "required_in_portal":    true,
  ##   "tag":                   null,
  ##   "created_at":            "2009-07-20T22:55:29Z",
  ##   "updated_at":            "2011-05-05T10:38:52Z",
  ##   "removable":             false
  ##   "agent_description":     "This is the agent only description for the subject field"
  ## }
  ## ```
  def model_json(ticket_field)
    json = super.merge!(
      id: ticket_field.id,
      type: ticket_field_type(ticket_field),
      title: display_or_translate(ticket_field, :title),
      raw_title: ticket_field.title,
      description: display_or_translate(ticket_field, :description),
      raw_description: ticket_field.description,
      position: ticket_field.position,
      active: ticket_field.is_active?,
      required: ticket_field.is_required?,
      collapsed_for_agents: ticket_field.is_collapsed_for_agents?,
      regexp_for_validation: ticket_field.regexp_for_validation,
      title_in_portal: display_or_translate(ticket_field, :title_in_portal),
      raw_title_in_portal: ticket_field.title_in_portal,
      visible_in_portal: ticket_field.is_visible_in_portal?,
      editable_in_portal: ticket_field.is_editable_in_portal?,
      required_in_portal: ticket_field.is_required_in_portal?,
      tag: ticket_field.tag,
      created_at: ticket_field.created_at,
      updated_at: ticket_field.updated_at,
      removable: ticket_field.is_removable?
    )

    json[:agent_description] = ticket_field.agent_description.presence

    field_options = system_field_options_for(ticket_field)

    if field_options
      json[:system_field_options] = field_options
    elsif ticket_field.is_a?(FieldTagger)
      json[:custom_field_options] = cached_custom_field_options(ticket_field)
    end

    if options[:creator]
      json[:creator_user_id] = creator_user_id(ticket_field)
      json[:creator_app_name] = ticket_field.creator_app_name
    end

    json[:sub_type_id] = ticket_field.sub_type_id if ticket_field.accepts_sub_type?

    json
  end

  def cached_custom_field_options(ticket_field)
    if account.has_custom_field_options_caching?
      Rails.cache.fetch(custom_field_options_cache_key(ticket_field), compress: true) do
        custom_field_options(ticket_field)
      end
    else
      custom_field_options(ticket_field)
    end
  end

  def custom_field_options(ticket_field)
    ticket_field.custom_field_options.map do |custom_field|
      {
        id: custom_field["id"],
        name: render_dynamic_content(custom_field["name"]),
        raw_name: custom_field["name"],
        value: custom_field.value,
        default: custom_field["default"]
      }
    end
  end

  def custom_field_options_cache_key(ticket_field)
    # Includes locale because custom field options can contain dynamic content.
    # The ticket_fields scoped cache key expires whenever a cms text or variant is updated (CachingObserver).
    ActiveSupport::Cache.expand_cache_key(
      [
        'ticket_field_presenter',
        '1',
        user_locale,
        account.scoped_cache_key(:ticket_fields),
        ticket_field.cache_key
      ]
    )
  end

  def system_field_options_for(ticket_field)
    case ticket_field
    when FieldTicketType
      system_field_options(TicketType, get_ticket_types_for(account))
    when FieldPriority
      system_field_options(PriorityType, get_priorities_for(account))
    when FieldStatus
      system_field_options(StatusType, get_status_for(account))
    end
  end

  def system_field_options(type, options)
    options.map do |name, idx|
      {
        name: name,
        value: type[idx].to_s.downcase
      }
    end
  end

  def display_or_translate(ticket_field, field_type)
    multilingual_field_text = ticket_field.multilingual_field(user_locale: user_locale, account_locale: account_locale, field_type: field_type)
    render_dynamic_content(multilingual_field_text, locale: options[:locale])
  end

  def association_preloads
    return {} unless options[:creator]
    { creation_event: {} }
  end

  def side_loads(ticket_field)
    return {} unless options[:creator]
    json = {}

    if side_load?(:users)
      # Grab all creator ids that are not nil and not the system user
      ticket_fields = Array[ticket_field].flatten
      creator_ids = ticket_fields.flat_map { |tf| tf.creation_event.try(&:actor_id) }.uniq.compact.select { |id| id != User.system_user_id }

      # We need to do an unscoped find here so we can find deleted users as well
      creators = account.all_users.find(creator_ids) if creator_ids.any?
      json[:users] = user_presenter.collection_presenter.model_json(creators)
    end
    json
  end

  # Ticket fields can be created by 3 different types of users:
  # - System created: -1(User.system) (can also be used in the case of monitor assumptions)
  # - User created: actor_id will be the id of the User
  # - App created: actor_id will be -1(User.system)
  # This value could also be nil if the ticket field is not a system field and has no corresponding audit event
  def creator_user_id(ticket_field)
    # Original ticket fields created during account creation do not have audit events
    if ticket_field.is_system_field?
      User.system_user_id
    else
      # Ticket fields created prior to Oct 2013 do not have audit events
      ticket_field.creation_event.try(:actor_id)
    end
  end

  def user_locale
    @user_locale ||= (options[:locale] || user.translation_locale)
  end

  def account_locale
    @account_locale ||= account.translation_locale
  end
end
