class Api::V2::Internal::CertificateIpPresenter < Api::V2::Presenter
  self.model_key = :certificate_ip

  def model_json(certificate_ip)
    attributes = certificate_ip.attributes.dup
    attributes['ip'] = certificate_ip.pod_vip_address if certificate_ip.sni?
    attributes
  end
end
