module Api
  module V2
    module Internal
      class DataDeletionAuditJobPresenter < Api::V2::Presenter
        self.model_key = :data_deletion_audit_job

        ## ### JSON Format
        ## DataDeletionAuditJob has the following keys:
        ##
        ## | Name                | Type    | Comment
        ## | ------------------- | ------- | -------
        ## | id                  | integer | Automatically assigned upon creation
        ## | shard_id            | integer | id of shard to perform deletion (moved) or null for all shards (canceled)
        ## | reason              | string  | "moved" if the account was shard moved; "canceled" if the account has been deactivated
        ## | job                 | string  | serialized classname of the job
        ## | status              | string  | "waiting", "enqueued", "started", "finished" or "failed"
        ## | created_at          | date    | when the job was enqueued
        ## | started_at          | date    | when the job has started
        ## | completed_at        | date    | when the job has completed (finished or failed)
        ## | error               | hash    | the end result of the job or error message
        ##
        ## #### Example
        ## ```js
        ## {
        ##   "data_deletion_audit_job": {
        ##     "id": 22,
        ##     "shard_id": null,
        ##     "reason": "cancellation",
        ##     "job": "Zendesk::Maintenance::Jobs::BaseDataDeleteJob",
        ##     "status": "enqueued",
        ##     "created_at": "2015-03-31T18:54:51Z",
        ##     "started_at": null,
        ##     "completed_at": null,
        ##     "error": null
        ##   }
        ## }
        ## ```
        def model_json(data_deletion_audit_job)
          {
            id:           data_deletion_audit_job.id,
            shard_id:     data_deletion_audit_job.shard_id,
            reason:       data_deletion_audit_job.reason,
            job:          data_deletion_audit_job.job,
            status:       data_deletion_audit_job.status,
            created_at:   data_deletion_audit_job.created_at,
            started_at:   data_deletion_audit_job.started_at,
            completed_at: data_deletion_audit_job.completed_at,
            error:        data_deletion_audit_job.error
          }
        end
      end
    end
  end
end
