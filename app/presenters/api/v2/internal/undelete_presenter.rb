class Api::V2::Internal::UndeletePresenter < Api::V2::Presenter
  self.model_key = :model

  def model_json(model)
    {
      id: id(model),
      parent_id: parent_id(model),
      deleted_at: deleted_at(model),
      name: name(model)
    }
  end

  private

  def name(model)
    case model
    when Forum then model.name
    when Entry then model.title
    when Post then model.body
    when Ticket then "#{model.nice_id} - #{model.title}"
    when Organization then "#{model.id} - #{model.name}"
    when Group then "#{model.id} - #{model.name}"
    else
      raise "Unsupported #{model.class}"
    end
  end

  def deleted_at(model)
    model.respond_to?(:deleted_at) ? model.deleted_at : model.updated_at
  end

  def id(model)
    model.respond_to?(:nice_id) ? model.nice_id : model.id
  end

  def parent_id(model)
    case model
    when Forum then model.category_id
    when Entry then model.forum_id
    when Post then model.entry_id
    when Ticket then model.account_id
    when Organization then model.account_id
    when Group then model.account_id
    else
      raise "Unsupported #{model.class}"
    end
  end
end
