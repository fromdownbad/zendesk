class Api::V2::Internal::InboundMailRateLimitAuditLogPresenter < Api::V2::AuditLogPresenter
  self.model_key = :audit_log

  def model_json(event)
    presenter = CIA::EventPresenter.new(FakeContext.new(current_user: user, current_account: user.account), event)

    json = super.merge!(
      id: event.id,
      actor_id: event.actor_id,
      source_id: event.source_id,
      source_type: event.source_type.underscore,
      source_label: presenter.source_label,
      message: event_message(event),
      action: event.action,
      change_description: presenter.change_descriptions.join("\n").strip_tags,
      ip_address: event.ip_address,
      created_at: event.created_at
    )
    json.delete(:url)

    json
  end

  private

  def event_message(event)
    JSON.parse(event.message)
  rescue JSON::ParserError
    ''
  end
end
