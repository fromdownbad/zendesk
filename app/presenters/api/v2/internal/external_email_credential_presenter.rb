module Api::V2::Internal
  class ExternalEmailCredentialPresenter < Api::V2::Presenter
    self.model_key = :external_email_credential

    def model_json(external_email_credential)
      attributes = {
        id: external_email_credential.id,
        active: external_email_credential.active?,
        credential_updated_at: external_email_credential.credential_updated_at,
        error_count: external_email_credential.error_count,
        last_error_at: external_email_credential.last_error_at,
        last_error_message: external_email_credential.last_error_message,
        last_fetched_at: external_email_credential.last_fetched_at,
        need_repair: external_email_credential.need_repair?
      }
      super.merge!(attributes)
    end
  end
end
