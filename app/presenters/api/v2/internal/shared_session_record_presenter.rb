class Api::V2::Internal::SharedSessionRecordPresenter < Api::V2::Presenter
  self.model_key = :shared_session_record

  def model_json(shared_session_record)
    {
      account_id: shared_session_record.account_id,
      user_id: shared_session_record.user_id,
      session_id: shared_session_record.session_id,
      expire_at: shared_session_record.expire_at,
      id: shared_session_record.id,
      data: {
        account: shared_session_record.data[:account],
        id: shared_session_record.data[:id],
        'warden.user.default.key': shared_session_record.data[:'warden.user.default.key'],
        auth_via: shared_session_record.data[:auth_via],
        auth_duration: shared_session_record.data[:auth_duration],
        auth_updated_at: shared_session_record.data[:auth_updated_at],
        auth_stored: shared_session_record.data[:auth_stored],
        auth_authenticated_at: shared_session_record.data[:auth_authenticated_at],
        user_role: shared_session_record.data[:user_role],
        csrf_token: shared_session_record.data[:csrf_token]
      }
    }
  end
end
