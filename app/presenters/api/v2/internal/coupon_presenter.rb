class Api::V2::Internal::CouponPresenter < Api::V2::Presenter
  self.model_key = :coupon

  def url(model)
    type = model.is_a?(ZendeskBillingCore::Zuora::Coupon) ? :zuora : :classic
    build_url model, type: type, format: :json
  end

  def model_json(model)
    hash = model.is_a?(ZendeskBillingCore::Zuora::Coupon) ? zuora_model_json(model) : classic_model_json(model)
    super.merge(hash)
  end

  private

  def zuora_model_json(model)
    {
      id:                      model.id,
      name:                    model.name,
      coupon_code:             model.coupon_code,
      forced_inactive:         model.forced_inactive?,
      discount_amount:         model.discount_amount,
      discount_type:           model.discount_type,
      duration:                model.duration,
      start_date:              model.start_date,
      end_date:                model.end_date,
      terms:                   model.terms,
      created_at:              model.created_at,
      updated_at:              model.updated_at,
      length_days:             model.length_days,
      effective:               model.effective,
      expiry:                  model.expiry,
      active?:                 model.active?,
      editable?:               model.editable?,
      new_record?:             model.new_record?,
      destroyed?:              model.destroyed?,
      valid?:                  model.valid?,
      errors:                  model.errors,
      coupon_redemptions:      model.coupon_redemptions,
      in_effective_window?:    model.in_effective_window?,
      after_effective_window?: model.after_effective_window?
    }
  end

  def classic_model_json(model)
    {
      id:                          model.id,
      coupon_code:                 model.coupon_code,
      type:                        model.type,
      forced_inactive:             model.forced_inactive,
      expiry:                      model.expiry,
      effective:                   model.effective,
      length_days:                 model.length_days,
      terms_and_conditions:        model.terms_and_conditions,
      discount_percentage:         model.discount_percentage,
      minimum_incremental_revenue: model.minimum_incremental_revenue,
      trial_plan_type:             model.trial_plan_type,
      trial_max_agents:            model.trial_max_agents,
      name:                        model.name,
      maximum_monthly_discount:    model.maximum_monthly_discount,
      created_at:                  model.created_at,
      updated_at:                  model.updated_at,
      active?:                     model.active?,
      editable?:                   model.editable?,
      discount_type:               model.discount_type,
      new_record?:                 model.new_record?,
      destroyed?:                  model.destroyed?,
      valid?:                      model.valid?,
      errors:                      model.errors,
      coupon_applications:         model.coupon_applications,
      in_effective_window?:        model.in_effective_window?,
      after_effective_window?:     model.after_effective_window?
    }
  end

  def build_url(model, options = { })
    if model.blank? || model.id.blank?
      url_builder.send(:api_v2_internal_coupons_url, options)
    else
      url_builder.send(:api_v2_internal_coupon_url, model, options)
    end
  end
end
