class Api::V2::Internal::ExperimentParticipationPresenter < Api::V2::Presenter
  self.model_key = :experiment_participation

  def model_json(model)
    Hash(model).slice(:name, :group, :version, :finished)
  end
end
