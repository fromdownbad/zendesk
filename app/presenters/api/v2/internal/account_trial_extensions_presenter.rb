class Api::V2::Internal::AccountTrialExtensionsPresenter < Api::V2::Presenter
  self.model_key = :trial_extensions

  ## ### JSON Format
  ## Account trial extensions data is only used for the internal API
  ##
  def model_json(account)
    result = {}
    result[:trial_extensions] = account.trial_extensions
    result
  end
end
