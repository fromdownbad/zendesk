module Api::V2::Internal::EntityLookup
  class ViewsTicketsPresenter < Api::V2::Presenter
    self.model_key = 'views-ticket'

    def model_json(ticket)
      {
        id: ticket.id
      }
    end

    def side_loads(_collection)
      includes
    end
  end
end
