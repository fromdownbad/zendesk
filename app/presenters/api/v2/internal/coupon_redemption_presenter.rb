class Api::V2::Internal::CouponRedemptionPresenter < Api::V2::Presenter
  self.model_key = :coupon_redemption

  def url(model)
    account = model.account
    url_builder.send(:api_v2_internal_account_coupon_redemptions_url, account_id: account.id, format: :json)
  end

  def model_json(model)
    hash = model.is_a?(ZendeskBillingCore::Zuora::CouponRedemption) ? zuora_model_json(model) : classic_model_json(model)
    super.merge(hash)
  end

  private

  def zuora_model_json(model)
    {
      type:               :zuora,
      coupon:             model.zuora_coupon,
      active?:            model.active?,
      created_at:         model.created_at,
      updated_at:         model.updated_at,
      billing_cycle_type: model.billing_cycle_type
    }
  end

  def classic_model_json(model)
    {
      type:                       :classic,
      coupon:                     model.coupon,
      active?:                    model.active?,
      created_at:                 model.created_at,
      updated_at:                 model.updated_at,
      initial_max_agents:         model.initial_max_agents,
      initial_plan_type:          model.initial_plan_type,
      initial_billing_cycle_type: model.initial_billing_cycle_type,
      initial_full_monthly_price: model.initial_full_monthly_price,
      exhaustion_date:            model.exhaustion_date
    }
  end
end
