module Api::V2::Internal
  class RecipientAddressPresenter < Api::V2::Presenter
    self.model_key = :recipient_address

    def model_json(recipient_address)
      attributes = {
        id: recipient_address.id,
        brand_id: recipient_address.brand_id,
        default: recipient_address.default,
        name: recipient_address.name,
        email: recipient_address.email,
        external_email_credential: external_email_credential(recipient_address),
        external_email_credential_id: recipient_address.external_email_credential_id,
        forwarding_status: recipient_address.forwarding_status,
        spf_status: recipient_address.spf_status,
        cname_status: recipient_address.cname_status,
        domain_verification_status: recipient_address.domain_verification_status,
        domain_verification_code: recipient_address.domain_verification_code,
        metadata: recipient_address.metadata,
        created_at: recipient_address.created_at,
        updated_at: recipient_address.updated_at
      }

      super.merge!(attributes)
    end

    private

    def external_email_credential(recipient_address)
      return nil if recipient_address.external_email_credential_id.nil?
      external_email_credential = account.external_email_credentials.where(id: recipient_address.external_email_credential_id).first_or_initialize
      external_email_credential_presenter.model_json(external_email_credential)
    end

    def external_email_credential_presenter
      @external_email_credential_presenter ||= Api::V2::Internal::ExternalEmailCredentialPresenter.new(user, options)
    end
  end
end
