class Api::V2::Internal::ZopimAccountPresenter < Api::V2::Presenter
  self.model_key = :account_id

  def model_json(account)
    account.id
  end
end
