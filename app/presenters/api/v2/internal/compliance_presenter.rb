class Api::V2::Internal::CompliancePresenter < Api::V2::Presenter
  self.model_key = :internal_compliances

  def model_json(compliance)
    super.merge!(
      id: compliance.id,
      name: compliance.name
    )
  end
end
