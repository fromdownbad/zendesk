class Api::V2::Internal::InboundMailRateLimitPresenter < Api::V2::Presenter
  self.model_key = :inbound_mail_rate_limit

  def model_json(inbound_mail_rate_limit)
    super.merge!(
      id: inbound_mail_rate_limit.id,
      account_id: inbound_mail_rate_limit.account_id,
      is_sender: inbound_mail_rate_limit.is_sender,
      description: inbound_mail_rate_limit.description,
      is_active: inbound_mail_rate_limit.is_active,
      email: inbound_mail_rate_limit.email,
      ticket_id: inbound_mail_rate_limit.ticket_id,
      rate_limit: inbound_mail_rate_limit.rate_limit,
      created_at: inbound_mail_rate_limit.created_at,
      updated_at: inbound_mail_rate_limit.updated_at,
      deleted_at: inbound_mail_rate_limit.deleted_at
    )
  end

  def url(inbound_mail_rate_limit)
    url_builder.send(:api_v2_internal_account_inbound_mail_rate_limit_url, inbound_mail_rate_limit.account_id, inbound_mail_rate_limit, format: :json)
  end
end
