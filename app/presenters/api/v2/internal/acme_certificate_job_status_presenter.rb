class Api::V2::Internal::AcmeCertificateJobStatusPresenter < Api::V2::Presenter
  self.model_key = :acme_certificate_job_status

  def model_json(job_status)
    job_status.attributes.dup
  end
end
