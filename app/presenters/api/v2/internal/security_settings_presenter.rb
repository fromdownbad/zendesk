class Api::V2::Internal::SecuritySettingsPresenter < Api::V2::Presenter
  self.model_key = :security_settings

  ## ### JSON Format
  ## Security Settings are only accessible via an internal API
  ##
  def model_json(account)
    stringify_integers security_settings_model_json(account)
  end

  private

  def security_settings_model_json(account)
    {
      admins_can_set_user_passwords: account.admins_can_set_user_passwords,
      assumption_duration: account.settings.assumption_duration,
      assumption_expiration: account.settings.assumption_expiration,
      # indicates when an account is always assumable ( based on account type like trial, sandbox etc -> do not show the assumption section when this is true )
      assumable_account_type: account.assumable_account_type?,
      assumable: account.assumable?,
      custom_session_timeout_is_available: account.has_custom_session_timeout?,
      email_agent_when_sensitive_fields_changed: account.settings.email_agent_when_sensitive_fields_changed?,
      session_timeout: account.settings.session_timeout,
      two_factor_last_update: account.two_factor_last_update,
      authentication: {
        agent: {
          security_policy_id: account.role_settings.agent_security_policy_id,
          google_login: account.role_settings.agent_google_login,
          office_365_login: account.role_settings.agent_office_365_login,
          zendesk_login: account.role_settings.agent_zendesk_login,
          remote_login: account.role_settings.agent_remote_login,
          enforce_sso: !account.role_settings.agent_password_allowed,
          remote_bypass: account.role_settings.agent_remote_bypass,
          password: custom_security_policy(account)
        },
        end_user: {
          security_policy_id: account.role_settings.end_user_security_policy_id,
          google_login: account.role_settings.end_user_google_login,
          office_365_login: account.role_settings.end_user_office_365_login,
          zendesk_login: account.role_settings.end_user_zendesk_login,
          remote_login: account.role_settings.end_user_remote_login,
          twitter_login: account.role_settings.end_user_twitter_login,
          facebook_login: account.role_settings.end_user_facebook_login,
          enforce_sso: !account.role_settings.end_user_password_allowed
        },
        two_factor_enforce: account.settings.two_factor_enforce
      },
      remote_authentications: remote_authentications(account),
      ip: {
        is_available: account.has_ip_restriction?,
        ip_ranges: account.texts.ip_restriction,
        ip_restriction_enabled: account.settings.ip_restriction_enabled?,
        enable_agent_ip_restrictions: account.settings.enable_agent_ip_restrictions?,
      }
    }
  end

  def remote_authentications(account)
    remote_authentications = account.remote_authentications
    {
      saml: saml_auth(remote_authentications.saml.first),
      jwt: jwt_auth(remote_authentications.jwt.first)
    }
  end

  def saml_auth(saml)
    saml = account.remote_authentications.saml.build unless saml
    {
      id: saml.id,
      is_active: saml.is_active,
      is_available: account.has_saml?,
      priority: saml.priority,
      remote_login_url: saml.remote_login_url,
      remote_logout_url: saml.remote_logout_url,
      fingerprint: saml.token,
      ip_ranges: saml.ip_ranges
    }
  end

  def jwt_auth(jwt)
    jwt = account.remote_authentications.jwt.build unless jwt
    {
      id: jwt.id,
      is_active: jwt.is_active,
      is_available:  account.has_jwt?,
      priority: jwt.priority,
      remote_login_url: jwt.remote_login_url,
      remote_logout_url: jwt.remote_logout_url,
      shared_secret: jwt.token.nil? ? 'not configured' : jwt.token.slice(0, 6),
      update_external_ids: jwt.update_external_ids,
      ip_ranges: jwt.ip_ranges,
      alt_shared_secret: Token.generate(48, false)
    }
  end

  def custom_security_policy(account) # if you have updated agent_security_policy_id via the internal api, a custom_security_policy would always exist
    policy = account.custom_security_policy || CustomSecurityPolicy.build_from_current_policy(account)
    {
      is_available: account.has_custom_security_policy?,
      password_history_length: policy.password_history_length,
      password_length: policy.password_length,
      password_complexity: policy.password_complexity,
      password_in_mixed_case: policy.password_in_mixed_case,
      failed_attempts_allowed: policy.failed_attempts_allowed,
      max_sequence: policy.max_sequence,
      disallow_local_part_from_email: policy.disallow_local_part_from_email,
      password_duration: policy.password_duration
    }
  end

  def stringify_integers(hash)
    hash.each do |k, v|
      if v.is_a?(Hash)
        stringify_integers(v)
      else
        hash[k] = v.to_s if v.is_a?(Integer)
      end
    end
  end
end
