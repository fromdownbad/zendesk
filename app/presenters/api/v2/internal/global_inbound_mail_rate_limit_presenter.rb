class Api::V2::Internal::GlobalInboundMailRateLimitPresenter < Api::V2::Presenter
  self.model_key = :global_inbound_mail_rate_limit

  ## ### JSON Format
  ## Global inbound mail rate limits have the following keys:
  ##
  ## | Name                | Type    | Read-only  | Comment
  ## | ------------------- | ------- | ---------- | -------
  ## | email               | string  | yes        | The email address
  ## | rate_limit          | integer | yes        | The rate limit for the email address

  def model_json(global_inbound_mail_rate_limit)
    {
      email: global_inbound_mail_rate_limit.email,
      rate_limit: global_inbound_mail_rate_limit.rate_limit
    }
  end
end
