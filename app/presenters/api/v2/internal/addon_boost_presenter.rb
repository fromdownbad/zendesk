module Api::V2::Internal
  class AddonBoostPresenter < Api::V2::Presenter
    include Zendesk::Features::Presentation

    self.model_key = :addon_boost

    def model_json(model)
      super.merge!(
        id:               model.id,
        feature_name:     model.name,
        readable_name:    readable_name(model.name),
        boost_expires_at: model.boost_expires_at
      )
    end

    def url(model)
      url_builder.send(:api_v2_internal_addon_boosts_url, model, format: :json)
    end
  end
end
