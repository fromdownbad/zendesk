class Api::V2::Internal::CommentPresenter < Api::V2::Tickets::CommentPresenter
  self.model_key = :comment

  def model_json(comment)
    super.merge!(ticket_id: comment.ticket.nice_id)
  end

  def collection_presenter
    @collection_presenter ||= Api::V2::Internal::CommentCollectionPresenter.new(user, options.merge(item_presenter: self, includes: includes))
  end

  def association_preloads
    {
      attachments: attachment_presenter.association_preloads
    }
  end
end
