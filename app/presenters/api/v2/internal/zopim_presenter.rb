class Api::V2::Internal::ZopimPresenter < Api::V2::Presenter
  self.model_key = :zopim

  def model_json(account)
    {
      account_id: account.id
    }
  end
end
