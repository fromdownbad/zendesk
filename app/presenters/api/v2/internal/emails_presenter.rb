class Api::V2::Internal::EmailsPresenter < Api::V2::Presenter
  self.model_key = :email

  def model_json(email)
    {
      id: email.id,
      user_id: email.user_id,
      type: email.type,
      value: email.value,
      verified: email.is_verified,
      primary: email.primary?,
      updated_at: email.updated_at,
      created_at: email.created_at
    }
  end
end
