class Api::V2::Internal::AccountSettingsPresenter < Api::V2::Presenter
  self.model_key = :settings

  ## ### JSON Format
  ## Account stats are only used for the internal API
  ##
  def model_json(account)
    json = {
      boost:                         nil,
      cdn_provider:                  account.settings.cdn_provider,
      default_cdn_provider:          account.default_cdn_provider,
      import_mode:                   account.api_rate_limit_boosted?,
      password_reset_override:       account.settings.password_reset_override?,
      captcha_required:              account.settings.captcha_required?,
      x_frame_header:                account.settings.x_frame_header? || account.settings.security_headers?,
      xss_header:                    account.settings.xss_header?,
      csp_header:                    account.settings.csp_header?,
      content_type_header:           account.settings.content_type_header?,
      prefer_lotus:                  account.settings.prefer_lotus?,
      ssl_enabled:                   account.ssl_should_be_used?,
      archive:                       account.settings.archive,
      archive_after_days:            account.settings.archive_after_days,
      close_after_days:              account.settings.close_after_days,
      max_ticket_batch_close_count:  account.settings.max_ticket_batch_close_count,
      voice_trial_enabled:           account.settings.voice_trial_enabled?,
      monitored_facebook_page_limit: account.settings.monitored_facebook_page_limit,
      spam_processor:                account.spam_protection_tool,
      spam_threshold_multiplier:     account.settings.spam_threshold_multiplier,
      end_user_entry_rate_limit:     account.settings.end_user_entry_rate_limit,
      end_user_comment_rate_limit:   account.settings.end_user_comment_rate_limit,
      hc_settings:                   hc_settings_presenter.model_json(account),
      partner_settings:              partner_settings_presenter.model_json(account),
      allow_test_locales:            account.settings.allow_test_locales?,
      allow_keys_locales:            account.settings.allow_keys_locales?,
      default_available_locales:     account.locales_for_selection.map(&:id),
      spam_prevention:               account.spam_prevention.to_s,
      active_brand_count:            account.active_brands.count(:all),
      owner:                         agent_presenter.model_json(account.owner),
      can_be_boosted:                account.can_be_boosted?,
      default_api_rate_limit:        account.default_api_rate_limit,
      default_api_time_rate_limit:   account.default_api_time_rate_limit,
      api_time_rate_limit:           account.api_time_rate_limit,
      bulk_user_upload:              account.settings.bulk_user_upload,
      is_abusive:                    account.abusive?,
      is_watchlisted:                account.settings.is_watchlisted,
      risk_assessment:               account.settings.risk_assessment,
      has_google_apps:               account.has_google_apps?,
      fraudulent:                    account.abusive?,
      support_risky:                 account.support_risky?,
      total_in_flight_jobs_limit:    account.settings.total_in_flight_jobs_limit,
      show_csat_on_dashboard:        account.settings.show_csat_on_dashboard,
      native_messaging:              account.settings.native_messaging?,
      default_api_incremental_exports_rate_limit: account.default_api_incremental_exports_rate_limit,
      api_incremental_exports_rate_limit:         account.api_incremental_exports_rate_limit,
      email_agent_when_sensitive_fields_changed:  account.settings.email_agent_when_sensitive_fields_changed,
      end_user_portal_ticket_rate_limit:          account.settings.end_user_portal_ticket_rate_limit,
      user_identity_limit_for_trialers:           account.settings.user_identity_limit_for_trialers,
      agent_count: {
        normal: account.billable_agent_count,
        light:  account.light_agents.count(:all),
        chat:   account.chat_agents.count(:all)
      },
      remote_auth: {
        saml: account.remote_authentications.saml.active.exists?,
        jwt: account.remote_authentications.jwt.active.exists?
      }
    }

    json[:security_headers] = json[:x_frame_header]

    if account.feature_boost.present?
      json.merge!(boost_presenter.present(account.feature_boost))
    end

    if account.subscription_feature_addons.boosted.present?
      json.merge!(addon_boost_presenter.collection_presenter.present(account.subscription_feature_addons.boosted.to_a))
    end

    if account.host_mapping.present?
      json[:host_mapped_url] = account.url(mapped: true)
    end

    json
  end

  def boost_presenter
    @boost_presenter ||= options[:boost_presenter] ||= Api::V2::Internal::BoostPresenter.new(user, options)
  end

  def hc_settings_presenter
    @hc_settings_presenter ||= options[:hc_settings_presenter] ||= Api::V2::Account::HcSettingsPresenter.new(user, options)
  end

  def partner_settings_presenter
    @partner_settings_presenter ||= options[:partner_settings_presenter] ||= Api::V2::Account::PartnerSettingsPresenter.new(user, options)
  end

  def agent_presenter
    @agent_presenter ||= options[:agent_presenter] ||= Api::V2::Users::AgentPresenter.new(user, options)
  end

  def addon_boost_presenter
    @addon_boost_presenter ||= Api::V2::Internal::AddonBoostPresenter.new(user, options)
  end
end
