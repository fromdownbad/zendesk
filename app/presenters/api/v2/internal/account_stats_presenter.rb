class Api::V2::Internal::AccountStatsPresenter < Api::V2::Presenter
  self.model_key = :account_stats

  ## ### JSON Format
  ## Account stats are only used for the internal API
  ##
  def model_json(account)
    # Use cached count if it's available (CT-1486)
    user_count = account.settings.user_count || account.users.capped_count(1_000_000)
    result = {
      user_count: user_count.to_s,
      organization_count: account.organizations.count(:all),
      agent_count: {
        normal: account.billable_agent_count,
        light:  account.light_agents.count(:all),
        chat:   account.chat_agents.count(:all)
      },
      admin_count: account.admins.count(:all),
      group_count: account.groups.count(:all),
      entry_count: account.entries.count(:all),
      brand_count: account.brands.count(:all),
      inactive_brand_count: account.inactive_brands.count(:all),
      active_brand_count: account.active_brands.count(:all),
      ticket_count: {
        normal:    account.tickets.count(:all),
        archived:  account.ticket_archive_stubs.count(:all),
        suspended: account.suspended_tickets.count(:all),
        deleted:   Ticket.with_deleted { account.deleted_tickets.count(:all) } + account.ticket_archive_stubs.deleted.count(:all)
      },
      ticket_forms_count: account.ticket_forms.count(:all),
      ticket_fields_count: account.ticket_fields.count(:all),
      ticket_field_conditions_count: account.ticket_field_conditions.count(:all),
      custom_field_options_count: account.custom_field_options.count(:all),
      sla_policies_count: account.sla_policies.active.count(:all)
    }

    result[:rule_count] = rule_count_presenter.model_json(account)
    result[:settings]   = account_settings_presenter.model_json(account)
    result[:owner]      = agent_presenter.model_json(account.owner)

    result
  end

  def rule_count_presenter
    @rule_count_presenter ||= options[:rule_count_presenter] ||= Api::V2::Internal::RuleCountPresenter.new(user, options)
  end

  def account_settings_presenter
    @account_settings_presenter ||= options[:account_settings_presenter] ||= Api::V2::Internal::AccountSettingsPresenter.new(user, options)
  end

  def agent_presenter
    @agent_presenter ||= options[:agent_presenter] ||= Api::V2::Users::AgentPresenter.new(user, options)
  end
end
