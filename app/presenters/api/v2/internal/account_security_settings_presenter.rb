class Api::V2::Internal::AccountSecuritySettingsPresenter < Api::V2::Presenter
  self.model_key = :security_settings

  ## ### JSON Format
  ## Account security settings are only used for the internal API
  ##
  def model_json(account)
    result = {}

    result[:remote_auth] = {
      saml: account.remote_authentications.saml.active.exists?,
      jwt: account.remote_authentications.jwt.active.exists?,
      primary: account.remote_authentications.active.first.try(:auth_mode)
    }

    result[:password_reset_override]       = account.settings.password_reset_override?
    result[:captcha_required]              = account.settings.captcha_required?
    result[:x_frame_header]                = account.settings.x_frame_header? || account.settings.security_headers?
    result[:security_headers]              = result[:x_frame_header]
    result[:email_agent_when_sensitive_fields_changed] = account.settings.email_agent_when_sensitive_fields_changed
    result[:xss_header]                    = account.settings.xss_header?
    result[:csp_header]                    = account.settings.csp_header?
    result[:content_type_header]           = account.settings.content_type_header?
    result[:ssl_enabled]                   = account.ssl_should_be_used?
    result[:spam_processor]                = account.spam_protection_tool
    result[:spam_threshold_multiplier]     = account.settings.spam_threshold_multiplier
    result[:end_user_comment_rate_limit]   = account.settings.end_user_comment_rate_limit
    result[:spam_prevention]               = account.spam_prevention.to_s
    result
  end
end
