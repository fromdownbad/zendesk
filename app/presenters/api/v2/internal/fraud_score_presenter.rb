module Api::V2::Internal
  class FraudScorePresenter < Api::V2::Presenter
    self.model_key = :fraud_score

    ## ### JSON Format
    ## Fraud Scores are represented as JSON objects which have the following keys:
    ##
    ## | Name                           | Type    | Read-only | Mandatory | Comment
    ## | ------------------------------ | ------- | --------- | --------- | -------
    ## | account_id                     | integer | yes       | no        |
    ## | verified_fraud                 | boolean | yes       | yes       | When the value is 0 it has been verified by the ML model as safe, 1 for fraudulent
    ## | score                          | boolean | yes       | no        | Fraud score at the time of account creation given by ML model, 0 as safe, 1 for fraudulent
    ## | account_fraud_service_release  | string  | yes       | yes       | The version of the ML model attributed to the score
    ## | owner_email                    | string  | yes       | yes       | Owner email address at sign up time
    ## | subdomain                      | string  | yes       | yes       | Subdomain at sign up time
    ## | other_params                   | text    | yes       | yes       | Any extra params we may wish to include in the future in JSON format
    ## | is_whitelisted                 | boolean | yes       | no        | Value set to true if account is manually whitelisted by human
    ##
    ## #### Example
    ## ```json
    ## {
    ##   "account_id":                      891865,
    ##   "verified_fraud":                  1,
    ##   "score":                           1,
    ##   "score_params":                    { "blacklisted_subdomain": false, "distance_subdomain_email_domain": 0, }
    ##   "is_whitelisted":                  0,
    ##   "account_fraud_service_release":   "1.0.0",
    ##   "source_event":                    "new_trial",
    ##   "owner_email":                     "spammy_email@gmail.com",
    ##   "subdomain":                       "gmail",
    ##   "other_params":                    {[accounts_by_owner]:5},
    ## }
    ## ```
    def model_json(model)
      {
        id:                            model.id,
        account_id:                    model.account_id,
        verified_fraud:                model.verified_fraud,
        is_whitelisted:                model.is_whitelisted,
        score:                         model.score,
        account_fraud_service_release: model.account_fraud_service_release,
        score_params:                  model.score_params,
        source_event:                  model.source_event,
        owner_email:                   model.owner_email,
        subdomain:                     model.subdomain,
        created_at:                    model.created_at,
        talk_risky:                    model.talk_risky,
        support_risky:                 model.support_risky,
        other_params:                  model.other_params
      }
    end
  end
end
