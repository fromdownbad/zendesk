module Api
  module V2
    module Internal
      class DataDeletionAuditPresenter < Api::V2::Presenter
        self.model_key = :data_deletion_audit

        ## ### JSON Format
        ## DataDeletionAudit has the following keys:
        ##
        ## | Name                | Type    | Comment
        ## | ------------------- | ------- | -------
        ## | id                  | integer | Automatically assigned upon creation
        ## | shard_id            | integer | id of shard to perform deletion (moved) or null for all shards (canceled)
        ## | reason              | string  | "moved" if the account was shard moved; "canceled" if the account has been deactivated
        ## | status              | string  | "waiting", "enqueued", "started", "finished" or "failed"
        ## | created_at          | date    | when the account was soft deleted or when the shard move has finished
        ## | started_at          | date    | when the first deletion jobs were scheduled
        ## | completed_at        | date    | when all of the deletion jobs have succeeded
        ## | jobs                | array   | list of data deletion jobs and their statuses
        ## | failed_reason       | hash    | exception klass, message, and backtrace when permanently failing a deletion request
        ##
        ## #### Example
        ## ```js
        ## {
        ##   "data_deletion_audit": {
        ##     "url": "https://minimum.zendesk-test.com/api/v2/internal/data_deletion_audits/12.json",
        ##     "id": 12,
        ##     "shard_id": null,
        ##     "reason": "cancellation",
        ##     "status": "started",
        ##     "created_at": "2015-02-19T18:54:51Z",
        ##     "started_at": "2015-03-31T18:54:51Z",
        ##     "completed_at": null,
        ##     "jobs": [...]
        ##   }
        ## }
        ## ```
        def model_json(data_deletion_audit)
          shard_id = if data_deletion_audit.reason == 'moved'
            data_deletion_audit.shard_id
          else
            data_deletion_audit.reason == 'canceled' ? data_deletion_audit.account.shard_id : nil
          end

          super.merge!(
            id:             data_deletion_audit.id,
            shard_id:       shard_id,
            account_id:     data_deletion_audit.account_id,
            subdomain:      data_deletion_audit.account.subdomain,
            reason:         data_deletion_audit.reason,
            status:         data_deletion_audit.status,
            created_at:     data_deletion_audit.created_at,
            started_at:     data_deletion_audit.started_at,
            completed_at:   data_deletion_audit.completed_at,
            jobs:           data_deletion_audit_job_presenter.collection_presenter.model_json(data_deletion_audit.job_audits),
            failed_reason:  data_deletion_audit.failed_reason
          )
        end

        def url(data_deletion_audit)
          url_builder.send(:api_v2_internal_data_deletion_audit_url, data_deletion_audit, format: :json)
        end
      end
    end
  end
end
