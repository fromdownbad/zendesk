module Api::V2::Internal
  class PredictionSettingsPresenter
    TRIGGERS_LIMIT = 50

    def present(deflection_triggers, potential_deflection_triggers)
      {
        prediction_settings: {
          deflection_triggers: as_json(deflection_triggers.take(TRIGGERS_LIMIT)),
          potential_deflection_triggers: as_json(potential_deflection_triggers.take(TRIGGERS_LIMIT))
        }
      }
    end

    private

    def as_json(triggers)
      triggers.map do |trigger|
        {
          id: trigger.id,
          title: trigger.title,
          permalink: trigger.lotus_permalink,
          usage_30d: trigger.usage.monthly
        }
      end
    end
  end
end
