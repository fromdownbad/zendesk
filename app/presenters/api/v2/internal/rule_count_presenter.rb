class Api::V2::Internal::RuleCountPresenter < Api::V2::Presenter
  self.model_key = :rule_count

  ## ### JSON Format
  ## Account stats are only used for the internal API
  ##
  def model_json(account)
    result = {}

    [View, Macro, Trigger, Automation].each do |type|
      result[type.name.pluralize.downcase] = {
        total: type.where(account_id: account.id).count(:all),
        active: type.where(account_id: account.id, is_active: true).count(:all)
      }
    end

    result
  end
end
