class Api::V2::Internal::ComplianceAgreementPresenter < Api::V2::Presenter
  self.model_key = :internal_compliance_agreements

  def model_json(compliance_agreement)
    super.merge!(
      id: compliance_agreement.id,
      name: compliance_agreement.compliance.name
    )
  end
end
