class Api::V2::Internal::VoiceBurnRatePresenter < Api::V2::Presenter
  self.model_key = :burn_rate

  def model_json(burn_rate)
    {
      units: burn_rate[:units],
      date:  burn_rate[:created_at]
    }
  end
end
