class Api::V2::Internal::StaffPresenter < Api::V2::Presenter
  self.model_key = :user

  def model_json(user)
    {
      account_id: user.account_id,
      created_at: user.created_at,
      id: user.id,
      is_active: user.is_active,
      last_login: user.last_login,
      locale_id: user.translation_locale.id,
      name: user.name,
      roles: user.roles,
      time_zone: user.time_zone,
      updated_at: user.updated_at,
      crypted_password: user.crypted_password,
      salt: user.salt
    }
  end
end
