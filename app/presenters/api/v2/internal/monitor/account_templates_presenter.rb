module Api
  module V2
    module Internal
      module Monitor
        class AccountTemplatesPresenter < Api::V2::Presenter
          self.model_key = :templates

          TEMPLATE_NAMES = %w[text_mail html_mail signup_email_text verify_email_text].freeze

          def model_json(account)
            json = {
                text_mail: account.text_mail_template.try(:sanitize),
                text_mail_changed: text_mail_changed?,
                html_mail: account.html_mail_template.try(:sanitize),
                html_mail_changed: html_mail_changed?,
                signup_email_text: account.signup_email_text.try(:sanitize),
                signup_email_text_changed: signup_email_text_changed?,
                verify_email_text: account.verify_email_text.try(:sanitize),
                verify_email_text_changed: verify_email_text_changed?,
                template_names: TEMPLATE_NAMES
            }

            json
          end

          private

          def text_mail_changed?
            account.text_mail_template != Zendesk::OutgoingMail::Variables::TEXT_MAIL_TEMPLATE
          end

          def html_mail_changed?
            account.html_mail_template != Zendesk::OutgoingMail::Variables::ACCESSIBLE_HTML_MAIL_TEMPLATE
          end

          def signup_email_text_changed?
            account.signup_email_text != I18n.t("txt.default.new_welcome_email_text", account_name: account.name)
          end

          def verify_email_text_changed?
            account.verify_email_text != I18n.t("txt.default.verify_email_text")
          end
        end
      end
    end
  end
end
