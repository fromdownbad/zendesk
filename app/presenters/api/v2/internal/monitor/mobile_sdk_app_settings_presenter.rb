module Api::V2::Internal::Monitor
  class MobileSdkAppSettingsPresenter < Api::V2::Presenter
    self.model_key = :settings

    def model_json(account)
      {
        account: {
          id: account.id,
          subdomain: account.subdomain,
          app_count: account.mobile_sdk_apps.count,
          attachments: {
            enabled: account.is_attaching_enabled,
            max_attachment_size: account.max_attachment_size,
          }
        },
        apps: account.mobile_sdk_apps.map do |app|
          json = {
            title: app.title,
            app_id: app.identifier,
            oauth_client_id: app.mobile_sdk_auth ? app.mobile_sdk_auth.client.identifier : nil
          }.merge(app.fetch_settings_for_monitor)

          if app.settings.authentication == MobileSdkAuth::TYPE_JWT
            json[:jwt_url] = app.mobile_sdk_auth ? app.mobile_sdk_auth.endpoint : nil
          end

          json
        end
      }
    end
  end
end
