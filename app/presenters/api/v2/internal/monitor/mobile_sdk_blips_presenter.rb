module Api::V2::Internal::Monitor
  class MobileSdkBlipsPresenter < Api::V2::Presenter
    self.model_key = :settings

    BLIPS = 'blips'.freeze
    REQUIRED_BLIPS = 'required_blips'.freeze
    BEHAVIOURAL_BLIPS = 'behavioural_blips'.freeze
    PATHFINDER_BLIPS = 'pathfinder_blips'.freeze

    def model_json(account)
      {
        account: {
          id: account.id,
          blips: {
            enabled: account.settings.mobile_sdk_blips,
            name: BLIPS.titleize,
            permissions: [
              { name: REQUIRED_BLIPS.titleize, key: REQUIRED_BLIPS, enabled: account.settings.mobile_sdk_required_blips },
              { name: BEHAVIOURAL_BLIPS.titleize, key: BEHAVIOURAL_BLIPS, enabled: account.settings.mobile_sdk_behavioural_blips },
              { name: PATHFINDER_BLIPS.titleize, key: PATHFINDER_BLIPS, enabled: account.settings.mobile_sdk_pathfinder_blips }
            ]
          }
        },
        apps: account.mobile_sdk_apps.map do |app|
          {
            id: app.id,
            title: app.title,
            blips: {
              enabled: app.settings.blips,
              name: 'Blips',
              permissions: [
                { name: REQUIRED_BLIPS.titleize, key: REQUIRED_BLIPS, enabled: app.settings.required_blips },
                { name: BEHAVIOURAL_BLIPS.titleize, key: BEHAVIOURAL_BLIPS, enabled: app.settings.behavioural_blips },
                { name: PATHFINDER_BLIPS.titleize, key: PATHFINDER_BLIPS, enabled: app.settings.pathfinder_blips }
              ]
            }
          }
        end
      }
    end
  end
end
