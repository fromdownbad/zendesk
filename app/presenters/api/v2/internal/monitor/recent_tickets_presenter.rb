module Api
  module V2
    module Internal
      module Monitor
        class RecentTicketsPresenter < Api::V2::Presenter
          include ActionView::Helpers::TextHelper
          self.model_key = :tickets

          def model_json(ticket)
            json = {
              title: ticket.title,
              description: truncate(ticket.description.to_s, length: 300),
              active: !ticket.status_id.in?([StatusType.DELETED, StatusType.ARCHIVED]),
              creation_method: ViaType[ticket.via_id].try(:name) || 'n/a',
              collaborator_count: ticket.collaborators.count,
              requester: {
                name: ticket.requester.name,
                email: ticket.requester.email
              },
              created_at: ticket.created_at,
              nice_id:    ticket.nice_id
            }

            json
          end
        end
      end
    end
  end
end
