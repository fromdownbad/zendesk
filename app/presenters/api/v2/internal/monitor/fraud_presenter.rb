module Api
  module V2
    module Internal
      module Monitor
        class FraudPresenter < Api::V2::Presenter
          self.model_key = :fraud

          SOCIAL_MEDIA = [ViaType.TWITTER, ViaType.TWITTER_DM, ViaType.TWITTER_FAVORITE,
                          ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE].freeze

          RECENT_TICKET_DATE_THRESHOLD = 30.days.ago
          RECENT_TICKET_LIMIT = 5

          PRESENTER_CAP_AMOUNT = 1_000_000

          FRAUD_SCORE_LIMIT = 50

          # Due to the nature of massive accounts, with many millions of tickets, cap amount implemented to prevent timeouts

          def model_json(account)
            json = {
              active_tickets: active_tickets,
              active_users: account.users.capped_count(PRESENTER_CAP_AMOUNT),
              anonymous_ticket_submission_enabled: account.is_open?,
              email_integration: enabled_email_integration?,
              facebook_users: facebook_user_count,
              fraud_score_count: account.fraud_scores.capped_count(PRESENTER_CAP_AMOUNT),
              ip_address: account.trial_extras.to_hash['client_ip'],
              is_whitelisted: account.whitelisted?,
              premium_support: account.premium_support?,
              recent_fraud_scores: fraud_scores_presenter.collection_presenter.present(fraud_scores),
              recent_tickets: recent_tickets_presenter.collection_presenter.present(recent_tickets),
              require_sign_in_enabled: account.is_signup_required?,
              risk_score: account.first_fraud_score&.risk_score,
              templates: fraud_templates,
              total_tickets: total_tickets,
              total_users: total_users,
              twitter_users: twitter_user_count
            }

            json
          end

          private

          def active_tickets
            account.tickets.capped_count(PRESENTER_CAP_AMOUNT)
          end

          def should_cap_counts?
            active_tickets >= PRESENTER_CAP_AMOUNT
          end

          def total_tickets
            should_cap_counts? ? PRESENTER_CAP_AMOUNT : Ticket.unscoped.where(account_id: account.id).capped_count(PRESENTER_CAP_AMOUNT)
          end

          def total_users
            should_cap_counts? ? PRESENTER_CAP_AMOUNT : User.unscoped.where(account_id: account.id).capped_count(PRESENTER_CAP_AMOUNT)
          end

          def recent_tickets
            Ticket.unscoped.
              where(account_id: account.id).
              where('created_at >= ?', RECENT_TICKET_DATE_THRESHOLD).
              where('via_id NOT in (?)', SOCIAL_MEDIA).
              order('nice_id DESC').
              limit(RECENT_TICKET_LIMIT)
          end

          def recent_tickets_presenter
            Api::V2::Internal::Monitor::RecentTicketsPresenter.new(user, url_builder: url_builder)
          end

          def fraud_templates
            templates_presenter = Api::V2::Internal::Monitor::AccountTemplatesPresenter.new(user, url_builder: url_builder)
            templates_presenter.model_json(account)
          rescue StandardError => e
            Rails.logger.info "Could not fetch templates for account #{account.subdomain} - #{e}"
            {}
          end

          def fraud_scores
            account.fraud_scores.order("created_at DESC").limit(FRAUD_SCORE_LIMIT)
          end

          def fraud_scores_presenter
            @fraud_scores_presenter ||= Api::V2::Internal::FraudScorePresenter.new(user, url_builder: url_builder)
          end

          def twitter_user_count
            if !account.try(:owner).try(:twitter_handles_active?)
              0
            elsif should_cap_counts?
              PRESENTER_CAP_AMOUNT
            else
              UserIdentity.unscoped.where(account_id: account.id, type: 'UserTwitterIdentity').capped_count(PRESENTER_CAP_AMOUNT)
            end
          end

          def facebook_user_count
            if !account.try(:owner).try(:facebook_pages_active?)
              0
            elsif should_cap_counts?
              PRESENTER_CAP_AMOUNT
            else
              UserIdentity.unscoped.where(account_id: account.id, type: 'UserFacebookIdentity').capped_count(PRESENTER_CAP_AMOUNT)
            end
          end

          def enabled_email_integration?
            account.recipient_addresses.any? { |address| address.external_email_credential.present? }
          end
        end
      end
    end
  end
end
