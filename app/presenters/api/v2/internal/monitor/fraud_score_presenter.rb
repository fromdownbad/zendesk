module Api
  module V2
    module Internal
      module Monitor
        class FraudScorePresenter < Api::V2::Presenter
          self.model_key = :fraud_score

          def model_json(model)
            {
              id:                            model.id,
              account_id:                    model.account_id,
              verified_fraud:                model.verified_fraud,
              is_whitelisted:                model.is_whitelisted,
              score:                         model.score,
              account_fraud_service_release: model.account_fraud_service_release,
              score_params:                  model.score_params,
              source_event:                  model.source_event,
              owner_email:                   model.owner_email,
              subdomain:                     model.subdomain,
              created_at:                    model.created_at,
              other_params:                  model.other_params
            }
          end

          private

          def fraud_score_presenter
            @fraud_score_presenter ||= Api::V2::Internal::FraudScorePresenter.new(user, url_builder: self)
          end
        end
      end
    end
  end
end
