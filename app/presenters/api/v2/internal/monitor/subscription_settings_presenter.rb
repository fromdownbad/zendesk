module Api
  module V2
    module Internal
      module Monitor
        class SubscriptionSettingsPresenter < Api::V2::Presenter
          self.model_key = :settings

          # This presenter is cached using
          # account.settings.maximum(:updated_at)
          # Arturo::Feature.maximum(:updated_at)
          # account.subscription.updated_at
          def model_json(account)
            json = {
              boost:                                      nil,
              import_mode:                                account.api_rate_limit_boosted?,
              has_high_volume_api:                        account.subscription.has_high_volume_api?,
              can_be_boosted:                             account.can_be_boosted?,
              default_api_rate_limit:                     account.default_api_rate_limit,
              default_api_time_rate_limit:                account.default_api_time_rate_limit,
              api_rate_limit:                             account.api_rate_limit,
              api_time_rate_limit:                        account.api_time_rate_limit,
              default_api_incremental_exports_rate_limit: account.default_api_incremental_exports_rate_limit,
              api_incremental_exports_rate_limit:         account.api_incremental_exports_rate_limit,
            }

            if account.feature_boost.present?
              json.merge!(boost_presenter.present(account.feature_boost))
            end

            if account.subscription_feature_addons.boosted.present?
              json.merge!(addon_boost_presenter.collection_presenter.present(account.subscription_feature_addons.boosted.to_a))
            end

            json
          end

          private

          def boost_presenter
            @boost_presenter ||= Api::V2::Internal::BoostPresenter.new(user, options)
          end

          def addon_boost_presenter
            @addon_boost_presenter ||= Api::V2::Internal::AddonBoostPresenter.new(user, options)
          end
        end
      end
    end
  end
end
