require 'api/v2/account/hc_settings_presenter'

module Api
  module V2
    module Internal
      module Monitor
        class AccountSettingsPresenter < Api::V2::Presenter
          self.model_key = :settings

          # This presenter is cached using account.settings.maximum(:updated_at) and
          # Arturo::Feature.maximum(:updated_at)
          def model_json(account)
            account.settings.get.merge(
              # account.settings
              x_frame_header:       account.settings.x_frame_header? || account.settings.security_headers?,
              spam_processor:       account.spam_protection_tool,
              spam_prevention:      account.spam_prevention.to_s,
              ssl_enabled:          account.ssl_should_be_used?,

              # other account methods
              assumable_account_type: account.assumable_account_type?,
              assumable:              account.assumable?,
              always_assumable:       account.always_assumable?,
              support_risky:          account.support_risky?,
              auto_suspended:         account.auto_suspended?,

              # arturo features
              default_cdn_provider: account.default_cdn_provider,
              import_mode:          account.api_rate_limit_boosted?,

              help_center_state:    help_center_state_presenter.model_json(account),

              remote_auth:          {
                saml: account.remote_authentications.saml.active.exists?,
                jwt:  account.remote_authentications.jwt.active.exists?
              }
            )
          end

          private

          def help_center_state_presenter
            @help_center_state_presenter ||= Api::V2::Account::HcSettingsPresenter::StatePresenter.new(user, options)
          end
        end
      end
    end
  end
end
