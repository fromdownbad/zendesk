module Api
  module V2
    module Internal
      module Monitor
        class AgentPresenter < Api::V2::Presenter
          self.model_key = :user

          def model_json(agent)
            {
              id:            agent.id,
              name:          agent.name,
              email:         agent.email,
              role:          agent.role.to_s.downcase.presence,
              last_login_at: agent.last_login,
              created_at:    agent.created_at,
              updated_at:    agent.updated_at,
              suspended:     agent.suspended?
            }
          end
        end
      end
    end
  end
end
