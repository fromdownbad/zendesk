class Api::V2::Internal::CommentCollectionPresenter < Api::V2::Tickets::CommentCollectionPresenter
  def model_json(collection)
    Array(collection).map do |item|
      json = item_presenter.model_json(item)

      audit_event = item.audit
      if audit_event.nil?
        # NOTE: The nil checks on the audit are being done due to bad data in the database for the snapchat account
        # Comment with id #30894015409 is an audit object
        # TODO: find cause of missing audits and remove this patch
        json.merge!(via: serialized_via_object(item), metadata: {}, created_at: item.created_at)
      else
        json.merge!(via: serialized_via_object(audit_event), metadata: audit_event.metadata, created_at: item.created_at)
      end

      if item.event_decoration.present?
        json[:metadata] = json[:metadata].merge(decoration: item.event_decoration.data)
      end

      json
    end
  end

  def as_json(collection)
    super.merge(custom_pagination_params(collection))
  end

  def next_page_url(collection)
    return nil if collection.size < options[:limit]
    params = { ticket_ids: options[:ticket_ids], format: :json }.merge(custom_pagination_params(collection))
    url_builder.send(:show_many_comments_api_v2_internal_tickets_url, params)
  end

  def custom_pagination_params(collection)
    if collection.size < options[:limit]
      { id_waterline: nil, comment_id_waterline: nil }
    else
      { id_waterline: collection.last.ticket.nice_id, comment_id_waterline: collection.last.id}
    end
  end
end
