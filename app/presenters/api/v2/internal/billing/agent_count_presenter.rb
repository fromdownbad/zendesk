class Api::V2::Internal::Billing::AgentCountPresenter < Api::V2::Presenter
  self.model_key = :agent_counts

  ## ### JSON Format
  ## Agent counts are only used for the internal API
  ##
  def model_json(account)
    {
      # number of agents using (or that would use) a Suite seat
      suite_agents: suite_agent_ids(account).count
    }
  end

  private

  def chat_agent_ids(account)
    if account.multiproduct?
      # check the staff service cache for agents have Chat entitlement
      # NOTE: can't use #pluck here since we're working with an Array
      account.agents.select(&:cp4_chat_agent?).map(&:id)
    else
      # aggregate Chat-enabled Support agents and Chat-only agents
      (account.chat_enabled_support_agents.pluck(:id) +
        account.chat_only_agents.pluck(:id)).uniq
    end
  end

  def suite_agent_ids(account)
    # NOTE: can't use #pluck here since billable_agents isn't guaranteed to be
    # an ActiveRecord collection
    (account.billable_agents.map(&:id) + chat_agent_ids(account)).uniq
  end
end
