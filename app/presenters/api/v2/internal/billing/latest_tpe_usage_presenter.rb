class Api::V2::Internal::Billing::LatestTpeUsagePresenter < Api::V2::Presenter
  self.model_key = :usage

  ## ### JSON Format
  ## Voice::CtiUsage details, used internally for Suites
  ##
  def model_json(usage)
    {
      created_at: usage.try(:created_at)
    }
  end
end
