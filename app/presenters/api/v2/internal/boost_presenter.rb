module Api
  module V2
    module Internal
      class BoostPresenter < Api::V2::Presenter
        self.model_key = :boost

        ## #### Example
        ## ```js
        ## {
        ##   "boost": {
        ##     "url": "https://account.zendesk.com/api/v2/internal/boost.json",
        ##     "id": 432,
        ##     "active": true,
        ##     "expiry": "2011-05-05T10:38:52Z",
        ##     "boost_plan": "Large",
        ##     "activated_at": "2011-05-05T10:38:52Z",
        ##     "created_at": "2011-05-05T10:38:52Z",
        ##     "updated_at": "2011-05-05T10:38:52Z"
        ##   }
        ## }
        ## ```
        def model_json(boost)
          super.merge!(
            id:           boost.id,
            active:       boost.active,
            expires:      boost.expires,
            boost_plan:   boost.boost_plan,
            activated_at: boost.activated_at,
            created_at:   boost.created_at,
            updated_at:   boost.updated_at
          )
        end

        def url(boost)
          url_builder.send(:api_v2_internal_boost_url, boost, format: :json)
        end
      end
    end
  end
end
