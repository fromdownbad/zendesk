class Api::V2::Internal::AppLinkPresenter < Api::V2::Presenter
  self.model_key = :app_link

  def model_json(app_link)
    super.merge!(
      id: app_link.id,
      app_id: app_link.app_id,
      package_name: app_link.package_name,
      paths: app_link.paths,
      fingerprints: app_link.fingerprints
    )
  end

  def url(model)
    if singular_resource
      url_builder.send("api_v2_internal_#{model_key}_url", format: :json)
    else
      param = (url_param ? model.send(url_param) : model)
      url_builder.send("api_v2_internal_#{model_key}_url", param, format: :json)
    end
  end
end
