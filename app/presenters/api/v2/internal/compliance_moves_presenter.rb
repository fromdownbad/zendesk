class Api::V2::Internal::ComplianceMovesPresenter < Api::V2::Presenter
  self.model_key = :compliance_moves

  def model_json(account)
    presented_moves = []
    moves.each do |move|
      presented_move = {}

      move.attributes.each_pair do |key, val|
        presented_move[key] = val
      end

      account = move.account
      presented_move['account_subdomain'] = account.subdomain
      presented_move['account_name'] = account.name
      presented_move['plan_name'] = account.subscription.plan_name
      presented_moves << presented_move
    end
    presented_moves
  end

  private

  def moves
    @moves ||= ComplianceMove.includes(:account).joins(:account).where(move_scheduled: false).where('accounts.subdomain NOT LIKE "%z3n%"')
  end
end
