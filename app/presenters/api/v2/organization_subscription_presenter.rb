class Api::V2::OrganizationSubscriptionPresenter < Api::V2::Presenter
  self.model_key = :organization_subscription

  ## ### JSON Format
  ## Organization subscriptions are links between users and organizations they subscribe to
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | organization_id | integer | no        | yes       | The organization being subscribed to
  ## | user_id         | integer | no        | yes       | The user subscribed to the forum
  ## | created_at      | date    | yes       | no        | The time the subscription was created
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "url":             "https://company.zendesk.com/api/v2/organization_subscriptions/35436.json",
  ##   "organization_id":        32,
  ##   "user_id":         482,
  ##   "created_at":      "2009-07-20T22:55:29Z"
  ## }
  ## ```
  def model_json(subscription)
    super.merge!(
      id: subscription.id,
      organization_id: subscription.source_id,
      user_id: subscription.user_id,
      created_at: subscription.created_at
    )
  end

  def side_loads(subscriptions)
    json = {}

    subscriptions = Array(subscriptions)

    side_load_users(json, subscriptions) if side_load?(:users)
    side_load_organizations(json, subscriptions) if side_load?(:organizations)

    json
  end

  def side_load_users(json, subscriptions)
    json[:users] = side_load_association(subscriptions, :user, user_presenter)
  end

  def side_load_organizations(json, subscriptions)
    json[:organizations] = side_load_association(subscriptions, :source, organization_presenter)
  end
end
