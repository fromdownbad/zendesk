class Api::V2::MobileDevicePresenter < Api::V2::Presenter
  self.model_key = :mobile_device

  ## ### JSON Format
  ## Mobile device records represent a unit that has been registered with Zendesk for push notifications
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | url             | string  | yes       | no        | The API url of this record
  ## | device_type     | string  | no        | yes       | The type of the registered device
  ## | token           | string  | no        | yes       | The token that identifiers the device
  ## | mobile_app      | string  | no        | no        | The identifier of the mobile application that registered the device
  ## | created_at      | date    | yes       | no        | The time the record was created
  ## | updated_at      | date    | yes       | no        | The time of the last update of the record
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              39,
  ##   "url":             "https://company.zendesk.com/api/v2/mobile_devices/39.json",
  ##   "device_type:":    "iPhone",
  ##   "token":           "5D41402ABC4B2A76B9719D911017C592",
  ##   "mobile_app:":     "com.zendesk.agent",
  ##   "created_at":      "2012/03/05 10:38:52 +1000",
  ##   "updated_at":      "2012/03/05 10:38:52 +1000"
  ## }
  ## ```
  def model_json(mobile_device)
    super.merge!(
      id: mobile_device.id,
      device_type: mobile_device.device_type,
      token: mobile_device.token,
      mobile_app: mobile_device.mobile_app_identifier,
      created_at: mobile_device.created_at,
      updated_at: mobile_device.updated_at
    )
  end
end
