class Api::V2::SessionRecordPresenter < Api::V2::Presenter
  self.model_key = :session

  ## ### JSON Format
  ## Sessions are represented as simple flat JSON objects with the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | yes       | Automatically assigned when the session is created
  ## | url             | string  | yes       | no        | The API URL of this session
  ## | user_id         | string  | yes       | no        | The id of the user
  ## | authenticated_at| date    | yes       | no        | When the session was created
  ## | last_seen_at    | date    | yes       | no        | The last approximate time this session was seen. This does not update on every request.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                3432,
  ##   "url":               "https://company.zendesk.com/api/v2/users/12345/sessions/3432.json",
  ##   "user_id":           12345,
  ##   "authenticated_at": "2014-11-18T17:24:29Z",
  ##   "last_seen_at":     "2014-11-18T17:30:52Z"
  ## }
  ## ```
  def model_json(session)
    json = {
      id: session.id,
      user_id: session.user_id,
      authenticated_at: Time.at(session.data['auth_authenticated_at']),
      last_seen_at: Time.at(session.data['auth_updated_at'])
    }
    super.merge!(json)
  end

  def url(session)
    url_builder.send(:api_v2_user_session_url, session.user_id, session, format: :json)
  end
end
