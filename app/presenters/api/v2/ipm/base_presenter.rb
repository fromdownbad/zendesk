module Api::V2
  module Ipm
    class BasePresenter < Api::V2::Presenter
      def model_json(ipm)
        attributes = public_attributes(ipm)
        attributes.merge!(internal_attributes(ipm)) if options[:internal]
        attributes
      end

      def public_attributes(ipm)
        ipm.attributes.slice("id", "created_at", "updated_at")
      end

      def internal_attributes(ipm)
        ipm.attributes.slice("state", "start_date", "end_date", "test_mode").
          merge("status" => ipm.status).
          merge(constraint_attributes(ipm))
      end

      # Constraints are meta-programmed and aren't returned in #attributes
      def constraint_attributes(ipm)
        Hash[ipm.class::CONSTRAINTS.map { |attribute| [attribute.to_s, ipm.send(attribute)] }]
      end
    end
  end
end
