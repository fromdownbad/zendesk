module Api::V2
  module Ipm
    class AlertPresenter < BasePresenter
      self.model_key = :alert

      ## ### JSON Format
      ## Alerts have the following keys:
      ##
      ## | Name                | Type    | Read-only | Comment
      ## | ------------------- | ------- | --------- | -------
      ## | id                  | integer | yes       | Automatically assigned upon creation
      ## | text                | string  | yes       | The text for this alert
      ## | link_url            | string  | yes       | The external link URL to learn more about the alert
      ## | link_text           | string  | yes       | The external link text
      ## | created_at          | date    | yes       | The time the identity got created
      ## | updated_at          | date    | yes       | The time the identity got updated
      ##
      ## Internal requests additionally have:
      ##
      ## | Name                | Type    | Read-only | Comment
      ## | ------------------- | ------- | --------- | -------
      ## | state               | integer | yes       | Automatically assigned upon creation, see Zendesk::Ipm::Status
      ## | status              | string  | yes       | 'live', 'disabled', 'scheduled_active', 'scheduled_inactive'
      ## | start_date          | string  | yes       | For state = 2 (scheduled)
      ## | end_date            | string  | yes       | For state = 2 (scheduled)
      ## | internal_name       | string  | yes       | Internal name for this notification not visible to users. No HTML allowed.
      ## | pods                | array   | yes       | Constraint: pods shown this message
      ## | shards              | array   | yes       | Constraint: shards shown this message
      ## | roles               | string  | yes       | Constraint: e.g. "Owner", see ALLOWED_ROLES
      ## | languages           | array   | yes       | Constraint: language ids
      ## | account_types       | array   | yes       | Constraint: e.g. "trial", see ALLOWED_ACCOUNT_TYPES
      ## | interfaces          | array   | yes       | Constraint: "classic" or "lotus", see ALLOWED_INTERFACES
      ## | plans               | array   | yes       | Constraint: plan ids
      ## | account_ids         | array   | yes       | Constraint: account id for a subdomain
      ## | exclude_account_ids | array   | yes       | Constraint:
      ## | subdomains          | string  | yes       | Constraint: e.g. "support"
      ## | exclude_subdomains  | string  | yes       | Constraint:
      ## | custom_factors      | array   | yes       | Constraint: alert based on custom logic that's in the code.

      ## #### Example
      ## ```js
      ## {
      ##   "id": 2127301143,
      ##   "text": "New Alert",
      ##   "link_url": "http://www.zendesk.com/alert",
      ##   "link_text": "Show me more",
      ##   "updated_at": "2011/09/25 22:35:44 -0700",
      ##   "created_at": "2011/09/25 22:35:44 -0700"
      ## }
      ## ```
      ##
      ## Internal request:
      ## {
      ##   "id": 1430,
      ##   "text": "New alert",
      ##   "link_url": "http://www.zendesk.com/alert",
      ##   "link_text": "Show me more",
      ##   "updated_at": "2011/09/25 22:35:44 -0700",
      ##   "created_at": "2011/09/25 22:35:44 -0700"
      ##   "state": 1,
      ##   "status": "live",
      ##   "internal_name": "My Test",
      ##   "pods": [1, 2],
      ##   "shards": [100, 200],
      ##   "roles": ["Owner"],
      ##   "languages": [1],
      ##   "account_types": ["trial"],
      ##   "interfaces": ["lotus"],
      ##   "plans": [4],
      ##   "account_ids": [82171],
      ##   "exclude_account_ids": [1],
      ##   "subdomains": "dev",
      ##   "exclude_subdomains": "support",
      ##   "custom_factors": ["remote_auth_retired"]
      ## }
      ##

      def public_attributes(alert)
        super(alert).merge(alert.attributes.slice(
          "text",
          "link_url",
          "link_text"
        ))
      end
    end
  end
end
