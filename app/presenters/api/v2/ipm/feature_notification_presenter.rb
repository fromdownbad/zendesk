module Api::V2
  module Ipm
    class FeatureNotificationPresenter < BasePresenter
      self.model_key = :feature_notification

      ## ### JSON Format
      ## FeatureNotifications have the following keys:
      ##
      ## | Name                | Type    | Read-only | Comment
      ## | ------------------- | ------- | --------- | -------
      ## | id                  | integer | yes       | Automatically assigned upon creation
      ## | title               | string  | yes       | The title of this feature notification
      ## | body                | string  | yes       | The body for this feature notification
      ## | image_name          | string  | yes       | The image name
      ## | call_to_action_text | string  | yes       | The text on the call-to-action button
      ## | call_to_action_url  | string  | yes       | The URL of the call-to-action button
      ## | learn_more_url      | string  | yes       | The external link URL to learn more about the feature
      ## | created_at          | date    | yes       | The time the identity got created
      ## | updated_at          | date    | yes       | The time the identity got updated
      ##
      ##
      ## Internal requests additionally have:
      ##
      ## | Name                | Type    | Read-only | Comment
      ## | ------------------- | ------- | --------- | -------
      ## | state               | integer | yes       | Automatically assigned upon creation, see Zendesk::Ipm::Status
      ## | status              | string  | yes       | 'live', 'disabled', 'scheduled_active', 'scheduled_inactive'
      ## | start_date          | string  | yes       | For state = 2 (scheduled)
      ## | end_date            | string  | yes       | For state = 2 (scheduled)
      ## | internal_name       | string  | yes       | Internal name for this notification not visible to users. No HTML allowed.
      ## | pods                | array   | yes       | Constraint: pods shown this message
      ## | shards              | array   | yes       | Constraint: shards shown this message
      ## | roles               | string  | yes       | Constraint: e.g. "Owner", see ALLOWED_ROLES
      ## | languages           | array   | yes       | Constraint: language ids
      ## | account_types       | array   | yes       | Constraint: e.g. "trial", see ALLOWED_ACCOUNT_TYPES
      ## | interfaces          | array   | yes       | Constraint: "classic" or "lotus", see ALLOWED_INTERFACES
      ## | plans               | array   | yes       | Constraint: plan ids
      ## | account_ids         | array   | yes       | Constraint: account id for a subdomain
      ## | exclude_account_ids | array   | yes       | Constraint:
      ## | subdomains          | string  | yes       | Constraint: e.g. "support"
      ## | exclude_subdomains  | string  | yes       | Constraint:

      ## #### Example
      ## ```js
      ## {
      ##   "id": 2127301143,
      ##   "title": "New Feature!",
      ##   "body": "Brand new feature!",
      ##   "image_name": "blah.jpg",
      ##   "call_to_action_text": "Click me",
      ##   "call_to_action_url": "http://feature_notifications.io",
      ##   "learn_more_url": "http://www.zendesk.com/feature",
      ##   "updated_at": "2011/09/25 22:35:44 -0700",
      ##   "created_at": "2011/09/25 22:35:44 -0700"
      ## }
      ##
      ## Internal request:
      ## ```
      ## {
      ##   "id": 2127301143,
      ##   "title": "New Feature!",
      ##   "body": "Brand new feature!",
      ##   "image_name": "blah.jpg",
      ##   "call_to_action_text": "Click me",
      ##   "call_to_action_url": "http://feature_notifications.io",
      ##   "learn_more_url": "http://www.zendesk.com/feature",
      ##   "updated_at": "2011/09/25 22:35:44 -0700",
      ##   "created_at": "2011/09/25 22:35:44 -0700"
      ##   "state": 1,
      ##   "status": "live",
      ##   "internal_name": "My Test",
      ##   "pods": [1, 2],
      ##   "shards": [100, 200],
      ##   "roles": ["Owner"],
      ##   "languages": [1],
      ##   "account_types": ["trial"],
      ##   "interfaces": ["lotus"],
      ##   "plans": [4],
      ##   "account_ids": [82171],
      ##   "exclude_account_ids": [1],
      ##   "subdomains": "dev",
      ##   "exclude_subdomains": "support",
      ##   "custom_factors": ["remote_auth_retired"]

      def public_attributes(feature_notification)
        super(feature_notification).merge(feature_notification.attributes.slice(
          "title",
          "body",
          "image_name",
          "call_to_action_text",
          "call_to_action_url",
          "learn_more_url"
        ))
      end

      def internal_attributes(feature_notification)
        super(feature_notification).merge(feature_notification.attributes.slice("category_code"))
      end
    end
  end
end
