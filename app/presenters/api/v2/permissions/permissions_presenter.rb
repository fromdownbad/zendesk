class Api::V2::Permissions::PermissionsPresenter < Api::V2::Presenter
  self.model_key = :external_permissions

  attr_reader :external_permissions

  def model_json(user)
    allowed_scopes = if user.account.has_external_permissions_presentation?
      permissions(user).allowed_scopes(Access::ExternalPermissions::Permissions::ADMIN_PERMISSIONS)
    else
      []
    end

    {
      can_manage_ticket_fields: can_manage_ticket_fields?(user, allowed_scopes),
      can_manage_ticket_forms: can_manage_ticket_forms?(user, allowed_scopes),
      can_manage_contextual_workspaces: can_manage_contextual_workspaces?(user, allowed_scopes),
      can_manage_user_fields: can_manage_user_fields?(user, allowed_scopes),
      can_manage_organization_fields: can_manage_organization_fields?(user, allowed_scopes)
    }
  end

  def can_manage_ticket_fields?(user, allowed_scopes)
    allowed?(user, allowed_scopes, Access::ExternalPermissions::Permissions::TICKET_FIELDS_MANAGE, user.account.has_permissions_ticket_fields_manage?)
  end

  def can_manage_ticket_forms?(user, allowed_scopes)
    allowed?(user, allowed_scopes, Access::ExternalPermissions::Permissions::TICKET_FORMS_MANAGE, user.account.has_permissions_ticket_forms_manage?)
  end

  def can_manage_contextual_workspaces?(user, allowed_scopes)
    allowed?(user, allowed_scopes, Access::ExternalPermissions::Permissions::CONTEXTUAL_WORKSPACES_MANAGE, user.account.has_permissions_contextual_workspaces_manage?)
  end

  def can_manage_user_fields?(user, allowed_scopes)
    allowed?(user, allowed_scopes, Access::ExternalPermissions::Permissions::USER_FIELDS_MANAGE, user.account.has_permissions_user_fields_manage?)
  end

  def can_manage_organization_fields?(user, allowed_scopes)
    allowed?(user, allowed_scopes, Access::ExternalPermissions::Permissions::ORGANIZATION_FIELDS_MANAGE, user.account.has_permissions_organization_fields_manage?)
  end

  def allowed?(user, allowed_scopes, resource_scope, arturo)
    if user.account.has_external_permissions_presentation? && arturo
      allowed_scopes.include?(resource_scope)
    else
      user.is_admin?
    end
  end

  def permissions(user)
    Access::ExternalPermissions::Permissions.new(user)
  end
end
