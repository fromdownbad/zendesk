class Api::V2::TagsPresenter < Api::V2::Presenter
  self.model_key = :tags

  def model_json(model)
    if model.is_a?(Ticket)
      TagManagement.normalize_tags(model.current_tags, model.account)
    else
      model.tag_array
    end
  end

  def url(model)
    url_builder.send(model_name_for_api_tags_url(model), model.id, format: :json)
  end

  private

  def model_name_for_api_tags_url(model)
    case model
    when Entry
      "api_v2_topic_tags_url"
    when Ticket
      "api_v2_ticket_tags_url"
    when Organization
      "api_v2_organization_tags_url"
    when User
      "api_v2_user_tags_url"
    end
  end
end
