class Api::V2::TimeZonePresenter < Api::V2::Presenter
  self.model_key = :time_zone
  self.url_param = :name

  ## ### JSON Format
  ## Time Zones have the following attributes
  ##
  ## | Name                | Type      | Comment
  ## | ------------------- | --------- | -------
  ## | name                | string    | Timezone name in ActiveSupport
  ## | iana_name           | string    | Timezone name in IANA database
  ## | url                 | string    |
  ## | offset              | integer   | current timezone offset in minutes
  ## | formatted_offset    | string    | current timezone abbreviation including offset e.g. "GMT+2:00"
  ## | moment_packed       | string    | Timezone data for the moment.js library
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "name":              "Copenhagen",
  ##   "iana_name"          "Europe/Copenhagen",
  ##   "offset":            120,
  ##   "formatted_offset":  "GMT+2:00",
  ##   "moment_packed":     "Europe/Copenhagen|CET CEST|-10 -20|010...|1dAN0 1qM0 WM0..."
  ## }
  ## ```

  def model_json(time_zone)
    now = time_zone.now
    json = {
      translated_name: time_zone.translated_name,
      name: time_zone.name,
      iana_name: time_zone.tzinfo.name,
      url: url(time_zone),
      offset: now.utc_offset / 60,
      formatted_offset: "GMT#{now.formatted_offset}"
    }

    if options[:moment]
      json[:moment_packed] = time_zone.moment_packed
    end

    json
  end
end
