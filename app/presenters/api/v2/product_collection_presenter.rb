class Api::V2::ProductCollectionPresenter < Api::V2::CollectionPresenter
  def initialize(account, user, options)
    super(user, options)
    @account = account
  end

  def as_json(collection)
    {
      products: products(collection),
      features: features
    }
  end

  private

  def products(collection = [])
    collection.map do |product|
      if product.label.nil?
        { key: product.key }
      else
        { key: product.key, label: product.label }
      end
    end
  end

  def features
    {
      product_tray_enable_list_view: @account.spp? || Arturo.feature_enabled_for?(:product_tray_enable_list_view, @account)
    }
  end
end
