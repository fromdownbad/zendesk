class Api::V2::AppMarketIntegrationPresenter < Api::V2::Presenter
  self.model_key         = :app_market_integration
  self.singular_resource = true

  def model_json(model)
    {
      app_market_id: model.app_market_id,
      app_installed: model.app_installed?,
      app_name: model.app_name,
      app_path: model.app_path
    }
  end
end
