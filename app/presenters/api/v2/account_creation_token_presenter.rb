class Api::V2::AccountCreationTokenPresenter < Api::V2::Presenter
  self.model_key = :oauth_token

  def model_json(model)
    {
      id:       model.id,
      user_id:  model.user_id,
      token:    model.token(true)
    }
  end
end
