class Api::V2::SatisfactionRatingPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings

  self.model_key = :satisfaction_rating

  def initialize(user, options = {})
    super(user, options)

    @filters = options[:filters] || {}
  end

  ## ### JSON Format
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | url             | string  | yes       | no        | The API url of this rating
  ## | assignee_id     | integer | yes       | yes       | The id of agent assigned to at the time of rating
  ## | group_id        | integer | yes       | yes       | The id of group assigned to at the time of rating
  ## | requester_id    | integer | yes       | yes       | The id of ticket requester submitting the rating
  ## | ticket_id       | integer | yes       | yes       | The id of ticket being rated
  ## | score           | string  | no        | yes       | The rating: "offered", "unoffered", "good" or "bad"
  ## | created_at      | date    | yes       | no        | The time the satisfaction rating got created
  ## | updated_at      | date    | yes       | no        | The time the satisfaction rating got updated
  ## | comment         | string  | no        | no        | The comment received with this rating, if available
  ## | reason          | string  | no        | no        | The reason for a bad rating given by the requester in a follow-up question. Satisfaction reasons must be [enabled](https://support.zendesk.com/hc/en-us/articles/223152967)
  ## | reason_code     | integer | no        | no        | The default reasons the user can select from a list menu for giving a negative rating. See [Reason codes](./satisfaction_reasons#reason-codes) in the Satisfaction Reasons API. Can only be set on ratings with a `score` of "bad".
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "url":             "https://company.zendesk.com/api/v2/satisfaction_ratings/62.json",
  ##   "assignee_id":     135,
  ##   "group_id":        44,
  ##   "requester_id":    7881,
  ##   "ticket_id":       208,
  ##   "score":           "good",
  ##   "updated_at":      "2011-07-20T22:55:29Z",
  ##   "created_at":      "2011-07-20T22:55:29Z"
  ## }
  ## ```
  def model_json(satisfaction_rating)
    ticket_id = satisfaction_rating.ticket_id

    satisfaction_rating.ticket = @tickets[ticket_id] if @tickets
    comment = @comments[satisfaction_rating.comment_event_id].try(:value) if @comments
    comment ||= satisfaction_rating.comment.try(:value)

    super.tap do |json|
      json.merge!(
        id:           satisfaction_rating.id,
        assignee_id:  satisfaction_rating.agent_user_id,
        group_id:     satisfaction_rating.agent_group_id,
        requester_id: satisfaction_rating.enduser_id,
        ticket_id:    satisfaction_rating.ticket.try(:nice_id),
        score:        score_attribute(satisfaction_rating.score),
        created_at:   satisfaction_rating.created_at,
        updated_at:   satisfaction_rating.updated_at,
        comment:      comment
      )

      json.merge!(reason(satisfaction_rating)) if account.csat_reason_code_enabled?
    end
  end

  # We preload tickets and comments here, because of archiving.
  # This prevents n+1 queries to MySQL and Riak.
  def present(satisfaction_ratings)
    # Need to handle cases where satisfaction_ratings is an Array,
    # an ActiveRecord::Relation or a single instance.
    list = satisfaction_ratings.respond_to?(:map) ? satisfaction_ratings : [satisfaction_ratings]
    return super if list.empty?

    comment_ids = list.map(&:comment_event_id).compact
    @comments = Hash[Comment.where(id: comment_ids).map { |c| [c.id, c] }]

    # Without manually preloading tickets, we call association_accesses_archive
    # from Satisfaction::Rating, which is defined in
    # https://github.com/zendesk/zendesk_archive/blob/master/lib/zendesk_archive/association_lookaside.rb
    # and would trigger calls to Riak for each individual ticket instead of one call
    # for the whole collection.
    # Ideally we would need to generalize this use case in the gem itself.
    ticket_ids = list.map(&:ticket_id).compact
    @tickets = Hash[Ticket.where(id: ticket_ids).all_with_archived.map { |t| [t.id, t] }]

    super
  end

  def association_preloads
    { event: {} }
  end

  def side_loads(satisfaction_ratings)
    {}.tap do |json|
      json[:users] = side_load_users(satisfaction_ratings) if side_load?(:users)
      json[:statistics] = side_load_statistics(satisfaction_ratings) if side_load?(:statistics)
    end
  end

  def present_for_ticket(ticket)
    return nil unless account.has_customer_satisfaction_enabled?

    {score: score_attribute(ticket.satisfaction_score)}.tap do |json|
      if satisfaction_score_present?(ticket)
        json.merge!(
          id:        ticket.satisfaction_rating.id,
          comment:   satisfaction_comment(ticket),
          reason:    account_reasons[ticket.satisfaction_rating.reason_code].try(:translated_value),
          reason_id: account_reasons[ticket.satisfaction_rating.reason_code].try(:id)
        )
      end
    end
  end

  protected

  def satisfaction_score_present?(ticket)
    ticket.satisfaction_score.to_i > SatisfactionType.OFFERED &&
      ticket.satisfaction_rating.present?
  end

  def satisfaction_comment(ticket)
    if account.has_satisfaction_rating_disable_comment_fallback?
      ticket.satisfaction_rating.comment.try(:value)
    else
      ticket.satisfaction_comment
    end
  end

  def side_load_users(satisfaction_ratings)
    satisfaction_ratings = Array(satisfaction_ratings)

    user_ids = satisfaction_ratings.map do |satisfaction_rating|
      [satisfaction_rating.enduser_id, satisfaction_rating.agent_user_id]
    end

    user_ids.flatten!
    user_ids.compact!
    user_ids.uniq!

    users = account.all_users.where(id: user_ids).to_a
    user_presenter.preload_associations(users)

    user_presenter.collection_presenter.model_json(users)
  end

  def side_load_statistics(_satisfaction_rating)
    statistics_presenter = Api::V2::SatisfactionRatingStatisticsPresenter.new(@filters)

    statistics_presenter.model_json(account)
  end

  def reason(satisfaction_rating)
    return {} unless satisfaction_rating.reason.present?
    {
      reason: satisfaction_rating.reason.translated_value,
      reason_id: satisfaction_rating.reason.id
    }
  end

  def account_reasons
    @account_reasons ||= ::Satisfaction::Reason.with_deleted do
      ::Satisfaction::Reason.where(account_id: account.id).each_with_object({}) do |reason, reasons|
        reasons[reason.reason_code] = reason
      end
    end
  end

  def score_attribute(satisfaction_score)
    ticket_attribute_value(
      :satisfaction_score,
      satisfaction_score.to_s,
      account
    )
  end
end
