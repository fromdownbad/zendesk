class Api::V2::OrganizationMembershipPresenter < Api::V2::Presenter
  self.model_key = :organization_membership

  ## ### JSON Format
  ## Organization memberships have the following properties:
  ##
  ## | Name                  | Type           | Read-only | Mandatory | Comment
  ## | --------------------- | -------------- | --------- | --------- | --------------------------
  ## | id                    | integer        | yes       | no        | Automatically assigned when the membership is created
  ## | url                   | string         | yes       | no        | The API url of this membership
  ## | user_id               | integer        | yes       | yes       | The ID of the user for whom this memberships belongs
  ## | organization_id       | integer        | yes       | yes       | The ID of the organization associated with this user, in this membership
  ## | default               | boolean        | no        | yes       | Denotes whether this is the default organization membership for the user. If false, returns `null`
  ## | created_at            | date           | yes       | no        | When this record was created
  ## | updated_at            | date           | yes       | no        | When this record last got updated
  ##
  def model_json(organization_membership)
    super.merge!(
      id: organization_membership.id,
      user_id: organization_membership.user_id,
      organization_id: organization_membership.organization_id,
      default: organization_membership.default,
      created_at: organization_membership.created_at,
      updated_at: organization_membership.updated_at
    )
  end

  def url(organization_membership)
    url_builder.send(:api_v2_organization_membership_url, organization_membership, format: :json)
  end

  def side_loads(organization_membership)
    json = {}
    organization_memberships = Array(organization_membership)

    if side_load?(:users)
      users = account.all_users.
        where(id: organization_memberships.map(&:user_id).uniq).
        includes(:identities)

      json[:users] = users.map { |user| user_presenter.condensed_model_json(user) }
    end

    if side_load?(:organizations)
      json[:organizations] = side_load_association(organization_memberships, :organization, organization_presenter)
    end

    json
  end

  private

  def organization_presenter
    @organization_presenter ||= Api::V2::Organizations::EndUserPresenter.new(user, options)
  end
end
