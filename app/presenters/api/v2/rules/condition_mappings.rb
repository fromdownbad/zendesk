module Api::V2::Rules::ConditionMappings
  def self.default_operator(definition)
    definition[:operator] ||= "is"
  end

  def self.wrap_value(definition)
    definition[:value] = Array.wrap(definition[:value])
  end

  def self.wrap_value_and_add_default_operator(_account, definition)
    wrap_value(definition)
    default_operator(definition)
  end

  def self.boolean_to_string(account, definition)
    definition[:value] = definition[:value].to_s
    wrap_value_and_add_default_operator(account, definition)
  end

  def self.upcase_date_definition(account, definition)
    definition[:field].upcase!
    date_definition(account, definition)
  end

  # The rule system expects certain date definitions in hours, but we'd like to be able to pass
  # dates into the v2 API. This method tries to convert anything non-integer into a datetime.
  def self.date_definition(_account, definition)
    return definition if definition[:field] == 'exact_created_at'
    return definition if definition[:field] =~ /custom_fields\./ # changing custom field date values to hours will not validate
    date_string = definition[:value].to_s
    return definition if date_string =~ /\A\d+\z/

    parsed_date = DateTime.parse(date_string)

    # Flip the operator because the rule system also flips it when converting from hours to a date.
    # E.g. less_than 40 (hours) is equivalent to greater_than 40.hours.ago
    # See lib/zendesk/rules/rule_query_builder.rb #add_duration_condition
    definition[:operator] =
      case definition[:operator]
      when /greater_than(_business_hours)?/
        "less_than#{$1}"
      when /less_than(_business_hours)?/
        "greater_than#{$1}"
      else
        definition[:operator]
      end

    # Definition in hours
    definition[:value] = ((Time.now - parsed_date) / 3600).to_i
  rescue ArgumentError
    # It's all good, just passing through
  end

  def self.nice_id(_account, definition)
    definition[:field] = "nice_id"
  end

  # Maps custom_fields_111 -> ticket_fields_111
  # Maps custom field option values to ids when ticket field is a FieldTagger
  def self.map_custom_field_options(account, definition)
    id = @match[1].to_i

    definition[:field] = "ticket_fields_#{id}"
    value = definition[:value].to_s
    ticket_field = account.fetch_active_tagger(id)

    if ticket_field
      option = if account.has_special_chars_in_custom_field_options?
        ticket_field.custom_field_options.find_by_enhanced_value(value)
      else
        ticket_field.custom_field_options.find_by_value(value)
      end
      definition[:value] = option.id.to_s if option
    end
  end

  # Maps 'follower' -> 'cc'.
  # We conditionally present `cc` macros with the field `follower` in the
  # definition object depending on an account's settings/features. This reverse
  # maps the value back to `cc`.
  #
  # If we start persisting macros with the field `follower` this mapping would
  # need to be removed.
  def self.follower_to_cc(account, definition)
    if account.has_follower_and_email_cc_collaborations_enabled?
      definition[:field] = 'cc'
    end
  end

  def self.sanitize_tags(account, definition)
    return unless account.has_sanitize_rule_tags?
    definition[:value] = TagManagement.normalize_tags(definition[:value]).join(' ')
  end

  MAPPINGS = ActiveSupport::OrderedHash.new.tap do |mappings|
    mappings[/\A(NEW|OPEN|PENDING|HOLD|SOLVED)\z/i] = method(:upcase_date_definition)
    mappings[/\A(?:ticket|custom)_fields_(\d+)\z/] = method(:map_custom_field_options)
    mappings[/\Acomment_mode_is_public\z/] = method(:boolean_to_string)
    mappings[/_at\z/] = method(:date_definition)
    mappings[/\Aid\z/] = method(:nice_id)
    mappings[/\Afollower\z/] = method(:follower_to_cc)
    mappings[/tags\z/] = method(:sanitize_tags)
    mappings[/.*/] = method(:wrap_value_and_add_default_operator)
  end

  def self.map_definition_item!(account, item)
    MAPPINGS.each do |mapping, function|
      next unless (@match = mapping.match(item[:field]))
      function.call(account, item)
    end

    item
  end
end
