class Api::V2::Rules::ConditionDiffsPresenter < Api::V2::Rules::ConditionsPresenter
  ## ### Condition Diffs
  ## *Condition diffs* are represented as a JSON object with two
  ## arrays of one or more condition diff objects.
  ##
  ## **Example**
  ##
  ## ```js
  ## {
  ##   "conditions": {
  ##     "any": [{"field": [ ... ], "operator": [ ... ], "value": [ ... ]}],
  ##     "all": [{"field": [ ... ], "operator": [ ... ], "value": [ ... ]}]
  ##   }
  ## }
  ## ```
  ##
  ## Each condition diff has the following properties:
  ##
  ## | Name     | Type  | Description
  ## | -------- | ----- | -----------
  ## | field    | array | An array of [change](#changes) objects
  ## | operator | array | An array of [change](#changes) objects
  ## | value    | array | An array of [change](#changes) objects
  def attribute_name(condition)
    [{change: condition.change, content: super(condition.content)}]
  end

  def attribute_operator(condition)
    [{change: condition.change, content: condition.content.operator}]
  end

  def attribute_value(condition)
    [{change: condition.change, content: super(condition.content)}]
  end
end
