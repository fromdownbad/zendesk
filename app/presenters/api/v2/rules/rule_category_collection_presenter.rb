class Api::V2::Rules::RuleCategoryCollectionPresenter < Api::V2::CursorCollectionPresenter
  NO_RULES = {active_count: 0, inactive_count: 0}.freeze

  def model_json(collection)
    if side_load?(:rule_counts)
      category_ids = collection.map(&:id)
      rule_counts = load_rule_counts(category_ids)

      super.each do |rule|
        counts = rule_counts[rule[:id].to_i] || NO_RULES

        rule.merge!(counts)
      end
    else
      super
    end
  end

  private

  def load_rule_counts(category_ids)
    Rule.
      where(account_id: account.id, type: options[:rule_type], rules_category_id: category_ids).
      group_by(&:rules_category_id).transform_values! do |rules_by_category_id|
        active_rules, inactive_rules = rules_by_category_id.partition(&:is_active)

        {active_count: active_rules.count, inactive_count: inactive_rules.count}
      end
  end
end
