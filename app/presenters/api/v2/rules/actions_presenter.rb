class Api::V2::Rules::ActionsPresenter < Api::V2::Presenter
  self.model_key = :actions

  ## ### Actions
  ## *Actions* consist of an array of one or more actions.
  ##
  ## **Example**
  ##
  ## ```js
  ## {
  ##   "actions": [
  ##     {"field": "status", "value": "open"},
  ##     {"field": "assignee_id", "value": "296220096"}
  ##   ]
  ## }
  ## ```
  ##
  ## Each action in the array has the following properties:
  ##
  ## | Name     | Type     | Description
  ## | -------- | -------- | -------------------
  ## | field    | string   | The name of a ticket field to modify
  ## | value    | string   | The new value of the field
  ##
  ## **Example action**
  ##
  ## ```js
  ## { "field": "status", "value": "solved" }
  ## ```
  ##
  ## #### Actions reference
  ##
  ## Triggers, automations, and macros share the following actions.
  ##
  ## | field                | value
  ## | -------------------- | ---------------------------------------------------------------
  ## | `status`             | Sets the ticket status. Takes `new`, `open`, `pending`, `hold`, `solved`, or `closed`, except for macros, which don't take `new` or `closed`.
  ## | `type`               | Sets the ticket type. Takes `question`, `incident`, `problem`, or `task`.
  ## | `priority`           | Sets the ticket priority. Takes `low`, `normal`, `high`, or `urgent`.
  ## | `group_id`           | Assigns the ticket to a group. Takes a string with a group id, or an empty string (`""`) to unassign the group assigned to the ticket.
  ## | `assignee_id`        | Assigns the ticket to a person. Takes a string with the user id of an assignee or requester, or `"current_user"`, or an empty string (`""`) to unassign the person assigned to the ticket.
  ## | `set_tags`           | A space-delimited list of tags to insert in the ticket. The action replaces the current tags.
  ## | `current_tags`       | A space-delimited list of tags to add to existing tags.
  ## | `remove_tags`        | A space-delimited list of tags to remove from existing tags.
  ## | `custom_fields_<id>` | Sets the value of a custom ticket field.
  ##
  ## #### Additional actions for triggers and automations
  ##
  ## In addition to the shared actions, triggers and automations share the following actions.
  ##
  ## | field                 | value
  ## | --------------------- | -------------------------------------------------
  ## | `satisfaction_score`  | Sends a survey request to the ticket requester. Takes `offered` as a value.
  ## | `notification_user`   | Sends an email to a user. Takes an array of three strings specifying the email recipient, subject, and body. See "Notification emails" below. Possible recipient value: `current_user`, `all_agents` (all non-restricted agents), `requester_id` (the current requester), `assignee_id` (the current assignee), or the numeric ID of an agent.
  ## | `notification_group`  | Sends an email to a group. Takes an array of three strings specifying the email recipient, subject, and body. See "Notification emails" below. Possible recipient value: `group_id` (the currently assigned group), or the numeric ID of a group.
  ## | `notification_target` | Sends a message to an external target. Takes an array of two strings specifying the numeric ID of the target and the message body.
  ## | `tweet_requester`     | Responds to the twitter requester with a tweet. Takes the text of the tweet.
  ## | `cc`                  | CC's somebody on the ticket. Takes `current_user` or the numeric ID of an agent.
  ## | `locale_id`           | Sets the requester's language to one of your supported languages. Takes the numeric ID of a supported locale. See [List locales](./locales) to list the available locale IDs for the account.
  ## | `requester.custom_fields.<field_key>` | Sets the value of a custom user field. The corresponding `"value"` property can be any string for a text field, or the id of an option for a dropdown field. An option id must be specified as a string. For a field's key or option id values, see [Show User Field](./user_fields#show-user-field) in User Fields
  ##
  ## #### Additional actions for macros
  ##
  ## In addition to the shared actions, macros have the following actions.
  ##
  ## | field                    | value
  ## | ------------------------ | -------------------------------------------------
  ## | `subject`                | Replaces the subject of a ticket. Takes the subject text.
  ## | `comment_value`          | Adds a comment to a ticket. Takes the comment text *or* an array of two strings specifying the comment channel and comment text. Possible comment channels : 'channel:all', 'channel:web' and 'channel:chat'
  ## | `comment_value_html`     | Adds a rich-text comment to a ticket.
  ## | `comment_mode_is_public` | Makes a ticket comment public or private. Takes `true` (public) or `false` (private).
  ##
  ## #### Notification emails
  ##
  ## Notification emails are represented by an array of three strings specifying the email recipient, subject, and body.
  ##
  ## **Example**
  ##
  ## ```js
  ## ["293741756", "Leaking radiator", "Open the steam valve."]
  ## ```
  ##
  ## The array is used for the `value` property of email notification actions. See "Additional actions for triggers and automations" above.
  ##
  ## **Example action**
  ##
  ## ```js
  ## {
  ##   "actions": [
  ##     {"field": "notification_user", "value": ["293741756", "Leaking radiator", "Open the steam valve."]}
  ##   ]
  ## }
  ## ```
  ##
  ## You can use dynamic content placeholders in the email subject and body. See [Zendesk Support data object (placeholders) reference](https://support.zendesk.com/entries/20203943).
  ##
  ## You can also use return (`\r`) and newline (`\n`) characters in the message body.
  ##
  ## **Example**
  ##
  ## ```js
  ## ["current_user","{{ticket.id}}: Leaking radiator","Open the steam valve.\r \nHope this helps."]
  ## ```

  def model_json(definition)
    definition.actions.map do |action|
      {
        field: attribute_name(action),
        value: attribute_value(action)
      }
    end
  end

  private

  def attribute_name(action)
    Api::V2::Tickets::AttributeMappings.
      ticket_attribute_name(action.sanitized_source, account)
  end

  def attribute_value(action)
    Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
      action.source,
      Array(action.value).size > 1 ? action.value : action.first_value,
      account
    ) || ''
  end
end
