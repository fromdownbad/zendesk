class Api::V2::Rules::TriggerDefinitionsPresenter < Api::V2::Rules::RuleDefinitionsPresenter
  self.model_key = :definitions

  def model_json(definitions)
    {
      **actions(definitions),
      **conditions_all(definitions),
      **conditions_any(definitions)
    }
  end
end
