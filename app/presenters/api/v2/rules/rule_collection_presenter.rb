class Api::V2::Rules::RuleCollectionPresenter < Api::V2::CollectionPresenter
  def model_json(collection)
    preload_apps_installations(collection) if side_load?(:app_installation)

    preload_execution_counts(collection, :hourly)  if side_load?(:usage_1h)
    preload_execution_counts(collection, :daily)   if side_load?(:usage_24h)
    preload_execution_counts(collection, :weekly)  if side_load?(:usage_7d)
    preload_execution_counts(collection, :monthly) if side_load?(:usage_30d)

    super
  end

  def as_json(collection)
    super.tap do |json|
      json[:categories] = collection.categories if side_load?(:categories)
    end
  end

  private

  def side_load?(side_load)
    super && options.key?(:side_loads) && options[:side_loads].include?(side_load)
  end

  def preload_apps_installations(collection)
    installations = Rule.apps_installations(account, collection.map(&:id))

    collection.each do |rule|
      rule.app_installation = installations.fetch(rule.id) {
        Zendesk::Rules::NullAppInstallation
      }
    end
  end

  def preload_execution_counts(collection, timeframe)
    execution_counter.rules_with_execution_counts(timeframe, collection)
  end

  def execution_counter
    @execution_counter ||= begin
      Zendesk::RuleSelection::ExecutionCounter.new(
        account: account,
        type:    item_presenter.rule_type
      )
    end
  end
end
