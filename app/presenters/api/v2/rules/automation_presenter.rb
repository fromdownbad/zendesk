class Api::V2::Rules::AutomationPresenter < Api::V2::Rules::RulePresenter
  self.model_key = :automation

  SIDE_LOADS = %i[
    app_installation
    permissions
    usage_1h
    usage_24h
    usage_7d
    usage_30d
  ].freeze

  ## ### JSON Format
  ## Automations are represented as simple flat JSON objects which have the following keys.
  ##
  ## | Name       | Type                      | Comment
  ## | ---------- | ------------------------- | -------------------
  ## | id         | integer                   | Automatically assigned when created
  ## | title      | string                    | The title of the automation
  ## | active     | boolean                   | Whether the automation is active
  ## | position   | integer                   | Position of the automation, determines the order they will execute in
  ## | conditions | [Conditions](#conditions) | An object that describes the conditions under which the automation will execute
  ## | actions    | [Actions](#actions)       | An object describing what the automation will do
  ## | created_at | date                      | The time the automation was created
  ## | updated_at | date                      | The time of the last update of the automation
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "automation": {
  ##     "id": 25,
  ##     "title": "Notify requester of comment update",
  ##     "active": true,
  ##     "actions": { ... },
  ##     "conditions": { ... },
  ##     "updated_at": "2012-09-25T22:50:26Z",
  ##     "created_at": "2012-09-25T22:50:26Z"
  ##   }
  ## }
  ## ```
  def model_json(automation)
    super.merge!(
      actions:    actions_presenter.model_json(automation.definition),
      conditions: conditions_presenter.model_json(automation.definition),
      position:   automation.position
    ).tap do |json|
      json.merge!(app_installation_json(automation))
      json.merge!(title_json(automation))
      json.merge!(usage_json(automation))
    end
  end

  private

  def actions_presenter
    @actions_presenter ||= Api::V2::Rules::ActionsPresenter.new(user, options)
  end

  def conditions_presenter
    @conditions_presenter ||= begin
      Api::V2::Rules::ConditionsPresenter.new(user, options)
    end
  end

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::RuleCollectionPresenter.new(
        user,
        options.merge(
          includes:       includes,
          item_presenter: self,
          side_loads:     SIDE_LOADS
        )
      )
    end
  end

  def app_installation_json(automation)
    return {} unless side_load?(:app_installation)

    {app_installation: automation.app_installation}
  end

  def title_json(automation)
    {
      title:     render_dynamic_content(automation.title),
      raw_title: automation.title
    }.tap do |json|
      json.merge!(highlight_json(automation)) unless dynamic_content?(json)
    end
  end

  def highlight_json(automation)
    return {} unless highlight_automation?(automation)

    {highlights: options[:highlights][automation.id]}
  end

  def highlight_automation?(automation)
    options[:highlights].present? && options[:highlights].key?(automation.id)
  end

  def usage_json(automation)
    {}.tap do |json|
      json[:usage_1h]  = automation.usage.hourly  if side_load?(:usage_1h)
      json[:usage_24h] = automation.usage.daily   if side_load?(:usage_24h)
      json[:usage_7d]  = automation.usage.weekly  if side_load?(:usage_7d)
      json[:usage_30d] = automation.usage.monthly if side_load?(:usage_30d)
    end
  end

  def dynamic_content?(json)
    json[:title] != json[:raw_title]
  end
end
