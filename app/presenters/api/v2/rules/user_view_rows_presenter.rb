class Api::V2::Rules::UserViewRowsPresenter < Api::V2::Presenter
  include Api::V2::Rules::UserViewColumnsHelper
  include Api::V2::Rules::UserViewRowsHelper
  include Api::V2::Rules::AssociationHelper

  self.model_key = :row

  ## ### User View Rows
  ##
  ## User View Rows are represented as simple flat JSON objects with the following keys.
  ##
  ## | Name            | Type                        | Comment
  ## | --------------- | --------------------------- | -------------------
  ## | user_view       | Array                       | User view that was executed. Consists of id and url.
  ## | rows            | Array                       | Array of users/organizations contained in the view, described by the fields.
  ## | columns         | Array                       | Array of [Fields](#execution) representing the columns in each row.
  ##
  ## A row contains the data indicated by the identifiers in the columns array.
  ##

  def present(users)
    comment = "user_rule:#{view.to_param}, user:#{user.to_param}, shard:#{view.account.shard_id}, subsystem:#{options[:subsystem_name]}"
    ActiveRecord::Comments.comment(comment) { super }
  end

  def model_json(row_user)
    super.merge!(user_fields_json(row_user))
  end

  def side_loads(users)
    json = { columns: columns_json(view.output.columns) }

    json[:user_view] = view_json if side_load?(:user_view)
    users = Array.wrap(users)
    if orgs = organization_side_loads(users)
      json[:organizations] = orgs
    end

    if view.account.has_user_views_sampling_enabled?
      sampling_rate = UserView::SAMPLING_THRESHOLD.to_f / view.account.settings.user_count
      json[:estimated_unsampled_count] = (users.total_entries / sampling_rate).to_i
      json[:sample] = true
    end

    json
  end

  def association_preloads
    preloads = {
      photo: { thumbnails: {} },
      identities: {}
    }

    if view.output.columns.include?("google")
      preloads[:identities] = { google_profile: {} }
    end

    preloads.merge!(custom_field_preloads)

    if view.output.columns.include?("organization_id")
      preloads[:organizations] = {}
    end

    if view.output.columns.include?("current_tags")
      preloads[:taggings] = {}
    end
    preloads
  end

  private

  def organization_side_loads(users)
    if (view.has_grouping? && view.output.group[:id] == 'organization_id') || view.output.columns.include?('organization_id')
      organization_ids = users.map(&:organization_id).uniq.compact
      organization_ids = users.flat_map(&:organization_ids).uniq.compact if account.has_multiple_organizations_enabled?
      organizations = view.account.organizations.find(organization_ids)
      association_json(organizations)
    end
  end

  def custom_field_preloads
    if view.output.columns.grep(/^custom_fields\./).present? ||
        (view.output.group && view.output.group[:id].match(/^custom_fields\./)) ||
        (view.output.sort && view.output.sort[:id].match(/^custom_fields\./))
      {
        custom_fields: { dropdown_choices: {} },
        custom_field_values: {}
      }
    else
      {}
    end
  end

  def user_fields_json(row_user)
    columns = view.output.columns + ['id']
    columns << view.output.group[:id] if view.has_grouping?
    columns = columns.map(&:to_s)

    user_data = {
      user: {
        name: row_user.name,
        email: row_user.email,
        photo: photo(row_user)
      }
    }

    if columns.include?('organization_id') && account.has_multiple_organizations_enabled?
      user_data["organization_ids"] = row_user.organizations.map(&:id)
    end

    custom_fields_json(row_user, columns).merge(user_data)
  end

  def photo(user)
    return nil unless user.photo.present?

    attachment_presenter.model_json(user.photo)
  end

  def custom_fields_json(row_user, columns)
    columns.each_with_object({}) do |column, json|
      custom_field_column = UserView.custom_field_key(column)
      json[column]        = custom_field_value(custom_field_column, row_user) if custom_field_column
      json[column]      ||= field_value(column, row_user)
    end
  end

  def view
    options[:user_view]
  end

  def view_json
    return {} if view.new_record?
    {
      id: view.id,
      url: url_builder.send(:api_v2_user_view_url, view, format: :json)
    }
  end

  def url(row_user)
    url_builder.send(:api_v2_user_url, row_user, format: :json)
  end
end
