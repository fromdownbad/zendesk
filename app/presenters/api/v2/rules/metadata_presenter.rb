class Api::V2::Rules::MetadataPresenter < Api::V2::Presenter
  self.model_key = :metadata

  def present(aspect)
    return {} unless supported_metadata?(aspect)

    super
  end

  def model_json(aspect)
    aspect.metadata.each_with_object({}) do |metadata, json|
      json[metadata] = definition_metadata.public_send(metadata, aspect.subject)
    end
  end

  private

  def definition_metadata
    @definition_metadata ||=
      Zendesk::Rules::DefinitionMetadata.new(account, user)
  end

  def supported_metadata?(aspect)
    aspect.metadata.all? { |key| definition_metadata.respond_to?(key) }
  end
end
