class Api::V2::Rules::ViewPresenter < Api::V2::Rules::RulePresenter
  self.model_key = :view

  SIDE_LOADS = %i[app_installation permissions].freeze

  ## ### JSON Format
  ## Views are represented as JSON objects with the following properties.
  ##
  ## | Name        | Type                      | Read-only | Comment
  ## | ----------- | ------------------------- | --------- | -------
  ## | id          | integer                   | yes       | Automatically assigned when created
  ## | title       | string                    | no        | The title of the view
  ## | active      | boolean                   | no        | Whether the view is active
  ## | restriction | object                    | no        | Who may access this account. Is null when everyone in the account can access it
  ## | position    | integer                   | no        | The position of the view
  ## | execution   | object                    | no        | Describes how the view should be executed. See [Execution](#execution)
  ## | conditions  | object                    | no        | Describes how the view is constructed. See [Conditions](#conditions)
  ## | description | string                    | no        | The description of the view
  ## | created_at  | date                      | yes       | The time the view was created
  ## | updated_at  | date                      | yes       | The time the view was last updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "view": {
  ##     "id": 25,
  ##     "title": "Tickets updated <12 Hours",
  ##     "description": "View for recent tickets",
  ##     "active": true,
  ##     "position": 8,
  ##     "execution": { ... },
  ##     "conditions": [ ... ],
  ##     "restriction": {
  ##       "type": "User",
  ##       "id": 4
  ##     }
  ##   }
  ## }
  ## ```
  def model_json(view)
    super.merge!(
      position:    view.position,
      description: view.description,
      execution:   output_presenter.model_json(view.output),
      conditions:  conditions_presenter.model_json(view.definition),
      restriction: restriction_json(view),
      watchable:   view.watchable?
    ).tap do |json|
      json.merge!(app_installation_json(view))
      json.merge!(title_json(view))
    end
  end

  def url(view)
    super unless view.new_record?
  end

  private

  def conditions_presenter
    @conditions_presenter ||= begin
      Api::V2::Rules::ConditionsPresenter.new(user, options)
    end
  end

  def output_presenter
    @output_presenter ||= Api::V2::Rules::OutputPresenter.new(user, options)
  end

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::RuleCollectionPresenter.new(
        user,
        options.merge(
          includes:       includes,
          item_presenter: self,
          side_loads:     SIDE_LOADS
        )
      )
    end
  end

  def restriction_json(view)
    case view.owner
    when Group
      {type: 'Group'}.tap do |restriction|
        if view.account.has_multiple_group_views_reads?
          restriction[:id]  = view.group_ids.first
          restriction[:ids] = view.group_ids
        else
          restriction[:id] = view.owner_id
        end
      end
    when User
      {type: 'User', id: view.owner_id}
    end
  end

  def app_installation_json(view)
    return {} unless side_load?(:app_installation)

    {app_installation: view.app_installation}
  end

  def title_json(view)
    {
      title:     render_dynamic_content(view.title),
      raw_title: view.title
    }.tap do |json|
      json.merge!(highlight_json(view)) unless dynamic_content?(json)
    end
  end

  def highlight_json(view)
    return {} unless highlight_view?(view)

    {highlights: options[:highlights][view.id]}
  end

  def highlight_view?(view)
    options[:highlights].present? && options[:highlights].key?(view.id)
  end

  def dynamic_content?(json)
    json[:title] != json[:raw_title]
  end
end
