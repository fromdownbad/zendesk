module Api::V2::Rules::UserViewColumnsHelper
  def columns_json(columns)
    columns.map { |field| column_json(field) }
  end

  def column_json(column)
    field = options[:user_view].field_from_source(column)
    base_column_json = { id: column, title: column_title(column, field) }
    base_column_json.update(custom_field_json(column, field)) if field
    base_column_json
  end

  private

  def column_title(column, field)
    if field
      if field.is_system?
        I18n.t(field.title)
      else
        Zendesk::Liquid::DcContext.render(field.title, account, user, 'text/plain', nil, user.dc_cache)
      end
    elsif !(column =~ /^custom_fields\..*/)
      I18n.t("txt.api.v2.user_views.columns.#{column}")
    else
      I18n.t("txt.api.v2.user_views.columns.deleted_field")
    end
  end

  def custom_field_json(_column, field)
    {
      type: field.type.downcase,
      url: url_builder.send(:api_v2_user_field_url, field, format: :json)
    }
  end
end
