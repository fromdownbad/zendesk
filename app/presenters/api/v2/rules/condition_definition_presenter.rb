class Api::V2::Rules::ConditionDefinitionPresenter < Api::V2::Presenter
  self.model_key = :condition

  def model_json(condition)
    {
      title:      condition.title,
      subject:    condition.subject,
      type:       condition.type,
      group:      condition.group,
      nullable:   condition.nullable?,
      repeatable: condition.repeatable?,
      operators:  condition.operators.map(&method(:present_operator))
    }.tap do |json|
      unless condition.values.empty?
        json[:values] = condition.values.map(&method(:present_value))
      end

      if condition.metadata.present?
        json.merge!(metadata_presenter.present(condition))
      end
    end
  end

  private

  def present_operator(operator)
    {
      value:    operator.value,
      title:    operator.title,
      terminal: operator.terminal?
    }.tap do |json|
      json[:format] = operator.format if operator.format.present?
    end
  end

  def present_value(list_value)
    {
      value:   list_value.value,
      title:   list_value.title,
      enabled: list_value.enabled?
    }
  end

  def metadata_presenter
    options[:metadata_presenter]
  end
end
