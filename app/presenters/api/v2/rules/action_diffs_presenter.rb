class Api::V2::Rules::ActionDiffsPresenter < Api::V2::Rules::ActionsPresenter
  ## ### Action Diffs
  ## *Action diffs* are represented by an array of one or more diff action objects.
  ##
  ## **Example**
  ##
  ## ```js
  ## {
  ##   "actions": [
  ##     {"field": [ ... ], "value": [ ... ]},
  ##     {"field": [ ... ], "value": [ ... ]}
  ##   ]
  ## }
  ## ```
  ##
  ## Each action diff has the following properties:
  ##
  ## | Name     | Type  | Description
  ## | -------- | ----- | -----------
  ## | field    | array | An array of [change](#changes) objects
  ## | value    | array | An array of [change](#changes) objects
  ##
  def attribute_name(action)
    [{change: action.change, content: super(action.content)}]
  end

  def attribute_value(action)
    [{change: action.change, content: super(action.content)}]
  end
end
