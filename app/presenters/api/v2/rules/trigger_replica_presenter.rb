class Api::V2::Rules::TriggerReplicaPresenter < Api::V2::Rules::TriggerPresenter
  HIDDEN_ATTRIBUTES = %i[active created_at id updated_at url].freeze

  private_constant :HIDDEN_ATTRIBUTES

  self.model_key = :trigger

  # ### JSON Format
  # Trigger replicas are represented as simple flat JSON objects with the following keys:
  #
  # | Name        | Type                      | Comment
  # | ----------- | ------------------------- | -------
  # | title       | string                    | The title of the trigger
  # | raw_title   | string                    | The title of the trigger before rendering dynamic content
  # | description | string                    | The description of the trigger
  # | conditions  | [Conditions](#conditions) | An object that describes the conditions under which the trigger will execute
  # | actions     | [Actions](#actions)       | An object describing what the trigger will do
  # | position    | integer                   | Position of the trigger, determines the order they will execute in
  #
  # #### Example
  # ```js
  # {
  #   "trigger": {
  #     "title": "Notify requester of comment update",
  #     "description": "Notifies requester that a comment was updated.",
  #     "actions": { ... },
  #     "conditions": { ... },
  #     "position": 10
  #   }
  # }
  # ```
  def model_json(*)
    super.tap do |json|
      HIDDEN_ATTRIBUTES.each { |attribute| json.delete(attribute) }
    end
  end

  def url(*); end
end
