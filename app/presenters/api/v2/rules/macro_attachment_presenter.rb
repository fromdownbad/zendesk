class Api::V2::Rules::MacroAttachmentPresenter < Api::V2::Presenter
  self.model_key = :macro_attachment
  self.url_param = :id

  # h3 JSON Format
  #
  # Macro attachments are represented as simple flat JSON objects which have the
  # following keys.
  #
  # | Name         | Type    | Comment
  # | ------------ | ------- | ----------------------------------- |
  # | id           | integer | Automatically assigned when created |
  # | filename     | string  | Filename of the attachment          |
  # | content_type | string  | Content type of the attachment      |
  # | size         | integer | The size of the attachment in bytes |
  # | created_at   | date    | The time the attachment was created |
  #
  # h4 Example
  #
  # ```js
  # {
  #   "macro_attachment": {
  #     "id": 100,
  #     "filename": "screenshot.jpg",
  #     "content_type": "image/jpeg",
  #     "size": 2532,
  #     "created_at": "2016-08-15T16:04:06Z"
  #   }
  # }
  # ```
  def model_json(macro_attachment)
    super.merge!(
      id:           macro_attachment.id,
      filename:     macro_attachment.filename,
      content_type: macro_attachment.content_type,
      content_url:  content_url(macro_attachment),
      size:         macro_attachment.size,
      created_at:   macro_attachment.created_at
    )
  end

  private

  def content_url(macro_attachment)
    url_builder.content_api_v2_macro_attachment_url(id: macro_attachment.id)
  end
end
