class Api::V2::Rules::MacroDefinitionsPresenter < Api::V2::Presenter
  self.model_key = :definitions

  include Api::V2::Tickets::AttributeMappings

  def model_json(definitions)
    {
      actions: action_definition_presenter.
        collection_presenter.model_json(definitions.actions)
    }
  end

  private

  def action_definition_presenter
    @action_definition_presenter ||= begin
      Api::V2::Rules::ActionDefinitionPresenter.new(
        user,
        url_builder: self
      )
    end
  end
end
