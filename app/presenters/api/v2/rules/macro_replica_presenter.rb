class Api::V2::Rules::MacroReplicaPresenter < Api::V2::Rules::MacroPresenter
  HIDDEN_ATTRIBUTES = %i[active created_at id updated_at url].freeze

  private_constant :HIDDEN_ATTRIBUTES

  self.model_key = :macro

  ## ### JSON Format
  ## Macro replicas are represented as simple flat JSON objects with the following keys:
  ##
  ## | Name        | Type                | Comment
  ## | ----------- | ------------------- | -------
  ## | title       | string              | The title of the macro
  ## | raw_title   | string              | The title of the macro before rendering dynamic content
  ## | description | string              | The description of the macro
  ## | restriction | object              | Who may access this macro. Will be null when everyone in the account can access it
  ## | actions     | [Actions](#actions) | An object describing what the macro will do
  ## | position    | integer             | The position of the macro                                                          |
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "macro": {
  ##     "title": "Take it!",
  ##     "description: "Assigns the ticket to the current user and group.",
  ##     "actions": { ... },
  ##     "restriction": null,
  ##     "position": 4
  ##   }
  ## }
  ## ```
  def model_json(*)
    super.tap do |json|
      HIDDEN_ATTRIBUTES.each { |attribute| json.delete(attribute) }

      json[:actions].each do |action|
        action[:value] = action[:value].to_s if action[:value].is_a?(Integer)
      end
    end
  end

  def url(*); end
end
