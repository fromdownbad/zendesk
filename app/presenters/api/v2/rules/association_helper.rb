module Api::V2::Rules::AssociationHelper
  def association_json(collection)
    collection.map do |resource|
      {
        id: resource.id,
        name: resource.name,
        url: url_builder.send("api_v2_#{resource.class.name.downcase}_url", resource, format: :json)
      }
    end
  end
end
