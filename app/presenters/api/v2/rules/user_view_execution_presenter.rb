class Api::V2::Rules::UserViewExecutionPresenter < Api::V2::Presenter
  include Api::V2::Rules::UserViewColumnsHelper

  self.model_key = :execution

  def model_json(execution)
    return {} unless execution.present?
    {
      group: clause_json(execution.group),
      sort: clause_json(execution.sort),
      columns: columns_json(execution.columns)
    }
  end

  private

  def clause_json(clause)
    if clause
      if field = User.columns_hash[clause[:id]]
        { id: clause[:id], title: clause[:id].to_s.titleize, order: clause[:order] }
      elsif field_key = UserView.custom_field_key(clause[:id])
        field = account.user_custom_fields.find_by_key(field_key)
        field && { id: clause[:id], title: field.title, order: clause[:order] }
      end
    end
  end
end
