require 'api/presentation'

class Api::V2::Rules::FilteredViewCollectionPresenter < Api::V2::CollectionPresenter
  def total(*)
    # Given that the number of results isn't known,
    # count and pagination information should not be included.
    nil
  end
end
