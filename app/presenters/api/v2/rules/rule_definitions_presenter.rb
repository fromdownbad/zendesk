class Api::V2::Rules::RuleDefinitionsPresenter < Api::V2::Presenter
  self.model_key = :definitions

  include Api::V2::Tickets::AttributeMappings

  def model_json(definitions)
    {
      **actions(definitions),
      **conditions_all(definitions),
      **conditions_any(definitions)
    }
  end

  private

  def actions(definitions)
    {
      actions: action_definition_presenter.
        collection_presenter.
        model_json(definitions.actions)
    }
  end

  def conditions_all(definitions)
    {
      conditions_all: condition_definition_presenter.
        collection_presenter.
        model_json(definitions.conditions_all)
    }
  end

  def conditions_any(definitions)
    {
      conditions_any: condition_definition_presenter.
        collection_presenter.
        model_json(definitions.conditions_any)
    }
  end

  def action_definition_presenter
    @action_definition_presenter ||= begin
      Api::V2::Rules::ActionDefinitionPresenter.new(
        user,
        url_builder: self,
        metadata_presenter: Api::V2::Rules::MetadataPresenter.new(
          user,
          url_builder: self
        )
      )
    end
  end

  def condition_definition_presenter
    @condition_definition_presenter ||= begin
      Api::V2::Rules::ConditionDefinitionPresenter.new(
        user,
        url_builder: self,
        metadata_presenter: Api::V2::Rules::MetadataPresenter.new(
          user,
          url_builder: self
        )
      )
    end
  end
end
