class Api::V2::Rules::TriggerPresenter < Api::V2::Rules::RulePresenter
  self.model_key = :trigger

  SIDE_LOADS = %i[
    app_installation
    permissions
    usage_1h
    usage_24h
    usage_7d
    usage_30d
  ].freeze

  ## ### JSON Format
  ## Triggers are represented as simple flat JSON objects which have the following keys.
  ##
  ## | Name        | Type                      | Comment
  ## | ----------- | ------------------------- | -------
  ## | id          | integer                   | Automatically assigned when created
  ## | title       | string                    | The title of the trigger
  ## | active      | boolean                   | Whether the trigger is active
  ## | position    | integer                   | Position of the trigger, determines the order they will execute in
  ## | conditions  | [Conditions](#conditions) | An object that describes the conditions under which the trigger will execute
  ## | actions     | array                     | An array of [Actions](#actions) describing what the trigger will do
  ## | description | string                    | The description of the trigger
  ## | created_at  | date                      | The time the trigger was created
  ## | updated_at  | date                      | The time of the last update of the trigger
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "trigger": {
  ##     "id": 25,
  ##     "title": "Notify requester of comment update",
  ##     "active": true,
  ##     "actions": [ ... ],
  ##     "conditions": { ... },
  ##     "description": "Notifies requester that a comment was updated",
  ##     "updated_at": "2012-09-25T22:50:26Z",
  ##     "created_at": "2012-09-25T22:50:26Z"
  ##   }
  ## }
  ## ```
  def model_json(trigger)
    super.merge!(
      actions:     actions_presenter.model_json(trigger.definition),
      conditions:  conditions_presenter.model_json(trigger.definition),
      description: trigger.description,
      position:    trigger.position
    ).tap do |json|
      json.merge!(app_installation_json(trigger))
      json.merge!(title_json(trigger))
      json.merge!(usage_json(trigger))

      if trigger.account.has_trigger_categories_api_enabled?
        json.merge!(category_id: trigger.rules_category_id.to_s)
      end
    end
  end

  private

  def actions_presenter
    @actions_presenter ||= Api::V2::Rules::ActionsPresenter.new(user, options)
  end

  def conditions_presenter
    @conditions_presenter ||= begin
      Api::V2::Rules::ConditionsPresenter.new(user, options)
    end
  end

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::RuleCollectionPresenter.new(
        user,
        options.merge(
          includes:       includes,
          item_presenter: self,
          side_loads:     SIDE_LOADS
        )
      )
    end
  end

  def app_installation_json(trigger)
    return {} unless side_load?(:app_installation)

    {app_installation: trigger.app_installation}
  end

  def title_json(trigger)
    {
      title:     render_dynamic_content(trigger.title),
      raw_title: trigger.title
    }.tap do |json|
      json.merge!(highlight_json(trigger)) unless dynamic_content?(json)
    end
  end

  def highlight_json(trigger)
    return {} unless highlight_trigger?(trigger)

    {highlights: options[:highlights][trigger.id]}
  end

  def highlight_trigger?(trigger)
    options[:highlights].present? && options[:highlights].key?(trigger.id)
  end

  def usage_json(trigger)
    {}.tap do |json|
      json[:usage_1h]  = trigger.usage.hourly  if side_load?(:usage_1h)
      json[:usage_24h] = trigger.usage.daily   if side_load?(:usage_24h)
      json[:usage_7d]  = trigger.usage.weekly  if side_load?(:usage_7d)
      json[:usage_30d] = trigger.usage.monthly if side_load?(:usage_30d)
    end
  end

  def dynamic_content?(json)
    json[:title] != json[:raw_title]
  end
end
