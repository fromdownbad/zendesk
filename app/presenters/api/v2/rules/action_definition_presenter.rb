class Api::V2::Rules::ActionDefinitionPresenter < Api::V2::Presenter
  self.model_key = :action

  def model_json(action)
    {
      title:      action.title,
      type:       action.type,
      subject:    action.subject,
      group:      action.group,
      nullable:   action.nullable?,
      repeatable: action.repeatable?
    }.tap do |json|
      unless action.values.empty?
        json[:values] = action.values.map(&method(:present_value))
      end

      if action.metadata.present?
        json.merge!(metadata_presenter.present(action))
      end
    end
  end

  private

  def present_value(list_value)
    {
      value:   list_value.value,
      title:   list_value.title,
      enabled: list_value.enabled?
    }.tap do |json|
      json[:format] = list_value.format if list_value.format.present?
    end
  end

  def metadata_presenter
    options[:metadata_presenter]
  end
end
