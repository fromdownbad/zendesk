require 'zendesk_rules/custom_rule_field'

class Api::V2::Rules::OutputPresenter < Api::V2::Presenter
  include Api::V2::Rules::RuleFieldHelper

  self.model_key = :execution

  ## ### Execution
  ## A view's `execution` object is a read-only object that describes how to display a collection of tickets in the view.
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | group_by, sort_by       | string  | An item from the [View columns](#view-columns) table
  ## | group_order, sort_order | string  | Either "asc" or "desc"
  ## | columns                 | array   | The ticket fields to display. Custom fields have an id, title, type, and url referencing the [ticket field](ticket_fields.html)
  ## | group                   | object  | When present, the structure indicating how the tickets are grouped
  ## | sort                    | object  | The column structure of the field used for sorting
  ##
  ## #### Example
  ## ```js
  ## {
  ##    "execution": {
  ##      "columns": [
  ##        { "id": "status",  "title": "Status" },
  ##        { "id": "updated", "title": "Updated" },
  ##        {
  ##          "id": 5, "title": "Account", "type": "text",
  ##          "url": "https://example.zendesk.com/api/v2/ticket_fields/5.json"
  ##        },
  ##        ...
  ##      ]
  ##      "group": { "id": "status", "title": "Status", "order": "desc" },
  ##      "sort": { "id": "updated", "title": "Updated", "order": "desc" }
  ##    }
  ## }
  ## ```
  def model_json(output)
    {
      group_by:    output.group,       # Deprecated
      group_order: output.group_order, # Deprecated
      sort_by:     output.order,       # Deprecated
      sort_order:  output.sort_order,  # Deprecated
      group:       group_json(output),
      sort:        sort_json(output),
      columns:     columns_json(output)
    }.merge!(fields_json(output.columns))
  end

  protected

  def fields_json(field_ids)
    fields = ZendeskRules::RuleField.lookup(field_ids)
    fields.each_with_object(fields: [], custom_fields: []) do |field, json|
      if field.is_a?(ZendeskRules::CustomRuleField)
        json[:custom_fields].push(custom_field_json(field.ticket_field))
      else
        json[:fields].push(system_field_json(field))
      end
    end
  end

  def group_json(output)
    field = ZendeskRules::RuleField.lookup(output.group)

    if field
      rule_field_json(field).merge!(order: output.group_order)
    elsif output.group
      { id: output.group, order: output.group_order }
    end
  end

  def sort_json(output)
    field = ZendeskRules::RuleField.lookup(output.order)

    if field
      rule_field_json(field).merge!(order: output.sort_order)
    elsif output.order
      { id: output.order, order: output.sort_order }
    end
  end
end
