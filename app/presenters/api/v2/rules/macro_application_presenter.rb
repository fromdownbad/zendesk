class Api::V2::Rules::MacroApplicationPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings

  self.model_key = :result

  ## ### JSON Format
  ##
  ## Macros are read-only and represented as JSON objects which have the following keys:
  ##
  ## | Name            | Type    | Comment
  ## | --------------- | ------- | -------
  ## | ticket          | Object  | Contains entire ticket with fields that have changed if presenting for a specific ticket, otherwise, just the changed fields.
  ## | comment         | Object  | Changed values for ticket comment.
  ##
  ## #### Example
  ##
  ## When applying macro to specific ticket:
  ##
  ## ```js
  ## {
  ##   "result": {
  ##     "ticket": {
  ##       "id":            35436,
  ##       "url":           "https://company.zendesk.com/api/v2/tickets/35436.json",
  ##       "assignee_id":   235323,
  ##       "group_id":      98738,
  ##       "custom_fields": [
  ##         {
  ##           "id":    27642,
  ##           "value": "745"
  ##         }
  ##       ],
  ##       "comment": {
  ##         "body":      "Assigned to **Agent Uno** for review.",
  ##         "html_body": "<p>Assigned to <strong>Agent Uno</strong> for review.</p>",
  ##         "public":    false
  ##       }
  ##     }
  ##   }
  ## }
  ## ```
  ##
  ## For bulk application:
  ##
  ## ```js
  ## {
  ##   "result": {
  ##     "ticket": {
  ##       "assignee_id": 235323,
  ##       "group_id":    98738
  ##     }
  ##   }
  ## }
  ## ```
  def model_json(macro_application)
    result = macro_application.result.with_indifferent_access

    json = {}

    result[:tags] = result.delete(:current_tags) if result[:current_tags]

    comment = result.delete(:comment)

    # Needs to happen before merge so that ticket_fields_*
    # keys get removed.
    fields = fields_json(result)

    # Be careful changing keys as this if statement will populate the resulting
    # json from two different sources.
    #
    # In the case a ticket is present the `ticket` key will be initially
    # populated from the tickets presenter. When no ticket is present the keys
    # are populated from the result of the macro application.
    if ticket
      ticket.attributes = mass_assignable_attributes(result)
      json[:ticket] = ticket_presenter.model_json(ticket)

      json[:ticket][:custom_fields] ||= []
      json[:ticket][:custom_fields] = merge_custom_fields(json[:ticket][:custom_fields], fields) if fields.any?

      # TODO: deprecated
      json[:ticket][:fields] = json[:ticket][:custom_fields]
    else
      result[:custom_fields] = fields if fields.any?

      if result[:tags]
        result[:tags] = TagManagement.normalize_tags(result[:tags], account)
      end

      map_ticket_attributes!(result)

      # TODO: deprecated
      result[:fields] = result[:custom_fields]

      json[:ticket] = result
      json
    end

    update_collaborator_ids!(result, json)
    update_follower_ids!(result, json)
    fixup_keys!(json)

    json[:ticket][:comment] = comment_json(comment) if comment.present?

    if macro_application.upload_token.present?
      json[:ticket].merge!(
        upload_presenter.present(macro_application.upload_token)
      )
    end

    json
  end

  private

  EXCLUDE = %w[followers_list collaborator_list].group_by(&:itself) # lookup

  def mass_assignable_attributes(input)
    input.reject { |k, _v| EXCLUDE[k.to_s] }
  end

  def update_collaborator_ids!(result, json)
    result[:collaborator_list] ||= []
    json[:ticket][:collaborator_ids] ||= []
    json[:ticket][:collaborator_ids] += result[:collaborator_list].map { |c| c[:id] unless c[:cced] }.compact
  end

  def update_follower_ids!(result, json)
    result[:followers_list]      ||= []
    json[:ticket][:follower_ids] ||= []

    json[:ticket][:follower_ids] += result[:followers_list].reduce([]) do |list, follower|
      follower[:following] ? list : list << follower[:id]
    end
  end

  def fixup_keys!(json)
    # json should not include `collaborator_list` or `followers_list`. These
    # keys are used in the `MacroApplication` class. In the case of presenting a
    # macro application with no ticket we pre-populate the result with the
    # result of `MacroApplication#run`.
    json[:ticket].delete(:collaborator_list)
    json[:ticket].delete(:followers_list)
  end

  def ticket
    @ticket ||= options[:ticket]
  end

  def comment_json(comment)
    applied_comment(comment).tap do |json|
      json[:public] = comment[:is_public] if comment.key?(:is_public)
    end
  end

  def applied_comment(comment)
    if comment.key?(:html_value) && comment.key?(:value)
      hybrid_comment(comment)
    elsif comment.key?(:html_value)
      html_comment(comment)
    elsif comment.key?(:value)
      basic_comment(comment)
    else
      {}
    end
  end

  def hybrid_comment(comment)
    {body: comment[:value], html_body: comment[:html_value]}
  end

  def html_comment(comment)
    {body: comment[:html_value], html_body: comment[:html_value]}
  end

  def basic_comment(comment)
    {
      body:        comment[:value],
      scoped_body: comment[:scoped_value],
      html_body:   ZendeskText::Markdown.html(comment[:value])
    }
  end

  # This method removes all ticket fields out of the main
  # macro result, where they are stored under the root key in the format
  # { "ticket_fields_{id}" => {value}, "ticket_fields_{id2} => {value2}, ... },
  # and returns them in the format [{ :id => .., :value => ... }, { :id => ... }, ...]
  def fields_json(result)
    result.
      select { |key, _| key.start_with?('ticket_fields_') }.
      map do |name, _|
        {
          id:    name.sub('ticket_fields_', '').to_i,
          value: result.delete(name)
        }
      end
  end

  # current: [{:id=>1, :value=>"from ticket"}, {:id=>2, :value=>"from ticket"}]
  # applied_by_macro: [{:id=>2, :value=>"from macro"}]
  #
  # result: [{:id=>1, :value=>"from ticket"}, {:id=>2, :value=>"from macro"}]
  def merge_custom_fields(current, applied_by_macro)
    # after union and group_by we'll have arrays with only old values if macro didn't change then:
    #   - [{:id=>1, :value=>"from ticket"}]
    # and arrays with old AND new value if macro changed them:
    #   - [{:id=>2, :value=>"from ticket"}, {:id=>2, :value=>"from macro"}]
    (current | applied_by_macro).group_by { |h| h[:id] }.map do |_, values|
      # here we prefer new value from macro if there's one or keep the current value in the ticket
      values.second || values.first
    end
  end

  def upload_presenter
    @upload_presenter ||= Api::V2::UploadPresenter.new(user, options)
  end
end
