class Api::V2::Rules::RulePresenter < Api::V2::Presenter
  alias rule_type model_key

  def model_json(rule)
    super.merge!(
      id:         rule.id,
      title:      rule.title,
      active:     rule.is_active?,
      updated_at: rule.updated_at,
      created_at: rule.created_at
    ).tap do |json|
      json[:permissions] = permissions_json(rule) if side_load?(:permissions)
      json[:valid] = rule.valid? if side_load?(:valid)
    end
  end

  private

  def permissions_json(rule)
    {can_edit: user.can?(:edit, rule)}
  end
end
