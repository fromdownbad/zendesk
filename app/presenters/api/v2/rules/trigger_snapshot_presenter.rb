class Api::V2::Rules::TriggerSnapshotPresenter < Api::V2::Presenter
  self.model_key = :snapshot

  ## ### Snapshot
  ##
  ## | Name        | Type                      | Description
  ## | ----------- | ------------------------- | -----------
  ## | title       | string                    | The title of the trigger
  ## | active      | boolean                   | Whether the trigger is active
  ## | conditions  | [Conditions](#conditions) | An object that describes the conditions under which the trigger will execute
  ## | actions     | array                     | An array of [Actions](#actions) describing what the trigger will do
  ## | description | string                    | The description of the trigger
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "title": "Notify requester of comment update",
  ##   "active": true,
  ##   "conditions": { ... },
  ##   "actions": [ ... ],
  ##   "description": "Notifies requester that a comment was updated"
  ## }
  ## ```
  def model_json(snapshot)
    {
      title:       snapshot.title,
      active:      snapshot.is_active?,
      conditions:  conditions_presenter.model_json(snapshot.definition),
      actions:     actions_presenter.model_json(snapshot.definition),
      description: snapshot.description
    }
  end

  private

  def actions_presenter
    @actions_presenter ||= Api::V2::Rules::ActionsPresenter.new(user, options)
  end

  def conditions_presenter
    @conditions_presenter ||=
      Api::V2::Rules::ConditionsPresenter.new(user, options)
  end
end
