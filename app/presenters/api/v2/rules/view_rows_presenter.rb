require 'zendesk_rules/custom_rule_field'

class Api::V2::Rules::ViewRowsPresenter < Api::V2::Rules::RulePresenter
  include Api::V2::Rules::RuleFieldHelper
  include Api::V2::Rules::AssociationHelper
  include Api::V2::Tickets::AttributeMappings
  include Api::V2::Tickets::EventViaPresenter

  self.model_key = :row

  ## ### View Rows
  ##
  ## View Rows are read-only and represented as simple flat JSON objects which have the following keys.
  ##
  ## | Name            | Type                        | Comment
  ## | --------------- | --------------------------- | -------------------
  ## | view            | Array                       | View that was executed. Consists of id and url.
  ## | rows            | Array                       | Array of tickets contained in the view, described by the fields.
  ## | columns         | Array                       | Array of [Fields](#execution) and [Custom Fields](#execution) representing the columns in each row.
  ##
  ## A row contains the data indicated by the idenitifiers in the columns array.
  ##
  ## | Name                   | Type                         | Optional  | Comment
  ## | ---------------------- | ---------------------------- | --------- | ----------------
  ## | ticket                 | Object                       | no        | Ticket id, url, subject, description, status, type, priority, brand_id and comment this row is a subset of.
  ## | custom_fields          | Array                        | no        | Custom fields values.
  ## | group                  | Integer                      | yes       | Id of this ticket's group.
  ## | organization           | Integer                      | yes       | Id of this ticket's organization.
  ## | requester              | Integer                      | yes       | Id of this ticket's requester.
  ## | assignee               | Integer                      | yes       | Id of this ticket's assignee.
  ## | submitter              | Integer                      | yes       | Id of this ticket's submitter.
  ## | locale                 | String                       | yes       | Locale of the requester.
  ## | type                   | String                       | yes       | See [Ticket](tickets.md#json-format)
  ## | priority               | String                       | yes       | See [Ticket](tickets.md#json-format)
  ## | status                 | String                       | yes       | See [Ticket](tickets.md#json-format)
  ## | brand_id               | Integer                      | yes       | See [Ticket](tickets.md#json-format)
  ## | updated_by_type        | String                       | yes       | Last updated by 'agent' or 'end user'
  ## | subject                | DateTime                     | yes       | Ticket subject.
  ## | requester_updated_at   | DateTime                     | yes       | When the requester last updated the ticket.
  ## | assignee_updated_at    | DateTime                     | yes       | When the assignee last updated the ticket.
  ## | assigned               | DateTime                     | yes       | When the ticket was assigned last.
  ## | due_at                 | DateTime                     | yes       | When the ticket is due.
  ## | solved                 | DateTime                     | yes       | When the ticket was solved.
  ## | created                | DateTime                     | yes       | When the ticket was created.
  ## | updated                | DateTime                     | yes       | When the ticket was updated.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "view": {
  ##     "id": 5,
  ##     "url": "https://example.zendesk.com/api/v2/views/5.json"
  ##   },
  ##   "rows": [
  ##     {
  ##       "ticket": { ... },
  ##       "locale": "en-US",
  ##       "group": { ... },
  ##       ...
  ##     },
  ##     ...
  ##   ],
  ##   "columns": [
  ##     {
  ##       "id": "locale",
  ##       "title": "Locale"
  ##     },
  ##     {
  ##       "id": 5,
  ##       "title": "Account",
  ##       "url": ...
  ##     },
  ##     ...
  ##   ]
  ## }
  ## ```
  def model_json(ticket)
    ticket_fields_json(ticket).merge!(ticket: ticket_json(ticket))
  end

  def present(collection)
    super(collection).tap do |hash|
      if hash.present?
        if view.account.has_hanlon_cache_info_api?
          hash['cached'] = view.cached_by_hanlon
        end
        hash[:count] = nil if hash[:count] == ::View::BROKEN_COUNT
      end
    end
  end

  def side_loads(tickets)
    tickets = Array(tickets)

    json = { columns: columns_json(filtered_view_output) }
    json[:view] = view_json if side_load?(:view)

    if side_load?(:group)
      tickets.pre_load(:group)
      groups = tickets.map(&:group).uniq.compact
      json[:groups] = association_json(groups)
    end

    if side_load?(:organization)
      tickets.pre_load(:organization)
      organizations = tickets.map(&:organization).uniq.compact
      json[:organizations] = association_json(organizations)
    end

    user_ids = [:requester, :assignee, :submitter].inject([]) do |ids, user_field|
      ids |= tickets.map { |ticket| ticket.send("#{user_field}_id") } if side_load?(user_field)
      ids
    end

    user_ids |= if side_load?(:last_comments)
      tickets.flat_map { |ticket| last_comments(ticket).map(&:author_id) }
    else
      tickets.each_with_object([]) do |ticket, ids|
        if (comment = comment(ticket))
          ids << comment.author_id
        end
      end
    end

    if user_ids.any?
      users = account.users.where(id: user_ids).to_a
      json[:users] = association_json(users)
    end

    json
  end

  def url(ticket)
    url_builder.send(:api_v2_ticket_url, ticket, format: :json)
  end

  def last_comments(ticket)
    if Arturo.feature_enabled_for?(:agent_as_end_user, ticket.account) && !user.can?(:view_private_content, ticket)
      # In principle, we could find the latest public comments.  But that would be a performance hit,
      # and it's not clear it's necessary- this user can't see this ticket in Support anyway.
      []
    else
      ticket.comments.reverse.take(2)
    end
  end

  def comment_count(ticket)
    ticket.comments.size
  end

  def comment(ticket)
    if Arturo.feature_enabled_for?(:agent_as_end_user, ticket.account) && !user.can?(:view_private_content, ticket)
      # In principle, we could find the latest public comment.  But that would be a performance hit,
      # and it's not clear it's necessary- this user can't see this ticket in Support anyway.
      nil
    else
      ticket.latest_comment_mapped || ticket.latest_comment
    end
  end

  def chat_message(ticket)
    ticket.latest_chat_message_mapped.try do |message|
      {
        body: message.body.strip,
        created_at: message.created_at,
        author_id: message.author_id,
        public: message.public?
      }
    end
  end

  protected

  def side_load?(id)
    fields.any? { |field| field.identifier == id } || super
  end

  def view
    options[:view]
  end

  def filtered_view_output
    return view.output if account.has_groups?
    view.output.clone.tap { |o| o.columns = o.columns.reject { |c| c == :group } }
  end

  def view_json
    json = if view.new_record?
      {}
    else
      {
        id: view.id,
        url: url_builder.send(:api_v2_view_url, view, format: :json)
      }
    end

    if side_load?(:execution)
      json[:execution] = output_presenter.model_json(view.output).slice(:sort, :group)
    end

    json[:conditions] = conditions_presenter.model_json(view.definition) if side_load?(:conditions)

    json
  end

  def fields
    @fields ||= begin
      columns = filtered_view_output.columns
      columns |= [view.output.group] if view.output.group
      ZendeskRules::RuleField.lookup(columns)
    end
  end

  def comment_presenter
    @comment_presenter ||= options[:comment_presenter] ||= Api::V2::Requests::CommentPresenter.new(user, options)
  end

  def sla_ticket_metric_presenter
    @sla_ticket_metric_presenter ||= begin
      Api::V2::Slas::TicketMetricPresenter.new(user, options)
    end
  end

  def ticket_json(ticket)
    json = {
      id: ticket.nice_id,
      subject: render_dynamic_content(ticket.title, ticket: ticket),
      description: render_dynamic_content(ticket.description, ticket: ticket),
      status: STATUS_MAP[ticket.status_id.to_s],
      type: TYPE_MAP[ticket.ticket_type_id.to_s],
      priority: PRIORITY_MAP[ticket.priority_id.to_s],
      url: url(ticket)
    }

    if account.has_multibrand?
      json[:brand_id] = ticket.brand_id
    end

    if ticket.account.has_mobile_views_execute_date_sideloads? && side_load?(:dates)
      json[:created_at] = ticket.created_at
      json[:updated_at] = ticket.updated_at
    end

    if side_load?(:last_comments)
      last_comments = last_comments(ticket)
      json[:last_comments] = comment_presenter.collection_presenter.model_json(last_comments)
      json[:comment_count] = comment_count(ticket)
    elsif (comment = comment(ticket))
      comment.url_builder = options[:shared_tickets_url_builder]
      json[:last_comment] = {
        id: comment.id,
        body: comment.plain_body.to_s.strip,
        created_at: comment.created_at,
        author_id: comment.author_id,
        public: comment.is_public?
      }
    end

    if side_load?(:latest_chat_message)
      json[:last_chat_message] = chat_message(ticket)
    end

    if side_load?(:via_id)
      json[:via_id] = ticket.via_id
    end

    if account.has_service_level_agreements? && side_load?(:sla_next_breach_at)
      ZendeskAPM.trace(
        'view_rows_presenter.ticket.sla.metric',
        service: Zendesk::Sla::APM_SERVICE_NAME
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.set_tag('zendesk.account_id', account.id)
        span.set_tag('zendesk.account_subdomain', account.subdomain)
        span.set_tag('zendesk.ticket.id', ticket.id)

        json[:sla_policy_metric] = sla_ticket_metric_presenter.model_json(ticket.sla.metric)
      end
    end

    json
  end

  def ticket_fields_json(ticket)
    fields_json = { custom_fields: [] }
    custom_fields = fields.each_with_object([]) do |field, ary|
      if field.is_a?(ZendeskRules::CustomRuleField)
        ary.push(field.ticket_field)
      else
        fields_json.deep_merge!(field_value(field, ticket))
      end
    end

    custom_fields.each do |field|
      # TODO: Remove
      if (custom_field_value = custom_field_value(field, ticket))
        fields_json[:custom_fields].push(custom_field_value)
      end

      field_value = custom_field_value.try(:[], :name)
      field_value ||= custom_field_value.try(:[], :value)

      fields_json.merge!(field.id => field_value)
    end

    # TODO: Remove (deprecated as of 09/21/12)
    fields_json[:fields] = fields_json[:custom_fields]

    fields_json
  end

  def custom_field_value(field, ticket)
    entry = ticket.ticket_field_entries.detect { |tfe| tfe.ticket_field_id == field.id }

    if entry
      value = ticket_field_entry_value(entry, field)

      hash = { id: field.id, value: value }
      hash.merge!(custom_field_name(field, value)) if field.respond_to?(:custom_field_options)
      hash
    end
  end

  def custom_field_name(field, value)
    custom_field_option = field.custom_field_options.detect { |custom_field| custom_field["value"] == value }
    custom_field_option ? { name: render_dynamic_content(custom_field_option["name"]) } : {}
  end

  def field_value(field, ticket)
    case field.identifier
    when :locale_id
      { locale: ticket.requester.translation_locale.name }
    when :submitter, :assignee, :requester, :group, :organization
      id = "#{field.identifier}_id"
      { id => ticket.send(id) }
    when :updated_by_type
      { updated_by_type: UpdatedByType[ticket.updated_by_type_id].localized_name }
    when :nice_id
      { ticket_id: ticket.nice_id }
    when :via
      { via: serialized_via_object(ticket) }
    when :due_date
      value = field.humanized_value(ticket)
      { due_date: value, due_at: value}
    when :subject
      { subject: render_dynamic_content(ticket.subject, ticket: ticket) }
    else
      { field.identifier => render_dynamic_content(field.humanized_value(ticket)) }
    end
  end

  def association_preloads
    { ticket_field_entries: {} }
  end

  private

  def conditions_presenter
    @conditions_presenter ||= begin
      Api::V2::Rules::ConditionsPresenter.new(user, options)
    end
  end

  def output_presenter
    @output_presenter ||= Api::V2::Rules::OutputPresenter.new(user, options)
  end
end
