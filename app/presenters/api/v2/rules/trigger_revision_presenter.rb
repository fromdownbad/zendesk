class Api::V2::Rules::TriggerRevisionPresenter < Api::V2::Presenter
  self.model_key = :trigger_revision

  ## ### Trigger revision
  ##
  ## | Name       | Type                  | Description
  ## | ---------- | --------------------- | -----------
  ## | url        | string                | The API url of this revision
  ## | id         | integer               | ID of the revision
  ## | author_id  | integer               | ID of the user who made the revision
  ## | created_at | date                  | Time the revision was created
  ## | snapshot   | object                | State of the trigger at time of revision. See [Snapshot](#snapshot)
  ## | diff       | object                | Changes introduced in this revision. See [Rule Diff](#rule-diff)
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "trigger_revision": {
  ##     "url": "https://{subdomain}.zendesk.com/api/v2/trigger/123/revisions/1.json"
  ##     "id": "1",
  ##     "author_id": "4",
  ##     "created_at": 2017-08-10T21:10:05Z,
  ##     "snapshot": { ... },
  ##     "diff": { ... }
  ##   }
  ## }
  ## ```
  def model_json(revision)
    {
      url:        url(revision),
      id:         revision.nice_id,
      author_id:  revision.author_id,
      created_at: revision.created_at,
      snapshot:   snapshot_presenter.model_json(revision.snapshot)
    }
  end

  def side_loads(item_or_collection)
    return {} unless side_load?(:users)

    {
      users: side_load_association(
        Array(item_or_collection),
        :author,
        user_presenter
      )
    }
  end

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::TriggerRevisionCursorCollectionPresenter.new(
        user,
        options.merge(item_presenter: self, includes: includes)
      )
    end
  end

  private

  def snapshot_presenter
    @snapshot_presenter ||=
      Api::V2::Rules::TriggerSnapshotPresenter.new(user, options)
  end

  def url(revision)
    url_builder.api_v2_trigger_revision_url(
      revision.rule_id,
      revision.nice_id,
      format: :json
    )
  end
end
