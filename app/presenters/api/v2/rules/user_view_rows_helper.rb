module Api::V2::Rules::UserViewRowsHelper
  # user.translation_locale.name is necessary because TranslationLocale is unsharded.
  #   It is also cached.
  def field_value(column, user)
    case column.to_s
    when 'locale_id'
      user.locale_id ? user.translation_locale.localized_name(display_in: I18n.translation_locale) : account.translation_locale.localized_name(display_in: I18n.translation_locale)
    when 'current_tags'
      user.tags
    when 'phone'
      user.phone_number
    when 'email'
      user.is_email_google? ? nil : user.email
    when 'google'
      user.identities.detect { |i| i.type == "UserEmailIdentity" && i.google? }.try(:value)
    when 'twitter'
      user.identities.detect { |i| i.type == "UserTwitterIdentity" }.try(:twitter_user_profile).try(:screen_name)
    when 'facebook'
      user.identities.detect { |i| i.type == "UserFacebookIdentity" }.try(:value)
    when 'roles'
      roles_field_value(user)
    else
      user.try(column) if user.has_attribute?(column)
    end
  end

  def custom_field_value(custom_field_column, row_user)
    if field = row_user.custom_fields.detect { |f| f['key'] == custom_field_column }
      cf_value = row_user.custom_field_values.detect { |fv| !fv.destroyed? && fv.cf_field_id == field.id }
      value = field.type == "Dropdown" ? field.liquid_title(cf_value) : field.value_as_json(cf_value.value)
      Zendesk::Liquid::DcContext.render(value, account, user, "text/plain", nil, user.dc_cache)
    end
  end

  def roles_field_value(user)
    if custom_role_present?(user) || user.is_chat_agent?
      user.permission_set.translated_name
    else
      user.translated_role
    end
  end

  private

  def custom_role_present?(user)
    account.enterprise_roles_in_user_views_enabled? &&
      user.has_permission_set?
  end
end
