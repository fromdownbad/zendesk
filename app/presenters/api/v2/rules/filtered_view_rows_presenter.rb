class Api::V2::Rules::FilteredViewRowsPresenter < Api::V2::Rules::ViewRowsPresenter
  self.model_key = :row

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::FilteredViewCollectionPresenter.new(
        user,
        options.merge(item_presenter: self, includes: includes)
      )
    end
  end

  def side_loads(tickets)
    super(tickets).tap do |json|
      json[:insufficient_results] = true if side_load?(:insufficient_results)
    end
  end
end
