class Api::V2::Rules::UserViewConditionsPresenter < Api::V2::Rules::ConditionsPresenter
  self.model_key = :conditions

  ## ### Conditions
  ##
  ## *Conditions* check the value of user attributes and custom fields and selects the user if the conditions are met.
  ## Conditions are represented as a JSON object with two arrays of one or more conditions.
  ##
  ## **Example**
  ##
  ## ```js
  ## {
  ##    "conditions": {
  ##      "all": [
  ##        { "field": "custom_fields.seats", "operator": "less_than", "value": "200" }
  ##      ],
  ##      "any": [
  ##        { "field": "custom_fields.plan_type", "operator": "is", "value": "plan_gold" },
    ##      { "field": "custom_fields.plan_type", "operator": "is", "value": "plan_silver" }
  ##      ]
  ##    }
  ## }
  ## ```
  ##
  ## The first array lists all the conditions that must be met. The second array lists any condition that must be met.
  ##
  ## | Name     | Type  | Description
  ## | -------- | ----- | -----------
  ## | `all`    | array | Logical AND. Users must fulfill all of the conditions to be considered matching
  ## | `any`    | array | Logical OR. Users may satisfy any of the conditions to be considered matching
  ##
  ## Each condition in an array has the following properties:
  ##
  ## | Name             | Type                       | Description
  ## | ---------------- | ---------------------------| -------------------
  ## | field            | string                     | The name of a user attribute or user custom field.
  ## | operator         | string                     | A comparison operator
  ## | value            | string                     | The value of a user attribute or user custom field.
  ## | label (optional) | string                     | An optional label for the condition value.
  ##
  ## **Example**
  ##
  ## ```js
  ## { "field": "custom_fields.plan_type", "operator": "is", "value": "plan_gold" }
  ## ```
  ##
  ## When specifying conditions in a PUT or POST request, use the "all" and "any" arrays without the "conditions" key. Example:
  ##
  ## ```js
  ## {
  ##   "user_view": {
  ##     "title": "Roger Wilco",
  ##     "all": [
  ##       { "field": "custom_fields.plan_type", "operator": "is", "value": "plan_gold" },
  ##       { "field": "custom_fields.seats", "operator": "less_than", "value": "200" }
  ##     ],
  ##     "any": [
  ##     ]
  ##   }
  ## }
  ## ```
  ##
  ## #### Conditions reference
  ##
  ## The following tables list the fields, allowed operators, and values of the conditions used in user views.
  ##
  ## | field                                         | operator                                                                                                                  | value                                                                                               |
  ## | --------------------------------------------  | ------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
  ## | `created_at`                                  | `is`,<br />`is_not`,<br />`less_than`,<br />`greater_than`,<br />`less_than_or_equal_to`,<br />`greater_than_or_equal_to` | A date string, e.g. '2013-11-30'                                                                    |
  ## | `created_at`                                  | `within_previous_days`                                                                                                    | A positive integer representing the number of days                                                  |
  ## | `last_login`                                  | `is`,<br />`is_not`,<br />`less_than`,<br />`greater_than`,<br />`less_than_or_equal_to`,<br />`greater_than_or_equal_to` | A date string, e.g. '2013-11-30'                                                                    |
  ## | `last_login`                                  | `within_previous_days`,<br />`within_next_days`                                                                           | A positive integer representing the number of days                                                  |
  ## | `language`                                    | `is`,<br />`is_not`                                                                                                       | A string containing the ID of the selected language.                                                |
  ## | `organization_id`                             | `is`,<br />`is_not`                                                                                                       | "" (no organization added to the ticket) or the numeric ID of the organization added to the ticket. |
  ## | `current_tags`                                | `includes` (contains one word),<br />`not_includes` (contains none of the words)                                          | A space delimited list of tags to compare against the user's tags                                   |
  ## | `custom_fields.<field_key>` (Numeric)         | `is`,<br />`is_not`,<br />`less_than`,<br />`greater_than`,<br />`less_than_or_equal_to`,<br />`greater_than_or_equal_to` | A string containing an integer value for comparison to the custom Numeric field                     |
  ## | `custom_fields.<field_key>` (Numeric)         | `is_present`,<br />`is_not_present`                                                                                       | No value required                                                                                   |
  ## | `custom_fields.<field_key>` (Decimal)         | `is`,<br />`is_not`,<br />`less_than`,<br />`greater_than`,<br />`less_than_or_equal_to`,<br />`greater_than_or_equal_to` | A string containing a decimal value for comparison to the custom Decimal field                      |
  ## | `custom_fields.<field_key>` (Decimal)         | `is_present`,<br />`is_not_present`                                                                                       | No value required                                                                                   |
  ## | `custom_fields.<field_key>` (Checkbox)        | `true`,<br />`false`                                                                                                      | No value required                                                                                   |
  ## | `custom_fields.<field_key>` (Dropdown)        | `is`,<br />`is_not`                                                                                                       | Dropdown option value to match against the custom Dropdown field                                    |
  ## | `custom_fields.<field_key>` (Dropdown)        | `is_present`,<br />`is_not_present`                                                                                       | No value required                                                                                   |
  ## | `custom_fields.<field_key>` (Date)            | `is`,<br />`is_not`,<br />`less_than`,<br />`greater_than`,<br />`less_than_or_equal_to`,<br />`greater_than_or_equal_to` | A date string, e.g. '2013-11-30'                                                                    |
  ## | `custom_fields.<field_key>` (Date)            | `within_previous_days`,<br />`within_next_days`                                                                           | A positive integer representing the number of days                                                  |
  ## | `custom_fields.<field_key>` (Text)            | `is`,<br />`is_not`                                                                                                       | A string for matching text                                                                          |
  ## | `custom_fields.<field_key>` (Multi-line Text) | `is`,<br />`is_not`                                                                                                       | A string for matching text                                                                          |
  ## | `custom_fields.<field_key>` (Regexp)          | `is`,<br />`is_not`                                                                                                       | A string for matching text                                                                          |
  ##

  protected

  def definition_json(conditions)
    conditions.map do |condition|
      label = label_for(condition)

      {
        field:    condition.source,
        operator: condition.operator,
        value:    condition.value
      }.tap do |json|
        json[:label] = label if label
      end
    end
  end

  def label_for(condition)
    return if UserView::PRESENCE_OPERATORS.include?(condition.operator)

    if condition.source == 'organization_id'
      account.organizations.find_by_id(condition.value).try(:name)
    end
  end
end
