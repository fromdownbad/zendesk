class Api::V2::Rules::RuleDiffPresenter < Api::V2::Presenter
  self.model_key = :diff

  ## ### Rule Diff
  ##
  ## | Name        | Type                          | Comment
  ## | ----------- | ----------------------------- | -------
  ## | source_id   | integer                       | ID of the source revision
  ## | target_id   | integer                       | ID of the target revision
  ## | active      | array                         | An array of [change](#changes) objects
  ## | title       | array                         | An array of [change](#changes) objects
  ## | description | array                         | An array of [change](#changes) objects
  ## | conditions  | object                        | The changes to conditions. See [Condition Diffs](#condition-diffs)
  ## | actions     | array                         | An array that contains [action diff objects](#action-diffs)
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "diff": {
  ##     "source_id": 1,
  ##     "target_id": 2,
  ##     "title": [
  ##       {
  ##         "change": "-",
  ##         "content": "deleted title"
  ##       },
  ##       {
  ##         "change": "+",
  ##         "content": "new title"
  ##       }
  ##     ],
  ##     "description": [
  ##       {"change": "=", "content": "A thing!"}
  ##     ],
  ##     "active": [
  ##       {
  ##         "change": "-",
  ##         "content": true
  ##       },
  ##       {
  ##         "change": "+",
  ##         "content": false
  ##       }
  ##     ],
  ##     "conditions": {
  ##       "all": [
  ##         {
  ##           "field": [
  ##             {"change": "+", "content": "status"},
  ##             {"change": "-", "content": "current_tags"}
  ##           ],
  ##           "operator": [
  ##             {"change": "+", "content": "is"},
  ##             {"change": "-", "content": "includes"}
  ##           ],
  ##           "value": [
  ##             {"change": "+", "content": "open"},
  ##             {"change": "-", "content": "tag"}
  ##           ],
  ##         }
  ##       ]
  ##     },
  ##     "actions": [
  ##       {
  ##         "field": [
  ##           {"change": "=", "content": "priority"},
  ##         ],
  ##         "value": [
  ##           {"change": "+", "content": "low"},
  ##           {"change": "-", "content": "high"}
  ##         ]
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  ## ### Changes
  ## *Changes* consist of an array of zero or more change objects.
  ##
  ## Each change object in the array has the following properties:
  ##
  ## | Name    | Type   | Description
  ## | ------- | ------ | -----------
  ## | change  | string | One of `-`, `+`, `=` representing the type of change
  ## | content | string | The value of the item it represents
  ##
  ## #### Example
  ## ```js
  ## [
  ##   { "change": "+", "content": "solved" },
  ##   { "change": "-", "content": "open" }
  ## ]
  ## ```
  def model_json(diff)
    {
      source_id:   diff.source_id,
      target_id:   diff.target_id,
      title:       diff.title.map(&:to_h),
      description: diff.description.map(&:to_h),
      active:      diff.active?.map(&:to_h),
      conditions:  condition_diffs_presenter.model_json(diff),
      actions:     action_diffs_presenter.model_json(diff)
    }
  end

  private

  def condition_diffs_presenter
    @condition_diffs_presenter ||= Api::V2::Rules::ConditionDiffsPresenter.new(
      user,
      options
    )
  end

  def action_diffs_presenter
    @action_diffs_presenter ||= Api::V2::Rules::ActionDiffsPresenter.new(
      user,
      options
    )
  end
end
