class Api::V2::Rules::RuleCategoryPresenter < Api::V2::Presenter
  def initialize(current_user, model_key: :rule_category, **presenter_options)
    @rule_type = presenter_options[:rule_type]
    @model_key = model_key

    super(current_user, presenter_options)
  end

  def model_json(category)
    {
      url: url(category),
      id: category.id.to_s,
      name: category.name,
      updated_at: category.updated_at,
      created_at: category.created_at,
      position: category.position
    }
  end

  # Change presenter to reference instance model_key for dynamic reassignment
  # due to polymorphic category type
  attr_reader :model_key, :rule_type

  # Change presenter to reference instance collection_key for dynamic reassignment due to
  # polymorphic category type
  def collection_key
    @collection_key ||= model_key.to_s.pluralize.to_sym
  end

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::RuleCategoryCollectionPresenter.new(
        user,
        options.merge(
          includes: includes,
          item_presenter: self,
          rule_type: rule_type
        )
      )
    end
  end
end
