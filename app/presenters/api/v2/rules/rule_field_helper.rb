require 'zendesk_rules/custom_rule_field'

module Api::V2::Rules::RuleFieldHelper
  include Api::V2::Tickets::TicketFieldHelper

  def columns_json(output)
    # Identifiers for CustomRuleFields come in a symbol of a string
    # of the id e.g. :"123"
    columns = output.columns.map { |col| col.to_s.to_sym }
    fields = ZendeskRules::RuleField.lookup(columns)

    fields = fields.sort_by do |field|
      pos = columns.index(field.identifier)

      unless pos
        als = field.aliases.detect { |a| columns.include?(a) }
        pos = columns.index(als)
      end

      pos
    end
    fields.map do |field|
      rule_field_json(field)
    end
  end

  def rule_field_json(field)
    if field.is_a?(ZendeskRules::CustomRuleField)
      custom_field_json(field.ticket_field)
    else
      system_field_json(field)
    end
  end

  def custom_field_json(field)
    {
      id: field.id,
      title: render_dynamic_content(field.title),
      type: ticket_field_type(field),
      url: url_builder.send(:api_v2_ticket_field_url, field, format: :json)
    }
  end

  def system_field_json(field)
    { id: map_system_field(field.identifier), title: field.title }
  end

  # Should match rules/view_rows presenter's
  # method 'field_value' mapping
  def map_system_field(field)
    case field
    when :nice_id
      "ticket_id"
    when :locale_id
      "locale"
    else
      field.to_s
    end
  end
end
