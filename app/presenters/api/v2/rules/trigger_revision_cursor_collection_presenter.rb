class Api::V2::Rules::TriggerRevisionCursorCollectionPresenter < Api::V2::CursorCollectionPresenter
  NULL_CURSORS = {before_url: nil, before_cursor: nil}.freeze
  private_constant :NULL_CURSORS

  def as_json(collection)
    super.merge(cursors(collection))
  end

  def model_json(collection)
    id_map = collection.index_by(&:nice_id)

    super(collection) do |_item, json|
      json[:diff] = diff(
        source(collection.first.rule_id, json[:id], id_map),
        id_map[json[:id]]
      )
    end
  end

  private

  def cursors(collection)
    initial_revision?(collection) ? NULL_CURSORS : {}
  end

  def initial_revision?(collection)
    collection.last.try(&:nice_id) == 1
  end

  def diff(source, target)
    trigger_revision_diff_presenter.model_json(
      Zendesk::Rules::Trigger::RevisionDiff.new(
        source: source || NullRevision,
        target: target
      )
    )
  end

  def source(rule_id, target_id, id_map)
    return NullRevision if target_id == 1

    id_map.fetch(target_id - 1) do
      TriggerRevision.where(
        account_id: account.id,
        rule_id:    rule_id,
        nice_id:    target_id - 1
      ).first
    end
  end

  def trigger_revision_diff_presenter
    @trigger_revision_diff_presenter ||=
      Api::V2::Rules::RuleDiffPresenter.new(user, options)
  end
end
