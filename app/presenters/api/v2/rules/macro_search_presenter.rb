class Api::V2::Rules::MacroSearchPresenter < Api::V2::Rules::MacroPresenter
  self.model_key = :macro

  SIDE_LOADS = %i[
    app_installation
    permissions
    usage_1h
    usage_24h
    usage_7d
    usage_30d
  ].freeze

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::RuleCollectionPresenter.new(
        user,
        options.merge(
          includes:       includes,
          item_presenter: self,
          side_loads:     SIDE_LOADS
        )
      )
    end
  end
end
