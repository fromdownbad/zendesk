class Api::V2::Rules::UserViewPresenter < Api::V2::Rules::RulePresenter
  self.model_key = :user_view

  ## ### JSON Format
  ## User views are represented as simple flat JSON objects which have the following keys.
  ##
  ## | Name            | Type                       | Comment
  ## | --------------- | -------------------------- | -------------------------
  ## | id              | integer                    | Automatically assigned when created
  ## | title           | string                     | The title of the user view
  ## | active          | boolean                    | Indicates if this view is shown by default in the agent interface
  ## | restriction     | object                     | Who may access this user view. Will be null when everyone in the account can access it.
  ## | execution       | [Execute](#execution)      | An object describing how the user view should be executed
  ## | conditions      | [Conditions](#conditions)  | An object describing how the user view is constructed
  ## | created_at      | date                       | Date and time when this user view was created
  ## | updated_at      | date                       | Date and tiem when this user view was last updated
  ##
  ## #### Example
  ## ```js
  # #{
  ## "user_view": {
  ##   "url": "http://support.localhost:3000/api/v2/user_views/702.json",
  ##   "id": 702,
  ##   "title": "Users created in the last week",
  ##   "active": true,
  ##   "updated_at": "2013-02-11T22:58:58Z",
  ##   "created_at": "2013-02-07T23:34:34Z",
  ##   "execution": { ... },
  ##   "conditions": { ... },
  ##   "restriction": { ... }.
  ## }
  ## ```
  def model_json(user_view)
    options[:user_view] = user_view
    super.merge!(
      execution:   execution_presenter.model_json(user_view.output),
      conditions:  conditions_presenter.model_json(user_view.definition),
      restriction: restriction_json(user_view)
    ).tap do |json|
      json.merge!(dynamic_content_json(user_view))
    end
  end

  def url(user_view)
    super unless user_view.new_record?
  end

  private

  def conditions_presenter
    @conditions_presenter ||= options[:conditions_presenter] ||= Api::V2::Rules::UserViewConditionsPresenter.new(user, options)
  end

  def execution_presenter
    @execution_presenter ||= options[:execution_presenter] ||= Api::V2::Rules::UserViewExecutionPresenter.new(user, options)
  end

  def restriction_json(user_view)
    case user_view.owner
    when Group
      {type: 'Group', id: user_view.owner.id}
    when User
      {type: 'User', id: user_view.owner.id}
    end
  end

  def dynamic_content_json(user_view)
    {
      title:     render_dynamic_content(user_view.title),
      raw_title: user_view.title
    }
  end
end
