class Api::V2::Rules::ViewDefinitionsPresenter < Api::V2::Rules::RuleDefinitionsPresenter
  self.model_key = :definitions

  def model_json(definitions)
    {
      **conditions_all(definitions),
      **conditions_any(definitions),
      **view_output(definitions),
      **groupables(definitions),
      **sortables(definitions)
    }
  end

  private

  def view_output(definitions)
    {
      output: view_output_definitions_presenter.
        collection_presenter.
        model_json(definitions.view_output)
    }
  end

  def groupables(definitions)
    {
      groupables: view_output_definitions_presenter.
        collection_presenter.
        model_json(definitions.groupables)
    }
  end

  def sortables(definitions)
    {
      sortables: view_output_definitions_presenter.
        collection_presenter.
        model_json(definitions.sortables)
    }
  end

  def view_output_definitions_presenter
    @view_output_definitions_presenter ||= begin
      Api::V2::Rules::ViewOutputDefinitionsPresenter.new(
        user,
        url_builder: self
      )
    end
  end
end
