class Api::V2::Rules::MacroPresenter < Api::V2::Rules::RulePresenter
  self.model_key = :macro

  SIDE_LOADS = %i[
    app_installation
    categories
    permissions
    usage_1h
    usage_24h
    usage_7d
    usage_30d
  ].freeze

  ## ### JSON Format
  ## Macros are represented as simple flat JSON objects which have the following keys.
  ##
  ## | Name        | Type                | Comment                                                                            |
  ## | ----------- | ------------------- | ---------------------------------------------------------------------------------- |
  ## | id          | integer             | Automatically assigned when created                                                |
  ## | actions     | [Actions](#actions) | An object describing what the macro will do                                        |
  ## | active      | boolean             | Useful for determining if the macro should be displayed                            |
  ## | description | string              | The description of the macro                                                       |
  ## | position    | integer             | The position of the macro                                                          |
  ## | restriction | object              | Who may access this macro. Will be null when everyone in the account can access it |
  ## | title       | string              | The title of the macro                                                             |
  ## | created_at  | date                | The time the macro was created                                                     |
  ## | updated_at  | date                | The time of the last update of the macro                                           |
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "macro": {
  ##     "id": 25,
  ##     "title": "Close and Save",
  ##     "active": true,
  ##     "actions": { ... },
  ##     "position": 42,
  ##     "restriction": {
  ##       "type": "User",
  ##       "id": 4
  ##     },
  ##    "description": "Sets the ticket status to `solved`"
  ##   }
  ## }
  ## ```
  def model_json(macro)
    super.merge!(
      position:    macro.position,
      description: macro.description,
      actions:     actions_presenter.model_json(macro.definition),
      restriction: restriction_json(macro)
    ).tap do |json|
      json.merge!(attachments_json(macro))
      json.merge!(app_installation_json(macro))
      json.merge!(title_json(macro))
      json.merge!(usage_json(macro))
    end
  end

  def collection_presenter
    @collection_presenter ||= begin
      Api::V2::Rules::RuleCollectionPresenter.new(
        user,
        options.merge(
          includes:       includes,
          item_presenter: self,
          side_loads:     SIDE_LOADS
        )
      )
    end
  end

  private

  def actions_presenter
    @actions_presenter ||= Api::V2::Rules::ActionsPresenter.new(user, options)
  end

  def association_preloads
    {attachments: {}}
  end

  def restriction_json(macro)
    case macro.owner_type
    when 'Group'
      {type: 'Group'}.tap do |restriction|
        restriction[:id]  = macro.group_ids.first
        restriction[:ids] = macro.group_ids
      end
    when 'User'
      {type: 'User', id: macro.owner.id}
    end
  end

  def attachments_json(macro)
    {}.tap do |json|
      json[:attachments] = macro.attachments.map(&:id) if macro.attachments.any?
    end
  end

  def app_installation_json(macro)
    return {} unless side_load?(:app_installation)

    {app_installation: macro.app_installation}
  end

  def title_json(macro)
    return highlight_json(macro) unless macro.account.has_dc_in_macro_titles?

    {
      title:     render_dynamic_content(macro.title),
      raw_title: macro.title
    }.tap do |json|
      json.merge!(highlight_json(macro)) unless dynamic_content?(json)
    end
  end

  def highlight_json(macro)
    return {} unless highlight_macro?(macro)

    {highlights: options[:highlights][macro.id]}
  end

  def highlight_macro?(macro)
    options[:highlights].present? && options[:highlights].key?(macro.id)
  end

  def usage_json(macro)
    {}.tap do |json|
      json[:usage_1h]  = macro.usage.hourly  if side_load?(:usage_1h)
      json[:usage_24h] = macro.usage.daily   if side_load?(:usage_24h)
      json[:usage_7d]  = macro.usage.weekly  if side_load?(:usage_7d)
      json[:usage_30d] = macro.usage.monthly if side_load?(:usage_30d)
    end
  end

  def dynamic_content?(json)
    json[:title] != json[:raw_title]
  end
end
