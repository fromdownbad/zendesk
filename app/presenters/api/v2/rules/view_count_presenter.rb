class Api::V2::Rules::ViewCountPresenter < Api::V2::Presenter
  self.model_key = :view_count

  ## ### JSON Format
  ## View counts are read-only and represented as simple flat JSON objects which have the following keys.
  ##
  ## | Name            | Type                       | Comment
  ## | --------------- | ---------------------------| -------------------
  ## | view_id         | integer                    | The id of the associated view
  ## | url             | string                     | A URL to retrieve the count for just this view
  ## | value           | integer                    | The number of tickets returned by the view
  ## | pretty          | string                     | A rendering of the approximate value, e.g. "~100"
  ## | fresh           | boolean                    | Whether the returned value was recently calculated
  #  | error          | string                     | Error status if the view was blocked
  #  | description    | string                     | Error description if the view was blocked
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "view_count": {
  ##     "url":     "https://company.zendesk.com/api/v2/rules/views/25/count.json",
  ##     "view_id": 25,
  ##     "value":   719,
  ##     "pretty":  "~700",
  ##     "fresh":   true
  #      "error": "LimitedViewExecution",
  #      "description": "The view has limited functionality until conditions are updated, including no counts"
  ##   }
  ## }
  ## ```
  def model_json(view_count)
    json = {
      view_id: view_count.rule_id,
      value:   nullify_negative_count(view_count.value),
      pretty:  view_count.pretty_print,
      fresh:   !view_count.is_updating?
    }

    # occam_count_many arturo enabled means view_count is an instance of
    # OccamTicketCount, it behaves like CachedRuleTicketCount but has
    # these unique methods we want to output
    json[:refresh]   = view_count.refresh   if view_count.respond_to?(:refresh)
    json[:channel]   = view_count.channel   if view_count.respond_to?(:channel)
    json[:poll_wait] = view_count.poll_wait if view_count.respond_to?(:poll_wait)

    if account.has_automatic_view_blocking? && view_blocked?(view_count.value)
      json.merge!(blocked_view_response)
    end

    super.merge!(json)
  end

  def side_loads(_view_counts)
    if side_load?(:suspended_tickets_count)
      { suspended_tickets_count: account.suspended_tickets.capped_count(5_000).to_s }
    else
      {}
    end
  end

  def url(view_count)
    url_builder.send(:count_api_v2_view_url, view_count.rule_id, format: :json) if view_count.rule_id
  end

  private

  def nullify_negative_count(count)
    view_blocked?(count) ? nil : count
  end

  def view_blocked?(count)
    count == ::View::BROKEN_COUNT
  end

  def blocked_view_response
    {
      error: 'LimitedViewExecution',
      description: I18n.t('txt.admin.views.rules.view_blocked_response')
    }
  end
end
