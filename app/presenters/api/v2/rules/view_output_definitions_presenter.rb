class Api::V2::Rules::ViewOutputDefinitionsPresenter < Api::V2::Presenter
  def model_json(output)
    {title: output.title, value: output.value}
  end
end
