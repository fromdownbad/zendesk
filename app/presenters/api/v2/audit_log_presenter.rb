class Api::V2::AuditLogPresenter < Api::V2::Presenter
  class FakeContext
    include TimeDateHelper

    def initialize(data)
      @data = data
    end

    def method_missing(name, *args)
      if @data.key?(name)
        @data[name]
      elsif ActionController::Base.helpers.respond_to?(name)
        ActionController::Base.helpers.send(name, *args)
      else
        message = "Unsupported method #{name} used in audit presenters"
        ZendeskExceptions::Logger.record(StandardError.new(message), location: self, message: message, reraise: !Rails.env.production?, fingerprint: '35c093bf437c21711de7b66e773d4fde36076816')
        "Error"
      end
    end
  end

  self.model_key = :audit_log

  ## ### JSON Format
  ## AuditLog have the following keys:
  ##
  ## | Name                | Type    | Read-only | Mandatory | Comment
  ## | ------------------- | ------- | --------- | --------- | -------
  ## | id                  | integer | yes       | no        | Automatically assigned upon creation
  ## | action              | string  | yes       | no        | create/update/destroy
  ## | action_label        | string  | yes       | no        | Localized string of action field
  ## | actor_id            | integer | yes       | no        | id of the user creating the ticket
  ## | source_id           | integer | yes       | no        | id of the item being audited
  ## | source_type         | string  | yes       | no        | type of the item being audited
  ## | source_label        | string  | yes       | no        | name of the item being audited
  ## | changes_description | string  | yes       | no        | description of the change that occurred
  ## | ip_address          | string  | yes       | no        | ip of the user doing the audit
  ## | created_at          | date    | yes       | no        | The time the audit got created
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":           498483,
  ##   "actor_id":     1234,
  ##   "source_id":    3456,
  ##   "source_type":  "user",
  ##   "source_label": "John Doe",
  ##   "action":       "update",
  ##   "action_label": "Updated"
  ##   "changes_description": "Role changed from Administrator to End User"
  ##   "ip_address":   "209.119.38.228",
  ##   "created_at":  "2012-03-05T11:32:44Z",
  ##   "url":          "https://company.zendesk.com/api/v2/audit_logs/498483.json",
  ## }
  ## ```
  def model_json(event)
    frontend_presenter = CIA::EventPresenter.new(FakeContext.new(current_user: user, current_account: user.account), event)

    super.merge!(
      id: event.id,
      action_label: frontend_presenter.action_label,
      actor_id: event.actor_id,
      source_id: event.source_id,
      source_type: event.source_type.underscore,
      source_label: frontend_presenter.source_label,
      action: event.action,
      change_description: frontend_presenter.change_descriptions.join("\n").strip_tags,
      ip_address: event.ip_address,
      created_at: event.created_at
    )
  end
end
