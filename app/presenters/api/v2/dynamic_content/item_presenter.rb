class Api::V2::DynamicContent::ItemPresenter < Api::V2::Presenter
  self.model_key = :item

  ## #### Dynamic Content Items
  ## Items are the parent level of Dynamic Content, they contain the variant language strings.
  ##
  ## Items have the following keys:
  ##
  ## | Name               | Type    | Read-only | Comment
  ## | ---------------    | ------- | --------- | -------
  ## | id                 | integer | yes       | Automatically assigned when creating the item
  ## | name               | string  | no        | The name of the dynamic content item
  ## | placeholder        | string  | yes       | The dynamic content placeholder
  ## | default_locale_id  | integer | no        | The locale id of the default variant
  ## | outdated           | boolean | yes       | If the variants are out of date or not
  ## | created_at         | date    | yes       | When this item was created
  ## | updated_at         | date    | yes       | When this item was updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "item": {
  ##     "url":               "https://company.zendesk.com/api/v2/dynamic_content/items/47.json",
  ##     "id":                47,
  ##     "name":              "Snowboard Problem",
  ##     "placeholder":       "{{dc.snowboard_problem}}",
  ##     "default_locale_id": 1,
  ##     "outdated":          false,
  ##     "created_at":        "2012-04-02T22:55:29Z",
  ##     "updated_at":        "2012-04-02T22:55:29Z"
  ##   }
  ## }
  ## ```

  def model_json(item)
    super.merge!(
      id:                item.id,
      name:              item.name,
      placeholder:       item.placeholder,
      default_locale_id: item.default_locale_id,
      outdated:          outdated(item),
      created_at:        item.created_at,
      updated_at:        item.updated_at,
      variants:          variant_presenter.present(item.variants)[:variants]
    )
  end

  def url(item)
    url_builder.send(:api_v2_dynamic_content_item_url, item, format: :json)
  end

  private

  def outdated(item)
    item.variants.outdated.present?
  end

  def variant_presenter
    @variant_presenter ||= options[:variant_presenter] ||= Api::V2::DynamicContent::VariantPresenter.new(user, options)
  end
end
