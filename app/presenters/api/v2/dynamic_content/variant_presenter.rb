class Api::V2::DynamicContent::VariantPresenter < Api::V2::Presenter
  self.model_key = :variant

  ## #### Dynamic Content Variants
  ## Variants are the children of Snippets, they contain the actual content for dynamic content.
  ##
  ## Items have the following keys:
  ##
  ## | Name               | Type    | Read-only | Comment
  ## | ---------------    | ------- | --------- | -------
  ## | id                 | integer | yes       | Automatically assigned when creating the variant
  ## | content            | string  | no        | The content of the dynamic content variant
  ## | locale_id          | integer | no        | The locale id of the variant
  ## | outdated           | boolean | yes       | If the variant is out of date or not
  ## | active             | boolean | no        | If the variant is active or not
  ## | default            | boolean | no        | If the variant is the default for the item
  ## | created_at         | date    | yes       | When this variant was created
  ## | updated_at         | date    | yes       | When this variant was updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "variant": {
  ##     "url":        "https://company.zendesk.com/api/v2/dynamic_content/items/6/variants/47.json",
  ##     "id":         47,
  ##     "content":    "Snowboard Problem",
  ##     "locale_id":  1,
  ##     "outdated":   false,
  ##     "active":     true,
  ##     "default":    false,
  ##     "created_at": "2012-04-02T22:55:29Z",
  ##     "updated_at": "2012-04-02T22:55:29Z"
  ##   }
  ## }
  ## ```

  def model_json(variant)
    super.merge!(
      id:         variant.id,
      content:    variant.value,
      locale_id:  variant.translation_locale_id,
      outdated:   variant.outdated?,
      active:     variant.active,
      default:    variant.is_fallback?,
      created_at: variant.created_at,
      updated_at: variant.updated_at
    )
  end

  def url(variant)
    url_builder.send(:api_v2_dynamic_content_item_variant_url, variant.text, variant, format: :json)
  end
end
