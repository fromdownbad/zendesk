class Api::V2::Search::LotusTicketPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings
  self.model_key = :ticket
  self.url_param = :nice_id

  def model_json(ticket)
    super.merge!(
      id:              ticket.nice_id,
      created_at:      ticket.created_at,
      updated_at:      ticket.updated_at,
      type:            TicketType[ticket.ticket_type_id.to_s].localized_name,
      subject:         ticket.subject,
      description:     render_description(ticket),
      status:          STATUS_MAP[ticket.status_id.to_s],
      requester_id:    ticket.requester_id,
      assignee_id:     ticket.assignee_id,
      organization_id: ticket.organization_id,
      group_id:        ticket.group_id
    )
  end

  def render_description(ticket)
    Zendesk::Liquid::TicketContext.render(ticket.description, ticket, user, user.is_agent?, user.translation_locale)
  end
end
