class Api::V2::Search::CountPresenter
  def present(result)
    {count: result.total_entries}
  end
end
