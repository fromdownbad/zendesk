class Api::V2::Search::LotusUserPresenter < Api::V2::Users::EndUserPresenter
  self.model_key = :user

  def model_json(user)
    {
      id:              user.id,
      name:            user.name,
      created_at:      user.created_at,
      updated_at:      user.updated_at,
      email:           user.email,
      photo:           photo(user),
      url:             url(user),
      organization_id: user.organization.try(:id),
      role:            user.role.downcase
    }
  end
end
