class Api::V2::Search::ResultsPresenter < Api::V2::CollectionPresenter
  include Api::Presentation::MixedCollection

  self.include_result_type = true

  ## ### JSON Format
  ## Search results are represented as JSON objects with the following keys.
  ##
  ## | Name                  | Type                 | Comment
  ## | --------------------- | ---------------------| --------------------
  ## | count                 | integer              | The total number of results matching this query
  ## | next_page             | string               | URL to the next page of results
  ## | prev_page             | string               | URL to the previous page of results
  ## | results               | array                | May consist of tickets, users, groups, or organizations, as specified by the `result_type` property in each result object
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "results": [
  ##     {
  ##       "name":        "Hello DJs",
  ##       "created_at":  "2009-05-13T00:07:08Z",
  ##       "updated_at":  "2011-07-22T00:11:12Z",
  ##       "id":          211,
  ##       "result_type": "group"
  ##       "url":         "https://foo.zendesk.com/api/v2/groups/211.json"
  ##     },
  ##     {
  ##       "name":        "Hello MCs",
  ##       "created_at":  "2009-08-26T00:07:08Z",
  ##       "updated_at":  "2010-05-13T00:07:08Z",
  ##       "id":          122,
  ##       "result_type": "group"
  ##       "url":         "https://foo.zendesk.com/api/v2/groups/122.json"
  ##     },
  ##     ...
  ##   ],
  ##   "facets":    null,
  ##   "next_page": "https://foo.zendesk.com/api/v2/search.json?query=\"type:Group hello\"&sort_by=created_at&sort_order=desc&page=2",
  ##   "prev_page": null,
  ##   "count":     1234
  ## }
  ## ```
  def self.presenters
    @presenters ||= {
      Ticket        => Api::V2::Tickets::MobileTicketPresenter,
      User          => Api::V2::Users::Presenter,
      Group         => Api::V2::GroupPresenter,
      Organization  => Api::V2::Organizations::Presenter,
      Entry         => Api::V2::TopicPresenter,
      Hash          => Api::V2::Search::HashPresenter
    }
  end

  def side_loads(results)
    super(results).merge(facets: results.facets)
  end

  def model_key
    :results
  end
end
