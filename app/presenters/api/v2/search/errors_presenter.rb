class Api::V2::Search::ErrorsPresenter
  ## ### Errors JSON Format
  ## Errors are represented as JSON objects which have the following keys:
  ##
  ## | Name                  | Type                 | Comment
  ## | --------------------- | ---------------------| --------------------
  ## | error                 | string               | The type of error. Examples: "unavailable", "invalid"
  ## | description           | string               |
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "error": "unavailable",
  ##   "description": "Sorry, we could not complete your search query. Please try again in a moment."
  ## }
  ## ```
  def present(query)
    { error: query.error.type, description: query.error.message }
  end
end
