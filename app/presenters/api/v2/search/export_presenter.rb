class Api::V2::Search::ExportPresenter < Api::V2::CursorCollectionPresenter
  include Api::Presentation::MixedCollection

  self.include_result_type = true

  # h3 JSON Format
  ## Export search results are represented as JSON objects with the following keys.
  ##
  ## | Name                  | Type                 | Comment
  ## | --------------------- | ---------------------| --------------------
  ## | links[next]           | string               | URL to the next page of results
  ## | meta[has_more]        | string               | Boolean indicating if there are more results
  ## | meta[after_cursor]    | string               | Cursor object returned from the Search Service
  ## | results               | array                | May consist of tickets, users, groups, or organizations, as specified by the `filter_type` parameter
  ##
  ## ##### Example
  ## ```js
  ## {
  ##   "results": [
  ##     {
  ##       "name":        "Hello DJs",
  ##       "created_at":  "2009-05-13T00:07:08Z",
  ##       "updated_at":  "2011-07-22T00:11:12Z",
  ##       "id":          211,
  ##       "result_type": "group"
  ##       "url":         "https://foo.zendesk.com/api/v2/groups/211.json"
  ##     },
  ##     {
  ##       "name":        "Hello MCs",
  ##       "created_at":  "2009-08-26T00:07:08Z",
  ##       "updated_at":  "2010-05-13T00:07:08Z",
  ##       "id":          122,
  ##       "result_type": "group"
  ##       "url":         "https://foo.zendesk.com/api/v2/groups/122.json"
  ##     },
  ##     ...
  ##   ],
  ##   "facets":    null,
  ##   "meta": {
  ##     "has_more": true,
  ##     "after_cursor": "ghHJKGuiyghjbkhYGLUGHJG7YG67C678yvbhjv"
  ##   },
  ##   "links": {
  ##     "next": "https://foo.zendesk.com/api/v2/search/export.json?query=\"hello\"&filter[type]=group&page[after]=ghHJKGuiyghjbkhYGLUGHJG7YG67C678yvbhjv&page[size]=3"
  ##   }
  ## }
  ## ```
  def self.presenters
    @presenters ||= {
      Ticket => Api::V2::Tickets::MobileTicketPresenter,
      User => Api::V2::Users::Presenter,
      Organization => Api::V2::Organizations::Presenter,
      Group => Api::V2::GroupPresenter,
      Hash => Api::V2::Search::HashPresenter
    }
  end

  def side_loads(results)
    super(results).merge(facets: results.facets)
  end

  def model_key
    :results
  end
end
