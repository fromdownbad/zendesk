class Api::V2::Search::LotusResultsPresenter < Api::V2::CollectionPresenter
  include Api::Presentation::MixedCollection

  self.include_result_type = true

  ## This is the internal API used by lotus to get search results, and as a result is as streamlined as possible.
  ## Do *not* expose this api to the world, it is not intended for public consumption (/api/v2/search.json is the public face)

  def self.presenters
    @presenters ||= {
      Ticket       => Api::V2::Search::LotusTicketPresenter,
      User         => Api::V2::Search::LotusUserPresenter,
      Group        => Api::V2::GroupPresenter,
      Organization => Api::V2::Organizations::Presenter,
      Entry        => Api::V2::TopicPresenter,
      Forum        => Api::V2::ForumPresenter,
      Hash         => Api::V2::Search::HashPresenter
    }
  end

  SIDE_LOAD_SOURCES = {
    User         => { Ticket => 'requester_id' },
    Organization => { User => 'organization_id', Ticket => 'organization_id' },
    Group        => { Organization => 'group_id', Ticket => 'group_id'},
    Forum        => { Entry => 'forum_id' }
  }.freeze

  def side_loads(results)
    side_loads = {}
    extra_user_ids = []
    if side_load?(:highlights)
      side_loads[:highlights] = options[:highlights]
      extra_user_ids = commenter_ids(options[:highlights])
    end

    side_loads.merge(
      facets:        results.facets,
      users:         side_load_from_results(results, User, extra_user_ids),
      organizations: side_load_from_results(results, Organization),
      groups:        side_load_from_results(results, Group),
      forums:        side_load_from_results(results, Forum)
    )
  end

  def side_load_from_results(results, target, extra_ids = [])
    sources = SIDE_LOAD_SOURCES[target]
    side_load_ids = results.map do |r|
      sources.key?(r.class) ? r[sources[r.class]] : nil
    end.concat(extra_ids).compact.uniq

    side_load_objs = target.where(id: side_load_ids).to_a
    presenter_for(target).collection_presenter.model_json(side_load_objs)
  end

  # Returns the commenter_ids referenced in the highlights
  def commenter_ids(highlights)
    commenter_ids = []
    if highlights && highlights[:results]
      highlights[:results].each do |result|
        next unless result['comments']
        result['comments'].each do |comment|
          commenter_ids << comment['commenter_id']
        end
      end
    end
    commenter_ids
  end

  def model_key
    :results
  end
end
