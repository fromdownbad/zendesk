class Api::V2::Search::HashPresenter < Api::V2::Presenter
  attr_reader :model_key

  def model_json(doc)
    if result_type = doc.delete('result_type')
      doc[:result_type] = @model_key = result_type
    end
    doc
  end
end
