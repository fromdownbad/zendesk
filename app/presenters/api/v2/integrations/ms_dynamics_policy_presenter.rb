class Api::V2::Integrations::MsDynamicsPolicyPresenter < Api::V2::Presenter
  self.model_key = :ms_dynamics_integration

  ## ### XML Format
  ## Generates the client access policy xml that Microsoft Dynamics requires to access the CRM.
  ## The <domain uri> record must be set to the CRM server address captured in the configuration page.
  ##
  ## #### Example
  ## ```xml
  ##  <access-policy>
  ##    <cross-domain-access>
  ##      <policy>
  ##        <allow-from http-methods="*" http-request-headers="*">
  ##          <domain uri="ZendeskDynamics.com"/>
  ##        </allow-from>
  ##        <grant-to>
  ##          <resource path="/" include-subpaths="true"/>
  ##        </grant-to>
  ##      </policy>
  ##    </cross-domain-access>
  ##  </access-policy>
  ## ```
  def present_xml(ms_dynamics_integration)
    http_request_headers = ms_dynamics_integration.account.try(:has_dynamics_soapaction?) ? "SOAPAction" : "*"
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.send(:"access-policy") do
        xml.send(:"cross-domain-access") do
          xml.policy do
            xml.send(:"allow-from", "http-methods" => "*", "http-request-headers" => http_request_headers) do
              xml.domain("uri" => ms_dynamics_integration.server_address_for_policy)
              ms_dynamics_integration.additional_domains_for_policy.each do |domain|
                xml.domain("uri" => domain)
              end
            end
            xml.send(:"grant-to") do
              xml.resource("include-subpaths" => "true", "path" => "/")
            end
          end
        end
      end
    end

    builder.to_xml(save_with: Nokogiri::XML::Node::SaveOptions::AS_XML | Nokogiri::XML::Node::SaveOptions::NO_DECLARATION).strip
  end
end
