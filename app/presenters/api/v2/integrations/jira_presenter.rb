class Api::V2::Integrations::JiraPresenter < Api::V2::Presenter
  self.model_key = nil

  ## ### JSON Format
  ## JIRA ticket details are represented as JSON objects which have the following keys:
  ## ...
  def model_json(jira)
    jira || {}
  end
end
