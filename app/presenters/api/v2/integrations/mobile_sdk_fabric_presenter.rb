class Api::V2::Integrations::MobileSdkFabricPresenter
  def present(mobile_sdk_app, account)
    {
      id: mobile_sdk_app['id'].to_s,
      keys: {
        primary: {
          url: account.url(mapped: false, protocol: 'https'),
          identifier: mobile_sdk_app['identifier'],
          client_identifier: mobile_sdk_app['client_identifier']
        }
      }
    }
  end
end
