class Api::V2::GroupPresenter < Api::V2::Presenter
  self.model_key = :group

  ## ### JSON Format
  ## Groups are represented as simple flat JSON objects which have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned when creating groups
  ## | url             | string  | yes       | no        | The API url of this group
  ## | name            | string  | no        | yes       | The name of the group
  ## | description     | string  | no        | no        | The description of the group
  ## | default         | boolean | yes       | no        | If group is default for the account
  ## | deleted         | boolean | yes       | no        | Deleted groups get marked as such
  ## | created_at      | date    | yes       | no        | The time the group was created
  ## | updated_at      | date    | yes       | no        | The time of the last update of the group
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":          3432,
  ##   "url":         "https://company.zendesk.com/api/v2/groups/3432.json",
  ##   "deleted",     false,
  ##   "name":        "First Level Support",
  ##   "description": "Some clever description here",
  ##   "default":     true,
  ##   "created_at":  "2009-07-20T22:55:29Z",
  ##   "updated_at":  "2011-05-05T10:38:52Z"
  ## }
  ## ```
  def model_json(group)
    super.merge!(
      {
        id:          group.id,
        name:        group.name,
        description: group.description,
        default:     group.default,
        deleted:     group.deleted?,
        created_at:  group.created_at,
        updated_at:  group.updated_at
      }.tap do |json|
        json[:user_ids] = group.users.map(&:id) if side_load?(:users) || side_load?(:user_ids)
        json[:settings] = group.settings.get if side_load?(:group_settings)
      end
    )
  end

  def condensed_model_json(group)
    model_json(group).slice(:id, :name, :description, :url)
  end

  def side_loads(groups)
    {}.tap do |side_loads|
      if side_load?(:users)
        side_loads.merge!(
          users: side_load_association(Array(groups), :users, user_presenter)
        )
      end
    end
  end

  def association_preloads
    {}.tap do |preloads|
      if side_load?(:users)
        preloads.merge!(users: user_presenter.association_preloads)
      end
    end
  end
end
