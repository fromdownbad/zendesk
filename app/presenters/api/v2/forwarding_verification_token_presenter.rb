class Api::V2::ForwardingVerificationTokenPresenter < Api::V2::Presenter
  self.model_key = :forwarding_verification_token

  def model_json(verification_token)
    verification_token.attributes.
      slice("id", "from_email", "to_email", "email_provider", "value", "created_at", "updated_at")
  end
end
