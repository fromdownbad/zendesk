module Api::V2
  class Routing::ConditionsPresenter < Rules::ConditionsPresenter
    self.model_key = :conditions

    protected

    def definition_json(conditions)
      conditions.map do |condition|
        {
          subject:  attribute_name(condition),
          operator: condition.operator,
          value:    attribute_value(condition)
        }
      end
    end
  end
end
