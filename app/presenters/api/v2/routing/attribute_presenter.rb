class Api::V2::Routing::AttributePresenter < Api::V2::Presenter
  self.model_key = :attribute

  def model_json(attribute)
    super.merge!(
      id:         attribute.id,
      name:       attribute.name,
      created_at: attribute.created_at,
      updated_at: attribute.updated_at
    )
  end

  def url(model)
    url_builder.api_v2_routing_attribute_url(model.id, format: :json)
  end
end
