class Api::V2::Routing::IncrementalInstanceValuePresenter < Api::V2::Presenter
  self.model_key = :instance_value

  ## ### JSON Format for Routing Instance Values
  ## Routing instance values have the following format:
  ##
  ## | Name               | Type    | Comment
  ## | ------------------ | ------- | -------
  ## | id                 | string  | Automatically assigned when an instance value is created
  ## | attribute_value_id | string  | Id of the associated attribute value
  ## | instance_id        | string  | Id of the associated agent or ticket
  ## | time               | date    | The time the instance value was created or deleted
  ## | type               | string  | One of "associate_agent", "unassociate_agent", "associate_ticket", or "unassociate_ticket"
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id": "62055cad-7326-11e8-b07e-73653560136b",
  ##   "attribute_value_id": "19ed17fb-7326-11e8-b07e-9de44e7e7f20",
  ##   "instance_id": "10001",
  ##   "time": "2018-06-19T01:35:27Z",
  ##   "type": "associate_agent"
  ## }
  ## ```
  def model_json(instance_value)
    {
      id:                 instance_value.id,
      attribute_value_id: instance_value.attribute_value_id,
      instance_id:        instance_value.instance_id,
      time:               instance_value.time,
      type:               instance_value.type
    }
  end
end
