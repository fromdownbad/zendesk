class Api::V2::Routing::AttributeValuePresenter < Api::V2::Presenter
  self.model_key = :attribute_value

  def model_json(attribute_value)
    json = super.merge!(
      id:         attribute_value.id,
      name:       attribute_value.name,
      created_at: attribute_value.created_at,
      updated_at: attribute_value.updated_at
    )

    if side_load?(:conditions)
      json.merge!(conditions_json(attribute_value.conditions))
    end

    if options[:include_attribute_id]
      json[:attribute_id] = attribute_value.attribute_id
    end

    json
  end

  def url(attribute_value)
    url_builder.api_v2_routing_attribute_value_url(
      attribute_value.id,
      attribute_id: attribute_value.attribute_id,
      format:       :json
    )
  end

  private

  def conditions_json(conditions)
    return {} unless conditions.present?

    {conditions: conditions_presenter.model_json(conditions)}
  end

  def conditions_presenter
    @conditions_presenter ||= begin
      Api::V2::Routing::ConditionsPresenter.new(user, options)
    end
  end
end
