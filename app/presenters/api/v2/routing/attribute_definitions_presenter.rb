class Api::V2::Routing::AttributeDefinitionsPresenter < Api::V2::Presenter
  self.model_key = :definitions

  include Api::V2::Tickets::AttributeMappings

  def model_json(definitions)
    {
      conditions_all: condition_definition_presenter.
        collection_presenter.
        model_json(definitions.conditions_all),
      conditions_any: condition_definition_presenter.
        collection_presenter.
        model_json(definitions.conditions_any)
    }
  end

  private

  def condition_definition_presenter
    @condition_definition_presenter ||= begin
      Api::V2::Rules::ConditionDefinitionPresenter.new(
        user,
        url_builder: self,
        metadata_presenter: Api::V2::Rules::MetadataPresenter.new(
          user,
          url_builder: self
        )
      )
    end
  end
end
