class Api::V2::Routing::IncrementalAttributeValuePresenter < Api::V2::Presenter
  self.model_key = :attribute_value

  ## ### JSON Format for Routing Attribute Values
  ## A routing attribute value is a skill. Routing attribute values have the following format:
  ##
  ## | Name         | Type    | Comment
  ## | ------------ | ------- | -------
  ## | id           | string  | Automatically assigned when an attribute value is created
  ## | attribute_id | string  | Id of the associated attribute
  ## | name         | string  | The name of the attribute value
  ## | time         | date    | The time the attribute value was created, updated, or deleted
  ## | type         | string  | One of "create", "update", or "delete"
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id": "19ed17fb-7326-11e8-b07e-9de44e7e7f20",
  ##   "attribute_id": "7c43bca9-8c7b-11e8-b808-b99aed889f62",
  ##   "name": "English",
  ##   "time": "2018-06-19T01:33:26Z",
  ##   "type": "create"
  ## }
  ## ```
  def model_json(attribute_value)
    {
      id:           attribute_value.id,
      attribute_id: attribute_value.attribute_id,
      name:         attribute_value.name,
      time:         attribute_value.time,
      type:         attribute_value.type
    }
  end
end
