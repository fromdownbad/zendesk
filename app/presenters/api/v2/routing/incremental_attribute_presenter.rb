class Api::V2::Routing::IncrementalAttributePresenter < Api::V2::Presenter
  self.model_key = :attribute

  ## ### JSON Format for Routing Attributes
  ## A routing attribute is a skill type. Routing attributes have the following format:
  ##
  ## | Name  | Type    | Comment
  ## | ----- | ------- | -------
  ## | id    | string  | Automatically assigned when an attribute is created
  ## | name  | string  | The name of the attribute
  ## | time  | date    | The time the attribute was created, updated, or deleted
  ## | type  | string  | One of "create", "update", or "delete"
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":   "7c43bca9-8c7b-11e8-b808-b99aed889f62",
  ##   "name": "Languages",
  ##   "time": "2018-07-21T07:17:42Z",
  ##   "type": "create"
  ## }
  ## ```
  def model_json(attribute)
    {
      id:   attribute.id,
      name: attribute.name,
      time: attribute.time,
      type: attribute.type
    }
  end
end
