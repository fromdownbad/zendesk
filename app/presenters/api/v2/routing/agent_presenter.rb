class Api::V2::Routing::AgentPresenter < Api::V2::Users::AgentPresenter
  self.model_key = :user

  def model_json(agent)
    super.merge!(
      has_attribute_value: options[:agents_with_value].key?(agent.id)
    ).tap do |json|
      if side_load?(:group_membership)
        json[:group_membership] = group_membership(agent)
      end
    end
  end

  private

  def group_membership(agent)
    agent.groups.pluck(:id)
  end
end
