class Api::V2::CustomStatusPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings

  self.model_key = :custom_status

  def model_json(custom_status)
    super.merge(
      id: custom_status.id,
      status_category: custom_status.status_category,
      agent_label: render_dynamic_content(custom_status.agent_label),
      raw_agent_label: custom_status.agent_label,
      end_user_label: render_dynamic_content(custom_status.end_user_label),
      raw_end_user_label: custom_status.end_user_label,
      description: render_dynamic_content(custom_status.description),
      raw_description: custom_status.description,
      active: custom_status.active?,
      default: custom_status.default?,
      created_at: custom_status.created_at,
      updated_at: custom_status.updated_at
    )
  end
end
