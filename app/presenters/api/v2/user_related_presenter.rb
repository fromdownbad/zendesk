class Api::V2::UserRelatedPresenter < Api::V2::Presenter
  self.model_key = :user_related

  ## ### JSON Format for User Related Information
  ## The JSON returned by the [user related information endpoint](#user-related-information) includes the following properties.
  ##
  ## **Note:** Depending on the user's permissions, the count results may not match the actual number of tickets returned.
  ##
  ## | Name                       | Type    | Comment
  ## | -------------------------- | ------- | -------
  ## | assigned_tickets           | integer | Count of assigned tickets
  ## | requested_tickets          | integer | Count of requested tickets
  ## | ccd_tickets                | integer | Count of collaborated tickets
  ## | organization_subscriptions | integer | Count of organization subscriptions
  ##
  ## It also includes the following additional properties about the Web portal, the self-help solution offered by Zendesk
  ## before Help Center. The Web portal is deprecated but the API still returns data about the user's activity if they used it.
  ## See the [Help Center API](https://developer.zendesk.com/rest_api/docs/help_center/introduction) to get comparable
  ## data for Help Center. Articles, posts, comments, subscriptions, and votes each have a list-by-user endpoint.
  ##
  ## | Name                       | Type    | Comment
  ## | -------------------------- | ------- | -------
  ## | topics                     | integer | Count of topics (Web portal only)
  ## | topic_comments             | integer | Count of comments on topics (Web portal only)
  ## | votes                      | integer | Count of votes (Web portal only)
  ## | subscriptions              | integer | Count of subscriptions (Web portal only)
  ## | entry_subscriptions        | integer | Count of entry subscriptions (Web portal only)
  ## | forum_subscriptions        | integer | Count of forum subscriptions (Web portal only)
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "user_related": {
  ##     "assigned_tickets": 5
  ##     "requested_tickets": 10,
  ##     "ccd_tickets": 3,
  ##     "topics": 2,
  ##     "topic_comments": 7,
  ##     "votes": 3,
  ##     "subscriptions": 4,
  ##     "entry_subscriptions": 1,
  ##     "forum_subscriptions": 2,
  ##     "organization_subscriptions": 1,
  ##   }
  ## }
  ## ```
  def model_json(user)
    json = {
      assigned_tickets: user_assigned_count(user),
      requested_tickets: count_tickets_filtered_by_current_user(@user, user),
      topics: user.entries.count(:all),
      topic_comments: user.posts.count(:all),
      votes: user.votes.count(:all),
      subscriptions: user.watchings.count(:all),
      entry_subscriptions: user.watchings.only_entry.count(:all),
      forum_subscriptions: user.watchings.only_forum.count(:all),
      organization_subscriptions: user.watchings.only_organization.count(:all),
    }

    if user.account.has_follower_and_email_cc_collaborations_enabled?
      json[:followed_tickets] = if user.account.has_ticket_followers_allowed_enabled?
        user.followed_tickets.count_with_archived
      else
        0
      end

      json[:ccd_tickets] = if user.account.has_comment_email_ccs_allowed_enabled?
        user.ccd_tickets.count_with_archived
      else
        0
      end
    else
      json[:ccd_tickets] = user.collaborated_tickets.count_with_archived
    end

    json
  end

  private

  REQUESTED_TICKET_COUNT_LIMIT = 100
  COUNT_CACHE_THRESHOLD = 10_000

  def user_assigned_count(user)
    cache_key = user_related_count_cache_key(user, :assigned)
    cached_value = Rails.cache.read(cache_key)
    return cached_value if user.account.has_cache_user_related_counts? && cached_value

    count = user.assigned.count_with_archived
    if user.account.has_cache_user_related_counts? && count > COUNT_CACHE_THRESHOLD
      Rails.cache.write(cache_key, count, expires_in: 1.hours)
    end
    count
  end

  def count_tickets_filtered_by_current_user(current_user, user)
    cache_key = user_related_count_cache_key(user, :requested)
    cached_value = Rails.cache.read(cache_key)
    return cached_value if user.account.has_cache_user_related_counts? && cached_value

    count = user.tickets.count_with_archived

    if user.account.has_api_filter_requested_tickets? && !current_user.is_admin? && count < REQUESTED_TICKET_COUNT_LIMIT
      tickets = user.tickets.all_with_archived(stub_only: true)
      tickets.select! { |ticket| current_user.can?(:view, ticket) }

      count = tickets.count
    end

    if user.account.has_cache_user_related_counts? && count > COUNT_CACHE_THRESHOLD
      Rails.cache.write(cache_key, count, expires_in: 1.hours)
    end

    count
  end

  def user_related_count_cache_key(user, key)
    ActiveSupport::Cache.expand_cache_key(
      [
        'user_related_presenter',
        '1',
        "#{user.account_id}/#{user.id}",
        key
      ]
    )
  end
end
