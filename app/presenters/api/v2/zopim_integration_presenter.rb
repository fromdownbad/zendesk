class Api::V2::ZopimIntegrationPresenter < Api::V2::Presenter
  self.model_key = :zopim_integration
  self.singular_resource = true

  # ### JSON Format
  # ZopimIntegrations have the following attributes
  #
  # | Name              | Type                              | Comment
  # | ---------------   | --------------------------------- | -------
  # | external_zopim_id | integer                           | Zopim ID
  # | zopim_key         | string                            | Zopim Key
  # | phase             | integer                           | The Zopim integration phase
  # | agent_sync        | boolean                           | If Zopim agent sync in enabled
  # | billing           | boolean                           | If Zopim billing is enabled
  # | created_at        | date                              | The time the record was created
  # | updated_at        | date                              | The time the record was updated
  #
  # #### Example
  # ```js
  # {
  #   "external_zopim_id": 12345678,
  #   "zopim_key":         "abcdefg",
  #   "phase":             2
  #   "agent_sync":        true,
  #   "billing":           true
  #   "created_at":        "2014-03-25T00:30:11Z",
  #   "updated_at":        "2014-03-25T00:30:11Z"
  # }
  # ```
  def model_json(model)
    {}.tap do |json|
      json.merge!(
        external_zopim_id: model.external_zopim_id,
        zopim_key: model.zopim_key,
        phase: model.phase,
        agent_sync: model.has_agent_sync?,
        billing: model.has_billing?,
        created_at: model.created_at,
        updated_at: model.updated_at
      ) unless model.blank?
    end
  end

  def side_loads(model)
    {}.tap do |json|
      if side_load?(:app_market_integration)
        json[:app_market_integration] = app_market_integration_json(model)
      end
    end
  end

  private

  def app_market_integration_json(model)
    app_market_integration_presenter.model_json(model)
  end

  def app_market_integration_presenter
    @app_market_integration_presenter ||= Api::V2::AppMarketIntegrationPresenter.
      new(user, options)
  end
end
