class Api::V2::TopicPresenter < Api::V2::Presenter
  self.model_key = :topic
  self.url_param = :id

  ## ### JSON Format
  ## Topics are represented in JSON with the below attributes
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | url             | string  | yes       | no        | The API url of this topic
  ## | title           | string  | no        | yes       | The title of the topic
  ## | body            | string  | no        | yes       | The unescaped body of the topic
  ## | topic_type      | string  | yes       | no        | The type of topic. Either "articles", "ideas" or "questions"
  ## | submitter_id    | integer | no        | no        | The id of the user who submitted the topic
  ## | updater_id      | integer | no        | no        | The id of the person to last update the topic
  ## | forum_id        | integer | no        | no        | Forum that the topic is associated to
  ## | locked          | boolean | no        | no        | Whether comments are allowed
  ## | pinned          | boolean | no        | no        | If the topic is marked as pinned and hence eligible to show up on the front page
  ## | highlighted     | boolean | no        | no        | Set to true to highlight a topic within its forum
  ## | answered        | boolean | yes       | no        | Set to true if the topic is a question and it has been marked as answered.
  ## | comments_count  | integer | yes       | no        | The number of comments on this topic
  ## | search_phrases  | array   | no        | no        | The search phrases set on the topic
  ## | position        | integer | no        | no        | The position of this topic relative to other topics in the same forum when the topics are ordered manually
  ## | tags            | array   | no        | no        | The tags set on the topic
  ## | created_at      | date    | yes       | no        | The time the topic was created
  ## | updated_at      | date    | yes       | no        | The time of the last update of the topic
  ## | attachments     | array   | yes       | no        | The attachments on this comment as [Attachment](./attachments) objects
  ## | uploads         | array   | no        | no        | List of upload tokens for adding attachments
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "url":             "https://company.zendesk.com/api/v2/topics/35436.json",
  ##   "title":           "How to Disassemble the ED209",
  ##   "body":            "Carefully with very large pliers",
  ##   "topic_type":      "articles",
  ##   "submitter_id":    116,
  ##   "updater_id":      116,
  ##   "forum_id":        1239,
  ##   "locked":          true,
  ##   "pinned":          false,
  ##   "locked":          true,
  ##   "position":        1,
  ##   "tags":            ["danger"]
  ##   "search_phrases":  ["red", "yellow"],
  ##   "created_at":      "2009-07-20T22:55:29Z",
  ##   "updated_at":      "2011-05-05T10:38:52Z"
  ## }
  ## ```
  def model_json(topic)
    return unless valid_topic?(topic)

    json = {
      id: topic.id,
      title: topic.title,
      body: topic.body,
      topic_type: topic.forum_type,
      submitter_id: topic.submitter_id,
      updater_id: topic.updater_id,
      forum_id: topic.forum_id,
      locked: topic.locked,
      pinned: topic.pinned,
      highlighted: topic.highlighted,
      position: topic.position,
      tags: topic.current_tags.to_s.split(" "),
      attachments: attachment_presenter.collection_presenter.model_json(topic.attachments),
      comments_count: topic.posts_count,
      created_at: topic.created_at,
      updated_at: topic.updated_at,
      search_phrases: topic.search_phrases
    }

    if topic.forum.display_type.questions?
      json[:answered] = topic.answered?
    end

    super.merge!(json)
  end

  def side_loads(topics)
    json = {}

    topics = Array(topics)

    side_load_users(json, topics) if side_load?(:users)
    side_load_forums(json, topics) if side_load?(:forums)

    json
  end

  def side_load_forums(json, topics)
    json[:forums] = side_load_association(topics, :forum, forum_presenter)
  end

  def side_load_users(json, topics)
    json[:users] = side_load_association(topics, :submitter, user_presenter)
  end

  def association_preloads
    { attachments: {}, forum: {}, entry_search_phrases: {} }
  end

  protected

  def valid_topic?(topic)
    return true if topic && topic.forum
    Forum.with_deleted { topic.reload.soft_delete! }
    false
  end
end
