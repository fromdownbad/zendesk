class Api::V2::SchedulePresenter < Api::V2::Presenter
  self.model_key = :schedule

  ## ### JSON Format
  ## Schedules are represented as JSON objects which have the following keys:
  ##
  ## | Name             | Type          | Read-only | Mandatory | Comment
  ## | ---------------- | ------------- | --------- | --------- | -------
  ## | url              | string        | yes       | no        | The API url of this schedule
  ## | id               | integer       | yes       | no        | Automatically assigned
  ## | name             | string        | no        | no        | The name of the schedule
  ## | time_zone        | string        | no        | yes       | The time zone of the schedule
  ## | created_at       | date          | yes       | no        | The time the schedule was created
  ## | updated_at       | date          | yes       | no        | The time of the last update of the schedule
  ## | deleted_at       | date          | yes       | no        | The time the schedule was deleted
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         47,
  ##   "url":        "https://company.zendesk.com/api/v2/business_hours/schedules/47.json",
  ##   "name":       "San Francisco Office",
  ##   "time_zone":  "Pacific Time (US & Canada)",
  ##   "created_at": "2012-04-02t22:55:29z",
  ##   "updated_at": "2012-04-04t22:55:29z",
  ##   "deleted_at": "2012-04-04t22:55:29z"
  ## }
  ## ```
  def model_json(schedule)
    super.merge!(
      id: schedule.id,
      name: schedule.name,
      time_zone: schedule.time_zone,
      created_at: schedule.created_at,
      updated_at: schedule.updated_at,
      deleted_at: schedule.deleted_at
    )
  end

  def url(schedule)
    url_builder.api_v2_business_hours_schedule_url(schedule, format: :json)
  end
end
