class Api::V2::Presenter < Api::Presentation::Presenter
  class_attribute :url_param, :singular_resource

  def collection_presenter
    @collection_presenter ||= begin
      klass = if options[:with_cursor_pagination] == true
        Api::V2::CursorCollectionPresenter
      else
        Api::V2::CollectionPresenter
      end
      klass.new(user, options.merge(item_presenter: self, includes: includes))
    end
  end

  def user_presenter
    @user_presenter ||= options[:user_presenter] ||= Api::V2::Users::Presenter.new(user, options)
  end

  def user_abilities_presenter
    # rubocop:disable Style/ConditionalAssignment
    if account.has_voice_read_user_seats_from_the_db?
      @user_abilities_presenter ||= options[:user_abilities_presenter] ||= Api::V2::Abilities::UserPresenter.new(user, {account: account}.merge(options))
    else
      @user_abilities_presenter ||= options[:user_abilities_presenter] ||= Api::V2::Abilities::UserPresenter.new(user, options)
    end
    # rubocop:enable Style/ConditionalAssignment
  end

  def user_permissions_presenter
    @user_permissions_presenter ||= options[:user_permissions_presenter] ||= Api::V2::Permissions::PermissionsPresenter.new(user, options)
  end

  def group_presenter
    @group_presenter ||= options[:group_presenter] ||= Api::V2::GroupPresenter.new(user, options)
  end

  def organization_presenter
    @organization_presenter ||= options[:organization_presenter] ||= Api::V2::Organizations::Presenter.new(user, options)
  end

  def ticket_presenter
    @ticket_presenter ||= options[:ticket_presenter] ||= Api::V2::Tickets::TicketPresenter.new(user, options)
  end

  def attachment_presenter
    @attachment_presenter ||= options[:attachment_presenter] ||= Api::V2::AttachmentPresenter.new(user, options)
  end

  def screencast_presenter
    @screencast_presenter ||= options[:screencast_presenter] ||= Api::V2::ScreencastPresenter.new(user, options)
  end

  def forum_presenter
    @forum_presenter ||= options[:forum_presenter] ||= Api::V2::ForumPresenter.new(user, options)
  end

  def topic_presenter
    @topic_presenter ||= options[:topic_presenter] ||= Api::V2::TopicPresenter.new(user, options)
  end

  def ticket_form_presenter
    @ticket_form_presenter ||= options[:ticket_form_presenter] ||= Api::V2::TicketFormPresenter.new(user, options)
  end

  def sharing_agreement_presenter
    @sharing_agreement_presenter ||= options[:sharing_agreement_presenter] ||= Api::V2::SharingAgreementPresenter.new(user, options)
  end

  def brand_presenter
    @brand_presenter ||= options[:brand_presenter] ||= Api::V2::BrandPresenter.new(user, options)
  end

  def schedule_presenter
    @schedule_presenter ||=
      options[:schedule_presenter] ||=
        Api::V2::SchedulePresenter.new(user, options.merge(url_builder: url_builder.zendesk_business_hours))
  end

  def account_presenter
    @account_presenter ||= options[:account_presenter] ||= Api::V2::AccountPresenter.new(user, options)
  end

  def macro_presenter
    @macro_presenter ||= options[:macro_presenter] ||= Api::V2::Rules::MacroPresenter.new(user, options)
  end

  def ticket_field_presenter
    @ticket_field_presenter ||= options[:ticket_field_presenter] ||= Api::V2::TicketFieldPresenter.new(user, options)
  end

  def assignable_group_presenter
    @assignable_group_presenter ||= options[:assignable_group_presenter] ||= Api::Mobile::Account::GroupPresenter.new(user, options)
  end

  def data_deletion_audit_job_presenter
    @data_deletion_audit_job_presenter ||= Api::V2::Internal::DataDeletionAuditJobPresenter.new(user, options)
  end

  def satisfaction_reason_presenter
    @satisfaction_reason_presenter ||=
      options[:satisfaction_reason_presenter] ||=
        Api::V2::SatisfactionReasonPresenter.new(user, options)
  end

  def ticket_field_condition_presenter
    @ticket_field_condition_presenter ||= options[:ticket_field_condition_presenter] ||= Api::V2::TicketFieldConditionPresenter.new(user, options)
  end

  def custom_status_presenter
    @custom_status_presenter ||= options[:custom_status_presenter] ||= Api::V2::CustomStatusPresenter.new(user, options)
  end

  def present_with(klass, account)
    klass.new(user, @options).model_json(account)
  end

  def present_errors(model)
    Api::V2::ErrorsPresenter.new.present(model)
  end

  def url(model)
    if singular_resource
      url_builder.send("api_v2_#{model_key}_url", format: :json)
    else
      param = (url_param ? model.send(url_param) : model)
      url_builder.send("api_v2_#{model_key}_url", param, format: :json)
    end
  end

  def render_dynamic_content(text, options = {})
    rendered = if options[:ticket]
      # Use :user_is_agent if you already know the value when calling #render_dynamic_content
      user_is_agent = options.fetch(:user_is_agent, user.is_agent?)
      Time.use_zone(user.time_zone) do
        render_options = {
          account: account,
          dc_cache: options[:dc_cache] || dc_cache,
          custom_fields_cache: options[:custom_fields_cache],
        }

        Zendesk::Liquid::TicketContext.render(text, options[:ticket], user, user_is_agent, locale, nil, render_options)
      end
    else
      Zendesk::Liquid::DcContext.render(text, account, user, 'text/plain', locale, dc_cache)
    end

    rendered.blank? ? text : rendered
  end

  private

  def dc_cache
    @dc_cache ||= if locale == user.translation_locale
      user.dc_cache
    else
      Zendesk::DynamicContent::AccountContent.cache(account, locale)
    end
  end

  def locale
    @options[:locale] || I18n.translation_locale
  end
end
