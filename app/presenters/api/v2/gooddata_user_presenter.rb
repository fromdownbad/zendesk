class Api::V2::GooddataUserPresenter < Api::V2::Presenter
  self.model_key = :gooddata_user
  attr_reader :last_successful_process

  def initialize(user, options = {})
    @last_successful_process = options[:last_successful_process]
    super
  end

  ## ### JSON Format
  ## Gooddata users are represented as JSON objects which have the following keys:
  ##
  ## | Name                | Type    | Read-only | Mandatory | Comment
  ## | ------------------- | ------- | --------- | --------- | -------
  ## | id                  | number  | yes       | no        | The Zendesk ID of the GooddataUser
  ## | account_id          | number  | yes       | no        | The ID of the Zendesk account
  ## | user_id             | number  | yes       | no        | The id of the Zendesk user
  ## | gooddata_user_id    | string  | no        | no        | The ID of the user in GoodData
  ## | gooddata_project_id | string  | no        | no        | The ID of the relevant GoodData project
  ## | created_at          | date    | yes       | no        | The time GooddataUser was created
  ## | updated_at          | date    | yes       | no        | The time GooddataUser was updated
  ## | url                 | string  | yes       | yes       | The URL to this information
  ## | integration         | object  | yes       | yes       | Information about the current integration state with GoodData
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "gooddata_user": {
  ##     "id": 0,
  ##     "account_id": 1,
  ##     "user_id": 6,
  ##     "gooddata_user_id": "SAMPLE_USER_ID",
  ##     "gooddata_project_id": "SAMPLE_PROJECT_ID",
  ##     "created_at": "2014-02-20T08:21:20Z",
  ##     "updated_at": "2014-02-20T08:21:20Z",
  ##     "url": "https:\/\/support.zendesk.dev\/api\/v2\/gooddata_user"
  ##     "integration": {
  ##       "state": "v1"
  ##     },
  ##   }
  ## }
  ## ```

  def model_json(gooddata_user)
    integration_status, integration_data = integration_status_info

    {
      integration: {state: integration_status}.tap do |h|
        h.merge!(integration_data) if integration_data

        unless integration_status == :v1
          h.merge!(tour_project_sso_url: GooddataIntegration.tour_project_sso_url)
        end
      end
    }.tap do |result|
      if gooddata_user.present?
        result.merge!(
          id: gooddata_user.id,
          account_id: gooddata_user.account_id,
          user_id: gooddata_user.user_id,
          gooddata_user_id: gooddata_user.gooddata_user_id,
          gooddata_project_id: gooddata_user.gooddata_project_id,
          created_at: gooddata_user.created_at,
          updated_at: gooddata_user.updated_at
        )
      end
    end.merge!(super)
  end

  def url(_model)
    # Our superclass includes the model in the call to url_builder, which includes the
    # ID in the URL.  We're a singleton, so we don't want to include the ID.
    url_builder.send("api_v2_#{model_key}_url", nil, format: :json)
  end

 # Returns the current GoodData integration status for the supplied user
  # as a status or an array like [status, status_data], where status is one of:
  #   :fully_provisioned
  #   :admin_user_not_provisioned
  #   :user_not_provisioned
  #   :account_integration_in_progress
  #   :admin_account_not_integrated
  #   :account_not_integrated
  #   :admin_account_integration_unsupported
  #   :account_integration_unsupported
  #   :account_data_load_in_progress
  #   :account_integration_disabled
  # and status_data is data that's specific to the status
  def integration_status_info
    return :account_integration_unsupported unless account.is_serviceable?

    if gooddata_integration.present?
      v2_integration_status
    else
      missing_integration_status
    end
  end

  protected

  def v2_integration_status
    if gooddata_integration.complete?
      if last_successful_process.present?
        user_status
      else
        :account_data_load_in_progress
      end
    elsif gooddata_integration.disabled?
      :account_integration_disabled
    else
      [:account_integration_in_progress, { account_integration_status: gooddata_integration.status }]
    end
  end

  def user_status
    gooddata_user = GooddataUser.for_user(@user)
    if gooddata_user.present?
      [
        :fully_provisioned,
        {
          dashboards: gooddata_integration.sso_dashboards(gooddata_user),
          portal_sso_url: gooddata_integration.portal_sso_url(gooddata_user),
          scheduled_at: gooddata_integration.scheduled_at
        }
      ]
    else
      if @user.id == gooddata_integration.admin_id
        :admin_user_not_provisioned
      else
        :user_not_provisioned
      end
    end
  end

  def missing_integration_status
    if account.has_gooddata_advanced_analytics?
      # Account is Plus or Enterprise and can be integrated with GoodData
      if @user.is_admin?
        :admin_account_not_integrated
      else
        :account_not_integrated
      end
    else
      # Account is not Plus or Enterprise, admin must upgrade before integrating
      if @user.is_admin?
        :admin_account_integration_unsupported
      else
        :account_integration_unsupported
      end
    end
  end

  def gooddata_integration
    @gooddata_integration ||= @user.account.gooddata_integration
  end
end
