class Api::V2::AttachmentPresenter < Api::V2::Presenter
  self.model_key = :attachment

  def collection_presenter
    @collection_presenter ||= Api::V2::AttachmentCollectionPresenter.new(user, options.merge(item_presenter: self, includes: includes))
  end

  ## ### JSON Format
  ## Attachments are represented as JSON objects with the following keys:
  ##
  ## | Name         | Type             | Read-only | Comment
  ## | ------------ | ---------------- | --------- | -------
  ## | id           | integer          | yes       | Automatically assigned when created
  ## | file_name    | string           | yes       | The name of the image file
  ## | content_url  | string           | yes       | A full URL where the attachment image file can be downloaded
  ## | content_type | string           | yes       | The content type of the image. Example value: `image/png`
  ## | size         | integer          | yes       | The size of the image file in bytes
  ## | thumbnails   | array            | yes       | An array of [Photo](#attachments) objects. Note that thumbnails do not have thumbnails.
  ## | inline       | boolean          | yes       | If true, the attachment is excluded from the attachment list and the attachment's URL can be referenced within the comment of a ticket. Default is `false`
  ## | deleted      | boolean          | yes       | If true, the attachment has been deleted.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":           928374,
  ##   "file_name":    "my_funny_profile_pic.png",
  ##   "content_url":  "https://company.zendesk.com/attachments/my_funny_profile_pic.png",
  ##   "content_type": "image/png",
  ##   "size":         166144,
  ##   "thumbnails": [
  ##     {
  ##       "id":           928375,
  ##       "file_name":    "my_funny_profile_pic_thumb.png",
  ##       "content_url":  "https://company.zendesk.com/attachments/my_funny_profile_pic_thumb.png",
  ##       "content_type": "image/png",
  ##       "size":         58298
  ##     }
  ##   ]
  ## }
  ## ```
  def model_json(attachment, include_thumbnails = true)
    json = super(attachment).merge!(
      id: attachment.id,
      file_name: attachment.display_filename,
      content_url: content_url(attachment, mapped: false, ssl: true),
      mapped_content_url: content_url(attachment),
      content_type: attachment.content_type,
      size: attachment.size,
      width: attachment.width,
      height: attachment.height,
      inline: inline_attachment?(attachment),
      deleted: attachment.stores.empty? && !attachment.not_saved_in_stores?
    )
    json[:thumbnails] = thumbnails(attachment) if include_thumbnails

    json
  end

  def thumbnails(attachment)
    attachment.thumbnails.map do |thumbnail|
      model_json(thumbnail, false)
    end
  end

  def association_preloads
    { thumbnails: {} }
  end

  private

  def inline_attachment?(attachment)
    attachment.is_a?(Attachment) ? attachment.inline : false
  end

  def content_url(attachment, options = {})
    ContentUrlBuilder.new.url_for(attachment, options)
  end
end
