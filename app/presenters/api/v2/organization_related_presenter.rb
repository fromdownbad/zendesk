class Api::V2::OrganizationRelatedPresenter < Api::V2::Presenter
  self.model_key = :organization_related

  attr_reader :organization

  COUNT_WITH_ARCHIVED_LIMIT = 100_000

  ## ### JSON Format
  ##
  ## | Name              | Type    | Read-only | Mandatory | Comment
  ## | ----------------- | ------- | --------- | --------- | -------
  ## | users_count       | integer | yes       | no        | Count of users in organization
  ## | tickets_count     | integer | yes       | no        | Count of tickets in organization
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "organization_related": {
  ##     "users_count": 4,
  ##     "tickets_count": 12
  ##   }
  ## }
  ## ```
  def model_json(organization)
    @organization = organization

    users_count, tickets_count = if account.has_enable_search_for_org_related? && organization_counts_from_search.any?
      [organization_counts_from_search['user'], organization_counts_from_search['ticket']]
    else
      # count_with_archived does a count from tickets and a count from ticket_archive_stubs,
      # passing the limit on to each query. The sum of both counts is then returned,
      # so the returned value can be up to twice the limit we pass in.
      # In order to rationalize the count a bit, just return whichever is smaller between
      # the return value and our limit
      [organization.users.count(:all), [organization.tickets.count_with_archived(limit: COUNT_WITH_ARCHIVED_LIMIT), COUNT_WITH_ARCHIVED_LIMIT].min]
    end

    {
      users_count: users_count,
      tickets_count: tickets_count
    }
  end

  private

  def organization_counts_from_search
    @organization_counts_from_search ||= begin
      # If successful, the query returns results in the form of {count: 0, results: [], facets: {type: {ticket: 0, user: 0}}}
      organization_query.execute(user)

      if organization_query.success?
        organization_query.result.facets['type']
      else
        Rails.logger.error "Could not fetch organization counts from search for account:#{account.id} organization:#{organization.id}"

        {}
      end
    end
  end

  def organization_query
    @query ||= Zendesk::Search::Query.new("organization_id:#{organization.id}", account: account).tap do |query|
      query.per_page = 1
      query.facets = true
      query.incremental = true
    end
  end
end
