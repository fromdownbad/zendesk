class Api::V2::Tickets::CreatePresenter < Api::V2::Tickets::AuditEventPresenter
  include Api::V2::Tickets::AttributeMappings

  ## #### Create event
  ## A ticket property was set on a newly created ticket. A separate event is created for each property set.
  ##
  ## Create events have the following keys:
  ##
  ## | Name       | Type                  | Read-only | Comment
  ## | ---------- | --------------------- | --------- | -------
  ## | id         | integer               | yes       | Automatically assigned when the event is created
  ## | type       | string                | yes       | Has the value `Create`
  ## | field_name | string                | yes       | The name of the field that was set
  ## | value      | string, array, object | yes       | The value of the field that was set
  ##
  ## `value` is normally a string. However, the attribute is an array when the
  ## value of `field_name` is `tags`. It's an object when the value of
  ## `field_name` is a SLA event like "first_reply_time". Example:
  ##
  ## ```javascript
  ## "value": {
  ##   "minutes": 1440,
  ##   "in_business_hours":false
  ## }
  ## ```
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1274,
  ##   "type":       "Create"
  ##   "field_name": "status",
  ##   "value":      "new"
  ## }
  ## ```
  def model_json(create_event)
    if create_event.field_name == "linked_id"
      super.merge(
        type: "Create",
        value: problem_ticket_nice_id(create_event).try(:to_s),
        field_name: "problem_id"
      )
    else
      super.merge(
        type: "Create",
        value: mapped_value(create_event),
        field_name: mapped_field_name(create_event)
      )
    end
  end

  def problem_ticket_nice_id(create_event, attribute = :value)
    ticket_id = create_event.send(attribute)
    return nil if ticket_id.blank?
    ticket_id = ticket_id.to_i

    ticket = if options[:problem_ticket_associations].present?
      # options[:problem_ticket_associations] is prepared by AuditEventPresenter#preload_problem_ticket_associations
      options[:problem_ticket_associations][ticket_id]
    else
      Ticket.with_deleted { account.tickets.find_by_id(ticket_id) }
    end

    ticket.try(:nice_id)
  end

  def mapped_value(create_event, attribute = :value)
    return nil unless create_event.field_name

    if ATTRIBUTE_MAPPINGS.key?(create_event.field_name.to_sym)
      ticket_attribute_value(
        create_event.field_name,
        create_event.send(attribute),
        account
      )
    elsif create_event.field_name == 'current_tags'
      (create_event.send(attribute) || '').split
    else
      create_event.send(attribute)
    end
  end

  def mapped_field_name(create_event)
    if create_event.field_name
      if create_event.field_name == 'current_tags'
        'tags'
      else
        ticket_attribute_name(create_event.field_name).to_s
      end
    end
  end
end
