class Api::V2::Tickets::RuleRevisionViaPresenter
  def initialize(user)
    @user = user
  end

  def model_json(revision)
    return {} unless revision.present?

    {
      from: from_source(revision),
      rel:  revision.trigger.try(:type).try(:downcase)
    }
  end

  private

  attr_reader :user

  def from_source(revision)
    rule = revision.trigger

    {deleted: deleted?(rule), title: title(rule)}.tap do |json|
      if user.can?(:view, rule || Trigger)
        json[:id]          = revision.rule_id
        json[:revision_id] = revision.nice_id if rule_revisions?
      end
    end
  end

  def title(rule)
    unless rule.present?
      return I18n.t('txt.api.v2.rule_source.hard_deleted_reference')
    end

    if rule.deleted?
      return I18n.t(
        'txt.api.v2.rule_source.soft_deleted_reference',
        title: rule.title
      )
    end

    rule.title
  end

  def deleted?(rule)
    rule.nil? || rule.deleted?
  end

  def rule_revisions?
    user.account.has_trigger_revision_history?
  end
end
