module Api::V2::Tickets::TicketFieldHelper
  def ticket_field_entries(ticket, scope, salesforce: false)
    if salesforce
      return ticket_field_entries_for_salesforce(ticket, scope)
    end

    ticket_field_entries_by_ticket_field_id = ticket.ticket_field_entries.index_by(&:ticket_field_id)
    scope.map do |ticket_field|
      entry = ticket_field_entries_by_ticket_field_id[ticket_field.id]

      if entry
        value = ticket_field_entry_value(entry, ticket_field)
      end

      { id: ticket_field.id, value: value }
    end
  end

  # Adds the ticket field title to the standard custom field attributes
  def ticket_field_entries_for_salesforce(ticket, _scope)
    ticket.ticket_field_entries.map do |ticket_field_entry|
      { id: ticket_field_entry.ticket_field_id,
        title: ticket_field_entry.ticket_field.title,
        value: ticket_field_entry.value }
    end
  end

  def ticket_field_entry_value(entry, ticket_field)
    normalize_ticket_field_value(entry.value, ticket_field)
  end

  def raw_ticket_field_entries(entries, scope)
    scope.map do |ticket_field|
      entry = entries[ticket_field.id]

      if entry
        value = normalize_ticket_field_value(entry[:value], ticket_field)
      end

      { id: ticket_field.id, value: value }
    end
  end

  def scrubbed_ticket_field_entries(ticket, scope)
    ticket_field_entries_by_ticket_field_id = ticket.ticket_field_entries.index_by(&:ticket_field_id)

    scope.map do |ticket_field|
      entry = ticket_field_entries_by_ticket_field_id[ticket_field.id]

      value = if entry
        case ticket_field_entry_value(entry, ticket_field).class
        when Hash then { 'X' => 'X' }
        when Array then ['X']
        else 'X'
        end
      end

      { id: ticket_field.id, value: value }
    end
  end

  def ticket_field_type(ticket_field)
    type = ticket_field.type.gsub(/^Field/, "").downcase
    type = "basic_priority" if type == "priority" && ticket_field.sub_type_id == PrioritySet.BASIC
    type
  end

  private

  def normalize_ticket_field_value(value, ticket_field)
    if ticket_field.is_a?(FieldCheckbox)
      Zendesk::DB::Util.truthy?(value)
    elsif ticket_field.is_a?(FieldMultiselect)
      value.present? ? value.to_s.split(" ") : nil
    else
      value
    end
  end
end
