# WORKING ON SIDELOADS:
#
# When adding or updating a sideload, always create unit and functional/integration tests.
# They need to make sure that you don't introduce n+1 queries.
# Also look at the output of your SQL in the log/test.log when running them (tips: isolate
# a specific test by running testrbl path:123 where 123 is a line inside your test case).
# In doubt, ask on #classic-dev on Slack.
# One rule is that if you're calling a Rails association inside a loop (ie.), you need
# to change it to fetch the data before presenting your model.
# See how we handle comments, ticket_forms or archived_tickets.
#
require 'zendesk/sla'
require 'set'

class Api::V2::Tickets::TicketPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings
  include Api::V2::Tickets::EventViaPresenter
  include Api::V2::Tickets::TicketFieldHelper

  self.model_key = :ticket

  def initialize(user, options = {})
    super

    @reduced_payload_size = options[:reduced_payload_size]
    @custom_fields = custom_fields(options)
    @custom_fields_cache = @custom_fields.each_with_object({}) { |cf, cache| cache[cf.id] = cf }
    @audit_count_since = options[:audit_count_since] if options[:audit_count_since]
    @archive_problem_tickets = {}
    @raw_ticket_field_entries = {}
    @deleted_ticket_forms = []
    @archived_tickets = {}
    @collaborators_from_archived_tickets = {}
    @sharing_agreement_ids = {}
    @follower_and_email_cc_collaborations_enabled = false
    @comment_email_ccs_allowed = false
    @ticket_followers_allowed = false
    @last_audits_side_load = nil
    @comment_counts = {}
    @ticket_incident_counts = {}
  end

  ## ### JSON Format
  ## Tickets are represented as JSON objects with the following properties:
  ##
  ## | Name                  | Type                                     | Read-only | Mandatory | Comment
  ## | --------------------- | ---------------------------------------- | --------- | --------- | -------
  ## | id                    | integer                                  | yes       | no        | Automatically assigned when the ticket is created
  ## | url                   | string                                   | yes       | no        | The API url of this ticket
  ## | external_id           | string                                   | no        | no        | An id you can use to link Zendesk Support tickets to local records
  ## | type                  | string                                   | no        | no        | The type of this ticket. Possible values: "problem", "incident", "question" or "task"
  ## | subject               | string                                   | no        | no        | The value of the subject field for this ticket
  ## | raw_subject           | string                                   | no        | no        | The dynamic content placeholder, if present, or the "subject" value, if not. See [Dynamic Content](dynamic_content.html)
  ## | description           | string                                   | yes       | no        | Read-only first comment on the ticket. When [creating a ticket](#create-ticket), use `comment` to set the description. See [Description and first comment](#description-and-first-comment)
  ## | priority              | string                                   | no        | no        | The urgency with which the ticket should be addressed. Possible values: "urgent", "high", "normal", "low"
  ## | status                | string                                   | no        | no        | The state of the ticket. Possible values: "new", "open", "pending", "hold", "solved", "closed"
  ## | recipient             | string                                   | no        | no        | The original recipient e-mail address of the ticket
  ## | requester_id          | integer                                  | no        | yes       | The user who requested this ticket
  ## | submitter_id          | integer                                  | no        | no        | The user who submitted the ticket. The submitter always becomes the author of the first comment on the ticket
  ## | assignee_id           | integer                                  | no        | no        | The agent currently assigned to the ticket
  ## | organization_id       | integer                                  | no        | no        | The organization of the requester. You can only specify the ID of an organization associated with the requester. See [Organization Memberships](./organization_memberships)
  ## | group_id              | integer                                  | no        | no        | The group this ticket is assigned to
  ## | collaborator_ids      | array                                    | no        | no        | The ids of users currently CC'ed on the ticket
  ## | collaborators         | array                                    | no        | no        | POST requests only. Users to add as cc's when creating a ticket. See [Setting Collaborators](#setting-collaborators)
  ## | email_cc_ids          | array                                    | no        | no        | The ids of agents or end users currently CC'ed on the ticket. See [CCs and followers resources](https://support.zendesk.com/hc/en-us/articles/360020585233) in the Support Help Center
  ## | follower_ids          | array                                    | no        | no        | The ids of agents currently following the ticket. See [CCs and followers resources](https://support.zendesk.com/hc/en-us/articles/360020585233)
  ## | forum_topic_id        | integer                                  | yes       | no        | The topic in the Zendesk Web portal this ticket originated from, if any. The Web portal is deprecated
  ## | problem_id            | integer                                  | no        | no        | For tickets of type "incident", the ID of the problem the incident is linked to
  ## | has_incidents         | boolean                                  | yes       | no        | Is true if a ticket is a problem type and has one or more incidents linked to it. Otherwise, the value is false.
  ## | due_at                | date                                     | no        | no        | If this is a ticket of type "task" it has a due date.  Due date format uses [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601) format.
  ## | tags                  | array                                    | no        | no        | The array of tags applied to this ticket
  ## | via                   | [Via](ticket_audits.html#the-via-object) | yes       | no        | This object explains how the ticket was created
  ## | custom_fields         | array                                    | no        | no        | Custom fields for the ticket. See [Setting custom field values](#setting-custom-field-values)
  ## | satisfaction_rating   | object                                   | yes       | no        | The satisfaction rating of the ticket, if it exists, or the state of satisfaction, 'offered' or 'unoffered'
  ## | sharing_agreement_ids | array                                    | yes       | no        | The ids of the sharing agreements used for this ticket
  ## | followup_ids          | array                                    | yes       | no        | The ids of the followups created from this ticket. Ids are only visible once the ticket is closed
  ## | via_followup_source_id | integer                                 | no        | no        | POST requests only. The id of a closed ticket when creating a follow-up ticket. See [Creating Follow-up Tickets](#creating-follow-up-tickets)
  ## | macro_ids             | array                                    | no        | no        | POST requests only. List of macro IDs to be recorded in the ticket audit
  ## | ticket_form_id        | integer                                  | no        | no        | Enterprise only. The id of the ticket form to render for the ticket
  ## | brand_id              | integer                                  | no        | no        | Enterprise only. The id of the brand this ticket is associated with
  ## | allow_channelback     | boolean                                  | yes       | no        | Is false if channelback is disabled, true otherwise. Only applicable for channels framework ticket
  ## | allow_attachments     | boolean                                  | yes       | no        | When an agent responds, are they allowed to add attachments?  Defaults to true
  ## | is_public             | boolean                                  | yes       | no        | Is true if any comments are public, false otherwise
  ## | created_at            | date                                     | yes       | no        | When this record was created
  ## | updated_at            | date                                     | yes       | no        | When this record last got updated
  ##
  ## You can also include a `comment_count` property in the JSON objects returned by GET requests by sideloading it. Example:
  ##
  ## `GET /api/v2/tickets.json?include=comment_count`
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":               35436,
  ##   "url":              "https://company.zendesk.com/api/v2/tickets/35436.json",
  ##   "external_id":      "ahg35h3jh",
  ##   "created_at":       "2009-07-20T22:55:29Z",
  ##   "updated_at":       "2011-05-05T10:38:52Z",
  ##   "type":             "incident",
  ##   "subject":          "Help, my printer is on fire!",
  ##   "raw_subject":      "{{dc.printer_on_fire}}",
  ##   "description":      "The fire is very colorful.",
  ##   "priority":         "high",
  ##   "status":           "open",
  ##   "recipient":        "support@company.com",
  ##   "requester_id":     20978392,
  ##   "submitter_id":     76872,
  ##   "assignee_id":      235323,
  ##   "organization_id":  509974,
  ##   "group_id":         98738,
  ##   "collaborator_ids": [35334, 234],
  ##   "follower_ids":     [35334, 234], // This functionally is the same as collaborators for now.
  ##   "problem_id":       9873764,
  ##   "has_incidents":    false,
  ##   "due_at":           null,
  ##   "tags":             ["enterprise", "other_tag"],
  ##   "via": {
  ##     "channel": "web"
  ##   },
  ##   "custom_fields": [
  ##     {
  ##       "id":    27642,
  ##       "value": "745"
  ##     },
  ##     {
  ##       "id":    27648,
  ##       "value": "yes"
  ##     }
  ##   ],
  ##   "satisfaction_rating": {
  ##     "id": 1234,
  ##     "score": "good",
  ##     "comment": "Great support!"
  ##   },
  ##   "sharing_agreement_ids": [84432]
  ## }
  ## ```
  def model_json(ticket)
    if archived_ticket = @archived_tickets[ticket.id]
      ticket = archived_ticket
    end

    # If we have the entries here, build a cache of them to prevent executing Array#find for every ticket field; however,
    # we don't want reset the cache if it has already been set by another path like `preload_raw_ticket_field_entries`.
    if ticket.association(:ticket_field_entries).loaded?
      ticket.ticket_field_entries_by_ticket_field_id_cache ||= ticket.ticket_field_entries.each_with_object({}) do |tfe, acc|
        acc[tfe.ticket_field_id] = tfe
      end
    end

    custom_fields = if ticket.scrubbed? && account.has_prevent_deletion_if_churned?
      scrubbed_ticket_field_entries(ticket, @custom_fields)
    elsif options[:use_raw_ticket_field_entries]
      raw_ticket_field_entries(@raw_ticket_field_entries[ticket.id] || {}, @custom_fields)
    else
      ticket_field_entries(ticket, @custom_fields, salesforce: options[:use_salesforce_custom_fields])
    end

    if reduced_payload_size? && custom_fields.present?
      custom_fields = custom_fields.reject { |f| f[:value].nil? }
    end

    json = super.merge!(
      id:               ticket.nice_id,
      external_id:      ticket.external_id,
      via:              via_export(ticket),
      created_at:       ticket.created_at,
      updated_at:       ticket.updated_at,
      type:             TYPE_MAP[ticket.ticket_type_id.to_s],
      subject:          replace_dc_if_agent(ticket, ticket.subject, @custom_fields_cache),
      raw_subject:      ticket.subject,
      description:      description(ticket),
      priority:         PRIORITY_MAP[ticket.priority_id.to_s],
      status:           STATUS_MAP[ticket.status_id.to_s],
      recipient:        ticket.original_recipient_address || ticket.recipient,
      requester_id:     ticket.requester_id,
      submitter_id:     ticket.submitter_id,
      assignee_id:      ticket.assignee_id,
      organization_id:  ticket.organization_id,
      group_id:         ticket.group_id,
      collaborator_ids: collaborator_ids(ticket),
      follower_ids:     follower_ids(ticket),
      email_cc_ids:     email_cc_ids(ticket),
      forum_topic_id:   ticket.entry_id,
      problem_id:       problem_id(ticket),
      has_incidents:    (@ticket_incident_counts[ticket.id] || 0) > 0,
      is_public:        ticket.is_public,
      due_at:           ticket.due_date,
      tags:             TagManagement.normalize_tags(ticket.current_tags, account),
      custom_fields:    custom_fields,
      satisfaction_rating: satisfaction_rating_presenter.present_for_ticket(ticket),
      sharing_agreement_ids: sharing_agreement_ids(ticket)
    )

    if account.has_custom_statuses? && ticket.respond_to?(:custom_status_id)
      json[:custom_status_id] = ticket.custom_status_id
    end

    if side_load?(:comment_count)
      json[:comment_count] = if ticket.archived?
        # No SQL executed, already in memory
        ticket.comments.size
      else
        # 1. Try the preload_comment_counts hash
        # 2. Execute an individual SELECT COUNT(*)
        comment_count = @comment_counts[ticket.id] || ticket.comments.count

        # Temporary discovery for consumers not calling `preload_associations`
        if @comment_counts[ticket.id].nil?
          Rails.logger.warn "comment_count preload not present for #{ticket.id}"
        end

        # Temporary parity check. I can be removed after investigation.
        # Previous changes indicated a mismatch between in-memory loaded
        # comments, and the COUNT(*) from the database.
        if ticket.comments.loaded? && ticket.comments.size != comment_count
          Rails.logger.info(
            "comment_count parity mismatch on #{ticket.id}, size: #{ticket.comments.size}, count: #{comment_count}"
          )
        end

        comment_count
      end
    end

    # TODO: EOL legacy 'fields' key.
    # Keep only 'custom_fields' which is the one we mention in documentation.
    # New endpoints that present tickets should exclude it to make EOL process easier.
    unless reduced_payload_size? || options[:exclude_legacy_fields]
      json[:fields] = json[:custom_fields]
    end

    if side_load?(:permissions)
      json[:permissions] = permissions(ticket)
    end

    if side_load?(:public_updated_at)
      json[:public_updated_at] = ticket.public_updated_at
    end

    json[:followup_ids] = if ticket.closed?
      followup_ids(ticket)
    else
      []
    end

    add_ticket_form_id_and_deleted_form_id(json, ticket)

    json[:brand_id] = ticket.brand_id

    if account.has_satisfaction_prediction?
      json[:satisfaction_probability] = ticket.satisfaction_probability
    end

    if side_load?(:metric_sets)
      if ticket.ticket_metric_set
        # prevent ticket_metric_set trying to load the ticket that is already loaded
        ticket.ticket_metric_set.ticket = ticket
        json[:metric_set] = metric_set_presenter.model_json(ticket.ticket_metric_set)
      else
        json[:metric_set] = nil
      end
    end

    if side_load?(:last_audits)
      json[:last_audit] = if preload_audit_events?
        last_audits_side_load(Array(ticket)).try(:first)
      else
        audit_presenter.model_json(latest_audit(ticket))
      end
    end

    if side_load?(:incident_counts) && ticket.ticket_type?(:problem)
      json[:incident_count] = @ticket_incident_counts[ticket.id] || 0
    end

    if account.has_service_level_agreements? && side_load?(:slas)
      ZendeskAPM.trace(
        'ticket_presenter.slas',
        service: Zendesk::Sla::APM_SERVICE_NAME
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.set_tag('zendesk.account_id', account.id)
        span.set_tag('zendesk.account_subdomain', account.subdomain)
        span.set_tag('zendesk.ticket.id', ticket.id)

        json[:slas] = {policy_metrics: ticket_sla_metrics_json(ticket)}
      end
    end

    if account.has_service_level_agreements? && side_load?(:sla_policy)
      json[:sla_policy] = if ticket.sla_ticket_policy.present?
        sla_policy_presenter.model_json(
          ticket.sla_ticket_policy.policy
        )
      end
    end

    if side_load?(:metric_events)
      json[:metric_events] = begin
        ticket.metric_events.
          to_a.
          sort_by { |metric_event| [metric_event.instance_id, metric_event.time, metric_event.precedence] }.
          map(&metric_event_presenter.method(:model_json)).
          group_by { |metric_event| metric_event[:metric].to_sym }
      end
    end

    if side_load?(:dates)
      json[:dates] = {
        assignee_updated_at:     ticket.assignee_updated_at,
        requester_updated_at:    ticket.requester_updated_at,
        status_updated_at:       ticket.status_updated_at,
        initially_assigned_at:   ticket.initially_assigned_at,
        assigned_at:             ticket.assigned_at,
        solved_at:               ticket.solved_at,
        latest_comment_added_at: ticket.latest_comment_added_at
      }
    end

    if @audit_count_since
      json[:audit_count_since] = audit_count_since(ticket, @audit_count_since)
    end

    if side_load?(:first_comment)
      comment = ticket.first_comment
      presenter = comment_presenter_for(comment)
      json[:first_comment] = presenter.model_json(comment)
    end

    if side_load?(:last_comment)
      last_comment = last_comment(ticket)

      if last_comment != ticket.first_comment
        presenter = comment_presenter_for(last_comment)
        json[:last_comment] = presenter.model_json(last_comment)
      end
    end

    json[:allow_channelback] = ticket.via_channel? ? Channels::TicketPresenterHelper.allow_channelback?(ticket) : false

    json[:allow_attachments] = ticket.via_channel? ? Channels::TicketPresenterHelper.allow_attachments?(ticket) : true

    if side_load?(:reply_options) && Arturo.feature_enabled_for?(:reply_options, account)
      json[:reply_options] = reply_options_presenter.model_json(ticket)
    end

    if side_load?(:tde_workspace) &&
       !account.has_contextual_workspaces_in_js? &&
       account.has_contextual_workspaces? &&
       ticket.workspace
      json[:tde_workspace] = workspace_presenter.model_json(ticket.workspace)
    end

    if Arturo.feature_enabled_for?(:agent_as_end_user, account)
      json[:is_aeu] = ticket.agent_as_end_user?
      if json[:is_aeu]
        json[:aeu_agent_name] = ticket.submitter.name
      end
    end

    json
  end

  def side_loads(ticket)
    json = {}

    tickets = Array(ticket)

    # This is the collection that `preload_associations` handled.
    #
    # If you operate on `tickets` directly, archived tickets will NOT be
    # preloaded, preload calls will silently fail, and lookups with N+1.
    consolidated_tickets = build_consolidated_tickets(tickets)

    if side_load?(:users)
      user_ids = tickets.map do |t|
        follower_ids(t) + email_cc_ids(t) + [t.assignee_id, t.requester_id, t.submitter_id]
      end

      user_ids += comment_email_cc_ids(tickets)

      user_ids.flatten!
      user_ids.compact!
      user_ids.uniq!

      users = account.all_users.where(id: user_ids).to_a
      user_presenter.preload_associations(users)
      json[:users] = user_presenter.collection_presenter.model_json(users)
    end

    if side_load?(:commenters)
      json[:commenters] = commenters_on(tickets)
    end

    if side_load?(:groups)
      json[:groups] = side_load_association(tickets, :group, group_presenter)
    end

    if side_load?(:organizations)
      json[:organizations] = side_load_association(consolidated_tickets, :organization, organization_presenter)
    end

    add_deleted_ticket_forms(json)

    if side_load?(:audit)
      Rails.logger.info("DEPRECATED audit side load on ticket. Use last_audits")
      json[:audit] = if preload_audit_events?
        last_audits_side_load(tickets).try(:first)
      else
        audit_presenter.model_json(latest_audit(ticket))
      end
    end

    if side_load?(:metrics)
      Rails.logger.info("DEPRECATED metrics side load on ticket. Use metric_sets")
      json[:metrics] = metric_set_presenter.model_json(ticket.ticket_metric_set)
    end

    if side_load?(:last_audits)
      json[:last_audits] = if preload_audit_events?
        last_audits_side_load(tickets)
      else
        audits = tickets.map { |t| latest_audit(t) }
        audit_presenter.preload_associations(audits)
        audit_presenter.collection_presenter.model_json(audits)
      end
    end

    if side_load?(:metric_sets)
      json[:metric_sets] = side_load_association(consolidated_tickets, :ticket_metric_set, metric_set_presenter)
    end

    if side_load?(:brands)
      Brand.with_deleted do
        brands = account.brands.where(id: tickets.map(&:brand_id)).to_a
        json[:brands] = brand_presenter.collection_presenter.model_json(brands)
      end
    end

    if side_load?(:sharing_agreements)
      agreement_ids = tickets.flat_map { |t| sharing_agreement_ids(t) }.uniq
      json[:sharing_agreements] =
        if agreement_ids.empty?
          []
        else
          sharing_agreements = account.sharing_agreements.where(id: agreement_ids).to_a
          sharing_agreement_presenter.preload_associations(sharing_agreements)
          sharing_agreement_presenter.collection_presenter.model_json(sharing_agreements)
        end
    end

    if side_load?(:followups)
      json[:followups] = collection_presenter.model_json(tickets.flat_map(&:followups).uniq(&:id))
    end

    set_satisfaction_ratings(consolidated_tickets, json)

    if side_load?(:ticket_forms) && account.has_ticket_forms?
      json[:ticket_forms] = side_load_association(consolidated_tickets, :ticket_form, ticket_form_presenter)
    end

    json
  end

  def last_audits_side_load(tickets)
    if !@last_audits_side_load.nil? && tickets.all? { |ticket| @last_audits_side_load.key?(ticket.nice_id) }
      return @last_audits_side_load.values_at(*tickets.map(&:nice_id))
    end

    # We are missing one or more of the tickets' last audits in our cache, so populate it again now
    @last_audits_side_load ||= {}

    audits = tickets.map { |t| latest_audit(t) }
    audit_presenter.preload_associations(audits)
    new_audits = audit_presenter.collection_presenter.model_json(audits)

    @last_audits_side_load.merge!(new_audits.index_by { |audit_json| audit_json[:ticket_id] })

    new_audits
  end

  def url(ticket)
    # url builder significantly slower than string concatenation
    # when called 1000s of times.

    # TWITTER-877
    # zendesk/channels/api/v2/twitter/tickets_controller that is now inside zendesk_channels is calling this method
    # becasue of the routing logic, the tickets_controller in channels doesn't understand api_v2_ticket_url magic method
    # Hence the check to see could we ask main_app for this information.
    @cached_url_path_part ||= if options[:url_builder].respond_to?(:main_app)
      options[:url_builder].main_app.send(:api_v2_ticket_url, ticket.nice_id, format: :json).to_s.gsub(/\d+.json$/, '')
    else
      options[:url_builder].send(:api_v2_ticket_url, ticket.nice_id, format: :json).to_s.gsub(/\d+.json$/, '')
    end

    @cached_url_path_part.clone << ticket.nice_id.to_s << ".json"
  end

  def audit_presenter
    @audit_presenter ||= options[:audit_presenter] ||= Api::V2::Tickets::AuditPresenter.new(user, options)
  end

  def comment_presenter_for(comment)
    case comment
    when VoiceComment, VoiceApiComment
      voice_comment_presenter
    else
      comment_presenter
    end
  end

  def voice_comment_presenter
    @voice_comment_presenter ||= Api::V2::Tickets::VoiceCommentPresenter.new(user, options)
  end

  def comment_presenter
    @comment_presenter ||= options[:comment_presenter] ||= Api::V2::Requests::CommentPresenter.new(user, options)
  end

  def metric_set_presenter
    @metric_set_presenter ||= options[:metric_set_presenter] ||= Api::V2::TicketMetricSetPresenter.new(user, options)
  end

  def brand_presenter
    @brand_presenter ||= options[:brand_presenter] ||= Api::V2::BrandPresenter.new(user, options)
  end

  def rating_presenter
    @rating_presenter ||= options[:rating_presenter] ||= Api::V2::SatisfactionRatingsPresenter.new(user, options)
  end

  def satisfaction_rating_presenter
    @satisfaction_rating_presenter ||= options[:satisfaction_rating_presenter] ||= Api::V2::SatisfactionRatingPresenter.new(user, options)
  end

  def ticket_form_presenter
    @ticket_form_presenter ||= options[:ticket_form_presenter] ||= Api::V2::TicketFormPresenter.new(user, options)
  end

  def sla_policy_presenter
    @sla_policy_presenter ||= Api::V2::Slas::PolicyPresenter.new(user, options)
  end

  def metric_event_presenter
    @metric_event_presenter ||= Api::V2::Exports::TicketMetricEventPresenter.new(user, options)
  end

  def reply_options_presenter
    @reply_options_presenter ||= Api::V2::Tickets::ReplyOptionsPresenter.new(user, options)
  end

  def workspace_presenter
    @workspace_presenter = Api::V2::WorkspacePresenter.new(user, options)
  end

  def problem_id(ticket)
    problem = @archive_problem_tickets.fetch(ticket.linked_id, nil) || ticket.problem

    # Archived problems can't be side loaded
    # this is a performance issue for exports with problems with a lot of incidents
    if problem && problem.archived?
      @archive_problem_tickets[ticket.linked_id] = problem
    end

    problem.try(:nice_id)
  end

  def collaborator_ids(ticket)
    if follower_and_email_cc_collaborations_enabled
      if ticket.archived?
        collaborators_from_archived_ticket(ticket)
      else
        # Take advantage of caching and preloaded users in follower_ids and email_cc_ids
        (follower_ids(ticket) + email_cc_ids(ticket)).uniq
      end
    else
      collaborators = if ticket.archived?
        # We keep collaborations in their mysql table, they never get deleted when a ticket is archived.
        @collaborators_from_archived_tickets[ticket.id] || []
      else
        ticket.collaborations
      end

      collaborators.map(&:user_id).uniq
    end
  end

  def follower_ids(ticket)
    return collaborator_ids(ticket) unless follower_and_email_cc_collaborations_enabled
    return [] unless ticket_followers_allowed

    if @follower_ids_by_ticket.present? && @follower_ids_by_ticket.key?(ticket.id)
      return @follower_ids_by_ticket[ticket.id]
    end

    @follower_ids_by_ticket ||= {}

    ticket_follower_ids = if ticket.archived?
      followers_from_archived_ticket(ticket)
    else
      ticket.followers(users_preloaded: true).map(&:id).uniq
    end

    @follower_ids_by_ticket[ticket.id] = ticket_follower_ids
  end

  def email_cc_ids(ticket)
    return [] unless comment_email_ccs_allowed

    if @email_cc_ids_by_ticket.present? && @email_cc_ids_by_ticket.key?(ticket.id)
      return @email_cc_ids_by_ticket[ticket.id]
    end

    @email_cc_ids_by_ticket ||= {}

    ticket_email_ccs = if ticket.archived?
      email_ccs_from_archived_ticket(ticket)
    else
      ticket.email_ccs(users_preloaded: true).map(&:id).uniq
    end

    @email_cc_ids_by_ticket[ticket.id] = ticket_email_ccs
  end

  def comment_email_cc_ids(tickets)
    return [] unless comment_email_ccs_allowed

    # active_recipients_ids for email results in an N+1, so we have to do it separately here
    email_audits, other_audits = tickets.reject(&:archived?).flat_map(&:audits).partition { |audit| audit.via?(:mail) }

    email_user_ids = account.user_email_identities.where(value: email_audits.flat_map(&:recipients_list).uniq).pluck(:user_id)

    active_email_user_ids = account.users.where(id: email_user_ids, is_active: true).pluck(:id)

    (active_email_user_ids + other_audits.flat_map(&:active_recipients_ids)).compact.uniq
  end

  def sharing_agreement_ids(ticket)
    @sharing_agreement_ids[ticket.id] ||= ticket.archived? ? [] : ticket.shared_tickets.map(&:agreement_id).compact
  end

  def followup_ids(ticket)
    ticket.followups.map(&:nice_id).uniq
  end

  def build_consolidated_tickets(tickets)
    # Need this to avoid Riak calls when calling methods on TicketArchiveStub instead
    # of Ticket.
    tickets.map do |t|
      t.archived? ? @archived_tickets[t.id] : t
      # the whole chain should be implemented as lazy calls to avoid multiple iterations. Ruby 2.4 introduces lazy uniq.
    end.compact.uniq(&:id)
  end

  def permissions(ticket)
    Api::V2::Tickets::TicketPermissionsPresenter.new(user, options).model_json(ticket)
  end

  def audit_count_since(ticket, date)
    ticket.audits.where("created_at > ?", date).count(:all)
  end

  def association_preloads
    collaboration_preload = {}
    collaboration_preload[:user] = {} if account.has_email_ccs?

    preload = {
      collaborations:    collaboration_preload,
      shared_tickets:    {},
      requester:         {identities: {}},
      problem:           {}, # for nice_ids
      event_decorations: {},
      assignee:          {}
    }

    if account.has_customer_satisfaction_enabled?
      preload[:satisfaction_rating] = if account.has_satisfaction_rating_disable_comment_fallback?
        {comment: {}, event: {}}
      else
        {}
      end

      if account.has_ticket_presenter_no_satisfaction_preloads? || account.has_satisfaction_rating_disable_comment_fallback?
        # Do not preload
      elsif account.has_ticket_presenter_no_satisfaction_score_preloads?
        preload[:satisfaction_rating_comments] = {}
      else
        preload[:satisfaction_rating_scores] = {}
        preload[:satisfaction_rating_comments] = {}
      end
    end

    if account.has_satisfaction_prediction?
      preload[:ticket_prediction] = {}
    end

    if side_load?(:metric_sets)
      preload[:ticket_metric_set] = metric_set_presenter.association_preloads
    end

    unless @preload_archived || options[:use_raw_ticket_field_entries]
      preload[:ticket_field_entries] = {}
    end

    if side_load?(:last_audits) && !@preload_archived
      preload[:audits] = audit_presenter.association_preloads
    elsif account.has_email_ccs_preload_notifications_with_ccs? && side_load?(:users)
      preload[:audits] = { notifications_with_ccs: {} }
    end

    if side_load?(:ticket_forms) && account.has_ticket_forms?
      preload[:ticket_form] = ticket_form_presenter.association_preloads
    end

    preload[:requester] = {}
    preload[:organization] = {}

    preload
  end

  def preload_associations(ticket)
    tickets = Array(ticket)

    # Archived tickets are preloaded separately.
    #
    # This is because the "blobs" of archive data already contain association
    # records for events, ticket_field_entries, and taggings.
    #
    # Presumably, this makes pre_load n+1 or similar. However, I can't reproduce
    # n+1 or nil issues for pre_load on a mixed archived/unarchived list.
    # Maybe this isn't necessary (anymore?)
    archived, unarchived = tickets.partition(&:archived?)

    if archived.present?
      # When fetching archived tickets, the "blob" fetch from Riak can be deferred with "is_stub: true".
      # If we don't eagerly load them, they will n+1 single riak_client.get calls
      archived_tickets_loaded = archived.none? { |t| t.respond_to?(:is_archive_stub?) && t.is_archive_stub? }

      unless archived_tickets_loaded
        Rails.logger.append_attributes(ticket_presenter_preload_archived_tickets: true)
        archived_ids = archived.map(&:id)
        archived = archived_ids.empty? ? [] : Ticket.with_deleted { Ticket.where(id: archived_ids).all_with_archived }
      end

      @archived_tickets = archived.index_by(&:id)
    end

    @preload_archived = true
    super(archived)
    @preload_archived = false
    super(unarchived)

    collaborators_from_archived(archived)
    consolidated_tickets = build_consolidated_tickets(tickets)
    preload_via_associations(consolidated_tickets)
    preload_followups_with_archived(consolidated_tickets)

    preload_raw_ticket_field_entries(tickets) if options[:use_raw_ticket_field_entries]
    preload_deleted_ticket_forms(tickets)
    preload_comment_counts(unarchived) if side_load?(:comment_count)
    preload_incidents(tickets)
  end

  def preload_raw_ticket_field_entries(tickets)
    return if tickets.empty?

    exporter = Zendesk::Export::TicketFieldEntriesRawDataExporter.new(account, tickets)

    @raw_ticket_field_entries = exporter.raw_ticket_field_entries
  end

  def preload(tickets, preload)
    tickets = tickets.select { |ticket| yield ticket }
    return if tickets.empty?

    tickets.pre_load(preload)
  end

  def preload_via_associations(events)
    hash = Api::V2::Tickets::AuditEventCollectionPresenter.via_objects_from_events(events, account)
    options[:event_via_associations] ||= {}
    options[:event_via_associations].deep_merge!(hash)
  end

  def via_export(ticket)
    via = serialized_via_object(ticket)
    via[:id] = ticket.via_id if options.fetch(:expanded_via_types, false)
    via
  end

  def last_comment(ticket)
    ticket.latest_comment_mapped || ticket.latest_comment
  end

  def replace_dc_if_agent(ticket, string, custom_fields_cache)
    # [ZD#2166537] We want to resolve placeholders as long as the current user is agent.
    # Otherwise, bulk updates give an impression that the update did not go through
    if user.is_agent?
      render_dynamic_content(string, ticket: ticket, user_is_agent: true, custom_fields_cache: custom_fields_cache)
    else
      string
    end
  end

  def collaborators_from_archived(archived_tickets)
    return [] if archived_tickets.empty?

    # Ideally we would restrict on both account_id and ticket_id but there is no index on both columns
    Collaboration.where(ticket_id: archived_tickets.map(&:id)).each do |collaboration|
      @collaborators_from_archived_tickets[collaboration.ticket_id] ||= []
      @collaborators_from_archived_tickets[collaboration.ticket_id] << collaboration
    end

    @collaborators_from_archived_tickets
  end

  def preload_followups_with_archived(tickets)
    tickets = Array(tickets)
    return if tickets.empty?

    tickets_for_followups = if side_load?(:followups)
      tickets
    else
      # followup_ids is only applied to closed tickets
      tickets.select(&:closed?)
    end

    tickets_for_followups.pre_load(:followup_target_links)

    ticket_ids_for_followups = tickets_for_followups.
      flat_map { |t| t.followup_target_links.map(&:target_id) }.
      uniq

    targets_by_id = if ticket_ids_for_followups.empty?
      {}
    else
      Ticket.where(id: ticket_ids_for_followups).
        all_with_archived.
        index_by(&:id)
    end

    tickets_for_followups.each do |ticket|
      followup_ids = ticket.followup_target_links.map(&:target_id)
      # We may have an id but a nil object, when it has been deleted for example.
      # That's why we need to compact the results.
      followups = followup_ids.map { |id| targets_by_id[id] }.compact
      ticket.association(:followups).target = followups
    end
  end

  # Run a single `SELECT COUNT(*), ticket_id GROUP BY ticket_id`
  # This is only useful for unarchived tickets.
  # Archived tickets store comments separate and are always loaded.
  def preload_comment_counts(unarchived_tickets)
    if unarchived_tickets.any?
      # @comment_counts[ticket.id] => count
      @comment_counts = Comment.where(ticket_id: unarchived_tickets.map(&:id)).group(:ticket_id).count
    end
  end

  def preload_incidents(tickets)
    problem_tickets = tickets.select(&:problem?)

    return unless problem_tickets.any?

    non_archived_incident_counts = account.tickets.
      force_index('index_tickets_on_linked_id').
      where(linked_id: problem_tickets.map(&:id)).
      group(:linked_id).count

    archived_incident_counts = account.ticket_archive_stubs.
      force_index('index_ticket_archive_stubs_on_linked_id').
      where(linked_id: problem_tickets.map(&:id)).
      group(:linked_id).count

    @ticket_incident_counts = non_archived_incident_counts.merge(archived_incident_counts) { |_, oldval, newval| oldval + newval }
  end

  def present_errors(model)
    Api::V2::Tickets::ErrorsPresenter.new.present(model)
  end

  private

  def reduced_payload_size?
    !!@reduced_payload_size
  end

  def description(ticket)
    return nil if reduced_payload_size?

    replace_dc_if_agent(ticket, ticket.description, @custom_fields_cache)
  end

  def custom_fields(options)
    ticket_fields = account.ticket_fields(include_voice_insights: options[:include_voice_insights_fields]).custom.active
    ticket_fields = ticket_fields.exportable if options[:exclude_non_exportable_fields]

    ticket_fields
  end

  # Return a hash of nice_id -> commenters
  def commenters_on(tickets)
    # Load all comments to not make a call to comments table for each ticket.
    tickets.pre_load(comments: :author)

    tickets.each_with_object({}) do |ticket, json|
      authors = ticket.comments.map(&:author)
      authors.uniq!
      user_presenter.preload_associations(authors)
      json[ticket.nice_id] = user_presenter.collection_presenter.model_json(authors)
    end
  end

  def add_ticket_form_id_and_deleted_form_id(json, ticket)
    if account.has_ticket_forms?
      json[:ticket_form_id] = ticket.ticket_form_id

      # @deleted_ticket_form_id_set is set when we call #present (usually
      # with a collection).
      # In that case we use the value from there so we don't fire a sql request
      # for each ticket.
      # Otherwise we call #deleted_ticket_form which will trigger a sql request,
      # but it should be the case when we only present one ticket at a time.
      if @deleted_ticket_form_id_set
        if @deleted_ticket_form_id_set.include?(ticket.ticket_form_id)
          json[:deleted_ticket_form_id] = ticket.ticket_form_id
        end
      else
        json[:deleted_ticket_form_id] = ticket.deleted_ticket_form.try(:id)
      end
    end
  end

  def add_deleted_ticket_forms(json)
    unless @deleted_ticket_forms.empty?
      json[:deleted_ticket_forms] = @deleted_ticket_forms.map do |tf|
        ticket_form_presenter.model_json(tf)
      end
    end
  end

  def preload_deleted_ticket_forms(tickets)
    @deleted_ticket_forms = if account.has_ticket_forms?
      # the whole chain should be implemented as lazy calls to avoid multiple iterations. Ruby 2.4 introduces lazy uniq.
      ticket_form_id_ids = tickets.map(&:ticket_form_id).compact.uniq
      if ticket_form_id_ids.empty?
        []
      else
        TicketForm.unscoped.deleted.where(id: ticket_form_id_ids)
      end
    else
      []
    end

    @deleted_ticket_form_id_set = @deleted_ticket_forms.map(&:id).to_set
  end

  def set_satisfaction_ratings(tickets, json)
    if side_load?(:satisfaction_ratings) && account.has_customer_satisfaction_enabled?
      # #pre_load is done in #side_load_association but if we don't assign the ticket
      # to each satisfaction rating, it will query mysql/riak again
      tickets.pre_load(:satisfaction_ratings)
      tickets.each do |t|
        t.satisfaction_ratings.each { |sr| sr.ticket = t }
      end
      json[:satisfaction_ratings] = side_load_association(tickets, :satisfaction_ratings, satisfaction_rating_presenter)
    end
  end

  # Adding a query scope breaks the audit fetch for archived tickets
  # so we should only use the optimized latest if we're unarchived
  def latest_audit(ticket)
    if ticket.archived?
      ticket.audits.last
    else
      ticket.audits.latest
    end
  end

  def collaborators_from_archived_ticket(ticket)
    # We keep collaborations in their mysql table, they never get deleted when a ticket is archived.
    collaborations = @collaborators_from_archived_tickets[ticket.id] || []
    collaborations.map(&:user_id).uniq
  end

  def email_ccs_from_archived_ticket(ticket)
    # We keep collaborations in their mysql table, they never get deleted when a ticket is archived.
    collaborations = @collaborators_from_archived_tickets[ticket.id] || []
    collaborations.select { |c| c.collaborator_type == CollaboratorType.EMAIL_CC }.map(&:user_id).uniq
  end

  def followers_from_archived_ticket(ticket)
    # We keep collaborations in their mysql table, they never get deleted when a ticket is archived.
    collaborations = @collaborators_from_archived_tickets[ticket.id] || []
    collaborations.select { |c| c.collaborator_type == CollaboratorType.FOLLOWER }.map(&:user_id).uniq
  end

  def follower_and_email_cc_collaborations_enabled
    @follower_and_email_cc_collaborations_enabled ||= account.has_follower_and_email_cc_collaborations_enabled?
  end

  def comment_email_ccs_allowed
    @comment_email_ccs_allowed ||= account.has_comment_email_ccs_allowed_enabled?
  end

  def ticket_followers_allowed
    @ticket_followers_allowed ||= account.has_ticket_followers_allowed_enabled?
  end

  def ticket_sla_metrics_json(ticket)
    return Zendesk::Sla::MetricsReader.new(ticket).metric_statuses.map do |ticket_metric|
      Api::V2::Slas::TicketMetricPresenter.new(user, options).model_json(ticket_metric)
    end if account.has_sla_ticket_metrics_reader?

    include_first_reply_time = account.has_sla_sideload_first_reply_time?

    Zendesk::Sla::MetricSelection.new(ticket).all(include_first_reply_time).map do |ticket_metric|
      Api::V2::Slas::TicketMetricPresenter.new(user, options).model_json(ticket_metric)
    end
  end

  def preload_audit_events?
    account.has_email_ccs_preload_notifications_with_ccs? ||
      account.has_preload_audit_events_in_ticket_presenter?
  end
end
