class Api::V2::Tickets::AuditPresenter < Api::V2::Presenter
  include Api::V2::Tickets::EventViaPresenter
  include Api::V2::Tickets::AuditSideloader

  self.model_key = :audit

  def initialize(audit, options = {})
    super

    @event_types_filter = options[:filter_event_types]
  end

  def event_presenter
    @event_presenter ||= options[:event_presenter] ||= Api::V2::Tickets::AuditEventPresenter.new(user, options)
  end

  ## ### JSON Format
  ## Audits are represented as JSON objects which have the following keys:
  ##
  ## | Name       | Type                   | Read-only | Comment
  ## | ---------- | ---------------------- | --------- | -------
  ## | id         | integer                | yes       | Automatically assigned when creating audits
  ## | ticket_id  | integer                | yes       | The ID of the associated ticket
  ## | metadata   | hash                   | yes       | Metadata for the audit, custom and system data
  ## | via        | [Via](#the-via-object) | yes       | This object explains how this audit was created
  ## | created_at | date                   | yes       | The time the audit was created
  ## | author_id  | integer                | yes       | The user who created the audit
  ## | events     | array                  | yes       | An array of the events that happened in this audit. See [Audit Events](#audit-events)
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         35436,
  ##   "ticket_id":  47,
  ##   "created_at": "2009-07-20T22:55:29Z",
  ##   "author_id":  35436,
  ##   "metadata":  { "custom": { "time_spent": "3m22s" }, "system": { "ip_address": "184.106.40.75" }}
  ##   "via": {
  ##     "channel": "web"
  ##   },
  ##   "events": [
  ##     {
  ##       "id":          1564245,
  ##       "type":        "Comment"
  ##       "body":        "Thanks for your help!",
  ##       "public":      true,
  ##       "attachments": []
  ##     },
  ##     {
  ##       "id":      1564246,
  ##       "type":    "Notification"
  ##       "subject": "Your ticket has been updated"
  ##       "body":    "Ticket #47 has been updated"
  ##     }
  ##   ]
  ## }
  ## ```
  def model_json(audit)
    json = {
      id:         audit.id,
      ticket_id:  audit.ticket.try(:nice_id),
      created_at: audit.created_at,
      author_id:  audit.author_id,
      metadata:   audit.metadata,
    }

    events = if @event_types_filter
      audit.events.where(type: @event_types_filter).all
    else
      audit.events
    end

    json[:events] = event_presenter.collection_presenter.model_json(events)

    comment = audit.events.find { |e| e.is_a?(Comment) }
    if comment && comment.event_decoration.present?
      json[:metadata] = json[:metadata].merge(decoration: comment.event_decoration.data)
    end

    options.merge!(preloaded_associations_from_collection_presenter)

    # #serialized_via_object must be called after we load audit.events a first time
    # so it doesn't fire another SQL request in
    # EventViaPresenter#get_voice_comment_from_resource
    json[:via] = serialized_via_object(audit)

    json
  end

  def association_preloads
    preload = {
      events: {
        audit:            {},
        event_decoration: {}
      },
      ticket: {}
    }

    preload[:notifications_with_ccs] = {} if account.has_email_ccs_preload_notifications_with_ccs?

    preload
  end

  def preload_associations(audit)
    super

    return unless audit.present?

    audits = Array(audit)

    return unless audits[0].is_a?(Audit)

    events = audits.map(&:events)
    events.flatten!

    event_presenter.collection_presenter.preload_associations(audits + events)
  end

  def preloaded_associations_from_collection_presenter
    event_presenter.
      collection_presenter.
      try(:options)&.
      slice(:event_via_associations) || {}
  end
end
