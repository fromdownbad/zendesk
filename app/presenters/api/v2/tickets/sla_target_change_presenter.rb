require 'zendesk/sla'

module Api::V2::Tickets
  class SlaTargetChangePresenter < AuditEventPresenter
    ## #### SLA target change event
    ## SLA target change events have the following keys:
    ##
    ## | Name             | Type    | Read-only | Comment
    ## | ---------------- | ------- | --------- | -------
    ## | id               | integer | yes       | Automatically assigned when creating events
    ## | type             | string  | yes       | Has the value "Change"
    ## | previous_value   | object  | yes       | Previous value in minutes, and indicator of whether the value was in calendar hours or business hours. May be empty
    ## | value            | object  | yes       | Current value in minutes, and indicator of whether the value was in calendar hours or business hours. May be empty
    ## | field_name       | string  | yes       | The identifier of the SLA metric. May be "first_reply_time" or "requester_wait_time"
    ## | via              | object  | yes       | Via information
    ##
    ## #### Example
    ## ```js
    ## {
    ##   "id": 4497,
    ##   "type": "Change",
    ##   "previous_value": { "minutes": 150, "business_hours": false },
    ##   "value": { "minutes": 45, "in_business_hours": false },
    ##   "field_name": "requester_wait_time",
    ##   "via": {
    ##     "source": {
    ##       "rel": "sla_target_change"
    ##     },
    ##     "current_sla_policy": "for demo"
    ##   }
    ## }
    ## ```
    def model_json(sla_target_change)
      super.merge(
        type:           'Change',
        previous_value: sla_target_change.initial_target,
        value:          sla_target_change.final_target,
        field_name:     metric_name(sla_target_change),
        via:            {
          source:             {rel: 'sla_target_change'},
          current_sla_policy: sla_target_change.current_sla_policy
        }
      )
    end

    private

    def metric_name(target_change)
      Zendesk::Sla::TicketMetric.from_id(target_change.metric_id).name
    end
  end
end
