module Api::V2::Tickets::AttributeMappings
  include Zendesk::Types

  TYPE_MAP = {
    TicketType.QUESTION.to_s  => 'question',
    TicketType.INCIDENT.to_s  => 'incident',
    TicketType.PROBLEM.to_s   => 'problem',
    TicketType.TASK.to_s      => 'task',
    TicketType.find!("-").to_s => nil
  }.freeze

  PRIORITY_MAP = {
    PriorityType.LOW.to_s       => 'low',
    PriorityType.NORMAL.to_s    => 'normal',
    PriorityType.HIGH.to_s      => 'high',
    PriorityType.URGENT.to_s    => 'urgent',
    PriorityType.find!("-").to_s => nil
  }.freeze

  STATUS_MAP = {
    StatusType.NEW.to_s     => 'new',
    StatusType.OPEN.to_s    => 'open',
    StatusType.PENDING.to_s => 'pending',
    StatusType.HOLD.to_s    => 'hold',
    StatusType.SOLVED.to_s  => 'solved',
    StatusType.CLOSED.to_s  => 'closed',
    StatusType.DELETED.to_s => 'deleted',
    StatusType.ARCHIVED.to_s => 'archived' # technically the end-user should never see this, but adding it in for completeness
  }.freeze

  SATISFACTION_MAP = {
    SatisfactionType.GOOD.to_s            => 'good',
    SatisfactionType.GOODWITHCOMMENT.to_s => 'good',
    SatisfactionType.BAD.to_s             => 'bad',
    SatisfactionType.BADWITHCOMMENT.to_s  => 'bad',
    SatisfactionType.OFFERED.to_s         => SatisfactionType[SatisfactionType.OFFERED].name.downcase,
    SatisfactionType.UNOFFERED.to_s       => SatisfactionType[SatisfactionType.UNOFFERED].name.downcase
  }.freeze

  FULL_SATISFACTION_MAP = SATISFACTION_MAP.merge(
    SatisfactionType.GOODWITHCOMMENT.to_s => 'good_with_comment',
    SatisfactionType.BADWITHCOMMENT.to_s  => 'bad_with_comment'
  ).freeze

  PARAMS_SATISFACTION_MAP = {
    "good" => SatisfactionType.GOOD,
    "bad" => SatisfactionType.BAD
  }.freeze

  # cuz Rails 4 decided to get weird and start storing t/f instead of 1/0 text columns
  TRUTH_MAP = {
    'f' => '0',
    '0' => '0',
    't' => '1',
    '1' => '1'
  }.freeze

  default_proc = lambda { |h, k| h[k] = k }

  VIA_ID_MAP = Api::V2::Tickets::EventViaPresenter::VIA_ID_MAP.stringify_keys.tap do |map|
    map.default_proc = default_proc
  end

  INVERSE_ID_MAP = {}.tap do |map|
    map.default_proc = default_proc
  end

  ATTRIBUTE_MAPPINGS = {
    ticket_type_id: { to: :type, using: TYPE_MAP },
    priority_id: { to: :priority, using: PRIORITY_MAP },
    status_id: { to: :status, using: STATUS_MAP },
    satisfaction_score: { full: FULL_SATISFACTION_MAP, using: SATISFACTION_MAP },
    via_id: { to: :via_type, using: VIA_ID_MAP, inverse_using: INVERSE_ID_MAP },
    current_via_id: { using: VIA_ID_MAP },
    entry_id: { to: :forum_topic_id },
    due_date: { to: :due_at },
    fields: { to: :custom_fields },
    original_recipient_address: { to: :recipient },
    is_public: { using: TRUTH_MAP }
  }.freeze

  def map_ticket_attributes!(hash)
    ATTRIBUTE_MAPPINGS.each do |key, mapping|
      next unless hash.key?(key)

      val = hash.delete(key)
      hash[mapping[:to]] = if mapping.key?(:using)
        mapping[:using][val]
      else
        val
      end
    end

    hash
  end
  module_function :map_ticket_attributes!

  def ticket_attribute_name(attribute, account = nil)
    mapping = ATTRIBUTE_MAPPINGS[attribute.to_sym]

    if mapping && mapping.key?(:to)
      mapping[:to]
    elsif attribute.start_with?("ticket_fields")
      attribute.sub(/ticket/, "custom")
    elsif attribute == 'cc' && account.try(:has_follower_and_email_cc_collaborations_enabled?)
      # The 'cc' attribute can refer to either 'follower' or 'cc' depending on
      # an account's settings/features. This overrides the value to be
      # 'follower' so any API consumers don't have to query the account to know
      # what attribute they are dealing with.
      'follower'
    else
      attribute
    end
  end
  module_function :ticket_attribute_name

  def ticket_attribute_value(attribute, value, account, full_fidelity: false)
    mapping = ATTRIBUTE_MAPPINGS[attribute.to_sym]

    if full_fidelity && mapping.try(:key?, :full)
      mapping[:full][value.to_s]
    elsif mapping.try(:key?, :using)
      mapping[:using][value.to_s]
    elsif attribute.start_with?("ticket_fields")
      account.fetch_custom_field_option_value(value) || value
    else
      value
    end
  end
  module_function :ticket_attribute_value
end
