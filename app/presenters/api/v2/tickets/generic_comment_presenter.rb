class Api::V2::Tickets::GenericCommentPresenter < Api::V2::Tickets::CommentPresenter
  def presenters
    @presenters ||= Api::V2::Tickets::AuditEventCollectionPresenter.presenters
  end

  self.model_key = :comment

  ## ### Ticket Mobile Comments
  ## Ticket comments represent the conversation between requesters, collaborators, and agents. Comments can be public or private.
  ##
  ## ### JSON Format
  ##
  ## Ticket comments are represented as JSON objects with the following properties:
  ##
  ## | Name                  | Type    | Read-only | Comment
  ## | --------------------- | ------- | --------- | -------
  ## | id                    | integer | yes       | Automatically assigned when the comment is created
  ## | type                  | string  | yes       | `Comment` or `VoiceComment`
  ## | body                  | string  | yes       | The comment string
  ## | html_body             | string  | yes       | The comment formatted as HTML
  ## | plain_body            | string  | yes       | The comment formatted as plain text
  ## | public                | boolean | no        | true if a public comment; false if an internal note
  ## | author_id             | integer | yes       | The id of the comment author
  ## | attachments           | array   | yes       | Attachments, if any. See [Attachment](./attachments)
  ## | via                   | object  | yes       | How the comment was created. See [Via Object](./ticket_audits#the-via-object)
  ## | metadata              | object  | yes       | System information (web client, IP address, etc.)
  ## | created_at            | date    | yes       | The time the comment was created
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":        1274,
  ##   "type":      "Comment"
  ##   "body":      "Thanks for your help!",
  ##   "public":    true,
  ##   "created_at": "2009-07-20T22:55:29Z",
  ##   "author_id": 123123,
  ##   "attachments": [
  ##     {
  ##       "id":           498483,
  ##       "file_name":    "crash.log",
  ##       "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##       "content_type": "text/plain",
  ##       "size":         2532,
  ##       "thumbnails":   []
  ##     }
  ##   ],
  ##   "metadata": {
  ##     ...
  ##   },
  ##   "via": {
  ##     ...
  ##   },
  ##   "some_other_type": {
  ##     // depending on comment type
  ##   }
  ## }
  ## ```

  def model_json(comment)
    return nil unless json = find_presenter(comment.class).model_json(comment)
    json.merge!(
      via:         serialized_via_object(comment.audit),
      created_at:  comment.created_at
    )
  end

  private

  def find_presenter(klass)
    return nil if klass.nil?
    generic_presenter = presenters[klass] || Api::V2::Tickets::CommentPresenter
    generic_presenter.new(user, @options)
  end
end
