module Api::V2::Tickets::AuditSideloader
  def side_loads(audit)
    audits = Array(audit)

    {}.tap do |json|
      if side_load?(:users)
        json[:users] = setup_side_load(audits, 'user', :all_users)
      end

      if side_load?(:organizations)
        json[:organizations] = setup_side_load(audits, 'organization', :organizations)
      end

      if side_load?(:groups)
        json[:groups] = setup_side_load(audits, 'group', :all_groups)
      end

      if side_load?(:tickets)
        json[:tickets] = setup_side_load(audits, 'ticket', :tickets)
      end

      if side_load?(:ticket_forms)
        json[:ticket_forms] = setup_side_load(audits, 'ticket_form', :ticket_forms)
      end

      if side_load?(:sharing_agreements)
        json[:sharing_agreements] = setup_side_load(audits, 'sharing_agreement', :sharing_agreements)
      end

      if side_load?(:brands)
        json[:brands] = setup_side_load(audits, 'brand', :brands)
      end

      if side_load?(:schedules)
        json[:schedules] = setup_side_load(audits, 'schedule', :schedules)
      end

      if side_load?(:satisfaction_reasons)
        json[:satisfaction_reasons] = satisfaction_reasons_sideload(audits)
      end
    end
  end

  def setup_side_load(audits, model_name, association)
    model_ids = audits.map { |audit| send("#{model_name}_ids", audit) }
    models    = prepare_models(model_ids, association)
    models.select! { |ticket| user.can?(:view, ticket) } if model_name == "ticket"
    present_models(models, model_name)
  end

  def fetch_models(model_ids, association)
    assoc = account.send(association)
    archived_v2 = (assoc.klass.respond_to?(:supports_archiver_v2?) && assoc.klass.supports_archiver_v2?)

    if archived_v2
      assoc.where(id: model_ids).all_with_archived
    else
      assoc.where(id: model_ids).to_a
    end
  end

  def prepare_models(model_ids, association)
    model_ids.flatten!
    model_ids.compact!
    model_ids.uniq!

    case association
    when :brands
      Brand.with_deleted { fetch_models(model_ids, association) }
    when :schedules
      Zendesk::BusinessHours::Schedule.with_deleted do
        fetch_models(model_ids, association)
      end
    else
      fetch_models(model_ids, association)
    end
  end

  def present_models(models, model_name)
    presenter = send("#{model_name}_presenter".to_sym)
    presenter.preload_associations(models)
    presenter.collection_presenter.model_json(models)
  end

  def gather_create_event(ids, event)
    ids << event.value.to_i if event.value.present?
  end

  def gather_change_event(ids, event)
    ids << event.value.to_i          if event.value.present?
    ids << event.value_previous.to_i if event.value_previous.present?
  end

  def model_ids(audit, type)
    ids = []

    audit.events.each do |event|
      case event
      when Create
        gather_create_event(ids, event) if event.field_name == "#{type}_id"
      when Change
        gather_change_event(ids, event) if event.field_name == "#{type}_id"
      when TicketSharingEvent, TicketUnshareEvent
        ids << event.agreement_id if event.agreement_id
      when ScheduleAssignment
        ids << event.new_schedule_id
      end
    end

    ids
  end

  def organization_ids(audit)
    model_ids(audit, "organization")
  end

  def group_ids(audit)
    model_ids(audit, "group")
  end

  def ticket_form_ids(audit)
    model_ids(audit, "ticket_form")
  end

  def sharing_agreement_ids(audit)
    model_ids(audit, "agreement")
  end

  def brand_ids(audit)
    Brand.with_deleted do
      model_ids(audit, "brand")
    end
  end

  def schedule_ids(audit)
    Zendesk::BusinessHours::Schedule.with_deleted do
      model_ids(audit, 'schedule')
    end
  end

  USER_FIELD_NAMES = ["requester_id", "submitter_id", "assignee_id"].freeze

  def user_ids(audit)
    ids = [audit.author_id]

    audit.events.each do |event|
      case event
      when Create
        gather_create_event(ids, event) if USER_FIELD_NAMES.include?(event.field_name)
      when Change
        gather_change_event(ids, event) if USER_FIELD_NAMES.include?(event.field_name)
      when Notification, Cc, OrganizationActivity, TwitterEvent
        ids += Array.wrap(event.recipients).map(&:to_i)
      when VoiceComment, VoiceApiComment
        ids << event.data[:answered_by_id].to_i if event.data && event.data[:answered_by_id].present?
      when Comment
        ids << event.author_id
      end
    end

    if account.has_comment_email_ccs_allowed_enabled? && email_ccs = audit.active_recipients_ids
      ids += email_ccs
    end

    ids
  end

  def ticket_ids(audit)
    ids = [audit.ticket_id]

    audit.events.each do |event|
      case event
      when Create
        gather_create_event(ids, event) if event.field_name == "linked_id"
      when Change
        gather_change_event(ids, event) if event.field_name == "linked_id"
      end
    end

    ids
  end

  def satisfaction_reasons_sideload(audits)
    reason_codes = []
    audits.each do |audit|
      audit.events.each do |event|
        next unless ["Change", "Create"].include?(event.type)
        if event.field_name == "satisfaction_reason_code"
          reason_codes << event.value.to_i if event.value.present?
          reason_codes << event.value_previous.to_i if event.value_previous.present?
        end
      end
    end

    reasons = account.satisfaction_reasons.where(reason_code: reason_codes.compact.uniq)
    satisfaction_reason_presenter.collection_presenter.model_json(reasons)
  end
end
