class Api::V2::Tickets::FacebookCommentPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Facebook comment event
  ## A comment was added to a ticket from Facebook.
  ##
  ## Facebook comments have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `FacebookComment`
  ## | data            | object  | yes       | Properties of the Facebook comment
  ## | body            | string  | yes       | The actual comment made by the author
  ## | html_body       | string  | yes       | The actual comment made by the author formatted as HTML
  ## | public          | boolean | yes       | If this is a public comment or an internal-agents-only note
  ## | trusted         | boolean | yes       | If this comment is trusted or marked as being potentially fraudulent
  ## | author_id       | integer | yes       | The id of the author of this comment
  ## | graph_object_id | string  | yes       | The graph object id of the associated Facebook Wall post or message
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id": 1274,
  ##   "type": "FacebookComment",
  ##   "data": {
  ##     "type": "status"
  ##     "content": "asrk2d",
  ##     "attachments": [
  ##       {
  ##         "id": "70713f06c93b0cba705cc10239ea3e4c",
  ##         "mime_type": "image/png",
  ##         "name": "transpmentor.png",
  ##         "size": 26981
  ##       }
  ##     ],
  ##     "via_zendesk": false
  ##   },
  ##   "public": true,
  ##   "author_id": 1,
  ##   "body": "Thanks for your help!",
  ##   "html_body": "<p>Thanks for your help!</p>",
  ##   "trusted": true,
  ##   "graph_object_id": "152318411530606_1523184115123123",
  ##   "attachments": []
  ## }
  ## ```
  def model_json(comment)
    super.merge(
      type:            "FacebookComment",
      public:          comment.is_public?,
      data:            comment.data,
      author_id:       comment.author_id,
      body:            comment.body,
      html_body:       comment.html_body(for_agent: user.is_agent?),
      trusted:         comment.trusted?,
      graph_object_id: comment.graph_object_id,
      attachments:     attachment_presenter.collection_presenter.model_json(comment.attachments)
    )
  end
end
