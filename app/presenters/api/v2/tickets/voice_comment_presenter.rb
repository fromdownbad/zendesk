class Api::V2::Tickets::VoiceCommentPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Voice comment event
  ## A voice comment was added to a ticket with Zendesk Talk.
  ##
  ## Voice comment events have the following properties:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `VoiceComment`
  ## | data            | object  | yes       | Properties describing the voice comment
  ## | formatted_from  | string  | yes       | A formatted version of the phone number which dialed the call
  ## | formatted_to    | string  | yes       | A formatted version of the phone number which answered the call
  ## | body            | string  | yes       | Comment added to the ticket
  ## | html_body       | string  | yes       | The comment formatted to HTML
  ## | public          | boolean | yes       | If true, the ticket requester can see the comment. If false, only agents can see it
  ## | trusted         | boolean | yes       | If this comment is trusted or marked as being potentially fraudulent
  ## | author_id       | integer | yes       | The comment author, typically the agent assigned to the ticket
  ## | transcription_visible | boolean | yes | Whether the transciption is visible on the ticket
  ## | attachments     | array   | yes       | The attachments on this comment as [Attachment](./attachments) objects
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":   215546547,
  ##   "type": "VoiceComment",
  ##   "body": ""Inbound call from +1 (123) 654-7890\nCall Details:\n\nCall from...",
  ##   "data": {
  ##     "answered_by_id":       63197591,
  ##     "answered_by_name":     "Keith Hayward",
  ##     "author_id":            63197591,
  ##     "brand_id":             1156956,
  ##     "call_duration":        64,
  ##     "call_id":              129873628,
  ##     "from":                 "+11236547890",
  ##     "location":             "Roselle, Illinois, United States",
  ##     "to":                   "+11233257890",
  ##     "public":               false,
  ##     "recording_url":        "https://omniwear.zendesk.com/api/v2/channels/voice/calls/CAed671/twilio/recording",
  ##     "started_at":           "2016-12-20T16:30:16Z",
  ##     "transcription_status": "completed",
  ##     "transcription_text":   "Hello, I have a problem with...",
  ##     "via_id":               34
  ##   },
  ##   "formatted_from":        "+1 (123) 654-7890",
  ##   "formatted_to":          "+1 (123) 325-7890",
  ##   "transcription_visible": false,
  ##   "public":                false,
  ##   "html_body":             "<div class=\"zd-comment\">\n<p dir=\"auto\">Inbound call from +1 (123) 654-7890<br>\nCall Details...",
  ##   "author_id":             63197591,
  ##   "trusted":               true,
  ##   "attachments":           []
  ## }
  ## ```
  ##
  ## Notes:
  ##
  ## * `data.answered_by_id` is not present for voicemails
  ## * `data.transcription_status` and `data.transcription_text` are only present for voicemails with transcription enabled
  ##
  def model_json(comment)
    json = super.merge(
      type: "VoiceComment",
      public: comment.is_public?,
      data: comment.data,
      formatted_from: comment.formatted_from,
      formatted_to: comment.formatted_to,
      transcription_visible: comment.transcription_visible,
      author_id: comment.author_id,
      body: comment.body,
      html_body: comment.html_body(for_agent: user.is_agent?),
      trusted: comment.trusted?,
      attachments: attachment_presenter.collection_presenter.model_json(comment.attachments),
      created_at: comment.created_at
    )
    json[:data][:recording_url] = comment.absolute_recording_url
    json
  end
end
