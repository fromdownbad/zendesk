class Api::V2::Tickets::RelatedPresenter < Api::V2::Presenter
  self.model_key = :ticket_related

  ## #### Ticket related information presenter
  ## Ticket related information has the following keys:
  ##
  ## | Name                | Type    | Read-only | Comment
  ## | ------------------- | ------- | --------- | -------
  ## | topic_id            | string  | yes       | Related topic to ticket
  ## | followup_source_ids | array   | yes       | Array of sources to follow up
  ## | from_archive        | boolean | yes       | Is the current ticket archived?
  ## | incidents           | integer | yes       | The count of related incident occurrences
  ## | twitter             | object  | yes       | Hash of associated twitter information
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "topic_id": null,
  ##   "jira_issue_ids": [],
  ##   "followup_source_ids": [],
  ##   "from_archive": false,
  ##   "incidents": 7,
  ##   "twitter": {
  ##     "handle_id": 10,
  ##     "profile": {
  ##       "created_at": "2013/01/08 23:24:49 -0800",
  ##       "description": "Zendesk is the leading ...",
  ##       ...
  ##     },
  ##     "direct": false
  ##   }
  ## }
  ## ```
  def model_json(ticket)
    json = {
      topic_id:            ticket.entry_id,
      jira_issue_ids:      ticket.jira_issues.map(&:issue_id),
      followup_source_ids: Array.wrap(ticket.followup_source.try(:nice_id)),
      from_archive:        ticket.archived?,
      incidents:           ticket.count_incidents
    }

    super.merge!(json)
  end

  def url(ticket)
    url_builder.send(:related_api_v2_ticket_url, ticket, format: :json)
  end
end
