require 'zendesk/sla'
require 'set'

class Api::V2::Tickets::TicketPermissionsPresenter < Api::V2::Presenter
  self.model_key = :permissions

  def model_json(ticket)
    json = {
      can_update_ticket:                       ticket.can_be_updated? && user.can?(:edit_properties, ticket),
      # :can_edit_ticket_properties is an alias to :can_update_ticket. Lotus and mobile clients will migrate
      # from using :can_update_ticket to :can_edit_ticket_properties.
      # See discussion on https://github.com/zendesk/zendesk/pull/5437
      can_edit_ticket_properties:              ticket.can_be_updated? && user.can?(:edit_properties, ticket),
      can_delete_ticket:                       user.can?(:delete, ticket),
      can_merge_ticket:                        ticket.can_be_updated? && user.can?(:merge, ticket),
      can_edit_ticket_tags:                    ticket.can_be_updated? && user.can?(:edit_tags, ticket),
      can_make_comments:                       ticket.can_be_updated? && user.can?(:add, Comment),
      can_make_public_comments:                ticket.can_be_updated? && user.can?(:publicly, Comment),
      can_mark_as_spam:                        ticket.can_be_updated? && user.can?(:mark_as_spam, ticket),
      can_create_followup_ticket:              !ticket.can_be_updated?,
      can_read_problem:                        ticket.problem && user.can?(:view, ticket.problem),
      can_view_ticket_satisfaction_prediction: user.can?(:view_satisfaction_prediction, Ticket)
    }

    json.merge!(extra_permissions(ticket)) if side_load?(:extended_permissions)

    json
  end

  def extra_permissions(ticket)
    {
      can_view_ticket: user.can?(:view, ticket)
    }
  end
end
