class Api::V2::Tickets::RuleViaPresenter
  def model_json(rule_reference)
    {
      to:   {},
      from: from_source(rule_reference),
      rel:  rule_reference.rule_type
    }
  end

  private

  def from_source(rule_reference)
    {deleted: rule_reference.deleted?, title: rule_reference.title}.tap do |json|
      json[:id] = rule_reference.rule_id if rule_reference.viewable?
    end
  end
end
