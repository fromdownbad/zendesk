class Api::V2::Tickets::AuditEventPresenter < Api::V2::Presenter
  include Api::V2::Tickets::EventViaPresenter

  self.model_key = :event

  def collection_presenter
    @collection_presenter ||= Api::V2::Tickets::AuditEventCollectionPresenter.new(user, options.merge(item_presenter: self, includes: includes))
  end

  def model_json(event)
    json = {
      id: event.id,
      type: event.type
    }

    if via = serialized_via_object(event)
      json[:via] = via
    end

    if options[:include_ticket_id].present?
      json[:ticket_id] = event.ticket.nice_id
    end

    if event.is_a?(CommentRedactionEvent)
      json[:comment_id] = event.comment_id
    end

    if event.is_a?(TicketFieldRedactEvent)
      # nil check here in case ticket field was deleted since event creation
      json[:ticket_field_id] = event.ticket_field_entry && event.ticket_field_entry.ticket_field_id
    end

    if event.is_a?(AttachmentRedactionEvent)
      json[:attachment_id] = event.attachment_id
      json[:comment_id] = event.comment_id
    end

    if event.is_a?(ChannelBackEvent)
      json[:value] = event.to_s
    end

    if event.is_a?(ScheduleAssignment)
      json[:previous_schedule_id] = event.previous_schedule_id
      json[:new_schedule_id] = event.new_schedule_id
    end

    if event.account && event.account.has_plain_body_on_all_comment_types? && event.respond_to?(:plain_body)
      json[:plain_body] = event.plain_body
    end

    if event.is_a?(AssociateAttValsEvent)
      json[:attribute_values] = event.attribute_values
    end

    if event.is_a?(ChannelBackFailedEvent)
      json[:description] = event.value[:description]
    end

    json
  end
end
