require "events/ticket_merge_audit"
module Api::V2::Tickets::EventViaPresenter
  include Zendesk::Types
  include Voice::Core::NumberSupport
  include Api::V2::Tickets::CcsAndFollowersHelper

  TICKET_VIA_TYPES = [ViaType.LINKED_PROBLEM, ViaType.MERGE, ViaType.CLOSED_TICKET].freeze

  # See methods that reference a ticket in #source_object
  # We use that Array to know for which event we need to load a ticket
  VIA_TYPES_LOADING_TICKETS = [
    ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE,
    ViaType.VOICEMAIL, ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND,
    ViaType.API_VOICEMAIL, ViaType.API_PHONE_CALL_INBOUND,
    ViaType.API_PHONE_CALL_OUTBOUND, ViaType.HELPCENTER, ViaType.ANY_CHANNEL,
    ViaType.TOPIC
  ].to_set

  VIA_ID_MAP = {
    ViaType.WEB_FORM                                  => :web,
    ViaType.DROPBOX                                   => :web,
    ViaType.WEB_WIDGET                                => :web,
    ViaType.BATCH                                     => :web,
    ViaType.MAIL                                      => :email,
    ViaType.CLOSED_TICKET                             => :web,
    ViaType.RULE                                      => :rule,
    ViaType.RULE_REVISION                             => :rule,
    ViaType.WEB_SERVICE                               => :api,
    ViaType.TICKET_SHARING                            => :api,
    ViaType.BLOG                                      => :api,
    ViaType.GITHUB                                    => :api,
    ViaType.IMPORT                                    => :api,
    ViaType.TOPIC                                     => :forum,
    ViaType.TWITTER_FAVORITE                          => :twitter,
    ViaType.TWITTER                                   => :twitter,
    ViaType.TWITTER_DM                                => :twitter,
    ViaType.CHAT                                      => :chat,
    ViaType.VOICEMAIL                                 => :voice,
    ViaType.PHONE_CALL_INBOUND                        => :voice,
    ViaType.PHONE_CALL_OUTBOUND                       => :voice,
    ViaType.FACEBOOK_POST                             => :facebook,
    ViaType.FACEBOOK_MESSAGE                          => :facebook,
    ViaType.API_VOICEMAIL                             => :api,
    ViaType.API_PHONE_CALL_INBOUND                    => :api,
    ViaType.API_PHONE_CALL_OUTBOUND                   => :api,
    ViaType.MOBILE_SDK                                => :mobile_sdk,
    ViaType.HELPCENTER                                => :help_center,
    ViaType.SAMPLE_TICKET                             => :sample_ticket,
    ViaType.SAMPLE_INTERACTIVE_TICKET                 => :sample_ticket,
    ViaType.MOBILE                                    => :mobile,
    ViaType.SIDE_CONVERSATION                         => :side_conversation,
    ViaType.SMS                                       => :sms,
    ViaType.ANSWER_BOT_API                            => :answer_bot_api,
    ViaType.ANSWER_BOT_FOR_WEB_WIDGET                 => :answer_bot_for_web_widget,
    ViaType.ANSWER_BOT_FOR_AGENTS                     => :answer_bot_for_agents,
    ViaType.ANSWER_BOT_FOR_SLACK                      => :answer_bot_for_slack,
    ViaType.ANSWER_BOT_FOR_SDK                        => :answer_bot_for_sdk,
    ViaType.LOTUS                                     => :web,
    ViaType.ANY_CHANNEL                               => :any_channel,
    ViaType.LINE                                      => :line,
    ViaType.WECHAT                                    => :wechat,
    ViaType.WHATSAPP                                  => :whatsapp,
    ViaType.NATIVE_MESSAGING                          => :native_messaging,
    ViaType.MAILGUN                                   => :mailgun,
    ViaType.MESSAGEBIRD_SMS                           => :messagebird_sms,
    ViaType.SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER => :sunshine_conversations_facebook_messenger,
    ViaType.TELEGRAM                                  => :telegram,
    ViaType.TWILIO_SMS                                => :twilio_sms,
    ViaType.SUNSHINE_CONVERSATIONS_TWITTER_DM         => :sunshine_conversations_twitter_dm,
    ViaType.VIBER                                     => :viber,
    ViaType.GOOGLE_RCS                                => :google_rcs,
    ViaType.APPLE_BUSINESS_CHAT                       => :apple_business_chat,
    ViaType.GOOGLE_BUSINESS_MESSAGES                  => :google_business_messages,
    ViaType.KAKAOTALK                                 => :kakaotalk,
    ViaType.INSTAGRAM_DM                              => :instagram_dm,
    ViaType.SUNSHINE_CONVERSATIONS_API                => :sunshine_conversations_api,
    ViaType.CHAT_TRANSCRIPT                           => :chat_transcript
  }.freeze

  REL_MAP = {
    ViaType.LINKED_PROBLEM          => :problem,
    ViaType.MERGE                   => :merge,
    ViaType.USER_MERGE              => :user_merge,
    ViaType.CLOSED_TICKET           => :follow_up,
    ViaType.DROPBOX                 => :feedback_tab,
    ViaType.WEB_WIDGET              => :web_widget,
    ViaType.BATCH                   => :batch,
    ViaType.TICKET_SHARING          => :ticket_sharing,
    ViaType.BLOG                    => :blog,
    ViaType.GITHUB                  => :github,
    ViaType.TWITTER_FAVORITE        => :favorite,
    ViaType.TWITTER                 => :mention,
    ViaType.TWITTER_DM              => :direct_message,
    ViaType.VOICEMAIL               => :voicemail,
    ViaType.PHONE_CALL_INBOUND      => :inbound,
    ViaType.PHONE_CALL_OUTBOUND     => :outbound,
    ViaType.FACEBOOK_POST           => :post,
    ViaType.FACEBOOK_MESSAGE        => :message,
    ViaType.IMPORT                  => :import,
    ViaType.API_VOICEMAIL           => :voicemail,
    ViaType.API_PHONE_CALL_INBOUND  => :inbound,
    ViaType.API_PHONE_CALL_OUTBOUND => :outbound,
    ViaType.MOBILE_SDK              => :mobile_sdk,
    ViaType.ADMIN_SETTING           => :admin_setting,
    ViaType.MOBILE                  => :mobile,
    ViaType.CHAT_OFFLINE_MESSAGE    => :chat_offline_message
  }.freeze

  ## The via object of a ticket audit or audit event tells you how or why the audit or event was created.
  ## Via objects have the following keys:
  ##
  ## | Name       | Type    | Comment
  ## | ---------- | ------- | -------
  ## | channel    | string  | This tells you how the ticket or event was created. Examples: "web", "mobile", "rule", "system"
  ## | source     | object  | For some channels a source object gives more information about how or why the ticket or event was created
  ##
  ## **Example**
  ##
  ## ```js
  ##  "via": {
  ##    "channel": "rule",
  ##    "source": {
  ##      "to": { },
  ##      "from": {
  ##        "id": 22472716,
  ##        "title": "Assign to first responder"
  ##      },
  ##      "rel": "trigger"
  ##    }
  ##  }
  ## ```
  ##
  ## The `source` attribute gives more information about the source of the ticket. It consists of `from`, `to`, and `rel` attributes. Examples:
  ##
  ## | source                         | from                                         | to                             | rel
  ## | ------                         | ------                                       | ------                         | ------
  ## | an email                       | address, name, original_recipients           | address, name                  | null
  ## | "Submit a request" on website  |                                              |                                | null
  ## | Zendesk widget                 |                                              |                                | zendesk_widget
  ## | Feedback tab                   |                                              |                                | feedback_tab
  ## | Zendesk mobile agent apps      |                                              |                                | mobile
  ## | API - ticket sharing           |                                              |                                | api
  ## | API - ticket endpoints         |                                              |                                | null
  ## | API - follow-up ticket         | ticket_id, subject                           |                                | follow_up
  ## | Business rule (trigger)        | id, title, deleted, revision_id (Enterprise) |                                | trigger
  ## | Business rule (automation)     | id, title, deleted                           |                                | automation
  ## | a forum topic                  | topic_id, topic_name                         |                                | null
  ## | a Twitter message or mention   | profile_url, username, name                  | profile_url, username, name    | direct_message
  ## | a chat                         |                                              |                                | null
  ## | a chat offline message         |                                              |                                | chat_offline_message
  ## | a call                         | phone, formatted_phone, name                 | phone, formatted_phone, name   | voicemail, inbound, or outbound
  ## | a Facebook post or message     | name, profile_url, facebook_id               | name, profile_url, facebook_id | post or message
  ## | system - ticket merged         | ticket_id, subject                           |                                | merge
  ## | system - ticket follow-up      | ticket_id, subject                           |                                | follow_up
  ## | system - problem ticket solved | ticket_id, subject                           |                                | problem
  ## | AnyChannel                     | service_info, supports_channelback, supports_clickthrough, registered_integration_service_name| |
  ##
  def serialized_via_object(event_or_ticket)
    # NOTE: The nil checks on the audit are being done due to bad data in the database for the snapchat account
    # Comment with id #30894015409 is an audit object
    # TODO: find cause of missing audits and remove this patch
    return nil if event_or_ticket.nil?

    # if this is a non-audit and it has the same via as the audit, then don't serialize it.
    if event_or_ticket.is_a?(AuditEvent) &&
       !event_or_ticket.audit.nil? &&
       event_or_ticket.via_id == event_or_ticket.audit.via_id &&
       event_or_ticket.via_reference_id == event_or_ticket.audit.via_reference_id &&
       !event_or_ticket.is_a?(FacebookComment) &&
       !event_or_ticket.is_a?(ChannelBackEvent)
      return nil
    end

    {
      channel: channel(event_or_ticket)
    }.tap do |json|
      source = source_object(event_or_ticket)
      json[:source] = source unless source.nil?
    end
  end

  def channel(event_or_ticket)
    VIA_ID_MAP[event_or_ticket.via_id] || suspended_channel(event_or_ticket) || :system
  end

  def suspended_channel(event)
    return nil unless event.via_id == ViaType.RECOVERED_FROM_SUSPENDED_TICKETS
    recipient = event.respond_to?(:recipient) ? event.recipient : (event.ticket && event.ticket.recipient)
    recipient.present? ? :email : :web
  end

  # Finds a ticket for an event (or returns the ticket if event_or_ticket
  # is a ticket).
  # Note that it can return a nil ticket if the event or its audit has
  # a nil ticket associated (the association is loaded, its value is nil, and
  # it will NOT fire a DB request). We do that in TicketEventsPresenter when
  # we fetch all tickets for audits, from deleted and archived tickets.
  def find_ticket(event_or_ticket)
    return event_or_ticket if event_or_ticket.is_a?(Ticket)

    event = event_or_ticket

    # event can be an AuditEvent, an Audit, or something else, or multiple things
    # according the usage of #serialized_via_object.
    if (event.association(:ticket).loaded? && event.ticket) || (cached_ticket = ticket_cache[event.try(:ticket_id)])
      # Retrieves the ticket without a DB request when it's already loaded.
      # One example where we load tickets is in TicketEventsPresenter, we fetch
      # some comments, then associate their Audit object with the associated
      # Ticket for each.
      cached_ticket || event.ticket
    elsif event.respond_to?(:audit) && (audit = event.audit) && audit.association(:ticket).loaded? && audit.ticket
      audit.ticket
    elsif (ticket_id = event.try(:ticket_id))
      scope = Ticket

      # The deleted_tickets_lookup option was added in https://github.com/zendesk/zendesk/pull/26700
      # and is used in api/v2/exports/ticket_events_presenter.rb
      # See discussion in the PR.
      # Because there are so few endpoints that are supposed to display data
      # from deleted tickets, I chose to make that explicit and working in this
      # case only.
      # If other presenters need that option at some point, they just need to set
      # that option to true as well.
      scope = scope.unscoped if options[:deleted_tickets_lookup]

      # In some cases ticket.nice_id (instead of ticket.id) is received as ticket_id
      # particularly few cases were encountered with VoiceComment where the ticket_id
      # was overridden by  Api::V2::Exports::TicketEventsPresenter#preload_comments
      ticket = scope.where(account_id: Integer(event.account_id), id: ticket_id).all_with_archived.first
      ticket_cache[ticket_id] = ticket if Arturo.feature_enabled_for?(:event_via_ticket_cache, account)
      if account.try(:has_attempt_to_find_ticket_by_ticket_id_from_audit?) && ticket.nil?
        Rails.logger.warn("Ticket with id #{ticket_id} was not found for event with id #{event.id} and type #{event.try(:type)} for account id #{account.id}")
        statsd_client.increment('find_ticket_by_ticket_id.fail', tags: ["account_id:#{account.id}"])
        if event.respond_to?(:audit)
          ticket = scope.where(account_id: Integer(event.account_id), id: event.audit.ticket_id).all_with_archived.first
          Rails.logger.warn("Ticket was found instead with id #{event.audit.ticket_id} and nice_id #{ticket.nice_id} for account id #{account.id}") if ticket
        end
      end

      ticket
    end
  end

  def ticket_cache
    @ticket_cache ||= {}
  end

  def source_object(event_or_ticket)
    case event_or_ticket.via_id
    when ViaType.RULE
      rule_source(event_or_ticket)
    when ViaType.RULE_REVISION
      rule_revision_source(event_or_ticket.via_reference_id)
    when ViaType.CLOSED_TICKET
      closed_ticket_source(event_or_ticket)
    when ViaType.MERGE
      merge_source(event_or_ticket)
    when ViaType.LINKED_PROBLEM
      linked_source(event_or_ticket)
    when ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE
      facebook_source(event_or_ticket, find_ticket(event_or_ticket))
    when ViaType.TWITTER, ViaType.TWITTER_DM, ViaType.TWITTER_FAVORITE
      if Arturo.feature_enabled_for?(:twitter_via_source_attributes, account)
        twitter_source(event_or_ticket)
      else
        {
          from: {}, to: {},
          rel: REL_MAP[event_or_ticket.via_id]
        }
      end
    when ViaType.MAIL
      mail_source(event_or_ticket, find_ticket(event_or_ticket))
    when ViaType.VOICEMAIL, ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND
      voice_source(event_or_ticket, find_ticket(event_or_ticket))
    when ViaType.API_VOICEMAIL, ViaType.API_PHONE_CALL_INBOUND, ViaType.API_PHONE_CALL_OUTBOUND
      voice_api_source(event_or_ticket)
    when ViaType.TOPIC
      ticket = find_ticket(event_or_ticket)
      from = ticket.entry
      {
        from: {
          topic_id: from.try(:id),
          topic_name: from.try(:name)
        },
        to: {},
        rel: REL_MAP[event_or_ticket.via_id]
      }
    when ViaType.HELPCENTER
      ticket = find_ticket(event_or_ticket)
      hc_ticket = HelpCenterTicket.new(ticket)
      {
        from: {
          post_id: hc_ticket.post_id,
          post_name: hc_ticket.post_name,
          post_url: hc_ticket.post_url
        },
        to: {},
        rel: nil
      }
    when ViaType.ADMIN_SETTING
      setting_source(event_or_ticket)
    when ViaType.ANY_CHANNEL
      any_channel_source(event_or_ticket, find_ticket(event_or_ticket))
    when ViaType.WEB_SERVICE, ViaType.WEB_FORM
      web_source(event_or_ticket)
    when ViaType.CHAT
      chat_source(event_or_ticket)
    else
      {
        from: {}, to: {},
        rel: REL_MAP[event_or_ticket.via_id]
      }
    end
  end

  def lookup_via_rule_association(rule_id)
    return nil if rule_id.blank?

    if options[:event_via_associations].present?
      # options[:event_via_associations] is prepared by AuditEventPresenter#preload_via_associations
      options[:event_via_associations][:rules][rule_id.to_i]
    else
      Rule.with_deleted do
        account.rules.find_by_id(rule_id)
      end
    end
  end

  def lookup_via_ticket_association(ticket_id)
    return nil if ticket_id.blank?

    if @tickets_cache_by_id && @tickets_cache_by_id.key?(ticket_id)
      # Returns the value even if it's nil, we know that ticket does not exist
      @tickets_cache_by_id[ticket_id]
    elsif options[:event_via_associations].present? &&
          options[:event_via_associations][:tickets].present?
      # options[:event_via_associations] is prepared by AuditEventPresenter#preload_via_associations
      options[:event_via_associations][:tickets][ticket_id.to_i]
    else
      Ticket.with_deleted do
        begin
          ticket_id = Integer(ticket_id)
          account.tickets.find_by_id(ticket_id)
        rescue TypeError
          nil
        end
      end
    end
  end

  def lookup_via_nice_ticket_association(ticket_nice_id)
    return nil if ticket_nice_id.blank?

    if options[:event_via_associations].present?
      # options[:event_via_associations] is prepared by AuditEventPresenter#preload_via_associations
      options[:event_via_associations][:nice_tickets][ticket_nice_id.to_i]
    else
      Ticket.with_deleted do
        begin
          nice_id = Integer(ticket_nice_id)
          account.tickets.find_by_nice_id(nice_id)
        rescue TypeError
          nil
        end
      end
    end
  end

  # This method preloads all associations required for the "via" information,
  # in order to avoid n+1 queries when `#serialized_via_object` method is called
  # in some loop by an upstream presenter.
  #
  # For preloading to work, `#event_via_presenter_preload_associations` must be
  # called beforehand.
  #
  # The required associations are different for each via type; hence there needs
  # to be an equivalent preloading logic for each via blob generator, ie
  # `rule_source`, `closed_ticket_source`, `merge_source`, etc.
  #
  # In addition, there are methods that are (at least appear to be written as)
  # helper methods meant to be shared by multiple via types, and each of these
  # also needs to have an equivalent preloading logic.
  # - `#find_ticket` and `#lookup_via_ticket_association` find `Ticket` by `id`
  # - `#lookup_via_nice_ticket_association` finds `Ticket` by `nice_id`
  # - `#lookup_via_rule_association` finds `Rule` by `id`
  #
  # Of the above 3 helper lookup strategies, preloading is implemented for only
  # the first.
  #
  # Currently, preloading is implemented for MERGE and MAIL via types only.
  #
  # In addition, there is `#suspended_channel` for legacy via types. Currently
  # there is no preloading for this, but it shouldn't be hard to implement.
  #
  # !!! We have to find some time to finish implementing preloading for all via
  # types. And whenever the logic for blob generation changes, preloading must
  # also be updated !!!
  #
  # Because `EventViaPresenter` was originally written to handle one item of
  # type `Ticket`, `Event`, or `Audit`, `#event_via_presenter_preload_associations`
  # obeys this (scary) flexibility by assuming that the argument `items` are
  # either all `Ticket`s, all `Event`s, or all `Audit`s.
  #
  # In most use cases, `items` are expected to contain different via types.
  def event_via_presenter_preload_associations(items)
    return if items.empty?

    if items.first.is_a?(Ticket)
      non_tickets = []
      tickets = items
    else
      non_tickets = items
      tickets = load_tickets_by_ids(items)
    end

    cache_tickets_by_ids(tickets)

    source_object_preload(non_tickets, tickets)

    audit_recipients_preload(non_tickets, tickets)
  end

  private

  def voice_source(resource, ticket)
    comment = get_voice_comment_from_resource(resource)
    json = voice_api_source(comment)

    if comment.is_a?(VoiceComment)
      rel = json[:rel]
      json[:from] = {
        formatted_phone: comment.formatted_from,
        phone: comment.data[:from],
        name: from_name(rel, ticket)
      }
      json[:to] = {
        formatted_phone: comment.formatted_to,
        phone: comment.data[:to],
        name: to_name(rel, ticket)
      }

      set_brand(json, rel, comment.data[:brand_id]) if account_has_multibrand? && comment.data[:brand_id]
    end
    json
  end

  def account_has_multibrand?
    @has_multibrand ||= account.has_multibrand?
  end

  def account_has_voice_via_only_voice_comments?
    @has_voice_via_only_voice_comments ||= account.has_voice_via_only_voice_comments?
  end

  def get_voice_comment_from_resource(resource)
    comment = (resource.respond_to?(:comment) && resource.comment.present?) ? resource.comment : resource

    if !comment.is_a?(VoiceComment) && !account_has_voice_via_only_voice_comments?
      if resource.is_a?(Audit) && resource.association(:events).loaded?
        # #to_a is needed in the case the ticket was archived, to avoid
        # ArgumentError: Only find(id) is supported for archived tickets
        voice_comment = resource.events.to_a.find { |c| c.is_a?(VoiceComment) }
      else
        ticket = comment.respond_to?(:ticket) && comment.ticket.present? ? comment.ticket : resource
        if ticket.respond_to?(:comments) && ticket.comments.present?
          voice_comment = ticket.comments.to_a.find { |c| c.is_a?(VoiceComment) }
        end
      end
    end

    # sometimes there's no VoiceComment and we should just use the comment
    # except when that's a hash because of macro application, then fallback to the original
    voice_comment || (comment.respond_to?(:via_id) && comment) || resource
  end

  def voice_api_source(event)
    {
      rel: REL_MAP[event.via_id]
    }
  end

  def from_name(rel, ticket)
    from = rel == :outbound ? account : ticket.requester
    from.name
  end

  def to_name(rel, ticket)
    to = rel == :outbound ? ticket.requester : account
    to.name
  end

  def set_brand(json, rel, brand_id)
    node = rel == :outbound ? json[:from] : json[:to]
    node[:brand_id] = brand_id
  end

  def any_channel_source(event, ticket)
    return {} if ticket.nil?

    service_info = Channels::AnyChannel::RegisteredIntegrationService.service_info_for_ticket(account.id, ticket.id)
    {
      from: {
        service_info: service_info
      },
      to: {},
      rel: REL_MAP[event.via_id]
    }
  end

  def facebook_source(event, ticket)
    return {} if event.is_a?(FacebookEvent)

    to = facebook_to_page(ticket)
    from_name, facebook_id = facebook_from(event, to)

    from_url = "http://www.facebook.com/#{facebook_id}" if facebook_id
    {
      from: {
        name: from_name,
        profile_url: from_url,
        facebook_id: facebook_id
      },
      to: {
        name: to.try(:name),
        profile_url: to.try(:link),
        facebook_id: to.try(:graph_object_id)
      },
      rel: REL_MAP[event.via_id]
    }
  end

  # Returns Twitter sender and receiver info.
  def twitter_source(event_or_ticket)
    # TwitterEvent is a deprecated model.
    return {} if event_or_ticket.is_a?(TwitterEvent)

    {
      from: twitter_sender_details(event_or_ticket),
      to: twitter_recipient_details(event_or_ticket),
      rel: REL_MAP[event_or_ticket.via_id]
    }
  end

  # Returns details for the sender.
  def twitter_sender_details(event_or_ticket)
    # If it's a comment, we want to choose the author. For a ticket we'll take the requester.
    sender = if event_or_ticket.is_a?(Audit) && event_or_ticket.comment.is_a?(Comment)
      event_or_ticket.comment.author
    elsif event_or_ticket.is_a?(Ticket)
      event_or_ticket.requester
    end

    return {} unless sender

    twitter_user_details(sender)
  end

  # Returns details for the recipient.
  def twitter_recipient_details(event_or_ticket)
    if event_or_ticket.is_a?(Audit) && event_or_ticket.comment.is_a?(Comment)
      # If the author is our handle, then we can assume the recipient is the ticket requester.
      # If the author isn't our handle, then we can assume the recipient is our handle.
      author = event_or_ticket.comment.author
      handle = Ticket.with_deleted do
        ticket = event_or_ticket.ticket || event_or_ticket.reload.ticket
        ticket_handle_source(ticket)
      end
      return {} unless handle

      if twitter_id(author) == handle.twitter_user_id.to_s
        recipient = event_or_ticket.ticket.requester
        twitter_user_details(recipient)
      else
        twitter_handle_details(handle)
      end
    elsif event_or_ticket.is_a?(Ticket)
      handle = ticket_handle_source(event_or_ticket)
      return {} unless handle

      twitter_handle_details(handle)
    else
      {}
    end
  end

  # Return details for a given user with a TwitterUserProfile.
  def twitter_user_details(user)
    # Get the external ID of the user.
    user_twitter_id = twitter_id(user)
    return {} unless user_twitter_id

    # Load up the Twitter profile by external ID.
    profile = twitter_user_profile(user_twitter_id)
    return {} unless profile

    {
      name: profile.metadata[:display_name],
      username: profile.name,
      profile_url: "https://www.twitter.com/#{profile.name}",
      twitter_id: user_twitter_id
    }
  end

  # Return details for a given Twitter handle.
  def twitter_handle_details(handle)
    {
      name: nil,
      username: handle.twitter_screen_name,
      profile_url: "https://www.twitter.com/#{handle.twitter_screen_name}",
      twitter_id: handle.twitter_user_id.to_s
    }
  end

  # Returns the handle through which the ticket was initially pulled in.
  def ticket_handle_source(ticket)
    decoration = ticket.event_decorations.first
    return unless decoration

    source = decoration.data.source
    return unless decoration

    account_twitter_handles_by_id[source.zendesk_id]
  end

  def twitter_user_profile(twitter_id)
    Channels::TwitterUserProfile.find_by(account_id: account.id, external_id: twitter_id)
  end

  def facebook_to_page(ticket)
    # Avoids having an exception like
    # undefined method `event_decorations' for nil:NilClass
    # Added in https://github.com/zendesk/zendesk/pull/27746
    # to fix errors like the one reported on https://support.zendesk.com/agent/tickets/2319334
    return unless ticket

    ed = ticket.event_decorations.first
    return unless ed

    source = ed.data.source
    return unless source

    account_facebook_pages_by_id[source.zendesk_id]
  end

  def account_facebook_pages_by_id
    @account_facebook_pages_by_id ||= Facebook::Page.where(account_id: account.id).
      each_with_object({}) { |page, pages| pages[page.id] = page }
  end

  # Builds up a hash memo mapping Twitter handle IDs to handles. The purpose of this memo is that we can re-use queried
  # data when presenting many comments.
  def account_twitter_handles_by_id
    @account_twitter_handle_by_id ||= MonitoredTwitterHandle.where(account_id: account.id).
      each_with_object({}) { |handle, handles| handles[handle.id] = handle }
  end

  def facebook_from(event, to)
    if (event.is_a?(Audit) && event.comment.is_a?(FacebookComment) && requester = event.comment.data[:requester]) ||
      (event.is_a?(FacebookComment) && requester = event.data[:requester])
      [requester[:name], requester[:id]]
    else
      from = event.is_a?(Ticket) ? event.requester : event.author
      [from.name, facebook_identity(from) || to.try(:graph_object_id)]
    end
  end

  def facebook_identity(user)
    user.identities.sort_by(&:priority).detect { |identity| identity.type == 'UserFacebookIdentity' }.try(:value)
  end

  def twitter_id(user)
    user.identities.sort_by(&:priority).detect { |identity| identity.type == 'UserTwitterIdentity' }.try(:value)
  end

  def closed_ticket_source(event)
    # via_followup_source_id is used in Ticket#set_followup_source and then discarded. However, it was never stored
    # in the initial Audit as it should've been. The fix (in 264497d), placed the via_followup_source_id directly
    # into the Audit via_reference_id. This is wrong, since it is always a nice_id and messes with how events are preloaded and
    # our DB standards. This mess fixes that by reading and then updating those via_reference_ids.
    source = { to: {}, from: { ticket_id: nil, subject: nil }, rel: REL_MAP[event.via_id] }

    if event.is_a?(Ticket) && event.via_reference_id.nil?
      event = event.audits.use_index('index_events_on_ticket_id_and_type').first
    end

    if event.respond_to?(:via_reference_id)
      via_ticket = lookup_via_nice_ticket_association(event.via_reference_id)

      if via_ticket && via_ticket.nice_id == event.via_reference_id
        Rails.logger.info("[Via.CLOSED_TICKET] Found event with improper via_reference_id #{event.class}(id: #{event.id}, account_id: #{event.account_id}, readonly: #{event.readonly?})")

        unless event.readonly?
          event.update_column(:via_reference_id, via_ticket.id)
        end
      end

      via_ticket ||= lookup_via_ticket_association(event.via_reference_id)

      source[:to] = event_recipients(event)

      if via_ticket
        source[:from] = {
          ticket_id: via_ticket.nice_id,
          subject: via_ticket.subject,
          channel: channel(via_ticket)
        }
      end
    end

    source
  end

  def rule_source(event)
    rule_via_presenter.model_json(
      Zendesk::Rules::RuleViaReference.new(
        rule: lookup_via_rule_association(event.via_reference_id),
        user: user
      )
    )
  end

  def rule_revision_source(revision_id)
    rule_revision_via_presenter.model_json(
      lookup_via_rule_revision_association(revision_id)
    )
  end

  def merge_source(event)
    source = { to: {}, from: { ticket_id: nil, subject: nil }, rel: REL_MAP[event.via_id] }

    if event.respond_to?(:via_reference_id) && (via_ticket = lookup_via_ticket_association(event.via_reference_id))
      source[:from] = { ticket_id: via_ticket.nice_id, subject: via_ticket.subject }
    elsif event.respond_to?(:merge_type) && event.merge_type == 'target'
      source[:from][:ticket_ids] = event.source_ids
    end

    source
  end

  def linked_source(event)
    source = { to: {}, from: { ticket_id: nil, subject: nil }, rel: REL_MAP[event.via_id] }

    if event.respond_to?(:via_reference_id) && (via_ticket = lookup_via_ticket_association(event.via_reference_id))
      source[:from] = { ticket_id: via_ticket.nice_id, subject: via_ticket.subject }
    end

    source
  end

  def mail_source(event, ticket)
    from = if event.is_a?(SuspendedTicket)
      { address: event.from_mail, name: event.from_name }
    elsif ticket && (req = ticket.requester)
      { address: req.email, name: req.name }
    else
      {}
    end

    if account.has_email_ccs? && (recipients = audit_recipients(event))
      from[:original_recipients] = recipients
    else
      if event.is_a?(Audit) && event.recipients.present?
        from[:original_recipients] = event.recipients.split(" ")
      end
    end

    recipient_source = (event.is_a?(SuspendedTicket) ? event : ticket)
    to = {
      name: account.name # this ideally would be ticket.recipient_address.try(:name) || ticket.brand.name but nobody uses it
    }

    if account.has_follower_and_email_cc_collaborations_enabled? && event.is_a?(Audit)
      if to_address = event.recipients_support_address
        to[:address] = to_address
      else
        add_recipient_source_address(to, recipient_source)
      end
    else
      add_recipient_source_address(to, recipient_source)
    end

    add_email_ccs(to, event) if account.has_comment_email_ccs_allowed_enabled?

    {
      from: from,
      to: to,
      rel: REL_MAP[event.via_id]
    }
  end

  def add_recipient_source_address(to, recipient_source)
    to[:address] = (recipient_source.original_recipient_address || recipient_source.recipient) if recipient_source
  end

  def web_source(event_or_ticket)
    result = {
      from: {},
      to: event_recipients(event_or_ticket),
      rel: nil
    }

    # NOTE: event_or_ticket.via_reference_id is generally null (and it's not even
    # implemented for suspended tickets.)  For Agent as End User tickets,
    # via_reference_id is HELPCENTER.
    result[:rel] = VIA_ID_MAP[event_or_ticket.via_reference_id] if event_or_ticket.respond_to?(:via_reference_id)
    result
  end

  def chat_source(event_or_ticket)
    {
      from: {},
      to: {},
      rel: event_or_ticket.respond_to?(:via_reference_id) ? REL_MAP[event_or_ticket.via_reference_id] : nil
    }
  end

  def setting_source(event)
    { to: {}, from: { id: event.via_reference_id, title: setting_name(event) }, rel: REL_MAP[event.via_id] }
  end

  def setting_name(event)
    return "" unless event.via_reference_id && setting = AccountSetting.find(event.via_reference_id)
    I18n.t("txt.admin.views.reports.tabs.audits.property.object.#{setting.name}")
  end

  # This method must preload all tickets including archived ones.
  def load_tickets_by_ids(non_tickets)
    ticket_id_getters = {
      ViaType.MERGE => :ticket_id_merge,
      ViaType.MAIL  => :ticket_id_mail
    }

    ticket_ids = non_tickets.map do |non_ticket|
      method_name = ticket_id_getters[non_ticket.via_id]
      send(method_name, non_ticket) if method_name
    end.uniq.compact

    Ticket.with_deleted do
      account.tickets.
        where(id: ticket_ids).
        all_with_archived
    end
  end

  def ticket_id_merge(non_ticket)
    non_ticket.via_reference_id
  end

  def ticket_id_mail(non_ticket)
    non_ticket.ticket_id
  end

  # In some weird edge cases, if any ticket existing in `@tickets_cache_by_id`,
  # its preloading would be lost.
  def cache_tickets_by_ids(tickets)
    @tickets_cache_by_id ||= {}
    @tickets_cache_by_id.merge!(tickets.index_by(&:id))
  end

  def source_object_preload(non_tickets, tickets)
    source_object_preloaders = {
      ViaType.MERGE => nil,
      ViaType.MAIL  => :mail_source_preload
    }

    non_tickets_by_via_id = non_tickets.group_by(&:via_id)
    tickets_by_via_id = tickets.group_by(&:via_id)

    via_ids = (non_tickets_by_via_id.keys + tickets_by_via_id.keys).to_set

    via_ids.each do |via_id|
      method_name = source_object_preloaders[via_id]
      non_tickets_for_via_id = non_tickets_by_via_id[via_id] || []
      tickets_for_via_id = tickets_by_via_id[via_id] || []

      send(method_name, non_tickets_for_via_id, tickets_for_via_id) if method_name
    end
  end

  def mail_source_preload(non_tickets, tickets)
    non_tickets.each do |non_ticket|
      non_ticket.association(:ticket).target = @tickets_cache_by_id[non_ticket.ticket_id]
    end

    tickets.pre_load(requester: :identities)
  end

  def audit_recipients_preload(non_tickets, tickets)
    return unless account.has_email_ccs_preload_notifications_with_ccs?
    return unless account.has_comment_email_ccs_allowed_enabled?

    load_audit_recipients_users(account, non_tickets: non_tickets, tickets: tickets)
  end

  def lookup_via_rule_revision_association(revision_id)
    return unless revision_id.present?

    if options[:event_via_associations].present?
      options[:event_via_associations][:rule_revisions][revision_id] ||= rule_revision(revision_id)
    else
      options[:event_via_associations] = {
        rule_revisions: {
          revision_id => rule_revision(revision_id)
        }
      }
    end

    options[:event_via_associations][:rule_revisions][revision_id]
  end

  def rule_revision(revision_id)
    Trigger.with_deleted do
      TriggerRevision.where(account_id: account.id, id: revision_id).includes(:trigger).first
    end
  end

  def rule_via_presenter
    @rule_via_presenter ||= Api::V2::Tickets::RuleViaPresenter.new
  end

  def audit_recipients(event)
    return unless event.is_a?(Audit)
    event.recipients_list
  end

  def load_audit_recipients_users(account, non_tickets: nil, tickets: nil, audit: nil)
    @audit_recipients_users ||= {}

    new_recipients_users = if !non_tickets.nil? || !tickets.nil?
      audit_recipients_users(account, non_tickets, tickets)
    elsif audit
      # Don't reload if this audit's recipients are all already loaded
      ids = audit_recipients_ids(account, [audit], nil)
      missing_ids = ids - @audit_recipients_users.keys

      missing_ids.empty? ? {} : audit_recipients_users(account, [audit], [])
    end

    @audit_recipients_users.merge!(new_recipients_users) if new_recipients_users.present?
    @audit_recipients_users
  end

  def audit_recipients_users_with_ids(ids)
    ids.compact!
    return [] unless ids.present?

    @audit_recipients_users ||= {}
    @audit_recipients_users.values_at(*ids).compact.presence || account.all_users.where(id: ids).to_a
  end

  def event_recipients(event)
    to = {}

    return to unless account.has_comment_email_ccs_allowed_enabled?

    event_requester = event_requester(event)

    if event_requester.present? && event.is_a?(Audit) && event.comment.try(:is_public?)
      to[:name] = event_requester.name
      to[:address] = event_requester.email
    end

    add_email_ccs(to, event)

    to
  end

  def add_email_ccs(to, event)
    return unless event.is_a?(Audit)

    email_ccs = if account.has_email_ccs_preload_notifications_with_ccs?
      load_audit_recipients_users(account, audit: event)
      recipients_ids_or_identifiers(account, event, @audit_recipients_users)
    else
      event.recipients_ids_or_identifiers.presence
    end

    if email_ccs.present?
      to[:email_ccs] = email_ccs
    end
  end

  def event_requester(event)
    return unless event.is_a?(Audit)

    if account.has_email_ccs_preload_notifications_with_ccs?
      load_audit_recipients_users(account, audit: event)
      audit_recipients_users_with_ids([event.recipients_notification_to]).first
    else
      event.recipients_notification_to
    end
  end

  def rule_revision_via_presenter
    @rule_revision_via_presenter ||= Api::V2::Tickets::RuleRevisionViaPresenter.new(user)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[presenter Api_V2_Tickets_EventViaPresenter])
  end
end
