class Api::V2::Tickets::TranslatableErrorPresenter < Api::V2::Tickets::ErrorPresenter
  def model_json(event)
    super.merge(
      message: event.to_s
    )
  end
end
