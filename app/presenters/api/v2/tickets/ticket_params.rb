class Api::V2::Tickets::TicketParams
  include Api::V2::Tickets::AttributeMappings

  def initialize(params)
    @params = params
  end

  def to_hash
    @hash ||= @params.dup.tap do |hash|
      hash[:ticket] = map_ticket_params(hash[:ticket])
    end
  end

  protected

  def map_ticket_params(ticket_params)
    ticket_params.dup.tap do |hash|
      ATTRIBUTE_MAPPINGS.each do |key, value|
        next unless hash.key?(value[:to])

        case value[:using]
        when Hash, ActionController::Parameters # RAILS5UPGRADE
          value[:inverse_using] ||= value[:using].invert
          hash[key] = value[:inverse_using][hash.delete(value[:to])]
        else
          hash[key] = hash.delete(value[:to])
        end
      end
    end
  end
end
