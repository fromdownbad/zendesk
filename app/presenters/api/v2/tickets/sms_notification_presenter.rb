class Api::V2::Tickets::SmsNotificationPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### SMS notification event
  ## An SMS notification was sent by a business rule such as a trigger when the ticket was created or updated.
  ##
  ## Notifications have the following keys:
  ##
  ## | Name            | Type                   | Read-only | Comment
  ## | --------------- | ---------------------- | --------- | -------
  ## | id              | integer                | yes       | Automatically assigned when the event is created
  ## | type            | string                 | yes       | Has the value "SmsNotification"
  ## | body            | string                 | yes       | The message sent to the recipients
  ## | recipients      | array                  | yes       | An array of simple objects with the ids and names of the recipients of this notification
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1275,
  ##   "type":       "SmsNotification"
  ##   "body":       "Ticket #235 has been updated"
  ##   "recipients": [847390, 93905]
  ## }
  ## ```
  def model_json(notification)
    super.except(:via).merge(
      type:       "SmsNotification",
      body:       notification.body,
      recipients: Array.wrap(notification.recipients).map(&:to_i)
    )
  end
end
