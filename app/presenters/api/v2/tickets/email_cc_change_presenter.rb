class Api::V2::Tickets::EmailCcChangePresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Email CC change event
  ##
  ## Email CCs have been added or removed from the ticket.
  ##
  ## Email CC change events have the following keys:
  ##
  ## | Name               | Type                  | Read-only | Comment
  ## | ------------------ | --------------------- | --------- | -------
  ## | id                 | integer               | yes       | Automatically assigned when the event is created
  ## | type               | string                | yes       | Has the value `EmailCcChange`
  ## | previous_email_ccs | array                 | yes       | The previous email CCs on the ticket
  ## | current_email_ccs  | array                 | yes       | The current email CCs on the ticket
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                 1274,
  ##   "type":               "EmailCcChange"
  ##   "previous_email_ccs": ["agent_uno@{subdomain}.com","end_user@example.com"]
  ##   "current_email_ccs":  ["end_user@example.com"]
  ## }
  ## ```
  def model_json(email_cc_change)
    super.merge(
      type: "EmailCcChange",
      previous_email_ccs: email_cc_change.previous_email_ccs,
      current_email_ccs: email_cc_change.current_email_ccs
    )
  end
end
