class Api::V2::Tickets::FacebookEventPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Facebook event
  ## A comment was posted on a Facebook Wall, or a private message was sent to a Facebook Page.
  ##
  ## Facebook events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `FacebookEvent`
  ## | page            | hash    | yes       | The name and graph id of the Facebook Page associated with the event
  ## | communication   | integer | yes       | The Zendesk Support id of the associated communication (wall post or message)
  ## | ticket_via      | string  | yes       | "post" or "message" depending on the association with a Wall post or a private message
  ## | body            | string  | yes       | The value of the message posted to Facebook
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":   1274,
  ##   "type": "FacebookEvent",
  ##   "page": {
  ##     "name": "Zendesk",
  ##     "graph_id": "61675732935"
  ##   },
  ##   "communication": 5,
  ##   "ticket_via": "post",
  ##   "body": "Thanks!"
  ## }
  ## ```
  def model_json(event)
    via = event.ticket.via?(:facebook_post) ? "post" : "message"
    super.merge(
      type: "FacebookEvent",
      ticket_via: via,
      body: event.value
    )
  end
end
