class Api::V2::Tickets::FollowerChangePresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Follower change event
  ##
  ## Followers have been added or removed from the ticket.
  ##
  ## Follower change events have the following keys:
  ##
  ## | Name               | Type                  | Read-only | Comment
  ## | ------------------ | --------------------- | --------- | -------
  ## | id                 | integer               | yes       | Automatically assigned when the event is created
  ## | type               | string                | yes       | Has the value `FollowersChange`
  ## | previous_followers | array                 | yes       | The previous followers on the ticket
  ## | current_followers  | array                 | yes       | The current followers on the ticket
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                 1274,
  ##   "type":               "FollowerChange"
  ##   "previous_followers": ["agent_uno@{subdomain}.com","agent_dos@{subdomain}.com"]
  ##   "current_followers":  ["agent_uno@{subdomain}.com"]
  ## }
  ## ```
  def model_json(follower_change)
    super.merge(
      type: "FollowerChange",
      previous_followers: follower_change.previous_followers,
      current_followers: follower_change.current_followers
    )
  end
end
