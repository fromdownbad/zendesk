class Api::V2::Tickets::CommentPrivacyChangePresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Comment privacy change event
  ## A public comment was marked as private.
  ##
  ## Ticket comment privacy change events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `CommentPrivacyChange`
  ## | comment_id      | integer | yes       | The id if the comment that changed privacy
  ## | public          | boolean | yes       | Tells if the comment was made public or private
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1274,
  ##   "type":       "CommentPrivacyChange",
  ##   "comment_id": 453,
  ##   "public": false
  ## }
  ## ```
  def model_json(event)
    super.merge(
      type: "CommentPrivacyChange",
      comment_id: event.event_id,
      public: event.changed_to == 'public'
    )
  end
end
