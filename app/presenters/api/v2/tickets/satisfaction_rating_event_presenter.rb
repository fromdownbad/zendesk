class Api::V2::Tickets::SatisfactionRatingEventPresenter < Api::V2::Tickets::AuditEventPresenter
  include Api::V2::Tickets::AttributeMappings

  ## #### Satisfaction rating event
  ## Satisfaction rating events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating events
  ## | type            | string  | yes       | Has the value `SatisfactionRating`
  ## | score           | string  | yes       | The rating state "offered", "unoffered", "good", "bad"
  ## | assignee_id     | integer | yes       | Who the ticket was assigned to upon rating time
  ## | body            | string  | yes       | The users comment posted during rating
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":          1274,
  ##   "type":        "SatisfactionRating",
  ##   "score":       "good",
  ##   "assignee_id": 87374,
  ##   "body":        "Thanks, you guys are great!"
  ## }
  ## ```
  def model_json(event)
    super.merge(
      type:        "SatisfactionRating",
      score:       score_attribute(event.score),
      assignee_id: event.assignee_id,
      body:        event.comment
    )
  end

  private

  def score_attribute(score)
    ticket_attribute_value(:satisfaction_score, score.to_s, account)
  end
end
