class Api::V2::Tickets::NotificationPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Notification event
  ## A notification was sent by a business rule such as a trigger when the ticket was created or updated.
  ##
  ## Notifications have the following keys:
  ##
  ## | Name            | Type                   | Read-only | Comment
  ## | --------------- | ---------------------- | --------- | -------
  ## | id              | integer                | yes       | Automatically assigned when the event is created
  ## | type            | string                 | yes       | Has the value `Notification`
  ## | subject         | string                 | yes       | The subject of the message sent to the recipients
  ## | body            | string                 | yes       | The message sent to the recipients
  ## | recipients      | array                  | yes       | An array of simple objects with the ids and names of the recipients of this notification
  ## | via             | [Via](#the-via-object) | yes       | The business rule that created the notification
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1275,
  ##   "type":       "Notification"
  ##   "subject":    "Your ticket has been updated"
  ##   "body":       "Ticket #235 has been updated"
  ##   "recipients": [847390, 93905],
  ##   "via": {
  ##     "channel": "system",
  ##     "source": {
  ##       "type":  "rule",
  ##       "id":    61,
  ##       "title": "Notify assignee of comment update"
  ##     }
  ##   }
  ## }
  ## ```
  ##
  ## #### Notification with CCs event
  ##
  ## A notification was sent to the requester and email CCs.
  ##
  ## Notifications have the following keys:
  ##
  ## | Name            | Type                   | Read-only | Comment
  ## | --------------- | ---------------------- | --------- | -------
  ## | id              | integer                | yes       | Automatically assigned when the event is created
  ## | type            | string                 | yes       | Has the value `NotificationWithCcs`
  ## | subject         | string                 | yes       | The subject of the message sent to the recipients
  ## | body            | string                 | yes       | The message sent to the recipients
  ## | recipients      | array                  | yes       | An array of simple objects with the ids and names of the recipients of this notification
  ## | via             | [Via](#the-via-object) | yes       | The business rule that created the notification
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1275,
  ##   "type":       "NotificationWithCcs"
  ##   "subject":    "Your ticket has been updated"
  ##   "body":       "Ticket #235 has been updated"
  ##   "recipients": [847390, 93905],
  ##   "via": {
  ##     "channel": "system",
  ##     "source": {
  ##       "type":  "rule",
  ##       "id":    61,
  ##       "title": "Notify requester and email CCs of comment update"
  ##     }
  ##   }
  ## }
  ## ```
  def model_json(notification)
    super.merge(
      type:       "Notification",
      subject:    notification.subject,
      body:       notification.body,
      recipients: Array.wrap(notification.recipients).map(&:to_i)
    )
  end
end
