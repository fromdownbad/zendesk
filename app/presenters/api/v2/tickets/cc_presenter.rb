class Api::V2::Tickets::CcPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### CC event
  ## A cc (also known as a collaborator) was notified when the ticket was updated.
  ##
  ## Ticket CC Events have the following keys:
  ##
  ## | Name            | Type                   | Read-only | Comment
  ## | --------------- | ---------------------- | --------- | -------
  ## | id              | integer                | yes       | Automatically assigned when the event is created
  ## | type            | string                 | yes       | Has the value `Cc`
  ## | body            | string                 | yes       | The message sent to the recipients
  ## | recipients      | array                  | yes       | A array of simple objects with the ids and names of the recipients of this notification
  ## | via             | [Via](#the-via-object) | yes       | A reference to the business rule that created this notification
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1275,
  ##   "type":       "Cc"
  ##   "recipients": [93905],
  ##   "body": "You are registered as a CC on this request ({{ticket.id}}). Reply to this email to add a comment to the request.\n\n{{ticket.comments_formatted}}",
  ##   "via": {
  ##     "channel": "system",
  ##     "source": {
  ##       "type":  "rule",
  ##       "id":    62,
  ##       "title": "Notify collaborator of comment update"
  ##     }
  ##   }
  ## }
  ## ```
  def model_json(notification)
    # NOTE: `body` is the Event's template source (not the email body sent)
    super.merge(
      type: 'Cc',
      body: notification.account.cc_email_template,
      recipients: Array.wrap(notification.recipients).map(&:to_i)
    )
  end
end
