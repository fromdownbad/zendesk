class Api::V2::Tickets::CommentCollectionPresenter < Api::V2::Tickets::AuditEventCollectionPresenter
  include Api::V2::Tickets::EventViaPresenter

  def model_json(collection)
    collection = Array(collection)

    preload_attachments(collection)

    # We use the author in Comment#html_body
    collection.pre_load(:author)

    shared_url_builder = Zendesk::Tickets::UrlBuilder.new(account: account, for_agent: user.is_agent?)

    begin
      audits = collection.map(&:audit).compact
      event_via_presenter_preload_associations(audits)
    rescue => e
      Rails.logger.error("Attempt from CommentCollectionPresenter to preload for EventViaPresenter failed. N+1 requests are likely to follow. #{e.message}")
      statsd_client.increment('preload_associations_for_via.fail',
        tags: ["controller:#{url_builder.class.name}"])
    end

    collection.map do |comment|
      # Instead of instantiating Zendesk::Tickets::UrlBuilder for each comment
      # we'll reuse the one we created above
      comment.url_builder = shared_url_builder

      via = serialized_via_object(comment.audit)

      # Avoids having n+1 requests on Account by having the same object
      comment.account = account if account

      json = presenter_for(comment.class).model_json(comment).merge(
        via: via,
        created_at: comment.created_at
      )

      # When we export comments from Api::V2::Exports::TicketEventsPresenter
      # we don't want metadata in each comment because they are part of the Audit
      unless options[:without_metadata]
        # This guard is to prevent an exception when bad child and/or parent event data
        # is present (eg. events with all fields set to nil or 0)
        json[:metadata] = comment.audit.try(:metadata) || {}
        if comment.event_decoration.present?
          json[:metadata] = json[:metadata].merge(decoration: comment.event_decoration.data)
        end
      end

      json
    end
  end

  def side_loads(comments)
    json = {}
    comments = Array(comments)

    if side_load?(:users)
      author_and_recipient_ids = comments.map do |comment|
        ids = [comment.author_id, comment.audit.try(:author_id)]
        if account.has_comment_email_ccs_allowed_enabled?
          ids += comment.audit.try(:active_recipients_ids).to_a
        end
        ids
      end

      author_and_recipient_ids.flatten!
      author_and_recipient_ids.compact!
      author_and_recipient_ids.uniq!

      users = account.all_users.where(id: author_and_recipient_ids).to_a
      user_presenter.preload_associations(users)
      json[:users] = user_presenter.collection_presenter.model_json(users)
    end

    json
  end

  def user_presenter
    @user_presenter ||= options[:user_presenter] ||= Api::V2::Users::Presenter.new(user, options)
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[presenter Api_V2_Tickets_CommentCollectionPresenter])
  end

  # Preloads attachments for comments
  def preload_attachments(collection)
    collection.pre_load(:attachments)

    # The following lines fixe a bug: when an attachment has been found for an
    # archived comment, it will mark the association as not loaded.
    # Possibly a bug with has_many association, because it works fine
    # when preloading authors.
    collection.each do |comment|
      unless comment.association(:attachments).loaded?
        comment.association(:attachments).target = []
      end
    end
  end
end
