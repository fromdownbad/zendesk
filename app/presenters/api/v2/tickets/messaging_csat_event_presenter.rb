class Api::V2::Tickets::MessagingCsatEventPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Messaging CSAT event
  ## A Messaging CSAT was sent by a business rule such as a trigger when the ticket created via messaging was updated as solved.
  ##
  ## Notifications have the following keys:
  ##
  ## | Name            | Type                   | Read-only | Comment
  ## | --------------- | ---------------------- | --------- | -------
  ## | id              | integer                | yes       | Automatically assigned when the event is created
  ## | type            | string                 | yes       |
  ## | via             | [Via](#the-via-object) | yes       | The business rule that created the event
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1275,
  ##   "type":       "MessagingCSATEvent"
  ##   "via": {
  ##     "channel": "system",
  ##     "source": {
  ##       "type":  "rule",
  ##       "id":    61,
  ##       "title": "Send Messaging CSAT"
  ##     }
  ##   }
  ## }
  ## ```
  def model_json(notification)
    super.merge(
      type: "MessagingCSATEvent"
    )
  end
end
