class Api::V2::Tickets::AuditEventCollectionPresenter < Api::V2::CollectionPresenter
  include Api::Presentation::MixedCollection

  def self.presenters
    @presenters ||= {
      AuditEvent              => Api::V2::Tickets::AuditEventPresenter,
      Cc                      => Api::V2::Tickets::CcPresenter,
      Change                  => Api::V2::Tickets::ChangePresenter,
      Comment                 => Api::V2::Tickets::CommentPresenter,
      CommentPrivacyChange    => Api::V2::Tickets::CommentPrivacyChangePresenter,
      Create                  => Api::V2::Tickets::CreatePresenter,
      EmailCcChange           => Api::V2::Tickets::EmailCcChangePresenter,
      Error                   => Api::V2::Tickets::ErrorPresenter,
      External                => Api::V2::Tickets::ExternalPresenter,
      FacebookEvent           => Api::V2::Tickets::FacebookEventPresenter,
      FollowerNotification    => Api::V2::Tickets::FollowerNotificationPresenter,
      FollowerChange          => Api::V2::Tickets::FollowerChangePresenter,
      LogMeInTranscript       => Api::V2::Tickets::LogMeInTranscriptPresenter,
      MacroReference          => Api::V2::Tickets::MacroReferencePresenter,
      MessagingCsatEvent      => Api::V2::Tickets::MessagingCsatEventPresenter,
      Notification            => Api::V2::Tickets::NotificationPresenter,
      NotificationWithCcs     => Api::V2::Tickets::NotificationPresenter,
      SmsNotification         => Api::V2::Tickets::SmsNotificationPresenter,
      OrganizationActivity    => Api::V2::Tickets::OrganizationActivityPresenter,
      Push                    => Api::V2::Tickets::PushPresenter,
      SatisfactionRatingEvent => Api::V2::Tickets::SatisfactionRatingEventPresenter,
      TicketSharingEvent      => Api::V2::Tickets::TicketSharingEventPresenter,
      TicketUnshareEvent      => Api::V2::Tickets::TicketSharingEventPresenter,
      TranslatableError       => Api::V2::Tickets::TranslatableErrorPresenter,
      TwitterAction           => ::Channels::Api::V2::Tickets::TwitterActionPresenter, # TODO: Channels.  TwitterActionPresenter is defined in the zendesk_channels repo.
      TwitterEvent            => Api::V2::Tickets::TwitterEventPresenter,
      # Can trigger n+1 requests to attachments, eg.
      # SELECT `attachments`.* FROM `attachments` WHERE `attachments`.`parent_id` IN (20293481)
      VoiceComment            => Api::V2::Tickets::VoiceCommentPresenter,
      VoiceApiComment         => Api::V2::Tickets::VoiceCommentPresenter,
      ChatStartedEvent        => Api::V2::Tickets::ChatEventPresenter,
      ChatEndedEvent          => Api::V2::Tickets::ChatEventPresenter,
      FacebookComment         => Api::V2::Tickets::FacebookCommentPresenter,
      SlaTargetChange         => Api::V2::Tickets::SlaTargetChangePresenter,
      Workspace               => Api::V2::WorkspacePresenter,
      AutomaticAnswerSend     => Api::Lotus::ConversationItemPresenter,
      AutomaticAnswerSolve    => Api::Lotus::ConversationItemPresenter,
      AutomaticAnswerViewed   => Api::Lotus::ConversationItemPresenter,
      AutomaticAnswerReject   => Api::Lotus::ConversationItemPresenter,
      AnswerBotNotification   => Api::Lotus::ConversationItemPresenter,
      KnowledgeLinked         => Api::Lotus::KnowledgeEventPresenter,
      KnowledgeCaptured       => Api::Lotus::KnowledgeEventPresenter,
      KnowledgeFlagged        => Api::Lotus::KnowledgeEventPresenter,
      KnowledgeLinkAccepted   => Api::Lotus::KnowledgeEventPresenter,
      KnowledgeLinkRejected   => Api::Lotus::KnowledgeEventPresenter,
      CollabThreadCreated     => Api::Lotus::CollaborationEventPresenter,
      CollabThreadClosed      => Api::Lotus::CollaborationEventPresenter,
      CollabThreadReopened    => Api::Lotus::CollaborationEventPresenter,
      CollabThreadReply       => Api::Lotus::CollaborationEventPresenter,
      WorkspaceChanged        => Api::Lotus::WorkspaceChangedPresenter,
    }
  end

  def self.via_objects_from_events(events, account)
    rule_ids          = []
    rule_revision_ids = []
    ticket_ids        = []
    ticket_nice_ids   = []
    macros_ids        = []

    Array(events).each do |event|
      case event.via_id
      when Zendesk::Types::ViaType.RULE
        rule_ids << event.via_reference_id if event.via_reference_id
      when Zendesk::Types::ViaType.RULE_REVISION
        rule_revision_ids << event.via_reference_id if event.via_reference_id
      when Zendesk::Types::ViaType.LINKED_PROBLEM, Zendesk::Types::ViaType.MERGE, Zendesk::Types::ViaType.CLOSED_TICKET
        if event.via_reference_id
          if event.via_id == Zendesk::Types::ViaType.CLOSED_TICKET && event[:value].nil?
            ticket_nice_ids << event.via_reference_id
          else
            ticket_ids << event.via_reference_id
          end
        end
      end

      macros_ids << event.macro_id if macro_reference_event?(event)
    end

    hash_from_records = lambda do |klass, ids, id_column = :id|
      association_name = klass.to_s.downcase.pluralize
      ids = ids.uniq.compact
      return {} if ids.empty?
      records = klass.with_deleted { account.send(association_name).where(id_column => ids).to_a }
      Hash[records.map { |r| [r.send(id_column), r] }]
    end

    macro_map = hash_from_records.call(Macro, macros_ids)
    rule_map = hash_from_records.call(Rule, rule_ids)
    ticket_map = hash_from_records.call(Ticket, ticket_ids)
    nice_ticket_map = hash_from_records.call(Ticket, ticket_nice_ids, :nice_id)

    {
      macros:         macro_map,
      rules:          rule_map,
      rule_revisions: rule_revisions(rule_revision_ids, account),
      tickets:        ticket_map,
      nice_tickets:   nice_ticket_map
    }
  end

  def self.rule_revisions(revision_ids, account_id)
    return {} unless revision_ids.present?

    Trigger.with_deleted do
      TriggerRevision.
        where(account_id: account_id, id: revision_ids).
        includes(:trigger).
        each_with_object({}) do |revision, rule_revisions_map|
          rule_revisions_map[revision.id] = revision
        end
    end
  end

  def preload_associations(event)
    super
    return unless event.present?

    events = Array(event)

    return unless events[0].is_a?(Event)

    preload_via_associations(events)
    # preload_comment_associations MUST be called after preload_via_associations
    # the former calls #presenter_for, which memoizes classes and the corresponding
    # presenter. If we do it first, then the second time it won't be initialized
    # with the proper options from the collection (in particular
    # :event_via_associations which stores preloaded objects) and will provoke
    # n+1 requests.
    preload_comment_associations(events)
    preload_problem_ticket_associations(events)

    preload_voice_comment_data_answered_by_users(events)
  end

  def preload_comment_associations(events)
    comments = events.select { |e| e.is_a?(Comment) }

    comments.pre_load(presenter_for(Comment).association_preloads)
  end

  def preload_via_associations(events)
    hash = self.class.via_objects_from_events(events, account)
    options[:event_via_associations] ||= {}
    options[:event_via_associations].deep_merge!(hash)
  end

  def preload_problem_ticket_associations(events)
    ticket_ids = []

    events.each do |event|
      next unless event.is_a?(Create) || event.is_a?(Change)
      next unless event.field_name == "linked_id"
      ticket_ids << event.value
      ticket_ids << event.value_previous if event.is_a?(Change)
    end

    ticket_ids.compact!
    ticket_ids.uniq!

    tickets = ticket_ids.any? ? Ticket.with_deleted { account.tickets.where(id: ticket_ids) } : []
    ticket_map = {}
    tickets.each { |t| ticket_map[t.id] = t }

    options[:problem_ticket_associations] = ticket_map
  end

  # Will do one SQL request to fetch all agents based on voice comment `answered_by_id`s.
  # Then it will assign a user to the corresponding event, so it doesn't do n+1 queries
  # inside VoiceComment when the agent is not set.
  # Note that the agent can be assigned nil so VoiceComment does not try unnecessarily
  # to fetch it.
  def preload_voice_comment_data_answered_by_users(events)
    voice_comments = events.select { |e| e.is_a?(VoiceComment) && e.answered_by_id }

    # We can't use a lazy enumerator here because lazy enums can't respond to `empty?` unless you
    # force evaluation. Since empty? is our first check it doesn't make sense to try to be lazy.
    agent_ids = voice_comments.map(&:answered_by_id).uniq

    return if agent_ids.empty?

    agent_hash = Hash[
      account.users.where(id: agent_ids).map { |u| [u.id, u] }
    ]

    voice_comments.each do |event|
      # user can be nil on purpose, see VoiceComment#agent=
      event.agent = agent_hash[event.answered_by_id]
    end
  end

  def self.macro_reference_event?(event)
    return false if event.is_a?(Ticket)

    event.type == 'MacroReference' || event.type == 'AgentMacroReference'
  end
end
