class Api::V2::Tickets::ErrorPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Error event
  ##
  ## An error occurred during the processing of the ticket.
  ##
  ## Ticket errors have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is creating
  ## | type            | string  | yes       | Has the value `Error`
  ## | message         | string  | yes       | The error message
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":      1274,
  ##   "type":    "Error",
  ##   "message": 453
  ## }
  ## ```
  def model_json(error)
    super.merge(
      type: "Error",
      message: error.value
    )
  end
end
