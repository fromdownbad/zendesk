class Api::V2::Tickets::EmailCommentIssuePresenter < Api::V2::Presenter
  self.model_key = :ticket

  def model_json(ticket)
    {
      ticket_id: ticket["id"],
      z1_request_url: "https://#{domain}/hc/requests/#{ticket["id"]}"
    }
  end

  private

  def domain
    @domain ||= "support.#{ENV.fetch('ZENDESK_HOST')}"
  end
end
