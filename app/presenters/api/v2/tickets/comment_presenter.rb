class Api::V2::Tickets::CommentPresenter < Api::V2::Tickets::AuditEventPresenter
  self.model_key = :comment

  ## ### JSON Format
  ##
  ## A ticket comment is represented as a JSON object named "comment" with the following properties:
  ##
  ## | Name                  | Type    | Read-only | Comment
  ## | --------------------- | ------- | --------- | -------
  ## | id                    | integer | yes       | Automatically assigned when the comment is created
  ## | type                  | string  | yes       | `Comment` or `VoiceComment`. The JSON object for adding voice comments to tickets is different. See [JSON Format for Voice Comments](#json-format-for-voice-comments)
  ## | body                  | string  | no        | The comment string
  ## | html_body             | string  | no        | The comment formatted as HTML
  ## | plain_body            | string  | yes       | The comment as plain text
  ## | public                | boolean | no        | true if a public comment; false if an internal note. The initial value set on ticket creation persists for any additional comment unless you change it
  ## | author_id             | integer | no        | The id of the comment author. See [Author id](#author-id)
  ## | attachments           | array   | yes       | Attachments, if any. See [Attachment](./attachments)
  ## | uploads               | array   | no        | List of tokens received from [uploading files](https://developer.zendesk.com/rest_api/docs/support/attachments#upload-files) for comment attachments. The files are attached by creating or updating tickets with the tokens. See [Attaching files](https://developer.zendesk.com/rest_api/docs/support/tickets#attaching-files) in Tickets
  ## | via                   | object  | yes       | How the comment was created. See [Via Object](./ticket_audits#the-via-object)
  ## | metadata              | object  | yes       | System information (web client, IP address, etc.) and comment flags, if any. See [Comment flags](#comment-flags)
  ## | created_at            | date    | yes       | The time the comment was created
  ##
  ## #### Author id
  ##
  ## If you set the `author_id`, the user with the id is shown as the author
  ## of the comment. However, this user is not considered the updater of the
  ## ticket. The authenticated user making the API request is the updater. This
  ## has implications for business rules and views, such as the requester
  ## updated attribute and current user conditions.
  ##
  ## #### Comment flags
  ##
  ## Each comment can be flagged by Zendesk for several reasons. If the comment
  ## is flagged, the `metadata` property will have a `flags` array with any of
  ## the following values:
  ##
  ## | Value | Reason for flag
  ## | ------| ---------------
  ## | 0     | Zendesk is unsure the comment should be trusted
  ## | 2     | The comment author was not part of the conversation. [Learn more](https://support.zendesk.com/hc/en-us/articles/203661606#topic_d32_mzc_3r)
  ## | 3     | The comment author was not signed in when the comment was submitted. [Learn more](https://support.zendesk.com/hc/en-us/articles/203663756#topic_nr4_4s5_cq)
  ## | 4     | The comment was automatically generated. Automatic email notifications have been suppressed
  ## | 5     | The attached file was rejected because it's too big
  ## | 11    | This comment was submitted by the user on behalf of the author. See [Requesters and submitters](./tickets#requesters-and-submitters)
  ##
  ## A `flags_options` object will also be included with additional
  ## information about the flags.
  ##
  ## The `flags` and `flags_options` properties are omitted if there are no flags.
  ##
  ## Example:
  ##
  ## ```javascript
  ## metadata: {
  ##   system: { ... },
  ##   flags: [2,5],
  ##  "flags_options": {
  ##    "2": {
  ##      "trusted": false
  ##    },
  ##    "5": {
  ##      "message": {
  ##        "file": "printer_manual.pdf",
  ##        "account_limit": "20"
  ##      },
  ##      "trusted": false
  ##    }
  ##  },
  ##  "trusted": false,
  ##  "suspension_type_id": null
  ## }
  ## ```
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":        1274,
  ##   "type":      "Comment"
  ##   "body":      "Thanks for your help!",
  ##   "public":    true,
  ##   "created_at": "2009-07-20T22:55:29Z",
  ##   "author_id": 123123,
  ##   "attachments": [
  ##     {
  ##       "id":           498483,
  ##       "file_name":    "crash.log",
  ##       "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##       "content_type": "text/plain",
  ##       "size":         2532,
  ##       "thumbnails":   []
  ##     }
  ##   ],
  ##   "metadata": {
  ##     ...
  ##   },
  ##   "via": {
  ##     ...
  ##   },
  ## }
  ## ```

  def model_json(comment)
    return if comment.nil?

    html_body = comment.html_body(for_agent: user.is_agent?)
    plain_body = comment.plain_body(html_body)
    attachments = attachment_presenter.collection_presenter.model_json(comment.attachments)

    super.merge!(
      type:        comment.type,
      author_id:   comment.author_id,
      body:        comment.body,
      html_body:   html_body,
      plain_body:  plain_body,
      public:      comment.is_public?,
      attachments: attachments,
      audit_id:    comment.audit.try(:id)
    )
  end

  def collection_presenter
    @collection_presenter ||= Api::V2::Tickets::CommentCollectionPresenter.new(user, options.merge(item_presenter: self, includes: includes))
  end

  def association_preloads
    audit_preload = {}
    audit_preload[:notifications_with_ccs] = {} if account.has_email_ccs_preload_notifications_with_ccs?

    {
      audit:       audit_preload,
      attachments: attachment_presenter.association_preloads
    }
  end
end
