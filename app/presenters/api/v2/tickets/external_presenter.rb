class Api::V2::Tickets::ExternalPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### External event
  ##
  ## External ticket events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `External`
  ## | resource        | string  | yes       | External target id
  ## | body            | string  | yes       | Trigger message for this target event
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":       1274,
  ##   "type":     "External",
  ##   "resource": 135476,
  ##   "body":     "Target this ticket {{ticket.id}}"
  ## }
  ## ```
  def model_json(external)
    super.merge(
      type:     "External",
      resource: external.value,
      body:     external.value_reference
    )
  end
end
