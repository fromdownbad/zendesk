class Api::V2::Tickets::ChangePresenter < Api::V2::Tickets::CreatePresenter
  ## #### Change event
  ## A ticket property was updated. The event describes the previous and newly updated value of each ticket property.
  ##
  ## Change events have the following keys:
  ##
  ## | Name           | Type                  | Read-only | Comment
  ## | -------------- | --------------------- | --------- | -------
  ## | id             | integer               | yes       | Automatically assigned when the event is created
  ## | type           | string                | yes       | Has the value `Change`
  ## | field_name     | string                | yes       | The name of the field that was changed
  ## | value          | string, array, object | yes       | The value of the field that was changed
  ## | previous_value | string, array, object | yes       | The previous value of the field that was changed
  ##
  ## `value` and `previous_value` are normally strings. However, the attribute
  ## is an array when the value of `field_name` is `tags`. It's an object
  ## when the value of `field_name` is a SLA event like "first_reply_time".
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":            1274,
  ##   "type":          "Change"
  ##   "field_name":    "subject",
  ##   "value":         "My printer is on fire!",
  ##   "previous_value": "I need help!"
  ## }
  ## ```
  def model_json(create_event)
    if create_event.field_name == "linked_id"
      super.merge(
        type: "Change",
        previous_value: problem_ticket_nice_id(create_event, :value_previous).try(:to_s)
      )
    else
      super.merge(
        type: "Change",
        previous_value: mapped_value(create_event, :value_previous)
      )
    end
  end
end
