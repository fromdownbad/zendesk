class Api::V2::Tickets::PushPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Push event
  ##
  ## Information was pushed to an external target. See [Notifying external targets](https://support.zendesk.com/hc/en-us/articles/203662136)
  ## in the Zendesk Help Center.
  ##
  ## Push events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `Push`
  ## | value           | string  | yes       | Data being pushed out of our system
  ## | value_reference | string  | yes       | A reference to the destination of the data
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              1274,
  ##   "type":            "Push",
  ##   "value":           "<li><strong>Project</strong>: Internal</li><li><strong>Task</strong>: Admin...",
  ##   "value_reference": "Harvest Time Tracking"
  ## }
  ## ```
  def model_json(external)
    super.merge(
      type:            "Push",
      value:           external.value,
      value_reference: external.value_reference
    )
  end
end
