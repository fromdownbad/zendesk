class Api::V2::Tickets::MobileTicketPresenter < Api::V2::Tickets::TicketPresenter
  self.model_key = :ticket

  private

  def comment_presenter
    @comment_presenter ||= options[:comment_presenter] ||= Api::V2::Tickets::GenericCommentPresenter.new(user, options)
  end
end
