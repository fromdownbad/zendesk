class Api::V2::Tickets::TicketSharingEventPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Ticket sharing event
  ## Ticket sharing events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating events
  ## | type            | string  | yes       | Has the value `TicketSharingEvent`
  ## | agreement_id    | integer | yes       | ID of the sharing agreement
  ## | action          | string  | yes       | Either `shared` or `unshared`
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":           1274,
  ##   "type":         "TicketSharingEvent",
  ##   "agreement_id": 3454,
  ##   "action":       "shared"
  ## }
  ## ```
  def model_json(event)
    super.merge(
      type: "TicketSharingEvent",
      agreement_id: event.agreement_id,
      action: event.is_a?(TicketSharingEvent) ? "shared" : "unshared"
    )
  end
end
