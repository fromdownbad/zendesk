class Api::V2::Tickets::FollowerNotificationPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Follower event
  ##
  ## A follower was notified when the ticket was updated.
  ##
  ## Ticket Follower Notification events have the following keys:
  ##
  ## | Name            | Type                   | Read-only | Comment
  ## | --------------- | ---------------------- | --------- | -------
  ## | id              | integer                | yes       | Automatically assigned when the event is created
  ## | type            | string                 | yes       | Has the value `FollowerNotificationEvent`
  ## | subject         | string                 | yes       | The subject of the message sent to the recipients
  ## | body            | string                 | yes       | The message sent to the recipients
  ## | recipients      | array                  | yes       | An array of simple objects with the ids and names of the recipients of this notification
  ## | via             | [Via](#the-via-object) | yes       | A reference to the business rule that created this notification
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1275,
  ##   "type":       "FollowerNotification",
  ##   "subject":    "Your ticket has been updated",
  ##   "body":       "You are a Follower on this request ({{ticket.id}}). {{ticket.follower_reply_type_message}}\n\n{{ticket.comments_formatted}}"
  ##   "recipients": [847390],
  ##   "via": {
  ##     "channel": "system",
  ##     "source": {
  ##       "type":  "rule",
  ##       "id":    61,
  ##       "title": "Notify follower of comment update"
  ##     }
  ##   }
  ## }
  ## ```
  def model_json(notification)
    # NOTE: `body` is the Event's template source (not the email body sent)
    super.merge(
      type: 'FollowerNotification',
      subject: notification.account.follower_subject_template,
      body: notification.account.follower_email_template,
      recipients: Array.wrap(notification.recipients).map(&:to_i)
    )
  end
end
