class Api::V2::Tickets::ErrorsPresenter < Api::V2::ErrorsPresenter
  protected

  def present_attribute_error(record, attribute, message)
    details = super

    if message.respond_to?(:field)
      details[:ticket_field_id] = message.field.id
      details[:ticket_field_type] = message.field.type
    end

    details
  end
end
