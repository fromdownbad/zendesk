class Api::V2::Tickets::LogMeInTranscriptPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### LogMeIn transcript event
  ## LogMeIn transcript events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating events
  ## | type            | string  | yes       | Has the value `LogMeInTranscript`
  ## | body            | string  | yes       | An audit of the transcript
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":   1274,
  ##   "type": "LogMeInTranscript",
  ##   "body": "Session ID: 1234 Technician name: Johnny User Technician email: johnny@example.com ..."
  ## }
  ## ```
  def model_json(external)
    super.merge(
      type: "LogMeInTranscript",
      body: external.chat_transcript
    )
  end
end
