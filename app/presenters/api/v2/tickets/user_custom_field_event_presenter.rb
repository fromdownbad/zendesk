class Api::V2::Tickets::UserCustomFieldEventPresenter < Api::V2::Tickets::AuditEventPresenter
  include Api::V2::Tickets::AttributeMappings

  ## #### User (Model) Custom Field Events
  ## User (model) custom field events have the following keys, where value type depends on field type:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating events
  ## | field_name      | string  | yes       | Custom field name, e.g. "# of Sprockets"
  ## | owner_type      | string  | yes       | "User", "Organization"
  ## | owner_name      | string  | yes       | Name, e.g. "Fred Jones"
  ## | action_type     | string  | yes       | "Set", "Change" or "Clear"
  ## | value           | depends | yes       | New value (for "Set" & "Change"), e.g. 5
  ## | value_previous  | depends | yes       | Previous value (for "Change" & "Clear"), e.g. 8
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":             1274,
  ##   "field_name":     "# of Sprockets",
  ##   "owner_type":     "User",
  ##   "owner_name":     "Fred Jones",
  ##   "action_type":    "Set"
  ##   "value":          5
  ##   "value_previous": 8
  ## }
  ## ```
  def model_json(event)
    json = super.merge(
      field_name:  event.field_name,
      owner_type:  event.owner_type,
      owner_name:  event.owner_name,
      action_type: event.action_type # Set, Change, Clear
    )

    # leave sparse
    json[:value] = event.render(event.value) unless event.action_type == "Clear"
    json[:value_previous] = event.render(event.value_previous) unless event.action_type == "Set"

    json
  end
end
