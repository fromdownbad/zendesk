class Api::V2::Tickets::EmailCommentPresenter < Api::V2::Presenter
  self.model_key = :comment

  def model_json(full_email_body)
    {
      full_email_body: full_email_body
    }
  end
end
