class Api::V2::Tickets::ChatEventPresenter < Api::V2::Tickets::AuditEventPresenter
  self.model_key = :chat_event

  ## #### Chat event
  ## A chat event was added to a ticket via API
  ##
  ## Chat events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `Chat<Something>Event`
  ## | value           | string  | yes       | Properties of the chat event
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id": 19737,
  ##   "type": "ChatStartedEvent",
  ##   "value": {
  ##     "visitor_id": "67-v1ieJ60Iq4TM5b",
  ##     "chat_id": "1910.67.RgLlIHUVO2tLP",
  ##     "chat_start_url": "https://www.chat.wwu.zdch.at/landing/simulatev2?id=mWLzu48Roa6I6LvHFofKC5WzwOmKqJMt&ww=false&lang=en#__zopim_widget_proxy=mediator02.chat.wwu.zdch.at"
  ##     "history": [...],
  ##     "webpath": [...],
  ##   },
  ##   "attachments": [...]
  ## }
  ## ```

  def model_json(chat_event)
    return unless chat_event
    attachments = attachment_presenter.collection_presenter.model_json(chat_event.attachments)
    super.merge!(
      type: chat_event.type,
      value: chat_event.value.tap do |value|
        next unless chat_event.type == 'ChatStartedEvent'
        value[:history] = chat_event.chat_transcripts.flat_map(&:chat_history)
        value[:webpath] = chat_event.chat_transcripts.flat_map(&:web_paths)
      end,
      attachments: attachments
    )
  end

  def association_preloads
    {
      chat_transcripts: {},
      attachments: attachment_presenter.association_preloads
    }
  end
end
