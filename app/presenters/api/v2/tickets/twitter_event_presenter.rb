class Api::V2::Tickets::TwitterEventPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Tweet event
  ## A comment was added to the ticket from Twitter.
  ##
  ## Tweet events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when the event is created
  ## | type            | string  | yes       | Has the value `Tweet`
  ## | direct_message  | boolean | yes       | Whether this tweet was a direct message
  ## | body            | string  | yes       | The body of the tweet
  ## | recipients      | array   | yes       | The recipients of this tweet
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":             1274,
  ##   "type":           "Tweet",
  ##   "direct_message": false,
  ##   "body":           "Hi there",
  ##   "recipients":     [847390, 93905]
  ## }
  ## ```
  def model_json(twitter_event)
    super.merge(
      type:           "Tweet",
      direct_message: twitter_event.is_a?(TwitterDmEvent),
      body:           twitter_event.body,
      recipients:     Array.wrap(twitter_event.recipients).map(&:to_i)
    )
  end
end
