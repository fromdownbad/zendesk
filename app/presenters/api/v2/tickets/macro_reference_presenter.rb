class Api::V2::Tickets::MacroReferencePresenter < Api::V2::Tickets::AuditEventPresenter
  self.model_key = :macro_reference

  ## #### Macro reference event
  ## Macro reference events have the following keys:
  ##
  ## | Name            | Type    | Read-only | Comment
  ## | --------------- | ------- | --------- | -------
  ## | id              | integer | yes       | Automatically assigned when creating events
  ## | type            | string  | yes       | Has the value "MacroReference"
  ## | macro_id        | integer | yes       | The id of the macro this event refers to
  ## | macro_title     | string  | yes       | The title of the macro this event refers to
  ## | macro_deleted   | boolean | yes       | Whether or not the macro this event refers to is deleted
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":            1274,
  ##   "type":          "MacroReference",
  ##   "macro_id":      45345,
  ##   "macro_title":   "Downgrade ticket",
  ##   "macro_deleted": false
  ## }
  ## ```
  def model_json(macro_reference)
    macro = retrieve_macro(macro_reference)

    super.merge!(
      type:          macro_reference.class.to_s,
      macro_title:   macro.title,
      macro_id:      macro_reference.macro_id,
      macro_deleted: macro.deleted?
    )
  end

  private

  def retrieve_macro(reference)
    preloaded_macro = options.dig(
      :event_via_associations,
      :macros,
      reference.macro_id.to_i
    )

    reference.macro(preloaded_macro)
  end
end
