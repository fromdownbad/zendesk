class Api::V2::Tickets::ReplyOptionsPresenter < Api::V2::Presenter
  def model_json(_ticket)
    # NOTE: This is just a "dummy" implementation for proof of concept.  It returns hard-coded values.  This
    # is meant to allow the API to be evaluated and tweaked, it is not meant for production use.
    [
      {
        text: "Reply on Wall",
        description: "reply publicly on Sanket",
        icon: "https://p6.zdassets.com/agent/assets/icons/reply_via_facebook.png",
        via_id: 38,
        public: true,
        available: true,
        supports_rich_text: false,
        option_id: {
          reply_via: [
            {
              reply_via: 'Mail',
              requester: 123
            },
            {
              monitored_twitter_handle_id: 234,
              requester: 345
            },
            {
              comment: {
                public: true
              }
            }
          ]
        }
      },
      {
        text: "Email only",
        description: "your comment is sent to the ticket requester",
        via_id: 38,
        public: true,
        available: true,
        supports_rich_text: true,
        option_id: {
          reply_via: [
            {
              reply_via: 'Mail',
              requester: 123
            },
            {
              comment: {
                public: true
              }
            }
          ]
        }
      },
      {
        text: "Internal note",
        description: "your comment is visible to agents only",
        via_id: 38,
        public: false,
        available: true,
        supports_rich_text: true,
        option_id: {
          reply_via: [
            {
              reply_via: 'Mail',
              requester: 123
            },
            {
              comment: {
                public: false
              }
            }
          ]
        }
      }
    ]
  end
end
