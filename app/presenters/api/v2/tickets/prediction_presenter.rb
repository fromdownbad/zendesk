class Api::V2::Tickets::PredictionPresenter < Api::V2::Presenter
  self.model_key = :ticket_prediction

  ## #### Ticket prediction information presenter
  ## Ticket prediction information has the following keys:
  ##
  ## | Name                     | Type    | Read-only | Comment
  ## | ------------------------ | ------- | --------- | -------
  ## | satisfaction_probability | float   | yes       | The current satisfaction probability for the related ticket
  ## | ticket_id                | integer | yes       | The nice_id of the related ticket
  ## | account_id               | integer | yes       | The id of the related account
  ## | created_at               | date    | yes       | When this record was created
  ## | updated_at               | date    | yes       | When this record was updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "ticket_prediction": {
  ##     "satisfaction_probability": 0.555555,
  ##     "ticket_id": 4,
  ##     "account_id": 1,
  ##     "created_at": "2015-11-26T00:59:20Z",
  ##     "updated_at": "2015-11-30T06:54:23Z"
  ##   }
  ## }
  ## ```
  def model_json(ticket_prediction)
    {
      satisfaction_probability: ticket_prediction.satisfaction_probability,
      ticket_id:                ticket_prediction.ticket.nice_id,
      account_id:               ticket_prediction.account_id,
      created_at:               ticket_prediction.created_at,
      updated_at:               ticket_prediction.updated_at
    }
  end
end
