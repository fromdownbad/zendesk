class Api::V2::Tickets::OrganizationActivityPresenter < Api::V2::Tickets::AuditEventPresenter
  ## #### Organization subscription notification event
  ## A notification was sent to the organization subscribers when somebody in the organization submitted a ticket.
  ##
  ## This feature was available in the Classic version of the Zendesk Support user interface. In the current version of Zendesk Support,
  ## you can use the [Organization Subscriptions API](organization_subscriptions.html) to create the subscriptions.
  ##
  ## Organization subscription notification events have the following keys:
  ##
  ## | Name            | Type                   | Read-only | Comment
  ## | --------------- | ---------------------- | --------- | -------
  ## | id              | integer                | yes       | Automatically assigned when the event is created
  ## | type            | string                 | yes       | Has the value `OrganizationActivity`
  ## | subject         | string                 | yes       | The subject of the message sent to the recipients
  ## | body            | string                 | yes       | The message sent to the recipients
  ## | recipients      | array                  | yes       | An array of simple objects with the ids and names of the recipients of the notification
  ## | via             | [Via](#the-via-object) | yes       | A reference to the trigger that created the notification
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         1275,
  ##   "type":       "OrganizationActivity"
  ##   "subject":    "Your ticket has been updated"
  ##   "body":       "Ticket #235 has been updated"
  ##   "recipients": [847390, 93905],
  ##   "via": {
  ##     "channel": "system",
  ##     "source": {
  ##       "type":  "rule",
  ##       "id":    61,
  ##       "title": "Notify requester of comment update"
  ##     }
  ##   }
  ## }
  ## ```
  def model_json(notification)
    super.merge(
      type:       "OrganizationActivity",
      subject:    notification.subject,
      body:       notification.body,
      recipients: Array.wrap(notification.recipients).map(&:to_i)
    )
  end
end
