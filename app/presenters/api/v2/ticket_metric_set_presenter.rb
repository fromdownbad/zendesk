class Api::V2::TicketMetricSetPresenter < Api::V2::Presenter
  self.model_key = :ticket_metric

  ## ### JSON Format
  ##
  ## | Name                               | Type     | Comment
  ## | ---------------------------------- | -------- | -------
  ## | id                                 | integer  | Automatically assigned
  ## | ticket_id                          | integer  | Id of the associated ticket
  ## | url                                | string   | The API url of the ticket metric
  ## | group_stations                     | integer  | Number of groups the ticket passed through
  ## | assignee_stations                  | integer  | Number of assignees the ticket had
  ## | reopens                            | integer  | Total number of times the ticket was reopened
  ## | replies                            | integer  | The number of public replies added to a ticket by an agent
  ## | assignee_updated_at                | date     | When the assignee last updated the ticket
  ## | requester_updated_at               | date     | When the requester last updated the ticket
  ## | status_updated_at                  | date     | When the status was last updated
  ## | initially_assigned_at              | date     | When the ticket was initially assigned
  ## | assigned_at                        | date     | When the ticket was last assigned
  ## | solved_at                          | date     | When the ticket was solved
  ## | latest_comment_added_at            | date     | When the latest comment was added
  ## | first_resolution_time_in_minutes   | object   | Number of minutes to the first resolution time during calendar and business hours
  ## | reply_time_in_minutes              | object   | Number of minutes to the first reply during calendar and business hours
  ## | full_resolution_time_in_minutes    | object   | Number of minutes to the full resolution during calendar and business hours
  ## | agent_wait_time_in_minutes         | object   | Number of minutes the agent spent waiting during calendar and business hours
  ## | requester_wait_time_in_minutes     | object   | Number of minutes the requester spent waiting during calendar and business hours
  ## | created_at                         | date     | When the record was created
  ## | updated_at                         | date     | When the record was last updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id": 33,
  ##   "ticket_id": 4343,
  ##   "created_at": "2009-07-20T22:55:29Z",
  ##   "updated_at": "2011-05-05T10:38:52Z",
  ##   "group_stations": 7,
  ##   "assignee_stations": 1,
  ##   "reopens": 55,
  ##   "replies": 322,
  ##   "assignee_updated_at": "2011-05-06T10:38:52Z",
  ##   "requester_updated_at": "2011-05-07T10:38:52Z",
  ##   "status_updated_at": "2011-05-04T10:38:52Z",
  ##   "initially_assigned_at": "2011-05-03T10:38:52Z",
  ##   "assigned_at": "2011-05-05T10:38:52Z",
  ##   "solved_at": "2011-05-09T10:38:52Z",
  ##   "latest_comment_added_at": "2011-05-09T10:38:52Z",
  ##   "reply_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##   "first_resolution_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##   "full_resolution_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##   "agent_wait_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##   "requester_wait_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##   "on_hold_time_in_minutes": { "calendar": 2290, "business": 637 }
  ## }
  ## ```

  def model_json(ticket_metric)
    Ticket.with_deleted { ticket_metric.ticket }
    super.merge!(
      id: ticket_metric.id,
      ticket_id: ticket_metric.ticket.nice_id,
      created_at: ticket_metric.created_at,
      updated_at: ticket_metric.updated_at,
      group_stations: ticket_metric.group_stations,
      assignee_stations: ticket_metric.assignee_stations,
      reopens: ticket_metric.reopens,
      replies: ticket_metric.replies,
      assignee_updated_at: ticket_metric.ticket.assignee_updated_at,
      requester_updated_at: ticket_metric.ticket.requester_updated_at,
      status_updated_at: ticket_metric.ticket.status_updated_at,
      initially_assigned_at: ticket_metric.ticket.initially_assigned_at,
      assigned_at: ticket_metric.ticket.assigned_at,
      solved_at: ticket_metric.ticket.solved_at,
      latest_comment_added_at: ticket_metric.ticket.latest_comment_added_at,
      reply_time_in_minutes: {
        calendar: ticket_metric.first_reply_time_in_minutes,
        business: ticket_metric.first_reply_time_in_minutes_within_business_hours
      },
      first_resolution_time_in_minutes: {
        calendar: ticket_metric.first_resolution_time_in_minutes,
        business: ticket_metric.first_resolution_time_in_minutes_within_business_hours
      },
      full_resolution_time_in_minutes: {
        calendar: ticket_metric.full_resolution_time_in_minutes,
        business: ticket_metric.full_resolution_time_in_minutes_within_business_hours
      },
      agent_wait_time_in_minutes: {
        calendar: ticket_metric.agent_wait_time_in_minutes,
        business: ticket_metric.agent_wait_time_in_minutes_within_business_hours
      },
      requester_wait_time_in_minutes: {
        calendar: ticket_metric.requester_wait_time_in_minutes,
        business: ticket_metric.requester_wait_time_in_minutes_within_business_hours
      },
      on_hold_time_in_minutes: {
        calendar: ticket_metric.on_hold_time_in_minutes,
        business: ticket_metric.on_hold_time_in_minutes_within_business_hours
      }
    )
  end
end
