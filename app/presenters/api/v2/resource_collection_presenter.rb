class Api::V2::ResourceCollectionPresenter < Api::V2::Presenter
  self.model_key = :resource_collection

  ## ### JSON Format
  ## Resource Collections have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | created_at      | date    | yes       | no        | The time the identity got created
  ## | updated_at      | date    | yes       | no        | The time the identity got updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "updated_at":      "2011-07-20T22:55:29Z",
  ##   "created_at":      "2011-07-20T22:55:29Z"
  ## }
  ## ```
  def model_json(resource_collection)
    {
      id: resource_collection.id,
      created_at: resource_collection.created_at,
      updated_at: resource_collection.updated_at,
      resources: resource_collection.collection_resources.map do |resource|
        {
          identifier: resource.identifier,
          resource_id: resource.resource_id,
          type: resource.type,
          deleted: resource.resource.nil?
        }
      end
    }
  end
end
