require 'api/presentation'

class Api::V2::CollectionPresenter < Api::Presentation::CollectionPresenter
  def url(model)
    url_builder.send("api_v2_#{model_key}_url", model, format: :json)
  end
end
