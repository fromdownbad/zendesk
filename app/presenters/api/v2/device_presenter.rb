class Api::V2::DevicePresenter < Api::V2::Presenter
  self.model_key = :device

  ## ### JSON Format
  ## Devices are represented as JSON objects which have the following keys:
  ##
  ## | Name             | Type    | Read-only | Mandatory | Comment
  ## | ---------------- | ------- | --------- | --------- | -------
  ## | id               | integer | yes       | yes       | Automatically assigned upon creation
  ## | name             | string  | no        | no        | Name of the device
  ## | ip               | string  | yes       | no        | Last seen ip for the device
  ## | user_agent       | string  | yes       | no        | Last seen user agent for the device
  ## | location         | string  | yes       | no        | Geolocation of last seen ip address
  ## | current          | boolean | yes       | no        | `true` if this is the current device
  ## | created_at       | date    | yes       | no        | When this record was created
  ## | updated_at       | date    | yes       | no        | When this record was last updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":           16439,
  ##   "user_id":      293864,
  ##   "name":         "Work Phone",
  ##   "ip":           "209.119.38.228",
  ##   "user_agent":   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) ... Safari/537.31",
  ##   "location":     "San Francisco, USA",
  ##   "current":      true,
  ##   "created_at":   "2013-02-20T22:55:29Z",
  ##   "updated_at":   "2013-05-05T10:38:52Z"
  ## }
  ## ```

  def initialize(user, options)
    super
    @device = options[:device]
  end

  def model_json(device)
    super.merge!(
      id: device.id,
      name: device.name,
      ip: device.ip,
      user_agent: device.user_agent,
      location: device.location,
      current: device == @device,
      created_at: device.created_at,
      updated_at: device.updated_at,
      last_active_at: device.last_active_at,
      mobile: device.mobile?
    )
  end

  def url(device)
    url_builder.send(:api_v2_user_devices_url, device.user_id, format: :json)
  end
end
