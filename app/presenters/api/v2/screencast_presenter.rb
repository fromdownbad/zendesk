class Api::V2::ScreencastPresenter < Api::V2::Presenter
  self.model_key = :screencast

  ## ### JSON Format
  ## Zendesk uses Screenr.com for recording screencasts.
  ## Screencasts are represented as JSON objects with the following keys:
  ##
  ## | Name          | Type             | Read-only | Comment
  ## | ------------- | ---------------- | --------- | -------
  ## | id            | string           | yes       | Screenr internal id
  ## | position      | string           | yes       | The cardinality of the object in a collection
  ## | content_url   | string           | yes       | A full URL where the screencast is stored by Screenr
  ## | thumbnail_url | string           | yes       | A full URL where a thumbnails for the screencast is stored by Screenr
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":            "f3d6edsd2123208c669fbaaa",
  ##   "position":      "3",
  ##   "content_url":   "http://video.example.com/embed/f3d6edsd2123208c669fbaaa",
  ##   "thumbnail_url": "http://video.example.com/embed/f3d6edsd2123208c669fbaaa.jpg"
  ## }
  ## ```
  def model_json(screencast)
    {
      id: screencast.id,
      position: screencast.position,
      content_url: screencast.url,
      thumbnail_url: screencast.thumbnail
    }
  end
end
