class Api::V2::SalesforcePresenter < Api::V2::Presenter
  ## ### JSON Format
  ## Ticket information necessary to create/update Salesforce case:
  ##
  ## | Name                  | Type    | Comment
  ## | ---------------       | ------- | ---------
  ## | ticket                | hash    | Contains the ticket information
  ## | users                 | array   | Includes requester, submitter and assignee information
  ## | comments              | array   | The 100 most recent comments in DESC order
  ## | metrics               | hash    | Ticket metrics
  ## | groups                | array   | Ticket group information
  ## | organizations         | array   | Ticket organization information
  ##
  ## #### Example
  ## ```js
  ## "ticket":
  ##   {
  ##     "id":               35436,
  ##     "url":              "https://company.zendesk.com/api/v2/tickets/35436.json",
  ##     "external_id":      "ahg35h3jh",
  ##     "created_at":       "2009-07-20T22:55:29Z",
  ##     "updated_at":       "2011-05-05T10:38:52Z",
  ##     "type":             "incident",
  ##     "subject":          "Help, my printer is on fire!",
  ##     "description":      "The fire is very colorful.",
  ##     ...
  ##     "tags":             ["enterprise", "other_tag"],
  ##     "via": {
  ##       "channel": "web"
  ##     },
  ##     "fields": [
  ##       {
  ##         "id":    27642,
  ##         "title": "Field title",
  ##         "value": "745"
  ##       },
  ##       {
  ##         "id":    27648,
  ##         "title": "Another field title",
  ##         "value": "yes"
  ##       }
  ##     ],
  ##   },
  ## "users": [
  ##   {
  ##     "id":                    35436,
  ##     "url":                   "https://company.zendesk.com/api/v2/users/35436.json",
  ##     "name":                  "Johnny Requester",
  ##     "external_id":           "sai989sur98w9",
  ##     "alias":                 "Mr. Requester",
  ##     ...
  ##   },
  ##   {
  ##     "id":                    35437,
  ##     "url":                   "https://company.zendesk.com/api/v2/users/35437.json",
  ##     "name":                  "Johnny Submitter",
  ##     "external_id":           "sai989sur98w9",
  ##     "alias":                 "Mr. Submitter",
  ##     ...
  ##   },
  ##   {
  ##     "id":                    35438,
  ##     "url":                   "https://company.zendesk.com/api/v2/users/35438.json",
  ##     "name":                  "Johnny Assignee",
  ##     "external_id":           "sai989sur98w9",
  ##     "alias":                 "Mr. Assignee",
  ##     ...
  ##   }
  ## ],
  ## "comments": [
  ##   {
  ##     "id":          1564245,
  ##     "type":        "Comment"
  ##     "body":        "Thanks for your help!",
  ##     "html_body":   "<p>Thanks for your help!</p>",
  ##     "public":      true,
  ##     "trusted":     true,
  ##     "attachments": [],
  ##     "author_id":   35436,
  ##     "name":        "Mr. Requester",
  ##     "email":       "johnny@requester.com",
  ##     "ticket_id":   35436,
  ##     "created_at":  "2009-07-20T22:55:29Z"
  ##   }
  ## ],
  ## "metrics":
  ##   {
  ##     "id": 33,
  ##     "ticket_id": 4343,
  ##     "created_at": "2009-07-20T22:55:29Z",
  ##     "updated_at": "2011-05-05T10:38:52Z",
  ##     "group_stations": 7,
  ##     "assignee_stations": 1,
  ##     "reopens": 55,
  ##     "replies": 322,
  ##     "assignee_updated_at": "2011-05-06T10:38:52Z",
  ##     "requester_updated_at": "2011-05-07T10:38:52Z",
  ##     "status_updated_at": "2011-05-04T10:38:52Z",
  ##     "initially_assigned_at": "2011-05-03T10:38:52Z",
  ##     "assigned_at": "2011-05-05T10:38:52Z",
  ##     "solved_at": "2011-05-09T10:38:52Z",
  ##     "latest_comment_added_at": "2011-05-09T10:38:52Z",
  ##     "reply_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##     "first_resolution_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##     "full_resolution_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##     "agent_wait_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##     "requester_wait_time_in_minutes": { "calendar": 2391, "business": 737 },
  ##     "on_hold_time_in_minutes": { "calendar": 2290, "business": 637 }
  ##   },
  ## "groups": [
  ##   {
  ##     "id":         3432,
  ##     "url":        "https://company.zendesk.com/api/v2/groups/3432.json",
  ##     "deleted",    false,
  ##     "name":       "First Level Support",
  ##     "created_at": "2009-07-20T22:55:29Z",
  ##     "updated_at": "2011-05-05T10:38:52Z"
  ##   }
  ## ],
  ## "organizations": [
  ##   {
  ##     "id":              35436,
  ##     "external_id":     "ABC123",
  ##     "url":             "https://company.zendesk.com/api/v2/organizations/35436.json",
  ##     "name":            "One Organization",
  ##     "created_at":      "2009-07-20T22:55:29Z",
  ##     "updated_at":      "2011-05-05T10:38:52Z",
  ##     "domain_names":    ["example.com", "test.com"],
  ##     "details":         "This is a kind of organization",
  ##     "notes":           "",
  ##     "group_id":        null,
  ##     "shared_tickets":  true,
  ##     "shared_comments": true,
  ##     "tags":            ["enterprise", "other_tag"]
  ##   }
  ## ]
  ## ```
  def as_json(ticket)
    data = ticket_presenter.present(ticket)

    # Comments
    data[:comments] = comment_presenter.collection_presenter.model_json(ticket.recent_comments)

    data
  end

  private

  def ticket_presenter
    @ticket_presenter ||= Api::V2::Tickets::TicketPresenter.new(user,
      options.merge(
        includes: [:metrics, :users, :groups, :organizations, :brands],
        use_salesforce_custom_fields: true
      ))
  end

  def comment_presenter
    @comment_presenter ||= CommentPresenter.new(user, options)
  end

  class CommentPresenter < Api::V2::Tickets::CommentPresenter
    # Adds some extra attributes to the standard comment attributes
    def model_json(comment)
      super.merge(
        author_id: comment.author.id,
        name: comment.author.name,
        email: comment.author.email,
        ticket_id: comment.ticket.nice_id,
        created_at: comment.created_at
      )
    end

    def collection_presenter
      @collection_presenter ||= Api::V2::CollectionPresenter.new(user, options.merge(item_presenter: self, includes: includes))
    end
  end
end
