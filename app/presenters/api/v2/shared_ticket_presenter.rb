class Api::V2::SharedTicketPresenter < Api::V2::Presenter
  self.model_key = :shared_ticket

  def initialize(user, options = {})
    @agreement = options[:agreement]
    super
  end

  ## ### JSON Format
  ## Sharing Agreements have the following format:
  ##
  ## | Name              | Type    | Comment
  ## | ----------------- | ------- | -------
  ## | local_ticket_id   | integer | Id of the ticket on the current account or null if deleted
  ## | remote_ticket_id  | integer | Id of the ticket on the remote account if agreement is inbound
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "local_ticket_id":  2342,
  ##   "remote_ticket_id": 743,
  ## }
  ## ```
  def model_json(shared_ticket)
    {
      local_ticket_id: shared_ticket.ticket.try(:nice_id),
      remote_ticket_id: @agreement.in? ? shared_ticket.original_id : nil,
    }
  end
end
