class Api::V2::Users::ComplianceDeletionStatusPresenter < Api::V2::Presenter
  self.model_key = :compliance_deletion_status

  # h3 JSON Format
  #
  # h3 Compliance Deletion Status
  # Compliance Deletion Statuses have the following keys:
  #
  # | Name                     | Type    | Comment
  # | ------------------------ | ------- | -------
  # |  user_id                 | Int     |
  # |  executer_id             | Int     |
  # |  account_subdomain       | String  |
  # |  action                  | String  |
  # |  application             | Boolean |
  # |  created_at              | Date    |
  #
  #
  # h4 Example
  #
  # ```js
  # {
  #   "compliance_deletion_status":
  #     {
  #       "user_id":1,
  #       "executer_id":1,
  #       "account_subdomain": 'accountABC',
  #       "action":'complete',
  #       "application":'voice',
  #       "created_at":"2018-01-05T10:38:52Z"
  #     }
  # }
  # ```
  def model_json(model)
    if model.class == ComplianceDeletionStatus
      {
        user_id: model.user_id,
        executer_id: model.executer_id,
        account_subdomain: model.account.subdomain,
        action: ComplianceDeletionStatus::REQUEST_DELETION,
        application: ComplianceDeletionStatus::ALL_APPLICATONS,
        created_at: model.created_at
      }
    else
      # ComplianceDeletionFeedback model
      {
        user_id: model.user_id,
        executer_id: nil,
        account_subdomain: model.account.subdomain,
        action: model.state,
        application: model.application,
        created_at: model.created_at
      }
    end
  end

  private

  def application(model)
    if model.application.to_s == ComplianceDeletionStatus::CLASSIC
      'support'
    else
      model.application
    end
  end
end
