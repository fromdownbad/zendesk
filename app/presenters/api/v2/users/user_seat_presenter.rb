class Api::V2::Users::UserSeatPresenter < Api::V2::Presenter
  self.model_key = :user_seat

  ## ### JSON Format
  ##
  ## ### Voice Seat
  ##
  ## | Name                     | Type    | Comment
  ## | ------------------------ | ------- | -------
  ## |  agent_id                | Int     |
  ## |  is_enabled              | Boolean |
  ## |  seat_type               | String  |

  ##
  ##
  ## #### Example
  ##
  ## ```js
  ## {
  ##   "user_seat":
  ##     {
  ##       "seat_type":'voice',
  ##       "role":1
  ##     }
  ## }
  ## ```

  def model_json(model)
    unless model.blank?
      {
        user_id: model.user_id,
        seat_type: model.seat_type,
        role: model.role
      }
    end
  end
end
