class Api::V2::Users::ZopimIdentityPresenter < Api::V2::Presenter
  self.model_key = :zopim_agent

  ## ### JSON Format
  ##
  ## ### Zopim Identity
  ##
  ## | Name                           | Type    | Comment
  ## | ------------------------------ | ------- | -------
  ## |  zopim_agent_id                | Int     |
  ## |  email                         | String  | eg. name+user_id@domain.com
  ## |  is_owner                      | Boolean |
  ## |  is_enabled                    | Boolean |
  ## |  is_administrator              | Boolean |
  ## |  display_name                  | String  |
  ## |  syncstate                     | String  | ready
  ## |  is_linked                     | Boolean |
  ## |  is_serviceable                | Boolean |
  ## |  agent_chat_limit              | Int     | Number of chat windows this agent may have open at once
  ##
  ##
  ## #### Example
  ##
  ## ```js
  ## {
  ##   "zopim_agent":
  ##     {
  ##       "zopim_agent_id":2131977,
  ##       "email":"chessagent7+10022@zendesk.com",
  ##       "is_owner":true,
  ##       "is_enabled":true,
  ##       "is_administrator":true,
  ##       "display_name":"Imma Trialler",
  ##       "syncstate":"ready",
  ##       "is_linked":true,
  ##       "is_serviceable":true,
  ##       "agent_chat_limit":3
  ##     }
  ## }
  ## ```

  def model_json(model)
    {
      zopim_agent_id:   model.zopim_agent_id,
      email:            model.email,
      is_owner:         model.is_owner,
      is_enabled:       model.is_enabled,
      is_administrator: model.is_administrator,
      display_name:     model.display_name,
      syncstate:        model.syncstate,
      is_linked:        model.is_linked?,
      is_serviceable:   model.is_serviceable?,
      agent_chat_limit: model.agent_chat_limit
    } unless model.blank?
  end

  def side_loads(model)
    {}.tap do |json|
      if side_load?(:zopim_subscription)
        json[:zopim_subscription] = zopim_subscription_presenter.model_json(model.zopim_subscription)
      end

      if side_load?(:zopim_integration)
        json[:zopim_integration] = zopim_integration_presenter.model_json(model.account.zopim_integration)
      end
    end
  end

  def zopim_subscription_presenter
    @zopim_subscription_presenter ||= Api::V2::Account::ZopimSubscriptionPresenter.new(user, options)
  end

  def zopim_integration_presenter
    @zopim_integration_presenter ||= Api::V2::ZopimIntegrationPresenter.new(user, options)
  end
end
