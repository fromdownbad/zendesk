class Api::V2::Users::DeletedUserPresenter < Api::V2::Presenter
  self.model_key = :deleted_user

  ## ### JSON Format for Deleted users
  ## The user is returned with the following attributes:
  ##
  ## | Name                  | Type                           | Read-only | Mandatory | Comment
  ## | --------------------- | ------------------------------ | --------- | --------- | -------
  ## | id                    | integer                        | yes       | no        | Automatically assigned when creating users
  ## | email                 | string                         | no        | no        | The primary email address of this user
  ## | name                  | string                         | no        | yes       | The name of the user
  ## | created_at            | date                           | yes       | no        | The time the user was created
  ## | locale                | string                         | yes       | no        | The locale for this user
  ## | locale_id             | integer                        | no        | no        | The language identifier for this user
  ## | organization_id       | integer                        | no        | no        | The id of the organization this user is associated with
  ## | phone                 | string                         | no        | no        | The primary phone number of this user. See [Phone Number](./users#phone-number) in the Users API
  ## | photo                 | [Attachment](attachments.html) | no        | no        | The user's profile picture represented as an [Attachment](attachments.html) object
  ## | role                  | string                         | no        | no        | The role of the user. Possible values: `"end-user"`, `"agent"`, `"admin"`
  ## | time_zone             | string                         | no        | no        | The time-zone of this user
  ## | updated_at            | date                           | yes       | no        | The time of the last update of the user
  ## | url                   | string                         | yes       | no        | The API url of this user
  ## | verified              | boolean                        | no        | no        | Zendesk Support has verified that this user is who he says he is
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                    35436,
  ##   "url":                   "https://company.zendesk.com/api/v2/end_users/35436.json",
  ##   "name":                  "Johnny End User",
  ##   "created_at":            "2009-07-20T22:55:29Z",
  ##   "updated_at":            "2011-05-05T10:38:52Z",
  ##   "time_zone":             "Copenhagen",
  ##   "phone":                 "+1 555-123-4567",
  ##   "locale":                "en-US",
  ##   "locale_id":             1,
  ##   "organization_id":       57542,
  ##   "role":                  "end-user",
  ##   "photo": {
  ##     "id":           928374,
  ##     "name":         "my_funny_profile_pic.png",
  ##     "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic.png",
  ##     "content_type": "image/png",
  ##     "size":         166144,
  ##     "thumbnails": [
  ##       {
  ##         "id":           928375,
  ##         "name":         "my_funny_profile_pic_thumb.png",
  ##         "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic_thumb.png",
  ##         "content_type": "image/png",
  ##         "size":         58298,
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  def model_json(deleted_user, _options = {})
    raise "Active users should not be presented as deleted" if deleted_user.is_active?
    json = {
      id:                  deleted_user.id,
      url:                 url_builder.send(:api_v2_deleted_user_url, deleted_user),
      name:                deleted_user.name,
      email:               deleted_user.email,
      created_at:          deleted_user.created_at,
      updated_at:          deleted_user.updated_at,
      time_zone:           deleted_user.time_zone,
      phone:               deleted_user.phone_number.presence,
      shared_phone_number: deleted_user.shared_phone_number,
      photo:               photo(deleted_user),
      locale_id:           deleted_user.translation_locale.id,
      locale:              deleted_user.translation_locale.locale,
      organization_id:     deleted_user.organization.try(:id),
      role:                deleted_user.role.to_s.downcase.presence,
      active:              deleted_user.is_active?
    }

    if account.has_private_attributes_in_end_user_presenter?
      json[:external_id] = deleted_user.external_id
      unless account.has_api_tags_disabled_for_end_user_presenter?
        json[:tags] = account.has_user_and_organization_tags? ? deleted_user.tags : []
      end
    end

    if side_load?(:latest_tickets)
      json[:latest_tickets] = ticket_presenter.collection_presenter.model_json(latest_tickets(deleted_user))
    end

    if side_load?(:related_ticket_ids)
      json[:related_ticket_ids] = related_ticket_ids(deleted_user)
    end

    json
  end

  def association_preloads
    {
      identities:               {},
      photo:                    {
        thumbnails: {}
      },
      account:                  {},
      # Organization memberships needs to be preloaded before organization, otherwise preloading the organization
      # causes the organization_memberships to be loaded for the user, causing an N+1
      organization_memberships: {},
      organization:             {}
    }
  end

  private

  def related_ticket_ids(deleted_user)
    ActiveRecord::Base.on_slave do
      Ticket.with_deleted do
        active_ticket_ids = Ticket.where("requester_id = ? OR submitter_id = ? OR assignee_id = ?", deleted_user.id, deleted_user.id, deleted_user.id).
          where(account_id: deleted_user.account_id).pluck(:nice_id)
        archived_ticket_ids = TicketArchiveStub.where("assignee_id = ? OR requester_id = ?", deleted_user.id, deleted_user.id).pluck(:nice_id)
        active_ticket_ids | archived_ticket_ids
      end
    end
  end

  def latest_tickets(deleted_user)
    @latest_tickets ||= begin
      ticket_ids = Comment.where(author_id: deleted_user.id, account_id: deleted_user.account_id).select('distinct(ticket_id)').order('created_at DESC').limit(6).pluck(:ticket_id)
      deleted_user.account.tickets.where(id: ticket_ids)
    end
  end

  def photo(user)
    return nil unless user.photo.present?

    attachment_presenter.model_json(user.photo)
  end
end
