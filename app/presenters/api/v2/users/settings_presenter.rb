class Api::V2::Users::SettingsPresenter < Api::V2::Presenter
  self.model_key = :settings

  ## ### JSON Format
  ##
  ## ### Lotus
  ##
  ## | Name                                   | Type    | Comment
  ## | ---------------------------------------| ------- | -------
  ## | show_reporting_video_tutorial          | boolean | Show video tutorial for Lotus reporting
  ## | show_onboarding_tooltips               | boolean | Show tooltips for first-time user
  ## | customer_list_onboarding_state         | string  | Current step in customer lists tour
  ## | show_welcome_dialog                    | boolean | Show welcome dialog for account owner
  ## | show_user_assume_tutorial              | boolean | Show the tutorial when assuming a user
  ## | show_hc_onboarding_tooltip             | boolean | Show help center onboarding tooltip for admins
  ## | show_options_move_tooltip              | boolean | Show options menu move tooltip
  ## | show_filter_options_tooltip            | boolean | Show filter options move tooltip
  ## | show_apps_tray                         | boolean | Show apps tray in ticket/user sidebar location
  ## | apps_last_collapsed_states             | string  | Current collapsed/expanded state of sidebar apps
  ## | show_feature_notifications             | boolean | Show feature notifications at boot time
  ## | device_notification                    | boolean | Send notification on device creation
  ## | keyboard_shortcuts_enabled             | boolean | Enable keyboard shortcuts in Lotus
  ## | voice_calling_number                   | string  | Voice calling number
  ## | show_color_branding_tooltip            | boolean | Show color branding tooltip
  ## | show_new_search_tooltip                | boolean | Show new search tooltip in the app header
  ## | show_onboarding_modal                  | boolean | Show onboarding modals in discovery
  ## | show_get_started_animation             | boolean | Show Get Started button animation
  ## | show_prediction_satisfaction_dashboard | boolean | Show prediction satisfaction promo message on reporting dashboard
  ## | show_google_apps_onboarding            | boolean | Show onboarding modals to G Suite admin
  ## | show_public_comment_warning            | boolean | Show warning modal when submitting public comment on private ticket
  ## | show_multiple_ticket_forms_tutorial    | boolean | Show tooltip when there are multiple ticket forms
  ## | revere_subscription                    | boolean |
  ## | show_enable_google_apps_modal          | boolean | Show modal prompting admin to enable G Suite
  ## | show_zero_state_tour_ticket            | boolean | Show Tour Ticket onboarding flow on first visit to ticket
  ## | show_new_comment_filter_tooltip        | boolean | Show two onboarding modal tooltips for the new comment filters
  ## | show_first_comment_private_tooltip     | boolean | Show tooltip for First comment private when first activated
  ## | show_answer_bot_trigger_onboarding     | boolean | Show onboarding tooltips on trigger edit page when deflection action first viewed
  ## | show_answer_bot_dashboard_onboarding   | boolean | Show onboarding tooltips on answer bot admin dashboard on activation
  ## | show_answer_bot_lotus_onboarding       | boolean | Show answer bot onboarding text displayed next to the answer bot suggestions on the ticket conversation
  ##
  ##
  ## #### Example
  ##
  ## ```js
  ## "settings": {
  ##   "lotus": {
  ##     "show_reporting_video_tutorial": true,
  ##     "show_onboarding_tooltips": true,
  ##     "show_welcome_dialog": true,
  ##     "show_hc_onboarding_tooltip": true,
  ##     "show_options_move_tooltip": false,
  ##     "show_filter_options_tooltip": false,
  ##     "show_user_nav_tooltip": false,
  ##     "show_apps_tray":   false,
  ##     "apps_last_collapsed_states": "{}",
  ##     "show_feature_notifications":   true,
  ##     "device_notification": true,
  ##     "keyboard_shortcuts_enabled": false,
  ##     "voice_calling_number": null,
  ##     "show_color_branding_tooltip": false,
  ##     "show_new_search_tooltip": false,
  ##     "show_onboarding_modal": true,
  ##     "show_get_started_animation": true,
  ##     "show_prediction_satisfaction_dashboard": true,
  ##     "show_google_apps_onboarding": true,
  ##     "show_public_comment_warning": true,
  ##     "show_multiple_ticket_forms_tutorial": true,
  ##     "revere_subscription": true,
  ##     "show_enable_google_apps_modal": true,
  ##     "show_zero_state_tour_ticket": true,
  ##     "show_new_comment_filter_tooltip": true,
  ##     "show_answer_bot_dashboard_onboarding": true,
  ##     "show_answer_bot_trigger_onboarding": true,
  ##     "show_answer_bot_lotus_onboarding": true
  ##   }
  ## }
  ## ```
  def model_json(user)
    json = {lotus: present_with(LotusPresenter, account)}

    if user.account.has_user_level_shared_view_ordering?
      json[:shared_views_order] = user.shared_views_order
    end

    json
  end

  class LotusPresenter < Api::V2::Presenter
    self.model_key = :lotus

    def model_json(_account)
      {
        show_reporting_video_tutorial:          user.settings.show_reporting_video_tutorial,
        show_onboarding_tooltips:               user.settings.show_onboarding_tooltips,
        show_welcome_dialog:                    user.settings.show_welcome_dialog,
        customer_list_onboarding_state:         user.settings.customer_list_onboarding_state,
        show_user_assume_tutorial:              user.settings.show_user_assume_tutorial,
        show_hc_onboarding_tooltip:             user.settings.show_hc_onboarding_tooltip,
        show_options_move_tooltip:              user.settings.show_options_move_tooltip,
        show_filter_options_tooltip:            user.settings.show_filter_options_tooltip,
        show_user_nav_tooltip:                  user.settings.show_user_nav_tooltip,
        show_apps_tray:                         user.settings.show_apps_tray,
        apps_last_collapsed_states:             user.settings.apps_last_collapsed_states,
        show_feature_notifications:             user.settings.show_feature_notifications,
        device_notification:                    user.device_notification?,
        keyboard_shortcuts_enabled:             user.settings.keyboard_shortcuts_enabled,
        voice_calling_number:                   user.settings.voice_calling_number,
        ticket_action_on_save:                  user.settings.ticket_action_on_save,
        show_insights_onboarding:               user.settings.show_insights_onboarding,
        show_color_branding_tooltip:            user.show_color_branding_tooltip?,
        two_factor_authentication:              user.eligible_for_2fa?,
        show_new_search_tooltip:                user.settings.show_new_search_tooltip,
        show_onboarding_modal:                  user.settings.show_onboarding_modal,
        show_get_started_animation:             user.settings.show_get_started_animation,
        show_prediction_satisfaction_dashboard: user.settings.show_prediction_satisfaction_dashboard,
        show_hc_product_tray_tooltip:           user.settings.show_hc_product_tray_tooltip,
        show_google_apps_onboarding:            user.settings.show_google_apps_onboarding,
        show_public_comment_warning:            user.settings.show_public_comment_warning,
        show_multiple_ticket_forms_tutorial:    user.settings.show_multiple_ticket_forms_tutorial,
        revere_subscription:                    user.settings.revere_subscription,
        show_enable_google_apps_modal:          user.settings.show_enable_google_apps_modal,
        show_zero_state_tour_ticket:            user.settings.show_zero_state_tour_ticket,
        quick_assist_first_dismissal:           user.settings.quick_assist_first_dismissal,
        show_help_panel_intro_tooltip:          user.settings.show_help_panel_intro_tooltip,
        show_new_comment_filter_tooltip:        user.settings.show_new_comment_filter_tooltip,
        show_first_comment_private_tooltip:     user.settings.show_first_comment_private_tooltip,
        show_answer_bot_dashboard_onboarding:   user.settings.show_answer_bot_dashboard_onboarding,
        show_answer_bot_trigger_onboarding:     user.settings.show_answer_bot_trigger_onboarding,
        show_answer_bot_lotus_onboarding:       user.settings.show_answer_bot_lotus_onboarding,
        show_agent_workspace_onboarding:        user.settings.show_agent_workspace_onboarding,
        has_seen_channel_switching_onboarding:  user.settings.has_seen_channel_switching_onboarding,
        show_manual_start_translation_tooltip:  user.settings.show_manual_start_translation_tooltip,
        show_manual_stop_translation_tooltip:   user.settings.show_manual_stop_translation_tooltip,
        show_composer_will_translate_tooltip:   user.settings.show_composer_will_translate_tooltip,
        show_composer_wont_translate_tooltip:   user.settings.show_composer_wont_translate_tooltip
      }
    end
  end
end
