class Api::V2::Users::AgentPresenter < Api::V2::Users::EndUserPresenter
  include Zendesk::Export::FieldsExport
  self.model_key = :user

  def initialize(actor, options)
    options[:user_url_builder] ||= options[:url_builder]
    super(actor, options)
    @custom_fields = actor.account.custom_fields_for_owner('User')
    @redacted_fields = options[:redacted_fields] || []
    @nil_value_fields = options[:nil_value_fields] || []
    @exclude_non_exportable_fields = options[:exclude_non_exportable_fields] || false
    @include_permanently_deleted = options[:include_permanently_deleted] || false
  end

  ## ### JSON Format for Agent or Admin Requests
  ## When an agent or admin makes the request, users are returned with the following attributes:
  ##
  ## | Name                   | Type                           | Read-only | Mandatory | Comment
  ## | ---------------------- | ------------------------------ | --------- | --------- | -------
  ## | id                     | integer                        | yes       | no        | Automatically assigned when the user is created
  ## | email                  | string                         | no*       | no        | The user's primary email address. *Writeable on create only. On update, a secondary email is added. See [Email Address](#email-address)
  ## | name                   | string                         | no        | yes       | The user's name
  ## | active                 | boolean                        | yes       | no        | `false` if the user has been deleted
  ## | alias                  | string                         | no        | no        | An alias displayed to end users
  ## | chat_only              | boolean                        | yes       | no        | Whether or not the user is a chat-only agent
  ## | created_at             | date                           | yes       | no        | The time the user was created
  ## | custom_role_id         | integer                        | no        | no        | A custom role if the user is an agent on the Enterprise plan
  ## | role_type              | integer                        | yes       | no        | The user's role id. 0 for custom agents, 1 for light agent, 2 for chat agent, and 3 for chat agent added to the Support account as a contributor ([Chat Phase 4](https://support.zendesk.com/hc/en-us/articles/360022365373#topic_djh_1zk_4fb))
  ## | details                | string                         | no        | no        | Any details you want to store about the user, such as an address
  ## | external_id            | string                         | no        | no        | A unique identifier from another system. The API treats the id as case insensitive. Example: ian1 and Ian1 are the same user
  ## | last_login_at          | date                           | yes       | no        | The last time the user signed in to Zendesk Support
  ## | locale                 | string                         | no        | no        | The user's locale. A BCP-47 compliant tag for the locale. If both "locale" and "locale_id" are present on create or update, "locale_id" is ignored and only "locale" is used.
  ## | locale_id              | integer                        | no        | no        | The user's language identifier
  ## | moderator              | boolean                        | no        | no        | Designates whether the user has forum moderation capabilities
  ## | notes                  | string                         | no        | no        | Any notes you want to store about the user
  ## | only_private_comments  | boolean                        | no        | no        | `true` if the user can only create private comments
  ## | organization_id        | integer                        | no        | no        | The id of the user's organization. If the user has more than one [organization memberships](./organization_memberships), the id of the user's default organization
  ## | default_group_id       | integer                        | no        | no        | The id of the user's default group
  ## | phone                  | string                         | no        | no        | The user's primary phone number. See [Phone Number](#phone-number) below
  ## | shared_phone_number    | boolean                        | yes       | no        | Whether the `phone` number is shared or not. See [Phone Number](#phone-number) below
  ## | photo                  | [Attachment](attachments.html) | no        | no        | The user's profile picture represented as an [Attachment](./attachments) object
  ## | restricted_agent       | boolean                        | no        | no        | If the agent has any restrictions; false for admins and unrestricted agents, true for other agents
  ## | role                   | string                         | no        | no        | The user's role. Possible values are `"end-user"`, `"agent"`, or `"admin"`
  ## | shared                 | boolean                        | yes       | no        | If the user is shared from a different Zendesk Support instance. Ticket sharing accounts only
  ## | shared_agent           | boolean                        | yes       | no        | If the user is a shared agent from a different Zendesk Support instance. Ticket sharing accounts only
  ## | signature              | string                         | no        | no        | The user's signature. Only agents and admins can have signatures
  ## | suspended              | boolean                        | no        | no        | If the agent is suspended. Tickets from suspended users are also suspended, and these users cannot sign in to the end user portal
  ## | tags                   | array                          | no        | no        | The user's tags. Only present if your account has user tagging enabled
  ## | ticket_restriction     | string                         | no        | no        | Specifies which tickets the user has access to. Possible values are: `"organization"`, `"groups"`, `"assigned"`, `"requested"`, `null`
  ## | time_zone              | string                         | no        | no        | The user's time zone. See [Time Zone](#time-zone)
  ## | two_factor_auth_enabled| boolean                        | yes       | no        | If two factor authentication is enabled.
  ## | updated_at             | date                           | yes       | no        | The time the user was last updated
  ## | url                    | string                         | yes       | no        | The user's API url
  ## | user_fields            | object                         | no        | no        | Values of custom fields in the user's profile. See [User Fields](#user-fields)
  ## | verified               | boolean                        | no        | no        | Any of the user's identities is verified. See [User Identities](./user_identities)
  ## | report_csv             | boolean                        | yes       | no        | Whether or not the user can access the CSV report on the Search tab of the Reporting page in the Support admin interface. See [Analyzing Help Center search results](https://support.zendesk.com/hc/en-us/articles/203664476) in Help Center. Only available on the Guide Professional plan
  ##
  ##
  ## #### Email Address
  ##
  ## You can specify a user's primary email address when you create the user.
  ## See [Specifying email and verified attributes]
  ## (./users#specifying-email-and-verified-attributes)
  ##
  ## To update a user's primary email address, use the
  ## [Make Identity Primary](./user_identities#make-identity-primary)
  ## endpoint.
  ##
  ## #### Time Zone
  ##
  ## A `time_zone` name consists of a string such as "Eastern Time (US & Canada)".
  ## For a list of valid names, see the **Time zone** list menu on the Localization page
  ## in the Support admin interface (**Admin** > **Account** > **Localization**).
  ## For example, if the menu lists "(GMT+02:00) Berlin", then use "Berlin" as the
  ## `time_zone` name.
  ##
  ## Request body in User API:
  ##
  ## ```javascript
  ## {
  ##   "user": {
  ##     "id":   35436,
  ##     "name": "Johnny Agent",
  ##     "time_zone": "Berlin",
  ##     ...
  ##   }
  ## }
  ## ```
  ##
  ## #### User Fields
  ##
  ## You can use the `user_fields` object to set the value of one or more custom fields in the user's profile.
  ## Specify [field keys](./user_fields#json-format) as the properties to set. Example:
  ##
  ## ```javascript
  ## "user_fields": {
  ##   "membership_level": "silver",
  ##   "membership_expires": "2019-07-23T00:00:00Z"
  ## }
  ## ```
  ##
  ## For more information, see [User Fields](./user_fields) and [Adding custom fields to users](https://support.zendesk.com/hc/en-us/articles/203662066).
  ##
  ## #### Phone Number
  ##
  ## The phone number should comply with the E.164 international [telephone numbering plan](https://en.wikipedia.org/wiki/E.164). Example `+15551234567`.
  ## E164 numbers are international numbers with a country dial prefix, usually an area code and a subscriber number.
  ## A valid E.164 phone number must include a [country calling code](https://en.wikipedia.org/wiki/List_of_country_calling_codes).
  ##
  ## A phone number can be one of the following types:
  ##
  ## * A direct line linked to a single user, which is indicated by a `shared_phone_number` attribute of false. A direct line can be used as a [user identity](./user_identities)
  ## * A shared number linked to multiple users, indicated by a `shared_phone_number` attribute of true. A shared number can not be used as a [user identity](./user_identities)
  ##
  ## See [Understanding how phone numbers are linked to end-user profiles](https://support.zendesk.com/hc/en-us/articles/230553307#topic_hz3_5nm_sx) in the Support Help Center.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                    35436,
  ##   "url":                   "https://company.zendesk.com/api/v2/users/35436.json",
  ##   "name":                  "Johnny Agent",
  ##   "external_id":           "sai989sur98w9",
  ##   "alias":                 "Mr. Johnny",
  ##   "created_at":            "2009-07-20T22:55:29Z",
  ##   "updated_at":            "2011-05-05T10:38:52Z",
  ##   "active":                true,
  ##   "verified":              true,
  ##   "shared":                false,
  ##   "shared_agent":          false,
  ##   "locale":                "en-US",
  ##   "locale_id":             1,
  ##   "time_zone":             "Copenhagen",
  ##   "last_login_at":         "2011-05-05T10:38:52Z",
  ##   "email":                 "johnny@example.com",
  ##   "phone":                 "+15551234567",
  ##   "signature":             "Have a nice day, Johnny",
  ##   "details":               "",
  ##   "notes":                 "Johnny is a nice guy!",
  ##   "organization_id":       57542,
  ##   "role":                  "agent",
  ##   "role_type":             0,
  ##   "custom_role_id":        9373643,
  ##   "moderator":             true,
  ##   "ticket_restriction":    "assigned",
  ##   "only_private_comments": false,
  ##   "tags":                  ["enterprise", "other_tag"],
  ##   "restricted_agent":      true,
  ##   "suspended":             true,
  ##   "photo": {
  ##     "id":           928374,
  ##     "name":         "my_funny_profile_pic.png",
  ##     "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic.png",
  ##     "content_type": "image/png",
  ##     "size":         166144,
  ##     "thumbnails": [
  ##       {
  ##         "id":           928375,
  ##         "name":         "my_funny_profile_pic_thumb.png",
  ##         "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic_thumb.png",
  ##         "content_type": "image/png",
  ##         "size":         58298
  ##       }
  ##     ]
  ##   },
  ##   "user_fields": {
  ##     "user_decimal": 5.1,
  ##     "user_dropdown": "option_1",
  ##     "user_date": "2012-07-23T00:00:00Z"
  ##   }
  ## }
  ## ```
  def model_json(user)
    json = if account.has_ocp_chat_only_agent_deprecation?
      super.merge!(
        external_id:             user.external_id,
        tags:                    has_tags? ? user.tags : [],
        alias:                   user.alias,
        active:                  user.is_active?,
        shared:                  user.foreign?,
        shared_agent:            user.foreign_agent?,
        last_login_at:           user.last_login,
        two_factor_auth_enabled: user.otp_configured?,
        signature:               user.signature.present? ? user.signature.value : nil,
        details:                 user.details,
        notes:                   user.notes,
        role_type:               role_type(user),
        custom_role_id:          custom_role_id(user),
        moderator:               user.is_moderator?,
        ticket_restriction:      ticket_restriction(user),
        only_private_comments:   user.is_private_comments_only?,
        restricted_agent:        user.is_restricted_agent?,
        suspended:               user.suspended?,
        default_group_id:        default_group_id(user),
        report_csv:              user.report_csv
      )
    else
      super.merge!(
        external_id:             user.external_id,
        tags:                    has_tags? ? user.tags : [],
        alias:                   user.alias,
        active:                  user.is_active?,
        shared:                  user.foreign?,
        shared_agent:            user.foreign_agent?,
        last_login_at:           user.last_login,
        two_factor_auth_enabled: user.otp_configured?,
        signature:               user.signature.present? ? user.signature.value : nil,
        details:                 user.details,
        notes:                   user.notes,
        role_type:               role_type(user),
        custom_role_id:          custom_role_id(user),
        moderator:               user.is_moderator?,
        ticket_restriction:      ticket_restriction(user),
        only_private_comments:   user.is_private_comments_only?,
        restricted_agent:        user.is_restricted_agent?,
        suspended:               user.suspended?,
        chat_only:               chat_only(user),
        default_group_id:        default_group_id(user),
        report_csv:              user.report_csv
      )
    end

    json[:blocklisted] = user.cc_blocklisted? if side_load?(:blocklist)

    json[:user_fields] = if account.has_user_and_organization_fields?
      custom_fields_hash(user)
    else
      {}
    end

    if @include_permanently_deleted && user.deleted?
      json[:permanently_deleted] = user.ultra_deleted?
    end

    if side_load?(:settings) && user.is_agent?
      json[:settings] = settings_presenter.model_json(user)
    end

    if side_load?(:abilities)
      json[:abilities] = user_abilities_presenter.model_json(user)
    end

    if side_load?(:external_permissions)
      json[:external_permissions] = user_permissions_presenter.model_json(user)
    end

    # Hide is_assuming_user_from_monitor property for non-Z1 users.
    zd_session = Zendesk::AuthenticatedSession.new(options[:shared_session] || {})
    if zd_session.is_assuming_user_from_monitor?
      json[:is_assuming_user_from_monitor] = true
    end

    @redacted_fields.each { |f| json.delete(f) }

    unless user.is_agent?
      @nil_value_fields.each { |f| json[f] = nil }
    end

    json
  end

  def chat_only(user)
    if account.has_chat_permission_set?
      user.is_chat_agent?
    else
      false
    end
  end

  def side_loads(user)
    super.tap do |json|
      users = Array(user)

      if side_load?(:roles)
        with_sets, with_roles = users.partition(&:has_permission_set?)

        json[:roles] = system_role_presenter.collection_presenter.model_json(with_roles.map(&:roles).uniq)
        json[:roles].concat(side_load_association(with_sets, :permission_set, custom_role_presenter))
      end

      if side_load?(:groups)
        json[:groups] = side_load_association(users, :groups, group_presenter)
      end

      # Need to remove this once all the customers switch to use open_ticket_count
      if side_load?(:open_ticket_counts)
        ids = users.map(&:id)
        counts = account.tickets.where(status_id: StatusType.OPEN, assignee_id: ids).group(:assignee_id).count
        (ids - counts.keys).each { |id| counts[id] = 0 } # zero counts return nil
        json[:open_ticket_counts] = counts
      end

      if side_load?(:open_ticket_count)
        ids = users.map(&:id)
        counts = account.tickets.where(status_id: StatusType.OPEN, assignee_id: ids).group(:assignee_id).count
        (ids - counts.keys).each { |id| counts[id] = 0 } # zero count return nil
        json[:open_ticket_count] = counts
      end

      # User's related data will not be sideloaded on collections of user to
      # avoid n+1 queries
      if side_load?(:related) && users.one?
        json[:related] = related_presenter.present(user)
      end
    end
  end

  def association_preloads
    super.merge!(
      settings:            {},
      signature:           {},
      otp_setting:         {},
      custom_field_values: {field: {dropdown_choices: {}}},
      permission_set:      {permissions: {}},
      memberships:         {}
    ).tap do |includes|
      includes[:taggings] = {} if has_tags?
      includes[:user_seats] = {} if side_load?(:abilities) && account.voice_enabled?
    end
  end

  def url(user)
    return nil if user.is_anonymous_user?

    # url builder significantly slower than string concatenation
    # when called 1000s of times.
    @cached_url_path_part ||= options[:user_url_builder].send(:api_v2_user_url, user, format: :json).to_s.gsub(/\d+.json$/, '')

    @cached_url_path_part.clone << user.id.to_s << '.json'
  end

  protected

  def default_group_id(user)
    # Preload the user's membeships and get the default
    # This is less disruptive but not optimal
    user.memberships.detect(&:default?).try(:group_id)
  end

  def custom_fields_hash(user)
    human_label_custom_fields(user)
  end

  def human_label_custom_fields(user)
    user_fields = user.custom_field_values.as_json(custom_fields: @custom_fields, account: account)
    fields_for_export(user_fields, @exclude_non_exportable_fields)
  end

  def ticket_restriction(user)
    if Api::V2::UserParams::RESTRICTION_MAP.key?(user.restriction_id)
      Api::V2::UserParams::RESTRICTION_MAP[user.restriction_id]
    else
      user.is_agent? ? nil : 'requested'
    end
  end

  def role_type(user)
    user.has_permission_set? ? user.permission_set.role_type : nil
  end

  def custom_role_id(user)
    user.has_permission_set? ? user.custom_role_id : nil
  end

  def related_presenter
    @related_presenter ||= ::Api::V2::UserRelatedPresenter.new(user, options)
  end

  def settings_presenter
    @settings_presenter ||= Api::V2::Users::SettingsPresenter.new(user, options)
  end

  def system_role_presenter
    @system_role_presenter ||= Api::V2::Roles::SystemRolePresenter.new(user, options)
  end

  def custom_role_presenter
    @custom_role_presenter ||= Api::V2::Roles::CustomRolePresenter.new(user, options)
  end
end
