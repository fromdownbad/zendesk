## ### JSON Format
## Users are presented via the Api::V2::Users::AgentPresenter or Api::V2::Users::EndUserPresenter
## ```
class Api::V2::Users::Presenter < Api::V2::Presenter
  def self.new(user, options)
    klass = if user.is_end_user?
      Api::V2::Users::EndUserPresenter
    else
      Api::V2::Users::AgentPresenter
    end
    klass.new(user, options)
  end
end
