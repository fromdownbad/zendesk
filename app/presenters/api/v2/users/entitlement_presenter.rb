class Api::V2::Users::EntitlementPresenter < Api::V2::Presenter
  self.model_key = :entitlements

  ENTITLEMENTS = %w[chat explore talk].freeze

  ## #### Example
  ##
  ## ```js
  ## {
  ##   {
  ##   "entitlements" : {
  ##     "chat" : {
  ##       "name" : "admin",
  ##       "is_active" : "false"
  ##     },
  ##     "connect" : {
  ##       "name" : "admin",
  ##       "is_active" : "false"
  ##     },
  ##     "explore" : {
  ##       "name" : "admin",
  ##       "is_active" : "false"
  ##     }
  ##   }
  ## }
  ## ```

  def model_json(model)
    map_entitlements(model)
  end

  private

  def map_entitlements(entitlements)
    Hash[ENTITLEMENTS.map { |e| [e, map_entitlement(entitlements, e)] }]
  end

  def map_entitlement(entitlements, product)
    entitlement = entitlements[product] unless entitlements.nil?
    return { 'is_active': false } if entitlement.nil?

    { 'name': entitlement['name'], 'is_active': entitlement['is_active'] }
  end
end
