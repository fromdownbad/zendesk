class Api::V2::Users::EndUserPresenter < Api::V2::Presenter
  self.model_key = :user

  ## ### JSON Format for End-user Requests
  ## When an end-user makes the request, the user is returned with the following attributes:
  ##
  ## | Name                  | Type                           | Read-only | Mandatory | Comment
  ## | --------------------- | ------------------------------ | --------- | --------- | -------
  ## | id                    | integer                        | yes       | no        | Automatically assigned when creating users
  ## | email                 | string                         | no        | no        | The primary email address of this user
  ## | name                  | string                         | no        | yes       | The name of the user
  ## | created_at            | date                           | yes       | no        | The time the user was created
  ## | locale                | string                         | yes       | no        | The locale for this user
  ## | locale_id             | integer                        | no        | no        | The language identifier for this user
  ## | organization_id       | integer                        | no        | no        | The id of the user's organization. If the user has more than one [organization memberships](./organization_memberships), the id of the user's default organization
  ## | phone                 | string                         | no        | no        | The primary phone number of this user. See [Phone Number](./users#phone-number) in the Users API
  ## | shared_phone_number   | boolean                        | yes       | no        | Whether the `phone` number is shared or not. See [Phone Number](./users#phone-number) in the Users API
  ## | photo                 | [Attachment](attachments.html) | no        | no        | The user's profile picture represented as an [Attachment](./attachments) object
  ## | role                  | string                         | no        | no        | The role of the user. Possible values: `"end-user"`, `"agent"`, `"admin"`
  ## | time_zone             | string                         | no        | no        | The time-zone of this user
  ## | updated_at            | date                           | yes       | no        | The time of the last update of the user
  ## | url                   | string                         | yes       | no        | The API url of this user
  ## | verified              | boolean                        | no        | no        | Any of the user's identities is verified. See [User Identities](./user_identities)
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                    35436,
  ##   "url":                   "https://company.zendesk.com/api/v2/end_users/35436.json",
  ##   "name":                  "Johnny End User",
  ##   "created_at":            "2009-07-20T22:55:29Z",
  ##   "updated_at":            "2011-05-05T10:38:52Z",
  ##   "time_zone":             "Copenhagen",
  ##   "email":                 "johnny@example.com",
  ##   "phone":                 "+15551234567",
  ##   "locale":                "en-US",
  ##   "locale_id":             1,
  ##   "organization_id":       57542,
  ##   "role":                  "end-user",
  ##   "verified":              true,
  ##   "photo": {
  ##     "id":           928374,
  ##     "name":         "my_funny_profile_pic.png",
  ##     "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic.png",
  ##     "content_type": "image/png",
  ##     "size":         166144,
  ##     "thumbnails": [
  ##       {
  ##         "id":           928375,
  ##         "name":         "my_funny_profile_pic_thumb.png",
  ##         "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic_thumb.png",
  ##         "content_type": "image/png",
  ##         "size":         58298,
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  def model_json(user, _options = {})
    json = condensed_model_json(user).merge!(
      created_at:          user.created_at,
      updated_at:          user.updated_at,
      time_zone:           user.time_zone,
      iana_time_zone:      user.iana_time_zone,
      phone:               user.phone_number.presence,
      shared_phone_number: user.shared_phone_number,
      photo:               photo(user),
      locale_id:           user.translation_locale.id,
      locale:              user.translation_locale.locale,
      organization_id:     user.organization.try(:id),
      role:                user.role.to_s.downcase.presence,
      verified:            user.is_verified?
    )

    json[:blocklisted] = user.cc_blocklisted? if side_load?(:blocklist)

    if account.has_private_attributes_in_end_user_presenter?
      json[:external_id] = user.external_id
      unless account.has_api_tags_disabled_for_end_user_presenter?
        json[:tags] = has_tags? ? user.tags : []
      end
    end

    if account.has_api_add_organization_ids_to_users?
      json[:organization_ids] = user.current_organization_ids
    end

    if account.has_voice_formatted_phone_numbers?
      json[:formatted_phone] = user.formatted_phone_number
    end

    if options[:include_authenticity_token] && user == self.user
      json[:authenticity_token] = options[:url_builder].send(:form_authenticity_token)
    end

    json
  end

  def condensed_model_json(user)
    {
      id:    user.id,
      url:   url(user),
      name:  user.name,
      email: user.email
    }
  end

  def side_loads(user)
    json = {}

    users = Array(user)

    if side_load?(:identities)
      json[:identities] = side_load_association(users, :identities, identity_presenter)
    end

    if side_load?(:organizations)
      json[:organizations] = side_load_association(users, :organizations, organization_presenter)
    end

    if side_load?(:highlights)
      json[:highlights] = options[:highlights]
    end

    json
  end

  def url(user)
    return nil if user.is_anonymous_user?

    url_builder.send(:api_v2_end_user_url, user, format: :json)
  end

  def association_preloads
    {
      identities:               {},
      photo:                    {
        thumbnails: {}
      },
      account:                  {},
      # Organization memberships needs to be preloaded before organization, otherwise preloading the organization
      # causes the organization_memberships to be loaded for the user, causing an N+1
      organization_memberships: {},
      organization:             {}
    }
  end

  private

  def photo(user)
    return nil unless user.photo.present?

    attachment_presenter.model_json(user.photo)
  end

  def has_tags? # rubocop:disable Naming/PredicateName
    @account_has_tags ||= account.has_user_and_organization_tags?
  end

  def identity_presenter
    @identity_presenter ||= options[:identity_presenter] ||= Api::V2::IdentityPresenter.new(user, options)
  end
end
