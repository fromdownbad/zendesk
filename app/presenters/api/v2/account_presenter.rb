class Api::V2::AccountPresenter < Api::V2::Presenter
  self.model_key = :account

  ## ### JSON Format
  ## Accounts are represented as JSON objects which have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | name            | string  | no        | yes       | The name of the account
  ## | subdomain       | string  | no        | no        | The subdomain of the account
  ## | sandbox         | boolean | yes       | no        | If this account is a sandbox
  ## | time_format     | integer | no        | no        | The clock format to use. Returns 12 or 24.
  ## | time_zone       | string  | no        | no        | The default time zone for this account
  ## | owner_id        | number  | no        | no        | The owner of the account
  ## | multiproduct    | number  | yes       | no        | If this account is multiproduct
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "url":         "https://example.zendesk.com/",
  ##   "name":        "Example Company",
  ##   "subdomain":   "example",
  ##   "sandbox":     false,
  ##   "time_format": 24,
  ##   "time_zone":   "Pacific Time (US & Canada)",
  ##   "owner_id":    16,
  ##   "multiproduct" false
  ## }
  ## ```

  def model_json(account)
    attributes = public_attributes(account)
    attributes.merge!(internal_attributes(account)) if options[:internal]
    super.merge!(attributes)
  end

  def internal_attributes(account)
    { id: account.id, active: account.is_serviceable }
  end

  def public_attributes(account)
    {
      name: account.name,
      sandbox: account.is_sandbox?,
      subdomain: account.subdomain,
      time_format: account.time_format,
      time_zone: account.time_zone,
      owner_id: account.owner_id,
      multiproduct: account.multiproduct?
    }
  end

  def url(account)
    account.url
  end

  def owner(user)
    user_presenter.model_json(user)
  end

  def side_loads(account)
    json = {}

    if side_load?(:settings)
      json[:settings] = settings_presenter.model_json(account)
    end

    if side_load?(:subscription)
      json[:subscription] = subscription_presenter.model_json(account.subscription)
    end

    if side_load?(:deployments)
      json[:deployments] = deployments_presenter.model_json(account)
    end

    if side_load?(:sandbox)
      json[:sandbox] = sandbox_presenter.model_json(account)
    end

    if side_load?(:magento)
      json[:magento] = magento_presenter.model_json(account)
    end

    if side_load?(:hc_settings)
      json[:hc_settings] = hc_settings_presenter.model_json(account)
    end

    if side_load?(:partner_settings)
      json[:partner_settings] = partner_settings_presenter.model_json(account)
    end

    if side_load?(:voice_settings)
      json[:voice_settings] = voice_settings_presenter.model_json(account)
    end

    if side_load?(:voice_subscription)
      json[:voice_subscription] = voice_subscription_presenter.model_json(account.voice_subscription)
    end

    if side_load?(:remote_authentications)
      remote_authentications = account.remote_authentications
      json[:remote_authentications] = remote_authentications_presenter.
        collection_presenter.model_json(remote_authentications)
    end

    if side_load?(:secondary_subscriptions)
      secondary_subscriptions = account.subscription_feature_addons.
        active.by_name(:temporary_zendesk_agents)

      json[:secondary_subscriptions] = secondary_subscriptions_presenter.
        collection_presenter.model_json(secondary_subscriptions)
    end

    if side_load?(:zopim_integration)
      json[:zopim_integration] = zopim_integration_presenter.model_json(
        account.zopim_integration
      )
    end

    if side_load?(:zopim_subscription)
      json[:zopim_subscription] = zopim_subscription_presenter.model_json(
        account.zopim_subscription
      )
    end

    if side_load?(:address)
      json[:address] = address_presenter.model_json(account.address)
    end

    if side_load?(:users)
      json[:users] = [owner(account.owner)]
    end

    json[:account_id] = account.id if side_load?(:account_id)

    json
  end

  def sandbox_presenter
    @sandbox_presenter ||= options[:sandbox_presenter] ||= Api::V2::Account::SandboxPresenter.new(user, options)
  end

  def settings_presenter
    @settings_presenter ||= options[:settings_presenter] ||= Api::V2::Account::SettingsPresenter.new(user, options)
  end

  def subscription_presenter
    @subscription_presenter ||= options[:subscription_presenter] ||= Api::V2::Account::SubscriptionPresenter.new(user, options)
  end

  def deployments_presenter
    @deployments_presenter ||= options[:deployments_presenter] ||= Api::V2::Account::DeploymentsPresenter.new(user, options)
  end

  def magento_presenter
    @magento_presenter ||= options[:magento_presenter] ||= Api::V2::Account::MagentoAccountCreationPresenter.new(user, options)
  end

  def hc_settings_presenter
    @hc_settings_presenter ||= options[:hc_settings_presenter] ||= Api::V2::Account::HcSettingsPresenter.new(user, options)
  end

  def voice_settings_presenter
    @voice_settings_presenter ||= options[:voice_settings_presenter] ||= Api::V2::Account::VoiceSettingsPresenter.new(user, options)
  end

  def voice_subscription_presenter
    @voice_subscription_presenter ||= options[:voice_subscription_presenter] ||= Api::V2::Account::VoiceSubscriptionPresenter.new(user, options)
  end

  def partner_settings_presenter
    @partner_settings_presenter ||= options[:partner_settings_presenter] ||= Api::V2::Account::PartnerSettingsPresenter.new(user, options)
  end

  def remote_authentications_presenter
    @remote_authentications_presenter ||= options[:remote_authentications_presenter] ||= Api::V2::Account::RemoteAuthenticationPresenter.new(user, options)
  end

  def secondary_subscriptions_presenter
    @secondary_subscriptions_presenter ||= options[:secondary_subscriptions_presenter] ||= Api::V2::Account::SecondarySubscriptionsPresenter.new(user, options)
  end

  def zopim_subscription_presenter
    @zopim_subscription_presenter ||= options[:zopim_subscription_presenter] ||= Api::V2::Account::ZopimSubscriptionPresenter.new(user, options)
  end

  def zopim_integration_presenter
    @zopim_integration_presenter ||= options[:zopim_integration_presenter] ||= Api::V2::ZopimIntegrationPresenter.new(user, options)
  end

  def address_presenter
    @address_presenter ||= options[:address_presenter] ||= Api::V2::AddressPresenter.new(user, options)
  end
end
