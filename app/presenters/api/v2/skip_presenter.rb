module Api::V2
  class SkipPresenter < Api::V2::Presenter
    self.model_key = :skip

    ## ### JSON Format
    ##
    ## Skips are represented as JSON objects which have the following keys:
    ##
    ## | Name       | Type                               | Comment
    ## | ---------- | ---------------------------------- | ------------------------------------
    ## | id         | integer                            | Automatically assigned upon creation
    ## | ticket_id  | integer                            | ID of the skipped ticket
    ## | user_id    | integer                            | ID of the skipping agent
    ## | reason     | string                             | Reason for skipping the ticket
    ## | ticket     | [Ticket](tickets.html#json-format) | The skipped ticket
    ## | created_at | date                               | Time the skip was created
    ## | updated_at | date                               | Time the skip was last updated
    ##
    ## #### Example
    ## ```js
    ## {
    ##   "id": 1,
    ##   "ticket_id": 123,
    ##   "user_id": 456,
    ##   "reason": "I have no idea what I'm doing.",
    ##   "ticket": {
    ##     "id":      123,
    ##     "subject": "Printer on fire",
    ##     ... // see the [Ticket documentation](tickets.html#json-format) for an example of a full Ticket object
    ##   },
    ##   "created_at": "2015-09-30T21:44:03Z",
    ##   "updated_at": "2015-09-30T21:44:03Z"
    ## }
    ## ```
    def model_json(skip)
      {
        id:         skip.id,
        ticket_id:  skip.ticket.try(:nice_id),
        user_id:    skip.user_id,
        reason:     skip.reason,
        created_at: skip.created_at,
        updated_at: skip.updated_at
      }.tap do |json|
        if skip.ticket.present?
          json[:ticket] = ticket_presenter.model_json(skip.ticket)
        end
      end
    end

    private

    def ticket_presenter
      @ticket_presenter ||= begin
        Tickets::TicketPresenter.new(user, url_builder: options[:url_builder])
      end
    end
  end
end
