class Api::V2::ForumPresenter < Api::V2::Presenter
  self.model_key = :forum
  self.url_param = :id

  ## ### JSON Format
  ## Forums are represented with the following attributes:
  ##
  ## | Name               | Type    | Read-only | Mandatory | Comment
  ## | ------------------ | ------- | --------- | --------- | -------
  ## | id                 | integer | yes       | no        | Automatically assigned upon creation
  ## | url                | string  | yes       | no        | The API url of this forum
  ## | name               | string  | no        | yes       | The name of the forum
  ## | description        | string  | no        | no        | A description of the forum
  ## | category_id        | integer | no        | no        | Category this forum is in
  ## | organization_id    | integer | no        | no        | Organization this forum is restricted to
  ## | locale_id          | integer | no        | no        | User locale id this forum is restricted to
  ## | locked             | boolean | no        | no        | Whether this forum is locked such that new entries and comments cannot be made
  ## | unanswered_topics  | integer | yes       | no        | Contains the number of unanswered questions if this forum's topics are questions.
  ## | position           | integer | no        | no        | The position of this forum relative to other forums in the same category
  ## | forum_type         | string  | no        | no        | The type of the topics in this forum, valid values: "articles", "ideas" or "questions"
  ## | access             | string  | no        | no        | Who has access to this forum, valid values: "everybody", "logged-in users" or "agents only"
  ## | tags               | array   | no        | no        | Restrict access to end-users and organizations with all these tags
  ## | created_at         | date    | yes       | no        | The time the forum was created
  ## | updated_at         | date    | yes       | no        | The time of the last update of the forum
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "url":             "https://company.zendesk.com/api/v2/forums/35436.json",
  ##   "name":            "FAQs",
  ##   "description:":    "This forum contains all product FAQs",
  ##   "category_id":     null,
  ##   "organization_id": null,
  ##   "locale_id":       null,
  ##   "locked":          true,
  ##   "position":        4,
  ##   "forum_type":      "articles",
  ##   "access":          "everybody",
  ##   "tags":            ["foo", "bar"],
  ##   "created_at":      "2010-07-20T22:55:29Z",
  ##   "updated_at":      "2012-03-05T10:38:52Z"
  ## }
  ## ```
  def model_json(forum)
    json = {
      id: forum.id,
      name: forum.name,
      description: forum.description,
      category_id: forum.category_id,
      organization_id: forum.organization_id,
      locale_id: forum.translation_locale_id,
      locked: forum.is_locked,
      position: forum.position,
      forum_type: forum.forum_type,
      access: forum.access,
      tags: forum.tag_array,
      created_at: forum.created_at,
      updated_at: forum.updated_at
    }

    if forum.display_type.questions?
      json[:unanswered_topics] = forum.unanswered_questions.count(:all)
    end

    super.merge!(json)
  end
end
