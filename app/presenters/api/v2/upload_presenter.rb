class Api::V2::UploadPresenter < Api::V2::Presenter
  self.model_key = :upload

  ## ### JSON Format
  ##
  ## | Name        | Type   | Comment
  ## | ----------- | ------ | -------
  ## | token       | string | Automatically assigned upon creation
  ## | expires_at  | date   | The time the token will expire
  ## | attachment  | object | The newly-uploaded attachment (if any)
  ## | attachments | array  | The attachments on this upload as [Attachment](./attachments) objects
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "token": "6bk3gql82em5nmf",
  ##   "expires_at": "2012-02-20T22:55:29Z",
  ##   "attachment": {
  ##     "id":           498483,
  ##     "name":         "crash.log",
  ##     "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##     "content_type": "text/plain",
  ##     "size":         2532,
  ##     "thumbnails":   []
  ##   },
  ##   "attachments": [
  ##     {
  ##       "id":           498483,
  ##       "name":         "crash.log",
  ##       "content_url":  "https://company.zendesk.com/attachments/crash.log",
  ##       "content_type": "text/plain",
  ##       "size":         2532,
  ##       "thumbnails":   []
  ##     }
  ##   ]
  ## }
  ## ```
  def model_json(token)
    attachment = @options[:attachment]
    {
      token: token.value,
      expires_at: token.expires_at,
      attachments: attachment_presenter.collection_presenter.model_json(token.attachments.originals),
    }.tap do |json|
      json[:attachment] = attachment_presenter.model_json(attachment) if attachment
    end
  end
end
