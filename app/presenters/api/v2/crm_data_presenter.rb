class Api::V2::CrmDataPresenter < Api::V2::Presenter
  self.model_key = :crm_data

  ## ### JSON Format
  ## Represents CRM data for CRM integrations
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned during creation
  ## | status          | integer | yes       | no        | Current state of the data, either "ok", "pending" or "errored"
  ## | type            | string  | yes       | no        | CRM implementation, either "sugar_crm_data" or "salesforce_data"
  ## | records         | array   | yes       | no        | Array of CRM specific records
  ## | created_at      | date    | yes       | no        | The time the record got created
  ## | updated_at      | date    | yes       | no        | The time the was last updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              561,
  ##   "status":          "pending",
  ##   "type":            "sugar_crm_data",
  ##   "records":         [ { "label": "Juan", "record_type": "Contact", "fields": [ { "label": "Phone", "value": "555-555-5555"} ] }],
  ##   "created_at":      "2009-07-20T22:55:29Z",
  ##   "updated_at":      "2011-05-05T10:38:52Z"
  ## }
  ## ```
  def model_json(crm_model)
    {
      id: crm_model.id,
      status: crm_model.status,
      type: crm_model.type.underscore,
      records: crm_model.records,
      created_at: crm_model.created_at,
      updated_at: crm_model.updated_at
    }
  end
end
