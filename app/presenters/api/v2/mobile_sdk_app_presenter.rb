# Used in `Api::V2::MobileSdkAppsController`
class Api::V2::MobileSdkAppPresenter < Api::V2::Presenter
  self.model_key = :mobile_sdk_app

  # ### Format
  # Presents basic details about a mobile SDK app identifier
  #
  # ### Features
  #
  # | Name                    | Type    | Comment
  # | ----------------------- | ------- | -------
  # | id                      | string  | The mobile SDK app identifier
  # | title                   | string  | The mobile SDK app title
  # | identifier              | string  | The mobile SDK app identifier
  # | mobile_sdk_auth_id      | integer | The mobile SDK app mobile_sdk_auth relation identifier
  # | account_id              | integer | The mobile SDK app account relation identifier
  # | brand_id                | integer | The mobile SDK app brand relation identifier
  # | created_at              | string  | The mobile SDK app creation date
  # | updated_at              | string  | The mobile SDK app last update date
  #
  # #### Example
  #
  # ```js
  #   {
  #   "mobile_sdk_app": {
  #     "title": "Foo",
  #     "brand_id": 1,
  #     "identifier": "2f51ff70dd678a6e1cb93d147a8e6f88a9b517647222d09e",
  #     "client_identifier": "mobile_sdk_client_dec70ea81c23554c28d0",
  #     "settings": {
  #       "contact_us_tags": [
  #         "foo",
  #         "bar"
  #       ],
  #       "conversations_enabled": true,
  #       "rma_enabled": true,
  #       "rma_visits": 1,
  #       "rma_duration": 1,
  #       "rma_delay": 1,
  #       "rma_tags": [],
  #       "helpcenter_enabled": true,
  #       "rma_android_store_url": "http://android.com/store/foo",
  #       "rma_ios_store_url": "http://apple.com/store/foo",
  #       "authentication": "jwt",
  #       "push_notifications_enabled": true,
  #       "push_notifications_callback": "http://foo.com/foo/push",
  #       "push_notifications_type": "webhook",
  #       "urban_airship_key": "ua_key",
  #       "urban_airship_master_secret": "ua_secret",
  #       "connect_enabled": true,
  #       "collect_only_required_data": true,
  #       "support_never_request_email": true,
  #       "support_show_closed_requests": true,
  #       "support_show_referrer_logo": true,
  #       "answer_bot": true,
  #       "support_show_csat: true
  #     },
  #     "mobile_sdk_auth": {
  #       "endpoint": "https://foo.com/foo/webhook",
  #       "shared_secret": "dlnfwhiou4goiwehgportjghprt"
  #     },
  #     "url": "https://support.zd-dev.com/api/v2/mobile_sdk_apps/10037.json"
  #   }
  # }
  # ```
  def model_json(mobile_sdk_app)
    json = {
      id: mobile_sdk_app.id,
      title: mobile_sdk_app.title,
      brand_id: mobile_sdk_app.brand_id,
      signup_source: mobile_sdk_app.signup_source,
      identifier: mobile_sdk_app.identifier,
      client_identifier: mobile_sdk_app.mobile_sdk_auth.client.identifier,
      settings: {
        contact_us_tags: mobile_sdk_app.settings.contact_us_tags,
        conversations_enabled: mobile_sdk_app.settings.conversations_enabled,
        rma_enabled: mobile_sdk_app.settings.rma_enabled,
        rma_visits: mobile_sdk_app.settings.rma_visits,
        rma_duration: mobile_sdk_app.settings.rma_duration,
        rma_delay: mobile_sdk_app.settings.rma_delay,
        rma_tags: mobile_sdk_app.settings.rma_tags,
        helpcenter_enabled: mobile_sdk_app.settings.helpcenter_enabled,
        help_center_article_voting_enabled: mobile_sdk_app.settings.help_center_article_voting_enabled,
        rma_android_store_url: mobile_sdk_app.settings.rma_android_store_url,
        rma_ios_store_url: mobile_sdk_app.settings.rma_ios_store_url,
        authentication: mobile_sdk_app.settings.authentication,
        push_notifications_enabled: mobile_sdk_app.settings.push_notifications_enabled,
        push_notifications_callback: mobile_sdk_app.settings.push_notifications_callback,
        push_notifications_type: mobile_sdk_app.settings.push_notifications_type,
        urban_airship_key: mobile_sdk_app.settings.urban_airship_key,
        urban_airship_master_secret: mobile_sdk_app.settings.urban_airship_master_secret,
        connect_enabled: mobile_sdk_app.settings.connect_enabled,
        blips: mobile_sdk_app.settings.blips,
        required_blips: mobile_sdk_app.settings.required_blips,
        behavioural_blips: mobile_sdk_app.settings.behavioural_blips,
        pathfinder_blips: mobile_sdk_app.settings.pathfinder_blips,
        support_never_request_email: mobile_sdk_app.support_never_request_email?,
        support_show_closed_requests: mobile_sdk_app.support_show_closed_requests?,
        support_show_referrer_logo: mobile_sdk_app.support_show_referrer_logo?,
        answer_bot: mobile_sdk_app.answer_bot_enabled?,
        support_show_csat: mobile_sdk_app.support_show_csat?
      },
      mobile_sdk_auth: {
        endpoint: mobile_sdk_app.mobile_sdk_auth.try(:endpoint),
        shared_secret: mobile_sdk_app.mobile_sdk_auth.try(:shared_secret, true)
      },
      url: url(mobile_sdk_app)
    }

    if mobile_sdk_app.account.has_mobile_sdk_blip_collection_control?
      json[:settings][:collect_only_required_data] = mobile_sdk_app.settings.collect_only_required_data
    end

    json
  end

  def url(mobile_sdk_app)
    url_builder.send(:api_v2_mobile_sdk_app_url, mobile_sdk_app.identifier, format: :json)
  end
end
