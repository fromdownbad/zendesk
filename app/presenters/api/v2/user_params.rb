class Api::V2::UserParams
  RESTRICTION_MAP = {
    Zendesk::Types::RoleRestrictionType.ORGANIZATION => 'organization',
    Zendesk::Types::RoleRestrictionType.GROUPS       => 'groups',
    Zendesk::Types::RoleRestrictionType.ASSIGNED     => 'assigned',
    Zendesk::Types::RoleRestrictionType.REQUESTED    => 'requested',
    Zendesk::Types::RoleRestrictionType.NONE         => nil
  }.freeze

  INVERSE_RESTRICTION_MAP = RESTRICTION_MAP.invert.freeze

  def initialize(params)
    @params = params
    # permit! is necessary here. Otherwise, we get an unpermitted
    # container, which we could just do with {}
    @params[:user] ||= ActionController::Parameters.new.permit!
  end

  def to_hash
    @hash ||= @params.dup.tap do |hash|
      user_params = hash[:user].dup
      user_params[:is_moderator]             = !!user_params.delete(:moderator)   if user_params.key?(:moderator)
      user_params[:is_private_comments_only] = only_private_comments(user_params) if user_params.key?(:only_private_comments)
      user_params[:is_verified]              = user_params.delete(:verified)      if user_params.key?(:verified)
      user_params[:restriction_id]           = restriction_id(user_params)        if user_params.key?(:ticket_restriction)
      hash[:photo]                           = user_params.delete(:photo)         if user_params.key?(:photo)

      # locale takes precedent over locale_id
      user_params.delete(:locale_id) if user_params.key?(:locale)

      hash[:user] = user_params
    end
  end

  protected

  def restriction_id(user_params)
    INVERSE_RESTRICTION_MAP[user_params.delete(:ticket_restriction)]
  end

  def only_private_comments(user_params)
    user_params.delete(:only_private_comments) || false
  end
end
