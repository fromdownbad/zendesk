class Api::V2::SatisfactionReasonPresenter < Api::V2::Presenter
  self.model_key = :satisfaction_reason

  def initialize(user, options = {})
    super(user, options)

    @brand = options.fetch(:brand, nil)
    @locale = options.fetch(:locale, nil)
  end

  ## ### JSON Format
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | url             | string  | yes       | no        | API URL for the resource
  ## | reason_code     | integer | yes       | no        | An account-level code for referencing the reason. Custom reasons are assigned an auto-incrementing integer (non-system reason codes begin at 1000). See [Reason codes](#reason-codes)
  ## | value           | string  | no        | yes       | Translated value of the reason in the account locale.
  ## | raw_value       | string  | no        | no        | The dynamic content placeholder, if present, or the current "value", if not. See [Dynamic Content](dynamic_content.html)
  ## | created_at      | date    | yes       | no        | The time the reason was created
  ## | updated_at      | date    | yes       | no        | The time the reason was updated
  ## | deleted_at      | date    | yes       | no        | The time the reason was deleted
  ##
  ## #### Reason codes
  ##
  ## The follow-up question has the following default reasons the user
  ## can select for giving a negative rating:
  ##
  ## | Code | Reason
  ## | ---- | ------
  ## | 0    | No reason provided (the user didn't select a reason from the list menu)
  ## | 5    | The issue took too long to resolve
  ## | 6    | The issue was not resolved
  ## | 7    | The agent's knowledge is unsatisfactory
  ## | 8    | The agent's attitude is unsatisfactory
  ## | 100  | Some other reason
  ##
  ## An admin in Zendesk Support can create custom reasons. Any custom reason
  ## is assigned a code of 1000 or higher when the reason is created. See
  ## [Customizing and localizing satisfaction reasons](https://support.zendesk.com/hc/en-us/articles/223152967#topic_u2m_c4b_rw) in the Support Help Center.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   reason: {
  ##     "id":              35436,
  ##     "url":             "https://company.zendesk.com/api/v2/satisfaction_reasons/35436.json",
  ##     "reason_code":     1003,
  ##     "value":           "Agent did not respond quickly",
  ##     "raw_value":       "{{dc.reason_code_1003}}",
  ##     "updated_at":      "2011-07-20T22:55:29Z",
  ##     "created_at":      "2011-07-20T22:55:29Z",
  ##     "deleted_at":      "2012-03-12T12:45:32Z",
  ##   }
  ## }
  ## ```

  def model_json(reason)
    super.merge(
      id: reason.id,
      reason_code: reason.reason_code,
      value: reason.translated_value(@locale),
      raw_value: reason.value,
      active: !reason.deactivated?(@brand),
      created_at: reason.created_at,
      updated_at: reason.updated_at,
      deleted_at: reason.deleted_at
    )
  end

  def association_preloads
    {
      brands: {}
    }
  end
end
