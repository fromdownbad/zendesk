class Api::V2::DashboardPresenter < Api::V2::Presenter
  include DashboardHelper

  self.model_key = :dashboard

  ## ### JSON Format
  ## The Dashboard is represented as a JSON object which has the following keys:
  ##
  ## | Name                                | Type    | Read-only | Comment
  ## | ----------------------------------- | ------- | --------- | -------
  ## | open_tickets                        | integer | yes       | Number of open tickets assigned to the agent.
  ## | new_and_open_tickets_groups         | integer | yes       | Number of new and open tickets in agent's groups.
  ## | tickets_solved_this_week            | integer | yes       | Number of tickets solved this week by the agent.
  ## | tickets_rated_good_this_week        | integer | yes       | If satisfaction ratings are enabled, number of agent's tickets rated good this week.
  ## | tickets_rated_bad_this_week         | integer | yes       | Number of agent's tickets rated bad this week.
  ## | sat_score_last_60_days              | integer | yes       | Overall satisfaction score for the agent over the last 60 days.
  ## | account_sat_score_last_60_days      | integer | yes       | Overall satisfaction score for the account over the last 60 days.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "open_tickets":  8,
  ##   "new_and_open_tickets_groups":  212,
  ##   "tickets_solved_this_week":  16,
  ##   "tickets_rated_good_this_week":  2,
  ##   "tickets_rated_bad_this_week":  0,
  ##   "sat_score_last_60_days": 98,
  ##   "account_sat_score_last_60_days":  99
  ## }
  ## ```
  def model_json(user)
    json = {
      open_tickets: user_open_ticket(user),
      new_and_open_tickets_groups: user.account.has_groups? ? new_and_open_tickets_in_group_with_restrictions(user) : nil,
      tickets_solved_this_week: agent_tickets_solved(user),
      tickets_rated_good_this_week: nil,
      tickets_rated_bad_this_week: nil,
      sat_score_last_60_days: nil,
      account_sat_score_last_60_days: nil
    }

    if user.account.has_customer_satisfaction_enabled? && user.account.settings.show_csat_on_dashboard
      json.merge!(
        tickets_rated_good_this_week: agent_tickets_received_good_rating(user),
        tickets_rated_bad_this_week: agent_tickets_received_bad_rating(user),
        sat_score_last_60_days: user.satisfaction_score(:received),
        account_sat_score_last_60_days: user.account.satisfaction_score
      )
    end

    json
  end
end
