class Api::V2::Stats::BenchmarkPresenter < Api::V2::Presenter
  self.model_key = :benchmark

  def initialize(user, options = {})
    @category = options[:category]
    @type = options[:type]
    super
  end

  # ### JSON Format
  # Benchmarks are represented as JSON objects which have the following keys:
  #
  # | Name                | Type    | Comment
  # | ------------------- | ------- | -------
  # | category            | string  | The benchmark category
  # | type                | string  | The type of category
  # | csr                 | number  | The percentage of positive satisfaction ratings, e.g. 0.96 means 96%
  # | first_response_time | integer | The average first response time in seconds
  # | created_count       | integer | The number of accounts in the particular benchmark
  #
  # #### Example
  # ```js
  # {
  #   "category": "industry",
  #   "type":     "software",
  #   "data:": {
  #     "csr":                 0.95,
  #     "first_response_time": 108000,
  #     "created_count":       777,
  #   }
  # }
  # ```

  def model_json(benchmarking_stats)
    data = if @category == 'overall'
      benchmarking_stats[@category]
    else
      benchmarking_stats[@category][@type] ||
        (raise Zendesk::UnknownAttributeError, "Unknown category type")
    end

    {}.tap do |json|
      json[:category] = @category
      json[:type] = @type || 'average'
      json[:data] = data
    end
  end
end
