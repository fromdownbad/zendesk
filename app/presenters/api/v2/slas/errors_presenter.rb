class Api::V2::Slas::ErrorsPresenter < Api::V2::ErrorsPresenter
  FIELD_MAPPING = {
    metric_id: :metric,
    priority_id: :priority
  }.freeze

  def present(record)
    json = { error: "RecordInvalid", description: "Record validation errors", details: {} }
    error_messages(record) do |attribute, message|
      if record.class.reflect_on_association(attribute)
        present_association_errors(record, attribute, json)
      else
        present_error(record, attribute, message, json)
      end
    end
    json
  end

  private

  def present_association_errors(record, attribute, json)
    record.errors.get(attribute).try(:clear)
    record.send(attribute).reject(&:valid?).each do |a|
      json[:details][attribute] ||= []
      json[:details][attribute] << present(a)
    end
  end

  def present_error(record, attribute, message, json)
    description = prefix_attribute_name_to_message(record, attribute, message)
    details     = { description: description }

    details[:error] = message.error if message.respond_to?(:error)
    attribute       = FIELD_MAPPING.fetch(attribute) { attribute }
    json[:details][attribute] ||= []
    json[:details][attribute] << details
  end
end
