class Api::V2::Slas::PolicyFilterItemPresenter < Api::V2::Presenter
  def model_json(policy_filter_item)
    # NOTE: PolicyFilterItem objects don't have individual URLs at this time,
    # so we do not call super here.
    # TODO: map output with Api::V2::Tickets::AttributeMappings (after it is
    # fixed)
    {
      field: policy_filter_item.source,
      operator: policy_filter_item.operator,
      value: policy_filter_item.first_value
    }
  end
end
