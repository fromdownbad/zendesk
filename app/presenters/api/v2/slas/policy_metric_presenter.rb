class Api::V2::Slas::PolicyMetricPresenter < Api::V2::Presenter
  def model_json(policy_metric)
    # NOTE: PolicyMetric objects don't have individual URLs at this time, so
    # we do not call super here.
    {
      priority:       policy_metric.priority.name.downcase,
      metric:         policy_metric.metric.name,
      target:         policy_metric.target,
      business_hours: policy_metric.business_hours
    }
  end
end
