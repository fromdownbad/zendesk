class Api::V2::Slas::TicketMetricPresenter < Api::V2::Presenter
  def model_json(ticket_metric)
    # NOTE: PolicyMetric objects don't have individual URLs at this time, so
    # we do not call super here.
    {
      breach_at: ticket_metric.breach_at,
      stage:     ticket_metric.stage.name,
      metric:    ticket_metric.name
    }.tap do |result|
      if ticket_metric.breach_at.present?
        result.merge!(relative_time(ticket_metric.breach_at))
      end
    end
  end

  private

  def relative_time(time)
    diff_minutes = ((time - Time.now) / 60.0).round
    abs_minutes  = diff_minutes.abs

    if abs_minutes <= 90
      # 90 minutes or less- render in minutes
      { minutes: diff_minutes }
    elsif abs_minutes <= 30 + (36 * 60)
      # > than 90 minutes, but <= a day and a half plus thirty minutes- render in hours
      { hours: (diff_minutes / 60.0).round }
    else
      # More than a day and a half plus thirty minutes- render in days
      { days: (diff_minutes / (60.0 * 24.0)).round }
    end
  end
end
