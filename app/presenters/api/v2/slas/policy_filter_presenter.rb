class Api::V2::Slas::PolicyFilterPresenter < Api::V2::Presenter
  def model_json(policy_filter)
    # NOTE: PolicyFilter objects don't have individual URLs at this time, so
    # we do not call super here.
    {
      all: policy_filter.conditions_all.map { |item| policy_filter_item_presenter.model_json(item) },
      any: policy_filter.conditions_any.map { |item| policy_filter_item_presenter.model_json(item) }
    }
  end

  private

  def policy_filter_item_presenter
    @policy_filter_item_presenter ||= Api::V2::Slas::PolicyFilterItemPresenter.new(user, options)
  end
end
