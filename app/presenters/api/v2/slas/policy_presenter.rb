class Api::V2::Slas::PolicyPresenter < Api::V2::Presenter
  self.model_key = :sla_policy

  ## ### JSON Format
  ## SLA Policies are represented as simple flat JSON objects which have the following keys:
  ##
  ## | Name            | Type                       | Comment
  ## | --------------- | ---------------------------| -------------------
  ## | id              | integer                    | Automatically assigned when created
  ## | title           | string                     | The title of the SLA policy
  ## | description     | string                     | The description of the SLA policy
  ## | position        | integer                    | Position of the SLA policy, determines the order they will be matched. If not specified, SLA Policy is added as the last position
  ## | filter          | [Filter](#filter)          | An object that describes the conditions that a ticket must match in order for an SLA policy to be applied to that ticket
  ## | policy_metrics  | array                      | Array of [Policy Metric](#policy-metric) objects.
  ## | created_at      | date                       | The time the SLA policy was created
  ## | updated_at      | date                       | The time of the last update of the SLA policy
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "url": "https://company.zendesk.com/api/v2/slas/policies/25.json",
  ##   "id": 25,
  ##   "title": "Silver Plan",
  ##   "description": "Organizations: Silver Plan",
  ##   "position": 3,
  ##   "filter": { ... },
  ##   "policy_metrics": [ ... ],
  ##   "updated_at": "2015-03-17T22:50:26Z",
  ##   "created_at": "2015-03-17T22:50:26Z"
  ## }
  ## ```
  ##
  ## ### Policy Metric
  ##
  ## An object that describes the metric targets for each value of the priority field.
  ##
  ## Policy Metrics are represented as simple flat JSON objects which have the following keys:
  ##
  ## | Name            | Type                       | Comment
  ## | --------------- | ---------------------------| -------------------
  ## | priority        | string                     | Priority that a ticket must match
  ## | metric          | [Metric](#metrics)         | The definition of the time that is being measured
  ## | target          | integer                    | The time within which the end-state for a metric should be met
  ## | business_hours  | boolean                    | Whether the metric targets are being measured in business hours or calendar hours
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "priority": "low",
  ##   "metric": "first_reply_time",
  ##   "target": 60,
  ##   "business_hours": false
  ## }
  ## ```
  ##
  ## #### Metrics
  ##
  ## | Metric                   | Value                  |
  ## | ------------------------ | ---------------------- |
  ## | Agent Work Time          | agent_work_time        |
  ## | First Reply Time         | first_reply_time       |
  ## | Next Reply Time          | next_reply_time        |
  ## | Pausable Update Time     | pausable_update_time   |
  ## | Periodic Update Time     | periodic_update_time   |
  ## | Requester Wait Time      | requester_wait_time    |
  ##
  def model_json(policy)
    policy_metrics = policy.policy_metrics.map do |metric|
      policy_metric_presenter.model_json(metric)
    end

    super.merge(
      id:             policy.id,
      title:          policy.title,
      description:    policy.description,
      position:       policy.position,
      filter:         policy_filter_presenter.model_json(policy.filter),
      policy_metrics: policy_metrics,
      created_at:     policy.created_at,
      updated_at:     policy.updated_at
    )
  end

  def url(conversion)
    url_builder.send(:api_v2_slas_policy_url, conversion, format: :json)
  end

  def present_errors(record)
    Api::V2::Slas::ErrorsPresenter.new.present(record)
  end

  def side_loads(model_or_collection)
    super.tap do |json|
      if side_load?(:organizations)
        ids = map_conditions_organization_ids(model_or_collection)

        json[:organizations] = map_organizations(ids)
      end
    end
  end

  private

  def policy_metric_presenter
    @policy_metric_presenter ||= Api::V2::Slas::PolicyMetricPresenter.new(user, options)
  end

  def policy_filter_presenter
    @policy_filter_presenter ||= Api::V2::Slas::PolicyFilterPresenter.new(user, options)
  end

  def map_conditions_organization_ids(model_or_collection)
    key = 'organization_id'
    Array(model_or_collection).flat_map do |policy|
      conditions = policy.filter.conditions_all + policy.filter.conditions_any
      conditions.select { |cond| cond.source == key }.map(&:first_value)
    end.compact
  end

  def map_organizations(ids)
    account.organizations.where(id: ids).map do |o|
      o.attributes.slice('id', 'name')
    end
  end
end
