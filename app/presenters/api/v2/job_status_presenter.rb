class Api::V2::JobStatusPresenter < Api::V2::Presenter
  self.model_key = :job_status
  self.url_param = :uuid

  ## ### JSON Format
  ## Job statuses have the following attributes.
  ##
  ## | Name            | Type                             | Read-only | Mandatory | Comment
  ## | --------------- | ---------------------------------| --------- | --------- | -------
  ## | id              | string                           | yes       | no        | Automatically assigned when the job is queued
  ## | url             | string                           | yes       | no        | The URL to poll for status updates
  ## | total           | integer                          | yes       | no        | The total number of tasks this job is batching through
  ## | progress        | integer                          | yes       | no        | Number of tasks that have already been completed
  ## | status          | string                           | yes       | no        | The current status. One of the following: "queued", "working", "failed", "completed", "killed"
  ## | message         | string                           | yes       | no        | Message from the job worker, if any
  ## | results         | array                            | yes       | no        | Result data from processed tasks. See [Results](#results) below
  ##
  ## #### Results
  ##
  ## The "results" array in a response lists the resources that were successfully and unsuccessfully updated or created after processing.
  ##
  ## The results differ depending on the type of job. If the job was to bulk create resources, each result specifies the following:
  ##
  ## - the id of the new resource (`"id": 245`)
  ## - the index number of the result (`"index": 1`)
  ##
  ## If the job was to bulk update resources, each result specifies the following:
  ##
  ## - the id of the resource the job attempted to update (`"id": 255`)
  ## - the action the job attempted (`"action": "update"`)
  ## - whether the action was successful or not (`"success": true`)
  ## - the status (`"status": "Updated"`)
  ##
  ## #### Examples
  ##
  ## **Bulk update**
  ##
  ## ```json
  ## {
  ##   "job_status": {
  ##     "id": "82de0b044094f0c67893ac9fe64f1a99",
  ##     "url": "https://example.zendesk.com/api/v2/job_statuses/82de0b0467893ac9fe64f1a99.json",
  ##     "total": 2,
  ##     "progress": 2,
  ##     "status": "completed",
  ##     "message": "Completed at 2018-03-08 10:07:04 +0000",
  ##     "results": [
  ##       {
  ##         "id": 244,
  ##         "action": "update",
  ##         "success": true,
  ##         "status": "Updated"
  ##       },
  ##       {
  ##         "id": 245,
  ##         "action": "update",
  ##         "success": true,
  ##         "status": "Updated"
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  ##
  ## **Bulk create**
  ##
  ## ```json
  ## {
  ##   "job_status": {
  ##     "id": "dd9321f29967688b27bc9499ebb4ae8d",
  ##     "url": "https://example.zendesk.com/api/v2/job_statuses/dd9321f299676c9499ebb4ae8d.json",
  ##     "total": 2,
  ##     "progress": 2,
  ##     "status": "completed",
  ##     "message": "Completed at 2018-03-08 06:07:49 +0000",
  ##     "results": [
  ##       {
  ##         "id": 244,
  ##         "index": 0
  ##       },
  ##       {
  ##         "id": 245,
  ##         "index": 1
  ##       }
  ##     ]
  ##   }
  ## }
  ## ```
  def model_json(job_status)
    {
      id: job_status.uuid,
      url: url(job_status),
      total: job_status.total,
      progress: job_status.num,
      status: job_status.status,
      message: job_status.message,
      results: job_status["results"]
    }
  end
end
