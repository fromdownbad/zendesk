class Api::V2::TicketFieldConditionPresenter < Api::V2::Presenter
  self.model_key = :ticket_field_condition

  def model_json(ticket_field_condition)
    super.merge!(
      id: ticket_field_condition.id,
      ticket_form_id: ticket_field_condition.ticket_form_id,
      parent_field_id: ticket_field_condition.parent_field_id,
      child_field_id: ticket_field_condition.child_field_id,
      value: ticket_field_condition.value,
      user_type: ticket_field_condition.user_type.to_s,
      created_at: ticket_field_condition.created_at,
      updated_at: ticket_field_condition.updated_at
    )
  end

  # Do we want any sideloads here? Currently already planning sideloading conditions on a ticket form
  # and also on ticket field presenter, which I believe will be most used unless we're directly manipulating the object
end
