module Api
  module V2
    class JetpackTaskPresenter < Presenter
      self.model_key = :jetpack_task

      def model_json(model)
        {
          section: model.section,
          status: model.status,
          enabled: model.enabled,
          key: model.key
        }
      end
    end
  end
end
