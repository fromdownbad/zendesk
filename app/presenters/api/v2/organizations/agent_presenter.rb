class Api::V2::Organizations::AgentPresenter < Api::V2::Organizations::EndUserPresenter
  include Zendesk::Export::FieldsExport
  self.model_key = :organization

  def initialize(actor, options)
    options[:organization_url_builder] ||= options[:url_builder]
    super(actor, options)
    @custom_fields = actor.account.custom_fields_for_owner("Organization")
    @exclude_non_exportable_fields = options[:exclude_non_exportable_fields] || false
  end

  ## ### JSON Format
  ## Organizations are represented as simple flat JSON objects with the following keys:
  ##
  ## | Name                | Type    | Read-only | Mandatory | Comment
  ## | ---------------     | ------- | --------- | --------- | -------
  ## | id                  | integer | yes       | no        | Automatically assigned when the organization is created
  ## | url                 | string  | yes       | no        | The API url of this organization
  ## | external_id         | string  | no        | no        | A unique external id to associate organizations to an external record
  ## | name                | string  | no        | yes       | A unique name for the organization
  ## | created_at          | date    | yes       | no        | The time the organization was created
  ## | updated_at          | date    | yes       | no        | The time of the last update of the organization
  ## | domain_names        | array   | no        | no        | An array of domain names associated with this organization
  ## | details             | string  | no        | no        | Any details obout the organization, such as the address
  ## | notes               | string  | no        | no        | Any notes you have about the organization
  ## | group_id            | integer | no        | no        | New tickets from users in this organization are automatically put in this group
  ## | shared_tickets      | boolean | no        | no        | End users in this organization are able to see each other's tickets
  ## | shared_comments     | boolean | no        | no        | End users in this organization are able to see each other's comments on tickets
  ## | tags                | array   | no        | no        | The tags of the organization
  ## | organization_fields | hash    | no        | no        | Custom fields for this organization
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                  35436,
  ##   "external_id":         "ABC123",
  ##   "url":                 "https://company.zendesk.com/api/v2/organizations/35436.json",
  ##   "name":                "One Organization",
  ##   "created_at":          "2009-07-20T22:55:29Z",
  ##   "updated_at":          "2011-05-05T10:38:52Z",
  ##   "domain_names":        ["example.com", "test.com"],
  ##   "details":             "This is a kind of organization",
  ##   "notes":               "",
  ##   "group_id":            null,
  ##   "shared_tickets":      true,
  ##   "shared_comments":     true,
  ##   "tags":                ["enterprise", "other_tag"],
  ##   "organization_fields": {
  ##     "org_dropdown": "option_1",
  ##     "org_decimal": 5.2,
  ##   }
  ## }
  ## ```
  def model_json(organization)
    json = super.merge!(
      external_id:  organization.external_id,
      created_at:   organization.created_at,
      updated_at:   organization.updated_at,
      domain_names: organization.domain_names,
      details:      organization.details,
      notes:        organization.notes,
      group_id:     organization.group_id,
      tags:         account.has_user_and_organization_tags? ? tags(organization) : []
    )

    if account.has_user_and_organization_fields?
      organization_fields = organization.custom_field_values.as_json(custom_fields: @custom_fields, account: account)
      json[:organization_fields] = fields_for_export(organization_fields, @exclude_non_exportable_fields)
    else
      json[:organization_fields] = {}
    end

    if side_load?(:abilities)
      json[:abilities] = organization_abilities_presenter.model_json(organization)
    end

    json
  end

  def side_loads(organizations)
    organizations = Array(organizations)
    json = {}

    if side_load?(:abilities)
      json[:abilities] = organization_abilities_presenter.collection_presenter.model_json(organizations)
    end

    json
  end

  def association_preloads
    includes = {
      custom_field_values: {
        field: {
          dropdown_choices: {}
        }
      }
    }

    includes[:taggings] = {} if account.has_user_and_organization_tags?
    includes[:organization_domains] = {}
    includes[:organization_emails] = {}

    includes
  end

  def url(organization)
    # url builder significantly slower than string concatenation
    # when called 1000s of times.
    @cached_url_path_part ||= options[:organization_url_builder].send(:api_v2_organization_url, organization, format: :json).to_s.gsub(/\d+.json$/, '')

    @cached_url_path_part.clone << organization.id.to_s << ".json"
  end

  protected

  def tags(organization)
    account.has_user_and_organization_tags? ? organization.taggings.map(&:tag_name) : []
  end

  def organization_abilities_presenter
    @organization_abilities_presenter ||=
      options[:organization_abilities_presenter] ||=
        Api::V2::Abilities::OrganizationPresenter.new(user, options)
  end
end
