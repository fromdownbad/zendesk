class Api::V2::Organizations::EndUserPresenter < Api::V2::Presenter
  self.model_key = :organization

  def model_json(organization)
    super.merge!(
      id:              organization.id,
      name:            organization.name,
      shared_tickets:  organization.shared_tickets?,
      shared_comments: organization.shared_comments?
    )
  end
end
