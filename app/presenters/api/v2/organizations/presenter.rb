## ### JSON Format
## Users are presented via the Api::V2::Organizations::AgentPresenter or Api::V2::Organizations::EndUserPresenter
## ```
class Api::V2::Organizations::Presenter < Api::V2::Presenter
  def self.new(user, options)
    klass = if user.is_end_user?
      Api::V2::Organizations::EndUserPresenter
    else
      Api::V2::Organizations::AgentPresenter
    end
    klass.new(user, options)
  end
end
