class Api::V2::Organizations::IncrementalPresenter < Api::V2::Organizations::AgentPresenter
  self.model_key = :organizations
  def model_json(organization)
    super.merge(deleted_at: organization.deleted_at)
  end
end
