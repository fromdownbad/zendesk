class Api::V2::BookmarkPresenter < Api::V2::Presenter
  self.model_key = :bookmark

  ## ### JSON Format
  ## Bookmarks have the below attributes
  ##
  ## | Name            | Type                               | Read-only | Mandatory | Comment
  ## | --------------- | -----------------------------------| --------- | --------- | -------
  ## | id              | integer                            | yes       | no        | Automatically assigned upon creation
  ## | url             | string                             | yes       | no        | The API url of this bookmark
  ## | created_at      | date                               | yes       | no        | The time the record was created
  ## | ticket          | [Ticket](tickets.html#json-format) | yes       | yes       | The bookmarked ticket
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         88335,
  ##   "url":        "https://company.zendesk.com/api/v2/bookmarks/88335.json",
  ##   "created_at": "2012-02-20T22:55:29Z",
  ##   "ticket": {
  ##     "id":      123,
  ##     "subject": "Printer on fire",
  ##     ... // see the [Ticket documentation](tickets.html#json-format) for an example of a full Ticket object
  ##   }
  ## }
  ## ```
  def model_json(bookmark)
    super.merge!(
      id: bookmark.id,
      created_at: bookmark.created_at,
      ticket: (bookmark.ticket && ticket_presenter.model_json(bookmark.ticket))
    )
  end

  def association_preloads
    { ticket: ticket_presenter.association_preloads }
  end

  def side_loads(bookmarks)
    ticket_presenter.side_loads(Array(bookmarks).map(&:ticket))
  end

  def ticket_presenter
    @ticket_presenter ||= Api::V2::Tickets::TicketPresenter.new(user, options.merge(includes: includes))
  end
end
