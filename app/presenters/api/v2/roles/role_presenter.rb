class Api::V2::Roles::RolePresenter < Api::V2::Presenter
  ## ### JSON Format
  ## Custom roles have the below attributes
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | name            | integer | yes       | yes       | Name of this custom role
  ## | description     | integer | yes       | no        | A description of the role
  #  | configuration   | hash    | yes       | no        | A detailed configuration of the role
  ## | created_at      | date    | yes       | no        | The time the record was created
  ## | updated_at      | date    | yes       | no        | The time the record was last updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":           35436,
  ##   "name":         "Partner",
  ##   "description":  "Can only make private comments on assigned tickets",
  ##   "created_at":   "2012-02-20T22:55:29Z",
  ##   "updated_at":   "2012-02-20T22:55:29Z"
  ##   "configuration": {
  ##     "chat_access":                     true,
  ##     "end_user_profile":                "readonly",
  ##     "explore_access":                  "edit",
  ##     "forum_access":                    "readonly",
  ##     "forum_access_restricted_content": false,
  ##     "light_agent":                     "false",
  ##     "macro_access":                    "full",
  ##     "manage_business_rules":           true,
  ##     "manage_dynamic_content":          false,
  ##     "manage_extensions_and_channels":  true,
  ##     "manage_facebook":                 false,
  ##     "organization_editing":            false,
  ##     "organization_notes_editing"       false,
  ##     "report_access":                   "none",
  ##     "ticket_access":                   "within-groups",
  ##     "ticket_comment_access":           "none",
  ##     "ticket_deletion":                 false,
  ##     "view_deleted_tickets":            false,
  ##     "ticket_editing":                  true,
  ##     "ticket_merge":                    false,
  ##     "ticket_tag_editing":              true,
  ##     "twitter_search_access":           true,
  ##     "view_access":                     "full",
  ##     "voice_access":                    true
  ##   },
  ## }
  ## ```

  def configuration(user)
    user_configuration = {
      chat_access:                     user.can?(:accept, ::Access::Chat::IncomingChats),
      manage_business_rules:           user.can?(:edit, ::Access::Settings::BusinessRules),
      manage_dynamic_content:          user.can?(:manage, Cms::Text),
      manage_extensions_and_channels:  user.can?(:edit, ::Access::Settings::Extensions),
      manage_facebook:                 user.can?(:manage, Facebook::Page),
      organization_editing:            user.can?(:edit, Organization),
      organization_notes_editing:      user.can?(:edit_notes, Organization),
      ticket_deletion:                 user.can?(:delete, Ticket),
      view_deleted_tickets:            user.can?(:view_deleted, Ticket),
      ticket_tag_editing:              user.can?(:edit_tags, Ticket),
      twitter_search_access:           user.can?(:view_twitter_saved_search, Channels::Twitter::Search),
      forum_access_restricted_content: user.can?(:access_restricted_content, Forum),
      end_user_list_access:            user.can?(:list, User) ? "full" : "none",
      ticket_access:                   user.ticket_access,
      ticket_comment_access:           comment_access(user),
      voice_access:                    user.can?(:accept, Voice::Call), # remove when abilities.can_use_voice_console is used in production Lotus
      moderate_forums:                 false,
      group_access:                    user.can?(:edit, Group),
      light_agent:                     user.is_light_agent?,
      side_conversation_create:        user.can?(:create_side_conversation, Ticket)
    }

    user_configuration[:ticket_bulk_edit] = user.can?(:ticket_bulk_edit, Ticket) if user.account.has_present_ticket_bulk_edit_permission?

    user_configuration.merge!(permissions(user))
  end

  private

  def comment_access(user)
    if user.can?(:publicly, Comment)
      "public"
    else
      "none"
    end
  end
end
