class Api::V2::Roles::CustomRolePresenter < Api::V2::Roles::RolePresenter
  self.model_key = :custom_role

  ## ### JSON Format
  ## Custom roles have the following properties.
  ##
  ## | Name            | Type                             | Read-only | Mandatory | Comment
  ## | --------------- | -------------------------------- | --------- | --------- | -------
  ## | id              | integer                          | yes       | no        | Automatically assigned on creation
  ## | name            | string                           | no        | yes       | Name of the custom role
  ## | description     | string                           | no        | no        | A description of the role
  ## | role_type       | integer                          | yes       | yes       | The value 0
  ## | configuration   | object                           | no        | no        | Configuration settings for the role. See [Configuration](#configuration)
  ## | created_at      | date                             | yes       | no        | The time the record was created
  ## | updated_at      | date                             | yes       | no        | The time the record was last updated
  ##
  ## #### Configuration
  ## The `configuration` object has the following properties, which are all optional.
  ##
  ## | Name                             | Type    | Read-only | Comment
  ## | -------------------------------- | ------- | --------- | -------
  ## | chat_access                      | boolean | yes       | Whether or not the agent has access to Chat
  ## | end_user_list_access             | string  | no        | Whether or not the agent can view lists of user profiles. Allowed values: "full", "none"
  ## | end_user_profile_access          | string  | no        | What the agent can do with end-user profiles. Allowed values: "edit", "edit-within-org", "full", "readonly"
  ## | explore_access                   | string  | no        | Allowed values: "edit", "full", "none", "readonly"
  ## | forum_access                     | string  | no        | The kind of access the agent has to Guide. Allowed values: "edit-topics", "full", "readonly"
  ## | forum_access_restricted_content  | boolean | no        |
  ## | group_access                     | boolean | yes       | Whether or not the agent can add or modify groups
  ## | light_agent                      | boolean | yes       |
  ## | macro_access                     | string  | no        | What the agent can do with macros. Allowed values: "full", "manage-group", "manage-personal", "readonly"
  ## | manage_business_rules            | boolean | no        | Whether or not the agent can manage business rules
  ## | manage_dynamic_content           | boolean | no        | Whether or not the agent can access dynamic content
  ## | manage_extensions_and_channels   | boolean | no        | Whether or not the agent can manage channels and extensions
  ## | manage_facebook                  | boolean | no        | Whether or not the agent can manage facebook pages
  ## | moderate_forums                  | boolean | yes       |
  ## | organization_editing             | boolean | no        | Whether or not the agent can add or modify organizations
  ## | organization_notes_editing       | boolean | yes       | Whether or not the agent can add or modify organization notes
  ## | report_access                    | string  | no        | What the agent can do with reports. Allowed values: "full", "none", "readonly"
  ## | ticket_access                    | string  | no        | What kind of tickets the agent can access. Allowed values: "all", "assigned-only", "within-groups", "within-organization"
  ## | ticket_comment_access            | string  | no        | What type of comments the agent can make. Allowed values: "public", "none"
  ## | ticket_deletion                  | boolean | no        | Whether or not the agent can delete tickets
  ## | ticket_editing                   | boolean | no        | Whether or not the agent can edit ticket properties
  ## | ticket_merge                     | boolean | no        | Whether or not the agent can merge tickets
  ## | ticket_tag_editing               | boolean | no        | Whether or not the agent can edit ticket tags
  ## | twitter_search_access            | boolean | no        |
  ## | user_view_access                 | string  | no        | What the agent can do with customer lists. Allowed values: "full", "manage-group", "manage-personal", "none", "readonly"
  ## | view_access                      | string  | no        | What the agent can do with views. Allowed values: "full", "manage-group", "manage-personal", "playonly", "readonly"
  ## | view_deleted_tickets             | boolean | no        | Whether or not the agent can view deleted tickets
  ## | voice_access                     | boolean | no        | Whether or not the agent has access to Talk
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":           35436,
  ##   "name":         "Partner",
  ##   "description":  "Can only make private comments on assigned tickets",
  ##   "created_at":   "2012-02-20T22:55:29Z",
  ##   "updated_at":   "2012-02-20T22:55:29Z",
  ##   "configuration": {
  ##     "chat_access":                     true,
  ##     "end_user_profile":                "readonly",
  ##     "explore_access":                  "edit",
  ##     "forum_access":                    "readonly",
  ##     "forum_access_restricted_content": false,
  ##     "light_agent":                     "false",
  ##     "macro_access":                    "full",
  ##     "manage_business_rules":           true,
  ##     "manage_dynamic_content":          false,
  ##     "manage_extensions_and_channels":  true,
  ##     "manage_facebook":                 false,
  ##     "organization_editing":            false,
  ##     "organization_notes_editing"       false,
  ##     "report_access":                   "none",
  ##     "ticket_access":                   "within-groups",
  ##     "ticket_comment_access":           "none",
  ##     "ticket_deletion":                 false,
  ##     "view_deleted_tickets":            false,
  ##     "ticket_editing":                  true,
  ##     "ticket_merge":                    false,
  ##     "ticket_tag_editing":              true,
  ##     "twitter_search_access":           true,
  ##     "view_access":                     "full",
  ##     "voice_access":                    true
  ##   }
  ## }
  ## ```
  def model_json(permission_set)
    user = User.new
    user.account = account
    user.roles = permission_set.legacy_role_type.id
    user.permission_set = permission_set

    {
      id:            user.permission_set.id,
      name:          user.permission_set.translated_name,
      description:   user.permission_set.translated_description,
      role_type:     user.permission_set.role_type,
      created_at:    user.permission_set.created_at,
      updated_at:    updated_at(user.permission_set),
      configuration: configuration(user)
    }
  end

  def association_preloads
    { permissions: {} }
  end

  protected

  def permissions(user)
    permissions = user.permission_set.permissions

    {
      end_user_profile_access: permissions.end_user_profile,              # ["full", "edit", "edit-within-org", "readonly"]
      explore_access:          permissions.explore_access,                # ["edit", "full", "none", "readonly"]
      forum_access:            permissions.forum_access,                  # ["full", "edit-topics", "readonly"],
      macro_access:            permissions.macro_access,                  # ["full", "manage-group", "manage-personal", "readonly"]
      report_access:           permissions.report_access,                 # ["full", "manage-group", "manage-personal", "readonly"],
      ticket_editing:          permissions.ticket_editing?,
      ticket_merge:            permissions.ticket_merge?,
      view_access:             permissions.view_access,                   # ["full", "manage-group", "manage-personal", "readonly"],
      user_view_access:        permissions.user_view_access               # ["full", "manage-group", "manage-personal", "readonly", "none"],
    }
  end

  def updated_at(permission_set)
    [permission_set.updated_at, *permission_set.permissions.map(&:updated_at)].compact.max
  end
end
