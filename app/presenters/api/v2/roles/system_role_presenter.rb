class Api::V2::Roles::SystemRolePresenter < Api::V2::Roles::RolePresenter
  def model_json(role)
    restriction = user.try(:restriction_id)
    user = User.new
    user.account = account
    user.roles = role
    user.restriction_id = restriction

    {
      id:            nil,
      description:   nil,
      created_at:    nil,
      updated_at:    nil,
      name:          user.role.downcase,
      configuration: configuration(user)
    }
  end

  protected

  def user_view_access(user)
    return "none" if user.is_restricted_agent?
    user.is_admin? ? "full" : "manage-personal"
  end

  def permissions(user)
    {
      end_user_profile_access: user.is_non_restricted_agent? ? "full" : "readonly", # abilities/user.rb:61
      forum_access:            user.is_admin? ? "full" : "readonly", # abilities/forum.rb:95
      macro_access:            user.is_admin? ? "full" : "manage-personal", # abilities/rule.rb:12
      report_access:           user.can?(:manage, Report) ? "full" : "readonly",
      ticket_editing:          true,
      ticket_merge:            user.is_agent?,
      view_access:             user.is_admin? ? "full" : "manage-personal", # abilities/rule.rb:12
      user_view_access:        user_view_access(user)
    }
  end
end
