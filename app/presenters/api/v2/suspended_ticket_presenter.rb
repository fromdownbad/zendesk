class Api::V2::SuspendedTicketPresenter < Api::V2::Presenter
  MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth
  SANITIZE_PARSER_OPTIONS = { max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH }.freeze

  include ActionView::Helpers::SanitizeHelper
  include Api::V2::Tickets::EventViaPresenter

  self.model_key = :suspended_ticket
  self.url_param = :id

  ##
  ## | Name             | Type                   | Read-only | Mandatory | Comment
  ## | ---------------- | ---------------------- | --------- | --------- | -------
  ## | id               | integer                | yes       | no        | Automatically assigned
  ## | url              | string                 | yes       | no        | The API url of this ticket
  ## | author           | object                 | yes       | no        | The author id (if available), name and email
  ## | subject          | string                 | yes       | no        | The value of the subject field for this ticket
  ## | content          | string                 | yes       | no        | The content that was flagged
  ## | cause            | string                 | yes       | no        | Why the ticket was suspended
  ## | message_id       | string                 | yes       | no        | The ID of the email, if available
  ## | ticket_id        | integer                | yes       | no        | The ticket ID this suspended email is associated with, if available
  ## | recipient        | string                 | yes       | no        | The original recipient e-mail address of the ticket
  ## | created_at       | date                   | yes       | no        | When this record was created
  ## | updated_at       | date                   | yes       | no        | When this record last got updated
  ## | via              | [Via](#the-via-object) | yes       | no        | This object explains how the ticket was created
  ## | brand_id         | integer                | yes       | no        | The id of the brand this ticket is associated with - only applicable for enterprise accounts
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":               435,
  ##   "url":              "https://example.zendesk.com/api/v2/tickets/35436.json",
  ##   "author":           { "id": 1, "name": "Mr. Roboto", "email": "styx@example.com" },
  ##   "subject":          "Help, my printer is on fire!",
  ##   "content":          "Out Of Office Reply",
  ##   "cause":            "Detected as spam",
  ##   "ticket_id":        67321,
  ##   "recipient":        "john@example.com",
  ##   "created_at":       "2009-07-20T22:55:29Z",
  ##   "updated_at":       "2011-05-05T10:38:52Z",
  ##   "brand_id":         123,
  ##   "via": {
  ##     "channel": "web"
  ##   }
  ## }
  ## ```

  def model_json(ticket)
    author = { id: ticket.author_id }
    if ticket.author
      author[:name] = ticket.author.name
      author[:email] = ticket.author.email
    else
      author[:name] = ticket.from_name
      author[:email] = ticket.from_mail
    end

    json = super.merge!(
      id: ticket.id,
      author: author,
      subject: ticket_subject(ticket),
      content: ticket_content(ticket),
      cause: SuspensionType[ticket.cause].try(:localized_name),
      cause_id: ticket.cause,
      error_messages: ticket.get_error_messages,
      message_id: ticket.message_id,
      ticket_id: ticket.ticket_id,
      created_at: ticket.created_at,
      updated_at: ticket.updated_at,
      via: serialized_via_object(ticket),
      attachments: attachment_presenter.collection_presenter.model_json(ticket.attachments),
      recipient: ticket.original_recipient_address || ticket.recipient
    )

    json[:brand_id] = ticket.properties[:brand_id] if account.has_multibrand? && ticket.properties.present?

    json
  end

  private

  def ticket_subject(ticket)
    Sanitize.fragment(ticket.subject || ticket.content, parser_options: SANITIZE_PARSER_OPTIONS)
  rescue SystemStackError
    Rails.logger.warn("Subject could not be parsed for ticket with id #{ticket.id} for account id #{account.id}")
    ''
  end

  def ticket_content(ticket)
    Sanitize.fragment(ticket.content, parser_options: SANITIZE_PARSER_OPTIONS).presence
  rescue SystemStackError
    Rails.logger.warn("Content could not be parsed for ticket with id #{ticket.id} for account id #{account.id}")
    ''
  end
end
