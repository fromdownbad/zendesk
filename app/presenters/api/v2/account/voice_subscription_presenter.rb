class Api::V2::Account::VoiceSubscriptionPresenter < Api::V2::Presenter
  self.model_key = :voice_subscription

  ## ### JSON Format
  ## Voice Subscriptions are represented as JSON objects which have the following keys:
  ##
  ## | Name                     | Type    | Read-only | Mandatory | Comment
  ## | -------------------------| ------- | --------- | --------- | -------
  ## | status                   | string  | yes       | yes       | off, customer, canceled or suspended
  ## | max_agents               | integer | yes       | no        | The number of allowed agents or unlimited if nil
  ## | plan_type                | integer | yes       | yes       | The type of voice plan
  ## | plan_name                | string  | yes       | yes       | The plan name
  ## | is_trial                 | boolean | yes       | yes       | Indicates if customer is trialing voice
  ## ```js
  ## {
  ##   "status":     "customer",
  ##   "max_agents": 5,
  ##   "plan_type":  1,
  ##   "plan_name":  "Basic",
  ##   "is_active":  true
  ## }
  ## ```

  def model_json(voice_subscription)
    {}.tap do |json|
      json.merge!(
        status:     voice_subscription.status,
        max_agents: voice_subscription.max_agents,
        plan_type:  voice_subscription.plan_type,
        plan_name:  voice_subscription.plan_name,
        is_active:  voice_subscription.is_active?
      ) unless voice_subscription.blank?
    end
  end
end
