class Api::V2::Account::KnowledgeSettingsPresenter < Api::V2::Presenter
  self.model_key = :settings

  def model_json(account)
    { features: present_with(FeaturesPresenter, account) }
  end

  class FeaturesPresenter < Api::V2::Presenter
    def model_json(account)
      {
        can_manage_lists: account.has_knowledge_bank_list_management?,
        has_labels: account.has_article_labels?
      }
    end
  end
end
