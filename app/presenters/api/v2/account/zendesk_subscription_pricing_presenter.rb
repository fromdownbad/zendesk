class Api::V2::Account::ZendeskSubscriptionPricingPresenter < Api::V2::Presenter
  self.model_key = :pricing

  def model_json(subscription_pricing)
    {
      version:                 subscription_pricing.pricing_model_revision,
      is_multiproduct_account: subscription_pricing.billing_multi_product_participant?, # remove when all accounts move to SOBA billilng

      plan:               {
        id:   subscription_pricing.plan_type,
        name: subscription_pricing.plan_name
      },

      available_plans:    subscription_pricing.available_plans,

      billing_cycle:      {
        id:       subscription_pricing.billing_cycle_type,
        name:     subscription_pricing.billing_cycle_name,
        discount: subscription_pricing.billing_cycle_discount_no_coupon
      },

      voice:              {
        active: subscription_pricing.voice_active?
      },

      promo:              {
        code:     subscription_pricing.promo_coupon_code,
        discount: subscription_pricing.promo_coupon_discount,
        ends_at:  subscription_pricing.promo_coupon_expires_on,
        status:   subscription_pricing.promo_coupon_status
      },

      charge:             {
        currency: subscription_pricing.currency_name,
        gross:    subscription_pricing.charge_undiscounted_price,
        net:      subscription_pricing.charge_discounted_price
      },

      agent_cost_summary: subscription_pricing.multi_product_agent_cost_summary('zendesk'),
      pending_changes:    subscription_pricing.pending_changes('zendesk'),

      products:           {
        voice:      present_voice_product('zendesk', subscription_pricing),
        guide:      present_subscription_product('guide', subscription_pricing),
        zendesk:    present_subscription_product('zendesk', subscription_pricing),
        zopim:      present_subscription_product('zopim', subscription_pricing),
        # TODO: Uncomment when Explore is GA
        # explore:    present_subscription_product('explore', subscription_pricing),
        tpe:        present_subscription_product('tpe', subscription_pricing),
        answer_bot: present_subscription_product('answer_bot', subscription_pricing),
        outbound:   present_subscription_product('outbound', subscription_pricing)
      }.tap do |json|
        # TODO: Remove when Explore is GA
        if Arturo.feature_enabled_for?(:explore_billing, subscription_pricing.subscription.account)
          json[:explore] = present_subscription_product('explore', subscription_pricing)
        end
      end
    }
  end

  def present_subscription_product(product_name, pricing)
    {
      plan_type:                  pricing.product_plan_type(product_name),
      gross:                      pricing.charge_gross(product_name),
      net:                        pricing.charge_subtotal(product_name),
      discount:                   pricing.product_discount(product_name),
      agent_cost_summary:         pricing.multi_product_agent_cost_summary(product_name),
      flat_fee_cost_summary:      pricing.flat_fee_cost_summary(product_name),
      pending_changes:            pricing.pending_changes(product_name),
      days_left_in_billing_cycle: pricing.days_left_in_billing_cycle(product_name)
    }
  end

  def present_voice_product(_product_name, pricing)
    hide_pricing = pricing.voice_subscription.nil? || pricing.voice_subscription.suspended?
    return {} if hide_pricing
    {
      pricing_type: 'usage',
      active:       pricing.voice_active?
    }.merge!(present_subscription_product('voice', pricing))
  end
end
