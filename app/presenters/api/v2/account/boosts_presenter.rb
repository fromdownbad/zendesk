class Api::V2::Account::BoostsPresenter < Api::V2::Presenter
  self.model_key = :boosts

  # ### JSON Format
  # Boosts are represented as JSON objects which have the following keys:
  #
  # | Name            | Type    | Read-only | Mandatory | Comment
  # | --------------- | ------- | --------- | --------- | -------
  # | type            | string  | yes       | no        | The type of boost (plan or addon)
  # | name            | string  | yes       | no        | The name of the boost
  # | days_remaining  | integer | yes       | no        | The number of days before the boost expires. Can be negative for expired boosts
  #
  # #### Example
  # ```js
  # {
  #   "type":           "addon",
  #   "name":           "light_agents",
  #   "days_remaining": 20,
  # }
  # ```

  def model_json(account)
    presented_boosts = addon_boosts(account) << plan_boost(account)

    presented_boosts.compact
  end

  private

  def addon_boosts(account)
    account.subscription_feature_addons.boosted.map do |boost|
      next unless expiry_date = boost.boost_expires_at

      {
        type: 'addon',
        name: boost.name,
        days_remaining: days_remaining(expiry_date)
      }
    end
  end

  def plan_boost(account)
    boost = account.feature_boost.presence
    return unless boost

    # edge case where boost has expiry date in future but was cancelled in monitor.
    # we don't want to return these cancelled boosts
    return if !boost.is_active && boost.valid_until > Time.now

    {
      type: 'plan',
      name: boost.boost_plan_name,
      days_remaining: days_remaining(boost.valid_until)
    }
  end

  def days_remaining(boost_expiry)
    ((boost_expiry - Time.now) / 1.day).ceil
  end
end
