class Api::V2::Account::TpeSubscriptionPricingPresenter < Api::V2::Presenter
  self.model_key = :tpe_pricing

  def model_json(tpe_subscription_pricing)
    {
      tpe_available_plans: tpe_subscription_pricing.tpe_plans_from_zuora
    }
  end
end
