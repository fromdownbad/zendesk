class Api::V2::Account::HcSettingsPresenter < Api::V2::Presenter
  # ### JSON Format
  # HcSettings are represented as simple flat JSON objects which have the following keys
  #
  # ### Features
  # This group should ONLY return boolean!
  #
  # | Name                                   | Type    | Comment
  # | -------------------------------------- | ------- | -------
  # | chat_enabled                           | boolean | "Always false, no account has legacy chat support anymore."
  # | chat_about_my_ticket                   | boolean | "If true, this account allows chat for tickets aka requests."
  # | end_user_password_change_visible       | boolean | "If true, this account allows end-users to change their password."
  # | end_user_profile_visible               | boolean | "If true, this account allows end-users to view their profile."
  # | remote_authentication_enabled          | boolean | "If true, this account has remote authentication."
  # | end_user_remote_authentication_enabled | boolean | "If true, this account allows end-user to use remote authentication."
  # | ticket_forms                           | boolean | "If true, this account has ticket forms."
  # | has_user_tags                          | boolean | "If true, this account allows tags on users and organizations."
  # | is_open                                | boolean | "If true, this account allows anyone to submit tickets."
  # | prefer_lotus                           | boolean | "If true, this account uses lotus."
  # | satisfaction_ratings_enabled           | boolean | "If true, this account has the customer satisfaction survey enabled."
  # | csat_reason_code_enabled               | boolean | "If true, this account has CSAT bad rating reasons enabled."
  # | end_user_attachments                   | boolean | "If true, this account making attachments to requests."
  # | captcha_required                       | boolean | "If true, this account requires a filled out captcha on anonymous requests."
  # | security_headers                       | boolean | "If true, this account sets the X-Frame-Options header."
  # | multiple_organizations                 | boolean | "If true, this account has multiple organizations enabled."
  # | is_signup_required                     | boolean | "if true, this account requires signup."
  # | has_agent_aliases                      | boolean | "if true, this account allows agents to set aliases."
  # | has_ios_mobile_deeplinking             | boolean | "if true, this account display the mobile deeplinking splash page for iOS."
  # | has_android_mobile_deeplinking         | boolean | "if true, this account display the mobile deeplinking splash page for Android."
  # | has_windows_mobile_deeplinking         | boolean | "if true, this account display the mobile deeplinking splash page for Window Phones."
  # | pathfinder_app                         | boolean | "if true, this account has access to Pathfinder app."
  # | unlimited_categories                   | boolean | "if true, this account can have more than one category."
  # | organizations                          | boolean | "if true, this account can have organizations."
  # | groups                                 | boolean | "if true, this account can have groups."
  # | configurable_spam_filter               | boolean | "if true, this account can disable spam filter."
  # | content_moderation                     | boolean | "if true, this account can content moderation feature."
  # | has_automatic_answers_enabled          | boolean | "if true, this account has Answer Bot enabled."
  # | native_conditional_fields_enabled      | boolean | "if true, this account has Native Conditional Fields enabled. (Not to be confused with the Conditional Field App)"
  # | has_any_active_ssl_certificates        | boolean | "if true, this account has at least one active certificate"
  #
  # ### Rate Limits
  #
  # | Name                                   | Type    | Comment
  # | -------------------------------------- | ------- | -------
  # | content_rate_limit_per_day             | integer | The number of comments, questions and answers allowed per day
  # | request_rate_limit_per_hour            | integer | The number of requests allowed per hour
  # | api_rate_limit_per_minute              | integer | The number of api requests allowed per minute
  # | api_time_limit_per_minute              | integer | The number of CPU minutes allowed per minute
  #
  # ### Security Policy
  #
  # | Name                               | Type    | Comment
  # | ---------------------------------- | ------- | -------
  # | agent_id                           | integer | The id of the agent security policy.
  # | agent_requirements_in_words        | hash    | An Hash describing the agent security requirements for the account.
  # | end_user_id                        | integer | The id of the end user security policy.
  # | end_user_requirements_in_words     | hash    | An Hash describing the end user security requirements for the account.
  #
  # ### State
  #
  # | Name                                    | Type    | Comment
  # | --------------------------------------- | ------- | -------
  # | web_portal                              | string  | The state of the Web Portal: enabled, disabled or restricted
  # | help_center                             | string  | The state of the Help Center: enabled, disabled or restricted
  # | help_center_onboarding_state            | string  | The state for Help Center Onboarding: enabled or disabled
  #
  # ### E-mail Setup
  #
  # | Name                               | Type    | Comment
  # | ---------------------------------- | ------- | -------
  # | noreply_address                    | string  | The noreply address to be used for the account.
  # | from_address                       | string  | The from_address address to be used for the account.
  # | text_mail_template                 | string  | Text variant email template
  # | html_mail_template                 | string  | HTML variant email template
  #
  # ### IP restrictions
  #
  # | Name                               | Type    | Comment
  # | ---------------------------------- | ------- | -------
  # | enabled                            | boolean | True if Account has IP restriction enabled.
  # | enable_agent_ip_restrictions       | boolean | True if Account has IP restriction enabled for agent.
  # | allowed_ip_ranges                  | string  | A comma separated list of ip ranges to whitelist.
  #
  #
  # ### Brands an array of brands described by
  #
  # | Name                               | Type    | Comment
  # | ---------------------------------- | ------- | -------
  # | id                                 | integer | The id of the brand
  # | name                               | string  | The name of the brand
  # | email_setup                        | object  | An email setup for the brand (see above)
  #
  # ### Currency an object with the account currency
  #
  # | Name                               | Type    | Comment
  # | ---------------------------------- | ------- | -------
  # | name                               | string  | 3 letters currency name
  # | symbol                             | string  | The symbol of the currency
  #
  # ### Default locale is an object with the account locale
  #
  # | Name                               | Type    | Comment
  # | ---------------------------------- | ------- | -------
  # | locale                             | string  | locale identifier
  #
  # #### Example
  #
  # ```js
  # "hc_settings": {
  #   "features": {
  #     "chat_enabled": false,
  #     "chat_about_my_ticket": true,
  #     "end_user_password_change_visible": true,
  #     "end_user_profile_visible": true,
  #     "remote_authentication_enabled": false,
  #     "ticket_forms": false,
  #     "has_user_tags": false,
  #     "prefer_lotus": true,
  #     "has_any_active_ssl_certificates" : false
  #   },
  #   "security_policy": {
  #     "id": 400,
  #     "requirements_in_words": {
  #       "password": ["must be at least 5 characters"]
  #     },
  #     "agent_id": 400,
  #     "agent_requirements_in_words": {
  #       "password": ["must be at least 5 characters"]
  #     },
  #     "end_user_id": 400,
  #     "end_user_requirements_in_words": {
  #       "password": ["must be at least 5 characters"]
  #     }
  #   },
  #   "state" : {
  #     "web_portal": "enabled",
  #     "help_center": "restricted",
  #     "help_center_onboarding_state": "disabled"
  #   },
  #   "ticket_forms" : {
  #     "instructions": "Ticket forms instructions"
  #   },
  #   "rate_limits" : {
  #     "content_rate_limit_per_day": 50,
  #     "request_rate_limit_per_hour": 5
  #   },
  #   "ip_restrictions" : {
  #     "enabled" : false,
  #     "enable_agent_ip_restrictions" : false
  #   },
  #   "currency" : {
  #     "name" : "USD",
  #     "symbol" : "$"
  #   }
  #   "default_locale" : {
  #     "locale" : "en-US"
  #   }
  # }
  # ```

  def model_json(account)
    {
      features:        present_with(FeaturesPresenter, account),
      security_policy: present_with(SecurityPolicyPresenter, account),
      state:           present_with(StatePresenter, account),
      email_setup:     present_with(EmailSetupPresenter, account),
      ticket_forms:    present_with(Api::V2::Account::SettingsPresenter::TicketFormPresenter, account),
      rate_limits:     present_with(RateLimitsPresenter, account),
      ip_restrictions: present_with(IpRestrictionsPresenter, account),
      brands:          brand_presenter.collection_presenter.model_json(account.active_brands),
      currency:        present_with(CurrencyPresenter, account),
      default_locale:  present_with(DefaultLocalePresenter, account)
    }
  end

  protected

  def brand_presenter
    Api::V2::Account::HcSettingsPresenter::BrandPresenter.new(user, options)
  end

  class FeaturesPresenter < Api::V2::Presenter
    def model_json(account)
      {
        chat_enabled:                           false,
        chat_about_my_ticket:                   account.settings.chat_about_my_ticket?,
        end_user_password_change_visible:       account.is_end_user_password_change_visible?,
        end_user_profile_visible:               account.is_end_user_profile_visible?,
        remote_authentication_enabled:          account.login_allowed_for_any_role?(:remote),
        end_user_remote_authentication_enabled: account.login_allowed_for_role?(:remote, :end_user),
        ticket_forms:                           account.has_ticket_forms?,
        individual_language_selection:          account.has_individual_language_selection?,
        dc_in_templates:                        account.has_dc_in_templates?,
        permission_sets:                        account.has_permission_sets?,
        has_user_tags:                          account.has_user_and_organization_tags?,
        is_open:                                account.is_open?,
        prefer_lotus:                           true,
        satisfaction_ratings_enabled:           account.has_customer_satisfaction_enabled?,
        csat_reason_code_enabled:               account.csat_reason_code_enabled?,
        end_user_attachments:                   account.settings.end_user_attachments?,
        captcha_required:                       account.settings.captcha_required?,
        security_headers:                       account.settings.security_headers?,
        multiple_organizations:                 account.has_multiple_organizations_enabled?,
        is_signup_required:                     account.is_signup_required?,
        has_agent_aliases:                      account.has_agent_aliases?,
        has_multibrand:                         account.has_multibrand?,
        has_multiple_help_centers:              multiple_help_centers?(account),
        has_ios_mobile_deeplinking:             account.has_ios_mobile_deeplinking?,
        has_android_mobile_deeplinking:         account.has_android_mobile_deeplinking?,
        has_windows_mobile_deeplinking:         account.has_windows_mobile_deeplinking?,
        has_settable_ccs:                       account.has_collaborators_settable_in_help_center?,
        has_comment_email_ccs:                  account.has_comment_email_ccs_allowed_enabled?,
        has_pci_credit_card_custom_field:       account.has_pci_credit_card_custom_field?,
        pathfinder_app:                         account.has_pathfinder_app?,
        has_organizations:                      account.has_organizations?,
        has_groups:                             account.has_groups?,
        has_automatic_answers_enabled:          account.has_automatic_answers_enabled?,
        has_native_conditional_fields_enabled:  account.has_native_conditional_fields_enabled?,
        has_gravatars_enabled:                  account.settings.have_gravatars_enabled?,
        has_any_active_ssl_certificates:        account.certificates.active.exists?
      }
    end

    private

    def multiple_help_centers?(account)
      Guide::MultipleHelpCenters.new(account).available_via_support?
    end
  end

  class SecurityPolicyPresenter < Api::V2::Presenter
    def model_json(account)
      agent_policy    = account.agent_security_policy
      end_user_policy = account.end_user_security_policy

      {
        agent_id:                       agent_policy.id,
        agent_requirements_in_words:    agent_policy.requirements_in_words,
        end_user_id:                    end_user_policy.id,
        end_user_requirements_in_words: end_user_policy.requirements_in_words
      }
    end
  end

  class StatePresenter < Api::V2::Presenter
    def model_json(account)
      {
        help_center:                  account.help_center_state,
        help_center_onboarding_state: account.help_center_onboarding_state
      }
    end
  end

  class EmailSetupPresenter < Api::V2::Presenter
    def model_json(account)
      account_address = Zendesk::Mailer::AccountAddress.new(account: account)

      {
        noreply_address:    account_address.noreply,
        from_address:       account_address.from,
        text_mail_template: account.text_mail_template,
        html_mail_template: account.html_mail_template
      }
    end
  end

  class RateLimitsPresenter < Api::V2::Presenter
    def model_json(account)
      {
        content_rate_limit_per_day:  account.settings.end_user_comment_rate_limit,
        request_rate_limit_per_hour: account.settings.end_user_portal_ticket_rate_limit,
        api_rate_limit_per_minute:   account.api_rate_limit,
        api_time_limit_per_minute:   account.api_time_rate_limit
      }
    end
  end

  class IpRestrictionsPresenter < Api::V2::Presenter
    def model_json(account)
      allowed_ip_ranges = if user.is_admin?
        account.texts.ip_restriction
      else
        ""
      end

      {
        enabled:                      account.settings.ip_restriction_enabled?,
        enable_agent_ip_restrictions: account.settings.enable_agent_ip_restrictions?,
        allowed_ip_ranges:            allowed_ip_ranges
      }
    end
  end

  class BrandPresenter < Api::V2::Presenter
    def model_json(brand)
      {
        id:          brand.id,
        name:        brand.name,
        email_setup: present_with(BrandedEmailSetupPresenter, brand),
        default:     brand.default?,
        logo:        present_with(BrandedLogoPresenter, brand.logo)
      }
    end
  end

  class CurrencyPresenter < Api::V2::Presenter
    def model_json(account)
      return { name: nil, symbol: nil } unless account.subscription.present?
      currency_type = CurrencyType[account.subscription.currency_type]

      {
          name:  currency_type.name,
          symbol: currency_type.score
      }
    end
  end

  class DefaultLocalePresenter < Api::V2::Presenter
    def model_json(account)
      {
        locale: TranslationLocale.find_by_id(account.locale_id).locale
      }
    end
  end

  class BrandedEmailSetupPresenter < Api::V2::Presenter
    def model_json(brand)
      account = brand.account
      brand_address = Zendesk::Mailer::AccountAddress.new(account: account, brand: brand)

      {
        noreply_address:    brand_address.noreply,
        from_address:       brand_address.from,
        text_mail_template: account.text_mail_template,
        html_mail_template: account.html_mail_template
      }
    end
  end

  class BrandedLogoPresenter < Api::V2::Presenter
    def model_json(logo)
      return nil if logo.nil?
      thumbnail_presenter = BrandedLogoThumbnailPresenter.new(user, options).collection_presenter

      {
        filename: logo.filename,
        thumbnails: thumbnail_presenter.model_json(logo.thumbnails)
      }
    end
  end

  class BrandedLogoThumbnailPresenter < Api::V2::Presenter
    def model_json(thumbnail)
      {
        filename: thumbnail.filename,
        thumbnail: thumbnail.thumbnail,
        path: thumbnail.public_filename
      }
    end
  end
end
