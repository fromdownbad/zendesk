class Api::V2::Account::AddonsPresenter < Api::V2::Presenter
  include Zendesk::Features::Presentation

  self.model_key = :addons

  ## ### JSON Format
  ## Features are represented as an array of JSON objects in the following
  ## format:
  ##
  ## {
  ##   "addons": [
  ##     {
  ##       "name": "advanced_security",
  ##       "readable_name": "Advanced security",
  ##       "purchased": true,
  ##       "boosted": false
  ##     },
  ##     {
  ##       "name": "enterprise_productivity_pack",
  ##       "readable_name": "Enterprise productivity pack",
  ##       "purchased": true,
  ##       "boosted": false
  ##     },
  ##     {
  ##       "name": "nps",
  ##       "readable_name": "NPS",
  ##       "purchased": false,
  ##       "boosted": true
  ##     },
  ##     ...
  ##   ],
  ##   "next_page" : null,
  ##   "previous_page" : null,
  ##   "count" : 3
  ## }
  ## ```

  def model_json(model)
    super.merge!(
      name:          model[:name],
      readable_name: readable_name(model[:name]),
      boosted:       (model[:boost_expires_at].present? && model[:boost_expires_at] > DateTime.now),
      purchased:     model[:zuora_rate_plan_id].present?
    )
  end

  def url(model)
    url_builder.send(:api_v2_account_addons_url, model, format: :json)
  end
end
