class Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter < Api::V2::Presenter
  self.model_key = :recharge_settings

  ## ### JSON Format
  ## The Voice Recharge Settings are represented as simple flat JSON objects which has the following keys
  ##
  ## ### Recharge Settings
  ##
  ## | Name             | Type       | Comment
  ## | -----------------| ---------- | -------
  ## | enabled          | boolean    | Voice prepaid enabled
  ## | minimum_balance  | float      | Value when recharge is triggered
  ## | amount           | float      | Value of the recharge
  ## | balance          | float      | Voice subscription balance in monetary amount
  ## | payment_status   | string     | ok/recharge_in_progress/failed
  ##
  ## #### Example
  ##
  ## ```js
  ## "recharge_settings": {
  ##   "enabled": true,
  ##   "minimum_balance": 5.0,
  ##   "amount": 50.0,
  ##   "balance": 1000.00,
  ##   "payment_status": 'ok'
  ## }
  ## ```

  def model_json(recharge_settings)
    {
      enabled:         recharge_settings.enabled?,
      minimum_balance: recharge_settings.minimum_balance.to_f,
      amount:          recharge_settings.amount.to_f,
      balance:         ZendeskBillingCore::Zuora::VoiceUsage.monetary_balance(recharge_settings.account_id),
      payment_status:  recharge_settings.payment_status
    } unless recharge_settings.blank?
  end
end
