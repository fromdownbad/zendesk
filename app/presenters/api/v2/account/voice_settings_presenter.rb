class Api::V2::Account::VoiceSettingsPresenter < Api::V2::Presenter
  # Note: Internal
  #
  # ### JSON Format
  # VoiceSettings are represented as simple flat JSON objects which have the following keys
  #
  # ### Features
  #
  # | Name                             | Type    | Comment
  # | -------------------------------- | ------- | -------
  # | permission_sets                  | boolean | "If true, this account has permissions sets."
  # | business_hours                   | boolean | "If true, this account has business hours."
  #
  # #### Example
  #
  # ```js
  # "voice_settings": {
  #   "features": {
  #     "permission_sets": true,
  #     "business_hours": true
  #   }
  # }
  # ```
  def model_json(account)
    {
      features: present_with(FeaturesPresenter, account)
    }
  end

  class FeaturesPresenter < Api::V2::Presenter
    def model_json(account)
      {
        permission_sets:       account.has_permission_sets?,
        business_hours:        account.has_business_hours?,
        multiple_schedules:    account.has_multiple_schedules?,
        business_hours_active: account.business_hours_active?,
        multibrand:            account.has_multibrand?
      }
    end
  end
end
