class Api::V2::Account::DeploymentsPresenter < Api::V2::Presenter
  self.model_key = :deployments

  # ### JSON Format
  # Deployments are represented as simple flat JSON objects which have the following keys
  #
  # ### Deployments
  #
  # | Name                               | Type    | Comment
  # | ---------------------------------- | ------- | -------
  # | collision                          | boolean | "If true, this account uses Radar for agent collision; if false uses polling for collision"
  # | client_side_logging                | boolean | "If true, this account has client side logging enabled"
  # | js_exception_logging               | boolean | "If true, this account has javascript exceptions logging enabled (i.e. send exceptions to the exception tracker)"
  # | gooddata_advanced_analytics        | boolean | "If true, this account has access to GoodData Advanced Analytics in Lotus Reporting."
  # | gooddata_hourly_synchronization    | boolean | "If true, this account would synchronize with GoodData Advanced Analytics hourly"
  # | ticket_forms                       | boolean | "If true, this account has the ability to create, edit, and use ticket forms."
  # | multibrand                         | boolean | "If true, this account has the ability to create, edit and use brands."
  # | at_mentions                        | boolean | "If true, this account will have @mentions enabled in textareas"
  # | user_assume                        | boolean | "If true, this account will have ablility to assume end users"
  # | voice_trust_on_outbound_calls      | boolean | "If true, this account will skip the restriction on outbound call length for trial accounts"
  # | nps_surveys                        | boolean | "If true, this account has NPS survey feature enabled"
  # | voice_dialer_autocomplete          | boolean | "If true, this account allows agents to autocomplete users/numbers in the outbound dialer"
  # | help_center_analytics              | boolean | "If true, this account has Help Center analytics enabled in the reporting module"
  # | customer_satisfaction              | boolean | "If true, this account has Customer Satisfaction"
  # | satisfaction_dashboard             | boolean | "If true, this account has the Satisfaction dashboard enabled in the reporting module"

  #
  # #### Example
  #
  # ```js
  # "deployments": {
  #   "collision": true,
  #   "client_side_logging": true,
  #   "js_exception_logging": true,
  #   "gooddata_advanced_analytics": true,
  #   "gooddata_hourly_synchronization": false,
  #   "at_mentions": true,
  #   "voice_trust_on_outbound_calls": true,
  #   "voice_dialer_autocomplete": true
  # }
  # ```
  def model_json(account)
    # NOTE: Deployments consist only on Arturos marked as public in capabilities.rb
    lotus_caps = Account.capabilities.select { |_name, capability| capability.public? }

    lotus_caps = lotus_caps.each_with_object({}) do |name_and_capability, result|
      name = name_and_capability.first
      result[name] = account.send("has_" + name.to_s + "?")
    end

    # HACK: until we're sure :collision is not used any more
    lotus_caps[:collision] = true

    # Need to surface this here, as required by GoodData
    lotus_caps[:voice_insights_v2] = account.voice_feature_enabled?(:insights_v2)

    lotus_caps.merge(implicit_lotus_caps)
  end

  protected

  def implicit_lotus_caps
    Rails.cache.fetch("lotus-features/#{account.id}", expires_in: Arturo::Feature.cache_ttl) do
      lotus_features = ::Arturo::Feature.
        where("`symbol` LIKE ?", "lotus_feature_%").
        where(deprecated_at: nil)
      lotus_features.reduce({}) do |result, feature|
        result.update(feature.symbol.sub(/^lotus_feature_/, '') => feature.enabled_for?(account))
      end
    end
  end
end
