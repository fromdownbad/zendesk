class Api::V2::Account::FeaturesPresenter < Api::V2::Presenter
  self.model_key = :features

  ## ### JSON Format
  ## Features are represented as JSON objects with the following keys
  ##
  ## | Name                     | Type    | Read-only | Mandatory | Comment
  ## | -------------------------| ------- | --------- | --------- | -------
  ## | {feature_name}           | boolean | yes       | no        | Is {feature_name} enabled for current account?
  ##
  ## ```js
  ## {
  ##   "features": {
  ##     "agent_collision": {
  ##       "enabled": true
  ##     },
  ##     "agent_display_names": {
  ##       "enabled": true
  ##     },
  ##     "api_limit_200_rpm": {
  ##       "enabled": false
  ##     },
  ##     "api_limit_400_rpm": {
  ##       "enabled": false
  ##     },
  ##     "api_limit_700_rpm" : {
  ##       "enabled": true
  ##     },
  ##     "api_limit_700_rpm_legacy": {
  ##       "enabled": false
  ##     },
  ##     "apps_private": {
  ##       "enabled": false
  ##     },
  ##     ...
  ##   }
  ## }
  ## ```

  def model_json(features)
    {}.tap do |json|
      features.each do |name, capability|
        json[name] = { enabled: capability.available_and_enabled?(account) }
      end
    end
  end
end
