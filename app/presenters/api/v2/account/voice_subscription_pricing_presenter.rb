class Api::V2::Account::VoiceSubscriptionPricingPresenter < Api::V2::Presenter
  self.model_key = :voice_pricing

  ####  Voice Pricing (sideloaded on subscription)
  ## "voice_pricing": {
  ##   "voice_available_plans": [
  ##     {
  ##       "plan_type":                             1,
  ##       "currency":                              "USD",
  ##       "per_agent_cost_with_monthly_billing":   0,
  ##       "per_agent_cost_with_quarterly_billing": null,
  ##       "per_agent_cost_with_annual_billing":    0
  ##     },
  ##     {
  ##       "plan_type":                             2,
  ##       "per_agent_cost_with_monthly_billing":   25,
  ##       "per_agent_cost_with_quarterly_billing": null,
  ##       "per_agent_cost_with_annual_billing":    19
  ##     }
  ##    ]
  ##   }
  ####

  def model_json(voice_subscription_pricing)
    {
      voice_available_plans: voice_subscription_pricing.voice_plans_from_zuora
    }
  end
end
