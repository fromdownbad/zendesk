class Api::V2::Account::SurveyResponsePresenter < Api::V2::Presenter
  self.model_key = :survey_response

  ## ### JSON Format
  ## The Survey Response is represented as simple flat JSON objects which has the following keys
  ##
  ## ### Survey Response
  ##
  ## | Name                               | Type    | Comment
  ## | ---------------------------------- | ------- | -------
  ## | id                                 | integer | The survey record's id
  ## | industry                           | string  | The account's industry
  ## | employee_count                     | string  | The account's employee count
  ## | target_audience                    | string  | The account's target audience
  ## | support_structure                  | string  | The account's support structure
  ## | customer_count                     | string  | The account's customer count
  ## | agent_count                        | integer | The account's agent count
  ## | team_count                         | integer | The account's teams of agents count
  ##
  ## #### Example
  ##
  ## ```js
  ## "survey_response": {
  ##   "id": 62,
  ##   "industry": "media",
  ##   "employee_count": "10-99",
  ##   "target_audience": "customers",
  ##   "support_structure": "part_time",
  ##   "customer_count": "hundreds",
  ##   "agent_count": null,
  ##   "team_acount": null
  ## }
  ## ```

  def model_json(survey_response)
    {
      id:                survey_response.id,
      industry:          survey_response.industry,
      employee_count:    survey_response.employee_count,
      target_audience:   survey_response.target_audience,
      support_structure: survey_response.support_structure,
      customer_count:    survey_response.customer_count,
      agent_count:       survey_response.agent_count,
      team_count:        survey_response.team_count
    }
  end
end
