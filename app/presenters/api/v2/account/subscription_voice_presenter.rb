class Api::V2::Account::SubscriptionVoicePresenter < Api::V2::Presenter
  self.model_key = :voice

  def model_json(subscription)
    {
      phone_numbers: subscription.account.phone_numbers.pluck(:number)
    }
  end
end
