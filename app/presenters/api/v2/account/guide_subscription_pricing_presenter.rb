class Api::V2::Account::GuideSubscriptionPricingPresenter < Api::V2::Presenter
  self.model_key = :guide_pricing

  ####  Guide Pricing (sideloaded on subscription)
  ## "guide_pricing": {
  ##   "guide_available_plans": [
  ##     {
  ##       "plan_type":                             1,
  ##       "currency":                              "USD",
  ##       "per_agent_cost_with_monthly_billing":   0,
  ##       "per_agent_cost_with_quarterly_billing": null,
  ##       "per_agent_cost_with_annual_billing":    0
  ##     },
  ##     {
  ##       "plan_type":                             2,
  ##       "currency":                              "USD",
  ##       "per_agent_cost_with_monthly_billing":   10,
  ##       "per_agent_cost_with_quarterly_billing": null,
  ##       "per_agent_cost_with_annual_billing":    9
  ##     }
  ##    ]
  ##   }
  ####

  def model_json(guide_subscription_pricing)
    {
      guide_available_plans: guide_subscription_pricing.guide_plans_from_zuora
    }
  end
end
