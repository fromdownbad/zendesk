class Api::V2::Account::RemoteAuthenticationPresenter < Api::V2::Presenter
  self.model_key = :remote_authentication

  def model_json(model)
    {
      active:     model.is_active?,
      type:       model.auth_mode,
      name:       model.name,
      login_url:  model.remote_login_url,
      logout_url: model.remote_logout_url
    }
  end
end
