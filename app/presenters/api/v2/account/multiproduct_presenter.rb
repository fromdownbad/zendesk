class Api::V2::Account::MultiproductPresenter < Api::V2::Presenter
  self.model_key = :multiproduct

  def model_json(subscription)
    {
      is_multiproduct_account: subscription.account.billing_multi_product_participant?
    }
  end
end
