# Used in `Api::V2::Internal::MobileSdkSettingsController`
class Api::V2::Account::MobileSdkSettingsPresenter < Api::V2::Presenter
  self.model_key = :mobile_sdk_settings

  ## ### JSON Format
  ## Mobile SDK configuration settings
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | enabled         | boolean | no        | yes       | Specifies if the SDK is enabled
  ## | auth_endpoint   | string  | no        | yes       | The authentication endpoint for the Mobile SDK
  ## | auth_token      | string  | no        | yes       | The JWT token that is used to verify the payload
  ## | created_at      | date    | yes       | no        | The time the record was created
  ## | updated_at      | date    | yes       | no        | The time of the last update of the record
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "auth_endpoint":   "https://somesite.com/zd/auth",
  ##   "auth_token:":     "aub7qw64ci7467c64738iubyret",
  ##   "created_at":      "2012/03/05 10:38:52 +1000",
  ##   "updated_at":      "2012/03/05 10:38:52 +1000"
  ## }
  ## ```
  def model_json(mobile_sdk_settings)
    {
      enabled:       mobile_sdk_settings.enabled,
      auth_endpoint: mobile_sdk_settings.auth_endpoint,
      auth_token:    mobile_sdk_settings.auth_token,
      created_at:    mobile_sdk_settings.created_at,
      updated_at:    mobile_sdk_settings.updated_at
    }
  end
end
