class Api::V2::Account::VoicePartnerEditionAccountPresenter < Api::V2::Presenter
  self.model_key = :voice_partner_edition_account

  ## ### JSON Format
  ## Talk Partner Edition account
  ##
  ## | Name             | Type     | Read-only | Comment
  ## | ---------------- | -------- | --------- | -------
  ## | plan_type        | integer  | no        | The plan the account is on
  ## | active           | boolean  | no        | Whether the account is active or not
  ## | trial            | boolean  | yes       | Specifies if account is on trial
  ## | trial_expires_at | datetime | yes       | Specifies when trial ends or ended
  ## | has_addon        | boolean  | yes       | Specifies if the account has a billing addon

  ##
  ## #### Example
  ## ```js
  ## {
  ##   "plan_type":         1,
  ##   "active:":           true,
  ##   "has_addon":         true,
  ##   ...
  ## }
  ## ```
  def model_json(voice_partner_edition_account)
    {}.tap do |json|
      json.merge!(
        plan_type: voice_partner_edition_account.plan_type,
        active: voice_partner_edition_account.active?,
        trial: voice_partner_edition_account.trial?,
        trial_expires_at: voice_partner_edition_account.trial_expires_at,
        has_addon: voice_partner_edition_account.account.has_talk_cti_partner?
      ) unless voice_partner_edition_account.blank?
    end
  end
end
