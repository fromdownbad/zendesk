class Api::V2::Account::PartnerSettingsPresenter < Api::V2::Presenter
  self.model_key = :partner_settings

  def model_json(account)
    {
      name: account.settings.partner_name,
      url:  account.settings.partner_url
    }
  end
end
