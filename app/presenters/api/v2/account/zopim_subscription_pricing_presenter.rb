class Api::V2::Account::ZopimSubscriptionPricingPresenter < Api::V2::Presenter
  self.model_key = :zopim_pricing

  def model_json(zopim_subscription_pricing)
    {
      zopim_available_plans: zopim_subscription_pricing.zopim_plans_from_zuora
    }
  end
end
