class Api::V2::Account::ExploreSubscriptionPricingPresenter < Api::V2::Presenter
  self.model_key = :explore_pricing

  def model_json(explore_subscription_pricing)
    {
      explore_available_plans: explore_subscription_pricing.explore_plans_from_zuora
    }
  end
end
