class Api::V2::Account::SubscriptionCreditCardPresenter < Api::V2::Presenter
  self.model_key = :credit_card

  def model_json(subscription)
    credit_card = subscription.zuora_subscription.try(:credit_card)

    if credit_card
      {
        shadowed_number: credit_card.credit_card_mask_number,
        expiry: {
          month: credit_card.credit_card_expiration_month,
          year: credit_card.credit_card_expiration_year
        }
      }
    end
  end
end
