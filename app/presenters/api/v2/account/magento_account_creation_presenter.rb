class Api::V2::Account::MagentoAccountCreationPresenter < Api::V2::Presenter
  self.model_key = :magento

  def model_json(account)
    new_token = account.api_tokens.create!(description: 'Magento')
    magento_order_number_ticket_field = account.ticket_fields.detect { |t| t.title == I18n.t('txt.admin.magento.custom_ticket_field.order_number_field_title', locale: account.translation_locale) }
    {
        api_token:                            new_token.temp_full_value,
        remote_authentication_token:          account.remote_authentications.active.jwt.last.token,
        magento_order_number_ticket_field_id: magento_order_number_ticket_field.id
    }
  end
end
