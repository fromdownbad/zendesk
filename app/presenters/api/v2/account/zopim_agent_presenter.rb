class Api::V2::Account::ZopimAgentPresenter < Api::V2::Presenter
  self.model_key = :zopim_agent

  def model_json(model)
    {}.tap do |json|
      json.merge!(
        zopim_agent_id:   model.zopim_agent_id,
        email:            model.email,
        is_owner:         model.is_owner,
        is_enabled:       model.is_enabled,
        is_administrator: model.is_administrator,
        display_name:     model.display_name,
        syncstate:        model.syncstate,
        is_linked:        model.is_linked?,
        is_serviceable:   model.is_serviceable?,
        user:             {
          id:    model.user.id,
          email: model.user.email
        }
      ) unless model.blank?
    end
  end
end
