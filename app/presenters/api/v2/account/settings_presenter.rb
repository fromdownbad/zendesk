class Api::V2::Account::SettingsPresenter < Api::V2::Presenter
  self.model_key = :settings

  ## ### JSON Format
  ## You can access the following data describing the settings of an account. You can update the settings marked as not read-only.
  ##
  ## ### Branding
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | header_color                       | string  | no        | HEX of the header color
  ## | page_background_color              | string  | no        | HEX of the page background color
  ## | tab_background_color               | string  | no        | HEX of tab background color
  ## | text_color                         | string  | no        | HEX of the text color, usually matched to contrast well with `header_color`
  ## | header_logo_url                    | string  | yes       | The URL for the custom header logo
  ## | favicon_url                        | string  | yes       | The URL for the custom favicon
  ##
  ## ### Apps
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | use                                | boolean | yes       | The account can use apps
  ## | create_private                     | boolean | yes       | The account can create private apps
  ##
  ## ### Tickets
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | comments_public_by_default         | boolean | no        | Comments from agents are public by default
  ## | is_first_comment_private_enabled   | boolean | no        | Allow first comment on tickets to be private
  ## | list_newest_comments_first         | boolean | no        | When viewing a ticket, show the newest comments and events first
  ## | collaboration                      | boolean | yes       | CCs may be added to a ticket
  ## | private_attachments                | boolean | no        | Users must login to access attachments
  ## | email_attachments                  | boolean | no        | Attachments should be sent as real attachments when under the size limit
  ## | agent_collision                    | boolean | yes       | Clients should provide an indicator when a ticket is being viewed by another agent
  ## | list_empty_views                   | boolean | no        | Clients should display Views with no matching Tickets in menus
  ## | maximum_personal_views_to_list     | number  | yes       | Maximum number of personal Views clients should display in menus
  ## | tagging                            | boolean | no        | Tickets may be tagged
  ## | markdown_ticket_comments           | boolean | no        | Whether agent comments should be processed with Markdown
  ## | emoji_autocompletion               | boolean | no        | Whether agent comments should allow for Emoji rendering
  ## | agent_ticket_deletion              | boolean | no        | Whether agents can delete tickets
  ##
  ## ### Agents
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | agent_workspace                    | boolean | no        | Toggles the Agent Workspace experience
  ##
  ## ### Chat
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | enabled                            | boolean | yes       | Chat is enabled
  ## | maximum_requests                   | number  | yes       | The maximum number of chat requests an agent may handle at one time
  ## | welcome_message                    | string  | yes       | The message automatically sent to end-users when they begin chatting with an agent
  ##
  ## ### Twitter
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | shorten_url                        | string  | yes       | Possible values: 'always', 'optional', 'never'
  ##
  ## ### G Suite
  ##
  ## | Name                               | Type     | Read-only | Comment
  ## | ---------------------------------- | -------  | --------- | -------
  ## | has_google_apps                    | boolean  | yes       |
  ## | has_google_apps_admin              | boolean  | no        | Account has at least one G Suite admin
  ##
  ## ### Voice
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | enabled                            | boolean | yes       | Voice is enabled
  ## | maintenance                        | boolean | yes       |
  ## | logging                            | boolean | yes       |
  ## | outbound_enabled                   | boolean | yes       |
  ## | agent_confirmation_when_forwarding | boolean | yes       |
  ## | agent_wrap_up_after_calls          | boolean | yes       |
  ## | maximum_queue_size                 | number  | yes       |
  ## | maximum_queue_wait_time            | number  | yes       |
  ## | only_during_business_hours         | boolean | yes       |
  ## | recordings_public                  | boolean | yes       |
  ## | uk_mobile_forwarding               | boolean | yes       |
  ##
  ## ### Users
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | tagging                            | boolean | no        | Users may be tagged
  ## | time_zone_selection                | boolean | yes       | Whether user can view time zone for profile
  ## | language_selection                 | boolean | yes       | Whether to display language drop down for a user
  ## | agent_created_welcome_emails       | boolean | no        | Whether a user created by an agent receives a welcome email
  ## | end_user_phone_number_validation   | boolean | no        | Whether a user's phone number is validated
  ## | have_gravatars_enabled             | boolean | no        | Whether user gravatars are displayed in the UI
  ##
  ## ### GooddataAdvancedAnalytics
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | enabled                            | boolean | yes       | GoodData Advanced Analytics is enabled
  ##
  ## ### Brands
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | default_brand_id                   | number  | no        | The id of the brand that is assigned to tickets by default
  ## | require_brand_on_new_tickets       | boolean | no        | Require agents to select a brand before saving tickets
  ##
  ## ### Statistics
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | forum                              | boolean | yes       | Allow users to view forum statistics
  ## | search                             | boolean | yes       | Allow users to view search statistics
  ##
  ## ### Billing
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | backend                            | string  | yes       | Backend Billing system either 'internal' or 'zuora'
  ##
  ## ### ActiveFeatures
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | on_hold_status                     | boolean | yes       | Account can use status hold
  ## | user_tagging                       | boolean | no        | Enable user tags
  ## | ticket_tagging                     | boolean | no        | Allow tagging tickets
  ## | topic_suggestion                   | boolean | yes       | Allow topic suggestions in tickets
  ## | voice                              | boolean | yes       | Voice support
  ## | facebook_login                     | boolean | yes       |
  ## | google_login                       | boolean | yes       |
  ## | twitter_login                      | boolean | yes       |
  ## | forum_analytics                    | boolean | yes       | Forum and search analytics
  ## | business_hours                     | boolean | no        |
  ## | agent_forwarding                   | boolean | yes       |
  ## | chat                               | boolean | yes       |
  ## | chat_about_my_ticket               | boolean | yes       |
  ## | customer_satisfaction              | boolean | no        |
  ## | satisfaction_prediction            | boolean | no        |
  ## | csat_reason_code                   | boolean | yes       |
  ## | markdown                           | boolean | yes       | Markdown in ticket comments
  ## | bcc_archiving                      | boolean | yes       | Account has a bcc_archive_address set
  ## | allow_ccs                          | boolean | yes       | Allow CCs on tickets
  ## | advanced_analytics                 | boolean | yes       |
  ## | insights                           | boolean | yes       |
  ## | explore                            | boolean | yes       | Has account plan setting 'explore'
  ## | good_data_and_explore              | boolean | yes       | Has account plan setting 'good_data_and_explore'
  ## | explore_on_support_pro_plan        | boolean | yes       | Allowed to show explore role controls
  ## | sandbox                            | boolean | yes       | Account has a sandbox
  ## | suspended_ticket_notification      | boolean | yes       |
  ## | twitter                            | boolean | yes       | Account monitors at least one Twitter handle
  ## | facebook                           | boolean | yes       | Account is actively linked to at least one Facebook page
  ## | feedback_tabs                      | boolean | yes       | Feedback tab has been configured before
  ## | dynamic_contents                   | boolean | yes       | Account has at least one dynamic content
  ## | light_agents                       | boolean | yes       | Account has at least one light agent
  ## | is_abusive                         | boolean | yes       | Account exceeded trial limits
  ## | rich_content_in_email              | boolean | yes       | Account supports incoming HTML email
  ## | fallback_composer                  | boolean | yes       | Fallback composer for Asian languages
  ##
  ## ### API
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | accepted_api_agreement             | boolean | no        | Account has accepted the API agreement
  ## | api_password_access                | boolean | no        | Allow the account to use the API with username/password
  ## | api_token_access                   | boolean | no        | Allow the account to use the API with API tokens
  ##
  ## ### TicketForm
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | ticket_forms_instructions          | string  | no        |
  ##
  ## ### Lotus
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | prefer_lotus                       | boolean | yes       | Prefers the current version of Zendesk Support rather than Zendesk Classic
  ##
  ## ### Ticket Sharing Partners
  ##
  ## | Name                               | Type    | Read-only | Comment
  ## | ---------------------------------- | ------- | --------- | -------
  ## | support_addresses                  | object  | yes       | Array of support addresses for all accepted sharing agreement accounts
  ##
  ## ### Rules
  ##
  ## | Name                        | Type    | Read-only | Comment
  ## | ----------------------------| ------- | --------- | -------
  ## | macro_most_used             | boolean | no        | Display the most-used macros in the `Apply macro` list. Defaults to `true`
  ## | macro_order                 | string  | no        | Default macro display order. Possible values are 'alphabetical' or 'position'
  ## | skill_based_filtered_views  | object  | no        | Array of view ids
  ##
  ## ### Limits
  ##
  ## | Name                 | Type   | Read-only | Comment
  ## | -------------------- | ------ | --------- | -------
  ## | attachment_size      | number | yes       | The maximum ticket attachment file size (in bytes)
  ##
  ## ### Metrics
  ##
  ## | Name                 | Type   | Read-only | Comment
  ## | -------------------- | ------ | --------- | -------
  ## | account_size         | string | yes       | An account size category computed from the number of billable agents
  #
  ## ### Localization
  ##
  ## | Name                 | Type   | Read-only | Comment
  ## | -------------------- | ------ | --------- | -------
  ## | locale_ids           | array  | no        | Array of locale IDs enabled for the account. See [Locales](./locales) for possible values
  ##
  ## #### Example
  ##
  ## ```js
  ## "settings": {
  ##   "branding": {
  ##     "header_color": "1A00C3",
  ##     "page_background_color": "333333",
  ##     "tab_background_color": "3915A2",
  ##     "text_color": "FFFFFF",
  ##     "header_logo_url": "/path/to/header_logo.png",
  ##     "favicon_url": "/path/to/favicon.png",
  ##   },
  ##   "apps": {
  ##     "use":            true,
  ##     "create_private": true,
  ##     "create_public":  true
  ##   },
  ##   "tickets": {
  ##     "comments_public_by_default":     true,
  ##     "list_newest_comments_first":     true,
  ##     "collaboration":                  true,
  ##     "private_attachments":            true,
  ##     "email_attachments":              true,
  ##     "agent_collision":                true
  ##     "list_empty_views":               true,
  ##     "maximum_personal_views_to_list": 12,
  ##     "tagging":                        true,
  ##     "markdown_ticket_comments":       false
  ##   },
  ##   "agents": {
  ##     "agent_workspace": true,
  ##   }
  ##   "chat": {
  ##     "maximum_request_count": 5,
  ##     "welcome_message":       "Hello, how may I help you?",
  ##     "enabled":               true
  ##   },
  ##   "voice": {
  ##     "enabled":     true,
  ##     "maintenance": false,
  ##     "logging":     true
  ##   },
  ##   "twitter": {
  ##     "shorten_url": "optional"
  ##   },
  ##   "google_apps": {
  ##     "has_google_apps": false,
  ##   },
  ##   "user": {
  ##     "tagging": true,
  ##     "time_zone_selection": true,
  ##     "language_selection": true,
  ##     "end_user_phone_number_validation": false,
  ##     "have_gravatars_enabled": false
  ##   },
  ##   "billing": {
  ##     "backend": 'internal'
  ##   },
  ##   "brands": {
  ##     "default_brand_id": 47
  ##   },
  ##   "active_features": {
  ##     "on_hold_status":                true,
  ##     "user_tagging":                  true,
  ##     "ticket_tagging":                true,
  ##     "topic_suggestion":              true,
  ##     "voice":                         true,
  ##     "business_hours":                true,
  ##     "facebook_login":                true,
  ##     "google_login":                  true,
  ##     "twitter_login":                 true,
  ##     "forum_analytics":               true,
  ##     "agent_forwarding":              true,
  ##     "chat":                          true,
  ##     "chat_about_my_ticket":          true,
  ##     "customer_satisfaction":         true,
  ##     "csat_reason_code":              true,
  ##     "markdown":                      true,
  ##     "bcc_archiving":                 true,
  ##     "allow_ccs":                     true,
  ##     "advanced_analytics":            true,
  ##     "sandbox":                       true,
  ##     "suspended_ticket_notification": true,
  ##     "twitter":                       true,
  ##     "facebook":                      true,
  ##     "feedback_tabs":                 true,
  ##     "dynamic_contents":              true,
  ##     "light_agents":                  true,
  ##     "explore":                       true,
  ##     "explore_on_support_pro_plan":   true,
  ##     "good_data_and_explore":         false,
  ##     "fallback_composer":             false,
  ##   },
  ##   "ticket_sharing_partners": {
  ##     "support_addresses": [
  ##       "foo@example.com"
  ##     ]
  ##   },
  ##   "api": {
  ##     "accepted_api_agreement": true
  ##     "api_password_access":    true
  ##     "api_token_access":       true
  ##   },
  ##   "rule": {
  ##     "macro_most_used": true,
  ##     "macro_order": "alphabetical",
  ##     "skill_based_filtered_views": []
  ##   },
  ##   "limits": {
  ##     "attachment_size": 20971520
  ##   },
  ##   "cdn": {
  ##     "cdn_provider": "default",
  ##     "fallback_cdn_provider": "cloudfront"
  ##   },
  ##   "metrics": {
  ##     "account_size": "1-99"
  ##   }
  ## }
  ## ```
  def model_json(account)
    json = {
      branding:                    present_with(BrandingPresenter, account),
      apps:                        present_with(AppsPresenter, account),
      tickets:                     present_with(TicketsPresenter, account),
      agents:                      present_with(AgentsPresenter, account),
      groups:                      present_with(GroupsPresenter, account),
      chat:                        present_with(ChatPresenter, account),
      voice:                       present_with(VoicePresenter, account),
      twitter:                     present_with(TwitterPresenter, account),
      google_apps:                 present_with(GoogleAppsPresenter, account),
      user:                        present_with(UserPresenter, account),
      screencast:                  present_with(ScreencastPresenter, account),
      lotus:                       present_with(LotusPresenter, account),
      gooddata_advanced_analytics: present_with(GooddataAdvancedAnalyticsPresenter, account),
      statistics:                  present_with(StatisticsPresenter, account),
      billing:                     present_with(BillingPresenter, account),
      active_features:             present_with(ActiveFeaturesPresenter, account),
      ticket_form:                 present_with(TicketFormPresenter, account),
      brands:                      present_with(BrandsPresenter, account),
      api:                         present_with(ApiPresenter, account),
      rule:                        present_with(RulePresenter, account),
      limits:                      present_with(LimitsPresenter, account),
      onboarding:                  present_with(OnboardingPresenter, account),
      cross_sell:                  present_with(CrossSellPresenter, account),
      cdn:                         present_with(CdnPresenter, account),
      metrics:                     present_with(MetricsPresenter, account),
      ticket_sharing_partners:     present_with(TicketSharingPartnersPresenter, account),
      localization:                present_with(LocalizationPresenter, account)
    }

    if account.has_email_simplified_threading_onboarding?
      json[:email] = present_with(EmailPresenter, account).extract!(:simplified_email_threading)
    end

    if account.has_email_settings_api?
      json[:email] = present_with(EmailPresenter, account)
    end

    if account.has_automatic_answers_enabled?
      json[:automatic_answers] = present_with(AutomaticAnswersPresenter, account)
    end

    json
  end

  class GroupsPresenter < Api::V2::Presenter
    self.model_key = :groups

    def model_json(account)
      {
        check_group_name_uniqueness: account.settings.check_group_name_uniqueness,
      }
    end
  end

  class ApiPresenter < Api::V2::Presenter
    self.model_key = :api

    def model_json(account)
      {
        accepted_api_agreement: account.settings.accepted_api_agreement,
        api_password_access:    account.settings.api_password_access,
        api_token_access:       account.settings.api_token_access,
      }
    end
  end

  class BrandingPresenter < Api::V2::Presenter
    self.model_key = :branding

    def model_json(account)
      {
        header_color:          account.branding.header_color,
        page_background_color: account.branding.page_background_color,
        tab_background_color:  account.branding.tab_background_color,
        text_color:            account.branding.text_color,
        header_logo_url:       account.header_logo.try(:public_filename),
        favicon_url:           account.favicon.try(:public_filename)
      }
    end
  end

  class TicketSharingPartnersPresenter < Api::V2::Presenter
    self.model_key = :ticket_sharing_partners

    def model_json(account)
      {support_addresses: account.ticket_sharing_partner_support_addresses}
    end
  end

  class AppsPresenter < Api::V2::Presenter
    self.model_key = :apps

    def model_json(account)
      {
        use:            true,
        create_private: true,
        create_public:  account.has_apps_create_public?,
      }
    end
  end

  class TicketsPresenter < Api::V2::Presenter
    self.model_key = :tickets

    def model_json(account)
      settings = {
        agent_ticket_deletion: account.settings.ticket_delete_for_agents?,
        list_newest_comments_first: account.settings.events_reverse_order?,
        collaboration: account.is_collaboration_enabled?,
        private_attachments: account.settings.private_attachments?,
        email_attachments: account.settings.email_attachments?,
        agent_collision: account.has_agent_collision?,
        tagging: account.settings.ticket_tagging?,
        list_empty_views: account.settings.ticket_show_empty_views?,
        comments_public_by_default: account.is_comment_public_by_default,
        is_first_comment_private_enabled: account.is_first_comment_private_enabled,
        maximum_personal_views_to_list: account.max_private_views_for_display,
        status_hold: account.use_status_hold?,
        markdown_ticket_comments: account.settings.markdown_ticket_comments?,
        rich_text_comments: account.settings.rich_text_comments?,
        emoji_autocompletion: account.settings.emoji_autocompletion?,
        assign_tickets_upon_solve: account.settings.assign_tickets_upon_solve?,
        allow_group_reset: account.settings.change_assignee_to_general_group?
      }

      if account.has_email_ccs?
        settings[:accepted_new_collaboration_tos] = account.settings.accepted_new_collaboration_tos?
        settings[:follower_and_email_cc_collaborations] = account.has_follower_and_email_cc_collaborations_enabled?
        settings[:auto_updated_ccs_followers_rules] = account.settings.auto_updated_ccs_followers_rules?
      end

      if account.has_follower_and_email_cc_collaborations_enabled?
        settings[:agent_can_change_requester]    = account.has_agent_can_change_requester_enabled?
        settings[:comment_email_ccs_allowed]     = account.has_comment_email_ccs_allowed_enabled?
        settings[:ticket_followers_allowed]      = account.has_ticket_followers_allowed_enabled?
      end

      if account.has_email_ccs_light_agents_v2?
        settings[:light_agent_email_ccs_allowed] = account.has_light_agent_email_ccs_allowed_enabled?
      end

      if account.has_comment_email_ccs_allowed_enabled? && account.has_ticket_followers_allowed_enabled?
        settings[:agent_email_ccs_become_followers] = account.has_agent_email_ccs_become_followers_enabled?
      end

      if account.has_skill_based_attribute_ticket_mapping? && account.has_skill_based_ticket_routing?
        settings[:using_skill_based_routing] = account.settings.using_skill_based_routing?
      end

      if account.has_skill_based_ticket_routing? && account.settings.using_skill_based_routing?
        settings[:edit_ticket_skills_permission] = account.settings.edit_ticket_skills_permission
      end

      settings
    end
  end

  class AgentsPresenter < Api::V2::Presenter
    self.model_key = :agents

    def model_json(account)
      {
        agent_workspace: account.settings.polaris,
        focus_mode: account.settings.focus_mode
      }
    end
  end

  class ChatPresenter < Api::V2::Presenter
    self.model_key = :chat

    def model_json(account)
      {
        enabled:               false,
        integrated:            account.chat_integrated?, # returns true if the account is integrated with either phase 1 or phase 2 zopim integration
        available:             account.chat_available?,
        maximum_request_count: account.maximum_chat_requests,
        welcome_message:       account.chat_welcome_message
      }
    end
  end

  class TicketFormPresenter < Api::V2::Presenter
    self.model_key = :ticket_form

    def model_json(account)
      {
        ticket_forms_instructions:     account.ticket_forms_instructions,
        raw_ticket_forms_instructions: account.account_property_set.ticket_forms_instructions
      }
    end
  end

  class BrandsPresenter < Api::V2::Presenter
    self.model_key = :brands

    def model_json(account)
      {
        default_brand_id:             account.default_brand_id,
        require_brand_on_new_tickets: account.settings.require_brand_on_new_tickets
      }
    end
  end

  class VoicePresenter < Api::V2::Presenter
    self.model_key = :chat

    def model_json(account)
      {
        enabled:                            account.has_voice_enabled?,
        logging:                            account.has_voice_logging?,
        outbound_enabled:                   account.settings.voice_outbound_enabled?,
        agent_confirmation_when_forwarding: account.settings.voice_agent_confirmation_when_forwarding?,
        agent_wrap_up_after_calls:          account.settings.voice_agent_wrap_up_after_calls?,
        maximum_queue_size:                 account.settings.voice_maximum_queue_size.to_i,
        maximum_queue_wait_time:            account.settings.voice_maximum_queue_wait_time.to_i,
        only_during_business_hours:         account.settings.voice_only_during_business_hours?,
        recordings_public:                  account.settings.voice_recordings_public?,
        uk_mobile_forwarding:               true
      }
    end
  end

  class TwitterPresenter < Api::V2::Presenter
    self.model_key = :twitter

    def model_json(account)
      {shorten_url: account.shortened_url_frequency}
    end
  end

  class GoogleAppsPresenter < Api::V2::Presenter
    self.model_key = :google_apps

    def model_json(account)
      {
        has_google_apps:       account.has_google_apps?,
        has_google_apps_admin: account.settings.has_google_apps_admin?
      }
    end
  end

  class UserPresenter < Api::V2::Presenter
    self.model_key = :user

    def model_json(account)
      {
        tagging: account.settings.has_user_tags?,
        time_zone_selection: account.has_time_zone_selection?,
        language_selection: account.has_individual_language_selection?,
        multiple_organizations: account.has_multiple_organizations_enabled?,
        agent_created_welcome_emails: account.is_welcome_email_when_agent_register_enabled?,
        end_user_phone_number_validation: account.settings.end_user_phone_number_validation,
        have_gravatars_enabled: account.settings.have_gravatars_enabled?
      }
    end
  end

  class ScreencastPresenter < Api::V2::Presenter
    self.model_key = :screencast

    def model_json(account)
      {
        enabled_for_tickets: account.has_screencasts_for_tickets_enabled?,
        host:                account.screenr_integration.host,
        tickets_recorder_id: account.screenr_integration.tickets_recorder_id
      }
    end
  end

  class LotusPresenter < Api::V2::Presenter
    self.model_key = :lotus

    def model_json(account)
      {
        prefer_lotus: true,
        reporting:    account.has_lotus_reporting?,
        pod_id:       account.pod_id
      }
    end
  end

  class GooddataAdvancedAnalyticsPresenter < Api::V2::Presenter
    self.model_key = :gooddata_integration

    def model_json(account)
      { enabled: account.has_gooddata_advanced_analytics? }
    end
  end

  class StatisticsPresenter < Api::V2::Presenter
    self.model_key = :display_analytics

    def model_json(account)
      {
        forum: account.has_forum_statistics?,
        search: account.has_search_statistics?,
        rule_usage: account.has_rule_usage_stats?
      }
    end
  end

  class BillingPresenter < Api::V2::Presenter
    self.model_key = :billing

    def model_json(account)
      { backend: billing_backend(account) }
    end

    private

    def billing_backend(account)
      account.has_zuora_managed_billing_enabled? ? 'zuora' : 'internal'
    end
  end

  class RulePresenter < Api::V2::Presenter
    self.model_key = :rule

    def model_json(account)
      {
        macro_most_used: account.settings.macro_most_used,
        macro_order:     account.settings.macro_order
      }.tap do |settings|
        if account.has_skill_based_view_filters?
          settings[:skill_based_filtered_views] = account.settings.skill_based_filtered_views
        end

        if account.has_skill_based_attribute_ticket_mapping? && account.has_skill_based_ticket_routing?
          settings[:using_skill_based_routing] = account.settings.using_skill_based_routing?
        end
      end
    end
  end

  class AutomaticAnswersPresenter < Api::V2::Presenter
    self.model_key = :automatic_answers

    def model_json(account)
      threshold_name = Zendesk::Types::PredictionThresholdType[account.settings.automatic_answers_threshold].name
      {
        threshold: threshold_name.downcase
      }
    end
  end

  class ActiveFeaturesPresenter < Api::V2::Presenter
    self.model_key = :active_features

    def model_json(account)
      settings = {
        on_hold_status:                account.use_status_hold?,

        user_tagging:                  account.settings.has_user_tags?,
        ticket_tagging:                account.settings.ticket_tagging?,
        topic_suggestion:              account.settings.topic_suggestion_for_ticket_submission?,
        voice:                         account.settings.voice?,

        business_hours:                account.business_hours_active?,

        facebook_login:                account.settings.is_facebook_login_enabled?,
        google_login:                  account.settings.is_google_login_enabled?,
        twitter_login:                 account.role_settings.end_user_twitter_login?,

        forum_analytics:               account.settings.display_analytics?,
        agent_forwarding:              account.settings.agent_forwardable_emails?,
        chat:                          account.settings.chat?,
        chat_about_my_ticket:          account.settings.chat_about_my_ticket?,
        customer_satisfaction:         account.has_customer_satisfaction_enabled?,
        satisfaction_prediction:       account.settings.satisfaction_prediction?,
        automatic_answers:             account.settings.automatic_answers?,
        csat_reason_code:              account.csat_reason_code_enabled?,
        screencasts:                   account.settings.screencasts_for_tickets?,
        markdown:                      account.settings.markdown_ticket_comments?,
        bcc_archiving:                 account.settings.bcc_archive_address?,
        allow_ccs:                     account.settings.collaboration_enabled?,

        advanced_analytics:            account.gooddata_integration.present?,
        insights:                      account.gooddata_integration.present?,
        explore:                       account.explore_plan_setting?,
        explore_on_support_ent_plan:   account.enterprise_support_plus_explore?,
        explore_on_support_pro_plan:   account.professional_support_plus_explore?,
        good_data_and_explore:         account.good_data_and_explore_plan_setting?,
        sandbox:                       account.sandbox.present?,
        suspended_ticket_notification: suspended_ticket_notification_enabled?(account),

        twitter:                       MonitoredTwitterHandle.where(account_id: account.id).any?,
        facebook:                      Facebook::Page.where(account_id: account.id).active.any?,
        dynamic_contents:              account.cms_texts.any?,
        light_agents:                  account.light_agents.any?,
        ticket_forms:                  account.ticket_forms_is_active?,
        user_org_fields:               account.custom_fields_is_active?,
        is_abusive:                    account.abusive?,
        rich_content_in_emails:        account.settings.rich_content_in_emails?
      }
      unless account.has_polaris?
        settings[:customer_context_as_default] = account.settings.customer_context_as_default?
      end

      if account.has_account_settings_benchmark_opt_out?
        settings[:benchmark_opt_out] = account.settings.benchmark_opt_out
      end

      settings[:fallback_composer] = account.has_fallback_omnicomposer? ? true : account.settings.fallback_composer?

      if account.has_email_settings_api?
        settings[:custom_dkim_domain] = account.subscription.has_custom_dkim_domain? || account.settings.custom_dkim_domain?
        settings[:allow_email_template_customization] = account.allow_email_template_customization?
      end

      settings
    end

    def suspended_ticket_notification_enabled?(account)
      notification = account.suspended_ticket_notification

      notification.present? && notification.frequency != 0
    end
  end

  class LimitsPresenter < Api::V2::Presenter
    self.model_key = :limits

    def model_json(account)
      {attachment_size: account.max_attachment_size}
    end
  end

  class OnboardingPresenter < Api::V2::Presenter
    self.model_key = :onboarding

    def model_json(account)
      {
        checklist_onboarding_version: account.settings.checklist_onboarding_version,
        onboarding_segments:          account.settings.onboarding_segments,
        product_sign_up:              account.settings.product_sign_up
      }
    end
  end

  class EmailPresenter < Api::V2::Presenter
    self.model_key = :email

    def model_json(account)
      {
        accept_wildcard_emails:        account.settings.accept_wildcard_emails?,
        custom_dkim_domain:            account.settings.custom_dkim_domain?,
        email_sender_authentication:   account.settings.email_sender_authentication?,
        email_template_photos:         account.settings.email_template_photos?,
        email_template_selection:      account.settings.email_template_selection?,
        gmail_actions:                 account.settings.gmail_actions?,
        html_mail_template:            account.html_mail_template,
        mail_delimiter:                account.mail_delimiter,
        modern_email_template:         account.settings.modern_email_template?,
        no_mail_delimiter:             account.settings.no_mail_delimiter,
        personalized_replies:          account.settings.personalized_replies?,
        simplified_email_threading:    account.settings.simplified_email_threading?,
        rich_content_in_emails:        account.settings.rich_content_in_emails?,
        send_gmail_messages_via_gmail: account.settings.send_gmail_messages_via_gmail?,
        text_mail_template:            account.text_mail_template
      }
    end
  end

  class CrossSellPresenter < Api::V2::Presenter
    self.model_key = :cross_sell

    def model_json(account)
      {
        show_chat_tooltip: account.settings.show_chat_tooltip,
        xsell_source: account.settings.xsell_source
      }
    end
  end

  class CdnPresenter < Api::V2::Presenter
    self.model_key = :cdn

    def model_json(account)
      json = {
        cdn_provider: account.settings.cdn_provider,
        fallback_cdn_provider: account.fallback_cdn_provider
      }

      json[:hosts] = cdn_hosts(account)
      json
    end

    def cdn_hosts(account)
      return [] if account.cdn_provider == 'disabled'

      hosts = []

      if default_cdn = Zendesk::AccountCdn.provider_pool.account_provider(account)
        url = "http#{"s" if options[:ssl]}://#{default_cdn.host}"
        hosts << { name: account.cdn_provider, url: url }
      end

      if fallback_cdn = account.fallback_cdn
        url = "http#{"s" if options[:ssl]}://#{fallback_cdn[1].host}"
        hosts << { name: fallback_cdn[0], url: url }
      end

      hosts
    end
  end

  class MetricsPresenter < Api::V2::Presenter
    self.model_key = :metrics

    def model_json(account)
      { account_size: account_size(account.billable_agents.count) }
    end

    private

    def account_size(billable_agent_count)
      return 'unknown' unless billable_agent_count.is_a? Integer

      if    billable_agent_count >= 50_000 then '50000+'
      elsif billable_agent_count >= 20_000 then '20000-49999'
      elsif billable_agent_count >= 5_000 then '5000-19999'
      elsif billable_agent_count >= 1_000 then '1000-4999'
      elsif billable_agent_count >= 400 then '400-999'
      elsif billable_agent_count >= 100 then '100-399'
      elsif billable_agent_count >= 1 then '1-99'
      else 'unknown'
      end
    end
  end

  class LocalizationPresenter < Api::V2::Presenter
    self.model_key = :localization

    def model_json(account)
      {locale_ids: account.allowed_translation_locale_ids}
    end
  end
end
