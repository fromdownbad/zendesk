class Api::V2::Account::ZopimSubscriptionPresenter < Api::V2::Presenter
  self.model_key = :zopim_subscription

  ## ### JSON Format
  ## Accounts are represented as JSON objects which have the following keys:
  ##
  ## | Name                     | Type    | Read-only | Mandatory | Comment
  ## | ------------------------ | ------- | --------- | --------- | -------
  ## | zopim_account_id         | integer | yes       | yes       | The Zopim account id
  ## | status                   | string  | yes       | yes       | The status of the Zopim account
  ## | plan_type                | string  | yes       | yes       | The plan type of the Zopim account
  ## | plan_name                | string  | yes       | yes       | The plan type's name
  ## | max_agents               | integer | yes       | yes       | The number of allowed agents in the Zopim account
  ## | is_serviceable           | boolean | yes       | yes       | Indicates if the subscription is active and linked to its Zopim counterpart
  ## | trial_expires_on         | date    | yes       | no        | The zopim trial plan type end date
  ## | last_synchronized_at     | date    | yes       | no        | The date/time when this record was last synchornized to its Zopim counterpart
  ## | syncstate                | string  | yes       | no        | Indicates the current sync state for this record
  ## | expired_trial            | boolean | yes       | no        | Indicates whether the zopim trial has expired
  ## | created_at               | date    | yes       | no        | The date the zopim subscription was created
  ## | trial_days               | integer | yes       | no        |
  ## | trial_ended_at           | date    | yes       | no        | Date the trial expired
  ## | trial_extension_eligible | boolean | yes       | no        | Indicates whether the trial is eligible for extension
  ## | final_trial_expire_date  | date    | yes       | no        |
  ## | days_left_in_trial       | integer | yes       | no        |
  ## | purchased_at             | date    | yes       | no        | Date the trial was purchased
  ## | zopim_reseller_id        | integer | yes       | no        |
  ## | chat_limits_enabled      | boolean | yes       | no        | Indicates whether chat limits are turned on at any level
  ## | account_chat_limit       | integer | yes       | no        | If > 0, indicates the chat limit enforced at account level. If == 0, limit is enforced at agent level
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "zopim_account_id":         891865,
  ##   "plan_type":                "trial",
  ##   "plan_name":                "Trial",
  ##   "max_agents":               8,
  ##   "status":                   "active",
  ##   "is_serviceable":           true,
  ##   "expired_trial":            false,
  ##   "last_synchronized_at":     "2015-01-12T21:20:40Z",
  ##   "syncstate":                "ready",
  ##   "trial_expires_on":         "2015-01-12T21:20:40Z",
  ##   "created_at":               "2015-01-12T21:20:40Z",
  ##   "trial_days":               30,
  ##   "trial_ended_at":           "2015-01-12T21:20:40Z",
  ##   "trial_extension_eligible": false,
  ##   "final_trial_expire_date":  "2015-01-12T21:20:40Z",
  ##   "days_left_in_trial":       10,
  ##   "purchased_at":             "2015-01-12T21:20:40Z",
  ##   "zopim_reseller_id":        1234,
  ##   "chat_limits_enabled":      true,
  ##   "account_chat_limit":       3
  ## }
  ## ```
  def model_json(model)
    {}.tap do |json|
      json.merge!(
        zopim_account_id:         model.zopim_account_id,
        plan_type:                model.plan_type,
        plan_name:                model.zopim_plan_type.description,
        max_agents:               model.max_agents,
        status:                   model.status,
        is_serviceable:           model.is_serviceable?,
        trial_expires_on:         model.trial_expires_on,
        expired_trial:            model.expired_trial?,
        created_at:               model.created_at.to_date,
        trial_days:               model.trial_days,
        trial_ended_at:           model.trial_ended_at,
        trial_extension_eligible: model.trial_extension_eligible?,
        final_trial_expire_date:  model.final_trial_expire_date,
        days_left_in_trial:       model.days_left_in_trial,
        last_synchronized_at:     model.last_synchronized_at,
        syncstate:                model.syncstate,
        purchased_at:             model.purchased_at,
        zopim_reseller_id:        model.zopim_reseller_id,
        chat_limits_enabled:      model.chat_limits_enabled,
        account_chat_limit:       model.account_chat_limit
      ) unless model.blank?
    end
  end

  def side_loads(model)
    {}.tap do |json|
      if side_load?(:zopim_integration)
        json[:zopim_integration] = integration_json(model.zopim_integration)
      end

      if side_load?(:administrators)
        json[:administrators] = agents_json(model.administrators)
      end

      if side_load?(:app_market_integration)
        json[:app_market_integration] = app_market_integration_json(model)
      end
    end
  end

  private

  def integration_json(model)
    zopim_integration_presenter.model_json(model) if model.present?
  end

  def agents_json(model)
    zopim_agent_presenter.collection_presenter.model_json(model)
  end

  def app_market_integration_json(model)
    app_market_integration_presenter.model_json(model)
  end

  def zopim_integration_presenter
    @zopim_integration_presenter ||= Api::V2::ZopimIntegrationPresenter.
      new(user, options)
  end

  def zopim_agent_presenter
    @zopim_agent_presenter ||= Api::V2::Account::ZopimAgentPresenter.
      new(user, options)
  end

  def app_market_integration_presenter
    @app_market_integration_presenter ||= Api::V2::AppMarketIntegrationPresenter.
      new(user, options)
  end
end
