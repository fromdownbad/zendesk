class Api::V2::Account::SandboxPresenter < Api::V2::Presenter
  self.model_key = :sandbox

  ## ### JSON Format
  ## Sandboxes are represented as JSON objects which have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | name            | string  | no        | yes       | The name of the sandbox
  ## | subdomain       | string  | no        | no        | The subdomain of the sandbox
  ## | url             | string  | yes       | yes       | The url of the sandbox
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "url":         "https://example231231413.zendesk.com/",
  ##   "name":        "Example Company",
  ##   "subdomain":   "example231231413",
  ## }
  ## ```

  def model_json(account)
    if account.sandbox
      super.merge!(
        name:      account.sandbox.name,
        subdomain: account.sandbox.subdomain
      )
    end
  end

  def url(account)
    account.sandbox.try(:url)
  end
end
