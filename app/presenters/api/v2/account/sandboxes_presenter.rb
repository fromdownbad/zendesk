class Api::V2::Account::SandboxesPresenter < Api::V2::AccountPresenter
  self.model_key = :account

  def model_json(account)
    attributes = {
        active: account.is_active && account.is_serviceable,
        created_at: account.created_at,
        sandbox_type: account.settings.sandbox_type,
    }
    super.merge!(attributes)
  end
end
