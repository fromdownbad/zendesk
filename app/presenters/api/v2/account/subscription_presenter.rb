###############################################################################
#
#                         IMPORTANT PLEASE READ
#
# Talk to Juris Galang, Prudhvi Dhulipalla, or Eric Huang before making your
# changes in this presenter.
#
###############################################################################
class Api::V2::Account::SubscriptionPresenter < Api::V2::Presenter
  self.model_key = :subscription

  ## ### Subscription
  ##
  ## | Name                               | Type    | Comment
  ## | ---------------------------------- | ------- | -------
  ## | account_type                       | string  | "trial", "spoke", "customer", "sandbox", "sponsored" or "unknown"
  ## | active                             | boolean | The account's  status
  ## | trial                              | boolean | The account's trial status
  ## | suite_active                       | boolean | True if account has a suite trial or paid suite
  ## | suspended                          | boolean | The account's suspension status
  ## | dunning                            | string  | The account's dunning status. Returns "OK" or "WARNING" or "SECONDCHANCE" or "LASTCHANCE" or "SUSPENDED"
  ## | sales_model                        | string  | The account's sales model. Returns "SELF_SERVICE" or "ASSISTED"
  ## | plan_type                          | string  | The accounts' plan type. "Small" or "Medium" or "Large" or "ExtraLarge"
  ## | plan_name                          | string  | The accounts' plan name. "Starter" or "Regular" or "Plus" or "Enterprise" (depending on pricing model)
  ## | source                             | string  | The lead source through which this account is created.
  ## | help_desk_size                     | number  | The accounts help desk size.
  ## | created_at                         | date    | The account's creation date
  ## | updated_at                         | date    | The last updated time for the account
  ## | trial_expires_on                   | date    | End date of the trial
  ## | days_left_in_trial                 | number  | Days left in account's trial period
  ## | voice_customer                     | boolean | The account's voice status. Returns true if voice customer , false otherwise.
  ## | billing_backend                    | string  | The account's billing_backend
  ## | is_elite                           | boolean | The account's elite status.
  ## | max_agents                         | number  | The total number of zendesk agents
  ## | base_agents                        | number  | The number of zendesk agents without any secondary agents included
  ## | zopim_max_agents                   | number  | Number of zopim agents
  ## | zopim_plan_type                    | string  | Zopim Subscription's plan_type
  ## | voice_max_agents                   | number  | Number of voice agents. nil if unlimited
  ## | voice_plan_type                    | integer | Voice Subscription's plan type
  ## | guide_max_agents                   | number  | Number of guide agents. nil if unlimited
  ## | guide_plan_type                    | integer | Guide Subscription's plan type
  ## | tpe_max_agents                     | number  | Number of tpe agents. nil if unlimited
  ## | tpe_plan_type                      | integer | Tpe Subscription's plan type
  ## | pricing_model_revision             | integer | The id of the pricing model used for this subscription
  ## | future_subscription_exists         | boolean | True if a future-dated subscription exists in Zuora, false otherwise
  ## | zuora_payment_method_type          | string  | the payment method type in zuora. "manual invoice" or "paypal" or "credit card" or "none"
  ## | suite_allowed                      | boolean | True if account has Suite product or it meets the criteria found in account model, False otherwise

  ##
  ## #### Example
  ## https://trial.zd-dev.com/api/v2/account/subscription/preview.json?include=pricing,configuration&subscription%5Bmax_agents%5D=5&subscription%5Bbilling_cycle_type%5D=1&subscription%5Bplan_type%5D=3&subscription%5Bvoice_plan_type%5D=2&subscription%5Bvoice_max_agents%5D=1
  ##
  ## ```js
  ##  {
  ##   "subscription": {
  ##     "account_type": "trial",
  ##     "active": true,
  ##     "trial": true,
  ##     "suite_active": true,
  ##     "suspended": false,
  ##     "dunning": "OK",
  ##     "collect_vat": false,
  ##     "sales_model": "Self-service",
  ##     "plan_type": "Large",
  ##     "plan_name": "Plus",
  ##     "file_upload_cap": 20,
  ##     "base_agents": 5,
  ##     "max_agents": 5,
  ##     "source": "",
  ##     "help_desk_size": "Small team",
  ##     "created_at": null,
  ##     "updated_at": null,
  ##     "trial_expires_on": "2015-06-25",
  ##     "days_left_in_trial": 28,
  ##     "voice_customer": false,
  ##     "billing_backend": "internal",
  ##     "is_elite": false,
  ##     "pricing_model_revision": 5,
  ##     "future_subscription_exists": false,
  ##     "add_agents_available": true,
  ##     "suite_allowed": true,
  ##     "plans": {
  ##       "zendesk": {
  ##         "plan_type": "Large",
  ##         "plan_name": "Plus",
  ##         "base_agents": 5,
  ##         "max_agents": 5,
  ##         "is_elite": false,
  ##         "trial": true,
  ##         "file_upload_cap": 20,
  ##         "help_desk_size": "Small team",
  ##         "trial_expires_on": "2015-06-25",
  ##         "days_left_in_trial": 28
  ##       },
  ##       "zopim": {
  ##         "max_agents": null,
  ##         "plan_type": null,
  ##         "trial": false,
  ##         "trial_expires_on": null,
  ##         "expired_trial": false,
  ##         "days_left_in_trial": null,
  ##         "is_legacy_plan": false,
  ##       },
  ##       "voice": {
  ##          "voice_customer": false,
  ##          "max_agents": 1,
  ##          "plan_type": 2,
  ##          "is_trial": true
  ##       },
  ##       "guide": {
  ##         "max_agents": null,
  ##         "plan_type": null
  ##       },
  ##       "tpe": {
  ##         max_agents: null,
  ##         plan_type:  null
  ##       }
  ##     }
  ##   },
  ##   "pricing": {
  ##     "version": 5,
  ##     "plan": {
  ##       "id": 3,
  ##       "name": "Plus"
  ##     },
  ##     "available_plans": [
  ##       {
  ##         "plan_type": 1,
  ##         "plan_type_name": "small",
  ##         "name": "Starter",
  ##         "price": 2,
  ##         "currency": "USD",
  ##         "base": 2,
  ##         "max_agent_floor": 1,
  ##         "max_agent_ceiling": 3,
  ##         "per_agent_cost_with_monthly_billing": 2,
  ##         "per_agent_cost_with_quarterly_billing": null,
  ##         "per_agent_cost_with_annual_billing": 1,
  ##         "promotional_discount_monthly": 0,
  ##         "promotional_discount_quarterly": null,
  ##         "promotional_discount_annually": 0
  ##       },
  ##       {
  ##         "plan_type": 2,
  ##         "plan_type_name": "medium",
  ##         "name": "Regular",
  ##         "price": 29,
  ##         "currency": "USD",
  ##         "base": 29,
  ##         "max_agent_floor": 1,
  ##         "max_agent_ceiling": 4294967296,
  ##         "per_agent_cost_with_monthly_billing": 29,
  ##         "per_agent_cost_with_quarterly_billing": null,
  ##         "per_agent_cost_with_annual_billing": 25,
  ##         "promotional_discount_monthly": 0,
  ##         "promotional_discount_quarterly": null,
  ##         "promotional_discount_annually": 0
  ##       },
  ##       {
  ##         "plan_type": 3,
  ##         "plan_type_name": "large",
  ##         "name": "Plus",
  ##         "price": 69,
  ##         "currency": "USD",
  ##         "base": 69,
  ##         "max_agent_floor": 1,
  ##         "max_agent_ceiling": 4294967296,
  ##         "per_agent_cost_with_monthly_billing": 68,
  ##         "per_agent_cost_with_quarterly_billing": null,
  ##         "per_agent_cost_with_annual_billing": 58,
  ##         "promotional_discount_monthly": 0,
  ##         "promotional_discount_quarterly": null,
  ##         "promotional_discount_annually": 0
  ##       },
  ##       {
  ##         "plan_type": 4,
  ##         "plan_type_name": "extra_large",
  ##         "name": "Enterprise",
  ##         "price": 139,
  ##         "currency": "USD",
  ##         "base": 139,
  ##         "max_agent_floor": 1,
  ##         "max_agent_ceiling": 4294967296,
  ##         "per_agent_cost_with_monthly_billing": 139,
  ##         "per_agent_cost_with_quarterly_billing": null,
  ##         "per_agent_cost_with_annual_billing": 125,
  ##         "promotional_discount_monthly": 0,
  ##         "promotional_discount_quarterly": null,
  ##         "promotional_discount_annually": 0
  ##       }
  ##     ],
  ##     "billing_cycle": {
  ##       "id": 1,
  ##       "name": "Monthly",
  ##       "discount": null
  ##     },
  ##     "voice": {
  ##        "active": false,
  ##        "transcription": false
  ##     },
  ##     "promo": {
  ##       "code": null,
  ##       "discount": 0,
  ##       "ends_at": null,
  ##       "status": null
  ##     },
  ##     "charge": {
  ##       "currency": "USD",
  ##       "gross": 365,
  ##       "net": 365
  ##     },
  ##     "agent_cost_summary": {
  ##       "monthly_unit_cost": 68,
  ##       "monthly_unit_cost_with_annual_billing": 58,
  ##       "agents_per_unit": 1,
  ##       "annual_unit_cost": 696
  ##     },
  ##     "pending_changes": {},
  ##     "products": {
  ##       "voice": {
  ##         "pricing_type": "usage",
  ##         "active": false,
  ##         "transcription": false,
  ##         "plan_type": 2,
  ##         "gross": 25,
  ##         "net": 25,
  ##         "discount": 0,
  ##         "agent_cost_summary": {
  ##           "monthly_unit_cost": 25,
  ##           "monthly_unit_cost_with_annual_billing": 19,
  ##           "agents_per_unit": 1,
  ##           "annual_unit_cost": 228
  ##         },
  ##       "pending_changes": {},
  ##       "days_left_in_billing_cycle": -1
  ##       },
  ##       "zendesk": {
  ##         "plan_type": "Large",
  ##         "gross": 340,
  ##         "net": 340,
  ##         "discount": 0,
  ##         "agent_cost_summary": {
  ##           "monthly_unit_cost": 68,
  ##           "monthly_unit_cost_with_annual_billing": 58,
  ##           "agents_per_unit": 1,
  ##           "annual_unit_cost": 696
  ##         },
  ##         "pending_changes": {},
  ##         "days_left_in_billing_cycle": -1
  ##       },
  ##       "zopim": {
  ##         "plan_type": null,
  ##         "gross": 0,
  ##         "net": 0,
  ##         "discount": 0,
  ##         "agent_cost_summary": {
  ##            "monthly_unit_cost": 25,
  ##            "monthly_unit_cost_with_annual_billing": 20,
  ##            "agents_per_unit": 1,
  ##            "annual_unit_cost": 240
  ##          },
  ##          "pending_changes": {},
  ##          "days_left_in_billing_cycle": -1
  ##         }
  ##       },
  ##       "guide": {
  ##         "plan_type": null,
  ##         "gross": 0,
  ##         "net": 0,
  ##         "discount": 0,
  ##         "agent_cost_summary": {
  ##            "monthly_unit_cost": 0,
  ##            "monthly_unit_cost_with_annual_billing": 0,
  ##            "agents_per_unit": 1,
  ##            "annual_unit_cost": 0
  ##          },
  ##          "pending_changes": {},
  ##          "days_left_in_billing_cycle": -1
  ##        },
  ##       "tpe": {
  ##         "plan_type": null,
  ##         "gross": 0,
  ##         "net": 0,
  ##         "discount": 0,
  ##         "agent_cost_summary": {
  ##            "monthly_unit_cost": 14,
  ##            "monthly_unit_cost_with_annual_billing": 9,
  ##            "agents_per_unit": 1,
  ##            "annual_unit_cost": 108
  ##          },
  ##          "pending_changes": {},
  ##          "days_left_in_billing_cycle": -1
  ##         }
  ##       }
  ##     }
  ##   }
  ##  '''

  def model_json(subscription)
    guide_subscription      = subscription.guide_preview
    zopim_subscription      = subscription.zopim_subscription
    voice_subscription      = subscription.voice_preview || subscription.trial_or_legacy_voice_subscription
    tpe_subscription        = subscription.tpe_preview || subscription.tpe_subscription

    # NOTE: We do NOT want to return the `answer_bot_plan` and `outbound` at
    # Lotus bootstrap, since they add a dependency on Accounts Service, so we'd
    # rather not have the Answer Bot and Outbound data at bootstrap than adding
    # this dependency. We will only load the data when the pricing side load is
    # requested, hence only on the subscription page or in billing apps.
    answer_bot_subscription = side_load?(:pricing) ? subscription.answer_bot_preview : nil
    outbound_subscription   = side_load?(:pricing) ? subscription.outbound_preview : nil
    # explore_subscription is also backed by Account Service
    # TODO: Uncomment when Explore is GA
    # explore_subscription    = side_load?(:pricing) ? subscription.explore_preview : nil

    {
      account_type:               subscription.account_type,
      active:                     subscription.account.is_active,
      trial:                      subscription.is_trial,
      suite_active:               subscription.account.settings.suite_subscription?,
      suspended:                  !subscription.account.is_serviceable?,
      dunning:                    subscription.current_dunning_state,
      collect_vat:                subscription.collect_vat?,
      sales_model:                subscription.sales_model_value,
      plan_type:                  SubscriptionPlanType[subscription.plan_type].to_s,
      plan_name:                  subscription.plan_name,
      file_upload_cap:            subscription.file_upload_cap,
      base_agents:                subscription.base_agents,
      max_agents:                 subscription.max_agents,
      source:                     subscription.account.source,
      help_desk_size:             subscription.account.help_desk_size,
      created_at:                 subscription.created_at,
      updated_at:                 subscription.updated_at,
      term_end_date:              subscription.term_end_date,
      trial_expires_on:           subscription.trial_expires_on,
      days_left_in_trial:         subscription.is_trial? ? subscription.days_left_in_trial : 0,
      voice_customer:             subscription.account.current_voice_status == "Customer",
      billing_backend:            subscription.billing_backend,
      is_elite:                   !!subscription.is_elite?,
      pricing_model_revision:     subscription.pricing_model_revision,
      future_subscription_exists: subscription.future_subscription?,
      add_agents_available:       subscription.add_agents_available?,
      zuora_payment_method_type:  subscription.zuora_payment_method_type,
      suite_allowed:              subscription.suite_allowed?,
      plans:                      {
        zendesk: {
          plan_type:          SubscriptionPlanType[subscription.plan_type].to_s,
          plan_name:          subscription.plan_name,
          base_agents:        subscription.base_agents,
          max_agents:         subscription.max_agents,
          is_elite:           !!subscription.is_elite?,
          trial:              subscription.is_trial,
          file_upload_cap:    subscription.file_upload_cap,
          help_desk_size:     subscription.account.help_desk_size,
          trial_expires_on:   subscription.trial_expires_on,
          days_left_in_trial: subscription.is_trial? ? subscription.days_left_in_trial : 0,
        },
        zopim:      zopim_plan(zopim_subscription, subscription),
        voice:      voice_plan(voice_subscription, subscription),
        guide:      guide_plan(guide_subscription),
        # TODO: Uncomment when Explore is GA
        # explore:    explore_plan(explore_subscription, subscription),
        tpe:        tpe_plan(tpe_subscription, subscription),
        answer_bot: answer_bot_plan(answer_bot_subscription),
        outbound:   outbound_plan(outbound_subscription)
      }
    }.tap do |json|
      # TODO: Remove when Explore is GA
      if Arturo.feature_enabled_for?(:explore_billing, subscription.account)
        explore_subscription   = side_load?(:pricing) ? subscription.explore_preview : nil
        json[:plans][:explore] = explore_plan(explore_subscription, subscription)
      end
    end
  end

  def guide_plan(guide_subscription)
    return {} unless side_load?(:pricing)

    return {} unless guide_subscription

    {
      legacy:             guide_subscription.legacy?,
      max_agents:         guide_subscription.max_agents,
      plan_type:          guide_subscription.plan_type,
      trial:              guide_subscription.trial?,
      trial_expires_on:   guide_subscription.trial_expires_at,
      days_left_in_trial: guide_subscription.days_left_in_trial
    }
  end

  def voice_plan(voice_subscription, zendesk_subscription)
    customer_status = zendesk_subscription.account.current_voice_status == "Customer"

    if voice_subscription.try(:is_active?)
      {
        voice_customer:      customer_status,
        max_agents:          voice_subscription.max_agents,
        plan_type:           voice_subscription.plan_type,
        trial:               !!voice_subscription.trial,
        legacy:              !!voice_subscription.legacy,
        original_voice_plan: voice_subscription.try(:original_voice_plan_type?)
      }
    else
      { voice_customer: customer_status }
    end
  end

  def zopim_plan(zopim_subscription, subscription)
    if zopim_subscription.try(:is_active?)
      {
        max_agents:         zopim_max_agents(zopim_subscription, subscription),
        plan_type:          zopim_subscription.try(:present_plan_type),
        trial:              zopim_subscription.try(:is_trial?),
        trial_expires_on:   zopim_subscription.try(:trial_expires_on),
        expired_trial:      zopim_subscription.try(:expired_trial?),
        days_left_in_trial: zopim_subscription.try(:is_trial?) ? zopim_subscription.days_left_in_trial : 0,
        plan_name:          zopim_subscription.try(:present_plan_name),
        plan_weight:        zopim_subscription.present? ? ZBC::Zopim::PlanType[zopim_subscription.plan_type].weight : nil,
        is_legacy_plan:     zopim_subscription.try(:is_legacy_chat?)
      }
    else
      {
        max_agents:         nil,
        plan_type:          nil,
        trial:              false,
        trial_expires_on:   nil,
        expired_trial:      false,
        days_left_in_trial: nil,
        plan_name:          nil,
        plan_weight:        nil,
        is_legacy_plan:     false
      }
    end
  end

  def tpe_plan(tpe_subscription, subscription)
    tpe_account = subscription.account.voice_partner_edition_account

    return {} if tpe_subscription.blank? && tpe_account.blank?

    if tpe_subscription.try(:is_active?)
      {
        max_agents:         tpe_subscription.max_agents,
        plan_type:          tpe_subscription.plan_type,
        trial:              false,
        trial_expires_on:   nil,
        days_left_in_trial: -1
      }
    elsif tpe_account.try(:in_trial?)
      {
        max_agents:         subscription.max_agents,
        plan_type:          ZBC::Tpe::PlanType::PartnerEdition.plan_type,
        trial:              true,
        trial_expires_on:   tpe_account.trial_expires_at,
        days_left_in_trial: trial_days_remaining(tpe_account.trial_expires_at)
      }
    else
      {}
    end
  end

  def answer_bot_plan(answer_bot_subscription)
    if answer_bot_subscription
      trial_expires_at = answer_bot_subscription.trial_expires_at
      {
        max_agents:         answer_bot_subscription.max_resolutions,
        plan_type:          answer_bot_subscription.plan_type,
        trial:              answer_bot_subscription.trial?,
        trial_expires_on:   trial_expires_at,
        days_left_in_trial: trial_days_remaining(trial_expires_at)
      }
    else
      # We always show Answer Bot in the UI with Guide Legacy/Pro so customers
      # can immediately purchase Answer Bot without starting a trial first
      {
        max_agents:         0,
        plan_type:          1,
        trial:              false,
        trial_expires_on:   nil,
        days_left_in_trial: trial_days_remaining(nil)
      }
    end
  end

  def outbound_plan(outbound_subscription)
    if outbound_subscription && outbound_subscription.active?
      plan_type_obj = ZBC::Outbound::PlanType[outbound_subscription.plan_type.to_i]
      trial_expires_at = outbound_subscription.trial_expires_at
      {
        max_agents:         outbound_subscription.monthly_messaged_users,
        plan_type:          outbound_subscription.plan_type,
        plan_name:          plan_type_obj.description,
        trial:              outbound_subscription.trial?,
        trial_expires_on:   trial_expires_at,
        days_left_in_trial: trial_days_remaining(trial_expires_at)
      }
    else
      {}
    end
  end

  def explore_plan(explore_subscription, subscription)
    plan_type_obj = ZBC::Explore::PlanType[explore_subscription.try(:plan_type)]
    if !!explore_subscription.try(:subscribed?)
      {
        max_agents:         explore_subscription.max_agents,
        plan_type:          explore_subscription.plan_type,
        plan_name:          plan_type_obj.description,
        plan_weight:        plan_type_obj.weight,
        trial:              false,
        trial_expires_on:   nil,
        days_left_in_trial: trial_days_remaining(nil),
        purchasable:        false
      }
    elsif trial_or_show_inactive?(explore_subscription)
      trial_expires_at = explore_subscription.trial_expires_at
      {
        # default max agents control to Support max agents for trial or inactive
        max_agents:         subscription.max_agents,
        plan_type:          explore_subscription.plan_type,
        plan_name:          plan_type_obj.description,
        plan_weight:        plan_type_obj.weight,
        trial:              explore_subscription.trial?,
        trial_expires_on:   trial_expires_at,
        days_left_in_trial: trial_days_remaining(trial_expires_at),
        purchasable:        true
      }
    else
      {}
    end
  end

  def side_loads(subscription)
    json = {}

    # NOTE: the :pricing side-load is outdated and is being deprecated on this endpoint. All pricing
    # info should be pulled from the billing app instead (see https://github.com/zendesk/billing).
    # Until this can be completed, we are instead enabling a new :eaa_pricing side-load specifically
    # for "easy agent adds" (aka EAA), which is the only known use case for this data at this time.
    if (side_load?(:pricing) && !subscription.account.has_billing_deprecate_classic_pricing?) || side_load?(:eaa_pricing)
      subscription_pricing = subscription_pricing(subscription)
      json[:pricing] = subscription_pricing_presenter.model_json(subscription_pricing)
    end

    if side_load?(:multiproduct)
      json[:multiproduct] = multiproduct_presenter.model_json(subscription)
    end

    if side_load?(:zopim_pricing)
      zopim_subscription_pricing = zopim_pricing(subscription.currency_type)
      json[:zopim_pricing] = zopim_subscription_pricing_presenter.model_json(zopim_subscription_pricing)
    end

    if side_load?(:voice_pricing)
      voice_subscription_pricing = voice_pricing(subscription.currency_type)
      json[:voice_pricing] = voice_subscription_pricing_presenter.model_json(voice_subscription_pricing)
    end

    if side_load?(:guide_pricing)
      guide_subscription_pricing = guide_pricing(subscription.currency_type)
      guide_pricing_json         = guide_subscription_pricing_presenter.model_json(guide_subscription_pricing)

      json[:guide_pricing] = begin
        !!subscription.guide_preview.try(:legacy?) ? guide_without_lite_and_professional(guide_pricing_json) : guide_pricing_json
      end
    end

    if side_load?(:tpe_pricing)
      tpe_subscription_pricing = tpe_pricing(subscription.currency_type)
      json[:tpe_pricing] = tpe_subscription_pricing_presenter.model_json(tpe_subscription_pricing)
    end

    if side_load?(:explore_pricing) && Arturo.feature_enabled_for?(:explore_billing, subscription.account)
      explore_subscription_pricing = explore_pricing(subscription.currency_type)
      json[:explore_pricing] = explore_subscription_pricing_presenter.model_json(explore_subscription_pricing)
    end

    if side_load?(:credit_card)
      json[:credit_card] = subscription_credit_card_presenter.model_json(subscription)
    end

    if side_load?(:voice)
      json[:voice] = subscription_voice_presenter.model_json(subscription)
    end

    if side_load?(:partner_settings)
      account = subscription.account
      json[:partner_settings] = partner_settings_presenter.model_json(account)
    end

    if side_load?(:remote_authentications)
      account = subscription.account
      json[:remote_authentications] = remote_authentications_presenter.
        collection_presenter.model_json(account.remote_authentications)
    end

    if side_load?(:zopim_integration)
      zopim_integration = account.zopim_integration
      json[:zopim_integration] = zopim_integration ? zopim_integration_presenter.model_json(zopim_integration) : nil
    end

    if side_load?(:voice_recharge_settings)
      recharge_settings = account.voice_recharge_settings
      json[:voice_recharge_settings] = recharge_settings ? voice_recharge_settings_presenter.model_json(recharge_settings) : nil
    end

    if side_load?(:explore_access) && Arturo.feature_enabled_for?(:explore_billing, subscription.account)
      json[:explore_access] = { explore_lite_available: subscription.account.eligible_for_explore_lite? }
    end

    json
  end

  private

  def subscription_pricing_presenter
    @subscription_pricing_presenter ||= Api::V2::Account::ZendeskSubscriptionPricingPresenter.new(user, options)
  end

  def multiproduct_presenter
    @multiproduct_presenter ||= Api::V2::Account::MultiproductPresenter.new(user, options)
  end

  def zopim_subscription_pricing_presenter
    @zopim_subscription_pricing_presenter ||= Api::V2::Account::ZopimSubscriptionPricingPresenter.new(user, options)
  end

  def voice_subscription_pricing_presenter
    @voice_subscription_pricing_presenter ||= Api::V2::Account::VoiceSubscriptionPricingPresenter.new(user, options)
  end

  def tpe_subscription_pricing_presenter
    @tpe_subscription_pricing_presenter ||= Api::V2::Account::TpeSubscriptionPricingPresenter.new(user, options)
  end

  def voice_recharge_settings_presenter
    @voice_recharge_settings_presenter ||= Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter.new(user, options)
  end

  def guide_subscription_pricing_presenter
    @guide_subscription_pricing_presenter ||= Api::V2::Account::GuideSubscriptionPricingPresenter.new(user, options)
  end

  def explore_subscription_pricing_presenter
    @explore_subscription_pricing_presenter ||= Api::V2::Account::ExploreSubscriptionPricingPresenter.new(user, options)
  end

  def subscription_credit_card_presenter
    @subscription_credit_card_presenter ||= Api::V2::Account::SubscriptionCreditCardPresenter.new(user, options)
  end

  def subscription_voice_presenter
    @subscription_voice_presenter ||= Api::V2::Account::SubscriptionVoicePresenter.new(user, options)
  end

  def zopim_integration_presenter
    @zopim_integration_presenter ||= Api::V2::ZopimIntegrationPresenter.new(user, options)
  end

  def subscription_pricing(subscription)
    @subscription_pricing ||= ZBC::Zuora::ZendeskSubscriptionPricing.new(subscription)
  end

  def zopim_pricing(currency_type)
    @zopim_pricing ||= ZBC::Zuora::ZopimSubscriptionPricing.new(currency_type)
  end

  def voice_pricing(currency_type)
    @voice_pricing ||= ZBC::Zuora::VoiceSubscriptionPricing.new(currency_type)
  end

  def guide_pricing(currency_type)
    @guide_pricing ||= ZBC::Zuora::GuideSubscriptionPricing.new(currency_type)
  end

  def tpe_pricing(currency_type)
    @tpe_pricing ||= ZBC::Zuora::TpeSubscriptionPricing.new(currency_type)
  end

  def answer_bot_pricing(currency_type)
    @answer_bot_pricing ||= ZBC::Zuora::AnswerBotSubscriptionPricing.new(currency_type)
  end

  def outbound_pricing(currency_type)
    @outbound_pricing ||= ZBC::Zuora::OutboundSubscriptionPricing.new(currency_type)
  end

  def explore_pricing(currency_type)
    @explore_pricing ||= ZBC::Zuora::ExploreSubscriptionPricing.new(currency_type)
  end

  def partner_settings_presenter
    @partner_settings_presenter ||= Api::V2::Account::PartnerSettingsPresenter.new(user, options)
  end

  def remote_authentications_presenter
    @remote_authentications_presenter ||= Api::V2::Account::RemoteAuthenticationPresenter.new(user, options)
  end

  def trial_days_remaining(expires_at)
    return -1 unless expires_at
    days = (expires_at.to_date - Date.today).to_i
    days > -1 ? days : -1
  end

  def zopim_max_agents(zopim_subscription, subscription)
    return nil unless zopim_subscription
    if zopim_subscription.is_trial?
      [zopim_subscription.max_agents, subscription.max_agents].min
    else
      zopim_subscription.max_agents
    end
  end

  def trial_or_show_inactive?(explore_subscription)
    is_trial      = !!explore_subscription.try(:trial?)
    is_cancelled  = !!explore_subscription.try(:cancelled?)
    is_expired    = !!explore_subscription.try(:expired?)
    # if explore_purchaseable is included, we want to show explore even if its
    # been cancelled or trial has expired
    show_inactive = side_load?(:explore_purchasable) && (is_cancelled || is_expired)

    is_trial || show_inactive
  end

  def guide_without_enterprise(guide_pricing_json)
    guide_pricing_json[:guide_available_plans].reject! do |plan|
      plan[:plan_type] == ZBC::Guide::PlanType::Enterprise.plan_type
    end
    guide_pricing_json
  end

  def guide_without_lite_and_professional(guide_pricing_json)
    to_remove = [
      ZBC::Guide::PlanType::Lite.plan_type,
      ZBC::Guide::PlanType::Professional.plan_type
    ]

    guide_pricing_json[:guide_available_plans].reject! do |plan|
      to_remove.include?(plan[:plan_type])
    end
    guide_pricing_json
  end
end
