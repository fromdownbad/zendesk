class Api::V2::Account::SecondarySubscriptionsPresenter < Api::V2::Presenter
  self.model_key = :secondary_subscriptions

  ## ### JSON Format
  ##
  ## Secondary subscriptions (handle seasonal agents concept) are represented
  ## as an array of JSON objects in the following format:
  ##
  ## ```json
  ## {
  ##   "secondary_subscriptions": [
  ##     {
  ##       "zuora_rate_plan_id": "123456",
  ##       "product": "temporary_zendesk_agents",
  ##       "quantity": 5,
  ##       "starts_at": "2016-03-22 00:00:00Z"
  ##       "expires_at": "2016-03-22 00:00:00Z"
  ##       "status": "started"
  ##     },
  ##     {
  ##       "zuora_rate_plan_id": "234567",
  ##       "product": "temporary_zendesk_agents",
  ##       "quantity": 2,
  ##       "starts_at": "2016-04-15 00:00:00Z"
  ##       "expires_at": "2016-04-15 00:00:00Z"
  ##       "status": "started"
  ##     },
  ##     ...
  ##   ],
  ##   "next_page": null,
  ##   "previous_page": null,
  ##   "count": 2
  ## }
  ## ```
  def model_json(model)
    {
      zuora_rate_plan_id: model.zuora_rate_plan_id,
      product:            model.name,
      quantity:           model.quantity,
      starts_at:          model.starts_at,
      expires_at:         model.expires_at,
      status:             model.status.try(:to_s)
    }
  end
end
