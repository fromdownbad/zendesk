class Api::V2::CustomFieldPresenter < Api::V2::Presenter
  include Zendesk::CustomField::FieldHelper
  def initialize(user, options = {})
    super(user, options)

    @model = options.fetch(:model)
  end

  def model_key
    (@model.to_s + "_field").to_sym
  end

  def collection_key
    model_key.to_s.pluralize.to_sym
  end

  ## ### JSON Format
  ## Custom fields have the following attributes:
  ##
  ## | Name                  | Type    | Read-only | Mandatory | Comment
  ## | --------------------- | ------- | --------- | --------- | -------
  ## | id                    | integer | yes       | no        | Automatically assigned upon creation
  ## | url                   | string  | yes       | no        | The URL for this resource
  ## | key                   | string  | no        | on create | A unique key that identifies this custom field. This is used for updating the field and referencing in placeholders.
  ## | type                  | string  | no        | yes       | Type of the custom field: "checkbox", "date", "decimal", "dropdown", "integer", "regexp", "text", or "textarea"
  ## | title                 | string  | no        | yes       | The title of the custom field
  ## | raw_title             | string  | no        | no        | The dynamic content placeholder, if present, or the "title" value, if not. See [Dynamic Content](dynamic_content.html)
  ## | description           | string  | no        | no        | User-defined description of this field's purpose
  ## | raw_description       | string  | no        | no        | The dynamic content placeholder, if present, or the "description" value, if not. See [Dynamic Content](dynamic_content.html)
  ## | position              | integer | no        | no        | Ordering of the field relative to other fields
  ## | active                | boolean | no        | no        | If true, this field is available for use
  ## | system                | boolean | yes       | no        | If true, only active and position values of this field can be changed
  ## | regexp_for_validation | string  | no        | no        | Regular expression field only. The validation pattern for a field value to be deemed valid.
  ## | created_at            | date    | yes       | no        | The time the ticket field was created
  ## | updated_at            | date    | yes       | no        | The time of the last update of the ticket field
  ## | tag                   | string  | no        | no        | Optional for custom field of type "checkbox"; not presented otherwise.
  ## | custom_field_options  | array   | no        | yes       | Required and presented for a custom field of type "dropdown"
  ##
  ##
  def model_json(custom_field)
    json = super.merge!(
      id: custom_field.id,
      type: field_type(custom_field),
      key: custom_field.key,
      title: render_dc_or_i18n(custom_field, custom_field.title),
      description: render_dc_or_i18n(custom_field, custom_field.description),
      raw_title: custom_field.title,
      raw_description: custom_field.description,
      position: custom_field.position,
      active: custom_field.is_active?,
      system: custom_field.is_system?,
      regexp_for_validation: custom_field.regexp_for_validation,
      created_at: custom_field.created_at,
      updated_at: custom_field.updated_at
    )

    if custom_field.is_a?(CustomField::Dropdown)
      json[:custom_field_options] = dropdown_options(custom_field)
    end

    if custom_field.is_a?(CustomField::Checkbox)
      json[:tag] = custom_field.tag
    end

    json
  end

  def association_preloads
    { dropdown_choices: {} }
  end

  def url(custom_field)
    url_builder.send("api_v2_#{@model}_field_url", custom_field, format: :json)
  end

  def field_type(custom_field)
    custom_field.type.gsub(/CustomField::/, "").downcase
  end

  def dropdown_options(custom_field)
    custom_field.dropdown_choices.map do |choice|
      next unless choice.active?
      {
        id: choice.id,
        name: render_dc_or_i18n(custom_field, choice.name),
        raw_name: choice.name,
        value: choice.value
      }
    end.compact
  end
end
