class Api::V2::AutomaticAnswerPresenter < Api::V2::Presenter
  self.model_key = :automatic_answer

  def model_json(deflection)
    ticket = tickets_by_id[deflection.ticket_id]

    {
      id: deflection.id,
      brand_id: deflection.brand_id,
      enquiry: deflection.enquiry,
      answer_bot_channel: answer_bot_channel(id: deflection.deflection_channel_id),
      solved_article_id: deflection.solved_article_id,
      state: deflection.state,
      created_at: deflection.created_at,
      updated_at: deflection.updated_at,
      ticket_id: ticket.try(:nice_id),
      user_id: deflection.user_id,
      articles: article_information(deflection: deflection)
    }
  end

  private

  def article_information(deflection:)
    deflection.ticket_deflection_articles.map do |article|
      {
        article_id: article.article_id,
        clicked_at: article.clicked_at,
        locale: article.locale,
        user_marked_unhelpful: article.irrelevant_by_end_user_updated_at,
        agent_marked_unhelpful: article.irrelevant_by_agent_updated_at,
        solved_at:  article.solved_at
      }
    end
  end

  def answer_bot_channel(id:)
    # Guard against a nil value being exposed as `"answer_bot_channel": "Zendesk::Types::ViaType"`
    Zendesk::Types::ViaType.to_s(id) if id
  end

  def preload_associations(deflection)
    deflections = Array(deflection)
    super deflections

    preload_tickets(deflections)
  end

  def preload_tickets(deflections)
    ticket_ids = deflections.map(&:ticket_id)
    @tickets = Ticket.with_deleted { account.tickets.where(id: ticket_ids).all_with_archived }
  end

  def tickets_by_id
    @tickets_by_id ||=
      begin
        @tickets.each_with_object({}) { |ticket, memo| memo[ticket.id] = ticket }
      end
  end
end
