class Api::V2::OnboardingTasksPresenter < Api::V2::Presenter
  def present(onboarding_tasks)
    {
      tasks: add_completion_attrs(onboarding_tasks).map { |it| it.slice('name', 'completed', 'changed') }
    }
  end

  private

  def add_completion_attrs(onboarding_tasks)
    return [] unless onboarding_tasks.is_a?(Array)

    completed_task_names = @user.completed_onboarding_task_names

    onboarding_tasks.map do |onboarding_task|
      onboarding_task['completed'] = completed_task_names.include? onboarding_task['name']
      onboarding_task['changed'] = false

      if !onboarding_task['completed'] && onboarding_task['conditions'].try(:values).try(:all?)
        onboarding_task['completed'] = true
        # we want the 'changed' attribute here to indicate that the condition check is true, but the table doesn't reflect it
        onboarding_task['changed'] = true
      end

      onboarding_task
    end
  end
end
