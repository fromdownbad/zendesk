class Api::V2::AgentBrandPresenter < Api::V2::Presenter
  self.model_key = :brand

  ## For documentation, see app/presenters/api/v2/admin_brand_presenter.rb
  def model_json(brand)
    super.merge!(
      id: brand.id,
      name: brand.name,
      brand_url: brand.url,
      subdomain: brand.subdomain,
      has_help_center: brand.has_help_center?,
      help_center_state: brand.help_center_state || :disabled,
      active: brand.active,
      default: brand.default?,
      is_deleted: brand.deleted?,
      logo: logo(brand),
      ticket_form_ids: brand.ticket_form_ids,
      signature_template: brand.signature_template,
      created_at: brand.created_at,
      updated_at: brand.updated_at
    )
  end

  def association_preloads
    {
      logo: {
        thumbnails: {}
      }
    }
  end

  def side_loads(brand)
    return {} unless side_load?(:ticket_forms)

    brands = Array[brand].flatten
    ticket_forms = brands.flat_map(&:ticket_forms).uniq
    { ticket_forms: ticket_form_presenter.collection_presenter.model_json(ticket_forms) }
  end

  private

  def logo(brand)
    return nil unless brand.logo.present?
    attachment_presenter.model_json(brand.logo)
  end
end
