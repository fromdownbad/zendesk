class Api::V2::RequestPresenter < Api::V2::Presenter
  include Api::V2::Tickets::AttributeMappings
  include Api::V2::Tickets::EventViaPresenter
  include Api::V2::Tickets::TicketFieldHelper

  self.model_key = :request
  self.url_param = :nice_id

  def initialize(*)
    super

    @custom_fields = account.ticket_fields.custom.active.visible_in_portal

    field = account.field_assignee
    @assignee = field.is_active? && field.is_visible_in_portal?
    @group = account.field_group.is_visible_in_portal?
  end

  ## ### JSON Format
  ## Requests are represented as JSON objects with the following properties:
  ##
  ## | Name                | Type                                     | Read-only | Mandatory | Comment
  ## | ----------------    | ---------------------------------------- | --------- | --------- | -------
  ## | id                  | integer                                  | yes       | no        | Automatically assigned when creating requests
  ## | url                 | string                                   | yes       | no        | The API url of this request
  ## | subject             | string                                   | no        | yes       | The value of the subject field for this request if the subject field is visible to end users; a truncated version of the description otherwise
  ## | description         | string                                   | yes       | no        | Read-only first comment on the request. When [creating a request](#create-request), use `comment` to set the description
  ## | status              | string                                   | no        | no        | The state of the request, "new", "open", "pending", "hold", "solved", "closed"
  ## | priority            | string                                   | no        | no        | The priority of the request, "low", "normal", "high", "urgent"
  ## | type                | string                                   | no        | no        | The type of the request, "question", "incident", "problem", "task"
  ## | custom_fields       | array                                    | no        | no        | Custom fields for the request. See [Setting custom field values](./tickets#setting-custom-field-values) in the Tickets doc
  ## | organization_id     | integer                                  | yes       | no        | The organization of the requester
  ## | requester_id        | integer                                  | yes       | no        | The id of the requester
  ## | assignee_id         | integer                                  | yes       | no        | The id of the assignee if the field is visible to end users
  ## | group_id            | integer                                  | yes       | no        | The id of the assigned group if the field is visible to end users
  ## | collaborator_ids    | array                                    | yes       | no        | The ids of users currently CC'ed on the ticket
  ## | email_cc_ids        | array                                    | yes       | no        | The ids of users who are currently email CCs on the ticket. See [CCs and followers resources](https://support.zendesk.com/hc/en-us/articles/360020585233) in the Support Help Center
  ## | via                 | [Via](ticket_audits.html#the-via-object) | yes       | no        | This object explains how the request was created
  ## | is_public           | boolean                                  | yes       | no        | Is true if any comments are public, false otherwise
  ## | due_at              | date                                     | no        | no        | When the task is due (only applies if the request is of type "task")
  ## | can_be_solved_by_me | boolean                                  | yes       | no        | If true, end user can mark request as solved.
  ## | solved              | boolean                                  | no        | no        | Whether or not request is solved (an end user can set this if "can_be_solved_by_me", above, is true for that user)
  ## | ticket_form_id      | integer                                  | no        | no        | The numeric id of the ticket form associated with this request if the form is visible to end users - only applicable for enterprise accounts
  ## | created_at          | date                                     | yes       | no        | When this record was created
  ## | updated_at          | date                                     | yes       | no        | When this record last got updated
  ## | recipient           | string                                   | no        | no        | The original recipient e-mail address of the request
  ## | followup_source_id  | integer                                  | yes       | no        | The id of the original ticket if this request is a follow-up ticket
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                  35436,
  ##   "url":                 "https://company.zendesk.com/api/v2/requests/35436.json",
  ##   "created_at":          "2009-07-20T22:55:29Z",
  ##   "updated_at":          "2011-05-05T10:38:52Z",
  ##   "due_at":              "2011-05-24T12:00:00Z",
  ##   "subject":             "Help, my printer is on fire!",
  ##   "description":         "The fire is very colorful.",
  ##   "status":              "open",
  ##   "priority":            "normal",
  ##   "type":                "problem",
  ##   "organization_id":     509974,
  ##   "assignee_id":         72983,
  ##   "group_id":            8665,
  ##   "requester_id":        1462,
  ##   "collaborator_ids":    [],
  ##   "can_be_solved_by_me": false,
  ##   "ticket_form_id":      2,
  ##   "via": {
  ##     "channel": "web"
  ##   }
  ## }
  ## ```
  def model_json(request)
    json = super.merge!(
      id: request.nice_id,
      status: STATUS_MAP[request.status_id.to_s],
      priority: PRIORITY_MAP[request.priority_id.to_s],
      type: TYPE_MAP[request.ticket_type_id.to_s],
      subject: visible_subject(request),
      description: request.public_description(markdown: true),
      organization_id: request.organization_id,
      via: serialized_via_object(request),
      custom_fields: ticket_field_entries(request, request_custom_ticket_fields(request)),
      requester_id: request.requester_id,
      collaborator_ids: collaborator_ids(request),
      email_cc_ids: email_cc_ids(request),
      is_public: request.is_public,
      due_at: request.due_date,
      can_be_solved_by_me: can_be_solved_by_me?(request),
      created_at: request.created_at,
      updated_at: request.updated_at,
      recipient: request.original_recipient_address || request.recipient
    )

    followup_link = request.followup_source_links.first
    json[:followup_source_id] = followup_link.try(:source_with_archived).try(:nice_id)

    json[:assignee_id] = request.assignee_id if @assignee
    json[:group_id] = request.group_id if @group

    if account.has_ticket_forms?
      json[:ticket_form_id] = request.ticket_form_id unless agent_only_ticket_form?(request)
    end

    if side_load?(:public_updated_at)
      json[:public_updated_at] = public_updated_at(request)
    end

    # deprecated
    json[:fields] = json[:custom_fields]
    json
  end

  def association_preloads
    preload = {
      ticket_field_entries: {},
      requester: {},
      collaborations: {},
      followup_source_links: {
        source: {},
        archived_source: {}
      }
    }

    preload[:assignee] = {} if @assignee
    preload
  end

  def public_updated_at(request)
    request.public_updated_at
  end

  def side_loads(requests)
    requests = Array(requests)
    json = {}

    if side_load?(:users)
      user_ids = requests.map { |request| collaborator_ids(request) }
      user_ids += requests.map { |request| email_cc_ids(request) }
      user_ids += requests.map(&:requester_id)
      user_ids += requests.map(&:assignee_id) if @assignee

      user_ids.flatten!
      user_ids.compact!
      user_ids.uniq!

      users = account.all_users.where(id: user_ids).to_a
      user_presenter.preload_associations(users)
      json[:users] = user_presenter.collection_presenter.model_json(users)
    end

    if side_load?(:organizations)
      organization_ids = requests.map(&:organization_id) + requests.map { |req| req.requester.organization_id }
      organization_ids += requests.map { |req| req.assignee.try(:organization_id) } if @assignee

      # Exclude deleted organizations
      organizations = account.organizations.where(id: organization_ids.compact.uniq)
      json[:organizations] = organization_presenter.collection_presenter.model_json(organizations)
    end

    if side_load?(:groups) && @group
      group_ids = requests.map(&:group_id)

      # Exclude deleted groups
      groups = account.groups.where(id: group_ids.compact.uniq)
      json[:groups] = group_presenter.collection_presenter.model_json(groups)
    end

    if side_load?(:satisfaction_ratings) && account.has_customer_satisfaction_enabled?
      json[:satisfaction_ratings] = side_load_association(requests, :satisfaction_ratings, satisfaction_rating_presenter)
    end

    json
  end

  def collaborator_ids(request)
    request.collaborations.map(&:user_id)
  end

  def email_cc_ids(request)
    if account.has_follower_and_email_cc_collaborations_enabled?
      account.has_comment_email_ccs_allowed_enabled? ? request.email_ccs.map(&:id) : []
    else
      collaborator_ids(request)
    end
  end

  def user_presenter
    @user_presenter ||= options[:user_presenter] ||= Api::V2::Users::RequestPresenter.new(user, options)
  end

  def organization_presenter
    @organization_presenter ||= options[:organization_presenter] ||= Api::V2::Requests::OrganizationPresenter.new(user, options)
  end

  def group_presenter
    @group_presenter ||= options[:group_presenter] ||= Api::V2::Requests::GroupPresenter.new(user, options)
  end

  def satisfaction_rating_presenter
    @satisfaction_rating_presenter ||= options[:satisfaction_rating_presenter] ||= Api::V2::SatisfactionRatingPresenter.new(user, options)
  end

  def request_custom_ticket_fields(request)
    if account.has_ticket_forms? && request.ticket_form.present?
      request.ticket_form.ticket_fields.custom.active.visible_in_portal
    else
      @custom_fields
    end
  end

  def present_errors(model)
    Api::V2::Requests::ErrorsPresenter.new.present(model)
  end

  private

  def can_be_solved_by_me?(request)
    request.assignee_id.present? &&
      request.working? &&
      !request.ticket_type?('problem') &&
      request.requested_by?(user)
  end

  def agent_only_ticket_form?(request)
    request.ticket_form && !request.ticket_form.end_user_visible
  end

  def visible_subject(request)
    subject = account.field_subject.is_visible_in_portal? ? request.subject : request.title(30, true)
    if request.submitter && request.submitter.is_agent?
      subject = render_dynamic_content(subject, ticket: request)
    end
    subject
  end
end
