class Api::V2::Billing::TokenPresenter < Api::V2::Presenter
  self.model_key = :token

  def model_json(token)
    {}.merge(token)
  end
end
