class Api::V2::Billing::ServiceConnectionPresenter < Api::V2::Presenter
  self.model_key = :service_connection

  def model_json(service_connection)
    {}.merge(service_connection)
  end
end
