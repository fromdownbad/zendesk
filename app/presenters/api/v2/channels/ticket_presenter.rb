module Api
  module V2
    module Channels
      class TicketPresenter < ::Api::V2::Presenter
        include ::Api::V2::Tickets::AttributeMappings

        self.model_key = :ticket

        def model_json(ticket)
          super.merge(id: ticket.id,
                      nice_id: ticket.nice_id,
                      status: STATUS_MAP[ticket.status_id.to_s])
        end

        def url(ticket)
          url_builder.send(:api_v2_ticket_url, ticket, format: :json)
        end
      end
    end
  end
end
