module Api
  module V2
    module Channels
      class CommentPresenter < TicketPresenter
        self.model_key = :ticket

        def model_json(comment)
          super(comment.ticket).merge(comment_id: comment.id)
        end
      end
    end
  end
end
