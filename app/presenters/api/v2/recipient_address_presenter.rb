class Api::V2::RecipientAddressPresenter < Api::V2::Presenter
  self.model_key = :recipient_address

  ## ### JSON Format
  ## Support addresses have the following keys:
  ##
  ## | Name                       | Type    | Read-only | Mandatory | Comment
  ## | -------------------------- | ------- | --------- | --------- | -------
  ## | id                         | integer | yes       | no        | Automatically assigned when created
  ## | email                      | string  | no        | yes       | The email address (not updateable)
  ## | name                       | string  | no        | no        | The name for the address
  ## | default                    | boolean | no        | no        | Whether the address is the account's default support address
  ## | brand_id                   | integer | no        | no        | The ID of the [brand](./brands)
  ## | forwarding_status          | string  | yes       | no        | Status of email forwarding. Possible values: "unknown", "waiting", "verified", or "failed"
  ## | spf_status                 | string  | yes       | no        | Whether the SPF record is set up correctly. Possible values: "unknown", "verified", "failed"
  ## | cname_status               | string  | yes       | no        | Whether all of the required CNAME records are set. Possible values: "unknown", "verified", "failed"
  ## | domain_verification_status | string  | yes       | no        | Whether the domain verification record is valid. Possible values: "unknown", "verified", "failed"
  ## | domain_verification_code   | string  | yes       | no        | Verification string to be added as a TXT record to the domain. Possible types: string or null.
  ## | dns_results                | hash    | yes       | no        | Verification statuses for the domain and CNAME records. Possible types: "verified", "failed"
  ## | created_at                 | date    | yes       | no        | When the address was created
  ## | updated_at                 | date    | yes       | no        | When the address was updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                         35436,
  ##   "email":                      "support@omniwear.com",
  ##   "name":                       "all",
  ##   "default":                    true,
  ##   "brand_id":                   123,
  ##   "forwarding_status":          "unknown",
  ##   "spf_status":                 "verified",
  ##   "cname_status":               "verified",
  ##   "domain_verification_status": "verified",
  ##   "dns_results": {
  ##     "verification_method": "cname"
  ##     "cname": {
  ##       "zendesk1": {
  ##         "lookup_result": "mail1.example.com",
  ##         "status": "verified"
  ##       },
  ##       "zendesk2": {
  ##         "lookup_result": "example.com",
  ##         "status": "failed"
  ##       },
  ##       [...]
  ##     }
  ##   }
  ##   "updated_at":                 "2016-09-21T20:15:20Z",
  ##   "created_at":                 "2015-07-20T22:55:29Z"
  ## }
  ## ```
  ##
  ## You can also include the brand for each Support address in the JSON objects returned by GET requests by sideloading it. Example:
  ## `GET /api/v2/recipient_addresses.json?include=brands`
  ##
  def model_json(recipient_address)
    attributes = {
      id: recipient_address.id,
      brand_id: recipient_address.brand_id,
      default: recipient_address.default,
      name: recipient_address.name,
      email: recipient_address.email,
      forwarding_status: recipient_address.forwarding_status,
      spf_status: recipient_address.spf_status,
      cname_status: recipient_address.cname_status,
      domain_verification_status: recipient_address.domain_verification_status,
      domain_verification_code: recipient_address.domain_verification_code,
      created_at: recipient_address.created_at,
      updated_at: recipient_address.updated_at
    }
    attributes[:dns_results] = dns_results(recipient_address) if recipient_address.metadata && recipient_address.account.has_email_dns_results?
    attributes[:domain_verification_code] = recipient_address.domain_verification_code
    attributes[:created_at] = recipient_address.created_at
    attributes[:updated_at] = recipient_address.updated_at

    if recipient_address.external_email_credential_id
      attributes[:external_email_credential_id] = recipient_address.external_email_credential_id
    end

    super.merge!(attributes)
  end

  def side_loads(recipient_address)
    json = {}
    recipient_addresses = Array(recipient_address)

    if side_load?(:brands)
      brands = account.brands.where(id: recipient_addresses.map(&:brand_id).uniq).to_a
      json[:brands] = brands.map { |brand| brand_presenter.model_json(brand) }
    end

    json
  end

  private

  def dns_results(recipient_address)
    if recipient_address.metadata["verification_method"] == "mx"
      {
        cname: recipient_address.metadata["cname"],
        mx: recipient_address.metadata["mx"],
        txt: recipient_address.metadata["txt"]
      }
    else
      {
        cname: recipient_address.metadata["cname"]
      }
    end
  end
end
