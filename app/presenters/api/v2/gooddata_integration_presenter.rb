class Api::V2::GooddataIntegrationPresenter < Api::V2::Presenter
  self.model_key = :gooddata_integration

  ## ### JSON Format
  ## Gooddata integrations are represented as JSON objects which have the following keys:
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | project_id      | string  | yes       | no        | The id of the project in Gooddata
  ## | admin_id        | number  | yes       | no        | The admin of the project
  ## | status          | string  | yes       | no        | The creation status of the project
  ## | scheduled_at    | string  | no        | no        | The time the integration will sync at
  ## | version         | number  | yes       | no        | The creation process version for the integration
  ## | dashboards      | array   | yes       | no        | A list of the project's GoodData dashboards
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":           3456,
  ##   "project_id":   "11k2kk4k98d9sk9876sa",
  ##   "admin_id":     16,
  ##   "status":       "CREATED",
  ##   "scheduled_at": "12am",
  ##   "version":      1,
  ##   "dashboards":   [{ "title": "d1", "link": "https://secure.gooddata.com/link/to/db" }, ...],
  ##   "url":          "https://company.zendesk.com/api/v2/gooddata_integration.json"
  ## }
  ## ```

  def model_json(integration)
    super.merge!(
      id: integration.id,
      project_id: integration.project_id,
      admin_id: integration.admin_id,
      status: integration.status,
      scheduled_at: integration.scheduled_at,
      version: integration.version,
      dashboards: integration.dashboards
    )
  end
end
