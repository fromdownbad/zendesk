class Api::V2::SharingAgreementPresenter < Api::V2::Presenter
  self.model_key = :sharing_agreement

  ## ### JSON Format
  ## Sharing Agreements have the following format:
  ##
  ## | Name              | Type    | Comment
  ## | ----------------- | ------- | -------
  ## | id                | integer | Automatically assigned upon creation
  ## | name              | string  | Name of this sharing agreement
  ## | type              | string  | Can be one of the following: 'inbound', 'outbound'
  ## | status            | string  | Can be one of the following: 'accepted', 'declined', 'pending', 'inactive'
  ## | partner_name      | string  | Can be one of the following: 'jira', null
  ## | remote_subdomain  | string  | Subdomain of the remote account or null if not associated with an account
  ## | created_at        | date    | The time the record was created
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         88335,
  ##   "url":        "https://company.zendesk.com/api/v2/agreements/88335.json",
  ##   "name":       "Ticket Sharing",
  ##   "type":       "inbound",
  ##   "status":     "accepted",
  ##   "created_at": "2012-02-20T22:55:29Z"
  ## }
  ## ```
  def model_json(agreement)
    {
      id: agreement.id,
      name: agreement.name,
      type: agreement.in? ? "inbound" : "outbound",
      status: agreement.status.to_s,
      partner_name: agreement.shared_with_id == 1 ? "jira" : nil,
      remote_subdomain: @remote_accounts ? @remote_accounts[agreement.remote_url].try(:subdomain) : agreement.remote_account.try(:subdomain),
      created_at: agreement.created_at
    }
  end

  # This has a
  #  key === all the remote_url's for the agreements needed for the presenter
  #  value is the account that is associated to the remote_url
  attr_writer :remote_accounts
end
