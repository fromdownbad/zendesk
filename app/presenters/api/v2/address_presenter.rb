class Api::V2::AddressPresenter < Api::V2::Presenter
  self.model_key = :address

  ## ### JSON Format
  ## Addresses are represented in JSON with the below attributes
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | city            | string  | no        | no        | The city of the address
  ## | country         | object  | no        | no        | The country object
  ## | phone           | string  | no        | yes       | The phone of the address
  ## | state           | string  | no        | no        | The state of the address
  ## | street          | string  | no        | no        | The street of the address
  ## | vat             | string  | no        | no        | The vat of the address
  ## | zip             | string  | no        | no        | The zip of the address
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "city": "San Francisco",
  ##   "country": {
  ##     "calling_code": "1",
  ##     "code": "US",
  ##     "id": 156,
  ##     "name": "United States",
  ##     "region": "Americas",
  ##     "url": "http://support.zd-dev.com/api/v2/countries/156.json"
  ##   },
  ##   "phone": "888-670-4887",
  ##   "state": "California",
  ##   "street": "1019 Market St",
  ##   "vat": null,
  ##   "zip": "94103"
  ## }
  ## ```
  def model_json(address)
    country = country_presenter.model_json(address.country) if address.country_id
    {
      street: address.street,
      zip: address.zip,
      city: address.city,
      state: address.state,
      phone: address.phone,
      country: country,
      vat: address.vat
    }
  end

  def country_presenter
    @country_presenter ||= options[:country_presenter] ||= Api::V2::CountryPresenter.new(user, options)
  end
end
