class Api::V2::TargetPresenter < Api::V2::Presenter
  self.model_key = :target
  self.url_param = :id

  ## ### JSON Format
  ## Targets have the following common attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | id              | integer  |           | Automatically assigned when created
  ## | title           | string   | yes       | A name for the target
  ## | type            | string   |           | A pre-defined target, such as "basecamp_target". See the additional attributes for the type that follow
  ## | active          | boolean  |           | Whether or not the target is activated
  ## | created_at      | date     |           | The time the target was created
  ##
  ## "basecamp_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | target_url      | string   | yes       | The URL of your Basecamp account, including protocol and path
  ## | username        | string   |           | The 37Signals username of the account you use to log in to Basecamp
  ## | password        | string   |           | The 37Signals password for the Basecamp account (only writable)
  ## | token           | string   | yes       | Get the API token from My info > Show your tokens > Token for feed readers or the Basecamp API in your Basecamp account
  ## | project_id      | string   | yes       | The ID of the project in Basecamp where updates should be pushed
  ## | resource        | string   | yes       | "todo" or "message"
  ## | message_id      | string   |           | Can be filled if it is a "todo" resource
  ## | todo_list_id    | string   |           | Can be filled if it is a "message" resource
  ##
  ## "campfire_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | subdomain       | string   | yes       |
  ## | ssl             | boolean  |           |
  ## | room            | string   | yes       |
  ## | token           | string   | yes       |
  ## | preserve_format | boolean  |           |
  ##
  ## "clickatell_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | target_url      | string   |           | Read-only
  ## | method          | string   |           | Read-only
  ## | attribute       | string   |           | Read-only
  ## | username        | string   | yes       |
  ## | password        | string   | yes       | only writable
  ## | api_id          | string   | yes       |
  ## | to              | string   | yes       |
  ## | from            | string   |           |
  ## | us_small_business_account | string   | Possible values: "0" or "1" for false or true |
  ##
  ## "email_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | email           | string   | yes       |
  ## | subject         | string   | yes       |
  ##
  ## "flowdock_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | api_token       | string   | yes       |
  ##
  ## "get_satisfaction_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | email           | string   | yes       |
  ## | password        | string   | yes       | only writable
  ## | account_name    | string   | yes       |
  ## | target_url      | string   |           |
  ##
  ## "jira_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | target_url      | string   | yes       |
  ## | username        | string   | yes       |
  ## | password        | string   | yes       | only writable
  ##
  ## "pivotal_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | token           | string   | yes       |
  ## | project_id      | string   | yes       |
  ## | story_type      | string   | yes       |
  ## | story_title     | string   | yes       |
  ## | requested_by    | string   |           |
  ## | owner_by        | string   |           |
  ## | story_labels    | string   |           |
  ##
  ## "twitter_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | token           | string   |           |
  ## | secret          | string   |           | only writable
  ##
  ## "url_target" has the following additional attributes:
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | target_url      | string   | yes       |
  ## | method          | string   |           | "get"
  ## | attribute       | string   | yes       |
  ## | username        | string   |           |
  ## | password        | string   |           | only writable
  ##
  ## "http_target" has the following additional attributes:
  ##
  ##
  ## | Name            | Type     | Mandatory | Comment
  ## | --------------- | ---------| --------- | -------
  ## | target_url      | string   | yes       |
  ## | method          | string   | yes       | "get", "patch", "put", "post", or "delete"
  ## | username        | string   |           |
  ## | password        | string   |           | only writable
  ## | content_type    | string   | yes       | "application/json", "application/xml", or "application/x-www-form-urlencoded"
  ##
  ## "yammer_target" has the following additional attributes:
  ##
  ## | Name               | Type     | Mandatory | Comment
  ## | ------------------ | ---------| --------- | -------
  ## | group_id           | string   |           |
  ## | token              | string   |           |
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         88335,
  ##   "url":        "https://company.zendesk.com/api/v2/targets/88335.json",
  ##   "created_at": "2012-02-20T22:55:29Z",
  ##   "active":     false,
  ##   "type":       "basecamp_target"
  ## }
  ## ```

  def model_json(target)
    additional_attributes = Hash[target.class.setting_writers.concat(target.class.encrypted_setting_writers).map { |k| [k, target.send(k)] }] # with nil's
    additional_attributes[:target_url] = additional_attributes.delete(:url) if additional_attributes.key?(:url)
    Target::SECRET_SETTINGS.each do |setting|
      additional_attributes[setting] = nil if additional_attributes.key?(setting)
    end

    super.merge!(
      id: target.id,
      created_at: target.created_at,
      type: Zendesk::Targets::Initializer.new(account: account, via: :api).target_types_map.key(target.class),
      title: target.title,
      active: target.is_active
    ).merge!(additional_attributes)
  end
end
