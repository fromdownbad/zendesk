class Api::V2::Abilities::UserPresenter < Api::V2::Presenter
  self.model_key = :abilities

  def initialize(actor, options = {})
    super(user, options)
    @actor = actor
    @account = options[:account]
  end

  ## ### JSON Format
  ## Users are represented as JSON objects which have the following keys:
  ##
  ## | Name                        | Type      | Read-only | Mandatory | Comment
  ## | --------------------------- | --------- | --------- | --------- | -------
  ## | user_id                     | integer   | yes       | no        | The user ID of the subject.
  ## | can_edit                    | boolean   | yes       | no        | When true, the current user may edit the subject user account.
  ## | can_edit_password           | boolean   | yes       | no        | When true, the current user may change the password of the subject user account.
  ## | can_manage_identities_of    | boolean   | yes       | no        | When true, the current user may manage identities associated with the subject user account.
  ## | can_verify_identities       | boolean   | yes       | no        | When true, the current user may verify identities associated with the subject user account.
  ## | can_reset_password          | boolean   | yes       | no        | When true, the current user may request a password reset.
  ## | can_set_password            | boolean   | yes       | no        | When true, the current user may set a new password without providing their current password.
  ## | can_create_password         | boolean   | yes       | no        | When true, the current user may request a password create.
  ## | can_change_password         | boolean   | yes       | no        | When true, the current user may change their own password, provided they have the current password.
  ## | can_send_verification_email | boolean   | yes       | no        | When true, the current user may send a verification e-mail.
  ## | can_verify_now              | boolean   | yes       | no        | When true, the current user may immediately verify an identity.
  ## | can_make_comment_private    | boolean   | yes       | no        | When true, the current user may mark a ticket comment as private.
  ## | can_edit_agent_forwarding   | boolean   | yes       | no        | When true, the current user may manage forwarding number associated with the subject agent account.
  ## | can_modify_user_tags        | boolean   | yes       | no        | When true, the current user may modify the tags of the user.
  ## | can_view_views              | boolean   | yes       | no        | When true, the subject user may view views.
  ## | can_manage_people           | boolean   | yes       | no        | When true, the subject user may edit a group and an org of the user.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "user_id":                     100,
  ##   "can_edit":                    true,
  ##   "can_edit_password":           false,
  ##   "can_manage_identities_of":    true,
  ##   "can_verify_identities":       false,
  ##   "can_reset_password":          false,
  ##   "can_set_password":            true,
  ##   "can_create_password":         false,
  ##   "can_change_password":         false,
  ##   "can_edit_agent_forwarding":   true,
  ##   "can_make_comment_private":    true,
  ##   "can_modify_user_tags":        true
  ##  }
  ## ```
  def model_json(user)
    @account ||= user.account

    super.merge!(
      user_id:                     user.id,
      can_edit:                    @actor.can?(:edit, user),
      can_edit_password:           @actor.can?(:edit_password, user),
      can_manage_identities_of:    @actor.can?(:manage_identities_of, user),
      can_verify_identities:       @actor.can?(:manually_verify_identities_of, user),
      can_reset_password:          @actor.can?(:request_password_reset, user),
      can_set_password:            @actor.can?(:set_password, user),
      can_create_password:         @actor.can?(:create_password, user),
      can_change_password:         @actor.can?(:change_password, user),
      can_set_alias:               @actor.can?(:set_agent_display_name, user),
      can_send_verification_email: @actor.can?(:send_verification_email, user),
      can_verify_now:              @actor.can?(:verify_now, user),
      can_make_comment_private:    @actor.can?(:make_comment_private, Comment),
      can_edit_agent_forwarding:   @actor.can?(:edit_agent_forwarding, user),
      can_modify_user_tags:        @actor.can?(:modify_user_tags, user),
      can_assume:                  @actor.can?(:assume, user),
      can_delete:                  @actor.can?(:delete, user),
      can_view_views:              user.can?(:view, View),
      can_view_reports:            @actor.can?(:view, Report),
      can_export:                  can_export?(@actor),
      can_use_voice_console:       can_use_voice_console?(user),
      voice_enabled_account:       user.account.voice_enabled?,
      can_use_voice:               user.can?(:accept, ::Voice::Call),
      can_view_voice_dashboard:    user.can?(:view_dashboard, ::Voice::Call),
      can_manage_people:           @actor.can?(:manage_people, user)
    )
  end

  def can_export?(user)
    export_configuration = Zendesk::Export::Configuration.new(@account)
    export_configuration.any_accessible? && export_configuration.whitelisted?(user)
  end

  def can_use_voice_console?(user)
    return false if user.is_end_user?
    return false unless @account.voice_enabled?

    if @account.voice_feature_enabled?(:user_seats)
      user.user_seats.any?(&:voice_seat?) # Rely on pre-loading rather than fetching the DB
    else
      user.can?(:accept, Voice::Call)
    end
  end

  def url(user)
    url_builder.send(:api_v2_user_url, user, format: :json) unless user.is_anonymous_user?
  end
end
