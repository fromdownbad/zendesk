class Api::V2::Abilities::OrganizationPresenter < Api::V2::Presenter
  self.model_key = :abilities

  def initialize(actor, options = {})
    super(actor, options)
    @actor = actor
  end

  ## ### JSON Format
  ## Users are represented as JSON objects which have the following keys:
  ##
  ## | Name                      | Type      | Read-only | Mandatory | Comment
  ## | ------------------------- | --------- | --------- | --------- | -------
  ## | organization_id           | integer   | yes       | no        | The organization ID of the subject.
  ## | can_edit_users            | boolean   | yes       | no        | When true, the current user may edit user profiles in the same organization.
  ## | can_access_tickets        | boolean   | yes       | no        | When true, the current user may access tickets within the organization.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "organization_id":        100,
  ##   "can_edit_users":         true,
  ##   "can_access_tickets":     false
  ##  }
  ## ```
  def model_json(org)
    super.merge!(
      organization_id:    org.id,
      can_edit_users:     can_edit_users?,
      can_access_tickets: can_access_tickets?
    )
  end

  def can_edit_users?
    @actor.is_admin? ||
      (@actor.has_permission_set? && @actor.permission_set.permissions.end_user_profile != 'readonly')
  end

  def can_access_tickets?
    @actor.agent_restriction?(:organization) || @actor.agent_restriction?(:none)
  end

  def url(org)
    url_builder.send(:api_v2_organization_url, org, format: :json)
  end
end
