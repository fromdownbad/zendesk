class Api::V2::TicketFormPresenter < Api::V2::Presenter
  self.model_key = :ticket_form

  ## ### JSON Format
  ## Ticket Forms are represented as JSON objects which have the following keys:
  ##
  ## | Name                 | Type    | Read-only | Mandatory | Comment
  ## | -------------------- | ------- | --------- | --------- | -------
  ## | name                 | string  | no        | yes       | The name of the form
  ## | raw_name             | string  | no        | no        | The dynamic content placeholder, if present, or the "name" value, if not. See [Dynamic Content](dynamic_content.html)
  ## | display_name         | string  | no        | no        | The name of the form that is displayed to an end user
  ## | raw_display_name     | string  | no        | no        | The dynamic content placeholder, if present, or the "display_name" value, if not. See [Dynamic Content](dynamic_content.html)
  ## | position             | integer | no        | no        | The position of this form among other forms in the account, i.e. dropdown
  ## | active               | boolean | no        | no        | If the form is set as active
  ## | end_user_visible     | boolean | no        | no        | Is the form visible to the end user
  ## | default              | boolean | no        | no        | Is the form the default form for this account
  ## | ticket_field_ids     | array   | no        | no        | ids of all ticket fields which are in this ticket form. The products use the order of the ids to show the field values in the tickets. See [Field limits](#field-limits)
  ## | in_all_brands        | boolean | no        | no        | Is the form available for use in all brands on this account
  ## | restricted_brand_ids | array   | yes       | no        | ids of all brands that this ticket form is restricted to
  ## | agent_conditions     | array   | no        | no        | Array of condition sets for agent workspaces
  ## | end_user_conditions  | array   | no        | no        | Array of condition sets for end user products
  ##
  ## Warning: Sending an empty array `[]` for `agent_conditions` or `end_user_conditions` will delete the conditions.
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":                    47,
  ##   "url":                   "https://company.zendesk.com/api/v2/ticket_forms/47.json",
  ##   "name":                  "Snowboard Problem",
  ##   "raw_name":              "Snowboard Problem",
  ##   "display_name":          "Snowboard Damage",
  ##   "raw_display_name":      "{{dc.my_display_name}}",
  ##   "end_user_visible":      true,
  ##   "position":              9999,
  ##   "ticket_field_ids":      [ 2, 4, 5, 10, 100, 101, 102, 200 ],
  ##   "active":                true,
  ##   "default":               true,
  ##   "in_all_brands":         false,
  ##   "restricted_brand_ids":  [ 47, 33, 22 ],
  ##    "agent_conditions": [
  ##      {
  ##        "parent_field_id": 100,
  ##        "value": "matching_value",
  ##        "child_fields": [
  ##          {
  ##            "id": 101,
  ##            "is_required": false,
  ##            "required_on_statuses": {
  ##              "type": "SOME_STATUSES",
  ##              "statuses": ["new", "open", "pending", "hold"]
  ##            }
  ##          },
  ##          {
  ##            "id": 200,
  ##            "is_required": true,
  ##            "required_on_statuses": {
  ##              "type": "SOME_STATUSES",
  ##              "statuses": ["solved"]
  ##            }
  ##          }
  ##        ]
  ##      },
  ##      {
  ##        "parent_field_id": 101,
  ##        "value": "matching_value_2",
  ##        "child_fields": [
  ##          {
  ##            "id": 102,
  ##            "is_required": true,
  ##            "required_on_statuses": {
  ##              "type": "ALL_STATUSES",
  ##            }
  ##          },
  ##          {
  ##            "id": 200,
  ##            "is_required": false,
  ##            "required_on_statuses": {
  ##              "type": "NO_STATUSES",
  ##            }
  ##          }
  ##        ]
  ##      }
  ##    ],
  ##    "end_user_conditions": [
  ##      {
  ##        "parent_field_id": 100,
  ##        "value": "matching_value",
  ##        "child_fields": [ {"id": 101, "is_required": true } ]
  ##      },
  ##      {
  ##        "parent_field_id": 200,
  ##        "value": "matching_value",
  ##        "child_fields": [ {"id": 202, "is_required": false } ]
  ##      }
  ##    ],
  ##   "created_at":            "2012-04-02T22:55:29Z",
  ##   "updated_at":            "2012-04-02T22:55:29Z"
  ## }
  ## ```

  def model_json(ticket_form)
    attributes = {
      id: ticket_form.id,
      name: display_or_translate(ticket_form.name),
      raw_name: ticket_form.name,
      display_name: display_or_translate(ticket_form.display_name),
      raw_display_name: ticket_form.display_name,
      end_user_visible: ticket_form.end_user_visible,
      position: ticket_form.position,
      ticket_field_ids: ticket_field_ids(ticket_form),
      active: ticket_form.active,
      default: ticket_form.default,
      created_at: ticket_form.created_at,
      updated_at: ticket_form.updated_at
    }

    if account.has_multibrand?
      attributes[:in_all_brands] = ticket_form.in_all_brands
      attributes[:restricted_brand_ids] = ticket_form.restricted_brand_ids
    end

    if account.has_native_conditional_fields_enabled?
      attributes[:end_user_conditions] = ticket_form.end_user_conditions
      if user.is_agent?
        attributes[:agent_conditions] = ticket_form.agent_conditions
      end
    end

    super.merge!(attributes)
  end

  def ticket_field_ids(ticket_form)
    ticket_fields_for([ticket_form]).map(&:id)
  end

  # "ticket_forms.flat_map(&:ticket_fields).uniq" sometimes causes multiple
  # INNER JOIN queries and the N+1 tests fail.
  # This approach is more reliable and does a multistep eager loading in all cases.
  def ticket_fields_for(ticket_forms)
    ticket_fields = ticket_forms.flat_map(&:ticket_form_fields).map(&:ticket_field).compact.uniq

    if user.is_agent?
      ticket_fields
    else
      ticket_fields.find_all do |tf|
        tf.is_active? && tf.is_editable_in_portal? && tf.is_visible_in_portal?
      end
    end
  end

  def association_preloads
    {
      restricted_brands: {},
      ticket_form_fields: {
        ticket_field: {}
      },
      ticket_field_conditions: {}
    }
  end

  def side_loads(ticket_form)
    payload = {}

    ticket_forms = Array[ticket_form].flatten
    if side_load?(:brands)
      brands = brands_for(ticket_forms)
      payload[:brands] = brand_presenter.collection_presenter.model_json(brands)
    end

    if side_load?(:ticket_fields)
      ticket_fields = ticket_fields_for(ticket_forms)
      payload[:ticket_fields] = ticket_field_presenter.collection_presenter.model_json(ticket_fields)
    end

    payload
  end

  private

  def brands_for(ticket_forms)
    if account.has_fix_brands_n_plus_one_in_ticket_forms? &&
        ticket_forms.any?(&:in_all_brands)
      account.active_brands
    else
      ticket_forms.flat_map(&:brands).uniq
    end
  end

  def display_or_translate(field_text)
    render_dynamic_content(field_text)
  end
end
