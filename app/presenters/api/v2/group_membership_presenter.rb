class Api::V2::GroupMembershipPresenter < Api::V2::Presenter
  self.model_key = :group_membership

  ## ### JSON Format
  ## Memberships are simple links between a user and a group
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | url             | string  | yes       | no        | The API url of this record
  ## | user_id         | integer | no        | yes       | The id of an agent
  ## | group_id        | integer | no        | yes       | The id of a group
  ## | default         | boolean | no        | no        | If true, tickets assigned directly to the agent will assume this membership's group.
  ## | created_at      | date    | yes       | no        | The time the membership was created
  ## | updated_at      | date    | yes       | no        | The time of the last update of the membership
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":         4,
  ##   "user_id":    29,
  ##   "group_id":   12,
  ##   "default":    true,
  ##   "created_at": "2009-05-13T00:07:08Z",
  ##   "updated_at": "2011-07-22T00:11:12Z"
  ## }
  ## ```
  def model_json(membership)
    super.merge!(
      id: membership.id,
      user_id: membership.user_id,
      group_id: membership.group_id,
      default: membership.default,
      created_at: membership.created_at,
      updated_at: membership.updated_at
    )
  end

  def side_loads(membership)
    json = {}
    memberships = Array(membership)
    full_json = account.has_api_v2_group_membership_full_sideload?

    if side_load?(:users)
      users = account.all_users.
        where(id: memberships.map(&:user_id).uniq).
        includes(:identities).
        to_a

      json[:users] = users.map { |user| full_json ? user_presenter.model_json(user) : user_presenter.condensed_model_json(user) }
    end

    if side_load?(:groups)
      groups = account.groups.where(id: memberships.map(&:group_id).uniq).to_a

      json[:groups] = groups.map do |group|
        full_json ? group_presenter.model_json(group) : group_presenter.condensed_model_json(group)
      end
    end

    json
  end
end
