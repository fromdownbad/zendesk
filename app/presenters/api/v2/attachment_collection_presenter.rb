class Api::V2::AttachmentCollectionPresenter < Api::V2::CollectionPresenter
  # Temporarily override attachment collections everywhere so that inline
  # attachments are excluded from API payloads.
  #
  # https://zendesk.atlassian.net/browse/AI-3912
  #
  # This class, along with the reference in
  # Api::V2::AttachmentPresenter#collection_presenter, can be removed once API
  # clients can distinguish between inline and regular attachments.
  def model_json(collection)
    if options[:include_inline_images]
      super collection
    else
      super collection.reject(&:inline?)
    end
  end
end
