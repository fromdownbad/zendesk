class Api::V2::TicketActivityPresenter < Api::V2::Presenter
  self.model_key = :activity
  self.url_param = :id

  ## ### JSON Format
  ## Ticket activities are represented as JSON objects with the following keys:
  ##
  ## | Name             | Type     | Comment
  ## | ---------------- | -------- | -------
  ## | id               | integer  | Automatically assigned on creation
  ## | url              | string   | The API url of the activity
  ## | title            | string   | Description of the activity
  ## | verb             | string   | The type of activity. Can be "tickets.assignment", "tickets.comment", or "tickets.priority_increase"
  ## | user_id          | integer  | The id of the agent making the request
  ## | actor_id         | integer  | The id of user responsible for the ticket activity
  ## | object           | object   | The content of the activity. Can be a ticket, comment, or change
  ## | target           | object   | The target of the activity, a ticket.
  ## | user             | [User](./users) | The full user record of the agent making the request
  ## | actor            | [User](./users) | The full user record of the user responsible for the ticket activity
  ## | created_at       | date     | When the record was created
  ## | updated_at       | date     | When the record was last updated
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":          35,
  ##   "url":         "https://company.zendesk.com/api/v2/activities/35.json",
  ##   "verb":        "tickets.assignment",
  ##   "title":       "John Hopeful assigned ticket #123 to you",
  ##   "user_id":      29451,
  ##   "actor_id":     23546,
  ##   "created_at":  "2019-03-05T10:38:52Z",
  ##   "updated_at":  "2019-03-05T10:38:52Z",
  ##   "object":      { ... },
  ##   "target":      { ... },
  ##   "user":        { ... },
  ##   "actor":       { ... }
  ## }
  ## ```
  def model_json(activity)
    return unless activity.activity_target
    return unless activity.activity_object

    super.merge!(
      id: activity.id,
      title: activity.title,
      verb: activity.verb,
      user_id: activity.user_id,
      actor_id: activity.actor_id,
      updated_at: activity.updated_at,
      created_at: activity.created_at,
      object: activity_object(activity),
      target: activity_target(activity),
      # Deprecated
      user: user_presenter.model_json(activity.user),
      actor: user_presenter.model_json(activity.actor)
    )
  end

  def association_preloads
    {
      activity_object: {},
      activity_target: {},
      # Deprecated
      actor: user_presenter.association_preloads,
      user: user_presenter.association_preloads
    }
  end

  def side_loads(activities)
    activities = Array(activities).select(&:activity_object)

    {
      actors: side_load_association(activities, :actor, user_presenter),
      users: side_load_association(activities, :user, user_presenter)
    }
  end

  private

  def activity_object(activity)
    case activity.verb
    when "tickets.assignment"
      { ticket: { id: activity.activity_object.nice_id, subject: activity.activity_object.title(120) } }
    when "tickets.comment"
      { comment: { value: activity.activity_object.body, public: activity.activity_object.is_public } }
    when "tickets.priority_increase"
      { change: { value_previous: PriorityType[activity.activity_object.value_previous].to_s, value: PriorityType[activity.activity_object.value].to_s } }
    end
  end

  def activity_target(activity)
    { ticket: { id: activity.activity_target.nice_id, subject: activity.activity_target.title(120) } }
  end
end
