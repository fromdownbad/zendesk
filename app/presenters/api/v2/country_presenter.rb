class Api::V2::CountryPresenter < Api::V2::Presenter
  include CountryHelper

  self.model_key = :country

  def initialize(user, options = {})
    super
  end

  ## ### JSON Format
  ## Countries are represented in JSON with the below attributes
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | yes       | Automatically assigned upon creation
  ## | name            | string  | yes       | yes       | The name of the country
  ## | code            | string  | yes       | yes       | The country's code
  ## | region          | string  | yes       | no        | The region of the country
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              156,
  ##   "name":            "United States",
  ##   "code":            "US",
  ##   "region":          "Americas"
  ##   "calling_code":    "1"
  ## }
  ## ```
  def model_json(country)
    super.merge(
      id: country.id,
      name: localized_name(country, locale),
      code: country.code,
      region: country.region,
      calling_code: country.calling_code
    )
  end

  private

  def locale
    I18n.locale
  end
end
