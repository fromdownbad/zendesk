class Api::V2::SubscriptionPresenter < Api::V2::Presenter
  self.model_key = :subscription

  ## ### JSON Format
  ## Subscriptions are links between users and sources(Forums, Topics, etc.) they subscribe to
  ##
  ## | Name            | Type    | Read-only | Mandatory | Comment
  ## | --------------- | ------- | --------- | --------- | -------
  ## | id              | integer | yes       | no        | Automatically assigned upon creation
  ## | user_id         | integer | no        | yes       | The user subscribed to the source
  ## | source_id       | integer | no        | yes       | The id of the source
  ## | source_type     | string  | yes       | yes       | The type of source being subscribed to (forum, topic)
  ## | created_at      | date    | yes       | no        | The time the subscription was created
  ##
  ## #### Example
  ## ```js
  ## {
  ##   "id":              35436,
  ##   "user_id":         482,
  ##   "source_id":       47,
  ##   "source_type":     "Forum",
  ##   "created_at":      "2009-07-20T22:55:29Z"
  ## }
  ## ```
  def model_json(subscription)
    {
      id: subscription.id,
      user_id: subscription.user_id,
      source_id: subscription.source_id,
      source_type: subscription.source_type,
      created_at: subscription.created_at
    }
  end

  def side_loads(subscriptions)
    json = {}

    if side_load?(:topics)
      topic_subscriptions = subscriptions.select { |s| s.source_type == Entry.name }
      side_load_topics(json, topic_subscriptions)
      topics = topic_subscriptions.map(&:source)
      json[:forums] = load_topic_parent_forums(json, topics)
    end

    if side_load?(:forums)
      forum_subscriptions = subscriptions.select { |s| s.source_type == Forum.name }
      side_load_forums(json, forum_subscriptions)
    end

    if side_load?(:organization)
      organization_subscriptions = subscriptions.select { |s| s.source_type == Organization.name }
      side_load_forums(json, organization_subscriptions)
    end

    json
  end

  def side_load_forums(json, forum_subscriptions)
    json[:forums] ||= []
    json[:forums] << side_load_association(forum_subscriptions, :source, forum_presenter)
    json[:forums].flatten!
    json[:forums].uniq!
  end

  def side_load_topics(json, topic_subscriptions)
    presenter = topic_presenter
    presenter.options[:includes] = []
    json[:topics] = side_load_association(topic_subscriptions, :source, presenter)
  end

  # TODO: Remove unused code.
  def side_load_organizations(json, organization_subscriptions)
    presenter = organization_presenter
    presenter.options[:includes] = []
    json[:topics] = side_load_association(organization_subscriptions, :source, presenter)
  end

  def load_topic_parent_forums(_json, topics)
    side_load_association(topics, :forum, forum_presenter)
  end
end
