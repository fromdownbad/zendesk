# Used by Mobile Core SDK boot process
# Mobile Support SDK 1.X uses `Api::Mobile::Sdk::SettingsPresenter`
class Api::Private::MobileSdk::SettingsPresenter < Api::V2::Presenter
  self.model_key = :mobile_sdk

  ## ### Format
  ## Presents the settings for a Mobile SDK app
  ##
  ## ### Core
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | identifier              | string  | The app's unique identifier
  ## | authentication          | string  | The app's authentication type
  ## | brand_id                | integer | The brand assigned to the app
  ## | updated_at              | string  | The updated_at stamp for the app and all it's settings
  ##
  ## ### Rate My App
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | enabled                 | boolean | If Rate My App is enabled
  ## | visits                  | integer | Number of visits before showing the RMA dialog
  ## | duration                | integer | The time (in days) it will take before showing the RMA dialog
  ## | delay                   | integer | The delay (in seconds) after we will show the RMA dialog when the conditions to show were met
  ## | tags                    | array   | Tags to be attached to the ticket
  ## | ios_store_url           | string  | iOS App Store URL of the app
  ## | android_store_url       | string  | Android Store URL of the app
  ##
  ## ### Support
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | conversations           | object  | Conversations
  ## | contact_us              | object  | Contact Us
  ## | attachments             | object  | Attachments
  ## | ticket_forms            | object  | Ticket Forms
  ## | csat                    | object  | Satisfaction ratings
  ##
  ## ### Conversations
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | enabled                 | boolean | If Conversations is enabled
  ##
  ## ### Contact Us
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | tags                    | array   | Tags to be attached to the ticket created
  ##
  ## ### Attachments
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | enabled                 | boolean | If Attachments is enabled
  ## | max_attachment_size     | bigint  | Maximum attachment size
  ##
  ## ### Ticket Forms
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | available               | boolean | If Ticket Forms is available
  ##
  ## ### Help Center
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | enabled                 | boolean | If Help Center is enabled
  ## | locale                  | string  | Localization iso code with small case
  ##
  ## ### Connect
  ##
  ## | Name                    | Type    | Comment
  ## | ----------------------- | ------- | -------
  ## | enabled                 | boolean | If Connect is enabled
  ##
  ## #### Example
  ##
  ## ```js
  ## {
  ##     "core": {
  ##         "identifier": "2ee572c3a63d90e2d9bfdc3ed8ba8b5710c1ef17e234ca1e",
  ##         "authentication": "anonymous",
  ##         "brand_id": 696178,
  ##         "updated_at": "2016-06-21T17:25:56Z"
  ##     },
  ##     "rma": {
  ##         "enabled": false,
  ##         "visits": 15,
  ##         "duration": 7,
  ##         "delay": 3,
  ##         "tags": [],
  ##         "ios_store_url": "",
  ##         "android_store_url": ""
  ##     },
  ##     "support": {
  ##         "conversations": {
  ##             "enabled": true
  ##         },
  ##         "contact_us": {
  ##             "tags": []
  ##         },
  ##         "attachments": {
  ##             "enabled": true,
  ##             "max_attachment_size": 20971520
  ##         },
  ##         "ticket_forms": {
  ##             "available": false
  ##         },
  ##         "never_request_email": false,
  ##         "show_closed_requests": true,
  ##         "show_referrer_logo": true,
  ##         "system_message": "Thank you for getting in touch. We will be contacting you as soon as possible.",
  ##         "referrer_url": "",
  ##         "csat": {
  ##             "enabled": true
  ##         }
  ##     },
  ##     "help_center" : {
  ##         "enabled": true,
  ##         "locale": "ja"
  ##     },
  ##     "connect" : {
  ##         "enabled": true,
  ##     },
  ##     "blips" : {
  ##        "permissions" : {
  ##          "required": true,
  ##          "behavioural": false,
  ##          "pathfinder": true
  ##        },
  ##       "collect_only_required_data": false
  ##     },
  ##     "answer_bot": {
  ##       "enabled": true
  ##     }
  ## }
  ## ```
  def model_json(mobile_sdk_app)
    payload = {
      core: core_settings(mobile_sdk_app),
      rma: rma_settings(mobile_sdk_app),
      support: support_settings(mobile_sdk_app),
      help_center: help_center_settings(mobile_sdk_app),
      blips: blips_settings(mobile_sdk_app),
      answer_bot: answer_bot_settings(mobile_sdk_app)
    }

    if mobile_sdk_app.account.has_connect_sdk?
      payload[:connect] = connect_settings(mobile_sdk_app)
    end

    payload
  end

  def as_json(model)
    model_json(model)
  end

  private

  def core_settings(mobile_sdk_app)
    {
      identifier: mobile_sdk_app.identifier,
      authentication: mobile_sdk_app.settings.authentication,
      brand_id: mobile_sdk_app.brand_id,
      updated_at: mobile_sdk_app.settings.pluck(:updated_at).append(mobile_sdk_app.updated_at).max
    }
  end

  def rma_settings(mobile_sdk_app)
    {
      enabled: mobile_sdk_app.settings.rma_enabled,
      visits: mobile_sdk_app.settings.rma_visits,
      duration: mobile_sdk_app.settings.rma_duration,
      delay: mobile_sdk_app.settings.rma_delay,
      tags: mobile_sdk_app.settings.rma_tags,
      ios_store_url: mobile_sdk_app.settings.rma_ios_store_url,
      android_store_url: mobile_sdk_app.settings.rma_android_store_url
    }
  end

  def support_settings(mobile_sdk_app)
    {
      conversations: conversations_settings(mobile_sdk_app),
      contact_us: contact_us_settings(mobile_sdk_app),
      attachments: attachments_settings(mobile_sdk_app),
      ticket_forms: ticket_forms_settings(mobile_sdk_app),
      never_request_email: mobile_sdk_app.support_never_request_email?,
      show_closed_requests: mobile_sdk_app.support_show_closed_requests?,
      show_referrer_logo: mobile_sdk_app.support_show_referrer_logo?,
      system_message: mobile_sdk_app.support_system_message(locale, user),
      referrer_url: MobileSdkApp::REFERRER_URL,
      csat: csat_settings(mobile_sdk_app)
    }
  end

  def conversations_settings(mobile_sdk_app)
    {
      enabled: mobile_sdk_app.conversations_enabled?
    }
  end

  def contact_us_settings(mobile_sdk_app)
    {
      tags: mobile_sdk_app.settings.contact_us_tags
    }
  end

  def attachments_settings(mobile_sdk_app)
    {
      enabled: mobile_sdk_app.account.is_attaching_enabled,
      max_attachment_size: mobile_sdk_app.account.max_attachment_size
    }
  end

  def ticket_forms_settings(mobile_sdk_app)
    {
      available: mobile_sdk_app.account.has_ticket_forms?
    }
  end

  def help_center_settings(mobile_sdk_app)
    {
      enabled: mobile_sdk_app.settings.helpcenter_enabled,
      locale: mobile_sdk_app.help_center_settings ? mobile_sdk_app.help_center_settings[:locale] : nil,
      help_center_article_voting_enabled: mobile_sdk_app.settings.help_center_article_voting_enabled
    }
  end

  def connect_settings(mobile_sdk_app)
    {
      enabled: mobile_sdk_app.settings.connect_enabled
    }
  end

  def blips_settings(mobile_sdk_app)
    json = {
      permissions: {
         required: mobile_sdk_app.required_blips_enabled?,
         behavioural: mobile_sdk_app.behavioural_blips_enabled?,
         pathfinder: mobile_sdk_app.pathfinder_blips_enabled?
      }
    }

    if mobile_sdk_app.account.has_mobile_sdk_blip_collection_control?
      json[:collect_only_required_data] = mobile_sdk_app.settings.collect_only_required_data
    end

    json
  end

  def answer_bot_settings(mobile_sdk_app)
    {
      enabled: mobile_sdk_app.answer_bot_enabled?
    }
  end

  def csat_settings(mobile_sdk_app)
    {
      enabled: mobile_sdk_app.support_show_csat?
    }
  end

  protected

  def locale
    options[:locale] || user.translation_locale || account.translation_locale || ENGLISH_BY_ZENDESK
  end
end
