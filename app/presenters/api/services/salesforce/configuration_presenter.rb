class Api::Services::Salesforce::ConfigurationPresenter < Api::V2::Presenter
  self.model_key = :config_settings

  def model_json(configuration)
    { objects: configuration[:objects] }
  end
end
