class Api::Services::Salesforce::ObjectsPresenter < Api::V2::Presenter
  self.model_key = :objects

  def model_json(object)
    {
      api_name: object["name"],
      label: object["label"]
    }
  end
end
