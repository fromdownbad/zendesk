require 'biz'

module CIA
  class AttributeChangePresenter
    SIMPLE = ["max_agents", "title", "time_zone", "signature_template", "subdomain", "host_mapping", "email", "name", "workweek", "email_list", "include_messages", "include_posts_by_page", "include_wall_posts", "description"].freeze

    include ActionView::Helpers::SanitizeHelper
    delegate :current_account, :current_user, :link_to, :link_to_function, to: :view_context # check if app/presenters/api/v2/audit_log_presenter.rb has access to newly added methods
    delegate :attribute_name, :source, :source_type, :event, to: :attribute_change

    def initialize(view_context, attribute_change)
      self.view_context     = view_context
      self.attribute_change = attribute_change
    end

    def description
      return unless CIA::AttributeChange::VISIBLE_ATTRIBUTES.include?(attribute_name)

      case attribute_name
      when "agent_security_policy_id" then security_policy_id_description
      when "attribute_values" then attribute_values_description
      when "billing_cycle_type" then billing_cycle_type_description
      when "crypted_password" then description_without_details
      when "default" then ticket_form_default_description
      when "definition" then definition_description
      when "end_user_security_policy_id" then security_policy_id_description
      when "frequency" then notification_frequency_description
      when "description", "rate_limit", "ticket_id" then generic_change_description
      when "is_trial", "is_active", "is_sender", "authorized", "opted_in", "active", "voice_trial" then boolean_description
      when "owner_id" then owner_id_description
      when "permission_set_id" then permission_set_id_description
      when "plan_type" then plan_type_description
      when "position" then position_description
      when "roles" then roles_description
      when "settings" then apps_settings
      when "start_date", "end_date" then date_description
      when "start_time", "end_time" then interval_endpoint_description
      when "state" then state_description
      when "trial_expires_on" then trial_expires_on_description
      when "value" then source.audit_description(self)
      when "voice_deleted_at" then voice_deleted_at
      when "voice_plan_type" then voice_plan_type_description
      when *SIMPLE then translated_description
      else
        raise "#{attribute_name} was not found in the when clauses above. Please add one for it."
      end
    rescue => e
      ZendeskExceptions::Logger.record(e, location: ReportsHelper, message: "attribute_change #{attribute_change.id} failed to generate report_audit_change_description for User with id: #{current_user.id} from #{attribute_name} with old value: #{old_value.inspect} and new value: #{new_value.inspect}", fingerprint: '55370d128c11c91a3fe112a5cf68b25633a45991')
      error_description
    end

    def multiline_description(old, new)
      I18n.t(
        "txt.admin.views.reports.tabs.audits.change_multiline",
        old: audit_value(old),
        new: audit_value(new)
      )
    end

    def audit_value_not_set
      I18n.t("txt.admin.views.reports.tabs.audits.no_set")
    end

    def old_value
      translated_value(attribute_change.old_value)
    end

    def new_value
      translated_value(attribute_change.new_value)
    end

    private

    attr_accessor :attribute_change, :view_context

    def translated_value(value)
      return nil unless value
      if source_type == 'AccountSetting' && source.try(:name) == 'session_timeout'
        return session_timeout_value(value)
      end

      value
    end

    def session_timeout_value(value)
      if value == '20160'
        I18n.t('txt.security_policy.session_timeout.2_weeks')
      elsif value == '480'
        I18n.t('txt.security_policy.session_timeout.8_hours')
      else
        I18n.t('txt.admin.helpers.time_date_helper.minutes.other', count: value.to_i)
      end
    end

    def format_date(*args)
      view_context.send(:format_date, *args)
    end

    def generic_change_description
      I18n.t('txt.admin.views.reports.tabs.audits.change',
        attribute: attribute_change.attribute_name,
        old: audit_value(old_value.to_s),
        new: audit_value(new_value.to_s))
    end

    def apps_settings
      old = JSON.parse(old_value)
      new = JSON.parse(new_value)

      old.keys.map do |key|
        next unless old.key?(key) && new.key?(key)
        I18n.t('txt.admin.views.reports.tabs.audits.change',
          attribute: key,
          old: audit_value(old[key].to_s),
          new: audit_value(new[key].to_s))
      end.compact.join_preserving_safety("<br/>")
    end

    def permission_set_id_description
      translated_description do |value|
        current_account.permission_sets.find_by_id(value).try(:name)
      end
    end

    def owner_id_description
      case source_type
      when "Rule"    then rule_owner_description
      when "Account" then account_owner_description
      end
    end

    def interval_endpoint_description
      translated_description do |value|
        interval_format_time(Biz::WeekTime.build(value)).strftime(
          current_account.week_time_format_string
        )
      end
    end

    def date_description
      translated_description do |value|
        hydrated_date(value).strftime(I18n.t('date.formats.long'))
      end
    end

    def account_owner_description
      translated_description do |value|
        current_account.users.find_by_id(value).try(:name)
      end
    end

    def attribute_values_description
      attribute_values = JSON.parse(new_value)

      I18n.t(
        "txt.admin.views.reports.tabs.audits.change_only_show_new",
        attribute: I18n.t('txt.admin.views.reports.tabs.audits.attribute.attribute_values'),
        new: attribute_values.empty? ? I18n.t('txt.admin.views.reports.tabs.audits.no_set') : attribute_values.join(', ')
      )
    end

    def rule_owner_description
      return audit_value_not_set unless source
      return error_description unless owner = source.owner

      new_owner = I18n.t(
        "txt.admin.helpers.reports_helper.model_attribute_combinator",
        model: owner.class.to_s,
        # All possible owners, Group, User and Account respond to "name"
        attribute: owner.name
      )

      I18n.t(
        "txt.admin.views.reports.tabs.audits.change_only_show_new",
        attribute: I18n.t("txt.admin.views.reports.tabs.audits.attribute.#{attribute_name}"),
        new: new_owner
      )
    end

    def notification_frequency_description
      translated_description { |v| I18n.t(SuspendedTicketNotification::Frequency.find(v.to_i).first) }
    end

    def state_description
      case source_type
      when 'Facebook::Page' then facebook_page_description
      when 'MonitoredTwitterHandle' then monitored_twitter_handle_description
      else generic_state_description
      end
    end

    def facebook_page_description
      translated_description do |value|
        Facebook::Page::STATE_TEXT[value.to_i]
      end
    end

    def monitored_twitter_handle_description
      translated_description do |value|
        MonitoredTwitterHandle::STATE_TEXT[value.to_i]
      end
    end

    def generic_state_description
      translated_description(&:to_s)
    end

    def description_without_details
      I18n.t(
        "txt.admin.helpers.reports_helper.model_attribute_combinator",
        model: I18n.t("txt.admin.views.reports.tabs.audits.attribute.#{attribute_name}"),
        attribute: I18n.t('txt.admin.views.reports.tabs.audits.change_without_details')
      )
    end

    def definition_description
      title_html = multiline_description(*deserialize_definition_change)

      link_to_function(description_without_details, nil, rel: "tooltip", title: title_html)
    end

    def deserialize_definition_change
      [old_value, new_value].map do |value|
        rule_definition_changes_for(value).compact.join("; ")
      end
    end

    def error_description
      I18n.t('txt.admin.views.reports.tabs.audits.error')
    end

    def billing_cycle_type_description
      translated_description do |value|
        BillingCycleType[value.to_i].try(:localized_name)
      end
    end

    def plan_type_description
      case source_type
      when 'ZendeskBillingCore::Zuora::Subscription' then support_plan_type_description
      when 'ZendeskBillingCore::Zopim::Subscription' then chat_plan_type_description
      else translated_description
      end
    end

    def chat_plan_type_description
      translated_description { |value| !value.nil? ? value.camelize : 'Not Set' }
    end

    def support_plan_type_description
      translated_description do |value|
        ZBC::Zendesk::PlanType.plan_name(
          current_account.subscription.pricing_model_revision,
          value.to_i
        ) if value
      end
    end

    def voice_plan_type_description
      translated_description(&:camelize)
    end

    def voice_deleted_at
      I18n.t("txt.admin.views.reports.tabs.audits.property.object.voice_deleted_at") unless new_value.to_s.empty?
    end

    def roles_description
      translated_description { |value| value.to_i.to_s == value ? Role.find(value.to_i).display_name : value }
    end

    def security_policy_id_description
      translated_description do |value|
        Zendesk::SecurityPolicy.all.find { |policy| policy.id == value.to_i }.try(:display_name)
      end
    end

    def trial_expires_on_description
      translated_description do |value|
        format_date(value.to_time) if value.present?
      end
    end

    def boolean_description
      translated_description do |value|
        if value == "1"
          I18n.t("txt.admin.views.reports.tabs.audits.is_trial.active")
        else
          I18n.t("txt.admin.views.reports.tabs.audits.is_trial.inactive")
        end
      end
    end

    def translated_description
      old, new = [old_value, new_value].map { |value| block_given? ? yield(value) : value }

      I18n.t(
        "txt.admin.views.reports.tabs.audits.change",
        attribute: I18n.t("txt.admin.views.reports.tabs.audits.attribute.#{attribute_name}"),
        old: audit_value(old),
        new: audit_value(new)
      )
    end

    def position_description
      translated_description(&:to_s)
    end

    def ticket_form_default_description
      prev = prev_default_form

      if new_value == "1"
        I18n.t("txt.admin.views.reports.tabs.audits.attribute.set_as_default", previous: prev != event.source_display_name ? prev : nil)
      end
    end

    def prev_default_form
      CIA::AttributeChange.
        where(account_id: current_account.id,
              source_type: "TicketForm",
              attribute_name: "default",
              old_value: "0").
        map(&:event).
        select { |e| e.created_at < event.created_at }.
        sort_by(&:created_at).
        last.
        try(:source_display_name)
    end

    def audit_value(text)
      text = I18n.t(text) if text.present? && I18n.exists?(text)
      ERB::Util.h(strip_tags(text)).presence || audit_value_not_set
    end

    def rule_definition_changes_for(value)
      return [audit_value_not_set] if value.blank?

      log_entries = JSON.parse(value)

      Definition::DEFINITION_ITEM_SETS.each_with_index.map do |scope, index|
        next unless entries = log_entries[index].presence

        I18n.t(
          "txt.admin.helpers.reports_helper.model_attribute_combinator",
          model: I18n.t("txt.admin.views.reports.tabs.audits.definition.#{scope}"),
          attribute: definition_items_to_sentence(scope, entries)
        )
      end
    end

    def definition_items_to_sentence(scope, entries)
      deserialized_entries = deserialize_definition_items(entries)

      if scope == :conditions_any
        deserialized_entries.to_sentence(
          two_words_connector: I18n.t("support.array.alternative.two_words_connector"),
          last_word_connector: I18n.t("support.array.alternative.last_word_connector")
        )
      else
        deserialized_entries.to_sentence
      end
    end

    def deserialize_definition_items(entries)
      entries.map { |entry| definition_item_from(entry) }
    end

    def definition_item_from(entry)
      source, operator, value = entry.split(" ", 3)

      unless RuleDictionary::OPERATORS.keys.include?(operator)
        value    = [operator, value].join(" ")
        operator = nil
      end

      DefinitionItem.new(source, operator, value).humanize(current_account).join(" ")
    end

    def interval_format_time(week_time)
      Time.new(2006, 1, 1, week_time.hour, week_time.minute, week_time.second)
    end

    def hydrated_date(value)
      if value.include?('-')
        Date.iso8601(value)
      else
        Date.new(2006, 1, 1) + value.to_i
      end
    end
  end
end
