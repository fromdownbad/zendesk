module CIA
  class EventPresenter
    PROPERTY_SET_SETTINGS = %w[AccountSetting AccountText UserSetting SubscriptionFeature].freeze
    MESSAGE_TYPE_WHITELIST = %w[login].freeze

    delegate :link_to, to: :view_context # check if app/presenters/api/v2/audit_log_presenter.rb has access to newly added methods
    delegate :account, :actor, :actor_type, :actor_id, :attribute_changes,
      :id, :ip_address, :source_type, :source_display_name, :source_id, :message, to: :audit_event

    def initialize(view_context, audit_event)
      self.audit_event  = audit_event
      self.view_context = view_context
      @reported = false
    end

    def action
      if property_set_setting?
        # all changes to properties need to be marked as updates
        "update"
      elsif soft_deleted?
        "destroy"
      else
        audit_event.action
      end
    end

    def action_label
      translation("txt.admin.views.reports.tabs.audits.event.#{action}")
    end

    def actor_link
      if actor
        user_link
      elsif actor_type && actor_id
        generic_user_link
      else
        unknown_user_link
      end
    end

    def date
      data = link_to_audit_search(
        format_date(audit_event.created_at),
        created_at: [audit_event.created_at.at_beginning_of_day.to_s(:db), audit_event.created_at.end_of_day.to_s(:db)]
      )
      data << " " << format_time(audit_event.created_at)
    end

    def ip_address_text
      link_to_audit_search(ip_address, ip_address: ip_address) if show_ip_address?
    end

    def source_link
      link_to_audit_search(
        ActionController::Base.helpers.sanitize(source_label),
        source_type: CIA::Event.inexact_source_type(source_type).underscore,
        source_id: source_id
      )
    end

    def source_label
      if source_display_name.present?
        if source_type == "User"
          source_display_name
        else
          display_value = if source_type == "View"
            dc_renderer.render(source_display_name)
          else
            source_display_name
          end
          translate_model_with_attribute(translated_source, CGI.escapeHTML(display_value))
        end
      elsif source_type == 'SubscriptionFeature'
        safe_source_name
      else
        translated_source
      end
    end

    def english_source_label
      if source_display_name.present?
        if source_type == "User"
          source_display_name
        else
          display_value = if source_type == "View"
            dc_renderer(ENGLISH_BY_ZENDESK)
            dc_renderer.render(source_display_name)
          else
            source_display_name
          end
          translate_model_with_attribute(english_only_source, CGI.escapeHTML(display_value))
        end
      elsif source_type == 'SubscriptionFeature'
        english_safe_source_name
      else
        english_only_source
      end
    end

    def change_descriptions
      return [] unless show_change_descriptions?
      return [I18n.t(audit_event.message, locale: default_locale)] if audit_event.action == "audit"
      desc = attribute_changes.map do |attribute_change|
        CIA::AttributeChangePresenter.new(view_context, attribute_change).description
      end
      desc << change_description_message if message_type_audit_event?
      desc.compact
    end

    private

    attr_accessor :audit_event, :view_context

    def source
      audit_event.source
    rescue NameError, ActiveRecord::SubclassNotFound
      nil
    end

    def format_date(*args)
      view_context.send(:format_date, *args)
    end

    def format_time(*args)
      view_context.send(:format_time, *args)
    end

    def show_change_descriptions?
      return true if audit_event.status_bits > 0 # Override for audit-log service
      !%w[create destroy].include?(action)
    end

    def user_link
      if actor.is_system_user?
        system_user_link
      else
        normal_user_link
      end
    end

    def generic_user_link
      link_to_audit_search(
        translate_model_with_attribute(translated_actor, actor_id),
        actor_link_options
      )
    end

    def unknown_user_link
      I18n.t('txt.admin.helpers.reports_helper.unknown_user', locale: default_locale)
    end

    def system_user_link
      link_to_audit_search(
        I18n.t('txt.admin.helpers.reports_helper.system_user', locale: default_locale),
        actor_link_options
      )
    end

    def normal_user_link
      if actor.is_active?
        active_user_link
      else
        deleted_user_link
      end
    end

    def active_user_link
      link_to_audit_search(actor.safe_name(true), actor_link_options)
    end

    def deleted_user_link
      I18n.t('txt.admin.helpers.reports_helper.deleted_user', username: active_user_link, locale: default_locale)
    end

    def actor_link_options
      {
        actor_type: actor_type,
        actor_id: actor_id
      }
    end

    def translated_source
      key_base = 'txt.admin.views.reports.tabs.audits'
      key = if property_set_setting? && source.try(:name)
        "#{key_base}.property.object.#{source.name}"
      else
        if source_type == 'RoleSettings'
          "#{key_base}.model.#{source_type.underscore}_v2"
        else
          "#{key_base}.model.#{source_type.underscore}"
        end
      end

      report_missing_source

      translation(key)
    end

    def english_only_source
      key_base = 'txt.admin.views.reports.tabs.audits'
      key = if property_set_setting? && source.try(:name)
        "#{key_base}.property.object.#{source.name}"
      else
        if source_type == 'RoleSettings'
          "#{key_base}.model.#{source_type.underscore}_v2"
        else
          "#{key_base}.model.#{source_type.underscore}"
        end
      end
      report_missing_source

      translation(key, ENGLISH_BY_ZENDESK)
    end

    def property_set_setting?
      PROPERTY_SET_SETTINGS.include? source_type
    end

    def soft_deleted?
      attribute_changes.find do |attribute_change|
        attribute_change.attribute_name == "deleted_at" &&
        attribute_change.old_value.blank? &&
        attribute_change.new_value.present?
      end
    end

    def translated_actor
      translation("txt.admin.views.reports.tabs.audits.model.#{actor_type.underscore}")
    end

    def show_ip_address?
      !actor.try(:is_system_user?) && ip_address.present?
    end

    def translate_model_with_attribute(model, attribute)
      I18n.t(
        "txt.admin.helpers.reports_helper.model_attribute_combinator",
        model: model,
        attribute: attribute,
        locale: default_locale
      )
    end

    def link_to_audit_search(text, options)
      link_to(text, filter: options)
    end

    def default_locale
      CIA.current_actor ? CIA.current_actor.locale : account.translation_locale
    end

    def dc_renderer(locale = account.translation_locale)
      @dc_renderer ||= Zendesk::DynamicContent::Renderer.new(account, locale)
    end

    def translation(key, locale = default_locale)
      translation = I18n.t(key, locale: locale)

      # TODO: remove once Staff audits source type issue is fixed
      # Currently, source_type for those audits have the format "Staff: [Customer name]", which generate translation keys
      # like `txt.admin.views.reports.tabs.audits.model.staff: customer name`. Ignore them until issue is fixed.
      staff_audit = audit_event.source_type && audit_event.source_type.start_with?('Staff: ')
      if translation.start_with?('translation missing:') && audit_event.visible? && !staff_audit
        tags = ["action:#{action}", "key:#{key}", "source_type:#{source_type}"]
        statsd_client.increment('translation_missing', tags: tags)
      end

      translation
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[cia_event_presenter])
    end

    def safe_source_name
      report_missing_source
      source.try(:name) || I18n.t('txt.admin.views.reports.tabs.audits.model.no_source', locale: default_locale)
    end

    def english_safe_source_name
      audit_event.source.try(:name) || translation('txt.admin.views.reports.tabs.audits.model.no_source', ENGLISH_BY_ZENDESK)
    end

    def report_missing_source
      return if @reported ||
        source ||
        audit_event.action == "destroy" ||
        audit_event.created_at < 3.months.ago ||
        soft_deleted?

      @reported = true
      Rails.logger.warn("MissingSourceError: #{audit_event.inspect}")
    end

    def message_type_audit_event?
      return true if MESSAGE_TYPE_WHITELIST.include?(audit_event.action) && account.has_audit_logs_api_include_login_events?
      # status-bits == 1 means just output message in raw format regardless of audit type
      # status-bits == 2 means message contains a JSON blob defining the translation key and params
      return true if [1, 2].include?(audit_event.status_bits)
    end

    def change_description_message
      case audit_event.status_bits
      when 1
        message
      when 2
        begin
          parsed_message = JSON.parse(message, symbolize_names: true)
          translate_message(parsed_message)
        rescue StandardError => ex
          Rails.logger.warn("CIA::change_message invalid JSON for message: #{ex} - #{message} - #{parsed_message}")
          tags = ["action:#{action}", "source_type:#{source_type}"]
          statsd_client.increment('invalid_audit_message', tags: tags)
          nil
        end
      else
        message
      end
    end

    def translate_message(parsed_message)
      pm = parsed_message.each_with_object({}) do |(k, v), obj|
        obj[k] = if k == :key
          v
        else
          i18n_or_passthrough(v)
        end
      end

      i18n_message(pm)
    end

    def i18n_or_passthrough(text)
      if text.present? && I18n.exists?(text)
        I18n.t(text, locale: default_locale)
      else
        text
      end
    end

    def i18n_message(parsed_message)
      I18n.t(
        parsed_message[:key],
        parsed_message.merge(locale: default_locale)
      )
    end
  end
end
