module FormHelper
  def self.included(_base)
    ActionView::Helpers::FormBuilder.instance_eval do
      include FormBuilderMethods
    end
  end

  def email_field_tag(name, value = nil, options = {})
    tag(:input, { "type" => "email", "name" => name, "id" => sanitize_to_id(name), "value" => value }.update(options.stringify_keys))
  end

  def email_field(object_name, method, options = {})
    ActionView::Helpers::Tags::EmailField.new(object_name, method, self, options).render
  end

  def url_field(object_name, method, options = {})
    ActionView::Helpers::Tags::UrlField.new(object_name, method, self, options).render
  end

  def number_field(object_name, method, options = {})
    ActionView::Helpers::Url::NumberField.new(object_name, method, self, options).render
  end

  module FormBuilderMethods
    def email_field(method, options = {})
      @template.send('email_field', @object_name, method, objectify_options(options))
    end

    def url_field(method, options = {})
      @template.send('email_field', @object_name, method, objectify_options(options))
    end

    def number_field(method, options = {})
      @template.send('number_field', @object_name, method, objectify_options(options))
    end

    def tagger_nested_select(tagger, options = {})
      current_value = nested_select_value(tagger, options)

      content =
        @template.content_tag('span',
          reverse_truncate(current_value, 26),
          class: 'drop link', style: 'cursor:default;text-decoration:underline;',
          id: "title-tagger-#{tagger.id}") +
        build_nested_select(tagger, current_account: options[:current_account], current_user: options[:current_user], dc_cache: options[:dc_cache])

      @template.content_tag('ul', @template.content_tag('li', content), :class => 'drop-list field-tagger', 'data-field-id' => tagger.id) +
      @template.hidden_field_tag("ticket[fields][#{tagger.id}]", options[:selected], id: "ticket_fields_#{tagger.id}", required: options[:required])
    end

    def static_nested_select(tagger, options = {})
      current_value = nested_select_value(tagger, options)
      @template.content_tag('span', reverse_truncate(current_value, 26))
    end

    def build_nested_select(tagger, options = {})
      current_account = options[:current_account]
      if current_account.present?
        tags = [{'id' => '', 'title' => I18n.t('txt.drop_down.no_selection')}]

        tagger.custom_field_options.each do |option|
          title = Zendesk::Liquid::DcContext.render(option["name"], current_account, options[:current_user], Mime[:text].to_s, nil, options[:dc_cache])
          tags << { 'id' => option["value"], 'title' => title }
        end
      else
        tags = [{'id' => '', 'title' => I18n.t('txt.drop_down.no_selection')}] + tagger.custom_field_options.map { |option| {'id' => option["value"], 'title' => option["name"]} }
      end
      @template.content_tag('ul',
        Zendesk::Utils::NestedCategoryBuilder.new(tags).render,
        class: 'first-drop ', id: "drop-list-tagger-#{tagger.id}")
    end

    def nested_select_value(tagger, options = {})
      current_account = options[:current_account]
      if options[:selected].blank?
        I18n.t('txt.drop_down.no_selection')
      else
        tagging = tagger.custom_field_options.detect { |option| option["value"] == options[:selected] }
        if tagging.present?
          if current_account.present?
            Zendesk::Liquid::DcContext.render(tagging["name"], current_account, options[:current_user], 'text/plain', nil, options[:dc_cache])
          else
            tagging["name"]
          end
        else
          '-'
        end
      end
    end

    def reverse_truncate(string, length)
      string.reverse.truncate(length || 30).reverse
    end
  end
end
