module UpsellHelper
  def has_feature?(feature) # rubocop:disable Naming/PredicateName
    current_account.subscription.send("has_#{feature}?")
  end

  def should_show_upsell?(feature)
    !has_feature?(feature)
  end

  def upsell_view_types(rule_type)
    if !current_account.subscription.has_personal_rules?
      I18n.t('txt.admin.helpers.upsell_helper.upgrade_to_get_group_' + rule_type)
    elsif !current_account.subscription.has_group_rules?
      I18n.t('txt.admin.helpers.upsell_helper.upgrade_to_get_personal_' + rule_type)
    elsif current_account.subscription.has_personal_rules? && current_account.subscription.has_group_rules?
      I18n.t('txt.admin.helpers.upsell_helper.upgrade_to_get_personal_and_group_' + rule_type)
    end
  end
end
