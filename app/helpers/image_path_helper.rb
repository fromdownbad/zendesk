module ImagePathHelper
  def photo_path(user, options = {})
    if user.photo
      cname_wrap(user.account, user.photo_path(options[:version]))
    else
      case options[:version]
      when nil
        path = 'frame_user.jpg'
        size = 79
      when :thumb
        path = '2016/default-avatar-80.png'
        size = 31
      else
        raise "unknown version #{options[:version].inspect}"
      end

      relative = "/images/#{path}"
      if user.account.has_disable_external_services? || !user.account.settings.have_gravatars_enabled?
        cname_wrap(user.account, relative)
      else
        # hardcode assets because we need gravatar to always have access to the default image
        default = "#{request.protocol}assets.zendesk.com#{relative}"
        Zendesk::Gravatar.url(user, default: default, size: size, ssl: request.ssl?) || cname_wrap(user.account, relative)
      end
    end
  end

  def photo_thumb_path_for_email(user)
    if user.photo
      user.large_photo_url
    else
      Zendesk::Gravatar.url(user, size: 40, ssl: true) || user.large_photo_url
    end
  end

  def favicon_link(account, options = {})
    path = favicon_path(account, options.delete(:icon_path))
    type = favicon_content_type(account)
    tag(:link, options.merge(
      type: type,
      href: cname_wrap(account, path),
      rel: 'shortcut icon'
    ))
  end

  def header_logo_url(account)
    if account && account.header_logo
      File.join("#{request.protocol}#{request.host_with_port}", account.header_logo.path_for_url)
    else
      image_url('/images/header-logo.png')
    end
  end

  # Path to the logo to be displayed when both help center and web_portal are disabled
  def neutral_logo_path(account)
    if account && account.header_logo
      account.header_logo.path_for_url
    else
      image_url("/images/zendesk_logo.svg")
    end
  end

  def help_center_favicon(account)
    if user_portal_state(account).show_help_center_favicon?
      path = '/hc/favicon'
      type = 'image/png'
    else
      path = cname_wrap(account, favicon_path(account))
      type = favicon_content_type(account)
    end

    "<link rel='shortcut icon' href='#{path}' type='#{type}'/>".html_safe
  end

  def help_center_logo(account)
    if user_portal_state(account).show_help_center_logo?
      url = '/hc'
      image_path = '/hc/logo'
      logo_size = nil
    else
      url = '/access'
      image_path = neutral_logo_path(account)
      logo_size = "109x30"
    end

    content_tag(:a, href: url, title: t('txt.help_center.helpers.application.home')) do
      image_tag(image_path, size: logo_size, id: 'logo', alt: 'Logo').html_safe
    end
  end

  def mobile_logo_path(account)
    cname_wrap(account, (account.nil? || account.header_logo.nil?) ? "/images/header-logo.png" : account.header_logo.path_for_url)
  end

  def logo_path(account)
    cname_wrap(account, account.account_logo.nil? ? "/images/yourlogohere.png" : account.account_logo.path_for_url)
  end

  def attachment_thumb_path(attachment)
    if attachment.thumbnailed?
      "/attachments/show/#{attachment.thumbnail_ids.first}"
    else
      image_url("/images/defaults/#{ICON_FOR_TYPE[attachment.content_type]}")
    end
  end

  def header_logo_image(account)
    image = image_tag(header_logo_url(account), id: 'logo', alt: '')
    if account.address && account.address.website_url.present?
      link_to image, account.address.website_url, title: '', target: '_blank'
    else
      image
    end
  end

  def link_to_attachment(attachment, with_thumbnail = false)
    link_content = if with_thumbnail
      tag("img", src: attachment_thumb_path(attachment), title: "#{attachment.display_filename} (#{attachment.is_public? ? '' : 'not '}public)", alt: attachment.display_filename, class: attachment.thumbnailed? ? 'thumbnailed' : 'default-icon')
    else
      attachment.display_filename
    end
    link_to(link_content, attachment.url(relative: true), target: '_blank')
  end

  private

  def favicon_content_type(account)
    type = 'image/x-icon'

    if account && account.favicon
      type = account.favicon.content_type
    end

    type
  end

  def favicon_path(account, icon_path = nil)
    if account && account.favicon
      account.favicon.path_for_url
    else
      icon_path || image_url('/images/favicon_2.ico')
    end
  end
end
