module LotusBrandingHelper
  def lotus_branding_active?
    current_account.settings.activate_lotus_branding
  end

  def custom_branding_body_class
    # Notably the old green color indicates default theming even though
    # the default color changed from "green" to "algae" in October 2016.
    if header_color != default_zendesk_green
      palette.light_font? ? "custom-branding-dark" : "custom-branding-light"
    end
  end

  def header_color
    @header_color ||= begin
      use_default_header_color? ? default_header_color : current_account.branding.header_color
    end
  end

  # I *think* this is intended to exclude Talk & Chat UI from Lotus' branding colors
  def use_default_header_color?
    ['chat', 'talk'].include? request.params['action']
  end

  def secondary_branding_color
    @secondary_branding_color ||= palette.secondary_color
  end

  def navbar_bg_color_rgb
    @navbar_bg_color_rgb ||= palette.navbar_bg_color_rgb
  end

  def tab_hover_color
    @tab_hover_color ||= palette.tab_hover_color
  end

  def contrast_color
    @contrast_color ||= palette.contrast_color
  end

  def header_is_light?
    palette.light_header?
  end

  # Old default color (until "Voltron", October 2016)
  def default_zendesk_green
    "78A300"
  end

  # Post-Voltron default color (since October 2016)
  def default_zendesk_algae
    '03363D'
  end

  def default_header_color
    default_zendesk_algae
  end

  private

  def palette
    @palette ||= Zendesk::Branding::Palette.new(current_account.branding)
  end
end
