module SatisfactionRatingsHelper
  def satisfaction_rating_form_url(ticket, token)
    if token.present?
      ticket_satisfaction_via_token_path(ticket_id: @token.ticket.nice_id, token: @token.value)
    else
      ticket_satisfaction_path(ticket_id: ticket.nice_id)
    end
  end

  def right_to_left?
    !!TranslationLocale.find_by_language_tag(I18n.locale).try(:rtl?)
  end

  def reason_disabled?(reason, brand)
    reason.deleted? || reason.deactivated?(brand)
  end

  def disabled_reason_options(ticket)
    reason_disabled?(ticket.satisfaction_reason, ticket.brand) ? [ticket.satisfaction_reason.reason_code] : []
  end
end
