require 'zendesk/account_cdn'
require 'uri'

module LotusBootstrapHelper
  ACCOUNT_SIDELOADS = %i[
    subscription
    deployments
    hc_settings
    branding
    secondary_subscriptions
  ].freeze

  TICKET_SIDELOADS = %i[
    brands
    permissions
    users
    groups
    organizations
    sharing_agreements
    incident_counts
  ].freeze

  def render_and_instrument(*args, &block)
    name = args.first.sub('lotus_bootstrap/preloads/', '')

    statsd_client.time('render_preload', tags: ["preload:#{name}"]) do
      render(*args, &block)
    end
  end

  def preload_cache(key_data, options = {}, &block)
    return unless block_given?

    version = options.delete(:version) || :v0
    options = { compress: true }.merge(options)

    key = ActiveSupport::Cache.expand_cache_key([key_data, version, :json])

    # http://guides.rubyonrails.org/caching_with_rails.html
    Rails.cache.fetch(key, options, &block)
  end

  def insert_json(presenter_class, data, options = {})
    json = nil
    realtime = Benchmark.realtime do
      json = preload_data(presenter_class, data, options).to_json
      json.gsub!('</', '<\/')
      json.gsub!('<!--', '&lt;!--')
      json.gsub!('-->', '--&gt;')
      json = json.html_safe
    end

    logger.info "LotusBootstrapHelper#insert_json #{presenter_class.name} #{realtime}"
    json
  end

  def preload_data(presenter_class, data, options)
    overwrite = options.delete(:overwrite) || {}
    default_options = { url_builder: self }
    default_options.merge!(options)
    presenter = presenter_class.new(current_user, default_options)

    presenter.present(data).tap do |result|
      overwrite.each { |k, v| result[k] = v unless result[k].nil? }
    end
  end

  def static_asset_host
    if request.params[:fallback_cdn]
      ENV.fetch('ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN')
    else
      ENV.fetch('ZENDESK_STATIC_ASSETS_DOMAIN')
    end
  end

  def static_cdn_provider
    request.params[:fallback_cdn] ? 'static-cloudfront' : cdn_provider
  end

  def cdn_provider
    current_account.cdn_provider
  end

  def requested_manifest_version_information
    manifest_manager.version_information(asset_manifest).to_json.html_safe
  end

  def javascript_asset_tag(asset, options = {})
    tag_options = options.reverse_merge(
      src: javascript_path(asset),
      crossorigin: :anonymous,
      onerror: "Zendesk.onScriptLoadError(this, '#{options[:name]}')".html_safe
    )

    if request.params[:fallback_cdn]
      tag_options[:onerror] = "Zd.onFatalError('cdn_failure', '#{javascript_path(asset)}', '#{options[:name]}')".html_safe
    end

    content_tag :script, nil, tag_options
  end

  def translations_asset_tag
    tag_options = {
      src:     translations_asset_tag_src,
      defer:   true,
      onerror: "Zd.onFatalError('translations_failure', '#{translations_asset_tag_src}', 'translations')".html_safe
    }

    if current_account.has_translations_from_cdn? && translations_asset_fallback_to_api_param.blank?
      tag_options[:crossorigin] = :anonymous
      tag_options[:onerror] = "Zendesk.onScriptLoadError(this, 'translations')".html_safe
    end

    content_tag :script, nil, tag_options
  end

  def fallback_cdn_name
    current_account.fallback_cdn_provider
  end

  # The 'translations_asset_fallback_to_api' param is used to detect if the translation
  # asset should make an addition call to get translations from the Rosetta API.
  def translations_asset_fallback_to_api_param
    request.params[:translations_asset_fallback_to_api]
  end

  def display_fallback_form?
    request.params[:fallback_cdn].blank? || translations_asset_should_fallback_to_api?
  end

  def translations_asset_should_fallback_to_api?
    request.params[:fallback_cdn] && request.params[:fallback_asset_name] == 'translations' &&
    translations_asset_fallback_to_api_param.blank?
  end

  def account_preload_id
    "/api/v2/account.json?include=#{ACCOUNT_SIDELOADS.join(',')}"
  end

  def ticket_sideloads
    @ticket_sideloads ||= begin
      TICKET_SIDELOADS + optional_sideloads
    end
  end

  def optional_sideloads
    sideloads =  []
    sideloads += [:tde_workspace] if current_account.has_contextual_workspaces? && !current_account.has_contextual_workspaces_in_js?
    sideloads += [:slas] if current_account.has_service_level_agreements?
    sideloads
  end

  # Important to include TICKET_SIDELOADS instead of `ticket_sideloads`,
  # so both urls with old and new list of sideloads are generated correctly
  def ticket_preload_id(ticket)
    "#{api_v2_ticket_path(ticket)}?include=#{TICKET_SIDELOADS.join(',')}"
  end

  def ticket_secondary_preload_id(ticket)
    sideloads = TICKET_SIDELOADS + [:tde_workspace] + [:slas]
    "#{api_v2_ticket_path(ticket)}?include=#{sideloads.join(',')}"
  end

  def lotus_base_uri
    url_for controller: 'lotus_bootstrap', action: 'index', only_path: false, trailing_slash: true
  end

  def pci_credit_card_javascript_include_tag
    return unless current_account.has_pci_credit_card_custom_field?

    src = [
      asset_host,
      Rails.application.config.assets.prefix,
      '/zendesk_pci_lotus.v2.js'
    ].join

    content_tag :script, "",
      type: "text/javascript",
      src: src,
      defer: 'defer'
  end

  def rollbar_access_token
    case ENV.fetch('ROLLBAR_REGION').downcase
    when 'us'
      ENV.fetch('ROLLBAR_US_LOTUS_ACCESS_TOKEN')
    when 'eu'
      ENV.fetch('ROLLBAR_EU_LOTUS_ACCESS_TOKEN')
    else
      ENV['ROLLBAR_LOTUS_ACCESS_TOKEN'].to_s # empty string so we don't crash on misconfiguration
    end
  end

  def auto_translation?
    current_account.has_polaris? && current_account.has_lotus_feature_auto_translation?
  end

  def auto_translation_languages
    locale = current_user.translation_locale
    Zendesk::AutoTranslation::GoogleCloudTranslate.languages(locale)
  end

  private

  def translations_api_uri
    locale = current_user.translation_locale.locale

    query_string = {
      include:    'translations',
      packages:   'lotus',
      bidi_chars: true,
      callback:   '__zendesk_config._cb'
    }.to_query

    "/api/v2/locales/#{locale}.json?#{query_string}"
  end

  def translations_asset_tag_src
    if current_account.has_translations_from_cdn? && translations_asset_fallback_to_api_param.blank?
      asset_path(translations_api_uri, host: Rails.application.config.deprecated_asset_host)
    else
      translations_api_uri
    end.html_safe
  end
end
