module DynamicContentHelper
  def dynamic_content_data
    res = {}
    current_account.cms_texts.each do |ct|
      ct.variants.each do |cv|
        res[ct.fully_qualified_identifier] = { value: cv.value, name: ct.name } if current_account.locale_id == cv.translation_locale_id
      end
    end
    res
  end
end
