module SecurityPolicyHelper
  def active_remote_authentications_for_select
    active_remote_authentications.map do |auth|
      remote_authentication_for_select(auth)
    end
  end

  def primary_remote_authentication_for_select
    if primary_remote_authentication
      remote_authentication_for_select(primary_remote_authentication)
    end
  end

  def remote_authentication_for_select(remote_auth)
    [remote_auth.name, remote_auth.id]
  end
end
