module AdminHelper
  ROLLBAR_URL = '%{base}/api/1/item/'.freeze

  protected

  def rollbar_config
    {
      endpoint: format(ROLLBAR_URL, base: ENV['ROLLBAR_WEB_BASE']),
      payload:  {environment: Rails.env}
    }.to_json.html_safe
  end
end
