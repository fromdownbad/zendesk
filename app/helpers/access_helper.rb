module AccessHelper
  def cancel_link(options = {})
    if current_account.help_center_enabled?
      link_to t('txt.access.index.cancel'), '/hc', options
    end
  end

  def email_value
    email = params[:email] || (params[:user] && params[:user][:email])
    email.blank? ? "" : CGI.unescape(email)
  end
end
