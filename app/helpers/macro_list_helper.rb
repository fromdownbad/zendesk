module MacroListHelper
  NUM_MACROS_FOR_SEARCH = 11

  def macro_search_enabled?
    @macro_search_enabled ||= (is_macro_list_nested? || macro_list_autocomplete_macros.length >= NUM_MACROS_FOR_SEARCH)
  end

  def is_macro_list_nested? # rubocop:disable Naming/PredicateName
    macro_list_autocomplete_macros if @is_macro_list_nested.nil?
    @is_macro_list_nested
  end

  def macro_list_shared_macros
    @macros_list_shared_macros ||= current_user.shared_macros.active.select('`rules`.`id`, `rules`.`title`').to_a
  end

  def macro_list_personal_macros
    @personal_macros ||= current_user.personal_macros.active.select('`rules`.`id`, `rules`.`title`').to_a
  end

  def macro_list_autocomplete_macros
    @macro_list_autocomplete_macros ||= (macro_list_shared_macros + macro_list_personal_macros).map do |macro|
      label = CGI.escape_html(macro.title.gsub(/::/, ' > '))
      @is_macro_list_nested ||= (label != macro.title)
      {label: label.as_json, value: macro.id}
    end
  end

  def setup_macro_list(element_id)
    if macro_search_enabled?
      js_init(macro_list_autocomplete_macros, true, "##{element_id}")
    else
      js_init([], true, "##{element_id}")
    end
  end

  def macro_cache_key
    [current_account.scoped_cache_key(:macros), current_user.cache_key]
  end

  def viewable_macros
    @viewable_macros ||= begin
      Zendesk::RuleSelection::Scope.viewable(
        Zendesk::RuleSelection::Context::Macro.new(current_user)
      )
    end
  end
end
