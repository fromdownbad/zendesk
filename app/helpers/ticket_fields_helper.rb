require 'zendesk/tickets/ticket_field_options'
require 'time_date_helper'

module TicketFieldsHelper
  include Zendesk::Tickets::TicketFieldOptions
  include TimeDateHelper

  def ticket_field_title(ticket_field)
    if ticket_field.id.present?
      I18n.t('txt.admin.views.ticket_field.edit.edit_label_caps_with_params', ticket_field: translate_edit_page_title(ticket_field.presentation_type))
    else
      I18n.t('txt.admin.views.ticket_field.edit.add_label_caps_with_params', ticket_field: translate_edit_page_title(ticket_field.presentation_type))
    end
  end

  def show_ticket_field_options?(ticket_field)
    [FieldAssignee, FieldGroup, FieldPriority, FieldRegexp, FieldTagger, FieldTicketType, FieldCheckbox, FieldMultiselect].include?(ticket_field.class) ||
      ticket_field.is_a?(FieldStatus) && current_account.has_status_hold?
  end

  def field_edit_element_for_end_user(field, ticket, form, saved_value = nil)
    title_in_portal = display_or_translate(field, :title_in_portal)
    description     = display_or_translate(field, :description)

    t = content_tag("h3", "#{h(Zendesk::Liquid::TicketContext.render(title_in_portal, ticket, current_user, false, current_user.translation_locale))} #{render_required(field.is_required_in_portal?)}".html_safe) << "\n"
    t << Zendesk::Liquid::TicketContext.render(h(description), ticket, current_user, false, current_user.translation_locale).present unless field.description.blank?
    t << content_tag("p", form_element_for_field(field, ticket, form, false, saved_value))
    t << ticket_type_options(ticket, form, false) if field.is_a?(FieldTicketType)
    t << render(partial: 'entries/suggest_topics', locals: {observable_id: 'ticket_subject'}) if field.is_a?(FieldSubject) && suggest_topics? && current_user.accessible_forums_count > 0
    content_tag :div, t, 'data-field-id' => field.id
  end

  def field_edit_element_for_end_user_mobile(field, ticket, form)
    content_tag("div", class: "field-wrapper #{"field-checkbox" if field[:type] == "FieldCheckbox"}  #{"field-dropdown" if field[:type] == "FieldTagger"}") do
      title_in_portal = display_or_translate(field, :title_in_portal)
      description     = display_or_translate(field, :description)

      t = content_tag("h3", "data-role": "title", id: field.title.downcase.gsub!(/\s/, "-")) do
        "#{Zendesk::Liquid::TicketContext.render(h(title_in_portal), ticket, current_user, false, current_user.translation_locale)} #{render_required(field.is_required_in_portal?)}".html_safe
      end
      t << content_tag("div", "data-role": "description") do
        Zendesk::Liquid::TicketContext.render(h(description), ticket, current_user, false, current_user.translation_locale).present
      end unless field.description.blank?
      t << form_element_for_field(field, ticket, form)
      t << ticket_type_options(ticket, form, false) if field.is_a?(FieldTicketType)
      t.gsub!(/<select([^>]*)>(.*?)<\/select>/m, "<div class=\"select-wrapper\"><select\\1>\\2</select><span class=\"select-arrow\">&#9660;</span></div>")

      t.html_safe
    end
  end

  # Agent ticket form fields
  def field_edit_element_for_agent(field, ticket, form, bulk_update = false, dc_cache = {})
    form_element, hidden = generate_form_element_for_field(field, ticket, form, bulk_update, dc_cache, nil)

    t = ''.html_safe
    if form_element.present?
      classes = ['select']
      # if it's a textarea or the subject field: give it full width:
      style = field.is_a?(FieldTextarea) || field.is_a?(FieldSubject) ? 'width:96%;' : ''

      # if it's a hidden field, don't show the chrome around it:
      # TODO: refactor form_element_for_field to return the method and
      #       arguments to be sent to the FormBuilder instead of passing
      #       it in and having that method make the call. That way we can
      #       check whether the method is :hidden_field instead of parsing
      #       the result. The tricky bit is the custom fields, which are
      #       more than one call in the current implementation.
      classes << 'hidden' if hidden

      t = content_tag(:div, class: classes.join(' '), style: style) do
        content_tag(:label) do
          title = h(render_liquid_using_appropriate_context(display_or_translate(field, :title), ticket, true, dc_cache))
          if field.is_required?
            title <<
              if ticket && !ticket.new_record?
                content_tag(:super, '*')
              else
                content_tag(:super, '*', class: 'only-if-solved')
              end
          end
          title
        end + form_element
      end

      t << content_tag(:div, "", style: "height:0px; clear:left") if field.is_a?(FieldTextarea) || field.is_a?(FieldSubject)
      t << ticket_type_options(ticket, form).html_safe if field.is_a?(FieldTicketType)
    end
    t.html_safe
  end

  def render_liquid_using_appropriate_context(field_title, ticket = nil, for_agent = nil, dc_cache = {})
    if ticket
      Zendesk::Liquid::TicketContext.render(field_title, ticket, current_user, for_agent, current_user.translation_locale, Mime[:text].to_s, dc_cache: dc_cache)
    else
      Zendesk::Liquid::DcContext.render(field_title, current_account, current_user, Mime[:text].to_s, nil, dc_cache)
    end
  end

  def field_show_for_agent(field, ticket)
    value = if field[:type] == 'FieldTextarea'
      field.value(ticket).to_s.present
    else
      render_liquid_using_appropriate_context(field.humanize_value(field.value(ticket))).to_s
    end

    content_tag(:li, content_tag(:h4, render_liquid_using_appropriate_context(field.title)) + value)
  end

  def ticket_field_value_for_print(field, ticket)
    if ticket.scrubbed? && current_account.has_prevent_deletion_if_churned?
      Zendesk::Scrub::SCRUB_TEXT
    elsif field[:type] == 'FieldTextarea'
      field.value(ticket).to_s.present
    elsif field[:type] == 'FieldDate'
      format_date(Time.parse(field.value(ticket)), time_zone: "UTC")
    else
      render_liquid_using_appropriate_context(field.humanize_value(field.value(ticket)))
    end
  end

  def field_show_for_print(field, ticket)
    value = ticket_field_value_for_print(field, ticket)
    content_tag(:h3, render_liquid_using_appropriate_context(field.title)) + content_tag(:p, value)
  end

  def assignable_groups
    @assignable_groups ||= current_account.groups.assignable(current_user)
  end

  def assignable_agents
    @assignable_agents ||= User.assignable(current_account).select("users.id, users.name")
  end

  def light_agents
    @light_agents ||= current_account.light_agents.select("users.id, users.name")
  end

  def assignable_agents_and_groups
    groups_agents_key = [current_account.scoped_cache_key(:groups_agents), current_user.id, 'js-groups-agents-6'].join("/")
    Rails.cache.fetch(groups_agents_key) do
      groups = js_groups(assignable_groups)
      agents = js_agents(assignable_agents)
      light  = js_light_agents(light_agents)

      [groups, agents, light].join_preserving_safety("\n".html_safe)
    end.html_safe
  end

  def render_required(property)
    property ? content_tag(:super, '*') : ''
  end

  ##############

  private

  ##############

  ###############################################################

  def suggest_topics?
    a = current_account
    a.subscription.has_forums2? && a.settings.topic_suggestion_for_ticket_submission?
  end

  def get_priorities(ticket, include_options = [])
    get_priorities_for(current_account, ticket, include_options)
  end

  def get_status(ticket, include_options = [])
    get_status_for(current_account, ticket, include_options)
  end

  def get_ticket_types(ticket, include_options = [])
    get_ticket_types_for(current_account, ticket, include_options)
  end

  def form_element_for_field(field, ticket, form, bulk_update = false, saved_value = nil)
    form_element, _hidden = generate_form_element_for_field(field, ticket, form, bulk_update, {}, saved_value)
    form_element
  end

  # This is a hack to let zendesk_channels/app/controllers/zendesk/channels/twitter/search_controller.rb
  # provide overrides to ticket system fields for API v2. When this helper is used in the context of said
  # controller, `in_twitter_search?` will be true.
  def in_twitter_search?
    # TODO: Channels.  The Twitter::SearchController class is defined in the zendesk_channels repo.
    controller.class.name == 'Zendesk::Channels::Twitter::SearchController'
  end

  def generate_form_element_for_field(field, ticket, form, bulk_update, dc_cache, saved_value)
    value = if saved_value.present? && !bulk_update
      h(saved_value)
    elsif field && !field.is_system_field?
      h(field.value(ticket))
    else
      ""
    end

    hidden = false
    form_element =
      case field[:type]
      #
      # System fields - direct ticket properties
      #
      when 'FieldSubject'
        form.text_field :subject, class: 'title', maxlength: 150
      when 'FieldDescription'
        text_area(:comment, :value, rows: 6, cols: 68, style: 'width:100%')
      when 'FieldStatus'
        if in_twitter_search?
          form.select :status, get_status_for_v2(current_account, ticket, include_option(ticket)), {}, onchange: 'toggleRequiredFields();'
        else
          form.select :status_id, get_status(ticket, include_option(ticket)), {}, onchange: 'toggleRequiredFields();'
        end
      when 'FieldTicketType'
        if request.format == :mobile_v2
          if in_twitter_search?
            select_tag 'ticket[type]', options_for_select(get_ticket_types_for_v2(current_account, ticket, include_option(ticket))), onchange: 'window.mobile.toggleDueDate();'
          else
            form.select :ticket_type_id, get_ticket_types(ticket, include_option(ticket)), {}, onchange: 'window.mobile.toggleDueDate();'
          end
        else
          if in_twitter_search?
            select_tag 'ticket[type]', options_for_select(get_ticket_types_for_v2(current_account, ticket, include_option(ticket))), onchange: onchange(ticket) + 'renderPerType();'
          else
            form.select :ticket_type_id, get_ticket_types(ticket, include_option(ticket)), {}, onchange: onchange(ticket) + 'renderPerType();'
          end
        end
      when 'FieldPriority'
        if in_twitter_search?
          form.select :priority, get_priorities_for_v2(current_account, ticket, include_option(ticket)), {}, onchange: onchange(ticket)
        else
          form.select :priority_id, get_priorities(ticket, include_option(ticket)), {}, onchange: onchange(ticket)
        end
      when 'FieldGroup'
        groups = current_user.account.groups || []
        if groups.length > 1
          field_group_options = {include_blank: (ticket && !ticket.group)}

          field_group_options[:selected] = groups_to_show(ticket).first.id if groups_to_show(ticket).size == 1
          form.select :group_id,
            include_option(ticket) + groups_to_show(ticket).collect { |g| [h(g.name), g.id] },
            field_group_options,
            style: 'width:auto;', onchange: onchange(ticket)
        else
          hidden = true
          form.hidden_field :group_id, value: groups.first.id
        end
      when 'FieldOrganization'
        organizations = current_user.organizations || []
        if organizations.length > 1
          field_organization_options = {
            include_blank: false,
            selected: ticket.organization_id || current_user.organization_id
          }

          form.select :organization_id,
            organizations.collect { |o| [h(o.name), o.id] },
            field_organization_options
        end
      when 'FieldAssignee'
        common_options = {
          :class => 'assignee',
          :onchange => onchange(ticket),
          'data-do-dynamic-filtering' => 'true',
          'data-bulk-update' => bulk_update,
          'data-refresh-url' => '',
          'data-no-matches-message' => I18n.t('txt.admin.helpers.ticket_fields_helpers.no_agents_found'),
          'data-value' => (ticket && ticket.assignee_id),
          :placeholder => I18n.t('txt.admin.helpers.ticket_fields_helpers.type_agent_name_label')
        }

        if groups_to_show(ticket).size > 1
          form.select :assignee_id,
            include_option(ticket),
            {include_blank: !ticket.nil?},
            common_options
        elsif assignable_agents.size > 1
          agents = assignable_agents.map { |o| [h(o.name), o.id] }

          form.select :assignee_id,
            include_option(ticket) + agents,
            {include_blank: !ticket.nil?},
            common_options.merge(style: 'width:auto;')
        end
        #
        # Custom fields - associated ticket properties. Values found in ticket_field_entries
        #
      when 'FieldText', 'FieldRegexp'
        form.fields_for :fields do |f|
          f.text_field field.id.to_s, value: value
        end
      when 'FieldTextarea'
        form.fields_for :fields do |f|
          f.text_area field.id.to_s, value: value, rows: 6, cols: 40, style: 'width:100%;'
        end
      when 'FieldDate'
        date = value
        form.fields_for :fields do |f|
          if request && request.format == :mobile_v2
            unless date.blank? || ticket.nil?
              date = I18n.localize(Date.parse(date), format: "%Y-%m-%d") rescue nil
            end
            fe = f.text_field("date_field_#{field.id}", :value => date, :class => "new_date_picker", "data-field" => "ticket_fields_#{field.id}")
            fe.gsub!(/type="text"/, 'type="date"')
          else
            unless date.blank? || ticket.nil?
              date = I18n.localize(Date.parse(date), format: "%y-%m-%d") rescue nil
            end
            fe = f.text_field("date_field_#{field.id}", :value => date, :class => "new_date_picker", :readonly => "readonly", "data-clear-allowed" => true, "data-field" => "ticket_fields_#{field.id}")
          end
          "#{fe}#{f.hidden_field(field.id.to_s, value: date)}<div id=\"date-flash\" class=\"error\" style=\"display:none;\">#{I18n.t("txt.requests.new.error_invalid_date")}</div>".html_safe
        end
      when 'FieldDecimal', 'FieldInteger'
        form.fields_for :fields do |f|
          f.text_field field.id.to_s, value: value, style: 'width:125px;'
        end
      when 'FieldCheckbox'
        form.fields_for :fields do |f|
          if ticket.present?
            f.check_box field.id.to_s, checked: value == '1'
          else
            f.select(field.id.to_s, [[I18n.t(NO_CHANGE), I18n.t(NO_CHANGE)], [I18n.t('txt.helpers.ticket_fields.on_label'), '1'], [I18n.t('txt.helpers.ticket_fields.off_label'), '0']])
          end
        end
      when 'FieldTagger'
        field_value = value

        # TODO: Cache this and user
        form.fields_for :fields do |f|
          if @ticket && !@ticket.new_record? && !current_user.can?(:edit_properties, @ticket)
            f.static_nested_select field, selected: field_value, current_account: current_account, current_user: current_user, dc_cache: dc_cache
          elsif field.custom_field_options.detect { |c| Zendesk::Liquid::DcContext.render(c["name"], current_account, current_user, Mime[:text].to_s, nil, dc_cache).index(CATEGORY_DELIMITER) } && request.format != :mobile
            f.tagger_nested_select field, selected: field_value, current_account: current_account, current_user: current_user, dc_cache: dc_cache
          elsif field.custom_field_options.detect { |c| c["name"].index(CATEGORY_DELIMITER) } && request.format != :mobile
            f.tagger_nested_select field, selected: field_value, current_account: current_account, current_user: current_user, dc_cache: dc_cache
          else
            # Note that taggings and tags is cached on the tagger object
            tags = ticket ? [['-', nil]] : [[I18n.t(NO_CHANGE), I18n.t(NO_CHANGE)], ['-', nil]]
            tags += field.custom_field_options.map { |cfo| [Zendesk::Liquid::DcContext.render(cfo["name"], current_account, current_user, Mime[:text].to_s, nil, dc_cache), h(cfo["value"])] }
            f.select(field.id.to_s, tags, selected: field_value) + "\n" # Linebreak due to a IE CSS bug...
          end
        end
      end

    [form_element, hidden]
  end

  # TODO: delete ticket_fields_helper#onchange()
  def onchange(_ticket)
    ''
  end

  def include_option(ticket)
    ticket ? [] : [[I18n.t(NO_CHANGE), -1]]
  end

  def groups_to_show(ticket)
    groups = assignable_groups

    # Allow group restricted agents to assign tickets to groups that they are not part of ZD:192698
    if current_user.agent_restriction?(:groups) && ticket && ticket.group
      groups |= [ticket.group]
    end

    groups
  end

  def js_agents_hash(agents)
    agents.map do |agent|
      {id: agent.id, name: h(agent.name)}
    end
  end

  def js_light_agents(agents)
    "var lightAgents = #{js_agents_hash(agents).to_json};".html_safe
  end

  def js_agents(agents)
    "var agents = #{js_agents_hash(agents).to_json};".html_safe
  end

  def js_groups_array(groups)
    group_ids = groups.pluck(:id)
    group_memberships = Hash.new { |hash, key| hash[key] = [] } # { group_id => [user_id, ...], ... }

    current_account.memberships.where(group_id: group_ids).pluck(:group_id, :user_id).each do |group_id, user_id|
      group_memberships[group_id] << user_id
    end

    group_ids.map do |group_id|
      { id: group_id, users: group_memberships[group_id] }
    end
  end

  def js_groups(groups)
    "var groups = #{js_groups_array(groups).to_json};".html_safe
  end

  def ticket_type_options(ticket, form, include_linked_problems = true)
    buffer = ''

    if include_linked_problems
      buffer << "<div class='select' id ='ticket_link' style='#{"display:none;" unless ticket && ticket.ticket_type?(:incident)}'>"

      linked_auto_complete = Arturo.feature_enabled_for?(:incident_auto_complete, current_account)
      limit = linked_auto_complete ? 1000 : 50

      problems = current_account.problems.working.
        select('id, nice_id, account_id, subject, description, is_public').
        from('tickets USE INDEX (index_tickets_on_account_id_and_ticket_type_id_and_status_id)').
        order(nice_id: :desc).limit(limit)

      problem = ticket.try(:problem)
      if problem && problem.ticket_type?(:problem) && !problems.detect { |p| p.id == problem.id }
        problems << problem
        problems.sort! { |x, y| y.id <=> x.id }
      end

      if problems.any?
        buffer << "<label style='font-weight:normal'> " + I18n.t('txt.admin.helpers.ticket_fields_helpers.link_incident_problem_label') + "</label>"

        select_properties = { style: 'width:auto' }
        if linked_auto_complete
          select_properties['auto-complete'] = 1
        end

        buffer << form.select(:linked_id,
          include_option(ticket) + problems.collect { |p| ["##{p.nice_id} #{h(p.title(linked_auto_complete ? 52 : 26))}".html_safe, p.id] },
          { include_blank: ticket.present? }, select_properties)
      end
      buffer << '</div>'
    end

    # Due date
    buffer << "<div class='select' id ='ticket_date' style='#{"display:none;" unless ticket && ticket.ticket_type?(:task)}'>"
    buffer << "<label style='font-weight:normal'>#{I18n.t('txt.helpers.ticket_fields_helper.task_due_date')}</label> "
    if current_user.is_agent? && !request.is_mobile?
      if ticket.present? && ticket.due_date.present?
        localized_date = I18n.localize(ticket.due_date, format: "%y-%m-%d")
        buffer << "<input type='text' id='ticket_due_date' class='new_date_picker' data-field='hidden_date' value='#{localized_date}' readonly='readonly'>"
        buffer << "<input type='hidden' id='hidden_date' name='ticket[due_date]' value='#{localized_date}'>"
      else
        buffer << "<input type='text' id='ticket_due_date' class='new_date_picker' data-field='hidden_date' readonly='readonly'>"
        buffer << "<input type='hidden' id='hidden_date' name='ticket[due_date]'>"
      end
    else
      buffer << form.date_select(:due_date, default: 7.days.from_now)
    end

    buffer << '</div>'
    buffer.html_safe
  end

  def translate_edit_page_title(ticket_field_presentation_type)
    ticket_field_presentation_type = ticket_field_presentation_type.downcase
    if ticket_field_presentation_type == "drop-down"
      I18n.t('txt.admin.helpers.ticket_field_helpers.drop_down_field_label')
    elsif ticket_field_presentation_type == "multiselect"
      I18n.t('txt.admin.helpers.ticket_field_helpers.multiselect_field_label')
    elsif ticket_field_presentation_type == "text"
      I18n.t('txt.admin.helpers.ticket_field_helpers.text_field_label')
    elsif ticket_field_presentation_type == "multi-line text"
      I18n.t('txt.admin.helpers.ticket_field_helpers.multi_line_text_field_label')
    elsif ticket_field_presentation_type == "numeric"
      I18n.t('txt.admin.helpers.ticket_field_helpers.numeric_field_label')
    elsif ticket_field_presentation_type == "decimal"
      I18n.t('txt.admin.helpers.ticket_field_helpers.decimal_field_label')
    elsif ticket_field_presentation_type == "checkbox"
      I18n.t('txt.admin.helpers.ticket_field_helpers.checkbox_field_label')
    elsif ticket_field_presentation_type == "regular expression"
      I18n.t('txt.admin.helpers.ticket_field_helpers.regular_expression_field_label')
    elsif ticket_field_presentation_type == "date"
      I18n.t('txt.admin.helpers.ticket_field_helpers.date_field_label')
    elsif ticket_field_presentation_type == "partialcreditcard"
      I18n.t('txt.admin.helpers.ticket_field_helpers.credit_card_field_label')
    else
      ticket_field_presentation_type + " field"
    end
  end

  def translate_setting_left_float_label(ticket_field_presentation_type)
    ticket_field_presentation_type = ticket_field_presentation_type.downcase
    if ticket_field_presentation_type == "drop-down"
      I18n.t('txt.admin.helpers.ticket_field_helpers.drop_down_label')
    elsif ticket_field_presentation_type == "multiselect"
      I18n.t('txt.admin.helpers.ticket_field_helpers.multiselect_label')
    elsif ticket_field_presentation_type == "text"
      I18n.t('txt.admin.helpers.ticket_field_helpers.text_label')
    elsif ticket_field_presentation_type == "multi-line text"
      I18n.t('txt.admin.helpers.ticket_field_helpers.multi_line_text_label')
    elsif ticket_field_presentation_type == "numeric"
      I18n.t('txt.admin.helpers.ticket_field_helpers.numeric_label')
    elsif ticket_field_presentation_type == "decimal"
      I18n.t('txt.admin.helpers.ticket_field_helpers.decimal_label')
    elsif ticket_field_presentation_type == "checkbox"
      I18n.t('txt.admin.helpers.ticket_field_helpers.checkbox_label')
    elsif ticket_field_presentation_type == "regular expression"
      I18n.t('txt.admin.helpers.ticket_field_helpers.regular_expression_label')
    elsif ticket_field_presentation_type == "custom"
      I18n.t('txt.admin.helpers.ticket_field_helpers.custom_label')
    elsif ticket_field_presentation_type == "date"
      I18n.t('txt.admin.helpers.ticket_field_helpers.date_label')
    elsif ticket_field_presentation_type == "partialcreditcard"
      I18n.t('txt.admin.helpers.ticket_field_helpers.credit_card_field_label')
    else
      ticket_field_presentation_type
    end
  end

  def display_or_translate(field, field_type)
    field.multilingual_field(user_locale: current_user.translation_locale, account_locale: current_account.translation_locale, field_type: field_type)
  end
end
