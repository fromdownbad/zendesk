require 'zendesk_voyager_api_client'

module ReportsHelper
  def extended_metrics_visible?
    current_account.extended_ticket_metrics_visible? && !current_account.settings.only_extended_metrics?
  end

  def xml_export_actions
    actions = []
    actions << link_to(I18n.t('txt.admin.helpers.reports_helper.request_file_label'), "/jobs/create?type=xml_export", method: :post)

    if has_feature?(:xml_export) && @latest_xml_export
      actions << link_to(I18n.t('txt.admin.helpers.reports_helper.latest_label'), @latest_xml_export.url(token: true, relative: true))
    end

    actions.join("<span class=\"delim\">|</span>").html_safe
  end

  def operator_mapping(item)
    return @operator_map[item[1]] unless item[0].index('ticket_fields_')
    return 'is_not' if item[1].upcase.index('NOT REGEXP')
    'is'
  end

  def user_xml_export_actions
    actions = []
    actions << link_to(I18n.t('txt.admin.helpers.reports_helper.request_file_label'), "/jobs/create?type=user_xml_export", method: :post)

    if has_feature?(:user_xml_export) && @latest_user_xml_export
      actions << link_to(I18n.t('txt.admin.helpers.reports_helper.latest_label'), @latest_user_xml_export.url(token: true, relative: true))
    end

    actions.join("<span class=\"delim\">|</span>").html_safe
  end

  def percentage(value)
    "#{(value * 100).round}%"
  rescue
    "0%"
  end

  def has_totals # rubocop:disable Naming/PredicateName
    !@totals.blank?
  end

  def to_total(key, value)
    @totals ||= {}
    @totals[key] = (@totals[key] || 0) + value.to_i
  end

  def total(key)
    @totals[key] || 0
  end

  # amCharts doesn't show correctly legend names with '<'
  def legend_name(name)
    name.gsub(/</, '&lt;')
  end

  def report_options(report)
    options = if report.new_record?
      ['preview_report', 'create_report']
    else
      ['preview_report', 'update_report', 'delete_report']
    end
    options.map { |op| [translate_report_options_humanize(op), op] }
  end

  def translate_report_options_humanize(option)
    if option == 'preview_report'
      I18n.t('txt.admin.helpers.reports_helper.preview_report')
    elsif option == 'create_report'
      I18n.t('txt.admin.helpers.reports_helper.create_report')
    elsif option == 'delete_report'
      I18n.t('txt.admin.helpers.reports_helper.delete_report_label')
    elsif option == 'update_report'
      I18n.t('txt.admin.helpers.reports_helper.update_report_label')
    else
      option.humanize
    end
  end

  def get_gooddata_url # rubocop:disable Naming/AccessorMethodName
    I18n.t("txt.admin.views.reports.tabs.overview.learn_about_gooddata_zendesk_url")
  end

  def sparkline_containers(array)
    containers = ''
    array.each { |args| containers += sparkline_container(args) }
    containers.html_safe
  end

  def sparkline_container(args)
    content_tag(:div, class: 'sparkline_container search_sparkline_container') do
      content_tag(:div, class: 'sparkline_title') do
        I18n.t('txt.admin.helpers.reports_helper.sparkline_container', stats_total: content_tag(:span, '', class: 'stat_total'), stats_title: content_tag(:span, args[:title], class: 'stat_title'))
      end +
      content_tag(:div, class: 'search_sparkline', id: args[:id]) do
        content_tag(:div, '', id: "#{args[:id]}_sparkline", style: 'width:140px; height:18px;')
      end
    end
  end

  def export_intro_text
    show_json_export? ? I18n.t('txt.admin.views.reports.export.intro_text_with_json') : I18n.t('txt.admin.views.reports.export.intro_text')
  end

  def show_json_export?
    ::Arturo.feature_enabled_for?(:voyager_json_export, current_account)
  end

  def ranged_json_exports_enabled?
    ::Arturo.feature_enabled_for?(:voyager_ranged_json_exports, current_account)
  end

  def show_csv_export_history?
    ::Arturo.feature_enabled_for?(:voyager_csv_export_history, current_account)
  end

  def voyager_api_client
    @csv_export_client ||= Zendesk::VoyagerClientBuilder.build(current_account)
  end

  def successful_exports(type, per_page = 10)
    @successful_exports ||= {}
    @successful_exports[type] ||= begin
      result = voyager_api_client.list_exports(format_type: type, sort_order: 'DESC', per_page: per_page, state: 'finished')
      if result.successful? && !result.empty?
        result.exports
      else
        []
      end
    end
  end

  def last_export_time(type)
    @last_export_time ||= {}
    @last_export_time[type] ||= successful_exports(type).empty? ? 0 : successful_exports(type).map(&:end_time).sort.last
  end

  def voyager_export_url
    options = { metrics: !extended_metrics_visible? }
    voyager_voyager_exports_path(options)
  end
end
