module People::UsersHelper
  include Devices::UsersHelper

  def group_label(groups)
    group_label = I18n.t('txt.admin.helpers.people.users_helper.member_of_group')
    group_label = I18n.t('txt.admin.helpers.people.users_helper.member_of_group_many') if groups.size > 1

    group_label
  end

  def display_groups(groups)
    result = groups.map do |group|
      content_tag(:strong, link_to_if(current_user.is_admin?, group.name, group))
    end

    result.to_sentence.html_safe
  end

  def show_role_config?(user)
    current_user.is_non_restricted_agent? && !current_account.has_permission_sets? && (current_user != user || current_user.is_admin?)
  end

  def can_remove_direct_number?(user)
    current_user.can?(:manage_identities_of, user) && !user.has_only_phone_identity?
  end

  def user_association_links
    tickets = if @user.is_agent?
      @user.assigned.for_user(current_user)
    else
      @user.tickets.for_user(current_user)
    end
    links = [
      link_to_association(@tickets_title, tickets, nil, true, true),
      link_to_association("ticket_fields.current_collaborators.label", @user.collaborated_tickets.for_user(current_user), 'ccs', false, true),
      link_to_association("txt.entries.topics", @user.entries.for_user(current_user), 'entries'),
      link_to_association("txt.entries.posts", @user.posts, 'posts'),
      link_to_association("txt.entries.votes", @user.voted_entries.for_user(current_user), 'votes'),
      link_to_association("txt.entries.subscriptions", @user.watchings, 'watchings')
    ]

    if show_devices_tab?
      links << link_to_association("txt.entries.devices_and_apps", nil, 'devices')
    end

    links
  end

  def link_to_association(key, items, select, no_i18n = false, try_archive = false)
    key = I18n.t(key) unless no_i18n
    count_method = try_archive ? 'count_with_archived' : 'count'
    link_text = items.nil? ? key : "#{key} (#{items.send(count_method)})"
    link_to(link_text, params.merge(select: select, page: 1), class: (params[:select] == select ? 'current' : ''))
  end

  def textual_time_zone(time_zone)
    Time.use_zone(time_zone) { Time.zone.to_s }
  end

  def delete_confirmation_message
    if @user.is_agent?
      I18n.t('txt.admin.view.people.users.basic_info.delete_agent_confirmation_message', agent_name: h(@user.name))
    else
      I18n.t('txt.admin.view.people.users.basic_info.delete_user_confirmation_message', user_name: h(@user.name))
    end
  end
end
