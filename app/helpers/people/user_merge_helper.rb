module People::UserMergeHelper
  def loser_identities_description
    if @loser.has_email? && @loser.has_twitter_identity?
      I18n.t('txt.admin.helpers.people.user_merge.user_has_twitter_email')
    elsif @loser.has_email?
      I18n.t('txt.admin.helpers.people.user_merge.user_has_email')
    else
      I18n.t('txt.admin.helpers.people.user_merge.user_has_twitter')
    end
  end
end
