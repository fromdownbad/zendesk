module People::RolesHelper
  private

  def string_prefix
    'txt.admin.views.people.roles.role_definition'
  end

  def rtl?
    !!TranslationLocale.find_by_language_tag(I18n.locale).try(:rtl?)
  end

  def rtl_class
    rtl? ? 'is-rtl' : ''
  end

  def section_separator
    content_tag(:div, '', class: 'u-border-b u-bc-grey-300')
  end

  def section(title: nil, description: nil, &block)
    content_tag(:section, class: 'u-mv-lg') do
      content_tag(:div, class: 'u-mb') do
        content_tag(:h2, title, class: 'u-fs-lg u-mb-xs') +
        content_tag(:p, description)
      end + content_tag(:div, &block)
    end + section_separator
  end

  def hidden_style(hidden)
    hidden ? 'display: none;' : ''
  end

  def permission_sub_section(id: nil, hidden: false, &block)
    content_tag(:div, nil, id: id, class: 'u-mb', style: hidden_style(hidden)) do
      content_tag(:div, &block)
    end
  end

  def permission_child_sub_section(id: nil, hidden: false, &block)
    content_tag(:div, nil, id: id, class: 'u-ph', style: hidden_style(hidden)) do
      content_tag(:div, &block)
    end
  end

  def permission_checkbox(form:, permission:, checked:, label:, label_weight_regular: false, &block)
    label_weight_class = label_weight_regular ? 'c-chk__label--regular' : ''
    content_tag(:div, id: permission, class: 'u-mb-xs c-chk') do
      form.check_box(permission, {
        checked: checked,
        class: 'c-chk__input'
      }) +
      form.label(permission, label, { class: ['c-chk__label', label_weight_class, rtl_class].join(' ') }) +
      content_tag(:div, &block)
    end
  end

  def permission_checkbox_hint(hint)
    content_tag(:div, hint, class: 'c-chk__hint')
  end

  def permission_radio_group_hint(hint)
    content_tag(:div, hint, class: 'c-txt__hint')
  end

  def permission_radio_group(form:, permission:, checked:, label:, radio_options:, &block)
    content_tag(:div, id: permission, class: 'u-mb-xs c-chk c-chk--radio') do
      content_tag(:div, class: 'u-mb-xs') do
        form.label(permission, label, class: 'c-txt__label u-mb-xs') +
        content_tag(:div, &block)
      end +
      form.collection_radio_buttons(permission, radio_options, :last, :first, {
        checked: checked,
        item_wrapper_class: 'c-chk c-chk--radio'
      }) { |b| permissions_radio_button(b) }
    end
  end

  def ticket_access_options(permission_set)
    options = [
      [I18n.t('txt.admin.helpers.people.roles_helper.assigned_to_this_agent'), 'assigned-only'],
      [I18n.t('txt.admin.helpers.people.roles_helper.requested_by_end_users_in_org'), 'within-organization'],
      [I18n.t('txt.admin.helpers.people.roles_helper.all_within_groups'), 'within-groups'],
      [I18n.t('txt.admin.helpers.people.roles_helper.all'), 'all']
    ]
    permission_set.is_light_agent? ? options[1..4] : options
  end

  def assign_tickets_message(permission_set)
    if permission_set.is_light_agent?
      I18n.t("#{string_prefix}.assign_tickets_to_any_group_when_is_requester")
    else
      I18n.t("#{string_prefix}.assign_tickets_to_any_group")
    end
  end

  def explore_access_options(account)
    if Arturo.feature_enabled_for?(:central_admin_staff_mgmt_roles_tab, account)
      [
        [I18n.t('txt.admin.views.people.roles.role_definition.explore_access.readonly'), 'readonly'],
        [I18n.t('txt.admin.views.people.roles.role_definition.explore_access.edit'), 'edit'],
        [I18n.t('txt.admin.views.people.roles.role_definition.explore_access.full'), 'full']
      ]
    else
      [
        [I18n.t('txt.admin.views.people.roles.role_definition.explore_access.none'), 'none'],
        [I18n.t('txt.admin.views.people.roles.role_definition.explore_access.readonly'), 'readonly'],
        [I18n.t('txt.admin.views.people.roles.role_definition.explore_access.edit'), 'edit'],
        [I18n.t('txt.admin.views.people.roles.role_definition.explore_access.full'), 'full']
      ]
    end
  end

  def view_access_options(account)
    if account.has_play_tickets_advanced?
      [
        [I18n.t('txt.admin.views.people.roles.role_definition.views.playonly'), 'playonly'],
        [I18n.t('txt.admin.views.people.roles.role_definition.views.readonly'), 'readonly'],
        [I18n.t('txt.admin.views.people.roles.role_definition.views.manage_personal'), 'manage-personal'],
        [I18n.t('txt.admin.views.people.roles.role_definition.views.manage_group'), 'manage-group'],
        [I18n.t('txt.admin.views.people.roles.role_definition.views.full'), 'full']
      ]
    else
      [
        [I18n.t('txt.admin.views.people.roles.role_definition.views.readonly'), 'readonly'],
        [I18n.t('txt.admin.views.people.roles.role_definition.views.manage_personal'), 'manage-personal'],
        [I18n.t('txt.admin.views.people.roles.role_definition.views.manage_group'), 'manage-group'],
        [I18n.t('txt.admin.views.people.roles.role_definition.views.full'), 'full']
      ]
    end
  end

  def role_edit_actions(permission_set)
    links = []
    links << link_to(
      I18n.t('txt.admin.views.people.roles.edit.view_agents_in_role'),
      users_path(permission_set: permission_set.id),
      class: 'c-menu__item'
    ) if permission_set.id && permission_set.agent_count > 0
    links << link_to(
      I18n.t("#{string_prefix}.clone_role_label"),
      clone_people_role_path(permission_set),
      class: 'c-menu__item'
    ) if current_user.can?(:clone, permission_set)
    links << link_to(
      I18n.t("#{string_prefix}.delete_role_label"),
      people_role_path(permission_set),
      data: { confirm: I18n.t("#{string_prefix}.delete_confirmation"), method: :delete },
      class: 'c-menu__item c-menu__item--danger admin remove'
    ) if current_user.can?(:delete, permission_set)
    links
  end

  def permission_set_actions(permission_set)
    links = []
    links << link_to(I18n.t('txt.admin.helpers.people.roles_helper.clone_label'), clone_people_role_path(permission_set)) if current_user.can?(:clone, permission_set)
    links << link_to(I18n.t('txt.admin.helpers.people.roles_helper.edit_label'), edit_people_role_path(permission_set), class: 'edit_this', rel: 'edit') if current_user.can?(:edit, permission_set)
    links.join_preserving_safety(content_tag(:span, '|', class: 'delim'))
  end

  def permissions_radio_button(option)
    content_tag(:div, class: 'c-chk c-chk--radio') do
      option.radio_button(class: 'c-chk__input') +
      option.label(class: "c-chk__label c-chk__label--radio c-chk__label--regular u-mb-xs #{rtl_class}")
    end
  end
end
