module People::BulkDeleteHelper
  def user_bulk_delete_field_header(identifier, title, sortable = true)
    content = h(title.truncate(50))
    if sortable
      desc = img = ''
      if params[:order].to_s == identifier.to_s
        desc = '1' if params[:desc].blank?
        img = image_tag("table-arrow#{desc}.png", class: 'view_sort')
      end
      content = link_to(content + img, params.merge(order: identifier, page: '1', desc: desc))
    end
    content_tag(:th, content, class: identifier, title: title)
  end

  def filter_check_box(filter_name)
    checked = params[:filter] == filter_name
    url = url_for(params.merge(filter: (checked ? '' : filter_name)))

    check_box_tag("filter_#{filter_name}", '', checked, class: 'filter-check-box', 'data-href' => url)
  end
end
