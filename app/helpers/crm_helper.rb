module CrmHelper
  APP_LATENCY_VALUES = ['15', '30', '45', SalesforceTicketData::DEFAULT_DELAY.to_s].freeze

  def crm_options_for_select
    options =  []
    options << ["Salesforce", SalesforceIntegration.name]
    options
  end

  def crm_selected_option
    if selected_integration = @crm_integration || current_account.crm_integration
      selected_integration.class.name
    elsif crm_options_for_select.size == 1
      crm_options_for_select[0][1]
    else
      SalesforceIntegration.name
    end
  end

  def salesforce_envs_for_select
    options = []
    options << [I18n.t('txt.admin.helpers.crm_helper.production_label'), "production"]
    options << [I18n.t('txt.admin.helpers.crm_helper.sandbox_label'), "sandbox"]
    options
  end

  def salesforce_env_selected_option
    if selected_integration = current_account.crm_integration
      return "sandbox" if selected_integration.is_sandbox?
    end
    "production"
  end

  def salesforce_app_latency_select_options
    options = APP_LATENCY_VALUES.map do |latency|
      [I18n.t("txt.admin.helpers.crm_helper.app_latency.minutes_label", minutes: latency), latency]
    end
    selected_option = current_account.salesforce_integration.app_latency || SalesforceTicketData::DEFAULT_DELAY
    options_for_select(options, selected_option)
  end

  def salesforce_lookup_types_for_select
    options = []
    options << [I18n.t('txt.admin.helpers.crm_helper.Requester_email_address'), "EmailAddress"]
    options << [I18n.t('txt.admin.helpers.crm_helper.User_tags'), "UserTags"]
    options << [I18n.t('txt.admin.helpers.crm_helper.Organization_name'), "OrganizationName"]
  end

  def salesforce_lookup_types_selected_option
    if selected_integration = current_account.crm_integration
      selected_integration.lookup_type
    end
  end

  def salesforce_show_parent_options
    [
      [I18n.t("txt.admin.views.settings.extensions._salesforce3.show_parent.option_none"), "none"],
      [I18n.t("txt.admin.views.settings.extensions._salesforce3.show_parent.option_show_parent_only"), "show_parent_only"],
      [I18n.t("txt.admin.views.settings.extensions._salesforce3.show_parent.option_show_parent_and_ultimate"), "show_parent_and_ultimate_parent"]
    ]
  end

  def salesforce_show_parent_selected_option?
    if selected_integration = current_account.crm_integration
      selected_integration.show_parent
    end
  end

  def class_for_crm_div(integration_class)
    crm_selected_option == integration_class ? "show" : "hide"
  end

  def remove_selected_crm_link
    if current_account.crm_integration_configured?
      link_to I18n.t('txt.admin.helpers.crm_helper.Remove'), {action: "remove_crm"},
        data: {
          confirm: I18n.t('txt.admin.helpers.crm_helper.Remove_confirm_message'),
          method: :delete
        },
        class: 'admin remove'
    end
  end

  def mappeable_zendesk_fields
    # Placeholders
    placeholders = %w[
      ticket.requester.name ticket.requester.last_name ticket.requester.email ticket.requester.phone
      ticket.requester.details ticket.requester.notes ticket.organization.name ticket.organization.details
      ticket.organization.notes ticket.assignee.name ticket.assignee.last_name ticket.assignee.email
      ticket.title ticket.id ticket.group.name ticket.requester.external_id ticket.organization.external_id
    ]

    placeholders << "ticket.brand.name" if current_account.has_multibrand?

    options = placeholders.map { |ph| [I18n.t("placeholder.#{ph}"), ph] }

    # Custom ticket fields
    types = %w[FieldText FieldTagger FieldDecimal FieldRegexp]
    ticket_fields = current_account.ticket_fields.where(is_active: true, type: types).to_a
    options += ticket_fields.map { |ticket_field| [ticket_field.title, "ticket.ticket_field_#{ticket_field.id}"] }

    options
  end

  def mapped_zendesk_fields(configured_objects, available_fields)
    configured_objects.map do |object_name|
      zendesk_field = current_account.salesforce_integration.configuration.zendesk_mapped_field(object_name)
      available_fields.find { |field| field[1] == zendesk_field }
    end.compact
  end

  def mapped_zendesk_field(configured_zendesk_field)
    mappeable_zendesk_fields.find { |zf| zf[1] == configured_zendesk_field }[0] rescue nil
  end

  def salesforce_configuration_errors(object_name)
    errors = current_account.salesforce_integration.configuration.errors(object_name)
    zendesk_field = current_account.salesforce_integration.configuration.zendesk_mapped_field(object_name)
    labels = current_account.salesforce_integration.configuration.labels(object_name)

    list = []

    list << I18n.t("txt.admin.views.settings.extensions._salesforce3.errors.missing_object") if errors[Salesforce::Configuration::MISSING_OBJECT]
    list << I18n.t("txt.admin.views.settings.extensions._salesforce3.errors.missing_salesforce_mapped") if errors[Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD]
    list << I18n.t("txt.admin.views.settings.extensions._salesforce3.errors.missing_zendesk_mapped") if mapped_zendesk_field(zendesk_field).nil?
    missing_fields = (errors[Salesforce::Configuration::MISSING_FIELDS] || []).map do |missing_field|
      if missing_field.include?("::")
        related_object, _related_field = missing_field.split("::")
        "#{related_object} -> #{labels[missing_field]}"
      else
        labels[missing_field]
      end
    end
    list << I18n.t("txt.admin.views.settings.extensions._salesforce3.errors.missing_fields", fields: missing_fields.join(', ')) if missing_fields.present?

    list
  end

  def salesforce_selected_objects
    configuration = current_account.salesforce_integration.configuration

    configuration.objects.map do |object_name|
      primary, related = configuration.organized_fields(object_name, true)
      salesforce_field, zendesk_field = configuration.mapping(object_name)
      labels = configuration.labels(object_name)
      object_label = configuration.object_label(object_name)

      render_to_string(
        partial: "salesforce_selected_object",
        locals: {
          object_name: object_name,
          object_label: object_label,
          primary: primary,
          related: related,
          salesforce_field: salesforce_field,
          zendesk_field: zendesk_field,
          labels: labels
        }
      )
    end.join.html_safe
  end

  def salesforce_filter_options(fields, selected, blank_text)
    options = [content_tag(:option, blank_text, value: "")]

    options += fields.map do |f|
      options = { :value => f[:key], "data-type" => f[:type] }
      options[:selected] = "selected" if f[:key] == selected

      content_tag(:option, f[:title], options)
    end

    options.join.html_safe
  end
end
