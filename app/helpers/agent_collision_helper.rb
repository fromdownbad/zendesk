module AgentCollisionHelper
  # @return [true, false] whether or not the current user sees agent collision notifications
  def show_agent_collision?
    current_user &&
    current_user.is_agent? &&
    current_account.has_agent_collision?
  end
end
