module AuditsHelper
  def cia_event_presenters_for(events)
    events.map { |event| CIA::EventPresenter.new(self, event) }
  end

  def order_direction_link(field, options = {})
    text = options[:text] || h(field.humanize)
    desc = (
      (params[:sort_by] || options[:default_sort_by]) == field &&
        params[:sort_order] == "desc"
    )
    text << image_tag("table-arrow#{"1" if desc}.png", class: 'view_sort')
    opposite_direction = desc ? "asc" : "desc"
    link_to(text, params.merge(sort_by: field, sort_order: opposite_direction, page: '1'))
  end
end
