module RssHelper
  # RSS content on ticket level
  def ticket_rss_item(xml, ticket, description, guid = nil)
    xml.title(%(#{ticket.ticket_type} ##{ticket.nice_id}: "#{ticket.title(100)}"))
    xml.link(ticket.url.to_s + (guid ? "?guid=#{guid}" : ''))
    xml.author(ticket.requester.name)
    xml.category(ticket.current_tags)
    xml.pubDate CGI.rfc1123_date(ticket.updated_at.in_time_zone)
    xml.description(description || ticket.description.sanitize)
    xml.guid(ticket.url)
  end

  # RSS content on entry level
  def entry_rss_item(xml, entry, comments = '')
    xml.title(%(Topic "#{entry.title}"))
    xml.link(entry.url)
    xml.author(entry.submitter.safe_name(current_user.is_agent?))
    xml.category(entry.current_tags)
    xml.pubDate CGI.rfc1123_date(entry.updated_at.in_time_zone)
    xml.description(comments)
    xml.guid(entry.url)
  end
end
