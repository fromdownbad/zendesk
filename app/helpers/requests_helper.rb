# This helper encapsulates functions used by the end users requests views
module RequestsHelper
  def render_visible_custom_fields(ticket)
    value = ''
    visible_custom_fields.select { |f| !f.is_a?(FieldAssignee) && !f.is_a?(FieldSubject) }.each do |field|
      title = h(Zendesk::Liquid::DcContext.render(field.title_in_portal, current_account, current_user))
      body  = h(Zendesk::Liquid::DcContext.render(field.humanize_value(field.value(ticket)), current_account, current_user))

      if field.is_a?(FieldTicketType) && ticket.ticket_type?(:task)
        body << " - #{I18n.t('ticket_fields.due_date.label')} #{content_tag(:strong, format_date(ticket.due_date))}".html_safe
      end
      value << content_tag(:h4, title)
      value << content_tag(:p, body, style: 'margin-top:0px')
    end
    value.html_safe
  end

  def render_editable_custom_fields(ticket, form, custom_fields)
    fields = editable_custom_fields.map do |field|
      field_edit_element_for_end_user(field, ticket, form, custom_fields && custom_fields[field.id.to_s])
    end

    if fields.all?(&:html_safe?)
      fields.join.html_safe
    else
      fields.join
    end
  end

  def render_editable_custom_fields_mobile(ticket, form)
    fields = mobile_editable_custom_fields.map do |field|
      field_edit_element_for_end_user_mobile(field, ticket, form)
    end

    if fields.all?(&:html_safe?)
      fields.join.html_safe
    else
      fields.join
    end
  end

  def render_assignee(ticket, show_picture = true)
    return '' if ticket.assignee.blank? || !visible_custom_fields.collect(&:class).member?(FieldAssignee)

    # produces one of the four:
    # * txt.requests.show.was_assigned_to_your_ticket
    # * txt.requests.show.is_assigned_to_your_ticket
    # * txt.requests.show.was_assigned_to_this_ticket
    # * txt.requests.show.is_assigned_to_this_ticket
    #

    ticket_reached_resolution = [StatusType.SOLVED, StatusType.CLOSED].include?(ticket.status_id)
    msg_key = "txt.requests.show.#{ticket_reached_resolution ? 'was' : 'is'}_assigned_to_#{current_user == ticket.requester ? 'your' : 'this'}_ticket"

    assigned_msg = I18n.t(msg_key, assignee_name: ticket.assignee.safe_name(false))

    if ticket.assignee.photo.nil? || !show_picture
      content_tag(:p, assigned_msg)
    else
      content_tag(:p, style: 'padding-top:10px;') do
        content_tag(:span, style: 'float: left; padding: 0 12px 0 0; margin:0;') do
          image_tag(photo_path(ticket.assignee), style: 'margin:0; border:#666;')
        end +
        content_tag(:span, assigned_msg)
      end +
      tag(:br, style: 'clear:left;')
    end
  end

  def render_assignee_for_mobile(ticket)
    return '' if ticket.assignee.blank? || !visible_custom_fields.collect(&:class).member?(FieldAssignee)

    content_tag :span, class: "assignee" do
      content_tag(:i, nil, class: "icon") + @ticket.assignee.safe_name(false)
    end
  end

  def all_comments_but_the_first(ticket)
    @all_but_first ||= ticket.comments.to_a[1..-1]
  end

  def can_add_attachments?
    return false unless current_account.is_attaching_enabled?
    return false if current_account.settings.private_attachments? && is_anonymous_request?
    true
  end

  def options_for_enduser_ticket_forms
    ticket_form_list = current_account.ticket_forms.active.end_user_visible.map do |tf|
      rendered_name = Zendesk::Liquid::DcContext.render(tf.display_name, current_account, current_user)
      [rendered_name, tf.id]
    end

    ticket_form_list.insert(0, ['-', -1])
    default_selected = @ticket.ticket_form_id.nil? ? -1 : @ticket.ticket_form_id
    options_for_select(ticket_form_list, default_selected)
  end

  def ticket_forms_data_tag
    return nil unless current_account.has_ticket_forms?
    content_tag :script, ticket_forms_data, type: 'application/json', id: 'ticket_forms_data'
  end

  def show_forms_dropdown?
    return @show_forms_dropdown if defined?(@show_forms_dropdown)
    @show_forms_dropdown = current_account.has_ticket_forms? && current_account.ticket_forms.active.end_user_visible.count(:all) > 1
  end

  protected

  def ticket_forms_data
    current_account.ticket_forms.active.end_user_visible.map do |form|
      { id: form.id, ticket_fields: ticket_field_data(form) }
    end.to_json.html_safe
  end

  def ticket_field_data(form)
    form.ticket_fields.active.visible_in_portal.editable_in_portal.with_organization(current_user).map do |ticket_field|
      { id: ticket_field.id, title_in_portal: ticket_field.title_in_portal, type: ticket_field.type }
    end
  end

  def visible_custom_fields
    @visible_custom_fields ||= (use_ticket_forms? ? ticket_form_visible_custom_fields : current_account.ticket_fields.active.visible_in_portal.for_visible)
  end

  def ticket_form_visible_custom_fields
    @ticket_form_visible_custom_fields ||= (find_ticket_form ? find_ticket_form.ticket_fields.active.visible_in_portal.for_visible : current_account.ticket_fields.active.visible_in_portal.for_visible)
  end

  def mobile_editable_custom_fields
    @editable_custom_fields ||= current_account.ticket_fields.active.visible_in_portal.editable_in_portal
  end

  def editable_custom_fields
    @editable_custom_fields ||= current_account.ticket_fields.active.visible_in_portal.editable_in_portal.with_organization(current_user)
  end

  def find_ticket_form
    found_ticket_form = TicketForm.with_deleted { current_account.ticket_forms.find_by_id(@ticket.ticket_form_id) }
    found_ticket_form.present? ? found_ticket_form : current_account.ticket_forms.default.first
  end

  def use_ticket_forms?
    current_account.has_ticket_forms? && current_account.ticket_forms.present?
  end
end
