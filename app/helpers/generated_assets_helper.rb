module GeneratedAssetsHelper
  def include_generated_stylesheets
    if current_account
      data_path = generated_data_path(current_account.id, current_account.updated_at)
      "<link id='generated_styles' rel='stylesheet' type='text/css' href='/generated/stylesheets/branding/#{data_path}.css' media='screen' />".html_safe
    end
  end

  def include_generated_mobile_stylesheets
    # Be aware that this is used in settings/mobile_portal#show for the mobile header preview
    # make sure it doesn't include styles that clash with other styling there
    if current_account
      data_path = generated_data_path(current_account.id, current_account.updated_at)
      "<link id='generated_styles' rel='stylesheet' type='text/css' href='/generated/stylesheets/branding/mobile/#{data_path}.css' media='screen' />".html_safe
    end
  end

  def current_locale_path
    if locale = I18n.translation_locale
      data_path = generated_data_path(locale.id, locale.cache_key)
      "/generated/javascripts/locale/#{data_path}.js"
    end
  end

  def include_generated_javascripts
    if path = current_locale_path
      "<script id='generated_javascript' type='text/javascript' src='#{path}'></script>".html_safe
    end
  end

  private

  def generated_data_path(id, cache_key)
    # There a validation, #validate_top_directory, in JavascriptsController that makes sure the top_directory is actually (id / 100)
    "#{id / 100}/#{id}/#{cache_key.is_a?(Time) ? cache_key.to_i : cache_key}"
  end
end
