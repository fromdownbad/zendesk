# http://nullstyle.com/2007/06/02/caching-time_ago_in_words/
module CachedDateHelper
  # Fully internationalized "time ago" helper
  #
  # @param [Time] from
  # @param [String] template A string in any language that includes an XXX where the time should go
  # @return a string containing html which includes an initially empty span tag and corresponding javascript that puts the timeago string into the span
  # @example
  #   i18n_cacheable_time_ago_in_words(ticket.created_at, "This ticket was created by Paul, XXX ago")
  #   => "<span id='cached-date-0.5425232701706972'></span>
  #       <script type=\"text/javascript\">\n//<![CDATA[\n$('cached-date-0.5425232701706972').replace('This ticket was created by Paul, XXX ago'.gsub('XXX', i18n_time_ago_in_words(1316035950000)));\n//]]>\n</script>"
  def i18n_cacheable_time_ago_in_words(from, template = 'XXX ago')
    compatible_document_write_js("#{escape_javascript_allow_tags(template)}.gsub('XXX', i18n_time_ago_in_words(#{(from.to_i * 1000).to_json}))")
  end

  protected

  # Similar to document.write, but doesn't break Prototype's Ajax.Updater
  def compatible_document_write_js(string)
    span_id = "cached-date-#{Kernel.rand}"
    span    = "<span id='#{span_id}'></span>"
    update_script = javascript_tag "$('#{span_id}').replace(#{string});"

    "#{span}#{update_script}".html_safe
  end
end
