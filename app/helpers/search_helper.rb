module SearchHelper
  def search_title(result)
    results = I18n.t('txt.search.results_pluralization', count: result.total_entries)
    if forum_id = params[:forum_id]
      if forum_id.is_a?(Array) || forum_id.to_i == 0
        return I18n.t('txt.search.found_in_forum', results: results, forum_name: I18n.t('txt.search.all_forums'))
      elsif current_user.accessible_forums_count > 1 && forum = current_account.forums.find_by_id(forum_id)
        return I18n.t('txt.search.found_in_forum', results: results, forum_name: forum.name)
      end
    end
    results
  end

  def search_result_class(item)
    additional_class =
      if item.is_a?(User)
        "search-result-user"
      elsif item.is_a?(Ticket)
        "search-result-entry"
      elsif item.is_a?(Group)
        "search-result-group"
      elsif item.is_a?(Organization)
        "search-result-organization"
      elsif item.is_a?(Entry) || item.is_a?(Watching) || item.is_a?(Vote)
        "search-result-entry"
      else
        "search-result-other"
      end
    "item #{additional_class}"
  end

  def display_bulk_update?
    current_account.has_permission_sets? && current_user.is_admin?
  end

  def display_seat_bulk_update?
    current_user.is_admin? &&
      params[:role] == [Role::AGENT.id.to_s, Role::ADMIN.id.to_s] &&
      voice_feature_enabled?(:user_seats)
  end
end
