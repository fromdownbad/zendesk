module OrganizationsHelper
  ORGANIZATION_MEMBERSHIP_LIMIT = 10_000

  def active_organization_options(organization)
    t = []
    if organization.domain_names.present?
      t << I18n.t('txt.admin.helpers.organization.automatically_added_to_org', name_organization: "<strong>#{ERB::Util.h(organization.domain_names)})</strong>")
    end
    if organization.group
      t << I18n.t('txt.admin.helpers.organization.automatically_added_to_group', group_name: organization.group.name)
    end
    if organization.is_shared?
      t << I18n.t('txt.admin.helpers.organization.can_view_all_tickets')
    end
    if organization.is_shared? && organization.is_shared_comments?
      t << I18n.t('txt.admin.helpers.organization.add_comments')
    end
    t
  end

  def group_and_org_association_links(ticket_count, user_count)
    [link_to(I18n.t('txt.admin.helpers.organization.users_tabs', ammount_of_users: user_count), params.merge(select: nil, page: 1), class: (params[:select] != 'tickets' ? 'current' : '')),
     link_to(I18n.t('txt.admin.helpers.organization.tickets_tabs', ammount_of_tickets: ticket_count), params.merge(select: 'tickets', page: 1), class: (params[:select] == 'tickets' ? 'current' : ''))]
  end

  def user_count_as_string(organization)
    membership_count = organization.organization_memberships.capped_count(ORGANIZATION_MEMBERSHIP_LIMIT)
    if membership_count.capped?
      ::I18n.t('txt.admin.people.membership_lower_bound', lower_bound: ORGANIZATION_MEMBERSHIP_LIMIT)
    else
      "(#{membership_count})"
    end
  end
end
