module HomeHelper
  def new_request_link
    link_to(I18n.t('txt.suggestions.deflect.submit_ticket'), appropiate_new_request_path)
  end

  def appropiate_new_request_path
    if current_account.is_open? && current_user.is_anonymous_user?
      main_app.new_anonymous_request_path
    else
      main_app.new_request_path
    end
  end

  def show_introductory_text?
    current_account.settings.show_introductory_text?
  end

  def account_introductory_title(edit_mode = false)
    intro_title = edit_mode ? current_account.introductory_title : Zendesk::Liquid::DcContext.render(current_account.introductory_title, current_account, current_user, nil, I18n.translation_locale)
    intro_title || default_introductory_title
  end

  def account_introductory_text(edit_mode = false)
    safe_text = current_account.introductory_text.to_s
    intro_text = edit_mode ? CGI.escape_html(safe_text) : Zendesk::Liquid::DcContext.render(safe_text, current_account, current_user, nil, I18n.translation_locale)
    (intro_text.presence || default_introductory_text).html_safe
  end

  def default_introductory_title
    I18n.t("txt.default.introductory_text.title", account_name: current_account.name, locale: current_account.translation_locale)
  end

  def default_introductory_text
    text =  I18n.t("txt.default.introductory_text.paragraph1", locale: current_account.translation_locale)
    text << tag(:br)
    text << I18n.t("txt.default.introductory_text.paragraph2", contact_us_link: mail_to(current_account.reply_address), locale: current_account.translation_locale)
  end
end
