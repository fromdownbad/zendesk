module TicketsHelper
  include TicketSharingHelperMethods

  def next_ticket_in_collection(nice_id)
    return @next_ticket if @next_ticket
    return nil unless current_user.is_agent? && defined?(params) && params[:col]
    key = tickets_in_collection_cache_key(params[:col])

    tickets_in_collection = Rails.cache.read(key)
    tickets_in_collection = tickets_in_page(get_page) unless tickets_in_collection

    return nil unless tickets_in_collection
    return nil unless nice_id_index = tickets_in_collection.rindex(nice_id)

    next_ticket = tickets_in_collection.fetch(nice_id_index + 1, nil)
    if !next_ticket && nice_id_index == tickets_in_collection.length - 1
      params[:next_page] = get_page + 1
      tickets_in_collection = tickets_in_page(params[:next_page])
      next_ticket = tickets_in_collection.first
    end

    @next_ticket = next_ticket
  end

  def tickets_in_page(page)
    return [] unless rule = current_account.rules.find_by_id(params[:col])
    tickets = rule.find_tickets(
      current_user,
      paginate: true, order: params[:order],
      desc: params[:desc], output_type: params[:output_type],
      page: page, backend: params[:backend], caller: 'backend'
    )
    set_tickets_in_collection_cache_and_map_comments(tickets, params[:col])
    tickets.map(&:nice_id)
  end

  def page_title
    "##{@ticket.nice_id} \"#{h @ticket.title(80)}\"".html_safe
  end

  def set_tickets_in_collection_cache_and_map_comments(tickets, id)
    if current_user.is_agent?
      key = tickets_in_collection_cache_key(id)
      Rails.cache.write(key, tickets.map(&:nice_id), expires_in: 3.hours)
      Comment.map_latest_comment_to_tickets(tickets)
    end
  end

  def next_ticket_page
    params[:next_page].present? ? params[:next_page] : get_page
  end

  def tickets_in_collection_cache_key(id)
    "/tickets_in_collection/#{current_account.id}/#{current_user.id}/#{id.to_i}/#{next_ticket_page}/#{params[:order]}/#{params[:desc]}"
  end

  def color_code_score(ticket, prefix = '')
    score_value = ZendeskRules::RuleField.lookup(:score).value(ticket)
    if score_value > 0
      content_tag(:span, prefix, class: "score_#{score_value}")
    else
      prefix.blank? ? '-' : prefix
    end
  end

  def ticket_actions(ticket)
    options = build_ticket_actions(ticket)
    options_for_select(options, params[:submit_type])
  end

  def build_ticket_actions(ticket)
    transaction_type = ticket.new_record? ? I18n.t('txt.admin.helpers.tickets_helper.create_label') : I18n.t('txt.admin.helpers.tickets_helper.update_label')
    options = [[I18n.t('txt.admin.helpers.tickets_helper.transaction_ticket', transaction: transaction_type), ""]]

    if next_ticket_in_collection(ticket.nice_id)
      options << [I18n.t('txt.admin.helpers.tickets_helper.transcation_ticket_go_next', transaction: transaction_type), "next"]
    end

    options << [I18n.t('txt.admin.helpers.tickets_helper.transaction_ticket_post_forum', transaction: transaction_type), "entry"]

    if current_user.can?(:merge, ticket)
      options << [I18n.t('txt.admin.helpers.tickets_helper.merge_into_another_ticket'), "merge"]
    end

    if !ticket.new_record? && current_user.can?(:delete, ticket)
      if !ticket.requester_is_agent? && current_user.can?(:edit, ticket.requester)
        options << [I18n.t('txt.admin.helpers.tickets_helper.mark_as_spam_and_suspend'), "spam"]
      end
      options << [I18n.t('txt.admin.helpers.tickets_helper.delete_ticket_label'), "delete"]
    end

    options
  end

  def closed_ticket_actions(ticket)
    actions = []
    if current_user.is_admin? && current_user.can?(:delete, ticket)

      actions << link_to(I18n.t('txt.admin.helpers.tickets_helper.delete_label'), ticket_path(ticket), data: { confirm: I18n.t('txt.admin.helpers.tickets_helper.delete_confirm'), method: :delete }, class: 'admin')
    end
    if ticket.requester.email.present?
      actions << button_to_function(I18n.t('txt.admin.helpers.tickets_helper.create_follow_up_label'), "parent.location='#{new_ticket_path('ticket[via_followup_source_id]' => @ticket.nice_id)}'; return false;")
    end
    content_tag(:div, class: 'action', id: 'closed_ticket_actions') do
      actions.join_preserving_safety('&nbsp; ' + I18n.t('txt.admin.helpers.tickets_helper.or_label') + ' &nbsp;'.html_safe)
    end
  end

  def recent_ticket_link_to(ticket)
    content = content_tag(:span, "##{ticket.nice_id}", class: "id") +
      "&nbsp;".html_safe +
      content_tag(:span, ticket.title(62)) +
      content_tag(:div, requester_name(ticket).truncate(28), class: "r_n")
    link_to(content, main_app.ticket_path(ticket))
  end

  def requester_name(ticket)
    (ticket && ticket.requester.try(:name)) || ""
  end

  def new_user_link(type)
    t = I18n.t('txt.admin.helpers.tickets_helper.start_typing')
    if current_user.can?(:create_new, User)
      t << ' - '
      t << link_to(I18n.t('txt.admin.helpers.tickets_helper.add_new_user'), new_user_path(inline: true), :class => "new_user_from_ticket", "data-type" => type)
    end
    content_tag(:p, t)
  end

  def public_comment_label(ticket)
    if ticket.present? && ticket.entry.present?
      I18n.t('txt.admin.helpers.tickets_helper.post_as_comment')
    else
      I18n.t('txt.admin.helpers.tickets_helper.requester_can_see_comment')
    end
  end

  def private_comment_label(ticket)
    if ticket.present? && ticket.entry.present?
      I18n.t('txt.admin.helpers.tickets_helper.comment_will_not_be_posted')
    else
      I18n.t('txt.admin.helpers.tickets_helper.comment_not_seen_by_requester')
    end
  end

  def author_name_for(comment)
    if comment.author.is_system_user? && comment.ticket.twitter_ticket_source
      "@#{comment.ticket.twitter_ticket_source.twitter_screen_name}"
    else
      comment.author.safe_name(current_user.is_agent?)
    end
  end

  def sub_header_for_end_user(ticket)
    t = ''
    if ticket.followup_source
      source_ticket = ticket.followup_source
      t = content_tag(:span, class: 'attenuate') do
        I18n.t('txt.requests.show.followup_with_params', followup_with_params_link: "<strong>#{link_to I18n.t('txt.requests.followup_with_params_link', ticket_id: source_ticket.nice_id), request_path(source_ticket), target: "_blank", title: source_ticket.title}</strong>")
      end
    end
    content_tag(:span, t, class: 'sub')
  end

  def requester_header(ticket, include_submitter = true)
    t = format_date(ticket.created_at, with_time: true)
    t << " <span id='zendesk_ticket_requester'>#{h(@ticket.requester.name)}</span> " if ticket.requester.present?
    t << "#{I18n.t('txt.helpers.tickets_helper.via_label')} #{h(@ticket.submitter.name)}" if ticket.submitter != ticket.requester && include_submitter
    content_tag(:span, t, id: "requester_header")
  end

  def is_twicket_and_twitter_profiles_are_present? # rubocop:disable Naming/PredicateName
    @ticket &&
    @ticket.twitter_ticket_source &&
    @ticket.twitter_ticket_source.twitter_screen_name &&
    @ticket.requester &&
    @ticket.requester.twitter_profile &&
    @ticket.requester.twitter_profile.screen_name
  end

  def ticket_pagination_col
    (defined?(params) ? params[:col] : nil) || (@rule ? @rule.id.to_i : 0)
  end

  def ticket_pagination_options
    { col: ticket_pagination_col, order: params[:order], desc: params[:desc], page: get_page }
  end

  def ticket_path_for_collection(nice_id, escape = true)
    return "/requests/#{nice_id}" unless current_user.is_agent?
    escape = (escape == false) ? false : nil
    link = ticket_path(nice_id, ticket_pagination_options.merge(page: next_ticket_page, escape: escape))
    link << "&return_to=#{CGI.escape(return_to_path)}".html_safe if return_to_path.present?
    link
  end

  def ticket_table_path_for_record(record)
    record_type = record.class.to_s.downcase.pluralize
    path = "/#{record_type}/#{record.id}?order=status"
    record_type == "users" ? path : path << "&select=tickets"
  end

  def return_to_path
    return nil unless defined?(params)
    params[:return_to] ||
    (['organizations', 'groups', 'users', 'tags'].member?(controller_name) ? request.fullpath : nil)
  end

  def ticket_stats_render(record)
    end_user = record if record.is_a?(User) && !record.is_agent?
    title = record.is_a?(User) && record.is_agent? ? I18n.t('txt.admin.helpers.tickets_helper.assigned_tickets_label') : I18n.t('txt.admin.helpers.tickets_helper.tickets_label')
    stats = ticket_stats(record)

    list = ticket_stat_render(stats[:working], end_user)
    list << ticket_stat_render(stats[:solved], end_user) if stats[:solved]
    list << ticket_stat_render(stats[:recently_solved], end_user) if stats[:recently_solved]
    list << ticket_stat_render(stats[:all], end_user) if stats[:all]
    list << ticket_stat_render(stats[:assigned], end_user) if stats[:assigned]

    content_tag(:h5, title) + content_tag(:ul, list, class: :options, id: "ticket-stats-widget")
  end

  def ticket_stat_render(stat, end_user)
    link = stat[:link]
    link << "&user_widget=#{end_user.id}" if end_user
    content_tag(:li, link_to("#{h(stat[:title])} <strong>(#{h(stat[:count])})</strong>".html_safe, link))
  end

  def custom_fields_render(f, expand_link, bulk_update)
    t = ''

    ticket_fields = current_account.ticket_fields.active.custom.select('id, account_id, type, is_required, title, is_collapsed_for_agents').to_a

    collapsed = []
    ticket_fields.each do |field|
      if field.is_collapsed_for_agents?
        collapsed << field_edit_element_for_agent(field, @ticket, f, bulk_update, current_user.dc_cache)
      else
        t << field_edit_element_for_agent(field, @ticket, f, bulk_update, current_user.dc_cache)
      end
    end

    if collapsed.any?
      t << expand_link
      t << content_tag(:div, collapsed.join_preserving_safety, id: 'collapsible_fields', style: 'display:none')
    end

    t.html_safe
  end

  def render_followers
    return unless @ticket.collaborators.present?
    t = "<div id='ccs'><h3>#{I18n.t('txt.admin.models.rules.rule_dictionary.ccs_label')}</h3><p>"
    t << h(@ticket.collaborators.map(&:email_address_with_name_without_quotes).join(", "))
    t << '</p></div>'
    t.html_safe
  end

  # TODO: Use #render_data_rtl_attribute in application_helper, once that method uses `dir` attribute
  def render_dir_rtl_attribute
    'dir=rtl' if current_account.has_rtl_lang_html_attr? && current_user.translation_locale.rtl?
  end

  def ticket_requester_name_for_display
    if current_account.has_rtl_lang_html_attr? && current_user.translation_locale.rtl? && @ticket.requester.email.present?
      content = h(@ticket.requester.name) + ' ' + content_tag(:span, "<#{@ticket.requester.email}>", style: 'display: inline-block;')
      content_tag(:p, content)
    else
      content_tag(:p, @ticket.requester_name_for_display)
    end
  end

  private

  def ticket_stats(record)
    new_open_pending_label_i18n_key = current_account.use_status_hold? ? 'txt.admin.helpers.tickets_helper.new_open_pending_on_hold_label' : 'txt.admin.helpers.tickets_helper.new_open_pending_label'

    data = {
      working: { title: I18n.t(new_open_pending_label_i18n_key), count: nil, link: nil },
      solved: { title: I18n.t('txt.admin.helpers.tickets_helper.solved_closed_label'), count: nil, link: nil },
      assigned: { title: I18n.t('txt.admin.helpers.tickets_helper.assigned_to_you_label'), count: nil, link: nil },
      all: { title: I18n.t('txt.admin.helpers.tickets_helper.all_label'), count: nil, link: nil },
      recently_solved: { title: I18n.t('txt.admin.helpers.tickets_helper.recently_solved_closed_label'), count: nil, link: nil },
    }

    if record.is_a?(User) && record.is_agent?
      condition = build_condition('is', record.id, 'assignee_id')

      working_conditions_all    = build_set(condition)
      working_conditions_any    = build_set(working_condition, 2)
      working_conditions        = working_conditions_all.merge(working_conditions_any)
      data[:working][:count]    = record.assigned.working.for_user(current_user).count(:all)
      data[:working][:link]     = search_rules_path(filter: 'views', sets: working_conditions)

      solved_conditions_all     = build_set(condition)
      solved_conditions_any     = build_set(solved_condition, 2)
      solved_conditions         = solved_conditions_all.merge(solved_conditions_any)

      data[:recently_solved][:count] = record.assigned.solved.for_user(current_user).count(:all)
      data[:recently_solved][:link]  = search_rules_path(filter: 'views', sets: solved_conditions)
      data[:all][:count]             = record.assigned.for_user(current_user).count_with_archived
      data[:all][:link]              = ticket_table_path_for_record(record)
    else
      association_id = record.class.to_s.downcase.gsub('user', 'requester') + '_id'
      condition      = build_condition('is', record.id, association_id)

      working_conditions_all    = build_set(condition)
      working_conditions_any    = build_set(working_condition, 2)
      working_conditions        = working_conditions_all.merge(working_conditions_any)
      data[:working][:count]    = record.tickets.working.for_user(current_user).count(:all)
      data[:working][:link]     = search_rules_path(filter: 'views', sets: working_conditions)

      solved_conditions_all     = build_set(condition)
      solved_conditions_any     = build_set(solved_condition, 2)
      solved_conditions         = solved_conditions_all.merge(solved_conditions_any)

      data[:recently_solved][:count] = record.tickets.solved.for_user(current_user).count(:all)
      data[:recently_solved][:link]  = search_rules_path(filter: 'views', sets: solved_conditions)
      data[:all][:count]             = record.tickets.for_user(current_user).count_with_archived
      data[:all][:link]              = ticket_table_path_for_record(record)
      data[:assigned][:count]   = current_user.assigned.where(association_id => record.id).count(:all)
      data[:assigned][:link]    = search_rules_path(filter: 'views', sets: build_set([condition, assignee_condition(current_user)]))
    end

    data.reject { |_key, value| value[:count].nil? }
  end

  def should_show_channel_back_options?
    @ticket && @ticket.via_twitter? && @ticket.requester.try(:has_twitter_identity?)
  end

  def should_show_facebook_back_options?
    @ticket && @ticket.via_facebook?
  end

  # The intention of this method is to determine which tag field to display.
  #
  # Originally:
  # If the ticket is a new record, no tags should exist
  #   So, show a blank tag field.
  #
  # Also:
  # If the ticket is a new record AND it's a followup
  #   It does have tags defined (from the source ticket)
  #   So, show those tags.
  #
  def tags_exist?(ticket)
    return unless ticket

    ticket.id || ticket.via_followup_source_id
  end

  def can_edit_ticket_tags?(ticket)
    ticket.present? && current_user.can?(:edit_tags, Ticket) && !current_user.limited_access?(ticket)
  end

  def ticket_field_print_title(field_name)
    Zendesk::Liquid::DcContext.render(field_name, current_account, current_user)
  end

  def show_ticket_tags?(ticket)
    return false unless current_account.settings.ticket_tagging?
    if ticket.present?
      can_edit_ticket_tags?(ticket) || !ticket.current_tags.blank?
    else
      current_user.can?(:edit_tags, Ticket)
    end
  end

  def eligible_agreements_for_dropdown(ticket)
    unless @agreements_for_dropdown
      agreements  = eligible_agreements_for_sharing.to_a
      from_ticket = ticket.shared_tickets(include: :agreement).map(&:agreement).compact
      remote_urls = from_ticket.map(&:remote_url).uniq

      # Don't display agreements that this ticket is already shared with
      agreements.reject! { |agreement| remote_urls.include?(agreement.remote_url) }

      @agreements_for_dropdown = agreements
    end

    @agreements_for_dropdown
  end

  def cc_enabled?(ticket)
    current_account.is_collaboration_enabled? &&
    current_user.can?(:edit, ticket)
  end

  def can_share?(ticket)
    eligible_agreements_for_dropdown(ticket).any? &&
    current_user.can?(:edit, ticket)
  end

  def can_unshare?(ticket)
    ticket.shared_tickets.any? &&
    current_user.can?(:edit, ticket)
  end

  def edit_share_link_text(ticket)
    link_text = []
    link_text << I18n.t('txt.admin.helpers.tickets_helper.edit_label') if cc_enabled? ticket
    link_text << I18n.t('txt.admin.helpers.tickets_helper.share_label') if can_share?(ticket) || can_unshare?(ticket)

    link_text.join(' / ')
  end

  def can_manage_sharing?(ticket)
    !ticket.closed? &&
    !current_user.limited_access?(ticket) &&
    (cc_enabled?(ticket) || can_share?(ticket) || can_unshare?(ticket))
  end

  def translate_ticket_status(status)
    I18n.t('txt.admin.helpers.ticket_helper.status_' + status.downcase)
  end

  def translate_ticket_type(ticket_type, ticket_id)
    ticket_type = ticket_type.sub(" ", "_")
    I18n.t('txt.admin.helpers.ticket_helper.ticket_type_' + ticket_type.downcase, ticket_id: ticket_id)
  end

  def translate_closed_ticket(ticket)
    if ticket.priority_id == 0
      I18n.t('txt.views.tickets.closed_tickets.closed_with_no_priority', date: format_date(ticket.status_updated_at, with_time: true))
    else
      I18n.t('txt.views.tickets.closed_tickets.closed_with_priority_' + ticket.priority.downcase, date: format_date(ticket.status_updated_at, with_time: true))
    end
  end

  def translate_model_details(model_name)
    if model_name == "User" || model_name == "Organization"
      I18n.t('txt.admin.views.tickets.editable_notes.model_details_label_' + model_name)
    else
      I18n.t('txt.admin.views.tickets.editable_notes.model_details_label', model_name: model_name)
    end
  end

  def output_due_date_or_linked_problem(field)
    if field.class == FieldTicketType && field.value(@ticket) == TicketType.INCIDENT && @ticket.linked_id.present?
      "<div class='name_value'><h3> #{I18n.t('txt.admin.views.tickets.print.incident_of')} </h3><p> #{@ticket.problem_id} </p></div>".html_safe
    elsif field.class == FieldTicketType && field.value(@ticket) == TicketType.TASK
      "<div class='name_value'><h3> #{I18n.t('txt.admin.views.tickets.print_ticket_field.due_date')} </h3><p> #{format_date(@ticket.due_date) if @ticket.due_date} </p></div>".html_safe
    end
  end
end
