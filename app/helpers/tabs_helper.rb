# Defines helper methods to build the standard Zendesk tabbed UI components. Used like this:
#
#  <%= zd_tabs do %>
#    <%= zd_tab('Tab One') do %>
#      <%= render :partial => 'tab_one' %>
#    <% end %>
#    <%= zd_tab('Tab Two') do %>
#      <%= render :partial => 'tab_two' %>
#    <% end %>
#  <% end %>
#
# The DOM id will be generated from the tab title, i.e. in the above there will be #tab_one and #tab_two. This
# can be overridden by specifying an explicit :id argument, e.g. zd_tab('Tab Three', :id => 'tab_3')
#
module TabsHelper
  def zd_tabs(args = {}, &block)
    @container_tabs = []

    content = content_tag(:div, capture(&block), class: "tabs_canvas")
    content = content_tag(:div, content, class: "tabs_content")

    unless @container_tabs.empty?
      display = @container_tabs.size == 1 ? "hide" : ""
      content = content_tag(:div, class: "tab_links clearfix #{display}") do
        @container_tabs.collect do |tab|
          link_to(tab[:title], "##{tab[:id]}", data: { title_value: tab[:title] }, class: tab[:default] ? "current" : "")
        end.join_preserving_safety
      end + content
    end
    content_tag(:div, content, class: "#{args[:class] || 'tabbed_container'} narrow")
  end

  def zd_tab(title, options = {}, &block)
    tab = { id: (options[:id] || title_to_dom_id(title)), title: title }

    if @container_tabs.empty?
      options[:class] = options[:class] ? "#{options[:class]} default_tab" : "default_tab"
      tab[:default] = true
    end
    @container_tabs << tab

    content_tag(:div, '', id: tab[:id], class: options[:class], &block)
  end

  def title_to_dom_id(title)
    title.gsub(/\W/, '_').squeeze('_').downcase
  end

  def zd_yes_no_radio(form, property, options = {})
    true_value  = true
    false_value = false

    if form.is_a?(ActionView::Helpers::FormBuilder::PropertySetFormBuilderProxy)
      true_value  = "1"
      false_value = "0"
    end

    if options.delete(:swap_values)
      true_value, false_value = false_value, true_value
    end

    yes_options = {}
    no_options  = {}

    if toggle = options.delete(:toggles)
      yes_options['data-toggles-show'] = toggle
      no_options['data-toggles-hide']  = toggle
    end

    if toggle = options.delete(:anti_toggles)
      yes_options['data-toggles-hide'] = toggle
      no_options['data-toggles-show']  = toggle
    end

    if form.is_a?(ActionView::Helpers::FormBuilder::PropertySetFormBuilderProxy)
      yes_options[:id] = "account_settings_#{property}_#{true_value}"
      no_options[:id] = "account_settings_#{property}_#{false_value}"
      yes_label = label_tag(yes_options[:id], I18n.t('txt.admin.helpers.tab_helper.yes_option'), class: "radio")
      no_label  = label_tag(no_options[:id], I18n.t('txt.admin.helpers.tab_helper.no_option'), class: "radio")
    else
      scrubbed_property = property.to_s.chomp('?')
      yes_label = form.label("#{scrubbed_property}_#{true_value}", I18n.t('txt.admin.helpers.tab_helper.yes_option'), class: "radio")
      no_label  = form.label("#{scrubbed_property}_#{false_value}", I18n.t('txt.admin.helpers.tab_helper.no_option'), class: "radio")
    end

    content_tag(:div, class: "radio horizontal") do
      form.radio_button(property, true_value, options.merge(yes_options)) + yes_label
    end + content_tag(:div, class: "radio horizontal") do
      form.radio_button(property, false_value, options.merge(no_options)) + no_label
    end
  end

  def zd_enable_checkbox(form, property, label_text, options = {})
    label = if form.is_a?(ActionView::Helpers::FormBuilder::PropertySetFormBuilderProxy)
      checkbox_id = form.send(:prepare_id_name, property, {})[:id]
      label_tag(property, label_text, class: 'checkbox', for: checkbox_id)
    else
      form.label(property, label_text, class: 'checkbox')
    end

    content_tag(:div, class: "enable_checkbox") do
      form.check_box(property, options) + "\n" + label
    end
  end

  def zd_sub_setting(property, options = {}, &block)
    display = options.reverse_merge(show: true).delete(:show) ? "show" : "hide"
    content_tag(:div, class: "sub_setting #{display}", id: "sub_setting_#{property}", &block)
  end
end
