module FlashHelper
  protected

  def flash_error(header, *models)
    errors = "#{header}" # rubocop:disable Style/RedundantInterpolation
    if models.any?
      errors << '<ul>'
      models.each do |model|
        next if model.nil?
        if model.is_a?(String)
          errors << "<li>#{ERB::Util.h(model.to_s)}</li>"
        elsif model.is_a?(ActiveRecord::Base)
          model.errors.each do |attribute, message|
            next if attribute == :events
            errors << '<li>'
            unless attribute == :base
              human_attribute = model.class.human_attribute_name(attribute)
              errors << "<strong>#{ERB::Util.h(human_attribute)}</strong> "
            end

            errors <<
              if model.is_a?(Rule)
                custom_rule_flash_error(model, message)
              else
                ERB::Util.h(message)
              end

            errors << '</li>'
          end
        elsif model.respond_to?(:errors)
          model.errors.each do |_, message|
            errors << "<li>#{ERB::Util.h(message)}</li>"
          end
        else
          errors << "<li>#{I18n.t('activerecord.attributes.rules.unknown_error')}</li>"
          logger.error("Unknown error. Trace dump: #{model.inspect}")
        end
      end
      errors << '</ul>'
    end
    errors.html_safe
  end

  def custom_rule_flash_error(model, message)
    case message
    when /Invalid value.*?Group \/ Is \// # Flash error for Rule with deleted group
      I18n.t("activerecord.attributes.flash_error.rules.deleted_group_#{model.rule_name}")
    else
      ERB::Util.h(message)
    end
  end
end
