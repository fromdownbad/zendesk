module AssetsHelper
  def body_class
    "#{controller_name} #{controller_name}-#{action_name}".tap do |str|
    end
  end

  def js_init(*args)
    js_init_json(args.to_json)
  end

  def js_init_with_escape(*args)
    js_init_json(args.to_json.gsub('/', '\/'))
  end

  # also try test/functional/home_controller_test.rb
  def js_init_json(json)
    path = @virtual_path.to_s.sub(/^\//, '')
    javascript_tag("zd.jsInitializers.push([#{path.to_json},#{json.clean_separators('\n')}]);")
  end
end
