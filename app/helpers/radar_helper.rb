require 'uri'
require 'hmac-sha2'

module RadarHelper
  # This comes from https://github.com/socketio/engine.io/blob/1.4.2/lib/server.js
  PING_INTERVAL = 25_000
  # This adds a bit of a buffer on top of the ping interval.
  SOCKET_TIMEOUT = PING_INTERVAL + 5000

  private

  def radar_configuration
    return '' if !current_account || !current_user
    time = Time.now.utc
    {
      'engineio_079' => true,
      'path' => '/engine.io-0.7.9',
      'secure' => true,
      'upgrade' => use_websockets?,
      'upgrades' => ['websocket'],
      'transports' => ['polling', 'websocket'],
      'rememberUpgrade' => true,
      'userId' => current_user.id || 0,
      'userType' => current_user.roles || 0,
      'accountName' => current_account.subdomain,
      'auth' => radar_auth_token(time),
      'expires_at' => expires_at(time),
      'query' => {
        'section' => current_account.shard_id,
        'user' => current_user.id || 0,
        'subdomain' => current_account.subdomain,
      },
      'userData' => user_data,
      'servers' => servers,
      'enabled' => current_account.radar_enabled?,
      'requestTimeout' => current_account.has_radar_polling_timeout? ? SOCKET_TIMEOUT : nil
    }
  end

  def user_data
    {
      'name' => current_user.name || '',
      'id' => current_user.id || 0,
      'accountId' => current_account.id
    }
  end

  def use_websockets?
    Rails.env.development? || current_account.has_radar_sockets?
  end

  def radar_api_host
    if current_account && current_account.host_mapping.present? && !current_account.ssl_should_be_used?
      current_account.default_host
    end
  end

  def get_chat_manager_token # rubocop:disable Naming/AccessorMethodName
    { 'token' => generate_chat_manager_token }
  end

  def servers
    cluster = Zendesk::Radar::Configuration.cluster_for_shard(current_account.shard_id)

    urls = [ENV['RADAR_URL']] if ENV['RADAR_URL']
    urls ||= cluster['urls']

    urls.map do |url|
      uri = URI(url)
      {
        'host' => uri.host,
        'port' => uri.port || 443 # default to secure
      }
    end
  end

  def expires_at(time)
    time_step = 30.minutes.to_i
    epoch = (time.to_i / time_step).floor
    time_step * epoch + time_step
  end

  def radar_auth_token(time)
    time_step = 30.minutes.to_i
    epoch = (time.to_i / time_step).floor
    encoded_hash("#{(current_user.roles || 0)}|#{current_account.subdomain}#{epoch}")
  end

  def generate_chat_manager_token
    # Make it unique for each time
    encoded_hash("#{(current_user.id || 0)}|#{current_account.subdomain}|#{Time.now.to_i}|#{rand}")
  end

  def encoded_hash(data)
    Base64.encode64(HMAC::SHA512.digest(Zendesk::Configuration.dig!(:node_pubsub, :secret_key), data)).delete("\n")
  end
end
