module StatsHelper
  private

  def extract_stats_options
    options = {}

    [:start, :end].each do |sym|
      options[sym] = Time.zone.at(params[sym].to_i) if params[sym]
    end

    options[:page] = params[:page].try(:to_i)
    options[:per_page] = params[:per_page].try(:to_i)

    if /^\w+$/.match?(params[:order])
      options[:order] = params[:order]

      if params[:desc] == "true"
        options[:order] += ' DESC'
      end
    end

    if /^(\d)+$/.match?(params[:limit])
      options[:page] = 1
      options[:per_page] = params[:limit]
    end

    options[:duration] = params[:duration].to_i if params[:duration]
    options[:interval] = params[:interval].to_i if params[:interval]

    if /^\w+$/.match?(params[:group])
      options[:group] = params[:group]
    end

    options[:stat_name] = params[:stat_name]
    options[:nocache] = params[:nocache]
    options[:precise_time] = params[:precise_time]

    options
  end

  def validate_stats_options
    # return bad request if start time > end time
    if params[:start] && params[:end]
      st_time = Time.zone.at(params[:start].to_i)
      end_time = Time.zone.at(params[:end].to_i)
      if st_time >= end_time
        head(:bad_request)
      end
    end

    if params[:page] && params[:page].to_i < 1
      head(:bad_request)
    end
  end

  def extract_conditions!(stats_scope, options)
    options[:conditions] ||= {}
    options[:conditions_string] ||= []

    stats_scope.valid_conditions.each do |c|
      if params[c] && operator = params["#{c}_operator".to_sym]
        case operator
        when 'gt'
          conditions_operator = '>'
        when 'gte'
          conditions_operator = '>='
        when 'lt'
          conditions_operator = '<'
        when 'lte'
          conditions_operator = '<='
        else
          return
        end

        options[:conditions_string] << "#{c} #{conditions_operator} #{params[c]}"
      elsif params[c]
        options[:conditions][c.to_sym] = params[c]
      end
    end
  end

  ## scoped_stats_association
  ##   transforms [account, "forum_stats_by_account"] ->
  #                account.forum_stats.by_account
  def scoped_stats_association(subject, association)
    name = association.sub(/_by_\w+/, '')
    raise Zendesk::Stats::InvalidStatsRequestError, "requested stat '#{association}' not found" unless subject.class.reflect_on_association(name.to_sym)
    @stats_association = subject.send(name)

    if association =~ /(by_\w+)/
      unless @stats_association.klass.respond_to?($1.to_sym)
        raise Zendesk::Stats::InvalidStatsRequestError, "requested stat '#{association}' not found"
      end
      @stats_association.send($1, subject)
    else
      @stats_association
    end
  end

  def find_subject
    id = params[:object_id].to_i

    @stats_subject =
      case params[:object_type]
      when 'group'
        # Groups use active / inactive, so we need our own scope
        Group.where(account_id: current_account.id, id: id).first
      when 'organization'
        Organization.with_deleted { current_account.organizations.find_by_id(id) }
      when 'agent'
        # Users use active / inactive, so we need our own scope
        User.where(account_id: current_account.id, id: id).first
      when 'forum'
        Forum.with_deleted { current_account.forums.find_by_id(id) }
      when 'entry'
        Entry.with_deleted { current_account.entries.find_by_id(id) }
      when 'category'
        Category.with_deleted { current_account.categories.find_by_id(id) }
      when 'account'
        current_account
      end

    raise Zendesk::Stats::InvalidStatsRequestError, "Requested object not found" unless @stats_subject
  end

  def set_cache_control
    if params[:start]
      headers['Cache-Control'] = "private, max-age=86400"
    end
  end

  def check_view_access!
    # The Api::V2::StatsController is used by forum and search stats
    # :assoc_name is used to define which permissions we should test
    return deny_access if params[:assoc_name].blank?
    assoc_name = params[:assoc_name]
    if assoc_name.include?('search')
      deny_access unless display_analytics?(:search)
    elsif assoc_name.include?('ticket')
      deny_access unless display_analytics?(:ticket)
    else
      deny_access unless display_analytics?(:forums)
    end
  end

  # Forum Stats and Search Stats share the same permissions for users and the same setting option, but distinct arturo config
  def display_analytics?(stat)
    can_view_report = current_user.can?(:view, Report)
    can_view = current_user.can?(:moderate, Entry) || can_view_report
    case stat
    when :ticket
      can_view_report
    when :forums
      can_view && current_account.has_forum_statistics_enabled?
    when :search
      can_view && current_account.has_search_statistics_enabled?
    end
  end

  def check_benchmarking_params
    if params[:object_type].blank? || params[:object_name].blank?
      render_failure(status: :unprocessable_entity, title: 'Incorrect parameters', message: 'You must provide object_type and object_name parameters')
    end
  end

  def shift_time_zone
    Time.use_zone(nearest_hourly_time_zone) do
      yield
    end
  end

  def nearest_hourly_time_zone
    offset = Time.zone.utc_offset
    return Time.zone if offset % 3600 == 0
    offset = 3600 * (offset / 3600.0).round
    ActiveSupport::TimeZone.all.find { |z| z.utc_offset == offset }
  end
end
