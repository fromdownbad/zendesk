module Devices
  module UsersHelper
    private

    def show_devices_tab?
      @user.is_agent?
    end

    def on_devices_tab?
      params[:select] == "devices"
    end
  end
end
