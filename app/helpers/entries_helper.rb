module EntriesHelper
  def clean(string)
    # Copied from action_controller/vendor/html-scanner/html/sanitizer.rb
    # Strip any comments, and if they have a newline at the end (ie. line with
    # only a comment) strip that too
    string.gsub!(/<!--(.*?)-->[\n]?/m, '')
    string.sanitize.strip_tags
  end

  def entry_title(entry)
    if entry.stats_value
      "#{entry.title} (#{number_with_delimiter(entry.stats_value)})"
    else
      entry.title
    end
  end

  def entry_label(entry)
    if entry.forum.display_type.questions?
      "question"
    elsif entry.forum.display_type.ideas?
      "idea"
    else
      "article"
    end
  end

  def meta_description(entry)
    description = [entry.title]
    if entry.forum
      description << entry.forum.title
      if entry.forum.category
        description << entry.forum.category.name
      end
    end
    description.join(". ").truncate(200)
  end

  def meta_keywords(entry)
    keywords = (entry.current_tags || "").split(' ')
    keywords += entry.title.split(' ')
    keywords << current_account.name if current_account
    keywords.map(&:downcase).uniq.join(' ')
  end
end
