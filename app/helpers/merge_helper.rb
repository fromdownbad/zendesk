module MergeHelper
  # Shortcuts
  def locale
    current_account.translation_locale
  end

  def source_ids_string
    params[:source_ids]
  end

  # Data
  def new_collaborator_names
    return '' unless !current_account.has_follower_and_email_cc_collaborations_enabled? && current_account.is_collaboration_enabled?
    new_collaborator_names = @sources.collect do |source|
      # Note that is_follower will return collaborations here and not only followers since follower_and_email_cc_collaborations is disabled
      if source.requester != @target.requester && source.requester != @target.assignee && !@target.is_follower?(source.requester)
        source.requester.name
      end
    end

    # Names from collaborators to be created by the merge separated by commas (localized).
    new_collaborator_names.uniq.compact.to_sentence(locale: locale)
  end

  def new_follower_names
    return '' unless current_account.has_ticket_followers_allowed_enabled?

    collaborator_names(CollaboratorType.FOLLOWER)
  end

  def new_email_cc_names
    return '' unless current_account.has_comment_email_ccs_allowed_enabled?

    collaborator_names(CollaboratorType.EMAIL_CC)
  end

  def default_source_comment
    I18n.t(
      'ticket_fields.merge.source_comment',
      target_nice_id: @target.nice_id,
      target_title:   @target.title(35),
      locale:         locale
    )
  end

  def default_target_comment
    if is_bulk_merge?
      I18n.t(
        'ticket_fields.merge.default_target_comment.bulk',
        source_ids: "##{@source_ids.join(', #')}",
        locale:     locale
      )
    else
      source = @sources.first
      I18n.t(
        'ticket_fields.merge.default_target_comment.non_bulk',
        source_nice_id: source.nice_id,
        source_title:   source.title(35),
        locale:         locale
      ) + source.latest_public_comment.to_s
    end
  end

  # Views
  def this_or_these_tickets_will_be_closed
    if is_bulk_merge?
      I18n.t('txt.admin.views.tickets.merge.show.These_tickets')
    else
      I18n.t('txt.admin.views.tickets.merge.show.This_ticket')
    end
  end

  def you_are_about_to_merge_or_merge_in_bulk
    if is_bulk_merge?
      I18n.t(
        'txt.admin.views.tickets.merge.show.You_are_about_to_merge_bulk',
        list_of_tickets:          @source_ids.join(', #'),
        the_number_of_the_ticket: @target.nice_id
      )
    else
      I18n.t(
        'txt.admin.views.tickets.merge.show.You_are_about_to_merge',
        ticket_number_source: @sources.first.nice_id,
        ticket_into_be_merge: @target.nice_id
      )
    end
  end

  def ccs_and_or_requester_can_see_this_comment
    if current_account.has_comment_email_ccs_allowed_enabled?
      I18n.t('txt.admin.views.tickets.merge.show.requester_and_ccs_can_see_this_comment')
    else
      I18n.t('txt.admin.views.tickets.merge.show.requester_can_see_this_comment')
    end
  end

  private

  def collaborator_names(collaborator_type_to_filter)
    @sources.
      map(&:requester).
      select do |requester|
        is_new_requester = requester != @target.requester

        case collaborator_type_to_filter
        when CollaboratorType.EMAIL_CC
          is_new_requester && !@target.is_email_cc?(requester)
        when CollaboratorType.FOLLOWER
          is_new_requester && requester.is_agent? && !@target.is_follower?(requester)
        end
      end.
      uniq.
      compact.
      to_sentence(locale: locale)
  end
end
