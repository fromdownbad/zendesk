module TargetsHelper
  def sent_messages(target)
    I18n.t('txt.admin.helpers.target_helpers.successful_send', count: target.sent)
  end

  def authorize_url
    @target.authorize_url
  end

  def reauthorize_url
    @target.reauthorize_url
  end

  def add_target(target_class, image, description)
    type = if target_class == UrlTargetV2
      "http_target"
    else
      target_class.name.underscore
    end

    target =
      link_to(image_tag("targets/#{image}"), "/targets/new/#{type}") +
      link_to(target_name_translated(type), "/targets/new/#{type}", class: 'big') +
      content_tag(:p, description)

    content_tag(:li, target)
  end

  def target_name_translated(name)
    I18n.t("txt.admin.views.targets.select_target_to_add.name_of_target_#{name}")
  end

  def translate_actions_targets(actions)
    I18n.t("txt.admin.views.targets.edit.action_#{actions}")
  end

  def show_target_failures?
    current_account.has_targets_v2_failures? && @target.is_a?(UrlTargetV2) && !@target.new_record? && @target.url_target_failures.exists?
  end

  def password_placeholder
    '•' * @target.password.to_s.size
  end
end
