module WatchingsHelper
  def subscribe_link(watched, method)
    link_to_remote I18n.t("txt.watchings.#{method}.link").downcase, {url: "/watchings/#{method}/#{watched.id}?type=#{watched.class.name.downcase}"}, class: 'subscribe', title: I18n.t("txt.watchings.#{method}.title")
  end
end
