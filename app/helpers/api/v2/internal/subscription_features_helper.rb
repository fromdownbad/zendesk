module Api::V2::Internal
  module SubscriptionFeaturesHelper
    private

    def catalog
      @catalog ||= Zendesk::Features::CatalogService.new(current_account).current_catalog
    end

    def plan_type_service
      @plan_type_service ||= Zendesk::Features::PlanTypeService.new(current_account)
    end

    def catalog_addons
      catalog.addons_for_plan(plan_type_service.proposed_plan_type).map(&:name)
    end

    def existing_addons
      current_account.
        subscription_feature_addons.
        non_secondary_subscriptions.
        map(&:name).
        map(&:to_sym)
    end

    def available_addons
      (catalog_addons - existing_addons).to_set
    end
  end
end
