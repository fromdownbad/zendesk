module Api::V2::Internal
  module SecuritySettingsErrorsFormatterHelper
    AGENT_LOGIN_KEYS = Set[:zendesk_login, :google_login, :office_365_login, :remote_login].freeze
    PASSWORD_SETTINGS = Set[
      :password_history_length, :password_length, :password_complexity, :password_in_mixed_case,
      :failed_attempts_allowed, :max_sequence, :disallow_local_part_from_email, :password_duration
    ].freeze

    class ApiError < StandardError; end

    private

    def hashify_errors(errors_array)
      errors_array.each { |error| hashify_error(error) }
      api_errors
    end

    def hashify_error(error)
      case error
      when all_ip_ranges_errors(ip_ip_ranges).try(:find) { |e| e == error }
        add_ip_ranges_error(error)
      when 'Cannot customize password without selecting agent custom policy.'
        add_custom_password_errors(error)
      when 'Cannot update to custom without any password customizations.'
        add_custom_agent_security_policy_id_errors(error)
      when I18n.t('txt.admin.views.settings.security_policy.update.more_than_one_auth_type_selected')
        add_multiple_agent_login_errors(error)
      when 'JWT and SAML remote authentications can\'t have the same priority'
        add_remote_auth_priority_errors(error)
      when 'Cannot disable all remote auths with agent/end-user remote login enabled'
        add_disabled_remote_auths_error(error)
      else
        if error.is_a?(Array) && (error.include?(:saml) || error.include?(:jwt))
          [:saml, :jwt].each { |auth_type| add_remote_auth_errors(error, auth_type) if error.include?(auth_type) }
        else raise(ApiError, error)
        end
      end
    rescue ApiError => e
      handle_unmatched_errors(e.message)
    end

    def api_errors
      @api_errors ||= Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
    end

    def add_remote_auth_errors(remote_auth_errors_array, auth_type)
      remote_auth_errors_array.each do |error|
        begin
          if remote_url_errors('remote_login_url').include? error                            then api_errors[:remote_authentications][auth_type][:remote_login_url] = error
          elsif remote_url_errors('remote_logout_url').include? error                        then api_errors[:remote_authentications][auth_type][:remote_logout_url] = error
          elsif auth_type == :saml && error == 'Fingerprint:  is invalid'                    then api_errors[:remote_authentications][auth_type][:fingerprint] = fingerprint_error_msg
          elsif auth_type == :jwt && error == 'Secret is invalid'                            then api_errors[:remote_authentications][auth_type][:shared_secret] = secret_error_msg
          elsif all_ip_ranges_errors(remote_auth_ip_ranges(auth_type)).try(:include?, error) then add_remote_auth_ip_ranges_error(error, auth_type)
          else raise(ApiError, error) if error != auth_type
          end
        rescue ApiError => e
          handle_unmatched_errors(e.message)
        end
      end
    end

    def fingerprint_error_msg
      I18n.t("txt.admin.models.remote_authentication.invalid_fingerprint")
    end

    def secret_error_msg
      I18n.t("txt.admin.models.remote_authentication.invalid_shared_secret")
    end

    def handle_unmatched_errors(error)
      api_errors[:errors_without_keys] = [] if api_errors[:errors_without_keys].empty?
      api_errors[:errors_without_keys] << error
    end

    def remote_url_errors(url_type)
      [
        I18n.t("txt.admin.models.remote_authentication.#{url_type}_is_http_v3"),
        I18n.t("txt.admin.models.remote_authentication.#{url_type}_is_not_url_v3"),
        I18n.t("txt.admin.models.remote_authentication.#{url_type}_is_not_zendesk_v3"),
        I18n.t("txt.admin.models.remote_authentication.#{url_type}_is_too_long_v4", char_limit: RemoteAuthentication::MAXIMUM_URL_LENGTH)
      ]
    end

    def add_remote_auth_ip_ranges_error(error, auth_type)
      api_errors[:remote_authentications][auth_type][:ip_ranges] = [] if api_errors[:remote_authentications][auth_type][:ip_ranges].empty?
      api_errors[:remote_authentications][auth_type][:ip_ranges] << error
    end

    def add_ip_ranges_error(error)
      api_errors[:ip][:ip_ranges] = [] if api_errors[:ip][:ip_ranges].empty?
      api_errors[:ip][:ip_ranges] << error
    end

    def add_multiple_agent_login_errors(error)
      params[:authentication][:agent].slice(*AGENT_LOGIN_KEYS).each { |k, _| api_errors[:authentication][:agent][k] = error }
    end

    def add_custom_password_errors(error)
      params[:authentication][:agent][:password].each { |k, _| api_errors[:authentication][:agent][:password][k] = error }
      api_errors[:authentication][:agent][:security_policy_id] = error
    end

    def add_custom_agent_security_policy_id_errors(error)
      api_errors[:authentication][:agent][:security_policy_id] = error
      PASSWORD_SETTINGS.each { |password_setting| api_errors[:authentication][:agent][:password][password_setting] = error }
    end

    def add_remote_auth_priority_errors(error)
      [:saml, :jwt].each { |remote_sso| api_errors[:remote_authentications][remote_sso][:priority] = error }
    end

    def add_disabled_remote_auths_error(error)
      [:saml, :jwt].each { |remote_sso| api_errors[:remote_authentications][remote_sso][:is_active] = error }
    end

    def all_ip_ranges_errors(params_ip_ranges)
      return unless params_ip_ranges
      params_ip_ranges.split.map { |range| I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: range) }
    end

    def ip_ip_ranges
      params[:ip][:ip_ranges] if params[:ip]
    end

    def remote_auth_ip_ranges(auth_type)
      params[:remote_authentications][auth_type][:ip_ranges] if params[:remote_authentications][auth_type]
    end
  end
end
