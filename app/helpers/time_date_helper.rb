require 'zendesk/cldr/datetimes/datetime'

module TimeDateHelper
  protected

  def format_date(date, with_time: false, user: current_user, time_zone: nil)
    return nil if date.blank?
    date.to_format(with_time: with_time, time_format: user.time_format_string, time_zone: time_zone || user.time_zone)
  end

  def format_time(date, user = current_user)
    return nil if date.blank?
    date.to_format(format: :time, time_format: user.time_format_string)
  end

  def format_absolute_date(date, time_zone: nil)
    return I18n.t('txt.admin.helpers.time_date_helper.na') if date.blank?
    date.to_format(format: :absolute, time_format: current_user.time_format_string, time_zone: time_zone || "UTC")
  end

  def format_absolute_date_with_utc_alt(date)
    return I18n.t('txt.admin.helpers.time_date_helper.na') if date.blank?
    content_tag(:span, date.to_format(format: :absolute, time_format: current_user.time_format_string), title: date.utc.iso8601)
  end

  def split_time_interval(interval_seconds)
    interval_seconds = interval_seconds.to_i
    hours = interval_seconds / 3600
    minutes = interval_seconds / 60 - hours * 60
    seconds = interval_seconds - (minutes * 60 + hours * 3600)
    [hours, minutes, seconds]
  end

  def format_time_interval(interval_seconds)
    return I18n.t('txt.admin.helpers.time_date_helper.unknown') if interval_seconds.to_i < 0
    hours, minutes, seconds = split_time_interval(interval_seconds)
    arr = []
    arr << I18n.t('txt.admin.helpers.time_date_helper.hours', count: hours) if hours > 0
    arr << I18n.t('txt.admin.helpers.time_date_helper.minutes', count: minutes) if minutes > 0
    arr << I18n.t('txt.admin.helpers.time_date_helper.seconds', count: seconds) if seconds > 0
    arr.compact.join(I18n.t('txt.admin.helpers.time_date_helper.separator'))
  end

  def format_date_with_format(date, locale:, format:)
    zendesk_cldr_dates = Zendesk::Cldr::DateTime.new(locale)
    I18n.localize(date, format: zendesk_cldr_dates.single(format))
  end
end
