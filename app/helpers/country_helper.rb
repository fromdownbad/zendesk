module CountryHelper
  private

  def sort_countries(list, locale)
    collator = ICU::Collation::Collator.new(locale)
    list.sort { |a, b| collator.compare(localized_name(a, locale), localized_name(b, locale)) }
  end

  def localized_name(country, locale)
    # ICU::Locale requires a locale (e.g. 'en-US') instead of just a country code (e.g. 'US')
    # Example: ICU::Locale.new("en-JP").display_country('en') => 'Japan'
    name = ICU::Locale.new("en-#{country[:code]}").display_country(locale)
    name = country[:name] if name.empty?
    name
  end
end
