require 'hmac-sha2'

# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  include BrowserPredicatesHelper
  include ActionView::Helpers::UrlHelper # make mailto work
  include SlugIds
  include Voice::Core::NumberSupport
  include Cms::ViewHelper

  def shared_csrf_token
    tag('meta', name: 'shared-csrf-token', content: shared_session[:csrf_token])
  end

  # avoid having to precompile all our images
  # overrides ActionView::Helpers::UrlHelper#image_path which is aliased as path_to_image
  # required so we'll load images relatively instead of using the asset_host (ZD#2706359)
  def path_to_image(source)
    if source.include?("//") || source.starts_with?("/")
      source
    else
      "#{compute_asset_host('/')}/images/#{source}"
    end
  end

  def is_agent_session? # rubocop:disable Naming/PredicateName
    current_user && current_user.is_agent?
  end

  def reload_flash
    page.replace_html 'flash_messages', partial: 'shared/flash'
  end

  def help_link(partial)
    "".tap do |out|
      out << content_tag(:span, image_tag('question.png', alt: "Help"), class: "question-link")
      out << " #{yield} " if block_given?
      out << render_to_string(partial: 'shared/help/help_container', locals: {content: partial})
    end.html_safe
  end

  def learn_more(title, options = {}, &block)
    if options.delete(:icon)
      title = content_tag(:span, image_tag('question.png', alt: title, title: title), class: "question-link")
    end
    if url = options.delete(:url)
      content_tag(:strong) { content_tag(:a, href: url, target: "_blank", class: "learn_more") { title } }
    elsif block_given?
      dom_id = "learn-more-#{rand(1000)}"
      content = content_tag(:div, capture(&block), id: dom_id, style: 'display:none;')
      inner = content_tag(:a, title, :href => "#", "data-learn-more-block" => "##{dom_id}")
      if options.delete(:no_strong)
        inner + content
      else
        content_tag(:strong, inner) + content
      end
    else
      raise "Need :url or block for content"
    end
  end

  def follow_link
    content_tag(:span, "»".html_safe, class: "follow_link")
  end

  def render_canonical_link
    return '' if current_user && current_user.is_agent?

    if action_name == 'show' && ['entries', 'forums', 'categories'].member?(controller_name)
      if model = instance_variable_get(:"@#{controller_name.singularize}")
        href = model.url
      end
    elsif action_name == 'unauthenticated' && controller_name == 'access'
      href = "#{current_account.url}#{access_unauthenticated_path}" unless current_account.nil?
    elsif current_account.try(:host_mapping).present?
      href = "#{current_account.url}#{URI.escape(request.fullpath.sanitize)}"
    end

    return '' if href.nil?
    content_tag(:link, nil, href: href, rel: 'canonical')
  end

  def anchor(name)
    content_tag :a, "", name: name
  end

  def render_to_string(*args, &block)
    capture do
      render(*args, &block)
    end
  end

  def current_language
    return "en" unless current_user && current_user.locale
    current_user.locale.split(/[_-]/).first
  end

  def available_languages
    current_user.available_languages.select do |language|
      language.id != current_user.translation_locale.id
    end
  end

  def available_languages_for_dropdown
    current_user.available_languages.select do |language|
      language.id != I18n.translation_locale.id
    end
  end

  def seat_types(types)
    return '' unless types.present?

    types.sort.map do |type|
      I18n.t("txt.admin.view.people.users.user.seat_type.#{type}")
    end.join(', ')
  end

  def zendesk_version
    version = {
      tag: GIT_HEAD_TAG,
      sha: GIT_HEAD_SHA.to_s[0...7],
      time: (GIT_TIMESTAMPS["."].in_time_zone("UTC").to_s(:db) + " UTC" if GIT_TIMESTAMPS["."])
    }

    unless Rails.env.production?
      version[:rails_version] = Rails::VERSION::STRING
      version[:ruby_version] = RUBY_VERSION
    end

    version.values.compact.join(" : ")
  end
  module_function :zendesk_version

  def ror_version_and_patch_level
    return "" if !current_user.is_zendesk_agent? && Rails.env.production?
    "Rails #{Rails.version}, Ruby #{RUBY_VERSION}-p#{RUBY_PATCHLEVEL}"
  end

  def meta_lookups
    @meta_lookups ||= {'"' => '&quot;', '\'' => "&#039;" }
  end

  def encode_meta_tag_content(content)
    return unless content
    content.to_utf8.gsub(/["']/) { |c| meta_lookups[c] }
  end

  def css_truncate(content)
    content_tag(:div, content, class: "truncate", title: content)
  end

  def form_tag_with_body(options, *)
    options["method"] = :put if options["method"] == :patch
    super
  end

  # TODO: Replace with dir=rtl, once all Classic views have RTL support integrated
  def render_data_rtl_attribute
    if current_account.try(:has_rtl_lang_html_attr?)
      "data-rtl-language=#{current_user.translation_locale.rtl?}"
    else
      ""
    end
  end
end
