module TagsHelper
  def tags_field(model_name, options = {})
    options[:id] ||= "#{model_name}-tags-input"
    options[:attribute_name] ||= :set_tags
    object = instance_variable_get("@#{model_name}")
    tags = (object && object.current_tags) ? object.current_tags.split : []
    include_content_for(options)
    @lookups ||= {'&' => '&amp;', '<' => '&lt;', '>' => '&gt;', '"' => '&quot;', '\'' => "&#39;" }
    js_code = <<-JAVASCRIPT
          var unescapeTagArray = function(unsafe_tag_array) {
            var safe_tag_array = [];
            unsafe_tag_array.forEach(function(tag){
              safe_tag_array.push(
                tag.replace(/&amp;/g, "&")
                   .replace(/&lt;/g, "<")
                   .replace(/&gt;/g, ">")
                   .replace(/&quot;/g, '"')
                   .replace(/&#39;/g, "'")
              );
            });
            return safe_tag_array;
          };

          if (typeof('#{model_name}TagField') == 'undefined') { var #{model_name}TagField = null; }
          Event.observe(window, 'load', function() {
            function valueTest(value, event) {
              // use "," and <Enter> to separate tags when whitespace is allowed
              return #{options[:allow_spaces] ? "event.keyCode == 188 || event.keyCode == 13" : "true"};
            }
            #{model_name}TagField = new Autocompleter.MultiValue(
              #{options[:id].to_json},
              Autocompleter.cachedLookupTag,
              unescapeTagArray(#{tags.sort.map { |tag| tag.gsub(/[&<>"']/) { |c| @lookups[c] } }}),
              {frequency: 0.3, minChars: 2, acceptNewValues: true, newValueChecker: valueTest});
          });
    JAVASCRIPT
    text_field_tag("#{model_name}[#{options[:attribute_name]}]",
      object.try(:current_tags), options) + javascript_tag(js_code)
  end

  def search_phrases_field(model_name, options = {})
    options[:id] ||= "#{model_name}-search_phrases-input"
    options[:attribute_name] ||= :search_phrases
    object = instance_variable_get("@#{model_name}")
    tags = object && object.search_phrases || {}
    include_content_for(options)
    js_code = get_partial_snippet("#{model_name}PhrasesField", tags.sort, options[:id], true)
    text_field_tag("#{model_name}[#{options[:attribute_name]}]", '', options) + js_code
  end

  def tag_cloud(model, tags)
    css_classes   = %w[nube1 nube2 nube3 nube4 nube5]
    cloud_content = "".html_safe

    model.cloud(tags, css_classes) do |name, count, css_class|
      cloud_content << tag_content(name, count, css_class, model) # The tag element content
      cloud_content << tag(:wbr)                                  # Give browsers a hint that it's OK (even encouraged!) to break here.
    end

    cloud_content << content_tag(:div, "", style: "clear: both;")
  end

  def include_content_for(options = {})
    return if options[:helper_javascript_included]
    content_for(:head) do
      javascript_include_tag "views/entries/entries-edit"
    end
  end

  def tag_encode(tag)
    return tag if tag.blank?
    CGI.escape(tag.gsub('/', '|||')).gsub('.', '%2E')
  end

  def render_static_tags_for(item, limit = false)
    return '' unless item.present? && current_user.is_agent?
    tags = item.tag_array.sort
    if limit && tags.length > limit
      tags_formatted = tags.slice(0, limit - 1).map { |tag| content_tag(:span, tag, class: 'tag property') }
      remaining_tags = tags.slice(limit - 1, 1000).join(' ')
      tags_formatted << content_tag(:span, I18n.t('txt.helpers.tags_helper.more_tags_label', how_many_tags: (tags.length - limit.to_i + 1)), class: 'tag property more', title: remaining_tags)
    else
      tags_formatted = tags.map { |tag| content_tag(:span, tag, class: 'tag property') }
    end
    if (item.is_a?(User) || item.is_a?(Organization)) && item.suspended?
      tags_formatted = [content_tag(:span, I18n.t('txt.helpers.tags_helper.suspended_tag'), class: 'suspended property')] + tags_formatted
    end
    return '' unless tags_formatted.any?
    content_tag(:span, tags_formatted.join_preserving_safety(' '), class: 'tags')
  end

  private

  def get_partial_snippet(model_name, tags, id, allow_spaces)
    render partial: 'entries/tags_field', locals: {
      model_name: model_name,
      tags: tags, id: id,
      allow_spaces: allow_spaces
    }
  end

  def tag_content(name, count, css_class, model)
    tag_link = tag_path(tag_encode(name), for: model.name.downcase)
    title    = I18n.t('txt.admin.helpers.tags_helper.used_tag_count', count: count)
    content_tag(:span, class: 'tag-wrapper', title: title) do
      link_to(name, tag_link, class: css_class) + "&nbsp;".html_safe +
      content_tag(:span, "(#{count})", class: "count", title: title)
    end
  end
end

ActionView::Helpers::FormBuilder.class_eval do
  def tags_field(options = {})
    @template.tags_field(@object_name, options)
  end

  def search_phrases_field(options = {})
    @template.search_phrases_field(@object_name, options)
  end
end
