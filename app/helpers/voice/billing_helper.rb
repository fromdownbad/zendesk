module Voice
  module BillingHelper
    include TimeDateHelper

    protected

    def periods_with_invoice
      periods = []

      current_period = add_initial_invoice_period(periods)

      while current_period < Time.now.utc
        add_next_invoice_period(periods, current_period)
        current_period = current_period.next_month
      end

      periods.reverse # We want them in descending order
    end

    def add_initial_invoice_period(periods)
      # How can we remove the explicit call to voice_sub_account here?
      start = current_account.voice_sub_account.created_at.utc.at_beginning_of_month
      current_period = [start, 5.months.ago.utc.at_beginning_of_month].max

      periods.push(period_object(
        I18n.t('txt.admin.helpers.voice.settings_helper.before_date', date: format_date(current_period)),
        0,
        current_period.to_i
      ))

      current_period
    end

    def add_next_invoice_period(periods, current_period)
      current_period_end = current_period.at_end_of_month

      label = if current_period.at_end_of_month > Time.now.utc
        I18n.t(
          'txt.admin.helpers.voice.settings_helper.date_to_present',
          date: format_date(current_period)
        )
      else
        I18n.t(
          'txt.admin.helpers.voice.settings_helper.date_to_date',
          start_date: format_date(current_period),
          end_date: format_date(current_period_end)
        )
      end

      periods.push(period_object(
        label,
        current_period.to_i,
        current_period_end.to_i
      ))
    end

    def periods_with_payments
      recent_payments = current_account.payments.voice_payments.recent_periods.to_a
      last_period = recent_payments.last.try(:period_begin_at)
      first_period = recent_payments.first.try(:period_end_at)

      periods = recent_payments.map do |payment|
        begin_at = payment.period_begin_at
        end_at = payment.period_end_at

        if (begin_at.to_i...end_at.to_i).cover?(Time.now.to_i)
          first_period = nil
          label = I18n.t('txt.admin.helpers.voice.settings_helper.date_to_present', date: format_date(begin_at))
        else
          label = I18n.t('txt.admin.helpers.voice.settings_helper.date_to_date', start_date: format_date(begin_at), end_date: format_date(end_at))
        end

        period_object(
          label,
          begin_at.to_i,
          end_at.to_i
        )
      end

      periods.unshift(period_object(
        I18n.t('txt.admin.helpers.voice.settings_helper.date_to_present', date: format_date(first_period)),
        first_period.to_i,
        Time.now.to_i
      )) if first_period

      periods.push(period_object(
        I18n.t('txt.admin.helpers.voice.settings_helper.before_date', date: format_date(last_period)),
        0,
        last_period.to_i
      )) if last_period

      periods
    end

    def period_object(label, start_timestamp, end_timestamp)
      {
        label: label,
        range: [start_timestamp, end_timestamp]
      }
    end
  end
end
