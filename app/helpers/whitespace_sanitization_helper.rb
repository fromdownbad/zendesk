module WhitespaceSanitizationHelper
  def to_single_spaces(string)
    string.gsub(/\s+/, ' ').strip
  end

  def strip_between_tags(html)
    String(html).gsub(/>\s+</, "><").strip
  end

  def strip_and_compress_lines(html)
    html.lines.map(&:chomp).map(&:strip).join('')
  end
end
