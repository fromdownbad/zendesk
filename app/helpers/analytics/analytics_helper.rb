module Analytics::AnalyticsHelper
  TABLE_COLUMNS = {
    'string' => 'Search string',
    'searches' => 'Searches',
    'avg_results' => 'Avg. # of results',
    'ctr' => 'CTR %',
    'tickets' => 'Tickets created',
    'top_entry' => 'Top clicked result'
  }.freeze

  def analytics_columns
    TABLE_COLUMNS.map { |identifier, _title| "<col class='#{identifier}' />" }.join.html_safe
  end

  def analytics_view_table_header
    TABLE_COLUMNS.map do |identifier, title|
      translated_title, help = translate_analytics_title_and_help(identifier)
      title = content_tag(:div, translated_title, class: 'title')
      content_tag(:th, title, class: identifier, title: help)
    end.join.html_safe
  end

  private

  def translate_analytics_title_and_help(identifier)
    if identifier == "top_entry"
      key = "txt.admin.helpers.analyticis.analytics_helper.top_clicked_results"
      [I18n.t(key), I18n.t(key)]
    else
      # TODO: change the identifiers to match the translations and rename the classes
      identifier = {
        "tickets" => "tickets_created",
        "top_entry" => "top_clicked",
        "avg_results" => "avg_number_of_results",
        "string" => "search_string"
      }[identifier] || identifier

      [
        I18n.t("txt.admin.helpers.analyticis.analytics_helper.#{identifier}_label"),
        I18n.t("txt.admin.helpers.analyticis.analytics_helper.#{identifier}_help")
      ]
    end
  end
end
