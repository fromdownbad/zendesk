module TriggersHelper
  include TicketSharingHelperMethods

  def sharing_agreements_for_trigger_actions
    eligible_agreements_for_sharing.map do |agreement|
      { value: agreement.id, text: agreement.name }
    end
  end
end
