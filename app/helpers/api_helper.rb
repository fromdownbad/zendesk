module ApiHelper
  protected

  def api_request?
    return true if request.format == :blank
    return true if [Mime[:json], Mime[:js], Mime[:xml], Mime[:rss], Mime[:atom]].include?(request.format)
    false
  end
end
