module MobileHelper
  include Zendesk::MobileSdk::AccountsSupport

  RETINA_LOGO_DIMENSIONS = 114
  MOBILE_LOGO_DIMENSIONS = 55

  def short_status_for(ticket)
    css_class = "badge "
    case ticket.status_id
    when StatusType.NEW
      css_class += "new"
      text = I18n.t('type.status.new')
    when StatusType.OPEN
      css_class += "open"
      text = I18n.t('type.status.open')
    when StatusType.PENDING
      css_class += "pending"
      text = I18n.t('type.status.pending')
    when StatusType.HOLD
      css_class += "hold"
      text = I18n.t('type.status.hold')
    else
      css_class += "solved"
      text = I18n.t('type.status.solved')
    end

    content_tag :span, text, class: css_class
  end

  def status_for(ticket)
    case ticket.status_id
    when StatusType.NEW
      I18n.t('txt.requests.show.mobile.status_new')
    when StatusType.OPEN
      I18n.t('txt.requests.show.mobile.status_open')
    when StatusType.PENDING
      if current_user == ticket.requester
        content_tag :span, I18n.t('txt.requests.show.mobile.status_awaiting_you'), class: 'attention'
      else
        I18n.t('txt.requests.show.mobile.status_awaiting_thirdparty')
      end
    when StatusType.HOLD
      I18n.t('txt.requests.show.mobile.status_awaiting_thirdparty')
    else
      I18n.t('txt.requests.show.mobile.status_solved', date: format_date(ticket.solved_at))
    end
  end

  def show_login_logout_link
    content_tag :span do
      if current_user_exists?
        link_to_action('logout')
      else
        link_to_action('login', 'sign_in')
      end
    end
  end

  def link_to_action(action, key = nil)
    key ||= action
    return_to = return_to_parser.return_to_url(url_for(controller: '/access', action: action))
    link_to(I18n.t("txt.layout.mobile.#{key}"), secure_url_for(controller: '/access', action: action, return_to: return_to))
  end

  def show_go_to_full_site_link_for(controller, opts = {})
    return if controller == 'mobile/landing_page'
    return_to_url = opts.fetch(:return_to, nil)

    content_tag :span do
      link_to I18n.t('txt.layout.mobile.full_site'), mobile_switch_url(return_to: return_to_url)
    end
  end

  def mobile_site_title
    return unless current_account
    if current_account.help_center_enabled?
      current_account.name
    else
      Zendesk::Liquid::DcContext.render(current_account.settings.mobile_title, current_account, current_user) || current_account.name
    end
  end

  def mobile_v2_logo_path
    if current_account.mobile_logo
      cname_wrap(current_account, current_account.mobile_logo.path_for_url)
    else
      cname_wrap(current_account, "/images/mobile/mobile-logo.png")
    end
  end

  def mobile_v2_logo
    image_tag mobile_v2_logo_path, width: mobile_retina_relative_width, height: mobile_retina_relative_height
  end

  def mobile_retina_relative_height
    mobile_retina_relative_dimension(:height)
  end

  def mobile_retina_relative_width
    mobile_retina_relative_dimension(:width)
  end

  def mobile_retina_relative_dimension(type)
    dimension = MOBILE_LOGO_DIMENSIONS
    dimension = current_account.mobile_logo.send(type) if current_account.mobile_logo && !current_account.mobile_logo.send(type).nil?

    dimension = if dimension > MOBILE_LOGO_DIMENSIONS
      ((dimension.to_f / RETINA_LOGO_DIMENSIONS) * MOBILE_LOGO_DIMENSIONS).ceil
    end

    dimension
  end

  def mobile_retina_background_size_string
    width = "55px"
    height = "55px"
    return "#{width} #{height}" if current_account.mobile_logo && (current_account.mobile_logo.width.nil? || current_account.mobile_logo.height.nil?)
    width = "auto" if current_account.mobile_logo && current_account.mobile_logo.width <= MOBILE_LOGO_DIMENSIONS
    height = "auto" if current_account.mobile_logo && current_account.mobile_logo.height <= MOBILE_LOGO_DIMENSIONS

    width  = "#{mobile_retina_relative_width}px" unless mobile_retina_relative_width.nil?
    height = "#{mobile_retina_relative_height}px" unless mobile_retina_relative_height.nil?

    "#{width} #{height}"
  end

  def include_apple_touch_icon_for(account)
    if account.mobile_logo
      icon_path = account.mobile_logo.path_for_url
    elsif account.favicon
      icon_path = account.favicon.path_for_url
    end

    if icon_path
      "<link href='#{icon_path}' rel='apple-touch-icon-precomposed' />".html_safe
    end
  end

  # Augmented link_to
  def mobile_ajax_button(*args)
    name         = args.first
    options      = args.second || {}
    html_options = args.third || {}

    spinner_span = content_tag("span", class: "spinner") do
      image_tag "mobile/spinner.svg"
    end
    text_span = content_tag("span", name, class: "text")

    classes = html_options.key?(:class) ? html_options[:class].split : []
    classes << "button"
    html_options[:class] = classes.join(" ")
    html_options[:"data-ajax-button"] = true
    html_options[:"data-ajax-page"] = 2

    if html_options[:"data-disabled"].nil?
      html_options[:"data-disabled"] = false
    end

    link_to("#{spinner_span}#{text_span}".html_safe, options, html_options)
  end

  # Checks if the account is an enterprise (Extra Large one)
  def enterprise_account?(account)
    account.subscription.plan_type == ZBC::Zendesk::PlanType::Enterprise.plan_type
  end
end
