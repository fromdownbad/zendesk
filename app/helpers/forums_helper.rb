module ForumsHelper
  def incremental_forum_search?
    current_account.has_incremental_forum_search? && current_account.settings.incremental_forum_search
  end
end
