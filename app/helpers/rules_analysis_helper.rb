module RulesAnalysisHelper
  def render_item(item)
    body = I18n.t('txt.admin.helpers.rules_analysis.amount_of_items', type_of_item: item_title(item), ammount_of_items: "<span class='sub-counter'>(#{@rule_stats.size_of(item)})</span>") + follow_link
    if current_account.has_use_get_for_rules_analysis_show?
      content_tag(:h3, link_to(body, rules_analysis_path(id: item)))
    else
      content_tag(:h3, link_to(body, rules_analysis_path(id: item, authenticity_token: form_authenticity_token), class: 'nube1'))
    end
  end

  def render_key(item, key, title, rule_count, do_strong = false)
    if current_account.has_use_get_for_rules_analysis_show?
      path = rules_analysis_path(item, key: key)
      klass = 'strong' if do_strong
    else
      path = rules_analysis_path(item, authenticity_token: form_authenticity_token)
      klass = do_strong ? 'nube1 strong' : 'nube1'
    end

    str = if params[:key] == key
      content_tag(:strong, title)
    else
      if current_account.has_use_get_for_rules_analysis_show?
        link_to(title, path, class: klass)
      else
        link_to(title, path, class: klass, "data-key": key)
      end
    end
    str = [str, "&nbsp;" + content_tag(:span, "(#{rule_count})", class: 'count', dir: 'ltr') + "&nbsp;"].join_preserving_safety("")
    "#{content_tag(:span, str.html_safe, class: 'tag-wrapper')}<wbr />".html_safe
  end

  def render_keys(item)
    keys = sort_keys(@rule_stats.send(item)).collect do |key, stat|
      render_key(item, key, key_title(item, key), stat.rules.size.to_i)
    end
    keys = keys.any? ? keys.join_preserving_safety : render_keys_none
    content_tag(:div, keys, class: 'tagcloud')
  end

  def render_keys_none
    @_render_keys_none ||= "<p>#{I18n.t('txt.admin.lib.zendesk.serialized.definition.None')}</p>".html_safe
  end

  def render_item_with_keys(item)
    render_item(item) + render_keys(item)
  end

  def item_title(item)
    item_titles_for_localization[item]
  end

  def item_titles_for_localization
    @_item_titles_for_localization ||= {
      'group_id'            => I18n.t('txt.admin.helpers.rules_analysis.group_assignment_label'),
      'assignee_id'         => I18n.t('txt.admin.helpers.rules_analysis.agent_assignment_label'),
      'organization_id'     => I18n.t('txt.admin.helpers.rules_analysis.organization_label'),
      'via_id'              => I18n.t('txt.admin.helpers.rules_analysis.channel_label'),
      'recipient'           => I18n.t('txt.admin.helpers.rules_analysis.request_via_email'),
      'tag'                 => I18n.t('txt.admin.helpers.rules_analysis.tag_label'),
      'macro_id'            => I18n.t('txt.admin.helpers.rules_analysis.macro_reference_label'),
      'notification_user'   => I18n.t('txt.admin.helpers.rules_analysis.email_user_label'),
      'notification_group'  => I18n.t('txt.admin.helpers.rules_analysis.email_group_label'),
      'notification_target' => I18n.t('txt.admin.helpers.rules_analysis.notify_target_label')
    }
  end

  def key_title(item, key)
    return '' if key.nil?
    DefinitionItem.new(item, nil, key).humanize_value(current_account)
  end

  def render_matches(item, key, definition, _match)
    item = 'organization' if item == 'organization_id'

    condition_matches = get_matches(item, key, definition.conditions_all + definition.conditions_any)
    action_matches = get_matches(item, key, definition.actions)

    matches = parse_matches(condition_matches, 'conditions') +
      parse_matches(action_matches, 'actions') # .collect {|m| content_tag(:span, m, :class => 'attenuate')}
    h(matches.join_preserving_safety('<br/>'))
  end

  def sort_keys(item)
    item.sort { |a, b| b.last.rules.size <=> a.last.rules.size }
  end

  def render_rules(rules)
    t = []
    [Trigger, Macro, Automation, View].each do |type|
      next if params[:select].present? && params[:select].classify != type.name
      active, inactive = rules.select { |r| r.is_a?(type) }.partition(&:is_active?)
      t << render(partial: "definition_type", locals: {rules: active}) if active.any?
      t << render(partial: "definition_type", locals: {rules: inactive}) if inactive.any?
    end
    t.join_preserving_safety
  end

  private

  def get_matches(item, key, definition_items)
    matches = definition_items.uniq.select { |d| d.source.index(item) }
    if !key.nil? && !RuleDictionary.value_exceptions.member?(key)
      matches = if item.index('tag')
        matches.select { |d| d.value == "" ? d.value == key.to_s : d.value.to_s.split(' ').member?(key.to_s) }
      else
        matches.select { |d| d.value == "" ? d.value == key.to_s : d.value.member?(key.to_s) }
      end
    end
    matches
  end

  def parse_matches(matches, type)
    matches.map { |match| humanize(match, type) }.compact
  end

  def humanize(match, type)
    t = match.humanize(current_account)
    if type == 'actions'
      "#{h(t[0])} <span class='action-attenuate'>=</span> #{h(t[2])}"
    else
      return unless t[0]
      t[0] = t[0].gsub(I18n.t('txt.admin.helpers.rules_analysis.add_tags_label'), I18n.t('txt.admin.helpers.rules_analysis.tags_label'))
      if RuleDictionary.value_exceptions.member?(t[1])
        I18n.t("txt.admin.helpers.rules_helper.suffix.question_mark", value: "#{h(t[0])} #{h(t[1])}".to_s)
      else
        get_translated_definition(t)
      end
    end.html_safe
  end

  def get_translated_definition(title_parts)
    if title_parts[0] == I18n.t('txt.admin.models.rules.rule_dictionary.assignee_label_cap')
      get_key_according_to_assignee_or_requester(title_parts, "txt.helpers.rules_analysis_helper.assignee_")
    elsif title_parts[0] == I18n.t('txt.admin.models.rules.rule_dictionary.requester_label_cap')
      get_key_according_to_assignee_or_requester(title_parts, "txt.helpers.rules_analysis_helper.requester_")
    else
      text = I18n.t("txt.admin.helpers.rules_helper.suffix.question_mark", value: h(title_parts[2]).to_s)
      "#{title_parts[0]} #{title_parts[1]} #{content_tag(:strong, text)}"
    end
  end

  def get_key_according_to_assignee_or_requester(title_parts, key)
    unchaged_value_of_assignee = get_unchaged_value_for_assignee(title_parts[2])
    unchaged_value_of_condition = get_unchaged_value_for_condition(title_parts[1])
    if !unchaged_value_of_assignee.nil?
      begin
        I18n.t("#{key}#{unchaged_value_of_condition}_#{unchaged_value_of_assignee}")
      rescue
        I18n.t("txt.admin.helpers.rules_helper.suffix.question_mark", value: "#{title_parts[0]} #{title_parts[1]} #{content_tag(:strong, title_parts[2])}".to_s)
      end
    else
      begin
        I18n.t("#{key}#{unchaged_value_of_condition}", name_of_user: title_parts[2])
      rescue
        text = I18n.t("txt.admin.helpers.rules_helper.suffix.question_mark", value: title_parts[2].to_s)
        "#{title_parts[0]} #{title_parts[1]} #{content_tag(:strong, text)}"
      end
    end
  end

  def get_unchaged_value_for_assignee(localized_assignee_value)
    hash_with_translated_options_for_assignee = {I18n.t('txt.admin.lib.zendesk.serialized.definition.blank') => "blank",
                                                 I18n.t('txt.admin.helpers.rules_helper.current_user_label') => "current_user",
                                                 I18n.t('txt.admin.helpers.rules_helper.requester_label') => "requester",
                                                 I18n.t('txt.admin.helpers.rules_helper.assignee_label') => "assignee",
                                                 I18n.t('txt.admin.models.rules.rule_dictionary.assigned_group_label') => "assigned_group",
                                                 I18n.t('txt.admin.models.rules.rule_dictionary.current_groups_label') => "current_groups"}
    hash_with_translated_options_for_assignee[localized_assignee_value]
  end

  def get_unchaged_value_for_condition(localized_condition_value)
    hash_with_translated_options_for_condition = {I18n.t('txt.admin.models.rules.rule_dictionary.is_label') => "is",
                                                  I18n.t('txt.admin.models.rules.rule_dictionary.is_not_label') => "is_not",
                                                  I18n.t('txt.admin.models.rules.rule_dictionary.changed_label') => 'changed',
                                                  I18n.t('txt.admin.models.rules.rule_dictionary.changed_to_label') => 'changed_to',
                                                  I18n.t('txt.admin.models.rules.rule_dictionary.changed_from_label') => 'changed_from',
                                                  I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_label') => 'not_changed',
                                                  I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_to_label') => 'not_changed_to',
                                                  I18n.t('txt.admin.models.rules.rule_dictionary.not_changed_from_label') => 'not_changed_from'}
    hash_with_translated_options_for_condition[localized_condition_value]
  end

  def dc_renderer
    @dc_renderer ||= Zendesk::DynamicContent::Renderer.new(current_account)
  end
end
