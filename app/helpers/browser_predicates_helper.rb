module BrowserPredicatesHelper
  private

  def msie?
    !!(request.user_agent =~ /MSIE/i)
  end

  alias_method :is_msie?, :msie?
  alias_method :is_ie?, :msie?
end
