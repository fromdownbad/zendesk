module AccountHelper::ChatSettingsHelper
  def maximum_chat_requests_options
    (1..9).map do |i|
      [I18n.t('txt.admin.helpers.account_helper.chat_settings_helper.calls_per_agent', calls: i), i]
    end.push([I18n.t('txt.admin.helpers.account_helper.chat_settings_helper.unlimited_label'), 1000])
  end
end
