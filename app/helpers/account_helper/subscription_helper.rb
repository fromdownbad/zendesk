module AccountHelper::SubscriptionHelper
  private

  # An account is boostable if it's not a Plus account, it's a paying account and it has not been priorily boosted
  def boostable?(account)
    BOOSTING_ENABLED && Time.now.utc.to_date <= BOOSTING_ON_UNTIL && account.subscription.plan_type != SubscriptionPlanType.Large && account.subscription.registered_with_gateway? && !account.feature_boost.present?
  end

  def boosted?(account)
    account.boosted?
  end

  def boost_expiry_date(_account)
    current_account.feature_boost.valid_until.to_date
  end

  def boost_days_left(account)
    return 0 unless account.boosted?

    [0, (boost_expiry_date(account) - Time.now.to_date)].max
  end

  def voice_opted_out_with_phone_number?
    (@old_voice_optin_status && !@new_voice_optin_status) && current_account.phone_numbers.present?
  end
end
