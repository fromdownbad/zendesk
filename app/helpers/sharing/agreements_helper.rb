module Sharing::AgreementsHelper
  def humanize_status(status)
    status_map = { declined: 'Rejected' }
    status = status_map[status] || status

    I18n.t('txt.admin.helpers.agreements_helper.status_' + status.to_s)
  end

  def permissions_string(agreement)
    comment_permissions_explanation(agreement) << " " << status_and_tags_permissions_explanation(agreement)
  end

  def sender_or_receiver(_agreement)
    @agreement.in? ? I18n.t('txt.admin.helper.agreements_helper.sender') : I18n.t('txt.admin.helper.agreements_helper.receiver')
  end

  private

  def comment_permissions_explanation(agreement)
    if agreement.allows_public_comments?
      I18n.t('txt.admin.helpers.sharing.agreements.public_comments_allowed')
    else
      I18n.t('txt.admin.helpers.sharing.agreements.private_comments_only')
    end
  end

  def status_and_tags_permissions_explanation(agreement)
    if agreement.allows_public_comments? && agreement.sync_tags?
      I18n.t('txt.admin.helpers.sharing.agreements.sync_and_share_tags')
    elsif agreement.allows_public_comments? && !agreement.sync_tags?
      I18n.t('txt.admin.helpers.sharing.agreements.sync_status')
    elsif !agreement.allows_public_comments? && agreement.sync_tags?
      I18n.t('txt.admin.helpers.sharing.agreements.dont_sync_status_and_share_tags')
    else
      I18n.t('txt.admin.helpers.sharing.agreements.dont_sync_status')
    end
  end
end
