module SuspendedTicketsHelper
  def render_cause(cause)
    SuspensionType[cause] ? SuspensionType[cause].localized_name : I18n.t('txt.admin.helpers.suspended_tickets_helper.unknown_label')
  end

  def recover_options(suspended_ticket)
    options = []
    if suspended_ticket.update_with_unknown_author? && suspended_ticket.potential_recover_as_users.present?
      users = [User.new(name: I18n.t('txt.helpers.suspended_tickets_helper.recover_ticket_user_name'))] + suspended_ticket.potential_recover_as_users
      users.each { |user| options << [I18n.t('txt.admin.helpers.suspended_tickets_helper.recover_user_name', user_name: user.name), "user:#{user.id}"] }
    else
      if suspended_ticket.recoverable?
        option = suspended_ticket.ticket.present? ? I18n.t('txt.admin.helpers.suspended_tickets_helper.automatic_recover_as_comment', suspended_ticket_id: suspended_ticket.ticket.nice_id) : I18n.t('txt.admin.helpers.suspended_tickets_helper.automatic_recover_as_new_ticket')
        options << [option, 'default']
      end
    end
    options << [I18n.t('txt.admin.helpers.suspended_tickets_helper.manually_recover_as_ticket_label'), 'new_ticket']
    options_for_select(options)
  end
end
