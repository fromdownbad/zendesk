module AccountsHelper
  def campaign_message(account, source = "webportal")
    text        = I18n.t('txt.admin.helpers.accounts_helper.Help_Desk_Software')
    site_url    = I18n.t('txt.admin.helpers.accounts_helper.marketing_website')
    prefix      = ''
    suffix      = nil

    tracking_params = { utm_medium: "poweredbyzendesk", utm_source: source,
                        utm_campaign: 'text'}
    tracking_params[:utm_content] = account.name if account
    url_with_tracking = add_tracking_params(site_url, tracking_params)
    message_string = I18n.t('txt.admin.helpers.account_helper.accounts_helper.by_zendesk_label_v2',
      link: link_to(text, url_with_tracking, target: "_blank"))

    if account
      if controller_name == 'users' && current_user.is_agent?
        if (['new', 'create'].member?(action_name) && current_user.is_admin?) || (['edit', 'update'].member?(action_name) && @user)
          prefix = '<a href="https://www.zendesk.com/company/customers-partners/#privacy-policy" target="_blank">' + I18n.t('txt.admin.views.account.dropboxes._advanced.Privacy_Policy') + '</a> | '
        end
      end

      return "" if account.id == 22844 # Formspring

      if account.has_image_self_branding?
        tracking_params[:utm_campaign] = "image"
        image_path = I18n.t('txt.admin.helpers.accounts_helper.powered_by_image_name')
        url_with_tracking = add_tracking_params(site_url, tracking_params)
        message_string = "<a href='#{url_with_tracking}' target='_blank' class='zd_branding_logo'><img src='/images/branding/#{image_path}' /></a>"
      end

      if !account.is_serviceable?
        prefix << "#{h(account.name)} &mdash;"
        suffix = I18n.t('txt.admin.helpers.account_helper.accounts_helper.an_expired_zendesk_account')
      elsif account.is_sandbox?
        prefix << "#{h(account.name)} &mdash;"
        suffix = I18n.t('txt.admin.helpers.account_helper.accounts_helper.this_is_a_sandbox')
      end
    end

    [
      prefix,
      message_string,
      suffix
    ].join(" ").html_safe
  end

  def translation_select_box_format(translations)
    collator = ICU::Collation::Collator.new(I18n.translation_locale.locale)
    mapped_translations = translations.map { |locale| [locale.display_name(display_in: I18n.translation_locale), locale.id] }
    mapped_translations.sort { |a, b| collator.compare(a[0], b[0]) }
  end

  def upgrade_detail_partial(plan_type)
    partials = {
      SubscriptionPlanType.Small  => 'solo_details',
      SubscriptionPlanType.Medium => 'regular_details',
      SubscriptionPlanType.Large  => 'plus_details'
    }

    partials[plan_type]
  end

  def render_upgrade_details_for(subscription)
    if partial = upgrade_detail_partial(subscription.plan_type)
      render(partial: "account/subscription/#{partial}", locals: {
        billing_cycle_type: subscription.billing_cycle_type,
        net: subscription.pricing_calculations.net,
        max_agents: subscription.max_agents,
        display_plan_image: true
      })
    end
  end

  def twitter_searches_for_account
    @twitter_searches_for_account ||=
      (twitter_searches_for_everyone + group_twitter_searches).uniq.sort_by do |s|
        s.position.to_i
      end
  end

  def twitter_searches_for_everyone
    @twitter_searches_for_everyone ||=
      valid_twitter_searches.select { |s| s.user_id.blank? && s.group_id.blank? }
  end

  def group_twitter_searches
    @group_twitter_searches ||=
      valid_twitter_searches.select do |s|
        s.group_accessible_by?(current_user)
      end
  end

  def twitter_searches_for_current_user
    @twitter_searches_for_current_user ||=
      valid_twitter_searches.select { |s| s.user_id == current_user.id }.sort_by do |s|
        s.position.to_i
      end
  end

  def twitter_no_saved_searches?
    twitter_searches_for_current_user.empty? && twitter_searches_for_account.empty?
  end

  def twitter_icon_link
    twitter_no_saved_searches? && current_user.is_admin? ? zendesk_channels.twitter_settings_path(anchor: "manage_searches") : "/twitter/search"
  end

  def valid_twitter_searches
    @valid_twitter_searches ||=
      Channels::Twitter::Search.where(account_id: current_account.id).select { |s| !s.new_record? && s.monitored_twitter_handle }
  end

  def help_desk_meta_description_tag
    return if current_account.nil?
    text = current_account.texts.help_desk_description
    tag(:meta, name: 'description', content: text) if text
  end

  def content_for_payment_warning
    if !current_account.is_serviceable?
      title = I18n.t("txt.admin.views.shared._payment_problem.account_suspended.title", account_owner: current_user.first_name)
      message = I18n.t("txt.admin.views.shared._payment_problem.account_suspended.message2", account_path: "/agent/admin/subscription",
                                                                                             account_owner: current_account.owner.name)
    elsif current_account.is_failing?
      if current_account.has_zero_paid_payments? && current_account.try(:voice_sub_account).try(:suspended?)
        title = I18n.t("txt.admin.views.shared._payment_problem.warning_with_voice_suspension.title")
        message = I18n.t("txt.admin.views.shared._payment_problem.warning_with_voice_suspension.message", account_owner: current_account.owner.name)
      else
        title = I18n.t("txt.admin.views.shared._payment_problem.warning.title")
        message = I18n.t("txt.admin.views.shared._payment_problem.warning.message", account_owner: current_account.owner.name)
      end
    end

    content_tag(:h1, title) + content_tag(:div, message) if title && message
  end

  # Adds GA tracking parameters and company name parameter to links
  # Ex: "?utm_medium=poweredbyzendesk&utm_source=email-notification&utm_campaign=text&utm_content=Acme"
  def add_tracking_params(url, params)
    url + (url.include?("?") ? "&" : "?") + params.to_query
  end
end
