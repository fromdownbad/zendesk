module BillingHelper
  # only called from app/views/shared/_plan_info.html.erb
  # Delete this method if that partial isn't used?
  def available_features(subscription)
    features = []
    features << I18n.t("txt.admin.subscriptions.available_features.selectable_language") unless subscription.has_individual_language_selection?
    features << I18n.t("txt.admin.subscriptions.available_features.chat") unless subscription.has_chat?
    features << I18n.t("txt.admin.subscriptions.available_features.business_hours_functionality") unless subscription.has_business_hours?
    features << I18n.t("txt.admin.subscriptions.available_features.extended_metrics") unless subscription.has_extended_ticket_metrics?
    features << I18n.t("txt.admin.subscriptions.available_features.csv_export") unless subscription.has_report_feed?
    features << I18n.t("txt.admin.subscriptions.available_features.sandbox") unless subscription.has_sandbox?
    features << I18n.t("txt.admin.subscriptions.available_features.group_views") unless subscription.has_group_rules?

    features.empty? ? nil : features
  end

  private

  # this is called from app/views/account/subscription/_solo_details.html.erb but it is a `private` method
  # How could this work?
  # Delete this method if that partial isn't used?
  def plan_header(plan)
    tag :img, src: "/images/plan_#{plan.name.downcase.gsub(/\W/, '')}.png", alt: I18n.t("txt.admin.subscriptions.plan_header", plan_name: plan.name)
  end
end
