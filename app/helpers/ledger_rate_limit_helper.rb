module LedgerRateLimitHelper
  private

  def ledger_config
    @ledger_config ||= {
      endpoints: [
        {
          base_url:     Zendesk::Configuration.dig!(:ledger, :base_url),
          access_token: Zendesk::Configuration.dig!(:ledger, :access_token)
        }
      ]
    }
  end

  def build_ledger_event(action, endpoint, user_id)
    {
      actor:       user_id,
      environment: Rails.env,
      id:          endpoint,
      name:        'Zendesk Classic: Conditional Rate Limiting',
      status:      action.to_s,
      started_at:  DateTime.now,
      subdomains:  [subdomain], # i.e. current_account.subdomain
      url:         endpoint
    }
  end

  def ledger
    @ledger_client ||= LedgerClient::ApiClient.new(ledger_config)
  end

  def ledger_log_rate_limit_create(endpoint = nil, user_id = nil)
    ledger.post_event(**build_ledger_event(:create, endpoint, user_id)) if has_ocp_log_crl_events_to_ledger_enabled?
  rescue StandardError => e
    Rails.logger.error "Ledger: Failed to log rate limit creation: #{e.inspect} #{e.message}"
  end

  def ledger_log_rate_limit_destroy(endpoint = nil)
    ledger.post_event(**build_ledger_event(:destroy, endpoint, nil)) if has_ocp_log_crl_events_to_ledger_enabled?
  rescue StandardError => e
    Rails.logger.error "Ledger: Failed to log rate limit delete: #{e.inspect} #{e.message}"
  end
end
