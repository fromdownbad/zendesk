module VoiceHelper
  private

  def voice_feature_enabled?(feature)
    # We experienced some performance difficulties with checking user_seats by making HTTP requests,
    # it was hitting Memcached intensively. Therefore we decided to check whether the user seat is enabled
    # by reaching out the database directly.
    #
    # https://zendesk.atlassian.net/browse/ICTF-80
    #
    if feature.to_sym == :user_seats && voice_account && current_account.has_voice_read_user_seats_from_the_db?
      return voice_account.user_seats_enabled?
    end

    # Github PR https://github.com/zendesk/zendesk/pull/30213 changed mechanism to check feature flag from SQL query
    # to HTTP request for a reason, it helped to reduce unnecessary lookups for business hours but introduced some side effect.
    #
    # As of 2019-05-15 Classic initiates 100M+ HTTP requests to Voice daily, for UsersController#index API call we do
    # up to 100 such HTTP requests to Voice. However Voice returns the same data in 95% cases because of 5 minutes cache TTL
    # https://github.com/zendesk/voice/blob/v5482/app/controllers/api/v2/internal/features_controller.rb#L6
    #
    # This caching block below should help to prevent vast majority of unnecessary HTTP requests, and reduce Classic dependency on Voice
    # hence give both systems more capacity

    cache_key = "voice_feature_#{feature}_for_account_#{current_account.id}"
    Rails.cache.fetch(cache_key, expires_in: 5.minutes, race_condition_ttl: 20.seconds) do
      feature_mapper.feature_enabled?(feature)
    end
  end

  def voice_account
    @voice_account ||= ActiveRecord::Base.on_slave do
      Voice::VoiceAccount.find_by(account_id: current_account.id)
    end
  end

  def voice_sub_account
    feature_mapper.voice_sub_account
  end

  def feature_mapper
    @feature_mapper ||= Voice::FeatureMapper.new(current_account)
  end
end
