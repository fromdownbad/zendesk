module AutomaticAnswers
  module AuthenticationHelper
    private

    def valid_request?(params)
      valid_auth_token?(params)
    end

    def jwt_token_params(params)
      AutomaticAnswersJwtToken.decode(params[:auth_token])
    end

    def valid_auth_token?(params)
      return false if params[:auth_token].blank?
      return false if jwt_token_params(params).nil?

      true
    end
  end
end
