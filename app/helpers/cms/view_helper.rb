module Cms::ViewHelper
  def rule_reference_link(rule)
    link_to(truncate(rule.title, length: 50), rule.lotus_relative_link)
  end

  def rule_reference_owner(rule)
    return unless rule.type == 'Macro' && rule.owner_type == 'User'

    content_tag :div, class: 'dc-owner-information' do
      render_bullet_character + macro_owner(rule.owner)
    end
  end

  def render_bullet_character
    content_tag(:span, "\u2022".force_encoding('utf-8'))
  end

  def macro_owner(owner)
    I18n.t(
      'txt.admin.views.cms.texts.references.macro_owned_by',
      owner: user_link(owner)
    )
  end

  def user_link(user)
    user.is_active? ? link_to(h(user.name), user_path(user.id)) : h(user.name)
  end

  def sort_header(field)
    case field.name
    when "Title"
      get_header_link(field, 'name')
    when "Last Update"
      get_header_link(field, 'updated_at')
    else
      I18n.t(field.key)
    end
  end

  def get_header_link(field, sort_by)
    sort, direction = get_sort_params
    direction = get_sort_direction(sort_by, sort, direction)
    img = get_sort_image(sort_by, sort, direction)
    link_to((h(I18n.t(field.key)) + img).html_safe, sort: sort_by, direction: direction, filter: params[:filter])
  end

  def get_sort_params # rubocop:disable Naming/AccessorMethodName
    sort = params[:sort] || 'name'
    direction = params[:direction] || "ASC"
    [sort, direction]
  end

  def get_sort_direction(sort_by, sort, direction)
    if sort_by == sort && direction == "ASC"
      "DESC"
    else
      "ASC"
    end
  end

  def get_sort_image(sort_by, sort, direction)
    if sort_by == sort
      image_tag("table-arrow#{direction == "DESC" ? '1' : ''}.png", class: 'view_sort')
    else
      ''
    end
  end

  def category_filter_selector(account)
    options = {}

    @category_stats = Cms::Text.category_stats(account)
    if @category_stats.present?
      options[I18n.t('txt.admin.helpers.view_helper.By_category_option_label')] = @category_stats.collect { |key, value| ["#{key.truncate(25)} (#{value.length})", key.to_s] }
    end

    if options.any?
      option_tags = content_tag(:option, I18n.t('txt.admin.helpers.view_helper.Category'), value: '')
      option_tags += grouped_options_for_select(options, selected: params[:category])

      select_tag(:category, option_tags)
    else
      ''
    end
  end

  def dc_tip(type)
    case type
    when "outdated-text"
      "<p>" + I18n.t('txt.admin.helpers.view_helper.Out_of_date_items') + "</p>"
    when "outdated"
      "<p>" + I18n.t('txt.admin.helpers.view_helper.Out_of_date_variants') + "</p>"
    when "default"
      "<p>" + I18n.t('txt.admin.helpers.view_helper.The_Default_variant_for') + "</p>"
    end.html_safe
  end
end
