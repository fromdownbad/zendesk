module UserInterfaceHelper
  JS_ESCAPE_MAP = { "\r\n" => '\n', "\n" => '\n', "\r" => '\n', '"' => '\\"', "'" => "\\'", "\\'" => "\'", '\\' => '\\' }.freeze

  def tip_attribute_text(text, length)
    return "" if text.nil?
    # ZD#244889
    text = text.to_s
    text.scrub!('')
    text = text.truncate(length || 30)
    text.gsub!(/\s*\n\s*\n\s*/, "\n")
    h(text).simple_format
  end

  # Return title, body for ticket hover tips
  def tip_attributes_for(model, is_end_user = false)
    title = body = ''

    case model
    when Ticket
      is_archive_stub = model.is_a?(ZendeskArchive::StubProxy)

      presented_status = model.status
      presented_status = "Open" if model.status == "Hold" && is_end_user

      properties = i18n_status(presented_status)

      should_show_priority = !is_archive_stub && model.priority_id && model.priority_id != 0 && (!is_end_user || model.account.field_priority.is_visible_in_portal)
      properties << "/#{i18n_priority(model.priority)}" if should_show_priority

      properties << '/' + I18n.t("ticket_fields.assigned.status") if model.assignee_id

      title = "#{i18n_ticket_type(model.ticket_type)} ##{model.nice_id} &nbsp;" << content_tag(:span, "(#{properties})", style: 'font-weight:normal')

      description = if is_archive_stub
        model.subject
      else
        model.description
      end
      description ||= I18n.t("txt.requests.index.no_description")

      body = if is_archive_stub || model.latest_comment_mapped.blank? || model.latest_comment_mapped.body == model.description
        tip_attribute_text(description, 600)
      else
        tip_attribute_text(description, 350) +
          tag(:hr) +
          content_tag(:div,
            I18n.t("txt.admin.user_interface_helper.tip_attributes.last_comment",
              date_created: format_date(model.latest_comment_mapped.created_at, with_time: true)),
            class: 'latest_comment_header') +
          content_tag(:div, tip_attribute_text(model.latest_comment_mapped.body, 250), class: 'latest_comment')
      end
    when SuspendedTicket
      title = h(model.title.truncate(80))
      body  = tip_attribute_text(model.content, 600)
    when Entry
      title = h(model.title.truncate(100) + " - " + model.submitter.name)
      body  = tip_attribute_text(model.body.strip_tags, 600)
    when Post
      if model.suspended?
        Entry.with_deleted do
          title = model.entry.title
        end
      else
        title = model.entry.title
      end

      title = I18n.t("txt.moderation.posts.tooltip", title: (title.truncate(80) + " - " + model.user.name))
      body = tip_attribute_text(model.body.strip_tags, 600)
    end

    [escape_javascript_allow_tags(title).html_safe, escape_javascript_allow_tags(body).html_safe]
  end

  def i18n_status(status)
    I18n.t("type.status." + status.downcase).downcase
  end

  def i18n_priority(priority)
    I18n.t("type.priority." + priority.downcase).downcase
  end

  def i18n_ticket_type(ticket_type)
    if ticket_type == 'ticket'
      I18n.t("ticket.name")
    end
    I18n.t("type.ticket." + ticket_type.downcase)
  end

  def escape_javascript_allow_tags(javascript)
    return '' unless javascript
    javascript.clean_separators.to_json.gsub('/', '\/')
  end

  def div_content(type = 'grey', &block)
    raise unless block
    %(<div class="content content_#{type}"><div class="#{type}_box_top"><div class="box box_top"></div></div>).html_safe +
      capture(&block).html_safe +
      %(<div class="#{type}_box_bottom"><div class="box box_bottom"></div></div></div><div class="box_bottom_clear">&nbsp;</div>).html_safe
  end

  def div_side_box(title = nil, &block)
    box_top    = content_tag(:div, "", class: "blue_box_top")
    title      = title ? content_tag(:h3, title) : "".html_safe
    clear      = content_tag(:div, "", style: "clear:left; height:8px;")
    box_bottom = content_tag(:div, "&nbsp;".html_safe, class: "blue_box_bottom")

    content = content_tag(:div, class: "side-box-content") do
      title + capture(&block) + clear
    end

    content_tag(:div, box_top + content + box_bottom)
  end

  def div_side_box_attenuate(title, id, klass, &block)
    moderator_box = content_tag(:div, id: id, class: "side-box-content #{klass}") do
      (title.nil? ? '' : content_tag(:h3, title)).html_safe + capture(&block)
    end
    moderator_box + content_tag(:div, "", style: "clear:both; height:20px;")
  end

  def div_comment_box(style = '', &block)
    raise unless block
    content_tag(:b, class: "rtop r_white #{style}") do
      content_tag(:b, '', class: "r1 r_grey") +
      content_tag(:b, '', class: "r2 r_grey") +
      content_tag(:b, '', class: "r3 r_grey") +
      content_tag(:b, '', class: "r4 r_grey")
    end +
    capture(&block).html_safe +
    content_tag(:b, class: "rbottom r_white") do
      content_tag(:b, '', class: "r4 r_grey") +
      content_tag(:b, '', class: "r3 r_grey") +
      content_tag(:b, '', class: "r2 r_grey") +
      content_tag(:b, '', class: "r1 r_grey")
    end
  end

  def menu_items
    @menu_items ||= []
  end

  def clear_menu_items
    @menu_items = nil
  end

  def render_menu_items(separator = "<span class='delim'>|</span>".html_safe)
    rendered = menu_items.compact.join_preserving_safety(separator)
    clear_menu_items
    rendered
  end

  def tab(name, title, url, options = {}, &block)
    name  = h(name)
    title = h(title)

    content_tag(:li, class: "main clazz tab_#{name}") do
      link_to(title, url, options.merge(class: 'tab')) + (block ? capture(&block) : '')
    end
  end
end
