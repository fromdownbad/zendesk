module DashboardHelper
  def render_dashboard_stats(count, condition, assignment, size = :small)
    output = <<-HTML
      <span class="dashboard-stats #{size}">
        <span class="count">#{count}</span>
        <span>#{condition}</span>
        <span class="you">#{assignment}</span>
      </span>
    HTML
    output.html_safe
  end

  def user_open_ticket(user = current_user)
    user.assigned.where(status_id: StatusType.OPEN).count(:all)
  end

  def new_and_open_tickets_in_group_with_restrictions(user = current_user)
    # note: shouldn't be account.tickets, in order to properly hit the group_id,status_id index.
    return 0 if user.groups.empty?
    Ticket.where(status_id: [StatusType.NEW, StatusType.OPEN], group_id: user.groups.pluck(:id)).count(:all)
  end

  def user_rated_tickets_as_good(user)
    user.satisfaction_ratings_created.good.latest.count(:all)
  end

  def user_rated_tickets_as_bad(user)
    user.satisfaction_ratings_created.bad.latest.count(:all)
  end

  def agent_tickets_received_good_rating(agent)
    agent.tickets_solved_this_week_received_good_rating
  end

  def agent_tickets_received_bad_rating(agent)
    agent.tickets_solved_this_week_received_bad_rating
  end

  def agent_tickets_solved(agent)
    agent.tickets_solved_this_week
  end

  def show_satisfaction_stats?
    current_account.has_customer_satisfaction_enabled? && (current_account.satisfaction_score || current_user.satisfaction_score(:received))
  end

  def user_satisfaction_score(user)
    user.satisfaction_score(:received)
  end

  def open_tickets_assigned_to_user_link(user = current_user)
    path = search_rules_path(
      filter: 'views', sets: build_set([assignee_condition(user), open_condition]),
      search_name: I18n.t('txt.helpers.dashboard_helper.open_tickets_assigned_to_user',
        user_name: user.name),
      authenticity_token: form_authenticity_token
    )
    link = render_dashboard_stats(user_open_ticket(user), "", (user == current_user ? I18n.t('txt.helpers.dashboard_helper.You') : I18n.t('txt.helpers.dashboard_helper.Agent')))
    link_to_unless(user_open_ticket(user).zero?, link, path, class: "dashboard_search_link")
  end

  def open_tickets_in_users_groups_link(user = current_user)
    path = search_rules_path(
      filter: 'views',
      sets: build_set([users_groups, new_and_open_condition, build_condition("is_not", nil, "group_id")]),
      search_name: I18n.t('txt.helpers.dashboard_helper.open_tickets_in_groups',
        user_name: user.name),
      authenticity_token: form_authenticity_token
    )
    link = render_dashboard_stats(new_and_open_tickets_in_group_with_restrictions(user), "", I18n.t('txt.helpers.dashboard_helper.Groups'))
    link_to_unless(new_and_open_tickets_in_group_with_restrictions(user).zero? || user != current_user, link, path, class: "dashboard_search_link")
  end

  def positively_rated_tickets_for_user_link(user = current_user)
    path = good_this_week_satisfaction_path(agent_id: user.id)
    link = render_dashboard_stats(agent_tickets_received_good_rating(user), "", I18n.t('txt.helpers.dashboard_helper.Good'))
    link_to_unless(agent_tickets_received_good_rating(user).zero?, link, path)
  end

  def negatively_rated_tickets_for_user_link(user = current_user)
    path = bad_this_week_satisfaction_path(agent_id: user.id)
    link = render_dashboard_stats(agent_tickets_received_bad_rating(user), "", I18n.t('txt.helpers.dashboard_helper.Bad'))
    link_to_unless(agent_tickets_received_bad_rating(user).zero?, link, path)
  end

  def solved_tickets_for_user_link(user = current_user)
    path = search_rules_path(
      filter: 'views',
      sets: build_set([assignee_condition(user), solved_this_week]),
      search_name: I18n.t('txt.helpers.dashboard_helper.tickets_solved', user_name: user.name),
      authenticity_token: form_authenticity_token
    )
    link = render_dashboard_stats(agent_tickets_solved(user), "", I18n.t('txt.helpers.dashboard_helper.Solved'))
    link_to_unless(agent_tickets_solved(user).zero?, link, path)
  end
end
