module EventsHelper
  # Render event and immediate children
  def describe(event, is_first = false)
    content_tag('ul', describe_if_authorized(event, is_first))
  end

  # Render event - if user is authorized to see it
  def describe_if_authorized(event, _is_first = false)
    if can?(:view, event) && SetLocaleEvent != event.class
      case event
      when VoiceComment, VoiceApiComment
        t = content_tag('li', render_to_string(partial: "/events/voice_comment", locals: {event: event}), class: comment_class(event))
      when FacebookComment
        t = content_tag('li', render_to_string(partial: "/events/facebook_comment", locals: {event: event}), class: comment_class(event))
      when Comment
        t = if request.format == :mobile_v2
          describe_mobile_comment(event)
        else
          describe_comment(event)
        end
      when Cc
        t = content_tag('li', "#{I18n.t('txt.helpers.events_helper.cc_notification_emailed', user: event)} (#{link_to_event(event)})".html_safe, class: 'notification')
      when OrganizationActivity
        t = content_tag('li', I18n.t('txt.app.helpers.events_helper.organization_activity_emailed', user_emailed: event) + " (#{link_to_event(event)})".html_safe, class: 'notification')
      when Notification
        t = content_tag('li', I18n.t('txt.app.helpers.events_helper.notification_emailed', user_emailed: event) + " (#{link_to_event(event)})".html_safe, class: 'notification')
      when Tweet, TwitterDmEvent
        monitored_twitter_handle = event.ticket ? event.ticket.twitter_ticket_source : nil
        t = if monitored_twitter_handle
          I18n.t('txt.helpers.events_helper.tweet_back_to_user_from_user', user: event, from_user: h(monitored_twitter_handle.twitter_profile.screen_name))
        else
          event.is_a?(TwitterDmEvent) ? I18n.t('txt.helpers.events_helper.direct_message_via_twitter', user: event) : I18n.t('txt.helpers.events_helper.tweet_back_to_user', user: event)
        end
        t << " ".html_safe if monitored_twitter_handle
        t << " (#{link_to_event(event)})".html_safe
        t = content_tag('li', t, class: 'tweet')
      when ChannelBackEvent
        t = describe_channel_back_event(event)
      when FacebookEvent
        t = facebook_event(event)
      when External
        t = content_tag('li', I18n.t('txt.app.helpers.events_helper.message_pushed_to_target', target_name: event) + " (#{link_to_event(event)})".html_safe, class: 'notification')
      when Change, Create, Error, MacroReference, CommentPrivacyChange, AuditTrustChange, TranslatableError
        t = content_tag('li', event.to_s(current_user: current_user, current_account: current_account), class: event.class.to_s.downcase) unless event.to_s.blank?
      when Push
        t = content_tag(:li, event.value_reference, class: "title") + event.to_s
      when AttachmentRedactionEvent
        t = content_tag(:li, I18n.t('txt.app.helpers.events_helper.attachment_redaction_message', attachment_id: event.attachment_id))
      when CommentRedactionEvent
        t = content_tag(:li, I18n.t('txt.app.helpers.events_helper.comment_redaction_message', comment_id: event.comment_id))
      when Audit
        # Iterate audit events
        t = event.events.collect do |e|
          describe_if_authorized(e)
        end.join_preserving_safety("\n\n".html_safe)
      else
        begin
          t = render_to_string(
            partial: "events/inline/#{event.class.name.underscore}",
            locals: { event: event }
          )
        rescue ActionView::MissingTemplate
          t = content_tag('li', 'Event type not recognized!', class: 'error')
        end
      end
      t = '' if t.nil?
      if !event.is_a?(Audit) && !(event.via_id == event.audit.via_id && event.via_reference_id == event.audit.via_reference_id)
        via = describe_via(event)
        t << content_tag(:div, via, class: "gray-text-per-event") unless via.blank?
      end
    else
      t = '- not allowed'
    end
    t.html_safe
  end

  # Render Via part of event - rule, linked ticket or push
  def describe_via(event)
    return '' unless current_user && current_user.is_agent?

    description = event.via_description(current_user.translation_locale)

    if event.via?(:linked_problem) && linked_problem = Ticket.find_by_id(event.via_reference_id, select: 'nice_id, id', stub_only: true)
      description = link_to(description, ticket_path(linked_problem))
    end

    if event.via?(:merge) && !event.via_reference_id.blank? && target = Ticket.find_by_id(event.via_reference_id, select: 'nice_id, id, account_id', stub_only: true)
      description = link_to(description, ticket_path(target))
    end

    if event.via?(:admin_setting)
      description = link_to(description, main_app.settings_tickets_path)
    end

    description.blank? ? '' : "(#{description})".html_safe
  end

  def describe_comment(comment)
    items = if comment.to_s == NO_CONTENT
      ''.html_safe
    else
      content_tag('li', comment_value_as_html(comment), class: comment_class(comment))
    end

    if attachment_items = render(partial: '/shared/attachment_list', object: comment.attachments).presence
      items << content_tag(:li, attachment_items, class: 'attachments')
    end
    items
  end

  def describe_mobile_comment(comment)
    items = if comment.to_s != NO_CONTENT
      content_tag('li', comment_value_as_html(comment), class: comment_class(comment))
    else
      ''.html_safe
    end

    items << render(partial: '/shared/screencasts_and_attachments', locals: {attachments: comment.attachments, screencasts: comment.screencasts })
    items
  end

  def comment_value_as_html(comment, options = {})
    case comment
    when VoiceComment, VoiceApiComment
      render_to_string(partial: "/events/voice_comment", locals: {event: comment})
    when FacebookComment
      render_to_string(partial: "/events/facebook_comment", locals: {event: comment})
    else
      render_comment_value(comment, options)
    end
  end

  def render_facebook_attachment(event)
    unless event.data.nil?
      data      = event.data
      post_type = data[:type]
      case post_type
      when 'link'
        attachment = link_to(data[:name], data[:link], title: data[:caption], target: '_blank')
      when 'photo'
        attachment = link_to(I18n.t('txt.facebook_integration.views.comment.see_picture'), data[:link], target: '_blank')
      when 'video'
        name = data[:name]
        name ||= I18n.t('txt.facebook_integration.views.comment.see_video')
        attachment = link_to(name, data[:link], target: '_blank')
      when 'question'
        attachment = link_to(data[:question], data[:link], target: '_blank')
      else
        return nil
      end
      attachment
    end
  end

  def render_comment_value(comment, options = {})
    options[:default_value] ||= "Empty comment"
    return options[:default_value] if comment.blank? || comment.empty?
    if comment.is_a?(VoiceComment)
      comment.display_subject.present
    else
      options[:for_agent] = current_user.is_agent?
      comment.html_body(options) || options[:default_value]
    end
  end

  def comment_class(comment)
    klass = 'say'
    klass << ' agent' if comment.author.is_agent?
    klass << (comment.is_public? ? ' public' : ' private')
    klass << comment_class_via(comment)
    klass
  end

  def comment_class_via(comment)
    return ' twitter' if comment.via?(:twitter) || comment.via?(:twitter_dm)
    return ' facebook' if (comment.via?(:facebook_post) || comment.via?(:facebook_message)) && comment.is_a?(FacebookComment) && !comment.data[:via_zendesk]
    return ' email' if comment.via?(:mail)
    return ' log_me_in_comment' if comment.via?(:logmein_rescue)
    return ' ticket_sharing' if comment.via?(:ticket_sharing)
    return ' voicemail' if comment.via?(:voicemail)
    return ' phone_call_inbound' if comment.via?(:phone_call_inbound)
    return ' phone_call_outbound' if comment.via?(:phone_call_outbound)
    ''
  end

  def user_badge_for_ticket(ticket, event)
    if (event.via?(:twitter) || event.via?(:twitter_dm)) && event.author.is_system_user? && ticket.twitter_ticket_source
      mth = nil
      comment = if event.is_a?(Comment)
        event
      else
        event.is_a?(Audit) ? event.comment : nil
      end
      if comment && comment.value_previous.to_i > 0
        mth = MonitoredTwitterHandle.where(account_id: ticket.account_id).find_by_id(comment.value_previous.to_i)
      end
      render partial: 'shared/monitored_handle_badge',
             locals: { profile_link: (mth || ticket.twitter_ticket_source).twitter_profile_link }
    elsif event.via?(:facebook_post) && event.is_a?(FacebookComment) && event.data[:requester]
      render partial: 'shared/facebook_user_badge', locals: {user: event.data[:requester]}
    elsif event.via?(:facebook_post) && event.is_a?(Audit) && event.comment.is_a?(FacebookComment) && event.comment.data[:type] == "comment" && event.comment.data[:requester]
      render partial: 'shared/facebook_user_badge', locals: {user: event.comment.data[:requester]}
    else
      render partial: 'shared/user_badge', locals: { user: event.author }
    end
  end

  # Deprecated
  def link_to_event(event)
    "##{event.id}"
  end

  # Deprecated
  def events_path(ticket, _filter, _source_id = nil)
    "/tickets/#{ticket.nice_id}"
  end

  def should_show_comment_privacy_link?(event, is_first = false)
    !is_first && event.is_a?(Comment) && current_user.can?(:edit, event) && !event.ticket.try(:inoperable?)
  end

  def is_first_event?(event, first_id, last_id, order) # rubocop:disable Naming/PredicateName
    case order
    when :desc
      event.id == last_id
    when :asc
      event.id == first_id
    end
  end

  def facebook_event(event)
    page_link = link_to event.page.name, event.page.link, target: '_blank'
    type = event.ticket.via?(:facebook_post) ? "post" : "message"
    t = I18n.t("txt.facebook_integration.facebook_event.#{type}", pageLink: page_link).html_safe
    content_tag('li', t, class: 'fbcomment')
  end

  def describe_channel_back_event(event)
    body = link_to_event(event).html_safe
    if event.ticket.via_twitter?
      body = if event.ticket.via?(:twitter_dm)
        I18n.t('txt.helpers.events_helper.direct_message_via_twitter',
          user: "@#{event.value[:requester][:screen_name]}") << " (#{body})".html_safe
      else
        I18n.t('txt.helpers.events_helper.tweet_back_to_user_from_user',
          user: "@#{event.value[:requester][:screen_name]}",
          from_user: event.value[:source][:screen_name]) << " (#{body})".html_safe
      end
    end
    content_tag('li', body, class: 'tweet')
  end

  def screencasts_from(event)
    return [] unless event.present?
    if event.is_a?(Audit)
      return [] unless event.comment.present?
      return event.comment.screencasts
    end
    event.screencasts
  end
end
