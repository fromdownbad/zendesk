module UserIdentitiesHelper
  def self.twitter_properties(profile)
    [].tap do |array|
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Name'), profile.name]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Language'), profile.lang]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Friends_Count'), profile.friends_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Statuses_Count'), profile.statuses_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Likes_Count'), profile.favorites_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Followers_Count'), profile.followers_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Web'), profile.url] unless profile.url.blank?
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Bio'), profile.description] unless profile.description.blank?
    end
  end

  protected

  def can_add_twitter_handle_manually?
    current_user.is_admin? && !@user.is_admin?
  end

  private

  def twitter_properties(profile)
    [].tap do |array|
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Name'), profile.name]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Language'), profile.lang]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Friends_Count'), profile.friends_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Statuses_Count'), profile.statuses_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Likes_Count'), profile.favorites_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Followers_Count'), profile.followers_count]
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Web'), profile.url] unless profile.url.blank?
      array << [I18n.t('txt.admin.helpers.user_identities_helper.Bio'), profile.description] unless profile.description.blank?
    end
  end
end
