module I18n::TranslationsHelper
  module Cldr
    def self.translations_for(*types)
      types.map(&:to_s).each_with_object({}) do |type, translations|
        type_dir = type.tr('_', '')

        require "zendesk/cldr/#{type_dir.pluralize}/#{type_dir}"

        klass = "Zendesk::Cldr::#{type.camelize}".constantize

        klass.new(I18n.locale).all.each do |translation|
          translations.store(*translation.slice(:zid, :translated_name).values)
        end
      end
    end
  end

  def translations(options = {})
    TRANSLATION_FILES.instance.js_keys.each_with_object({}) do |key, t|
      t.store(key, I18n.t(key))
    end.merge!(Cldr.translations_for(*options[:cldr_includes]))
  end

  def rosetta_url
    rosetta_url = "/rosetta/translations_gui"
    rosetta_url << "/#{params[:id]}" if params[:id]
    rosetta_url << "/#{params[:action]}" if show_action?

    Rails.env == "test" ? "http://support.localhost:5100#{rosetta_url}" : rosetta_url
  end

  private

  def show_action?
    params[:action] && params[:action] != "show" && params[:action] != "index"
  end
end
