module PrototypeLegacyHelper
  # Copied from https://github.com/rails/prototype_legacy_helper/blob/master/lib/prototype_legacy_helper.rb#L214-L223
  # to remove the dependency for the Rails 5 upgrade. See there for
  # documentation.
  def form_remote_tag(options = {}, &block)
    options[:form] = true

    options[:html] ||= {}
    options[:html][:onsubmit] =
      (options[:html][:onsubmit] ? options[:html][:onsubmit] + "; " : "") +
      "#{remote_function(options)}; return false;"

    form_tag(options[:html].delete(:action) || url_for(options[:url]), options[:html], &block)
  end

  # Copied from https://github.com/rails/prototype_legacy_helper/blob/master/lib/prototype_legacy_helper.rb#L177-L179
  # to remove the dependency for the Rails 5 upgrade. See there for
  # documentation.
  def link_to_remote(name, options = {}, html_options = nil)
    link_to_function(name, remote_function(options), html_options || options.delete(:html))
  end
end
