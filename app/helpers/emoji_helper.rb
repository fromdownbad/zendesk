module EmojiHelper
  def self.included(_base)
    return if WhiteList.initialized?

    WhiteList.entries = Set.new
    Dir[File.expand_path "./public/images/emojis/*.png"].each do |f|
      WhiteList.entries.add File.basename(f).gsub(/\..*$/, '')
    end
  end

  class WhiteList
    class << self
      attr_accessor :entries

      def lookup?(word)
        entries.include?(word)
      end

      def initialized?
        !entries.nil?
      end
    end
  end

  protected

  def emojify!(text)
    return unless text
    gsubbed = text.to_str.gsub(/<code>.*?<\/code>|<pre>.*?<\/pre>|:([a-z0-9\+\-_]+):/m) do |match|
      if WhiteList.lookup?($1)
        name = CGI.escape($1)
        "<img src='https://static.zdassets.com/classic/images/emojis/#{name}.png' height='20px' width='20px' alt='#{name}' title='#{name}' />"
      else
        match
      end
    end
    text.replace(gsubbed)
  end
end
