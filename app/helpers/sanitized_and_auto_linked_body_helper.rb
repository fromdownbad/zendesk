module SanitizedAndAutoLinkedBodyHelper
  def sanitized_and_auto_linked_body(entry_or_post)
    return '' if entry_or_post.body.blank?
    displayable_body = entry_or_post.sanitize_on_display? ? entry_or_post.body.sanitize : entry_or_post.body
    displayable_body.auto_link(rel: "nofollow") { |link_text| link_text.truncate(70) }.html_safe
  end

  def formatted_sanitized_linked_text(text)
    return '' if text.blank?
    text.sanitize.auto_link { |link_text| link_text.truncate(50) }.html_safe.simple_format
  end
end
