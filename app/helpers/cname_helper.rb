module CnameHelper
  include Zendesk::ReturnTo::Helper

  # Ensures that we prefix a correct host if this is a CNAME request under SSL
  def cname_wrap(account, relative_path)
    if request.ssl? && account && is_cname_request?
      "#{account.url(mapped: false, protocol: 'https')}#{relative_path}"
    else
      relative_path
    end
  end

  def return_to_for_input(_options = {})
    if destination = request.params[:return_to].presence
      CGI.escape(destination)
    end
  end

  def return_to_tag(options = {})
    hidden_field_tag('return_to', return_to_for_input(options))
  end

  def secure_url_for(args)
    if args.delete(:unmapped)
      args[:protocol], args[:host] = current_url_provider.url(ssl: true, mapped: false).split("://")
    else
      determine_host_for_secure_url(args)
    end

    host, port = args[:host].split(':', 2)
    if port
      args[:host] = host
      args[:port] = port
    end

    if args.delete(:challenge) != false && current_user_exists? && args[:host] != request.host_with_port
      args[:challenge] = current_user.challenge_tokens.lookup_by_ip_address(request.remote_ip).value
    end
    url_for(args)
  end

  def secure_form_for(record_or_name_or_array, *args, &block)
    options = args.extract_options!
    options[:url] = secure_url_for((options[:url] || {}).merge(challenge: false))

    return_to = options.delete(:return_to) || {}

    form_for(*(args.unshift(record_or_name_or_array) << options)) do |f|
      content = "".html_safe
      content << return_to_tag(return_to)
      content << hidden_field_tag('challenge', current_user.challenge_tokens.lookup_by_ip_address(request.remote_ip).value) if current_user_exists?
      result = capture(f, &block).to_s
      message = "secure_form_for blocks do not work if they end with an if, add newline before end of block"
      ZendeskExceptions::Logger.record(StandardError.new(message), location: self.class, message: message, reraise: !Rails.env.production?, fingerprint: '757b2729cad23f46628251975a78abac0b7ad518') if result.blank?
      content << result
    end
  end

  private

  def determine_host_for_secure_url(args)
    if controller.send(:ssl_environment?) && !request.ssl?
      args[:protocol], args[:host] = current_url_provider.url(ssl: true).split("://")
    else
      args[:host] = request.host_with_port
    end
  end
end
