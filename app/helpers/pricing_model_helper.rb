module PricingModelHelper
  def small_plan_name
    ZBC::Zendesk::PlanType.plan_name(
      current_account.subscription.pricing_model_revision,
      SubscriptionPlanType.Small
    )
  end

  def medium_plan_name
    ZBC::Zendesk::PlanType.plan_name(
      current_account.subscription.pricing_model_revision,
      SubscriptionPlanType.Medium
    )
  end

  def large_plan_name
    ZBC::Zendesk::PlanType.plan_name(
      current_account.subscription.pricing_model_revision,
      SubscriptionPlanType.Large
    )
  end

  def extra_large_plan_name
    ZBC::Zendesk::PlanType.plan_name(
      current_account.subscription.pricing_model_revision,
      SubscriptionPlanType.ExtraLarge
    )
  end
end
