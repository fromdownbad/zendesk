module Twitter::LinkHelper
  include ActionView::Helpers::TagHelper

  protected

  def twitter_saved_search_path(search)
    "/twitter/search#/?q=#{URI.escape(search.query)}&id=#{search.id}"
  end

  def saved_search_menu_item(search)
    content_tag(:li, class: "clearfix twitter_menu_item", id: "twitter_menu_item_#{search.id}") do
      link_to(ERB::Util.h(search.name), twitter_saved_search_path(search))
    end
  end

  def link_to_more_twitter_searches
    content_tag(:li, link_to('More &#187;'.html_safe, zendesk_channels.twitter_settings_path(anchor: 'manage_searches'), class: 'all_views'))
  end

  def render_shared_twitter_searches
    shared_twitter_searches_links.join.html_safe
  end

  def render_personal_twitter_searches
    personal_twitter_searches_links.join.html_safe
  end

  def shared_twitter_searches_links
    limit = current_account.max_twitter_searches_for_display
    twitter_searches_for_account[0, limit].collect do |search|
      saved_search_menu_item(search)
    end
  end

  def personal_twitter_searches_links
    limit = current_account.max_private_twitter_searches_for_display
    twitter_searches_for_current_user[0, limit].collect do |search|
      saved_search_menu_item(search)
    end
  end
end
