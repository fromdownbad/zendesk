module Settings::BrandingHelper
  def favicon_description
    context = current_account.help_center_enabled? ||
              (current_account.help_center_restricted? && current_account.web_portal_disabled?)

    base = 'txt.admin.views.settings.account._branding'
    key = "help_center#{context ? '_enabled' : '_disabled'}_favicon_description"
    I18n.t("#{base}.#{key}")
  end

  def help_desk_name_does_not_affect_url_message
    url = '<strong>' + current_url_provider.url(mapped: false) + '</strong>'
    I18n.t('txt.admin.views.settings.account._branding.help_desk_name_does_not_affect_url', account_url: url)
  end
end
