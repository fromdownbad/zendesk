module Settings::TicketsHelper
  # first_comment_private_enabled is an account setting to turn on Private Tickets.
  # We need this setting and we can't enforce because existing accounts need to
  # adjust their rules and add ticket privacy conditions when necessary before opt-in.
  #
  # On new accounts, default rules contain the necessary ticket privacy conditions
  # and the setting is on by default.
  # We don't want to let those accounts turn off the setting because we eventually want
  # to, after some announcements, enable setting for everyone and finally remove it.
  #
  # That's why we want to show setting in UI only when:
  #   * the setting is disabled
  #   * this account was created before the rollout date: these are the existing accounts
  #     mentioned above, they enabled the setting and probably need to disable for a reason.
  #   * this is a sandox and master account was created before that date: customers want
  #     a sandbox and master account with same options/behaviour.
  #   * this is a spoke and hub account was created before that date: customers want
  #     a spoke and hub account with same options/behaviour.
  def display_fcp_setting?
    rollout_date = Account::FIRST_COMMENT_PRIVATE_ROLLOUT_DATE
    return true unless current_account.is_first_comment_private_enabled?
    return true if current_account.created_at < rollout_date

    return true if current_account.is_sandbox? &&
      current_account.sandbox_master.created_at < rollout_date

    return true if current_account.spoke? &&
      current_account.subscription.hub_account.created_at < rollout_date

    false
  end
end
