module Settings::SpamPreventionHelper
  def spam_prevention_options(account)
    options = {'data-toggles' => '#sub_setting_spam_prevention_settings'}

    if account.subscription.with_compulsory_spam_prevention?
      options['disabled'] = 'disabled'
    end

    options
  end
end
