module Settings::SecurityHelper
  protected

  def password_allowed_column(role)
    role == :agent ? :agent_password_allowed : :end_user_password_allowed
  end

  def sso_bypass_options
    options = [
      [I18n.t('txt.admin.views.settings.security_policy.show.enforce_sso.sso_bypass.account_owner'), RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER],
      [I18n.t('txt.admin.views.settings.security_policy.show.enforce_sso.sso_bypass.admins'), RoleSettings::REMOTE_BYPASS_ADMINS]
    ]

    if current_account.settings.sso_bypass_can_be_disabled?
      options.unshift([I18n.t('txt.admin.views.settings.security_policy.show.enforce_sso.sso_bypass.disabled'), RoleSettings::REMOTE_BYPASS_DISABLED])
    end

    options
  end

  def sso_bypass_link
    url = "#{current_account.url(mapped: false)}/access/sso_bypass"
    link_to url, url, target: '_blank'
  end

  def download_two_factor_link
    link_to I18n.t('txt.admin.security.settings.two_factor_status.link'), jobs_create_path(type: 'two_factor_export'), method: :post
  end

  def formatted_two_factor_last_update
    format_date_with_format(
      current_account.two_factor_last_update,
      locale: current_user.translation_locale,
      format: I18n.t('datetime.time_format_string.with_12_hours_clock.with_timezone')
    )
  end

  def last_lets_encrypt_status
    @last_lets_encrypt_status ||= current_account.acme_certificate_job_statuses.last
  end

  def lets_encrypt_status
    if !last_lets_encrypt_status
      nil
    elsif last_lets_encrypt_status.present? && last_lets_encrypt_status.completed? && !!current_account.certificates.active.first.try(:autoprovisioned?)
      :success
    elsif last_lets_encrypt_status.pending_worker? || last_lets_encrypt_status.working?
      :pending
    elsif last_lets_encrypt_status.completed? || last_lets_encrypt_status.failed?
      :failed
    end
  end

  def active_cert_is_activating?
    !!(active_cert && Time.now < active_cert.created_at + 15.minutes)
  end
end
