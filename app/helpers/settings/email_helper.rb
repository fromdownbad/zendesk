module Settings
  module EmailHelper
    VERIFICATION_STATUS_ORDER = {verified: 1, warning: 2, failed: 3}.freeze
    EMAIL_PROVIDERS = [
      'gmail.com', 'googlemail.com', 'web.de', 'hotmail.com', 'outlook.com', 'aol.com', 'mail.com', 'yandex.com'
    ].freeze
    EMAIL_PROVIDERS_REGEX = /@(#{EMAIL_PROVIDERS.map { |e| Regexp.escape(e) }.join("|")}|yahoo\..*)\z/

    STATUS_ICON = {
     verified: "checkmark-circle",
     warning: "warning",
     failed: "error"
    }.freeze

    def gmail_error_detail(credential)
      return if credential.error_count == 0

      case credential.last_error_message
      when /Invalid credentials|invalid_grant/ then I18n.t("txt.admin.helpers.settings.email.errors.invalid_credential")
      when /Folder name conflicts with existing folder name/ then I18n.t("txt.admin.helpers.settings.email.errors.folder_conflict", url: 'https://support.google.com/mail/answer/118708')
      when /IMAP access is disabled for your domain/ then I18n.t("txt.admin.helpers.settings.email.errors.imap_disabled", url: 'https://support.google.com/a/answer/105694')
      when /Too many emails/ then I18n.t("txt.admin.helpers.settings.email.errors.email_burst", limit: 500)
      when /Account exceeded command or bandwidth limits|Too many simultaneous connections/ then I18n.t("txt.admin.helpers.settings.email.errors.request_limit")
      when /RateLimitedError/ then I18n.t("txt.admin.helpers.settings.email.errors.request_limit")
      when /BadRequestError/ then I18n.t("txt.admin.helpers.settings.email.errors.invalid_credential")
      when /UnauthorizedError/ then I18n.t("txt.admin.helpers.settings.email.errors.invalid_credential")
      else
        I18n.t("txt.admin.helpers.settings.email.errors.unknown") if credential.need_repair?
      end
    end

    def recipient_address_verifications(recipient_address)
      if !recipient_address.brand.active?
        label = status_icon("failed") << " " << I18n.t("txt.admin.helpers.settings.email.sending_status.invalid_address")
        recipient_address_tooltip label, I18n.t("txt.admin.helpers.settings.email.sending_status.brand_inactive")
      elsif recipient_address.trusted_domain?
        text = status_icon("verified")
        text << I18n.t('txt.admin.helpers.settings.email.forwarding_status.always_verified')

        content_tag(:div, text, class: "ra_verification_check")
      else # detailed results
        display_detailed_verifications(recipient_address)
      end
    end

    def display_detailed_verifications(recipient_address)
      verifications = [
        recipient_forwarding_status(recipient_address),
        recipient_spf_status(recipient_address)
      ]

      verifications << if current_account.has_email_deprecate_cname_checks?
        recipient_domain_verification_status(recipient_address)
      else
        recipient_dns_status(recipient_address)
      end

      verifications.compact!

      output = ""

      verifications.map.with_index do |verification, index|
        status, header, details = verification
        toggle = "ra_verification_#{recipient_address.id}_#{index}"

        button = I18n.t("txt.admin.helpers.settings.email.see_details") << content_tag(:div, "&nbsp;".html_safe, class: "chevron")
        button = link_to_function button, "$j('##{toggle}').toggle();$j(this).toggleClass('expanded')", class: "verifications_toggle"

        text = status_icon(status, header: true)
        text << content_tag(:span, header, class: "verification_header")
        text << " " << button << content_tag(:div, details, id: toggle, style: "display: none", class: "ra_verification_details") if details
        output << content_tag(:div, text, class: "ra_verification_check")
      end

      output.html_safe
    end

    def brands_with_recipient_addresses(page: page = 1)
      brands = current_account.brands.includes(:recipient_addresses)

      # Hide inactive brands with only one address after MB downgrade.
      # Default addresses can't be deleted and we don't want to keep
      # showing inactive brands forever in email page.
      brands = brands.reject { |brand| !brand.active? && brand.recipient_addresses.one? } unless current_account.has_multibrand?

      # Show active brands on top and sorted by name
      brands = brands.sort_by { |brand| [brand.active? ? 0 : 1, brand.name] }

      if current_account.has_email_settings_paginated?
        brands.paginate(per_page: 100, page: page)
      else
        brands
      end
    end

    # [DEPRECATED] remove after :email_settings_paginated gets GA'd
    def should_show_brand_name
      @should_show_brand_name ||= brands_with_recipient_addresses.size > 1
    end

    def recipient_address_actions(address)
      actions = []

      if address.forwarding_verified? && !address.default?
        actions << link_to(
          I18n.t("txt.admin.views.settings.email._settings.support_address_make_default_v2"),
          "javascript:",
          class: "make_default",
          "data-id": address.id
        )
      end
      actions <<
        if address.default?
          recipient_address_tooltip(
            I18n.t("txt.admin.views.settings.email._settings.default.label"),
            I18n.t("txt.admin.views.settings.email._settings.default.description")
          )
        elsif address.backup?
          recipient_address_tooltip(
            I18n.t("txt.admin.views.settings.email._settings.backup.label"),
            I18n.t("txt.admin.views.settings.email._settings.backup.description")
          )
        else
          if address.external_email_credential
            link_title = I18n.t('txt.admin.views.settings.email._settings.external_email_credentials.disconnect')
            id = address.external_email_credential.id
            model_name = 'external-email-credential'
          else
            link_title = I18n.t('txt.admin.views.settings.email._settings.delete_email_v2')
            id = address.id
            model_name = 'recipient-address'
          end
          link_to(
            link_title,
            "#delete-#{model_name}-#{id}",
            class: 'delete colorbox_inline'
          )
        end
      actions << link_to(
        I18n.t('txt.admin.views.settings.email._settings.edit_email'),
        edit_settings_recipient_address_path(address.id),
        class: 'edit_this colorbox'
      )

      actions.map! { |action| content_tag(:li, action.html_safe, class: "address-action") }
      content_tag(:ul, actions.join.html_safe)
    end

    def recipient_address_tooltip(label, description)
      link_to_function(label, nil, rel: "tooltip", title: description)
    end

    def available_brands_for_address(recipient_address)
      ([recipient_address.brand] | current_account.brands.active).compact
    end

    def recipient_addresses_for_brand(brand, page = 1)
      brand.recipient_addresses.
        not_collaboration.
        sort_by(&:position).
        paginate(per_page: 100, page: page)
    end

    private

    def recipient_forwarding_status(recipient_address)
      status, text =
        case recipient_address.forwarding_status
        when :waiting
          [
            :warning,
            I18n.t("txt.admin.helpers.settings.email.forwarding_status.waiting_v2", time: time_ago_in_words(recipient_address.forwarding_sent_at))
          ]
        when :verified
          text = if recipient_address.forwarding_verified_at.to_i == 0
            I18n.t("txt.admin.helpers.settings.email.forwarding_status.verified_plain")
          else
            I18n.t("txt.admin.helpers.settings.email.forwarding_status.verified_v2", time: time_ago_in_words(recipient_address.forwarding_verified_at))
          end
          [:verified, text]
        else # :failed or :unknown
          [
            :failed,
            I18n.t("txt.admin.helpers.settings.email.forwarding_status.failed_v2", time: time_ago_in_words(recipient_address.forwarding_sent_at || Time.now))
          ]
        end

      details = content_tag(:p, I18n.t("txt.admin.helpers.settings.email.forwarding_status.help_text"))
      details << verification_button(recipient_address, "forwarding")

      [status, text, details]
    end

    def recipient_spf_status(recipient_address)
      return if recipient_address.email =~ EMAIL_PROVIDERS_REGEX

      status, text =
        case recipient_address.spf_status
        when :verified
          [:verified, I18n.t("txt.admin.helpers.settings.email.spf_status.verified_v2")]
        when :deprecated
          [:warning, I18n.t("txt.admin.helpers.settings.email.spf_status.deprecated"), true]
        else # :failed or :unknown
          [:warning, I18n.t("txt.admin.helpers.settings.email.spf_status.failed"), true]
        end

      details = content_tag(:p, I18n.t("txt.admin.helpers.settings.email.spf_status.help_text"))
      details << verification_button(recipient_address, "spf")

      [status, text, details]
    end

    def recipient_cname_status(recipient_address)
      return if recipient_address.email =~ EMAIL_PROVIDERS_REGEX

      status, text, help =
        case recipient_address.cname_status
        when :verified
          [:verified, I18n.t("txt.admin.helpers.settings.email.cname_status.verified")]
        else # :failed or :unknown
          [:warning, I18n.t("txt.admin.helpers.settings.email.cname_status.failed"), true]
        end
      help = I18n.t("txt.admin.helpers.settings.email.forwarding_status.learn_more_spf_url") if help
      text = recipient_address_verification_row(recipient_address, "dns", text, help)
      [status, text]
    end

    def recipient_dns_status(recipient_address)
      return if recipient_address.email =~ EMAIL_PROVIDERS_REGEX

      status = dns_worst_status(recipient_address)

      dns_list = if recipient_address.using_cname_verification?
        (1..4).inject("") do |memo, n|
          lookup = recipient_address.metadata&.dig("cname", "zendesk#{n}") || {"lookup_result": "", "status": "failed"}
          memo + dns_row(lookup, "CNAME", "zendesk#{n}.#{recipient_address.domain}", "mail#{n}.#{Zendesk::Configuration.fetch(:host)}")
        end
      else
        (1..4).inject("") do |memo, n|
          memo + ["MX", "TXT"].inject("") do |acc, type|
            lookup = recipient_address.metadata&.dig(type.downcase, "zendesk#{n}") || {"lookup_result": "", "status": "failed"}
            acc + dns_row(lookup, type, "zendesk#{n}.#{recipient_address.domain}", "mail#{n}.#{Zendesk::Configuration.fetch(:host)}")
          end
        end
      end

      dns_list += dns_row(
        recipient_address.metadata&.dig("domain_verification"),
        "TXT",
        "zendeskverification.#{recipient_address.domain}",
        recipient_address.domain_verification_code
      )

      header = if status == :verified
        I18n.t("txt.admin.helpers.settings.email.dns_status.verified")
      else
        I18n.t("txt.admin.helpers.settings.email.dns_status.failed")
      end

      details = content_tag(:p, I18n.t("txt.admin.helpers.settings.email.dns_status.help_text")) << content_tag(:p, I18n.t("txt.admin.helpers.settings.email.dns_status.domain_verification.help_text", default_address: current_account.backup_email_address))
      details << verification_button(recipient_address, "dns") << content_tag(:ol, dns_list.html_safe, class: "verification_detail")

      [status, header, details]
    end

    def recipient_domain_verification_status(recipient_address)
      return if recipient_address.email =~ EMAIL_PROVIDERS_REGEX

      status = recipient_address.domain_verified? ? :verified : :failed

      dns_list = "" << dns_row(
        recipient_address.metadata&.dig("domain_verification"),
        "TXT",
        "zendeskverification.#{recipient_address.domain}",
        recipient_address.domain_verification_code,
        tag: :div
      )

      header = status == :verified ? I18n.t("txt.admin.helpers.settings.email.dns_status.verified") : I18n.t("txt.admin.helpers.settings.email.dns_status.failed")

      details = content_tag(:p, I18n.t("txt.admin.helpers.settings.email.dns_status.help_text")) << content_tag(:p, I18n.t("txt.admin.helpers.settings.email.dns_status.domain_verification.help_text", default_address: current_account.backup_email_address))
      details << verification_button(recipient_address, "dns") << dns_list.html_safe

      [status, header, details]
    end

    def dns_worst_status(recipient_address)
      return :failed unless recipient_address.domain_verified?
      return :verified if recipient_address.all_cnames_valid? || (recipient_address.all_mx_records_valid? && recipient_address.all_txt_records_valid?)
      :warning
    end

    def dns_row(lookup, type, subdomain, expected_record, tag: :li)
      status, text =
        if lookup.present? && lookup["status"] == "verified"
          ["verified",
           I18n.t("txt.admin.helpers.settings.email.dns_status.record_valid", type: type, domain: "<code>#{subdomain}</code>")]
        elsif lookup.nil? || lookup["lookup_result"].blank?
          [
            "failed",
            [
              I18n.t("txt.admin.helpers.settings.email.dns_status.record_missing", type: type, domain: "<code>#{subdomain}</code>"),
              content_tag(:div, I18n.t("txt.admin.helpers.settings.email.dns_status.set_record", domain: "<code>#{expected_record}</code>"), class: "verification_resolution")
            ].join.html_safe
          ]
        else
          [
            "failed",
            [
              I18n.t("txt.admin.helpers.settings.email.dns_status.record_invalid", type: type, domain: "<code>#{subdomain}</code>"),
              content_tag(:div, I18n.t("txt.admin.helpers.settings.email.dns_status.change_record", incorrect: "<code>#{lookup['lookup_result']}</code>", correct: "<code>#{expected_record}</code>"), class: "verification_resolution")
            ].join.html_safe
          ]
        end

      text = status_icon(status) << text
      content_tag(tag, text, class: status.to_s)
    end

    def status_icon(status, options = { header: false })
      icon = STATUS_ICON.key?(status.to_sym) ? STATUS_ICON[status.to_sym] : 'error'

      status_icon_header = options[:header] ? ' status-icon-header' : ''

      "<svg class='verification-status-icon status-#{status}#{status_icon_header}'><use xlink:href='/images/garden_vector_icons.svg#zd-svg-icon-14-#{icon}'></use></svg>".html_safe
    end

    def verification_button(recipient_address, type)
      link = link_to(
        I18n.t("txt.admin.helpers.settings.email.#{type}_status.verify"),
        "#",
        class: "c-btn c-btn--sm c-btn--primary refresh_status", data: {id: recipient_address.id, type: type}
      )
      content_tag(:p, link, class: "verification_button")
    end

    def recipient_address_verification_row(recipient_address, type, text, help)
      text += " "
      text << link_to(
        I18n.t("txt.admin.helpers.settings.email.forwarding_status.retry"),
        "#",
        class: "refresh_status", data: {id: recipient_address.id, type: type}
      )
      if help
        text << " | "
        text << link_to(I18n.t('txt.admin.helpers.settings.email.forwarding_status.learn_more'), help, target: "_blank")
      end
      text
    end
  end
end
