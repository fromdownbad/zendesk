module Settings::CustomerHelper
  protected

  def render_reason_codes
    content_tag(:div) do
      concat(content_tag(:ul) do
        current_account.satisfaction_reasons.each do |reason|
          concat(content_tag(:li) { reason.translated_value })
        end
      end)
    end
  end

  def satisfaction_reasons(account)
    account.satisfaction_reasons.selectable.includes(:brands).map do |reason|
      {
        id: reason.id,
        translated_value: reason.translated_value,
        active: reason.brands.include?(account.default_brand),
        value: reason.value
      }
    end
  end

  def satisfaction_reasons_vendor_css(account)
    Lotus::ManifestManager.new(account).find(:current).initial_css_assets.find { |s| s["name"] == "vendor" }["path"]
  end

  def satisfaction_prediction_setting_content(opts)
    content_tag(:p, class: opts[:class]) do
      concat(opts[:text])
      concat(" ")
      concat(link_to(I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_learn_more_article_label'), I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_learn_more_article_link_1'), target: '_blank'))
    end
  end

  def satisfaction_prediction_feature_available?(account)
    ::Arturo.feature_enabled_for?(:satisfaction_prediction, account)
  end

  def satisfaction_prediction_model_available?(account)
    TicketPrediction.satisfaction_prediction_model_available?(account)
  end

  def satisfaction_prediction_allowed_for_subscription?(account)
    account.subscription.has_satisfaction_prediction?
  end

  def satisfaction_prediction_checkbox_options(account)
    # disabled has to be true when you cannot enable the setting
    # and checked has to be false
    {disabled: !satisfaction_prediction_checkbox_enabled?(account),
     checked: satisfaction_prediction_checkbox_enabled?(account) }
  end

  def satisfaction_prediction_checkbox_enabled?(account)
    satisfaction_prediction_allowed_for_subscription?(account) && satisfaction_prediction_model_available?(account)
  end
end
