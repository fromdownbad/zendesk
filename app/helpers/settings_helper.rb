module SettingsHelper
  include CountryHelper

  def translate_industries
    translated_survey_group("industry")
  end

  def translate_target_audiences
    translated_survey_group("target_audience")
  end

  def translate_customer_counts
    translated_survey_group("customer_count")
  end

  def translate_support_structures
    translated_survey_group("support_structure")
  end

  def translated_survey_group(group_name)
    group_hash = "Account::SurveyResponse::#{group_name.pluralize.upcase}".constantize
    translated_hash = ActiveSupport::OrderedHash.new
    group_hash.each do |_k, v|
      translated_hash[I18n.t("txt.admin.models.account.survey_response.#{group_name}_" + v)] = v
    end
    translated_hash
  end

  def password_history_length_options
    options = [[I18n.t("txt.security_policy.password_history_length.none"), 0]]
    (3..7).each do |number|
      options << [I18n.t("txt.security_policy.password_history_length.previous", number: number), number]
    end
    options
  end

  def password_complexity_options
    [[I18n.t("txt.security_policy.password_complexity.no"), CustomSecurityPolicy::NO_COMPLEXITY],
     [I18n.t("txt.security_policy.password_complexity.numbers"), CustomSecurityPolicy::NUMBERS_COMPLEXITY],
     [I18n.t("txt.security_policy.password_complexity.numbers_special"), CustomSecurityPolicy::SPECIAL_COMPLEXITY]]
  end

  def password_in_mixed_case_options
    [[I18n.t("txt.security_policy.password_in_mixed_case.yes"), true],
     [I18n.t("txt.security_policy.password_in_mixed_case.no"), false]]
  end

  def session_timeout_options
    options = []
    [5, 10, 15, 20, 25, 30].each do |number|
      options << [number.to_s, number]
    end

    options << [I18n.t("txt.security_policy.session_timeout.8_hours"), Zendesk::AuthenticatedSession::DEFAULT_SESSION_DURATION / 60] # 8 hours
    options << [I18n.t("txt.security_policy.session_timeout.2_weeks"), Zendesk::AuthenticatedSession::TWO_WEEK_DURATION]
  end

  def max_chars_numbers_in_sequence_options
    options = [[I18n.t('activerecord.attributes.custom_security_policy.max_consecutive_options.unlimited'), nil]]

    [3, 4, 5].each do |number|
      options << [number.to_s, number]
    end

    options
  end

  def disallow_local_part_options
    [
      [I18n.t("activerecord.attributes.custom_security_policy.must_not_include_the_local_part_options.yes"), true],
      [I18n.t("activerecord.attributes.custom_security_policy.must_not_include_the_local_part_options.no"), false]
    ]
  end

  def password_duration_options
    options = []
    [30, 60, 90, 180, 365].each do |number|
      options << [number.to_s, number]
    end
    options << [I18n.t("txt.security_policy.password_duration.never"), 0]
  end

  def others_state_field_value
    return nil unless country = current_account.try(:address).try(:country)
    ["United States", "Canada"].exclude?(country.name) ? current_account.address.state : nil
  end

  def format_states(country_name)
    country = Country.find_by_name(country_name)
    return nil unless country && country.states
    country.states.map { |state| [state[:name], state[:name]] }
  end

  def format_countries
    countries = Country.unrestricted.collect { |country| [localized_name(country, locale), country.id] }
    collator = ICU::Collation::Collator.new(locale)
    countries.sort! { |a, b| collator.compare(a[0], b[0]) }
  end

  def locale
    I18n.translation_locale.locale
  end

  def link_to_authorize_gmail_for_external_email_credentials(text, credential_id, options = {})
    initial_import = (credential_id ? false : ExternalEmailCredential::DEFAULT_INITIAL_IMPORT)
    url = oauth_start_external_email_credentials_path(id: credential_id, initial_import: initial_import)
    warning = I18n.t("txt.admin.views.settings.email._settings.external_email_credentials.confirm_warning_v3")
    link_to text, url, options.reverse_merge(data: { confirm: warning }, target: "_parent", id: "authorize_gmail_link")
  end

  def personalized_address_example(recipient_address)
    recipient_address.name = recipient_address.name.to_s
    recipient_address.email = recipient_address.email.to_s
    recipient_address.forwarding_verified_at = Time.current

    ticket = Ticket.new do |t|
      t.id = 123
      t.original_recipient_address = recipient_address.email
      t.account = current_account
      t.override_recipient_address = recipient_address
    end

    address = Zendesk::Mailer::TicketAddress.new(
      account: current_account,
      author: current_user,
      ticket: ticket
    )
    [address.from, address.reply_to(false).sub(/\+id123@/, "@")].map { |e| Mail::Encodings.value_decode(e) }
  end

  def show_upgrade_to_hc?
    [SubscriptionPlanType.LARGE, SubscriptionPlanType.EXTRALARGE].include?(current_account.subscription.plan_type)
  end

  def api_token_value(token)
    token.visible_value
  end

  def api_used_string(token)
    update_string = if !token.last_used
      I18n.t('txt.admin.views.settings.api._settings.never_used')
    elsif token.last_used > 60.minutes.ago
      I18n.t('txt.admin.views.settings.api._settings.api_token_used_60_min_ago')
    else
      I18n.t('txt.admin.views.settings.api._settings.api_token_used_more_60_min_ago')
    end

    "<i>#{update_string}</i>".html_safe
  end

  def created_api_token(token)
    "#{I18n.t('txt.admin.views.settings.api._settings.api_token_created')} #{token.created_at.to_format(format: :absolute)}".html_safe
  end

  def locale_html_id(locale)
    "locale_check_#{locale[:id]}"
  end
end
