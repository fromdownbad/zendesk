require 'zendesk_rules/custom_rule_field'

module RulesHelper
  private

  include Zendesk::Rules::DynamicContent
  # Ticket count in views
  def describe_count(view)
    cached_count = view.ticket_count(current_user)
    if cached_count.fresh? && cached_count.value == 0
      nil
    else
      "(#{cached_count.pretty_print})"
    end
  end

  def js_array(values)
    deep_escape(Array(values)).to_json.html_safe
  end

  def bulk_ticket_update_options
    options = []
    options << [I18n.t('txt.admin.helpers.rules_helper.update_selected_tickets'), ""]
    options << [I18n.t('txt.admin.helpers.rules_helper.merge_into_another_ticket'), "merge"] if current_user.can?(:bulk_merge, Ticket)
    options << [I18n.t('txt.admin.helpers.rules_helper.delete_selected_ticket'), "delete"] if current_user.can?(:delete, Ticket)
    options_for_select(options)
  end

  def system_feature_url_for(subject)
    case subject.feature_identifier
    when 'salesforce_integration'
      settings_extensions_path(anchor: "crm")
    else
      "#"
    end
  end

  def ticket_field_value(ticket, field, trunc = nil)
    value = field.value(ticket)

    if field.is_a?(ZendeskRules::CustomRuleField)
      return value.truncate(trunc || 26) unless field.ticket_field
      if field.ticket_field.is_a?(FieldCheckbox)
        value = (value == '1' ? I18n.t('txt.helpers.rules_helpers.yes_custom_ticket_field') : '-')
      elsif field.ticket_field.is_a?(FieldTagger) && !value.blank?
        unrendered_text = ticket.account.field_taggers_cache[value]
        value = Zendesk::Liquid::DcContext.render(unrendered_text, current_account, current_user)
      elsif field.ticket_field.is_a?(FieldDate) && !value.blank?
        value = I18n.localize(Date.parse(value), format: I18n.t("date.formats.short")) rescue nil
      end
      return value.blank? ? '-' : value.truncate(trunc || 26)
    end

    case field.identifier
    when :status
      value = StatusType.OPEN if current_user.is_end_user? && value == StatusType.HOLD
      StatusType[value].localized_name
    when :subject
      url = ticket_path_for_collection(ticket.nice_id)
      content_tag(:span, id: "#{ticket.id}_subj") do
        link_to(ticket.title_for_role(current_user.is_agent?, 135), url)
      end
    when :type
      value ? TicketType[value].localized_name : '-'
    when :priority
      color_code_priority(ticket)
    when :group
      CGI.escapeHTML((ticket.group.try(:name) || '-').truncate(trunc || 15))
    when :assignee
      user = ticket.send(field.identifier)
      user.nil? ? '-' : CGI.escapeHTML(user.safe_name(current_user.is_agent?).truncate(trunc || 21))
    when :requester, :submitter
      user = ticket.send(field.identifier)
      user.nil? ? '-' : user.safe_name(current_user.is_agent?).truncate(trunc || 21)
    when :organization
      (ticket.organization.try(:name) || '-').truncate(trunc || 21)
    when :score
      color_code_score(ticket, '&nbsp;&nbsp;&nbsp;'.html_safe)
    when :due_date
      value ? format_date(value) : '-'
    when :updated_by_type
      ticket.updated_by_type == 'Agent' ? I18n.t('txt.admin.models.user.roles.role_name_short_agent_label') : I18n.t('txt.admin.models.user.roles.role_name_short_user_label')
    when :satisfaction_score
      SatisfactionType[value].localized_name
    when :ticket_form
      CGI.escapeHTML(field.humanized_value(ticket)).truncate(trunc || 21)
    when :brand
      (ticket.brand.try(:name) || '-').truncate(trunc || 21)
    when :via
      ViaType[value].localized_name
    else
      if value.is_a?(Time)
        if value.in_time_zone < Time.now.in_time_zone.at_beginning_of_day ||
           value.in_time_zone > Time.now.tomorrow.in_time_zone.at_beginning_of_day
          format_date(value)
        else
          format_time(value)
        end
      elsif value.blank?
        '-'
      else
        value.to_s.truncate(trunc || 26)
      end
    end.html_safe
  end

  def view_header(field, sorting_allowed = false)
    full_title = field.title(current_user.is_agent?)
    rendered_title = Zendesk::Liquid::DcContext.render(full_title, current_account, current_user)
    content = h(rendered_title.truncate(13))

    if sorting_allowed && Zendesk::Fom::Field.for(field.identifier).sortable?(current_user.is_agent?)
      desc = img = ''
      if params[:order].to_s == field.identifier.to_s
        desc = '1' if params[:desc].blank?
        img = image_tag("table-arrow#{desc}.png", class: 'view_sort')
      elsif ZendeskRules::RuleField.lookup(field.identifier).value_class_symbol == :Time
        desc = '1'
      end
      content = link_to(h(content) + img, params.merge(order: field.identifier, page: '1', desc: desc))
    end
    th_options = { class: field.identifier }

    full_title = Zendesk::Liquid::DcContext.render(full_title, current_account, current_user)

    th_options[:title] = full_title if full_title != content
    content_tag(:th, content, th_options)
  end

  def render_view_tickets_table_ticket_rows_with_grouping(tickets, fields, output, options)
    if output.group && params[:order].nil? && grouping_field = ZendeskRules::RuleField.lookup(output.group)
      grouped_tickets = tickets.group_by { |ticket| grouping_field.grouping_value(ticket) || '' }

      # apply sorting to a group based on specified order_value
      if output.order && order_field = ZendeskRules::RuleField.lookup(output.order)
        sorter = order_field.in_memory_order(output.order_asc ? :asc : :desc)
      end
      grouped_tickets.map do |_group, group_tickets|
        group_tickets.sort!(&sorter) unless sorter.nil?
        render_view_tickets_table_group_row(group_tickets.first, fields, grouping_field) +
        render_view_tickets_table_ticket_rows(group_tickets, fields, options)
      end.join_preserving_safety
    else
      render_view_tickets_table_ticket_rows(tickets, fields, options)
    end
  end

  def week_time_label(time)
    date = time.to_date
    year = date.year
    week = date.cweek

    current_date = Time.zone.now.to_date
    current_year = current_date.year
    current_week = current_date.cweek

    time_is_in_current_week = (week == current_week && year == current_year)

    label = if year != current_year
      I18n.t("txt.admin.helpers.rules_helper.week_number_label_year", week_number: week, year: year)
    else
      key = time_is_in_current_week ? "txt.admin.helpers.rules_helper.week_current_week" : "txt.admin.helpers.rules_helper.week_number_label"
      I18n.t(key, week_number: week)
    end

    label
  end

  def render_view_tickets_table_group_row(ticket, fields, grouping_field)
    grouping_colspan = current_user.is_end_user? ? fields.length : fields.length + 1

    label = if grouping_field.value_class_symbol == :Time
      if time = grouping_field.value(ticket)
        week_time_label(time)
      else
        case grouping_field.identifier
        when :due_date then I18n.t('txt.admin.helpers.rules_helper.not_set_label')
        when :assigned then I18n.t('txt.admin.helpers.rules_helper.not_assigned_label')
        when :updated  then I18n.t('txt.admin.helpers.rules_helper.not_updated_label')
        else
          '-'
        end
      end
    else
      ticket_field_value(ticket, grouping_field, 200)
    end

    content_tag(:tr, class: 'group_by') do
      content_tag(:td, colspan: grouping_colspan.to_s) do
        (Zendesk::Liquid::DcContext.render(grouping_field.title(current_user.is_agent?), current_account, current_user) + ' ' + content_tag(:strong, label)).html_safe
      end
    end
  end

  def render_view_tickets_table_ticket_rows(tickets, _fields, options)
    tickets.map do |ticket|
      render partial: 'shared/tickets_table_ticket', locals: {ticket: ticket, fields: @fields, options: options}
    end.join_preserving_safety
  end

  def color_code_priority(ticket)
    if ticket.closed?
      ticket.priority
    else
      content_tag(:span, PriorityType[ticket.priority_id].localized_name, class: "priority_#{ticket.priority.downcase}")
    end
  end

  def assignee_condition(user)
    build_condition("is", user.id, "assignee_id")
  end

  def requester_condition(user)
    build_condition("is", user.id, "requester_id")
  end

  def users_groups
    build_condition("is", "current_groups", "group_id")
  end

  def working_condition
    statuses = [StatusType.NEW, StatusType.OPEN, StatusType.PENDING]
    statuses <<  StatusType.HOLD if current_account.use_status_hold?

    statuses.map do |status_type|
      build_condition("is", status_type, "status_id")
    end
  end

  def open_condition
    build_condition("is", StatusType.OPEN.to_s, "status_id")
  end

  def new_and_open_condition
    build_condition("less_than", StatusType.PENDING.to_s, "status_id")
  end

  def solved_condition
    [StatusType.SOLVED, StatusType.CLOSED].map do |status_type|
      build_condition("is", status_type, "status_id")
    end
  end

  def good_rating_condition
    build_condition("is", SatisfactionType.GOOD, "satisfaction_score")
  end

  def good_rating_with_comment_condition
    build_condition("is", SatisfactionType.GOODWITHCOMMENT, "satisfaction_score")
  end

  def bad_rating_condition
    build_condition("is", SatisfactionType.BAD, "satisfaction_score")
  end

  def bad_rating_with_comment_condition
    build_condition("is", SatisfactionType.BADWITHCOMMENT, "satisfaction_score")
  end

  def build_condition(operator, value, source)
    {operator: operator, value: {"0" => value}, source: source}
  end

  def solved_this_week
    # rules want an "hours since"; padded with 1 for any tickets solved right on the boundary of the start_of_week
    build_condition("less_than", ((Time.now - current_account.start_of_week) / 3600).to_i + 1, "SOLVED")
  end

  def rule_sort_selector(rule_type)
    options = [
      [sort_alphabetically? ? I18n.t('txt.admin.helpers.rules_helper.sorted_alphabetically') : I18n.t('txt.admin.helpers.rules_helper.sorted_by_position'), nil],
      [I18n.t('txt.admin.helpers.rules_helper.sorted_by_creation_latest_first'), "created_at"],
      [I18n.t('txt.admin.helpers.rules_helper.sorted_by_updated_latest_first'), "updated_at"]
    ]

    usage_options = [
      [I18n.t('txt.admin.helpers.rules_helper.sorted_by_usage_over_last_hour'), "execution_count_hourly"],
      [I18n.t('txt.admin.helpers.rules_helper.sorted_by_usage_over_last_24_hours'), "execution_count_daily"],
      [I18n.t('txt.admin.helpers.rules_helper.sorted_by_usage_over_last_7_days'), "execution_count_weekly"]
    ]

    if ['automations', 'triggers', 'macros'].include?(rule_type)
      options += usage_options
    end
    option_tags = options_for_select(options, selected: params[:sort])
    content_tag(:select, option_tags, id: 'rule-sort')
  end

  def rule_filter_selector(rule_type)
    if ['views', 'macros'].member?(rule_type)
      options = {}

      if current_account.subscription.has_group_rules? && current_account.groups.count(:all) > 0
        options[I18n.t('txt.admin.helpers.rules_helper.by_group_label')] = current_account.groups.collect { |g| ["#{g.name.truncate(25)} (#{g.send(rule_type).count(:all)})", "group_id_#{g.id}"] }
        options[I18n.t('txt.admin.helpers.rules_helper.by_group_label')] << [I18n.t('txt.admin.helpers.rules_helper.no_group_label'), 'group_none']
      end

      if options.any?
        option_tags = content_tag(:option, I18n.t('txt.admin.helpers.rules_helper.all_shared_rule_type_' + rule_type), value: '')
        option_tags << grouped_options_for_select(options, selected: params[:select]).html_safe

        content_tag(:select, option_tags, id: 'rule-select')
      else
        ''
      end
    end
  end

  def rule_links(rule_type, for_tabs = false)
    return [] unless ['views', 'macros'].member?(rule_type)
    return [] unless current_user.can?(:manage_personal, rule_type.capitalize.singularize.constantize)
    if for_tabs
      return [link_to(I18n.t('txt.admin.helpers.rules_helper.shared_label').html_safe, params.merge(select: nil), class: (params[:select] != 'personal' ? 'current' : '')),
              link_to(I18n.t('txt.admin.helpers.rules_helper.yours_label').html_safe, params.merge(select: 'personal'), class: (params[:select] == 'personal' ? 'current' : ''))]
    else
      return [link_to_if(params[:select] == 'personal', I18n.t('txt.admin.helpers.rules_helper.shared_low_caps_label').html_safe, params.merge(select: nil)),
              link_to_if(params[:select] != 'personal', I18n.t('txt.admin.helpers.rules_helper.yours_low_caps_label').html_safe, params.merge(select: 'personal'))]
    end
  end

  def can_create_rule?(rule_type)
    klass = rule_type.capitalize.singularize.constantize
    case rule_type
    when "triggers", "automations"
      current_user.can?(:edit, klass)
    when "macros", "views"
      if params[:select] == 'personal'
        current_user.can?(:manage_personal, klass)
      else
        can_manage_group_or_shared?(klass)
      end
    end
  end

  def filtered?
    (current_account.settings.skill_based_filtered_views || []).include?(@rule.id)
  end

  def can_edit_rule?
    @rule.new_record? ||
      current_user.can?(:edit, @rule) && !@rule.app_installation.present?
  end

  def can_manage_group_or_shared?(klass)
    current_user.can?(:manage_group, klass) || current_user.can?(:manage_shared, klass)
  end

  def groups_for_rule(klass)
    current_user.can?(:manage_shared, klass) ? current_account.groups.active : current_user.groups.active
  end

  def app_installation_title
    I18n.t(
      "txt.admin.#{@rule.rule_type}.app_requirement.title",
      title: @rule.app_installation['settings']['title']
    )
  end

  def allow_rules_sort?
    return false if sort_alphabetically?
    params[:select] == 'personal' || (allow_rule_manipulations? && params[:select].blank? && params[:sort].blank?)
  end

  def rule_title(rule)
    if rule.is_a?(View)
      link_to rule.rendered_title, rule_path(rule)
    else
      h(rule.rendered_title)
    end
  end

  def rule_label(rule)
    return '' unless rule.owner.is_a?(Group)
    content_tag(:span, rule.owner.name, class: 'property')
  end

  def recipient_emails(rule)
    return [] unless rule.definition.present?
    rule.definition.items.select { |i| i.source.to_s == "recipient" }.flat_map(&:value)
  end

  def build_set(conditions, index = "1")
    set = {index => {conditions: {}}}

    if conditions.is_a?(Array)
      conditions.flatten!
    else
      conditions = [conditions]
    end

    conditions.each_with_index do |condition, i|
      set[index][:conditions][i.to_s] = condition
    end
    set
  end

  def render_rules_for_mobile(rules)
    content = "".html_safe
    rules.each do |rule|
      content << tag(:hr) unless rule.id == rules.first.id
      content << render(partial: 'rule', locals: {rule: rule})
    end
    content
  end

  def rule_options(rule)
    options = if rule.is_active?
      ['update', 'deactivate', 'delete']
    else
      ['update', 'activate', 'delete']
    end
    options.map { |op| [I18n.t("txt.admin.views.rules.form.#{op}"), op] }
  end

  def rule_translation(rule_name)
    if rule_name == "trigger"
      I18n.t('txt.admin.views.rules.form.trigger_label')
    elsif rule_name == "automation"
      I18n.t('txt.admin.views.rules.form.automation_label')
    elsif rule_name == 'view'
      I18n.t('txt.admin.views.rules.form.view_label')
    else
      rule_name
    end
  end

  def rule_translation_pluralize(rule_name)
    if rule_name == "trigger"
      I18n.t('txt.admin.views.rules.form.triggers_label')
    elsif rule_name == "automation"
      I18n.t('txt.admin.views.rules.form.automations_label')
    elsif rule_name == 'view'
      I18n.t('txt.admin.views.rules.form.views_label')
    else
      rule_name.pluralize
    end
  end

  # rubocop:disable Style/MultipleComparison
  def rule_type_translation(rule_type)
    if rule_type == "triggers" || rule_type == "Triggers"
      I18n.t('txt.admin.views.rules.analysis.show.triggers_label')
    elsif rule_type == "automations" || rule_type == "Automations"
      I18n.t('txt.admin.views.rules.analysis.show.automations_label')
    elsif rule_type == 'views' || rule_type == 'Views'
      I18n.t('txt.admin.views.rules.analysis.show.views_label')
    elsif rule_type == 'macros' || rule_type == 'Macros'
      I18n.t('txt.admin.views.rules.analysis.show.macros_label')
    else
      rule_type.capitalize
    end
  end

  def rule_description_translation(rule_type)
    if rule_type == "triggers" || rule_type == "Triggers"
      I18n.t('txt.admin.views.rules.analysis.show.triggers_description1')
    elsif rule_type == "automations" || rule_type == "Automations"
      I18n.t('txt.admin.views.rules.analysis.show.automations_description1')
    elsif rule_type == 'views' || rule_type == 'Views'
      I18n.t('txt.admin.views.rules.analysis.show.views_description1')
    elsif rule_type == 'macros' || rule_type == 'Macros'
      I18n.t('txt.admin.views.rules.analysis.show.macros_description')
    else
      ''
    end
  end

  def rule_learn_more_link_translation(rule_type)
    if rule_type == "triggers" || rule_type == "Triggers"
      I18n.t('txt.admin.views.rules.analysis.show.triggers_description_link')
    elsif rule_type == "automations" || rule_type == "Automations"
      I18n.t('txt.admin.views.rules.analysis.show.automations_description_link')
    elsif rule_type == 'views' || rule_type == 'Views'
      I18n.t('txt.admin.views.rules.analysis.show.views_description_link')
    elsif rule_type == 'macros' || rule_type == 'Macros'
      I18n.t('txt.admin.views.rules.analysis.show.macros_description_link')
    else
      ''
    end
  end
  # rubocop:enable Style/MultipleComparison

  def include_available_for_me_radio_option?
    return false unless current_user.can?(:manage_personal, @rule.class)
    !@rule.owner.is_a?(User) || current_user == @rule.owner
  end

  def rule_deactivate_translation
    @rule_deactivate_translation ||= I18n.t('txt.admin.views.rules.mobile.deactivate')
  end

  def rule_activate_translation
    @rule_activate_translation ||= I18n.t('txt.admin.views.rules.mobile.activate')
  end

  def rule_confirm_deactivate_translation
    @rule_confirm_deactivate_translation ||= I18n.t('txt.admin.views.rules.confirm_deactivate')
  end

  def rule_clone_translation
    @rule_clone_translation ||= I18n.t('txt.admin.views.rules.rule_actions.clone')
  end

  def rule_edit_translation
    @rule_edit_translation ||= I18n.t('txt.admin.views.rules.rule_actions.edit')
  end

  def sanitize_definitions(definition_class, context, component_type, condition_type = nil)
    context.options.merge!(condition_type: condition_type, component_type: component_type)
    all_defs = definition_class.definitions_as_json(context)
    all_defs.map do |d|
      next unless d[:values]
      d[:values][:list].map do |item|
        item[:title] = CGI.escapeHTML(item[:title]).clean_separators(' ')
      end
    end

    string_replacements = [
      ['/', '\/'], # old, new
      ["\u2028", '\u2028'],
      ["\u2029", '\u2029']
    ]

    string_replacements.reduce(all_defs.to_json) { |s, (old, new)| s.gsub(old, new) }.html_safe
  end

  def render_rule_select_row(items, type, id, set)
    (items.presence || [nil]).map do |item|
      render "rule_#{type}_row", item: item, id: id, set: set
    end.join_preserving_safety("\n")
  end

  def selectables(included:)
    list = list_from_targets(:selection)
    sorted_list = []
    if included
      # preserve output field order
      @output.column_identifiers.each do |i|
        sorted_list += list.select { |_, identifier| i == identifier.to_s }
      end
    else
      sorted_list = list.select { |_, identifier| !@output.column_identifiers.member?(identifier.to_s) }
    end
    sorted_list.map { |t, i| [t, i.to_s] }
  end

  def groupables
    list_from_targets(:grouping)
  end

  def sortables
    list_from_targets(:sorting)
  end

  def list_from_targets(type)
    context = Zendesk::Rules::Context.new(current_account, current_user,
      rule_type: :view, component_type: type)

    ZendeskRules::Definitions::Conditions.definitions_as_json(context).map do |t|
      [Zendesk::Liquid::DcContext.render(t[:title_for_field], current_account, current_user), t[:output_key]]
    end
  end

  def deep_escape(array)
    array.map do |element|
      next deep_escape(element) if element.is_a?(Array)
      element.to_s.gsub(/[&<>"']/) { |c| escape_lookups[c] }
    end
  end

  def escape_lookups
    @escape_lookups ||= {'&' => '&amp;', '<' => '&lt;', '>' => '&gt;', '"' => '&quot;', '\'' => "&#039;" }
  end
end
