module JsModelsHelper
  def asset_host
    compute_asset_host('/')
  end

  def render_js_models
    return unless current_account.present?

    if current_user.nil? || current_user.is_anonymous_user?
      return render_js_models_in_page
    end

    javascript_tag "jQuery.ajax({
       async: false,
       url: '/generated/javascripts/user.json',
       success: User.load
     });"
  end

  def render_js_models_in_page
    javascript_tag render('/generated/javascripts/user')
  end

  def render_js_models_for_mobile
    if current_account.present?
      if current_user.nil? || current_user.is_anonymous_user?
        javascript_tag render(file: '/generated/javascripts/mobile_user')
      else
        '<script type="text/javascript" src="/generated/javascripts/mobile_user.js"></script>'.html_safe
      end
    end
  end
end
