module AuthHelper
  def javascript_include_auth(options = {})
    defaults = {
      brand_id: current_brand.try(:id),
      auth_origin: [current_brand.try(:id), current_request_mapped?, request.ssl?].join(','),
      auth_domain: current_account.authentication_domain,
      return_to: return_to_url,
      theme: 'hc',
      locale: I18n.translation_locale.id,
      action: options[:sso_action],
      token: params[:token],
    }

    data = defaults.delete_if { |_k, v| v.nil? }
    content_tag :script, '', src: host_file_path, data: data
  end

  def return_to_url
    return root_url unless params[:return_to].present? && params[:return_to].is_a?(String)

    uri = Addressable::URI.parse(params[:return_to])
    path = uri.path.starts_with?('/') ? uri.path : "/" + uri.path

    url = if current_route.host_mapping?
      current_route.url + path
    else
      current_route.url(ssl: true) + path
    end

    if uri.query.present?
      url << "?#{uri.query}"
    end

    url
  rescue Addressable::URI::InvalidURIError
    root_url
  end

  private

  def host_file_path
    if current_account.has_passport?
      javascript_path 'zendesk/auth/v2/passport_host'
    else
      javascript_path 'zendesk/auth/v2/host'
    end
  end

  def current_request_mapped?
    current_route.host_mapping.present? && current_route.host_mapping == request.host
  end
end
