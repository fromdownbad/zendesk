module CaptchaHelper
  def zendesk_captcha(options = {})
    if current_account.has_orca_classic_recaptcha_enterprise?
      Recaptcha.with_configuration(
        api_server_url: 'https://www.recaptcha.net/recaptcha/enterprise.js'
      ) do
        options[:site_key] = ENV.fetch('ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY')
        recaptcha_tags(_recaptcha_default_options.deep_merge(options))
      end
    else
      recaptcha_tags(_recaptcha_default_options.deep_merge(options))
    end
  end

  private

  def _recaptcha_default_options
    {
      theme: 'light',
      hl: I18n.translation_locale.locale,
      site_key: Zendesk::Configuration.fetch(:recaptcha_v2).fetch('site_key')
    }
  end
end
