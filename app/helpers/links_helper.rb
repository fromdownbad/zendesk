module LinksHelper
  def render_personal_views
    return unless current_account.subscription.has_personal_rules?
    joined_links = Rails.cache.fetch("#{current_account.scoped_cache_key(:rules)}/#{current_user.cache_key_with_time}/private-views-and-more", expires_in: 1.hour) do
      limit = current_account.max_private_views_for_display
      links = current_user.personal_views.select(&:is_active?)[0, limit].collect do |view|
        link_view(view)
      end

      if current_user.personal_views.select(&:is_active?).length > current_account.max_private_views_for_display
        links << content_tag(:li, link_to(I18n.t('txt.admin.helpers.links_helper.more_tab'), main_app.rules_path(filter: 'views', select: 'personal'), class: 'all_views'))
      end
      links.join
    end
    # Writing with rails 2 and reading with rails 3 causes issues with safe_html.
    joined_links.html_safe
  end

  def render_shared_views
    links = Rails.cache.fetch("#{current_account.scoped_cache_key(:rules)}/#{current_user.cache_key_with_time}/public-views-and-suspended", expires_in: 1.hour) do
      limit = current_account.max_views_for_display + 1
      current_user.shared_views.select(&:is_active?)[0, limit].collect do |view|
        link_view(view)
      end
    end

    add_more_link = links.size > current_account.max_views_for_display
    links = links[0, current_account.max_views_for_display]

    cache_options = "#{current_account.scoped_cache_key(:views)}/#{current_user.cache_key_with_time}/suspended-tickets"

    links += []

    links << Rails.cache.fetch(*cache_options) do
      link_suspended_tickets.to_s
    end

    if current_account.settings.archive?
      links << content_tag(:li, link_to(I18n.t('txt.admin.views.layout.tabs._views_tab.archived_tickets_tab'), '/archived_tickets', class: 'all_views'))
    end

    if add_more_link
      links << content_tag(:li, link_to(I18n.t('txt.admin.helpers.links_helper.more_tab'), main_app.rules_path(filter: 'views'), class: 'all_views'))
    end

    links.join.html_safe
  end

  private

  def link_view(view)
    content_tag(:li) do
      content_tag(:a, href: "/rules/#{view.id}") do
        view_title = dc_renderer.render(view.title)
        h(view_title) + ' ' + content_tag(:span, class: 'r_count', 'data-rule-id': view.id, 'data-fresh': false) do
          "(&hellip;)".html_safe
        end
      end
    end
  end

  def link_suspended_tickets
    if current_user.is_non_restricted_agent? && (count = current_account.suspended_tickets.count(:all)) > 0
      @link_suspended_tickets ||=
        content_tag(:li) do
          content_tag(:a, href: main_app.suspended_tickets_path, class: 'warning') do
            h(I18n.t('txt.admin.helpers.links_helper.suspended_tickets_tab')) + content_tag(:span, class: 'r_count') do
              " (#{count})"
            end
          end
        end.html_safe
    end
  end

  def dc_renderer
    @dc_renderer ||= Zendesk::DynamicContent::Renderer.new(current_account)
  end
end
