module WorkspacesHelper
  def convert_to_zd_menu_format(entries, no_select_entry = nil, entry_map = { title: "title", value: "id" })
    key = entry_map[:title] || "title"
    value = entry_map[:value] || "id"

    if entries
      entries_list = entries.reduce([]) { |initial_list, entry| initial_list.append(label: entry[key], value: entry[value]) }
      entries_list.unshift(no_select_entry) if no_select_entry
      entries_list.to_json.html_safe
    else
      []
    end
  end

  def ticket_form_id(ticket_form)
    ticket_form ? ticket_form.id : -1
  end
end
