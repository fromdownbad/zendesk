module PeopleHelper
  def people_creation_links
    links = []

    remote_login_allowed = current_account.login_allowed_for_any_role?(:remote)

    create_users_options = {
      data: { confirm: remote_login_allowed ? I18n.t('txt.admin.helpers.people.people_helper.create_users_manually_remote_authentication_enable') : nil }
    }

    links << link_to(I18n.t('txt.admin.helpers.people.people_helper.user_label'), new_user_path, create_users_options)                   if current_user.is_admin? || current_account.agents_can_create_users
    links << link_to(I18n.t('txt.admin.helpers.people.people_helper.group_label'), new_group_path)                                       if current_user.can?(:edit, Group) && current_account.has_groups?
    links << link_to(I18n.t('txt.admin.helpers.people.people_helper.organization_label'), new_organization_path)                         if current_user.can?(:edit, Organization) && current_account.has_organizations?
    links << link_to(I18n.t('txt.admin.helpers.people.people_helper.role_label'), new_people_role_path)                                  if current_user.is_admin? && current_account.has_permission_sets?

    if links.any?
      links = links.zip([" | "] * (links.length - 1)).flatten.compact.map(&:html_safe)
      I18n.t('txt.helpers.people_helper.add_links', links_to_add_people: links.join_preserving_safety)
    end
  end

  def people_browser
    links = []
    links << people_filter(I18n.t('txt.admin.helpers.people.people_helper.end_users_label'), users_path(role: 0), '0')               if current_user.can?(:list, User)
    links << people_filter(I18n.t('txt.admin.helpers.people.people_helper.agents_label'), users_path(role: [4, 2]), '["4", "2"]')    if current_user.is_admin?
    links << people_filter(I18n.t('txt.admin.helpers.people.people_helper.admins_label'), users_path(role: 2), '2')                  if current_user.is_admin? && !current_account.subscription.has_permission_sets?
    links << people_filter(I18n.t('txt.admin.helpers.people.people_helper.groups_label'), groups_path, 'group')                      if current_user.can?(:edit, Group) && current_account.has_groups?
    links << people_filter(I18n.t('txt.admin.helpers.people.people_helper.organizations_label'), organizations_path, 'organization') if current_user.can?(:edit, Organization) && current_account.has_organizations?
    links << people_filter(I18n.t('txt.admin.helpers.people.people_helper.roles_label'), people_roles_path, 'role')                  if current_user.is_admin? && current_account.has_permission_sets?
    links << people_filter(I18n.t('txt.admin.helpers.people.people_helper.tags_label'), people_tags_path, 'tag')                     if current_user.is_admin? && current_account.has_user_and_organization_tags?

    if links.any?
      content_tag(:div, id: "people-browse") do
        joined_links = links.join_preserving_safety(content_tag(:span, "|", class: "delim"))
        I18n.t('txt.admin.helpers.people.people_helper.or_browse_label_with_links', links: joined_links)
      end
    end
  end

  def people_filter(name, url, role, _with_delim = true)
    if controller_name == name || params[:role].to_s == role
      content_tag(:span, name, style: "color:#888;")
    else
      link_to(name, url)
    end
  end

  def people_filter_title
    if controller_name == 'search' && !@query.blank?
      I18n.t('txt.admin.helpers.people.people_helper.people_label')
    elsif params[:legacyagents]
      I18n.t('txt.admin.helpers.people.people_helper.legacy_agents_label')
    elsif controller_name != 'users' || (params[:role].nil? && params[:permission_set].nil?)
      get_localization_string_controller
    elsif params[:permission_set] && permission_set = current_account.permission_sets.find_by_id(params[:permission_set])
      permission_set.translated_name
    elsif Role.find_by_id(params[:role])
      get_translated_users_title(params[:role])
    else
      I18n.t('txt.admin.helpers.people.people_helper.users_label')
    end
  end

  def get_translated_users_title(role)
    role = Array(role).flatten
    if role == [Role::END_USER.id.to_s]
      I18n.t('txt.helpers.people_helper.end_users_label')
    elsif role == [Role::AGENT.id.to_s, Role::ADMIN.id.to_s]
      I18n.t('txt.helpers.people_helper.agents_label')
    elsif role == [Role::ADMIN.id.to_s]
      I18n.t('txt.admin.helpers.user_helper.administrators_label_plural')
    elsif role.first.is_a?(Role)
      [*role].sort_by(&:id).last.name.pluralize
    else
      I18n.t('txt.admin.helpers.people.people_helper.people_label')
    end
  end

  def get_localization_string_controller # rubocop:disable Naming/AccessorMethodName
    current_controller_formated = controller_name.to_s.capitalize.pluralize
    if current_controller_formated == "Groups"
      I18n.t('txt.helpers.people_helper.groups_label')
    elsif current_controller_formated == "Organizations"
      I18n.t('txt.helpers.people_helper.organizations_label')
    elsif current_controller_formated == "Roles"
      I18n.t('txt.helpers.people_helper.roles_label')
    elsif current_controller_formated == "Users"
      I18n.t('txt.organization.section.users')
    else
      current_controller_formated
    end
  end

  def permission_set_name_or_link(user)
    if user.is_admin?
      link_to_if(params[:role] != Role::ADMIN.id.to_s, I18n.t('txt.admin.helpers.people.people_helper.administrator_label'), users_path(role: Role::ADMIN.id)) +
      (user.is_account_owner? ? I18n.t('txt.admin.helpers.people.people_helper.owner_label') : "")
    elsif user.has_permission_set?
      translated_role_name = user.permission_set.translated_name
      link_to_if(params[:permission_set].blank?, translated_role_name, users_path(permission_set: user.permission_set.id))
    elsif user.is_legacy_agent?
      link_to(I18n.t('txt.admin.helpers.people.people_helper.legacy_agents_label'), users_path(legacyagents: 1)) + I18n.t('txt.admin.helpers.people.people_helper.unassigned_label')
    end
  end

  def count_tags_for_users(tag_name)
    conditions = "taggable_type = 'User' and tag_name = '#{tag_name}' and users.is_active = true"
    current_account.taggings.joins('JOIN users on users.id = taggings.taggable_id').where(conditions).count(:all)
    # TODO: USER_TAGS whoa expensive. Cache or something.
    # result = User.connection.execute("select count(*) from (select users.id from taggings, users where taggings.tag_name = '#{tag_name}' and (users.organization_id = taggings.taggable_id and taggings.taggable_type = 'Organization') and users.account_id = #{current_account.id} and taggings.account_id = #{current_account.id} union distinct select users.id from users, taggings where taggings.tag_name = '#{tag_name}' and users.id = taggings.taggable_id and taggings.taggable_type = 'User' and users.account_id = #{current_account.id} and taggings.account_id = #{current_account.id} ) as a;")
    # result.fetch_row.first.to_i
  end

  def count_tags_for_organizations(tag_name)
    conditions = "taggable_type = 'Organization' and tag_name = '#{tag_name}'"
    current_account.taggings.where(conditions).count(:all)
  end

  def search_bar_state
    %w[roles tags].include?(controller_name) ? "disabled" : nil
  end

  def default_group?(group_id)
    current_account.default_group && (group_id == current_account.default_group.id)
  end
end
