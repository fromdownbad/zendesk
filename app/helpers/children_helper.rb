module ChildrenHelper
  def sub_account(account)
    t =  link_to account.name, action: :show, id: account.id
    t << " - #{SubscriptionPlanType.to_s(account.subscription.plan_type)}"
    content_tag('p', t)
  end
end
