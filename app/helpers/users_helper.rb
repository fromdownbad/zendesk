module UsersHelper
  def edit_options(user)
    edits = []

    unless is_assuming_user?
      if user == current_user
        # Theoretically accounts shouldn't have mixed 'normal' users and Remote Authentication users. But sometimes they do. So we have this somewhat convoluted expression
        # A quick caveat: both people who use OpenID and Password can be RememberMeStrategy but obviously OpenID can't change passwords. But there's nothing in this code to prevent that
        # even if  there's no Remote Authentication enabled so I assume it's ok. That may bite me in the ass sometime later.
        if current_user.is_agent? || (current_account.is_end_user_password_change_visible? && (auth_via?(:password) || !current_user.only_login_service_allowed?(:remote) || !current_user.crypted_password.nil?))
          edits << content_tag(:li, link_to(I18n.t('txt.layout.change_pwd'), '/password'))
        end
      elsif current_user.can?(:assume, user)
        edits << content_tag(:li, link_to(I18n.t('txt.admin.helpers.user_helper.assume_identity_label'), assume_user_path(user), data: {method: :post}))
      end
    end

    if current_user.can?(:request_password_reset, user)
      edits << content_tag(:li, link_to(I18n.t('txt.users.reset_pwd'), "/password/admin_reset_request/#{user.id}", data: { confirm: I18n.t('txt.users.reset_pwd_confirmation'), method: :post }))
    end

    if current_user.can?(:create_password, user)
      edits << content_tag(:li, link_to(I18n.t('txt.users.create_pwd'), "/password/admin_change_request/#{user.id}", data: { confirm: I18n.t('txt.users.create_pwd_confirmation'), method: :post }))
    end

    if can_start_merge?(user) && !user.foreign?
      edits << content_tag(:li, link_to(I18n.t('txt.admin.helpers.user_helper.merge_user_label'), new_user_merge_path(user), id: 'merge_link'))
    end

    if current_user.can?(:edit, user) && (current_user.id != user.id)
      edits << content_tag(:li, link_to(I18n.t('txt.admin.helpers.user_helper.suspend_label'), 'javascript:void(0);', :id => 'suspend_access', :class => 'alert', 'data-user-suspended' => user.suspended?))
    end

    if user.id && current_user.can?(:delete, user) && (current_user.id != user.id)
      message = if user.is_agent?
        I18n.t('txt.admin.helpers.user_helper.confirm_message_if_agent')
      else
        I18n.t('txt.admin.helpers.user_helper.delete_user_confirm_message')
      end
      edits << content_tag(:li, link_to(I18n.t('txt.admin.helpers.user_helper.delete_user_label'), user_path(user), class: 'alert', data: { confirm: message, method: :delete }))
    end

    edits.join_preserving_safety
  end

  def link_to_user_profile(user, _truncate = false)
    name = user.safe_name(current_user.is_agent?)
    if current_user.is_end_user? || user.account_id == SYSTEM_ACCOUNT_ID || user.foreign?
      return name
    else
      if user.is_active?
        return link_to(h(name), user_path(user), title: I18n.t('txt.admin.helpers.user_helper.click_to_view_users_profile'))
      else
        return I18n.t('txt.admin.helpers.user_helper.user_deleted_label', user_name: h(name))
      end
    end
  end

  def should_ask_for_email?
    !is_assuming_user? && current_user && current_user.should_ask_for_email?
  end

  def can_start_merge?(user)
    !current_user.is_end_user? && current_user.can?(:edit, user) && Users::Merge.valid_loser?(user)
  end

  # UserHelper::permission_sets_list
  #
  # Generates a collection of permission_sets with the Admin role included.
  # Optionally can provide an option for Legacy Agents
  # Passed as JSON to build the Edit User Profile's role list
  def permission_sets_list(account, include_legacy_agent = false)
    permission_sets = account.permission_sets.map do |ps|
      users_in_set = ps.users.count(:all)
      headcount = if users_in_set.zero?
        I18n.t('txt.admin.helpers.user_helper.no_agents_label')
      else
        link_to(I18n.t('txt.admin.helpers.user_helper.agent_pluralization', count: users_in_set), users_path(permission_set: ps.id))
      end

      {
        id: ps.id,
        name: h(ps.translated_name),
        headcount: headcount,
        description: truncate(h(role_description(ps)), length: 500)
      }
    end

    admin_count = account.admins.count(:all)

    permission_sets << {
      id: 'admin',
      name: I18n.t('txt.admin.helpers.user_helper.administrator_label'),
      headcount: (admin_count == 0 ? I18n.t('txt.admin.helpers.user_helper.no_agents_label') : link_to(I18n.t('txt.admin.helpers.user_helper.agent_pluralization', count: admin_count), users_path(role: Role::ADMIN.id))),
      description: I18n.t('txt.admin.helpers.user_helper.admin_description')
    }

    permission_sets.sort! { |a, b| a[:name] <=> b[:name] }

    if include_legacy_agent
      legacy_agents_count = account.agents.where(permission_set_id: nil).count(:all)
      permission_sets.insert(
        0,
        id: "",
        name: I18n.t("txt.admin.helpers.user_helper.legacy_agent_name"),
        headcount: (legacy_agents_count == 0 ? I18n.t('txt.admin.helpers.user_helper.no_agents_label') : I18n.t('txt.admin.helpers.user_helper.agent_pluralization', count: legacy_agents_count)),
        description: I18n.t("txt.admin.helpers.user_helper.legacy_agent_description")
      )
    end

    permission_sets
  end

  def role_description(ps)
    return ps.description if ps.description.present?
    ps_name = ps.name.downcase.tr(' ', '_')
    return I18n.t("txt.default.roles.#{ps_name}.description") if ['light_agent', 'staff', 'team_leader', 'advisor'].include?(ps_name)
    I18n.t('txt.admin.helpers.user_helper.no_description_label')
  end

  def legacy_agent_selected?(user_ids)
    users = current_account.users.where(id: user_ids).to_a
    users.any?(&:is_legacy_agent?)
  end

  def can_edit_organization?(current_user, current_account)
    current_user.is_non_restricted_agent? &&
    current_account.organizations.exists? &&
    !org_restricted(current_user, current_account)
  end

  def org_restricted(current_user, current_account)
    if current_account.has_permission_sets?
      current_user.permission_set &&
      current_user.permission_set.permissions.end_user_profile == 'edit-within-org'
    else
      false
    end
  end

  ROSETTA_TO_FACEBOOK = {
    "af" => "af_ZA",
    "af-za" => "af_ZA",
    "ajp-ps" => "ajp_PS",
    "apc-ps" => "apc_PS",
    "ar" => "ar_AR",
    "ar-ae" => "ar_AE",
    "ar-eg" => "ar_EG",
    "ar-ps" => "ar_PS",
    "as-in" => "as_IN",
    "ay-bo" => "ay_BO",
    "az" => "az_AZ",
    "be" => "be_BY",
    "bg" => "bg_BG",
    "bg-bg" => "bg_BG",
    "bn" => "bn_IN",
    "bn-in" => "bn_IN",
    "bs" => "bs_BA",
    "ca" => "ca_ES",
    "ca-es" => "ca_ES",
    "cs" => "cs_CZ",
    "cs-cz" => "cs_CZ",
    "cy" => "cy_GB",
    "da" => "da_DK",
    "da-dk" => "da_DK",
    "de" => "de_DE",
    "de-at" => "de_DE",
    "de-be" => "de_BE",
    "de-ch" => "de_DE",
    "de-de" => "de_DE",
    "de-dk" => "de_DK",
    "de-it" => "de_IT",
    "de-lu" => "de_LU",
    "de-ro" => "de_RO",
    "el" => "el_GR",
    "el-cy" => "el_CY",
    "el-gr" => "el_GR",
    "en" => "en_US",
    "en-001" => "en_US",
    "en-150" => "en_US",
    "en-ae" => "en_AE",
    "en-at" => "en_AT",
    "en-au" => "en_US",
    "en-be" => "en_US",
    "en-bg" => "en_BG",
    "en-bo" => "en_BO",
    "en-bz" => "en_US",
    "en-ca" => "en_US",
    "en-ch" => "en_CH",
    "en-co" => "en_CO",
    "en-cr" => "en_CR",
    "en-cy" => "en_CY",
    "en-cz" => "en_CZ",
    "en-dk" => "en_DK",
    "en-ec" => "en_EC",
    "en-ee" => "en_EE",
    "en-es" => "en_ES",
    "en-gb" => "en_GB",
    "en-gr" => "en_GR",
    "en-gu" => "en_GU",
    "en-hk" => "en_HK",
    "en-hn" => "en_HN",
    "en-hu" => "en_HU",
    "en-id" => "en_ID",
    "en-ie" => "en_US",
    "en-il" => "en_US",
    "en-in" => "en_US",
    "en-it" => "en_IT",
    "en-jm" => "en_US",
    "en-kr" => "en_KR",
    "en-lr" => "en_LR",
    "en-lt" => "en_LT",
    "en-lu" => "en_LU",
    "en-lv" => "en_LV",
    "en-mt" => "en_MT",
    "en-mx" => "en_MX",
    "en-my" => "en_US",
    "en-nl" => "en_NL",
    "en-no" => "en_NO",
    "en-nz" => "en_US",
    "en-pe" => "en_PE",
    "en-ph" => "en_US",
    "en-pl" => "en_PL",
    "en-pr" => "en_PR",
    "en-ps" => "en_PS",
    "en-pt" => "en_PT",
    "en-ro" => "en_RO",
    "en-se" => "en_SE",
    "en-sg" => "en_US",
    "en-sk" => "en_SK",
    "en-th" => "en_TH",
    "en-tt" => "en_US",
    "en-ua" => "en_UA",
    "en-us" => "en_US",
    "en-vn" => "en_VN",
    "en-za" => "en_US",
    "en-zw" => "en_US",
    "es" => "es_LA",
    "es-419" => "es_LA",
    "es-ar" => "es_LA",
    "es-bo" => "es_LA",
    "es-cl" => "es_LA",
    "es-co" => "es_LA",
    "es-cr" => "es_LA",
    "es-do" => "es_LA",
    "es-ec" => "es_LA",
    "es-es" => "es_ES",
    "es-gt" => "es_LA",
    "es-hn" => "es_HN",
    "es-mx" => "es_LA",
    "es-ni" => "es_LA",
    "es-pa" => "es_LA",
    "es-pe" => "es_LA",
    "es-pr" => "es_LA",
    "es-py" => "es_LA",
    "es-sv" => "es_LA",
    "es-us" => "es_LA",
    "es-uy" => "es_LA",
    "es-ve" => "es_LA",
    "et" => "et_EE",
    "et-ee" => "et_EE",
    "eu" => "eu_ES",
    "eu-es" => "eu_ES",
    "fa" => "fa_IR",
    "fi" => "fi_FI",
    "fil" => "fil_PH",
    "fil-ph" => "fil_PH",
    "fo" => "fo_FO",
    "fo-dk" => "fo_DK",
    "fr" => "fr_FR",
    "fr-002" => "fr_002",
    "fr-be" => "fr_FR",
    "fr-ca" => "fr_CA",
    "fr-ch" => "fr_FR",
    "fr-ci" => "fr_CI",
    "fr-fr" => "fr_FR",
    "fr-it" => "fr_IT",
    "fr-lu" => "fr_LU",
    "fr-ma" => "fr_MA",
    "ga" => "ga_IE",
    "ga-ie" => "ga_IE",
    "gl" => "gl_ES",
    "gl-es" => "gl_ES",
    "gu" => "gu_IN",
    "gu-in" => "gu_IN",
    "he" => "he_IL",
    "hi" => "hi_IN",
    "hi-in" => "hi_IN",
    "hr" => "hr_HR",
    "hu" => "hu_HU",
    "hu-hu" => "hu_HU",
    "hu-ro" => "hu_RO",
    "hu-sk" => "hu_SK",
    "hu-ua" => "hu_UA",
    "hy" => "hy_AM",
    "id" => "id_ID",
    "id-id" => "id_ID",
    "ikt" => "iu",
    "is" => "is_IS",
    "it" => "it_IT",
    "it-ch" => "it_IT",
    "it-it" => "it_IT",
    "ja" => "ja_JP",
    "jv-id" => "jv_ID",
    "ka" => "ka_GE",
    "kk" => "kk_KZ",
    "kl-dk" => "kl_DK",
    "km" => "km_KH",
    "kn" => "kn_IN",
    "kn-in" => "kn_IN",
    "ko" => "ko_KR",
    "ko-kr" => "ko_KR",
    "ks-in" => "ks_IN",
    "ku" => "ku_TR",
    "lt" => "lt_LT",
    "lt-lt" => "lt_LT",
    "lt-lv" => "lt_LV",
    "lv" => "lv_LV",
    "lv-lv" => "lv_LV",
    "mi-nz" => "mi_NZ",
    "mk" => "mk_MK",
    "ml" => "ml_IN",
    "ml-in" => "ml_IN",
    "mn" => "mn_MN",
    "mr" => "mr_IN",
    "mr-in" => "mr_IN",
    "ms" => "ms_MY",
    "ms-my" => "ms_MY",
    "my" => "my_MM",
    "nb" => "nb_NO",
    "nb-no" => "nb_NO",
    "nl" => "nl_NL",
    "nl-be" => "nl_NL",
    "nl-id" => "nl_ID",
    "nl-nl" => "nl_NL",
    "nn" => "nn_NO",
    "nn-no" => "nn_NO",
    "no" => "nb_NO",
    "nso-za" => "nso_ZA",
    "or-in" => "or_IN",
    "pa" => "pa_IN",
    "pa-in" => "pa_IN",
    "pl" => "pl_PL",
    "pl-cz" => "pl_CZ",
    "pl-lt" => "pl_LT",
    "pl-pl" => "pl_PL",
    "pl-ua" => "pl_UA",
    "ps" => "ps_AF",
    "pt" => "pt_PT",
    "pt-br" => "pt_BR",
    "pt-pt" => "pt_PT",
    "qu-bo" => "qu_BO",
    "qu-ec" => "qu_EC",
    "qu-pe" => "qu_PE",
    "ro" => "ro_RO",
    "ro-bg" => "ro_BG",
    "ro-ro" => "ro_RO",
    "ro-sk" => "ro_SK",
    "ro-ua" => "ro_UA",
    "ru" => "ru_RU",
    "ru-ee" => "ru_EE",
    "ru-kz" => "ru_RU",
    "ru-lt" => "ru_LT",
    "ru-lv" => "ru_LV",
    "ru-ua" => "ru_UA",
    "sa-in" => "sa_IN",
    "sd-in" => "sd_IN",
    "si" => "si_LK",
    "sk" => "sk_SK",
    "sk-cz" => "sk_CZ",
    "sk-sk" => "sk_SK",
    "sl" => "sl_SI",
    "sq" => "sq_AL",
    "sr" => "sr_RS",
    "sr-me" => "sr_ME",
    "st-za" => "st_ZA",
    "sv" => "sv_SE",
    "sv-se" => "sv_SE",
    "sw" => "sw_KE",
    "ta" => "ta_IN",
    "ta-in" => "ta_IN",
    "te" => "te_IN",
    "te-in" => "te_IN",
    "th" => "th_TH",
    "tk" => "tk_TM",
    "tl" => "tl_PH",
    "tn-za" => "tn_ZA",
    "tr" => "tr_TR",
    "tr-bg" => "tr_BG",
    "ts-za" => "ts_ZA",
    "uk" => "uk_UA",
    "uk-sk" => "uk_SK",
    "uk-ua" => "uk_UA",
    "ur-in" => "ur_IN",
    "vi" => "vi_VN",
    "vi-vn" => "vi_VN",
    "xh-za" => "xh_ZA",
    "zh-cn" => "zh_CN",
    "zh-hk" => "zh_HK",
    "zh-mo" => "zh_TW",
    "zh-sg" => "zh_CN",
    "zh-tw" => "zh_TW",
    "zu-za" => "zu_ZA"
  }.freeze

  def user_locale_for_facebook(user)
    return "en_US" unless user && user.translation_locale
    # Facebook requires locales with underscore, e,g. https://developers.facebook.com/docs/messenger-platform/messenger-profile/supported-locales/
    language_code = user.translation_locale.language_code
    ROSETTA_TO_FACEBOOK[language_code] || 'en_US'
  end

  def get_translations_with_localized_agent # rubocop:disable Naming/AccessorMethodName
    locales = TranslationLocale.available_for_agents(current_account)
    locales += TranslationLocale.test_locales if current_account.settings.allow_test_locales?
    locales += TranslationLocale.keys_locales if current_account.settings.allow_keys_locales?
    locales.uniq
  end

  def get_default_language_agent # rubocop:disable Naming/AccessorMethodName
    current_account.translation_locale.localized_agent? ? current_account.translation_locale : ENGLISH_BY_ZENDESK
  end
end
