require 'zendesk/protobuf/support/views/ticket_pb'

# TODO: Refactor this when publisher is changed to use composition.
# It should not the be the ViewsTicketProtobufEncoder responsibility to add headers
module ViewsEncoders
  module HeaderProtobufEncoder
    ZC = ::Zendesk::Protobuf::Common
    G = ::Google::Protobuf

    def headers(schema_path)
      ZC::ProtobufHeader.new(
        schema_path: schema_path,
        message_id:  G::StringValue.new(value: SecureRandom.uuid),
        account_id:  G::Int32Value.new(value: entity.account_id),
        occurred_at: G::Timestamp.new(seconds: entity.updated_at.to_i)
      )
    end
  end
end
