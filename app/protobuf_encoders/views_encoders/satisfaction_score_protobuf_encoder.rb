require 'zendesk_protobuf_clients/zendesk/protobuf/support/views/ticket_pb'

module ViewsEncoders
  class SatisfactionScoreProtobufEncoder < ZendeskProtobufEncoder
    alias ticket entity

    def to_object
      @to_object ||= begin
        ZV::Satisfaction.new(
          score_id: ticket.satisfaction_score && G::Int64Value.new(value: ticket.satisfaction_score),
          probability: current_satisfaction_probability && G::FloatValue.new(value: current_satisfaction_probability), # This requires a join
          # ::SatisfactionScoreProtobufEncoder pulls the comment, we don't need it so map it here instead
          score: (::SatisfactionScoreProtobufEncoder::SATISFACTION_MAP[ticket.satisfaction_score] || ZT::Score::UNKNOWN_SCORE),
          reason_code: ticket.satisfaction_reason_code && G::Int64Value.new(value: ticket.satisfaction_reason_code)
        )
      end
    end

    private

    def current_satisfaction_probability
      TicketPrediction.current_satisfaction_probability(ticket)
    end
  end
end
