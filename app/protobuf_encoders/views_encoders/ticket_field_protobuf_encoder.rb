require 'zendesk_protobuf_clients/zendesk/protobuf/support/views/ticket_field_pb'

module ViewsEncoders
  class TicketFieldProtobufEncoder < ZendeskProtobufEncoder
    include HeaderProtobufEncoder
    alias ticket_field_entry entity

    SCHEMA_PATH = 'support/views/ticket_field'.freeze

    def to_object
      @to_object ||= begin
        ZV::ViewsTicketField.new(
          ticket_id:          G::Int64Value.new(value: ticket_field_entry.ticket_id),
          ticket_field:       ticket_field_object,
          ticket_field_entry: ZT::TicketFieldEntry.new(encode_value)
        ).tap { |message| message.header = headers(SCHEMA_PATH) }
      end
    end

    private

    def ticket_field_object
      ::TicketFieldProtobufEncoder.new(
        ticket_field_entry.ticket_field
      ).thin.to_object
    end

    def encode_value
      TicketFieldEntryValueProtobufEncoder.new.encode_value(
        ticket_field_entry.ticket_field,
        ticket_field_entry.value
      )
    end
  end
end
