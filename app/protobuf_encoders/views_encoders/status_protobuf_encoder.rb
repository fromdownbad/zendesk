require 'zendesk_protobuf_clients/zendesk/protobuf/support/views/ticket_pb'

module ViewsEncoders
  class StatusProtobufEncoder < TicketStatusProtobufEncoder
    alias ticket entity

    def to_object
      @to_object ||= begin
        ZV::Status.new(
          id: ticket.status_id && G::Int64Value.new(value: ticket.status_id),
          type: super,
          position: ticket.status_id && G::Int64Value.new(value: status_position),
          updated_at: ticket.status_updated_at && G::Timestamp.new(seconds: ticket.status_updated_at.to_i, nanos: ticket.status_updated_at.nsec)
        )
      end
    end

    private

    def status_position
      Zendesk::Types::StatusType.order.index(ticket.status_id)
    end
  end
end
