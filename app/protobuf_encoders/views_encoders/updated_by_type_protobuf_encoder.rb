require 'zendesk_protobuf_clients/zendesk/protobuf/support/views/ticket_pb'

module ViewsEncoders
  class UpdatedByTypeProtobufEncoder < ZendeskProtobufEncoder
    alias ticket entity

    UPDATED_BY_TYPE_MAP = {
      UpdatedByType.AGENT => ZV::UpdatedByTypeValue::AGENT,
      UpdatedByType.END_USER => ZV::UpdatedByTypeValue::END_USER
    }.freeze

    def to_object
      @to_object ||= begin
        ZV::UpdatedByType.new(
          id: G::Int64Value.new(value: ticket.updated_by_type_id),
          type: encode_type
        )
      end
    end

    def encode_type
      UPDATED_BY_TYPE_MAP[ticket.updated_by_type_id] || ZV::UpdatedByTypeValue::UNKNOWN_UPDATED_BY_TYPE
    end
  end
end
