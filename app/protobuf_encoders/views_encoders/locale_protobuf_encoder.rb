require 'zendesk_protobuf_clients/zendesk/protobuf/support/views/ticket_pb'

module ViewsEncoders
  class LocaleProtobufEncoder < ZendeskProtobufEncoder
    alias ticket entity

    def to_object
      @to_object ||= begin
        ZV::Locale.new(
          id: G::Int64Value.new(value: ticket.requester&.translation_locale&.id),
          code: G::StringValue.new(value: ticket.requester&.translation_locale&.locale)
        )
      end
    end
  end
end
