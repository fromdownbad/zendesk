require 'zendesk_protobuf_clients/zendesk/protobuf/support/accounts/account_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/accounts/brand_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/groups/group_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_form_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/organization_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/user_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/views/ticket_pb'

module ViewsEncoders
  class ViewsTicketProtobufEncoder
    include HeaderProtobufEncoder

    G   = ::Google::Protobuf
    ZA  = ::Zendesk::Protobuf::Support::Accounts
    ZG  = ::Zendesk::Protobuf::Support::Groups
    ZT  = ::Zendesk::Protobuf::Support::Tickets::V2
    ZU2 = ::Zendesk::Protobuf::Support::Users::V2
    ZV  = ::Zendesk::Protobuf::Support::Views

    SCHEMA_PATH = 'support/views/ticket'.freeze

    attr_reader :entity

    alias ticket entity

    def initialize(entity)
      @entity = entity
    end

    def to_object
      ZV::Ticket.new(
        header: headers(SCHEMA_PATH),
        account: account,
        assignee: user(ticket.assignee),
        assigned_at: assigned_at,
        assignee_updated_at: assignee_updated_at,
        brand: brand,
        description: encode_string(ticket.description),
        due_at: due_at,
        group: group,
        id: encode_int(ticket.id),
        initially_assigned_at: initially_assigned_at,
        is_public: G::BoolValue.new(value: ticket.is_public),
        locale: locale,
        nice_id: encode_int(ticket.nice_id),
        organization: organization,
        priority: priority,
        requester: requester,
        recipient: ticket.recipient && encode_string(ticket.recipient),
        requester_updated_at: requester_updated_at,
        satisfaction: satisfaction,
        schedule: schedule,
        sharing_agreements: sharing_agreements,
        status: status,
        sla_breach_status: ticket.sla_breach_status && encode_int(ticket.sla_breach_status),
        subject: ticket.subject && encode_string(ticket.subject),
        submitter: user(ticket.submitter),
        solved_at: solved_at,
        tags: tags,
        ticket_form: ticket_form,
        ticket_type: ticket_type,
        timestamps: timestamps,
        updated_by_type: updated_by_type,
        via: via,
        via_reference_id: ticket.via_reference_id && encode_int(ticket.via_reference_id)
      )
    end

    def to_proto
      to_object.to_proto
    end

    private

    def account
      Account.with_deleted { ZA::Account.new(id: encode_int(ticket.account_id)) }
    end

    def assigned_at
      ticket.assigned_at &&
        G::Timestamp.new(seconds: ticket.assigned_at.to_i, nanos: ticket.assigned_at.nsec)
    end

    def assignee_updated_at
      ticket.assignee_updated_at &&
        G::Timestamp.new(seconds: ticket.assignee_updated_at.to_i, nanos: ticket.assignee_updated_at.nsec)
    end

    def brand
      ticket.brand &&
        ZA::Brand.new(id: encode_int(ticket.brand_id), name: encode_string(ticket.brand.name))
    end

    def due_at
      ticket.due_date &&
        G::Timestamp.new(seconds: ticket.due_date.to_i, nanos: ticket.due_date.nsec)
    end

    def group
      ticket.group &&
        ZG::Group.new(id: encode_int(ticket.group_id), name: encode_string(ticket.group.name))
    end

    def initially_assigned_at
      ticket.initially_assigned_at &&
        G::Timestamp.new(seconds: ticket.initially_assigned_at.to_i, nanos: ticket.initially_assigned_at.nsec)
    end

    def locale
      ticket.locale_id &&
        ViewsEncoders::LocaleProtobufEncoder.new(ticket).to_object
    end

    def organization
      ticket.organization &&
        ZU2::Organization.new(id: encode_int(ticket.organization_id), name: encode_string(ticket.organization.name))
    end

    def priority
      return unless ticket.priority_id

      ZV::Priority.new(
        id: encode_int(ticket.priority_id),
        type: TicketPriorityProtobufEncoder.new(ticket).to_object
      )
    end

    def requester
      ticket.requester && UserV2ProtobufEncoder.new(ticket.requester, [:id, :name, role: [:role_id]]).to_object
    end

    def requester_updated_at
      ticket.requester_updated_at &&
        G::Timestamp.new(seconds: ticket.requester_updated_at.to_i, nanos: ticket.requester_updated_at.nsec)
    end

    def satisfaction
      ViewsEncoders::SatisfactionScoreProtobufEncoder.new(ticket).to_object
    end

    def sharing_agreements
      ids = ticket.shared_tickets.map { |shared_ticket| encode_int(shared_ticket.agreement_id) }
      return if ids.empty?

      ZV::SharingAgreements.new(ids: ids)
    end

    def solved_at
      ticket.solved_at &&
        G::Timestamp.new(seconds: ticket.solved_at.to_i, nanos: ticket.solved_at.nsec)
    end

    def schedule
      return unless ticket.ticket_schedule.present?

      ZV::Schedule.new(
        id: encode_int(ticket.ticket_schedule.id)
      )
    end

    def status
      ViewsEncoders::StatusProtobufEncoder.new(ticket).to_object
    end

    def tags
      ticket.current_tags &&
        ZC::TagList.new(tags: ticket.current_tags.split(' '))
    end

    def ticket_form
      ticket.ticket_form &&
        ZT::TicketForm.new(id: encode_int(ticket.ticket_form.id))
    end

    def ticket_type
      return unless ticket.ticket_type_id

      ZV::TicketType.new(
        id: encode_int(ticket.ticket_type_id),
        type: TicketTypeProtobufEncoder.new(ticket).to_object
      )
    end

    def timestamps
      TimestampsProtobufEncoder.new(ticket).to_object
    end

    def updated_by_type
      ticket.updated_by_type_id &&
        ViewsEncoders::UpdatedByTypeProtobufEncoder.new(ticket).to_object
    end

    def user(user)
      user &&
        ZU2::User.new(id: encode_int(user.id), name: encode_string(user.name))
    end

    def via
      return unless ticket.via_id

      ZV::Via.new(
        id: encode_int(ticket.via_id),
        type: ViaTypeProtobufEncoder.new(ticket).to_object
      )
    end

    def encode_int(value)
      G::Int64Value.new(value: value)
    end

    def encode_string(value)
      G::StringValue.new(value: value)
    end
  end
end
