require 'zendesk_protobuf_clients/zendesk/protobuf/platform/standard/users/user_authentication_events_pb'

class UserAuthenticationEventProtobufEncoder < ZendeskProtobufEncoder
  SCHEMA_PATH = 'platform/standard/users'.freeze

  class NotImplemented < RuntimeError; end

  alias user entity

  def to_object
    ZPU::UserAuthenticationEvent.new({
      user_id: G::Int64Value.new(value: user.id),
      header: ZC::ProtobufHeader.new(
        schema_path: SCHEMA_PATH,
        message_id:  G::StringValue.new(value: message_id),
        account_id:  G::Int32Value.new(value: user.account_id),
        occurred_at: G::Timestamp.new(seconds: Time.now.to_i)
      )
    }.merge(encoder_params))
  end

  def encoder_params
    raise NotImplemented
  end
end
