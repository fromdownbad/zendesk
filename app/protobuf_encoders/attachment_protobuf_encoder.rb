require 'zendesk_protobuf_clients/zendesk/protobuf/support/attachments/attachment_pb'

class AttachmentProtobufEncoder < ZendeskProtobufEncoder
  attr_reader :attachment

  def initialize(attachment)
    @attachment = attachment
  end

  def to_object
    @to_object ||= begin
      ZAT::Attachment.new(apply_field_map)
    end
  end

  def full
    @field_map = [
      :id,
      :content_url,
      :content_type,
      :filename,
      :size,
      :is_inline,
      :is_public
    ]
    self
  end

  def id_only
    @field_map = [:id]
    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :id
      G::Int64Value.new(value: attachment.id)
    when :content_url
      content_url = ContentUrlBuilder.new.url_for(attachment, mapped: false, ssl: true)
      G::StringValue.new(value: content_url)
    when :content_type
      G::StringValue.new(value: attachment.content_type)
    when :filename
      G::StringValue.new(value: attachment.display_filename)
    when :size
      G::Int64Value.new(value: attachment.size)
    when :is_inline
      G::BoolValue.new(value: attachment.inline)
    when :is_public
      G::BoolValue.new(value: attachment.is_public)
    end
  end
end
