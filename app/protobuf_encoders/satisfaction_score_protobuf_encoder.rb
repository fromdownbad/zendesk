require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/satisfaction_type_pb'

class SatisfactionScoreProtobufEncoder < ZendeskProtobufEncoder
  alias ticket entity

  SATISFACTION_MAP = {
    SatisfactionType.UNOFFERED       => ZT::Score::UNOFFERED,
    SatisfactionType.OFFERED         => ZT::Score::OFFERED,
    SatisfactionType.GOOD            => ZT::Score::GOOD,
    SatisfactionType.GOODWITHCOMMENT => ZT::Score::GOOD,
    SatisfactionType.BAD             => ZT::Score::BAD,
    SatisfactionType.BADWITHCOMMENT  => ZT::Score::BAD,
  }.freeze

  def to_object
    @to_object ||= begin
      ZT::SatisfactionScore.new(apply_field_map)
    end
  end

  def full
    @field_map = [
      :score,
      :comment
    ]

    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :score
      map_satisfaction_score
    when :comment
      # NOTE:  Satisfaction comments are on the Events table, so this requires a query to populate
      ticket.satisfaction_comment && G::StringValue.new(value: ticket.satisfaction_comment)
    else
      raise InvalidFieldMap
    end
  end

  def map_satisfaction_score
    SATISFACTION_MAP[ticket.satisfaction_score] || ZT::Score::UNKNOWN_SCORE
  end
end
