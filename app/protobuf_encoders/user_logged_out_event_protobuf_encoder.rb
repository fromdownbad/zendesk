require 'zendesk_protobuf_clients/zendesk/protobuf/platform/standard/users/user_authentication_events_pb'

class UserLoggedOutEventProtobufEncoder < UserAuthenticationEventProtobufEncoder
  def encoder_params
    { user_logged_out: ZPU::UserLoggedOut.new }
  end
end
