require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_pb'

class TicketPriorityProtobufEncoder < ZendeskProtobufEncoder
  alias ticket entity

  PRIORITY_MAP = {
    PriorityType.-      => ZT::TicketPriority::UNKNOWN_TICKET_PRIORITY,
    PriorityType.LOW    => ZT::TicketPriority::LOW,
    PriorityType.NORMAL => ZT::TicketPriority::NORMAL,
    PriorityType.HIGH   => ZT::TicketPriority::HIGH,
    PriorityType.URGENT => ZT::TicketPriority::URGENT,
  }.freeze

  def to_object
    @to_object ||= PRIORITY_MAP[ticket.priority_id] || ZT::TicketPriority::UNKNOWN_TICKET_PRIORITY
  end
end
