require 'zendesk/protobuf/support/views/ticket_pb'

# Extracted from CustomFieldChangedProbufEncoder for reusability
class TicketFieldEntryValueProtobufEncoder
  ZC = ::Zendesk::Protobuf::Common
  G = ::Google::Protobuf

  def encode_value(parent_ticket_field, value)
    case parent_ticket_field
    when FieldDate
      begin
        date = Date.parse(value)
        {
          date_value: ZC::Date.new(
            year: date.year,
            month: date.month,
            day: date.day
          )
        }
      rescue ArgumentError, RangeError
        # Prior validation only checks DATE_REGEXP, we may still receive
        # impossible dates like February 31st.
        # We can also get an impossibly large year due to trigger values we allow like
        # '2737907006988507635340185-11-15' which blow our int32 year in the schemas
        { date_value: nil }
      end
    when FieldCheckbox
      { checkbox_value:    value == '1' }
    when FieldDecimal
      { decimal_string:    value }
    when FieldInteger
      { integer_string:    value }
    when FieldRegexp
      { regexp_value:      value }
    when FieldMultiselect
      { multiselect_value: ZC::TagList.new(tags: value.split(' ')) }
    when FieldTagger
      { tagger_value:      value }
    when FieldText
      { text_value:        value }
    when FieldTextarea
      { text_area_value:   value }
    when FieldPartialCreditCard
      { credit_card_value: value }
    else
      raise ArgumentError,
        "Unexpected ticket field type: #{parent_ticket_field.class}"
    end
  end
end
