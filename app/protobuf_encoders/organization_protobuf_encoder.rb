require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/organization_pb'

class OrganizationProtobufEncoder < ZendeskProtobufEncoder
  alias organization entity

  def to_object
    @to_object ||= ZU2::Organization.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :account,
      :name,
      :is_shared,
      :timestamps,
      :group,
      :details,
      :notes,
      :is_shared_comments,
      :external_id
    ]

    self
  end

  def thin
    @field_map = [
      :id,
      :name,
      :is_shared,
      :timestamps,
      :is_shared_comments,
      :external_id,
      account: [
        :id
      ],
      group: [
        :id
      ],
    ]

    self
  end

  private

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: organization.id)
    when :account
      AccountProtobufEncoder.new(organization.account, sub_map).to_object
    when :name
      G::StringValue.new(value: organization.name)
    when :is_shared
      G::BoolValue.new(value: organization.is_shared)
    when :timestamps
      TimestampsProtobufEncoder.new(organization).to_object
    when :group
      organization.group && GroupProtobufEncoder.new(organization.group, sub_map).to_object
    when :details
      organization.details && G::StringValue.new(value: organization.details)
    when :notes
      organization.notes && G::StringValue.new(value: organization.notes)
    when :is_shared_comments
      G::BoolValue.new(value: organization.is_shared_comments)
    when :external_id
      organization.external_id && G::StringValue.new(value: organization.external_id)
    else
      raise InvalidFieldMap
    end
  end
end
