class OrganizationEventsProtobufEncoder < ZendeskProtobufEncoder
  alias organization entity

  delegate :any?, to: :event_list

  SCHEMA_PATH = 'support/users/v2/organization_events'.freeze

  # The order of this list determines order of publishing
  INFERRED_EVENTS = [
    OrganizationCreatedProtobufEncoder,
    OrganizationDeletedProtobufEncoder,
    OrganizationExternalIdChangedProtobufEncoder,
    OrganizationTagsChangedProtobufEncoder,
    OrganizationNameChangedProtobufEncoder
  ].freeze

  def to_a
    @to_a = event_list.each_with_index do |event, i|
      # Add "organization"
      event.organization = OrganizationProtobufEncoder.new(organization).thin.to_object
      event.actor = actor_context

      # Add "header"
      # Due to mutating `sequence`, this object is mutated and shouldn't be memoized
      event.header = ZC::ProtobufHeader.new(
        schema_path: SCHEMA_PATH,
        message_id:  G::StringValue.new(value: message_id),
        account_id:  G::Int32Value.new(value: organization.account_id),
        occurred_at: G::Timestamp.new(seconds: organization.updated_at.to_i),
        sequence:    ZC::MessageSequence.new(
          sequence_id: sequence_id,
          position:    i + 1,
          total:       event_list.length
        )
      )
    end
  end

  # Inferred and Explicit domain events
  def event_list
    inferred_events + organization.domain_events
  end

  def clear
    organization.domain_events.clear
    @inferred_events.clear
  end

  def inferred_events
    @inferred_events ||= INFERRED_EVENTS.
      map { |klass| klass.new(organization) }.
      select(&:matches?).
      map(&:to_safe_object).
      compact
  end

  def actor_context
    actor_id = CIA.current_actor ? CIA.current_actor.id : -1
    ip = CIA.current_transaction&.fetch(:ip_address, "") # Fetch IP-address if it's set
    ::Zendesk::Protobuf::AccountAudits::Actor.new(
      id: G::Int64Value.new(value: actor_id),
      account_id: G::Int64Value.new(value: organization.account_id),
      ip_address: G::StringValue.new(value: ip)
    )
  end
end
