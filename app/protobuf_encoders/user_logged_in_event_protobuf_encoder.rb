require 'zendesk_protobuf_clients/zendesk/protobuf/platform/standard/users/user_authentication_events_pb'

class UserLoggedInEventProtobufEncoder < UserAuthenticationEventProtobufEncoder
  def encoder_params
    { user_logged_in: ZPU::UserLoggedIn.new }
  end
end
