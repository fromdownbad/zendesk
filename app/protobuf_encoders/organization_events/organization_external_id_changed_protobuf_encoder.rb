class OrganizationExternalIdChangedProtobufEncoder < BaseOrganizationEventProtobufEncoder
  def matches?
    organization.external_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::OrganizationEvent.new(
        external_id_changed: ZU2::OrganizationExternalIdChanged.new(
          previous: organization.external_id_was  && G::StringValue.new(value: organization.external_id_was),
          current:  organization.external_id      && G::StringValue.new(value: organization.external_id)
        )
      )
    end
  end
end
