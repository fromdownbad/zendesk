class OrganizationTagsChangedProtobufEncoder < BaseOrganizationEventProtobufEncoder
  def matches?
    organization.tags_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::OrganizationEvent.new(
        tags_changed: ZU2::OrganizationTagsChanged.new(
          tags_added: ZC::TagList.new(tags: organization.tags_changes[:added]),
          tags_removed: ZC::TagList.new(tags: organization.tags_changes[:removed])
        )
      )
    end
  end
end
