class OrganizationNameChangedProtobufEncoder < BaseOrganizationEventProtobufEncoder
  def matches?
    !organization.id_changed? && organization.name_changed? &&
      !(organization.deleted_at_changed? && organization.deleted_at) # Ignore org deletion that appends _deleted to name
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::OrganizationEvent.new(
        organization_name_changed: ZU2::OrganizationNameChanged.new(
          previous: organization.name_was && G::StringValue.new(value: organization.name_was),
          current:  organization.name     && G::StringValue.new(value: organization.name)
        )
      )
    end
  end
end
