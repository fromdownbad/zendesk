require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/organization_events_pb'

class BaseOrganizationEventProtobufEncoder < ZendeskProtobufEncoder
  class NotImplemented < RuntimeError; end
  class DoesNotMatch   < RuntimeError; end

  alias organization entity

  def matches?
    raise NotImplemented
  end

  def to_object
    raise NotImplemented
  end

  def to_safe_object
    to_object
  rescue StandardError => e
    # For now, we prefer silently dropping domain events over crashing save.
    Rails.logger.error(e)
    encoder_statsd_client.increment('errors', tags: ["exception:#{e.class}"])
    ZendeskExceptions::Logger.record(e, location: self, message: "#{self.class}: #{e.message}", fingerprint: '5ee85d32001502efaae6ea16a62298b240aab51b')
    nil
  end

  def encoder_statsd_client
    @encoder_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['support', 'event_bus', 'organization', 'encoder'], tags: ["encoder:#{self.class}"])
  end
end
