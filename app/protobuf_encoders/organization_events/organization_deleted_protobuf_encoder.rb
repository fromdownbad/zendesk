class OrganizationDeletedProtobufEncoder < BaseOrganizationEventProtobufEncoder
  def matches?
    organization.deleted_at_changed? && organization.deleted_at
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::OrganizationEvent.new(
        organization_deleted: ZU2::OrganizationDeleted.new
      )
    end
  end
end
