class OrganizationCreatedProtobufEncoder < BaseOrganizationEventProtobufEncoder
  def matches?
    organization.id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::OrganizationEvent.new(
        organization_created: ZU2::OrganizationCreated.new
      )
    end
  end
end
