class OrganizationCustomFieldChangedProtobufEncoder < BaseOrganizationEventProtobufEncoder
  attr_reader :field, :previous_value, :current_value

  def initialize(field, previous_value, current_value)
    @field = field
    @previous_value = previous_value
    @current_value = current_value
  end

  def to_object
    @to_object ||= begin
      ZU2::OrganizationEvent.new(
        custom_field_changed: ZU2::OrganizationCustomFieldChanged.new(
          field: CustomFieldProtobufEncoder.new(field).thin.to_object,
          previous: previous_value.presence && ZU2::CustomFieldEntry.new(encode_value(previous_value)),
          current:  current_value.presence && ZU2::CustomFieldEntry.new(encode_value(current_value))
        )
      )
    end
  end

  def encode_value(value)
    case field
    when CustomField::Date
      begin
        date = Date.parse(value)
        { date_value: ZC::Date.new(year: date.year, month: date.month, day: date.day) }
      rescue ArgumentError
        # Prior validation only checks DATE_REGEXP, we may still receive
        # impossible dates like February 31st.
        { date_value: nil }
      end
    when CustomField::Checkbox
      { checkbox_value:    value == '1' }
    when CustomField::Decimal
      { decimal_string:    value ? remove_leading_zeroes(value) : value }
    when CustomField::Integer
      { integer_string:    value ? value.to_i.to_s : value }
    when CustomField::Regexp
      { regexp_value:      value }
    when CustomField::Dropdown
      { tagger_value:      value && CustomField::DropdownChoice.find_by(id: value, account_id: field.account_id)&.value }
    when CustomField::Text
      { text_value:        value }
    when CustomField::Textarea
      { text_area_value:   value }
    else
      raise ArgumentError, "Unexpected field type: #{field.class}"
    end
  end

  def remove_leading_zeroes(value_to_parse)
    (left, right) = value_to_parse.split('.')
    left.to_i.to_s + '.' + right.to_s
  end
end
