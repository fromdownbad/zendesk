require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/custom_field_pb'

class CustomFieldProtobufEncoder < ZendeskProtobufEncoder
  alias custom_field entity

  TYPE_MAP = {
    "Text" => ZU2::CustomFieldType::TEXT,
    "Textarea" => ZU2::CustomFieldType::TEXTAREA,
    "Checkbox" => ZU2::CustomFieldType::CHECKBOX,
    "Date" => ZU2::CustomFieldType::DATE,
    "Integer" => ZU2::CustomFieldType::INTEGER,
    "Decimal" => ZU2::CustomFieldType::DECIMAL,
    "Regexp" => ZU2::CustomFieldType::REGEXP,
    "Dropdown" => ZU2::CustomFieldType::TAGGER,
  }.freeze

  def to_object
    @to_object ||= ZU2::CustomField.new(apply_field_map)
  end

  def thin
    @field_map = [
      :id,
      :title,
      :type
    ]
    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :id
      G::Int64Value.new(value: custom_field.id)
    when :title
      custom_field.title && G::StringValue.new(value: custom_field.title)
    when :type
      custom_field.type && encode_type
    else
      raise InvalidFieldMap
    end
  end

  def encode_type
    TYPE_MAP[custom_field.type] || ZU2::CustomFieldType::UNKNOWN_CUSTOM_FIELD_TYPE
  end
end
