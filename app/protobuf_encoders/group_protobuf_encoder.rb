require 'zendesk_protobuf_clients/zendesk/protobuf/support/groups/group_pb'

class GroupProtobufEncoder < ZendeskProtobufEncoder
  alias group entity

  def to_object
    @to_object ||= ZG::Group.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :account,
      :is_active,
      :name,
      :timestamps,
      :default,
      :chat_enabled
    ]

    self
  end

  private

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: group.id)
    when :account
      AccountProtobufEncoder.new(group.account, sub_map).to_object
    when :name
      G::StringValue.new(value: group.name)
    when :timestamps
      TimestampsProtobufEncoder.new(group).to_object
    when :is_active, :default, :chat_enabled
      G::BoolValue.new(value: group.public_send(field))
    else
      raise InvalidFieldMap
    end
  end
end
