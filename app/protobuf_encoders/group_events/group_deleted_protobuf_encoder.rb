class GroupDeletedProtobufEncoder < BaseGroupEventProtobufEncoder
  def matches?
    group.is_active_changed? && group.deleted?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::GroupEvent.new(
        group_deleted: ZU2::GroupDeleted.new
      )
    end
  end
end
