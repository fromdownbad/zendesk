class GroupCreatedProtobufEncoder < BaseGroupEventProtobufEncoder
  def matches?
    group.id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::GroupEvent.new(
        group_created: ZU2::GroupCreated.new
      )
    end
  end
end
