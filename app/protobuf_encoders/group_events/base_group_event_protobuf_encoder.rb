require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/group_events_pb'

class BaseGroupEventProtobufEncoder < ZendeskProtobufEncoder
  class NotImplemented < RuntimeError; end
  class DoesNotMatch   < RuntimeError; end

  alias group entity

  def matches?
    raise NotImplemented
  end

  def to_object
    raise NotImplemented
  end

  def to_safe_object
    to_object
  rescue StandardError => e
    # For now, we prefer silently dropping domain events over crashing save.
    encoder_statsd_client.increment('errors', tags: ["exception:#{e.class}"])
    ZendeskExceptions::Logger.record(e, location: self, message: "#{self.class}: #{e.message}", fingerprint: '217cc5c06a61a24e77e8bb2c1a950d42e36a7d7e')
    nil
  end

  def encoder_statsd_client
    @encoder_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['support', 'domain_event', 'group', 'encoder'], tags: ["encoder:#{self.class}"])
  end
end
