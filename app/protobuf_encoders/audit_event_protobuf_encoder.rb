require 'zendesk_protobuf_clients/zendesk/protobuf/account_audits/audit_event_pb'

class AuditEventProtobufEncoder < ZendeskProtobufEncoder
  def to_object
    @to_object ||= ::Zendesk::Protobuf::AccountAudits::AuditEvent.new(apply_field_map)
  end

  def full
    @field_map = [
      :account_id,
      :actor_id,
      :action,
      :ip_address,
      :source_id,
      :source_type,
      :raw_message,
      :visible,
      :source_display_name,
      :changes
    ]
    self
  end

  private

  def encode_field(field, _)
    case field
    when :account_id
      entity.account_id
    when :actor_id
      entity.actor_id
    when :action
      case entity.action
      when 'create'
        ::Zendesk::Protobuf::AccountAudits::AuditEvent::Action::CREATE
      when 'destroy'
        ::Zendesk::Protobuf::AccountAudits::AuditEvent::Action::DESTROY
      when 'login'
        ::Zendesk::Protobuf::AccountAudits::AuditEvent::Action::LOGIN
      when 'update'
        ::Zendesk::Protobuf::AccountAudits::AuditEvent::Action::UPDATE
      else
        raise 'invalid action type in an audit'
      end
    when :ip_address
      entity.ip_address
    when :source_id
      entity.source_id.to_s
    when :source_type
      entity.source_type
    when :source_display_name
      entity.source_display_name
    when :raw_message
      ::Zendesk::Protobuf::AccountAudits::AuditEvent::RawMessage.new(text: entity.message)
    when :visible
      entity.visible
    when :changes
      entity.attribute_changes.map do |change|
        ::Zendesk::Protobuf::AccountAudits::AuditEvent::AttributeChange.new(
            attribute_name: change.attribute_name,
            old_value: change.old_value,
            new_value: change.new_value
          )
      end
    else
      Rails.logger.info("Invalid field: #{field}")
      raise InvalidFieldMap
    end
  end
end
