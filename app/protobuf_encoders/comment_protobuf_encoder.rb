require 'zendesk/protobuf/support/tickets/v2/ticket_pb'
class CommentProtobufEncoder < ZendeskProtobufEncoder
  alias comment entity

  def to_object
    @to_object ||= ZT::Comment.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :timestamps,
      :body,
      :is_public,
      :author
    ]

    self
  end

  def id_only
    @field_map = [:id]

    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :id
      G::Int64Value.new(value: comment.id)
    when :timestamps
      TimestampsProtobufEncoder.new(comment).to_object
    when :body
      comment.body && G::StringValue.new(value: comment.body)
    when :is_public
      G::BoolValue.new(value: comment.is_public?)
    when :author
      comment.author && UserProtobufEncoder.new(comment.author).thin.to_object
    end
  end
end
