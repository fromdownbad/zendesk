class GroupEventsProtobufEncoder < ZendeskProtobufEncoder
  alias group entity

  delegate :any?, to: :event_list

  SCHEMA_PATH = 'support/users/v2/group_events'.freeze

  # The order of this list determines order of publishing
  INFERRED_EVENTS = [
    GroupCreatedProtobufEncoder,
    GroupDeletedProtobufEncoder
  ].freeze

  def to_a
    @to_a ||= event_list.each_with_index do |event, i|
      event.group = group_object
      event.actor = actor_context

      # Add "header"
      # Due to mutating `sequence`, this object is mutated and shouldn't be memoized
      event.header = ZC::ProtobufHeader.new(
        schema_path: SCHEMA_PATH,
        message_id:  G::StringValue.new(value: message_id),
        account_id:  G::Int32Value.new(value: group.account_id),
        occurred_at: G::Timestamp.new(seconds: group.updated_at.to_i),
        sequence:    ZC::MessageSequence.new(
          sequence_id: sequence_id,
          position:    i + 1,
          total:       event_list.length
        )
      )
    end
  end

  # Inferred and Explicit domain events
  def event_list
    inferred_events
  end

  def clear
    @inferred_events.clear
  end

  def inferred_events
    @inferred_events ||= INFERRED_EVENTS.
      map { |klass| klass.new(group) }.
      select(&:matches?).
      map(&:to_safe_object).
      compact
  end

  def actor_context
    actor_id = CIA.current_actor ? CIA.current_actor.id : -1
    ip = CIA.current_transaction&.fetch(:ip_address, "") # Fetch IP-address if it's set
    ::Zendesk::Protobuf::AccountAudits::Actor.new(
      id: G::Int64Value.new(value: actor_id),
      account_id: G::Int64Value.new(value: group.account_id),
      ip_address: G::StringValue.new(value: ip)
    )
  end

  def group_object
    @group_object ||= ZU2::Group.new(
      id: G::Int64Value.new(value: group.id),
      account_id: G::Int64Value.new(value: group.account_id),
      name: G::StringValue.new(value: group.name)
    )
  end
end
