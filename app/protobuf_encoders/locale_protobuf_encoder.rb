require 'zendesk_protobuf_clients/zendesk/protobuf/support/locales/locale_pb'

class LocaleProtobufEncoder < ZendeskProtobufEncoder
  alias locale entity

  def to_object
    @to_object ||= begin
      ZL::Locale.new(
        id: G::Int64Value.new(value: locale.id)
      )
    end
  end
end
