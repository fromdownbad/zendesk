require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/user_pb'

class UserProtobufEncoder < ZendeskProtobufEncoder
  alias user entity

  IDENTITY_MAP = {
    "UserAnyChannelIdentity"      => ZU::IdentityType::ANY_CHANNEL,
    "UserEmailIdentity"           => ZU::IdentityType::EMAIL,
    "UserFacebookIdentity"        => ZU::IdentityType::FACEBOOK,
    "UserForeignIdentity"         => ZU::IdentityType::FOREIGN,
    "UserPhoneNumberIdentity"     => ZU::IdentityType::PHONE_NUMBER,
    "UserTwitterIdentity"         => ZU::IdentityType::TWITTER,
    "UserVoiceForwardingIdentity" => ZU::IdentityType::AGENT_FORWARDING,
    "UserSDKIdentity"             => ZU::IdentityType::SDK
  }.freeze

  def to_object
    @to_object ||= ZU::User.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :account,
      :name,
      :is_staff,
      :is_active,
      :is_suspended,
      :time_zone,
      :locale,
      :locale_id,
      :email,
      :agent_display_name,
      :photo,
      :details,
      :notes,
      :timestamps,
      :user_field_values,
      :tag_list,
      :orgs,
      :identities
    ]

    self
  end

  def thin
    @field_map = [
      :id,
      :name,
      :is_staff
    ]

    self
  end

  private

  def user_fields(user)
    user.custom_field_values.as_json(custom_fields: user.custom_fields, account: user.account).transform_values(&:to_s)
  end

  def encode_identities!
    ZU::UserIdentityList.new(
      identities: user.identities.map do |identity|
        ZU::UserIdentity.new(
          id:    G::Int64Value.new(value: identity.id),
          type:  encode_identity_type(identity.type),
          deliverable: G::BoolValue.new(value: deliverable_identity?(identity)),
          value: G::StringValue.new(value: identity.value)
        )
      end
    )
  end

  def deliverable_identity?(identity)
    identity.deliverable_state == DeliverableStateType.DELIVERABLE
  end

  def encode_identity_type(type)
    IDENTITY_MAP[type] || ZU::IdentityType::UNKNOWN_IDENTITY_TYPE
  end

  def encode_photo
    return if user.photo.nil?

    thumbnail = user.photo.thumbnails&.detect { |t| t.thumbnail == "thumb" }
    ZU::Photo.new(
      thumbnail_url: G::StringValue.new(value: photo_uri_path(thumbnail)),
      original_url:  G::StringValue.new(value: photo_uri_path(user.photo))
    )
  end

  def encode_account
    ZU::Account.new(
      id:         G::Int64Value.new(value: user.account_id),
      name:       G::StringValue.new(value: user.account.name),
      subdomain:  G::StringValue.new(value: user.account.subdomain)
    )
  end

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: user.id)
    when :account
      encode_account
    when :name
      G::StringValue.new(value: user.name)
    when :is_staff
      G::BoolValue.new(value: !user.is_end_user?)
    when :is_active
      G::BoolValue.new(value: user.is_active?)
    when :is_suspended
      G::BoolValue.new(value: user.suspended?)
    when :time_zone
      G::StringValue.new(value: user.time_zone)
    when :locale
      G::StringValue.new(value: user.locale.to_s)
    when :locale_id
      G::Int32Value.new(value: user.locale_id) if user.locale_id
    when :email
      G::StringValue.new(value: user.email)
    when :agent_display_name
      G::StringValue.new(value: user.agent_display_name.to_s)
    when :photo
      encode_photo
    when :details
      G::StringValue.new(value: user.details.to_s)
    when :notes
      G::StringValue.new(value: user.notes.to_s)
    when :timestamps
      TimestampsProtobufEncoder.new(user, sub_map).to_object
    when :user_field_values
      G::Struct.from_hash(user_fields(user))
    when :tag_list
      ZC::TagList.new(tags: user.tags)
    when :orgs
      ZU::OrganizationList.new(orgs: user.organizations.map { |org| ZU::Organization.new(id: org.id, name: org.name) })
    when :identities
      encode_identities!
    else
      raise InvalidFieldMap
    end
  end

  def photo_uri_path(photo)
    Addressable::URI.parse(ContentUrlBuilder.new.url_for(photo)).path
  end
end
