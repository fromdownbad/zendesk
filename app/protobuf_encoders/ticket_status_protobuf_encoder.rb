require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_pb'

class TicketStatusProtobufEncoder < ZendeskProtobufEncoder
  alias ticket entity

  STATUS_MAP = {
    StatusType.NEW      => ZT::TicketStatus::NEW,
    StatusType.OPEN     => ZT::TicketStatus::OPEN,
    StatusType.PENDING  => ZT::TicketStatus::PENDING,
    StatusType.HOLD     => ZT::TicketStatus::HOLD,
    StatusType.SOLVED   => ZT::TicketStatus::SOLVED,
    StatusType.CLOSED   => ZT::TicketStatus::CLOSED,
    StatusType.DELETED  => ZT::TicketStatus::DELETED,
    StatusType.ARCHIVED => ZT::TicketStatus::ARCHIVED
  }.freeze

  def to_object
    @to_object ||= STATUS_MAP[ticket.status_id] || ZT::TicketStatus::UNKNOWN_TICKET_STATUS
  end
end
