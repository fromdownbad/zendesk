class UserEventsProtobufEncoder < ZendeskProtobufEncoder
  alias user entity

  delegate :any?, to: :event_list

  SCHEMA_PATH = 'support/users/v2/user_events'.freeze

  # The order of this list determines order of publishing
  INFERRED_EVENTS = [
    UserCreatedProtobufEncoder,
    UserNameChangedProtobufEncoder,
    UserExternalIdChangedProtobufEncoder,
    UserNotesChangedProtobufEncoder,
    UserTimeZoneChangedProtobufEncoder,
    UserOnlyPrivateCommentsChangedProtobufEncoder,
    UserSoftDeletedProtobufEncoder,
    UserTagsChangedProtobufEncoder,
    UserIsActiveChangedProtobufEncoder,
    UserDetailsChangedProtobufEncoder,
    UserPhotoChangedProtobufEncoder,
    UserRoleChangedProtobufEncoder,
    UserCustomRoleChangedProtobufEncoder,
    UserPasswordChangedProtobufEncoder
  ].freeze

  def to_a
    @to_a = event_list.each_with_index do |event, i|
      # Add "user"
      event.user = encoded_user
      event.deprecated_actor = actor_context
      event.actor = new_actor_context

      # Add "header"
      # Due to mutating `sequence`, this object is mutated and shouldn't be memoized
      event.header = ZC::ProtobufHeader.new(
        schema_path: SCHEMA_PATH,
        message_id:  G::StringValue.new(value: message_id),
        account_id:  G::Int32Value.new(value: user.account_id),
        occurred_at: G::Timestamp.new(seconds: user.updated_at.to_i),
        sequence:    ZC::MessageSequence.new(
          sequence_id: sequence_id,
          position:    i + 1,
          total:       event_list.length
        )
      )
    end
  end

  def actor_context
    @actor_context ||=
      if user.current_user
        ZU2::User.new(id: G::Int64Value.new(value: user.current_user.id))
      elsif CIA.current_actor
        ZU2::User.new(id: G::Int64Value.new(value: CIA.current_actor.id))
      end
  end

  def new_actor_context
    @new_actor_context ||= begin
      actor = user.current_user || CIA.current_actor
      if actor
        ip = CIA.current_transaction&.fetch(:ip_address, "") # Fetch IP-address if it's set
        ::Zendesk::Protobuf::AccountAudits::Actor.new(
          id: G::Int64Value.new(value: actor.id),
          account_id: G::Int64Value.new(value: actor.account_id),
          ip_address: G::StringValue.new(value: ip)
        )
      end
    end
  end

  def encoded_user
    @encoded_user ||= UserV2ProtobufEncoder.new(user).thin.to_object if user
  end

  # Inferred and Explicit domain events
  def event_list
    @event_list = inferred_events + user.domain_events
  end

  def clear
    user.domain_events.clear
    @inferred_events.clear
  end

  def inferred_events
    @inferred_events ||= INFERRED_EVENTS.
      map { |klass| klass.new(user) }.
      select(&:matches?).
      map(&:to_safe_object).
      compact
  end
end
