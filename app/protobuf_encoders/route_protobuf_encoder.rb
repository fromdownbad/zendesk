require 'zendesk_protobuf_clients/zendesk/protobuf/support/accounts/route_pb'

class RouteProtobufEncoder < ZendeskProtobufEncoder
  alias route entity

  def to_object
    @to_object ||= ZA::Route.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :account,
      :subdomain,
      :host_mapping,
      :timestamps,
      :ssl_required,
      :gam_domain
    ]

    self
  end

  private

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: route.id)
    when :account
      AccountProtobufEncoder.new(route.account, sub_map).to_object
    when :subdomain
      G::StringValue.new(value: route.subdomain)
    when :host_mapping
      route.host_mapping && G::StringValue.new(value: route.host_mapping)
    when :timestamps
      TimestampsProtobufEncoder.new(route).to_object
    when :ssl_required
      G::BoolValue.new(value: route.ssl_required)
    when :gam_domain
      route.gam_domain && G::StringValue.new(value: route.gam_domain)
    else
      raise InvalidFieldMap
    end
  end
end
