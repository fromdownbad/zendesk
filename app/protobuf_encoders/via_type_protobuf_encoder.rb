require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/via_type_pb'

class ViaTypeProtobufEncoder < ZendeskProtobufEncoder
  VIA_MAP = {
    ViaType.API_PHONE_CALL_INBOUND => {type: :api_phone_call_inbound, klass: ZT::ApiPhoneCallInbound},
    ViaType.API_PHONE_CALL_OUTBOUND => {type: :api_phone_call_outbound, klass: ZT::ApiPhoneCallOutbound},
    ViaType.API_VOICEMAIL => {type: :api_voicemail, klass: ZT::ApiVoicemail},
    ViaType.ADMIN_SETTING => {type: :admin_setting, klass: ZT::AdminSetting},
    ViaType.ANSWER_BOT_API => {type: :answer_bot_api, klass: ZT::AnswerBotApi},
    ViaType.ANSWER_BOT_FOR_AGENTS => {type: :answer_bot_for_agents, klass: ZT::AnswerBotForAgents},
    ViaType.ANSWER_BOT_FOR_SDK => {type: :answer_bot_for_sdk, klass: ZT::AnswerBotForSdk},
    ViaType.ANSWER_BOT_FOR_SLACK => {type: :answer_bot_for_slack, klass: ZT::AnswerBotForSlack},
    ViaType.ANSWER_BOT_FOR_WEB_WIDGET => {type: :answer_bot_for_web_widget, klass: ZT::AnswerBotForWebWidget},
    ViaType.ANY_CHANNEL => {type: :any_channel, klass: ZT::AnyChannel},
    ViaType.AUTOMATIC_SOLUTION_SUGGESTIONS => {type: :automatic_solution_suggestions, klass: ZT::AutomaticSolutionSuggestions},
    ViaType.BATCH => {type: :batch, klass: ZT::Batch},
    ViaType.BLOG => {type: :blog, klass: ZT::Blog},
    ViaType.CHAT => {type: :chat, klass: ZT::Chat},
    ViaType.CHURNED_ACCOUNT => {type: :churned_account, klass: ZT::ChurnedAccount},
    ViaType.CLOSED_TICKET => {type: :closed_ticket, klass: ZT::ClosedTicket},
    ViaType.CONNECT_IPM => {type: :connect_ipm, klass: ZT::ConnectIpm},
    ViaType.CONNECT_MAIL => {type: :connect_mail, klass: ZT::ConnectMail},
    ViaType.CONNECT_SMS => {type: :connect_sms, klass: ZT::ConnectSms},
    ViaType.DROPBOX => {type: :dropbox, klass: ZT::Dropbox},
    ViaType.FACEBOOK_MESSAGE => {type: :facebook_message, klass: ZT::FacebookMessage},
    ViaType.FACEBOOK_POST => {type: :facebook_post, klass: ZT::FacebookPost},
    ViaType.GET_SATISFACTION => {type: :get_satisfaction, klass: ZT::GetSatisfaction},
    ViaType.GITHUB => {type: :github, klass: ZT::Github},
    ViaType.GROUP_CHANGE => {type: :group_change, klass: ZT::GroupChange},
    ViaType.GROUP_DELETION => {type: :group_deletion, klass: ZT::GroupDeletion},
    ViaType.HELPCENTER => {type: :helpcenter, klass: ZT::Helpcenter},
    ViaType.IMPORT => {type: :import, klass: ZT::Import},
    ViaType.LINKED_PROBLEM => {type: :linked_problem, klass: ZT::LinkedProblem},
    ViaType.LOGMEIN_RESCUE => {type: :logmein_rescue, klass: ZT::LogmeinRescue},
    ViaType.LOTUS => {type: :lotus, klass: ZT::Lotus},
    ViaType.MACRO_REFERENCE => {type: :macro_reference, klass: ZT::MacroReference},
    ViaType.MAIL => {type: :mail, klass: ZT::Mail},
    ViaType.MERGE => {type: :merge, klass: ZT::Merge},
    ViaType.MOBILE => {type: :mobile, klass: ZT::Mobile},
    ViaType.MOBILE_SDK => {type: :mobile_sdk, klass: ZT::MobileSdk},
    ViaType.MONITOR_EVENT => {type: :monitor_event, klass: ZT::MonitorEvent},
    ViaType.PHONE_CALL_INBOUND => {type: :phone_call_inbound, klass: ZT::PhoneCallInbound},
    ViaType.PHONE_CALL_OUTBOUND => {type: :phone_call_outbound, klass: ZT::PhoneCallOutbound},
    ViaType.RECOVERED_FROM_SUSPENDED_TICKETS => {type: :recovered_from_suspended_tickets, klass: ZT::RecoveredFromSuspendedTickets},
    ViaType.RESOURCE_PUSH => {type: :resource_push, klass: ZT::ResourcePush},
    ViaType.RULE => {type: :rule, klass: ZT::Rule},
    ViaType.RULE_REVISION => {type: :rule_revision, klass: ZT::RuleRevision},
    ViaType.SAMPLE_INTERACTIVE_TICKET => {type: :sample_interactive_ticket, klass: ZT::SampleInteractiveTicket},
    ViaType.SAMPLE_TICKET => {type: :sample_ticket, klass: ZT::SampleTicket},
    ViaType.SATISFACTION_PREDICTION => {type: :satisfaction_prediction, klass: ZT::SatisfactionPrediction},
    ViaType.SMS => {type: :sms, klass: ZT::Sms},
    ViaType.TEXT_MESSAGE => {type: :text_message, klass: ZT::TextMessage},
    ViaType.TICKET_SHARING => {type: :ticket_sharing, klass: ZT::TicketSharing},
    ViaType.TICKET_TAGGING => {type: :ticket_tagging, klass: ZT::TicketTagging},
    ViaType.TOPIC => {type: :topic, klass: ZT::Topic},
    ViaType.TWITTER => {type: :twitter, klass: ZT::Twitter},
    ViaType.TWITTER_DM => {type: :twitter_dm, klass: ZT::TwitterDm},
    ViaType.TWITTER_FAVORITE => {type: :twitter_favorite, klass: ZT::TwitterFavorite},
    ViaType.USER_MERGE => {type: :user_merge, klass: ZT::UserMerge},
    ViaType.USER_CHANGE => {type: :user_change, klass: ZT::UserChange},
    ViaType.USER_DELETION => {type: :user_deletion, klass: ZT::UserDeletion},
    ViaType.VOICEMAIL => {type: :voicemail, klass: ZT::Voicemail},
    ViaType.WEB_WIDGET => {type: :web_widget, klass: ZT::WebWidget},
    ViaType.WEB_FORM => {type: :web_form, klass: ZT::WebForm},
    ViaType.WEB_SERVICE => {type: :web_service, klass: ZT::WebService},
    ViaType.IPHONE => {type: :iphone, klass: ZT::Iphone},
    ViaType.SIDE_CONVERSATION => {type: :side_conversation, klass: ZT::SideConversation},
    ViaType.LINE => {type: :line, klass: ZT::Line},
    ViaType.WECHAT => {type: :wechat, klass: ZT::WeChat},
    ViaType.WHATSAPP => {type: :whatsapp, klass: ZT::WhatsApp},
    ViaType.NATIVE_MESSAGING => {type: :native_messaging, klass: ZT::NativeMessaging},
    ViaType.MAILGUN => {type: :mailgun, klass: ZT::Mailgun},
    ViaType.MESSAGEBIRD_SMS => {type: :messagebird_sms, klass: ZT::MessagebirdSms},
    ViaType.TELEGRAM => {type: :telegram, klass: ZT::Telegram},
    ViaType.TWILIO_SMS => {type: :twilio_sms, klass: ZT::TwilioSms},
    ViaType.VIBER => {type: :viber, klass: ZT::Viber},
    ViaType.GOOGLE_RCS => {type: :google_rcs, klass: ZT::GoogleRcs},
    ViaType.APPLE_BUSINESS_CHAT => {type: :apple_business_chat, klass: ZT::AppleBusinessChat},
    ViaType.GOOGLE_BUSINESS_MESSAGES => {type: :google_business_messages, klass: ZT::GoogleBusinessMessages},
    ViaType.KAKAOTALK => {type: :kakaotalk, klass: ZT::Kakaotalk},
    ViaType.INSTAGRAM_DM => {type: :instagram_dm, klass: ZT::InstagramDm},
    ViaType.SUNSHINE_CONVERSATIONS_API => {type: :sunshine_conversations_api, klass: ZT::SunshineConversationsApi},
    ViaType.SUNSHINE_CONVERSATIONS_TWITTER_DM => {type: :sunshine_conversations_twitter_dm, klass: ZT::SunshineConversationsTwitterDm},
    ViaType.SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER => {type: :sunshine_conversations_facebook_messenger, klass: ZT::SunshineConversationsFacebookMessenger}
  }.freeze

  def to_object
    @to_object ||= begin
      ZT::ViaType.new(mapped_via[:type] => mapped_via[:klass].new)
    end
  end

  private

  def mapped_via
    @mapped_via ||= VIA_MAP[entity.via_id] || {type: :unknown, klass: ZT::UnknownVia}
  end
end
