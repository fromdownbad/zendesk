require 'zendesk_protobuf_clients/zendesk/protobuf/support/accounts/account_pb'

class AccountProtobufEncoder < ZendeskProtobufEncoder
  alias account entity

  def to_object
    @to_object ||= Account.with_deleted { ZA::Account.new(apply_field_map) }
  end

  def full
    @field_map = [
      :id,
      :is_active,
      :name,
      :subdomain,
      :time_zone,
      :timestamps,
      :owner,
      :host_mapping,
      :locale,
      :is_serviceable,
      :help_desk_size,
      :shard_id,
      :lock_state,
      :deleted_at,
      :dnsttl,
      :multiproduct,
      :default_brand_id,
      sandbox_master: [
        :id
      ],
    ]

    self
  end

  private

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: account.id)
    when :is_active
      G::BoolValue.new(value: account.is_active)
    when :name
      G::StringValue.new(value: account.name)
    when :subdomain
      G::StringValue.new(value: account.subdomain)
    when :time_zone
      G::StringValue.new(value: account.time_zone)
    when :timestamps
      TimestampsProtobufEncoder.new(account).to_object
    when :owner
      account.owner && UserProtobufEncoder.new(account.owner, sub_map).to_object
    when :host_mapping
      account.host_mapping && G::StringValue.new(value: account.host_mapping)
    when :locale
      LocaleProtobufEncoder.new(account.translation_locale).to_object
    when :sandbox_master
      account.sandbox_master && AccountProtobufEncoder.new(account.sandbox_master, sub_map).to_object
    when :is_serviceable
      G::BoolValue.new(value: account.is_serviceable)
    when :help_desk_size
      account.help_desk_size && G::StringValue.new(value: account.help_desk_size)
    when :shard_id
      G::Int64Value.new(value: account.shard_id)
    when :lock_state
      G::Int64Value.new(value: account.lock_state)
    when :deleted_at
      account.deleted_at && G::Timestamp.new(seconds: account.deleted_at.to_i, nanos: account.deleted_at.nsec)
    when :dnsttl
      account.dnsttl && G::Int64Value.new(value: account.dnsttl)
    when :multiproduct
      G::BoolValue.new(value: account.multiproduct)
    when :default_brand_id
      account.default_brand_id && G::Int64Value.new(value: account.default_brand_id)
    else
      raise InvalidFieldMap
    end
  end
end
