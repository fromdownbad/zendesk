class UserIdentityRemovedProtobufEncoder < BaseUserEventProtobufEncoder
  include Zendesk::Users::Identities::UserIdentityEncoder

  def initialize(identity)
    super(identity.user)
    @identity = identity
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless @identity

      ZU2::UserEvent.new(
        user_identity_removed: ZU2::UserIdentityRemoved.new(
          identity: ZU2::UserIdentity.new(
            id:     G::Int64Value.new(value: @identity.id),
            type:   encode_identity_type(@identity.type),
            value:  G::StringValue.new(value: @identity.value),
            primary: G::BoolValue.new(value: @identity.primary?)
          )
        )
      )
    end
  end
end
