class UserNotesChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.notes_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_notes_changed: ZU2::UserNotesChanged.new(
          previous: user.notes_was && G::StringValue.new(value: user.notes_was),
          current:  user.notes     && G::StringValue.new(value: user.notes)
        )
      )
    end
  end
end
