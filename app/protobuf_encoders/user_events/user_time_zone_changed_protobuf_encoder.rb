class UserTimeZoneChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.time_zone_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        time_zone_changed: ZU2::TimeZoneChanged.new(
          previous: user.time_zone_was && G::StringValue.new(value: user.time_zone_was),
          current:  user.time_zone     && G::StringValue.new(value: user.time_zone)
        )
      )
    end
  end
end
