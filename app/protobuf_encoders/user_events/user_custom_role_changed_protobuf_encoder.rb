class UserCustomRoleChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.permission_set_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        custom_role_changed: ZU2::SupportCustomRoleChanged.new(
          previous: ZU2::Role.new(
            permission_set_id: G::Int64Value.new(value: user.permission_set_id_was)
          ),
          current: ZU2::Role.new(
            permission_set_id: G::Int64Value.new(value: user.permission_set_id)
          )
        )
      )
    end
  end
end
