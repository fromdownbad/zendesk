class UserExternalIdChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.external_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        external_id_changed: ZU2::ExternalIdChanged.new(
          previous: user.external_id_was && G::StringValue.new(value: user.external_id_was),
          current:  user.external_id     && G::StringValue.new(value: user.external_id)
        )
      )
    end
  end
end
