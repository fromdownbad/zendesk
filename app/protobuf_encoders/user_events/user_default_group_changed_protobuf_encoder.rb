class UserDefaultGroupChangedProtobufEncoder < BaseUserEventProtobufEncoder
  attr_reader :previous, :current

  def initialize(user, previous:, current:)
    super user
    @previous = previous
    @current  = current
  end

  def to_object
    @to_object ||= begin
      ZU2::UserEvent.new(
        user_default_group_changed: ZU2::UserDefaultGroupChanged.new(
          previous: previous && ZG::Group.new(id: G::Int64Value.new(value: previous.id)),
          current:  current && ZG::Group.new(id: G::Int64Value.new(value: current.id))
        )
      )
    end
  end
end
