class UserMergedProtobufEncoder < BaseUserEventProtobufEncoder
  attr_accessor :winner_id

  def initialize(winner_id)
    @winner_id = winner_id
  end

  def to_object
    @to_object ||= begin
      ZU2::UserEvent.new(
        user_merged: ZU2::UserMerged.new(
          winner: ZU2::User.new(id: G::Int64Value.new(value: winner_id))
        )
      )
    end
  end
end
