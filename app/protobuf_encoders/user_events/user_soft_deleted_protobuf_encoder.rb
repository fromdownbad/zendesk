class UserSoftDeletedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    user.is_active_changed? && !user.is_active
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_soft_deleted: ZU2::UserSoftDeleted.new
      )
    end
  end
end
