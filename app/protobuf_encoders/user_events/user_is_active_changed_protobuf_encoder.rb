class UserIsActiveChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    user.is_active_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?
      ZU2::UserEvent.new(
        user_is_active_changed: ZU2::UserIsActiveChanged.new(
          previous: G::BoolValue.new(value: user.is_active_was),
          current: G::BoolValue.new(value: user.is_active)
        )
      )
    end
  end
end
