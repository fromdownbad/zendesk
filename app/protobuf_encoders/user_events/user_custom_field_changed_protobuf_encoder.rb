class UserCustomFieldChangedProtobufEncoder < BaseUserEventProtobufEncoder
  attr_reader :user_field, :previous_value, :current_value

  def initialize(user_field, previous_value, current_value)
    @user_field = user_field
    @previous_value = previous_value
    @current_value = current_value
  end

  def to_object
    @to_object ||= begin
      ZU2::UserEvent.new(
        custom_field_changed: ZU2::CustomFieldChanged.new(
          field: CustomFieldProtobufEncoder.new(user_field).thin.to_object,
          previous: previous_value.presence && ZU2::CustomFieldEntry.new(encode_value(previous_value)),
          current:  current_value.presence && ZU2::CustomFieldEntry.new(encode_value(current_value))
        )
      )
    end
  end

  def encode_value(value)
    case user_field
    when CustomField::Date
      begin
        date = Date.parse(value)
        { date_value: ZC::Date.new(year: date.year, month: date.month, day: date.day) }
      rescue ArgumentError
        # Prior validation only checks DATE_REGEXP, we may still receive
        # impossible dates like February 31st.
        { date_value: nil }
      end
    when CustomField::Checkbox
      { checkbox_value:    value == '1' }
    when CustomField::Decimal
      { decimal_string:    value ? remove_leading_zeros(value) : value }
    when CustomField::Integer
      { integer_string:    value ? value.to_i.to_s : value }
    when CustomField::Regexp
      { regexp_value:      value }
    when CustomField::Dropdown
      { tagger_value:      value && CustomField::DropdownChoice.find_by(id: value, account_id: user_field.account_id)&.value }
    when CustomField::Text
      { text_value:        value }
    when CustomField::Textarea
      { text_area_value:   value }
    else
      raise ArgumentError, "Unexpected user field type: #{user_field.class}"
    end
  end

  private

  def remove_leading_zeros(value_to_parse)
    (left, right) = value_to_parse.split('.')
    left.to_i.to_s + '.' + right.to_s
  end
end
