class UserCreatedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    user.id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_created: ZU2::UserCreated.new
      )
    end
  end
end
