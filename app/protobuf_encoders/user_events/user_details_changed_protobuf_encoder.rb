class UserDetailsChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.details_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_details_changed: ZU2::UserDetailsChanged.new(
          previous: user.details_was && G::StringValue.new(value: user.details_was),
          current:  user.details     && G::StringValue.new(value: user.details)
        )
      )
    end
  end
end
