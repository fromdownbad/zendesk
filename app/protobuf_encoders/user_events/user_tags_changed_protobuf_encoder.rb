class UserTagsChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.tags_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?
      ZU2::UserEvent.new(
        tags_changed: ZU2::TagsChanged.new(
          tags_added: ZC::TagList.new(tags: user.tags_changes[:added]),
          tags_removed: ZC::TagList.new(tags: user.tags_changes[:removed])
        )
      )
    end
  end
end
