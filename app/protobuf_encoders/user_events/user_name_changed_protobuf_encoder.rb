class UserNameChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.name_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_name_changed: ZU2::UserNameChanged.new(
          previous: user.name_was && G::StringValue.new(value: user.name_was),
          current:  user.name     && G::StringValue.new(value: user.name)
        )
      )
    end
  end
end
