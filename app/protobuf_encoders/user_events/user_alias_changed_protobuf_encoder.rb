class UserAliasChangedProtobufEncoder < BaseUserEventProtobufEncoder
  attr_reader :previous, :current

  def initialize(user, previous, current)
    super user
    @previous = previous
    @current  = current
  end

  def to_object
    @to_object ||= begin
      ZU2::UserEvent.new(
        user_alias_changed: ZU2::UserAliasChanged.new(
          previous: G::StringValue.new(value: @previous),
          current: G::StringValue.new(value: @current)
        )
      )
    end
  end
end
