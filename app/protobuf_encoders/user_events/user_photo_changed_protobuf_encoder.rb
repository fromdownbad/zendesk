class UserPhotoChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    user.photo_url != user.photo_url_before_update
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_photo_changed: ZU2::UserPhotoChanged.new(
          previous: ZU2::Photo.new(
            thumbnail_url: G::StringValue.new(value: user.photo_url_before_update),
            original_url:  G::StringValue.new(value: user.large_photo_url_before_update)
          ),
          current:  ZU2::Photo.new(
            thumbnail_url: G::StringValue.new(value: user.photo_url),
            original_url:  G::StringValue.new(value: user.large_photo_url)
          )
        )
      )
    end
  end
end
