class UserOrganizationRemovedProtobufEncoder < BaseUserEventProtobufEncoder
  def initialize(user, organization)
    super(user)
    @organization = organization
  end

  def to_object
    @to_object ||= begin
      ZU2::UserEvent.new(
        user_organization_removed: ZU2::UserOrganizationRemoved.new(
          organization: @organization && ZU2::Organization.new(id: G::Int64Value.new(value: @organization.id))
        )
      )
    end
  end
end
