require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/user_events_pb'

class BaseUserEventProtobufEncoder < ZendeskProtobufEncoder
  class NotImplemented < RuntimeError; end
  class DoesNotMatch   < RuntimeError; end

  alias user entity

  def matches?
    raise NotImplemented
  end

  def to_object
    raise NotImplemented
  end

  def to_safe_object
    to_object
  rescue StandardError => e
    # For now, we prefer silently dropping domain events over crashing User save.
    Rails.logger.error(e)
    encoder_statsd_client.increment('errors', tags: ["exception:#{e.class}"])
    ZendeskExceptions::Logger.record(e, location: self, message: "#{self.class}: #{e.message}", fingerprint: '4ba911ef84a6887c6430e7008c083e166600858e')
    nil
  end

  def encoder_statsd_client
    @encoder_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['support', 'event_bus', 'user', 'encoder'], tags: ["encoder:#{self.class}"])
  end
end
