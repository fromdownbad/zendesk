class UserGroupRemovedProtobufEncoder < BaseUserEventProtobufEncoder
  def initialize(user, group)
    super(user)
    @group = group
  end

  def to_object
    @to_object ||= begin
      ZU2::UserEvent.new(
        user_group_removed: ZU2::UserGroupRemoved.new(
          group: @group && ZG::Group.new(id: G::Int64Value.new(value: @group.id))
        )
      )
    end
  end
end
