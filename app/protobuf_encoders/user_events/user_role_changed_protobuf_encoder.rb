class UserRoleChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.roles_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_role_changed: ZU2::SupportUserRoleChanged.new(
          previous: ZU2::Role.new(
            role_id: G::Int32Value.new(value: user.roles_was)
          ),
          current: ZU2:: Role.new(
            role_id: G::Int32Value.new(value: user.roles)
          )
        )
      )
    end
  end
end
