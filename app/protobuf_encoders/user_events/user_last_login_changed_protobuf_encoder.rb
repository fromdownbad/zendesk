class UserLastLoginChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def initialize(previous, current)
    @previous = previous
    @current = current
  end

  def matches?
    @previous != @current
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        last_login_changed: ZU2::LastLoginChanged.new(
          previous: @previous && G::Timestamp.new(seconds: @previous.to_i, nanos: @previous.nsec),
          current:  @current && G::Timestamp.new(seconds: @current.to_i, nanos: @current.nsec)
        )
      )
    end
  end
end
