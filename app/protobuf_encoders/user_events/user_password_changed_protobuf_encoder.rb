class UserPasswordChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.crypted_password_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        user_password_changed: ZU2::UserPasswordChanged.new
      )
    end
  end
end
