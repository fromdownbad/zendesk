class UserOnlyPrivateCommentsChangedProtobufEncoder < BaseUserEventProtobufEncoder
  def matches?
    !user.user_created? && user.is_private_comments_only_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZU2::UserEvent.new(
        only_private_comments_changed: ZU2::OnlyPrivateCommentsChanged.new(
          previous: G::BoolValue.new(value: user.is_private_comments_only_was),
          current:  G::BoolValue.new(value: user.is_private_comments_only)
        )
      )
    end
  end
end
