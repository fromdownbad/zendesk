require 'zendesk_protobuf_clients/zendesk/protobuf/auth/assumption_events_pb.rb'

class AssumptionEventsProtobufEncoder < ZendeskProtobufEncoder
  attr_reader :event_type, :event_time, :account_id, :original_user_id, :current_user_id

  def initialize(event_type:, event_time:, account_id:, original_user_id:, current_user_id:)
    @event_time = event_time
    @event_type = event_type
    @account_id = account_id
    @original_user_id = original_user_id
    @current_user_id = current_user_id
  end

  def to_object
    @to_object ||= ZAE::AssumptionEvent.new(apply_field_map)
  end

  def full
    @field_map = [
      :account_id,
      :original_user_id,
      :current_user_id,
      :event_type,
      :event_time
    ]

    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :account_id
      G::Int64Value.new(value: account_id)
    when :original_user_id
      G::Int64Value.new(value: original_user_id)
    when :current_user_id
      G::Int64Value.new(value: current_user_id)
    when :event_type
      G::StringValue.new(value: event_type)
    when :event_time
      G::Timestamp.new(seconds: event_time.to_i, nanos: event_time.nsec)
    else
      raise InvalidFieldMap
    end
  end
end
