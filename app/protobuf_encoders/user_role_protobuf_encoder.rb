class UserRoleProtobufEncoder < ZendeskProtobufEncoder
  alias user entity

  def to_object
    @to_object ||= ZU2::Role.new(apply_field_map)
  end

  def full
    @field_map = [
      :role_id,
      :permission_set_id
    ]

    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :role_id
      G::Int32Value.new(value: user.roles)
    when :permission_set_id
      G::Int64Value.new(value: user.permission_set_id)
    else
      raise InvalidFieldMap
    end
  end
end
