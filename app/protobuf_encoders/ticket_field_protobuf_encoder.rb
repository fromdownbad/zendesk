require 'zendesk/protobuf/support/tickets/v2/ticket_field_pb'

class TicketFieldProtobufEncoder < ZendeskProtobufEncoder
  alias ticket_field entity

  TYPE_MAP = {
    "FieldCheckbox"          => ZT::TicketFieldType::CHECKBOX,
    "FieldDate"              => ZT::TicketFieldType::DATE,
    "FieldDecimal"           => ZT::TicketFieldType::DECIMAL,
    "FieldInteger"           => ZT::TicketFieldType::INTEGER,
    "FieldRegexp"            => ZT::TicketFieldType::REGEXP,
    "FieldTagger"            => ZT::TicketFieldType::TAGGER,
    "FieldText"              => ZT::TicketFieldType::TEXT,
    "FieldTextarea"          => ZT::TicketFieldType::TEXTAREA,
    "FieldMultiselect"       => ZT::TicketFieldType::MULTISELECT,
    "FieldPartialCreditCard" => ZT::TicketFieldType::CREDITCARD
  }.freeze

  def to_object
    @to_object ||= ZT::TicketField.new(apply_field_map)
  end

  def thin
    @field_map = [
      :id,
      :raw_title,
      :type
    ]
    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :id
      G::Int64Value.new(value: ticket_field.id)
    when :raw_title
      ticket_field.title && G::StringValue.new(value: ticket_field.title)
    when :type
      ticket_field.type && encode_type
    else
      raise InvalidFieldMap
    end
  end

  def encode_type
    TYPE_MAP[ticket_field.type] || ZT::TicketFieldType::UNKNOWN
  end
end
