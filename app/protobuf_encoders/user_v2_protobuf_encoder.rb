require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/user_pb'

class UserV2ProtobufEncoder < ZendeskProtobufEncoder
  include Zendesk::Users::Identities::UserIdentityEncoder

  alias user entity

  def to_object
    @to_object ||= ZU2::User.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :account,
      :name,
      :email,
      :organization_id,
      :group_id,
      :role,
      :external_id,
      :active,
      :time_zone,
      :locale,
      :alias,
      :photo,
      :details,
      :notes,
      :timestamps,
      :user_field_values,
      :tag_list,
      :organizations,
      :groups,
      :identities
    ]

    self
  end

  def thin
    @field_map = [
      :id,
      :name,
      :email,
      :organization_id,
      :group_id,
      :role,
      :external_id,
      :active,
      :time_zone,
      :locale,
      :alias,
      :photo,
      :timestamps,
      :identities,
      account: [
        :id,
      ],
      organizations: [
        :id,
        :name,
      ],
      groups: [
        :id,
        :name,
      ],
    ]

    self
  end

  private

  def user_fields(user)
    user.custom_field_values.as_json(custom_fields: user.custom_fields, account: user.account).transform_values(&:to_s)
  end

  def encode_identities!
    ZU2::UserIdentityList.new(
      identities: user.identities.map do |identity|
        ZU2::UserIdentity.new(
          id:    G::Int64Value.new(value: identity.id),
          type:  encode_identity_type(identity.type),
          value: G::StringValue.new(value: identity.value)
        )
      end
    )
  end

  def encode_locale
    ZU2::Locale.new(id: G::Int64Value.new(value: user.locale_id), code: G::StringValue.new(value: user.locale)) if user.locale_id
  end

  def encode_photo
    ZU2::Photo.new(
      thumbnail_url: G::StringValue.new(value: user.photo_url),
      original_url:  G::StringValue.new(value: user.large_photo_url)
    )
  end

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: user.id)
    when :account
      AccountProtobufEncoder.new(user.account, sub_map).to_object
    when :name
      G::StringValue.new(value: user.name)
    when :email
      G::StringValue.new(value: user.email)
    when :organization_id
      G::Int64Value.new(value: user.organization_from_default_membership&.id)
    when :group_id
      G::Int64Value.new(value: user.default_group_id)
    when :role
      UserRoleProtobufEncoder.new(user, sub_map).to_object
    when :external_id
      G::StringValue.new(value: user.external_id.to_s)
    when :active
      G::BoolValue.new(value: user.is_active?)
    when :time_zone
      G::StringValue.new(value: user.time_zone)
    when :locale
      encode_locale
    when :alias
      G::StringValue.new(value: user.agent_display_name.to_s)
    when :photo
      encode_photo
    when :details
      G::StringValue.new(value: user.details.to_s)
    when :notes
      G::StringValue.new(value: user.notes.to_s)
    when :timestamps
      TimestampsProtobufEncoder.new(user, sub_map).to_object
    when :user_field_values
      G::Struct.from_hash(user_fields(user))
    when :tag_list
      ZC::TagList.new(tags: user.tags)
    when :organizations
      # this ensures we don't include empty orgs USERV-318
      # since there is an issue when organization_memberships can reference deleted organizations
      orgs = user.organization_memberships.reduce([]) do |org_mem, org|
        if org.organization.present?
          org_mem << OrganizationProtobufEncoder.new(org.organization, sub_map).to_object
        else
          org_mem
        end
      end
      ZU2::OrganizationList.new(organizations: orgs)
    when :groups
      ZU2::GroupList.new(groups: user.memberships.map { |mem| GroupProtobufEncoder.new(mem.group, sub_map).to_object })
    when :identities
      encode_identities!
    else
      raise InvalidFieldMap
    end
  end
end
