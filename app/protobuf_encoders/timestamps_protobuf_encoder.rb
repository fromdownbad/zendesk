require 'zendesk_protobuf_clients/zendesk/protobuf/common/timestamps_pb'

class TimestampsProtobufEncoder < ZendeskProtobufEncoder
  def to_object
    @to_object ||= begin
      ZC::Timestamps.new(apply_field_map)
    end
  end

  def full
    @field_map = [
      :created_at,
      :updated_at
    ]

    self
  end

  private

  def encode_field(field, _sub_map)
    case field
    when :created_at
      entity.created_at && G::Timestamp.new(seconds: entity.created_at.to_i, nanos: entity.created_at.nsec)
    when :updated_at
      entity.updated_at && G::Timestamp.new(seconds: entity.updated_at.to_i, nanos: entity.updated_at.nsec)
    else
      raise InvalidFieldMap
    end
  end
end
