require 'zendesk_protobuf_clients/zendesk/protobuf/common/date_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/accounts/account_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/attachments/attachment_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/locales/locale_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/user_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/organization_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/groups/group_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/auth/assumption_events_pb.rb'
require 'zendesk_protobuf_clients/zendesk/protobuf/support/views/ticket_pb'
require 'zendesk_protobuf_clients/zendesk/protobuf/platform/standard/users/user_authentication_events_pb'

class ZendeskProtobufEncoder
  # Alias long namespaces
  G   = ::Google::Protobuf
  ZC  = ::Zendesk::Protobuf::Common
  ZA  = ::Zendesk::Protobuf::Support::Accounts
  ZAT = ::Zendesk::Protobuf::Support::Attachments
  ZL  = ::Zendesk::Protobuf::Support::Locales
  ZU  = ::Zendesk::Protobuf::Support::Users
  ZU2 = ::Zendesk::Protobuf::Support::Users::V2
  ZG  = ::Zendesk::Protobuf::Support::Groups
  ZT  = ::Zendesk::Protobuf::Support::Tickets::V2
  ZAE = ::Zendesk::Protobuf::Auth::AssumptionEvents
  ZV  = ::Zendesk::Protobuf::Support::Views
  ZPU = Zendesk::Protobuf::Platform::Standard::Users

  class InvalidFieldMap < RuntimeError; end

  attr_reader :field_map, :entity

  def initialize(entity, field_map = [])
    @entity = entity
    # Use provided field map, otherwise default to the full map
    if field_map.any?
      @field_map = field_map
    else
      full
    end
  end

  def to_object
    raise 'Must be implemented by subclass'
  end

  def to_proto
    @to_proto ||= to_object.to_proto
  end

  # This must be implemented by the subclass if field maps are used
  def full; end

  protected

  def message_id
    SecureRandom.uuid
  end

  def sequence_id
    # Binary UUID v4 would be ideal, but unnecessarily hard to generate.
    # Instead, enjoy the same random bytes without the "v4" version mask.
    @sequence_id ||= SecureRandom.random_bytes(16)
  end

  private

  def apply_field_map
    field_map.each_with_object({}) do |field, final_map|
      sub_field_map = []

      if field.is_a?(Hash)
        field.each do |hash_field, hash_sub_field_map|
          final_map[hash_field] = encode_field(hash_field, hash_sub_field_map)
        end
      else
        final_map[field] = encode_field(field, sub_field_map)
      end
    end
  end

  def encode_field(_field, _sub_field_map)
    raise 'Must be implemented by subclass'
  end
end
