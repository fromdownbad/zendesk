require 'zendesk_protobuf_clients/zendesk/protobuf/support/accounts/settings_pb'

class BooleanSettingEncoder < ZendeskProtobufEncoder
  def self.encode(setting)
    {
      previous: maybe(setting.value_was),
      current: maybe(setting.value)
    }
  end

  def self.maybe(value)
    G::BoolValue.new(value: PropertySets::Casting.read(:boolean, value)) unless value.nil?
  end
end

class SettingChangeEncoder < ZendeskProtobufEncoder
  def initialize(setting, type:, name: nil, pb_class: nil, namespace: ZA)
    @setting = setting
    @value_class = "#{type.to_s.camelize}SettingEncoder".constantize
    @event_name = name || "#{setting.name}_changed"
    @pb_class = pb_class || "#{namespace}::#{setting.name.camelize}Changed".constantize
  end

  def to_object
    { @event_name => @pb_class.new(@value_class.encode(@setting)) }
  end
end

class AccountSettingProtobufEncoder < ZendeskProtobufEncoder
  SCHEMA_PATH = 'support/accounts/settings'.freeze
  concerning :PublishedSettings do
    class_methods do
      def setting(key, options = {})
        @settings ||= HashWithIndifferentAccess.new
        @settings[key] = options
      end

      def supported?(key)
        @settings.include? key
      end

      def options(key)
        @settings.fetch key, {}
      end
    end
  end

  setting :end_user_attachments, type: :boolean
  setting :polaris, type: :boolean
  setting :social_messaging_agent_workspace, type: :boolean
  setting :native_messaging, type: :boolean
  setting :focus_mode, type: :boolean

  def initialize(setting)
    @setting = setting
  end

  def to_object
    @to_object ||= ZA::AccountSettingEvent.new(encode(@setting).merge(header: header))
  end

  private

  def encode(setting)
    return {} unless self.class.supported? setting.name
    SettingChangeEncoder.new(setting, **self.class.options(setting.name).symbolize_keys).to_object
  end

  def header
    ZC::ProtobufHeader.new(
      account_id: G::Int32Value.new(value: @setting.account_id),
      occurred_at: G::Timestamp.new(seconds: @setting.updated_at.to_i),
      schema_path: SCHEMA_PATH
    )
  end
end
