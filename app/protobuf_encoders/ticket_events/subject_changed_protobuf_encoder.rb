class SubjectChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.subject_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        subject_changed: ZT::SubjectChanged.new(
          previous:    ticket.subject_was && G::StringValue.new(value: ticket.subject_was),
          current:     ticket.subject     && G::StringValue.new(value: ticket.subject)
        )
      )
    end
  end
end
