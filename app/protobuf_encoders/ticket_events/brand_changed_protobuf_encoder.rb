class BrandChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.brand_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        brand_changed: ZT::BrandChanged.new(
          previous:    ticket.brand_id_was && ZA::Brand.new(id: G::Int64Value.new(value: ticket.brand_id_was)),
          current:     ticket.brand_id     && ZA::Brand.new(id: G::Int64Value.new(value: ticket.brand_id))
        )
      )
    end
  end
end
