class OrganizationChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.organization_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        organization_changed: ZT::OrganizationChanged.new(
          previous:    ticket.organization_id_was && ZU2::Organization.new(id: G::Int64Value.new(value: ticket.organization_id_was)),
          current:     ticket.organization_id     && ZU2::Organization.new(id: G::Int64Value.new(value: ticket.organization_id))
        )
      )
    end
  end
end
