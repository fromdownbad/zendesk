class TicketPermanentlyDeletedProtobufEncoder < BaseTicketEventProtobufEncoder
  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        ticket_permanently_deleted: ZT::TicketPermanentlyDeleted.new
      )
    end
  end
end
