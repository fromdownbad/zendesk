class EmailCcsChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  attr_reader :users_added_ids, :users_removed_ids

  def initialize(users_added_ids, users_removed_ids)
    @users_added_ids = users_added_ids
    @users_removed_ids = users_removed_ids
  end

  def to_object
    @to_object ||= begin
      raise ArgumentError if users_added_ids.empty? && users_removed_ids.empty?
      ZT::TicketEvent.new(
        ccs_changed: ZT::CcsChanged.new(
          users_added: users_added_ids && ZC::UserIdList.new(user_ids: users_added_ids),
          users_removed: users_removed_ids && ZC::UserIdList.new(user_ids: users_removed_ids)
        )
      )
    end
  end
end
