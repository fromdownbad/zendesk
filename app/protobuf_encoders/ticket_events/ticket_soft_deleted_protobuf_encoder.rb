class TicketSoftDeletedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.status_id_changed? && ticket.status_id == StatusType.DELETED
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        ticket_soft_deleted: ZT::TicketSoftDeleted.new
      )
    end
  end
end
