class TicketFormChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.ticket_form_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        ticket_form_changed: ZT::TicketFormChanged.new(
          previous:    ticket.ticket_form_id_was && ZT::TicketForm.new(id: G::Int64Value.new(value: ticket.ticket_form_id_was)),
          current:     ticket.ticket_form_id     && ZT::TicketForm.new(id: G::Int64Value.new(value: ticket.ticket_form_id))
        )
      )
    end
  end
end
