class AttachmentRedactedFromCommentProtobufEncoder < BaseTicketEventProtobufEncoder
  attr_reader :attachment, :comment

  def initialize(attachment, comment)
    @attachment = attachment
    @comment = comment
  end

  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        attachment_redacted_from_comment: ZT::AttachmentRedactedFromComment.new(
          attachment: AttachmentProtobufEncoder.new(attachment).id_only.to_object,
          comment: CommentProtobufEncoder.new(comment).id_only.to_object
        )
      )
    end
  end
end
