class RequesterChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.requester_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        requester_changed: ZT::RequesterChanged.new(
          previous:    ticket.requester_id_was && ZU::User.new(id: G::Int64Value.new(value: ticket.requester_id_was)),
          current:     ticket.requester_id     && ZU::User.new(id: G::Int64Value.new(value: ticket.requester_id))
        )
      )
    end
  end
end
