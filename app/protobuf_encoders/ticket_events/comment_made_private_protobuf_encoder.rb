class CommentMadePrivateProtobufEncoder < BaseTicketEventProtobufEncoder
  def initialize(comment)
    @comment = comment
  end

  attr_reader :comment

  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        comment_made_private: ZT::CommentMadePrivate.new(
          comment: ZT::Comment.new(
            id:        G::Int64Value.new(value: comment.id),
            is_public: G::BoolValue.new(value: comment.is_public?)
          )
        )
      )
    end
  end
end
