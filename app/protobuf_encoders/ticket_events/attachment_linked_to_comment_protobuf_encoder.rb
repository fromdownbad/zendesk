class AttachmentLinkedToCommentProtobufEncoder < BaseTicketEventProtobufEncoder
  attr_reader :attachment, :comment

  def initialize(attachment, comment)
    @attachment = attachment
    @comment = comment
  end

  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        attachment_linked_to_comment: ZT::AttachmentLinkedToComment.new(
          attachment: AttachmentProtobufEncoder.new(attachment).full.to_object,
          comment: CommentProtobufEncoder.new(comment).id_only.to_object
        )
      )
    end
  end
end
