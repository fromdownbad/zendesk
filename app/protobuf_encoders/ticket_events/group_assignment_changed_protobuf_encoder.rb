class GroupAssignmentChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.group_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        group_assignment_changed: ZT::GroupAssignmentChanged.new(
          previous:    ticket.group_id_was && ZG::Group.new(id: G::Int64Value.new(value: ticket.group_id_was)),
          current:     ticket.group_id     && ZG::Group.new(id: G::Int64Value.new(value: ticket.group_id))
        )
      )
    end
  end
end
