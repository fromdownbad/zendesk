require 'zendesk_protobuf_clients/zendesk/protobuf/common/date_pb'

class TaskDueAtChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.due_date_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        task_due_at_changed: ZT::TaskDueAtChanged.new(
          previous: ticket.due_date_was && G::Timestamp.new(seconds: ticket.due_date_was.to_i, nanos: ticket.due_date_was.nsec),
          current:  ticket.due_date     && G::Timestamp.new(seconds: ticket.due_date.to_i, nanos: ticket.due_date.nsec)
        )
      )
    end
  end
end
