class CommentAddedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    # - :comment is an attr_accessor assigned by Ticket#add_comment
    # - `Comment#empty?` is subsequently checked on Ticket#set_comment
    # TODO: We haven't decided how to model attachments yet, but we want parity
    #       with `Comment` creation.
    comment = ticket.comment
    !comment.nil? && (!comment.empty? || comment.attachments.any?)
  end

  def to_object
    raise DoesNotMatch unless matches?
    @to_object ||= begin
      ZT::TicketEvent.new(
        comment_added: ZT::CommentAdded.new(
          comment: CommentProtobufEncoder.new(ticket.comment).to_object
        )
      )
    end
  end
end
