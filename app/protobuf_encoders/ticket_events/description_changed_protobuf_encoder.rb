class DescriptionChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  # Description is not _generally_ mutatable. This event should only be
  # triggered by:
  # 1. Ticket creation
  # 2. Comment redaction (see Ticket#redact_comment!)
  def matches?
    ticket.description_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        description_changed: ZT::DescriptionChanged.new(
          previous:    ticket.description_was && G::StringValue.new(value: ticket.description_was),
          current:     ticket.description     && G::StringValue.new(value: ticket.description)
        )
      )
    end
  end
end
