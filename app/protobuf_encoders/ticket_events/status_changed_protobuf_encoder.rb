class StatusChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.status_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        status_changed: ZT::StatusChanged.new(
          previous:    TicketStatusProtobufEncoder::STATUS_MAP[ticket.status_id_was],
          current:     TicketStatusProtobufEncoder::STATUS_MAP[ticket.status_id]
        )
      )
    end
  end
end
