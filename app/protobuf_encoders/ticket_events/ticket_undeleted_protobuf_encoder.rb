class TicketUndeletedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.status_id_changed? && ticket.status_id_was == StatusType.DELETED
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        ticket_undeleted: ZT::TicketUndeleted.new
      )
    end
  end
end
