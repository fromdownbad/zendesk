class ExternalIdChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.external_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        external_id_changed: ZT::ExternalIdChanged.new(
          previous:    ticket.external_id_was && G::StringValue.new(value: ticket.external_id_was),
          current:     ticket.external_id     && G::StringValue.new(value: ticket.external_id)
        )
      )
    end
  end
end
