class TicketCreatedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        ticket_created: ZT::TicketCreated.new
      )
    end
  end
end
