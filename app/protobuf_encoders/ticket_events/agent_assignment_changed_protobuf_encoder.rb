class AgentAssignmentChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.assignee_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        agent_assignment_changed: ZT::AgentAssignmentChanged.new(
          previous:    ticket.assignee_id_was && ZU::User.new(id: G::Int64Value.new(value: ticket.assignee_id_was)),
          current:     ticket.assignee_id     && ZU::User.new(id: G::Int64Value.new(value: ticket.assignee_id))
        )
      )
    end
  end
end
