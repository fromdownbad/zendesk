class TicketMergedProtobufEncoder < BaseTicketEventProtobufEncoder
  def initialize(source_ticket, target_ticket)
    @source_ticket = source_ticket
    @target_ticket = target_ticket
  end

  attr_reader :source_ticket, :target_ticket

  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        ticket_merged: ZT::TicketMerged.new(
          target_ticket: TicketProtobufEncoder.new(target_ticket).id_only.to_object
        )
      )
    end
  end
end
