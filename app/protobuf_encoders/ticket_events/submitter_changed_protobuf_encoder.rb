class SubmitterChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.submitter_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        submitter_changed: ZT::SubmitterChanged.new(
          previous:    ticket.submitter_id_was && ZU::User.new(id: G::Int64Value.new(value: ticket.submitter_id_was)),
          current:     ticket.submitter_id     && ZU::User.new(id: G::Int64Value.new(value: ticket.submitter_id))
        )
      )
    end
  end
end
