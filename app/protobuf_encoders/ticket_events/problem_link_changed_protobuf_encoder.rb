class ProblemLinkChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.linked_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      linked_ticket_was = Ticket.with_deleted { Ticket.find(ticket.linked_id_was) } if ticket.linked_id_was
      linked_ticket     = ticket.problem

      ZT::TicketEvent.new(
        problem_link_changed: ZT::ProblemLinkChanged.new(
          previous: linked_ticket_was && TicketProtobufEncoder.new(linked_ticket_was).id_only.to_object,
          current:  linked_ticket     && TicketProtobufEncoder.new(linked_ticket).id_only.to_object
        )
      )
    end
  end
end
