class TagsChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.current_tags_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      # returns Array<String>
      new_tags = ticket.send(:normalize_tags, ticket.current_tags)
      old_tags = ticket.send(:normalize_tags, ticket.current_tags_was)

      # I thought to use `Set#-`, but `Array#-` benchmarks almost twice as fast
      removed = old_tags - new_tags
      added   = new_tags - old_tags

      ZT::TicketEvent.new(
        tags_changed: ZT::TagsChanged.new(
          tags_added:   ZC::TagList.new(tags: added),
          tags_removed: ZC::TagList.new(tags: removed)
        )
      )
    end
  end
end
