require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_events_pb'

class BaseTicketEventProtobufEncoder < ZendeskProtobufEncoder
  class NotImplemented < RuntimeError; end
  class DoesNotMatch   < RuntimeError; end

  alias ticket entity

  def matches?
    raise NotImplemented
  end

  def to_object
    raise NotImplemented
  end

  def to_safe_object
    to_object
  rescue StandardError => e
    # For now, we prefer silently dropping domain events over crashing Ticket save.
    Rails.logger.error(e)
    encoder_statsd_client.increment('errors', tags: ["exception:#{e.class}"])
    ZendeskExceptions::Logger.record(e, location: self, message: "#{self.class}: #{e.message}", fingerprint: '8e83b892d8a48de4c05d0c6c184295a22cbf03ca')
    nil
  end

  def encoder_statsd_client
    @encoder_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['support', 'event_bus', 'ticket', 'encoder'], tags: ["encoder:#{self.class}"])
  end
end
