class CommentRedactedProtobufEncoder < BaseTicketEventProtobufEncoder
  attr_reader :comment

  def initialize(comment)
    @comment = comment
  end

  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        comment_redacted: ZT::CommentRedacted.new(
          comment: CommentProtobufEncoder.new(comment).to_object
        )
      )
    end
  end
end
