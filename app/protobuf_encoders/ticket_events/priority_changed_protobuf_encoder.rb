class PriorityChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.priority_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        priority_changed: ZT::PriorityChanged.new(
          previous:    TicketPriorityProtobufEncoder::PRIORITY_MAP[ticket.priority_id_was],
          current:     TicketPriorityProtobufEncoder::PRIORITY_MAP[ticket.priority_id]
        )
      )
    end
  end
end
