class CustomFieldChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  attr_reader :ticket_field, :previous_value, :current_value

  def initialize(ticket_field, previous_value, current_value)
    @ticket_field = ticket_field
    @previous_value = previous_value
    @current_value = current_value
  end

  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        custom_field_changed: ZT::CustomFieldChanged.new(
          ticket_field: TicketFieldProtobufEncoder.new(ticket_field).thin.to_object,
          previous:    previous_value.presence && ZT::TicketFieldEntry.new(encode_value(previous_value)),
          current:     current_value.presence && ZT::TicketFieldEntry.new(encode_value(current_value))
        )
      )
    end
  end

  def encode_value(value)
    case ticket_field
    when FieldDate
      begin
        date = Date.parse(value)
        { date_value: ZC::Date.new(year: date.year, month: date.month, day: date.day) }
      rescue ArgumentError, RangeError
        # Prior validation only checks DATE_REGEXP, we may still receive
        # impossible dates like February 31st.
        # We can also get an impossibly large year due to trigger values we allow like
        # '2737907006988507635340185-11-15' which blow our int32 year in the schemas
        { date_value: nil }
      end
    when FieldCheckbox
      { checkbox_value:    value == '1' }
    when FieldDecimal
      { decimal_string:    value }
    when FieldInteger
      { integer_string:    value }
    when FieldRegexp
      { regexp_value:      value }
    when FieldMultiselect
      { multiselect_value: ZC::TagList.new(tags: value.split(' ')) }
    when FieldTagger
      { tagger_value:      value }
    when FieldText
      { text_value:        value }
    when FieldTextarea
      { text_area_value:   value }
    when FieldPartialCreditCard
      { credit_card_value: value }
    else
      raise ArgumentError, "Unexpected ticket field type: #{ticket_field_entry.ticket_field.class}"
    end
  end
end
