class TicketTypeChangedProtobufEncoder < BaseTicketEventProtobufEncoder
  def matches?
    ticket.ticket_type_id_changed?
  end

  def to_object
    @to_object ||= begin
      raise DoesNotMatch unless matches?

      ZT::TicketEvent.new(
        ticket_type_changed: ZT::TicketTypeChanged.new(
          previous:    TicketTypeProtobufEncoder::TYPE_MAP[ticket.ticket_type_id_was],
          current:     TicketTypeProtobufEncoder::TYPE_MAP[ticket.ticket_type_id]
        )
      )
    end
  end
end
