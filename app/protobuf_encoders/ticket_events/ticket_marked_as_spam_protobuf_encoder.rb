class TicketMarkedAsSpamProtobufEncoder < BaseTicketEventProtobufEncoder
  def to_object
    @to_object ||= begin
      ZT::TicketEvent.new(
        marked_as_spam: ZT::MarkedAsSpam.new
      )
    end
  end
end
