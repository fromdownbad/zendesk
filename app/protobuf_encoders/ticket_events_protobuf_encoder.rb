require 'zendesk/protobuf/support/tickets/v2/ticket_field_pb'

class TicketEventsProtobufEncoder < ZendeskProtobufEncoder
  alias ticket entity

  delegate :any?, to: :event_list

  SCHEMA_PATH = 'support/tickets/v2/ticket_events'.freeze

  # The order of this list determines order of publishing
  INFERRED_EVENTS = [
    TicketCreatedProtobufEncoder,
    TicketSoftDeletedProtobufEncoder,
    TicketUndeletedProtobufEncoder,
    CommentAddedProtobufEncoder,
    StatusChangedProtobufEncoder,
    PriorityChangedProtobufEncoder,
    TagsChangedProtobufEncoder,
    SubjectChangedProtobufEncoder,
    DescriptionChangedProtobufEncoder,
    TicketTypeChangedProtobufEncoder,
    RequesterChangedProtobufEncoder,
    OrganizationChangedProtobufEncoder,
    SubmitterChangedProtobufEncoder,
    AgentAssignmentChangedProtobufEncoder,
    GroupAssignmentChangedProtobufEncoder,
    ExternalIdChangedProtobufEncoder,
    BrandChangedProtobufEncoder,
    TaskDueAtChangedProtobufEncoder,
    ProblemLinkChangedProtobufEncoder,
    TicketFormChangedProtobufEncoder
  ].freeze

  def to_a
    @to_a ||= event_list.each_with_index do |event, i|
      # Add "ticket"
      event.ticket = TicketProtobufEncoder.new(ticket).thin.to_object

      # Add "via"
      # TODO: This should actually be event-specific, and may need inferring from the audit events.
      # That's hard. We probably need to hoist this into BaseTicketEventProtobufEncoder
      event.via = ViaTypeProtobufEncoder.new(ticket.audit).to_object if ticket.audit

      # Add "actor"
      # TODO: Convert to UserProtobufEncoder when projection is implemented. (or not - it's just the id)
      event.actor = actor_context

      # Add "header"
      # Due to mutating `sequence`, this object is mutated and shouldn't be memoized
      event.header = ZC::ProtobufHeader.new(
        schema_path: SCHEMA_PATH,
        message_id:  G::StringValue.new(value: message_id),
        account_id:  G::Int32Value.new(value: ticket.account_id),
        occurred_at: G::Timestamp.new(seconds: ticket.updated_at.to_i),
        sequence:    ZC::MessageSequence.new(
          sequence_id: sequence_id,
          position:    i + 1,
          total:       event_list.length
        )
      )
    end
  end

  def actor_context
    @actor_context ||= ZU::User.new(id: G::Int64Value.new(value: ticket.current_user.id)) if ticket.current_user
  end

  # Inferred and Explicit domain events
  def event_list
    @event_list ||= ticket.domain_events + inferred_events
  end

  def inferred_events
    INFERRED_EVENTS.
      map { |klass| klass.new(ticket) }.
      select(&:matches?).
      map(&:to_safe_object).
      compact
  end
end
