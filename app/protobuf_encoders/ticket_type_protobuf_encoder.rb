require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_pb'

class TicketTypeProtobufEncoder < ZendeskProtobufEncoder
  alias ticket entity

  TYPE_MAP = {
    TicketType.-        => ZT::TicketType::UNKNOWN_TICKET_TYPE,
    TicketType.QUESTION => ZT::TicketType::QUESTION,
    TicketType.INCIDENT => ZT::TicketType::INCIDENT,
    TicketType.PROBLEM  => ZT::TicketType::PROBLEM,
    TicketType.TASK     => ZT::TicketType::TASK
  }.freeze

  def to_object
    @to_object ||= TYPE_MAP[ticket.ticket_type_id] || ZT::TicketType::UNKNOWN_TICKET_TYPE
  end
end
