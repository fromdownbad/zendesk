require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_pb'

class TicketProtobufEncoder < ZendeskProtobufEncoder
  alias ticket entity

  def to_object
    @to_object ||= ZT::Ticket.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :account,
      :requester,
      :submitter,
      :assignee,
      :group,
      :status,
      :priority,
      :via,
      :ticket_type,
      :linked_id,
      :timestamps,
      :description,
      :nice_id,
      :recipient,
      :organization,
      :due_at,
      :tags,
      :collaborators,
      :raw_subject,
      :external_id,
      :generated_timestamp,
      :satisfaction_score,
      :ticket_form,
      :brand,
      :is_public,
    ]

    self
  end

  def thin
    @field_map = [
      :id,
      :status,
      :priority,
      :via,
      :ticket_type,
      :linked_id,
      :timestamps,
      :description,
      :nice_id,
      :external_id,
      :tags,
      :raw_subject,
      :generated_timestamp,
      :is_public,
      :description,
      account: [
        :id
      ],
      requester: [
        :id
      ],
      submitter: [
        :id
      ],
      assignee: [
        :id
      ],
      group: [
        :id
      ],
      brand: [
        :id
      ],
      organization: [
        :id
      ],
      ticket_form: [
        :id
      ],
    ]

    self
  end

  def id_only
    @field_map = [:id, :nice_id]
    self
  end

  private

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: ticket.id)
    when :account
      AccountProtobufEncoder.new(ticket.account, sub_map).to_object
    when :requester
      UserProtobufEncoder.new(ticket.requester, sub_map).to_object
    when :submitter
      ticket.submitter && UserProtobufEncoder.new(ticket.submitter, sub_map).to_object
    when :assignee
      ticket.assignee && UserProtobufEncoder.new(ticket.assignee, sub_map).to_object
    when :group
      ticket.group && GroupProtobufEncoder.new(ticket.group, sub_map).to_object
    when :status
      TicketStatusProtobufEncoder.new(ticket).to_object
    when :priority
      TicketPriorityProtobufEncoder.new(ticket).to_object
    when :via
      ViaTypeProtobufEncoder.new(ticket).to_object
    when :ticket_type
      TicketTypeProtobufEncoder.new(ticket).to_object
    when :linked_id
      ticket.linked_id && G::Int64Value.new(value: ticket.linked_id)
    when :timestamps
      TimestampsProtobufEncoder.new(ticket).to_object
    when :description
      G::StringValue.new(value: ticket.description)
    when :nice_id
      G::Int64Value.new(value: ticket.nice_id)
    when :recipient
      ticket.recipient && G::StringValue.new(value: ticket.recipient)
    when :organization
      ticket.organization && OrganizationProtobufEncoder.new(ticket.organization, sub_map).to_object
    when :due_at
      ticket.due_date && G::Timestamp.new(seconds: ticket.due_date.to_i, nanos: ticket.due_date.nsec)
    when :tags
      ticket.current_tags && ZC::TagList.new(tags: ticket.current_tags.split(' '))
    when :collaborators
      ticket.current_collaborators && G::StringValue.new(value: ticket.current_collaborators)
    when :raw_subject
      ticket.subject && G::StringValue.new(value: ticket.subject)
    when :external_id
      # Database has a mix empty strings and null. This seems accidental.
      # Always present empty strings as null.
      ticket.external_id.presence && G::StringValue.new(value: ticket.external_id)
    when :generated_timestamp
      G::Timestamp.new(seconds: ticket.generated_timestamp&.to_i, nanos: ticket.generated_timestamp&.nsec)
    when :satisfaction_score
      SatisfactionScoreProtobufEncoder.new(ticket, sub_map).to_object
    when :ticket_form
      ticket.ticket_form && TicketFormProtobufEncoder.new(ticket.ticket_form, sub_map).to_object
    when :brand
      ticket.brand && BrandProtobufEncoder.new(ticket.brand, sub_map).to_object
    when :is_public
      G::BoolValue.new(value: ticket.is_public)
    else
      raise InvalidFieldMap
    end
  end
end
