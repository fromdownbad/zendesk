require 'zendesk_protobuf_clients/zendesk/protobuf/support/tickets/v2/ticket_form_pb'

class TicketFormProtobufEncoder < ZendeskProtobufEncoder
  alias ticket_form entity

  def to_object
    @to_object ||= ZT::TicketForm.new(apply_field_map)
  end

  def full
    @field_map = [
      :id,
      :account,
      :name,
      :display_name,
      :is_end_user_visible,
      :position,
      :is_active,
      :is_default,
      :is_in_all_organizations,
      :is_in_all_brands,
      :timestamps,
      :deleted_at
    ]

    self
  end

  private

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: ticket_form.id)
    when :account
      AccountProtobufEncoder.new(ticket_form.account, sub_map).to_object
    when :name
      G::StringValue.new(value: ticket_form.name)
    when :display_name
      G::StringValue.new(value: ticket_form.display_name)
    when :is_end_user_visible
      G::BoolValue.new(value: ticket_form.end_user_visible)
    when :position
      G::Int64Value.new(value: ticket_form.position)
    when :is_active
      G::BoolValue.new(value: ticket_form.active)
    when :is_default
      G::BoolValue.new(value: ticket_form.default)
    when :is_in_all_organizations
      G::BoolValue.new(value: ticket_form.in_all_organizations)
    when :is_in_all_brands
      G::BoolValue.new(value: ticket_form.in_all_brands)
    when :timestamps
      TimestampsProtobufEncoder.new(ticket_form).to_object
    when :deleted_at
      ticket_form.deleted_at && G::Timestamp.new(seconds: ticket_form.deleted_at.to_i, nanos: ticket_form.deleted_at.nsec)
    else
      raise InvalidFieldMap
    end
  end
end
