require 'zendesk_protobuf_clients/zendesk/protobuf/support/accounts/brand_pb'

class BrandProtobufEncoder < ZendeskProtobufEncoder
  alias brand entity

  def to_object
    @to_object ||= Brand.with_deleted { ZA::Brand.new(apply_field_map) }
  end

  def full
    @field_map = [
      :id,
      :account,
      :name,
      :is_active,
      :timestamps,
      :route,
      :signature_template,
      :logo
    ]

    self
  end

  private

  def encode_field(field, sub_map)
    case field
    when :id
      G::Int64Value.new(value: brand.id)
    when :account
      AccountProtobufEncoder.new(brand.account, sub_map).to_object
    when :name
      G::StringValue.new(value: brand.name)
    when :is_active
      G::BoolValue.new(value: brand.active)
    when :timestamps
      TimestampsProtobufEncoder.new(brand).to_object
    when :route
      RouteProtobufEncoder.new(brand.route, sub_map).to_object
    when :signature_template
      brand.signature_template && G::StringValue.new(value: brand.signature_template)
    when :logo
      ZA::BrandLogo.new(thumbnail_url: logo)
    else
      raise InvalidFieldMap
    end
  end

  def logo
    thumbnail = brand&.logo&.thumbnails&.detect { |t| t.thumbnail == "thumb" }
    G::StringValue.new(value: ContentUrlBuilder.new.url_for(thumbnail))
  end
end
