class LotusBootstrapServiceMiddleware
  LOTUS_PATH_PREFIX = /^\/agent\/?/

  def initialize(app)
    @app = app
  end

  def call(env)
    if use_lotus_bootstrap_service?(env) && redirect = redirect_path(env['PATH_INFO'])
      headers = { "X-Accel-Redirect" => redirect }
      return [200, headers, ['Lotus Bootstrap Service Redirect']]
    end

    @app.call(env)
  end

  private

  def use_lotus_bootstrap_service?(env)
    ::Arturo.feature_enabled_for?(:lotus_bootstrap_service, env['zendesk.account'])
  end

  def redirect_path(path)
    path.dup.sub!(LOTUS_PATH_PREFIX, '/agent/boot/')
  end
end
