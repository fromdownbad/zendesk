class MappedDatabaseExceptionsMiddleware
  SPLIT = '::'.freeze

  def initialize(app, _options = {})
    @app = app
  end

  def call(env)
    @app.call(env)
  rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::MappedDatabaseException => e
    record_exception(e)
    raise e # reraise so we don't randomly change the response
  end

  private

  def record_exception(e)
    name   = e.class.name
    metric = "db_exception.#{name.split(SPLIT).last.underscore}"

    Rails.application.config.statsd.client.increment(metric)
    Rails.logger.error "#{name} -- #{e.message}"
  end
end
