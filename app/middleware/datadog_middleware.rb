require 'ddtrace'

class DatadogMiddleware
  NOT_AVAILABLE = 'N/A'.freeze

  def self.find_request_span(env)
    env[Datadog::Contrib::Rack::Ext::RACK_ENV_REQUEST_SPAN] || Datadog.tracer.active_span
  end

  def initialize(app)
    @app = app
    @rss_reader = UnicornWrangler::RssReader.new(logger: Rails.logger) if defined?(UnicornWrangler)
  end

  def call(env)
    pre_request_rss = @rss_reader.rss if @rss_reader
    @app.call(env)
  ensure
    if request_span = find_request_span(env)
      request = Rack::Request.new(env)
      request_origin = Zendesk::RequestOriginDetails.new(env)

      correlate_logs(request_span, request_origin)

      record_queueing_time(request_span, env)

      set_application_tags(request_span)
      set_unicorn_tags(request_span, pre_request_rss)
      set_zendesk_tags(request_span, request_origin, env)
      set_mobile_tags(request_span, request_origin)
      set_request_tags(request_span, request, request_origin, env)
      set_extra_tags(request_span, env)
      set_trace_id(request_span)
      set_prop_tags(request_span, env)
    end
  end

  private

  QUEUEING_DELAY_METRIC = 'http.request.queueing_delay'.freeze

  private_constant :QUEUEING_DELAY_METRIC

  def correlate_logs(request_span, request_origin)
    Rails.logger.append_attributes(
      {
        dd: {
          trace_id: request_span.trace_id,
          span_id: request_span.span_id
        }
      }.merge(
        **logger_account_attributes(request_origin),
        **logger_lotus_attributes(request_origin),
        **logger_mobile_attributes(request_origin)
      )
    )
  end

  def logger_account_attributes(request_origin)
    account = request_origin.account

    return {} if account.nil?

    time_now = Process.clock_gettime(Process::CLOCK_REALTIME, :second)

    {
      zendesk: {
        account_id: account.id,
        account_subdomain: account.subdomain,
        account_created_at: account.created_at.to_date.to_s(:db),
        account_age: (time_now - account.created_at.to_i).to_i / 86400,
        account_premier_support: premier_support_feature_addon(account),
        whitelisted_from_fraud_restrictions: account.settings.whitelisted_from_fraud_restrictions
      }
    }
  end

  def logger_lotus_attributes(request_origin)
    lotus_version = request_origin.lotus_version
    app_id = request_origin.app_id

    return {} if lotus_version.nil? && app_id.nil?

    {
      zendesk: {
        lotus: {
          version: lotus_version,
          app_id: app_id
        }
      }
    }
  end

  def logger_mobile_attributes(request_origin)
    client = request_origin.mobile_client

    return {} unless request_origin.zendesk_mobile_client?

    {
      zendesk: {
        mobile: {
          client: client,
          client_version: request_origin.mobile_client_version,
          integration: request_origin.mobile_integration,
          integration_version: request_origin.mobile_integration_version
        }
      }
    }
  end

  def find_request_span(env)
    DatadogMiddleware.find_request_span(env)
  end

  def record_queueing_time(request_span, env)
    request_start = Datadog::Contrib::Rack::QueueTime.get_request_start(env)
    return if request_start.nil?

    queueing_time = (request_span.start_time - request_start).round(3)

    request_span.set_tag(QUEUEING_DELAY_METRIC, queueing_time)
    Rails.application.config.statsd.client.histogram(QUEUEING_DELAY_METRIC, queueing_time)
  end

  # rubocop:disable Naming/AccessorMethodName
  def set_application_tags(request_span)
    tag_with_default(request_span, 'application.version',     GIT_HEAD_TAG)
    tag_with_default(request_span, 'application.revision',    GIT_HEAD_SHA)
    tag_with_default(request_span, 'application.github_tree', github_tree_url)

    tag_with_default(request_span, 'rails.version', Rails.version)
    tag_with_default(request_span, 'ruby.version',  RUBY_VERSION)
  end

  def set_unicorn_tags(request_span, pre_request_rss)
    return unless defined?(UnicornWrangler)

    if count = UnicornWrangler.requests
      request_span.set_tag('zendesk.unicorn.request_count', count)
    end

    if rss = @rss_reader&.rss
      request_span.set_tag('zendesk.unicorn.worker_memory', rss)
      request_span.set_tag('zendesk.unicorn.worker_memory_delta', rss - pre_request_rss) if pre_request_rss > 0
    end
  end

  def user_id_mismatch(user_id, request_origin)
    return ['both_blank', false] if user_id.blank? && request_origin.lotus_initial_user_id.blank?
    return ['no_user_id', true] if user_id.blank? && !request_origin.lotus_initial_user_id.blank?
    return ['no_lotus_initial_user_id', true] if !user_id.blank? && request_origin.lotus_initial_user_id.blank?
    return ['mismatch', true] if request_origin.lotus_initial_user_id != user_id.to_s
    ['no_mismatch', false]
  end

  def browser_id_mismatch(env, request_origin)
    shared_session = env['zendesk.shared_session']

    if shared_session
      stored_lotus_uuid = Rails.cache.read("zendesk.lotus_uuid-#{shared_session['id']}")

      if stored_lotus_uuid
        stored_lotus_uuid == request_origin.lotus_uuid ? ['no_mismatch', false] : ['mismatch', true]
      else
        Rails.cache.write("zendesk.lotus_uuid-#{shared_session['id']}", request_origin.lotus_uuid, expires_in: 1.day)
        ['no_stored_lotus_uuid', true]
      end
    end
  end

  def set_zendesk_tags(request_span, request_origin, env)
    account = env['zendesk.account']
    brand = env['zendesk.brand']
    user = env['zendesk.user']

    shared_session = env['zendesk.shared_session']
    original_user_id = shared_session ? shared_session['auth_original_user_id'] : nil
    is_assuming_user = original_user_id.present? || 'false'
    Rails.logger.debug("user_id: #{user&.id}")
    Rails.logger.debug("original_user_id: #{original_user_id}")
    Rails.logger.debug("is_assuming_user: #{is_assuming_user}")

    tag_with_default(request_span, 'zendesk.account_id',        account&.id)
    tag_with_default(request_span, 'zendesk.account_subdomain', account&.subdomain)
    tag_with_default(request_span, 'zendesk.brand_id',          brand&.id)
    tag_with_default(request_span, 'zendesk.brand_subdomain',   brand&.subdomain)
    tag_with_default(request_span, 'zendesk.user_id',           user&.id)
    tag_with_default(request_span, 'zendesk.user_role',         user&.role)

    tag_with_default(request_span, 'zendesk.lotus_version',  request_origin.lotus_version)
    tag_with_default(request_span, 'zendesk.lotus_feature',  request_origin.lotus_feature)
    tag_with_default(request_span, 'zendesk.app_id',         request_origin.app_id)
    tag_with_default(request_span, 'zendesk.app_name',       request_origin.app_name)
    tag_with_default(request_span, 'zendesk.request_source', request_origin.request_source)
    tag_with_default(request_span, 'zendesk.pod_relay',      request_origin.pod_relay)

    tag_with_default(request_span, 'zendesk.lotus_initial_user_id',     request_origin.lotus_initial_user_id)
    tag_with_default(request_span, 'zendesk.lotus_initial_user_role',   request_origin.lotus_initial_user_role)
    tag_with_default(request_span, 'zendesk.lotus_tab_id',              request_origin.lotus_tab_id)
    tag_with_default(request_span, 'zendesk.lotus_uuid',                request_origin.lotus_uuid)
    tag_with_default(request_span, 'zendesk.lotus_expiry_refetch',      request_origin.lotus_expiry_refetch)
    tag_with_default(request_span, 'zendesk.lotus_radar_client_socket', request_origin.lotus_radar_client_socket)
    tag_with_default(request_span, 'zendesk.lotus_radar_client_status', request_origin.lotus_radar_client_status)
    tag_with_default(request_span, 'zendesk.force_exception_locale',    request_origin.force_exception_locale)
    tag_with_default(request_span, 'zendesk.is_assuming_user',          is_assuming_user)
    tag_with_default(request_span, 'zendesk.original_user_id',          original_user_id)

    details, mismatch = user_id_mismatch(user&.id, request_origin)
    request_span.set_tag('zendesk.lotus_user_id_mismatch', mismatch)
    request_span.set_tag('zendesk.lotus_user_id_mismatch_details', details)

    browser_details, browser_mismatch = browser_id_mismatch(env, request_origin)
    request_span.set_tag('zendesk.lotus_uuid_mismatch', browser_mismatch)
    request_span.set_tag('zendesk.lotus_uuid_mismatch_details', browser_details)

    # db params must be Integers, so don't set them to N/A if they aren't available
    if account
      request_span.set_tag('zendesk.account_is_sandbox', !!account.is_sandbox?)
      request_span.set_tag('zendesk.account_is_trial',   !!account.is_trial?)
      request_span.set_tag('zendesk.account_premier_support', premier_support_feature_addon(account))
      request_span.set_tag('zendesk.db.shard', account.shard_id)
      # Account features/capabilities should follow the `account_features` namespace
      request_span.set_tag('zendesk.account_features.agent_workspace', !!account.settings&.polaris)

      # We use this constant in order to avoid calls to Consul at runtime.
      if db_cluster = DB_SHARDS_TO_CLUSTERS[account.shard_id]
        request_span.set_tag('zendesk.db.cluster', db_cluster.to_s)
      end
    end
    env['zendesk.idempotency_hit'].try { |is_hit| request_span.set_tag('zendesk.idempotency_hit', is_hit) }
  end

  def set_mobile_tags(request_span, request_origin)
    tag_with_default(request_span, 'zendesk.mobile.client',              request_origin.mobile_client)
    tag_with_default(request_span, 'zendesk.mobile.client_version',      request_origin.mobile_client_version)
    tag_with_default(request_span, 'zendesk.mobile.integration',         request_origin.mobile_integration)
    tag_with_default(request_span, 'zendesk.mobile.integration_version', request_origin.mobile_integration_version)
  end

  def set_request_tags(request_span, request, request_origin, env)
    tag_with_default(request_span, 'rails.request_id',               env['request_id'])
    tag_with_default(request_span, 'request_ip',                     request_origin.remote_ip)
    tag_with_default(request_span, 'http.request.headers.referer',   request_origin.referer)
    tag_with_default(request_span, 'http.request.params.sort_by',    request.params['sort_by'])
    tag_with_default(request_span, 'http.request.params.sort_order', request.params['sort_order'])

    # page params must be Integers, so don't set them to N/A if they aren't available
    if (params_page = request.params['page'].to_s.to_i) && params_page > 0
      request_span.set_tag('http.request.params.page', params_page.to_i)
    end

    if (params_per_page = request.params['per_page'].to_s.to_i) && params_per_page > 0
      request_span.set_tag('http.request.params.per_page', params_per_page.to_i)
    end

    # CBP v2:
    #   * page[size]=5
    #   * page[size]=5&page[after]=xxx
    #   * page[size]=5&page[before]=yyy
    #   * page[size]=5&sort=created_at
    #
    # To separate things from offset pagination, we add the tags
    # using cursor[size], cursor[before] and cursor[after].
    if (cbp_page = request.params['page']).is_a?(Hash)
      if (page_size = cbp_page['size'].to_s.to_i) > 0
        request_span.set_tag('http.request.params.cursor.size', page_size)
      end
      if cbp_page['before'].present?
        request_span.set_tag('http.request.params.cursor.before', cbp_page['before'])
      end
      if cbp_page['after'].present?
        request_span.set_tag('http.request.params.cursor.after', cbp_page['after'])
      end
      if request.params['sort'].present?
        request_span.set_tag('http.request.params.sort', request.params['sort'])
      end
    end

    # https://developer.zendesk.com/rest_api/docs/support/incremental_export#side-loading
    if includes = request.params['include'].presence
      request_span.set_tag('http.request.params.include', csv_sort(includes))
    end
  end

  def set_extra_tags(request_span, env)
    tag_with_default(request_span, 'warden.winning_strategy', env['warden']&.winning_strategy)
    doorman_time = env['HTTP_X_ZENDESK_DOORMAN_TIME']
    request_span.set_tag('zendesk.doorman.bypassed', doorman_time.blank?)
    request_span.set_tag('zendesk.doorman.time_spent', doorman_time.to_i)
    tag_with_default(request_span, 'zendesk.doorman.auth_response_code', env["HTTP_X_ZENDESK_DOORMAN_AUTH_RESPONSE"])
  end

  def set_trace_id(request_span)
    request_span.set_tag('http.trace_id', request_span.trace_id)
  end

  def set_prop_tags(request_span, env)
    account = env['zendesk.account']

    if account
      count_limit = account.api_rate_limit.to_i
      time_limit  = account.api_time_rate_limit.to_i

      if count_limit > 0
        prop_api_requests           = Prop.count(:api_requests, account.id)
        count_limit_used            = [prop_api_requests, count_limit].min
        count_limit_used_percentage = count_limit_used * 100 / count_limit
        count_limit_remaining       = count_limit - count_limit_used

        request_span.set_tag('http.rate_limit.api_requests.limit', count_limit)
        request_span.set_tag('http.rate_limit.api_requests.remaining', count_limit_remaining)
        request_span.set_tag('http.rate_limit.api_requests.used', count_limit_used)
        request_span.set_tag('http.rate_limit.api_requests.used_percentage', count_limit_used_percentage)
      end

      if time_limit > 0
        prop_api_time              = Prop.count(:api_time, account.id)
        time_limit_used            = [prop_api_time, time_limit].min
        time_limit_used_percentage = time_limit_used * 100 / time_limit
        time_limit_remaining       = time_limit - time_limit_used

        request_span.set_tag('http.rate_limit.api_time.limit', time_limit)
        request_span.set_tag('http.rate_limit.api_time.remaining', time_limit_remaining)
        request_span.set_tag('http.rate_limit.api_time.used', time_limit_used)
        request_span.set_tag('http.rate_limit.api_time.used_percentage', time_limit_used_percentage)
      end
    end
  end

  # rubocop:enable Naming/AccessorMethodName
  # This is a saftey method to handle defaulting to N/A when the value is nil
  def tag_with_default(request_span, tag, value)
    # Don't reset a tag someone else has already set
    return unless request_span.get_tag(tag).nil?

    request_span.set_tag(tag, value || NOT_AVAILABLE)
  end

  def csv_sort(csv)
    csv&.split(',')&.sort&.join(',')
  end

  # We can safely memoize this because the git revision won't ever change for a running app,
  # it will only change when the app restarts which will reinstantiate this middleware.
  def github_tree_url
    @github_tree_url ||= "https://github.com/zendesk/zendesk/tree/#{GIT_HEAD_SHA}"
  end

  def premier_support_feature_addon(account)
    account.subscription_feature_addons.where(name: 'premier_support').any?
  end
end
