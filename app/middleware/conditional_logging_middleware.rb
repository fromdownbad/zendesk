### Description
#
# Conditional logging is a tool that is targeted at issues that are not able to be reproduced on demand.
# Bugs like this can often take days or weeks to resurface and debug logging for that entire time would
# be a significant performance problem. The goal of conditional logging is to be able to only output debug
# logging for requests which are highly likely to be related to the bug. This will make it possible to
# get enough debugging information from the production environment to help identify the exact circumstances
# needed for reproduction
#
### Filters
#
# Logs can be filtered both before and after the request completes. There is a set list
# of supported attributes, each of which can have a custom regex assigned to determine a match.
# All filters that are defined on an account must match in order for the logging to be activated.
#
# Attributes that can be checked before the request:
#
# - The request QUERRY_STRING, HTTP_USER_AGENT, PATH_INFO, and REQUEST_METHOD
# - The authenticated user's ID (or 'null' if the request is anonymous)
#
# Attributes that can be checked after the request:
#
# - Response status
#
# Additional filters may be added in the future as a need becomes apparent.
#
#### Usage
#
# A new account method `set_conditional_logging` is provided which simplifies the process of setting the
# filters for the account. Filters are per-account and stored in the setting `conditional_logging_pattern`
# which has a maximum total length of 254 characters. The `set_conditional_logging` method will reset all
# account filters to nil if called without parameters. Detailed examples using the method can be found in
# the tests for conditional_logging_middleware and account.
#
# An example filter would be something like this:
#
# account.set_conditional_logging(request_method: /PUT/, path_info: /api\/v2\//, http_user_agent: /Zendesk Target/, user_id: /123456/, status: /[4-5][0-9][0-9]/)
#
#### Log output
#
# Logs come out in JSON format and are saved to `logs/conditional.log`. Each logging event contains the
# request ID and account ID associated with that request.
#
####
class ConditionalLoggingMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    @account = env["zendesk.account"]
    @user    = env["zendesk.user"]
    return @app.call(env) unless @account
    return @app.call(env) unless @account.settings.conditional_logging_pattern
    return @app.call(env) unless should_log(env)
    result = nil
    Rails.logger.info("Initiating conditional_logging per #{should_log_conditions}")
    logs = capture_logging do
      result = begin
        @app.call(env)
      rescue StandardError => e
        e
      end
    end
    output_log(env, logs, result)
    raise result if result.is_a?(Exception)
    result
  end

  private

  def should_log(env)
    (filters.any? &&
      nil_or_match(filters[0], env["REQUEST_METHOD"]) &&
      nil_or_match(filters[1], env["PATH_INFO"]) &&
      nil_or_match(filters[2], env["HTTP_USER_AGENT"]) &&
      nil_or_match(filters[3], env["QUERY_STRING"]) &&
      nil_or_match(filters[4], user_id))
  end

  def should_log_conditions
    [filters[0] && "request_method: #{filters[0].inspect}",
     filters[1] && "path_info: #{filters[1].inspect}",
     filters[2] && "user_agent: #{filters[2].inspect}",
     filters[3] && "query_string: #{filters[3].inspect}",
     filters[4] && "user_id: #{filters[4].inspect}"].compact.join(", ")
  end

  def output_log(env, logs, result)
    status, _headers, response = result
    responses = []
    response.each { |s| responses << s }
    return unless result.is_a?(Exception) ||
      (nil_or_match(filters[5], status.to_s) && responses.any? { |body| nil_or_match(filters[6], body) })
    instance = Zendesk::Logging::Logger.new(CONDITIONAL_LOG).instance()
    instance.append_attributes(
      account_id: @account.id,
      prefix: "classic_debug",
      host: Socket.gethostname,
      pid: $$,
      request_id: env["request_id"],
      method: env["REQUEST_METHOD"],
      user_agent: env["HTTP_USER_AGENT"],
      url: env["REQUEST_URI"],
      user_id: user_id,
      status: status
    )
    instance.info(logs)
    instance.info(env.fetch('action_dispatch.request.request_parameters', "conditional_logging_error": "no action_dispatch.request.request_parameters key in env"))
    instance.info(responses)
  end

  def filters
    @filters ||= JSON.parse(@account.settings.conditional_logging_pattern).map { |r| r && Regexp.new(r) }
  rescue JSON::ParserError, TypeError
    @filters = []
  end

  def user_id
    return "null" unless @user
    @user.id.to_s
  end

  def nil_or_match(filter, string)
    filter.nil? || filter =~ string
  end

  def capture_logging(&block)
    logger = Rails.logger
    recorder = StringIO.new

    capturing_logger = Logger.new(recorder)
    capturing_logger.formatter = proc { |_severity, _datetime, _progname, msg|
      "%.4f %s\n" % [Time.now.to_f, msg]
    }

    debug_level(logger) { logger.subscribe(capturing_logger, &block) }
    recorder.string
  end

  def debug_level(logger)
    old = logger.level
    logger.level = Logger::DEBUG
    yield
  ensure
    logger.level = old
  end
end
