class HttpMethodNotAllowedMiddleware
  #
  # All these should be already filtered at nginx level
  # This is a redundant check to protect from potential internal attack
  #
  RFC2616 = %w[OPTIONS GET HEAD POST PUT DELETE TRACE CONNECT].freeze
  RFC5789 = %w[PATCH].freeze
  ALLOWED_HTTP_METHODS = RFC2616 + RFC5789

  def initialize(app)
    @app = app
  end

  # Gracefully return 405 if we get a request with an unsupported HTTP method
  def call(env)
    if !ALLOWED_HTTP_METHODS.include?(env["REQUEST_METHOD"])
      Rails.logger.info "ActionController::UnknownHttpMethod: #{env['REQUEST_METHOD']}"
      [405, {"Content-Type" => "text/plain"}, ["Method Not Allowed"]]
    else
      @app.call(env)
    end
  end
end
