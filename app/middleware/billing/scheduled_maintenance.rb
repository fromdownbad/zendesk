module Billing
  class ScheduledMaintenance
    def initialize(app)
      @app = app
    end

    def call(env)
      @request = Rack::Request.new(env)
      serviceable? ? app.call(env) : refuse_service
    end

    private

    attr_reader :app, :request

    # These request-methods are blacklisted when Billing is in maintenance mode
    # and can be allowed by creating and enabling a request-method specific
    # Arturo bit: blling_maintenance_allow_<request-method>
    BLACKLISTED_REQUEST_METHODS = %i[post put patch delete].freeze

    # The following are the list of requests that are blacklisted when Billing
    # is in maintenance. We may selectively let them through by creating and
    # enabling a billing_maintenance_allow_<request-name> Arturo bit.
    #
    # So the following Arturo bit enables purchases even if Billing is in
    # maintenance:
    #
    #   billing_maintenance_allow_purchases
    #
    BLACKLISTED_ENDPOINTS = {
      purchases:                    [:post,   '/api/v2/account/subscription'],
      amendments:                   [:put,    '/api/v2/account/subscription'],
      payment_method_updates:       [:post,   '/api/v2/account/subscription/update_payment_method'],
      currency_updates:             [:post,   '/api/v2/account/subscription/update_currency'],
      add_agents:                   [:post,   '/api/v2/account/subscription/add_agents'],
      cancellations:                [:delete, '/api/v2/account/subscription'],
      account_address_updates:      [:put,    '/settings/account/update_address'],
      account_invoice_updates:      [:post,   '/settings/account/update_invoices'],
      internal_api_amendments:      [:put,    '/api/v2/internal/billing/account'],
      internal_api_cancellations:   [:delete, '/api/v2/internal/billing/subscription'],
      internal_api_update_addons:   [:put,    '/api/v2/internal/billing/subscription/addons'],
      internal_api_update_features: [:put,    '/api/v2/internal/billing/subscription/features'],
      monitor_cancellations:        [:post,   '/api/internal/monitor/destroy_account']
    }.freeze

    SCHEDULED_MAINTENANCE = {
      message: 'Scheduled Maintenance'
    }.freeze

    def serviceable?
      maintenance_complete? || request_method_allowed? || request_permitted?
    end

    def refuse_service
      [503, response_header, [response_body]]
    end

    def response_header
      { 'content-type' => request.content_type }
    end

    def response_body
      json? ? SCHEDULED_MAINTENANCE.to_json : SCHEDULED_MAINTENANCE[:message]
    end

    def request_permitted?
      action = BLACKLISTED_ENDPOINTS.key([request_method, request_path])
      return true if action.blank? # ie: action is not blacklisted
      allowed_in_maintenance?(action)
    end

    def request_method_allowed?
      return true unless request_method.in?(BLACKLISTED_REQUEST_METHODS)
      allowed_in_maintenance?(request_method)
    end

    def request_method
      request.request_method.downcase.to_sym
    end

    def request_path
      request.path.sub(/\..*$/, '')
    end

    def json?
      request.content_type.to_s.casecmp('application/json').zero?
    end

    def maintenance_complete?
      !arturo_enabled?(:billing_maintenance)
    end

    def allowed_in_maintenance?(action)
      arturo_enabled?(:"billing_maintenance_allow_#{action}")
    end

    def arturo_enabled?(name)
      feature = Arturo::Feature.find_feature(name)
      feature.present? && !feature.off?
    end
  end
end
