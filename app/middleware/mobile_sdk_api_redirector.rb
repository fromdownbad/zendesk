require 'rack/request'

class MobileSdkApiRedirector
  BODY = "If you're seeing this, you do not have X-Accel-Redirect set up properly in your nginx.conf".freeze
  PATHS = {
    # Classic paths                       # Mobile SDK API paths
    '/api/mobile/sdk/settings'         => '/mobile_sdk_api/v1/settings',
    '/api/private/mobile_sdk/settings' => '/mobile_sdk_api/v2/settings'
  }.freeze

  def initialize(app)
    @app = app
  end

  def call(env)
    account = env['zendesk.account']

    if account && mobile_sdk_api_request?(env['PATH_INFO']) && redirector_enabled_for?(account)
      env['zendesk.session.persistence'] = false
      x_accel_redirect
    else
      @app.call(env)
    end
  end

  private

  def mobile_sdk_api_request?(path)
    PATHS.each do |old_prefix, new_prefix|
      if path.start_with?(old_prefix)
        @new_path = path.sub(old_prefix, new_prefix)
        return true
      end
    end
    false
  end

  def redirector_enabled_for?(account)
    Rails.cache.fetch("mobile_sdk_redirector/#{account.id}", expires_in: 10.minutes) do
      account.has_mobile_sdk_api_redirector?
    end
  end

  def x_accel_redirect
    [200, { 'X-Accel-Redirect' => @new_path, 'Cache-Control' => 'max-age=600, public' }, [BODY]]
  end
end
