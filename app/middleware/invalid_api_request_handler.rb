require 'rexml/parseexception'
class InvalidApiRequestHandler
  def initialize(app)
    @app = app
  end

  # TODO: RAILS5UPGRADE remove this and fold into call below
  PARSE_ERROR_CLASS = if !RAILS51
    ActionDispatch::ParamsParser::ParseError
  else
    ActionDispatch::Http::Parameters::ParseError
  end

  def call(env)
    # If no content type is set, the default content type Rails sets is urlencoded which
    # leads to cryptic errors because we're then unable parse params and they are discarded.
    # So lets set content type to json if it's not specified.
    if env["CONTENT_TYPE"] == 'application/x-www-form-urlencoded' &&
        ::Arturo.feature_enabled_for?(:default_json_content_type, env["zendesk.account"])
      env["CONTENT_TYPE"] = 'application/json'
    end

    @app.call(env)
  rescue Yajl::ParseError, MultiJson::LoadError, JSON::ParserError
    invalid_payload(:json)
  rescue Nokogiri::XML::SyntaxError, REXML::ParseException
    invalid_payload(:xml)
  rescue ZendeskApi::ApiAccessDenied
    deny_api_access
  rescue PARSE_ERROR_CLASS
    http_accept = env['HTTP_ACCEPT']
    if http_accept && http_accept =~ %r{(?:application|text)/xml}
      invalid_payload(:xml)
    else
      invalid_payload(:json)
    end
  end

  def invalid_payload(content_type)
    Rails.logger.info("Invalid API request: parsing error for content_type: #{content_type}")
    body = { error: 'Unprocessable Entity', message: "Server could not parse #{content_type.to_s.upcase}" }.send("to_#{content_type}")
    headers = { 'Content-Type' => "application/#{content_type}" }

    [422, headers, [body]]
  end

  def deny_api_access
    body =  { error: 'APIAccessDenied', message: "The current plan for this account does not support API usage." }.to_json
    headers = { 'Content-Type' => "application/json" }

    [403, headers, [body]]
  end
end
