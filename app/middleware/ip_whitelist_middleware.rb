class IpWhitelistMiddleware < Zendesk::Net::IpWhitelistMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    account = env['zendesk.account']

    path = env["PATH_INFO"]

    return @app.call(env) if account.nil?
    return @app.call(env) if zendesk_override_request?(path)
    return @app.call(env) if ip_override_request?(env, path)
    return @app.call(env) if requesting_voice_assets?(env['X-Twilio-Signature'], env['REQUEST_URI'], env["REQUEST_METHOD"])

    super
  end

  protected

  def zendesk_override_request?(path)
    return true if path =~ %r{\A/api/v[0-9]/any_channel}
    return true if path =~ %r{\A/voice/calls}
    return true if path =~ %r{\A/media/voice}
    return true if path =~ %r{\A/csv_exports}
    return true if path =~ %r{\A/sharing/agreements}
    return true if path =~ %r{\A/sharing/tickets}
    return true if path =~ %r{\A/oauth/authorizations}
    return true if path =~ %r{\A/oauth/tokens}
    return true if path =~ %r{\A/access/oauth_mobile_login}
    return true if path =~ %r{\A/\.well-known/acme-challenge/[A-Za-z0-9_\-]+\z}
    false
  end

  def ip_override_request?(env, _path)
    ip = Zendesk::Net::IPTools.trusted_ip(env)

    company = nil

    company ||= 'gooddata' if GOODDATA_IPS.include?(ip)
    company ||= 'bime' if Rails.configuration.bime_ips.include?(ip)
    company ||= 'connect' if Rails.configuration.connect_ips.include?(ip)
    company ||= "internal" if Zendesk::Net::IPTools.internal_ip_whitelist_bypass?(ip)

    if company.present?
      env["zendesk.#{company}.trusted_request"] = true
      return true
    end

    false
  end

  def requesting_voice_assets?(twilio_signature, uri, method)
    # http://www.rubular.com/r/2mZojdW055
    !!(twilio_signature.present? &&
       (uri =~ %r{\A/cassets/system/voice/uploads/}i ||
        uri =~ %r{\A/system/voice/uploads/}i) &&
       method.casecmp("GET").zero?
      )
  end
end
