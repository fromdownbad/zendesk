class RuleRoutingMiddleware
  excluded_paths   = ['count', 'tickets', 'analysis', 'rules/views', 'rules/macros', 'rules/automations', 'rules/triggers']
  NON_RULE_PATTERN = Regexp.new(excluded_paths.join('|'))
  RULE_PATTERN     = /^\/rules/

  def initialize(app)
    @app = app
  end

  def call(env)
    if rule_path?(env)
      log("detected rule path")
      url = Zendesk::Rules::Url.new(env)

      log("detected type: #{url.rule_type}")

      if url.supported_rule_type?
        log("routing to #{url.new_path}")
        reroute_to(url, env)
      else
        if /^\/rules$/.match?(env['PATH_INFO'])
          log("using default routing for plain rules endpoint.")
        else
          log("unsupported endpoint, failing with 404.")
          return [404, {'Content-Type' => 'text/plain'}, ['Requested object not found']]
        end
      end
    end

    @app.call(env)
  end

  def rule_path?(env)
    path = env['PATH_INFO']

    rule_match = (path =~ RULE_PATTERN)

    rule_match.present? && (path !~ NON_RULE_PATTERN)
  end

  def log(message)
    Rails.logger.info("RuleRouting: #{message}") unless Rails.env.production?
  end

  def reroute_to(url, env)
    env['PATH_INFO'] = url.new_path.sub(/\?.*/, "")
  end
end
