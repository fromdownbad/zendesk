# frozen_string_literal: true

# 1st Engineering's Principle: A single Account should not be able to take down a pod
# This solution will allow to short circuiting request on a single account via Arturo.
# (faster solution than a DNS black-hole)
class AccountRateLimitMiddleware
  MAX_POST_BODY_SIZE = 1.megabyte
  TOO_LARGE_RETRY_AFTER = 1.day

  def initialize(app)
    @app = app
  end

  def call(env)
    account = env["zendesk.account"]

    return @app.call(env) unless account

    if ::Arturo.feature_enabled_for?(:classic_short_circuit_limiter, account)
      headers = { 'Cache-Control' => 'public, max-age=120' } # Cache this for 2 min
      strip_session_response_cookie(env)
      return [429, headers, ["Too many requests.\nPlease retry later\n"]]
    end

    if block_large_post_request?(env, account)
      headers = {
        'Retry-After' => TOO_LARGE_RETRY_AFTER,
        'Max-Content-Length' => MAX_POST_BODY_SIZE
      }

      return [413, headers, [JSON.dump(error: "PayloadTooLarge", description: "Payload too large, current max is 1MB.")]]
    end

    @app.call(env)
  end

  protected

  def strip_session_response_cookie(env)
    Rails.logger.warn "Disabling session Set-Cookie response headers via AccountRateLimit middleware"
    env['rack.session.options'][:skip] = true
  end

  def block_large_post_request?(env, account)
    ::Arturo.feature_enabled_for?(:block_large_post_requests, account) \
      && env[Rack::REQUEST_METHOD] == 'POST' \
      && env['CONTENT_TYPE'] == 'application/json' \
      && env['rack.input'].size > MAX_POST_BODY_SIZE
  end
end
