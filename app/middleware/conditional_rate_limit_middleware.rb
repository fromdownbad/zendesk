###
# Conditional rate limiting is designed as a temporary tool to help keep a specific
# accounts traffic pattern from harming the rest of our customers. These rate limits
# are not intended to be a permanent limitation put on an account.
#
# Limits can also be applied at the user level. If the limit definition uses the property
# `user_id`, then the rate limit will be unique per user, otherwise all limits will
# apply to the entire account.
#
### Limits
# Limits can be defined base on four different parts of the request. Each of these
# attributes will be compared to the given pattern based on a predetermined
# operator specific to the part of the request.
#
# Attributes of a request that can be used in a rate limit:
#
#  - request_method: this will be compared using equality, so it must be an exact match
#  - endpoint:       this will be compared by checking for inclusion of the pattern (excepting pattern `/`)
#  - user_agent:     this will be compared as a regex, meaning it can be a flexible or fuzzy match
#  - user_id:        this will be compared using equality, so it must be an exact match
#  - query_string:   this will be compared as a regex, meaning it can be a flexible or fuzzy match
#
### Usage
# There are three methods on an account that should be used to control these conditional
# rate limits.
#
# - `add_conditional_rate_limit!`
#     This will create a new rate limit with the given properties and save the account.
#
#     required args:
#       - key:      - the key that Prop will use to identify the rate limit. Must be unique.
#       - limit:    - the number of requests allowed before triggering the limit.
#       - interval: - the number of seconds to allow `limit:` seconds in before triggering the limit.
#
#     optional args:
#       - strict_match - checks whether endpoint match will use == or a regex (which means a substring will suffice)
#
#     partial args (at least one must not be nil):
#       - request_method: - GET, POST, PUT, etc...
#       - endpoint:       - a substring or exact match of the URL for a request (SEE optional args: strict_match)
#       - user_agent:     - a pattern to match the user agent against
#       - query_string:   - a pattern to match the query string against
#       - user_id:        - the user id of the authenticated user to match
#       - ip_address:     - a pattern (CIDR supported) to match the ip address, checks `HTTP_X_FORWARDED_FOR` or `REMOTE_ADDR`
#       - enforce_lotus:  - should the limit be applied to requests from lotus? default: false
#
#
# - `remove_conditional_rate_limit!`
#     This will remove a single rate limit based on its key and save the account.
#
#     args:
#       - key - the rate limit to remove, it should be the same as a `key:` argument from `add_conditional_rate_limit`
#
#
# - `clear_conditional_rate_limits!`
#      This will remove all rate limits and save the account.
#
#      no args
#
### Example Usage:
# This will allow an account to list users, but only create a new user once per day.
#
# account.add_conditional_rate_limit!(
#   key: 'incremental_export_limit_2017-01-01',
#   limit: 1,
#   interval: 86400,
#   request_method: 'POST',
#   endpoint: 'users/create'
# )
#
###
class ConditionalRateLimitMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    @account = env["zendesk.account"]
    @user    = env["zendesk.user"]

    return @app.call(env) unless @account

    matching_limits(env).each do |limit|
      prop_key     = limit['prop_key'].to_sym
      threshold    = limit['limit'].to_i
      interval     = limit['interval_seconds'].to_i
      desc         = "Temporary rate limit for account: #{@account.subdomain}"
      throttle_key = build_throttle_key(limit)

      if config = Prop.configurations[prop_key]
        log "Prop key '#{prop_key}' is already configured: #{config.inspect}"
      else
        log "Configuring Prop key '#{prop_key}' -- threshold: #{threshold}, interval: #{interval}"
        Prop.configure(prop_key, threshold: threshold, interval: interval.seconds, description: desc)
      end

      current_count = Prop.count(prop_key, throttle_key)
      log "Prop key '#{prop_key}' limit matched request -- current count in last #{interval} seconds: #{current_count}"

      log "Throttling request with throttle key: '#{throttle_key}'"
      statsd_client.increment("ratelimited", tags: ["key:#{throttle_key}", "account_id:#{@account.id}"])
      Prop.throttle!(prop_key, throttle_key)
    end

    @app.call(env)
  end

  private

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'conditional_rate_limit')
  end

  def matching_limits(env)
    Array(@account.texts.conditional_rate_limits).select do |limit|
      limit_matches_request?(limit, env)
    end
  end

  def limit_matches_request?(limit, env)
    all_match = limit['properties'].all? do |property, pattern|
      pattern.nil? || matches_property?(property, pattern, env, limit['strict_match'])
    end

    all_match && enforce_for_lotus?(limit, env)
  end

  def matches_property?(property, pattern, env, strict_match)
    case property.to_sym
    when :request_method
      env['REQUEST_METHOD'].casecmp(pattern).zero?
    when :endpoint
      if strict_match
        env['PATH_INFO'] == pattern
      else
        env['PATH_INFO'].include? pattern
      end
    when :user_agent
      env['HTTP_USER_AGENT'] =~ Regexp.new(pattern, Regexp::IGNORECASE)
    when :user_id
      user_id == pattern.to_i
    when :query_string
      env['QUERY_STRING'].to_s =~ Regexp.new(pattern, Regexp::IGNORECASE)
    when :ip_address
      IPAddr.new(pattern).include?(env['HTTP_X_FORWARDED_FOR'] || env['REMOTE_ADDR'])
    else
      true # default true case so we dont ruin the `all?` check above for unknown properties
    end
  end

  def enforce_for_lotus?(limit, env)
    env['HTTP_X_ZENDESK_LOTUS_VERSION'].blank? || limit['properties']['enforce_lotus']
  end

  def user_id
    @user && @user.id
  end

  def build_throttle_key(limit)
    if limit['properties']['user_id']
      "#{@account.id}-#{user_id}"
    else
      @account.id
    end
  end

  def log(msg)
    Rails.logger.info "[ConditionalRateLimitMiddleware] #{msg}"
  end
end
