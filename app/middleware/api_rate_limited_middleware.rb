require "prop/middleware"

# TODO: there can be only a single prop-middleware in the stack, so inheriting is wrong
# should use the `handler` option of Prop::Middleware
class ApiRateLimitedMiddleware < Prop::Middleware
  def call(env)
    status, headers, body = super

    if status == 429
      env['zendesk.session.persistence'] = false
    end
    [status, headers, body]
  end

  protected

  # called when request was rate limited
  def render_response(env, error)
    account = env["zendesk.account"]

    Rails.logger.warn("API Rate Limited in middleware for #{account.subdomain}/#{account.id}: #{error.message}")

    status, headers, body = super

    # TODO: ZendeskApi::Limiter should also set X-Rate-Limit on failed requests instead of this hack
    # NOTE:  This is not setup by default when the Prop gem throttles a response
    api_rate_limit = env['HTTP_X_RATE_LIMIT']
    if api_rate_limit

      # rubocop:disable Performance/RedundantMerge
      headers.merge!(
        'X-Rate-Limit' => api_rate_limit,
        'X-Rate-Limit-Remaining' => 0
      )
      # rubocop:enable Performance/RedundantMerge
    end

    # when the account rate limit was triggered TODO: check error handle instead ?
    if api_rate_limit
      # Cache-control header here is a solution to prevent an abuse from integrations
      # such segment.io, we receive 4M requests per day per one integration account,
      # most of them resulted in HTTP 429 errors because an account's API limit
      # is being exhausted very quick, but segement.io http client does not respect
      # Rate Limiting headers, unfortunately generation of HTTP 429 is not cheap
      # and may take up to 50ms. Imagine 50 accounts configured with segment.io per
      # pod and we are run out of unicorns Cache-control:max-age=5 header instructs
      # the segment client to wait 5 seconds after first HTTP 429 error.

      headers['Cache-Control'] = "max-age=#{error.retry_after},public"

      # When serving a HTTP 429 we also update the session, try to avoid this overhead by disabling it
      # which will be checked in zendesk_shared_session/lib/zendesk_shared_session/middleware/session_persistence.rb
      env['zendesk.session.persistence'] = false
    end

    [status, headers, body]
  end
end
