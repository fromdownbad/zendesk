require 'zendesk_protobuf_clients/zendesk/protobuf/exodus/event_pb'
require 'racecar'

class UserAccountMoveConsumer < Racecar::Consumer
  subscribes_to "exodus.state-events", start_from_beginning: false

  self.group_id = "classic.user_account_move_consumer".freeze

  def process(message)
    event = Zendesk::Protobuf::Exodus::Event.decode(message.value)
    return unless account_finished_moving?(event)

    publish_user_tombstones_for_account!(
      account_id: event.header.account_id.value,
      previous_pod_id: event.account_move.source_pod_id.value
    )

    publish_users_for_account(
      account_id: event.header.account_id.value,
      new_pod_id: event.account_move.target_pod_id.value
    )
  rescue StandardError => exception
    Rails.logger.error "UserAccountMoveConsumer exception: '#{exception.class}' -- '#{exception.message}'"
  end

  private

  def account_finished_moving?(event)
    event.state_changed.current_state == :DONE
  end

  def publish_user_tombstones_for_account!(account_id:, previous_pod_id:)
    return if Zendesk::Configuration.fetch(:pod_id) != previous_pod_id

    publisher = UserEntityPublisher.new(account_id: account_id)
    User.unscoped.select(:id).where(account_id: account_id).find_each(batch_size: 500) do |user|
      publisher.publish_tombstone(user_id: user.id)
    end
    Rails.application.config.statsd.client.increment("user_account_move_consumer.published.tombstones", tags: ["account_id:#{account_id}"])
  end

  def publish_users_for_account(account_id:, new_pod_id:)
    return if Zendesk::Configuration.fetch(:pod_id) != new_pod_id

    UserEntityPublisher.new(account_id: account_id).publish_for_account
    Rails.application.config.statsd.client.increment("user_account_move_consumer.published.users", tags: ["account_id:#{account_id}"])
  end
end
