require 'racecar'

# Consumes Account Audits messages from the Kafka Topic - audit_events
class AccountAuditsConsumer < Racecar::Consumer
  subscribes_to 'account_audit_events_v1'

  def process(message)
    account(message).on_shard do
      CiaEventCreator.create!(
        message&.value,
        decoder: CiaEventCreator::ProtoDecoder
      )
    end
  rescue StandardError => e
    ZendeskExceptions::Logger.record(
      e,
      location: self,
      message: e.message,
      fingerprint: 'apdlkasdioryfdkpuy2364389127svdfbss262aa'
    )
  end

  private

  def account(message)
    account_id = message.headers['ZENDESK_ACCOUNT_ID']
    raise ArgumentError, "account_id not provided in the Kafka Header" unless account_id

    Account.find(account_id)
  end
end
