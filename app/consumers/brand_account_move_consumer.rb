require 'zendesk_protobuf_clients/zendesk/protobuf/exodus/event_pb'
require 'racecar'

# Responsible for removing stale entities from compacted topics after an account move
class BrandAccountMoveConsumer < Racecar::Consumer
  subscribes_to "exodus.state-events", start_from_beginning: false

  self.group_id = "classic.brand_account_move_consumer".freeze

  def process(message)
    event = Zendesk::Protobuf::Exodus::Event.decode(message.value)
    return unless account_finished_moving?(event)

    publish_brand_tombstones_for_account!(
      account_id: event.header.account_id.value,
      previous_pod_id: event.account_move.source_pod_id.value
    )

    publish_brands_for_account(
      account_id: event.header.account_id.value,
      new_pod_id: event.account_move.target_pod_id.value
    )
  rescue StandardError => exception
    Rails.logger.error "BrandAccountMoveConsumer exception: '#{exception.class}' -- '#{exception.message}'"
  end

  private

  def account_finished_moving?(event)
    event.state_changed.current_state == :DONE
  end

  def publish_brand_tombstones_for_account!(account_id:, previous_pod_id:)
    return if Zendesk::Configuration.fetch(:pod_id) != previous_pod_id

    publisher = BrandPublisher.new(account_id: account_id)
    Brand.unscoped.where(account_id: account_id).pluck(:id).each do |brand_id|
      publisher.publish_tombstone(brand_id: brand_id)
    end
    Rails.application.config.statsd.client.increment("brand_account_move_consumer.published.tombstones", tags: ["account_id:#{account_id}"])
  end

  def publish_brands_for_account(account_id:, new_pod_id:)
    return if Zendesk::Configuration.fetch(:pod_id) != new_pod_id

    BrandPublisher.new(account_id: account_id).publish_for_account
    Rails.application.config.statsd.client.increment("brand_account_move_consumer.published.brands", tags: ["account_id:#{account_id}"])
  end
end
