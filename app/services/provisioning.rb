class Provisioning
  require_dependency 'provisioning/account_state'
  require_dependency 'provisioning/after_actions'
  require_dependency 'provisioning/before_actions'
  require_dependency 'provisioning/chat_product'
  require_dependency 'provisioning/talk_product'
  require_dependency 'provisioning/talk_partner_product'
  require_dependency 'provisioning/addons_state'
  require_dependency 'provisioning/features_state'
  require_dependency 'provisioning/multi_brand_state'
  require_dependency 'provisioning/payload_helper'
  require_dependency 'provisioning/payload'
  require_dependency 'provisioning/support_product'

  include PayloadHelper

  private_class_method :new

  def self.call(master_account_id, payload:, dry_run: true)
    new(master_account_id, payload, dry_run).call
  end

  def call
    account.on_shard do
      inflate? ? inflate : provision
    end
    journal
  end

  private

  attr_reader :master_account_id,
    :payload,
    :dry_run

  def initialize(master_account_id, payload, dry_run)
    @master_account_id = master_account_id
    @payload           = payload.deep_symbolize_keys
    @dry_run           = dry_run
  end

  def billing_id
    payload.dig(:account, :billing_id)
  end

  def journal
    metadata = { master_account_id: master_account_id, billing_id: billing_id }
    @journal ||= ZendeskBillingCore::Journal.new(metadata: metadata)
  end

  # REF: ZBC::Zuora::Synchronizer#inflate_support?
  def inflate?
    support_subscription.blank? &&
      _current_subscription.active? &&
      _support_product.present?
  end

  def provisioning_options
    {
      account: account,
      dry_run: dry_run,
      payload: payload,
      journal: journal
    }
  end

  def inflate
    SupportProduct::Inflator.call(provisioning_options)
  end

  SERVICES = [
    SupportProduct,
    ChatProduct,
    TalkProduct,
    TalkPartnerProduct,
    MultiBrandState,
    AddonsState,
    FeaturesState,
    AccountState
  ].freeze

  def provision
    BeforeActions.call(provisioning_options)
    SERVICES.each do |service|
      service.call(provisioning_options)
    end
    AfterActions.call(provisioning_options)
  end

  delegate :subscription,
    to:     :account,
    prefix: :support

  def account
    @account ||= begin
      find_account = lambda { Account.find(master_account_id) }
      if defined?(Kasket)
        ActiveRecord::Base.without_kasket(&find_account)
      else
        find_account.call
      end
    end
  end
end
