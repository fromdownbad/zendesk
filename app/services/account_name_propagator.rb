class AccountNamePropagator
  def self.call(account, name: nil, name_was: nil)
    new(account, name, name_was).call
  end

  def call
    if name_changed?
      update_organizations_names
      update_recipient_addresses_names
      update_address_name
    end
  end

  private

  attr_reader :account, :name, :name_was
  delegate \
    :address,
    :organizations,
    :recipient_addresses,
    to: :account

  def initialize(account, name, name_was)
    @account = account
    @name = name || account.name
    @name_was = name_was || account.name_was
  end

  def name_changed?
    # NB. using this instead of `account.name_changed?` so we can trigger the `before_update` callbacks to `Account` when
    # Account#name has been changed from outside of rails.
    # (eg. when it gets changed by the AccountService, we have to tell rails to call its callbacks with the previous value for `name`)
    # TODO: Eventually ... get AccountService & Classic to stop writing to the same `accounts` table.
    @name != @name_was
  end

  def update_address_name
    address.update_attribute(:name, name) if address && address.name == name_was
  end

  def update_recipient_addresses_names
    recipient_addresses.where(name: name_was).each do |address|
      address.update_attribute(:name, name)
    end
  end

  def update_organizations_names
    organizations.where(name: name_was).each do |org|
      org.update(name: name.delete("|").squeeze(" "))
    end
  end
end
