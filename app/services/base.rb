class Base
  require_dependency 'provisioning/payload_helper'

  include Provisioning::PayloadHelper

  def self.call(options)
    new(options).call
  end

  def call
    fail NotImplementedError
  end

  private

  delegate :account,
    :dry_run,
    :payload,
    :journal,
    to: :options

  alias_method :dry_run?, :dry_run

  attr_reader :options

  def initialize(options)
    @options = OpenStruct.new(options)
  end
end
