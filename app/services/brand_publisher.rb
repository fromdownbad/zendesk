class BrandPublisher
  TOPIC = "guide.brand_entities".freeze

  def initialize(account_id:)
    @account_id = account_id
  end

  def publish_for_account
    account = Account.with_deleted { Account.find(account_id) }
    return if account.deleted?

    Brand.on_master.without_kasket do
      account.brands.each do |brand|
        publish(brand: brand)
      end
    end
  end

  def publish_for_route(route_id:)
    brand = Brand.on_master.without_kasket do
      Brand.find_by(route_id: route_id)
    end

    publish(brand: brand)
  end

  def publish_for_brand(brand_id:)
    brand = Brand.on_master.without_kasket do
      Brand.where(account_id: account_id).find_by(id: brand_id)
    end

    publish(brand: brand)
  end

  def publish_tombstone(brand_id:)
    write_message(
      brand_id: brand_id,
      value: nil
    )
  end

  private

  attr_reader :account_id

  def publish(brand:)
    return unless brand
    write_message(
      brand_id: brand.id,
      value: BrandProtobufEncoder.new(brand).to_proto
    )
  end

  def write_message(brand_id:, value:)
    EscKafkaMessage.create!(
      account_id: account_id,
      topic: TOPIC,
      value: value,
      key: "#{account_id}:#{brand_id}"
    )
  end
end
