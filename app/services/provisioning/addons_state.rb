class Provisioning
  class AddonsState < Base
    require_dependency 'provisioning/addons_state/synchronizer'

    def call
      return unless synchronize_addons?

      synchronize_addons
    end

    private

    def synchronize_addons?
      _support_product.present? && account.reload.subscription.present?
    end

    def synchronize_addons
      Synchronizer.call(options.to_h)
    end
  end
end
