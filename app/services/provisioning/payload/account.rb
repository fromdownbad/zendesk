class Provisioning
  class Payload::Account < ::Hashie::Mash
    include Hashie::Extensions::Coercion

    coerce_key :dunning,                Payload::Dunning
    coerce_key :talk_recharge_settings, Payload::TalkRechargeSettings

    def subscriptions
      Array(self[:subscriptions]).map do |item|
        Payload::Subscription.new(item)
      end
    end

    def current_subscription
      subscription('current')
    end

    def future_subscription
      subscription('future')
    end

    def subscription(type)
      subscriptions.detect do |subscription|
        subscription.type.casecmp?(type.to_s)
      end
    end
  end
end
