class Provisioning
  class Payload::PrepaidCredit < ::Hashie::Mash
    include Hashie::Extensions::Coercion

    # REF: ZBC::Zuora::Synchronizer::VoicePrepaidCreditsSupport#already_synchronized_credit?
    def synchronized?
      voice_usage_record? || unprocessed_usage_record?
    end

    private

    def voice_usage_record?
      ZBC::Zuora::VoiceUsage.find_by(
        zuora_reference_id: reference_id
      ).present?
    end

    def unprocessed_usage_record?
      ZBC::Voice::UnprocessedUsage.find_by(
        zuora_reference_id: reference_id
      ).present?
    end
  end
end
