class Provisioning
  class Payload::Addon < ::Hashie::Mash
    include Hashie::Extensions::Coercion

    def to_addon_rateplan_info
      data = { zuora_rate_plan_id: reference_id }

      if temporary_zendesk_agents?
        data.merge!(slice(:quantity, :starts_at, :expires_at))
      end

      [name, data.symbolize_keys]
    end

    private

    def temporary_zendesk_agents?
      name.casecmp?('temporary_zendesk_agents')
    end
  end
end
