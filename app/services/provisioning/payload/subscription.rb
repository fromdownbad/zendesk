class Provisioning
  class Payload::Subscription < ::Hashie::Mash
    include Hashie::Extensions::Coercion

    def products
      Array(self[:products]).map do |item|
        Payload::Product.new(item)
      end
    end

    def addons
      Array(self[:addons]).map do |item|
        Payload::Addon.new(item)
      end
    end

    def prepaid_credits
      Array(self[:prepaid_credits]).map do |item|
        Payload::PrepaidCredit.new(item)
      end
    end

    def active?
      self[:status].casecmp?('active')
    end

    SUPPORTED_PRODUCTS = %i[
      chat
      support
      talk
      talk_partner
    ].freeze

    SUPPORTED_PRODUCTS.each do |name|
      define_method("#{name}_product") { product(name) }
    end

    def product(name)
      products.detect { |product| product.name.casecmp?(name.to_s) }
    end
  end
end
