class Provisioning
  class BeforeActions < Base
    # REF: ZBC::Zuora::Synchronizer::SuiteSupport#suite_support_before_actions
    def call
      return if _support_product.blank?

      upsert_suite_subscription_setting
      enable_chat_only_role
    end

    private

    # REF: ZBC::Zuora::Synchronizer::SuiteSupport#upsert_suite_subscription_setting
    def upsert_suite_subscription_setting
      account.settings.reload.
        where(name: 'suite_subscription').
        first_or_initialize.
        tap { |setting| setting.value = _support_product.is_suite_product }.
        save! unless dry_run?

      journal.add(:account_settings, suite_account_settings_updated: true)
    end

    # REF: ZBC::Zuora::Synchronizer::SuiteSupport#enable_chat_only_role
    def enable_chat_only_role
      return if _chat_product.blank?
      return if _support_product.is_suite_product

      if account.has_ocp_chat_only_agent_deprecation?
        PermissionSet.enable_contributor!(account) unless dry_run?
      else
        PermissionSet.create_chat_agent!(account) unless dry_run?
      end
    end
  end
end
