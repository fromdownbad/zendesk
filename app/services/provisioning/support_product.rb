class Provisioning
  class SupportProduct < Base
    require_dependency 'provisioning/support_product/inflator'
    require_dependency 'provisioning/support_product/synchronizer'

    def call
      return unless synchronize_support?

      synchronize_support
    end

    private

    delegate :subscription,
      to:     :account,
      prefix: :support

    # REF: ZBC::Zuora::Synchronizer#synchronize_support?
    def synchronize_support?
      account.zuora_subscription.present? || support_trial_needs_syncing?
    end

    def synchronize_support
      Synchronizer.call(options.to_h)
    end

    # REF: ZBC::Zuora::Synchronizer#support_trial_needs_syncing?
    def support_trial_needs_syncing?
      purchasable_trial? && support_product_in_payload?
    end

    def support_product_in_payload?
      _current_subscription.active? && _support_product.present?
    end

    # REF: ZBC::Zuora::Synchronizer#purchasable_trial?
    def purchasable_trial?
      active_support_trial? || expired_support_trial?
    end

    # REF: ZBC::Zuora::Synchronizer#active_support_trial?
    def active_support_trial?
      # Subscription#is_trial should probably be named #is_active_trial, since
      # it gets set to false on expiration. Thus, we need to check for both
      # active trials and expired trials here, since both should be synced if
      # purchased.
      !!support_subscription.try(:is_trial?)
    end

    # REF: ZBC::Zuora::Synchronizer#expired_support_trial?
    def expired_support_trial?
      # we can't use Subscription#trial_expired? here since it calls
      # #is_expired?, which assumes Account#is_serviceable? will be false, which
      # is not necessarily true for expired Support trials on CP4 accounts.
      #
      #   def is_expired?
      #     !account.is_serviceable? && past_expiration_date?
      #   end
      #
      !!support_subscription.try(:past_expiration_date?) &&
        !support_subscription.try(:zuora_managed?)
    end
  end
end
