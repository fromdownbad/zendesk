class Provisioning
  class TalkProduct < Base
    require_dependency 'provisioning/talk_product/prepaid_credits_synchronizer'
    require_dependency 'provisioning/talk_product/synchronizer'
    require_dependency 'provisioning/talk_product/recharge_settings_synchronizer'

    SERVICES = [
      PrepaidCreditsSynchronizer,
      Synchronizer,
      RechargeSettingsSynchronizer
    ].freeze

    def call
      SERVICES.each do |service|
        service.call(options.to_h)
      end
    end
  end
end
