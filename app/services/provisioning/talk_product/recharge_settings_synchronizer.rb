class Provisioning
  class TalkProduct::RechargeSettingsSynchronizer < Base
    # REF: ZBC::Zuora::Synchronizer::VoiceRechargeSettingsSupport#synchronize_voice_recharge_settings!
    def call
      return unless account.has_voice_recharge?
      return unless _talk_recharge_settings.amount.present?

      attributes.each { |key, value| talk_recharge_settings[key] = value }
      talk_recharge_settings.save! unless dry_run?

      journal.add(
        :talk,
        talk_recharge_settings_updated: true,
        talk_recharge_setting_options:  attributes
      )
    end

    private

    def talk_recharge_settings
      @talk_recharge_settings ||= account.voice_recharge_settings ||
        account.build_voice_recharge_settings(attributes)
    end

    def attributes
      _talk_recharge_settings.slice(:amount, :enabled)
    end
  end
end
