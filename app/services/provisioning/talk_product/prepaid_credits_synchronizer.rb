class Provisioning
  class TalkProduct::PrepaidCreditsSynchronizer < Base
    # REF: ZBC::Zuora::Synchronizer::VoicePrepaidCreditsSupport#synchronize_voice_prepaid_credits
    def call
      return unless account.voice_sub_account.present?

      if serialize_voice_usage?
        insert_into_unprocessed_usage_table
      else
        insert_into_usage_table
      end

      journal.add(
        :talk,
        talk_usage_processed: true,
        talk_usage_options:   usages
      )
    end

    private

    def insert_into_unprocessed_usage_table
      return if usages.empty?

      ZBC::Voice::UnprocessedUsage.create!(usages) unless dry_run?
    end

    def insert_into_usage_table
      return if usages.empty?

      ZBC::Zuora::VoiceUsage.create!(usages) unless dry_run?
    end

    delegate :prepaid_credits,
      to: :_current_subscription

    def talk_prepaid_credits
      prepaid_credits.select do |prepaid_credit|
        prepaid_credit.name.casecmp?('talk_prepaid')
      end
    end

    def usages
      @usages ||= begin
        talk_prepaid_credits.each_with_object([]) do |credit, collection|
          next if credit.synchronized?

          attributes = {
            account_id:                  account.id,
            units_from_rate_plan_charge: credit.quantity,
            zuora_reference_id:          credit.reference_id,
            usage_type:                  'credit'
          }
          collection << attributes
        end
      end
    end

    def serialize_voice_usage?
      Arturo.feature_enabled_for?(:serialize_voice_usage, account)
    end
  end
end
