class Provisioning
  class TalkProduct::Synchronizer < Base
    # REF: ZBC::Zuora::Synchronizer::VoiceSupport#synchronize_voice_subscription!
    def call
      _talk_product.present? ? update : suspend
    end

    private

    def update
      update_subscription
      ensure_talk_activation
      add_talk_usage
    end

    def update_subscription
      attributes.each { |key, value| talk_subscription[key] = value }
      talk_subscription.save! unless dry_run?
      journal.add(
        :talk,
        talk_subscription_updated: true,
        talk_subscription_options: attributes
      )
    end

    def activate?
      account.is_active? &&
        account.is_serviceable? &&
        !!talk_sub_account.try(:suspended?)
    end

    def ensure_talk_activation
      return unless activate?

      # TODO: Port ZBC::Voice::Jobs::ActivateJob back into Classic
      ZBC::Voice::Jobs::ActivateJob.enqueue(account.id) unless dry_run?
      journal.add(:talk, enqueue_activate_talk_job: true)
    end

    # NOTE: There might not be a need to port this method/step since the Arturo
    # flag :billing_check_voice_usage_subscription_during_sync is OFF in
    # production and staging.
    def add_talk_usage
      return unless check_usage_subscription?

      # TODO: if we need to keep this step we will need to port the job back
      # into Classic and update its implementation so that it requests Billing
      # to create the usage subscription in Zuora.
      ZBC::Zuora::Jobs::AddVoiceJob.enqueue(account.id) unless dry_run?
      journal.add(:talk, enqueue_talk_job: true)
    end

    def suspend
      return unless talk_subscription.persisted?

      talk_subscription.suspend! unless dry_run?
      journal.add(:talk, talk_suspended: true)
    end

    # REF: ZBC::Zuora::Synchronizer::VoiceSupport#voice_subscription_options
    def attributes
      {
        plan_type:  _talk_product.plan.id,
        max_agents: _talk_product.quantity +
                    _talk_product.seasonal_quantity,
        suspended:  false,
        is_prepaid: false
      }
    end

    def talk_subscription
      @talk_subscription ||= account.voice_subscription ||
        account.build_voice_subscription
    end

    def talk_sub_account
      @talk_sub_account ||= account.voice_sub_account
    end

    def check_usage_subscription?
      flag = :billing_check_voice_usage_subscription_during_sync
      Arturo.feature_enabled_for?(flag, account)
    end
  end
end
