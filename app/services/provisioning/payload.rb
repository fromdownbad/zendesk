class Provisioning
  # SEE: https://github.com/zendesk/billing/wiki/Classic-Provisioning-Payload
  class Payload
    require_dependency 'provisioning/payload/account'
    require_dependency 'provisioning/payload/dunning'
    require_dependency 'provisioning/payload/addon'
    require_dependency 'provisioning/payload/prepaid_credit'
    require_dependency 'provisioning/payload/product'
    require_dependency 'provisioning/payload/subscription'
    require_dependency 'provisioning/payload/talk_recharge_settings'
  end
end
