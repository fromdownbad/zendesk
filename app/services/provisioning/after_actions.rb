class Provisioning
  class AfterActions < Base
    # REF: ZBC::Zuora::Synchronizer::SuiteSupport#suite_all_after_actions
    def call
      return if _support_product.blank?
      return unless _support_product.is_suite_product

      create_suite_agent
      destroy_chat_agent
    end

    private

    def create_suite_agent
      account.agents.reload.each(&:make_suite_agent!) unless dry_run?

      journal.add(:account_agents, suite_agent_created: true)
    end

    def destroy_chat_agent
      return if account.has_ocp_chat_only_agent_deprecation?
      PermissionSet.destroy_chat_agent!(account) unless dry_run?

      journal.add(:account_agents, chat_agent_destroyed: true)
    end
  end
end
