require 'simplediff-ruby'

class Provisioning
  class JournalComparator
    def self.call(payload)
      new(payload).call
    end

    def call
      # NOTE: We always return true when we turn off the comparator. This is
      # because it means that we always want to use the Classic Provisioning
      # service instead of the ZBC::Zuora::Synchronizer
      compare? ? comparison : SKIPPED_COMPARISON
    end

    private

    SKIPPED_COMPARISON = OpenStruct.new(
      matched?: true,
      details:  [:diff_skipped]
    ).freeze

    attr_reader :payload,
      :master_account_id,
      :billing_id

    def initialize(payload)
      @payload = payload
      @master_account_id, @billing_id = *payload[:account].
        values_at(:master_account_id, :billing_id)
    end

    def comparison
      data    = SimpleDiff.compare(zbc_journal.to_a, classic_journal.to_a)
      matched = data.all? { |item| item[:change] == '=' }
      OpenStruct.new(
        matched?:        matched,
        details:         [:diff_checked, data],
        zbc_journal:     zbc_journal,
        classic_journal: classic_journal
      )
    end

    def compare?
      account = Account.find(master_account_id)
      account.has_billing_journal_comparator?
    end

    def zbc_journal
      @zbc_journal ||= ::ZBC::Zuora::Synchronizer.new(
        billing_id,
        dry_run: true
      ).synchronize!.options
    end

    def classic_journal
      @classic_journal ||= ::Provisioning.call(
        master_account_id,
        payload: payload,
        dry_run: true
      ).options
    end
  end
end
