class Provisioning
  class AddonsState::Synchronizer < Base
    # ZBC::Zuora::Synchronizer::AddonsSupport#synchronize_addons!
    def call
      execute_addon_service
      return unless temporary_zendesk_agents?
      # NOTE: We need to re-save subscription to update max_agents based on
      # temporary zendesk agents.
      save_support_subscription_wrapper
    end

    private

    delegate :subscription,
      to:     :account,
      prefix: :support

    delegate :subscription_feature_addons,
      to: :account

    def addons
      Hash[_current_subscription.addons.map(&:to_addon_rateplan_info)]
    end

    def temporary_zendesk_agents?
      addons.key?('temporary_zendesk_agents') ||
        subscription_feature_addons.by_name(:temporary_zendesk_agents).any?
    end

    # rubocop:disable Lint/UnderscorePrefixedVariableName
    def execute_addon_service
      _addons       = addons
      addon_service = ::Zendesk::Features::AddonService.new(account, _addons)
      addon_service.execute! unless dry_run?

      journal.add(
        :support,
        support_addons_updated: true,
        support_addon_options:  _addons
      )
    end
    # rubocop:enable Lint/UnderscorePrefixedVariableName

    # Per Team Bilby's requirement when updating the Account or Subscription
    # it needs to be done via the ff wrapper. Otherwise the #save! method
    # will throw a RuntimeError with the message:
    #
    #   Please do not update subscription directly in Rails console. Talk to
    #   team Bilby at #ask-bilby for more details.
    #
    def save_support_subscription_wrapper
      support_subscription_wrapper.save! unless dry_run?

      journal.add(
        :support,
        support_subscription_with_temp_agents_updated: true
      )
    end

    def support_subscription_wrapper
      Zendesk::SupportAccounts::Product.retrieve(support_subscription.account)
    end
  end
end
