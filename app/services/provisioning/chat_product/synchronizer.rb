class Provisioning
  class ChatProduct::Synchronizer < Base
    # REF: ZBC::Zuora::Synchronizer::ZopimSupport#synchronize_zopim_subscription!
    def call
      _chat_product.present? ? update : cancel
    end

    private

    def update
      attributes.each { |key, value| chat_subscription[key] = value }
      chat_subscription.save! unless dry_run?

      journal.add(
        :chat,
        chat_subscription_updated: true,
        chat_subscription_options: attributes
      )
    end

    def cancel
      chat_subscription.cancel! unless dry_run?
      journal.add(:chat, chat_subscription_cancelled: true)
    end

    # REF: ZBC::Zuora::Synchronizer::ZopimSupport#zopim_subscription_options
    def attributes
      Hash(initial_provisioning_details).tap do |hash|
        hash.merge!(
          zopim_account_id:   chat_subscription.try(:zopim_account_id),
          billing_cycle_type: _current_subscription.billing_cycle_id,
          plan_type:          _chat_product.plan.name,
          max_agents:         _chat_product.quantity +
                              _chat_product.seasonal_quantity,
          status:             product_status
        )
      end
    end

    # REF: ZBC::Zuora::PlanAdapter::ZopimPlanAdapter#status
    #
    # def status
    #   const = active? ? 'Active' : 'Cancelled'
    #   "ZBC::Zopim::SubscriptionStatus::#{const}".constantize
    # end
    #
    # def active?
    #   has_zopim_rate_plan? && (zuora_subscription.status == 'Active')
    # end
    def product_status
      _current_subscription.status # active|cancelled
    end

    def initial_provisioning_details
      return unless initial_provisioning?

      {
        owner_email:  account.owner.zopim_email,
        purchased_at: Time.now
      }
    end

    def initial_provisioning?
      chat_subscription.blank? || chat_subscription.purchased_at.blank?
    end

    def chat_subscription
      @chat_subscription ||= account.zopim_subscription
    end
  end
end
