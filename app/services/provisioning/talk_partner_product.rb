class Provisioning
  class TalkPartnerProduct < Base
    require_dependency 'provisioning/talk_partner_product/synchronizer'

    def call
      Synchronizer.call(options.to_h)
    end
  end
end
