class Provisioning
  class FeaturesState < Base
    def call
      set_feature_framework_persistence
      execute_subscription_feature_service
    end

    private

    def addons
      Hash[_current_subscription.addons.map(&:to_addon_rateplan_info)].tap do |hash|
        hash.each do |name, data|
          hash[name] = data.merge(name: name)
        end
      end
    end

    def set_feature_framework_persistence
      account.set_use_feature_framework_persistence(true) unless dry_run?

      journal.add(:support, support_framework_persistence: true)
    end

    # rubocop:disable Lint/UnderscorePrefixedVariableName
    def execute_subscription_feature_service
      pricing_model_revision = _support_product.pricing_model_revision
      plan_type              = _support_product.plan.id
      _addons                = addons

      subscription_feature_service = ::Zendesk::Features::SubscriptionFeatureService.new(
        account,
        pricing_model_revision,
        plan_type,
        _addons
      )

      subscription_feature_service.execute! unless dry_run?
      subscription_feature_options = {
        pricing_model_revision: pricing_model_revision,
        plan_type:              plan_type,
        zuora_addons:           _addons
      }

      journal.add(
        :support,
        support_subscription_features_updated: true,
        support_subscription_feature_options:  subscription_feature_options
      )
    end
    # rubocop:enable Lint/UnderscorePrefixedVariableName
  end
end
