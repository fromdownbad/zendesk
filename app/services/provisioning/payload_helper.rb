class Provisioning
  module PayloadHelper
    require_dependency 'provisioning/payload/account'
    require_dependency 'provisioning/payload/dunning'
    require_dependency 'provisioning/payload/addon'
    require_dependency 'provisioning/payload/prepaid_credit'
    require_dependency 'provisioning/payload/product'
    require_dependency 'provisioning/payload/subscription'
    require_dependency 'provisioning/payload/talk_recharge_settings'
    require_dependency 'provisioning/payload'

    extend ActiveSupport::Concern

    def _account
      @_account ||= Payload::Account.new(payload.fetch(:account))
    end

    def _dunning
      _account.dunning
    end

    def _talk_recharge_settings
      _account.talk_recharge_settings
    end

    def _current_subscription
      _account.current_subscription
    end

    def _future_subscription
      _account.future_subscription
    end

    # Defines a set of methods to fetch a product associated with the current
    # subscription.
    Payload::Subscription::SUPPORTED_PRODUCTS.each do |name|
      define_method("_#{name}_product") do
        _current_subscription&.__send__(:"#{name}_product")
      end
    end
  end
end
