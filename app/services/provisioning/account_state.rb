class Provisioning
  class AccountState < Base
    def call
      return unless synchronize_account_state?

      synchronize_account_state
    end

    private

    # REF: ZBC::Zuora::Synchronizer#synchronize_account_state?
    def synchronize_account_state?
      # NOTE: The original implementation checks:
      #
      #   zendesk_account.multiproduct? && !approval_required?
      #
      # There is no need to check if approval is required anymore. When
      # provisioning is executed in Classic the caller has already determined
      # that it is good to sync.
      account.multiproduct?
    end

    def synchronize_account_state
      _current_subscription.active? ? sync_dunning_state : cancel_account
    end

    # REF: ZBC::Zuora::Synchronizer#sync_dunning_state!
    def sync_dunning_state
      # NOTE: This step should be handled by Billing on sync via Pravda by
      # setting the pravda account status to either 'suspended' or 'cancelled'
      # -- modifying the :is_serviceable and :is_active independent of Pravda is
      # NOT recommended.
      #
      # SEE: https://zendesk.atlassian.net/wiki/spaces/BOLT/pages/444826334/Account+States
      account.update_attributes!(dunning_attributes) unless dry_run?

      journal.add(
        :account,
        dunning_state_updated: true,
        dunning_state_options: dunning_attributes
      )
    end

    # :reek:FeatureEnvy { enabled: false }
    def dunning_attributes
      state = _dunning.state
      level = ZBC::States::Dunning.level_for_state(state)

      {
        is_active:      (level < ZBC::States::Dunning::State::SUSPENDED.level),
        is_serviceable: (level < ZBC::States::Dunning::State::LASTCHANCE.level)
      }
    end

    # REF: ZBC::Zuora::Synchronizer#synchronize_account_state!
    def cancel_account
      # NOTE: This step should be handled by Billing on sync via Pravda by
      # setting the pravda account status to 'cancelled'
      #
      # SEE: https://zendesk.atlassian.net/wiki/spaces/BOLT/pages/444826334/Account+States
      account.cancel! unless dry_run?
      journal.add(:account, account_cancelled: true)
    end
  end
end
