class Provisioning
  class MultiBrandState < Base
    # ZBC::Zuora::Synchronizer::MultibrandHelpCenterSupport#synchronize_multibrand_help_center!
    def call
      multi_brand_setting.value = _account.multibrand_includes_help_centers?
      multi_brand_setting.save! unless dry_run?

      journal.add(
        :guide,
        multibrand_help_center_updated:  true,
        multibrand_help_center_included: _account.multibrand_includes_help_centers?
      )
    end

    private

    def multi_brand_setting
      @multi_brand_setting ||= account.settings.
        where(name: 'multibrand_includes_help_centers').
        first_or_initialize
    end
  end
end
