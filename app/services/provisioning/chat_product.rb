class Provisioning
  class ChatProduct < Base
    require_dependency 'provisioning/chat_product/synchronizer'

    def call
      return unless synchronize_chat?

      synchronize_chat
    end

    private

    # REF: ZBC::Zuora::Synchronizer#synchronize_zopim_subscription?
    #
    # def synchronize_zopim_subscription?
    #   if current_zopim_product.try(:rate_plan)
    #     ----------------------------------------------------------------------
    #     NOTE: THE CALL TO update_chat_product IS NOT IMPLEMENTED SINCE
    #     BILLING WOULD ALREADY HAVE UPDATED PRAVDA BEFORE SENDING THE
    #     PROVISIONING PAYLOAD TO CLASSIC.
    #     ----------------------------------------------------------------------
    #     if local_zopim_subscription.blank?
    #       update_chat_product
    #       return false
    #     end
    #
    #     if zopim_integration.try(:external_zopim_id).present?
    #       return local_zopim_subscription.present? && zopim_integration.present?
    #     else
    #       return true
    #     end
    #   end
    #
    #   local_zopim_subscription.present? &&
    #     !local_zopim_subscription.is_trial? &&
    #     local_zopim_subscription.purchased_at.present?
    # end
    def synchronize_chat?
      if _chat_product.present?
        previously_integrated?
      else
        previously_purchased?
      end
    end

    def previously_integrated?
      chat_subscription.present? &&
        chat_integration.present? &&
        chat_integration.external_zopim_id.present?
    end

    def previously_purchased?
      chat_subscription.present? &&
        !chat_subscription.is_trial? &&
        chat_subscription.purchased_at.present?
    end

    def chat_subscription
      @chat_subscription ||= account.zopim_subscription
    end

    def chat_integration
      @chat_integration ||= ::ZopimIntegration.find_by_account_id(account.id)
    end

    def synchronize_chat
      Synchronizer.call(options.to_h)
    end
  end
end
