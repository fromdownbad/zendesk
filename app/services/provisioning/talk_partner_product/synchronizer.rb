class Provisioning
  class TalkPartnerProduct::Synchronizer < Base
    # REF: ZBC::Zuora::Synchronizer::VoiceSupport#synchronize_voice_subscription!
    def call
      _talk_partner_product.present? ? update : deactivate
    end

    private

    def update
      attributes.each { |key, value| talk_partner_subscription[key] = value }
      talk_partner_subscription.save! unless dry_run?

      journal.add(
        :talk_partner,
        talk_partner_subscription_updated: true,
        talk_partner_subscription_options: attributes
      )
    end

    # REF: ZBC::Zuora::Synchronizer::TpeSupport#synchronize_tpe_subscription!
    def deactivate
      return unless talk_partner_subscription.persisted?

      talk_partner_subscription.deactivate unless dry_run?
      journal.add(:talk_partner, talk_partner_deactivated: true)
    end

    # REF: ZBC::Zuora::Synchronizer::TpeSupport#tpe_subscription_options
    def attributes
      {
        active:     true,
        plan_type:  _talk_partner_product.plan.id,
        max_agents: _talk_partner_product.quantity +
                    _talk_partner_product.seasonal_quantity
      }
    end

    # REF: ZBC::Zuora::Synchronizer::TpeSupport#tpe_subscription
    def talk_partner_subscription
      @talk_partner_subscription ||= account.tpe_subscription ||
        account.build_tpe_subscription
    end
  end
end
