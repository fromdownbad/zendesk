class Provisioning
  class SupportProduct::Inflator < Base
    def call
      return unless inflation_required?

      inflate_support
    end

    private

    delegate :subscription,
      to:     :account,
      prefix: :support

    # REF: ZBC::Zuora::Synchronizer#inflate_support?
    def inflation_required?
      support_subscription.blank? &&
        _current_subscription.active? &&
        _support_product.present?
    end

    def inflate_support
      account.start_support_trial unless dry_run?
      journal.add(:support, inflate_support: true)
    end
  end
end
