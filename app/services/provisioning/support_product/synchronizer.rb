class Provisioning
  class SupportProduct::Synchronizer < Base
    # :reek:TooManyStatements { enabled: false }
    def call
      # NOTE: These are virtual attributes. The :bypass_approval field will
      # always be true because approval-requirements should already have been
      # approved before provisioning is initiated.
      zuora_subscription.bypass_approval   = true
      zuora_subscription.seasonal_quantity = _support_product.seasonal_quantity

      # NOTE: The ff skirts the mass-assignment protection for the :account_id,
      # :subscription_id, and :last_synchronized_at fields (via attr_accessible
      # declaration in ZBC::Zuora::Subscription)
      zuora_subscription.account_id           ||= account.id
      zuora_subscription.subscription_id      ||= support_subscription.id
      zuora_subscription.last_synchronized_at = Time.now

      attributes.each { |attribute, value| zuora_subscription[attribute] = value }
      zuora_subscription.save! unless dry_run?

      journal.add(
        :support,
        zuora_subscription_updated: true,
        zuora_subscription_options: attributes.except(:term_end_date, :future_subscription_starts_at)
      )
    end

    private

    delegate :subscription,
      to:     :account,
      prefix: :support

    # REF: ZBC::Zuora::Synchronizer::ZendeskSupport#zuora_subscription_options
    def attributes
      {
        account_id:                    account.id,
        subscription_id:               support_subscription.id,
        assisted_agent_add_enabled:    _account.assisted_agent_add_enabled,
        currency_type:                 _account.currency_id,
        dunning_level:                 _dunning.level_id,
        is_dunning_enabled:            _dunning.enabled,
        payment_method_type:           _account.payment_method_type_id,
        sales_model:                   _account.sales_model,
        billing_cycle_type:            _current_subscription.billing_cycle_id,
        term_end_date:                 _current_subscription.term_end_date,
        is_active:                     _current_subscription.active?,
        future_subscription_starts_at: future_subscription_starts_at,
        is_elite:                      _support_product.is_elite,
        max_agents:                    _support_product.quantity +
                                       _support_product.seasonal_quantity,
        plan_type:                     _support_product.plan.id,
        pricing_model_revision:        _support_product.pricing_model_revision
      }
    end

    def future_subscription_starts_at
      _future_subscription.try(:term_start_date)
    end

    # REF: ZBC::Zuora::Synchronizer#zuora_subscription
    def zuora_subscription
      @zuora_subscription ||= ZBC::Zuora::Subscription.find_or_initialize_by(
        zuora_account_id: payload.dig(:account, :billing_id)
      )
    end
  end
end
