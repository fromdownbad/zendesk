class UserEntityPublisher
  TOPIC = 'support.user_entity'.freeze

  def initialize(account_id:)
    @account_id = account_id
  end

  def publish_for_user(user_id:)
    user = User.on_master.without_kasket do
      User.find_by(account_id: account_id, id: user_id)
    end
    publish(user: user)
    Rails.logger.info "[UserEntityPublisher] published for account #{account_id} user #{user_id}"
  end

  def publish_for_account(batch_size: 100, backfill_status:, user_counter:)
    account = Account.with_deleted { Account.find(account_id) }
    return if account.deleted?
    User.on_master.without_kasket do
      user_id_index = [backfill_status.last_processed_user(account_id: account_id), account.users.first.id].max
      account.users.find_each(start: user_id_index, batch_size: batch_size).with_index do |user, index|
        publish(user: user)
        if (index + 1) % user_counter == 0
          backfill_status.set_last_processed_user(account_id: account_id, user_id: user.id)
        end
      end
    end
  end

  def publish_tombstone(user_id:)
    write_message(
      user_id: user_id,
      value: nil
    )
  end

  private

  attr_reader :account_id

  def publish(user:)
    return unless user
    write_message(
      user_id: user.id,
      value: UserProtobufEncoder.new(user).to_proto
    )
    true
  rescue StandardError => e
    Rails.application.config.statsd.client.increment('user_entity_publisher.publish.error', tags: ["account_id:#{user.account_id}"])
    Rails.logger.info "#{self.class}: Error while trying to publish user message. #{e.class}\n#{e.message}"
  end

  def write_message(user_id:, value:)
    EscKafkaMessage.create!(
      account_id: account_id,
      topic: TOPIC,
      key: "#{account_id}/#{user_id}",
      partition_key: "#{account_id}/#{user_id}",
      value: value
    )
  end
end
