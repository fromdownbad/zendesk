class AccountSynchronizer
  def self.call(payload)
    new(payload).call
  end

  def call
    comparison.matched? ? provision : synchronize
  end

  private

  attr_reader :payload

  def initialize(payload)
    @payload = payload.deep_symbolize_keys
  end

  def master_account_id
    payload.dig(:account, :master_account_id)
  end

  def billing_id
    payload.dig(:account, :billing_id)
  end

  def provision
    trace('classic') do
      Provisioning.call(master_account_id, payload: payload, dry_run: false)
    end
  end

  def synchronize
    trace('zbc') do
      ZBC::Zuora::Synchronizer.new(
        billing_id,
        bypass_approval: true
      ).synchronize!
    end
  end

  def comparison
    @comparison ||= Provisioning::JournalComparator.call(payload)
  end

  # :reek:TooManyStatements
  def trace(topic)
    subject = "#{topic}.sync.executed"
    ZendeskAPM.trace("classic-provisioning", service: "classic-provisioning-service", resource: subject) do |span|
      yield
      span.set_tag('account.master_account_id', master_account_id)
      span.set_tag('account.billing_id', billing_id)
      span.set_tag('payload', payload)
      span.set_tag('comparison', comparison.to_h)
    end

    message = <<~MESSAGE.squish
      #{subject},
      master_account_id=#{master_account_id},
      billing_id=#{billing_id}
      payload=#{payload}
      comparison=#{comparison.to_h}
    MESSAGE
    Rails.logger.info(message)
  end
end
