class GroupPublisher
  TOPIC = "support.group_entities".freeze

  def initialize(account_id:)
    @account_id = account_id
  end

  def publish_tombstone(group_id:)
    produce_record group_id: group_id, value: nil
  end

  def publish(group:)
    return unless group

    produce_record group_id: group.id, value: encode(group: group)
  end

  private

  attr_reader :account_id

  def produce_record(group_id:, value:)
    EscKafkaMessage.create!(
      account_id: account_id,
      topic: TOPIC,
      value: value,
      key: "#{account_id}:#{group_id}"
    )
  end

  def encode(group:)
    GroupProtobufEncoder.new(group).to_proto
  end
end
