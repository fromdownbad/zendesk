class TicketDeflector
  WAIT_TIME_BEFORE_SOLVE = 20

  def initialize(account:, deflection:, resolution_channel_id:)
    @account = account
    @ticket_deflection = deflection
    @resolution_channel_id = resolution_channel_id
  end

  def solve(article_id:, mobile: nil)
    solve_ticket(article_id, mobile)
  end

  def process_deflection_rejection(article_id:, mobile: nil, reason: nil)
    return if ticket_already_solved?(ticket)
    return if ticket_deflection.solved?

    ticket_deflection.mark_as_not_solved(resolution_channel_id)

    create_marked_unhelpful_tag(ticket) if ticket.present?

    TicketDeflectionArticle.reject_by_end_user(
      ticket_deflection,
      article_id,
      reason
    )

    ab_statsd_client.irrelevant(stats_tags(mobile))
  end

  private

  attr_reader :account, :resolution_channel_id, :ticket_deflection

  def ticket
    @ticket ||= ticket_deflection.ticket
  end

  def solve_ticket(article_id, mobile)
    return if ticket_already_solved?(ticket)
    return if solve_attempt_too_quick?(mobile)

    ab_statsd_client.solve_initiated(stats_tags(mobile))

    stats = build_stats(mobile)

    AnswerBot::TicketSolverJob.enqueue(
      account.id,
      ticket_deflection_user.id,
      ticket_deflection.id,
      article_id,
      resolution_channel_id,
      stats
    )
    create_resolved_tag(ticket)

    true
  end

  def ticket_already_solved?(ticket)
    ticket && ticket.finished?
  end

  def solve_attempt_too_quick?(mobile)
    return unless ticket_deflection.deflection_channel_id == Zendesk::Types::ViaType.MAIL
    if (Time.zone.now.to_i - ticket_deflection.created_at.to_i) < WAIT_TIME_BEFORE_SOLVE
      ab_statsd_client.solved_too_quick(stats_tags(mobile))
      true
    end
  end

  def ticket_deflection_user
    user = ticket_deflection.user

    if user.is_agent? # avoid running into missing required fields
      User.system
    else
      user
    end
  end

  def build_stats(mobile)
    stats_tags(mobile).merge(
      subdomain: ticket_deflection.brand.subdomain,
      via_id: ticket.via_id
    )
  end

  def stats_tags(mobile)
    tags = {
      model_version: ticket_deflection.model_version,
      language: ticket_deflection.detected_locale
    }
    tags[:mobile] = mobile unless mobile.nil?

    tags
  end

  def create_marked_unhelpful_tag(ticket)
    tag = 'ab_marked_unhelpful'.freeze
    add_tag(ticket, tag)
  end

  def create_resolved_tag(ticket)
    tag = 'ab_resolved'.freeze
    add_tag(ticket, tag)
  end

  def add_tag(ticket, tag)
    if Arturo.feature_enabled_for?(:answer_bot_tag_automation, account)
      ::AnswerBot::TicketDeflectionTaggingJob.enqueue(account.id, ticket.id, tag)
    end
  end

  def ab_statsd_client
    @ab_statsd_client ||= Datadog::AnswerBot.new(
      via_id: ticket_deflection.via_id,
      subdomain: ticket_deflection.brand.subdomain,
      deflection_channel_id: ticket_deflection.deflection_channel_id,
      resolution_channel_id: resolution_channel_id
    )
  end
end
