class AccountSubdomainPropagator
  def self.call(account, subdomain: nil, subdomain_was: nil)
    Rails.logger.info("AccountSubdomainPropagator.call(#{account.inspect}, #{subdomain.inspect}, #{subdomain_was.inspect})")
    new(account, subdomain, subdomain_was).call
  end

  def call
    if subdomain_changed?
      update_route_subdomain
      update_recipient_address_host
    end
  end

  private

  attr_reader :account, :subdomain, :subdomain_was

  def initialize(account, subdomain, subdomain_was)
    @account = account
    @subdomain = subdomain || account.subdomain
    @subdomain_was = subdomain_was || account.subdomain_was
  end

  def subdomain_changed?
    @subdomain != @subdomain_was
  end

  def logger
    @logger ||= Rails.logger
  end

  def update_route_subdomain
    account.route.update_attribute(:subdomain, subdomain)
  end

  # horribly copied from https://github.com/zendesk/zendesk_core/blob/78aaabef17b9fe49eb58b554c13b410e512cac9a/lib/zendesk/routes/url_generator.rb#L12-L14
  def default_host
    "#{subdomain.downcase}.#{Zendesk::Configuration[:host]}"
  end

  # horribly copied from https://github.com/zendesk/zendesk/blob/61d86cc4f9db12dc1e0e2c7f49cd4cc7eec29680/app/models/route.rb#L74-L76
  def recipient_addresses
    @recipient_addresses ||= account.recipient_addresses.where("email like ?", "%@#{default_host_was}").to_a
  end

  # horribly copied from https://github.com/zendesk/zendesk/blob/61d86cc4f9db12dc1e0e2c7f49cd4cc7eec29680/app/models/route.rb#L78-L82
  def default_host_was
    host = default_host
    host = host.sub(subdomain, subdomain_was) if subdomain_changed?
    host
  end

  # horribly copied from https://github.com/zendesk/zendesk/blob/61d86cc4f9db12dc1e0e2c7f49cd4cc7eec29680/app/models/route.rb#L84-L91
  def update_recipient_address_host
    logger.info "[AccountSubdomainPropagator] updating #{recipient_addresses.count} recipient addresses like #{default_host_was}"
    recipient_addresses.each do |ra|
      new_email = ra.email.sub(/@.*/, "@#{default_host}")
      logger.info "[Route] updating #{ra.id} email (Old: #{ra.email} => New: #{new_email})"
      ra.update_column(:email, new_email)
    end
  end
end
