module Billing
  # This service evaluates if the account is participating in Zuora
  # Out-of-Classic transition.
  class OOC::ZuoraParticipator
    private_class_method :new

    def self.call(account)
      new(account).call
    end

    def call
      account.has_billing_ooc_zuora?
    end

    private

    attr_reader :account

    def initialize(account)
      @account = account
    end
  end
end
