module Billing
  class SobaParticipation
    private_class_method :new

    def self.eligible?(account)
      new(account).eligible?
    end

    def eligible?
      allow_as_multiproduct? && allow_for_account?
    end

    private

    attr_reader :account

    delegate :subscription,
      :zuora_subscription,
      :reseller_account?,
      to: :account

    def initialize(account)
      @account = account
    end

    def allow_as_multiproduct?
      multiproduct_enabled? &&
        sales_model_allowed? &&
        !reseller_account?
    end

    def allow_for_account?
      allow_for_new_accounts? ||
        allow_for_legacy_accounts?
    end

    # NOTE: New accounts are either in still in trial or was purchased as
    # a multi-product account (ie: it would have its initiated-by field
    # set to Billing)
    def allow_for_new_accounts?
      still_in_trial? ||
        initiated_by_billing?
    end

    def multiproduct_enabled?
      flag = :billing_multi_product_participant
      Arturo.feature_enabled_for?(flag, account)
    end

    def sales_model_allowed?
      all_sales_model_enabled? ||
        subscription.self_service?
    end

    def all_sales_model_enabled?
      flag = :billing_multi_product_for_all_sales_models
      Arturo.feature_enabled_for?(flag, account)
    end

    # NOTE: Expired trials may have their :is_trial state set to false
    def still_in_trial?
      account.is_trial? ||
        account.trial_expired?
    end

    def initiated_by_billing?
      initiated_by = zuora_subscription.try(:initiated_by).to_s
      initiated_by.casecmp('billing').zero?
    end

    def allow_for_legacy_accounts?
      legacy_accounts_enabled? &&
        pricing_model_allowed?
    end

    def pricing_model_allowed?
      all_pricing_model_revisions_enabled? ||
        allow_for_pricing_model?
    end

    def legacy_accounts_enabled?
      flag = :billing_multi_product_for_legacy_accounts
      Arturo.feature_enabled_for?(flag, account)
    end

    def all_pricing_model_revisions_enabled?
      flag = :billing_multi_product_for_all_pricing_models
      Arturo.feature_enabled_for?(flag, account)
    end

    def allow_for_pricing_model?
      revision = subscription.pricing_model_revision.to_i
      flag     = :"billing_multi_product_for_pricing_model_#{revision}"
      Arturo.feature_enabled_for?(flag, account)
    end
  end
end
