# try fixing corrupt pack errors https://github.com/bundler/bundler/issues/3589
# and 'You appear to have cloned an empty repository'
# errors we saw so far originate in copy_to and checkout
# - git fetch --force --quiet --tags # copy_to
# - git reset --hard # copy_to
# - git clone --no-checkout --quiet # checkout
# just wrapping copy_to/checkout does not work since the retries already produces
# so much output that travis stops, so we attack from inside of the git_retry ... good times
require 'bundler/source/git/git_proxy'
Bundler::Source::Git::GitProxy.prepend(Module.new do
  def copy_to(destination, *args)
    with_hard_retry(destination) { super }
  end

  def checkout
    with_hard_retry { super }
  end

  def with_hard_retry(destination = nil)
    Thread.current[:bundler_hard_retry] = true
    yield
  rescue Bundler::Source::Git::GitCommandError
    retried ||= :no
    raise if retried == :yes
    retried = :yes

    puts "Git error detected, trying hard retry"

    FileUtils.rm_rf(path) && checkout # clean checkout of source dir
    FileUtils.rm_rf(destination) if destination # trigger clean copy when retrying copy_to

    # private deadmansnitch by grosser
    recorded = system("curl -d 'm=#{ENV["TRAVIS_JOB_NUMBER"]}' https://nosnch.in/6c60fc3ba5")
    puts "Recording fix: #{recorded}"

    retry
  ensure
    Thread.current[:bundler_hard_retry] = nil
  end

  def git_retry(*args)
    if Thread.current[:bundler_hard_retry]
      git(*args)
    else
      super
    end
  end
end)
