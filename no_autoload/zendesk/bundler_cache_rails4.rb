class << Bundler
  def app_cache(custom_path = nil)
    path = custom_path || root
    path.join("vendor/cache.rails4")
  end
end

Bundler::Runtime.class_eval do
  def cache_path
    Bundler.app_cache
  end
end
