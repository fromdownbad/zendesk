# https://github.com/bundler/bundler/issues/3014
# Do not re-build native extensions: speedup + avoiding crashing already running apps

klass = defined?(Bundler::RubyGemsGemInstaller) ? Bundler::RubyGemsGemInstaller : Bundler::GemInstaller

klass.class_eval do
  def build_extensions
    marker = "#{gem_dir}/ext-built"
    return if File.exist?(marker)
    result = super
    File.write(marker, "true")
    result
  end
end
