# The "service" database credentials (in database.yml) shouldn't be used by humans.
# Instead, individuals should use their personal database credentials.
# You create a ~/.my.cnf to provide those credentials to dbconsole and rails console

require 'inifile'
require 'rails/all'

module Zendesk
  module Extensions
    module MyCnf
      def self.sh(cmd)
        result = `#{cmd}`
        raise "failed to execute #{cmd} successfully" unless $?.success?
        result
      end

      def self.credentials_from_mycnf
        # Using 'zendesk' credentials is unacceptable, even if coming from a my.cnf.
        # Instead we must find the real user, and then read their my.cnf.
        # my.cnfs will be chmod 600. So in turn, we must use sudo to read that other users my.cnf

        me = ENV['USER']
        real_me = if me == 'zendesk'
          # get the real user who sudo'ed into zendesk (perhaps via console_classic)
          sh("who -m").split(' ').first
        else
          me
        end

        ini_file = File.expand_path("~#{real_me}/.my.cnf")
        unless File.exist?(ini_file)
          warn "no my.cnf (#{ini_file}) available for database credentials"
          return
        end

        file_content = if me == 'zendesk'
          sh("sudo cat #{ini_file}")
        else
          File.open(ini_file).read
        end

        ini_contents = IniFile.new(content: file_content)
        warn "no [mysql] section in my.cnf (#{ini_file})" unless ini_contents['mysql']

        user = ini_contents['mysql']['user']
        password = ini_contents['mysql']['password'] || ''

        [user, password]
      end

      def database_configuration
        config = super

        username, password = Zendesk::Extensions::MyCnf.credentials_from_mycnf
        if username
          h = config[ENV['RAILS_ENV']]
          h['username'] = username
          h['password'] = password
        end
        config
      end
    end
  end
end

Rails::Application::Configuration.class_eval do
  prepend Zendesk::Extensions::MyCnf
end
