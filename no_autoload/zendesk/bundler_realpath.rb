# In production we symlink vendor/bundle
# this results in double load errors which looks like:
# `already initialized constant Arturo::Middleware::MISSING_FEATURE_ERROR`
#
# Reason:
#  - rails adds the realpath of each engines lib (and various other folders) to the $LOAD_PATH
#  - bundler adds the symlinked version to the $LOAD_PATH
#  - require_relative uses the realpath
#
# Unicorn loads the bundle before it loads rails, so we cannot patch in config/* or we run into
# double load warnings from rack. So we patch Bundler::Runtime#setup which gets called by Bundler.setup after the
# Gemfile (where we require this file) is done loading.
#
# Reproduce:
# - mv vendor/bundle tmp
# - cd vendor && ln -s ../tmp/bundle bundle
# - enable eager_load + preload_frameworks in development
# - rails runner 1
Bundler::Runtime.prepend(Module.new do
  def setup(*)
    super
  ensure
    linked_bundle = File.expand_path(Bundler.bundle_path)
    real_bundle = File.realpath(Bundler.bundle_path)
    if linked_bundle != real_bundle
      $LOAD_PATH.each_with_index do |path, i|
        $LOAD_PATH[i] = path.sub(linked_bundle, real_bundle)
      end
    end
  end
end)
