require 'sinatra/base'
require 'resque/server'
require 'resque/status_server'
require 'resque-retry'
require 'resque-retry/server'
require 'zendesk/configuration'
require 'zendesk/redis_factory'

rails_env = ENV['RAILS_ENV'] || 'development'

Zendesk::Configuration.setup(env: rails_env)
Resque.redis = Zendesk::RedisFactory.create_from_consul(ENV.fetch('RESQUE_CLUSTER'))

map "/" do
  run Resque::Server
end
