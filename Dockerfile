# ICU image create by https://github.com/zendesk/docker-images-community
FROM gcr.io/docker-images-180022/base/ruby:2.5.8-xenial-jemalloc-icu

RUN apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    iproute2 \
    libmysqlclient-dev \
    libsasl2-dev memcached \
    make \
    build-essential \
    libcurl4-openssl-dev \
    imagemagick \
    default-jre \
    libsnappy-dev \
    libidn11-dev \
    git \
    file \
    less && \
  apt-mark hold ruby ruby-jemalloc && \
  apt-get -y dist-upgrade && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

RUN ruby --version

# Install the version of nokogiri specified in the Gemfile so it stays cached / fast
RUN gem install bundler -v 1.17.3 --no-document && \
  gem install nokogiri -v 1.10.9 --no-document

ENTRYPOINT ["/usr/local/bin/dumb-init", "--", "./script/docker-boot"]

CMD bin/app-server

RUN mkdir -p /app/log
WORKDIR /app

# 4080: app-server; 9292: resque-web
EXPOSE 4080 9292

COPY [".ruby-version", "Gemfile", "Gemfile.lock", "Rakefile", "resque.ru", "./"]
COPY config.ru.example config.ru
COPY vendor/cache vendor/cache
COPY no_autoload no_autoload

ARG BUNDLER_JOBS=4
# Our base ruby image sets `BUNDLE_BIN` which results in binstubs being installed when running `bundle install`
# Run `BUNDLE_GEMFILE=Gemfile bundle install` last to ensure the binstubs are using the correct Gemfile.
RUN BUNDLE_GEMFILE=Gemfile bundle install --local --jobs=${BUNDLER_JOBS} || bundle check

# Databases
COPY db/seeds.rb db/seeds.rb
COPY db/fixtures db/fixtures
COPY db/defaults db/defaults

# App code
COPY bin bin
COPY script script
COPY public public
COPY config config
COPY lib lib
COPY app app

# Generate .github/codeowners.yml
COPY .github/CODEOWNERS .github/CODEOWNERS
RUN bundle exec rake zendesk:precompile_codeowners

# This is only needed to generate the manifests, the assets aren't actually used
RUN RAILS_ENV=development PRE_COMPILE_ENV=production DIGEST_ASSETS=1 bundle exec rake assets:precompile

RUN rm -f /app/log/*

ARG REVISION
LABEL REVISION=${REVISION}
