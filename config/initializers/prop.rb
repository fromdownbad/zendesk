require 'prop'

Prop.cache = if ENV['PROP_USE_ELASTICACHE'] == 'true'
  require 'active_support/cache/dalli_store'

  elasticache_servers = Dalli::ElastiCache.new(ENV.fetch('PROP_ELASTICACHE_ENDPOINT')).servers
  ActiveSupport::Cache::DalliStore.new(elasticache_servers, Rails.application.config.memcache_options)
else
  Rails.cache
end

# If you update anything here, please be sure to change the documentation
# at https://support.zendesk.com/entries/503405-rate-limits
# as well as the Rate Limit part from introduction.md

# rubocop:disable Layout/ExtraSpacing
Prop.configure :portal_tickets,                       threshold: 5,         interval: 1.hour,     description: 'Rate limit for creating new tickets reached, please wait before submitting more tickets'
Prop.configure :twitter_api_server_error,             threshold: 500,       interval: 5.minutes
Prop.configure :email_sender_ticket_updates,          threshold: 10,        interval: 1.hour,     description: 'Rate limit for email ticket updates exceeded, please wait before updating this ticket again'
Prop.configure :email_ticket_updates,                 threshold: 20,        interval: 1.hour,     description: 'Rate limit for email ticket updates exceeded, please wait before updating this ticket again'
Prop.configure :rule_pagination,                      threshold: 50,        interval: 5.minutes,  description: 'Pagination out of bounds, please wait before using this view again and make sure to use valid pages only'
Prop.configure :rule_execution_time_limit,            threshold: 5 * 1000,  interval: 10.minutes, description: 'Time limit on rule execution for a specific rule and rule context in msec'
Prop.configure :credit_card_requests_by_ip,           threshold: 10,        interval: 1.hour,     description: 'This page can only be accessed a limited number of times within the same time span. This is a security measure, please try again later if you wish to update your credit card. Thank you for your understanding.'
Prop.configure :credit_card_requests_by_account,      threshold: 10,        interval: 1.hour,     description: 'This page can only be accessed a limited number of times within the same time span. This is a security measure, please try again later if you wish to update your credit card. Thank you for your understanding.'
Prop.configure :credit_card_callbacks_by_ip,          threshold: 10,        interval: 1.day,      description: 'Too many attempts to register an invalid credit card, try again tomorrow.'
Prop.configure :zuora_account_updates,                threshold: 20,        interval: 1.hour,     description: "Account synchronization rate limit reached."
Prop.configure :satisfaction_token_attempts,          threshold: 5,         interval: 5.minutes
Prop.configure :incremental_ticket_exports,           threshold: 1,         interval: 1.minutes,  description: "Number of allowed incremental ticket export API requests per minute exceeded"
Prop.configure :sample_incremental_ticket_exports,    threshold: 10,        interval: 20.minutes, description: "Number of allowed incremental ticket sample export API requests per 20 minutes exceeded"
Prop.configure :incremental_exports,                  threshold: 10,        interval: 1.minutes,  description: "Number of allowed incremental export API requests per minute exceeded"
Prop.configure :sample_incremental_exports,           threshold: 10,        interval: 20.minutes, description: "Number of allowed incremental sample export API requests per 20 minutes exceeded"
Prop.configure :monitor_delay_notification,           threshold: 1,         interval: 10.minutes
Prop.configure :twitter_monitor_delay_notification,   threshold: 1,         interval: 10.minutes
Prop.configure :attachment_requests_by_ip,            threshold: 30,        interval: 1.minutes,  description: 'Number of allowed attachment requests per minute exceeded, please wait before trying to retrieve another attachment'
Prop.configure :anonymous_requests,                   threshold: 5,         interval: 1.hour,     description: 'Number of allowed requests per hour exceeded'
Prop.configure :anonymous_web_widget_ticket_requests, threshold: 5,         interval: 1.minute,   description: 'Number of allowed requests per minute exceeded'
Prop.configure :fabric_account_creations,             threshold: 2500,      interval: 1.minute,   description: 'Number of allowed account creations per minute exceeded'
Prop.configure :execute_user_view,                    threshold: 1,         interval: 10.minutes, description: 'Rate limit for viewing this customer list exceeded, please wait before viewing this customer list again.'
Prop.configure :execute_user_view_2,                  threshold: 1,         interval: 1.minutes,  description: 'Rate limit for viewing this customer list exceeded, please wait before viewing this customer list again.'
Prop.configure :password_reset_requests,              threshold: 10,        interval: 5.minutes,  description: 'Rate limit for password reset requests exceeded, please wait before trying again.'
Prop.configure :test_target,                          threshold: 20,        interval: 1.minutes,  description: 'Rate limit for test target requests.'
Prop.configure :update_sugar_crm,                     threshold: 20,        interval: 1.minutes,  description: 'Rate limit for sugar crm integration update requests exceeded, please wait before trying again.'
Prop.configure :gmail_recipients,                     threshold: 1500,      interval: 1.day,      description: 'Rate limit for gmail sending via api.', first_throttled: true
Prop.configure :csv_export_throttle,                  threshold: 100000,    interval: 1.hour,     description: 'Rate limit for CSV export exceeded. Each user is allowed to export 100,000 tickets per hour.'
Prop.configure :radar_expiry,                         threshold: 5,         interval: 10.seconds, description: 'Rate limit for updating resources via radar'
Prop.configure :create_or_update_user,                threshold: 1,         interval: 1.second,   description: 'Rate limit for creating or updating a single user. If you need to update many users, consider the create_or_update_many endpoint.'
Prop.configure :lets_encrypt_certificates,            threshold: 10,        interval: 3.hours,    description: "Rate limit for requesting certificates from Let's Encrypt"
Prop.configure :twitter_action,                       threshold: 1000,      interval: 1.hour,     description: 'Rate limit for the Twitter trigger action.  This is intended to prevent or slow down trigger-induced loops.  See CT-2386.'
Prop.configure :resend_owner_welcome_email,           threshold: 1,         interval: 1.minutes,  description: 'Number of allowed requests for sending owner welcome emails exceeded'
Prop.configure :ticket_deletions,                     threshold: 400,       interval: 1.minutes,  description: 'Number of allowed ticket deletions per minute exceeded'
Prop.configure :email_full_body_fetches,              threshold: 20,        interval: 1.minute,   description: 'Rate limit for full email body fetches exceeded. Please wait 1 minute and try again.'
Prop.configure :create_or_update_on_unique_user,      threshold: 5,         interval: 1.minute,   description: 'Rate limit for create_or_update for a single unique user exceeded. Please wait 1 minute and try again.'
Prop.configure :audit_logs_csv_export,                threshold: 1,         interval: 1.minute,   description: 'Rate limit for Audit log CSV Export exceeded. Please wait 1 minute and try again.'
Prop.configure :search_export_api,                    threshold: 100,       interval: 1.minute,   description: 'Rate limit for Export API (api/v2/search/export) exceeded. Please wait 1 minute and try again.'

# Prop to limit the number of permanent deletions to avoid accounts deleting all of their inactive users
# with scripts and causing problems in the GDPR architecture.
Prop.configure :user_permanent_deletions, threshold: 700, interval: 10.minutes, description: 'Number of allowed permanent user deletions exceeded, please wait before trying to permanently delete another user'

# Prop for bulk import
Prop.configure :trial_ticket_bulk_import, threshold: 1, interval: 5.minutes, description: 'Rate limit trial bulk import requests'

# TODO: remove forum_entries once monitor is not using it anymore.
Prop.configure :forum_entries,  threshold: 10, interval: 1.day, description: 'You have exceeded the daily limit of articles that can be created by a user. Please try again in 24 hours.'
Prop.configure :forum_comments, threshold: 50, interval: 1.day, description: 'You have exceeded the daily limit of comments that can be submitted by a user. Please try again in 24 hours.'

# API team

# See https://zendesk.atlassian.net/browse/API-893
Prop.configure :api_unauthorized_requests, threshold: 60, interval: 1.hour, description: "Number of allowed unauthorized API requests per hour exceeded"

# Account Level API Product Limits
Prop.configure :v2_tickets_update, threshold: 1000,          interval: 10.minutes,   description: 'Rate limit for Ticket updates'
Prop.configure :v2_tickets_index, threshold: 100000,         interval: 1.minute,   description: 'Rate limit for Ticket index'
Prop.configure :v2_tickets_index_pagination, threshold: 100, interval: 10.minutes,   description: 'Rate limit for Ticket index pagination'
Prop.configure :v2_tickets_index_deep_pagination, threshold: 5, interval: 1.minute,   description: 'Rate limit for Ticket index deep pagination'
Prop.configure :v2_tags_index, threshold: 100000,         interval: 1.minute,   description: 'Rate limit for Tags index'
Prop.configure :v2_tags_index_pagination, threshold: 100, interval: 10.minutes,   description: 'Rate limit for Tags index pagination'
Prop.configure :v2_tags_index_deep_pagination, threshold: 5, interval: 1.minutes,   description: 'Rate limit for Tags index deep pagination'

Prop.configure :v2_ticket_metrics_index, threshold: 1, interval: 10.seconds, description: 'Rate limit for the v2 ticket metrics index'

# The threshold for ticket_updates is overrided by account setting `ticket_update_rate_limit`. Be sure to update the setting default value if the limit needs to be changed.
Prop.configure :ticket_updates, threshold: 30, interval: 10.minutes, description: 'Rate limit for ticket updates exceeded, please wait before updating this ticket again'

# The threshold for contextual workspaces, Orchid owns it
Prop.configure :v2_workspaces_controller, threshold: 250,       interval: 5.minutes,   description: 'Workspaces API threshold exceeded.'

# Billing team

# This prop is not to be communicated outside of Zendesk, hence it has not been added to `introduction.md`
Prop.configure :zuora_credit_card_iframe_load_by_account, threshold: 5, interval: 1.day, description: 'Hosted Payment Method load limit exceeded'
Prop.configure :zuora_credit_card_tokens, threshold: 10, interval: 1.hour, description: 'Auth billing token for payment method page limit exceeded'

# Machine Learning Data Team
Prop.configure :ticket_deflection_account, threshold: 30, interval: 1.minutes, first_throttled: true, description: 'Rate limit for ticket deflection (AnswerBot) on an account'

# Mobile SDK team
Prop.configure :push_notification_device_registrations, threshold: 1, interval: 1.second, description: 'Number of allowed SDK push notifications devices registration per second exceeded'
Prop.configure :push_notification_device_registrations_1h, threshold: 1, interval: 1.hour, description: 'Number of allowed SDK push notifications devices registration per hour exceeded'
Prop.configure :push_notification_device_deregistrations, threshold: 1, interval: 15.minutes, description: 'Number of allowed SDK push notification device deletion per 15 minutes exceeded'
Prop.configure :push_notification_device_deregistrations_per_account, threshold: 100, interval: 1.second, description: 'Number of allowed SDK push notification device deletions per account per second exceeded'
Prop.configure :mobile_sdk_uploads_per_device, threshold: 60, interval: 1.minutes, description: 'Number of failed uploads per minute exceeded'

rate_limit_message = 'Number of allowed requests per minute exceeded'
Prop.configure :mobile_sdk_limit_low, threshold: 500, interval: 1.minute, description: rate_limit_message
Prop.configure :mobile_sdk_limit_medium, threshold: 2000, interval: 1.minute, description: rate_limit_message
Prop.configure :mobile_sdk_limit_high, threshold: 5000, interval: 1.minute, description: rate_limit_message
Prop.configure :mobile_sdk_emergency_limit, threshold: 10000, interval: 1.minute, description: rate_limit_message

# Abuse Team
Prop.configure :fraud_service_requests,               threshold: 200,       interval: 1.hours,    description: 'Rate limit for hitting the account fraud service'

# Views(Vortex) Team
Zendesk::Rules::ViewsRateLimiter.configure(:count_many, :view_find, :view_count, :preview_find, :preview_count, :view_play, :view_tickets)

# Bolt Team
Prop.configure :index_users_with_deep_offset_pagination_ceiling_limit, threshold: 100, interval: 1.minute, description: 'Ceiling rate limit for Users#index endpoint accessing records with offset greater than 1,000,000'

Prop.configure :index_organizations_with_deep_offset_pagination_ceiling_limit, threshold: 100, interval: 1.minute, description: 'Ceiling rate limit for Organizations#index endpoint accessing records with offset greater than 1,000,000'

Prop.configure :index_deleted_users_with_deep_offset_pagination_ceiling_limit, threshold: 100, interval: 1.minute, description: 'Ceiling rate limit for DeletedUsers#index endpoint accessing records with offset greater than 1,000,000'

Prop.configure :index_group_memberships_with_deep_offset_pagination_ceiling_limit, threshold: 100, interval: 1.minute, description: 'Ceiling rate limit for GroupMemberships#index endpoint accessing records with offset greater than 1,000,000'

Prop.configure :index_organization_memberships_with_deep_offset_pagination_ceiling_limit, threshold: 100, interval: 1.minute, description: 'Ceiling rate limit for OrganizationMemberships#index endpoint accessing records with offset greater than 1,000,000'

# Ngiyari/Sandbox Team
Prop.configure :sandbox_creates, threshold: 10, interval: 1.hour, description: 'Number of sandbox creation requests exceeded'
Prop.configure :sandboxes_indexes, threshold: 90, interval: 1.minute, description: 'Number of sandboxes index requests exceeded'

# rubocop:enable Layout/ExtraSpacing
