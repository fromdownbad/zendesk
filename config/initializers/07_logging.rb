if LOG_JSON_TO_STDOUT_ONLY || Rails.env.master? || Rails.env.staging?
  ENV['ZENDESK_CLASSIC_STDOUT_JSON_LOGGING'] = '1'
end

require 'zendesk/unicorn_util'
require 'zendesk_logging'
require 'zendesk/log_rotator'

no_file_logs =
  ENV['SKIP_EXTRA_LOG_FILES'] ||
  LOG_JSON_TO_STDOUT_ONLY ||
  (Rails.env.production? && STDIN.tty?)

TRIGGER_LOG = ActiveSupport::Logger.new(no_file_logs ? '/dev/null' : "#{Rails.root}/log/trigger.log")
ZUORA_LOG   = ActiveSupport::Logger.new(no_file_logs ? '/dev/null' : "#{Rails.root}/log/zuora.log")
CONDITIONAL_LOG = ActiveSupport::Logger.new(no_file_logs ? '/dev/null' : "#{Rails.root}/log/conditional.log")

Rails.logger.extend(Zendesk::Logging::Collation)

Zendesk::LogRotator.try_listen unless Rails.env.test?

# This is essentially what Rails does
# when outputting to logs
module UploadedFileJson
  def as_json(_options = {})
    inspect
  end
end

Rack::Test::UploadedFile.send(:include, UploadedFileJson) if defined?(Rack::Test::UploadedFile)
ActionDispatch::Http::UploadedFile.send(:include, UploadedFileJson)

module Codeowner
  def self.resolve_controller(klass)
    filename = locate_controller(klass)
    # Unowned controllers are marked @zendesk/support-review
    codeowners.key?(filename) ? codeowners[filename] : "@zendesk/support-review"
  end

  def self.codeowners
    @codeowners ||= YAML.load(File.read("#{Rails.root}/.github/codeowners.yml")).freeze
  end

  def self.locate_controller(klass)
    "app/controllers/#{klass.name.underscore}.rb"
  end
end
