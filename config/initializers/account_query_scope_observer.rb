TRACK_QUERIES_WITHOUT_ACCOUNT_ID = ENV['TRACK_QUERIES_WITHOUT_ACCOUNT_ID'] == 'true'
LOG_QUERIES_WITHOUT_ACCCOUNT_ID = ENV['LOG_QUERIES_WITHOUT_ACCCOUNT_ID'] == 'true'

if TRACK_QUERIES_WITHOUT_ACCOUNT_ID
  module AccountIDQueryTracking
    # A super naive approach to determine if a query includes the account id column. This is pretty dumb
    # and only checks to see if the string `account_id` after the word `WHERE` in a query. Not great,
    # but accurate in the 99% case for ActiveRecord
    ACCOUNT_ID_MATCHER = /WHERE.*account_id/i.freeze

    def self.log?(sql)
      return false if sql == SELECT_1
      return false if sql.match?(ARTURO_QUERY)
      return false if sql.match?(ESC_MESSAGE)

      sql.match?(INTERESTING_STATEMENT) && sql.match?(SHARDED_TABLES)
    end

    SELECT_1 = 'select 1'.freeze
    ARTURO_QUERY = /arturo_features/i.freeze
    ESC_MESSAGE = /esc_kafka_messages/i.freeze
    INTERESTING_STATEMENT = /^\s*(SELECT|INSERT|UPDATE|REPLACE)\s+/i.freeze
    SHARDED_TABLES = Regexp.union(
      ActiveRecord::Base.on_first_shard { ActiveRecord::Base.connection.tables }
    ).freeze

    private_constant(:SELECT_1, :ARTURO_QUERY, :ESC_MESSAGE, :INTERESTING_STATEMENT, :SHARDED_TABLES)
  end

  ActiveSupport::Notifications.subscribe('sql.active_record') do |_name, _started, _finished, _unique_id, data|
    # TODO: we should have an explicit check to see if it's a sharded connection. Right now we only
    # have `data[:connection_id]` but I'm not sure how to go from a connection_id back to a connection
    # to see if it's sharded. On top of that, the connection_id is removed from the data payload in
    # newer versions of Rails, so we shouldn't rely on it.

    if AccountIDQueryTracking.log?(data[:sql])
      includes_account = data[:sql]&.match?(AccountIDQueryTracking::ACCOUNT_ID_MATCHER)

      if !includes_account && LOG_QUERIES_WITHOUT_ACCCOUNT_ID
        Rails.logger.debug("[sql.active_record]: Statement without account_id! | #{data[:sql]}")
      end

      # rubocop:disable Style/GlobalVars
      $statsd.increment(
        'account_scoped_query',
        tags: %W[includes_account:#{includes_account}]
      )
      # rubocop:enable Style/GlobalVars
    end
  end
end
