IronBank.configure do |config|
  config.auth_type = ENV.fetch("BILLING_ZUORA_AUTH_TYPE", "token")

  # We use two pairs of credential for Zuora. One is based of Zuora OAuth and is
  # the preferred way of interacting with Zuora as per their Developer Center.
  #
  # The other pair is username/password (same as our SOAP client) since we have
  # had two incidents in July 2019 with Zuora OAuth. This let us quickly use one
  # or the other, based on the authentication type.
  #
  # Simply edit the ENV of BILLING_ZUORA_AUTH_TYPE to swap credentials in case
  # of such incident.
  if config.auth_type == "token"
    config.client_id     = ENV["BILLING_ZUORA_OAUTH_CLIENT_ID"]
    config.client_secret = ENV["BILLING_ZUORA_OAUTH_CLIENT_SECRET"]
  else
    config.client_id     = ENV["BILLING_ZUORA_API_USERNAME"]
    config.client_secret = ENV["BILLING_ZUORA_API_PASSWORD"]
  end

  config.domain               = ENV["BILLING_ZUORA_REST_API_DOMAIN"]
  config.excluded_fields_file = ENV["BILLING_ZUORA_EXCLUDED_FIELDS_FILE"]
  config.export_directory     = ENV["BILLING_ZUORA_LOCAL_EXPORT_DIRECTORY"]
  config.schema_directory     = ENV["BILLING_ZUORA_SCHEMA_DIRECTORY"]
  config.logger               = Rails.logger
end
