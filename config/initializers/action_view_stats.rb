require 'active_support/notifications'

module ActionViewStats
  EVENTS = %w[render_template.action_view render_partial.action_view].freeze

  class << self
    def subscribe
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['classic.app'])
      @subscriptions ||= []

      EVENTS.map do |event|
        @subscriptions << ActiveSupport::Notifications.subscribe(event) do |*args|
          e = ActiveSupport::Notifications::Event.new(*args)
          record_event(e.name, e.payload[:identifier])
        end
      end
    end

    def unsubscribe
      @subscriptions.each do |subscription|
        ActiveSupport::Notifications.unsubscribe(subscription)
      end
      @subscriptions = []
    end

    private

    PROTOTYPE_RAILS_USERS = [
      "tickets/_ticket.html.erb",
      "watchings/_subscribe.html.erb",
      "people/users/_basic_info.html.erb",
      "settings/account/_logo_upload.html.erb",
      "shared/_attachment_item.html.erb",
      "tags/show.html.erb",
      "people/users/merge.html.erb",
      "tickets/merge/show.html.erb",
    ].freeze

    def record_event(event_name, file_name)
      file_name = split_filename(file_name)
      tags = ["event_name:#{event_name}", "file:#{file_name}",
              "top_level_folder:#{top_level_folder(file_name)}",
              "prototype_rails:#{PROTOTYPE_RAILS_USERS.include?(file_name)}"]

      @statsd_client.increment("as_notify.action_view", tags: tags)
    end

    def split_filename(fname)
      fname.split("app/views/").last rescue "error"
    end

    def top_level_folder(fname)
      fname.split("/").first rescue "unknown"
    end
  end
end

ActionViewStats.subscribe
