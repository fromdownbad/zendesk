require 'zendesk_core_application/recaptcha'

Recaptcha.configure do |config|
  config.api_server_url = 'https://www.recaptcha.net/recaptcha/api.js'
  config.verify_url = 'https://www.recaptcha.net/recaptcha/api/siteverify'
end

unless Rails.env.production?
  # make 'testing' a valid response so automated tests can use it,
  # for example:
  #
  # https://github.com/zendesk/zendesk_browser_tests/blob/master/spec/features/help_center/browser/authentication/sign_up_spec.rb
  ZendeskCoreApplication::Recaptcha.class_eval do
    def verify_recaptcha(*args)
      if params[:recaptcha_response_field] == 'testing' || params['g-recaptcha-response'] == 'testing'
        logger.info("Recaptcha is valid because verification code is 'testing'")
        return true
      end
      return false if recaptcha_rate_limited?
      super
    end
  end
end

module Zendesk::RecaptchaExtensions
  def verify_recaptcha(options = {})
    super(options.merge(private_key: Zendesk::Configuration.dig(:recaptcha_v2, :secret_key), hostname: request.host))
  end
end
ZendeskCoreApplication::Recaptcha.send(:prepend, Zendesk::RecaptchaExtensions)
