require 'zendesk/permanent_cookie/permanent_cookie'
require 'zendesk/auth/warden/callbacks'

ActiveSupport::Notifications.subscribe('zendesk.auth.maximum_login_attempts_exceeded') do |*args|
  options = args.extract_options!

  user = options[:user]
  account = user.account
  key = [account.id, user.id, options[:ip]]

  if account.try(:has_security_email_notifications_for_password_strategy?) && user.is_agent? && Rails.cache.read(key).nil?
    SecurityMailer.deliver_maximum_login_attempts_exceeded(user)
    Rails.cache.write(key, true, expires_in: 1.day)
  end
end
