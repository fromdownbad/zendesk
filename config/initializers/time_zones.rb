# ActiveSupport::TimeZone.[] adds valid time zone aliases to an internal map.
#
#     ActiveSupport::TimeZone["Europe/London"]
#     ActiveSupport::TimeZone["Europe/Copenhagen"]
#
# ActiveSupport::TimeZone.all memoizes the zone_map the first time it is called.
# Depending on what time zones our users have stored in the database, and which
# users hit a certain Rails process, the zone_map will get populated with some
# random list of time zone aliases.
#
#     ActiveSupport::TimeZone.all.size # => 144
#
#     ActiveSupport::TimeZone["Europe/London"]
#     ActiveSupport::TimeZone.all.size # => 145
#
#     ActiveSupport::TimeZone["Europe/London"]
#     ActiveSupport::TimeZone["Europe/Copenhagen"]
#     ActiveSupport::TimeZone.all.size # => 146
#
# This initializer memoizes the zone_map as soon as the process starts,
# preventing additional zone aliases from being added to the map.
#
ActiveSupport::TimeZone.all
