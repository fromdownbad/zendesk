Zendesk::ReturnTo::Parser.redirector = lambda do |account, user, url|
  Zendesk::LotusRedirector.redirect(account, user, Addressable::URI.parse(url))
end
