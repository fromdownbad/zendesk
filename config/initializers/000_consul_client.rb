# This initializer is intentionally the first one so we can use the consul client
# in the rest of the initializers if desired.
require 'diplomat'

Diplomat.configure do |config|
  config.url = "http://" + ENV.fetch('CONSUL_HTTP_ADDR')
end
