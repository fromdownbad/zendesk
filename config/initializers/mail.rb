module Zendesk
  # Tested via test/unit/mailers/application_mailer_test.rb
  module PreventEmailsToInvalidDomains
    def self.delivering_email(mail)
      to = [*mail.to].last.to_s

      # Ignore outbound mail for invalid or RFC reserved domains.
      if to =~ /mailer-daemon/i || (!test_env? && to =~ /@(.*\.)?example\.(com|net|org)$/i)
        Rails.logger.warn("Ignoring outbound mail for invalid or RFC reserved domain #{mail.to}")
        mail.perform_deliveries = false
      end
    end

    def self.test_env?
      Rails.env.development? || Rails.env.test?
    end
  end

  # Tested via test/unit/jobs/mail_rendering_job_test.rb
  module PreventEmailsToNobody
    def self.delivering_email(mail)
      return if [mail.to, mail.cc, mail.bcc].compact.flatten.any?

      Rails.logger.warn("Not sending empty mail, since Mail would raise an exception.")
      mail.perform_deliveries = false
    end
  end
end

ActionMailer::Base.register_interceptors(
  [
    Zendesk::PreventEmailsToInvalidDomains,
    Zendesk::PreventEmailsToNobody
  ]
)
