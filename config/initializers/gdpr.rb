require 'zendesk/gdpr/configuration'

if Rails.env.development? || Rails.env.test?
  # These are configured in config/.env.gdpr.development and .env (copy from .env.example):
  Zendesk::Gdpr::Configuration.setup do
    {
      'aws_access_key' => ENV['GDPR_AWS_ACCESS_KEY'] || 'your aws access key',
      'aws_secret_key' => ENV['GDPR_AWS_SECRET_KEY'] || 'your aws secret key',
      'aws_session_token' => ENV['GDPR_AWS_SESSION_TOKEN'] || 'your aws session token',
      'sqs_support_queue_url' => ENV['GDPR_SQS_SUPPORT_QUEUE_URL'] || 'https://sqs.aws.example.com',
      'development_namespace' => ENV['GDPR_DEVELOPMENT_NAMESPACE'] || 'your_github_username',
      'region' => ENV['GDPR_REGION'] || 'us-west-2',
      'topic_arn' => ENV['GDPR_TOPIC_ARN'] || 'this_is_cool',
      'audit_feedback_topic_arn' => ENV['GDPR_AUDIT_FEEDBACK_TOPIC_ARN'] || 'this_is_fun',
      'sqs_audit_feedback_queue_url' => ENV['GDPR_SQS_AUDIT_FEEDBACK_QUEUE_URL'] || 'https://sqs.aws.example.com',
      'sns_endpoint' => ENV['GDPR_SNS_ENDPOINT']
    }
  end
else
  region = ENV.fetch('GDPR_CONFIG_REGION')
  pod = ENV.fetch('ZENDESK_POD_ID')

  configurator = ->(entry, config) do
    key = entry[:key].split('/').last
    config[key] = entry[:value]
  end

  global_settings = Diplomat::Kv.get('global/gdpr/global', recurse: true).each_with_object({}, &configurator)
  regional_settings = Diplomat::Kv.get("global/gdpr/#{region}", recurse: true).each_with_object({}, &configurator)
  pod_settings = Diplomat::Kv.get("global/gdpr/#{pod}", recurse: true).each_with_object({}, &configurator)

  Zendesk::Gdpr::Configuration.setup do
    global_settings.merge(regional_settings.merge(pod_settings))
  end
end
