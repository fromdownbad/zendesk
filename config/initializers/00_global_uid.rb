# If you want to know more about how GlobalUid works:
#   https://github.com/zendesk/global_uid#summary

databases = Rails.application.config.database_configuration.keys
id_servers = databases.grep(/^#{Rails.env}_id_server/).presence || databases.grep(/^id_server/).presence
statsd_client = proc { @global_uid_statsd ||= Zendesk::StatsD::Client.new(namespace: :global_uid) }

# The increment_by and offset values we use in production are set by the DBA team
# on the alloc servers, defined here:
#   https://zendesk.atlassian.net/wiki/spaces/ops/pages/52658202/Production+Alloc+Servers
# The values we use in dev and test are defined by zendesk_configuration:
#   https://github.com/zendesk/zendesk_configuration/blob/43e8a0fe4bad2acd2fbd265aba59683aaa291b7f/lib/zendesk/configuration/db_config.rb#L173-L184

# Zendesk::Configuration will look for environment variables prefixed
# with ZENDESK_GLOBAL_UID_OPTIONS_ or are defined in config/zendesk.yml.
global_uid_options = Zendesk::Configuration.fetch(:global_uid_options).transform_keys!(&:to_sym)

GlobalUid.configure do |config|
  config.disabled = global_uid_options[:disabled] if global_uid_options.key?(:disabled)

  config.id_servers = id_servers
  config.increment_by = global_uid_options[:increment_by] if global_uid_options.key?(:increment_by)

  config.connection_timeout = global_uid_options[:connection_timeout] if global_uid_options.key?(:connection_timeout)
  config.connection_retry = global_uid_options[:connection_retry] if global_uid_options.key?(:connection_retry)
  config.query_timeout = global_uid_options[:query_timeout] if global_uid_options.key?(:query_timeout)

  config.notifier = lambda do |exception|
    begin
      Rails.logger.error("[global_uid] GlobalUID error: #{exception.class} #{exception.message}")
      Rollbar.error(exception)
      # Metrics go to the DBA owned auto_increment error dashboard (https://zendesk.datadoghq.com/dashboard/xpi-yud-8s7)
      statsd_client.call.increment("error", tags: ["service:classic", "exception_class:#{exception.class.name.demodulize.underscore}"])
    rescue StandardError => e
      Rails.logger.error("[global_uid] Unexpected error while performing GlobalUidErrorHandler.call: #{e}")
    end
  end
end
