if Rails.env.test?
  GIT_HEAD_SHA = 'abcdefghijklmnop'.freeze
  GIT_HEAD_TAG = 'ci-test'.freeze
else
  GIT_HEAD_SHA =
    ENV['REVISION'] || # the ideal case, should get set whenever possible. set by samson for k8s deploys
    (File.exist?(Rails.root.join('REVISION')) && File.read(Rails.root.join('REVISION')).strip) || # pre-k8s production case, file uploaded by samson
    (File.exist?('/REVISION') && File.read('/REVISION').strip) || # zdi case, file added to docker container by GCB or zdi dev mode
    ''

  GIT_HEAD_TAG =
    ENV['TAG'] || # the ideal case, should get set whenever possible. set by samson for k8s deploys
    (File.exist?(Rails.root.join('TAG')) && File.read(Rails.root.join('TAG')).strip) || # pre-k8s production case, file uploaded by samson
    GIT_HEAD_SHA[0...7] # safety net so the TAG variable always has a value
end

GIT_TIMESTAMPS = if Rails.env.development?
  # In development, assets can change after the app boots so allow the lookups to change over time
  Hash.new { Time.now }
else
  # record boot time so it does not change everytime the hash lookup is performed
  Hash.new(Time.now.freeze)
end

ENV['RAILS_ASSET_ID'] = GIT_TIMESTAMPS['public'].to_i.to_s unless Rails.env.development?

# ensure that Zendesk::Configuration.fetch(:application_version) returns an expected version case
ENV['ZENDESK_APPLICATION_VERSION'] = GIT_HEAD_TAG
