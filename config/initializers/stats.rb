require 'zendesk_stats'
require 'zendesk_classic_stats'
require 'help_center_stats'
require 'zendesk/stats/consul_to_stats_yml_adapter'

# The redis sentinel name MUST use hyphens instead of underscores. The servers are registered in
# consul both as `cluster-foo_bar` and `cluster-foo-bar`, but within redis itself, all of the
# sentinels have a master name with only hyphens. Incorrectly setting the master_name in the
# sentinel config will result in an infinite retry loop and request timeouts.
if KUBERNETES || ENV['STATS_REDIS_TYPE'] == 'sentinel' || Zendesk::Configuration.config_from_env?('stats')
  cluster_name = ENV['STATS_REDIS_CLUSTER'] || "stats-queue-#{Zendesk::Configuration.fetch(:pod_id)}"

  options = {}
  options[:password] = ENV['STATS_REDIS_PASSWORD'] if ENV['STATS_REDIS_PASSWORD']
  options[:ssl] = ENV['STATS_REDIS_USE_SSL'].to_s.casecmp('true').zero?

  Zendesk::Stats::Queue.redis = Zendesk::RedisFactory.create_from_consul(cluster_name, options)
  Zendesk::Stats::Config.set_config(Zendesk::Stats::ConsulToStatsYmlAdapter.config)
else
  config_file = File.expand_path("../stats.yml", File.dirname(__FILE__))
  config_hash = YAML.load(ERB.new(File.read(config_file)).result)[Rails.env]

  raise "No stats config set for '#{Rails.env}' environment" unless config_hash

  Zendesk::Stats::Config.set_config(config_hash)
end

# load up zendesk-application-local stats
["ticket", "help_center", "rollup_job"].each do |f|
  require Rails.root.join("lib/zendesk/stats/#{f}.rb")
end

Zendesk::Stats.logger = Rails.logger # RESCUE_LOGGER from resque.rb?
Mongo::Logger.logger = Rails.logger
