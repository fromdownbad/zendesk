# https://github.com/zendesk/help_center/blob/4c0dd2f8/config/initializers/04_uploader_config.rb
HC_UPLOADER_ENV = {} # rubocop:disable Style/MutableConstant

if ENV.key?('UPLOADER_STORAGE')
  HC_UPLOADER_ENV.merge!(
    storage: ENV.fetch('UPLOADER_STORAGE').to_sym,
    bucket_name: ENV.fetch('UPLOADER_BUCKET_NAME'),
    region: ENV.fetch('UPLOADER_REGION'),
    aws_access_key_id: ENV.fetch('UPLOADER_AWS_ACCESS_KEY_ID'),
    aws_secret_access_key: ENV.fetch('UPLOADER_AWS_SECRET_ACCESS_KEY')
  )
end

if ENV.key?('REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY')
  HC_UPLOADER_ENV.merge!(
    replicated_bucket_name: ENV.fetch('REPLICATED_UPLOADER_BUCKET_NAME'),
    replicated_region: ENV.fetch('REPLICATED_UPLOADER_REGION'),
    replicated_aws_access_key_id: ENV.fetch('REPLICATED_UPLOADER_AWS_ACCESS_KEY_ID'),
    replicated_aws_secret_access_key: ENV.fetch('REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY')
  )
end

HC_UPLOADER_ENV.freeze
