require 'zuora_client'
require 'zendesk_billing_core'

# Logging
ZuoraClient.logger        = ZUORA_LOG
ZuoraClient.logger.level  = Logger::INFO

# Caching
ZuoraClient.cache         = Rails.cache
ZuoraClient.cache_options = { expires_in: 24.hours }

# NOTE: Monkey-patching `ZuoraClient::Session.client` to create the Savon client
#       using the `:net_http` adapter instead of the default `:httpclient`
#       (which uses its own outdated CA certificates.)
#
# TODO: Remove this monkey-patch after ZuoraClient has been updated to specify
#       the `:net_http` adapter.
module ZuoraClient
  class Session
    def client
      @client ||= ::Savon.client(
        ssl_version:      :TLSv1_2,
        wsdl:             wsdl_location,
        namespaces:       STANDARD_NAMESPACES,
        soap_header:      request_headers,
        pretty_print_xml: false,
        log_level:        default_log_level,
        adapter:          :net_http
      )
    end
  end
end

# Exported Product Catalog
catalog_path = ENV.fetch('BILLING_ZUORA_PRODUCT_CATALOG_PATH')
ZuoraClient.exported_pricing_folder = Rails.root.join(catalog_path)

# Silence HTTPI
HTTPI.logger.level = Logger::FATAL

# NOTE: We no longer need to inherit from Zendesk::Configuration to depend on
# the zuora.yml file for the configuration *except* that we still use this
# pattern in the dummy application for test purposes. We rely on environment
# variables in master, staging and production.
#
# See https://zendesk.atlassian.net/browse/BILL-4648
#
if Rails.env.development? || Rails.env.test?
  ZBC::Zuora::Configuration.setup(
    file: Rails.root.join('config/zuora.yml'),
    env:  Rails.env
  )
else
  ZBC::Zuora::Configuration.setup(ENV: true, env: Rails.env)
end

class ZBC::Zuora::Configuration
  def self.env_prefix
    'billing_zuora'
  end
end
