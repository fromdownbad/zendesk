# This fixes uninitizlized constant errors from an incorrect
# load order of the Google Protobuf constants
require 'protobuf/init'
