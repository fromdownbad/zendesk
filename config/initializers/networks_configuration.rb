Zendesk::Net::Configuration.setup

if ENV['NETWORKS_CONFIG_PATH']
  class << Zendesk::Net::Configuration
    def config_from_env?(_)
      false
    end

    private

    def config_path
      ENV['NETWORKS_CONFIG_PATH']
    end
  end

  # Call setup again to use the updated config path
  Zendesk::Net::Configuration.setup
end
