require 'liquid/c'
require 'liquid/template'

Liquid::Template.register_filter(Zendesk::Liquid::CustomFilter)

module Liquid
  module C
    class << self
      def enabled
        Arturo.feature_enabled_for_pod?(:liquid_c, Zendesk::Configuration.fetch(:pod_id))
      end
    end
  end
end

module Liquid
  module Extensions
    module HtmlSafety
      class << self
        def escape_html(text)
          text.html_safe? ? text : CGI.escapeHTML(text.to_s).html_safe
        end

        # TODO: You'll sometimes see collections here, e.g. {{ticket.comments}}. Need to handle that.
        def escape?(text)
          @enabled && text.present? &&
            (text.is_a?(String) || text.is_a?(Zendesk::Liquid::Wrapper))
        end

        def with_enabled(value)
          old = @enabled
          @enabled = value
          yield
        ensure
          @enabled = old
        end
      end
    end
  end

  class Variable
    def render_with_html_safety(liquid_context)
      # Remove filters from this Liquid::Variable that introduce HTML. We handle formatting elsewhere.
      filters.reject! { |filter| filter.first == "newline_to_br" }

      rendered_text = render_without_html_safety(liquid_context)
      return rendered_text unless Liquid::Extensions::HtmlSafety.escape?(rendered_text)

      if rendered_text.respond_to?(:html_safe?)
        Liquid::Extensions::HtmlSafety.escape_html(rendered_text)
      else
        rendered_text
      end
    end
    alias_method :render_without_html_safety, :render
    alias_method :render, :render_with_html_safety
  end
end

##
# Delegation is different in rails 4 vs rails 5 that prevents the
# to_liquid method from being callable. This pops in a `to_liquid`
# method directly that does the equivalent of rails 4's
# `array_delegable?` branch of method_missing.

module Rails5LiquidHack
  def to_liquid
    to_a.to_liquid
  end
end

if RAILS5
  ActiveRecord::AssociationRelation.prepend Rails5LiquidHack
  ActiveRecord::Associations::CollectionProxy.prepend Rails5LiquidHack
end
