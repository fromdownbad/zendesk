require 'remote_files'
require 'resque-retry'
require 'timeout'

REMOTE_FILES_CONFIG = if KUBERNETES || Zendesk::Configuration.config_from_env?('remote_files')
  Diplomat::Kv.
    get("global/classic/remote_files/#{Zendesk::Configuration.fetch(:pod_id)}", transformation: JSON.method(:load)).
    transform_values do |model_config|
      model_config.transform_values do |regional_config|
        regional_config.merge(
          'aws_access_key_id' => ENV.fetch('REMOTE_FILES_AWS_ACCESS_KEY'),
          'aws_secret_access_key' => ENV.fetch('REMOTE_FILES_AWS_SECRET_KEY')
        )
      end
    end
else
  YAML.
    load(
      ERB.new(
        Rails.root.join('config/remote_files.yml').read
      ).result
    ).
    fetch(Rails.env)
end.freeze

# we can lose this once all config files have been converted
unless REMOTE_FILES_CONFIG.key?('inbound_email_attachments')
  REMOTE_FILES_CONFIG = {
    'inbound_email_attachments' => REMOTE_FILES_CONFIG,
    'inbound_email'             => REMOTE_FILES_CONFIG
  }.freeze
end

REMOTE_FILES_CONFIG.each do |name, config|
  RemoteFiles.configure(name.to_sym, config)
end

module RemoteFiles
  class RetryingResqueJob
    extend ZendeskJob::Resque::BaseJob
    extend Resque::Plugins::ExponentialBackoff

    priority :cloud_storage

    def self.args_to_log(account_id, action, options, durable_audit_id = nil)
      { account_id: account_id, action: action, options: options, audit_id: durable_audit_id }
    end

    def self.work(_account_id, action, options, _durable_audit_id = nil)
      options    = options.dup
      identifier = options.delete(:identifier) || options.delete("identifier")

      file = RemoteFiles::File.new(identifier, options)

      case action.to_sym
      when :synchronize
        Timeout.timeout(60 * 30, Timeout::Error) { file.synchronize! }
      when :delete
        begin
          Timeout.timeout(60, Timeout::Error) { file.delete_now! }
        rescue NotFoundError
          nil
        end
      else
        raise "unknown action #{action.inspect}"
      end
    end

    def self.backoff_strategy
      @backoff_strategy ||= [0, 60, 600, 3600, 10_800, 21_600, 86_400]
    end
  end

  # This, along with the durable_audit_id argument, can be removed after code begins running in production.
  DurableResqueJob = RetryingResqueJob

  synchronize_stores do |file|
    RetryingResqueJob.enqueue(file.options[:account_id], :synchronize,
      identifier: file.identifier,
      configuration: file.configuration.name,
      stored_in: file.stored_in)
  end

  delete_file do |file|
    RetryingResqueJob.enqueue(file.options[:account_id], :delete,
      identifier: file.identifier,
      configuration: file.configuration.name,
      stored_in: file.stored_in)
  end
end
