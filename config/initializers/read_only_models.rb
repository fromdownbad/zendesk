require 'zendesk/read_only_model'

Zendesk::ReadOnlyModel.enabled = !Rails.env.development? && !Rails.env.test?
