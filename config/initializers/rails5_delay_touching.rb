if RAILS5
  module ActiveRecord
    class Base
      class << self
        def delay_touching
          yield
        end
      end
    end
  end
end
