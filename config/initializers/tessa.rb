require_relative '../../lib/tessa/tessa_cache_updater'

# To reduce the load in development, the updater by default will not run unless you use the
# following command:
#
# zdi zendesk -d restart -e TESSA_CACHE_UPDATER=1
#
USE_TESSA = ENV.fetch('USE_TESSA', 'false') == 'true'

production_or_staging = Rails.env.production? || Rails.env.staging?

cache_updater_in_development = Rails.env.development? && ENV['TESSA_CACHE_UPDATER']

not_console = !defined?(Rails::Console)

if not_console && (production_or_staging || cache_updater_in_development)
  # All projects that use TessaClient to fetch "current" manifests should be listed here.
  projects = [:lotus]

  TessaCacheUpdater::V1::Runner.new(projects).run
end
