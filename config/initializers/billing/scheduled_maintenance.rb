# This middleware allows us to put Billing in maintenance mode.
# Consult the Billing project's README's Maintenance Mode section for details.
Rails.application.config.middleware.use Billing::ScheduledMaintenance
