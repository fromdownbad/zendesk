ZendeskHealthCheck::Diagnostic::Config.setup do |config|
  if ENV['APP_NAME'] == 'mail-ticket-creator'
    config.diagnostic_registry.
      add(ZendeskHealthCheck::HealthChecks::ActiveRecordDBDiagnostic).
      add(ZendeskHealthCheck::HealthChecks::RailsCacheDiagnostic).
      add(ZendeskHealthCheck::HealthChecks::ResqueDiagnostic).
      add(ZendeskHealthCheck::HealthChecks::ConsulDiagnostic).
      add(Health::Diagnostic::MysqlMasterDiagnostic).
      add(Health::Diagnostic::MysqlShardDiagnostic).
      add(Health::Diagnostic::RedisStoreDiagnostic)

    config.monitoring.enable(client: Zendesk::InboundMail::StatsD.client)
  else
    config.diagnostic_registry.
      add(ZendeskHealthCheck::HealthChecks::ActiveRecordDBDiagnostic).
      add(ZendeskHealthCheck::HealthChecks::RailsCacheDiagnostic).
      add(ZendeskHealthCheck::HealthChecks::ResqueDiagnostic).
      add(ZendeskHealthCheck::HealthChecks::ConsulDiagnostic).
      add(ZendeskHealthCheck::HealthChecks::VoiceServerStatusDiagnostic, mandatory: false).
      add(Health::Diagnostic::MysqlMasterDiagnostic).
      add(Health::Diagnostic::MysqlShardDiagnostic).
      add(Health::Diagnostic::RedisStoreDiagnostic).
      add(Health::Diagnostic::SearchDiagnostic, mandatory: false).
      add(Health::Diagnostic::StaffServiceDiagnostic).
      add(Health::Diagnostic::ZuoraDiagnostic).
      add(Health::Diagnostic::I18nDownloaderDiagnostic, mandatory: false).
      add(Health::Diagnostic::SupportRuleAdminResourceDiagnostic, mandatory: false).
      add(Health::Diagnostic::SupportRoutingAdminResourceDiagnostic, mandatory: false).
      add(Health::Diagnostic::DecoDiagnostic, mandatory: false).
      add(Health::Diagnostic::TessaDiagnostic, mandatory: USE_TESSA).
      add(Health::Diagnostic::AccountServiceDiagnostic, mandatory: false).
      add(Health::Diagnostic::VoyagerDiagnostic, mandatory: false).
      add(Health::Diagnostic::PigeonDiagnostic, mandatory: false).
      add(Health::Diagnostic::ElastiCacheDiagnostic, mandatory: false).
      add(Health::Diagnostic::UrbanAirshipDiagnostic, mandatory: false).
      add(Health::Diagnostic::SamsonSecretsPullerDiagnostic, mandatory: false).
      add(Health::Diagnostic::RadarRedisDiagnostic, mandatory: true).
      add(Health::Diagnostic::TwilioDiagnostic, mandatory: false).
      add(Health::Diagnostic::SalesforceIntegrationDiagnostic, mandatory: false).
      add(Health::Diagnostic::SatisfactionPredictionAppDiagnostic, mandatory: false).
      add(Health::Diagnostic::AccountFraudServiceDiagnostic, mandatory: false).
      add(Health::Diagnostic::GooddataConnectivityDiagnostic, mandatory: false).
      add(Health::Diagnostic::ZendeskArchiveRiakKvDiagnostic, mandatory: false)
  end
end
