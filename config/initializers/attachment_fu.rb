require 'attachment_fu'
require 'technoweenie/attachment_fu/scoped_stores'

configmap_file = Rails.root.join('config/attachments.d/attachments.yml')
legacy_yaml_file = Rails.root.join('config/attachments.yml')
s3_store = ->(store, _config) { store.start_with?('s3') }

base_stores =
  if %w[development test].include?(Rails.env)
    [
      {
        store_name: 'fs',
        storage: 'file_system',
        default: true
      },
      {
        store_name: 's3',
        storage: 's3',
        default: false,
        s3_access_key: 'access_key',
        s3_secret_key: 'secret_key',
        s3_distribution_domain: 'domain.cloudfront.net',
        bucket_name: 'zendesk-test',
        cloudfront: true,
        secondary: true,
        endpoint: 'US-ENDPOINT',
        region: 'us-west-2'
      },
      {
        store_name: 's3eu',
        storage: 's3',
        s3_access_key: 'access_key',
        s3_secret_key: 'secret_key',
        s3_distribution_domain: 'domain.cloudfront.net',
        bucket_name: 'zendesk-test',
        cloudfront: true,
        secondary: false,
        endpoint: 'EU-ENDPOINT',
        region: 'eu-west-1'
      },
      {
        store_name: 's3apne',
        storage: 's3',
        s3_access_key: 'access_key',
        s3_secret_key: 'secret_key',
        s3_distribution_domain: 'domain.cloudfront.net',
        bucket_name: 'zendesk-test',
        cloudfront: true,
        secondary: false,
        endpoint: 'APNE-ENDPOINT',
        region: 'ap-northeast-1'
      }
    ]
  elsif KUBERNETES && ENV['USE_ATTACHMENTS_CONFIGMAP'] == 'true' && File.exist?(configmap_file)
    YAML.load_file(configmap_file).select(&s3_store).map do |_store, config|
      config.merge(
        s3_access_key: ENV.fetch('ATTACHMENT_FU_S3_ACCESS_KEY'),
        s3_secret_key: ENV.fetch('ATTACHMENT_FU_S3_SECRET_KEY')
      ).symbolize_keys
    end
  elsif KUBERNETES || Zendesk::Configuration.config_from_env?('attachments')
    Diplomat::Kv.
      get("global/attachments/#{Zendesk::Configuration.fetch(:pod_id)}", recurse: true, transformation: JSON.method(:load)).
      map do |kv_pair|
        kv_pair[:value].merge(
          s3_access_key: ENV.fetch('ATTACHMENT_FU_S3_ACCESS_KEY'),
          s3_secret_key: ENV.fetch('ATTACHMENT_FU_S3_SECRET_KEY')
        ).symbolize_keys
      end
  elsif File.exist?(legacy_yaml_file)
    YAML.load_file(legacy_yaml_file).select(&s3_store).values.map(&:deep_symbolize_keys)
  else
    raise 'No stores found for attachment_fu'
  end

::Technoweenie::AttachmentFu.stores =
  ::Technoweenie::AttachmentFu::ScopedStores.add_scoped_stores(
    stores: base_stores,
    default_to_scoped: !Rails.env.development? && !Rails.env.test?
  )
