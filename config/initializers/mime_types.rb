Mime::Type.register "text/text", :txt # redundant to rails but necessary for robots.txt?
Mime::Type.register "text/plain", :properties
Mime::Type.register_alias "text/html", :mobile_v2
Mime::Type.register "message/rfc822", :eml
Mime::Type.register "image/jpg", :jpg
Mime::Type.register "audio/mp3", :mp3
Mime::Type.register "audio/wav", :wav
