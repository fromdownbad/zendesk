require 'zendesk/arturo'

class DeprecatedFeatureError < StandardError
end

Arturo::Feature.feature_caching_strategy = Arturo::FeatureCaching::AllStrategy

Arturo.feature_reactivation_notifier = ->(f) do
  msg = "Deprecated feature #{f.symbol} was used. It has been reactivated."
  ZendeskExceptions::Logger.record(DeprecatedFeatureError.new(msg), location: self, message: msg, fingerprint: '6773f615195c3698e2d7cd037bac88a524df842e')
end
