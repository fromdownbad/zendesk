require 'zendesk_api/controller'

ZendeskApi::Controller.configure do |config|
  config.generic_error_handling = true
end
