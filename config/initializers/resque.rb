require 'resque'
require 'resque/failure/redis'
require 'resque/failure/multiple'
require 'resque-throttle'
require 'resque-lifecycle'
require 'resque/status'
require 'zendesk/redis_factory'
require 'zendesk/resque_exceptions'
require 'zendesk/redis_store'
require 'zendesk_job/resque'
require 'zendesk/extensions/resque/worker'

Resque.redis = Zendesk::RedisStore.client(:resque)

Resque::Plugins::Status::Hash.expire_in = (60 * 60) # 1 hour

# Remove options from the status updates because they are big and we don't need them
module Zendesk
  module RemoveJobWithStatusOptions
    def encode(h)
      h[:options] = {account_id: h[:options][:account_id]} if h[:options]
      super(h)
    end
  end
end
Resque::Plugins::Status::Hash.singleton_class.prepend(Zendesk::RemoveJobWithStatusOptions)

Resque.include Resque::Lifecycle

exclude_preload = %w[Slug]
Resque.before_first_fork do
  # work around an issue where resque's class loader breaks when > 2 levels deep
  Zendesk::Cms::ImportJob # rubocop:disable Lint/Void
  Zendesk::Cms::ExportJob # rubocop:disable Lint/Void

  ActiveRecord::Base.on_first_shard do
    ActiveRecord::Base.send(:subclasses).each do |k|
      next if exclude_preload.member?(k.name)
      next if k.abstract_class?
      k.columns
    end
  end
end

ZendeskJob::Resque.setup do |config|
  log_level = ["debug", "info", "warn", "error", "fatal"][Rails.logger.level]
  config.resque_logger = (Rails.env.test? ? Rails.logger.json_logger : Rails.logger.stdout_logger(log_level: log_level))

  config.statsd_client = Zendesk::StatsD::Client.new(namespace: 'classic')
end

failure_backends = [
  Zendesk::DatadogFailureReporter,
  ZendeskJob::Resque::Failure::LogFile,
  Zendesk::ResqueExceptions
]

Resque::Failure::Multiple.configure do |config|
  config.classes = failure_backends
end

if Rails.env.test?
  module Resque
    # This should *never* have been the default behavior, but
    # it was for a lot of tests.  Maintaining that...
    Resque.inline = true

    def self.with_inline
      old = Resque.inline
      Resque.inline = true
      yield
    ensure
      Resque.inline = old
    end

    def self.jobs
      @queued_jobs ||= []
    end

    def self.perform_all
      while (job = Resque.jobs.pop)
        Resque::Job.new(:high, JSON.load(job.to_json)).perform
      end
    end

    def self.push(_queue, options)
      Resque.jobs << options
    end
  end
elsif Rails.env.development?
  module Resque
    alias enqueue_without_inprocess_failover enqueue

    def enqueue(klass, *args)
      enqueue_without_inprocess_failover(klass, *args)
    rescue Errno::ECONNREFUSED
      Rails.logger.warn("COULD NOT CONNECT TO REDIS! Executing #{klass.name} locally")
      klass.perform(*args)
    end

    def self.enqueue_scheduled
      while (timestamp = Resque.next_delayed_timestamp)
        Resque::Scheduler.enqueue_delayed_items_for_timestamp(timestamp)
      end
    end

    def self.perform_all(queue_names = Resque.queues)
      Array(queue_names).each do |queue_name|
        reserve(queue_name).perform until size(queue_name) == 0
      end
    end
  end
end

Zendesk::Jobs.pod_id = Zendesk::Configuration.fetch(:pod_id)
Zendesk::Jobs.application_name = 'classic'
Zendesk::Jobs.setup

# to start resque (for all queues in alphabetical order)
# QUEUE=* rake environment resque:work

# to run the sinatra app (from the root of the application):
# RAILS_ENV=production resque-web config/initializers/resque.rb
