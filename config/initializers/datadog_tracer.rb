require 'zendesk_apm'
require 'ddtrace'
require 'ddtrace/contrib/resque/resque_job'
require 'aws-sdk-core'
require 'zendesk_job/resque'

dalli_in_use = [
  ENV['DALLI'],
  ENV['USE_DALLI_ELASTICACHE'],
  ENV['PROP_USE_ELASTICACHE'],
  ENV['KASKET_USE_ELASTICACHE'],
].include?('true')

mail_ticket_creator = ENV['APP_NAME'] == 'mail-ticket-creator'

# in words the following matches are for:
# 1 - any subdomain that starts with z#n like z3n, z4n, z5n...
# 2 - accounts named new-canary-cluster-09 or canaryexodusglobal23 or new-shard-cluster-19
# 3 - ticketsrus1379545783 or skynetaddons
# 4 - pod19, shard1234, skynet1, skynetaddons22...
is_test_account = ENV['SUBDOMAIN'].to_s.match?(/\Az\dn/i) ||
  ENV['SUBDOMAIN'].to_s.match?(/\A(new-)?canary(-(shard|cluster)-|exodusglobal)?[0-9-]+\Z/i) ||
  ENV['SUBDOMAIN'].to_s.match?(/\A(ticketsrus1379545783|skynetaddons)\Z/i) ||
  ENV['SUBDOMAIN'].to_s.match?(/(skynet|pod|shard|skynetaddons)[0-9]+/i)

Datadog.configure do |c|
  # Tracer
  c.tracer(
    hostname: Zendesk::StatsD::Configuration.fetch(:host),
    tags: {
      env: ENV['RAILS_ENV'],
      pod: ENV['ZENDESK_POD_ID'],
      cell: ENV['COMPUTE_CELL_NAME'],
      version: ENV['TAG'],
      test_account: is_test_account
    },
    debug: ENV.fetch('ZENDESK_APM_DEBUG', false)
  )

  # https://docs.datadoghq.com/tracing/trace_search_and_analytics/?tab=ruby#automatic-configuration
  c.analytics_enabled = true

  if mail_ticket_creator
    c.use :rails,
      service_name: 'mail-ticket-creator',
      database_service: 'mail-ticket-creator-mysql',
      distributed_tracing: true

    c.use :active_record, service_name: 'mail-ticket-creator-active-record'
    c.use :redis, service_name: 'mail-ticket-creator-redis'
  else
    c.use :rails,
      service_name: 'classic',
      controller_service: 'classic-rails-controller',
      cache_service: 'classic-cache',
      database_service: 'classic-mysql',
      middleware_names: true,
      distributed_tracing: true

    unless Rails.env.production?
      c.use :active_record, orm_service_name: 'classic'
    end

    c.use :rack,
      application: ::Rails.application,
      middleware_names: true,
      headers: {
        request: %w[
          If-Modified-Since
          If-None-Match
          CF-Ray
          User-Agent
          Client-Identifier
          X-Zendesk-App-Id
          Idempotency-Key
          X-Forwarded-For
          X-Real-Ip
          X-Zendesk-Proxy-Node
          X-Unity-Version
        ],
        response: %w[
          Content-Type
          Content-Length
          Content-Encoding
          X-Request-ID
          ETag
          Cache-Control
          Expires
          X-Accel-Redirect
          X-Rate-Limit
          X-Rate-Limit-Remaining
          Retry-After
          X-Idempotency-Lookup
          ZENDESK-EP
        ]
      },
      request_queuing: false, # we manually compute this time, we don't want the change in root span
      analytics_enabled: true

    c.use :faraday,
      service_name: 'classic-faraday',
      distributed_tracing: true,
      analytics_enabled: true

    c.use :redis, service_name: 'classic-redis'
    c.use :aws, service_name: 'classic-aws', analytics_enabled: true
    c.use :resque, service_name: 'classic-resque', analytics_enabled: true

    c.use :dalli, service_name: 'classic-cache' if dalli_in_use
  end
end

Datadog.tracer.default_service = 'mail-ticket-creator' if mail_ticket_creator

# Resque Instrumentation
# Hopefully temporary patch until we put this into zendesk_jobs
ZendeskJob::Resque::BaseJob.include(Datadog::Contrib::Resque::ResqueJob)

# Span Filters
# Filter out ping and diagnostic checks
APM_IGNORED_URLS = Set[
  '/z/ping',
  '/z/diagnostic',
  '/ping/host',
  '/ping/shards',
].freeze

Datadog::Pipeline.before_flush(
  Datadog::Pipeline::SpanFilter.new do |span|
    span.name == 'rack.request' && APM_IGNORED_URLS.include?(span.get_tag('http.url'))
  end
)

if ZendeskAPM.enabled?
  Rails.configuration.after_initialize do
    Api::V2::Presenter.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_methods :model_json, :as_json, :present, :side_loads, :side_load_association, :preload_associations
    end

    Api::V2::CollectionPresenter.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_methods :model_json, :item_presenter, :total, :as_json, :present, :page_url
    end

    Api::Presentation::MixedCollection.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_methods :model_json, :side_loads, :item_presenter, :merge_sideloads
    end

    String.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_method :auto_link
    end

    ZendeskBillingCore::Zuora::Commands::Subscribe.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_methods :preview!, :execute!
    end

    ZendeskBillingCore::Zuora::Commands::Amend.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_method :execute!
    end

    ZendeskBillingCore::Zuora::Commands::Amend::Preview.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_method :preview!
    end

    Rule.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_methods :find_tickets, :count_tickets
    end

    Zendesk::InboundMail::AdrianTicketProcessingWorker.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_method :perform
    end

    Zendesk::InboundMail::SQSTicketProcessingWorker.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_method :perform
    end

    Zendesk::InboundMail::TicketProcessingJob.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_method :work
    end

    Zendesk::InboundMail::TicketProcessingJob.all_processors.each do |processor|
      processor.singleton_class.class_eval do
        extend ZendeskAPM::MethodTracing
        trace_method :execute
      end
    end

    Ticket.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_methods :save, :save!, :publish_ticket_events_to_bus!
    end

    [TicketEventsProtobufEncoder, TicketProtobufEncoder].each do |encoder|
      encoder.class_eval do
        extend ZendeskAPM::MethodTracing
        trace_method :to_object
      end
    end

    ChatTranscript.class_eval do
      extend ZendeskAPM::MethodTracing
      trace_singleton_method :last_message_for_tickets
    end
  end
end

Datadog.tracer.enabled = ZendeskAPM.enabled?

# Cache the shard-to-cluster map at init time in order to avoid calls to Consul
# at runtime, where they could cause latency and errors.
#
# If the lookup fails, we'll default to not reporting the cluster info.
if Rails.env.development? || Rails.env.test?
  DB_SHARDS_TO_CLUSTERS ||= {}.freeze
else
  DB_SHARDS_TO_CLUSTERS ||= begin
    Zendesk::Configuration::DBConfig.shards_to_clusters.freeze
  rescue => e
    Rails.logger.error "Consul can not be reached to fetch DB shards and clusters information, #{e.message}"
    {}
  end
end
