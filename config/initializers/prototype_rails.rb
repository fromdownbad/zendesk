# Configuration for remaining prototype_rails methods - remove once methods are no longer used
ActiveSupport.on_load(:action_controller) do
  require 'prototype_rails/on_load_action_controller'
end
ActiveSupport.on_load(:action_view) do
  require 'prototype_rails/on_load_action_view'
end
