Kragle.configure do |config|
  config.oauth_key = Zendesk::Configuration.dig :system_user_auth, :mac_key
  config.oauth_secret = Zendesk::Configuration.dig :system_user_auth, :zendesk, :oauth

  config.statsd_host = Zendesk::StatsD::Configuration.fetch :host
  config.statsd_port = Zendesk::StatsD::Configuration.fetch :port

  config.logger = Rails.logger
  config.compact_logging = true
  config.client = 'classic'

  config.cache = Rails.cache
end
