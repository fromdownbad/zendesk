# TODO: this should be removed as part of https://zendesk.atlassian.net/browse/R5U-1472
if RAILS4
  class ZendeskCustomLoggerSanitizer < ActiveModel::MassAssignmentSecurity::LoggerSanitizer
    def process_removed_attributes(klass, _attrs)
      track_blocked_params(klass) if attr_accessible_logging_on?
      super
    end

    def attr_accessible_statsd_client
      @attr_accessible_statsd_client ||= Zendesk::StatsD::Client.new(namespace: :attr_accessible_blocked_parameters)
    end

    def attr_accessible_logging_on?
      return false if Rails.env.test?
      Arturo.feature_enabled_for_pod?(:track_attr_accessible_blocked_parameters, Zendesk::Configuration.fetch(:pod_id))
    end

    def track_blocked_params(klass)
      attr_accessible_statsd_client.increment(
        :blocked_parameters,
        tags: [
          "model:#{klass.name}"
        ]
      )
    end
  end

  # As part of the Rails 5 upgrade, the protected_attributes gem is being removed. As an in between step, this will alert
  # the developer if code is added which results in invalid attributes being passed to a model since that will result in
  # an error being raised once protected_attributes is removed.
  class ZendeskCustomSkipSanitizer < ActiveModel::MassAssignmentSecurity::Sanitizer
    def initialize(_target = nil)
      super()
    end

    def sanitize(_klass, attributes, _authorizer)
      attributes
    end
  end

  sanitizer = Rails.env.test? ? :zendesk_custom_skip : :zendesk_custom_logger
  ActiveRecord::Base.mass_assignment_sanitizer = sanitizer
end
