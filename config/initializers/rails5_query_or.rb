if RAILS5
  raise "remove me?" if RAILS51

  # https://github.com/rails/rails/issues/24055
  # https://stackoverflow.com/questions/40742078/relation-passed-to-or-must-be-structurally-compatible-incompatible-values-r
  module ActiveRecord
    module QueryMethods
      # Removed :eager_load, :references, :order from MULTI checks
      def structurally_incompatible_values_for_or(other)
        Relation::SINGLE_VALUE_METHODS.reject { |m| send("#{m}_value") == other.send("#{m}_value") } +
          (Relation::MULTI_VALUE_METHODS - [:extending, :eager_load, :references, :order]).reject { |m| send("#{m}_values") == other.send("#{m}_values") } +
          (Relation::CLAUSE_METHODS - [:having, :where]).reject { |m| send("#{m}_clause") == other.send("#{m}_clause") }
      end
    end
  end
end
