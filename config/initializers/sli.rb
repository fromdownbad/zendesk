# this initializer measures request durations and increments statsd counters for
# a number of namespaces based on request metadata
require_relative '../../lib/zendesk/sli_request_tagger'

@hist = Zendesk::Histogram::Base::Decremental.new
@statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['classic.app'], noversion: true)

ActiveSupport::Notifications.subscribe 'process_action.action_controller' do |*args|
  event = ActiveSupport::Notifications::Event.new *args

  tags = Zendesk::SliRequestTagger.tags || []

  # count every request, add status code range and latency bin tags
  # status code range cardinality should be 1:1 with resource
  #
  # add a 2xx,3xx,4xx status code range tag
  tags << if event.payload[:status].present?
    "status_code_range:#{event.payload[:status].to_s[0].to_i}xx"
  elsif event.payload[:exception].present?
    # add 5xx if exception present
    "status_code_range:5xx"
  else
    "status_code_range:NA"
  end

  # Add the bin tag from the histogram to track the latency band
  if event.duration.present?

    bins = @hist.find_bins(event.duration.round)
    bins.each do |bin|
      tags << "bin:#{bin}"
    end

  else
    tags << "bin:NA"
  end

  # embed resource into namespace
  @statsd_client.increment("sli.request.#{event.payload[:controller]}.#{event.payload[:action]}", tags: tags)
  Zendesk::SliRequestTagger.reset
end
