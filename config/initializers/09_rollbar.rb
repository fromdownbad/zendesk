Rollbar::Notifier.prepend(Module.new do
  # Rollbar API responses (async / sync)
  def handle_response(response)
    $statsd.increment('rollbar.api_response', tags: ["status_code:#{response.code}"]) # rubocop:disable Style/GlobalVars
    super
  end

  # Call if exception happens when building notice (before sending to Rollbar)
  def report_internal_error(exception)
    $statsd.increment('rollbar.internal_error', tags: ["exception:#{exception.class.name.underscore}"]) # rubocop:disable Style/GlobalVars
    super
  end
end)

ZendeskExceptions.setup!

Rollbar.configure do |config|
  # no production (i.e. customer data) until Security has had a chance to properly evaluate
  config.access_token = ENV['ROLLBAR_ACCESS_TOKEN'] if ENV['ROLLBAR_ACCESS_TOKEN'].present?
  config.scrub_fields |= Rails.application.config.filter_parameters.select { |x| x.is_a?(Symbol) }.map(&:to_s)
  config.project_gems = [/zendesk/] # full traces for these gems
  config.populate_empty_backtraces = false # don't report duplicate stack traces on exceptions
  config.use_async = true # Use a thread to report

  # Disable the stock Rollbar patches in favor of ZendeskExceptions::Middleware
  config.disable_monkey_patch = true

  config.person_method = :current_account
  config.person_username_method = :subdomain

  if ENV['COMPUTE_CELL_NAME'].present?
    config.payload_options[:cell_name] = ENV['COMPUTE_CELL_NAME']
  end

  config.exception_level_filters.merge!(
    'AbstractController::ActionNotFound' => 'ignore',
    'ActionController::BadRequest' => 'ignore',
    'ActionController::MethodNotAllowed' => 'ignore',
    'ActionController::RoutingError' => 'ignore',
    'ActionController::UnknownHttpMethod' => 'ignore',
    'ActiveRecord::RecordNotFound' => 'ignore',
    'Faraday::ConnectionFailed' => 'ignore',
    'Faraday::TimeoutError' => 'ignore',
    'Kragle::BadGateway' => 'ignore',
    'Kragle::GatewayTimeout' => 'ignore',
    'Kragle::InternalServerError' => 'ignore',
    'Kragle::ServiceUnavailable' => 'ignore',
    'JohnnyFive::CircuitTrippedError' => 'ignore', # reported as statsd events by Kragle already
    'Occam::Client::QueryTimeoutError' => 'ignore',
    'SystemExit' => 'ignore', # DataDeletion::JobHeartbeater intentionally suicides resque processes
    'Zendesk::OutgoingMail::SMTPServerBusy' => 'ignore', # Do not report MailRenderingJob SMTP Server Busy errors to rollbar
    'Zendesk::ProductNotReady' => 'ignore', # Do not report SyncJob errors to rollbar when account is still been setup
    'Zendesk::Rules::RuleQueryBuilder::UnsupportedRuleDefinition' => 'ignore',
    'ZendeskDatabaseSupport::MappedDatabaseExceptions::ConnectionFailed' => 'ignore',
    'ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound' => 'ignore',
    'ZendeskDatabaseSupport::MappedDatabaseExceptions::LockWaitTimeout' => 'ignore',
    'ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection' => 'ignore',
    'ZendeskDatabaseSupport::MappedDatabaseExceptions::ServerGoneAway' => 'ignore',
    'ZendeskDatabaseSupport::MappedDatabaseExceptions::TemporaryException' => 'ignore'
  )
end
