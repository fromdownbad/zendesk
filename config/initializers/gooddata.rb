require 'gooddata_client'

# GPGME relies on being able to write to files when you perform actions like import keys or set
# certain configuration values. By default, it will try to write into /usr/bin/gpg/..., but when we
# run as a non-privileged user, like in Kubernetes or on a dbadmin server, we don't have access to
# write to the /usr paths. This tells the library to use a different path which allows key imports
# to succeed.
# https://github.com/ueno/ruby-gpgme#engine
#
# Currently we use GnuPG for the GoodData client, which would fail to import keys in kubernetes.
# `GPGME::Error::General: General error`
# https://rollbar-eu.zendesk.com/Zendesk/Classic-K8S/items/4/
# https://github.com/zendesk/gooddata_client/blob/227662d8/lib/gooddata/crypto.rb#L18
GPGME::Engine.home_dir = '/tmp' if KUBERNETES

Zendesk::GooddataIntegrations::Configuration.setup

GoodData.configure do |gd|
  gd.request_timeout = 60.seconds
end
