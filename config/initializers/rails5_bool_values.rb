raise "kill me" if RAILS6

module Zendesk
  FALSE_VALUES = if RAILS4
    ActiveRecord::ConnectionAdapters::Column::FALSE_VALUES
  else
    ActiveModel::Type::Boolean::FALSE_VALUES
  end

  TRUE_VALUES = if RAILS4
    ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES
  else
    [true, 1, '1', 't', 'T', 'true', 'TRUE', 'on', 'ON'].to_set # stolen from rails 4
  end
end
