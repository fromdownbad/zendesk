require 'zendesk_search/client'

ZendeskSearch.kragle = {
  circuit_options: {
    failure_threshold: 3,
    reset_timeout: 30
  }
}

ZendeskSearch::Client.configure(
  ENV.fetch('SEARCH_SERVICE_URL_BLUE'),
  ENV.fetch('SEARCH_SERVICE_URL_GREEN'),
  ENV.fetch('SEARCH_SERVICE_HUB_URL')
)
