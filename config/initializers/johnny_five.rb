require 'johnny_five/memcached_store'

if dalli = Rails.cache.try(:dalli)
  JohnnyFive::MemcachedStore.client = dalli
end
