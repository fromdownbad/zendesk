if Rails.env.development? && File.exist?(File.join(Rails.root, 'tmp', 'debug.txt'))
  require 'byebug/core'
  File.delete(File.join(Rails.root, 'tmp', 'debug.txt'))
  Byebug.wait_connection = true
  Byebug.start_server
end
