# Configure the Coverband Middleware
if ENV['ENABLE_COVERBAND'] == 'true'
  Zendesk::CoverbandHelpers.initializer
end
