if RAILS5
  # validates_lengths_from_database uses tokenizer which is deprecated
  ActiveModel::Validations::LengthValidator::RESERVED_OPTIONS.delete :tokenizer
end
