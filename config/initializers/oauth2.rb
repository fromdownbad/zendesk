require 'oauth2'

OAuth2::Response.register_parser(:text, 'text/plain') do |body|
  key, value = body.split('=')
  {key => value}
end

Zendesk::OAuth.configure do |config|
  config.token_length = 64
  config.allowed_strategies = config.valid_strategies
  config.globally_allowed_strategies = %w[authorization_code client_credentials]

  config.default_expiry = false

  system_users = Zendesk::Configuration.fetch(:system_user_auth).keys - ['mac_key', 'mac_key_deprecated']

  # When configuring from ENV Zendesk.config will be a flat hash so we need to correct the keys.
  #
  # In YAML world, it looks like this:
  # {
  #   system_user_auth:
  #     help_center: {
  #       signed: foo
  #       oauth: bar
  #   }
  # }
  #
  # In ENV world, it looks like this:
  # {
  #   system_user_auth_help_center_signed: foo
  #   system_user_auth_help_center_oauth: bar
  # }
  #
  # So we need convert 'help_center_signed' to 'help_center'
  system_users.map! { |u| u.rpartition('_').first } if Zendesk::Configuration.config_from_env?('zendesk')

  system_users.concat Zendesk::Configuration.fetch(:system_global_clients).keys
  system_users.uniq!

  config.scopes = {
    "massive:read":               ["*:read"],
    "massive:write":              ["*:write"],
    sdk:                          ["read", "write"],
    read:                         ["general:read", *system_users],
    write:                        ["general:write", *system_users],
    impersonate:                  ["impersonate"],
    private_twitter_fabric:       ["private_twitter_fabric"],
    "tickets:read":               ["tickets:read"],
    "tickets:write":              ["tickets:write"],
    "users:read":                 ["users:read"],
    "users:write":                ["users:write"],
    "organizations:read":         ["organizations:read"],
    "organizations:write":        ["organizations:write"],
    "auditlogs:read":             ["auditlogs:read"],
    "hc:read":                    ["hc:read"],
    "hc:write":                   ["hc:write"],
    "apps:read":                  ["apps:read"],
    "apps:write":                 ["apps:write"],
    "automations:read":           ["automations:read"],
    "automations:write":          ["automations:write"],
    "targets:read":               ["targets:read"],
    "targets:write":              ["targets:write"],
    "triggers:read":              ["triggers:read"],
    "triggers:write":             ["triggers:write"],
    "any_channel:write":          ["any_channel:write"],
    "web_widget:write":           ["web_widget:write"],
    "macros:read":                ["macros:read"],
    "macros:write":               ["macros:write"],
    "requests:read":              ["request:read"],
    "requests:write":             ["requests:write"],
    "satisfaction_ratings:read":  ["satisfaction_ratings:read"],
    "satisfaction_ratings:write": ["satisfaction_ratings:write"],
    "dynamic_content:read":       ["dynamic_content:read"],
    "dynamic_content:write":      ["dynamic_content:write"],
    "accounts:write":             ["accounts:write"],
    "themes:read":                ["themes:read"],
    "themes:write":               ["themes:write"],
    "zis:read":                   ["zis:read"],
    "zis:write":                  ["zis:write"],
  }
end
