require "zendesk/extensions"
require "zendesk/serialized"
require 'zendesk_rules/rule_field_definitions'
require 'zendesk_rules/definitions/conditions'
require 'zendesk_rules/definitions/actions'

# This can be removed if you can load the following url twice in dev environment:
# http://dev.localhost:3000/api/v2/locales/current.json
require 'api/presentation'

if Rails.env.development? && !Rails.configuration.eager_load
  # Make sure we do not regress into slow startup time by preloading everything
  Rails.configuration.after_initialize do
    bad = ["User", "Ticket", "Target"]
    found = ActiveRecord::Base.send(:descendants).map(&:name)
    found_bad = bad & found

    raise "#{found_bad.join(" or ")} should not be preloaded, they will slow everything down!" if found_bad.any?
    raise "Preloading too many models (#{found.size} > 32): #{found.inspect}" if found.size > 32
    raise "mocha should not be loaded" if defined?(Mocha)
    # raise "rake should not be loaded" if defined?(Rake) && !%w[rake resque-pool].include?(File.basename($0))
    # The latest version of Rollbar Gem (2.12.0) loads rake. Uncomment if a fix becomes available.
    raise "controllers should not be loaded -- #{ActionController::Base.descendants}" if ActionController::Base.descendants.any?
    raise "mailers should not be loaded -- #{ActionMailer::Base.descendants}" if ActionMailer::Base.descendants.any?
  end
end
