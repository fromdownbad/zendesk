require 'stronger_parameters'

ActionController::Parameters.extend Zendesk::CustomParameterTypes

ActiveSupport::Notifications.subscribe('invalid_parameter.action_controller') do |*args|
  options = args.last
  Rails.logger.info "Invalid parameter #{options.inspect}"
end

module Zendesk
  module ForbiddenAttributesProtection
    def sanitize_for_mass_assignment(*args)
      sanitize_forbidden_attributes(args[0])
    end
  end
end
