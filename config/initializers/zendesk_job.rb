module CIA
  USER_MARKER = "Serialized-User".freeze

  class << self
    def serialize_users(hash)
      Hash[hash.map do |k, v|
        v = [USER_MARKER, v.id] if v.is_a?(User)
        [k, v]
      end]
    end

    def deserialize_users(hash)
      Hash[hash.map do |k, v|
        if v.is_a?(Array) && v.first == USER_MARKER && v.last
          v = find_user(v.last)
        end

        [k, v]
      end]
    end

    private

    # On unsharded jobs, we can't access the User table
    # but some jobs may eventually change shards and be
    # auditable, so we return a fake system user
    def find_user(id)
      if ActiveRecord::Base.current_shard_id
        User.find_by_id(id)
      else
        Rails.logger.debug("Cannot audit job using system user")
        User.new do |user|
          user.id = User.system_user_id
          user.instance_variable_set(:@new_record, false)
        end
      end
    end
  end
end

class CIAJobEnvironmentMiddleware
  def capture_environment
    {"cia" => CIA.serialize_users((CIA.current_transaction || {}).stringify_keys)}
  end

  def restore_environment(environment, _job_klass, &block)
    if environment[:perform_now]
      yield # not from an enqueued job, don't mess with the audit
    else
      audit = CIA.deserialize_users((environment["cia"] || {}).symbolize_keys)
      audit = { actor: User.system } if audit.blank?
      CIA.audit(audit, &block)
    end
  end
end

require 'zendesk_job/resque'
ZendeskJob::Resque::Environment.middlewares << CIAJobEnvironmentMiddleware.new
