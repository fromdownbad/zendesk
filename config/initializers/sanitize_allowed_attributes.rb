Rails.configuration.after_initialize do
  %w[data-method data-confirm target style rel].each do |attr|
    ActionView::Base.sanitized_allowed_attributes << attr
  end

  %w[table tbody thead tr td].each do |tag|
    ActionView::Base.sanitized_allowed_tags << tag
  end
end
