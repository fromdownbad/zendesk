require 'kasket'

ActiveRecord::Base.class_eval do
  def kasket_cacheable?
    # let kasket cache from-slave account records
    !self.class.is_sharded? || !from_slave?
  end
end

Kasket.setup(
  max_collection_size: 200,
  write_through:       false,
  default_expires_in:  ENV.fetch('KASKET_EXPIRES_IN_DEFAULT', 24.hours).to_i
)

if ENV['KASKET_USE_ELASTICACHE'] == 'true'
  require 'active_support/cache/dalli_store'

  elasticache_servers = Dalli::ElastiCache.new(ENV.fetch('KASKET_ELASTICACHE_ENDPOINT')).servers
  Kasket.cache_store = :dalli_store, elasticache_servers, Rails.application.config.memcache_options
end
