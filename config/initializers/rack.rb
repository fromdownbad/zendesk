# doubling the key_space_limit to handle clients with too many ticket_fields attempting to PUT a lot of data at once.
# see https://support.zendesk.com/tickets/233643
# See also https://github.com/rack/rack/issues/318 to understand what this does and why it's there
Rack::Utils.key_space_limit = 256000
