# rubocop:disable Security/Eval
# http://grosser.it/2012/08/16/rackutils-escape-unescape-cgi-escapeunescapeescapehtml-vs-undefined-method-bytesize-for-nil/
AUTOMATED_TO_STR_FOR_SAFE_BUFFER = <<-RUBY.freeze
  def METHOD_with_html_safe(object, *args)
    if object.is_a?(ActiveSupport::SafeBuffer)
      METHOD(object.to_str, *args)
    else
      METHOD_without_html_safe(object, *args)
    end
  end
  alias_method :METHOD_without_html_safe, :METHOD
  alias_method :METHOD, :METHOD_with_html_safe
RUBY

# can be removed if
# Rack::Utils.escape "a & & %24".html_safe
# Rack::Utils.unescape "a & & %24".html_safe
# does not raise an error, must work with strings and symbols
# and t test/integration/authentication_test.rb:460 does not show warnings
Rack::Utils.class_eval do
  def escape(s)
    CGI.escape(s.to_s)
  end

  def unescape(s)
    CGI.unescape(s)
  end

  def self.escape(s)
    CGI.escape(s.to_s)
  end

  def self.unescape(s)
    CGI.unescape(s)
  end
end

# can be removed if
# URI.escape "a & & %24".html_safe
# does not raise an error, must work with strings and symbols
URI.class_eval do
  class << self
    [:escape].each do |method|
      eval AUTOMATED_TO_STR_FOR_SAFE_BUFFER.gsub("METHOD", method.to_s)
    end
  end
end

# can be removed if
# CGI.escape "a & & %24".html_safe
# CGI.unescape "a & & %24".html_safe
# CGI.unescapeHTML "a & & %24".html_safe
# does not raise an error, must work with strings and symbols
# (escapeHTML always works)
CGI.class_eval do
  class << self
    [:escape, :unescape, :unescapeHTML].each do |method|
      eval AUTOMATED_TO_STR_FOR_SAFE_BUFFER.gsub("METHOD", method.to_s)
    end
  end
end

Object.send :remove_const, :AUTOMATED_TO_STR_FOR_SAFE_BUFFER
# rubocop:enable Security/Eval
