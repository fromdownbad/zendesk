# Arsi: https://github.com/zendesk/arsi
#
# Arsi intercepts ActiveRelation.update_all and ActiveRelation.delete_all method
# calls and verifies that the SQL always includes a scoping operator (eg
# account_id) or an id column. If you have a query that legitimately doesn't
# need to be scoped to an account you can disable Arsi by using `without_arsi`
# in an ActiveRelation chain.

require 'arsi'

Arsi.disable! if Rails.env.production?
