require 'zendesk/db/association_reflection_columns_on_slave'

require 'predictive_load'
require 'predictive_load/loader'
require 'predictive_load/active_record_collection_observation'

ActiveRecord::Base.include PredictiveLoad::ActiveRecordCollectionObservation
ActiveRecord::Relation.collection_observer = PredictiveLoad::Loader

# Don't try to preload a sharded association if the parent isn't sharded
# e.g. [Account,Account].map(&:brands) can't be preloaded since the brands may be on different shards
PredictiveLoad::Loader.prepend(Module.new do
  def supports_preload?(association)
    return false if records.first && !records.first.class.is_sharded? && association.klass.is_sharded?
    super
  end
end)
