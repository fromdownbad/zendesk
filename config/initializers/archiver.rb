require 'zendesk_archive'
require 'zendesk_archive/configuration'
require_relative '../../lib/zendesk/archive/key_versions'

class ZendeskArchiveUnknownError < StandardError
end

ZendeskArchive.configure do |config|
  ZendeskArchive::DynamoDBConfiguration.setup
  ZendeskArchive::RiakConfiguration.setup
  config.dynamodb_key_version = Zendesk::Archive.key_version_for([:dynamodb])
  config.dynamodb_primary = ->(stub_record) { Arturo.feature_enabled_for?(:archive_dynamodb_primary, stub_record.account) }
  config.riak_key_version = Zendesk::Archive.key_version_for([:riak_no_account_id, :riak])
  config.riak_primary = ->(stub_record) { Arturo.feature_enabled_for?(:archive_riak_primary, stub_record.account) }
end

# After a ticket is archived, DELETE the records from MySQL.
ZendeskArchive.delete_after_archive = true

ZendeskArchive.on_missing_record_error do |e|
  e ||= ZendeskArchiveUnknownError.new
  msg = e.message || 'Unknown ZendeskArchive Error'

  Rails.logger.warn(msg)
  Rails.application.config.statsd.client.increment('archiver.missing_record_error', tags: ["exception:#{e.class}"])
end
