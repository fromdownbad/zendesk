SHARDED_ENVIRONMENT = !Rails.env.test?

module ActiveRecord
  class Base
    class << self
      def quoted_table_name
        # try to prevent access to the master in certain cases.
        # mysql-only.  decided that our time was better spent elsewhere.
        #  if you needed to read this comment, sorry.
        escaped = table_name.to_s.gsub('`', '``')
        "`#{escaped}`".gsub('.', '`.`')
      end
    end
  end
end

# isolate the dbadmin boxes, restoring `on_all_shards` functionality.
if Rails.env != "development" && `hostname` =~ /dbadmin.*?\.pod/
  ActiveRecord::Base.configurations[Rails.env]["shard_names"] = Zendesk::Configuration::PodConfig.local_shards
end
