if RAILS4
  # The problem addressed by DelayTouching is fixed in ActiveRecord 5, so
  # the library (and calls to it) should no longer be necessary.  See
  # https://github.com/rails/rails/issues/18606
  require 'activerecord/delay_touching'
  require 'active_record/delay_touching/state'
else
  class << ActiveRecord::Base
    def delay_touching
      ActiveSupport::Deprecation.warn('activerecord-delay_touching can be removed')
      yield
    end
  end
end
