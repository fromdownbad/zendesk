require 'nokogiri'

# from https://github.com/sparklemotion/nokogiri/issues/822
# fix for soap calls failing with: ArgumentError: 'utf-8' is not a valid encoding
# patch to handle lower-case encoding from some SOAP services (e.g. soap4r)
Nokogiri::XML::SAX::Parser::ENCODINGS['utf-8'] ||= Nokogiri::XML::SAX::Parser::ENCODINGS['UTF-8']
