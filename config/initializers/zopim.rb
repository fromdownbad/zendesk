if KUBERNETES # TODO: K8S
  Zopim::Configuration.setup
else
  config_file = File.join(Rails.root, 'config/zopim.yml')

  unless File.exist? config_file
    config_file = File.join(Rails.root, 'config/zopim.yml.example')
    puts "WARNING: falling back to loading '#{config_file}'"
  end

  Zopim::Configuration.setup(file: config_file)
end
