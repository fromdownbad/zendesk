require 'zendesk_configuration'
require 'zendesk_mtc_mpq_migration'
require 'zendesk_statsd'

Zendesk::MtcMpqMigration.configure do |config|
  config.statsd_client = Zendesk::StatsD::Client.new(namespace: 'mail_ticket_creator')
  config.zendesk_host = Zendesk::Configuration.fetch(:host)
end
