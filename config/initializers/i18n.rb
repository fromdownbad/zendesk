require 'zendesk/utils/syck_patch'
require 'zendesk/i18n/english_by_zendesk'
require 'zendesk/i18n/backend/interpolation_old_style' # Rails 2 style interpolation (`{{}}`)
require 'zendesk/i18n/backend/interpolation_sanitize' # Automatically sanitize interpolation arguments (I18n.t)
require 'zendesk/i18n/extensions/time_formatting' # Time class formatting extension. Should be replaced with ICU.
require 'ffi-icu'

# get development translations via `rake i18n:backend:marshal_file[production]`
translation_paths = [
  Rails.root.join("config/locales/translations"),
  Rails.root.join("config/locales/translations/shared"),
  Zendesk::BusinessHours::Engine.root.join("config/locales/translations"),
  Zendesk::ApiDashboard::Engine.root.join("config/locales/translations"),
  Zendesk::MobileDeeplink::Engine.root.join("config/locales/translations"),
  Zendesk::OAuth::Engine.root.join("config/locales/translations")
]

TRANSLATION_FILES = Zendesk::I18n::TranslationFiles::Proxy.new([translation_paths])
TRANSLATION_FILES.instance unless ["test", "development"].include?(Rails.env)

backend_options = {
  packages: [
    "classic",
    "business_hours",
    "api_dashboard",
    "zendesk_mobile_deeplink",
    "zendesk_oauth",
    "plan_selection", # Remove after Zuora hosted payment page moves to Billing app
    "audit_logs" # Allows migration of audit_log strings to occur by giving access temporarily to audit-log service strings.
  ]
}

backend_options[:cache_path] = Rails.root.join("test/files/generated_i18n_cache") if Rails.env.test?
backend_options[:translation_paths] = translation_paths

# Same behavior from the time backends below were part of the zendesk_i18n gem.
backend_options[:raise_on_interpolation_error] = %w[development test].include? Rails.env

I18n.enforce_available_locales = false
I18n.default_locale = I18n.locale = :"en-US-x-1"

I18n.backend = if KUBERNETES || ENV['USE_ROSETTA_BACKEND'] == 'true'
  require 'zendesk/i18n/backend/rosetta'

  ROSETTA_URL = Zendesk::Configuration.fetch(:rosetta_url)

  # Rosetta will accept non-account requests for URLs like p5translations.zendesk.com
  backend_options[:host]    = ROSETTA_URL
  backend_options[:headers] = { 'Host' => "p#{Zendesk::Configuration.fetch(:pod_id)}translations.#{Zendesk::Configuration.fetch(:host)}" }
  backend_options[:logger]  = Rails.logger
  backend_options[:statsd_client] = Rails.application.config.statsd.client
  backend_options[:service_name] = 'classic'
  backend_options[:exception_handler] = ->(e) {
    Rails.application.config.statsd.client.increment(
      'i18n.rosetta_http_backend.failed',
      tags: ["error:#{e.class}"]
    )

    Rails.logger.error "Failed to load RosettaBackend. Falling back to YAML\n#{e.class}\n#{e.message}"
  }

  Rails.logger.info "I18n backend: RosettaBackend. service URL: #{ROSETTA_URL}"

  # RosettaBackend is a chain of RosettaHttpBackend and YmlBackend
  Zendesk::I18n::Backend::Rosetta.new(backend_options)
else
  silence_warnings do
    require 'zendesk/i18n/backend/local'
  end

  Rails.logger.info 'I18n backend: LocalBackend'

  Zendesk::I18n::Backend::Local.new(backend_options)
end

ENGLISH_BY_ZENDESK = Zendesk::I18n::EnglishByZendesk.instantiate
