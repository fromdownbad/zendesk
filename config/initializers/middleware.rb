# this is here because:
# - load paths are set up
# - app is not yet build (modifying config.application.middleware after this has no effect)
# - is executed in test (integration) and server (config.ru)
#
# when upgrading rails:
#  - check rake middleware stays the same
#  - check railties/lib/rails/application.rb to see if all the things we rely on are still there
require "zendesk/middleware/remote_ip_middleware"
require "zendesk/middleware/ultragrep"
require "zendesk/middleware/weak_etag"
require "zendesk_logging"
require "zendesk_api/controller"
require "zendesk_ip_tools"
require "zendesk_exceptions/middleware"
require "warden"
require "raindrops"

Rails.application.config.middleware.instance_eval do
  insert_before(0, Rack::ContentLength)
  insert_after Rack::ContentLength, Raindrops::Middleware

  # Stick in our stuff right after Rollbar so we actually stop things from getting there
  insert_after Rollbar::Middleware::Rails::RollbarMiddleware, MappedDatabaseExceptionsMiddleware unless %w[development test].include?(Rails.env)

  # Add the Unknown HTTP methods middleware
  insert_before(0, HttpMethodNotAllowedMiddleware)

  insert_before(0, Zendesk::Logging::Middleware, 'classic') do |env, attrs|
    attrs.merge(
      account_id: env["zendesk.account"].try(:id),
      session: env["zendesk.shared_session"]
    )
  end

  unless ["test", "development"].include?(Rails.env) # in dev Logger is synced anyway / we only have 1 process
    insert_before(0, Zendesk::Middleware::Ultragrep)
  end

  # RAILS5UPGRADE: ActionDispatch::ParamsParser gone in rails 5
  if RAILS4
    insert_before(ActionDispatch::ParamsParser, InvalidApiRequestHandler)
    insert_after(ActionDispatch::ParamsParser, ActionDispatch::XmlParamsParser)
  else
    use InvalidApiRequestHandler
  end

  insert_before(Rack::ETag, Zendesk::Middleware::WeakEtag) if RAILS4

  delete ActiveRecord::QueryCache

  use Zendesk::RemoteIpMiddleware, Zendesk::Configuration['forwarding_netblocks']

  use IpWhitelistMiddleware
  use RuleRoutingMiddleware
  use MobileSdkApiRedirector
  use LotusBootstrapServiceMiddleware

  use DatadogMiddleware if ZendeskAPM.enabled?

  use ZendeskExceptions::Middleware if ENV['ROLLBAR_ENABLED']

  insert_after(LotusBootstrapServiceMiddleware, ZendeskHealthCheck::Endpoint::Diagnostic)

  delete SecureHeaders::Middleware

  # Due to some problems with input data encoding, we're normalizing all the
  # incoming data as soon as possible with the `rack-utf8_sanitizer` gem.
  insert_before(0, Rack::UTF8Sanitizer)

  # The MiddlewareTracingMiddleware needs to be added after the Rack::TraceMiddleware to ensure
  # that we correctly connect all the middleware spans with the correct rack parent span.
  if ENV['ENABLE_MIDDLEWARE_TRACING'] == 'true'
    insert_after(Datadog::Contrib::Rack::TraceMiddleware, MiddlewareTracingMiddleware)
  end
end

Rack::Request.class_eval do
  include Zendesk::SharedSession::RequestMixin
end
