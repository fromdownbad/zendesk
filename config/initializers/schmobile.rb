require 'schmobile'

Schmobile::UserAgents.add_user_agent_pattern("Inbox by Zendesk")

module RewriteForceClassicParam
  def self.call(request)
    unless request.params["force_classic_layout"].nil?
      Rails.logger.info("Request forcing classic layout rewritten to is_mobile")
      request.params["is_mobile"] = (request.params.delete("force_classic_layout") == 0).to_s
    end

    nil
  end
end

Schmobile::Filters::CHAIN.unshift(RewriteForceClassicParam)
