require 'poddable_client'

# https://github.com/zendesk/poddable_client_rb
PoddableClient.configure do |config|
  config.base_path = if Rails.env.development?
    'master' # No 'development' entry yet
  else
    Rails.env
  end
end
