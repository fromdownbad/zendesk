if defined?(Unicorn)
  require_relative '../lib/unicorn_killer'
  require_relative '../lib/unicorn_error_logger'
end

require './lib/zendesk/web_lifecycle'
require 'zendesk/lifecycle'
Zendesk::Lifecycle::Unicorn.prepend(Zendesk::WebLifecycle)
