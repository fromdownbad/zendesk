require File.expand_path('../boot', __FILE__)
require 'rails/all'

# make gem requires show the time they use without the preloads they trigger (bumbler)
require 'action_controller/base'

if File.exist?('.env')
  require 'dotenv'
  Dotenv.load('.env')
end

Bundler.require :database, :preload, :memcache

Zendesk::Configuration.load_dotenv if ["test", "development"].include?(Rails.env)

Encoding.default_external = Encoding.default_internal = 'UTF-8'

require 'socket'
$application_hostname = Socket.gethostname # rubocop:disable Style/GlobalVars

require "zendesk/utils/syck_patch"

# Require vars and types here as they're used in class definitions
require 'zendesk_core'
require 'zendesk_configuration'
require 'zendesk/configuration/pod_config'
require 'zendesk/configuration/db_config'
Zendesk::Configuration.setup
require "#{Bundler.root}/lib/vars"
autoload :Country, 'zendesk/models/country'

include Zendesk::Types
require 'action_controller'
require 'zendesk/account_cdn'

# kasket needs to be loaded before any ActiveRecord:Base descendants
# require "active_record"
# raise unless ActiveRecord::Base.send(:descendants).empty?

# class ActiveRecord::Base
#  def self.inherited(subclass)
#    puts subclass
#    puts caller
#    puts "-" * 72
#    super
#  end
# end

# Initialize StatsD so we can use it for the application boot process
Zendesk::StatsD::Configuration.setup(env: Rails.env)

# Required to get rid of zendesk_instrumentation gem
# https://github.com/zendesk/zendesk_instrumentation/blob/v0.0.4/lib/zendesk_instrumentation/railtie.rb#L5
module ZendeskInstrumentation
  class Railtie < Rails::Railtie
    config.statsd = ActiveSupport::OrderedOptions.new
  end
end

module Zendesk
  class Application < Rails::Application
    test_or_dev_env = ["test", "development"].include?(Rails.env)

    if !test_or_dev_env && Zendesk::Configuration.config_from_env?("database") # necessary during migration to ENV vars
      # kube-console sets MYSQL_DATASUNRISE for us, but on dbadmin hosts we don't have that env var
      db_consul_mode = if ENV['MYSQL_DATASUNRISE'] || Socket.gethostname.start_with?('dbadmin')
        :datasunrise
      else
        :default
      end

      Zendesk::Configuration::DBConfig.configure_rails_from_env(consul: db_consul_mode)
    end

    # Add additional load paths for your own custom dirs (please keep sorted)
    config.autoload_paths += %w[
      app/controllers
      app/mailers
      app/middleware
      app/models/access
      app/models/activities
      app/models/activities
      app/models/concerns
      app/models/events
      app/models/external_ticket_datas
      app/models/external_user_datas
      app/models/jobs
      app/models/jobs/import
      app/models/logos
      app/models/organization/observers
      app/models/reports
      app/models/routing
      app/models/rules
      app/models/sequences
      app/models/targets
      app/models/ticket/observers
      app/models/ticket_fields
      app/models/ticket_form
      app/models/tokens
      app/models/users/observers
      app/observers
      app/presenters
      app/protobuf_encoders
      app/protobuf_encoders/group_events
      app/protobuf_encoders/organization_events
      app/protobuf_encoders/ticket_events
      app/protobuf_encoders/user_events
      app/protobuf_encoders/views_encoders
      lib
    ].map { |path| File.join Rails.root, path }

    config.autoload_once_paths += %w[
      app/helpers
      app/models
    ].map { |path| File.join Rails.root, path }

    if RAILS5
      # lib *must* also be in eager in order to be loaded in production
      config.eager_load_paths << "#{Rails.root}/lib"
      config.paths.add "lib", glob: "**/*.rb", eager_load: true
    end

    # Required by zendesk_instrumentation
    require 'zendesk/artist'
    Zendesk::Artist.statsd = $statsd = Zendesk::StatsD.client(namespace: 'classic.app') # rubocop:disable Style/GlobalVars
    config.statsd.client = $statsd # rubocop:disable Style/GlobalVars

    # To check which memcached client is used use rails console> Zendesk::Application.config.cache_store
    if ENV['USE_DALLI_ELASTICACHE'] == 'true'
      config.memcache_options = {
        namespace: RAILS_CACHE_PREFIX_VERSION_DALLI_ELASTICACHE,
        compress: true,
        compression_min_size: 4096,
        cache_nils: false,
        expires_in: 1.week
      }

      elasticache_servers = Dalli::ElastiCache.new(ENV.fetch('DALLI_ELASTICACHE_ENDPOINT')).servers
      config.cache_store = :dalli_store, elasticache_servers, config.memcache_options
    else
      servers = if test_or_dev_env
        Zendesk::Configuration.fetch(:memcached_servers).values.sort
      else
        Zendesk::Configuration.servers("memcached", ["pod-#{Zendesk::Configuration.fetch(:pod_id)}", "cluster-zendesk"], include_dead: true).sort
      end

      config.memcache_options = {
        prefix_key:       RAILS_CACHE_PREFIX_VERSION,
        prefix_delimiter: ':'
      }
      config.cache_store = :libmemcached_local_store, servers, {
        client: config.memcache_options,
        race_condition_ttl: 60.seconds
      }
    end

    # Removes the Rack::Lock middleware
    # We never actually use threads
    config.allow_concurrency = true

    # Force all environments to use the same logger level
    # (by default production uses :info, the others :debug)
    config.log_level = if ENV['ZENDESK_LOG_LEVEL'] && ["debug", "info", "warn", "error", "fatal"].include?(ENV['ZENDESK_LOG_LEVEL'])
      ENV['ZENDESK_LOG_LEVEL'].to_sym
    elsif Rails.env.production?
      :info
    else
      :debug
    end

    config.action_dispatch.ip_spoofing_check = false

    # Note - the session store used here is fast sessions, not regular AR session store
    # config.action_controller.session_store = :active_record_store

    # Observers that should always be running
    # do not add new observers, use pure modules -> Rails 4 safe + faster startup
    # config.active_record.observers = [ :account_restriction_observer ]

    # Make Active Record use UTC-base instead of local time
    config.active_record.default_timezone = :utc
    config.time_zone = 'UTC'

    config.i18n.enforce_available_locales = false

    if RAILS4
      config.active_record.whitelist_attributes = false
    end

    # Override ProtectedAttributes' disabling of strong_parameters
    # https://github.com/rails/protected_attributes/blob/master/lib/protected_attributes/railtie.rb#L6-L7
    config.action_controller.permit_all_parameters = false

    config.action_dispatch.cookies_serializer = ENV["HYBRID_COOKIES_ENABLED"]&.downcase == 'true' ? :hybrid : :marshal

    # config.active_record.default_shard = 1

    # Use Active Record's schema dumper instead of SQL when creating the test database
    # (enables use of different database adapters for development and test environments)
    config.active_record.schema_format = :sql
    ActiveSupport.use_standard_json_time_format = false # return :created_at => "2012/07/25 23:09:04 +0000", to keep v1 stable
    ActiveRecord::Base.include_root_in_json = false

    # TODO: RAILS4 remove this line but it should invalidate lots of caches but it would prevent fast updates to generate invalid caches
    config.active_record.cache_timestamp_format = :number

    # RAILS5UPGRADE remove this line since errors will raise in transactional callbacks regardless of this setting
    if RAILS4
      config.active_record.raise_in_transactional_callbacks = true
    end

    config.encoding = 'UTF-8'

    config.session_store :cookie_store, key: '_zendesk_session'
    config.secret_token = config.secret_key_base = Zendesk::Configuration.fetch(:session_salt)

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:transcription_text]

    ActionController::Parameters.allow_nil_for_everything = true
    config.action_controller.action_on_unpermitted_parameters = :log
    config.stronger_parameters_violation_header = 'X-Zendesk-API-Warn'

    # Configure sending emails
    ActionMailer::Base.default charset: "utf-8"

    config.gooddata_login_domain = 'zendesk-dev.com'

    require 'zendesk_outgoing_mail'
    require 'zendesk_mobile_deeplink'

    Zendesk::OutgoingMail.configure(config)
    config.action_mailer.perform_deliveries = true

    config.deprecated_asset_host = Zendesk::AccountCdn.asset_host
    config.public_asset_namespace = '/classic'
    config.assets.enabled = true
    config.assets.digest = (!Rails.env.development? || ENV["DIGEST_ASSETS"])
    config.assets.compress = false # we handle this on our own in assets.rake -> faster
    config.assets.version = '1.0'
    config.assets.precompile += %w[
      admin.css
      admin.js
      admin/vendor/jquery.js
      application/admin.css
      application/agent.css
      application/end_user.css
      application/global.css
      application/help_center.css
      auto_included.js
      bootstrap.js
      brands.js
      clean.css
      color_bundle.js
      csat.js
      csat.css
      email.css
      email.js
      embedded_lotus.js
      enhanced_security_settings.css
      help_center.js
      history.js
      lotus_launchpad.css
      mobile.css
      mobile.js
      mobile_sdk.js
      mobile_v2.css
      mobile_v2.js
      native_mobile.css
      nested_hash.js
      new_rule.js
      new_zuora_hosted_page.css
      node_pubsub_2.js
      Placeholders.js
      print.css
      roles.css
      roles.js
      routing.css
      routing.js
      rule.js
      screen.css
      slider_bundle.js
      settings/slas.css
      settings/slas.js
      zendesk_survey_support_churn.js
      swfobject.js
      admin/ace/ace_setup.js
      admin/vendor/ace/worker-zendesk_json.js
      vendor/jQuery_UI_themes/base/jquery.ui.theme.css
      vendor/jquery-1.6.1.min.js
      vendor/jquery-1.8.3.min.js
      vendor/jquery-3.5.1.min.js
      vendor/minilog.js
      vendor/player.js
      vendor/unobtrusive_jquery.js
      vendor/zendesk_menus.css
      vendor/zendesk_menus.js
      vendor/zendesk_minilog.js
      vendor/zendesk_reporter.js
      vendor/ZeroClipboard.js
      vendor/zuora/zuora_postmessage.js
      views/access/login_service_manager.js
      views/brands/brands.css
      views/brands/landing.css
      views/mobile_sdk/mobile_sdk.css
      views/mobile_sdk/onboarding.css
      views/entries/entries-edit.js
      views/voice/greetings_override.css
      workspaces.js
      zd_tickets_table.js
      zd_tickets_table_node.js
      zd_ticket_viewing_status.js
      zd_ticket_viewing_status_node.js
      zuora.js
    ]

    # Fix for sprockets backport until we upgrade to v3.0
    # https://github.com/sstephenson/sprockets/pull/545
    # Backports https://github.com/rails/rails/blob/3-2-stable/actionpack/lib/sprockets/railtie.rb#L31
    config.assets.configure do |env|
      env.cache = Rails.cache
    end

    config.assets.compile = test_or_dev_env
    # RAILS5UPGRADE: Remove
    if Rails::VERSION::MAJOR < 5
      config.serve_static_files = config.assets.compile
    else
      config.public_file_server.enabled = config.assets.compile
    end

    config.action_controller.include_all_helpers = false

    config.active_support.test_order = :sorted

    # The account master is a single point of failure and incurs high latency.
    # This code simulates its inavailability for integration tests
    if ENV.key?('ACCOUNT_MASTER_UNAVAILABLE')
      # Note the `after: ` parameter here appears functionally useless, and is
      # here for your benefit. The `before` parameter is what's actually
      # positioning us between the two.
      initializer 'zendesk.account_master_unavailable', after: 'active_record.initialize_database', before: 'active_record.set_reloader_hooks' do
        puts "zendesk.account_master_unavailable initializer is poisoning account master database configuration"
        Rails.logger.warn "zendesk.account_master_unavailable initializer is poisoning account master database configuration"

        if ActiveRecord::Base.configurations["#{Rails.env}_slave"].nil?
          Rails.logger.warn "zendesk.account_master_unavailable cloning valid configuration into missing '#{Rails.env}_slave'"
          ActiveRecord::Base.configurations["#{Rails.env}_slave"] = ActiveRecord::Base.configurations[Rails.env].clone
        end

        # Set the host for more communicative failures
        ActiveRecord::Base.configurations[Rails.env]['host'] = 'account-master-unavailable-test.localhost'
        # Using TCP port 1 ensures an immediate rejection instead of a timeout
        ActiveRecord::Base.configurations[Rails.env]['port'] = 1

        # Rebuild the ConnectionPool to propagate the configuration
        ActiveRecord::Base.remove_connection(Rails.env)
        ActiveRecord::Base.establish_connection
      end
    end

    # Make sure OAuth middleware is inserted before adding ZendeskApi::Limiter
    # TODO: move this into config/initializers/middleware.rb
    initializer "api_middleware", after: "zendesk_oauth.middleware" do |app|
      app.middleware.insert_before(Zendesk::OAuth::Middleware, ApiRateLimitedMiddleware)
      app.middleware.insert_after(ApiRateLimitedMiddleware, ConditionalRateLimitMiddleware)
      app.middleware.insert_before(ApiRateLimitedMiddleware, AccountRateLimitMiddleware)
      app.middleware.use(ZendeskApi::Limiter)
    end

    # Coverband use Middleware
    if ENV['ENABLE_COVERBAND'] == 'true'
      require 'coverband'
      config.middleware.use Coverband::Middleware
    end

    # :group => :all so it will run with config.assets.initialize_on_precompile = false
    initializer "commonjs", group: :all do |app|
      require 'zendesk/commonjs/compiler'
      app.assets.register_postprocessor 'application/javascript', Zendesk::CommonJS::Compiler
      app.assets.append_path Zendesk::CommonJS::Compiler::COMMONJS_DIR
    end

    config.after_initialize do
      # Need to preload these into the schema cache before it is replaced by
      # the "master" schema cache which only has a connection to the account db
      if config.eager_load && !(defined?($rails_rake_task) && $rails_rake_task) # rubocop:disable Style/GlobalVars
        # Any calls to `ActiveRecord::Base.connection` will open a connection to
        # the single account master unless wrapped in `on_slave`
        ActiveRecord::Base.on_slave do
          cache = ActiveRecord::Base.connection.schema_cache
          # One-off HABTM join table
          cache.table_exists?('accounts_allowed_translation_locales')
          cache.columns('accounts_allowed_translation_locales')

          # Hack from lib/zendesk/export/incremental_ticket_export.rb
          ActiveRecord::Base.on_first_shard do
            cache = ActiveRecord::Base.connection.schema_cache

            # From RuleExecuter#enhance_finder_includes
            # Arel will attempt to lookup join table aliases
            #
            # e.g. tickets.all(include: { requester: {}, assignee: { }, submitter: {} })
            # (with appropriate conditions to trigger Rails' one-query eager loading)
            # Arel will attempt to lookup `requester_tickets` and `submitters_tickets`
            # (assignee is looked up as `users` since it's alphabetically first)
            cache.table_exists?('requesters_tickets')
            cache.table_exists?('submitters_tickets')

            %i[photo identities taggings groups].each do |join_key|
              cache.table_exists?("#{join_key}_users")
              cache.table_exists?("#{join_key}_users_2")
            end
          end
        end
      end
    end

    ActiveSupport::XmlMini.backend = 'Nokogiri'
  end
end

require 'zendesk/rule_selection'

require "resque" # load before new_relic so it is detected

ActionView::Base.field_error_proc = proc { |input, _instance| input }
