# Don't add a `resource` or `resources` definition if you are going to use
# `only: []`. Instead use a `namespace` or a `scope`.
#

Zendesk::Application.routes.draw do
  namespace :api do
    namespace :internal do
      resource :monitor, controller: :monitor, only: [] do
        member do
          get  :agents
          get  :targets
          get  :user
          post :token
          post :destroy_account
          post :set_account_setting
          post :apply_subscription_changes
          post :suspend_account
          post :logout_all_users_and_devices
          post :send_one_time_login_to_user
          post :takeover_account
        end
      end
    end

    namespace :v1 do
      scope 'stats', controller: :stats do
        %w[
          aggregate_average
          aggregate_median
          average
          benchmarking
          graph_data
          latest
          lpf_csr
          median
          summation
          top_n
        ].each do |action|
          get "#{action}/:object_type(/:object_id)/:assoc_name/:stat_name", action: action
        end
        get 'benchmarking/:object_type/:object_name', action: :benchmarking
        get 'search/:object_type/:assoc_name',        action: :top_n
      end
    end

    namespace :v2beta do
      get 'tickets/:ticket_id/related',
        controller: 'tickets/related',
        action:     :show

      resources :crm, only: :show do
        member do
          get :sync_user_info
        end
      end
    end

    namespace :lotus do
      resources :assignables, controller: 'assignables/base', only: [] do
        collection do
          get :groups
          get :autocomplete
          get 'groups/:id/agents',
            action: :group_agents,
            as:     :assignables_group_agents
        end
      end

      resources :groups, only: :show do
        collection do
          get :assignable
        end

        resources :agents, only: [] do
          collection do
            get :assignable
          end
        end
      end

      resources :activities, only: :index
      resources :chat_settings, only: :index

      resources :tickets, only: [] do
        resources :conversations, only: %i[index show]
        resources :knowledge_events, only: %i[create]

        collection do
          get :recent
        end
      end

      resources :macros, only: :index do
        collection do
          # macros #apply endpoint listed manually because the Ticket Initializer
          # requires the ticket id to be under the :id param key.
          post ':macro_id/apply', action: :apply
        end
      end

      namespace :triggers do
        get 'search'
      end

      namespace :trigger_categories_migration do
        put 'migrate'
        put 'revert'
      end

      resources :time_zones, only: %i[index show]

      resources :manifests, only: %i[show] do
        collection do
          get 'versions', action: :versions
        end
      end

      namespace :cfa_migration do
        put 'migrate'
        put 'exit'
      end

      namespace :chat_migrations do
        get '/departments/options', action: :departments_options
        post '/departments', action: :departments_migrate
      end

      namespace :ccs_and_followers do
        namespace :jobs do
          resources :update_requester_target_rules, only: %i[create index]
        end
      end

      namespace :simplified_email_threading do
        resources :opt_in, only: %i[create index]
        resources :opt_out, only: %i[create index]
        post '/opt_out/feedback', controller: :opt_out, action: :feedback
      end
    end

    # These routes will be used by private endpoints mostly new mobile apps.
    namespace :private do
      namespace :mobile_sdk do
        resources :settings, only: :show, controller: :settings_v2
        post 'tos/accept', controller: :tos, action: :accept
      end
    end

    namespace :crm do
      namespace :private do
        resources :users, only: [] do
          collection do
            post :create_or_update
            post :create_or_update_many
          end
        end
      end
    end

    # These routes will be used by the internal mobile apps.
    # They will be organized around the needs of the mobile apps, not pure REST
    namespace :mobile do
      resources :lookup, only: :index, format: 'json', constraints: { subdomain: 'mobile-lookup' }

      namespace :account do
        resources :lookup, only: :index
        resources :groups, only: [] do
          collection do
            get :assignable
          end
        end
      end
      namespace :sdk do
        resources :settings, only: :show
      end
      resources :views, only: [], controller: 'rules/views' do
        member do
          get :execute
        end
      end
      resources :requests, only: %i[index show create update] do
        collection do
          get :show_many
          get :count
        end
        resources :comments,
          only:       %i[index show],
          controller: 'requests/comments'
      end
      resources :uploads, only: %i[create destroy]
      resources :push_notification_devices, only: %i[create destroy]
      resources :user_tags, only: :create do
        collection do
          delete :destroy_many
        end
      end
      resources :user_fields, only: :index
      resources :bootstrap, only: :index

      resource :devices, only: [] do
        post   :register
        delete :logout
      end

      get 'users/me', controller: :current_user, action: :show
      put 'users/me', controller: :current_user, action: :update
      post 'requests/:ticket_id/satisfaction_rating', controller: :satisfaction_ratings, action: :create
    end

    namespace :v2 do
      resources :custom_statuses, only: %i[index show create update destroy]

      resources :comment_redactions, only: %i[update]

      resources :workspaces, only: %i[index create update destroy show] do
        collection do
          delete 'destroy_many', action: :destroy_many
          put 'reorder', action: :reorder
          get 'definitions', action: :definitions
        end
      end

      match ':path',
        action:      :preflight,
        controller:  :cors,
        via:         :options,
        constraints: {path: /.*/} # apparently this is needed https://support.zendesk.com/agent/tickets/1777662

      namespace :billing do
        resource :service_connection, only: :create
        resource :notification,       only: :create
      end

      namespace :auth_billing do
        resource :service_connection, only: :show
        resources :tokens, only: :create
      end

      namespace :internal do
        resources :app_links, except: %i[edit new], format: 'json'

        resources :staff, only: %i[create update destroy] do
          resource :password, only: %i[update create], controller: 'password' do
            member do
              post :reset
            end
          end

          resources :emails, only: %i[create update destroy] do
            put :deliver_verify
          end

          collection do
            put :owner_welcome
          end
        end

        resources :users do
          collection do
            post :find_or_create_user_by_social_identity
            get  :find_user_identity_by_email
            get  :find_user_identity
            post :create_or_update
          end
        end

        resources :challenge_token, only: :create

        namespace :monitor do
          resource :account, only: [] do
            get :settings
            get :owner
            get :agent_counts

            put :change_account_owner
            put :create_new_account_owner
            put :change_account_country_id
          end

          resource :agents, only: [] do
            get :admins
            get :agents
            get :light_agents
            get :chat_agents
            get :associated_accounts
            get :contributors
          end

          resource :fraud_scores, controller: :fraud_scores, only: %i[create show]

          resource :fraud, controller: :fraud, only: [:show] do
            post :fraud_score_job
            post :watchlist
            post :spam_cleanup_job
          end

          resource :subscription, only: [] do
            get :settings
          end

          resource :mobile_sdk_blips, controller: :mobile_sdk_blips, only: :show, format: 'json' do
            put 'update_app/:id', action: :update_app
            put :update_account
          end

          resource :mobile_sdk_app_settings, controller: :mobile_sdk_app_settings, only: :show, format: 'json'

          resource :conditional_rate_limits do
            get :index
            post :create
            delete :destroy
            delete :destroy_all, action: :destroy_all
          end

          get :reserved_subdomains

          post :reserved_subdomains,
            action: :add_reserved_subdomain,
            as:     :add_reserved_subdomain
        end

        namespace :explore do
          post 'onboard', controller: 'onboarding', action: 'onboard'
        end

        %w[
          account
          account_stats
          add_reserved_subdomain
          agent_id_name_hash
          agents
          append_intermediate_certificate
          audit_events
          boost_lite_subscription
          boost_paid_subscription
          burn_rates
          cancel_account
          compliance_moves
          create_voice_usage
          delete_attachment
          destroy_zopim_subscription_boost
          disable_voice_failover_activated
          disable_voice_trial
          enable_voice_failover_activated
          enable_voice_trial
          enqueue_fraud_score_job
          experiment_participations
          facebook_pages
          feature_boost
          generate_twilio_account
          integration_service_instances
          logout_all_users_and_devices
          mobile_device
          mobile_devices
          reactivate_account
          registered_integration_services
          release_account_subdomain
          remove_host_mapping
          rename_brand_subdomain
          rename_subdomain
          reports
          reserved_subdomains
          reset_email
          reset_mobile_device_push_notif_count
          security_settings
          send_one_time_login_to_user
          settings
          soft_delete_account
          subscription
          takeover_account
          temporary_agent_count
          token
          trial_extensions
          twitter_handles
          undelete_suspended
          update_dns
          update_zopim_subscription_boost
          update_zuora_subscription
          zopim_subscription_boost
          zuora_approve_subscription
          zuora_subscription
          zuora_sync_subscription
        ].each do |action|
          match "monitor/#{action}", controller: :monitor, via: :all, action: action
        end
        resources :coupons
        resources :compliance_agreements, only: %i[index create destroy]

        resources :compliance_moves, only: :index

        resources :compliances,        only: :index
        resource  :mobile_sdk_setting, only: %i[show create update]

        namespace :answer_bot do
          resources :deflection, only: %i[create update], format: :json

          post :resolution, controller: :resolution, action: :resolve, format: :json
          post :rejection, controller: :rejection, action: :reject, format: :json
          post :viewed, controller: :viewed, action: :viewed, format: :json
        end

        namespace :collaboration do
          resources :events, only: %i[create], constraints: { format: :json }
          resources :tickets, only: %i[create update], constraints: { format: :json }
        end

        namespace :chat do
          resources :tickets, only: %i[create update] do
            get :chats, on: :member
          end
        end

        namespace :app_market do
          resource :account, only: [] do
            get :billable_agents
            get :app_recommendations_info
          end
        end

        resources :fraud_scores, only: %i[create index show update] do
          collection do
            get :vendor_review_prohibited
          end
        end

        resources :accounts, only: :index do
          collection do
            put :clear_cache
            post :sync_support_product
            post :sync_account_roles
            post :sync_account_users_entitlements
          end

          member do
            resources :data_deletion_audits, only: :active do
              collection do
                get :active
              end
            end

            resource :subscription, only: :update, controller: :subscription

            scope :spf_verification, controller: :spf_verification do
              get :index
              put :update
            end
          end

          resources :inbound_mail_rate_limits, controller: :inbound_mail_rate_limits do
            member do
              get :audits, controller: :inbound_mail_rate_limits, action: :audits
            end
          end

          get '/audits/:id/email', controller: :audit_emails, action: :show
        end

        resources :global_inbound_mail_rate_limits, only: :index

        namespace :billing do
          get 'latest_tpe_usage/:account_id', controller: :latest_tpe_usage, action: :show
          get 'agent_counts/:account_id',     controller: :agent_counts,     action: :show
        end

        resources :brands do
          member do
            put :remove_host_mapping
          end
        end

        resources :certificates, only: %i[index show destroy] do
          member do
            post :activate
          end
        end

        resources :certificate_moves do
          collection do
            post :prepare
            post :swap
            post :rollback
          end
        end

        resource :zendesk_provisioned_ssl, only: [], controller: :zendesk_provisioned_ssl do
          get :jobs
          post :enqueue
          delete :rate_limits
          delete :jobs, action: :kill_jobs
          post :reset
        end

        resources :radar do
          collection do
            post :reconfigure
            post :suspend
            post :unsuspend
          end
        end

        resources :tickets, only: :index do
          collection do
            get :show_many_comments
          end
          member do
            put :archive
            get :permissions
          end

          resources :audits, only: :index
        end

        resource :zopim_subscription, only: %i[show destroy], controller: :zopim_subscription do
          member do
            delete :reset
            put    :toggle_status
            put    :toggle_app_installation
            post   :restore_integration
            post   :restore_phase_3_integration
            post   :phase_three, action: 'create_phase_three'
            put    :phase_three, action: 'update_phase_three'
            put    :extend_trial_to_date
            put    :restore_trial_status
            put    :restore_phase
          end
        end

        resources :secondary_subscriptions, only: :index

        resource :boost, only: %i[show update destroy]

        resource :gooddata_integration,
          only:       %i[show update destroy],
          controller: :gooddata_integration do
          member do
            put :sync_configuration
            put :reprovision_user
          end
        end

        resource :zopim, only: :show, controller: 'zopim/account'

        resource :account, only: [], controller: :account do
          resources :settings, only: [:index], controller: :account_settings do
            collection do
              get :show_many
              put :update
            end
          end

          member do
            post  :enable_contributor
            put   :route
          end
        end

        resource :help_center_state, only: [:update, :destroy]

        resource :import_export_job_policy, only: [], controller: :import_export_job_policy do
          member do
            get :job_keys_by_type
          end
        end

        resource :prediction_settings, only: [], controller: :prediction_settings do
          member do
            get :automatic_answers_triggers
          end
        end

        resources :recipient_addresses, only: %i[index show], controller: :recipient_addresses do
          member do
            put :verify
          end
        end

        resource :authentication, only: :update, controller: :authentication

        resource :field_export, controller: :field_export do
          collection do
            get  :ticket_fields
            get  :custom_fields
            post :update_exportable_fields
          end
        end

        resources :account, only: [] do
          resources :coupon_redemptions, only: :index
        end

        resources :data_deletion_audits, only: %i[show create index] do
          member do
            put :enqueue
          end
        end

        namespace :zopim do
          resource :omnichannel_tickets, only: :create

          resource :account, only: :show, controller: :account

          resources :accounts, only: :show, controller: :accounts

          resources :tickets, only: [] do
            resource :satisfaction_rating,
              only:       :create,
              controller: :satisfaction_ratings
          end

          resource :agent, only: :destroy, controller: :agent do
            member do
              post :transfer_ownership
              put  :downgrade
            end
          end

          resource :chat_limit_settings, only: :update
        end

        post 'zopim_subscription/transfer_ownership',
          controller: 'zopim/agent',
          action:     :transfer_ownership

        resources :addon_boosts, only: %i[index create update destroy]

        resource :pod_moves, only: [] do
          get :fetch_feature
          put :enable_feature
          put :disable_feature
        end

        resources :expirable_attachments, only: :create

        resource 'moderation', controller: :moderation, only: :create do
          get :forums
        end

        resources :security_settings, only: [] do
          collection do
            get :show
            put :update
          end
        end

        resources :shared_session_record, only: :create

        post 'user_reports', controller: :user_reports, action: :new
        post '/jobs/create', controller: :jobs, action: :create

        delete 'users/:user_id/user_otp_settings', controller: :user_otp_settings, action: :destroy
        get 'users/:user_id/user_otp_settings/configured', controller: :user_otp_settings, action: :configured
        get 'users/:user_id/user_otp_settings/recovery_code', controller: :user_otp_settings, action: :recovery_code

        resources :subscription_features, only: [] do
          collection do
            post :master, action: :create_master_job
            post :local,  action: :create_pod_job
          end
        end

        resources :sandboxes, only: %i[index update], controller: :sandboxes do
          member do
            post :reactivate
            post :product_addon_sync
            post :notification
          end
        end

        resources :csv_exports, only: :create do
          collection do
            get :accounts
          end
        end

        resources :audit_logs, only: :create
        resources :undelete,   only: %i[index update], controller: :undelete

        namespace :voyager do
          post :notify, controller: :voyager_exports, action: :notify
        end

        namespace :billing do
          resource :account, only: :update
          resource :events,  only: :create

          namespace :subscription do
            resource :preview,  only: :show
            resource :features, only: :update, controller: :features
            resource :addons,   only: :update, controller: :addons
          end

          resource :subscription, only: %i[show destroy]
        end

        namespace :ipm do
          resources :alerts
          resources :feature_notifications
        end

        namespace :voice do
          resource :usage, only: :create

          resources :status, only: [] do
            collection do
              put :update
            end
          end

          resource :subscription,            only: :show
          resource :partner_edition_account, only: %i[show update]
          resource :tickets,                 only: %i[create]
          resource :recharge_settings,       only: %i[create show update] do
            post :recharge
          end

          resource :settings, only: :update do
            delete :cleanup_legacy_settings
            get    :billing_periods
          end

          resources :user_phone_number_identities, only: :update
          get 'user_phone_number_identities', controller: :user_phone_number_identities, action: :show
        end

        resource :experiments, only: [] do
          get    :stats
          get    :manifest
          put    'pick/:name/:group', action: :pick
          put    'start/:name',       action: :start
          delete 'reset/:name',       action: :reset
          delete 'delete/:name',      action: :destroy
        end

        resource :google_app_market, controller: :google_app_market do
          get  :qualifies_for_google_apps
          post :email_forwarding_setup
          post :group_email_forwarding_setup
          post :enable_google_apps
          post :unbind_google_apps_domain
          post :purge_google_apps_domain_aliases
        end

        resource :staff_events, only: :create
        resource :account_events, only: :create

        namespace :entity_lookup do
          resources :views_tickets, only: :index
        end

        namespace :entity_publication do
          resources :views_tickets, only: :index
          resource :views_tickets, only: :update
        end
      end

      namespace :voice do
        resource :settings, only: :update
      end

      namespace :stats do
        get 'benchmarks/:category', controller: :benchmarks, action: :show
      end

      namespace :integrations do
        resources :github, only: :create
        resources :fabric, only: :create, format: 'json'
      end

      namespace :channels do
        namespace :voice do
          resources :tickets, only: :create
          resource :app_interactions, only: :create

          resources :agents, only: [] do
            resources :tickets, only: [] do
              member do
                post :display
              end
            end

            resources :users, only: [] do
              member do
                post :display
              end
            end
          end
        end
      end

      scope '/mail_resources/:message_id' do
        resources :images,
          param: :filename,
          constraints: { filename: /.+/ },
          only: :show,
          controller: :mail_inline_images
      end

      get 'organizations/:organization_id/subscriptions',
        controller: 'help_center/organization_subscriptions',
        action:     :organization_index

      post 'resend_owner_welcome_email',
        controller: 'resend_owner_welcome_email',
        action:     :resend_owner_welcome_email

      resources :organizations do
        member do
          get :related
        end

        collection do
          get    :autocomplete
          post   :autocomplete
          get    :search
          post   :create_many
          post   :create_or_update
          get    :show_many
          delete :destroy_many
          put    :update_many
          get    :count
        end

        resources :organization_memberships,
          only:       :index,
          controller: :organization_memberships do
            collection do
              get :count
            end
          end

        resources :tickets,  only: :index do
          collection do
            get :count
          end
        end

        resources :users, only: :index do
          collection do
            get :count
          end
        end

        resources :requests, only: :index
        resource  :tags,     only: %i[show create update destroy]
      end

      resources :groups do
        collection do
          get :assignable
          get :autocomplete
          get :count
        end

        resources :memberships, only: :index, controller: :group_memberships do
          collection do
            get :assignable
            get :count
          end
        end

        resources :users, only: :index do
          collection do
            get :count
          end
        end
      end

      match 'users/me', action: :show, controller: :current_user, via: :all

      get 'users/me/settings', action: :show,       controller: 'users/settings'
      put 'users/me/settings', action: :update,     controller: 'users/settings'
      put 'users/me/merge',    action: :merge_self, controller: :users

      get 'users/me/session',       action: :show_current, controller: :sessions
      get 'users/me/session/renew', action: :renew,        controller: :sessions
      delete 'users/me/logout',     action: :logout,       controller: :sessions

      match 'users/radar_token',
        controller: :current_user,
        action:     :radar_token,
        via:        :all

      get 'users/:user_id/organization_subscriptions',
        controller: 'help_center/organization_subscriptions',
        action:     :user_index

      get 'users/:user_id/organization_subscriptions/count',
        controller: 'help_center/organization_subscriptions',
        action:     :count

      resources :deleted_users, only: %i[index show destroy] do
        collection do
          get :count
        end
      end

      resources :users do
        member do
          get :related
          put :merge
        end

        collection do
          get  :count
          get  :autocomplete
          post :autocomplete

          get :current
          get :search

          post :create_many
          post :create_or_update
          post :create_or_update_many

          get  :show_many
          post :show_many

          put :update_many

          delete :destroy_many

          post :request_create

          post :all_user_seats,          controller: 'users/user_seats'
          get  :voice_assignable_agents, controller: 'users/user_seats'

          get :seats_remaining
        end

        resources :identities, only: %i[index show create update destroy] do
          collection do
            get :count
          end

          member do
            put :make_primary
            put :verify
            put :request_verification
          end
        end

        resources :tickets, only: [] do
          collection do
            get :requested
            get :ccd
            get :followed
            get :assigned
          end
        end

        resources :groups, only: :index do
          collection do
            get :count
          end
        end

        resources :organizations, only: :index do
          collection do
            get :count
          end
        end
        resources :devices, only: %i[index update destroy]

        resources :sessions, only: %i[index show destroy]
        delete    :sessions, controller: :sessions, action: :destroy_many

        resource :crm_data, only: %i[show destroy], controller: :crm_data do
          member do
            get :status
            delete :all_active
          end
        end

        resource :zopim_identity,
          only:       %i[show create update destroy],
          controller: 'users/zopim_identity'

        resource :user_seats,
          only:       %i[create destroy],
          controller: 'users/user_seats'

        resources :user_seats, only: :index, controller: 'users/user_seats'

        resources :group_memberships, only: %i[index show create destroy] do
          collection do
            get :count
          end

          member do
            put :make_default
          end
        end

        resources :organization_memberships, only: %i[index show create destroy] do
          collection do
            get :count
          end

          member do
            put :make_default
          end
        end

        resources :requests, only: :index

        resource :password, only: %i[update create], controller: 'users/password' do
          member do
            post :reset
            get :requirements
          end
        end

        resource  :tags,  only: %i[show create update destroy]
        resources :skips, only: :index

        resource :entitlements, only: %i[show update], controller: 'users/entitlements' do
          member do
            get :full
            put :update_full
          end
        end

        resource :settings, controller: 'users/settings' do
          member do
            get :user_settings
          end
        end

        resources :compliance_deletion_statuses, controller: 'users/compliance_deletion_statuses', only: :index
      end

      resources :agents, only: :index

      namespace :exports do
        resources :tickets, only: :index do
          collection do
            get :sample
          end
        end
      end

      namespace :incremental do
        resources :users, only: :index do
          collection do
            get :sample
          end
        end

        resources :organizations, only: :index do
          collection do
            get :sample
          end
        end

        resources :ticket_events, only: :index do
          collection do
            get :sample
          end
        end

        resources :tickets, only: :index do
          collection do
            get :sample
            get :cursor
          end
        end

        resources :ticket_metric_events, only: :index

        resources :automatic_answers, only: :index do
          collection do
            get :sample
          end
        end

        resources :routing do
          collection do
            get :attributes
            get :attribute_values
            get :instance_values
          end
        end
      end

      resource :preview, only: :create, path: 'views/preview', controller: 'rules/previews' do
        member do
          post :count
        end
      end

      resources :views, except: :edit, controller: 'rules/views' do
        collection do
          get    :active
          get    :compact
          get    :show_many
          get    :count_many
          get    :search
          put    :update_many
          delete :destroy_many
          get    :groups
          get    :definitions
        end

        member do
          get :tickets
          get :feed
          get :execute
          get :export
          get :count
          get :next
          get :filter
        end

        resources :tickets, only: :show
      end

      resources :feature_usage_metrics, only: :index
      resources :ticket_metrics,        only: %i[index show]
      resources :ticket_predictions,    only: :show

      resources :tickets do
        member do
          put  :mark_as_spam
          get  :related
          post :merge
        end

        collection do
          get    :recent
          get    :count
          get    :show_many
          post   :show_many
          put    :update_many
          put    :mark_many_as_spam
          delete :destroy_many
          post   :create_many
        end

        resources :audits, only: %i[index show] do
          member do
            put :trust
            put :make_private
          end
        end

        resources :skips, only: :index

        resource :ticket_prediction,
          only:       :show,
          controller: :ticket_predictions

        resources :satisfaction_prediction_surveys, only: :create
        resource  :metrics, only: :show, controller: :ticket_metrics
        resources :incidents, only: :index do
          collection do
            get :count
          end
        end
        resources :collaborators, only: :index
        resources :followers, only: :index
        resources :email_ccs, only: :index

        resources :macros, only: :apply, controller: 'rules/macros' do
          member do
            get :apply
          end
        end

        resource :tags, only: %i[show create update destroy]

        resource :satisfaction_rating,
          only:       :create,
          controller: :satisfaction_ratings

        resources :comments, only: :index, controller: 'tickets/comments' do
          member do
            get :full_email_body
            put :redact
            put :make_private
            post :report
          end

          collection do
            get :twitter
          end

          resources :attachments, only: [], controller: 'tickets/attachments' do
            member do
              put :redact
            end
          end
        end

        get 'jira', controller: 'integrations/jira', action: :ticket_details
      end

      resources :problems, only: :index do
        collection do
          post :autocomplete
        end
      end

      namespace :imports do
        resources :tickets, only: :create do
          collection do
            post :create_many
          end
        end
      end

      resources :suspended_tickets, only: %i[index show destroy] do
        collection do
          delete :destroy_many
          put    :recover_many
          put    :bulk_recover
        end

        member do
          put  :recover
          post :attachments
        end
      end

      resources :ticket_audits, only: :index

      resources :ticket_fields, only: %i[index show create update destroy] do
        collection do
          put :reorder
          get :show_many
        end

        resources :options,
          only:       %i[index show destroy],
          controller: 'ticket_fields/custom_field_options'

        post :options,
          controller: 'ticket_fields/custom_field_options',
          action:     :create_or_update
      end

      resources :ticket_forms, only: %i[create index show update destroy] do
        collection do
          put :reorder
          get :show_many
        end

        member do
          post :clone
        end
      end

      resources :user_fields, only: %i[index show create update destroy] do
        collection do
          get :count
          put :reorder
          get :check_permissions
        end

        resources :options,
          only:       %i[index show destroy],
          controller: 'user_fields/custom_field_options'

        post :options,
          controller: 'user_fields/custom_field_options',
          action:     :create_or_update
      end

      resources :organization_fields, only: %i[index show create update destroy] do
        collection do
          get :count
          put :reorder
          get :check_permissions
        end
      end

      resources :user_views, controller: 'rules/user_views', only: %i[index show create update destroy] do
        member do
          get :execute
          get :export
        end

        collection do
          post :preview
          get  :definitions
          put  :reorder
        end
      end

      resources :trigger_categories,
        except: [:new, :edit],
        controller: 'rules/categories/trigger_categories' do
        collection do
          post :jobs
        end
      end

      resources :macros, only: %i[index new create show update destroy], controller: 'rules/macros' do
        member do
          get :apply
        end

        collection do
          get    :active
          get    :categories
          get    :definitions
          get    :actions
          get    :search
          put    :update_many
          delete :destroy_many
          get    :groups
        end

        resources :attachments,
          only:       %i[create index],
          controller: 'rules/macro_attachments'
      end

      scope path: '/macros', module: :rules, as: :macro do
        resources :attachments,
          only:       %i[create show],
          controller: :macro_attachments do
            member do
              get :content
            end
          end
      end

      resources :triggers, except: :edit, controller: 'rules/triggers' do
        collection do
          get    :active
          get    :definitions
          put    :reorder
          get    :search
          put    :update_many
          delete :destroy_many
        end

        resources :revisions,
          only:       %i[index show],
          controller: 'rules/trigger_revisions'
      end

      resources :automations, except: :edit, controller: 'rules/automations' do
        collection do
          get    :active
          get    :search
          put    :update_many
          delete :destroy_many
        end
      end

      namespace :routing do
        resources :attributes,
          only:       %i[index create show update destroy],
          controller: :attributes do
            collection do
              get :definitions
            end

            resources :values,
              only:       %i[index create show update destroy],
              controller: :attribute_values do
                member do
                  get :agents
                  get :agent_search
                end
              end
          end

        resources :requirements, only: [] do
          collection do
            get :fulfilled
          end
        end

        resources :tickets,
          only:       [],
          controller: :instance_values do
          member do
            get :instance_values, action: :show_ticket_instance_values
            post :instance_values, action: :update_ticket_instance_values
            put 'rules/execute', action: :rerun_attribute_ticket_mapping_rules
          end
        end

        resources :agents,
          only:       [],
          controller: :instance_values do
          member do
            get :instance_values, action: :show_agent_instance_values
            post :instance_values, action: :update_agent_instance_values
          end
        end
      end

      resources :jetpack_tasks, only: %i[index update]

      resource :dashboard, only: [], controller: :dashboard do
        member do
          get :agent
        end
      end

      resource :autocomplete, only: [], controller: :autocomplete do
        member do
          get  :tags
          post :tags
        end
      end

      resources :tags, only: :index do
        collection do
          get :count
        end
      end

      resource :format, only: [], controller: :format do
        member do
          post :markdown
          post :dc
        end
      end

      resources :custom_roles, only: %i[index create show update destroy]
      resources :job_statuses, only: %i[index show] do
        collection do
          get :show_many
        end
      end

      resources :bookmarks,      only: %i[index create destroy]
      resources :activities,     only: %i[index show]

      resources :brands, only: %i[create index show update destroy] do
        collection do
          get :check_host_mapping
        end

        member do
          get :check_host_mapping,
            action: :check_host_mapping_for_brand,
            as:     :brand_check_host_mapping
        end
      end

      resources :account_creation_token, only: :create

      resources :requests, only: %i[index show create update] do
        collection do
          get :open
          get :solved
          get :ccd
          get :search
        end

        resources :comments, only: %i[index show], controller: 'requests/comments'
      end

      resources :group_memberships, only: %i[index show create destroy] do
        collection do
          get    :assignable
          get    :count
          delete :destroy_many
          post   :create_many
        end
      end

      resources :organization_memberships, only: %i[index show destroy create] do
        collection do
          get :count
          post   :create_many
          delete :destroy_many
        end
      end

      resources :countries, only: %i[index show]

      resource :account, controller: :current_account, only: %i[show update] do
        resource :settings,
          only:       %i[show update],
          controller: 'account/settings'

        resource :resolution,
          only:       :show,
          path:       'resolve',
          controller: 'account/resolutions'

        resource :survey_response,
          only:       %i[show create update destroy],
          controller: 'account/survey_responses'

        # legacy sandbox controller
        resource :sandbox,
          only:       %i[show create destroy],
          controller: 'account/sandbox'

        resource :sandboxes, controller: 'account/sandboxes' do
          get     :index
          get     ':subdomain', action: :show
          delete  ':subdomain', action: :destroy
          post    :create
        end

        resource :subscription, only: %i[show create update destroy], controller: 'account/subscription' do
          member do
            get :preview
            post :update_payment_method
            post :update_currency
            post :add_agents
          end
        end

        resource :zopim_subscription, only: %i[create update destroy show], controller: 'account/zopim_subscription' do
          member do
            put    :activate
            delete :deactivate
            post   :authorize
          end
        end

        resource :explore_subscription, only: :destroy, controller: 'account/explore_subscription'

        resources :zopim_agents, only: :show, controller: 'account/zopim_agents'

        resource :voice_subscription do
          resource :recharge_settings,
            only:       %i[create show update],
            controller: 'account/voice_subscription/recharge_settings'
        end

        resource :features,
          only:       %i[show update],
          controller: 'account/features'

        get 'features/:feature_id/status',
          controller:  'account/features',
          action:      :status,
          constraints: { id: /\w+/ }

        resource :addons, only: :show, controller: 'account/addons'

        resource :boosts, only: :show, controller: 'account/boosts'
      end

      post 'internal/new_account',
        controller: 'accounts_creation',
        action:     :create_new_account

      resources :accounts, only: :create do
        collection do
          get :available
        end
      end

      resource :gooddata_integration, only: %i[create show update], controller: :gooddata_integration do
        collection do
          post :upgrade
          get :version
          post :re_enable
        end

        member do
          get :last_successful_process
        end
      end

      resource :gooddata_user,
        only:       %i[create show],
        controller: :gooddata_users

      resources :mobile_devices, only: %i[index show create destroy] do
        member do
          put :clear_badge
        end
      end

      resources :satisfaction_reasons, only: %i[index show]

      resources :satisfaction_ratings, only: %i[index show] do
        collection do
          get :count
          get :received
          get :export
        end
      end

      resources :uploads,     only: %i[create destroy]
      resources :attachments, only: %i[show destroy]

      namespace :knowledge do
        resource :settings, controller: :settings, only: :show
      end

      resources :organization_subscriptions,
        only:       %i[index create show destroy],
        controller: 'help_center/organization_subscriptions' do
          collection do
            get :count
          end
        end

      resources :targets, except: %i[new edit]

      resources :search, only: :index do
        collection do
          get  :incremental
          get  :index
          post :index
          get  :count
          get  :export
        end
      end

      resources :sharing_agreements, only: %i[index show create update destroy] do
        get :jira, controller: 'integrations/jira', action: :projects
        resources :shared_tickets, only: :index
      end

      namespace :portal do
        resources :search, only: :index
        resources :requests, only: :create
      end

      namespace :dynamic_content do
        resources :items do
          collection do
            get :show_many
          end
          resources :variants do
            collection do
              post :create_many
              put  :update_many
            end
          end
        end
      end

      resources :alerts, only: %i[index destroy] do
        collection do
          delete :destroy_many
        end
      end

      namespace :ipm do
        resources :feature_notifications, only: %i[index destroy]
        resources :alerts, only: %i[index destroy] do
          collection do
            delete :destroy_many
          end
        end
      end

      resources :revision, only: :index

      resources :end_users, only: %i[show update] do
        resources :identities, only: :create, controller: :end_user_identities do
          collection do
            get :count
          end

          member do
            put :make_primary
          end
        end
      end

      resources :time_zones, only: %i[index show], constraints: { id: /.+?/ }

      resources :audit_logs, only: %i[index show] do
        collection do
          post :export
        end
      end

      resources :recipient_addresses do
        collection do
          get :lookup
        end

        member do
          put :verify
        end
      end

      resources :forwarding_verification_tokens, only: :index

      resources :external_email_credentials, only: :destroy

      resources :reports, only: [] do
        member do
          get :result
        end
      end

      post 'experiments', controller: :experiments, action: :create
      put  'experiments', controller: :experiments, action: :update

      namespace :tracking do
        get  'properties', controller: :properties, action: :index
        get  'experiment_properties', controller: :experiment_properties, action: :index
        get  'support_user_properties', controller: :support_user_properties, action: :index
        get  'support_group_properties', controller: :support_group_properties, action: :index
        get  'subscription_properties', controller: :subscription_properties, action: :index
      end

      resources :onboarding_tasks, only: %i[index create] do
        collection do
          get :find_sample_ticket_id
          get :web_widget_snippet
          post :web_widget_setup_info
        end
      end

      match 'gooddata/tickets_export',
        controller: 'exports/gooddata',
        action:     :tickets_export,
        via:        :all

      match 'gooddata/account_options',
        controller: 'exports/gooddata',
        action:     :account_options,
        via:        :all

      match 'gooddata/show_many_tickets',
        controller: 'exports/gooddata',
        action:     :show_many_tickets,
        via:        :all

      resource :zopim_integration, only: %i[show create update destroy]

      namespace :slas do
        resources :policies, only: %i[index show create update destroy] do
          collection do
            put :reorder
            get :definitions
          end

          member do
            put :replace
          end
        end
      end

      resources :sessions, only: :index

      resources :resource_collections, only: %i[index show create update destroy]

      resources :skips, only: %i[create index]

      post 'push_notification_devices/destroy_many',
        controller: :push_notification_devices,
        action:     :destroy_many

      resources :target_failures, only: %i[index show]

      resources :mobile_sdk_apps, except: %i[new edit]

      resources :products, only: :index

      namespace :embeddable do
        namespace :config_sets do
          get :default, action: 'show_default'
          put :default, action: 'update_default'
        end
      end

      resources :deleted_tickets, only: %i[index destroy] do
        collection do
          delete :destroy_many
          put    :restore_many
        end
        member do
          put    :restore
        end
      end

      resources :ticket_field_conditions
    end

    namespace :services do
      resource :salesforce_integration, path: :salesforce do
        get 'configuration', controller: 'salesforce/configuration', action: :show
        post 'records', controller: 'salesforce/records', action: :create_record
        post 'configuration/objects', controller: 'salesforce/configuration', action: :save_object
        put 'configuration/objects/remove', controller: 'salesforce/configuration', action: :remove_object
        put 'configuration/objects/reorder', controller: 'salesforce/configuration', action: :reorder_objects
        put 'configuration/objects/filter', controller: 'salesforce/configuration', action: :save_object_filter

        get 'fields', controller: 'salesforce/fields', action: :show
        get 'objects', controller: 'salesforce/objects', action: :index
        get 'integration/status', controller: 'salesforce/integration', action: :status
      end
    end
  end

  scope module: :channels do
    namespace :api do
      namespace :v2 do
        namespace :internal do
          resources :tickets, path: 'channels/tickets', only: :show do
            collection do
              post :create_incoming_comment
              post :decorate_outgoing_comment
            end
          end
          resources :mailer, path: 'channels/mailer', only: [] do
            collection do
              post :facebook_page_unauthorized_notification
              post :twitter_handle_unauthorized_notification
              post :twitter_handle_deactivated_by_twitter_notification
              post :any_channel_instance_deactivate_notification
              post :any_channel_instance_needs_reinitialization_notification
            end
          end
        end
      end
    end
  end
end
