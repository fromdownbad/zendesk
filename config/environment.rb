bundle_gemfile_override = File.expand_path('../../.bundle-gemfile', __FILE__)
if File.exist?(bundle_gemfile_override)
  gemfile = File.read(bundle_gemfile_override).strip

  ENV['BUNDLE_GEMFILE'] = gemfile
end

require File.expand_path('../application', __FILE__)
# RAILS5UPGRADE: delete the following line
require_relative '../lib/zendesk/extensions/ar_protected_attributes_patch'
Zendesk::Application.initialize!

# enforce pre-commit gem usage on local dev environments
# if Rails.env.development? &&
#   !ENV["SKIP_PRE_COMMIT_GUARD"] &&
#   !File.exist?(".git/hooks/pre-commit") &&
#   !`hostname`.strip.end_with?(".sys.zendesk.com") &&
#   !ENV['DOCKER']
#   raise "gem install pre-commit && pre-commit install"
# end

Spring::Commands::Test.after_environment if ENV['SPRING_PRELOADED']
