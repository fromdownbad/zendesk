# rubocop:disable Naming/FileName
# Tested with test/unit/cleanliness_test.rb
require 'code_owners'
require 'ostruct'

module PreCommitChecks
  LOCK_FILES = {
    'Gemfile.lock' => 'vendor/cache',
    'Gemfile.rails5.lock' => 'vendor/cache-rails5'
  }.freeze

  class << self
    def check_duplicate_test_classes(files)
      found = {}
      files.each do |file|
        classes = File.read(file).scan(/(module [\w+\:]+)?\n\s*class ([\w\:]+)/).map do |m, c|
          next if m
          c
        end.compact
        classes.each do |klass|
          found[klass] ||= []
          found[klass] << file
        end
      end
      found.select { |_klass, f| f.size != 1 }.map { |klass, f| "#{klass} is defined in #{f.join(", ")}" }
    end

    def check_options_usage(files)
      files = (files - ["test/integration/cleanliness_test.rb"]).select do |file|
        file.start_with?("test/") && File.read(file) =~ /[^\:]@options\b/
      end
      files.map { |f| "#{f} uses @options, remove it" }
    end

    def check_ssh_git_gems
      over_ssh = LOCK_FILES.keys.map do |f|
        File.read(f).each_line.map { |line| line[/remote:\W+git@github\.com:.+/] }.compact
      end.flatten.sort.uniq

      return [] if over_ssh.empty?
      message = "These repos were requested over ssh: #{over_ssh.join(', ')}\n"\
                "Samson can't build a docker image with ssh auth repos therefore switch these gems to HTTPS.\n"\
                "You can ignore this if you're temporarily working with private sources but do not merge a private git gem or you will break the docker build."
      [message]
    end

    def check_file_ownerships
      new_files = (unowned_files_in_commit - known_unowned_files) - collectively_owned_files - file_ownership_exceptions
      old_files = known_unowned_files - unowned_files_in_commit
      result = []

      unless new_files.empty?
        result << "Add an ownership pattern to .github/CODEOWNERS for:"
        result << new_files.map { |f| "  #{f}" }.join("\n")
        result << nil
      end

      unless old_files.empty?
        result << "Remove obsolete entries from test/files/unowned.txt for:"
        result << old_files.map { |f| "  #{f}" }.join("\n")
        result << nil
      end

      result
    end

    def check_file_ownership_establishment(changed)
      changed_unowned_files = (changed & unowned_files_in_commit) - collectively_owned_files - file_ownership_exceptions

      if changed_unowned_files.any?
        [
          "\nAdd an ownership pattern to .github/CODEOWNERS for: \n #{changed_unowned_files.join("\n ")} \n"\
          "or get a +1 from \e[1m@support-review\e[22m before merging"
        ]
      else
        []
      end
    end

    def bundle_diff_allowances(lockfile)
      @allowances ||= {}
      @allowances[lockfile] ||= File.readlines("test/files/#{lockfile}.allowances.txt").map(&:strip)
    end

    # Iterate over each lockfile and compare the spec with the Gemfile.lock,
    # ignoring anything in bundle_diff_allowances and returning a list of
    # missing gems with their associated lockfile.
    def check_lockfile_diff(files)
      return [] if lockfiles_unchanged?(files)

      base_spec = parse_lockfiles[0].last # drop(1) so we don't reuse this

      parse_lockfiles.drop(1).
        map do |(name, spec)|
          diff = ((base_spec | spec) - (base_spec & spec)).map { |(_n, missing_gem)| missing_gem }

          OpenStruct.new(name: name, missing_gems: (diff - bundle_diff_allowances(name)).sort.uniq)
        end. # rubocop:disable Style/MultilineBlockChain
        reject { |group| group.missing_gems.empty? }
    end

    def check_package(files)
      return [] if lockfiles_unchanged?(files)

      missing = []
      parse_lockfiles.flat_map do |file, expected|
        expected.map do |revision, gem, version|
          cache_directory = LOCK_FILES[file]
          path = "#{cache_directory}/#{gem}-#{version}.gem"
          next if revision || File.exist?(path)
          missing << path
        end.compact
      end
      missing
    end

    def lockfiles_unchanged?(files)
      (LOCK_FILES.keys & files).empty?
    end

    # from https://github.com/grosser/bundle_package_check/blob/master/lib/bundle_package_check.rb#L28
    def parse_lockfiles
      LOCK_FILES.keys.map do |file|
        lock = File.read(file)
        [file, lock.scan(/(?:revision: (\S+)(?:\n  .*)*\n  specs:\n|^)    (\S+) \((\S+)\)/)]
      end
    end

    def unowned_files_in_commit
      @unowned_files_in_commit ||=
        CodeOwners.ownerships.
          select { |f| f[:owner] == "UNOWNED" }.map { |f| f[:file] }
    end

    def known_unowned_files
      @known_unowned_files ||=
        File.read('test/files/unowned.txt').
          split("\n").
          map { |file| file.chomp(' # collective ownership') }
    end

    def collectively_owned_files
      @collectively_owned_files ||=
        File.read('test/files/unowned.txt').
          split("\n").
          select { |file| file.end_with?('# collective ownership') }.
          map { |file| file.chomp(' # collective ownership') }.
          concat(Dir.glob('benchmarks/*', File::FNM_DOTMATCH)[2..-1])
    end

    def file_ownership_exceptions
      %w[.github/CODEOWNERS]
    end
  end
end

if $0 == "config/pre-commit.rb" # executed via pre-commit hook
  changed = ARGV - ["config/pre-commit.rb"]

  errors = []

  if changed.include?("Gemfile") && File.read("Gemfile").include?("git://")
    errors << "Use https:// not git:// for git sources"
  end

  changed.select { |file| file.include?("app/models/events") && File.read(file).include?(" serialize :") }.each do |file|
    errors << "Use fixed_serialize instead of serialize or children will suffer #{file}"
  end

  if `git config branch.alwaysrebasingiswrong`.strip != "true" && `git config branch.autosetuprebase`.strip != "always"
    errors << "Add to your ~/.gitconfig to avoid merges on pull:\n[branch]\n  autosetuprebase = always"
  end

  unless ["current", "upstream", "tracking", "simple"].include?(`git config push.default`.strip)
    errors << "Add to your ~/.gitconfig to avoid accidental force pushes:\n[push]\n  default = upstream"
  end

  current_branch = `git branch | grep "^* "`.sub("* ", "").strip
  prohibited_branches = ["master"]

  if (branch = prohibited_branches.delete(current_branch))
    errors << "Please create a pull request instead of committing directly to the #{branch} branch."
  end

  changed.select { |file| file.start_with?("app/models") }.each do |file|
    File.readlines(file).each_with_index do |line, i|
      if line =~ / validate.*:message => / && !line.include?("# message is admin only")
        errors << "validate* with :message is not i18n-ified in #{file}:#{i + 1}"
      end
    end
  end

  errors.concat PreCommitChecks.check_duplicate_test_classes(changed)
  errors.concat PreCommitChecks.check_options_usage(changed)
  errors.concat PreCommitChecks.check_ssh_git_gems
  errors.concat PreCommitChecks.check_file_ownerships
  errors.concat PreCommitChecks.check_file_ownership_establishment(changed)
  errors.concat(PreCommitChecks.check_lockfile_diff(changed).map do |gem_group|
    "Gemfile.lock and #{gem_group.name} are out of sync. Run: script/bundle. Missing #{gem_group.missing_gems.join(', ')}"
  end)
  errors.concat(PreCommitChecks.check_package(changed).map do |gem|
    "#{gem} is missing. Run: script/bundle"
  end)

  if errors.any?
    puts errors
    exit 1
  end
end
# rubocop:enable Naming/FileName
