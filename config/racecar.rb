require 'racecar'

Racecar.configure do |config|
  # Each config variable can be set using a writer attribute.
  config.brokers = ENV.fetch("KAFKA_BROKERS").split(",")

  # Prefix all consumer group ids with this string.
  config.group_id_prefix = "classic."

  # We want a longer session timeout in order to have more time for consumers to
  # fetch messages before having to send a heartbeat to Kafka.
  config.session_timeout = 60

  # Commit consumer offsets every 10 seconds.
  config.offset_commit_interval = 10

  # Send a consumer group heartbeat every 10 seconds.
  config.heartbeat_interval = 10

  # After an exception in consumer processing code, pause the partition from
  # which the message causing the exception came from for this many seconds.
  config.pause_timeout = 60

  # Allow at most this many seconds when trying to connect to a broker.
  config.connect_timeout = 30

  # Allow at most this many seconds when trying to read from a socket.
  config.socket_timeout = 30

  # Enable gzip compression on messages
  config.producer_compression_codec = "gzip"

  # Enable Datadog
  config.datadog_enabled = "true"
  config.datadog_host = Zendesk::StatsD::Configuration.fetch :host
end
