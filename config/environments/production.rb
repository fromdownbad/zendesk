Zendesk::Application.configure do
  ENV['HOME'] = '/home/zendesk'

  if LOG_JSON_TO_STDOUT_ONLY && !defined?(Rails::Console)
    # zendesk_logging v0.6 uses a log subscriber to send JSON to STDOUT, so we need Rails to have a
    # non-STDOUT logger here, but tell it to send file logs to a black hole leaving us with only
    # logs on STDOUT.
    config.logger = ActiveSupport::TaggedLogging.new(Logger.new('/dev/null'))
  elsif STDIN.tty?
    config.logger = ActiveSupport::TaggedLogging.new(Logger.new(STDOUT))
    config.log_formatter = ::Logger::Formatter.new
  end

  # We only want logs decorated with colors on Rails console. We don't need extra unicode characters
  # in our JSON logs.
  config.colorize_logging = !!defined?(Rails::Console)

  # Code is not reloaded between requests
  config.cache_classes = config.eager_load = ENV['PERF_TEST'] || !defined?(Rails::Console)
  config.preload_frameworks = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true

  config.action_controller.asset_host = "#{ENV.fetch('ZENDESK_STATIC_ASSETS_DOMAIN')}/classic"

  config.gooddata_login_domain = 'zendesk.com'
  config.bime_ips = ['107.23.115.201', '34.193.122.226', '34.193.90.142', '34.192.210.226',
                     '52.214.149.124', '52.31.137.238', '52.214.110.43']
  config.connect_ips = ['104.154.96.207', '104.197.128.180', '146.148.35.208', '35.184.57.219',
                        '104.197.38.118']

  ActiveSupport::Deprecation.silenced = true
end
