Zendesk::Application.configure do
  config.cache_classes = true
  config.eager_load    = !!ENV['FORCE_EAGER_LOAD']
  config.action_controller.perform_caching = true

  config.consider_all_requests_local = true
  config.action_dispatch.show_exceptions = false # Raise exceptions instead of rendering exception templates

  config.active_support.deprecation = if RAILS4
    :raise
  else
    :log
  end

  require 'test_memory_store'
  config.cache_store = TestMemoryStore.new

  session_options = {
    key: "_zendesk_test_Session",
    secure_key: "_zendesk_test_Secure",
    secret: "You don't test the zendesk. The zendesk tests you"
  }
  config.session_store :cookie_store, session_options.except(:secret)
  config.secret_token = session_options.fetch(:secret)

  config.action_mailer.delivery_method = :test
  config.action_mailer.perform_deliveries = false

  config.action_controller.allow_forgery_protection = false # Disable request forgery protection in test environment

  config.action_controller.action_on_unpermitted_parameters = :log

  config.after_initialize do
    config.active_record.default_shard = nil
  end

  ENV['BENCHMARK'] ||= 'none'

  Rollbar.configure do |config|
    config.enabled = false
  end

  if ENV['RAILS_DISABLE_TEST_LOG'] || ENV['TRAVIS']
    config.logger = Logger.new(nil)
    config.log_level = :fatal
  end

  # TODO: remove this comment as part of https://zendesk.atlassian.net/browse/R5U-1472
  # config.active_record.mass_assignment_sanitizer is set in zendesk_custom_logger_sanitizer.rb

  config.bime_ips = ['107.23.115.201', '35.165.243.93', '52.33.144.235', '34.193.176.172',
                     '34.194.28.39', '34.192.97.104', '13.59.151.184', '13.59.151.214', '13.59.151.217', '34.193.122.226',
                     '34.193.90.142', '34.192.210.226', '52.214.149.124', '52.31.137.238', '52.214.110.43']
  config.connect_ips = ['104.154.96.207', '104.197.128.180', '146.148.35.208', '35.184.57.219',
                        '104.197.38.118']
end
# RAILS5UPGRADE: remove this
require 'rails_5_param_hash_regression.rb'
