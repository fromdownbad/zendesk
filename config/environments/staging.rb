load File.expand_path '../production.rb', __FILE__
Zendesk::Application.configure do
  config.log_level = :info
  config.gooddata_login_domain = "zendesk-dev.com"
  config.bime_ips = ['84.14.150.26', '62.23.44.146', '34.193.176.172', '34.194.28.39', '34.192.97.104']
  config.connect_ips = ['35.186.174.138', '35.186.175.155', '35.186.182.238', '35.186.186.165']
end
