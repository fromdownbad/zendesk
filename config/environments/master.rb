load File.expand_path '../staging.rb', __FILE__
Zendesk::Application.configure do
  config.consider_all_requests_local = true
  config.bime_ips = ['84.14.150.26', '62.23.44.146', '35.165.243.93', '52.33.144.235']
  config.connect_ips = []
end
