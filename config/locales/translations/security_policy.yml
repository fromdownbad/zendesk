title: "Security Policy"
packages:
  - default
  - classic
  - help_center
  - enduser

parts:
  - translation:
      key: "txt.security_policy.requirements.title"
      title: "Password requirements title"
      value: "Password requirements:"
  - translation:
      key: "txt.security_policy.requirements.max_length"
      title: "Password is too long. Chinese, Japanese and Korean, please use 128 halfwidth characters instead of 128 characters."
      value: "must be fewer than 128 characters"
  - translation:
      key: "txt.security_policy.requirements.text_max_length"
      title: "Password requirements maximum length title. Chinese, Japanese and Korean, please use 128 halfwidth characters instead of 128 characters."
      value: "Enter a password that is fewer than 128 characters"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.too_short"
      title: "Password is too short"
      value: "must be at least {{count}} characters"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.complex"
      title: "Password is too simple, must include letters in mixed case and numbers"
      value: "must include letters in mixed case and numbers"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.difference_username"
      title: "Password: must be different from username"
      value: "must be different from username"
      obsolete: "2015-08-12"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.difference_email_address"
      title: "Password: must be different from email address"
      value: "must be different from email address"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.numbers"
      title: "Password is too simple, must include numbers"
      value: "must include numbers"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.mixed"
      title: "Password is too simple, must include letters in mixed case"
      value: "must include letters in mixed case"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.unique"
      title: "Password is not unique"
      value: "must be different than the previous {{count}} passwords"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.special"
      title: "Password is missing a special character (! @ # , % } ^, etc.)"
      value: "must include a character that is not a letter or number"
  - translation:
      key: "txt.security_policy.requirements.expiration"
      title: "Password expiration"
      value: "expires after 90 days"
  - translation:
      key: "txt.security_policy.requirements.expiration_in_days"
      title: "Password expiration in a certain amount of days"
      value: "expires after {{count}} days"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.local_part"
      title: "Password contains local part from the users email (localpart@domain.com). Password: must not include the {{local_part part}} from your email"
      value: 'must not include the "{{local_part}}" part from your email'
  - translation:
      key: "txt.security_policy.expiration_alert"
      title: 'Expiration Alert'
      value: 'Your password will expire in {{count}} <a href="{{url}}">change password</a>'
      js: true
  - translation:
      key: "activerecord.attributes.user.password"
      title: "Password keyword. Example of how it is used: 'Password: must be at least 5 characters'."
      value: "Password: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.password_history_length"
      title: "Field to set that passwords must be different than at least a given number of previous passwords"
      value: "must be different than at least this many previous passwords: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.password_length"
      title: "Field to set that passwords must be at least this many characters"
      value: "must be at least this many characters: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.password_complexity"
      title: "Field to set that passwords must include numbers and special characters"
      value: "must include numbers and special characters: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.password_in_mixed_case"
      title: "Field to set that passwords must include letters in mixed case"
      value: "must include letters in mixed case: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.password_duration"
      title: "Field to set the password expiration time in days"
      value: "expires after how many days: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.failed_attempts_allowed"
      title: "Field to set number of failed attempts until lockout: "
      value: "number of failed attempts until lockout: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.session_timeout"
      title: "Field to set session expiration time in minutes"
      value: "sessions expire after how many minutes: "
  - translation:
      key: "activerecord.attributes.custom_security_policy.max_consecutive_letters_and_numbers"
      title: "Field to set max number of consecutive letters or numbers allowed in passwords. https://zendesk.app.box.com/files/0/f/2892703037/1/f_34483842397"
      value: "max number of consecutive letters or numbers allowed: "
      obsolete: "2018-08-01"
  - translation:
      key: "activerecord.attributes.custom_security_policy.max_sequential_letters_and_numbers"
      title: "Field to set max number of sequential letters or numbers allowed in passwords."
      value: "maximum number of sequential numbers or letters allowed: "
      screenshot: "https://drive.google.com/file/d/19skmYKH3CfWtnkkWgt_NuEH1kEeHYhYH/view"
  - translation:
      key: "activerecord.attributes.custom_security_policy.max_consecutive_options.unlimited"
      title: "Dropdown option for the unlimited option of max number of consecutive letters/numbers in password. https://zendesk.app.box.com/files/0/f/2892703037/1/f_34483842397"
      value: "unlimited"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.max_consecutive_letters_and_numbers"
      title: "Password must not have more than X consecutive letters or numbers (abcde or 12345 are not valid if X is 5). https://zendesk.app.box.com/files/0/f/2892703037/1/f_34483842397"
      value: "must not have more than {{number}} consecutive letters or numbers"
      obsolete: "2018-08-01"
  - translation:
      key: "activerecord.errors.models.user.attributes.password.max_sequential_letters_and_numbers"
      title: "Password must not have more than X sequential letters or numbers (abcde or 12345 are not valid if X is 5)."
      value: "must not contain any sequence of more than {{number}} consecutive letters or numbers"
      screenshot: "https://drive.google.com/file/d/1Urynm8yHiFTadxNvkTzXzHtv4DxJfsdz/view"
  - translation:
      key: "activerecord.attributes.custom_security_policy.must_not_include_the_local_part"
      title: "Field to set if the local part of localpart@email.com is allowed as part of a password"
      value: "must not include the local part of the user's email (localpart@domain.com): "
  - translation:
      key: "activerecord.attributes.custom_security_policy.must_not_include_the_local_part_options.yes"
      title: "Yes option for the setting if local part of localpart@email.com is allowed as part of a password"
      value: "yes"
  - translation:
      key: "activerecord.attributes.custom_security_policy.must_not_include_the_local_part_options.no"
      title: "No option for the setting if local part of localpart@email.com is allowed as part of a password"
      value: "no"
  - translation:
      key: "txt.security_policy.password_history_length.none"
      title: "Pulldown option to disable previous passwords checking in the custom security policy"
      value: "none"
  - translation:
      key: "txt.security_policy.password_history_length.previous"
      title: "Pulldown option to select a certain number of previous passwords to check in the custom security policy"
      value: "{{number}} previous passwords"
  - translation:
      key: "txt.security_policy.password_complexity.no"
      title: "Pulldown option to disable password complexity"
      value: "no"
  - translation:
      key: "txt.security_policy.password_complexity.numbers"
      title: "Pulldown option to include numbers only in passwords"
      value: "numbers only"
  - translation:
      key: "txt.security_policy.password_complexity.numbers_special"
      title: "Pulldown option to include numbers and special characters in passwords"
      value: "numbers and special characters"
  - translation:
      key: "txt.security_policy.password_in_mixed_case.no"
      title: "Pulldown option to disable letters in mixed case check in passwords"
      value: "no"
  - translation:
      key: "txt.security_policy.password_in_mixed_case.yes"
      title: "Pulldown option to enable letters in mixed case check in passwords"
      value: "yes"
  - translation:
      key: "txt.security_policy.session_timeout.8_hours"
      title: "Pulldown option to set session timeout to 8 hours"
      value: "8 hours"
  - translation:
      key: "txt.security_policy.session_timeout.2_weeks"
      title: "Pulldown option to set session timeout to 2 weeks"
      value: "2 weeks"
  - translation:
      key: "txt.security_policy.password_duration.never"
      title: "Pulldown option to never expire password"
      value: "never"
  - translation:
      key: "txt.security_policy.attempts_before_lockout"
      title: "10 attempts allowed before lockout. Password security requirements option: https://zendesk.box.com/s/5398qezis30gj9q1f9u69xnuhywktfgy"
      screenshot: "https://zendesk.box.com/s/5398qezis30gj9q1f9u69xnuhywktfgy"
      value: "10 attempts allowed before lockout"
