title: "Request Page"
packages:
  - default
  - classic

parts:
  - translation:
      key: "txt.requests.title"
      title: "A request"
      value: "Request"
      packages: ["enduser"]
  - translation:
      key: "txt.requests.new.title"
      title: "Page title"
      value: "Submit a request for assistance"
  - translation:
      key: "txt.requests.new.header"
      title: "Page header"
      value: "Submit a request"
  - translation:
      key: "txt.requests.new.message"
      title: "Page help"
      value: "<p>Fields marked with an asterisk (<super>*</super>) are mandatory.</p><p>You'll be notified when our staff answers your request.</p>"
  - translation:
      key: "txt.requests.new.email_address"
      title: "Email address"
      value: "Your email address"
  - translation:
      key: "txt.uploads.header"
      title: "Attachments header"
      value: "Attachment(s)"
  - translation:
      key: "txt.uploads.create.link"
      title: "Attach file"
      value: "Attach file"
      obsolete: "2019-11-15"
  - translation:
      key: "txt.uploads.create.close"
      title: "Cancel attachment"
      value: "Close"
  - translation:
      key: "txt.uploads.create.uploading"
      title: "Attachment uploading"
      value: "Uploading"
      packages: ["lotus"]
      obsolete: "2019-11-15"
  - translation:
      key: "txt.uploads.create.too_big"
      title: "Attachment too big"
      value: "The file '{{filename}}' is too big. Please attach files that are less than {{max_size}}."
      obsolete: "2019-11-15"
  - translation:
      key: "txt.uploads.create.error"
      title: "Attachment error"
      value: "There was an error with the attachment. Try again or contact our Customer Service team if the problem continues."
      obsolete: "2019-11-15"
  - translation:
      key: "txt.uploads.create.max_size"
      title: "Max file size"
      value: "Max file size: {{max_size}}"
      obsolete: "2019-11-15"
  - translation:
      key: "txt.uploads.destroy.link"
      title: "Delete attachment"
      value: "remove"
      obsolete: "2019-11-15"
  - translation:
      key: "txt.uploads.show.quickview"
      title: "Quick view for pictures"
      value: "quick view"
  - translation:
      key: "txt.requests.new.submit"
      title: "Submit button"
      value: "Submit"
      obsolete: "2019-11-15"
  - translation:
      key: "txt.requests.new.created"
      title: "Request created"
      value: "Request {{request_title}} created"
  - translation:
      key: "txt.requests.new.verified"
      title: "Request verified"
      value: "{{request_title}}  successfully verified"
  - translation:
      key: "txt.requests.new.successfully_verified"
      title: "Request verified via email"
      value: "Your email has been successfully verified. Please sign in to see your request status."
  - translation:
      key: "txt.requests.new.error"
      title: "Failed to create request"
      value: "Request could not be created as"
  - translation:
      key: "txt.requests.new.error_invalid_date"
      title: "Invalid date"
      value: "Invalid date"
  - translation:
      key: "txt.requests.new.almost_done"
      title: "Almost done creating request, but there is one more step"
      value: "You're almost done creating your request"
  - translation:
      key: "txt.requests.new.verify_email_sent"
      title: "Explaining that an email has been sent to the requesters inbox for verification"
      value: "We sent you an email to verify your request because you are not signed in."
  - translation:
      key: "txt.requests.new.verify_login"
      title: "Explaining they can also login to verify the ticket, the link takes them to the login page"
      value: "You can also <a href=\"{{verify_link}}\">sign in now to verify your request</a>."
  - translation:
      key: "txt.requests.new.not_anonymous"
      title: "User already registered"
      value: "You are already registered. Please sign in to submit your request."
  - translation:
      key: "txt.requests.new.suspended_user"
      title: "User is suspended"
      value: "You have been suspended from this account. You are not allowed to submit requests at this time."
  - translation:
      key: "txt.requests.new.restricted_domain"
      title: "Domain not allowed"
      value: "Users from your domain are not allowed to submit tickets. Please sign in if you have an account."
  - translation:
      key: "txt.requests.new.captcha"
      title: "CAPTCHA validation title"
      value: "Please verify that you are human"
      screenshot: "https://zendesk.box.com/s/p7pj8tgcd9t1yqkutz3rkh8fd1qdiodh"
      packages: ["help_center", "enduser"]
  - translation:
      key: "txt.requests.new.captcha_msg"
      title: "CAPTCHA instruction line"
      value: "Type the above text in the box below"
      packages: ["help_center", "enduser"]
  - translation:
      key: "txt.requests.new.mobile.captcha_msg"
      title: "CAPTCHA instruction line"
      value: "Type the above text here"
      packages: ["help_center", "enduser"]
      obsolete: "2016-02-01"
  - translation:
      key: "txt.requests.new.captcha_error"
      title: "CAPTCHA Verification failed"
      value: "Verification failed. Please try with new text."
      packages: ["help_center", "enduser"]
      obsolete: '2016-01-01'
  - translation:
      key: "txt.requests.new.captcha_error_v2"
      title: "CAPTCHA Verification failed"
      value: "Verification failed. Please try again."
      packages: ["help_center", "enduser"]
  - translation:
      key: "txt.requests.new.captcha_reload"
      title: "Request new words"
      value: "Different text please"
      packages: ["help_center", "enduser"]
  - translation:
      key: "txt.requests.show.mobile.your_comment"
      title: "Your comment... placeholder"
      value: "Your comment..."
  - translation:
      key: "txt.requests.new.want_audio"
      title: "Audio instead of image"
      value: "I want audio instead"
  - translation:
      key: "txt.requests.new.want_image"
      title: "Image instead of audio"
      value: "I want an image instead"

# Request List Page

  - translation:
      key: "txt.requests.index.title_open"
      title: "Page title: open requests"
      value: "Open requests"
  - translation:
      key: "txt.requests.index.view_open"
      title: "Link to list of open requests"
      value: "View your open requests"
  - translation:
      key: "txt.requests.index.no_open"
      title: "No open requests message"
      value: "You have no open requests"
  - translation:
      key: "txt.requests.index.mobile.no_requests"
      title: "You have no requests"
      value: "You have no requests"
  - translation:
      key: "txt.requests.index.title_solved"
      title: "Page title: solved requests"
      value: "Recently solved and closed requests"
  - translation:
      key: "txt.requests.index.view_solved"
      title: "Link to list of recently solved requests"
      value: "View your recently solved and closed requests"
  - translation:
      key: "txt.requests.index.no_solved"
      title: "No solved requests message"
      value: "You have no recently solved or closed requests"
  - translation:
      key: "txt.requests.index.title_archived"
      title: "Page title: older closed requests"
      value: "Older closed requests"
  - translation:
      key: "txt.requests.index.view_archived"
      title: "Link to list of older closed requests"
      value: "View your older closed requests"
  - translation:
      key: "txt.requests.index.no_archived"
      title: "No older closed requests message"
      value: "You have no older closed requests"
  - translation:
      key: "txt.requests.index.message"
      title: "Information about solved tickets"
      value: "<p>You can reopen a solved request by clicking the request and adding a comment.</p>   <p>You cannot re-open a closed request.</p>"
  - translation:
      key: "txt.requests.index.title_cc"
      title: "Page title: CC requests"
      value: "Requests you are copied on"
  - translation:
      key: "txt.requests.index.view_copied"
      title: "Link to list of CC requests"
      value: "View requests you are copied on"
  - translation:
      key: "txt.requests.index.no_copied"
      title: "No CC requests message"
      value: "You are not copied on any requests"
  - translation:
      key: "txt.requests.index.mobile.my_open_requests"
      title: "My open requests"
      value: "My open requests"
  - translation:
      key: "txt.requests.index.mobile.my_closed_and_solved_requests"
      title: "My closed and solved requests"
      value: "My closed and solved requests"
  - translation:
      key: "txt.requests.side_box.title_solved"
      title: "This text appears at the top of the side column when you are viewing different types of tickets"
      value: "You are viewing a list of recently solved and closed requests"
  - translation:
      key: "txt.requests.side_box.title_cc"
      title: "This text appears at the top of the side column when you are viewing different types of tickets"
      value: "You are viewing a list of requests you are copied on"
  - translation:
      key: "txt.requests.side_box.title_archived"
      title: "This text appears at the top of the side column when you are viewing different types of tickets"
      value: "You are viewing a list of older closed requests"
  - translation:
      key: "txt.requests.side_box.title_open"
      title: "This text appears at the top of the side column when you are viewing different types of tickets"
      value: "You are viewing a list of open requests"
  - translation:
      key: "txt.requests.index.submitted"
      title: "Request submission time"
      value: "Submitted {{time_in_words}} ago"
  - translation:
      key: "txt.requests.index.status_new"
      title: "Request status: new"
      value: "Awaiting assignment to a support agent"
  - translation:
      key: "txt.requests.index.status_open"
      title: "Request status: open"
      value: "Being processed"
  - translation:
      key: "txt.requests.index.status_pending"
      title: "Request status: pending"
      value: "View request history"
  - translation:
      key: "txt.requests.index.awaiting"
      title: "Request status: awaiting"
      value: "Awaiting your response"
  - translation:
      key: "txt.requests.index.no_description"
      title: "Description does not exist"
      value: "Description does not exist"
  - translation:
      key: "txt.requests.index.submit_request"
      title: "Create new request"
      value: "Submit a request for assistance by our support staff"
  - translation:
      key: "txt.requests.index.message2"
      title: "Information about new requests"
      value: "<p> By submitting a request you <span class=\"highlight\">report a problem</span> to our support staff, who will review and process your request as soon as possible.  </p> <p>You can always return to this page to view the status of all your submitted requests.</p>"
      type: text_area

# Organization request List Page

  - translation:
      key: "txt.requests.organization.index.title_by"
      title: "Page title: requests by a user"
      value: "Requests by {{user}}"
  - translation:
      key: "txt.requests.organization.index.viewing"
      title: "You are viewing requests for an organization"
      value: "Requests for {{organization}}"
  - translation:
      key: "txt.requests.organization.index.view_open"
      title: "Link to list of open requests"
      value: "View open requests"
  - translation:
      key: "txt.requests.organization.index.view_solved"
      title: "Link to list of solved requests"
      value: "View solved and closed requests"
  - translation:
      key: "txt.requests.organization.index.view_by_user"
      title: "Link to list of requests by a user"
      value: "Requests by user"
  - translation:
      key: "txt.requests.organization.index.no_requests"
      title: "No requests found"
      value: "No requests found"
  - translation:
      key: "txt.requests.index.detailed_list"
      title: "Link to detailed list (with comments) of requests"
      value: "Detailed list"
  - translation:
      key: "txt.requests.index.compact_list"
      title: "Link to compact list (table) of requests"
      value: "Compact list"

# Show Request Page

  - translation:
      key: "txt.requests.show.title"
      title: "Show requests page title"
      value: "Request {{request_title}}"
  - translation:
      key: "txt.requests.show.status_new"
      title: "Request status: new"
      value: "This request is awaiting assignment to a support agent"
  - translation:
      key: "txt.requests.show.mobile.status_new"
      title: "Request status: new"
      value: "Awaiting assignment"
  - translation:
      key: "txt.requests.show.status_open"
      title: "Request status: open"
      value: "This request is currently being processed by our staff"
  - translation:
      key: "txt.requests.show.mobile.status_open"
      title: "Request status: open"
      value: "Assigned"
  - translation:
      key: "txt.requests.show.status_awaiting_you"
      title: "Request status: awaiting you"
      value: "This request is awaiting your response"
  - translation:
      key: "txt.requests.show.mobile.status_awaiting_you"
      title: "Request status: awaiting you"
      value: "Awaiting your response"
  - translation:
      key: "txt.requests.show.status_awaiting_thirdparty"
      title: "Request status: awaiting third-party"
      value: "This request is currently being processed"
  - translation:
      key: "txt.requests.show.mobile.status_awaiting_thirdparty"
      title: "Request status: awaiting awaiting thirparty"
      value: "Assigned"
  - translation:
      key: "txt.requests.show.status_solved"
      title: "Request status: solved"
      value: "This request has been deemed solved {{date}}"
  - translation:
      key: "txt.requests.show.mobile.status_solved"
      title: "Request status: solved"
      value: "Solved {{date}}"
  - translation:
      key: "txt.requests.show.comments"
      title: "Comments header"
      value: "Comments"
  - translation:
      key: "txt.requests.show.add_comment"
      title: "Comment field header"
      value: "Add a comment to this request"
  - translation:
      key: "txt.requests.show.awaiting_response"
      title: "Comment field header when awaiting your response"
      value: "Awaiting your response"
  - translation:
      key: "txt.requests.show.resolved"
      title: "Mark request as resolved"
      value: "Please consider this request <strong>resolved</strong>."
  - translation:
      key: "txt.requests.show.keep_solved"
      title: "Keep request resolved"
      value: "Request is <strong>solved</strong>."
  - translation:
      key: "txt.requests.assigned_to.title"
      title: "Title for the 'Assigned To' side component"
      value: "Assigned To"
      packages: ["enduser"]
  - translation:
      key: "txt.requests.show.is_assigned_to_your_ticket"
      title: "Assignment of your open request"
      value: "<strong>{{assignee_name}}</strong> is assigned to your request."
  - translation:
      key: "txt.requests.show.is_assigned_to_this_ticket"
      title: "Assignment of someone's open request"
      value: "<strong>{{assignee_name}}</strong> is assigned to this request."
  - translation:
      key: "txt.requests.show.was_assigned_to_your_ticket"
      title: "Assignment of your closed request"
      value: "<strong>{{assignee_name}}</strong> was assigned to your request."
  - translation:
      key: "txt.requests.show.was_assigned_to_this_ticket"
      title: "Assignment of someone's closed request"
      value: "<strong>{{assignee_name}}</strong> was assigned to this request."
      packages: ["enduser"]
  - translation:
      key: "txt.requests.show.notification"
      title: "Information on open requests"
      value: "You will receive a notification when the request status is updated by our staff."
  - translation:
      key: "txt.requests.show.reopen"
      title: "Information on closed requests"
      value: "You can reopen the request by adding a comment."
  - translation:
      key: "txt.requests.show.closed_for_comments"
      title: "Request is closed for comments"
      value: "This request is closed for comments."
  - translation:
      key: "txt.requests.show.has_followups"
      title: "Request has follow-up"
      value: "The request has a follow-up in"
  - translation:
      key: "txt.requests.show.you_can"
      title: "You can..."
      value: "You can"
  - translation:
      key: "txt.requests.show.create_a_followup"
      title: "Create a follow-up"
      value: "create a follow-up"
  - translation:
      key: "txt.requests.show.followup"
      title: "Follow-up to to a request"
      value: "Follow-up to"
  - translation:
      key: "txt.requests.show.ccs"
      title: "Information about copied people"
      value: "The following people will also be notified when this request is updated"
  - translation:
      key: "txt.requests.show.related_topics"
      title: "Related topics"
      value: "Related topics"
  - translation:
      key: "txt.requests.show.updated"
      title: "Request updated"
      value: "Request {{request_title}} updated"
  - translation:
      key: "txt.requests.show.error"
      title: "Failed to update request"
      value: "Request could not be updated as"
  - translation:
      key: "txt.requests.show.error_need_email"
      title: "Please provide us with a verified email address before submitting a new request"
      value: "Please <a href='/users/current/edit'>provide a verified email address</a> before submitting a new request."
  - translation:
      key: "txt.requests.show.error_blank_comment"
      title: "You cannot submit a blank comment"
      value: "You cannot submit a blank comment"
  - translation:
      key: "txt.requests.show.not_found"
      title: "This message displays when you cannot access a ticket. "
      value: "Request not found"
  - translation:
      key: "txt.requests.show.cannot_see_ticket"
      title: "You do not have access to see that ticket"
      value: "You do not have access to request #{{nice_id}}. It may have been solved or deleted."
      packages: ["enduser"]
  - translation:
      key: "txt.requests.show.has_followups_with_params"
      title: "Request has follow-up in {{name_of_the_ticket}}."
      value: "The request has a follow-up in {{name_of_the_ticket}}."
  - translation:
      key: "txt.requests.show.you_can_create_a_followup"
      title: "You can {{create_a_followup_link}}"
      value: "You can {{create_a_followup_link}}."
  - translation:
      key: "txt.requests.show.you_can_create_a_followup_link"
      title: "Create a follow-up"
      value: "create a follow-up"
  - translation:
      key: "txt.requests.show.you_can_create_a_followup_v2"
      title: "You can {{create_a_followup_link}} &#187;"
      value: "You can {{create_a_followup_link}} &#187;"
  - translation:
      key: "txt.requests.show.you_can_create_a_followup_v2_link"
      title: "Create a follow-up"
      value: "create a follow-up"
  - translation:
      key: "txt.requests.show.followup_with_params"
      title: "Note: If possible maintain the lowecase. follow-up to {{ticket_number_and_title_link}}"
      value: "follow-up to {{followup_with_params_link}}"
  - translation:
      key: "txt.requests.followup_with_params_link"
      title: "request #{{ticket_id}}"
      value: "request #{{ticket_id}}"
      packages: ["enduser"]
  - translation:
      key: "txt.requests.form.new.continue"
      title: "Continue"
      value: "Continue"
