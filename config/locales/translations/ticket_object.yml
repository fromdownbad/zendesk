title: "Tickets"
packages:
  - default
  - classic

parts:
  - translation:
      key: "ticket.name"
      title: "Ticket"
      value: "Ticket"
  - translation:
      key: "ticket.comment.flagged_reason_other_user_update"
      title: "The reason that Zendesk believes the comment should not be trusted; the comment may not have been submitted by the person the interface is showing"
      value: "{{comment_author}} was not explicitly added to the ticket by the requester or an agent. {{learn_more}}"
  - translation:
      key: "ticket.comment.flagged_reason_registered_user_logged_out"
      title: "Comment author was not signed in when this comment was submitted (what is this?)"
      value: "{{comment_author}} was not signed in when this comment was submitted. {{learn_more}}"
  - translation:
      key: "ticket.comment.flagged_reason_machine_generated"
      title: "The reason that Zendesk believes the comment should be flagged; the comment was automatically generated and notifications to this user should be suppressed. Before Automatic, an icon will be loaded (do not delete)"
      value: "This comment was automatically generated. Automatic email notifications have been suppressed. {{learn_more}}"
  - translation:
      key: "ticket.comment.flagged_reason_attachment_too_big"
      title: "The reason we're showing a warning flag; the attachment was larger than the allowed limit"
      value: "Attached file {{file}} was rejected because it is too big. Your account has an attachment size limit of {{account_limit}} MB."
  - translation:
      key: "ticket.comment.flagged_reason_on_behalf_of_user"
      title: "The reason we're showing a warning flag; This comment was submitted by a user on behalf of the comment author"
      value: "This comment was submitted by {{user}} on behalf of the author"
  - translation:
      key: "ticket.comment.flagged_reason_potential_message_spoofing"
      title: "The reason we're showing a warning flag; The message may be an attempt to impersonate the email address it appears to be coming from"
      value: "This message might not have been sent by {{userEmail}}"
  - translation:
      key: "ticket.comment.flagged_reason_email_ccs_exceeded_limit"
      title: "The reason why email addresses were not added to CC. Number of user records (means the number of ccs) has exceeded the allowed limit."
      value: "Some email recipients were excluded from CCs due to the limit of {{max_ccs_limit}} CCs per email. View the original email for the full list. {{learn_more}}"
      screenshot: "https://drive.google.com/file/d/1-w64XvdkTIyHPUmo3Njdz507lcJxh9tE"
  - translation:
      key: "ticket.comment.flagged_link_email_ccs_exceeded_limit"
      title: "The help center article that shows when clicking the comment flagged for reason 18 (email_ccs_exceeded_limit)."
      value: "https://support.zendesk.com/hc/en-us/articles/203690846"
      hidden: true
  - translation:
      key: "ticket.comment.flagged_reason_light_agent_suspension_recovery_ignore_ccs"
      title: "The reason we're showing a warning flag: comment created by light agent recovering a suspended ticket, so we did not update the ticket's CCs."
      value: "A light agent recovered this comment from suspension, so CCs from the suspended email were not added to the ticket. {{learn_more}}"
      screenshot: "https://drive.google.com/file/d/1pM61QZLvivLEmd7lEqylEui_bLGsIqkQ"
      obsolete: "2020-07-29"
  - translation:
      key: "ticket.comment.flagged_link_light_agent_suspension_recovery_ignore_ccs"
      title: "The help center article that shows when clicking the comment flagged for reason 19 (light_agent_suspension_recovery_ignore_ccs)."
      value: "https://support.zendesk.com/hc/en-us/articles/203691046"
      hidden: true
      obsolete: "2020-07-29"
  - translation:
      key: "ticket.comment.flagged_reason_default"
      title: "The reason that Zendesk believes the comment should not be trusted; Zendesk is unsure this comment should be trusted"
      value: "This comment has been flagged. {{learn_more}}"
  - translation:
      key: "ticket.comment.flagged_link_other_user_update"
      title: "The help center article that shows when clicking the comment flagged for reason 2 (other_user_update)."
      value: "https://support.zendesk.com/hc/en-us/articles/203661606#topic_d32_mzc_3r"
      hidden: true
  - translation:
      key: "ticket.comment.flagged_link_registered_user_logged_out"
      title: "The help center article that shows when clicking the comment flagged for reason 3 (registered_user_logged_out)."
      value: "https://support.zendesk.com/hc/en-us/articles/203661466-Sharing-tickets-with-other-Zendesk-Support-accounts"
      hidden: true
  - translation:
      key: "ticket.comment.flagged_link_machine_generated"
      title: "The help center article that shows when clicking the comment flagged for reason 4 (flagged_comments_machine_generated)."
      value: "https://support.zendesk.com/hc/en-us/articles/218137678#topic_kh1_5d1_qv7"
      hidden: true
  - translation:
      key: "ticket.comment.flagged_link_default"
      title: "The help center article that shows when clicking the comment flagged for a default reason."
      value: "https://support.zendesk.com/hc/en-us/articles/203663756#topic_nr4_4s5_cq"
      hidden: true
  - translation:
      key: "ticket.comment.flagged_learn_more"
      title: "Learn more"
      value: "Learn more"
  - translation:
      key: "txt.tickets.comment.flagged.reason.other_user_update"
      title: "Comment author was not explicitly added to the ticket by the requester or an agent (what is this?)"
      value: "Comment author was not a part of this conversation. {{learn_more}}"
      obsolete: "2019-10-31"
  - translation:
      key: "txt.tickets.comment.flagged.reason.registered_user_logged_out"
      title: "Comment author was not signed in when this comment was submitted (what is this?)"
      value: "Comment author was not signed in when this comment was submitted. {{learn_more}}"
      obsolete: "2019-10-31"
  - translation:
      key: "txt.tickets.comment.flagged.reason.requester_name_truncated"
      title: "The requester name length exceeds the allowed limit"
      value: "Requester's name has been truncated. Please edit the name or mark as spam."
      obsolete: "2019-11-01"
  - translation:
      key: "ticket.comment.flagged_reason_requester_name_truncated"
      title: "The requester name length exceeds the allowed limit"
      value: "Requester's name has been truncated. Please edit the name or mark as spam."
  - translation:
      key: "ticket.comment.flagged_link_requester_name_truncated"
      title: "The help center article that shows when clicking the comment flagged for reason 16 (requester_name_truncated)."
      value: "https://support.zendesk.com/hc/en-us/articles/203661606#topic_d32_mzc_3r"
      hidden: true
      wip: true
  - translation:
      key: "ticket.comment.flagged_reason_private_comment_requester_missing"
      title: "When the comment author is an email cc and the original requester was excluded in the list of recipients, a private comment is created."
      value: "This is a private comment created by an end user. {{learn_more}}"
      screenshot: "https://drive.google.com/open?id=10BcuBqEf9N6rkPrE_eyH2cmq5bBlfOZ6"
  - translation:
      key: "ticket.comment.flagged_link_private_comment_requester_missing"
      title: "The help center article that shows when clicking the comment flagged for reason 17 (private_comment_requester_missing)."
      value: "https://support.zendesk.com/hc/en-us/articles/203661606#topic_d32_mzc_3r"
      hidden: true
      wip: true
  - translation:
      key: "ticket.comment.flagged_reason_reply_to_agent_possible_spoof"
      title: "When the comment author is an agent set in the reply_to but not in the from of an email header."
      value: "To protect your agents, we suspended the ability of the user in this Reply-to address to perform certain actions. {{learn_more}}"
      screenshot: "https://drive.google.com/file/d/1Gk3Epz6a7V_QJG3dzZIbxwbT9kpwAmAQ/view?usp=sharing"
  - translation:
      key: "ticket.comment.flagged_link_reply_to_agent_possible_spoof"
      title: "The help center article that shows when clicking the comment flagged for reason 20 (reply_to_agent_possible_spoof)."
      value: "https://support.zendesk.com/hc/en-us/articles/360057609553"
      hidden: true
  - translation:
      key: "ticket.comment.flagged_reason_reply_to_end_user_possible_spoof"
      title: "When the comment author is an end user set in the reply_to but not in the from of an email header."
      value: "We flagged this comment because the From and Reply-to in the messages don’t match. {{learn_more}} about Reply-to addresses"
      screenshot: "https://drive.google.com/file/d/1Q3OCGFnBaqQFVl92-Ekm0B3BLRsWbA-7/view?usp=sharing"
  - translation:
      key: "ticket.comment.flagged_link_reply_to_end_user_possible_spoof"
      title: "The help center article that shows when clicking the comment flagged for reason 21 (reply_to_agent_end_user_spoof)."
      value: "https://support.zendesk.com/hc/en-us/articles/360057609553"
      hidden: true
  - translation:
      key: "txt.tickets.comment.flagged.reason.default"
      title: "Comment has been flagged (what is this?)"
      value: "Comment has been flagged. {{learn_more}}"
      obsolete: "2019-10-31"
  - translation:
      key: "ticket.comment.untrusted_learn_more"
      title: "What is this?"
      value: "What is this?"
  - translation:
      key: "ticket.comment.untrusted_learn_more_link"
      title: "link to untrusted comment docs"
      value: "https://support.zendesk.com/hc/en-us/articles/203663756#topic_nr4_4s5_cq"
      hidden: true
  - translation:
      key: "ticket_fields.subject.label"
      title: "Ticket field: Subject"
      value: "Subject"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.description.label"
      title: "Ticket field: Description"
      value: "Description"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.status.label"
      title: "Ticket field: Status"
      value: "Status"
      packages: ["lotus"]
  - translation:
      key: 'type.status.new'
      title: "Ticket status values"
      value: "New"
      packages: ["lotus"]
  - translation:
      key: 'type.status.open'
      title: "Ticket status values"
      value: "Open"
      packages: ["lotus"]
  - translation:
      key: 'type.status.pending'
      title: "Ticket status values"
      value: "Pending"
      packages: ["lotus"]
  - translation:
      key: 'type.status.hold'
      title: "Ticket status values"
      value: "On-hold"
      packages: ["lotus"]
  - translation:
      key: 'type.status.solved'
      title: "Ticket status values"
      value: "Solved"
      packages: ["lotus"]
  - translation:
      key: 'type.status.closed'
      title: "Ticket status values"
      value: "Closed"
      packages: ["lotus"]
  - translation:
      key: 'type.status.archived'
      title: "Ticket status values"
      value: "Archived"
      packages: ["lotus"]
  - translation:
      key: 'type.status.deleted'
      title: "Ticket status values"
      value: "Deleted"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.type.label"
      title: "Ticket field: Type"
      value: "Type"
      packages: ["lotus"]
  - translation:
      key: 'type.ticket.question'
      title: "Ticket type values"
      value: "Question"
      packages: ["lotus"]
  - translation:
      key: 'type.ticket.incident'
      title: "Ticket type values"
      value: "Incident"
      packages: ["lotus"]
  - translation:
      key: 'type.ticket.problem'
      title: "Ticket type values"
      value: "Problem"
      packages: ["lotus"]
  - translation:
      key: 'type.ticket.task'
      title: "Ticket type values"
      value: "Task"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.priority.label"
      title: "Ticket field: Priority"
      value: "Priority"
      packages: ["lotus"]
  - translation:
      key: 'type.priority.low'
      title: "Ticket priority values"
      value: "Low"
      packages: ["lotus"]
  - translation:
      key: 'type.priority.normal'
      title: "Ticket priority values"
      value: "Normal"
      packages: ["lotus"]
  - translation:
      key: 'type.priority.high'
      title: "Ticket priority values"
      value: "High"
      packages: ["lotus"]
  - translation:
      key: 'type.priority.urgent'
      title: "Ticket priority values"
      value: "Urgent"
      packages: ["lotus"]
  - translation:
      key: 'type.priority.undefined'
      title: "Ticket priority values"
      value: "Not Set"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.privacy.label"
      title: "Ticket field: Privacy"
      value: "Privacy"
      screenshot: 'https://zendesk.box.com/s/p81wu0zwyuwh5pb2h6g14cch3zgv0tvn'
      packages: ["lotus"]
  - translation:
      key: 'type.privacy.public'
      title: "Ticket privacy values"
      value: "Ticket has public comments"
      screenshot: 'https://zendesk.box.com/s/p81wu0zwyuwh5pb2h6g14cch3zgv0tvn'
      packages: ["lotus"]
  - translation:
      key: 'type.privacy.private'
      title: "Ticket privacy values"
      value: "Ticket has no public comments"
      screenshot: 'https://zendesk.box.com/s/p81wu0zwyuwh5pb2h6g14cch3zgv0tvn'
      packages: ["lotus"]
  - translation:
      key: 'type.satisfaction.unoffered'
      title: "Ticket satisfaction rating values.  These values (i.e. Good, Bad) are predicate nominatives: [The satisfaction rating is] Good"
      value: "Unoffered"
      packages: ["lotus"]
  - translation:
      key: 'type.satisfaction.offered'
      title: "Ticket satisfaction rating values.  These values (i.e. Good, Bad) are predicate nominatives: [The satisfaction rating is] Good"
      value: "Offered"
      packages: ["lotus"]
  - translation:
      key: 'type.satisfaction.bad'
      title: "Ticket satisfaction rating values.  These values (i.e. Good, Bad) are predicate nominatives: [The satisfaction rating is] Good"
      value: "Bad"
      packages: ["lotus"]
  - translation:
      key: 'type.satisfaction.good'
      title: "Ticket satisfaction rating values.  These values (i.e. Good, Bad) are predicate nominatives: [The satisfaction rating is] Good"
      value: "Good"
      packages: ["lotus"]
  - translation:
      key: 'type.satisfaction.good_with_comment'
      title: "Ticket satisfaction rating values.  These values (i.e. Good, Bad) are predicate nominatives: [The satisfaction rating is] Good"
      value: "Good with Comment"
      packages: ["lotus"]
  - translation:
      key: 'type.satisfaction.bad_with_comment'
      title: "Ticket satisfaction rating values.  These values (i.e. Good, Bad) are predicate nominatives: [The satisfaction rating is] Good"
      value: "Bad with Comment"
      packages: ["lotus"]
  - translation:
      key: 'type.satisfaction_reason_code.none'
      title: "Ticket satisfaction_reason_code values"
      value: "No reason provided"
      packages: ["lotus", "enduser"]
  - translation:
      key: 'type.satisfaction_reason_code.issue_took_too_long'
      title: "Ticket satisfaction_reason_code values"
      value: "The issue took too long to resolve"
      packages: ["lotus", "enduser"]
  - translation:
      key: 'type.satisfaction_reason_code.issue_not_resolved'
      title: "Ticket satisfaction_reason_code values"
      value: "The issue was not resolved"
      packages: ["lotus", "enduser"]
  - translation:
      key: 'type.satisfaction_reason_code.agent_knowledge_unsatisfactory'
      title: "Ticket satisfaction_reason_code values"
      value: "The agent's knowledge is unsatisfactory"
      packages: ["lotus", "enduser"]
  - translation:
      key: 'type.satisfaction_reason_code.agent_attitude_unsatisfactory'
      title: "Ticket satisfaction_reason_code values"
      value: "The agent's attitude is unsatisfactory"
      packages: ["lotus", "enduser"]
  - translation:
      key: 'type.satisfaction_reason_code.some_other_reason'
      title: "Ticket satisfaction_reason_code values"
      value: "Some other reason"
      packages: ["lotus", "enduser"]
  - translation:
      key: "ticket_fields.group.label"
      title: "Ticket field: Group"
      value: "Group"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.assignee.label"
      title: "Ticket field: Assignee"
      value: "Assignee"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.assigned.status"
      title: "Ticket field: Assignee"
      value: "assigned"
  - translation:
      key: "ticket_fields.requester.label"
      title: "Ticket field: Requester"
      value: "Requester"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.submitter.label"
      title: "Ticket field: Submitter"
      value: "Submitter"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.score.label"
      title: "Ticket field: Score"
      value: "Score"
  - translation:
      key: "ticket_fields.nice_id.label"
      title: "Ticket field: Id"
      value: "ID"
  - translation:
      key: "ticket_fields.created.label"
      title: "Ticket field: Created at"
      value: "Requested"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.updated.label"
      title: "Ticket field: Updated at"
      value: "Updated"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.updated_requester.label"
      title: "Ticket field: Requester updated at"
      value: "Requester updated"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.updated_assignee.label"
      title: "Ticket field: Assignee updated at"
      value: "Assignee updated"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.assigned.label"
      title: "Ticket field: Assigned at"
      value: "Assigned"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.latest_comment_added_at.label"
      title: "Ticket field: Comment updated at"
      value: "Latest comment"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.latest_public_comment_added_at.label"
      title: "Ticket field: Public comment updated at"
      value: "Latest public comment"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.latest_agent_comment_added_at.label"
      title: "Ticket field: Agent comment updated at"
      value: "Latest agent comment"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.solved.label"
      title: "Ticket field: Solved at"
      value: "Solved"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.due_date.label"
      title: "Ticket field: Due date"
      value: "Due date"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.organization.label"
      title: "Ticket field: Organization"
      value: "Organization"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.organization.description"
      title: "Ticket field description: Organization"
      value: "Select the Organization you would like to associate with this request. You can change this later."
  - translation:
      key: "ticket_fields.updated_by_type.label"
      title: "Ticket field: Last updater"
      value: "Updater"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.linked.label"
      title: "Ticket field: Linked problem"
      value: "Linked problem"
  - translation:
      key: "ticket_fields.current_tags.label"
      title: "Ticket field: Tags"
      value: "Tags"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.satisfaction_score.label"
      title: "Ticket field: Satisfaction Rating"
      value: "Satisfaction"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.satisfaction_reason_code.label"
      title: "Ticket field: Satisfaction Reason Code. Please use Satisfaction as feature name (use upper case), same as key: txt.views.reporting.satisfaction_pane.name. Reason given for the Customer Satisfaction Rating."
      value: "Satisfaction Reason"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.ticket_form.label"
      title: "Ticket field: Ticket form"
      value: "Ticket form"
      packages: ["lotus"]

# Merging

  - translation:
      key: "ticket_fields.current_collaborators.label"
      title: "Ticket field: Collaborators"
      value: "CCs"
  - translation:
      key: "ticket_fields.current_collaborators.new_users_not_allowed"
      title: "New CC users not allowed, were removed"
      value: "This user does not have permission to create new user records. The following email addresses have been removed from CC: {{removed_ccs}}"
  - translation:
      key: "ticket_fields.merge.default_target_comment.bulk"
      title: "Default target comment for bulk merge"
      value: "Requests {{source_ids}} were closed and merged into this request."
  - translation:
      key: 'ticket_fields.merge.default_target_comment.non_bulk'
      title: "Default target comment for non-bulk merge"
      value: "Request #{{source_nice_id}} \"{{source_title}}\" was closed and merged into this request. Last comment in request #{{source_nice_id}}:\n\n"
  - translation:
      key: 'ticket_fields.merge.source_comment'
      title: "Source comment for ticket merge"
      value: "This request was closed and merged into request #{{target_nice_id}} \"{{target_title}}\"."
  - translation:
      key: 'ticket_fields.merge.private_comment'
      title: "Telling the user that all comments made by the merge procedure will be private and not end user visible"
      screenshot: "https://zendesk.box.com/s/8knjzc1gh921y1yyx6ivm0mjxj8uo5b2"
      value: "This comment will be private."
  - translation:
      key: "ticket_fields.locale_id.label"
      title: "Ticket field: Requester language"
      value: "Requester language"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.locale.label"
      title: "Ticket field: Requester language"
      value: "Requester language"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.via.label"
      title: "Ticket field: Channel"
      value: "Channel"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.via_reference.label"
      title: "Ticket field: Subchannel. Can be any added channel integration. "
      screenshot: "https://zendesk.box.com/s/wmvwh2oaxakoguiv3kt07npe3pulzsmr"
      value: "Integration account"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.current_tags.too_long_truncated"
      title: "Tags were too long, were truncated"
      value: "Tags were too long, were truncated to fit the limit. Excluded tags: #{{pruned_tags}}"
  - translation:
      key: 'ticket.followup.comment_with_title'
      title: "Default comment for ticket follow-up"
      value: "This is a follow-up to your previous request #{{source_ticket_nice_id}} \"{{source_ticket_title}}\""
  - translation:
      key: "ticket.followup.subject"
      title: "Default subject for ticket follow-up"
      value: "Re: {{source_ticket_subject}}"
      packages: ["lotus"]
  - translation:
      key: 'ticket.followup.comment_with_params'
      title: 'This is a follow-up to your previous request #{{ticket_number}} "{{ticket_title}}"'
      value: 'This is a follow-up to your previous request #{{ticket_number}} "{{ticket_title}}"'
  - translation:
      key: 'ticket.validation.open_twitter_ticket'
      title: "this text will appear in a link : 'open Twitter ticket' (open is an adjective)"
      value: "open Twitter ticket"
  - translation:
      key: 'comment_draft.recovered_a_comment_from_draft'
      title: "Recovered a comment from draft"
      value: "Recovered a comment from draft"
      js: true
  - translation:
      key: "ticket_fields.brand.label"
      title: "Ticket field: Brand"
      value: "Brand"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.sla_next_breach_at.label"
      title: "Ticket field: Next SLA"
      value: "Next SLA breach"
      packages: ["lotus"]
  - translation:
      key: "ticket_fields.ticket_sharing.ssl_failure"
      title: "Added to audit when ticket sharing fails due to SSL. A ticket can be shared among for example, two Zendesk accounts. The update was not succesful on that shared ticket."
      value: "Shared ticket update failed due to an SSL error"
  - translation:
      key: "ticket_fields.ticket_sharing.dns_failure"
      title: "Added to audit when ticket sharing fails due to DNS. A ticket can be shared among for example, two Zendesk accounts. The update was not succesful on that shared ticket."
      value: "Shared ticket update failed due to a DNS error"
  - translation:
      key: "ticket_fields.attributes_match.label"
      title: "A column in a Ticket View which indicates if the Ticket can be answered by the Agent, based on the known skills of the Agent"
      value: "Skill match"
      screenshot: 'https://zendesk.box.com/s/f841z2xlf22g4caoqv9mratr64fuc2fg'
  - translation:
      key: "ticket.comment.channelback_failed.growl"
      title: "Growl to show an Agent when they perform a Channelback, but the Channelback has asynchronously failed"
      value: "Message failed: %{error_message}"
      screenshot: 'https://drive.google.com/open?id=1ZINws-UYFjfJaH7-ohyEWzN30CRhou4g'
  - translation:
      key: "ticket.comment.channelback_retrying.growl"
      title: "Growl to show an Agent when they perform a Channelback, but the Channelback has triggered a retry error"
      value: "Retrying message: %{error_message}"
      screenshot: 'https://drive.google.com/file/d/1qtus35XMYSEcw70DLv3dX1tAg-iUkv8R'
  - translation:
      key: "ticket.comment.omniredaction.image_redacted"
      title: "This message is displayed in the ticket log to inform the agent that an inline image attachment belonging to this comment has been redacted"
      value: "Image redacted"
      screenshot: "https://drive.google.com/file/d/10sIu1P9CPYLyA0T_uq3FHlOedfJumdPf/view?usp=sharing"
  - translation:
      key: 'type.custom_status.new.description'
      title: "Description for default custom status New"
      value: "Ticket is awaiting assignment to an agent"
      screenshot: "https://drive.google.com/file/d/1TTfOb41xAlixlYvhwCgG0atJaeGy0Mwc/view?usp=sharing"
  - translation:
      key: 'type.custom_status.open.description'
      title: "Description for default custom status Open"
      value: "Staff is working on the ticket"
      screenshot: "https://drive.google.com/file/d/1BjDL5wVrCgYjhUG1SM31DsI8AVgix3H3/view?usp=sharing"
  - translation:
      key: 'type.custom_status.pending.description'
      title: "Description for default custom status Pending"
      value: "Staff is waiting for the requester to reply"
      screenshot: "https://drive.google.com/file/d/1kjk7HiWr63i4dj6tNkfuPJecGGvOTFa4/view?usp=sharing"
  - translation:
      key: "type.custom_status.hold.description"
      title: "Description for default custom status On-hold"
      value: "Staff is waiting for a third party"
      screenshot: "https://drive.google.com/file/d/1SP7tin6waKnVXuQ-4rvH-peaf-z2o44_/view?usp=sharing"
  - translation:
      key: 'type.custom_status.solved.description'
      title: "Description for default custom status Solved"
      value: "The ticket has been solved"
      screenshot: "https://drive.google.com/file/d/1l4QRejhFJgwH1ki2JI6HMWROufVNUvCq/view?usp=sharing"
