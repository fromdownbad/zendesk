title: "SLA Admin Page"
packages:
  - default
  - admin
  - classic
  - sla

parts:
  - translation:
      key: 'txt.admin.slas.title'
      title: "Title for slas admin page."
      value: "Service Level Agreements"
      js: true
  - translation:
      key: 'txt.admin.slas.page_heading'
      title: "Heading for slas admin page."
      value: "Service Level Agreements"
      js: true
  - translation:
      key: 'txt.admin.slas.page_description'
      title: "Description for slas admin page."
      value: "A service level agreement (SLA) is a contract between you and your customers that specifies performance measures for support by ticket priority. For example, we respond to urgent tickets in ten minutes and resolve them within two hours. Your SLA policies are applied to tickets in the order they appear on this page, so drag to reorder as needed."
      js: true
  - translation:
      key: 'txt.admin.slas.page_link'
      title: "Link to learn how slas work"
      value: "https://support.zendesk.com/hc/en-us/articles/204770038"
      js: true
      hidden: true
  - translation:
      key: 'txt.admin.learn_more'
      title: "Learn more label."
      value: "Learn more"
      js: true
  - translation:
      key: 'txt.admin.slas.policies_heading'
      title: "Heading for slas policy section in slas admin page."
      value: "SLA policies"
      js: true
  - translation:
      key: 'txt.admin.slas.not_setup_heading'
      title: "Heading for accounts with priority field deactivated or no policies. https://zendesk.box.com/s/fpb428no4ydvud2gmanhxaiqib1reu65"
      screenshot: "https://zendesk.box.com/s/fpb428no4ydvud2gmanhxaiqib1reu65"
      value: "SLAs aren't set up yet"
      js: true
  - translation:
      key: "txt.admin.slas.no_priorities_subheading"
      title: "Subheading for accounts with priority field deactivated."
      value: "To get started with SLAs, activate the Zendesk Support system priority field."
      js: true
  - translation:
      key: 'txt.admin.slas.activate_priority'
      title: "Call to action for navigating to ticket fields admin page."
      value: "Activate priority"
      js: true
  - translation:
      key: 'txt.admin.slas.no_policies_subheading'
      title: "Subheading for accounts without policies. https://zendesk.box.com/s/fpb428no4ydvud2gmanhxaiqib1reu65"
      screenshot: "https://zendesk.box.com/s/fpb428no4ydvud2gmanhxaiqib1reu65"
      value: "Get started with SLAs to help set standards and raise the bar for your whole team."
      js: true
  - translation:
      key: 'txt.admin.slas.add_policy'
      title: "Call to action for adding an SLA policy."
      value: "Add policy"
      js: true
  - translation:
      key: 'txt.admin.slas.policy_name_label'
      title: "Label for SLA policy name field."
      value: "Policy Name"
      js: true
  - translation:
      key: 'txt.admin.slas.policy_description_label'
      title: "Label for SLA policy description field."
      value: "Description"
      js: true
  - translation:
      key: 'txt.admin.slas.policy_heading_placeholder'
      title: "Placeholder for new SLA policy heading."
      value: "New SLA policy"
      js: true
  - translation:
      key: 'txt.admin.slas.policy_name_placeholder'
      title: "Placeholder for policy name input."
      value: "Enter a name for this policy"
      js: true
  - translation:
      key: 'txt.admin.slas.policy_description_placeholder'
      title: "Placeholder for policy description input."
      value: "Enter an optional description"
      js: true
  - translation:
      key: 'txt.admin.slas.target_placeholder'
      title: "Placeholder for SLA target duration field. https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      screenshot: "https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      value: "Type time duration"
      js: true
  - translation:
      key: 'txt.admin.slas.target_days_label'
      title: "Label for SLA target days field."
      value: "days"
      js: true
  - translation:
      key: 'txt.admin.slas.target_hours_label'
      title: "Label for SLA target hours field."
      value: "hours"
      js: true
  - translation:
      key: 'txt.admin.slas.target_mins_label'
      title: "Label for SLA target mins field."
      value: "mins"
      js: true
  - translation:
      key: 'txt.admin.form.actions.save'
      title: "Text for admin form save button."
      value: "Save"
      js: true
  - translation:
      key: 'txt.admin.form.actions.cancel'
      title: "Text for admin form cancel button."
      value: "Cancel"
      js: true
  - translation:
      key: 'txt.admin.slas.targets_heading'
      title: "Heading for sla targets section."
      value: "Targets"
      js: true
  - translation:
      key: 'txt.admin.slas.targets_description'
      title: "Description for sla targets section."
      value: "For each metric, set a time target for each ticket priority. Choose to measure targets in calendar or business hours."
      js: true
  - translation:
      key: 'txt.admin.slas.target_urgent_heading'
      title: "Heading for targets of urgent ticket priority."
      value: "Urgent"
      js: true
  - translation:
      key: 'txt.admin.slas.target_high_heading'
      title: "Heading for targets of high ticket priority."
      value: "High"
      js: true
  - translation:
      key: 'txt.admin.slas.target_normal_heading'
      title: "Heading for targets of normal ticket priority."
      value: "Normal"
      js: true
  - translation:
      key: 'txt.admin.slas.target_low_heading'
      title: "Heading for targets of low ticket priority."
      value: "Low"
      js: true
  - translation:
      key: "txt.admin.slas.target_first_reply_time_heading"
      title: "Heading for the `First reply time` metric section. See a description of the metric under this key: txt.admin.slas.metric_tooltip.first_reply_time_description."
      screenshot: "https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      value: "First reply time"
      js: true
  - translation:
      key: "txt.admin.slas.target_requester_wait_time_heading"
      title: "Heading for the `Requester wait time` metric section. See a description of the metric under this key: txt.admin.slas.metric_tooltip.requester_wait_time_description."
      screenshot: "https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      value: "Requester wait time"
      js: true
  - translation:
      key: "txt.admin.slas.target_agent_work_time_heading"
      title: "Heading for the `Agent work time` metric section. See a description of the metric under this key: txt.admin.slas.metric_tooltip.agent_work_time_description."
      screenshot: "https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      value: "Agent work time"
      js: true
  - translation:
      key: "txt.admin.slas.target_next_reply_time_heading"
      title: "Heading for the `Next reply time` metric section. See a description of the metric under this key: txt.admin.slas.metric_tooltip.next_reply_time_description."
      screenshot: "https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      value: "Next reply time"
      js: true
  - translation:
      key: "txt.admin.slas.target_pausable_update_time_heading"
      title: "Heading for the `Pausable update` metric section. See a description of the metric under this key: txt.admin.slas.metric_tooltip.pausable_update_time_description."
      screenshot: "https://zendesk.box.com/s/67g5npuszseb465p35rip0efzfw41z5i"
      value: "Pausable update"
      js: true
  - translation:
      key: 'txt.admin.slas.target_periodic_update_time_heading'
      title: "Heading for the `Periodic update` metric section. See a description of the metric under this key: txt.admin.slas.metric_tooltip.periodic_update_time_description."
      value: "Periodic update"
      js: true
  - translation:
      key: 'txt.admin.slas.metric_tooltip.first_reply_time_description'
      title: "Description of first reply time policy metric. Similar to https://zendesk.box.com/s/5qr3drixpdk1s6x1n26zpbk9qan58r8c for first reply time metric."
      screenshot: "https://zendesk.box.com/s/5qr3drixpdk1s6x1n26zpbk9qan58r8c"
      value: "The time between the first customer comment and the first public comment from an agent, displayed in minutes. First reply time will not be measured on tickets created by an agent."
      js: true
  - translation:
      key: 'txt.admin.slas.metric_tooltip.requester_wait_time_description'
      title: "Description of requester wait time policy metric.UI similar to: https://zendesk.box.com/s/vee6ep383b8twu45glrruijrdhhljwwc for Wait time metric."
      screenshot: "https://zendesk.box.com/s/vee6ep383b8twu45glrruijrdhhljwwc"
      value: "The combined total time spent in the New, Open, and On-hold statuses. The SLA will pause on Pending."
      js: true
  - translation:
      key: 'txt.admin.slas.metric_tooltip.agent_work_time_description'
      title: "Description of agent work time policy metric. UI similar to: https://zendesk.box.com/s/vee6ep383b8twu45glrruijrdhhljwwc for Work time metric."
      screenshot: "https://zendesk.box.com/s/vee6ep383b8twu45glrruijrdhhljwwc"
      value: "The combined total time spent in the New and Open statuses. The SLA will pause on Pending and On-hold."
      js: true
  - translation:
      key: 'txt.admin.slas.metric_tooltip.next_reply_time_description'
      title: "Description of next reply time policy metric. Similar to https://zendesk.box.com/s/5qr3drixpdk1s6x1n26zpbk9qan58r8c for Next reply time metric."
      screenshot: "https://zendesk.box.com/s/5qr3drixpdk1s6x1n26zpbk9qan58r8c"
      value: "The time between the oldest, unanswered customer comment and the next public comment from an agent, displayed in minutes."
      js: true
  - translation:
      key: "txt.admin.slas.metric_tooltip.pausable_update_time_description"
      title: "Description of the `Pausable update` metric."
      screenshot: "https://zendesk.box.com/s/67g5npuszseb465p35rip0efzfw41z5i"
      value: "The time between each public comment from agents, displayed in minutes. The SLA will pause on Pending."
      js: true
  - translation:
      key: 'txt.admin.slas.metric_tooltip.periodic_update_time_description'
      title: "Description of periodic update policy metric."
      value: "The time between each public comment from agents, displayed in minutes."
      js: true
  - translation:
      key: "txt.admin.slas.metric_tooltip.learn_more_link_text"
      title: "Text for link to learn more about metrics."
      value: "Learn more about metrics"
      js: true
  - translation:
      key: 'txt.admin.slas.target_hours_of_operation_heading'
      title: "Heading for hours of operation options. https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      screenshot: "https://zendesk.box.com/s/qy3ymzqc44il25qubmb0vbjkmt6470x5"
      value: "Hours of operation"
      js: true
  - translation:
      key: 'txt.admin.slas.target_calendar_hours_option'
      title: "Calendar hours option for targets."
      value: "Calendar hours"
      js: true
  - translation:
      key: 'txt.admin.slas.target_business_hours_option'
      title: "Business hours option for targets."
      value: "Business hours"
      js: true
  - translation:
      key: 'txt.admin.slas.states.saved'
      title: "Label for 'Saved' state."
      value: "Saved"
      js: true
  - translation:
      key: 'txt.admin.slas.states.saving'
      title: "Label for 'Saving' state."
      value: "Saving"
      js: true
  - translation:
      key: 'txt.admin.slas.states.unsaved'
      title: "Label for 'Unsaved' state."
      value: "Unsaved"
      js: true
  - translation:
      key: 'txt.admin.slas.policy_delete_option'
      title: "Delete option for policy."
      value: "Delete"
      js: true
  - translation:
      key: 'txt.admin.slas.policy_clone_option'
      title: "Clone option for policy."
      value: "Clone"
      js: true
  - translation:
      key: "txt.admin.slas.delete_modal.title"
      title: "Delete modal title"
      value: "Confirm"
      js: true
  - translation:
      key: "txt.admin.slas.delete_modal.body"
      title: "Delete modal body"
      value: "Deleting this policy will permanently remove it from your account. This will take immediate effect on all metrics and business rules."
      js: true
  - translation:
      key: "txt.admin.slas.delete_modal.confirm"
      title: "Delete modal confirm button"
      value: "Delete policy"
      js: true
  - translation:
      key: "txt.admin.slas.delete_modal.cancel"
      title: "Delete modal cancel button"
      value: "Cancel"
      js: true
  - translation:
      key: "txt.admin.slas.conditions_heading"
      title: "Heading for sla conditions section"
      value: "Conditions"
      js: true
  - translation:
      key: "txt.admin.slas.conditions_description"
      title: "Description for sla conditions section"
      value: "Add conditions to determine when to apply this SLA policy. For example, tickets from VIP customers."
      js: true
  - translation:
      key: "txt.admin.slas.conditions_all"
      title: "Label for conditions where all must be met"
      value: "Apply this policy to tickets that meet <strong>all</strong> of these conditions"
      js: true
  - translation:
      key: "txt.admin.slas.conditions_any"
      title: "Label for conditions where any can be met"
      value: "Apply this policy to tickets that meet <strong>any</strong> of these conditions"
      js: true
  - translation:
      key: "txt.admin.slas.conditions_placeholder"
      title: "Placeholder for conditions dropdown"
      value: "Type or select condition"
      js: true
  - translation:
      key: "txt.admin.slas.conditions.missing_field"
      title: "Error message letting users know that a field that used to be there is no longer available"
      value: "Option no longer available"
      screenshot: "https://drive.google.com/file/d/1XnrYFJYdLoP0nrR7u1fT5p7RCFNUmj84/view?usp=sharing"
      js: true
  - translation:
      key: "txt.admin.slas.eol.admin_page_notice"
      title: "Notice about legacy SLAs end of life for accounts without SLAs v2."
      screenshot: "https://zendesk.box.com/s/n0i7b1fbdp9zzyqwfa9l5mv2blxl0qld"
      value: "This version of Service Level Agreements will no longer be available after August 21, 2017. For more information, see this article {{link_content}}."
  - translation:
      key: "txt.admin.slas.eol.has_slas_v2.admin_page_notice"
      title: "Notice about legacy SLAs end of life for accounts with SLAs v2. Placeholder loads txt.admin.slas.eol.link.title"
      screenshot: "https://zendesk.box.com/s/bi56tz5n2nej7ywrkae724cddods264o"
      value: "This version of Service Level Agreements will no longer be available after August 21, 2017. For more information and tips on switching to the new version, see this article {{link_content}}."
      obsolete: "2019-03-13"
  - translation:
      key: "txt.admin.slas.eol.has_slas_v2.admin_page_cta"
      title: "Link to the new SLAs admin page."
      screenshot: "https://zendesk.box.com/s/ik7bj55nxmk9xuh1n8k3td2oc6zlqsy1"
      value: "Try the <a href='/agent/admin/slas' target='_parent'>new version of SLAs</a> now."
      obsolete: "2019-03-13"
  - translation:
      key: "txt.admin.slas.eol.link.title"
      title: "Title of the SLAs v1 end of life article. Please localize."
      screenshot: "https://zendesk.box.com/s/n0i7b1fbdp9zzyqwfa9l5mv2blxl0qld"
      value: "Removal of Service Level Agreements (SLAs) v1 in Zendesk Support"
  - translation:
      key: "txt.admin.slas.eol.link.url"
      title: "URL of the SLAs v1 end of life article."
      screenshot: "https://zendesk.box.com/s/n0i7b1fbdp9zzyqwfa9l5mv2blxl0qld"
      value: "https://support.zendesk.com/hc/en-us/articles/115002461528"
      hidden: true
      obsolete: "2019-06-06"
  - translation:
      key: "txt.admin.slas.new_admin_page_notice"
      title: "Notice about new SLAs admin page. https://zendesk.box.com/s/q3weyyhw4rq15z9rfrhavxg7u95c2ra3"
      screenshot: "https://zendesk.box.com/s/q3weyyhw4rq15z9rfrhavxg7u95c2ra3"
      value: "We've improved our Service Level Agreements feature. If you're on Professional or Enterprise, you can create multiple SLA policies based on ticket, user, and organization fields, and measure up to four metrics. You can also pause the clock so pending and on-hold time don't count against you."
  - translation:
      key: "txt.admin.slas.new_admin_page_cta"
      title: "Link to new SLAs admin page. https://zendesk.box.com/s/q3weyyhw4rq15z9rfrhavxg7u95c2ra3"
      screenshot: "https://zendesk.box.com/s/q3weyyhw4rq15z9rfrhavxg7u95c2ra3"
      value: "Give it a try."
  - translation:
      key: "txt.admin.definition_groups.ticket"
      title: "Title for ticket definition group (displayed in select menu)."
      value: "Ticket"
      js: true
  - translation:
      key: "txt.admin.definition_groups.requester"
      title: "Title for requester definition group (displayed in select menu)."
      value: "Requester"
      js: true
  - translation:
      key: "txt.admin.definition_groups.organization"
      title: "Title for organization definition group (displayed in select menu)."
      value: "Organization"
      js: true
  - translation:
      key: "txt.admin.slas.clone_title"
      title: "Title for cloned SLA policy"
      value: "Copy of: {{title}}"
      js: true
  - translation:
      key: "txt.admin.slas.ticket.status.mnemonic_new"
      title: "Mnemonic character for the status New. Typically the first character in the word 'New'. Note, this cannot be the same mnemonic used for Open, Pending, On-hold, Solved or Closed."
      value: "n"
      js: true
  - translation:
      key: "txt.admin.slas.ticket.status.mnemonic_open"
      title: "Mnemonic character for the status Open. Typically the first character in the word 'Open'. Note, this cannot be the same mnemonic used for New, Pending, On-hold, Solved or Closed."
      value: "o"
      js: true
  - translation:
      key: "txt.admin.slas.ticket.status.mnemonic_pending"
      title: "Mnemonic character for the status Pending. Typically the first character in the word 'Pending'. Note, this cannot be the same mnemonic used for New, Open, On-hold, Solved or Closed."
      value: "p"
      js: true
  - translation:
      key: "txt.admin.slas.ticket.status.mnemonic_hold"
      title: "Mnemonic character for the status On-Hold. Typically the first character in the word 'On-Hold'. Note, this cannot be the same mnemonic used for New, Open, Pending, Solved or Closed."
      value: "h"
      js: true
  - translation:
      key: "txt.admin.slas.ticket.status.mnemonic_solved"
      title: "Mnemonic character for the status Solved. Typically the first character in the word 'Solved'. Note, this cannot be the same mnemonic used for New, Open, Pending, On-Hold or Closed."
      value: "s"
      js: true
  - translation:
      key: "txt.admin.slas.ticket.status.mnemonic_closed"
      title: "Mnemonic character for the status Closed. Typically the first character in the word 'Closed'. Note, this cannot be the same mnemonic used for New, Open, Pending, On-Hold or Solved."
      value: "c"
      js: true
