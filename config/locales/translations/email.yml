title: "Email"
packages:
  - default
  - classic

parts:
  - translation:
      key: "txt.admin.settings.email.errors.syntax.variable_termination"
      title: "Error message when tag is not properly terminated. {{template_type}} will be replaced with html or text. Screenshot: https://cloud.githubusercontent.com/assets/10756/6629431/3510163e-c8cb-11e4-87f3-613d86909cda.png"
      value: "Looks like you added a placeholder to the {{template_type}} email template, but didn't format it correctly. It must begin with \"{{\" and end with \"}}\"."
  - translation:
      key: "txt.admin.settings.email.type.html"
      title: "This string will be inserted into {{template_type}} in txt.admin.settings.email.errors.syntax.variable_termination"
      value: "html"
  - translation:
      key: "txt.admin.settings.email.type.text"
      title: "This string will be inserted into {{template_type}} in txt.admin.settings.email.errors.syntax.variable_termination"
      value: "text"
  - translation:
      key: "txt.email.accounts_mailer.update_subscription.subject"
      title: "Email to notify account owner they are out of agent seats. title of email: https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg. First placeholder loades the name of the Zendesk account, use separator as appropriate in your language"
      screenshot: "https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg"
      value: "[{{account_name}}] Action Required: Please purchase more agents"
  - translation:
      key: "txt.email.accounts_mailer.update_subscription.html_body"
      title: "html formatted email to notify account owner they are out of agent seats. https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg"
      screenshot: "https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg"
      value: "Hi {{owner_name}},\n\n\n{{agent_name}} is trying to add {{new_user_name}} ({{new_user_email}}) as a new {{new_user_role}}, but your account has reached the maximum agent limit.\nPlease purchase more agents by <a href={{billing_url}}>updating your subscription</a>."
  - translation:
      key: "txt.email.accounts_mailer.update_subscription.html_body_v2"
      title: "html formatted email to notify account owner they are out of agent seats. https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg"
      screenshot: "https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg"
      value: "Hi {{owner_name}},\n\n\n{{agent_name}} is trying to add {{new_user_name}} ({{new_user_email}}) as a new {{new_user_role}}, but your account has reached the maximum agent limit.\nPlease purchase more agents by <a href='{{billing_url}}'>updating your subscription</a>."
  - translation:
      key: "txt.email.accounts_mailer.update_subscription.txt_body"
      title: "plain text email to notify account owner they are out of agent seats. https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg"
      screenshot: "https://zendesk.box.com/s/jycvhigi2was52o89r27k5povwf4qzsg"
      value: "Hi {{owner_name}},\n\n\n{{agent_name}} is trying to add {{new_user_name}} ({{new_user_email}}) as a new {{new_user_role}}, but your account has reached the maximum agent limit.\nPlease purchase more agents by updating your subscription at {{billing_url}}."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.subject"
      title: "Email subject sent to developers with instructions on how to set up web widget"
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "How to install the Zendesk Web Widget"
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part1"
      title: "Beginning of email sent to developers with information about the Web Widget. Don't translate HTML tags <p></p>"
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "Hey there, <p> We’re writing because {{name}} wants your help implementing the Zendesk Web Widget. The Web Widget brings Zendesk customer service features onto websites and web apps. Luckily, it’s pretty straightforward. Here’s how to get started. </p>"
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part1_text"
      title: "Beginning of email sent to developers with information about the Web Widget."
      screenshot: "https://zendesk.box.com/s/hvsrcctgo7ii1eir6z8uogv6rqkj703o"
      value: "Hey there,


      We’re writing because {{name}} wants your help implementing the Zendesk Web Widget. The Web Widget brings Zendesk customer service features onto websites and web apps. Luckily, it’s pretty straightforward. Here’s how to get started."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part2"
      title: "Instructions on how to add the Code snippet to your own website to enable Web Widget. Don't translate HTML: <b style=\"color:#fda028\">&lt;head&gt;</b> AND <b style=\"color:#fda028\">&lt;/head&gt;</b>"
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "To add the Web Widget to your websites, just copy and paste the following code into the website’s HTML source code between the <b style=\"color:#fda028\">&lt;head&gt;</b> and <b style=\"color:#fda028\">&lt;/head&gt;</b> tags."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part2_text"
      title: "Instructions on how to add the Code snippet to your own website to enable Web Widget. Don't translate HTML entities &lt;head&gt; AND &lt;/head&gt;"
      screenshot: "https://zendesk.box.com/s/hvsrcctgo7ii1eir6z8uogv6rqkj703o"
      value: "To add the Web Widget to your websites, just copy and paste the following code into the website’s HTML source code between the &lt;head&gt; and &lt;/head&gt; tags."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part3"
      title: "Additional info that tells user they need to insert the Web Widget Code snippet in all their webpages"
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "This code must be inserted into every page that you want to display the Web Widget."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part4"
      title: "A note that tells user there are alternate instructions on how to enable Web Widget if they use Google Tag Manager on their website. Don't translate HTML tags <a></a>. Check if Google Tag Manager is localized in your language, otherwise, leave as is."
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "Note: If you are using Google Tag Manager on your site, make sure to check out these <a href=\"{{link}}\">modified instructions</a>."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part4_text"
      title: "A note that tells user there are alternate instructions on how to enable Web Widget if they use Google Tag Manager on their website."
      screenshot: "https://zendesk.box.com/s/hvsrcctgo7ii1eir6z8uogv6rqkj703o"
      value: "Note: If you are using Google Tag Manager on your site, make sure to check out these modified instructions: {{link}}."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part5"
      title: "Informs user they can also enable Web Widget via plugins."
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "You can also activate the Web Widget with our Magento, Shopify and WordPress integrations."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part6"
      title: "Inform user they can learn more about Web Widget and APIs. Links to a Help Center article. Don't translate HTML tags <a></a>."
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "You can learn more about the Web Widget and explore advanced customizations via our JavaScript APIs <a href=\"{{link}}\">here</a>."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part6_text"
      title: "Inform user they can learn more about Web Widget and APIs. Links to a Help Center article."
      screenshot: "https://zendesk.box.com/s/hvsrcctgo7ii1eir6z8uogv6rqkj703o"
      value: "You can learn more about the Web Widget and explore advanced customizations via our JavaScript APIs here: {{link}}."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part7"
      title: "Info on how to contact support if you need help. Don't translate HTML tags <a></a>."
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "We’re here to help. Just email us at <a href=\"mailto:support@zendesk.com\">support@zendesk.com</a>."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.part7_text"
      title: "Info on how to contact support if you need help."
      screenshot: "https://zendesk.box.com/s/hvsrcctgo7ii1eir6z8uogv6rqkj703o"
      value: "We’re here to help. Just email us at support@zendesk.com."
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.heading1"
      title: "Heading for section informing user on how to add Web Widget code to their websites"
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "Adding the Web Widget to your websites"
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.heading2"
      title: "Heading for section informing users they can enable Web Widget via plugins"
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "Activate with plugins"
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.heading3"
      title: "Heading for section which includes a link for more information on setting up web widget"
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "Learn more"
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.heading4"
      title: "Heading for section that tells users how to contact support."
      screenshot: "https://zendesk.box.com/s/5gqw80b0knbx78n24tor0xrstl1glf5t"
      value: "Need help?"
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.gtm_link"
      title: "Link to google tag manager docs"
      value: "https://developer.zendesk.com/embeddables/docs/widget/gtm"
      hidden: true
  - translation:
      key: "txt.email.onboarding_mailer.web_widget.setup_info.hc_link"
      title: "Link to help center article"
      value: "https://support.zendesk.com/hc/en-us/articles/203908456-Using-Web-Widget-to-embed-customer-service-in-your-website"
      hidden: true
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.subject"
      title: "Email subject sent to developers with instructions on how to set up Connect. Connect is the product name and should not be localized."
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "How to install the Connect code snippet"
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part1"
      title: "Beginning of email sent to developers with information about Connect. Don't translate HTML tags <p></p>"
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "Hey there, <p> We’re writing because {{name}} wants your help implementing Zendesk Connect - a product to help understand your customers and proactively message them on your website. Luckily, it’s pretty straightforward. Here’s how to get started.</p>"
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part1_text"
      title: "Beginning of email sent to developers with information about Connect."
      screenshot: "https://zendesk.box.com/s/mbzope2de60it782eqwsc0atcj3nmryn"
      value: "Hey there,


      We’re writing because {{name}} wants your help implementing Zendesk Connect - a product to help understand your customers and proactively message them on your website. Luckily, it’s pretty straightforward. Here’s how to get started."
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part2"
      title: "Instructions on how to add the Code snippet to your own website to enable Web Widget. Don't translate HTML: <b style=\"color:#fda028\">&lt;head&gt;</b> AND <b style=\"color:#fda028\">&lt;/head&gt;</b>"
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "To add Zendesk Connect to your websites, just copy and paste the following code into the website’s HTML source code between the <b style=\"color:#fda028\">&lt;head&gt;</b> and <b style=\"color:#fda028\">&lt;/head&gt;</b> tags."
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part2_text"
      title: "Instructions on how to add the Code snippet to your own website to enable Web Widget. Don't translate HTML entities &lt;head&gt; AND &lt;/head&gt; Zendesk Connect is a product name, do not localize."
      screenshot: "https://zendesk.box.com/s/mbzope2de60it782eqwsc0atcj3nmryn"
      value: "To add Zendesk Connect to your websites, just copy and paste the following code into the website’s HTML source code between the &lt;head&gt; and &lt;/head&gt; tags."
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part3"
      title: "Additional info that tells user they need to insert the Connect code snippet in all their webpages"
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "This code must be inserted into every page that you want to use Zendesk Connect to track interactions and proactively message customers."
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part5"
      title: "Informs user how to install Connect if already using the Web Widget"
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "If your website uses the Zendesk Web Widget, you already have some of the code you need. To install Zendesk Connect you will only need to add and complete the code below containing zE.identify and zE.activateIpm."
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part6"
      title: "Info on how to contact support if you need help. Don't translate HTML tags <a></a>."
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "We’re here to help. Just email us at <a href=\"mailto:connect@zendesk.com\">connect@zendesk.com</a>."
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.part6_text"
      title: "Info on how to contact support if you need help."
      screenshot: "https://zendesk.box.com/s/mbzope2de60it782eqwsc0atcj3nmryn"
      value: "We’re here to help. Just email us at connect@zendesk.com."
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.heading1"
      title: "Heading for section informing user on how to add the Connect (product name) snippet to their websites"
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "Adding the Zendesk Connect code snippet to your website"
  - translation:
      key: "txt.email.onboarding_mailer.connect.setup_info.heading2"
      title: "Heading for section informing users how to install snippet if they're already using the Zendeek Web Widget (product name)"
      screenshot: "https://zendesk.box.com/s/5gyhjub1lr3tg46ospko667zjy5dpmrj"
      value: "Already using the Zendesk Web Widget?"
  - translation:
      screenshot: "https://zendesk.box.com/s/slx1o9fzwg6yvu658dpg0u6eh1vz89rf"
      key: "txt.admin.settings.is_email_comment_public_by_default.true"
      title: "Included in {{ticket.follower_reply_type_message}} template variable, and used by txt.default.follower_email_text message to help determine the final outcome from email replies."
      value: "Reply to this email to add a comment to the request."
  - translation:
      screenshot: "https://zendesk.box.com/s/slx1o9fzwg6yvu658dpg0u6eh1vz89rf"
      key: "txt.admin.settings.is_email_comment_public_by_default.false"
      title: "Included in {{ticket.follower_reply_type_message}} template variable, and used by txt.default.follower_email_text message to help determine the final outcome from email replies."
      value: "Reply to this email to add an internal note to the request."
  - translation:
      key: "txt.email.accounts_mailer.contact_zendesk_customer_support_url"
      title: "URL with information about how customers can contact Zendesk Customer Support"
      value: "https://support.zendesk.com/hc/en-us/articles/360026614173"
      hidden: true
  - translation:
      key: "txt.email.accounts_mailer.contact_your_sales_rep"
      title: "Contact your sales representative text."
      screenshot: "https://drive.google.com/open?id=1NTjbF_TRVxxHew9DOVHRXVMWA3LdD-EO"
      value: "contact your sales representative"
  - translation:
      key: "txt.email.accounts_mailer.subscription_page"
      title: "Subscription page text."
      screenshot: "https://drive.google.com/open?id=10m2naJCZA_1BXCVVNP0KtpWh0xfb0qb7"
      value: "subscription page"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.subject"
      title: "Email subject which notifies the account owner there are no more agent seats available on their account."
      screenshot: "https://drive.google.com/open?id=10m2naJCZA_1BXCVVNP0KtpWh0xfb0qb7"
      value: "You're out of agent seats on {{account_name}}"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.self_serve.html_body"
      title: "HTML formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=10m2naJCZA_1BXCVVNP0KtpWh0xfb0qb7"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. You can manage your agent seats on your <a href='{{cta_target}}'>subscription page</a>"
      obsolete: "2020-12-30"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.self_serve.html_body_v2"
      title: "HTML formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=10m2naJCZA_1BXCVVNP0KtpWh0xfb0qb7"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. You can manage your agent seats on your {{link_content}}"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.self_serve.text_body"
      title: "Plaintext formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=1UpD-5aNF8aYjKCj1x-dO87ECnObxVVYO"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. You can manage your agent seats on your subscription page at {{cta_target}}"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.sales_assisted.html_body"
      title: "HTML formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=1NTjbF_TRVxxHew9DOVHRXVMWA3LdD-EO"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. To manage your agent seats, <a href='mailto:{{cta_target}}'>contact your sales representative</a>"
      obsolete: "2020-12-30"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.sales_assisted.html_body_v2"
      title: "HTML formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=1NTjbF_TRVxxHew9DOVHRXVMWA3LdD-EO"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. To manage your agent seats, <a href='{{link}}'>contact your sales representative</a>"
      obsolete: "2020-12-30"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.sales_assisted.html_body_v3"
      title: "HTML formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=1NTjbF_TRVxxHew9DOVHRXVMWA3LdD-EO"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. To manage your agent seats, {{link_content}}"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.sales_assisted.text_body"
      title: "Plaintext formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=1UpD-5aNF8aYjKCj1x-dO87ECnObxVVYO"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. To manage your agent seats, contact your sales representative at {{cta_target}}"
      obsolete: "2020-12-30"
  - translation:
      key: "txt.email.accounts_mailer.low_seat_count_notification.sales_assisted.text_body_v2"
      title: "Plaintext formatted email to notify account owner they are out of agent seats."
      screenshot: "https://drive.google.com/open?id=1UpD-5aNF8aYjKCj1x-dO87ECnObxVVYO"
      value: "Hi {{owner_name}},\n\n\nYou’ve reached the agent limit on your {{account_name}} account. To manage your agent seats, contact your sales representative or {{cta_target}}"
  - translation:
      key: "txt.email.sandbox_mailer.greeting_body"
      title: "Customer greeting in sandbox emails"
      value: "Hi {{admin_name}},\n\n<br /><br />"
  - translation:
      key: "txt.email.sandbox_mailer.ending_body"
      title: "Customer ending in sandbox emails"
      value: "For help with your sandbox, you can always reply to this email.\n\n<br /><br />Kind regards,\n\n<br /><br />Your Sandbox Team"
  - translation:
      key: "txt.email.sandbox_mailer.failure_subject"
      title: "Subject text in failure sandbox emails"
      value: "Your sandbox didn't complete"
  - translation:
      key: "txt.email.sandbox_mailer.failure_body"
      title: "Customer text in failure sandbox emails"
      value: "Your sandbox wasn't created, but you can try again in a moment.\n\n<br /><br />"
  - translation:
      key: "txt.email.sandbox_mailer.ready_subject"
      title: "Subject text in ready sandbox emails"
      value: "You can use your sandbox"
  - translation:
      key: "txt.email.sandbox_mailer.ready_body"
      title: "Customer text in ready sandbox emails"
      value: "Your tickets are copying over but you can start using your {{sandbox_subdomain}} sandbox now.\n\n<br /><br />You'll get a notification when your sandbox setup is complete.\n\n<br /><br />"
  - translation:
      key: "txt.email.sandbox_mailer.success_subject"
      title: "Subject text in success sandbox emails"
      value: "Your sandbox has finished the ticket copying"
      obsolete: "2021-02-15"
  - translation:
      key: "txt.email.sandbox_mailer.success_subject_v2"
      title: "Subject text in success sandbox emails"
      value: "Your sandbox has finished copying"
  - translation:
      key: "txt.email.sandbox_mailer.success_body"
      title: "Customer text in success sandbox emails"
      value: "Your tickets have been copied over and your {{sandbox_subdomain}} sandbox is ready to use.\n\n<br /><br />"
      obsolete: "2020-01-04"
  - translation:
      key: "txt.email.sandbox_mailer.success_body_v2"
      title: "Customer text in success sandbox emails"
      value: "Your sandbox, {{sandbox_subdomain}}, is ready to use.\n\n<br /><br />"
  - translation:
      key: "txt.email.content_processor.no_content"
      title: "Content shown in a ticket comment when the email has no content"
      value: "[No content]"
