require_relative './boot'

preload_app      true
worker_processes (ENV['UNICORN_WORKERS'] || 2).to_i
timeout          (ENV['UNICORN_TIMEOUT'] || 60).to_i # rubocop:disable Lint/Timeout
listen           4080, backlog: 2048
pid              "/tmp/unicorn.pid"

require './lib/zendesk/web_lifecycle'

before_exec do |_|
  ActiveRecord::Base.clear_all_connections!
  GlobalUid::Base.disconnect!
  Zendesk::WebLifecycle.close_connections
end

before_fork do |server, worker|
  Zendesk::WebLifecycle.before_fork(server, worker)
  ActiveRecord::Base.clear_all_connections!
  GlobalUid::Base.disconnect!
  Zendesk::WebLifecycle.close_connections

  # Unicorn's example configuration suggests using a sleep to reduce race
  # conditions when handling rapid handling of the _same_ unix signal.
  #
  # Kubernetes deploys do not use the traditional fork-and-replace strategy, but
  # instead require cold boots. We want to be able to accept traffic ASAP.
  sleep 1 unless KUBERNETES
end

after_fork do |server, worker|
  Zendesk::WebLifecycle.after_fork(server, worker)
end

require 'unicorn_wrangler'
require 'zendesk_statsd'

UNICORN_STATSD_CLIENT = Datadog::Statsd.new(
  (ENV['STATSD_HOST'] || 'localhost'),
  (ENV['STATSD_PORT'] || 8125),
  namespace: 'classic.app',
  tags: (ENV['COMPUTE_CELL_NAME'].present? ? ["cell:#{ENV['COMPUTE_CELL_NAME']}"] : [])
)

# Don't set any handlers initially, we're going to manually add them below.
# https://github.com/grosser/unicorn_wrangler/blob/08a0da4a/lib/unicorn_wrangler.rb#L27-L30
UnicornWrangler.setup(
  kill_after_requests: false,
  kill_on_too_much_memory: false,
  gc_after_request_time: false,
  stats: UNICORN_STATSD_CLIENT,
  logger: set.fetch(:logger)
)

# UnicornWrangler kills based on memory usage before running GC, so we are going to reset the
# handlers and run GC first, then check memory for killing.
UnicornWrangler.handlers.clear

UnicornWrangler.handlers << UnicornWrangler::RequestKiller.new(
  set.fetch(:logger),
  UNICORN_STATSD_CLIENT,
  (ENV['UNICORN_WRANGLER_REQUEST_LIMIT'] || 10_000).to_i
)

UnicornWrangler.handlers << UnicornWrangler::OutOfBandGC.new(
  set.fetch(:logger),
  UNICORN_STATSD_CLIENT,
  (ENV['UNICORN_WRANGLER_GC_AFTER'] || 8).to_i # seconds
)

UnicornWrangler.handlers << UnicornWrangler::OutOfMemoryKiller.new(
  set.fetch(:logger),
  UNICORN_STATSD_CLIENT,
  {
    max: (ENV['UNICORN_WRANGLER_MAX_MEMORY'] || 850).to_i, # Mb
    check_every: (ENV['UNICORN_WRANGLER_CHECK_MEM_EVERY_REQ'] || 250).to_i # requests
  }
)

module Zendesk::UnicornExtension
  # We really just want to extend `build_app!` by adding the warmup call at the end, but we're also
  # taking this opportunity to remove some unnecessary code. The original method implementation was:
  #
  # https://github.com/defunkt/unicorn/blob/2c347116/lib/unicorn/http_server.rb#L816-L824
  #
  # def build_app!
  #   if app.respond_to?(:arity) && (app.arity == 0 || app.arity == 2)
  #     if defined?(Gem) && Gem.respond_to?(:refresh)
  #       logger.info "Refreshing Gem list"
  #       Gem.refresh
  #     end
  #     self.app = app.arity == 0 ? app.call : app.call(nil, self)
  #   end
  # end
  #
  # We are removing the call to `Gem.refresh` in our implementation. Unicorn does this to support
  # hot reloading and to ensure that newly installed gems get picked up by the new generation.
  # Because we run in a Docker container on Kubernetes, we don't use the hot-reload functionality.
  # We also know our install gems will never change because they are installed in the immutable
  # container.
  def build_app!
    if app.respond_to?(:arity) && (app.arity == 0 || app.arity == 2)
      # removed `Gem.refresh` call
      self.app = app.arity == 0 ? app.call : app.call(nil, self)
    end

    Zendesk::WebLifecycle.warmup(self) # added for Zendesk
  end

  def join
    instrument_boot_time
    super
  end

  private

  def instrument_boot_time
    return unless File.exist?('/tmp/unicorn_boot_start')

    start = Time.parse(File.read('/tmp/unicorn_boot_start').chomp).to_i
    ready = Time.now.to_i - start

    logger.info "master process ready in #{ready}s"
    UNICORN_STATSD_CLIENT.histogram('unicorn.boot_time', ready)
  rescue StandardError => e
    logger.warn "failed to instrument boot time: #{e.message}"
  end
end

Unicorn::HttpServer.prepend(Zendesk::UnicornExtension)
