#!/bin/bash

set -e

heading() {
  echo "$1...";
}

pass() {
  echo "DONE"
}

fail() {
  echo "FAIL"
}

fail_and_exit() {
  fail
  exit -1
}

try() {
  heading "$1"
  if eval "$2"
  then
    pass
  else
    fail_and_exit
  fi
}

link_example() {
  source=$(basename $1)
  real=${1/%\.example/}
  target=$(basename $real)

  pushd $(dirname $real) > /dev/null
  if [ -L $target ] && [ $(readlink $target) = $source ]; then
    try "Linking $real " "true"
  else
    if [ -f $target ]; then
      echo "WARNING: Not linking $real, file was manually overwritten"
    else
      try "Linking $real " "ln -s $source $target"
    fi
  fi
  popd > /dev/null
}

echo "Symlinking config files to examples"
for example in $(ls config/*.example | grep -v statsd | grep -vF redis.)
do
  link_example $example
done
link_example config/environments/development.rb.example
link_example config.ru.example
