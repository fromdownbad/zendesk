require 'bundler/setup'
require 'socket'
require 'samson_secret_puller'

rails_env = ENV['RAILS_ENV'] || ENV['RACK_ENV'] || 'development'
if ['development', 'test'].include?(rails_env) || ENV['USE_BOOTSNAP'] == 'true'
  require 'bootsnap'
  BOOTSNAP_CACHE_DIR = ENV.fetch('BOOTSNAP_CACHE_DIR', 'tmp/bootsnap')
  Bootsnap.setup(
    cache_dir:            BOOTSNAP_CACHE_DIR, # Path to your cache
    development_mode:     rails_env == "development",
    load_path_cache:      true, # optimizes the LOAD_PATH with a cache
    autoload_paths_cache: true, # optimizes ActiveSupport autoloads with cache
    disable_trace:        false, # Sets `RubyVM::InstructionSequence.compile_option = { trace_instruction: true }`
    compile_cache_iseq:   (rails_env != "test"), # compiles Ruby code into ISeq cache .. breaks coverage reporting
    compile_cache_yaml:   true # compiles YAML into a cache
  )
end

KUBERNETES = ENV.key?('KUBERNETES_PORT')

LOG_JSON_TO_STDOUT_ONLY = KUBERNETES || ENV['LOG_JSON_TO_STDOUT_ONLY'] == 'true'

# The way that Samson configures ENV means we can't set different values if we
# are in kubernetes or not. So this is where we manually set the ENV to k8s
# specific values that are environment agnostic.
if KUBERNETES
  ENV['ZENDESK_CONFIG_FROM_ENV'] = 'all'
  ENV['STATSD_HOST'] = '169.254.1.1'
  ENV['CONSUL_HTTP_ADDR'] = '169.254.1.1:8500'
  ENV['GOODDATA_SIGNER_KEY_PATH'] = '/secrets/GOODDATA_SIGNER_PRIVATE_KEY'
  ENV['UNICORN_WORKERS'] = ENV['UNICORN_WORKERS_PER_K8S_POD']
end

# In kubernetes, when running kube-console, we need to let the mysql credentials persist since
# kube-console sets them in the system ENV correctly based on the user launching the console. To do
# this, we need to store them and reset them after the secret puller overrides our ENV.
#
# Because the rails console script is wrapped by the kube-console script, which is run with sudo,
# we also lose the USER environment variable. This sets that variable for the session as well.

# kube-console creates a kubernetes pod with a name in the following format:
# <project>-console-<user>-<YYYY>-<rest-of-timestamp>
user = Socket.gethostname[/classic-console-(.+?)-\d{4}/, 1]
kube_console_mysql_user = ENV['MYSQL_USER']
kube_console_mysql_pass = ENV['MYSQL_PASSWORD']

SamsonSecretPuller.replace_ENV!

# TODO: ideally we bring this override back but we first need to figure out why it breaks
# remote authentication in weird ways.
#
# https://rollbar-eu.zendesk.com/Zendesk/Classic-K8S/items/4217/
# ENV['USER'] = user if user

if user && kube_console_mysql_user && kube_console_mysql_pass
  ENV['MYSQL_USER'] = kube_console_mysql_user
  ENV['MYSQL_PASSWORD'] = kube_console_mysql_pass
end
