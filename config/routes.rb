load File.expand_path('api_routes.rb', File.dirname(__FILE__))

# rubocop:disable Layout/ExtraSpacing
Zendesk::Application.routes.draw do
  root controller: :access, action: :dispatch_to_home

  resource :account_setup, controller: :account_setup, only: :show do
    member do
      get :state, format: 'json'
    end
  end

  resources :texts, module: :cms, as: :cms_texts, path: '/dynamic_content/items' do
    collection do
      get  :export
      get  :export_csv
      get  :import
      post :import_data
    end

    resources :variants, except: :index
  end

  get '/cms/search', controller: 'cms/search', action: :index

  resources :csv_exports do
    collection do
      get :search_stats
      get :hc_search_stats
    end
  end

  resource :getting_started, controller: :getting_started do
    member do
      post :invite_agents
      get  :test_ticket
    end
  end

  get '/robots', controller: :robots, action: :index, constraints: lambda { |req| req.format == :txt }

  %i[index email challenge ticket details].each do |action|
    match "/verification/#{action}/:token", controller: :verification, action: action, as: :"#{action}_token_verification", via: :all
  end
  post  '/verification/details', controller: :verification, action: :details

  match '/export/tickets/:token', controller: :export, action: :tickets, via: :all

  namespace :account do
    resources :payments

    get '/:id/ready', controller: :readiness_proxy, action: :ready

    resource :subscription, controller: :subscription do
      collection do
        get :sponsorship
      end

      member do
        post :upgrade_from_trial
      end
    end

    resource  :feature_boost, controller: :feature_boost
    resources :translations, only: [:index]
  end

  scope '/support/admin', controller: :admin, as: :admin do
    get :automations,                          action: :rule
    get 'automations/:id',                     action: :rule
    get :macros,                               action: :rule
    get 'macros/:id',                          action: :rule
    get 'modal/automations/:id/reorder',       action: :rule
    get 'modal/automations/reorder',           action: :rule
    get 'modal/macros/:id/reorder',            action: :rule
    get 'modal/macros/reorder',                action: :rule
    get 'modal/macros/settings',               action: :rule
    get 'modal/triggers/:id/reorder',          action: :rule
    get 'modal/triggers/reorder',              action: :rule
    get 'modal/views/:id/reorder',             action: :rule
    get 'modal/views/reorder',                 action: :rule
    get :slas,                                 action: :rule
    get :triggers,                             action: :rule
    get 'triggers/:id',                        action: :rule
    get 'triggers/:id/revisions',              action: :rule
    get 'triggers/:id/revisions/:revision_id', action: :rule
    get :views,                                action: :rule
    get 'views/:id',                           action: :rule

    get 'routing/attributes/:id/values/:value_id',              action: :routing
    get 'routing/attributes',                                   action: :routing
    get 'modal/routing/attributes/:id/values/:value_id/agents', action: :routing
    get 'modal/routing/attributes/:id/values/:value_id/delete', action: :routing
    get 'modal/routing/attributes/:id/delete',                  action: :routing
    get 'modal/routing/views/:id/update',                       action: :routing
    get 'modal/routing/view/update',                            action: :routing
    get 'modal/routing/settings/ticket_skills',                 action: :routing
  end

  resource :certificate_signing_request, controller: :certificate_signing_request, only: [] do
    member do
      post :generate
      post :cancel
    end
  end

  resources :external_email_credentials, only: :destroy do
    collection do
      get :oauth_start
      get :oauth_callback
    end
  end

  namespace :settings do
    resource :account, controller: :account do
      collection do
        get :audits
      end

      member do
        put  :update_address
        put  :update_branding
        post :preview_branding
        put  :update_account_owner
        put  :update_localization
        put  :update_benchmark
        put  :update_subdomain
        get  :invoice
      end
    end

    resource :channels
    resource :chat, controller: :chat, only: [:show, :create, :update]

    resource :credit_card, controller: :credit_card, only: :new do
      collection do
        get :upgrade_from_trial
      end

      member do
        get :callback
      end
    end

    resource :email, controller: :email, only: [:show, :update]

    resource :api, controller: :api do
      member do
        put :update_settings
      end
    end

    resources :api_tokens, only: [:create, :update, :destroy]

    resource :mobile_portal, controller: :mobile_portal, only: :show do
      member do
        put :update_settings
        put :update_displayed_settings
        put :update_analytics_settings
      end
    end

    resource :customers do
      member do
        put :update_settings
        put :update_satisfaction
      end
    end

    resource :agents do
      member do
        put :update_settings
      end
    end

    resource :tickets do
      member do
        put :update_settings
        put :update_ticket_sharing
      end
    end

    resource :extensions do
      member do
        post   :update_sugar_crm
        get    :salesforce_oauth_callback
        get    :connect_to_salesforce
        get    :edit_salesforce_object
        delete :remove_salesforce_object
        get    :edit_salesforce_fields
        delete :remove_crm
        get    :salesforce_related_fields
        post   :save_salesforce_object
        put    :update_salesforce
        post   :sort_salesforce_objects
        post   :update_ms_dynamics
        get    :edit_object_filter
        post   :save_object_filter
      end
    end

    resource :yammer_integration, controller: :yammer_integration do
      member do
        get :connect_to_yammer
      end
    end

    resource :export_configuration, controller: :export_configuration, only: [] do
      member do
        put  :update_whitelisted_domain
        put  :disable
        post :enable_request
      end
    end

    resource :slas, controller: :slas, only: :show

    resource :security, controller: :security, only: [:show, :update] do
      collection do
        post :enqueue_certificate_job
        post :disable_zendesk_provisioned_ssl
        post :transition_to_sni
        get  :ssl_provisioning_status
      end
    end

    resources :recipient_addresses, except: :index do
      collection do
        get :preview
        get :table
      end
    end
  end

  # Mobile SDK Configuration
  namespace :mobile do
    namespace :sdk do
      resources :apps, except: :show do
        collection do
          get :onboarding
          put :accept_tos
        end
      end

      resources :auths, except: :show do
        member do
          put :generate_secret
        end
      end
    end
  end

  post '/settings/extensions/update_salesforce', controller: 'settings/extensions', action: :update_salesforce

  namespace :zuora do
    resource :credit_card, controller: :credit_card do
      member do
        get :zendesk_iframe
      end
    end

    resource :invoice, controller: :invoice, only: [] do
      member do
        get :invoice_items
        get :invoices
      end
    end
  end

  resource :crm, controller: :crm do
    member do
      get :sync_user_info
      get :sync_ticket_info
    end
  end

  match '/accounts/extend', controller: 'account/payments', action: :index, via: :all
  match '/payments',        controller: 'account/payments', action: :index, via: :all

  get '/agent/start', controller: 'activate_trial', action: 'index'
  post '/agent/activate_trial/activate', controller: 'activate_trial', action: 'activate', format: 'json'
  get '/agent/activate_trial/wait', controller: 'activate_trial', action: 'wait'
  get '/agent/activate_trial/failed', controller: 'activate_trial', action: 'fail'
  get '/agent/activate_trial/state', controller: 'activate_trial', action: 'state', format: 'json'

  scope '/agent', controller: :lotus_bootstrap do
    get  :dashboard
    post :dashboard

    match '/versions',           action: :versions, via: :all
    match '/',                   action: :show,     via: [:get, :post],
      constraints: lambda { |request| request.query_parameters.key?('redirect') }
    match '/',                   action: :index,    via: [:get, :post]
    match '/tickets/:ticket_id', action: :ticket,   via: [:get, :post]
    match '/talk(/*path)',       action: :talk,     via: [:get, :post]
    match '/chat(/*path)',       action: :chat,     via: [:get, :post]
    match '/(*route)',           action: :show,     via: [:get, :post]
  end

  resources :tickets do
    collection do
      get :created
      put :bulk
      get :bulk_result
      get :requester_change
    end

    member do
      get :incidents
      get :jira_ticket_details
      get :print
    end
  end

  resource :merge, controller: 'tickets/merge' do
    member do
      get :result
    end
  end

  get '/audits/:id/email', controller: :audit_emails, action: :show

  resources :accounts do
    collection do
      get :reminder
      get :available
    end
  end

  resources :requests, controller: 'requests/portal'

  scope controller: :satisfaction_ratings do
    get   '/satisfaction/good_this_week', action: :good_this_week, as: :good_this_week_satisfaction
    get   '/satisfaction/bad_this_week', action: :bad_this_week, as: :bad_this_week_satisfaction
    get   '/requests/:ticket_id/satisfaction/new/:token', action: :new, constraints: { token: /\w+/ }, as: :new_ticket_satisfaction
    post  '/requests/:ticket_id/satisfaction/:token', action: :create_via_token, constraints: { token: /\w+/ }, as: :ticket_satisfaction_via_token
    post  '/requests/:ticket_id/satisfaction', action: :create, as: :ticket_satisfaction
    match '/satisfaction', action: :latest, as: :public_satisfaction, via: :all
    match '/satisfaction/latest', action: :latest, as: :latest_satisfaction, via: :all
  end

  scope controller: 'ticket_deflection' do
    get "/requests/automatic-answers/ticket/article/:article_id", action: 'article', format: 'html'
    get "/requests/automatic-answers/ticket/solve/article/:article_id", action: 'solve_ticket_and_redirect', format: 'html'
    get "/requests/automatic-answers/ticket/feedback/article/:article_id", action: 'article_feedback_redirect', format: 'html'
  end

  scope controller: 'automatic_answers/agent_feedback' do
    post "/requests/automatic-answers/agent/feedback", action: 'agent_feedback', format: 'json'
  end

  scope controller: :automatic_answers_embed do
    get "/requests/automatic-answers/embed/ticket/fetch", action: 'fetch_ticket', format: 'json'
    post "/requests/automatic-answers/embed/ticket/solve", action: 'solve_ticket', format: 'json'
    post "/requests/automatic-answers/embed/ticket/cancel_solve", action: 'cancel_solve', format: 'json'
    post "/requests/automatic-answers/embed/article/irrelevant", action: 'reject_article', format: 'json'
  end

  resources :anonymous_requests, controller: 'requests/anonymous', only: [:index, :new, :create]

  resources :addresses

  # show entries/topics matching tags with dots in them
  match '/tags/:id/entries', controller: :tags, action: :show, for: 'entry', constraints: {id: %r{[^/]+}}, via: :all

  # allow tags with the names of collection methods like autotag
  match '/tags/show/:id', controller: :tags, action: :show, constraints: {id: %r{[^/]+}}, via: :all

  resources :tags, except: [:new, :show] do
    collection do
      get :autotag
      get :bulk_result
    end

    member do
      put :bulk
    end
  end

  # allow tags with the names of collection methods like autotag
  match '/tags/:id', controller: :tags, action: :show, constraints: {id: %r{[^/]+}}, via: :all

  namespace :people do
    resources :tags, only: [:index, :destroy]

    resources :roles do
      member do
        get :clone
      end
    end

    resources :bulk_delete, only: [:index, :new, :create]
    resources :permanently_delete_users, only: [:index, :show, :destroy]
  end

  match '/mobile/switch', controller: 'mobile/switch', action: :index, as: :mobile_switch, via: :all
  match '/mobile/landing_page', controller: 'mobile/landing_page', action: :index, as: :mobile_landing_page, via: :all

  match '/people/tags/:id/destroy', controller: 'people/tags', action: :destroy, constraints: {id: /.*/}, via: :delete
  match '/people/tags', controller: 'people/tags', action: :destroy, via: :delete

  resources :people, controller: 'people/search'

  resources :brands, only: [:index, :create, :update, :destroy] do
    collection do
      put :switch_require_brand
    end

    member do
      put :make_default
      put :toggle
    end
  end

  post '/brands/request_multibrand', controller: :brands, action: :request_multibrand

  # The 'users/current' route must be defined before the users_controller #show route
  match '/users/current',      controller: 'people/current_user', action: :show, via: :all
  match '/users/current/edit', controller: 'people/current_user', action: :edit, via: :all

  match '/verify/email/:token', controller: :unverified_email_addresses, action: :verify, via: :all

  scope controller: 'people/password' do
    match '/password',                   action: :index, as: :password, via: :all
    get   '/password/reset/:token',      action: :reset
    post  '/password/reset/(:token)',    action: :reset_password
    get   '/password/create/:token',     action: :create
    post  '/password/create/:token',     action: :create_password
    put   '/password/update',            action: :update
    match '/password/validate_password', action: :validate_password, via: :all
  end

  scope controller: :password_reset_requests do
    get  '/access/help', action: :index
    post '/access/help', action: :create
  end

  post '/password/admin_reset_request/:user_id', controller: :admin_password_reset_requests, action: :create
  post '/password/admin_change_request/:user_id', controller: :admin_password_change_requests, action: :create

  scope 'registration', controller: :registration do
    match 'request_oauth', action: :request_oauth, as: :registration_request_oauth, via: :all
    match 'success', action: :success, as: :registration_success, via: :all
  end

  resources :organizations, controller: 'people/organizations' do
    collection do
      get   :search
      get   :revert
      get   :edit_password
      match 'autocomplete', via: :all
      get   :jq_autocomplete
    end

    member do
      put :update_notes
      put :toggle_suspend
    end

    resources :users, controller: 'people/users'

    resources :requests, only: :index, controller: 'requests/organization' do
      collection do
        get :search
        get :sidebar_users
      end
    end
  end

  get '/organization_requests', controller: 'requests/organization', action: :index # getting hit as /organization_requests.rss

  resources :groups, controller: 'people/groups' do
    collection do
      get :search
    end

    resources :users, controller: 'people/users'
  end

  get '/groups/:id/confirm_delete', controller: 'people/groups', action: :confirm_delete

  resources :users, controller: 'people/users' do
    collection do
      put  :bulk
      get  :revert
      post :revert
      get  :edit_password
      get  :merge
      put  :merge_with_email
      put  :merge_with_winner
      post :find_or_create
      post :select_bulk_action
      get  :autocomplete
      get  :multivalue_autocomplete
    end

    member do
      get  :successful_twitter_auth
      get  :successful_facebook_auth
      get  :create_twitter_profile
      put  :choose_locale
      post :assume
      put  :update_notes
      post :update_number
      get  :verify
      post :resend_welcome_email
      post :merge_complete
      get  :profiles_with_the_same_phone_number
    end

    resources :user_identities, only: [:index, :new, :create, :destroy] do
      member do
        post :make_primary
      end
    end

    resources :photos

    resources :unverified_email_addresses, only: [:new, :create, :destroy] do
      member do
        get :resend_email
        get :manual_agent_verify
      end
    end

    resource :merge, controller: 'people/user_merge' do
      collection do
        get :autocomplete
      end
    end
  end

  match '/rules/count', controller: 'rules/count', action: :index, via: :all

  namespace :rules do
    resources :analysis, only: :index do
      member do
        get :show # user is 302 redirected back here
        post :show
      end
    end

    # Internal routing
    get  '/views/preview', controller: :views, action: :preview
    post '/views/new_from_search', controller: :views, action: :new

    resources :views, :triggers, :automations do
      collection do
        put    :sort
        delete :destroy_inactive
      end

      member do
        put :activate
      end
    end
  end

  resources :rules, except: :index do
    collection do
      match  'search', via: :all
      put    :sort
      delete :destroy_inactive
    end

    member do
      put :activate
    end

    resources :tickets, only: :index, controller: 'rules/tickets'
  end

  get '/rules', controller: :tickets, action: :index

  resources :photos
  resources :logos

  match '/proxy/direct', controller: :proxy, action: :direct, via: :all

  resources :attachments do
    member do
      get :download_landing
    end
  end

  get  '/attachments/token/:id', controller: :attachments, action: :token, as: :attachment_by_token
  post '/attachments/create.json', controller: :attachments, action: :create, format: 'json'
  get  '/attachments/show/:id', controller: :attachments, action: :show

  get '/attachment/:account_id/:attachment_token', as: :attachment_token, controller: :attachment_token, action: :show

  resources :reports do
    collection do
      match :preview, via: [:post, :put]
      get   :settings
      get   :feed
      get   :amline
      get   :insights
    end

    member do
      get :data
    end
  end

  resources :ticket_fields do
    collection do
      get  :select_field_to_add
      post :sort
    end

    member do
      put :activate
    end
  end

  resources :suspended_tickets do
    member do
      put  :recover
      post :bulk
      get  :assign_to_requester
    end
  end

  match '/targets/new/:id', controller: :targets, action: :new, via: :all
  match '/targets/test/:id', controller: :targets, action: :test_target, via: [:put, :post]
  # this is special for yammer target callbacks
  match '/targets/yammer_callback', controller: :targets, action: :yammer_callback, via: :all

  resources :targets do
    collection do
      get    :select_target_to_add
      delete :delete_inactive
    end

    member do
      post :activate
      post :deactivate
    end
  end

  post  '/watchings/create/:id', controller: :watchings, action: :create
  post  '/watchings/destroy/:id', controller: :watchings, action: :destroy
  match '/watchings/unsubscribe/:token', controller: :watchings, action: :unsubscribe, via: :all
  match '/watchings/unsubscribe_comments/:token', controller: :watchings, action: :unsubscribe_comments, via: :all
  resources :watchings

  namespace :twitter do
    resources :reviewed_tweets, only: [:index, :create]
  end

  match '/recordings/:ticket_id/:comment_id/:sid', controller: 'voice/recordings', action: :show, via: :all
  match '/v2/recordings/:call_id/:sid', controller: 'voice/recordings', action: :show, via: :all

  resources :sharing_agreements, only: [:create, :show, :update, :destroy] do
    collection do
      delete :destroy_inactive
    end

    member do
      get :jira_projects
    end
  end

  namespace :voyager do
    resources :voyager_exports, only: :create

    get '/download/:id', controller: :voyager_exports, action: :download
  end

  namespace :sharing do
    post   '/agreements/:uuid', controller: :agreements, action: :create
    put    '/agreements/:uuid', controller: :agreements, action: :update
    post   '/tickets/:uuid', controller: :tickets, action: :create
    put    '/tickets/:uuid', controller: :tickets, action: :update
    delete '/tickets/:uuid', controller: :tickets, action: :destroy
  end

  match '/requests/organization/search', controller: 'requests/organization', action: :search, via: :all
  match '/requests/organization/:id',    controller: 'requests/organization', action: :index,  via: :all

  # Enable e.g. /tickets/315/events?filter=comments
  resources :tickets

  resources :entries, only: :show
  resources :forums, only: :show
  resources :categories, only: :show

  resources :survey, only: [:show, :create]

  match '/clientaccesspolicy.xml', controller: 'api/v2/integrations/ms_dynamics_policy', action: :index, via: :all

  match '/generated/stylesheets/branding/:top_directory/:account_id/:account_updated_at',
    controller: 'generated/stylesheets', action: :branding, via: :all
  match '/generated/stylesheets/branding/mobile/:top_directory/:account_id/:account_updated_at',
    controller: 'generated/stylesheets', action: :mobile_branding, via: :all
  match '/generated/javascripts/locale/:top_directory/:id/:cache_key', controller: 'generated/javascripts', action: :locale, via: :all

  resource :ping, controller: :ping do
    match 'host',                action: :host, via: :all
    match 'redirect_to_account', action: :redirect_to_account, via: :all
    match 'remote_ip',           action: :remote_ip, via: :all
    match 'shards(/:shard_id)',  action: :shards, via: :all
  end
  get :version, controller: :ping, action: :version

  match '/zuora/callback/ping',        controller: 'zuora/callback', action: :ping,        via: :all
  match '/zuora/callback/callback',    controller: 'zuora/callback', action: :callback,    via: :all
  match '/zuora/callback/inline_sync', controller: 'zuora/callback', action: :inline_sync, via: :all

  match '/cassets/:id', controller: :assets, action: :show, constraints: {id: /.*/}, via: :all
  match '/favicons',    controller: :favicons, action: :show, via: :all
  match '/favicon.ico', controller: :favicons, action: :show, via: :all

  match '/access/request_oauth', controller: :access, action: :request_oauth, as: :request_oauth_access, via: :all

  get :access, controller: :access, action: :index # This should be `root` in the namespace but that changes the route name
  namespace :access do
    get  :login
    post :login
    put  :login
    post :normal_login
    post :unauthenticated
    get  :unauthenticated
    get  :logout
    get  :normal
    get  :request_two_factor
    get  :configure_two_factor
    get  :oauth
    post :oauth
    get  :return_to
    post :return_to
    post :saml
    get  'saml/login',          action: :saml, sso_redirection_strategy: 'saml'
    get  :jwt
    post :jwt
    get  :sso
    post :sso
    get  'master/:token',       action: :master
    post :oauth_mobile,         action: :oauth_mobile_login
    get  :google
    get  :gam
    get  :office_365
    post 'sdk/jwt',             action: :sdk_jwt
    post 'sdk/anonymous',       action: :sdk_anonymous
    post :google_play
    post :google_play_jwt
    get  :one_time_password
    post :token
    get  'access_token/:token', action: :access_token
    get  :sso_bypass
  end

  # previously used default routing
  match '/generated/javascripts/mobile_user.js', controller: 'generated/javascripts', action: :mobile_user, via: :all, format: 'js'
  match '/generated/javascripts/user.json',      controller: 'generated/javascripts', action: :user,        via: :all, format: 'json'
  match '/generated/javascripts/locale.js',      controller: 'generated/javascripts', action: :locale,      via: :all, format: 'js'

  post  '/suspended_tickets/bulk',               controller: :suspended_tickets,      action: :bulk
  match '/registration',                         controller: :registration,           action: :index,   via: :all
  post  '/tags/autotag',                         controller: :tags,                   action: :autotag
  match '/requests/anonymous/new',               controller: 'requests/anonymous',    action: :new,     via: :all
  match '/requests/portal/new',                  controller: 'requests/portal',       action: :new,     via: :all
  match '/requests/portal/index',                controller: 'requests/portal',       action: :index,   via: :all

  match '/voice/settings',                       controller: 'voice/settings',                          via: :all
  match '/import',                               controller: :import,                 action: :index,   via: :all
  post  '/import/create',                        controller: :import,                 action: :create

  match '/home',                                 controller: :home,                   action: :index, via: :all, as: :home
  match '/Home',                                 controller: :home,                   action: :index, via: :all
  match '/home/index',                           controller: :home,                   action: :index, via: :all
  match '/home/:id',                             controller: :home,                   action: :index, via: :all

  # Account Subscription
  get '/account/subscription/upgrade_from_trial', controller: 'account/subscription', action: :upgrade_from_trial
  put '/account/subscription/upgrade_from_trial', controller: 'account/subscription', action: :upgrade_from_trial
  # Registration
  get  '/registration/register', controller: :registration, action: :register
  post '/registration/register', controller: :registration, action: :register
  # Recaptcha
  get '/recaptcha/fetch', controller: :recaptcha, action: :fetch
  # Requests Embeded
  match '/requests/embedded/create', controller: 'requests/embedded', action: :create, via: [:get, :post]
  match '/requests/embedded/create', controller: 'api/v2/cors', action: :preflight, via: :options
  get '/requests/embedded/create', controller: 'requests/embedded', action: :create
  # Twitter
  get '/twitter/tickets/exist.json', controller: 'twitter/tickets', action: :exist, format: 'json'
  # Jobs
  post '/jobs/create', controller: :jobs, action: :create
  # Rules
  post '/rules/triggers/sort',    controller: 'rules/triggers',    action: :sort
  post '/rules/views/sort',       controller: 'rules/views',       action: :sort
  post '/rules/automations/sort', controller: 'rules/automations', action: :sort
  # Mobile API
  post '/requests/mobile_api/create', controller: 'requests/mobile_api', action: :create
  # Log
  get '/log/error', controller: :log, action: :error

  post '/account/base/update', controller: 'account/base', action: :update

  match '/salesforce/auth/callback', controller: 'salesforce/auth', action: :callback, via: :all

  get '/expirable_attachments/token', controller: :expirable_attachments, action: :token
  get '/expirable_attachments/download', controller: :expirable_attachments, action: :download
  get '/expirable_attachments/show', controller: :expirable_attachments, action: :show
  get '/expirable_attachments/token/:id', controller: :expirable_attachments, action: :token
  get '/expirable_attachments/download/:id', controller: :expirable_attachments, action: :download
  get '/expirable_attachments/show/:id', controller: :expirable_attachments, action: :show

  get '/.well-known/acme-challenge/:token', controller: :acme_challenges, action: :show
  get '/.well-known/apple-app-site-association', controller: :app_links, action: :apple, format: 'json'
  get '/.well-known/assetlinks.json', controller: :app_links, action: :android, format: 'json'
  # Google Site Verification
  get '/google:token', controller: 'api/v2/internal/google_app_market', action: :google_site_verification

  # put new rules above this
  # do not generate these, just recognize them
  match '/portal', controller: :home, action: :index, via: :all
  match '/login', controller: :access, action: :index, as: :login, via: :all

  mount Zendesk::ApiDashboard::Engine   => ''
  mount Zendesk::BusinessHours::Engine  => ''
  mount Zendesk::OAuth::Engine          => ''
  mount Zendesk::Auth::Engine           => ''
  mount Zendesk::MobileDeeplink::Engine => ''
  mount Zendesk::Channels::Engine => ''

  if Rails.env.test?
    instance_eval(File.read(Rails.root.join("test/support/routing/routes.rb")))
  end

  # OAuth defines API routes
  # This should be last, as it's a catch-all.
  namespace :api do
    namespace :v2 do
      match '*path', controller: :errors, action: :routing, via: :all
    end

    namespace :mobile do
      get 'attachment/thumbnail', controller: :attachments, action: :show, format: 'json'

      namespace :user do
        resource :avatar, only: :show, format: 'json'
      end
    end
  end

  get '/zendeskchat/start', controller: 'zopim_chat_start', action: :index

  # Catch-all for routes that are not defined above to show a 404 page
  match '*path', controller: :routing_errors, action: :show, via: :all
end
# rubocop:enable Layout/ExtraSpacing
