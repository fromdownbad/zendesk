require 'tmpdir'

namespace :zendesk do
  task :setup_cache_folders do
    dirs = [File.join(fetch(:shared_path), 'data'), File.join(fetch(:shared_path), 'system'), File.join(fetch(:deploy_to), 'generated')]

    on release_roles(:all) do
      execute("sudo install -d -m 0775 -o #{fetch(:user)} -g #{fetch(:group, fetch(:user))} #{dirs.join(' ')}")
    end
  end

  desc 'Configuration files'
  task :update_config do
    on release_roles(:all) do
      script = ['set -e']

      script << "rm -rf #{fetch(:release_path)}/data"
      script << "ln -s /data/zendesk/shared/data #{fetch(:release_path)}"

      script << "rm -rf #{fetch(:release_path)}/public/generated"
      script << "ln -s /data/zendesk/generated #{fetch(:release_path)}/public/"

      script << "rm -rf #{fetch(:release_path)}/public/system"
      script << "ln -s /data/zendesk/shared/system #{fetch(:release_path)}/public/"

      script << "rm -rf #{fetch(:release_path)}/log"
      script << "ln -s /data/zendesk/log #{fetch(:release_path)}"

      fetch(:app_configs).each do |config_file|
        script << "rm -f #{fetch(:release_path)}/config/#{config_file}"
        script << "ln -nfs /data/zendesk/config/#{config_file} #{fetch(:release_path)}/config/#{config_file}"
      end

      execute(script.join("\n"))

      if test('[ -e /data/zendesk/shared/system/.ruby-version ]')
        execute("cp /data/zendesk/shared/system/.ruby-version #{fetch(:release_path)}/.ruby-version")
      end
    end
  end

  task :timestamps_for_release do
    timestamp_paths = YAML.load_file(Bundler.root.join('config', 'timestamp_paths.yml'))

    on :local do
      timestamp_paths.each do |path|
        timestamp_path = (path == ".") ? "TIMESTAMP" : "#{path}.TIMESTAMP"
        execute("git log -n 1 --pretty=format:%ct -- #{path} > #{timestamp_path}")
        fetch(:extra_paths).push(timestamp_path)
      end
    end
  end

  desc 'Bundle + minify + pack the JS and CSS files'
  task :update_assets do
    if ENV['SKIP_ASSETS_TO_PROXY'] == 'true'
      logger.warn('Skipping asset copy to tar')
    else
      invoke('zendesk:generate_new_assets')
      fetch(:extra_paths).push('public/assets/')
    end
  end

  desc 'Upload the JS and CSS files to the proxy server'
  task :upload_assets do
    if ENV['SKIP_ASSETS_TO_PROXY'] == 'true'
      logger.warn('Skipping asset upload to proxies!')
    else
      invoke('zendesk:copy_version_to_proxy')
      invoke('zendesk:copy_public_folder_to_proxy')
    end
  end

  desc "generates new assets locally"
  task :generate_new_assets do
    on :local do
      if ENV['SKIP_ASSETS_TO_PROXY'] == 'true'
        logger.warn('Skipping asset build for proxies')
      else
        execute('bundle exec rake assets:precompile assets:clean[0] RAILS_ENV=development PRE_COMPILE_ENV=production DIGEST_ASSETS=1')
      end
    end
  end

  task :copy_version_to_proxy do
    on roles(:proxy) do
      execute("mkdir -p #{fetch(:current_path)}")
      execute("echo #{fetch(:real_revision)} > #{File.join(fetch(:current_path), 'REVISION')}")
    end
  end

  task :copy_public_folder_to_proxy do
    logger.info 'Fetching archive from mirrors to proxies'

    on roles(:proxy) do
      execute(fetch(:fetch_release))

      compressed_path = File.join(fetch(:archive_cache_path), fetch(:compressed_filename))
      execute("tar --use-compress-prog=#{fetch(:compression_command)} -xf #{compressed_path} -C #{fetch(:current_path)} public/")
      execute("rm #{compressed_path}")
    end
  end

  desc 'force console_classic to launch a rails console via kube-console'
  task :switch_console_classic_to_kube_console do
    on roles(:dbadmin) do
      execute("touch #{File.join(fetch(:shared_path), '.use-kube-console')}")
    end
  end

  desc 'prevent console_classic from launching a rails console via kube-console'
  task :switch_console_classic_to_bin_rails do
    on roles(:dbadmin) do
      execute("rm -f #{File.join(fetch(:shared_path), '.use-kube-console')}")
    end
  end
end
