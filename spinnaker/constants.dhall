let project = ../dhall-config/project.dhall

let application = project.name

let roles = [ "okta:role:read-only", "okta:role:${project.name}" ]

in  { application, roles, repoName = "zendesk" }
