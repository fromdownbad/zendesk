{-
Use dhall-render and build up a list of all files that need to be generated.

TODO: explain this further
-}
let Render = ../dhall-config/dependencies/render.dhall

let Prelude = ../dhall-config/dependencies/prelude.dhall

let Pod = (../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let spinnaker = (./dependencies/spinnaker.dhall).schemas

let PodPipeline = ./pod-pipeline.dhall

let project = ../dhall-config/project.dhall

let Constants = ./constants.dhall

let PipelineJSON = Render.JSONFile spinnaker.Pipeline.Type

let applicationJSON =
      let application =
            spinnaker.Application::{
            , name = project.name
            , email = "${project.team}@zendesk.com"
            , description = project.description
            , repoSlug = "zendesk"
            , permissions =
              { EXECUTE = [ "okta:role:${project.name}" ]
              , READ = [ "okta:role:read-only", "okta:role:${project.name}" ]
              , WRITE = [ "okta:role:${project.name}" ]
              }
            , dataSources =
              { disabled = [] : List Text
              , enabled =
                [ "canaryConfigs"
                , "executions"
                , "loadBalancers"
                , "securityGroups"
                , "serverGroups"
                ]
              }
            }

      let renderFile =
            (Render.JSONFile spinnaker.Application.Type)::{
            , install = Render.Install.Write
            , path = Some "${project.name}.json"
            , contents = application
            }

      in  renderFile

let stagingPipelineJSON =
      PipelineJSON::{
      , path = Some "staging.json"
      , install = Render.Install.Write
      , contents = ./staging.dhall
      }

let jenkinsPipelineJSON =
      PipelineJSON::{
      , path = Some "jenkins.json"
      , install = Render.Install.Write
      , contents = ./jenkins.dhall
      }

let productionPipelineJSON =
      PipelineJSON::{
      , path = Some "production.json"
      , install = Render.Install.Write
      , contents = ./production.dhall
      }

let PodPipelineJSON/fromPod
    : Pod.Type -> PipelineJSON.Type
    = \(pod : Pod.Type) ->
        PipelineJSON::{
        , path = Some "${Pod.name pod}.json"
        , install = Render.Install.Write
        , contents = PodPipeline.forPod pod
        }

let PodPipelineJSON/perPod
    : List PipelineJSON.Type
    = (Pod.All.Of PipelineJSON.Type).values
        (Pod.All.generate PipelineJSON.Type PodPipelineJSON/fromPod)

in  { options = Render.Options::{ destination = "../spinnaker/generated" }
    , files =
      { applications = applicationJSON
      , pipelines =
            [ stagingPipelineJSON, jenkinsPipelineJSON, productionPipelineJSON ]
          # PodPipelineJSON/perPod
      }
    }
