# Spinnaker pipelines

We can define how Classic gets deployed to Zendesk infrastructure with [Spinnaker Pipelines](https://spinnaker.io/concepts/pipelines/). In this folder, we define these pipelines as code, since Spinnaker accepts JSON that configures a pipeline. Generating pipelines saves project maintainers a lot of work when new Zendesk PODs are added, removed, or deploy orders change.

## Pipelines as Dhall

Using Dhall bindings defined in the [zendesk-dhall repo](https://github.com/zendesk/zendesk-dhall/tree/master/Spinnaker#spinnaker), we can ensure that the JSON we generate never has typos in field names or incorrectly typed values. We've chosen Dhall for a faster feedback loop during pipeline development.

## Development

```bash
$ pwd # you must be in this directory!

.../zendesk/kubernetes/dhall/spinnaker

$ ./dhall-render # call our multi-file rendering solution

$ ls ./pipelines/ # You'll find the output JSON in ./pipelines/

pod13.json      pod17.json      pod20.json      pod26.json      pod999.json
pod14.json      pod18.json      pod23.json      pod27.json      production.json
pod15.json      pod19.json      pod25.json      pod998.json     staging.json
```

## Architecture

Classic has a massive number of kubernetes roles. Deploying a kubernetes role to one Zendesk POD would require a single Spinnaker stage. We could configure a single pipeline for all of these stages (currently, 60 roles * 13 PODs = 780 stages). The Spinnaker UI would then render a dependency graph for all of these stages on a single page.

### One pipeline

We believe the Spinnaker UI rendering 780 interactable stages in a single page would result in a horrible maintenance experience. Most Zendesk services with only a handful of kubernetes roles are likely fine to deploy in a single pipeline.

### Multiple pipelines

With Classic's massive number of roles, we've decided to use Spinnaker's "pipelines as a first-class citizen" feature set. For a given stage, we can invoke another Spinnaker pipeline instead of deploying to a role to a POD.

#### Staging example

```
# Staging Pipeline
Configure => Pod 998 Pipeline => Pod 999 Pipeline => Success

# Pod 998 Pipeline

Configure => Deploy App Server to 998    => Success
          => Deploy Console to 998       /
          => Deploy ResqueWorker to 998 /
          => ...

# Pod 999 Pipeline

Configure => Deploy App Server to 999    => Success
          => Deploy Console to 999       /
          => Deploy ResqueWorker to 999 /
          => ...
```
