let not = (../dhall-config/dependencies/prelude.dhall).Bool.not

let spinnaker = (./dependencies/spinnaker.dhall).schemas

let Constants = ./constants.dhall

let Switches = ./switches.dhall

let Notifications = ./notifications.dhall

let DeployGroup = ./deploy-group.dhall

let Stages = ./stages.dhall

in  spinnaker.Pipeline::{
    , disabled = Some (not Switches.staging.enabled)
    , application = Some Constants.application
    , lastModifiedBy = None Text
    , name = Some "Staging"
    , notifications = Some [ Notifications.slack ]
    , parameterConfig = Some
      [ spinnaker.ParameterConfig::{
        , default = ""
        , description = "Git ref (commit, branch, tag) to be deployed"
        , name = "gitRef"
        , options = None (List spinnaker.ParameterConfigOption.Type)
        , pinned = True
        , required = True
        }
      ]
    , roles = Some Constants.roles
    , stages = DeployGroup.stagingAsStages # [ Stages.staging.jenkins ]
    , triggers =
      [ spinnaker.Trigger::{
        , branch = Some "refs/tags/v\\d+\$"
        , enabled = False
        , expectedArtifactIds = Some ([] : List Text)
        , project = Some "zendesk"
        , slug = Some Constants.repoName
        , source = Some "github"
        , type = "git"
        }
      ]
    , updateTs = None Text
    }
