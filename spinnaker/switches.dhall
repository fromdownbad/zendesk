let EnvironmentSwitch = { enabled : Bool }

let Switches = { production : EnvironmentSwitch, staging : EnvironmentSwitch }

in  { production.enabled = False, staging.enabled = True } : Switches
