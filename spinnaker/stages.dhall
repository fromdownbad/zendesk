let Prelude = ../dhall-config/dependencies/prelude.dhall

let Pod = (../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let spinnaker = (./dependencies/spinnaker.dhall).schemas

let Constants = ./constants.dhall

let podNames = Pod.All.generate Text Pod.name

let buddy =
      spinnaker.Stage::{
      , refId = "buddy"
      , name = "Buddy"
      , failPipeline = Some True
      , type = "manualJudgment"
      , instructions = Some
          "This is a production deployment. Continue? (This is a buddy check.)"
      , continuePipeline = Some True
      , completeOtherBranchesThenFail = Some False
      }

let ids =
      \(stages : List spinnaker.Stage.Type) ->
        Prelude.List.map
          spinnaker.Stage.Type
          Text
          (\(stage : spinnaker.Stage.Type) -> stage.refId)
          stages

let anotherPipelineAsStage =
      \(previousStages : List spinnaker.Stage.Type) ->
      \(pod : Pod.Type) ->
        let podName = Pod.name pod

        in  spinnaker.Stage::{
            , application = Some Constants.application
            , completeOtherBranchesThenFail = Some False
            , failPipeline = Some True
            , name = podName
            , pipeline = Some "\${#pipelineId(\"${podName}\")}"
            , pipelineParameters = Some
                (toMap { gitRef = "\${ trigger.parameters.branch }" })
            , refId = "${podName}"
            , requisiteStageRefIds = Some (ids previousStages)
            , type = "pipeline"
            , waitForCompletion = Some True
            }

let staging =
      { canary = spinnaker.Stage::{
        , application = Some Constants.application
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some False
        , failPipeline = Some True
        , name = "Canary check"
        , refId = "canary"
        , requisiteStageRefIds = Some [ podNames.pod998 ]
        , type = "manualJudgment"
        }
      , jenkins = spinnaker.Stage::{
        , application = Some Constants.application
        , completeOtherBranchesThenFail = Some False
        , failPipeline = Some True
        , name = "Jenkins"
        , pipeline = Some "\${#pipelineId(\"jenkins\")}"
        , pipelineParameters = Some
            (toMap { gitRef = "\${ trigger.parameters.branch }" })
        , refId = "jenkins"
        , requisiteStageRefIds = Some [ podNames.pod999 ]
        , type = "pipeline"
        , waitForCompletion = Some True
        }
      }

let production =
      { buddy
      , canary = spinnaker.Stage::{
        , application = Some Constants.application
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some False
        , failPipeline = Some True
        , name = "Canary check"
        , refId = "canary"
        , requisiteStageRefIds = Some
          [ podNames.pod15, podNames.pod25, podNames.pod26 ]
        , type = "manualJudgment"
        }
      , bake = spinnaker.Stage::{
        , application = Some Constants.application
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some False
        , failPipeline = Some True
        , name = "Group 1 check"
        , refId = "group1Check"
        , requisiteStageRefIds = Some [ podNames.pod18, podNames.pod23 ]
        , type = "manualJudgment"
        }
      , bake2 = spinnaker.Stage::{
        , application = Some Constants.application
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some False
        , failPipeline = Some True
        , name = "Group 2 check"
        , refId = "group2Check"
        , requisiteStageRefIds = Some
          [ podNames.pod17, podNames.pod19, podNames.pod27 ]
        , type = "manualJudgment"
        }
      }

in  { anotherPipelineAsStage, staging, production, ids }
