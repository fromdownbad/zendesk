let Prelude = ../dhall-config/dependencies/prelude.dhall

let spinnaker = (./dependencies/spinnaker.dhall).schemas

let Constants = ./constants.dhall

let Switches = ./switches.dhall

let Notifications = ./notifications.dhall

let DeployGroup = ./deploy-group.dhall

let not = Prelude.Bool.not

in  spinnaker.Pipeline::{
    , disabled = Some (not Switches.production.enabled)
    , application = Some Constants.application
    , lastModifiedBy = None Text
    , name = Some "Production"
    , notifications = Some [ Notifications.slack ]
    , parameterConfig = Some
      [ spinnaker.ParameterConfig::{
        , default = ""
        , description = "Git ref (commit, branch, tag) to be deployed"
        , name = "gitRef"
        , options = None (List spinnaker.ParameterConfigOption.Type)
        , pinned = True
        , required = True
        }
      ]
    , roles = Some Constants.roles
    , stages = DeployGroup.productionAsStages
    , triggers =
      [ spinnaker.Trigger::{
        , branch = Some "refs/tags/v\\d+\$"
        , enabled = False
        , expectedArtifactIds = Some ([] : List Text)
        , project = Some Constants.repoName
        , slug = Some Constants.repoName
        , source = Some "github"
        , type = "git"
        }
      ]
    , updateTs = None Text
    }
