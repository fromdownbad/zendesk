let Prelude = ../dhall-config/dependencies/prelude.dhall

let spinnaker = (./dependencies/spinnaker.dhall).schemas

let Pod = (../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let Stages = ./stages.dhall

let DeployGroupAttributes =
      { requisiteStages : List spinnaker.Stage.Type, pods : List Pod.Type }

let DeployGroup =
      < Canary : DeployGroupAttributes
      | Bake : DeployGroupAttributes
      | Bake2 : DeployGroupAttributes
      | Finish : DeployGroupAttributes
      >

let DeployGroupAttributes/toDeployStages =
      \(attributes : DeployGroupAttributes) ->
        Prelude.List.map
          Pod.Type
          spinnaker.Stage.Type
          (Stages.anotherPipelineAsStage attributes.requisiteStages)
          attributes.pods

let staging =
      { canary =
          DeployGroup.Canary
            { requisiteStages = [] : List spinnaker.Stage.Type
            , pods = [ Pod.Type.Pod998 ]
            }
      , finish =
          DeployGroup.Finish
            { requisiteStages = [ Stages.staging.canary ]
            , pods = [ Pod.Type.Pod999 ]
            }
      }

let Staging/toStages =
      \(deployGroup : DeployGroup) ->
        merge
          { Canary = DeployGroupAttributes/toDeployStages
          , Bake = DeployGroupAttributes/toDeployStages
          , Bake2 = DeployGroupAttributes/toDeployStages
          , Finish =
              \(attributes : DeployGroupAttributes) ->
                  [ Stages.staging.canary ]
                # DeployGroupAttributes/toDeployStages attributes
          }
          deployGroup

let production =
      { canary =
          DeployGroup.Canary
            { requisiteStages = [ Stages.production.buddy ]
            , pods = [ Pod.Type.Pod15, Pod.Type.Pod25, Pod.Type.Pod26 ]
            }
      , bake =
          DeployGroup.Bake
            { requisiteStages = [ Stages.production.canary ]
            , pods = [ Pod.Type.Pod18, Pod.Type.Pod23 ]
            }
      , bake2 =
          DeployGroup.Bake2
            { requisiteStages = [ Stages.production.bake ]
            , pods = [ Pod.Type.Pod17, Pod.Type.Pod19, Pod.Type.Pod27 ]
            }
      , finish =
          DeployGroup.Finish
            { requisiteStages = [ Stages.production.bake2 ]
            , pods = [ Pod.Type.Pod13, Pod.Type.Pod14, Pod.Type.Pod20 ]
            }
      }

let Production/toStages =
      \(deployGroup : DeployGroup) ->
        merge
          { Canary =
              \(attributes : DeployGroupAttributes) ->
                  [ Stages.production.buddy ]
                # DeployGroupAttributes/toDeployStages attributes
          , Bake =
              \(attributes : DeployGroupAttributes) ->
                  [ Stages.production.canary ]
                # DeployGroupAttributes/toDeployStages attributes
          , Bake2 =
              \(attributes : DeployGroupAttributes) ->
                  [ Stages.production.bake ]
                # DeployGroupAttributes/toDeployStages attributes
          , Finish =
              \(attributes : DeployGroupAttributes) ->
                  [ Stages.production.bake2 ]
                # DeployGroupAttributes/toDeployStages attributes
          }
          deployGroup

let stagingAsStages =
      Staging/toStages staging.canary # Staging/toStages staging.finish

let productionAsStages =
        Production/toStages production.canary
      # Production/toStages production.bake
      # Production/toStages production.bake2
      # Production/toStages production.finish

in  { Type = DeployGroup
    , Attributes = DeployGroupAttributes
    , staging
    , production
    , stagingAsStages
    , productionAsStages
    }
