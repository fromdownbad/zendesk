let spinnaker = (./dependencies/spinnaker.dhall).schemas

let project = ../dhall-config/project.dhall

let constants = ./constants.dhall

let Prelude = ../dhall-config/dependencies/prelude.dhall

let createDeploymentRef = "createDeployment"

let jenkinsJobNames =
      [ "classic_staging_core_api_tests_pod999"
      , "classic_browser_tests_pod999"
      , "Support_Account_Creation_Staging_999"
      , "Classic_Staging_Pod999_Status"
      , "Sqid_Classic_Authentication_Staging_Status_Pod999"
      ]

let commonPipelineParams =
      Some (toMap { gitRef = "\${ trigger.parameters.gitRef }" })

let jenkinsStage =
      \(jobName : Text) ->
        spinnaker.Stage::{
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some True
        , failPipeline = Some False
        , job = Some jobName
        , master = Some "jenkins-qa"
        , name = jobName
        , parameters = Some (toMap { SAMSON_tag = "\${parameters.gitRef}" })
        , refId = jobName
        , requisiteStageRefIds = Some ([] : List Text)
        , stageTimeoutMs = Some 108000000
        , type = "jenkins"
        }

let jenkinsStages =
      Prelude.List.map Text spinnaker.Stage.Type jenkinsStage jenkinsJobNames

in  spinnaker.Pipeline::{
    , application = Some project.name
    , lastModifiedBy = None Text
    , name = Some "jenkins"
    , notifications = Some ([] : List spinnaker.Notification.Type)
    , parameterConfig = Some
      [ spinnaker.ParameterConfig::{
        , default = ""
        , description = "Git ref (commit, branch, tag) to be tested"
        , name = "gitRef"
        , options = None (List spinnaker.ParameterConfigOption.Type)
        , pinned = True
        , required = True
        }
      ]
    , roles = Some constants.roles
    , stages = jenkinsStages
    , triggers = [] : List spinnaker.Trigger.Type
    , updateTs = None Text
    }
