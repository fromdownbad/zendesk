let spinnaker = (./dependencies/spinnaker.dhall).schemas

let executionInMinutesAndSeconds =
      "\${(execution.getEndTime() - execution.getStartTime()) / 1000 / 60} minute(s), \${(execution.getEndTime() - execution.getStartTime()) / 1000 % 60} second(s)"

let slack =
      spinnaker.Notification::{
      , address = Some "classic-workflow-staging"
      , level = Some "pipeline"
      , message = Some spinnaker.NotificationTiming::{
        , `pipeline.complete` = Some spinnaker.NotificationMessage::{
          , text =
              "Deployed \${trigger.branch} in ${executionInMinutesAndSeconds}"
          }
        , `pipeline.failed` = Some spinnaker.NotificationMessage::{
          , text =
              "Failed to deploy \${trigger.branch} after ${executionInMinutesAndSeconds}"
          }
        }
      , type = Some "slack"
      , when = Some [ "pipeline.failed", "pipeline.complete" ]
      }

in  { slack }
