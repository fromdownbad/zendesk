let Prelude = ../dhall-config/dependencies/prelude.dhall

let Pod = (../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let spinnaker = (./dependencies/spinnaker.dhall).schemas

let Switches = ./switches.dhall

let Stages = ./stages.dhall

let constants = ./constants.dhall

let fileNames = ../kubernetes/dhall/roles/names.dhall

let not = Prelude.Bool.not

let expectedArtifact =
      \(pod : Pod.Type) ->
      \(artifactName : Text) ->
        spinnaker.ExpectedArtifact::{
        , customKind = None Bool
        , defaultArtifact = Some spinnaker.DefaultArtifact::{
          , artifactAccount = Some "github"
          , customKind = None Bool
          , id = None Text
          , kind = Some "github"
          , name = Some artifactName
          , reference = Some
              "https://api.github.com/repos/zendesk/zendesk/contents/kubernetes/generated/${Pod.name
                                                                                              pod}/${artifactName}"
          , type = Some "github/file"
          , version = Some "\${ parameters.gitRef }"
          }
        , displayName = Some artifactName
        , id = artifactName
        , matchArtifact = spinnaker.MatchArtifact::{
          , artifactAccount = Some "github"
          , customKind = None Bool
          , id = None Text
          , name = artifactName
          , type = Some "github/file"
          }
        , usePriorArtifact = Some False
        }

let rollbackArtifact =
      \(artifactName : Text) ->
        spinnaker.ExpectedArtifact::{
        , customKind = None Bool
        , displayName = Some "Previous ${artifactName}"
        , id = "previous-${artifactName}"
        , matchArtifact = spinnaker.MatchArtifact::{
          , artifactAccount = Some "github"
          , customKind = None Bool
          , id = None Text
          , name = artifactName
          , type = Some "github/file"
          }
        , useDefaultArtifact = False
        , usePriorArtifact = Some False
        }

let classicRefId = \(artifactName : Text) -> "deploy-${artifactName}"

let classicRollbackArtifactId =
      \(artifactName : Text) -> "previous-${artifactName}"

let classicRollbackRefId =
      \(artifactName : Text) ->
        classicRefId (classicRollbackArtifactId artifactName)

let classicStage =
      \(pod : Pod.Type) ->
      \(requisiteStageIds : List Text) ->
      \(artifactName : Text) ->
        spinnaker.Stage::{
        , account = Some (Pod.name pod)
        , cloudProvider = Some "kubernetes"
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some True
        , failPipeline = Some False
        , manifestArtifactId = Some artifactName
        , moniker = Some spinnaker.Moniker::{
          , app = Some "classic"
          , detail = Some
              "/#/applications/\${execution['application']}/executions/\${execution['id']}"
          }
        , name = "Deploy ${artifactName}"
        , refId = classicRefId artifactName
        , requisiteStageRefIds = Some requisiteStageIds
        , skipExpressionEvaluation = Some False
        , source = Some "artifact"
        , trafficManagement = Some
          { enabled = False
          , options =
            { enableTraffic = False, services = Some ([] : List Text) }
          }
        , type = "deployManifest"
        }

let classicRollbackStage =
      \(pod : Pod.Type) ->
      \(artifactName : Text) ->
        spinnaker.Stage::{
        , account = Some (Pod.name pod)
        , cloudProvider = Some "kubernetes"
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some True
        , failPipeline = Some False
        , manifestArtifactId = Some (classicRollbackArtifactId artifactName)
        , moniker = Some spinnaker.Moniker::{
          , app = Some "classic"
          , detail = Some
              "/#/applications/\${execution['application']}/executions/\${execution['id']}"
          }
        , name = "Deploy Previous ${artifactName}"
        , refId = classicRollbackRefId artifactName
        , requisiteStageRefIds = Some [ "deploy-failed" ]
        , skipExpressionEvaluation = Some False
        , source = Some "artifact"
        , trafficManagement = Some
          { enabled = False
          , options =
            { enableTraffic = False, services = Some ([] : List Text) }
          }
        , type = "deployManifest"
        }

let stageSuccessPrecondition =
      \(artifactName : Text) ->
        spinnaker.Precondition::{
        , context = spinnaker.PreconditionContext::{
          , stageName = Some "Deploy ${artifactName}"
          , stageStatus = Some "SUCCEEDED"
          }
        , failPipeline = True
        , type = "stageStatus"
        }

let expectedArtifacts =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Text
          spinnaker.ExpectedArtifact.Type
          (expectedArtifact pod)
          (fileNames.deploys pod # fileNames.configMaps)

let rollbackArtifacts =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Text
          spinnaker.ExpectedArtifact.Type
          rollbackArtifact
          (fileNames.deploys pod # fileNames.configMaps)

let classicStages =
      \(pod : Pod.Type) ->
      \(requisiteStageIds : List Text) ->
      \(names : List Text) ->
        Prelude.List.map
          Text
          spinnaker.Stage.Type
          (classicStage pod requisiteStageIds)
          names

let classicRollbackStages =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Text
          spinnaker.Stage.Type
          (classicRollbackStage pod)
          (fileNames.deploys pod # fileNames.configMaps)

let stageSuccessPreconditions =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Text
          spinnaker.Precondition.Type
          stageSuccessPrecondition
          (fileNames.deploys pod)

let classicRefIds =
      \(pod : Pod.Type) ->
        Prelude.List.map Text Text classicRefId (fileNames.deploys pod)

let classicRollbackRefIds =
      \(pod : Pod.Type) ->
        Prelude.List.map Text Text classicRollbackRefId (fileNames.deploys pod)

let refId = \(stage : spinnaker.Stage.Type) -> stage.refId

let stageRefCheck =
      \(stage : spinnaker.Stage.Type) ->
        "#stageByRefId(\"${refId stage}\").status == \"FAILED_CONTINUE\""

let determineFailureExpression =
      \(stages : List spinnaker.Stage.Type) ->
        let stageRefChecks =
              Prelude.List.map spinnaker.Stage.Type Text stageRefCheck stages

        in  "\${ " ++ Prelude.Text.concatSep " || " stageRefChecks ++ " }"

let findManifestStage/fromPod =
      \(requisiteStageIds : List Text) ->
      \(pod : Pod.Type) ->
        spinnaker.Stage::{
        , application = Some "classic"
        , completeOtherBranchesThenFail = Some False
        , continuePipeline = Some True
        , executionOptions = Some { successful = True }
        , expectedArtifacts = Some (rollbackArtifacts pod)
        , failPipeline = Some False
        , name = "Find Manifests"
        , pipeline = Some "\${#pipelineId(\"${Pod.name pod}\")}"
        , requisiteStageRefIds = Some requisiteStageIds
        , refId = "find-manifests"
        , type = "findArtifactFromExecution"
        }

let forPod =
      \(pod : Pod.Type) ->
        let podName = Pod.name pod

        let spinnakerEnv =
              if Pod.isProduction pod then "spinnaker" else "spinnaker-staging"

        let buddyStages =
              if    Pod.isProduction pod
              then  [ Stages.production.buddy ]
              else  [] : List spinnaker.Stage.Type

        let buddyRefIds = Stages.ids buddyStages

        let findManifestsStage = findManifestStage/fromPod buddyRefIds pod

        let configMapStages =
              classicStages
                pod
                [ findManifestsStage.refId ]
                fileNames.configMaps

        let deployStages =
              classicStages
                pod
                (Stages.ids configMapStages)
                (fileNames.deploys pod)

        in  spinnaker.Pipeline::{
            , disabled = Some
                ( if    Pod.isProduction pod
                  then  not Switches.production.enabled
                  else  not Switches.staging.enabled
                )
            , name = Some podName
            , application = Some "classic"
            , expectedArtifacts = Some (expectedArtifacts pod)
            , lastModifiedBy = None Text
            , notifications = Some ([] : List spinnaker.Notification.Type)
            , parameterConfig = Some
              [ spinnaker.ParameterConfig::{
                , default = ""
                , description = "Git ref (commit, branch, tag) to be deployed"
                , name = "gitRef"
                , options = None (List { value : Text })
                , pinned = True
                , required = True
                }
              ]
            , roles = Some constants.roles
            , stages =
                  buddyStages
                # [ spinnaker.Stage::{
                    , alias = Some "preconfiguredWebhook"
                    , completeOtherBranchesThenFail = Some False
                    , continuePipeline = Some False
                    , failPipeline = Some True
                    , name = "Create GitHub Deployment"
                    , parameterValues = Some
                      [ { mapKey = "autoMerge", mapValue = "false" }
                      , { mapKey = "description"
                        , mapValue = "Deploy to ${podName}"
                        }
                      , { mapKey = "environment", mapValue = podName }
                      , { mapKey = "gitReference"
                        , mapValue = "\${ parameters.gitRef }"
                        }
                      , { mapKey = "organization", mapValue = "zendesk" }
                      , { mapKey = "payload", mapValue = "{}" }
                      , { mapKey = "project", mapValue = "zendesk" }
                      , { mapKey = "requiredContexts", mapValue = "[]" }
                      ]
                    , refId = "create-github-deployment"
                    , requisiteStageRefIds = Some buddyRefIds
                    , statusUrlResolution = Some "getMethod"
                    , type = "createGithubDeployment"
                    }
                  , findManifestsStage
                  ]
                # configMapStages
                # deployStages
                # [ spinnaker.Stage::{
                    , completeOtherBranchesThenFail = Some False
                    , continuePipeline = Some False
                    , failPipeline = Some False
                    , name = "Deploy Succeeded"
                    , preconditions = Some (stageSuccessPreconditions pod)
                    , refId = "deploy-succeeded"
                    , requisiteStageRefIds = Some (classicRefIds pod)
                    , type = "checkPreconditions"
                    }
                  , spinnaker.Stage::{
                    , completeOtherBranchesThenFail = Some False
                    , continuePipeline = Some False
                    , failPipeline = Some False
                    , name = "Deploy Failed"
                    , preconditions = Some
                      [ spinnaker.Precondition::{
                        , context = spinnaker.PreconditionContext::{
                          , expression = Some
                              (determineFailureExpression deployStages)
                          , failureMessage = Some
                              "One of the previous steps failed"
                          }
                        , failPipeline = False
                        , type = "expression"
                        }
                      ]
                    , refId = "deploy-failed"
                    , requisiteStageRefIds = Some (classicRefIds pod)
                    , type = "checkPreconditions"
                    }
                  ]
                # classicRollbackStages pod
                # [ spinnaker.Stage::{
                    , alias = Some "preconfiguredWebhook"
                    , completeOtherBranchesThenFail = Some False
                    , continuePipeline = Some False
                    , failPipeline = Some True
                    , name = "GitHub Deployment Success"
                    , parameterValues = Some
                      [ { mapKey = "acceptHeader"
                        , mapValue =
                            "application/vnd.github.ant-man-preview+json"
                        }
                      , { mapKey = "deploymentId"
                        , mapValue =
                            "\${ #stage( 'Create GitHub Deployment' )['context']['webhook']['body']['id'] }"
                        }
                      , { mapKey = "description"
                        , mapValue = "See Spinnaker for deployment details"
                        }
                      , { mapKey = "environment", mapValue = podName }
                      , { mapKey = "environmentURL"
                        , mapValue =
                            "https://${spinnakerEnv}.zende.sk/#/applications/\${ execution.application }/executions/details/\${ execution['id'] }"
                        }
                      , { mapKey = "organization", mapValue = "zendesk" }
                      , { mapKey = "project", mapValue = "zendesk" }
                      , { mapKey = "state", mapValue = "success" }
                      , { mapKey = "targetURL"
                        , mapValue =
                            "https://${spinnakerEnv}.zende.sk/#/applications/\${ execution.application }/executions/details/\${ execution['id'] }"
                        }
                      ]
                    , refId = "github-deployment-success"
                    , requisiteStageRefIds = Some [ "deploy-succeeded" ]
                    , statusUrlResolution = Some "getMethod"
                    , type = "updateGithubDeploymentStatus"
                    }
                  , spinnaker.Stage::{
                    , alias = Some "preconfiguredWebhook"
                    , completeOtherBranchesThenFail = Some False
                    , continuePipeline = Some True
                    , failPipeline = Some False
                    , name = "GitHub Deployment Failure"
                    , parameterValues = Some
                      [ { mapKey = "acceptHeader"
                        , mapValue =
                            "application/vnd.github.ant-man-preview+json"
                        }
                      , { mapKey = "deploymentId"
                        , mapValue =
                            "\${ #stage( 'Create GitHub Deployment' )['context']['webhook']['body']['id'] }"
                        }
                      , { mapKey = "description"
                        , mapValue = "See Spinnaker for deployment details"
                        }
                      , { mapKey = "environment", mapValue = podName }
                      , { mapKey = "environmentURL"
                        , mapValue =
                            "https://${spinnakerEnv}.zende.sk/#/applications/\${ execution.application }/executions/details/\${ execution['id'] }"
                        }
                      , { mapKey = "organization", mapValue = "zendesk" }
                      , { mapKey = "project", mapValue = "zendesk" }
                      , { mapKey = "state", mapValue = "failure" }
                      , { mapKey = "targetURL"
                        , mapValue =
                            "https://${spinnakerEnv}.zende.sk/#/applications/\${ execution.application }/executions/details/\${ execution['id'] }"
                        }
                      ]
                    , refId = "github-deployment-failure"
                    , requisiteStageRefIds = Some [ "deploy-failed" ]
                    , statusUrlResolution = Some "getMethod"
                    , type = "updateGithubDeploymentStatus"
                    }
                  , spinnaker.Stage::{
                    , completeOtherBranchesThenFail = Some False
                    , continuePipeline = Some False
                    , failPipeline = Some True
                    , name = "Signal Execution Failure"
                    , preconditions = Some
                      [ spinnaker.Precondition::{
                        , context = spinnaker.PreconditionContext::{
                          , expression = Some "false"
                          , failureMessage = Some
                              ''
                              This stage failed in order to inform you that an error has occurred during
                              pipeline execution; see execution details for further information
                              ''
                          }
                        , failPipeline = True
                        , type = "expression"
                        }
                      ]
                    , refId = "signal-execution-failure"
                    , requisiteStageRefIds = Some
                        (   classicRollbackRefIds pod
                          # [ "github-deployment-failure" ]
                        )
                    , type = "checkPreconditions"
                    }
                  ]
            , triggers =
              [ spinnaker.Trigger::{
                , enabled = False
                , payloadConstraints = Some
                    ([] : List { mapKey : Text, mapValue : Text })
                , source = Some "classic-${podName}"
                , type = "webhook"
                }
              ]
            , updateTs = None Text
            }

in  { forPod }
