# Zendesk "Classic"

### GitHub Actions

| master                                                               | production                                                                             |
| -------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| ![Test](https://github.com/zendesk/zendesk/workflows/Test/badge.svg) | ![Test](https://github.com/zendesk/zendesk/workflows/Test/badge.svg?branch=production) |

## Owners

Classic is owned by the Support product and governed by the [Classic Development Laws][].

Notably, this may mean that you’ll need approval to do new work or implement new
features in Classic. We use [ADRs](doc/adr) to track these decisions.

| Ref    | Link                                                          |
| ------ | ------------------------------------------------------------- |
| Email  | support-eng-team@zendesk.com                                  |
| Slack  | [#support-eng-team][], [#classic-dev][], [#classic-deploys][] |
| GitHub | [@zendesk/support-review][], [@zendesk/classic][]             |


## Getting started

The [Zengineering Handbook](https://zendesk.atlassian.net/wiki/display/ENG/Zengineering+Handbook)
explains some of the basics needed to understand how Zendesk operates.

If you want to start making changes to Classic, you'll want to get [ZDI](https://github.com/zendesk/docker-images)
up and running on your machine.

If you run into any problems, just ask one of the old hands to work you through
the setup; [#classic-dev][] is a good starting point.

If you're not interested in test logs, you can disable them by adding
`export RAILS_DISABLE_TEST_LOG=1` to your shell configuration script.

#### Initial setup - install dependencies
    brew install mysql
    brew install libidn
    brew install icu4c
    brew install snappy
    brew install gnupg
    brew install imagemagick

For more details, see [Initial laptop setup](https://zendesk.atlassian.net/wiki/spaces/ENG/pages/139824557/Initial+Laptop+Setup).

## Testing a gem using a git dependency

Sometimes it is helpful to test a change to a gem before bumping the version of the gem. To do that
with bundler, you can point the gem reference at any piece of the git tree using a `git@` address:

zdi migrations update
zdi migrations migrate
rake db:test:prepare

```rb
gem 'zendesk_arturo', git: 'git@github.com:zendesk/zendesk_arturo.git', ref: 'commitish' # can be a sha/tag/branch
```

Then update your local bundle:

```sh
bundle update zendesk_arturo --conservative
```

Now is where you need to take an explicit step to support our docker build process. By default, any
subdirectories of `vendor/cache` are ignored by git. When pointing at a git ref, bundler will copy
the full gem repo into a subdirectory in `vendor/cache`. If you push with the subdirectory ignored,
the docker build will fail because it doesn't have git credentials to pull the repo.

To resolve this, you must manually add the explicit subdirectory to git:

```sh
git add -f vendor/cache/zendesk_arturo-abcd1234
```

Once this is done, you should be able to verify the gem is correctly installed from the logs of
`bundle install` during the docker build:

```
Using zendesk_arturo 0.8.7 from https://github.com/zendesk/zendesk_arturo.git (at /app/vendor/cache/zendesk_arturo-8a8aca4327fe@8a8aca4)
```

## Contributing

Classic follows our [Basic Development Workflow](https://zendesk.atlassian.net/wiki/spaces/ENG/pages/271528870/Basic+Development+Workflow)
and is subject to the [Classic Development Laws][].

## Public documentation

This repo contains the OpenAPI files that are used to generate the documentation for the Support REST API. Please cc **@zendesk/documentation** on any PR that adds or updates the documentation. For an overview of the docs process, see [How developer docs are produced at Zendesk](https://zendesk.atlassian.net/wiki/spaces/DOC/pages/641704628/How+developer+docs+are+produced+at+Zendesk) on the Docs team wiki.

## Testing

Classic uses Minitest.

Before you can run the tests, you may need to run DB migrations:
```sh
$ zdi classic -d shell
$ bundle exec rake db:test:prepare
$ bundle exec rake db:migrate RAILS_ENV=test
```

The commands above are usually sufficient to update your `zendesk_test`
database. But in some cases, you may need to run the most recent migrations
(e.g. if you wrote a migration and want to write unit tests that depend on it).
In such cases, you should bump Classic's version of the gem
`zendesk_database_migrations` and then re-run the commands above.

Run your unit tests locally with [testrbl](https://github.com/grosser/testrbl):
* `testrbl test/path/your_tests.rb` for a whole file
* `testrbl test/path/your_tests.rb:123` by line number
* `testrbl --changed` for changed tests or files in last commit

Unless you're a masochist, don't try to run the full test suite locally. That's
what GitHub Actions is for.

## Generated protobuf bindings

We have a set of protobuf bindings available.  For more information on how they are
incorporated, see [the README](lib/protobuf/README.md).

## Ownership

Classic is what remains of a legacy monolithic codebase where a number of
features originated. Because of this, there are many teams that still have an
interest in and ownership of segments of the codebase. To facilitate the
modification and deployment of changes in this complex ownership model, a system
for tracking which teams own what has been established to determine code review
standards and the required deployment strategy.

When making changes to a file owned by a team as specified in [`CODEOWNERS`](.github/CODEOWNERS),
that team will automatically be added as a reviewer for the PR. A :+1: must be
received by a member of that team before merging.

When changing an unowned file, `@zendesk/support-review` should be tagged to
either 1) grant an exception to change an unowned file; or 2) push the PR author
to assign an owner. If an exception is granted, that PR is ineligible to be
included in scheduled batch deploys and a plan must be specified before merging
for how and when the PR will be deployed to production separately (e.g. "I will
build a deploy train Wednesday morning to take this out.").

Ownership can be claimed by adding the file or directory to [`CODEOWNERS`](.github/CODEOWNERS)
in the proper format and removing it from [`unowned.txt`](test/files/unowned.txt).

A couple of additional things to keep in mind:
* Meta-teams, such as `@zendesk/support-review`, should not be assigned as owner
  of any code unless approved by `@zendesk/squonk`.
* Please refrain from assigning ownership to teams of which you are not a
  member.

## Deployment

See [`DEPLOY.md`](DEPLOY.md) for a thorough explanation of the Classic deploy
process.

[Classic Development Laws]: https://zendesk.atlassian.net/wiki/spaces/ENG/pages/509019151/Classic+Development+Laws

[#classic-deploys]: https://zendesk.slack.com/messages/CBX3FKN93
[#classic-dev]: https://zendesk.slack.com/messages/C1MD39D4Y
[#support-eng-team]: https://zendesk.slack.com/messages/C9Q88NF5M

[@zendesk/classic]: https://github.com/orgs/zendesk/teams/classic
[@zendesk/support-review]: https://github.com/orgs/zendesk/teams/support-review
