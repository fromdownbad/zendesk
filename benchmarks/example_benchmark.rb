require_relative 'benchmark_helper'

benchmark do |x|
  x.report('addition') { 1 + 2 }
  x.report('subtraction') { 1 - 2 }

  x.compare!
end
