require_relative 'benchmark_helper'

INT_PATTERN = /\d+/

INT = 10
STR = '10'
ARR = []
NULL = nil
HASH = {}

benchmark do |x|
  x.config(time: 5, warmup: 1)

  x.report('Regexp#match? Integer#to_s') { INT_PATTERN.match?(INT.to_s) }
  x.report('Regexp#match? String#to_s') { INT_PATTERN.match?(STR.to_s) }
  x.report('Regexp#match? Array#to_s') { INT_PATTERN.match?(ARR.to_s) }
  x.report('Regexp#match? Hash#to_s') { INT_PATTERN.match?(HASH.to_s) }
  x.report('Regexp#match? nil#to_s') { INT_PATTERN.match?(NULL.to_s) }

  x.report('String#match? Integer#to_s') { INT.to_s.match?(INT_PATTERN) }
  x.report('String#match? String#to_s') { STR.to_s.match?(INT_PATTERN) }
  x.report('String#match? Array#to_s') { ARR.to_s.match?(INT_PATTERN) }
  x.report('String#match? Hash#to_s') { HASH.to_s.match?(INT_PATTERN) }
  x.report('String#match? nil#to_s') { NULL.to_s.match?(INT_PATTERN) }

  x.report('.to_s.to_i > 0 Integer') { INT.to_s.to_i > 0 }
  x.report('.to_s.to_i > 0 String') { STR.to_s.to_i > 0 }
  x.report('.to_s.to_i > 0 Array') { ARR.to_s.to_i > 0 }
  x.report('.to_s.to_i > 0 Hash') { HASH.to_s.to_i > 0 }
  x.report('.to_s.to_i > 0 nil') { NULL.to_s.to_i > 0 }

  x.compare!
end
