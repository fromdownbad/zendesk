require_relative 'benchmark_helper'

BIG_ARRAY = ActiveRecord::Base.on_first_shard { ActiveRecord::Base.connection.tables }
REGEX_UNION = Regexp.union(BIG_ARRAY).freeze
TABLE_PATTERN = /FROM\s+`?(\w+)|(?:INSERT|REPLACE)\s+INTO\s+`?(\w+)|UPDATE\s`?(\w+)+/.freeze

# rubocop:disable Style/PercentLiteralDelimiters
SELECT_QUERY = %{SELECT `cf_values`.* FROM `cf_values` WHERE `cf_values`.`owner_type` = 'User' AND `cf_values`.`owner_id` IN (10001) /* controller:Api::V2::CurrentUserController#show,uri:http://support.zd-dev.com/api/v2/users/me.json,account:1,user:10001,from_app:no,trace_id:4738345937375959795 */}.freeze
UPDATE_QUERY = %{UPDATE `users` SET `users`.`last_login` = '2020-04-14 20:43:33', `users`.`updated_at` = '2020-04-14 20:43:33' WHERE `users`.`id` = 10001}.freeze
INSERT_QUERY = %{INSERT INTO `esc_kafka_messages` (`account_id`, `topic`, `partition_key`, `value`, `key`) VALUES (1, 'support.user_events', x'312f3130303031', x'129837129837', NULL)}.freeze
# rubocop:enable Style/PercentLiteralDelimiters

benchmark_without_gc do |x|
  x.report('SELECT, Regexp.union, String#match?') { SELECT_QUERY.match?(REGEX_UNION) }
  x.report('UPDATE, Regexp.union, String#match?') { UPDATE_QUERY.match?(REGEX_UNION) }
  x.report('INSERT, Regexp.union, String#match?') { INSERT_QUERY.match?(REGEX_UNION) }

  x.report('SELECT, Regexp.union, String#=~') { SELECT_QUERY =~ REGEX_UNION }
  x.report('UPDATE, Regexp.union, String#=~') { UPDATE_QUERY =~ REGEX_UNION }
  x.report('INSERT, Regexp.union, String#=~') { INSERT_QUERY =~ REGEX_UNION }

  x.report('SELECT, Regexp.union, Regexp#match?') { REGEX_UNION.match?(SELECT_QUERY) }
  x.report('UPDATE, Regexp.union, Regexp#match?') { REGEX_UNION.match?(UPDATE_QUERY) }
  x.report('INSERT, Regexp.union, Regexp#match?') { REGEX_UNION.match?(INSERT_QUERY) }

  x.report('SELECT, Regexp.union, Regexp#=~') { REGEX_UNION =~ SELECT_QUERY }
  x.report('UPDATE, Regexp.union, Regexp#=~') { REGEX_UNION =~ UPDATE_QUERY }
  x.report('INSERT, Regexp.union, Regexp#=~') { REGEX_UNION =~ INSERT_QUERY }

  x.report('SELECT, String.match, Array#include?') do
    SELECT_QUERY.match(TABLE_PATTERN) { |m| BIG_ARRAY.include?(m[1] || m[2] || m[3]) }
  end
  x.report('UPDATE, String.match, Array#include?') do
    UPDATE_QUERY.match(TABLE_PATTERN) { |m| BIG_ARRAY.include?(m[1] || m[2] || m[3]) }
  end
  x.report('INSERT, String.match, Array#include?') do
    INSERT_QUERY.match(TABLE_PATTERN) { |m| BIG_ARRAY.include?(m[1] || m[2] || m[3]) }
  end

  x.compare!
end
