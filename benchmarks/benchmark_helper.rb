# This provides some convenience methods and basic setup to write a benchmark.
require 'benchmark/ipsa'

# Mute the loggers so we don't see SQL queries during benchmarks.
ActiveRecord::Base.logger = nil
Rails.logger = nil

###############################################################################
# Helpers
#
# The methods here should be something that is used by almost every benchmark.
# If it isn't reusable, define the method within the benchmark file itself.
###############################################################################
def benchmark(&block)
  Benchmark.ipsa(&block)
end

def benchmark_without_gc(&block)
  GC.start
  GC.disable
  Benchmark.ipsa(&block)
  GC.enable
  GC.start
end

def use_account(specific_account)
  @specific_account = specific_account
end

def account
  @account ||= @specific_account || Account.find_by!(subdomain: 'support')
end

def url_builder
  Rails.application.routes.url_helpers
end

# Basic setup needed for most benchmarks.
ActiveRecord::Base.default_shard = account.shard_id
Rails.application.routes.default_url_options[:host] = account.host_name
