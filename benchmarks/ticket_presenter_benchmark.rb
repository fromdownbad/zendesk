require_relative 'benchmark_helper'
require 'zendesk/inbound_mail' # silence constant already defined warning during benchmark

user = account.owner

P1  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder)
P2  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:users])
P3  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:metric_sets])
P4  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:metric_events])
P5  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:permissions])
P6  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:incident_counts])
P7  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:sla_policy])
P8  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:dates])
P9  = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:first_comment])
P10 = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:last_comment])
P11 = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:reply_options])
P12 = Api::V2::Tickets::TicketPresenter.new(user, url_builder: url_builder, includes: [:tde_workspace])

TICKET = account.tickets.first

puts 'Benchmarking Api::V2::Tickets::TicketPresenter sideloads'

benchmark do |x|
  x.config(time: 10, warmup: 3)

  x.report('no sideloads')    { P1.present(TICKET) }
  x.report('user')            { P2.present(TICKET) }
  x.report('metric_sets')     { P3.present(TICKET) }
  x.report('metric_events')   { P4.present(TICKET) }
  x.report('permissions')     { P5.present(TICKET) }
  x.report('incident_counts') { P6.present(TICKET) }
  x.report('sla_policy')      { P7.present(TICKET) }
  x.report('dates')           { P8.present(TICKET) }
  x.report('first_comment')   { P8.present(TICKET) }
  x.report('last_comment')    { P8.present(TICKET) }
  x.report('reply_options')   { P8.present(TICKET) }
  x.report('tde_workspace')   { P8.present(TICKET) }

  x.compare!
end
