# Benchmarks!

This directory contains benchmarks for various pieces of the application. It also contains a helper with convenience methods, such as `account`, and a wrapper for the benchmark block.


## Usage

1. Write your benchmark in a new file in `benchmarks/`. The file should end in `_benchmark.rb`.
1. Require the benchmark helper:

    ```ruby
    require_relative 'benchmark_helper'
    ```

1. Write your benchmark using the `benchmark` helper. Don't forget to call `compare!` at the end of the block:

    ```ruby
    benchmark do |x|
      x.report('foo') { do_stuff }
      x.report('bar') { do_thing }
      x.compare!
    end
    ```

1. Run the benchmark with the `bin/benchmark` script:

    ```bash
    bin/benchmark benchmarks/my_file.rb
    ```


## Helpers

### `benchmark`

A wrapper method which calls `Benchmark.ipsa` and yields your block.

```ruby
benchmark do |x|
  x.report('something') { do_the_thing }
end
```

### `use_account`

Expects an `Account` record. This record will be used when configuring sharding and the default URL host.
Defaults to the `support` subdomain.

```ruby
use_account Account.find_by!(subdomain: 'dev')
```

### `account`

The `Account` record being used for configuration of the benchmarks.

### `url_builder`

An object with the Rails URL and path helpers. This is needed to run a presenter explicitly.


## Examples

### Basic

**`benchmarks/example_benchmark.rb`**

```ruby
require_relative 'benchmark_helper'

benchmark do |x|
  x.report('addition') { 1 + 2 }
  x.report('subtraction') { 1 - 2 }

  x.compare!
end
```

**Run it**

```bash
bin/benchmark benchmarks/example_benchmark.rb

Allocations -------------------------------------
            addition       7/3  alloc/ret        0/0  strings/ret
         subtraction       0/0  alloc/ret        0/0  strings/ret
Warming up --------------------------------------
            addition   154.016k i/100ms
         subtraction   154.793k i/100ms
Calculating -------------------------------------
            addition     12.792M (± 7.8%) i/s -     63.455M
         subtraction     13.030M (± 4.4%) i/s -     65.013M
```

### Executing a Presenter

**`benchmarks/presenter_benchmark.rb`**

```ruby
require_relative 'benchmark_helper'

PRESENTER = Api::V2::Tickets::TicketPresenter.new(account.owner, url_builder: url_builder)
TICKET    = account.tickets.first

benchmark do |x|
  x.report('model_json') { PRESENTER.model_json(TICKET) }
  x.report('present')    { PRESENTER.present(TICKET) }

  x.compare!
end
```

**Run it**

```bash
bin/benchmark benchmarks/presenter_benchmark.rb

Allocations -------------------------------------
          model_json   158895/18273  alloc/ret      50/50  strings/ret
             present     18714/1374  alloc/ret      50/50  strings/ret
Warming up --------------------------------------
          model_json   214.000  i/100ms
             present   123.000  i/100ms
Calculating -------------------------------------
          model_json      2.140k (± 3.3%) i/s -     10.700k
             present      1.228k (± 3.8%) i/s -      6.150k

Comparison:
          model_json:     2140.1 i/s
             present:     1228.2 i/s - 1.74x slower
```
