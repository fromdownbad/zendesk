	
var status_codes = {"0":"New", "1":"Open", "2":"Pending", "3":"Solved", "4":"Closed"};
var priority_codes = {"0":"Not set", "1":"Low", "2":"Normal", "3":"High", "4":"Urgent"};
var zendesk_username;
var zendesk_password;
var page_size = 15;
var page = 1;
var searchType = "";
var searchTerm = "";
var preparedSearchTerm = "";
var results = 0;
var debug = 0;

function setupPage_debug(sType, sSearchTerm, sLabel) 
{
	debug = 1;
	setupPage(sType, sSearchTerm, sLabel);
}	

function setupPage(sType, sSearchTerm, sLabel) {
	
	// if the protocol isn't specified (in the S-Control), default to http://
	if (domain.toLowerCase().indexOf("http://") == -1 && domain.toLowerCase().indexOf("https://") == -1)
		domain = "http://" + domain;
		
	zendesk_username = getCookie("zendesk_username");
	zendesk_password = getCookie("zendesk_password");

	searchType = sType;
	searchTerm = sSearchTerm;
	
	// check for default username and passwords. Those would be set in the S-Control in SalesForce.
	if (default_username > "" && default_password > "")
	{
		zendesk_username = default_username;
		zendesk_password = default_password;
	}	
			
	content = getTitle("Zendesk Tickets", false) + 
		"&nbsp;<a href=\"#\" onClick=\"doShowCases()\">Click to see tickets for '" + sLabel + "'</a>";

	replaceHtml('output',content);

	frameResize();	
}

function getSearchTerm()
{
	// Check for prepared query
	if (preparedSearchTerm > '')
		return preparedSearchTerm;
	else {
		// no prepared query. Create query
		if (searchType == 'contact') {
			// retrieve contact name and email
			result = sforce.connection.query("select email, firstname, lastname from contact where Id = '" + searchTerm + "'");
			records = result.getArray("records");		

			// prefer the email. If no email, use firstname + lastname
			// assumes records[0] exists.
			term = 'requester:' + (records[0].Email > "" ? records[0].Email : '"' + records[0].FirstName + ' ' + records[0].LastName + '"') + ' ';

		} else {
			// retrieve all contacts from the account
			result = sforce.connection.query("select email, firstname, lastname from contact where AccountId = '" + searchTerm + "'");
			records = result.getArray("records");

			term = "";
			for (var i=0; i<records.length; i++)
				// prefer the email. If no email, use firstname + lastname			
				term += 'requester:' + (records[i].Email > "" ? records[i].Email : '"' + records[i].FirstName + ' ' + records[i].LastName + '"') + ' ';

			// Retrieve the organization to include the functionality of the Pro-edition control
			/*
			result = sforce.connection.query("select Name from Account where Id = '" + searchTerm + "'");
			records = result.getArray("records");
			if (records.length > 0)
				term += 'organization:\"' + records[0].Name + '\"';
			*/
		}	

		term += ' status<closed';

		// save the query for paging purposes.
		preparedSearchTerm = term;
		return term;
	}
}

function waiting(t)
{
	// displays the spinner
	document.getElementById("waiting").style.display = (t ? "block" : "none");
}

function replaceHtml(el, html) 
{
        var oldEl = (typeof el === "string" ? document.getElementById(el) : el);
        var newEl = document.createElement(oldEl.nodeName);

        newEl.id = oldEl.id;
        newEl.className = oldEl.className;

        newEl.innerHTML = html;
        oldEl.parentNode.replaceChild(newEl, oldEl);

        return newEl;
};

function doShowCases()
{
	if (zendesk_username == null || zendesk_username == '')
		getLogin();
	else {
		page = 1;
		doShowList();
	}	
}

function doShowList()
{
	waiting(true);

	tempurl = domain + "/search.js";
	term = getSearchTerm();
	tempdata = "query=" + encodeURIComponent(term) + "&page=" + page;
	
	if (debug == 1)
	{
		alert(term);
		alert(tempdata);
	}
	
	l = String(tempdata.length);
	// prepare the AJAX-request. Include basic authentication for Zendesk.
	req = {
		url : tempurl,
		method : "POST",
		requestData : tempdata,
		requestHeaders : {
			"Authorization":"Basic " + toBase64(zendesk_username +':' + zendesk_password),  
			"Content-Type":"application/x-www-form-urlencoded", 
			"Content-Length":l},
		onSuccess : function(response) {
			showCases(response);
			waiting(false);
		},
		onFailure : function(response) {
			alert("Lookup Failed. Did you configure the S-Control and Remote Site Settings? (" + response + ")");
			waiting(false);
		}
	};

	sforce.connection.remoteFunction(req);
}

function getLogin()
{
	content = getTitle("Zendesk Tickets - Login", false) + 
	'<form id="loginForm" onSubmit="doLogin();return false;">' +
	'<table>' + 
		'<tr>' + 
			'<td class="fieldlabel">Username:</td>' + 
			'<td class="field"><input type="text" name="username" /></td>' +
		'</tr>' + 
		'<tr>' +
			'<td class="fieldlabel">Password:</td>' +
			'<td class="field"><input type="password" name="password" /></td>' +
		'</tr>' +
		'<tr>' + 
			'<td></td>' +
			'<td class="field"><input type="button" value="Login" onClick="doLogin()" class="button"></td>' +
		'</tr>' +
	'</form>';
	
	replaceHtml('output', content);
	
	frameResize();
}

function doPage(n)
{
	if (page+n > 0) {
		page+=n;
		doShowList();
	}
}

function getTitle(sTitle, bList) {
		return '<table class="title" cellpadding="0" cellspacing="0" border="0">' +
			'<tr>' +
				'<td>' + sTitle + '</td>' +
				'<td>' +
					(bList ? '<input type="button" id="btnPrev" class="button" value="Prev" onClick="doPage(-1);">&nbsp;' + 
					'<input type="button" id="btnNext" class="button" value="Next" onClick="doPage(1);">&nbsp;' +
					'<input type="button" id="btnRefresh" class="button" value="Refresh" onClick="doShowCases();">&nbsp;' : '') +
					(zendesk_username > '' && default_username == '' ? '<input type="button" id="btnLogout" class="button" value="Logout" onClick="doLogout();">&nbsp;' : '') +
				'</td>' +
				'<td></td>' + 
			'</tr>' +
		'</table>';
}

function doLogin()
{
	zendesk_username = document.forms['loginForm']['username'].value;
	zendesk_password = document.forms['loginForm']['password'].value;

	setCookie('zendesk_username', zendesk_username, null);
	setCookie('zendesk_password', zendesk_password, null);
	
	doShowCases();
}

function doLogout()
{
	setCookie('zendesk_username', '');
	setCookie('zendesk_password', '');
	
	zendesk_username = '';
	zendesk_password = '';

	getLogin();
}

function showCases(r)
{
	// retrieve JSON object
	cases = eval('(' + r + ')');
	
	st = getTitle("Zendesk Tickets", true);
	
	st += 

		"<table class='list' cellspacing='0' cellpadding='0' border='0'>" + 
		"<tr class='header'>" + 
			"<th width='60'>Action</th>" + 
			"<th width='20'>#</th>" + 
			"<th>Subject</th>" +
			"<th width='40'>Priority</th>" + 
			"<th width='40'>Status</th>" + 
		"</tr>";
	
	for (i = 0; i < cases.length; i++)
	{
		comment = cases[i].comments[0].value;
		if (comment.indexOf('\n') > 0)
			comment = comment.substr(0, comment.indexOf('\n'));

		st +=
			"<tr class='datarow' onMouseOver='hiOn(this);' onMouseOut='hiOff(this);'>" +
				"<td><a href='" + domain + "/tickets/" + cases[i].nice_id + "' target='_blank'>Open</a></td>" + 
				"<td>" + cases[i].nice_id + "</td>" +
				"<td>" + (cases[i].subject ? cases[i].subject : comment) + "</td>" +
				"<td>" + priority_codes[cases[i].priority_id] + "</td>" + 
				"<td>" + status_codes[cases[i].status_id] + "</td>" + 
			"</tr>";
	}
	st += "</table>";
	
	replaceHtml('output', st);
	
	frameResize();
}

// used for compiling basic authentication string.
function toBase64(input) {
	var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var output = "";
	var chr1, chr2, chr3;
	var enc1, enc2, enc3, enc4;
	var i = 0;

	do {
		chr1 = input.charCodeAt(i++);
		chr2 = input.charCodeAt(i++);
		chr3 = input.charCodeAt(i++);

		enc1 = chr1 >> 2;
		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		enc4 = chr3 & 63;
  
		if (isNaN(chr2)) {
			enc3 = enc4 = 64;
		} else if (isNaN(chr3)) {
			enc4 = 64;
		}
  
		output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) +
		keyStr.charAt(enc3) + keyStr.charAt(enc4);
	} while (i < input.length);
	return output;
}

function setCookie(c_name,value,expiredays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value) + ((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

function getCookie(c_name)
{
	if (document.cookie.length>0)
	{
		c_start=document.cookie.indexOf(c_name + "=");
		if (c_start!=-1)
		{ 
			c_start=c_start + c_name.length+1; 
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
				return unescape(document.cookie.substring(c_start,c_end));
		} 
	}
	return "";
}

function frameResize() 
{
	var sframe = new String(window.name);
	var iframeElement = parent.document.getElementById(sframe);

	var nHeight = (document.all && !window.opera)? document.body.scrollHeight:document.documentElement.offsetHeight;
	nHeight += (navigator.userAgent.indexOf("Firefox") != -1 ? 20 : 10);
		
	iframeElement.style.height = nHeight+"px"; 
}