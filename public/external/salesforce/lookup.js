
sXmlLogin = '<?xml version="1.0" encoding="UTF-8"?>' +
  '<SOAP-ENV:Envelope ' +
    'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
    'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
    'xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" ' +
    'SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" ' +
    'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">' +
      '<SOAP-ENV:Body>' +
        '<login xmlns="urn:enterprise.soap.sforce.com">' +
          '<username xsi:type="xsd:string">__USERNAME__</username>' +
          '<password xsi:type="xsd:string">__PASSWORD__</password>' +
        '</login>' +
      '</SOAP-ENV:Body>' +
    '</SOAP-ENV:Envelope>';

sXmlQuery = '<?xml version="1.0" encoding="utf-8"?>' +
  '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' +
  ' xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
  '<soap:Header>' +
    '<SessionHeader xmlns="urn:enterprise.soap.sforce.com">' +
'<sessionId>__SESSION__</sessionId>' +
    '</SessionHeader>' +
  '</soap:Header>' +
  '<soap:Body>' +
    '<query xmlns="urn:enterprise.soap.sforce.com">' +
      '<queryString>__QUERY__</queryString>' +
    '</query>' +
  '</soap:Body>' +
'</soap:Envelope>';


  sSessionId = "";
  sServerUrl = "";
  sUsername = "";
  sPassword = "";
  sEmail = "";
  objForm = null;

  function render_salesforcelookup(email)
  {
    $('salesforce_lookup_content').innerHTML =
      '<form class="form" onSubmit="lookup(this); return false;">' +
              '<input type="text" id="email" value="' + email + '"/>' +
              '<input type="submit" value="Lookup user" id="submit">' +
              '</form>';
  }

  function lookup(form)
  {
    objForm = form;
    sEmail = form.email.value;
    form.submit.value ="Submitting";
    form.submit.disabled = true;

    doc = sXmlLogin;
    doc = doc.replace("__USERNAME__", $('salesforce-widget').readAttribute('username'));
    doc = doc.replace("__PASSWORD__", $('salesforce-widget').readAttribute('password'));

    url = '/proxy/direct/?url=https://login.salesforce.com/services/Soap/c/13.0&body=' + doc;

    new Ajax.Request(url, {
      method:'post',
      requestHeaders: { SOAPAction: 'login' },
      onSuccess: function(transport) { doGetLogin(transport.responseText); },
      onFailure: function() { alert("Salesforce login failed."); reset(); }
    });

  }

  function doGetLogin(responseText)
  {
    reg1 = new RegExp("/.*<sessionId>(.*)</sessionId>.*/");
    reg1.match(responseText)
    sSessionId = RegExp.$1;

    reg2 = new RegExp("/.*<serverUrl>(.*)</serverUrl>.*/");
    reg2.match(responseText);
    sServerUrl = RegExp.$1; //.replace("https","http");
    sServerUrl = sServerUrl.replace("http://", "https://");

    if (sSessionId > "" && sServerUrl > "")
      doPerformQuery();
    else {
      if (responseText.indexOf("sf:LOGIN_MUST_USE_SECURITY_TOKEN") > -1)
        alert("Salesforce login failed - Salesforce network access needs configuration.");

      if (responseText.indexOf("sf:API_DISABLED_FOR_ORG") > -1)
        alert("Salesforce login failed - Your Salesforce ediion doesn't support API access.");
      reset();
    }
  }

  function doPerformQuery()
  {
    sQuery = "select Department, Email, Fax, FirstName, HomePhone, LastName, MailingCity, MailingCountry, MailingPostalCode, MailingStreet, MobilePhone, Name, OtherCity, OtherCountry, OtherPhone, OtherPostalCode, OtherState, OtherStreet, Phone, Salutation, Title " +
         "from contact " +
         "where email = '" + sEmail + "'";

    arr = sEmail.split(" ");

    if (arr.length > 1)
      sQuery = sQuery + " or (firstname = '" + arr[0] + "' and lastname = '" + arr[arr.length-1] + "')";

    sXml = sXmlQuery;
    sXml = sXml.replace('__SESSION__', sSessionId);
    sXml = sXml.replace('__QUERY__', sQuery);

    url = '/proxy/direct/?url=' + sServerUrl + '&body=' + sXml;

    new Ajax.Request(url, {
      method:'post',
      requestHeaders: { SOAPAction: 'query' },
      onSuccess: function(transport) { doGetResult(transport.responseText); },
      onFailure: function() { alert("Salesforce query failed."); reset(); }
    });

  }

  function doGetResult(sXml)
  {
    if (sXml.indexOf("<size>0</size>") > -1)
    {
      alert("No match found in Salesforce.");
      reset();
      return;
    }

    html = "";

    html += '<h5 style="font-size: 16px;"><a>' + getAttribute("sf:FirstName", sXml) + " " + getAttribute("sf:LastName", sXml) + "</a></h5>";

    attr = getAttribute("sf:Title", sXml)
    if (attr > "")
      html += '<p class="minimum">' + attr + '</p>';

    attr = getAttribute("sf:Department");
    if (attr > "")
      html += '<p class="minimum">' + attr + '</p>';

    p1 = getAttribute("sf:Phone", sXml);
    p2 = getAttribute("sf:MobilePhone", sXml);
    p3 = getAttribute("sf:Fax", sXml);

    if (p1 > "" || p2 > "" || p3 > "")
    {
      html += "<h5>Phone</h5>";
      if (p1 > "")
        html += p1 + ' <span class="sub">work</span><br />';
      if (p2 > "")
        html += p2 + ' <span class="sub">mobile</span><br />';
      if (p3 > "")
        html += p3 + ' <span class="sub">fax</span><br />';
    }

    e1 = getAttribute("sf:Email", sXml);
    if (e1 > "")
      html += '<br /><a href="mailto:' + e1 + '">' + e1 + '</a>';

    a1 = getAttribute("sf:MailingStreet", sXml);
    a2 = getAttribute("sf:MailingPostalCode", sXml);
    a3 = getAttribute("sf:MailingCity", sXml);
    a4 = getAttribute("sf:MailingCountry", sXml);

    if (a1 > "")
    {
      html += '<h5>Mailing address</h5>';
      html += a1.replace(",","<br />");
      if (a2 > "")
        html += '<br />' + a2 + ' ' + a3;
      if (a4 > "")
        html += '<br />' + a4;
    }

    $('salesforce_lookup_content').innerHTML = html;
    reset();
  }

  function getAttribute(sName, sXml)
  {
    reg = new RegExp("/.*<" + sName + ">(.*)</" + sName + ">.*/");
    if (reg.match(sXml))
      return RegExp.$1;
    else
      return '';
  }

  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
  }

  function reset()
  {
    objForm.submit.value = 'Lookup user';
    objForm.submit.disabled = false;
  }
