# ADR Title: Kafka Consumers in Classic

date: 2020-01-14T10:50:46+01:00

## Context

As the organization increases the usage of Kafka, we need to make sure that all projects that are producing messages to a Kafka compacted topic have the necessary tooling to handle account moves.

Having the ability to act upon account moves means that we need to be able to backfill the topic in the cluster the account is being moved to and produce tombstone messages for Kafka topics that contain account-specific data. In the context of Project Red Sea, this means making Users and Brands topics reliable, so Guide can entirely rely on their accuracy.

The goal of this ADR is to have Classic being able to keep the existing compacted topics up to date, taking into consideration account moves.

### Existing Solutions

As of right now, there is no solution in place to run Kafka Consumers in Classic.

## Decision

Classic currently owns the entity streams of brands and users but is not able to backfill these Kafka topics or tombstone messages when necessary, as described in the Context of this ADR. Classic is the owner of these tables; consequentially is the one responsible for maintaining the Kafka topics related to these tables up to date.

* Classic can run Kafka Consumer processes.
* Classic code must use Racecar (v1 and upgrade to v2 when it's ready), so we get consistent tracing, metrics, logs, and configuration.
* Classic code must discover Kafka in a standardized safe way, as discussed below.
* We should not reuse the same consumer for different entities nor should a consumer access data outside of its  domain. There are different consumers for handling account move for Brands and Users.

### Scope

* Classic can run new Kafka Consumers using a new Kubernetes deployment.
* Classic listen to partition move Kafka topic and takes necessary operations to keep `guide.brand_entities` and `support.user_entities` up to date.

## Consequences

### Benefits

* Classic can consume Kafka Topics.
* Teams now have the tooling for running Kafka Consumers in Classic.

### Costs

N/A

### Reliability & Resiliency

There are no changes in reliability or resiliency to Classic since the Kafka Consumers are running as separate kubernetes resources.

### Scalability

N/A

### Latency

N/A

### Data locality

N/A

### Type of data (PII etc.)

N/A

### Data deletion / GDPR compliance

One of the primary outcomes of this ADR is gaining the ability to tombstone and effectively clean up data the should no longer be present in compacted topics because of a move.

### Security

Kafka Consumers must have mutual TLS (mTLS) authentication configured. mTLS provides two-way authentication, so we ensure that the traffic is secured and trustable from both directions.

The Racecar library is currently supported; [check example configuration](https://github.com/zendesk/kafka-tls-client-examples/blob/master/ruby/racecar/config/racecar.rb).

### Security Compliance

N/A

### Internal stakeholders

N/A

### External dependencies

N/A

## Requirements

### Discover Kafka Brokers

We use Consul for service discovery; when using Racecar, it's enough to add a `KAFKA_BROKERS` environment variable with the following value `pod$ZENDESK_POD_ID.kafka.service.consul` to the Samson project. The project **must** have the `ZENDESK_POD_ID` variable added to it, so the `KAFKA_BROKERS` value is correctly evaluated.

For authentication, we also need to add the environment variable group described [here](https://zendesk.atlassian.net/browse/MELINF-1510).

### Kafka Consumer Framework

Currently, the easiest way to consume Kafka topics for Ruby projects is by using [Racecar](https://github.com/zendesk/racecar/).

The Racecar framework is being widely used in Guide to consume Kafka topics, and a new major version is under development. This new version uses [`rdkafka-ruby`](https://github.com/appsignal/rdkafka-ruby) instead of [`ruby-kafka`](https://github.com/zendesk/ruby-kafka), which solves a few known issues that we have today with Racecar.

We should upgrade to v2 as soon as it becomes production-ready, that way moving away from `ruby-kafka`.

### Account Move Domain Events

There are currently a few different topics backed out by different strategies that we can use to act upon a move. All the strategies are better described [here](https://zendesk.atlassian.net/wiki/spaces/SPY/pages/726434654/Account+Move+Events).

The preferred way is using Exodus move events, but it's also important to be aware that Foundation and Spyglass are working on a new standard. It is heavily encouraged that we move to that as soon as it's ready for production.

## Rollout plan

* Add Racecar as a dependency in Classic.
* Have a new Kubernetes Deployment that runs a Racecar process, listening to the pod move topic and taking action in backfilling and tombstoning the `guide.brand_entities` Topic.
* Have a new Kubernetes Deployment that runs a Racecar process, listening to the pod move topic and taking action in backfilling and tombstoning the `support.user_entities` Topic.
* Update ZDI to support running Classic Kafka Consumers.
