The migration from `protected_attributes` to `strong_parameter` protection contains a good bit of techincal complexity. This document serves as a place to store knowledge related to the migration.
#### protected_attributes

With the [protected_attributes](https://github.com/rails/protected_attributes)
gem, if a model is annotated with attr_accessible/attr_protected, when we do:

```
model.assign_attributes(attributes)
or
model.attributes = attributes
or
model.update(attributes)
or
model.create(attributes)
```

the protection kicks in by introducing a mass-assignment sanitization step.
`attr_accessible` and `attr_protected` simply add an attribute to an allow list or block list, respectively,
through which the `attributes` can be filtered out. The kicker here is that `attributes`
can just be any hash, and we get a good amount of protection if this code is
assigning values in a non-request context.

If you do `model.field = value`, this skips any sanization step related to
mass-assignment as it is assigning a single attribute, not mass assigning a hash of attributes.

#### strong_parameters

Rails 4.0 moved mass assignment protection from the model to the controller
through a new feature called [strong_parameters](https://github.com/rails/strong_parameters). With this, a controller
can only create/update a model through mass assignment of the request parameters if it
has explicitly allowed the parameters through code like `params.permit()`.
In a controller, `params` is an instance of ActionController::Parameters. Under strong
parameters, `assign_attributes` will only sanitize the attributes it receives if it is an instance
of ActionController::Parameters (confirmed by checking if `attributes` responds to `.permitted?`).

For regular hashes, it will simply do a basic attribute protection (based on
primary keys). For example, it will not allow you to change the `id` field on
model based on primary key detection. But crucially, the allow/block list is
not used.

The strong parameters feature is intended to protect models from mass assignment
of data received from an external source, not from an internal call.

#### stronger_parameters

Zendesk has built upon `strong_parameters` by adding type constraints to it.
This gem is called [stronger_parameters](https://github.com/zendesk/stronger_parameters) and is
currently used sparingly in Classic.

#### allowed_parameters
The `stronger_parameters` gem is further augmented by the [allowed_parameters](https://github.com/zendesk/allowed_parameters) gem, which is an extraction of the
[ParameterWhitelist](https://github.com/zendesk/zendesk/blob/master/lib/zendesk/parameter_whitelist.rb)
module originally defined in Classic (and copied in [many other
places](https://github.com/search?q=org%3Azendesk+%22class+AllowedParameters%22&type=Code)).
This gem implements the `allow_parameters` directive for each controller action, which
allows us to filter the bad parameters out in a `before_action` step.

A permutation of this module has also been included in the
`stronger_parameters` gem and is called
[PermittedParameters](https://github.com/zendesk/stronger_parameters/blob/master/lib/stronger_parameters/controller_support/permitted_parameters.rb).
This is not used by Classic code (or any other gems Classic depends on). While very similar, the modules are not currently interchangeable.

#### Current state in Classic

Classic currently uses `attr_accessible` from `protected_attributes` to
define an allow-list at the model level which handles mass assignment of any
hashes that we pass in.

In addition, the ParameterWhitelist module was originally written for the external V2 Api.
Therefore, most external V2 Api routes already utilize the `allow_parameters` directive
to define an allow-list at the controller/action level, combined with the built in before_action
that filters out any unpermitted parameters.

Although the Api::V2::Internal::BaseController inherits from the Api::V2::BaseController, it includes `skip_before_action :whitelist_parameters`.
As a result, even though many of the controllers include the `allow_parameters` directive,
the directive has no impact on the actual params objects since the before_action does not run.

To upgrade to Rails 5, we must proceed to eliminate all `protected_attributes`
code and the gem from Classic and its dependencies, because the gem is not compatible
with Rails 5. In its place, all of the existing protections must be replaced (and
any missing protections added) by using the `allow_parameters` directive at
the controller/action level.

#### Key distinctions

The `protected_attributes` gem will provide its protection regardless of the origin of the code
that updates the model. The `allowed_parameters` gem only reviews data received by controllers.
Mitigation of the potential gaps between the protection methods is addressed below in
[Non-request contexts](#non-request-contexts)


#### Bugs and security risks

The version of protected_attributes (v1.0.9) used in Classic skips the
strong_parameters sanitization for those models which don’t provide any
attr_accessible/attr_protected annotations (https://github.com/rails/protected_attributes/issues/41) which is remedied by
[this PR](https://github.com/rails/protected_attributes/pull/43) and available in the next version of `protected_attributes`.
Upgrading to the next version of the gem is not necessary for the migration since we will be using `allowed_parameters` to
filter out any unpermitted/invalid parameters before any create or update call is made.


#### allowed_parameters vs PermittedParameters

One alternative we have for this work is to move to PermittedParameters first.
This lacks things like logs, exceptions and Datadog instrumentation at the
moment. Various gems are currently using allowed_parameters through copied
implementation. We feel that the safe way is to first standardize on the
closest implementation, and then switch.

Another alternative is to switch the gems to use PermittedParameters whenever
possible in a transparent manner. Code will switch between allowed_parameters
and permitted_parameters based on when running in Classic, vs running in
tests. We believe this to be a bit risky, as the implementations on either
side are already slightly different, and potentially more variations can happen
while we proceed with the migration. Let us also test what we will be running in
production rather than what we will be using in the longer term.

#### Non-request contexts

When we remove `protected_attributes` for models, we will remove basic
protections which were available for things like jobs and other script based
services which directly manipulate our models. Clearly, these are internal services,
which unfortunately do not use internal API to manipulate our models. In this case,
we are trusting our developers to be more careful when they are using hashes to
assign values to a model.

It is possible that we will introduce ways of doing the wrong things here: A
script based service could poll from an external service, say facebook, and
decide to create a ticket comment using the response it received without
sanitization. Another example is a public API which enqueues a job with
unsanitized parameters as job-arguments. The Job could then go on to modify
models with this data and cause trouble.

A very good reason we are not fixing this situation is because there is no
solution that exists today in the Rails world to cater to this. We must instead
rely on careful development rather than attempt to fix these edgecases.

We had conversations with @zendesk/ocean and @zendesk/strongbad regarding zendesk_channels and emails, respectively,
and both felt confident that the data flowing through their services was being handled properly and
would not manipulate models incorrectly.

#### Brakeman

Brakeman has checks which protect against dropping attr_accessible. Whenever we
remove attr_accessible, we will introduce a new entry in brakeman.ignore, with
an appropriate note indicating this ADR. Once the migration is done, Brakeman
may completely remove this check, in favor of any checks related to
stronger_parameters. This work is out of scope of the migration.

#### Changes to tests

How an application behaves when it encounters unpermitted parameters can be configured for each environment. Classic previously
had configured the error to `:log` in production but `:raise` in development and test. This configuration has been adjusted in the
test environment (and example dev config) so the behavior of both production and development/test is aligned. Any tests that
tested the strong parameters framework will be adjusted or removed as appropriate.


#### Steps taken to minimize risk

This is a large migration affecting the entire Classic application and has security implications. In order to minimize this risk,
two temporary logging and tracking mechanisms have been put in place:
1.  *Creation of the `Simulated` module in `allowed_parameters`*: this module allows the `allow_parameters` directived to be added to
    a controller in a purely observational way. This was created in response to the concern about gaps in knowledge regarding the parameters being
    received by controllers. While some of this is tested or obvious through the code, there are some parameters used in co-dependent gems that
    are less obvious. The `Simulated` module records any parameters that are received by a controller that are either not contained in or are not
    the allowed value type declared in the list of allowed parameters for that action. This data can then be analyzed to determine whether the
    parameters should be added to the list of allowed parameters or filtered out. This module helps mitigate security risk by providing more data
    about parameters being received and also helps mitigate the risk of a parameter being removed that is needed by Classic or a co-dependent gem.
1.  *Capturing data about attributes that are currently being removed by the `protected_attributes` sanitization*: this data was already
    being logged, but we've added the additional step of creating a [custom sanitizer class](https://github.com/zendesk/zendesk/blob/master/config/initializers/zendesk_custom_logger_sanitizer.rb)
    to track the instances of violations/model to better quantify any concerns. This will be most important when the `Simulated` module is removed
    to help ensure that there aren't any remaining "bad" attributes being sent to the model.
