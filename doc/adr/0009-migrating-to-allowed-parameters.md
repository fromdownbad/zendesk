9. Migrating from protected_attributes to stronger_parameters

Date: 2020-04-01

### Status

Accepted

### Context

Classic's mass-assignment protection mechanism changed in Rails 4, however,
the decision was made to extend the previous functionality using the
`protected_attributes` gem.  This gem is not compatible with Rails 5 so it must
be removed and replaced with Rails' strong parameters protection. We need to make
some very important decisions around how we write code, as well as the state of
existing protections.

Options:
1. Fork `protected_attributes` and make it compatible with Rails 5
1. Remove `attr_accessible`/`attr_protected` from models and use strong_parameters to control parameters permitted by controllers
   on a 1:1 basis
1. Leverage the `allow_parameters` directive to add parameter protection to every controller action before removing model level protections.
   Create a "Simulated" module to allow observation of the impact of the `allow_parameters` directive before it is actually implemented.


### Decision
-  We will not port and maintain `protected_attributes` gem for Rails 5 or above.
-  We will extract the ParameterWhitelist module into a gem called
   [allowed_parameters](https://github.com/zendesk/allowed_parameters)
   and add it to Classic, zendesk_auth, and zendesk_channels.
-  We will annotate all controller actions in Classic, zendesk_auth, and zendesk_channels
   with the `allow_parameters` directive to manage request parameters.
-  We will observe the parameters received by controllers by including the `AllowedParameters::Simulated`
   module in any controller to which we add the `allow_parameters` directive.
-  Specs will be written as needed to cover the testing gap.
-  Once we are comfortable that all necessary parameters and parameter
   types have been documented, we will remove the `Simulated` module from the controllers.
-  After the `Simulated` module is removed, we will remove the protected_attributes gem
   as well as all attr_accessible/attr_protected annotations.
-  We will then make a plan to move towards PermittedParameters (the
   implementation in stronger_parameters gem) and remove the allowed_parameters
   gem.

For a more detailed plan, read the [Classic plan to migrate over from
protected_attributes to
allowed_parameters](https://docs.google.com/spreadsheets/d/1orEMZ276uxb1FcB09CUi-laOEEBIk5p3qWMfVcAWhdY/edit#gid=1962224103)

### Rationale
Option 1 may have been the path of least resistance, however, all that would have done is to push
the larger problem down the road. It would also be a significant burden to maintain this fork
and would be a blocker for upgrading to Rails 6.

Option 2 would likely have touched less code, but since models are created or updated by unrelated controllers,
it also would have introduced a significantly higher risk of human error, which was unpalatable for
something that is protecting bad and potentially malicious data from entering the database.

We decided to go with Option 3 for several reasons.
-  The `allow_parameters` pattern was already well established in the V2 API controllers (ParameterWhitelist was originally written for the V2 API).
-  The `Simulated` module will allow us to observe the parameters that are being sent to each controller action without actually altering the params object.
   Please see [Steps taken to minimize risk](/doc/adr/0009-supplemental-details.md#steps-taken-to-minimize-risk) for additional information regarding
   the `Simulated` module
-  Model level protection is very explicit about what data is allowed to be added to the model. Adding a list of allowed parameters
   to each controller action allows us to continue to be explicit about the data that comes through.
-  The main goal of mass assignment protection is to protect the database from external malicious data. Utilizing the
   allow_parameters directive allows us to effectively "secure the perimeter" by effectively creating a contract of
   what a controller action is allowed to accept. This ensures that data will not get into the application without our knowledge.
-  There is a good amount of legacy code in Classic and some gaps in test coverage. This option provides us with the highest level of
   confidence that we will not introduce a bug and will maintain or increase the security of the application.
-  There are 3 gems, `zendesk_core`, `zendesk_channels` and `zendesk_auth`, that contain models and controllers that are closely
   interwoven with Classic. The use of the `Simulated` module allows us to make sure that the data needed by these gems is taken
   into consideration as well.

### Consequences
-  This process is very time consuming as it essentially requires multiple passes through every controller - first to add the
   anticipated allowed parameters under the `Simulated` module, a potential second pass to add any missed parameters discovered
   via logging, and finally the removal of the `Simulated` module. The tradeoff here is it adds an extra level of confidence and
   caution in a legacy codebase, where there are many unknowns. By first observing the parameters that are being sent to each
   controller action, there is an opportunity to create a better understanding of what data is being sent to and consumed by the
   application.
-  We will end up in the short term with yet another gem.
-  Classic will end up removing the protected_attributes gem.
-  Classic and its co-dependencies (zendesk_auth and zendesk_channels) will standardize on stronger_parameters and allowed_parameters
   until the latter gem is replaced with the PermittedParameters module from stronger_parameters, which is similar to
   allowed_parameters, but not fully interchangeable.
-  Classic jobs and non-request services will take on additional risk associated with removing the protection from attr_accessible.



---

If a deeper understanding of the technical complexities and overall project process is desired, [additional details can be found here](/doc/adr/0009-supplemental-details.md).
