# ADR Readme

### What's an ADR?

ADR stands for Architecture Decision Record.  See [Architecture Records](https://github.com/zendesk/architecture_records) for information on how Architectural Decision Records are created and used at Zendesk.

Be sure to familiarize yourself with how Zendesk creates and uses ADRs before creating one of your own.

### Can this document also be an index?

Yeah, either manually or by setting something up to use `adr generate toc` ¯\\\_(ツ)\_/¯

Or just use the GitHub directory listing ¯\\\_(ツ)\_/¯
