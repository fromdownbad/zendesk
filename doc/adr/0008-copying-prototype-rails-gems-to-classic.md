# 8. Copying prototype-rails and prototype_legacy_helper code into Classic

Date: 2020-03-09

## Status

Accepted

## Context

We need to update Rails to 5.x or above. The gems, prototype-rails and
prototype_legacy_helper are no longer supported in Rails 5.x and above.  These
gems are used by Classic based UI elements for some useful things. For
example, when you delete a Favicon in Admin->Accounts->Branding, the alert
about "Do you want to delete?" is generated via Prototype.js

The gem prototype-rails publishes version 1.7.0 of the JS, while Classic also
publishes 1.6.1 version through app/assets/javascripts/prototype/*. A similar
problem exists for all JS in thot folder. In addition, Classic also publishes
Scriptaculous.js within that folder.

The main alternatives considered for doing this are:
1. Remove all usage of these files.
2. Add Rails 5 support to these gems and maintain them.
3. Bring the relevant code from these gems into Classic.

## Decision

The approach that will be used to update these gems is #3:

1. Bring all used code from prototype_legacy_helper directly in to Classic.
2. Bring all used code from prototype-rails into Classic.
3. Leave the JS in the gem behind as they don't seem to be used.
4. Continue to publish JS from app/assets/javascript/prototype/ folder in Classic.

Further down the line, we will publish a plan to remove these files from Classic, including
Scriptaculous. This is being drafted in https://zendesk.atlassian.net/browse/R5U-679

### Rationale

We are doing this, in favor of removing the usage completely because this is a
faster route to Rails upgrade. There are several such stumbling blocks before
we can complete Classic Rails upgrade. Removing Prototype-rails is an
important and right thing to do eventually, but it will considerably extend the
timelines for Rails upgrade. We are taking the shorter approach here, but we
make sure that we will publish a plan to remove the usage of this code, which
any volunteer can contribute to.

Doing #1 is the 'right thing', but it will take too long. Doing #2 sounds like
a lot of wasted effort.

## Consequences

This will add ~700 lines of untested (directly) and unowned code into Classic.
This is code which has not changed in several years, and directly copied over
from the gems.

Classic will break its dependency on these gems.

There will be a plan to get rid of this code, included as a readme in the
folder.
