## Title - ADR 0003: Global Security Settings integration with Admin Center

## Context

Security settings include

1. Agent Log In Settings
* Zendesk user email and password (basic auth)
* Google auth
* Microsoft auth
* SAML (plan type specific, linked to end user setting)
* JWT (plan type specific, linked to end user setting)
* Enforce SSO/Disable basic auth password (access/normal or login form access)
* SSO Bypass - allow admins or owners to request a one-time sign-in link via https://subdomain.zendesk.com/access/sso\_bypass
   
2.  End User Log In Settings
* Zendesk - user email and password (basic auth)
* Google auth
* Microsoft auth
* Facebook auth
* Twitter auth
* SAML (plan type specific, linked to agent setting)
* JWT (plan type specific, linked to agent setting)
* Enforce SSO

3. Password control (for Agents &amp; End Users)
* High, medium, low
* Custom (plan type specific\*,  Enterprise only, Agent ONLY feature)
4. General
* Allow admins to set users passwords (only available to owner)
* Session expiration control (plan type specific\* Enterprise only, product change\*\*)
* User password change email notification
* IP restrictions
  * Whitelist of IP restrictions
  * Customers can bypass IP restriction
* Customer controlled user assumption
* Require two-factor authentication
    - Download this status of all agents with respect to two-factor auth status
5. Support Specific Security Settings
* Agents can use the Zendesk Support mobile app
* Automatic credit card redaction
* SSL certificate settings

Notes

1. security settings are accessible to users with admin or owner permission level.  If a setting is only available to the owner it will be noted above.
2. SSL will not be considered a security setting.  While the SSO feature is present in the current security settings tab in the support product, this feature is a Support/Guide specific feature and it will not be part of Central Admin.  The current location of this feature in the UI may not be the future location of this UI.  This is a product discussion for another ADR.  For the moment we are leaving this code in place for Support Legacy and Support CP4 customers this may change during the extraction of the UI to Lotus moving this feature control closer to where hostmapping is set on the Brand section.

Linked requirements

- Email notifications
- Audit of change events
- Password expiration jobs
- Two factor authentication status download

Database Storage of Global Security Settings

NOT SHARDED

- role\_settings
  - Agent and end user log in settings

SHARDED

- remote\_authentications
- custom\_security\_policies
- account\_settings
  - two\_factor\_enforce
  - enable\_agent\_ip\_restrictions
  - session\_expiration
  - account\_assumption\_expiration
  - account\_assumption\_duration
  - credit\_card\_redaction
  - admins\_can\_set\_user\_passwords
  - email\_agent\_when\_sensitive\_fields\_changed
- account\_texts
  - ip\_restrictions

## MVP

### COMPLETED WORK
The following work has already been written approved and deployed

1. Internal API endpoints for Admin Center to show and update security settings
  - GET api/v2/internal/security_settings
  - PUT api/v2/internal/security_settings

2. Globalization of specific features to work across all products
  - session expiration
      - This setting is currently in the custom password policy.
      - This feature was not working as expected so a series of BUG fixes were completed.
      - this setting was moved to the account settings table to allow customers to set this setting independent of password policy.
      - This setting will still be restricted to  Enterprise plan type customers.  This will depend on plan type and thus be Support product specific until a global feature framework is available to determine the Chat plan type independent of Support.
  - IP restrictions
     - the work to globalize IP restriction was done by chat. So that chat will apply the IP restrictions that are set via this API to the Chat customers and the customers will no longer be able to set IP restrictions independently from Admin Center.
     - The IP restrictions are plan type specific.  This work is currently in PR out for review and waiting on this ADR.

3. Modifications to the security settings models to support the needs of the Admin Center API/react infrastructure
4. Ensure that all Authentication mechanisms that are currently available to our Support customers are available to a shell account without Support
5. Ensure full test coverage
  - browser and unit test for security settings including tests of feature functionality.  This work is outside of classic but part of the MVP.

### OUTSTANDING WORK WAITING ON PR REVIEW AND ADR

1. Mechanism to handle security features that have a plan type dependency [PR link for SECDEV-2224](https://github.com/zendesk/zendesk/pull/33117)
  - This work was scoped specifically to ip\_restrictions since Chat already offers this feature and we would be taking away existing functionality if we did not support this feature.
  - JWT, SAML, custom password policy and session expiration that depend on plan type are going to be Support Specific until they are extracted from the classic infrastructure or a global feature framework can be used to detect the plan type across Chat and Support products.

2. Support product UI redesign for when Admin Center is present and the customer has the Support Product on their account. [PR link for SEDEV-1913](https://github.com/zendesk/zendesk/pull/32438)
3. BUG fixes that will arise when the settings are built and tested.  We have a number of these in our backlog and I would like all of these to be considered covered by this ADR.

## Decision
We will make security settings globally accessible for all products via admin center while maintaining functionality for legacy Support customers and maintaining Support-specific security settings.

## Consequences

PROS

- Security settings will be globally available for all products (current and future)
- Products such as chat will have access to an expanded set of security features.  SSO, session expiration, password levels, custom passwords
- Security settings have been updated and numerous bugs have been discovered and eliminated because of this work.
- A browser test suite is now available for these essential features and dashboarding will be developed so we can monitor accessibility of authentication.

CONS

- Additional code has temporarily been added to classic until authentication code can be extracted from classic into its own service
- Chat product will be dependent of classic for these services until product extraction
- Could not provide all settings due to lack of global feature framework

## Updates - Outstanding work (Post MVP)

1. Extract Security Settings from Classic into a stand alone service (extraction design will require the following)
  - Design the database or databases that will be needed to support all of the security settings described above (specifically global security settings)
  - Where does this service belong?  On its own or partnered with an existing service.  Pravda, Passport etc.
  - How do we support existing customers once we spin up a new service?
    - Data migration
    - Implementation of the new source of information
  - Dependencies
    - Data storage
    - Global feature framework to determine plan type dependencies in the multiproduct future
    - Engineering manpower
2. Convert the existing Security Settings UI to React using zendesk garden components
3. Global Feature Framework
  - Design and implement a framework that considers all products on an account and based on plan type rules determines if a feature is available to an account.
