# 10. Copying zendesk_auth code into Classic

Date: 2020-07-06

## Status

Accepted

## Context

The zendesk_auth gem contains authentication logic which is tightly coupled to Classic. The gem was [extracted as a standalone library](https://github.com/zendesk/zendesk/pull/8800) in 2014, but it is used exclusively by Classic. Previous attempts have been made to update the gem to be able to be used in a standalone service to handle login (Project Passport); as it stands now, the gem remains coupled to Classic and would require a significant overhaul to be compatible with extraction into a standalone service. This project has been iceboxed and is not currently scheduled. For the foreseeable future this code will continue to evolve as part of Classic.

Tests in zendesk_auth try hard to mimic the Classic app, and often fail. Functional tests are run against a [dummy Rails app](https://github.com/zendesk/zendesk_auth/tree/138fc9b5b3c8a3b5096b31c24343a061261522a0/test/dummy) with stub implementations of dozens of methods manually pasted from Classic. Writing good tests in this environment is more complicated than in Classic.

Under the current versioning process, changes are merged into the `master` branch and new versions are regularly released based on `master`. Changes must then be deployed in a linear order, creating an interdependency between changes and adding more overhead to every change made.

We considered the following options to address these problems:
1. Create and enforce a multi-branch versioning process in which changes are cherry-picked into a separate branch from master.
2. Merge zendesk_auth back into Classic.
3. Do nothing.

## Decision

The approach that will be used to address this problem is to merge zendesk_auth back into Classic.

* The zendesk_auth code will be merged back into Classic under a root-level `zendesk_auth/` directory.
* The code will continue to be linked as a gem by specifying a local path in the Gemfile.
* The test suite will be run independently as its own Github Action.
* Codeowners patterns will be merged with Classic's `CODEOWNERS` file.

### Rationale

More often than not, we've found that the zendesk_auth versioning process introduces additional risk into the process. Consider this scenario:

* Alice merges change A
* Bob merges change B
* Cara receives an urgent request and merges change C to address it.

Because zendesk_auth is a library, Cara will either need to deploy Alice and Bob's changes with her change, or diverge from the regular release process and release a special version of the gem which contains only her version. This problem was a contributing factor for [#inc-2020-06-24-d](https://zendesk.atlassian.net/wiki/spaces/IM/pages/1193608097).

We've also found that the versioning process for the zendesk_auth gem requires significant [additional overhead](https://github.com/zendesk/zendesk_auth/blob/87eadae9cd72e83a2c43fed742303d03ad389d20/README.md#releases) for each change made to the gem's code. For each change, we must open a PR in the gem, get it approved, release a new version of the gem, open a PR in classic to bump the gem, and get that PR approved. Option 1 would add even more friction to the process.

Therefore, given the current state of play, we see Option 2 as the fastest and safest path to allow quicker iteration on zendesk_auth code, thereby decreasing time-to-value for our customers and decreasing risk associated with deploying changes.

### Future

A glaring question that this ADR doesn't answer is a decision about whether or not the code should be fully merged back into Classic, or refactored and extracted into a standalone Ruby service. There is still a long-term need to either move login out of Classic or decide that login will always remain part of Classic. In either case, this project needs to be prioritized and business investment needs to be made to see that project through to completion. Merging the gem back into Classic in the manner described here does not close the door on either approach.

A potential next project, once this initial merge is completed, is to refactor the test suite to execute tests against the Classic app itself rather than against a dummy app. This change would ensure greater reliability of the zendesk_auth code, but could also make extracting the test suite later more difficult. Until a decision is made on the future architecture of login, the refactor would need to be accomplished while maintaining a logical separation between zendesk_auth tests and Classic.

## Consequences

* This change will add zendesk_auth back into Classic, and link it from the Gemfile as a local gem.
* Commit history will not be transferred, but it will remain available in https://github.com/zendesk/zendesk_auth in archived form. The merge commit message will reference this location.
* There will be a README included in the folder with context on the zendesk_auth library.
