# Idempotent Resource Creation
Date: 2019-08-21

## Status
Proposed

## Context
Local resource creation has the following advantages over their remote counterpart:
- Creation can be guarded by database transactions to ensure all or nothing semantics, i.e. resources are either created in their entirety or not at all;
- Errors that occur as part of the creation process are guaranteed to surface within the code.

Remote resource creation introduces issues related to the reliability of the network. For example, a resource may have been created successfully, but the caller encounters a network timeout and didn't receive an authoritative answer; it's unclear whether the request should be retried as is, risking creation of a duplicate resource.

## Decision
The HTTP protocol is extended for POST requests to make them idempotent [^1] when the `Idempotency-Key` header is present.

### Prerequisites
Responses need to be cached for a certain amount of time; we assume the general availability of Redis to provide this facility. The actual backend may be changed on a case-by-case basis.

### Out of scope
Authentication of requests isn't part of this concern, we assume that permissions are handled outside of the idempotent handling.

### Behaviour
When an `Idempotency-Key` header is provided, the corresponding cached response is looked up. When a cached response is present, it will be rendered without invoking the underlying action; a response comprises status code, headers, and response body. Note that if the parameters don't match the original request, an error response will be returned (400).

Note that when a controller action raises an error that's handled outside of the method body (e.g. validation errors), responses will *not* be cached and subsequent requests will invoke the action body again.

When a cached response was looked up during request handling, the `X-Idempotency-Lookup` response header is returned to indicate the result of that lookup, which is either `hit` or `miss`.

### Cache identifiers
Each cached response must be stored in a such a way that it avoids unintended information disclosure; the following request information will be used to construct cache keys:
- Request method; e.g. `create`, `update`, etc.
- Path; e.g. `/api/tickets` (for creation) or `/api/tickets/123` (for update)
- Account identifier
- Value of `Idempotency-Key` header (a sha1 digest is generated to ensure its size is contained)

### Implementation
Support for idempotency is activated through the inclusion of the `Zendesk::Idempotency` module, which introduces a singleton method on the base class called `idempotent_proxy()` which can be used as an argument to `.around_action` to wrap around an action that should be made idempotent aware.

```controller.rb
class Api::V2::TicketsController < Api::V2::BaseController
  # ...
  around_action idempotent_proxy(expiry: 15.minutes), only: :create
end
```

[^1]: For the duration of response cache lifetime.
