# Unified Configuration for Support

Date: 2019-11-08

## Context

Configuration in Support is not particularly straightforward and it is not well tested.

> Configuration testing is quite underrated in an industry where the majority of work is becoming configuration.

[Jaana B. Dogan](https://twitter.com/rakyll/status/1177324158956388352)

We are now designing systems to talk to one another beyond a method call. Distributed systems that cross programming
language boundaries require expressive, versioned, and correct interfaces at all times. Errors that used to be caught
with a linter, compiler, or tests now exist in a different domain. Before rolling out configuration updates,
we'll need verification that structure and values in these interfaces (YAML, JSON, HOCON, etc) are valid.

**@zendesk/squonk**'s work getting Classic onto Kubernetes revealed just how much implicit configuration exists in
Support's principal codebase. More than a year of work went into understanding a single codebase's configuration. Most
of that time was spent asking "What does this config do? What is it for? Where do we change it?" and most importantly:
"What are all the possible values?". Much of these questions are answered by common coding constructs: comments,
imports, types, and some stateless functions.

We're proposing a configuration language, [Dhall](https://dhall-lang.org/), for defining all app and infrastructure
settings. Below are some examples of "app and infrastructure settings".

### Use-cases

* Kubernetes config generation using the [dhall-kubernetes](https://github.com/dhall-lang/dhall-kubernetes) library
* Build-tool config (settings for webpack, sbt, gradle, etc)
* Build-time application settings
  * [Environment](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/Settings/outsidePod#L11) [tooling](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/Settings/fromPod#L11-L24) documentation (.env.x.development, x.yml.example)
  * Shared service identifiers ([Consul URL](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/zendesk/Consul.dhall), [ProxyNLB URL](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/zendesk/Pod/proxyNLB#L15), etc)
  * Sharing notable ports and endpoints between Docker, [K8s](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/Kubernetes/containers/AppServer.dhall#L26), and [Application config](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/Settings/outsidePod#L15)
* [Metadata sharing](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/Meta/package.dhall#L26-L30) like project name, team, email, etc. through all pieces of the project

#### Possible use-cases

* [Deploy pipeline](https://github.com/zendesk/support-graph/blob/a80244f92d792ef5e59a28aacf74df343b9135c4/config/Deployment/spinnaker.dhall) generation
* Defining shared Zendesk infrastructure config like [PODs](https://github.com/zendesk/config-service-data/tree/gziegan/dhall-example/dhall/Pod#pod)
* Replacing all of [zendesk_configuration](https://github.com/zendesk/zendesk_configuration/)
* CI config (travis, GitHub actions, cloudbuild)
* Sharing types across API schemas (GraphQL SDL, Swagger, Protobuf)
* Product limit definitions
* K8s cluster configuration

### Existing Solutions

Our current configuration languages are not unified and each have their own limitations or undesirable behaviors.

| Name    | Comments? | Imports? | Types? | Functions?       | Tests?     | Loops? |
| ------- | --------- | -------- | ------ | ---------------- | ---------- | ------ |
| JSON    | No        | No       | No     | No               | No         | No     |
| YAML    | Yes       | No       | No     | No               | No         | No     |
| HOCON   | Yes       | Yes      | No     | No               | No         | No     |
| ERB     | Yes       | Yes      | No     | Kinda, "partial" | via Ruby   | Yes    |
| Jinja   | Yes       | Yes      | No     | Kinda, "partial" | via Python | Yes    |
| Jsonnet | Yes       | Yes      | No     | Yes              | No         | Yes    |
| Dhall   | Yes       | Yes      | Yes    | Yes, "total"     | Yes        | No*    |

\* Dhall has two collections: Lists and Maps. There is only one way to "loop" over them: `List.fold` (a.k.a. reduce).
No out-of-bounds, no infinite loop (considered "partial"), only `fold` and abstractions atop of fold like
`List.map` and `List.filter`.

#### JSON, YAML, HOCON

JSON and YAML are quite similar. Both have odd quirks, YAML probably has more. They are both 90% "understood" within
a few minutes of reading. However, they have **zero** constructs to deal with repetition, validation, or modularization.
None of those constructs matter for a small application but matter a lot for configuring many dependencies.

HOCON is a super-set of JSON, adding imports and comments. Imports can be local, remote, or environment variables.
It attempts to reduce repetition with an override syntax `defaults { a: 1 } development = ${defaults} { b : 2 }`

HOCON's default error handling for imports is to [fail silently](https://github.com/lightbend/config/blob/master/HOCON.md#include-semantics-missing-files-and-required-files). Good HOCON-compatible libraries will provide additional
validation, but that does not come out-of-the-box.

JSON, YAML - If these were sufficient large-scale config solutions we wouldn’t have gems like Zendesk config, ERB
templates for K8s YAML, Kustomize for K8s YAML, etc.

HOCON - We dont't want to run Java libraries to generate K8s config. This spec is likely good enough for
application-specific config, but it would be insufficient for shared config for many reasons. Some being heavyweight
runtime, library-specific implementation for error-handling, inherited loose type system from JSON (arrays of different
shapes going "untagged"), and quite limited DRY solutions (no safe list operations).

#### ERB, Jinja

Programming configuration using a full-featured programming language is [often a bad idea](https://github.com/dhall-lang/dhall-lang/wiki/Safety-guarantees#effects). It's how we've gotten into such a mess with the `zendesk_configuration` gem. It's also some
of the worst tech debt that our team faced in the push for Classic on Kubernetes. The vast majority of general purpose
programming languages can do any dangerous operation. A dependency can log data, make malicious POSTS, and more in any
function... making security review quite laborious. Try booting any webpack (Javascript) project after a few months of
inactivity and you'll see dozens of security warnings for the "necessary" dependencies.

#### Kubernetes-specific Solutions

[Helm](https://helm.sh/docs/helm/) and [Kustomize](https://kustomize.io/). We aren't considering these for two reasons:

1. These encourage a separation between "DevOps" config and application config. Very often these values are shared. See
   Samson's environment variable groups for that sharing indirection. We want to eliminate that boundary since we
   so commonly cross it when writing to environment variables.*

1. They lack the validation infrastructure we want (types and tests).

\* We're converting rich and tested data structures into namespaced strings (env vars)? This is yet another layer of
complexity, serialization, and deserialization we need to test against. A layer that we also seldom effectively test.

#### Jsonnet

[Jsonnet](https://jsonnet.org/learning/tutorial.html) is a templating language from Google. If Dhall is "JSON + functions + types + imports"
Jsonnet is "JSON + functions + imports + OO". OO concepts exist to reduce repetition but add API complexity. The
language's lack of a type and built-in testing system make it a less safe alternative to Dhall. Some examples of
increased complexity include the concept of `self`, object construction lifecycles, `null`, and "computed properties".

From a Guide Ops perspective using Jsonnet the last few months:

> Jsonnet is good \[...\]. The most annoying thing with Jsonnet is the lack of types, so debugging is hard, you basically have to use trace(fancy print functions) to actually understand what is being passed around. Apart from that it’s great, in most cases you can evaluate any file, to see what it produces, which makes some parts of debugging easy, but when your doing complex things, it gets hard fast

\- Russell Sim

We don't yet know that complex config in Dhall will "get hard fast". But, we've not had trouble in our experiments
with a few services and the massive HashMap that is `zendesk_configuration`. We think Jsonnet is the best alternative
we've seen, but we believe Dhall's tooling is superior.

#### Dhall

We think the [design philosophy](https://github.com/dhall-lang/dhall-lang#design-philosophy) of Dhall makes a lot of
sense in a world with heaps of shared config.

Particularly:

* Beginner-friendliness

> Dhall needs to be a language that anybody can learn in a day and debug with little to no assistance from others.
> Otherwise people can't recommend Dhall to their team with confidence.

* Polish

> The language should delight users. Error messages should be fantastic, execution should be snappy, documentation
> should be excellent, and everything should "just work".

* Simplicity

> Every Dhall configuration file can be reduced to a normal form which eliminates all abstraction and indirection
>
> > "Users will go crazy with syntax and user-defined constructs"
>
> Dhall is a very minimal programming language. For example: you cannot even compare strings for equality. The language also forbids many other common operations in order to force users to keep things simple

#### Short term consequences

For shared config, Support relies on contributors writing tests against config structure and values in a shared config
repository. These tests are usually written in whatever generation tool they're using, if any at all. Currently,
that's Python via Jinja.

If config structure changes and a test case is missing, we hope said teams manually catch it before releasing to
production. When structure or value changes, we rely on that team to correctly tag these changes with the right semantic
version increment. This way, we can roll back independently if new config is broken. Otherwise, we have to wait for that
team to rollback the new version. We prefer Dhall's opinion of placing a version in the URL string, requiring a code
change to update config.

For our app config, Support will continue to [build](https://github.com/zendesk/zendesk/tree/master/kubernetes#kubernetes-role-configuration) [bespoke](https://github.com/zendesk/zendesk_configuration/)
[abstractions](https://github.com/zendesk/support-starter-java/blob/master/support-starter-config/src/main/java/com/zendesk/core/config/AppSettings.java). It will be difficult to share configuration without uniform imports independent of application
language.

#### Long term consequences

For both shared and app config, we will either see another tool or set of tools take off (Kustomize, Helm, Cue, etc),
a homegrown tool rising to the challenge, or a huge disparity in tooling for configuration and app config structure
looking very different project-to-project.

## Decision

We propose that services in Support use Dhall to configure their applications once they become sufficiently complicated.

By "sufficiently complicated" we mean any service that is:

* overriding shared config
* having difficulty testing some config
* deviating from standard deploy pipelines

If your service just needs a few key value pairs and is otherwise only building atop shared infra and code, there's
no need for Dhall! Depending on how much shared tooling we build, it might be time-consuming to opt-out in the future.

### Scope

Support services may be implement their configuration in Dhall. Organizations outside of Support may continue
to publish YAML and other common data formats. Support services may consume said data in Dhall by [wrapping, typing, and
testing](#Example-of-consuming-non-Support-/-non-Dhall-config) those artifacts with tools like `yaml-to-dhall` and `json-to-dhall`.

#### In-scope use-cases

* Kubernetes environments will read _dhall generated_ YAML
* Build-time environments will read _dhall generated_ JSON/YAML

Build time generation may include tests (in Dhall) that prevent bad config from being read into an application.
Example: If a developer changes an environment variable via Samson, the deploy will fail if the new value used by the
service is incorrect. Incorrect could be either a) missing (type-check) or b) wrong (Dhall test).

#### Out-of-scope use-cases

No app code will consume Dhall expressions directly.

**Note**: This means adding Dhall to `zendesk_configuration` and then continuing to publish it as a ruby gem is out-of-scope
for this ADR.

### Example of consuming non-Support / non-Dhall config

```bash
yaml-to-dhall ./Pod.dhall https://raw.githubusercontent.com/zendesk/config-service/data/pod14.yaml
```

[yaml-to-dhall](https://github.com/dhall-lang/dhall-haskell/tree/master/dhall-yaml#example) will validate config service's data about pod-14 by

1. validate the shape of the data with type checking (./Pod.dhall)
2. validate the values of the data with tests (tests defined inside ./Pod.dhall)

When config changes that YAML file, our Dhall representation will also update. If tests or type checking fails, we will
know immediately while writing code on our machines.

#### Interested parties

Organizations like ZNOC will not need to deal with Dhall directly. They can change the generated YAML or JSON. This is
less safe, but no different than what we have today (no config language).

Using Dhall fits within Engineering Productivity's current "App Config Solution" approach. Adoption is optional since
the end result for deployment config is either Kubernetes-compatible YAML or third-party-compatible JSON (Argo/Spinnaker).

To build trust with shared configuration, we've asked Foundation Config to provide us config as Dhall expressions in
addition to their plan of providing YAML, JSON, etc. They've accepted this request contingent on the ADR being
approved.

## Consequences

### Benefits

> Configuration bugs, not code bugs, are the most common cause I've seen of really bad outages. When I looked at publicly available postmortems, searching for “global outage postmortem” returned about 50% outages caused by configuration changes. Publicly available postmortems aren't a representative sample of all outages, but a random sampling of postmortem databases also reveals that config changes are responsible for a disproportionate fraction of extremely bad outages. As with error handling, I'm often told that it's obvious that config changes are scary, but it's not so obvious that most companies test and stage config changes like they do code changes.

^ [Dan Luu](https://danluu.com/postmortem-lessons/)

We will begin testing and typing configuration on its own. Tests will run on compilation. No CI setup, no programming
environment needed for distributing new versions of code. We should have many less outages due to bad config.

#### Incident Prevention

Mis-configuration is a common cause of incidents at Zendesk as of November 2019.

![Tableu-Root-Cause-Nov-2019](https://user-images.githubusercontent.com/3099999/68315427-6003df80-007d-11ea-834c-cd6799d22736.png)

As configuration is largely a deployment concern and we're in the process of moving off of Samson, it's hard to say
what configuration-based incidents would be solved by Dhall. For instance, [this configuration incident](https://zendesk.atlassian.net/wiki/spaces/ops/pages/766116403/2019-09-16-a+Incorrect+rollout+removed+K8s+worker+nodes+on+pod14) happened due to incorrect ordering of Samson script
stages. That kind of stateful setup could still happen if the required environment variables were set with Dhall or set
with Chef, YAML, etc.

However, if we rethink how we do deployments by making them as stateless as possible... Dhall could provide us some
very strong guarantees. If we adopt Dhall in Support, we'd likely pursue the following workflow for services:

![Dhall-based Pipeline](https://user-images.githubusercontent.com/3099999/68314928-a3118300-007c-11ea-8905-de4fa3b80b41.png)

Assuming the above pipeline is in place, here are some incidents that could be prevented with Dhall:

* [Sev 1](https://zendesk.atlassian.net/wiki/spaces/ops/pages/720568377/2019-06-05-b+Pod12+Outage+2019-06-05-c+Pod+13+15+20+Redis+connection+errors) - test the configuration. Missing or invalid configuration values would fail the Dhall build.
* [Sev 1](https://zendesk.atlassian.net/wiki/spaces/ops/pages/709722202/2019-05-21+Pod+12+and+15+Support+Issues+Non-SI) -
test the configuration. Incorrect port references would be caught by tests, or by types, depending on the implementation.
* [Sev 2](https://zendesk.atlassian.net/wiki/spaces/ops/pages/646673912/2019-03-01-b+ZAM+POD+18+Site+went+down+-+App+Market) -
would have been entirely avoided as on-the-fly dhall config changes would only be possible via env variables. This is not
the pipeline we're proposing, so config-updates-as-code would be **enforced** by Dhall. If a contributor forgot to bump
the modified config's version, using a "frozen" import would prevent reading in new config, preventing this incident.
* [Sev 3](https://zendesk.atlassian.net/wiki/spaces/ops/pages/761679847/2019-09-05-a+Issues+with+restarting+k8s+pods+Pods+14+19) -
some re-design required. Network data could be read in via Dhall instead of generated YAML. Moves the config read-in
to build/deploy time instead of runtime.

There are more configuration incidents I could list, but they require more in-depth explanations that engineers with
some Dhall experience would be able to figure out themselves.

#### Types

```json
{
  "resilience": {
    "retryTimOut" : 5
  }
}
```

The above typo is sort of obvious... when the diff is small and code reviewers are well-rested. But, as a reviewer,
how do you know that the typo is a typo? The consuming API could also have a typo. Perhaps a GitHub org search?
Third-party API docs? Is that key even nested in the right spot? We assume the original programmer tested this manually,
but sometimes the environment is hard to access without deploying pushed code. We get through all this day-to-day by
searching for example code or docs. But, that's a slow feedback loop. Just as we test our app's integration with
various libraries, we should be testing and perhaps typing our configuration.

Here's an [end-to-end example](https://github.com/zendesk/dhall-demo-app/blob/master/doc/end-to-end-example.md) that
covers two wins of typed config: safety and developer productivity.

#### Documentation

Dhall's docs are excellent. The entire language grammer is documented [here in ABNF](https://github.com/dhall-lang/dhall-lang/blob/master/standard/dhall.abnf).
There's comprehensive documentation describing [normalization, encoding, equivalence, and more](https://github.com/dhall-lang/dhall-lang/blob/master/standard/README.md).
Finally, all built-in types, functions, and operators are on documented [on this single page](https://github.com/dhall-lang/dhall-lang/wiki/Built-in-types%2C-functions%2C-and-operators).

If you're wondering how Dhall can make the claim that it is _provably_ not Turing complete, read some of the above
documentation.

### Costs

The cost of adopting Dhall is mostly in developer time training and converting old config to it. Based on the experience
of myself and a few other engineers, it takes about [an afternoon](https://github.com/zendesk/dhall-demo-app/blob/master/doc/intro.md) to be "dangerous". However, to build re-usable config can take a few days of learning about functions and the standard library.
Tools like `yaml-to-dhall` and `json-to-dhall` make conversion work significantly cheaper, but there will be a non-zero
time to type and test our configuration. As more config is expressed in Dhall, more configuration mistakes should become
impossible or harder-to-make. But each step: K8s, app config, build config, etc will take more time until Dhall is further
adopted. The total developer cost is likely a bell curve, peaking when the first large services are deployed entirely
configured by Dhall.

### Reliability & Resiliency

Many of our configuration environments use full programming languages: JS, Ruby, Python, etc. While we understand
these languages quite well, they do open us up to some common "footguns". Such as specifying overly complicated
configuration and potentially dangerous side effects - interacting with a DB, third-party API, etc.

Dhall's "easily debugged" in that the compiler error messages are quite good, there's an `--explain` option, and the
project is open-source. However, the language is self-hosted so a bug with Dhall-the-language will require working
knowledge of Dhall. The main CLI tool is built in Haskell. Currently, this would be a challenge to overcome if we had an
issue serializing Dhall expressions to YAML/JSON.

### Scalability

Hard to know if this will scale to Zendesk scale. It's a new tool like Cue. We believe it will scale better than no
unified language for configuration.

There's a [public list of companies](https://github.com/dhall-lang/dhall-lang/wiki/Dhall-in-production) using Dhall in
production but we'd certainly be early adopters.

### Latency

If you run Dhall as a build step, it may add some time to your deploy or image build time.

### Data locality

N/A.

### Type of data (PII etc.)

Non-secret configuration data. Ids, URLs, ports, and other references to data, services, external or internal.

Non-secret configuration data shoul'd have any PII.

### Data deletion / GDPR compliance

Assuming no one uses Dhall to define PII, GDPR is not in scope.

Dhall cannot persist data. It's an entirely expression-based language. A CLI tool can pipe its output to a file but
that's not a concern of Dhall the language.

### Security

A very well-writted post on safety and security is written on the [repo's wiki](https://github.com/dhall-lang/dhall-lang/wiki/Safety-guarantees)

Security risks:

* [Cross-site scripting (XSS)](https://github.com/dhall-lang/dhall-lang/wiki/Safety-guarantees#cross-site-scripting-xss)
* [Server-side request forgery](https://github.com/dhall-lang/dhall-lang/wiki/Safety-guarantees#server-side-request-forgery)

XSS is prevented by Dhall in the following ways:

> Dhall protects against leaking sensitive files and environment variables by restricting transitive imports of remote expressions.
> The Dhall language also does not allow "computed imports" (i.e. imports where the import path depends on a value within the configuration file). This prevents information from being leaked through the requested path. Similarly, the language does not permit conditional importing of values. The interpreter is "strict" and always resolves imports regardless of whether the imported expressions are used. Therefore, the set of imports your program fetches cannot be used to leak program state. At most the imports will reveal what you have not yet locally cached, but nothing else.

Server-side request forgery is prevented by Dhall via built-in CORS:

> The language protects against by providing built-in support for Cross-origin resource sharing (CORS), so that transitive imports must opt in to answering transitive requests made on a client's behalf. This protection blocks the above exploit because the interpreter will check if a transitive import like http://169.254.169.254/latest/meta-data/iam/security-credentials/role supports CORS (it does not) and will therefore reject the request.

For someone doing a security review, using `dhall normalize` will show every line of code for every import, nested n
levels deep. There is an `--alpha` helper to condense that code further (removing pure helper functions, for instance).

As a security reviewer, I'll be able to see all
[effectful code](https://github.com/dhall-lang/dhall-lang/wiki/Safety-guarantees#effects) at once:

* Importing expressions from a file path (absolute, relative, or home-anchored)
* Importing expressions using an HTTP/HTTPS request to a web service
* Importing expressions from an environment variable

### Security Compliance

(Talk about the compliance risk that using this technology / standard exposes Zendesk to.

* Are you storing data in a new system? No.
* Are there impacts to any other certifications, for example [PCI](https://zendesk.atlassian.net/wiki/spaces/SECC/pages/139821278/PCI+Controls+List)? No.
* Important controls to consider include Access Management, Change Management, and Logging and Monitoring. What engineering team will own updating or operating these controls?) This should all be covered by the access and change management roles we define
for Zendesk repositories on GitHub. Logging and Monitoring will not be affected, as the output will be JSON, YAML, etc.

### Internal stakeholders

* Support engineers

#### Impacted parties

These teams may or may not use Dhall to define shared config.

* Foundation Config
* Engineering Productivity

### External dependencies

[The Dhall language implementation](https://github.com/dhall-lang/dhall-lang)

Useful CLI tools:

[`dhall`](https://github.com/dhall-lang/dhall-haskell/tree/master/dhall)
[`dhall-to-yaml`](https://github.com/dhall-lang/dhall-haskell/tree/master/dhall-yaml)
[`dhall-to-json`](https://github.com/dhall-lang/dhall-haskell/tree/master/dhall-json)

## Requirements

To get started with Dhall, consider the following:

* Tools: `dhall` and `dhall-json` executables. Both obtainable via `brew install`.
* Resources & Training: A day or two to do the [official tutorials](https://github.com/zendesk/dhall-demo-app/blob/master/doc/intro.md).
* Documentation: [Dhall's website](https://dhall-lang.org/) has a lot of excellent documentation. Dhall's compiler
and linter will also help keep code consistent.

## Rollout plan

Q4: **@zendesk/squonk** does numerous deploys of [one of our services](https://github.com/zendesk/support-graph)
configured with Dhall. Our team will also replace [our custom ERB solution](https://github.com/zendesk/zendesk/tree/master/kubernetes#role-templates) for K8s definitions in Classic. Any Support team is welcome to use Dhall to configure their
services. Support teams can choose to use it only for K8s as a start, or go "the extra mile" and define config via Dhall
instead of environment variables.

Q4/Q1: **@zendesk/squonk** will do some Gameday testing in staging - we'll publish bad Dhall configuration and
see how our Dhall configured deploy pipelines and then do the same for a non-Dhall configured service.

Q1/Q2: Support engineers will weigh in on these results to determine if we want to continue with Dhall. If so,
we'll type out Zendesk Configuration and run the same experiments before deploying to production.

Q2: Support teams are pushed heavily be tech leads to use Dhall. This point is contingent on our previous experiments
demonstrating great results for R&S and engineering productivity.
