## Title - ADR 0004: API endpoint for Zendesk Website to resend Owner Verification Email

## Context

The Account sign up process occurs from www.zendesk.com website.  Sign up requires a user email and password.
In an effort to prevent fraudulent sign ups, email verification is now required for every customer who signs up for an account.
There is concern from the COO and Marketing that there is an impact on the sales funnel because of this verification step. We have seen a small reduction in conversion. All parties are in agreement that this change was necessary, and has value, but we need to make sure this process produces as little artificial drag as possible. A simple, clear method for sending a password reset email is an absolute must.


## The Problem
 
 Customer could be blocked from starting their trial if:
* Customer does not receive the verification email.
* The email address entered was incorrect.
* The email went to a spam folder and the customer didn't find it.
* The customer waited too long and the link in the email expired.
  
## Decision

 API endpoint for owner verification email to support the new sign up flow design. \
 New endpoint \
 `POST https:{subdomain}/.zendesk.com/api/v2/account/resend_owner_welcome_email.json'
 
The design team has redesigned the website so the last step sends the customer to look for the verification email.
On this final page there will also be a link for a customer who can not find the email to send in a new email address to resend the verification email.

### Consequences

* API will be unauthenticated
* API will be available behind a link on the www.zendesk.com website.
* Will utilize already existing verification email functionality.
* API will be very limited in functionality:
    * accessible to every account
    * only impacts the owner of that account
    * only works on unverified account owners
    * only works on trial accounts
    * throttle once per minute up to 5 times

### Functionality

* send an email to the account owner if the account owner email matches the email in the params
  * change the owner email and sends an email to the new email address.  Only if the email does not match, the account is a trial, and the owner is unverified.
  * Send an email to the original account owner email address if updated, informing them of the change
* Datadog will be used to monitor the frequency with which this path is used.  This should be used rarely.  If we detect high activity on this endpoint we will need to redesign the sign up flow and analyze the accounts that are changed.
    
## Risks
 * Product has reviewed the security risk involved in changing an email address on a trial account with an unverified owner, and approved of this change. [Z1 ticket](https://support.zendesk.com/agent/tickets/3840177)
* Account owner take over of an unverified account
* This is an unauthenticated api endpoint, however, the limited functionality of this endpoint should keep this from being used as an abuse vector.

## Future
 * email sending will be removed from classic.  This api endpoint will be using an already existing endpoint and that endpoint can be extracted and utilized in the future.  This will be added in the Support on Shared Services project.
 * Code in progress [pull request #33410](https://github.com/zendesk/zendesk/pull/33410)

