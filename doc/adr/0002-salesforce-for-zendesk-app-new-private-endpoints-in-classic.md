
# 2. Salesforce for Zendesk App new private endpoints in Classic

Date: 2018-06-14

## Status

Proposed

## Context

Presently, [**Salesforce for Zendesk**](https://www.zendesk.com/apps/support/salesforce/) app admin users can configure how Salesforce records are displayed in the ticket's sidebar. They can:
* specify (_add_[1], _edit_[2], _delete_[3]) which Salesforce object's records to display, including which _fields and related objects_[4] to fetch; and _set the criteria to match records_[5] with
* set limit and criteria for fetching related object records[6] 

**See in action:**
 [[1, 2, 3]](https://cl.ly/3b3d0A0f373U)
 [[4, 5]](https://cl.ly/2j3g1z3q0w1s)
 [[6]](https://cl.ly/41142q1T2p3I)


The above functionality are available in a page within Classic-- `/agent/admin/extensions`-- that in turn interacts with Classic controller `extension_controller.rb` to fetch, save, and delete sidebar app configuration records from the database. Normally, admin users of the app sets its configurations only once after it is installed; it is seldom they have to edit their existing configurations.

Additionally, [@zendesk/platycorn](https://github.com/orgs/zendesk/teams/platycorn) team (Integrations - Salesforce) is working to modernize the app, starting from the user experience. For the new **Salesforce for Zendesk**  app V2, the team plans on moving setting the app configurations from the Classic page to within the app itself.

The Integrations Team's long-term goal is to create an integration service that would host existing and potential third-party integrations with Zendesk products, including Salesforce. However, in the interest of swift delivery of **Salesforce for Zendesk** app V2, we're proposing to create new endpoints that would expose existing methods and models in Classic to the new sidebar app.


**Preview:** [Salesforce for Zendesk App V2](https://cl.ly/0K3F1j3Y1m0b)


## Decision

In order to make the methods and classes the Classic page presently utilizes accessible from the app, we are creating new API endpoints. In particular, the following endpoints will connect the app with our existing methods and models in Classic:
   
 * `configuration_controller` - responsible for CRUD operations on app configurations. Each subdomain is associated with only one configuration record-- a json string that contains all the objects, fields, related objects, and filters to be displayed in the sidebar app
    * fetch saved configuration
    ```
    GET /api/services/salesforce/configuration.json
    ```
    * add new or edit object in configuration (along with associated fields, related objects, mapping criteria)
    ```
    POST /api/services/salesforce/configuration/objects.json
    ```
    * filter related objects of saved configuration
    ```
    PUT /api/services/salesforce/configuration/objects/filter.json
    ```
    * reorder objects within configuration - __NOTE__:  a new functionality
    ```
    PUT /api/services/salesforce/configuration/objects/reorder.json
    ```
    * remove object from configuration record
    ```
    PUT /api/services/configuration/objects/remove.json?object_name={object}
    ```
 * `fields_controller`
    * _fetch requested Salesforce object's fields and related objects_ as options when editing/adding a Salesforce object to configuration
    ```
    GET /api/services/salesforce/fields.json?object={salesforce_object}
    ```
    * fetch requested related object's fields
    ```
    GET /api/services/salesforce/fields.json?object={salesforce_object}&relationship_name={related_object}
    ```
    * fetch Zendesk Support fields to use as mapping criteria
    ```
    GET /api/services/salesforce/fields.json?type=zendesk
    ```
 * `objects_controller` - fetch Salesforce objects as options to add into the Salesforce configuration
     ```
     GET /api/services/salesforce/objects.json
     ```

    In addition to preserving existing functionality, we intend to introduce new functionalities that require the following new endpoints to achieve:
 * `crm_data_controller` - an existing controller that fetches Salesforce records relevant to currently opened Support ticket/user
    * endpoint the new app calls out to after saving the app configuration that deletes cached `crm_data` for currently viewed ticket/user
    ```
    DELETE api/v2/users/{user_id}/crm_data.json?ticket_id={ticket_id}
    ```
    * endpoint the new app calls out to after saving the app configuration that deletes cached `crm_data` in bulk as well as the ticket's/user's the agent is currently viewing
    ```
    DELETE api/v2/users/{user_id}/crm_data/all_active.json?ticket_id={ticket_id}
    ```
    Similar to the present configuration page, no retry mechanisms are necessary. It is already sufficient to return the appropriate error message for the app to process, and the admin user to take action.

## Consequences

By taking out some of the elements from the Classic page, a new page to replace `/agent/admin/extensions` is being created in `zendesk_console`. The new page in console will also consume data from these proposed new endpoints.  That said, these endpoints are still for the very exclusive use of the Salesforce integration into Zendesk Support.

