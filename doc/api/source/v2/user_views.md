## User Views

<div style="background-color:yellow; text-align:center; font-size: 16px;">
    <strong>Please note that the User Views API is currently in beta and subject to change.</strong>
</div>

A user view consists of one or more conditions that define a collection of users. If a user in Zendesk Support matches a view's conditions, the user is included in that view. For example, a user view can define a condition that displays all users who were created within the last week.

@import app/presenters/api/v2/rules/user_view_presenter.rb

@import app/presenters/api/v2/rules/output_presenter.rb

@import app/presenters/api/v2/rules/user_view_conditions_presenter.rb

@import app/controllers/api/v2/rules/user_views_controller.rb
