## Incremental Tickets

The incremental ticket API is designed for API consumers who want to know about tickets that changed in Zendesk Support "since you last asked". It works something like this:

```
You: Hello Zendesk Support, give me the tickets since 0 o'clock
Us: Sure, here are the tickets up until, and including, 5 o'clock
You: Hello Zendesk Support, give me the tickets since 5 o'clock
Us: Sure, here are the tickets up until, and including, 7 o'clock
```

Because of this behavior, the API has a different behavior, requirements, and semantics than other APIs. Most important to note is that **the ticket response returns a lightweight representation of each ticket and does not include comments**. Custom fields are included but not multi-line ones. To retrieve the full ticket, use our [Tickets API](tickets.html).

@import app/presenters/api/v2/exports/ticket_presenter.rb

@import app/controllers/api/v2/exports/tickets_controller.rb
