## Internal API - Account Groups

<p class="alert alert-warning" style="margin-top:20px;">This API is internal and meant for Zendesk employees only. If you see this API but aren't signed in to the Developer Portal as a Zendesk employee, please contact us at api@zendesk.com.</p>

@import app/presenters/api/mobile/account/group_presenter.rb

@import app/controllers/api/mobile/account/groups_controller.rb
