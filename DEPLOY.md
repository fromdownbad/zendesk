# Deployment

| Resource   | Link                                                    |
|:-----------|:--------------------------------------------------------|
| GHA        | <https://github.com/zendesk/zendesk/actions?query=branch%3Amaster> |
| Samson     | <https://samson.zende.sk/projects/5>                    |
| Rollbar US | <https://rollbar-us.zendesk.com/Zendesk/Classic/items/> |
| Rollbar EU | <https://rollbar-eu.zendesk.com/Zendesk/Classic/items/> |

## Table of Contents

- [General Rules](#general-rules)
  * [SOC2 Controls Relevant to Deploying](#soc2-controls-relevant-to-deploying)
  * [Restrictions](#restrictions)
- [High-Level Workflow](#high-level-workflow)
- [Branches and Environments](#branches-and-environments)
  * [Master](#master)
  * [Production](#production)
- [Versioning](#versioning)
- [Scheduled Reset Deploys](#scheduled-reset-deploys)
  * [Eligible PRs](#eligible-prs)
  * [Ineligible PRs](#ineligible-prs)
  * [Preparing for the reset deploy](#preparing-for-the-reset-deploy)
    + [Notes](#notes)
    + [Caveats](#caveats)
- [Patches](#patches)
  * [Patching Production](#patching-production)
  * [Deploying to Production](#deploying-to-production)
- [FAQ](#faq)
- [Recovery](#recovery)
  * [Rollback](#rollback)
  * [Revert](#revert)
  * [Redeploy](#redeploy)
- [Verification](#verification)
  * [Best Practices](#best-practices)
  * [Rollbar](#rollbar)
  * [Datadog](#datadog)
    + [Overview Dashboards](#overview-dashboards)
    + [APM](#apm)

## General Rules

- 📢 Use [#classic-deploys][] for all discussions related to the Classic
  deploy process, including announcing and coordinating deploys.
- 💬 Questions, concerns, and problems can be directed at [#classic-deploys][]
  or [#support-eng-team][].
- 🚒 Please see [Recovery](#recovery) for how to roll back.
- 🗓 [Scheduled reset deploys](#scheduled-reset-deploys) to production occur every other week. [Not all changes
  are eligible](#ineligible-prs) to be part of those deploys. [Patch deploys](#patches)
  happen throughout the week.
- 👮 A buddy is required for all production deploys.
- ⛔️ Deploys are more restricted during code freezes. See [Restrictions](#restrictions)
  for more information.

### SOC2 Controls Relevant to Deploying

- 🔒 __(SOC2 C.24) Standard Code Deployment:__ All standard source code changes
  are approved prior to deployment into production using the buddy/ peer
  approval function within the deployment tool.
- 🔒 __(SOC2 C.25) Separate Environments:__ Code changes are developed and
  tested in separate environments prior to being deployed into the production
  environment.
- 🔒 __(SOC2 C.26) Code Testing (QA):__ All code changes, including emergency
  changes, must successfully undergo automated continuous integration testing.
  Regular changes are tested prior to being deployed into the production
  environment. Due to their nature, emergency changes may undergo testing and
  documentation post-production deploy.
- 🔒 __(SOC2 C.43) Non-standard Deploys:__ Non-standard code deploys are
  documented and authorized in JIRA tickets. Non-standard changes include:
  emergency deploys that are completed manually (when the deployment tool is
  down) and emergency deploys which use the deployment tool, but bypass the
  buddy/peer approval function.

### Restrictions

[Code freezes](https://zendesk.atlassian.net/wiki/display/ops/Production+Freeze)
are periods of time when the rules for making changes to code, including
deploys, infrastructure, or related resources become more strict.

If there are any questions, please ask in [#classic-deploys][].

## High-Level Workflow

Classic uses two branches as part of its development process: `master` and `production`.

Here is what that looks like a high level:

1. PRs are created against `master` with proposed changes and may be merged after
   receiving a :+1: and a passing build.
1. `production` branches are cut every other week by force-pushing current
   `master` to become the new `production`.
1. The new `production` branch is deployed to all customers.

PRs must meet [certain eligibility criteria](#scheduled-reset-deploys) to be
eligible to be included in the scheduled reset deploys.

Reset-eligible PRs on `master` will be deployed to production on the next scheduled Reset Deploy.

> ⚠️ Please note that a branch cut is a _force push_ of one branch to another,
> not a _merge_ of two branches. Any changes not present upstream of a branch
> to be cut will be lost.

## Branches and Environments

There are two distinct branches and associated environments in Classic:
`master` and `production`.

### Master

<(https://github.com/zendesk/zendesk/actions?query=branch%3Amaster>

The `master` branch is where the next scheduled reset release stays for several
days for verification and testing. There are two staging environment pods:
[Pod 998][] and [Pod 999][].

- [Pod 998][] is used for verifying changes before they are merged.
  - Before you deploy your changes to [Pod 998][], notify [#staging-env][] channel and rebase to the latest version of master to avoid rolling back previously merged changes.
- [Pod 999][] is the staging environment that serves as the guidepost for overall
stability of the release currently sitting on `master`.
  - Please refrain from deploying to this pod. If you MUST deploy to [Pod 999][] (e.g. account creation, infrastructure dependencies):
    - rebase to the current master
    - notify [#staging-env][] channel with a reason, duration, and when pod 999 is avaliable with master for testing.

#### Helpful Account/Pod Info
- You can [create staging accounts via API](https://zendesk.atlassian.net/wiki/spaces/QA/pages/95879584/Creating+an+account+via+API+Dev+Master+Staging) to a specific pod if `--data "force_pod_id=xxx"` (e.g. `--data "force_pod_id=998"`) is included in the request
- If your account is not in the correct pod, you can manually move the account over by doing the following (This is not an instant move, it may take up to a day):
    - locate the account on monitor
    - in the sidebar navigation, click on `Moves`
    - specify a [destination shard](https://github.com/zendesk/chef-repo/tree/master/data_bags/staging_shards)
    - click `Submit` to queue up the account move

The `master` branch is tagged and deployed to Staging, which includes both
[Pod 998][] and [Pod 999][], automatically after a merge.

If you'd like to test your changes manually in the staging environment, create
test accounts on both staging pods.

### Production

<https://github.com/zendesk/zendesk/actions?query=branch%3Aproduction>

Production releases are tagged and deployed to customers after a branch cut or a
production patch is merged.

The `production` branch is tagged and deployed to the [Build Assets - Production][] automatically
when changes are pushed to the branch.

Under certain circumstances, it's possible and/or necessary to [patch the
`production` branch](#patching-production).

Releases are deployed to production pods by sensitivity level. Always follow
Samson's preconfigured deploy order. Start with the **Production Canary** stage and continue until you see the stage labeled **Stop -- Don't Click Me**.

## Versioning

Classic uses a major-minor versioning system:

`v<MAJOR_VERSION>.<MINOR_VERSION>`

When a new `production` branch is cut, the tip of the branch is tagged with an
incremented major version and a `0` minor version (e.g. `v56.0`). For example,
if the last `production` branch had a major version of `55` (e.g. `v55.0`), the new
`production` branch would have a major version of `56` (e.g. `v56.0`). Every
subsequent merge to `production` is tagged with an incremented minor version
automatically (e.g. `v56.1`).

`production` inherits the same tags from `master` when a branch cut occurs
since the operation involves force-pushing the current `master` branch. Every
subsequent merge to `production` is tagged automatically with an incremented
minor version (e.g. `v56.2`).

## Scheduled Reset Deploys

Reset deploys align the `master` and `production` branches that gradually drift apart
due to our production patch process. Some code changes are able to ride along, mostly documentation,
additional tests, and small, low-risk changes.

Scheduled reset deploys are performed on a semi-regular basis, typically every other
week; immediately before each, a new `production` branch is cut
from the `master` branch. The tip of the new `production` branch is then tagged
and deployed to customers. Check the [#classic-deploys][] channel topic for the next
scheduled deploy date.

Not all PRs are eligible to be included in these deploys. For those PRs, you
will need to deploy them separately as part of a [deploy train](#faq), or in
isolation.

PRs with any of the following characteristics are ineligible for inclusion in a
reset deploy:
* a self-described high risk level
* changes more than 5 relevant lines
* changes a gem version
* changes a file listed in the [`unowned.txt`](test/files/unowned.txt) file

Relevant lines are those that are not comments or excluded by directory,
extension, or filename. Some of the key exclusions include tests, documentation,
and translations. See the [exclusion configuration](https://github.com/zendesk/classic_companion/blob/master/config/deploy_assessment.yml)
for the full list.

A PR is assigned a label in GitHub automatically designating the required deploy
strategy. A `Deploy: Reset` label means the PR is eligible to be included in
a scheduled reset deploy; a `Deploy: Manual` label means it is not.

### Eligible PRs

These are PRs assigned a `Deploy: Reset` label in GitHub.

There is typically a scheduled reset deploy every other week.

Once your reset-eligible PR is merged into the `master` branch, your changes
will remain there until the next branch cut (and reset deploy). At that time,
the current `master` branch will become the new `production` branch.

### Ineligible PRs

These are PRs assigned a `Deploy: Manual` label in GitHub.

A few points of order to keep in mind:
* If you merge a PR into to the `production` branch, you must tag and deploy it
  immediately.
* You are encouraged to coordinate with other patch authors to reduce the number
  of deploys and save time. Use [#classic-deploys][] to coordinate.
* PRs against the `production` branch should be +1'd by the originating team for
  each included change before merging.

In addition, you should:
* have a plan to get your changes to production
* think about what level of sanity checking is appropriate in the staging
  environment
* consider putting your changes behind Arturo

If your PR is reset-ineligible, and it hasn't made it to production by other
means, it will be reverted off `next-reset` before a new `production` branch is
cut.

### Preparing for the reset deploy

A few days before the scheduled reset, a branch is made from the head of `master`. This branch,
`next-reset` serves as our point of reference in time so we have a consistent record of changes that
will be shipped during the reset deploy. _No changes merged to master after this branch is cut
should be deployed to production until the reset is complete._

Each time the deploy manager runs `./script/non-reset-prs` to check the backlog of changes, it will
use this `next-reset` branch as the point of reference.

#### Notes

Because we branch from master a few days before, it doesn't matter if you look at the SHA from
merging your PR, or if you find it on the `next-reset` branch. They will be the same.

#### Caveats

For non-emergency situations, no changes merged to master after this branch is cut should be
deployed to production until the reset is complete. This makes tracking PRs harder, and can make the
reset deploy more risky than necessary.

If you need to make an emergency patch that includes a PR that was merged _after_ the `next-reset`
branch was cut, **you must cherry-pick your changes from master to the `next-reset` _AND_
`production` branches!**

**Power-user tip**: To check if you have ineligible changes on `next-reset` but not `production`,
you can run `./script/non-reset-prs --limit=next-reset` in this repo. [Make sure to set the prerequisite env variables.](./script/non-reset-prs#L3-L11) Make sure you're on a RFA VPN!

## Patches

Patching is necessary when making changes to the `production`outside of the scheduled branch cuts.

### Patching Production

Patching production is appropriate in the following circumstances:
* You are deploying an emergency patch to production.
* You are deploying changes that are not eligible to be part of the scheduled
  reset deploys.

To make changes to production:
1. Notify [#classic-deploys][] that you intend to make a production patch and
   coordinate with others who are looking to do the same, which is best done by
   building a [deploy train](#faq). If you would prefer to take your changes out
   in complete isolation (e.g. they might be particularly risky), stake a claim
   to the next deploy slot by sharing a link to the production patch branch you
   are about to create.
1. Create a branch off `production` with your changes:
   ```sh
   # Get the latest from remote (i.e. GitHub)
   git fetch --all

   # Check out the production branch
   git checkout production
   git reset --hard origin/production

   # Create a branch (notice the prefix name to avoid collision)
   git checkout -b $(whoami)/patch-prod

   # Cherry pick the commit(s) (use the merge commit SHA)
   git cherry-pick -x -m1 merge-commit-sha

   # Push up the branch
   git push origin $(whoami)/patch-prod
   ```
1. Open a PR against the `production` branch (_not_ the `master` branch!).
   Standard code review practices and procedures
   apply (i.e. get a :+1: from someone; if PRs from other teams/engineers are
   included, those people make excellent reviewers).
1. Before merging, make sure you're ready to deploy *immediately*. If so,
   you may [deploy to production](#deploying-to-production).

### Deploying to Production

1. Merge your PR against `production`. It will be tagged and
   deployed to [Build Assets - Production][] automatically.
1. Watch for a notification in [#classic-deploys][] that the [Build Assets - Production][]
   stage has finished. It will be posted no matter the outcome and should only
   take a few minutes. If the stage fails, follow the link and try redeploying,
   which usually resolves the problem. If not, start a conversation in
   [#ask-burrito][]. When successful, you're clear to proceed to the next
   step.
1. Open and monitor the following dashboards throughout the deploy (see
   [Verification](#verification) for a thorough explanation of deploy monitoring
   best practices):
   * [Classic Deploy Overview Dashboard][]
   * [Rollbar US][]
   * [Rollbar EU][]
   * [Rollbar MTC US][]
   * [Rollbar MTC EU][]
1. Deploy your tag to the [Production -- Canary][] stage.
1. After the canary pod is deployed and stable, you may continue with Samson
   prompts (e.g. "Deploy vXXXX.X to..." buttons) to deploy the other stages.
   __After each stage, be sure to monitor the "Triggered Deploys" that are linked just above the deploy logs.__

## FAQ

__How do I get permissions to deploy classic?__

Any [admin in the Samson classic project](https://samson.zende.sk/projects/zendesk/user_project_roles) is allowed to give you Deployer permissions. You can usually find someone in [#classic-deploys][] who will grant you permissions (please don't `@here`). No ENGACS ticket is required ([source](https://zendesk.atlassian.net/wiki/spaces/SECC/pages/97321570/C.15+-+User+Access+Provisioning+Modification)).

__What should I do if my PR is ineligible to be part of a scheduled reset
deploy?__

Merge it into `master`. If you would prefer your changes to go out in complete isolation,
follow the [production patch](#patching-production) instructions. Otherwise, jump into
[#classic-deploys][] and coordinate with other engineers in a similar situation
to build a deploy train.

__How do I know if my PR is eligible to be part of a scheduled reset deploy?__

Look for one of the following labels on your PR in GitHub: `Deploy: Reset` or
`Deploy: Manual`.

The former means the PR is eligible; the latter means it is not.

__What is a deploy train?__

A deploy train is a PR against the `production` branch that contains multiple
cherry-picked PRs to be deployed. It's the recommended way for PRs that are
ineligible to be part of a scheduled reset deploy to make their way to
production.

We recommend deploy trains be limited to five PRs, at most, unless a single team
is taking out a batch of their own changes.

To be clear, there is not a requirement for PRs ineligible for inclusion in
a scheduled reset deploy to go out in complete isolation *from each other*.
So, go forth and coordinate with your fellow developers; it can be quite fun to
employ a little teamwork.

(And if you want to take out other PRs at the same time, feel free.)

__How do I join a deploy train?__

Once you have permission from a deploy train author to join, here are the steps
to do so:
1. Visit the deploy train PR and note the branch name.
1. Pull down the deploy train branch and add your merge commit(s):
```sh
# Get the latest for the deploy train branch
git fetch origin <deploy train branch>

# Check out the deploy train branch
git checkout <deploy train branch>

# Cherry pick the commit(s) (use the master merge commit)
git cherry-pick -x -m1 merge-commit-sha

# Push up the branch
git push origin <deploy train branch>
```

Note:
- If you already have the pull request pulled down, make sure to run `git fetch && git reset
  --hard origin/<branch>` before proceeding to add commits; others might have
  added their own commits in the meantime.
- *Never* force-push deploy train branches unless you know what you're doing.
  You could clobber other additions pushed to the branch.
- Always use `master` merge commits when patching `production`;
  doing so will ensure your patches are recorded properly by our tracking
  scripts.
- Merge conflicts may occur on your deploy branch if you are cherry-picking a merge commit
  that depends on another that has not yet reached the `production` branch. If you
  determine that your commit is dependent on another reset eligible merge commit, you can
  also cherry-pick that merge commit to your production PR. If it is ineligible,
  work with the author of the commit to deploy it.
- Due to commit message checks, if you use the "Refined GitHub" browser plugin,
  you must change the default settings to uncheck the `sync-pr-commit-title`
  before merging your PR to `master`. If you have already merged without this
  step, use the `-e` flag for the `cherry-pick` step above to re-write the first
  line of your commit message in the standard format: `Merge pull request #xxxxx from
  zendesk/<branch name>`

__If my PR is reset-ineligible, do I have to take it to production immediately
after merging to `master`?__

No! Instead, the preferred workflow is to merge into `master`, verify your change in
the staging environment, and then coordinate a deploy to production from there.

__What happens if I don't deploy my reset-ineligible PR to production before the
next scheduled reset deploy?__

It will be reverted off the `next-reset` branch before the next reset deploy but stay
on `master` for patching.

Pro tip: Before merging an reset-ineligible PR to `master`, have a plan for
getting your changes to production before a reset deploy is scheduled to take
them out.  If you don't have a plan or your plan seems sketchy, perhaps you
should reconsider merging until the situation improves.

__What should I do if I have an reset-ineligible PR on `master` that I will not
be able to deploy before the next scheduled reset deploy?__

Your PR will stay on `master` but will not go out with the reset deploy automatically.
You will still need to patch production with your ineligible PR. Take care to not
merge an ineligible PR to `master` without a plan to patch it to production on a
timely basis.

__What if I have a risky PR on `master` that should "bake" in the staging environment
longer?__

If the PR is reset eligible and needs further testing in Staging, please revert
it prior to the `next-reset` branch cut or discuss it with the reset deploy manager
or it may be deployed to `production` early with the reset deploy.

If the PR is reset ineligible it will be reverted off `next-reset` prior to the
deploy.

__Why do we cherry pick the merge commit instead of the individual commits from
master?__

The SOC2 compliance auditors will want to see that every commit comes from a PR
that has been code reviewed and given a +1 and has a green build. For this
reason, we cherry-pick the merge commit.  The merge commit message contains the
PR number that allows the auditors to use GitHub to verify that we followed our
process.

__What's the recommended strategy to take when a deploy is only partially
successful?__

If a deploy has to be interrupted for any circumstance in the middle of the
process (i.e. it has been deployed successfully to a subset of pods), deploy the
previous tag to the stages that got the new tag so there is a consistent version
across all pods in case your changes had nothing to do with the interruption.
Notify [#classic-deploys][] that you've done this.

__Why do I see that my PR needs 6 review approval to merge?__

This is because a reset deploy is about to happen soon, and we are blocking merges
to master. See [holding master changes](#holding-master-changes) for more details.

## Recovery

### Rollback

To roll back a deploy, [deploy the desired tag to production](#deploying-to-production).
This will usually mean simply decrementing the minor version of the tag of your
initial deploy (e.g. if you're deploying `v524.7`, roll back by deploying
`v524.6`).


### Revert

[Open a PR](#patching-production) with the reverted merge commits and
[deploy it to production](#deploying-to-production) once merged.

### Redeploy

If you are deploying a previous tag already released, [deploy it to production](#deploying-to-production)
without cutting a new tag.

## Verification

Because Classic affects so many different parts of Zendesk, verifying changes
can be tricky. Below you'll find a selection of tools to use.

### Best Practices

Here are some recommended verification best practices while deploying code to
production:
* Let the new code "bake" on the pods as you move through the steps to allow
  enough time to catch issues. Here are some guidelines for how long to stop on
  each step:

  |Step|Time|Notes|
  |----|----|-----|
  | Canary | 5 minutes | This step is there to ferret out blatantly obvious bugs, problems with the deploy process itself, or an environmental issue. |
  | 2 | 20 minutes | This is primary step to stop and watch. |
  | 3+ | N/A | Monitor the deploy via Datadog and Rollbar until ~15 minutes after the last stage finishes. |

* Monitoring the [Classic Deploy Overview Dashboard][] is critical. As the new
  code reaches a new pod, there will often be a blip in the metrics which should
  quickly settle. If abnormal results persist, it could be a sign of a problem.
* Pull up both the EU and US instances of [Rollbar](#rollbar) for both the Classic and MTC projects. Keep an eye out for new
  exceptions or spikes in frequency as evidenced by the small graph next to each
  exception.
* Watching worker queues on Pod 14 at the top of the hour often catches a brewing
  issue. To do this, filter the dashboard on `pod14` and look at the "Resque
  Queue Size by Namespace" graph.

### Rollbar

**A note about Rollbar during the transition to Kubernetes**

Due to data locality requirements, there are two Rollbar instances:
* [Rollbar US][]: Pods 12, 13, 14, 19, 20, 23, 25
* [Rollbar EU][]: Pods 15, 17, 18

The signal-to-noise ratio can be difficult to sift through. The bar graph on
the left hand side is useful for determining new errors or errors whose rate has
increased.

To discover new errors more easily, Rollbar's search bar in the upper right accepts
code version tags.  To see errors only occurring for your new tag `v123.4`, you can
filter for it in the search bar with `code_version:v123.4`.

### Datadog

#### Overview Dashboards

See: [Classic Deploy Overview Dashboard][]

Most graphs on this dashboard show safe regions for our average production pod.
Some key things to look out for are **Total Time Spent**, **Throughput**, and
**Resque Queue Size**.

#### APM

From Datadog, you can see the average response time and the throughput. These
should be roughly constant before and after your deploy, but there will be a
spike in response time:

- [pod12](https://www.youtube.com/watch?v=dQw4w9WgXcQ) - Classic Pod 12
- [pod13](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod13&paused=false) - Classic Pod 13
- [pod14](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod14&paused=false) - Classic Pod 14
- [pod15](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod15&paused=false) - Classic Pod 15
- [pod16](https://www.youtube.com/watch?v=dQw4w9WgXcQ) - Classic Pod 16
- [pod17](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod17&paused=false) - Classic Pod 17
- [pod18](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod18&paused=false) - Classic Pod 18
- [pod19](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod19&paused=false) - Classic Pod 19
- [pod20](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod20&paused=false) - Classic Pod 20
- [pod23](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod23&paused=false) - Classic Pod 23
- [pod25](https://zendesk.datadoghq.com/apm/service/classic/rack.request?&env=production&hostGroup=pod23&paused=false) - Classic Pod 25

[#ask-burrito]: https://zendesk.slack.com/messages/CAJA4SSFQ
[#classic-deploys]: https://zendesk.slack.com/messages/CBX3FKN93
[#support-eng-team]: https://zendesk.slack.com/messages/C9Q88NF5M
[#staging-env]: https://zendesk.slack.com/messages/C27P99J64

[Classic Deploy Overview Dashboard]: https://zendesk.datadoghq.com/dash/735134/classic-deployment-overview
[Rollbar US]: https://rollbar-us.zendesk.com/Zendesk/Classic/items/
[Rollbar EU]: https://rollbar-eu.zendesk.com/Zendesk/Classic/items/
[Rollbar MTC US]: https://rollbar-us.zendesk.com/Zendesk/Mail-Ticket-Creator/items/
[Rollbar MTC EU]: https://rollbar-eu.zendesk.com/Zendesk/Mail-Ticket-Creator/items/

[classic_staging_smoke_tests_998]: https://jenkins.zende.sk/view/classic_staging_smoke_tests_998/
[StagingStatus]: https://jenkins.zende.sk/view/StagingStatus/#

[Pod 998]: https://samson.zende.sk/projects/zendesk/stages/6400
[Pod 999]: https://samson.zende.sk/projects/zendesk/stages/7228
[Build Assets - Production]: https://samson.zende.sk/projects/zendesk/stages/9046
[stages]: https://samson.zende.sk/projects/5
[Production -- Canary]: https://samson.zende.sk/projects/zendesk/stages/8000
