x-zendesk-owner:
  name: bolt
  channel: "#ask-bolt"
parameters:
- $ref: "#/components/parameters/UserId"
- $ref: "#/components/parameters/UserIdentityId"
get:
  operationId: ShowUserIdentity
  tags:
  - User Identities
  summary: Show Identity
  description: |
    Shows the identity with the given id for a given user.

    #### Allowed For

    * Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/UserIdentityResponse.yaml
          examples:
            default:
              $ref: ../../../examples/UserIdentityResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{user_identity_id}.json \
        -v -u {email_address}:{password}
put:
  operationId: UpdateUserIdentity
  tags:
  - User Identities
  summary: Update Identity
  description: |
    This endpoint allows you to:

    * Set the specified identity as verified (but you cannot unverify a verified identity)
    * Update the `value` property of the specified identity

    You can't change an identity's `primary` attribute with this endpoint. You must use the [Make Identity Primary](#make-identity-primary) endpoint instead.

    #### Allowed For

    * Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/UserIdentityResponse.yaml
          examples:
            default:
              $ref: ../../../examples/UserIdentityUpdateResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    description: To update `verified`
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{user_identity_id}.json \
        -H "Content-Type: application/json" -X PUT \
        -d '{"identity": {"verified": true}}' -v -u {email_address}:{password}
  - lang: sh
    label: curl
    description: To update `value`
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{user_identity_id}.json \
        -H "Content-Type: application/json" -X PUT \
        -d '{"identity": {"value": "foo@bar.com"}}' -v -u {email_address}:{password}
delete:
  operationId: DeleteUserIdentity
  tags:
  - User Identities
  summary: Delete Identity
  description: |
    Deletes the identity for a given user.

    #### Allowed For
    * Agents
  response:
    "204":
      description: No Content response
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/identities/{user_identity_id}.json \
        -X DELETE -v -u {email_address}:{password}
