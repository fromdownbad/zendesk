x-zendesk-owner:
  channel: '#ask-boxoffice'
  name: boxoffice
post:
  operationId: CreateManyUsers
  tags:
  - Users
  summary: Create Many Users
  description: |
    Accepts an array of up to 100 user objects.

    **Note**: Access to this endpoint is limited. You may not be able to use
    it to bulk-import users or organizations. If you're unable, a 403 Forbidden
    error is returned. If you need access and can't use this endpoint,
    contact us at <a href="mailto:support@zendesk.com">support@zendesk.com</a>
    for assistance.

    #### Allowed For

    * Agents, with restrictions on certain actions

    #### Specifying an organization

    You can assign a user to an existing organization by setting an
    `organization_id` property in the user object.

    #### Response

    This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
    Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
  requestBody:
    required: true
    content:
      application/json:
        schema:
          $ref: ../../../schemas/UsersRequest.yaml
        examples:
          default:
            $ref: ../../../examples/UsersCreateManyRequestExample.yaml
  responses:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/JobStatusResponse.yaml
          examples:
            default:
              $ref: ../../../examples/JobStatusResponseExample.yaml
  x-code-samples:
  - label: cURL
    lang: sh
    source: |-
      curl https://{subdomain}.zendesk.com/api/v2/users/create_many.json \
        -d '{"users": [{"name": "Roger Wilco", "email": "roge@example.org", "role": "agent"}, {"name": "Woger Rilco", "email": "woge@example.org", "role": "admin"}]}' \
        -H "Content-Type: application/json" -X POST \
        -v -u {email_address}:{password}
