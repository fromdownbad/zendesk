x-zendesk-owner:
  channel: '#ask-boxoffice'
  name: boxoffice
post:
  operationId: CreateOrUpdateManyUsers
  tags:
  - Users
  summary: Create Or Update Many Users
  description: |
    Accepts an array of up to 100 user objects. For each user, the user is created if it does not
    already exist, or the existing user is updated.

    Each individual user object can identify an existing user by `email` or by `external_id`.

    This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
    Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.

    #### Allowed For

    * Agents, with restrictions on certain actions
  requestBody:
    required: true
    content:
      application/json:
        schema:
          $ref: ../../../schemas/UsersRequest.yaml
        examples:
          default:
            $ref: ../../../examples/UsersRequestExample.yaml
  responses:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/JobStatusResponse.yaml
          examples:
            default:
              $ref: ../../../examples/JobStatusResponseExample.yaml
  x-code-samples:
  - label: cURL
    lang: sh
    source: |-
      # Existing user identified by e-mail address:
      curl https://{subdomain}.zendesk.com/api/v2/users/create_or_update_many.json \
        -d '{"users": [{"name": "Roger Wilco", "email": "roge@example.org", "role": "agent"}, {"external_id": "account_54321", "name": "Woger Rilco", "email": "woge@example.org", "role": "admin"}]}' \
        -H "Content-Type: application/json" -X POST \
        -v -u {email_address}:{password}
