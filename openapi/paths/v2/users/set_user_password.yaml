x-zendesk-owner:
  name: bolt
  channel: "#ask-bolt"
parameters:
- $ref: "#/components/parameters/UserId"
post:
  operationId: SetUserPassword
  tags:
  - User Passwords
  summary: Set a User's Password
  description: |
    An admin can set a user's password only if the setting is enabled in Zendesk Support under **Settings** > **Security** > **Global**. The setting is off by default. Only the account owner can access and change this setting.

    #### Allowed For

    * Admins
  response:
    "200":
      description: Success description
      content:
        application/json:
          schema:
            type: string
            description: Empty response
            example: ""
          example: ""
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/password.json \
        -X POST -d '{"password": "newpassword"}' -H "Content-Type: application/json" \
        -v -u {email_address}:{password}
put:
  operationId: ChangeOwnPassword
  tags:
  - User Passwords
  summary: Change Your Password
  description: |
    You can only change your own password. Nobody can change the password of another user because it requires knowing the user's existing password. However, an admin can set a new password for another user without knowing the existing password. See [Set a User's Password](#set-a-users-password) above.

    #### Allowed For

    * Agents
    * End Users
  response:
    "200":
      description: Success description
      content:
        application/json:
          schema:
            type: string
            description: Empty response
            example: ""
          example: ""
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/users/{user_id}/password.json \
        -X PUT -d '{"previous_password": "oldpassword", "password": "newpassword"}' \
        -v -u {email_address}:{password} -H "Content-Type: application/json"
