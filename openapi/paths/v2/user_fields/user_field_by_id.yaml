x-zendesk-owner:
  name: bolt
  channel: "#ask-bolt"
parameters:
- $ref: "#/components/parameters/UserFieldId"
get:
  operationId: ShowUserField
  tags:
  - User Fields
  summary: Show User Field
  description: |
    #### Allowed for

    * Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/UserFieldResponse.yaml
          examples:
            default:
              $ref: ../../../examples/UserFieldResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/user_fields/{user_field_id}.json \
        -v -u {email_address}:{password}
put:
  operationId: UpdateUserField
  tags:
  - User Fields
  summary: Update User Field
  description: |
    #### Updating a Dropdown (Tagger) Field

    Dropdown fields return an array of `custom_field_options` which specify the name, value and order of the list of dropdown options.
    Understand the following behavior when updating a dropdown field:

    - All options must be passed on update. Options that are not passed will be removed. As a result, these values will be removed from any organizations.
    - To create a new option, pass a null `id` along with `name` and `value`.
    - To update an existing option, pass its `id` along with `name` and `value`.
    - To re-order an option, reposition it in the `custom_field_options` array relative to the other options.
    - To remove an option, omit it from the list of options upon update.

    #### Example Request

    ```bash
    curl https://{subdomain}.zendesk.com/api/v2/user_fields/{user_field_id}.json \
      -H "Content-Type: application/json" -X PUT \
      -d '{"user_field": {"custom_field_options": [{"id": 124, "name": "Option 2", "value": "option_2"}, {"id": 123, "name": "Option 1", "value": "option_1"}, {"id": 125, "name": "Option 2", "value": "option_3"}]}}' \
      -v -u {email_address}:{password}
    ```
    #### Allowed for

    * Admins
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/UserFieldResponse.yaml
          examples:
            default:
              $ref: ../../../examples/UserFieldUpdateResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/user_fields/{user_field_id}.json \
        -H "Content-Type: application/json" -X PUT \
        -d '{ "user_field": { "title": "Support description" }}' \
        -v -u {email_address}:{password}
delete:
  operationId: DeleteUserField
  tags:
  - User Fields
  summary: Delete User Field
  description: |
    #### Allowed for

    * Admins
  response:
    "204":
      description: No Content response
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/user_fields/{user_field_id}.json \
        -v -u {email_address}:{password} -X DELETE
