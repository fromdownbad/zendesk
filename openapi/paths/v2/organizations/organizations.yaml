x-zendesk-owner:
  name: bolt
  channel: "#ask-bolt"
get:
  operationId: ListOrganizations
  tags:
  - Organizations
  summary: List Organizations
  description: |
    #### Pagination

    * Cursor pagination (recommended)
    * Offset pagination

    See [Pagination](https://developer.zendesk.com/rest_api/docs/support/introduction#pagination).

    Returns a maximum of 100 records per page.

    #### Allowed For

    * Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/organizations/OrganizationsResponse.yaml
          examples:
            default:
              $ref: ../../../examples/organizations/OrganizationsResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organizations.json \
        -v -u {email_address}:{password}
  x-operation-aliases:
  - GET /api/v2/users/{user_id}/organizations.json
post:
  operationId: CreateOrganization
  tags:
  - Organizations
  summary: Create Organization
  description: |
    You must provide a unique `name` for each organization. Normally
    the system doesn't allow records to be created with identical names.
    However, a race condition can occur if you make two or more identical
    POSTs very close to each other, causing the records to have identical
    organization names.

    #### Allowed For

    * Admins
  response:
    "201":
      description: Created
      content:
        application/json:
          schema:
            $ref: ../../../schemas/organizations/OrganizationResponse.yaml
          examples:
            default:
              $ref: ../../../examples/organizations/CreatedOrganizationResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organizations.json \
        -H "Content-Type: application/json" -d '{"organization": {"name": "My Organization"}}' \
        -v -u {email_address}:{password}
