x-zendesk-owner:
  name: bolt
  channel: "#ask-bolt"
parameters:
- $ref: "#/components/parameters/OrganizationId"
get:
  operationId: ShowOrganization
  tags:
  - Organizations
  summary: Show Organization
  description: |
    #### Allowed For

    * Admins
    * Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/organizations/OrganizationResponse.yaml
          examples:
            default:
              $ref: ../../../examples/organizations/OrganizationResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organizations/{organization_id}.json \
        -v -u {email_address}:{password}
put:
  operationId: UpdateOrganization
  tags:
  - Organizations
  summary: Update Organization
  description: |
    #### Allowed For

    * Admins
    * Agents

    Agents with no permissions restrictions can only update "notes" on organizations.

    #### Example Request

    ```js
    {
      "organization": {
        "notes": "Something interesting"
      }
    }
    ```
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/organizations/OrganizationResponse.yaml
          examples:
            default:
              $ref: ../../../examples/organizations/UpdateOrganizationResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organizations/{organization_id}.json \
        -H "Content-Type: application/json" -d '{"organization": {"notes": "Something interesting"}}' \
        -v -u {email_address}:{password} -X PUT
delete:
  operationId: DeleteOrganization
  tags:
  - Organizations
  summary: Delete Organization
  description: |
    #### Allowed For

    * Admins
  response:
    "204":
      description: No Content Response
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organizations/{organization_id}.json \
        -v -u {email_address}:{password} -X DELETE
