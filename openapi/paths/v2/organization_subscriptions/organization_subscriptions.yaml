get:
  tags:
  - Organization Subscriptions
  operationId: ListOrganizationSubscriptions
  summary: List Organization Subscriptions
  description: |-
    #### Pagination

    * Cursor pagination (recommended)
    * Offset pagination

    See [Pagination](./introduction#pagination).

    Returns a maximum of 100 records per page.

    #### Allowed For:

    * Agents
    * End users

    For end users, the response will only list the subscriptions created by the requesting end user.
  responses:
    "200":
      description: Successful response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/OrganizationSubscriptionsResponse.yaml
          examples:
            default:
              $ref: ../../../examples/OrganizationSubscriptionsResponseExample.yaml
  x-code-samples:
  - label: cURL
    lang: sh
    source: |-
      curl https://{subdomain}.zendesk.com/api/v2/organization_subscriptions.json \
        -v -u {email_address}:{password}
  x-operation-aliases:
  - GET /api/v2/organizations/{organization_id}/subscriptions
  - GET /api/v2/users/{user_id}/organization_subscriptions
post:
  tags:
  - Organization Subscriptions
  operationId: CreateOrganizationSubscription
  summary: Create Organization Subscription
  description: |-
    #### Allowed For:

    * Agents
    * End users

    End users can only subscribe to shared organizations in which they're members.
  requestBody:
    content:
      application/json:
        schema:
          $ref: ../../../schemas/OrganizationSubscriptionCreateRequest.yaml
        examples:
          default:
            $ref: ../../../examples/OrganizationSubscriptionCreateRequestExample.yaml
  responses:
    "200":
      description: Successful response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/OrganizationSubscriptionResponse.yaml
          examples:
            default:
              $ref: ../../../examples/OrganizationSubscriptionResponseExample.yaml
  x-code-samples:
  - label: cURL
    lang: sh
    source: |-
      curl https://{subdomain}.zendesk.com/api/v2/organization_subscriptions.json \
        -d '{"organization_subscription": {"user_id": 772, "organization_id": 881}}' \
        -v -u {email_address}:{password} -H "Content-Type: application/json" -X POST
x-zendesk-owner:
  name: Bolt
  channel: "#ask-bolt"
