x-zendesk-owner:
  name: bolt
  channel: "#ask-bolt"
parameters:
- $ref: "#/components/parameters/OrganizationFieldId"
get:
  operationId: ShowOrganizationField
  tags:
  - Organization Fields
  summary: Show Organization Field
  description: |
    #### Allowed for

    * Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/OrganizationFieldResponse.yaml
          examples:
            default:
              $ref: ../../../examples/OrganizationFieldResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organization_fields/{organization_field_id}.json \
        -v -u {email_address}:{password}
put:
  operationId: UpdateOrganizationField
  tags:
  - Organization Fields
  summary: Update Organization Field
  description: |
    #### Updating a Drop-down (Tagger) Field

    Drop-down fields return an array of `custom_field_options` which specify the name, value, and order of drop-down options. When updating a drop-down field, note the following information:

    - All options must be passed on update. Options that are not passed will be removed. As a result, these values will be removed from any organizations
    - To create a new option, pass a null `id` along with the `name` and `value`
    - To update an existing option, pass its `id` along with the `name` and `value`
    - To reorder an option, reposition it in the `custom_field_options` array relative to the other options
    - To remove an option, omit it from the list of options upon update

    #### Example Request

    ```bash
    curl https://{subdomain}.zendesk.com/api/v2/organization_fields/{organization_field_id}.json \
      -H "Content-Type: application/json" -X PUT \
      -d '{"organization_field": {"custom_field_options": [{"id": 124, "name": "Option 2", "value": "option_2"}, {"id": 123, "name": "Option 1", "value": "option_1"}, {"id": 125, "name": "Option 3", "value": "option_3"}]}}' \
      -v -u {email_address}:{password}
    ```
    #### Allowed for

    * Admins
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/OrganizationFieldResponse.yaml
          examples:
            default:
              $ref: ../../../examples/OrganizationFieldUpdateResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organization_fields/{organization_field_id}.json \
        -H "Content-Type: application/json" -X PUT \
        -d '{ "organization_field": { "title": "Support description" }}' \
        -v -u {email_address}:{password}
delete:
  operationId: DeleteOrganizationField
  tags:
  - Organization Fields
  summary: Delete Organization Field
  description: |
    #### Allowed for

    * Admins
  response:
    "204":
      description: No Content response
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organization_fields/{organization_field_id}.json \
        -v -u {email_address}:{password} -X DELETE
