post:
  operationId: TicketsCreateMany
  summary: Create Many Tickets
  description: |-
    Accepts an array of up to 100 ticket objects. **Note**: Every ticket created with this endpoint may be affected by your business rules, which can include sending email notifications to your end users. If you are importing historical tickets or creating more than 1000 tickets, consider using the [Ticket Bulk Import](https://developer.zendesk.com/rest_api/docs/support/ticket_import#ticket-bulk-import) endpoint.

    This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
    Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
    
    #### Allowed For
    * Agents
  tags:
  - Tickets
  requestBody:
    content:
      application/json:
        schema:
          $ref: ../../../schemas/tickets/TicketsCreateRequest.yaml
        examples:
          default:
            $ref: ../../../examples/tickets/TicketsCreateRequestExample.yaml
  responses:
    '200':
      description: Create many tickets
      content:
        application/json:
          schema:
            $ref: ../../../schemas/JobStatusResponse.yaml
          examples:
            default:
              $ref: ../../../examples/JobStatusResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |-
      curl https://{subdomain}.zendesk.com/api/v2/tickets/create_many.json \
        -d '{"tickets": [{"subject": "My printer is on fire!", "comment": { "body": "The smoke is very colorful." }}, {"subject": "Help!", "comment": { "body": "Help I need somebody." }}]}' \
        -H "Content-Type: application/json" -v -u {email_address}:{password} -X POST
x-zendesk-owner:
  name: Box Office
  channel: ask-boxoffice
