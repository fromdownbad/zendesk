get:
  operationId: ListDeletedTickets
  summary: List deleted tickets
  tags:
  - Tickets
  description: |-
    Returns a maximum of 100 deleted tickets per page. See [Pagination](./introduction#pagination).

    The results includes all deleted (and not yet archived) tickets that
    have not yet been [scrubbed](https://help.zendesk.com/hc/en-us/articles/360000976667#topic_fv5_w51_sdb) in the past 30 days. Archived tickets are
    not included in the results. See [About archived tickets](https://support.zendesk.com/hc/en-us/articles/203657756)
    in the Support Help Center.

    The tickets are ordered chronologically by created date, from oldest to newest.
    The first ticket listed may not be the oldest ticket in your
    account due to [ticket archiving](https://support.zendesk.com/hc/en-us/articles/203657756).

    #### Allowed For

    * Agents
  parameters:
  - $ref: "#/components/parameters/TicketSortBy"
  - $ref: "#/components/parameters/TicketSortOrder"
  responses:
    '200':
      description: Successful response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/tickets/ListDeletedTicketsResponse.yaml 
          examples:
            default:
              $ref: ../../../examples/tickets/ListDeletedTicketsResponseExample.yaml 
  x-code-samples:
  - lang: sh
    label: curl
    source: |-
      curl https://{subdomain}.zendesk.com/api/v2/deleted_tickets.json \
        -v -u {email_address}:{password}
x-zendesk-owner:
  name: Box Office
  channel: ask-boxoffice