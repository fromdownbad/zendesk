post:
  operationId: MergeTicketsIntoTargetTicket
  summary: Merge Tickets into Target Ticket
  tags:
  - Tickets
  description: |-
    Merges one or more tickets into the ticket with the specified id.

    See [Merging tickets](https://support.zendesk.com/hc/en-us/articles/203690916)
    in the Support Help Center for ticket merging rules.

    Any attachment to the source ticket is copied to the target ticket.

    This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
    Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.

    #### Allowed For

    * Agents

    Agents in the Enterprise account must have merge permissions.
    See [Creating custom roles and assigning agents (Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662026)
    in the Support Help Center.

    #### Available parameters

    The request takes a data object with the following properties:

    | Name                     | Type    | Required | Comments                                                |
    | ------------------------ | ------- | -------- | ------------------------------------------------------- |
    | ids                      | array   | yes      | Ids of tickets to merge into the target ticket          |
    | target_comment           | string  | no       | Private comment to add to the target ticket             |
    | source_comment           | string  | no       | Private comment to add to the source ticket             |
    | target_comment_is_public | boolean | no       | Whether comment in target ticket is public or private   |
    | source_comment_is_public | boolean | no       | Whether comment in source tickets are public or private |

    The comments are optional but strongly recommended.

    Comments are private and can't be modified in the following cases:

      * Any of the sources or target tickets are private
      * Any of the sources or target tickets were created through Twitter, Facebook or the Channel framework
    
    In any other case, comments default to private but can be modified with the comment privacy parameters.
  parameters:
  - $ref: "#/components/parameters/TicketId"
  requestBody:
    content:
      application/json:
        schema:
            $ref: ../../../schemas/tickets/TicketMergeInput.yaml
        examples:
          default:
            $ref: ../../../examples/tickets/TicketMergeInputExample.yaml
  responses:
    '200':
      description: Successful response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/JobStatusResponse.yaml
          examples:
            default:
              $ref: ../../../examples/JobStatusResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |-
      curl https://{subdomain}.zendesk.com/api/v2/tickets/{ticket_id}/merge.json \
        -v -u {email_address}:{password} -X POST \
        -d '{"ids": [123, 456, 789], "source_comment": "Closing in favor of #111", "target_comment": "Combining with #123, #456, #789"}' \
        -H "Content-Type: application/json"
x-zendesk-owner:
  name: Box Office
  channel: ask-boxoffice