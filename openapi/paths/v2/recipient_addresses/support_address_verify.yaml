x-zendesk-owner:
  name: Strong Bad
  channel: "#ask-email-eng"
parameters:
- $ref: "#/components/parameters/SupportAddressId"
get:
  operationId: VerifySupportAddressForwarding
  tags:
  - Support Addresses
  summary: Verify Support Address Forwarding
  description: |
    Sends a test email to the specified support address to verify that email forwarding for the address works. An external support address won't work in Zendesk Support until it's verified.

    **Note**: You don't need to verify Zendesk support addresses.

    The endpoint takes the following body parameter: `{"type": "forwarding"}`.

    Use this endpoint after [adding](#create-support-address) an external support address to Zendesk Support and setting up forwarding on your email server. See [Forwarding incoming email to Zendesk Support](https://support.zendesk.com/hc/en-us/articles/203663266).

    The endpoint doesn't return the results of the test. Instead, use the [Show Support Address](#show-support-address) endpoint to check that the `forwarding_status` property is "verified".

    #### Allowed For

    * Admins
    * Agents with permission to manage channels and extensions. See the system permissions in [Creating custom roles and assigning agents (Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662026-Creating-custom-roles-and-assigning-agents-Enterprise-#topic_cxn_hig_bd) in the Support Help Center
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            type: string
            description: Empty response
            example: ""
          example: ""
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/recipient_addresses/{support_address_id}/verify.json \
        -H "Content-Type: application/json" -X PUT \
        -d '{"type": "forwarding"}' \
        -v -u {email_address}:{password}
