x-zendesk-owner:
  name: Bolt
  channel: "#ask-bolt"
get:
  operationId: ListOrganizationMemberships
  tags:
  - Organization Memberships
  summary: List Memberships
  description: |
    Returns a list of organization memberships for the account, user or organization in question.

    **Note**: When returning organization memberships for a user, organization memberships are sorted with the default organization first, and then by organization name.

    #### Pagination

    * Cursor pagination (recommended)
    * Offset pagination

    See [Pagination](./introduction#pagination).

    Returns a maximum of 100 records per page.

    #### Allowed For

    - Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/OrganizationMembershipsResponse.yaml
          examples:
            default:
              $ref: ../../../examples/OrganizationMembershipsResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organization_memberships.json \
        -v -u {email_address}:{password}
  x-operation-aliases:
  - GET /api/v2/users/{user_id}/organization_memberships.json
  - GET /api/v2/organizations/{organization_id}/organization_memberships.json
post:
  operationId: CreateOrganizationMembership
  tags:
  - Organization Memberships
  summary: Create Membership
  description: |
    Assigns a user to a given organization. Returns an error with status 422 if the user is already assigned to the organization.

    #### Allowed For

    * Agents
  response:
    "201":
      description: Created response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/OrganizationMembershipResponse.yaml
          examples:
            default:
              $ref: ../../../examples/OrganizationMembershipCreateResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/organization_memberships.json \
        -X POST -d '{"organization_membership": {"user_id": 72, "organization_id": 88}}' \
        -H "Content-Type: application/json" -v -u {email_address}:{password}
  x-operation-aliases:
  - POST /api/v2/users/{user_id}/organization_memberships
