x-zendesk-owner:
  name: boxoffice
  channel: "#ask-boxoffice"
get:
  operationId: ListTicketFields
  tags:
  - Ticket Fields
  summary: List Ticket Fields
  description: |
    Returns a list of all system and custom ticket fields in your account.

    The results are not paginated. Every field is returned in the response.

    Fields are returned in the order specified by the position and id.

    For accounts without access to multiple ticket forms, positions can be changed using the [Update Ticket Field](./ticket_fields#update-ticket-field) endpoint or the Ticket Forms page in Zendesk Support (**Admin** > **Manage** > **Ticket Forms**). The Ticket Forms page shows the fields for the account. The order of the fields is used in the different products to show the field values in the tickets.

    For accounts with access to multiple ticket forms, positions can only be changed using the [Update Ticket Field](./ticket_fields#update-ticket-field) endpoint because products use the order defined on each form to show the field values instead of the general position of the ticket field in the account.

    Consider caching this resource to use with the [Tickets](./tickets#json-format) API.

    #### Allowed For

    * Agents
  response:
    "200":
      description: Success response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/TicketFieldsResponse.yaml
          examples:
            default:
              $ref: ../../../examples/TicketFieldsResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/ticket_fields.json \
        -v -u {email_address}:{password}
post:
  operationId: CreateTicketField
  tags:
  - Ticket Fields
  summary: Create Ticket Field
  description: |
    Creates any of the following custom field types:

    * text (default when no "type" is specified)
    * textarea
    * checkbox
    * date
    * integer
    * decimal
    * regexp
    * partialcreditcard
    * multiselect (multi-select dropdown menu)
    * tagger (single-select dropdown menu)

    See [About custom field types](https://support.zendesk.com/hc/en-us/articles/203661866) in the Zendesk Help Center.

    #### Allowed For

    * Admins

    #### Field limits

    * 400 ticket fields per account if your account doesn't have ticket forms
    * 400 ticket fields per ticket form if your account has ticket forms
  response:
    "201":
      description: Created response
      content:
        application/json:
          schema:
            $ref: ../../../schemas/TicketFieldResponse.yaml
          examples:
            default:
              $ref: ../../../examples/TicketFieldResponseExample.yaml
  x-code-samples:
  - lang: sh
    label: curl
    source: |
      curl https://{subdomain}.zendesk.com/api/v2/ticket_fields.json \
        -d '{"ticket_field": {"type": "text", "title": "Age"}}' \
        -H "Content-Type: application/json" -X POST \
        -v -u {email_address}:{password}
