In addition to the general properties above, additional properties may be available depending on the event `type`:

| Name    | Type    | Read-only | Mandatory | Comment                                                                                                                                    |
|---------|---------|-----------|-----------|--------------------------------------------------------------------------------------------------------------------------------------------|
| sla     | object  | yes       | no        | Available if `type` is `apply_sla`. The SLA policy and target being enforced on the ticket and metric in question, if any. See [sla](#sla) |
| status  | object  | yes       | no        | Available if `type` is `update_status`. Minutes since the metric has been open. See [status](#status)                                      |
| deleted | boolean | yes       | no        | Available if `type` is `breach`. In general, you can ignore any breach event when `deleted` is true. See [deleted](#deleted)               |

#### instance_id
Use the `instance_id` property to track each instance of a metric event
that can occur more than once per ticket, such as the `reply_time` event.
The value increments over the lifetime of the ticket.
#### sla
Optional. The `sla` property provides key information about the SLA policy
and target being enforced on the ticket and metric in question. The target
time is provided in minutes, along with whether the target is being
measured in business or calendar hours. Policy information is also provided,
including the ID, title, and description of the policy currently applied to
the ticket.
#### status
Optional. The `status` property provides the number of minutes in both
business and calendar hours for which the metric has been open. The `status`
property is only updated for a `fulfill` event. Any ticket metric that
hasn't breached yet or fulfilled at least once won't have a calculated status.
#### deleted
Optional. The `deleted` property is only used to indicate whether or not
a breach event should be ignored. In general, you can ignore any breach
event where `deleted` is true.