Besides the common fields, the target may have extra properties depending on its type.

"basecamp_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| target_url      | string   | yes       | The URL of your Basecamp account, including protocol and path
| username        | string   |           | The 37Signals username of the account you use to log in to Basecamp
| password        | string   |           | The 37Signals password for the Basecamp account (only writable)
| token           | string   | yes       | Get the API token from My info > Show your tokens > Token for feed readers or the Basecamp API in your Basecamp account
| project_id      | string   | yes       | The ID of the project in Basecamp where updates should be pushed
| resource        | string   | yes       | "todo" or "message"
| message_id      | string   |           | Can be filled if it is a "todo" resource
| todo_list_id    | string   |           | Can be filled if it is a "message" resource

"campfire_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| subdomain       | string   | yes       |
| ssl             | boolean  |           |
| room            | string   | yes       |
| token           | string   | yes       |
| preserve_format | boolean  |           |

"clickatell_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| target_url      | string   |           | Read-only
| method          | string   |           | Read-only
| attribute       | string   |           | Read-only
| username        | string   | yes       |
| password        | string   | yes       | only writable
| api_id          | string   | yes       |
| to              | string   | yes       |
| from            | string   |           |
| us_small_business_account | string   | Possible values: "0" or "1" for false or true |

"email_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| email           | string   | yes       |
| subject         | string   | yes       |

"flowdock_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| api_token       | string   | yes       |

"get_satisfaction_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| email           | string   | yes       |
| password        | string   | yes       | only writable
| account_name    | string   | yes       |
| target_url      | string   |           |

"jira_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| target_url      | string   | yes       |
| username        | string   | yes       |
| password        | string   | yes       | only writable

"pivotal_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| token           | string   | yes       |
| project_id      | string   | yes       |
| story_type      | string   | yes       |
| story_title     | string   | yes       |
| requested_by    | string   |           |
| owner_by        | string   |           |
| story_labels    | string   |           |

"twitter_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| token           | string   |           |
| secret          | string   |           | only writable

"url_target" has the following additional attributes:

| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| target_url      | string   | yes       |
| method          | string   |           | "get"
| attribute       | string   | yes       |
| username        | string   |           |
| password        | string   |           | only writable

"http_target" has the following additional attributes:


| Name            | Type     | Mandatory | Comment
| --------------- | ---------| --------- | -------
| target_url      | string   | yes       |
| method          | string   | yes       | "get", "patch", "put", "post", or "delete"
| username        | string   |           |
| password        | string   |           | only writable
| content_type    | string   | yes       | "application/json", "application/xml", or "application/x-www-form-urlencoded"

"yammer_target" has the following additional attributes:

| Name               | Type     | Mandatory | Comment
| ------------------ | ---------| --------- | -------
| group_id           | string   |           |
| token              | string   |           |
