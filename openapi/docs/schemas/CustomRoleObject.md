#### Configuration
The `configuration` object has the following properties, which are all optional.

| Name                             | Type    | Read-only | Comment
| -------------------------------- | ------- | --------- | -------
| chat_access                      | boolean | yes       | Whether or not the agent has access to Chat
| end_user_list_access             | string  | no        | Whether or not the agent can view lists of user profiles. Allowed values: "full", "none"
| end_user_profile_access          | string  | no        | What the agent can do with end-user profiles. Allowed values: "edit", "edit-within-org", "full", "readonly"
| explore_access                   | string  | no        | Allowed values: "edit", "full", "none", "readonly"
| forum_access                     | string  | no        | The kind of access the agent has to Guide. Allowed values: "edit-topics", "full", "readonly"
| forum_access_restricted_content  | boolean | no        |
| group_access                     | boolean | yes       | Whether or not the agent can add or modify groups
| light_agent                      | boolean | yes       |
| macro_access                     | string  | no        | What the agent can do with macros. Allowed values: "full", "manage-group", "manage-personal", "readonly"
| manage_business_rules            | boolean | no        | Whether or not the agent can manage business rules
| manage_dynamic_content           | boolean | no        | Whether or not the agent can access dynamic content
| manage_extensions_and_channels   | boolean | no        | Whether or not the agent can manage channels and extensions
| manage_facebook                  | boolean | no        | Whether or not the agent can manage facebook pages
| moderate_forums                  | boolean | yes       |
| organization_editing             | boolean | no        | Whether or not the agent can add or modify organizations
| organization_notes_editing       | boolean | yes       | Whether or not the agent can add or modify organization notes
| report_access                    | string  | no        | What the agent can do with reports. Allowed values: "full", "none", "readonly"
| ticket_access                    | string  | no        | What kind of tickets the agent can access. Allowed values: "all", "assigned-only", "within-groups", "within-organization"
| ticket_comment_access            | string  | no        | What type of comments the agent can make. Allowed values: "public", "none"
| ticket_deletion                  | boolean | no        | Whether or not the agent can delete tickets
| ticket_editing                   | boolean | no        | Whether or not the agent can edit ticket properties
| ticket_merge                     | boolean | no        | Whether or not the agent can merge tickets
| ticket_tag_editing               | boolean | no        | Whether or not the agent can edit ticket tags
| twitter_search_access            | boolean | no        |
| user_view_access                 | string  | no        | What the agent can do with customer lists. Allowed values: "full", "manage-group", "manage-personal", "none", "readonly"
| view_access                      | string  | no        | What the agent can do with views. Allowed values: "full", "manage-group", "manage-personal", "playonly", "readonly"
| view_deleted_tickets             | boolean | no        | Whether or not the agent can view deleted tickets
| voice_access                     | boolean | no        | Whether or not the agent has access to Talk

#### Example
```js
{
  "id":           35436,
  "name":         "Partner",
  "description":  "Can only make private comments on assigned tickets",
  "created_at":   "2012-02-20T22:55:29Z",
  "updated_at":   "2012-02-20T22:55:29Z",
  "configuration": {
    "chat_access":                     true,
    "end_user_profile_access":                "readonly",
    "explore_access":                  "edit",
    "forum_access":                    "readonly",
    "forum_access_restricted_content": false,
    "light_agent":                     false,
    "macro_access":                    "full",
    "manage_business_rules":           true,
    "manage_dynamic_content":          false,
    "manage_extensions_and_channels":  true,
    "manage_facebook":                 false,
    "organization_editing":            false,
    "organization_notes_editing":      false,
    "report_access":                   "none",
    "ticket_access":                   "within-groups",
    "ticket_comment_access":           "none",
    "ticket_deletion":                 false,
    "view_deleted_tickets":            false,
    "ticket_editing":                  true,
    "ticket_merge":                    false,
    "ticket_tag_editing":              true,
    "twitter_search_access":           true,
    "view_access":                     "full",
    "voice_access":                    true
  }
}
```
